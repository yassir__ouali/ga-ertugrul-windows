﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Svelto.DataStructures.FasterList`1<Svelto.Tasks.IPausableTask>
struct FasterList_1_t2B537C5D221BBAAC4DA80F69541C173562A8478A;
// Svelto.DataStructures.FasterList`1<System.Collections.Generic.Stack`1<System.Collections.IEnumerator>>
struct FasterList_1_t886A5ED25FB67F8EC1D7DFF6CEA1170A4D44691D;
// Svelto.DataStructures.LockFreeQueue`1<Svelto.Tasks.Internal.PausableTask>
struct LockFreeQueue_1_t7C81336F4B1C44CA51A59FBAA9F56374D28E451A;
// Svelto.DataStructures.ThreadSafeDictionary`2<System.String,Svelto.Tasks.Profiler.TaskInfo>
struct ThreadSafeDictionary_2_t0EA3C0E086E61CDEFFED886B494936B21F921C5C;
// Svelto.DataStructures.ThreadSafeQueue`1<Svelto.Tasks.IPausableTask>
struct ThreadSafeQueue_1_t4B42DDB151ED238066A071C89AF1FC3DB9C1BE45;
// Svelto.Tasks.ContinuationWrapper
struct ContinuationWrapper_t135700DA709759BA12FEE8996B57AF89BFE20399;
// Svelto.Tasks.CoroutineMonoRunner
struct CoroutineMonoRunner_tE7E5367413EE32878D5A3D8EC78406F687A475C5;
// Svelto.Tasks.IAbstractTask
struct IAbstractTask_tA0CB1873FA0D494A27C65C461B3BA0422BD04C28;
// Svelto.Tasks.IRunner
struct IRunner_t96158A9FB6851BBF0F23AEF5C493D58110CEDD50;
// Svelto.Tasks.ITaskRoutine[]
struct ITaskRoutineU5BU5D_tB6D6BF001B80D26410DC0893D3B2B883DB81969E;
// Svelto.Tasks.Internal.PausableTaskPool
struct PausableTaskPool_t650A2D678FDFAA152B5CB537987A31A82A014D28;
// Svelto.Tasks.Internal.RunnerBehaviourEndOfFrame
struct RunnerBehaviourEndOfFrame_t62CC360021E4159E5AFD077D2A603BD4450B1555;
// Svelto.Tasks.Internal.UnityCoroutineRunner/FlushingOperation
struct FlushingOperation_tB0B9CC8E029067E8D130896A68A0E2D390E01027;
// Svelto.Tasks.Internal.UnityCoroutineRunner/RunningTasksInfo
struct RunningTasksInfo_t10813DB30B3A26BF8CF0830C17B2B3EE5DBF523C;
// Svelto.Tasks.LateMonoRunner
struct LateMonoRunner_t78843F08125CAA775F86434EFC83B5828B6D69AA;
// Svelto.Tasks.MultiThreadRunner
struct MultiThreadRunner_tC4A17C81CFC215E6A3719AADB82ACFB718255F92;
// Svelto.Tasks.MultiThreadRunner[]
struct MultiThreadRunnerU5BU5D_t51722EA373DB40FD79EC225511635908DD497C52;
// Svelto.Tasks.ParallelTaskCollection
struct ParallelTaskCollection_t8DFC5A6DAFD6AFB617F77CF0767758AF060049DA;
// Svelto.Tasks.ParallelTaskCollection[]
struct ParallelTaskCollectionU5BU5D_tF1DED26D88B5622CF291D46EFBA6B774870A6161;
// Svelto.Tasks.PhysicMonoRunner
struct PhysicMonoRunner_tF4F299E77C8B07EF8D39343FF568FAE80CF909F6;
// Svelto.Tasks.SerialTaskCollection
struct SerialTaskCollection_t31E5E0CD4A5BAA251A396D5B51F12E1FB6B68BE7;
// Svelto.Tasks.StaggeredMonoRunner/FlushingOperationStaggered
struct FlushingOperationStaggered_tB9B268018A84589696B8AC6CA3B9B8E6778AF1F0;
// Svelto.Tasks.UpdateMonoRunner
struct UpdateMonoRunner_tD70489504A43A0A4ADA16547CB1203C1FDC1B54C;
// Svelto.Utilities.ManualResetEventEx
struct ManualResetEventEx_t33E2A70F227B45E36C283FE859544123EC73BB78;
// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.Action`1<Svelto.Tasks.PausableTaskException>
struct Action_1_tCB2457F2DCF5883C9AC781663523E6FF0009B33C;
// System.Action`1<System.Single>
struct Action_1_tCBF7F8C203F735692364E5850B0A960E0EABD03B;
// System.Action`1<UnityEngine.Color>
struct Action_1_tB42F900CEA2D8B71F6167E641B4822FF8FB34159;
// System.Action`3<System.Exception,System.String,System.String>
struct Action_3_t027A7299AE99097D560366120BFE2FB96A80B743;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.List`1<System.Action>
struct List_1_t9E7F53A878191B703A76E69CE36AA90A372800E7;
// System.Collections.Generic.List`1<System.Single>
struct List_1_t8980FA0E6CB3848F706C43D859930435C34BCC37;
// System.Collections.Generic.List`1<Tayx.Graphy.GraphyDebugger/DebugCondition>
struct List_1_t2AE6E2AF8D86C9DAB57BDAC45B5116E9C3E277D8;
// System.Collections.Generic.List`1<UnityEngine.AudioClip>
struct List_1_tE0FA93AA3DC624E2BF3DC417F5FEBE1DBD81256F;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t99909CDEDA6D21189884AEA74B1FD99FC9C6A4C0;
// System.Collections.Generic.List`1<UnityEngine.UI.Image>
struct List_1_t5E6CEE165340A9D74D8BD47B8E6F422DFB7744ED;
// System.Collections.Generic.Queue`1<System.Double>
struct Queue_1_t4EE8B6A18F07374C2B3FF98A98B5369D5542EF98;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.Collections.IEnumerator
struct IEnumerator_t8789118187258CC88B77AFAC6315B5AF87D3E18A;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.Diagnostics.Stopwatch
struct Stopwatch_t0778B5C8DF8FE1D87FC57A2411DA695850BD64D4;
// System.Func`1<System.Boolean>
struct Func_1_t4ABD6DAD480574F152452DD6B9C9A55F4F6655F1;
// System.Func`1<System.Collections.IEnumerator>
struct Func_1_tB87849BEE3BBD29FEB301AB876F67ADA1BFEC3AB;
// System.Func`2<System.Exception,System.Boolean>
struct Func_2_tC816C5FDED4A7372C449E7660CAB9F94E2AC12F8;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.Predicate`1<Tayx.Graphy.GraphyDebugger/DebugPacket>
struct Predicate_1_tC4C6DB54C5F647220AFB7B208EACB0A2F3B13B81;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.Single[]
struct SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// Tayx.Graphy.Advanced.G_AdvancedData
struct G_AdvancedData_t0796834EAEDC1D020D0AF60EB6D8A19A6CB61F6E;
// Tayx.Graphy.Audio.G_AudioGraph
struct G_AudioGraph_t02349ECE72DF71E24A86159551A347D323CB37CB;
// Tayx.Graphy.Audio.G_AudioManager
struct G_AudioManager_t19B7806D5B65E31D8B338BF53B13C8ACB6057F60;
// Tayx.Graphy.Audio.G_AudioMonitor
struct G_AudioMonitor_t386A2D77995058DA7241FC1C90AF9FF6DE38EA2E;
// Tayx.Graphy.Audio.G_AudioText
struct G_AudioText_t7BCE4D478232F4284809B83E3150DBCC8DC64BF3;
// Tayx.Graphy.CustomizationScene.G_CUIColorPicker
struct G_CUIColorPicker_t65243BB7AF1C8840635414A3F44F39FCBB1B3D83;
// Tayx.Graphy.Fps.G_FpsGraph
struct G_FpsGraph_t3DAC783275A67E078E4C1C2338F2DD9F03D14DEF;
// Tayx.Graphy.Fps.G_FpsManager
struct G_FpsManager_tEB48DC94964043565EAC186648A2F0FCD4794243;
// Tayx.Graphy.Fps.G_FpsMonitor
struct G_FpsMonitor_tDCDEDA2708585BE6B0202F2B3230D8C5F6820672;
// Tayx.Graphy.Fps.G_FpsText
struct G_FpsText_tDA7B016BED64F8C283E352FEB686BD8372567DE5;
// Tayx.Graphy.G_GraphShader
struct G_GraphShader_t3D41D924F74863B5AFC8C101907225362954A8D3;
// Tayx.Graphy.GraphyManager
struct GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF;
// Tayx.Graphy.Ram.G_RamGraph
struct G_RamGraph_t3408E644B21F1A8F7E4A4932CB72BBE7483130CE;
// Tayx.Graphy.Ram.G_RamManager
struct G_RamManager_t2C694E71E71ABB53B78046251A3430D920B9B0C9;
// Tayx.Graphy.Ram.G_RamMonitor
struct G_RamMonitor_tCE95C3ECC0C03781E405698F56E20B12BC2650CA;
// Tayx.Graphy.Ram.G_RamText
struct G_RamText_t1097A490F592CBE6D7CF0EF4BA0232252CDE5806;
// UnityEngine.AudioListener
struct AudioListener_tE3E1467B84A4AFD509947B44A7C8ACFB67FF2099;
// UnityEngine.AudioSource
struct AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C;
// UnityEngine.Color[]
struct ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.RectTransform
struct RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20;
// UnityEngine.Shader
struct Shader_tE2731FF351B74AB4186897484FB01E000C1160CA;
// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C;
// UnityEngine.UI.Button
struct Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B;
// UnityEngine.UI.Dropdown
struct Dropdown_tF6331401084B1213CAB10587A6EC81461501930F;
// UnityEngine.UI.Image
struct Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E;
// UnityEngine.UI.InputField
struct InputField_t533609195B110760BCFF00B746C87D81969CB005;
// UnityEngine.UI.Slider
struct Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09;
// UnityEngine.UI.Text
struct Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030;
// UnityEngine.UI.Toggle
struct Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106;
// UnityEngine.WaitForEndOfFrame
struct WaitForEndOfFrame_t75980FB3F246D6AD36A85CA2BFDF8474E5EEBCCA;
// Utility.ILogger
struct ILogger_tD19BC17F824D7F3F27535058D85C0BA26503DE37;

struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef CHECK_T12CC5342B775CEA4EF3935C4572D5CAAD7A99834_H
#define CHECK_T12CC5342B775CEA4EF3935C4572D5CAAD7A99834_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DBC.Check
struct  Check_t12CC5342B775CEA4EF3935C4572D5CAAD7A99834  : public RuntimeObject
{
public:

public:
};

struct Check_t12CC5342B775CEA4EF3935C4572D5CAAD7A99834_StaticFields
{
public:
	// System.Boolean DBC.Check::useAssertions
	bool ___useAssertions_0;

public:
	inline static int32_t get_offset_of_useAssertions_0() { return static_cast<int32_t>(offsetof(Check_t12CC5342B775CEA4EF3935C4572D5CAAD7A99834_StaticFields, ___useAssertions_0)); }
	inline bool get_useAssertions_0() const { return ___useAssertions_0; }
	inline bool* get_address_of_useAssertions_0() { return &___useAssertions_0; }
	inline void set_useAssertions_0(bool value)
	{
		___useAssertions_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHECK_T12CC5342B775CEA4EF3935C4572D5CAAD7A99834_H
#ifndef TRACE_T0AC06925CD6AFB335DCBDE5EA04106D467EDC943_H
#define TRACE_T0AC06925CD6AFB335DCBDE5EA04106D467EDC943_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DBC.Trace
struct  Trace_t0AC06925CD6AFB335DCBDE5EA04106D467EDC943  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACE_T0AC06925CD6AFB335DCBDE5EA04106D467EDC943_H
#ifndef BREAK_T16D4B71AB2F870282D0F365D3B777C915C2576C4_H
#define BREAK_T16D4B71AB2F870282D0F365D3B777C915C2576C4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Svelto.Tasks.Break
struct  Break_t16D4B71AB2F870282D0F365D3B777C915C2576C4  : public RuntimeObject
{
public:

public:
};

struct Break_t16D4B71AB2F870282D0F365D3B777C915C2576C4_StaticFields
{
public:
	// Svelto.Tasks.Break Svelto.Tasks.Break::It
	Break_t16D4B71AB2F870282D0F365D3B777C915C2576C4 * ___It_0;
	// Svelto.Tasks.Break Svelto.Tasks.Break::AndStop
	Break_t16D4B71AB2F870282D0F365D3B777C915C2576C4 * ___AndStop_1;

public:
	inline static int32_t get_offset_of_It_0() { return static_cast<int32_t>(offsetof(Break_t16D4B71AB2F870282D0F365D3B777C915C2576C4_StaticFields, ___It_0)); }
	inline Break_t16D4B71AB2F870282D0F365D3B777C915C2576C4 * get_It_0() const { return ___It_0; }
	inline Break_t16D4B71AB2F870282D0F365D3B777C915C2576C4 ** get_address_of_It_0() { return &___It_0; }
	inline void set_It_0(Break_t16D4B71AB2F870282D0F365D3B777C915C2576C4 * value)
	{
		___It_0 = value;
		Il2CppCodeGenWriteBarrier((&___It_0), value);
	}

	inline static int32_t get_offset_of_AndStop_1() { return static_cast<int32_t>(offsetof(Break_t16D4B71AB2F870282D0F365D3B777C915C2576C4_StaticFields, ___AndStop_1)); }
	inline Break_t16D4B71AB2F870282D0F365D3B777C915C2576C4 * get_AndStop_1() const { return ___AndStop_1; }
	inline Break_t16D4B71AB2F870282D0F365D3B777C915C2576C4 ** get_address_of_AndStop_1() { return &___AndStop_1; }
	inline void set_AndStop_1(Break_t16D4B71AB2F870282D0F365D3B777C915C2576C4 * value)
	{
		___AndStop_1 = value;
		Il2CppCodeGenWriteBarrier((&___AndStop_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BREAK_T16D4B71AB2F870282D0F365D3B777C915C2576C4_H
#ifndef PAUSABLETASKPOOL_T650A2D678FDFAA152B5CB537987A31A82A014D28_H
#define PAUSABLETASKPOOL_T650A2D678FDFAA152B5CB537987A31A82A014D28_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Svelto.Tasks.Internal.PausableTaskPool
struct  PausableTaskPool_t650A2D678FDFAA152B5CB537987A31A82A014D28  : public RuntimeObject
{
public:
	// Svelto.DataStructures.LockFreeQueue`1<Svelto.Tasks.Internal.PausableTask> Svelto.Tasks.Internal.PausableTaskPool::_pool
	LockFreeQueue_1_t7C81336F4B1C44CA51A59FBAA9F56374D28E451A * ____pool_0;

public:
	inline static int32_t get_offset_of__pool_0() { return static_cast<int32_t>(offsetof(PausableTaskPool_t650A2D678FDFAA152B5CB537987A31A82A014D28, ____pool_0)); }
	inline LockFreeQueue_1_t7C81336F4B1C44CA51A59FBAA9F56374D28E451A * get__pool_0() const { return ____pool_0; }
	inline LockFreeQueue_1_t7C81336F4B1C44CA51A59FBAA9F56374D28E451A ** get_address_of__pool_0() { return &____pool_0; }
	inline void set__pool_0(LockFreeQueue_1_t7C81336F4B1C44CA51A59FBAA9F56374D28E451A * value)
	{
		____pool_0 = value;
		Il2CppCodeGenWriteBarrier((&____pool_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAUSABLETASKPOOL_T650A2D678FDFAA152B5CB537987A31A82A014D28_H
#ifndef U3CWAITFORENDOFFRAMELOOPU3ED__2_TD82F296AFE6584098A6FA2F78BBB9F16730287FA_H
#define U3CWAITFORENDOFFRAMELOOPU3ED__2_TD82F296AFE6584098A6FA2F78BBB9F16730287FA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Svelto.Tasks.Internal.RunnerBehaviourEndOfFrame_<WaitForEndOfFrameLoop>d__2
struct  U3CWaitForEndOfFrameLoopU3Ed__2_tD82F296AFE6584098A6FA2F78BBB9F16730287FA  : public RuntimeObject
{
public:
	// System.Int32 Svelto.Tasks.Internal.RunnerBehaviourEndOfFrame_<WaitForEndOfFrameLoop>d__2::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Svelto.Tasks.Internal.RunnerBehaviourEndOfFrame_<WaitForEndOfFrameLoop>d__2::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Svelto.Tasks.Internal.RunnerBehaviourEndOfFrame Svelto.Tasks.Internal.RunnerBehaviourEndOfFrame_<WaitForEndOfFrameLoop>d__2::<>4__this
	RunnerBehaviourEndOfFrame_t62CC360021E4159E5AFD077D2A603BD4450B1555 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitForEndOfFrameLoopU3Ed__2_tD82F296AFE6584098A6FA2F78BBB9F16730287FA, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitForEndOfFrameLoopU3Ed__2_tD82F296AFE6584098A6FA2F78BBB9F16730287FA, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CWaitForEndOfFrameLoopU3Ed__2_tD82F296AFE6584098A6FA2F78BBB9F16730287FA, ___U3CU3E4__this_2)); }
	inline RunnerBehaviourEndOfFrame_t62CC360021E4159E5AFD077D2A603BD4450B1555 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline RunnerBehaviourEndOfFrame_t62CC360021E4159E5AFD077D2A603BD4450B1555 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(RunnerBehaviourEndOfFrame_t62CC360021E4159E5AFD077D2A603BD4450B1555 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWAITFORENDOFFRAMELOOPU3ED__2_TD82F296AFE6584098A6FA2F78BBB9F16730287FA_H
#ifndef FLUSHINGOPERATION_TB0B9CC8E029067E8D130896A68A0E2D390E01027_H
#define FLUSHINGOPERATION_TB0B9CC8E029067E8D130896A68A0E2D390E01027_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Svelto.Tasks.Internal.UnityCoroutineRunner_FlushingOperation
struct  FlushingOperation_tB0B9CC8E029067E8D130896A68A0E2D390E01027  : public RuntimeObject
{
public:
	// System.Boolean Svelto.Tasks.Internal.UnityCoroutineRunner_FlushingOperation::stopped
	bool ___stopped_0;

public:
	inline static int32_t get_offset_of_stopped_0() { return static_cast<int32_t>(offsetof(FlushingOperation_tB0B9CC8E029067E8D130896A68A0E2D390E01027, ___stopped_0)); }
	inline bool get_stopped_0() const { return ___stopped_0; }
	inline bool* get_address_of_stopped_0() { return &___stopped_0; }
	inline void set_stopped_0(bool value)
	{
		___stopped_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLUSHINGOPERATION_TB0B9CC8E029067E8D130896A68A0E2D390E01027_H
#ifndef LOOPACTIONENUMERATOR_T54A9470950C20832A1CDA56CC03B15C6DC61BCA0_H
#define LOOPACTIONENUMERATOR_T54A9470950C20832A1CDA56CC03B15C6DC61BCA0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Svelto.Tasks.LoopActionEnumerator
struct  LoopActionEnumerator_t54A9470950C20832A1CDA56CC03B15C6DC61BCA0  : public RuntimeObject
{
public:
	// System.Action Svelto.Tasks.LoopActionEnumerator::_action
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ____action_0;

public:
	inline static int32_t get_offset_of__action_0() { return static_cast<int32_t>(offsetof(LoopActionEnumerator_t54A9470950C20832A1CDA56CC03B15C6DC61BCA0, ____action_0)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get__action_0() const { return ____action_0; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of__action_0() { return &____action_0; }
	inline void set__action_0(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		____action_0 = value;
		Il2CppCodeGenWriteBarrier((&____action_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOOPACTIONENUMERATOR_T54A9470950C20832A1CDA56CC03B15C6DC61BCA0_H
#ifndef MONORUNNER_T65496222247A6A3CAFE8294914A4776DE8693784_H
#define MONORUNNER_T65496222247A6A3CAFE8294914A4776DE8693784_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Svelto.Tasks.MonoRunner
struct  MonoRunner_t65496222247A6A3CAFE8294914A4776DE8693784  : public RuntimeObject
{
public:
	// System.Boolean Svelto.Tasks.MonoRunner::<paused>k__BackingField
	bool ___U3CpausedU3Ek__BackingField_0;
	// UnityEngine.GameObject Svelto.Tasks.MonoRunner::_go
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____go_1;

public:
	inline static int32_t get_offset_of_U3CpausedU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MonoRunner_t65496222247A6A3CAFE8294914A4776DE8693784, ___U3CpausedU3Ek__BackingField_0)); }
	inline bool get_U3CpausedU3Ek__BackingField_0() const { return ___U3CpausedU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CpausedU3Ek__BackingField_0() { return &___U3CpausedU3Ek__BackingField_0; }
	inline void set_U3CpausedU3Ek__BackingField_0(bool value)
	{
		___U3CpausedU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of__go_1() { return static_cast<int32_t>(offsetof(MonoRunner_t65496222247A6A3CAFE8294914A4776DE8693784, ____go_1)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__go_1() const { return ____go_1; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__go_1() { return &____go_1; }
	inline void set__go_1(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____go_1 = value;
		Il2CppCodeGenWriteBarrier((&____go_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONORUNNER_T65496222247A6A3CAFE8294914A4776DE8693784_H
#ifndef U3CU3EC__DISPLAYCLASS10_0_T95EFA810ABCD70BA5CB25CD61CD6A068E8F5F9DB_H
#define U3CU3EC__DISPLAYCLASS10_0_T95EFA810ABCD70BA5CB25CD61CD6A068E8F5F9DB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Svelto.Tasks.MultiThreadRunner_<>c__DisplayClass10_0
struct  U3CU3Ec__DisplayClass10_0_t95EFA810ABCD70BA5CB25CD61CD6A068E8F5F9DB  : public RuntimeObject
{
public:
	// Svelto.Tasks.MultiThreadRunner Svelto.Tasks.MultiThreadRunner_<>c__DisplayClass10_0::<>4__this
	MultiThreadRunner_tC4A17C81CFC215E6A3719AADB82ACFB718255F92 * ___U3CU3E4__this_0;
	// System.String Svelto.Tasks.MultiThreadRunner_<>c__DisplayClass10_0::name
	String_t* ___name_1;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass10_0_t95EFA810ABCD70BA5CB25CD61CD6A068E8F5F9DB, ___U3CU3E4__this_0)); }
	inline MultiThreadRunner_tC4A17C81CFC215E6A3719AADB82ACFB718255F92 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline MultiThreadRunner_tC4A17C81CFC215E6A3719AADB82ACFB718255F92 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(MultiThreadRunner_tC4A17C81CFC215E6A3719AADB82ACFB718255F92 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass10_0_t95EFA810ABCD70BA5CB25CD61CD6A068E8F5F9DB, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS10_0_T95EFA810ABCD70BA5CB25CD61CD6A068E8F5F9DB_H
#ifndef PARALLELTASK_T1552F13FB2B55A71306310C98077D837B194A1B4_H
#define PARALLELTASK_T1552F13FB2B55A71306310C98077D837B194A1B4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Svelto.Tasks.ParallelTaskCollection_ParallelTask
struct  ParallelTask_t1552F13FB2B55A71306310C98077D837B194A1B4  : public RuntimeObject
{
public:
	// Svelto.Tasks.ParallelTaskCollection Svelto.Tasks.ParallelTaskCollection_ParallelTask::_parent
	ParallelTaskCollection_t8DFC5A6DAFD6AFB617F77CF0767758AF060049DA * ____parent_0;

public:
	inline static int32_t get_offset_of__parent_0() { return static_cast<int32_t>(offsetof(ParallelTask_t1552F13FB2B55A71306310C98077D837B194A1B4, ____parent_0)); }
	inline ParallelTaskCollection_t8DFC5A6DAFD6AFB617F77CF0767758AF060049DA * get__parent_0() const { return ____parent_0; }
	inline ParallelTaskCollection_t8DFC5A6DAFD6AFB617F77CF0767758AF060049DA ** get_address_of__parent_0() { return &____parent_0; }
	inline void set__parent_0(ParallelTaskCollection_t8DFC5A6DAFD6AFB617F77CF0767758AF060049DA * value)
	{
		____parent_0 = value;
		Il2CppCodeGenWriteBarrier((&____parent_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARALLELTASK_T1552F13FB2B55A71306310C98077D837B194A1B4_H
#ifndef TASKPROFILER_T2C3463D07E47F9E0399CDBBC5FBD606FEE890759_H
#define TASKPROFILER_T2C3463D07E47F9E0399CDBBC5FBD606FEE890759_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Svelto.Tasks.Profiler.TaskProfiler
struct  TaskProfiler_t2C3463D07E47F9E0399CDBBC5FBD606FEE890759  : public RuntimeObject
{
public:
	// System.Diagnostics.Stopwatch Svelto.Tasks.Profiler.TaskProfiler::_stopwatch
	Stopwatch_t0778B5C8DF8FE1D87FC57A2411DA695850BD64D4 * ____stopwatch_0;

public:
	inline static int32_t get_offset_of__stopwatch_0() { return static_cast<int32_t>(offsetof(TaskProfiler_t2C3463D07E47F9E0399CDBBC5FBD606FEE890759, ____stopwatch_0)); }
	inline Stopwatch_t0778B5C8DF8FE1D87FC57A2411DA695850BD64D4 * get__stopwatch_0() const { return ____stopwatch_0; }
	inline Stopwatch_t0778B5C8DF8FE1D87FC57A2411DA695850BD64D4 ** get_address_of__stopwatch_0() { return &____stopwatch_0; }
	inline void set__stopwatch_0(Stopwatch_t0778B5C8DF8FE1D87FC57A2411DA695850BD64D4 * value)
	{
		____stopwatch_0 = value;
		Il2CppCodeGenWriteBarrier((&____stopwatch_0), value);
	}
};

struct TaskProfiler_t2C3463D07E47F9E0399CDBBC5FBD606FEE890759_StaticFields
{
public:
	// System.Object Svelto.Tasks.Profiler.TaskProfiler::LOCK_OBJECT
	RuntimeObject * ___LOCK_OBJECT_1;
	// Svelto.DataStructures.ThreadSafeDictionary`2<System.String,Svelto.Tasks.Profiler.TaskInfo> Svelto.Tasks.Profiler.TaskProfiler::taskInfos
	ThreadSafeDictionary_2_t0EA3C0E086E61CDEFFED886B494936B21F921C5C * ___taskInfos_2;

public:
	inline static int32_t get_offset_of_LOCK_OBJECT_1() { return static_cast<int32_t>(offsetof(TaskProfiler_t2C3463D07E47F9E0399CDBBC5FBD606FEE890759_StaticFields, ___LOCK_OBJECT_1)); }
	inline RuntimeObject * get_LOCK_OBJECT_1() const { return ___LOCK_OBJECT_1; }
	inline RuntimeObject ** get_address_of_LOCK_OBJECT_1() { return &___LOCK_OBJECT_1; }
	inline void set_LOCK_OBJECT_1(RuntimeObject * value)
	{
		___LOCK_OBJECT_1 = value;
		Il2CppCodeGenWriteBarrier((&___LOCK_OBJECT_1), value);
	}

	inline static int32_t get_offset_of_taskInfos_2() { return static_cast<int32_t>(offsetof(TaskProfiler_t2C3463D07E47F9E0399CDBBC5FBD606FEE890759_StaticFields, ___taskInfos_2)); }
	inline ThreadSafeDictionary_2_t0EA3C0E086E61CDEFFED886B494936B21F921C5C * get_taskInfos_2() const { return ___taskInfos_2; }
	inline ThreadSafeDictionary_2_t0EA3C0E086E61CDEFFED886B494936B21F921C5C ** get_address_of_taskInfos_2() { return &___taskInfos_2; }
	inline void set_taskInfos_2(ThreadSafeDictionary_2_t0EA3C0E086E61CDEFFED886B494936B21F921C5C * value)
	{
		___taskInfos_2 = value;
		Il2CppCodeGenWriteBarrier((&___taskInfos_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TASKPROFILER_T2C3463D07E47F9E0399CDBBC5FBD606FEE890759_H
#ifndef STANDARDSCHEDULERS_TE4C2F7648F973E3B6B62A5695FE96A851F546E8B_H
#define STANDARDSCHEDULERS_TE4C2F7648F973E3B6B62A5695FE96A851F546E8B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Svelto.Tasks.StandardSchedulers
struct  StandardSchedulers_tE4C2F7648F973E3B6B62A5695FE96A851F546E8B  : public RuntimeObject
{
public:

public:
};

struct StandardSchedulers_tE4C2F7648F973E3B6B62A5695FE96A851F546E8B_StaticFields
{
public:
	// Svelto.Tasks.MultiThreadRunner Svelto.Tasks.StandardSchedulers::_multiThreadScheduler
	MultiThreadRunner_tC4A17C81CFC215E6A3719AADB82ACFB718255F92 * ____multiThreadScheduler_0;
	// Svelto.Tasks.CoroutineMonoRunner Svelto.Tasks.StandardSchedulers::_coroutineScheduler
	CoroutineMonoRunner_tE7E5367413EE32878D5A3D8EC78406F687A475C5 * ____coroutineScheduler_1;
	// Svelto.Tasks.PhysicMonoRunner Svelto.Tasks.StandardSchedulers::_physicScheduler
	PhysicMonoRunner_tF4F299E77C8B07EF8D39343FF568FAE80CF909F6 * ____physicScheduler_2;
	// Svelto.Tasks.LateMonoRunner Svelto.Tasks.StandardSchedulers::_lateScheduler
	LateMonoRunner_t78843F08125CAA775F86434EFC83B5828B6D69AA * ____lateScheduler_3;
	// Svelto.Tasks.UpdateMonoRunner Svelto.Tasks.StandardSchedulers::_updateScheduler
	UpdateMonoRunner_tD70489504A43A0A4ADA16547CB1203C1FDC1B54C * ____updateScheduler_4;

public:
	inline static int32_t get_offset_of__multiThreadScheduler_0() { return static_cast<int32_t>(offsetof(StandardSchedulers_tE4C2F7648F973E3B6B62A5695FE96A851F546E8B_StaticFields, ____multiThreadScheduler_0)); }
	inline MultiThreadRunner_tC4A17C81CFC215E6A3719AADB82ACFB718255F92 * get__multiThreadScheduler_0() const { return ____multiThreadScheduler_0; }
	inline MultiThreadRunner_tC4A17C81CFC215E6A3719AADB82ACFB718255F92 ** get_address_of__multiThreadScheduler_0() { return &____multiThreadScheduler_0; }
	inline void set__multiThreadScheduler_0(MultiThreadRunner_tC4A17C81CFC215E6A3719AADB82ACFB718255F92 * value)
	{
		____multiThreadScheduler_0 = value;
		Il2CppCodeGenWriteBarrier((&____multiThreadScheduler_0), value);
	}

	inline static int32_t get_offset_of__coroutineScheduler_1() { return static_cast<int32_t>(offsetof(StandardSchedulers_tE4C2F7648F973E3B6B62A5695FE96A851F546E8B_StaticFields, ____coroutineScheduler_1)); }
	inline CoroutineMonoRunner_tE7E5367413EE32878D5A3D8EC78406F687A475C5 * get__coroutineScheduler_1() const { return ____coroutineScheduler_1; }
	inline CoroutineMonoRunner_tE7E5367413EE32878D5A3D8EC78406F687A475C5 ** get_address_of__coroutineScheduler_1() { return &____coroutineScheduler_1; }
	inline void set__coroutineScheduler_1(CoroutineMonoRunner_tE7E5367413EE32878D5A3D8EC78406F687A475C5 * value)
	{
		____coroutineScheduler_1 = value;
		Il2CppCodeGenWriteBarrier((&____coroutineScheduler_1), value);
	}

	inline static int32_t get_offset_of__physicScheduler_2() { return static_cast<int32_t>(offsetof(StandardSchedulers_tE4C2F7648F973E3B6B62A5695FE96A851F546E8B_StaticFields, ____physicScheduler_2)); }
	inline PhysicMonoRunner_tF4F299E77C8B07EF8D39343FF568FAE80CF909F6 * get__physicScheduler_2() const { return ____physicScheduler_2; }
	inline PhysicMonoRunner_tF4F299E77C8B07EF8D39343FF568FAE80CF909F6 ** get_address_of__physicScheduler_2() { return &____physicScheduler_2; }
	inline void set__physicScheduler_2(PhysicMonoRunner_tF4F299E77C8B07EF8D39343FF568FAE80CF909F6 * value)
	{
		____physicScheduler_2 = value;
		Il2CppCodeGenWriteBarrier((&____physicScheduler_2), value);
	}

	inline static int32_t get_offset_of__lateScheduler_3() { return static_cast<int32_t>(offsetof(StandardSchedulers_tE4C2F7648F973E3B6B62A5695FE96A851F546E8B_StaticFields, ____lateScheduler_3)); }
	inline LateMonoRunner_t78843F08125CAA775F86434EFC83B5828B6D69AA * get__lateScheduler_3() const { return ____lateScheduler_3; }
	inline LateMonoRunner_t78843F08125CAA775F86434EFC83B5828B6D69AA ** get_address_of__lateScheduler_3() { return &____lateScheduler_3; }
	inline void set__lateScheduler_3(LateMonoRunner_t78843F08125CAA775F86434EFC83B5828B6D69AA * value)
	{
		____lateScheduler_3 = value;
		Il2CppCodeGenWriteBarrier((&____lateScheduler_3), value);
	}

	inline static int32_t get_offset_of__updateScheduler_4() { return static_cast<int32_t>(offsetof(StandardSchedulers_tE4C2F7648F973E3B6B62A5695FE96A851F546E8B_StaticFields, ____updateScheduler_4)); }
	inline UpdateMonoRunner_tD70489504A43A0A4ADA16547CB1203C1FDC1B54C * get__updateScheduler_4() const { return ____updateScheduler_4; }
	inline UpdateMonoRunner_tD70489504A43A0A4ADA16547CB1203C1FDC1B54C ** get_address_of__updateScheduler_4() { return &____updateScheduler_4; }
	inline void set__updateScheduler_4(UpdateMonoRunner_tD70489504A43A0A4ADA16547CB1203C1FDC1B54C * value)
	{
		____updateScheduler_4 = value;
		Il2CppCodeGenWriteBarrier((&____updateScheduler_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STANDARDSCHEDULERS_TE4C2F7648F973E3B6B62A5695FE96A851F546E8B_H
#ifndef SYNCRUNNER_T0F4700CF530FD98101CFCD566EBAAE1E8E4EE8D2_H
#define SYNCRUNNER_T0F4700CF530FD98101CFCD566EBAAE1E8E4EE8D2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Svelto.Tasks.SyncRunner
struct  SyncRunner_t0F4700CF530FD98101CFCD566EBAAE1E8E4EE8D2  : public RuntimeObject
{
public:
	// System.Boolean Svelto.Tasks.SyncRunner::<paused>k__BackingField
	bool ___U3CpausedU3Ek__BackingField_0;
	// System.Boolean Svelto.Tasks.SyncRunner::<isStopping>k__BackingField
	bool ___U3CisStoppingU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CpausedU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(SyncRunner_t0F4700CF530FD98101CFCD566EBAAE1E8E4EE8D2, ___U3CpausedU3Ek__BackingField_0)); }
	inline bool get_U3CpausedU3Ek__BackingField_0() const { return ___U3CpausedU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CpausedU3Ek__BackingField_0() { return &___U3CpausedU3Ek__BackingField_0; }
	inline void set_U3CpausedU3Ek__BackingField_0(bool value)
	{
		___U3CpausedU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CisStoppingU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(SyncRunner_t0F4700CF530FD98101CFCD566EBAAE1E8E4EE8D2, ___U3CisStoppingU3Ek__BackingField_1)); }
	inline bool get_U3CisStoppingU3Ek__BackingField_1() const { return ___U3CisStoppingU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CisStoppingU3Ek__BackingField_1() { return &___U3CisStoppingU3Ek__BackingField_1; }
	inline void set_U3CisStoppingU3Ek__BackingField_1(bool value)
	{
		___U3CisStoppingU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYNCRUNNER_T0F4700CF530FD98101CFCD566EBAAE1E8E4EE8D2_H
#ifndef TASKCOLLECTION_TD5BE67D01B98473132820546A081DDB58E47C931_H
#define TASKCOLLECTION_TD5BE67D01B98473132820546A081DDB58E47C931_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Svelto.Tasks.TaskCollection
struct  TaskCollection_tD5BE67D01B98473132820546A081DDB58E47C931  : public RuntimeObject
{
public:
	// System.Boolean Svelto.Tasks.TaskCollection::<isRunning>k__BackingField
	bool ___U3CisRunningU3Ek__BackingField_0;
	// Svelto.DataStructures.FasterList`1<System.Collections.Generic.Stack`1<System.Collections.IEnumerator>> Svelto.Tasks.TaskCollection::_listOfStacks
	FasterList_1_t886A5ED25FB67F8EC1D7DFF6CEA1170A4D44691D * ____listOfStacks_1;

public:
	inline static int32_t get_offset_of_U3CisRunningU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(TaskCollection_tD5BE67D01B98473132820546A081DDB58E47C931, ___U3CisRunningU3Ek__BackingField_0)); }
	inline bool get_U3CisRunningU3Ek__BackingField_0() const { return ___U3CisRunningU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CisRunningU3Ek__BackingField_0() { return &___U3CisRunningU3Ek__BackingField_0; }
	inline void set_U3CisRunningU3Ek__BackingField_0(bool value)
	{
		___U3CisRunningU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of__listOfStacks_1() { return static_cast<int32_t>(offsetof(TaskCollection_tD5BE67D01B98473132820546A081DDB58E47C931, ____listOfStacks_1)); }
	inline FasterList_1_t886A5ED25FB67F8EC1D7DFF6CEA1170A4D44691D * get__listOfStacks_1() const { return ____listOfStacks_1; }
	inline FasterList_1_t886A5ED25FB67F8EC1D7DFF6CEA1170A4D44691D ** get_address_of__listOfStacks_1() { return &____listOfStacks_1; }
	inline void set__listOfStacks_1(FasterList_1_t886A5ED25FB67F8EC1D7DFF6CEA1170A4D44691D * value)
	{
		____listOfStacks_1 = value;
		Il2CppCodeGenWriteBarrier((&____listOfStacks_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TASKCOLLECTION_TD5BE67D01B98473132820546A081DDB58E47C931_H
#ifndef TASKRUNNER_TD224F90700B75918458C78D09272B5CA50A80747_H
#define TASKRUNNER_TD224F90700B75918458C78D09272B5CA50A80747_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Svelto.Tasks.TaskRunner
struct  TaskRunner_tD224F90700B75918458C78D09272B5CA50A80747  : public RuntimeObject
{
public:
	// Svelto.Tasks.IRunner Svelto.Tasks.TaskRunner::_runner
	RuntimeObject* ____runner_1;
	// Svelto.Tasks.Internal.PausableTaskPool Svelto.Tasks.TaskRunner::_taskPool
	PausableTaskPool_t650A2D678FDFAA152B5CB537987A31A82A014D28 * ____taskPool_2;

public:
	inline static int32_t get_offset_of__runner_1() { return static_cast<int32_t>(offsetof(TaskRunner_tD224F90700B75918458C78D09272B5CA50A80747, ____runner_1)); }
	inline RuntimeObject* get__runner_1() const { return ____runner_1; }
	inline RuntimeObject** get_address_of__runner_1() { return &____runner_1; }
	inline void set__runner_1(RuntimeObject* value)
	{
		____runner_1 = value;
		Il2CppCodeGenWriteBarrier((&____runner_1), value);
	}

	inline static int32_t get_offset_of__taskPool_2() { return static_cast<int32_t>(offsetof(TaskRunner_tD224F90700B75918458C78D09272B5CA50A80747, ____taskPool_2)); }
	inline PausableTaskPool_t650A2D678FDFAA152B5CB537987A31A82A014D28 * get__taskPool_2() const { return ____taskPool_2; }
	inline PausableTaskPool_t650A2D678FDFAA152B5CB537987A31A82A014D28 ** get_address_of__taskPool_2() { return &____taskPool_2; }
	inline void set__taskPool_2(PausableTaskPool_t650A2D678FDFAA152B5CB537987A31A82A014D28 * value)
	{
		____taskPool_2 = value;
		Il2CppCodeGenWriteBarrier((&____taskPool_2), value);
	}
};

struct TaskRunner_tD224F90700B75918458C78D09272B5CA50A80747_StaticFields
{
public:
	// Svelto.Tasks.TaskRunner Svelto.Tasks.TaskRunner::_instance
	TaskRunner_tD224F90700B75918458C78D09272B5CA50A80747 * ____instance_0;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(TaskRunner_tD224F90700B75918458C78D09272B5CA50A80747_StaticFields, ____instance_0)); }
	inline TaskRunner_tD224F90700B75918458C78D09272B5CA50A80747 * get__instance_0() const { return ____instance_0; }
	inline TaskRunner_tD224F90700B75918458C78D09272B5CA50A80747 ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(TaskRunner_tD224F90700B75918458C78D09272B5CA50A80747 * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier((&____instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TASKRUNNER_TD224F90700B75918458C78D09272B5CA50A80747_H
#ifndef TASKWRAPPER_T5FAAE4C120A656C29A4379FC352788AA30010467_H
#define TASKWRAPPER_T5FAAE4C120A656C29A4379FC352788AA30010467_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Svelto.Tasks.TaskWrapper
struct  TaskWrapper_t5FAAE4C120A656C29A4379FC352788AA30010467  : public RuntimeObject
{
public:
	// Svelto.Tasks.IAbstractTask Svelto.Tasks.TaskWrapper::<task>k__BackingField
	RuntimeObject* ___U3CtaskU3Ek__BackingField_0;
	// System.Boolean Svelto.Tasks.TaskWrapper::_started
	bool ____started_1;

public:
	inline static int32_t get_offset_of_U3CtaskU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(TaskWrapper_t5FAAE4C120A656C29A4379FC352788AA30010467, ___U3CtaskU3Ek__BackingField_0)); }
	inline RuntimeObject* get_U3CtaskU3Ek__BackingField_0() const { return ___U3CtaskU3Ek__BackingField_0; }
	inline RuntimeObject** get_address_of_U3CtaskU3Ek__BackingField_0() { return &___U3CtaskU3Ek__BackingField_0; }
	inline void set_U3CtaskU3Ek__BackingField_0(RuntimeObject* value)
	{
		___U3CtaskU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtaskU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of__started_1() { return static_cast<int32_t>(offsetof(TaskWrapper_t5FAAE4C120A656C29A4379FC352788AA30010467, ____started_1)); }
	inline bool get__started_1() const { return ____started_1; }
	inline bool* get_address_of__started_1() { return &____started_1; }
	inline void set__started_1(bool value)
	{
		____started_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TASKWRAPPER_T5FAAE4C120A656C29A4379FC352788AA30010467_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef U3CU3EC_T27C553FE38FB7E37C2611A42F15FA22A8E408636_H
#define U3CU3EC_T27C553FE38FB7E37C2611A42F15FA22A8E408636_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayx.Graphy.GraphyDebugger_<>c
struct  U3CU3Ec_t27C553FE38FB7E37C2611A42F15FA22A8E408636  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t27C553FE38FB7E37C2611A42F15FA22A8E408636_StaticFields
{
public:
	// Tayx.Graphy.GraphyDebugger_<>c Tayx.Graphy.GraphyDebugger_<>c::<>9
	U3CU3Ec_t27C553FE38FB7E37C2611A42F15FA22A8E408636 * ___U3CU3E9_0;
	// System.Predicate`1<Tayx.Graphy.GraphyDebugger_DebugPacket> Tayx.Graphy.GraphyDebugger_<>c::<>9__24_0
	Predicate_1_tC4C6DB54C5F647220AFB7B208EACB0A2F3B13B81 * ___U3CU3E9__24_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t27C553FE38FB7E37C2611A42F15FA22A8E408636_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t27C553FE38FB7E37C2611A42F15FA22A8E408636 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t27C553FE38FB7E37C2611A42F15FA22A8E408636 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t27C553FE38FB7E37C2611A42F15FA22A8E408636 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__24_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t27C553FE38FB7E37C2611A42F15FA22A8E408636_StaticFields, ___U3CU3E9__24_0_1)); }
	inline Predicate_1_tC4C6DB54C5F647220AFB7B208EACB0A2F3B13B81 * get_U3CU3E9__24_0_1() const { return ___U3CU3E9__24_0_1; }
	inline Predicate_1_tC4C6DB54C5F647220AFB7B208EACB0A2F3B13B81 ** get_address_of_U3CU3E9__24_0_1() { return &___U3CU3E9__24_0_1; }
	inline void set_U3CU3E9__24_0_1(Predicate_1_tC4C6DB54C5F647220AFB7B208EACB0A2F3B13B81 * value)
	{
		___U3CU3E9__24_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__24_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T27C553FE38FB7E37C2611A42F15FA22A8E408636_H
#ifndef U3CU3EC__DISPLAYCLASS18_0_TC525A1655E9BA745C7370C785F2C8D3AB0AE5F00_H
#define U3CU3EC__DISPLAYCLASS18_0_TC525A1655E9BA745C7370C785F2C8D3AB0AE5F00_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayx.Graphy.GraphyDebugger_<>c__DisplayClass18_0
struct  U3CU3Ec__DisplayClass18_0_tC525A1655E9BA745C7370C785F2C8D3AB0AE5F00  : public RuntimeObject
{
public:
	// System.Int32 Tayx.Graphy.GraphyDebugger_<>c__DisplayClass18_0::packetId
	int32_t ___packetId_0;

public:
	inline static int32_t get_offset_of_packetId_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass18_0_tC525A1655E9BA745C7370C785F2C8D3AB0AE5F00, ___packetId_0)); }
	inline int32_t get_packetId_0() const { return ___packetId_0; }
	inline int32_t* get_address_of_packetId_0() { return &___packetId_0; }
	inline void set_packetId_0(int32_t value)
	{
		___packetId_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS18_0_TC525A1655E9BA745C7370C785F2C8D3AB0AE5F00_H
#ifndef U3CU3EC__DISPLAYCLASS19_0_T5E47DBDDAF14E12B236F95335BF75EB7471BA900_H
#define U3CU3EC__DISPLAYCLASS19_0_T5E47DBDDAF14E12B236F95335BF75EB7471BA900_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayx.Graphy.GraphyDebugger_<>c__DisplayClass19_0
struct  U3CU3Ec__DisplayClass19_0_t5E47DBDDAF14E12B236F95335BF75EB7471BA900  : public RuntimeObject
{
public:
	// System.Int32 Tayx.Graphy.GraphyDebugger_<>c__DisplayClass19_0::packetId
	int32_t ___packetId_0;

public:
	inline static int32_t get_offset_of_packetId_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass19_0_t5E47DBDDAF14E12B236F95335BF75EB7471BA900, ___packetId_0)); }
	inline int32_t get_packetId_0() const { return ___packetId_0; }
	inline int32_t* get_address_of_packetId_0() { return &___packetId_0; }
	inline void set_packetId_0(int32_t value)
	{
		___packetId_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS19_0_T5E47DBDDAF14E12B236F95335BF75EB7471BA900_H
#ifndef U3CU3EC__DISPLAYCLASS21_0_T41B4ACE0CF1F630C5EDBC7AC123DC7F1B5726534_H
#define U3CU3EC__DISPLAYCLASS21_0_T41B4ACE0CF1F630C5EDBC7AC123DC7F1B5726534_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayx.Graphy.GraphyDebugger_<>c__DisplayClass21_0
struct  U3CU3Ec__DisplayClass21_0_t41B4ACE0CF1F630C5EDBC7AC123DC7F1B5726534  : public RuntimeObject
{
public:
	// System.Int32 Tayx.Graphy.GraphyDebugger_<>c__DisplayClass21_0::packetId
	int32_t ___packetId_0;

public:
	inline static int32_t get_offset_of_packetId_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass21_0_t41B4ACE0CF1F630C5EDBC7AC123DC7F1B5726534, ___packetId_0)); }
	inline int32_t get_packetId_0() const { return ___packetId_0; }
	inline int32_t* get_address_of_packetId_0() { return &___packetId_0; }
	inline void set_packetId_0(int32_t value)
	{
		___packetId_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS21_0_T41B4ACE0CF1F630C5EDBC7AC123DC7F1B5726534_H
#ifndef G_EXTENSIONMETHODS_TF66F9E35495E364A880753E446D08308CFE3E276_H
#define G_EXTENSIONMETHODS_TF66F9E35495E364A880753E446D08308CFE3E276_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayx.Graphy.Utils.G_ExtensionMethods
struct  G_ExtensionMethods_tF66F9E35495E364A880753E446D08308CFE3E276  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // G_EXTENSIONMETHODS_TF66F9E35495E364A880753E446D08308CFE3E276_H
#ifndef G_FLOATSTRING_T6D89C4A8C15ABBC570E80DEC140A4EA53EB955D6_H
#define G_FLOATSTRING_T6D89C4A8C15ABBC570E80DEC140A4EA53EB955D6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayx.Graphy.Utils.NumString.G_FloatString
struct  G_FloatString_t6D89C4A8C15ABBC570E80DEC140A4EA53EB955D6  : public RuntimeObject
{
public:

public:
};

struct G_FloatString_t6D89C4A8C15ABBC570E80DEC140A4EA53EB955D6_StaticFields
{
public:
	// System.Single Tayx.Graphy.Utils.NumString.G_FloatString::decimalMultiplier
	float ___decimalMultiplier_1;
	// System.String[] Tayx.Graphy.Utils.NumString.G_FloatString::negativeBuffer
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___negativeBuffer_2;
	// System.String[] Tayx.Graphy.Utils.NumString.G_FloatString::positiveBuffer
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___positiveBuffer_3;

public:
	inline static int32_t get_offset_of_decimalMultiplier_1() { return static_cast<int32_t>(offsetof(G_FloatString_t6D89C4A8C15ABBC570E80DEC140A4EA53EB955D6_StaticFields, ___decimalMultiplier_1)); }
	inline float get_decimalMultiplier_1() const { return ___decimalMultiplier_1; }
	inline float* get_address_of_decimalMultiplier_1() { return &___decimalMultiplier_1; }
	inline void set_decimalMultiplier_1(float value)
	{
		___decimalMultiplier_1 = value;
	}

	inline static int32_t get_offset_of_negativeBuffer_2() { return static_cast<int32_t>(offsetof(G_FloatString_t6D89C4A8C15ABBC570E80DEC140A4EA53EB955D6_StaticFields, ___negativeBuffer_2)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_negativeBuffer_2() const { return ___negativeBuffer_2; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_negativeBuffer_2() { return &___negativeBuffer_2; }
	inline void set_negativeBuffer_2(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___negativeBuffer_2 = value;
		Il2CppCodeGenWriteBarrier((&___negativeBuffer_2), value);
	}

	inline static int32_t get_offset_of_positiveBuffer_3() { return static_cast<int32_t>(offsetof(G_FloatString_t6D89C4A8C15ABBC570E80DEC140A4EA53EB955D6_StaticFields, ___positiveBuffer_3)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_positiveBuffer_3() const { return ___positiveBuffer_3; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_positiveBuffer_3() { return &___positiveBuffer_3; }
	inline void set_positiveBuffer_3(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___positiveBuffer_3 = value;
		Il2CppCodeGenWriteBarrier((&___positiveBuffer_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // G_FLOATSTRING_T6D89C4A8C15ABBC570E80DEC140A4EA53EB955D6_H
#ifndef G_INTSTRING_TD13FE91428976CAA79DA24721DD0FA959CABDE64_H
#define G_INTSTRING_TD13FE91428976CAA79DA24721DD0FA959CABDE64_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayx.Graphy.Utils.NumString.G_IntString
struct  G_IntString_tD13FE91428976CAA79DA24721DD0FA959CABDE64  : public RuntimeObject
{
public:

public:
};

struct G_IntString_tD13FE91428976CAA79DA24721DD0FA959CABDE64_StaticFields
{
public:
	// System.String[] Tayx.Graphy.Utils.NumString.G_IntString::negativeBuffer
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___negativeBuffer_0;
	// System.String[] Tayx.Graphy.Utils.NumString.G_IntString::positiveBuffer
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___positiveBuffer_1;

public:
	inline static int32_t get_offset_of_negativeBuffer_0() { return static_cast<int32_t>(offsetof(G_IntString_tD13FE91428976CAA79DA24721DD0FA959CABDE64_StaticFields, ___negativeBuffer_0)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_negativeBuffer_0() const { return ___negativeBuffer_0; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_negativeBuffer_0() { return &___negativeBuffer_0; }
	inline void set_negativeBuffer_0(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___negativeBuffer_0 = value;
		Il2CppCodeGenWriteBarrier((&___negativeBuffer_0), value);
	}

	inline static int32_t get_offset_of_positiveBuffer_1() { return static_cast<int32_t>(offsetof(G_IntString_tD13FE91428976CAA79DA24721DD0FA959CABDE64_StaticFields, ___positiveBuffer_1)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_positiveBuffer_1() const { return ___positiveBuffer_1; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_positiveBuffer_1() { return &___positiveBuffer_1; }
	inline void set_positiveBuffer_1(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___positiveBuffer_1 = value;
		Il2CppCodeGenWriteBarrier((&___positiveBuffer_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // G_INTSTRING_TD13FE91428976CAA79DA24721DD0FA959CABDE64_H
#ifndef U3CU3EC_T95995C3290CB491D3FF50C75A935A381987078B4_H
#define U3CU3EC_T95995C3290CB491D3FF50C75A935A381987078B4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utility.Console_<>c
struct  U3CU3Ec_t95995C3290CB491D3FF50C75A935A381987078B4  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t95995C3290CB491D3FF50C75A935A381987078B4_StaticFields
{
public:
	// Utility.Console_<>c Utility.Console_<>c::<>9
	U3CU3Ec_t95995C3290CB491D3FF50C75A935A381987078B4 * ___U3CU3E9_0;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t95995C3290CB491D3FF50C75A935A381987078B4_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t95995C3290CB491D3FF50C75A935A381987078B4 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t95995C3290CB491D3FF50C75A935A381987078B4 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t95995C3290CB491D3FF50C75A935A381987078B4 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T95995C3290CB491D3FF50C75A935A381987078B4_H
#ifndef SIMPLELOGGER_TCF6CBA7AF8F03D6AAC6225F8E1DD9E19EBF7B3F0_H
#define SIMPLELOGGER_TCF6CBA7AF8F03D6AAC6225F8E1DD9E19EBF7B3F0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utility.SimpleLogger
struct  SimpleLogger_tCF6CBA7AF8F03D6AAC6225F8E1DD9E19EBF7B3F0  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLELOGGER_TCF6CBA7AF8F03D6AAC6225F8E1DD9E19EBF7B3F0_H
#ifndef SLOWLOGGERUNITY_TD909793CED39946C2553FB0A295F9296342401EB_H
#define SLOWLOGGERUNITY_TD909793CED39946C2553FB0A295F9296342401EB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utility.SlowLoggerUnity
struct  SlowLoggerUnity_tD909793CED39946C2553FB0A295F9296342401EB  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SLOWLOGGERUNITY_TD909793CED39946C2553FB0A295F9296342401EB_H
#ifndef DESIGNBYCONTRACTEXCEPTION_T8D28E8A87F2986C2D9F7C4FA9B4AB37C8B884A45_H
#define DESIGNBYCONTRACTEXCEPTION_T8D28E8A87F2986C2D9F7C4FA9B4AB37C8B884A45_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DBC.DesignByContractException
struct  DesignByContractException_t8D28E8A87F2986C2D9F7C4FA9B4AB37C8B884A45  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESIGNBYCONTRACTEXCEPTION_T8D28E8A87F2986C2D9F7C4FA9B4AB37C8B884A45_H
#ifndef COROUTINEMONORUNNER_TE7E5367413EE32878D5A3D8EC78406F687A475C5_H
#define COROUTINEMONORUNNER_TE7E5367413EE32878D5A3D8EC78406F687A475C5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Svelto.Tasks.CoroutineMonoRunner
struct  CoroutineMonoRunner_tE7E5367413EE32878D5A3D8EC78406F687A475C5  : public MonoRunner_t65496222247A6A3CAFE8294914A4776DE8693784
{
public:
	// Svelto.DataStructures.ThreadSafeQueue`1<Svelto.Tasks.IPausableTask> Svelto.Tasks.CoroutineMonoRunner::_newTaskRoutines
	ThreadSafeQueue_1_t4B42DDB151ED238066A071C89AF1FC3DB9C1BE45 * ____newTaskRoutines_2;
	// Svelto.Tasks.Internal.UnityCoroutineRunner_FlushingOperation Svelto.Tasks.CoroutineMonoRunner::_flushingOperation
	FlushingOperation_tB0B9CC8E029067E8D130896A68A0E2D390E01027 * ____flushingOperation_3;
	// Svelto.Tasks.Internal.UnityCoroutineRunner_RunningTasksInfo Svelto.Tasks.CoroutineMonoRunner::_info
	RunningTasksInfo_t10813DB30B3A26BF8CF0830C17B2B3EE5DBF523C * ____info_4;

public:
	inline static int32_t get_offset_of__newTaskRoutines_2() { return static_cast<int32_t>(offsetof(CoroutineMonoRunner_tE7E5367413EE32878D5A3D8EC78406F687A475C5, ____newTaskRoutines_2)); }
	inline ThreadSafeQueue_1_t4B42DDB151ED238066A071C89AF1FC3DB9C1BE45 * get__newTaskRoutines_2() const { return ____newTaskRoutines_2; }
	inline ThreadSafeQueue_1_t4B42DDB151ED238066A071C89AF1FC3DB9C1BE45 ** get_address_of__newTaskRoutines_2() { return &____newTaskRoutines_2; }
	inline void set__newTaskRoutines_2(ThreadSafeQueue_1_t4B42DDB151ED238066A071C89AF1FC3DB9C1BE45 * value)
	{
		____newTaskRoutines_2 = value;
		Il2CppCodeGenWriteBarrier((&____newTaskRoutines_2), value);
	}

	inline static int32_t get_offset_of__flushingOperation_3() { return static_cast<int32_t>(offsetof(CoroutineMonoRunner_tE7E5367413EE32878D5A3D8EC78406F687A475C5, ____flushingOperation_3)); }
	inline FlushingOperation_tB0B9CC8E029067E8D130896A68A0E2D390E01027 * get__flushingOperation_3() const { return ____flushingOperation_3; }
	inline FlushingOperation_tB0B9CC8E029067E8D130896A68A0E2D390E01027 ** get_address_of__flushingOperation_3() { return &____flushingOperation_3; }
	inline void set__flushingOperation_3(FlushingOperation_tB0B9CC8E029067E8D130896A68A0E2D390E01027 * value)
	{
		____flushingOperation_3 = value;
		Il2CppCodeGenWriteBarrier((&____flushingOperation_3), value);
	}

	inline static int32_t get_offset_of__info_4() { return static_cast<int32_t>(offsetof(CoroutineMonoRunner_tE7E5367413EE32878D5A3D8EC78406F687A475C5, ____info_4)); }
	inline RunningTasksInfo_t10813DB30B3A26BF8CF0830C17B2B3EE5DBF523C * get__info_4() const { return ____info_4; }
	inline RunningTasksInfo_t10813DB30B3A26BF8CF0830C17B2B3EE5DBF523C ** get_address_of__info_4() { return &____info_4; }
	inline void set__info_4(RunningTasksInfo_t10813DB30B3A26BF8CF0830C17B2B3EE5DBF523C * value)
	{
		____info_4 = value;
		Il2CppCodeGenWriteBarrier((&____info_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COROUTINEMONORUNNER_TE7E5367413EE32878D5A3D8EC78406F687A475C5_H
#ifndef ENDOFFRAMERUNNER_T9F78CD19F94BE7C130860CA0A4A912A0F6224FBB_H
#define ENDOFFRAMERUNNER_T9F78CD19F94BE7C130860CA0A4A912A0F6224FBB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Svelto.Tasks.EndOfFrameRunner
struct  EndOfFrameRunner_t9F78CD19F94BE7C130860CA0A4A912A0F6224FBB  : public MonoRunner_t65496222247A6A3CAFE8294914A4776DE8693784
{
public:
	// Svelto.DataStructures.ThreadSafeQueue`1<Svelto.Tasks.IPausableTask> Svelto.Tasks.EndOfFrameRunner::_newTaskRoutines
	ThreadSafeQueue_1_t4B42DDB151ED238066A071C89AF1FC3DB9C1BE45 * ____newTaskRoutines_2;
	// Svelto.Tasks.Internal.UnityCoroutineRunner_FlushingOperation Svelto.Tasks.EndOfFrameRunner::_flushingOperation
	FlushingOperation_tB0B9CC8E029067E8D130896A68A0E2D390E01027 * ____flushingOperation_3;
	// Svelto.Tasks.Internal.UnityCoroutineRunner_RunningTasksInfo Svelto.Tasks.EndOfFrameRunner::_info
	RunningTasksInfo_t10813DB30B3A26BF8CF0830C17B2B3EE5DBF523C * ____info_4;

public:
	inline static int32_t get_offset_of__newTaskRoutines_2() { return static_cast<int32_t>(offsetof(EndOfFrameRunner_t9F78CD19F94BE7C130860CA0A4A912A0F6224FBB, ____newTaskRoutines_2)); }
	inline ThreadSafeQueue_1_t4B42DDB151ED238066A071C89AF1FC3DB9C1BE45 * get__newTaskRoutines_2() const { return ____newTaskRoutines_2; }
	inline ThreadSafeQueue_1_t4B42DDB151ED238066A071C89AF1FC3DB9C1BE45 ** get_address_of__newTaskRoutines_2() { return &____newTaskRoutines_2; }
	inline void set__newTaskRoutines_2(ThreadSafeQueue_1_t4B42DDB151ED238066A071C89AF1FC3DB9C1BE45 * value)
	{
		____newTaskRoutines_2 = value;
		Il2CppCodeGenWriteBarrier((&____newTaskRoutines_2), value);
	}

	inline static int32_t get_offset_of__flushingOperation_3() { return static_cast<int32_t>(offsetof(EndOfFrameRunner_t9F78CD19F94BE7C130860CA0A4A912A0F6224FBB, ____flushingOperation_3)); }
	inline FlushingOperation_tB0B9CC8E029067E8D130896A68A0E2D390E01027 * get__flushingOperation_3() const { return ____flushingOperation_3; }
	inline FlushingOperation_tB0B9CC8E029067E8D130896A68A0E2D390E01027 ** get_address_of__flushingOperation_3() { return &____flushingOperation_3; }
	inline void set__flushingOperation_3(FlushingOperation_tB0B9CC8E029067E8D130896A68A0E2D390E01027 * value)
	{
		____flushingOperation_3 = value;
		Il2CppCodeGenWriteBarrier((&____flushingOperation_3), value);
	}

	inline static int32_t get_offset_of__info_4() { return static_cast<int32_t>(offsetof(EndOfFrameRunner_t9F78CD19F94BE7C130860CA0A4A912A0F6224FBB, ____info_4)); }
	inline RunningTasksInfo_t10813DB30B3A26BF8CF0830C17B2B3EE5DBF523C * get__info_4() const { return ____info_4; }
	inline RunningTasksInfo_t10813DB30B3A26BF8CF0830C17B2B3EE5DBF523C ** get_address_of__info_4() { return &____info_4; }
	inline void set__info_4(RunningTasksInfo_t10813DB30B3A26BF8CF0830C17B2B3EE5DBF523C * value)
	{
		____info_4 = value;
		Il2CppCodeGenWriteBarrier((&____info_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENDOFFRAMERUNNER_T9F78CD19F94BE7C130860CA0A4A912A0F6224FBB_H
#ifndef LATEMONORUNNER_T78843F08125CAA775F86434EFC83B5828B6D69AA_H
#define LATEMONORUNNER_T78843F08125CAA775F86434EFC83B5828B6D69AA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Svelto.Tasks.LateMonoRunner
struct  LateMonoRunner_t78843F08125CAA775F86434EFC83B5828B6D69AA  : public MonoRunner_t65496222247A6A3CAFE8294914A4776DE8693784
{
public:
	// Svelto.DataStructures.ThreadSafeQueue`1<Svelto.Tasks.IPausableTask> Svelto.Tasks.LateMonoRunner::_newTaskRoutines
	ThreadSafeQueue_1_t4B42DDB151ED238066A071C89AF1FC3DB9C1BE45 * ____newTaskRoutines_2;
	// Svelto.Tasks.Internal.UnityCoroutineRunner_FlushingOperation Svelto.Tasks.LateMonoRunner::_flushingOperation
	FlushingOperation_tB0B9CC8E029067E8D130896A68A0E2D390E01027 * ____flushingOperation_3;
	// Svelto.Tasks.Internal.UnityCoroutineRunner_RunningTasksInfo Svelto.Tasks.LateMonoRunner::_info
	RunningTasksInfo_t10813DB30B3A26BF8CF0830C17B2B3EE5DBF523C * ____info_4;

public:
	inline static int32_t get_offset_of__newTaskRoutines_2() { return static_cast<int32_t>(offsetof(LateMonoRunner_t78843F08125CAA775F86434EFC83B5828B6D69AA, ____newTaskRoutines_2)); }
	inline ThreadSafeQueue_1_t4B42DDB151ED238066A071C89AF1FC3DB9C1BE45 * get__newTaskRoutines_2() const { return ____newTaskRoutines_2; }
	inline ThreadSafeQueue_1_t4B42DDB151ED238066A071C89AF1FC3DB9C1BE45 ** get_address_of__newTaskRoutines_2() { return &____newTaskRoutines_2; }
	inline void set__newTaskRoutines_2(ThreadSafeQueue_1_t4B42DDB151ED238066A071C89AF1FC3DB9C1BE45 * value)
	{
		____newTaskRoutines_2 = value;
		Il2CppCodeGenWriteBarrier((&____newTaskRoutines_2), value);
	}

	inline static int32_t get_offset_of__flushingOperation_3() { return static_cast<int32_t>(offsetof(LateMonoRunner_t78843F08125CAA775F86434EFC83B5828B6D69AA, ____flushingOperation_3)); }
	inline FlushingOperation_tB0B9CC8E029067E8D130896A68A0E2D390E01027 * get__flushingOperation_3() const { return ____flushingOperation_3; }
	inline FlushingOperation_tB0B9CC8E029067E8D130896A68A0E2D390E01027 ** get_address_of__flushingOperation_3() { return &____flushingOperation_3; }
	inline void set__flushingOperation_3(FlushingOperation_tB0B9CC8E029067E8D130896A68A0E2D390E01027 * value)
	{
		____flushingOperation_3 = value;
		Il2CppCodeGenWriteBarrier((&____flushingOperation_3), value);
	}

	inline static int32_t get_offset_of__info_4() { return static_cast<int32_t>(offsetof(LateMonoRunner_t78843F08125CAA775F86434EFC83B5828B6D69AA, ____info_4)); }
	inline RunningTasksInfo_t10813DB30B3A26BF8CF0830C17B2B3EE5DBF523C * get__info_4() const { return ____info_4; }
	inline RunningTasksInfo_t10813DB30B3A26BF8CF0830C17B2B3EE5DBF523C ** get_address_of__info_4() { return &____info_4; }
	inline void set__info_4(RunningTasksInfo_t10813DB30B3A26BF8CF0830C17B2B3EE5DBF523C * value)
	{
		____info_4 = value;
		Il2CppCodeGenWriteBarrier((&____info_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LATEMONORUNNER_T78843F08125CAA775F86434EFC83B5828B6D69AA_H
#ifndef MULTITHREADEDPARALLELTASKCOLLECTIONEXCEPTION_TFFE7D199C2FA3521FC9C3463E92FFB43C1CEAAFA_H
#define MULTITHREADEDPARALLELTASKCOLLECTIONEXCEPTION_TFFE7D199C2FA3521FC9C3463E92FFB43C1CEAAFA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Svelto.Tasks.MultiThreadedParallelTaskCollectionException
struct  MultiThreadedParallelTaskCollectionException_tFFE7D199C2FA3521FC9C3463E92FFB43C1CEAAFA  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTITHREADEDPARALLELTASKCOLLECTIONEXCEPTION_TFFE7D199C2FA3521FC9C3463E92FFB43C1CEAAFA_H
#ifndef PARALLELTASKCOLLECTION_T8DFC5A6DAFD6AFB617F77CF0767758AF060049DA_H
#define PARALLELTASKCOLLECTION_T8DFC5A6DAFD6AFB617F77CF0767758AF060049DA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Svelto.Tasks.ParallelTaskCollection
struct  ParallelTaskCollection_t8DFC5A6DAFD6AFB617F77CF0767758AF060049DA  : public TaskCollection_tD5BE67D01B98473132820546A081DDB58E47C931
{
public:
	// System.Action Svelto.Tasks.ParallelTaskCollection::onComplete
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___onComplete_4;
	// System.Func`2<System.Exception,System.Boolean> Svelto.Tasks.ParallelTaskCollection::onException
	Func_2_tC816C5FDED4A7372C449E7660CAB9F94E2AC12F8 * ___onException_5;
	// System.Object Svelto.Tasks.ParallelTaskCollection::_current
	RuntimeObject * ____current_6;
	// System.Int32 Svelto.Tasks.ParallelTaskCollection::_offset
	int32_t ____offset_7;
	// System.Object Svelto.Tasks.ParallelTaskCollection::_currentWrapper
	RuntimeObject * ____currentWrapper_8;
	// System.String Svelto.Tasks.ParallelTaskCollection::_name
	String_t* ____name_9;

public:
	inline static int32_t get_offset_of_onComplete_4() { return static_cast<int32_t>(offsetof(ParallelTaskCollection_t8DFC5A6DAFD6AFB617F77CF0767758AF060049DA, ___onComplete_4)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_onComplete_4() const { return ___onComplete_4; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_onComplete_4() { return &___onComplete_4; }
	inline void set_onComplete_4(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___onComplete_4 = value;
		Il2CppCodeGenWriteBarrier((&___onComplete_4), value);
	}

	inline static int32_t get_offset_of_onException_5() { return static_cast<int32_t>(offsetof(ParallelTaskCollection_t8DFC5A6DAFD6AFB617F77CF0767758AF060049DA, ___onException_5)); }
	inline Func_2_tC816C5FDED4A7372C449E7660CAB9F94E2AC12F8 * get_onException_5() const { return ___onException_5; }
	inline Func_2_tC816C5FDED4A7372C449E7660CAB9F94E2AC12F8 ** get_address_of_onException_5() { return &___onException_5; }
	inline void set_onException_5(Func_2_tC816C5FDED4A7372C449E7660CAB9F94E2AC12F8 * value)
	{
		___onException_5 = value;
		Il2CppCodeGenWriteBarrier((&___onException_5), value);
	}

	inline static int32_t get_offset_of__current_6() { return static_cast<int32_t>(offsetof(ParallelTaskCollection_t8DFC5A6DAFD6AFB617F77CF0767758AF060049DA, ____current_6)); }
	inline RuntimeObject * get__current_6() const { return ____current_6; }
	inline RuntimeObject ** get_address_of__current_6() { return &____current_6; }
	inline void set__current_6(RuntimeObject * value)
	{
		____current_6 = value;
		Il2CppCodeGenWriteBarrier((&____current_6), value);
	}

	inline static int32_t get_offset_of__offset_7() { return static_cast<int32_t>(offsetof(ParallelTaskCollection_t8DFC5A6DAFD6AFB617F77CF0767758AF060049DA, ____offset_7)); }
	inline int32_t get__offset_7() const { return ____offset_7; }
	inline int32_t* get_address_of__offset_7() { return &____offset_7; }
	inline void set__offset_7(int32_t value)
	{
		____offset_7 = value;
	}

	inline static int32_t get_offset_of__currentWrapper_8() { return static_cast<int32_t>(offsetof(ParallelTaskCollection_t8DFC5A6DAFD6AFB617F77CF0767758AF060049DA, ____currentWrapper_8)); }
	inline RuntimeObject * get__currentWrapper_8() const { return ____currentWrapper_8; }
	inline RuntimeObject ** get_address_of__currentWrapper_8() { return &____currentWrapper_8; }
	inline void set__currentWrapper_8(RuntimeObject * value)
	{
		____currentWrapper_8 = value;
		Il2CppCodeGenWriteBarrier((&____currentWrapper_8), value);
	}

	inline static int32_t get_offset_of__name_9() { return static_cast<int32_t>(offsetof(ParallelTaskCollection_t8DFC5A6DAFD6AFB617F77CF0767758AF060049DA, ____name_9)); }
	inline String_t* get__name_9() const { return ____name_9; }
	inline String_t** get_address_of__name_9() { return &____name_9; }
	inline void set__name_9(String_t* value)
	{
		____name_9 = value;
		Il2CppCodeGenWriteBarrier((&____name_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARALLELTASKCOLLECTION_T8DFC5A6DAFD6AFB617F77CF0767758AF060049DA_H
#ifndef PAUSABLETASKEXCEPTION_T2592A41B7346E95CB061E90F12CACEDEC35208B3_H
#define PAUSABLETASKEXCEPTION_T2592A41B7346E95CB061E90F12CACEDEC35208B3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Svelto.Tasks.PausableTaskException
struct  PausableTaskException_t2592A41B7346E95CB061E90F12CACEDEC35208B3  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAUSABLETASKEXCEPTION_T2592A41B7346E95CB061E90F12CACEDEC35208B3_H
#ifndef PHYSICMONORUNNER_TF4F299E77C8B07EF8D39343FF568FAE80CF909F6_H
#define PHYSICMONORUNNER_TF4F299E77C8B07EF8D39343FF568FAE80CF909F6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Svelto.Tasks.PhysicMonoRunner
struct  PhysicMonoRunner_tF4F299E77C8B07EF8D39343FF568FAE80CF909F6  : public MonoRunner_t65496222247A6A3CAFE8294914A4776DE8693784
{
public:
	// Svelto.DataStructures.ThreadSafeQueue`1<Svelto.Tasks.IPausableTask> Svelto.Tasks.PhysicMonoRunner::_newTaskRoutines
	ThreadSafeQueue_1_t4B42DDB151ED238066A071C89AF1FC3DB9C1BE45 * ____newTaskRoutines_2;
	// Svelto.Tasks.Internal.UnityCoroutineRunner_FlushingOperation Svelto.Tasks.PhysicMonoRunner::_flushingOperation
	FlushingOperation_tB0B9CC8E029067E8D130896A68A0E2D390E01027 * ____flushingOperation_3;
	// Svelto.Tasks.Internal.UnityCoroutineRunner_RunningTasksInfo Svelto.Tasks.PhysicMonoRunner::_info
	RunningTasksInfo_t10813DB30B3A26BF8CF0830C17B2B3EE5DBF523C * ____info_4;

public:
	inline static int32_t get_offset_of__newTaskRoutines_2() { return static_cast<int32_t>(offsetof(PhysicMonoRunner_tF4F299E77C8B07EF8D39343FF568FAE80CF909F6, ____newTaskRoutines_2)); }
	inline ThreadSafeQueue_1_t4B42DDB151ED238066A071C89AF1FC3DB9C1BE45 * get__newTaskRoutines_2() const { return ____newTaskRoutines_2; }
	inline ThreadSafeQueue_1_t4B42DDB151ED238066A071C89AF1FC3DB9C1BE45 ** get_address_of__newTaskRoutines_2() { return &____newTaskRoutines_2; }
	inline void set__newTaskRoutines_2(ThreadSafeQueue_1_t4B42DDB151ED238066A071C89AF1FC3DB9C1BE45 * value)
	{
		____newTaskRoutines_2 = value;
		Il2CppCodeGenWriteBarrier((&____newTaskRoutines_2), value);
	}

	inline static int32_t get_offset_of__flushingOperation_3() { return static_cast<int32_t>(offsetof(PhysicMonoRunner_tF4F299E77C8B07EF8D39343FF568FAE80CF909F6, ____flushingOperation_3)); }
	inline FlushingOperation_tB0B9CC8E029067E8D130896A68A0E2D390E01027 * get__flushingOperation_3() const { return ____flushingOperation_3; }
	inline FlushingOperation_tB0B9CC8E029067E8D130896A68A0E2D390E01027 ** get_address_of__flushingOperation_3() { return &____flushingOperation_3; }
	inline void set__flushingOperation_3(FlushingOperation_tB0B9CC8E029067E8D130896A68A0E2D390E01027 * value)
	{
		____flushingOperation_3 = value;
		Il2CppCodeGenWriteBarrier((&____flushingOperation_3), value);
	}

	inline static int32_t get_offset_of__info_4() { return static_cast<int32_t>(offsetof(PhysicMonoRunner_tF4F299E77C8B07EF8D39343FF568FAE80CF909F6, ____info_4)); }
	inline RunningTasksInfo_t10813DB30B3A26BF8CF0830C17B2B3EE5DBF523C * get__info_4() const { return ____info_4; }
	inline RunningTasksInfo_t10813DB30B3A26BF8CF0830C17B2B3EE5DBF523C ** get_address_of__info_4() { return &____info_4; }
	inline void set__info_4(RunningTasksInfo_t10813DB30B3A26BF8CF0830C17B2B3EE5DBF523C * value)
	{
		____info_4 = value;
		Il2CppCodeGenWriteBarrier((&____info_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHYSICMONORUNNER_TF4F299E77C8B07EF8D39343FF568FAE80CF909F6_H
#ifndef TASKINFO_TE81761C55B71E6B50D0D42AF7CA89B3E5C22B8E1_H
#define TASKINFO_TE81761C55B71E6B50D0D42AF7CA89B3E5C22B8E1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Svelto.Tasks.Profiler.TaskInfo
struct  TaskInfo_tE81761C55B71E6B50D0D42AF7CA89B3E5C22B8E1 
{
public:
	// System.Double Svelto.Tasks.Profiler.TaskInfo::_accumulatedUpdateDuration
	double ____accumulatedUpdateDuration_2;
	// System.Double Svelto.Tasks.Profiler.TaskInfo::_lastUpdateDuration
	double ____lastUpdateDuration_3;
	// System.Double Svelto.Tasks.Profiler.TaskInfo::_maxUpdateDuration
	double ____maxUpdateDuration_4;
	// System.Double Svelto.Tasks.Profiler.TaskInfo::_minUpdateDuration
	double ____minUpdateDuration_5;
	// System.String Svelto.Tasks.Profiler.TaskInfo::_taskName
	String_t* ____taskName_6;
	// System.String Svelto.Tasks.Profiler.TaskInfo::_threadInfo
	String_t* ____threadInfo_7;
	// System.Collections.Generic.Queue`1<System.Double> Svelto.Tasks.Profiler.TaskInfo::_updateFrameTimes
	Queue_1_t4EE8B6A18F07374C2B3FF98A98B5369D5542EF98 * ____updateFrameTimes_8;

public:
	inline static int32_t get_offset_of__accumulatedUpdateDuration_2() { return static_cast<int32_t>(offsetof(TaskInfo_tE81761C55B71E6B50D0D42AF7CA89B3E5C22B8E1, ____accumulatedUpdateDuration_2)); }
	inline double get__accumulatedUpdateDuration_2() const { return ____accumulatedUpdateDuration_2; }
	inline double* get_address_of__accumulatedUpdateDuration_2() { return &____accumulatedUpdateDuration_2; }
	inline void set__accumulatedUpdateDuration_2(double value)
	{
		____accumulatedUpdateDuration_2 = value;
	}

	inline static int32_t get_offset_of__lastUpdateDuration_3() { return static_cast<int32_t>(offsetof(TaskInfo_tE81761C55B71E6B50D0D42AF7CA89B3E5C22B8E1, ____lastUpdateDuration_3)); }
	inline double get__lastUpdateDuration_3() const { return ____lastUpdateDuration_3; }
	inline double* get_address_of__lastUpdateDuration_3() { return &____lastUpdateDuration_3; }
	inline void set__lastUpdateDuration_3(double value)
	{
		____lastUpdateDuration_3 = value;
	}

	inline static int32_t get_offset_of__maxUpdateDuration_4() { return static_cast<int32_t>(offsetof(TaskInfo_tE81761C55B71E6B50D0D42AF7CA89B3E5C22B8E1, ____maxUpdateDuration_4)); }
	inline double get__maxUpdateDuration_4() const { return ____maxUpdateDuration_4; }
	inline double* get_address_of__maxUpdateDuration_4() { return &____maxUpdateDuration_4; }
	inline void set__maxUpdateDuration_4(double value)
	{
		____maxUpdateDuration_4 = value;
	}

	inline static int32_t get_offset_of__minUpdateDuration_5() { return static_cast<int32_t>(offsetof(TaskInfo_tE81761C55B71E6B50D0D42AF7CA89B3E5C22B8E1, ____minUpdateDuration_5)); }
	inline double get__minUpdateDuration_5() const { return ____minUpdateDuration_5; }
	inline double* get_address_of__minUpdateDuration_5() { return &____minUpdateDuration_5; }
	inline void set__minUpdateDuration_5(double value)
	{
		____minUpdateDuration_5 = value;
	}

	inline static int32_t get_offset_of__taskName_6() { return static_cast<int32_t>(offsetof(TaskInfo_tE81761C55B71E6B50D0D42AF7CA89B3E5C22B8E1, ____taskName_6)); }
	inline String_t* get__taskName_6() const { return ____taskName_6; }
	inline String_t** get_address_of__taskName_6() { return &____taskName_6; }
	inline void set__taskName_6(String_t* value)
	{
		____taskName_6 = value;
		Il2CppCodeGenWriteBarrier((&____taskName_6), value);
	}

	inline static int32_t get_offset_of__threadInfo_7() { return static_cast<int32_t>(offsetof(TaskInfo_tE81761C55B71E6B50D0D42AF7CA89B3E5C22B8E1, ____threadInfo_7)); }
	inline String_t* get__threadInfo_7() const { return ____threadInfo_7; }
	inline String_t** get_address_of__threadInfo_7() { return &____threadInfo_7; }
	inline void set__threadInfo_7(String_t* value)
	{
		____threadInfo_7 = value;
		Il2CppCodeGenWriteBarrier((&____threadInfo_7), value);
	}

	inline static int32_t get_offset_of__updateFrameTimes_8() { return static_cast<int32_t>(offsetof(TaskInfo_tE81761C55B71E6B50D0D42AF7CA89B3E5C22B8E1, ____updateFrameTimes_8)); }
	inline Queue_1_t4EE8B6A18F07374C2B3FF98A98B5369D5542EF98 * get__updateFrameTimes_8() const { return ____updateFrameTimes_8; }
	inline Queue_1_t4EE8B6A18F07374C2B3FF98A98B5369D5542EF98 ** get_address_of__updateFrameTimes_8() { return &____updateFrameTimes_8; }
	inline void set__updateFrameTimes_8(Queue_1_t4EE8B6A18F07374C2B3FF98A98B5369D5542EF98 * value)
	{
		____updateFrameTimes_8 = value;
		Il2CppCodeGenWriteBarrier((&____updateFrameTimes_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Svelto.Tasks.Profiler.TaskInfo
struct TaskInfo_tE81761C55B71E6B50D0D42AF7CA89B3E5C22B8E1_marshaled_pinvoke
{
	double ____accumulatedUpdateDuration_2;
	double ____lastUpdateDuration_3;
	double ____maxUpdateDuration_4;
	double ____minUpdateDuration_5;
	char* ____taskName_6;
	char* ____threadInfo_7;
	Queue_1_t4EE8B6A18F07374C2B3FF98A98B5369D5542EF98 * ____updateFrameTimes_8;
};
// Native definition for COM marshalling of Svelto.Tasks.Profiler.TaskInfo
struct TaskInfo_tE81761C55B71E6B50D0D42AF7CA89B3E5C22B8E1_marshaled_com
{
	double ____accumulatedUpdateDuration_2;
	double ____lastUpdateDuration_3;
	double ____maxUpdateDuration_4;
	double ____minUpdateDuration_5;
	Il2CppChar* ____taskName_6;
	Il2CppChar* ____threadInfo_7;
	Queue_1_t4EE8B6A18F07374C2B3FF98A98B5369D5542EF98 * ____updateFrameTimes_8;
};
#endif // TASKINFO_TE81761C55B71E6B50D0D42AF7CA89B3E5C22B8E1_H
#ifndef SEQUENTIALMONORUNNER_T65E8C95EDAD86104CA20B30E48F79611A1C53B62_H
#define SEQUENTIALMONORUNNER_T65E8C95EDAD86104CA20B30E48F79611A1C53B62_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Svelto.Tasks.SequentialMonoRunner
struct  SequentialMonoRunner_t65E8C95EDAD86104CA20B30E48F79611A1C53B62  : public MonoRunner_t65496222247A6A3CAFE8294914A4776DE8693784
{
public:
	// Svelto.DataStructures.ThreadSafeQueue`1<Svelto.Tasks.IPausableTask> Svelto.Tasks.SequentialMonoRunner::_newTaskRoutines
	ThreadSafeQueue_1_t4B42DDB151ED238066A071C89AF1FC3DB9C1BE45 * ____newTaskRoutines_2;
	// Svelto.Tasks.Internal.UnityCoroutineRunner_FlushingOperation Svelto.Tasks.SequentialMonoRunner::_flushingOperation
	FlushingOperation_tB0B9CC8E029067E8D130896A68A0E2D390E01027 * ____flushingOperation_3;
	// Svelto.Tasks.Internal.UnityCoroutineRunner_RunningTasksInfo Svelto.Tasks.SequentialMonoRunner::_info
	RunningTasksInfo_t10813DB30B3A26BF8CF0830C17B2B3EE5DBF523C * ____info_4;

public:
	inline static int32_t get_offset_of__newTaskRoutines_2() { return static_cast<int32_t>(offsetof(SequentialMonoRunner_t65E8C95EDAD86104CA20B30E48F79611A1C53B62, ____newTaskRoutines_2)); }
	inline ThreadSafeQueue_1_t4B42DDB151ED238066A071C89AF1FC3DB9C1BE45 * get__newTaskRoutines_2() const { return ____newTaskRoutines_2; }
	inline ThreadSafeQueue_1_t4B42DDB151ED238066A071C89AF1FC3DB9C1BE45 ** get_address_of__newTaskRoutines_2() { return &____newTaskRoutines_2; }
	inline void set__newTaskRoutines_2(ThreadSafeQueue_1_t4B42DDB151ED238066A071C89AF1FC3DB9C1BE45 * value)
	{
		____newTaskRoutines_2 = value;
		Il2CppCodeGenWriteBarrier((&____newTaskRoutines_2), value);
	}

	inline static int32_t get_offset_of__flushingOperation_3() { return static_cast<int32_t>(offsetof(SequentialMonoRunner_t65E8C95EDAD86104CA20B30E48F79611A1C53B62, ____flushingOperation_3)); }
	inline FlushingOperation_tB0B9CC8E029067E8D130896A68A0E2D390E01027 * get__flushingOperation_3() const { return ____flushingOperation_3; }
	inline FlushingOperation_tB0B9CC8E029067E8D130896A68A0E2D390E01027 ** get_address_of__flushingOperation_3() { return &____flushingOperation_3; }
	inline void set__flushingOperation_3(FlushingOperation_tB0B9CC8E029067E8D130896A68A0E2D390E01027 * value)
	{
		____flushingOperation_3 = value;
		Il2CppCodeGenWriteBarrier((&____flushingOperation_3), value);
	}

	inline static int32_t get_offset_of__info_4() { return static_cast<int32_t>(offsetof(SequentialMonoRunner_t65E8C95EDAD86104CA20B30E48F79611A1C53B62, ____info_4)); }
	inline RunningTasksInfo_t10813DB30B3A26BF8CF0830C17B2B3EE5DBF523C * get__info_4() const { return ____info_4; }
	inline RunningTasksInfo_t10813DB30B3A26BF8CF0830C17B2B3EE5DBF523C ** get_address_of__info_4() { return &____info_4; }
	inline void set__info_4(RunningTasksInfo_t10813DB30B3A26BF8CF0830C17B2B3EE5DBF523C * value)
	{
		____info_4 = value;
		Il2CppCodeGenWriteBarrier((&____info_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEQUENTIALMONORUNNER_T65E8C95EDAD86104CA20B30E48F79611A1C53B62_H
#ifndef SERIALTASKCOLLECTION_T31E5E0CD4A5BAA251A396D5B51F12E1FB6B68BE7_H
#define SERIALTASKCOLLECTION_T31E5E0CD4A5BAA251A396D5B51F12E1FB6B68BE7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Svelto.Tasks.SerialTaskCollection
struct  SerialTaskCollection_t31E5E0CD4A5BAA251A396D5B51F12E1FB6B68BE7  : public TaskCollection_tD5BE67D01B98473132820546A081DDB58E47C931
{
public:
	// System.Action Svelto.Tasks.SerialTaskCollection::onComplete
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___onComplete_4;
	// System.Func`2<System.Exception,System.Boolean> Svelto.Tasks.SerialTaskCollection::onException
	Func_2_tC816C5FDED4A7372C449E7660CAB9F94E2AC12F8 * ___onException_5;
	// System.Int32 Svelto.Tasks.SerialTaskCollection::_index
	int32_t ____index_6;
	// System.Object Svelto.Tasks.SerialTaskCollection::_current
	RuntimeObject * ____current_7;
	// System.String Svelto.Tasks.SerialTaskCollection::_name
	String_t* ____name_8;

public:
	inline static int32_t get_offset_of_onComplete_4() { return static_cast<int32_t>(offsetof(SerialTaskCollection_t31E5E0CD4A5BAA251A396D5B51F12E1FB6B68BE7, ___onComplete_4)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_onComplete_4() const { return ___onComplete_4; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_onComplete_4() { return &___onComplete_4; }
	inline void set_onComplete_4(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___onComplete_4 = value;
		Il2CppCodeGenWriteBarrier((&___onComplete_4), value);
	}

	inline static int32_t get_offset_of_onException_5() { return static_cast<int32_t>(offsetof(SerialTaskCollection_t31E5E0CD4A5BAA251A396D5B51F12E1FB6B68BE7, ___onException_5)); }
	inline Func_2_tC816C5FDED4A7372C449E7660CAB9F94E2AC12F8 * get_onException_5() const { return ___onException_5; }
	inline Func_2_tC816C5FDED4A7372C449E7660CAB9F94E2AC12F8 ** get_address_of_onException_5() { return &___onException_5; }
	inline void set_onException_5(Func_2_tC816C5FDED4A7372C449E7660CAB9F94E2AC12F8 * value)
	{
		___onException_5 = value;
		Il2CppCodeGenWriteBarrier((&___onException_5), value);
	}

	inline static int32_t get_offset_of__index_6() { return static_cast<int32_t>(offsetof(SerialTaskCollection_t31E5E0CD4A5BAA251A396D5B51F12E1FB6B68BE7, ____index_6)); }
	inline int32_t get__index_6() const { return ____index_6; }
	inline int32_t* get_address_of__index_6() { return &____index_6; }
	inline void set__index_6(int32_t value)
	{
		____index_6 = value;
	}

	inline static int32_t get_offset_of__current_7() { return static_cast<int32_t>(offsetof(SerialTaskCollection_t31E5E0CD4A5BAA251A396D5B51F12E1FB6B68BE7, ____current_7)); }
	inline RuntimeObject * get__current_7() const { return ____current_7; }
	inline RuntimeObject ** get_address_of__current_7() { return &____current_7; }
	inline void set__current_7(RuntimeObject * value)
	{
		____current_7 = value;
		Il2CppCodeGenWriteBarrier((&____current_7), value);
	}

	inline static int32_t get_offset_of__name_8() { return static_cast<int32_t>(offsetof(SerialTaskCollection_t31E5E0CD4A5BAA251A396D5B51F12E1FB6B68BE7, ____name_8)); }
	inline String_t* get__name_8() const { return ____name_8; }
	inline String_t** get_address_of__name_8() { return &____name_8; }
	inline void set__name_8(String_t* value)
	{
		____name_8 = value;
		Il2CppCodeGenWriteBarrier((&____name_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALTASKCOLLECTION_T31E5E0CD4A5BAA251A396D5B51F12E1FB6B68BE7_H
#ifndef STAGGEREDMONORUNNER_T0488A628E15A0D79BE8BA58D55E271618F4C53FA_H
#define STAGGEREDMONORUNNER_T0488A628E15A0D79BE8BA58D55E271618F4C53FA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Svelto.Tasks.StaggeredMonoRunner
struct  StaggeredMonoRunner_t0488A628E15A0D79BE8BA58D55E271618F4C53FA  : public MonoRunner_t65496222247A6A3CAFE8294914A4776DE8693784
{
public:
	// Svelto.Tasks.StaggeredMonoRunner_FlushingOperationStaggered Svelto.Tasks.StaggeredMonoRunner::_flushingOperation
	FlushingOperationStaggered_tB9B268018A84589696B8AC6CA3B9B8E6778AF1F0 * ____flushingOperation_2;
	// Svelto.Tasks.Internal.UnityCoroutineRunner_RunningTasksInfo Svelto.Tasks.StaggeredMonoRunner::_info
	RunningTasksInfo_t10813DB30B3A26BF8CF0830C17B2B3EE5DBF523C * ____info_3;
	// Svelto.DataStructures.ThreadSafeQueue`1<Svelto.Tasks.IPausableTask> Svelto.Tasks.StaggeredMonoRunner::_newTaskRoutines
	ThreadSafeQueue_1_t4B42DDB151ED238066A071C89AF1FC3DB9C1BE45 * ____newTaskRoutines_4;

public:
	inline static int32_t get_offset_of__flushingOperation_2() { return static_cast<int32_t>(offsetof(StaggeredMonoRunner_t0488A628E15A0D79BE8BA58D55E271618F4C53FA, ____flushingOperation_2)); }
	inline FlushingOperationStaggered_tB9B268018A84589696B8AC6CA3B9B8E6778AF1F0 * get__flushingOperation_2() const { return ____flushingOperation_2; }
	inline FlushingOperationStaggered_tB9B268018A84589696B8AC6CA3B9B8E6778AF1F0 ** get_address_of__flushingOperation_2() { return &____flushingOperation_2; }
	inline void set__flushingOperation_2(FlushingOperationStaggered_tB9B268018A84589696B8AC6CA3B9B8E6778AF1F0 * value)
	{
		____flushingOperation_2 = value;
		Il2CppCodeGenWriteBarrier((&____flushingOperation_2), value);
	}

	inline static int32_t get_offset_of__info_3() { return static_cast<int32_t>(offsetof(StaggeredMonoRunner_t0488A628E15A0D79BE8BA58D55E271618F4C53FA, ____info_3)); }
	inline RunningTasksInfo_t10813DB30B3A26BF8CF0830C17B2B3EE5DBF523C * get__info_3() const { return ____info_3; }
	inline RunningTasksInfo_t10813DB30B3A26BF8CF0830C17B2B3EE5DBF523C ** get_address_of__info_3() { return &____info_3; }
	inline void set__info_3(RunningTasksInfo_t10813DB30B3A26BF8CF0830C17B2B3EE5DBF523C * value)
	{
		____info_3 = value;
		Il2CppCodeGenWriteBarrier((&____info_3), value);
	}

	inline static int32_t get_offset_of__newTaskRoutines_4() { return static_cast<int32_t>(offsetof(StaggeredMonoRunner_t0488A628E15A0D79BE8BA58D55E271618F4C53FA, ____newTaskRoutines_4)); }
	inline ThreadSafeQueue_1_t4B42DDB151ED238066A071C89AF1FC3DB9C1BE45 * get__newTaskRoutines_4() const { return ____newTaskRoutines_4; }
	inline ThreadSafeQueue_1_t4B42DDB151ED238066A071C89AF1FC3DB9C1BE45 ** get_address_of__newTaskRoutines_4() { return &____newTaskRoutines_4; }
	inline void set__newTaskRoutines_4(ThreadSafeQueue_1_t4B42DDB151ED238066A071C89AF1FC3DB9C1BE45 * value)
	{
		____newTaskRoutines_4 = value;
		Il2CppCodeGenWriteBarrier((&____newTaskRoutines_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STAGGEREDMONORUNNER_T0488A628E15A0D79BE8BA58D55E271618F4C53FA_H
#ifndef FLUSHINGOPERATIONSTAGGERED_TB9B268018A84589696B8AC6CA3B9B8E6778AF1F0_H
#define FLUSHINGOPERATIONSTAGGERED_TB9B268018A84589696B8AC6CA3B9B8E6778AF1F0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Svelto.Tasks.StaggeredMonoRunner_FlushingOperationStaggered
struct  FlushingOperationStaggered_tB9B268018A84589696B8AC6CA3B9B8E6778AF1F0  : public FlushingOperation_tB0B9CC8E029067E8D130896A68A0E2D390E01027
{
public:
	// System.Int32 Svelto.Tasks.StaggeredMonoRunner_FlushingOperationStaggered::maxTasksPerFrame
	int32_t ___maxTasksPerFrame_1;

public:
	inline static int32_t get_offset_of_maxTasksPerFrame_1() { return static_cast<int32_t>(offsetof(FlushingOperationStaggered_tB9B268018A84589696B8AC6CA3B9B8E6778AF1F0, ___maxTasksPerFrame_1)); }
	inline int32_t get_maxTasksPerFrame_1() const { return ___maxTasksPerFrame_1; }
	inline int32_t* get_address_of_maxTasksPerFrame_1() { return &___maxTasksPerFrame_1; }
	inline void set_maxTasksPerFrame_1(int32_t value)
	{
		___maxTasksPerFrame_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLUSHINGOPERATIONSTAGGERED_TB9B268018A84589696B8AC6CA3B9B8E6778AF1F0_H
#ifndef TASKYIELDSIENUMERABLEEXCEPTION_T7355F3FDE518ABBB2BB70429484D0D3B92134964_H
#define TASKYIELDSIENUMERABLEEXCEPTION_T7355F3FDE518ABBB2BB70429484D0D3B92134964_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Svelto.Tasks.TaskYieldsIEnumerableException
struct  TaskYieldsIEnumerableException_t7355F3FDE518ABBB2BB70429484D0D3B92134964  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TASKYIELDSIENUMERABLEEXCEPTION_T7355F3FDE518ABBB2BB70429484D0D3B92134964_H
#ifndef UPDATEMONORUNNER_TD70489504A43A0A4ADA16547CB1203C1FDC1B54C_H
#define UPDATEMONORUNNER_TD70489504A43A0A4ADA16547CB1203C1FDC1B54C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Svelto.Tasks.UpdateMonoRunner
struct  UpdateMonoRunner_tD70489504A43A0A4ADA16547CB1203C1FDC1B54C  : public MonoRunner_t65496222247A6A3CAFE8294914A4776DE8693784
{
public:
	// Svelto.DataStructures.ThreadSafeQueue`1<Svelto.Tasks.IPausableTask> Svelto.Tasks.UpdateMonoRunner::_newTaskRoutines
	ThreadSafeQueue_1_t4B42DDB151ED238066A071C89AF1FC3DB9C1BE45 * ____newTaskRoutines_2;
	// Svelto.Tasks.Internal.UnityCoroutineRunner_FlushingOperation Svelto.Tasks.UpdateMonoRunner::_flushingOperation
	FlushingOperation_tB0B9CC8E029067E8D130896A68A0E2D390E01027 * ____flushingOperation_3;
	// Svelto.Tasks.Internal.UnityCoroutineRunner_RunningTasksInfo Svelto.Tasks.UpdateMonoRunner::_info
	RunningTasksInfo_t10813DB30B3A26BF8CF0830C17B2B3EE5DBF523C * ____info_4;

public:
	inline static int32_t get_offset_of__newTaskRoutines_2() { return static_cast<int32_t>(offsetof(UpdateMonoRunner_tD70489504A43A0A4ADA16547CB1203C1FDC1B54C, ____newTaskRoutines_2)); }
	inline ThreadSafeQueue_1_t4B42DDB151ED238066A071C89AF1FC3DB9C1BE45 * get__newTaskRoutines_2() const { return ____newTaskRoutines_2; }
	inline ThreadSafeQueue_1_t4B42DDB151ED238066A071C89AF1FC3DB9C1BE45 ** get_address_of__newTaskRoutines_2() { return &____newTaskRoutines_2; }
	inline void set__newTaskRoutines_2(ThreadSafeQueue_1_t4B42DDB151ED238066A071C89AF1FC3DB9C1BE45 * value)
	{
		____newTaskRoutines_2 = value;
		Il2CppCodeGenWriteBarrier((&____newTaskRoutines_2), value);
	}

	inline static int32_t get_offset_of__flushingOperation_3() { return static_cast<int32_t>(offsetof(UpdateMonoRunner_tD70489504A43A0A4ADA16547CB1203C1FDC1B54C, ____flushingOperation_3)); }
	inline FlushingOperation_tB0B9CC8E029067E8D130896A68A0E2D390E01027 * get__flushingOperation_3() const { return ____flushingOperation_3; }
	inline FlushingOperation_tB0B9CC8E029067E8D130896A68A0E2D390E01027 ** get_address_of__flushingOperation_3() { return &____flushingOperation_3; }
	inline void set__flushingOperation_3(FlushingOperation_tB0B9CC8E029067E8D130896A68A0E2D390E01027 * value)
	{
		____flushingOperation_3 = value;
		Il2CppCodeGenWriteBarrier((&____flushingOperation_3), value);
	}

	inline static int32_t get_offset_of__info_4() { return static_cast<int32_t>(offsetof(UpdateMonoRunner_tD70489504A43A0A4ADA16547CB1203C1FDC1B54C, ____info_4)); }
	inline RunningTasksInfo_t10813DB30B3A26BF8CF0830C17B2B3EE5DBF523C * get__info_4() const { return ____info_4; }
	inline RunningTasksInfo_t10813DB30B3A26BF8CF0830C17B2B3EE5DBF523C ** get_address_of__info_4() { return &____info_4; }
	inline void set__info_4(RunningTasksInfo_t10813DB30B3A26BF8CF0830C17B2B3EE5DBF523C * value)
	{
		____info_4 = value;
		Il2CppCodeGenWriteBarrier((&____info_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATEMONORUNNER_TD70489504A43A0A4ADA16547CB1203C1FDC1B54C_H
#ifndef BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#define BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifndef DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#define DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 
{
public:
	// System.UInt64 System.DateTime::dateData
	uint64_t ___dateData_44;

public:
	inline static int32_t get_offset_of_dateData_44() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132, ___dateData_44)); }
	inline uint64_t get_dateData_44() const { return ___dateData_44; }
	inline uint64_t* get_address_of_dateData_44() { return &___dateData_44; }
	inline void set_dateData_44(uint64_t value)
	{
		___dateData_44 = value;
	}
};

struct DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields
{
public:
	// System.Int32[] System.DateTime::DaysToMonth365
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth365_29;
	// System.Int32[] System.DateTime::DaysToMonth366
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth366_30;
	// System.DateTime System.DateTime::MinValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MinValue_31;
	// System.DateTime System.DateTime::MaxValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MaxValue_32;

public:
	inline static int32_t get_offset_of_DaysToMonth365_29() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth365_29)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth365_29() const { return ___DaysToMonth365_29; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth365_29() { return &___DaysToMonth365_29; }
	inline void set_DaysToMonth365_29(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth365_29 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth365_29), value);
	}

	inline static int32_t get_offset_of_DaysToMonth366_30() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth366_30)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth366_30() const { return ___DaysToMonth366_30; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth366_30() { return &___DaysToMonth366_30; }
	inline void set_DaysToMonth366_30(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth366_30 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth366_30), value);
	}

	inline static int32_t get_offset_of_MinValue_31() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MinValue_31)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MinValue_31() const { return ___MinValue_31; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MinValue_31() { return &___MinValue_31; }
	inline void set_MinValue_31(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MinValue_31 = value;
	}

	inline static int32_t get_offset_of_MaxValue_32() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MaxValue_32)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MaxValue_32() const { return ___MaxValue_32; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MaxValue_32() { return &___MaxValue_32; }
	inline void set_MaxValue_32(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MaxValue_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#define COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef ASSERTIONEXCEPTION_T253F52A8725E364A1AE7A79923C78A5BBD298242_H
#define ASSERTIONEXCEPTION_T253F52A8725E364A1AE7A79923C78A5BBD298242_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DBC.AssertionException
struct  AssertionException_t253F52A8725E364A1AE7A79923C78A5BBD298242  : public DesignByContractException_t8D28E8A87F2986C2D9F7C4FA9B4AB37C8B884A45
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSERTIONEXCEPTION_T253F52A8725E364A1AE7A79923C78A5BBD298242_H
#ifndef INVARIANTEXCEPTION_TC747C04CFC7A61ED5A3E5D44B0BE3A5BECE11DC5_H
#define INVARIANTEXCEPTION_TC747C04CFC7A61ED5A3E5D44B0BE3A5BECE11DC5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DBC.InvariantException
struct  InvariantException_tC747C04CFC7A61ED5A3E5D44B0BE3A5BECE11DC5  : public DesignByContractException_t8D28E8A87F2986C2D9F7C4FA9B4AB37C8B884A45
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVARIANTEXCEPTION_TC747C04CFC7A61ED5A3E5D44B0BE3A5BECE11DC5_H
#ifndef POSTCONDITIONEXCEPTION_T38575CEE6CA0119BF41CAA638262B95E7DBCFFD1_H
#define POSTCONDITIONEXCEPTION_T38575CEE6CA0119BF41CAA638262B95E7DBCFFD1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DBC.PostconditionException
struct  PostconditionException_t38575CEE6CA0119BF41CAA638262B95E7DBCFFD1  : public DesignByContractException_t8D28E8A87F2986C2D9F7C4FA9B4AB37C8B884A45
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTCONDITIONEXCEPTION_T38575CEE6CA0119BF41CAA638262B95E7DBCFFD1_H
#ifndef PRECONDITIONEXCEPTION_T5A81684757F5B89788A3697803DE89B4357FFE5B_H
#define PRECONDITIONEXCEPTION_T5A81684757F5B89788A3697803DE89B4357FFE5B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DBC.PreconditionException
struct  PreconditionException_t5A81684757F5B89788A3697803DE89B4357FFE5B  : public DesignByContractException_t8D28E8A87F2986C2D9F7C4FA9B4AB37C8B884A45
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRECONDITIONEXCEPTION_T5A81684757F5B89788A3697803DE89B4357FFE5B_H
#ifndef CONTINUATIONWRAPPER_T135700DA709759BA12FEE8996B57AF89BFE20399_H
#define CONTINUATIONWRAPPER_T135700DA709759BA12FEE8996B57AF89BFE20399_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Svelto.Tasks.ContinuationWrapper
struct  ContinuationWrapper_t135700DA709759BA12FEE8996B57AF89BFE20399  : public RuntimeObject
{
public:
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) Svelto.Tasks.ContinuationWrapper::_completed
	bool ____completed_0;
	// System.Func`1<System.Boolean> Svelto.Tasks.ContinuationWrapper::_condition
	Func_1_t4ABD6DAD480574F152452DD6B9C9A55F4F6655F1 * ____condition_1;

public:
	inline static int32_t get_offset_of__completed_0() { return static_cast<int32_t>(offsetof(ContinuationWrapper_t135700DA709759BA12FEE8996B57AF89BFE20399, ____completed_0)); }
	inline bool get__completed_0() const { return ____completed_0; }
	inline bool* get_address_of__completed_0() { return &____completed_0; }
	inline void set__completed_0(bool value)
	{
		____completed_0 = value;
	}

	inline static int32_t get_offset_of__condition_1() { return static_cast<int32_t>(offsetof(ContinuationWrapper_t135700DA709759BA12FEE8996B57AF89BFE20399, ____condition_1)); }
	inline Func_1_t4ABD6DAD480574F152452DD6B9C9A55F4F6655F1 * get__condition_1() const { return ____condition_1; }
	inline Func_1_t4ABD6DAD480574F152452DD6B9C9A55F4F6655F1 ** get_address_of__condition_1() { return &____condition_1; }
	inline void set__condition_1(Func_1_t4ABD6DAD480574F152452DD6B9C9A55F4F6655F1 * value)
	{
		____condition_1 = value;
		Il2CppCodeGenWriteBarrier((&____condition_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTINUATIONWRAPPER_T135700DA709759BA12FEE8996B57AF89BFE20399_H
#ifndef INTERLEAVEDLOOPACTIONENUMERATOR_TAED110EE2D9E8DBE40BE1BD5D5E4D8439B4F9954_H
#define INTERLEAVEDLOOPACTIONENUMERATOR_TAED110EE2D9E8DBE40BE1BD5D5E4D8439B4F9954_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Svelto.Tasks.InterleavedLoopActionEnumerator
struct  InterleavedLoopActionEnumerator_tAED110EE2D9E8DBE40BE1BD5D5E4D8439B4F9954  : public RuntimeObject
{
public:
	// System.Action Svelto.Tasks.InterleavedLoopActionEnumerator::_action
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ____action_0;
	// System.DateTime Svelto.Tasks.InterleavedLoopActionEnumerator::_then
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ____then_1;
	// System.Double Svelto.Tasks.InterleavedLoopActionEnumerator::_interval
	double ____interval_2;

public:
	inline static int32_t get_offset_of__action_0() { return static_cast<int32_t>(offsetof(InterleavedLoopActionEnumerator_tAED110EE2D9E8DBE40BE1BD5D5E4D8439B4F9954, ____action_0)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get__action_0() const { return ____action_0; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of__action_0() { return &____action_0; }
	inline void set__action_0(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		____action_0 = value;
		Il2CppCodeGenWriteBarrier((&____action_0), value);
	}

	inline static int32_t get_offset_of__then_1() { return static_cast<int32_t>(offsetof(InterleavedLoopActionEnumerator_tAED110EE2D9E8DBE40BE1BD5D5E4D8439B4F9954, ____then_1)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get__then_1() const { return ____then_1; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of__then_1() { return &____then_1; }
	inline void set__then_1(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		____then_1 = value;
	}

	inline static int32_t get_offset_of__interval_2() { return static_cast<int32_t>(offsetof(InterleavedLoopActionEnumerator_tAED110EE2D9E8DBE40BE1BD5D5E4D8439B4F9954, ____interval_2)); }
	inline double get__interval_2() const { return ____interval_2; }
	inline double* get_address_of__interval_2() { return &____interval_2; }
	inline void set__interval_2(double value)
	{
		____interval_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERLEAVEDLOOPACTIONENUMERATOR_TAED110EE2D9E8DBE40BE1BD5D5E4D8439B4F9954_H
#ifndef PAUSABLETASK_T9370376F9736D7C79A37C1C5F687174754FB618A_H
#define PAUSABLETASK_T9370376F9736D7C79A37C1C5F687174754FB618A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Svelto.Tasks.Internal.PausableTask
struct  PausableTask_t9370376F9736D7C79A37C1C5F687174754FB618A  : public RuntimeObject
{
public:
	// System.Action Svelto.Tasks.Internal.PausableTask::<onExplicitlyStopped>k__BackingField
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___U3ConExplicitlyStoppedU3Ek__BackingField_1;
	// Svelto.Tasks.IRunner Svelto.Tasks.Internal.PausableTask::_runner
	RuntimeObject* ____runner_2;
	// System.Collections.IEnumerator Svelto.Tasks.Internal.PausableTask::_coroutine
	RuntimeObject* ____coroutine_3;
	// Svelto.Tasks.SerialTaskCollection Svelto.Tasks.Internal.PausableTask::_coroutineWrapper
	SerialTaskCollection_t31E5E0CD4A5BAA251A396D5B51F12E1FB6B68BE7 * ____coroutineWrapper_4;
	// Svelto.Tasks.ContinuationWrapper Svelto.Tasks.Internal.PausableTask::_continuationWrapper
	ContinuationWrapper_t135700DA709759BA12FEE8996B57AF89BFE20399 * ____continuationWrapper_5;
	// Svelto.Tasks.ContinuationWrapper Svelto.Tasks.Internal.PausableTask::_pendingContinuationWrapper
	ContinuationWrapper_t135700DA709759BA12FEE8996B57AF89BFE20399 * ____pendingContinuationWrapper_6;
	// System.Boolean Svelto.Tasks.Internal.PausableTask::_threadSafe
	bool ____threadSafe_7;
	// System.Boolean Svelto.Tasks.Internal.PausableTask::_compilerGenerated
	bool ____compilerGenerated_8;
	// System.Boolean Svelto.Tasks.Internal.PausableTask::_taskEnumeratorJustSet
	bool ____taskEnumeratorJustSet_9;
	// System.Collections.IEnumerator Svelto.Tasks.Internal.PausableTask::_pendingEnumerator
	RuntimeObject* ____pendingEnumerator_10;
	// System.Collections.IEnumerator Svelto.Tasks.Internal.PausableTask::_taskEnumerator
	RuntimeObject* ____taskEnumerator_11;
	// Svelto.Tasks.Internal.PausableTaskPool Svelto.Tasks.Internal.PausableTask::_pool
	PausableTaskPool_t650A2D678FDFAA152B5CB537987A31A82A014D28 * ____pool_12;
	// System.Func`1<System.Collections.IEnumerator> Svelto.Tasks.Internal.PausableTask::_taskGenerator
	Func_1_tB87849BEE3BBD29FEB301AB876F67ADA1BFEC3AB * ____taskGenerator_13;
	// System.Action`1<Svelto.Tasks.PausableTaskException> Svelto.Tasks.Internal.PausableTask::_onFail
	Action_1_tCB2457F2DCF5883C9AC781663523E6FF0009B33C * ____onFail_14;
	// System.Action Svelto.Tasks.Internal.PausableTask::_onStop
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ____onStop_15;
	// System.String Svelto.Tasks.Internal.PausableTask::_name
	String_t* ____name_16;
	// System.Boolean Svelto.Tasks.Internal.PausableTask::_started
	bool ____started_17;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) Svelto.Tasks.Internal.PausableTask::_completed
	bool ____completed_18;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) Svelto.Tasks.Internal.PausableTask::_explicitlyStopped
	bool ____explicitlyStopped_19;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) Svelto.Tasks.Internal.PausableTask::_paused
	bool ____paused_20;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) Svelto.Tasks.Internal.PausableTask::_pendingRestart
	bool ____pendingRestart_21;
	// System.String Svelto.Tasks.Internal.PausableTask::_callStartFirstError
	String_t* ____callStartFirstError_22;

public:
	inline static int32_t get_offset_of_U3ConExplicitlyStoppedU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PausableTask_t9370376F9736D7C79A37C1C5F687174754FB618A, ___U3ConExplicitlyStoppedU3Ek__BackingField_1)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_U3ConExplicitlyStoppedU3Ek__BackingField_1() const { return ___U3ConExplicitlyStoppedU3Ek__BackingField_1; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_U3ConExplicitlyStoppedU3Ek__BackingField_1() { return &___U3ConExplicitlyStoppedU3Ek__BackingField_1; }
	inline void set_U3ConExplicitlyStoppedU3Ek__BackingField_1(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___U3ConExplicitlyStoppedU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3ConExplicitlyStoppedU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of__runner_2() { return static_cast<int32_t>(offsetof(PausableTask_t9370376F9736D7C79A37C1C5F687174754FB618A, ____runner_2)); }
	inline RuntimeObject* get__runner_2() const { return ____runner_2; }
	inline RuntimeObject** get_address_of__runner_2() { return &____runner_2; }
	inline void set__runner_2(RuntimeObject* value)
	{
		____runner_2 = value;
		Il2CppCodeGenWriteBarrier((&____runner_2), value);
	}

	inline static int32_t get_offset_of__coroutine_3() { return static_cast<int32_t>(offsetof(PausableTask_t9370376F9736D7C79A37C1C5F687174754FB618A, ____coroutine_3)); }
	inline RuntimeObject* get__coroutine_3() const { return ____coroutine_3; }
	inline RuntimeObject** get_address_of__coroutine_3() { return &____coroutine_3; }
	inline void set__coroutine_3(RuntimeObject* value)
	{
		____coroutine_3 = value;
		Il2CppCodeGenWriteBarrier((&____coroutine_3), value);
	}

	inline static int32_t get_offset_of__coroutineWrapper_4() { return static_cast<int32_t>(offsetof(PausableTask_t9370376F9736D7C79A37C1C5F687174754FB618A, ____coroutineWrapper_4)); }
	inline SerialTaskCollection_t31E5E0CD4A5BAA251A396D5B51F12E1FB6B68BE7 * get__coroutineWrapper_4() const { return ____coroutineWrapper_4; }
	inline SerialTaskCollection_t31E5E0CD4A5BAA251A396D5B51F12E1FB6B68BE7 ** get_address_of__coroutineWrapper_4() { return &____coroutineWrapper_4; }
	inline void set__coroutineWrapper_4(SerialTaskCollection_t31E5E0CD4A5BAA251A396D5B51F12E1FB6B68BE7 * value)
	{
		____coroutineWrapper_4 = value;
		Il2CppCodeGenWriteBarrier((&____coroutineWrapper_4), value);
	}

	inline static int32_t get_offset_of__continuationWrapper_5() { return static_cast<int32_t>(offsetof(PausableTask_t9370376F9736D7C79A37C1C5F687174754FB618A, ____continuationWrapper_5)); }
	inline ContinuationWrapper_t135700DA709759BA12FEE8996B57AF89BFE20399 * get__continuationWrapper_5() const { return ____continuationWrapper_5; }
	inline ContinuationWrapper_t135700DA709759BA12FEE8996B57AF89BFE20399 ** get_address_of__continuationWrapper_5() { return &____continuationWrapper_5; }
	inline void set__continuationWrapper_5(ContinuationWrapper_t135700DA709759BA12FEE8996B57AF89BFE20399 * value)
	{
		____continuationWrapper_5 = value;
		Il2CppCodeGenWriteBarrier((&____continuationWrapper_5), value);
	}

	inline static int32_t get_offset_of__pendingContinuationWrapper_6() { return static_cast<int32_t>(offsetof(PausableTask_t9370376F9736D7C79A37C1C5F687174754FB618A, ____pendingContinuationWrapper_6)); }
	inline ContinuationWrapper_t135700DA709759BA12FEE8996B57AF89BFE20399 * get__pendingContinuationWrapper_6() const { return ____pendingContinuationWrapper_6; }
	inline ContinuationWrapper_t135700DA709759BA12FEE8996B57AF89BFE20399 ** get_address_of__pendingContinuationWrapper_6() { return &____pendingContinuationWrapper_6; }
	inline void set__pendingContinuationWrapper_6(ContinuationWrapper_t135700DA709759BA12FEE8996B57AF89BFE20399 * value)
	{
		____pendingContinuationWrapper_6 = value;
		Il2CppCodeGenWriteBarrier((&____pendingContinuationWrapper_6), value);
	}

	inline static int32_t get_offset_of__threadSafe_7() { return static_cast<int32_t>(offsetof(PausableTask_t9370376F9736D7C79A37C1C5F687174754FB618A, ____threadSafe_7)); }
	inline bool get__threadSafe_7() const { return ____threadSafe_7; }
	inline bool* get_address_of__threadSafe_7() { return &____threadSafe_7; }
	inline void set__threadSafe_7(bool value)
	{
		____threadSafe_7 = value;
	}

	inline static int32_t get_offset_of__compilerGenerated_8() { return static_cast<int32_t>(offsetof(PausableTask_t9370376F9736D7C79A37C1C5F687174754FB618A, ____compilerGenerated_8)); }
	inline bool get__compilerGenerated_8() const { return ____compilerGenerated_8; }
	inline bool* get_address_of__compilerGenerated_8() { return &____compilerGenerated_8; }
	inline void set__compilerGenerated_8(bool value)
	{
		____compilerGenerated_8 = value;
	}

	inline static int32_t get_offset_of__taskEnumeratorJustSet_9() { return static_cast<int32_t>(offsetof(PausableTask_t9370376F9736D7C79A37C1C5F687174754FB618A, ____taskEnumeratorJustSet_9)); }
	inline bool get__taskEnumeratorJustSet_9() const { return ____taskEnumeratorJustSet_9; }
	inline bool* get_address_of__taskEnumeratorJustSet_9() { return &____taskEnumeratorJustSet_9; }
	inline void set__taskEnumeratorJustSet_9(bool value)
	{
		____taskEnumeratorJustSet_9 = value;
	}

	inline static int32_t get_offset_of__pendingEnumerator_10() { return static_cast<int32_t>(offsetof(PausableTask_t9370376F9736D7C79A37C1C5F687174754FB618A, ____pendingEnumerator_10)); }
	inline RuntimeObject* get__pendingEnumerator_10() const { return ____pendingEnumerator_10; }
	inline RuntimeObject** get_address_of__pendingEnumerator_10() { return &____pendingEnumerator_10; }
	inline void set__pendingEnumerator_10(RuntimeObject* value)
	{
		____pendingEnumerator_10 = value;
		Il2CppCodeGenWriteBarrier((&____pendingEnumerator_10), value);
	}

	inline static int32_t get_offset_of__taskEnumerator_11() { return static_cast<int32_t>(offsetof(PausableTask_t9370376F9736D7C79A37C1C5F687174754FB618A, ____taskEnumerator_11)); }
	inline RuntimeObject* get__taskEnumerator_11() const { return ____taskEnumerator_11; }
	inline RuntimeObject** get_address_of__taskEnumerator_11() { return &____taskEnumerator_11; }
	inline void set__taskEnumerator_11(RuntimeObject* value)
	{
		____taskEnumerator_11 = value;
		Il2CppCodeGenWriteBarrier((&____taskEnumerator_11), value);
	}

	inline static int32_t get_offset_of__pool_12() { return static_cast<int32_t>(offsetof(PausableTask_t9370376F9736D7C79A37C1C5F687174754FB618A, ____pool_12)); }
	inline PausableTaskPool_t650A2D678FDFAA152B5CB537987A31A82A014D28 * get__pool_12() const { return ____pool_12; }
	inline PausableTaskPool_t650A2D678FDFAA152B5CB537987A31A82A014D28 ** get_address_of__pool_12() { return &____pool_12; }
	inline void set__pool_12(PausableTaskPool_t650A2D678FDFAA152B5CB537987A31A82A014D28 * value)
	{
		____pool_12 = value;
		Il2CppCodeGenWriteBarrier((&____pool_12), value);
	}

	inline static int32_t get_offset_of__taskGenerator_13() { return static_cast<int32_t>(offsetof(PausableTask_t9370376F9736D7C79A37C1C5F687174754FB618A, ____taskGenerator_13)); }
	inline Func_1_tB87849BEE3BBD29FEB301AB876F67ADA1BFEC3AB * get__taskGenerator_13() const { return ____taskGenerator_13; }
	inline Func_1_tB87849BEE3BBD29FEB301AB876F67ADA1BFEC3AB ** get_address_of__taskGenerator_13() { return &____taskGenerator_13; }
	inline void set__taskGenerator_13(Func_1_tB87849BEE3BBD29FEB301AB876F67ADA1BFEC3AB * value)
	{
		____taskGenerator_13 = value;
		Il2CppCodeGenWriteBarrier((&____taskGenerator_13), value);
	}

	inline static int32_t get_offset_of__onFail_14() { return static_cast<int32_t>(offsetof(PausableTask_t9370376F9736D7C79A37C1C5F687174754FB618A, ____onFail_14)); }
	inline Action_1_tCB2457F2DCF5883C9AC781663523E6FF0009B33C * get__onFail_14() const { return ____onFail_14; }
	inline Action_1_tCB2457F2DCF5883C9AC781663523E6FF0009B33C ** get_address_of__onFail_14() { return &____onFail_14; }
	inline void set__onFail_14(Action_1_tCB2457F2DCF5883C9AC781663523E6FF0009B33C * value)
	{
		____onFail_14 = value;
		Il2CppCodeGenWriteBarrier((&____onFail_14), value);
	}

	inline static int32_t get_offset_of__onStop_15() { return static_cast<int32_t>(offsetof(PausableTask_t9370376F9736D7C79A37C1C5F687174754FB618A, ____onStop_15)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get__onStop_15() const { return ____onStop_15; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of__onStop_15() { return &____onStop_15; }
	inline void set__onStop_15(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		____onStop_15 = value;
		Il2CppCodeGenWriteBarrier((&____onStop_15), value);
	}

	inline static int32_t get_offset_of__name_16() { return static_cast<int32_t>(offsetof(PausableTask_t9370376F9736D7C79A37C1C5F687174754FB618A, ____name_16)); }
	inline String_t* get__name_16() const { return ____name_16; }
	inline String_t** get_address_of__name_16() { return &____name_16; }
	inline void set__name_16(String_t* value)
	{
		____name_16 = value;
		Il2CppCodeGenWriteBarrier((&____name_16), value);
	}

	inline static int32_t get_offset_of__started_17() { return static_cast<int32_t>(offsetof(PausableTask_t9370376F9736D7C79A37C1C5F687174754FB618A, ____started_17)); }
	inline bool get__started_17() const { return ____started_17; }
	inline bool* get_address_of__started_17() { return &____started_17; }
	inline void set__started_17(bool value)
	{
		____started_17 = value;
	}

	inline static int32_t get_offset_of__completed_18() { return static_cast<int32_t>(offsetof(PausableTask_t9370376F9736D7C79A37C1C5F687174754FB618A, ____completed_18)); }
	inline bool get__completed_18() const { return ____completed_18; }
	inline bool* get_address_of__completed_18() { return &____completed_18; }
	inline void set__completed_18(bool value)
	{
		____completed_18 = value;
	}

	inline static int32_t get_offset_of__explicitlyStopped_19() { return static_cast<int32_t>(offsetof(PausableTask_t9370376F9736D7C79A37C1C5F687174754FB618A, ____explicitlyStopped_19)); }
	inline bool get__explicitlyStopped_19() const { return ____explicitlyStopped_19; }
	inline bool* get_address_of__explicitlyStopped_19() { return &____explicitlyStopped_19; }
	inline void set__explicitlyStopped_19(bool value)
	{
		____explicitlyStopped_19 = value;
	}

	inline static int32_t get_offset_of__paused_20() { return static_cast<int32_t>(offsetof(PausableTask_t9370376F9736D7C79A37C1C5F687174754FB618A, ____paused_20)); }
	inline bool get__paused_20() const { return ____paused_20; }
	inline bool* get_address_of__paused_20() { return &____paused_20; }
	inline void set__paused_20(bool value)
	{
		____paused_20 = value;
	}

	inline static int32_t get_offset_of__pendingRestart_21() { return static_cast<int32_t>(offsetof(PausableTask_t9370376F9736D7C79A37C1C5F687174754FB618A, ____pendingRestart_21)); }
	inline bool get__pendingRestart_21() const { return ____pendingRestart_21; }
	inline bool* get_address_of__pendingRestart_21() { return &____pendingRestart_21; }
	inline void set__pendingRestart_21(bool value)
	{
		____pendingRestart_21 = value;
	}

	inline static int32_t get_offset_of__callStartFirstError_22() { return static_cast<int32_t>(offsetof(PausableTask_t9370376F9736D7C79A37C1C5F687174754FB618A, ____callStartFirstError_22)); }
	inline String_t* get__callStartFirstError_22() const { return ____callStartFirstError_22; }
	inline String_t** get_address_of__callStartFirstError_22() { return &____callStartFirstError_22; }
	inline void set__callStartFirstError_22(String_t* value)
	{
		____callStartFirstError_22 = value;
		Il2CppCodeGenWriteBarrier((&____callStartFirstError_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAUSABLETASK_T9370376F9736D7C79A37C1C5F687174754FB618A_H
#ifndef MULTITHREADRUNNER_TC4A17C81CFC215E6A3719AADB82ACFB718255F92_H
#define MULTITHREADRUNNER_TC4A17C81CFC215E6A3719AADB82ACFB718255F92_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Svelto.Tasks.MultiThreadRunner
struct  MultiThreadRunner_tC4A17C81CFC215E6A3719AADB82ACFB718255F92  : public RuntimeObject
{
public:
	// System.Boolean Svelto.Tasks.MultiThreadRunner::<paused>k__BackingField
	bool ___U3CpausedU3Ek__BackingField_0;
	// Svelto.DataStructures.FasterList`1<Svelto.Tasks.IPausableTask> Svelto.Tasks.MultiThreadRunner::_coroutines
	FasterList_1_t2B537C5D221BBAAC4DA80F69541C173562A8478A * ____coroutines_1;
	// Svelto.DataStructures.ThreadSafeQueue`1<Svelto.Tasks.IPausableTask> Svelto.Tasks.MultiThreadRunner::_newTaskRoutines
	ThreadSafeQueue_1_t4B42DDB151ED238066A071C89AF1FC3DB9C1BE45 * ____newTaskRoutines_2;
	// System.String Svelto.Tasks.MultiThreadRunner::_name
	String_t* ____name_3;
	// System.Int32 Svelto.Tasks.MultiThreadRunner::_interlock
	int32_t ____interlock_4;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) Svelto.Tasks.MultiThreadRunner::_isAlive
	bool ____isAlive_5;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) Svelto.Tasks.MultiThreadRunner::_waitForflush
	bool ____waitForflush_6;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) Svelto.Tasks.MultiThreadRunner::_breakThread
	bool ____breakThread_7;
	// Svelto.Utilities.ManualResetEventEx Svelto.Tasks.MultiThreadRunner::_mevent
	ManualResetEventEx_t33E2A70F227B45E36C283FE859544123EC73BB78 * ____mevent_8;
	// System.Action Svelto.Tasks.MultiThreadRunner::_lockingMechanism
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ____lockingMechanism_9;
	// System.Int32 Svelto.Tasks.MultiThreadRunner::_interval
	int32_t ____interval_10;
	// System.Diagnostics.Stopwatch Svelto.Tasks.MultiThreadRunner::_watch
	Stopwatch_t0778B5C8DF8FE1D87FC57A2411DA695850BD64D4 * ____watch_11;
	// System.Action Svelto.Tasks.MultiThreadRunner::_onThreadKilled
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ____onThreadKilled_12;

public:
	inline static int32_t get_offset_of_U3CpausedU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MultiThreadRunner_tC4A17C81CFC215E6A3719AADB82ACFB718255F92, ___U3CpausedU3Ek__BackingField_0)); }
	inline bool get_U3CpausedU3Ek__BackingField_0() const { return ___U3CpausedU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CpausedU3Ek__BackingField_0() { return &___U3CpausedU3Ek__BackingField_0; }
	inline void set_U3CpausedU3Ek__BackingField_0(bool value)
	{
		___U3CpausedU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of__coroutines_1() { return static_cast<int32_t>(offsetof(MultiThreadRunner_tC4A17C81CFC215E6A3719AADB82ACFB718255F92, ____coroutines_1)); }
	inline FasterList_1_t2B537C5D221BBAAC4DA80F69541C173562A8478A * get__coroutines_1() const { return ____coroutines_1; }
	inline FasterList_1_t2B537C5D221BBAAC4DA80F69541C173562A8478A ** get_address_of__coroutines_1() { return &____coroutines_1; }
	inline void set__coroutines_1(FasterList_1_t2B537C5D221BBAAC4DA80F69541C173562A8478A * value)
	{
		____coroutines_1 = value;
		Il2CppCodeGenWriteBarrier((&____coroutines_1), value);
	}

	inline static int32_t get_offset_of__newTaskRoutines_2() { return static_cast<int32_t>(offsetof(MultiThreadRunner_tC4A17C81CFC215E6A3719AADB82ACFB718255F92, ____newTaskRoutines_2)); }
	inline ThreadSafeQueue_1_t4B42DDB151ED238066A071C89AF1FC3DB9C1BE45 * get__newTaskRoutines_2() const { return ____newTaskRoutines_2; }
	inline ThreadSafeQueue_1_t4B42DDB151ED238066A071C89AF1FC3DB9C1BE45 ** get_address_of__newTaskRoutines_2() { return &____newTaskRoutines_2; }
	inline void set__newTaskRoutines_2(ThreadSafeQueue_1_t4B42DDB151ED238066A071C89AF1FC3DB9C1BE45 * value)
	{
		____newTaskRoutines_2 = value;
		Il2CppCodeGenWriteBarrier((&____newTaskRoutines_2), value);
	}

	inline static int32_t get_offset_of__name_3() { return static_cast<int32_t>(offsetof(MultiThreadRunner_tC4A17C81CFC215E6A3719AADB82ACFB718255F92, ____name_3)); }
	inline String_t* get__name_3() const { return ____name_3; }
	inline String_t** get_address_of__name_3() { return &____name_3; }
	inline void set__name_3(String_t* value)
	{
		____name_3 = value;
		Il2CppCodeGenWriteBarrier((&____name_3), value);
	}

	inline static int32_t get_offset_of__interlock_4() { return static_cast<int32_t>(offsetof(MultiThreadRunner_tC4A17C81CFC215E6A3719AADB82ACFB718255F92, ____interlock_4)); }
	inline int32_t get__interlock_4() const { return ____interlock_4; }
	inline int32_t* get_address_of__interlock_4() { return &____interlock_4; }
	inline void set__interlock_4(int32_t value)
	{
		____interlock_4 = value;
	}

	inline static int32_t get_offset_of__isAlive_5() { return static_cast<int32_t>(offsetof(MultiThreadRunner_tC4A17C81CFC215E6A3719AADB82ACFB718255F92, ____isAlive_5)); }
	inline bool get__isAlive_5() const { return ____isAlive_5; }
	inline bool* get_address_of__isAlive_5() { return &____isAlive_5; }
	inline void set__isAlive_5(bool value)
	{
		____isAlive_5 = value;
	}

	inline static int32_t get_offset_of__waitForflush_6() { return static_cast<int32_t>(offsetof(MultiThreadRunner_tC4A17C81CFC215E6A3719AADB82ACFB718255F92, ____waitForflush_6)); }
	inline bool get__waitForflush_6() const { return ____waitForflush_6; }
	inline bool* get_address_of__waitForflush_6() { return &____waitForflush_6; }
	inline void set__waitForflush_6(bool value)
	{
		____waitForflush_6 = value;
	}

	inline static int32_t get_offset_of__breakThread_7() { return static_cast<int32_t>(offsetof(MultiThreadRunner_tC4A17C81CFC215E6A3719AADB82ACFB718255F92, ____breakThread_7)); }
	inline bool get__breakThread_7() const { return ____breakThread_7; }
	inline bool* get_address_of__breakThread_7() { return &____breakThread_7; }
	inline void set__breakThread_7(bool value)
	{
		____breakThread_7 = value;
	}

	inline static int32_t get_offset_of__mevent_8() { return static_cast<int32_t>(offsetof(MultiThreadRunner_tC4A17C81CFC215E6A3719AADB82ACFB718255F92, ____mevent_8)); }
	inline ManualResetEventEx_t33E2A70F227B45E36C283FE859544123EC73BB78 * get__mevent_8() const { return ____mevent_8; }
	inline ManualResetEventEx_t33E2A70F227B45E36C283FE859544123EC73BB78 ** get_address_of__mevent_8() { return &____mevent_8; }
	inline void set__mevent_8(ManualResetEventEx_t33E2A70F227B45E36C283FE859544123EC73BB78 * value)
	{
		____mevent_8 = value;
		Il2CppCodeGenWriteBarrier((&____mevent_8), value);
	}

	inline static int32_t get_offset_of__lockingMechanism_9() { return static_cast<int32_t>(offsetof(MultiThreadRunner_tC4A17C81CFC215E6A3719AADB82ACFB718255F92, ____lockingMechanism_9)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get__lockingMechanism_9() const { return ____lockingMechanism_9; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of__lockingMechanism_9() { return &____lockingMechanism_9; }
	inline void set__lockingMechanism_9(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		____lockingMechanism_9 = value;
		Il2CppCodeGenWriteBarrier((&____lockingMechanism_9), value);
	}

	inline static int32_t get_offset_of__interval_10() { return static_cast<int32_t>(offsetof(MultiThreadRunner_tC4A17C81CFC215E6A3719AADB82ACFB718255F92, ____interval_10)); }
	inline int32_t get__interval_10() const { return ____interval_10; }
	inline int32_t* get_address_of__interval_10() { return &____interval_10; }
	inline void set__interval_10(int32_t value)
	{
		____interval_10 = value;
	}

	inline static int32_t get_offset_of__watch_11() { return static_cast<int32_t>(offsetof(MultiThreadRunner_tC4A17C81CFC215E6A3719AADB82ACFB718255F92, ____watch_11)); }
	inline Stopwatch_t0778B5C8DF8FE1D87FC57A2411DA695850BD64D4 * get__watch_11() const { return ____watch_11; }
	inline Stopwatch_t0778B5C8DF8FE1D87FC57A2411DA695850BD64D4 ** get_address_of__watch_11() { return &____watch_11; }
	inline void set__watch_11(Stopwatch_t0778B5C8DF8FE1D87FC57A2411DA695850BD64D4 * value)
	{
		____watch_11 = value;
		Il2CppCodeGenWriteBarrier((&____watch_11), value);
	}

	inline static int32_t get_offset_of__onThreadKilled_12() { return static_cast<int32_t>(offsetof(MultiThreadRunner_tC4A17C81CFC215E6A3719AADB82ACFB718255F92, ____onThreadKilled_12)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get__onThreadKilled_12() const { return ____onThreadKilled_12; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of__onThreadKilled_12() { return &____onThreadKilled_12; }
	inline void set__onThreadKilled_12(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		____onThreadKilled_12 = value;
		Il2CppCodeGenWriteBarrier((&____onThreadKilled_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTITHREADRUNNER_TC4A17C81CFC215E6A3719AADB82ACFB718255F92_H
#ifndef MULTITHREADEDPARALLELTASKCOLLECTION_TAB10527D86B69A23BF4EAD4E4467524993005D12_H
#define MULTITHREADEDPARALLELTASKCOLLECTION_TAB10527D86B69A23BF4EAD4E4467524993005D12_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Svelto.Tasks.MultiThreadedParallelTaskCollection
struct  MultiThreadedParallelTaskCollection_tAB10527D86B69A23BF4EAD4E4467524993005D12  : public RuntimeObject
{
public:
	// System.Action Svelto.Tasks.MultiThreadedParallelTaskCollection::onComplete
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___onComplete_0;
	// System.Boolean Svelto.Tasks.MultiThreadedParallelTaskCollection::<isRunning>k__BackingField
	bool ___U3CisRunningU3Ek__BackingField_1;
	// Svelto.Tasks.MultiThreadRunner[] Svelto.Tasks.MultiThreadedParallelTaskCollection::_runners
	MultiThreadRunnerU5BU5D_t51722EA373DB40FD79EC225511635908DD497C52* ____runners_2;
	// Svelto.Tasks.ParallelTaskCollection[] Svelto.Tasks.MultiThreadedParallelTaskCollection::_parallelTasks
	ParallelTaskCollectionU5BU5D_tF1DED26D88B5622CF291D46EFBA6B774870A6161* ____parallelTasks_3;
	// Svelto.Tasks.ITaskRoutine[] Svelto.Tasks.MultiThreadedParallelTaskCollection::_taskRoutines
	ITaskRoutineU5BU5D_tB6D6BF001B80D26410DC0893D3B2B883DB81969E* ____taskRoutines_4;
	// System.Int32 Svelto.Tasks.MultiThreadedParallelTaskCollection::_numberOfTasksAdded
	int32_t ____numberOfTasksAdded_5;
	// System.Int32 Svelto.Tasks.MultiThreadedParallelTaskCollection::_numberOfConcurrentOperationsToRun
	int32_t ____numberOfConcurrentOperationsToRun_6;
	// System.Int32 Svelto.Tasks.MultiThreadedParallelTaskCollection::_counter
	int32_t ____counter_7;
	// System.Int32 Svelto.Tasks.MultiThreadedParallelTaskCollection::_disposingThreads
	int32_t ____disposingThreads_8;
	// System.Int32 Svelto.Tasks.MultiThreadedParallelTaskCollection::_stoppingThreads
	int32_t ____stoppingThreads_9;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) Svelto.Tasks.MultiThreadedParallelTaskCollection::_isDisposing
	bool ____isDisposing_10;

public:
	inline static int32_t get_offset_of_onComplete_0() { return static_cast<int32_t>(offsetof(MultiThreadedParallelTaskCollection_tAB10527D86B69A23BF4EAD4E4467524993005D12, ___onComplete_0)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_onComplete_0() const { return ___onComplete_0; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_onComplete_0() { return &___onComplete_0; }
	inline void set_onComplete_0(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___onComplete_0 = value;
		Il2CppCodeGenWriteBarrier((&___onComplete_0), value);
	}

	inline static int32_t get_offset_of_U3CisRunningU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(MultiThreadedParallelTaskCollection_tAB10527D86B69A23BF4EAD4E4467524993005D12, ___U3CisRunningU3Ek__BackingField_1)); }
	inline bool get_U3CisRunningU3Ek__BackingField_1() const { return ___U3CisRunningU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CisRunningU3Ek__BackingField_1() { return &___U3CisRunningU3Ek__BackingField_1; }
	inline void set_U3CisRunningU3Ek__BackingField_1(bool value)
	{
		___U3CisRunningU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of__runners_2() { return static_cast<int32_t>(offsetof(MultiThreadedParallelTaskCollection_tAB10527D86B69A23BF4EAD4E4467524993005D12, ____runners_2)); }
	inline MultiThreadRunnerU5BU5D_t51722EA373DB40FD79EC225511635908DD497C52* get__runners_2() const { return ____runners_2; }
	inline MultiThreadRunnerU5BU5D_t51722EA373DB40FD79EC225511635908DD497C52** get_address_of__runners_2() { return &____runners_2; }
	inline void set__runners_2(MultiThreadRunnerU5BU5D_t51722EA373DB40FD79EC225511635908DD497C52* value)
	{
		____runners_2 = value;
		Il2CppCodeGenWriteBarrier((&____runners_2), value);
	}

	inline static int32_t get_offset_of__parallelTasks_3() { return static_cast<int32_t>(offsetof(MultiThreadedParallelTaskCollection_tAB10527D86B69A23BF4EAD4E4467524993005D12, ____parallelTasks_3)); }
	inline ParallelTaskCollectionU5BU5D_tF1DED26D88B5622CF291D46EFBA6B774870A6161* get__parallelTasks_3() const { return ____parallelTasks_3; }
	inline ParallelTaskCollectionU5BU5D_tF1DED26D88B5622CF291D46EFBA6B774870A6161** get_address_of__parallelTasks_3() { return &____parallelTasks_3; }
	inline void set__parallelTasks_3(ParallelTaskCollectionU5BU5D_tF1DED26D88B5622CF291D46EFBA6B774870A6161* value)
	{
		____parallelTasks_3 = value;
		Il2CppCodeGenWriteBarrier((&____parallelTasks_3), value);
	}

	inline static int32_t get_offset_of__taskRoutines_4() { return static_cast<int32_t>(offsetof(MultiThreadedParallelTaskCollection_tAB10527D86B69A23BF4EAD4E4467524993005D12, ____taskRoutines_4)); }
	inline ITaskRoutineU5BU5D_tB6D6BF001B80D26410DC0893D3B2B883DB81969E* get__taskRoutines_4() const { return ____taskRoutines_4; }
	inline ITaskRoutineU5BU5D_tB6D6BF001B80D26410DC0893D3B2B883DB81969E** get_address_of__taskRoutines_4() { return &____taskRoutines_4; }
	inline void set__taskRoutines_4(ITaskRoutineU5BU5D_tB6D6BF001B80D26410DC0893D3B2B883DB81969E* value)
	{
		____taskRoutines_4 = value;
		Il2CppCodeGenWriteBarrier((&____taskRoutines_4), value);
	}

	inline static int32_t get_offset_of__numberOfTasksAdded_5() { return static_cast<int32_t>(offsetof(MultiThreadedParallelTaskCollection_tAB10527D86B69A23BF4EAD4E4467524993005D12, ____numberOfTasksAdded_5)); }
	inline int32_t get__numberOfTasksAdded_5() const { return ____numberOfTasksAdded_5; }
	inline int32_t* get_address_of__numberOfTasksAdded_5() { return &____numberOfTasksAdded_5; }
	inline void set__numberOfTasksAdded_5(int32_t value)
	{
		____numberOfTasksAdded_5 = value;
	}

	inline static int32_t get_offset_of__numberOfConcurrentOperationsToRun_6() { return static_cast<int32_t>(offsetof(MultiThreadedParallelTaskCollection_tAB10527D86B69A23BF4EAD4E4467524993005D12, ____numberOfConcurrentOperationsToRun_6)); }
	inline int32_t get__numberOfConcurrentOperationsToRun_6() const { return ____numberOfConcurrentOperationsToRun_6; }
	inline int32_t* get_address_of__numberOfConcurrentOperationsToRun_6() { return &____numberOfConcurrentOperationsToRun_6; }
	inline void set__numberOfConcurrentOperationsToRun_6(int32_t value)
	{
		____numberOfConcurrentOperationsToRun_6 = value;
	}

	inline static int32_t get_offset_of__counter_7() { return static_cast<int32_t>(offsetof(MultiThreadedParallelTaskCollection_tAB10527D86B69A23BF4EAD4E4467524993005D12, ____counter_7)); }
	inline int32_t get__counter_7() const { return ____counter_7; }
	inline int32_t* get_address_of__counter_7() { return &____counter_7; }
	inline void set__counter_7(int32_t value)
	{
		____counter_7 = value;
	}

	inline static int32_t get_offset_of__disposingThreads_8() { return static_cast<int32_t>(offsetof(MultiThreadedParallelTaskCollection_tAB10527D86B69A23BF4EAD4E4467524993005D12, ____disposingThreads_8)); }
	inline int32_t get__disposingThreads_8() const { return ____disposingThreads_8; }
	inline int32_t* get_address_of__disposingThreads_8() { return &____disposingThreads_8; }
	inline void set__disposingThreads_8(int32_t value)
	{
		____disposingThreads_8 = value;
	}

	inline static int32_t get_offset_of__stoppingThreads_9() { return static_cast<int32_t>(offsetof(MultiThreadedParallelTaskCollection_tAB10527D86B69A23BF4EAD4E4467524993005D12, ____stoppingThreads_9)); }
	inline int32_t get__stoppingThreads_9() const { return ____stoppingThreads_9; }
	inline int32_t* get_address_of__stoppingThreads_9() { return &____stoppingThreads_9; }
	inline void set__stoppingThreads_9(int32_t value)
	{
		____stoppingThreads_9 = value;
	}

	inline static int32_t get_offset_of__isDisposing_10() { return static_cast<int32_t>(offsetof(MultiThreadedParallelTaskCollection_tAB10527D86B69A23BF4EAD4E4467524993005D12, ____isDisposing_10)); }
	inline bool get__isDisposing_10() const { return ____isDisposing_10; }
	inline bool* get_address_of__isDisposing_10() { return &____isDisposing_10; }
	inline void set__isDisposing_10(bool value)
	{
		____isDisposing_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTITHREADEDPARALLELTASKCOLLECTION_TAB10527D86B69A23BF4EAD4E4467524993005D12_H
#ifndef TIMEDLOOPACTIONENUMERATOR_T38CCD290BCE439D8D805B8838A26D2FFA881E673_H
#define TIMEDLOOPACTIONENUMERATOR_T38CCD290BCE439D8D805B8838A26D2FFA881E673_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Svelto.Tasks.TimedLoopActionEnumerator
struct  TimedLoopActionEnumerator_t38CCD290BCE439D8D805B8838A26D2FFA881E673  : public RuntimeObject
{
public:
	// System.Action`1<System.Single> Svelto.Tasks.TimedLoopActionEnumerator::_action
	Action_1_tCBF7F8C203F735692364E5850B0A960E0EABD03B * ____action_0;
	// System.DateTime Svelto.Tasks.TimedLoopActionEnumerator::_then
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ____then_1;

public:
	inline static int32_t get_offset_of__action_0() { return static_cast<int32_t>(offsetof(TimedLoopActionEnumerator_t38CCD290BCE439D8D805B8838A26D2FFA881E673, ____action_0)); }
	inline Action_1_tCBF7F8C203F735692364E5850B0A960E0EABD03B * get__action_0() const { return ____action_0; }
	inline Action_1_tCBF7F8C203F735692364E5850B0A960E0EABD03B ** get_address_of__action_0() { return &____action_0; }
	inline void set__action_0(Action_1_tCBF7F8C203F735692364E5850B0A960E0EABD03B * value)
	{
		____action_0 = value;
		Il2CppCodeGenWriteBarrier((&____action_0), value);
	}

	inline static int32_t get_offset_of__then_1() { return static_cast<int32_t>(offsetof(TimedLoopActionEnumerator_t38CCD290BCE439D8D805B8838A26D2FFA881E673, ____then_1)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get__then_1() const { return ____then_1; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of__then_1() { return &____then_1; }
	inline void set__then_1(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		____then_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMEDLOOPACTIONENUMERATOR_T38CCD290BCE439D8D805B8838A26D2FFA881E673_H
#ifndef U3CU3EC__DISPLAYCLASS13_0_T41DB50CD101C7F4552F773579AFE24F88D55FB96_H
#define U3CU3EC__DISPLAYCLASS13_0_T41DB50CD101C7F4552F773579AFE24F88D55FB96_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayx.Graphy.CustomizationScene.G_CUIColorPicker_<>c__DisplayClass13_0
struct  U3CU3Ec__DisplayClass13_0_t41DB50CD101C7F4552F773579AFE24F88D55FB96  : public RuntimeObject
{
public:
	// UnityEngine.Texture2D Tayx.Graphy.CustomizationScene.G_CUIColorPicker_<>c__DisplayClass13_0::satvalTex
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___satvalTex_0;
	// UnityEngine.Color[] Tayx.Graphy.CustomizationScene.G_CUIColorPicker_<>c__DisplayClass13_0::satvalColors
	ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* ___satvalColors_1;
	// System.Single Tayx.Graphy.CustomizationScene.G_CUIColorPicker_<>c__DisplayClass13_0::Hue
	float ___Hue_2;
	// UnityEngine.Color[] Tayx.Graphy.CustomizationScene.G_CUIColorPicker_<>c__DisplayClass13_0::hueColors
	ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* ___hueColors_3;
	// System.Action Tayx.Graphy.CustomizationScene.G_CUIColorPicker_<>c__DisplayClass13_0::resetSatValTexture
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___resetSatValTexture_4;
	// System.Single Tayx.Graphy.CustomizationScene.G_CUIColorPicker_<>c__DisplayClass13_0::Saturation
	float ___Saturation_5;
	// System.Single Tayx.Graphy.CustomizationScene.G_CUIColorPicker_<>c__DisplayClass13_0::Value
	float ___Value_6;
	// UnityEngine.GameObject Tayx.Graphy.CustomizationScene.G_CUIColorPicker_<>c__DisplayClass13_0::result
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___result_7;
	// Tayx.Graphy.CustomizationScene.G_CUIColorPicker Tayx.Graphy.CustomizationScene.G_CUIColorPicker_<>c__DisplayClass13_0::<>4__this
	G_CUIColorPicker_t65243BB7AF1C8840635414A3F44F39FCBB1B3D83 * ___U3CU3E4__this_8;
	// UnityEngine.GameObject Tayx.Graphy.CustomizationScene.G_CUIColorPicker_<>c__DisplayClass13_0::hueGO
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___hueGO_9;
	// System.Action Tayx.Graphy.CustomizationScene.G_CUIColorPicker_<>c__DisplayClass13_0::dragH
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___dragH_10;
	// UnityEngine.GameObject Tayx.Graphy.CustomizationScene.G_CUIColorPicker_<>c__DisplayClass13_0::satvalGO
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___satvalGO_11;
	// System.Action Tayx.Graphy.CustomizationScene.G_CUIColorPicker_<>c__DisplayClass13_0::dragSV
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___dragSV_12;
	// UnityEngine.Vector2 Tayx.Graphy.CustomizationScene.G_CUIColorPicker_<>c__DisplayClass13_0::hueSz
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___hueSz_13;
	// System.Action Tayx.Graphy.CustomizationScene.G_CUIColorPicker_<>c__DisplayClass13_0::applyHue
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___applyHue_14;
	// System.Action Tayx.Graphy.CustomizationScene.G_CUIColorPicker_<>c__DisplayClass13_0::applySaturationValue
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___applySaturationValue_15;
	// UnityEngine.GameObject Tayx.Graphy.CustomizationScene.G_CUIColorPicker_<>c__DisplayClass13_0::hueKnob
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___hueKnob_16;
	// System.Action Tayx.Graphy.CustomizationScene.G_CUIColorPicker_<>c__DisplayClass13_0::idle
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___idle_17;
	// UnityEngine.Vector2 Tayx.Graphy.CustomizationScene.G_CUIColorPicker_<>c__DisplayClass13_0::satvalSz
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___satvalSz_18;
	// UnityEngine.GameObject Tayx.Graphy.CustomizationScene.G_CUIColorPicker_<>c__DisplayClass13_0::satvalKnob
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___satvalKnob_19;

public:
	inline static int32_t get_offset_of_satvalTex_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass13_0_t41DB50CD101C7F4552F773579AFE24F88D55FB96, ___satvalTex_0)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_satvalTex_0() const { return ___satvalTex_0; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_satvalTex_0() { return &___satvalTex_0; }
	inline void set_satvalTex_0(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___satvalTex_0 = value;
		Il2CppCodeGenWriteBarrier((&___satvalTex_0), value);
	}

	inline static int32_t get_offset_of_satvalColors_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass13_0_t41DB50CD101C7F4552F773579AFE24F88D55FB96, ___satvalColors_1)); }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* get_satvalColors_1() const { return ___satvalColors_1; }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399** get_address_of_satvalColors_1() { return &___satvalColors_1; }
	inline void set_satvalColors_1(ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* value)
	{
		___satvalColors_1 = value;
		Il2CppCodeGenWriteBarrier((&___satvalColors_1), value);
	}

	inline static int32_t get_offset_of_Hue_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass13_0_t41DB50CD101C7F4552F773579AFE24F88D55FB96, ___Hue_2)); }
	inline float get_Hue_2() const { return ___Hue_2; }
	inline float* get_address_of_Hue_2() { return &___Hue_2; }
	inline void set_Hue_2(float value)
	{
		___Hue_2 = value;
	}

	inline static int32_t get_offset_of_hueColors_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass13_0_t41DB50CD101C7F4552F773579AFE24F88D55FB96, ___hueColors_3)); }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* get_hueColors_3() const { return ___hueColors_3; }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399** get_address_of_hueColors_3() { return &___hueColors_3; }
	inline void set_hueColors_3(ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* value)
	{
		___hueColors_3 = value;
		Il2CppCodeGenWriteBarrier((&___hueColors_3), value);
	}

	inline static int32_t get_offset_of_resetSatValTexture_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass13_0_t41DB50CD101C7F4552F773579AFE24F88D55FB96, ___resetSatValTexture_4)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_resetSatValTexture_4() const { return ___resetSatValTexture_4; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_resetSatValTexture_4() { return &___resetSatValTexture_4; }
	inline void set_resetSatValTexture_4(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___resetSatValTexture_4 = value;
		Il2CppCodeGenWriteBarrier((&___resetSatValTexture_4), value);
	}

	inline static int32_t get_offset_of_Saturation_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass13_0_t41DB50CD101C7F4552F773579AFE24F88D55FB96, ___Saturation_5)); }
	inline float get_Saturation_5() const { return ___Saturation_5; }
	inline float* get_address_of_Saturation_5() { return &___Saturation_5; }
	inline void set_Saturation_5(float value)
	{
		___Saturation_5 = value;
	}

	inline static int32_t get_offset_of_Value_6() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass13_0_t41DB50CD101C7F4552F773579AFE24F88D55FB96, ___Value_6)); }
	inline float get_Value_6() const { return ___Value_6; }
	inline float* get_address_of_Value_6() { return &___Value_6; }
	inline void set_Value_6(float value)
	{
		___Value_6 = value;
	}

	inline static int32_t get_offset_of_result_7() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass13_0_t41DB50CD101C7F4552F773579AFE24F88D55FB96, ___result_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_result_7() const { return ___result_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_result_7() { return &___result_7; }
	inline void set_result_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___result_7 = value;
		Il2CppCodeGenWriteBarrier((&___result_7), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_8() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass13_0_t41DB50CD101C7F4552F773579AFE24F88D55FB96, ___U3CU3E4__this_8)); }
	inline G_CUIColorPicker_t65243BB7AF1C8840635414A3F44F39FCBB1B3D83 * get_U3CU3E4__this_8() const { return ___U3CU3E4__this_8; }
	inline G_CUIColorPicker_t65243BB7AF1C8840635414A3F44F39FCBB1B3D83 ** get_address_of_U3CU3E4__this_8() { return &___U3CU3E4__this_8; }
	inline void set_U3CU3E4__this_8(G_CUIColorPicker_t65243BB7AF1C8840635414A3F44F39FCBB1B3D83 * value)
	{
		___U3CU3E4__this_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_8), value);
	}

	inline static int32_t get_offset_of_hueGO_9() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass13_0_t41DB50CD101C7F4552F773579AFE24F88D55FB96, ___hueGO_9)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_hueGO_9() const { return ___hueGO_9; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_hueGO_9() { return &___hueGO_9; }
	inline void set_hueGO_9(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___hueGO_9 = value;
		Il2CppCodeGenWriteBarrier((&___hueGO_9), value);
	}

	inline static int32_t get_offset_of_dragH_10() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass13_0_t41DB50CD101C7F4552F773579AFE24F88D55FB96, ___dragH_10)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_dragH_10() const { return ___dragH_10; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_dragH_10() { return &___dragH_10; }
	inline void set_dragH_10(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___dragH_10 = value;
		Il2CppCodeGenWriteBarrier((&___dragH_10), value);
	}

	inline static int32_t get_offset_of_satvalGO_11() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass13_0_t41DB50CD101C7F4552F773579AFE24F88D55FB96, ___satvalGO_11)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_satvalGO_11() const { return ___satvalGO_11; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_satvalGO_11() { return &___satvalGO_11; }
	inline void set_satvalGO_11(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___satvalGO_11 = value;
		Il2CppCodeGenWriteBarrier((&___satvalGO_11), value);
	}

	inline static int32_t get_offset_of_dragSV_12() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass13_0_t41DB50CD101C7F4552F773579AFE24F88D55FB96, ___dragSV_12)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_dragSV_12() const { return ___dragSV_12; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_dragSV_12() { return &___dragSV_12; }
	inline void set_dragSV_12(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___dragSV_12 = value;
		Il2CppCodeGenWriteBarrier((&___dragSV_12), value);
	}

	inline static int32_t get_offset_of_hueSz_13() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass13_0_t41DB50CD101C7F4552F773579AFE24F88D55FB96, ___hueSz_13)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_hueSz_13() const { return ___hueSz_13; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_hueSz_13() { return &___hueSz_13; }
	inline void set_hueSz_13(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___hueSz_13 = value;
	}

	inline static int32_t get_offset_of_applyHue_14() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass13_0_t41DB50CD101C7F4552F773579AFE24F88D55FB96, ___applyHue_14)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_applyHue_14() const { return ___applyHue_14; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_applyHue_14() { return &___applyHue_14; }
	inline void set_applyHue_14(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___applyHue_14 = value;
		Il2CppCodeGenWriteBarrier((&___applyHue_14), value);
	}

	inline static int32_t get_offset_of_applySaturationValue_15() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass13_0_t41DB50CD101C7F4552F773579AFE24F88D55FB96, ___applySaturationValue_15)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_applySaturationValue_15() const { return ___applySaturationValue_15; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_applySaturationValue_15() { return &___applySaturationValue_15; }
	inline void set_applySaturationValue_15(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___applySaturationValue_15 = value;
		Il2CppCodeGenWriteBarrier((&___applySaturationValue_15), value);
	}

	inline static int32_t get_offset_of_hueKnob_16() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass13_0_t41DB50CD101C7F4552F773579AFE24F88D55FB96, ___hueKnob_16)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_hueKnob_16() const { return ___hueKnob_16; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_hueKnob_16() { return &___hueKnob_16; }
	inline void set_hueKnob_16(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___hueKnob_16 = value;
		Il2CppCodeGenWriteBarrier((&___hueKnob_16), value);
	}

	inline static int32_t get_offset_of_idle_17() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass13_0_t41DB50CD101C7F4552F773579AFE24F88D55FB96, ___idle_17)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_idle_17() const { return ___idle_17; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_idle_17() { return &___idle_17; }
	inline void set_idle_17(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___idle_17 = value;
		Il2CppCodeGenWriteBarrier((&___idle_17), value);
	}

	inline static int32_t get_offset_of_satvalSz_18() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass13_0_t41DB50CD101C7F4552F773579AFE24F88D55FB96, ___satvalSz_18)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_satvalSz_18() const { return ___satvalSz_18; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_satvalSz_18() { return &___satvalSz_18; }
	inline void set_satvalSz_18(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___satvalSz_18 = value;
	}

	inline static int32_t get_offset_of_satvalKnob_19() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass13_0_t41DB50CD101C7F4552F773579AFE24F88D55FB96, ___satvalKnob_19)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_satvalKnob_19() const { return ___satvalKnob_19; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_satvalKnob_19() { return &___satvalKnob_19; }
	inline void set_satvalKnob_19(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___satvalKnob_19 = value;
		Il2CppCodeGenWriteBarrier((&___satvalKnob_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS13_0_T41DB50CD101C7F4552F773579AFE24F88D55FB96_H
#ifndef G_GRAPHSHADER_T3D41D924F74863B5AFC8C101907225362954A8D3_H
#define G_GRAPHSHADER_T3D41D924F74863B5AFC8C101907225362954A8D3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayx.Graphy.G_GraphShader
struct  G_GraphShader_t3D41D924F74863B5AFC8C101907225362954A8D3  : public RuntimeObject
{
public:
	// System.Int32 Tayx.Graphy.G_GraphShader::ArrayMaxSize
	int32_t ___ArrayMaxSize_2;
	// System.Single[] Tayx.Graphy.G_GraphShader::Array
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___Array_3;
	// UnityEngine.UI.Image Tayx.Graphy.G_GraphShader::Image
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___Image_4;
	// System.String Tayx.Graphy.G_GraphShader::Name
	String_t* ___Name_5;
	// System.String Tayx.Graphy.G_GraphShader::Name_Length
	String_t* ___Name_Length_6;
	// System.Single Tayx.Graphy.G_GraphShader::Average
	float ___Average_7;
	// System.Int32 Tayx.Graphy.G_GraphShader::averagePropertyId
	int32_t ___averagePropertyId_8;
	// System.Single Tayx.Graphy.G_GraphShader::GoodThreshold
	float ___GoodThreshold_9;
	// System.Single Tayx.Graphy.G_GraphShader::CautionThreshold
	float ___CautionThreshold_10;
	// System.Int32 Tayx.Graphy.G_GraphShader::goodThresholdPropertyId
	int32_t ___goodThresholdPropertyId_11;
	// System.Int32 Tayx.Graphy.G_GraphShader::cautionThresholdPropertyId
	int32_t ___cautionThresholdPropertyId_12;
	// UnityEngine.Color Tayx.Graphy.G_GraphShader::GoodColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___GoodColor_13;
	// UnityEngine.Color Tayx.Graphy.G_GraphShader::CautionColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___CautionColor_14;
	// UnityEngine.Color Tayx.Graphy.G_GraphShader::CriticalColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___CriticalColor_15;
	// System.Int32 Tayx.Graphy.G_GraphShader::goodColorPropertyId
	int32_t ___goodColorPropertyId_16;
	// System.Int32 Tayx.Graphy.G_GraphShader::cautionColorPropertyId
	int32_t ___cautionColorPropertyId_17;
	// System.Int32 Tayx.Graphy.G_GraphShader::criticalColorPropertyId
	int32_t ___criticalColorPropertyId_18;

public:
	inline static int32_t get_offset_of_ArrayMaxSize_2() { return static_cast<int32_t>(offsetof(G_GraphShader_t3D41D924F74863B5AFC8C101907225362954A8D3, ___ArrayMaxSize_2)); }
	inline int32_t get_ArrayMaxSize_2() const { return ___ArrayMaxSize_2; }
	inline int32_t* get_address_of_ArrayMaxSize_2() { return &___ArrayMaxSize_2; }
	inline void set_ArrayMaxSize_2(int32_t value)
	{
		___ArrayMaxSize_2 = value;
	}

	inline static int32_t get_offset_of_Array_3() { return static_cast<int32_t>(offsetof(G_GraphShader_t3D41D924F74863B5AFC8C101907225362954A8D3, ___Array_3)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_Array_3() const { return ___Array_3; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_Array_3() { return &___Array_3; }
	inline void set_Array_3(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___Array_3 = value;
		Il2CppCodeGenWriteBarrier((&___Array_3), value);
	}

	inline static int32_t get_offset_of_Image_4() { return static_cast<int32_t>(offsetof(G_GraphShader_t3D41D924F74863B5AFC8C101907225362954A8D3, ___Image_4)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_Image_4() const { return ___Image_4; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_Image_4() { return &___Image_4; }
	inline void set_Image_4(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___Image_4 = value;
		Il2CppCodeGenWriteBarrier((&___Image_4), value);
	}

	inline static int32_t get_offset_of_Name_5() { return static_cast<int32_t>(offsetof(G_GraphShader_t3D41D924F74863B5AFC8C101907225362954A8D3, ___Name_5)); }
	inline String_t* get_Name_5() const { return ___Name_5; }
	inline String_t** get_address_of_Name_5() { return &___Name_5; }
	inline void set_Name_5(String_t* value)
	{
		___Name_5 = value;
		Il2CppCodeGenWriteBarrier((&___Name_5), value);
	}

	inline static int32_t get_offset_of_Name_Length_6() { return static_cast<int32_t>(offsetof(G_GraphShader_t3D41D924F74863B5AFC8C101907225362954A8D3, ___Name_Length_6)); }
	inline String_t* get_Name_Length_6() const { return ___Name_Length_6; }
	inline String_t** get_address_of_Name_Length_6() { return &___Name_Length_6; }
	inline void set_Name_Length_6(String_t* value)
	{
		___Name_Length_6 = value;
		Il2CppCodeGenWriteBarrier((&___Name_Length_6), value);
	}

	inline static int32_t get_offset_of_Average_7() { return static_cast<int32_t>(offsetof(G_GraphShader_t3D41D924F74863B5AFC8C101907225362954A8D3, ___Average_7)); }
	inline float get_Average_7() const { return ___Average_7; }
	inline float* get_address_of_Average_7() { return &___Average_7; }
	inline void set_Average_7(float value)
	{
		___Average_7 = value;
	}

	inline static int32_t get_offset_of_averagePropertyId_8() { return static_cast<int32_t>(offsetof(G_GraphShader_t3D41D924F74863B5AFC8C101907225362954A8D3, ___averagePropertyId_8)); }
	inline int32_t get_averagePropertyId_8() const { return ___averagePropertyId_8; }
	inline int32_t* get_address_of_averagePropertyId_8() { return &___averagePropertyId_8; }
	inline void set_averagePropertyId_8(int32_t value)
	{
		___averagePropertyId_8 = value;
	}

	inline static int32_t get_offset_of_GoodThreshold_9() { return static_cast<int32_t>(offsetof(G_GraphShader_t3D41D924F74863B5AFC8C101907225362954A8D3, ___GoodThreshold_9)); }
	inline float get_GoodThreshold_9() const { return ___GoodThreshold_9; }
	inline float* get_address_of_GoodThreshold_9() { return &___GoodThreshold_9; }
	inline void set_GoodThreshold_9(float value)
	{
		___GoodThreshold_9 = value;
	}

	inline static int32_t get_offset_of_CautionThreshold_10() { return static_cast<int32_t>(offsetof(G_GraphShader_t3D41D924F74863B5AFC8C101907225362954A8D3, ___CautionThreshold_10)); }
	inline float get_CautionThreshold_10() const { return ___CautionThreshold_10; }
	inline float* get_address_of_CautionThreshold_10() { return &___CautionThreshold_10; }
	inline void set_CautionThreshold_10(float value)
	{
		___CautionThreshold_10 = value;
	}

	inline static int32_t get_offset_of_goodThresholdPropertyId_11() { return static_cast<int32_t>(offsetof(G_GraphShader_t3D41D924F74863B5AFC8C101907225362954A8D3, ___goodThresholdPropertyId_11)); }
	inline int32_t get_goodThresholdPropertyId_11() const { return ___goodThresholdPropertyId_11; }
	inline int32_t* get_address_of_goodThresholdPropertyId_11() { return &___goodThresholdPropertyId_11; }
	inline void set_goodThresholdPropertyId_11(int32_t value)
	{
		___goodThresholdPropertyId_11 = value;
	}

	inline static int32_t get_offset_of_cautionThresholdPropertyId_12() { return static_cast<int32_t>(offsetof(G_GraphShader_t3D41D924F74863B5AFC8C101907225362954A8D3, ___cautionThresholdPropertyId_12)); }
	inline int32_t get_cautionThresholdPropertyId_12() const { return ___cautionThresholdPropertyId_12; }
	inline int32_t* get_address_of_cautionThresholdPropertyId_12() { return &___cautionThresholdPropertyId_12; }
	inline void set_cautionThresholdPropertyId_12(int32_t value)
	{
		___cautionThresholdPropertyId_12 = value;
	}

	inline static int32_t get_offset_of_GoodColor_13() { return static_cast<int32_t>(offsetof(G_GraphShader_t3D41D924F74863B5AFC8C101907225362954A8D3, ___GoodColor_13)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_GoodColor_13() const { return ___GoodColor_13; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_GoodColor_13() { return &___GoodColor_13; }
	inline void set_GoodColor_13(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___GoodColor_13 = value;
	}

	inline static int32_t get_offset_of_CautionColor_14() { return static_cast<int32_t>(offsetof(G_GraphShader_t3D41D924F74863B5AFC8C101907225362954A8D3, ___CautionColor_14)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_CautionColor_14() const { return ___CautionColor_14; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_CautionColor_14() { return &___CautionColor_14; }
	inline void set_CautionColor_14(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___CautionColor_14 = value;
	}

	inline static int32_t get_offset_of_CriticalColor_15() { return static_cast<int32_t>(offsetof(G_GraphShader_t3D41D924F74863B5AFC8C101907225362954A8D3, ___CriticalColor_15)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_CriticalColor_15() const { return ___CriticalColor_15; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_CriticalColor_15() { return &___CriticalColor_15; }
	inline void set_CriticalColor_15(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___CriticalColor_15 = value;
	}

	inline static int32_t get_offset_of_goodColorPropertyId_16() { return static_cast<int32_t>(offsetof(G_GraphShader_t3D41D924F74863B5AFC8C101907225362954A8D3, ___goodColorPropertyId_16)); }
	inline int32_t get_goodColorPropertyId_16() const { return ___goodColorPropertyId_16; }
	inline int32_t* get_address_of_goodColorPropertyId_16() { return &___goodColorPropertyId_16; }
	inline void set_goodColorPropertyId_16(int32_t value)
	{
		___goodColorPropertyId_16 = value;
	}

	inline static int32_t get_offset_of_cautionColorPropertyId_17() { return static_cast<int32_t>(offsetof(G_GraphShader_t3D41D924F74863B5AFC8C101907225362954A8D3, ___cautionColorPropertyId_17)); }
	inline int32_t get_cautionColorPropertyId_17() const { return ___cautionColorPropertyId_17; }
	inline int32_t* get_address_of_cautionColorPropertyId_17() { return &___cautionColorPropertyId_17; }
	inline void set_cautionColorPropertyId_17(int32_t value)
	{
		___cautionColorPropertyId_17 = value;
	}

	inline static int32_t get_offset_of_criticalColorPropertyId_18() { return static_cast<int32_t>(offsetof(G_GraphShader_t3D41D924F74863B5AFC8C101907225362954A8D3, ___criticalColorPropertyId_18)); }
	inline int32_t get_criticalColorPropertyId_18() const { return ___criticalColorPropertyId_18; }
	inline int32_t* get_address_of_criticalColorPropertyId_18() { return &___criticalColorPropertyId_18; }
	inline void set_criticalColorPropertyId_18(int32_t value)
	{
		___criticalColorPropertyId_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // G_GRAPHSHADER_T3D41D924F74863B5AFC8C101907225362954A8D3_H
#ifndef CONDITIONEVALUATION_T0D296B50D90D5D57015600337C2F187306360B41_H
#define CONDITIONEVALUATION_T0D296B50D90D5D57015600337C2F187306360B41_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayx.Graphy.GraphyDebugger_ConditionEvaluation
struct  ConditionEvaluation_t0D296B50D90D5D57015600337C2F187306360B41 
{
public:
	// System.Int32 Tayx.Graphy.GraphyDebugger_ConditionEvaluation::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ConditionEvaluation_t0D296B50D90D5D57015600337C2F187306360B41, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONDITIONEVALUATION_T0D296B50D90D5D57015600337C2F187306360B41_H
#ifndef DEBUGCOMPARER_TCB311680F0549F814BFC2425FB2E8BB8E1B09FB0_H
#define DEBUGCOMPARER_TCB311680F0549F814BFC2425FB2E8BB8E1B09FB0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayx.Graphy.GraphyDebugger_DebugComparer
struct  DebugComparer_tCB311680F0549F814BFC2425FB2E8BB8E1B09FB0 
{
public:
	// System.Int32 Tayx.Graphy.GraphyDebugger_DebugComparer::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebugComparer_tCB311680F0549F814BFC2425FB2E8BB8E1B09FB0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGCOMPARER_TCB311680F0549F814BFC2425FB2E8BB8E1B09FB0_H
#ifndef DEBUGVARIABLE_T2DB5EEB46D7C214A30CC66E93BC7D3C85015B42C_H
#define DEBUGVARIABLE_T2DB5EEB46D7C214A30CC66E93BC7D3C85015B42C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayx.Graphy.GraphyDebugger_DebugVariable
struct  DebugVariable_t2DB5EEB46D7C214A30CC66E93BC7D3C85015B42C 
{
public:
	// System.Int32 Tayx.Graphy.GraphyDebugger_DebugVariable::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebugVariable_t2DB5EEB46D7C214A30CC66E93BC7D3C85015B42C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGVARIABLE_T2DB5EEB46D7C214A30CC66E93BC7D3C85015B42C_H
#ifndef MESSAGETYPE_T8C988B371F31BB759293369DCB06F840CC7071BE_H
#define MESSAGETYPE_T8C988B371F31BB759293369DCB06F840CC7071BE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayx.Graphy.GraphyDebugger_MessageType
struct  MessageType_t8C988B371F31BB759293369DCB06F840CC7071BE 
{
public:
	// System.Int32 Tayx.Graphy.GraphyDebugger_MessageType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MessageType_t8C988B371F31BB759293369DCB06F840CC7071BE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGETYPE_T8C988B371F31BB759293369DCB06F840CC7071BE_H
#ifndef LOOKFORAUDIOLISTENER_T15A9234D069E6D39793B8D9A1F86967B26C2E595_H
#define LOOKFORAUDIOLISTENER_T15A9234D069E6D39793B8D9A1F86967B26C2E595_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayx.Graphy.GraphyManager_LookForAudioListener
struct  LookForAudioListener_t15A9234D069E6D39793B8D9A1F86967B26C2E595 
{
public:
	// System.Int32 Tayx.Graphy.GraphyManager_LookForAudioListener::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LookForAudioListener_t15A9234D069E6D39793B8D9A1F86967B26C2E595, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOOKFORAUDIOLISTENER_T15A9234D069E6D39793B8D9A1F86967B26C2E595_H
#ifndef MODE_T667FFFB5CD98FFF5F5CF160C75A8D5DABDEE01D3_H
#define MODE_T667FFFB5CD98FFF5F5CF160C75A8D5DABDEE01D3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayx.Graphy.GraphyManager_Mode
struct  Mode_t667FFFB5CD98FFF5F5CF160C75A8D5DABDEE01D3 
{
public:
	// System.Int32 Tayx.Graphy.GraphyManager_Mode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Mode_t667FFFB5CD98FFF5F5CF160C75A8D5DABDEE01D3, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T667FFFB5CD98FFF5F5CF160C75A8D5DABDEE01D3_H
#ifndef MODULEPOSITION_T9E6A5FC6F6A46E059894821A6008D4B883B61BF4_H
#define MODULEPOSITION_T9E6A5FC6F6A46E059894821A6008D4B883B61BF4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayx.Graphy.GraphyManager_ModulePosition
struct  ModulePosition_t9E6A5FC6F6A46E059894821A6008D4B883B61BF4 
{
public:
	// System.Int32 Tayx.Graphy.GraphyManager_ModulePosition::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ModulePosition_t9E6A5FC6F6A46E059894821A6008D4B883B61BF4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODULEPOSITION_T9E6A5FC6F6A46E059894821A6008D4B883B61BF4_H
#ifndef MODULEPRESET_TCF6CF54709DEE9D6CDB813678E7D8024F182E5F8_H
#define MODULEPRESET_TCF6CF54709DEE9D6CDB813678E7D8024F182E5F8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayx.Graphy.GraphyManager_ModulePreset
struct  ModulePreset_tCF6CF54709DEE9D6CDB813678E7D8024F182E5F8 
{
public:
	// System.Int32 Tayx.Graphy.GraphyManager_ModulePreset::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ModulePreset_tCF6CF54709DEE9D6CDB813678E7D8024F182E5F8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODULEPRESET_TCF6CF54709DEE9D6CDB813678E7D8024F182E5F8_H
#ifndef MODULESTATE_T86FF08A91528850D39B1CC8D7BF087667469645D_H
#define MODULESTATE_T86FF08A91528850D39B1CC8D7BF087667469645D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayx.Graphy.GraphyManager_ModuleState
struct  ModuleState_t86FF08A91528850D39B1CC8D7BF087667469645D 
{
public:
	// System.Int32 Tayx.Graphy.GraphyManager_ModuleState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ModuleState_t86FF08A91528850D39B1CC8D7BF087667469645D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODULESTATE_T86FF08A91528850D39B1CC8D7BF087667469645D_H
#ifndef MODULETYPE_TEBD56C8E3621C37A842EC97568034D28DFE4AD7F_H
#define MODULETYPE_TEBD56C8E3621C37A842EC97568034D28DFE4AD7F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayx.Graphy.GraphyManager_ModuleType
struct  ModuleType_tEBD56C8E3621C37A842EC97568034D28DFE4AD7F 
{
public:
	// System.Int32 Tayx.Graphy.GraphyManager_ModuleType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ModuleType_tEBD56C8E3621C37A842EC97568034D28DFE4AD7F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODULETYPE_TEBD56C8E3621C37A842EC97568034D28DFE4AD7F_H
#ifndef FFTWINDOW_T6897F3BD1458FB66ADF3C1EA8E1274414AE7C643_H
#define FFTWINDOW_T6897F3BD1458FB66ADF3C1EA8E1274414AE7C643_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.FFTWindow
struct  FFTWindow_t6897F3BD1458FB66ADF3C1EA8E1274414AE7C643 
{
public:
	// System.Int32 UnityEngine.FFTWindow::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FFTWindow_t6897F3BD1458FB66ADF3C1EA8E1274414AE7C643, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FFTWINDOW_T6897F3BD1458FB66ADF3C1EA8E1274414AE7C643_H
#ifndef KEYCODE_TC93EA87C5A6901160B583ADFCD3EF6726570DC3C_H
#define KEYCODE_TC93EA87C5A6901160B583ADFCD3EF6726570DC3C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.KeyCode
struct  KeyCode_tC93EA87C5A6901160B583ADFCD3EF6726570DC3C 
{
public:
	// System.Int32 UnityEngine.KeyCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(KeyCode_tC93EA87C5A6901160B583ADFCD3EF6726570DC3C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYCODE_TC93EA87C5A6901160B583ADFCD3EF6726570DC3C_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef CONSOLE_TE6853A1DDC9A583838FA1AE18B5C7D7103B66FC7_H
#define CONSOLE_TE6853A1DDC9A583838FA1AE18B5C7D7103B66FC7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utility.Console
struct  Console_tE6853A1DDC9A583838FA1AE18B5C7D7103B66FC7  : public RuntimeObject
{
public:

public:
};

struct Console_tE6853A1DDC9A583838FA1AE18B5C7D7103B66FC7_StaticFields
{
public:
	// System.Text.StringBuilder Utility.Console::_stringBuilder
	StringBuilder_t * ____stringBuilder_0;
	// Utility.ILogger Utility.Console::logger
	RuntimeObject* ___logger_1;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) Utility.Console::BatchLog
	bool ___BatchLog_2;
	// System.Action`3<System.Exception,System.String,System.String> Utility.Console::onException
	Action_3_t027A7299AE99097D560366120BFE2FB96A80B743 * ___onException_3;

public:
	inline static int32_t get_offset_of__stringBuilder_0() { return static_cast<int32_t>(offsetof(Console_tE6853A1DDC9A583838FA1AE18B5C7D7103B66FC7_StaticFields, ____stringBuilder_0)); }
	inline StringBuilder_t * get__stringBuilder_0() const { return ____stringBuilder_0; }
	inline StringBuilder_t ** get_address_of__stringBuilder_0() { return &____stringBuilder_0; }
	inline void set__stringBuilder_0(StringBuilder_t * value)
	{
		____stringBuilder_0 = value;
		Il2CppCodeGenWriteBarrier((&____stringBuilder_0), value);
	}

	inline static int32_t get_offset_of_logger_1() { return static_cast<int32_t>(offsetof(Console_tE6853A1DDC9A583838FA1AE18B5C7D7103B66FC7_StaticFields, ___logger_1)); }
	inline RuntimeObject* get_logger_1() const { return ___logger_1; }
	inline RuntimeObject** get_address_of_logger_1() { return &___logger_1; }
	inline void set_logger_1(RuntimeObject* value)
	{
		___logger_1 = value;
		Il2CppCodeGenWriteBarrier((&___logger_1), value);
	}

	inline static int32_t get_offset_of_BatchLog_2() { return static_cast<int32_t>(offsetof(Console_tE6853A1DDC9A583838FA1AE18B5C7D7103B66FC7_StaticFields, ___BatchLog_2)); }
	inline bool get_BatchLog_2() const { return ___BatchLog_2; }
	inline bool* get_address_of_BatchLog_2() { return &___BatchLog_2; }
	inline void set_BatchLog_2(bool value)
	{
		___BatchLog_2 = value;
	}

	inline static int32_t get_offset_of_onException_3() { return static_cast<int32_t>(offsetof(Console_tE6853A1DDC9A583838FA1AE18B5C7D7103B66FC7_StaticFields, ___onException_3)); }
	inline Action_3_t027A7299AE99097D560366120BFE2FB96A80B743 * get_onException_3() const { return ___onException_3; }
	inline Action_3_t027A7299AE99097D560366120BFE2FB96A80B743 ** get_address_of_onException_3() { return &___onException_3; }
	inline void set_onException_3(Action_3_t027A7299AE99097D560366120BFE2FB96A80B743 * value)
	{
		___onException_3 = value;
		Il2CppCodeGenWriteBarrier((&___onException_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSOLE_TE6853A1DDC9A583838FA1AE18B5C7D7103B66FC7_H
#ifndef LOGTYPE_TA9C618A228638545AB29CED55521DAFD43F209DA_H
#define LOGTYPE_TA9C618A228638545AB29CED55521DAFD43F209DA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utility.LogType
struct  LogType_tA9C618A228638545AB29CED55521DAFD43F209DA 
{
public:
	// System.Int32 Utility.LogType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LogType_tA9C618A228638545AB29CED55521DAFD43F209DA, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGTYPE_TA9C618A228638545AB29CED55521DAFD43F209DA_H
#ifndef DEBUGCONDITION_TF8420D6FE8E0424DBBE01BA12DC0D3EDAA503A28_H
#define DEBUGCONDITION_TF8420D6FE8E0424DBBE01BA12DC0D3EDAA503A28_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayx.Graphy.GraphyDebugger_DebugCondition
struct  DebugCondition_tF8420D6FE8E0424DBBE01BA12DC0D3EDAA503A28 
{
public:
	// Tayx.Graphy.GraphyDebugger_DebugVariable Tayx.Graphy.GraphyDebugger_DebugCondition::Variable
	int32_t ___Variable_0;
	// Tayx.Graphy.GraphyDebugger_DebugComparer Tayx.Graphy.GraphyDebugger_DebugCondition::Comparer
	int32_t ___Comparer_1;
	// System.Single Tayx.Graphy.GraphyDebugger_DebugCondition::Value
	float ___Value_2;

public:
	inline static int32_t get_offset_of_Variable_0() { return static_cast<int32_t>(offsetof(DebugCondition_tF8420D6FE8E0424DBBE01BA12DC0D3EDAA503A28, ___Variable_0)); }
	inline int32_t get_Variable_0() const { return ___Variable_0; }
	inline int32_t* get_address_of_Variable_0() { return &___Variable_0; }
	inline void set_Variable_0(int32_t value)
	{
		___Variable_0 = value;
	}

	inline static int32_t get_offset_of_Comparer_1() { return static_cast<int32_t>(offsetof(DebugCondition_tF8420D6FE8E0424DBBE01BA12DC0D3EDAA503A28, ___Comparer_1)); }
	inline int32_t get_Comparer_1() const { return ___Comparer_1; }
	inline int32_t* get_address_of_Comparer_1() { return &___Comparer_1; }
	inline void set_Comparer_1(int32_t value)
	{
		___Comparer_1 = value;
	}

	inline static int32_t get_offset_of_Value_2() { return static_cast<int32_t>(offsetof(DebugCondition_tF8420D6FE8E0424DBBE01BA12DC0D3EDAA503A28, ___Value_2)); }
	inline float get_Value_2() const { return ___Value_2; }
	inline float* get_address_of_Value_2() { return &___Value_2; }
	inline void set_Value_2(float value)
	{
		___Value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGCONDITION_TF8420D6FE8E0424DBBE01BA12DC0D3EDAA503A28_H
#ifndef DEBUGPACKET_T05CCF6D8D66B7CB75FC9F561F62FCA17802CD590_H
#define DEBUGPACKET_T05CCF6D8D66B7CB75FC9F561F62FCA17802CD590_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayx.Graphy.GraphyDebugger_DebugPacket
struct  DebugPacket_t05CCF6D8D66B7CB75FC9F561F62FCA17802CD590  : public RuntimeObject
{
public:
	// System.Boolean Tayx.Graphy.GraphyDebugger_DebugPacket::Active
	bool ___Active_0;
	// System.Int32 Tayx.Graphy.GraphyDebugger_DebugPacket::Id
	int32_t ___Id_1;
	// System.Boolean Tayx.Graphy.GraphyDebugger_DebugPacket::ExecuteOnce
	bool ___ExecuteOnce_2;
	// System.Single Tayx.Graphy.GraphyDebugger_DebugPacket::InitSleepTime
	float ___InitSleepTime_3;
	// System.Single Tayx.Graphy.GraphyDebugger_DebugPacket::ExecuteSleepTime
	float ___ExecuteSleepTime_4;
	// Tayx.Graphy.GraphyDebugger_ConditionEvaluation Tayx.Graphy.GraphyDebugger_DebugPacket::ConditionEvaluation
	int32_t ___ConditionEvaluation_5;
	// System.Collections.Generic.List`1<Tayx.Graphy.GraphyDebugger_DebugCondition> Tayx.Graphy.GraphyDebugger_DebugPacket::DebugConditions
	List_1_t2AE6E2AF8D86C9DAB57BDAC45B5116E9C3E277D8 * ___DebugConditions_6;
	// Tayx.Graphy.GraphyDebugger_MessageType Tayx.Graphy.GraphyDebugger_DebugPacket::MessageType
	int32_t ___MessageType_7;
	// System.String Tayx.Graphy.GraphyDebugger_DebugPacket::Message
	String_t* ___Message_8;
	// System.Boolean Tayx.Graphy.GraphyDebugger_DebugPacket::TakeScreenshot
	bool ___TakeScreenshot_9;
	// System.String Tayx.Graphy.GraphyDebugger_DebugPacket::ScreenshotFileName
	String_t* ___ScreenshotFileName_10;
	// System.Boolean Tayx.Graphy.GraphyDebugger_DebugPacket::DebugBreak
	bool ___DebugBreak_11;
	// UnityEngine.Events.UnityEvent Tayx.Graphy.GraphyDebugger_DebugPacket::UnityEvents
	UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * ___UnityEvents_12;
	// System.Collections.Generic.List`1<System.Action> Tayx.Graphy.GraphyDebugger_DebugPacket::Callbacks
	List_1_t9E7F53A878191B703A76E69CE36AA90A372800E7 * ___Callbacks_13;
	// System.Boolean Tayx.Graphy.GraphyDebugger_DebugPacket::canBeChecked
	bool ___canBeChecked_14;
	// System.Boolean Tayx.Graphy.GraphyDebugger_DebugPacket::executed
	bool ___executed_15;
	// System.Single Tayx.Graphy.GraphyDebugger_DebugPacket::timePassed
	float ___timePassed_16;

public:
	inline static int32_t get_offset_of_Active_0() { return static_cast<int32_t>(offsetof(DebugPacket_t05CCF6D8D66B7CB75FC9F561F62FCA17802CD590, ___Active_0)); }
	inline bool get_Active_0() const { return ___Active_0; }
	inline bool* get_address_of_Active_0() { return &___Active_0; }
	inline void set_Active_0(bool value)
	{
		___Active_0 = value;
	}

	inline static int32_t get_offset_of_Id_1() { return static_cast<int32_t>(offsetof(DebugPacket_t05CCF6D8D66B7CB75FC9F561F62FCA17802CD590, ___Id_1)); }
	inline int32_t get_Id_1() const { return ___Id_1; }
	inline int32_t* get_address_of_Id_1() { return &___Id_1; }
	inline void set_Id_1(int32_t value)
	{
		___Id_1 = value;
	}

	inline static int32_t get_offset_of_ExecuteOnce_2() { return static_cast<int32_t>(offsetof(DebugPacket_t05CCF6D8D66B7CB75FC9F561F62FCA17802CD590, ___ExecuteOnce_2)); }
	inline bool get_ExecuteOnce_2() const { return ___ExecuteOnce_2; }
	inline bool* get_address_of_ExecuteOnce_2() { return &___ExecuteOnce_2; }
	inline void set_ExecuteOnce_2(bool value)
	{
		___ExecuteOnce_2 = value;
	}

	inline static int32_t get_offset_of_InitSleepTime_3() { return static_cast<int32_t>(offsetof(DebugPacket_t05CCF6D8D66B7CB75FC9F561F62FCA17802CD590, ___InitSleepTime_3)); }
	inline float get_InitSleepTime_3() const { return ___InitSleepTime_3; }
	inline float* get_address_of_InitSleepTime_3() { return &___InitSleepTime_3; }
	inline void set_InitSleepTime_3(float value)
	{
		___InitSleepTime_3 = value;
	}

	inline static int32_t get_offset_of_ExecuteSleepTime_4() { return static_cast<int32_t>(offsetof(DebugPacket_t05CCF6D8D66B7CB75FC9F561F62FCA17802CD590, ___ExecuteSleepTime_4)); }
	inline float get_ExecuteSleepTime_4() const { return ___ExecuteSleepTime_4; }
	inline float* get_address_of_ExecuteSleepTime_4() { return &___ExecuteSleepTime_4; }
	inline void set_ExecuteSleepTime_4(float value)
	{
		___ExecuteSleepTime_4 = value;
	}

	inline static int32_t get_offset_of_ConditionEvaluation_5() { return static_cast<int32_t>(offsetof(DebugPacket_t05CCF6D8D66B7CB75FC9F561F62FCA17802CD590, ___ConditionEvaluation_5)); }
	inline int32_t get_ConditionEvaluation_5() const { return ___ConditionEvaluation_5; }
	inline int32_t* get_address_of_ConditionEvaluation_5() { return &___ConditionEvaluation_5; }
	inline void set_ConditionEvaluation_5(int32_t value)
	{
		___ConditionEvaluation_5 = value;
	}

	inline static int32_t get_offset_of_DebugConditions_6() { return static_cast<int32_t>(offsetof(DebugPacket_t05CCF6D8D66B7CB75FC9F561F62FCA17802CD590, ___DebugConditions_6)); }
	inline List_1_t2AE6E2AF8D86C9DAB57BDAC45B5116E9C3E277D8 * get_DebugConditions_6() const { return ___DebugConditions_6; }
	inline List_1_t2AE6E2AF8D86C9DAB57BDAC45B5116E9C3E277D8 ** get_address_of_DebugConditions_6() { return &___DebugConditions_6; }
	inline void set_DebugConditions_6(List_1_t2AE6E2AF8D86C9DAB57BDAC45B5116E9C3E277D8 * value)
	{
		___DebugConditions_6 = value;
		Il2CppCodeGenWriteBarrier((&___DebugConditions_6), value);
	}

	inline static int32_t get_offset_of_MessageType_7() { return static_cast<int32_t>(offsetof(DebugPacket_t05CCF6D8D66B7CB75FC9F561F62FCA17802CD590, ___MessageType_7)); }
	inline int32_t get_MessageType_7() const { return ___MessageType_7; }
	inline int32_t* get_address_of_MessageType_7() { return &___MessageType_7; }
	inline void set_MessageType_7(int32_t value)
	{
		___MessageType_7 = value;
	}

	inline static int32_t get_offset_of_Message_8() { return static_cast<int32_t>(offsetof(DebugPacket_t05CCF6D8D66B7CB75FC9F561F62FCA17802CD590, ___Message_8)); }
	inline String_t* get_Message_8() const { return ___Message_8; }
	inline String_t** get_address_of_Message_8() { return &___Message_8; }
	inline void set_Message_8(String_t* value)
	{
		___Message_8 = value;
		Il2CppCodeGenWriteBarrier((&___Message_8), value);
	}

	inline static int32_t get_offset_of_TakeScreenshot_9() { return static_cast<int32_t>(offsetof(DebugPacket_t05CCF6D8D66B7CB75FC9F561F62FCA17802CD590, ___TakeScreenshot_9)); }
	inline bool get_TakeScreenshot_9() const { return ___TakeScreenshot_9; }
	inline bool* get_address_of_TakeScreenshot_9() { return &___TakeScreenshot_9; }
	inline void set_TakeScreenshot_9(bool value)
	{
		___TakeScreenshot_9 = value;
	}

	inline static int32_t get_offset_of_ScreenshotFileName_10() { return static_cast<int32_t>(offsetof(DebugPacket_t05CCF6D8D66B7CB75FC9F561F62FCA17802CD590, ___ScreenshotFileName_10)); }
	inline String_t* get_ScreenshotFileName_10() const { return ___ScreenshotFileName_10; }
	inline String_t** get_address_of_ScreenshotFileName_10() { return &___ScreenshotFileName_10; }
	inline void set_ScreenshotFileName_10(String_t* value)
	{
		___ScreenshotFileName_10 = value;
		Il2CppCodeGenWriteBarrier((&___ScreenshotFileName_10), value);
	}

	inline static int32_t get_offset_of_DebugBreak_11() { return static_cast<int32_t>(offsetof(DebugPacket_t05CCF6D8D66B7CB75FC9F561F62FCA17802CD590, ___DebugBreak_11)); }
	inline bool get_DebugBreak_11() const { return ___DebugBreak_11; }
	inline bool* get_address_of_DebugBreak_11() { return &___DebugBreak_11; }
	inline void set_DebugBreak_11(bool value)
	{
		___DebugBreak_11 = value;
	}

	inline static int32_t get_offset_of_UnityEvents_12() { return static_cast<int32_t>(offsetof(DebugPacket_t05CCF6D8D66B7CB75FC9F561F62FCA17802CD590, ___UnityEvents_12)); }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * get_UnityEvents_12() const { return ___UnityEvents_12; }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F ** get_address_of_UnityEvents_12() { return &___UnityEvents_12; }
	inline void set_UnityEvents_12(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * value)
	{
		___UnityEvents_12 = value;
		Il2CppCodeGenWriteBarrier((&___UnityEvents_12), value);
	}

	inline static int32_t get_offset_of_Callbacks_13() { return static_cast<int32_t>(offsetof(DebugPacket_t05CCF6D8D66B7CB75FC9F561F62FCA17802CD590, ___Callbacks_13)); }
	inline List_1_t9E7F53A878191B703A76E69CE36AA90A372800E7 * get_Callbacks_13() const { return ___Callbacks_13; }
	inline List_1_t9E7F53A878191B703A76E69CE36AA90A372800E7 ** get_address_of_Callbacks_13() { return &___Callbacks_13; }
	inline void set_Callbacks_13(List_1_t9E7F53A878191B703A76E69CE36AA90A372800E7 * value)
	{
		___Callbacks_13 = value;
		Il2CppCodeGenWriteBarrier((&___Callbacks_13), value);
	}

	inline static int32_t get_offset_of_canBeChecked_14() { return static_cast<int32_t>(offsetof(DebugPacket_t05CCF6D8D66B7CB75FC9F561F62FCA17802CD590, ___canBeChecked_14)); }
	inline bool get_canBeChecked_14() const { return ___canBeChecked_14; }
	inline bool* get_address_of_canBeChecked_14() { return &___canBeChecked_14; }
	inline void set_canBeChecked_14(bool value)
	{
		___canBeChecked_14 = value;
	}

	inline static int32_t get_offset_of_executed_15() { return static_cast<int32_t>(offsetof(DebugPacket_t05CCF6D8D66B7CB75FC9F561F62FCA17802CD590, ___executed_15)); }
	inline bool get_executed_15() const { return ___executed_15; }
	inline bool* get_address_of_executed_15() { return &___executed_15; }
	inline void set_executed_15(bool value)
	{
		___executed_15 = value;
	}

	inline static int32_t get_offset_of_timePassed_16() { return static_cast<int32_t>(offsetof(DebugPacket_t05CCF6D8D66B7CB75FC9F561F62FCA17802CD590, ___timePassed_16)); }
	inline float get_timePassed_16() const { return ___timePassed_16; }
	inline float* get_address_of_timePassed_16() { return &___timePassed_16; }
	inline void set_timePassed_16(float value)
	{
		___timePassed_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGPACKET_T05CCF6D8D66B7CB75FC9F561F62FCA17802CD590_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef RUNNERBEHAVIOUR_T9AE6F5864AD5F25FE25A18838BC70B25D4B70DFD_H
#define RUNNERBEHAVIOUR_T9AE6F5864AD5F25FE25A18838BC70B25D4B70DFD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Svelto.Tasks.Internal.RunnerBehaviour
struct  RunnerBehaviour_t9AE6F5864AD5F25FE25A18838BC70B25D4B70DFD  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNNERBEHAVIOUR_T9AE6F5864AD5F25FE25A18838BC70B25D4B70DFD_H
#ifndef RUNNERBEHAVIOURENDOFFRAME_T62CC360021E4159E5AFD077D2A603BD4450B1555_H
#define RUNNERBEHAVIOURENDOFFRAME_T62CC360021E4159E5AFD077D2A603BD4450B1555_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Svelto.Tasks.Internal.RunnerBehaviourEndOfFrame
struct  RunnerBehaviourEndOfFrame_t62CC360021E4159E5AFD077D2A603BD4450B1555  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Collections.IEnumerator Svelto.Tasks.Internal.RunnerBehaviourEndOfFrame::_mainRoutine
	RuntimeObject* ____mainRoutine_4;
	// UnityEngine.WaitForEndOfFrame Svelto.Tasks.Internal.RunnerBehaviourEndOfFrame::_waitForEndOfFrame
	WaitForEndOfFrame_t75980FB3F246D6AD36A85CA2BFDF8474E5EEBCCA * ____waitForEndOfFrame_5;

public:
	inline static int32_t get_offset_of__mainRoutine_4() { return static_cast<int32_t>(offsetof(RunnerBehaviourEndOfFrame_t62CC360021E4159E5AFD077D2A603BD4450B1555, ____mainRoutine_4)); }
	inline RuntimeObject* get__mainRoutine_4() const { return ____mainRoutine_4; }
	inline RuntimeObject** get_address_of__mainRoutine_4() { return &____mainRoutine_4; }
	inline void set__mainRoutine_4(RuntimeObject* value)
	{
		____mainRoutine_4 = value;
		Il2CppCodeGenWriteBarrier((&____mainRoutine_4), value);
	}

	inline static int32_t get_offset_of__waitForEndOfFrame_5() { return static_cast<int32_t>(offsetof(RunnerBehaviourEndOfFrame_t62CC360021E4159E5AFD077D2A603BD4450B1555, ____waitForEndOfFrame_5)); }
	inline WaitForEndOfFrame_t75980FB3F246D6AD36A85CA2BFDF8474E5EEBCCA * get__waitForEndOfFrame_5() const { return ____waitForEndOfFrame_5; }
	inline WaitForEndOfFrame_t75980FB3F246D6AD36A85CA2BFDF8474E5EEBCCA ** get_address_of__waitForEndOfFrame_5() { return &____waitForEndOfFrame_5; }
	inline void set__waitForEndOfFrame_5(WaitForEndOfFrame_t75980FB3F246D6AD36A85CA2BFDF8474E5EEBCCA * value)
	{
		____waitForEndOfFrame_5 = value;
		Il2CppCodeGenWriteBarrier((&____waitForEndOfFrame_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNNERBEHAVIOURENDOFFRAME_T62CC360021E4159E5AFD077D2A603BD4450B1555_H
#ifndef G_ADVANCEDDATA_T0796834EAEDC1D020D0AF60EB6D8A19A6CB61F6E_H
#define G_ADVANCEDDATA_T0796834EAEDC1D020D0AF60EB6D8A19A6CB61F6E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayx.Graphy.Advanced.G_AdvancedData
struct  G_AdvancedData_t0796834EAEDC1D020D0AF60EB6D8A19A6CB61F6E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Collections.Generic.List`1<UnityEngine.UI.Image> Tayx.Graphy.Advanced.G_AdvancedData::m_backgroundImages
	List_1_t5E6CEE165340A9D74D8BD47B8E6F422DFB7744ED * ___m_backgroundImages_4;
	// UnityEngine.UI.Text Tayx.Graphy.Advanced.G_AdvancedData::m_graphicsDeviceVersionText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___m_graphicsDeviceVersionText_5;
	// UnityEngine.UI.Text Tayx.Graphy.Advanced.G_AdvancedData::m_processorTypeText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___m_processorTypeText_6;
	// UnityEngine.UI.Text Tayx.Graphy.Advanced.G_AdvancedData::m_operatingSystemText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___m_operatingSystemText_7;
	// UnityEngine.UI.Text Tayx.Graphy.Advanced.G_AdvancedData::m_systemMemoryText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___m_systemMemoryText_8;
	// UnityEngine.UI.Text Tayx.Graphy.Advanced.G_AdvancedData::m_graphicsDeviceNameText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___m_graphicsDeviceNameText_9;
	// UnityEngine.UI.Text Tayx.Graphy.Advanced.G_AdvancedData::m_graphicsMemorySizeText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___m_graphicsMemorySizeText_10;
	// UnityEngine.UI.Text Tayx.Graphy.Advanced.G_AdvancedData::m_screenResolutionText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___m_screenResolutionText_11;
	// UnityEngine.UI.Text Tayx.Graphy.Advanced.G_AdvancedData::m_gameWindowResolutionText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___m_gameWindowResolutionText_12;
	// System.Single Tayx.Graphy.Advanced.G_AdvancedData::m_updateRate
	float ___m_updateRate_13;
	// Tayx.Graphy.GraphyManager Tayx.Graphy.Advanced.G_AdvancedData::m_graphyManager
	GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF * ___m_graphyManager_14;
	// UnityEngine.RectTransform Tayx.Graphy.Advanced.G_AdvancedData::m_rectTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_rectTransform_15;
	// System.Single Tayx.Graphy.Advanced.G_AdvancedData::m_deltaTime
	float ___m_deltaTime_16;
	// System.Text.StringBuilder Tayx.Graphy.Advanced.G_AdvancedData::m_sb
	StringBuilder_t * ___m_sb_17;
	// Tayx.Graphy.GraphyManager_ModuleState Tayx.Graphy.Advanced.G_AdvancedData::m_previousModuleState
	int32_t ___m_previousModuleState_18;
	// Tayx.Graphy.GraphyManager_ModuleState Tayx.Graphy.Advanced.G_AdvancedData::m_currentModuleState
	int32_t ___m_currentModuleState_19;
	// System.String[] Tayx.Graphy.Advanced.G_AdvancedData::m_windowStrings
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___m_windowStrings_20;

public:
	inline static int32_t get_offset_of_m_backgroundImages_4() { return static_cast<int32_t>(offsetof(G_AdvancedData_t0796834EAEDC1D020D0AF60EB6D8A19A6CB61F6E, ___m_backgroundImages_4)); }
	inline List_1_t5E6CEE165340A9D74D8BD47B8E6F422DFB7744ED * get_m_backgroundImages_4() const { return ___m_backgroundImages_4; }
	inline List_1_t5E6CEE165340A9D74D8BD47B8E6F422DFB7744ED ** get_address_of_m_backgroundImages_4() { return &___m_backgroundImages_4; }
	inline void set_m_backgroundImages_4(List_1_t5E6CEE165340A9D74D8BD47B8E6F422DFB7744ED * value)
	{
		___m_backgroundImages_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_backgroundImages_4), value);
	}

	inline static int32_t get_offset_of_m_graphicsDeviceVersionText_5() { return static_cast<int32_t>(offsetof(G_AdvancedData_t0796834EAEDC1D020D0AF60EB6D8A19A6CB61F6E, ___m_graphicsDeviceVersionText_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_m_graphicsDeviceVersionText_5() const { return ___m_graphicsDeviceVersionText_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_m_graphicsDeviceVersionText_5() { return &___m_graphicsDeviceVersionText_5; }
	inline void set_m_graphicsDeviceVersionText_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___m_graphicsDeviceVersionText_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_graphicsDeviceVersionText_5), value);
	}

	inline static int32_t get_offset_of_m_processorTypeText_6() { return static_cast<int32_t>(offsetof(G_AdvancedData_t0796834EAEDC1D020D0AF60EB6D8A19A6CB61F6E, ___m_processorTypeText_6)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_m_processorTypeText_6() const { return ___m_processorTypeText_6; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_m_processorTypeText_6() { return &___m_processorTypeText_6; }
	inline void set_m_processorTypeText_6(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___m_processorTypeText_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_processorTypeText_6), value);
	}

	inline static int32_t get_offset_of_m_operatingSystemText_7() { return static_cast<int32_t>(offsetof(G_AdvancedData_t0796834EAEDC1D020D0AF60EB6D8A19A6CB61F6E, ___m_operatingSystemText_7)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_m_operatingSystemText_7() const { return ___m_operatingSystemText_7; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_m_operatingSystemText_7() { return &___m_operatingSystemText_7; }
	inline void set_m_operatingSystemText_7(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___m_operatingSystemText_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_operatingSystemText_7), value);
	}

	inline static int32_t get_offset_of_m_systemMemoryText_8() { return static_cast<int32_t>(offsetof(G_AdvancedData_t0796834EAEDC1D020D0AF60EB6D8A19A6CB61F6E, ___m_systemMemoryText_8)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_m_systemMemoryText_8() const { return ___m_systemMemoryText_8; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_m_systemMemoryText_8() { return &___m_systemMemoryText_8; }
	inline void set_m_systemMemoryText_8(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___m_systemMemoryText_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_systemMemoryText_8), value);
	}

	inline static int32_t get_offset_of_m_graphicsDeviceNameText_9() { return static_cast<int32_t>(offsetof(G_AdvancedData_t0796834EAEDC1D020D0AF60EB6D8A19A6CB61F6E, ___m_graphicsDeviceNameText_9)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_m_graphicsDeviceNameText_9() const { return ___m_graphicsDeviceNameText_9; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_m_graphicsDeviceNameText_9() { return &___m_graphicsDeviceNameText_9; }
	inline void set_m_graphicsDeviceNameText_9(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___m_graphicsDeviceNameText_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_graphicsDeviceNameText_9), value);
	}

	inline static int32_t get_offset_of_m_graphicsMemorySizeText_10() { return static_cast<int32_t>(offsetof(G_AdvancedData_t0796834EAEDC1D020D0AF60EB6D8A19A6CB61F6E, ___m_graphicsMemorySizeText_10)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_m_graphicsMemorySizeText_10() const { return ___m_graphicsMemorySizeText_10; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_m_graphicsMemorySizeText_10() { return &___m_graphicsMemorySizeText_10; }
	inline void set_m_graphicsMemorySizeText_10(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___m_graphicsMemorySizeText_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_graphicsMemorySizeText_10), value);
	}

	inline static int32_t get_offset_of_m_screenResolutionText_11() { return static_cast<int32_t>(offsetof(G_AdvancedData_t0796834EAEDC1D020D0AF60EB6D8A19A6CB61F6E, ___m_screenResolutionText_11)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_m_screenResolutionText_11() const { return ___m_screenResolutionText_11; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_m_screenResolutionText_11() { return &___m_screenResolutionText_11; }
	inline void set_m_screenResolutionText_11(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___m_screenResolutionText_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_screenResolutionText_11), value);
	}

	inline static int32_t get_offset_of_m_gameWindowResolutionText_12() { return static_cast<int32_t>(offsetof(G_AdvancedData_t0796834EAEDC1D020D0AF60EB6D8A19A6CB61F6E, ___m_gameWindowResolutionText_12)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_m_gameWindowResolutionText_12() const { return ___m_gameWindowResolutionText_12; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_m_gameWindowResolutionText_12() { return &___m_gameWindowResolutionText_12; }
	inline void set_m_gameWindowResolutionText_12(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___m_gameWindowResolutionText_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_gameWindowResolutionText_12), value);
	}

	inline static int32_t get_offset_of_m_updateRate_13() { return static_cast<int32_t>(offsetof(G_AdvancedData_t0796834EAEDC1D020D0AF60EB6D8A19A6CB61F6E, ___m_updateRate_13)); }
	inline float get_m_updateRate_13() const { return ___m_updateRate_13; }
	inline float* get_address_of_m_updateRate_13() { return &___m_updateRate_13; }
	inline void set_m_updateRate_13(float value)
	{
		___m_updateRate_13 = value;
	}

	inline static int32_t get_offset_of_m_graphyManager_14() { return static_cast<int32_t>(offsetof(G_AdvancedData_t0796834EAEDC1D020D0AF60EB6D8A19A6CB61F6E, ___m_graphyManager_14)); }
	inline GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF * get_m_graphyManager_14() const { return ___m_graphyManager_14; }
	inline GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF ** get_address_of_m_graphyManager_14() { return &___m_graphyManager_14; }
	inline void set_m_graphyManager_14(GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF * value)
	{
		___m_graphyManager_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_graphyManager_14), value);
	}

	inline static int32_t get_offset_of_m_rectTransform_15() { return static_cast<int32_t>(offsetof(G_AdvancedData_t0796834EAEDC1D020D0AF60EB6D8A19A6CB61F6E, ___m_rectTransform_15)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_rectTransform_15() const { return ___m_rectTransform_15; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_rectTransform_15() { return &___m_rectTransform_15; }
	inline void set_m_rectTransform_15(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_rectTransform_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_rectTransform_15), value);
	}

	inline static int32_t get_offset_of_m_deltaTime_16() { return static_cast<int32_t>(offsetof(G_AdvancedData_t0796834EAEDC1D020D0AF60EB6D8A19A6CB61F6E, ___m_deltaTime_16)); }
	inline float get_m_deltaTime_16() const { return ___m_deltaTime_16; }
	inline float* get_address_of_m_deltaTime_16() { return &___m_deltaTime_16; }
	inline void set_m_deltaTime_16(float value)
	{
		___m_deltaTime_16 = value;
	}

	inline static int32_t get_offset_of_m_sb_17() { return static_cast<int32_t>(offsetof(G_AdvancedData_t0796834EAEDC1D020D0AF60EB6D8A19A6CB61F6E, ___m_sb_17)); }
	inline StringBuilder_t * get_m_sb_17() const { return ___m_sb_17; }
	inline StringBuilder_t ** get_address_of_m_sb_17() { return &___m_sb_17; }
	inline void set_m_sb_17(StringBuilder_t * value)
	{
		___m_sb_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_sb_17), value);
	}

	inline static int32_t get_offset_of_m_previousModuleState_18() { return static_cast<int32_t>(offsetof(G_AdvancedData_t0796834EAEDC1D020D0AF60EB6D8A19A6CB61F6E, ___m_previousModuleState_18)); }
	inline int32_t get_m_previousModuleState_18() const { return ___m_previousModuleState_18; }
	inline int32_t* get_address_of_m_previousModuleState_18() { return &___m_previousModuleState_18; }
	inline void set_m_previousModuleState_18(int32_t value)
	{
		___m_previousModuleState_18 = value;
	}

	inline static int32_t get_offset_of_m_currentModuleState_19() { return static_cast<int32_t>(offsetof(G_AdvancedData_t0796834EAEDC1D020D0AF60EB6D8A19A6CB61F6E, ___m_currentModuleState_19)); }
	inline int32_t get_m_currentModuleState_19() const { return ___m_currentModuleState_19; }
	inline int32_t* get_address_of_m_currentModuleState_19() { return &___m_currentModuleState_19; }
	inline void set_m_currentModuleState_19(int32_t value)
	{
		___m_currentModuleState_19 = value;
	}

	inline static int32_t get_offset_of_m_windowStrings_20() { return static_cast<int32_t>(offsetof(G_AdvancedData_t0796834EAEDC1D020D0AF60EB6D8A19A6CB61F6E, ___m_windowStrings_20)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_m_windowStrings_20() const { return ___m_windowStrings_20; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_m_windowStrings_20() { return &___m_windowStrings_20; }
	inline void set_m_windowStrings_20(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___m_windowStrings_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_windowStrings_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // G_ADVANCEDDATA_T0796834EAEDC1D020D0AF60EB6D8A19A6CB61F6E_H
#ifndef G_AUDIOMANAGER_T19B7806D5B65E31D8B338BF53B13C8ACB6057F60_H
#define G_AUDIOMANAGER_T19B7806D5B65E31D8B338BF53B13C8ACB6057F60_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayx.Graphy.Audio.G_AudioManager
struct  G_AudioManager_t19B7806D5B65E31D8B338BF53B13C8ACB6057F60  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject Tayx.Graphy.Audio.G_AudioManager::m_audioGraphGameObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_audioGraphGameObject_4;
	// UnityEngine.UI.Text Tayx.Graphy.Audio.G_AudioManager::m_audioDbText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___m_audioDbText_5;
	// System.Collections.Generic.List`1<UnityEngine.UI.Image> Tayx.Graphy.Audio.G_AudioManager::m_backgroundImages
	List_1_t5E6CEE165340A9D74D8BD47B8E6F422DFB7744ED * ___m_backgroundImages_6;
	// Tayx.Graphy.GraphyManager Tayx.Graphy.Audio.G_AudioManager::m_graphyManager
	GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF * ___m_graphyManager_7;
	// Tayx.Graphy.Audio.G_AudioGraph Tayx.Graphy.Audio.G_AudioManager::m_audioGraph
	G_AudioGraph_t02349ECE72DF71E24A86159551A347D323CB37CB * ___m_audioGraph_8;
	// Tayx.Graphy.Audio.G_AudioMonitor Tayx.Graphy.Audio.G_AudioManager::m_audioMonitor
	G_AudioMonitor_t386A2D77995058DA7241FC1C90AF9FF6DE38EA2E * ___m_audioMonitor_9;
	// Tayx.Graphy.Audio.G_AudioText Tayx.Graphy.Audio.G_AudioManager::m_audioText
	G_AudioText_t7BCE4D478232F4284809B83E3150DBCC8DC64BF3 * ___m_audioText_10;
	// UnityEngine.RectTransform Tayx.Graphy.Audio.G_AudioManager::m_rectTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_rectTransform_11;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> Tayx.Graphy.Audio.G_AudioManager::m_childrenGameObjects
	List_1_t99909CDEDA6D21189884AEA74B1FD99FC9C6A4C0 * ___m_childrenGameObjects_12;
	// Tayx.Graphy.GraphyManager_ModuleState Tayx.Graphy.Audio.G_AudioManager::m_previousModuleState
	int32_t ___m_previousModuleState_13;
	// Tayx.Graphy.GraphyManager_ModuleState Tayx.Graphy.Audio.G_AudioManager::m_currentModuleState
	int32_t ___m_currentModuleState_14;

public:
	inline static int32_t get_offset_of_m_audioGraphGameObject_4() { return static_cast<int32_t>(offsetof(G_AudioManager_t19B7806D5B65E31D8B338BF53B13C8ACB6057F60, ___m_audioGraphGameObject_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_audioGraphGameObject_4() const { return ___m_audioGraphGameObject_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_audioGraphGameObject_4() { return &___m_audioGraphGameObject_4; }
	inline void set_m_audioGraphGameObject_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_audioGraphGameObject_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_audioGraphGameObject_4), value);
	}

	inline static int32_t get_offset_of_m_audioDbText_5() { return static_cast<int32_t>(offsetof(G_AudioManager_t19B7806D5B65E31D8B338BF53B13C8ACB6057F60, ___m_audioDbText_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_m_audioDbText_5() const { return ___m_audioDbText_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_m_audioDbText_5() { return &___m_audioDbText_5; }
	inline void set_m_audioDbText_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___m_audioDbText_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_audioDbText_5), value);
	}

	inline static int32_t get_offset_of_m_backgroundImages_6() { return static_cast<int32_t>(offsetof(G_AudioManager_t19B7806D5B65E31D8B338BF53B13C8ACB6057F60, ___m_backgroundImages_6)); }
	inline List_1_t5E6CEE165340A9D74D8BD47B8E6F422DFB7744ED * get_m_backgroundImages_6() const { return ___m_backgroundImages_6; }
	inline List_1_t5E6CEE165340A9D74D8BD47B8E6F422DFB7744ED ** get_address_of_m_backgroundImages_6() { return &___m_backgroundImages_6; }
	inline void set_m_backgroundImages_6(List_1_t5E6CEE165340A9D74D8BD47B8E6F422DFB7744ED * value)
	{
		___m_backgroundImages_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_backgroundImages_6), value);
	}

	inline static int32_t get_offset_of_m_graphyManager_7() { return static_cast<int32_t>(offsetof(G_AudioManager_t19B7806D5B65E31D8B338BF53B13C8ACB6057F60, ___m_graphyManager_7)); }
	inline GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF * get_m_graphyManager_7() const { return ___m_graphyManager_7; }
	inline GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF ** get_address_of_m_graphyManager_7() { return &___m_graphyManager_7; }
	inline void set_m_graphyManager_7(GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF * value)
	{
		___m_graphyManager_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_graphyManager_7), value);
	}

	inline static int32_t get_offset_of_m_audioGraph_8() { return static_cast<int32_t>(offsetof(G_AudioManager_t19B7806D5B65E31D8B338BF53B13C8ACB6057F60, ___m_audioGraph_8)); }
	inline G_AudioGraph_t02349ECE72DF71E24A86159551A347D323CB37CB * get_m_audioGraph_8() const { return ___m_audioGraph_8; }
	inline G_AudioGraph_t02349ECE72DF71E24A86159551A347D323CB37CB ** get_address_of_m_audioGraph_8() { return &___m_audioGraph_8; }
	inline void set_m_audioGraph_8(G_AudioGraph_t02349ECE72DF71E24A86159551A347D323CB37CB * value)
	{
		___m_audioGraph_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_audioGraph_8), value);
	}

	inline static int32_t get_offset_of_m_audioMonitor_9() { return static_cast<int32_t>(offsetof(G_AudioManager_t19B7806D5B65E31D8B338BF53B13C8ACB6057F60, ___m_audioMonitor_9)); }
	inline G_AudioMonitor_t386A2D77995058DA7241FC1C90AF9FF6DE38EA2E * get_m_audioMonitor_9() const { return ___m_audioMonitor_9; }
	inline G_AudioMonitor_t386A2D77995058DA7241FC1C90AF9FF6DE38EA2E ** get_address_of_m_audioMonitor_9() { return &___m_audioMonitor_9; }
	inline void set_m_audioMonitor_9(G_AudioMonitor_t386A2D77995058DA7241FC1C90AF9FF6DE38EA2E * value)
	{
		___m_audioMonitor_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_audioMonitor_9), value);
	}

	inline static int32_t get_offset_of_m_audioText_10() { return static_cast<int32_t>(offsetof(G_AudioManager_t19B7806D5B65E31D8B338BF53B13C8ACB6057F60, ___m_audioText_10)); }
	inline G_AudioText_t7BCE4D478232F4284809B83E3150DBCC8DC64BF3 * get_m_audioText_10() const { return ___m_audioText_10; }
	inline G_AudioText_t7BCE4D478232F4284809B83E3150DBCC8DC64BF3 ** get_address_of_m_audioText_10() { return &___m_audioText_10; }
	inline void set_m_audioText_10(G_AudioText_t7BCE4D478232F4284809B83E3150DBCC8DC64BF3 * value)
	{
		___m_audioText_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_audioText_10), value);
	}

	inline static int32_t get_offset_of_m_rectTransform_11() { return static_cast<int32_t>(offsetof(G_AudioManager_t19B7806D5B65E31D8B338BF53B13C8ACB6057F60, ___m_rectTransform_11)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_rectTransform_11() const { return ___m_rectTransform_11; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_rectTransform_11() { return &___m_rectTransform_11; }
	inline void set_m_rectTransform_11(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_rectTransform_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_rectTransform_11), value);
	}

	inline static int32_t get_offset_of_m_childrenGameObjects_12() { return static_cast<int32_t>(offsetof(G_AudioManager_t19B7806D5B65E31D8B338BF53B13C8ACB6057F60, ___m_childrenGameObjects_12)); }
	inline List_1_t99909CDEDA6D21189884AEA74B1FD99FC9C6A4C0 * get_m_childrenGameObjects_12() const { return ___m_childrenGameObjects_12; }
	inline List_1_t99909CDEDA6D21189884AEA74B1FD99FC9C6A4C0 ** get_address_of_m_childrenGameObjects_12() { return &___m_childrenGameObjects_12; }
	inline void set_m_childrenGameObjects_12(List_1_t99909CDEDA6D21189884AEA74B1FD99FC9C6A4C0 * value)
	{
		___m_childrenGameObjects_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_childrenGameObjects_12), value);
	}

	inline static int32_t get_offset_of_m_previousModuleState_13() { return static_cast<int32_t>(offsetof(G_AudioManager_t19B7806D5B65E31D8B338BF53B13C8ACB6057F60, ___m_previousModuleState_13)); }
	inline int32_t get_m_previousModuleState_13() const { return ___m_previousModuleState_13; }
	inline int32_t* get_address_of_m_previousModuleState_13() { return &___m_previousModuleState_13; }
	inline void set_m_previousModuleState_13(int32_t value)
	{
		___m_previousModuleState_13 = value;
	}

	inline static int32_t get_offset_of_m_currentModuleState_14() { return static_cast<int32_t>(offsetof(G_AudioManager_t19B7806D5B65E31D8B338BF53B13C8ACB6057F60, ___m_currentModuleState_14)); }
	inline int32_t get_m_currentModuleState_14() const { return ___m_currentModuleState_14; }
	inline int32_t* get_address_of_m_currentModuleState_14() { return &___m_currentModuleState_14; }
	inline void set_m_currentModuleState_14(int32_t value)
	{
		___m_currentModuleState_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // G_AUDIOMANAGER_T19B7806D5B65E31D8B338BF53B13C8ACB6057F60_H
#ifndef G_AUDIOMONITOR_T386A2D77995058DA7241FC1C90AF9FF6DE38EA2E_H
#define G_AUDIOMONITOR_T386A2D77995058DA7241FC1C90AF9FF6DE38EA2E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayx.Graphy.Audio.G_AudioMonitor
struct  G_AudioMonitor_t386A2D77995058DA7241FC1C90AF9FF6DE38EA2E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Tayx.Graphy.GraphyManager Tayx.Graphy.Audio.G_AudioMonitor::m_graphyManager
	GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF * ___m_graphyManager_5;
	// UnityEngine.AudioListener Tayx.Graphy.Audio.G_AudioMonitor::m_audioListener
	AudioListener_tE3E1467B84A4AFD509947B44A7C8ACFB67FF2099 * ___m_audioListener_6;
	// Tayx.Graphy.GraphyManager_LookForAudioListener Tayx.Graphy.Audio.G_AudioMonitor::m_findAudioListenerInCameraIfNull
	int32_t ___m_findAudioListenerInCameraIfNull_7;
	// UnityEngine.FFTWindow Tayx.Graphy.Audio.G_AudioMonitor::m_FFTWindow
	int32_t ___m_FFTWindow_8;
	// System.Int32 Tayx.Graphy.Audio.G_AudioMonitor::m_spectrumSize
	int32_t ___m_spectrumSize_9;
	// System.Single[] Tayx.Graphy.Audio.G_AudioMonitor::m_spectrum
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___m_spectrum_10;
	// System.Single[] Tayx.Graphy.Audio.G_AudioMonitor::m_spectrumHighestValues
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___m_spectrumHighestValues_11;
	// System.Single Tayx.Graphy.Audio.G_AudioMonitor::m_maxDB
	float ___m_maxDB_12;

public:
	inline static int32_t get_offset_of_m_graphyManager_5() { return static_cast<int32_t>(offsetof(G_AudioMonitor_t386A2D77995058DA7241FC1C90AF9FF6DE38EA2E, ___m_graphyManager_5)); }
	inline GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF * get_m_graphyManager_5() const { return ___m_graphyManager_5; }
	inline GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF ** get_address_of_m_graphyManager_5() { return &___m_graphyManager_5; }
	inline void set_m_graphyManager_5(GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF * value)
	{
		___m_graphyManager_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_graphyManager_5), value);
	}

	inline static int32_t get_offset_of_m_audioListener_6() { return static_cast<int32_t>(offsetof(G_AudioMonitor_t386A2D77995058DA7241FC1C90AF9FF6DE38EA2E, ___m_audioListener_6)); }
	inline AudioListener_tE3E1467B84A4AFD509947B44A7C8ACFB67FF2099 * get_m_audioListener_6() const { return ___m_audioListener_6; }
	inline AudioListener_tE3E1467B84A4AFD509947B44A7C8ACFB67FF2099 ** get_address_of_m_audioListener_6() { return &___m_audioListener_6; }
	inline void set_m_audioListener_6(AudioListener_tE3E1467B84A4AFD509947B44A7C8ACFB67FF2099 * value)
	{
		___m_audioListener_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_audioListener_6), value);
	}

	inline static int32_t get_offset_of_m_findAudioListenerInCameraIfNull_7() { return static_cast<int32_t>(offsetof(G_AudioMonitor_t386A2D77995058DA7241FC1C90AF9FF6DE38EA2E, ___m_findAudioListenerInCameraIfNull_7)); }
	inline int32_t get_m_findAudioListenerInCameraIfNull_7() const { return ___m_findAudioListenerInCameraIfNull_7; }
	inline int32_t* get_address_of_m_findAudioListenerInCameraIfNull_7() { return &___m_findAudioListenerInCameraIfNull_7; }
	inline void set_m_findAudioListenerInCameraIfNull_7(int32_t value)
	{
		___m_findAudioListenerInCameraIfNull_7 = value;
	}

	inline static int32_t get_offset_of_m_FFTWindow_8() { return static_cast<int32_t>(offsetof(G_AudioMonitor_t386A2D77995058DA7241FC1C90AF9FF6DE38EA2E, ___m_FFTWindow_8)); }
	inline int32_t get_m_FFTWindow_8() const { return ___m_FFTWindow_8; }
	inline int32_t* get_address_of_m_FFTWindow_8() { return &___m_FFTWindow_8; }
	inline void set_m_FFTWindow_8(int32_t value)
	{
		___m_FFTWindow_8 = value;
	}

	inline static int32_t get_offset_of_m_spectrumSize_9() { return static_cast<int32_t>(offsetof(G_AudioMonitor_t386A2D77995058DA7241FC1C90AF9FF6DE38EA2E, ___m_spectrumSize_9)); }
	inline int32_t get_m_spectrumSize_9() const { return ___m_spectrumSize_9; }
	inline int32_t* get_address_of_m_spectrumSize_9() { return &___m_spectrumSize_9; }
	inline void set_m_spectrumSize_9(int32_t value)
	{
		___m_spectrumSize_9 = value;
	}

	inline static int32_t get_offset_of_m_spectrum_10() { return static_cast<int32_t>(offsetof(G_AudioMonitor_t386A2D77995058DA7241FC1C90AF9FF6DE38EA2E, ___m_spectrum_10)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_m_spectrum_10() const { return ___m_spectrum_10; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_m_spectrum_10() { return &___m_spectrum_10; }
	inline void set_m_spectrum_10(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___m_spectrum_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_spectrum_10), value);
	}

	inline static int32_t get_offset_of_m_spectrumHighestValues_11() { return static_cast<int32_t>(offsetof(G_AudioMonitor_t386A2D77995058DA7241FC1C90AF9FF6DE38EA2E, ___m_spectrumHighestValues_11)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_m_spectrumHighestValues_11() const { return ___m_spectrumHighestValues_11; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_m_spectrumHighestValues_11() { return &___m_spectrumHighestValues_11; }
	inline void set_m_spectrumHighestValues_11(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___m_spectrumHighestValues_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_spectrumHighestValues_11), value);
	}

	inline static int32_t get_offset_of_m_maxDB_12() { return static_cast<int32_t>(offsetof(G_AudioMonitor_t386A2D77995058DA7241FC1C90AF9FF6DE38EA2E, ___m_maxDB_12)); }
	inline float get_m_maxDB_12() const { return ___m_maxDB_12; }
	inline float* get_address_of_m_maxDB_12() { return &___m_maxDB_12; }
	inline void set_m_maxDB_12(float value)
	{
		___m_maxDB_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // G_AUDIOMONITOR_T386A2D77995058DA7241FC1C90AF9FF6DE38EA2E_H
#ifndef G_AUDIOTEXT_T7BCE4D478232F4284809B83E3150DBCC8DC64BF3_H
#define G_AUDIOTEXT_T7BCE4D478232F4284809B83E3150DBCC8DC64BF3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayx.Graphy.Audio.G_AudioText
struct  G_AudioText_t7BCE4D478232F4284809B83E3150DBCC8DC64BF3  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text Tayx.Graphy.Audio.G_AudioText::m_DBText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___m_DBText_4;
	// Tayx.Graphy.GraphyManager Tayx.Graphy.Audio.G_AudioText::m_graphyManager
	GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF * ___m_graphyManager_5;
	// Tayx.Graphy.Audio.G_AudioMonitor Tayx.Graphy.Audio.G_AudioText::m_audioMonitor
	G_AudioMonitor_t386A2D77995058DA7241FC1C90AF9FF6DE38EA2E * ___m_audioMonitor_6;
	// System.Int32 Tayx.Graphy.Audio.G_AudioText::m_updateRate
	int32_t ___m_updateRate_7;
	// System.Single Tayx.Graphy.Audio.G_AudioText::m_deltaTimeOffset
	float ___m_deltaTimeOffset_8;

public:
	inline static int32_t get_offset_of_m_DBText_4() { return static_cast<int32_t>(offsetof(G_AudioText_t7BCE4D478232F4284809B83E3150DBCC8DC64BF3, ___m_DBText_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_m_DBText_4() const { return ___m_DBText_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_m_DBText_4() { return &___m_DBText_4; }
	inline void set_m_DBText_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___m_DBText_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_DBText_4), value);
	}

	inline static int32_t get_offset_of_m_graphyManager_5() { return static_cast<int32_t>(offsetof(G_AudioText_t7BCE4D478232F4284809B83E3150DBCC8DC64BF3, ___m_graphyManager_5)); }
	inline GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF * get_m_graphyManager_5() const { return ___m_graphyManager_5; }
	inline GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF ** get_address_of_m_graphyManager_5() { return &___m_graphyManager_5; }
	inline void set_m_graphyManager_5(GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF * value)
	{
		___m_graphyManager_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_graphyManager_5), value);
	}

	inline static int32_t get_offset_of_m_audioMonitor_6() { return static_cast<int32_t>(offsetof(G_AudioText_t7BCE4D478232F4284809B83E3150DBCC8DC64BF3, ___m_audioMonitor_6)); }
	inline G_AudioMonitor_t386A2D77995058DA7241FC1C90AF9FF6DE38EA2E * get_m_audioMonitor_6() const { return ___m_audioMonitor_6; }
	inline G_AudioMonitor_t386A2D77995058DA7241FC1C90AF9FF6DE38EA2E ** get_address_of_m_audioMonitor_6() { return &___m_audioMonitor_6; }
	inline void set_m_audioMonitor_6(G_AudioMonitor_t386A2D77995058DA7241FC1C90AF9FF6DE38EA2E * value)
	{
		___m_audioMonitor_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_audioMonitor_6), value);
	}

	inline static int32_t get_offset_of_m_updateRate_7() { return static_cast<int32_t>(offsetof(G_AudioText_t7BCE4D478232F4284809B83E3150DBCC8DC64BF3, ___m_updateRate_7)); }
	inline int32_t get_m_updateRate_7() const { return ___m_updateRate_7; }
	inline int32_t* get_address_of_m_updateRate_7() { return &___m_updateRate_7; }
	inline void set_m_updateRate_7(int32_t value)
	{
		___m_updateRate_7 = value;
	}

	inline static int32_t get_offset_of_m_deltaTimeOffset_8() { return static_cast<int32_t>(offsetof(G_AudioText_t7BCE4D478232F4284809B83E3150DBCC8DC64BF3, ___m_deltaTimeOffset_8)); }
	inline float get_m_deltaTimeOffset_8() const { return ___m_deltaTimeOffset_8; }
	inline float* get_address_of_m_deltaTimeOffset_8() { return &___m_deltaTimeOffset_8; }
	inline void set_m_deltaTimeOffset_8(float value)
	{
		___m_deltaTimeOffset_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // G_AUDIOTEXT_T7BCE4D478232F4284809B83E3150DBCC8DC64BF3_H
#ifndef CUSTOMIZEGRAPHY_TC2261ED0BAB21EF20627C51AB165620ABF7325BF_H
#define CUSTOMIZEGRAPHY_TC2261ED0BAB21EF20627C51AB165620ABF7325BF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayx.Graphy.CustomizationScene.CustomizeGraphy
struct  CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Tayx.Graphy.CustomizationScene.G_CUIColorPicker Tayx.Graphy.CustomizationScene.CustomizeGraphy::m_colorPicker
	G_CUIColorPicker_t65243BB7AF1C8840635414A3F44F39FCBB1B3D83 * ___m_colorPicker_4;
	// UnityEngine.UI.Toggle Tayx.Graphy.CustomizationScene.CustomizeGraphy::m_backgroundToggle
	Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * ___m_backgroundToggle_5;
	// UnityEngine.UI.Dropdown Tayx.Graphy.CustomizationScene.CustomizeGraphy::m_graphyModeDropdown
	Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * ___m_graphyModeDropdown_6;
	// UnityEngine.UI.Button Tayx.Graphy.CustomizationScene.CustomizeGraphy::m_backgroundColorButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___m_backgroundColorButton_7;
	// UnityEngine.UI.Dropdown Tayx.Graphy.CustomizationScene.CustomizeGraphy::m_graphModulePositionDropdown
	Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * ___m_graphModulePositionDropdown_8;
	// UnityEngine.UI.Dropdown Tayx.Graphy.CustomizationScene.CustomizeGraphy::m_fpsModuleStateDropdown
	Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * ___m_fpsModuleStateDropdown_9;
	// UnityEngine.UI.InputField Tayx.Graphy.CustomizationScene.CustomizeGraphy::m_goodInputField
	InputField_t533609195B110760BCFF00B746C87D81969CB005 * ___m_goodInputField_10;
	// UnityEngine.UI.InputField Tayx.Graphy.CustomizationScene.CustomizeGraphy::m_cautionInputField
	InputField_t533609195B110760BCFF00B746C87D81969CB005 * ___m_cautionInputField_11;
	// UnityEngine.UI.Button Tayx.Graphy.CustomizationScene.CustomizeGraphy::m_goodColorButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___m_goodColorButton_12;
	// UnityEngine.UI.Button Tayx.Graphy.CustomizationScene.CustomizeGraphy::m_cautionColorButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___m_cautionColorButton_13;
	// UnityEngine.UI.Button Tayx.Graphy.CustomizationScene.CustomizeGraphy::m_criticalColorButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___m_criticalColorButton_14;
	// UnityEngine.UI.Slider Tayx.Graphy.CustomizationScene.CustomizeGraphy::m_timeToResetMinMaxSlider
	Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * ___m_timeToResetMinMaxSlider_15;
	// UnityEngine.UI.Slider Tayx.Graphy.CustomizationScene.CustomizeGraphy::m_fpsGraphResolutionSlider
	Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * ___m_fpsGraphResolutionSlider_16;
	// UnityEngine.UI.Slider Tayx.Graphy.CustomizationScene.CustomizeGraphy::m_fpsTextUpdateRateSlider
	Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * ___m_fpsTextUpdateRateSlider_17;
	// UnityEngine.UI.Dropdown Tayx.Graphy.CustomizationScene.CustomizeGraphy::m_ramModuleStateDropdown
	Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * ___m_ramModuleStateDropdown_18;
	// UnityEngine.UI.Button Tayx.Graphy.CustomizationScene.CustomizeGraphy::m_reservedColorButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___m_reservedColorButton_19;
	// UnityEngine.UI.Button Tayx.Graphy.CustomizationScene.CustomizeGraphy::m_allocatedColorButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___m_allocatedColorButton_20;
	// UnityEngine.UI.Button Tayx.Graphy.CustomizationScene.CustomizeGraphy::m_monoColorButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___m_monoColorButton_21;
	// UnityEngine.UI.Slider Tayx.Graphy.CustomizationScene.CustomizeGraphy::m_ramGraphResolutionSlider
	Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * ___m_ramGraphResolutionSlider_22;
	// UnityEngine.UI.Slider Tayx.Graphy.CustomizationScene.CustomizeGraphy::m_ramTextUpdateRateSlider
	Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * ___m_ramTextUpdateRateSlider_23;
	// UnityEngine.UI.Dropdown Tayx.Graphy.CustomizationScene.CustomizeGraphy::m_audioModuleStateDropdown
	Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * ___m_audioModuleStateDropdown_24;
	// UnityEngine.UI.Button Tayx.Graphy.CustomizationScene.CustomizeGraphy::m_audioGraphColorButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___m_audioGraphColorButton_25;
	// UnityEngine.UI.Dropdown Tayx.Graphy.CustomizationScene.CustomizeGraphy::m_findAudioListenerDropdown
	Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * ___m_findAudioListenerDropdown_26;
	// UnityEngine.UI.Dropdown Tayx.Graphy.CustomizationScene.CustomizeGraphy::m_fttWindowDropdown
	Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * ___m_fttWindowDropdown_27;
	// UnityEngine.UI.Slider Tayx.Graphy.CustomizationScene.CustomizeGraphy::m_spectrumSizeSlider
	Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * ___m_spectrumSizeSlider_28;
	// UnityEngine.UI.Slider Tayx.Graphy.CustomizationScene.CustomizeGraphy::m_audioGraphResolutionSlider
	Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * ___m_audioGraphResolutionSlider_29;
	// UnityEngine.UI.Slider Tayx.Graphy.CustomizationScene.CustomizeGraphy::m_audioTextUpdateRateSlider
	Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * ___m_audioTextUpdateRateSlider_30;
	// UnityEngine.UI.Dropdown Tayx.Graphy.CustomizationScene.CustomizeGraphy::m_advancedModulePositionDropdown
	Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * ___m_advancedModulePositionDropdown_31;
	// UnityEngine.UI.Toggle Tayx.Graphy.CustomizationScene.CustomizeGraphy::m_advancedModuleToggle
	Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * ___m_advancedModuleToggle_32;
	// UnityEngine.UI.Button Tayx.Graphy.CustomizationScene.CustomizeGraphy::m_musicButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___m_musicButton_33;
	// UnityEngine.UI.Button Tayx.Graphy.CustomizationScene.CustomizeGraphy::m_sfxButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___m_sfxButton_34;
	// UnityEngine.UI.Slider Tayx.Graphy.CustomizationScene.CustomizeGraphy::m_musicVolumeSlider
	Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * ___m_musicVolumeSlider_35;
	// UnityEngine.UI.Slider Tayx.Graphy.CustomizationScene.CustomizeGraphy::m_sfxVolumeSlider
	Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * ___m_sfxVolumeSlider_36;
	// UnityEngine.AudioSource Tayx.Graphy.CustomizationScene.CustomizeGraphy::m_musicAudioSource
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ___m_musicAudioSource_37;
	// UnityEngine.AudioSource Tayx.Graphy.CustomizationScene.CustomizeGraphy::m_sfxAudioSource
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ___m_sfxAudioSource_38;
	// System.Collections.Generic.List`1<UnityEngine.AudioClip> Tayx.Graphy.CustomizationScene.CustomizeGraphy::m_sfxAudioClips
	List_1_tE0FA93AA3DC624E2BF3DC417F5FEBE1DBD81256F * ___m_sfxAudioClips_39;
	// Tayx.Graphy.GraphyManager Tayx.Graphy.CustomizationScene.CustomizeGraphy::m_graphyManager
	GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF * ___m_graphyManager_40;

public:
	inline static int32_t get_offset_of_m_colorPicker_4() { return static_cast<int32_t>(offsetof(CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF, ___m_colorPicker_4)); }
	inline G_CUIColorPicker_t65243BB7AF1C8840635414A3F44F39FCBB1B3D83 * get_m_colorPicker_4() const { return ___m_colorPicker_4; }
	inline G_CUIColorPicker_t65243BB7AF1C8840635414A3F44F39FCBB1B3D83 ** get_address_of_m_colorPicker_4() { return &___m_colorPicker_4; }
	inline void set_m_colorPicker_4(G_CUIColorPicker_t65243BB7AF1C8840635414A3F44F39FCBB1B3D83 * value)
	{
		___m_colorPicker_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_colorPicker_4), value);
	}

	inline static int32_t get_offset_of_m_backgroundToggle_5() { return static_cast<int32_t>(offsetof(CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF, ___m_backgroundToggle_5)); }
	inline Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * get_m_backgroundToggle_5() const { return ___m_backgroundToggle_5; }
	inline Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 ** get_address_of_m_backgroundToggle_5() { return &___m_backgroundToggle_5; }
	inline void set_m_backgroundToggle_5(Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * value)
	{
		___m_backgroundToggle_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_backgroundToggle_5), value);
	}

	inline static int32_t get_offset_of_m_graphyModeDropdown_6() { return static_cast<int32_t>(offsetof(CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF, ___m_graphyModeDropdown_6)); }
	inline Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * get_m_graphyModeDropdown_6() const { return ___m_graphyModeDropdown_6; }
	inline Dropdown_tF6331401084B1213CAB10587A6EC81461501930F ** get_address_of_m_graphyModeDropdown_6() { return &___m_graphyModeDropdown_6; }
	inline void set_m_graphyModeDropdown_6(Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * value)
	{
		___m_graphyModeDropdown_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_graphyModeDropdown_6), value);
	}

	inline static int32_t get_offset_of_m_backgroundColorButton_7() { return static_cast<int32_t>(offsetof(CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF, ___m_backgroundColorButton_7)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_m_backgroundColorButton_7() const { return ___m_backgroundColorButton_7; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_m_backgroundColorButton_7() { return &___m_backgroundColorButton_7; }
	inline void set_m_backgroundColorButton_7(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___m_backgroundColorButton_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_backgroundColorButton_7), value);
	}

	inline static int32_t get_offset_of_m_graphModulePositionDropdown_8() { return static_cast<int32_t>(offsetof(CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF, ___m_graphModulePositionDropdown_8)); }
	inline Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * get_m_graphModulePositionDropdown_8() const { return ___m_graphModulePositionDropdown_8; }
	inline Dropdown_tF6331401084B1213CAB10587A6EC81461501930F ** get_address_of_m_graphModulePositionDropdown_8() { return &___m_graphModulePositionDropdown_8; }
	inline void set_m_graphModulePositionDropdown_8(Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * value)
	{
		___m_graphModulePositionDropdown_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_graphModulePositionDropdown_8), value);
	}

	inline static int32_t get_offset_of_m_fpsModuleStateDropdown_9() { return static_cast<int32_t>(offsetof(CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF, ___m_fpsModuleStateDropdown_9)); }
	inline Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * get_m_fpsModuleStateDropdown_9() const { return ___m_fpsModuleStateDropdown_9; }
	inline Dropdown_tF6331401084B1213CAB10587A6EC81461501930F ** get_address_of_m_fpsModuleStateDropdown_9() { return &___m_fpsModuleStateDropdown_9; }
	inline void set_m_fpsModuleStateDropdown_9(Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * value)
	{
		___m_fpsModuleStateDropdown_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_fpsModuleStateDropdown_9), value);
	}

	inline static int32_t get_offset_of_m_goodInputField_10() { return static_cast<int32_t>(offsetof(CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF, ___m_goodInputField_10)); }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 * get_m_goodInputField_10() const { return ___m_goodInputField_10; }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 ** get_address_of_m_goodInputField_10() { return &___m_goodInputField_10; }
	inline void set_m_goodInputField_10(InputField_t533609195B110760BCFF00B746C87D81969CB005 * value)
	{
		___m_goodInputField_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_goodInputField_10), value);
	}

	inline static int32_t get_offset_of_m_cautionInputField_11() { return static_cast<int32_t>(offsetof(CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF, ___m_cautionInputField_11)); }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 * get_m_cautionInputField_11() const { return ___m_cautionInputField_11; }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 ** get_address_of_m_cautionInputField_11() { return &___m_cautionInputField_11; }
	inline void set_m_cautionInputField_11(InputField_t533609195B110760BCFF00B746C87D81969CB005 * value)
	{
		___m_cautionInputField_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_cautionInputField_11), value);
	}

	inline static int32_t get_offset_of_m_goodColorButton_12() { return static_cast<int32_t>(offsetof(CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF, ___m_goodColorButton_12)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_m_goodColorButton_12() const { return ___m_goodColorButton_12; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_m_goodColorButton_12() { return &___m_goodColorButton_12; }
	inline void set_m_goodColorButton_12(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___m_goodColorButton_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_goodColorButton_12), value);
	}

	inline static int32_t get_offset_of_m_cautionColorButton_13() { return static_cast<int32_t>(offsetof(CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF, ___m_cautionColorButton_13)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_m_cautionColorButton_13() const { return ___m_cautionColorButton_13; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_m_cautionColorButton_13() { return &___m_cautionColorButton_13; }
	inline void set_m_cautionColorButton_13(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___m_cautionColorButton_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_cautionColorButton_13), value);
	}

	inline static int32_t get_offset_of_m_criticalColorButton_14() { return static_cast<int32_t>(offsetof(CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF, ___m_criticalColorButton_14)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_m_criticalColorButton_14() const { return ___m_criticalColorButton_14; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_m_criticalColorButton_14() { return &___m_criticalColorButton_14; }
	inline void set_m_criticalColorButton_14(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___m_criticalColorButton_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_criticalColorButton_14), value);
	}

	inline static int32_t get_offset_of_m_timeToResetMinMaxSlider_15() { return static_cast<int32_t>(offsetof(CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF, ___m_timeToResetMinMaxSlider_15)); }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * get_m_timeToResetMinMaxSlider_15() const { return ___m_timeToResetMinMaxSlider_15; }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 ** get_address_of_m_timeToResetMinMaxSlider_15() { return &___m_timeToResetMinMaxSlider_15; }
	inline void set_m_timeToResetMinMaxSlider_15(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * value)
	{
		___m_timeToResetMinMaxSlider_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_timeToResetMinMaxSlider_15), value);
	}

	inline static int32_t get_offset_of_m_fpsGraphResolutionSlider_16() { return static_cast<int32_t>(offsetof(CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF, ___m_fpsGraphResolutionSlider_16)); }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * get_m_fpsGraphResolutionSlider_16() const { return ___m_fpsGraphResolutionSlider_16; }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 ** get_address_of_m_fpsGraphResolutionSlider_16() { return &___m_fpsGraphResolutionSlider_16; }
	inline void set_m_fpsGraphResolutionSlider_16(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * value)
	{
		___m_fpsGraphResolutionSlider_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_fpsGraphResolutionSlider_16), value);
	}

	inline static int32_t get_offset_of_m_fpsTextUpdateRateSlider_17() { return static_cast<int32_t>(offsetof(CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF, ___m_fpsTextUpdateRateSlider_17)); }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * get_m_fpsTextUpdateRateSlider_17() const { return ___m_fpsTextUpdateRateSlider_17; }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 ** get_address_of_m_fpsTextUpdateRateSlider_17() { return &___m_fpsTextUpdateRateSlider_17; }
	inline void set_m_fpsTextUpdateRateSlider_17(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * value)
	{
		___m_fpsTextUpdateRateSlider_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_fpsTextUpdateRateSlider_17), value);
	}

	inline static int32_t get_offset_of_m_ramModuleStateDropdown_18() { return static_cast<int32_t>(offsetof(CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF, ___m_ramModuleStateDropdown_18)); }
	inline Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * get_m_ramModuleStateDropdown_18() const { return ___m_ramModuleStateDropdown_18; }
	inline Dropdown_tF6331401084B1213CAB10587A6EC81461501930F ** get_address_of_m_ramModuleStateDropdown_18() { return &___m_ramModuleStateDropdown_18; }
	inline void set_m_ramModuleStateDropdown_18(Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * value)
	{
		___m_ramModuleStateDropdown_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_ramModuleStateDropdown_18), value);
	}

	inline static int32_t get_offset_of_m_reservedColorButton_19() { return static_cast<int32_t>(offsetof(CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF, ___m_reservedColorButton_19)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_m_reservedColorButton_19() const { return ___m_reservedColorButton_19; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_m_reservedColorButton_19() { return &___m_reservedColorButton_19; }
	inline void set_m_reservedColorButton_19(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___m_reservedColorButton_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_reservedColorButton_19), value);
	}

	inline static int32_t get_offset_of_m_allocatedColorButton_20() { return static_cast<int32_t>(offsetof(CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF, ___m_allocatedColorButton_20)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_m_allocatedColorButton_20() const { return ___m_allocatedColorButton_20; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_m_allocatedColorButton_20() { return &___m_allocatedColorButton_20; }
	inline void set_m_allocatedColorButton_20(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___m_allocatedColorButton_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_allocatedColorButton_20), value);
	}

	inline static int32_t get_offset_of_m_monoColorButton_21() { return static_cast<int32_t>(offsetof(CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF, ___m_monoColorButton_21)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_m_monoColorButton_21() const { return ___m_monoColorButton_21; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_m_monoColorButton_21() { return &___m_monoColorButton_21; }
	inline void set_m_monoColorButton_21(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___m_monoColorButton_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_monoColorButton_21), value);
	}

	inline static int32_t get_offset_of_m_ramGraphResolutionSlider_22() { return static_cast<int32_t>(offsetof(CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF, ___m_ramGraphResolutionSlider_22)); }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * get_m_ramGraphResolutionSlider_22() const { return ___m_ramGraphResolutionSlider_22; }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 ** get_address_of_m_ramGraphResolutionSlider_22() { return &___m_ramGraphResolutionSlider_22; }
	inline void set_m_ramGraphResolutionSlider_22(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * value)
	{
		___m_ramGraphResolutionSlider_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_ramGraphResolutionSlider_22), value);
	}

	inline static int32_t get_offset_of_m_ramTextUpdateRateSlider_23() { return static_cast<int32_t>(offsetof(CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF, ___m_ramTextUpdateRateSlider_23)); }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * get_m_ramTextUpdateRateSlider_23() const { return ___m_ramTextUpdateRateSlider_23; }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 ** get_address_of_m_ramTextUpdateRateSlider_23() { return &___m_ramTextUpdateRateSlider_23; }
	inline void set_m_ramTextUpdateRateSlider_23(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * value)
	{
		___m_ramTextUpdateRateSlider_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_ramTextUpdateRateSlider_23), value);
	}

	inline static int32_t get_offset_of_m_audioModuleStateDropdown_24() { return static_cast<int32_t>(offsetof(CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF, ___m_audioModuleStateDropdown_24)); }
	inline Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * get_m_audioModuleStateDropdown_24() const { return ___m_audioModuleStateDropdown_24; }
	inline Dropdown_tF6331401084B1213CAB10587A6EC81461501930F ** get_address_of_m_audioModuleStateDropdown_24() { return &___m_audioModuleStateDropdown_24; }
	inline void set_m_audioModuleStateDropdown_24(Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * value)
	{
		___m_audioModuleStateDropdown_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_audioModuleStateDropdown_24), value);
	}

	inline static int32_t get_offset_of_m_audioGraphColorButton_25() { return static_cast<int32_t>(offsetof(CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF, ___m_audioGraphColorButton_25)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_m_audioGraphColorButton_25() const { return ___m_audioGraphColorButton_25; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_m_audioGraphColorButton_25() { return &___m_audioGraphColorButton_25; }
	inline void set_m_audioGraphColorButton_25(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___m_audioGraphColorButton_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_audioGraphColorButton_25), value);
	}

	inline static int32_t get_offset_of_m_findAudioListenerDropdown_26() { return static_cast<int32_t>(offsetof(CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF, ___m_findAudioListenerDropdown_26)); }
	inline Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * get_m_findAudioListenerDropdown_26() const { return ___m_findAudioListenerDropdown_26; }
	inline Dropdown_tF6331401084B1213CAB10587A6EC81461501930F ** get_address_of_m_findAudioListenerDropdown_26() { return &___m_findAudioListenerDropdown_26; }
	inline void set_m_findAudioListenerDropdown_26(Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * value)
	{
		___m_findAudioListenerDropdown_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_findAudioListenerDropdown_26), value);
	}

	inline static int32_t get_offset_of_m_fttWindowDropdown_27() { return static_cast<int32_t>(offsetof(CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF, ___m_fttWindowDropdown_27)); }
	inline Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * get_m_fttWindowDropdown_27() const { return ___m_fttWindowDropdown_27; }
	inline Dropdown_tF6331401084B1213CAB10587A6EC81461501930F ** get_address_of_m_fttWindowDropdown_27() { return &___m_fttWindowDropdown_27; }
	inline void set_m_fttWindowDropdown_27(Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * value)
	{
		___m_fttWindowDropdown_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_fttWindowDropdown_27), value);
	}

	inline static int32_t get_offset_of_m_spectrumSizeSlider_28() { return static_cast<int32_t>(offsetof(CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF, ___m_spectrumSizeSlider_28)); }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * get_m_spectrumSizeSlider_28() const { return ___m_spectrumSizeSlider_28; }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 ** get_address_of_m_spectrumSizeSlider_28() { return &___m_spectrumSizeSlider_28; }
	inline void set_m_spectrumSizeSlider_28(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * value)
	{
		___m_spectrumSizeSlider_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_spectrumSizeSlider_28), value);
	}

	inline static int32_t get_offset_of_m_audioGraphResolutionSlider_29() { return static_cast<int32_t>(offsetof(CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF, ___m_audioGraphResolutionSlider_29)); }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * get_m_audioGraphResolutionSlider_29() const { return ___m_audioGraphResolutionSlider_29; }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 ** get_address_of_m_audioGraphResolutionSlider_29() { return &___m_audioGraphResolutionSlider_29; }
	inline void set_m_audioGraphResolutionSlider_29(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * value)
	{
		___m_audioGraphResolutionSlider_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_audioGraphResolutionSlider_29), value);
	}

	inline static int32_t get_offset_of_m_audioTextUpdateRateSlider_30() { return static_cast<int32_t>(offsetof(CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF, ___m_audioTextUpdateRateSlider_30)); }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * get_m_audioTextUpdateRateSlider_30() const { return ___m_audioTextUpdateRateSlider_30; }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 ** get_address_of_m_audioTextUpdateRateSlider_30() { return &___m_audioTextUpdateRateSlider_30; }
	inline void set_m_audioTextUpdateRateSlider_30(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * value)
	{
		___m_audioTextUpdateRateSlider_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_audioTextUpdateRateSlider_30), value);
	}

	inline static int32_t get_offset_of_m_advancedModulePositionDropdown_31() { return static_cast<int32_t>(offsetof(CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF, ___m_advancedModulePositionDropdown_31)); }
	inline Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * get_m_advancedModulePositionDropdown_31() const { return ___m_advancedModulePositionDropdown_31; }
	inline Dropdown_tF6331401084B1213CAB10587A6EC81461501930F ** get_address_of_m_advancedModulePositionDropdown_31() { return &___m_advancedModulePositionDropdown_31; }
	inline void set_m_advancedModulePositionDropdown_31(Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * value)
	{
		___m_advancedModulePositionDropdown_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_advancedModulePositionDropdown_31), value);
	}

	inline static int32_t get_offset_of_m_advancedModuleToggle_32() { return static_cast<int32_t>(offsetof(CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF, ___m_advancedModuleToggle_32)); }
	inline Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * get_m_advancedModuleToggle_32() const { return ___m_advancedModuleToggle_32; }
	inline Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 ** get_address_of_m_advancedModuleToggle_32() { return &___m_advancedModuleToggle_32; }
	inline void set_m_advancedModuleToggle_32(Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * value)
	{
		___m_advancedModuleToggle_32 = value;
		Il2CppCodeGenWriteBarrier((&___m_advancedModuleToggle_32), value);
	}

	inline static int32_t get_offset_of_m_musicButton_33() { return static_cast<int32_t>(offsetof(CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF, ___m_musicButton_33)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_m_musicButton_33() const { return ___m_musicButton_33; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_m_musicButton_33() { return &___m_musicButton_33; }
	inline void set_m_musicButton_33(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___m_musicButton_33 = value;
		Il2CppCodeGenWriteBarrier((&___m_musicButton_33), value);
	}

	inline static int32_t get_offset_of_m_sfxButton_34() { return static_cast<int32_t>(offsetof(CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF, ___m_sfxButton_34)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_m_sfxButton_34() const { return ___m_sfxButton_34; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_m_sfxButton_34() { return &___m_sfxButton_34; }
	inline void set_m_sfxButton_34(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___m_sfxButton_34 = value;
		Il2CppCodeGenWriteBarrier((&___m_sfxButton_34), value);
	}

	inline static int32_t get_offset_of_m_musicVolumeSlider_35() { return static_cast<int32_t>(offsetof(CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF, ___m_musicVolumeSlider_35)); }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * get_m_musicVolumeSlider_35() const { return ___m_musicVolumeSlider_35; }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 ** get_address_of_m_musicVolumeSlider_35() { return &___m_musicVolumeSlider_35; }
	inline void set_m_musicVolumeSlider_35(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * value)
	{
		___m_musicVolumeSlider_35 = value;
		Il2CppCodeGenWriteBarrier((&___m_musicVolumeSlider_35), value);
	}

	inline static int32_t get_offset_of_m_sfxVolumeSlider_36() { return static_cast<int32_t>(offsetof(CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF, ___m_sfxVolumeSlider_36)); }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * get_m_sfxVolumeSlider_36() const { return ___m_sfxVolumeSlider_36; }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 ** get_address_of_m_sfxVolumeSlider_36() { return &___m_sfxVolumeSlider_36; }
	inline void set_m_sfxVolumeSlider_36(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * value)
	{
		___m_sfxVolumeSlider_36 = value;
		Il2CppCodeGenWriteBarrier((&___m_sfxVolumeSlider_36), value);
	}

	inline static int32_t get_offset_of_m_musicAudioSource_37() { return static_cast<int32_t>(offsetof(CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF, ___m_musicAudioSource_37)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get_m_musicAudioSource_37() const { return ___m_musicAudioSource_37; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of_m_musicAudioSource_37() { return &___m_musicAudioSource_37; }
	inline void set_m_musicAudioSource_37(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		___m_musicAudioSource_37 = value;
		Il2CppCodeGenWriteBarrier((&___m_musicAudioSource_37), value);
	}

	inline static int32_t get_offset_of_m_sfxAudioSource_38() { return static_cast<int32_t>(offsetof(CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF, ___m_sfxAudioSource_38)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get_m_sfxAudioSource_38() const { return ___m_sfxAudioSource_38; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of_m_sfxAudioSource_38() { return &___m_sfxAudioSource_38; }
	inline void set_m_sfxAudioSource_38(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		___m_sfxAudioSource_38 = value;
		Il2CppCodeGenWriteBarrier((&___m_sfxAudioSource_38), value);
	}

	inline static int32_t get_offset_of_m_sfxAudioClips_39() { return static_cast<int32_t>(offsetof(CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF, ___m_sfxAudioClips_39)); }
	inline List_1_tE0FA93AA3DC624E2BF3DC417F5FEBE1DBD81256F * get_m_sfxAudioClips_39() const { return ___m_sfxAudioClips_39; }
	inline List_1_tE0FA93AA3DC624E2BF3DC417F5FEBE1DBD81256F ** get_address_of_m_sfxAudioClips_39() { return &___m_sfxAudioClips_39; }
	inline void set_m_sfxAudioClips_39(List_1_tE0FA93AA3DC624E2BF3DC417F5FEBE1DBD81256F * value)
	{
		___m_sfxAudioClips_39 = value;
		Il2CppCodeGenWriteBarrier((&___m_sfxAudioClips_39), value);
	}

	inline static int32_t get_offset_of_m_graphyManager_40() { return static_cast<int32_t>(offsetof(CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF, ___m_graphyManager_40)); }
	inline GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF * get_m_graphyManager_40() const { return ___m_graphyManager_40; }
	inline GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF ** get_address_of_m_graphyManager_40() { return &___m_graphyManager_40; }
	inline void set_m_graphyManager_40(GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF * value)
	{
		___m_graphyManager_40 = value;
		Il2CppCodeGenWriteBarrier((&___m_graphyManager_40), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMIZEGRAPHY_TC2261ED0BAB21EF20627C51AB165620ABF7325BF_H
#ifndef FORCESLIDERTOMULTIPLEOF3_T758350D319AC12D824AF0489E81E2433E2282284_H
#define FORCESLIDERTOMULTIPLEOF3_T758350D319AC12D824AF0489E81E2433E2282284_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayx.Graphy.CustomizationScene.ForceSliderToMultipleOf3
struct  ForceSliderToMultipleOf3_t758350D319AC12D824AF0489E81E2433E2282284  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Slider Tayx.Graphy.CustomizationScene.ForceSliderToMultipleOf3::m_slider
	Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * ___m_slider_4;

public:
	inline static int32_t get_offset_of_m_slider_4() { return static_cast<int32_t>(offsetof(ForceSliderToMultipleOf3_t758350D319AC12D824AF0489E81E2433E2282284, ___m_slider_4)); }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * get_m_slider_4() const { return ___m_slider_4; }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 ** get_address_of_m_slider_4() { return &___m_slider_4; }
	inline void set_m_slider_4(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * value)
	{
		___m_slider_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_slider_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORCESLIDERTOMULTIPLEOF3_T758350D319AC12D824AF0489E81E2433E2282284_H
#ifndef FORCESLIDERTOPOWEROF2_T330FD4A336FD5026A124672AA063E35451ABEFA6_H
#define FORCESLIDERTOPOWEROF2_T330FD4A336FD5026A124672AA063E35451ABEFA6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayx.Graphy.CustomizationScene.ForceSliderToPowerOf2
struct  ForceSliderToPowerOf2_t330FD4A336FD5026A124672AA063E35451ABEFA6  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Slider Tayx.Graphy.CustomizationScene.ForceSliderToPowerOf2::m_slider
	Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * ___m_slider_4;
	// System.Int32[] Tayx.Graphy.CustomizationScene.ForceSliderToPowerOf2::m_powerOf2Values
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___m_powerOf2Values_5;
	// UnityEngine.UI.Text Tayx.Graphy.CustomizationScene.ForceSliderToPowerOf2::m_text
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___m_text_6;

public:
	inline static int32_t get_offset_of_m_slider_4() { return static_cast<int32_t>(offsetof(ForceSliderToPowerOf2_t330FD4A336FD5026A124672AA063E35451ABEFA6, ___m_slider_4)); }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * get_m_slider_4() const { return ___m_slider_4; }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 ** get_address_of_m_slider_4() { return &___m_slider_4; }
	inline void set_m_slider_4(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * value)
	{
		___m_slider_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_slider_4), value);
	}

	inline static int32_t get_offset_of_m_powerOf2Values_5() { return static_cast<int32_t>(offsetof(ForceSliderToPowerOf2_t330FD4A336FD5026A124672AA063E35451ABEFA6, ___m_powerOf2Values_5)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_m_powerOf2Values_5() const { return ___m_powerOf2Values_5; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_m_powerOf2Values_5() { return &___m_powerOf2Values_5; }
	inline void set_m_powerOf2Values_5(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___m_powerOf2Values_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_powerOf2Values_5), value);
	}

	inline static int32_t get_offset_of_m_text_6() { return static_cast<int32_t>(offsetof(ForceSliderToPowerOf2_t330FD4A336FD5026A124672AA063E35451ABEFA6, ___m_text_6)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_m_text_6() const { return ___m_text_6; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_m_text_6() { return &___m_text_6; }
	inline void set_m_text_6(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___m_text_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_text_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORCESLIDERTOPOWEROF2_T330FD4A336FD5026A124672AA063E35451ABEFA6_H
#ifndef G_CUICOLORPICKER_T65243BB7AF1C8840635414A3F44F39FCBB1B3D83_H
#define G_CUICOLORPICKER_T65243BB7AF1C8840635414A3F44F39FCBB1B3D83_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayx.Graphy.CustomizationScene.G_CUIColorPicker
struct  G_CUIColorPicker_t65243BB7AF1C8840635414A3F44F39FCBB1B3D83  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Slider Tayx.Graphy.CustomizationScene.G_CUIColorPicker::alphaSlider
	Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * ___alphaSlider_4;
	// UnityEngine.UI.Image Tayx.Graphy.CustomizationScene.G_CUIColorPicker::alphaSliderBGImage
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___alphaSliderBGImage_5;
	// UnityEngine.Color Tayx.Graphy.CustomizationScene.G_CUIColorPicker::_color
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ____color_6;
	// System.Action`1<UnityEngine.Color> Tayx.Graphy.CustomizationScene.G_CUIColorPicker::_onValueChange
	Action_1_tB42F900CEA2D8B71F6167E641B4822FF8FB34159 * ____onValueChange_7;
	// System.Action Tayx.Graphy.CustomizationScene.G_CUIColorPicker::_update
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ____update_8;

public:
	inline static int32_t get_offset_of_alphaSlider_4() { return static_cast<int32_t>(offsetof(G_CUIColorPicker_t65243BB7AF1C8840635414A3F44F39FCBB1B3D83, ___alphaSlider_4)); }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * get_alphaSlider_4() const { return ___alphaSlider_4; }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 ** get_address_of_alphaSlider_4() { return &___alphaSlider_4; }
	inline void set_alphaSlider_4(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * value)
	{
		___alphaSlider_4 = value;
		Il2CppCodeGenWriteBarrier((&___alphaSlider_4), value);
	}

	inline static int32_t get_offset_of_alphaSliderBGImage_5() { return static_cast<int32_t>(offsetof(G_CUIColorPicker_t65243BB7AF1C8840635414A3F44F39FCBB1B3D83, ___alphaSliderBGImage_5)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_alphaSliderBGImage_5() const { return ___alphaSliderBGImage_5; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_alphaSliderBGImage_5() { return &___alphaSliderBGImage_5; }
	inline void set_alphaSliderBGImage_5(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___alphaSliderBGImage_5 = value;
		Il2CppCodeGenWriteBarrier((&___alphaSliderBGImage_5), value);
	}

	inline static int32_t get_offset_of__color_6() { return static_cast<int32_t>(offsetof(G_CUIColorPicker_t65243BB7AF1C8840635414A3F44F39FCBB1B3D83, ____color_6)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get__color_6() const { return ____color_6; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of__color_6() { return &____color_6; }
	inline void set__color_6(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		____color_6 = value;
	}

	inline static int32_t get_offset_of__onValueChange_7() { return static_cast<int32_t>(offsetof(G_CUIColorPicker_t65243BB7AF1C8840635414A3F44F39FCBB1B3D83, ____onValueChange_7)); }
	inline Action_1_tB42F900CEA2D8B71F6167E641B4822FF8FB34159 * get__onValueChange_7() const { return ____onValueChange_7; }
	inline Action_1_tB42F900CEA2D8B71F6167E641B4822FF8FB34159 ** get_address_of__onValueChange_7() { return &____onValueChange_7; }
	inline void set__onValueChange_7(Action_1_tB42F900CEA2D8B71F6167E641B4822FF8FB34159 * value)
	{
		____onValueChange_7 = value;
		Il2CppCodeGenWriteBarrier((&____onValueChange_7), value);
	}

	inline static int32_t get_offset_of__update_8() { return static_cast<int32_t>(offsetof(G_CUIColorPicker_t65243BB7AF1C8840635414A3F44F39FCBB1B3D83, ____update_8)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get__update_8() const { return ____update_8; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of__update_8() { return &____update_8; }
	inline void set__update_8(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		____update_8 = value;
		Il2CppCodeGenWriteBarrier((&____update_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // G_CUICOLORPICKER_T65243BB7AF1C8840635414A3F44F39FCBB1B3D83_H
#ifndef UPDATETEXTWITHSLIDERVALUE_T2260AC538D1286A58088474F49BB13DFFB51160A_H
#define UPDATETEXTWITHSLIDERVALUE_T2260AC538D1286A58088474F49BB13DFFB51160A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayx.Graphy.CustomizationScene.UpdateTextWithSliderValue
struct  UpdateTextWithSliderValue_t2260AC538D1286A58088474F49BB13DFFB51160A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Slider Tayx.Graphy.CustomizationScene.UpdateTextWithSliderValue::m_slider
	Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * ___m_slider_4;
	// UnityEngine.UI.Text Tayx.Graphy.CustomizationScene.UpdateTextWithSliderValue::m_text
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___m_text_5;

public:
	inline static int32_t get_offset_of_m_slider_4() { return static_cast<int32_t>(offsetof(UpdateTextWithSliderValue_t2260AC538D1286A58088474F49BB13DFFB51160A, ___m_slider_4)); }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * get_m_slider_4() const { return ___m_slider_4; }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 ** get_address_of_m_slider_4() { return &___m_slider_4; }
	inline void set_m_slider_4(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * value)
	{
		___m_slider_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_slider_4), value);
	}

	inline static int32_t get_offset_of_m_text_5() { return static_cast<int32_t>(offsetof(UpdateTextWithSliderValue_t2260AC538D1286A58088474F49BB13DFFB51160A, ___m_text_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_m_text_5() const { return ___m_text_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_m_text_5() { return &___m_text_5; }
	inline void set_m_text_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___m_text_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_text_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATETEXTWITHSLIDERVALUE_T2260AC538D1286A58088474F49BB13DFFB51160A_H
#ifndef G_FPSMANAGER_TEB48DC94964043565EAC186648A2F0FCD4794243_H
#define G_FPSMANAGER_TEB48DC94964043565EAC186648A2F0FCD4794243_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayx.Graphy.Fps.G_FpsManager
struct  G_FpsManager_tEB48DC94964043565EAC186648A2F0FCD4794243  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject Tayx.Graphy.Fps.G_FpsManager::m_fpsGraphGameObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_fpsGraphGameObject_4;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> Tayx.Graphy.Fps.G_FpsManager::m_nonBasicTextGameObjects
	List_1_t99909CDEDA6D21189884AEA74B1FD99FC9C6A4C0 * ___m_nonBasicTextGameObjects_5;
	// System.Collections.Generic.List`1<UnityEngine.UI.Image> Tayx.Graphy.Fps.G_FpsManager::m_backgroundImages
	List_1_t5E6CEE165340A9D74D8BD47B8E6F422DFB7744ED * ___m_backgroundImages_6;
	// Tayx.Graphy.GraphyManager Tayx.Graphy.Fps.G_FpsManager::m_graphyManager
	GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF * ___m_graphyManager_7;
	// Tayx.Graphy.Fps.G_FpsGraph Tayx.Graphy.Fps.G_FpsManager::m_fpsGraph
	G_FpsGraph_t3DAC783275A67E078E4C1C2338F2DD9F03D14DEF * ___m_fpsGraph_8;
	// Tayx.Graphy.Fps.G_FpsMonitor Tayx.Graphy.Fps.G_FpsManager::m_fpsMonitor
	G_FpsMonitor_tDCDEDA2708585BE6B0202F2B3230D8C5F6820672 * ___m_fpsMonitor_9;
	// Tayx.Graphy.Fps.G_FpsText Tayx.Graphy.Fps.G_FpsManager::m_fpsText
	G_FpsText_tDA7B016BED64F8C283E352FEB686BD8372567DE5 * ___m_fpsText_10;
	// UnityEngine.RectTransform Tayx.Graphy.Fps.G_FpsManager::m_rectTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_rectTransform_11;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> Tayx.Graphy.Fps.G_FpsManager::m_childrenGameObjects
	List_1_t99909CDEDA6D21189884AEA74B1FD99FC9C6A4C0 * ___m_childrenGameObjects_12;
	// Tayx.Graphy.GraphyManager_ModuleState Tayx.Graphy.Fps.G_FpsManager::m_previousModuleState
	int32_t ___m_previousModuleState_13;
	// Tayx.Graphy.GraphyManager_ModuleState Tayx.Graphy.Fps.G_FpsManager::m_currentModuleState
	int32_t ___m_currentModuleState_14;

public:
	inline static int32_t get_offset_of_m_fpsGraphGameObject_4() { return static_cast<int32_t>(offsetof(G_FpsManager_tEB48DC94964043565EAC186648A2F0FCD4794243, ___m_fpsGraphGameObject_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_fpsGraphGameObject_4() const { return ___m_fpsGraphGameObject_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_fpsGraphGameObject_4() { return &___m_fpsGraphGameObject_4; }
	inline void set_m_fpsGraphGameObject_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_fpsGraphGameObject_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_fpsGraphGameObject_4), value);
	}

	inline static int32_t get_offset_of_m_nonBasicTextGameObjects_5() { return static_cast<int32_t>(offsetof(G_FpsManager_tEB48DC94964043565EAC186648A2F0FCD4794243, ___m_nonBasicTextGameObjects_5)); }
	inline List_1_t99909CDEDA6D21189884AEA74B1FD99FC9C6A4C0 * get_m_nonBasicTextGameObjects_5() const { return ___m_nonBasicTextGameObjects_5; }
	inline List_1_t99909CDEDA6D21189884AEA74B1FD99FC9C6A4C0 ** get_address_of_m_nonBasicTextGameObjects_5() { return &___m_nonBasicTextGameObjects_5; }
	inline void set_m_nonBasicTextGameObjects_5(List_1_t99909CDEDA6D21189884AEA74B1FD99FC9C6A4C0 * value)
	{
		___m_nonBasicTextGameObjects_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_nonBasicTextGameObjects_5), value);
	}

	inline static int32_t get_offset_of_m_backgroundImages_6() { return static_cast<int32_t>(offsetof(G_FpsManager_tEB48DC94964043565EAC186648A2F0FCD4794243, ___m_backgroundImages_6)); }
	inline List_1_t5E6CEE165340A9D74D8BD47B8E6F422DFB7744ED * get_m_backgroundImages_6() const { return ___m_backgroundImages_6; }
	inline List_1_t5E6CEE165340A9D74D8BD47B8E6F422DFB7744ED ** get_address_of_m_backgroundImages_6() { return &___m_backgroundImages_6; }
	inline void set_m_backgroundImages_6(List_1_t5E6CEE165340A9D74D8BD47B8E6F422DFB7744ED * value)
	{
		___m_backgroundImages_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_backgroundImages_6), value);
	}

	inline static int32_t get_offset_of_m_graphyManager_7() { return static_cast<int32_t>(offsetof(G_FpsManager_tEB48DC94964043565EAC186648A2F0FCD4794243, ___m_graphyManager_7)); }
	inline GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF * get_m_graphyManager_7() const { return ___m_graphyManager_7; }
	inline GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF ** get_address_of_m_graphyManager_7() { return &___m_graphyManager_7; }
	inline void set_m_graphyManager_7(GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF * value)
	{
		___m_graphyManager_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_graphyManager_7), value);
	}

	inline static int32_t get_offset_of_m_fpsGraph_8() { return static_cast<int32_t>(offsetof(G_FpsManager_tEB48DC94964043565EAC186648A2F0FCD4794243, ___m_fpsGraph_8)); }
	inline G_FpsGraph_t3DAC783275A67E078E4C1C2338F2DD9F03D14DEF * get_m_fpsGraph_8() const { return ___m_fpsGraph_8; }
	inline G_FpsGraph_t3DAC783275A67E078E4C1C2338F2DD9F03D14DEF ** get_address_of_m_fpsGraph_8() { return &___m_fpsGraph_8; }
	inline void set_m_fpsGraph_8(G_FpsGraph_t3DAC783275A67E078E4C1C2338F2DD9F03D14DEF * value)
	{
		___m_fpsGraph_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_fpsGraph_8), value);
	}

	inline static int32_t get_offset_of_m_fpsMonitor_9() { return static_cast<int32_t>(offsetof(G_FpsManager_tEB48DC94964043565EAC186648A2F0FCD4794243, ___m_fpsMonitor_9)); }
	inline G_FpsMonitor_tDCDEDA2708585BE6B0202F2B3230D8C5F6820672 * get_m_fpsMonitor_9() const { return ___m_fpsMonitor_9; }
	inline G_FpsMonitor_tDCDEDA2708585BE6B0202F2B3230D8C5F6820672 ** get_address_of_m_fpsMonitor_9() { return &___m_fpsMonitor_9; }
	inline void set_m_fpsMonitor_9(G_FpsMonitor_tDCDEDA2708585BE6B0202F2B3230D8C5F6820672 * value)
	{
		___m_fpsMonitor_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_fpsMonitor_9), value);
	}

	inline static int32_t get_offset_of_m_fpsText_10() { return static_cast<int32_t>(offsetof(G_FpsManager_tEB48DC94964043565EAC186648A2F0FCD4794243, ___m_fpsText_10)); }
	inline G_FpsText_tDA7B016BED64F8C283E352FEB686BD8372567DE5 * get_m_fpsText_10() const { return ___m_fpsText_10; }
	inline G_FpsText_tDA7B016BED64F8C283E352FEB686BD8372567DE5 ** get_address_of_m_fpsText_10() { return &___m_fpsText_10; }
	inline void set_m_fpsText_10(G_FpsText_tDA7B016BED64F8C283E352FEB686BD8372567DE5 * value)
	{
		___m_fpsText_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_fpsText_10), value);
	}

	inline static int32_t get_offset_of_m_rectTransform_11() { return static_cast<int32_t>(offsetof(G_FpsManager_tEB48DC94964043565EAC186648A2F0FCD4794243, ___m_rectTransform_11)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_rectTransform_11() const { return ___m_rectTransform_11; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_rectTransform_11() { return &___m_rectTransform_11; }
	inline void set_m_rectTransform_11(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_rectTransform_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_rectTransform_11), value);
	}

	inline static int32_t get_offset_of_m_childrenGameObjects_12() { return static_cast<int32_t>(offsetof(G_FpsManager_tEB48DC94964043565EAC186648A2F0FCD4794243, ___m_childrenGameObjects_12)); }
	inline List_1_t99909CDEDA6D21189884AEA74B1FD99FC9C6A4C0 * get_m_childrenGameObjects_12() const { return ___m_childrenGameObjects_12; }
	inline List_1_t99909CDEDA6D21189884AEA74B1FD99FC9C6A4C0 ** get_address_of_m_childrenGameObjects_12() { return &___m_childrenGameObjects_12; }
	inline void set_m_childrenGameObjects_12(List_1_t99909CDEDA6D21189884AEA74B1FD99FC9C6A4C0 * value)
	{
		___m_childrenGameObjects_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_childrenGameObjects_12), value);
	}

	inline static int32_t get_offset_of_m_previousModuleState_13() { return static_cast<int32_t>(offsetof(G_FpsManager_tEB48DC94964043565EAC186648A2F0FCD4794243, ___m_previousModuleState_13)); }
	inline int32_t get_m_previousModuleState_13() const { return ___m_previousModuleState_13; }
	inline int32_t* get_address_of_m_previousModuleState_13() { return &___m_previousModuleState_13; }
	inline void set_m_previousModuleState_13(int32_t value)
	{
		___m_previousModuleState_13 = value;
	}

	inline static int32_t get_offset_of_m_currentModuleState_14() { return static_cast<int32_t>(offsetof(G_FpsManager_tEB48DC94964043565EAC186648A2F0FCD4794243, ___m_currentModuleState_14)); }
	inline int32_t get_m_currentModuleState_14() const { return ___m_currentModuleState_14; }
	inline int32_t* get_address_of_m_currentModuleState_14() { return &___m_currentModuleState_14; }
	inline void set_m_currentModuleState_14(int32_t value)
	{
		___m_currentModuleState_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // G_FPSMANAGER_TEB48DC94964043565EAC186648A2F0FCD4794243_H
#ifndef G_FPSMONITOR_TDCDEDA2708585BE6B0202F2B3230D8C5F6820672_H
#define G_FPSMONITOR_TDCDEDA2708585BE6B0202F2B3230D8C5F6820672_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayx.Graphy.Fps.G_FpsMonitor
struct  G_FpsMonitor_tDCDEDA2708585BE6B0202F2B3230D8C5F6820672  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 Tayx.Graphy.Fps.G_FpsMonitor::m_averageSamples
	int32_t ___m_averageSamples_4;
	// Tayx.Graphy.GraphyManager Tayx.Graphy.Fps.G_FpsMonitor::m_graphyManager
	GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF * ___m_graphyManager_5;
	// System.Single Tayx.Graphy.Fps.G_FpsMonitor::m_currentFps
	float ___m_currentFps_6;
	// System.Single Tayx.Graphy.Fps.G_FpsMonitor::m_avgFps
	float ___m_avgFps_7;
	// System.Single Tayx.Graphy.Fps.G_FpsMonitor::m_minFps
	float ___m_minFps_8;
	// System.Single Tayx.Graphy.Fps.G_FpsMonitor::m_maxFps
	float ___m_maxFps_9;
	// System.Collections.Generic.List`1<System.Single> Tayx.Graphy.Fps.G_FpsMonitor::m_averageFpsSamples
	List_1_t8980FA0E6CB3848F706C43D859930435C34BCC37 * ___m_averageFpsSamples_10;
	// System.Int32 Tayx.Graphy.Fps.G_FpsMonitor::m_timeToResetMinMaxFps
	int32_t ___m_timeToResetMinMaxFps_11;
	// System.Single Tayx.Graphy.Fps.G_FpsMonitor::m_timeToResetMinFpsPassed
	float ___m_timeToResetMinFpsPassed_12;
	// System.Single Tayx.Graphy.Fps.G_FpsMonitor::m_timeToResetMaxFpsPassed
	float ___m_timeToResetMaxFpsPassed_13;
	// System.Single Tayx.Graphy.Fps.G_FpsMonitor::unscaledDeltaTime
	float ___unscaledDeltaTime_14;

public:
	inline static int32_t get_offset_of_m_averageSamples_4() { return static_cast<int32_t>(offsetof(G_FpsMonitor_tDCDEDA2708585BE6B0202F2B3230D8C5F6820672, ___m_averageSamples_4)); }
	inline int32_t get_m_averageSamples_4() const { return ___m_averageSamples_4; }
	inline int32_t* get_address_of_m_averageSamples_4() { return &___m_averageSamples_4; }
	inline void set_m_averageSamples_4(int32_t value)
	{
		___m_averageSamples_4 = value;
	}

	inline static int32_t get_offset_of_m_graphyManager_5() { return static_cast<int32_t>(offsetof(G_FpsMonitor_tDCDEDA2708585BE6B0202F2B3230D8C5F6820672, ___m_graphyManager_5)); }
	inline GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF * get_m_graphyManager_5() const { return ___m_graphyManager_5; }
	inline GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF ** get_address_of_m_graphyManager_5() { return &___m_graphyManager_5; }
	inline void set_m_graphyManager_5(GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF * value)
	{
		___m_graphyManager_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_graphyManager_5), value);
	}

	inline static int32_t get_offset_of_m_currentFps_6() { return static_cast<int32_t>(offsetof(G_FpsMonitor_tDCDEDA2708585BE6B0202F2B3230D8C5F6820672, ___m_currentFps_6)); }
	inline float get_m_currentFps_6() const { return ___m_currentFps_6; }
	inline float* get_address_of_m_currentFps_6() { return &___m_currentFps_6; }
	inline void set_m_currentFps_6(float value)
	{
		___m_currentFps_6 = value;
	}

	inline static int32_t get_offset_of_m_avgFps_7() { return static_cast<int32_t>(offsetof(G_FpsMonitor_tDCDEDA2708585BE6B0202F2B3230D8C5F6820672, ___m_avgFps_7)); }
	inline float get_m_avgFps_7() const { return ___m_avgFps_7; }
	inline float* get_address_of_m_avgFps_7() { return &___m_avgFps_7; }
	inline void set_m_avgFps_7(float value)
	{
		___m_avgFps_7 = value;
	}

	inline static int32_t get_offset_of_m_minFps_8() { return static_cast<int32_t>(offsetof(G_FpsMonitor_tDCDEDA2708585BE6B0202F2B3230D8C5F6820672, ___m_minFps_8)); }
	inline float get_m_minFps_8() const { return ___m_minFps_8; }
	inline float* get_address_of_m_minFps_8() { return &___m_minFps_8; }
	inline void set_m_minFps_8(float value)
	{
		___m_minFps_8 = value;
	}

	inline static int32_t get_offset_of_m_maxFps_9() { return static_cast<int32_t>(offsetof(G_FpsMonitor_tDCDEDA2708585BE6B0202F2B3230D8C5F6820672, ___m_maxFps_9)); }
	inline float get_m_maxFps_9() const { return ___m_maxFps_9; }
	inline float* get_address_of_m_maxFps_9() { return &___m_maxFps_9; }
	inline void set_m_maxFps_9(float value)
	{
		___m_maxFps_9 = value;
	}

	inline static int32_t get_offset_of_m_averageFpsSamples_10() { return static_cast<int32_t>(offsetof(G_FpsMonitor_tDCDEDA2708585BE6B0202F2B3230D8C5F6820672, ___m_averageFpsSamples_10)); }
	inline List_1_t8980FA0E6CB3848F706C43D859930435C34BCC37 * get_m_averageFpsSamples_10() const { return ___m_averageFpsSamples_10; }
	inline List_1_t8980FA0E6CB3848F706C43D859930435C34BCC37 ** get_address_of_m_averageFpsSamples_10() { return &___m_averageFpsSamples_10; }
	inline void set_m_averageFpsSamples_10(List_1_t8980FA0E6CB3848F706C43D859930435C34BCC37 * value)
	{
		___m_averageFpsSamples_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_averageFpsSamples_10), value);
	}

	inline static int32_t get_offset_of_m_timeToResetMinMaxFps_11() { return static_cast<int32_t>(offsetof(G_FpsMonitor_tDCDEDA2708585BE6B0202F2B3230D8C5F6820672, ___m_timeToResetMinMaxFps_11)); }
	inline int32_t get_m_timeToResetMinMaxFps_11() const { return ___m_timeToResetMinMaxFps_11; }
	inline int32_t* get_address_of_m_timeToResetMinMaxFps_11() { return &___m_timeToResetMinMaxFps_11; }
	inline void set_m_timeToResetMinMaxFps_11(int32_t value)
	{
		___m_timeToResetMinMaxFps_11 = value;
	}

	inline static int32_t get_offset_of_m_timeToResetMinFpsPassed_12() { return static_cast<int32_t>(offsetof(G_FpsMonitor_tDCDEDA2708585BE6B0202F2B3230D8C5F6820672, ___m_timeToResetMinFpsPassed_12)); }
	inline float get_m_timeToResetMinFpsPassed_12() const { return ___m_timeToResetMinFpsPassed_12; }
	inline float* get_address_of_m_timeToResetMinFpsPassed_12() { return &___m_timeToResetMinFpsPassed_12; }
	inline void set_m_timeToResetMinFpsPassed_12(float value)
	{
		___m_timeToResetMinFpsPassed_12 = value;
	}

	inline static int32_t get_offset_of_m_timeToResetMaxFpsPassed_13() { return static_cast<int32_t>(offsetof(G_FpsMonitor_tDCDEDA2708585BE6B0202F2B3230D8C5F6820672, ___m_timeToResetMaxFpsPassed_13)); }
	inline float get_m_timeToResetMaxFpsPassed_13() const { return ___m_timeToResetMaxFpsPassed_13; }
	inline float* get_address_of_m_timeToResetMaxFpsPassed_13() { return &___m_timeToResetMaxFpsPassed_13; }
	inline void set_m_timeToResetMaxFpsPassed_13(float value)
	{
		___m_timeToResetMaxFpsPassed_13 = value;
	}

	inline static int32_t get_offset_of_unscaledDeltaTime_14() { return static_cast<int32_t>(offsetof(G_FpsMonitor_tDCDEDA2708585BE6B0202F2B3230D8C5F6820672, ___unscaledDeltaTime_14)); }
	inline float get_unscaledDeltaTime_14() const { return ___unscaledDeltaTime_14; }
	inline float* get_address_of_unscaledDeltaTime_14() { return &___unscaledDeltaTime_14; }
	inline void set_unscaledDeltaTime_14(float value)
	{
		___unscaledDeltaTime_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // G_FPSMONITOR_TDCDEDA2708585BE6B0202F2B3230D8C5F6820672_H
#ifndef G_FPSTEXT_TDA7B016BED64F8C283E352FEB686BD8372567DE5_H
#define G_FPSTEXT_TDA7B016BED64F8C283E352FEB686BD8372567DE5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayx.Graphy.Fps.G_FpsText
struct  G_FpsText_tDA7B016BED64F8C283E352FEB686BD8372567DE5  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text Tayx.Graphy.Fps.G_FpsText::m_fpsText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___m_fpsText_4;
	// UnityEngine.UI.Text Tayx.Graphy.Fps.G_FpsText::m_msText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___m_msText_5;
	// UnityEngine.UI.Text Tayx.Graphy.Fps.G_FpsText::m_avgFpsText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___m_avgFpsText_6;
	// UnityEngine.UI.Text Tayx.Graphy.Fps.G_FpsText::m_minFpsText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___m_minFpsText_7;
	// UnityEngine.UI.Text Tayx.Graphy.Fps.G_FpsText::m_maxFpsText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___m_maxFpsText_8;
	// Tayx.Graphy.GraphyManager Tayx.Graphy.Fps.G_FpsText::m_graphyManager
	GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF * ___m_graphyManager_9;
	// Tayx.Graphy.Fps.G_FpsMonitor Tayx.Graphy.Fps.G_FpsText::m_fpsMonitor
	G_FpsMonitor_tDCDEDA2708585BE6B0202F2B3230D8C5F6820672 * ___m_fpsMonitor_10;
	// System.Int32 Tayx.Graphy.Fps.G_FpsText::m_updateRate
	int32_t ___m_updateRate_11;
	// System.Int32 Tayx.Graphy.Fps.G_FpsText::m_frameCount
	int32_t ___m_frameCount_12;
	// System.Single Tayx.Graphy.Fps.G_FpsText::m_deltaTime
	float ___m_deltaTime_13;
	// System.Single Tayx.Graphy.Fps.G_FpsText::m_fps
	float ___m_fps_14;

public:
	inline static int32_t get_offset_of_m_fpsText_4() { return static_cast<int32_t>(offsetof(G_FpsText_tDA7B016BED64F8C283E352FEB686BD8372567DE5, ___m_fpsText_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_m_fpsText_4() const { return ___m_fpsText_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_m_fpsText_4() { return &___m_fpsText_4; }
	inline void set_m_fpsText_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___m_fpsText_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_fpsText_4), value);
	}

	inline static int32_t get_offset_of_m_msText_5() { return static_cast<int32_t>(offsetof(G_FpsText_tDA7B016BED64F8C283E352FEB686BD8372567DE5, ___m_msText_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_m_msText_5() const { return ___m_msText_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_m_msText_5() { return &___m_msText_5; }
	inline void set_m_msText_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___m_msText_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_msText_5), value);
	}

	inline static int32_t get_offset_of_m_avgFpsText_6() { return static_cast<int32_t>(offsetof(G_FpsText_tDA7B016BED64F8C283E352FEB686BD8372567DE5, ___m_avgFpsText_6)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_m_avgFpsText_6() const { return ___m_avgFpsText_6; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_m_avgFpsText_6() { return &___m_avgFpsText_6; }
	inline void set_m_avgFpsText_6(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___m_avgFpsText_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_avgFpsText_6), value);
	}

	inline static int32_t get_offset_of_m_minFpsText_7() { return static_cast<int32_t>(offsetof(G_FpsText_tDA7B016BED64F8C283E352FEB686BD8372567DE5, ___m_minFpsText_7)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_m_minFpsText_7() const { return ___m_minFpsText_7; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_m_minFpsText_7() { return &___m_minFpsText_7; }
	inline void set_m_minFpsText_7(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___m_minFpsText_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_minFpsText_7), value);
	}

	inline static int32_t get_offset_of_m_maxFpsText_8() { return static_cast<int32_t>(offsetof(G_FpsText_tDA7B016BED64F8C283E352FEB686BD8372567DE5, ___m_maxFpsText_8)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_m_maxFpsText_8() const { return ___m_maxFpsText_8; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_m_maxFpsText_8() { return &___m_maxFpsText_8; }
	inline void set_m_maxFpsText_8(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___m_maxFpsText_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_maxFpsText_8), value);
	}

	inline static int32_t get_offset_of_m_graphyManager_9() { return static_cast<int32_t>(offsetof(G_FpsText_tDA7B016BED64F8C283E352FEB686BD8372567DE5, ___m_graphyManager_9)); }
	inline GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF * get_m_graphyManager_9() const { return ___m_graphyManager_9; }
	inline GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF ** get_address_of_m_graphyManager_9() { return &___m_graphyManager_9; }
	inline void set_m_graphyManager_9(GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF * value)
	{
		___m_graphyManager_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_graphyManager_9), value);
	}

	inline static int32_t get_offset_of_m_fpsMonitor_10() { return static_cast<int32_t>(offsetof(G_FpsText_tDA7B016BED64F8C283E352FEB686BD8372567DE5, ___m_fpsMonitor_10)); }
	inline G_FpsMonitor_tDCDEDA2708585BE6B0202F2B3230D8C5F6820672 * get_m_fpsMonitor_10() const { return ___m_fpsMonitor_10; }
	inline G_FpsMonitor_tDCDEDA2708585BE6B0202F2B3230D8C5F6820672 ** get_address_of_m_fpsMonitor_10() { return &___m_fpsMonitor_10; }
	inline void set_m_fpsMonitor_10(G_FpsMonitor_tDCDEDA2708585BE6B0202F2B3230D8C5F6820672 * value)
	{
		___m_fpsMonitor_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_fpsMonitor_10), value);
	}

	inline static int32_t get_offset_of_m_updateRate_11() { return static_cast<int32_t>(offsetof(G_FpsText_tDA7B016BED64F8C283E352FEB686BD8372567DE5, ___m_updateRate_11)); }
	inline int32_t get_m_updateRate_11() const { return ___m_updateRate_11; }
	inline int32_t* get_address_of_m_updateRate_11() { return &___m_updateRate_11; }
	inline void set_m_updateRate_11(int32_t value)
	{
		___m_updateRate_11 = value;
	}

	inline static int32_t get_offset_of_m_frameCount_12() { return static_cast<int32_t>(offsetof(G_FpsText_tDA7B016BED64F8C283E352FEB686BD8372567DE5, ___m_frameCount_12)); }
	inline int32_t get_m_frameCount_12() const { return ___m_frameCount_12; }
	inline int32_t* get_address_of_m_frameCount_12() { return &___m_frameCount_12; }
	inline void set_m_frameCount_12(int32_t value)
	{
		___m_frameCount_12 = value;
	}

	inline static int32_t get_offset_of_m_deltaTime_13() { return static_cast<int32_t>(offsetof(G_FpsText_tDA7B016BED64F8C283E352FEB686BD8372567DE5, ___m_deltaTime_13)); }
	inline float get_m_deltaTime_13() const { return ___m_deltaTime_13; }
	inline float* get_address_of_m_deltaTime_13() { return &___m_deltaTime_13; }
	inline void set_m_deltaTime_13(float value)
	{
		___m_deltaTime_13 = value;
	}

	inline static int32_t get_offset_of_m_fps_14() { return static_cast<int32_t>(offsetof(G_FpsText_tDA7B016BED64F8C283E352FEB686BD8372567DE5, ___m_fps_14)); }
	inline float get_m_fps_14() const { return ___m_fps_14; }
	inline float* get_address_of_m_fps_14() { return &___m_fps_14; }
	inline void set_m_fps_14(float value)
	{
		___m_fps_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // G_FPSTEXT_TDA7B016BED64F8C283E352FEB686BD8372567DE5_H
#ifndef G_GRAPH_TC294DBE51D02C7EAB91E324525602C788CD5A9CC_H
#define G_GRAPH_TC294DBE51D02C7EAB91E324525602C788CD5A9CC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayx.Graphy.Graph.G_Graph
struct  G_Graph_tC294DBE51D02C7EAB91E324525602C788CD5A9CC  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // G_GRAPH_TC294DBE51D02C7EAB91E324525602C788CD5A9CC_H
#ifndef G_RAMMANAGER_T2C694E71E71ABB53B78046251A3430D920B9B0C9_H
#define G_RAMMANAGER_T2C694E71E71ABB53B78046251A3430D920B9B0C9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayx.Graphy.Ram.G_RamManager
struct  G_RamManager_t2C694E71E71ABB53B78046251A3430D920B9B0C9  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject Tayx.Graphy.Ram.G_RamManager::m_ramGraphGameObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_ramGraphGameObject_4;
	// System.Collections.Generic.List`1<UnityEngine.UI.Image> Tayx.Graphy.Ram.G_RamManager::m_backgroundImages
	List_1_t5E6CEE165340A9D74D8BD47B8E6F422DFB7744ED * ___m_backgroundImages_5;
	// Tayx.Graphy.GraphyManager Tayx.Graphy.Ram.G_RamManager::m_graphyManager
	GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF * ___m_graphyManager_6;
	// Tayx.Graphy.Ram.G_RamGraph Tayx.Graphy.Ram.G_RamManager::m_ramGraph
	G_RamGraph_t3408E644B21F1A8F7E4A4932CB72BBE7483130CE * ___m_ramGraph_7;
	// Tayx.Graphy.Ram.G_RamText Tayx.Graphy.Ram.G_RamManager::m_ramText
	G_RamText_t1097A490F592CBE6D7CF0EF4BA0232252CDE5806 * ___m_ramText_8;
	// UnityEngine.RectTransform Tayx.Graphy.Ram.G_RamManager::m_rectTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_rectTransform_9;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> Tayx.Graphy.Ram.G_RamManager::m_childrenGameObjects
	List_1_t99909CDEDA6D21189884AEA74B1FD99FC9C6A4C0 * ___m_childrenGameObjects_10;
	// Tayx.Graphy.GraphyManager_ModuleState Tayx.Graphy.Ram.G_RamManager::m_previousModuleState
	int32_t ___m_previousModuleState_11;
	// Tayx.Graphy.GraphyManager_ModuleState Tayx.Graphy.Ram.G_RamManager::m_currentModuleState
	int32_t ___m_currentModuleState_12;

public:
	inline static int32_t get_offset_of_m_ramGraphGameObject_4() { return static_cast<int32_t>(offsetof(G_RamManager_t2C694E71E71ABB53B78046251A3430D920B9B0C9, ___m_ramGraphGameObject_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_ramGraphGameObject_4() const { return ___m_ramGraphGameObject_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_ramGraphGameObject_4() { return &___m_ramGraphGameObject_4; }
	inline void set_m_ramGraphGameObject_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_ramGraphGameObject_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_ramGraphGameObject_4), value);
	}

	inline static int32_t get_offset_of_m_backgroundImages_5() { return static_cast<int32_t>(offsetof(G_RamManager_t2C694E71E71ABB53B78046251A3430D920B9B0C9, ___m_backgroundImages_5)); }
	inline List_1_t5E6CEE165340A9D74D8BD47B8E6F422DFB7744ED * get_m_backgroundImages_5() const { return ___m_backgroundImages_5; }
	inline List_1_t5E6CEE165340A9D74D8BD47B8E6F422DFB7744ED ** get_address_of_m_backgroundImages_5() { return &___m_backgroundImages_5; }
	inline void set_m_backgroundImages_5(List_1_t5E6CEE165340A9D74D8BD47B8E6F422DFB7744ED * value)
	{
		___m_backgroundImages_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_backgroundImages_5), value);
	}

	inline static int32_t get_offset_of_m_graphyManager_6() { return static_cast<int32_t>(offsetof(G_RamManager_t2C694E71E71ABB53B78046251A3430D920B9B0C9, ___m_graphyManager_6)); }
	inline GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF * get_m_graphyManager_6() const { return ___m_graphyManager_6; }
	inline GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF ** get_address_of_m_graphyManager_6() { return &___m_graphyManager_6; }
	inline void set_m_graphyManager_6(GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF * value)
	{
		___m_graphyManager_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_graphyManager_6), value);
	}

	inline static int32_t get_offset_of_m_ramGraph_7() { return static_cast<int32_t>(offsetof(G_RamManager_t2C694E71E71ABB53B78046251A3430D920B9B0C9, ___m_ramGraph_7)); }
	inline G_RamGraph_t3408E644B21F1A8F7E4A4932CB72BBE7483130CE * get_m_ramGraph_7() const { return ___m_ramGraph_7; }
	inline G_RamGraph_t3408E644B21F1A8F7E4A4932CB72BBE7483130CE ** get_address_of_m_ramGraph_7() { return &___m_ramGraph_7; }
	inline void set_m_ramGraph_7(G_RamGraph_t3408E644B21F1A8F7E4A4932CB72BBE7483130CE * value)
	{
		___m_ramGraph_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_ramGraph_7), value);
	}

	inline static int32_t get_offset_of_m_ramText_8() { return static_cast<int32_t>(offsetof(G_RamManager_t2C694E71E71ABB53B78046251A3430D920B9B0C9, ___m_ramText_8)); }
	inline G_RamText_t1097A490F592CBE6D7CF0EF4BA0232252CDE5806 * get_m_ramText_8() const { return ___m_ramText_8; }
	inline G_RamText_t1097A490F592CBE6D7CF0EF4BA0232252CDE5806 ** get_address_of_m_ramText_8() { return &___m_ramText_8; }
	inline void set_m_ramText_8(G_RamText_t1097A490F592CBE6D7CF0EF4BA0232252CDE5806 * value)
	{
		___m_ramText_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_ramText_8), value);
	}

	inline static int32_t get_offset_of_m_rectTransform_9() { return static_cast<int32_t>(offsetof(G_RamManager_t2C694E71E71ABB53B78046251A3430D920B9B0C9, ___m_rectTransform_9)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_rectTransform_9() const { return ___m_rectTransform_9; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_rectTransform_9() { return &___m_rectTransform_9; }
	inline void set_m_rectTransform_9(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_rectTransform_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_rectTransform_9), value);
	}

	inline static int32_t get_offset_of_m_childrenGameObjects_10() { return static_cast<int32_t>(offsetof(G_RamManager_t2C694E71E71ABB53B78046251A3430D920B9B0C9, ___m_childrenGameObjects_10)); }
	inline List_1_t99909CDEDA6D21189884AEA74B1FD99FC9C6A4C0 * get_m_childrenGameObjects_10() const { return ___m_childrenGameObjects_10; }
	inline List_1_t99909CDEDA6D21189884AEA74B1FD99FC9C6A4C0 ** get_address_of_m_childrenGameObjects_10() { return &___m_childrenGameObjects_10; }
	inline void set_m_childrenGameObjects_10(List_1_t99909CDEDA6D21189884AEA74B1FD99FC9C6A4C0 * value)
	{
		___m_childrenGameObjects_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_childrenGameObjects_10), value);
	}

	inline static int32_t get_offset_of_m_previousModuleState_11() { return static_cast<int32_t>(offsetof(G_RamManager_t2C694E71E71ABB53B78046251A3430D920B9B0C9, ___m_previousModuleState_11)); }
	inline int32_t get_m_previousModuleState_11() const { return ___m_previousModuleState_11; }
	inline int32_t* get_address_of_m_previousModuleState_11() { return &___m_previousModuleState_11; }
	inline void set_m_previousModuleState_11(int32_t value)
	{
		___m_previousModuleState_11 = value;
	}

	inline static int32_t get_offset_of_m_currentModuleState_12() { return static_cast<int32_t>(offsetof(G_RamManager_t2C694E71E71ABB53B78046251A3430D920B9B0C9, ___m_currentModuleState_12)); }
	inline int32_t get_m_currentModuleState_12() const { return ___m_currentModuleState_12; }
	inline int32_t* get_address_of_m_currentModuleState_12() { return &___m_currentModuleState_12; }
	inline void set_m_currentModuleState_12(int32_t value)
	{
		___m_currentModuleState_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // G_RAMMANAGER_T2C694E71E71ABB53B78046251A3430D920B9B0C9_H
#ifndef G_RAMMONITOR_TCE95C3ECC0C03781E405698F56E20B12BC2650CA_H
#define G_RAMMONITOR_TCE95C3ECC0C03781E405698F56E20B12BC2650CA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayx.Graphy.Ram.G_RamMonitor
struct  G_RamMonitor_tCE95C3ECC0C03781E405698F56E20B12BC2650CA  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single Tayx.Graphy.Ram.G_RamMonitor::m_allocatedRam
	float ___m_allocatedRam_4;
	// System.Single Tayx.Graphy.Ram.G_RamMonitor::m_reservedRam
	float ___m_reservedRam_5;
	// System.Single Tayx.Graphy.Ram.G_RamMonitor::m_monoRam
	float ___m_monoRam_6;

public:
	inline static int32_t get_offset_of_m_allocatedRam_4() { return static_cast<int32_t>(offsetof(G_RamMonitor_tCE95C3ECC0C03781E405698F56E20B12BC2650CA, ___m_allocatedRam_4)); }
	inline float get_m_allocatedRam_4() const { return ___m_allocatedRam_4; }
	inline float* get_address_of_m_allocatedRam_4() { return &___m_allocatedRam_4; }
	inline void set_m_allocatedRam_4(float value)
	{
		___m_allocatedRam_4 = value;
	}

	inline static int32_t get_offset_of_m_reservedRam_5() { return static_cast<int32_t>(offsetof(G_RamMonitor_tCE95C3ECC0C03781E405698F56E20B12BC2650CA, ___m_reservedRam_5)); }
	inline float get_m_reservedRam_5() const { return ___m_reservedRam_5; }
	inline float* get_address_of_m_reservedRam_5() { return &___m_reservedRam_5; }
	inline void set_m_reservedRam_5(float value)
	{
		___m_reservedRam_5 = value;
	}

	inline static int32_t get_offset_of_m_monoRam_6() { return static_cast<int32_t>(offsetof(G_RamMonitor_tCE95C3ECC0C03781E405698F56E20B12BC2650CA, ___m_monoRam_6)); }
	inline float get_m_monoRam_6() const { return ___m_monoRam_6; }
	inline float* get_address_of_m_monoRam_6() { return &___m_monoRam_6; }
	inline void set_m_monoRam_6(float value)
	{
		___m_monoRam_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // G_RAMMONITOR_TCE95C3ECC0C03781E405698F56E20B12BC2650CA_H
#ifndef G_RAMTEXT_T1097A490F592CBE6D7CF0EF4BA0232252CDE5806_H
#define G_RAMTEXT_T1097A490F592CBE6D7CF0EF4BA0232252CDE5806_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayx.Graphy.Ram.G_RamText
struct  G_RamText_t1097A490F592CBE6D7CF0EF4BA0232252CDE5806  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text Tayx.Graphy.Ram.G_RamText::m_allocatedSystemMemorySizeText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___m_allocatedSystemMemorySizeText_4;
	// UnityEngine.UI.Text Tayx.Graphy.Ram.G_RamText::m_reservedSystemMemorySizeText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___m_reservedSystemMemorySizeText_5;
	// UnityEngine.UI.Text Tayx.Graphy.Ram.G_RamText::m_monoSystemMemorySizeText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___m_monoSystemMemorySizeText_6;
	// Tayx.Graphy.GraphyManager Tayx.Graphy.Ram.G_RamText::m_graphyManager
	GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF * ___m_graphyManager_7;
	// Tayx.Graphy.Ram.G_RamMonitor Tayx.Graphy.Ram.G_RamText::m_ramMonitor
	G_RamMonitor_tCE95C3ECC0C03781E405698F56E20B12BC2650CA * ___m_ramMonitor_8;
	// System.Single Tayx.Graphy.Ram.G_RamText::m_updateRate
	float ___m_updateRate_9;
	// System.Single Tayx.Graphy.Ram.G_RamText::m_deltaTime
	float ___m_deltaTime_10;
	// System.String Tayx.Graphy.Ram.G_RamText::m_memoryStringFormat
	String_t* ___m_memoryStringFormat_11;

public:
	inline static int32_t get_offset_of_m_allocatedSystemMemorySizeText_4() { return static_cast<int32_t>(offsetof(G_RamText_t1097A490F592CBE6D7CF0EF4BA0232252CDE5806, ___m_allocatedSystemMemorySizeText_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_m_allocatedSystemMemorySizeText_4() const { return ___m_allocatedSystemMemorySizeText_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_m_allocatedSystemMemorySizeText_4() { return &___m_allocatedSystemMemorySizeText_4; }
	inline void set_m_allocatedSystemMemorySizeText_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___m_allocatedSystemMemorySizeText_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_allocatedSystemMemorySizeText_4), value);
	}

	inline static int32_t get_offset_of_m_reservedSystemMemorySizeText_5() { return static_cast<int32_t>(offsetof(G_RamText_t1097A490F592CBE6D7CF0EF4BA0232252CDE5806, ___m_reservedSystemMemorySizeText_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_m_reservedSystemMemorySizeText_5() const { return ___m_reservedSystemMemorySizeText_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_m_reservedSystemMemorySizeText_5() { return &___m_reservedSystemMemorySizeText_5; }
	inline void set_m_reservedSystemMemorySizeText_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___m_reservedSystemMemorySizeText_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_reservedSystemMemorySizeText_5), value);
	}

	inline static int32_t get_offset_of_m_monoSystemMemorySizeText_6() { return static_cast<int32_t>(offsetof(G_RamText_t1097A490F592CBE6D7CF0EF4BA0232252CDE5806, ___m_monoSystemMemorySizeText_6)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_m_monoSystemMemorySizeText_6() const { return ___m_monoSystemMemorySizeText_6; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_m_monoSystemMemorySizeText_6() { return &___m_monoSystemMemorySizeText_6; }
	inline void set_m_monoSystemMemorySizeText_6(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___m_monoSystemMemorySizeText_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_monoSystemMemorySizeText_6), value);
	}

	inline static int32_t get_offset_of_m_graphyManager_7() { return static_cast<int32_t>(offsetof(G_RamText_t1097A490F592CBE6D7CF0EF4BA0232252CDE5806, ___m_graphyManager_7)); }
	inline GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF * get_m_graphyManager_7() const { return ___m_graphyManager_7; }
	inline GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF ** get_address_of_m_graphyManager_7() { return &___m_graphyManager_7; }
	inline void set_m_graphyManager_7(GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF * value)
	{
		___m_graphyManager_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_graphyManager_7), value);
	}

	inline static int32_t get_offset_of_m_ramMonitor_8() { return static_cast<int32_t>(offsetof(G_RamText_t1097A490F592CBE6D7CF0EF4BA0232252CDE5806, ___m_ramMonitor_8)); }
	inline G_RamMonitor_tCE95C3ECC0C03781E405698F56E20B12BC2650CA * get_m_ramMonitor_8() const { return ___m_ramMonitor_8; }
	inline G_RamMonitor_tCE95C3ECC0C03781E405698F56E20B12BC2650CA ** get_address_of_m_ramMonitor_8() { return &___m_ramMonitor_8; }
	inline void set_m_ramMonitor_8(G_RamMonitor_tCE95C3ECC0C03781E405698F56E20B12BC2650CA * value)
	{
		___m_ramMonitor_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_ramMonitor_8), value);
	}

	inline static int32_t get_offset_of_m_updateRate_9() { return static_cast<int32_t>(offsetof(G_RamText_t1097A490F592CBE6D7CF0EF4BA0232252CDE5806, ___m_updateRate_9)); }
	inline float get_m_updateRate_9() const { return ___m_updateRate_9; }
	inline float* get_address_of_m_updateRate_9() { return &___m_updateRate_9; }
	inline void set_m_updateRate_9(float value)
	{
		___m_updateRate_9 = value;
	}

	inline static int32_t get_offset_of_m_deltaTime_10() { return static_cast<int32_t>(offsetof(G_RamText_t1097A490F592CBE6D7CF0EF4BA0232252CDE5806, ___m_deltaTime_10)); }
	inline float get_m_deltaTime_10() const { return ___m_deltaTime_10; }
	inline float* get_address_of_m_deltaTime_10() { return &___m_deltaTime_10; }
	inline void set_m_deltaTime_10(float value)
	{
		___m_deltaTime_10 = value;
	}

	inline static int32_t get_offset_of_m_memoryStringFormat_11() { return static_cast<int32_t>(offsetof(G_RamText_t1097A490F592CBE6D7CF0EF4BA0232252CDE5806, ___m_memoryStringFormat_11)); }
	inline String_t* get_m_memoryStringFormat_11() const { return ___m_memoryStringFormat_11; }
	inline String_t** get_address_of_m_memoryStringFormat_11() { return &___m_memoryStringFormat_11; }
	inline void set_m_memoryStringFormat_11(String_t* value)
	{
		___m_memoryStringFormat_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_memoryStringFormat_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // G_RAMTEXT_T1097A490F592CBE6D7CF0EF4BA0232252CDE5806_H
#ifndef G_SINGLETON_1_TBF26C83E4663C941362F47E3A1E11E3520A22402_H
#define G_SINGLETON_1_TBF26C83E4663C941362F47E3A1E11E3520A22402_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayx.Graphy.Utils.G_Singleton`1<Tayx.Graphy.GraphyManager>
struct  G_Singleton_1_tBF26C83E4663C941362F47E3A1E11E3520A22402  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct G_Singleton_1_tBF26C83E4663C941362F47E3A1E11E3520A22402_StaticFields
{
public:
	// T Tayx.Graphy.Utils.G_Singleton`1::_instance
	GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF * ____instance_4;
	// System.Object Tayx.Graphy.Utils.G_Singleton`1::_lock
	RuntimeObject * ____lock_5;
	// System.Boolean Tayx.Graphy.Utils.G_Singleton`1::_applicationIsQuitting
	bool ____applicationIsQuitting_6;

public:
	inline static int32_t get_offset_of__instance_4() { return static_cast<int32_t>(offsetof(G_Singleton_1_tBF26C83E4663C941362F47E3A1E11E3520A22402_StaticFields, ____instance_4)); }
	inline GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF * get__instance_4() const { return ____instance_4; }
	inline GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF ** get_address_of__instance_4() { return &____instance_4; }
	inline void set__instance_4(GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF * value)
	{
		____instance_4 = value;
		Il2CppCodeGenWriteBarrier((&____instance_4), value);
	}

	inline static int32_t get_offset_of__lock_5() { return static_cast<int32_t>(offsetof(G_Singleton_1_tBF26C83E4663C941362F47E3A1E11E3520A22402_StaticFields, ____lock_5)); }
	inline RuntimeObject * get__lock_5() const { return ____lock_5; }
	inline RuntimeObject ** get_address_of__lock_5() { return &____lock_5; }
	inline void set__lock_5(RuntimeObject * value)
	{
		____lock_5 = value;
		Il2CppCodeGenWriteBarrier((&____lock_5), value);
	}

	inline static int32_t get_offset_of__applicationIsQuitting_6() { return static_cast<int32_t>(offsetof(G_Singleton_1_tBF26C83E4663C941362F47E3A1E11E3520A22402_StaticFields, ____applicationIsQuitting_6)); }
	inline bool get__applicationIsQuitting_6() const { return ____applicationIsQuitting_6; }
	inline bool* get_address_of__applicationIsQuitting_6() { return &____applicationIsQuitting_6; }
	inline void set__applicationIsQuitting_6(bool value)
	{
		____applicationIsQuitting_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // G_SINGLETON_1_TBF26C83E4663C941362F47E3A1E11E3520A22402_H
#ifndef G_AUDIOGRAPH_T02349ECE72DF71E24A86159551A347D323CB37CB_H
#define G_AUDIOGRAPH_T02349ECE72DF71E24A86159551A347D323CB37CB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayx.Graphy.Audio.G_AudioGraph
struct  G_AudioGraph_t02349ECE72DF71E24A86159551A347D323CB37CB  : public G_Graph_tC294DBE51D02C7EAB91E324525602C788CD5A9CC
{
public:
	// UnityEngine.UI.Image Tayx.Graphy.Audio.G_AudioGraph::m_imageGraph
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___m_imageGraph_4;
	// UnityEngine.UI.Image Tayx.Graphy.Audio.G_AudioGraph::m_imageGraphHighestValues
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___m_imageGraphHighestValues_5;
	// UnityEngine.Shader Tayx.Graphy.Audio.G_AudioGraph::ShaderFull
	Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * ___ShaderFull_6;
	// UnityEngine.Shader Tayx.Graphy.Audio.G_AudioGraph::ShaderLight
	Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * ___ShaderLight_7;
	// Tayx.Graphy.GraphyManager Tayx.Graphy.Audio.G_AudioGraph::m_graphyManager
	GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF * ___m_graphyManager_8;
	// Tayx.Graphy.Audio.G_AudioMonitor Tayx.Graphy.Audio.G_AudioGraph::m_audioMonitor
	G_AudioMonitor_t386A2D77995058DA7241FC1C90AF9FF6DE38EA2E * ___m_audioMonitor_9;
	// System.Int32 Tayx.Graphy.Audio.G_AudioGraph::m_resolution
	int32_t ___m_resolution_10;
	// Tayx.Graphy.G_GraphShader Tayx.Graphy.Audio.G_AudioGraph::m_shaderGraph
	G_GraphShader_t3D41D924F74863B5AFC8C101907225362954A8D3 * ___m_shaderGraph_11;
	// Tayx.Graphy.G_GraphShader Tayx.Graphy.Audio.G_AudioGraph::m_shaderGraphHighestValues
	G_GraphShader_t3D41D924F74863B5AFC8C101907225362954A8D3 * ___m_shaderGraphHighestValues_12;
	// System.Single[] Tayx.Graphy.Audio.G_AudioGraph::m_graphArray
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___m_graphArray_13;
	// System.Single[] Tayx.Graphy.Audio.G_AudioGraph::m_graphArrayHighestValue
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___m_graphArrayHighestValue_14;

public:
	inline static int32_t get_offset_of_m_imageGraph_4() { return static_cast<int32_t>(offsetof(G_AudioGraph_t02349ECE72DF71E24A86159551A347D323CB37CB, ___m_imageGraph_4)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_m_imageGraph_4() const { return ___m_imageGraph_4; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_m_imageGraph_4() { return &___m_imageGraph_4; }
	inline void set_m_imageGraph_4(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___m_imageGraph_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_imageGraph_4), value);
	}

	inline static int32_t get_offset_of_m_imageGraphHighestValues_5() { return static_cast<int32_t>(offsetof(G_AudioGraph_t02349ECE72DF71E24A86159551A347D323CB37CB, ___m_imageGraphHighestValues_5)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_m_imageGraphHighestValues_5() const { return ___m_imageGraphHighestValues_5; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_m_imageGraphHighestValues_5() { return &___m_imageGraphHighestValues_5; }
	inline void set_m_imageGraphHighestValues_5(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___m_imageGraphHighestValues_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_imageGraphHighestValues_5), value);
	}

	inline static int32_t get_offset_of_ShaderFull_6() { return static_cast<int32_t>(offsetof(G_AudioGraph_t02349ECE72DF71E24A86159551A347D323CB37CB, ___ShaderFull_6)); }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * get_ShaderFull_6() const { return ___ShaderFull_6; }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA ** get_address_of_ShaderFull_6() { return &___ShaderFull_6; }
	inline void set_ShaderFull_6(Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * value)
	{
		___ShaderFull_6 = value;
		Il2CppCodeGenWriteBarrier((&___ShaderFull_6), value);
	}

	inline static int32_t get_offset_of_ShaderLight_7() { return static_cast<int32_t>(offsetof(G_AudioGraph_t02349ECE72DF71E24A86159551A347D323CB37CB, ___ShaderLight_7)); }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * get_ShaderLight_7() const { return ___ShaderLight_7; }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA ** get_address_of_ShaderLight_7() { return &___ShaderLight_7; }
	inline void set_ShaderLight_7(Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * value)
	{
		___ShaderLight_7 = value;
		Il2CppCodeGenWriteBarrier((&___ShaderLight_7), value);
	}

	inline static int32_t get_offset_of_m_graphyManager_8() { return static_cast<int32_t>(offsetof(G_AudioGraph_t02349ECE72DF71E24A86159551A347D323CB37CB, ___m_graphyManager_8)); }
	inline GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF * get_m_graphyManager_8() const { return ___m_graphyManager_8; }
	inline GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF ** get_address_of_m_graphyManager_8() { return &___m_graphyManager_8; }
	inline void set_m_graphyManager_8(GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF * value)
	{
		___m_graphyManager_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_graphyManager_8), value);
	}

	inline static int32_t get_offset_of_m_audioMonitor_9() { return static_cast<int32_t>(offsetof(G_AudioGraph_t02349ECE72DF71E24A86159551A347D323CB37CB, ___m_audioMonitor_9)); }
	inline G_AudioMonitor_t386A2D77995058DA7241FC1C90AF9FF6DE38EA2E * get_m_audioMonitor_9() const { return ___m_audioMonitor_9; }
	inline G_AudioMonitor_t386A2D77995058DA7241FC1C90AF9FF6DE38EA2E ** get_address_of_m_audioMonitor_9() { return &___m_audioMonitor_9; }
	inline void set_m_audioMonitor_9(G_AudioMonitor_t386A2D77995058DA7241FC1C90AF9FF6DE38EA2E * value)
	{
		___m_audioMonitor_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_audioMonitor_9), value);
	}

	inline static int32_t get_offset_of_m_resolution_10() { return static_cast<int32_t>(offsetof(G_AudioGraph_t02349ECE72DF71E24A86159551A347D323CB37CB, ___m_resolution_10)); }
	inline int32_t get_m_resolution_10() const { return ___m_resolution_10; }
	inline int32_t* get_address_of_m_resolution_10() { return &___m_resolution_10; }
	inline void set_m_resolution_10(int32_t value)
	{
		___m_resolution_10 = value;
	}

	inline static int32_t get_offset_of_m_shaderGraph_11() { return static_cast<int32_t>(offsetof(G_AudioGraph_t02349ECE72DF71E24A86159551A347D323CB37CB, ___m_shaderGraph_11)); }
	inline G_GraphShader_t3D41D924F74863B5AFC8C101907225362954A8D3 * get_m_shaderGraph_11() const { return ___m_shaderGraph_11; }
	inline G_GraphShader_t3D41D924F74863B5AFC8C101907225362954A8D3 ** get_address_of_m_shaderGraph_11() { return &___m_shaderGraph_11; }
	inline void set_m_shaderGraph_11(G_GraphShader_t3D41D924F74863B5AFC8C101907225362954A8D3 * value)
	{
		___m_shaderGraph_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_shaderGraph_11), value);
	}

	inline static int32_t get_offset_of_m_shaderGraphHighestValues_12() { return static_cast<int32_t>(offsetof(G_AudioGraph_t02349ECE72DF71E24A86159551A347D323CB37CB, ___m_shaderGraphHighestValues_12)); }
	inline G_GraphShader_t3D41D924F74863B5AFC8C101907225362954A8D3 * get_m_shaderGraphHighestValues_12() const { return ___m_shaderGraphHighestValues_12; }
	inline G_GraphShader_t3D41D924F74863B5AFC8C101907225362954A8D3 ** get_address_of_m_shaderGraphHighestValues_12() { return &___m_shaderGraphHighestValues_12; }
	inline void set_m_shaderGraphHighestValues_12(G_GraphShader_t3D41D924F74863B5AFC8C101907225362954A8D3 * value)
	{
		___m_shaderGraphHighestValues_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_shaderGraphHighestValues_12), value);
	}

	inline static int32_t get_offset_of_m_graphArray_13() { return static_cast<int32_t>(offsetof(G_AudioGraph_t02349ECE72DF71E24A86159551A347D323CB37CB, ___m_graphArray_13)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_m_graphArray_13() const { return ___m_graphArray_13; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_m_graphArray_13() { return &___m_graphArray_13; }
	inline void set_m_graphArray_13(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___m_graphArray_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_graphArray_13), value);
	}

	inline static int32_t get_offset_of_m_graphArrayHighestValue_14() { return static_cast<int32_t>(offsetof(G_AudioGraph_t02349ECE72DF71E24A86159551A347D323CB37CB, ___m_graphArrayHighestValue_14)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_m_graphArrayHighestValue_14() const { return ___m_graphArrayHighestValue_14; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_m_graphArrayHighestValue_14() { return &___m_graphArrayHighestValue_14; }
	inline void set_m_graphArrayHighestValue_14(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___m_graphArrayHighestValue_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_graphArrayHighestValue_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // G_AUDIOGRAPH_T02349ECE72DF71E24A86159551A347D323CB37CB_H
#ifndef G_FPSGRAPH_T3DAC783275A67E078E4C1C2338F2DD9F03D14DEF_H
#define G_FPSGRAPH_T3DAC783275A67E078E4C1C2338F2DD9F03D14DEF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayx.Graphy.Fps.G_FpsGraph
struct  G_FpsGraph_t3DAC783275A67E078E4C1C2338F2DD9F03D14DEF  : public G_Graph_tC294DBE51D02C7EAB91E324525602C788CD5A9CC
{
public:
	// UnityEngine.UI.Image Tayx.Graphy.Fps.G_FpsGraph::m_imageGraph
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___m_imageGraph_4;
	// UnityEngine.Shader Tayx.Graphy.Fps.G_FpsGraph::ShaderFull
	Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * ___ShaderFull_5;
	// UnityEngine.Shader Tayx.Graphy.Fps.G_FpsGraph::ShaderLight
	Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * ___ShaderLight_6;
	// Tayx.Graphy.GraphyManager Tayx.Graphy.Fps.G_FpsGraph::m_graphyManager
	GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF * ___m_graphyManager_7;
	// Tayx.Graphy.Fps.G_FpsMonitor Tayx.Graphy.Fps.G_FpsGraph::m_fpsMonitor
	G_FpsMonitor_tDCDEDA2708585BE6B0202F2B3230D8C5F6820672 * ___m_fpsMonitor_8;
	// System.Int32 Tayx.Graphy.Fps.G_FpsGraph::m_resolution
	int32_t ___m_resolution_9;
	// Tayx.Graphy.G_GraphShader Tayx.Graphy.Fps.G_FpsGraph::m_shaderGraph
	G_GraphShader_t3D41D924F74863B5AFC8C101907225362954A8D3 * ___m_shaderGraph_10;
	// System.Int32[] Tayx.Graphy.Fps.G_FpsGraph::m_fpsArray
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___m_fpsArray_11;
	// System.Int32 Tayx.Graphy.Fps.G_FpsGraph::m_highestFps
	int32_t ___m_highestFps_12;

public:
	inline static int32_t get_offset_of_m_imageGraph_4() { return static_cast<int32_t>(offsetof(G_FpsGraph_t3DAC783275A67E078E4C1C2338F2DD9F03D14DEF, ___m_imageGraph_4)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_m_imageGraph_4() const { return ___m_imageGraph_4; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_m_imageGraph_4() { return &___m_imageGraph_4; }
	inline void set_m_imageGraph_4(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___m_imageGraph_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_imageGraph_4), value);
	}

	inline static int32_t get_offset_of_ShaderFull_5() { return static_cast<int32_t>(offsetof(G_FpsGraph_t3DAC783275A67E078E4C1C2338F2DD9F03D14DEF, ___ShaderFull_5)); }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * get_ShaderFull_5() const { return ___ShaderFull_5; }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA ** get_address_of_ShaderFull_5() { return &___ShaderFull_5; }
	inline void set_ShaderFull_5(Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * value)
	{
		___ShaderFull_5 = value;
		Il2CppCodeGenWriteBarrier((&___ShaderFull_5), value);
	}

	inline static int32_t get_offset_of_ShaderLight_6() { return static_cast<int32_t>(offsetof(G_FpsGraph_t3DAC783275A67E078E4C1C2338F2DD9F03D14DEF, ___ShaderLight_6)); }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * get_ShaderLight_6() const { return ___ShaderLight_6; }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA ** get_address_of_ShaderLight_6() { return &___ShaderLight_6; }
	inline void set_ShaderLight_6(Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * value)
	{
		___ShaderLight_6 = value;
		Il2CppCodeGenWriteBarrier((&___ShaderLight_6), value);
	}

	inline static int32_t get_offset_of_m_graphyManager_7() { return static_cast<int32_t>(offsetof(G_FpsGraph_t3DAC783275A67E078E4C1C2338F2DD9F03D14DEF, ___m_graphyManager_7)); }
	inline GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF * get_m_graphyManager_7() const { return ___m_graphyManager_7; }
	inline GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF ** get_address_of_m_graphyManager_7() { return &___m_graphyManager_7; }
	inline void set_m_graphyManager_7(GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF * value)
	{
		___m_graphyManager_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_graphyManager_7), value);
	}

	inline static int32_t get_offset_of_m_fpsMonitor_8() { return static_cast<int32_t>(offsetof(G_FpsGraph_t3DAC783275A67E078E4C1C2338F2DD9F03D14DEF, ___m_fpsMonitor_8)); }
	inline G_FpsMonitor_tDCDEDA2708585BE6B0202F2B3230D8C5F6820672 * get_m_fpsMonitor_8() const { return ___m_fpsMonitor_8; }
	inline G_FpsMonitor_tDCDEDA2708585BE6B0202F2B3230D8C5F6820672 ** get_address_of_m_fpsMonitor_8() { return &___m_fpsMonitor_8; }
	inline void set_m_fpsMonitor_8(G_FpsMonitor_tDCDEDA2708585BE6B0202F2B3230D8C5F6820672 * value)
	{
		___m_fpsMonitor_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_fpsMonitor_8), value);
	}

	inline static int32_t get_offset_of_m_resolution_9() { return static_cast<int32_t>(offsetof(G_FpsGraph_t3DAC783275A67E078E4C1C2338F2DD9F03D14DEF, ___m_resolution_9)); }
	inline int32_t get_m_resolution_9() const { return ___m_resolution_9; }
	inline int32_t* get_address_of_m_resolution_9() { return &___m_resolution_9; }
	inline void set_m_resolution_9(int32_t value)
	{
		___m_resolution_9 = value;
	}

	inline static int32_t get_offset_of_m_shaderGraph_10() { return static_cast<int32_t>(offsetof(G_FpsGraph_t3DAC783275A67E078E4C1C2338F2DD9F03D14DEF, ___m_shaderGraph_10)); }
	inline G_GraphShader_t3D41D924F74863B5AFC8C101907225362954A8D3 * get_m_shaderGraph_10() const { return ___m_shaderGraph_10; }
	inline G_GraphShader_t3D41D924F74863B5AFC8C101907225362954A8D3 ** get_address_of_m_shaderGraph_10() { return &___m_shaderGraph_10; }
	inline void set_m_shaderGraph_10(G_GraphShader_t3D41D924F74863B5AFC8C101907225362954A8D3 * value)
	{
		___m_shaderGraph_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_shaderGraph_10), value);
	}

	inline static int32_t get_offset_of_m_fpsArray_11() { return static_cast<int32_t>(offsetof(G_FpsGraph_t3DAC783275A67E078E4C1C2338F2DD9F03D14DEF, ___m_fpsArray_11)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_m_fpsArray_11() const { return ___m_fpsArray_11; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_m_fpsArray_11() { return &___m_fpsArray_11; }
	inline void set_m_fpsArray_11(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___m_fpsArray_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_fpsArray_11), value);
	}

	inline static int32_t get_offset_of_m_highestFps_12() { return static_cast<int32_t>(offsetof(G_FpsGraph_t3DAC783275A67E078E4C1C2338F2DD9F03D14DEF, ___m_highestFps_12)); }
	inline int32_t get_m_highestFps_12() const { return ___m_highestFps_12; }
	inline int32_t* get_address_of_m_highestFps_12() { return &___m_highestFps_12; }
	inline void set_m_highestFps_12(int32_t value)
	{
		___m_highestFps_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // G_FPSGRAPH_T3DAC783275A67E078E4C1C2338F2DD9F03D14DEF_H
#ifndef GRAPHYMANAGER_TEFC0816B65530C4EA0AB3871373B9A72E986A9DF_H
#define GRAPHYMANAGER_TEFC0816B65530C4EA0AB3871373B9A72E986A9DF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayx.Graphy.GraphyManager
struct  GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF  : public G_Singleton_1_tBF26C83E4663C941362F47E3A1E11E3520A22402
{
public:
	// Tayx.Graphy.GraphyManager_Mode Tayx.Graphy.GraphyManager::m_graphyMode
	int32_t ___m_graphyMode_7;
	// System.Boolean Tayx.Graphy.GraphyManager::m_enableOnStartup
	bool ___m_enableOnStartup_8;
	// System.Boolean Tayx.Graphy.GraphyManager::m_keepAlive
	bool ___m_keepAlive_9;
	// System.Boolean Tayx.Graphy.GraphyManager::m_background
	bool ___m_background_10;
	// UnityEngine.Color Tayx.Graphy.GraphyManager::m_backgroundColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_backgroundColor_11;
	// System.Boolean Tayx.Graphy.GraphyManager::m_enableHotkeys
	bool ___m_enableHotkeys_12;
	// UnityEngine.KeyCode Tayx.Graphy.GraphyManager::m_toggleModeKeyCode
	int32_t ___m_toggleModeKeyCode_13;
	// System.Boolean Tayx.Graphy.GraphyManager::m_toggleModeCtrl
	bool ___m_toggleModeCtrl_14;
	// System.Boolean Tayx.Graphy.GraphyManager::m_toggleModeAlt
	bool ___m_toggleModeAlt_15;
	// UnityEngine.KeyCode Tayx.Graphy.GraphyManager::m_toggleActiveKeyCode
	int32_t ___m_toggleActiveKeyCode_16;
	// System.Boolean Tayx.Graphy.GraphyManager::m_toggleActiveCtrl
	bool ___m_toggleActiveCtrl_17;
	// System.Boolean Tayx.Graphy.GraphyManager::m_toggleActiveAlt
	bool ___m_toggleActiveAlt_18;
	// Tayx.Graphy.GraphyManager_ModulePosition Tayx.Graphy.GraphyManager::m_graphModulePosition
	int32_t ___m_graphModulePosition_19;
	// Tayx.Graphy.GraphyManager_ModuleState Tayx.Graphy.GraphyManager::m_fpsModuleState
	int32_t ___m_fpsModuleState_20;
	// System.Int32 Tayx.Graphy.GraphyManager::m_timeToResetMinMaxFps
	int32_t ___m_timeToResetMinMaxFps_21;
	// UnityEngine.Color Tayx.Graphy.GraphyManager::m_goodFpsColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_goodFpsColor_22;
	// System.Int32 Tayx.Graphy.GraphyManager::m_goodFpsThreshold
	int32_t ___m_goodFpsThreshold_23;
	// UnityEngine.Color Tayx.Graphy.GraphyManager::m_cautionFpsColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_cautionFpsColor_24;
	// System.Int32 Tayx.Graphy.GraphyManager::m_cautionFpsThreshold
	int32_t ___m_cautionFpsThreshold_25;
	// UnityEngine.Color Tayx.Graphy.GraphyManager::m_criticalFpsColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_criticalFpsColor_26;
	// System.Int32 Tayx.Graphy.GraphyManager::m_fpsGraphResolution
	int32_t ___m_fpsGraphResolution_27;
	// System.Int32 Tayx.Graphy.GraphyManager::m_fpsTextUpdateRate
	int32_t ___m_fpsTextUpdateRate_28;
	// Tayx.Graphy.GraphyManager_ModuleState Tayx.Graphy.GraphyManager::m_ramModuleState
	int32_t ___m_ramModuleState_29;
	// UnityEngine.Color Tayx.Graphy.GraphyManager::m_allocatedRamColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_allocatedRamColor_30;
	// UnityEngine.Color Tayx.Graphy.GraphyManager::m_reservedRamColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_reservedRamColor_31;
	// UnityEngine.Color Tayx.Graphy.GraphyManager::m_monoRamColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_monoRamColor_32;
	// System.Int32 Tayx.Graphy.GraphyManager::m_ramGraphResolution
	int32_t ___m_ramGraphResolution_33;
	// System.Int32 Tayx.Graphy.GraphyManager::m_ramTextUpdateRate
	int32_t ___m_ramTextUpdateRate_34;
	// Tayx.Graphy.GraphyManager_ModuleState Tayx.Graphy.GraphyManager::m_audioModuleState
	int32_t ___m_audioModuleState_35;
	// Tayx.Graphy.GraphyManager_LookForAudioListener Tayx.Graphy.GraphyManager::m_findAudioListenerInCameraIfNull
	int32_t ___m_findAudioListenerInCameraIfNull_36;
	// UnityEngine.AudioListener Tayx.Graphy.GraphyManager::m_audioListener
	AudioListener_tE3E1467B84A4AFD509947B44A7C8ACFB67FF2099 * ___m_audioListener_37;
	// UnityEngine.Color Tayx.Graphy.GraphyManager::m_audioGraphColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_audioGraphColor_38;
	// System.Int32 Tayx.Graphy.GraphyManager::m_audioGraphResolution
	int32_t ___m_audioGraphResolution_39;
	// System.Int32 Tayx.Graphy.GraphyManager::m_audioTextUpdateRate
	int32_t ___m_audioTextUpdateRate_40;
	// UnityEngine.FFTWindow Tayx.Graphy.GraphyManager::m_FFTWindow
	int32_t ___m_FFTWindow_41;
	// System.Int32 Tayx.Graphy.GraphyManager::m_spectrumSize
	int32_t ___m_spectrumSize_42;
	// Tayx.Graphy.GraphyManager_ModulePosition Tayx.Graphy.GraphyManager::m_advancedModulePosition
	int32_t ___m_advancedModulePosition_43;
	// Tayx.Graphy.GraphyManager_ModuleState Tayx.Graphy.GraphyManager::m_advancedModuleState
	int32_t ___m_advancedModuleState_44;
	// System.Boolean Tayx.Graphy.GraphyManager::m_initialized
	bool ___m_initialized_45;
	// System.Boolean Tayx.Graphy.GraphyManager::m_active
	bool ___m_active_46;
	// System.Boolean Tayx.Graphy.GraphyManager::m_focused
	bool ___m_focused_47;
	// Tayx.Graphy.Fps.G_FpsManager Tayx.Graphy.GraphyManager::m_fpsManager
	G_FpsManager_tEB48DC94964043565EAC186648A2F0FCD4794243 * ___m_fpsManager_48;
	// Tayx.Graphy.Ram.G_RamManager Tayx.Graphy.GraphyManager::m_ramManager
	G_RamManager_t2C694E71E71ABB53B78046251A3430D920B9B0C9 * ___m_ramManager_49;
	// Tayx.Graphy.Audio.G_AudioManager Tayx.Graphy.GraphyManager::m_audioManager
	G_AudioManager_t19B7806D5B65E31D8B338BF53B13C8ACB6057F60 * ___m_audioManager_50;
	// Tayx.Graphy.Advanced.G_AdvancedData Tayx.Graphy.GraphyManager::m_advancedData
	G_AdvancedData_t0796834EAEDC1D020D0AF60EB6D8A19A6CB61F6E * ___m_advancedData_51;
	// Tayx.Graphy.Fps.G_FpsMonitor Tayx.Graphy.GraphyManager::m_fpsMonitor
	G_FpsMonitor_tDCDEDA2708585BE6B0202F2B3230D8C5F6820672 * ___m_fpsMonitor_52;
	// Tayx.Graphy.Ram.G_RamMonitor Tayx.Graphy.GraphyManager::m_ramMonitor
	G_RamMonitor_tCE95C3ECC0C03781E405698F56E20B12BC2650CA * ___m_ramMonitor_53;
	// Tayx.Graphy.Audio.G_AudioMonitor Tayx.Graphy.GraphyManager::m_audioMonitor
	G_AudioMonitor_t386A2D77995058DA7241FC1C90AF9FF6DE38EA2E * ___m_audioMonitor_54;
	// Tayx.Graphy.GraphyManager_ModulePreset Tayx.Graphy.GraphyManager::m_modulePresetState
	int32_t ___m_modulePresetState_55;

public:
	inline static int32_t get_offset_of_m_graphyMode_7() { return static_cast<int32_t>(offsetof(GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF, ___m_graphyMode_7)); }
	inline int32_t get_m_graphyMode_7() const { return ___m_graphyMode_7; }
	inline int32_t* get_address_of_m_graphyMode_7() { return &___m_graphyMode_7; }
	inline void set_m_graphyMode_7(int32_t value)
	{
		___m_graphyMode_7 = value;
	}

	inline static int32_t get_offset_of_m_enableOnStartup_8() { return static_cast<int32_t>(offsetof(GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF, ___m_enableOnStartup_8)); }
	inline bool get_m_enableOnStartup_8() const { return ___m_enableOnStartup_8; }
	inline bool* get_address_of_m_enableOnStartup_8() { return &___m_enableOnStartup_8; }
	inline void set_m_enableOnStartup_8(bool value)
	{
		___m_enableOnStartup_8 = value;
	}

	inline static int32_t get_offset_of_m_keepAlive_9() { return static_cast<int32_t>(offsetof(GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF, ___m_keepAlive_9)); }
	inline bool get_m_keepAlive_9() const { return ___m_keepAlive_9; }
	inline bool* get_address_of_m_keepAlive_9() { return &___m_keepAlive_9; }
	inline void set_m_keepAlive_9(bool value)
	{
		___m_keepAlive_9 = value;
	}

	inline static int32_t get_offset_of_m_background_10() { return static_cast<int32_t>(offsetof(GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF, ___m_background_10)); }
	inline bool get_m_background_10() const { return ___m_background_10; }
	inline bool* get_address_of_m_background_10() { return &___m_background_10; }
	inline void set_m_background_10(bool value)
	{
		___m_background_10 = value;
	}

	inline static int32_t get_offset_of_m_backgroundColor_11() { return static_cast<int32_t>(offsetof(GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF, ___m_backgroundColor_11)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_backgroundColor_11() const { return ___m_backgroundColor_11; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_backgroundColor_11() { return &___m_backgroundColor_11; }
	inline void set_m_backgroundColor_11(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_backgroundColor_11 = value;
	}

	inline static int32_t get_offset_of_m_enableHotkeys_12() { return static_cast<int32_t>(offsetof(GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF, ___m_enableHotkeys_12)); }
	inline bool get_m_enableHotkeys_12() const { return ___m_enableHotkeys_12; }
	inline bool* get_address_of_m_enableHotkeys_12() { return &___m_enableHotkeys_12; }
	inline void set_m_enableHotkeys_12(bool value)
	{
		___m_enableHotkeys_12 = value;
	}

	inline static int32_t get_offset_of_m_toggleModeKeyCode_13() { return static_cast<int32_t>(offsetof(GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF, ___m_toggleModeKeyCode_13)); }
	inline int32_t get_m_toggleModeKeyCode_13() const { return ___m_toggleModeKeyCode_13; }
	inline int32_t* get_address_of_m_toggleModeKeyCode_13() { return &___m_toggleModeKeyCode_13; }
	inline void set_m_toggleModeKeyCode_13(int32_t value)
	{
		___m_toggleModeKeyCode_13 = value;
	}

	inline static int32_t get_offset_of_m_toggleModeCtrl_14() { return static_cast<int32_t>(offsetof(GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF, ___m_toggleModeCtrl_14)); }
	inline bool get_m_toggleModeCtrl_14() const { return ___m_toggleModeCtrl_14; }
	inline bool* get_address_of_m_toggleModeCtrl_14() { return &___m_toggleModeCtrl_14; }
	inline void set_m_toggleModeCtrl_14(bool value)
	{
		___m_toggleModeCtrl_14 = value;
	}

	inline static int32_t get_offset_of_m_toggleModeAlt_15() { return static_cast<int32_t>(offsetof(GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF, ___m_toggleModeAlt_15)); }
	inline bool get_m_toggleModeAlt_15() const { return ___m_toggleModeAlt_15; }
	inline bool* get_address_of_m_toggleModeAlt_15() { return &___m_toggleModeAlt_15; }
	inline void set_m_toggleModeAlt_15(bool value)
	{
		___m_toggleModeAlt_15 = value;
	}

	inline static int32_t get_offset_of_m_toggleActiveKeyCode_16() { return static_cast<int32_t>(offsetof(GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF, ___m_toggleActiveKeyCode_16)); }
	inline int32_t get_m_toggleActiveKeyCode_16() const { return ___m_toggleActiveKeyCode_16; }
	inline int32_t* get_address_of_m_toggleActiveKeyCode_16() { return &___m_toggleActiveKeyCode_16; }
	inline void set_m_toggleActiveKeyCode_16(int32_t value)
	{
		___m_toggleActiveKeyCode_16 = value;
	}

	inline static int32_t get_offset_of_m_toggleActiveCtrl_17() { return static_cast<int32_t>(offsetof(GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF, ___m_toggleActiveCtrl_17)); }
	inline bool get_m_toggleActiveCtrl_17() const { return ___m_toggleActiveCtrl_17; }
	inline bool* get_address_of_m_toggleActiveCtrl_17() { return &___m_toggleActiveCtrl_17; }
	inline void set_m_toggleActiveCtrl_17(bool value)
	{
		___m_toggleActiveCtrl_17 = value;
	}

	inline static int32_t get_offset_of_m_toggleActiveAlt_18() { return static_cast<int32_t>(offsetof(GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF, ___m_toggleActiveAlt_18)); }
	inline bool get_m_toggleActiveAlt_18() const { return ___m_toggleActiveAlt_18; }
	inline bool* get_address_of_m_toggleActiveAlt_18() { return &___m_toggleActiveAlt_18; }
	inline void set_m_toggleActiveAlt_18(bool value)
	{
		___m_toggleActiveAlt_18 = value;
	}

	inline static int32_t get_offset_of_m_graphModulePosition_19() { return static_cast<int32_t>(offsetof(GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF, ___m_graphModulePosition_19)); }
	inline int32_t get_m_graphModulePosition_19() const { return ___m_graphModulePosition_19; }
	inline int32_t* get_address_of_m_graphModulePosition_19() { return &___m_graphModulePosition_19; }
	inline void set_m_graphModulePosition_19(int32_t value)
	{
		___m_graphModulePosition_19 = value;
	}

	inline static int32_t get_offset_of_m_fpsModuleState_20() { return static_cast<int32_t>(offsetof(GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF, ___m_fpsModuleState_20)); }
	inline int32_t get_m_fpsModuleState_20() const { return ___m_fpsModuleState_20; }
	inline int32_t* get_address_of_m_fpsModuleState_20() { return &___m_fpsModuleState_20; }
	inline void set_m_fpsModuleState_20(int32_t value)
	{
		___m_fpsModuleState_20 = value;
	}

	inline static int32_t get_offset_of_m_timeToResetMinMaxFps_21() { return static_cast<int32_t>(offsetof(GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF, ___m_timeToResetMinMaxFps_21)); }
	inline int32_t get_m_timeToResetMinMaxFps_21() const { return ___m_timeToResetMinMaxFps_21; }
	inline int32_t* get_address_of_m_timeToResetMinMaxFps_21() { return &___m_timeToResetMinMaxFps_21; }
	inline void set_m_timeToResetMinMaxFps_21(int32_t value)
	{
		___m_timeToResetMinMaxFps_21 = value;
	}

	inline static int32_t get_offset_of_m_goodFpsColor_22() { return static_cast<int32_t>(offsetof(GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF, ___m_goodFpsColor_22)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_goodFpsColor_22() const { return ___m_goodFpsColor_22; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_goodFpsColor_22() { return &___m_goodFpsColor_22; }
	inline void set_m_goodFpsColor_22(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_goodFpsColor_22 = value;
	}

	inline static int32_t get_offset_of_m_goodFpsThreshold_23() { return static_cast<int32_t>(offsetof(GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF, ___m_goodFpsThreshold_23)); }
	inline int32_t get_m_goodFpsThreshold_23() const { return ___m_goodFpsThreshold_23; }
	inline int32_t* get_address_of_m_goodFpsThreshold_23() { return &___m_goodFpsThreshold_23; }
	inline void set_m_goodFpsThreshold_23(int32_t value)
	{
		___m_goodFpsThreshold_23 = value;
	}

	inline static int32_t get_offset_of_m_cautionFpsColor_24() { return static_cast<int32_t>(offsetof(GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF, ___m_cautionFpsColor_24)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_cautionFpsColor_24() const { return ___m_cautionFpsColor_24; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_cautionFpsColor_24() { return &___m_cautionFpsColor_24; }
	inline void set_m_cautionFpsColor_24(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_cautionFpsColor_24 = value;
	}

	inline static int32_t get_offset_of_m_cautionFpsThreshold_25() { return static_cast<int32_t>(offsetof(GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF, ___m_cautionFpsThreshold_25)); }
	inline int32_t get_m_cautionFpsThreshold_25() const { return ___m_cautionFpsThreshold_25; }
	inline int32_t* get_address_of_m_cautionFpsThreshold_25() { return &___m_cautionFpsThreshold_25; }
	inline void set_m_cautionFpsThreshold_25(int32_t value)
	{
		___m_cautionFpsThreshold_25 = value;
	}

	inline static int32_t get_offset_of_m_criticalFpsColor_26() { return static_cast<int32_t>(offsetof(GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF, ___m_criticalFpsColor_26)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_criticalFpsColor_26() const { return ___m_criticalFpsColor_26; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_criticalFpsColor_26() { return &___m_criticalFpsColor_26; }
	inline void set_m_criticalFpsColor_26(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_criticalFpsColor_26 = value;
	}

	inline static int32_t get_offset_of_m_fpsGraphResolution_27() { return static_cast<int32_t>(offsetof(GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF, ___m_fpsGraphResolution_27)); }
	inline int32_t get_m_fpsGraphResolution_27() const { return ___m_fpsGraphResolution_27; }
	inline int32_t* get_address_of_m_fpsGraphResolution_27() { return &___m_fpsGraphResolution_27; }
	inline void set_m_fpsGraphResolution_27(int32_t value)
	{
		___m_fpsGraphResolution_27 = value;
	}

	inline static int32_t get_offset_of_m_fpsTextUpdateRate_28() { return static_cast<int32_t>(offsetof(GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF, ___m_fpsTextUpdateRate_28)); }
	inline int32_t get_m_fpsTextUpdateRate_28() const { return ___m_fpsTextUpdateRate_28; }
	inline int32_t* get_address_of_m_fpsTextUpdateRate_28() { return &___m_fpsTextUpdateRate_28; }
	inline void set_m_fpsTextUpdateRate_28(int32_t value)
	{
		___m_fpsTextUpdateRate_28 = value;
	}

	inline static int32_t get_offset_of_m_ramModuleState_29() { return static_cast<int32_t>(offsetof(GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF, ___m_ramModuleState_29)); }
	inline int32_t get_m_ramModuleState_29() const { return ___m_ramModuleState_29; }
	inline int32_t* get_address_of_m_ramModuleState_29() { return &___m_ramModuleState_29; }
	inline void set_m_ramModuleState_29(int32_t value)
	{
		___m_ramModuleState_29 = value;
	}

	inline static int32_t get_offset_of_m_allocatedRamColor_30() { return static_cast<int32_t>(offsetof(GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF, ___m_allocatedRamColor_30)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_allocatedRamColor_30() const { return ___m_allocatedRamColor_30; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_allocatedRamColor_30() { return &___m_allocatedRamColor_30; }
	inline void set_m_allocatedRamColor_30(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_allocatedRamColor_30 = value;
	}

	inline static int32_t get_offset_of_m_reservedRamColor_31() { return static_cast<int32_t>(offsetof(GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF, ___m_reservedRamColor_31)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_reservedRamColor_31() const { return ___m_reservedRamColor_31; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_reservedRamColor_31() { return &___m_reservedRamColor_31; }
	inline void set_m_reservedRamColor_31(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_reservedRamColor_31 = value;
	}

	inline static int32_t get_offset_of_m_monoRamColor_32() { return static_cast<int32_t>(offsetof(GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF, ___m_monoRamColor_32)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_monoRamColor_32() const { return ___m_monoRamColor_32; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_monoRamColor_32() { return &___m_monoRamColor_32; }
	inline void set_m_monoRamColor_32(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_monoRamColor_32 = value;
	}

	inline static int32_t get_offset_of_m_ramGraphResolution_33() { return static_cast<int32_t>(offsetof(GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF, ___m_ramGraphResolution_33)); }
	inline int32_t get_m_ramGraphResolution_33() const { return ___m_ramGraphResolution_33; }
	inline int32_t* get_address_of_m_ramGraphResolution_33() { return &___m_ramGraphResolution_33; }
	inline void set_m_ramGraphResolution_33(int32_t value)
	{
		___m_ramGraphResolution_33 = value;
	}

	inline static int32_t get_offset_of_m_ramTextUpdateRate_34() { return static_cast<int32_t>(offsetof(GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF, ___m_ramTextUpdateRate_34)); }
	inline int32_t get_m_ramTextUpdateRate_34() const { return ___m_ramTextUpdateRate_34; }
	inline int32_t* get_address_of_m_ramTextUpdateRate_34() { return &___m_ramTextUpdateRate_34; }
	inline void set_m_ramTextUpdateRate_34(int32_t value)
	{
		___m_ramTextUpdateRate_34 = value;
	}

	inline static int32_t get_offset_of_m_audioModuleState_35() { return static_cast<int32_t>(offsetof(GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF, ___m_audioModuleState_35)); }
	inline int32_t get_m_audioModuleState_35() const { return ___m_audioModuleState_35; }
	inline int32_t* get_address_of_m_audioModuleState_35() { return &___m_audioModuleState_35; }
	inline void set_m_audioModuleState_35(int32_t value)
	{
		___m_audioModuleState_35 = value;
	}

	inline static int32_t get_offset_of_m_findAudioListenerInCameraIfNull_36() { return static_cast<int32_t>(offsetof(GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF, ___m_findAudioListenerInCameraIfNull_36)); }
	inline int32_t get_m_findAudioListenerInCameraIfNull_36() const { return ___m_findAudioListenerInCameraIfNull_36; }
	inline int32_t* get_address_of_m_findAudioListenerInCameraIfNull_36() { return &___m_findAudioListenerInCameraIfNull_36; }
	inline void set_m_findAudioListenerInCameraIfNull_36(int32_t value)
	{
		___m_findAudioListenerInCameraIfNull_36 = value;
	}

	inline static int32_t get_offset_of_m_audioListener_37() { return static_cast<int32_t>(offsetof(GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF, ___m_audioListener_37)); }
	inline AudioListener_tE3E1467B84A4AFD509947B44A7C8ACFB67FF2099 * get_m_audioListener_37() const { return ___m_audioListener_37; }
	inline AudioListener_tE3E1467B84A4AFD509947B44A7C8ACFB67FF2099 ** get_address_of_m_audioListener_37() { return &___m_audioListener_37; }
	inline void set_m_audioListener_37(AudioListener_tE3E1467B84A4AFD509947B44A7C8ACFB67FF2099 * value)
	{
		___m_audioListener_37 = value;
		Il2CppCodeGenWriteBarrier((&___m_audioListener_37), value);
	}

	inline static int32_t get_offset_of_m_audioGraphColor_38() { return static_cast<int32_t>(offsetof(GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF, ___m_audioGraphColor_38)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_audioGraphColor_38() const { return ___m_audioGraphColor_38; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_audioGraphColor_38() { return &___m_audioGraphColor_38; }
	inline void set_m_audioGraphColor_38(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_audioGraphColor_38 = value;
	}

	inline static int32_t get_offset_of_m_audioGraphResolution_39() { return static_cast<int32_t>(offsetof(GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF, ___m_audioGraphResolution_39)); }
	inline int32_t get_m_audioGraphResolution_39() const { return ___m_audioGraphResolution_39; }
	inline int32_t* get_address_of_m_audioGraphResolution_39() { return &___m_audioGraphResolution_39; }
	inline void set_m_audioGraphResolution_39(int32_t value)
	{
		___m_audioGraphResolution_39 = value;
	}

	inline static int32_t get_offset_of_m_audioTextUpdateRate_40() { return static_cast<int32_t>(offsetof(GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF, ___m_audioTextUpdateRate_40)); }
	inline int32_t get_m_audioTextUpdateRate_40() const { return ___m_audioTextUpdateRate_40; }
	inline int32_t* get_address_of_m_audioTextUpdateRate_40() { return &___m_audioTextUpdateRate_40; }
	inline void set_m_audioTextUpdateRate_40(int32_t value)
	{
		___m_audioTextUpdateRate_40 = value;
	}

	inline static int32_t get_offset_of_m_FFTWindow_41() { return static_cast<int32_t>(offsetof(GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF, ___m_FFTWindow_41)); }
	inline int32_t get_m_FFTWindow_41() const { return ___m_FFTWindow_41; }
	inline int32_t* get_address_of_m_FFTWindow_41() { return &___m_FFTWindow_41; }
	inline void set_m_FFTWindow_41(int32_t value)
	{
		___m_FFTWindow_41 = value;
	}

	inline static int32_t get_offset_of_m_spectrumSize_42() { return static_cast<int32_t>(offsetof(GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF, ___m_spectrumSize_42)); }
	inline int32_t get_m_spectrumSize_42() const { return ___m_spectrumSize_42; }
	inline int32_t* get_address_of_m_spectrumSize_42() { return &___m_spectrumSize_42; }
	inline void set_m_spectrumSize_42(int32_t value)
	{
		___m_spectrumSize_42 = value;
	}

	inline static int32_t get_offset_of_m_advancedModulePosition_43() { return static_cast<int32_t>(offsetof(GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF, ___m_advancedModulePosition_43)); }
	inline int32_t get_m_advancedModulePosition_43() const { return ___m_advancedModulePosition_43; }
	inline int32_t* get_address_of_m_advancedModulePosition_43() { return &___m_advancedModulePosition_43; }
	inline void set_m_advancedModulePosition_43(int32_t value)
	{
		___m_advancedModulePosition_43 = value;
	}

	inline static int32_t get_offset_of_m_advancedModuleState_44() { return static_cast<int32_t>(offsetof(GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF, ___m_advancedModuleState_44)); }
	inline int32_t get_m_advancedModuleState_44() const { return ___m_advancedModuleState_44; }
	inline int32_t* get_address_of_m_advancedModuleState_44() { return &___m_advancedModuleState_44; }
	inline void set_m_advancedModuleState_44(int32_t value)
	{
		___m_advancedModuleState_44 = value;
	}

	inline static int32_t get_offset_of_m_initialized_45() { return static_cast<int32_t>(offsetof(GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF, ___m_initialized_45)); }
	inline bool get_m_initialized_45() const { return ___m_initialized_45; }
	inline bool* get_address_of_m_initialized_45() { return &___m_initialized_45; }
	inline void set_m_initialized_45(bool value)
	{
		___m_initialized_45 = value;
	}

	inline static int32_t get_offset_of_m_active_46() { return static_cast<int32_t>(offsetof(GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF, ___m_active_46)); }
	inline bool get_m_active_46() const { return ___m_active_46; }
	inline bool* get_address_of_m_active_46() { return &___m_active_46; }
	inline void set_m_active_46(bool value)
	{
		___m_active_46 = value;
	}

	inline static int32_t get_offset_of_m_focused_47() { return static_cast<int32_t>(offsetof(GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF, ___m_focused_47)); }
	inline bool get_m_focused_47() const { return ___m_focused_47; }
	inline bool* get_address_of_m_focused_47() { return &___m_focused_47; }
	inline void set_m_focused_47(bool value)
	{
		___m_focused_47 = value;
	}

	inline static int32_t get_offset_of_m_fpsManager_48() { return static_cast<int32_t>(offsetof(GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF, ___m_fpsManager_48)); }
	inline G_FpsManager_tEB48DC94964043565EAC186648A2F0FCD4794243 * get_m_fpsManager_48() const { return ___m_fpsManager_48; }
	inline G_FpsManager_tEB48DC94964043565EAC186648A2F0FCD4794243 ** get_address_of_m_fpsManager_48() { return &___m_fpsManager_48; }
	inline void set_m_fpsManager_48(G_FpsManager_tEB48DC94964043565EAC186648A2F0FCD4794243 * value)
	{
		___m_fpsManager_48 = value;
		Il2CppCodeGenWriteBarrier((&___m_fpsManager_48), value);
	}

	inline static int32_t get_offset_of_m_ramManager_49() { return static_cast<int32_t>(offsetof(GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF, ___m_ramManager_49)); }
	inline G_RamManager_t2C694E71E71ABB53B78046251A3430D920B9B0C9 * get_m_ramManager_49() const { return ___m_ramManager_49; }
	inline G_RamManager_t2C694E71E71ABB53B78046251A3430D920B9B0C9 ** get_address_of_m_ramManager_49() { return &___m_ramManager_49; }
	inline void set_m_ramManager_49(G_RamManager_t2C694E71E71ABB53B78046251A3430D920B9B0C9 * value)
	{
		___m_ramManager_49 = value;
		Il2CppCodeGenWriteBarrier((&___m_ramManager_49), value);
	}

	inline static int32_t get_offset_of_m_audioManager_50() { return static_cast<int32_t>(offsetof(GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF, ___m_audioManager_50)); }
	inline G_AudioManager_t19B7806D5B65E31D8B338BF53B13C8ACB6057F60 * get_m_audioManager_50() const { return ___m_audioManager_50; }
	inline G_AudioManager_t19B7806D5B65E31D8B338BF53B13C8ACB6057F60 ** get_address_of_m_audioManager_50() { return &___m_audioManager_50; }
	inline void set_m_audioManager_50(G_AudioManager_t19B7806D5B65E31D8B338BF53B13C8ACB6057F60 * value)
	{
		___m_audioManager_50 = value;
		Il2CppCodeGenWriteBarrier((&___m_audioManager_50), value);
	}

	inline static int32_t get_offset_of_m_advancedData_51() { return static_cast<int32_t>(offsetof(GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF, ___m_advancedData_51)); }
	inline G_AdvancedData_t0796834EAEDC1D020D0AF60EB6D8A19A6CB61F6E * get_m_advancedData_51() const { return ___m_advancedData_51; }
	inline G_AdvancedData_t0796834EAEDC1D020D0AF60EB6D8A19A6CB61F6E ** get_address_of_m_advancedData_51() { return &___m_advancedData_51; }
	inline void set_m_advancedData_51(G_AdvancedData_t0796834EAEDC1D020D0AF60EB6D8A19A6CB61F6E * value)
	{
		___m_advancedData_51 = value;
		Il2CppCodeGenWriteBarrier((&___m_advancedData_51), value);
	}

	inline static int32_t get_offset_of_m_fpsMonitor_52() { return static_cast<int32_t>(offsetof(GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF, ___m_fpsMonitor_52)); }
	inline G_FpsMonitor_tDCDEDA2708585BE6B0202F2B3230D8C5F6820672 * get_m_fpsMonitor_52() const { return ___m_fpsMonitor_52; }
	inline G_FpsMonitor_tDCDEDA2708585BE6B0202F2B3230D8C5F6820672 ** get_address_of_m_fpsMonitor_52() { return &___m_fpsMonitor_52; }
	inline void set_m_fpsMonitor_52(G_FpsMonitor_tDCDEDA2708585BE6B0202F2B3230D8C5F6820672 * value)
	{
		___m_fpsMonitor_52 = value;
		Il2CppCodeGenWriteBarrier((&___m_fpsMonitor_52), value);
	}

	inline static int32_t get_offset_of_m_ramMonitor_53() { return static_cast<int32_t>(offsetof(GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF, ___m_ramMonitor_53)); }
	inline G_RamMonitor_tCE95C3ECC0C03781E405698F56E20B12BC2650CA * get_m_ramMonitor_53() const { return ___m_ramMonitor_53; }
	inline G_RamMonitor_tCE95C3ECC0C03781E405698F56E20B12BC2650CA ** get_address_of_m_ramMonitor_53() { return &___m_ramMonitor_53; }
	inline void set_m_ramMonitor_53(G_RamMonitor_tCE95C3ECC0C03781E405698F56E20B12BC2650CA * value)
	{
		___m_ramMonitor_53 = value;
		Il2CppCodeGenWriteBarrier((&___m_ramMonitor_53), value);
	}

	inline static int32_t get_offset_of_m_audioMonitor_54() { return static_cast<int32_t>(offsetof(GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF, ___m_audioMonitor_54)); }
	inline G_AudioMonitor_t386A2D77995058DA7241FC1C90AF9FF6DE38EA2E * get_m_audioMonitor_54() const { return ___m_audioMonitor_54; }
	inline G_AudioMonitor_t386A2D77995058DA7241FC1C90AF9FF6DE38EA2E ** get_address_of_m_audioMonitor_54() { return &___m_audioMonitor_54; }
	inline void set_m_audioMonitor_54(G_AudioMonitor_t386A2D77995058DA7241FC1C90AF9FF6DE38EA2E * value)
	{
		___m_audioMonitor_54 = value;
		Il2CppCodeGenWriteBarrier((&___m_audioMonitor_54), value);
	}

	inline static int32_t get_offset_of_m_modulePresetState_55() { return static_cast<int32_t>(offsetof(GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF, ___m_modulePresetState_55)); }
	inline int32_t get_m_modulePresetState_55() const { return ___m_modulePresetState_55; }
	inline int32_t* get_address_of_m_modulePresetState_55() { return &___m_modulePresetState_55; }
	inline void set_m_modulePresetState_55(int32_t value)
	{
		___m_modulePresetState_55 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHYMANAGER_TEFC0816B65530C4EA0AB3871373B9A72E986A9DF_H
#ifndef G_RAMGRAPH_T3408E644B21F1A8F7E4A4932CB72BBE7483130CE_H
#define G_RAMGRAPH_T3408E644B21F1A8F7E4A4932CB72BBE7483130CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayx.Graphy.Ram.G_RamGraph
struct  G_RamGraph_t3408E644B21F1A8F7E4A4932CB72BBE7483130CE  : public G_Graph_tC294DBE51D02C7EAB91E324525602C788CD5A9CC
{
public:
	// UnityEngine.UI.Image Tayx.Graphy.Ram.G_RamGraph::m_imageAllocated
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___m_imageAllocated_4;
	// UnityEngine.UI.Image Tayx.Graphy.Ram.G_RamGraph::m_imageReserved
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___m_imageReserved_5;
	// UnityEngine.UI.Image Tayx.Graphy.Ram.G_RamGraph::m_imageMono
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___m_imageMono_6;
	// UnityEngine.Shader Tayx.Graphy.Ram.G_RamGraph::ShaderFull
	Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * ___ShaderFull_7;
	// UnityEngine.Shader Tayx.Graphy.Ram.G_RamGraph::ShaderLight
	Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * ___ShaderLight_8;
	// Tayx.Graphy.GraphyManager Tayx.Graphy.Ram.G_RamGraph::m_graphyManager
	GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF * ___m_graphyManager_9;
	// Tayx.Graphy.Ram.G_RamMonitor Tayx.Graphy.Ram.G_RamGraph::m_ramMonitor
	G_RamMonitor_tCE95C3ECC0C03781E405698F56E20B12BC2650CA * ___m_ramMonitor_10;
	// System.Int32 Tayx.Graphy.Ram.G_RamGraph::m_resolution
	int32_t ___m_resolution_11;
	// Tayx.Graphy.G_GraphShader Tayx.Graphy.Ram.G_RamGraph::m_shaderGraphAllocated
	G_GraphShader_t3D41D924F74863B5AFC8C101907225362954A8D3 * ___m_shaderGraphAllocated_12;
	// Tayx.Graphy.G_GraphShader Tayx.Graphy.Ram.G_RamGraph::m_shaderGraphReserved
	G_GraphShader_t3D41D924F74863B5AFC8C101907225362954A8D3 * ___m_shaderGraphReserved_13;
	// Tayx.Graphy.G_GraphShader Tayx.Graphy.Ram.G_RamGraph::m_shaderGraphMono
	G_GraphShader_t3D41D924F74863B5AFC8C101907225362954A8D3 * ___m_shaderGraphMono_14;
	// System.Single[] Tayx.Graphy.Ram.G_RamGraph::m_allocatedArray
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___m_allocatedArray_15;
	// System.Single[] Tayx.Graphy.Ram.G_RamGraph::m_reservedArray
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___m_reservedArray_16;
	// System.Single[] Tayx.Graphy.Ram.G_RamGraph::m_monoArray
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___m_monoArray_17;
	// System.Single Tayx.Graphy.Ram.G_RamGraph::m_highestMemory
	float ___m_highestMemory_18;

public:
	inline static int32_t get_offset_of_m_imageAllocated_4() { return static_cast<int32_t>(offsetof(G_RamGraph_t3408E644B21F1A8F7E4A4932CB72BBE7483130CE, ___m_imageAllocated_4)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_m_imageAllocated_4() const { return ___m_imageAllocated_4; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_m_imageAllocated_4() { return &___m_imageAllocated_4; }
	inline void set_m_imageAllocated_4(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___m_imageAllocated_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_imageAllocated_4), value);
	}

	inline static int32_t get_offset_of_m_imageReserved_5() { return static_cast<int32_t>(offsetof(G_RamGraph_t3408E644B21F1A8F7E4A4932CB72BBE7483130CE, ___m_imageReserved_5)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_m_imageReserved_5() const { return ___m_imageReserved_5; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_m_imageReserved_5() { return &___m_imageReserved_5; }
	inline void set_m_imageReserved_5(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___m_imageReserved_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_imageReserved_5), value);
	}

	inline static int32_t get_offset_of_m_imageMono_6() { return static_cast<int32_t>(offsetof(G_RamGraph_t3408E644B21F1A8F7E4A4932CB72BBE7483130CE, ___m_imageMono_6)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_m_imageMono_6() const { return ___m_imageMono_6; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_m_imageMono_6() { return &___m_imageMono_6; }
	inline void set_m_imageMono_6(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___m_imageMono_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_imageMono_6), value);
	}

	inline static int32_t get_offset_of_ShaderFull_7() { return static_cast<int32_t>(offsetof(G_RamGraph_t3408E644B21F1A8F7E4A4932CB72BBE7483130CE, ___ShaderFull_7)); }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * get_ShaderFull_7() const { return ___ShaderFull_7; }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA ** get_address_of_ShaderFull_7() { return &___ShaderFull_7; }
	inline void set_ShaderFull_7(Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * value)
	{
		___ShaderFull_7 = value;
		Il2CppCodeGenWriteBarrier((&___ShaderFull_7), value);
	}

	inline static int32_t get_offset_of_ShaderLight_8() { return static_cast<int32_t>(offsetof(G_RamGraph_t3408E644B21F1A8F7E4A4932CB72BBE7483130CE, ___ShaderLight_8)); }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * get_ShaderLight_8() const { return ___ShaderLight_8; }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA ** get_address_of_ShaderLight_8() { return &___ShaderLight_8; }
	inline void set_ShaderLight_8(Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * value)
	{
		___ShaderLight_8 = value;
		Il2CppCodeGenWriteBarrier((&___ShaderLight_8), value);
	}

	inline static int32_t get_offset_of_m_graphyManager_9() { return static_cast<int32_t>(offsetof(G_RamGraph_t3408E644B21F1A8F7E4A4932CB72BBE7483130CE, ___m_graphyManager_9)); }
	inline GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF * get_m_graphyManager_9() const { return ___m_graphyManager_9; }
	inline GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF ** get_address_of_m_graphyManager_9() { return &___m_graphyManager_9; }
	inline void set_m_graphyManager_9(GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF * value)
	{
		___m_graphyManager_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_graphyManager_9), value);
	}

	inline static int32_t get_offset_of_m_ramMonitor_10() { return static_cast<int32_t>(offsetof(G_RamGraph_t3408E644B21F1A8F7E4A4932CB72BBE7483130CE, ___m_ramMonitor_10)); }
	inline G_RamMonitor_tCE95C3ECC0C03781E405698F56E20B12BC2650CA * get_m_ramMonitor_10() const { return ___m_ramMonitor_10; }
	inline G_RamMonitor_tCE95C3ECC0C03781E405698F56E20B12BC2650CA ** get_address_of_m_ramMonitor_10() { return &___m_ramMonitor_10; }
	inline void set_m_ramMonitor_10(G_RamMonitor_tCE95C3ECC0C03781E405698F56E20B12BC2650CA * value)
	{
		___m_ramMonitor_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_ramMonitor_10), value);
	}

	inline static int32_t get_offset_of_m_resolution_11() { return static_cast<int32_t>(offsetof(G_RamGraph_t3408E644B21F1A8F7E4A4932CB72BBE7483130CE, ___m_resolution_11)); }
	inline int32_t get_m_resolution_11() const { return ___m_resolution_11; }
	inline int32_t* get_address_of_m_resolution_11() { return &___m_resolution_11; }
	inline void set_m_resolution_11(int32_t value)
	{
		___m_resolution_11 = value;
	}

	inline static int32_t get_offset_of_m_shaderGraphAllocated_12() { return static_cast<int32_t>(offsetof(G_RamGraph_t3408E644B21F1A8F7E4A4932CB72BBE7483130CE, ___m_shaderGraphAllocated_12)); }
	inline G_GraphShader_t3D41D924F74863B5AFC8C101907225362954A8D3 * get_m_shaderGraphAllocated_12() const { return ___m_shaderGraphAllocated_12; }
	inline G_GraphShader_t3D41D924F74863B5AFC8C101907225362954A8D3 ** get_address_of_m_shaderGraphAllocated_12() { return &___m_shaderGraphAllocated_12; }
	inline void set_m_shaderGraphAllocated_12(G_GraphShader_t3D41D924F74863B5AFC8C101907225362954A8D3 * value)
	{
		___m_shaderGraphAllocated_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_shaderGraphAllocated_12), value);
	}

	inline static int32_t get_offset_of_m_shaderGraphReserved_13() { return static_cast<int32_t>(offsetof(G_RamGraph_t3408E644B21F1A8F7E4A4932CB72BBE7483130CE, ___m_shaderGraphReserved_13)); }
	inline G_GraphShader_t3D41D924F74863B5AFC8C101907225362954A8D3 * get_m_shaderGraphReserved_13() const { return ___m_shaderGraphReserved_13; }
	inline G_GraphShader_t3D41D924F74863B5AFC8C101907225362954A8D3 ** get_address_of_m_shaderGraphReserved_13() { return &___m_shaderGraphReserved_13; }
	inline void set_m_shaderGraphReserved_13(G_GraphShader_t3D41D924F74863B5AFC8C101907225362954A8D3 * value)
	{
		___m_shaderGraphReserved_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_shaderGraphReserved_13), value);
	}

	inline static int32_t get_offset_of_m_shaderGraphMono_14() { return static_cast<int32_t>(offsetof(G_RamGraph_t3408E644B21F1A8F7E4A4932CB72BBE7483130CE, ___m_shaderGraphMono_14)); }
	inline G_GraphShader_t3D41D924F74863B5AFC8C101907225362954A8D3 * get_m_shaderGraphMono_14() const { return ___m_shaderGraphMono_14; }
	inline G_GraphShader_t3D41D924F74863B5AFC8C101907225362954A8D3 ** get_address_of_m_shaderGraphMono_14() { return &___m_shaderGraphMono_14; }
	inline void set_m_shaderGraphMono_14(G_GraphShader_t3D41D924F74863B5AFC8C101907225362954A8D3 * value)
	{
		___m_shaderGraphMono_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_shaderGraphMono_14), value);
	}

	inline static int32_t get_offset_of_m_allocatedArray_15() { return static_cast<int32_t>(offsetof(G_RamGraph_t3408E644B21F1A8F7E4A4932CB72BBE7483130CE, ___m_allocatedArray_15)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_m_allocatedArray_15() const { return ___m_allocatedArray_15; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_m_allocatedArray_15() { return &___m_allocatedArray_15; }
	inline void set_m_allocatedArray_15(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___m_allocatedArray_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_allocatedArray_15), value);
	}

	inline static int32_t get_offset_of_m_reservedArray_16() { return static_cast<int32_t>(offsetof(G_RamGraph_t3408E644B21F1A8F7E4A4932CB72BBE7483130CE, ___m_reservedArray_16)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_m_reservedArray_16() const { return ___m_reservedArray_16; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_m_reservedArray_16() { return &___m_reservedArray_16; }
	inline void set_m_reservedArray_16(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___m_reservedArray_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_reservedArray_16), value);
	}

	inline static int32_t get_offset_of_m_monoArray_17() { return static_cast<int32_t>(offsetof(G_RamGraph_t3408E644B21F1A8F7E4A4932CB72BBE7483130CE, ___m_monoArray_17)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_m_monoArray_17() const { return ___m_monoArray_17; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_m_monoArray_17() { return &___m_monoArray_17; }
	inline void set_m_monoArray_17(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___m_monoArray_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_monoArray_17), value);
	}

	inline static int32_t get_offset_of_m_highestMemory_18() { return static_cast<int32_t>(offsetof(G_RamGraph_t3408E644B21F1A8F7E4A4932CB72BBE7483130CE, ___m_highestMemory_18)); }
	inline float get_m_highestMemory_18() const { return ___m_highestMemory_18; }
	inline float* get_address_of_m_highestMemory_18() { return &___m_highestMemory_18; }
	inline void set_m_highestMemory_18(float value)
	{
		___m_highestMemory_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // G_RAMGRAPH_T3408E644B21F1A8F7E4A4932CB72BBE7483130CE_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7700 = { sizeof (DebugComparer_tCB311680F0549F814BFC2425FB2E8BB8E1B09FB0)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7700[6] = 
{
	DebugComparer_tCB311680F0549F814BFC2425FB2E8BB8E1B09FB0::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7701 = { sizeof (ConditionEvaluation_t0D296B50D90D5D57015600337C2F187306360B41)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7701[3] = 
{
	ConditionEvaluation_t0D296B50D90D5D57015600337C2F187306360B41::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7702 = { sizeof (MessageType_t8C988B371F31BB759293369DCB06F840CC7071BE)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7702[4] = 
{
	MessageType_t8C988B371F31BB759293369DCB06F840CC7071BE::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7703 = { sizeof (DebugCondition_tF8420D6FE8E0424DBBE01BA12DC0D3EDAA503A28)+ sizeof (RuntimeObject), sizeof(DebugCondition_tF8420D6FE8E0424DBBE01BA12DC0D3EDAA503A28 ), 0, 0 };
extern const int32_t g_FieldOffsetTable7703[3] = 
{
	DebugCondition_tF8420D6FE8E0424DBBE01BA12DC0D3EDAA503A28::get_offset_of_Variable_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DebugCondition_tF8420D6FE8E0424DBBE01BA12DC0D3EDAA503A28::get_offset_of_Comparer_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DebugCondition_tF8420D6FE8E0424DBBE01BA12DC0D3EDAA503A28::get_offset_of_Value_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7704 = { sizeof (DebugPacket_t05CCF6D8D66B7CB75FC9F561F62FCA17802CD590), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7704[17] = 
{
	DebugPacket_t05CCF6D8D66B7CB75FC9F561F62FCA17802CD590::get_offset_of_Active_0(),
	DebugPacket_t05CCF6D8D66B7CB75FC9F561F62FCA17802CD590::get_offset_of_Id_1(),
	DebugPacket_t05CCF6D8D66B7CB75FC9F561F62FCA17802CD590::get_offset_of_ExecuteOnce_2(),
	DebugPacket_t05CCF6D8D66B7CB75FC9F561F62FCA17802CD590::get_offset_of_InitSleepTime_3(),
	DebugPacket_t05CCF6D8D66B7CB75FC9F561F62FCA17802CD590::get_offset_of_ExecuteSleepTime_4(),
	DebugPacket_t05CCF6D8D66B7CB75FC9F561F62FCA17802CD590::get_offset_of_ConditionEvaluation_5(),
	DebugPacket_t05CCF6D8D66B7CB75FC9F561F62FCA17802CD590::get_offset_of_DebugConditions_6(),
	DebugPacket_t05CCF6D8D66B7CB75FC9F561F62FCA17802CD590::get_offset_of_MessageType_7(),
	DebugPacket_t05CCF6D8D66B7CB75FC9F561F62FCA17802CD590::get_offset_of_Message_8(),
	DebugPacket_t05CCF6D8D66B7CB75FC9F561F62FCA17802CD590::get_offset_of_TakeScreenshot_9(),
	DebugPacket_t05CCF6D8D66B7CB75FC9F561F62FCA17802CD590::get_offset_of_ScreenshotFileName_10(),
	DebugPacket_t05CCF6D8D66B7CB75FC9F561F62FCA17802CD590::get_offset_of_DebugBreak_11(),
	DebugPacket_t05CCF6D8D66B7CB75FC9F561F62FCA17802CD590::get_offset_of_UnityEvents_12(),
	DebugPacket_t05CCF6D8D66B7CB75FC9F561F62FCA17802CD590::get_offset_of_Callbacks_13(),
	DebugPacket_t05CCF6D8D66B7CB75FC9F561F62FCA17802CD590::get_offset_of_canBeChecked_14(),
	DebugPacket_t05CCF6D8D66B7CB75FC9F561F62FCA17802CD590::get_offset_of_executed_15(),
	DebugPacket_t05CCF6D8D66B7CB75FC9F561F62FCA17802CD590::get_offset_of_timePassed_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7705 = { sizeof (U3CU3Ec__DisplayClass18_0_tC525A1655E9BA745C7370C785F2C8D3AB0AE5F00), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7705[1] = 
{
	U3CU3Ec__DisplayClass18_0_tC525A1655E9BA745C7370C785F2C8D3AB0AE5F00::get_offset_of_packetId_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7706 = { sizeof (U3CU3Ec__DisplayClass19_0_t5E47DBDDAF14E12B236F95335BF75EB7471BA900), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7706[1] = 
{
	U3CU3Ec__DisplayClass19_0_t5E47DBDDAF14E12B236F95335BF75EB7471BA900::get_offset_of_packetId_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7707 = { sizeof (U3CU3Ec__DisplayClass21_0_t41B4ACE0CF1F630C5EDBC7AC123DC7F1B5726534), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7707[1] = 
{
	U3CU3Ec__DisplayClass21_0_t41B4ACE0CF1F630C5EDBC7AC123DC7F1B5726534::get_offset_of_packetId_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7708 = { sizeof (U3CU3Ec_t27C553FE38FB7E37C2611A42F15FA22A8E408636), -1, sizeof(U3CU3Ec_t27C553FE38FB7E37C2611A42F15FA22A8E408636_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7708[2] = 
{
	U3CU3Ec_t27C553FE38FB7E37C2611A42F15FA22A8E408636_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t27C553FE38FB7E37C2611A42F15FA22A8E408636_StaticFields::get_offset_of_U3CU3E9__24_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7709 = { sizeof (GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7709[49] = 
{
	GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF::get_offset_of_m_graphyMode_7(),
	GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF::get_offset_of_m_enableOnStartup_8(),
	GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF::get_offset_of_m_keepAlive_9(),
	GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF::get_offset_of_m_background_10(),
	GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF::get_offset_of_m_backgroundColor_11(),
	GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF::get_offset_of_m_enableHotkeys_12(),
	GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF::get_offset_of_m_toggleModeKeyCode_13(),
	GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF::get_offset_of_m_toggleModeCtrl_14(),
	GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF::get_offset_of_m_toggleModeAlt_15(),
	GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF::get_offset_of_m_toggleActiveKeyCode_16(),
	GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF::get_offset_of_m_toggleActiveCtrl_17(),
	GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF::get_offset_of_m_toggleActiveAlt_18(),
	GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF::get_offset_of_m_graphModulePosition_19(),
	GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF::get_offset_of_m_fpsModuleState_20(),
	GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF::get_offset_of_m_timeToResetMinMaxFps_21(),
	GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF::get_offset_of_m_goodFpsColor_22(),
	GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF::get_offset_of_m_goodFpsThreshold_23(),
	GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF::get_offset_of_m_cautionFpsColor_24(),
	GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF::get_offset_of_m_cautionFpsThreshold_25(),
	GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF::get_offset_of_m_criticalFpsColor_26(),
	GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF::get_offset_of_m_fpsGraphResolution_27(),
	GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF::get_offset_of_m_fpsTextUpdateRate_28(),
	GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF::get_offset_of_m_ramModuleState_29(),
	GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF::get_offset_of_m_allocatedRamColor_30(),
	GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF::get_offset_of_m_reservedRamColor_31(),
	GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF::get_offset_of_m_monoRamColor_32(),
	GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF::get_offset_of_m_ramGraphResolution_33(),
	GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF::get_offset_of_m_ramTextUpdateRate_34(),
	GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF::get_offset_of_m_audioModuleState_35(),
	GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF::get_offset_of_m_findAudioListenerInCameraIfNull_36(),
	GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF::get_offset_of_m_audioListener_37(),
	GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF::get_offset_of_m_audioGraphColor_38(),
	GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF::get_offset_of_m_audioGraphResolution_39(),
	GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF::get_offset_of_m_audioTextUpdateRate_40(),
	GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF::get_offset_of_m_FFTWindow_41(),
	GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF::get_offset_of_m_spectrumSize_42(),
	GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF::get_offset_of_m_advancedModulePosition_43(),
	GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF::get_offset_of_m_advancedModuleState_44(),
	GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF::get_offset_of_m_initialized_45(),
	GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF::get_offset_of_m_active_46(),
	GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF::get_offset_of_m_focused_47(),
	GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF::get_offset_of_m_fpsManager_48(),
	GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF::get_offset_of_m_ramManager_49(),
	GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF::get_offset_of_m_audioManager_50(),
	GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF::get_offset_of_m_advancedData_51(),
	GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF::get_offset_of_m_fpsMonitor_52(),
	GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF::get_offset_of_m_ramMonitor_53(),
	GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF::get_offset_of_m_audioMonitor_54(),
	GraphyManager_tEFC0816B65530C4EA0AB3871373B9A72E986A9DF::get_offset_of_m_modulePresetState_55(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7710 = { sizeof (Mode_t667FFFB5CD98FFF5F5CF160C75A8D5DABDEE01D3)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7710[3] = 
{
	Mode_t667FFFB5CD98FFF5F5CF160C75A8D5DABDEE01D3::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7711 = { sizeof (ModuleType_tEBD56C8E3621C37A842EC97568034D28DFE4AD7F)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7711[5] = 
{
	ModuleType_tEBD56C8E3621C37A842EC97568034D28DFE4AD7F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7712 = { sizeof (ModuleState_t86FF08A91528850D39B1CC8D7BF087667469645D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7712[6] = 
{
	ModuleState_t86FF08A91528850D39B1CC8D7BF087667469645D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7713 = { sizeof (ModulePosition_t9E6A5FC6F6A46E059894821A6008D4B883B61BF4)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7713[6] = 
{
	ModulePosition_t9E6A5FC6F6A46E059894821A6008D4B883B61BF4::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7714 = { sizeof (LookForAudioListener_t15A9234D069E6D39793B8D9A1F86967B26C2E595)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7714[4] = 
{
	LookForAudioListener_t15A9234D069E6D39793B8D9A1F86967B26C2E595::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7715 = { sizeof (ModulePreset_tCF6CF54709DEE9D6CDB813678E7D8024F182E5F8)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7715[13] = 
{
	ModulePreset_tCF6CF54709DEE9D6CDB813678E7D8024F182E5F8::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7716 = { sizeof (G_GraphShader_t3D41D924F74863B5AFC8C101907225362954A8D3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7716[19] = 
{
	0,
	0,
	G_GraphShader_t3D41D924F74863B5AFC8C101907225362954A8D3::get_offset_of_ArrayMaxSize_2(),
	G_GraphShader_t3D41D924F74863B5AFC8C101907225362954A8D3::get_offset_of_Array_3(),
	G_GraphShader_t3D41D924F74863B5AFC8C101907225362954A8D3::get_offset_of_Image_4(),
	G_GraphShader_t3D41D924F74863B5AFC8C101907225362954A8D3::get_offset_of_Name_5(),
	G_GraphShader_t3D41D924F74863B5AFC8C101907225362954A8D3::get_offset_of_Name_Length_6(),
	G_GraphShader_t3D41D924F74863B5AFC8C101907225362954A8D3::get_offset_of_Average_7(),
	G_GraphShader_t3D41D924F74863B5AFC8C101907225362954A8D3::get_offset_of_averagePropertyId_8(),
	G_GraphShader_t3D41D924F74863B5AFC8C101907225362954A8D3::get_offset_of_GoodThreshold_9(),
	G_GraphShader_t3D41D924F74863B5AFC8C101907225362954A8D3::get_offset_of_CautionThreshold_10(),
	G_GraphShader_t3D41D924F74863B5AFC8C101907225362954A8D3::get_offset_of_goodThresholdPropertyId_11(),
	G_GraphShader_t3D41D924F74863B5AFC8C101907225362954A8D3::get_offset_of_cautionThresholdPropertyId_12(),
	G_GraphShader_t3D41D924F74863B5AFC8C101907225362954A8D3::get_offset_of_GoodColor_13(),
	G_GraphShader_t3D41D924F74863B5AFC8C101907225362954A8D3::get_offset_of_CautionColor_14(),
	G_GraphShader_t3D41D924F74863B5AFC8C101907225362954A8D3::get_offset_of_CriticalColor_15(),
	G_GraphShader_t3D41D924F74863B5AFC8C101907225362954A8D3::get_offset_of_goodColorPropertyId_16(),
	G_GraphShader_t3D41D924F74863B5AFC8C101907225362954A8D3::get_offset_of_cautionColorPropertyId_17(),
	G_GraphShader_t3D41D924F74863B5AFC8C101907225362954A8D3::get_offset_of_criticalColorPropertyId_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7717 = { sizeof (G_ExtensionMethods_tF66F9E35495E364A880753E446D08308CFE3E276), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7718 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7718[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7719 = { sizeof (G_FloatString_t6D89C4A8C15ABBC570E80DEC140A4EA53EB955D6), -1, sizeof(G_FloatString_t6D89C4A8C15ABBC570E80DEC140A4EA53EB955D6_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7719[4] = 
{
	0,
	G_FloatString_t6D89C4A8C15ABBC570E80DEC140A4EA53EB955D6_StaticFields::get_offset_of_decimalMultiplier_1(),
	G_FloatString_t6D89C4A8C15ABBC570E80DEC140A4EA53EB955D6_StaticFields::get_offset_of_negativeBuffer_2(),
	G_FloatString_t6D89C4A8C15ABBC570E80DEC140A4EA53EB955D6_StaticFields::get_offset_of_positiveBuffer_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7720 = { sizeof (G_IntString_tD13FE91428976CAA79DA24721DD0FA959CABDE64), -1, sizeof(G_IntString_tD13FE91428976CAA79DA24721DD0FA959CABDE64_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7720[2] = 
{
	G_IntString_tD13FE91428976CAA79DA24721DD0FA959CABDE64_StaticFields::get_offset_of_negativeBuffer_0(),
	G_IntString_tD13FE91428976CAA79DA24721DD0FA959CABDE64_StaticFields::get_offset_of_positiveBuffer_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7721 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7722 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7723 = { sizeof (G_RamGraph_t3408E644B21F1A8F7E4A4932CB72BBE7483130CE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7723[15] = 
{
	G_RamGraph_t3408E644B21F1A8F7E4A4932CB72BBE7483130CE::get_offset_of_m_imageAllocated_4(),
	G_RamGraph_t3408E644B21F1A8F7E4A4932CB72BBE7483130CE::get_offset_of_m_imageReserved_5(),
	G_RamGraph_t3408E644B21F1A8F7E4A4932CB72BBE7483130CE::get_offset_of_m_imageMono_6(),
	G_RamGraph_t3408E644B21F1A8F7E4A4932CB72BBE7483130CE::get_offset_of_ShaderFull_7(),
	G_RamGraph_t3408E644B21F1A8F7E4A4932CB72BBE7483130CE::get_offset_of_ShaderLight_8(),
	G_RamGraph_t3408E644B21F1A8F7E4A4932CB72BBE7483130CE::get_offset_of_m_graphyManager_9(),
	G_RamGraph_t3408E644B21F1A8F7E4A4932CB72BBE7483130CE::get_offset_of_m_ramMonitor_10(),
	G_RamGraph_t3408E644B21F1A8F7E4A4932CB72BBE7483130CE::get_offset_of_m_resolution_11(),
	G_RamGraph_t3408E644B21F1A8F7E4A4932CB72BBE7483130CE::get_offset_of_m_shaderGraphAllocated_12(),
	G_RamGraph_t3408E644B21F1A8F7E4A4932CB72BBE7483130CE::get_offset_of_m_shaderGraphReserved_13(),
	G_RamGraph_t3408E644B21F1A8F7E4A4932CB72BBE7483130CE::get_offset_of_m_shaderGraphMono_14(),
	G_RamGraph_t3408E644B21F1A8F7E4A4932CB72BBE7483130CE::get_offset_of_m_allocatedArray_15(),
	G_RamGraph_t3408E644B21F1A8F7E4A4932CB72BBE7483130CE::get_offset_of_m_reservedArray_16(),
	G_RamGraph_t3408E644B21F1A8F7E4A4932CB72BBE7483130CE::get_offset_of_m_monoArray_17(),
	G_RamGraph_t3408E644B21F1A8F7E4A4932CB72BBE7483130CE::get_offset_of_m_highestMemory_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7724 = { sizeof (G_RamManager_t2C694E71E71ABB53B78046251A3430D920B9B0C9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7724[9] = 
{
	G_RamManager_t2C694E71E71ABB53B78046251A3430D920B9B0C9::get_offset_of_m_ramGraphGameObject_4(),
	G_RamManager_t2C694E71E71ABB53B78046251A3430D920B9B0C9::get_offset_of_m_backgroundImages_5(),
	G_RamManager_t2C694E71E71ABB53B78046251A3430D920B9B0C9::get_offset_of_m_graphyManager_6(),
	G_RamManager_t2C694E71E71ABB53B78046251A3430D920B9B0C9::get_offset_of_m_ramGraph_7(),
	G_RamManager_t2C694E71E71ABB53B78046251A3430D920B9B0C9::get_offset_of_m_ramText_8(),
	G_RamManager_t2C694E71E71ABB53B78046251A3430D920B9B0C9::get_offset_of_m_rectTransform_9(),
	G_RamManager_t2C694E71E71ABB53B78046251A3430D920B9B0C9::get_offset_of_m_childrenGameObjects_10(),
	G_RamManager_t2C694E71E71ABB53B78046251A3430D920B9B0C9::get_offset_of_m_previousModuleState_11(),
	G_RamManager_t2C694E71E71ABB53B78046251A3430D920B9B0C9::get_offset_of_m_currentModuleState_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7725 = { sizeof (G_RamMonitor_tCE95C3ECC0C03781E405698F56E20B12BC2650CA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7725[3] = 
{
	G_RamMonitor_tCE95C3ECC0C03781E405698F56E20B12BC2650CA::get_offset_of_m_allocatedRam_4(),
	G_RamMonitor_tCE95C3ECC0C03781E405698F56E20B12BC2650CA::get_offset_of_m_reservedRam_5(),
	G_RamMonitor_tCE95C3ECC0C03781E405698F56E20B12BC2650CA::get_offset_of_m_monoRam_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7726 = { sizeof (G_RamText_t1097A490F592CBE6D7CF0EF4BA0232252CDE5806), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7726[8] = 
{
	G_RamText_t1097A490F592CBE6D7CF0EF4BA0232252CDE5806::get_offset_of_m_allocatedSystemMemorySizeText_4(),
	G_RamText_t1097A490F592CBE6D7CF0EF4BA0232252CDE5806::get_offset_of_m_reservedSystemMemorySizeText_5(),
	G_RamText_t1097A490F592CBE6D7CF0EF4BA0232252CDE5806::get_offset_of_m_monoSystemMemorySizeText_6(),
	G_RamText_t1097A490F592CBE6D7CF0EF4BA0232252CDE5806::get_offset_of_m_graphyManager_7(),
	G_RamText_t1097A490F592CBE6D7CF0EF4BA0232252CDE5806::get_offset_of_m_ramMonitor_8(),
	G_RamText_t1097A490F592CBE6D7CF0EF4BA0232252CDE5806::get_offset_of_m_updateRate_9(),
	G_RamText_t1097A490F592CBE6D7CF0EF4BA0232252CDE5806::get_offset_of_m_deltaTime_10(),
	G_RamText_t1097A490F592CBE6D7CF0EF4BA0232252CDE5806::get_offset_of_m_memoryStringFormat_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7727 = { sizeof (G_Graph_tC294DBE51D02C7EAB91E324525602C788CD5A9CC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7728 = { sizeof (G_FpsGraph_t3DAC783275A67E078E4C1C2338F2DD9F03D14DEF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7728[9] = 
{
	G_FpsGraph_t3DAC783275A67E078E4C1C2338F2DD9F03D14DEF::get_offset_of_m_imageGraph_4(),
	G_FpsGraph_t3DAC783275A67E078E4C1C2338F2DD9F03D14DEF::get_offset_of_ShaderFull_5(),
	G_FpsGraph_t3DAC783275A67E078E4C1C2338F2DD9F03D14DEF::get_offset_of_ShaderLight_6(),
	G_FpsGraph_t3DAC783275A67E078E4C1C2338F2DD9F03D14DEF::get_offset_of_m_graphyManager_7(),
	G_FpsGraph_t3DAC783275A67E078E4C1C2338F2DD9F03D14DEF::get_offset_of_m_fpsMonitor_8(),
	G_FpsGraph_t3DAC783275A67E078E4C1C2338F2DD9F03D14DEF::get_offset_of_m_resolution_9(),
	G_FpsGraph_t3DAC783275A67E078E4C1C2338F2DD9F03D14DEF::get_offset_of_m_shaderGraph_10(),
	G_FpsGraph_t3DAC783275A67E078E4C1C2338F2DD9F03D14DEF::get_offset_of_m_fpsArray_11(),
	G_FpsGraph_t3DAC783275A67E078E4C1C2338F2DD9F03D14DEF::get_offset_of_m_highestFps_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7729 = { sizeof (G_FpsManager_tEB48DC94964043565EAC186648A2F0FCD4794243), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7729[11] = 
{
	G_FpsManager_tEB48DC94964043565EAC186648A2F0FCD4794243::get_offset_of_m_fpsGraphGameObject_4(),
	G_FpsManager_tEB48DC94964043565EAC186648A2F0FCD4794243::get_offset_of_m_nonBasicTextGameObjects_5(),
	G_FpsManager_tEB48DC94964043565EAC186648A2F0FCD4794243::get_offset_of_m_backgroundImages_6(),
	G_FpsManager_tEB48DC94964043565EAC186648A2F0FCD4794243::get_offset_of_m_graphyManager_7(),
	G_FpsManager_tEB48DC94964043565EAC186648A2F0FCD4794243::get_offset_of_m_fpsGraph_8(),
	G_FpsManager_tEB48DC94964043565EAC186648A2F0FCD4794243::get_offset_of_m_fpsMonitor_9(),
	G_FpsManager_tEB48DC94964043565EAC186648A2F0FCD4794243::get_offset_of_m_fpsText_10(),
	G_FpsManager_tEB48DC94964043565EAC186648A2F0FCD4794243::get_offset_of_m_rectTransform_11(),
	G_FpsManager_tEB48DC94964043565EAC186648A2F0FCD4794243::get_offset_of_m_childrenGameObjects_12(),
	G_FpsManager_tEB48DC94964043565EAC186648A2F0FCD4794243::get_offset_of_m_previousModuleState_13(),
	G_FpsManager_tEB48DC94964043565EAC186648A2F0FCD4794243::get_offset_of_m_currentModuleState_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7730 = { sizeof (G_FpsMonitor_tDCDEDA2708585BE6B0202F2B3230D8C5F6820672), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7730[11] = 
{
	G_FpsMonitor_tDCDEDA2708585BE6B0202F2B3230D8C5F6820672::get_offset_of_m_averageSamples_4(),
	G_FpsMonitor_tDCDEDA2708585BE6B0202F2B3230D8C5F6820672::get_offset_of_m_graphyManager_5(),
	G_FpsMonitor_tDCDEDA2708585BE6B0202F2B3230D8C5F6820672::get_offset_of_m_currentFps_6(),
	G_FpsMonitor_tDCDEDA2708585BE6B0202F2B3230D8C5F6820672::get_offset_of_m_avgFps_7(),
	G_FpsMonitor_tDCDEDA2708585BE6B0202F2B3230D8C5F6820672::get_offset_of_m_minFps_8(),
	G_FpsMonitor_tDCDEDA2708585BE6B0202F2B3230D8C5F6820672::get_offset_of_m_maxFps_9(),
	G_FpsMonitor_tDCDEDA2708585BE6B0202F2B3230D8C5F6820672::get_offset_of_m_averageFpsSamples_10(),
	G_FpsMonitor_tDCDEDA2708585BE6B0202F2B3230D8C5F6820672::get_offset_of_m_timeToResetMinMaxFps_11(),
	G_FpsMonitor_tDCDEDA2708585BE6B0202F2B3230D8C5F6820672::get_offset_of_m_timeToResetMinFpsPassed_12(),
	G_FpsMonitor_tDCDEDA2708585BE6B0202F2B3230D8C5F6820672::get_offset_of_m_timeToResetMaxFpsPassed_13(),
	G_FpsMonitor_tDCDEDA2708585BE6B0202F2B3230D8C5F6820672::get_offset_of_unscaledDeltaTime_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7731 = { sizeof (G_FpsText_tDA7B016BED64F8C283E352FEB686BD8372567DE5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7731[14] = 
{
	G_FpsText_tDA7B016BED64F8C283E352FEB686BD8372567DE5::get_offset_of_m_fpsText_4(),
	G_FpsText_tDA7B016BED64F8C283E352FEB686BD8372567DE5::get_offset_of_m_msText_5(),
	G_FpsText_tDA7B016BED64F8C283E352FEB686BD8372567DE5::get_offset_of_m_avgFpsText_6(),
	G_FpsText_tDA7B016BED64F8C283E352FEB686BD8372567DE5::get_offset_of_m_minFpsText_7(),
	G_FpsText_tDA7B016BED64F8C283E352FEB686BD8372567DE5::get_offset_of_m_maxFpsText_8(),
	G_FpsText_tDA7B016BED64F8C283E352FEB686BD8372567DE5::get_offset_of_m_graphyManager_9(),
	G_FpsText_tDA7B016BED64F8C283E352FEB686BD8372567DE5::get_offset_of_m_fpsMonitor_10(),
	G_FpsText_tDA7B016BED64F8C283E352FEB686BD8372567DE5::get_offset_of_m_updateRate_11(),
	G_FpsText_tDA7B016BED64F8C283E352FEB686BD8372567DE5::get_offset_of_m_frameCount_12(),
	G_FpsText_tDA7B016BED64F8C283E352FEB686BD8372567DE5::get_offset_of_m_deltaTime_13(),
	G_FpsText_tDA7B016BED64F8C283E352FEB686BD8372567DE5::get_offset_of_m_fps_14(),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7732 = { sizeof (G_AudioGraph_t02349ECE72DF71E24A86159551A347D323CB37CB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7732[11] = 
{
	G_AudioGraph_t02349ECE72DF71E24A86159551A347D323CB37CB::get_offset_of_m_imageGraph_4(),
	G_AudioGraph_t02349ECE72DF71E24A86159551A347D323CB37CB::get_offset_of_m_imageGraphHighestValues_5(),
	G_AudioGraph_t02349ECE72DF71E24A86159551A347D323CB37CB::get_offset_of_ShaderFull_6(),
	G_AudioGraph_t02349ECE72DF71E24A86159551A347D323CB37CB::get_offset_of_ShaderLight_7(),
	G_AudioGraph_t02349ECE72DF71E24A86159551A347D323CB37CB::get_offset_of_m_graphyManager_8(),
	G_AudioGraph_t02349ECE72DF71E24A86159551A347D323CB37CB::get_offset_of_m_audioMonitor_9(),
	G_AudioGraph_t02349ECE72DF71E24A86159551A347D323CB37CB::get_offset_of_m_resolution_10(),
	G_AudioGraph_t02349ECE72DF71E24A86159551A347D323CB37CB::get_offset_of_m_shaderGraph_11(),
	G_AudioGraph_t02349ECE72DF71E24A86159551A347D323CB37CB::get_offset_of_m_shaderGraphHighestValues_12(),
	G_AudioGraph_t02349ECE72DF71E24A86159551A347D323CB37CB::get_offset_of_m_graphArray_13(),
	G_AudioGraph_t02349ECE72DF71E24A86159551A347D323CB37CB::get_offset_of_m_graphArrayHighestValue_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7733 = { sizeof (G_AudioManager_t19B7806D5B65E31D8B338BF53B13C8ACB6057F60), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7733[11] = 
{
	G_AudioManager_t19B7806D5B65E31D8B338BF53B13C8ACB6057F60::get_offset_of_m_audioGraphGameObject_4(),
	G_AudioManager_t19B7806D5B65E31D8B338BF53B13C8ACB6057F60::get_offset_of_m_audioDbText_5(),
	G_AudioManager_t19B7806D5B65E31D8B338BF53B13C8ACB6057F60::get_offset_of_m_backgroundImages_6(),
	G_AudioManager_t19B7806D5B65E31D8B338BF53B13C8ACB6057F60::get_offset_of_m_graphyManager_7(),
	G_AudioManager_t19B7806D5B65E31D8B338BF53B13C8ACB6057F60::get_offset_of_m_audioGraph_8(),
	G_AudioManager_t19B7806D5B65E31D8B338BF53B13C8ACB6057F60::get_offset_of_m_audioMonitor_9(),
	G_AudioManager_t19B7806D5B65E31D8B338BF53B13C8ACB6057F60::get_offset_of_m_audioText_10(),
	G_AudioManager_t19B7806D5B65E31D8B338BF53B13C8ACB6057F60::get_offset_of_m_rectTransform_11(),
	G_AudioManager_t19B7806D5B65E31D8B338BF53B13C8ACB6057F60::get_offset_of_m_childrenGameObjects_12(),
	G_AudioManager_t19B7806D5B65E31D8B338BF53B13C8ACB6057F60::get_offset_of_m_previousModuleState_13(),
	G_AudioManager_t19B7806D5B65E31D8B338BF53B13C8ACB6057F60::get_offset_of_m_currentModuleState_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7734 = { sizeof (G_AudioMonitor_t386A2D77995058DA7241FC1C90AF9FF6DE38EA2E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7734[9] = 
{
	0,
	G_AudioMonitor_t386A2D77995058DA7241FC1C90AF9FF6DE38EA2E::get_offset_of_m_graphyManager_5(),
	G_AudioMonitor_t386A2D77995058DA7241FC1C90AF9FF6DE38EA2E::get_offset_of_m_audioListener_6(),
	G_AudioMonitor_t386A2D77995058DA7241FC1C90AF9FF6DE38EA2E::get_offset_of_m_findAudioListenerInCameraIfNull_7(),
	G_AudioMonitor_t386A2D77995058DA7241FC1C90AF9FF6DE38EA2E::get_offset_of_m_FFTWindow_8(),
	G_AudioMonitor_t386A2D77995058DA7241FC1C90AF9FF6DE38EA2E::get_offset_of_m_spectrumSize_9(),
	G_AudioMonitor_t386A2D77995058DA7241FC1C90AF9FF6DE38EA2E::get_offset_of_m_spectrum_10(),
	G_AudioMonitor_t386A2D77995058DA7241FC1C90AF9FF6DE38EA2E::get_offset_of_m_spectrumHighestValues_11(),
	G_AudioMonitor_t386A2D77995058DA7241FC1C90AF9FF6DE38EA2E::get_offset_of_m_maxDB_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7735 = { sizeof (G_AudioText_t7BCE4D478232F4284809B83E3150DBCC8DC64BF3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7735[5] = 
{
	G_AudioText_t7BCE4D478232F4284809B83E3150DBCC8DC64BF3::get_offset_of_m_DBText_4(),
	G_AudioText_t7BCE4D478232F4284809B83E3150DBCC8DC64BF3::get_offset_of_m_graphyManager_5(),
	G_AudioText_t7BCE4D478232F4284809B83E3150DBCC8DC64BF3::get_offset_of_m_audioMonitor_6(),
	G_AudioText_t7BCE4D478232F4284809B83E3150DBCC8DC64BF3::get_offset_of_m_updateRate_7(),
	G_AudioText_t7BCE4D478232F4284809B83E3150DBCC8DC64BF3::get_offset_of_m_deltaTimeOffset_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7736 = { sizeof (G_AdvancedData_t0796834EAEDC1D020D0AF60EB6D8A19A6CB61F6E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7736[17] = 
{
	G_AdvancedData_t0796834EAEDC1D020D0AF60EB6D8A19A6CB61F6E::get_offset_of_m_backgroundImages_4(),
	G_AdvancedData_t0796834EAEDC1D020D0AF60EB6D8A19A6CB61F6E::get_offset_of_m_graphicsDeviceVersionText_5(),
	G_AdvancedData_t0796834EAEDC1D020D0AF60EB6D8A19A6CB61F6E::get_offset_of_m_processorTypeText_6(),
	G_AdvancedData_t0796834EAEDC1D020D0AF60EB6D8A19A6CB61F6E::get_offset_of_m_operatingSystemText_7(),
	G_AdvancedData_t0796834EAEDC1D020D0AF60EB6D8A19A6CB61F6E::get_offset_of_m_systemMemoryText_8(),
	G_AdvancedData_t0796834EAEDC1D020D0AF60EB6D8A19A6CB61F6E::get_offset_of_m_graphicsDeviceNameText_9(),
	G_AdvancedData_t0796834EAEDC1D020D0AF60EB6D8A19A6CB61F6E::get_offset_of_m_graphicsMemorySizeText_10(),
	G_AdvancedData_t0796834EAEDC1D020D0AF60EB6D8A19A6CB61F6E::get_offset_of_m_screenResolutionText_11(),
	G_AdvancedData_t0796834EAEDC1D020D0AF60EB6D8A19A6CB61F6E::get_offset_of_m_gameWindowResolutionText_12(),
	G_AdvancedData_t0796834EAEDC1D020D0AF60EB6D8A19A6CB61F6E::get_offset_of_m_updateRate_13(),
	G_AdvancedData_t0796834EAEDC1D020D0AF60EB6D8A19A6CB61F6E::get_offset_of_m_graphyManager_14(),
	G_AdvancedData_t0796834EAEDC1D020D0AF60EB6D8A19A6CB61F6E::get_offset_of_m_rectTransform_15(),
	G_AdvancedData_t0796834EAEDC1D020D0AF60EB6D8A19A6CB61F6E::get_offset_of_m_deltaTime_16(),
	G_AdvancedData_t0796834EAEDC1D020D0AF60EB6D8A19A6CB61F6E::get_offset_of_m_sb_17(),
	G_AdvancedData_t0796834EAEDC1D020D0AF60EB6D8A19A6CB61F6E::get_offset_of_m_previousModuleState_18(),
	G_AdvancedData_t0796834EAEDC1D020D0AF60EB6D8A19A6CB61F6E::get_offset_of_m_currentModuleState_19(),
	G_AdvancedData_t0796834EAEDC1D020D0AF60EB6D8A19A6CB61F6E::get_offset_of_m_windowStrings_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7737 = { sizeof (G_CUIColorPicker_t65243BB7AF1C8840635414A3F44F39FCBB1B3D83), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7737[5] = 
{
	G_CUIColorPicker_t65243BB7AF1C8840635414A3F44F39FCBB1B3D83::get_offset_of_alphaSlider_4(),
	G_CUIColorPicker_t65243BB7AF1C8840635414A3F44F39FCBB1B3D83::get_offset_of_alphaSliderBGImage_5(),
	G_CUIColorPicker_t65243BB7AF1C8840635414A3F44F39FCBB1B3D83::get_offset_of__color_6(),
	G_CUIColorPicker_t65243BB7AF1C8840635414A3F44F39FCBB1B3D83::get_offset_of__onValueChange_7(),
	G_CUIColorPicker_t65243BB7AF1C8840635414A3F44F39FCBB1B3D83::get_offset_of__update_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7738 = { sizeof (U3CU3Ec__DisplayClass13_0_t41DB50CD101C7F4552F773579AFE24F88D55FB96), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7738[20] = 
{
	U3CU3Ec__DisplayClass13_0_t41DB50CD101C7F4552F773579AFE24F88D55FB96::get_offset_of_satvalTex_0(),
	U3CU3Ec__DisplayClass13_0_t41DB50CD101C7F4552F773579AFE24F88D55FB96::get_offset_of_satvalColors_1(),
	U3CU3Ec__DisplayClass13_0_t41DB50CD101C7F4552F773579AFE24F88D55FB96::get_offset_of_Hue_2(),
	U3CU3Ec__DisplayClass13_0_t41DB50CD101C7F4552F773579AFE24F88D55FB96::get_offset_of_hueColors_3(),
	U3CU3Ec__DisplayClass13_0_t41DB50CD101C7F4552F773579AFE24F88D55FB96::get_offset_of_resetSatValTexture_4(),
	U3CU3Ec__DisplayClass13_0_t41DB50CD101C7F4552F773579AFE24F88D55FB96::get_offset_of_Saturation_5(),
	U3CU3Ec__DisplayClass13_0_t41DB50CD101C7F4552F773579AFE24F88D55FB96::get_offset_of_Value_6(),
	U3CU3Ec__DisplayClass13_0_t41DB50CD101C7F4552F773579AFE24F88D55FB96::get_offset_of_result_7(),
	U3CU3Ec__DisplayClass13_0_t41DB50CD101C7F4552F773579AFE24F88D55FB96::get_offset_of_U3CU3E4__this_8(),
	U3CU3Ec__DisplayClass13_0_t41DB50CD101C7F4552F773579AFE24F88D55FB96::get_offset_of_hueGO_9(),
	U3CU3Ec__DisplayClass13_0_t41DB50CD101C7F4552F773579AFE24F88D55FB96::get_offset_of_dragH_10(),
	U3CU3Ec__DisplayClass13_0_t41DB50CD101C7F4552F773579AFE24F88D55FB96::get_offset_of_satvalGO_11(),
	U3CU3Ec__DisplayClass13_0_t41DB50CD101C7F4552F773579AFE24F88D55FB96::get_offset_of_dragSV_12(),
	U3CU3Ec__DisplayClass13_0_t41DB50CD101C7F4552F773579AFE24F88D55FB96::get_offset_of_hueSz_13(),
	U3CU3Ec__DisplayClass13_0_t41DB50CD101C7F4552F773579AFE24F88D55FB96::get_offset_of_applyHue_14(),
	U3CU3Ec__DisplayClass13_0_t41DB50CD101C7F4552F773579AFE24F88D55FB96::get_offset_of_applySaturationValue_15(),
	U3CU3Ec__DisplayClass13_0_t41DB50CD101C7F4552F773579AFE24F88D55FB96::get_offset_of_hueKnob_16(),
	U3CU3Ec__DisplayClass13_0_t41DB50CD101C7F4552F773579AFE24F88D55FB96::get_offset_of_idle_17(),
	U3CU3Ec__DisplayClass13_0_t41DB50CD101C7F4552F773579AFE24F88D55FB96::get_offset_of_satvalSz_18(),
	U3CU3Ec__DisplayClass13_0_t41DB50CD101C7F4552F773579AFE24F88D55FB96::get_offset_of_satvalKnob_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7739 = { sizeof (CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7739[37] = 
{
	CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF::get_offset_of_m_colorPicker_4(),
	CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF::get_offset_of_m_backgroundToggle_5(),
	CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF::get_offset_of_m_graphyModeDropdown_6(),
	CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF::get_offset_of_m_backgroundColorButton_7(),
	CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF::get_offset_of_m_graphModulePositionDropdown_8(),
	CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF::get_offset_of_m_fpsModuleStateDropdown_9(),
	CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF::get_offset_of_m_goodInputField_10(),
	CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF::get_offset_of_m_cautionInputField_11(),
	CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF::get_offset_of_m_goodColorButton_12(),
	CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF::get_offset_of_m_cautionColorButton_13(),
	CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF::get_offset_of_m_criticalColorButton_14(),
	CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF::get_offset_of_m_timeToResetMinMaxSlider_15(),
	CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF::get_offset_of_m_fpsGraphResolutionSlider_16(),
	CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF::get_offset_of_m_fpsTextUpdateRateSlider_17(),
	CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF::get_offset_of_m_ramModuleStateDropdown_18(),
	CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF::get_offset_of_m_reservedColorButton_19(),
	CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF::get_offset_of_m_allocatedColorButton_20(),
	CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF::get_offset_of_m_monoColorButton_21(),
	CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF::get_offset_of_m_ramGraphResolutionSlider_22(),
	CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF::get_offset_of_m_ramTextUpdateRateSlider_23(),
	CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF::get_offset_of_m_audioModuleStateDropdown_24(),
	CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF::get_offset_of_m_audioGraphColorButton_25(),
	CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF::get_offset_of_m_findAudioListenerDropdown_26(),
	CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF::get_offset_of_m_fttWindowDropdown_27(),
	CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF::get_offset_of_m_spectrumSizeSlider_28(),
	CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF::get_offset_of_m_audioGraphResolutionSlider_29(),
	CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF::get_offset_of_m_audioTextUpdateRateSlider_30(),
	CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF::get_offset_of_m_advancedModulePositionDropdown_31(),
	CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF::get_offset_of_m_advancedModuleToggle_32(),
	CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF::get_offset_of_m_musicButton_33(),
	CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF::get_offset_of_m_sfxButton_34(),
	CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF::get_offset_of_m_musicVolumeSlider_35(),
	CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF::get_offset_of_m_sfxVolumeSlider_36(),
	CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF::get_offset_of_m_musicAudioSource_37(),
	CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF::get_offset_of_m_sfxAudioSource_38(),
	CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF::get_offset_of_m_sfxAudioClips_39(),
	CustomizeGraphy_tC2261ED0BAB21EF20627C51AB165620ABF7325BF::get_offset_of_m_graphyManager_40(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7740 = { sizeof (ForceSliderToMultipleOf3_t758350D319AC12D824AF0489E81E2433E2282284), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7740[1] = 
{
	ForceSliderToMultipleOf3_t758350D319AC12D824AF0489E81E2433E2282284::get_offset_of_m_slider_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7741 = { sizeof (ForceSliderToPowerOf2_t330FD4A336FD5026A124672AA063E35451ABEFA6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7741[3] = 
{
	ForceSliderToPowerOf2_t330FD4A336FD5026A124672AA063E35451ABEFA6::get_offset_of_m_slider_4(),
	ForceSliderToPowerOf2_t330FD4A336FD5026A124672AA063E35451ABEFA6::get_offset_of_m_powerOf2Values_5(),
	ForceSliderToPowerOf2_t330FD4A336FD5026A124672AA063E35451ABEFA6::get_offset_of_m_text_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7742 = { sizeof (UpdateTextWithSliderValue_t2260AC538D1286A58088474F49BB13DFFB51160A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7742[2] = 
{
	UpdateTextWithSliderValue_t2260AC538D1286A58088474F49BB13DFFB51160A::get_offset_of_m_slider_4(),
	UpdateTextWithSliderValue_t2260AC538D1286A58088474F49BB13DFFB51160A::get_offset_of_m_text_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7743 = { sizeof (Check_t12CC5342B775CEA4EF3935C4572D5CAAD7A99834), -1, sizeof(Check_t12CC5342B775CEA4EF3935C4572D5CAAD7A99834_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7743[1] = 
{
	Check_t12CC5342B775CEA4EF3935C4572D5CAAD7A99834_StaticFields::get_offset_of_useAssertions_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7744 = { sizeof (Trace_t0AC06925CD6AFB335DCBDE5EA04106D467EDC943), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7745 = { sizeof (DesignByContractException_t8D28E8A87F2986C2D9F7C4FA9B4AB37C8B884A45), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7746 = { sizeof (PreconditionException_t5A81684757F5B89788A3697803DE89B4357FFE5B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7747 = { sizeof (PostconditionException_t38575CEE6CA0119BF41CAA638262B95E7DBCFFD1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7748 = { sizeof (InvariantException_tC747C04CFC7A61ED5A3E5D44B0BE3A5BECE11DC5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7749 = { sizeof (AssertionException_t253F52A8725E364A1AE7A79923C78A5BBD298242), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7750 = { sizeof (Console_tE6853A1DDC9A583838FA1AE18B5C7D7103B66FC7), -1, sizeof(Console_tE6853A1DDC9A583838FA1AE18B5C7D7103B66FC7_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7750[4] = 
{
	Console_tE6853A1DDC9A583838FA1AE18B5C7D7103B66FC7_StaticFields::get_offset_of__stringBuilder_0(),
	Console_tE6853A1DDC9A583838FA1AE18B5C7D7103B66FC7_StaticFields::get_offset_of_logger_1(),
	Console_tE6853A1DDC9A583838FA1AE18B5C7D7103B66FC7_StaticFields::get_offset_of_BatchLog_2(),
	Console_tE6853A1DDC9A583838FA1AE18B5C7D7103B66FC7_StaticFields::get_offset_of_onException_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7751 = { sizeof (U3CU3Ec_t95995C3290CB491D3FF50C75A935A381987078B4), -1, sizeof(U3CU3Ec_t95995C3290CB491D3FF50C75A935A381987078B4_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7751[1] = 
{
	U3CU3Ec_t95995C3290CB491D3FF50C75A935A381987078B4_StaticFields::get_offset_of_U3CU3E9_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7752 = { sizeof (LogType_tA9C618A228638545AB29CED55521DAFD43F209DA)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7752[5] = 
{
	LogType_tA9C618A228638545AB29CED55521DAFD43F209DA::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7753 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7754 = { sizeof (SimpleLogger_tCF6CBA7AF8F03D6AAC6225F8E1DD9E19EBF7B3F0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7755 = { sizeof (SlowLoggerUnity_tD909793CED39946C2553FB0A295F9296342401EB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7756 = { sizeof (Break_t16D4B71AB2F870282D0F365D3B777C915C2576C4), -1, sizeof(Break_t16D4B71AB2F870282D0F365D3B777C915C2576C4_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7756[2] = 
{
	Break_t16D4B71AB2F870282D0F365D3B777C915C2576C4_StaticFields::get_offset_of_It_0(),
	Break_t16D4B71AB2F870282D0F365D3B777C915C2576C4_StaticFields::get_offset_of_AndStop_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7757 = { sizeof (ParallelTaskCollection_t8DFC5A6DAFD6AFB617F77CF0767758AF060049DA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7757[6] = 
{
	ParallelTaskCollection_t8DFC5A6DAFD6AFB617F77CF0767758AF060049DA::get_offset_of_onComplete_4(),
	ParallelTaskCollection_t8DFC5A6DAFD6AFB617F77CF0767758AF060049DA::get_offset_of_onException_5(),
	ParallelTaskCollection_t8DFC5A6DAFD6AFB617F77CF0767758AF060049DA::get_offset_of__current_6(),
	ParallelTaskCollection_t8DFC5A6DAFD6AFB617F77CF0767758AF060049DA::get_offset_of__offset_7(),
	ParallelTaskCollection_t8DFC5A6DAFD6AFB617F77CF0767758AF060049DA::get_offset_of__currentWrapper_8(),
	ParallelTaskCollection_t8DFC5A6DAFD6AFB617F77CF0767758AF060049DA::get_offset_of__name_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7758 = { sizeof (ParallelTask_t1552F13FB2B55A71306310C98077D837B194A1B4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7758[1] = 
{
	ParallelTask_t1552F13FB2B55A71306310C98077D837B194A1B4::get_offset_of__parent_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7759 = { sizeof (SerialTaskCollection_t31E5E0CD4A5BAA251A396D5B51F12E1FB6B68BE7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7759[5] = 
{
	SerialTaskCollection_t31E5E0CD4A5BAA251A396D5B51F12E1FB6B68BE7::get_offset_of_onComplete_4(),
	SerialTaskCollection_t31E5E0CD4A5BAA251A396D5B51F12E1FB6B68BE7::get_offset_of_onException_5(),
	SerialTaskCollection_t31E5E0CD4A5BAA251A396D5B51F12E1FB6B68BE7::get_offset_of__index_6(),
	SerialTaskCollection_t31E5E0CD4A5BAA251A396D5B51F12E1FB6B68BE7::get_offset_of__current_7(),
	SerialTaskCollection_t31E5E0CD4A5BAA251A396D5B51F12E1FB6B68BE7::get_offset_of__name_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7760 = { sizeof (TaskCollection_tD5BE67D01B98473132820546A081DDB58E47C931), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7760[4] = 
{
	TaskCollection_tD5BE67D01B98473132820546A081DDB58E47C931::get_offset_of_U3CisRunningU3Ek__BackingField_0(),
	TaskCollection_tD5BE67D01B98473132820546A081DDB58E47C931::get_offset_of__listOfStacks_1(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7761 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7761[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7762 = { sizeof (LoopActionEnumerator_t54A9470950C20832A1CDA56CC03B15C6DC61BCA0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7762[1] = 
{
	LoopActionEnumerator_t54A9470950C20832A1CDA56CC03B15C6DC61BCA0::get_offset_of__action_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7763 = { sizeof (TimedLoopActionEnumerator_t38CCD290BCE439D8D805B8838A26D2FFA881E673), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7763[2] = 
{
	TimedLoopActionEnumerator_t38CCD290BCE439D8D805B8838A26D2FFA881E673::get_offset_of__action_0(),
	TimedLoopActionEnumerator_t38CCD290BCE439D8D805B8838A26D2FFA881E673::get_offset_of__then_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7764 = { sizeof (InterleavedLoopActionEnumerator_tAED110EE2D9E8DBE40BE1BD5D5E4D8439B4F9954), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7764[3] = 
{
	InterleavedLoopActionEnumerator_tAED110EE2D9E8DBE40BE1BD5D5E4D8439B4F9954::get_offset_of__action_0(),
	InterleavedLoopActionEnumerator_tAED110EE2D9E8DBE40BE1BD5D5E4D8439B4F9954::get_offset_of__then_1(),
	InterleavedLoopActionEnumerator_tAED110EE2D9E8DBE40BE1BD5D5E4D8439B4F9954::get_offset_of__interval_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7765 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7766 = { sizeof (MultiThreadedParallelTaskCollection_tAB10527D86B69A23BF4EAD4E4467524993005D12), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7766[11] = 
{
	MultiThreadedParallelTaskCollection_tAB10527D86B69A23BF4EAD4E4467524993005D12::get_offset_of_onComplete_0(),
	MultiThreadedParallelTaskCollection_tAB10527D86B69A23BF4EAD4E4467524993005D12::get_offset_of_U3CisRunningU3Ek__BackingField_1(),
	MultiThreadedParallelTaskCollection_tAB10527D86B69A23BF4EAD4E4467524993005D12::get_offset_of__runners_2(),
	MultiThreadedParallelTaskCollection_tAB10527D86B69A23BF4EAD4E4467524993005D12::get_offset_of__parallelTasks_3(),
	MultiThreadedParallelTaskCollection_tAB10527D86B69A23BF4EAD4E4467524993005D12::get_offset_of__taskRoutines_4(),
	MultiThreadedParallelTaskCollection_tAB10527D86B69A23BF4EAD4E4467524993005D12::get_offset_of__numberOfTasksAdded_5(),
	MultiThreadedParallelTaskCollection_tAB10527D86B69A23BF4EAD4E4467524993005D12::get_offset_of__numberOfConcurrentOperationsToRun_6(),
	MultiThreadedParallelTaskCollection_tAB10527D86B69A23BF4EAD4E4467524993005D12::get_offset_of__counter_7(),
	MultiThreadedParallelTaskCollection_tAB10527D86B69A23BF4EAD4E4467524993005D12::get_offset_of__disposingThreads_8(),
	MultiThreadedParallelTaskCollection_tAB10527D86B69A23BF4EAD4E4467524993005D12::get_offset_of__stoppingThreads_9(),
	MultiThreadedParallelTaskCollection_tAB10527D86B69A23BF4EAD4E4467524993005D12::get_offset_of__isDisposing_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7767 = { sizeof (MultiThreadedParallelTaskCollectionException_tFFE7D199C2FA3521FC9C3463E92FFB43C1CEAAFA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7768 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7769 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7770 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7771 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7772 = { sizeof (PausableTaskException_t2592A41B7346E95CB061E90F12CACEDEC35208B3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7773 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7774 = { sizeof (ContinuationWrapper_t135700DA709759BA12FEE8996B57AF89BFE20399), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7774[2] = 
{
	ContinuationWrapper_t135700DA709759BA12FEE8996B57AF89BFE20399::get_offset_of__completed_0(),
	ContinuationWrapper_t135700DA709759BA12FEE8996B57AF89BFE20399::get_offset_of__condition_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7775 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7776 = { sizeof (MultiThreadRunner_tC4A17C81CFC215E6A3719AADB82ACFB718255F92), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7776[13] = 
{
	MultiThreadRunner_tC4A17C81CFC215E6A3719AADB82ACFB718255F92::get_offset_of_U3CpausedU3Ek__BackingField_0(),
	MultiThreadRunner_tC4A17C81CFC215E6A3719AADB82ACFB718255F92::get_offset_of__coroutines_1(),
	MultiThreadRunner_tC4A17C81CFC215E6A3719AADB82ACFB718255F92::get_offset_of__newTaskRoutines_2(),
	MultiThreadRunner_tC4A17C81CFC215E6A3719AADB82ACFB718255F92::get_offset_of__name_3(),
	MultiThreadRunner_tC4A17C81CFC215E6A3719AADB82ACFB718255F92::get_offset_of__interlock_4(),
	MultiThreadRunner_tC4A17C81CFC215E6A3719AADB82ACFB718255F92::get_offset_of__isAlive_5(),
	MultiThreadRunner_tC4A17C81CFC215E6A3719AADB82ACFB718255F92::get_offset_of__waitForflush_6(),
	MultiThreadRunner_tC4A17C81CFC215E6A3719AADB82ACFB718255F92::get_offset_of__breakThread_7(),
	MultiThreadRunner_tC4A17C81CFC215E6A3719AADB82ACFB718255F92::get_offset_of__mevent_8(),
	MultiThreadRunner_tC4A17C81CFC215E6A3719AADB82ACFB718255F92::get_offset_of__lockingMechanism_9(),
	MultiThreadRunner_tC4A17C81CFC215E6A3719AADB82ACFB718255F92::get_offset_of__interval_10(),
	MultiThreadRunner_tC4A17C81CFC215E6A3719AADB82ACFB718255F92::get_offset_of__watch_11(),
	MultiThreadRunner_tC4A17C81CFC215E6A3719AADB82ACFB718255F92::get_offset_of__onThreadKilled_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7777 = { sizeof (U3CU3Ec__DisplayClass10_0_t95EFA810ABCD70BA5CB25CD61CD6A068E8F5F9DB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7777[2] = 
{
	U3CU3Ec__DisplayClass10_0_t95EFA810ABCD70BA5CB25CD61CD6A068E8F5F9DB::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass10_0_t95EFA810ABCD70BA5CB25CD61CD6A068E8F5F9DB::get_offset_of_name_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7778 = { sizeof (SyncRunner_t0F4700CF530FD98101CFCD566EBAAE1E8E4EE8D2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7778[2] = 
{
	SyncRunner_t0F4700CF530FD98101CFCD566EBAAE1E8E4EE8D2::get_offset_of_U3CpausedU3Ek__BackingField_0(),
	SyncRunner_t0F4700CF530FD98101CFCD566EBAAE1E8E4EE8D2::get_offset_of_U3CisStoppingU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7779 = { sizeof (CoroutineMonoRunner_tE7E5367413EE32878D5A3D8EC78406F687A475C5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7779[4] = 
{
	CoroutineMonoRunner_tE7E5367413EE32878D5A3D8EC78406F687A475C5::get_offset_of__newTaskRoutines_2(),
	CoroutineMonoRunner_tE7E5367413EE32878D5A3D8EC78406F687A475C5::get_offset_of__flushingOperation_3(),
	CoroutineMonoRunner_tE7E5367413EE32878D5A3D8EC78406F687A475C5::get_offset_of__info_4(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7780 = { sizeof (EndOfFrameRunner_t9F78CD19F94BE7C130860CA0A4A912A0F6224FBB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7780[4] = 
{
	EndOfFrameRunner_t9F78CD19F94BE7C130860CA0A4A912A0F6224FBB::get_offset_of__newTaskRoutines_2(),
	EndOfFrameRunner_t9F78CD19F94BE7C130860CA0A4A912A0F6224FBB::get_offset_of__flushingOperation_3(),
	EndOfFrameRunner_t9F78CD19F94BE7C130860CA0A4A912A0F6224FBB::get_offset_of__info_4(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7781 = { sizeof (LateMonoRunner_t78843F08125CAA775F86434EFC83B5828B6D69AA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7781[4] = 
{
	LateMonoRunner_t78843F08125CAA775F86434EFC83B5828B6D69AA::get_offset_of__newTaskRoutines_2(),
	LateMonoRunner_t78843F08125CAA775F86434EFC83B5828B6D69AA::get_offset_of__flushingOperation_3(),
	LateMonoRunner_t78843F08125CAA775F86434EFC83B5828B6D69AA::get_offset_of__info_4(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7782 = { sizeof (MonoRunner_t65496222247A6A3CAFE8294914A4776DE8693784), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7782[2] = 
{
	MonoRunner_t65496222247A6A3CAFE8294914A4776DE8693784::get_offset_of_U3CpausedU3Ek__BackingField_0(),
	MonoRunner_t65496222247A6A3CAFE8294914A4776DE8693784::get_offset_of__go_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7783 = { sizeof (PhysicMonoRunner_tF4F299E77C8B07EF8D39343FF568FAE80CF909F6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7783[4] = 
{
	PhysicMonoRunner_tF4F299E77C8B07EF8D39343FF568FAE80CF909F6::get_offset_of__newTaskRoutines_2(),
	PhysicMonoRunner_tF4F299E77C8B07EF8D39343FF568FAE80CF909F6::get_offset_of__flushingOperation_3(),
	PhysicMonoRunner_tF4F299E77C8B07EF8D39343FF568FAE80CF909F6::get_offset_of__info_4(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7784 = { sizeof (SequentialMonoRunner_t65E8C95EDAD86104CA20B30E48F79611A1C53B62), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7784[4] = 
{
	SequentialMonoRunner_t65E8C95EDAD86104CA20B30E48F79611A1C53B62::get_offset_of__newTaskRoutines_2(),
	SequentialMonoRunner_t65E8C95EDAD86104CA20B30E48F79611A1C53B62::get_offset_of__flushingOperation_3(),
	SequentialMonoRunner_t65E8C95EDAD86104CA20B30E48F79611A1C53B62::get_offset_of__info_4(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7785 = { sizeof (StaggeredMonoRunner_t0488A628E15A0D79BE8BA58D55E271618F4C53FA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7785[4] = 
{
	StaggeredMonoRunner_t0488A628E15A0D79BE8BA58D55E271618F4C53FA::get_offset_of__flushingOperation_2(),
	StaggeredMonoRunner_t0488A628E15A0D79BE8BA58D55E271618F4C53FA::get_offset_of__info_3(),
	StaggeredMonoRunner_t0488A628E15A0D79BE8BA58D55E271618F4C53FA::get_offset_of__newTaskRoutines_4(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7786 = { sizeof (FlushingOperationStaggered_tB9B268018A84589696B8AC6CA3B9B8E6778AF1F0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7786[1] = 
{
	FlushingOperationStaggered_tB9B268018A84589696B8AC6CA3B9B8E6778AF1F0::get_offset_of_maxTasksPerFrame_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7787 = { sizeof (UpdateMonoRunner_tD70489504A43A0A4ADA16547CB1203C1FDC1B54C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7787[4] = 
{
	UpdateMonoRunner_tD70489504A43A0A4ADA16547CB1203C1FDC1B54C::get_offset_of__newTaskRoutines_2(),
	UpdateMonoRunner_tD70489504A43A0A4ADA16547CB1203C1FDC1B54C::get_offset_of__flushingOperation_3(),
	UpdateMonoRunner_tD70489504A43A0A4ADA16547CB1203C1FDC1B54C::get_offset_of__info_4(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7788 = { sizeof (StandardSchedulers_tE4C2F7648F973E3B6B62A5695FE96A851F546E8B), -1, sizeof(StandardSchedulers_tE4C2F7648F973E3B6B62A5695FE96A851F546E8B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7788[5] = 
{
	StandardSchedulers_tE4C2F7648F973E3B6B62A5695FE96A851F546E8B_StaticFields::get_offset_of__multiThreadScheduler_0(),
	StandardSchedulers_tE4C2F7648F973E3B6B62A5695FE96A851F546E8B_StaticFields::get_offset_of__coroutineScheduler_1(),
	StandardSchedulers_tE4C2F7648F973E3B6B62A5695FE96A851F546E8B_StaticFields::get_offset_of__physicScheduler_2(),
	StandardSchedulers_tE4C2F7648F973E3B6B62A5695FE96A851F546E8B_StaticFields::get_offset_of__lateScheduler_3(),
	StandardSchedulers_tE4C2F7648F973E3B6B62A5695FE96A851F546E8B_StaticFields::get_offset_of__updateScheduler_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7789 = { sizeof (TaskRunner_tD224F90700B75918458C78D09272B5CA50A80747), -1, sizeof(TaskRunner_tD224F90700B75918458C78D09272B5CA50A80747_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7789[3] = 
{
	TaskRunner_tD224F90700B75918458C78D09272B5CA50A80747_StaticFields::get_offset_of__instance_0(),
	TaskRunner_tD224F90700B75918458C78D09272B5CA50A80747::get_offset_of__runner_1(),
	TaskRunner_tD224F90700B75918458C78D09272B5CA50A80747::get_offset_of__taskPool_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7790 = { sizeof (TaskWrapper_t5FAAE4C120A656C29A4379FC352788AA30010467), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7790[2] = 
{
	TaskWrapper_t5FAAE4C120A656C29A4379FC352788AA30010467::get_offset_of_U3CtaskU3Ek__BackingField_0(),
	TaskWrapper_t5FAAE4C120A656C29A4379FC352788AA30010467::get_offset_of__started_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7791 = { sizeof (TaskYieldsIEnumerableException_t7355F3FDE518ABBB2BB70429484D0D3B92134964), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7792 = { sizeof (TaskInfo_tE81761C55B71E6B50D0D42AF7CA89B3E5C22B8E1)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7792[9] = 
{
	0,
	0,
	TaskInfo_tE81761C55B71E6B50D0D42AF7CA89B3E5C22B8E1::get_offset_of__accumulatedUpdateDuration_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TaskInfo_tE81761C55B71E6B50D0D42AF7CA89B3E5C22B8E1::get_offset_of__lastUpdateDuration_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TaskInfo_tE81761C55B71E6B50D0D42AF7CA89B3E5C22B8E1::get_offset_of__maxUpdateDuration_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TaskInfo_tE81761C55B71E6B50D0D42AF7CA89B3E5C22B8E1::get_offset_of__minUpdateDuration_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TaskInfo_tE81761C55B71E6B50D0D42AF7CA89B3E5C22B8E1::get_offset_of__taskName_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TaskInfo_tE81761C55B71E6B50D0D42AF7CA89B3E5C22B8E1::get_offset_of__threadInfo_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TaskInfo_tE81761C55B71E6B50D0D42AF7CA89B3E5C22B8E1::get_offset_of__updateFrameTimes_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7793 = { sizeof (TaskProfiler_t2C3463D07E47F9E0399CDBBC5FBD606FEE890759), -1, sizeof(TaskProfiler_t2C3463D07E47F9E0399CDBBC5FBD606FEE890759_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7793[3] = 
{
	TaskProfiler_t2C3463D07E47F9E0399CDBBC5FBD606FEE890759::get_offset_of__stopwatch_0(),
	TaskProfiler_t2C3463D07E47F9E0399CDBBC5FBD606FEE890759_StaticFields::get_offset_of_LOCK_OBJECT_1(),
	TaskProfiler_t2C3463D07E47F9E0399CDBBC5FBD606FEE890759_StaticFields::get_offset_of_taskInfos_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7794 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7794[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7795 = { sizeof (PausableTask_t9370376F9736D7C79A37C1C5F687174754FB618A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7795[23] = 
{
	0,
	PausableTask_t9370376F9736D7C79A37C1C5F687174754FB618A::get_offset_of_U3ConExplicitlyStoppedU3Ek__BackingField_1(),
	PausableTask_t9370376F9736D7C79A37C1C5F687174754FB618A::get_offset_of__runner_2(),
	PausableTask_t9370376F9736D7C79A37C1C5F687174754FB618A::get_offset_of__coroutine_3(),
	PausableTask_t9370376F9736D7C79A37C1C5F687174754FB618A::get_offset_of__coroutineWrapper_4(),
	PausableTask_t9370376F9736D7C79A37C1C5F687174754FB618A::get_offset_of__continuationWrapper_5(),
	PausableTask_t9370376F9736D7C79A37C1C5F687174754FB618A::get_offset_of__pendingContinuationWrapper_6(),
	PausableTask_t9370376F9736D7C79A37C1C5F687174754FB618A::get_offset_of__threadSafe_7(),
	PausableTask_t9370376F9736D7C79A37C1C5F687174754FB618A::get_offset_of__compilerGenerated_8(),
	PausableTask_t9370376F9736D7C79A37C1C5F687174754FB618A::get_offset_of__taskEnumeratorJustSet_9(),
	PausableTask_t9370376F9736D7C79A37C1C5F687174754FB618A::get_offset_of__pendingEnumerator_10(),
	PausableTask_t9370376F9736D7C79A37C1C5F687174754FB618A::get_offset_of__taskEnumerator_11(),
	PausableTask_t9370376F9736D7C79A37C1C5F687174754FB618A::get_offset_of__pool_12(),
	PausableTask_t9370376F9736D7C79A37C1C5F687174754FB618A::get_offset_of__taskGenerator_13(),
	PausableTask_t9370376F9736D7C79A37C1C5F687174754FB618A::get_offset_of__onFail_14(),
	PausableTask_t9370376F9736D7C79A37C1C5F687174754FB618A::get_offset_of__onStop_15(),
	PausableTask_t9370376F9736D7C79A37C1C5F687174754FB618A::get_offset_of__name_16(),
	PausableTask_t9370376F9736D7C79A37C1C5F687174754FB618A::get_offset_of__started_17(),
	PausableTask_t9370376F9736D7C79A37C1C5F687174754FB618A::get_offset_of__completed_18(),
	PausableTask_t9370376F9736D7C79A37C1C5F687174754FB618A::get_offset_of__explicitlyStopped_19(),
	PausableTask_t9370376F9736D7C79A37C1C5F687174754FB618A::get_offset_of__paused_20(),
	PausableTask_t9370376F9736D7C79A37C1C5F687174754FB618A::get_offset_of__pendingRestart_21(),
	PausableTask_t9370376F9736D7C79A37C1C5F687174754FB618A::get_offset_of__callStartFirstError_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7796 = { sizeof (PausableTaskPool_t650A2D678FDFAA152B5CB537987A31A82A014D28), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7796[1] = 
{
	PausableTaskPool_t650A2D678FDFAA152B5CB537987A31A82A014D28::get_offset_of__pool_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7797 = { sizeof (RunnerBehaviour_t9AE6F5864AD5F25FE25A18838BC70B25D4B70DFD), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7798 = { sizeof (RunnerBehaviourEndOfFrame_t62CC360021E4159E5AFD077D2A603BD4450B1555), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7798[2] = 
{
	RunnerBehaviourEndOfFrame_t62CC360021E4159E5AFD077D2A603BD4450B1555::get_offset_of__mainRoutine_4(),
	RunnerBehaviourEndOfFrame_t62CC360021E4159E5AFD077D2A603BD4450B1555::get_offset_of__waitForEndOfFrame_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7799 = { sizeof (U3CWaitForEndOfFrameLoopU3Ed__2_tD82F296AFE6584098A6FA2F78BBB9F16730287FA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7799[3] = 
{
	U3CWaitForEndOfFrameLoopU3Ed__2_tD82F296AFE6584098A6FA2F78BBB9F16730287FA::get_offset_of_U3CU3E1__state_0(),
	U3CWaitForEndOfFrameLoopU3Ed__2_tD82F296AFE6584098A6FA2F78BBB9F16730287FA::get_offset_of_U3CU3E2__current_1(),
	U3CWaitForEndOfFrameLoopU3Ed__2_tD82F296AFE6584098A6FA2F78BBB9F16730287FA::get_offset_of_U3CU3E4__this_2(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
