﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Org.BouncyCastle.Asn1.Asn1Encodable
struct Asn1Encodable_t390FCE82F5F52E41A7320BBF2E8BF18DA1622A8B;
// Org.BouncyCastle.Asn1.Asn1Object
struct Asn1Object_tB780E50883209D5C975E83B3D16257DFC3CCE36D;
// Org.BouncyCastle.Asn1.Asn1StreamParser
struct Asn1StreamParser_t2FE3FB89834CCEE990CDD0F38B5DD0960CDC9C90;
// Org.BouncyCastle.Asn1.CryptoPro.Gost3410ParamSetParameters
struct Gost3410ParamSetParameters_tA09B7D79E4C03898F7D5F3AB8C9A8A061B69E6C2;
// Org.BouncyCastle.Asn1.DefiniteLengthInputStream
struct DefiniteLengthInputStream_t9461CF6517E1119EFD75E65D001C0969476E74D1;
// Org.BouncyCastle.Asn1.DerEnumerated[]
struct DerEnumeratedU5BU5D_tEBACB932DA336D53CCA9EEA8D863431522CD024D;
// Org.BouncyCastle.Asn1.DerInteger
struct DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A;
// Org.BouncyCastle.Asn1.DerObjectIdentifier
struct DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441;
// Org.BouncyCastle.Asn1.DerObjectIdentifier[]
struct DerObjectIdentifierU5BU5D_t95AEC7AE62F0F0FD9FF53036DF9BEDDF497B02B9;
// Org.BouncyCastle.Asn1.X9.X9ECParameters
struct X9ECParameters_t818A7DD4D13D91F0D36622C6C117D7E9A10E51C3;
// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder
struct X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Byte[][]
struct ByteU5BU5DU5BU5D_tD1CB918775FFB351821F10AC338FECDDE22DEEC7;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.Collections.IEnumerable
struct IEnumerable_tD74549CEA1AA48E768382B94FEACBB07E2E3FA2C;
// System.Collections.IList
struct IList_tA637AB426E16F84F84ACC2813BDCF3A0414AF0AA;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.IO.Stream
struct Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7;
// System.IO.Stream/ReadWriteTask
struct ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.String
struct String_t;
// System.Threading.SemaphoreSlim
struct SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.Video.VideoPlayer
struct VideoPlayer_tFC1C27AF83D59A5213B2AC561B43FD7E19FE02F2;
// UnityEngine.Video.VideoPlayer/ErrorEventHandler
struct ErrorEventHandler_tF5863946928B48BE13146ED5FF70AC92678FE222;
// UnityEngine.Video.VideoPlayer/EventHandler
struct EventHandler_t5069D72E1ED46BD04F19D8D4534811B95A8E2308;
// UnityEngine.Video.VideoPlayer/FrameReadyEventHandler
struct FrameReadyEventHandler_t518B277D916AB292680CAA186BCDB3D3EF130422;
// UnityEngine.Video.VideoPlayer/TimeEventHandler
struct TimeEventHandler_tDD815DAABFADDD98C8993B2A97A2FCE858266BC1;

struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;



#ifndef U3CMODULEU3E_T25A4C93C391400B6C1AF0A8BEAB317DE8B1AD521_H
#define U3CMODULEU3E_T25A4C93C391400B6C1AF0A8BEAB317DE8B1AD521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t25A4C93C391400B6C1AF0A8BEAB317DE8B1AD521 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T25A4C93C391400B6C1AF0A8BEAB317DE8B1AD521_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef ANSSINAMEDCURVES_T96455013467BD6208CE1B457D05FFA940B09555B_H
#define ANSSINAMEDCURVES_T96455013467BD6208CE1B457D05FFA940B09555B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.Anssi.AnssiNamedCurves
struct  AnssiNamedCurves_t96455013467BD6208CE1B457D05FFA940B09555B  : public RuntimeObject
{
public:

public:
};

struct AnssiNamedCurves_t96455013467BD6208CE1B457D05FFA940B09555B_StaticFields
{
public:
	// System.Collections.IDictionary Org.BouncyCastle.Asn1.Anssi.AnssiNamedCurves::objIds
	RuntimeObject* ___objIds_0;
	// System.Collections.IDictionary Org.BouncyCastle.Asn1.Anssi.AnssiNamedCurves::curves
	RuntimeObject* ___curves_1;
	// System.Collections.IDictionary Org.BouncyCastle.Asn1.Anssi.AnssiNamedCurves::names
	RuntimeObject* ___names_2;

public:
	inline static int32_t get_offset_of_objIds_0() { return static_cast<int32_t>(offsetof(AnssiNamedCurves_t96455013467BD6208CE1B457D05FFA940B09555B_StaticFields, ___objIds_0)); }
	inline RuntimeObject* get_objIds_0() const { return ___objIds_0; }
	inline RuntimeObject** get_address_of_objIds_0() { return &___objIds_0; }
	inline void set_objIds_0(RuntimeObject* value)
	{
		___objIds_0 = value;
		Il2CppCodeGenWriteBarrier((&___objIds_0), value);
	}

	inline static int32_t get_offset_of_curves_1() { return static_cast<int32_t>(offsetof(AnssiNamedCurves_t96455013467BD6208CE1B457D05FFA940B09555B_StaticFields, ___curves_1)); }
	inline RuntimeObject* get_curves_1() const { return ___curves_1; }
	inline RuntimeObject** get_address_of_curves_1() { return &___curves_1; }
	inline void set_curves_1(RuntimeObject* value)
	{
		___curves_1 = value;
		Il2CppCodeGenWriteBarrier((&___curves_1), value);
	}

	inline static int32_t get_offset_of_names_2() { return static_cast<int32_t>(offsetof(AnssiNamedCurves_t96455013467BD6208CE1B457D05FFA940B09555B_StaticFields, ___names_2)); }
	inline RuntimeObject* get_names_2() const { return ___names_2; }
	inline RuntimeObject** get_address_of_names_2() { return &___names_2; }
	inline void set_names_2(RuntimeObject* value)
	{
		___names_2 = value;
		Il2CppCodeGenWriteBarrier((&___names_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANSSINAMEDCURVES_T96455013467BD6208CE1B457D05FFA940B09555B_H
#ifndef ANSSIOBJECTIDENTIFIERS_TCBD0B6EE2A518136CC188003673CA647860AA470_H
#define ANSSIOBJECTIDENTIFIERS_TCBD0B6EE2A518136CC188003673CA647860AA470_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.Anssi.AnssiObjectIdentifiers
struct  AnssiObjectIdentifiers_tCBD0B6EE2A518136CC188003673CA647860AA470  : public RuntimeObject
{
public:

public:
};

struct AnssiObjectIdentifiers_tCBD0B6EE2A518136CC188003673CA647860AA470_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Anssi.AnssiObjectIdentifiers::FRP256v1
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___FRP256v1_0;

public:
	inline static int32_t get_offset_of_FRP256v1_0() { return static_cast<int32_t>(offsetof(AnssiObjectIdentifiers_tCBD0B6EE2A518136CC188003673CA647860AA470_StaticFields, ___FRP256v1_0)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_FRP256v1_0() const { return ___FRP256v1_0; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_FRP256v1_0() { return &___FRP256v1_0; }
	inline void set_FRP256v1_0(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___FRP256v1_0 = value;
		Il2CppCodeGenWriteBarrier((&___FRP256v1_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANSSIOBJECTIDENTIFIERS_TCBD0B6EE2A518136CC188003673CA647860AA470_H
#ifndef ASN1ENCODABLE_T390FCE82F5F52E41A7320BBF2E8BF18DA1622A8B_H
#define ASN1ENCODABLE_T390FCE82F5F52E41A7320BBF2E8BF18DA1622A8B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.Asn1Encodable
struct  Asn1Encodable_t390FCE82F5F52E41A7320BBF2E8BF18DA1622A8B  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASN1ENCODABLE_T390FCE82F5F52E41A7320BBF2E8BF18DA1622A8B_H
#ifndef ASN1ENCODABLEVECTOR_TABDFD1A771D989CF6BB9F956783729884A2BB34F_H
#define ASN1ENCODABLEVECTOR_TABDFD1A771D989CF6BB9F956783729884A2BB34F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.Asn1EncodableVector
struct  Asn1EncodableVector_tABDFD1A771D989CF6BB9F956783729884A2BB34F  : public RuntimeObject
{
public:
	// System.Collections.IList Org.BouncyCastle.Asn1.Asn1EncodableVector::v
	RuntimeObject* ___v_0;

public:
	inline static int32_t get_offset_of_v_0() { return static_cast<int32_t>(offsetof(Asn1EncodableVector_tABDFD1A771D989CF6BB9F956783729884A2BB34F, ___v_0)); }
	inline RuntimeObject* get_v_0() const { return ___v_0; }
	inline RuntimeObject** get_address_of_v_0() { return &___v_0; }
	inline void set_v_0(RuntimeObject* value)
	{
		___v_0 = value;
		Il2CppCodeGenWriteBarrier((&___v_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASN1ENCODABLEVECTOR_TABDFD1A771D989CF6BB9F956783729884A2BB34F_H
#ifndef DERCOMPARER_TA00AC1811F1B46623B9BF6E2813C6914B8808A4C_H
#define DERCOMPARER_TA00AC1811F1B46623B9BF6E2813C6914B8808A4C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.Asn1Set_DerComparer
struct  DerComparer_tA00AC1811F1B46623B9BF6E2813C6914B8808A4C  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERCOMPARER_TA00AC1811F1B46623B9BF6E2813C6914B8808A4C_H
#ifndef ASN1STREAMPARSER_T2FE3FB89834CCEE990CDD0F38B5DD0960CDC9C90_H
#define ASN1STREAMPARSER_T2FE3FB89834CCEE990CDD0F38B5DD0960CDC9C90_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.Asn1StreamParser
struct  Asn1StreamParser_t2FE3FB89834CCEE990CDD0F38B5DD0960CDC9C90  : public RuntimeObject
{
public:
	// System.IO.Stream Org.BouncyCastle.Asn1.Asn1StreamParser::_in
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ____in_0;
	// System.Int32 Org.BouncyCastle.Asn1.Asn1StreamParser::_limit
	int32_t ____limit_1;
	// System.Byte[][] Org.BouncyCastle.Asn1.Asn1StreamParser::tmpBuffers
	ByteU5BU5DU5BU5D_tD1CB918775FFB351821F10AC338FECDDE22DEEC7* ___tmpBuffers_2;

public:
	inline static int32_t get_offset_of__in_0() { return static_cast<int32_t>(offsetof(Asn1StreamParser_t2FE3FB89834CCEE990CDD0F38B5DD0960CDC9C90, ____in_0)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get__in_0() const { return ____in_0; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of__in_0() { return &____in_0; }
	inline void set__in_0(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		____in_0 = value;
		Il2CppCodeGenWriteBarrier((&____in_0), value);
	}

	inline static int32_t get_offset_of__limit_1() { return static_cast<int32_t>(offsetof(Asn1StreamParser_t2FE3FB89834CCEE990CDD0F38B5DD0960CDC9C90, ____limit_1)); }
	inline int32_t get__limit_1() const { return ____limit_1; }
	inline int32_t* get_address_of__limit_1() { return &____limit_1; }
	inline void set__limit_1(int32_t value)
	{
		____limit_1 = value;
	}

	inline static int32_t get_offset_of_tmpBuffers_2() { return static_cast<int32_t>(offsetof(Asn1StreamParser_t2FE3FB89834CCEE990CDD0F38B5DD0960CDC9C90, ___tmpBuffers_2)); }
	inline ByteU5BU5DU5BU5D_tD1CB918775FFB351821F10AC338FECDDE22DEEC7* get_tmpBuffers_2() const { return ___tmpBuffers_2; }
	inline ByteU5BU5DU5BU5D_tD1CB918775FFB351821F10AC338FECDDE22DEEC7** get_address_of_tmpBuffers_2() { return &___tmpBuffers_2; }
	inline void set_tmpBuffers_2(ByteU5BU5DU5BU5D_tD1CB918775FFB351821F10AC338FECDDE22DEEC7* value)
	{
		___tmpBuffers_2 = value;
		Il2CppCodeGenWriteBarrier((&___tmpBuffers_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASN1STREAMPARSER_T2FE3FB89834CCEE990CDD0F38B5DD0960CDC9C90_H
#ifndef BERAPPLICATIONSPECIFICPARSER_TB09D0EF6489FA446DF94E1CA6E40DB0FAAD90997_H
#define BERAPPLICATIONSPECIFICPARSER_TB09D0EF6489FA446DF94E1CA6E40DB0FAAD90997_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.BerApplicationSpecificParser
struct  BerApplicationSpecificParser_tB09D0EF6489FA446DF94E1CA6E40DB0FAAD90997  : public RuntimeObject
{
public:
	// System.Int32 Org.BouncyCastle.Asn1.BerApplicationSpecificParser::tag
	int32_t ___tag_0;
	// Org.BouncyCastle.Asn1.Asn1StreamParser Org.BouncyCastle.Asn1.BerApplicationSpecificParser::parser
	Asn1StreamParser_t2FE3FB89834CCEE990CDD0F38B5DD0960CDC9C90 * ___parser_1;

public:
	inline static int32_t get_offset_of_tag_0() { return static_cast<int32_t>(offsetof(BerApplicationSpecificParser_tB09D0EF6489FA446DF94E1CA6E40DB0FAAD90997, ___tag_0)); }
	inline int32_t get_tag_0() const { return ___tag_0; }
	inline int32_t* get_address_of_tag_0() { return &___tag_0; }
	inline void set_tag_0(int32_t value)
	{
		___tag_0 = value;
	}

	inline static int32_t get_offset_of_parser_1() { return static_cast<int32_t>(offsetof(BerApplicationSpecificParser_tB09D0EF6489FA446DF94E1CA6E40DB0FAAD90997, ___parser_1)); }
	inline Asn1StreamParser_t2FE3FB89834CCEE990CDD0F38B5DD0960CDC9C90 * get_parser_1() const { return ___parser_1; }
	inline Asn1StreamParser_t2FE3FB89834CCEE990CDD0F38B5DD0960CDC9C90 ** get_address_of_parser_1() { return &___parser_1; }
	inline void set_parser_1(Asn1StreamParser_t2FE3FB89834CCEE990CDD0F38B5DD0960CDC9C90 * value)
	{
		___parser_1 = value;
		Il2CppCodeGenWriteBarrier((&___parser_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BERAPPLICATIONSPECIFICPARSER_TB09D0EF6489FA446DF94E1CA6E40DB0FAAD90997_H
#ifndef BEROCTETSTRINGPARSER_T70D00245C14BA6E5384FB87FF87ACAEDF900ABFB_H
#define BEROCTETSTRINGPARSER_T70D00245C14BA6E5384FB87FF87ACAEDF900ABFB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.BerOctetStringParser
struct  BerOctetStringParser_t70D00245C14BA6E5384FB87FF87ACAEDF900ABFB  : public RuntimeObject
{
public:
	// Org.BouncyCastle.Asn1.Asn1StreamParser Org.BouncyCastle.Asn1.BerOctetStringParser::_parser
	Asn1StreamParser_t2FE3FB89834CCEE990CDD0F38B5DD0960CDC9C90 * ____parser_0;

public:
	inline static int32_t get_offset_of__parser_0() { return static_cast<int32_t>(offsetof(BerOctetStringParser_t70D00245C14BA6E5384FB87FF87ACAEDF900ABFB, ____parser_0)); }
	inline Asn1StreamParser_t2FE3FB89834CCEE990CDD0F38B5DD0960CDC9C90 * get__parser_0() const { return ____parser_0; }
	inline Asn1StreamParser_t2FE3FB89834CCEE990CDD0F38B5DD0960CDC9C90 ** get_address_of__parser_0() { return &____parser_0; }
	inline void set__parser_0(Asn1StreamParser_t2FE3FB89834CCEE990CDD0F38B5DD0960CDC9C90 * value)
	{
		____parser_0 = value;
		Il2CppCodeGenWriteBarrier((&____parser_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEROCTETSTRINGPARSER_T70D00245C14BA6E5384FB87FF87ACAEDF900ABFB_H
#ifndef BERSEQUENCEPARSER_T495DBD16AAB2689118528D757B8486C68765081B_H
#define BERSEQUENCEPARSER_T495DBD16AAB2689118528D757B8486C68765081B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.BerSequenceParser
struct  BerSequenceParser_t495DBD16AAB2689118528D757B8486C68765081B  : public RuntimeObject
{
public:
	// Org.BouncyCastle.Asn1.Asn1StreamParser Org.BouncyCastle.Asn1.BerSequenceParser::_parser
	Asn1StreamParser_t2FE3FB89834CCEE990CDD0F38B5DD0960CDC9C90 * ____parser_0;

public:
	inline static int32_t get_offset_of__parser_0() { return static_cast<int32_t>(offsetof(BerSequenceParser_t495DBD16AAB2689118528D757B8486C68765081B, ____parser_0)); }
	inline Asn1StreamParser_t2FE3FB89834CCEE990CDD0F38B5DD0960CDC9C90 * get__parser_0() const { return ____parser_0; }
	inline Asn1StreamParser_t2FE3FB89834CCEE990CDD0F38B5DD0960CDC9C90 ** get_address_of__parser_0() { return &____parser_0; }
	inline void set__parser_0(Asn1StreamParser_t2FE3FB89834CCEE990CDD0F38B5DD0960CDC9C90 * value)
	{
		____parser_0 = value;
		Il2CppCodeGenWriteBarrier((&____parser_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BERSEQUENCEPARSER_T495DBD16AAB2689118528D757B8486C68765081B_H
#ifndef BERSETPARSER_TA6DDD6EB5C5DE7D9185780BBBA0DD441D79D5632_H
#define BERSETPARSER_TA6DDD6EB5C5DE7D9185780BBBA0DD441D79D5632_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.BerSetParser
struct  BerSetParser_tA6DDD6EB5C5DE7D9185780BBBA0DD441D79D5632  : public RuntimeObject
{
public:
	// Org.BouncyCastle.Asn1.Asn1StreamParser Org.BouncyCastle.Asn1.BerSetParser::_parser
	Asn1StreamParser_t2FE3FB89834CCEE990CDD0F38B5DD0960CDC9C90 * ____parser_0;

public:
	inline static int32_t get_offset_of__parser_0() { return static_cast<int32_t>(offsetof(BerSetParser_tA6DDD6EB5C5DE7D9185780BBBA0DD441D79D5632, ____parser_0)); }
	inline Asn1StreamParser_t2FE3FB89834CCEE990CDD0F38B5DD0960CDC9C90 * get__parser_0() const { return ____parser_0; }
	inline Asn1StreamParser_t2FE3FB89834CCEE990CDD0F38B5DD0960CDC9C90 ** get_address_of__parser_0() { return &____parser_0; }
	inline void set__parser_0(Asn1StreamParser_t2FE3FB89834CCEE990CDD0F38B5DD0960CDC9C90 * value)
	{
		____parser_0 = value;
		Il2CppCodeGenWriteBarrier((&____parser_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BERSETPARSER_TA6DDD6EB5C5DE7D9185780BBBA0DD441D79D5632_H
#ifndef BERTAGGEDOBJECTPARSER_T4146B8C0B583D98889266329D1700101EA287607_H
#define BERTAGGEDOBJECTPARSER_T4146B8C0B583D98889266329D1700101EA287607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.BerTaggedObjectParser
struct  BerTaggedObjectParser_t4146B8C0B583D98889266329D1700101EA287607  : public RuntimeObject
{
public:
	// System.Boolean Org.BouncyCastle.Asn1.BerTaggedObjectParser::_constructed
	bool ____constructed_0;
	// System.Int32 Org.BouncyCastle.Asn1.BerTaggedObjectParser::_tagNumber
	int32_t ____tagNumber_1;
	// Org.BouncyCastle.Asn1.Asn1StreamParser Org.BouncyCastle.Asn1.BerTaggedObjectParser::_parser
	Asn1StreamParser_t2FE3FB89834CCEE990CDD0F38B5DD0960CDC9C90 * ____parser_2;

public:
	inline static int32_t get_offset_of__constructed_0() { return static_cast<int32_t>(offsetof(BerTaggedObjectParser_t4146B8C0B583D98889266329D1700101EA287607, ____constructed_0)); }
	inline bool get__constructed_0() const { return ____constructed_0; }
	inline bool* get_address_of__constructed_0() { return &____constructed_0; }
	inline void set__constructed_0(bool value)
	{
		____constructed_0 = value;
	}

	inline static int32_t get_offset_of__tagNumber_1() { return static_cast<int32_t>(offsetof(BerTaggedObjectParser_t4146B8C0B583D98889266329D1700101EA287607, ____tagNumber_1)); }
	inline int32_t get__tagNumber_1() const { return ____tagNumber_1; }
	inline int32_t* get_address_of__tagNumber_1() { return &____tagNumber_1; }
	inline void set__tagNumber_1(int32_t value)
	{
		____tagNumber_1 = value;
	}

	inline static int32_t get_offset_of__parser_2() { return static_cast<int32_t>(offsetof(BerTaggedObjectParser_t4146B8C0B583D98889266329D1700101EA287607, ____parser_2)); }
	inline Asn1StreamParser_t2FE3FB89834CCEE990CDD0F38B5DD0960CDC9C90 * get__parser_2() const { return ____parser_2; }
	inline Asn1StreamParser_t2FE3FB89834CCEE990CDD0F38B5DD0960CDC9C90 ** get_address_of__parser_2() { return &____parser_2; }
	inline void set__parser_2(Asn1StreamParser_t2FE3FB89834CCEE990CDD0F38B5DD0960CDC9C90 * value)
	{
		____parser_2 = value;
		Il2CppCodeGenWriteBarrier((&____parser_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BERTAGGEDOBJECTPARSER_T4146B8C0B583D98889266329D1700101EA287607_H
#ifndef CRYPTOPROOBJECTIDENTIFIERS_T6FBDFC987F7A8828381DCDC802E2CB1EC3A258B1_H
#define CRYPTOPROOBJECTIDENTIFIERS_T6FBDFC987F7A8828381DCDC802E2CB1EC3A258B1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.CryptoPro.CryptoProObjectIdentifiers
struct  CryptoProObjectIdentifiers_t6FBDFC987F7A8828381DCDC802E2CB1EC3A258B1  : public RuntimeObject
{
public:

public:
};

struct CryptoProObjectIdentifiers_t6FBDFC987F7A8828381DCDC802E2CB1EC3A258B1_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.CryptoPro.CryptoProObjectIdentifiers::GostR3411
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___GostR3411_0;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.CryptoPro.CryptoProObjectIdentifiers::GostR3411Hmac
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___GostR3411Hmac_1;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.CryptoPro.CryptoProObjectIdentifiers::GostR28147Cbc
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___GostR28147Cbc_2;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.CryptoPro.CryptoProObjectIdentifiers::ID_Gost28147_89_CryptoPro_A_ParamSet
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___ID_Gost28147_89_CryptoPro_A_ParamSet_3;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.CryptoPro.CryptoProObjectIdentifiers::GostR3410x94
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___GostR3410x94_4;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.CryptoPro.CryptoProObjectIdentifiers::GostR3410x2001
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___GostR3410x2001_5;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.CryptoPro.CryptoProObjectIdentifiers::GostR3411x94WithGostR3410x94
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___GostR3411x94WithGostR3410x94_6;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.CryptoPro.CryptoProObjectIdentifiers::GostR3411x94WithGostR3410x2001
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___GostR3411x94WithGostR3410x2001_7;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.CryptoPro.CryptoProObjectIdentifiers::GostR3411x94CryptoProParamSet
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___GostR3411x94CryptoProParamSet_8;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.CryptoPro.CryptoProObjectIdentifiers::GostR3410x94CryptoProA
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___GostR3410x94CryptoProA_9;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.CryptoPro.CryptoProObjectIdentifiers::GostR3410x94CryptoProB
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___GostR3410x94CryptoProB_10;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.CryptoPro.CryptoProObjectIdentifiers::GostR3410x94CryptoProC
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___GostR3410x94CryptoProC_11;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.CryptoPro.CryptoProObjectIdentifiers::GostR3410x94CryptoProD
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___GostR3410x94CryptoProD_12;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.CryptoPro.CryptoProObjectIdentifiers::GostR3410x94CryptoProXchA
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___GostR3410x94CryptoProXchA_13;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.CryptoPro.CryptoProObjectIdentifiers::GostR3410x94CryptoProXchB
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___GostR3410x94CryptoProXchB_14;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.CryptoPro.CryptoProObjectIdentifiers::GostR3410x94CryptoProXchC
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___GostR3410x94CryptoProXchC_15;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.CryptoPro.CryptoProObjectIdentifiers::GostR3410x2001CryptoProA
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___GostR3410x2001CryptoProA_16;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.CryptoPro.CryptoProObjectIdentifiers::GostR3410x2001CryptoProB
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___GostR3410x2001CryptoProB_17;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.CryptoPro.CryptoProObjectIdentifiers::GostR3410x2001CryptoProC
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___GostR3410x2001CryptoProC_18;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.CryptoPro.CryptoProObjectIdentifiers::GostR3410x2001CryptoProXchA
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___GostR3410x2001CryptoProXchA_19;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.CryptoPro.CryptoProObjectIdentifiers::GostR3410x2001CryptoProXchB
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___GostR3410x2001CryptoProXchB_20;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.CryptoPro.CryptoProObjectIdentifiers::GostElSgDH3410Default
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___GostElSgDH3410Default_21;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.CryptoPro.CryptoProObjectIdentifiers::GostElSgDH3410x1
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___GostElSgDH3410x1_22;

public:
	inline static int32_t get_offset_of_GostR3411_0() { return static_cast<int32_t>(offsetof(CryptoProObjectIdentifiers_t6FBDFC987F7A8828381DCDC802E2CB1EC3A258B1_StaticFields, ___GostR3411_0)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_GostR3411_0() const { return ___GostR3411_0; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_GostR3411_0() { return &___GostR3411_0; }
	inline void set_GostR3411_0(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___GostR3411_0 = value;
		Il2CppCodeGenWriteBarrier((&___GostR3411_0), value);
	}

	inline static int32_t get_offset_of_GostR3411Hmac_1() { return static_cast<int32_t>(offsetof(CryptoProObjectIdentifiers_t6FBDFC987F7A8828381DCDC802E2CB1EC3A258B1_StaticFields, ___GostR3411Hmac_1)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_GostR3411Hmac_1() const { return ___GostR3411Hmac_1; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_GostR3411Hmac_1() { return &___GostR3411Hmac_1; }
	inline void set_GostR3411Hmac_1(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___GostR3411Hmac_1 = value;
		Il2CppCodeGenWriteBarrier((&___GostR3411Hmac_1), value);
	}

	inline static int32_t get_offset_of_GostR28147Cbc_2() { return static_cast<int32_t>(offsetof(CryptoProObjectIdentifiers_t6FBDFC987F7A8828381DCDC802E2CB1EC3A258B1_StaticFields, ___GostR28147Cbc_2)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_GostR28147Cbc_2() const { return ___GostR28147Cbc_2; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_GostR28147Cbc_2() { return &___GostR28147Cbc_2; }
	inline void set_GostR28147Cbc_2(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___GostR28147Cbc_2 = value;
		Il2CppCodeGenWriteBarrier((&___GostR28147Cbc_2), value);
	}

	inline static int32_t get_offset_of_ID_Gost28147_89_CryptoPro_A_ParamSet_3() { return static_cast<int32_t>(offsetof(CryptoProObjectIdentifiers_t6FBDFC987F7A8828381DCDC802E2CB1EC3A258B1_StaticFields, ___ID_Gost28147_89_CryptoPro_A_ParamSet_3)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_ID_Gost28147_89_CryptoPro_A_ParamSet_3() const { return ___ID_Gost28147_89_CryptoPro_A_ParamSet_3; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_ID_Gost28147_89_CryptoPro_A_ParamSet_3() { return &___ID_Gost28147_89_CryptoPro_A_ParamSet_3; }
	inline void set_ID_Gost28147_89_CryptoPro_A_ParamSet_3(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___ID_Gost28147_89_CryptoPro_A_ParamSet_3 = value;
		Il2CppCodeGenWriteBarrier((&___ID_Gost28147_89_CryptoPro_A_ParamSet_3), value);
	}

	inline static int32_t get_offset_of_GostR3410x94_4() { return static_cast<int32_t>(offsetof(CryptoProObjectIdentifiers_t6FBDFC987F7A8828381DCDC802E2CB1EC3A258B1_StaticFields, ___GostR3410x94_4)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_GostR3410x94_4() const { return ___GostR3410x94_4; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_GostR3410x94_4() { return &___GostR3410x94_4; }
	inline void set_GostR3410x94_4(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___GostR3410x94_4 = value;
		Il2CppCodeGenWriteBarrier((&___GostR3410x94_4), value);
	}

	inline static int32_t get_offset_of_GostR3410x2001_5() { return static_cast<int32_t>(offsetof(CryptoProObjectIdentifiers_t6FBDFC987F7A8828381DCDC802E2CB1EC3A258B1_StaticFields, ___GostR3410x2001_5)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_GostR3410x2001_5() const { return ___GostR3410x2001_5; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_GostR3410x2001_5() { return &___GostR3410x2001_5; }
	inline void set_GostR3410x2001_5(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___GostR3410x2001_5 = value;
		Il2CppCodeGenWriteBarrier((&___GostR3410x2001_5), value);
	}

	inline static int32_t get_offset_of_GostR3411x94WithGostR3410x94_6() { return static_cast<int32_t>(offsetof(CryptoProObjectIdentifiers_t6FBDFC987F7A8828381DCDC802E2CB1EC3A258B1_StaticFields, ___GostR3411x94WithGostR3410x94_6)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_GostR3411x94WithGostR3410x94_6() const { return ___GostR3411x94WithGostR3410x94_6; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_GostR3411x94WithGostR3410x94_6() { return &___GostR3411x94WithGostR3410x94_6; }
	inline void set_GostR3411x94WithGostR3410x94_6(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___GostR3411x94WithGostR3410x94_6 = value;
		Il2CppCodeGenWriteBarrier((&___GostR3411x94WithGostR3410x94_6), value);
	}

	inline static int32_t get_offset_of_GostR3411x94WithGostR3410x2001_7() { return static_cast<int32_t>(offsetof(CryptoProObjectIdentifiers_t6FBDFC987F7A8828381DCDC802E2CB1EC3A258B1_StaticFields, ___GostR3411x94WithGostR3410x2001_7)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_GostR3411x94WithGostR3410x2001_7() const { return ___GostR3411x94WithGostR3410x2001_7; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_GostR3411x94WithGostR3410x2001_7() { return &___GostR3411x94WithGostR3410x2001_7; }
	inline void set_GostR3411x94WithGostR3410x2001_7(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___GostR3411x94WithGostR3410x2001_7 = value;
		Il2CppCodeGenWriteBarrier((&___GostR3411x94WithGostR3410x2001_7), value);
	}

	inline static int32_t get_offset_of_GostR3411x94CryptoProParamSet_8() { return static_cast<int32_t>(offsetof(CryptoProObjectIdentifiers_t6FBDFC987F7A8828381DCDC802E2CB1EC3A258B1_StaticFields, ___GostR3411x94CryptoProParamSet_8)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_GostR3411x94CryptoProParamSet_8() const { return ___GostR3411x94CryptoProParamSet_8; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_GostR3411x94CryptoProParamSet_8() { return &___GostR3411x94CryptoProParamSet_8; }
	inline void set_GostR3411x94CryptoProParamSet_8(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___GostR3411x94CryptoProParamSet_8 = value;
		Il2CppCodeGenWriteBarrier((&___GostR3411x94CryptoProParamSet_8), value);
	}

	inline static int32_t get_offset_of_GostR3410x94CryptoProA_9() { return static_cast<int32_t>(offsetof(CryptoProObjectIdentifiers_t6FBDFC987F7A8828381DCDC802E2CB1EC3A258B1_StaticFields, ___GostR3410x94CryptoProA_9)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_GostR3410x94CryptoProA_9() const { return ___GostR3410x94CryptoProA_9; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_GostR3410x94CryptoProA_9() { return &___GostR3410x94CryptoProA_9; }
	inline void set_GostR3410x94CryptoProA_9(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___GostR3410x94CryptoProA_9 = value;
		Il2CppCodeGenWriteBarrier((&___GostR3410x94CryptoProA_9), value);
	}

	inline static int32_t get_offset_of_GostR3410x94CryptoProB_10() { return static_cast<int32_t>(offsetof(CryptoProObjectIdentifiers_t6FBDFC987F7A8828381DCDC802E2CB1EC3A258B1_StaticFields, ___GostR3410x94CryptoProB_10)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_GostR3410x94CryptoProB_10() const { return ___GostR3410x94CryptoProB_10; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_GostR3410x94CryptoProB_10() { return &___GostR3410x94CryptoProB_10; }
	inline void set_GostR3410x94CryptoProB_10(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___GostR3410x94CryptoProB_10 = value;
		Il2CppCodeGenWriteBarrier((&___GostR3410x94CryptoProB_10), value);
	}

	inline static int32_t get_offset_of_GostR3410x94CryptoProC_11() { return static_cast<int32_t>(offsetof(CryptoProObjectIdentifiers_t6FBDFC987F7A8828381DCDC802E2CB1EC3A258B1_StaticFields, ___GostR3410x94CryptoProC_11)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_GostR3410x94CryptoProC_11() const { return ___GostR3410x94CryptoProC_11; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_GostR3410x94CryptoProC_11() { return &___GostR3410x94CryptoProC_11; }
	inline void set_GostR3410x94CryptoProC_11(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___GostR3410x94CryptoProC_11 = value;
		Il2CppCodeGenWriteBarrier((&___GostR3410x94CryptoProC_11), value);
	}

	inline static int32_t get_offset_of_GostR3410x94CryptoProD_12() { return static_cast<int32_t>(offsetof(CryptoProObjectIdentifiers_t6FBDFC987F7A8828381DCDC802E2CB1EC3A258B1_StaticFields, ___GostR3410x94CryptoProD_12)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_GostR3410x94CryptoProD_12() const { return ___GostR3410x94CryptoProD_12; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_GostR3410x94CryptoProD_12() { return &___GostR3410x94CryptoProD_12; }
	inline void set_GostR3410x94CryptoProD_12(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___GostR3410x94CryptoProD_12 = value;
		Il2CppCodeGenWriteBarrier((&___GostR3410x94CryptoProD_12), value);
	}

	inline static int32_t get_offset_of_GostR3410x94CryptoProXchA_13() { return static_cast<int32_t>(offsetof(CryptoProObjectIdentifiers_t6FBDFC987F7A8828381DCDC802E2CB1EC3A258B1_StaticFields, ___GostR3410x94CryptoProXchA_13)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_GostR3410x94CryptoProXchA_13() const { return ___GostR3410x94CryptoProXchA_13; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_GostR3410x94CryptoProXchA_13() { return &___GostR3410x94CryptoProXchA_13; }
	inline void set_GostR3410x94CryptoProXchA_13(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___GostR3410x94CryptoProXchA_13 = value;
		Il2CppCodeGenWriteBarrier((&___GostR3410x94CryptoProXchA_13), value);
	}

	inline static int32_t get_offset_of_GostR3410x94CryptoProXchB_14() { return static_cast<int32_t>(offsetof(CryptoProObjectIdentifiers_t6FBDFC987F7A8828381DCDC802E2CB1EC3A258B1_StaticFields, ___GostR3410x94CryptoProXchB_14)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_GostR3410x94CryptoProXchB_14() const { return ___GostR3410x94CryptoProXchB_14; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_GostR3410x94CryptoProXchB_14() { return &___GostR3410x94CryptoProXchB_14; }
	inline void set_GostR3410x94CryptoProXchB_14(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___GostR3410x94CryptoProXchB_14 = value;
		Il2CppCodeGenWriteBarrier((&___GostR3410x94CryptoProXchB_14), value);
	}

	inline static int32_t get_offset_of_GostR3410x94CryptoProXchC_15() { return static_cast<int32_t>(offsetof(CryptoProObjectIdentifiers_t6FBDFC987F7A8828381DCDC802E2CB1EC3A258B1_StaticFields, ___GostR3410x94CryptoProXchC_15)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_GostR3410x94CryptoProXchC_15() const { return ___GostR3410x94CryptoProXchC_15; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_GostR3410x94CryptoProXchC_15() { return &___GostR3410x94CryptoProXchC_15; }
	inline void set_GostR3410x94CryptoProXchC_15(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___GostR3410x94CryptoProXchC_15 = value;
		Il2CppCodeGenWriteBarrier((&___GostR3410x94CryptoProXchC_15), value);
	}

	inline static int32_t get_offset_of_GostR3410x2001CryptoProA_16() { return static_cast<int32_t>(offsetof(CryptoProObjectIdentifiers_t6FBDFC987F7A8828381DCDC802E2CB1EC3A258B1_StaticFields, ___GostR3410x2001CryptoProA_16)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_GostR3410x2001CryptoProA_16() const { return ___GostR3410x2001CryptoProA_16; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_GostR3410x2001CryptoProA_16() { return &___GostR3410x2001CryptoProA_16; }
	inline void set_GostR3410x2001CryptoProA_16(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___GostR3410x2001CryptoProA_16 = value;
		Il2CppCodeGenWriteBarrier((&___GostR3410x2001CryptoProA_16), value);
	}

	inline static int32_t get_offset_of_GostR3410x2001CryptoProB_17() { return static_cast<int32_t>(offsetof(CryptoProObjectIdentifiers_t6FBDFC987F7A8828381DCDC802E2CB1EC3A258B1_StaticFields, ___GostR3410x2001CryptoProB_17)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_GostR3410x2001CryptoProB_17() const { return ___GostR3410x2001CryptoProB_17; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_GostR3410x2001CryptoProB_17() { return &___GostR3410x2001CryptoProB_17; }
	inline void set_GostR3410x2001CryptoProB_17(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___GostR3410x2001CryptoProB_17 = value;
		Il2CppCodeGenWriteBarrier((&___GostR3410x2001CryptoProB_17), value);
	}

	inline static int32_t get_offset_of_GostR3410x2001CryptoProC_18() { return static_cast<int32_t>(offsetof(CryptoProObjectIdentifiers_t6FBDFC987F7A8828381DCDC802E2CB1EC3A258B1_StaticFields, ___GostR3410x2001CryptoProC_18)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_GostR3410x2001CryptoProC_18() const { return ___GostR3410x2001CryptoProC_18; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_GostR3410x2001CryptoProC_18() { return &___GostR3410x2001CryptoProC_18; }
	inline void set_GostR3410x2001CryptoProC_18(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___GostR3410x2001CryptoProC_18 = value;
		Il2CppCodeGenWriteBarrier((&___GostR3410x2001CryptoProC_18), value);
	}

	inline static int32_t get_offset_of_GostR3410x2001CryptoProXchA_19() { return static_cast<int32_t>(offsetof(CryptoProObjectIdentifiers_t6FBDFC987F7A8828381DCDC802E2CB1EC3A258B1_StaticFields, ___GostR3410x2001CryptoProXchA_19)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_GostR3410x2001CryptoProXchA_19() const { return ___GostR3410x2001CryptoProXchA_19; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_GostR3410x2001CryptoProXchA_19() { return &___GostR3410x2001CryptoProXchA_19; }
	inline void set_GostR3410x2001CryptoProXchA_19(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___GostR3410x2001CryptoProXchA_19 = value;
		Il2CppCodeGenWriteBarrier((&___GostR3410x2001CryptoProXchA_19), value);
	}

	inline static int32_t get_offset_of_GostR3410x2001CryptoProXchB_20() { return static_cast<int32_t>(offsetof(CryptoProObjectIdentifiers_t6FBDFC987F7A8828381DCDC802E2CB1EC3A258B1_StaticFields, ___GostR3410x2001CryptoProXchB_20)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_GostR3410x2001CryptoProXchB_20() const { return ___GostR3410x2001CryptoProXchB_20; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_GostR3410x2001CryptoProXchB_20() { return &___GostR3410x2001CryptoProXchB_20; }
	inline void set_GostR3410x2001CryptoProXchB_20(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___GostR3410x2001CryptoProXchB_20 = value;
		Il2CppCodeGenWriteBarrier((&___GostR3410x2001CryptoProXchB_20), value);
	}

	inline static int32_t get_offset_of_GostElSgDH3410Default_21() { return static_cast<int32_t>(offsetof(CryptoProObjectIdentifiers_t6FBDFC987F7A8828381DCDC802E2CB1EC3A258B1_StaticFields, ___GostElSgDH3410Default_21)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_GostElSgDH3410Default_21() const { return ___GostElSgDH3410Default_21; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_GostElSgDH3410Default_21() { return &___GostElSgDH3410Default_21; }
	inline void set_GostElSgDH3410Default_21(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___GostElSgDH3410Default_21 = value;
		Il2CppCodeGenWriteBarrier((&___GostElSgDH3410Default_21), value);
	}

	inline static int32_t get_offset_of_GostElSgDH3410x1_22() { return static_cast<int32_t>(offsetof(CryptoProObjectIdentifiers_t6FBDFC987F7A8828381DCDC802E2CB1EC3A258B1_StaticFields, ___GostElSgDH3410x1_22)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_GostElSgDH3410x1_22() const { return ___GostElSgDH3410x1_22; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_GostElSgDH3410x1_22() { return &___GostElSgDH3410x1_22; }
	inline void set_GostElSgDH3410x1_22(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___GostElSgDH3410x1_22 = value;
		Il2CppCodeGenWriteBarrier((&___GostElSgDH3410x1_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRYPTOPROOBJECTIDENTIFIERS_T6FBDFC987F7A8828381DCDC802E2CB1EC3A258B1_H
#ifndef ECGOST3410NAMEDCURVES_T0C88B00B67BA085050DE1BCCC8BAEF2A7CAC6C39_H
#define ECGOST3410NAMEDCURVES_T0C88B00B67BA085050DE1BCCC8BAEF2A7CAC6C39_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.CryptoPro.ECGost3410NamedCurves
struct  ECGost3410NamedCurves_t0C88B00B67BA085050DE1BCCC8BAEF2A7CAC6C39  : public RuntimeObject
{
public:

public:
};

struct ECGost3410NamedCurves_t0C88B00B67BA085050DE1BCCC8BAEF2A7CAC6C39_StaticFields
{
public:
	// System.Collections.IDictionary Org.BouncyCastle.Asn1.CryptoPro.ECGost3410NamedCurves::objIds
	RuntimeObject* ___objIds_0;
	// System.Collections.IDictionary Org.BouncyCastle.Asn1.CryptoPro.ECGost3410NamedCurves::parameters
	RuntimeObject* ___parameters_1;
	// System.Collections.IDictionary Org.BouncyCastle.Asn1.CryptoPro.ECGost3410NamedCurves::names
	RuntimeObject* ___names_2;

public:
	inline static int32_t get_offset_of_objIds_0() { return static_cast<int32_t>(offsetof(ECGost3410NamedCurves_t0C88B00B67BA085050DE1BCCC8BAEF2A7CAC6C39_StaticFields, ___objIds_0)); }
	inline RuntimeObject* get_objIds_0() const { return ___objIds_0; }
	inline RuntimeObject** get_address_of_objIds_0() { return &___objIds_0; }
	inline void set_objIds_0(RuntimeObject* value)
	{
		___objIds_0 = value;
		Il2CppCodeGenWriteBarrier((&___objIds_0), value);
	}

	inline static int32_t get_offset_of_parameters_1() { return static_cast<int32_t>(offsetof(ECGost3410NamedCurves_t0C88B00B67BA085050DE1BCCC8BAEF2A7CAC6C39_StaticFields, ___parameters_1)); }
	inline RuntimeObject* get_parameters_1() const { return ___parameters_1; }
	inline RuntimeObject** get_address_of_parameters_1() { return &___parameters_1; }
	inline void set_parameters_1(RuntimeObject* value)
	{
		___parameters_1 = value;
		Il2CppCodeGenWriteBarrier((&___parameters_1), value);
	}

	inline static int32_t get_offset_of_names_2() { return static_cast<int32_t>(offsetof(ECGost3410NamedCurves_t0C88B00B67BA085050DE1BCCC8BAEF2A7CAC6C39_StaticFields, ___names_2)); }
	inline RuntimeObject* get_names_2() const { return ___names_2; }
	inline RuntimeObject** get_address_of_names_2() { return &___names_2; }
	inline void set_names_2(RuntimeObject* value)
	{
		___names_2 = value;
		Il2CppCodeGenWriteBarrier((&___names_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECGOST3410NAMEDCURVES_T0C88B00B67BA085050DE1BCCC8BAEF2A7CAC6C39_H
#ifndef GOST3410NAMEDPARAMETERS_TF61B5B4918143C6E3604A67AA4F4E55928F83FA5_H
#define GOST3410NAMEDPARAMETERS_TF61B5B4918143C6E3604A67AA4F4E55928F83FA5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.CryptoPro.Gost3410NamedParameters
struct  Gost3410NamedParameters_tF61B5B4918143C6E3604A67AA4F4E55928F83FA5  : public RuntimeObject
{
public:

public:
};

struct Gost3410NamedParameters_tF61B5B4918143C6E3604A67AA4F4E55928F83FA5_StaticFields
{
public:
	// System.Collections.IDictionary Org.BouncyCastle.Asn1.CryptoPro.Gost3410NamedParameters::objIds
	RuntimeObject* ___objIds_0;
	// System.Collections.IDictionary Org.BouncyCastle.Asn1.CryptoPro.Gost3410NamedParameters::parameters
	RuntimeObject* ___parameters_1;
	// Org.BouncyCastle.Asn1.CryptoPro.Gost3410ParamSetParameters Org.BouncyCastle.Asn1.CryptoPro.Gost3410NamedParameters::cryptoProA
	Gost3410ParamSetParameters_tA09B7D79E4C03898F7D5F3AB8C9A8A061B69E6C2 * ___cryptoProA_2;
	// Org.BouncyCastle.Asn1.CryptoPro.Gost3410ParamSetParameters Org.BouncyCastle.Asn1.CryptoPro.Gost3410NamedParameters::cryptoProB
	Gost3410ParamSetParameters_tA09B7D79E4C03898F7D5F3AB8C9A8A061B69E6C2 * ___cryptoProB_3;
	// Org.BouncyCastle.Asn1.CryptoPro.Gost3410ParamSetParameters Org.BouncyCastle.Asn1.CryptoPro.Gost3410NamedParameters::cryptoProXchA
	Gost3410ParamSetParameters_tA09B7D79E4C03898F7D5F3AB8C9A8A061B69E6C2 * ___cryptoProXchA_4;

public:
	inline static int32_t get_offset_of_objIds_0() { return static_cast<int32_t>(offsetof(Gost3410NamedParameters_tF61B5B4918143C6E3604A67AA4F4E55928F83FA5_StaticFields, ___objIds_0)); }
	inline RuntimeObject* get_objIds_0() const { return ___objIds_0; }
	inline RuntimeObject** get_address_of_objIds_0() { return &___objIds_0; }
	inline void set_objIds_0(RuntimeObject* value)
	{
		___objIds_0 = value;
		Il2CppCodeGenWriteBarrier((&___objIds_0), value);
	}

	inline static int32_t get_offset_of_parameters_1() { return static_cast<int32_t>(offsetof(Gost3410NamedParameters_tF61B5B4918143C6E3604A67AA4F4E55928F83FA5_StaticFields, ___parameters_1)); }
	inline RuntimeObject* get_parameters_1() const { return ___parameters_1; }
	inline RuntimeObject** get_address_of_parameters_1() { return &___parameters_1; }
	inline void set_parameters_1(RuntimeObject* value)
	{
		___parameters_1 = value;
		Il2CppCodeGenWriteBarrier((&___parameters_1), value);
	}

	inline static int32_t get_offset_of_cryptoProA_2() { return static_cast<int32_t>(offsetof(Gost3410NamedParameters_tF61B5B4918143C6E3604A67AA4F4E55928F83FA5_StaticFields, ___cryptoProA_2)); }
	inline Gost3410ParamSetParameters_tA09B7D79E4C03898F7D5F3AB8C9A8A061B69E6C2 * get_cryptoProA_2() const { return ___cryptoProA_2; }
	inline Gost3410ParamSetParameters_tA09B7D79E4C03898F7D5F3AB8C9A8A061B69E6C2 ** get_address_of_cryptoProA_2() { return &___cryptoProA_2; }
	inline void set_cryptoProA_2(Gost3410ParamSetParameters_tA09B7D79E4C03898F7D5F3AB8C9A8A061B69E6C2 * value)
	{
		___cryptoProA_2 = value;
		Il2CppCodeGenWriteBarrier((&___cryptoProA_2), value);
	}

	inline static int32_t get_offset_of_cryptoProB_3() { return static_cast<int32_t>(offsetof(Gost3410NamedParameters_tF61B5B4918143C6E3604A67AA4F4E55928F83FA5_StaticFields, ___cryptoProB_3)); }
	inline Gost3410ParamSetParameters_tA09B7D79E4C03898F7D5F3AB8C9A8A061B69E6C2 * get_cryptoProB_3() const { return ___cryptoProB_3; }
	inline Gost3410ParamSetParameters_tA09B7D79E4C03898F7D5F3AB8C9A8A061B69E6C2 ** get_address_of_cryptoProB_3() { return &___cryptoProB_3; }
	inline void set_cryptoProB_3(Gost3410ParamSetParameters_tA09B7D79E4C03898F7D5F3AB8C9A8A061B69E6C2 * value)
	{
		___cryptoProB_3 = value;
		Il2CppCodeGenWriteBarrier((&___cryptoProB_3), value);
	}

	inline static int32_t get_offset_of_cryptoProXchA_4() { return static_cast<int32_t>(offsetof(Gost3410NamedParameters_tF61B5B4918143C6E3604A67AA4F4E55928F83FA5_StaticFields, ___cryptoProXchA_4)); }
	inline Gost3410ParamSetParameters_tA09B7D79E4C03898F7D5F3AB8C9A8A061B69E6C2 * get_cryptoProXchA_4() const { return ___cryptoProXchA_4; }
	inline Gost3410ParamSetParameters_tA09B7D79E4C03898F7D5F3AB8C9A8A061B69E6C2 ** get_address_of_cryptoProXchA_4() { return &___cryptoProXchA_4; }
	inline void set_cryptoProXchA_4(Gost3410ParamSetParameters_tA09B7D79E4C03898F7D5F3AB8C9A8A061B69E6C2 * value)
	{
		___cryptoProXchA_4 = value;
		Il2CppCodeGenWriteBarrier((&___cryptoProXchA_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GOST3410NAMEDPARAMETERS_TF61B5B4918143C6E3604A67AA4F4E55928F83FA5_H
#ifndef DEROCTETSTRINGPARSER_T903FF756D87E0AFCD73B346F9CC472474C960453_H
#define DEROCTETSTRINGPARSER_T903FF756D87E0AFCD73B346F9CC472474C960453_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.DerOctetStringParser
struct  DerOctetStringParser_t903FF756D87E0AFCD73B346F9CC472474C960453  : public RuntimeObject
{
public:
	// Org.BouncyCastle.Asn1.DefiniteLengthInputStream Org.BouncyCastle.Asn1.DerOctetStringParser::stream
	DefiniteLengthInputStream_t9461CF6517E1119EFD75E65D001C0969476E74D1 * ___stream_0;

public:
	inline static int32_t get_offset_of_stream_0() { return static_cast<int32_t>(offsetof(DerOctetStringParser_t903FF756D87E0AFCD73B346F9CC472474C960453, ___stream_0)); }
	inline DefiniteLengthInputStream_t9461CF6517E1119EFD75E65D001C0969476E74D1 * get_stream_0() const { return ___stream_0; }
	inline DefiniteLengthInputStream_t9461CF6517E1119EFD75E65D001C0969476E74D1 ** get_address_of_stream_0() { return &___stream_0; }
	inline void set_stream_0(DefiniteLengthInputStream_t9461CF6517E1119EFD75E65D001C0969476E74D1 * value)
	{
		___stream_0 = value;
		Il2CppCodeGenWriteBarrier((&___stream_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEROCTETSTRINGPARSER_T903FF756D87E0AFCD73B346F9CC472474C960453_H
#ifndef DERSEQUENCEPARSER_T3972E45BFD6EDFD9B205AD8D02D28A90FBA637CD_H
#define DERSEQUENCEPARSER_T3972E45BFD6EDFD9B205AD8D02D28A90FBA637CD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.DerSequenceParser
struct  DerSequenceParser_t3972E45BFD6EDFD9B205AD8D02D28A90FBA637CD  : public RuntimeObject
{
public:
	// Org.BouncyCastle.Asn1.Asn1StreamParser Org.BouncyCastle.Asn1.DerSequenceParser::_parser
	Asn1StreamParser_t2FE3FB89834CCEE990CDD0F38B5DD0960CDC9C90 * ____parser_0;

public:
	inline static int32_t get_offset_of__parser_0() { return static_cast<int32_t>(offsetof(DerSequenceParser_t3972E45BFD6EDFD9B205AD8D02D28A90FBA637CD, ____parser_0)); }
	inline Asn1StreamParser_t2FE3FB89834CCEE990CDD0F38B5DD0960CDC9C90 * get__parser_0() const { return ____parser_0; }
	inline Asn1StreamParser_t2FE3FB89834CCEE990CDD0F38B5DD0960CDC9C90 ** get_address_of__parser_0() { return &____parser_0; }
	inline void set__parser_0(Asn1StreamParser_t2FE3FB89834CCEE990CDD0F38B5DD0960CDC9C90 * value)
	{
		____parser_0 = value;
		Il2CppCodeGenWriteBarrier((&____parser_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERSEQUENCEPARSER_T3972E45BFD6EDFD9B205AD8D02D28A90FBA637CD_H
#ifndef DERSETPARSER_T22F843F89A112F11B8ED89F2804AEA97C6FABD73_H
#define DERSETPARSER_T22F843F89A112F11B8ED89F2804AEA97C6FABD73_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.DerSetParser
struct  DerSetParser_t22F843F89A112F11B8ED89F2804AEA97C6FABD73  : public RuntimeObject
{
public:
	// Org.BouncyCastle.Asn1.Asn1StreamParser Org.BouncyCastle.Asn1.DerSetParser::_parser
	Asn1StreamParser_t2FE3FB89834CCEE990CDD0F38B5DD0960CDC9C90 * ____parser_0;

public:
	inline static int32_t get_offset_of__parser_0() { return static_cast<int32_t>(offsetof(DerSetParser_t22F843F89A112F11B8ED89F2804AEA97C6FABD73, ____parser_0)); }
	inline Asn1StreamParser_t2FE3FB89834CCEE990CDD0F38B5DD0960CDC9C90 * get__parser_0() const { return ____parser_0; }
	inline Asn1StreamParser_t2FE3FB89834CCEE990CDD0F38B5DD0960CDC9C90 ** get_address_of__parser_0() { return &____parser_0; }
	inline void set__parser_0(Asn1StreamParser_t2FE3FB89834CCEE990CDD0F38B5DD0960CDC9C90 * value)
	{
		____parser_0 = value;
		Il2CppCodeGenWriteBarrier((&____parser_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERSETPARSER_T22F843F89A112F11B8ED89F2804AEA97C6FABD73_H
#ifndef IANAOBJECTIDENTIFIERS_T30772100C27C8AC123FC767639073FDD8925DBE3_H
#define IANAOBJECTIDENTIFIERS_T30772100C27C8AC123FC767639073FDD8925DBE3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.Iana.IanaObjectIdentifiers
struct  IanaObjectIdentifiers_t30772100C27C8AC123FC767639073FDD8925DBE3  : public RuntimeObject
{
public:

public:
};

struct IanaObjectIdentifiers_t30772100C27C8AC123FC767639073FDD8925DBE3_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Iana.IanaObjectIdentifiers::IsakmpOakley
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IsakmpOakley_0;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Iana.IanaObjectIdentifiers::HmacMD5
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___HmacMD5_1;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Iana.IanaObjectIdentifiers::HmacSha1
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___HmacSha1_2;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Iana.IanaObjectIdentifiers::HmacTiger
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___HmacTiger_3;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Iana.IanaObjectIdentifiers::HmacRipeMD160
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___HmacRipeMD160_4;

public:
	inline static int32_t get_offset_of_IsakmpOakley_0() { return static_cast<int32_t>(offsetof(IanaObjectIdentifiers_t30772100C27C8AC123FC767639073FDD8925DBE3_StaticFields, ___IsakmpOakley_0)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IsakmpOakley_0() const { return ___IsakmpOakley_0; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IsakmpOakley_0() { return &___IsakmpOakley_0; }
	inline void set_IsakmpOakley_0(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IsakmpOakley_0 = value;
		Il2CppCodeGenWriteBarrier((&___IsakmpOakley_0), value);
	}

	inline static int32_t get_offset_of_HmacMD5_1() { return static_cast<int32_t>(offsetof(IanaObjectIdentifiers_t30772100C27C8AC123FC767639073FDD8925DBE3_StaticFields, ___HmacMD5_1)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_HmacMD5_1() const { return ___HmacMD5_1; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_HmacMD5_1() { return &___HmacMD5_1; }
	inline void set_HmacMD5_1(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___HmacMD5_1 = value;
		Il2CppCodeGenWriteBarrier((&___HmacMD5_1), value);
	}

	inline static int32_t get_offset_of_HmacSha1_2() { return static_cast<int32_t>(offsetof(IanaObjectIdentifiers_t30772100C27C8AC123FC767639073FDD8925DBE3_StaticFields, ___HmacSha1_2)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_HmacSha1_2() const { return ___HmacSha1_2; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_HmacSha1_2() { return &___HmacSha1_2; }
	inline void set_HmacSha1_2(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___HmacSha1_2 = value;
		Il2CppCodeGenWriteBarrier((&___HmacSha1_2), value);
	}

	inline static int32_t get_offset_of_HmacTiger_3() { return static_cast<int32_t>(offsetof(IanaObjectIdentifiers_t30772100C27C8AC123FC767639073FDD8925DBE3_StaticFields, ___HmacTiger_3)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_HmacTiger_3() const { return ___HmacTiger_3; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_HmacTiger_3() { return &___HmacTiger_3; }
	inline void set_HmacTiger_3(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___HmacTiger_3 = value;
		Il2CppCodeGenWriteBarrier((&___HmacTiger_3), value);
	}

	inline static int32_t get_offset_of_HmacRipeMD160_4() { return static_cast<int32_t>(offsetof(IanaObjectIdentifiers_t30772100C27C8AC123FC767639073FDD8925DBE3_StaticFields, ___HmacRipeMD160_4)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_HmacRipeMD160_4() const { return ___HmacRipeMD160_4; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_HmacRipeMD160_4() { return &___HmacRipeMD160_4; }
	inline void set_HmacRipeMD160_4(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___HmacRipeMD160_4 = value;
		Il2CppCodeGenWriteBarrier((&___HmacRipeMD160_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IANAOBJECTIDENTIFIERS_T30772100C27C8AC123FC767639073FDD8925DBE3_H
#ifndef MISCOBJECTIDENTIFIERS_T3188E33E8700951DC1133A1CD98D6C2E889A7721_H
#define MISCOBJECTIDENTIFIERS_T3188E33E8700951DC1133A1CD98D6C2E889A7721_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers
struct  MiscObjectIdentifiers_t3188E33E8700951DC1133A1CD98D6C2E889A7721  : public RuntimeObject
{
public:

public:
};

struct MiscObjectIdentifiers_t3188E33E8700951DC1133A1CD98D6C2E889A7721_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::Netscape
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___Netscape_0;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::NetscapeCertType
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___NetscapeCertType_1;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::NetscapeBaseUrl
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___NetscapeBaseUrl_2;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::NetscapeRevocationUrl
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___NetscapeRevocationUrl_3;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::NetscapeCARevocationUrl
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___NetscapeCARevocationUrl_4;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::NetscapeRenewalUrl
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___NetscapeRenewalUrl_5;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::NetscapeCAPolicyUrl
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___NetscapeCAPolicyUrl_6;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::NetscapeSslServerName
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___NetscapeSslServerName_7;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::NetscapeCertComment
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___NetscapeCertComment_8;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::Verisign
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___Verisign_9;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::VerisignCzagExtension
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___VerisignCzagExtension_10;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::VerisignPrivate_6_9
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___VerisignPrivate_6_9_11;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::VerisignOnSiteJurisdictionHash
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___VerisignOnSiteJurisdictionHash_12;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::VerisignBitString_6_13
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___VerisignBitString_6_13_13;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::VerisignDnbDunsNumber
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___VerisignDnbDunsNumber_14;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::VerisignIssStrongCrypto
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___VerisignIssStrongCrypto_15;
	// System.String Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::Novell
	String_t* ___Novell_16;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::NovellSecurityAttribs
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___NovellSecurityAttribs_17;
	// System.String Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::Entrust
	String_t* ___Entrust_18;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::EntrustVersionExtension
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___EntrustVersionExtension_19;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::as_sys_sec_alg_ideaCBC
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___as_sys_sec_alg_ideaCBC_20;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::cryptlib
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___cryptlib_21;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::cryptlib_algorithm
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___cryptlib_algorithm_22;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::cryptlib_algorithm_blowfish_ECB
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___cryptlib_algorithm_blowfish_ECB_23;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::cryptlib_algorithm_blowfish_CBC
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___cryptlib_algorithm_blowfish_CBC_24;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::cryptlib_algorithm_blowfish_CFB
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___cryptlib_algorithm_blowfish_CFB_25;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::cryptlib_algorithm_blowfish_OFB
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___cryptlib_algorithm_blowfish_OFB_26;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::blake2
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___blake2_27;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::id_blake2b160
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___id_blake2b160_28;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::id_blake2b256
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___id_blake2b256_29;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::id_blake2b384
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___id_blake2b384_30;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::id_blake2b512
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___id_blake2b512_31;

public:
	inline static int32_t get_offset_of_Netscape_0() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t3188E33E8700951DC1133A1CD98D6C2E889A7721_StaticFields, ___Netscape_0)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_Netscape_0() const { return ___Netscape_0; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_Netscape_0() { return &___Netscape_0; }
	inline void set_Netscape_0(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___Netscape_0 = value;
		Il2CppCodeGenWriteBarrier((&___Netscape_0), value);
	}

	inline static int32_t get_offset_of_NetscapeCertType_1() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t3188E33E8700951DC1133A1CD98D6C2E889A7721_StaticFields, ___NetscapeCertType_1)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_NetscapeCertType_1() const { return ___NetscapeCertType_1; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_NetscapeCertType_1() { return &___NetscapeCertType_1; }
	inline void set_NetscapeCertType_1(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___NetscapeCertType_1 = value;
		Il2CppCodeGenWriteBarrier((&___NetscapeCertType_1), value);
	}

	inline static int32_t get_offset_of_NetscapeBaseUrl_2() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t3188E33E8700951DC1133A1CD98D6C2E889A7721_StaticFields, ___NetscapeBaseUrl_2)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_NetscapeBaseUrl_2() const { return ___NetscapeBaseUrl_2; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_NetscapeBaseUrl_2() { return &___NetscapeBaseUrl_2; }
	inline void set_NetscapeBaseUrl_2(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___NetscapeBaseUrl_2 = value;
		Il2CppCodeGenWriteBarrier((&___NetscapeBaseUrl_2), value);
	}

	inline static int32_t get_offset_of_NetscapeRevocationUrl_3() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t3188E33E8700951DC1133A1CD98D6C2E889A7721_StaticFields, ___NetscapeRevocationUrl_3)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_NetscapeRevocationUrl_3() const { return ___NetscapeRevocationUrl_3; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_NetscapeRevocationUrl_3() { return &___NetscapeRevocationUrl_3; }
	inline void set_NetscapeRevocationUrl_3(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___NetscapeRevocationUrl_3 = value;
		Il2CppCodeGenWriteBarrier((&___NetscapeRevocationUrl_3), value);
	}

	inline static int32_t get_offset_of_NetscapeCARevocationUrl_4() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t3188E33E8700951DC1133A1CD98D6C2E889A7721_StaticFields, ___NetscapeCARevocationUrl_4)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_NetscapeCARevocationUrl_4() const { return ___NetscapeCARevocationUrl_4; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_NetscapeCARevocationUrl_4() { return &___NetscapeCARevocationUrl_4; }
	inline void set_NetscapeCARevocationUrl_4(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___NetscapeCARevocationUrl_4 = value;
		Il2CppCodeGenWriteBarrier((&___NetscapeCARevocationUrl_4), value);
	}

	inline static int32_t get_offset_of_NetscapeRenewalUrl_5() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t3188E33E8700951DC1133A1CD98D6C2E889A7721_StaticFields, ___NetscapeRenewalUrl_5)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_NetscapeRenewalUrl_5() const { return ___NetscapeRenewalUrl_5; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_NetscapeRenewalUrl_5() { return &___NetscapeRenewalUrl_5; }
	inline void set_NetscapeRenewalUrl_5(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___NetscapeRenewalUrl_5 = value;
		Il2CppCodeGenWriteBarrier((&___NetscapeRenewalUrl_5), value);
	}

	inline static int32_t get_offset_of_NetscapeCAPolicyUrl_6() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t3188E33E8700951DC1133A1CD98D6C2E889A7721_StaticFields, ___NetscapeCAPolicyUrl_6)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_NetscapeCAPolicyUrl_6() const { return ___NetscapeCAPolicyUrl_6; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_NetscapeCAPolicyUrl_6() { return &___NetscapeCAPolicyUrl_6; }
	inline void set_NetscapeCAPolicyUrl_6(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___NetscapeCAPolicyUrl_6 = value;
		Il2CppCodeGenWriteBarrier((&___NetscapeCAPolicyUrl_6), value);
	}

	inline static int32_t get_offset_of_NetscapeSslServerName_7() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t3188E33E8700951DC1133A1CD98D6C2E889A7721_StaticFields, ___NetscapeSslServerName_7)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_NetscapeSslServerName_7() const { return ___NetscapeSslServerName_7; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_NetscapeSslServerName_7() { return &___NetscapeSslServerName_7; }
	inline void set_NetscapeSslServerName_7(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___NetscapeSslServerName_7 = value;
		Il2CppCodeGenWriteBarrier((&___NetscapeSslServerName_7), value);
	}

	inline static int32_t get_offset_of_NetscapeCertComment_8() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t3188E33E8700951DC1133A1CD98D6C2E889A7721_StaticFields, ___NetscapeCertComment_8)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_NetscapeCertComment_8() const { return ___NetscapeCertComment_8; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_NetscapeCertComment_8() { return &___NetscapeCertComment_8; }
	inline void set_NetscapeCertComment_8(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___NetscapeCertComment_8 = value;
		Il2CppCodeGenWriteBarrier((&___NetscapeCertComment_8), value);
	}

	inline static int32_t get_offset_of_Verisign_9() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t3188E33E8700951DC1133A1CD98D6C2E889A7721_StaticFields, ___Verisign_9)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_Verisign_9() const { return ___Verisign_9; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_Verisign_9() { return &___Verisign_9; }
	inline void set_Verisign_9(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___Verisign_9 = value;
		Il2CppCodeGenWriteBarrier((&___Verisign_9), value);
	}

	inline static int32_t get_offset_of_VerisignCzagExtension_10() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t3188E33E8700951DC1133A1CD98D6C2E889A7721_StaticFields, ___VerisignCzagExtension_10)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_VerisignCzagExtension_10() const { return ___VerisignCzagExtension_10; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_VerisignCzagExtension_10() { return &___VerisignCzagExtension_10; }
	inline void set_VerisignCzagExtension_10(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___VerisignCzagExtension_10 = value;
		Il2CppCodeGenWriteBarrier((&___VerisignCzagExtension_10), value);
	}

	inline static int32_t get_offset_of_VerisignPrivate_6_9_11() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t3188E33E8700951DC1133A1CD98D6C2E889A7721_StaticFields, ___VerisignPrivate_6_9_11)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_VerisignPrivate_6_9_11() const { return ___VerisignPrivate_6_9_11; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_VerisignPrivate_6_9_11() { return &___VerisignPrivate_6_9_11; }
	inline void set_VerisignPrivate_6_9_11(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___VerisignPrivate_6_9_11 = value;
		Il2CppCodeGenWriteBarrier((&___VerisignPrivate_6_9_11), value);
	}

	inline static int32_t get_offset_of_VerisignOnSiteJurisdictionHash_12() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t3188E33E8700951DC1133A1CD98D6C2E889A7721_StaticFields, ___VerisignOnSiteJurisdictionHash_12)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_VerisignOnSiteJurisdictionHash_12() const { return ___VerisignOnSiteJurisdictionHash_12; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_VerisignOnSiteJurisdictionHash_12() { return &___VerisignOnSiteJurisdictionHash_12; }
	inline void set_VerisignOnSiteJurisdictionHash_12(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___VerisignOnSiteJurisdictionHash_12 = value;
		Il2CppCodeGenWriteBarrier((&___VerisignOnSiteJurisdictionHash_12), value);
	}

	inline static int32_t get_offset_of_VerisignBitString_6_13_13() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t3188E33E8700951DC1133A1CD98D6C2E889A7721_StaticFields, ___VerisignBitString_6_13_13)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_VerisignBitString_6_13_13() const { return ___VerisignBitString_6_13_13; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_VerisignBitString_6_13_13() { return &___VerisignBitString_6_13_13; }
	inline void set_VerisignBitString_6_13_13(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___VerisignBitString_6_13_13 = value;
		Il2CppCodeGenWriteBarrier((&___VerisignBitString_6_13_13), value);
	}

	inline static int32_t get_offset_of_VerisignDnbDunsNumber_14() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t3188E33E8700951DC1133A1CD98D6C2E889A7721_StaticFields, ___VerisignDnbDunsNumber_14)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_VerisignDnbDunsNumber_14() const { return ___VerisignDnbDunsNumber_14; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_VerisignDnbDunsNumber_14() { return &___VerisignDnbDunsNumber_14; }
	inline void set_VerisignDnbDunsNumber_14(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___VerisignDnbDunsNumber_14 = value;
		Il2CppCodeGenWriteBarrier((&___VerisignDnbDunsNumber_14), value);
	}

	inline static int32_t get_offset_of_VerisignIssStrongCrypto_15() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t3188E33E8700951DC1133A1CD98D6C2E889A7721_StaticFields, ___VerisignIssStrongCrypto_15)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_VerisignIssStrongCrypto_15() const { return ___VerisignIssStrongCrypto_15; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_VerisignIssStrongCrypto_15() { return &___VerisignIssStrongCrypto_15; }
	inline void set_VerisignIssStrongCrypto_15(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___VerisignIssStrongCrypto_15 = value;
		Il2CppCodeGenWriteBarrier((&___VerisignIssStrongCrypto_15), value);
	}

	inline static int32_t get_offset_of_Novell_16() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t3188E33E8700951DC1133A1CD98D6C2E889A7721_StaticFields, ___Novell_16)); }
	inline String_t* get_Novell_16() const { return ___Novell_16; }
	inline String_t** get_address_of_Novell_16() { return &___Novell_16; }
	inline void set_Novell_16(String_t* value)
	{
		___Novell_16 = value;
		Il2CppCodeGenWriteBarrier((&___Novell_16), value);
	}

	inline static int32_t get_offset_of_NovellSecurityAttribs_17() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t3188E33E8700951DC1133A1CD98D6C2E889A7721_StaticFields, ___NovellSecurityAttribs_17)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_NovellSecurityAttribs_17() const { return ___NovellSecurityAttribs_17; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_NovellSecurityAttribs_17() { return &___NovellSecurityAttribs_17; }
	inline void set_NovellSecurityAttribs_17(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___NovellSecurityAttribs_17 = value;
		Il2CppCodeGenWriteBarrier((&___NovellSecurityAttribs_17), value);
	}

	inline static int32_t get_offset_of_Entrust_18() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t3188E33E8700951DC1133A1CD98D6C2E889A7721_StaticFields, ___Entrust_18)); }
	inline String_t* get_Entrust_18() const { return ___Entrust_18; }
	inline String_t** get_address_of_Entrust_18() { return &___Entrust_18; }
	inline void set_Entrust_18(String_t* value)
	{
		___Entrust_18 = value;
		Il2CppCodeGenWriteBarrier((&___Entrust_18), value);
	}

	inline static int32_t get_offset_of_EntrustVersionExtension_19() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t3188E33E8700951DC1133A1CD98D6C2E889A7721_StaticFields, ___EntrustVersionExtension_19)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_EntrustVersionExtension_19() const { return ___EntrustVersionExtension_19; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_EntrustVersionExtension_19() { return &___EntrustVersionExtension_19; }
	inline void set_EntrustVersionExtension_19(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___EntrustVersionExtension_19 = value;
		Il2CppCodeGenWriteBarrier((&___EntrustVersionExtension_19), value);
	}

	inline static int32_t get_offset_of_as_sys_sec_alg_ideaCBC_20() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t3188E33E8700951DC1133A1CD98D6C2E889A7721_StaticFields, ___as_sys_sec_alg_ideaCBC_20)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_as_sys_sec_alg_ideaCBC_20() const { return ___as_sys_sec_alg_ideaCBC_20; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_as_sys_sec_alg_ideaCBC_20() { return &___as_sys_sec_alg_ideaCBC_20; }
	inline void set_as_sys_sec_alg_ideaCBC_20(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___as_sys_sec_alg_ideaCBC_20 = value;
		Il2CppCodeGenWriteBarrier((&___as_sys_sec_alg_ideaCBC_20), value);
	}

	inline static int32_t get_offset_of_cryptlib_21() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t3188E33E8700951DC1133A1CD98D6C2E889A7721_StaticFields, ___cryptlib_21)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_cryptlib_21() const { return ___cryptlib_21; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_cryptlib_21() { return &___cryptlib_21; }
	inline void set_cryptlib_21(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___cryptlib_21 = value;
		Il2CppCodeGenWriteBarrier((&___cryptlib_21), value);
	}

	inline static int32_t get_offset_of_cryptlib_algorithm_22() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t3188E33E8700951DC1133A1CD98D6C2E889A7721_StaticFields, ___cryptlib_algorithm_22)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_cryptlib_algorithm_22() const { return ___cryptlib_algorithm_22; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_cryptlib_algorithm_22() { return &___cryptlib_algorithm_22; }
	inline void set_cryptlib_algorithm_22(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___cryptlib_algorithm_22 = value;
		Il2CppCodeGenWriteBarrier((&___cryptlib_algorithm_22), value);
	}

	inline static int32_t get_offset_of_cryptlib_algorithm_blowfish_ECB_23() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t3188E33E8700951DC1133A1CD98D6C2E889A7721_StaticFields, ___cryptlib_algorithm_blowfish_ECB_23)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_cryptlib_algorithm_blowfish_ECB_23() const { return ___cryptlib_algorithm_blowfish_ECB_23; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_cryptlib_algorithm_blowfish_ECB_23() { return &___cryptlib_algorithm_blowfish_ECB_23; }
	inline void set_cryptlib_algorithm_blowfish_ECB_23(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___cryptlib_algorithm_blowfish_ECB_23 = value;
		Il2CppCodeGenWriteBarrier((&___cryptlib_algorithm_blowfish_ECB_23), value);
	}

	inline static int32_t get_offset_of_cryptlib_algorithm_blowfish_CBC_24() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t3188E33E8700951DC1133A1CD98D6C2E889A7721_StaticFields, ___cryptlib_algorithm_blowfish_CBC_24)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_cryptlib_algorithm_blowfish_CBC_24() const { return ___cryptlib_algorithm_blowfish_CBC_24; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_cryptlib_algorithm_blowfish_CBC_24() { return &___cryptlib_algorithm_blowfish_CBC_24; }
	inline void set_cryptlib_algorithm_blowfish_CBC_24(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___cryptlib_algorithm_blowfish_CBC_24 = value;
		Il2CppCodeGenWriteBarrier((&___cryptlib_algorithm_blowfish_CBC_24), value);
	}

	inline static int32_t get_offset_of_cryptlib_algorithm_blowfish_CFB_25() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t3188E33E8700951DC1133A1CD98D6C2E889A7721_StaticFields, ___cryptlib_algorithm_blowfish_CFB_25)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_cryptlib_algorithm_blowfish_CFB_25() const { return ___cryptlib_algorithm_blowfish_CFB_25; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_cryptlib_algorithm_blowfish_CFB_25() { return &___cryptlib_algorithm_blowfish_CFB_25; }
	inline void set_cryptlib_algorithm_blowfish_CFB_25(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___cryptlib_algorithm_blowfish_CFB_25 = value;
		Il2CppCodeGenWriteBarrier((&___cryptlib_algorithm_blowfish_CFB_25), value);
	}

	inline static int32_t get_offset_of_cryptlib_algorithm_blowfish_OFB_26() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t3188E33E8700951DC1133A1CD98D6C2E889A7721_StaticFields, ___cryptlib_algorithm_blowfish_OFB_26)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_cryptlib_algorithm_blowfish_OFB_26() const { return ___cryptlib_algorithm_blowfish_OFB_26; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_cryptlib_algorithm_blowfish_OFB_26() { return &___cryptlib_algorithm_blowfish_OFB_26; }
	inline void set_cryptlib_algorithm_blowfish_OFB_26(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___cryptlib_algorithm_blowfish_OFB_26 = value;
		Il2CppCodeGenWriteBarrier((&___cryptlib_algorithm_blowfish_OFB_26), value);
	}

	inline static int32_t get_offset_of_blake2_27() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t3188E33E8700951DC1133A1CD98D6C2E889A7721_StaticFields, ___blake2_27)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_blake2_27() const { return ___blake2_27; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_blake2_27() { return &___blake2_27; }
	inline void set_blake2_27(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___blake2_27 = value;
		Il2CppCodeGenWriteBarrier((&___blake2_27), value);
	}

	inline static int32_t get_offset_of_id_blake2b160_28() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t3188E33E8700951DC1133A1CD98D6C2E889A7721_StaticFields, ___id_blake2b160_28)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_id_blake2b160_28() const { return ___id_blake2b160_28; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_id_blake2b160_28() { return &___id_blake2b160_28; }
	inline void set_id_blake2b160_28(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___id_blake2b160_28 = value;
		Il2CppCodeGenWriteBarrier((&___id_blake2b160_28), value);
	}

	inline static int32_t get_offset_of_id_blake2b256_29() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t3188E33E8700951DC1133A1CD98D6C2E889A7721_StaticFields, ___id_blake2b256_29)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_id_blake2b256_29() const { return ___id_blake2b256_29; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_id_blake2b256_29() { return &___id_blake2b256_29; }
	inline void set_id_blake2b256_29(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___id_blake2b256_29 = value;
		Il2CppCodeGenWriteBarrier((&___id_blake2b256_29), value);
	}

	inline static int32_t get_offset_of_id_blake2b384_30() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t3188E33E8700951DC1133A1CD98D6C2E889A7721_StaticFields, ___id_blake2b384_30)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_id_blake2b384_30() const { return ___id_blake2b384_30; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_id_blake2b384_30() { return &___id_blake2b384_30; }
	inline void set_id_blake2b384_30(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___id_blake2b384_30 = value;
		Il2CppCodeGenWriteBarrier((&___id_blake2b384_30), value);
	}

	inline static int32_t get_offset_of_id_blake2b512_31() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t3188E33E8700951DC1133A1CD98D6C2E889A7721_StaticFields, ___id_blake2b512_31)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_id_blake2b512_31() const { return ___id_blake2b512_31; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_id_blake2b512_31() { return &___id_blake2b512_31; }
	inline void set_id_blake2b512_31(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___id_blake2b512_31 = value;
		Il2CppCodeGenWriteBarrier((&___id_blake2b512_31), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MISCOBJECTIDENTIFIERS_T3188E33E8700951DC1133A1CD98D6C2E889A7721_H
#ifndef NISTNAMEDCURVES_T1556B77B6F59D3B9BDCB07D9CD1C51AAE683F3B1_H
#define NISTNAMEDCURVES_T1556B77B6F59D3B9BDCB07D9CD1C51AAE683F3B1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.Nist.NistNamedCurves
struct  NistNamedCurves_t1556B77B6F59D3B9BDCB07D9CD1C51AAE683F3B1  : public RuntimeObject
{
public:

public:
};

struct NistNamedCurves_t1556B77B6F59D3B9BDCB07D9CD1C51AAE683F3B1_StaticFields
{
public:
	// System.Collections.IDictionary Org.BouncyCastle.Asn1.Nist.NistNamedCurves::objIds
	RuntimeObject* ___objIds_0;
	// System.Collections.IDictionary Org.BouncyCastle.Asn1.Nist.NistNamedCurves::names
	RuntimeObject* ___names_1;

public:
	inline static int32_t get_offset_of_objIds_0() { return static_cast<int32_t>(offsetof(NistNamedCurves_t1556B77B6F59D3B9BDCB07D9CD1C51AAE683F3B1_StaticFields, ___objIds_0)); }
	inline RuntimeObject* get_objIds_0() const { return ___objIds_0; }
	inline RuntimeObject** get_address_of_objIds_0() { return &___objIds_0; }
	inline void set_objIds_0(RuntimeObject* value)
	{
		___objIds_0 = value;
		Il2CppCodeGenWriteBarrier((&___objIds_0), value);
	}

	inline static int32_t get_offset_of_names_1() { return static_cast<int32_t>(offsetof(NistNamedCurves_t1556B77B6F59D3B9BDCB07D9CD1C51AAE683F3B1_StaticFields, ___names_1)); }
	inline RuntimeObject* get_names_1() const { return ___names_1; }
	inline RuntimeObject** get_address_of_names_1() { return &___names_1; }
	inline void set_names_1(RuntimeObject* value)
	{
		___names_1 = value;
		Il2CppCodeGenWriteBarrier((&___names_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NISTNAMEDCURVES_T1556B77B6F59D3B9BDCB07D9CD1C51AAE683F3B1_H
#ifndef NISTOBJECTIDENTIFIERS_TC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_H
#define NISTOBJECTIDENTIFIERS_TC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers
struct  NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E  : public RuntimeObject
{
public:

public:
};

struct NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::NistAlgorithm
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___NistAlgorithm_0;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::HashAlgs
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___HashAlgs_1;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdSha256
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdSha256_2;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdSha384
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdSha384_3;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdSha512
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdSha512_4;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdSha224
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdSha224_5;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdSha512_224
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdSha512_224_6;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdSha512_256
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdSha512_256_7;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdSha3_224
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdSha3_224_8;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdSha3_256
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdSha3_256_9;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdSha3_384
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdSha3_384_10;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdSha3_512
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdSha3_512_11;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdShake128
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdShake128_12;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdShake256
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdShake256_13;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::Aes
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___Aes_14;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdAes128Ecb
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdAes128Ecb_15;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdAes128Cbc
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdAes128Cbc_16;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdAes128Ofb
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdAes128Ofb_17;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdAes128Cfb
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdAes128Cfb_18;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdAes128Wrap
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdAes128Wrap_19;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdAes128Gcm
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdAes128Gcm_20;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdAes128Ccm
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdAes128Ccm_21;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdAes192Ecb
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdAes192Ecb_22;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdAes192Cbc
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdAes192Cbc_23;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdAes192Ofb
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdAes192Ofb_24;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdAes192Cfb
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdAes192Cfb_25;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdAes192Wrap
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdAes192Wrap_26;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdAes192Gcm
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdAes192Gcm_27;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdAes192Ccm
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdAes192Ccm_28;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdAes256Ecb
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdAes256Ecb_29;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdAes256Cbc
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdAes256Cbc_30;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdAes256Ofb
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdAes256Ofb_31;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdAes256Cfb
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdAes256Cfb_32;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdAes256Wrap
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdAes256Wrap_33;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdAes256Gcm
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdAes256Gcm_34;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdAes256Ccm
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdAes256Ccm_35;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdDsaWithSha2
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdDsaWithSha2_36;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::DsaWithSha224
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___DsaWithSha224_37;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::DsaWithSha256
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___DsaWithSha256_38;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::DsaWithSha384
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___DsaWithSha384_39;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::DsaWithSha512
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___DsaWithSha512_40;

public:
	inline static int32_t get_offset_of_NistAlgorithm_0() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields, ___NistAlgorithm_0)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_NistAlgorithm_0() const { return ___NistAlgorithm_0; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_NistAlgorithm_0() { return &___NistAlgorithm_0; }
	inline void set_NistAlgorithm_0(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___NistAlgorithm_0 = value;
		Il2CppCodeGenWriteBarrier((&___NistAlgorithm_0), value);
	}

	inline static int32_t get_offset_of_HashAlgs_1() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields, ___HashAlgs_1)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_HashAlgs_1() const { return ___HashAlgs_1; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_HashAlgs_1() { return &___HashAlgs_1; }
	inline void set_HashAlgs_1(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___HashAlgs_1 = value;
		Il2CppCodeGenWriteBarrier((&___HashAlgs_1), value);
	}

	inline static int32_t get_offset_of_IdSha256_2() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields, ___IdSha256_2)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdSha256_2() const { return ___IdSha256_2; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdSha256_2() { return &___IdSha256_2; }
	inline void set_IdSha256_2(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdSha256_2 = value;
		Il2CppCodeGenWriteBarrier((&___IdSha256_2), value);
	}

	inline static int32_t get_offset_of_IdSha384_3() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields, ___IdSha384_3)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdSha384_3() const { return ___IdSha384_3; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdSha384_3() { return &___IdSha384_3; }
	inline void set_IdSha384_3(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdSha384_3 = value;
		Il2CppCodeGenWriteBarrier((&___IdSha384_3), value);
	}

	inline static int32_t get_offset_of_IdSha512_4() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields, ___IdSha512_4)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdSha512_4() const { return ___IdSha512_4; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdSha512_4() { return &___IdSha512_4; }
	inline void set_IdSha512_4(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdSha512_4 = value;
		Il2CppCodeGenWriteBarrier((&___IdSha512_4), value);
	}

	inline static int32_t get_offset_of_IdSha224_5() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields, ___IdSha224_5)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdSha224_5() const { return ___IdSha224_5; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdSha224_5() { return &___IdSha224_5; }
	inline void set_IdSha224_5(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdSha224_5 = value;
		Il2CppCodeGenWriteBarrier((&___IdSha224_5), value);
	}

	inline static int32_t get_offset_of_IdSha512_224_6() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields, ___IdSha512_224_6)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdSha512_224_6() const { return ___IdSha512_224_6; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdSha512_224_6() { return &___IdSha512_224_6; }
	inline void set_IdSha512_224_6(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdSha512_224_6 = value;
		Il2CppCodeGenWriteBarrier((&___IdSha512_224_6), value);
	}

	inline static int32_t get_offset_of_IdSha512_256_7() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields, ___IdSha512_256_7)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdSha512_256_7() const { return ___IdSha512_256_7; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdSha512_256_7() { return &___IdSha512_256_7; }
	inline void set_IdSha512_256_7(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdSha512_256_7 = value;
		Il2CppCodeGenWriteBarrier((&___IdSha512_256_7), value);
	}

	inline static int32_t get_offset_of_IdSha3_224_8() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields, ___IdSha3_224_8)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdSha3_224_8() const { return ___IdSha3_224_8; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdSha3_224_8() { return &___IdSha3_224_8; }
	inline void set_IdSha3_224_8(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdSha3_224_8 = value;
		Il2CppCodeGenWriteBarrier((&___IdSha3_224_8), value);
	}

	inline static int32_t get_offset_of_IdSha3_256_9() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields, ___IdSha3_256_9)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdSha3_256_9() const { return ___IdSha3_256_9; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdSha3_256_9() { return &___IdSha3_256_9; }
	inline void set_IdSha3_256_9(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdSha3_256_9 = value;
		Il2CppCodeGenWriteBarrier((&___IdSha3_256_9), value);
	}

	inline static int32_t get_offset_of_IdSha3_384_10() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields, ___IdSha3_384_10)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdSha3_384_10() const { return ___IdSha3_384_10; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdSha3_384_10() { return &___IdSha3_384_10; }
	inline void set_IdSha3_384_10(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdSha3_384_10 = value;
		Il2CppCodeGenWriteBarrier((&___IdSha3_384_10), value);
	}

	inline static int32_t get_offset_of_IdSha3_512_11() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields, ___IdSha3_512_11)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdSha3_512_11() const { return ___IdSha3_512_11; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdSha3_512_11() { return &___IdSha3_512_11; }
	inline void set_IdSha3_512_11(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdSha3_512_11 = value;
		Il2CppCodeGenWriteBarrier((&___IdSha3_512_11), value);
	}

	inline static int32_t get_offset_of_IdShake128_12() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields, ___IdShake128_12)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdShake128_12() const { return ___IdShake128_12; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdShake128_12() { return &___IdShake128_12; }
	inline void set_IdShake128_12(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdShake128_12 = value;
		Il2CppCodeGenWriteBarrier((&___IdShake128_12), value);
	}

	inline static int32_t get_offset_of_IdShake256_13() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields, ___IdShake256_13)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdShake256_13() const { return ___IdShake256_13; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdShake256_13() { return &___IdShake256_13; }
	inline void set_IdShake256_13(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdShake256_13 = value;
		Il2CppCodeGenWriteBarrier((&___IdShake256_13), value);
	}

	inline static int32_t get_offset_of_Aes_14() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields, ___Aes_14)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_Aes_14() const { return ___Aes_14; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_Aes_14() { return &___Aes_14; }
	inline void set_Aes_14(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___Aes_14 = value;
		Il2CppCodeGenWriteBarrier((&___Aes_14), value);
	}

	inline static int32_t get_offset_of_IdAes128Ecb_15() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields, ___IdAes128Ecb_15)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdAes128Ecb_15() const { return ___IdAes128Ecb_15; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdAes128Ecb_15() { return &___IdAes128Ecb_15; }
	inline void set_IdAes128Ecb_15(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdAes128Ecb_15 = value;
		Il2CppCodeGenWriteBarrier((&___IdAes128Ecb_15), value);
	}

	inline static int32_t get_offset_of_IdAes128Cbc_16() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields, ___IdAes128Cbc_16)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdAes128Cbc_16() const { return ___IdAes128Cbc_16; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdAes128Cbc_16() { return &___IdAes128Cbc_16; }
	inline void set_IdAes128Cbc_16(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdAes128Cbc_16 = value;
		Il2CppCodeGenWriteBarrier((&___IdAes128Cbc_16), value);
	}

	inline static int32_t get_offset_of_IdAes128Ofb_17() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields, ___IdAes128Ofb_17)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdAes128Ofb_17() const { return ___IdAes128Ofb_17; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdAes128Ofb_17() { return &___IdAes128Ofb_17; }
	inline void set_IdAes128Ofb_17(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdAes128Ofb_17 = value;
		Il2CppCodeGenWriteBarrier((&___IdAes128Ofb_17), value);
	}

	inline static int32_t get_offset_of_IdAes128Cfb_18() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields, ___IdAes128Cfb_18)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdAes128Cfb_18() const { return ___IdAes128Cfb_18; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdAes128Cfb_18() { return &___IdAes128Cfb_18; }
	inline void set_IdAes128Cfb_18(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdAes128Cfb_18 = value;
		Il2CppCodeGenWriteBarrier((&___IdAes128Cfb_18), value);
	}

	inline static int32_t get_offset_of_IdAes128Wrap_19() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields, ___IdAes128Wrap_19)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdAes128Wrap_19() const { return ___IdAes128Wrap_19; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdAes128Wrap_19() { return &___IdAes128Wrap_19; }
	inline void set_IdAes128Wrap_19(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdAes128Wrap_19 = value;
		Il2CppCodeGenWriteBarrier((&___IdAes128Wrap_19), value);
	}

	inline static int32_t get_offset_of_IdAes128Gcm_20() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields, ___IdAes128Gcm_20)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdAes128Gcm_20() const { return ___IdAes128Gcm_20; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdAes128Gcm_20() { return &___IdAes128Gcm_20; }
	inline void set_IdAes128Gcm_20(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdAes128Gcm_20 = value;
		Il2CppCodeGenWriteBarrier((&___IdAes128Gcm_20), value);
	}

	inline static int32_t get_offset_of_IdAes128Ccm_21() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields, ___IdAes128Ccm_21)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdAes128Ccm_21() const { return ___IdAes128Ccm_21; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdAes128Ccm_21() { return &___IdAes128Ccm_21; }
	inline void set_IdAes128Ccm_21(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdAes128Ccm_21 = value;
		Il2CppCodeGenWriteBarrier((&___IdAes128Ccm_21), value);
	}

	inline static int32_t get_offset_of_IdAes192Ecb_22() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields, ___IdAes192Ecb_22)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdAes192Ecb_22() const { return ___IdAes192Ecb_22; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdAes192Ecb_22() { return &___IdAes192Ecb_22; }
	inline void set_IdAes192Ecb_22(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdAes192Ecb_22 = value;
		Il2CppCodeGenWriteBarrier((&___IdAes192Ecb_22), value);
	}

	inline static int32_t get_offset_of_IdAes192Cbc_23() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields, ___IdAes192Cbc_23)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdAes192Cbc_23() const { return ___IdAes192Cbc_23; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdAes192Cbc_23() { return &___IdAes192Cbc_23; }
	inline void set_IdAes192Cbc_23(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdAes192Cbc_23 = value;
		Il2CppCodeGenWriteBarrier((&___IdAes192Cbc_23), value);
	}

	inline static int32_t get_offset_of_IdAes192Ofb_24() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields, ___IdAes192Ofb_24)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdAes192Ofb_24() const { return ___IdAes192Ofb_24; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdAes192Ofb_24() { return &___IdAes192Ofb_24; }
	inline void set_IdAes192Ofb_24(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdAes192Ofb_24 = value;
		Il2CppCodeGenWriteBarrier((&___IdAes192Ofb_24), value);
	}

	inline static int32_t get_offset_of_IdAes192Cfb_25() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields, ___IdAes192Cfb_25)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdAes192Cfb_25() const { return ___IdAes192Cfb_25; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdAes192Cfb_25() { return &___IdAes192Cfb_25; }
	inline void set_IdAes192Cfb_25(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdAes192Cfb_25 = value;
		Il2CppCodeGenWriteBarrier((&___IdAes192Cfb_25), value);
	}

	inline static int32_t get_offset_of_IdAes192Wrap_26() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields, ___IdAes192Wrap_26)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdAes192Wrap_26() const { return ___IdAes192Wrap_26; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdAes192Wrap_26() { return &___IdAes192Wrap_26; }
	inline void set_IdAes192Wrap_26(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdAes192Wrap_26 = value;
		Il2CppCodeGenWriteBarrier((&___IdAes192Wrap_26), value);
	}

	inline static int32_t get_offset_of_IdAes192Gcm_27() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields, ___IdAes192Gcm_27)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdAes192Gcm_27() const { return ___IdAes192Gcm_27; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdAes192Gcm_27() { return &___IdAes192Gcm_27; }
	inline void set_IdAes192Gcm_27(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdAes192Gcm_27 = value;
		Il2CppCodeGenWriteBarrier((&___IdAes192Gcm_27), value);
	}

	inline static int32_t get_offset_of_IdAes192Ccm_28() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields, ___IdAes192Ccm_28)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdAes192Ccm_28() const { return ___IdAes192Ccm_28; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdAes192Ccm_28() { return &___IdAes192Ccm_28; }
	inline void set_IdAes192Ccm_28(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdAes192Ccm_28 = value;
		Il2CppCodeGenWriteBarrier((&___IdAes192Ccm_28), value);
	}

	inline static int32_t get_offset_of_IdAes256Ecb_29() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields, ___IdAes256Ecb_29)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdAes256Ecb_29() const { return ___IdAes256Ecb_29; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdAes256Ecb_29() { return &___IdAes256Ecb_29; }
	inline void set_IdAes256Ecb_29(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdAes256Ecb_29 = value;
		Il2CppCodeGenWriteBarrier((&___IdAes256Ecb_29), value);
	}

	inline static int32_t get_offset_of_IdAes256Cbc_30() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields, ___IdAes256Cbc_30)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdAes256Cbc_30() const { return ___IdAes256Cbc_30; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdAes256Cbc_30() { return &___IdAes256Cbc_30; }
	inline void set_IdAes256Cbc_30(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdAes256Cbc_30 = value;
		Il2CppCodeGenWriteBarrier((&___IdAes256Cbc_30), value);
	}

	inline static int32_t get_offset_of_IdAes256Ofb_31() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields, ___IdAes256Ofb_31)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdAes256Ofb_31() const { return ___IdAes256Ofb_31; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdAes256Ofb_31() { return &___IdAes256Ofb_31; }
	inline void set_IdAes256Ofb_31(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdAes256Ofb_31 = value;
		Il2CppCodeGenWriteBarrier((&___IdAes256Ofb_31), value);
	}

	inline static int32_t get_offset_of_IdAes256Cfb_32() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields, ___IdAes256Cfb_32)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdAes256Cfb_32() const { return ___IdAes256Cfb_32; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdAes256Cfb_32() { return &___IdAes256Cfb_32; }
	inline void set_IdAes256Cfb_32(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdAes256Cfb_32 = value;
		Il2CppCodeGenWriteBarrier((&___IdAes256Cfb_32), value);
	}

	inline static int32_t get_offset_of_IdAes256Wrap_33() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields, ___IdAes256Wrap_33)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdAes256Wrap_33() const { return ___IdAes256Wrap_33; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdAes256Wrap_33() { return &___IdAes256Wrap_33; }
	inline void set_IdAes256Wrap_33(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdAes256Wrap_33 = value;
		Il2CppCodeGenWriteBarrier((&___IdAes256Wrap_33), value);
	}

	inline static int32_t get_offset_of_IdAes256Gcm_34() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields, ___IdAes256Gcm_34)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdAes256Gcm_34() const { return ___IdAes256Gcm_34; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdAes256Gcm_34() { return &___IdAes256Gcm_34; }
	inline void set_IdAes256Gcm_34(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdAes256Gcm_34 = value;
		Il2CppCodeGenWriteBarrier((&___IdAes256Gcm_34), value);
	}

	inline static int32_t get_offset_of_IdAes256Ccm_35() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields, ___IdAes256Ccm_35)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdAes256Ccm_35() const { return ___IdAes256Ccm_35; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdAes256Ccm_35() { return &___IdAes256Ccm_35; }
	inline void set_IdAes256Ccm_35(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdAes256Ccm_35 = value;
		Il2CppCodeGenWriteBarrier((&___IdAes256Ccm_35), value);
	}

	inline static int32_t get_offset_of_IdDsaWithSha2_36() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields, ___IdDsaWithSha2_36)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdDsaWithSha2_36() const { return ___IdDsaWithSha2_36; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdDsaWithSha2_36() { return &___IdDsaWithSha2_36; }
	inline void set_IdDsaWithSha2_36(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdDsaWithSha2_36 = value;
		Il2CppCodeGenWriteBarrier((&___IdDsaWithSha2_36), value);
	}

	inline static int32_t get_offset_of_DsaWithSha224_37() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields, ___DsaWithSha224_37)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_DsaWithSha224_37() const { return ___DsaWithSha224_37; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_DsaWithSha224_37() { return &___DsaWithSha224_37; }
	inline void set_DsaWithSha224_37(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___DsaWithSha224_37 = value;
		Il2CppCodeGenWriteBarrier((&___DsaWithSha224_37), value);
	}

	inline static int32_t get_offset_of_DsaWithSha256_38() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields, ___DsaWithSha256_38)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_DsaWithSha256_38() const { return ___DsaWithSha256_38; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_DsaWithSha256_38() { return &___DsaWithSha256_38; }
	inline void set_DsaWithSha256_38(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___DsaWithSha256_38 = value;
		Il2CppCodeGenWriteBarrier((&___DsaWithSha256_38), value);
	}

	inline static int32_t get_offset_of_DsaWithSha384_39() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields, ___DsaWithSha384_39)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_DsaWithSha384_39() const { return ___DsaWithSha384_39; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_DsaWithSha384_39() { return &___DsaWithSha384_39; }
	inline void set_DsaWithSha384_39(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___DsaWithSha384_39 = value;
		Il2CppCodeGenWriteBarrier((&___DsaWithSha384_39), value);
	}

	inline static int32_t get_offset_of_DsaWithSha512_40() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields, ___DsaWithSha512_40)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_DsaWithSha512_40() const { return ___DsaWithSha512_40; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_DsaWithSha512_40() { return &___DsaWithSha512_40; }
	inline void set_DsaWithSha512_40(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___DsaWithSha512_40 = value;
		Il2CppCodeGenWriteBarrier((&___DsaWithSha512_40), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NISTOBJECTIDENTIFIERS_TC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_H
#ifndef OIDTOKENIZER_T03CBB940F9BDDC36B196B95EC928E4FFBD15DC3A_H
#define OIDTOKENIZER_T03CBB940F9BDDC36B196B95EC928E4FFBD15DC3A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.OidTokenizer
struct  OidTokenizer_t03CBB940F9BDDC36B196B95EC928E4FFBD15DC3A  : public RuntimeObject
{
public:
	// System.String Org.BouncyCastle.Asn1.OidTokenizer::oid
	String_t* ___oid_0;
	// System.Int32 Org.BouncyCastle.Asn1.OidTokenizer::index
	int32_t ___index_1;

public:
	inline static int32_t get_offset_of_oid_0() { return static_cast<int32_t>(offsetof(OidTokenizer_t03CBB940F9BDDC36B196B95EC928E4FFBD15DC3A, ___oid_0)); }
	inline String_t* get_oid_0() const { return ___oid_0; }
	inline String_t** get_address_of_oid_0() { return &___oid_0; }
	inline void set_oid_0(String_t* value)
	{
		___oid_0 = value;
		Il2CppCodeGenWriteBarrier((&___oid_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(OidTokenizer_t03CBB940F9BDDC36B196B95EC928E4FFBD15DC3A, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OIDTOKENIZER_T03CBB940F9BDDC36B196B95EC928E4FFBD15DC3A_H
#ifndef X9ECPARAMETERSHOLDER_TB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE_H
#define X9ECPARAMETERSHOLDER_TB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder
struct  X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE  : public RuntimeObject
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParameters Org.BouncyCastle.Asn1.X9.X9ECParametersHolder::parameters
	X9ECParameters_t818A7DD4D13D91F0D36622C6C117D7E9A10E51C3 * ___parameters_0;

public:
	inline static int32_t get_offset_of_parameters_0() { return static_cast<int32_t>(offsetof(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE, ___parameters_0)); }
	inline X9ECParameters_t818A7DD4D13D91F0D36622C6C117D7E9A10E51C3 * get_parameters_0() const { return ___parameters_0; }
	inline X9ECParameters_t818A7DD4D13D91F0D36622C6C117D7E9A10E51C3 ** get_address_of_parameters_0() { return &___parameters_0; }
	inline void set_parameters_0(X9ECParameters_t818A7DD4D13D91F0D36622C6C117D7E9A10E51C3 * value)
	{
		___parameters_0 = value;
		Il2CppCodeGenWriteBarrier((&___parameters_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X9ECPARAMETERSHOLDER_TB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#define MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MarshalByRefObject
struct  MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF  : public RuntimeObject
{
public:
	// System.Object System.MarshalByRefObject::_identity
	RuntimeObject * ____identity_0;

public:
	inline static int32_t get_offset_of__identity_0() { return static_cast<int32_t>(offsetof(MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF, ____identity_0)); }
	inline RuntimeObject * get__identity_0() const { return ____identity_0; }
	inline RuntimeObject ** get_address_of__identity_0() { return &____identity_0; }
	inline void set__identity_0(RuntimeObject * value)
	{
		____identity_0 = value;
		Il2CppCodeGenWriteBarrier((&____identity_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF_marshaled_pinvoke
{
	Il2CppIUnknown* ____identity_0;
};
// Native definition for COM marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF_marshaled_com
{
	Il2CppIUnknown* ____identity_0;
};
#endif // MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef FRP256V1HOLDER_T9655688B104110C311CCA3E65F1498CD2FEB2EA3_H
#define FRP256V1HOLDER_T9655688B104110C311CCA3E65F1498CD2FEB2EA3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.Anssi.AnssiNamedCurves_Frp256v1Holder
struct  Frp256v1Holder_t9655688B104110C311CCA3E65F1498CD2FEB2EA3  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct Frp256v1Holder_t9655688B104110C311CCA3E65F1498CD2FEB2EA3_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Asn1.Anssi.AnssiNamedCurves_Frp256v1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Frp256v1Holder_t9655688B104110C311CCA3E65F1498CD2FEB2EA3_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FRP256V1HOLDER_T9655688B104110C311CCA3E65F1498CD2FEB2EA3_H
#ifndef ASN1OBJECT_TB780E50883209D5C975E83B3D16257DFC3CCE36D_H
#define ASN1OBJECT_TB780E50883209D5C975E83B3D16257DFC3CCE36D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.Asn1Object
struct  Asn1Object_tB780E50883209D5C975E83B3D16257DFC3CCE36D  : public Asn1Encodable_t390FCE82F5F52E41A7320BBF2E8BF18DA1622A8B
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASN1OBJECT_TB780E50883209D5C975E83B3D16257DFC3CCE36D_H
#ifndef GOST3410PARAMSETPARAMETERS_TA09B7D79E4C03898F7D5F3AB8C9A8A061B69E6C2_H
#define GOST3410PARAMSETPARAMETERS_TA09B7D79E4C03898F7D5F3AB8C9A8A061B69E6C2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.CryptoPro.Gost3410ParamSetParameters
struct  Gost3410ParamSetParameters_tA09B7D79E4C03898F7D5F3AB8C9A8A061B69E6C2  : public Asn1Encodable_t390FCE82F5F52E41A7320BBF2E8BF18DA1622A8B
{
public:
	// System.Int32 Org.BouncyCastle.Asn1.CryptoPro.Gost3410ParamSetParameters::keySize
	int32_t ___keySize_0;
	// Org.BouncyCastle.Asn1.DerInteger Org.BouncyCastle.Asn1.CryptoPro.Gost3410ParamSetParameters::p
	DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A * ___p_1;
	// Org.BouncyCastle.Asn1.DerInteger Org.BouncyCastle.Asn1.CryptoPro.Gost3410ParamSetParameters::q
	DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A * ___q_2;
	// Org.BouncyCastle.Asn1.DerInteger Org.BouncyCastle.Asn1.CryptoPro.Gost3410ParamSetParameters::a
	DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A * ___a_3;

public:
	inline static int32_t get_offset_of_keySize_0() { return static_cast<int32_t>(offsetof(Gost3410ParamSetParameters_tA09B7D79E4C03898F7D5F3AB8C9A8A061B69E6C2, ___keySize_0)); }
	inline int32_t get_keySize_0() const { return ___keySize_0; }
	inline int32_t* get_address_of_keySize_0() { return &___keySize_0; }
	inline void set_keySize_0(int32_t value)
	{
		___keySize_0 = value;
	}

	inline static int32_t get_offset_of_p_1() { return static_cast<int32_t>(offsetof(Gost3410ParamSetParameters_tA09B7D79E4C03898F7D5F3AB8C9A8A061B69E6C2, ___p_1)); }
	inline DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A * get_p_1() const { return ___p_1; }
	inline DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A ** get_address_of_p_1() { return &___p_1; }
	inline void set_p_1(DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A * value)
	{
		___p_1 = value;
		Il2CppCodeGenWriteBarrier((&___p_1), value);
	}

	inline static int32_t get_offset_of_q_2() { return static_cast<int32_t>(offsetof(Gost3410ParamSetParameters_tA09B7D79E4C03898F7D5F3AB8C9A8A061B69E6C2, ___q_2)); }
	inline DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A * get_q_2() const { return ___q_2; }
	inline DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A ** get_address_of_q_2() { return &___q_2; }
	inline void set_q_2(DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A * value)
	{
		___q_2 = value;
		Il2CppCodeGenWriteBarrier((&___q_2), value);
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Gost3410ParamSetParameters_tA09B7D79E4C03898F7D5F3AB8C9A8A061B69E6C2, ___a_3)); }
	inline DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A * get_a_3() const { return ___a_3; }
	inline DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A ** get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A * value)
	{
		___a_3 = value;
		Il2CppCodeGenWriteBarrier((&___a_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GOST3410PARAMSETPARAMETERS_TA09B7D79E4C03898F7D5F3AB8C9A8A061B69E6C2_H
#ifndef GOST3410PUBLICKEYALGPARAMETERS_TBE2739ED2E3FF2B3DF0D7C073753194550175243_H
#define GOST3410PUBLICKEYALGPARAMETERS_TBE2739ED2E3FF2B3DF0D7C073753194550175243_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.CryptoPro.Gost3410PublicKeyAlgParameters
struct  Gost3410PublicKeyAlgParameters_tBE2739ED2E3FF2B3DF0D7C073753194550175243  : public Asn1Encodable_t390FCE82F5F52E41A7320BBF2E8BF18DA1622A8B
{
public:
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.CryptoPro.Gost3410PublicKeyAlgParameters::publicKeyParamSet
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___publicKeyParamSet_0;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.CryptoPro.Gost3410PublicKeyAlgParameters::digestParamSet
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___digestParamSet_1;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.CryptoPro.Gost3410PublicKeyAlgParameters::encryptionParamSet
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___encryptionParamSet_2;

public:
	inline static int32_t get_offset_of_publicKeyParamSet_0() { return static_cast<int32_t>(offsetof(Gost3410PublicKeyAlgParameters_tBE2739ED2E3FF2B3DF0D7C073753194550175243, ___publicKeyParamSet_0)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_publicKeyParamSet_0() const { return ___publicKeyParamSet_0; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_publicKeyParamSet_0() { return &___publicKeyParamSet_0; }
	inline void set_publicKeyParamSet_0(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___publicKeyParamSet_0 = value;
		Il2CppCodeGenWriteBarrier((&___publicKeyParamSet_0), value);
	}

	inline static int32_t get_offset_of_digestParamSet_1() { return static_cast<int32_t>(offsetof(Gost3410PublicKeyAlgParameters_tBE2739ED2E3FF2B3DF0D7C073753194550175243, ___digestParamSet_1)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_digestParamSet_1() const { return ___digestParamSet_1; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_digestParamSet_1() { return &___digestParamSet_1; }
	inline void set_digestParamSet_1(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___digestParamSet_1 = value;
		Il2CppCodeGenWriteBarrier((&___digestParamSet_1), value);
	}

	inline static int32_t get_offset_of_encryptionParamSet_2() { return static_cast<int32_t>(offsetof(Gost3410PublicKeyAlgParameters_tBE2739ED2E3FF2B3DF0D7C073753194550175243, ___encryptionParamSet_2)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_encryptionParamSet_2() const { return ___encryptionParamSet_2; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_encryptionParamSet_2() { return &___encryptionParamSet_2; }
	inline void set_encryptionParamSet_2(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___encryptionParamSet_2 = value;
		Il2CppCodeGenWriteBarrier((&___encryptionParamSet_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GOST3410PUBLICKEYALGPARAMETERS_TBE2739ED2E3FF2B3DF0D7C073753194550175243_H
#ifndef DEREXTERNALPARSER_TE05E2163A7E760BAFC8F526300708CB7B931653C_H
#define DEREXTERNALPARSER_TE05E2163A7E760BAFC8F526300708CB7B931653C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.DerExternalParser
struct  DerExternalParser_tE05E2163A7E760BAFC8F526300708CB7B931653C  : public Asn1Encodable_t390FCE82F5F52E41A7320BBF2E8BF18DA1622A8B
{
public:
	// Org.BouncyCastle.Asn1.Asn1StreamParser Org.BouncyCastle.Asn1.DerExternalParser::_parser
	Asn1StreamParser_t2FE3FB89834CCEE990CDD0F38B5DD0960CDC9C90 * ____parser_0;

public:
	inline static int32_t get_offset_of__parser_0() { return static_cast<int32_t>(offsetof(DerExternalParser_tE05E2163A7E760BAFC8F526300708CB7B931653C, ____parser_0)); }
	inline Asn1StreamParser_t2FE3FB89834CCEE990CDD0F38B5DD0960CDC9C90 * get__parser_0() const { return ____parser_0; }
	inline Asn1StreamParser_t2FE3FB89834CCEE990CDD0F38B5DD0960CDC9C90 ** get_address_of__parser_0() { return &____parser_0; }
	inline void set__parser_0(Asn1StreamParser_t2FE3FB89834CCEE990CDD0F38B5DD0960CDC9C90 * value)
	{
		____parser_0 = value;
		Il2CppCodeGenWriteBarrier((&____parser_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEREXTERNALPARSER_TE05E2163A7E760BAFC8F526300708CB7B931653C_H
#ifndef DOUBLE_T358B8F23BDC52A5DD700E727E204F9F7CDE12409_H
#define DOUBLE_T358B8F23BDC52A5DD700E727E204F9F7CDE12409_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Double
struct  Double_t358B8F23BDC52A5DD700E727E204F9F7CDE12409 
{
public:
	// System.Double System.Double::m_value
	double ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Double_t358B8F23BDC52A5DD700E727E204F9F7CDE12409, ___m_value_0)); }
	inline double get_m_value_0() const { return ___m_value_0; }
	inline double* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(double value)
	{
		___m_value_0 = value;
	}
};

struct Double_t358B8F23BDC52A5DD700E727E204F9F7CDE12409_StaticFields
{
public:
	// System.Double System.Double::NegativeZero
	double ___NegativeZero_7;

public:
	inline static int32_t get_offset_of_NegativeZero_7() { return static_cast<int32_t>(offsetof(Double_t358B8F23BDC52A5DD700E727E204F9F7CDE12409_StaticFields, ___NegativeZero_7)); }
	inline double get_NegativeZero_7() const { return ___NegativeZero_7; }
	inline double* get_address_of_NegativeZero_7() { return &___NegativeZero_7; }
	inline void set_NegativeZero_7(double value)
	{
		___NegativeZero_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLE_T358B8F23BDC52A5DD700E727E204F9F7CDE12409_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#define STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Stream
struct  Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7  : public MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF
{
public:
	// System.IO.Stream_ReadWriteTask System.IO.Stream::_activeReadWriteTask
	ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * ____activeReadWriteTask_3;
	// System.Threading.SemaphoreSlim System.IO.Stream::_asyncActiveSemaphore
	SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * ____asyncActiveSemaphore_4;

public:
	inline static int32_t get_offset_of__activeReadWriteTask_3() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7, ____activeReadWriteTask_3)); }
	inline ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * get__activeReadWriteTask_3() const { return ____activeReadWriteTask_3; }
	inline ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 ** get_address_of__activeReadWriteTask_3() { return &____activeReadWriteTask_3; }
	inline void set__activeReadWriteTask_3(ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * value)
	{
		____activeReadWriteTask_3 = value;
		Il2CppCodeGenWriteBarrier((&____activeReadWriteTask_3), value);
	}

	inline static int32_t get_offset_of__asyncActiveSemaphore_4() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7, ____asyncActiveSemaphore_4)); }
	inline SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * get__asyncActiveSemaphore_4() const { return ____asyncActiveSemaphore_4; }
	inline SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 ** get_address_of__asyncActiveSemaphore_4() { return &____asyncActiveSemaphore_4; }
	inline void set__asyncActiveSemaphore_4(SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * value)
	{
		____asyncActiveSemaphore_4 = value;
		Il2CppCodeGenWriteBarrier((&____asyncActiveSemaphore_4), value);
	}
};

struct Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7_StaticFields
{
public:
	// System.IO.Stream System.IO.Stream::Null
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___Null_1;

public:
	inline static int32_t get_offset_of_Null_1() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7_StaticFields, ___Null_1)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_Null_1() const { return ___Null_1; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_Null_1() { return &___Null_1; }
	inline void set_Null_1(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___Null_1 = value;
		Il2CppCodeGenWriteBarrier((&___Null_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#ifndef INT64_T7A386C2FF7B0280A0F516992401DDFCF0FF7B436_H
#define INT64_T7A386C2FF7B0280A0F516992401DDFCF0FF7B436_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int64
struct  Int64_t7A386C2FF7B0280A0F516992401DDFCF0FF7B436 
{
public:
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int64_t7A386C2FF7B0280A0F516992401DDFCF0FF7B436, ___m_value_0)); }
	inline int64_t get_m_value_0() const { return ___m_value_0; }
	inline int64_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int64_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT64_T7A386C2FF7B0280A0F516992401DDFCF0FF7B436_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#define SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef ASN1NULL_T9AC6DDB8C5501D354D5550F4B7674F94B3411DF7_H
#define ASN1NULL_T9AC6DDB8C5501D354D5550F4B7674F94B3411DF7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.Asn1Null
struct  Asn1Null_t9AC6DDB8C5501D354D5550F4B7674F94B3411DF7  : public Asn1Object_tB780E50883209D5C975E83B3D16257DFC3CCE36D
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASN1NULL_T9AC6DDB8C5501D354D5550F4B7674F94B3411DF7_H
#ifndef ASN1OCTETSTRING_T2088F9E88E4C97BB9CF3B2FFC53CFBAF9B8B79CB_H
#define ASN1OCTETSTRING_T2088F9E88E4C97BB9CF3B2FFC53CFBAF9B8B79CB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.Asn1OctetString
struct  Asn1OctetString_t2088F9E88E4C97BB9CF3B2FFC53CFBAF9B8B79CB  : public Asn1Object_tB780E50883209D5C975E83B3D16257DFC3CCE36D
{
public:
	// System.Byte[] Org.BouncyCastle.Asn1.Asn1OctetString::str
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___str_0;

public:
	inline static int32_t get_offset_of_str_0() { return static_cast<int32_t>(offsetof(Asn1OctetString_t2088F9E88E4C97BB9CF3B2FFC53CFBAF9B8B79CB, ___str_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_str_0() const { return ___str_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_str_0() { return &___str_0; }
	inline void set_str_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___str_0 = value;
		Il2CppCodeGenWriteBarrier((&___str_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASN1OCTETSTRING_T2088F9E88E4C97BB9CF3B2FFC53CFBAF9B8B79CB_H
#ifndef ASN1SEQUENCE_T292AE1C416511A0DE153DE285AEE498B50523B85_H
#define ASN1SEQUENCE_T292AE1C416511A0DE153DE285AEE498B50523B85_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.Asn1Sequence
struct  Asn1Sequence_t292AE1C416511A0DE153DE285AEE498B50523B85  : public Asn1Object_tB780E50883209D5C975E83B3D16257DFC3CCE36D
{
public:
	// System.Collections.IList Org.BouncyCastle.Asn1.Asn1Sequence::seq
	RuntimeObject* ___seq_0;

public:
	inline static int32_t get_offset_of_seq_0() { return static_cast<int32_t>(offsetof(Asn1Sequence_t292AE1C416511A0DE153DE285AEE498B50523B85, ___seq_0)); }
	inline RuntimeObject* get_seq_0() const { return ___seq_0; }
	inline RuntimeObject** get_address_of_seq_0() { return &___seq_0; }
	inline void set_seq_0(RuntimeObject* value)
	{
		___seq_0 = value;
		Il2CppCodeGenWriteBarrier((&___seq_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASN1SEQUENCE_T292AE1C416511A0DE153DE285AEE498B50523B85_H
#ifndef ASN1SET_TD51325409687C84A095F7692B251BDE783A1A4A7_H
#define ASN1SET_TD51325409687C84A095F7692B251BDE783A1A4A7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.Asn1Set
struct  Asn1Set_tD51325409687C84A095F7692B251BDE783A1A4A7  : public Asn1Object_tB780E50883209D5C975E83B3D16257DFC3CCE36D
{
public:
	// System.Collections.IList Org.BouncyCastle.Asn1.Asn1Set::_set
	RuntimeObject* ____set_0;

public:
	inline static int32_t get_offset_of__set_0() { return static_cast<int32_t>(offsetof(Asn1Set_tD51325409687C84A095F7692B251BDE783A1A4A7, ____set_0)); }
	inline RuntimeObject* get__set_0() const { return ____set_0; }
	inline RuntimeObject** get_address_of__set_0() { return &____set_0; }
	inline void set__set_0(RuntimeObject* value)
	{
		____set_0 = value;
		Il2CppCodeGenWriteBarrier((&____set_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASN1SET_TD51325409687C84A095F7692B251BDE783A1A4A7_H
#ifndef ASN1TAGGEDOBJECT_T14E9E19978062973ABA220F78322FDDCEFC39B57_H
#define ASN1TAGGEDOBJECT_T14E9E19978062973ABA220F78322FDDCEFC39B57_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.Asn1TaggedObject
struct  Asn1TaggedObject_t14E9E19978062973ABA220F78322FDDCEFC39B57  : public Asn1Object_tB780E50883209D5C975E83B3D16257DFC3CCE36D
{
public:
	// System.Int32 Org.BouncyCastle.Asn1.Asn1TaggedObject::tagNo
	int32_t ___tagNo_0;
	// System.Boolean Org.BouncyCastle.Asn1.Asn1TaggedObject::explicitly
	bool ___explicitly_1;
	// Org.BouncyCastle.Asn1.Asn1Encodable Org.BouncyCastle.Asn1.Asn1TaggedObject::obj
	Asn1Encodable_t390FCE82F5F52E41A7320BBF2E8BF18DA1622A8B * ___obj_2;

public:
	inline static int32_t get_offset_of_tagNo_0() { return static_cast<int32_t>(offsetof(Asn1TaggedObject_t14E9E19978062973ABA220F78322FDDCEFC39B57, ___tagNo_0)); }
	inline int32_t get_tagNo_0() const { return ___tagNo_0; }
	inline int32_t* get_address_of_tagNo_0() { return &___tagNo_0; }
	inline void set_tagNo_0(int32_t value)
	{
		___tagNo_0 = value;
	}

	inline static int32_t get_offset_of_explicitly_1() { return static_cast<int32_t>(offsetof(Asn1TaggedObject_t14E9E19978062973ABA220F78322FDDCEFC39B57, ___explicitly_1)); }
	inline bool get_explicitly_1() const { return ___explicitly_1; }
	inline bool* get_address_of_explicitly_1() { return &___explicitly_1; }
	inline void set_explicitly_1(bool value)
	{
		___explicitly_1 = value;
	}

	inline static int32_t get_offset_of_obj_2() { return static_cast<int32_t>(offsetof(Asn1TaggedObject_t14E9E19978062973ABA220F78322FDDCEFC39B57, ___obj_2)); }
	inline Asn1Encodable_t390FCE82F5F52E41A7320BBF2E8BF18DA1622A8B * get_obj_2() const { return ___obj_2; }
	inline Asn1Encodable_t390FCE82F5F52E41A7320BBF2E8BF18DA1622A8B ** get_address_of_obj_2() { return &___obj_2; }
	inline void set_obj_2(Asn1Encodable_t390FCE82F5F52E41A7320BBF2E8BF18DA1622A8B * value)
	{
		___obj_2 = value;
		Il2CppCodeGenWriteBarrier((&___obj_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASN1TAGGEDOBJECT_T14E9E19978062973ABA220F78322FDDCEFC39B57_H
#ifndef DERAPPLICATIONSPECIFIC_T172FBAA17F6C55C0E6B5999D6AB82A8773B75DEA_H
#define DERAPPLICATIONSPECIFIC_T172FBAA17F6C55C0E6B5999D6AB82A8773B75DEA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.DerApplicationSpecific
struct  DerApplicationSpecific_t172FBAA17F6C55C0E6B5999D6AB82A8773B75DEA  : public Asn1Object_tB780E50883209D5C975E83B3D16257DFC3CCE36D
{
public:
	// System.Boolean Org.BouncyCastle.Asn1.DerApplicationSpecific::isConstructed
	bool ___isConstructed_0;
	// System.Int32 Org.BouncyCastle.Asn1.DerApplicationSpecific::tag
	int32_t ___tag_1;
	// System.Byte[] Org.BouncyCastle.Asn1.DerApplicationSpecific::octets
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___octets_2;

public:
	inline static int32_t get_offset_of_isConstructed_0() { return static_cast<int32_t>(offsetof(DerApplicationSpecific_t172FBAA17F6C55C0E6B5999D6AB82A8773B75DEA, ___isConstructed_0)); }
	inline bool get_isConstructed_0() const { return ___isConstructed_0; }
	inline bool* get_address_of_isConstructed_0() { return &___isConstructed_0; }
	inline void set_isConstructed_0(bool value)
	{
		___isConstructed_0 = value;
	}

	inline static int32_t get_offset_of_tag_1() { return static_cast<int32_t>(offsetof(DerApplicationSpecific_t172FBAA17F6C55C0E6B5999D6AB82A8773B75DEA, ___tag_1)); }
	inline int32_t get_tag_1() const { return ___tag_1; }
	inline int32_t* get_address_of_tag_1() { return &___tag_1; }
	inline void set_tag_1(int32_t value)
	{
		___tag_1 = value;
	}

	inline static int32_t get_offset_of_octets_2() { return static_cast<int32_t>(offsetof(DerApplicationSpecific_t172FBAA17F6C55C0E6B5999D6AB82A8773B75DEA, ___octets_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_octets_2() const { return ___octets_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_octets_2() { return &___octets_2; }
	inline void set_octets_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___octets_2 = value;
		Il2CppCodeGenWriteBarrier((&___octets_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERAPPLICATIONSPECIFIC_T172FBAA17F6C55C0E6B5999D6AB82A8773B75DEA_H
#ifndef DERBOOLEAN_TAF849A79A0BCE5C3F0DBB8BE9F6BA279E3D1A8CE_H
#define DERBOOLEAN_TAF849A79A0BCE5C3F0DBB8BE9F6BA279E3D1A8CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.DerBoolean
struct  DerBoolean_tAF849A79A0BCE5C3F0DBB8BE9F6BA279E3D1A8CE  : public Asn1Object_tB780E50883209D5C975E83B3D16257DFC3CCE36D
{
public:
	// System.Byte Org.BouncyCastle.Asn1.DerBoolean::value
	uint8_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(DerBoolean_tAF849A79A0BCE5C3F0DBB8BE9F6BA279E3D1A8CE, ___value_0)); }
	inline uint8_t get_value_0() const { return ___value_0; }
	inline uint8_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(uint8_t value)
	{
		___value_0 = value;
	}
};

struct DerBoolean_tAF849A79A0BCE5C3F0DBB8BE9F6BA279E3D1A8CE_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.DerBoolean Org.BouncyCastle.Asn1.DerBoolean::False
	DerBoolean_tAF849A79A0BCE5C3F0DBB8BE9F6BA279E3D1A8CE * ___False_1;
	// Org.BouncyCastle.Asn1.DerBoolean Org.BouncyCastle.Asn1.DerBoolean::True
	DerBoolean_tAF849A79A0BCE5C3F0DBB8BE9F6BA279E3D1A8CE * ___True_2;

public:
	inline static int32_t get_offset_of_False_1() { return static_cast<int32_t>(offsetof(DerBoolean_tAF849A79A0BCE5C3F0DBB8BE9F6BA279E3D1A8CE_StaticFields, ___False_1)); }
	inline DerBoolean_tAF849A79A0BCE5C3F0DBB8BE9F6BA279E3D1A8CE * get_False_1() const { return ___False_1; }
	inline DerBoolean_tAF849A79A0BCE5C3F0DBB8BE9F6BA279E3D1A8CE ** get_address_of_False_1() { return &___False_1; }
	inline void set_False_1(DerBoolean_tAF849A79A0BCE5C3F0DBB8BE9F6BA279E3D1A8CE * value)
	{
		___False_1 = value;
		Il2CppCodeGenWriteBarrier((&___False_1), value);
	}

	inline static int32_t get_offset_of_True_2() { return static_cast<int32_t>(offsetof(DerBoolean_tAF849A79A0BCE5C3F0DBB8BE9F6BA279E3D1A8CE_StaticFields, ___True_2)); }
	inline DerBoolean_tAF849A79A0BCE5C3F0DBB8BE9F6BA279E3D1A8CE * get_True_2() const { return ___True_2; }
	inline DerBoolean_tAF849A79A0BCE5C3F0DBB8BE9F6BA279E3D1A8CE ** get_address_of_True_2() { return &___True_2; }
	inline void set_True_2(DerBoolean_tAF849A79A0BCE5C3F0DBB8BE9F6BA279E3D1A8CE * value)
	{
		___True_2 = value;
		Il2CppCodeGenWriteBarrier((&___True_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERBOOLEAN_TAF849A79A0BCE5C3F0DBB8BE9F6BA279E3D1A8CE_H
#ifndef DERENUMERATED_TA6A732BA209AA4B10E55871BB22A8ACC7CFBCEC0_H
#define DERENUMERATED_TA6A732BA209AA4B10E55871BB22A8ACC7CFBCEC0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.DerEnumerated
struct  DerEnumerated_tA6A732BA209AA4B10E55871BB22A8ACC7CFBCEC0  : public Asn1Object_tB780E50883209D5C975E83B3D16257DFC3CCE36D
{
public:
	// System.Byte[] Org.BouncyCastle.Asn1.DerEnumerated::bytes
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___bytes_0;

public:
	inline static int32_t get_offset_of_bytes_0() { return static_cast<int32_t>(offsetof(DerEnumerated_tA6A732BA209AA4B10E55871BB22A8ACC7CFBCEC0, ___bytes_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_bytes_0() const { return ___bytes_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_bytes_0() { return &___bytes_0; }
	inline void set_bytes_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___bytes_0 = value;
		Il2CppCodeGenWriteBarrier((&___bytes_0), value);
	}
};

struct DerEnumerated_tA6A732BA209AA4B10E55871BB22A8ACC7CFBCEC0_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.DerEnumerated[] Org.BouncyCastle.Asn1.DerEnumerated::cache
	DerEnumeratedU5BU5D_tEBACB932DA336D53CCA9EEA8D863431522CD024D* ___cache_1;

public:
	inline static int32_t get_offset_of_cache_1() { return static_cast<int32_t>(offsetof(DerEnumerated_tA6A732BA209AA4B10E55871BB22A8ACC7CFBCEC0_StaticFields, ___cache_1)); }
	inline DerEnumeratedU5BU5D_tEBACB932DA336D53CCA9EEA8D863431522CD024D* get_cache_1() const { return ___cache_1; }
	inline DerEnumeratedU5BU5D_tEBACB932DA336D53CCA9EEA8D863431522CD024D** get_address_of_cache_1() { return &___cache_1; }
	inline void set_cache_1(DerEnumeratedU5BU5D_tEBACB932DA336D53CCA9EEA8D863431522CD024D* value)
	{
		___cache_1 = value;
		Il2CppCodeGenWriteBarrier((&___cache_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERENUMERATED_TA6A732BA209AA4B10E55871BB22A8ACC7CFBCEC0_H
#ifndef DEREXTERNAL_T0D40AE43FB75A047DB8035C40D8601F20DC35F5C_H
#define DEREXTERNAL_T0D40AE43FB75A047DB8035C40D8601F20DC35F5C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.DerExternal
struct  DerExternal_t0D40AE43FB75A047DB8035C40D8601F20DC35F5C  : public Asn1Object_tB780E50883209D5C975E83B3D16257DFC3CCE36D
{
public:
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.DerExternal::directReference
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___directReference_0;
	// Org.BouncyCastle.Asn1.DerInteger Org.BouncyCastle.Asn1.DerExternal::indirectReference
	DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A * ___indirectReference_1;
	// Org.BouncyCastle.Asn1.Asn1Object Org.BouncyCastle.Asn1.DerExternal::dataValueDescriptor
	Asn1Object_tB780E50883209D5C975E83B3D16257DFC3CCE36D * ___dataValueDescriptor_2;
	// System.Int32 Org.BouncyCastle.Asn1.DerExternal::encoding
	int32_t ___encoding_3;
	// Org.BouncyCastle.Asn1.Asn1Object Org.BouncyCastle.Asn1.DerExternal::externalContent
	Asn1Object_tB780E50883209D5C975E83B3D16257DFC3CCE36D * ___externalContent_4;

public:
	inline static int32_t get_offset_of_directReference_0() { return static_cast<int32_t>(offsetof(DerExternal_t0D40AE43FB75A047DB8035C40D8601F20DC35F5C, ___directReference_0)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_directReference_0() const { return ___directReference_0; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_directReference_0() { return &___directReference_0; }
	inline void set_directReference_0(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___directReference_0 = value;
		Il2CppCodeGenWriteBarrier((&___directReference_0), value);
	}

	inline static int32_t get_offset_of_indirectReference_1() { return static_cast<int32_t>(offsetof(DerExternal_t0D40AE43FB75A047DB8035C40D8601F20DC35F5C, ___indirectReference_1)); }
	inline DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A * get_indirectReference_1() const { return ___indirectReference_1; }
	inline DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A ** get_address_of_indirectReference_1() { return &___indirectReference_1; }
	inline void set_indirectReference_1(DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A * value)
	{
		___indirectReference_1 = value;
		Il2CppCodeGenWriteBarrier((&___indirectReference_1), value);
	}

	inline static int32_t get_offset_of_dataValueDescriptor_2() { return static_cast<int32_t>(offsetof(DerExternal_t0D40AE43FB75A047DB8035C40D8601F20DC35F5C, ___dataValueDescriptor_2)); }
	inline Asn1Object_tB780E50883209D5C975E83B3D16257DFC3CCE36D * get_dataValueDescriptor_2() const { return ___dataValueDescriptor_2; }
	inline Asn1Object_tB780E50883209D5C975E83B3D16257DFC3CCE36D ** get_address_of_dataValueDescriptor_2() { return &___dataValueDescriptor_2; }
	inline void set_dataValueDescriptor_2(Asn1Object_tB780E50883209D5C975E83B3D16257DFC3CCE36D * value)
	{
		___dataValueDescriptor_2 = value;
		Il2CppCodeGenWriteBarrier((&___dataValueDescriptor_2), value);
	}

	inline static int32_t get_offset_of_encoding_3() { return static_cast<int32_t>(offsetof(DerExternal_t0D40AE43FB75A047DB8035C40D8601F20DC35F5C, ___encoding_3)); }
	inline int32_t get_encoding_3() const { return ___encoding_3; }
	inline int32_t* get_address_of_encoding_3() { return &___encoding_3; }
	inline void set_encoding_3(int32_t value)
	{
		___encoding_3 = value;
	}

	inline static int32_t get_offset_of_externalContent_4() { return static_cast<int32_t>(offsetof(DerExternal_t0D40AE43FB75A047DB8035C40D8601F20DC35F5C, ___externalContent_4)); }
	inline Asn1Object_tB780E50883209D5C975E83B3D16257DFC3CCE36D * get_externalContent_4() const { return ___externalContent_4; }
	inline Asn1Object_tB780E50883209D5C975E83B3D16257DFC3CCE36D ** get_address_of_externalContent_4() { return &___externalContent_4; }
	inline void set_externalContent_4(Asn1Object_tB780E50883209D5C975E83B3D16257DFC3CCE36D * value)
	{
		___externalContent_4 = value;
		Il2CppCodeGenWriteBarrier((&___externalContent_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEREXTERNAL_T0D40AE43FB75A047DB8035C40D8601F20DC35F5C_H
#ifndef DERGENERALIZEDTIME_T1694B0E0C14EFC3A2FFC46DD64F5D8D6B55599E9_H
#define DERGENERALIZEDTIME_T1694B0E0C14EFC3A2FFC46DD64F5D8D6B55599E9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.DerGeneralizedTime
struct  DerGeneralizedTime_t1694B0E0C14EFC3A2FFC46DD64F5D8D6B55599E9  : public Asn1Object_tB780E50883209D5C975E83B3D16257DFC3CCE36D
{
public:
	// System.String Org.BouncyCastle.Asn1.DerGeneralizedTime::time
	String_t* ___time_0;

public:
	inline static int32_t get_offset_of_time_0() { return static_cast<int32_t>(offsetof(DerGeneralizedTime_t1694B0E0C14EFC3A2FFC46DD64F5D8D6B55599E9, ___time_0)); }
	inline String_t* get_time_0() const { return ___time_0; }
	inline String_t** get_address_of_time_0() { return &___time_0; }
	inline void set_time_0(String_t* value)
	{
		___time_0 = value;
		Il2CppCodeGenWriteBarrier((&___time_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERGENERALIZEDTIME_T1694B0E0C14EFC3A2FFC46DD64F5D8D6B55599E9_H
#ifndef DERINTEGER_TAFB56211E7C3C0E309C4E109687773710F2B227A_H
#define DERINTEGER_TAFB56211E7C3C0E309C4E109687773710F2B227A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.DerInteger
struct  DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A  : public Asn1Object_tB780E50883209D5C975E83B3D16257DFC3CCE36D
{
public:
	// System.Byte[] Org.BouncyCastle.Asn1.DerInteger::bytes
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___bytes_0;

public:
	inline static int32_t get_offset_of_bytes_0() { return static_cast<int32_t>(offsetof(DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A, ___bytes_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_bytes_0() const { return ___bytes_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_bytes_0() { return &___bytes_0; }
	inline void set_bytes_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___bytes_0 = value;
		Il2CppCodeGenWriteBarrier((&___bytes_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERINTEGER_TAFB56211E7C3C0E309C4E109687773710F2B227A_H
#ifndef DEROBJECTIDENTIFIER_TC3F1B6EAC07B07726FEC51CED107FBC369DEF441_H
#define DEROBJECTIDENTIFIER_TC3F1B6EAC07B07726FEC51CED107FBC369DEF441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.DerObjectIdentifier
struct  DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441  : public Asn1Object_tB780E50883209D5C975E83B3D16257DFC3CCE36D
{
public:
	// System.String Org.BouncyCastle.Asn1.DerObjectIdentifier::identifier
	String_t* ___identifier_0;
	// System.Byte[] Org.BouncyCastle.Asn1.DerObjectIdentifier::body
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___body_1;

public:
	inline static int32_t get_offset_of_identifier_0() { return static_cast<int32_t>(offsetof(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441, ___identifier_0)); }
	inline String_t* get_identifier_0() const { return ___identifier_0; }
	inline String_t** get_address_of_identifier_0() { return &___identifier_0; }
	inline void set_identifier_0(String_t* value)
	{
		___identifier_0 = value;
		Il2CppCodeGenWriteBarrier((&___identifier_0), value);
	}

	inline static int32_t get_offset_of_body_1() { return static_cast<int32_t>(offsetof(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441, ___body_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_body_1() const { return ___body_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_body_1() { return &___body_1; }
	inline void set_body_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___body_1 = value;
		Il2CppCodeGenWriteBarrier((&___body_1), value);
	}
};

struct DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.DerObjectIdentifier[] Org.BouncyCastle.Asn1.DerObjectIdentifier::cache
	DerObjectIdentifierU5BU5D_t95AEC7AE62F0F0FD9FF53036DF9BEDDF497B02B9* ___cache_2;

public:
	inline static int32_t get_offset_of_cache_2() { return static_cast<int32_t>(offsetof(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441_StaticFields, ___cache_2)); }
	inline DerObjectIdentifierU5BU5D_t95AEC7AE62F0F0FD9FF53036DF9BEDDF497B02B9* get_cache_2() const { return ___cache_2; }
	inline DerObjectIdentifierU5BU5D_t95AEC7AE62F0F0FD9FF53036DF9BEDDF497B02B9** get_address_of_cache_2() { return &___cache_2; }
	inline void set_cache_2(DerObjectIdentifierU5BU5D_t95AEC7AE62F0F0FD9FF53036DF9BEDDF497B02B9* value)
	{
		___cache_2 = value;
		Il2CppCodeGenWriteBarrier((&___cache_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEROBJECTIDENTIFIER_TC3F1B6EAC07B07726FEC51CED107FBC369DEF441_H
#ifndef DERSTRINGBASE_T56EF0CDEB7AF4C8CE9701F3940380FF0EAC26A01_H
#define DERSTRINGBASE_T56EF0CDEB7AF4C8CE9701F3940380FF0EAC26A01_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.DerStringBase
struct  DerStringBase_t56EF0CDEB7AF4C8CE9701F3940380FF0EAC26A01  : public Asn1Object_tB780E50883209D5C975E83B3D16257DFC3CCE36D
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERSTRINGBASE_T56EF0CDEB7AF4C8CE9701F3940380FF0EAC26A01_H
#ifndef DERUTCTIME_T2E017A28117C3C01BCC8DD70844E080FCBB0A896_H
#define DERUTCTIME_T2E017A28117C3C01BCC8DD70844E080FCBB0A896_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.DerUtcTime
struct  DerUtcTime_t2E017A28117C3C01BCC8DD70844E080FCBB0A896  : public Asn1Object_tB780E50883209D5C975E83B3D16257DFC3CCE36D
{
public:
	// System.String Org.BouncyCastle.Asn1.DerUtcTime::time
	String_t* ___time_0;

public:
	inline static int32_t get_offset_of_time_0() { return static_cast<int32_t>(offsetof(DerUtcTime_t2E017A28117C3C01BCC8DD70844E080FCBB0A896, ___time_0)); }
	inline String_t* get_time_0() const { return ___time_0; }
	inline String_t** get_address_of_time_0() { return &___time_0; }
	inline void set_time_0(String_t* value)
	{
		___time_0 = value;
		Il2CppCodeGenWriteBarrier((&___time_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERUTCTIME_T2E017A28117C3C01BCC8DD70844E080FCBB0A896_H
#ifndef BASEINPUTSTREAM_T391EE4FC5A02C738BAA41F88320C83E7C99061A7_H
#define BASEINPUTSTREAM_T391EE4FC5A02C738BAA41F88320C83E7C99061A7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Utilities.IO.BaseInputStream
struct  BaseInputStream_t391EE4FC5A02C738BAA41F88320C83E7C99061A7  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// System.Boolean Org.BouncyCastle.Utilities.IO.BaseInputStream::closed
	bool ___closed_5;

public:
	inline static int32_t get_offset_of_closed_5() { return static_cast<int32_t>(offsetof(BaseInputStream_t391EE4FC5A02C738BAA41F88320C83E7C99061A7, ___closed_5)); }
	inline bool get_closed_5() const { return ___closed_5; }
	inline bool* get_address_of_closed_5() { return &___closed_5; }
	inline void set_closed_5(bool value)
	{
		___closed_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEINPUTSTREAM_T391EE4FC5A02C738BAA41F88320C83E7C99061A7_H
#ifndef FILTERSTREAM_T88E5588F96DA875764C392A00134F11FFAEFF567_H
#define FILTERSTREAM_T88E5588F96DA875764C392A00134F11FFAEFF567_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Utilities.IO.FilterStream
struct  FilterStream_t88E5588F96DA875764C392A00134F11FFAEFF567  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// System.IO.Stream Org.BouncyCastle.Utilities.IO.FilterStream::s
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___s_5;

public:
	inline static int32_t get_offset_of_s_5() { return static_cast<int32_t>(offsetof(FilterStream_t88E5588F96DA875764C392A00134F11FFAEFF567, ___s_5)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_s_5() const { return ___s_5; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_s_5() { return &___s_5; }
	inline void set_s_5(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___s_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILTERSTREAM_T88E5588F96DA875764C392A00134F11FFAEFF567_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef IOEXCEPTION_T60E052020EDE4D3075F57A1DCC224FF8864354BA_H
#define IOEXCEPTION_T60E052020EDE4D3075F57A1DCC224FF8864354BA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.IOException
struct  IOException_t60E052020EDE4D3075F57A1DCC224FF8864354BA  : public SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782
{
public:
	// System.String System.IO.IOException::_maybeFullPath
	String_t* ____maybeFullPath_17;

public:
	inline static int32_t get_offset_of__maybeFullPath_17() { return static_cast<int32_t>(offsetof(IOException_t60E052020EDE4D3075F57A1DCC224FF8864354BA, ____maybeFullPath_17)); }
	inline String_t* get__maybeFullPath_17() const { return ____maybeFullPath_17; }
	inline String_t** get_address_of__maybeFullPath_17() { return &____maybeFullPath_17; }
	inline void set__maybeFullPath_17(String_t* value)
	{
		____maybeFullPath_17 = value;
		Il2CppCodeGenWriteBarrier((&____maybeFullPath_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOEXCEPTION_T60E052020EDE4D3075F57A1DCC224FF8864354BA_H
#ifndef INVALIDOPERATIONEXCEPTION_T0530E734D823F78310CAFAFA424CA5164D93A1F1_H
#define INVALIDOPERATIONEXCEPTION_T0530E734D823F78310CAFAFA424CA5164D93A1F1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.InvalidOperationException
struct  InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1  : public SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVALIDOPERATIONEXCEPTION_T0530E734D823F78310CAFAFA424CA5164D93A1F1_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef PLAYABLEHANDLE_T9D3B4E540D4413CED81DDD6A24C5373BEFA1D182_H
#define PLAYABLEHANDLE_T9D3B4E540D4413CED81DDD6A24C5373BEFA1D182_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableHandle
struct  PlayableHandle_t9D3B4E540D4413CED81DDD6A24C5373BEFA1D182 
{
public:
	// System.IntPtr UnityEngine.Playables.PlayableHandle::m_Handle
	intptr_t ___m_Handle_0;
	// System.UInt32 UnityEngine.Playables.PlayableHandle::m_Version
	uint32_t ___m_Version_1;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(PlayableHandle_t9D3B4E540D4413CED81DDD6A24C5373BEFA1D182, ___m_Handle_0)); }
	inline intptr_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline intptr_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(intptr_t value)
	{
		___m_Handle_0 = value;
	}

	inline static int32_t get_offset_of_m_Version_1() { return static_cast<int32_t>(offsetof(PlayableHandle_t9D3B4E540D4413CED81DDD6A24C5373BEFA1D182, ___m_Version_1)); }
	inline uint32_t get_m_Version_1() const { return ___m_Version_1; }
	inline uint32_t* get_address_of_m_Version_1() { return &___m_Version_1; }
	inline void set_m_Version_1(uint32_t value)
	{
		___m_Version_1 = value;
	}
};

struct PlayableHandle_t9D3B4E540D4413CED81DDD6A24C5373BEFA1D182_StaticFields
{
public:
	// UnityEngine.Playables.PlayableHandle UnityEngine.Playables.PlayableHandle::m_Null
	PlayableHandle_t9D3B4E540D4413CED81DDD6A24C5373BEFA1D182  ___m_Null_2;

public:
	inline static int32_t get_offset_of_m_Null_2() { return static_cast<int32_t>(offsetof(PlayableHandle_t9D3B4E540D4413CED81DDD6A24C5373BEFA1D182_StaticFields, ___m_Null_2)); }
	inline PlayableHandle_t9D3B4E540D4413CED81DDD6A24C5373BEFA1D182  get_m_Null_2() const { return ___m_Null_2; }
	inline PlayableHandle_t9D3B4E540D4413CED81DDD6A24C5373BEFA1D182 * get_address_of_m_Null_2() { return &___m_Null_2; }
	inline void set_m_Null_2(PlayableHandle_t9D3B4E540D4413CED81DDD6A24C5373BEFA1D182  value)
	{
		___m_Null_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEHANDLE_T9D3B4E540D4413CED81DDD6A24C5373BEFA1D182_H
#ifndef VIDEO3DLAYOUT_T5F64D0CE5E9B37C2FCE67F397FA5CFE9C047E4A1_H
#define VIDEO3DLAYOUT_T5F64D0CE5E9B37C2FCE67F397FA5CFE9C047E4A1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Video.Video3DLayout
struct  Video3DLayout_t5F64D0CE5E9B37C2FCE67F397FA5CFE9C047E4A1 
{
public:
	// System.Int32 UnityEngine.Video.Video3DLayout::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Video3DLayout_t5F64D0CE5E9B37C2FCE67F397FA5CFE9C047E4A1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEO3DLAYOUT_T5F64D0CE5E9B37C2FCE67F397FA5CFE9C047E4A1_H
#ifndef VIDEOASPECTRATIO_T5739968D28C4F8F802B085E293F22110205B8379_H
#define VIDEOASPECTRATIO_T5739968D28C4F8F802B085E293F22110205B8379_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Video.VideoAspectRatio
struct  VideoAspectRatio_t5739968D28C4F8F802B085E293F22110205B8379 
{
public:
	// System.Int32 UnityEngine.Video.VideoAspectRatio::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VideoAspectRatio_t5739968D28C4F8F802B085E293F22110205B8379, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOASPECTRATIO_T5739968D28C4F8F802B085E293F22110205B8379_H
#ifndef VIDEOAUDIOOUTPUTMODE_T8CDE10B382F3C321345EC57C9164A9177139DC6F_H
#define VIDEOAUDIOOUTPUTMODE_T8CDE10B382F3C321345EC57C9164A9177139DC6F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Video.VideoAudioOutputMode
struct  VideoAudioOutputMode_t8CDE10B382F3C321345EC57C9164A9177139DC6F 
{
public:
	// System.Int32 UnityEngine.Video.VideoAudioOutputMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VideoAudioOutputMode_t8CDE10B382F3C321345EC57C9164A9177139DC6F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOAUDIOOUTPUTMODE_T8CDE10B382F3C321345EC57C9164A9177139DC6F_H
#ifndef VIDEORENDERMODE_T0DBAABB576FDA890C49C6AD3EE641623F93E9161_H
#define VIDEORENDERMODE_T0DBAABB576FDA890C49C6AD3EE641623F93E9161_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Video.VideoRenderMode
struct  VideoRenderMode_t0DBAABB576FDA890C49C6AD3EE641623F93E9161 
{
public:
	// System.Int32 UnityEngine.Video.VideoRenderMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VideoRenderMode_t0DBAABB576FDA890C49C6AD3EE641623F93E9161, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEORENDERMODE_T0DBAABB576FDA890C49C6AD3EE641623F93E9161_H
#ifndef VIDEOSOURCE_T32501B57EA7F9CF835FBA8184C9AF427CBBEFD0A_H
#define VIDEOSOURCE_T32501B57EA7F9CF835FBA8184C9AF427CBBEFD0A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Video.VideoSource
struct  VideoSource_t32501B57EA7F9CF835FBA8184C9AF427CBBEFD0A 
{
public:
	// System.Int32 UnityEngine.Video.VideoSource::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VideoSource_t32501B57EA7F9CF835FBA8184C9AF427CBBEFD0A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOSOURCE_T32501B57EA7F9CF835FBA8184C9AF427CBBEFD0A_H
#ifndef VIDEOTIMEREFERENCE_T9EAEBD354AE5E56F0D0F36E73A428BB2A0B8B31B_H
#define VIDEOTIMEREFERENCE_T9EAEBD354AE5E56F0D0F36E73A428BB2A0B8B31B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Video.VideoTimeReference
struct  VideoTimeReference_t9EAEBD354AE5E56F0D0F36E73A428BB2A0B8B31B 
{
public:
	// System.Int32 UnityEngine.Video.VideoTimeReference::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VideoTimeReference_t9EAEBD354AE5E56F0D0F36E73A428BB2A0B8B31B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOTIMEREFERENCE_T9EAEBD354AE5E56F0D0F36E73A428BB2A0B8B31B_H
#ifndef VIDEOTIMESOURCE_T15F04FD6B3D75A8D98480E8B77117C0FF691BB77_H
#define VIDEOTIMESOURCE_T15F04FD6B3D75A8D98480E8B77117C0FF691BB77_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Video.VideoTimeSource
struct  VideoTimeSource_t15F04FD6B3D75A8D98480E8B77117C0FF691BB77 
{
public:
	// System.Int32 UnityEngine.Video.VideoTimeSource::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VideoTimeSource_t15F04FD6B3D75A8D98480E8B77117C0FF691BB77, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOTIMESOURCE_T15F04FD6B3D75A8D98480E8B77117C0FF691BB77_H
#ifndef ASN1EXCEPTION_T719F7048E3DB1600F42B61F3F25501A5CDB1B123_H
#define ASN1EXCEPTION_T719F7048E3DB1600F42B61F3F25501A5CDB1B123_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.Asn1Exception
struct  Asn1Exception_t719F7048E3DB1600F42B61F3F25501A5CDB1B123  : public IOException_t60E052020EDE4D3075F57A1DCC224FF8864354BA
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASN1EXCEPTION_T719F7048E3DB1600F42B61F3F25501A5CDB1B123_H
#ifndef ASN1INPUTSTREAM_TA178A8C2AB75A5D4506835E71C3A4CAB02C9E17A_H
#define ASN1INPUTSTREAM_TA178A8C2AB75A5D4506835E71C3A4CAB02C9E17A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.Asn1InputStream
struct  Asn1InputStream_tA178A8C2AB75A5D4506835E71C3A4CAB02C9E17A  : public FilterStream_t88E5588F96DA875764C392A00134F11FFAEFF567
{
public:
	// System.Int32 Org.BouncyCastle.Asn1.Asn1InputStream::limit
	int32_t ___limit_6;
	// System.Byte[][] Org.BouncyCastle.Asn1.Asn1InputStream::tmpBuffers
	ByteU5BU5DU5BU5D_tD1CB918775FFB351821F10AC338FECDDE22DEEC7* ___tmpBuffers_7;

public:
	inline static int32_t get_offset_of_limit_6() { return static_cast<int32_t>(offsetof(Asn1InputStream_tA178A8C2AB75A5D4506835E71C3A4CAB02C9E17A, ___limit_6)); }
	inline int32_t get_limit_6() const { return ___limit_6; }
	inline int32_t* get_address_of_limit_6() { return &___limit_6; }
	inline void set_limit_6(int32_t value)
	{
		___limit_6 = value;
	}

	inline static int32_t get_offset_of_tmpBuffers_7() { return static_cast<int32_t>(offsetof(Asn1InputStream_tA178A8C2AB75A5D4506835E71C3A4CAB02C9E17A, ___tmpBuffers_7)); }
	inline ByteU5BU5DU5BU5D_tD1CB918775FFB351821F10AC338FECDDE22DEEC7* get_tmpBuffers_7() const { return ___tmpBuffers_7; }
	inline ByteU5BU5DU5BU5D_tD1CB918775FFB351821F10AC338FECDDE22DEEC7** get_address_of_tmpBuffers_7() { return &___tmpBuffers_7; }
	inline void set_tmpBuffers_7(ByteU5BU5DU5BU5D_tD1CB918775FFB351821F10AC338FECDDE22DEEC7* value)
	{
		___tmpBuffers_7 = value;
		Il2CppCodeGenWriteBarrier((&___tmpBuffers_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASN1INPUTSTREAM_TA178A8C2AB75A5D4506835E71C3A4CAB02C9E17A_H
#ifndef ASN1PARSINGEXCEPTION_T47AE48198AC29ED9C016413382AD7D1ED8722E43_H
#define ASN1PARSINGEXCEPTION_T47AE48198AC29ED9C016413382AD7D1ED8722E43_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.Asn1ParsingException
struct  Asn1ParsingException_t47AE48198AC29ED9C016413382AD7D1ED8722E43  : public InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASN1PARSINGEXCEPTION_T47AE48198AC29ED9C016413382AD7D1ED8722E43_H
#ifndef BERAPPLICATIONSPECIFIC_T1B612BEE42DD61691B140761B80F136FCB8D6F8D_H
#define BERAPPLICATIONSPECIFIC_T1B612BEE42DD61691B140761B80F136FCB8D6F8D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.BerApplicationSpecific
struct  BerApplicationSpecific_t1B612BEE42DD61691B140761B80F136FCB8D6F8D  : public DerApplicationSpecific_t172FBAA17F6C55C0E6B5999D6AB82A8773B75DEA
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BERAPPLICATIONSPECIFIC_T1B612BEE42DD61691B140761B80F136FCB8D6F8D_H
#ifndef CONSTRUCTEDOCTETSTREAM_T7A51325971607AF7EE9FF10F000BBDBD5B245BF8_H
#define CONSTRUCTEDOCTETSTREAM_T7A51325971607AF7EE9FF10F000BBDBD5B245BF8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.ConstructedOctetStream
struct  ConstructedOctetStream_t7A51325971607AF7EE9FF10F000BBDBD5B245BF8  : public BaseInputStream_t391EE4FC5A02C738BAA41F88320C83E7C99061A7
{
public:
	// Org.BouncyCastle.Asn1.Asn1StreamParser Org.BouncyCastle.Asn1.ConstructedOctetStream::_parser
	Asn1StreamParser_t2FE3FB89834CCEE990CDD0F38B5DD0960CDC9C90 * ____parser_6;
	// System.Boolean Org.BouncyCastle.Asn1.ConstructedOctetStream::_first
	bool ____first_7;
	// System.IO.Stream Org.BouncyCastle.Asn1.ConstructedOctetStream::_currentStream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ____currentStream_8;

public:
	inline static int32_t get_offset_of__parser_6() { return static_cast<int32_t>(offsetof(ConstructedOctetStream_t7A51325971607AF7EE9FF10F000BBDBD5B245BF8, ____parser_6)); }
	inline Asn1StreamParser_t2FE3FB89834CCEE990CDD0F38B5DD0960CDC9C90 * get__parser_6() const { return ____parser_6; }
	inline Asn1StreamParser_t2FE3FB89834CCEE990CDD0F38B5DD0960CDC9C90 ** get_address_of__parser_6() { return &____parser_6; }
	inline void set__parser_6(Asn1StreamParser_t2FE3FB89834CCEE990CDD0F38B5DD0960CDC9C90 * value)
	{
		____parser_6 = value;
		Il2CppCodeGenWriteBarrier((&____parser_6), value);
	}

	inline static int32_t get_offset_of__first_7() { return static_cast<int32_t>(offsetof(ConstructedOctetStream_t7A51325971607AF7EE9FF10F000BBDBD5B245BF8, ____first_7)); }
	inline bool get__first_7() const { return ____first_7; }
	inline bool* get_address_of__first_7() { return &____first_7; }
	inline void set__first_7(bool value)
	{
		____first_7 = value;
	}

	inline static int32_t get_offset_of__currentStream_8() { return static_cast<int32_t>(offsetof(ConstructedOctetStream_t7A51325971607AF7EE9FF10F000BBDBD5B245BF8, ____currentStream_8)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get__currentStream_8() const { return ____currentStream_8; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of__currentStream_8() { return &____currentStream_8; }
	inline void set__currentStream_8(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		____currentStream_8 = value;
		Il2CppCodeGenWriteBarrier((&____currentStream_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTRUCTEDOCTETSTREAM_T7A51325971607AF7EE9FF10F000BBDBD5B245BF8_H
#ifndef DERBITSTRING_TCA371BCECC9BA7643D0127424F54EE10C1B19A64_H
#define DERBITSTRING_TCA371BCECC9BA7643D0127424F54EE10C1B19A64_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.DerBitString
struct  DerBitString_tCA371BCECC9BA7643D0127424F54EE10C1B19A64  : public DerStringBase_t56EF0CDEB7AF4C8CE9701F3940380FF0EAC26A01
{
public:
	// System.Byte[] Org.BouncyCastle.Asn1.DerBitString::mData
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mData_1;
	// System.Int32 Org.BouncyCastle.Asn1.DerBitString::mPadBits
	int32_t ___mPadBits_2;

public:
	inline static int32_t get_offset_of_mData_1() { return static_cast<int32_t>(offsetof(DerBitString_tCA371BCECC9BA7643D0127424F54EE10C1B19A64, ___mData_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mData_1() const { return ___mData_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mData_1() { return &___mData_1; }
	inline void set_mData_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mData_1 = value;
		Il2CppCodeGenWriteBarrier((&___mData_1), value);
	}

	inline static int32_t get_offset_of_mPadBits_2() { return static_cast<int32_t>(offsetof(DerBitString_tCA371BCECC9BA7643D0127424F54EE10C1B19A64, ___mPadBits_2)); }
	inline int32_t get_mPadBits_2() const { return ___mPadBits_2; }
	inline int32_t* get_address_of_mPadBits_2() { return &___mPadBits_2; }
	inline void set_mPadBits_2(int32_t value)
	{
		___mPadBits_2 = value;
	}
};

struct DerBitString_tCA371BCECC9BA7643D0127424F54EE10C1B19A64_StaticFields
{
public:
	// System.Char[] Org.BouncyCastle.Asn1.DerBitString::table
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___table_0;

public:
	inline static int32_t get_offset_of_table_0() { return static_cast<int32_t>(offsetof(DerBitString_tCA371BCECC9BA7643D0127424F54EE10C1B19A64_StaticFields, ___table_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_table_0() const { return ___table_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_table_0() { return &___table_0; }
	inline void set_table_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___table_0 = value;
		Il2CppCodeGenWriteBarrier((&___table_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERBITSTRING_TCA371BCECC9BA7643D0127424F54EE10C1B19A64_H
#ifndef DERBMPSTRING_T05DD26A4899B75AE508EFB316660C76CFA263CDB_H
#define DERBMPSTRING_T05DD26A4899B75AE508EFB316660C76CFA263CDB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.DerBmpString
struct  DerBmpString_t05DD26A4899B75AE508EFB316660C76CFA263CDB  : public DerStringBase_t56EF0CDEB7AF4C8CE9701F3940380FF0EAC26A01
{
public:
	// System.String Org.BouncyCastle.Asn1.DerBmpString::str
	String_t* ___str_0;

public:
	inline static int32_t get_offset_of_str_0() { return static_cast<int32_t>(offsetof(DerBmpString_t05DD26A4899B75AE508EFB316660C76CFA263CDB, ___str_0)); }
	inline String_t* get_str_0() const { return ___str_0; }
	inline String_t** get_address_of_str_0() { return &___str_0; }
	inline void set_str_0(String_t* value)
	{
		___str_0 = value;
		Il2CppCodeGenWriteBarrier((&___str_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERBMPSTRING_T05DD26A4899B75AE508EFB316660C76CFA263CDB_H
#ifndef DERGENERALSTRING_T7C07A4AAAB11F833D0D50FF091D1AC2F10F2EEF6_H
#define DERGENERALSTRING_T7C07A4AAAB11F833D0D50FF091D1AC2F10F2EEF6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.DerGeneralString
struct  DerGeneralString_t7C07A4AAAB11F833D0D50FF091D1AC2F10F2EEF6  : public DerStringBase_t56EF0CDEB7AF4C8CE9701F3940380FF0EAC26A01
{
public:
	// System.String Org.BouncyCastle.Asn1.DerGeneralString::str
	String_t* ___str_0;

public:
	inline static int32_t get_offset_of_str_0() { return static_cast<int32_t>(offsetof(DerGeneralString_t7C07A4AAAB11F833D0D50FF091D1AC2F10F2EEF6, ___str_0)); }
	inline String_t* get_str_0() const { return ___str_0; }
	inline String_t** get_address_of_str_0() { return &___str_0; }
	inline void set_str_0(String_t* value)
	{
		___str_0 = value;
		Il2CppCodeGenWriteBarrier((&___str_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERGENERALSTRING_T7C07A4AAAB11F833D0D50FF091D1AC2F10F2EEF6_H
#ifndef DERGRAPHICSTRING_T289E34555383228BE834CF3D6BD83FD1C9D9BEFF_H
#define DERGRAPHICSTRING_T289E34555383228BE834CF3D6BD83FD1C9D9BEFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.DerGraphicString
struct  DerGraphicString_t289E34555383228BE834CF3D6BD83FD1C9D9BEFF  : public DerStringBase_t56EF0CDEB7AF4C8CE9701F3940380FF0EAC26A01
{
public:
	// System.Byte[] Org.BouncyCastle.Asn1.DerGraphicString::mString
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mString_0;

public:
	inline static int32_t get_offset_of_mString_0() { return static_cast<int32_t>(offsetof(DerGraphicString_t289E34555383228BE834CF3D6BD83FD1C9D9BEFF, ___mString_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mString_0() const { return ___mString_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mString_0() { return &___mString_0; }
	inline void set_mString_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mString_0 = value;
		Il2CppCodeGenWriteBarrier((&___mString_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERGRAPHICSTRING_T289E34555383228BE834CF3D6BD83FD1C9D9BEFF_H
#ifndef DERIA5STRING_T8BA9BA3EE704517270F77ED3B87506183AB11CA1_H
#define DERIA5STRING_T8BA9BA3EE704517270F77ED3B87506183AB11CA1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.DerIA5String
struct  DerIA5String_t8BA9BA3EE704517270F77ED3B87506183AB11CA1  : public DerStringBase_t56EF0CDEB7AF4C8CE9701F3940380FF0EAC26A01
{
public:
	// System.String Org.BouncyCastle.Asn1.DerIA5String::str
	String_t* ___str_0;

public:
	inline static int32_t get_offset_of_str_0() { return static_cast<int32_t>(offsetof(DerIA5String_t8BA9BA3EE704517270F77ED3B87506183AB11CA1, ___str_0)); }
	inline String_t* get_str_0() const { return ___str_0; }
	inline String_t** get_address_of_str_0() { return &___str_0; }
	inline void set_str_0(String_t* value)
	{
		___str_0 = value;
		Il2CppCodeGenWriteBarrier((&___str_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERIA5STRING_T8BA9BA3EE704517270F77ED3B87506183AB11CA1_H
#ifndef DERNULL_TC71A4E14BB19051D3E11138674CEF9FFAB1F3ABD_H
#define DERNULL_TC71A4E14BB19051D3E11138674CEF9FFAB1F3ABD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.DerNull
struct  DerNull_tC71A4E14BB19051D3E11138674CEF9FFAB1F3ABD  : public Asn1Null_t9AC6DDB8C5501D354D5550F4B7674F94B3411DF7
{
public:
	// System.Byte[] Org.BouncyCastle.Asn1.DerNull::zeroBytes
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___zeroBytes_1;

public:
	inline static int32_t get_offset_of_zeroBytes_1() { return static_cast<int32_t>(offsetof(DerNull_tC71A4E14BB19051D3E11138674CEF9FFAB1F3ABD, ___zeroBytes_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_zeroBytes_1() const { return ___zeroBytes_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_zeroBytes_1() { return &___zeroBytes_1; }
	inline void set_zeroBytes_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___zeroBytes_1 = value;
		Il2CppCodeGenWriteBarrier((&___zeroBytes_1), value);
	}
};

struct DerNull_tC71A4E14BB19051D3E11138674CEF9FFAB1F3ABD_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.DerNull Org.BouncyCastle.Asn1.DerNull::Instance
	DerNull_tC71A4E14BB19051D3E11138674CEF9FFAB1F3ABD * ___Instance_0;

public:
	inline static int32_t get_offset_of_Instance_0() { return static_cast<int32_t>(offsetof(DerNull_tC71A4E14BB19051D3E11138674CEF9FFAB1F3ABD_StaticFields, ___Instance_0)); }
	inline DerNull_tC71A4E14BB19051D3E11138674CEF9FFAB1F3ABD * get_Instance_0() const { return ___Instance_0; }
	inline DerNull_tC71A4E14BB19051D3E11138674CEF9FFAB1F3ABD ** get_address_of_Instance_0() { return &___Instance_0; }
	inline void set_Instance_0(DerNull_tC71A4E14BB19051D3E11138674CEF9FFAB1F3ABD * value)
	{
		___Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERNULL_TC71A4E14BB19051D3E11138674CEF9FFAB1F3ABD_H
#ifndef DERNUMERICSTRING_T844038ADFA3B55C739B25A1A39AF888B36F7EF1C_H
#define DERNUMERICSTRING_T844038ADFA3B55C739B25A1A39AF888B36F7EF1C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.DerNumericString
struct  DerNumericString_t844038ADFA3B55C739B25A1A39AF888B36F7EF1C  : public DerStringBase_t56EF0CDEB7AF4C8CE9701F3940380FF0EAC26A01
{
public:
	// System.String Org.BouncyCastle.Asn1.DerNumericString::str
	String_t* ___str_0;

public:
	inline static int32_t get_offset_of_str_0() { return static_cast<int32_t>(offsetof(DerNumericString_t844038ADFA3B55C739B25A1A39AF888B36F7EF1C, ___str_0)); }
	inline String_t* get_str_0() const { return ___str_0; }
	inline String_t** get_address_of_str_0() { return &___str_0; }
	inline void set_str_0(String_t* value)
	{
		___str_0 = value;
		Il2CppCodeGenWriteBarrier((&___str_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERNUMERICSTRING_T844038ADFA3B55C739B25A1A39AF888B36F7EF1C_H
#ifndef DEROCTETSTRING_T4A1C1733812CD4857D067A49DBF362FECBF84782_H
#define DEROCTETSTRING_T4A1C1733812CD4857D067A49DBF362FECBF84782_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.DerOctetString
struct  DerOctetString_t4A1C1733812CD4857D067A49DBF362FECBF84782  : public Asn1OctetString_t2088F9E88E4C97BB9CF3B2FFC53CFBAF9B8B79CB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEROCTETSTRING_T4A1C1733812CD4857D067A49DBF362FECBF84782_H
#ifndef DEROUTPUTSTREAM_TEBC38D06077ED0B3F731C084CFE1E0590389C3A4_H
#define DEROUTPUTSTREAM_TEBC38D06077ED0B3F731C084CFE1E0590389C3A4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.DerOutputStream
struct  DerOutputStream_tEBC38D06077ED0B3F731C084CFE1E0590389C3A4  : public FilterStream_t88E5588F96DA875764C392A00134F11FFAEFF567
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEROUTPUTSTREAM_TEBC38D06077ED0B3F731C084CFE1E0590389C3A4_H
#ifndef DERPRINTABLESTRING_T379C98FE6054A375C307F97D18CF2BBA040FAA1E_H
#define DERPRINTABLESTRING_T379C98FE6054A375C307F97D18CF2BBA040FAA1E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.DerPrintableString
struct  DerPrintableString_t379C98FE6054A375C307F97D18CF2BBA040FAA1E  : public DerStringBase_t56EF0CDEB7AF4C8CE9701F3940380FF0EAC26A01
{
public:
	// System.String Org.BouncyCastle.Asn1.DerPrintableString::str
	String_t* ___str_0;

public:
	inline static int32_t get_offset_of_str_0() { return static_cast<int32_t>(offsetof(DerPrintableString_t379C98FE6054A375C307F97D18CF2BBA040FAA1E, ___str_0)); }
	inline String_t* get_str_0() const { return ___str_0; }
	inline String_t** get_address_of_str_0() { return &___str_0; }
	inline void set_str_0(String_t* value)
	{
		___str_0 = value;
		Il2CppCodeGenWriteBarrier((&___str_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERPRINTABLESTRING_T379C98FE6054A375C307F97D18CF2BBA040FAA1E_H
#ifndef DERSEQUENCE_TC013954AC20047A131699A1AD9B8899F88B8B811_H
#define DERSEQUENCE_TC013954AC20047A131699A1AD9B8899F88B8B811_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.DerSequence
struct  DerSequence_tC013954AC20047A131699A1AD9B8899F88B8B811  : public Asn1Sequence_t292AE1C416511A0DE153DE285AEE498B50523B85
{
public:

public:
};

struct DerSequence_tC013954AC20047A131699A1AD9B8899F88B8B811_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.DerSequence Org.BouncyCastle.Asn1.DerSequence::Empty
	DerSequence_tC013954AC20047A131699A1AD9B8899F88B8B811 * ___Empty_1;

public:
	inline static int32_t get_offset_of_Empty_1() { return static_cast<int32_t>(offsetof(DerSequence_tC013954AC20047A131699A1AD9B8899F88B8B811_StaticFields, ___Empty_1)); }
	inline DerSequence_tC013954AC20047A131699A1AD9B8899F88B8B811 * get_Empty_1() const { return ___Empty_1; }
	inline DerSequence_tC013954AC20047A131699A1AD9B8899F88B8B811 ** get_address_of_Empty_1() { return &___Empty_1; }
	inline void set_Empty_1(DerSequence_tC013954AC20047A131699A1AD9B8899F88B8B811 * value)
	{
		___Empty_1 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERSEQUENCE_TC013954AC20047A131699A1AD9B8899F88B8B811_H
#ifndef DERSET_T979F83976CC4AD737C9DE6638EEE89E0A2DE355F_H
#define DERSET_T979F83976CC4AD737C9DE6638EEE89E0A2DE355F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.DerSet
struct  DerSet_t979F83976CC4AD737C9DE6638EEE89E0A2DE355F  : public Asn1Set_tD51325409687C84A095F7692B251BDE783A1A4A7
{
public:

public:
};

struct DerSet_t979F83976CC4AD737C9DE6638EEE89E0A2DE355F_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.DerSet Org.BouncyCastle.Asn1.DerSet::Empty
	DerSet_t979F83976CC4AD737C9DE6638EEE89E0A2DE355F * ___Empty_1;

public:
	inline static int32_t get_offset_of_Empty_1() { return static_cast<int32_t>(offsetof(DerSet_t979F83976CC4AD737C9DE6638EEE89E0A2DE355F_StaticFields, ___Empty_1)); }
	inline DerSet_t979F83976CC4AD737C9DE6638EEE89E0A2DE355F * get_Empty_1() const { return ___Empty_1; }
	inline DerSet_t979F83976CC4AD737C9DE6638EEE89E0A2DE355F ** get_address_of_Empty_1() { return &___Empty_1; }
	inline void set_Empty_1(DerSet_t979F83976CC4AD737C9DE6638EEE89E0A2DE355F * value)
	{
		___Empty_1 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERSET_T979F83976CC4AD737C9DE6638EEE89E0A2DE355F_H
#ifndef DERT61STRING_T22B685156B3CF7EDA32775AB1370E673BE0DF378_H
#define DERT61STRING_T22B685156B3CF7EDA32775AB1370E673BE0DF378_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.DerT61String
struct  DerT61String_t22B685156B3CF7EDA32775AB1370E673BE0DF378  : public DerStringBase_t56EF0CDEB7AF4C8CE9701F3940380FF0EAC26A01
{
public:
	// System.String Org.BouncyCastle.Asn1.DerT61String::str
	String_t* ___str_0;

public:
	inline static int32_t get_offset_of_str_0() { return static_cast<int32_t>(offsetof(DerT61String_t22B685156B3CF7EDA32775AB1370E673BE0DF378, ___str_0)); }
	inline String_t* get_str_0() const { return ___str_0; }
	inline String_t** get_address_of_str_0() { return &___str_0; }
	inline void set_str_0(String_t* value)
	{
		___str_0 = value;
		Il2CppCodeGenWriteBarrier((&___str_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERT61STRING_T22B685156B3CF7EDA32775AB1370E673BE0DF378_H
#ifndef DERTAGGEDOBJECT_T080B7D91E7F2DEBB0707D74896A566377D841F5F_H
#define DERTAGGEDOBJECT_T080B7D91E7F2DEBB0707D74896A566377D841F5F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.DerTaggedObject
struct  DerTaggedObject_t080B7D91E7F2DEBB0707D74896A566377D841F5F  : public Asn1TaggedObject_t14E9E19978062973ABA220F78322FDDCEFC39B57
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERTAGGEDOBJECT_T080B7D91E7F2DEBB0707D74896A566377D841F5F_H
#ifndef DERUNIVERSALSTRING_TDAAC3796CE023B0E4FF54837DF2CEB46FDF259CF_H
#define DERUNIVERSALSTRING_TDAAC3796CE023B0E4FF54837DF2CEB46FDF259CF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.DerUniversalString
struct  DerUniversalString_tDAAC3796CE023B0E4FF54837DF2CEB46FDF259CF  : public DerStringBase_t56EF0CDEB7AF4C8CE9701F3940380FF0EAC26A01
{
public:
	// System.Byte[] Org.BouncyCastle.Asn1.DerUniversalString::str
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___str_1;

public:
	inline static int32_t get_offset_of_str_1() { return static_cast<int32_t>(offsetof(DerUniversalString_tDAAC3796CE023B0E4FF54837DF2CEB46FDF259CF, ___str_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_str_1() const { return ___str_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_str_1() { return &___str_1; }
	inline void set_str_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___str_1 = value;
		Il2CppCodeGenWriteBarrier((&___str_1), value);
	}
};

struct DerUniversalString_tDAAC3796CE023B0E4FF54837DF2CEB46FDF259CF_StaticFields
{
public:
	// System.Char[] Org.BouncyCastle.Asn1.DerUniversalString::table
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___table_0;

public:
	inline static int32_t get_offset_of_table_0() { return static_cast<int32_t>(offsetof(DerUniversalString_tDAAC3796CE023B0E4FF54837DF2CEB46FDF259CF_StaticFields, ___table_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_table_0() const { return ___table_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_table_0() { return &___table_0; }
	inline void set_table_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___table_0 = value;
		Il2CppCodeGenWriteBarrier((&___table_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERUNIVERSALSTRING_TDAAC3796CE023B0E4FF54837DF2CEB46FDF259CF_H
#ifndef DERUTF8STRING_T35F781218A4AFEBE1AEAD46D8ECC6C899AC73EF6_H
#define DERUTF8STRING_T35F781218A4AFEBE1AEAD46D8ECC6C899AC73EF6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.DerUtf8String
struct  DerUtf8String_t35F781218A4AFEBE1AEAD46D8ECC6C899AC73EF6  : public DerStringBase_t56EF0CDEB7AF4C8CE9701F3940380FF0EAC26A01
{
public:
	// System.String Org.BouncyCastle.Asn1.DerUtf8String::str
	String_t* ___str_0;

public:
	inline static int32_t get_offset_of_str_0() { return static_cast<int32_t>(offsetof(DerUtf8String_t35F781218A4AFEBE1AEAD46D8ECC6C899AC73EF6, ___str_0)); }
	inline String_t* get_str_0() const { return ___str_0; }
	inline String_t** get_address_of_str_0() { return &___str_0; }
	inline void set_str_0(String_t* value)
	{
		___str_0 = value;
		Il2CppCodeGenWriteBarrier((&___str_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERUTF8STRING_T35F781218A4AFEBE1AEAD46D8ECC6C899AC73EF6_H
#ifndef DERVIDEOTEXSTRING_T407B1B1614BB70830C5F7C954965867D199FC48A_H
#define DERVIDEOTEXSTRING_T407B1B1614BB70830C5F7C954965867D199FC48A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.DerVideotexString
struct  DerVideotexString_t407B1B1614BB70830C5F7C954965867D199FC48A  : public DerStringBase_t56EF0CDEB7AF4C8CE9701F3940380FF0EAC26A01
{
public:
	// System.Byte[] Org.BouncyCastle.Asn1.DerVideotexString::mString
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mString_0;

public:
	inline static int32_t get_offset_of_mString_0() { return static_cast<int32_t>(offsetof(DerVideotexString_t407B1B1614BB70830C5F7C954965867D199FC48A, ___mString_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mString_0() const { return ___mString_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mString_0() { return &___mString_0; }
	inline void set_mString_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mString_0 = value;
		Il2CppCodeGenWriteBarrier((&___mString_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERVIDEOTEXSTRING_T407B1B1614BB70830C5F7C954965867D199FC48A_H
#ifndef DERVISIBLESTRING_TE8A25CDDAADC28C2AD79CB7B5B8A10DB93472A10_H
#define DERVISIBLESTRING_TE8A25CDDAADC28C2AD79CB7B5B8A10DB93472A10_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.DerVisibleString
struct  DerVisibleString_tE8A25CDDAADC28C2AD79CB7B5B8A10DB93472A10  : public DerStringBase_t56EF0CDEB7AF4C8CE9701F3940380FF0EAC26A01
{
public:
	// System.String Org.BouncyCastle.Asn1.DerVisibleString::str
	String_t* ___str_0;

public:
	inline static int32_t get_offset_of_str_0() { return static_cast<int32_t>(offsetof(DerVisibleString_tE8A25CDDAADC28C2AD79CB7B5B8A10DB93472A10, ___str_0)); }
	inline String_t* get_str_0() const { return ___str_0; }
	inline String_t** get_address_of_str_0() { return &___str_0; }
	inline void set_str_0(String_t* value)
	{
		___str_0 = value;
		Il2CppCodeGenWriteBarrier((&___str_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERVISIBLESTRING_TE8A25CDDAADC28C2AD79CB7B5B8A10DB93472A10_H
#ifndef LIMITEDINPUTSTREAM_T73C3A42509FEDEF3F9D07B873C54F6B68E407ADC_H
#define LIMITEDINPUTSTREAM_T73C3A42509FEDEF3F9D07B873C54F6B68E407ADC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.LimitedInputStream
struct  LimitedInputStream_t73C3A42509FEDEF3F9D07B873C54F6B68E407ADC  : public BaseInputStream_t391EE4FC5A02C738BAA41F88320C83E7C99061A7
{
public:
	// System.IO.Stream Org.BouncyCastle.Asn1.LimitedInputStream::_in
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ____in_6;
	// System.Int32 Org.BouncyCastle.Asn1.LimitedInputStream::_limit
	int32_t ____limit_7;

public:
	inline static int32_t get_offset_of__in_6() { return static_cast<int32_t>(offsetof(LimitedInputStream_t73C3A42509FEDEF3F9D07B873C54F6B68E407ADC, ____in_6)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get__in_6() const { return ____in_6; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of__in_6() { return &____in_6; }
	inline void set__in_6(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		____in_6 = value;
		Il2CppCodeGenWriteBarrier((&____in_6), value);
	}

	inline static int32_t get_offset_of__limit_7() { return static_cast<int32_t>(offsetof(LimitedInputStream_t73C3A42509FEDEF3F9D07B873C54F6B68E407ADC, ____limit_7)); }
	inline int32_t get__limit_7() const { return ____limit_7; }
	inline int32_t* get_address_of__limit_7() { return &____limit_7; }
	inline void set__limit_7(int32_t value)
	{
		____limit_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIMITEDINPUTSTREAM_T73C3A42509FEDEF3F9D07B873C54F6B68E407ADC_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef VIDEOCLIPPLAYABLE_T4B7997FDB02C74F9E88F37574F0F4F9DE08CCC12_H
#define VIDEOCLIPPLAYABLE_T4B7997FDB02C74F9E88F37574F0F4F9DE08CCC12_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Video.VideoClipPlayable
struct  VideoClipPlayable_t4B7997FDB02C74F9E88F37574F0F4F9DE08CCC12 
{
public:
	// UnityEngine.Playables.PlayableHandle UnityEngine.Experimental.Video.VideoClipPlayable::m_Handle
	PlayableHandle_t9D3B4E540D4413CED81DDD6A24C5373BEFA1D182  ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(VideoClipPlayable_t4B7997FDB02C74F9E88F37574F0F4F9DE08CCC12, ___m_Handle_0)); }
	inline PlayableHandle_t9D3B4E540D4413CED81DDD6A24C5373BEFA1D182  get_m_Handle_0() const { return ___m_Handle_0; }
	inline PlayableHandle_t9D3B4E540D4413CED81DDD6A24C5373BEFA1D182 * get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(PlayableHandle_t9D3B4E540D4413CED81DDD6A24C5373BEFA1D182  value)
	{
		___m_Handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOCLIPPLAYABLE_T4B7997FDB02C74F9E88F37574F0F4F9DE08CCC12_H
#ifndef VIDEOCLIP_TA4039CBBC6F9C3AD62B067964A6C20C6FB7376D5_H
#define VIDEOCLIP_TA4039CBBC6F9C3AD62B067964A6C20C6FB7376D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Video.VideoClip
struct  VideoClip_tA4039CBBC6F9C3AD62B067964A6C20C6FB7376D5  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOCLIP_TA4039CBBC6F9C3AD62B067964A6C20C6FB7376D5_H
#ifndef ASN1OUTPUTSTREAM_T0EE35BDCBBF8956140C93CB7DC6E06084C2CB4CE_H
#define ASN1OUTPUTSTREAM_T0EE35BDCBBF8956140C93CB7DC6E06084C2CB4CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.Asn1OutputStream
struct  Asn1OutputStream_t0EE35BDCBBF8956140C93CB7DC6E06084C2CB4CE  : public DerOutputStream_tEBC38D06077ED0B3F731C084CFE1E0590389C3A4
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASN1OUTPUTSTREAM_T0EE35BDCBBF8956140C93CB7DC6E06084C2CB4CE_H
#ifndef BERBITSTRING_TCF2F7DF4E6782CB4C6BDB280003F88A2F1E9D49F_H
#define BERBITSTRING_TCF2F7DF4E6782CB4C6BDB280003F88A2F1E9D49F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.BerBitString
struct  BerBitString_tCF2F7DF4E6782CB4C6BDB280003F88A2F1E9D49F  : public DerBitString_tCA371BCECC9BA7643D0127424F54EE10C1B19A64
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BERBITSTRING_TCF2F7DF4E6782CB4C6BDB280003F88A2F1E9D49F_H
#ifndef BEROCTETSTRING_TB2BDEA365647209A293CC751E5CE18E76839EF1A_H
#define BEROCTETSTRING_TB2BDEA365647209A293CC751E5CE18E76839EF1A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.BerOctetString
struct  BerOctetString_tB2BDEA365647209A293CC751E5CE18E76839EF1A  : public DerOctetString_t4A1C1733812CD4857D067A49DBF362FECBF84782
{
public:
	// System.Collections.IEnumerable Org.BouncyCastle.Asn1.BerOctetString::octs
	RuntimeObject* ___octs_1;

public:
	inline static int32_t get_offset_of_octs_1() { return static_cast<int32_t>(offsetof(BerOctetString_tB2BDEA365647209A293CC751E5CE18E76839EF1A, ___octs_1)); }
	inline RuntimeObject* get_octs_1() const { return ___octs_1; }
	inline RuntimeObject** get_address_of_octs_1() { return &___octs_1; }
	inline void set_octs_1(RuntimeObject* value)
	{
		___octs_1 = value;
		Il2CppCodeGenWriteBarrier((&___octs_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEROCTETSTRING_TB2BDEA365647209A293CC751E5CE18E76839EF1A_H
#ifndef BEROUTPUTSTREAM_T13E621AC2C0A899FAA6D0AF2A25B18458DB7FFA8_H
#define BEROUTPUTSTREAM_T13E621AC2C0A899FAA6D0AF2A25B18458DB7FFA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.BerOutputStream
struct  BerOutputStream_t13E621AC2C0A899FAA6D0AF2A25B18458DB7FFA8  : public DerOutputStream_tEBC38D06077ED0B3F731C084CFE1E0590389C3A4
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEROUTPUTSTREAM_T13E621AC2C0A899FAA6D0AF2A25B18458DB7FFA8_H
#ifndef BERSEQUENCE_TF95F44F0E4851727D1E8CC3327EA944E423FB0A1_H
#define BERSEQUENCE_TF95F44F0E4851727D1E8CC3327EA944E423FB0A1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.BerSequence
struct  BerSequence_tF95F44F0E4851727D1E8CC3327EA944E423FB0A1  : public DerSequence_tC013954AC20047A131699A1AD9B8899F88B8B811
{
public:

public:
};

struct BerSequence_tF95F44F0E4851727D1E8CC3327EA944E423FB0A1_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.BerSequence Org.BouncyCastle.Asn1.BerSequence::Empty
	BerSequence_tF95F44F0E4851727D1E8CC3327EA944E423FB0A1 * ___Empty_2;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(BerSequence_tF95F44F0E4851727D1E8CC3327EA944E423FB0A1_StaticFields, ___Empty_2)); }
	inline BerSequence_tF95F44F0E4851727D1E8CC3327EA944E423FB0A1 * get_Empty_2() const { return ___Empty_2; }
	inline BerSequence_tF95F44F0E4851727D1E8CC3327EA944E423FB0A1 ** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(BerSequence_tF95F44F0E4851727D1E8CC3327EA944E423FB0A1 * value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BERSEQUENCE_TF95F44F0E4851727D1E8CC3327EA944E423FB0A1_H
#ifndef BERSET_TC45260E37D4B07A18C14BC65682CAC61BA472A3E_H
#define BERSET_TC45260E37D4B07A18C14BC65682CAC61BA472A3E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.BerSet
struct  BerSet_tC45260E37D4B07A18C14BC65682CAC61BA472A3E  : public DerSet_t979F83976CC4AD737C9DE6638EEE89E0A2DE355F
{
public:

public:
};

struct BerSet_tC45260E37D4B07A18C14BC65682CAC61BA472A3E_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.BerSet Org.BouncyCastle.Asn1.BerSet::Empty
	BerSet_tC45260E37D4B07A18C14BC65682CAC61BA472A3E * ___Empty_2;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(BerSet_tC45260E37D4B07A18C14BC65682CAC61BA472A3E_StaticFields, ___Empty_2)); }
	inline BerSet_tC45260E37D4B07A18C14BC65682CAC61BA472A3E * get_Empty_2() const { return ___Empty_2; }
	inline BerSet_tC45260E37D4B07A18C14BC65682CAC61BA472A3E ** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(BerSet_tC45260E37D4B07A18C14BC65682CAC61BA472A3E * value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BERSET_TC45260E37D4B07A18C14BC65682CAC61BA472A3E_H
#ifndef BERTAGGEDOBJECT_TD0E7E44591074770576C171C266CC3B0AA877578_H
#define BERTAGGEDOBJECT_TD0E7E44591074770576C171C266CC3B0AA877578_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.BerTaggedObject
struct  BerTaggedObject_tD0E7E44591074770576C171C266CC3B0AA877578  : public DerTaggedObject_t080B7D91E7F2DEBB0707D74896A566377D841F5F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BERTAGGEDOBJECT_TD0E7E44591074770576C171C266CC3B0AA877578_H
#ifndef DEFINITELENGTHINPUTSTREAM_T9461CF6517E1119EFD75E65D001C0969476E74D1_H
#define DEFINITELENGTHINPUTSTREAM_T9461CF6517E1119EFD75E65D001C0969476E74D1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.DefiniteLengthInputStream
struct  DefiniteLengthInputStream_t9461CF6517E1119EFD75E65D001C0969476E74D1  : public LimitedInputStream_t73C3A42509FEDEF3F9D07B873C54F6B68E407ADC
{
public:
	// System.Int32 Org.BouncyCastle.Asn1.DefiniteLengthInputStream::_originalLength
	int32_t ____originalLength_9;
	// System.Int32 Org.BouncyCastle.Asn1.DefiniteLengthInputStream::_remaining
	int32_t ____remaining_10;

public:
	inline static int32_t get_offset_of__originalLength_9() { return static_cast<int32_t>(offsetof(DefiniteLengthInputStream_t9461CF6517E1119EFD75E65D001C0969476E74D1, ____originalLength_9)); }
	inline int32_t get__originalLength_9() const { return ____originalLength_9; }
	inline int32_t* get_address_of__originalLength_9() { return &____originalLength_9; }
	inline void set__originalLength_9(int32_t value)
	{
		____originalLength_9 = value;
	}

	inline static int32_t get_offset_of__remaining_10() { return static_cast<int32_t>(offsetof(DefiniteLengthInputStream_t9461CF6517E1119EFD75E65D001C0969476E74D1, ____remaining_10)); }
	inline int32_t get__remaining_10() const { return ____remaining_10; }
	inline int32_t* get_address_of__remaining_10() { return &____remaining_10; }
	inline void set__remaining_10(int32_t value)
	{
		____remaining_10 = value;
	}
};

struct DefiniteLengthInputStream_t9461CF6517E1119EFD75E65D001C0969476E74D1_StaticFields
{
public:
	// System.Byte[] Org.BouncyCastle.Asn1.DefiniteLengthInputStream::EmptyBytes
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___EmptyBytes_8;

public:
	inline static int32_t get_offset_of_EmptyBytes_8() { return static_cast<int32_t>(offsetof(DefiniteLengthInputStream_t9461CF6517E1119EFD75E65D001C0969476E74D1_StaticFields, ___EmptyBytes_8)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_EmptyBytes_8() const { return ___EmptyBytes_8; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_EmptyBytes_8() { return &___EmptyBytes_8; }
	inline void set_EmptyBytes_8(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___EmptyBytes_8 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyBytes_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFINITELENGTHINPUTSTREAM_T9461CF6517E1119EFD75E65D001C0969476E74D1_H
#ifndef INDEFINITELENGTHINPUTSTREAM_T36FE2CE5A20117914D5CF3E4524E2A616C8537BB_H
#define INDEFINITELENGTHINPUTSTREAM_T36FE2CE5A20117914D5CF3E4524E2A616C8537BB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.IndefiniteLengthInputStream
struct  IndefiniteLengthInputStream_t36FE2CE5A20117914D5CF3E4524E2A616C8537BB  : public LimitedInputStream_t73C3A42509FEDEF3F9D07B873C54F6B68E407ADC
{
public:
	// System.Int32 Org.BouncyCastle.Asn1.IndefiniteLengthInputStream::_lookAhead
	int32_t ____lookAhead_8;
	// System.Boolean Org.BouncyCastle.Asn1.IndefiniteLengthInputStream::_eofOn00
	bool ____eofOn00_9;

public:
	inline static int32_t get_offset_of__lookAhead_8() { return static_cast<int32_t>(offsetof(IndefiniteLengthInputStream_t36FE2CE5A20117914D5CF3E4524E2A616C8537BB, ____lookAhead_8)); }
	inline int32_t get__lookAhead_8() const { return ____lookAhead_8; }
	inline int32_t* get_address_of__lookAhead_8() { return &____lookAhead_8; }
	inline void set__lookAhead_8(int32_t value)
	{
		____lookAhead_8 = value;
	}

	inline static int32_t get_offset_of__eofOn00_9() { return static_cast<int32_t>(offsetof(IndefiniteLengthInputStream_t36FE2CE5A20117914D5CF3E4524E2A616C8537BB, ____eofOn00_9)); }
	inline bool get__eofOn00_9() const { return ____eofOn00_9; }
	inline bool* get_address_of__eofOn00_9() { return &____eofOn00_9; }
	inline void set__eofOn00_9(bool value)
	{
		____eofOn00_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INDEFINITELENGTHINPUTSTREAM_T36FE2CE5A20117914D5CF3E4524E2A616C8537BB_H
#ifndef NETSCAPECERTTYPE_T9C742E0B655467187C3593E578FF36789B49C006_H
#define NETSCAPECERTTYPE_T9C742E0B655467187C3593E578FF36789B49C006_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.Misc.NetscapeCertType
struct  NetscapeCertType_t9C742E0B655467187C3593E578FF36789B49C006  : public DerBitString_tCA371BCECC9BA7643D0127424F54EE10C1B19A64
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETSCAPECERTTYPE_T9C742E0B655467187C3593E578FF36789B49C006_H
#ifndef NETSCAPEREVOCATIONURL_T8B3C3F349F87B28BCCB80B2278ACAB4446F29C62_H
#define NETSCAPEREVOCATIONURL_T8B3C3F349F87B28BCCB80B2278ACAB4446F29C62_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.Misc.NetscapeRevocationUrl
struct  NetscapeRevocationUrl_t8B3C3F349F87B28BCCB80B2278ACAB4446F29C62  : public DerIA5String_t8BA9BA3EE704517270F77ED3B87506183AB11CA1
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETSCAPEREVOCATIONURL_T8B3C3F349F87B28BCCB80B2278ACAB4446F29C62_H
#ifndef VERISIGNCZAGEXTENSION_T6CE98CD98CCAC7F3BD54ADCE0F7F60FF0A1001D2_H
#define VERISIGNCZAGEXTENSION_T6CE98CD98CCAC7F3BD54ADCE0F7F60FF0A1001D2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.Misc.VerisignCzagExtension
struct  VerisignCzagExtension_t6CE98CD98CCAC7F3BD54ADCE0F7F60FF0A1001D2  : public DerIA5String_t8BA9BA3EE704517270F77ED3B87506183AB11CA1
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERISIGNCZAGEXTENSION_T6CE98CD98CCAC7F3BD54ADCE0F7F60FF0A1001D2_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef ERROREVENTHANDLER_TF5863946928B48BE13146ED5FF70AC92678FE222_H
#define ERROREVENTHANDLER_TF5863946928B48BE13146ED5FF70AC92678FE222_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Video.VideoPlayer_ErrorEventHandler
struct  ErrorEventHandler_tF5863946928B48BE13146ED5FF70AC92678FE222  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ERROREVENTHANDLER_TF5863946928B48BE13146ED5FF70AC92678FE222_H
#ifndef EVENTHANDLER_T5069D72E1ED46BD04F19D8D4534811B95A8E2308_H
#define EVENTHANDLER_T5069D72E1ED46BD04F19D8D4534811B95A8E2308_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Video.VideoPlayer_EventHandler
struct  EventHandler_t5069D72E1ED46BD04F19D8D4534811B95A8E2308  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTHANDLER_T5069D72E1ED46BD04F19D8D4534811B95A8E2308_H
#ifndef FRAMEREADYEVENTHANDLER_T518B277D916AB292680CAA186BCDB3D3EF130422_H
#define FRAMEREADYEVENTHANDLER_T518B277D916AB292680CAA186BCDB3D3EF130422_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Video.VideoPlayer_FrameReadyEventHandler
struct  FrameReadyEventHandler_t518B277D916AB292680CAA186BCDB3D3EF130422  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FRAMEREADYEVENTHANDLER_T518B277D916AB292680CAA186BCDB3D3EF130422_H
#ifndef TIMEEVENTHANDLER_TDD815DAABFADDD98C8993B2A97A2FCE858266BC1_H
#define TIMEEVENTHANDLER_TDD815DAABFADDD98C8993B2A97A2FCE858266BC1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Video.VideoPlayer_TimeEventHandler
struct  TimeEventHandler_tDD815DAABFADDD98C8993B2A97A2FCE858266BC1  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMEEVENTHANDLER_TDD815DAABFADDD98C8993B2A97A2FCE858266BC1_H
#ifndef VIDEOPLAYER_TFC1C27AF83D59A5213B2AC561B43FD7E19FE02F2_H
#define VIDEOPLAYER_TFC1C27AF83D59A5213B2AC561B43FD7E19FE02F2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Video.VideoPlayer
struct  VideoPlayer_tFC1C27AF83D59A5213B2AC561B43FD7E19FE02F2  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:
	// UnityEngine.Video.VideoPlayer_EventHandler UnityEngine.Video.VideoPlayer::prepareCompleted
	EventHandler_t5069D72E1ED46BD04F19D8D4534811B95A8E2308 * ___prepareCompleted_4;
	// UnityEngine.Video.VideoPlayer_EventHandler UnityEngine.Video.VideoPlayer::loopPointReached
	EventHandler_t5069D72E1ED46BD04F19D8D4534811B95A8E2308 * ___loopPointReached_5;
	// UnityEngine.Video.VideoPlayer_EventHandler UnityEngine.Video.VideoPlayer::started
	EventHandler_t5069D72E1ED46BD04F19D8D4534811B95A8E2308 * ___started_6;
	// UnityEngine.Video.VideoPlayer_EventHandler UnityEngine.Video.VideoPlayer::frameDropped
	EventHandler_t5069D72E1ED46BD04F19D8D4534811B95A8E2308 * ___frameDropped_7;
	// UnityEngine.Video.VideoPlayer_ErrorEventHandler UnityEngine.Video.VideoPlayer::errorReceived
	ErrorEventHandler_tF5863946928B48BE13146ED5FF70AC92678FE222 * ___errorReceived_8;
	// UnityEngine.Video.VideoPlayer_EventHandler UnityEngine.Video.VideoPlayer::seekCompleted
	EventHandler_t5069D72E1ED46BD04F19D8D4534811B95A8E2308 * ___seekCompleted_9;
	// UnityEngine.Video.VideoPlayer_TimeEventHandler UnityEngine.Video.VideoPlayer::clockResyncOccurred
	TimeEventHandler_tDD815DAABFADDD98C8993B2A97A2FCE858266BC1 * ___clockResyncOccurred_10;
	// UnityEngine.Video.VideoPlayer_FrameReadyEventHandler UnityEngine.Video.VideoPlayer::frameReady
	FrameReadyEventHandler_t518B277D916AB292680CAA186BCDB3D3EF130422 * ___frameReady_11;

public:
	inline static int32_t get_offset_of_prepareCompleted_4() { return static_cast<int32_t>(offsetof(VideoPlayer_tFC1C27AF83D59A5213B2AC561B43FD7E19FE02F2, ___prepareCompleted_4)); }
	inline EventHandler_t5069D72E1ED46BD04F19D8D4534811B95A8E2308 * get_prepareCompleted_4() const { return ___prepareCompleted_4; }
	inline EventHandler_t5069D72E1ED46BD04F19D8D4534811B95A8E2308 ** get_address_of_prepareCompleted_4() { return &___prepareCompleted_4; }
	inline void set_prepareCompleted_4(EventHandler_t5069D72E1ED46BD04F19D8D4534811B95A8E2308 * value)
	{
		___prepareCompleted_4 = value;
		Il2CppCodeGenWriteBarrier((&___prepareCompleted_4), value);
	}

	inline static int32_t get_offset_of_loopPointReached_5() { return static_cast<int32_t>(offsetof(VideoPlayer_tFC1C27AF83D59A5213B2AC561B43FD7E19FE02F2, ___loopPointReached_5)); }
	inline EventHandler_t5069D72E1ED46BD04F19D8D4534811B95A8E2308 * get_loopPointReached_5() const { return ___loopPointReached_5; }
	inline EventHandler_t5069D72E1ED46BD04F19D8D4534811B95A8E2308 ** get_address_of_loopPointReached_5() { return &___loopPointReached_5; }
	inline void set_loopPointReached_5(EventHandler_t5069D72E1ED46BD04F19D8D4534811B95A8E2308 * value)
	{
		___loopPointReached_5 = value;
		Il2CppCodeGenWriteBarrier((&___loopPointReached_5), value);
	}

	inline static int32_t get_offset_of_started_6() { return static_cast<int32_t>(offsetof(VideoPlayer_tFC1C27AF83D59A5213B2AC561B43FD7E19FE02F2, ___started_6)); }
	inline EventHandler_t5069D72E1ED46BD04F19D8D4534811B95A8E2308 * get_started_6() const { return ___started_6; }
	inline EventHandler_t5069D72E1ED46BD04F19D8D4534811B95A8E2308 ** get_address_of_started_6() { return &___started_6; }
	inline void set_started_6(EventHandler_t5069D72E1ED46BD04F19D8D4534811B95A8E2308 * value)
	{
		___started_6 = value;
		Il2CppCodeGenWriteBarrier((&___started_6), value);
	}

	inline static int32_t get_offset_of_frameDropped_7() { return static_cast<int32_t>(offsetof(VideoPlayer_tFC1C27AF83D59A5213B2AC561B43FD7E19FE02F2, ___frameDropped_7)); }
	inline EventHandler_t5069D72E1ED46BD04F19D8D4534811B95A8E2308 * get_frameDropped_7() const { return ___frameDropped_7; }
	inline EventHandler_t5069D72E1ED46BD04F19D8D4534811B95A8E2308 ** get_address_of_frameDropped_7() { return &___frameDropped_7; }
	inline void set_frameDropped_7(EventHandler_t5069D72E1ED46BD04F19D8D4534811B95A8E2308 * value)
	{
		___frameDropped_7 = value;
		Il2CppCodeGenWriteBarrier((&___frameDropped_7), value);
	}

	inline static int32_t get_offset_of_errorReceived_8() { return static_cast<int32_t>(offsetof(VideoPlayer_tFC1C27AF83D59A5213B2AC561B43FD7E19FE02F2, ___errorReceived_8)); }
	inline ErrorEventHandler_tF5863946928B48BE13146ED5FF70AC92678FE222 * get_errorReceived_8() const { return ___errorReceived_8; }
	inline ErrorEventHandler_tF5863946928B48BE13146ED5FF70AC92678FE222 ** get_address_of_errorReceived_8() { return &___errorReceived_8; }
	inline void set_errorReceived_8(ErrorEventHandler_tF5863946928B48BE13146ED5FF70AC92678FE222 * value)
	{
		___errorReceived_8 = value;
		Il2CppCodeGenWriteBarrier((&___errorReceived_8), value);
	}

	inline static int32_t get_offset_of_seekCompleted_9() { return static_cast<int32_t>(offsetof(VideoPlayer_tFC1C27AF83D59A5213B2AC561B43FD7E19FE02F2, ___seekCompleted_9)); }
	inline EventHandler_t5069D72E1ED46BD04F19D8D4534811B95A8E2308 * get_seekCompleted_9() const { return ___seekCompleted_9; }
	inline EventHandler_t5069D72E1ED46BD04F19D8D4534811B95A8E2308 ** get_address_of_seekCompleted_9() { return &___seekCompleted_9; }
	inline void set_seekCompleted_9(EventHandler_t5069D72E1ED46BD04F19D8D4534811B95A8E2308 * value)
	{
		___seekCompleted_9 = value;
		Il2CppCodeGenWriteBarrier((&___seekCompleted_9), value);
	}

	inline static int32_t get_offset_of_clockResyncOccurred_10() { return static_cast<int32_t>(offsetof(VideoPlayer_tFC1C27AF83D59A5213B2AC561B43FD7E19FE02F2, ___clockResyncOccurred_10)); }
	inline TimeEventHandler_tDD815DAABFADDD98C8993B2A97A2FCE858266BC1 * get_clockResyncOccurred_10() const { return ___clockResyncOccurred_10; }
	inline TimeEventHandler_tDD815DAABFADDD98C8993B2A97A2FCE858266BC1 ** get_address_of_clockResyncOccurred_10() { return &___clockResyncOccurred_10; }
	inline void set_clockResyncOccurred_10(TimeEventHandler_tDD815DAABFADDD98C8993B2A97A2FCE858266BC1 * value)
	{
		___clockResyncOccurred_10 = value;
		Il2CppCodeGenWriteBarrier((&___clockResyncOccurred_10), value);
	}

	inline static int32_t get_offset_of_frameReady_11() { return static_cast<int32_t>(offsetof(VideoPlayer_tFC1C27AF83D59A5213B2AC561B43FD7E19FE02F2, ___frameReady_11)); }
	inline FrameReadyEventHandler_t518B277D916AB292680CAA186BCDB3D3EF130422 * get_frameReady_11() const { return ___frameReady_11; }
	inline FrameReadyEventHandler_t518B277D916AB292680CAA186BCDB3D3EF130422 ** get_address_of_frameReady_11() { return &___frameReady_11; }
	inline void set_frameReady_11(FrameReadyEventHandler_t518B277D916AB292680CAA186BCDB3D3EF130422 * value)
	{
		___frameReady_11 = value;
		Il2CppCodeGenWriteBarrier((&___frameReady_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOPLAYER_TFC1C27AF83D59A5213B2AC561B43FD7E19FE02F2_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4800 = { sizeof (VideoClip_tA4039CBBC6F9C3AD62B067964A6C20C6FB7376D5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4801 = { sizeof (VideoClipPlayable_t4B7997FDB02C74F9E88F37574F0F4F9DE08CCC12)+ sizeof (RuntimeObject), sizeof(VideoClipPlayable_t4B7997FDB02C74F9E88F37574F0F4F9DE08CCC12 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4801[1] = 
{
	VideoClipPlayable_t4B7997FDB02C74F9E88F37574F0F4F9DE08CCC12::get_offset_of_m_Handle_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4802 = { sizeof (VideoRenderMode_t0DBAABB576FDA890C49C6AD3EE641623F93E9161)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4802[6] = 
{
	VideoRenderMode_t0DBAABB576FDA890C49C6AD3EE641623F93E9161::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4803 = { sizeof (Video3DLayout_t5F64D0CE5E9B37C2FCE67F397FA5CFE9C047E4A1)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4803[4] = 
{
	Video3DLayout_t5F64D0CE5E9B37C2FCE67F397FA5CFE9C047E4A1::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4804 = { sizeof (VideoAspectRatio_t5739968D28C4F8F802B085E293F22110205B8379)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4804[7] = 
{
	VideoAspectRatio_t5739968D28C4F8F802B085E293F22110205B8379::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4805 = { sizeof (VideoTimeSource_t15F04FD6B3D75A8D98480E8B77117C0FF691BB77)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4805[3] = 
{
	VideoTimeSource_t15F04FD6B3D75A8D98480E8B77117C0FF691BB77::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4806 = { sizeof (VideoTimeReference_t9EAEBD354AE5E56F0D0F36E73A428BB2A0B8B31B)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4806[4] = 
{
	VideoTimeReference_t9EAEBD354AE5E56F0D0F36E73A428BB2A0B8B31B::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4807 = { sizeof (VideoSource_t32501B57EA7F9CF835FBA8184C9AF427CBBEFD0A)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4807[3] = 
{
	VideoSource_t32501B57EA7F9CF835FBA8184C9AF427CBBEFD0A::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4808 = { sizeof (VideoAudioOutputMode_t8CDE10B382F3C321345EC57C9164A9177139DC6F)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4808[5] = 
{
	VideoAudioOutputMode_t8CDE10B382F3C321345EC57C9164A9177139DC6F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4809 = { sizeof (VideoPlayer_tFC1C27AF83D59A5213B2AC561B43FD7E19FE02F2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4809[8] = 
{
	VideoPlayer_tFC1C27AF83D59A5213B2AC561B43FD7E19FE02F2::get_offset_of_prepareCompleted_4(),
	VideoPlayer_tFC1C27AF83D59A5213B2AC561B43FD7E19FE02F2::get_offset_of_loopPointReached_5(),
	VideoPlayer_tFC1C27AF83D59A5213B2AC561B43FD7E19FE02F2::get_offset_of_started_6(),
	VideoPlayer_tFC1C27AF83D59A5213B2AC561B43FD7E19FE02F2::get_offset_of_frameDropped_7(),
	VideoPlayer_tFC1C27AF83D59A5213B2AC561B43FD7E19FE02F2::get_offset_of_errorReceived_8(),
	VideoPlayer_tFC1C27AF83D59A5213B2AC561B43FD7E19FE02F2::get_offset_of_seekCompleted_9(),
	VideoPlayer_tFC1C27AF83D59A5213B2AC561B43FD7E19FE02F2::get_offset_of_clockResyncOccurred_10(),
	VideoPlayer_tFC1C27AF83D59A5213B2AC561B43FD7E19FE02F2::get_offset_of_frameReady_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4810 = { sizeof (EventHandler_t5069D72E1ED46BD04F19D8D4534811B95A8E2308), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4811 = { sizeof (ErrorEventHandler_tF5863946928B48BE13146ED5FF70AC92678FE222), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4812 = { sizeof (FrameReadyEventHandler_t518B277D916AB292680CAA186BCDB3D3EF130422), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4813 = { sizeof (TimeEventHandler_tDD815DAABFADDD98C8993B2A97A2FCE858266BC1), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4814 = { sizeof (U3CModuleU3E_t25A4C93C391400B6C1AF0A8BEAB317DE8B1AD521), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4815 = { sizeof (BerBitString_tCF2F7DF4E6782CB4C6BDB280003F88A2F1E9D49F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4816 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4817 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4818 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4819 = { sizeof (Asn1StreamParser_t2FE3FB89834CCEE990CDD0F38B5DD0960CDC9C90), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4819[3] = 
{
	Asn1StreamParser_t2FE3FB89834CCEE990CDD0F38B5DD0960CDC9C90::get_offset_of__in_0(),
	Asn1StreamParser_t2FE3FB89834CCEE990CDD0F38B5DD0960CDC9C90::get_offset_of__limit_1(),
	Asn1StreamParser_t2FE3FB89834CCEE990CDD0F38B5DD0960CDC9C90::get_offset_of_tmpBuffers_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4820 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4821 = { sizeof (Asn1Encodable_t390FCE82F5F52E41A7320BBF2E8BF18DA1622A8B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4822 = { sizeof (Asn1EncodableVector_tABDFD1A771D989CF6BB9F956783729884A2BB34F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4822[1] = 
{
	Asn1EncodableVector_tABDFD1A771D989CF6BB9F956783729884A2BB34F::get_offset_of_v_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4823 = { sizeof (Asn1Exception_t719F7048E3DB1600F42B61F3F25501A5CDB1B123), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4824 = { sizeof (Asn1InputStream_tA178A8C2AB75A5D4506835E71C3A4CAB02C9E17A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4824[2] = 
{
	Asn1InputStream_tA178A8C2AB75A5D4506835E71C3A4CAB02C9E17A::get_offset_of_limit_6(),
	Asn1InputStream_tA178A8C2AB75A5D4506835E71C3A4CAB02C9E17A::get_offset_of_tmpBuffers_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4825 = { sizeof (Asn1Null_t9AC6DDB8C5501D354D5550F4B7674F94B3411DF7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4826 = { sizeof (Asn1Object_tB780E50883209D5C975E83B3D16257DFC3CCE36D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4827 = { sizeof (Asn1OctetString_t2088F9E88E4C97BB9CF3B2FFC53CFBAF9B8B79CB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4827[1] = 
{
	Asn1OctetString_t2088F9E88E4C97BB9CF3B2FFC53CFBAF9B8B79CB::get_offset_of_str_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4828 = { sizeof (Asn1OutputStream_t0EE35BDCBBF8956140C93CB7DC6E06084C2CB4CE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4829 = { sizeof (Asn1ParsingException_t47AE48198AC29ED9C016413382AD7D1ED8722E43), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4830 = { sizeof (Asn1Sequence_t292AE1C416511A0DE153DE285AEE498B50523B85), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4830[1] = 
{
	Asn1Sequence_t292AE1C416511A0DE153DE285AEE498B50523B85::get_offset_of_seq_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4831 = { sizeof (Asn1Set_tD51325409687C84A095F7692B251BDE783A1A4A7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4831[1] = 
{
	Asn1Set_tD51325409687C84A095F7692B251BDE783A1A4A7::get_offset_of__set_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4832 = { sizeof (DerComparer_tA00AC1811F1B46623B9BF6E2813C6914B8808A4C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4833 = { sizeof (Asn1TaggedObject_t14E9E19978062973ABA220F78322FDDCEFC39B57), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4833[3] = 
{
	Asn1TaggedObject_t14E9E19978062973ABA220F78322FDDCEFC39B57::get_offset_of_tagNo_0(),
	Asn1TaggedObject_t14E9E19978062973ABA220F78322FDDCEFC39B57::get_offset_of_explicitly_1(),
	Asn1TaggedObject_t14E9E19978062973ABA220F78322FDDCEFC39B57::get_offset_of_obj_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4834 = { sizeof (BerOctetStringParser_t70D00245C14BA6E5384FB87FF87ACAEDF900ABFB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4834[1] = 
{
	BerOctetStringParser_t70D00245C14BA6E5384FB87FF87ACAEDF900ABFB::get_offset_of__parser_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4835 = { sizeof (BerSequenceParser_t495DBD16AAB2689118528D757B8486C68765081B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4835[1] = 
{
	BerSequenceParser_t495DBD16AAB2689118528D757B8486C68765081B::get_offset_of__parser_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4836 = { sizeof (BerSetParser_tA6DDD6EB5C5DE7D9185780BBBA0DD441D79D5632), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4836[1] = 
{
	BerSetParser_tA6DDD6EB5C5DE7D9185780BBBA0DD441D79D5632::get_offset_of__parser_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4837 = { sizeof (BerTaggedObjectParser_t4146B8C0B583D98889266329D1700101EA287607), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4837[3] = 
{
	BerTaggedObjectParser_t4146B8C0B583D98889266329D1700101EA287607::get_offset_of__constructed_0(),
	BerTaggedObjectParser_t4146B8C0B583D98889266329D1700101EA287607::get_offset_of__tagNumber_1(),
	BerTaggedObjectParser_t4146B8C0B583D98889266329D1700101EA287607::get_offset_of__parser_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4838 = { sizeof (BerApplicationSpecific_t1B612BEE42DD61691B140761B80F136FCB8D6F8D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4839 = { sizeof (BerApplicationSpecificParser_tB09D0EF6489FA446DF94E1CA6E40DB0FAAD90997), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4839[2] = 
{
	BerApplicationSpecificParser_tB09D0EF6489FA446DF94E1CA6E40DB0FAAD90997::get_offset_of_tag_0(),
	BerApplicationSpecificParser_tB09D0EF6489FA446DF94E1CA6E40DB0FAAD90997::get_offset_of_parser_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4840 = { sizeof (BerOctetString_tB2BDEA365647209A293CC751E5CE18E76839EF1A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4840[1] = 
{
	BerOctetString_tB2BDEA365647209A293CC751E5CE18E76839EF1A::get_offset_of_octs_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4841 = { sizeof (BerOutputStream_t13E621AC2C0A899FAA6D0AF2A25B18458DB7FFA8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4842 = { sizeof (BerSequence_tF95F44F0E4851727D1E8CC3327EA944E423FB0A1), -1, sizeof(BerSequence_tF95F44F0E4851727D1E8CC3327EA944E423FB0A1_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4842[1] = 
{
	BerSequence_tF95F44F0E4851727D1E8CC3327EA944E423FB0A1_StaticFields::get_offset_of_Empty_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4843 = { sizeof (BerSet_tC45260E37D4B07A18C14BC65682CAC61BA472A3E), -1, sizeof(BerSet_tC45260E37D4B07A18C14BC65682CAC61BA472A3E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4843[1] = 
{
	BerSet_tC45260E37D4B07A18C14BC65682CAC61BA472A3E_StaticFields::get_offset_of_Empty_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4844 = { sizeof (BerTaggedObject_tD0E7E44591074770576C171C266CC3B0AA877578), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4845 = { sizeof (ConstructedOctetStream_t7A51325971607AF7EE9FF10F000BBDBD5B245BF8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4845[3] = 
{
	ConstructedOctetStream_t7A51325971607AF7EE9FF10F000BBDBD5B245BF8::get_offset_of__parser_6(),
	ConstructedOctetStream_t7A51325971607AF7EE9FF10F000BBDBD5B245BF8::get_offset_of__first_7(),
	ConstructedOctetStream_t7A51325971607AF7EE9FF10F000BBDBD5B245BF8::get_offset_of__currentStream_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4846 = { sizeof (DerExternal_t0D40AE43FB75A047DB8035C40D8601F20DC35F5C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4846[5] = 
{
	DerExternal_t0D40AE43FB75A047DB8035C40D8601F20DC35F5C::get_offset_of_directReference_0(),
	DerExternal_t0D40AE43FB75A047DB8035C40D8601F20DC35F5C::get_offset_of_indirectReference_1(),
	DerExternal_t0D40AE43FB75A047DB8035C40D8601F20DC35F5C::get_offset_of_dataValueDescriptor_2(),
	DerExternal_t0D40AE43FB75A047DB8035C40D8601F20DC35F5C::get_offset_of_encoding_3(),
	DerExternal_t0D40AE43FB75A047DB8035C40D8601F20DC35F5C::get_offset_of_externalContent_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4847 = { sizeof (DerExternalParser_tE05E2163A7E760BAFC8F526300708CB7B931653C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4847[1] = 
{
	DerExternalParser_tE05E2163A7E760BAFC8F526300708CB7B931653C::get_offset_of__parser_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4848 = { sizeof (DerOctetStringParser_t903FF756D87E0AFCD73B346F9CC472474C960453), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4848[1] = 
{
	DerOctetStringParser_t903FF756D87E0AFCD73B346F9CC472474C960453::get_offset_of_stream_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4849 = { sizeof (DerSequenceParser_t3972E45BFD6EDFD9B205AD8D02D28A90FBA637CD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4849[1] = 
{
	DerSequenceParser_t3972E45BFD6EDFD9B205AD8D02D28A90FBA637CD::get_offset_of__parser_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4850 = { sizeof (DerSetParser_t22F843F89A112F11B8ED89F2804AEA97C6FABD73), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4850[1] = 
{
	DerSetParser_t22F843F89A112F11B8ED89F2804AEA97C6FABD73::get_offset_of__parser_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4851 = { sizeof (DefiniteLengthInputStream_t9461CF6517E1119EFD75E65D001C0969476E74D1), -1, sizeof(DefiniteLengthInputStream_t9461CF6517E1119EFD75E65D001C0969476E74D1_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4851[3] = 
{
	DefiniteLengthInputStream_t9461CF6517E1119EFD75E65D001C0969476E74D1_StaticFields::get_offset_of_EmptyBytes_8(),
	DefiniteLengthInputStream_t9461CF6517E1119EFD75E65D001C0969476E74D1::get_offset_of__originalLength_9(),
	DefiniteLengthInputStream_t9461CF6517E1119EFD75E65D001C0969476E74D1::get_offset_of__remaining_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4852 = { sizeof (DerApplicationSpecific_t172FBAA17F6C55C0E6B5999D6AB82A8773B75DEA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4852[3] = 
{
	DerApplicationSpecific_t172FBAA17F6C55C0E6B5999D6AB82A8773B75DEA::get_offset_of_isConstructed_0(),
	DerApplicationSpecific_t172FBAA17F6C55C0E6B5999D6AB82A8773B75DEA::get_offset_of_tag_1(),
	DerApplicationSpecific_t172FBAA17F6C55C0E6B5999D6AB82A8773B75DEA::get_offset_of_octets_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4853 = { sizeof (DerBmpString_t05DD26A4899B75AE508EFB316660C76CFA263CDB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4853[1] = 
{
	DerBmpString_t05DD26A4899B75AE508EFB316660C76CFA263CDB::get_offset_of_str_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4854 = { sizeof (DerBitString_tCA371BCECC9BA7643D0127424F54EE10C1B19A64), -1, sizeof(DerBitString_tCA371BCECC9BA7643D0127424F54EE10C1B19A64_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4854[3] = 
{
	DerBitString_tCA371BCECC9BA7643D0127424F54EE10C1B19A64_StaticFields::get_offset_of_table_0(),
	DerBitString_tCA371BCECC9BA7643D0127424F54EE10C1B19A64::get_offset_of_mData_1(),
	DerBitString_tCA371BCECC9BA7643D0127424F54EE10C1B19A64::get_offset_of_mPadBits_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4855 = { sizeof (DerBoolean_tAF849A79A0BCE5C3F0DBB8BE9F6BA279E3D1A8CE), -1, sizeof(DerBoolean_tAF849A79A0BCE5C3F0DBB8BE9F6BA279E3D1A8CE_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4855[3] = 
{
	DerBoolean_tAF849A79A0BCE5C3F0DBB8BE9F6BA279E3D1A8CE::get_offset_of_value_0(),
	DerBoolean_tAF849A79A0BCE5C3F0DBB8BE9F6BA279E3D1A8CE_StaticFields::get_offset_of_False_1(),
	DerBoolean_tAF849A79A0BCE5C3F0DBB8BE9F6BA279E3D1A8CE_StaticFields::get_offset_of_True_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4856 = { sizeof (DerEnumerated_tA6A732BA209AA4B10E55871BB22A8ACC7CFBCEC0), -1, sizeof(DerEnumerated_tA6A732BA209AA4B10E55871BB22A8ACC7CFBCEC0_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4856[2] = 
{
	DerEnumerated_tA6A732BA209AA4B10E55871BB22A8ACC7CFBCEC0::get_offset_of_bytes_0(),
	DerEnumerated_tA6A732BA209AA4B10E55871BB22A8ACC7CFBCEC0_StaticFields::get_offset_of_cache_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4857 = { sizeof (DerGeneralString_t7C07A4AAAB11F833D0D50FF091D1AC2F10F2EEF6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4857[1] = 
{
	DerGeneralString_t7C07A4AAAB11F833D0D50FF091D1AC2F10F2EEF6::get_offset_of_str_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4858 = { sizeof (DerGeneralizedTime_t1694B0E0C14EFC3A2FFC46DD64F5D8D6B55599E9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4858[1] = 
{
	DerGeneralizedTime_t1694B0E0C14EFC3A2FFC46DD64F5D8D6B55599E9::get_offset_of_time_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4859 = { sizeof (DerGraphicString_t289E34555383228BE834CF3D6BD83FD1C9D9BEFF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4859[1] = 
{
	DerGraphicString_t289E34555383228BE834CF3D6BD83FD1C9D9BEFF::get_offset_of_mString_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4860 = { sizeof (DerIA5String_t8BA9BA3EE704517270F77ED3B87506183AB11CA1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4860[1] = 
{
	DerIA5String_t8BA9BA3EE704517270F77ED3B87506183AB11CA1::get_offset_of_str_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4861 = { sizeof (DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4861[1] = 
{
	DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A::get_offset_of_bytes_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4862 = { sizeof (DerNull_tC71A4E14BB19051D3E11138674CEF9FFAB1F3ABD), -1, sizeof(DerNull_tC71A4E14BB19051D3E11138674CEF9FFAB1F3ABD_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4862[2] = 
{
	DerNull_tC71A4E14BB19051D3E11138674CEF9FFAB1F3ABD_StaticFields::get_offset_of_Instance_0(),
	DerNull_tC71A4E14BB19051D3E11138674CEF9FFAB1F3ABD::get_offset_of_zeroBytes_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4863 = { sizeof (DerNumericString_t844038ADFA3B55C739B25A1A39AF888B36F7EF1C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4863[1] = 
{
	DerNumericString_t844038ADFA3B55C739B25A1A39AF888B36F7EF1C::get_offset_of_str_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4864 = { sizeof (DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441), -1, sizeof(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4864[3] = 
{
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441::get_offset_of_identifier_0(),
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441::get_offset_of_body_1(),
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441_StaticFields::get_offset_of_cache_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4865 = { sizeof (DerOctetString_t4A1C1733812CD4857D067A49DBF362FECBF84782), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4866 = { sizeof (DerOutputStream_tEBC38D06077ED0B3F731C084CFE1E0590389C3A4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4867 = { sizeof (DerPrintableString_t379C98FE6054A375C307F97D18CF2BBA040FAA1E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4867[1] = 
{
	DerPrintableString_t379C98FE6054A375C307F97D18CF2BBA040FAA1E::get_offset_of_str_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4868 = { sizeof (DerSequence_tC013954AC20047A131699A1AD9B8899F88B8B811), -1, sizeof(DerSequence_tC013954AC20047A131699A1AD9B8899F88B8B811_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4868[1] = 
{
	DerSequence_tC013954AC20047A131699A1AD9B8899F88B8B811_StaticFields::get_offset_of_Empty_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4869 = { sizeof (DerSet_t979F83976CC4AD737C9DE6638EEE89E0A2DE355F), -1, sizeof(DerSet_t979F83976CC4AD737C9DE6638EEE89E0A2DE355F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4869[1] = 
{
	DerSet_t979F83976CC4AD737C9DE6638EEE89E0A2DE355F_StaticFields::get_offset_of_Empty_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4870 = { sizeof (DerStringBase_t56EF0CDEB7AF4C8CE9701F3940380FF0EAC26A01), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4871 = { sizeof (DerT61String_t22B685156B3CF7EDA32775AB1370E673BE0DF378), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4871[1] = 
{
	DerT61String_t22B685156B3CF7EDA32775AB1370E673BE0DF378::get_offset_of_str_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4872 = { sizeof (DerTaggedObject_t080B7D91E7F2DEBB0707D74896A566377D841F5F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4873 = { sizeof (DerUtcTime_t2E017A28117C3C01BCC8DD70844E080FCBB0A896), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4873[1] = 
{
	DerUtcTime_t2E017A28117C3C01BCC8DD70844E080FCBB0A896::get_offset_of_time_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4874 = { sizeof (DerUtf8String_t35F781218A4AFEBE1AEAD46D8ECC6C899AC73EF6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4874[1] = 
{
	DerUtf8String_t35F781218A4AFEBE1AEAD46D8ECC6C899AC73EF6::get_offset_of_str_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4875 = { sizeof (DerUniversalString_tDAAC3796CE023B0E4FF54837DF2CEB46FDF259CF), -1, sizeof(DerUniversalString_tDAAC3796CE023B0E4FF54837DF2CEB46FDF259CF_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4875[2] = 
{
	DerUniversalString_tDAAC3796CE023B0E4FF54837DF2CEB46FDF259CF_StaticFields::get_offset_of_table_0(),
	DerUniversalString_tDAAC3796CE023B0E4FF54837DF2CEB46FDF259CF::get_offset_of_str_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4876 = { sizeof (DerVideotexString_t407B1B1614BB70830C5F7C954965867D199FC48A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4876[1] = 
{
	DerVideotexString_t407B1B1614BB70830C5F7C954965867D199FC48A::get_offset_of_mString_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4877 = { sizeof (DerVisibleString_tE8A25CDDAADC28C2AD79CB7B5B8A10DB93472A10), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4877[1] = 
{
	DerVisibleString_tE8A25CDDAADC28C2AD79CB7B5B8A10DB93472A10::get_offset_of_str_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4878 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4879 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4880 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4881 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4882 = { sizeof (IndefiniteLengthInputStream_t36FE2CE5A20117914D5CF3E4524E2A616C8537BB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4882[2] = 
{
	IndefiniteLengthInputStream_t36FE2CE5A20117914D5CF3E4524E2A616C8537BB::get_offset_of__lookAhead_8(),
	IndefiniteLengthInputStream_t36FE2CE5A20117914D5CF3E4524E2A616C8537BB::get_offset_of__eofOn00_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4883 = { sizeof (LimitedInputStream_t73C3A42509FEDEF3F9D07B873C54F6B68E407ADC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4883[2] = 
{
	LimitedInputStream_t73C3A42509FEDEF3F9D07B873C54F6B68E407ADC::get_offset_of__in_6(),
	LimitedInputStream_t73C3A42509FEDEF3F9D07B873C54F6B68E407ADC::get_offset_of__limit_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4884 = { sizeof (OidTokenizer_t03CBB940F9BDDC36B196B95EC928E4FFBD15DC3A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4884[2] = 
{
	OidTokenizer_t03CBB940F9BDDC36B196B95EC928E4FFBD15DC3A::get_offset_of_oid_0(),
	OidTokenizer_t03CBB940F9BDDC36B196B95EC928E4FFBD15DC3A::get_offset_of_index_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4885 = { sizeof (AnssiNamedCurves_t96455013467BD6208CE1B457D05FFA940B09555B), -1, sizeof(AnssiNamedCurves_t96455013467BD6208CE1B457D05FFA940B09555B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4885[3] = 
{
	AnssiNamedCurves_t96455013467BD6208CE1B457D05FFA940B09555B_StaticFields::get_offset_of_objIds_0(),
	AnssiNamedCurves_t96455013467BD6208CE1B457D05FFA940B09555B_StaticFields::get_offset_of_curves_1(),
	AnssiNamedCurves_t96455013467BD6208CE1B457D05FFA940B09555B_StaticFields::get_offset_of_names_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4886 = { sizeof (Frp256v1Holder_t9655688B104110C311CCA3E65F1498CD2FEB2EA3), -1, sizeof(Frp256v1Holder_t9655688B104110C311CCA3E65F1498CD2FEB2EA3_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4886[1] = 
{
	Frp256v1Holder_t9655688B104110C311CCA3E65F1498CD2FEB2EA3_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4887 = { sizeof (AnssiObjectIdentifiers_tCBD0B6EE2A518136CC188003673CA647860AA470), -1, sizeof(AnssiObjectIdentifiers_tCBD0B6EE2A518136CC188003673CA647860AA470_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4887[1] = 
{
	AnssiObjectIdentifiers_tCBD0B6EE2A518136CC188003673CA647860AA470_StaticFields::get_offset_of_FRP256v1_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4888 = { sizeof (CryptoProObjectIdentifiers_t6FBDFC987F7A8828381DCDC802E2CB1EC3A258B1), -1, sizeof(CryptoProObjectIdentifiers_t6FBDFC987F7A8828381DCDC802E2CB1EC3A258B1_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4888[23] = 
{
	CryptoProObjectIdentifiers_t6FBDFC987F7A8828381DCDC802E2CB1EC3A258B1_StaticFields::get_offset_of_GostR3411_0(),
	CryptoProObjectIdentifiers_t6FBDFC987F7A8828381DCDC802E2CB1EC3A258B1_StaticFields::get_offset_of_GostR3411Hmac_1(),
	CryptoProObjectIdentifiers_t6FBDFC987F7A8828381DCDC802E2CB1EC3A258B1_StaticFields::get_offset_of_GostR28147Cbc_2(),
	CryptoProObjectIdentifiers_t6FBDFC987F7A8828381DCDC802E2CB1EC3A258B1_StaticFields::get_offset_of_ID_Gost28147_89_CryptoPro_A_ParamSet_3(),
	CryptoProObjectIdentifiers_t6FBDFC987F7A8828381DCDC802E2CB1EC3A258B1_StaticFields::get_offset_of_GostR3410x94_4(),
	CryptoProObjectIdentifiers_t6FBDFC987F7A8828381DCDC802E2CB1EC3A258B1_StaticFields::get_offset_of_GostR3410x2001_5(),
	CryptoProObjectIdentifiers_t6FBDFC987F7A8828381DCDC802E2CB1EC3A258B1_StaticFields::get_offset_of_GostR3411x94WithGostR3410x94_6(),
	CryptoProObjectIdentifiers_t6FBDFC987F7A8828381DCDC802E2CB1EC3A258B1_StaticFields::get_offset_of_GostR3411x94WithGostR3410x2001_7(),
	CryptoProObjectIdentifiers_t6FBDFC987F7A8828381DCDC802E2CB1EC3A258B1_StaticFields::get_offset_of_GostR3411x94CryptoProParamSet_8(),
	CryptoProObjectIdentifiers_t6FBDFC987F7A8828381DCDC802E2CB1EC3A258B1_StaticFields::get_offset_of_GostR3410x94CryptoProA_9(),
	CryptoProObjectIdentifiers_t6FBDFC987F7A8828381DCDC802E2CB1EC3A258B1_StaticFields::get_offset_of_GostR3410x94CryptoProB_10(),
	CryptoProObjectIdentifiers_t6FBDFC987F7A8828381DCDC802E2CB1EC3A258B1_StaticFields::get_offset_of_GostR3410x94CryptoProC_11(),
	CryptoProObjectIdentifiers_t6FBDFC987F7A8828381DCDC802E2CB1EC3A258B1_StaticFields::get_offset_of_GostR3410x94CryptoProD_12(),
	CryptoProObjectIdentifiers_t6FBDFC987F7A8828381DCDC802E2CB1EC3A258B1_StaticFields::get_offset_of_GostR3410x94CryptoProXchA_13(),
	CryptoProObjectIdentifiers_t6FBDFC987F7A8828381DCDC802E2CB1EC3A258B1_StaticFields::get_offset_of_GostR3410x94CryptoProXchB_14(),
	CryptoProObjectIdentifiers_t6FBDFC987F7A8828381DCDC802E2CB1EC3A258B1_StaticFields::get_offset_of_GostR3410x94CryptoProXchC_15(),
	CryptoProObjectIdentifiers_t6FBDFC987F7A8828381DCDC802E2CB1EC3A258B1_StaticFields::get_offset_of_GostR3410x2001CryptoProA_16(),
	CryptoProObjectIdentifiers_t6FBDFC987F7A8828381DCDC802E2CB1EC3A258B1_StaticFields::get_offset_of_GostR3410x2001CryptoProB_17(),
	CryptoProObjectIdentifiers_t6FBDFC987F7A8828381DCDC802E2CB1EC3A258B1_StaticFields::get_offset_of_GostR3410x2001CryptoProC_18(),
	CryptoProObjectIdentifiers_t6FBDFC987F7A8828381DCDC802E2CB1EC3A258B1_StaticFields::get_offset_of_GostR3410x2001CryptoProXchA_19(),
	CryptoProObjectIdentifiers_t6FBDFC987F7A8828381DCDC802E2CB1EC3A258B1_StaticFields::get_offset_of_GostR3410x2001CryptoProXchB_20(),
	CryptoProObjectIdentifiers_t6FBDFC987F7A8828381DCDC802E2CB1EC3A258B1_StaticFields::get_offset_of_GostElSgDH3410Default_21(),
	CryptoProObjectIdentifiers_t6FBDFC987F7A8828381DCDC802E2CB1EC3A258B1_StaticFields::get_offset_of_GostElSgDH3410x1_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4889 = { sizeof (ECGost3410NamedCurves_t0C88B00B67BA085050DE1BCCC8BAEF2A7CAC6C39), -1, sizeof(ECGost3410NamedCurves_t0C88B00B67BA085050DE1BCCC8BAEF2A7CAC6C39_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4889[3] = 
{
	ECGost3410NamedCurves_t0C88B00B67BA085050DE1BCCC8BAEF2A7CAC6C39_StaticFields::get_offset_of_objIds_0(),
	ECGost3410NamedCurves_t0C88B00B67BA085050DE1BCCC8BAEF2A7CAC6C39_StaticFields::get_offset_of_parameters_1(),
	ECGost3410NamedCurves_t0C88B00B67BA085050DE1BCCC8BAEF2A7CAC6C39_StaticFields::get_offset_of_names_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4890 = { sizeof (Gost3410NamedParameters_tF61B5B4918143C6E3604A67AA4F4E55928F83FA5), -1, sizeof(Gost3410NamedParameters_tF61B5B4918143C6E3604A67AA4F4E55928F83FA5_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4890[5] = 
{
	Gost3410NamedParameters_tF61B5B4918143C6E3604A67AA4F4E55928F83FA5_StaticFields::get_offset_of_objIds_0(),
	Gost3410NamedParameters_tF61B5B4918143C6E3604A67AA4F4E55928F83FA5_StaticFields::get_offset_of_parameters_1(),
	Gost3410NamedParameters_tF61B5B4918143C6E3604A67AA4F4E55928F83FA5_StaticFields::get_offset_of_cryptoProA_2(),
	Gost3410NamedParameters_tF61B5B4918143C6E3604A67AA4F4E55928F83FA5_StaticFields::get_offset_of_cryptoProB_3(),
	Gost3410NamedParameters_tF61B5B4918143C6E3604A67AA4F4E55928F83FA5_StaticFields::get_offset_of_cryptoProXchA_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4891 = { sizeof (Gost3410ParamSetParameters_tA09B7D79E4C03898F7D5F3AB8C9A8A061B69E6C2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4891[4] = 
{
	Gost3410ParamSetParameters_tA09B7D79E4C03898F7D5F3AB8C9A8A061B69E6C2::get_offset_of_keySize_0(),
	Gost3410ParamSetParameters_tA09B7D79E4C03898F7D5F3AB8C9A8A061B69E6C2::get_offset_of_p_1(),
	Gost3410ParamSetParameters_tA09B7D79E4C03898F7D5F3AB8C9A8A061B69E6C2::get_offset_of_q_2(),
	Gost3410ParamSetParameters_tA09B7D79E4C03898F7D5F3AB8C9A8A061B69E6C2::get_offset_of_a_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4892 = { sizeof (Gost3410PublicKeyAlgParameters_tBE2739ED2E3FF2B3DF0D7C073753194550175243), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4892[3] = 
{
	Gost3410PublicKeyAlgParameters_tBE2739ED2E3FF2B3DF0D7C073753194550175243::get_offset_of_publicKeyParamSet_0(),
	Gost3410PublicKeyAlgParameters_tBE2739ED2E3FF2B3DF0D7C073753194550175243::get_offset_of_digestParamSet_1(),
	Gost3410PublicKeyAlgParameters_tBE2739ED2E3FF2B3DF0D7C073753194550175243::get_offset_of_encryptionParamSet_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4893 = { sizeof (IanaObjectIdentifiers_t30772100C27C8AC123FC767639073FDD8925DBE3), -1, sizeof(IanaObjectIdentifiers_t30772100C27C8AC123FC767639073FDD8925DBE3_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4893[5] = 
{
	IanaObjectIdentifiers_t30772100C27C8AC123FC767639073FDD8925DBE3_StaticFields::get_offset_of_IsakmpOakley_0(),
	IanaObjectIdentifiers_t30772100C27C8AC123FC767639073FDD8925DBE3_StaticFields::get_offset_of_HmacMD5_1(),
	IanaObjectIdentifiers_t30772100C27C8AC123FC767639073FDD8925DBE3_StaticFields::get_offset_of_HmacSha1_2(),
	IanaObjectIdentifiers_t30772100C27C8AC123FC767639073FDD8925DBE3_StaticFields::get_offset_of_HmacTiger_3(),
	IanaObjectIdentifiers_t30772100C27C8AC123FC767639073FDD8925DBE3_StaticFields::get_offset_of_HmacRipeMD160_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4894 = { sizeof (MiscObjectIdentifiers_t3188E33E8700951DC1133A1CD98D6C2E889A7721), -1, sizeof(MiscObjectIdentifiers_t3188E33E8700951DC1133A1CD98D6C2E889A7721_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4894[32] = 
{
	MiscObjectIdentifiers_t3188E33E8700951DC1133A1CD98D6C2E889A7721_StaticFields::get_offset_of_Netscape_0(),
	MiscObjectIdentifiers_t3188E33E8700951DC1133A1CD98D6C2E889A7721_StaticFields::get_offset_of_NetscapeCertType_1(),
	MiscObjectIdentifiers_t3188E33E8700951DC1133A1CD98D6C2E889A7721_StaticFields::get_offset_of_NetscapeBaseUrl_2(),
	MiscObjectIdentifiers_t3188E33E8700951DC1133A1CD98D6C2E889A7721_StaticFields::get_offset_of_NetscapeRevocationUrl_3(),
	MiscObjectIdentifiers_t3188E33E8700951DC1133A1CD98D6C2E889A7721_StaticFields::get_offset_of_NetscapeCARevocationUrl_4(),
	MiscObjectIdentifiers_t3188E33E8700951DC1133A1CD98D6C2E889A7721_StaticFields::get_offset_of_NetscapeRenewalUrl_5(),
	MiscObjectIdentifiers_t3188E33E8700951DC1133A1CD98D6C2E889A7721_StaticFields::get_offset_of_NetscapeCAPolicyUrl_6(),
	MiscObjectIdentifiers_t3188E33E8700951DC1133A1CD98D6C2E889A7721_StaticFields::get_offset_of_NetscapeSslServerName_7(),
	MiscObjectIdentifiers_t3188E33E8700951DC1133A1CD98D6C2E889A7721_StaticFields::get_offset_of_NetscapeCertComment_8(),
	MiscObjectIdentifiers_t3188E33E8700951DC1133A1CD98D6C2E889A7721_StaticFields::get_offset_of_Verisign_9(),
	MiscObjectIdentifiers_t3188E33E8700951DC1133A1CD98D6C2E889A7721_StaticFields::get_offset_of_VerisignCzagExtension_10(),
	MiscObjectIdentifiers_t3188E33E8700951DC1133A1CD98D6C2E889A7721_StaticFields::get_offset_of_VerisignPrivate_6_9_11(),
	MiscObjectIdentifiers_t3188E33E8700951DC1133A1CD98D6C2E889A7721_StaticFields::get_offset_of_VerisignOnSiteJurisdictionHash_12(),
	MiscObjectIdentifiers_t3188E33E8700951DC1133A1CD98D6C2E889A7721_StaticFields::get_offset_of_VerisignBitString_6_13_13(),
	MiscObjectIdentifiers_t3188E33E8700951DC1133A1CD98D6C2E889A7721_StaticFields::get_offset_of_VerisignDnbDunsNumber_14(),
	MiscObjectIdentifiers_t3188E33E8700951DC1133A1CD98D6C2E889A7721_StaticFields::get_offset_of_VerisignIssStrongCrypto_15(),
	MiscObjectIdentifiers_t3188E33E8700951DC1133A1CD98D6C2E889A7721_StaticFields::get_offset_of_Novell_16(),
	MiscObjectIdentifiers_t3188E33E8700951DC1133A1CD98D6C2E889A7721_StaticFields::get_offset_of_NovellSecurityAttribs_17(),
	MiscObjectIdentifiers_t3188E33E8700951DC1133A1CD98D6C2E889A7721_StaticFields::get_offset_of_Entrust_18(),
	MiscObjectIdentifiers_t3188E33E8700951DC1133A1CD98D6C2E889A7721_StaticFields::get_offset_of_EntrustVersionExtension_19(),
	MiscObjectIdentifiers_t3188E33E8700951DC1133A1CD98D6C2E889A7721_StaticFields::get_offset_of_as_sys_sec_alg_ideaCBC_20(),
	MiscObjectIdentifiers_t3188E33E8700951DC1133A1CD98D6C2E889A7721_StaticFields::get_offset_of_cryptlib_21(),
	MiscObjectIdentifiers_t3188E33E8700951DC1133A1CD98D6C2E889A7721_StaticFields::get_offset_of_cryptlib_algorithm_22(),
	MiscObjectIdentifiers_t3188E33E8700951DC1133A1CD98D6C2E889A7721_StaticFields::get_offset_of_cryptlib_algorithm_blowfish_ECB_23(),
	MiscObjectIdentifiers_t3188E33E8700951DC1133A1CD98D6C2E889A7721_StaticFields::get_offset_of_cryptlib_algorithm_blowfish_CBC_24(),
	MiscObjectIdentifiers_t3188E33E8700951DC1133A1CD98D6C2E889A7721_StaticFields::get_offset_of_cryptlib_algorithm_blowfish_CFB_25(),
	MiscObjectIdentifiers_t3188E33E8700951DC1133A1CD98D6C2E889A7721_StaticFields::get_offset_of_cryptlib_algorithm_blowfish_OFB_26(),
	MiscObjectIdentifiers_t3188E33E8700951DC1133A1CD98D6C2E889A7721_StaticFields::get_offset_of_blake2_27(),
	MiscObjectIdentifiers_t3188E33E8700951DC1133A1CD98D6C2E889A7721_StaticFields::get_offset_of_id_blake2b160_28(),
	MiscObjectIdentifiers_t3188E33E8700951DC1133A1CD98D6C2E889A7721_StaticFields::get_offset_of_id_blake2b256_29(),
	MiscObjectIdentifiers_t3188E33E8700951DC1133A1CD98D6C2E889A7721_StaticFields::get_offset_of_id_blake2b384_30(),
	MiscObjectIdentifiers_t3188E33E8700951DC1133A1CD98D6C2E889A7721_StaticFields::get_offset_of_id_blake2b512_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4895 = { sizeof (NetscapeCertType_t9C742E0B655467187C3593E578FF36789B49C006), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4896 = { sizeof (NetscapeRevocationUrl_t8B3C3F349F87B28BCCB80B2278ACAB4446F29C62), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4897 = { sizeof (VerisignCzagExtension_t6CE98CD98CCAC7F3BD54ADCE0F7F60FF0A1001D2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4898 = { sizeof (NistNamedCurves_t1556B77B6F59D3B9BDCB07D9CD1C51AAE683F3B1), -1, sizeof(NistNamedCurves_t1556B77B6F59D3B9BDCB07D9CD1C51AAE683F3B1_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4898[2] = 
{
	NistNamedCurves_t1556B77B6F59D3B9BDCB07D9CD1C51AAE683F3B1_StaticFields::get_offset_of_objIds_0(),
	NistNamedCurves_t1556B77B6F59D3B9BDCB07D9CD1C51AAE683F3B1_StaticFields::get_offset_of_names_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4899 = { sizeof (NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E), -1, sizeof(NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4899[41] = 
{
	NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields::get_offset_of_NistAlgorithm_0(),
	NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields::get_offset_of_HashAlgs_1(),
	NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields::get_offset_of_IdSha256_2(),
	NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields::get_offset_of_IdSha384_3(),
	NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields::get_offset_of_IdSha512_4(),
	NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields::get_offset_of_IdSha224_5(),
	NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields::get_offset_of_IdSha512_224_6(),
	NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields::get_offset_of_IdSha512_256_7(),
	NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields::get_offset_of_IdSha3_224_8(),
	NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields::get_offset_of_IdSha3_256_9(),
	NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields::get_offset_of_IdSha3_384_10(),
	NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields::get_offset_of_IdSha3_512_11(),
	NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields::get_offset_of_IdShake128_12(),
	NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields::get_offset_of_IdShake256_13(),
	NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields::get_offset_of_Aes_14(),
	NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields::get_offset_of_IdAes128Ecb_15(),
	NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields::get_offset_of_IdAes128Cbc_16(),
	NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields::get_offset_of_IdAes128Ofb_17(),
	NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields::get_offset_of_IdAes128Cfb_18(),
	NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields::get_offset_of_IdAes128Wrap_19(),
	NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields::get_offset_of_IdAes128Gcm_20(),
	NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields::get_offset_of_IdAes128Ccm_21(),
	NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields::get_offset_of_IdAes192Ecb_22(),
	NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields::get_offset_of_IdAes192Cbc_23(),
	NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields::get_offset_of_IdAes192Ofb_24(),
	NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields::get_offset_of_IdAes192Cfb_25(),
	NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields::get_offset_of_IdAes192Wrap_26(),
	NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields::get_offset_of_IdAes192Gcm_27(),
	NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields::get_offset_of_IdAes192Ccm_28(),
	NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields::get_offset_of_IdAes256Ecb_29(),
	NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields::get_offset_of_IdAes256Cbc_30(),
	NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields::get_offset_of_IdAes256Ofb_31(),
	NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields::get_offset_of_IdAes256Cfb_32(),
	NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields::get_offset_of_IdAes256Wrap_33(),
	NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields::get_offset_of_IdAes256Gcm_34(),
	NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields::get_offset_of_IdAes256Ccm_35(),
	NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields::get_offset_of_IdDsaWithSha2_36(),
	NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields::get_offset_of_DsaWithSha224_37(),
	NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields::get_offset_of_DsaWithSha256_38(),
	NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields::get_offset_of_DsaWithSha384_39(),
	NistObjectIdentifiers_tC55C8BB482E802B6A7AC270D5FAF9C2D4FBFCD8E_StaticFields::get_offset_of_DsaWithSha512_40(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
