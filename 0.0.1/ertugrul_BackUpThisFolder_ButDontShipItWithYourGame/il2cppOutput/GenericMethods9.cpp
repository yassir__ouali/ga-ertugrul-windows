﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// ModestTree.Util.Action`5<System.Object,System.Object,System.Object,System.Object,System.Object>
struct Action_5_tA3F3461C4A9081A2000D185A35A46E23F4E4338F;
// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.Action`1<System.Object>
struct Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0;
// System.Action`2<System.Object,System.Object>
struct Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C;
// System.Action`3<System.Object,System.Object,System.Object>
struct Action_3_tCC14115B7178951118504E7198B7C872630643C5;
// System.Action`4<System.Object,System.Object,System.Object,System.Object>
struct Action_4_t46ACC504A4217F2E92EF2A827836993B819AF923;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.Type,Zenject.ZenjectTypeInfo>
struct Dictionary_2_t6BA2F0666D38DF096F45B41AB67474768713CE58;
// System.Collections.Generic.Dictionary`2<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.DiContainer/ProviderInfo>>
struct Dictionary_2_t7C592452CF60789D3B03C452382E134D587ABADC;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t2F75FCBEC68AFE08982DA43985F9D04056E2BE73;
// System.Collections.Generic.IEnumerable`1<System.Type>
struct IEnumerable_1_tF9225691990EF9799D9F4B64E4063CA0D1DF03CA;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D;
// System.Collections.Generic.List`1<System.Type>
struct List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0;
// System.Collections.Generic.List`1<Zenject.DiContainer>
struct List_1_tC0C0C06645444D1ED985242DB086B9AFC1D8E1FA;
// System.Collections.Generic.List`1<Zenject.IBindingFinalizer>
struct List_1_t260A5990EE904C05AE0C7D28243F87BFA487442A;
// System.Collections.Generic.List`1<Zenject.ILazy>
struct List_1_t4B97E348520D143EA8C892D9014012A434C2A126;
// System.Collections.Generic.List`1<Zenject.InjectableInfo>
struct List_1_t19FD687049A90924DCEA81F81C85555F15021A68;
// System.Collections.Generic.List`1<Zenject.PostInjectableInfo>
struct List_1_t8AA9EFA21EF1045CBB5A905EB31B2D66A23339FB;
// System.Collections.Generic.List`1<Zenject.TypeValuePair>
struct List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69;
// System.Collections.Generic.Queue`1<Zenject.IBindingFinalizer>
struct Queue_1_tB4CDAB25704B2A2FE785C4404A82F5B755C20BD9;
// System.Collections.Generic.Stack`1<Zenject.DiContainer/LookupId>
struct Stack_1_t30B30BF8B50F476BF18BDC1667E81F8EE0045E48;
// System.Delegate
struct Delegate_t;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Func`2<System.Object,System.Action>
struct Func_2_t058479A951AB44C28D842472579EAA2FDD8DFC0D;
// System.Func`2<System.Object,System.Action`1<System.Object>>
struct Func_2_t38649B64802A8DCC2476C1CBE0CF179DBBFA5113;
// System.Func`2<System.Object,System.Action`2<System.Object,System.Object>>
struct Func_2_t88CE136AD37DD8F7713742358AC56B012C611D88;
// System.Func`2<System.Object,System.Action`3<System.Object,System.Object,System.Object>>
struct Func_2_t6D7E96A36A34348A9F5DA29E4FFE3BCEA4558CBC;
// System.Func`2<System.Object,System.Action`4<System.Object,System.Object,System.Object,System.Object>>
struct Func_2_t501D8CF44938B0EB347A4669809ED06E577C2D3C;
// System.Func`2<System.Object,System.Object>
struct Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4;
// System.Func`2<Zenject.DiContainer,Zenject.IProvider>
struct Func_2_t3BE49542C4369B3A5CA2F7EFB4B994EA5A9C449C;
// System.Func`2<Zenject.InjectContext,System.Collections.Generic.IEnumerable`1<System.Object>>
struct Func_2_tEB18CC35489403B1DD34E84D07D6EBE38C06F980;
// System.Func`2<Zenject.InjectContext,System.Object>
struct Func_2_tE24EB23C7D8F4AD0A3A34C5DB2C9F329886F48F2;
// System.Func`3<System.Object,System.Object,System.Object>
struct Func_3_t0875D079514B9064DE951B01B4AE82F6C7436F64;
// System.Func`3<Zenject.DiContainer,System.Type,Zenject.IProvider>
struct Func_3_tF2BBB0DC92895D84974BAECD6D1B2AE7FFC65AAA;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.Reflection.Binder
struct Binder_t4D5CB06963501D32847C057B57157D6DC49CA759;
// System.Reflection.ConstructorInfo
struct ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF;
// System.Reflection.MemberFilter
struct MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// Zenject.ArgConditionCopyNonLazyBinder
struct ArgConditionCopyNonLazyBinder_t5F1B4361E0805E20DDBEDEBC61B90CFFBB68B21B;
// Zenject.BindFinalizerWrapper
struct BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8;
// Zenject.BindInfo
struct BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991;
// Zenject.BindingCondition
struct BindingCondition_t3BC89ABD74DA160C3396036CE0360DE414B4ADD8;
// Zenject.BindingId
struct BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA;
// Zenject.ConcreteBinderNonGeneric
struct ConcreteBinderNonGeneric_t607DFF5BDEE16A8EF207F5D6148835CD108F72F9;
// Zenject.ConcreteIdBinderNonGeneric
struct ConcreteIdBinderNonGeneric_t372B43CB3CC50C44EF038F152C0F6345DFA22E24;
// Zenject.ConditionCopyNonLazyBinder
struct ConditionCopyNonLazyBinder_tF7F37C5F1508AAB283151819E412C853A8D94B9B;
// Zenject.Context
struct Context_t04459FF3A5B7E8AF25B965F5717F535AE8FA6DBA;
// Zenject.DiContainer
struct DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5;
// Zenject.FactoryBindInfo
struct FactoryBindInfo_t13E5B099259F31F1EF31D3550FE94DF78C9F1C00;
// Zenject.FactoryFromBinder`1<System.Object>
struct FactoryFromBinder_1_t8A38890E41D19C23AC52C50A9AEF8B9DA43E17D4;
// Zenject.FactorySubContainerBinderBase`1<System.Object>
struct FactorySubContainerBinderBase_1_t10122DEBCBD743FE20FF2CCC6889131951876194;
// Zenject.FactorySubContainerBinderWithParams`1<System.Object>
struct FactorySubContainerBinderWithParams_1_t45B14E1162744205D8DDBEBD21BD61C7629B12FC;
// Zenject.FromBinder
struct FromBinder_t69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75;
// Zenject.FromBinderGeneric`1<System.Object>
struct FromBinderGeneric_1_tB3D3D4B24691B0AFF684D39C08F63B2193327187;
// Zenject.FromBinderNonGeneric
struct FromBinderNonGeneric_t677B667DE367A055191AC7B64A622B8495C6CF89;
// Zenject.GameObjectCreationParameters
struct GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A;
// Zenject.IBindingFinalizer
struct IBindingFinalizer_t08B71AF38DA228689170154233A3C472FE81BD83;
// Zenject.IProvider
struct IProvider_t26B24AA2351CC93A1621F3C57B5BE5CFC4624394;
// Zenject.IdScopeConditionCopyNonLazyBinder
struct IdScopeConditionCopyNonLazyBinder_tF4C46379BF6B4595053DBCAB75767E759A18ECEE;
// Zenject.InjectContext
struct InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF;
// Zenject.LazyInstanceInjector
struct LazyInstanceInjector_t0120534A9142C2F3AC0A2A87E5CAC778A6F831A6;
// Zenject.MemoryPoolBindInfo
struct MemoryPoolBindInfo_tB7A75979568ABAD9B4C0F599B76DF4BDBD3A5D01;
// Zenject.MemoryPoolInitialSizeBinder`1<System.Object>
struct MemoryPoolInitialSizeBinder_1_t831900A50FA233E589596598650E789772546324;
// Zenject.NameTransformConditionCopyNonLazyBinder
struct NameTransformConditionCopyNonLazyBinder_t2E9664EE069D9C6CA15EFEDD64FAE2B4125AC36D;
// Zenject.NonLazyBinder
struct NonLazyBinder_tA9FB0768A2F39559FBDDCAFBF940AAD240B7C43C;
// Zenject.NullBindingFinalizer
struct NullBindingFinalizer_tFEAF0122BA4AF8A5209DE0330F0BFF444F06AD65;
// Zenject.ScopableBindingFinalizer
struct ScopableBindingFinalizer_t1278F9BE4EE396C7D8B5CA5625C540EC3582632C;
// Zenject.ScopeArgConditionCopyNonLazyBinder
struct ScopeArgConditionCopyNonLazyBinder_t218A003451367EEC6A439D63A0F38B964EF07B33;
// Zenject.ScopeArgNonLazyBinder
struct ScopeArgNonLazyBinder_t1D85A2EED80F3DAE72346683D48CCCEAB4738BA6;
// Zenject.ScopeConditionCopyNonLazyBinder
struct ScopeConditionCopyNonLazyBinder_t6221939C8173223799835D9DF0944D6D777C3EC6;
// Zenject.ScopeNonLazyBinder
struct ScopeNonLazyBinder_t7BC7AB773CA2EAECC76C3B733A1271EE6866646A;
// Zenject.SignalBinderWithId
struct SignalBinderWithId_tF62257893A9AA47A533D47DC5DB0C0C046F67B81;
// Zenject.SignalFromBinder`1<System.Object>
struct SignalFromBinder_1_t862D9073BD2CB93CFFF7F75A6F7262CDF34AA1D7;
// Zenject.SignalHandlerBinder
struct SignalHandlerBinder_t4AB70F7A7BD1596D46B88F834A400EBEDE185771;
// Zenject.SignalHandlerBinderWithId
struct SignalHandlerBinderWithId_tF2F486B2BBE4565DB9C112C9D5755BDCFE7BCD27;
// Zenject.SignalHandlerBinderWithId`1<System.Object>
struct SignalHandlerBinderWithId_1_t7C2451301E4286C80D2E3AA5B7D4A77DBC08DBFE;
// Zenject.SignalHandlerBinderWithId`2<System.Object,System.Object>
struct SignalHandlerBinderWithId_2_t5075E83F01DA88E7F92BC36E706802EC87425C9D;
// Zenject.SignalHandlerBinderWithId`3<System.Object,System.Object,System.Object>
struct SignalHandlerBinderWithId_3_t76D4499350CECBF7B17E476217D26420F3E8D532;
// Zenject.SignalHandlerBinderWithId`4<System.Object,System.Object,System.Object,System.Object>
struct SignalHandlerBinderWithId_4_t36F180155CE8C6D813408F9B2086613D07CF41C9;
// Zenject.SignalHandlerBinder`1<System.Object>
struct SignalHandlerBinder_1_tAC85A1AD98374941739974445C41BF8617CF78FC;
// Zenject.SignalHandlerBinder`2<System.Object,System.Object>
struct SignalHandlerBinder_2_t6F32E8CD8248414B4F3681F2E4D8BFB658F24D30;
// Zenject.SignalHandlerBinder`3<System.Object,System.Object,System.Object>
struct SignalHandlerBinder_3_t9ADF0354A52CA5EFF23194E48F870113CF337055;
// Zenject.SignalHandlerBinder`4<System.Object,System.Object,System.Object,System.Object>
struct SignalHandlerBinder_4_t6088EB5507EA2460DDEA35BEFBCE49C3F0F851DC;
// Zenject.SignalSettings
struct SignalSettings_tD753AA3D16CEA36F97CDAEC5428C5217F1EB6540;
// Zenject.SingletonImplIds/ToGetter
struct ToGetter_t269C4EED29F0AC2A1C9FB4061EA228DC2DDADED4;
// Zenject.SingletonImplIds/ToMethod
struct ToMethod_t12F5E0A4D9C370BB507C42523A4401D55EBE2836;
// Zenject.SingletonMarkRegistry
struct SingletonMarkRegistry_tB0ADDC9314F22EFEF3C51F08F71E6E1CB236EC7F;
// Zenject.SingletonProviderCreator
struct SingletonProviderCreator_t07218AEFEEBFF6A8B65FB17100F1AB292A94F5AD;
// Zenject.SubContainerBinder
struct SubContainerBinder_t24411FF88DADEE6DFBC16EE722E0F4944E8732D6;
// Zenject.TypeValuePair
struct TypeValuePair_tD309CDC658476FDDF08345C92D3F4D7A0A1F9792;
// Zenject.TypeValuePair[]
struct TypeValuePairU5BU5D_t97D37FE05D254669B0CE4B9D66B5B1E3F204A325;
// Zenject.ZenjectTypeInfo
struct ZenjectTypeInfo_t2866A662C0FDA5B357BCC135021933EE8914535E;

extern RuntimeClass* BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991_il2cpp_TypeInfo_var;
extern RuntimeClass* BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA_il2cpp_TypeInfo_var;
extern RuntimeClass* FactoryBindInfo_t13E5B099259F31F1EF31D3550FE94DF78C9F1C00_il2cpp_TypeInfo_var;
extern RuntimeClass* Func_3_tF2BBB0DC92895D84974BAECD6D1B2AE7FFC65AAA_il2cpp_TypeInfo_var;
extern RuntimeClass* Guid_t_il2cpp_TypeInfo_var;
extern RuntimeClass* IdScopeConditionCopyNonLazyBinder_tF4C46379BF6B4595053DBCAB75767E759A18ECEE_il2cpp_TypeInfo_var;
extern RuntimeClass* InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF_il2cpp_TypeInfo_var;
extern RuntimeClass* MemoryPoolBindInfo_tB7A75979568ABAD9B4C0F599B76DF4BDBD3A5D01_il2cpp_TypeInfo_var;
extern RuntimeClass* NullBindingFinalizer_tFEAF0122BA4AF8A5209DE0330F0BFF444F06AD65_il2cpp_TypeInfo_var;
extern RuntimeClass* ScopableBindingFinalizer_t1278F9BE4EE396C7D8B5CA5625C540EC3582632C_il2cpp_TypeInfo_var;
extern RuntimeClass* ScopeArgConditionCopyNonLazyBinder_t218A003451367EEC6A439D63A0F38B964EF07B33_il2cpp_TypeInfo_var;
extern RuntimeClass* ScopeConditionCopyNonLazyBinder_t6221939C8173223799835D9DF0944D6D777C3EC6_il2cpp_TypeInfo_var;
extern RuntimeClass* ScopeNonLazyBinder_t7BC7AB773CA2EAECC76C3B733A1271EE6866646A_il2cpp_TypeInfo_var;
extern RuntimeClass* SignalBinderWithId_tF62257893A9AA47A533D47DC5DB0C0C046F67B81_il2cpp_TypeInfo_var;
extern RuntimeClass* SignalHandlerBinderWithId_tF2F486B2BBE4565DB9C112C9D5755BDCFE7BCD27_il2cpp_TypeInfo_var;
extern RuntimeClass* SignalSettings_tD753AA3D16CEA36F97CDAEC5428C5217F1EB6540_il2cpp_TypeInfo_var;
extern RuntimeClass* ToGetter_t269C4EED29F0AC2A1C9FB4061EA228DC2DDADED4_il2cpp_TypeInfo_var;
extern RuntimeClass* ToMethod_t12F5E0A4D9C370BB507C42523A4401D55EBE2836_il2cpp_TypeInfo_var;
extern RuntimeClass* TypeAnalyzer_t19EB8063B9BA23AD8FAAB2AF5228A8D8D086F722_il2cpp_TypeInfo_var;
extern RuntimeClass* TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F_il2cpp_TypeInfo_var;
extern RuntimeClass* TypeValuePair_tD309CDC658476FDDF08345C92D3F4D7A0A1F9792_il2cpp_TypeInfo_var;
extern RuntimeClass* Type_t_il2cpp_TypeInfo_var;
extern const RuntimeMethod* ArgConditionCopyNonLazyBinder_WithArguments_TisSignalSettings_tD753AA3D16CEA36F97CDAEC5428C5217F1EB6540_TisBindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991_mD9A3095921FC1EFD57D2AD25CFAA64C2A5C56FF8_RuntimeMethod_var;
extern const RuntimeMethod* Func_3__ctor_m0FFFF142D2BDA3D5294EED98E0A00AAD6961E0A0_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m57994E0773CB740B2495C2AE0CE7CBE3C47BB938_RuntimeMethod_var;
extern const RuntimeType* IDisposable_t7218B22548186B208D65EA5B7870503810A2D15A_0_0_0_var;
extern const RuntimeType* IInitializable_t8CDB5239D7F1A24EC49AD4017F6D65D2A8A5F345_0_0_0_var;
extern const RuntimeType* IMemoryPool_t6DF3F57F29BCA8BB662B1642357C53A871F0202B_0_0_0_var;
extern const uint32_t ConcreteBinderNonGeneric_To_TisRuntimeObject_mE7A8C0B5609C65749168E03B0B82307BA2E9E2B5_MetadataUsageId;
extern const uint32_t DiContainer_BindInstance_TisRuntimeObject_m98F908B1F4EB94BB04404CA3B0A1CA24A64D0548_MetadataUsageId;
extern const uint32_t DiContainer_BindInterfacesAndSelfTo_TisRuntimeObject_m232B041132998F17891F0723CC1C7A22F5926875_MetadataUsageId;
extern const uint32_t DiContainer_BindInterfacesTo_TisRuntimeObject_m4C1025EA591985E3BD8CC62353998FC1F2434424_MetadataUsageId;
extern const uint32_t DiContainer_BindMemoryPool_TisRuntimeObject_TisRuntimeObject_TisRuntimeObject_m685E7FB309A0EB33BE5E3258AB7ADDEEF634BC27_MetadataUsageId;
extern const uint32_t FactorySubContainerBinderBase_1_ByNewPrefabInstaller_TisRuntimeObject_mAB991259136BCE813E2B850B78B2316AFD49B8DC_MetadataUsageId;
extern const uint32_t FactorySubContainerBinderBase_1_ByNewPrefabResourceInstaller_TisRuntimeObject_m296FE0F1CD5C1DF3EB42956CD186C8E30749DFB9_MetadataUsageId;
extern const uint32_t FactorySubContainerBinderWithParams_1_ByNewPrefabResource_TisRuntimeObject_m3270E38F2C74F969B54FEFB94768CA95F5E2E17B_MetadataUsageId;
extern const uint32_t FactorySubContainerBinderWithParams_1_ByNewPrefab_TisRuntimeObject_mFA81888D8C211CB9EE876AEB08B267FCC8B3A90B_MetadataUsageId;
extern const uint32_t FromBinder_FromFactoryBase_TisRuntimeObject_TisRuntimeObject_m4B8B3693DD9AA53E12628BB91321A0D1D1DBAF23_MetadataUsageId;
extern const uint32_t FromBinder_FromMethodBase_TisRuntimeObject_mDE242188EC651A59753B87DA48AB571C08CD1389_MetadataUsageId;
extern const uint32_t FromBinder_FromMethodMultipleBase_TisRuntimeObject_mA1F7B7CD47342E6DD5EC125DBFCD696CE92679C7_MetadataUsageId;
extern const uint32_t FromBinder_FromResolveGetterBase_TisRuntimeObject_TisRuntimeObject_m35AE38CDA9C722CFA39C31625CA0803CF2738E8F_MetadataUsageId;
extern const uint32_t InjectUtil_CreateTypePair_TisRuntimeObject_m3FC0D38A137237CADAA24DEC0530CDE1C30B2B4B_MetadataUsageId;
extern const uint32_t SignalExtensions_BindSignal_TisRuntimeObject_TisRuntimeObject_TisRuntimeObject_TisRuntimeObject_TisRuntimeObject_mF00BBB207286CA8A011B5A6CC8CCF5646AF94679_MetadataUsageId;
extern const uint32_t SignalExtensions_BindSignal_TisRuntimeObject_TisRuntimeObject_TisRuntimeObject_TisRuntimeObject_mCD45D46A7F09666035E38FA09D85AC2036120CEE_MetadataUsageId;
extern const uint32_t SignalExtensions_BindSignal_TisRuntimeObject_TisRuntimeObject_TisRuntimeObject_mD07A9C35647F9E5133B90059FCB1003CDF011D05_MetadataUsageId;
extern const uint32_t SignalExtensions_BindSignal_TisRuntimeObject_TisRuntimeObject_mA30903AF0FAE5039370DEB54BB3A9D6BD2B65382_MetadataUsageId;
extern const uint32_t SignalExtensions_BindSignal_TisRuntimeObject_m2F8E2FCCF499410D18C110D3A9C0350D2B3AE69D_MetadataUsageId;
extern const uint32_t SignalExtensions_DeclareSignal_TisRuntimeObject_m6595FFCE35DCB74F393F0A2563E095355776E0F1_MetadataUsageId;
extern const uint32_t SignalFromBinder_1_FromResolveGetter_TisRuntimeObject_m2F3F2A4FB39B45822A6D05915E40DC24F9969BD3_MetadataUsageId;
extern const uint32_t SignalFromBinder_1_FromResolveGetter_TisRuntimeObject_m9D8D6C91ACCB9CB6884007A448EBA113E8FAB046_MetadataUsageId;
extern const uint32_t SignalHandlerBinder_1_To_TisRuntimeObject_m825B6A42D885A2F242B8F763BCABFE8488A0EE2A_MetadataUsageId;
extern const uint32_t SignalHandlerBinder_1_To_TisRuntimeObject_m85941C21720C8AA6D0DC2E27BB82A2EC4F32CBEE_MetadataUsageId;
extern const uint32_t SignalHandlerBinder_2_To_TisRuntimeObject_m511B8D1DCEF7D8D08E952754B7D65568D9D6B2D6_MetadataUsageId;
extern const uint32_t SignalHandlerBinder_2_To_TisRuntimeObject_m62670A40CD41CEFB436F17F06AAF381F16B65607_MetadataUsageId;
extern const uint32_t SignalHandlerBinder_3_To_TisRuntimeObject_m0886F343F5082F2EE0E27951D9F21C489608A929_MetadataUsageId;
extern const uint32_t SignalHandlerBinder_3_To_TisRuntimeObject_m237EC2847CEC91DE21A589DF099C732956AB2F6C_MetadataUsageId;
extern const uint32_t SignalHandlerBinder_4_To_TisRuntimeObject_m2AE099BAC1852E16FEFAEB81D2DFCB05DAA3A059_MetadataUsageId;
extern const uint32_t SignalHandlerBinder_4_To_TisRuntimeObject_m88CDE60E94639D3111738874458A215E623C06FB_MetadataUsageId;
extern const uint32_t SignalHandlerBinder_To_TisRuntimeObject_m848A03675EF5E582AA51AFF1DE8080E1F1EEF68B_MetadataUsageId;
extern const uint32_t SignalHandlerBinder_To_TisRuntimeObject_mD2ABC9CBE6C2353BF9577B1523BCB2AAC8E2E6CA_MetadataUsageId;
extern const uint32_t SubContainerBinder_ByInstaller_TisRuntimeObject_m9E70B6975FC0797BBD2981ED12509D7974444A16_MetadataUsageId;
extern const uint32_t TypeAnalyzer_GetInfo_TisRuntimeObject_m41DD5D8E47571BC5CDEF9DDA899B887F340D0D3F_MetadataUsageId;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;

struct TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F;


#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef LIST_1_TE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0_H
#define LIST_1_TE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.Type>
struct  List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0, ____items_1)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get__items_1() const { return ____items_1; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0_StaticFields, ____emptyArray_5)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get__emptyArray_5() const { return ____emptyArray_5; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_TE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0_H
#ifndef LIST_1_TCEA704C275810C578B5AC9C854F275F3CAF7BA69_H
#define LIST_1_TCEA704C275810C578B5AC9C854F275F3CAF7BA69_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<Zenject.TypeValuePair>
struct  List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	TypeValuePairU5BU5D_t97D37FE05D254669B0CE4B9D66B5B1E3F204A325* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69, ____items_1)); }
	inline TypeValuePairU5BU5D_t97D37FE05D254669B0CE4B9D66B5B1E3F204A325* get__items_1() const { return ____items_1; }
	inline TypeValuePairU5BU5D_t97D37FE05D254669B0CE4B9D66B5B1E3F204A325** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(TypeValuePairU5BU5D_t97D37FE05D254669B0CE4B9D66B5B1E3F204A325* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	TypeValuePairU5BU5D_t97D37FE05D254669B0CE4B9D66B5B1E3F204A325* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69_StaticFields, ____emptyArray_5)); }
	inline TypeValuePairU5BU5D_t97D37FE05D254669B0CE4B9D66B5B1E3F204A325* get__emptyArray_5() const { return ____emptyArray_5; }
	inline TypeValuePairU5BU5D_t97D37FE05D254669B0CE4B9D66B5B1E3F204A325** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(TypeValuePairU5BU5D_t97D37FE05D254669B0CE4B9D66B5B1E3F204A325* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_TCEA704C275810C578B5AC9C854F275F3CAF7BA69_H
#ifndef MEMBERINFO_T_H
#define MEMBERINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERINFO_T_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef BINDFINALIZERWRAPPER_T043E4C78D018E6AF48A956B33FF3AEA55CDD86C8_H
#define BINDFINALIZERWRAPPER_T043E4C78D018E6AF48A956B33FF3AEA55CDD86C8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.BindFinalizerWrapper
struct  BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8  : public RuntimeObject
{
public:
	// Zenject.IBindingFinalizer Zenject.BindFinalizerWrapper::_subFinalizer
	RuntimeObject* ____subFinalizer_0;

public:
	inline static int32_t get_offset_of__subFinalizer_0() { return static_cast<int32_t>(offsetof(BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8, ____subFinalizer_0)); }
	inline RuntimeObject* get__subFinalizer_0() const { return ____subFinalizer_0; }
	inline RuntimeObject** get_address_of__subFinalizer_0() { return &____subFinalizer_0; }
	inline void set__subFinalizer_0(RuntimeObject* value)
	{
		____subFinalizer_0 = value;
		Il2CppCodeGenWriteBarrier((&____subFinalizer_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDFINALIZERWRAPPER_T043E4C78D018E6AF48A956B33FF3AEA55CDD86C8_H
#ifndef BINDINGID_TDE4E18829D06364806DC2F3F04F0574D68DF92BA_H
#define BINDINGID_TDE4E18829D06364806DC2F3F04F0574D68DF92BA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.BindingId
struct  BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA  : public RuntimeObject
{
public:
	// System.Type Zenject.BindingId::Type
	Type_t * ___Type_0;
	// System.Object Zenject.BindingId::Identifier
	RuntimeObject * ___Identifier_1;

public:
	inline static int32_t get_offset_of_Type_0() { return static_cast<int32_t>(offsetof(BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA, ___Type_0)); }
	inline Type_t * get_Type_0() const { return ___Type_0; }
	inline Type_t ** get_address_of_Type_0() { return &___Type_0; }
	inline void set_Type_0(Type_t * value)
	{
		___Type_0 = value;
		Il2CppCodeGenWriteBarrier((&___Type_0), value);
	}

	inline static int32_t get_offset_of_Identifier_1() { return static_cast<int32_t>(offsetof(BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA, ___Identifier_1)); }
	inline RuntimeObject * get_Identifier_1() const { return ___Identifier_1; }
	inline RuntimeObject ** get_address_of_Identifier_1() { return &___Identifier_1; }
	inline void set_Identifier_1(RuntimeObject * value)
	{
		___Identifier_1 = value;
		Il2CppCodeGenWriteBarrier((&___Identifier_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGID_TDE4E18829D06364806DC2F3F04F0574D68DF92BA_H
#ifndef DICONTAINER_T7619E999A5CE72FEE4D2419403214E62D95FFFD5_H
#define DICONTAINER_T7619E999A5CE72FEE4D2419403214E62D95FFFD5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.DiContainer
struct  DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.DiContainer_ProviderInfo>> Zenject.DiContainer::_providers
	Dictionary_2_t7C592452CF60789D3B03C452382E134D587ABADC * ____providers_0;
	// System.Collections.Generic.List`1<Zenject.DiContainer> Zenject.DiContainer::_parentContainers
	List_1_tC0C0C06645444D1ED985242DB086B9AFC1D8E1FA * ____parentContainers_1;
	// System.Collections.Generic.List`1<Zenject.DiContainer> Zenject.DiContainer::_ancestorContainers
	List_1_tC0C0C06645444D1ED985242DB086B9AFC1D8E1FA * ____ancestorContainers_2;
	// System.Collections.Generic.Stack`1<Zenject.DiContainer_LookupId> Zenject.DiContainer::_resolvesInProgress
	Stack_1_t30B30BF8B50F476BF18BDC1667E81F8EE0045E48 * ____resolvesInProgress_3;
	// Zenject.SingletonProviderCreator Zenject.DiContainer::_singletonProviderCreator
	SingletonProviderCreator_t07218AEFEEBFF6A8B65FB17100F1AB292A94F5AD * ____singletonProviderCreator_4;
	// Zenject.SingletonMarkRegistry Zenject.DiContainer::_singletonMarkRegistry
	SingletonMarkRegistry_tB0ADDC9314F22EFEF3C51F08F71E6E1CB236EC7F * ____singletonMarkRegistry_5;
	// Zenject.LazyInstanceInjector Zenject.DiContainer::_lazyInjector
	LazyInstanceInjector_t0120534A9142C2F3AC0A2A87E5CAC778A6F831A6 * ____lazyInjector_6;
	// System.Collections.Generic.Queue`1<Zenject.IBindingFinalizer> Zenject.DiContainer::_currentBindings
	Queue_1_tB4CDAB25704B2A2FE785C4404A82F5B755C20BD9 * ____currentBindings_7;
	// System.Collections.Generic.List`1<Zenject.IBindingFinalizer> Zenject.DiContainer::_childBindings
	List_1_t260A5990EE904C05AE0C7D28243F87BFA487442A * ____childBindings_8;
	// System.Collections.Generic.List`1<Zenject.ILazy> Zenject.DiContainer::_lateBindingsToValidate
	List_1_t4B97E348520D143EA8C892D9014012A434C2A126 * ____lateBindingsToValidate_9;
	// Zenject.Context Zenject.DiContainer::_context
	Context_t04459FF3A5B7E8AF25B965F5717F535AE8FA6DBA * ____context_10;
	// System.Boolean Zenject.DiContainer::_isFinalizingBinding
	bool ____isFinalizingBinding_11;
	// System.Boolean Zenject.DiContainer::_isValidating
	bool ____isValidating_12;
	// System.Boolean Zenject.DiContainer::_isInstalling
	bool ____isInstalling_13;
	// System.Boolean Zenject.DiContainer::_hasDisplayedInstallWarning
	bool ____hasDisplayedInstallWarning_14;
	// System.Boolean Zenject.DiContainer::<ShouldCheckForInstallWarning>k__BackingField
	bool ___U3CShouldCheckForInstallWarningU3Ek__BackingField_15;
	// System.Boolean Zenject.DiContainer::<AssertOnNewGameObjects>k__BackingField
	bool ___U3CAssertOnNewGameObjectsU3Ek__BackingField_16;
	// UnityEngine.Transform Zenject.DiContainer::<DefaultParent>k__BackingField
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___U3CDefaultParentU3Ek__BackingField_17;

public:
	inline static int32_t get_offset_of__providers_0() { return static_cast<int32_t>(offsetof(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5, ____providers_0)); }
	inline Dictionary_2_t7C592452CF60789D3B03C452382E134D587ABADC * get__providers_0() const { return ____providers_0; }
	inline Dictionary_2_t7C592452CF60789D3B03C452382E134D587ABADC ** get_address_of__providers_0() { return &____providers_0; }
	inline void set__providers_0(Dictionary_2_t7C592452CF60789D3B03C452382E134D587ABADC * value)
	{
		____providers_0 = value;
		Il2CppCodeGenWriteBarrier((&____providers_0), value);
	}

	inline static int32_t get_offset_of__parentContainers_1() { return static_cast<int32_t>(offsetof(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5, ____parentContainers_1)); }
	inline List_1_tC0C0C06645444D1ED985242DB086B9AFC1D8E1FA * get__parentContainers_1() const { return ____parentContainers_1; }
	inline List_1_tC0C0C06645444D1ED985242DB086B9AFC1D8E1FA ** get_address_of__parentContainers_1() { return &____parentContainers_1; }
	inline void set__parentContainers_1(List_1_tC0C0C06645444D1ED985242DB086B9AFC1D8E1FA * value)
	{
		____parentContainers_1 = value;
		Il2CppCodeGenWriteBarrier((&____parentContainers_1), value);
	}

	inline static int32_t get_offset_of__ancestorContainers_2() { return static_cast<int32_t>(offsetof(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5, ____ancestorContainers_2)); }
	inline List_1_tC0C0C06645444D1ED985242DB086B9AFC1D8E1FA * get__ancestorContainers_2() const { return ____ancestorContainers_2; }
	inline List_1_tC0C0C06645444D1ED985242DB086B9AFC1D8E1FA ** get_address_of__ancestorContainers_2() { return &____ancestorContainers_2; }
	inline void set__ancestorContainers_2(List_1_tC0C0C06645444D1ED985242DB086B9AFC1D8E1FA * value)
	{
		____ancestorContainers_2 = value;
		Il2CppCodeGenWriteBarrier((&____ancestorContainers_2), value);
	}

	inline static int32_t get_offset_of__resolvesInProgress_3() { return static_cast<int32_t>(offsetof(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5, ____resolvesInProgress_3)); }
	inline Stack_1_t30B30BF8B50F476BF18BDC1667E81F8EE0045E48 * get__resolvesInProgress_3() const { return ____resolvesInProgress_3; }
	inline Stack_1_t30B30BF8B50F476BF18BDC1667E81F8EE0045E48 ** get_address_of__resolvesInProgress_3() { return &____resolvesInProgress_3; }
	inline void set__resolvesInProgress_3(Stack_1_t30B30BF8B50F476BF18BDC1667E81F8EE0045E48 * value)
	{
		____resolvesInProgress_3 = value;
		Il2CppCodeGenWriteBarrier((&____resolvesInProgress_3), value);
	}

	inline static int32_t get_offset_of__singletonProviderCreator_4() { return static_cast<int32_t>(offsetof(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5, ____singletonProviderCreator_4)); }
	inline SingletonProviderCreator_t07218AEFEEBFF6A8B65FB17100F1AB292A94F5AD * get__singletonProviderCreator_4() const { return ____singletonProviderCreator_4; }
	inline SingletonProviderCreator_t07218AEFEEBFF6A8B65FB17100F1AB292A94F5AD ** get_address_of__singletonProviderCreator_4() { return &____singletonProviderCreator_4; }
	inline void set__singletonProviderCreator_4(SingletonProviderCreator_t07218AEFEEBFF6A8B65FB17100F1AB292A94F5AD * value)
	{
		____singletonProviderCreator_4 = value;
		Il2CppCodeGenWriteBarrier((&____singletonProviderCreator_4), value);
	}

	inline static int32_t get_offset_of__singletonMarkRegistry_5() { return static_cast<int32_t>(offsetof(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5, ____singletonMarkRegistry_5)); }
	inline SingletonMarkRegistry_tB0ADDC9314F22EFEF3C51F08F71E6E1CB236EC7F * get__singletonMarkRegistry_5() const { return ____singletonMarkRegistry_5; }
	inline SingletonMarkRegistry_tB0ADDC9314F22EFEF3C51F08F71E6E1CB236EC7F ** get_address_of__singletonMarkRegistry_5() { return &____singletonMarkRegistry_5; }
	inline void set__singletonMarkRegistry_5(SingletonMarkRegistry_tB0ADDC9314F22EFEF3C51F08F71E6E1CB236EC7F * value)
	{
		____singletonMarkRegistry_5 = value;
		Il2CppCodeGenWriteBarrier((&____singletonMarkRegistry_5), value);
	}

	inline static int32_t get_offset_of__lazyInjector_6() { return static_cast<int32_t>(offsetof(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5, ____lazyInjector_6)); }
	inline LazyInstanceInjector_t0120534A9142C2F3AC0A2A87E5CAC778A6F831A6 * get__lazyInjector_6() const { return ____lazyInjector_6; }
	inline LazyInstanceInjector_t0120534A9142C2F3AC0A2A87E5CAC778A6F831A6 ** get_address_of__lazyInjector_6() { return &____lazyInjector_6; }
	inline void set__lazyInjector_6(LazyInstanceInjector_t0120534A9142C2F3AC0A2A87E5CAC778A6F831A6 * value)
	{
		____lazyInjector_6 = value;
		Il2CppCodeGenWriteBarrier((&____lazyInjector_6), value);
	}

	inline static int32_t get_offset_of__currentBindings_7() { return static_cast<int32_t>(offsetof(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5, ____currentBindings_7)); }
	inline Queue_1_tB4CDAB25704B2A2FE785C4404A82F5B755C20BD9 * get__currentBindings_7() const { return ____currentBindings_7; }
	inline Queue_1_tB4CDAB25704B2A2FE785C4404A82F5B755C20BD9 ** get_address_of__currentBindings_7() { return &____currentBindings_7; }
	inline void set__currentBindings_7(Queue_1_tB4CDAB25704B2A2FE785C4404A82F5B755C20BD9 * value)
	{
		____currentBindings_7 = value;
		Il2CppCodeGenWriteBarrier((&____currentBindings_7), value);
	}

	inline static int32_t get_offset_of__childBindings_8() { return static_cast<int32_t>(offsetof(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5, ____childBindings_8)); }
	inline List_1_t260A5990EE904C05AE0C7D28243F87BFA487442A * get__childBindings_8() const { return ____childBindings_8; }
	inline List_1_t260A5990EE904C05AE0C7D28243F87BFA487442A ** get_address_of__childBindings_8() { return &____childBindings_8; }
	inline void set__childBindings_8(List_1_t260A5990EE904C05AE0C7D28243F87BFA487442A * value)
	{
		____childBindings_8 = value;
		Il2CppCodeGenWriteBarrier((&____childBindings_8), value);
	}

	inline static int32_t get_offset_of__lateBindingsToValidate_9() { return static_cast<int32_t>(offsetof(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5, ____lateBindingsToValidate_9)); }
	inline List_1_t4B97E348520D143EA8C892D9014012A434C2A126 * get__lateBindingsToValidate_9() const { return ____lateBindingsToValidate_9; }
	inline List_1_t4B97E348520D143EA8C892D9014012A434C2A126 ** get_address_of__lateBindingsToValidate_9() { return &____lateBindingsToValidate_9; }
	inline void set__lateBindingsToValidate_9(List_1_t4B97E348520D143EA8C892D9014012A434C2A126 * value)
	{
		____lateBindingsToValidate_9 = value;
		Il2CppCodeGenWriteBarrier((&____lateBindingsToValidate_9), value);
	}

	inline static int32_t get_offset_of__context_10() { return static_cast<int32_t>(offsetof(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5, ____context_10)); }
	inline Context_t04459FF3A5B7E8AF25B965F5717F535AE8FA6DBA * get__context_10() const { return ____context_10; }
	inline Context_t04459FF3A5B7E8AF25B965F5717F535AE8FA6DBA ** get_address_of__context_10() { return &____context_10; }
	inline void set__context_10(Context_t04459FF3A5B7E8AF25B965F5717F535AE8FA6DBA * value)
	{
		____context_10 = value;
		Il2CppCodeGenWriteBarrier((&____context_10), value);
	}

	inline static int32_t get_offset_of__isFinalizingBinding_11() { return static_cast<int32_t>(offsetof(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5, ____isFinalizingBinding_11)); }
	inline bool get__isFinalizingBinding_11() const { return ____isFinalizingBinding_11; }
	inline bool* get_address_of__isFinalizingBinding_11() { return &____isFinalizingBinding_11; }
	inline void set__isFinalizingBinding_11(bool value)
	{
		____isFinalizingBinding_11 = value;
	}

	inline static int32_t get_offset_of__isValidating_12() { return static_cast<int32_t>(offsetof(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5, ____isValidating_12)); }
	inline bool get__isValidating_12() const { return ____isValidating_12; }
	inline bool* get_address_of__isValidating_12() { return &____isValidating_12; }
	inline void set__isValidating_12(bool value)
	{
		____isValidating_12 = value;
	}

	inline static int32_t get_offset_of__isInstalling_13() { return static_cast<int32_t>(offsetof(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5, ____isInstalling_13)); }
	inline bool get__isInstalling_13() const { return ____isInstalling_13; }
	inline bool* get_address_of__isInstalling_13() { return &____isInstalling_13; }
	inline void set__isInstalling_13(bool value)
	{
		____isInstalling_13 = value;
	}

	inline static int32_t get_offset_of__hasDisplayedInstallWarning_14() { return static_cast<int32_t>(offsetof(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5, ____hasDisplayedInstallWarning_14)); }
	inline bool get__hasDisplayedInstallWarning_14() const { return ____hasDisplayedInstallWarning_14; }
	inline bool* get_address_of__hasDisplayedInstallWarning_14() { return &____hasDisplayedInstallWarning_14; }
	inline void set__hasDisplayedInstallWarning_14(bool value)
	{
		____hasDisplayedInstallWarning_14 = value;
	}

	inline static int32_t get_offset_of_U3CShouldCheckForInstallWarningU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5, ___U3CShouldCheckForInstallWarningU3Ek__BackingField_15)); }
	inline bool get_U3CShouldCheckForInstallWarningU3Ek__BackingField_15() const { return ___U3CShouldCheckForInstallWarningU3Ek__BackingField_15; }
	inline bool* get_address_of_U3CShouldCheckForInstallWarningU3Ek__BackingField_15() { return &___U3CShouldCheckForInstallWarningU3Ek__BackingField_15; }
	inline void set_U3CShouldCheckForInstallWarningU3Ek__BackingField_15(bool value)
	{
		___U3CShouldCheckForInstallWarningU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3CAssertOnNewGameObjectsU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5, ___U3CAssertOnNewGameObjectsU3Ek__BackingField_16)); }
	inline bool get_U3CAssertOnNewGameObjectsU3Ek__BackingField_16() const { return ___U3CAssertOnNewGameObjectsU3Ek__BackingField_16; }
	inline bool* get_address_of_U3CAssertOnNewGameObjectsU3Ek__BackingField_16() { return &___U3CAssertOnNewGameObjectsU3Ek__BackingField_16; }
	inline void set_U3CAssertOnNewGameObjectsU3Ek__BackingField_16(bool value)
	{
		___U3CAssertOnNewGameObjectsU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CDefaultParentU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5, ___U3CDefaultParentU3Ek__BackingField_17)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_U3CDefaultParentU3Ek__BackingField_17() const { return ___U3CDefaultParentU3Ek__BackingField_17; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_U3CDefaultParentU3Ek__BackingField_17() { return &___U3CDefaultParentU3Ek__BackingField_17; }
	inline void set_U3CDefaultParentU3Ek__BackingField_17(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___U3CDefaultParentU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDefaultParentU3Ek__BackingField_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICONTAINER_T7619E999A5CE72FEE4D2419403214E62D95FFFD5_H
#ifndef U3CU3EC__DISPLAYCLASS181_0_1_TC62938210FC962790D274FA4E04218CD84EA7EB3_H
#define U3CU3EC__DISPLAYCLASS181_0_1_TC62938210FC962790D274FA4E04218CD84EA7EB3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.DiContainer_<>c__DisplayClass181_0`1<System.Object>
struct  U3CU3Ec__DisplayClass181_0_1_tC62938210FC962790D274FA4E04218CD84EA7EB3  : public RuntimeObject
{
public:
	// TContract Zenject.DiContainer_<>c__DisplayClass181_0`1::instance
	RuntimeObject * ___instance_0;

public:
	inline static int32_t get_offset_of_instance_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass181_0_1_tC62938210FC962790D274FA4E04218CD84EA7EB3, ___instance_0)); }
	inline RuntimeObject * get_instance_0() const { return ___instance_0; }
	inline RuntimeObject ** get_address_of_instance_0() { return &___instance_0; }
	inline void set_instance_0(RuntimeObject * value)
	{
		___instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS181_0_1_TC62938210FC962790D274FA4E04218CD84EA7EB3_H
#ifndef FACTORYBINDINFO_T13E5B099259F31F1EF31D3550FE94DF78C9F1C00_H
#define FACTORYBINDINFO_T13E5B099259F31F1EF31D3550FE94DF78C9F1C00_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.FactoryBindInfo
struct  FactoryBindInfo_t13E5B099259F31F1EF31D3550FE94DF78C9F1C00  : public RuntimeObject
{
public:
	// System.Type Zenject.FactoryBindInfo::<FactoryType>k__BackingField
	Type_t * ___U3CFactoryTypeU3Ek__BackingField_0;
	// System.Func`2<Zenject.DiContainer,Zenject.IProvider> Zenject.FactoryBindInfo::<ProviderFunc>k__BackingField
	Func_2_t3BE49542C4369B3A5CA2F7EFB4B994EA5A9C449C * ___U3CProviderFuncU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CFactoryTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(FactoryBindInfo_t13E5B099259F31F1EF31D3550FE94DF78C9F1C00, ___U3CFactoryTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CFactoryTypeU3Ek__BackingField_0() const { return ___U3CFactoryTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CFactoryTypeU3Ek__BackingField_0() { return &___U3CFactoryTypeU3Ek__BackingField_0; }
	inline void set_U3CFactoryTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CFactoryTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFactoryTypeU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CProviderFuncU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(FactoryBindInfo_t13E5B099259F31F1EF31D3550FE94DF78C9F1C00, ___U3CProviderFuncU3Ek__BackingField_1)); }
	inline Func_2_t3BE49542C4369B3A5CA2F7EFB4B994EA5A9C449C * get_U3CProviderFuncU3Ek__BackingField_1() const { return ___U3CProviderFuncU3Ek__BackingField_1; }
	inline Func_2_t3BE49542C4369B3A5CA2F7EFB4B994EA5A9C449C ** get_address_of_U3CProviderFuncU3Ek__BackingField_1() { return &___U3CProviderFuncU3Ek__BackingField_1; }
	inline void set_U3CProviderFuncU3Ek__BackingField_1(Func_2_t3BE49542C4369B3A5CA2F7EFB4B994EA5A9C449C * value)
	{
		___U3CProviderFuncU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CProviderFuncU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACTORYBINDINFO_T13E5B099259F31F1EF31D3550FE94DF78C9F1C00_H
#ifndef FACTORYPROVIDERBASE_2_T846258810464D2AA8EA9C0F78CBE910F211A7429_H
#define FACTORYPROVIDERBASE_2_T846258810464D2AA8EA9C0F78CBE910F211A7429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.FactoryProviderBase`2<System.Object,System.Object>
struct  FactoryProviderBase_2_t846258810464D2AA8EA9C0F78CBE910F211A7429  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.FactoryProviderBase`2::_factoryArgs
	List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * ____factoryArgs_0;
	// System.Object Zenject.FactoryProviderBase`2::_factory
	RuntimeObject * ____factory_1;
	// Zenject.DiContainer Zenject.FactoryProviderBase`2::<Container>k__BackingField
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ___U3CContainerU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of__factoryArgs_0() { return static_cast<int32_t>(offsetof(FactoryProviderBase_2_t846258810464D2AA8EA9C0F78CBE910F211A7429, ____factoryArgs_0)); }
	inline List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * get__factoryArgs_0() const { return ____factoryArgs_0; }
	inline List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 ** get_address_of__factoryArgs_0() { return &____factoryArgs_0; }
	inline void set__factoryArgs_0(List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * value)
	{
		____factoryArgs_0 = value;
		Il2CppCodeGenWriteBarrier((&____factoryArgs_0), value);
	}

	inline static int32_t get_offset_of__factory_1() { return static_cast<int32_t>(offsetof(FactoryProviderBase_2_t846258810464D2AA8EA9C0F78CBE910F211A7429, ____factory_1)); }
	inline RuntimeObject * get__factory_1() const { return ____factory_1; }
	inline RuntimeObject ** get_address_of__factory_1() { return &____factory_1; }
	inline void set__factory_1(RuntimeObject * value)
	{
		____factory_1 = value;
		Il2CppCodeGenWriteBarrier((&____factory_1), value);
	}

	inline static int32_t get_offset_of_U3CContainerU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(FactoryProviderBase_2_t846258810464D2AA8EA9C0F78CBE910F211A7429, ___U3CContainerU3Ek__BackingField_2)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get_U3CContainerU3Ek__BackingField_2() const { return ___U3CContainerU3Ek__BackingField_2; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of_U3CContainerU3Ek__BackingField_2() { return &___U3CContainerU3Ek__BackingField_2; }
	inline void set_U3CContainerU3Ek__BackingField_2(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		___U3CContainerU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CContainerU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACTORYPROVIDERBASE_2_T846258810464D2AA8EA9C0F78CBE910F211A7429_H
#ifndef FACTORYSUBCONTAINERBINDERBASE_1_T10122DEBCBD743FE20FF2CCC6889131951876194_H
#define FACTORYSUBCONTAINERBINDERBASE_1_T10122DEBCBD743FE20FF2CCC6889131951876194_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.FactorySubContainerBinderBase`1<System.Object>
struct  FactorySubContainerBinderBase_1_t10122DEBCBD743FE20FF2CCC6889131951876194  : public RuntimeObject
{
public:
	// Zenject.FactoryBindInfo Zenject.FactorySubContainerBinderBase`1::<FactoryBindInfo>k__BackingField
	FactoryBindInfo_t13E5B099259F31F1EF31D3550FE94DF78C9F1C00 * ___U3CFactoryBindInfoU3Ek__BackingField_0;
	// Zenject.BindInfo Zenject.FactorySubContainerBinderBase`1::<BindInfo>k__BackingField
	BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * ___U3CBindInfoU3Ek__BackingField_1;
	// System.Object Zenject.FactorySubContainerBinderBase`1::<SubIdentifier>k__BackingField
	RuntimeObject * ___U3CSubIdentifierU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CFactoryBindInfoU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(FactorySubContainerBinderBase_1_t10122DEBCBD743FE20FF2CCC6889131951876194, ___U3CFactoryBindInfoU3Ek__BackingField_0)); }
	inline FactoryBindInfo_t13E5B099259F31F1EF31D3550FE94DF78C9F1C00 * get_U3CFactoryBindInfoU3Ek__BackingField_0() const { return ___U3CFactoryBindInfoU3Ek__BackingField_0; }
	inline FactoryBindInfo_t13E5B099259F31F1EF31D3550FE94DF78C9F1C00 ** get_address_of_U3CFactoryBindInfoU3Ek__BackingField_0() { return &___U3CFactoryBindInfoU3Ek__BackingField_0; }
	inline void set_U3CFactoryBindInfoU3Ek__BackingField_0(FactoryBindInfo_t13E5B099259F31F1EF31D3550FE94DF78C9F1C00 * value)
	{
		___U3CFactoryBindInfoU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFactoryBindInfoU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CBindInfoU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(FactorySubContainerBinderBase_1_t10122DEBCBD743FE20FF2CCC6889131951876194, ___U3CBindInfoU3Ek__BackingField_1)); }
	inline BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * get_U3CBindInfoU3Ek__BackingField_1() const { return ___U3CBindInfoU3Ek__BackingField_1; }
	inline BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 ** get_address_of_U3CBindInfoU3Ek__BackingField_1() { return &___U3CBindInfoU3Ek__BackingField_1; }
	inline void set_U3CBindInfoU3Ek__BackingField_1(BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * value)
	{
		___U3CBindInfoU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBindInfoU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CSubIdentifierU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(FactorySubContainerBinderBase_1_t10122DEBCBD743FE20FF2CCC6889131951876194, ___U3CSubIdentifierU3Ek__BackingField_2)); }
	inline RuntimeObject * get_U3CSubIdentifierU3Ek__BackingField_2() const { return ___U3CSubIdentifierU3Ek__BackingField_2; }
	inline RuntimeObject ** get_address_of_U3CSubIdentifierU3Ek__BackingField_2() { return &___U3CSubIdentifierU3Ek__BackingField_2; }
	inline void set_U3CSubIdentifierU3Ek__BackingField_2(RuntimeObject * value)
	{
		___U3CSubIdentifierU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSubIdentifierU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACTORYSUBCONTAINERBINDERBASE_1_T10122DEBCBD743FE20FF2CCC6889131951876194_H
#ifndef U3CU3EC__DISPLAYCLASS35_0_1_TBEC171F34B86D93E7BC341C18B082EE87604E233_H
#define U3CU3EC__DISPLAYCLASS35_0_1_TBEC171F34B86D93E7BC341C18B082EE87604E233_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.FromBinder_<>c__DisplayClass35_0`1<System.Object>
struct  U3CU3Ec__DisplayClass35_0_1_tBEC171F34B86D93E7BC341C18B082EE87604E233  : public RuntimeObject
{
public:
	// System.Func`2<Zenject.InjectContext,TConcrete> Zenject.FromBinder_<>c__DisplayClass35_0`1::method
	Func_2_tE24EB23C7D8F4AD0A3A34C5DB2C9F329886F48F2 * ___method_0;

public:
	inline static int32_t get_offset_of_method_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass35_0_1_tBEC171F34B86D93E7BC341C18B082EE87604E233, ___method_0)); }
	inline Func_2_tE24EB23C7D8F4AD0A3A34C5DB2C9F329886F48F2 * get_method_0() const { return ___method_0; }
	inline Func_2_tE24EB23C7D8F4AD0A3A34C5DB2C9F329886F48F2 ** get_address_of_method_0() { return &___method_0; }
	inline void set_method_0(Func_2_tE24EB23C7D8F4AD0A3A34C5DB2C9F329886F48F2 * value)
	{
		___method_0 = value;
		Il2CppCodeGenWriteBarrier((&___method_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS35_0_1_TBEC171F34B86D93E7BC341C18B082EE87604E233_H
#ifndef U3CU3EC__DISPLAYCLASS36_0_1_T02B1D9C112A722BCD5BC4A3CA54F5C5B13A17266_H
#define U3CU3EC__DISPLAYCLASS36_0_1_T02B1D9C112A722BCD5BC4A3CA54F5C5B13A17266_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.FromBinder_<>c__DisplayClass36_0`1<System.Object>
struct  U3CU3Ec__DisplayClass36_0_1_t02B1D9C112A722BCD5BC4A3CA54F5C5B13A17266  : public RuntimeObject
{
public:
	// System.Func`2<Zenject.InjectContext,System.Collections.Generic.IEnumerable`1<TConcrete>> Zenject.FromBinder_<>c__DisplayClass36_0`1::method
	Func_2_tEB18CC35489403B1DD34E84D07D6EBE38C06F980 * ___method_0;

public:
	inline static int32_t get_offset_of_method_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass36_0_1_t02B1D9C112A722BCD5BC4A3CA54F5C5B13A17266, ___method_0)); }
	inline Func_2_tEB18CC35489403B1DD34E84D07D6EBE38C06F980 * get_method_0() const { return ___method_0; }
	inline Func_2_tEB18CC35489403B1DD34E84D07D6EBE38C06F980 ** get_address_of_method_0() { return &___method_0; }
	inline void set_method_0(Func_2_tEB18CC35489403B1DD34E84D07D6EBE38C06F980 * value)
	{
		___method_0 = value;
		Il2CppCodeGenWriteBarrier((&___method_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS36_0_1_T02B1D9C112A722BCD5BC4A3CA54F5C5B13A17266_H
#ifndef U3CU3EC__DISPLAYCLASS38_0_2_TF5C66905CC804ED68CBE4C8EDF02C23BC57797A6_H
#define U3CU3EC__DISPLAYCLASS38_0_2_TF5C66905CC804ED68CBE4C8EDF02C23BC57797A6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.FromBinder_<>c__DisplayClass38_0`2<System.Object,System.Object>
struct  U3CU3Ec__DisplayClass38_0_2_tF5C66905CC804ED68CBE4C8EDF02C23BC57797A6  : public RuntimeObject
{
public:
	// System.Object Zenject.FromBinder_<>c__DisplayClass38_0`2::identifier
	RuntimeObject * ___identifier_0;
	// System.Func`2<TObj,TResult> Zenject.FromBinder_<>c__DisplayClass38_0`2::method
	Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * ___method_1;

public:
	inline static int32_t get_offset_of_identifier_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass38_0_2_tF5C66905CC804ED68CBE4C8EDF02C23BC57797A6, ___identifier_0)); }
	inline RuntimeObject * get_identifier_0() const { return ___identifier_0; }
	inline RuntimeObject ** get_address_of_identifier_0() { return &___identifier_0; }
	inline void set_identifier_0(RuntimeObject * value)
	{
		___identifier_0 = value;
		Il2CppCodeGenWriteBarrier((&___identifier_0), value);
	}

	inline static int32_t get_offset_of_method_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass38_0_2_tF5C66905CC804ED68CBE4C8EDF02C23BC57797A6, ___method_1)); }
	inline Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * get_method_1() const { return ___method_1; }
	inline Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 ** get_address_of_method_1() { return &___method_1; }
	inline void set_method_1(Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * value)
	{
		___method_1 = value;
		Il2CppCodeGenWriteBarrier((&___method_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS38_0_2_TF5C66905CC804ED68CBE4C8EDF02C23BC57797A6_H
#ifndef INJECTUTIL_T0C34AD40453372DB40929117C9254504D6F619FD_H
#define INJECTUTIL_T0C34AD40453372DB40929117C9254504D6F619FD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.InjectUtil
struct  InjectUtil_t0C34AD40453372DB40929117C9254504D6F619FD  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INJECTUTIL_T0C34AD40453372DB40929117C9254504D6F619FD_H
#ifndef NONLAZYBINDER_TA9FB0768A2F39559FBDDCAFBF940AAD240B7C43C_H
#define NONLAZYBINDER_TA9FB0768A2F39559FBDDCAFBF940AAD240B7C43C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.NonLazyBinder
struct  NonLazyBinder_tA9FB0768A2F39559FBDDCAFBF940AAD240B7C43C  : public RuntimeObject
{
public:
	// Zenject.BindInfo Zenject.NonLazyBinder::<BindInfo>k__BackingField
	BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * ___U3CBindInfoU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CBindInfoU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(NonLazyBinder_tA9FB0768A2F39559FBDDCAFBF940AAD240B7C43C, ___U3CBindInfoU3Ek__BackingField_0)); }
	inline BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * get_U3CBindInfoU3Ek__BackingField_0() const { return ___U3CBindInfoU3Ek__BackingField_0; }
	inline BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 ** get_address_of_U3CBindInfoU3Ek__BackingField_0() { return &___U3CBindInfoU3Ek__BackingField_0; }
	inline void set_U3CBindInfoU3Ek__BackingField_0(BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * value)
	{
		___U3CBindInfoU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBindInfoU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NONLAZYBINDER_TA9FB0768A2F39559FBDDCAFBF940AAD240B7C43C_H
#ifndef NULLBINDINGFINALIZER_TFEAF0122BA4AF8A5209DE0330F0BFF444F06AD65_H
#define NULLBINDINGFINALIZER_TFEAF0122BA4AF8A5209DE0330F0BFF444F06AD65_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.NullBindingFinalizer
struct  NullBindingFinalizer_tFEAF0122BA4AF8A5209DE0330F0BFF444F06AD65  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLBINDINGFINALIZER_TFEAF0122BA4AF8A5209DE0330F0BFF444F06AD65_H
#ifndef PROVIDERBINDINGFINALIZER_T3C2E6FBD89B98A057674B926F42EF13E4ED1EAAE_H
#define PROVIDERBINDINGFINALIZER_T3C2E6FBD89B98A057674B926F42EF13E4ED1EAAE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ProviderBindingFinalizer
struct  ProviderBindingFinalizer_t3C2E6FBD89B98A057674B926F42EF13E4ED1EAAE  : public RuntimeObject
{
public:
	// Zenject.BindInfo Zenject.ProviderBindingFinalizer::<BindInfo>k__BackingField
	BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * ___U3CBindInfoU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CBindInfoU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ProviderBindingFinalizer_t3C2E6FBD89B98A057674B926F42EF13E4ED1EAAE, ___U3CBindInfoU3Ek__BackingField_0)); }
	inline BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * get_U3CBindInfoU3Ek__BackingField_0() const { return ___U3CBindInfoU3Ek__BackingField_0; }
	inline BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 ** get_address_of_U3CBindInfoU3Ek__BackingField_0() { return &___U3CBindInfoU3Ek__BackingField_0; }
	inline void set_U3CBindInfoU3Ek__BackingField_0(BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * value)
	{
		___U3CBindInfoU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBindInfoU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROVIDERBINDINGFINALIZER_T3C2E6FBD89B98A057674B926F42EF13E4ED1EAAE_H
#ifndef SIGNALEXTENSIONS_T33C6464932CFDFDA4BEB1F734024765C8A955F25_H
#define SIGNALEXTENSIONS_T33C6464932CFDFDA4BEB1F734024765C8A955F25_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SignalExtensions
struct  SignalExtensions_t33C6464932CFDFDA4BEB1F734024765C8A955F25  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGNALEXTENSIONS_T33C6464932CFDFDA4BEB1F734024765C8A955F25_H
#ifndef SIGNALHANDLERBINDER_T4AB70F7A7BD1596D46B88F834A400EBEDE185771_H
#define SIGNALHANDLERBINDER_T4AB70F7A7BD1596D46B88F834A400EBEDE185771_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SignalHandlerBinder
struct  SignalHandlerBinder_t4AB70F7A7BD1596D46B88F834A400EBEDE185771  : public RuntimeObject
{
public:
	// Zenject.BindFinalizerWrapper Zenject.SignalHandlerBinder::_finalizerWrapper
	BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 * ____finalizerWrapper_0;
	// System.Type Zenject.SignalHandlerBinder::_signalType
	Type_t * ____signalType_1;
	// Zenject.DiContainer Zenject.SignalHandlerBinder::_container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____container_2;
	// System.Object Zenject.SignalHandlerBinder::<Identifier>k__BackingField
	RuntimeObject * ___U3CIdentifierU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of__finalizerWrapper_0() { return static_cast<int32_t>(offsetof(SignalHandlerBinder_t4AB70F7A7BD1596D46B88F834A400EBEDE185771, ____finalizerWrapper_0)); }
	inline BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 * get__finalizerWrapper_0() const { return ____finalizerWrapper_0; }
	inline BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 ** get_address_of__finalizerWrapper_0() { return &____finalizerWrapper_0; }
	inline void set__finalizerWrapper_0(BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 * value)
	{
		____finalizerWrapper_0 = value;
		Il2CppCodeGenWriteBarrier((&____finalizerWrapper_0), value);
	}

	inline static int32_t get_offset_of__signalType_1() { return static_cast<int32_t>(offsetof(SignalHandlerBinder_t4AB70F7A7BD1596D46B88F834A400EBEDE185771, ____signalType_1)); }
	inline Type_t * get__signalType_1() const { return ____signalType_1; }
	inline Type_t ** get_address_of__signalType_1() { return &____signalType_1; }
	inline void set__signalType_1(Type_t * value)
	{
		____signalType_1 = value;
		Il2CppCodeGenWriteBarrier((&____signalType_1), value);
	}

	inline static int32_t get_offset_of__container_2() { return static_cast<int32_t>(offsetof(SignalHandlerBinder_t4AB70F7A7BD1596D46B88F834A400EBEDE185771, ____container_2)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__container_2() const { return ____container_2; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__container_2() { return &____container_2; }
	inline void set__container_2(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____container_2 = value;
		Il2CppCodeGenWriteBarrier((&____container_2), value);
	}

	inline static int32_t get_offset_of_U3CIdentifierU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(SignalHandlerBinder_t4AB70F7A7BD1596D46B88F834A400EBEDE185771, ___U3CIdentifierU3Ek__BackingField_3)); }
	inline RuntimeObject * get_U3CIdentifierU3Ek__BackingField_3() const { return ___U3CIdentifierU3Ek__BackingField_3; }
	inline RuntimeObject ** get_address_of_U3CIdentifierU3Ek__BackingField_3() { return &___U3CIdentifierU3Ek__BackingField_3; }
	inline void set_U3CIdentifierU3Ek__BackingField_3(RuntimeObject * value)
	{
		___U3CIdentifierU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CIdentifierU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGNALHANDLERBINDER_T4AB70F7A7BD1596D46B88F834A400EBEDE185771_H
#ifndef SIGNALHANDLERBINDER_1_TAC85A1AD98374941739974445C41BF8617CF78FC_H
#define SIGNALHANDLERBINDER_1_TAC85A1AD98374941739974445C41BF8617CF78FC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SignalHandlerBinder`1<System.Object>
struct  SignalHandlerBinder_1_tAC85A1AD98374941739974445C41BF8617CF78FC  : public RuntimeObject
{
public:
	// Zenject.BindFinalizerWrapper Zenject.SignalHandlerBinder`1::_finalizerWrapper
	BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 * ____finalizerWrapper_0;
	// System.Type Zenject.SignalHandlerBinder`1::_signalType
	Type_t * ____signalType_1;
	// Zenject.DiContainer Zenject.SignalHandlerBinder`1::_container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____container_2;
	// System.Object Zenject.SignalHandlerBinder`1::<Identifier>k__BackingField
	RuntimeObject * ___U3CIdentifierU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of__finalizerWrapper_0() { return static_cast<int32_t>(offsetof(SignalHandlerBinder_1_tAC85A1AD98374941739974445C41BF8617CF78FC, ____finalizerWrapper_0)); }
	inline BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 * get__finalizerWrapper_0() const { return ____finalizerWrapper_0; }
	inline BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 ** get_address_of__finalizerWrapper_0() { return &____finalizerWrapper_0; }
	inline void set__finalizerWrapper_0(BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 * value)
	{
		____finalizerWrapper_0 = value;
		Il2CppCodeGenWriteBarrier((&____finalizerWrapper_0), value);
	}

	inline static int32_t get_offset_of__signalType_1() { return static_cast<int32_t>(offsetof(SignalHandlerBinder_1_tAC85A1AD98374941739974445C41BF8617CF78FC, ____signalType_1)); }
	inline Type_t * get__signalType_1() const { return ____signalType_1; }
	inline Type_t ** get_address_of__signalType_1() { return &____signalType_1; }
	inline void set__signalType_1(Type_t * value)
	{
		____signalType_1 = value;
		Il2CppCodeGenWriteBarrier((&____signalType_1), value);
	}

	inline static int32_t get_offset_of__container_2() { return static_cast<int32_t>(offsetof(SignalHandlerBinder_1_tAC85A1AD98374941739974445C41BF8617CF78FC, ____container_2)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__container_2() const { return ____container_2; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__container_2() { return &____container_2; }
	inline void set__container_2(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____container_2 = value;
		Il2CppCodeGenWriteBarrier((&____container_2), value);
	}

	inline static int32_t get_offset_of_U3CIdentifierU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(SignalHandlerBinder_1_tAC85A1AD98374941739974445C41BF8617CF78FC, ___U3CIdentifierU3Ek__BackingField_3)); }
	inline RuntimeObject * get_U3CIdentifierU3Ek__BackingField_3() const { return ___U3CIdentifierU3Ek__BackingField_3; }
	inline RuntimeObject ** get_address_of_U3CIdentifierU3Ek__BackingField_3() { return &___U3CIdentifierU3Ek__BackingField_3; }
	inline void set_U3CIdentifierU3Ek__BackingField_3(RuntimeObject * value)
	{
		___U3CIdentifierU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CIdentifierU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGNALHANDLERBINDER_1_TAC85A1AD98374941739974445C41BF8617CF78FC_H
#ifndef SIGNALHANDLERBINDER_2_T6F32E8CD8248414B4F3681F2E4D8BFB658F24D30_H
#define SIGNALHANDLERBINDER_2_T6F32E8CD8248414B4F3681F2E4D8BFB658F24D30_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SignalHandlerBinder`2<System.Object,System.Object>
struct  SignalHandlerBinder_2_t6F32E8CD8248414B4F3681F2E4D8BFB658F24D30  : public RuntimeObject
{
public:
	// Zenject.BindFinalizerWrapper Zenject.SignalHandlerBinder`2::_finalizerWrapper
	BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 * ____finalizerWrapper_0;
	// System.Type Zenject.SignalHandlerBinder`2::_signalType
	Type_t * ____signalType_1;
	// Zenject.DiContainer Zenject.SignalHandlerBinder`2::_container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____container_2;
	// System.Object Zenject.SignalHandlerBinder`2::<Identifier>k__BackingField
	RuntimeObject * ___U3CIdentifierU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of__finalizerWrapper_0() { return static_cast<int32_t>(offsetof(SignalHandlerBinder_2_t6F32E8CD8248414B4F3681F2E4D8BFB658F24D30, ____finalizerWrapper_0)); }
	inline BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 * get__finalizerWrapper_0() const { return ____finalizerWrapper_0; }
	inline BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 ** get_address_of__finalizerWrapper_0() { return &____finalizerWrapper_0; }
	inline void set__finalizerWrapper_0(BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 * value)
	{
		____finalizerWrapper_0 = value;
		Il2CppCodeGenWriteBarrier((&____finalizerWrapper_0), value);
	}

	inline static int32_t get_offset_of__signalType_1() { return static_cast<int32_t>(offsetof(SignalHandlerBinder_2_t6F32E8CD8248414B4F3681F2E4D8BFB658F24D30, ____signalType_1)); }
	inline Type_t * get__signalType_1() const { return ____signalType_1; }
	inline Type_t ** get_address_of__signalType_1() { return &____signalType_1; }
	inline void set__signalType_1(Type_t * value)
	{
		____signalType_1 = value;
		Il2CppCodeGenWriteBarrier((&____signalType_1), value);
	}

	inline static int32_t get_offset_of__container_2() { return static_cast<int32_t>(offsetof(SignalHandlerBinder_2_t6F32E8CD8248414B4F3681F2E4D8BFB658F24D30, ____container_2)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__container_2() const { return ____container_2; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__container_2() { return &____container_2; }
	inline void set__container_2(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____container_2 = value;
		Il2CppCodeGenWriteBarrier((&____container_2), value);
	}

	inline static int32_t get_offset_of_U3CIdentifierU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(SignalHandlerBinder_2_t6F32E8CD8248414B4F3681F2E4D8BFB658F24D30, ___U3CIdentifierU3Ek__BackingField_3)); }
	inline RuntimeObject * get_U3CIdentifierU3Ek__BackingField_3() const { return ___U3CIdentifierU3Ek__BackingField_3; }
	inline RuntimeObject ** get_address_of_U3CIdentifierU3Ek__BackingField_3() { return &___U3CIdentifierU3Ek__BackingField_3; }
	inline void set_U3CIdentifierU3Ek__BackingField_3(RuntimeObject * value)
	{
		___U3CIdentifierU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CIdentifierU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGNALHANDLERBINDER_2_T6F32E8CD8248414B4F3681F2E4D8BFB658F24D30_H
#ifndef SIGNALHANDLERBINDER_3_T9ADF0354A52CA5EFF23194E48F870113CF337055_H
#define SIGNALHANDLERBINDER_3_T9ADF0354A52CA5EFF23194E48F870113CF337055_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SignalHandlerBinder`3<System.Object,System.Object,System.Object>
struct  SignalHandlerBinder_3_t9ADF0354A52CA5EFF23194E48F870113CF337055  : public RuntimeObject
{
public:
	// Zenject.BindFinalizerWrapper Zenject.SignalHandlerBinder`3::_finalizerWrapper
	BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 * ____finalizerWrapper_0;
	// System.Type Zenject.SignalHandlerBinder`3::_signalType
	Type_t * ____signalType_1;
	// Zenject.DiContainer Zenject.SignalHandlerBinder`3::_container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____container_2;
	// System.Object Zenject.SignalHandlerBinder`3::<Identifier>k__BackingField
	RuntimeObject * ___U3CIdentifierU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of__finalizerWrapper_0() { return static_cast<int32_t>(offsetof(SignalHandlerBinder_3_t9ADF0354A52CA5EFF23194E48F870113CF337055, ____finalizerWrapper_0)); }
	inline BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 * get__finalizerWrapper_0() const { return ____finalizerWrapper_0; }
	inline BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 ** get_address_of__finalizerWrapper_0() { return &____finalizerWrapper_0; }
	inline void set__finalizerWrapper_0(BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 * value)
	{
		____finalizerWrapper_0 = value;
		Il2CppCodeGenWriteBarrier((&____finalizerWrapper_0), value);
	}

	inline static int32_t get_offset_of__signalType_1() { return static_cast<int32_t>(offsetof(SignalHandlerBinder_3_t9ADF0354A52CA5EFF23194E48F870113CF337055, ____signalType_1)); }
	inline Type_t * get__signalType_1() const { return ____signalType_1; }
	inline Type_t ** get_address_of__signalType_1() { return &____signalType_1; }
	inline void set__signalType_1(Type_t * value)
	{
		____signalType_1 = value;
		Il2CppCodeGenWriteBarrier((&____signalType_1), value);
	}

	inline static int32_t get_offset_of__container_2() { return static_cast<int32_t>(offsetof(SignalHandlerBinder_3_t9ADF0354A52CA5EFF23194E48F870113CF337055, ____container_2)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__container_2() const { return ____container_2; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__container_2() { return &____container_2; }
	inline void set__container_2(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____container_2 = value;
		Il2CppCodeGenWriteBarrier((&____container_2), value);
	}

	inline static int32_t get_offset_of_U3CIdentifierU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(SignalHandlerBinder_3_t9ADF0354A52CA5EFF23194E48F870113CF337055, ___U3CIdentifierU3Ek__BackingField_3)); }
	inline RuntimeObject * get_U3CIdentifierU3Ek__BackingField_3() const { return ___U3CIdentifierU3Ek__BackingField_3; }
	inline RuntimeObject ** get_address_of_U3CIdentifierU3Ek__BackingField_3() { return &___U3CIdentifierU3Ek__BackingField_3; }
	inline void set_U3CIdentifierU3Ek__BackingField_3(RuntimeObject * value)
	{
		___U3CIdentifierU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CIdentifierU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGNALHANDLERBINDER_3_T9ADF0354A52CA5EFF23194E48F870113CF337055_H
#ifndef SIGNALHANDLERBINDER_4_T6088EB5507EA2460DDEA35BEFBCE49C3F0F851DC_H
#define SIGNALHANDLERBINDER_4_T6088EB5507EA2460DDEA35BEFBCE49C3F0F851DC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SignalHandlerBinder`4<System.Object,System.Object,System.Object,System.Object>
struct  SignalHandlerBinder_4_t6088EB5507EA2460DDEA35BEFBCE49C3F0F851DC  : public RuntimeObject
{
public:
	// Zenject.BindFinalizerWrapper Zenject.SignalHandlerBinder`4::_finalizerWrapper
	BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 * ____finalizerWrapper_0;
	// System.Type Zenject.SignalHandlerBinder`4::_signalType
	Type_t * ____signalType_1;
	// Zenject.DiContainer Zenject.SignalHandlerBinder`4::_container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____container_2;
	// System.Object Zenject.SignalHandlerBinder`4::<Identifier>k__BackingField
	RuntimeObject * ___U3CIdentifierU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of__finalizerWrapper_0() { return static_cast<int32_t>(offsetof(SignalHandlerBinder_4_t6088EB5507EA2460DDEA35BEFBCE49C3F0F851DC, ____finalizerWrapper_0)); }
	inline BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 * get__finalizerWrapper_0() const { return ____finalizerWrapper_0; }
	inline BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 ** get_address_of__finalizerWrapper_0() { return &____finalizerWrapper_0; }
	inline void set__finalizerWrapper_0(BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 * value)
	{
		____finalizerWrapper_0 = value;
		Il2CppCodeGenWriteBarrier((&____finalizerWrapper_0), value);
	}

	inline static int32_t get_offset_of__signalType_1() { return static_cast<int32_t>(offsetof(SignalHandlerBinder_4_t6088EB5507EA2460DDEA35BEFBCE49C3F0F851DC, ____signalType_1)); }
	inline Type_t * get__signalType_1() const { return ____signalType_1; }
	inline Type_t ** get_address_of__signalType_1() { return &____signalType_1; }
	inline void set__signalType_1(Type_t * value)
	{
		____signalType_1 = value;
		Il2CppCodeGenWriteBarrier((&____signalType_1), value);
	}

	inline static int32_t get_offset_of__container_2() { return static_cast<int32_t>(offsetof(SignalHandlerBinder_4_t6088EB5507EA2460DDEA35BEFBCE49C3F0F851DC, ____container_2)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__container_2() const { return ____container_2; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__container_2() { return &____container_2; }
	inline void set__container_2(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____container_2 = value;
		Il2CppCodeGenWriteBarrier((&____container_2), value);
	}

	inline static int32_t get_offset_of_U3CIdentifierU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(SignalHandlerBinder_4_t6088EB5507EA2460DDEA35BEFBCE49C3F0F851DC, ___U3CIdentifierU3Ek__BackingField_3)); }
	inline RuntimeObject * get_U3CIdentifierU3Ek__BackingField_3() const { return ___U3CIdentifierU3Ek__BackingField_3; }
	inline RuntimeObject ** get_address_of_U3CIdentifierU3Ek__BackingField_3() { return &___U3CIdentifierU3Ek__BackingField_3; }
	inline void set_U3CIdentifierU3Ek__BackingField_3(RuntimeObject * value)
	{
		___U3CIdentifierU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CIdentifierU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGNALHANDLERBINDER_4_T6088EB5507EA2460DDEA35BEFBCE49C3F0F851DC_H
#ifndef SIGNALSETTINGS_TD753AA3D16CEA36F97CDAEC5428C5217F1EB6540_H
#define SIGNALSETTINGS_TD753AA3D16CEA36F97CDAEC5428C5217F1EB6540_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SignalSettings
struct  SignalSettings_tD753AA3D16CEA36F97CDAEC5428C5217F1EB6540  : public RuntimeObject
{
public:
	// System.Boolean Zenject.SignalSettings::RequiresHandler
	bool ___RequiresHandler_0;

public:
	inline static int32_t get_offset_of_RequiresHandler_0() { return static_cast<int32_t>(offsetof(SignalSettings_tD753AA3D16CEA36F97CDAEC5428C5217F1EB6540, ___RequiresHandler_0)); }
	inline bool get_RequiresHandler_0() const { return ___RequiresHandler_0; }
	inline bool* get_address_of_RequiresHandler_0() { return &___RequiresHandler_0; }
	inline void set_RequiresHandler_0(bool value)
	{
		___RequiresHandler_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGNALSETTINGS_TD753AA3D16CEA36F97CDAEC5428C5217F1EB6540_H
#ifndef TOGETTER_T269C4EED29F0AC2A1C9FB4061EA228DC2DDADED4_H
#define TOGETTER_T269C4EED29F0AC2A1C9FB4061EA228DC2DDADED4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SingletonImplIds_ToGetter
struct  ToGetter_t269C4EED29F0AC2A1C9FB4061EA228DC2DDADED4  : public RuntimeObject
{
public:
	// System.Delegate Zenject.SingletonImplIds_ToGetter::_method
	Delegate_t * ____method_0;
	// System.Object Zenject.SingletonImplIds_ToGetter::_identifier
	RuntimeObject * ____identifier_1;

public:
	inline static int32_t get_offset_of__method_0() { return static_cast<int32_t>(offsetof(ToGetter_t269C4EED29F0AC2A1C9FB4061EA228DC2DDADED4, ____method_0)); }
	inline Delegate_t * get__method_0() const { return ____method_0; }
	inline Delegate_t ** get_address_of__method_0() { return &____method_0; }
	inline void set__method_0(Delegate_t * value)
	{
		____method_0 = value;
		Il2CppCodeGenWriteBarrier((&____method_0), value);
	}

	inline static int32_t get_offset_of__identifier_1() { return static_cast<int32_t>(offsetof(ToGetter_t269C4EED29F0AC2A1C9FB4061EA228DC2DDADED4, ____identifier_1)); }
	inline RuntimeObject * get__identifier_1() const { return ____identifier_1; }
	inline RuntimeObject ** get_address_of__identifier_1() { return &____identifier_1; }
	inline void set__identifier_1(RuntimeObject * value)
	{
		____identifier_1 = value;
		Il2CppCodeGenWriteBarrier((&____identifier_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOGETTER_T269C4EED29F0AC2A1C9FB4061EA228DC2DDADED4_H
#ifndef TOMETHOD_T12F5E0A4D9C370BB507C42523A4401D55EBE2836_H
#define TOMETHOD_T12F5E0A4D9C370BB507C42523A4401D55EBE2836_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SingletonImplIds_ToMethod
struct  ToMethod_t12F5E0A4D9C370BB507C42523A4401D55EBE2836  : public RuntimeObject
{
public:
	// System.Delegate Zenject.SingletonImplIds_ToMethod::_method
	Delegate_t * ____method_0;

public:
	inline static int32_t get_offset_of__method_0() { return static_cast<int32_t>(offsetof(ToMethod_t12F5E0A4D9C370BB507C42523A4401D55EBE2836, ____method_0)); }
	inline Delegate_t * get__method_0() const { return ____method_0; }
	inline Delegate_t ** get_address_of__method_0() { return &____method_0; }
	inline void set__method_0(Delegate_t * value)
	{
		____method_0 = value;
		Il2CppCodeGenWriteBarrier((&____method_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOMETHOD_T12F5E0A4D9C370BB507C42523A4401D55EBE2836_H
#ifndef SUBCONTAINERBINDER_T24411FF88DADEE6DFBC16EE722E0F4944E8732D6_H
#define SUBCONTAINERBINDER_T24411FF88DADEE6DFBC16EE722E0F4944E8732D6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerBinder
struct  SubContainerBinder_t24411FF88DADEE6DFBC16EE722E0F4944E8732D6  : public RuntimeObject
{
public:
	// Zenject.BindInfo Zenject.SubContainerBinder::_bindInfo
	BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * ____bindInfo_0;
	// Zenject.BindFinalizerWrapper Zenject.SubContainerBinder::_finalizerWrapper
	BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 * ____finalizerWrapper_1;
	// System.Object Zenject.SubContainerBinder::_subIdentifier
	RuntimeObject * ____subIdentifier_2;

public:
	inline static int32_t get_offset_of__bindInfo_0() { return static_cast<int32_t>(offsetof(SubContainerBinder_t24411FF88DADEE6DFBC16EE722E0F4944E8732D6, ____bindInfo_0)); }
	inline BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * get__bindInfo_0() const { return ____bindInfo_0; }
	inline BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 ** get_address_of__bindInfo_0() { return &____bindInfo_0; }
	inline void set__bindInfo_0(BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * value)
	{
		____bindInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&____bindInfo_0), value);
	}

	inline static int32_t get_offset_of__finalizerWrapper_1() { return static_cast<int32_t>(offsetof(SubContainerBinder_t24411FF88DADEE6DFBC16EE722E0F4944E8732D6, ____finalizerWrapper_1)); }
	inline BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 * get__finalizerWrapper_1() const { return ____finalizerWrapper_1; }
	inline BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 ** get_address_of__finalizerWrapper_1() { return &____finalizerWrapper_1; }
	inline void set__finalizerWrapper_1(BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 * value)
	{
		____finalizerWrapper_1 = value;
		Il2CppCodeGenWriteBarrier((&____finalizerWrapper_1), value);
	}

	inline static int32_t get_offset_of__subIdentifier_2() { return static_cast<int32_t>(offsetof(SubContainerBinder_t24411FF88DADEE6DFBC16EE722E0F4944E8732D6, ____subIdentifier_2)); }
	inline RuntimeObject * get__subIdentifier_2() const { return ____subIdentifier_2; }
	inline RuntimeObject ** get_address_of__subIdentifier_2() { return &____subIdentifier_2; }
	inline void set__subIdentifier_2(RuntimeObject * value)
	{
		____subIdentifier_2 = value;
		Il2CppCodeGenWriteBarrier((&____subIdentifier_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBCONTAINERBINDER_T24411FF88DADEE6DFBC16EE722E0F4944E8732D6_H
#ifndef TYPEANALYZER_T19EB8063B9BA23AD8FAAB2AF5228A8D8D086F722_H
#define TYPEANALYZER_T19EB8063B9BA23AD8FAAB2AF5228A8D8D086F722_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.TypeAnalyzer
struct  TypeAnalyzer_t19EB8063B9BA23AD8FAAB2AF5228A8D8D086F722  : public RuntimeObject
{
public:

public:
};

struct TypeAnalyzer_t19EB8063B9BA23AD8FAAB2AF5228A8D8D086F722_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Type,Zenject.ZenjectTypeInfo> Zenject.TypeAnalyzer::_typeInfo
	Dictionary_2_t6BA2F0666D38DF096F45B41AB67474768713CE58 * ____typeInfo_0;

public:
	inline static int32_t get_offset_of__typeInfo_0() { return static_cast<int32_t>(offsetof(TypeAnalyzer_t19EB8063B9BA23AD8FAAB2AF5228A8D8D086F722_StaticFields, ____typeInfo_0)); }
	inline Dictionary_2_t6BA2F0666D38DF096F45B41AB67474768713CE58 * get__typeInfo_0() const { return ____typeInfo_0; }
	inline Dictionary_2_t6BA2F0666D38DF096F45B41AB67474768713CE58 ** get_address_of__typeInfo_0() { return &____typeInfo_0; }
	inline void set__typeInfo_0(Dictionary_2_t6BA2F0666D38DF096F45B41AB67474768713CE58 * value)
	{
		____typeInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&____typeInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEANALYZER_T19EB8063B9BA23AD8FAAB2AF5228A8D8D086F722_H
#ifndef TYPEVALUEPAIR_TD309CDC658476FDDF08345C92D3F4D7A0A1F9792_H
#define TYPEVALUEPAIR_TD309CDC658476FDDF08345C92D3F4D7A0A1F9792_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.TypeValuePair
struct  TypeValuePair_tD309CDC658476FDDF08345C92D3F4D7A0A1F9792  : public RuntimeObject
{
public:
	// System.Type Zenject.TypeValuePair::Type
	Type_t * ___Type_0;
	// System.Object Zenject.TypeValuePair::Value
	RuntimeObject * ___Value_1;

public:
	inline static int32_t get_offset_of_Type_0() { return static_cast<int32_t>(offsetof(TypeValuePair_tD309CDC658476FDDF08345C92D3F4D7A0A1F9792, ___Type_0)); }
	inline Type_t * get_Type_0() const { return ___Type_0; }
	inline Type_t ** get_address_of_Type_0() { return &___Type_0; }
	inline void set_Type_0(Type_t * value)
	{
		___Type_0 = value;
		Il2CppCodeGenWriteBarrier((&___Type_0), value);
	}

	inline static int32_t get_offset_of_Value_1() { return static_cast<int32_t>(offsetof(TypeValuePair_tD309CDC658476FDDF08345C92D3F4D7A0A1F9792, ___Value_1)); }
	inline RuntimeObject * get_Value_1() const { return ___Value_1; }
	inline RuntimeObject ** get_address_of_Value_1() { return &___Value_1; }
	inline void set_Value_1(RuntimeObject * value)
	{
		___Value_1 = value;
		Il2CppCodeGenWriteBarrier((&___Value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEVALUEPAIR_TD309CDC658476FDDF08345C92D3F4D7A0A1F9792_H
#ifndef ZENJECTTYPEINFO_T2866A662C0FDA5B357BCC135021933EE8914535E_H
#define ZENJECTTYPEINFO_T2866A662C0FDA5B357BCC135021933EE8914535E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ZenjectTypeInfo
struct  ZenjectTypeInfo_t2866A662C0FDA5B357BCC135021933EE8914535E  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Zenject.PostInjectableInfo> Zenject.ZenjectTypeInfo::_postInjectMethods
	List_1_t8AA9EFA21EF1045CBB5A905EB31B2D66A23339FB * ____postInjectMethods_0;
	// System.Collections.Generic.List`1<Zenject.InjectableInfo> Zenject.ZenjectTypeInfo::_constructorInjectables
	List_1_t19FD687049A90924DCEA81F81C85555F15021A68 * ____constructorInjectables_1;
	// System.Collections.Generic.List`1<Zenject.InjectableInfo> Zenject.ZenjectTypeInfo::_fieldInjectables
	List_1_t19FD687049A90924DCEA81F81C85555F15021A68 * ____fieldInjectables_2;
	// System.Collections.Generic.List`1<Zenject.InjectableInfo> Zenject.ZenjectTypeInfo::_propertyInjectables
	List_1_t19FD687049A90924DCEA81F81C85555F15021A68 * ____propertyInjectables_3;
	// System.Reflection.ConstructorInfo Zenject.ZenjectTypeInfo::_injectConstructor
	ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF * ____injectConstructor_4;
	// System.Type Zenject.ZenjectTypeInfo::_typeAnalyzed
	Type_t * ____typeAnalyzed_5;

public:
	inline static int32_t get_offset_of__postInjectMethods_0() { return static_cast<int32_t>(offsetof(ZenjectTypeInfo_t2866A662C0FDA5B357BCC135021933EE8914535E, ____postInjectMethods_0)); }
	inline List_1_t8AA9EFA21EF1045CBB5A905EB31B2D66A23339FB * get__postInjectMethods_0() const { return ____postInjectMethods_0; }
	inline List_1_t8AA9EFA21EF1045CBB5A905EB31B2D66A23339FB ** get_address_of__postInjectMethods_0() { return &____postInjectMethods_0; }
	inline void set__postInjectMethods_0(List_1_t8AA9EFA21EF1045CBB5A905EB31B2D66A23339FB * value)
	{
		____postInjectMethods_0 = value;
		Il2CppCodeGenWriteBarrier((&____postInjectMethods_0), value);
	}

	inline static int32_t get_offset_of__constructorInjectables_1() { return static_cast<int32_t>(offsetof(ZenjectTypeInfo_t2866A662C0FDA5B357BCC135021933EE8914535E, ____constructorInjectables_1)); }
	inline List_1_t19FD687049A90924DCEA81F81C85555F15021A68 * get__constructorInjectables_1() const { return ____constructorInjectables_1; }
	inline List_1_t19FD687049A90924DCEA81F81C85555F15021A68 ** get_address_of__constructorInjectables_1() { return &____constructorInjectables_1; }
	inline void set__constructorInjectables_1(List_1_t19FD687049A90924DCEA81F81C85555F15021A68 * value)
	{
		____constructorInjectables_1 = value;
		Il2CppCodeGenWriteBarrier((&____constructorInjectables_1), value);
	}

	inline static int32_t get_offset_of__fieldInjectables_2() { return static_cast<int32_t>(offsetof(ZenjectTypeInfo_t2866A662C0FDA5B357BCC135021933EE8914535E, ____fieldInjectables_2)); }
	inline List_1_t19FD687049A90924DCEA81F81C85555F15021A68 * get__fieldInjectables_2() const { return ____fieldInjectables_2; }
	inline List_1_t19FD687049A90924DCEA81F81C85555F15021A68 ** get_address_of__fieldInjectables_2() { return &____fieldInjectables_2; }
	inline void set__fieldInjectables_2(List_1_t19FD687049A90924DCEA81F81C85555F15021A68 * value)
	{
		____fieldInjectables_2 = value;
		Il2CppCodeGenWriteBarrier((&____fieldInjectables_2), value);
	}

	inline static int32_t get_offset_of__propertyInjectables_3() { return static_cast<int32_t>(offsetof(ZenjectTypeInfo_t2866A662C0FDA5B357BCC135021933EE8914535E, ____propertyInjectables_3)); }
	inline List_1_t19FD687049A90924DCEA81F81C85555F15021A68 * get__propertyInjectables_3() const { return ____propertyInjectables_3; }
	inline List_1_t19FD687049A90924DCEA81F81C85555F15021A68 ** get_address_of__propertyInjectables_3() { return &____propertyInjectables_3; }
	inline void set__propertyInjectables_3(List_1_t19FD687049A90924DCEA81F81C85555F15021A68 * value)
	{
		____propertyInjectables_3 = value;
		Il2CppCodeGenWriteBarrier((&____propertyInjectables_3), value);
	}

	inline static int32_t get_offset_of__injectConstructor_4() { return static_cast<int32_t>(offsetof(ZenjectTypeInfo_t2866A662C0FDA5B357BCC135021933EE8914535E, ____injectConstructor_4)); }
	inline ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF * get__injectConstructor_4() const { return ____injectConstructor_4; }
	inline ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF ** get_address_of__injectConstructor_4() { return &____injectConstructor_4; }
	inline void set__injectConstructor_4(ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF * value)
	{
		____injectConstructor_4 = value;
		Il2CppCodeGenWriteBarrier((&____injectConstructor_4), value);
	}

	inline static int32_t get_offset_of__typeAnalyzed_5() { return static_cast<int32_t>(offsetof(ZenjectTypeInfo_t2866A662C0FDA5B357BCC135021933EE8914535E, ____typeAnalyzed_5)); }
	inline Type_t * get__typeAnalyzed_5() const { return ____typeAnalyzed_5; }
	inline Type_t ** get_address_of__typeAnalyzed_5() { return &____typeAnalyzed_5; }
	inline void set__typeAnalyzed_5(Type_t * value)
	{
		____typeAnalyzed_5 = value;
		Il2CppCodeGenWriteBarrier((&____typeAnalyzed_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZENJECTTYPEINFO_T2866A662C0FDA5B357BCC135021933EE8914535E_H
#ifndef BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#define BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef GUID_T_H
#define GUID_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Guid
struct  Guid_t 
{
public:
	// System.Int32 System.Guid::_a
	int32_t ____a_1;
	// System.Int16 System.Guid::_b
	int16_t ____b_2;
	// System.Int16 System.Guid::_c
	int16_t ____c_3;
	// System.Byte System.Guid::_d
	uint8_t ____d_4;
	// System.Byte System.Guid::_e
	uint8_t ____e_5;
	// System.Byte System.Guid::_f
	uint8_t ____f_6;
	// System.Byte System.Guid::_g
	uint8_t ____g_7;
	// System.Byte System.Guid::_h
	uint8_t ____h_8;
	// System.Byte System.Guid::_i
	uint8_t ____i_9;
	// System.Byte System.Guid::_j
	uint8_t ____j_10;
	// System.Byte System.Guid::_k
	uint8_t ____k_11;

public:
	inline static int32_t get_offset_of__a_1() { return static_cast<int32_t>(offsetof(Guid_t, ____a_1)); }
	inline int32_t get__a_1() const { return ____a_1; }
	inline int32_t* get_address_of__a_1() { return &____a_1; }
	inline void set__a_1(int32_t value)
	{
		____a_1 = value;
	}

	inline static int32_t get_offset_of__b_2() { return static_cast<int32_t>(offsetof(Guid_t, ____b_2)); }
	inline int16_t get__b_2() const { return ____b_2; }
	inline int16_t* get_address_of__b_2() { return &____b_2; }
	inline void set__b_2(int16_t value)
	{
		____b_2 = value;
	}

	inline static int32_t get_offset_of__c_3() { return static_cast<int32_t>(offsetof(Guid_t, ____c_3)); }
	inline int16_t get__c_3() const { return ____c_3; }
	inline int16_t* get_address_of__c_3() { return &____c_3; }
	inline void set__c_3(int16_t value)
	{
		____c_3 = value;
	}

	inline static int32_t get_offset_of__d_4() { return static_cast<int32_t>(offsetof(Guid_t, ____d_4)); }
	inline uint8_t get__d_4() const { return ____d_4; }
	inline uint8_t* get_address_of__d_4() { return &____d_4; }
	inline void set__d_4(uint8_t value)
	{
		____d_4 = value;
	}

	inline static int32_t get_offset_of__e_5() { return static_cast<int32_t>(offsetof(Guid_t, ____e_5)); }
	inline uint8_t get__e_5() const { return ____e_5; }
	inline uint8_t* get_address_of__e_5() { return &____e_5; }
	inline void set__e_5(uint8_t value)
	{
		____e_5 = value;
	}

	inline static int32_t get_offset_of__f_6() { return static_cast<int32_t>(offsetof(Guid_t, ____f_6)); }
	inline uint8_t get__f_6() const { return ____f_6; }
	inline uint8_t* get_address_of__f_6() { return &____f_6; }
	inline void set__f_6(uint8_t value)
	{
		____f_6 = value;
	}

	inline static int32_t get_offset_of__g_7() { return static_cast<int32_t>(offsetof(Guid_t, ____g_7)); }
	inline uint8_t get__g_7() const { return ____g_7; }
	inline uint8_t* get_address_of__g_7() { return &____g_7; }
	inline void set__g_7(uint8_t value)
	{
		____g_7 = value;
	}

	inline static int32_t get_offset_of__h_8() { return static_cast<int32_t>(offsetof(Guid_t, ____h_8)); }
	inline uint8_t get__h_8() const { return ____h_8; }
	inline uint8_t* get_address_of__h_8() { return &____h_8; }
	inline void set__h_8(uint8_t value)
	{
		____h_8 = value;
	}

	inline static int32_t get_offset_of__i_9() { return static_cast<int32_t>(offsetof(Guid_t, ____i_9)); }
	inline uint8_t get__i_9() const { return ____i_9; }
	inline uint8_t* get_address_of__i_9() { return &____i_9; }
	inline void set__i_9(uint8_t value)
	{
		____i_9 = value;
	}

	inline static int32_t get_offset_of__j_10() { return static_cast<int32_t>(offsetof(Guid_t, ____j_10)); }
	inline uint8_t get__j_10() const { return ____j_10; }
	inline uint8_t* get_address_of__j_10() { return &____j_10; }
	inline void set__j_10(uint8_t value)
	{
		____j_10 = value;
	}

	inline static int32_t get_offset_of__k_11() { return static_cast<int32_t>(offsetof(Guid_t, ____k_11)); }
	inline uint8_t get__k_11() const { return ____k_11; }
	inline uint8_t* get_address_of__k_11() { return &____k_11; }
	inline void set__k_11(uint8_t value)
	{
		____k_11 = value;
	}
};

struct Guid_t_StaticFields
{
public:
	// System.Guid System.Guid::Empty
	Guid_t  ___Empty_0;
	// System.Object System.Guid::_rngAccess
	RuntimeObject * ____rngAccess_12;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_rng
	RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * ____rng_13;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ___Empty_0)); }
	inline Guid_t  get_Empty_0() const { return ___Empty_0; }
	inline Guid_t * get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(Guid_t  value)
	{
		___Empty_0 = value;
	}

	inline static int32_t get_offset_of__rngAccess_12() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rngAccess_12)); }
	inline RuntimeObject * get__rngAccess_12() const { return ____rngAccess_12; }
	inline RuntimeObject ** get_address_of__rngAccess_12() { return &____rngAccess_12; }
	inline void set__rngAccess_12(RuntimeObject * value)
	{
		____rngAccess_12 = value;
		Il2CppCodeGenWriteBarrier((&____rngAccess_12), value);
	}

	inline static int32_t get_offset_of__rng_13() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rng_13)); }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * get__rng_13() const { return ____rng_13; }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 ** get_address_of__rng_13() { return &____rng_13; }
	inline void set__rng_13(RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * value)
	{
		____rng_13 = value;
		Il2CppCodeGenWriteBarrier((&____rng_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUID_T_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef ARGNONLAZYBINDER_T56DEDF75B9E224623A2E117702BC79D8C9E84383_H
#define ARGNONLAZYBINDER_T56DEDF75B9E224623A2E117702BC79D8C9E84383_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ArgNonLazyBinder
struct  ArgNonLazyBinder_t56DEDF75B9E224623A2E117702BC79D8C9E84383  : public NonLazyBinder_tA9FB0768A2F39559FBDDCAFBF940AAD240B7C43C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGNONLAZYBINDER_T56DEDF75B9E224623A2E117702BC79D8C9E84383_H
#ifndef COPYNONLAZYBINDER_TC55396C09D2CF16C40164CC4E24D58AC45632D11_H
#define COPYNONLAZYBINDER_TC55396C09D2CF16C40164CC4E24D58AC45632D11_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.CopyNonLazyBinder
struct  CopyNonLazyBinder_tC55396C09D2CF16C40164CC4E24D58AC45632D11  : public NonLazyBinder_tA9FB0768A2F39559FBDDCAFBF940AAD240B7C43C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COPYNONLAZYBINDER_TC55396C09D2CF16C40164CC4E24D58AC45632D11_H
#ifndef FACTORYPROVIDER_2_TA5A2F50F6778F0A3FBF4C73BD0CF6787D9640ACA_H
#define FACTORYPROVIDER_2_TA5A2F50F6778F0A3FBF4C73BD0CF6787D9640ACA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.FactoryProvider`2<System.Object,System.Object>
struct  FactoryProvider_2_tA5A2F50F6778F0A3FBF4C73BD0CF6787D9640ACA  : public FactoryProviderBase_2_t846258810464D2AA8EA9C0F78CBE910F211A7429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACTORYPROVIDER_2_TA5A2F50F6778F0A3FBF4C73BD0CF6787D9640ACA_H
#ifndef FACTORYSUBCONTAINERBINDERWITHPARAMS_1_T45B14E1162744205D8DDBEBD21BD61C7629B12FC_H
#define FACTORYSUBCONTAINERBINDERWITHPARAMS_1_T45B14E1162744205D8DDBEBD21BD61C7629B12FC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.FactorySubContainerBinderWithParams`1<System.Object>
struct  FactorySubContainerBinderWithParams_1_t45B14E1162744205D8DDBEBD21BD61C7629B12FC  : public FactorySubContainerBinderBase_1_t10122DEBCBD743FE20FF2CCC6889131951876194
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACTORYSUBCONTAINERBINDERWITHPARAMS_1_T45B14E1162744205D8DDBEBD21BD61C7629B12FC_H
#ifndef MEMORYPOOLBINDINGFINALIZER_1_TAE0E857B07874662F4D4844CC5ED6495972A128F_H
#define MEMORYPOOLBINDINGFINALIZER_1_TAE0E857B07874662F4D4844CC5ED6495972A128F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.MemoryPoolBindingFinalizer`1<System.Object>
struct  MemoryPoolBindingFinalizer_1_tAE0E857B07874662F4D4844CC5ED6495972A128F  : public ProviderBindingFinalizer_t3C2E6FBD89B98A057674B926F42EF13E4ED1EAAE
{
public:
	// Zenject.MemoryPoolBindInfo Zenject.MemoryPoolBindingFinalizer`1::_poolBindInfo
	MemoryPoolBindInfo_tB7A75979568ABAD9B4C0F599B76DF4BDBD3A5D01 * ____poolBindInfo_1;
	// Zenject.FactoryBindInfo Zenject.MemoryPoolBindingFinalizer`1::_factoryBindInfo
	FactoryBindInfo_t13E5B099259F31F1EF31D3550FE94DF78C9F1C00 * ____factoryBindInfo_2;

public:
	inline static int32_t get_offset_of__poolBindInfo_1() { return static_cast<int32_t>(offsetof(MemoryPoolBindingFinalizer_1_tAE0E857B07874662F4D4844CC5ED6495972A128F, ____poolBindInfo_1)); }
	inline MemoryPoolBindInfo_tB7A75979568ABAD9B4C0F599B76DF4BDBD3A5D01 * get__poolBindInfo_1() const { return ____poolBindInfo_1; }
	inline MemoryPoolBindInfo_tB7A75979568ABAD9B4C0F599B76DF4BDBD3A5D01 ** get_address_of__poolBindInfo_1() { return &____poolBindInfo_1; }
	inline void set__poolBindInfo_1(MemoryPoolBindInfo_tB7A75979568ABAD9B4C0F599B76DF4BDBD3A5D01 * value)
	{
		____poolBindInfo_1 = value;
		Il2CppCodeGenWriteBarrier((&____poolBindInfo_1), value);
	}

	inline static int32_t get_offset_of__factoryBindInfo_2() { return static_cast<int32_t>(offsetof(MemoryPoolBindingFinalizer_1_tAE0E857B07874662F4D4844CC5ED6495972A128F, ____factoryBindInfo_2)); }
	inline FactoryBindInfo_t13E5B099259F31F1EF31D3550FE94DF78C9F1C00 * get__factoryBindInfo_2() const { return ____factoryBindInfo_2; }
	inline FactoryBindInfo_t13E5B099259F31F1EF31D3550FE94DF78C9F1C00 ** get_address_of__factoryBindInfo_2() { return &____factoryBindInfo_2; }
	inline void set__factoryBindInfo_2(FactoryBindInfo_t13E5B099259F31F1EF31D3550FE94DF78C9F1C00 * value)
	{
		____factoryBindInfo_2 = value;
		Il2CppCodeGenWriteBarrier((&____factoryBindInfo_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMORYPOOLBINDINGFINALIZER_1_TAE0E857B07874662F4D4844CC5ED6495972A128F_H
#ifndef SCOPENONLAZYBINDER_T7BC7AB773CA2EAECC76C3B733A1271EE6866646A_H
#define SCOPENONLAZYBINDER_T7BC7AB773CA2EAECC76C3B733A1271EE6866646A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ScopeNonLazyBinder
struct  ScopeNonLazyBinder_t7BC7AB773CA2EAECC76C3B733A1271EE6866646A  : public NonLazyBinder_tA9FB0768A2F39559FBDDCAFBF940AAD240B7C43C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCOPENONLAZYBINDER_T7BC7AB773CA2EAECC76C3B733A1271EE6866646A_H
#ifndef SIGNALHANDLERBINDERWITHID_TF2F486B2BBE4565DB9C112C9D5755BDCFE7BCD27_H
#define SIGNALHANDLERBINDERWITHID_TF2F486B2BBE4565DB9C112C9D5755BDCFE7BCD27_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SignalHandlerBinderWithId
struct  SignalHandlerBinderWithId_tF2F486B2BBE4565DB9C112C9D5755BDCFE7BCD27  : public SignalHandlerBinder_t4AB70F7A7BD1596D46B88F834A400EBEDE185771
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGNALHANDLERBINDERWITHID_TF2F486B2BBE4565DB9C112C9D5755BDCFE7BCD27_H
#ifndef SIGNALHANDLERBINDERWITHID_1_T7C2451301E4286C80D2E3AA5B7D4A77DBC08DBFE_H
#define SIGNALHANDLERBINDERWITHID_1_T7C2451301E4286C80D2E3AA5B7D4A77DBC08DBFE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SignalHandlerBinderWithId`1<System.Object>
struct  SignalHandlerBinderWithId_1_t7C2451301E4286C80D2E3AA5B7D4A77DBC08DBFE  : public SignalHandlerBinder_1_tAC85A1AD98374941739974445C41BF8617CF78FC
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGNALHANDLERBINDERWITHID_1_T7C2451301E4286C80D2E3AA5B7D4A77DBC08DBFE_H
#ifndef SIGNALHANDLERBINDERWITHID_2_T5075E83F01DA88E7F92BC36E706802EC87425C9D_H
#define SIGNALHANDLERBINDERWITHID_2_T5075E83F01DA88E7F92BC36E706802EC87425C9D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SignalHandlerBinderWithId`2<System.Object,System.Object>
struct  SignalHandlerBinderWithId_2_t5075E83F01DA88E7F92BC36E706802EC87425C9D  : public SignalHandlerBinder_2_t6F32E8CD8248414B4F3681F2E4D8BFB658F24D30
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGNALHANDLERBINDERWITHID_2_T5075E83F01DA88E7F92BC36E706802EC87425C9D_H
#ifndef SIGNALHANDLERBINDERWITHID_3_T76D4499350CECBF7B17E476217D26420F3E8D532_H
#define SIGNALHANDLERBINDERWITHID_3_T76D4499350CECBF7B17E476217D26420F3E8D532_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SignalHandlerBinderWithId`3<System.Object,System.Object,System.Object>
struct  SignalHandlerBinderWithId_3_t76D4499350CECBF7B17E476217D26420F3E8D532  : public SignalHandlerBinder_3_t9ADF0354A52CA5EFF23194E48F870113CF337055
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGNALHANDLERBINDERWITHID_3_T76D4499350CECBF7B17E476217D26420F3E8D532_H
#ifndef SIGNALHANDLERBINDERWITHID_4_T36F180155CE8C6D813408F9B2086613D07CF41C9_H
#define SIGNALHANDLERBINDERWITHID_4_T36F180155CE8C6D813408F9B2086613D07CF41C9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SignalHandlerBinderWithId`4<System.Object,System.Object,System.Object,System.Object>
struct  SignalHandlerBinderWithId_4_t36F180155CE8C6D813408F9B2086613D07CF41C9  : public SignalHandlerBinder_4_t6088EB5507EA2460DDEA35BEFBCE49C3F0F851DC
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGNALHANDLERBINDERWITHID_4_T36F180155CE8C6D813408F9B2086613D07CF41C9_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef BINDINGFLAGS_TE35C91D046E63A1B92BB9AB909FCF9DA84379ED0_H
#define BINDINGFLAGS_TE35C91D046E63A1B92BB9AB909FCF9DA84379ED0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_tE35C91D046E63A1B92BB9AB909FCF9DA84379ED0 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tE35C91D046E63A1B92BB9AB909FCF9DA84379ED0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_TE35C91D046E63A1B92BB9AB909FCF9DA84379ED0_H
#ifndef RUNTIMETYPEHANDLE_T7B542280A22F0EC4EAC2061C29178845847A8B2D_H
#define RUNTIMETYPEHANDLE_T7B542280A22F0EC4EAC2061C29178845847A8B2D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMETYPEHANDLE_T7B542280A22F0EC4EAC2061C29178845847A8B2D_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef CONDITIONCOPYNONLAZYBINDER_TF7F37C5F1508AAB283151819E412C853A8D94B9B_H
#define CONDITIONCOPYNONLAZYBINDER_TF7F37C5F1508AAB283151819E412C853A8D94B9B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ConditionCopyNonLazyBinder
struct  ConditionCopyNonLazyBinder_tF7F37C5F1508AAB283151819E412C853A8D94B9B  : public CopyNonLazyBinder_tC55396C09D2CF16C40164CC4E24D58AC45632D11
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONDITIONCOPYNONLAZYBINDER_TF7F37C5F1508AAB283151819E412C853A8D94B9B_H
#ifndef INJECTSOURCES_T643ECF8A27899CEE7D40760762395F6C3E359049_H
#define INJECTSOURCES_T643ECF8A27899CEE7D40760762395F6C3E359049_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.InjectSources
struct  InjectSources_t643ECF8A27899CEE7D40760762395F6C3E359049 
{
public:
	// System.Int32 Zenject.InjectSources::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InjectSources_t643ECF8A27899CEE7D40760762395F6C3E359049, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INJECTSOURCES_T643ECF8A27899CEE7D40760762395F6C3E359049_H
#ifndef INVALIDBINDRESPONSES_TF86F894C536EBB639E3D76953623A03398F924E5_H
#define INVALIDBINDRESPONSES_TF86F894C536EBB639E3D76953623A03398F924E5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.InvalidBindResponses
struct  InvalidBindResponses_tF86F894C536EBB639E3D76953623A03398F924E5 
{
public:
	// System.Int32 Zenject.InvalidBindResponses::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InvalidBindResponses_tF86F894C536EBB639E3D76953623A03398F924E5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVALIDBINDRESPONSES_TF86F894C536EBB639E3D76953623A03398F924E5_H
#ifndef POOLEXPANDMETHODS_T880D0B909A07FAA5A93BBE2918D20E825322B07E_H
#define POOLEXPANDMETHODS_T880D0B909A07FAA5A93BBE2918D20E825322B07E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.PoolExpandMethods
struct  PoolExpandMethods_t880D0B909A07FAA5A93BBE2918D20E825322B07E 
{
public:
	// System.Int32 Zenject.PoolExpandMethods::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PoolExpandMethods_t880D0B909A07FAA5A93BBE2918D20E825322B07E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POOLEXPANDMETHODS_T880D0B909A07FAA5A93BBE2918D20E825322B07E_H
#ifndef SCOPEARGNONLAZYBINDER_T1D85A2EED80F3DAE72346683D48CCCEAB4738BA6_H
#define SCOPEARGNONLAZYBINDER_T1D85A2EED80F3DAE72346683D48CCCEAB4738BA6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ScopeArgNonLazyBinder
struct  ScopeArgNonLazyBinder_t1D85A2EED80F3DAE72346683D48CCCEAB4738BA6  : public ArgNonLazyBinder_t56DEDF75B9E224623A2E117702BC79D8C9E84383
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCOPEARGNONLAZYBINDER_T1D85A2EED80F3DAE72346683D48CCCEAB4738BA6_H
#ifndef SCOPETYPES_T315A9DCBF5346C5A17A873669FFE3A13751201A8_H
#define SCOPETYPES_T315A9DCBF5346C5A17A873669FFE3A13751201A8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ScopeTypes
struct  ScopeTypes_t315A9DCBF5346C5A17A873669FFE3A13751201A8 
{
public:
	// System.Int32 Zenject.ScopeTypes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ScopeTypes_t315A9DCBF5346C5A17A873669FFE3A13751201A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCOPETYPES_T315A9DCBF5346C5A17A873669FFE3A13751201A8_H
#ifndef SINGLETONTYPES_T95744D53A57B5D7318E65315B811CD90544AEF62_H
#define SINGLETONTYPES_T95744D53A57B5D7318E65315B811CD90544AEF62_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SingletonTypes
struct  SingletonTypes_t95744D53A57B5D7318E65315B811CD90544AEF62 
{
public:
	// System.Int32 Zenject.SingletonTypes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SingletonTypes_t95744D53A57B5D7318E65315B811CD90544AEF62, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETONTYPES_T95744D53A57B5D7318E65315B811CD90544AEF62_H
#ifndef TOCHOICES_T6D37BE8E3C1FCAFC151B4DF68384D5D67A526ADF_H
#define TOCHOICES_T6D37BE8E3C1FCAFC151B4DF68384D5D67A526ADF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ToChoices
struct  ToChoices_t6D37BE8E3C1FCAFC151B4DF68384D5D67A526ADF 
{
public:
	// System.Int32 Zenject.ToChoices::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ToChoices_t6D37BE8E3C1FCAFC151B4DF68384D5D67A526ADF, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOCHOICES_T6D37BE8E3C1FCAFC151B4DF68384D5D67A526ADF_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef TYPE_T_H
#define TYPE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((&___FilterAttribute_0), value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((&___FilterName_1), value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((&___FilterNameIgnoreCase_2), value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((&___Missing_3), value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTypes_5), value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((&___defaultBinder_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T_H
#ifndef ARGCONDITIONCOPYNONLAZYBINDER_T5F1B4361E0805E20DDBEDEBC61B90CFFBB68B21B_H
#define ARGCONDITIONCOPYNONLAZYBINDER_T5F1B4361E0805E20DDBEDEBC61B90CFFBB68B21B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ArgConditionCopyNonLazyBinder
struct  ArgConditionCopyNonLazyBinder_t5F1B4361E0805E20DDBEDEBC61B90CFFBB68B21B  : public ConditionCopyNonLazyBinder_tF7F37C5F1508AAB283151819E412C853A8D94B9B
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGCONDITIONCOPYNONLAZYBINDER_T5F1B4361E0805E20DDBEDEBC61B90CFFBB68B21B_H
#ifndef BINDINFO_T56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991_H
#define BINDINFO_T56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.BindInfo
struct  BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991  : public RuntimeObject
{
public:
	// System.String Zenject.BindInfo::<ContextInfo>k__BackingField
	String_t* ___U3CContextInfoU3Ek__BackingField_0;
	// System.Boolean Zenject.BindInfo::<RequireExplicitScope>k__BackingField
	bool ___U3CRequireExplicitScopeU3Ek__BackingField_1;
	// System.Object Zenject.BindInfo::<Identifier>k__BackingField
	RuntimeObject * ___U3CIdentifierU3Ek__BackingField_2;
	// System.Collections.Generic.List`1<System.Type> Zenject.BindInfo::<ContractTypes>k__BackingField
	List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0 * ___U3CContractTypesU3Ek__BackingField_3;
	// System.Boolean Zenject.BindInfo::<CopyIntoAllSubContainers>k__BackingField
	bool ___U3CCopyIntoAllSubContainersU3Ek__BackingField_4;
	// Zenject.InvalidBindResponses Zenject.BindInfo::<InvalidBindResponse>k__BackingField
	int32_t ___U3CInvalidBindResponseU3Ek__BackingField_5;
	// System.Boolean Zenject.BindInfo::<NonLazy>k__BackingField
	bool ___U3CNonLazyU3Ek__BackingField_6;
	// Zenject.BindingCondition Zenject.BindInfo::<Condition>k__BackingField
	BindingCondition_t3BC89ABD74DA160C3396036CE0360DE414B4ADD8 * ___U3CConditionU3Ek__BackingField_7;
	// Zenject.ToChoices Zenject.BindInfo::<ToChoice>k__BackingField
	int32_t ___U3CToChoiceU3Ek__BackingField_8;
	// System.Collections.Generic.List`1<System.Type> Zenject.BindInfo::<ToTypes>k__BackingField
	List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0 * ___U3CToTypesU3Ek__BackingField_9;
	// Zenject.ScopeTypes Zenject.BindInfo::<Scope>k__BackingField
	int32_t ___U3CScopeU3Ek__BackingField_10;
	// System.Object Zenject.BindInfo::<ConcreteIdentifier>k__BackingField
	RuntimeObject * ___U3CConcreteIdentifierU3Ek__BackingField_11;
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.BindInfo::<Arguments>k__BackingField
	List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * ___U3CArgumentsU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_U3CContextInfoU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991, ___U3CContextInfoU3Ek__BackingField_0)); }
	inline String_t* get_U3CContextInfoU3Ek__BackingField_0() const { return ___U3CContextInfoU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CContextInfoU3Ek__BackingField_0() { return &___U3CContextInfoU3Ek__BackingField_0; }
	inline void set_U3CContextInfoU3Ek__BackingField_0(String_t* value)
	{
		___U3CContextInfoU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CContextInfoU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CRequireExplicitScopeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991, ___U3CRequireExplicitScopeU3Ek__BackingField_1)); }
	inline bool get_U3CRequireExplicitScopeU3Ek__BackingField_1() const { return ___U3CRequireExplicitScopeU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CRequireExplicitScopeU3Ek__BackingField_1() { return &___U3CRequireExplicitScopeU3Ek__BackingField_1; }
	inline void set_U3CRequireExplicitScopeU3Ek__BackingField_1(bool value)
	{
		___U3CRequireExplicitScopeU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CIdentifierU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991, ___U3CIdentifierU3Ek__BackingField_2)); }
	inline RuntimeObject * get_U3CIdentifierU3Ek__BackingField_2() const { return ___U3CIdentifierU3Ek__BackingField_2; }
	inline RuntimeObject ** get_address_of_U3CIdentifierU3Ek__BackingField_2() { return &___U3CIdentifierU3Ek__BackingField_2; }
	inline void set_U3CIdentifierU3Ek__BackingField_2(RuntimeObject * value)
	{
		___U3CIdentifierU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CIdentifierU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CContractTypesU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991, ___U3CContractTypesU3Ek__BackingField_3)); }
	inline List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0 * get_U3CContractTypesU3Ek__BackingField_3() const { return ___U3CContractTypesU3Ek__BackingField_3; }
	inline List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0 ** get_address_of_U3CContractTypesU3Ek__BackingField_3() { return &___U3CContractTypesU3Ek__BackingField_3; }
	inline void set_U3CContractTypesU3Ek__BackingField_3(List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0 * value)
	{
		___U3CContractTypesU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CContractTypesU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CCopyIntoAllSubContainersU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991, ___U3CCopyIntoAllSubContainersU3Ek__BackingField_4)); }
	inline bool get_U3CCopyIntoAllSubContainersU3Ek__BackingField_4() const { return ___U3CCopyIntoAllSubContainersU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CCopyIntoAllSubContainersU3Ek__BackingField_4() { return &___U3CCopyIntoAllSubContainersU3Ek__BackingField_4; }
	inline void set_U3CCopyIntoAllSubContainersU3Ek__BackingField_4(bool value)
	{
		___U3CCopyIntoAllSubContainersU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CInvalidBindResponseU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991, ___U3CInvalidBindResponseU3Ek__BackingField_5)); }
	inline int32_t get_U3CInvalidBindResponseU3Ek__BackingField_5() const { return ___U3CInvalidBindResponseU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CInvalidBindResponseU3Ek__BackingField_5() { return &___U3CInvalidBindResponseU3Ek__BackingField_5; }
	inline void set_U3CInvalidBindResponseU3Ek__BackingField_5(int32_t value)
	{
		___U3CInvalidBindResponseU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CNonLazyU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991, ___U3CNonLazyU3Ek__BackingField_6)); }
	inline bool get_U3CNonLazyU3Ek__BackingField_6() const { return ___U3CNonLazyU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CNonLazyU3Ek__BackingField_6() { return &___U3CNonLazyU3Ek__BackingField_6; }
	inline void set_U3CNonLazyU3Ek__BackingField_6(bool value)
	{
		___U3CNonLazyU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CConditionU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991, ___U3CConditionU3Ek__BackingField_7)); }
	inline BindingCondition_t3BC89ABD74DA160C3396036CE0360DE414B4ADD8 * get_U3CConditionU3Ek__BackingField_7() const { return ___U3CConditionU3Ek__BackingField_7; }
	inline BindingCondition_t3BC89ABD74DA160C3396036CE0360DE414B4ADD8 ** get_address_of_U3CConditionU3Ek__BackingField_7() { return &___U3CConditionU3Ek__BackingField_7; }
	inline void set_U3CConditionU3Ek__BackingField_7(BindingCondition_t3BC89ABD74DA160C3396036CE0360DE414B4ADD8 * value)
	{
		___U3CConditionU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CConditionU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CToChoiceU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991, ___U3CToChoiceU3Ek__BackingField_8)); }
	inline int32_t get_U3CToChoiceU3Ek__BackingField_8() const { return ___U3CToChoiceU3Ek__BackingField_8; }
	inline int32_t* get_address_of_U3CToChoiceU3Ek__BackingField_8() { return &___U3CToChoiceU3Ek__BackingField_8; }
	inline void set_U3CToChoiceU3Ek__BackingField_8(int32_t value)
	{
		___U3CToChoiceU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CToTypesU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991, ___U3CToTypesU3Ek__BackingField_9)); }
	inline List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0 * get_U3CToTypesU3Ek__BackingField_9() const { return ___U3CToTypesU3Ek__BackingField_9; }
	inline List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0 ** get_address_of_U3CToTypesU3Ek__BackingField_9() { return &___U3CToTypesU3Ek__BackingField_9; }
	inline void set_U3CToTypesU3Ek__BackingField_9(List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0 * value)
	{
		___U3CToTypesU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CToTypesU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3CScopeU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991, ___U3CScopeU3Ek__BackingField_10)); }
	inline int32_t get_U3CScopeU3Ek__BackingField_10() const { return ___U3CScopeU3Ek__BackingField_10; }
	inline int32_t* get_address_of_U3CScopeU3Ek__BackingField_10() { return &___U3CScopeU3Ek__BackingField_10; }
	inline void set_U3CScopeU3Ek__BackingField_10(int32_t value)
	{
		___U3CScopeU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CConcreteIdentifierU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991, ___U3CConcreteIdentifierU3Ek__BackingField_11)); }
	inline RuntimeObject * get_U3CConcreteIdentifierU3Ek__BackingField_11() const { return ___U3CConcreteIdentifierU3Ek__BackingField_11; }
	inline RuntimeObject ** get_address_of_U3CConcreteIdentifierU3Ek__BackingField_11() { return &___U3CConcreteIdentifierU3Ek__BackingField_11; }
	inline void set_U3CConcreteIdentifierU3Ek__BackingField_11(RuntimeObject * value)
	{
		___U3CConcreteIdentifierU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CConcreteIdentifierU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CArgumentsU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991, ___U3CArgumentsU3Ek__BackingField_12)); }
	inline List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * get_U3CArgumentsU3Ek__BackingField_12() const { return ___U3CArgumentsU3Ek__BackingField_12; }
	inline List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 ** get_address_of_U3CArgumentsU3Ek__BackingField_12() { return &___U3CArgumentsU3Ek__BackingField_12; }
	inline void set_U3CArgumentsU3Ek__BackingField_12(List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * value)
	{
		___U3CArgumentsU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CArgumentsU3Ek__BackingField_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINFO_T56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991_H
#ifndef INJECTCONTEXT_TD1BDE94C49785956950BCFD5E1693537A7008FFF_H
#define INJECTCONTEXT_TD1BDE94C49785956950BCFD5E1693537A7008FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.InjectContext
struct  InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF  : public RuntimeObject
{
public:
	// System.Type Zenject.InjectContext::<ObjectType>k__BackingField
	Type_t * ___U3CObjectTypeU3Ek__BackingField_0;
	// Zenject.InjectContext Zenject.InjectContext::<ParentContext>k__BackingField
	InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * ___U3CParentContextU3Ek__BackingField_1;
	// System.Object Zenject.InjectContext::<ObjectInstance>k__BackingField
	RuntimeObject * ___U3CObjectInstanceU3Ek__BackingField_2;
	// System.Object Zenject.InjectContext::<Identifier>k__BackingField
	RuntimeObject * ___U3CIdentifierU3Ek__BackingField_3;
	// System.Object Zenject.InjectContext::<ConcreteIdentifier>k__BackingField
	RuntimeObject * ___U3CConcreteIdentifierU3Ek__BackingField_4;
	// System.String Zenject.InjectContext::<MemberName>k__BackingField
	String_t* ___U3CMemberNameU3Ek__BackingField_5;
	// System.Type Zenject.InjectContext::<MemberType>k__BackingField
	Type_t * ___U3CMemberTypeU3Ek__BackingField_6;
	// System.Boolean Zenject.InjectContext::<Optional>k__BackingField
	bool ___U3COptionalU3Ek__BackingField_7;
	// Zenject.InjectSources Zenject.InjectContext::<SourceType>k__BackingField
	int32_t ___U3CSourceTypeU3Ek__BackingField_8;
	// System.Object Zenject.InjectContext::<FallBackValue>k__BackingField
	RuntimeObject * ___U3CFallBackValueU3Ek__BackingField_9;
	// Zenject.DiContainer Zenject.InjectContext::<Container>k__BackingField
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ___U3CContainerU3Ek__BackingField_10;

public:
	inline static int32_t get_offset_of_U3CObjectTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF, ___U3CObjectTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CObjectTypeU3Ek__BackingField_0() const { return ___U3CObjectTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CObjectTypeU3Ek__BackingField_0() { return &___U3CObjectTypeU3Ek__BackingField_0; }
	inline void set_U3CObjectTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CObjectTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CObjectTypeU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CParentContextU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF, ___U3CParentContextU3Ek__BackingField_1)); }
	inline InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * get_U3CParentContextU3Ek__BackingField_1() const { return ___U3CParentContextU3Ek__BackingField_1; }
	inline InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF ** get_address_of_U3CParentContextU3Ek__BackingField_1() { return &___U3CParentContextU3Ek__BackingField_1; }
	inline void set_U3CParentContextU3Ek__BackingField_1(InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * value)
	{
		___U3CParentContextU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CParentContextU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CObjectInstanceU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF, ___U3CObjectInstanceU3Ek__BackingField_2)); }
	inline RuntimeObject * get_U3CObjectInstanceU3Ek__BackingField_2() const { return ___U3CObjectInstanceU3Ek__BackingField_2; }
	inline RuntimeObject ** get_address_of_U3CObjectInstanceU3Ek__BackingField_2() { return &___U3CObjectInstanceU3Ek__BackingField_2; }
	inline void set_U3CObjectInstanceU3Ek__BackingField_2(RuntimeObject * value)
	{
		___U3CObjectInstanceU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CObjectInstanceU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CIdentifierU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF, ___U3CIdentifierU3Ek__BackingField_3)); }
	inline RuntimeObject * get_U3CIdentifierU3Ek__BackingField_3() const { return ___U3CIdentifierU3Ek__BackingField_3; }
	inline RuntimeObject ** get_address_of_U3CIdentifierU3Ek__BackingField_3() { return &___U3CIdentifierU3Ek__BackingField_3; }
	inline void set_U3CIdentifierU3Ek__BackingField_3(RuntimeObject * value)
	{
		___U3CIdentifierU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CIdentifierU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CConcreteIdentifierU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF, ___U3CConcreteIdentifierU3Ek__BackingField_4)); }
	inline RuntimeObject * get_U3CConcreteIdentifierU3Ek__BackingField_4() const { return ___U3CConcreteIdentifierU3Ek__BackingField_4; }
	inline RuntimeObject ** get_address_of_U3CConcreteIdentifierU3Ek__BackingField_4() { return &___U3CConcreteIdentifierU3Ek__BackingField_4; }
	inline void set_U3CConcreteIdentifierU3Ek__BackingField_4(RuntimeObject * value)
	{
		___U3CConcreteIdentifierU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CConcreteIdentifierU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CMemberNameU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF, ___U3CMemberNameU3Ek__BackingField_5)); }
	inline String_t* get_U3CMemberNameU3Ek__BackingField_5() const { return ___U3CMemberNameU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CMemberNameU3Ek__BackingField_5() { return &___U3CMemberNameU3Ek__BackingField_5; }
	inline void set_U3CMemberNameU3Ek__BackingField_5(String_t* value)
	{
		___U3CMemberNameU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMemberNameU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CMemberTypeU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF, ___U3CMemberTypeU3Ek__BackingField_6)); }
	inline Type_t * get_U3CMemberTypeU3Ek__BackingField_6() const { return ___U3CMemberTypeU3Ek__BackingField_6; }
	inline Type_t ** get_address_of_U3CMemberTypeU3Ek__BackingField_6() { return &___U3CMemberTypeU3Ek__BackingField_6; }
	inline void set_U3CMemberTypeU3Ek__BackingField_6(Type_t * value)
	{
		___U3CMemberTypeU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMemberTypeU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3COptionalU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF, ___U3COptionalU3Ek__BackingField_7)); }
	inline bool get_U3COptionalU3Ek__BackingField_7() const { return ___U3COptionalU3Ek__BackingField_7; }
	inline bool* get_address_of_U3COptionalU3Ek__BackingField_7() { return &___U3COptionalU3Ek__BackingField_7; }
	inline void set_U3COptionalU3Ek__BackingField_7(bool value)
	{
		___U3COptionalU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CSourceTypeU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF, ___U3CSourceTypeU3Ek__BackingField_8)); }
	inline int32_t get_U3CSourceTypeU3Ek__BackingField_8() const { return ___U3CSourceTypeU3Ek__BackingField_8; }
	inline int32_t* get_address_of_U3CSourceTypeU3Ek__BackingField_8() { return &___U3CSourceTypeU3Ek__BackingField_8; }
	inline void set_U3CSourceTypeU3Ek__BackingField_8(int32_t value)
	{
		___U3CSourceTypeU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CFallBackValueU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF, ___U3CFallBackValueU3Ek__BackingField_9)); }
	inline RuntimeObject * get_U3CFallBackValueU3Ek__BackingField_9() const { return ___U3CFallBackValueU3Ek__BackingField_9; }
	inline RuntimeObject ** get_address_of_U3CFallBackValueU3Ek__BackingField_9() { return &___U3CFallBackValueU3Ek__BackingField_9; }
	inline void set_U3CFallBackValueU3Ek__BackingField_9(RuntimeObject * value)
	{
		___U3CFallBackValueU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFallBackValueU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3CContainerU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF, ___U3CContainerU3Ek__BackingField_10)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get_U3CContainerU3Ek__BackingField_10() const { return ___U3CContainerU3Ek__BackingField_10; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of_U3CContainerU3Ek__BackingField_10() { return &___U3CContainerU3Ek__BackingField_10; }
	inline void set_U3CContainerU3Ek__BackingField_10(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		___U3CContainerU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CContainerU3Ek__BackingField_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INJECTCONTEXT_TD1BDE94C49785956950BCFD5E1693537A7008FFF_H
#ifndef MEMORYPOOLBINDINFO_TB7A75979568ABAD9B4C0F599B76DF4BDBD3A5D01_H
#define MEMORYPOOLBINDINFO_TB7A75979568ABAD9B4C0F599B76DF4BDBD3A5D01_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.MemoryPoolBindInfo
struct  MemoryPoolBindInfo_tB7A75979568ABAD9B4C0F599B76DF4BDBD3A5D01  : public RuntimeObject
{
public:
	// Zenject.PoolExpandMethods Zenject.MemoryPoolBindInfo::<ExpandMethod>k__BackingField
	int32_t ___U3CExpandMethodU3Ek__BackingField_0;
	// System.Int32 Zenject.MemoryPoolBindInfo::<InitialSize>k__BackingField
	int32_t ___U3CInitialSizeU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CExpandMethodU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MemoryPoolBindInfo_tB7A75979568ABAD9B4C0F599B76DF4BDBD3A5D01, ___U3CExpandMethodU3Ek__BackingField_0)); }
	inline int32_t get_U3CExpandMethodU3Ek__BackingField_0() const { return ___U3CExpandMethodU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CExpandMethodU3Ek__BackingField_0() { return &___U3CExpandMethodU3Ek__BackingField_0; }
	inline void set_U3CExpandMethodU3Ek__BackingField_0(int32_t value)
	{
		___U3CExpandMethodU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CInitialSizeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(MemoryPoolBindInfo_tB7A75979568ABAD9B4C0F599B76DF4BDBD3A5D01, ___U3CInitialSizeU3Ek__BackingField_1)); }
	inline int32_t get_U3CInitialSizeU3Ek__BackingField_1() const { return ___U3CInitialSizeU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CInitialSizeU3Ek__BackingField_1() { return &___U3CInitialSizeU3Ek__BackingField_1; }
	inline void set_U3CInitialSizeU3Ek__BackingField_1(int32_t value)
	{
		___U3CInitialSizeU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMORYPOOLBINDINFO_TB7A75979568ABAD9B4C0F599B76DF4BDBD3A5D01_H
#ifndef SCOPABLEBINDINGFINALIZER_T1278F9BE4EE396C7D8B5CA5625C540EC3582632C_H
#define SCOPABLEBINDINGFINALIZER_T1278F9BE4EE396C7D8B5CA5625C540EC3582632C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ScopableBindingFinalizer
struct  ScopableBindingFinalizer_t1278F9BE4EE396C7D8B5CA5625C540EC3582632C  : public ProviderBindingFinalizer_t3C2E6FBD89B98A057674B926F42EF13E4ED1EAAE
{
public:
	// Zenject.SingletonTypes Zenject.ScopableBindingFinalizer::_singletonType
	int32_t ____singletonType_1;
	// System.Func`3<Zenject.DiContainer,System.Type,Zenject.IProvider> Zenject.ScopableBindingFinalizer::_providerFactory
	Func_3_tF2BBB0DC92895D84974BAECD6D1B2AE7FFC65AAA * ____providerFactory_2;
	// System.Object Zenject.ScopableBindingFinalizer::_singletonSpecificId
	RuntimeObject * ____singletonSpecificId_3;

public:
	inline static int32_t get_offset_of__singletonType_1() { return static_cast<int32_t>(offsetof(ScopableBindingFinalizer_t1278F9BE4EE396C7D8B5CA5625C540EC3582632C, ____singletonType_1)); }
	inline int32_t get__singletonType_1() const { return ____singletonType_1; }
	inline int32_t* get_address_of__singletonType_1() { return &____singletonType_1; }
	inline void set__singletonType_1(int32_t value)
	{
		____singletonType_1 = value;
	}

	inline static int32_t get_offset_of__providerFactory_2() { return static_cast<int32_t>(offsetof(ScopableBindingFinalizer_t1278F9BE4EE396C7D8B5CA5625C540EC3582632C, ____providerFactory_2)); }
	inline Func_3_tF2BBB0DC92895D84974BAECD6D1B2AE7FFC65AAA * get__providerFactory_2() const { return ____providerFactory_2; }
	inline Func_3_tF2BBB0DC92895D84974BAECD6D1B2AE7FFC65AAA ** get_address_of__providerFactory_2() { return &____providerFactory_2; }
	inline void set__providerFactory_2(Func_3_tF2BBB0DC92895D84974BAECD6D1B2AE7FFC65AAA * value)
	{
		____providerFactory_2 = value;
		Il2CppCodeGenWriteBarrier((&____providerFactory_2), value);
	}

	inline static int32_t get_offset_of__singletonSpecificId_3() { return static_cast<int32_t>(offsetof(ScopableBindingFinalizer_t1278F9BE4EE396C7D8B5CA5625C540EC3582632C, ____singletonSpecificId_3)); }
	inline RuntimeObject * get__singletonSpecificId_3() const { return ____singletonSpecificId_3; }
	inline RuntimeObject ** get_address_of__singletonSpecificId_3() { return &____singletonSpecificId_3; }
	inline void set__singletonSpecificId_3(RuntimeObject * value)
	{
		____singletonSpecificId_3 = value;
		Il2CppCodeGenWriteBarrier((&____singletonSpecificId_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCOPABLEBINDINGFINALIZER_T1278F9BE4EE396C7D8B5CA5625C540EC3582632C_H
#ifndef SCOPECONDITIONCOPYNONLAZYBINDER_T6221939C8173223799835D9DF0944D6D777C3EC6_H
#define SCOPECONDITIONCOPYNONLAZYBINDER_T6221939C8173223799835D9DF0944D6D777C3EC6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ScopeConditionCopyNonLazyBinder
struct  ScopeConditionCopyNonLazyBinder_t6221939C8173223799835D9DF0944D6D777C3EC6  : public ConditionCopyNonLazyBinder_tF7F37C5F1508AAB283151819E412C853A8D94B9B
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCOPECONDITIONCOPYNONLAZYBINDER_T6221939C8173223799835D9DF0944D6D777C3EC6_H
#ifndef SIGNALBINDER_TAF04C55124D4BDEFC6038E643AD06EFA454E3584_H
#define SIGNALBINDER_TAF04C55124D4BDEFC6038E643AD06EFA454E3584_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SignalBinder
struct  SignalBinder_tAF04C55124D4BDEFC6038E643AD06EFA454E3584  : public ConditionCopyNonLazyBinder_tF7F37C5F1508AAB283151819E412C853A8D94B9B
{
public:
	// Zenject.SignalSettings Zenject.SignalBinder::_signalSettings
	SignalSettings_tD753AA3D16CEA36F97CDAEC5428C5217F1EB6540 * ____signalSettings_1;

public:
	inline static int32_t get_offset_of__signalSettings_1() { return static_cast<int32_t>(offsetof(SignalBinder_tAF04C55124D4BDEFC6038E643AD06EFA454E3584, ____signalSettings_1)); }
	inline SignalSettings_tD753AA3D16CEA36F97CDAEC5428C5217F1EB6540 * get__signalSettings_1() const { return ____signalSettings_1; }
	inline SignalSettings_tD753AA3D16CEA36F97CDAEC5428C5217F1EB6540 ** get_address_of__signalSettings_1() { return &____signalSettings_1; }
	inline void set__signalSettings_1(SignalSettings_tD753AA3D16CEA36F97CDAEC5428C5217F1EB6540 * value)
	{
		____signalSettings_1 = value;
		Il2CppCodeGenWriteBarrier((&____signalSettings_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGNALBINDER_TAF04C55124D4BDEFC6038E643AD06EFA454E3584_H
#ifndef SIGNALFROMBINDER_1_T862D9073BD2CB93CFFF7F75A6F7262CDF34AA1D7_H
#define SIGNALFROMBINDER_1_T862D9073BD2CB93CFFF7F75A6F7262CDF34AA1D7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SignalFromBinder`1<System.Object>
struct  SignalFromBinder_1_t862D9073BD2CB93CFFF7F75A6F7262CDF34AA1D7  : public ScopeArgNonLazyBinder_t1D85A2EED80F3DAE72346683D48CCCEAB4738BA6
{
public:
	// Zenject.BindInfo Zenject.SignalFromBinder`1::_info
	BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * ____info_1;
	// Zenject.FromBinderGeneric`1<TContract> Zenject.SignalFromBinder`1::_subBinder
	FromBinderGeneric_1_tB3D3D4B24691B0AFF684D39C08F63B2193327187 * ____subBinder_2;

public:
	inline static int32_t get_offset_of__info_1() { return static_cast<int32_t>(offsetof(SignalFromBinder_1_t862D9073BD2CB93CFFF7F75A6F7262CDF34AA1D7, ____info_1)); }
	inline BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * get__info_1() const { return ____info_1; }
	inline BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 ** get_address_of__info_1() { return &____info_1; }
	inline void set__info_1(BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * value)
	{
		____info_1 = value;
		Il2CppCodeGenWriteBarrier((&____info_1), value);
	}

	inline static int32_t get_offset_of__subBinder_2() { return static_cast<int32_t>(offsetof(SignalFromBinder_1_t862D9073BD2CB93CFFF7F75A6F7262CDF34AA1D7, ____subBinder_2)); }
	inline FromBinderGeneric_1_tB3D3D4B24691B0AFF684D39C08F63B2193327187 * get__subBinder_2() const { return ____subBinder_2; }
	inline FromBinderGeneric_1_tB3D3D4B24691B0AFF684D39C08F63B2193327187 ** get_address_of__subBinder_2() { return &____subBinder_2; }
	inline void set__subBinder_2(FromBinderGeneric_1_tB3D3D4B24691B0AFF684D39C08F63B2193327187 * value)
	{
		____subBinder_2 = value;
		Il2CppCodeGenWriteBarrier((&____subBinder_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGNALFROMBINDER_1_T862D9073BD2CB93CFFF7F75A6F7262CDF34AA1D7_H
#ifndef TRANSFORMCONDITIONCOPYNONLAZYBINDER_T1D4F796012991D42811487B4BF4D01311600463F_H
#define TRANSFORMCONDITIONCOPYNONLAZYBINDER_T1D4F796012991D42811487B4BF4D01311600463F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.TransformConditionCopyNonLazyBinder
struct  TransformConditionCopyNonLazyBinder_t1D4F796012991D42811487B4BF4D01311600463F  : public ConditionCopyNonLazyBinder_tF7F37C5F1508AAB283151819E412C853A8D94B9B
{
public:
	// Zenject.GameObjectCreationParameters Zenject.TransformConditionCopyNonLazyBinder::<GameObjectInfo>k__BackingField
	GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A * ___U3CGameObjectInfoU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CGameObjectInfoU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(TransformConditionCopyNonLazyBinder_t1D4F796012991D42811487B4BF4D01311600463F, ___U3CGameObjectInfoU3Ek__BackingField_1)); }
	inline GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A * get_U3CGameObjectInfoU3Ek__BackingField_1() const { return ___U3CGameObjectInfoU3Ek__BackingField_1; }
	inline GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A ** get_address_of_U3CGameObjectInfoU3Ek__BackingField_1() { return &___U3CGameObjectInfoU3Ek__BackingField_1; }
	inline void set_U3CGameObjectInfoU3Ek__BackingField_1(GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A * value)
	{
		___U3CGameObjectInfoU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGameObjectInfoU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMCONDITIONCOPYNONLAZYBINDER_T1D4F796012991D42811487B4BF4D01311600463F_H
#ifndef ACTION_5_TA3F3461C4A9081A2000D185A35A46E23F4E4338F_H
#define ACTION_5_TA3F3461C4A9081A2000D185A35A46E23F4E4338F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModestTree.Util.Action`5<System.Object,System.Object,System.Object,System.Object,System.Object>
struct  Action_5_tA3F3461C4A9081A2000D185A35A46E23F4E4338F  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_5_TA3F3461C4A9081A2000D185A35A46E23F4E4338F_H
#ifndef ACTION_1_T551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0_H
#define ACTION_1_T551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`1<System.Object>
struct  Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_1_T551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0_H
#ifndef ACTION_2_T0DB6FD6F515527EAB552B690A291778C6F00D48C_H
#define ACTION_2_T0DB6FD6F515527EAB552B690A291778C6F00D48C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`2<System.Object,System.Object>
struct  Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_2_T0DB6FD6F515527EAB552B690A291778C6F00D48C_H
#ifndef ACTION_3_TCC14115B7178951118504E7198B7C872630643C5_H
#define ACTION_3_TCC14115B7178951118504E7198B7C872630643C5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`3<System.Object,System.Object,System.Object>
struct  Action_3_tCC14115B7178951118504E7198B7C872630643C5  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_3_TCC14115B7178951118504E7198B7C872630643C5_H
#ifndef ACTION_4_T46ACC504A4217F2E92EF2A827836993B819AF923_H
#define ACTION_4_T46ACC504A4217F2E92EF2A827836993B819AF923_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`4<System.Object,System.Object,System.Object,System.Object>
struct  Action_4_t46ACC504A4217F2E92EF2A827836993B819AF923  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_4_T46ACC504A4217F2E92EF2A827836993B819AF923_H
#ifndef FUNC_2_T058479A951AB44C28D842472579EAA2FDD8DFC0D_H
#define FUNC_2_T058479A951AB44C28D842472579EAA2FDD8DFC0D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Func`2<System.Object,System.Action>
struct  Func_2_t058479A951AB44C28D842472579EAA2FDD8DFC0D  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUNC_2_T058479A951AB44C28D842472579EAA2FDD8DFC0D_H
#ifndef FUNC_2_T38649B64802A8DCC2476C1CBE0CF179DBBFA5113_H
#define FUNC_2_T38649B64802A8DCC2476C1CBE0CF179DBBFA5113_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Func`2<System.Object,System.Action`1<System.Object>>
struct  Func_2_t38649B64802A8DCC2476C1CBE0CF179DBBFA5113  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUNC_2_T38649B64802A8DCC2476C1CBE0CF179DBBFA5113_H
#ifndef FUNC_2_T88CE136AD37DD8F7713742358AC56B012C611D88_H
#define FUNC_2_T88CE136AD37DD8F7713742358AC56B012C611D88_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Func`2<System.Object,System.Action`2<System.Object,System.Object>>
struct  Func_2_t88CE136AD37DD8F7713742358AC56B012C611D88  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUNC_2_T88CE136AD37DD8F7713742358AC56B012C611D88_H
#ifndef FUNC_2_T6D7E96A36A34348A9F5DA29E4FFE3BCEA4558CBC_H
#define FUNC_2_T6D7E96A36A34348A9F5DA29E4FFE3BCEA4558CBC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Func`2<System.Object,System.Action`3<System.Object,System.Object,System.Object>>
struct  Func_2_t6D7E96A36A34348A9F5DA29E4FFE3BCEA4558CBC  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUNC_2_T6D7E96A36A34348A9F5DA29E4FFE3BCEA4558CBC_H
#ifndef FUNC_2_T501D8CF44938B0EB347A4669809ED06E577C2D3C_H
#define FUNC_2_T501D8CF44938B0EB347A4669809ED06E577C2D3C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Func`2<System.Object,System.Action`4<System.Object,System.Object,System.Object,System.Object>>
struct  Func_2_t501D8CF44938B0EB347A4669809ED06E577C2D3C  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUNC_2_T501D8CF44938B0EB347A4669809ED06E577C2D3C_H
#ifndef FUNC_2_TE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4_H
#define FUNC_2_TE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Func`2<System.Object,System.Object>
struct  Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUNC_2_TE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4_H
#ifndef FUNC_2_TEB18CC35489403B1DD34E84D07D6EBE38C06F980_H
#define FUNC_2_TEB18CC35489403B1DD34E84D07D6EBE38C06F980_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Func`2<Zenject.InjectContext,System.Collections.Generic.IEnumerable`1<System.Object>>
struct  Func_2_tEB18CC35489403B1DD34E84D07D6EBE38C06F980  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUNC_2_TEB18CC35489403B1DD34E84D07D6EBE38C06F980_H
#ifndef FUNC_2_TE24EB23C7D8F4AD0A3A34C5DB2C9F329886F48F2_H
#define FUNC_2_TE24EB23C7D8F4AD0A3A34C5DB2C9F329886F48F2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Func`2<Zenject.InjectContext,System.Object>
struct  Func_2_tE24EB23C7D8F4AD0A3A34C5DB2C9F329886F48F2  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUNC_2_TE24EB23C7D8F4AD0A3A34C5DB2C9F329886F48F2_H
#ifndef FUNC_3_TF2BBB0DC92895D84974BAECD6D1B2AE7FFC65AAA_H
#define FUNC_3_TF2BBB0DC92895D84974BAECD6D1B2AE7FFC65AAA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Func`3<Zenject.DiContainer,System.Type,Zenject.IProvider>
struct  Func_3_tF2BBB0DC92895D84974BAECD6D1B2AE7FFC65AAA  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUNC_3_TF2BBB0DC92895D84974BAECD6D1B2AE7FFC65AAA_H
#ifndef FACTORYFROMBINDERBASE_1_T6E4C619ED3747053EEBAD0353A44672C64C18298_H
#define FACTORYFROMBINDERBASE_1_T6E4C619ED3747053EEBAD0353A44672C64C18298_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.FactoryFromBinderBase`1<System.Object>
struct  FactoryFromBinderBase_1_t6E4C619ED3747053EEBAD0353A44672C64C18298  : public ArgConditionCopyNonLazyBinder_t5F1B4361E0805E20DDBEDEBC61B90CFFBB68B21B
{
public:
	// Zenject.FactoryBindInfo Zenject.FactoryFromBinderBase`1::<FactoryBindInfo>k__BackingField
	FactoryBindInfo_t13E5B099259F31F1EF31D3550FE94DF78C9F1C00 * ___U3CFactoryBindInfoU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CFactoryBindInfoU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(FactoryFromBinderBase_1_t6E4C619ED3747053EEBAD0353A44672C64C18298, ___U3CFactoryBindInfoU3Ek__BackingField_1)); }
	inline FactoryBindInfo_t13E5B099259F31F1EF31D3550FE94DF78C9F1C00 * get_U3CFactoryBindInfoU3Ek__BackingField_1() const { return ___U3CFactoryBindInfoU3Ek__BackingField_1; }
	inline FactoryBindInfo_t13E5B099259F31F1EF31D3550FE94DF78C9F1C00 ** get_address_of_U3CFactoryBindInfoU3Ek__BackingField_1() { return &___U3CFactoryBindInfoU3Ek__BackingField_1; }
	inline void set_U3CFactoryBindInfoU3Ek__BackingField_1(FactoryBindInfo_t13E5B099259F31F1EF31D3550FE94DF78C9F1C00 * value)
	{
		___U3CFactoryBindInfoU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFactoryBindInfoU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACTORYFROMBINDERBASE_1_T6E4C619ED3747053EEBAD0353A44672C64C18298_H
#ifndef IDSCOPECONDITIONCOPYNONLAZYBINDER_TF4C46379BF6B4595053DBCAB75767E759A18ECEE_H
#define IDSCOPECONDITIONCOPYNONLAZYBINDER_TF4C46379BF6B4595053DBCAB75767E759A18ECEE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.IdScopeConditionCopyNonLazyBinder
struct  IdScopeConditionCopyNonLazyBinder_tF4C46379BF6B4595053DBCAB75767E759A18ECEE  : public ScopeConditionCopyNonLazyBinder_t6221939C8173223799835D9DF0944D6D777C3EC6
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IDSCOPECONDITIONCOPYNONLAZYBINDER_TF4C46379BF6B4595053DBCAB75767E759A18ECEE_H
#ifndef NAMETRANSFORMCONDITIONCOPYNONLAZYBINDER_T2E9664EE069D9C6CA15EFEDD64FAE2B4125AC36D_H
#define NAMETRANSFORMCONDITIONCOPYNONLAZYBINDER_T2E9664EE069D9C6CA15EFEDD64FAE2B4125AC36D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.NameTransformConditionCopyNonLazyBinder
struct  NameTransformConditionCopyNonLazyBinder_t2E9664EE069D9C6CA15EFEDD64FAE2B4125AC36D  : public TransformConditionCopyNonLazyBinder_t1D4F796012991D42811487B4BF4D01311600463F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMETRANSFORMCONDITIONCOPYNONLAZYBINDER_T2E9664EE069D9C6CA15EFEDD64FAE2B4125AC36D_H
#ifndef SCOPEARGCONDITIONCOPYNONLAZYBINDER_T218A003451367EEC6A439D63A0F38B964EF07B33_H
#define SCOPEARGCONDITIONCOPYNONLAZYBINDER_T218A003451367EEC6A439D63A0F38B964EF07B33_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ScopeArgConditionCopyNonLazyBinder
struct  ScopeArgConditionCopyNonLazyBinder_t218A003451367EEC6A439D63A0F38B964EF07B33  : public ArgConditionCopyNonLazyBinder_t5F1B4361E0805E20DDBEDEBC61B90CFFBB68B21B
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCOPEARGCONDITIONCOPYNONLAZYBINDER_T218A003451367EEC6A439D63A0F38B964EF07B33_H
#ifndef SIGNALBINDERWITHID_TF62257893A9AA47A533D47DC5DB0C0C046F67B81_H
#define SIGNALBINDERWITHID_TF62257893A9AA47A533D47DC5DB0C0C046F67B81_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SignalBinderWithId
struct  SignalBinderWithId_tF62257893A9AA47A533D47DC5DB0C0C046F67B81  : public SignalBinder_tAF04C55124D4BDEFC6038E643AD06EFA454E3584
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGNALBINDERWITHID_TF62257893A9AA47A533D47DC5DB0C0C046F67B81_H
#ifndef FACTORYFROMBINDER_1_T8A38890E41D19C23AC52C50A9AEF8B9DA43E17D4_H
#define FACTORYFROMBINDER_1_T8A38890E41D19C23AC52C50A9AEF8B9DA43E17D4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.FactoryFromBinder`1<System.Object>
struct  FactoryFromBinder_1_t8A38890E41D19C23AC52C50A9AEF8B9DA43E17D4  : public FactoryFromBinderBase_1_t6E4C619ED3747053EEBAD0353A44672C64C18298
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACTORYFROMBINDER_1_T8A38890E41D19C23AC52C50A9AEF8B9DA43E17D4_H
#ifndef FROMBINDER_T69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75_H
#define FROMBINDER_T69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.FromBinder
struct  FromBinder_t69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75  : public ScopeArgConditionCopyNonLazyBinder_t218A003451367EEC6A439D63A0F38B964EF07B33
{
public:
	// Zenject.BindFinalizerWrapper Zenject.FromBinder::<FinalizerWrapper>k__BackingField
	BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 * ___U3CFinalizerWrapperU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CFinalizerWrapperU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(FromBinder_t69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75, ___U3CFinalizerWrapperU3Ek__BackingField_1)); }
	inline BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 * get_U3CFinalizerWrapperU3Ek__BackingField_1() const { return ___U3CFinalizerWrapperU3Ek__BackingField_1; }
	inline BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 ** get_address_of_U3CFinalizerWrapperU3Ek__BackingField_1() { return &___U3CFinalizerWrapperU3Ek__BackingField_1; }
	inline void set_U3CFinalizerWrapperU3Ek__BackingField_1(BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 * value)
	{
		___U3CFinalizerWrapperU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFinalizerWrapperU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FROMBINDER_T69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75_H
#ifndef FACTORYTOCHOICEBINDER_1_T1B20469F7095193A94918647E424C8F3DB8EEA7E_H
#define FACTORYTOCHOICEBINDER_1_T1B20469F7095193A94918647E424C8F3DB8EEA7E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.FactoryToChoiceBinder`1<System.Object>
struct  FactoryToChoiceBinder_1_t1B20469F7095193A94918647E424C8F3DB8EEA7E  : public FactoryFromBinder_1_t8A38890E41D19C23AC52C50A9AEF8B9DA43E17D4
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACTORYTOCHOICEBINDER_1_T1B20469F7095193A94918647E424C8F3DB8EEA7E_H
#ifndef FROMBINDERGENERIC_1_TB3D3D4B24691B0AFF684D39C08F63B2193327187_H
#define FROMBINDERGENERIC_1_TB3D3D4B24691B0AFF684D39C08F63B2193327187_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.FromBinderGeneric`1<System.Object>
struct  FromBinderGeneric_1_tB3D3D4B24691B0AFF684D39C08F63B2193327187  : public FromBinder_t69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FROMBINDERGENERIC_1_TB3D3D4B24691B0AFF684D39C08F63B2193327187_H
#ifndef FROMBINDERNONGENERIC_T677B667DE367A055191AC7B64A622B8495C6CF89_H
#define FROMBINDERNONGENERIC_T677B667DE367A055191AC7B64A622B8495C6CF89_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.FromBinderNonGeneric
struct  FromBinderNonGeneric_t677B667DE367A055191AC7B64A622B8495C6CF89  : public FromBinder_t69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FROMBINDERNONGENERIC_T677B667DE367A055191AC7B64A622B8495C6CF89_H
#ifndef CONCRETEBINDERGENERIC_1_TDC772935855A7D8CFF1BF9CD8DB8696DCDAE19F1_H
#define CONCRETEBINDERGENERIC_1_TDC772935855A7D8CFF1BF9CD8DB8696DCDAE19F1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ConcreteBinderGeneric`1<System.Object>
struct  ConcreteBinderGeneric_1_tDC772935855A7D8CFF1BF9CD8DB8696DCDAE19F1  : public FromBinderGeneric_1_tB3D3D4B24691B0AFF684D39C08F63B2193327187
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONCRETEBINDERGENERIC_1_TDC772935855A7D8CFF1BF9CD8DB8696DCDAE19F1_H
#ifndef CONCRETEBINDERNONGENERIC_T607DFF5BDEE16A8EF207F5D6148835CD108F72F9_H
#define CONCRETEBINDERNONGENERIC_T607DFF5BDEE16A8EF207F5D6148835CD108F72F9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ConcreteBinderNonGeneric
struct  ConcreteBinderNonGeneric_t607DFF5BDEE16A8EF207F5D6148835CD108F72F9  : public FromBinderNonGeneric_t677B667DE367A055191AC7B64A622B8495C6CF89
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONCRETEBINDERNONGENERIC_T607DFF5BDEE16A8EF207F5D6148835CD108F72F9_H
#ifndef FACTORYTOCHOICEIDBINDER_1_TD76B8693EB5AEA17EA91AB32B01E1BE832EE8A4E_H
#define FACTORYTOCHOICEIDBINDER_1_TD76B8693EB5AEA17EA91AB32B01E1BE832EE8A4E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.FactoryToChoiceIdBinder`1<System.Object>
struct  FactoryToChoiceIdBinder_1_tD76B8693EB5AEA17EA91AB32B01E1BE832EE8A4E  : public FactoryToChoiceBinder_1_t1B20469F7095193A94918647E424C8F3DB8EEA7E
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACTORYTOCHOICEIDBINDER_1_TD76B8693EB5AEA17EA91AB32B01E1BE832EE8A4E_H
#ifndef CONCRETEIDBINDERGENERIC_1_TAD404DF3AD6E1FDBC78FD89CAC0C982BC2B642A7_H
#define CONCRETEIDBINDERGENERIC_1_TAD404DF3AD6E1FDBC78FD89CAC0C982BC2B642A7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ConcreteIdBinderGeneric`1<System.Object>
struct  ConcreteIdBinderGeneric_1_tAD404DF3AD6E1FDBC78FD89CAC0C982BC2B642A7  : public ConcreteBinderGeneric_1_tDC772935855A7D8CFF1BF9CD8DB8696DCDAE19F1
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONCRETEIDBINDERGENERIC_1_TAD404DF3AD6E1FDBC78FD89CAC0C982BC2B642A7_H
#ifndef CONCRETEIDBINDERNONGENERIC_T372B43CB3CC50C44EF038F152C0F6345DFA22E24_H
#define CONCRETEIDBINDERNONGENERIC_T372B43CB3CC50C44EF038F152C0F6345DFA22E24_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ConcreteIdBinderNonGeneric
struct  ConcreteIdBinderNonGeneric_t372B43CB3CC50C44EF038F152C0F6345DFA22E24  : public ConcreteBinderNonGeneric_t607DFF5BDEE16A8EF207F5D6148835CD108F72F9
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONCRETEIDBINDERNONGENERIC_T372B43CB3CC50C44EF038F152C0F6345DFA22E24_H
#ifndef MEMORYPOOLEXPANDBINDER_1_T5E44A2CA7F72A47AE602120481D62EBAEFC0E0A5_H
#define MEMORYPOOLEXPANDBINDER_1_T5E44A2CA7F72A47AE602120481D62EBAEFC0E0A5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.MemoryPoolExpandBinder`1<System.Object>
struct  MemoryPoolExpandBinder_1_t5E44A2CA7F72A47AE602120481D62EBAEFC0E0A5  : public FactoryToChoiceIdBinder_1_tD76B8693EB5AEA17EA91AB32B01E1BE832EE8A4E
{
public:
	// Zenject.MemoryPoolBindInfo Zenject.MemoryPoolExpandBinder`1::<MemoryPoolBindInfo>k__BackingField
	MemoryPoolBindInfo_tB7A75979568ABAD9B4C0F599B76DF4BDBD3A5D01 * ___U3CMemoryPoolBindInfoU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CMemoryPoolBindInfoU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(MemoryPoolExpandBinder_1_t5E44A2CA7F72A47AE602120481D62EBAEFC0E0A5, ___U3CMemoryPoolBindInfoU3Ek__BackingField_2)); }
	inline MemoryPoolBindInfo_tB7A75979568ABAD9B4C0F599B76DF4BDBD3A5D01 * get_U3CMemoryPoolBindInfoU3Ek__BackingField_2() const { return ___U3CMemoryPoolBindInfoU3Ek__BackingField_2; }
	inline MemoryPoolBindInfo_tB7A75979568ABAD9B4C0F599B76DF4BDBD3A5D01 ** get_address_of_U3CMemoryPoolBindInfoU3Ek__BackingField_2() { return &___U3CMemoryPoolBindInfoU3Ek__BackingField_2; }
	inline void set_U3CMemoryPoolBindInfoU3Ek__BackingField_2(MemoryPoolBindInfo_tB7A75979568ABAD9B4C0F599B76DF4BDBD3A5D01 * value)
	{
		___U3CMemoryPoolBindInfoU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMemoryPoolBindInfoU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMORYPOOLEXPANDBINDER_1_T5E44A2CA7F72A47AE602120481D62EBAEFC0E0A5_H
#ifndef MEMORYPOOLINITIALSIZEBINDER_1_T831900A50FA233E589596598650E789772546324_H
#define MEMORYPOOLINITIALSIZEBINDER_1_T831900A50FA233E589596598650E789772546324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.MemoryPoolInitialSizeBinder`1<System.Object>
struct  MemoryPoolInitialSizeBinder_1_t831900A50FA233E589596598650E789772546324  : public MemoryPoolExpandBinder_1_t5E44A2CA7F72A47AE602120481D62EBAEFC0E0A5
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMORYPOOLINITIALSIZEBINDER_1_T831900A50FA233E589596598650E789772546324_H
// System.Type[]
struct TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Type_t * m_Items[1];

public:
	inline Type_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Type_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Type_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Type_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Type_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Type_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};


// System.Void System.Func`3<System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void Func_3__ctor_mDCF191A98C4C31CEBD4FAD60551C0B4EA244E1A8_gshared (Func_3_t0875D079514B9064DE951B01B4AE82F6C7436F64 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
extern "C" IL2CPP_METHOD_ATTR void List_1_Add_m6930161974C7504C80F52EC379EF012387D43138_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, RuntimeObject * p0, const RuntimeMethod* method);
// Zenject.ConditionCopyNonLazyBinder Zenject.ArgConditionCopyNonLazyBinder::WithArguments<System.Object,System.Object>(TParam1,TParam2)
extern "C" IL2CPP_METHOD_ATTR ConditionCopyNonLazyBinder_tF7F37C5F1508AAB283151819E412C853A8D94B9B * ArgConditionCopyNonLazyBinder_WithArguments_TisRuntimeObject_TisRuntimeObject_m7ADAE64E9AD4C9F33DA8052EA450D19516ABDB2C_gshared (ArgConditionCopyNonLazyBinder_t5F1B4361E0805E20DDBEDEBC61B90CFFBB68B21B * __this, RuntimeObject * ___param10, RuntimeObject * ___param21, const RuntimeMethod* method);

// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
extern "C" IL2CPP_METHOD_ATTR Type_t * Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6 (RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  p0, const RuntimeMethod* method);
// Zenject.FromBinderNonGeneric Zenject.ConcreteBinderNonGeneric::To(System.Type[])
extern "C" IL2CPP_METHOD_ATTR FromBinderNonGeneric_t677B667DE367A055191AC7B64A622B8495C6CF89 * ConcreteBinderNonGeneric_To_m645DAE23651A7397F47DB3EDCF884379CAF38261 (ConcreteBinderNonGeneric_t607DFF5BDEE16A8EF207F5D6148835CD108F72F9 * __this, TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___concreteTypes0, const RuntimeMethod* method);
// Zenject.FromBinderNonGeneric Zenject.DiContainer::BindInterfacesAndSelfTo(System.Type)
extern "C" IL2CPP_METHOD_ATTR FromBinderNonGeneric_t677B667DE367A055191AC7B64A622B8495C6CF89 * DiContainer_BindInterfacesAndSelfTo_m63B0EA606AC67AE3F5F87B44D720A6183811F35E (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * __this, Type_t * ___type0, const RuntimeMethod* method);
// Zenject.FromBinderNonGeneric Zenject.DiContainer::BindInterfacesTo(System.Type)
extern "C" IL2CPP_METHOD_ATTR FromBinderNonGeneric_t677B667DE367A055191AC7B64A622B8495C6CF89 * DiContainer_BindInterfacesTo_m8C4D9BFFBDEE779BD35960740549A4C9ABB71970 (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * __this, Type_t * ___type0, const RuntimeMethod* method);
// Zenject.BindInfo Zenject.NonLazyBinder::get_BindInfo()
extern "C" IL2CPP_METHOD_ATTR BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * NonLazyBinder_get_BindInfo_mF5E53384F322F5C56719E3DEBDBB4426BE287FC4 (NonLazyBinder_tA9FB0768A2F39559FBDDCAFBF940AAD240B7C43C * __this, const RuntimeMethod* method);
// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.BindInfo::get_Arguments()
extern "C" IL2CPP_METHOD_ATTR List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * BindInfo_get_Arguments_mE740D3FFEBCD5CB04091FE9C7901D0F033801D80 (BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * __this, const RuntimeMethod* method);
// System.Void Zenject.BindInfo::.ctor(System.Type)
extern "C" IL2CPP_METHOD_ATTR void BindInfo__ctor_m0653A2A5799A7A231939E22C2E728AFA4B006B6D (BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * __this, Type_t * ___contractType0, const RuntimeMethod* method);
// Zenject.BindFinalizerWrapper Zenject.DiContainer::StartBinding()
extern "C" IL2CPP_METHOD_ATTR BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 * DiContainer_StartBinding_mC62BEAE4A89F0380BDCA1FF4747BD12D15CC2C2E (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * __this, const RuntimeMethod* method);
// System.Void System.Func`3<Zenject.DiContainer,System.Type,Zenject.IProvider>::.ctor(System.Object,System.IntPtr)
inline void Func_3__ctor_m0FFFF142D2BDA3D5294EED98E0A00AAD6961E0A0 (Func_3_tF2BBB0DC92895D84974BAECD6D1B2AE7FFC65AAA * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method)
{
	((  void (*) (Func_3_tF2BBB0DC92895D84974BAECD6D1B2AE7FFC65AAA *, RuntimeObject *, intptr_t, const RuntimeMethod*))Func_3__ctor_mDCF191A98C4C31CEBD4FAD60551C0B4EA244E1A8_gshared)(__this, p0, p1, method);
}
// System.Void Zenject.ScopableBindingFinalizer::.ctor(Zenject.BindInfo,Zenject.SingletonTypes,System.Object,System.Func`3<Zenject.DiContainer,System.Type,Zenject.IProvider>)
extern "C" IL2CPP_METHOD_ATTR void ScopableBindingFinalizer__ctor_m330A27CBA37E6C02F51614042BCE169953861BFF (ScopableBindingFinalizer_t1278F9BE4EE396C7D8B5CA5625C540EC3582632C * __this, BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * ___bindInfo0, int32_t ___singletonType1, RuntimeObject * ___singletonSpecificId2, Func_3_tF2BBB0DC92895D84974BAECD6D1B2AE7FFC65AAA * ___providerFactory3, const RuntimeMethod* method);
// System.Void Zenject.BindFinalizerWrapper::set_SubFinalizer(Zenject.IBindingFinalizer)
extern "C" IL2CPP_METHOD_ATTR void BindFinalizerWrapper_set_SubFinalizer_m6AF16F9C3D2BE2F006E60D76D85F20EFE8C76874 (BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 * __this, RuntimeObject* ___value0, const RuntimeMethod* method);
// System.Void Zenject.IdScopeConditionCopyNonLazyBinder::.ctor(Zenject.BindInfo)
extern "C" IL2CPP_METHOD_ATTR void IdScopeConditionCopyNonLazyBinder__ctor_m624AAE35191DFF2DEB38C7C83A325D0322C12CD7 (IdScopeConditionCopyNonLazyBinder_tF4C46379BF6B4595053DBCAB75767E759A18ECEE * __this, BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * ___bindInfo0, const RuntimeMethod* method);
// System.Collections.Generic.List`1<System.Type> Zenject.BindInfo::get_ContractTypes()
extern "C" IL2CPP_METHOD_ATTR List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0 * BindInfo_get_ContractTypes_m493FD195FF21350E33D380CA7D3DCA61695E4BB4 (BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Type>::Add(!0)
inline void List_1_Add_m57994E0773CB740B2495C2AE0CE7CBE3C47BB938 (List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0 * __this, Type_t * p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0 *, Type_t *, const RuntimeMethod*))List_1_Add_m6930161974C7504C80F52EC379EF012387D43138_gshared)(__this, p0, method);
}
// System.Void Zenject.FactoryBindInfo::.ctor(System.Type)
extern "C" IL2CPP_METHOD_ATTR void FactoryBindInfo__ctor_m2061ED219CF2D7C82D996731D18CE0F1508CD56C (FactoryBindInfo_t13E5B099259F31F1EF31D3550FE94DF78C9F1C00 * __this, Type_t * ___factoryType0, const RuntimeMethod* method);
// System.Void Zenject.MemoryPoolBindInfo::.ctor()
extern "C" IL2CPP_METHOD_ATTR void MemoryPoolBindInfo__ctor_mF320AA3C6D60D763A3BC276D8B3C91E33B043A47 (MemoryPoolBindInfo_tB7A75979568ABAD9B4C0F599B76DF4BDBD3A5D01 * __this, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerable`1<System.Type> Zenject.FromBinder::get_AllParentTypes()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* FromBinder_get_AllParentTypes_m63693115CEF2E9320ED2C7FC487E65922E5280C3 (FromBinder_t69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75 * __this, const RuntimeMethod* method);
// System.Void Zenject.BindingUtil::AssertIsDerivedFromTypes(System.Type,System.Collections.Generic.IEnumerable`1<System.Type>)
extern "C" IL2CPP_METHOD_ATTR void BindingUtil_AssertIsDerivedFromTypes_m96464ADDE8B2DC7631EEF597C4A6BD3177F01A0A (Type_t * ___concreteType0, RuntimeObject* ___parentTypes1, const RuntimeMethod* method);
// System.Void Zenject.BindInfo::set_RequireExplicitScope(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void BindInfo_set_RequireExplicitScope_mDC1F2FA5156C50519ADEE9662AE7CA900F522DE9 (BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void Zenject.FromBinder::set_SubFinalizer(Zenject.IBindingFinalizer)
extern "C" IL2CPP_METHOD_ATTR void FromBinder_set_SubFinalizer_mA7D75C7F3CFC63AB07447CBF5F7B43A584AD7E09 (FromBinder_t69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75 * __this, RuntimeObject* ___value0, const RuntimeMethod* method);
// System.Void Zenject.ScopeArgConditionCopyNonLazyBinder::.ctor(Zenject.BindInfo)
extern "C" IL2CPP_METHOD_ATTR void ScopeArgConditionCopyNonLazyBinder__ctor_m6EA69747BB9E3C8190B7018268FE19F17E5DEDC1 (ScopeArgConditionCopyNonLazyBinder_t218A003451367EEC6A439D63A0F38B964EF07B33 * __this, BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * ___bindInfo0, const RuntimeMethod* method);
// System.Void Zenject.SingletonImplIds/ToMethod::.ctor(System.Delegate)
extern "C" IL2CPP_METHOD_ATTR void ToMethod__ctor_mE467473AC4FB18D55CA26E25EF57FCC5578D9B7A (ToMethod_t12F5E0A4D9C370BB507C42523A4401D55EBE2836 * __this, Delegate_t * ___method0, const RuntimeMethod* method);
// System.Void Zenject.SingletonImplIds/ToGetter::.ctor(System.Object,System.Delegate)
extern "C" IL2CPP_METHOD_ATTR void ToGetter__ctor_m60F6511830872820A5BEBA4561C5F91A435D6BF8 (ToGetter_t269C4EED29F0AC2A1C9FB4061EA228DC2DDADED4 * __this, RuntimeObject * ___identifier0, Delegate_t * ___method1, const RuntimeMethod* method);
// System.Void Zenject.ScopeConditionCopyNonLazyBinder::.ctor(Zenject.BindInfo)
extern "C" IL2CPP_METHOD_ATTR void ScopeConditionCopyNonLazyBinder__ctor_m35C93A0113B188F814DAC675B4DB6D089A4B9342 (ScopeConditionCopyNonLazyBinder_t6221939C8173223799835D9DF0944D6D777C3EC6 * __this, BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * ___bindInfo0, const RuntimeMethod* method);
// Zenject.ScopeConditionCopyNonLazyBinder Zenject.SubContainerBinder::ByInstaller(System.Type)
extern "C" IL2CPP_METHOD_ATTR ScopeConditionCopyNonLazyBinder_t6221939C8173223799835D9DF0944D6D777C3EC6 * SubContainerBinder_ByInstaller_m59CFC60CD64FF80F0FE96B542484A7F034BF62FA (SubContainerBinder_t24411FF88DADEE6DFBC16EE722E0F4944E8732D6 * __this, Type_t * ___installerType0, const RuntimeMethod* method);
// System.Void Zenject.ScopeNonLazyBinder::.ctor(Zenject.BindInfo)
extern "C" IL2CPP_METHOD_ATTR void ScopeNonLazyBinder__ctor_m37680839EDF90FF98C733A21C48D95CFAF1A30CB (ScopeNonLazyBinder_t7BC7AB773CA2EAECC76C3B733A1271EE6866646A * __this, BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * ___bindInfo0, const RuntimeMethod* method);
// System.Void Zenject.SignalSettings::.ctor()
extern "C" IL2CPP_METHOD_ATTR void SignalSettings__ctor_m832E7FDF44930C19D612D5996D4BDAC71723F1E3 (SignalSettings_tD753AA3D16CEA36F97CDAEC5428C5217F1EB6540 * __this, const RuntimeMethod* method);
// Zenject.ArgConditionCopyNonLazyBinder Zenject.ScopeArgConditionCopyNonLazyBinder::AsCached()
extern "C" IL2CPP_METHOD_ATTR ArgConditionCopyNonLazyBinder_t5F1B4361E0805E20DDBEDEBC61B90CFFBB68B21B * ScopeArgConditionCopyNonLazyBinder_AsCached_mAAD80E3CF24D5B46128725D1045BCA847A1205D2 (ScopeArgConditionCopyNonLazyBinder_t218A003451367EEC6A439D63A0F38B964EF07B33 * __this, const RuntimeMethod* method);
// Zenject.ConditionCopyNonLazyBinder Zenject.ArgConditionCopyNonLazyBinder::WithArguments<Zenject.SignalSettings,Zenject.BindInfo>(TParam1,TParam2)
inline ConditionCopyNonLazyBinder_tF7F37C5F1508AAB283151819E412C853A8D94B9B * ArgConditionCopyNonLazyBinder_WithArguments_TisSignalSettings_tD753AA3D16CEA36F97CDAEC5428C5217F1EB6540_TisBindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991_mD9A3095921FC1EFD57D2AD25CFAA64C2A5C56FF8 (ArgConditionCopyNonLazyBinder_t5F1B4361E0805E20DDBEDEBC61B90CFFBB68B21B * __this, SignalSettings_tD753AA3D16CEA36F97CDAEC5428C5217F1EB6540 * ___param10, BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * ___param21, const RuntimeMethod* method)
{
	return ((  ConditionCopyNonLazyBinder_tF7F37C5F1508AAB283151819E412C853A8D94B9B * (*) (ArgConditionCopyNonLazyBinder_t5F1B4361E0805E20DDBEDEBC61B90CFFBB68B21B *, SignalSettings_tD753AA3D16CEA36F97CDAEC5428C5217F1EB6540 *, BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *, const RuntimeMethod*))ArgConditionCopyNonLazyBinder_WithArguments_TisRuntimeObject_TisRuntimeObject_m7ADAE64E9AD4C9F33DA8052EA450D19516ABDB2C_gshared)(__this, ___param10, ___param21, method);
}
// System.Void Zenject.SignalBinderWithId::.ctor(Zenject.BindInfo,Zenject.SignalSettings)
extern "C" IL2CPP_METHOD_ATTR void SignalBinderWithId__ctor_mC60EC8985D8B8AD5881C02CE95B0A8D992DF63F6 (SignalBinderWithId_tF62257893A9AA47A533D47DC5DB0C0C046F67B81 * __this, BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * ___bindInfo0, SignalSettings_tD753AA3D16CEA36F97CDAEC5428C5217F1EB6540 * ___signalSettings1, const RuntimeMethod* method);
// System.Void Zenject.NullBindingFinalizer::.ctor()
extern "C" IL2CPP_METHOD_ATTR void NullBindingFinalizer__ctor_m2B90434471F862EF8E0C197230BDB1EAB586834A (NullBindingFinalizer_tFEAF0122BA4AF8A5209DE0330F0BFF444F06AD65 * __this, const RuntimeMethod* method);
// System.Guid System.Guid::NewGuid()
extern "C" IL2CPP_METHOD_ATTR Guid_t  Guid_NewGuid_m541CAC23EBB140DFD3AB5B313315647E95FADB29 (const RuntimeMethod* method);
// Zenject.ConcreteIdBinderNonGeneric Zenject.DiContainer::Bind(System.Type[])
extern "C" IL2CPP_METHOD_ATTR ConcreteIdBinderNonGeneric_t372B43CB3CC50C44EF038F152C0F6345DFA22E24 * DiContainer_Bind_m838139625D2905AE9B447C19F948EEE024380782 (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * __this, TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___contractTypes0, const RuntimeMethod* method);
// System.Void Zenject.InjectContext::.ctor(Zenject.DiContainer,System.Type,System.Object)
extern "C" IL2CPP_METHOD_ATTR void InjectContext__ctor_mEA73C2DE706F160699AA9C80A0171F62225C8F2E (InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * __this, DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ___container0, Type_t * ___memberType1, RuntimeObject * ___identifier2, const RuntimeMethod* method);
// System.Object Zenject.SignalHandlerBinder::get_Identifier()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * SignalHandlerBinder_get_Identifier_mDA06F489C05DEA9CAB1FBB2224D6A4A2A8341C0D (SignalHandlerBinder_t4AB70F7A7BD1596D46B88F834A400EBEDE185771 * __this, const RuntimeMethod* method);
// System.Void Zenject.BindingId::.ctor(System.Type,System.Object)
extern "C" IL2CPP_METHOD_ATTR void BindingId__ctor_mA092ED49B59C24FC8075ABB8C6070B8FEBFC19B1 (BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA * __this, Type_t * ___type0, RuntimeObject * ___identifier1, const RuntimeMethod* method);
// System.Void Zenject.SignalHandlerBinderWithId::.ctor(Zenject.DiContainer,System.Type,Zenject.BindFinalizerWrapper)
extern "C" IL2CPP_METHOD_ATTR void SignalHandlerBinderWithId__ctor_m32ECF91BFE03FBEAB9069263E745A645225396CE (SignalHandlerBinderWithId_tF2F486B2BBE4565DB9C112C9D5755BDCFE7BCD27 * __this, DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ___container0, Type_t * ___signalType1, BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 * ___finalizerWrapper2, const RuntimeMethod* method);
// System.Type System.Object::GetType()
extern "C" IL2CPP_METHOD_ATTR Type_t * Object_GetType_m2E0B62414ECCAA3094B703790CE88CBB2F83EA60 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void Zenject.TypeValuePair::.ctor(System.Type,System.Object)
extern "C" IL2CPP_METHOD_ATTR void TypeValuePair__ctor_m2129895C0D465FFE1E5DE30CECE2EF01A3185860 (TypeValuePair_tD309CDC658476FDDF08345C92D3F4D7A0A1F9792 * __this, Type_t * ___type0, RuntimeObject * ___value1, const RuntimeMethod* method);
// Zenject.ZenjectTypeInfo Zenject.TypeAnalyzer::GetInfo(System.Type)
extern "C" IL2CPP_METHOD_ATTR ZenjectTypeInfo_t2866A662C0FDA5B357BCC135021933EE8914535E * TypeAnalyzer_GetInfo_m7D98EB86402655EC572CBFA64ADC04E199C88CF4 (Type_t * ___type0, const RuntimeMethod* method);
// Zenject.FromBinderNonGeneric Zenject.ConcreteBinderNonGeneric::To<System.Object>()
extern "C" IL2CPP_METHOD_ATTR FromBinderNonGeneric_t677B667DE367A055191AC7B64A622B8495C6CF89 * ConcreteBinderNonGeneric_To_TisRuntimeObject_mE7A8C0B5609C65749168E03B0B82307BA2E9E2B5_gshared (ConcreteBinderNonGeneric_t607DFF5BDEE16A8EF207F5D6148835CD108F72F9 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ConcreteBinderNonGeneric_To_TisRuntimeObject_mE7A8C0B5609C65749168E03B0B82307BA2E9E2B5_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* L_0 = (TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)SZArrayNew(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F_il2cpp_TypeInfo_var, (uint32_t)1);
		TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* L_1 = (TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)L_0;
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_2 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6((RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D )L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_3);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_3);
		NullCheck((ConcreteBinderNonGeneric_t607DFF5BDEE16A8EF207F5D6148835CD108F72F9 *)__this);
		FromBinderNonGeneric_t677B667DE367A055191AC7B64A622B8495C6CF89 * L_4 = ConcreteBinderNonGeneric_To_m645DAE23651A7397F47DB3EDCF884379CAF38261((ConcreteBinderNonGeneric_t607DFF5BDEE16A8EF207F5D6148835CD108F72F9 *)__this, (TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)L_1, /*hidden argument*/NULL);
		return L_4;
	}
}
// Zenject.FromBinderNonGeneric Zenject.DiContainer::BindInterfacesAndSelfTo<System.Object>()
extern "C" IL2CPP_METHOD_ATTR FromBinderNonGeneric_t677B667DE367A055191AC7B64A622B8495C6CF89 * DiContainer_BindInterfacesAndSelfTo_TisRuntimeObject_m232B041132998F17891F0723CC1C7A22F5926875_gshared (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DiContainer_BindInterfacesAndSelfTo_TisRuntimeObject_m232B041132998F17891F0723CC1C7A22F5926875_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_0 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6((RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D )L_0, /*hidden argument*/NULL);
		NullCheck((DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)__this);
		FromBinderNonGeneric_t677B667DE367A055191AC7B64A622B8495C6CF89 * L_2 = DiContainer_BindInterfacesAndSelfTo_m63B0EA606AC67AE3F5F87B44D720A6183811F35E((DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)__this, (Type_t *)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// Zenject.FromBinderNonGeneric Zenject.DiContainer::BindInterfacesTo<System.Object>()
extern "C" IL2CPP_METHOD_ATTR FromBinderNonGeneric_t677B667DE367A055191AC7B64A622B8495C6CF89 * DiContainer_BindInterfacesTo_TisRuntimeObject_m4C1025EA591985E3BD8CC62353998FC1F2434424_gshared (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DiContainer_BindInterfacesTo_TisRuntimeObject_m4C1025EA591985E3BD8CC62353998FC1F2434424_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_0 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6((RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D )L_0, /*hidden argument*/NULL);
		NullCheck((DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)__this);
		FromBinderNonGeneric_t677B667DE367A055191AC7B64A622B8495C6CF89 * L_2 = DiContainer_BindInterfacesTo_m8C4D9BFFBDEE779BD35960740549A4C9ABB71970((DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)__this, (Type_t *)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// Zenject.IProvider Zenject.FactoryFromBinder`1<System.Object>::<FromFactory>b__5_0<System.Object>(Zenject.DiContainer)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* FactoryFromBinder_1_U3CFromFactoryU3Eb__5_0_TisRuntimeObject_m6E2DA3566B612965C2B53591AD6BB464DCBE7BE1_gshared (FactoryFromBinder_1_t8A38890E41D19C23AC52C50A9AEF8B9DA43E17D4 * __this, DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ___container0, const RuntimeMethod* method)
{
	{
		DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * L_0 = ___container0;
		NullCheck((NonLazyBinder_tA9FB0768A2F39559FBDDCAFBF940AAD240B7C43C *)__this);
		BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * L_1 = NonLazyBinder_get_BindInfo_mF5E53384F322F5C56719E3DEBDBB4426BE287FC4((NonLazyBinder_tA9FB0768A2F39559FBDDCAFBF940AAD240B7C43C *)__this, /*hidden argument*/NULL);
		NullCheck((BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)L_1);
		List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * L_2 = BindInfo_get_Arguments_mE740D3FFEBCD5CB04091FE9C7901D0F033801D80((BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)L_1, /*hidden argument*/NULL);
		FactoryProvider_2_tA5A2F50F6778F0A3FBF4C73BD0CF6787D9640ACA * L_3 = (FactoryProvider_2_tA5A2F50F6778F0A3FBF4C73BD0CF6787D9640ACA *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (FactoryProvider_2_tA5A2F50F6778F0A3FBF4C73BD0CF6787D9640ACA *, DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *, List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(L_3, (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)L_0, (List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_3;
	}
}
// Zenject.IProvider Zenject.FromBinder::<FromFactoryBase>b__37_0<System.Object,System.Object>(Zenject.DiContainer,System.Type)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* FromBinder_U3CFromFactoryBaseU3Eb__37_0_TisRuntimeObject_TisRuntimeObject_m6A7C34C7DDA7CA3E7ECDD0162F1DBB602955E3C6_gshared (FromBinder_t69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75 * __this, DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ___container0, Type_t * ___type1, const RuntimeMethod* method)
{
	{
		DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * L_0 = ___container0;
		NullCheck((NonLazyBinder_tA9FB0768A2F39559FBDDCAFBF940AAD240B7C43C *)__this);
		BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * L_1 = NonLazyBinder_get_BindInfo_mF5E53384F322F5C56719E3DEBDBB4426BE287FC4((NonLazyBinder_tA9FB0768A2F39559FBDDCAFBF940AAD240B7C43C *)__this, /*hidden argument*/NULL);
		NullCheck((BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)L_1);
		List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * L_2 = BindInfo_get_Arguments_mE740D3FFEBCD5CB04091FE9C7901D0F033801D80((BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)L_1, /*hidden argument*/NULL);
		FactoryProvider_2_tA5A2F50F6778F0A3FBF4C73BD0CF6787D9640ACA * L_3 = (FactoryProvider_2_tA5A2F50F6778F0A3FBF4C73BD0CF6787D9640ACA *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (FactoryProvider_2_tA5A2F50F6778F0A3FBF4C73BD0CF6787D9640ACA *, DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *, List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(L_3, (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)L_0, (List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_3;
	}
}
// Zenject.IdScopeConditionCopyNonLazyBinder Zenject.DiContainer::BindInstance<System.Object>(TContract)
extern "C" IL2CPP_METHOD_ATTR IdScopeConditionCopyNonLazyBinder_tF4C46379BF6B4595053DBCAB75767E759A18ECEE * DiContainer_BindInstance_TisRuntimeObject_m98F908B1F4EB94BB04404CA3B0A1CA24A64D0548_gshared (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * __this, RuntimeObject * ___instance0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DiContainer_BindInstance_TisRuntimeObject_m98F908B1F4EB94BB04404CA3B0A1CA24A64D0548_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass181_0_1_tC62938210FC962790D274FA4E04218CD84EA7EB3 * V_0 = NULL;
	BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * V_1 = NULL;
	{
		U3CU3Ec__DisplayClass181_0_1_tC62938210FC962790D274FA4E04218CD84EA7EB3 * L_0 = (U3CU3Ec__DisplayClass181_0_1_tC62938210FC962790D274FA4E04218CD84EA7EB3 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (U3CU3Ec__DisplayClass181_0_1_tC62938210FC962790D274FA4E04218CD84EA7EB3 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		V_0 = (U3CU3Ec__DisplayClass181_0_1_tC62938210FC962790D274FA4E04218CD84EA7EB3 *)L_0;
		U3CU3Ec__DisplayClass181_0_1_tC62938210FC962790D274FA4E04218CD84EA7EB3 * L_1 = V_0;
		RuntimeObject * L_2 = ___instance0;
		NullCheck(L_1);
		L_1->set_instance_0(L_2);
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_3 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 2)) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6((RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D )L_3, /*hidden argument*/NULL);
		BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * L_5 = (BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)il2cpp_codegen_object_new(BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991_il2cpp_TypeInfo_var);
		BindInfo__ctor_m0653A2A5799A7A231939E22C2E728AFA4B006B6D(L_5, (Type_t *)L_4, /*hidden argument*/NULL);
		V_1 = (BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)L_5;
		NullCheck((DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)__this);
		BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 * L_6 = DiContainer_StartBinding_mC62BEAE4A89F0380BDCA1FF4747BD12D15CC2C2E((DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)__this, /*hidden argument*/NULL);
		BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * L_7 = V_1;
		U3CU3Ec__DisplayClass181_0_1_tC62938210FC962790D274FA4E04218CD84EA7EB3 * L_8 = V_0;
		NullCheck(L_8);
		RuntimeObject * L_9 = (RuntimeObject *)L_8->get_instance_0();
		U3CU3Ec__DisplayClass181_0_1_tC62938210FC962790D274FA4E04218CD84EA7EB3 * L_10 = V_0;
		Func_3_tF2BBB0DC92895D84974BAECD6D1B2AE7FFC65AAA * L_11 = (Func_3_tF2BBB0DC92895D84974BAECD6D1B2AE7FFC65AAA *)il2cpp_codegen_object_new(Func_3_tF2BBB0DC92895D84974BAECD6D1B2AE7FFC65AAA_il2cpp_TypeInfo_var);
		Func_3__ctor_m0FFFF142D2BDA3D5294EED98E0A00AAD6961E0A0(L_11, (RuntimeObject *)L_10, (intptr_t)((intptr_t)IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4)), /*hidden argument*/Func_3__ctor_m0FFFF142D2BDA3D5294EED98E0A00AAD6961E0A0_RuntimeMethod_var);
		ScopableBindingFinalizer_t1278F9BE4EE396C7D8B5CA5625C540EC3582632C * L_12 = (ScopableBindingFinalizer_t1278F9BE4EE396C7D8B5CA5625C540EC3582632C *)il2cpp_codegen_object_new(ScopableBindingFinalizer_t1278F9BE4EE396C7D8B5CA5625C540EC3582632C_il2cpp_TypeInfo_var);
		ScopableBindingFinalizer__ctor_m330A27CBA37E6C02F51614042BCE169953861BFF(L_12, (BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)L_7, (int32_t)4, (RuntimeObject *)L_9, (Func_3_tF2BBB0DC92895D84974BAECD6D1B2AE7FFC65AAA *)L_11, /*hidden argument*/NULL);
		NullCheck((BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 *)L_6);
		BindFinalizerWrapper_set_SubFinalizer_m6AF16F9C3D2BE2F006E60D76D85F20EFE8C76874((BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 *)L_6, (RuntimeObject*)L_12, /*hidden argument*/NULL);
		BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * L_13 = V_1;
		IdScopeConditionCopyNonLazyBinder_tF4C46379BF6B4595053DBCAB75767E759A18ECEE * L_14 = (IdScopeConditionCopyNonLazyBinder_tF4C46379BF6B4595053DBCAB75767E759A18ECEE *)il2cpp_codegen_object_new(IdScopeConditionCopyNonLazyBinder_tF4C46379BF6B4595053DBCAB75767E759A18ECEE_il2cpp_TypeInfo_var);
		IdScopeConditionCopyNonLazyBinder__ctor_m624AAE35191DFF2DEB38C7C83A325D0322C12CD7(L_14, (BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
// Zenject.MemoryPoolInitialSizeBinder`1<TItemContract> Zenject.DiContainer::BindMemoryPool<System.Object,System.Object,System.Object>()
extern "C" IL2CPP_METHOD_ATTR MemoryPoolInitialSizeBinder_1_t831900A50FA233E589596598650E789772546324 * DiContainer_BindMemoryPool_TisRuntimeObject_TisRuntimeObject_TisRuntimeObject_m685E7FB309A0EB33BE5E3258AB7ADDEEF634BC27_gshared (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DiContainer_BindMemoryPool_TisRuntimeObject_TisRuntimeObject_TisRuntimeObject_m685E7FB309A0EB33BE5E3258AB7ADDEEF634BC27_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * V_0 = NULL;
	FactoryBindInfo_t13E5B099259F31F1EF31D3550FE94DF78C9F1C00 * V_1 = NULL;
	MemoryPoolBindInfo_tB7A75979568ABAD9B4C0F599B76DF4BDBD3A5D01 * V_2 = NULL;
	{
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_0 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6((RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D )L_0, /*hidden argument*/NULL);
		BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * L_2 = (BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)il2cpp_codegen_object_new(BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991_il2cpp_TypeInfo_var);
		BindInfo__ctor_m0653A2A5799A7A231939E22C2E728AFA4B006B6D(L_2, (Type_t *)L_1, /*hidden argument*/NULL);
		V_0 = (BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)L_2;
		BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * L_3 = V_0;
		NullCheck((BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)L_3);
		List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0 * L_4 = BindInfo_get_ContractTypes_m493FD195FF21350E33D380CA7D3DCA61695E4BB4((BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)L_3, /*hidden argument*/NULL);
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_5 = { reinterpret_cast<intptr_t> (IMemoryPool_t6DF3F57F29BCA8BB662B1642357C53A871F0202B_0_0_0_var) };
		Type_t * L_6 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6((RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D )L_5, /*hidden argument*/NULL);
		NullCheck((List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0 *)L_4);
		List_1_Add_m57994E0773CB740B2495C2AE0CE7CBE3C47BB938((List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0 *)L_4, (Type_t *)L_6, /*hidden argument*/List_1_Add_m57994E0773CB740B2495C2AE0CE7CBE3C47BB938_RuntimeMethod_var);
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_7 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 1)) };
		Type_t * L_8 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6((RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D )L_7, /*hidden argument*/NULL);
		FactoryBindInfo_t13E5B099259F31F1EF31D3550FE94DF78C9F1C00 * L_9 = (FactoryBindInfo_t13E5B099259F31F1EF31D3550FE94DF78C9F1C00 *)il2cpp_codegen_object_new(FactoryBindInfo_t13E5B099259F31F1EF31D3550FE94DF78C9F1C00_il2cpp_TypeInfo_var);
		FactoryBindInfo__ctor_m2061ED219CF2D7C82D996731D18CE0F1508CD56C(L_9, (Type_t *)L_8, /*hidden argument*/NULL);
		V_1 = (FactoryBindInfo_t13E5B099259F31F1EF31D3550FE94DF78C9F1C00 *)L_9;
		MemoryPoolBindInfo_tB7A75979568ABAD9B4C0F599B76DF4BDBD3A5D01 * L_10 = (MemoryPoolBindInfo_tB7A75979568ABAD9B4C0F599B76DF4BDBD3A5D01 *)il2cpp_codegen_object_new(MemoryPoolBindInfo_tB7A75979568ABAD9B4C0F599B76DF4BDBD3A5D01_il2cpp_TypeInfo_var);
		MemoryPoolBindInfo__ctor_mF320AA3C6D60D763A3BC276D8B3C91E33B043A47(L_10, /*hidden argument*/NULL);
		V_2 = (MemoryPoolBindInfo_tB7A75979568ABAD9B4C0F599B76DF4BDBD3A5D01 *)L_10;
		NullCheck((DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)__this);
		BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 * L_11 = DiContainer_StartBinding_mC62BEAE4A89F0380BDCA1FF4747BD12D15CC2C2E((DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)__this, /*hidden argument*/NULL);
		BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * L_12 = V_0;
		FactoryBindInfo_t13E5B099259F31F1EF31D3550FE94DF78C9F1C00 * L_13 = V_1;
		MemoryPoolBindInfo_tB7A75979568ABAD9B4C0F599B76DF4BDBD3A5D01 * L_14 = V_2;
		MemoryPoolBindingFinalizer_1_tAE0E857B07874662F4D4844CC5ED6495972A128F * L_15 = (MemoryPoolBindingFinalizer_1_tAE0E857B07874662F4D4844CC5ED6495972A128F *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 2));
		((  void (*) (MemoryPoolBindingFinalizer_1_tAE0E857B07874662F4D4844CC5ED6495972A128F *, BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *, FactoryBindInfo_t13E5B099259F31F1EF31D3550FE94DF78C9F1C00 *, MemoryPoolBindInfo_tB7A75979568ABAD9B4C0F599B76DF4BDBD3A5D01 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3)->methodPointer)(L_15, (BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)L_12, (FactoryBindInfo_t13E5B099259F31F1EF31D3550FE94DF78C9F1C00 *)L_13, (MemoryPoolBindInfo_tB7A75979568ABAD9B4C0F599B76DF4BDBD3A5D01 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3));
		NullCheck((BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 *)L_11);
		BindFinalizerWrapper_set_SubFinalizer_m6AF16F9C3D2BE2F006E60D76D85F20EFE8C76874((BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 *)L_11, (RuntimeObject*)L_15, /*hidden argument*/NULL);
		BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * L_16 = V_0;
		FactoryBindInfo_t13E5B099259F31F1EF31D3550FE94DF78C9F1C00 * L_17 = V_1;
		MemoryPoolBindInfo_tB7A75979568ABAD9B4C0F599B76DF4BDBD3A5D01 * L_18 = V_2;
		MemoryPoolInitialSizeBinder_1_t831900A50FA233E589596598650E789772546324 * L_19 = (MemoryPoolInitialSizeBinder_1_t831900A50FA233E589596598650E789772546324 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 4));
		((  void (*) (MemoryPoolInitialSizeBinder_1_t831900A50FA233E589596598650E789772546324 *, BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *, FactoryBindInfo_t13E5B099259F31F1EF31D3550FE94DF78C9F1C00 *, MemoryPoolBindInfo_tB7A75979568ABAD9B4C0F599B76DF4BDBD3A5D01 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 5)->methodPointer)(L_19, (BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)L_16, (FactoryBindInfo_t13E5B099259F31F1EF31D3550FE94DF78C9F1C00 *)L_17, (MemoryPoolBindInfo_tB7A75979568ABAD9B4C0F599B76DF4BDBD3A5D01 *)L_18, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 5));
		return L_19;
	}
}
// Zenject.MemoryPoolInitialSizeBinder`1<TItemContract> Zenject.DiContainer::BindMemoryPool<System.Object,System.Object>()
extern "C" IL2CPP_METHOD_ATTR MemoryPoolInitialSizeBinder_1_t831900A50FA233E589596598650E789772546324 * DiContainer_BindMemoryPool_TisRuntimeObject_TisRuntimeObject_m99AD91EB64584C4F597AABCF6D359E77C9DACF52_gshared (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)__this);
		MemoryPoolInitialSizeBinder_1_t831900A50FA233E589596598650E789772546324 * L_0 = ((  MemoryPoolInitialSizeBinder_1_t831900A50FA233E589596598650E789772546324 * (*) (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_0;
	}
}
// Zenject.MemoryPoolInitialSizeBinder`1<TItemContract> Zenject.DiContainer::BindMemoryPool<System.Object>()
extern "C" IL2CPP_METHOD_ATTR MemoryPoolInitialSizeBinder_1_t831900A50FA233E589596598650E789772546324 * DiContainer_BindMemoryPool_TisRuntimeObject_m014A3034D78A4CEBB15B820C0B2C9F699864DCFF_gshared (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)__this);
		MemoryPoolInitialSizeBinder_1_t831900A50FA233E589596598650E789772546324 * L_0 = ((  MemoryPoolInitialSizeBinder_1_t831900A50FA233E589596598650E789772546324 * (*) (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_0;
	}
}
// Zenject.NameTransformConditionCopyNonLazyBinder Zenject.FactorySubContainerBinderBase`1<System.Object>::ByNewPrefabInstaller<System.Object>(UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR NameTransformConditionCopyNonLazyBinder_t2E9664EE069D9C6CA15EFEDD64FAE2B4125AC36D * FactorySubContainerBinderBase_1_ByNewPrefabInstaller_TisRuntimeObject_mAB991259136BCE813E2B850B78B2316AFD49B8DC_gshared (FactorySubContainerBinderBase_1_t10122DEBCBD743FE20FF2CCC6889131951876194 * __this, Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___prefab0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FactorySubContainerBinderBase_1_ByNewPrefabInstaller_TisRuntimeObject_mAB991259136BCE813E2B850B78B2316AFD49B8DC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * L_0 = ___prefab0;
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_1 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6((RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D )L_1, /*hidden argument*/NULL);
		NullCheck((FactorySubContainerBinderBase_1_t10122DEBCBD743FE20FF2CCC6889131951876194 *)__this);
		NameTransformConditionCopyNonLazyBinder_t2E9664EE069D9C6CA15EFEDD64FAE2B4125AC36D * L_3 = ((  NameTransformConditionCopyNonLazyBinder_t2E9664EE069D9C6CA15EFEDD64FAE2B4125AC36D * (*) (FactorySubContainerBinderBase_1_t10122DEBCBD743FE20FF2CCC6889131951876194 *, Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *, Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)((FactorySubContainerBinderBase_1_t10122DEBCBD743FE20FF2CCC6889131951876194 *)__this, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)L_0, (Type_t *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return L_3;
	}
}
// Zenject.NameTransformConditionCopyNonLazyBinder Zenject.FactorySubContainerBinderBase`1<System.Object>::ByNewPrefabResourceInstaller<System.Object>(System.String)
extern "C" IL2CPP_METHOD_ATTR NameTransformConditionCopyNonLazyBinder_t2E9664EE069D9C6CA15EFEDD64FAE2B4125AC36D * FactorySubContainerBinderBase_1_ByNewPrefabResourceInstaller_TisRuntimeObject_m296FE0F1CD5C1DF3EB42956CD186C8E30749DFB9_gshared (FactorySubContainerBinderBase_1_t10122DEBCBD743FE20FF2CCC6889131951876194 * __this, String_t* ___resourcePath0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FactorySubContainerBinderBase_1_ByNewPrefabResourceInstaller_TisRuntimeObject_m296FE0F1CD5C1DF3EB42956CD186C8E30749DFB9_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___resourcePath0;
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_1 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6((RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D )L_1, /*hidden argument*/NULL);
		NullCheck((FactorySubContainerBinderBase_1_t10122DEBCBD743FE20FF2CCC6889131951876194 *)__this);
		NameTransformConditionCopyNonLazyBinder_t2E9664EE069D9C6CA15EFEDD64FAE2B4125AC36D * L_3 = ((  NameTransformConditionCopyNonLazyBinder_t2E9664EE069D9C6CA15EFEDD64FAE2B4125AC36D * (*) (FactorySubContainerBinderBase_1_t10122DEBCBD743FE20FF2CCC6889131951876194 *, String_t*, Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 15)->methodPointer)((FactorySubContainerBinderBase_1_t10122DEBCBD743FE20FF2CCC6889131951876194 *)__this, (String_t*)L_0, (Type_t *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 15));
		return L_3;
	}
}
// Zenject.NameTransformConditionCopyNonLazyBinder Zenject.FactorySubContainerBinderWithParams`1<System.Object>::ByNewPrefab<System.Object>(UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR NameTransformConditionCopyNonLazyBinder_t2E9664EE069D9C6CA15EFEDD64FAE2B4125AC36D * FactorySubContainerBinderWithParams_1_ByNewPrefab_TisRuntimeObject_mFA81888D8C211CB9EE876AEB08B267FCC8B3A90B_gshared (FactorySubContainerBinderWithParams_1_t45B14E1162744205D8DDBEBD21BD61C7629B12FC * __this, Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___prefab0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FactorySubContainerBinderWithParams_1_ByNewPrefab_TisRuntimeObject_mFA81888D8C211CB9EE876AEB08B267FCC8B3A90B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_0 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6((RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D )L_0, /*hidden argument*/NULL);
		Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * L_2 = ___prefab0;
		NullCheck((FactorySubContainerBinderWithParams_1_t45B14E1162744205D8DDBEBD21BD61C7629B12FC *)__this);
		NameTransformConditionCopyNonLazyBinder_t2E9664EE069D9C6CA15EFEDD64FAE2B4125AC36D * L_3 = ((  NameTransformConditionCopyNonLazyBinder_t2E9664EE069D9C6CA15EFEDD64FAE2B4125AC36D * (*) (FactorySubContainerBinderWithParams_1_t45B14E1162744205D8DDBEBD21BD61C7629B12FC *, Type_t *, Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2)->methodPointer)((FactorySubContainerBinderWithParams_1_t45B14E1162744205D8DDBEBD21BD61C7629B12FC *)__this, (Type_t *)L_1, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2));
		return L_3;
	}
}
// Zenject.NameTransformConditionCopyNonLazyBinder Zenject.FactorySubContainerBinderWithParams`1<System.Object>::ByNewPrefabResource<System.Object>(System.String)
extern "C" IL2CPP_METHOD_ATTR NameTransformConditionCopyNonLazyBinder_t2E9664EE069D9C6CA15EFEDD64FAE2B4125AC36D * FactorySubContainerBinderWithParams_1_ByNewPrefabResource_TisRuntimeObject_m3270E38F2C74F969B54FEFB94768CA95F5E2E17B_gshared (FactorySubContainerBinderWithParams_1_t45B14E1162744205D8DDBEBD21BD61C7629B12FC * __this, String_t* ___resourcePath0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FactorySubContainerBinderWithParams_1_ByNewPrefabResource_TisRuntimeObject_m3270E38F2C74F969B54FEFB94768CA95F5E2E17B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_0 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6((RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D )L_0, /*hidden argument*/NULL);
		String_t* L_2 = ___resourcePath0;
		NullCheck((FactorySubContainerBinderWithParams_1_t45B14E1162744205D8DDBEBD21BD61C7629B12FC *)__this);
		NameTransformConditionCopyNonLazyBinder_t2E9664EE069D9C6CA15EFEDD64FAE2B4125AC36D * L_3 = ((  NameTransformConditionCopyNonLazyBinder_t2E9664EE069D9C6CA15EFEDD64FAE2B4125AC36D * (*) (FactorySubContainerBinderWithParams_1_t45B14E1162744205D8DDBEBD21BD61C7629B12FC *, Type_t *, String_t*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((FactorySubContainerBinderWithParams_1_t45B14E1162744205D8DDBEBD21BD61C7629B12FC *)__this, (Type_t *)L_1, (String_t*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		return L_3;
	}
}
// Zenject.ScopeArgConditionCopyNonLazyBinder Zenject.FromBinder::FromFactoryBase<System.Object,System.Object>()
extern "C" IL2CPP_METHOD_ATTR ScopeArgConditionCopyNonLazyBinder_t218A003451367EEC6A439D63A0F38B964EF07B33 * FromBinder_FromFactoryBase_TisRuntimeObject_TisRuntimeObject_m4B8B3693DD9AA53E12628BB91321A0D1D1DBAF23_gshared (FromBinder_t69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FromBinder_FromFactoryBase_TisRuntimeObject_TisRuntimeObject_m4B8B3693DD9AA53E12628BB91321A0D1D1DBAF23_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_0 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6((RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D )L_0, /*hidden argument*/NULL);
		NullCheck((FromBinder_t69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75 *)__this);
		RuntimeObject* L_2 = FromBinder_get_AllParentTypes_m63693115CEF2E9320ED2C7FC487E65922E5280C3((FromBinder_t69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75 *)__this, /*hidden argument*/NULL);
		BindingUtil_AssertIsDerivedFromTypes_m96464ADDE8B2DC7631EEF597C4A6BD3177F01A0A((Type_t *)L_1, (RuntimeObject*)L_2, /*hidden argument*/NULL);
		NullCheck((NonLazyBinder_tA9FB0768A2F39559FBDDCAFBF940AAD240B7C43C *)__this);
		BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * L_3 = NonLazyBinder_get_BindInfo_mF5E53384F322F5C56719E3DEBDBB4426BE287FC4((NonLazyBinder_tA9FB0768A2F39559FBDDCAFBF940AAD240B7C43C *)__this, /*hidden argument*/NULL);
		NullCheck((BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)L_3);
		BindInfo_set_RequireExplicitScope_mDC1F2FA5156C50519ADEE9662AE7CA900F522DE9((BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)L_3, (bool)0, /*hidden argument*/NULL);
		NullCheck((NonLazyBinder_tA9FB0768A2F39559FBDDCAFBF940AAD240B7C43C *)__this);
		BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * L_4 = NonLazyBinder_get_BindInfo_mF5E53384F322F5C56719E3DEBDBB4426BE287FC4((NonLazyBinder_tA9FB0768A2F39559FBDDCAFBF940AAD240B7C43C *)__this, /*hidden argument*/NULL);
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_5 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 1)) };
		Type_t * L_6 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6((RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D )L_5, /*hidden argument*/NULL);
		Func_3_tF2BBB0DC92895D84974BAECD6D1B2AE7FFC65AAA * L_7 = (Func_3_tF2BBB0DC92895D84974BAECD6D1B2AE7FFC65AAA *)il2cpp_codegen_object_new(Func_3_tF2BBB0DC92895D84974BAECD6D1B2AE7FFC65AAA_il2cpp_TypeInfo_var);
		Func_3__ctor_m0FFFF142D2BDA3D5294EED98E0A00AAD6961E0A0(L_7, (RuntimeObject *)__this, (intptr_t)((intptr_t)IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)), /*hidden argument*/Func_3__ctor_m0FFFF142D2BDA3D5294EED98E0A00AAD6961E0A0_RuntimeMethod_var);
		ScopableBindingFinalizer_t1278F9BE4EE396C7D8B5CA5625C540EC3582632C * L_8 = (ScopableBindingFinalizer_t1278F9BE4EE396C7D8B5CA5625C540EC3582632C *)il2cpp_codegen_object_new(ScopableBindingFinalizer_t1278F9BE4EE396C7D8B5CA5625C540EC3582632C_il2cpp_TypeInfo_var);
		ScopableBindingFinalizer__ctor_m330A27CBA37E6C02F51614042BCE169953861BFF(L_8, (BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)L_4, (int32_t)7, (RuntimeObject *)L_6, (Func_3_tF2BBB0DC92895D84974BAECD6D1B2AE7FFC65AAA *)L_7, /*hidden argument*/NULL);
		NullCheck((FromBinder_t69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75 *)__this);
		FromBinder_set_SubFinalizer_mA7D75C7F3CFC63AB07447CBF5F7B43A584AD7E09((FromBinder_t69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75 *)__this, (RuntimeObject*)L_8, /*hidden argument*/NULL);
		NullCheck((NonLazyBinder_tA9FB0768A2F39559FBDDCAFBF940AAD240B7C43C *)__this);
		BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * L_9 = NonLazyBinder_get_BindInfo_mF5E53384F322F5C56719E3DEBDBB4426BE287FC4((NonLazyBinder_tA9FB0768A2F39559FBDDCAFBF940AAD240B7C43C *)__this, /*hidden argument*/NULL);
		ScopeArgConditionCopyNonLazyBinder_t218A003451367EEC6A439D63A0F38B964EF07B33 * L_10 = (ScopeArgConditionCopyNonLazyBinder_t218A003451367EEC6A439D63A0F38B964EF07B33 *)il2cpp_codegen_object_new(ScopeArgConditionCopyNonLazyBinder_t218A003451367EEC6A439D63A0F38B964EF07B33_il2cpp_TypeInfo_var);
		ScopeArgConditionCopyNonLazyBinder__ctor_m6EA69747BB9E3C8190B7018268FE19F17E5DEDC1(L_10, (BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)L_9, /*hidden argument*/NULL);
		return L_10;
	}
}
// Zenject.ScopeArgConditionCopyNonLazyBinder Zenject.FromBinder::FromMethodBase<System.Object>(System.Func`2<Zenject.InjectContext,TConcrete>)
extern "C" IL2CPP_METHOD_ATTR ScopeArgConditionCopyNonLazyBinder_t218A003451367EEC6A439D63A0F38B964EF07B33 * FromBinder_FromMethodBase_TisRuntimeObject_mDE242188EC651A59753B87DA48AB571C08CD1389_gshared (FromBinder_t69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75 * __this, Func_2_tE24EB23C7D8F4AD0A3A34C5DB2C9F329886F48F2 * ___method0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FromBinder_FromMethodBase_TisRuntimeObject_mDE242188EC651A59753B87DA48AB571C08CD1389_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass35_0_1_tBEC171F34B86D93E7BC341C18B082EE87604E233 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass35_0_1_tBEC171F34B86D93E7BC341C18B082EE87604E233 * L_0 = (U3CU3Ec__DisplayClass35_0_1_tBEC171F34B86D93E7BC341C18B082EE87604E233 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (U3CU3Ec__DisplayClass35_0_1_tBEC171F34B86D93E7BC341C18B082EE87604E233 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		V_0 = (U3CU3Ec__DisplayClass35_0_1_tBEC171F34B86D93E7BC341C18B082EE87604E233 *)L_0;
		U3CU3Ec__DisplayClass35_0_1_tBEC171F34B86D93E7BC341C18B082EE87604E233 * L_1 = V_0;
		Func_2_tE24EB23C7D8F4AD0A3A34C5DB2C9F329886F48F2 * L_2 = ___method0;
		NullCheck(L_1);
		L_1->set_method_0(L_2);
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_3 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 2)) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6((RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D )L_3, /*hidden argument*/NULL);
		NullCheck((FromBinder_t69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75 *)__this);
		RuntimeObject* L_5 = FromBinder_get_AllParentTypes_m63693115CEF2E9320ED2C7FC487E65922E5280C3((FromBinder_t69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75 *)__this, /*hidden argument*/NULL);
		BindingUtil_AssertIsDerivedFromTypes_m96464ADDE8B2DC7631EEF597C4A6BD3177F01A0A((Type_t *)L_4, (RuntimeObject*)L_5, /*hidden argument*/NULL);
		NullCheck((NonLazyBinder_tA9FB0768A2F39559FBDDCAFBF940AAD240B7C43C *)__this);
		BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * L_6 = NonLazyBinder_get_BindInfo_mF5E53384F322F5C56719E3DEBDBB4426BE287FC4((NonLazyBinder_tA9FB0768A2F39559FBDDCAFBF940AAD240B7C43C *)__this, /*hidden argument*/NULL);
		NullCheck((BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)L_6);
		BindInfo_set_RequireExplicitScope_mDC1F2FA5156C50519ADEE9662AE7CA900F522DE9((BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)L_6, (bool)0, /*hidden argument*/NULL);
		NullCheck((NonLazyBinder_tA9FB0768A2F39559FBDDCAFBF940AAD240B7C43C *)__this);
		BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * L_7 = NonLazyBinder_get_BindInfo_mF5E53384F322F5C56719E3DEBDBB4426BE287FC4((NonLazyBinder_tA9FB0768A2F39559FBDDCAFBF940AAD240B7C43C *)__this, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass35_0_1_tBEC171F34B86D93E7BC341C18B082EE87604E233 * L_8 = V_0;
		NullCheck(L_8);
		Func_2_tE24EB23C7D8F4AD0A3A34C5DB2C9F329886F48F2 * L_9 = (Func_2_tE24EB23C7D8F4AD0A3A34C5DB2C9F329886F48F2 *)L_8->get_method_0();
		ToMethod_t12F5E0A4D9C370BB507C42523A4401D55EBE2836 * L_10 = (ToMethod_t12F5E0A4D9C370BB507C42523A4401D55EBE2836 *)il2cpp_codegen_object_new(ToMethod_t12F5E0A4D9C370BB507C42523A4401D55EBE2836_il2cpp_TypeInfo_var);
		ToMethod__ctor_mE467473AC4FB18D55CA26E25EF57FCC5578D9B7A(L_10, (Delegate_t *)L_9, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass35_0_1_tBEC171F34B86D93E7BC341C18B082EE87604E233 * L_11 = V_0;
		Func_3_tF2BBB0DC92895D84974BAECD6D1B2AE7FFC65AAA * L_12 = (Func_3_tF2BBB0DC92895D84974BAECD6D1B2AE7FFC65AAA *)il2cpp_codegen_object_new(Func_3_tF2BBB0DC92895D84974BAECD6D1B2AE7FFC65AAA_il2cpp_TypeInfo_var);
		Func_3__ctor_m0FFFF142D2BDA3D5294EED98E0A00AAD6961E0A0(L_12, (RuntimeObject *)L_11, (intptr_t)((intptr_t)IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3)), /*hidden argument*/Func_3__ctor_m0FFFF142D2BDA3D5294EED98E0A00AAD6961E0A0_RuntimeMethod_var);
		ScopableBindingFinalizer_t1278F9BE4EE396C7D8B5CA5625C540EC3582632C * L_13 = (ScopableBindingFinalizer_t1278F9BE4EE396C7D8B5CA5625C540EC3582632C *)il2cpp_codegen_object_new(ScopableBindingFinalizer_t1278F9BE4EE396C7D8B5CA5625C540EC3582632C_il2cpp_TypeInfo_var);
		ScopableBindingFinalizer__ctor_m330A27CBA37E6C02F51614042BCE169953861BFF(L_13, (BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)L_7, (int32_t)1, (RuntimeObject *)L_10, (Func_3_tF2BBB0DC92895D84974BAECD6D1B2AE7FFC65AAA *)L_12, /*hidden argument*/NULL);
		NullCheck((FromBinder_t69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75 *)__this);
		FromBinder_set_SubFinalizer_mA7D75C7F3CFC63AB07447CBF5F7B43A584AD7E09((FromBinder_t69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75 *)__this, (RuntimeObject*)L_13, /*hidden argument*/NULL);
		return __this;
	}
}
// Zenject.ScopeArgConditionCopyNonLazyBinder Zenject.FromBinder::FromMethodMultipleBase<System.Object>(System.Func`2<Zenject.InjectContext,System.Collections.Generic.IEnumerable`1<TConcrete>>)
extern "C" IL2CPP_METHOD_ATTR ScopeArgConditionCopyNonLazyBinder_t218A003451367EEC6A439D63A0F38B964EF07B33 * FromBinder_FromMethodMultipleBase_TisRuntimeObject_mA1F7B7CD47342E6DD5EC125DBFCD696CE92679C7_gshared (FromBinder_t69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75 * __this, Func_2_tEB18CC35489403B1DD34E84D07D6EBE38C06F980 * ___method0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FromBinder_FromMethodMultipleBase_TisRuntimeObject_mA1F7B7CD47342E6DD5EC125DBFCD696CE92679C7_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass36_0_1_t02B1D9C112A722BCD5BC4A3CA54F5C5B13A17266 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass36_0_1_t02B1D9C112A722BCD5BC4A3CA54F5C5B13A17266 * L_0 = (U3CU3Ec__DisplayClass36_0_1_t02B1D9C112A722BCD5BC4A3CA54F5C5B13A17266 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (U3CU3Ec__DisplayClass36_0_1_t02B1D9C112A722BCD5BC4A3CA54F5C5B13A17266 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		V_0 = (U3CU3Ec__DisplayClass36_0_1_t02B1D9C112A722BCD5BC4A3CA54F5C5B13A17266 *)L_0;
		U3CU3Ec__DisplayClass36_0_1_t02B1D9C112A722BCD5BC4A3CA54F5C5B13A17266 * L_1 = V_0;
		Func_2_tEB18CC35489403B1DD34E84D07D6EBE38C06F980 * L_2 = ___method0;
		NullCheck(L_1);
		L_1->set_method_0(L_2);
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_3 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 2)) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6((RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D )L_3, /*hidden argument*/NULL);
		NullCheck((FromBinder_t69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75 *)__this);
		RuntimeObject* L_5 = FromBinder_get_AllParentTypes_m63693115CEF2E9320ED2C7FC487E65922E5280C3((FromBinder_t69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75 *)__this, /*hidden argument*/NULL);
		BindingUtil_AssertIsDerivedFromTypes_m96464ADDE8B2DC7631EEF597C4A6BD3177F01A0A((Type_t *)L_4, (RuntimeObject*)L_5, /*hidden argument*/NULL);
		NullCheck((NonLazyBinder_tA9FB0768A2F39559FBDDCAFBF940AAD240B7C43C *)__this);
		BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * L_6 = NonLazyBinder_get_BindInfo_mF5E53384F322F5C56719E3DEBDBB4426BE287FC4((NonLazyBinder_tA9FB0768A2F39559FBDDCAFBF940AAD240B7C43C *)__this, /*hidden argument*/NULL);
		NullCheck((BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)L_6);
		BindInfo_set_RequireExplicitScope_mDC1F2FA5156C50519ADEE9662AE7CA900F522DE9((BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)L_6, (bool)0, /*hidden argument*/NULL);
		NullCheck((NonLazyBinder_tA9FB0768A2F39559FBDDCAFBF940AAD240B7C43C *)__this);
		BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * L_7 = NonLazyBinder_get_BindInfo_mF5E53384F322F5C56719E3DEBDBB4426BE287FC4((NonLazyBinder_tA9FB0768A2F39559FBDDCAFBF940AAD240B7C43C *)__this, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass36_0_1_t02B1D9C112A722BCD5BC4A3CA54F5C5B13A17266 * L_8 = V_0;
		NullCheck(L_8);
		Func_2_tEB18CC35489403B1DD34E84D07D6EBE38C06F980 * L_9 = (Func_2_tEB18CC35489403B1DD34E84D07D6EBE38C06F980 *)L_8->get_method_0();
		ToMethod_t12F5E0A4D9C370BB507C42523A4401D55EBE2836 * L_10 = (ToMethod_t12F5E0A4D9C370BB507C42523A4401D55EBE2836 *)il2cpp_codegen_object_new(ToMethod_t12F5E0A4D9C370BB507C42523A4401D55EBE2836_il2cpp_TypeInfo_var);
		ToMethod__ctor_mE467473AC4FB18D55CA26E25EF57FCC5578D9B7A(L_10, (Delegate_t *)L_9, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass36_0_1_t02B1D9C112A722BCD5BC4A3CA54F5C5B13A17266 * L_11 = V_0;
		Func_3_tF2BBB0DC92895D84974BAECD6D1B2AE7FFC65AAA * L_12 = (Func_3_tF2BBB0DC92895D84974BAECD6D1B2AE7FFC65AAA *)il2cpp_codegen_object_new(Func_3_tF2BBB0DC92895D84974BAECD6D1B2AE7FFC65AAA_il2cpp_TypeInfo_var);
		Func_3__ctor_m0FFFF142D2BDA3D5294EED98E0A00AAD6961E0A0(L_12, (RuntimeObject *)L_11, (intptr_t)((intptr_t)IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3)), /*hidden argument*/Func_3__ctor_m0FFFF142D2BDA3D5294EED98E0A00AAD6961E0A0_RuntimeMethod_var);
		ScopableBindingFinalizer_t1278F9BE4EE396C7D8B5CA5625C540EC3582632C * L_13 = (ScopableBindingFinalizer_t1278F9BE4EE396C7D8B5CA5625C540EC3582632C *)il2cpp_codegen_object_new(ScopableBindingFinalizer_t1278F9BE4EE396C7D8B5CA5625C540EC3582632C_il2cpp_TypeInfo_var);
		ScopableBindingFinalizer__ctor_m330A27CBA37E6C02F51614042BCE169953861BFF(L_13, (BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)L_7, (int32_t)1, (RuntimeObject *)L_10, (Func_3_tF2BBB0DC92895D84974BAECD6D1B2AE7FFC65AAA *)L_12, /*hidden argument*/NULL);
		NullCheck((FromBinder_t69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75 *)__this);
		FromBinder_set_SubFinalizer_mA7D75C7F3CFC63AB07447CBF5F7B43A584AD7E09((FromBinder_t69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75 *)__this, (RuntimeObject*)L_13, /*hidden argument*/NULL);
		return __this;
	}
}
// Zenject.ScopeArgConditionCopyNonLazyBinder Zenject.FromBinderGeneric`1<System.Object>::FromFactory<System.Object>()
extern "C" IL2CPP_METHOD_ATTR ScopeArgConditionCopyNonLazyBinder_t218A003451367EEC6A439D63A0F38B964EF07B33 * FromBinderGeneric_1_FromFactory_TisRuntimeObject_m6A0A64AE1332AC3655B33D35C34E3636032EE175_gshared (FromBinderGeneric_1_tB3D3D4B24691B0AFF684D39C08F63B2193327187 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((FromBinder_t69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75 *)__this);
		ScopeArgConditionCopyNonLazyBinder_t218A003451367EEC6A439D63A0F38B964EF07B33 * L_0 = ((  ScopeArgConditionCopyNonLazyBinder_t218A003451367EEC6A439D63A0F38B964EF07B33 * (*) (FromBinder_t69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((FromBinder_t69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_0;
	}
}
// Zenject.ScopeArgConditionCopyNonLazyBinder Zenject.FromBinderNonGeneric::FromFactory<System.Object,System.Object>()
extern "C" IL2CPP_METHOD_ATTR ScopeArgConditionCopyNonLazyBinder_t218A003451367EEC6A439D63A0F38B964EF07B33 * FromBinderNonGeneric_FromFactory_TisRuntimeObject_TisRuntimeObject_m8196FB0D7B33EFC0C56B460BF11AC15E3C04B2B9_gshared (FromBinderNonGeneric_t677B667DE367A055191AC7B64A622B8495C6CF89 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((FromBinder_t69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75 *)__this);
		ScopeArgConditionCopyNonLazyBinder_t218A003451367EEC6A439D63A0F38B964EF07B33 * L_0 = ((  ScopeArgConditionCopyNonLazyBinder_t218A003451367EEC6A439D63A0F38B964EF07B33 * (*) (FromBinder_t69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((FromBinder_t69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_0;
	}
}
// Zenject.ScopeArgConditionCopyNonLazyBinder Zenject.FromBinderNonGeneric::FromMethod<System.Object>(System.Func`2<Zenject.InjectContext,TConcrete>)
extern "C" IL2CPP_METHOD_ATTR ScopeArgConditionCopyNonLazyBinder_t218A003451367EEC6A439D63A0F38B964EF07B33 * FromBinderNonGeneric_FromMethod_TisRuntimeObject_m252BE8FD5B8E50F61EAC14816559794B4AFEDC9B_gshared (FromBinderNonGeneric_t677B667DE367A055191AC7B64A622B8495C6CF89 * __this, Func_2_tE24EB23C7D8F4AD0A3A34C5DB2C9F329886F48F2 * ___method0, const RuntimeMethod* method)
{
	{
		Func_2_tE24EB23C7D8F4AD0A3A34C5DB2C9F329886F48F2 * L_0 = ___method0;
		NullCheck((FromBinder_t69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75 *)__this);
		ScopeArgConditionCopyNonLazyBinder_t218A003451367EEC6A439D63A0F38B964EF07B33 * L_1 = ((  ScopeArgConditionCopyNonLazyBinder_t218A003451367EEC6A439D63A0F38B964EF07B33 * (*) (FromBinder_t69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75 *, Func_2_tE24EB23C7D8F4AD0A3A34C5DB2C9F329886F48F2 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((FromBinder_t69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75 *)__this, (Func_2_tE24EB23C7D8F4AD0A3A34C5DB2C9F329886F48F2 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_1;
	}
}
// Zenject.ScopeArgConditionCopyNonLazyBinder Zenject.FromBinderNonGeneric::FromMethodMultiple<System.Object>(System.Func`2<Zenject.InjectContext,System.Collections.Generic.IEnumerable`1<TConcrete>>)
extern "C" IL2CPP_METHOD_ATTR ScopeArgConditionCopyNonLazyBinder_t218A003451367EEC6A439D63A0F38B964EF07B33 * FromBinderNonGeneric_FromMethodMultiple_TisRuntimeObject_m5D2B8A9F987AFB7A33F1EA4C3291E6733E92B2C9_gshared (FromBinderNonGeneric_t677B667DE367A055191AC7B64A622B8495C6CF89 * __this, Func_2_tEB18CC35489403B1DD34E84D07D6EBE38C06F980 * ___method0, const RuntimeMethod* method)
{
	{
		Func_2_tEB18CC35489403B1DD34E84D07D6EBE38C06F980 * L_0 = ___method0;
		NullCheck((FromBinder_t69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75 *)__this);
		ScopeArgConditionCopyNonLazyBinder_t218A003451367EEC6A439D63A0F38B964EF07B33 * L_1 = ((  ScopeArgConditionCopyNonLazyBinder_t218A003451367EEC6A439D63A0F38B964EF07B33 * (*) (FromBinder_t69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75 *, Func_2_tEB18CC35489403B1DD34E84D07D6EBE38C06F980 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((FromBinder_t69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75 *)__this, (Func_2_tEB18CC35489403B1DD34E84D07D6EBE38C06F980 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_1;
	}
}
// Zenject.ScopeArgNonLazyBinder Zenject.SignalFromBinder`1<System.Object>::FromFactory<System.Object>()
extern "C" IL2CPP_METHOD_ATTR ScopeArgNonLazyBinder_t1D85A2EED80F3DAE72346683D48CCCEAB4738BA6 * SignalFromBinder_1_FromFactory_TisRuntimeObject_m277A415C156592E715BEE60DEA70482C480ECE11_gshared (SignalFromBinder_1_t862D9073BD2CB93CFFF7F75A6F7262CDF34AA1D7 * __this, const RuntimeMethod* method)
{
	{
		FromBinderGeneric_1_tB3D3D4B24691B0AFF684D39C08F63B2193327187 * L_0 = (FromBinderGeneric_1_tB3D3D4B24691B0AFF684D39C08F63B2193327187 *)__this->get__subBinder_2();
		NullCheck((FromBinderGeneric_1_tB3D3D4B24691B0AFF684D39C08F63B2193327187 *)L_0);
		((  ScopeArgConditionCopyNonLazyBinder_t218A003451367EEC6A439D63A0F38B964EF07B33 * (*) (FromBinderGeneric_1_tB3D3D4B24691B0AFF684D39C08F63B2193327187 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((FromBinderGeneric_1_tB3D3D4B24691B0AFF684D39C08F63B2193327187 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return __this;
	}
}
// Zenject.ScopeConditionCopyNonLazyBinder Zenject.FromBinder::FromResolveGetterBase<System.Object,System.Object>(System.Object,System.Func`2<TObj,TResult>)
extern "C" IL2CPP_METHOD_ATTR ScopeConditionCopyNonLazyBinder_t6221939C8173223799835D9DF0944D6D777C3EC6 * FromBinder_FromResolveGetterBase_TisRuntimeObject_TisRuntimeObject_m35AE38CDA9C722CFA39C31625CA0803CF2738E8F_gshared (FromBinder_t69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75 * __this, RuntimeObject * ___identifier0, Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * ___method1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FromBinder_FromResolveGetterBase_TisRuntimeObject_TisRuntimeObject_m35AE38CDA9C722CFA39C31625CA0803CF2738E8F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass38_0_2_tF5C66905CC804ED68CBE4C8EDF02C23BC57797A6 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass38_0_2_tF5C66905CC804ED68CBE4C8EDF02C23BC57797A6 * L_0 = (U3CU3Ec__DisplayClass38_0_2_tF5C66905CC804ED68CBE4C8EDF02C23BC57797A6 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (U3CU3Ec__DisplayClass38_0_2_tF5C66905CC804ED68CBE4C8EDF02C23BC57797A6 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		V_0 = (U3CU3Ec__DisplayClass38_0_2_tF5C66905CC804ED68CBE4C8EDF02C23BC57797A6 *)L_0;
		U3CU3Ec__DisplayClass38_0_2_tF5C66905CC804ED68CBE4C8EDF02C23BC57797A6 * L_1 = V_0;
		RuntimeObject * L_2 = ___identifier0;
		NullCheck(L_1);
		L_1->set_identifier_0(L_2);
		U3CU3Ec__DisplayClass38_0_2_tF5C66905CC804ED68CBE4C8EDF02C23BC57797A6 * L_3 = V_0;
		Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * L_4 = ___method1;
		NullCheck(L_3);
		L_3->set_method_1(L_4);
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_5 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 2)) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_6 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6((RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D )L_5, /*hidden argument*/NULL);
		NullCheck((FromBinder_t69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75 *)__this);
		RuntimeObject* L_7 = FromBinder_get_AllParentTypes_m63693115CEF2E9320ED2C7FC487E65922E5280C3((FromBinder_t69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75 *)__this, /*hidden argument*/NULL);
		BindingUtil_AssertIsDerivedFromTypes_m96464ADDE8B2DC7631EEF597C4A6BD3177F01A0A((Type_t *)L_6, (RuntimeObject*)L_7, /*hidden argument*/NULL);
		NullCheck((NonLazyBinder_tA9FB0768A2F39559FBDDCAFBF940AAD240B7C43C *)__this);
		BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * L_8 = NonLazyBinder_get_BindInfo_mF5E53384F322F5C56719E3DEBDBB4426BE287FC4((NonLazyBinder_tA9FB0768A2F39559FBDDCAFBF940AAD240B7C43C *)__this, /*hidden argument*/NULL);
		NullCheck((BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)L_8);
		BindInfo_set_RequireExplicitScope_mDC1F2FA5156C50519ADEE9662AE7CA900F522DE9((BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)L_8, (bool)0, /*hidden argument*/NULL);
		NullCheck((NonLazyBinder_tA9FB0768A2F39559FBDDCAFBF940AAD240B7C43C *)__this);
		BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * L_9 = NonLazyBinder_get_BindInfo_mF5E53384F322F5C56719E3DEBDBB4426BE287FC4((NonLazyBinder_tA9FB0768A2F39559FBDDCAFBF940AAD240B7C43C *)__this, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass38_0_2_tF5C66905CC804ED68CBE4C8EDF02C23BC57797A6 * L_10 = V_0;
		NullCheck(L_10);
		RuntimeObject * L_11 = (RuntimeObject *)L_10->get_identifier_0();
		U3CU3Ec__DisplayClass38_0_2_tF5C66905CC804ED68CBE4C8EDF02C23BC57797A6 * L_12 = V_0;
		NullCheck(L_12);
		Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * L_13 = (Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 *)L_12->get_method_1();
		ToGetter_t269C4EED29F0AC2A1C9FB4061EA228DC2DDADED4 * L_14 = (ToGetter_t269C4EED29F0AC2A1C9FB4061EA228DC2DDADED4 *)il2cpp_codegen_object_new(ToGetter_t269C4EED29F0AC2A1C9FB4061EA228DC2DDADED4_il2cpp_TypeInfo_var);
		ToGetter__ctor_m60F6511830872820A5BEBA4561C5F91A435D6BF8(L_14, (RuntimeObject *)L_11, (Delegate_t *)L_13, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass38_0_2_tF5C66905CC804ED68CBE4C8EDF02C23BC57797A6 * L_15 = V_0;
		Func_3_tF2BBB0DC92895D84974BAECD6D1B2AE7FFC65AAA * L_16 = (Func_3_tF2BBB0DC92895D84974BAECD6D1B2AE7FFC65AAA *)il2cpp_codegen_object_new(Func_3_tF2BBB0DC92895D84974BAECD6D1B2AE7FFC65AAA_il2cpp_TypeInfo_var);
		Func_3__ctor_m0FFFF142D2BDA3D5294EED98E0A00AAD6961E0A0(L_16, (RuntimeObject *)L_15, (intptr_t)((intptr_t)IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3)), /*hidden argument*/Func_3__ctor_m0FFFF142D2BDA3D5294EED98E0A00AAD6961E0A0_RuntimeMethod_var);
		ScopableBindingFinalizer_t1278F9BE4EE396C7D8B5CA5625C540EC3582632C * L_17 = (ScopableBindingFinalizer_t1278F9BE4EE396C7D8B5CA5625C540EC3582632C *)il2cpp_codegen_object_new(ScopableBindingFinalizer_t1278F9BE4EE396C7D8B5CA5625C540EC3582632C_il2cpp_TypeInfo_var);
		ScopableBindingFinalizer__ctor_m330A27CBA37E6C02F51614042BCE169953861BFF(L_17, (BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)L_9, (int32_t)((int32_t)11), (RuntimeObject *)L_14, (Func_3_tF2BBB0DC92895D84974BAECD6D1B2AE7FFC65AAA *)L_16, /*hidden argument*/NULL);
		NullCheck((FromBinder_t69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75 *)__this);
		FromBinder_set_SubFinalizer_mA7D75C7F3CFC63AB07447CBF5F7B43A584AD7E09((FromBinder_t69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75 *)__this, (RuntimeObject*)L_17, /*hidden argument*/NULL);
		NullCheck((NonLazyBinder_tA9FB0768A2F39559FBDDCAFBF940AAD240B7C43C *)__this);
		BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * L_18 = NonLazyBinder_get_BindInfo_mF5E53384F322F5C56719E3DEBDBB4426BE287FC4((NonLazyBinder_tA9FB0768A2F39559FBDDCAFBF940AAD240B7C43C *)__this, /*hidden argument*/NULL);
		ScopeConditionCopyNonLazyBinder_t6221939C8173223799835D9DF0944D6D777C3EC6 * L_19 = (ScopeConditionCopyNonLazyBinder_t6221939C8173223799835D9DF0944D6D777C3EC6 *)il2cpp_codegen_object_new(ScopeConditionCopyNonLazyBinder_t6221939C8173223799835D9DF0944D6D777C3EC6_il2cpp_TypeInfo_var);
		ScopeConditionCopyNonLazyBinder__ctor_m35C93A0113B188F814DAC675B4DB6D089A4B9342(L_19, (BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)L_18, /*hidden argument*/NULL);
		return L_19;
	}
}
// Zenject.ScopeConditionCopyNonLazyBinder Zenject.FromBinderGeneric`1<System.Object>::FromResolveGetter<System.Object>(System.Func`2<TObj,TContract>)
extern "C" IL2CPP_METHOD_ATTR ScopeConditionCopyNonLazyBinder_t6221939C8173223799835D9DF0944D6D777C3EC6 * FromBinderGeneric_1_FromResolveGetter_TisRuntimeObject_m03FA31C3E65EC992969D31F739B2A26DBFCE9041_gshared (FromBinderGeneric_1_tB3D3D4B24691B0AFF684D39C08F63B2193327187 * __this, Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * ___method0, const RuntimeMethod* method)
{
	{
		Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * L_0 = ___method0;
		NullCheck((FromBinderGeneric_1_tB3D3D4B24691B0AFF684D39C08F63B2193327187 *)__this);
		ScopeConditionCopyNonLazyBinder_t6221939C8173223799835D9DF0944D6D777C3EC6 * L_1 = ((  ScopeConditionCopyNonLazyBinder_t6221939C8173223799835D9DF0944D6D777C3EC6 * (*) (FromBinderGeneric_1_tB3D3D4B24691B0AFF684D39C08F63B2193327187 *, RuntimeObject *, Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((FromBinderGeneric_1_tB3D3D4B24691B0AFF684D39C08F63B2193327187 *)__this, (RuntimeObject *)NULL, (Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_1;
	}
}
// Zenject.ScopeConditionCopyNonLazyBinder Zenject.FromBinderGeneric`1<System.Object>::FromResolveGetter<System.Object>(System.Object,System.Func`2<TObj,TContract>)
extern "C" IL2CPP_METHOD_ATTR ScopeConditionCopyNonLazyBinder_t6221939C8173223799835D9DF0944D6D777C3EC6 * FromBinderGeneric_1_FromResolveGetter_TisRuntimeObject_mE78B4196447D7F91ADD2A9031D9315BACF56BFDD_gshared (FromBinderGeneric_1_tB3D3D4B24691B0AFF684D39C08F63B2193327187 * __this, RuntimeObject * ___identifier0, Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * ___method1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___identifier0;
		Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * L_1 = ___method1;
		NullCheck((FromBinder_t69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75 *)__this);
		ScopeConditionCopyNonLazyBinder_t6221939C8173223799835D9DF0944D6D777C3EC6 * L_2 = ((  ScopeConditionCopyNonLazyBinder_t6221939C8173223799835D9DF0944D6D777C3EC6 * (*) (FromBinder_t69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75 *, RuntimeObject *, Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((FromBinder_t69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75 *)__this, (RuntimeObject *)L_0, (Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_2;
	}
}
// Zenject.ScopeConditionCopyNonLazyBinder Zenject.FromBinderNonGeneric::FromResolveGetter<System.Object,System.Object>(System.Func`2<TObj,TContract>)
extern "C" IL2CPP_METHOD_ATTR ScopeConditionCopyNonLazyBinder_t6221939C8173223799835D9DF0944D6D777C3EC6 * FromBinderNonGeneric_FromResolveGetter_TisRuntimeObject_TisRuntimeObject_m56577DE58E01FDECC56F078947622843ECD91068_gshared (FromBinderNonGeneric_t677B667DE367A055191AC7B64A622B8495C6CF89 * __this, Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * ___method0, const RuntimeMethod* method)
{
	{
		Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * L_0 = ___method0;
		NullCheck((FromBinderNonGeneric_t677B667DE367A055191AC7B64A622B8495C6CF89 *)__this);
		ScopeConditionCopyNonLazyBinder_t6221939C8173223799835D9DF0944D6D777C3EC6 * L_1 = ((  ScopeConditionCopyNonLazyBinder_t6221939C8173223799835D9DF0944D6D777C3EC6 * (*) (FromBinderNonGeneric_t677B667DE367A055191AC7B64A622B8495C6CF89 *, RuntimeObject *, Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((FromBinderNonGeneric_t677B667DE367A055191AC7B64A622B8495C6CF89 *)__this, (RuntimeObject *)NULL, (Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_1;
	}
}
// Zenject.ScopeConditionCopyNonLazyBinder Zenject.FromBinderNonGeneric::FromResolveGetter<System.Object,System.Object>(System.Object,System.Func`2<TObj,TContract>)
extern "C" IL2CPP_METHOD_ATTR ScopeConditionCopyNonLazyBinder_t6221939C8173223799835D9DF0944D6D777C3EC6 * FromBinderNonGeneric_FromResolveGetter_TisRuntimeObject_TisRuntimeObject_m147D21C83B99AD14C33B5AEC4C6D92395EA37893_gshared (FromBinderNonGeneric_t677B667DE367A055191AC7B64A622B8495C6CF89 * __this, RuntimeObject * ___identifier0, Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * ___method1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___identifier0;
		Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * L_1 = ___method1;
		NullCheck((FromBinder_t69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75 *)__this);
		ScopeConditionCopyNonLazyBinder_t6221939C8173223799835D9DF0944D6D777C3EC6 * L_2 = ((  ScopeConditionCopyNonLazyBinder_t6221939C8173223799835D9DF0944D6D777C3EC6 * (*) (FromBinder_t69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75 *, RuntimeObject *, Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((FromBinder_t69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75 *)__this, (RuntimeObject *)L_0, (Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_2;
	}
}
// Zenject.ScopeConditionCopyNonLazyBinder Zenject.SubContainerBinder::ByInstaller<System.Object>()
extern "C" IL2CPP_METHOD_ATTR ScopeConditionCopyNonLazyBinder_t6221939C8173223799835D9DF0944D6D777C3EC6 * SubContainerBinder_ByInstaller_TisRuntimeObject_m9E70B6975FC0797BBD2981ED12509D7974444A16_gshared (SubContainerBinder_t24411FF88DADEE6DFBC16EE722E0F4944E8732D6 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SubContainerBinder_ByInstaller_TisRuntimeObject_m9E70B6975FC0797BBD2981ED12509D7974444A16_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_0 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6((RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D )L_0, /*hidden argument*/NULL);
		NullCheck((SubContainerBinder_t24411FF88DADEE6DFBC16EE722E0F4944E8732D6 *)__this);
		ScopeConditionCopyNonLazyBinder_t6221939C8173223799835D9DF0944D6D777C3EC6 * L_2 = SubContainerBinder_ByInstaller_m59CFC60CD64FF80F0FE96B542484A7F034BF62FA((SubContainerBinder_t24411FF88DADEE6DFBC16EE722E0F4944E8732D6 *)__this, (Type_t *)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// Zenject.ScopeNonLazyBinder Zenject.SignalFromBinder`1<System.Object>::FromResolveGetter<System.Object>(System.Func`2<TObj,TContract>)
extern "C" IL2CPP_METHOD_ATTR ScopeNonLazyBinder_t7BC7AB773CA2EAECC76C3B733A1271EE6866646A * SignalFromBinder_1_FromResolveGetter_TisRuntimeObject_m2F3F2A4FB39B45822A6D05915E40DC24F9969BD3_gshared (SignalFromBinder_1_t862D9073BD2CB93CFFF7F75A6F7262CDF34AA1D7 * __this, Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * ___method0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SignalFromBinder_1_FromResolveGetter_TisRuntimeObject_m2F3F2A4FB39B45822A6D05915E40DC24F9969BD3_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		FromBinderGeneric_1_tB3D3D4B24691B0AFF684D39C08F63B2193327187 * L_0 = (FromBinderGeneric_1_tB3D3D4B24691B0AFF684D39C08F63B2193327187 *)__this->get__subBinder_2();
		Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * L_1 = ___method0;
		NullCheck((FromBinderGeneric_1_tB3D3D4B24691B0AFF684D39C08F63B2193327187 *)L_0);
		((  ScopeConditionCopyNonLazyBinder_t6221939C8173223799835D9DF0944D6D777C3EC6 * (*) (FromBinderGeneric_1_tB3D3D4B24691B0AFF684D39C08F63B2193327187 *, Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((FromBinderGeneric_1_tB3D3D4B24691B0AFF684D39C08F63B2193327187 *)L_0, (Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * L_2 = (BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)__this->get__info_1();
		ScopeNonLazyBinder_t7BC7AB773CA2EAECC76C3B733A1271EE6866646A * L_3 = (ScopeNonLazyBinder_t7BC7AB773CA2EAECC76C3B733A1271EE6866646A *)il2cpp_codegen_object_new(ScopeNonLazyBinder_t7BC7AB773CA2EAECC76C3B733A1271EE6866646A_il2cpp_TypeInfo_var);
		ScopeNonLazyBinder__ctor_m37680839EDF90FF98C733A21C48D95CFAF1A30CB(L_3, (BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// Zenject.ScopeNonLazyBinder Zenject.SignalFromBinder`1<System.Object>::FromResolveGetter<System.Object>(System.Object,System.Func`2<TObj,TContract>)
extern "C" IL2CPP_METHOD_ATTR ScopeNonLazyBinder_t7BC7AB773CA2EAECC76C3B733A1271EE6866646A * SignalFromBinder_1_FromResolveGetter_TisRuntimeObject_m9D8D6C91ACCB9CB6884007A448EBA113E8FAB046_gshared (SignalFromBinder_1_t862D9073BD2CB93CFFF7F75A6F7262CDF34AA1D7 * __this, RuntimeObject * ___identifier0, Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * ___method1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SignalFromBinder_1_FromResolveGetter_TisRuntimeObject_m9D8D6C91ACCB9CB6884007A448EBA113E8FAB046_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		FromBinderGeneric_1_tB3D3D4B24691B0AFF684D39C08F63B2193327187 * L_0 = (FromBinderGeneric_1_tB3D3D4B24691B0AFF684D39C08F63B2193327187 *)__this->get__subBinder_2();
		RuntimeObject * L_1 = ___identifier0;
		Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * L_2 = ___method1;
		NullCheck((FromBinderGeneric_1_tB3D3D4B24691B0AFF684D39C08F63B2193327187 *)L_0);
		((  ScopeConditionCopyNonLazyBinder_t6221939C8173223799835D9DF0944D6D777C3EC6 * (*) (FromBinderGeneric_1_tB3D3D4B24691B0AFF684D39C08F63B2193327187 *, RuntimeObject *, Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((FromBinderGeneric_1_tB3D3D4B24691B0AFF684D39C08F63B2193327187 *)L_0, (RuntimeObject *)L_1, (Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * L_3 = (BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)__this->get__info_1();
		ScopeNonLazyBinder_t7BC7AB773CA2EAECC76C3B733A1271EE6866646A * L_4 = (ScopeNonLazyBinder_t7BC7AB773CA2EAECC76C3B733A1271EE6866646A *)il2cpp_codegen_object_new(ScopeNonLazyBinder_t7BC7AB773CA2EAECC76C3B733A1271EE6866646A_il2cpp_TypeInfo_var);
		ScopeNonLazyBinder__ctor_m37680839EDF90FF98C733A21C48D95CFAF1A30CB(L_4, (BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// Zenject.SignalBinderWithId Zenject.SignalExtensions::DeclareSignal<System.Object>(Zenject.DiContainer)
extern "C" IL2CPP_METHOD_ATTR SignalBinderWithId_tF62257893A9AA47A533D47DC5DB0C0C046F67B81 * SignalExtensions_DeclareSignal_TisRuntimeObject_m6595FFCE35DCB74F393F0A2563E095355776E0F1_gshared (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ___container0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SignalExtensions_DeclareSignal_TisRuntimeObject_m6595FFCE35DCB74F393F0A2563E095355776E0F1_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * V_0 = NULL;
	SignalSettings_tD753AA3D16CEA36F97CDAEC5428C5217F1EB6540 * V_1 = NULL;
	{
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_0 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6((RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D )L_0, /*hidden argument*/NULL);
		BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * L_2 = (BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)il2cpp_codegen_object_new(BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991_il2cpp_TypeInfo_var);
		BindInfo__ctor_m0653A2A5799A7A231939E22C2E728AFA4B006B6D(L_2, (Type_t *)L_1, /*hidden argument*/NULL);
		V_0 = (BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)L_2;
		SignalSettings_tD753AA3D16CEA36F97CDAEC5428C5217F1EB6540 * L_3 = (SignalSettings_tD753AA3D16CEA36F97CDAEC5428C5217F1EB6540 *)il2cpp_codegen_object_new(SignalSettings_tD753AA3D16CEA36F97CDAEC5428C5217F1EB6540_il2cpp_TypeInfo_var);
		SignalSettings__ctor_m832E7FDF44930C19D612D5996D4BDAC71723F1E3(L_3, /*hidden argument*/NULL);
		V_1 = (SignalSettings_tD753AA3D16CEA36F97CDAEC5428C5217F1EB6540 *)L_3;
		DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * L_4 = ___container0;
		BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * L_5 = V_0;
		NullCheck((DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)L_4);
		ConcreteIdBinderGeneric_1_tAD404DF3AD6E1FDBC78FD89CAC0C982BC2B642A7 * L_6 = ((  ConcreteIdBinderGeneric_1_tAD404DF3AD6E1FDBC78FD89CAC0C982BC2B642A7 * (*) (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *, BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)((DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)L_4, (BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		NullCheck((ScopeArgConditionCopyNonLazyBinder_t218A003451367EEC6A439D63A0F38B964EF07B33 *)L_6);
		ArgConditionCopyNonLazyBinder_t5F1B4361E0805E20DDBEDEBC61B90CFFBB68B21B * L_7 = ScopeArgConditionCopyNonLazyBinder_AsCached_mAAD80E3CF24D5B46128725D1045BCA847A1205D2((ScopeArgConditionCopyNonLazyBinder_t218A003451367EEC6A439D63A0F38B964EF07B33 *)L_6, /*hidden argument*/NULL);
		SignalSettings_tD753AA3D16CEA36F97CDAEC5428C5217F1EB6540 * L_8 = V_1;
		BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * L_9 = V_0;
		NullCheck((ArgConditionCopyNonLazyBinder_t5F1B4361E0805E20DDBEDEBC61B90CFFBB68B21B *)L_7);
		ArgConditionCopyNonLazyBinder_WithArguments_TisSignalSettings_tD753AA3D16CEA36F97CDAEC5428C5217F1EB6540_TisBindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991_mD9A3095921FC1EFD57D2AD25CFAA64C2A5C56FF8((ArgConditionCopyNonLazyBinder_t5F1B4361E0805E20DDBEDEBC61B90CFFBB68B21B *)L_7, (SignalSettings_tD753AA3D16CEA36F97CDAEC5428C5217F1EB6540 *)L_8, (BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)L_9, /*hidden argument*/ArgConditionCopyNonLazyBinder_WithArguments_TisSignalSettings_tD753AA3D16CEA36F97CDAEC5428C5217F1EB6540_TisBindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991_mD9A3095921FC1EFD57D2AD25CFAA64C2A5C56FF8_RuntimeMethod_var);
		BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * L_10 = V_0;
		SignalSettings_tD753AA3D16CEA36F97CDAEC5428C5217F1EB6540 * L_11 = V_1;
		SignalBinderWithId_tF62257893A9AA47A533D47DC5DB0C0C046F67B81 * L_12 = (SignalBinderWithId_tF62257893A9AA47A533D47DC5DB0C0C046F67B81 *)il2cpp_codegen_object_new(SignalBinderWithId_tF62257893A9AA47A533D47DC5DB0C0C046F67B81_il2cpp_TypeInfo_var);
		SignalBinderWithId__ctor_mC60EC8985D8B8AD5881C02CE95B0A8D992DF63F6(L_12, (BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)L_10, (SignalSettings_tD753AA3D16CEA36F97CDAEC5428C5217F1EB6540 *)L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
// Zenject.SignalFromBinder`1<THandler> Zenject.SignalHandlerBinder::To<System.Object>(System.Action`1<THandler>)
extern "C" IL2CPP_METHOD_ATTR SignalFromBinder_1_t862D9073BD2CB93CFFF7F75A6F7262CDF34AA1D7 * SignalHandlerBinder_To_TisRuntimeObject_m848A03675EF5E582AA51AFF1DE8080E1F1EEF68B_gshared (SignalHandlerBinder_t4AB70F7A7BD1596D46B88F834A400EBEDE185771 * __this, Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * ___method0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SignalHandlerBinder_To_TisRuntimeObject_m848A03675EF5E582AA51AFF1DE8080E1F1EEF68B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Guid_t  V_0;
	memset(&V_0, 0, sizeof(V_0));
	BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * V_1 = NULL;
	{
		BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 * L_0 = (BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 *)__this->get__finalizerWrapper_0();
		NullBindingFinalizer_tFEAF0122BA4AF8A5209DE0330F0BFF444F06AD65 * L_1 = (NullBindingFinalizer_tFEAF0122BA4AF8A5209DE0330F0BFF444F06AD65 *)il2cpp_codegen_object_new(NullBindingFinalizer_tFEAF0122BA4AF8A5209DE0330F0BFF444F06AD65_il2cpp_TypeInfo_var);
		NullBindingFinalizer__ctor_m2B90434471F862EF8E0C197230BDB1EAB586834A(L_1, /*hidden argument*/NULL);
		NullCheck((BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 *)L_0);
		BindFinalizerWrapper_set_SubFinalizer_m6AF16F9C3D2BE2F006E60D76D85F20EFE8C76874((BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 *)L_0, (RuntimeObject*)L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Guid_t_il2cpp_TypeInfo_var);
		Guid_t  L_2 = Guid_NewGuid_m541CAC23EBB140DFD3AB5B313315647E95FADB29(/*hidden argument*/NULL);
		V_0 = (Guid_t )L_2;
		DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * L_3 = (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)__this->get__container_2();
		TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* L_4 = (TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)SZArrayNew(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F_il2cpp_TypeInfo_var, (uint32_t)2);
		TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* L_5 = (TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)L_4;
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_6 = { reinterpret_cast<intptr_t> (IInitializable_t8CDB5239D7F1A24EC49AD4017F6D65D2A8A5F345_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_7 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6((RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D )L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_7);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_7);
		TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* L_8 = (TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)L_5;
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_9 = { reinterpret_cast<intptr_t> (IDisposable_t7218B22548186B208D65EA5B7870503810A2D15A_0_0_0_var) };
		Type_t * L_10 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6((RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D )L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_10);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_10);
		NullCheck((DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)L_3);
		ConcreteIdBinderNonGeneric_t372B43CB3CC50C44EF038F152C0F6345DFA22E24 * L_11 = DiContainer_Bind_m838139625D2905AE9B447C19F948EEE024380782((DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)L_3, (TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)L_8, /*hidden argument*/NULL);
		NullCheck((ConcreteBinderNonGeneric_t607DFF5BDEE16A8EF207F5D6148835CD108F72F9 *)L_11);
		FromBinderNonGeneric_t677B667DE367A055191AC7B64A622B8495C6CF89 * L_12 = ((  FromBinderNonGeneric_t677B667DE367A055191AC7B64A622B8495C6CF89 * (*) (ConcreteBinderNonGeneric_t607DFF5BDEE16A8EF207F5D6148835CD108F72F9 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((ConcreteBinderNonGeneric_t607DFF5BDEE16A8EF207F5D6148835CD108F72F9 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		NullCheck((ScopeArgConditionCopyNonLazyBinder_t218A003451367EEC6A439D63A0F38B964EF07B33 *)L_12);
		ArgConditionCopyNonLazyBinder_t5F1B4361E0805E20DDBEDEBC61B90CFFBB68B21B * L_13 = ScopeArgConditionCopyNonLazyBinder_AsCached_mAAD80E3CF24D5B46128725D1045BCA847A1205D2((ScopeArgConditionCopyNonLazyBinder_t218A003451367EEC6A439D63A0F38B964EF07B33 *)L_12, /*hidden argument*/NULL);
		Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * L_14 = ___method0;
		DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * L_15 = (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)__this->get__container_2();
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_16 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 1)) };
		Type_t * L_17 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6((RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D )L_16, /*hidden argument*/NULL);
		Guid_t  L_18 = V_0;
		Guid_t  L_19 = L_18;
		RuntimeObject * L_20 = Box(Guid_t_il2cpp_TypeInfo_var, &L_19);
		InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * L_21 = (InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF *)il2cpp_codegen_object_new(InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF_il2cpp_TypeInfo_var);
		InjectContext__ctor_mEA73C2DE706F160699AA9C80A0171F62225C8F2E(L_21, (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)L_15, (Type_t *)L_17, (RuntimeObject *)L_20, /*hidden argument*/NULL);
		Type_t * L_22 = (Type_t *)__this->get__signalType_1();
		NullCheck((SignalHandlerBinder_t4AB70F7A7BD1596D46B88F834A400EBEDE185771 *)__this);
		RuntimeObject * L_23 = SignalHandlerBinder_get_Identifier_mDA06F489C05DEA9CAB1FBB2224D6A4A2A8341C0D((SignalHandlerBinder_t4AB70F7A7BD1596D46B88F834A400EBEDE185771 *)__this, /*hidden argument*/NULL);
		BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA * L_24 = (BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA *)il2cpp_codegen_object_new(BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA_il2cpp_TypeInfo_var);
		BindingId__ctor_mA092ED49B59C24FC8075ABB8C6070B8FEBFC19B1(L_24, (Type_t *)L_22, (RuntimeObject *)L_23, /*hidden argument*/NULL);
		NullCheck((ArgConditionCopyNonLazyBinder_t5F1B4361E0805E20DDBEDEBC61B90CFFBB68B21B *)L_13);
		((  ConditionCopyNonLazyBinder_tF7F37C5F1508AAB283151819E412C853A8D94B9B * (*) (ArgConditionCopyNonLazyBinder_t5F1B4361E0805E20DDBEDEBC61B90CFFBB68B21B *, Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 *, InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF *, BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)((ArgConditionCopyNonLazyBinder_t5F1B4361E0805E20DDBEDEBC61B90CFFBB68B21B *)L_13, (Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 *)L_14, (InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF *)L_21, (BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA *)L_24, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_25 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 1)) };
		Type_t * L_26 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6((RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D )L_25, /*hidden argument*/NULL);
		BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * L_27 = (BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)il2cpp_codegen_object_new(BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991_il2cpp_TypeInfo_var);
		BindInfo__ctor_m0653A2A5799A7A231939E22C2E728AFA4B006B6D(L_27, (Type_t *)L_26, /*hidden argument*/NULL);
		V_1 = (BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)L_27;
		BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * L_28 = V_1;
		DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * L_29 = (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)__this->get__container_2();
		BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * L_30 = V_1;
		NullCheck((DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)L_29);
		ConcreteIdBinderGeneric_1_tAD404DF3AD6E1FDBC78FD89CAC0C982BC2B642A7 * L_31 = ((  ConcreteIdBinderGeneric_1_tAD404DF3AD6E1FDBC78FD89CAC0C982BC2B642A7 * (*) (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *, BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3)->methodPointer)((DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)L_29, (BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3));
		Guid_t  L_32 = V_0;
		Guid_t  L_33 = L_32;
		RuntimeObject * L_34 = Box(Guid_t_il2cpp_TypeInfo_var, &L_33);
		NullCheck((ConcreteIdBinderGeneric_1_tAD404DF3AD6E1FDBC78FD89CAC0C982BC2B642A7 *)L_31);
		ConcreteBinderGeneric_1_tDC772935855A7D8CFF1BF9CD8DB8696DCDAE19F1 * L_35 = ((  ConcreteBinderGeneric_1_tDC772935855A7D8CFF1BF9CD8DB8696DCDAE19F1 * (*) (ConcreteIdBinderGeneric_1_tAD404DF3AD6E1FDBC78FD89CAC0C982BC2B642A7 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4)->methodPointer)((ConcreteIdBinderGeneric_1_tAD404DF3AD6E1FDBC78FD89CAC0C982BC2B642A7 *)L_31, (RuntimeObject *)L_34, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4));
		NullCheck((ConcreteBinderGeneric_1_tDC772935855A7D8CFF1BF9CD8DB8696DCDAE19F1 *)L_35);
		FromBinderGeneric_1_tB3D3D4B24691B0AFF684D39C08F63B2193327187 * L_36 = ((  FromBinderGeneric_1_tB3D3D4B24691B0AFF684D39C08F63B2193327187 * (*) (ConcreteBinderGeneric_1_tDC772935855A7D8CFF1BF9CD8DB8696DCDAE19F1 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 5)->methodPointer)((ConcreteBinderGeneric_1_tDC772935855A7D8CFF1BF9CD8DB8696DCDAE19F1 *)L_35, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 5));
		SignalFromBinder_1_t862D9073BD2CB93CFFF7F75A6F7262CDF34AA1D7 * L_37 = (SignalFromBinder_1_t862D9073BD2CB93CFFF7F75A6F7262CDF34AA1D7 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 6));
		((  void (*) (SignalFromBinder_1_t862D9073BD2CB93CFFF7F75A6F7262CDF34AA1D7 *, BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *, FromBinderGeneric_1_tB3D3D4B24691B0AFF684D39C08F63B2193327187 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 7)->methodPointer)(L_37, (BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)L_28, (FromBinderGeneric_1_tB3D3D4B24691B0AFF684D39C08F63B2193327187 *)L_36, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 7));
		return L_37;
	}
}
// Zenject.SignalFromBinder`1<THandler> Zenject.SignalHandlerBinder::To<System.Object>(System.Func`2<THandler,System.Action>)
extern "C" IL2CPP_METHOD_ATTR SignalFromBinder_1_t862D9073BD2CB93CFFF7F75A6F7262CDF34AA1D7 * SignalHandlerBinder_To_TisRuntimeObject_mD2ABC9CBE6C2353BF9577B1523BCB2AAC8E2E6CA_gshared (SignalHandlerBinder_t4AB70F7A7BD1596D46B88F834A400EBEDE185771 * __this, Func_2_t058479A951AB44C28D842472579EAA2FDD8DFC0D * ___methodGetter0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SignalHandlerBinder_To_TisRuntimeObject_mD2ABC9CBE6C2353BF9577B1523BCB2AAC8E2E6CA_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Guid_t  V_0;
	memset(&V_0, 0, sizeof(V_0));
	BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * V_1 = NULL;
	{
		BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 * L_0 = (BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 *)__this->get__finalizerWrapper_0();
		NullBindingFinalizer_tFEAF0122BA4AF8A5209DE0330F0BFF444F06AD65 * L_1 = (NullBindingFinalizer_tFEAF0122BA4AF8A5209DE0330F0BFF444F06AD65 *)il2cpp_codegen_object_new(NullBindingFinalizer_tFEAF0122BA4AF8A5209DE0330F0BFF444F06AD65_il2cpp_TypeInfo_var);
		NullBindingFinalizer__ctor_m2B90434471F862EF8E0C197230BDB1EAB586834A(L_1, /*hidden argument*/NULL);
		NullCheck((BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 *)L_0);
		BindFinalizerWrapper_set_SubFinalizer_m6AF16F9C3D2BE2F006E60D76D85F20EFE8C76874((BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 *)L_0, (RuntimeObject*)L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Guid_t_il2cpp_TypeInfo_var);
		Guid_t  L_2 = Guid_NewGuid_m541CAC23EBB140DFD3AB5B313315647E95FADB29(/*hidden argument*/NULL);
		V_0 = (Guid_t )L_2;
		DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * L_3 = (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)__this->get__container_2();
		TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* L_4 = (TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)SZArrayNew(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F_il2cpp_TypeInfo_var, (uint32_t)2);
		TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* L_5 = (TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)L_4;
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_6 = { reinterpret_cast<intptr_t> (IInitializable_t8CDB5239D7F1A24EC49AD4017F6D65D2A8A5F345_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_7 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6((RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D )L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_7);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_7);
		TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* L_8 = (TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)L_5;
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_9 = { reinterpret_cast<intptr_t> (IDisposable_t7218B22548186B208D65EA5B7870503810A2D15A_0_0_0_var) };
		Type_t * L_10 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6((RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D )L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_10);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_10);
		NullCheck((DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)L_3);
		ConcreteIdBinderNonGeneric_t372B43CB3CC50C44EF038F152C0F6345DFA22E24 * L_11 = DiContainer_Bind_m838139625D2905AE9B447C19F948EEE024380782((DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)L_3, (TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)L_8, /*hidden argument*/NULL);
		NullCheck((ConcreteBinderNonGeneric_t607DFF5BDEE16A8EF207F5D6148835CD108F72F9 *)L_11);
		FromBinderNonGeneric_t677B667DE367A055191AC7B64A622B8495C6CF89 * L_12 = ((  FromBinderNonGeneric_t677B667DE367A055191AC7B64A622B8495C6CF89 * (*) (ConcreteBinderNonGeneric_t607DFF5BDEE16A8EF207F5D6148835CD108F72F9 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((ConcreteBinderNonGeneric_t607DFF5BDEE16A8EF207F5D6148835CD108F72F9 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		NullCheck((ScopeArgConditionCopyNonLazyBinder_t218A003451367EEC6A439D63A0F38B964EF07B33 *)L_12);
		ArgConditionCopyNonLazyBinder_t5F1B4361E0805E20DDBEDEBC61B90CFFBB68B21B * L_13 = ScopeArgConditionCopyNonLazyBinder_AsCached_mAAD80E3CF24D5B46128725D1045BCA847A1205D2((ScopeArgConditionCopyNonLazyBinder_t218A003451367EEC6A439D63A0F38B964EF07B33 *)L_12, /*hidden argument*/NULL);
		Func_2_t058479A951AB44C28D842472579EAA2FDD8DFC0D * L_14 = ___methodGetter0;
		DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * L_15 = (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)__this->get__container_2();
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_16 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 1)) };
		Type_t * L_17 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6((RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D )L_16, /*hidden argument*/NULL);
		Guid_t  L_18 = V_0;
		Guid_t  L_19 = L_18;
		RuntimeObject * L_20 = Box(Guid_t_il2cpp_TypeInfo_var, &L_19);
		InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * L_21 = (InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF *)il2cpp_codegen_object_new(InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF_il2cpp_TypeInfo_var);
		InjectContext__ctor_mEA73C2DE706F160699AA9C80A0171F62225C8F2E(L_21, (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)L_15, (Type_t *)L_17, (RuntimeObject *)L_20, /*hidden argument*/NULL);
		Type_t * L_22 = (Type_t *)__this->get__signalType_1();
		NullCheck((SignalHandlerBinder_t4AB70F7A7BD1596D46B88F834A400EBEDE185771 *)__this);
		RuntimeObject * L_23 = SignalHandlerBinder_get_Identifier_mDA06F489C05DEA9CAB1FBB2224D6A4A2A8341C0D((SignalHandlerBinder_t4AB70F7A7BD1596D46B88F834A400EBEDE185771 *)__this, /*hidden argument*/NULL);
		BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA * L_24 = (BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA *)il2cpp_codegen_object_new(BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA_il2cpp_TypeInfo_var);
		BindingId__ctor_mA092ED49B59C24FC8075ABB8C6070B8FEBFC19B1(L_24, (Type_t *)L_22, (RuntimeObject *)L_23, /*hidden argument*/NULL);
		NullCheck((ArgConditionCopyNonLazyBinder_t5F1B4361E0805E20DDBEDEBC61B90CFFBB68B21B *)L_13);
		((  ConditionCopyNonLazyBinder_tF7F37C5F1508AAB283151819E412C853A8D94B9B * (*) (ArgConditionCopyNonLazyBinder_t5F1B4361E0805E20DDBEDEBC61B90CFFBB68B21B *, Func_2_t058479A951AB44C28D842472579EAA2FDD8DFC0D *, InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF *, BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)((ArgConditionCopyNonLazyBinder_t5F1B4361E0805E20DDBEDEBC61B90CFFBB68B21B *)L_13, (Func_2_t058479A951AB44C28D842472579EAA2FDD8DFC0D *)L_14, (InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF *)L_21, (BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA *)L_24, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_25 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 1)) };
		Type_t * L_26 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6((RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D )L_25, /*hidden argument*/NULL);
		BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * L_27 = (BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)il2cpp_codegen_object_new(BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991_il2cpp_TypeInfo_var);
		BindInfo__ctor_m0653A2A5799A7A231939E22C2E728AFA4B006B6D(L_27, (Type_t *)L_26, /*hidden argument*/NULL);
		V_1 = (BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)L_27;
		BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * L_28 = V_1;
		DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * L_29 = (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)__this->get__container_2();
		BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * L_30 = V_1;
		NullCheck((DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)L_29);
		ConcreteIdBinderGeneric_1_tAD404DF3AD6E1FDBC78FD89CAC0C982BC2B642A7 * L_31 = ((  ConcreteIdBinderGeneric_1_tAD404DF3AD6E1FDBC78FD89CAC0C982BC2B642A7 * (*) (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *, BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3)->methodPointer)((DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)L_29, (BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3));
		Guid_t  L_32 = V_0;
		Guid_t  L_33 = L_32;
		RuntimeObject * L_34 = Box(Guid_t_il2cpp_TypeInfo_var, &L_33);
		NullCheck((ConcreteIdBinderGeneric_1_tAD404DF3AD6E1FDBC78FD89CAC0C982BC2B642A7 *)L_31);
		ConcreteBinderGeneric_1_tDC772935855A7D8CFF1BF9CD8DB8696DCDAE19F1 * L_35 = ((  ConcreteBinderGeneric_1_tDC772935855A7D8CFF1BF9CD8DB8696DCDAE19F1 * (*) (ConcreteIdBinderGeneric_1_tAD404DF3AD6E1FDBC78FD89CAC0C982BC2B642A7 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4)->methodPointer)((ConcreteIdBinderGeneric_1_tAD404DF3AD6E1FDBC78FD89CAC0C982BC2B642A7 *)L_31, (RuntimeObject *)L_34, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4));
		NullCheck((ConcreteBinderGeneric_1_tDC772935855A7D8CFF1BF9CD8DB8696DCDAE19F1 *)L_35);
		FromBinderGeneric_1_tB3D3D4B24691B0AFF684D39C08F63B2193327187 * L_36 = ((  FromBinderGeneric_1_tB3D3D4B24691B0AFF684D39C08F63B2193327187 * (*) (ConcreteBinderGeneric_1_tDC772935855A7D8CFF1BF9CD8DB8696DCDAE19F1 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 5)->methodPointer)((ConcreteBinderGeneric_1_tDC772935855A7D8CFF1BF9CD8DB8696DCDAE19F1 *)L_35, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 5));
		SignalFromBinder_1_t862D9073BD2CB93CFFF7F75A6F7262CDF34AA1D7 * L_37 = (SignalFromBinder_1_t862D9073BD2CB93CFFF7F75A6F7262CDF34AA1D7 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 6));
		((  void (*) (SignalFromBinder_1_t862D9073BD2CB93CFFF7F75A6F7262CDF34AA1D7 *, BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *, FromBinderGeneric_1_tB3D3D4B24691B0AFF684D39C08F63B2193327187 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 7)->methodPointer)(L_37, (BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)L_28, (FromBinderGeneric_1_tB3D3D4B24691B0AFF684D39C08F63B2193327187 *)L_36, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 7));
		return L_37;
	}
}
// Zenject.SignalFromBinder`1<THandler> Zenject.SignalHandlerBinder`1<System.Object>::To<System.Object>(System.Action`2<THandler,TParam1>)
extern "C" IL2CPP_METHOD_ATTR SignalFromBinder_1_t862D9073BD2CB93CFFF7F75A6F7262CDF34AA1D7 * SignalHandlerBinder_1_To_TisRuntimeObject_m825B6A42D885A2F242B8F763BCABFE8488A0EE2A_gshared (SignalHandlerBinder_1_tAC85A1AD98374941739974445C41BF8617CF78FC * __this, Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C * ___method0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SignalHandlerBinder_1_To_TisRuntimeObject_m825B6A42D885A2F242B8F763BCABFE8488A0EE2A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Guid_t  V_0;
	memset(&V_0, 0, sizeof(V_0));
	BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * V_1 = NULL;
	{
		BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 * L_0 = (BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 *)__this->get__finalizerWrapper_0();
		NullBindingFinalizer_tFEAF0122BA4AF8A5209DE0330F0BFF444F06AD65 * L_1 = (NullBindingFinalizer_tFEAF0122BA4AF8A5209DE0330F0BFF444F06AD65 *)il2cpp_codegen_object_new(NullBindingFinalizer_tFEAF0122BA4AF8A5209DE0330F0BFF444F06AD65_il2cpp_TypeInfo_var);
		NullBindingFinalizer__ctor_m2B90434471F862EF8E0C197230BDB1EAB586834A(L_1, /*hidden argument*/NULL);
		NullCheck((BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 *)L_0);
		BindFinalizerWrapper_set_SubFinalizer_m6AF16F9C3D2BE2F006E60D76D85F20EFE8C76874((BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 *)L_0, (RuntimeObject*)L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Guid_t_il2cpp_TypeInfo_var);
		Guid_t  L_2 = Guid_NewGuid_m541CAC23EBB140DFD3AB5B313315647E95FADB29(/*hidden argument*/NULL);
		V_0 = (Guid_t )L_2;
		DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * L_3 = (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)__this->get__container_2();
		TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* L_4 = (TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)SZArrayNew(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F_il2cpp_TypeInfo_var, (uint32_t)2);
		TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* L_5 = (TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)L_4;
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_6 = { reinterpret_cast<intptr_t> (IInitializable_t8CDB5239D7F1A24EC49AD4017F6D65D2A8A5F345_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_7 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6((RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D )L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_7);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_7);
		TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* L_8 = (TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)L_5;
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_9 = { reinterpret_cast<intptr_t> (IDisposable_t7218B22548186B208D65EA5B7870503810A2D15A_0_0_0_var) };
		Type_t * L_10 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6((RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D )L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_10);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_10);
		NullCheck((DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)L_3);
		ConcreteIdBinderNonGeneric_t372B43CB3CC50C44EF038F152C0F6345DFA22E24 * L_11 = DiContainer_Bind_m838139625D2905AE9B447C19F948EEE024380782((DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)L_3, (TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)L_8, /*hidden argument*/NULL);
		NullCheck((ConcreteBinderNonGeneric_t607DFF5BDEE16A8EF207F5D6148835CD108F72F9 *)L_11);
		FromBinderNonGeneric_t677B667DE367A055191AC7B64A622B8495C6CF89 * L_12 = ((  FromBinderNonGeneric_t677B667DE367A055191AC7B64A622B8495C6CF89 * (*) (ConcreteBinderNonGeneric_t607DFF5BDEE16A8EF207F5D6148835CD108F72F9 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((ConcreteBinderNonGeneric_t607DFF5BDEE16A8EF207F5D6148835CD108F72F9 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		NullCheck((ScopeArgConditionCopyNonLazyBinder_t218A003451367EEC6A439D63A0F38B964EF07B33 *)L_12);
		ArgConditionCopyNonLazyBinder_t5F1B4361E0805E20DDBEDEBC61B90CFFBB68B21B * L_13 = ScopeArgConditionCopyNonLazyBinder_AsCached_mAAD80E3CF24D5B46128725D1045BCA847A1205D2((ScopeArgConditionCopyNonLazyBinder_t218A003451367EEC6A439D63A0F38B964EF07B33 *)L_12, /*hidden argument*/NULL);
		Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C * L_14 = ___method0;
		DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * L_15 = (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)__this->get__container_2();
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_16 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 1)) };
		Type_t * L_17 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6((RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D )L_16, /*hidden argument*/NULL);
		Guid_t  L_18 = V_0;
		Guid_t  L_19 = L_18;
		RuntimeObject * L_20 = Box(Guid_t_il2cpp_TypeInfo_var, &L_19);
		InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * L_21 = (InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF *)il2cpp_codegen_object_new(InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF_il2cpp_TypeInfo_var);
		InjectContext__ctor_mEA73C2DE706F160699AA9C80A0171F62225C8F2E(L_21, (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)L_15, (Type_t *)L_17, (RuntimeObject *)L_20, /*hidden argument*/NULL);
		Type_t * L_22 = (Type_t *)__this->get__signalType_1();
		NullCheck((SignalHandlerBinder_1_tAC85A1AD98374941739974445C41BF8617CF78FC *)__this);
		RuntimeObject * L_23 = ((  RuntimeObject * (*) (SignalHandlerBinder_1_tAC85A1AD98374941739974445C41BF8617CF78FC *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((SignalHandlerBinder_1_tAC85A1AD98374941739974445C41BF8617CF78FC *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA * L_24 = (BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA *)il2cpp_codegen_object_new(BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA_il2cpp_TypeInfo_var);
		BindingId__ctor_mA092ED49B59C24FC8075ABB8C6070B8FEBFC19B1(L_24, (Type_t *)L_22, (RuntimeObject *)L_23, /*hidden argument*/NULL);
		NullCheck((ArgConditionCopyNonLazyBinder_t5F1B4361E0805E20DDBEDEBC61B90CFFBB68B21B *)L_13);
		((  ConditionCopyNonLazyBinder_tF7F37C5F1508AAB283151819E412C853A8D94B9B * (*) (ArgConditionCopyNonLazyBinder_t5F1B4361E0805E20DDBEDEBC61B90CFFBB68B21B *, Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C *, InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF *, BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)((ArgConditionCopyNonLazyBinder_t5F1B4361E0805E20DDBEDEBC61B90CFFBB68B21B *)L_13, (Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C *)L_14, (InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF *)L_21, (BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA *)L_24, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_25 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 1)) };
		Type_t * L_26 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6((RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D )L_25, /*hidden argument*/NULL);
		BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * L_27 = (BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)il2cpp_codegen_object_new(BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991_il2cpp_TypeInfo_var);
		BindInfo__ctor_m0653A2A5799A7A231939E22C2E728AFA4B006B6D(L_27, (Type_t *)L_26, /*hidden argument*/NULL);
		V_1 = (BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)L_27;
		BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * L_28 = V_1;
		DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * L_29 = (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)__this->get__container_2();
		BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * L_30 = V_1;
		NullCheck((DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)L_29);
		ConcreteIdBinderGeneric_1_tAD404DF3AD6E1FDBC78FD89CAC0C982BC2B642A7 * L_31 = ((  ConcreteIdBinderGeneric_1_tAD404DF3AD6E1FDBC78FD89CAC0C982BC2B642A7 * (*) (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *, BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3)->methodPointer)((DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)L_29, (BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3));
		Guid_t  L_32 = V_0;
		Guid_t  L_33 = L_32;
		RuntimeObject * L_34 = Box(Guid_t_il2cpp_TypeInfo_var, &L_33);
		NullCheck((ConcreteIdBinderGeneric_1_tAD404DF3AD6E1FDBC78FD89CAC0C982BC2B642A7 *)L_31);
		ConcreteBinderGeneric_1_tDC772935855A7D8CFF1BF9CD8DB8696DCDAE19F1 * L_35 = ((  ConcreteBinderGeneric_1_tDC772935855A7D8CFF1BF9CD8DB8696DCDAE19F1 * (*) (ConcreteIdBinderGeneric_1_tAD404DF3AD6E1FDBC78FD89CAC0C982BC2B642A7 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4)->methodPointer)((ConcreteIdBinderGeneric_1_tAD404DF3AD6E1FDBC78FD89CAC0C982BC2B642A7 *)L_31, (RuntimeObject *)L_34, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4));
		NullCheck((ConcreteBinderGeneric_1_tDC772935855A7D8CFF1BF9CD8DB8696DCDAE19F1 *)L_35);
		FromBinderGeneric_1_tB3D3D4B24691B0AFF684D39C08F63B2193327187 * L_36 = ((  FromBinderGeneric_1_tB3D3D4B24691B0AFF684D39C08F63B2193327187 * (*) (ConcreteBinderGeneric_1_tDC772935855A7D8CFF1BF9CD8DB8696DCDAE19F1 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 5)->methodPointer)((ConcreteBinderGeneric_1_tDC772935855A7D8CFF1BF9CD8DB8696DCDAE19F1 *)L_35, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 5));
		SignalFromBinder_1_t862D9073BD2CB93CFFF7F75A6F7262CDF34AA1D7 * L_37 = (SignalFromBinder_1_t862D9073BD2CB93CFFF7F75A6F7262CDF34AA1D7 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 6));
		((  void (*) (SignalFromBinder_1_t862D9073BD2CB93CFFF7F75A6F7262CDF34AA1D7 *, BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *, FromBinderGeneric_1_tB3D3D4B24691B0AFF684D39C08F63B2193327187 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 7)->methodPointer)(L_37, (BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)L_28, (FromBinderGeneric_1_tB3D3D4B24691B0AFF684D39C08F63B2193327187 *)L_36, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 7));
		return L_37;
	}
}
// Zenject.SignalFromBinder`1<THandler> Zenject.SignalHandlerBinder`1<System.Object>::To<System.Object>(System.Func`2<THandler,System.Action`1<TParam1>>)
extern "C" IL2CPP_METHOD_ATTR SignalFromBinder_1_t862D9073BD2CB93CFFF7F75A6F7262CDF34AA1D7 * SignalHandlerBinder_1_To_TisRuntimeObject_m85941C21720C8AA6D0DC2E27BB82A2EC4F32CBEE_gshared (SignalHandlerBinder_1_tAC85A1AD98374941739974445C41BF8617CF78FC * __this, Func_2_t38649B64802A8DCC2476C1CBE0CF179DBBFA5113 * ___methodGetter0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SignalHandlerBinder_1_To_TisRuntimeObject_m85941C21720C8AA6D0DC2E27BB82A2EC4F32CBEE_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Guid_t  V_0;
	memset(&V_0, 0, sizeof(V_0));
	BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * V_1 = NULL;
	{
		BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 * L_0 = (BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 *)__this->get__finalizerWrapper_0();
		NullBindingFinalizer_tFEAF0122BA4AF8A5209DE0330F0BFF444F06AD65 * L_1 = (NullBindingFinalizer_tFEAF0122BA4AF8A5209DE0330F0BFF444F06AD65 *)il2cpp_codegen_object_new(NullBindingFinalizer_tFEAF0122BA4AF8A5209DE0330F0BFF444F06AD65_il2cpp_TypeInfo_var);
		NullBindingFinalizer__ctor_m2B90434471F862EF8E0C197230BDB1EAB586834A(L_1, /*hidden argument*/NULL);
		NullCheck((BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 *)L_0);
		BindFinalizerWrapper_set_SubFinalizer_m6AF16F9C3D2BE2F006E60D76D85F20EFE8C76874((BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 *)L_0, (RuntimeObject*)L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Guid_t_il2cpp_TypeInfo_var);
		Guid_t  L_2 = Guid_NewGuid_m541CAC23EBB140DFD3AB5B313315647E95FADB29(/*hidden argument*/NULL);
		V_0 = (Guid_t )L_2;
		DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * L_3 = (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)__this->get__container_2();
		TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* L_4 = (TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)SZArrayNew(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F_il2cpp_TypeInfo_var, (uint32_t)2);
		TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* L_5 = (TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)L_4;
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_6 = { reinterpret_cast<intptr_t> (IInitializable_t8CDB5239D7F1A24EC49AD4017F6D65D2A8A5F345_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_7 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6((RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D )L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_7);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_7);
		TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* L_8 = (TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)L_5;
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_9 = { reinterpret_cast<intptr_t> (IDisposable_t7218B22548186B208D65EA5B7870503810A2D15A_0_0_0_var) };
		Type_t * L_10 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6((RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D )L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_10);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_10);
		NullCheck((DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)L_3);
		ConcreteIdBinderNonGeneric_t372B43CB3CC50C44EF038F152C0F6345DFA22E24 * L_11 = DiContainer_Bind_m838139625D2905AE9B447C19F948EEE024380782((DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)L_3, (TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)L_8, /*hidden argument*/NULL);
		NullCheck((ConcreteBinderNonGeneric_t607DFF5BDEE16A8EF207F5D6148835CD108F72F9 *)L_11);
		FromBinderNonGeneric_t677B667DE367A055191AC7B64A622B8495C6CF89 * L_12 = ((  FromBinderNonGeneric_t677B667DE367A055191AC7B64A622B8495C6CF89 * (*) (ConcreteBinderNonGeneric_t607DFF5BDEE16A8EF207F5D6148835CD108F72F9 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((ConcreteBinderNonGeneric_t607DFF5BDEE16A8EF207F5D6148835CD108F72F9 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		NullCheck((ScopeArgConditionCopyNonLazyBinder_t218A003451367EEC6A439D63A0F38B964EF07B33 *)L_12);
		ArgConditionCopyNonLazyBinder_t5F1B4361E0805E20DDBEDEBC61B90CFFBB68B21B * L_13 = ScopeArgConditionCopyNonLazyBinder_AsCached_mAAD80E3CF24D5B46128725D1045BCA847A1205D2((ScopeArgConditionCopyNonLazyBinder_t218A003451367EEC6A439D63A0F38B964EF07B33 *)L_12, /*hidden argument*/NULL);
		Func_2_t38649B64802A8DCC2476C1CBE0CF179DBBFA5113 * L_14 = ___methodGetter0;
		DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * L_15 = (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)__this->get__container_2();
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_16 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 1)) };
		Type_t * L_17 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6((RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D )L_16, /*hidden argument*/NULL);
		Guid_t  L_18 = V_0;
		Guid_t  L_19 = L_18;
		RuntimeObject * L_20 = Box(Guid_t_il2cpp_TypeInfo_var, &L_19);
		InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * L_21 = (InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF *)il2cpp_codegen_object_new(InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF_il2cpp_TypeInfo_var);
		InjectContext__ctor_mEA73C2DE706F160699AA9C80A0171F62225C8F2E(L_21, (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)L_15, (Type_t *)L_17, (RuntimeObject *)L_20, /*hidden argument*/NULL);
		Type_t * L_22 = (Type_t *)__this->get__signalType_1();
		NullCheck((SignalHandlerBinder_1_tAC85A1AD98374941739974445C41BF8617CF78FC *)__this);
		RuntimeObject * L_23 = ((  RuntimeObject * (*) (SignalHandlerBinder_1_tAC85A1AD98374941739974445C41BF8617CF78FC *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((SignalHandlerBinder_1_tAC85A1AD98374941739974445C41BF8617CF78FC *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA * L_24 = (BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA *)il2cpp_codegen_object_new(BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA_il2cpp_TypeInfo_var);
		BindingId__ctor_mA092ED49B59C24FC8075ABB8C6070B8FEBFC19B1(L_24, (Type_t *)L_22, (RuntimeObject *)L_23, /*hidden argument*/NULL);
		NullCheck((ArgConditionCopyNonLazyBinder_t5F1B4361E0805E20DDBEDEBC61B90CFFBB68B21B *)L_13);
		((  ConditionCopyNonLazyBinder_tF7F37C5F1508AAB283151819E412C853A8D94B9B * (*) (ArgConditionCopyNonLazyBinder_t5F1B4361E0805E20DDBEDEBC61B90CFFBB68B21B *, Func_2_t38649B64802A8DCC2476C1CBE0CF179DBBFA5113 *, InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF *, BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)((ArgConditionCopyNonLazyBinder_t5F1B4361E0805E20DDBEDEBC61B90CFFBB68B21B *)L_13, (Func_2_t38649B64802A8DCC2476C1CBE0CF179DBBFA5113 *)L_14, (InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF *)L_21, (BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA *)L_24, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_25 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 1)) };
		Type_t * L_26 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6((RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D )L_25, /*hidden argument*/NULL);
		BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * L_27 = (BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)il2cpp_codegen_object_new(BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991_il2cpp_TypeInfo_var);
		BindInfo__ctor_m0653A2A5799A7A231939E22C2E728AFA4B006B6D(L_27, (Type_t *)L_26, /*hidden argument*/NULL);
		V_1 = (BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)L_27;
		BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * L_28 = V_1;
		DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * L_29 = (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)__this->get__container_2();
		BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * L_30 = V_1;
		NullCheck((DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)L_29);
		ConcreteIdBinderGeneric_1_tAD404DF3AD6E1FDBC78FD89CAC0C982BC2B642A7 * L_31 = ((  ConcreteIdBinderGeneric_1_tAD404DF3AD6E1FDBC78FD89CAC0C982BC2B642A7 * (*) (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *, BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3)->methodPointer)((DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)L_29, (BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3));
		Guid_t  L_32 = V_0;
		Guid_t  L_33 = L_32;
		RuntimeObject * L_34 = Box(Guid_t_il2cpp_TypeInfo_var, &L_33);
		NullCheck((ConcreteIdBinderGeneric_1_tAD404DF3AD6E1FDBC78FD89CAC0C982BC2B642A7 *)L_31);
		ConcreteBinderGeneric_1_tDC772935855A7D8CFF1BF9CD8DB8696DCDAE19F1 * L_35 = ((  ConcreteBinderGeneric_1_tDC772935855A7D8CFF1BF9CD8DB8696DCDAE19F1 * (*) (ConcreteIdBinderGeneric_1_tAD404DF3AD6E1FDBC78FD89CAC0C982BC2B642A7 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4)->methodPointer)((ConcreteIdBinderGeneric_1_tAD404DF3AD6E1FDBC78FD89CAC0C982BC2B642A7 *)L_31, (RuntimeObject *)L_34, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4));
		NullCheck((ConcreteBinderGeneric_1_tDC772935855A7D8CFF1BF9CD8DB8696DCDAE19F1 *)L_35);
		FromBinderGeneric_1_tB3D3D4B24691B0AFF684D39C08F63B2193327187 * L_36 = ((  FromBinderGeneric_1_tB3D3D4B24691B0AFF684D39C08F63B2193327187 * (*) (ConcreteBinderGeneric_1_tDC772935855A7D8CFF1BF9CD8DB8696DCDAE19F1 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 5)->methodPointer)((ConcreteBinderGeneric_1_tDC772935855A7D8CFF1BF9CD8DB8696DCDAE19F1 *)L_35, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 5));
		SignalFromBinder_1_t862D9073BD2CB93CFFF7F75A6F7262CDF34AA1D7 * L_37 = (SignalFromBinder_1_t862D9073BD2CB93CFFF7F75A6F7262CDF34AA1D7 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 6));
		((  void (*) (SignalFromBinder_1_t862D9073BD2CB93CFFF7F75A6F7262CDF34AA1D7 *, BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *, FromBinderGeneric_1_tB3D3D4B24691B0AFF684D39C08F63B2193327187 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 7)->methodPointer)(L_37, (BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)L_28, (FromBinderGeneric_1_tB3D3D4B24691B0AFF684D39C08F63B2193327187 *)L_36, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 7));
		return L_37;
	}
}
// Zenject.SignalFromBinder`1<THandler> Zenject.SignalHandlerBinder`2<System.Object,System.Object>::To<System.Object>(System.Action`3<THandler,TParam1,TParam2>)
extern "C" IL2CPP_METHOD_ATTR SignalFromBinder_1_t862D9073BD2CB93CFFF7F75A6F7262CDF34AA1D7 * SignalHandlerBinder_2_To_TisRuntimeObject_m511B8D1DCEF7D8D08E952754B7D65568D9D6B2D6_gshared (SignalHandlerBinder_2_t6F32E8CD8248414B4F3681F2E4D8BFB658F24D30 * __this, Action_3_tCC14115B7178951118504E7198B7C872630643C5 * ___method0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SignalHandlerBinder_2_To_TisRuntimeObject_m511B8D1DCEF7D8D08E952754B7D65568D9D6B2D6_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Guid_t  V_0;
	memset(&V_0, 0, sizeof(V_0));
	BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * V_1 = NULL;
	{
		BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 * L_0 = (BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 *)__this->get__finalizerWrapper_0();
		NullBindingFinalizer_tFEAF0122BA4AF8A5209DE0330F0BFF444F06AD65 * L_1 = (NullBindingFinalizer_tFEAF0122BA4AF8A5209DE0330F0BFF444F06AD65 *)il2cpp_codegen_object_new(NullBindingFinalizer_tFEAF0122BA4AF8A5209DE0330F0BFF444F06AD65_il2cpp_TypeInfo_var);
		NullBindingFinalizer__ctor_m2B90434471F862EF8E0C197230BDB1EAB586834A(L_1, /*hidden argument*/NULL);
		NullCheck((BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 *)L_0);
		BindFinalizerWrapper_set_SubFinalizer_m6AF16F9C3D2BE2F006E60D76D85F20EFE8C76874((BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 *)L_0, (RuntimeObject*)L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Guid_t_il2cpp_TypeInfo_var);
		Guid_t  L_2 = Guid_NewGuid_m541CAC23EBB140DFD3AB5B313315647E95FADB29(/*hidden argument*/NULL);
		V_0 = (Guid_t )L_2;
		DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * L_3 = (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)__this->get__container_2();
		TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* L_4 = (TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)SZArrayNew(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F_il2cpp_TypeInfo_var, (uint32_t)2);
		TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* L_5 = (TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)L_4;
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_6 = { reinterpret_cast<intptr_t> (IInitializable_t8CDB5239D7F1A24EC49AD4017F6D65D2A8A5F345_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_7 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6((RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D )L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_7);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_7);
		TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* L_8 = (TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)L_5;
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_9 = { reinterpret_cast<intptr_t> (IDisposable_t7218B22548186B208D65EA5B7870503810A2D15A_0_0_0_var) };
		Type_t * L_10 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6((RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D )L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_10);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_10);
		NullCheck((DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)L_3);
		ConcreteIdBinderNonGeneric_t372B43CB3CC50C44EF038F152C0F6345DFA22E24 * L_11 = DiContainer_Bind_m838139625D2905AE9B447C19F948EEE024380782((DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)L_3, (TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)L_8, /*hidden argument*/NULL);
		NullCheck((ConcreteBinderNonGeneric_t607DFF5BDEE16A8EF207F5D6148835CD108F72F9 *)L_11);
		FromBinderNonGeneric_t677B667DE367A055191AC7B64A622B8495C6CF89 * L_12 = ((  FromBinderNonGeneric_t677B667DE367A055191AC7B64A622B8495C6CF89 * (*) (ConcreteBinderNonGeneric_t607DFF5BDEE16A8EF207F5D6148835CD108F72F9 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((ConcreteBinderNonGeneric_t607DFF5BDEE16A8EF207F5D6148835CD108F72F9 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		NullCheck((ScopeArgConditionCopyNonLazyBinder_t218A003451367EEC6A439D63A0F38B964EF07B33 *)L_12);
		ArgConditionCopyNonLazyBinder_t5F1B4361E0805E20DDBEDEBC61B90CFFBB68B21B * L_13 = ScopeArgConditionCopyNonLazyBinder_AsCached_mAAD80E3CF24D5B46128725D1045BCA847A1205D2((ScopeArgConditionCopyNonLazyBinder_t218A003451367EEC6A439D63A0F38B964EF07B33 *)L_12, /*hidden argument*/NULL);
		Action_3_tCC14115B7178951118504E7198B7C872630643C5 * L_14 = ___method0;
		DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * L_15 = (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)__this->get__container_2();
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_16 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 1)) };
		Type_t * L_17 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6((RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D )L_16, /*hidden argument*/NULL);
		Guid_t  L_18 = V_0;
		Guid_t  L_19 = L_18;
		RuntimeObject * L_20 = Box(Guid_t_il2cpp_TypeInfo_var, &L_19);
		InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * L_21 = (InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF *)il2cpp_codegen_object_new(InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF_il2cpp_TypeInfo_var);
		InjectContext__ctor_mEA73C2DE706F160699AA9C80A0171F62225C8F2E(L_21, (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)L_15, (Type_t *)L_17, (RuntimeObject *)L_20, /*hidden argument*/NULL);
		Type_t * L_22 = (Type_t *)__this->get__signalType_1();
		NullCheck((SignalHandlerBinder_2_t6F32E8CD8248414B4F3681F2E4D8BFB658F24D30 *)__this);
		RuntimeObject * L_23 = ((  RuntimeObject * (*) (SignalHandlerBinder_2_t6F32E8CD8248414B4F3681F2E4D8BFB658F24D30 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((SignalHandlerBinder_2_t6F32E8CD8248414B4F3681F2E4D8BFB658F24D30 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA * L_24 = (BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA *)il2cpp_codegen_object_new(BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA_il2cpp_TypeInfo_var);
		BindingId__ctor_mA092ED49B59C24FC8075ABB8C6070B8FEBFC19B1(L_24, (Type_t *)L_22, (RuntimeObject *)L_23, /*hidden argument*/NULL);
		NullCheck((ArgConditionCopyNonLazyBinder_t5F1B4361E0805E20DDBEDEBC61B90CFFBB68B21B *)L_13);
		((  ConditionCopyNonLazyBinder_tF7F37C5F1508AAB283151819E412C853A8D94B9B * (*) (ArgConditionCopyNonLazyBinder_t5F1B4361E0805E20DDBEDEBC61B90CFFBB68B21B *, Action_3_tCC14115B7178951118504E7198B7C872630643C5 *, InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF *, BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)((ArgConditionCopyNonLazyBinder_t5F1B4361E0805E20DDBEDEBC61B90CFFBB68B21B *)L_13, (Action_3_tCC14115B7178951118504E7198B7C872630643C5 *)L_14, (InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF *)L_21, (BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA *)L_24, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_25 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 1)) };
		Type_t * L_26 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6((RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D )L_25, /*hidden argument*/NULL);
		BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * L_27 = (BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)il2cpp_codegen_object_new(BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991_il2cpp_TypeInfo_var);
		BindInfo__ctor_m0653A2A5799A7A231939E22C2E728AFA4B006B6D(L_27, (Type_t *)L_26, /*hidden argument*/NULL);
		V_1 = (BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)L_27;
		BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * L_28 = V_1;
		DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * L_29 = (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)__this->get__container_2();
		BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * L_30 = V_1;
		NullCheck((DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)L_29);
		ConcreteIdBinderGeneric_1_tAD404DF3AD6E1FDBC78FD89CAC0C982BC2B642A7 * L_31 = ((  ConcreteIdBinderGeneric_1_tAD404DF3AD6E1FDBC78FD89CAC0C982BC2B642A7 * (*) (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *, BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3)->methodPointer)((DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)L_29, (BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3));
		Guid_t  L_32 = V_0;
		Guid_t  L_33 = L_32;
		RuntimeObject * L_34 = Box(Guid_t_il2cpp_TypeInfo_var, &L_33);
		NullCheck((ConcreteIdBinderGeneric_1_tAD404DF3AD6E1FDBC78FD89CAC0C982BC2B642A7 *)L_31);
		ConcreteBinderGeneric_1_tDC772935855A7D8CFF1BF9CD8DB8696DCDAE19F1 * L_35 = ((  ConcreteBinderGeneric_1_tDC772935855A7D8CFF1BF9CD8DB8696DCDAE19F1 * (*) (ConcreteIdBinderGeneric_1_tAD404DF3AD6E1FDBC78FD89CAC0C982BC2B642A7 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4)->methodPointer)((ConcreteIdBinderGeneric_1_tAD404DF3AD6E1FDBC78FD89CAC0C982BC2B642A7 *)L_31, (RuntimeObject *)L_34, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4));
		NullCheck((ConcreteBinderGeneric_1_tDC772935855A7D8CFF1BF9CD8DB8696DCDAE19F1 *)L_35);
		FromBinderGeneric_1_tB3D3D4B24691B0AFF684D39C08F63B2193327187 * L_36 = ((  FromBinderGeneric_1_tB3D3D4B24691B0AFF684D39C08F63B2193327187 * (*) (ConcreteBinderGeneric_1_tDC772935855A7D8CFF1BF9CD8DB8696DCDAE19F1 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 5)->methodPointer)((ConcreteBinderGeneric_1_tDC772935855A7D8CFF1BF9CD8DB8696DCDAE19F1 *)L_35, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 5));
		SignalFromBinder_1_t862D9073BD2CB93CFFF7F75A6F7262CDF34AA1D7 * L_37 = (SignalFromBinder_1_t862D9073BD2CB93CFFF7F75A6F7262CDF34AA1D7 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 6));
		((  void (*) (SignalFromBinder_1_t862D9073BD2CB93CFFF7F75A6F7262CDF34AA1D7 *, BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *, FromBinderGeneric_1_tB3D3D4B24691B0AFF684D39C08F63B2193327187 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 7)->methodPointer)(L_37, (BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)L_28, (FromBinderGeneric_1_tB3D3D4B24691B0AFF684D39C08F63B2193327187 *)L_36, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 7));
		return L_37;
	}
}
// Zenject.SignalFromBinder`1<THandler> Zenject.SignalHandlerBinder`2<System.Object,System.Object>::To<System.Object>(System.Func`2<THandler,System.Action`2<TParam1,TParam2>>)
extern "C" IL2CPP_METHOD_ATTR SignalFromBinder_1_t862D9073BD2CB93CFFF7F75A6F7262CDF34AA1D7 * SignalHandlerBinder_2_To_TisRuntimeObject_m62670A40CD41CEFB436F17F06AAF381F16B65607_gshared (SignalHandlerBinder_2_t6F32E8CD8248414B4F3681F2E4D8BFB658F24D30 * __this, Func_2_t88CE136AD37DD8F7713742358AC56B012C611D88 * ___methodGetter0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SignalHandlerBinder_2_To_TisRuntimeObject_m62670A40CD41CEFB436F17F06AAF381F16B65607_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Guid_t  V_0;
	memset(&V_0, 0, sizeof(V_0));
	BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * V_1 = NULL;
	{
		BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 * L_0 = (BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 *)__this->get__finalizerWrapper_0();
		NullBindingFinalizer_tFEAF0122BA4AF8A5209DE0330F0BFF444F06AD65 * L_1 = (NullBindingFinalizer_tFEAF0122BA4AF8A5209DE0330F0BFF444F06AD65 *)il2cpp_codegen_object_new(NullBindingFinalizer_tFEAF0122BA4AF8A5209DE0330F0BFF444F06AD65_il2cpp_TypeInfo_var);
		NullBindingFinalizer__ctor_m2B90434471F862EF8E0C197230BDB1EAB586834A(L_1, /*hidden argument*/NULL);
		NullCheck((BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 *)L_0);
		BindFinalizerWrapper_set_SubFinalizer_m6AF16F9C3D2BE2F006E60D76D85F20EFE8C76874((BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 *)L_0, (RuntimeObject*)L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Guid_t_il2cpp_TypeInfo_var);
		Guid_t  L_2 = Guid_NewGuid_m541CAC23EBB140DFD3AB5B313315647E95FADB29(/*hidden argument*/NULL);
		V_0 = (Guid_t )L_2;
		DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * L_3 = (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)__this->get__container_2();
		TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* L_4 = (TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)SZArrayNew(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F_il2cpp_TypeInfo_var, (uint32_t)2);
		TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* L_5 = (TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)L_4;
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_6 = { reinterpret_cast<intptr_t> (IInitializable_t8CDB5239D7F1A24EC49AD4017F6D65D2A8A5F345_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_7 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6((RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D )L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_7);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_7);
		TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* L_8 = (TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)L_5;
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_9 = { reinterpret_cast<intptr_t> (IDisposable_t7218B22548186B208D65EA5B7870503810A2D15A_0_0_0_var) };
		Type_t * L_10 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6((RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D )L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_10);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_10);
		NullCheck((DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)L_3);
		ConcreteIdBinderNonGeneric_t372B43CB3CC50C44EF038F152C0F6345DFA22E24 * L_11 = DiContainer_Bind_m838139625D2905AE9B447C19F948EEE024380782((DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)L_3, (TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)L_8, /*hidden argument*/NULL);
		NullCheck((ConcreteBinderNonGeneric_t607DFF5BDEE16A8EF207F5D6148835CD108F72F9 *)L_11);
		FromBinderNonGeneric_t677B667DE367A055191AC7B64A622B8495C6CF89 * L_12 = ((  FromBinderNonGeneric_t677B667DE367A055191AC7B64A622B8495C6CF89 * (*) (ConcreteBinderNonGeneric_t607DFF5BDEE16A8EF207F5D6148835CD108F72F9 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((ConcreteBinderNonGeneric_t607DFF5BDEE16A8EF207F5D6148835CD108F72F9 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		NullCheck((ScopeArgConditionCopyNonLazyBinder_t218A003451367EEC6A439D63A0F38B964EF07B33 *)L_12);
		ArgConditionCopyNonLazyBinder_t5F1B4361E0805E20DDBEDEBC61B90CFFBB68B21B * L_13 = ScopeArgConditionCopyNonLazyBinder_AsCached_mAAD80E3CF24D5B46128725D1045BCA847A1205D2((ScopeArgConditionCopyNonLazyBinder_t218A003451367EEC6A439D63A0F38B964EF07B33 *)L_12, /*hidden argument*/NULL);
		Func_2_t88CE136AD37DD8F7713742358AC56B012C611D88 * L_14 = ___methodGetter0;
		DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * L_15 = (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)__this->get__container_2();
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_16 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 1)) };
		Type_t * L_17 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6((RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D )L_16, /*hidden argument*/NULL);
		Guid_t  L_18 = V_0;
		Guid_t  L_19 = L_18;
		RuntimeObject * L_20 = Box(Guid_t_il2cpp_TypeInfo_var, &L_19);
		InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * L_21 = (InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF *)il2cpp_codegen_object_new(InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF_il2cpp_TypeInfo_var);
		InjectContext__ctor_mEA73C2DE706F160699AA9C80A0171F62225C8F2E(L_21, (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)L_15, (Type_t *)L_17, (RuntimeObject *)L_20, /*hidden argument*/NULL);
		Type_t * L_22 = (Type_t *)__this->get__signalType_1();
		NullCheck((SignalHandlerBinder_2_t6F32E8CD8248414B4F3681F2E4D8BFB658F24D30 *)__this);
		RuntimeObject * L_23 = ((  RuntimeObject * (*) (SignalHandlerBinder_2_t6F32E8CD8248414B4F3681F2E4D8BFB658F24D30 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((SignalHandlerBinder_2_t6F32E8CD8248414B4F3681F2E4D8BFB658F24D30 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA * L_24 = (BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA *)il2cpp_codegen_object_new(BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA_il2cpp_TypeInfo_var);
		BindingId__ctor_mA092ED49B59C24FC8075ABB8C6070B8FEBFC19B1(L_24, (Type_t *)L_22, (RuntimeObject *)L_23, /*hidden argument*/NULL);
		NullCheck((ArgConditionCopyNonLazyBinder_t5F1B4361E0805E20DDBEDEBC61B90CFFBB68B21B *)L_13);
		((  ConditionCopyNonLazyBinder_tF7F37C5F1508AAB283151819E412C853A8D94B9B * (*) (ArgConditionCopyNonLazyBinder_t5F1B4361E0805E20DDBEDEBC61B90CFFBB68B21B *, Func_2_t88CE136AD37DD8F7713742358AC56B012C611D88 *, InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF *, BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)((ArgConditionCopyNonLazyBinder_t5F1B4361E0805E20DDBEDEBC61B90CFFBB68B21B *)L_13, (Func_2_t88CE136AD37DD8F7713742358AC56B012C611D88 *)L_14, (InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF *)L_21, (BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA *)L_24, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_25 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 1)) };
		Type_t * L_26 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6((RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D )L_25, /*hidden argument*/NULL);
		BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * L_27 = (BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)il2cpp_codegen_object_new(BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991_il2cpp_TypeInfo_var);
		BindInfo__ctor_m0653A2A5799A7A231939E22C2E728AFA4B006B6D(L_27, (Type_t *)L_26, /*hidden argument*/NULL);
		V_1 = (BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)L_27;
		BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * L_28 = V_1;
		DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * L_29 = (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)__this->get__container_2();
		BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * L_30 = V_1;
		NullCheck((DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)L_29);
		ConcreteIdBinderGeneric_1_tAD404DF3AD6E1FDBC78FD89CAC0C982BC2B642A7 * L_31 = ((  ConcreteIdBinderGeneric_1_tAD404DF3AD6E1FDBC78FD89CAC0C982BC2B642A7 * (*) (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *, BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3)->methodPointer)((DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)L_29, (BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3));
		Guid_t  L_32 = V_0;
		Guid_t  L_33 = L_32;
		RuntimeObject * L_34 = Box(Guid_t_il2cpp_TypeInfo_var, &L_33);
		NullCheck((ConcreteIdBinderGeneric_1_tAD404DF3AD6E1FDBC78FD89CAC0C982BC2B642A7 *)L_31);
		ConcreteBinderGeneric_1_tDC772935855A7D8CFF1BF9CD8DB8696DCDAE19F1 * L_35 = ((  ConcreteBinderGeneric_1_tDC772935855A7D8CFF1BF9CD8DB8696DCDAE19F1 * (*) (ConcreteIdBinderGeneric_1_tAD404DF3AD6E1FDBC78FD89CAC0C982BC2B642A7 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4)->methodPointer)((ConcreteIdBinderGeneric_1_tAD404DF3AD6E1FDBC78FD89CAC0C982BC2B642A7 *)L_31, (RuntimeObject *)L_34, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4));
		NullCheck((ConcreteBinderGeneric_1_tDC772935855A7D8CFF1BF9CD8DB8696DCDAE19F1 *)L_35);
		FromBinderGeneric_1_tB3D3D4B24691B0AFF684D39C08F63B2193327187 * L_36 = ((  FromBinderGeneric_1_tB3D3D4B24691B0AFF684D39C08F63B2193327187 * (*) (ConcreteBinderGeneric_1_tDC772935855A7D8CFF1BF9CD8DB8696DCDAE19F1 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 5)->methodPointer)((ConcreteBinderGeneric_1_tDC772935855A7D8CFF1BF9CD8DB8696DCDAE19F1 *)L_35, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 5));
		SignalFromBinder_1_t862D9073BD2CB93CFFF7F75A6F7262CDF34AA1D7 * L_37 = (SignalFromBinder_1_t862D9073BD2CB93CFFF7F75A6F7262CDF34AA1D7 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 6));
		((  void (*) (SignalFromBinder_1_t862D9073BD2CB93CFFF7F75A6F7262CDF34AA1D7 *, BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *, FromBinderGeneric_1_tB3D3D4B24691B0AFF684D39C08F63B2193327187 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 7)->methodPointer)(L_37, (BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)L_28, (FromBinderGeneric_1_tB3D3D4B24691B0AFF684D39C08F63B2193327187 *)L_36, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 7));
		return L_37;
	}
}
// Zenject.SignalFromBinder`1<THandler> Zenject.SignalHandlerBinder`3<System.Object,System.Object,System.Object>::To<System.Object>(System.Action`4<THandler,TParam1,TParam2,TParam3>)
extern "C" IL2CPP_METHOD_ATTR SignalFromBinder_1_t862D9073BD2CB93CFFF7F75A6F7262CDF34AA1D7 * SignalHandlerBinder_3_To_TisRuntimeObject_m0886F343F5082F2EE0E27951D9F21C489608A929_gshared (SignalHandlerBinder_3_t9ADF0354A52CA5EFF23194E48F870113CF337055 * __this, Action_4_t46ACC504A4217F2E92EF2A827836993B819AF923 * ___method0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SignalHandlerBinder_3_To_TisRuntimeObject_m0886F343F5082F2EE0E27951D9F21C489608A929_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Guid_t  V_0;
	memset(&V_0, 0, sizeof(V_0));
	BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * V_1 = NULL;
	{
		BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 * L_0 = (BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 *)__this->get__finalizerWrapper_0();
		NullBindingFinalizer_tFEAF0122BA4AF8A5209DE0330F0BFF444F06AD65 * L_1 = (NullBindingFinalizer_tFEAF0122BA4AF8A5209DE0330F0BFF444F06AD65 *)il2cpp_codegen_object_new(NullBindingFinalizer_tFEAF0122BA4AF8A5209DE0330F0BFF444F06AD65_il2cpp_TypeInfo_var);
		NullBindingFinalizer__ctor_m2B90434471F862EF8E0C197230BDB1EAB586834A(L_1, /*hidden argument*/NULL);
		NullCheck((BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 *)L_0);
		BindFinalizerWrapper_set_SubFinalizer_m6AF16F9C3D2BE2F006E60D76D85F20EFE8C76874((BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 *)L_0, (RuntimeObject*)L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Guid_t_il2cpp_TypeInfo_var);
		Guid_t  L_2 = Guid_NewGuid_m541CAC23EBB140DFD3AB5B313315647E95FADB29(/*hidden argument*/NULL);
		V_0 = (Guid_t )L_2;
		DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * L_3 = (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)__this->get__container_2();
		TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* L_4 = (TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)SZArrayNew(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F_il2cpp_TypeInfo_var, (uint32_t)2);
		TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* L_5 = (TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)L_4;
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_6 = { reinterpret_cast<intptr_t> (IInitializable_t8CDB5239D7F1A24EC49AD4017F6D65D2A8A5F345_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_7 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6((RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D )L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_7);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_7);
		TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* L_8 = (TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)L_5;
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_9 = { reinterpret_cast<intptr_t> (IDisposable_t7218B22548186B208D65EA5B7870503810A2D15A_0_0_0_var) };
		Type_t * L_10 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6((RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D )L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_10);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_10);
		NullCheck((DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)L_3);
		ConcreteIdBinderNonGeneric_t372B43CB3CC50C44EF038F152C0F6345DFA22E24 * L_11 = DiContainer_Bind_m838139625D2905AE9B447C19F948EEE024380782((DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)L_3, (TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)L_8, /*hidden argument*/NULL);
		NullCheck((ConcreteBinderNonGeneric_t607DFF5BDEE16A8EF207F5D6148835CD108F72F9 *)L_11);
		FromBinderNonGeneric_t677B667DE367A055191AC7B64A622B8495C6CF89 * L_12 = ((  FromBinderNonGeneric_t677B667DE367A055191AC7B64A622B8495C6CF89 * (*) (ConcreteBinderNonGeneric_t607DFF5BDEE16A8EF207F5D6148835CD108F72F9 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((ConcreteBinderNonGeneric_t607DFF5BDEE16A8EF207F5D6148835CD108F72F9 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		NullCheck((ScopeArgConditionCopyNonLazyBinder_t218A003451367EEC6A439D63A0F38B964EF07B33 *)L_12);
		ArgConditionCopyNonLazyBinder_t5F1B4361E0805E20DDBEDEBC61B90CFFBB68B21B * L_13 = ScopeArgConditionCopyNonLazyBinder_AsCached_mAAD80E3CF24D5B46128725D1045BCA847A1205D2((ScopeArgConditionCopyNonLazyBinder_t218A003451367EEC6A439D63A0F38B964EF07B33 *)L_12, /*hidden argument*/NULL);
		Action_4_t46ACC504A4217F2E92EF2A827836993B819AF923 * L_14 = ___method0;
		DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * L_15 = (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)__this->get__container_2();
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_16 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 1)) };
		Type_t * L_17 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6((RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D )L_16, /*hidden argument*/NULL);
		Guid_t  L_18 = V_0;
		Guid_t  L_19 = L_18;
		RuntimeObject * L_20 = Box(Guid_t_il2cpp_TypeInfo_var, &L_19);
		InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * L_21 = (InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF *)il2cpp_codegen_object_new(InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF_il2cpp_TypeInfo_var);
		InjectContext__ctor_mEA73C2DE706F160699AA9C80A0171F62225C8F2E(L_21, (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)L_15, (Type_t *)L_17, (RuntimeObject *)L_20, /*hidden argument*/NULL);
		Type_t * L_22 = (Type_t *)__this->get__signalType_1();
		NullCheck((SignalHandlerBinder_3_t9ADF0354A52CA5EFF23194E48F870113CF337055 *)__this);
		RuntimeObject * L_23 = ((  RuntimeObject * (*) (SignalHandlerBinder_3_t9ADF0354A52CA5EFF23194E48F870113CF337055 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((SignalHandlerBinder_3_t9ADF0354A52CA5EFF23194E48F870113CF337055 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA * L_24 = (BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA *)il2cpp_codegen_object_new(BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA_il2cpp_TypeInfo_var);
		BindingId__ctor_mA092ED49B59C24FC8075ABB8C6070B8FEBFC19B1(L_24, (Type_t *)L_22, (RuntimeObject *)L_23, /*hidden argument*/NULL);
		NullCheck((ArgConditionCopyNonLazyBinder_t5F1B4361E0805E20DDBEDEBC61B90CFFBB68B21B *)L_13);
		((  ConditionCopyNonLazyBinder_tF7F37C5F1508AAB283151819E412C853A8D94B9B * (*) (ArgConditionCopyNonLazyBinder_t5F1B4361E0805E20DDBEDEBC61B90CFFBB68B21B *, Action_4_t46ACC504A4217F2E92EF2A827836993B819AF923 *, InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF *, BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)((ArgConditionCopyNonLazyBinder_t5F1B4361E0805E20DDBEDEBC61B90CFFBB68B21B *)L_13, (Action_4_t46ACC504A4217F2E92EF2A827836993B819AF923 *)L_14, (InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF *)L_21, (BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA *)L_24, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_25 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 1)) };
		Type_t * L_26 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6((RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D )L_25, /*hidden argument*/NULL);
		BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * L_27 = (BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)il2cpp_codegen_object_new(BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991_il2cpp_TypeInfo_var);
		BindInfo__ctor_m0653A2A5799A7A231939E22C2E728AFA4B006B6D(L_27, (Type_t *)L_26, /*hidden argument*/NULL);
		V_1 = (BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)L_27;
		BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * L_28 = V_1;
		DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * L_29 = (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)__this->get__container_2();
		BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * L_30 = V_1;
		NullCheck((DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)L_29);
		ConcreteIdBinderGeneric_1_tAD404DF3AD6E1FDBC78FD89CAC0C982BC2B642A7 * L_31 = ((  ConcreteIdBinderGeneric_1_tAD404DF3AD6E1FDBC78FD89CAC0C982BC2B642A7 * (*) (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *, BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3)->methodPointer)((DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)L_29, (BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3));
		Guid_t  L_32 = V_0;
		Guid_t  L_33 = L_32;
		RuntimeObject * L_34 = Box(Guid_t_il2cpp_TypeInfo_var, &L_33);
		NullCheck((ConcreteIdBinderGeneric_1_tAD404DF3AD6E1FDBC78FD89CAC0C982BC2B642A7 *)L_31);
		ConcreteBinderGeneric_1_tDC772935855A7D8CFF1BF9CD8DB8696DCDAE19F1 * L_35 = ((  ConcreteBinderGeneric_1_tDC772935855A7D8CFF1BF9CD8DB8696DCDAE19F1 * (*) (ConcreteIdBinderGeneric_1_tAD404DF3AD6E1FDBC78FD89CAC0C982BC2B642A7 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4)->methodPointer)((ConcreteIdBinderGeneric_1_tAD404DF3AD6E1FDBC78FD89CAC0C982BC2B642A7 *)L_31, (RuntimeObject *)L_34, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4));
		NullCheck((ConcreteBinderGeneric_1_tDC772935855A7D8CFF1BF9CD8DB8696DCDAE19F1 *)L_35);
		FromBinderGeneric_1_tB3D3D4B24691B0AFF684D39C08F63B2193327187 * L_36 = ((  FromBinderGeneric_1_tB3D3D4B24691B0AFF684D39C08F63B2193327187 * (*) (ConcreteBinderGeneric_1_tDC772935855A7D8CFF1BF9CD8DB8696DCDAE19F1 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 5)->methodPointer)((ConcreteBinderGeneric_1_tDC772935855A7D8CFF1BF9CD8DB8696DCDAE19F1 *)L_35, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 5));
		SignalFromBinder_1_t862D9073BD2CB93CFFF7F75A6F7262CDF34AA1D7 * L_37 = (SignalFromBinder_1_t862D9073BD2CB93CFFF7F75A6F7262CDF34AA1D7 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 6));
		((  void (*) (SignalFromBinder_1_t862D9073BD2CB93CFFF7F75A6F7262CDF34AA1D7 *, BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *, FromBinderGeneric_1_tB3D3D4B24691B0AFF684D39C08F63B2193327187 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 7)->methodPointer)(L_37, (BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)L_28, (FromBinderGeneric_1_tB3D3D4B24691B0AFF684D39C08F63B2193327187 *)L_36, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 7));
		return L_37;
	}
}
// Zenject.SignalFromBinder`1<THandler> Zenject.SignalHandlerBinder`3<System.Object,System.Object,System.Object>::To<System.Object>(System.Func`2<THandler,System.Action`3<TParam1,TParam2,TParam3>>)
extern "C" IL2CPP_METHOD_ATTR SignalFromBinder_1_t862D9073BD2CB93CFFF7F75A6F7262CDF34AA1D7 * SignalHandlerBinder_3_To_TisRuntimeObject_m237EC2847CEC91DE21A589DF099C732956AB2F6C_gshared (SignalHandlerBinder_3_t9ADF0354A52CA5EFF23194E48F870113CF337055 * __this, Func_2_t6D7E96A36A34348A9F5DA29E4FFE3BCEA4558CBC * ___methodGetter0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SignalHandlerBinder_3_To_TisRuntimeObject_m237EC2847CEC91DE21A589DF099C732956AB2F6C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Guid_t  V_0;
	memset(&V_0, 0, sizeof(V_0));
	BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * V_1 = NULL;
	{
		BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 * L_0 = (BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 *)__this->get__finalizerWrapper_0();
		NullBindingFinalizer_tFEAF0122BA4AF8A5209DE0330F0BFF444F06AD65 * L_1 = (NullBindingFinalizer_tFEAF0122BA4AF8A5209DE0330F0BFF444F06AD65 *)il2cpp_codegen_object_new(NullBindingFinalizer_tFEAF0122BA4AF8A5209DE0330F0BFF444F06AD65_il2cpp_TypeInfo_var);
		NullBindingFinalizer__ctor_m2B90434471F862EF8E0C197230BDB1EAB586834A(L_1, /*hidden argument*/NULL);
		NullCheck((BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 *)L_0);
		BindFinalizerWrapper_set_SubFinalizer_m6AF16F9C3D2BE2F006E60D76D85F20EFE8C76874((BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 *)L_0, (RuntimeObject*)L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Guid_t_il2cpp_TypeInfo_var);
		Guid_t  L_2 = Guid_NewGuid_m541CAC23EBB140DFD3AB5B313315647E95FADB29(/*hidden argument*/NULL);
		V_0 = (Guid_t )L_2;
		DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * L_3 = (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)__this->get__container_2();
		TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* L_4 = (TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)SZArrayNew(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F_il2cpp_TypeInfo_var, (uint32_t)2);
		TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* L_5 = (TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)L_4;
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_6 = { reinterpret_cast<intptr_t> (IInitializable_t8CDB5239D7F1A24EC49AD4017F6D65D2A8A5F345_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_7 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6((RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D )L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_7);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_7);
		TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* L_8 = (TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)L_5;
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_9 = { reinterpret_cast<intptr_t> (IDisposable_t7218B22548186B208D65EA5B7870503810A2D15A_0_0_0_var) };
		Type_t * L_10 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6((RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D )L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_10);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_10);
		NullCheck((DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)L_3);
		ConcreteIdBinderNonGeneric_t372B43CB3CC50C44EF038F152C0F6345DFA22E24 * L_11 = DiContainer_Bind_m838139625D2905AE9B447C19F948EEE024380782((DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)L_3, (TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)L_8, /*hidden argument*/NULL);
		NullCheck((ConcreteBinderNonGeneric_t607DFF5BDEE16A8EF207F5D6148835CD108F72F9 *)L_11);
		FromBinderNonGeneric_t677B667DE367A055191AC7B64A622B8495C6CF89 * L_12 = ((  FromBinderNonGeneric_t677B667DE367A055191AC7B64A622B8495C6CF89 * (*) (ConcreteBinderNonGeneric_t607DFF5BDEE16A8EF207F5D6148835CD108F72F9 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((ConcreteBinderNonGeneric_t607DFF5BDEE16A8EF207F5D6148835CD108F72F9 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		NullCheck((ScopeArgConditionCopyNonLazyBinder_t218A003451367EEC6A439D63A0F38B964EF07B33 *)L_12);
		ArgConditionCopyNonLazyBinder_t5F1B4361E0805E20DDBEDEBC61B90CFFBB68B21B * L_13 = ScopeArgConditionCopyNonLazyBinder_AsCached_mAAD80E3CF24D5B46128725D1045BCA847A1205D2((ScopeArgConditionCopyNonLazyBinder_t218A003451367EEC6A439D63A0F38B964EF07B33 *)L_12, /*hidden argument*/NULL);
		Func_2_t6D7E96A36A34348A9F5DA29E4FFE3BCEA4558CBC * L_14 = ___methodGetter0;
		DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * L_15 = (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)__this->get__container_2();
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_16 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 1)) };
		Type_t * L_17 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6((RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D )L_16, /*hidden argument*/NULL);
		Guid_t  L_18 = V_0;
		Guid_t  L_19 = L_18;
		RuntimeObject * L_20 = Box(Guid_t_il2cpp_TypeInfo_var, &L_19);
		InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * L_21 = (InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF *)il2cpp_codegen_object_new(InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF_il2cpp_TypeInfo_var);
		InjectContext__ctor_mEA73C2DE706F160699AA9C80A0171F62225C8F2E(L_21, (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)L_15, (Type_t *)L_17, (RuntimeObject *)L_20, /*hidden argument*/NULL);
		Type_t * L_22 = (Type_t *)__this->get__signalType_1();
		NullCheck((SignalHandlerBinder_3_t9ADF0354A52CA5EFF23194E48F870113CF337055 *)__this);
		RuntimeObject * L_23 = ((  RuntimeObject * (*) (SignalHandlerBinder_3_t9ADF0354A52CA5EFF23194E48F870113CF337055 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((SignalHandlerBinder_3_t9ADF0354A52CA5EFF23194E48F870113CF337055 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA * L_24 = (BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA *)il2cpp_codegen_object_new(BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA_il2cpp_TypeInfo_var);
		BindingId__ctor_mA092ED49B59C24FC8075ABB8C6070B8FEBFC19B1(L_24, (Type_t *)L_22, (RuntimeObject *)L_23, /*hidden argument*/NULL);
		NullCheck((ArgConditionCopyNonLazyBinder_t5F1B4361E0805E20DDBEDEBC61B90CFFBB68B21B *)L_13);
		((  ConditionCopyNonLazyBinder_tF7F37C5F1508AAB283151819E412C853A8D94B9B * (*) (ArgConditionCopyNonLazyBinder_t5F1B4361E0805E20DDBEDEBC61B90CFFBB68B21B *, Func_2_t6D7E96A36A34348A9F5DA29E4FFE3BCEA4558CBC *, InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF *, BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)((ArgConditionCopyNonLazyBinder_t5F1B4361E0805E20DDBEDEBC61B90CFFBB68B21B *)L_13, (Func_2_t6D7E96A36A34348A9F5DA29E4FFE3BCEA4558CBC *)L_14, (InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF *)L_21, (BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA *)L_24, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_25 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 1)) };
		Type_t * L_26 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6((RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D )L_25, /*hidden argument*/NULL);
		BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * L_27 = (BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)il2cpp_codegen_object_new(BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991_il2cpp_TypeInfo_var);
		BindInfo__ctor_m0653A2A5799A7A231939E22C2E728AFA4B006B6D(L_27, (Type_t *)L_26, /*hidden argument*/NULL);
		V_1 = (BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)L_27;
		BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * L_28 = V_1;
		DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * L_29 = (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)__this->get__container_2();
		BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * L_30 = V_1;
		NullCheck((DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)L_29);
		ConcreteIdBinderGeneric_1_tAD404DF3AD6E1FDBC78FD89CAC0C982BC2B642A7 * L_31 = ((  ConcreteIdBinderGeneric_1_tAD404DF3AD6E1FDBC78FD89CAC0C982BC2B642A7 * (*) (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *, BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3)->methodPointer)((DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)L_29, (BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3));
		Guid_t  L_32 = V_0;
		Guid_t  L_33 = L_32;
		RuntimeObject * L_34 = Box(Guid_t_il2cpp_TypeInfo_var, &L_33);
		NullCheck((ConcreteIdBinderGeneric_1_tAD404DF3AD6E1FDBC78FD89CAC0C982BC2B642A7 *)L_31);
		ConcreteBinderGeneric_1_tDC772935855A7D8CFF1BF9CD8DB8696DCDAE19F1 * L_35 = ((  ConcreteBinderGeneric_1_tDC772935855A7D8CFF1BF9CD8DB8696DCDAE19F1 * (*) (ConcreteIdBinderGeneric_1_tAD404DF3AD6E1FDBC78FD89CAC0C982BC2B642A7 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4)->methodPointer)((ConcreteIdBinderGeneric_1_tAD404DF3AD6E1FDBC78FD89CAC0C982BC2B642A7 *)L_31, (RuntimeObject *)L_34, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4));
		NullCheck((ConcreteBinderGeneric_1_tDC772935855A7D8CFF1BF9CD8DB8696DCDAE19F1 *)L_35);
		FromBinderGeneric_1_tB3D3D4B24691B0AFF684D39C08F63B2193327187 * L_36 = ((  FromBinderGeneric_1_tB3D3D4B24691B0AFF684D39C08F63B2193327187 * (*) (ConcreteBinderGeneric_1_tDC772935855A7D8CFF1BF9CD8DB8696DCDAE19F1 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 5)->methodPointer)((ConcreteBinderGeneric_1_tDC772935855A7D8CFF1BF9CD8DB8696DCDAE19F1 *)L_35, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 5));
		SignalFromBinder_1_t862D9073BD2CB93CFFF7F75A6F7262CDF34AA1D7 * L_37 = (SignalFromBinder_1_t862D9073BD2CB93CFFF7F75A6F7262CDF34AA1D7 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 6));
		((  void (*) (SignalFromBinder_1_t862D9073BD2CB93CFFF7F75A6F7262CDF34AA1D7 *, BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *, FromBinderGeneric_1_tB3D3D4B24691B0AFF684D39C08F63B2193327187 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 7)->methodPointer)(L_37, (BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)L_28, (FromBinderGeneric_1_tB3D3D4B24691B0AFF684D39C08F63B2193327187 *)L_36, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 7));
		return L_37;
	}
}
// Zenject.SignalFromBinder`1<THandler> Zenject.SignalHandlerBinder`4<System.Object,System.Object,System.Object,System.Object>::To<System.Object>(ModestTree.Util.Action`5<THandler,TParam1,TParam2,TParam3,TParam4>)
extern "C" IL2CPP_METHOD_ATTR SignalFromBinder_1_t862D9073BD2CB93CFFF7F75A6F7262CDF34AA1D7 * SignalHandlerBinder_4_To_TisRuntimeObject_m88CDE60E94639D3111738874458A215E623C06FB_gshared (SignalHandlerBinder_4_t6088EB5507EA2460DDEA35BEFBCE49C3F0F851DC * __this, Action_5_tA3F3461C4A9081A2000D185A35A46E23F4E4338F * ___method0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SignalHandlerBinder_4_To_TisRuntimeObject_m88CDE60E94639D3111738874458A215E623C06FB_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Guid_t  V_0;
	memset(&V_0, 0, sizeof(V_0));
	BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * V_1 = NULL;
	{
		BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 * L_0 = (BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 *)__this->get__finalizerWrapper_0();
		NullBindingFinalizer_tFEAF0122BA4AF8A5209DE0330F0BFF444F06AD65 * L_1 = (NullBindingFinalizer_tFEAF0122BA4AF8A5209DE0330F0BFF444F06AD65 *)il2cpp_codegen_object_new(NullBindingFinalizer_tFEAF0122BA4AF8A5209DE0330F0BFF444F06AD65_il2cpp_TypeInfo_var);
		NullBindingFinalizer__ctor_m2B90434471F862EF8E0C197230BDB1EAB586834A(L_1, /*hidden argument*/NULL);
		NullCheck((BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 *)L_0);
		BindFinalizerWrapper_set_SubFinalizer_m6AF16F9C3D2BE2F006E60D76D85F20EFE8C76874((BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 *)L_0, (RuntimeObject*)L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Guid_t_il2cpp_TypeInfo_var);
		Guid_t  L_2 = Guid_NewGuid_m541CAC23EBB140DFD3AB5B313315647E95FADB29(/*hidden argument*/NULL);
		V_0 = (Guid_t )L_2;
		DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * L_3 = (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)__this->get__container_2();
		TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* L_4 = (TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)SZArrayNew(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F_il2cpp_TypeInfo_var, (uint32_t)2);
		TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* L_5 = (TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)L_4;
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_6 = { reinterpret_cast<intptr_t> (IInitializable_t8CDB5239D7F1A24EC49AD4017F6D65D2A8A5F345_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_7 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6((RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D )L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_7);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_7);
		TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* L_8 = (TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)L_5;
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_9 = { reinterpret_cast<intptr_t> (IDisposable_t7218B22548186B208D65EA5B7870503810A2D15A_0_0_0_var) };
		Type_t * L_10 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6((RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D )L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_10);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_10);
		NullCheck((DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)L_3);
		ConcreteIdBinderNonGeneric_t372B43CB3CC50C44EF038F152C0F6345DFA22E24 * L_11 = DiContainer_Bind_m838139625D2905AE9B447C19F948EEE024380782((DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)L_3, (TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)L_8, /*hidden argument*/NULL);
		NullCheck((ConcreteBinderNonGeneric_t607DFF5BDEE16A8EF207F5D6148835CD108F72F9 *)L_11);
		FromBinderNonGeneric_t677B667DE367A055191AC7B64A622B8495C6CF89 * L_12 = ((  FromBinderNonGeneric_t677B667DE367A055191AC7B64A622B8495C6CF89 * (*) (ConcreteBinderNonGeneric_t607DFF5BDEE16A8EF207F5D6148835CD108F72F9 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((ConcreteBinderNonGeneric_t607DFF5BDEE16A8EF207F5D6148835CD108F72F9 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		NullCheck((ScopeArgConditionCopyNonLazyBinder_t218A003451367EEC6A439D63A0F38B964EF07B33 *)L_12);
		ArgConditionCopyNonLazyBinder_t5F1B4361E0805E20DDBEDEBC61B90CFFBB68B21B * L_13 = ScopeArgConditionCopyNonLazyBinder_AsCached_mAAD80E3CF24D5B46128725D1045BCA847A1205D2((ScopeArgConditionCopyNonLazyBinder_t218A003451367EEC6A439D63A0F38B964EF07B33 *)L_12, /*hidden argument*/NULL);
		Action_5_tA3F3461C4A9081A2000D185A35A46E23F4E4338F * L_14 = ___method0;
		DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * L_15 = (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)__this->get__container_2();
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_16 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 1)) };
		Type_t * L_17 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6((RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D )L_16, /*hidden argument*/NULL);
		Guid_t  L_18 = V_0;
		Guid_t  L_19 = L_18;
		RuntimeObject * L_20 = Box(Guid_t_il2cpp_TypeInfo_var, &L_19);
		InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * L_21 = (InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF *)il2cpp_codegen_object_new(InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF_il2cpp_TypeInfo_var);
		InjectContext__ctor_mEA73C2DE706F160699AA9C80A0171F62225C8F2E(L_21, (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)L_15, (Type_t *)L_17, (RuntimeObject *)L_20, /*hidden argument*/NULL);
		Type_t * L_22 = (Type_t *)__this->get__signalType_1();
		NullCheck((SignalHandlerBinder_4_t6088EB5507EA2460DDEA35BEFBCE49C3F0F851DC *)__this);
		RuntimeObject * L_23 = ((  RuntimeObject * (*) (SignalHandlerBinder_4_t6088EB5507EA2460DDEA35BEFBCE49C3F0F851DC *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((SignalHandlerBinder_4_t6088EB5507EA2460DDEA35BEFBCE49C3F0F851DC *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA * L_24 = (BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA *)il2cpp_codegen_object_new(BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA_il2cpp_TypeInfo_var);
		BindingId__ctor_mA092ED49B59C24FC8075ABB8C6070B8FEBFC19B1(L_24, (Type_t *)L_22, (RuntimeObject *)L_23, /*hidden argument*/NULL);
		NullCheck((ArgConditionCopyNonLazyBinder_t5F1B4361E0805E20DDBEDEBC61B90CFFBB68B21B *)L_13);
		((  ConditionCopyNonLazyBinder_tF7F37C5F1508AAB283151819E412C853A8D94B9B * (*) (ArgConditionCopyNonLazyBinder_t5F1B4361E0805E20DDBEDEBC61B90CFFBB68B21B *, Action_5_tA3F3461C4A9081A2000D185A35A46E23F4E4338F *, InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF *, BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)((ArgConditionCopyNonLazyBinder_t5F1B4361E0805E20DDBEDEBC61B90CFFBB68B21B *)L_13, (Action_5_tA3F3461C4A9081A2000D185A35A46E23F4E4338F *)L_14, (InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF *)L_21, (BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA *)L_24, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_25 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 1)) };
		Type_t * L_26 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6((RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D )L_25, /*hidden argument*/NULL);
		BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * L_27 = (BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)il2cpp_codegen_object_new(BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991_il2cpp_TypeInfo_var);
		BindInfo__ctor_m0653A2A5799A7A231939E22C2E728AFA4B006B6D(L_27, (Type_t *)L_26, /*hidden argument*/NULL);
		V_1 = (BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)L_27;
		BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * L_28 = V_1;
		DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * L_29 = (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)__this->get__container_2();
		BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * L_30 = V_1;
		NullCheck((DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)L_29);
		ConcreteIdBinderGeneric_1_tAD404DF3AD6E1FDBC78FD89CAC0C982BC2B642A7 * L_31 = ((  ConcreteIdBinderGeneric_1_tAD404DF3AD6E1FDBC78FD89CAC0C982BC2B642A7 * (*) (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *, BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3)->methodPointer)((DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)L_29, (BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3));
		Guid_t  L_32 = V_0;
		Guid_t  L_33 = L_32;
		RuntimeObject * L_34 = Box(Guid_t_il2cpp_TypeInfo_var, &L_33);
		NullCheck((ConcreteIdBinderGeneric_1_tAD404DF3AD6E1FDBC78FD89CAC0C982BC2B642A7 *)L_31);
		ConcreteBinderGeneric_1_tDC772935855A7D8CFF1BF9CD8DB8696DCDAE19F1 * L_35 = ((  ConcreteBinderGeneric_1_tDC772935855A7D8CFF1BF9CD8DB8696DCDAE19F1 * (*) (ConcreteIdBinderGeneric_1_tAD404DF3AD6E1FDBC78FD89CAC0C982BC2B642A7 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4)->methodPointer)((ConcreteIdBinderGeneric_1_tAD404DF3AD6E1FDBC78FD89CAC0C982BC2B642A7 *)L_31, (RuntimeObject *)L_34, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4));
		NullCheck((ConcreteBinderGeneric_1_tDC772935855A7D8CFF1BF9CD8DB8696DCDAE19F1 *)L_35);
		FromBinderGeneric_1_tB3D3D4B24691B0AFF684D39C08F63B2193327187 * L_36 = ((  FromBinderGeneric_1_tB3D3D4B24691B0AFF684D39C08F63B2193327187 * (*) (ConcreteBinderGeneric_1_tDC772935855A7D8CFF1BF9CD8DB8696DCDAE19F1 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 5)->methodPointer)((ConcreteBinderGeneric_1_tDC772935855A7D8CFF1BF9CD8DB8696DCDAE19F1 *)L_35, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 5));
		SignalFromBinder_1_t862D9073BD2CB93CFFF7F75A6F7262CDF34AA1D7 * L_37 = (SignalFromBinder_1_t862D9073BD2CB93CFFF7F75A6F7262CDF34AA1D7 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 6));
		((  void (*) (SignalFromBinder_1_t862D9073BD2CB93CFFF7F75A6F7262CDF34AA1D7 *, BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *, FromBinderGeneric_1_tB3D3D4B24691B0AFF684D39C08F63B2193327187 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 7)->methodPointer)(L_37, (BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)L_28, (FromBinderGeneric_1_tB3D3D4B24691B0AFF684D39C08F63B2193327187 *)L_36, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 7));
		return L_37;
	}
}
// Zenject.SignalFromBinder`1<THandler> Zenject.SignalHandlerBinder`4<System.Object,System.Object,System.Object,System.Object>::To<System.Object>(System.Func`2<THandler,System.Action`4<TParam1,TParam2,TParam3,TParam4>>)
extern "C" IL2CPP_METHOD_ATTR SignalFromBinder_1_t862D9073BD2CB93CFFF7F75A6F7262CDF34AA1D7 * SignalHandlerBinder_4_To_TisRuntimeObject_m2AE099BAC1852E16FEFAEB81D2DFCB05DAA3A059_gshared (SignalHandlerBinder_4_t6088EB5507EA2460DDEA35BEFBCE49C3F0F851DC * __this, Func_2_t501D8CF44938B0EB347A4669809ED06E577C2D3C * ___methodGetter0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SignalHandlerBinder_4_To_TisRuntimeObject_m2AE099BAC1852E16FEFAEB81D2DFCB05DAA3A059_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Guid_t  V_0;
	memset(&V_0, 0, sizeof(V_0));
	BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * V_1 = NULL;
	{
		BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 * L_0 = (BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 *)__this->get__finalizerWrapper_0();
		NullBindingFinalizer_tFEAF0122BA4AF8A5209DE0330F0BFF444F06AD65 * L_1 = (NullBindingFinalizer_tFEAF0122BA4AF8A5209DE0330F0BFF444F06AD65 *)il2cpp_codegen_object_new(NullBindingFinalizer_tFEAF0122BA4AF8A5209DE0330F0BFF444F06AD65_il2cpp_TypeInfo_var);
		NullBindingFinalizer__ctor_m2B90434471F862EF8E0C197230BDB1EAB586834A(L_1, /*hidden argument*/NULL);
		NullCheck((BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 *)L_0);
		BindFinalizerWrapper_set_SubFinalizer_m6AF16F9C3D2BE2F006E60D76D85F20EFE8C76874((BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 *)L_0, (RuntimeObject*)L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Guid_t_il2cpp_TypeInfo_var);
		Guid_t  L_2 = Guid_NewGuid_m541CAC23EBB140DFD3AB5B313315647E95FADB29(/*hidden argument*/NULL);
		V_0 = (Guid_t )L_2;
		DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * L_3 = (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)__this->get__container_2();
		TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* L_4 = (TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)SZArrayNew(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F_il2cpp_TypeInfo_var, (uint32_t)2);
		TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* L_5 = (TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)L_4;
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_6 = { reinterpret_cast<intptr_t> (IInitializable_t8CDB5239D7F1A24EC49AD4017F6D65D2A8A5F345_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_7 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6((RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D )L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_7);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_7);
		TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* L_8 = (TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)L_5;
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_9 = { reinterpret_cast<intptr_t> (IDisposable_t7218B22548186B208D65EA5B7870503810A2D15A_0_0_0_var) };
		Type_t * L_10 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6((RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D )L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_10);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_10);
		NullCheck((DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)L_3);
		ConcreteIdBinderNonGeneric_t372B43CB3CC50C44EF038F152C0F6345DFA22E24 * L_11 = DiContainer_Bind_m838139625D2905AE9B447C19F948EEE024380782((DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)L_3, (TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)L_8, /*hidden argument*/NULL);
		NullCheck((ConcreteBinderNonGeneric_t607DFF5BDEE16A8EF207F5D6148835CD108F72F9 *)L_11);
		FromBinderNonGeneric_t677B667DE367A055191AC7B64A622B8495C6CF89 * L_12 = ((  FromBinderNonGeneric_t677B667DE367A055191AC7B64A622B8495C6CF89 * (*) (ConcreteBinderNonGeneric_t607DFF5BDEE16A8EF207F5D6148835CD108F72F9 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((ConcreteBinderNonGeneric_t607DFF5BDEE16A8EF207F5D6148835CD108F72F9 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		NullCheck((ScopeArgConditionCopyNonLazyBinder_t218A003451367EEC6A439D63A0F38B964EF07B33 *)L_12);
		ArgConditionCopyNonLazyBinder_t5F1B4361E0805E20DDBEDEBC61B90CFFBB68B21B * L_13 = ScopeArgConditionCopyNonLazyBinder_AsCached_mAAD80E3CF24D5B46128725D1045BCA847A1205D2((ScopeArgConditionCopyNonLazyBinder_t218A003451367EEC6A439D63A0F38B964EF07B33 *)L_12, /*hidden argument*/NULL);
		Func_2_t501D8CF44938B0EB347A4669809ED06E577C2D3C * L_14 = ___methodGetter0;
		DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * L_15 = (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)__this->get__container_2();
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_16 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 1)) };
		Type_t * L_17 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6((RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D )L_16, /*hidden argument*/NULL);
		Guid_t  L_18 = V_0;
		Guid_t  L_19 = L_18;
		RuntimeObject * L_20 = Box(Guid_t_il2cpp_TypeInfo_var, &L_19);
		InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * L_21 = (InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF *)il2cpp_codegen_object_new(InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF_il2cpp_TypeInfo_var);
		InjectContext__ctor_mEA73C2DE706F160699AA9C80A0171F62225C8F2E(L_21, (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)L_15, (Type_t *)L_17, (RuntimeObject *)L_20, /*hidden argument*/NULL);
		Type_t * L_22 = (Type_t *)__this->get__signalType_1();
		NullCheck((SignalHandlerBinder_4_t6088EB5507EA2460DDEA35BEFBCE49C3F0F851DC *)__this);
		RuntimeObject * L_23 = ((  RuntimeObject * (*) (SignalHandlerBinder_4_t6088EB5507EA2460DDEA35BEFBCE49C3F0F851DC *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((SignalHandlerBinder_4_t6088EB5507EA2460DDEA35BEFBCE49C3F0F851DC *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA * L_24 = (BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA *)il2cpp_codegen_object_new(BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA_il2cpp_TypeInfo_var);
		BindingId__ctor_mA092ED49B59C24FC8075ABB8C6070B8FEBFC19B1(L_24, (Type_t *)L_22, (RuntimeObject *)L_23, /*hidden argument*/NULL);
		NullCheck((ArgConditionCopyNonLazyBinder_t5F1B4361E0805E20DDBEDEBC61B90CFFBB68B21B *)L_13);
		((  ConditionCopyNonLazyBinder_tF7F37C5F1508AAB283151819E412C853A8D94B9B * (*) (ArgConditionCopyNonLazyBinder_t5F1B4361E0805E20DDBEDEBC61B90CFFBB68B21B *, Func_2_t501D8CF44938B0EB347A4669809ED06E577C2D3C *, InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF *, BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)((ArgConditionCopyNonLazyBinder_t5F1B4361E0805E20DDBEDEBC61B90CFFBB68B21B *)L_13, (Func_2_t501D8CF44938B0EB347A4669809ED06E577C2D3C *)L_14, (InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF *)L_21, (BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA *)L_24, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_25 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 1)) };
		Type_t * L_26 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6((RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D )L_25, /*hidden argument*/NULL);
		BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * L_27 = (BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)il2cpp_codegen_object_new(BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991_il2cpp_TypeInfo_var);
		BindInfo__ctor_m0653A2A5799A7A231939E22C2E728AFA4B006B6D(L_27, (Type_t *)L_26, /*hidden argument*/NULL);
		V_1 = (BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)L_27;
		BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * L_28 = V_1;
		DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * L_29 = (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)__this->get__container_2();
		BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * L_30 = V_1;
		NullCheck((DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)L_29);
		ConcreteIdBinderGeneric_1_tAD404DF3AD6E1FDBC78FD89CAC0C982BC2B642A7 * L_31 = ((  ConcreteIdBinderGeneric_1_tAD404DF3AD6E1FDBC78FD89CAC0C982BC2B642A7 * (*) (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *, BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3)->methodPointer)((DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)L_29, (BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3));
		Guid_t  L_32 = V_0;
		Guid_t  L_33 = L_32;
		RuntimeObject * L_34 = Box(Guid_t_il2cpp_TypeInfo_var, &L_33);
		NullCheck((ConcreteIdBinderGeneric_1_tAD404DF3AD6E1FDBC78FD89CAC0C982BC2B642A7 *)L_31);
		ConcreteBinderGeneric_1_tDC772935855A7D8CFF1BF9CD8DB8696DCDAE19F1 * L_35 = ((  ConcreteBinderGeneric_1_tDC772935855A7D8CFF1BF9CD8DB8696DCDAE19F1 * (*) (ConcreteIdBinderGeneric_1_tAD404DF3AD6E1FDBC78FD89CAC0C982BC2B642A7 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4)->methodPointer)((ConcreteIdBinderGeneric_1_tAD404DF3AD6E1FDBC78FD89CAC0C982BC2B642A7 *)L_31, (RuntimeObject *)L_34, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4));
		NullCheck((ConcreteBinderGeneric_1_tDC772935855A7D8CFF1BF9CD8DB8696DCDAE19F1 *)L_35);
		FromBinderGeneric_1_tB3D3D4B24691B0AFF684D39C08F63B2193327187 * L_36 = ((  FromBinderGeneric_1_tB3D3D4B24691B0AFF684D39C08F63B2193327187 * (*) (ConcreteBinderGeneric_1_tDC772935855A7D8CFF1BF9CD8DB8696DCDAE19F1 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 5)->methodPointer)((ConcreteBinderGeneric_1_tDC772935855A7D8CFF1BF9CD8DB8696DCDAE19F1 *)L_35, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 5));
		SignalFromBinder_1_t862D9073BD2CB93CFFF7F75A6F7262CDF34AA1D7 * L_37 = (SignalFromBinder_1_t862D9073BD2CB93CFFF7F75A6F7262CDF34AA1D7 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 6));
		((  void (*) (SignalFromBinder_1_t862D9073BD2CB93CFFF7F75A6F7262CDF34AA1D7 *, BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *, FromBinderGeneric_1_tB3D3D4B24691B0AFF684D39C08F63B2193327187 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 7)->methodPointer)(L_37, (BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 *)L_28, (FromBinderGeneric_1_tB3D3D4B24691B0AFF684D39C08F63B2193327187 *)L_36, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 7));
		return L_37;
	}
}
// Zenject.SignalHandlerBinderWithId Zenject.SignalExtensions::BindSignal<System.Object>(Zenject.DiContainer)
extern "C" IL2CPP_METHOD_ATTR SignalHandlerBinderWithId_tF2F486B2BBE4565DB9C112C9D5755BDCFE7BCD27 * SignalExtensions_BindSignal_TisRuntimeObject_m2F8E2FCCF499410D18C110D3A9C0350D2B3AE69D_gshared (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ___container0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SignalExtensions_BindSignal_TisRuntimeObject_m2F8E2FCCF499410D18C110D3A9C0350D2B3AE69D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 * V_0 = NULL;
	{
		DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * L_0 = ___container0;
		NullCheck((DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)L_0);
		BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 * L_1 = DiContainer_StartBinding_mC62BEAE4A89F0380BDCA1FF4747BD12D15CC2C2E((DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)L_0, /*hidden argument*/NULL);
		V_0 = (BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 *)L_1;
		DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * L_2 = ___container0;
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_3 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6((RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D )L_3, /*hidden argument*/NULL);
		BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 * L_5 = V_0;
		SignalHandlerBinderWithId_tF2F486B2BBE4565DB9C112C9D5755BDCFE7BCD27 * L_6 = (SignalHandlerBinderWithId_tF2F486B2BBE4565DB9C112C9D5755BDCFE7BCD27 *)il2cpp_codegen_object_new(SignalHandlerBinderWithId_tF2F486B2BBE4565DB9C112C9D5755BDCFE7BCD27_il2cpp_TypeInfo_var);
		SignalHandlerBinderWithId__ctor_m32ECF91BFE03FBEAB9069263E745A645225396CE(L_6, (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)L_2, (Type_t *)L_4, (BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 *)L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// Zenject.SignalHandlerBinderWithId`1<TParam1> Zenject.SignalExtensions::BindSignal<System.Object,System.Object>(Zenject.DiContainer)
extern "C" IL2CPP_METHOD_ATTR SignalHandlerBinderWithId_1_t7C2451301E4286C80D2E3AA5B7D4A77DBC08DBFE * SignalExtensions_BindSignal_TisRuntimeObject_TisRuntimeObject_mA30903AF0FAE5039370DEB54BB3A9D6BD2B65382_gshared (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ___container0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SignalExtensions_BindSignal_TisRuntimeObject_TisRuntimeObject_mA30903AF0FAE5039370DEB54BB3A9D6BD2B65382_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 * V_0 = NULL;
	{
		DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * L_0 = ___container0;
		NullCheck((DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)L_0);
		BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 * L_1 = DiContainer_StartBinding_mC62BEAE4A89F0380BDCA1FF4747BD12D15CC2C2E((DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)L_0, /*hidden argument*/NULL);
		V_0 = (BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 *)L_1;
		DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * L_2 = ___container0;
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_3 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6((RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D )L_3, /*hidden argument*/NULL);
		BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 * L_5 = V_0;
		SignalHandlerBinderWithId_1_t7C2451301E4286C80D2E3AA5B7D4A77DBC08DBFE * L_6 = (SignalHandlerBinderWithId_1_t7C2451301E4286C80D2E3AA5B7D4A77DBC08DBFE *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 1));
		((  void (*) (SignalHandlerBinderWithId_1_t7C2451301E4286C80D2E3AA5B7D4A77DBC08DBFE *, DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *, Type_t *, BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)(L_6, (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)L_2, (Type_t *)L_4, (BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		return L_6;
	}
}
// Zenject.SignalHandlerBinderWithId`2<TParam1,TParam2> Zenject.SignalExtensions::BindSignal<System.Object,System.Object,System.Object>(Zenject.DiContainer)
extern "C" IL2CPP_METHOD_ATTR SignalHandlerBinderWithId_2_t5075E83F01DA88E7F92BC36E706802EC87425C9D * SignalExtensions_BindSignal_TisRuntimeObject_TisRuntimeObject_TisRuntimeObject_mD07A9C35647F9E5133B90059FCB1003CDF011D05_gshared (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ___container0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SignalExtensions_BindSignal_TisRuntimeObject_TisRuntimeObject_TisRuntimeObject_mD07A9C35647F9E5133B90059FCB1003CDF011D05_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 * V_0 = NULL;
	{
		DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * L_0 = ___container0;
		NullCheck((DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)L_0);
		BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 * L_1 = DiContainer_StartBinding_mC62BEAE4A89F0380BDCA1FF4747BD12D15CC2C2E((DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)L_0, /*hidden argument*/NULL);
		V_0 = (BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 *)L_1;
		DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * L_2 = ___container0;
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_3 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6((RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D )L_3, /*hidden argument*/NULL);
		BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 * L_5 = V_0;
		SignalHandlerBinderWithId_2_t5075E83F01DA88E7F92BC36E706802EC87425C9D * L_6 = (SignalHandlerBinderWithId_2_t5075E83F01DA88E7F92BC36E706802EC87425C9D *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 1));
		((  void (*) (SignalHandlerBinderWithId_2_t5075E83F01DA88E7F92BC36E706802EC87425C9D *, DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *, Type_t *, BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)(L_6, (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)L_2, (Type_t *)L_4, (BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		return L_6;
	}
}
// Zenject.SignalHandlerBinderWithId`3<TParam1,TParam2,TParam3> Zenject.SignalExtensions::BindSignal<System.Object,System.Object,System.Object,System.Object>(Zenject.DiContainer)
extern "C" IL2CPP_METHOD_ATTR SignalHandlerBinderWithId_3_t76D4499350CECBF7B17E476217D26420F3E8D532 * SignalExtensions_BindSignal_TisRuntimeObject_TisRuntimeObject_TisRuntimeObject_TisRuntimeObject_mCD45D46A7F09666035E38FA09D85AC2036120CEE_gshared (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ___container0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SignalExtensions_BindSignal_TisRuntimeObject_TisRuntimeObject_TisRuntimeObject_TisRuntimeObject_mCD45D46A7F09666035E38FA09D85AC2036120CEE_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 * V_0 = NULL;
	{
		DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * L_0 = ___container0;
		NullCheck((DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)L_0);
		BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 * L_1 = DiContainer_StartBinding_mC62BEAE4A89F0380BDCA1FF4747BD12D15CC2C2E((DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)L_0, /*hidden argument*/NULL);
		V_0 = (BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 *)L_1;
		DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * L_2 = ___container0;
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_3 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6((RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D )L_3, /*hidden argument*/NULL);
		BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 * L_5 = V_0;
		SignalHandlerBinderWithId_3_t76D4499350CECBF7B17E476217D26420F3E8D532 * L_6 = (SignalHandlerBinderWithId_3_t76D4499350CECBF7B17E476217D26420F3E8D532 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 1));
		((  void (*) (SignalHandlerBinderWithId_3_t76D4499350CECBF7B17E476217D26420F3E8D532 *, DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *, Type_t *, BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)(L_6, (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)L_2, (Type_t *)L_4, (BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		return L_6;
	}
}
// Zenject.SignalHandlerBinderWithId`4<TParam1,TParam2,TParam3,TParam4> Zenject.SignalExtensions::BindSignal<System.Object,System.Object,System.Object,System.Object,System.Object>(Zenject.DiContainer)
extern "C" IL2CPP_METHOD_ATTR SignalHandlerBinderWithId_4_t36F180155CE8C6D813408F9B2086613D07CF41C9 * SignalExtensions_BindSignal_TisRuntimeObject_TisRuntimeObject_TisRuntimeObject_TisRuntimeObject_TisRuntimeObject_mF00BBB207286CA8A011B5A6CC8CCF5646AF94679_gshared (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ___container0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SignalExtensions_BindSignal_TisRuntimeObject_TisRuntimeObject_TisRuntimeObject_TisRuntimeObject_TisRuntimeObject_mF00BBB207286CA8A011B5A6CC8CCF5646AF94679_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 * V_0 = NULL;
	{
		DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * L_0 = ___container0;
		NullCheck((DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)L_0);
		BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 * L_1 = DiContainer_StartBinding_mC62BEAE4A89F0380BDCA1FF4747BD12D15CC2C2E((DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)L_0, /*hidden argument*/NULL);
		V_0 = (BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 *)L_1;
		DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * L_2 = ___container0;
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_3 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6((RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D )L_3, /*hidden argument*/NULL);
		BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 * L_5 = V_0;
		SignalHandlerBinderWithId_4_t36F180155CE8C6D813408F9B2086613D07CF41C9 * L_6 = (SignalHandlerBinderWithId_4_t36F180155CE8C6D813408F9B2086613D07CF41C9 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 1));
		((  void (*) (SignalHandlerBinderWithId_4_t36F180155CE8C6D813408F9B2086613D07CF41C9 *, DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *, Type_t *, BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)(L_6, (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 *)L_2, (Type_t *)L_4, (BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		return L_6;
	}
}
// Zenject.TypeValuePair Zenject.InjectUtil::CreateTypePair<System.Object>(T)
extern "C" IL2CPP_METHOD_ATTR TypeValuePair_tD309CDC658476FDDF08345C92D3F4D7A0A1F9792 * InjectUtil_CreateTypePair_TisRuntimeObject_m3FC0D38A137237CADAA24DEC0530CDE1C30B2B4B_gshared (RuntimeObject * ___param0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InjectUtil_CreateTypePair_TisRuntimeObject_m3FC0D38A137237CADAA24DEC0530CDE1C30B2B4B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * G_B3_0 = NULL;
	{
		RuntimeObject * L_0 = ___param0;
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		NullCheck((RuntimeObject *)(___param0));
		Type_t * L_1 = Object_GetType_m2E0B62414ECCAA3094B703790CE88CBB2F83EA60((RuntimeObject *)(___param0), /*hidden argument*/NULL);
		G_B3_0 = L_1;
		goto IL_0021;
	}

IL_0017:
	{
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_2 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 1)) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6((RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D )L_2, /*hidden argument*/NULL);
		G_B3_0 = L_3;
	}

IL_0021:
	{
		RuntimeObject * L_4 = ___param0;
		TypeValuePair_tD309CDC658476FDDF08345C92D3F4D7A0A1F9792 * L_5 = (TypeValuePair_tD309CDC658476FDDF08345C92D3F4D7A0A1F9792 *)il2cpp_codegen_object_new(TypeValuePair_tD309CDC658476FDDF08345C92D3F4D7A0A1F9792_il2cpp_TypeInfo_var);
		TypeValuePair__ctor_m2129895C0D465FFE1E5DE30CECE2EF01A3185860(L_5, (Type_t *)G_B3_0, (RuntimeObject *)L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// Zenject.ZenjectTypeInfo Zenject.TypeAnalyzer::GetInfo<System.Object>()
extern "C" IL2CPP_METHOD_ATTR ZenjectTypeInfo_t2866A662C0FDA5B357BCC135021933EE8914535E * TypeAnalyzer_GetInfo_TisRuntimeObject_m41DD5D8E47571BC5CDEF9DDA899B887F340D0D3F_gshared (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypeAnalyzer_GetInfo_TisRuntimeObject_m41DD5D8E47571BC5CDEF9DDA899B887F340D0D3F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_0 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6((RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D )L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(TypeAnalyzer_t19EB8063B9BA23AD8FAAB2AF5228A8D8D086F722_il2cpp_TypeInfo_var);
		ZenjectTypeInfo_t2866A662C0FDA5B357BCC135021933EE8914535E * L_2 = TypeAnalyzer_GetInfo_m7D98EB86402655EC572CBFA64ADC04E199C88CF4((Type_t *)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
