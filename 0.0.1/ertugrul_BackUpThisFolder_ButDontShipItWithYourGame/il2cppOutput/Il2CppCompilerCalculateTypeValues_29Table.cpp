﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Microsoft.Win32.SafeHandles.SafeWaitHandle
struct SafeWaitHandle_t51DB35FF382E636FF3B868D87816733894D46CF2;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.ArrayList
struct ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4;
// System.Collections.Generic.LinkedList`1<System.WeakReference>
struct LinkedList_1_t4BA9E2BFC78453D2CB909F7665EBFC5366C2EE25;
// System.Collections.Generic.List`1<System.String>
struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3;
// System.Collections.Generic.List`1<System.Uri>
struct List_1_t19C69957130AB21842A46224AD233BAE769AE4E8;
// System.Collections.Hashtable
struct Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.Collections.IEqualityComparer
struct IEqualityComparer_t3102D0F5BABD60224F6DFF4815BCA1045831FB7C;
// System.Collections.Specialized.ListDictionary
struct ListDictionary_tE68C8A5DB37EB10F3AA958AB3BF214346402074D;
// System.Collections.Specialized.NameObjectCollectionBase/KeysCollection
struct KeysCollection_tEE4F7181091AFE8FCC0E7B41727577860F0DF62D;
// System.Collections.Specialized.NameObjectCollectionBase/NameObjectEntry
struct NameObjectEntry_tC137E0E1F256300B1F9F0ED88EE02B6611918B54;
// System.Collections.Specialized.NameValueCollection
struct NameValueCollection_t7C7CED43E4C6E997E3C8012F1D2CC4027FAD10D1;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.IO.Stream/ReadWriteTask
struct ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Int64[]
struct Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.Net.BaseLoggingObject
struct BaseLoggingObject_tE425700C69CB9F232260567801E833D111633A4A;
// System.Net.BufferOffsetSize[]
struct BufferOffsetSizeU5BU5D_tA2E04E7114D20AD4B552D2684A84A326A9DBFE27;
// System.Net.Cache.RequestCacheBinding
struct RequestCacheBinding_tB84D71781C4BCEF43DEBC72165283C4543BA4724;
// System.Net.Cache.RequestCachePolicy
struct RequestCachePolicy_t30D7352C7E9D49EEADD492A70EC92C118D90CD61;
// System.Net.Cache.RequestCacheProtocol
struct RequestCacheProtocol_t51DE21412EAD66CAD600D3A6940942920340D35D;
// System.Net.ConnectionPool
struct ConnectionPool_t7BEC38A8953B38D2C1262D373089B8915ECE020C;
// System.Net.GeneralAsyncDelegate
struct GeneralAsyncDelegate_tD535232B4C5C7E31A01C18A2A072F80C1C085D92;
// System.Net.HeaderInfo
struct HeaderInfo_t08A38618F1A42BEE66373512B83B804DAEAF2EB8;
// System.Net.HeaderInfoTable
struct HeaderInfoTable_t16B4CA77715B871579C8DE60EBD92E8EDD332BAF;
// System.Net.HeaderParser
struct HeaderParser_t6B59FF0FD79FFD511A019AE5383DCEF641BA822E;
// System.Net.HttpAbortDelegate
struct HttpAbortDelegate_tBF5D375FB316FE054524CCF1035D90029966A637;
// System.Net.HttpWebRequest
struct HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0;
// System.Net.ICredentials
struct ICredentials_t1A41F1096B037CAB53AE01434DF0747881455344;
// System.Net.IPAddress
struct IPAddress_t77F35D21A3027F0CE7B38EA9B56BFD31B28952CE;
// System.Net.IWebProxy
struct IWebProxy_tA24C0862A1ACA35D20FD079E2672CA5786C1A67E;
// System.Net.IWebRequestCreate
struct IWebRequestCreate_t06784F00B2587AB3D7D13A6B2C96819C8F5260ED;
// System.Net.LazyAsyncResult/ThreadContext
struct ThreadContext_tCC2E1DE0DDF550CCA67AE22CDF6F1AD426DC9082;
// System.Net.ProxyChain
struct ProxyChain_t58CD16408B22DC2B56772ADD1414604ABEF5AA53;
// System.Net.ProxyChain/ProxyEnumerator
struct ProxyEnumerator_t92F0DA133B067B6E5BA335083CDE845D610B7016;
// System.Net.ScatterGatherBuffers/MemoryChunk
struct MemoryChunk_t2B4F05956C1526847FF22066DC4E91A9A297951F;
// System.Net.Sockets.NetworkStream
struct NetworkStream_t362D0CD0C74C2F5CBD02905C9422E4240872ADCA;
// System.Net.Sockets.Socket
struct Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8;
// System.Net.TimerThread/Callback
struct Callback_t8DF3FD84AB632709C486978BE28ED721EB3A40E3;
// System.Net.TimerThread/Queue
struct Queue_tCCFF6A2FCF584216AEDA04A483FB808E2D493643;
// System.Net.TimerThread/Timer
struct Timer_t3B21B1013E27B5DC9FED14EC0921A5F51230D46F;
// System.Net.TimerThread/TimerNode
struct TimerNode_t5596C0BAF3E85D0C2444ECA74917630FDEF5087B;
// System.Net.WebException
struct WebException_tD400C9DEBEBB6AEDA77500E634D20692E27A993D;
// System.Net.WebHeaderCollection
struct WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304;
// System.Net.WebHeaderCollection/RfcChar[]
struct RfcCharU5BU5D_t27AD0ADBD612E10FCEF4917B5E70094398C6EC4E;
// System.Net.WebProxy
struct WebProxy_t075305900B1D4D8BC8FAB08852842E42BC000417;
// System.Net.WebProxyData
struct WebProxyData_t12D22F3302B5ED2B04EA88CE36650BB097E46922;
// System.Net.WebRequest
struct WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8;
// System.Net.WebRequest/DesignerWebRequestCreate
struct DesignerWebRequestCreate_t613DD91D4F07703DC65E847B367F4DCD5710E2A3;
// System.Net.WebResponse
struct WebResponse_t5C91B5B83E2FBA2EABC6FDF2A70E9AFD9BB059BD;
// System.Net.WriteStreamClosedEventArgs
struct WriteStreamClosedEventArgs_t1E55EEFF430B18CCEF826ED9078E8D7BB6783869;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.InteropServices.SafeHandle
struct SafeHandle_t1E326D75E23FD5BB6D40BA322298FDC6526CC383;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1BB80E9C9DEA52DBF464487234B045E2930ADA26;
// System.SByte[]
struct SByteU5BU5D_t623D1F33C61DEAC564E2B0560E00F1E1364F7889;
// System.Security.Authentication.ExtendedProtection.ChannelBinding
struct ChannelBinding_t008B670203CDC7E4926CE95D32EB19E30AFE5E2C;
// System.Security.Authentication.ExtendedProtection.ServiceNameCollection
struct ServiceNameCollection_t3C60F59BA95A4A706BB70439AE03C07D09796C2C;
// System.Security.Principal.WindowsIdentity
struct WindowsIdentity_t18859A4ADBD52470392E1AA64255F45A4970DD87;
// System.Security.SecureString
struct SecureString_t0E7DCB36E6C027EA7265B7BDC2E3CAB0BA1FF2E5;
// System.String
struct String_t;
// System.StringComparer
struct StringComparer_t588BC7FEF85D6E7425E0A8147A3D5A334F1F82DE;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Text.Encoding
struct Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4;
// System.Text.RegularExpressions.Regex
struct Regex_tFD46E63A462E852189FD6AB4E2B0B67C4D8FDBDF;
// System.Threading.AutoResetEvent
struct AutoResetEvent_t2A1182CEEE4E184587D4DEAA4F382B810B21D3B7;
// System.Threading.ManualResetEvent
struct ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408;
// System.Threading.SemaphoreSlim
struct SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048;
// System.Threading.WaitHandle[]
struct WaitHandleU5BU5D_t79FF2E6AC099CC1FDD2B24F21A72D36BA31298CC;
// System.Type
struct Type_t;
// System.Uri
struct Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E;
// System.Uri[]
struct UriU5BU5D_t2BA00D5A5C9D2A274C64EB3328ADE0D952CD0EB5;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// System.WeakReference
struct WeakReference_t0495CC81CD6403E662B7700B802443F6F730E39D;

struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#define ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifndef NAMEOBJECTCOLLECTIONBASE_T593D97BF1A2AEA0C7FC1684B447BF92A5383883D_H
#define NAMEOBJECTCOLLECTIONBASE_T593D97BF1A2AEA0C7FC1684B447BF92A5383883D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Specialized.NameObjectCollectionBase
struct  NameObjectCollectionBase_t593D97BF1A2AEA0C7FC1684B447BF92A5383883D  : public RuntimeObject
{
public:
	// System.Boolean System.Collections.Specialized.NameObjectCollectionBase::_readOnly
	bool ____readOnly_0;
	// System.Collections.ArrayList System.Collections.Specialized.NameObjectCollectionBase::_entriesArray
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ____entriesArray_1;
	// System.Collections.IEqualityComparer System.Collections.Specialized.NameObjectCollectionBase::_keyComparer
	RuntimeObject* ____keyComparer_2;
	// System.Collections.Hashtable modreq(System.Runtime.CompilerServices.IsVolatile) System.Collections.Specialized.NameObjectCollectionBase::_entriesTable
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ____entriesTable_3;
	// System.Collections.Specialized.NameObjectCollectionBase_NameObjectEntry modreq(System.Runtime.CompilerServices.IsVolatile) System.Collections.Specialized.NameObjectCollectionBase::_nullKeyEntry
	NameObjectEntry_tC137E0E1F256300B1F9F0ED88EE02B6611918B54 * ____nullKeyEntry_4;
	// System.Collections.Specialized.NameObjectCollectionBase_KeysCollection System.Collections.Specialized.NameObjectCollectionBase::_keys
	KeysCollection_tEE4F7181091AFE8FCC0E7B41727577860F0DF62D * ____keys_5;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Specialized.NameObjectCollectionBase::_serializationInfo
	SerializationInfo_t1BB80E9C9DEA52DBF464487234B045E2930ADA26 * ____serializationInfo_6;
	// System.Int32 System.Collections.Specialized.NameObjectCollectionBase::_version
	int32_t ____version_7;
	// System.Object System.Collections.Specialized.NameObjectCollectionBase::_syncRoot
	RuntimeObject * ____syncRoot_8;

public:
	inline static int32_t get_offset_of__readOnly_0() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t593D97BF1A2AEA0C7FC1684B447BF92A5383883D, ____readOnly_0)); }
	inline bool get__readOnly_0() const { return ____readOnly_0; }
	inline bool* get_address_of__readOnly_0() { return &____readOnly_0; }
	inline void set__readOnly_0(bool value)
	{
		____readOnly_0 = value;
	}

	inline static int32_t get_offset_of__entriesArray_1() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t593D97BF1A2AEA0C7FC1684B447BF92A5383883D, ____entriesArray_1)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get__entriesArray_1() const { return ____entriesArray_1; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of__entriesArray_1() { return &____entriesArray_1; }
	inline void set__entriesArray_1(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		____entriesArray_1 = value;
		Il2CppCodeGenWriteBarrier((&____entriesArray_1), value);
	}

	inline static int32_t get_offset_of__keyComparer_2() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t593D97BF1A2AEA0C7FC1684B447BF92A5383883D, ____keyComparer_2)); }
	inline RuntimeObject* get__keyComparer_2() const { return ____keyComparer_2; }
	inline RuntimeObject** get_address_of__keyComparer_2() { return &____keyComparer_2; }
	inline void set__keyComparer_2(RuntimeObject* value)
	{
		____keyComparer_2 = value;
		Il2CppCodeGenWriteBarrier((&____keyComparer_2), value);
	}

	inline static int32_t get_offset_of__entriesTable_3() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t593D97BF1A2AEA0C7FC1684B447BF92A5383883D, ____entriesTable_3)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get__entriesTable_3() const { return ____entriesTable_3; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of__entriesTable_3() { return &____entriesTable_3; }
	inline void set__entriesTable_3(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		____entriesTable_3 = value;
		Il2CppCodeGenWriteBarrier((&____entriesTable_3), value);
	}

	inline static int32_t get_offset_of__nullKeyEntry_4() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t593D97BF1A2AEA0C7FC1684B447BF92A5383883D, ____nullKeyEntry_4)); }
	inline NameObjectEntry_tC137E0E1F256300B1F9F0ED88EE02B6611918B54 * get__nullKeyEntry_4() const { return ____nullKeyEntry_4; }
	inline NameObjectEntry_tC137E0E1F256300B1F9F0ED88EE02B6611918B54 ** get_address_of__nullKeyEntry_4() { return &____nullKeyEntry_4; }
	inline void set__nullKeyEntry_4(NameObjectEntry_tC137E0E1F256300B1F9F0ED88EE02B6611918B54 * value)
	{
		____nullKeyEntry_4 = value;
		Il2CppCodeGenWriteBarrier((&____nullKeyEntry_4), value);
	}

	inline static int32_t get_offset_of__keys_5() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t593D97BF1A2AEA0C7FC1684B447BF92A5383883D, ____keys_5)); }
	inline KeysCollection_tEE4F7181091AFE8FCC0E7B41727577860F0DF62D * get__keys_5() const { return ____keys_5; }
	inline KeysCollection_tEE4F7181091AFE8FCC0E7B41727577860F0DF62D ** get_address_of__keys_5() { return &____keys_5; }
	inline void set__keys_5(KeysCollection_tEE4F7181091AFE8FCC0E7B41727577860F0DF62D * value)
	{
		____keys_5 = value;
		Il2CppCodeGenWriteBarrier((&____keys_5), value);
	}

	inline static int32_t get_offset_of__serializationInfo_6() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t593D97BF1A2AEA0C7FC1684B447BF92A5383883D, ____serializationInfo_6)); }
	inline SerializationInfo_t1BB80E9C9DEA52DBF464487234B045E2930ADA26 * get__serializationInfo_6() const { return ____serializationInfo_6; }
	inline SerializationInfo_t1BB80E9C9DEA52DBF464487234B045E2930ADA26 ** get_address_of__serializationInfo_6() { return &____serializationInfo_6; }
	inline void set__serializationInfo_6(SerializationInfo_t1BB80E9C9DEA52DBF464487234B045E2930ADA26 * value)
	{
		____serializationInfo_6 = value;
		Il2CppCodeGenWriteBarrier((&____serializationInfo_6), value);
	}

	inline static int32_t get_offset_of__version_7() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t593D97BF1A2AEA0C7FC1684B447BF92A5383883D, ____version_7)); }
	inline int32_t get__version_7() const { return ____version_7; }
	inline int32_t* get_address_of__version_7() { return &____version_7; }
	inline void set__version_7(int32_t value)
	{
		____version_7 = value;
	}

	inline static int32_t get_offset_of__syncRoot_8() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t593D97BF1A2AEA0C7FC1684B447BF92A5383883D, ____syncRoot_8)); }
	inline RuntimeObject * get__syncRoot_8() const { return ____syncRoot_8; }
	inline RuntimeObject ** get_address_of__syncRoot_8() { return &____syncRoot_8; }
	inline void set__syncRoot_8(RuntimeObject * value)
	{
		____syncRoot_8 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_8), value);
	}
};

struct NameObjectCollectionBase_t593D97BF1A2AEA0C7FC1684B447BF92A5383883D_StaticFields
{
public:
	// System.StringComparer System.Collections.Specialized.NameObjectCollectionBase::defaultComparer
	StringComparer_t588BC7FEF85D6E7425E0A8147A3D5A334F1F82DE * ___defaultComparer_9;

public:
	inline static int32_t get_offset_of_defaultComparer_9() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t593D97BF1A2AEA0C7FC1684B447BF92A5383883D_StaticFields, ___defaultComparer_9)); }
	inline StringComparer_t588BC7FEF85D6E7425E0A8147A3D5A334F1F82DE * get_defaultComparer_9() const { return ___defaultComparer_9; }
	inline StringComparer_t588BC7FEF85D6E7425E0A8147A3D5A334F1F82DE ** get_address_of_defaultComparer_9() { return &___defaultComparer_9; }
	inline void set_defaultComparer_9(StringComparer_t588BC7FEF85D6E7425E0A8147A3D5A334F1F82DE * value)
	{
		___defaultComparer_9 = value;
		Il2CppCodeGenWriteBarrier((&___defaultComparer_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMEOBJECTCOLLECTIONBASE_T593D97BF1A2AEA0C7FC1684B447BF92A5383883D_H
#ifndef EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#define EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventArgs
struct  EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E  : public RuntimeObject
{
public:

public:
};

struct EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields, ___Empty_0)); }
	inline EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#define MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MarshalByRefObject
struct  MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF  : public RuntimeObject
{
public:
	// System.Object System.MarshalByRefObject::_identity
	RuntimeObject * ____identity_0;

public:
	inline static int32_t get_offset_of__identity_0() { return static_cast<int32_t>(offsetof(MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF, ____identity_0)); }
	inline RuntimeObject * get__identity_0() const { return ____identity_0; }
	inline RuntimeObject ** get_address_of__identity_0() { return &____identity_0; }
	inline void set__identity_0(RuntimeObject * value)
	{
		____identity_0 = value;
		Il2CppCodeGenWriteBarrier((&____identity_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF_marshaled_pinvoke
{
	Il2CppIUnknown* ____identity_0;
};
// Native definition for COM marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF_marshaled_com
{
	Il2CppIUnknown* ____identity_0;
};
#endif // MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#ifndef BASELOGGINGOBJECT_TE425700C69CB9F232260567801E833D111633A4A_H
#define BASELOGGINGOBJECT_TE425700C69CB9F232260567801E833D111633A4A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.BaseLoggingObject
struct  BaseLoggingObject_tE425700C69CB9F232260567801E833D111633A4A  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASELOGGINGOBJECT_TE425700C69CB9F232260567801E833D111633A4A_H
#ifndef BUFFEROFFSETSIZE_T167DA604FC5C1B4C7784C746315EABFE6D40F317_H
#define BUFFEROFFSETSIZE_T167DA604FC5C1B4C7784C746315EABFE6D40F317_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.BufferOffsetSize
struct  BufferOffsetSize_t167DA604FC5C1B4C7784C746315EABFE6D40F317  : public RuntimeObject
{
public:
	// System.Byte[] System.Net.BufferOffsetSize::Buffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___Buffer_0;
	// System.Int32 System.Net.BufferOffsetSize::Offset
	int32_t ___Offset_1;
	// System.Int32 System.Net.BufferOffsetSize::Size
	int32_t ___Size_2;

public:
	inline static int32_t get_offset_of_Buffer_0() { return static_cast<int32_t>(offsetof(BufferOffsetSize_t167DA604FC5C1B4C7784C746315EABFE6D40F317, ___Buffer_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_Buffer_0() const { return ___Buffer_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_Buffer_0() { return &___Buffer_0; }
	inline void set_Buffer_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___Buffer_0 = value;
		Il2CppCodeGenWriteBarrier((&___Buffer_0), value);
	}

	inline static int32_t get_offset_of_Offset_1() { return static_cast<int32_t>(offsetof(BufferOffsetSize_t167DA604FC5C1B4C7784C746315EABFE6D40F317, ___Offset_1)); }
	inline int32_t get_Offset_1() const { return ___Offset_1; }
	inline int32_t* get_address_of_Offset_1() { return &___Offset_1; }
	inline void set_Offset_1(int32_t value)
	{
		___Offset_1 = value;
	}

	inline static int32_t get_offset_of_Size_2() { return static_cast<int32_t>(offsetof(BufferOffsetSize_t167DA604FC5C1B4C7784C746315EABFE6D40F317, ___Size_2)); }
	inline int32_t get_Size_2() const { return ___Size_2; }
	inline int32_t* get_address_of_Size_2() { return &___Size_2; }
	inline void set_Size_2(int32_t value)
	{
		___Size_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUFFEROFFSETSIZE_T167DA604FC5C1B4C7784C746315EABFE6D40F317_H
#ifndef CASEINSENSITIVEASCII_TAC44F3DBCEA33C5581DCE8ADC66B0317FFA41E8B_H
#define CASEINSENSITIVEASCII_TAC44F3DBCEA33C5581DCE8ADC66B0317FFA41E8B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.CaseInsensitiveAscii
struct  CaseInsensitiveAscii_tAC44F3DBCEA33C5581DCE8ADC66B0317FFA41E8B  : public RuntimeObject
{
public:

public:
};

struct CaseInsensitiveAscii_tAC44F3DBCEA33C5581DCE8ADC66B0317FFA41E8B_StaticFields
{
public:
	// System.Net.CaseInsensitiveAscii System.Net.CaseInsensitiveAscii::StaticInstance
	CaseInsensitiveAscii_tAC44F3DBCEA33C5581DCE8ADC66B0317FFA41E8B * ___StaticInstance_0;
	// System.Byte[] System.Net.CaseInsensitiveAscii::AsciiToLower
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___AsciiToLower_1;

public:
	inline static int32_t get_offset_of_StaticInstance_0() { return static_cast<int32_t>(offsetof(CaseInsensitiveAscii_tAC44F3DBCEA33C5581DCE8ADC66B0317FFA41E8B_StaticFields, ___StaticInstance_0)); }
	inline CaseInsensitiveAscii_tAC44F3DBCEA33C5581DCE8ADC66B0317FFA41E8B * get_StaticInstance_0() const { return ___StaticInstance_0; }
	inline CaseInsensitiveAscii_tAC44F3DBCEA33C5581DCE8ADC66B0317FFA41E8B ** get_address_of_StaticInstance_0() { return &___StaticInstance_0; }
	inline void set_StaticInstance_0(CaseInsensitiveAscii_tAC44F3DBCEA33C5581DCE8ADC66B0317FFA41E8B * value)
	{
		___StaticInstance_0 = value;
		Il2CppCodeGenWriteBarrier((&___StaticInstance_0), value);
	}

	inline static int32_t get_offset_of_AsciiToLower_1() { return static_cast<int32_t>(offsetof(CaseInsensitiveAscii_tAC44F3DBCEA33C5581DCE8ADC66B0317FFA41E8B_StaticFields, ___AsciiToLower_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_AsciiToLower_1() const { return ___AsciiToLower_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_AsciiToLower_1() { return &___AsciiToLower_1; }
	inline void set_AsciiToLower_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___AsciiToLower_1 = value;
		Il2CppCodeGenWriteBarrier((&___AsciiToLower_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CASEINSENSITIVEASCII_TAC44F3DBCEA33C5581DCE8ADC66B0317FFA41E8B_H
#ifndef DELAYEDREGEX_T8C9BDFF6F3232C12E0AB4BCE4A0B84E70A5FC541_H
#define DELAYEDREGEX_T8C9BDFF6F3232C12E0AB4BCE4A0B84E70A5FC541_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.DelayedRegex
struct  DelayedRegex_t8C9BDFF6F3232C12E0AB4BCE4A0B84E70A5FC541  : public RuntimeObject
{
public:
	// System.Text.RegularExpressions.Regex System.Net.DelayedRegex::_AsRegex
	Regex_tFD46E63A462E852189FD6AB4E2B0B67C4D8FDBDF * ____AsRegex_0;
	// System.String System.Net.DelayedRegex::_AsString
	String_t* ____AsString_1;

public:
	inline static int32_t get_offset_of__AsRegex_0() { return static_cast<int32_t>(offsetof(DelayedRegex_t8C9BDFF6F3232C12E0AB4BCE4A0B84E70A5FC541, ____AsRegex_0)); }
	inline Regex_tFD46E63A462E852189FD6AB4E2B0B67C4D8FDBDF * get__AsRegex_0() const { return ____AsRegex_0; }
	inline Regex_tFD46E63A462E852189FD6AB4E2B0B67C4D8FDBDF ** get_address_of__AsRegex_0() { return &____AsRegex_0; }
	inline void set__AsRegex_0(Regex_tFD46E63A462E852189FD6AB4E2B0B67C4D8FDBDF * value)
	{
		____AsRegex_0 = value;
		Il2CppCodeGenWriteBarrier((&____AsRegex_0), value);
	}

	inline static int32_t get_offset_of__AsString_1() { return static_cast<int32_t>(offsetof(DelayedRegex_t8C9BDFF6F3232C12E0AB4BCE4A0B84E70A5FC541, ____AsString_1)); }
	inline String_t* get__AsString_1() const { return ____AsString_1; }
	inline String_t** get_address_of__AsString_1() { return &____AsString_1; }
	inline void set__AsString_1(String_t* value)
	{
		____AsString_1 = value;
		Il2CppCodeGenWriteBarrier((&____AsString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELAYEDREGEX_T8C9BDFF6F3232C12E0AB4BCE4A0B84E70A5FC541_H
#ifndef EMPTYWEBPROXY_TF6CEF11A280246455534D46AD1710271B8BEE22D_H
#define EMPTYWEBPROXY_TF6CEF11A280246455534D46AD1710271B8BEE22D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.EmptyWebProxy
struct  EmptyWebProxy_tF6CEF11A280246455534D46AD1710271B8BEE22D  : public RuntimeObject
{
public:
	// System.Net.ICredentials System.Net.EmptyWebProxy::m_credentials
	RuntimeObject* ___m_credentials_0;

public:
	inline static int32_t get_offset_of_m_credentials_0() { return static_cast<int32_t>(offsetof(EmptyWebProxy_tF6CEF11A280246455534D46AD1710271B8BEE22D, ___m_credentials_0)); }
	inline RuntimeObject* get_m_credentials_0() const { return ___m_credentials_0; }
	inline RuntimeObject** get_address_of_m_credentials_0() { return &___m_credentials_0; }
	inline void set_m_credentials_0(RuntimeObject* value)
	{
		___m_credentials_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_credentials_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMPTYWEBPROXY_TF6CEF11A280246455534D46AD1710271B8BEE22D_H
#ifndef GLOBALLOG_TA300083DFE1CA3CDE1223FE5A71452FB24EF4AFD_H
#define GLOBALLOG_TA300083DFE1CA3CDE1223FE5A71452FB24EF4AFD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.GlobalLog
struct  GlobalLog_tA300083DFE1CA3CDE1223FE5A71452FB24EF4AFD  : public RuntimeObject
{
public:

public:
};

struct GlobalLog_tA300083DFE1CA3CDE1223FE5A71452FB24EF4AFD_StaticFields
{
public:
	// System.Net.BaseLoggingObject System.Net.GlobalLog::Logobject
	BaseLoggingObject_tE425700C69CB9F232260567801E833D111633A4A * ___Logobject_0;

public:
	inline static int32_t get_offset_of_Logobject_0() { return static_cast<int32_t>(offsetof(GlobalLog_tA300083DFE1CA3CDE1223FE5A71452FB24EF4AFD_StaticFields, ___Logobject_0)); }
	inline BaseLoggingObject_tE425700C69CB9F232260567801E833D111633A4A * get_Logobject_0() const { return ___Logobject_0; }
	inline BaseLoggingObject_tE425700C69CB9F232260567801E833D111633A4A ** get_address_of_Logobject_0() { return &___Logobject_0; }
	inline void set_Logobject_0(BaseLoggingObject_tE425700C69CB9F232260567801E833D111633A4A * value)
	{
		___Logobject_0 = value;
		Il2CppCodeGenWriteBarrier((&___Logobject_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLOBALLOG_TA300083DFE1CA3CDE1223FE5A71452FB24EF4AFD_H
#ifndef HEADERINFO_T08A38618F1A42BEE66373512B83B804DAEAF2EB8_H
#define HEADERINFO_T08A38618F1A42BEE66373512B83B804DAEAF2EB8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HeaderInfo
struct  HeaderInfo_t08A38618F1A42BEE66373512B83B804DAEAF2EB8  : public RuntimeObject
{
public:
	// System.Boolean System.Net.HeaderInfo::IsRequestRestricted
	bool ___IsRequestRestricted_0;
	// System.Boolean System.Net.HeaderInfo::IsResponseRestricted
	bool ___IsResponseRestricted_1;
	// System.Net.HeaderParser System.Net.HeaderInfo::Parser
	HeaderParser_t6B59FF0FD79FFD511A019AE5383DCEF641BA822E * ___Parser_2;
	// System.String System.Net.HeaderInfo::HeaderName
	String_t* ___HeaderName_3;
	// System.Boolean System.Net.HeaderInfo::AllowMultiValues
	bool ___AllowMultiValues_4;

public:
	inline static int32_t get_offset_of_IsRequestRestricted_0() { return static_cast<int32_t>(offsetof(HeaderInfo_t08A38618F1A42BEE66373512B83B804DAEAF2EB8, ___IsRequestRestricted_0)); }
	inline bool get_IsRequestRestricted_0() const { return ___IsRequestRestricted_0; }
	inline bool* get_address_of_IsRequestRestricted_0() { return &___IsRequestRestricted_0; }
	inline void set_IsRequestRestricted_0(bool value)
	{
		___IsRequestRestricted_0 = value;
	}

	inline static int32_t get_offset_of_IsResponseRestricted_1() { return static_cast<int32_t>(offsetof(HeaderInfo_t08A38618F1A42BEE66373512B83B804DAEAF2EB8, ___IsResponseRestricted_1)); }
	inline bool get_IsResponseRestricted_1() const { return ___IsResponseRestricted_1; }
	inline bool* get_address_of_IsResponseRestricted_1() { return &___IsResponseRestricted_1; }
	inline void set_IsResponseRestricted_1(bool value)
	{
		___IsResponseRestricted_1 = value;
	}

	inline static int32_t get_offset_of_Parser_2() { return static_cast<int32_t>(offsetof(HeaderInfo_t08A38618F1A42BEE66373512B83B804DAEAF2EB8, ___Parser_2)); }
	inline HeaderParser_t6B59FF0FD79FFD511A019AE5383DCEF641BA822E * get_Parser_2() const { return ___Parser_2; }
	inline HeaderParser_t6B59FF0FD79FFD511A019AE5383DCEF641BA822E ** get_address_of_Parser_2() { return &___Parser_2; }
	inline void set_Parser_2(HeaderParser_t6B59FF0FD79FFD511A019AE5383DCEF641BA822E * value)
	{
		___Parser_2 = value;
		Il2CppCodeGenWriteBarrier((&___Parser_2), value);
	}

	inline static int32_t get_offset_of_HeaderName_3() { return static_cast<int32_t>(offsetof(HeaderInfo_t08A38618F1A42BEE66373512B83B804DAEAF2EB8, ___HeaderName_3)); }
	inline String_t* get_HeaderName_3() const { return ___HeaderName_3; }
	inline String_t** get_address_of_HeaderName_3() { return &___HeaderName_3; }
	inline void set_HeaderName_3(String_t* value)
	{
		___HeaderName_3 = value;
		Il2CppCodeGenWriteBarrier((&___HeaderName_3), value);
	}

	inline static int32_t get_offset_of_AllowMultiValues_4() { return static_cast<int32_t>(offsetof(HeaderInfo_t08A38618F1A42BEE66373512B83B804DAEAF2EB8, ___AllowMultiValues_4)); }
	inline bool get_AllowMultiValues_4() const { return ___AllowMultiValues_4; }
	inline bool* get_address_of_AllowMultiValues_4() { return &___AllowMultiValues_4; }
	inline void set_AllowMultiValues_4(bool value)
	{
		___AllowMultiValues_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEADERINFO_T08A38618F1A42BEE66373512B83B804DAEAF2EB8_H
#ifndef HEADERINFOTABLE_T16B4CA77715B871579C8DE60EBD92E8EDD332BAF_H
#define HEADERINFOTABLE_T16B4CA77715B871579C8DE60EBD92E8EDD332BAF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HeaderInfoTable
struct  HeaderInfoTable_t16B4CA77715B871579C8DE60EBD92E8EDD332BAF  : public RuntimeObject
{
public:

public:
};

struct HeaderInfoTable_t16B4CA77715B871579C8DE60EBD92E8EDD332BAF_StaticFields
{
public:
	// System.Collections.Hashtable System.Net.HeaderInfoTable::HeaderHashTable
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___HeaderHashTable_0;
	// System.Net.HeaderInfo System.Net.HeaderInfoTable::UnknownHeaderInfo
	HeaderInfo_t08A38618F1A42BEE66373512B83B804DAEAF2EB8 * ___UnknownHeaderInfo_1;
	// System.Net.HeaderParser System.Net.HeaderInfoTable::SingleParser
	HeaderParser_t6B59FF0FD79FFD511A019AE5383DCEF641BA822E * ___SingleParser_2;
	// System.Net.HeaderParser System.Net.HeaderInfoTable::MultiParser
	HeaderParser_t6B59FF0FD79FFD511A019AE5383DCEF641BA822E * ___MultiParser_3;

public:
	inline static int32_t get_offset_of_HeaderHashTable_0() { return static_cast<int32_t>(offsetof(HeaderInfoTable_t16B4CA77715B871579C8DE60EBD92E8EDD332BAF_StaticFields, ___HeaderHashTable_0)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_HeaderHashTable_0() const { return ___HeaderHashTable_0; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_HeaderHashTable_0() { return &___HeaderHashTable_0; }
	inline void set_HeaderHashTable_0(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___HeaderHashTable_0 = value;
		Il2CppCodeGenWriteBarrier((&___HeaderHashTable_0), value);
	}

	inline static int32_t get_offset_of_UnknownHeaderInfo_1() { return static_cast<int32_t>(offsetof(HeaderInfoTable_t16B4CA77715B871579C8DE60EBD92E8EDD332BAF_StaticFields, ___UnknownHeaderInfo_1)); }
	inline HeaderInfo_t08A38618F1A42BEE66373512B83B804DAEAF2EB8 * get_UnknownHeaderInfo_1() const { return ___UnknownHeaderInfo_1; }
	inline HeaderInfo_t08A38618F1A42BEE66373512B83B804DAEAF2EB8 ** get_address_of_UnknownHeaderInfo_1() { return &___UnknownHeaderInfo_1; }
	inline void set_UnknownHeaderInfo_1(HeaderInfo_t08A38618F1A42BEE66373512B83B804DAEAF2EB8 * value)
	{
		___UnknownHeaderInfo_1 = value;
		Il2CppCodeGenWriteBarrier((&___UnknownHeaderInfo_1), value);
	}

	inline static int32_t get_offset_of_SingleParser_2() { return static_cast<int32_t>(offsetof(HeaderInfoTable_t16B4CA77715B871579C8DE60EBD92E8EDD332BAF_StaticFields, ___SingleParser_2)); }
	inline HeaderParser_t6B59FF0FD79FFD511A019AE5383DCEF641BA822E * get_SingleParser_2() const { return ___SingleParser_2; }
	inline HeaderParser_t6B59FF0FD79FFD511A019AE5383DCEF641BA822E ** get_address_of_SingleParser_2() { return &___SingleParser_2; }
	inline void set_SingleParser_2(HeaderParser_t6B59FF0FD79FFD511A019AE5383DCEF641BA822E * value)
	{
		___SingleParser_2 = value;
		Il2CppCodeGenWriteBarrier((&___SingleParser_2), value);
	}

	inline static int32_t get_offset_of_MultiParser_3() { return static_cast<int32_t>(offsetof(HeaderInfoTable_t16B4CA77715B871579C8DE60EBD92E8EDD332BAF_StaticFields, ___MultiParser_3)); }
	inline HeaderParser_t6B59FF0FD79FFD511A019AE5383DCEF641BA822E * get_MultiParser_3() const { return ___MultiParser_3; }
	inline HeaderParser_t6B59FF0FD79FFD511A019AE5383DCEF641BA822E ** get_address_of_MultiParser_3() { return &___MultiParser_3; }
	inline void set_MultiParser_3(HeaderParser_t6B59FF0FD79FFD511A019AE5383DCEF641BA822E * value)
	{
		___MultiParser_3 = value;
		Il2CppCodeGenWriteBarrier((&___MultiParser_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEADERINFOTABLE_T16B4CA77715B871579C8DE60EBD92E8EDD332BAF_H
#ifndef HOSTHEADERSTRING_TBC7E5E660D162413E9357EAD3354E813E410FBB7_H
#define HOSTHEADERSTRING_TBC7E5E660D162413E9357EAD3354E813E410FBB7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HostHeaderString
struct  HostHeaderString_tBC7E5E660D162413E9357EAD3354E813E410FBB7  : public RuntimeObject
{
public:
	// System.Boolean System.Net.HostHeaderString::m_Converted
	bool ___m_Converted_0;
	// System.String System.Net.HostHeaderString::m_String
	String_t* ___m_String_1;
	// System.Byte[] System.Net.HostHeaderString::m_Bytes
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___m_Bytes_2;

public:
	inline static int32_t get_offset_of_m_Converted_0() { return static_cast<int32_t>(offsetof(HostHeaderString_tBC7E5E660D162413E9357EAD3354E813E410FBB7, ___m_Converted_0)); }
	inline bool get_m_Converted_0() const { return ___m_Converted_0; }
	inline bool* get_address_of_m_Converted_0() { return &___m_Converted_0; }
	inline void set_m_Converted_0(bool value)
	{
		___m_Converted_0 = value;
	}

	inline static int32_t get_offset_of_m_String_1() { return static_cast<int32_t>(offsetof(HostHeaderString_tBC7E5E660D162413E9357EAD3354E813E410FBB7, ___m_String_1)); }
	inline String_t* get_m_String_1() const { return ___m_String_1; }
	inline String_t** get_address_of_m_String_1() { return &___m_String_1; }
	inline void set_m_String_1(String_t* value)
	{
		___m_String_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_String_1), value);
	}

	inline static int32_t get_offset_of_m_Bytes_2() { return static_cast<int32_t>(offsetof(HostHeaderString_tBC7E5E660D162413E9357EAD3354E813E410FBB7, ___m_Bytes_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_m_Bytes_2() const { return ___m_Bytes_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_m_Bytes_2() { return &___m_Bytes_2; }
	inline void set_m_Bytes_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___m_Bytes_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Bytes_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HOSTHEADERSTRING_TBC7E5E660D162413E9357EAD3354E813E410FBB7_H
#ifndef HTTPDATEPARSE_T4A1233E61431D860AD7B37695E231713DD89D524_H
#define HTTPDATEPARSE_T4A1233E61431D860AD7B37695E231713DD89D524_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HttpDateParse
struct  HttpDateParse_t4A1233E61431D860AD7B37695E231713DD89D524  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPDATEPARSE_T4A1233E61431D860AD7B37695E231713DD89D524_H
#ifndef HTTPKNOWNHEADERNAMES_T524E4B913B15F4D16D00C7F6EFDB5A44446189F9_H
#define HTTPKNOWNHEADERNAMES_T524E4B913B15F4D16D00C7F6EFDB5A44446189F9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HttpKnownHeaderNames
struct  HttpKnownHeaderNames_t524E4B913B15F4D16D00C7F6EFDB5A44446189F9  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPKNOWNHEADERNAMES_T524E4B913B15F4D16D00C7F6EFDB5A44446189F9_H
#ifndef HTTPPROTOCOLUTILS_T31E9AE5BAACF7A04CECA4AB8903A4CAE2B94ECA5_H
#define HTTPPROTOCOLUTILS_T31E9AE5BAACF7A04CECA4AB8903A4CAE2B94ECA5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HttpProtocolUtils
struct  HttpProtocolUtils_t31E9AE5BAACF7A04CECA4AB8903A4CAE2B94ECA5  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPPROTOCOLUTILS_T31E9AE5BAACF7A04CECA4AB8903A4CAE2B94ECA5_H
#ifndef KNOWNHTTPVERB_TC4B46E08A385DC41D33A27F644B86A76CCA26633_H
#define KNOWNHTTPVERB_TC4B46E08A385DC41D33A27F644B86A76CCA26633_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.KnownHttpVerb
struct  KnownHttpVerb_tC4B46E08A385DC41D33A27F644B86A76CCA26633  : public RuntimeObject
{
public:
	// System.String System.Net.KnownHttpVerb::Name
	String_t* ___Name_0;
	// System.Boolean System.Net.KnownHttpVerb::RequireContentBody
	bool ___RequireContentBody_1;
	// System.Boolean System.Net.KnownHttpVerb::ContentBodyNotAllowed
	bool ___ContentBodyNotAllowed_2;
	// System.Boolean System.Net.KnownHttpVerb::ConnectRequest
	bool ___ConnectRequest_3;
	// System.Boolean System.Net.KnownHttpVerb::ExpectNoContentResponse
	bool ___ExpectNoContentResponse_4;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(KnownHttpVerb_tC4B46E08A385DC41D33A27F644B86A76CCA26633, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_RequireContentBody_1() { return static_cast<int32_t>(offsetof(KnownHttpVerb_tC4B46E08A385DC41D33A27F644B86A76CCA26633, ___RequireContentBody_1)); }
	inline bool get_RequireContentBody_1() const { return ___RequireContentBody_1; }
	inline bool* get_address_of_RequireContentBody_1() { return &___RequireContentBody_1; }
	inline void set_RequireContentBody_1(bool value)
	{
		___RequireContentBody_1 = value;
	}

	inline static int32_t get_offset_of_ContentBodyNotAllowed_2() { return static_cast<int32_t>(offsetof(KnownHttpVerb_tC4B46E08A385DC41D33A27F644B86A76CCA26633, ___ContentBodyNotAllowed_2)); }
	inline bool get_ContentBodyNotAllowed_2() const { return ___ContentBodyNotAllowed_2; }
	inline bool* get_address_of_ContentBodyNotAllowed_2() { return &___ContentBodyNotAllowed_2; }
	inline void set_ContentBodyNotAllowed_2(bool value)
	{
		___ContentBodyNotAllowed_2 = value;
	}

	inline static int32_t get_offset_of_ConnectRequest_3() { return static_cast<int32_t>(offsetof(KnownHttpVerb_tC4B46E08A385DC41D33A27F644B86A76CCA26633, ___ConnectRequest_3)); }
	inline bool get_ConnectRequest_3() const { return ___ConnectRequest_3; }
	inline bool* get_address_of_ConnectRequest_3() { return &___ConnectRequest_3; }
	inline void set_ConnectRequest_3(bool value)
	{
		___ConnectRequest_3 = value;
	}

	inline static int32_t get_offset_of_ExpectNoContentResponse_4() { return static_cast<int32_t>(offsetof(KnownHttpVerb_tC4B46E08A385DC41D33A27F644B86A76CCA26633, ___ExpectNoContentResponse_4)); }
	inline bool get_ExpectNoContentResponse_4() const { return ___ExpectNoContentResponse_4; }
	inline bool* get_address_of_ExpectNoContentResponse_4() { return &___ExpectNoContentResponse_4; }
	inline void set_ExpectNoContentResponse_4(bool value)
	{
		___ExpectNoContentResponse_4 = value;
	}
};

struct KnownHttpVerb_tC4B46E08A385DC41D33A27F644B86A76CCA26633_StaticFields
{
public:
	// System.Collections.Specialized.ListDictionary System.Net.KnownHttpVerb::NamedHeaders
	ListDictionary_tE68C8A5DB37EB10F3AA958AB3BF214346402074D * ___NamedHeaders_5;
	// System.Net.KnownHttpVerb System.Net.KnownHttpVerb::Get
	KnownHttpVerb_tC4B46E08A385DC41D33A27F644B86A76CCA26633 * ___Get_6;
	// System.Net.KnownHttpVerb System.Net.KnownHttpVerb::Connect
	KnownHttpVerb_tC4B46E08A385DC41D33A27F644B86A76CCA26633 * ___Connect_7;
	// System.Net.KnownHttpVerb System.Net.KnownHttpVerb::Head
	KnownHttpVerb_tC4B46E08A385DC41D33A27F644B86A76CCA26633 * ___Head_8;
	// System.Net.KnownHttpVerb System.Net.KnownHttpVerb::Put
	KnownHttpVerb_tC4B46E08A385DC41D33A27F644B86A76CCA26633 * ___Put_9;
	// System.Net.KnownHttpVerb System.Net.KnownHttpVerb::Post
	KnownHttpVerb_tC4B46E08A385DC41D33A27F644B86A76CCA26633 * ___Post_10;
	// System.Net.KnownHttpVerb System.Net.KnownHttpVerb::MkCol
	KnownHttpVerb_tC4B46E08A385DC41D33A27F644B86A76CCA26633 * ___MkCol_11;

public:
	inline static int32_t get_offset_of_NamedHeaders_5() { return static_cast<int32_t>(offsetof(KnownHttpVerb_tC4B46E08A385DC41D33A27F644B86A76CCA26633_StaticFields, ___NamedHeaders_5)); }
	inline ListDictionary_tE68C8A5DB37EB10F3AA958AB3BF214346402074D * get_NamedHeaders_5() const { return ___NamedHeaders_5; }
	inline ListDictionary_tE68C8A5DB37EB10F3AA958AB3BF214346402074D ** get_address_of_NamedHeaders_5() { return &___NamedHeaders_5; }
	inline void set_NamedHeaders_5(ListDictionary_tE68C8A5DB37EB10F3AA958AB3BF214346402074D * value)
	{
		___NamedHeaders_5 = value;
		Il2CppCodeGenWriteBarrier((&___NamedHeaders_5), value);
	}

	inline static int32_t get_offset_of_Get_6() { return static_cast<int32_t>(offsetof(KnownHttpVerb_tC4B46E08A385DC41D33A27F644B86A76CCA26633_StaticFields, ___Get_6)); }
	inline KnownHttpVerb_tC4B46E08A385DC41D33A27F644B86A76CCA26633 * get_Get_6() const { return ___Get_6; }
	inline KnownHttpVerb_tC4B46E08A385DC41D33A27F644B86A76CCA26633 ** get_address_of_Get_6() { return &___Get_6; }
	inline void set_Get_6(KnownHttpVerb_tC4B46E08A385DC41D33A27F644B86A76CCA26633 * value)
	{
		___Get_6 = value;
		Il2CppCodeGenWriteBarrier((&___Get_6), value);
	}

	inline static int32_t get_offset_of_Connect_7() { return static_cast<int32_t>(offsetof(KnownHttpVerb_tC4B46E08A385DC41D33A27F644B86A76CCA26633_StaticFields, ___Connect_7)); }
	inline KnownHttpVerb_tC4B46E08A385DC41D33A27F644B86A76CCA26633 * get_Connect_7() const { return ___Connect_7; }
	inline KnownHttpVerb_tC4B46E08A385DC41D33A27F644B86A76CCA26633 ** get_address_of_Connect_7() { return &___Connect_7; }
	inline void set_Connect_7(KnownHttpVerb_tC4B46E08A385DC41D33A27F644B86A76CCA26633 * value)
	{
		___Connect_7 = value;
		Il2CppCodeGenWriteBarrier((&___Connect_7), value);
	}

	inline static int32_t get_offset_of_Head_8() { return static_cast<int32_t>(offsetof(KnownHttpVerb_tC4B46E08A385DC41D33A27F644B86A76CCA26633_StaticFields, ___Head_8)); }
	inline KnownHttpVerb_tC4B46E08A385DC41D33A27F644B86A76CCA26633 * get_Head_8() const { return ___Head_8; }
	inline KnownHttpVerb_tC4B46E08A385DC41D33A27F644B86A76CCA26633 ** get_address_of_Head_8() { return &___Head_8; }
	inline void set_Head_8(KnownHttpVerb_tC4B46E08A385DC41D33A27F644B86A76CCA26633 * value)
	{
		___Head_8 = value;
		Il2CppCodeGenWriteBarrier((&___Head_8), value);
	}

	inline static int32_t get_offset_of_Put_9() { return static_cast<int32_t>(offsetof(KnownHttpVerb_tC4B46E08A385DC41D33A27F644B86A76CCA26633_StaticFields, ___Put_9)); }
	inline KnownHttpVerb_tC4B46E08A385DC41D33A27F644B86A76CCA26633 * get_Put_9() const { return ___Put_9; }
	inline KnownHttpVerb_tC4B46E08A385DC41D33A27F644B86A76CCA26633 ** get_address_of_Put_9() { return &___Put_9; }
	inline void set_Put_9(KnownHttpVerb_tC4B46E08A385DC41D33A27F644B86A76CCA26633 * value)
	{
		___Put_9 = value;
		Il2CppCodeGenWriteBarrier((&___Put_9), value);
	}

	inline static int32_t get_offset_of_Post_10() { return static_cast<int32_t>(offsetof(KnownHttpVerb_tC4B46E08A385DC41D33A27F644B86A76CCA26633_StaticFields, ___Post_10)); }
	inline KnownHttpVerb_tC4B46E08A385DC41D33A27F644B86A76CCA26633 * get_Post_10() const { return ___Post_10; }
	inline KnownHttpVerb_tC4B46E08A385DC41D33A27F644B86A76CCA26633 ** get_address_of_Post_10() { return &___Post_10; }
	inline void set_Post_10(KnownHttpVerb_tC4B46E08A385DC41D33A27F644B86A76CCA26633 * value)
	{
		___Post_10 = value;
		Il2CppCodeGenWriteBarrier((&___Post_10), value);
	}

	inline static int32_t get_offset_of_MkCol_11() { return static_cast<int32_t>(offsetof(KnownHttpVerb_tC4B46E08A385DC41D33A27F644B86A76CCA26633_StaticFields, ___MkCol_11)); }
	inline KnownHttpVerb_tC4B46E08A385DC41D33A27F644B86A76CCA26633 * get_MkCol_11() const { return ___MkCol_11; }
	inline KnownHttpVerb_tC4B46E08A385DC41D33A27F644B86A76CCA26633 ** get_address_of_MkCol_11() { return &___MkCol_11; }
	inline void set_MkCol_11(KnownHttpVerb_tC4B46E08A385DC41D33A27F644B86A76CCA26633 * value)
	{
		___MkCol_11 = value;
		Il2CppCodeGenWriteBarrier((&___MkCol_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KNOWNHTTPVERB_TC4B46E08A385DC41D33A27F644B86A76CCA26633_H
#ifndef LAZYASYNCRESULT_T6D867D275402699126BB3DC89093BD94CFFDA5D3_H
#define LAZYASYNCRESULT_T6D867D275402699126BB3DC89093BD94CFFDA5D3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.LazyAsyncResult
struct  LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3  : public RuntimeObject
{
public:
	// System.Object System.Net.LazyAsyncResult::m_AsyncObject
	RuntimeObject * ___m_AsyncObject_3;
	// System.Object System.Net.LazyAsyncResult::m_AsyncState
	RuntimeObject * ___m_AsyncState_4;
	// System.AsyncCallback System.Net.LazyAsyncResult::m_AsyncCallback
	AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___m_AsyncCallback_5;
	// System.Object System.Net.LazyAsyncResult::m_Result
	RuntimeObject * ___m_Result_6;
	// System.Int32 System.Net.LazyAsyncResult::m_ErrorCode
	int32_t ___m_ErrorCode_7;
	// System.Int32 System.Net.LazyAsyncResult::m_IntCompleted
	int32_t ___m_IntCompleted_8;
	// System.Boolean System.Net.LazyAsyncResult::m_EndCalled
	bool ___m_EndCalled_9;
	// System.Boolean System.Net.LazyAsyncResult::m_UserEvent
	bool ___m_UserEvent_10;
	// System.Object System.Net.LazyAsyncResult::m_Event
	RuntimeObject * ___m_Event_11;

public:
	inline static int32_t get_offset_of_m_AsyncObject_3() { return static_cast<int32_t>(offsetof(LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3, ___m_AsyncObject_3)); }
	inline RuntimeObject * get_m_AsyncObject_3() const { return ___m_AsyncObject_3; }
	inline RuntimeObject ** get_address_of_m_AsyncObject_3() { return &___m_AsyncObject_3; }
	inline void set_m_AsyncObject_3(RuntimeObject * value)
	{
		___m_AsyncObject_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_AsyncObject_3), value);
	}

	inline static int32_t get_offset_of_m_AsyncState_4() { return static_cast<int32_t>(offsetof(LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3, ___m_AsyncState_4)); }
	inline RuntimeObject * get_m_AsyncState_4() const { return ___m_AsyncState_4; }
	inline RuntimeObject ** get_address_of_m_AsyncState_4() { return &___m_AsyncState_4; }
	inline void set_m_AsyncState_4(RuntimeObject * value)
	{
		___m_AsyncState_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_AsyncState_4), value);
	}

	inline static int32_t get_offset_of_m_AsyncCallback_5() { return static_cast<int32_t>(offsetof(LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3, ___m_AsyncCallback_5)); }
	inline AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * get_m_AsyncCallback_5() const { return ___m_AsyncCallback_5; }
	inline AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 ** get_address_of_m_AsyncCallback_5() { return &___m_AsyncCallback_5; }
	inline void set_m_AsyncCallback_5(AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * value)
	{
		___m_AsyncCallback_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_AsyncCallback_5), value);
	}

	inline static int32_t get_offset_of_m_Result_6() { return static_cast<int32_t>(offsetof(LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3, ___m_Result_6)); }
	inline RuntimeObject * get_m_Result_6() const { return ___m_Result_6; }
	inline RuntimeObject ** get_address_of_m_Result_6() { return &___m_Result_6; }
	inline void set_m_Result_6(RuntimeObject * value)
	{
		___m_Result_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Result_6), value);
	}

	inline static int32_t get_offset_of_m_ErrorCode_7() { return static_cast<int32_t>(offsetof(LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3, ___m_ErrorCode_7)); }
	inline int32_t get_m_ErrorCode_7() const { return ___m_ErrorCode_7; }
	inline int32_t* get_address_of_m_ErrorCode_7() { return &___m_ErrorCode_7; }
	inline void set_m_ErrorCode_7(int32_t value)
	{
		___m_ErrorCode_7 = value;
	}

	inline static int32_t get_offset_of_m_IntCompleted_8() { return static_cast<int32_t>(offsetof(LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3, ___m_IntCompleted_8)); }
	inline int32_t get_m_IntCompleted_8() const { return ___m_IntCompleted_8; }
	inline int32_t* get_address_of_m_IntCompleted_8() { return &___m_IntCompleted_8; }
	inline void set_m_IntCompleted_8(int32_t value)
	{
		___m_IntCompleted_8 = value;
	}

	inline static int32_t get_offset_of_m_EndCalled_9() { return static_cast<int32_t>(offsetof(LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3, ___m_EndCalled_9)); }
	inline bool get_m_EndCalled_9() const { return ___m_EndCalled_9; }
	inline bool* get_address_of_m_EndCalled_9() { return &___m_EndCalled_9; }
	inline void set_m_EndCalled_9(bool value)
	{
		___m_EndCalled_9 = value;
	}

	inline static int32_t get_offset_of_m_UserEvent_10() { return static_cast<int32_t>(offsetof(LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3, ___m_UserEvent_10)); }
	inline bool get_m_UserEvent_10() const { return ___m_UserEvent_10; }
	inline bool* get_address_of_m_UserEvent_10() { return &___m_UserEvent_10; }
	inline void set_m_UserEvent_10(bool value)
	{
		___m_UserEvent_10 = value;
	}

	inline static int32_t get_offset_of_m_Event_11() { return static_cast<int32_t>(offsetof(LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3, ___m_Event_11)); }
	inline RuntimeObject * get_m_Event_11() const { return ___m_Event_11; }
	inline RuntimeObject ** get_address_of_m_Event_11() { return &___m_Event_11; }
	inline void set_m_Event_11(RuntimeObject * value)
	{
		___m_Event_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Event_11), value);
	}
};

struct LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3_ThreadStaticFields
{
public:
	// System.Net.LazyAsyncResult_ThreadContext System.Net.LazyAsyncResult::t_ThreadContext
	ThreadContext_tCC2E1DE0DDF550CCA67AE22CDF6F1AD426DC9082 * ___t_ThreadContext_2;

public:
	inline static int32_t get_offset_of_t_ThreadContext_2() { return static_cast<int32_t>(offsetof(LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3_ThreadStaticFields, ___t_ThreadContext_2)); }
	inline ThreadContext_tCC2E1DE0DDF550CCA67AE22CDF6F1AD426DC9082 * get_t_ThreadContext_2() const { return ___t_ThreadContext_2; }
	inline ThreadContext_tCC2E1DE0DDF550CCA67AE22CDF6F1AD426DC9082 ** get_address_of_t_ThreadContext_2() { return &___t_ThreadContext_2; }
	inline void set_t_ThreadContext_2(ThreadContext_tCC2E1DE0DDF550CCA67AE22CDF6F1AD426DC9082 * value)
	{
		___t_ThreadContext_2 = value;
		Il2CppCodeGenWriteBarrier((&___t_ThreadContext_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAZYASYNCRESULT_T6D867D275402699126BB3DC89093BD94CFFDA5D3_H
#ifndef THREADCONTEXT_TCC2E1DE0DDF550CCA67AE22CDF6F1AD426DC9082_H
#define THREADCONTEXT_TCC2E1DE0DDF550CCA67AE22CDF6F1AD426DC9082_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.LazyAsyncResult_ThreadContext
struct  ThreadContext_tCC2E1DE0DDF550CCA67AE22CDF6F1AD426DC9082  : public RuntimeObject
{
public:
	// System.Int32 System.Net.LazyAsyncResult_ThreadContext::m_NestedIOCount
	int32_t ___m_NestedIOCount_0;

public:
	inline static int32_t get_offset_of_m_NestedIOCount_0() { return static_cast<int32_t>(offsetof(ThreadContext_tCC2E1DE0DDF550CCA67AE22CDF6F1AD426DC9082, ___m_NestedIOCount_0)); }
	inline int32_t get_m_NestedIOCount_0() const { return ___m_NestedIOCount_0; }
	inline int32_t* get_address_of_m_NestedIOCount_0() { return &___m_NestedIOCount_0; }
	inline void set_m_NestedIOCount_0(int32_t value)
	{
		___m_NestedIOCount_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THREADCONTEXT_TCC2E1DE0DDF550CCA67AE22CDF6F1AD426DC9082_H
#ifndef NETRES_TB18DF1FAF98D8D7505D72FA149E57F0D31E2653B_H
#define NETRES_TB18DF1FAF98D8D7505D72FA149E57F0D31E2653B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.NetRes
struct  NetRes_tB18DF1FAF98D8D7505D72FA149E57F0D31E2653B  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETRES_TB18DF1FAF98D8D7505D72FA149E57F0D31E2653B_H
#ifndef NETWORKCREDENTIAL_TA91C6E62EA0F0915E6E393F5DFD87D03FF2C3062_H
#define NETWORKCREDENTIAL_TA91C6E62EA0F0915E6E393F5DFD87D03FF2C3062_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.NetworkCredential
struct  NetworkCredential_tA91C6E62EA0F0915E6E393F5DFD87D03FF2C3062  : public RuntimeObject
{
public:
	// System.String System.Net.NetworkCredential::m_domain
	String_t* ___m_domain_0;
	// System.String System.Net.NetworkCredential::m_userName
	String_t* ___m_userName_1;
	// System.Security.SecureString System.Net.NetworkCredential::m_password
	SecureString_t0E7DCB36E6C027EA7265B7BDC2E3CAB0BA1FF2E5 * ___m_password_2;

public:
	inline static int32_t get_offset_of_m_domain_0() { return static_cast<int32_t>(offsetof(NetworkCredential_tA91C6E62EA0F0915E6E393F5DFD87D03FF2C3062, ___m_domain_0)); }
	inline String_t* get_m_domain_0() const { return ___m_domain_0; }
	inline String_t** get_address_of_m_domain_0() { return &___m_domain_0; }
	inline void set_m_domain_0(String_t* value)
	{
		___m_domain_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_domain_0), value);
	}

	inline static int32_t get_offset_of_m_userName_1() { return static_cast<int32_t>(offsetof(NetworkCredential_tA91C6E62EA0F0915E6E393F5DFD87D03FF2C3062, ___m_userName_1)); }
	inline String_t* get_m_userName_1() const { return ___m_userName_1; }
	inline String_t** get_address_of_m_userName_1() { return &___m_userName_1; }
	inline void set_m_userName_1(String_t* value)
	{
		___m_userName_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_userName_1), value);
	}

	inline static int32_t get_offset_of_m_password_2() { return static_cast<int32_t>(offsetof(NetworkCredential_tA91C6E62EA0F0915E6E393F5DFD87D03FF2C3062, ___m_password_2)); }
	inline SecureString_t0E7DCB36E6C027EA7265B7BDC2E3CAB0BA1FF2E5 * get_m_password_2() const { return ___m_password_2; }
	inline SecureString_t0E7DCB36E6C027EA7265B7BDC2E3CAB0BA1FF2E5 ** get_address_of_m_password_2() { return &___m_password_2; }
	inline void set_m_password_2(SecureString_t0E7DCB36E6C027EA7265B7BDC2E3CAB0BA1FF2E5 * value)
	{
		___m_password_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_password_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKCREDENTIAL_TA91C6E62EA0F0915E6E393F5DFD87D03FF2C3062_H
#ifndef PROXYCHAIN_T58CD16408B22DC2B56772ADD1414604ABEF5AA53_H
#define PROXYCHAIN_T58CD16408B22DC2B56772ADD1414604ABEF5AA53_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.ProxyChain
struct  ProxyChain_t58CD16408B22DC2B56772ADD1414604ABEF5AA53  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.Uri> System.Net.ProxyChain::m_Cache
	List_1_t19C69957130AB21842A46224AD233BAE769AE4E8 * ___m_Cache_0;
	// System.Boolean System.Net.ProxyChain::m_CacheComplete
	bool ___m_CacheComplete_1;
	// System.Net.ProxyChain_ProxyEnumerator System.Net.ProxyChain::m_MainEnumerator
	ProxyEnumerator_t92F0DA133B067B6E5BA335083CDE845D610B7016 * ___m_MainEnumerator_2;
	// System.Uri System.Net.ProxyChain::m_Destination
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___m_Destination_3;
	// System.Net.HttpAbortDelegate System.Net.ProxyChain::m_HttpAbortDelegate
	HttpAbortDelegate_tBF5D375FB316FE054524CCF1035D90029966A637 * ___m_HttpAbortDelegate_4;

public:
	inline static int32_t get_offset_of_m_Cache_0() { return static_cast<int32_t>(offsetof(ProxyChain_t58CD16408B22DC2B56772ADD1414604ABEF5AA53, ___m_Cache_0)); }
	inline List_1_t19C69957130AB21842A46224AD233BAE769AE4E8 * get_m_Cache_0() const { return ___m_Cache_0; }
	inline List_1_t19C69957130AB21842A46224AD233BAE769AE4E8 ** get_address_of_m_Cache_0() { return &___m_Cache_0; }
	inline void set_m_Cache_0(List_1_t19C69957130AB21842A46224AD233BAE769AE4E8 * value)
	{
		___m_Cache_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Cache_0), value);
	}

	inline static int32_t get_offset_of_m_CacheComplete_1() { return static_cast<int32_t>(offsetof(ProxyChain_t58CD16408B22DC2B56772ADD1414604ABEF5AA53, ___m_CacheComplete_1)); }
	inline bool get_m_CacheComplete_1() const { return ___m_CacheComplete_1; }
	inline bool* get_address_of_m_CacheComplete_1() { return &___m_CacheComplete_1; }
	inline void set_m_CacheComplete_1(bool value)
	{
		___m_CacheComplete_1 = value;
	}

	inline static int32_t get_offset_of_m_MainEnumerator_2() { return static_cast<int32_t>(offsetof(ProxyChain_t58CD16408B22DC2B56772ADD1414604ABEF5AA53, ___m_MainEnumerator_2)); }
	inline ProxyEnumerator_t92F0DA133B067B6E5BA335083CDE845D610B7016 * get_m_MainEnumerator_2() const { return ___m_MainEnumerator_2; }
	inline ProxyEnumerator_t92F0DA133B067B6E5BA335083CDE845D610B7016 ** get_address_of_m_MainEnumerator_2() { return &___m_MainEnumerator_2; }
	inline void set_m_MainEnumerator_2(ProxyEnumerator_t92F0DA133B067B6E5BA335083CDE845D610B7016 * value)
	{
		___m_MainEnumerator_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_MainEnumerator_2), value);
	}

	inline static int32_t get_offset_of_m_Destination_3() { return static_cast<int32_t>(offsetof(ProxyChain_t58CD16408B22DC2B56772ADD1414604ABEF5AA53, ___m_Destination_3)); }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * get_m_Destination_3() const { return ___m_Destination_3; }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E ** get_address_of_m_Destination_3() { return &___m_Destination_3; }
	inline void set_m_Destination_3(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * value)
	{
		___m_Destination_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Destination_3), value);
	}

	inline static int32_t get_offset_of_m_HttpAbortDelegate_4() { return static_cast<int32_t>(offsetof(ProxyChain_t58CD16408B22DC2B56772ADD1414604ABEF5AA53, ___m_HttpAbortDelegate_4)); }
	inline HttpAbortDelegate_tBF5D375FB316FE054524CCF1035D90029966A637 * get_m_HttpAbortDelegate_4() const { return ___m_HttpAbortDelegate_4; }
	inline HttpAbortDelegate_tBF5D375FB316FE054524CCF1035D90029966A637 ** get_address_of_m_HttpAbortDelegate_4() { return &___m_HttpAbortDelegate_4; }
	inline void set_m_HttpAbortDelegate_4(HttpAbortDelegate_tBF5D375FB316FE054524CCF1035D90029966A637 * value)
	{
		___m_HttpAbortDelegate_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_HttpAbortDelegate_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROXYCHAIN_T58CD16408B22DC2B56772ADD1414604ABEF5AA53_H
#ifndef PROXYENUMERATOR_T92F0DA133B067B6E5BA335083CDE845D610B7016_H
#define PROXYENUMERATOR_T92F0DA133B067B6E5BA335083CDE845D610B7016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.ProxyChain_ProxyEnumerator
struct  ProxyEnumerator_t92F0DA133B067B6E5BA335083CDE845D610B7016  : public RuntimeObject
{
public:
	// System.Net.ProxyChain System.Net.ProxyChain_ProxyEnumerator::m_Chain
	ProxyChain_t58CD16408B22DC2B56772ADD1414604ABEF5AA53 * ___m_Chain_0;
	// System.Boolean System.Net.ProxyChain_ProxyEnumerator::m_Finished
	bool ___m_Finished_1;
	// System.Int32 System.Net.ProxyChain_ProxyEnumerator::m_CurrentIndex
	int32_t ___m_CurrentIndex_2;
	// System.Boolean System.Net.ProxyChain_ProxyEnumerator::m_TriedDirect
	bool ___m_TriedDirect_3;

public:
	inline static int32_t get_offset_of_m_Chain_0() { return static_cast<int32_t>(offsetof(ProxyEnumerator_t92F0DA133B067B6E5BA335083CDE845D610B7016, ___m_Chain_0)); }
	inline ProxyChain_t58CD16408B22DC2B56772ADD1414604ABEF5AA53 * get_m_Chain_0() const { return ___m_Chain_0; }
	inline ProxyChain_t58CD16408B22DC2B56772ADD1414604ABEF5AA53 ** get_address_of_m_Chain_0() { return &___m_Chain_0; }
	inline void set_m_Chain_0(ProxyChain_t58CD16408B22DC2B56772ADD1414604ABEF5AA53 * value)
	{
		___m_Chain_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Chain_0), value);
	}

	inline static int32_t get_offset_of_m_Finished_1() { return static_cast<int32_t>(offsetof(ProxyEnumerator_t92F0DA133B067B6E5BA335083CDE845D610B7016, ___m_Finished_1)); }
	inline bool get_m_Finished_1() const { return ___m_Finished_1; }
	inline bool* get_address_of_m_Finished_1() { return &___m_Finished_1; }
	inline void set_m_Finished_1(bool value)
	{
		___m_Finished_1 = value;
	}

	inline static int32_t get_offset_of_m_CurrentIndex_2() { return static_cast<int32_t>(offsetof(ProxyEnumerator_t92F0DA133B067B6E5BA335083CDE845D610B7016, ___m_CurrentIndex_2)); }
	inline int32_t get_m_CurrentIndex_2() const { return ___m_CurrentIndex_2; }
	inline int32_t* get_address_of_m_CurrentIndex_2() { return &___m_CurrentIndex_2; }
	inline void set_m_CurrentIndex_2(int32_t value)
	{
		___m_CurrentIndex_2 = value;
	}

	inline static int32_t get_offset_of_m_TriedDirect_3() { return static_cast<int32_t>(offsetof(ProxyEnumerator_t92F0DA133B067B6E5BA335083CDE845D610B7016, ___m_TriedDirect_3)); }
	inline bool get_m_TriedDirect_3() const { return ___m_TriedDirect_3; }
	inline bool* get_address_of_m_TriedDirect_3() { return &___m_TriedDirect_3; }
	inline void set_m_TriedDirect_3(bool value)
	{
		___m_TriedDirect_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROXYENUMERATOR_T92F0DA133B067B6E5BA335083CDE845D610B7016_H
#ifndef SCATTERGATHERBUFFERS_T5090EA13155952525395C6141ACA51BDBE255E6B_H
#define SCATTERGATHERBUFFERS_T5090EA13155952525395C6141ACA51BDBE255E6B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.ScatterGatherBuffers
struct  ScatterGatherBuffers_t5090EA13155952525395C6141ACA51BDBE255E6B  : public RuntimeObject
{
public:
	// System.Net.ScatterGatherBuffers_MemoryChunk System.Net.ScatterGatherBuffers::headChunk
	MemoryChunk_t2B4F05956C1526847FF22066DC4E91A9A297951F * ___headChunk_0;
	// System.Net.ScatterGatherBuffers_MemoryChunk System.Net.ScatterGatherBuffers::currentChunk
	MemoryChunk_t2B4F05956C1526847FF22066DC4E91A9A297951F * ___currentChunk_1;
	// System.Int32 System.Net.ScatterGatherBuffers::nextChunkLength
	int32_t ___nextChunkLength_2;
	// System.Int32 System.Net.ScatterGatherBuffers::totalLength
	int32_t ___totalLength_3;
	// System.Int32 System.Net.ScatterGatherBuffers::chunkCount
	int32_t ___chunkCount_4;

public:
	inline static int32_t get_offset_of_headChunk_0() { return static_cast<int32_t>(offsetof(ScatterGatherBuffers_t5090EA13155952525395C6141ACA51BDBE255E6B, ___headChunk_0)); }
	inline MemoryChunk_t2B4F05956C1526847FF22066DC4E91A9A297951F * get_headChunk_0() const { return ___headChunk_0; }
	inline MemoryChunk_t2B4F05956C1526847FF22066DC4E91A9A297951F ** get_address_of_headChunk_0() { return &___headChunk_0; }
	inline void set_headChunk_0(MemoryChunk_t2B4F05956C1526847FF22066DC4E91A9A297951F * value)
	{
		___headChunk_0 = value;
		Il2CppCodeGenWriteBarrier((&___headChunk_0), value);
	}

	inline static int32_t get_offset_of_currentChunk_1() { return static_cast<int32_t>(offsetof(ScatterGatherBuffers_t5090EA13155952525395C6141ACA51BDBE255E6B, ___currentChunk_1)); }
	inline MemoryChunk_t2B4F05956C1526847FF22066DC4E91A9A297951F * get_currentChunk_1() const { return ___currentChunk_1; }
	inline MemoryChunk_t2B4F05956C1526847FF22066DC4E91A9A297951F ** get_address_of_currentChunk_1() { return &___currentChunk_1; }
	inline void set_currentChunk_1(MemoryChunk_t2B4F05956C1526847FF22066DC4E91A9A297951F * value)
	{
		___currentChunk_1 = value;
		Il2CppCodeGenWriteBarrier((&___currentChunk_1), value);
	}

	inline static int32_t get_offset_of_nextChunkLength_2() { return static_cast<int32_t>(offsetof(ScatterGatherBuffers_t5090EA13155952525395C6141ACA51BDBE255E6B, ___nextChunkLength_2)); }
	inline int32_t get_nextChunkLength_2() const { return ___nextChunkLength_2; }
	inline int32_t* get_address_of_nextChunkLength_2() { return &___nextChunkLength_2; }
	inline void set_nextChunkLength_2(int32_t value)
	{
		___nextChunkLength_2 = value;
	}

	inline static int32_t get_offset_of_totalLength_3() { return static_cast<int32_t>(offsetof(ScatterGatherBuffers_t5090EA13155952525395C6141ACA51BDBE255E6B, ___totalLength_3)); }
	inline int32_t get_totalLength_3() const { return ___totalLength_3; }
	inline int32_t* get_address_of_totalLength_3() { return &___totalLength_3; }
	inline void set_totalLength_3(int32_t value)
	{
		___totalLength_3 = value;
	}

	inline static int32_t get_offset_of_chunkCount_4() { return static_cast<int32_t>(offsetof(ScatterGatherBuffers_t5090EA13155952525395C6141ACA51BDBE255E6B, ___chunkCount_4)); }
	inline int32_t get_chunkCount_4() const { return ___chunkCount_4; }
	inline int32_t* get_address_of_chunkCount_4() { return &___chunkCount_4; }
	inline void set_chunkCount_4(int32_t value)
	{
		___chunkCount_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCATTERGATHERBUFFERS_T5090EA13155952525395C6141ACA51BDBE255E6B_H
#ifndef MEMORYCHUNK_T2B4F05956C1526847FF22066DC4E91A9A297951F_H
#define MEMORYCHUNK_T2B4F05956C1526847FF22066DC4E91A9A297951F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.ScatterGatherBuffers_MemoryChunk
struct  MemoryChunk_t2B4F05956C1526847FF22066DC4E91A9A297951F  : public RuntimeObject
{
public:
	// System.Byte[] System.Net.ScatterGatherBuffers_MemoryChunk::Buffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___Buffer_0;
	// System.Int32 System.Net.ScatterGatherBuffers_MemoryChunk::FreeOffset
	int32_t ___FreeOffset_1;
	// System.Net.ScatterGatherBuffers_MemoryChunk System.Net.ScatterGatherBuffers_MemoryChunk::Next
	MemoryChunk_t2B4F05956C1526847FF22066DC4E91A9A297951F * ___Next_2;

public:
	inline static int32_t get_offset_of_Buffer_0() { return static_cast<int32_t>(offsetof(MemoryChunk_t2B4F05956C1526847FF22066DC4E91A9A297951F, ___Buffer_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_Buffer_0() const { return ___Buffer_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_Buffer_0() { return &___Buffer_0; }
	inline void set_Buffer_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___Buffer_0 = value;
		Il2CppCodeGenWriteBarrier((&___Buffer_0), value);
	}

	inline static int32_t get_offset_of_FreeOffset_1() { return static_cast<int32_t>(offsetof(MemoryChunk_t2B4F05956C1526847FF22066DC4E91A9A297951F, ___FreeOffset_1)); }
	inline int32_t get_FreeOffset_1() const { return ___FreeOffset_1; }
	inline int32_t* get_address_of_FreeOffset_1() { return &___FreeOffset_1; }
	inline void set_FreeOffset_1(int32_t value)
	{
		___FreeOffset_1 = value;
	}

	inline static int32_t get_offset_of_Next_2() { return static_cast<int32_t>(offsetof(MemoryChunk_t2B4F05956C1526847FF22066DC4E91A9A297951F, ___Next_2)); }
	inline MemoryChunk_t2B4F05956C1526847FF22066DC4E91A9A297951F * get_Next_2() const { return ___Next_2; }
	inline MemoryChunk_t2B4F05956C1526847FF22066DC4E91A9A297951F ** get_address_of_Next_2() { return &___Next_2; }
	inline void set_Next_2(MemoryChunk_t2B4F05956C1526847FF22066DC4E91A9A297951F * value)
	{
		___Next_2 = value;
		Il2CppCodeGenWriteBarrier((&___Next_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMORYCHUNK_T2B4F05956C1526847FF22066DC4E91A9A297951F_H
#ifndef SECCHANNELBINDINGS_TE856680A2915B8FACAC224539BF99AE2AC8DAB71_H
#define SECCHANNELBINDINGS_TE856680A2915B8FACAC224539BF99AE2AC8DAB71_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.SecChannelBindings
struct  SecChannelBindings_tE856680A2915B8FACAC224539BF99AE2AC8DAB71  : public RuntimeObject
{
public:
	// System.Int32 System.Net.SecChannelBindings::dwInitiatorAddrType
	int32_t ___dwInitiatorAddrType_0;
	// System.Int32 System.Net.SecChannelBindings::cbInitiatorLength
	int32_t ___cbInitiatorLength_1;
	// System.Int32 System.Net.SecChannelBindings::dwInitiatorOffset
	int32_t ___dwInitiatorOffset_2;
	// System.Int32 System.Net.SecChannelBindings::dwAcceptorAddrType
	int32_t ___dwAcceptorAddrType_3;
	// System.Int32 System.Net.SecChannelBindings::cbAcceptorLength
	int32_t ___cbAcceptorLength_4;
	// System.Int32 System.Net.SecChannelBindings::dwAcceptorOffset
	int32_t ___dwAcceptorOffset_5;
	// System.Int32 System.Net.SecChannelBindings::cbApplicationDataLength
	int32_t ___cbApplicationDataLength_6;
	// System.Int32 System.Net.SecChannelBindings::dwApplicationDataOffset
	int32_t ___dwApplicationDataOffset_7;

public:
	inline static int32_t get_offset_of_dwInitiatorAddrType_0() { return static_cast<int32_t>(offsetof(SecChannelBindings_tE856680A2915B8FACAC224539BF99AE2AC8DAB71, ___dwInitiatorAddrType_0)); }
	inline int32_t get_dwInitiatorAddrType_0() const { return ___dwInitiatorAddrType_0; }
	inline int32_t* get_address_of_dwInitiatorAddrType_0() { return &___dwInitiatorAddrType_0; }
	inline void set_dwInitiatorAddrType_0(int32_t value)
	{
		___dwInitiatorAddrType_0 = value;
	}

	inline static int32_t get_offset_of_cbInitiatorLength_1() { return static_cast<int32_t>(offsetof(SecChannelBindings_tE856680A2915B8FACAC224539BF99AE2AC8DAB71, ___cbInitiatorLength_1)); }
	inline int32_t get_cbInitiatorLength_1() const { return ___cbInitiatorLength_1; }
	inline int32_t* get_address_of_cbInitiatorLength_1() { return &___cbInitiatorLength_1; }
	inline void set_cbInitiatorLength_1(int32_t value)
	{
		___cbInitiatorLength_1 = value;
	}

	inline static int32_t get_offset_of_dwInitiatorOffset_2() { return static_cast<int32_t>(offsetof(SecChannelBindings_tE856680A2915B8FACAC224539BF99AE2AC8DAB71, ___dwInitiatorOffset_2)); }
	inline int32_t get_dwInitiatorOffset_2() const { return ___dwInitiatorOffset_2; }
	inline int32_t* get_address_of_dwInitiatorOffset_2() { return &___dwInitiatorOffset_2; }
	inline void set_dwInitiatorOffset_2(int32_t value)
	{
		___dwInitiatorOffset_2 = value;
	}

	inline static int32_t get_offset_of_dwAcceptorAddrType_3() { return static_cast<int32_t>(offsetof(SecChannelBindings_tE856680A2915B8FACAC224539BF99AE2AC8DAB71, ___dwAcceptorAddrType_3)); }
	inline int32_t get_dwAcceptorAddrType_3() const { return ___dwAcceptorAddrType_3; }
	inline int32_t* get_address_of_dwAcceptorAddrType_3() { return &___dwAcceptorAddrType_3; }
	inline void set_dwAcceptorAddrType_3(int32_t value)
	{
		___dwAcceptorAddrType_3 = value;
	}

	inline static int32_t get_offset_of_cbAcceptorLength_4() { return static_cast<int32_t>(offsetof(SecChannelBindings_tE856680A2915B8FACAC224539BF99AE2AC8DAB71, ___cbAcceptorLength_4)); }
	inline int32_t get_cbAcceptorLength_4() const { return ___cbAcceptorLength_4; }
	inline int32_t* get_address_of_cbAcceptorLength_4() { return &___cbAcceptorLength_4; }
	inline void set_cbAcceptorLength_4(int32_t value)
	{
		___cbAcceptorLength_4 = value;
	}

	inline static int32_t get_offset_of_dwAcceptorOffset_5() { return static_cast<int32_t>(offsetof(SecChannelBindings_tE856680A2915B8FACAC224539BF99AE2AC8DAB71, ___dwAcceptorOffset_5)); }
	inline int32_t get_dwAcceptorOffset_5() const { return ___dwAcceptorOffset_5; }
	inline int32_t* get_address_of_dwAcceptorOffset_5() { return &___dwAcceptorOffset_5; }
	inline void set_dwAcceptorOffset_5(int32_t value)
	{
		___dwAcceptorOffset_5 = value;
	}

	inline static int32_t get_offset_of_cbApplicationDataLength_6() { return static_cast<int32_t>(offsetof(SecChannelBindings_tE856680A2915B8FACAC224539BF99AE2AC8DAB71, ___cbApplicationDataLength_6)); }
	inline int32_t get_cbApplicationDataLength_6() const { return ___cbApplicationDataLength_6; }
	inline int32_t* get_address_of_cbApplicationDataLength_6() { return &___cbApplicationDataLength_6; }
	inline void set_cbApplicationDataLength_6(int32_t value)
	{
		___cbApplicationDataLength_6 = value;
	}

	inline static int32_t get_offset_of_dwApplicationDataOffset_7() { return static_cast<int32_t>(offsetof(SecChannelBindings_tE856680A2915B8FACAC224539BF99AE2AC8DAB71, ___dwApplicationDataOffset_7)); }
	inline int32_t get_dwApplicationDataOffset_7() const { return ___dwApplicationDataOffset_7; }
	inline int32_t* get_address_of_dwApplicationDataOffset_7() { return &___dwApplicationDataOffset_7; }
	inline void set_dwApplicationDataOffset_7(int32_t value)
	{
		___dwApplicationDataOffset_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Net.SecChannelBindings
struct SecChannelBindings_tE856680A2915B8FACAC224539BF99AE2AC8DAB71_marshaled_pinvoke
{
	int32_t ___dwInitiatorAddrType_0;
	int32_t ___cbInitiatorLength_1;
	int32_t ___dwInitiatorOffset_2;
	int32_t ___dwAcceptorAddrType_3;
	int32_t ___cbAcceptorLength_4;
	int32_t ___dwAcceptorOffset_5;
	int32_t ___cbApplicationDataLength_6;
	int32_t ___dwApplicationDataOffset_7;
};
// Native definition for COM marshalling of System.Net.SecChannelBindings
struct SecChannelBindings_tE856680A2915B8FACAC224539BF99AE2AC8DAB71_marshaled_com
{
	int32_t ___dwInitiatorAddrType_0;
	int32_t ___cbInitiatorLength_1;
	int32_t ___dwInitiatorOffset_2;
	int32_t ___dwAcceptorAddrType_3;
	int32_t ___cbAcceptorLength_4;
	int32_t ___dwAcceptorOffset_5;
	int32_t ___cbApplicationDataLength_6;
	int32_t ___dwApplicationDataOffset_7;
};
#endif // SECCHANNELBINDINGS_TE856680A2915B8FACAC224539BF99AE2AC8DAB71_H
#ifndef SECURITYBUFFERDESCRIPTOR_TE3CAC792561CE08ADCA2530C44649FC052E719C8_H
#define SECURITYBUFFERDESCRIPTOR_TE3CAC792561CE08ADCA2530C44649FC052E719C8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.SecurityBufferDescriptor
struct  SecurityBufferDescriptor_tE3CAC792561CE08ADCA2530C44649FC052E719C8  : public RuntimeObject
{
public:
	// System.Int32 System.Net.SecurityBufferDescriptor::Version
	int32_t ___Version_0;
	// System.Int32 System.Net.SecurityBufferDescriptor::Count
	int32_t ___Count_1;
	// System.Void* System.Net.SecurityBufferDescriptor::UnmanagedPointer
	void* ___UnmanagedPointer_2;

public:
	inline static int32_t get_offset_of_Version_0() { return static_cast<int32_t>(offsetof(SecurityBufferDescriptor_tE3CAC792561CE08ADCA2530C44649FC052E719C8, ___Version_0)); }
	inline int32_t get_Version_0() const { return ___Version_0; }
	inline int32_t* get_address_of_Version_0() { return &___Version_0; }
	inline void set_Version_0(int32_t value)
	{
		___Version_0 = value;
	}

	inline static int32_t get_offset_of_Count_1() { return static_cast<int32_t>(offsetof(SecurityBufferDescriptor_tE3CAC792561CE08ADCA2530C44649FC052E719C8, ___Count_1)); }
	inline int32_t get_Count_1() const { return ___Count_1; }
	inline int32_t* get_address_of_Count_1() { return &___Count_1; }
	inline void set_Count_1(int32_t value)
	{
		___Count_1 = value;
	}

	inline static int32_t get_offset_of_UnmanagedPointer_2() { return static_cast<int32_t>(offsetof(SecurityBufferDescriptor_tE3CAC792561CE08ADCA2530C44649FC052E719C8, ___UnmanagedPointer_2)); }
	inline void* get_UnmanagedPointer_2() const { return ___UnmanagedPointer_2; }
	inline void** get_address_of_UnmanagedPointer_2() { return &___UnmanagedPointer_2; }
	inline void set_UnmanagedPointer_2(void* value)
	{
		___UnmanagedPointer_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Net.SecurityBufferDescriptor
struct SecurityBufferDescriptor_tE3CAC792561CE08ADCA2530C44649FC052E719C8_marshaled_pinvoke
{
	int32_t ___Version_0;
	int32_t ___Count_1;
	void* ___UnmanagedPointer_2;
};
// Native definition for COM marshalling of System.Net.SecurityBufferDescriptor
struct SecurityBufferDescriptor_tE3CAC792561CE08ADCA2530C44649FC052E719C8_marshaled_com
{
	int32_t ___Version_0;
	int32_t ___Count_1;
	void* ___UnmanagedPointer_2;
};
#endif // SECURITYBUFFERDESCRIPTOR_TE3CAC792561CE08ADCA2530C44649FC052E719C8_H
#ifndef SERVICENAMESTORE_TF45F4346CE113F34D3E08E515FD32642512CEA37_H
#define SERVICENAMESTORE_TF45F4346CE113F34D3E08E515FD32642512CEA37_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.ServiceNameStore
struct  ServiceNameStore_tF45F4346CE113F34D3E08E515FD32642512CEA37  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.String> System.Net.ServiceNameStore::serviceNames
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___serviceNames_0;
	// System.Security.Authentication.ExtendedProtection.ServiceNameCollection System.Net.ServiceNameStore::serviceNameCollection
	ServiceNameCollection_t3C60F59BA95A4A706BB70439AE03C07D09796C2C * ___serviceNameCollection_1;

public:
	inline static int32_t get_offset_of_serviceNames_0() { return static_cast<int32_t>(offsetof(ServiceNameStore_tF45F4346CE113F34D3E08E515FD32642512CEA37, ___serviceNames_0)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_serviceNames_0() const { return ___serviceNames_0; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_serviceNames_0() { return &___serviceNames_0; }
	inline void set_serviceNames_0(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___serviceNames_0 = value;
		Il2CppCodeGenWriteBarrier((&___serviceNames_0), value);
	}

	inline static int32_t get_offset_of_serviceNameCollection_1() { return static_cast<int32_t>(offsetof(ServiceNameStore_tF45F4346CE113F34D3E08E515FD32642512CEA37, ___serviceNameCollection_1)); }
	inline ServiceNameCollection_t3C60F59BA95A4A706BB70439AE03C07D09796C2C * get_serviceNameCollection_1() const { return ___serviceNameCollection_1; }
	inline ServiceNameCollection_t3C60F59BA95A4A706BB70439AE03C07D09796C2C ** get_address_of_serviceNameCollection_1() { return &___serviceNameCollection_1; }
	inline void set_serviceNameCollection_1(ServiceNameCollection_t3C60F59BA95A4A706BB70439AE03C07D09796C2C * value)
	{
		___serviceNameCollection_1 = value;
		Il2CppCodeGenWriteBarrier((&___serviceNameCollection_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERVICENAMESTORE_TF45F4346CE113F34D3E08E515FD32642512CEA37_H
#ifndef SOCKETADDRESS_TFD1A629405590229D8DAA15D03083147B767C969_H
#define SOCKETADDRESS_TFD1A629405590229D8DAA15D03083147B767C969_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.SocketAddress
struct  SocketAddress_tFD1A629405590229D8DAA15D03083147B767C969  : public RuntimeObject
{
public:
	// System.Int32 System.Net.SocketAddress::m_Size
	int32_t ___m_Size_2;
	// System.Byte[] System.Net.SocketAddress::m_Buffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___m_Buffer_3;
	// System.Boolean System.Net.SocketAddress::m_changed
	bool ___m_changed_6;
	// System.Int32 System.Net.SocketAddress::m_hash
	int32_t ___m_hash_7;

public:
	inline static int32_t get_offset_of_m_Size_2() { return static_cast<int32_t>(offsetof(SocketAddress_tFD1A629405590229D8DAA15D03083147B767C969, ___m_Size_2)); }
	inline int32_t get_m_Size_2() const { return ___m_Size_2; }
	inline int32_t* get_address_of_m_Size_2() { return &___m_Size_2; }
	inline void set_m_Size_2(int32_t value)
	{
		___m_Size_2 = value;
	}

	inline static int32_t get_offset_of_m_Buffer_3() { return static_cast<int32_t>(offsetof(SocketAddress_tFD1A629405590229D8DAA15D03083147B767C969, ___m_Buffer_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_m_Buffer_3() const { return ___m_Buffer_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_m_Buffer_3() { return &___m_Buffer_3; }
	inline void set_m_Buffer_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___m_Buffer_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Buffer_3), value);
	}

	inline static int32_t get_offset_of_m_changed_6() { return static_cast<int32_t>(offsetof(SocketAddress_tFD1A629405590229D8DAA15D03083147B767C969, ___m_changed_6)); }
	inline bool get_m_changed_6() const { return ___m_changed_6; }
	inline bool* get_address_of_m_changed_6() { return &___m_changed_6; }
	inline void set_m_changed_6(bool value)
	{
		___m_changed_6 = value;
	}

	inline static int32_t get_offset_of_m_hash_7() { return static_cast<int32_t>(offsetof(SocketAddress_tFD1A629405590229D8DAA15D03083147B767C969, ___m_hash_7)); }
	inline int32_t get_m_hash_7() const { return ___m_hash_7; }
	inline int32_t* get_address_of_m_hash_7() { return &___m_hash_7; }
	inline void set_m_hash_7(int32_t value)
	{
		___m_hash_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOCKETADDRESS_TFD1A629405590229D8DAA15D03083147B767C969_H
#ifndef SPLITWRITESSTATE_T7D1B22995BE24ABBE5D8C213BF991B1614789100_H
#define SPLITWRITESSTATE_T7D1B22995BE24ABBE5D8C213BF991B1614789100_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.SplitWritesState
struct  SplitWritesState_t7D1B22995BE24ABBE5D8C213BF991B1614789100  : public RuntimeObject
{
public:
	// System.Net.BufferOffsetSize[] System.Net.SplitWritesState::_UserBuffers
	BufferOffsetSizeU5BU5D_tA2E04E7114D20AD4B552D2684A84A326A9DBFE27* ____UserBuffers_1;
	// System.Int32 System.Net.SplitWritesState::_Index
	int32_t ____Index_2;
	// System.Int32 System.Net.SplitWritesState::_LastBufferConsumed
	int32_t ____LastBufferConsumed_3;
	// System.Net.BufferOffsetSize[] System.Net.SplitWritesState::_RealBuffers
	BufferOffsetSizeU5BU5D_tA2E04E7114D20AD4B552D2684A84A326A9DBFE27* ____RealBuffers_4;

public:
	inline static int32_t get_offset_of__UserBuffers_1() { return static_cast<int32_t>(offsetof(SplitWritesState_t7D1B22995BE24ABBE5D8C213BF991B1614789100, ____UserBuffers_1)); }
	inline BufferOffsetSizeU5BU5D_tA2E04E7114D20AD4B552D2684A84A326A9DBFE27* get__UserBuffers_1() const { return ____UserBuffers_1; }
	inline BufferOffsetSizeU5BU5D_tA2E04E7114D20AD4B552D2684A84A326A9DBFE27** get_address_of__UserBuffers_1() { return &____UserBuffers_1; }
	inline void set__UserBuffers_1(BufferOffsetSizeU5BU5D_tA2E04E7114D20AD4B552D2684A84A326A9DBFE27* value)
	{
		____UserBuffers_1 = value;
		Il2CppCodeGenWriteBarrier((&____UserBuffers_1), value);
	}

	inline static int32_t get_offset_of__Index_2() { return static_cast<int32_t>(offsetof(SplitWritesState_t7D1B22995BE24ABBE5D8C213BF991B1614789100, ____Index_2)); }
	inline int32_t get__Index_2() const { return ____Index_2; }
	inline int32_t* get_address_of__Index_2() { return &____Index_2; }
	inline void set__Index_2(int32_t value)
	{
		____Index_2 = value;
	}

	inline static int32_t get_offset_of__LastBufferConsumed_3() { return static_cast<int32_t>(offsetof(SplitWritesState_t7D1B22995BE24ABBE5D8C213BF991B1614789100, ____LastBufferConsumed_3)); }
	inline int32_t get__LastBufferConsumed_3() const { return ____LastBufferConsumed_3; }
	inline int32_t* get_address_of__LastBufferConsumed_3() { return &____LastBufferConsumed_3; }
	inline void set__LastBufferConsumed_3(int32_t value)
	{
		____LastBufferConsumed_3 = value;
	}

	inline static int32_t get_offset_of__RealBuffers_4() { return static_cast<int32_t>(offsetof(SplitWritesState_t7D1B22995BE24ABBE5D8C213BF991B1614789100, ____RealBuffers_4)); }
	inline BufferOffsetSizeU5BU5D_tA2E04E7114D20AD4B552D2684A84A326A9DBFE27* get__RealBuffers_4() const { return ____RealBuffers_4; }
	inline BufferOffsetSizeU5BU5D_tA2E04E7114D20AD4B552D2684A84A326A9DBFE27** get_address_of__RealBuffers_4() { return &____RealBuffers_4; }
	inline void set__RealBuffers_4(BufferOffsetSizeU5BU5D_tA2E04E7114D20AD4B552D2684A84A326A9DBFE27* value)
	{
		____RealBuffers_4 = value;
		Il2CppCodeGenWriteBarrier((&____RealBuffers_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPLITWRITESSTATE_T7D1B22995BE24ABBE5D8C213BF991B1614789100_H
#ifndef TIMERTHREAD_T834F44C51FF3F25350F8B4E03670F941F352DF01_H
#define TIMERTHREAD_T834F44C51FF3F25350F8B4E03670F941F352DF01_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.TimerThread
struct  TimerThread_t834F44C51FF3F25350F8B4E03670F941F352DF01  : public RuntimeObject
{
public:

public:
};

struct TimerThread_t834F44C51FF3F25350F8B4E03670F941F352DF01_StaticFields
{
public:
	// System.Collections.Generic.LinkedList`1<System.WeakReference> System.Net.TimerThread::s_Queues
	LinkedList_1_t4BA9E2BFC78453D2CB909F7665EBFC5366C2EE25 * ___s_Queues_3;
	// System.Collections.Generic.LinkedList`1<System.WeakReference> System.Net.TimerThread::s_NewQueues
	LinkedList_1_t4BA9E2BFC78453D2CB909F7665EBFC5366C2EE25 * ___s_NewQueues_4;
	// System.Int32 System.Net.TimerThread::s_ThreadState
	int32_t ___s_ThreadState_5;
	// System.Threading.AutoResetEvent System.Net.TimerThread::s_ThreadReadyEvent
	AutoResetEvent_t2A1182CEEE4E184587D4DEAA4F382B810B21D3B7 * ___s_ThreadReadyEvent_6;
	// System.Threading.ManualResetEvent System.Net.TimerThread::s_ThreadShutdownEvent
	ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * ___s_ThreadShutdownEvent_7;
	// System.Threading.WaitHandle[] System.Net.TimerThread::s_ThreadEvents
	WaitHandleU5BU5D_t79FF2E6AC099CC1FDD2B24F21A72D36BA31298CC* ___s_ThreadEvents_8;
	// System.Int32 System.Net.TimerThread::s_CacheScanIteration
	int32_t ___s_CacheScanIteration_9;
	// System.Collections.Hashtable System.Net.TimerThread::s_QueuesCache
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___s_QueuesCache_10;

public:
	inline static int32_t get_offset_of_s_Queues_3() { return static_cast<int32_t>(offsetof(TimerThread_t834F44C51FF3F25350F8B4E03670F941F352DF01_StaticFields, ___s_Queues_3)); }
	inline LinkedList_1_t4BA9E2BFC78453D2CB909F7665EBFC5366C2EE25 * get_s_Queues_3() const { return ___s_Queues_3; }
	inline LinkedList_1_t4BA9E2BFC78453D2CB909F7665EBFC5366C2EE25 ** get_address_of_s_Queues_3() { return &___s_Queues_3; }
	inline void set_s_Queues_3(LinkedList_1_t4BA9E2BFC78453D2CB909F7665EBFC5366C2EE25 * value)
	{
		___s_Queues_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_Queues_3), value);
	}

	inline static int32_t get_offset_of_s_NewQueues_4() { return static_cast<int32_t>(offsetof(TimerThread_t834F44C51FF3F25350F8B4E03670F941F352DF01_StaticFields, ___s_NewQueues_4)); }
	inline LinkedList_1_t4BA9E2BFC78453D2CB909F7665EBFC5366C2EE25 * get_s_NewQueues_4() const { return ___s_NewQueues_4; }
	inline LinkedList_1_t4BA9E2BFC78453D2CB909F7665EBFC5366C2EE25 ** get_address_of_s_NewQueues_4() { return &___s_NewQueues_4; }
	inline void set_s_NewQueues_4(LinkedList_1_t4BA9E2BFC78453D2CB909F7665EBFC5366C2EE25 * value)
	{
		___s_NewQueues_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_NewQueues_4), value);
	}

	inline static int32_t get_offset_of_s_ThreadState_5() { return static_cast<int32_t>(offsetof(TimerThread_t834F44C51FF3F25350F8B4E03670F941F352DF01_StaticFields, ___s_ThreadState_5)); }
	inline int32_t get_s_ThreadState_5() const { return ___s_ThreadState_5; }
	inline int32_t* get_address_of_s_ThreadState_5() { return &___s_ThreadState_5; }
	inline void set_s_ThreadState_5(int32_t value)
	{
		___s_ThreadState_5 = value;
	}

	inline static int32_t get_offset_of_s_ThreadReadyEvent_6() { return static_cast<int32_t>(offsetof(TimerThread_t834F44C51FF3F25350F8B4E03670F941F352DF01_StaticFields, ___s_ThreadReadyEvent_6)); }
	inline AutoResetEvent_t2A1182CEEE4E184587D4DEAA4F382B810B21D3B7 * get_s_ThreadReadyEvent_6() const { return ___s_ThreadReadyEvent_6; }
	inline AutoResetEvent_t2A1182CEEE4E184587D4DEAA4F382B810B21D3B7 ** get_address_of_s_ThreadReadyEvent_6() { return &___s_ThreadReadyEvent_6; }
	inline void set_s_ThreadReadyEvent_6(AutoResetEvent_t2A1182CEEE4E184587D4DEAA4F382B810B21D3B7 * value)
	{
		___s_ThreadReadyEvent_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_ThreadReadyEvent_6), value);
	}

	inline static int32_t get_offset_of_s_ThreadShutdownEvent_7() { return static_cast<int32_t>(offsetof(TimerThread_t834F44C51FF3F25350F8B4E03670F941F352DF01_StaticFields, ___s_ThreadShutdownEvent_7)); }
	inline ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * get_s_ThreadShutdownEvent_7() const { return ___s_ThreadShutdownEvent_7; }
	inline ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 ** get_address_of_s_ThreadShutdownEvent_7() { return &___s_ThreadShutdownEvent_7; }
	inline void set_s_ThreadShutdownEvent_7(ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * value)
	{
		___s_ThreadShutdownEvent_7 = value;
		Il2CppCodeGenWriteBarrier((&___s_ThreadShutdownEvent_7), value);
	}

	inline static int32_t get_offset_of_s_ThreadEvents_8() { return static_cast<int32_t>(offsetof(TimerThread_t834F44C51FF3F25350F8B4E03670F941F352DF01_StaticFields, ___s_ThreadEvents_8)); }
	inline WaitHandleU5BU5D_t79FF2E6AC099CC1FDD2B24F21A72D36BA31298CC* get_s_ThreadEvents_8() const { return ___s_ThreadEvents_8; }
	inline WaitHandleU5BU5D_t79FF2E6AC099CC1FDD2B24F21A72D36BA31298CC** get_address_of_s_ThreadEvents_8() { return &___s_ThreadEvents_8; }
	inline void set_s_ThreadEvents_8(WaitHandleU5BU5D_t79FF2E6AC099CC1FDD2B24F21A72D36BA31298CC* value)
	{
		___s_ThreadEvents_8 = value;
		Il2CppCodeGenWriteBarrier((&___s_ThreadEvents_8), value);
	}

	inline static int32_t get_offset_of_s_CacheScanIteration_9() { return static_cast<int32_t>(offsetof(TimerThread_t834F44C51FF3F25350F8B4E03670F941F352DF01_StaticFields, ___s_CacheScanIteration_9)); }
	inline int32_t get_s_CacheScanIteration_9() const { return ___s_CacheScanIteration_9; }
	inline int32_t* get_address_of_s_CacheScanIteration_9() { return &___s_CacheScanIteration_9; }
	inline void set_s_CacheScanIteration_9(int32_t value)
	{
		___s_CacheScanIteration_9 = value;
	}

	inline static int32_t get_offset_of_s_QueuesCache_10() { return static_cast<int32_t>(offsetof(TimerThread_t834F44C51FF3F25350F8B4E03670F941F352DF01_StaticFields, ___s_QueuesCache_10)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_s_QueuesCache_10() const { return ___s_QueuesCache_10; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_s_QueuesCache_10() { return &___s_QueuesCache_10; }
	inline void set_s_QueuesCache_10(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___s_QueuesCache_10 = value;
		Il2CppCodeGenWriteBarrier((&___s_QueuesCache_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMERTHREAD_T834F44C51FF3F25350F8B4E03670F941F352DF01_H
#ifndef QUEUE_TCCFF6A2FCF584216AEDA04A483FB808E2D493643_H
#define QUEUE_TCCFF6A2FCF584216AEDA04A483FB808E2D493643_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.TimerThread_Queue
struct  Queue_tCCFF6A2FCF584216AEDA04A483FB808E2D493643  : public RuntimeObject
{
public:
	// System.Int32 System.Net.TimerThread_Queue::m_DurationMilliseconds
	int32_t ___m_DurationMilliseconds_0;

public:
	inline static int32_t get_offset_of_m_DurationMilliseconds_0() { return static_cast<int32_t>(offsetof(Queue_tCCFF6A2FCF584216AEDA04A483FB808E2D493643, ___m_DurationMilliseconds_0)); }
	inline int32_t get_m_DurationMilliseconds_0() const { return ___m_DurationMilliseconds_0; }
	inline int32_t* get_address_of_m_DurationMilliseconds_0() { return &___m_DurationMilliseconds_0; }
	inline void set_m_DurationMilliseconds_0(int32_t value)
	{
		___m_DurationMilliseconds_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUEUE_TCCFF6A2FCF584216AEDA04A483FB808E2D493643_H
#ifndef TIMER_T3B21B1013E27B5DC9FED14EC0921A5F51230D46F_H
#define TIMER_T3B21B1013E27B5DC9FED14EC0921A5F51230D46F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.TimerThread_Timer
struct  Timer_t3B21B1013E27B5DC9FED14EC0921A5F51230D46F  : public RuntimeObject
{
public:
	// System.Int32 System.Net.TimerThread_Timer::m_StartTimeMilliseconds
	int32_t ___m_StartTimeMilliseconds_0;
	// System.Int32 System.Net.TimerThread_Timer::m_DurationMilliseconds
	int32_t ___m_DurationMilliseconds_1;

public:
	inline static int32_t get_offset_of_m_StartTimeMilliseconds_0() { return static_cast<int32_t>(offsetof(Timer_t3B21B1013E27B5DC9FED14EC0921A5F51230D46F, ___m_StartTimeMilliseconds_0)); }
	inline int32_t get_m_StartTimeMilliseconds_0() const { return ___m_StartTimeMilliseconds_0; }
	inline int32_t* get_address_of_m_StartTimeMilliseconds_0() { return &___m_StartTimeMilliseconds_0; }
	inline void set_m_StartTimeMilliseconds_0(int32_t value)
	{
		___m_StartTimeMilliseconds_0 = value;
	}

	inline static int32_t get_offset_of_m_DurationMilliseconds_1() { return static_cast<int32_t>(offsetof(Timer_t3B21B1013E27B5DC9FED14EC0921A5F51230D46F, ___m_DurationMilliseconds_1)); }
	inline int32_t get_m_DurationMilliseconds_1() const { return ___m_DurationMilliseconds_1; }
	inline int32_t* get_address_of_m_DurationMilliseconds_1() { return &___m_DurationMilliseconds_1; }
	inline void set_m_DurationMilliseconds_1(int32_t value)
	{
		___m_DurationMilliseconds_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMER_T3B21B1013E27B5DC9FED14EC0921A5F51230D46F_H
#ifndef TRANSPORTCONTEXT_T1753CC7BFFA637B35BE353AAE4452C4371F89A9F_H
#define TRANSPORTCONTEXT_T1753CC7BFFA637B35BE353AAE4452C4371F89A9F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.TransportContext
struct  TransportContext_t1753CC7BFFA637B35BE353AAE4452C4371F89A9F  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSPORTCONTEXT_T1753CC7BFFA637B35BE353AAE4452C4371F89A9F_H
#ifndef WEBEXCEPTIONMAPPING_T4E7EF581D0224FFC2F2C8556EF320557517A378A_H
#define WEBEXCEPTIONMAPPING_T4E7EF581D0224FFC2F2C8556EF320557517A378A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebExceptionMapping
struct  WebExceptionMapping_t4E7EF581D0224FFC2F2C8556EF320557517A378A  : public RuntimeObject
{
public:

public:
};

struct WebExceptionMapping_t4E7EF581D0224FFC2F2C8556EF320557517A378A_StaticFields
{
public:
	// System.String[] System.Net.WebExceptionMapping::s_Mapping
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___s_Mapping_0;

public:
	inline static int32_t get_offset_of_s_Mapping_0() { return static_cast<int32_t>(offsetof(WebExceptionMapping_t4E7EF581D0224FFC2F2C8556EF320557517A378A_StaticFields, ___s_Mapping_0)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_s_Mapping_0() const { return ___s_Mapping_0; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_s_Mapping_0() { return &___s_Mapping_0; }
	inline void set_s_Mapping_0(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___s_Mapping_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Mapping_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBEXCEPTIONMAPPING_T4E7EF581D0224FFC2F2C8556EF320557517A378A_H
#ifndef HEADERENCODING_T8DF9E931588E144C16512A8B643C07C9197B05FA_H
#define HEADERENCODING_T8DF9E931588E144C16512A8B643C07C9197B05FA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebHeaderCollection_HeaderEncoding
struct  HeaderEncoding_t8DF9E931588E144C16512A8B643C07C9197B05FA  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEADERENCODING_T8DF9E931588E144C16512A8B643C07C9197B05FA_H
#ifndef WEBPROXYDATABUILDER_T8FB484D8DDC07224E75F151C09E491B702C1A75A_H
#define WEBPROXYDATABUILDER_T8FB484D8DDC07224E75F151C09E491B702C1A75A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebProxyDataBuilder
struct  WebProxyDataBuilder_t8FB484D8DDC07224E75F151C09E491B702C1A75A  : public RuntimeObject
{
public:
	// System.Net.WebProxyData System.Net.WebProxyDataBuilder::m_Result
	WebProxyData_t12D22F3302B5ED2B04EA88CE36650BB097E46922 * ___m_Result_3;

public:
	inline static int32_t get_offset_of_m_Result_3() { return static_cast<int32_t>(offsetof(WebProxyDataBuilder_t8FB484D8DDC07224E75F151C09E491B702C1A75A, ___m_Result_3)); }
	inline WebProxyData_t12D22F3302B5ED2B04EA88CE36650BB097E46922 * get_m_Result_3() const { return ___m_Result_3; }
	inline WebProxyData_t12D22F3302B5ED2B04EA88CE36650BB097E46922 ** get_address_of_m_Result_3() { return &___m_Result_3; }
	inline void set_m_Result_3(WebProxyData_t12D22F3302B5ED2B04EA88CE36650BB097E46922 * value)
	{
		___m_Result_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Result_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBPROXYDATABUILDER_T8FB484D8DDC07224E75F151C09E491B702C1A75A_H
#ifndef U3CU3EC__DISPLAYCLASS78_0_TC7AC5DA365F8D5A75E48F83C5D127EF753913861_H
#define U3CU3EC__DISPLAYCLASS78_0_TC7AC5DA365F8D5A75E48F83C5D127EF753913861_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebRequest_<>c__DisplayClass78_0
struct  U3CU3Ec__DisplayClass78_0_tC7AC5DA365F8D5A75E48F83C5D127EF753913861  : public RuntimeObject
{
public:
	// System.Security.Principal.WindowsIdentity System.Net.WebRequest_<>c__DisplayClass78_0::currentUser
	WindowsIdentity_t18859A4ADBD52470392E1AA64255F45A4970DD87 * ___currentUser_0;
	// System.Net.WebRequest System.Net.WebRequest_<>c__DisplayClass78_0::<>4__this
	WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_currentUser_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass78_0_tC7AC5DA365F8D5A75E48F83C5D127EF753913861, ___currentUser_0)); }
	inline WindowsIdentity_t18859A4ADBD52470392E1AA64255F45A4970DD87 * get_currentUser_0() const { return ___currentUser_0; }
	inline WindowsIdentity_t18859A4ADBD52470392E1AA64255F45A4970DD87 ** get_address_of_currentUser_0() { return &___currentUser_0; }
	inline void set_currentUser_0(WindowsIdentity_t18859A4ADBD52470392E1AA64255F45A4970DD87 * value)
	{
		___currentUser_0 = value;
		Il2CppCodeGenWriteBarrier((&___currentUser_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass78_0_tC7AC5DA365F8D5A75E48F83C5D127EF753913861, ___U3CU3E4__this_1)); }
	inline WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS78_0_TC7AC5DA365F8D5A75E48F83C5D127EF753913861_H
#ifndef U3CU3EC__DISPLAYCLASS79_0_T4CA87BE9AA29F9315C6AD70F8C79A62C7727013F_H
#define U3CU3EC__DISPLAYCLASS79_0_T4CA87BE9AA29F9315C6AD70F8C79A62C7727013F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebRequest_<>c__DisplayClass79_0
struct  U3CU3Ec__DisplayClass79_0_t4CA87BE9AA29F9315C6AD70F8C79A62C7727013F  : public RuntimeObject
{
public:
	// System.Security.Principal.WindowsIdentity System.Net.WebRequest_<>c__DisplayClass79_0::currentUser
	WindowsIdentity_t18859A4ADBD52470392E1AA64255F45A4970DD87 * ___currentUser_0;
	// System.Net.WebRequest System.Net.WebRequest_<>c__DisplayClass79_0::<>4__this
	WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_currentUser_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass79_0_t4CA87BE9AA29F9315C6AD70F8C79A62C7727013F, ___currentUser_0)); }
	inline WindowsIdentity_t18859A4ADBD52470392E1AA64255F45A4970DD87 * get_currentUser_0() const { return ___currentUser_0; }
	inline WindowsIdentity_t18859A4ADBD52470392E1AA64255F45A4970DD87 ** get_address_of_currentUser_0() { return &___currentUser_0; }
	inline void set_currentUser_0(WindowsIdentity_t18859A4ADBD52470392E1AA64255F45A4970DD87 * value)
	{
		___currentUser_0 = value;
		Il2CppCodeGenWriteBarrier((&___currentUser_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass79_0_t4CA87BE9AA29F9315C6AD70F8C79A62C7727013F, ___U3CU3E4__this_1)); }
	inline WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS79_0_T4CA87BE9AA29F9315C6AD70F8C79A62C7727013F_H
#ifndef DESIGNERWEBREQUESTCREATE_T613DD91D4F07703DC65E847B367F4DCD5710E2A3_H
#define DESIGNERWEBREQUESTCREATE_T613DD91D4F07703DC65E847B367F4DCD5710E2A3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebRequest_DesignerWebRequestCreate
struct  DesignerWebRequestCreate_t613DD91D4F07703DC65E847B367F4DCD5710E2A3  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESIGNERWEBREQUESTCREATE_T613DD91D4F07703DC65E847B367F4DCD5710E2A3_H
#ifndef WEBPROXYWRAPPEROPAQUE_T6CC216364481C2A8254832AA0897F770BB494A62_H
#define WEBPROXYWRAPPEROPAQUE_T6CC216364481C2A8254832AA0897F770BB494A62_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebRequest_WebProxyWrapperOpaque
struct  WebProxyWrapperOpaque_t6CC216364481C2A8254832AA0897F770BB494A62  : public RuntimeObject
{
public:
	// System.Net.WebProxy System.Net.WebRequest_WebProxyWrapperOpaque::webProxy
	WebProxy_t075305900B1D4D8BC8FAB08852842E42BC000417 * ___webProxy_0;

public:
	inline static int32_t get_offset_of_webProxy_0() { return static_cast<int32_t>(offsetof(WebProxyWrapperOpaque_t6CC216364481C2A8254832AA0897F770BB494A62, ___webProxy_0)); }
	inline WebProxy_t075305900B1D4D8BC8FAB08852842E42BC000417 * get_webProxy_0() const { return ___webProxy_0; }
	inline WebProxy_t075305900B1D4D8BC8FAB08852842E42BC000417 ** get_address_of_webProxy_0() { return &___webProxy_0; }
	inline void set_webProxy_0(WebProxy_t075305900B1D4D8BC8FAB08852842E42BC000417 * value)
	{
		___webProxy_0 = value;
		Il2CppCodeGenWriteBarrier((&___webProxy_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBPROXYWRAPPEROPAQUE_T6CC216364481C2A8254832AA0897F770BB494A62_H
#ifndef WEBREQUESTMETHODS_TEB20991FE33A8D697EB75C8B3179C887CD5CACCF_H
#define WEBREQUESTMETHODS_TEB20991FE33A8D697EB75C8B3179C887CD5CACCF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebRequestMethods
struct  WebRequestMethods_tEB20991FE33A8D697EB75C8B3179C887CD5CACCF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBREQUESTMETHODS_TEB20991FE33A8D697EB75C8B3179C887CD5CACCF_H
#ifndef FILE_TFDCA2B2C65C229BDD0AE1FF8AED12010D5849647_H
#define FILE_TFDCA2B2C65C229BDD0AE1FF8AED12010D5849647_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebRequestMethods_File
struct  File_tFDCA2B2C65C229BDD0AE1FF8AED12010D5849647  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILE_TFDCA2B2C65C229BDD0AE1FF8AED12010D5849647_H
#ifndef FTP_T48212CF2A906E6BCB6EB93FD8DB7A1B12A5EF5D1_H
#define FTP_T48212CF2A906E6BCB6EB93FD8DB7A1B12A5EF5D1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebRequestMethods_Ftp
struct  Ftp_t48212CF2A906E6BCB6EB93FD8DB7A1B12A5EF5D1  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FTP_T48212CF2A906E6BCB6EB93FD8DB7A1B12A5EF5D1_H
#ifndef HTTP_T2D48E64A5696B3936B4E68BC526E2BE4E0D16EA3_H
#define HTTP_T2D48E64A5696B3936B4E68BC526E2BE4E0D16EA3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebRequestMethods_Http
struct  Http_t2D48E64A5696B3936B4E68BC526E2BE4E0D16EA3  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTP_T2D48E64A5696B3936B4E68BC526E2BE4E0D16EA3_H
#ifndef WEBREQUESTPREFIXELEMENT_T78873458EC7C1DE7A0CDDD8498A7A7D5DAD27482_H
#define WEBREQUESTPREFIXELEMENT_T78873458EC7C1DE7A0CDDD8498A7A7D5DAD27482_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebRequestPrefixElement
struct  WebRequestPrefixElement_t78873458EC7C1DE7A0CDDD8498A7A7D5DAD27482  : public RuntimeObject
{
public:
	// System.String System.Net.WebRequestPrefixElement::Prefix
	String_t* ___Prefix_0;
	// System.Net.IWebRequestCreate System.Net.WebRequestPrefixElement::creator
	RuntimeObject* ___creator_1;
	// System.Type System.Net.WebRequestPrefixElement::creatorType
	Type_t * ___creatorType_2;

public:
	inline static int32_t get_offset_of_Prefix_0() { return static_cast<int32_t>(offsetof(WebRequestPrefixElement_t78873458EC7C1DE7A0CDDD8498A7A7D5DAD27482, ___Prefix_0)); }
	inline String_t* get_Prefix_0() const { return ___Prefix_0; }
	inline String_t** get_address_of_Prefix_0() { return &___Prefix_0; }
	inline void set_Prefix_0(String_t* value)
	{
		___Prefix_0 = value;
		Il2CppCodeGenWriteBarrier((&___Prefix_0), value);
	}

	inline static int32_t get_offset_of_creator_1() { return static_cast<int32_t>(offsetof(WebRequestPrefixElement_t78873458EC7C1DE7A0CDDD8498A7A7D5DAD27482, ___creator_1)); }
	inline RuntimeObject* get_creator_1() const { return ___creator_1; }
	inline RuntimeObject** get_address_of_creator_1() { return &___creator_1; }
	inline void set_creator_1(RuntimeObject* value)
	{
		___creator_1 = value;
		Il2CppCodeGenWriteBarrier((&___creator_1), value);
	}

	inline static int32_t get_offset_of_creatorType_2() { return static_cast<int32_t>(offsetof(WebRequestPrefixElement_t78873458EC7C1DE7A0CDDD8498A7A7D5DAD27482, ___creatorType_2)); }
	inline Type_t * get_creatorType_2() const { return ___creatorType_2; }
	inline Type_t ** get_address_of_creatorType_2() { return &___creatorType_2; }
	inline void set_creatorType_2(Type_t * value)
	{
		___creatorType_2 = value;
		Il2CppCodeGenWriteBarrier((&___creatorType_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBREQUESTPREFIXELEMENT_T78873458EC7C1DE7A0CDDD8498A7A7D5DAD27482_H
#ifndef HTMLENTITIES_T8BEA66BA49047ECD8960950145CA6259D95B2F79_H
#define HTMLENTITIES_T8BEA66BA49047ECD8960950145CA6259D95B2F79_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebUtility_HtmlEntities
struct  HtmlEntities_t8BEA66BA49047ECD8960950145CA6259D95B2F79  : public RuntimeObject
{
public:

public:
};

struct HtmlEntities_t8BEA66BA49047ECD8960950145CA6259D95B2F79_StaticFields
{
public:
	// System.Int64[] System.Net.WebUtility_HtmlEntities::entities
	Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* ___entities_0;
	// System.Char[] System.Net.WebUtility_HtmlEntities::entities_values
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___entities_values_1;

public:
	inline static int32_t get_offset_of_entities_0() { return static_cast<int32_t>(offsetof(HtmlEntities_t8BEA66BA49047ECD8960950145CA6259D95B2F79_StaticFields, ___entities_0)); }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* get_entities_0() const { return ___entities_0; }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F** get_address_of_entities_0() { return &___entities_0; }
	inline void set_entities_0(Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* value)
	{
		___entities_0 = value;
		Il2CppCodeGenWriteBarrier((&___entities_0), value);
	}

	inline static int32_t get_offset_of_entities_values_1() { return static_cast<int32_t>(offsetof(HtmlEntities_t8BEA66BA49047ECD8960950145CA6259D95B2F79_StaticFields, ___entities_values_1)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_entities_values_1() const { return ___entities_values_1; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_entities_values_1() { return &___entities_values_1; }
	inline void set_entities_values_1(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___entities_values_1 = value;
		Il2CppCodeGenWriteBarrier((&___entities_values_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTMLENTITIES_T8BEA66BA49047ECD8960950145CA6259D95B2F79_H
#ifndef URLDECODER_T9C62102EA32FB43498BED35B0526E09C666C83FC_H
#define URLDECODER_T9C62102EA32FB43498BED35B0526E09C666C83FC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebUtility_UrlDecoder
struct  UrlDecoder_t9C62102EA32FB43498BED35B0526E09C666C83FC  : public RuntimeObject
{
public:
	// System.Int32 System.Net.WebUtility_UrlDecoder::_bufferSize
	int32_t ____bufferSize_0;
	// System.Int32 System.Net.WebUtility_UrlDecoder::_numChars
	int32_t ____numChars_1;
	// System.Char[] System.Net.WebUtility_UrlDecoder::_charBuffer
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ____charBuffer_2;
	// System.Int32 System.Net.WebUtility_UrlDecoder::_numBytes
	int32_t ____numBytes_3;
	// System.Byte[] System.Net.WebUtility_UrlDecoder::_byteBuffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____byteBuffer_4;
	// System.Text.Encoding System.Net.WebUtility_UrlDecoder::_encoding
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ____encoding_5;

public:
	inline static int32_t get_offset_of__bufferSize_0() { return static_cast<int32_t>(offsetof(UrlDecoder_t9C62102EA32FB43498BED35B0526E09C666C83FC, ____bufferSize_0)); }
	inline int32_t get__bufferSize_0() const { return ____bufferSize_0; }
	inline int32_t* get_address_of__bufferSize_0() { return &____bufferSize_0; }
	inline void set__bufferSize_0(int32_t value)
	{
		____bufferSize_0 = value;
	}

	inline static int32_t get_offset_of__numChars_1() { return static_cast<int32_t>(offsetof(UrlDecoder_t9C62102EA32FB43498BED35B0526E09C666C83FC, ____numChars_1)); }
	inline int32_t get__numChars_1() const { return ____numChars_1; }
	inline int32_t* get_address_of__numChars_1() { return &____numChars_1; }
	inline void set__numChars_1(int32_t value)
	{
		____numChars_1 = value;
	}

	inline static int32_t get_offset_of__charBuffer_2() { return static_cast<int32_t>(offsetof(UrlDecoder_t9C62102EA32FB43498BED35B0526E09C666C83FC, ____charBuffer_2)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get__charBuffer_2() const { return ____charBuffer_2; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of__charBuffer_2() { return &____charBuffer_2; }
	inline void set__charBuffer_2(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		____charBuffer_2 = value;
		Il2CppCodeGenWriteBarrier((&____charBuffer_2), value);
	}

	inline static int32_t get_offset_of__numBytes_3() { return static_cast<int32_t>(offsetof(UrlDecoder_t9C62102EA32FB43498BED35B0526E09C666C83FC, ____numBytes_3)); }
	inline int32_t get__numBytes_3() const { return ____numBytes_3; }
	inline int32_t* get_address_of__numBytes_3() { return &____numBytes_3; }
	inline void set__numBytes_3(int32_t value)
	{
		____numBytes_3 = value;
	}

	inline static int32_t get_offset_of__byteBuffer_4() { return static_cast<int32_t>(offsetof(UrlDecoder_t9C62102EA32FB43498BED35B0526E09C666C83FC, ____byteBuffer_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__byteBuffer_4() const { return ____byteBuffer_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__byteBuffer_4() { return &____byteBuffer_4; }
	inline void set__byteBuffer_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____byteBuffer_4 = value;
		Il2CppCodeGenWriteBarrier((&____byteBuffer_4), value);
	}

	inline static int32_t get_offset_of__encoding_5() { return static_cast<int32_t>(offsetof(UrlDecoder_t9C62102EA32FB43498BED35B0526E09C666C83FC, ____encoding_5)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get__encoding_5() const { return ____encoding_5; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of__encoding_5() { return &____encoding_5; }
	inline void set__encoding_5(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		____encoding_5 = value;
		Il2CppCodeGenWriteBarrier((&____encoding_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URLDECODER_T9C62102EA32FB43498BED35B0526E09C666C83FC_H
#ifndef CODEACCESSPERMISSION_T1825F96A910182C8630635E511546E40DC0FF5FB_H
#define CODEACCESSPERMISSION_T1825F96A910182C8630635E511546E40DC0FF5FB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.CodeAccessPermission
struct  CodeAccessPermission_t1825F96A910182C8630635E511546E40DC0FF5FB  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CODEACCESSPERMISSION_T1825F96A910182C8630635E511546E40DC0FF5FB_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#define BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifndef NAMEVALUECOLLECTION_T7C7CED43E4C6E997E3C8012F1D2CC4027FAD10D1_H
#define NAMEVALUECOLLECTION_T7C7CED43E4C6E997E3C8012F1D2CC4027FAD10D1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Specialized.NameValueCollection
struct  NameValueCollection_t7C7CED43E4C6E997E3C8012F1D2CC4027FAD10D1  : public NameObjectCollectionBase_t593D97BF1A2AEA0C7FC1684B447BF92A5383883D
{
public:
	// System.String[] System.Collections.Specialized.NameValueCollection::_all
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ____all_10;
	// System.String[] System.Collections.Specialized.NameValueCollection::_allKeys
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ____allKeys_11;

public:
	inline static int32_t get_offset_of__all_10() { return static_cast<int32_t>(offsetof(NameValueCollection_t7C7CED43E4C6E997E3C8012F1D2CC4027FAD10D1, ____all_10)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get__all_10() const { return ____all_10; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of__all_10() { return &____all_10; }
	inline void set__all_10(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		____all_10 = value;
		Il2CppCodeGenWriteBarrier((&____all_10), value);
	}

	inline static int32_t get_offset_of__allKeys_11() { return static_cast<int32_t>(offsetof(NameValueCollection_t7C7CED43E4C6E997E3C8012F1D2CC4027FAD10D1, ____allKeys_11)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get__allKeys_11() const { return ____allKeys_11; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of__allKeys_11() { return &____allKeys_11; }
	inline void set__allKeys_11(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		____allKeys_11 = value;
		Il2CppCodeGenWriteBarrier((&____allKeys_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMEVALUECOLLECTION_T7C7CED43E4C6E997E3C8012F1D2CC4027FAD10D1_H
#ifndef DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#define DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 
{
public:
	// System.UInt64 System.DateTime::dateData
	uint64_t ___dateData_44;

public:
	inline static int32_t get_offset_of_dateData_44() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132, ___dateData_44)); }
	inline uint64_t get_dateData_44() const { return ___dateData_44; }
	inline uint64_t* get_address_of_dateData_44() { return &___dateData_44; }
	inline void set_dateData_44(uint64_t value)
	{
		___dateData_44 = value;
	}
};

struct DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields
{
public:
	// System.Int32[] System.DateTime::DaysToMonth365
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth365_29;
	// System.Int32[] System.DateTime::DaysToMonth366
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth366_30;
	// System.DateTime System.DateTime::MinValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MinValue_31;
	// System.DateTime System.DateTime::MaxValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MaxValue_32;

public:
	inline static int32_t get_offset_of_DaysToMonth365_29() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth365_29)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth365_29() const { return ___DaysToMonth365_29; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth365_29() { return &___DaysToMonth365_29; }
	inline void set_DaysToMonth365_29(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth365_29 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth365_29), value);
	}

	inline static int32_t get_offset_of_DaysToMonth366_30() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth366_30)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth366_30() const { return ___DaysToMonth366_30; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth366_30() { return &___DaysToMonth366_30; }
	inline void set_DaysToMonth366_30(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth366_30 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth366_30), value);
	}

	inline static int32_t get_offset_of_MinValue_31() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MinValue_31)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MinValue_31() const { return ___MinValue_31; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MinValue_31() { return &___MinValue_31; }
	inline void set_MinValue_31(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MinValue_31 = value;
	}

	inline static int32_t get_offset_of_MaxValue_32() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MaxValue_32)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MaxValue_32() const { return ___MaxValue_32; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MaxValue_32() { return &___MaxValue_32; }
	inline void set_MaxValue_32(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MaxValue_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#define STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Stream
struct  Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7  : public MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF
{
public:
	// System.IO.Stream_ReadWriteTask System.IO.Stream::_activeReadWriteTask
	ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * ____activeReadWriteTask_3;
	// System.Threading.SemaphoreSlim System.IO.Stream::_asyncActiveSemaphore
	SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * ____asyncActiveSemaphore_4;

public:
	inline static int32_t get_offset_of__activeReadWriteTask_3() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7, ____activeReadWriteTask_3)); }
	inline ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * get__activeReadWriteTask_3() const { return ____activeReadWriteTask_3; }
	inline ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 ** get_address_of__activeReadWriteTask_3() { return &____activeReadWriteTask_3; }
	inline void set__activeReadWriteTask_3(ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * value)
	{
		____activeReadWriteTask_3 = value;
		Il2CppCodeGenWriteBarrier((&____activeReadWriteTask_3), value);
	}

	inline static int32_t get_offset_of__asyncActiveSemaphore_4() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7, ____asyncActiveSemaphore_4)); }
	inline SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * get__asyncActiveSemaphore_4() const { return ____asyncActiveSemaphore_4; }
	inline SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 ** get_address_of__asyncActiveSemaphore_4() { return &____asyncActiveSemaphore_4; }
	inline void set__asyncActiveSemaphore_4(SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * value)
	{
		____asyncActiveSemaphore_4 = value;
		Il2CppCodeGenWriteBarrier((&____asyncActiveSemaphore_4), value);
	}
};

struct Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7_StaticFields
{
public:
	// System.IO.Stream System.IO.Stream::Null
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___Null_1;

public:
	inline static int32_t get_offset_of_Null_1() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7_StaticFields, ___Null_1)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_Null_1() const { return ___Null_1; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_Null_1() { return &___Null_1; }
	inline void set_Null_1(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___Null_1 = value;
		Il2CppCodeGenWriteBarrier((&___Null_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#ifndef INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#define INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef BLOB_T59A9790FE6F4E01F8B23D436D97BF1E33C06B4E9_H
#define BLOB_T59A9790FE6F4E01F8B23D436D97BF1E33C06B4E9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Blob
struct  Blob_t59A9790FE6F4E01F8B23D436D97BF1E33C06B4E9 
{
public:
	// System.Int32 System.Net.Blob::cbSize
	int32_t ___cbSize_0;
	// System.Int32 System.Net.Blob::pBlobData
	int32_t ___pBlobData_1;

public:
	inline static int32_t get_offset_of_cbSize_0() { return static_cast<int32_t>(offsetof(Blob_t59A9790FE6F4E01F8B23D436D97BF1E33C06B4E9, ___cbSize_0)); }
	inline int32_t get_cbSize_0() const { return ___cbSize_0; }
	inline int32_t* get_address_of_cbSize_0() { return &___cbSize_0; }
	inline void set_cbSize_0(int32_t value)
	{
		___cbSize_0 = value;
	}

	inline static int32_t get_offset_of_pBlobData_1() { return static_cast<int32_t>(offsetof(Blob_t59A9790FE6F4E01F8B23D436D97BF1E33C06B4E9, ___pBlobData_1)); }
	inline int32_t get_pBlobData_1() const { return ___pBlobData_1; }
	inline int32_t* get_address_of_pBlobData_1() { return &___pBlobData_1; }
	inline void set_pBlobData_1(int32_t value)
	{
		___pBlobData_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLOB_T59A9790FE6F4E01F8B23D436D97BF1E33C06B4E9_H
#ifndef CACHEDTRANSPORTCONTEXT_TEF48AE220F07023D4CF5E7A627D7C79CEFCF4A18_H
#define CACHEDTRANSPORTCONTEXT_TEF48AE220F07023D4CF5E7A627D7C79CEFCF4A18_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.CachedTransportContext
struct  CachedTransportContext_tEF48AE220F07023D4CF5E7A627D7C79CEFCF4A18  : public TransportContext_t1753CC7BFFA637B35BE353AAE4452C4371F89A9F
{
public:
	// System.Security.Authentication.ExtendedProtection.ChannelBinding System.Net.CachedTransportContext::binding
	ChannelBinding_t008B670203CDC7E4926CE95D32EB19E30AFE5E2C * ___binding_0;

public:
	inline static int32_t get_offset_of_binding_0() { return static_cast<int32_t>(offsetof(CachedTransportContext_tEF48AE220F07023D4CF5E7A627D7C79CEFCF4A18, ___binding_0)); }
	inline ChannelBinding_t008B670203CDC7E4926CE95D32EB19E30AFE5E2C * get_binding_0() const { return ___binding_0; }
	inline ChannelBinding_t008B670203CDC7E4926CE95D32EB19E30AFE5E2C ** get_address_of_binding_0() { return &___binding_0; }
	inline void set_binding_0(ChannelBinding_t008B670203CDC7E4926CE95D32EB19E30AFE5E2C * value)
	{
		___binding_0 = value;
		Il2CppCodeGenWriteBarrier((&___binding_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CACHEDTRANSPORTCONTEXT_TEF48AE220F07023D4CF5E7A627D7C79CEFCF4A18_H
#ifndef DIRECTPROXY_T04EFD8672502A0DC9EC819DA854421367D6B4B2D_H
#define DIRECTPROXY_T04EFD8672502A0DC9EC819DA854421367D6B4B2D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.DirectProxy
struct  DirectProxy_t04EFD8672502A0DC9EC819DA854421367D6B4B2D  : public ProxyChain_t58CD16408B22DC2B56772ADD1414604ABEF5AA53
{
public:
	// System.Boolean System.Net.DirectProxy::m_ProxyRetrieved
	bool ___m_ProxyRetrieved_5;

public:
	inline static int32_t get_offset_of_m_ProxyRetrieved_5() { return static_cast<int32_t>(offsetof(DirectProxy_t04EFD8672502A0DC9EC819DA854421367D6B4B2D, ___m_ProxyRetrieved_5)); }
	inline bool get_m_ProxyRetrieved_5() const { return ___m_ProxyRetrieved_5; }
	inline bool* get_address_of_m_ProxyRetrieved_5() { return &___m_ProxyRetrieved_5; }
	inline void set_m_ProxyRetrieved_5(bool value)
	{
		___m_ProxyRetrieved_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTPROXY_T04EFD8672502A0DC9EC819DA854421367D6B4B2D_H
#ifndef PROXYSCRIPTCHAIN_T3016818D00BD2D8517CBF113249542FE4CE2EF20_H
#define PROXYSCRIPTCHAIN_T3016818D00BD2D8517CBF113249542FE4CE2EF20_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.ProxyScriptChain
struct  ProxyScriptChain_t3016818D00BD2D8517CBF113249542FE4CE2EF20  : public ProxyChain_t58CD16408B22DC2B56772ADD1414604ABEF5AA53
{
public:
	// System.Net.WebProxy System.Net.ProxyScriptChain::m_Proxy
	WebProxy_t075305900B1D4D8BC8FAB08852842E42BC000417 * ___m_Proxy_5;
	// System.Uri[] System.Net.ProxyScriptChain::m_ScriptProxies
	UriU5BU5D_t2BA00D5A5C9D2A274C64EB3328ADE0D952CD0EB5* ___m_ScriptProxies_6;
	// System.Int32 System.Net.ProxyScriptChain::m_CurrentIndex
	int32_t ___m_CurrentIndex_7;
	// System.Int32 System.Net.ProxyScriptChain::m_SyncStatus
	int32_t ___m_SyncStatus_8;

public:
	inline static int32_t get_offset_of_m_Proxy_5() { return static_cast<int32_t>(offsetof(ProxyScriptChain_t3016818D00BD2D8517CBF113249542FE4CE2EF20, ___m_Proxy_5)); }
	inline WebProxy_t075305900B1D4D8BC8FAB08852842E42BC000417 * get_m_Proxy_5() const { return ___m_Proxy_5; }
	inline WebProxy_t075305900B1D4D8BC8FAB08852842E42BC000417 ** get_address_of_m_Proxy_5() { return &___m_Proxy_5; }
	inline void set_m_Proxy_5(WebProxy_t075305900B1D4D8BC8FAB08852842E42BC000417 * value)
	{
		___m_Proxy_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Proxy_5), value);
	}

	inline static int32_t get_offset_of_m_ScriptProxies_6() { return static_cast<int32_t>(offsetof(ProxyScriptChain_t3016818D00BD2D8517CBF113249542FE4CE2EF20, ___m_ScriptProxies_6)); }
	inline UriU5BU5D_t2BA00D5A5C9D2A274C64EB3328ADE0D952CD0EB5* get_m_ScriptProxies_6() const { return ___m_ScriptProxies_6; }
	inline UriU5BU5D_t2BA00D5A5C9D2A274C64EB3328ADE0D952CD0EB5** get_address_of_m_ScriptProxies_6() { return &___m_ScriptProxies_6; }
	inline void set_m_ScriptProxies_6(UriU5BU5D_t2BA00D5A5C9D2A274C64EB3328ADE0D952CD0EB5* value)
	{
		___m_ScriptProxies_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_ScriptProxies_6), value);
	}

	inline static int32_t get_offset_of_m_CurrentIndex_7() { return static_cast<int32_t>(offsetof(ProxyScriptChain_t3016818D00BD2D8517CBF113249542FE4CE2EF20, ___m_CurrentIndex_7)); }
	inline int32_t get_m_CurrentIndex_7() const { return ___m_CurrentIndex_7; }
	inline int32_t* get_address_of_m_CurrentIndex_7() { return &___m_CurrentIndex_7; }
	inline void set_m_CurrentIndex_7(int32_t value)
	{
		___m_CurrentIndex_7 = value;
	}

	inline static int32_t get_offset_of_m_SyncStatus_8() { return static_cast<int32_t>(offsetof(ProxyScriptChain_t3016818D00BD2D8517CBF113249542FE4CE2EF20, ___m_SyncStatus_8)); }
	inline int32_t get_m_SyncStatus_8() const { return ___m_SyncStatus_8; }
	inline int32_t* get_address_of_m_SyncStatus_8() { return &___m_SyncStatus_8; }
	inline void set_m_SyncStatus_8(int32_t value)
	{
		___m_SyncStatus_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROXYSCRIPTCHAIN_T3016818D00BD2D8517CBF113249542FE4CE2EF20_H
#ifndef STATICPROXY_T5FDF2D06DD4DC75D5EEE6E7BC060A9A30FAF3711_H
#define STATICPROXY_T5FDF2D06DD4DC75D5EEE6E7BC060A9A30FAF3711_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.StaticProxy
struct  StaticProxy_t5FDF2D06DD4DC75D5EEE6E7BC060A9A30FAF3711  : public ProxyChain_t58CD16408B22DC2B56772ADD1414604ABEF5AA53
{
public:
	// System.Uri System.Net.StaticProxy::m_Proxy
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___m_Proxy_5;

public:
	inline static int32_t get_offset_of_m_Proxy_5() { return static_cast<int32_t>(offsetof(StaticProxy_t5FDF2D06DD4DC75D5EEE6E7BC060A9A30FAF3711, ___m_Proxy_5)); }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * get_m_Proxy_5() const { return ___m_Proxy_5; }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E ** get_address_of_m_Proxy_5() { return &___m_Proxy_5; }
	inline void set_m_Proxy_5(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * value)
	{
		___m_Proxy_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Proxy_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATICPROXY_T5FDF2D06DD4DC75D5EEE6E7BC060A9A30FAF3711_H
#ifndef INFINITETIMER_T2DCA24E378A3B09EF224C9DCDC84EAA4B3CE1CC8_H
#define INFINITETIMER_T2DCA24E378A3B09EF224C9DCDC84EAA4B3CE1CC8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.TimerThread_InfiniteTimer
struct  InfiniteTimer_t2DCA24E378A3B09EF224C9DCDC84EAA4B3CE1CC8  : public Timer_t3B21B1013E27B5DC9FED14EC0921A5F51230D46F
{
public:
	// System.Int32 System.Net.TimerThread_InfiniteTimer::cancelled
	int32_t ___cancelled_2;

public:
	inline static int32_t get_offset_of_cancelled_2() { return static_cast<int32_t>(offsetof(InfiniteTimer_t2DCA24E378A3B09EF224C9DCDC84EAA4B3CE1CC8, ___cancelled_2)); }
	inline int32_t get_cancelled_2() const { return ___cancelled_2; }
	inline int32_t* get_address_of_cancelled_2() { return &___cancelled_2; }
	inline void set_cancelled_2(int32_t value)
	{
		___cancelled_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INFINITETIMER_T2DCA24E378A3B09EF224C9DCDC84EAA4B3CE1CC8_H
#ifndef INFINITETIMERQUEUE_T141BA98635EDB34E2BAAFE8BA5C91795E7CCAB51_H
#define INFINITETIMERQUEUE_T141BA98635EDB34E2BAAFE8BA5C91795E7CCAB51_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.TimerThread_InfiniteTimerQueue
struct  InfiniteTimerQueue_t141BA98635EDB34E2BAAFE8BA5C91795E7CCAB51  : public Queue_tCCFF6A2FCF584216AEDA04A483FB808E2D493643
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INFINITETIMERQUEUE_T141BA98635EDB34E2BAAFE8BA5C91795E7CCAB51_H
#ifndef WEBPERMISSION_TFC726018222D4C825C7A1CD2B66E7E3C0326A23C_H
#define WEBPERMISSION_TFC726018222D4C825C7A1CD2B66E7E3C0326A23C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebPermission
struct  WebPermission_tFC726018222D4C825C7A1CD2B66E7E3C0326A23C  : public CodeAccessPermission_t1825F96A910182C8630635E511546E40DC0FF5FB
{
public:
	// System.Boolean System.Net.WebPermission::m_noRestriction
	bool ___m_noRestriction_0;
	// System.Boolean System.Net.WebPermission::m_UnrestrictedConnect
	bool ___m_UnrestrictedConnect_1;
	// System.Boolean System.Net.WebPermission::m_UnrestrictedAccept
	bool ___m_UnrestrictedAccept_2;
	// System.Collections.ArrayList System.Net.WebPermission::m_connectList
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ___m_connectList_3;
	// System.Collections.ArrayList System.Net.WebPermission::m_acceptList
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ___m_acceptList_4;

public:
	inline static int32_t get_offset_of_m_noRestriction_0() { return static_cast<int32_t>(offsetof(WebPermission_tFC726018222D4C825C7A1CD2B66E7E3C0326A23C, ___m_noRestriction_0)); }
	inline bool get_m_noRestriction_0() const { return ___m_noRestriction_0; }
	inline bool* get_address_of_m_noRestriction_0() { return &___m_noRestriction_0; }
	inline void set_m_noRestriction_0(bool value)
	{
		___m_noRestriction_0 = value;
	}

	inline static int32_t get_offset_of_m_UnrestrictedConnect_1() { return static_cast<int32_t>(offsetof(WebPermission_tFC726018222D4C825C7A1CD2B66E7E3C0326A23C, ___m_UnrestrictedConnect_1)); }
	inline bool get_m_UnrestrictedConnect_1() const { return ___m_UnrestrictedConnect_1; }
	inline bool* get_address_of_m_UnrestrictedConnect_1() { return &___m_UnrestrictedConnect_1; }
	inline void set_m_UnrestrictedConnect_1(bool value)
	{
		___m_UnrestrictedConnect_1 = value;
	}

	inline static int32_t get_offset_of_m_UnrestrictedAccept_2() { return static_cast<int32_t>(offsetof(WebPermission_tFC726018222D4C825C7A1CD2B66E7E3C0326A23C, ___m_UnrestrictedAccept_2)); }
	inline bool get_m_UnrestrictedAccept_2() const { return ___m_UnrestrictedAccept_2; }
	inline bool* get_address_of_m_UnrestrictedAccept_2() { return &___m_UnrestrictedAccept_2; }
	inline void set_m_UnrestrictedAccept_2(bool value)
	{
		___m_UnrestrictedAccept_2 = value;
	}

	inline static int32_t get_offset_of_m_connectList_3() { return static_cast<int32_t>(offsetof(WebPermission_tFC726018222D4C825C7A1CD2B66E7E3C0326A23C, ___m_connectList_3)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get_m_connectList_3() const { return ___m_connectList_3; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of_m_connectList_3() { return &___m_connectList_3; }
	inline void set_m_connectList_3(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		___m_connectList_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_connectList_3), value);
	}

	inline static int32_t get_offset_of_m_acceptList_4() { return static_cast<int32_t>(offsetof(WebPermission_tFC726018222D4C825C7A1CD2B66E7E3C0326A23C, ___m_acceptList_4)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get_m_acceptList_4() const { return ___m_acceptList_4; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of_m_acceptList_4() { return &___m_acceptList_4; }
	inline void set_m_acceptList_4(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		___m_acceptList_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_acceptList_4), value);
	}
};

struct WebPermission_tFC726018222D4C825C7A1CD2B66E7E3C0326A23C_StaticFields
{
public:
	// System.Text.RegularExpressions.Regex modreq(System.Runtime.CompilerServices.IsVolatile) System.Net.WebPermission::s_MatchAllRegex
	Regex_tFD46E63A462E852189FD6AB4E2B0B67C4D8FDBDF * ___s_MatchAllRegex_6;

public:
	inline static int32_t get_offset_of_s_MatchAllRegex_6() { return static_cast<int32_t>(offsetof(WebPermission_tFC726018222D4C825C7A1CD2B66E7E3C0326A23C_StaticFields, ___s_MatchAllRegex_6)); }
	inline Regex_tFD46E63A462E852189FD6AB4E2B0B67C4D8FDBDF * get_s_MatchAllRegex_6() const { return ___s_MatchAllRegex_6; }
	inline Regex_tFD46E63A462E852189FD6AB4E2B0B67C4D8FDBDF ** get_address_of_s_MatchAllRegex_6() { return &___s_MatchAllRegex_6; }
	inline void set_s_MatchAllRegex_6(Regex_tFD46E63A462E852189FD6AB4E2B0B67C4D8FDBDF * value)
	{
		___s_MatchAllRegex_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_MatchAllRegex_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBPERMISSION_TFC726018222D4C825C7A1CD2B66E7E3C0326A23C_H
#ifndef WEBPROXYWRAPPER_T47B30DCD77853C5079F4944A6FCA329026D84E3B_H
#define WEBPROXYWRAPPER_T47B30DCD77853C5079F4944A6FCA329026D84E3B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebRequest_WebProxyWrapper
struct  WebProxyWrapper_t47B30DCD77853C5079F4944A6FCA329026D84E3B  : public WebProxyWrapperOpaque_t6CC216364481C2A8254832AA0897F770BB494A62
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBPROXYWRAPPER_T47B30DCD77853C5079F4944A6FCA329026D84E3B_H
#ifndef WEBRESPONSE_T5C91B5B83E2FBA2EABC6FDF2A70E9AFD9BB059BD_H
#define WEBRESPONSE_T5C91B5B83E2FBA2EABC6FDF2A70E9AFD9BB059BD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebResponse
struct  WebResponse_t5C91B5B83E2FBA2EABC6FDF2A70E9AFD9BB059BD  : public MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF
{
public:
	// System.Boolean System.Net.WebResponse::m_IsCacheFresh
	bool ___m_IsCacheFresh_1;
	// System.Boolean System.Net.WebResponse::m_IsFromCache
	bool ___m_IsFromCache_2;

public:
	inline static int32_t get_offset_of_m_IsCacheFresh_1() { return static_cast<int32_t>(offsetof(WebResponse_t5C91B5B83E2FBA2EABC6FDF2A70E9AFD9BB059BD, ___m_IsCacheFresh_1)); }
	inline bool get_m_IsCacheFresh_1() const { return ___m_IsCacheFresh_1; }
	inline bool* get_address_of_m_IsCacheFresh_1() { return &___m_IsCacheFresh_1; }
	inline void set_m_IsCacheFresh_1(bool value)
	{
		___m_IsCacheFresh_1 = value;
	}

	inline static int32_t get_offset_of_m_IsFromCache_2() { return static_cast<int32_t>(offsetof(WebResponse_t5C91B5B83E2FBA2EABC6FDF2A70E9AFD9BB059BD, ___m_IsFromCache_2)); }
	inline bool get_m_IsFromCache_2() const { return ___m_IsFromCache_2; }
	inline bool* get_address_of_m_IsFromCache_2() { return &___m_IsFromCache_2; }
	inline void set_m_IsFromCache_2(bool value)
	{
		___m_IsFromCache_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBRESPONSE_T5C91B5B83E2FBA2EABC6FDF2A70E9AFD9BB059BD_H
#ifndef WRITESTREAMCLOSEDEVENTARGS_T1E55EEFF430B18CCEF826ED9078E8D7BB6783869_H
#define WRITESTREAMCLOSEDEVENTARGS_T1E55EEFF430B18CCEF826ED9078E8D7BB6783869_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WriteStreamClosedEventArgs
struct  WriteStreamClosedEventArgs_t1E55EEFF430B18CCEF826ED9078E8D7BB6783869  : public EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WRITESTREAMCLOSEDEVENTARGS_T1E55EEFF430B18CCEF826ED9078E8D7BB6783869_H
#ifndef SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#define SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef INVALIDOPERATIONEXCEPTION_T0530E734D823F78310CAFAFA424CA5164D93A1F1_H
#define INVALIDOPERATIONEXCEPTION_T0530E734D823F78310CAFAFA424CA5164D93A1F1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.InvalidOperationException
struct  InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1  : public SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVALIDOPERATIONEXCEPTION_T0530E734D823F78310CAFAFA424CA5164D93A1F1_H
#ifndef BUFFERTYPE_T5742CC1ED9479EEFA365C61C7B35C31113F928A3_H
#define BUFFERTYPE_T5742CC1ED9479EEFA365C61C7B35C31113F928A3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.BufferType
struct  BufferType_t5742CC1ED9479EEFA365C61C7B35C31113F928A3 
{
public:
	// System.Int32 System.Net.BufferType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BufferType_t5742CC1ED9479EEFA365C61C7B35C31113F928A3, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUFFERTYPE_T5742CC1ED9479EEFA365C61C7B35C31113F928A3_H
#ifndef CERTUSAGE_TD6645AB4AE2BF82C76670A980E82C4DC1527CA95_H
#define CERTUSAGE_TD6645AB4AE2BF82C76670A980E82C4DC1527CA95_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.CertUsage
struct  CertUsage_tD6645AB4AE2BF82C76670A980E82C4DC1527CA95 
{
public:
	// System.Int32 System.Net.CertUsage::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CertUsage_tD6645AB4AE2BF82C76670A980E82C4DC1527CA95, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CERTUSAGE_TD6645AB4AE2BF82C76670A980E82C4DC1527CA95_H
#ifndef CERTIFICATEENCODING_TA8B341F3892CEDDCD4BD2D2C65177AD96668E3E6_H
#define CERTIFICATEENCODING_TA8B341F3892CEDDCD4BD2D2C65177AD96668E3E6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.CertificateEncoding
struct  CertificateEncoding_tA8B341F3892CEDDCD4BD2D2C65177AD96668E3E6 
{
public:
	// System.Int32 System.Net.CertificateEncoding::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CertificateEncoding_tA8B341F3892CEDDCD4BD2D2C65177AD96668E3E6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CERTIFICATEENCODING_TA8B341F3892CEDDCD4BD2D2C65177AD96668E3E6_H
#ifndef CERTIFICATEPROBLEM_T406FB8728C32165998C1BA55463557F33011D8E4_H
#define CERTIFICATEPROBLEM_T406FB8728C32165998C1BA55463557F33011D8E4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.CertificateProblem
struct  CertificateProblem_t406FB8728C32165998C1BA55463557F33011D8E4 
{
public:
	// System.Int32 System.Net.CertificateProblem::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CertificateProblem_t406FB8728C32165998C1BA55463557F33011D8E4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CERTIFICATEPROBLEM_T406FB8728C32165998C1BA55463557F33011D8E4_H
#ifndef CLOSEEXSTATE_T7AD30E3EACEBBAF7661B1AC45F7BC018DA33E429_H
#define CLOSEEXSTATE_T7AD30E3EACEBBAF7661B1AC45F7BC018DA33E429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.CloseExState
struct  CloseExState_t7AD30E3EACEBBAF7661B1AC45F7BC018DA33E429 
{
public:
	// System.Int32 System.Net.CloseExState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CloseExState_t7AD30E3EACEBBAF7661B1AC45F7BC018DA33E429, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLOSEEXSTATE_T7AD30E3EACEBBAF7661B1AC45F7BC018DA33E429_H
#ifndef UNICODEDECODINGCONFORMANCE_T9467A928E4A46098930CA0A67315E0159230771E_H
#define UNICODEDECODINGCONFORMANCE_T9467A928E4A46098930CA0A67315E0159230771E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Configuration.UnicodeDecodingConformance
struct  UnicodeDecodingConformance_t9467A928E4A46098930CA0A67315E0159230771E 
{
public:
	// System.Int32 System.Net.Configuration.UnicodeDecodingConformance::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UnicodeDecodingConformance_t9467A928E4A46098930CA0A67315E0159230771E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNICODEDECODINGCONFORMANCE_T9467A928E4A46098930CA0A67315E0159230771E_H
#ifndef UNICODEENCODINGCONFORMANCE_TB184A12AA9972C115D899779A92DCB82719B487A_H
#define UNICODEENCODINGCONFORMANCE_TB184A12AA9972C115D899779A92DCB82719B487A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Configuration.UnicodeEncodingConformance
struct  UnicodeEncodingConformance_tB184A12AA9972C115D899779A92DCB82719B487A 
{
public:
	// System.Int32 System.Net.Configuration.UnicodeEncodingConformance::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UnicodeEncodingConformance_tB184A12AA9972C115D899779A92DCB82719B487A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNICODEENCODINGCONFORMANCE_TB184A12AA9972C115D899779A92DCB82719B487A_H
#ifndef DATAPARSESTATUS_TE7B5667E06BC732DC9F73853C4B7B001D74C63FC_H
#define DATAPARSESTATUS_TE7B5667E06BC732DC9F73853C4B7B001D74C63FC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.DataParseStatus
struct  DataParseStatus_tE7B5667E06BC732DC9F73853C4B7B001D74C63FC 
{
public:
	// System.Int32 System.Net.DataParseStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DataParseStatus_tE7B5667E06BC732DC9F73853C4B7B001D74C63FC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATAPARSESTATUS_TE7B5667E06BC732DC9F73853C4B7B001D74C63FC_H
#ifndef DEFAULTPORTS_TEADFE734E2CEFF4E0F75A9B4A73309281C47C0E6_H
#define DEFAULTPORTS_TEADFE734E2CEFF4E0F75A9B4A73309281C47C0E6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.DefaultPorts
struct  DefaultPorts_tEADFE734E2CEFF4E0F75A9B4A73309281C47C0E6 
{
public:
	// System.Int32 System.Net.DefaultPorts::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DefaultPorts_tEADFE734E2CEFF4E0F75A9B4A73309281C47C0E6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTPORTS_TEADFE734E2CEFF4E0F75A9B4A73309281C47C0E6_H
#ifndef HTTPBEHAVIOUR_T74C1678BDAC7E0224BF99517C97D5BB41C124C0A_H
#define HTTPBEHAVIOUR_T74C1678BDAC7E0224BF99517C97D5BB41C124C0A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HttpBehaviour
struct  HttpBehaviour_t74C1678BDAC7E0224BF99517C97D5BB41C124C0A 
{
public:
	// System.Byte System.Net.HttpBehaviour::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HttpBehaviour_t74C1678BDAC7E0224BF99517C97D5BB41C124C0A, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPBEHAVIOUR_T74C1678BDAC7E0224BF99517C97D5BB41C124C0A_H
#ifndef HTTPPROCESSINGRESULT_T8E9C0D1BFD5EF70718EBCA4D35CE11A812FD5572_H
#define HTTPPROCESSINGRESULT_T8E9C0D1BFD5EF70718EBCA4D35CE11A812FD5572_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HttpProcessingResult
struct  HttpProcessingResult_t8E9C0D1BFD5EF70718EBCA4D35CE11A812FD5572 
{
public:
	// System.Int32 System.Net.HttpProcessingResult::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HttpProcessingResult_t8E9C0D1BFD5EF70718EBCA4D35CE11A812FD5572, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPPROCESSINGRESULT_T8E9C0D1BFD5EF70718EBCA4D35CE11A812FD5572_H
#ifndef HTTPWRITEMODE_T4428EB73952EA86B84C7BE5034ED93F7CF49F997_H
#define HTTPWRITEMODE_T4428EB73952EA86B84C7BE5034ED93F7CF49F997_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HttpWriteMode
struct  HttpWriteMode_t4428EB73952EA86B84C7BE5034ED93F7CF49F997 
{
public:
	// System.Int32 System.Net.HttpWriteMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HttpWriteMode_t4428EB73952EA86B84C7BE5034ED93F7CF49F997, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPWRITEMODE_T4428EB73952EA86B84C7BE5034ED93F7CF49F997_H
#ifndef NETWORKACCESS_TDF7B3E99D1313AA25FE2613FA9CA07818AD8A394_H
#define NETWORKACCESS_TDF7B3E99D1313AA25FE2613FA9CA07818AD8A394_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.NetworkAccess
struct  NetworkAccess_tDF7B3E99D1313AA25FE2613FA9CA07818AD8A394 
{
public:
	// System.Int32 System.Net.NetworkAccess::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NetworkAccess_tDF7B3E99D1313AA25FE2613FA9CA07818AD8A394, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKACCESS_TDF7B3E99D1313AA25FE2613FA9CA07818AD8A394_H
#ifndef AUTHENTICATIONLEVEL_TC0FE8B3A1A9C4F39798DD6F6C024078BB137F52B_H
#define AUTHENTICATIONLEVEL_TC0FE8B3A1A9C4F39798DD6F6C024078BB137F52B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Security.AuthenticationLevel
struct  AuthenticationLevel_tC0FE8B3A1A9C4F39798DD6F6C024078BB137F52B 
{
public:
	// System.Int32 System.Net.Security.AuthenticationLevel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AuthenticationLevel_tC0FE8B3A1A9C4F39798DD6F6C024078BB137F52B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTHENTICATIONLEVEL_TC0FE8B3A1A9C4F39798DD6F6C024078BB137F52B_H
#ifndef THREADKINDS_T17B8CB7BF8789F46FD497AC5EDE85173280ADC20_H
#define THREADKINDS_T17B8CB7BF8789F46FD497AC5EDE85173280ADC20_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.ThreadKinds
struct  ThreadKinds_t17B8CB7BF8789F46FD497AC5EDE85173280ADC20 
{
public:
	// System.Int32 System.Net.ThreadKinds::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ThreadKinds_t17B8CB7BF8789F46FD497AC5EDE85173280ADC20, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THREADKINDS_T17B8CB7BF8789F46FD497AC5EDE85173280ADC20_H
#ifndef TIMERSTATE_TD555FD971FFAFF7BE6F94885EC160D206354BD35_H
#define TIMERSTATE_TD555FD971FFAFF7BE6F94885EC160D206354BD35_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.TimerThread_TimerNode_TimerState
struct  TimerState_tD555FD971FFAFF7BE6F94885EC160D206354BD35 
{
public:
	// System.Int32 System.Net.TimerThread_TimerNode_TimerState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TimerState_tD555FD971FFAFF7BE6F94885EC160D206354BD35, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMERSTATE_TD555FD971FFAFF7BE6F94885EC160D206354BD35_H
#ifndef TIMERQUEUE_T8C40E5540B8DCC1AF23C12BC62F6D1D8061F754C_H
#define TIMERQUEUE_T8C40E5540B8DCC1AF23C12BC62F6D1D8061F754C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.TimerThread_TimerQueue
struct  TimerQueue_t8C40E5540B8DCC1AF23C12BC62F6D1D8061F754C  : public Queue_tCCFF6A2FCF584216AEDA04A483FB808E2D493643
{
public:
	// System.IntPtr System.Net.TimerThread_TimerQueue::m_ThisHandle
	intptr_t ___m_ThisHandle_1;
	// System.Net.TimerThread_TimerNode System.Net.TimerThread_TimerQueue::m_Timers
	TimerNode_t5596C0BAF3E85D0C2444ECA74917630FDEF5087B * ___m_Timers_2;

public:
	inline static int32_t get_offset_of_m_ThisHandle_1() { return static_cast<int32_t>(offsetof(TimerQueue_t8C40E5540B8DCC1AF23C12BC62F6D1D8061F754C, ___m_ThisHandle_1)); }
	inline intptr_t get_m_ThisHandle_1() const { return ___m_ThisHandle_1; }
	inline intptr_t* get_address_of_m_ThisHandle_1() { return &___m_ThisHandle_1; }
	inline void set_m_ThisHandle_1(intptr_t value)
	{
		___m_ThisHandle_1 = value;
	}

	inline static int32_t get_offset_of_m_Timers_2() { return static_cast<int32_t>(offsetof(TimerQueue_t8C40E5540B8DCC1AF23C12BC62F6D1D8061F754C, ___m_Timers_2)); }
	inline TimerNode_t5596C0BAF3E85D0C2444ECA74917630FDEF5087B * get_m_Timers_2() const { return ___m_Timers_2; }
	inline TimerNode_t5596C0BAF3E85D0C2444ECA74917630FDEF5087B ** get_address_of_m_Timers_2() { return &___m_Timers_2; }
	inline void set_m_Timers_2(TimerNode_t5596C0BAF3E85D0C2444ECA74917630FDEF5087B * value)
	{
		___m_Timers_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Timers_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMERQUEUE_T8C40E5540B8DCC1AF23C12BC62F6D1D8061F754C_H
#ifndef TIMERTHREADSTATE_T47E2D1C00D1FBAACE6D01C4A9449E06D50A374F8_H
#define TIMERTHREADSTATE_T47E2D1C00D1FBAACE6D01C4A9449E06D50A374F8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.TimerThread_TimerThreadState
struct  TimerThreadState_t47E2D1C00D1FBAACE6D01C4A9449E06D50A374F8 
{
public:
	// System.Int32 System.Net.TimerThread_TimerThreadState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TimerThreadState_t47E2D1C00D1FBAACE6D01C4A9449E06D50A374F8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMERTHREADSTATE_T47E2D1C00D1FBAACE6D01C4A9449E06D50A374F8_H
#ifndef TRANSPORTTYPE_TE05CF39764C8131A3248837320DF17C2129E0490_H
#define TRANSPORTTYPE_TE05CF39764C8131A3248837320DF17C2129E0490_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.TransportType
struct  TransportType_tE05CF39764C8131A3248837320DF17C2129E0490 
{
public:
	// System.Int32 System.Net.TransportType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TransportType_tE05CF39764C8131A3248837320DF17C2129E0490, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSPORTTYPE_TE05CF39764C8131A3248837320DF17C2129E0490_H
#ifndef TRISTATE_T1CD7C17BF90AEFF980E903EE05FFB7A3CA4A16FE_H
#define TRISTATE_T1CD7C17BF90AEFF980E903EE05FFB7A3CA4A16FE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.TriState
struct  TriState_t1CD7C17BF90AEFF980E903EE05FFB7A3CA4A16FE 
{
public:
	// System.Int32 System.Net.TriState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TriState_t1CD7C17BF90AEFF980E903EE05FFB7A3CA4A16FE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRISTATE_T1CD7C17BF90AEFF980E903EE05FFB7A3CA4A16FE_H
#ifndef WEBEXCEPTIONINTERNALSTATUS_T2B50725020F5BAB7DCBE324ADF308881FEB3B64D_H
#define WEBEXCEPTIONINTERNALSTATUS_T2B50725020F5BAB7DCBE324ADF308881FEB3B64D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebExceptionInternalStatus
struct  WebExceptionInternalStatus_t2B50725020F5BAB7DCBE324ADF308881FEB3B64D 
{
public:
	// System.Int32 System.Net.WebExceptionInternalStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WebExceptionInternalStatus_t2B50725020F5BAB7DCBE324ADF308881FEB3B64D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBEXCEPTIONINTERNALSTATUS_T2B50725020F5BAB7DCBE324ADF308881FEB3B64D_H
#ifndef WEBEXCEPTIONSTATUS_T97365CBADE462C1E2A1A0FACF18F3B111900F8DC_H
#define WEBEXCEPTIONSTATUS_T97365CBADE462C1E2A1A0FACF18F3B111900F8DC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebExceptionStatus
struct  WebExceptionStatus_t97365CBADE462C1E2A1A0FACF18F3B111900F8DC 
{
public:
	// System.Int32 System.Net.WebExceptionStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WebExceptionStatus_t97365CBADE462C1E2A1A0FACF18F3B111900F8DC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBEXCEPTIONSTATUS_T97365CBADE462C1E2A1A0FACF18F3B111900F8DC_H
#ifndef RFCCHAR_TD4173F085F19DF711D550AC6CAD7EF61939EF27F_H
#define RFCCHAR_TD4173F085F19DF711D550AC6CAD7EF61939EF27F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebHeaderCollection_RfcChar
struct  RfcChar_tD4173F085F19DF711D550AC6CAD7EF61939EF27F 
{
public:
	// System.Byte System.Net.WebHeaderCollection_RfcChar::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RfcChar_tD4173F085F19DF711D550AC6CAD7EF61939EF27F, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RFCCHAR_TD4173F085F19DF711D550AC6CAD7EF61939EF27F_H
#ifndef WEBHEADERCOLLECTIONTYPE_T2994510EB856AC407AB0757A9814CDF80185A862_H
#define WEBHEADERCOLLECTIONTYPE_T2994510EB856AC407AB0757A9814CDF80185A862_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebHeaderCollectionType
struct  WebHeaderCollectionType_t2994510EB856AC407AB0757A9814CDF80185A862 
{
public:
	// System.UInt16 System.Net.WebHeaderCollectionType::value__
	uint16_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WebHeaderCollectionType_t2994510EB856AC407AB0757A9814CDF80185A862, ___value___2)); }
	inline uint16_t get_value___2() const { return ___value___2; }
	inline uint16_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint16_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBHEADERCOLLECTIONTYPE_T2994510EB856AC407AB0757A9814CDF80185A862_H
#ifndef WEBPARSEERRORCODE_T82D6E594C83923E1C0FB47D4B0EE90EDA0AA22CC_H
#define WEBPARSEERRORCODE_T82D6E594C83923E1C0FB47D4B0EE90EDA0AA22CC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebParseErrorCode
struct  WebParseErrorCode_t82D6E594C83923E1C0FB47D4B0EE90EDA0AA22CC 
{
public:
	// System.Int32 System.Net.WebParseErrorCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WebParseErrorCode_t82D6E594C83923E1C0FB47D4B0EE90EDA0AA22CC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBPARSEERRORCODE_T82D6E594C83923E1C0FB47D4B0EE90EDA0AA22CC_H
#ifndef WEBPARSEERRORSECTION_T97E38DEF05BB39EFF8D4703ECB7DD74C30072219_H
#define WEBPARSEERRORSECTION_T97E38DEF05BB39EFF8D4703ECB7DD74C30072219_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebParseErrorSection
struct  WebParseErrorSection_t97E38DEF05BB39EFF8D4703ECB7DD74C30072219 
{
public:
	// System.Int32 System.Net.WebParseErrorSection::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WebParseErrorSection_t97E38DEF05BB39EFF8D4703ECB7DD74C30072219, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBPARSEERRORSECTION_T97E38DEF05BB39EFF8D4703ECB7DD74C30072219_H
#ifndef WRITEBUFFERSTATE_T6870E49BDF9175CC064514AC1F0C9CD7784E22CC_H
#define WRITEBUFFERSTATE_T6870E49BDF9175CC064514AC1F0C9CD7784E22CC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WriteBufferState
struct  WriteBufferState_t6870E49BDF9175CC064514AC1F0C9CD7784E22CC 
{
public:
	// System.Int32 System.Net.WriteBufferState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WriteBufferState_t6870E49BDF9175CC064514AC1F0C9CD7784E22CC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WRITEBUFFERSTATE_T6870E49BDF9175CC064514AC1F0C9CD7784E22CC_H
#ifndef HOSTENT_T618E4548593621ED80D901F36C0153673B95959E_H
#define HOSTENT_T618E4548593621ED80D901F36C0153673B95959E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.hostent
struct  hostent_t618E4548593621ED80D901F36C0153673B95959E 
{
public:
	// System.IntPtr System.Net.hostent::h_name
	intptr_t ___h_name_0;
	// System.IntPtr System.Net.hostent::h_aliases
	intptr_t ___h_aliases_1;
	// System.Int16 System.Net.hostent::h_addrtype
	int16_t ___h_addrtype_2;
	// System.Int16 System.Net.hostent::h_length
	int16_t ___h_length_3;
	// System.IntPtr System.Net.hostent::h_addr_list
	intptr_t ___h_addr_list_4;

public:
	inline static int32_t get_offset_of_h_name_0() { return static_cast<int32_t>(offsetof(hostent_t618E4548593621ED80D901F36C0153673B95959E, ___h_name_0)); }
	inline intptr_t get_h_name_0() const { return ___h_name_0; }
	inline intptr_t* get_address_of_h_name_0() { return &___h_name_0; }
	inline void set_h_name_0(intptr_t value)
	{
		___h_name_0 = value;
	}

	inline static int32_t get_offset_of_h_aliases_1() { return static_cast<int32_t>(offsetof(hostent_t618E4548593621ED80D901F36C0153673B95959E, ___h_aliases_1)); }
	inline intptr_t get_h_aliases_1() const { return ___h_aliases_1; }
	inline intptr_t* get_address_of_h_aliases_1() { return &___h_aliases_1; }
	inline void set_h_aliases_1(intptr_t value)
	{
		___h_aliases_1 = value;
	}

	inline static int32_t get_offset_of_h_addrtype_2() { return static_cast<int32_t>(offsetof(hostent_t618E4548593621ED80D901F36C0153673B95959E, ___h_addrtype_2)); }
	inline int16_t get_h_addrtype_2() const { return ___h_addrtype_2; }
	inline int16_t* get_address_of_h_addrtype_2() { return &___h_addrtype_2; }
	inline void set_h_addrtype_2(int16_t value)
	{
		___h_addrtype_2 = value;
	}

	inline static int32_t get_offset_of_h_length_3() { return static_cast<int32_t>(offsetof(hostent_t618E4548593621ED80D901F36C0153673B95959E, ___h_length_3)); }
	inline int16_t get_h_length_3() const { return ___h_length_3; }
	inline int16_t* get_address_of_h_length_3() { return &___h_length_3; }
	inline void set_h_length_3(int16_t value)
	{
		___h_length_3 = value;
	}

	inline static int32_t get_offset_of_h_addr_list_4() { return static_cast<int32_t>(offsetof(hostent_t618E4548593621ED80D901F36C0153673B95959E, ___h_addr_list_4)); }
	inline intptr_t get_h_addr_list_4() const { return ___h_addr_list_4; }
	inline intptr_t* get_address_of_h_addr_list_4() { return &___h_addr_list_4; }
	inline void set_h_addr_list_4(intptr_t value)
	{
		___h_addr_list_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HOSTENT_T618E4548593621ED80D901F36C0153673B95959E_H
#ifndef SECURITYACTION_T88130AC24A6765CFA3B9DF2906E8E8F9360EB029_H
#define SECURITYACTION_T88130AC24A6765CFA3B9DF2906E8E8F9360EB029_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Permissions.SecurityAction
struct  SecurityAction_t88130AC24A6765CFA3B9DF2906E8E8F9360EB029 
{
public:
	// System.Int32 System.Security.Permissions.SecurityAction::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SecurityAction_t88130AC24A6765CFA3B9DF2906E8E8F9360EB029, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECURITYACTION_T88130AC24A6765CFA3B9DF2906E8E8F9360EB029_H
#ifndef TOKENIMPERSONATIONLEVEL_TED478ED25688E978F79556E1A2335F7262023D26_H
#define TOKENIMPERSONATIONLEVEL_TED478ED25688E978F79556E1A2335F7262023D26_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Principal.TokenImpersonationLevel
struct  TokenImpersonationLevel_tED478ED25688E978F79556E1A2335F7262023D26 
{
public:
	// System.Int32 System.Security.Principal.TokenImpersonationLevel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TokenImpersonationLevel_tED478ED25688E978F79556E1A2335F7262023D26, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOKENIMPERSONATIONLEVEL_TED478ED25688E978F79556E1A2335F7262023D26_H
#ifndef WAITHANDLE_TFD46B5B45A6BB296EA3A104C91DF2A7C03C10AC6_H
#define WAITHANDLE_TFD46B5B45A6BB296EA3A104C91DF2A7C03C10AC6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.WaitHandle
struct  WaitHandle_tFD46B5B45A6BB296EA3A104C91DF2A7C03C10AC6  : public MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF
{
public:
	// System.IntPtr System.Threading.WaitHandle::waitHandle
	intptr_t ___waitHandle_3;
	// Microsoft.Win32.SafeHandles.SafeWaitHandle modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.WaitHandle::safeWaitHandle
	SafeWaitHandle_t51DB35FF382E636FF3B868D87816733894D46CF2 * ___safeWaitHandle_4;
	// System.Boolean System.Threading.WaitHandle::hasThreadAffinity
	bool ___hasThreadAffinity_5;

public:
	inline static int32_t get_offset_of_waitHandle_3() { return static_cast<int32_t>(offsetof(WaitHandle_tFD46B5B45A6BB296EA3A104C91DF2A7C03C10AC6, ___waitHandle_3)); }
	inline intptr_t get_waitHandle_3() const { return ___waitHandle_3; }
	inline intptr_t* get_address_of_waitHandle_3() { return &___waitHandle_3; }
	inline void set_waitHandle_3(intptr_t value)
	{
		___waitHandle_3 = value;
	}

	inline static int32_t get_offset_of_safeWaitHandle_4() { return static_cast<int32_t>(offsetof(WaitHandle_tFD46B5B45A6BB296EA3A104C91DF2A7C03C10AC6, ___safeWaitHandle_4)); }
	inline SafeWaitHandle_t51DB35FF382E636FF3B868D87816733894D46CF2 * get_safeWaitHandle_4() const { return ___safeWaitHandle_4; }
	inline SafeWaitHandle_t51DB35FF382E636FF3B868D87816733894D46CF2 ** get_address_of_safeWaitHandle_4() { return &___safeWaitHandle_4; }
	inline void set_safeWaitHandle_4(SafeWaitHandle_t51DB35FF382E636FF3B868D87816733894D46CF2 * value)
	{
		___safeWaitHandle_4 = value;
		Il2CppCodeGenWriteBarrier((&___safeWaitHandle_4), value);
	}

	inline static int32_t get_offset_of_hasThreadAffinity_5() { return static_cast<int32_t>(offsetof(WaitHandle_tFD46B5B45A6BB296EA3A104C91DF2A7C03C10AC6, ___hasThreadAffinity_5)); }
	inline bool get_hasThreadAffinity_5() const { return ___hasThreadAffinity_5; }
	inline bool* get_address_of_hasThreadAffinity_5() { return &___hasThreadAffinity_5; }
	inline void set_hasThreadAffinity_5(bool value)
	{
		___hasThreadAffinity_5 = value;
	}
};

struct WaitHandle_tFD46B5B45A6BB296EA3A104C91DF2A7C03C10AC6_StaticFields
{
public:
	// System.IntPtr System.Threading.WaitHandle::InvalidHandle
	intptr_t ___InvalidHandle_10;

public:
	inline static int32_t get_offset_of_InvalidHandle_10() { return static_cast<int32_t>(offsetof(WaitHandle_tFD46B5B45A6BB296EA3A104C91DF2A7C03C10AC6_StaticFields, ___InvalidHandle_10)); }
	inline intptr_t get_InvalidHandle_10() const { return ___InvalidHandle_10; }
	inline intptr_t* get_address_of_InvalidHandle_10() { return &___InvalidHandle_10; }
	inline void set_InvalidHandle_10(intptr_t value)
	{
		___InvalidHandle_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Threading.WaitHandle
struct WaitHandle_tFD46B5B45A6BB296EA3A104C91DF2A7C03C10AC6_marshaled_pinvoke : public MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF_marshaled_pinvoke
{
	intptr_t ___waitHandle_3;
	void* ___safeWaitHandle_4;
	int32_t ___hasThreadAffinity_5;
};
// Native definition for COM marshalling of System.Threading.WaitHandle
struct WaitHandle_tFD46B5B45A6BB296EA3A104C91DF2A7C03C10AC6_marshaled_com : public MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF_marshaled_com
{
	intptr_t ___waitHandle_3;
	void* ___safeWaitHandle_4;
	int32_t ___hasThreadAffinity_5;
};
#endif // WAITHANDLE_TFD46B5B45A6BB296EA3A104C91DF2A7C03C10AC6_H
#ifndef TIMESPAN_TA8069278ACE8A74D6DF7D514A9CD4432433F64C4_H
#define TIMESPAN_TA8069278ACE8A74D6DF7D514A9CD4432433F64C4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_22;

public:
	inline static int32_t get_offset_of__ticks_22() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4, ____ticks_22)); }
	inline int64_t get__ticks_22() const { return ____ticks_22; }
	inline int64_t* get_address_of__ticks_22() { return &____ticks_22; }
	inline void set__ticks_22(int64_t value)
	{
		____ticks_22 = value;
	}
};

struct TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___Zero_19;
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___MaxValue_20;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___MinValue_21;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.TimeSpan::_legacyConfigChecked
	bool ____legacyConfigChecked_23;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.TimeSpan::_legacyMode
	bool ____legacyMode_24;

public:
	inline static int32_t get_offset_of_Zero_19() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ___Zero_19)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_Zero_19() const { return ___Zero_19; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_Zero_19() { return &___Zero_19; }
	inline void set_Zero_19(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___Zero_19 = value;
	}

	inline static int32_t get_offset_of_MaxValue_20() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ___MaxValue_20)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_MaxValue_20() const { return ___MaxValue_20; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_MaxValue_20() { return &___MaxValue_20; }
	inline void set_MaxValue_20(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___MaxValue_20 = value;
	}

	inline static int32_t get_offset_of_MinValue_21() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ___MinValue_21)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_MinValue_21() const { return ___MinValue_21; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_MinValue_21() { return &___MinValue_21; }
	inline void set_MinValue_21(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___MinValue_21 = value;
	}

	inline static int32_t get_offset_of__legacyConfigChecked_23() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ____legacyConfigChecked_23)); }
	inline bool get__legacyConfigChecked_23() const { return ____legacyConfigChecked_23; }
	inline bool* get_address_of__legacyConfigChecked_23() { return &____legacyConfigChecked_23; }
	inline void set__legacyConfigChecked_23(bool value)
	{
		____legacyConfigChecked_23 = value;
	}

	inline static int32_t get_offset_of__legacyMode_24() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ____legacyMode_24)); }
	inline bool get__legacyMode_24() const { return ____legacyMode_24; }
	inline bool* get_address_of__legacyMode_24() { return &____legacyMode_24; }
	inline void set__legacyMode_24(bool value)
	{
		____legacyMode_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_TA8069278ACE8A74D6DF7D514A9CD4432433F64C4_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef POOLEDSTREAM_TA0A798FD29060971887DF12D77E4BB444BE34D1B_H
#define POOLEDSTREAM_TA0A798FD29060971887DF12D77E4BB444BE34D1B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.PooledStream
struct  PooledStream_tA0A798FD29060971887DF12D77E4BB444BE34D1B  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// System.Boolean System.Net.PooledStream::m_CheckLifetime
	bool ___m_CheckLifetime_5;
	// System.TimeSpan System.Net.PooledStream::m_Lifetime
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___m_Lifetime_6;
	// System.DateTime System.Net.PooledStream::m_CreateTime
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___m_CreateTime_7;
	// System.Boolean System.Net.PooledStream::m_ConnectionIsDoomed
	bool ___m_ConnectionIsDoomed_8;
	// System.Net.ConnectionPool System.Net.PooledStream::m_ConnectionPool
	ConnectionPool_t7BEC38A8953B38D2C1262D373089B8915ECE020C * ___m_ConnectionPool_9;
	// System.WeakReference System.Net.PooledStream::m_Owner
	WeakReference_t0495CC81CD6403E662B7700B802443F6F730E39D * ___m_Owner_10;
	// System.Int32 System.Net.PooledStream::m_PooledCount
	int32_t ___m_PooledCount_11;
	// System.Boolean System.Net.PooledStream::m_Initalizing
	bool ___m_Initalizing_12;
	// System.Net.IPAddress System.Net.PooledStream::m_ServerAddress
	IPAddress_t77F35D21A3027F0CE7B38EA9B56BFD31B28952CE * ___m_ServerAddress_13;
	// System.Net.Sockets.NetworkStream System.Net.PooledStream::m_NetworkStream
	NetworkStream_t362D0CD0C74C2F5CBD02905C9422E4240872ADCA * ___m_NetworkStream_14;
	// System.Net.Sockets.Socket System.Net.PooledStream::m_AbortSocket
	Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 * ___m_AbortSocket_15;
	// System.Net.Sockets.Socket System.Net.PooledStream::m_AbortSocket6
	Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 * ___m_AbortSocket6_16;
	// System.Boolean System.Net.PooledStream::m_JustConnected
	bool ___m_JustConnected_17;
	// System.Net.GeneralAsyncDelegate System.Net.PooledStream::m_AsyncCallback
	GeneralAsyncDelegate_tD535232B4C5C7E31A01C18A2A072F80C1C085D92 * ___m_AsyncCallback_18;

public:
	inline static int32_t get_offset_of_m_CheckLifetime_5() { return static_cast<int32_t>(offsetof(PooledStream_tA0A798FD29060971887DF12D77E4BB444BE34D1B, ___m_CheckLifetime_5)); }
	inline bool get_m_CheckLifetime_5() const { return ___m_CheckLifetime_5; }
	inline bool* get_address_of_m_CheckLifetime_5() { return &___m_CheckLifetime_5; }
	inline void set_m_CheckLifetime_5(bool value)
	{
		___m_CheckLifetime_5 = value;
	}

	inline static int32_t get_offset_of_m_Lifetime_6() { return static_cast<int32_t>(offsetof(PooledStream_tA0A798FD29060971887DF12D77E4BB444BE34D1B, ___m_Lifetime_6)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_m_Lifetime_6() const { return ___m_Lifetime_6; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_m_Lifetime_6() { return &___m_Lifetime_6; }
	inline void set_m_Lifetime_6(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___m_Lifetime_6 = value;
	}

	inline static int32_t get_offset_of_m_CreateTime_7() { return static_cast<int32_t>(offsetof(PooledStream_tA0A798FD29060971887DF12D77E4BB444BE34D1B, ___m_CreateTime_7)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_m_CreateTime_7() const { return ___m_CreateTime_7; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_m_CreateTime_7() { return &___m_CreateTime_7; }
	inline void set_m_CreateTime_7(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___m_CreateTime_7 = value;
	}

	inline static int32_t get_offset_of_m_ConnectionIsDoomed_8() { return static_cast<int32_t>(offsetof(PooledStream_tA0A798FD29060971887DF12D77E4BB444BE34D1B, ___m_ConnectionIsDoomed_8)); }
	inline bool get_m_ConnectionIsDoomed_8() const { return ___m_ConnectionIsDoomed_8; }
	inline bool* get_address_of_m_ConnectionIsDoomed_8() { return &___m_ConnectionIsDoomed_8; }
	inline void set_m_ConnectionIsDoomed_8(bool value)
	{
		___m_ConnectionIsDoomed_8 = value;
	}

	inline static int32_t get_offset_of_m_ConnectionPool_9() { return static_cast<int32_t>(offsetof(PooledStream_tA0A798FD29060971887DF12D77E4BB444BE34D1B, ___m_ConnectionPool_9)); }
	inline ConnectionPool_t7BEC38A8953B38D2C1262D373089B8915ECE020C * get_m_ConnectionPool_9() const { return ___m_ConnectionPool_9; }
	inline ConnectionPool_t7BEC38A8953B38D2C1262D373089B8915ECE020C ** get_address_of_m_ConnectionPool_9() { return &___m_ConnectionPool_9; }
	inline void set_m_ConnectionPool_9(ConnectionPool_t7BEC38A8953B38D2C1262D373089B8915ECE020C * value)
	{
		___m_ConnectionPool_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_ConnectionPool_9), value);
	}

	inline static int32_t get_offset_of_m_Owner_10() { return static_cast<int32_t>(offsetof(PooledStream_tA0A798FD29060971887DF12D77E4BB444BE34D1B, ___m_Owner_10)); }
	inline WeakReference_t0495CC81CD6403E662B7700B802443F6F730E39D * get_m_Owner_10() const { return ___m_Owner_10; }
	inline WeakReference_t0495CC81CD6403E662B7700B802443F6F730E39D ** get_address_of_m_Owner_10() { return &___m_Owner_10; }
	inline void set_m_Owner_10(WeakReference_t0495CC81CD6403E662B7700B802443F6F730E39D * value)
	{
		___m_Owner_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_Owner_10), value);
	}

	inline static int32_t get_offset_of_m_PooledCount_11() { return static_cast<int32_t>(offsetof(PooledStream_tA0A798FD29060971887DF12D77E4BB444BE34D1B, ___m_PooledCount_11)); }
	inline int32_t get_m_PooledCount_11() const { return ___m_PooledCount_11; }
	inline int32_t* get_address_of_m_PooledCount_11() { return &___m_PooledCount_11; }
	inline void set_m_PooledCount_11(int32_t value)
	{
		___m_PooledCount_11 = value;
	}

	inline static int32_t get_offset_of_m_Initalizing_12() { return static_cast<int32_t>(offsetof(PooledStream_tA0A798FD29060971887DF12D77E4BB444BE34D1B, ___m_Initalizing_12)); }
	inline bool get_m_Initalizing_12() const { return ___m_Initalizing_12; }
	inline bool* get_address_of_m_Initalizing_12() { return &___m_Initalizing_12; }
	inline void set_m_Initalizing_12(bool value)
	{
		___m_Initalizing_12 = value;
	}

	inline static int32_t get_offset_of_m_ServerAddress_13() { return static_cast<int32_t>(offsetof(PooledStream_tA0A798FD29060971887DF12D77E4BB444BE34D1B, ___m_ServerAddress_13)); }
	inline IPAddress_t77F35D21A3027F0CE7B38EA9B56BFD31B28952CE * get_m_ServerAddress_13() const { return ___m_ServerAddress_13; }
	inline IPAddress_t77F35D21A3027F0CE7B38EA9B56BFD31B28952CE ** get_address_of_m_ServerAddress_13() { return &___m_ServerAddress_13; }
	inline void set_m_ServerAddress_13(IPAddress_t77F35D21A3027F0CE7B38EA9B56BFD31B28952CE * value)
	{
		___m_ServerAddress_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_ServerAddress_13), value);
	}

	inline static int32_t get_offset_of_m_NetworkStream_14() { return static_cast<int32_t>(offsetof(PooledStream_tA0A798FD29060971887DF12D77E4BB444BE34D1B, ___m_NetworkStream_14)); }
	inline NetworkStream_t362D0CD0C74C2F5CBD02905C9422E4240872ADCA * get_m_NetworkStream_14() const { return ___m_NetworkStream_14; }
	inline NetworkStream_t362D0CD0C74C2F5CBD02905C9422E4240872ADCA ** get_address_of_m_NetworkStream_14() { return &___m_NetworkStream_14; }
	inline void set_m_NetworkStream_14(NetworkStream_t362D0CD0C74C2F5CBD02905C9422E4240872ADCA * value)
	{
		___m_NetworkStream_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_NetworkStream_14), value);
	}

	inline static int32_t get_offset_of_m_AbortSocket_15() { return static_cast<int32_t>(offsetof(PooledStream_tA0A798FD29060971887DF12D77E4BB444BE34D1B, ___m_AbortSocket_15)); }
	inline Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 * get_m_AbortSocket_15() const { return ___m_AbortSocket_15; }
	inline Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 ** get_address_of_m_AbortSocket_15() { return &___m_AbortSocket_15; }
	inline void set_m_AbortSocket_15(Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 * value)
	{
		___m_AbortSocket_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_AbortSocket_15), value);
	}

	inline static int32_t get_offset_of_m_AbortSocket6_16() { return static_cast<int32_t>(offsetof(PooledStream_tA0A798FD29060971887DF12D77E4BB444BE34D1B, ___m_AbortSocket6_16)); }
	inline Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 * get_m_AbortSocket6_16() const { return ___m_AbortSocket6_16; }
	inline Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 ** get_address_of_m_AbortSocket6_16() { return &___m_AbortSocket6_16; }
	inline void set_m_AbortSocket6_16(Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 * value)
	{
		___m_AbortSocket6_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_AbortSocket6_16), value);
	}

	inline static int32_t get_offset_of_m_JustConnected_17() { return static_cast<int32_t>(offsetof(PooledStream_tA0A798FD29060971887DF12D77E4BB444BE34D1B, ___m_JustConnected_17)); }
	inline bool get_m_JustConnected_17() const { return ___m_JustConnected_17; }
	inline bool* get_address_of_m_JustConnected_17() { return &___m_JustConnected_17; }
	inline void set_m_JustConnected_17(bool value)
	{
		___m_JustConnected_17 = value;
	}

	inline static int32_t get_offset_of_m_AsyncCallback_18() { return static_cast<int32_t>(offsetof(PooledStream_tA0A798FD29060971887DF12D77E4BB444BE34D1B, ___m_AsyncCallback_18)); }
	inline GeneralAsyncDelegate_tD535232B4C5C7E31A01C18A2A072F80C1C085D92 * get_m_AsyncCallback_18() const { return ___m_AsyncCallback_18; }
	inline GeneralAsyncDelegate_tD535232B4C5C7E31A01C18A2A072F80C1C085D92 ** get_address_of_m_AsyncCallback_18() { return &___m_AsyncCallback_18; }
	inline void set_m_AsyncCallback_18(GeneralAsyncDelegate_tD535232B4C5C7E31A01C18A2A072F80C1C085D92 * value)
	{
		___m_AsyncCallback_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_AsyncCallback_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POOLEDSTREAM_TA0A798FD29060971887DF12D77E4BB444BE34D1B_H
#ifndef PROTOCOLVIOLATIONEXCEPTION_T287E1EFCC1BC7BB76C74A681581BF3A67C68BDFB_H
#define PROTOCOLVIOLATIONEXCEPTION_T287E1EFCC1BC7BB76C74A681581BF3A67C68BDFB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.ProtocolViolationException
struct  ProtocolViolationException_t287E1EFCC1BC7BB76C74A681581BF3A67C68BDFB  : public InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROTOCOLVIOLATIONEXCEPTION_T287E1EFCC1BC7BB76C74A681581BF3A67C68BDFB_H
#ifndef SECURITYBUFFER_T577E697FBA98983D3C81105A488F7940DAAD615A_H
#define SECURITYBUFFER_T577E697FBA98983D3C81105A488F7940DAAD615A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.SecurityBuffer
struct  SecurityBuffer_t577E697FBA98983D3C81105A488F7940DAAD615A  : public RuntimeObject
{
public:
	// System.Int32 System.Net.SecurityBuffer::size
	int32_t ___size_0;
	// System.Net.BufferType System.Net.SecurityBuffer::type
	int32_t ___type_1;
	// System.Byte[] System.Net.SecurityBuffer::token
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___token_2;
	// System.Runtime.InteropServices.SafeHandle System.Net.SecurityBuffer::unmanagedToken
	SafeHandle_t1E326D75E23FD5BB6D40BA322298FDC6526CC383 * ___unmanagedToken_3;
	// System.Int32 System.Net.SecurityBuffer::offset
	int32_t ___offset_4;

public:
	inline static int32_t get_offset_of_size_0() { return static_cast<int32_t>(offsetof(SecurityBuffer_t577E697FBA98983D3C81105A488F7940DAAD615A, ___size_0)); }
	inline int32_t get_size_0() const { return ___size_0; }
	inline int32_t* get_address_of_size_0() { return &___size_0; }
	inline void set_size_0(int32_t value)
	{
		___size_0 = value;
	}

	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(SecurityBuffer_t577E697FBA98983D3C81105A488F7940DAAD615A, ___type_1)); }
	inline int32_t get_type_1() const { return ___type_1; }
	inline int32_t* get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(int32_t value)
	{
		___type_1 = value;
	}

	inline static int32_t get_offset_of_token_2() { return static_cast<int32_t>(offsetof(SecurityBuffer_t577E697FBA98983D3C81105A488F7940DAAD615A, ___token_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_token_2() const { return ___token_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_token_2() { return &___token_2; }
	inline void set_token_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___token_2 = value;
		Il2CppCodeGenWriteBarrier((&___token_2), value);
	}

	inline static int32_t get_offset_of_unmanagedToken_3() { return static_cast<int32_t>(offsetof(SecurityBuffer_t577E697FBA98983D3C81105A488F7940DAAD615A, ___unmanagedToken_3)); }
	inline SafeHandle_t1E326D75E23FD5BB6D40BA322298FDC6526CC383 * get_unmanagedToken_3() const { return ___unmanagedToken_3; }
	inline SafeHandle_t1E326D75E23FD5BB6D40BA322298FDC6526CC383 ** get_address_of_unmanagedToken_3() { return &___unmanagedToken_3; }
	inline void set_unmanagedToken_3(SafeHandle_t1E326D75E23FD5BB6D40BA322298FDC6526CC383 * value)
	{
		___unmanagedToken_3 = value;
		Il2CppCodeGenWriteBarrier((&___unmanagedToken_3), value);
	}

	inline static int32_t get_offset_of_offset_4() { return static_cast<int32_t>(offsetof(SecurityBuffer_t577E697FBA98983D3C81105A488F7940DAAD615A, ___offset_4)); }
	inline int32_t get_offset_4() const { return ___offset_4; }
	inline int32_t* get_address_of_offset_4() { return &___offset_4; }
	inline void set_offset_4(int32_t value)
	{
		___offset_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECURITYBUFFER_T577E697FBA98983D3C81105A488F7940DAAD615A_H
#ifndef SECURITYBUFFERSTRUCT_T40575D8D51B46283505616EB5FA6A4CD9713A58A_H
#define SECURITYBUFFERSTRUCT_T40575D8D51B46283505616EB5FA6A4CD9713A58A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.SecurityBufferStruct
struct  SecurityBufferStruct_t40575D8D51B46283505616EB5FA6A4CD9713A58A 
{
public:
	// System.Int32 System.Net.SecurityBufferStruct::count
	int32_t ___count_0;
	// System.Net.BufferType System.Net.SecurityBufferStruct::type
	int32_t ___type_1;
	// System.IntPtr System.Net.SecurityBufferStruct::token
	intptr_t ___token_2;

public:
	inline static int32_t get_offset_of_count_0() { return static_cast<int32_t>(offsetof(SecurityBufferStruct_t40575D8D51B46283505616EB5FA6A4CD9713A58A, ___count_0)); }
	inline int32_t get_count_0() const { return ___count_0; }
	inline int32_t* get_address_of_count_0() { return &___count_0; }
	inline void set_count_0(int32_t value)
	{
		___count_0 = value;
	}

	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(SecurityBufferStruct_t40575D8D51B46283505616EB5FA6A4CD9713A58A, ___type_1)); }
	inline int32_t get_type_1() const { return ___type_1; }
	inline int32_t* get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(int32_t value)
	{
		___type_1 = value;
	}

	inline static int32_t get_offset_of_token_2() { return static_cast<int32_t>(offsetof(SecurityBufferStruct_t40575D8D51B46283505616EB5FA6A4CD9713A58A, ___token_2)); }
	inline intptr_t get_token_2() const { return ___token_2; }
	inline intptr_t* get_address_of_token_2() { return &___token_2; }
	inline void set_token_2(intptr_t value)
	{
		___token_2 = value;
	}
};

struct SecurityBufferStruct_t40575D8D51B46283505616EB5FA6A4CD9713A58A_StaticFields
{
public:
	// System.Int32 System.Net.SecurityBufferStruct::Size
	int32_t ___Size_3;

public:
	inline static int32_t get_offset_of_Size_3() { return static_cast<int32_t>(offsetof(SecurityBufferStruct_t40575D8D51B46283505616EB5FA6A4CD9713A58A_StaticFields, ___Size_3)); }
	inline int32_t get_Size_3() const { return ___Size_3; }
	inline int32_t* get_address_of_Size_3() { return &___Size_3; }
	inline void set_Size_3(int32_t value)
	{
		___Size_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECURITYBUFFERSTRUCT_T40575D8D51B46283505616EB5FA6A4CD9713A58A_H
#ifndef SEMAPHORE_T2C8E706636BE60950385992A48222B63F4165885_H
#define SEMAPHORE_T2C8E706636BE60950385992A48222B63F4165885_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Semaphore
struct  Semaphore_t2C8E706636BE60950385992A48222B63F4165885  : public WaitHandle_tFD46B5B45A6BB296EA3A104C91DF2A7C03C10AC6
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEMAPHORE_T2C8E706636BE60950385992A48222B63F4165885_H
#ifndef TIMERNODE_T5596C0BAF3E85D0C2444ECA74917630FDEF5087B_H
#define TIMERNODE_T5596C0BAF3E85D0C2444ECA74917630FDEF5087B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.TimerThread_TimerNode
struct  TimerNode_t5596C0BAF3E85D0C2444ECA74917630FDEF5087B  : public Timer_t3B21B1013E27B5DC9FED14EC0921A5F51230D46F
{
public:
	// System.Net.TimerThread_TimerNode_TimerState System.Net.TimerThread_TimerNode::m_TimerState
	int32_t ___m_TimerState_2;
	// System.Net.TimerThread_Callback System.Net.TimerThread_TimerNode::m_Callback
	Callback_t8DF3FD84AB632709C486978BE28ED721EB3A40E3 * ___m_Callback_3;
	// System.Object System.Net.TimerThread_TimerNode::m_Context
	RuntimeObject * ___m_Context_4;
	// System.Object System.Net.TimerThread_TimerNode::m_QueueLock
	RuntimeObject * ___m_QueueLock_5;
	// System.Net.TimerThread_TimerNode System.Net.TimerThread_TimerNode::next
	TimerNode_t5596C0BAF3E85D0C2444ECA74917630FDEF5087B * ___next_6;
	// System.Net.TimerThread_TimerNode System.Net.TimerThread_TimerNode::prev
	TimerNode_t5596C0BAF3E85D0C2444ECA74917630FDEF5087B * ___prev_7;

public:
	inline static int32_t get_offset_of_m_TimerState_2() { return static_cast<int32_t>(offsetof(TimerNode_t5596C0BAF3E85D0C2444ECA74917630FDEF5087B, ___m_TimerState_2)); }
	inline int32_t get_m_TimerState_2() const { return ___m_TimerState_2; }
	inline int32_t* get_address_of_m_TimerState_2() { return &___m_TimerState_2; }
	inline void set_m_TimerState_2(int32_t value)
	{
		___m_TimerState_2 = value;
	}

	inline static int32_t get_offset_of_m_Callback_3() { return static_cast<int32_t>(offsetof(TimerNode_t5596C0BAF3E85D0C2444ECA74917630FDEF5087B, ___m_Callback_3)); }
	inline Callback_t8DF3FD84AB632709C486978BE28ED721EB3A40E3 * get_m_Callback_3() const { return ___m_Callback_3; }
	inline Callback_t8DF3FD84AB632709C486978BE28ED721EB3A40E3 ** get_address_of_m_Callback_3() { return &___m_Callback_3; }
	inline void set_m_Callback_3(Callback_t8DF3FD84AB632709C486978BE28ED721EB3A40E3 * value)
	{
		___m_Callback_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Callback_3), value);
	}

	inline static int32_t get_offset_of_m_Context_4() { return static_cast<int32_t>(offsetof(TimerNode_t5596C0BAF3E85D0C2444ECA74917630FDEF5087B, ___m_Context_4)); }
	inline RuntimeObject * get_m_Context_4() const { return ___m_Context_4; }
	inline RuntimeObject ** get_address_of_m_Context_4() { return &___m_Context_4; }
	inline void set_m_Context_4(RuntimeObject * value)
	{
		___m_Context_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Context_4), value);
	}

	inline static int32_t get_offset_of_m_QueueLock_5() { return static_cast<int32_t>(offsetof(TimerNode_t5596C0BAF3E85D0C2444ECA74917630FDEF5087B, ___m_QueueLock_5)); }
	inline RuntimeObject * get_m_QueueLock_5() const { return ___m_QueueLock_5; }
	inline RuntimeObject ** get_address_of_m_QueueLock_5() { return &___m_QueueLock_5; }
	inline void set_m_QueueLock_5(RuntimeObject * value)
	{
		___m_QueueLock_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_QueueLock_5), value);
	}

	inline static int32_t get_offset_of_next_6() { return static_cast<int32_t>(offsetof(TimerNode_t5596C0BAF3E85D0C2444ECA74917630FDEF5087B, ___next_6)); }
	inline TimerNode_t5596C0BAF3E85D0C2444ECA74917630FDEF5087B * get_next_6() const { return ___next_6; }
	inline TimerNode_t5596C0BAF3E85D0C2444ECA74917630FDEF5087B ** get_address_of_next_6() { return &___next_6; }
	inline void set_next_6(TimerNode_t5596C0BAF3E85D0C2444ECA74917630FDEF5087B * value)
	{
		___next_6 = value;
		Il2CppCodeGenWriteBarrier((&___next_6), value);
	}

	inline static int32_t get_offset_of_prev_7() { return static_cast<int32_t>(offsetof(TimerNode_t5596C0BAF3E85D0C2444ECA74917630FDEF5087B, ___prev_7)); }
	inline TimerNode_t5596C0BAF3E85D0C2444ECA74917630FDEF5087B * get_prev_7() const { return ___prev_7; }
	inline TimerNode_t5596C0BAF3E85D0C2444ECA74917630FDEF5087B ** get_address_of_prev_7() { return &___prev_7; }
	inline void set_prev_7(TimerNode_t5596C0BAF3E85D0C2444ECA74917630FDEF5087B * value)
	{
		___prev_7 = value;
		Il2CppCodeGenWriteBarrier((&___prev_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMERNODE_T5596C0BAF3E85D0C2444ECA74917630FDEF5087B_H
#ifndef WEBEXCEPTION_TD400C9DEBEBB6AEDA77500E634D20692E27A993D_H
#define WEBEXCEPTION_TD400C9DEBEBB6AEDA77500E634D20692E27A993D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebException
struct  WebException_tD400C9DEBEBB6AEDA77500E634D20692E27A993D  : public InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1
{
public:
	// System.Net.WebExceptionStatus System.Net.WebException::m_Status
	int32_t ___m_Status_17;
	// System.Net.WebResponse System.Net.WebException::m_Response
	WebResponse_t5C91B5B83E2FBA2EABC6FDF2A70E9AFD9BB059BD * ___m_Response_18;
	// System.Net.WebExceptionInternalStatus System.Net.WebException::m_InternalStatus
	int32_t ___m_InternalStatus_19;

public:
	inline static int32_t get_offset_of_m_Status_17() { return static_cast<int32_t>(offsetof(WebException_tD400C9DEBEBB6AEDA77500E634D20692E27A993D, ___m_Status_17)); }
	inline int32_t get_m_Status_17() const { return ___m_Status_17; }
	inline int32_t* get_address_of_m_Status_17() { return &___m_Status_17; }
	inline void set_m_Status_17(int32_t value)
	{
		___m_Status_17 = value;
	}

	inline static int32_t get_offset_of_m_Response_18() { return static_cast<int32_t>(offsetof(WebException_tD400C9DEBEBB6AEDA77500E634D20692E27A993D, ___m_Response_18)); }
	inline WebResponse_t5C91B5B83E2FBA2EABC6FDF2A70E9AFD9BB059BD * get_m_Response_18() const { return ___m_Response_18; }
	inline WebResponse_t5C91B5B83E2FBA2EABC6FDF2A70E9AFD9BB059BD ** get_address_of_m_Response_18() { return &___m_Response_18; }
	inline void set_m_Response_18(WebResponse_t5C91B5B83E2FBA2EABC6FDF2A70E9AFD9BB059BD * value)
	{
		___m_Response_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_Response_18), value);
	}

	inline static int32_t get_offset_of_m_InternalStatus_19() { return static_cast<int32_t>(offsetof(WebException_tD400C9DEBEBB6AEDA77500E634D20692E27A993D, ___m_InternalStatus_19)); }
	inline int32_t get_m_InternalStatus_19() const { return ___m_InternalStatus_19; }
	inline int32_t* get_address_of_m_InternalStatus_19() { return &___m_InternalStatus_19; }
	inline void set_m_InternalStatus_19(int32_t value)
	{
		___m_InternalStatus_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBEXCEPTION_TD400C9DEBEBB6AEDA77500E634D20692E27A993D_H
#ifndef WEBHEADERCOLLECTION_TB57EC4CD795CACE87271D6887BBED385DC37B304_H
#define WEBHEADERCOLLECTION_TB57EC4CD795CACE87271D6887BBED385DC37B304_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebHeaderCollection
struct  WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304  : public NameValueCollection_t7C7CED43E4C6E997E3C8012F1D2CC4027FAD10D1
{
public:
	// System.String[] System.Net.WebHeaderCollection::m_CommonHeaders
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___m_CommonHeaders_15;
	// System.Int32 System.Net.WebHeaderCollection::m_NumCommonHeaders
	int32_t ___m_NumCommonHeaders_16;
	// System.Collections.Specialized.NameValueCollection System.Net.WebHeaderCollection::m_InnerCollection
	NameValueCollection_t7C7CED43E4C6E997E3C8012F1D2CC4027FAD10D1 * ___m_InnerCollection_37;
	// System.Net.WebHeaderCollectionType System.Net.WebHeaderCollection::m_Type
	uint16_t ___m_Type_38;

public:
	inline static int32_t get_offset_of_m_CommonHeaders_15() { return static_cast<int32_t>(offsetof(WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304, ___m_CommonHeaders_15)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_m_CommonHeaders_15() const { return ___m_CommonHeaders_15; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_m_CommonHeaders_15() { return &___m_CommonHeaders_15; }
	inline void set_m_CommonHeaders_15(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___m_CommonHeaders_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_CommonHeaders_15), value);
	}

	inline static int32_t get_offset_of_m_NumCommonHeaders_16() { return static_cast<int32_t>(offsetof(WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304, ___m_NumCommonHeaders_16)); }
	inline int32_t get_m_NumCommonHeaders_16() const { return ___m_NumCommonHeaders_16; }
	inline int32_t* get_address_of_m_NumCommonHeaders_16() { return &___m_NumCommonHeaders_16; }
	inline void set_m_NumCommonHeaders_16(int32_t value)
	{
		___m_NumCommonHeaders_16 = value;
	}

	inline static int32_t get_offset_of_m_InnerCollection_37() { return static_cast<int32_t>(offsetof(WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304, ___m_InnerCollection_37)); }
	inline NameValueCollection_t7C7CED43E4C6E997E3C8012F1D2CC4027FAD10D1 * get_m_InnerCollection_37() const { return ___m_InnerCollection_37; }
	inline NameValueCollection_t7C7CED43E4C6E997E3C8012F1D2CC4027FAD10D1 ** get_address_of_m_InnerCollection_37() { return &___m_InnerCollection_37; }
	inline void set_m_InnerCollection_37(NameValueCollection_t7C7CED43E4C6E997E3C8012F1D2CC4027FAD10D1 * value)
	{
		___m_InnerCollection_37 = value;
		Il2CppCodeGenWriteBarrier((&___m_InnerCollection_37), value);
	}

	inline static int32_t get_offset_of_m_Type_38() { return static_cast<int32_t>(offsetof(WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304, ___m_Type_38)); }
	inline uint16_t get_m_Type_38() const { return ___m_Type_38; }
	inline uint16_t* get_address_of_m_Type_38() { return &___m_Type_38; }
	inline void set_m_Type_38(uint16_t value)
	{
		___m_Type_38 = value;
	}
};

struct WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304_StaticFields
{
public:
	// System.Net.HeaderInfoTable System.Net.WebHeaderCollection::HInfo
	HeaderInfoTable_t16B4CA77715B871579C8DE60EBD92E8EDD332BAF * ___HInfo_14;
	// System.String[] System.Net.WebHeaderCollection::s_CommonHeaderNames
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___s_CommonHeaderNames_17;
	// System.SByte[] System.Net.WebHeaderCollection::s_CommonHeaderHints
	SByteU5BU5D_t623D1F33C61DEAC564E2B0560E00F1E1364F7889* ___s_CommonHeaderHints_18;
	// System.Char[] System.Net.WebHeaderCollection::HttpTrimCharacters
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___HttpTrimCharacters_39;
	// System.Net.WebHeaderCollection_RfcChar[] System.Net.WebHeaderCollection::RfcCharMap
	RfcCharU5BU5D_t27AD0ADBD612E10FCEF4917B5E70094398C6EC4E* ___RfcCharMap_40;

public:
	inline static int32_t get_offset_of_HInfo_14() { return static_cast<int32_t>(offsetof(WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304_StaticFields, ___HInfo_14)); }
	inline HeaderInfoTable_t16B4CA77715B871579C8DE60EBD92E8EDD332BAF * get_HInfo_14() const { return ___HInfo_14; }
	inline HeaderInfoTable_t16B4CA77715B871579C8DE60EBD92E8EDD332BAF ** get_address_of_HInfo_14() { return &___HInfo_14; }
	inline void set_HInfo_14(HeaderInfoTable_t16B4CA77715B871579C8DE60EBD92E8EDD332BAF * value)
	{
		___HInfo_14 = value;
		Il2CppCodeGenWriteBarrier((&___HInfo_14), value);
	}

	inline static int32_t get_offset_of_s_CommonHeaderNames_17() { return static_cast<int32_t>(offsetof(WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304_StaticFields, ___s_CommonHeaderNames_17)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_s_CommonHeaderNames_17() const { return ___s_CommonHeaderNames_17; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_s_CommonHeaderNames_17() { return &___s_CommonHeaderNames_17; }
	inline void set_s_CommonHeaderNames_17(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___s_CommonHeaderNames_17 = value;
		Il2CppCodeGenWriteBarrier((&___s_CommonHeaderNames_17), value);
	}

	inline static int32_t get_offset_of_s_CommonHeaderHints_18() { return static_cast<int32_t>(offsetof(WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304_StaticFields, ___s_CommonHeaderHints_18)); }
	inline SByteU5BU5D_t623D1F33C61DEAC564E2B0560E00F1E1364F7889* get_s_CommonHeaderHints_18() const { return ___s_CommonHeaderHints_18; }
	inline SByteU5BU5D_t623D1F33C61DEAC564E2B0560E00F1E1364F7889** get_address_of_s_CommonHeaderHints_18() { return &___s_CommonHeaderHints_18; }
	inline void set_s_CommonHeaderHints_18(SByteU5BU5D_t623D1F33C61DEAC564E2B0560E00F1E1364F7889* value)
	{
		___s_CommonHeaderHints_18 = value;
		Il2CppCodeGenWriteBarrier((&___s_CommonHeaderHints_18), value);
	}

	inline static int32_t get_offset_of_HttpTrimCharacters_39() { return static_cast<int32_t>(offsetof(WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304_StaticFields, ___HttpTrimCharacters_39)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_HttpTrimCharacters_39() const { return ___HttpTrimCharacters_39; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_HttpTrimCharacters_39() { return &___HttpTrimCharacters_39; }
	inline void set_HttpTrimCharacters_39(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___HttpTrimCharacters_39 = value;
		Il2CppCodeGenWriteBarrier((&___HttpTrimCharacters_39), value);
	}

	inline static int32_t get_offset_of_RfcCharMap_40() { return static_cast<int32_t>(offsetof(WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304_StaticFields, ___RfcCharMap_40)); }
	inline RfcCharU5BU5D_t27AD0ADBD612E10FCEF4917B5E70094398C6EC4E* get_RfcCharMap_40() const { return ___RfcCharMap_40; }
	inline RfcCharU5BU5D_t27AD0ADBD612E10FCEF4917B5E70094398C6EC4E** get_address_of_RfcCharMap_40() { return &___RfcCharMap_40; }
	inline void set_RfcCharMap_40(RfcCharU5BU5D_t27AD0ADBD612E10FCEF4917B5E70094398C6EC4E* value)
	{
		___RfcCharMap_40 = value;
		Il2CppCodeGenWriteBarrier((&___RfcCharMap_40), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBHEADERCOLLECTION_TB57EC4CD795CACE87271D6887BBED385DC37B304_H
#ifndef WEBPARSEERROR_TC5C6CFEAE6CEB2F0FA842D77D1F2DDD2C65865E3_H
#define WEBPARSEERROR_TC5C6CFEAE6CEB2F0FA842D77D1F2DDD2C65865E3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebParseError
struct  WebParseError_tC5C6CFEAE6CEB2F0FA842D77D1F2DDD2C65865E3 
{
public:
	// System.Net.WebParseErrorSection System.Net.WebParseError::Section
	int32_t ___Section_0;
	// System.Net.WebParseErrorCode System.Net.WebParseError::Code
	int32_t ___Code_1;

public:
	inline static int32_t get_offset_of_Section_0() { return static_cast<int32_t>(offsetof(WebParseError_tC5C6CFEAE6CEB2F0FA842D77D1F2DDD2C65865E3, ___Section_0)); }
	inline int32_t get_Section_0() const { return ___Section_0; }
	inline int32_t* get_address_of_Section_0() { return &___Section_0; }
	inline void set_Section_0(int32_t value)
	{
		___Section_0 = value;
	}

	inline static int32_t get_offset_of_Code_1() { return static_cast<int32_t>(offsetof(WebParseError_tC5C6CFEAE6CEB2F0FA842D77D1F2DDD2C65865E3, ___Code_1)); }
	inline int32_t get_Code_1() const { return ___Code_1; }
	inline int32_t* get_address_of_Code_1() { return &___Code_1; }
	inline void set_Code_1(int32_t value)
	{
		___Code_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBPARSEERROR_TC5C6CFEAE6CEB2F0FA842D77D1F2DDD2C65865E3_H
#ifndef WEBREQUEST_T5668DA48802E9FE2F1DE5F5A770B218608B918C8_H
#define WEBREQUEST_T5668DA48802E9FE2F1DE5F5A770B218608B918C8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebRequest
struct  WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8  : public MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF
{
public:
	// System.Net.Security.AuthenticationLevel System.Net.WebRequest::m_AuthenticationLevel
	int32_t ___m_AuthenticationLevel_5;
	// System.Security.Principal.TokenImpersonationLevel System.Net.WebRequest::m_ImpersonationLevel
	int32_t ___m_ImpersonationLevel_6;
	// System.Net.Cache.RequestCachePolicy System.Net.WebRequest::m_CachePolicy
	RequestCachePolicy_t30D7352C7E9D49EEADD492A70EC92C118D90CD61 * ___m_CachePolicy_7;
	// System.Net.Cache.RequestCacheProtocol System.Net.WebRequest::m_CacheProtocol
	RequestCacheProtocol_t51DE21412EAD66CAD600D3A6940942920340D35D * ___m_CacheProtocol_8;
	// System.Net.Cache.RequestCacheBinding System.Net.WebRequest::m_CacheBinding
	RequestCacheBinding_tB84D71781C4BCEF43DEBC72165283C4543BA4724 * ___m_CacheBinding_9;

public:
	inline static int32_t get_offset_of_m_AuthenticationLevel_5() { return static_cast<int32_t>(offsetof(WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8, ___m_AuthenticationLevel_5)); }
	inline int32_t get_m_AuthenticationLevel_5() const { return ___m_AuthenticationLevel_5; }
	inline int32_t* get_address_of_m_AuthenticationLevel_5() { return &___m_AuthenticationLevel_5; }
	inline void set_m_AuthenticationLevel_5(int32_t value)
	{
		___m_AuthenticationLevel_5 = value;
	}

	inline static int32_t get_offset_of_m_ImpersonationLevel_6() { return static_cast<int32_t>(offsetof(WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8, ___m_ImpersonationLevel_6)); }
	inline int32_t get_m_ImpersonationLevel_6() const { return ___m_ImpersonationLevel_6; }
	inline int32_t* get_address_of_m_ImpersonationLevel_6() { return &___m_ImpersonationLevel_6; }
	inline void set_m_ImpersonationLevel_6(int32_t value)
	{
		___m_ImpersonationLevel_6 = value;
	}

	inline static int32_t get_offset_of_m_CachePolicy_7() { return static_cast<int32_t>(offsetof(WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8, ___m_CachePolicy_7)); }
	inline RequestCachePolicy_t30D7352C7E9D49EEADD492A70EC92C118D90CD61 * get_m_CachePolicy_7() const { return ___m_CachePolicy_7; }
	inline RequestCachePolicy_t30D7352C7E9D49EEADD492A70EC92C118D90CD61 ** get_address_of_m_CachePolicy_7() { return &___m_CachePolicy_7; }
	inline void set_m_CachePolicy_7(RequestCachePolicy_t30D7352C7E9D49EEADD492A70EC92C118D90CD61 * value)
	{
		___m_CachePolicy_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_CachePolicy_7), value);
	}

	inline static int32_t get_offset_of_m_CacheProtocol_8() { return static_cast<int32_t>(offsetof(WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8, ___m_CacheProtocol_8)); }
	inline RequestCacheProtocol_t51DE21412EAD66CAD600D3A6940942920340D35D * get_m_CacheProtocol_8() const { return ___m_CacheProtocol_8; }
	inline RequestCacheProtocol_t51DE21412EAD66CAD600D3A6940942920340D35D ** get_address_of_m_CacheProtocol_8() { return &___m_CacheProtocol_8; }
	inline void set_m_CacheProtocol_8(RequestCacheProtocol_t51DE21412EAD66CAD600D3A6940942920340D35D * value)
	{
		___m_CacheProtocol_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_CacheProtocol_8), value);
	}

	inline static int32_t get_offset_of_m_CacheBinding_9() { return static_cast<int32_t>(offsetof(WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8, ___m_CacheBinding_9)); }
	inline RequestCacheBinding_tB84D71781C4BCEF43DEBC72165283C4543BA4724 * get_m_CacheBinding_9() const { return ___m_CacheBinding_9; }
	inline RequestCacheBinding_tB84D71781C4BCEF43DEBC72165283C4543BA4724 ** get_address_of_m_CacheBinding_9() { return &___m_CacheBinding_9; }
	inline void set_m_CacheBinding_9(RequestCacheBinding_tB84D71781C4BCEF43DEBC72165283C4543BA4724 * value)
	{
		___m_CacheBinding_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_CacheBinding_9), value);
	}
};

struct WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8_StaticFields
{
public:
	// System.Collections.ArrayList modreq(System.Runtime.CompilerServices.IsVolatile) System.Net.WebRequest::s_PrefixList
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ___s_PrefixList_2;
	// System.Object System.Net.WebRequest::s_InternalSyncObject
	RuntimeObject * ___s_InternalSyncObject_3;
	// System.Net.TimerThread_Queue System.Net.WebRequest::s_DefaultTimerQueue
	Queue_tCCFF6A2FCF584216AEDA04A483FB808E2D493643 * ___s_DefaultTimerQueue_4;
	// System.Net.WebRequest_DesignerWebRequestCreate System.Net.WebRequest::webRequestCreate
	DesignerWebRequestCreate_t613DD91D4F07703DC65E847B367F4DCD5710E2A3 * ___webRequestCreate_10;
	// System.Net.IWebProxy modreq(System.Runtime.CompilerServices.IsVolatile) System.Net.WebRequest::s_DefaultWebProxy
	RuntimeObject* ___s_DefaultWebProxy_11;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.Net.WebRequest::s_DefaultWebProxyInitialized
	bool ___s_DefaultWebProxyInitialized_12;

public:
	inline static int32_t get_offset_of_s_PrefixList_2() { return static_cast<int32_t>(offsetof(WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8_StaticFields, ___s_PrefixList_2)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get_s_PrefixList_2() const { return ___s_PrefixList_2; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of_s_PrefixList_2() { return &___s_PrefixList_2; }
	inline void set_s_PrefixList_2(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		___s_PrefixList_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_PrefixList_2), value);
	}

	inline static int32_t get_offset_of_s_InternalSyncObject_3() { return static_cast<int32_t>(offsetof(WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8_StaticFields, ___s_InternalSyncObject_3)); }
	inline RuntimeObject * get_s_InternalSyncObject_3() const { return ___s_InternalSyncObject_3; }
	inline RuntimeObject ** get_address_of_s_InternalSyncObject_3() { return &___s_InternalSyncObject_3; }
	inline void set_s_InternalSyncObject_3(RuntimeObject * value)
	{
		___s_InternalSyncObject_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_InternalSyncObject_3), value);
	}

	inline static int32_t get_offset_of_s_DefaultTimerQueue_4() { return static_cast<int32_t>(offsetof(WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8_StaticFields, ___s_DefaultTimerQueue_4)); }
	inline Queue_tCCFF6A2FCF584216AEDA04A483FB808E2D493643 * get_s_DefaultTimerQueue_4() const { return ___s_DefaultTimerQueue_4; }
	inline Queue_tCCFF6A2FCF584216AEDA04A483FB808E2D493643 ** get_address_of_s_DefaultTimerQueue_4() { return &___s_DefaultTimerQueue_4; }
	inline void set_s_DefaultTimerQueue_4(Queue_tCCFF6A2FCF584216AEDA04A483FB808E2D493643 * value)
	{
		___s_DefaultTimerQueue_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultTimerQueue_4), value);
	}

	inline static int32_t get_offset_of_webRequestCreate_10() { return static_cast<int32_t>(offsetof(WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8_StaticFields, ___webRequestCreate_10)); }
	inline DesignerWebRequestCreate_t613DD91D4F07703DC65E847B367F4DCD5710E2A3 * get_webRequestCreate_10() const { return ___webRequestCreate_10; }
	inline DesignerWebRequestCreate_t613DD91D4F07703DC65E847B367F4DCD5710E2A3 ** get_address_of_webRequestCreate_10() { return &___webRequestCreate_10; }
	inline void set_webRequestCreate_10(DesignerWebRequestCreate_t613DD91D4F07703DC65E847B367F4DCD5710E2A3 * value)
	{
		___webRequestCreate_10 = value;
		Il2CppCodeGenWriteBarrier((&___webRequestCreate_10), value);
	}

	inline static int32_t get_offset_of_s_DefaultWebProxy_11() { return static_cast<int32_t>(offsetof(WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8_StaticFields, ___s_DefaultWebProxy_11)); }
	inline RuntimeObject* get_s_DefaultWebProxy_11() const { return ___s_DefaultWebProxy_11; }
	inline RuntimeObject** get_address_of_s_DefaultWebProxy_11() { return &___s_DefaultWebProxy_11; }
	inline void set_s_DefaultWebProxy_11(RuntimeObject* value)
	{
		___s_DefaultWebProxy_11 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultWebProxy_11), value);
	}

	inline static int32_t get_offset_of_s_DefaultWebProxyInitialized_12() { return static_cast<int32_t>(offsetof(WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8_StaticFields, ___s_DefaultWebProxyInitialized_12)); }
	inline bool get_s_DefaultWebProxyInitialized_12() const { return ___s_DefaultWebProxyInitialized_12; }
	inline bool* get_address_of_s_DefaultWebProxyInitialized_12() { return &___s_DefaultWebProxyInitialized_12; }
	inline void set_s_DefaultWebProxyInitialized_12(bool value)
	{
		___s_DefaultWebProxyInitialized_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBREQUEST_T5668DA48802E9FE2F1DE5F5A770B218608B918C8_H
#ifndef WEBUTILITY_T405BCFEA1292946A0C732330025C9D3C02AF6C1A_H
#define WEBUTILITY_T405BCFEA1292946A0C732330025C9D3C02AF6C1A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebUtility
struct  WebUtility_t405BCFEA1292946A0C732330025C9D3C02AF6C1A  : public RuntimeObject
{
public:

public:
};

struct WebUtility_t405BCFEA1292946A0C732330025C9D3C02AF6C1A_StaticFields
{
public:
	// System.Char[] System.Net.WebUtility::_htmlEntityEndingChars
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ____htmlEntityEndingChars_7;
	// System.Net.Configuration.UnicodeDecodingConformance modreq(System.Runtime.CompilerServices.IsVolatile) System.Net.WebUtility::_htmlDecodeConformance
	int32_t ____htmlDecodeConformance_8;
	// System.Net.Configuration.UnicodeEncodingConformance modreq(System.Runtime.CompilerServices.IsVolatile) System.Net.WebUtility::_htmlEncodeConformance
	int32_t ____htmlEncodeConformance_9;

public:
	inline static int32_t get_offset_of__htmlEntityEndingChars_7() { return static_cast<int32_t>(offsetof(WebUtility_t405BCFEA1292946A0C732330025C9D3C02AF6C1A_StaticFields, ____htmlEntityEndingChars_7)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get__htmlEntityEndingChars_7() const { return ____htmlEntityEndingChars_7; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of__htmlEntityEndingChars_7() { return &____htmlEntityEndingChars_7; }
	inline void set__htmlEntityEndingChars_7(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		____htmlEntityEndingChars_7 = value;
		Il2CppCodeGenWriteBarrier((&____htmlEntityEndingChars_7), value);
	}

	inline static int32_t get_offset_of__htmlDecodeConformance_8() { return static_cast<int32_t>(offsetof(WebUtility_t405BCFEA1292946A0C732330025C9D3C02AF6C1A_StaticFields, ____htmlDecodeConformance_8)); }
	inline int32_t get__htmlDecodeConformance_8() const { return ____htmlDecodeConformance_8; }
	inline int32_t* get_address_of__htmlDecodeConformance_8() { return &____htmlDecodeConformance_8; }
	inline void set__htmlDecodeConformance_8(int32_t value)
	{
		____htmlDecodeConformance_8 = value;
	}

	inline static int32_t get_offset_of__htmlEncodeConformance_9() { return static_cast<int32_t>(offsetof(WebUtility_t405BCFEA1292946A0C732330025C9D3C02AF6C1A_StaticFields, ____htmlEncodeConformance_9)); }
	inline int32_t get__htmlEncodeConformance_9() const { return ____htmlEncodeConformance_9; }
	inline int32_t* get_address_of__htmlEncodeConformance_9() { return &____htmlEncodeConformance_9; }
	inline void set__htmlEncodeConformance_9(int32_t value)
	{
		____htmlEncodeConformance_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBUTILITY_T405BCFEA1292946A0C732330025C9D3C02AF6C1A_H
#ifndef SECURITYATTRIBUTE_T9F1DB5B572D6E2D518A2F186A21522667521B195_H
#define SECURITYATTRIBUTE_T9F1DB5B572D6E2D518A2F186A21522667521B195_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Permissions.SecurityAttribute
struct  SecurityAttribute_t9F1DB5B572D6E2D518A2F186A21522667521B195  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Security.Permissions.SecurityAction System.Security.Permissions.SecurityAttribute::m_Action
	int32_t ___m_Action_0;
	// System.Boolean System.Security.Permissions.SecurityAttribute::m_Unrestricted
	bool ___m_Unrestricted_1;

public:
	inline static int32_t get_offset_of_m_Action_0() { return static_cast<int32_t>(offsetof(SecurityAttribute_t9F1DB5B572D6E2D518A2F186A21522667521B195, ___m_Action_0)); }
	inline int32_t get_m_Action_0() const { return ___m_Action_0; }
	inline int32_t* get_address_of_m_Action_0() { return &___m_Action_0; }
	inline void set_m_Action_0(int32_t value)
	{
		___m_Action_0 = value;
	}

	inline static int32_t get_offset_of_m_Unrestricted_1() { return static_cast<int32_t>(offsetof(SecurityAttribute_t9F1DB5B572D6E2D518A2F186A21522667521B195, ___m_Unrestricted_1)); }
	inline bool get_m_Unrestricted_1() const { return ___m_Unrestricted_1; }
	inline bool* get_address_of_m_Unrestricted_1() { return &___m_Unrestricted_1; }
	inline void set_m_Unrestricted_1(bool value)
	{
		___m_Unrestricted_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECURITYATTRIBUTE_T9F1DB5B572D6E2D518A2F186A21522667521B195_H
#ifndef GENERALASYNCDELEGATE_TD535232B4C5C7E31A01C18A2A072F80C1C085D92_H
#define GENERALASYNCDELEGATE_TD535232B4C5C7E31A01C18A2A072F80C1C085D92_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.GeneralAsyncDelegate
struct  GeneralAsyncDelegate_tD535232B4C5C7E31A01C18A2A072F80C1C085D92  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERALASYNCDELEGATE_TD535232B4C5C7E31A01C18A2A072F80C1C085D92_H
#ifndef HEADERPARSER_T6B59FF0FD79FFD511A019AE5383DCEF641BA822E_H
#define HEADERPARSER_T6B59FF0FD79FFD511A019AE5383DCEF641BA822E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HeaderParser
struct  HeaderParser_t6B59FF0FD79FFD511A019AE5383DCEF641BA822E  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEADERPARSER_T6B59FF0FD79FFD511A019AE5383DCEF641BA822E_H
#ifndef HTTPABORTDELEGATE_TBF5D375FB316FE054524CCF1035D90029966A637_H
#define HTTPABORTDELEGATE_TBF5D375FB316FE054524CCF1035D90029966A637_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HttpAbortDelegate
struct  HttpAbortDelegate_tBF5D375FB316FE054524CCF1035D90029966A637  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPABORTDELEGATE_TBF5D375FB316FE054524CCF1035D90029966A637_H
#ifndef HTTPCONTINUEDELEGATE_T38DB016AD9C4FA9F4E9B4417278FB8D0594F37AC_H
#define HTTPCONTINUEDELEGATE_T38DB016AD9C4FA9F4E9B4417278FB8D0594F37AC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HttpContinueDelegate
struct  HttpContinueDelegate_t38DB016AD9C4FA9F4E9B4417278FB8D0594F37AC  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPCONTINUEDELEGATE_T38DB016AD9C4FA9F4E9B4417278FB8D0594F37AC_H
#ifndef CALLBACK_T8DF3FD84AB632709C486978BE28ED721EB3A40E3_H
#define CALLBACK_T8DF3FD84AB632709C486978BE28ED721EB3A40E3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.TimerThread_Callback
struct  Callback_t8DF3FD84AB632709C486978BE28ED721EB3A40E3  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CALLBACK_T8DF3FD84AB632709C486978BE28ED721EB3A40E3_H
#ifndef UNLOCKCONNECTIONDELEGATE_T561D49B895EC99A61A9D4BE2F0FBF9B85595A67B_H
#define UNLOCKCONNECTIONDELEGATE_T561D49B895EC99A61A9D4BE2F0FBF9B85595A67B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.UnlockConnectionDelegate
struct  UnlockConnectionDelegate_t561D49B895EC99A61A9D4BE2F0FBF9B85595A67B  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNLOCKCONNECTIONDELEGATE_T561D49B895EC99A61A9D4BE2F0FBF9B85595A67B_H
#ifndef WRITESTREAMCLOSEDEVENTHANDLER_T1FADB2B0A561617F95C30007B786BF5F6FB71B94_H
#define WRITESTREAMCLOSEDEVENTHANDLER_T1FADB2B0A561617F95C30007B786BF5F6FB71B94_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WriteStreamClosedEventHandler
struct  WriteStreamClosedEventHandler_t1FADB2B0A561617F95C30007B786BF5F6FB71B94  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WRITESTREAMCLOSEDEVENTHANDLER_T1FADB2B0A561617F95C30007B786BF5F6FB71B94_H
#ifndef CODEACCESSSECURITYATTRIBUTE_TBBF0EFA8FE4C0ED2D8E2469B658D1CDDFF8850E6_H
#define CODEACCESSSECURITYATTRIBUTE_TBBF0EFA8FE4C0ED2D8E2469B658D1CDDFF8850E6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Permissions.CodeAccessSecurityAttribute
struct  CodeAccessSecurityAttribute_tBBF0EFA8FE4C0ED2D8E2469B658D1CDDFF8850E6  : public SecurityAttribute_t9F1DB5B572D6E2D518A2F186A21522667521B195
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CODEACCESSSECURITYATTRIBUTE_TBBF0EFA8FE4C0ED2D8E2469B658D1CDDFF8850E6_H
#ifndef WEBPERMISSIONATTRIBUTE_T4705EE8C353B760970949AF3EB4B534E9BADE8C3_H
#define WEBPERMISSIONATTRIBUTE_T4705EE8C353B760970949AF3EB4B534E9BADE8C3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebPermissionAttribute
struct  WebPermissionAttribute_t4705EE8C353B760970949AF3EB4B534E9BADE8C3  : public CodeAccessSecurityAttribute_tBBF0EFA8FE4C0ED2D8E2469B658D1CDDFF8850E6
{
public:
	// System.Object System.Net.WebPermissionAttribute::m_accept
	RuntimeObject * ___m_accept_2;
	// System.Object System.Net.WebPermissionAttribute::m_connect
	RuntimeObject * ___m_connect_3;

public:
	inline static int32_t get_offset_of_m_accept_2() { return static_cast<int32_t>(offsetof(WebPermissionAttribute_t4705EE8C353B760970949AF3EB4B534E9BADE8C3, ___m_accept_2)); }
	inline RuntimeObject * get_m_accept_2() const { return ___m_accept_2; }
	inline RuntimeObject ** get_address_of_m_accept_2() { return &___m_accept_2; }
	inline void set_m_accept_2(RuntimeObject * value)
	{
		___m_accept_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_accept_2), value);
	}

	inline static int32_t get_offset_of_m_connect_3() { return static_cast<int32_t>(offsetof(WebPermissionAttribute_t4705EE8C353B760970949AF3EB4B534E9BADE8C3, ___m_connect_3)); }
	inline RuntimeObject * get_m_connect_3() const { return ___m_connect_3; }
	inline RuntimeObject ** get_address_of_m_connect_3() { return &___m_connect_3; }
	inline void set_m_connect_3(RuntimeObject * value)
	{
		___m_connect_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_connect_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBPERMISSIONATTRIBUTE_T4705EE8C353B760970949AF3EB4B534E9BADE8C3_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2900 = { sizeof (CertUsage_tD6645AB4AE2BF82C76670A980E82C4DC1527CA95)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2900[3] = 
{
	CertUsage_tD6645AB4AE2BF82C76670A980E82C4DC1527CA95::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2901 = { sizeof (SecurityBufferStruct_t40575D8D51B46283505616EB5FA6A4CD9713A58A)+ sizeof (RuntimeObject), sizeof(SecurityBufferStruct_t40575D8D51B46283505616EB5FA6A4CD9713A58A ), sizeof(SecurityBufferStruct_t40575D8D51B46283505616EB5FA6A4CD9713A58A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2901[4] = 
{
	SecurityBufferStruct_t40575D8D51B46283505616EB5FA6A4CD9713A58A::get_offset_of_count_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SecurityBufferStruct_t40575D8D51B46283505616EB5FA6A4CD9713A58A::get_offset_of_type_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SecurityBufferStruct_t40575D8D51B46283505616EB5FA6A4CD9713A58A::get_offset_of_token_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SecurityBufferStruct_t40575D8D51B46283505616EB5FA6A4CD9713A58A_StaticFields::get_offset_of_Size_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2902 = { sizeof (SecurityBuffer_t577E697FBA98983D3C81105A488F7940DAAD615A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2902[5] = 
{
	SecurityBuffer_t577E697FBA98983D3C81105A488F7940DAAD615A::get_offset_of_size_0(),
	SecurityBuffer_t577E697FBA98983D3C81105A488F7940DAAD615A::get_offset_of_type_1(),
	SecurityBuffer_t577E697FBA98983D3C81105A488F7940DAAD615A::get_offset_of_token_2(),
	SecurityBuffer_t577E697FBA98983D3C81105A488F7940DAAD615A::get_offset_of_unmanagedToken_3(),
	SecurityBuffer_t577E697FBA98983D3C81105A488F7940DAAD615A::get_offset_of_offset_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2903 = { sizeof (SecurityBufferDescriptor_tE3CAC792561CE08ADCA2530C44649FC052E719C8), sizeof(SecurityBufferDescriptor_tE3CAC792561CE08ADCA2530C44649FC052E719C8_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2903[3] = 
{
	SecurityBufferDescriptor_tE3CAC792561CE08ADCA2530C44649FC052E719C8::get_offset_of_Version_0(),
	SecurityBufferDescriptor_tE3CAC792561CE08ADCA2530C44649FC052E719C8::get_offset_of_Count_1(),
	SecurityBufferDescriptor_tE3CAC792561CE08ADCA2530C44649FC052E719C8::get_offset_of_UnmanagedPointer_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2904 = { sizeof (CertificateEncoding_tA8B341F3892CEDDCD4BD2D2C65177AD96668E3E6)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2904[7] = 
{
	CertificateEncoding_tA8B341F3892CEDDCD4BD2D2C65177AD96668E3E6::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2905 = { sizeof (CertificateProblem_t406FB8728C32165998C1BA55463557F33011D8E4)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2905[32] = 
{
	CertificateProblem_t406FB8728C32165998C1BA55463557F33011D8E4::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2906 = { sizeof (SecChannelBindings_tE856680A2915B8FACAC224539BF99AE2AC8DAB71), sizeof(SecChannelBindings_tE856680A2915B8FACAC224539BF99AE2AC8DAB71_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2906[8] = 
{
	SecChannelBindings_tE856680A2915B8FACAC224539BF99AE2AC8DAB71::get_offset_of_dwInitiatorAddrType_0(),
	SecChannelBindings_tE856680A2915B8FACAC224539BF99AE2AC8DAB71::get_offset_of_cbInitiatorLength_1(),
	SecChannelBindings_tE856680A2915B8FACAC224539BF99AE2AC8DAB71::get_offset_of_dwInitiatorOffset_2(),
	SecChannelBindings_tE856680A2915B8FACAC224539BF99AE2AC8DAB71::get_offset_of_dwAcceptorAddrType_3(),
	SecChannelBindings_tE856680A2915B8FACAC224539BF99AE2AC8DAB71::get_offset_of_cbAcceptorLength_4(),
	SecChannelBindings_tE856680A2915B8FACAC224539BF99AE2AC8DAB71::get_offset_of_dwAcceptorOffset_5(),
	SecChannelBindings_tE856680A2915B8FACAC224539BF99AE2AC8DAB71::get_offset_of_cbApplicationDataLength_6(),
	SecChannelBindings_tE856680A2915B8FACAC224539BF99AE2AC8DAB71::get_offset_of_dwApplicationDataOffset_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2907 = { sizeof (WebRequestPrefixElement_t78873458EC7C1DE7A0CDDD8498A7A7D5DAD27482), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2907[3] = 
{
	WebRequestPrefixElement_t78873458EC7C1DE7A0CDDD8498A7A7D5DAD27482::get_offset_of_Prefix_0(),
	WebRequestPrefixElement_t78873458EC7C1DE7A0CDDD8498A7A7D5DAD27482::get_offset_of_creator_1(),
	WebRequestPrefixElement_t78873458EC7C1DE7A0CDDD8498A7A7D5DAD27482::get_offset_of_creatorType_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2908 = { sizeof (HttpAbortDelegate_tBF5D375FB316FE054524CCF1035D90029966A637), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2909 = { sizeof (HttpKnownHeaderNames_t524E4B913B15F4D16D00C7F6EFDB5A44446189F9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2909[63] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2910 = { sizeof (HttpContinueDelegate_t38DB016AD9C4FA9F4E9B4417278FB8D0594F37AC), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2911 = { sizeof (HttpWriteMode_t4428EB73952EA86B84C7BE5034ED93F7CF49F997)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2911[6] = 
{
	HttpWriteMode_t4428EB73952EA86B84C7BE5034ED93F7CF49F997::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2912 = { sizeof (UnlockConnectionDelegate_t561D49B895EC99A61A9D4BE2F0FBF9B85595A67B), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2913 = { sizeof (HttpBehaviour_t74C1678BDAC7E0224BF99517C97D5BB41C124C0A)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2913[5] = 
{
	HttpBehaviour_t74C1678BDAC7E0224BF99517C97D5BB41C124C0A::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2914 = { sizeof (HttpProcessingResult_t8E9C0D1BFD5EF70718EBCA4D35CE11A812FD5572)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2914[4] = 
{
	HttpProcessingResult_t8E9C0D1BFD5EF70718EBCA4D35CE11A812FD5572::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2915 = { sizeof (KnownHttpVerb_tC4B46E08A385DC41D33A27F644B86A76CCA26633), -1, sizeof(KnownHttpVerb_tC4B46E08A385DC41D33A27F644B86A76CCA26633_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2915[12] = 
{
	KnownHttpVerb_tC4B46E08A385DC41D33A27F644B86A76CCA26633::get_offset_of_Name_0(),
	KnownHttpVerb_tC4B46E08A385DC41D33A27F644B86A76CCA26633::get_offset_of_RequireContentBody_1(),
	KnownHttpVerb_tC4B46E08A385DC41D33A27F644B86A76CCA26633::get_offset_of_ContentBodyNotAllowed_2(),
	KnownHttpVerb_tC4B46E08A385DC41D33A27F644B86A76CCA26633::get_offset_of_ConnectRequest_3(),
	KnownHttpVerb_tC4B46E08A385DC41D33A27F644B86A76CCA26633::get_offset_of_ExpectNoContentResponse_4(),
	KnownHttpVerb_tC4B46E08A385DC41D33A27F644B86A76CCA26633_StaticFields::get_offset_of_NamedHeaders_5(),
	KnownHttpVerb_tC4B46E08A385DC41D33A27F644B86A76CCA26633_StaticFields::get_offset_of_Get_6(),
	KnownHttpVerb_tC4B46E08A385DC41D33A27F644B86A76CCA26633_StaticFields::get_offset_of_Connect_7(),
	KnownHttpVerb_tC4B46E08A385DC41D33A27F644B86A76CCA26633_StaticFields::get_offset_of_Head_8(),
	KnownHttpVerb_tC4B46E08A385DC41D33A27F644B86A76CCA26633_StaticFields::get_offset_of_Put_9(),
	KnownHttpVerb_tC4B46E08A385DC41D33A27F644B86A76CCA26633_StaticFields::get_offset_of_Post_10(),
	KnownHttpVerb_tC4B46E08A385DC41D33A27F644B86A76CCA26633_StaticFields::get_offset_of_MkCol_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2916 = { sizeof (HttpProtocolUtils_t31E9AE5BAACF7A04CECA4AB8903A4CAE2B94ECA5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2917 = { sizeof (TriState_t1CD7C17BF90AEFF980E903EE05FFB7A3CA4A16FE)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2917[4] = 
{
	TriState_t1CD7C17BF90AEFF980E903EE05FFB7A3CA4A16FE::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2918 = { sizeof (DefaultPorts_tEADFE734E2CEFF4E0F75A9B4A73309281C47C0E6)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2918[8] = 
{
	DefaultPorts_tEADFE734E2CEFF4E0F75A9B4A73309281C47C0E6::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2919 = { sizeof (hostent_t618E4548593621ED80D901F36C0153673B95959E)+ sizeof (RuntimeObject), sizeof(hostent_t618E4548593621ED80D901F36C0153673B95959E ), 0, 0 };
extern const int32_t g_FieldOffsetTable2919[5] = 
{
	hostent_t618E4548593621ED80D901F36C0153673B95959E::get_offset_of_h_name_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	hostent_t618E4548593621ED80D901F36C0153673B95959E::get_offset_of_h_aliases_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	hostent_t618E4548593621ED80D901F36C0153673B95959E::get_offset_of_h_addrtype_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	hostent_t618E4548593621ED80D901F36C0153673B95959E::get_offset_of_h_length_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	hostent_t618E4548593621ED80D901F36C0153673B95959E::get_offset_of_h_addr_list_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2920 = { sizeof (Blob_t59A9790FE6F4E01F8B23D436D97BF1E33C06B4E9)+ sizeof (RuntimeObject), sizeof(Blob_t59A9790FE6F4E01F8B23D436D97BF1E33C06B4E9 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2920[2] = 
{
	Blob_t59A9790FE6F4E01F8B23D436D97BF1E33C06B4E9::get_offset_of_cbSize_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Blob_t59A9790FE6F4E01F8B23D436D97BF1E33C06B4E9::get_offset_of_pBlobData_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2921 = { sizeof (SplitWritesState_t7D1B22995BE24ABBE5D8C213BF991B1614789100), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2921[5] = 
{
	0,
	SplitWritesState_t7D1B22995BE24ABBE5D8C213BF991B1614789100::get_offset_of__UserBuffers_1(),
	SplitWritesState_t7D1B22995BE24ABBE5D8C213BF991B1614789100::get_offset_of__Index_2(),
	SplitWritesState_t7D1B22995BE24ABBE5D8C213BF991B1614789100::get_offset_of__LastBufferConsumed_3(),
	SplitWritesState_t7D1B22995BE24ABBE5D8C213BF991B1614789100::get_offset_of__RealBuffers_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2922 = { sizeof (NetworkAccess_tDF7B3E99D1313AA25FE2613FA9CA07818AD8A394)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2922[3] = 
{
	NetworkAccess_tDF7B3E99D1313AA25FE2613FA9CA07818AD8A394::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2923 = { sizeof (NetworkCredential_tA91C6E62EA0F0915E6E393F5DFD87D03FF2C3062), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2923[3] = 
{
	NetworkCredential_tA91C6E62EA0F0915E6E393F5DFD87D03FF2C3062::get_offset_of_m_domain_0(),
	NetworkCredential_tA91C6E62EA0F0915E6E393F5DFD87D03FF2C3062::get_offset_of_m_userName_1(),
	NetworkCredential_tA91C6E62EA0F0915E6E393F5DFD87D03FF2C3062::get_offset_of_m_password_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2924 = { sizeof (ProtocolViolationException_t287E1EFCC1BC7BB76C74A681581BF3A67C68BDFB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2925 = { sizeof (SocketAddress_tFD1A629405590229D8DAA15D03083147B767C969), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2925[8] = 
{
	0,
	0,
	SocketAddress_tFD1A629405590229D8DAA15D03083147B767C969::get_offset_of_m_Size_2(),
	SocketAddress_tFD1A629405590229D8DAA15D03083147B767C969::get_offset_of_m_Buffer_3(),
	0,
	0,
	SocketAddress_tFD1A629405590229D8DAA15D03083147B767C969::get_offset_of_m_changed_6(),
	SocketAddress_tFD1A629405590229D8DAA15D03083147B767C969::get_offset_of_m_hash_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2926 = { sizeof (TransportContext_t1753CC7BFFA637B35BE353AAE4452C4371F89A9F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2927 = { sizeof (CachedTransportContext_tEF48AE220F07023D4CF5E7A627D7C79CEFCF4A18), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2927[1] = 
{
	CachedTransportContext_tEF48AE220F07023D4CF5E7A627D7C79CEFCF4A18::get_offset_of_binding_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2928 = { sizeof (TransportType_tE05CF39764C8131A3248837320DF17C2129E0490)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2928[6] = 
{
	TransportType_tE05CF39764C8131A3248837320DF17C2129E0490::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2929 = { sizeof (WebException_tD400C9DEBEBB6AEDA77500E634D20692E27A993D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2929[3] = 
{
	WebException_tD400C9DEBEBB6AEDA77500E634D20692E27A993D::get_offset_of_m_Status_17(),
	WebException_tD400C9DEBEBB6AEDA77500E634D20692E27A993D::get_offset_of_m_Response_18(),
	WebException_tD400C9DEBEBB6AEDA77500E634D20692E27A993D::get_offset_of_m_InternalStatus_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2930 = { sizeof (WebExceptionInternalStatus_t2B50725020F5BAB7DCBE324ADF308881FEB3B64D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2930[5] = 
{
	WebExceptionInternalStatus_t2B50725020F5BAB7DCBE324ADF308881FEB3B64D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2931 = { sizeof (WebExceptionStatus_t97365CBADE462C1E2A1A0FACF18F3B111900F8DC)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2931[22] = 
{
	WebExceptionStatus_t97365CBADE462C1E2A1A0FACF18F3B111900F8DC::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2932 = { sizeof (WebExceptionMapping_t4E7EF581D0224FFC2F2C8556EF320557517A378A), -1, sizeof(WebExceptionMapping_t4E7EF581D0224FFC2F2C8556EF320557517A378A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2932[1] = 
{
	WebExceptionMapping_t4E7EF581D0224FFC2F2C8556EF320557517A378A_StaticFields::get_offset_of_s_Mapping_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2933 = { sizeof (WebHeaderCollectionType_t2994510EB856AC407AB0757A9814CDF80185A862)+ sizeof (RuntimeObject), sizeof(uint16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2933[12] = 
{
	WebHeaderCollectionType_t2994510EB856AC407AB0757A9814CDF80185A862::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2934 = { sizeof (WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304), -1, sizeof(WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2934[29] = 
{
	0,
	0,
	WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304_StaticFields::get_offset_of_HInfo_14(),
	WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304::get_offset_of_m_CommonHeaders_15(),
	WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304::get_offset_of_m_NumCommonHeaders_16(),
	WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304_StaticFields::get_offset_of_s_CommonHeaderNames_17(),
	WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304_StaticFields::get_offset_of_s_CommonHeaderHints_18(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304::get_offset_of_m_InnerCollection_37(),
	WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304::get_offset_of_m_Type_38(),
	WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304_StaticFields::get_offset_of_HttpTrimCharacters_39(),
	WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304_StaticFields::get_offset_of_RfcCharMap_40(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2935 = { sizeof (HeaderEncoding_t8DF9E931588E144C16512A8B643C07C9197B05FA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2936 = { sizeof (RfcChar_tD4173F085F19DF711D550AC6CAD7EF61939EF27F)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2936[9] = 
{
	RfcChar_tD4173F085F19DF711D550AC6CAD7EF61939EF27F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2937 = { sizeof (CaseInsensitiveAscii_tAC44F3DBCEA33C5581DCE8ADC66B0317FFA41E8B), -1, sizeof(CaseInsensitiveAscii_tAC44F3DBCEA33C5581DCE8ADC66B0317FFA41E8B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2937[2] = 
{
	CaseInsensitiveAscii_tAC44F3DBCEA33C5581DCE8ADC66B0317FFA41E8B_StaticFields::get_offset_of_StaticInstance_0(),
	CaseInsensitiveAscii_tAC44F3DBCEA33C5581DCE8ADC66B0317FFA41E8B_StaticFields::get_offset_of_AsciiToLower_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2938 = { sizeof (HostHeaderString_tBC7E5E660D162413E9357EAD3354E813E410FBB7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2938[3] = 
{
	HostHeaderString_tBC7E5E660D162413E9357EAD3354E813E410FBB7::get_offset_of_m_Converted_0(),
	HostHeaderString_tBC7E5E660D162413E9357EAD3354E813E410FBB7::get_offset_of_m_String_1(),
	HostHeaderString_tBC7E5E660D162413E9357EAD3354E813E410FBB7::get_offset_of_m_Bytes_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2939 = { sizeof (WebPermissionAttribute_t4705EE8C353B760970949AF3EB4B534E9BADE8C3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2939[2] = 
{
	WebPermissionAttribute_t4705EE8C353B760970949AF3EB4B534E9BADE8C3::get_offset_of_m_accept_2(),
	WebPermissionAttribute_t4705EE8C353B760970949AF3EB4B534E9BADE8C3::get_offset_of_m_connect_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2940 = { sizeof (DelayedRegex_t8C9BDFF6F3232C12E0AB4BCE4A0B84E70A5FC541), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2940[2] = 
{
	DelayedRegex_t8C9BDFF6F3232C12E0AB4BCE4A0B84E70A5FC541::get_offset_of__AsRegex_0(),
	DelayedRegex_t8C9BDFF6F3232C12E0AB4BCE4A0B84E70A5FC541::get_offset_of__AsString_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2941 = { sizeof (WebPermission_tFC726018222D4C825C7A1CD2B66E7E3C0326A23C), -1, sizeof(WebPermission_tFC726018222D4C825C7A1CD2B66E7E3C0326A23C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2941[7] = 
{
	WebPermission_tFC726018222D4C825C7A1CD2B66E7E3C0326A23C::get_offset_of_m_noRestriction_0(),
	WebPermission_tFC726018222D4C825C7A1CD2B66E7E3C0326A23C::get_offset_of_m_UnrestrictedConnect_1(),
	WebPermission_tFC726018222D4C825C7A1CD2B66E7E3C0326A23C::get_offset_of_m_UnrestrictedAccept_2(),
	WebPermission_tFC726018222D4C825C7A1CD2B66E7E3C0326A23C::get_offset_of_m_connectList_3(),
	WebPermission_tFC726018222D4C825C7A1CD2B66E7E3C0326A23C::get_offset_of_m_acceptList_4(),
	0,
	WebPermission_tFC726018222D4C825C7A1CD2B66E7E3C0326A23C_StaticFields::get_offset_of_s_MatchAllRegex_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2942 = { sizeof (WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8), -1, sizeof(WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2942[12] = 
{
	0,
	WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8_StaticFields::get_offset_of_s_PrefixList_2(),
	WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8_StaticFields::get_offset_of_s_InternalSyncObject_3(),
	WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8_StaticFields::get_offset_of_s_DefaultTimerQueue_4(),
	WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8::get_offset_of_m_AuthenticationLevel_5(),
	WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8::get_offset_of_m_ImpersonationLevel_6(),
	WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8::get_offset_of_m_CachePolicy_7(),
	WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8::get_offset_of_m_CacheProtocol_8(),
	WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8::get_offset_of_m_CacheBinding_9(),
	WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8_StaticFields::get_offset_of_webRequestCreate_10(),
	WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8_StaticFields::get_offset_of_s_DefaultWebProxy_11(),
	WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8_StaticFields::get_offset_of_s_DefaultWebProxyInitialized_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2943 = { sizeof (DesignerWebRequestCreate_t613DD91D4F07703DC65E847B367F4DCD5710E2A3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2944 = { sizeof (WebProxyWrapperOpaque_t6CC216364481C2A8254832AA0897F770BB494A62), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2944[1] = 
{
	WebProxyWrapperOpaque_t6CC216364481C2A8254832AA0897F770BB494A62::get_offset_of_webProxy_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2945 = { sizeof (WebProxyWrapper_t47B30DCD77853C5079F4944A6FCA329026D84E3B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2946 = { sizeof (U3CU3Ec__DisplayClass78_0_tC7AC5DA365F8D5A75E48F83C5D127EF753913861), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2946[2] = 
{
	U3CU3Ec__DisplayClass78_0_tC7AC5DA365F8D5A75E48F83C5D127EF753913861::get_offset_of_currentUser_0(),
	U3CU3Ec__DisplayClass78_0_tC7AC5DA365F8D5A75E48F83C5D127EF753913861::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2947 = { sizeof (U3CU3Ec__DisplayClass79_0_t4CA87BE9AA29F9315C6AD70F8C79A62C7727013F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2947[2] = 
{
	U3CU3Ec__DisplayClass79_0_t4CA87BE9AA29F9315C6AD70F8C79A62C7727013F::get_offset_of_currentUser_0(),
	U3CU3Ec__DisplayClass79_0_t4CA87BE9AA29F9315C6AD70F8C79A62C7727013F::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2948 = { sizeof (WebRequestMethods_tEB20991FE33A8D697EB75C8B3179C887CD5CACCF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2949 = { sizeof (Ftp_t48212CF2A906E6BCB6EB93FD8DB7A1B12A5EF5D1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2949[13] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2950 = { sizeof (Http_t2D48E64A5696B3936B4E68BC526E2BE4E0D16EA3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2950[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2951 = { sizeof (File_tFDCA2B2C65C229BDD0AE1FF8AED12010D5849647), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2951[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2952 = { sizeof (WebResponse_t5C91B5B83E2FBA2EABC6FDF2A70E9AFD9BB059BD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2952[2] = 
{
	WebResponse_t5C91B5B83E2FBA2EABC6FDF2A70E9AFD9BB059BD::get_offset_of_m_IsCacheFresh_1(),
	WebResponse_t5C91B5B83E2FBA2EABC6FDF2A70E9AFD9BB059BD::get_offset_of_m_IsFromCache_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2953 = { sizeof (WebUtility_t405BCFEA1292946A0C732330025C9D3C02AF6C1A), -1, sizeof(WebUtility_t405BCFEA1292946A0C732330025C9D3C02AF6C1A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2953[10] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	WebUtility_t405BCFEA1292946A0C732330025C9D3C02AF6C1A_StaticFields::get_offset_of__htmlEntityEndingChars_7(),
	WebUtility_t405BCFEA1292946A0C732330025C9D3C02AF6C1A_StaticFields::get_offset_of__htmlDecodeConformance_8(),
	WebUtility_t405BCFEA1292946A0C732330025C9D3C02AF6C1A_StaticFields::get_offset_of__htmlEncodeConformance_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2954 = { sizeof (UrlDecoder_t9C62102EA32FB43498BED35B0526E09C666C83FC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2954[6] = 
{
	UrlDecoder_t9C62102EA32FB43498BED35B0526E09C666C83FC::get_offset_of__bufferSize_0(),
	UrlDecoder_t9C62102EA32FB43498BED35B0526E09C666C83FC::get_offset_of__numChars_1(),
	UrlDecoder_t9C62102EA32FB43498BED35B0526E09C666C83FC::get_offset_of__charBuffer_2(),
	UrlDecoder_t9C62102EA32FB43498BED35B0526E09C666C83FC::get_offset_of__numBytes_3(),
	UrlDecoder_t9C62102EA32FB43498BED35B0526E09C666C83FC::get_offset_of__byteBuffer_4(),
	UrlDecoder_t9C62102EA32FB43498BED35B0526E09C666C83FC::get_offset_of__encoding_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2955 = { sizeof (HtmlEntities_t8BEA66BA49047ECD8960950145CA6259D95B2F79), -1, sizeof(HtmlEntities_t8BEA66BA49047ECD8960950145CA6259D95B2F79_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2955[2] = 
{
	HtmlEntities_t8BEA66BA49047ECD8960950145CA6259D95B2F79_StaticFields::get_offset_of_entities_0(),
	HtmlEntities_t8BEA66BA49047ECD8960950145CA6259D95B2F79_StaticFields::get_offset_of_entities_values_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2956 = { sizeof (WriteStreamClosedEventArgs_t1E55EEFF430B18CCEF826ED9078E8D7BB6783869), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2957 = { sizeof (WriteStreamClosedEventHandler_t1FADB2B0A561617F95C30007B786BF5F6FB71B94), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2958 = { sizeof (BufferOffsetSize_t167DA604FC5C1B4C7784C746315EABFE6D40F317), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2958[3] = 
{
	BufferOffsetSize_t167DA604FC5C1B4C7784C746315EABFE6D40F317::get_offset_of_Buffer_0(),
	BufferOffsetSize_t167DA604FC5C1B4C7784C746315EABFE6D40F317::get_offset_of_Offset_1(),
	BufferOffsetSize_t167DA604FC5C1B4C7784C746315EABFE6D40F317::get_offset_of_Size_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2959 = { sizeof (DataParseStatus_tE7B5667E06BC732DC9F73853C4B7B001D74C63FC)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2959[6] = 
{
	DataParseStatus_tE7B5667E06BC732DC9F73853C4B7B001D74C63FC::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2960 = { sizeof (WriteBufferState_t6870E49BDF9175CC064514AC1F0C9CD7784E22CC)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2960[5] = 
{
	WriteBufferState_t6870E49BDF9175CC064514AC1F0C9CD7784E22CC::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2961 = { sizeof (WebParseErrorSection_t97E38DEF05BB39EFF8D4703ECB7DD74C30072219)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2961[5] = 
{
	WebParseErrorSection_t97E38DEF05BB39EFF8D4703ECB7DD74C30072219::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2962 = { sizeof (WebParseErrorCode_t82D6E594C83923E1C0FB47D4B0EE90EDA0AA22CC)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2962[8] = 
{
	WebParseErrorCode_t82D6E594C83923E1C0FB47D4B0EE90EDA0AA22CC::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2963 = { sizeof (WebParseError_tC5C6CFEAE6CEB2F0FA842D77D1F2DDD2C65865E3)+ sizeof (RuntimeObject), sizeof(WebParseError_tC5C6CFEAE6CEB2F0FA842D77D1F2DDD2C65865E3 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2963[2] = 
{
	WebParseError_tC5C6CFEAE6CEB2F0FA842D77D1F2DDD2C65865E3::get_offset_of_Section_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WebParseError_tC5C6CFEAE6CEB2F0FA842D77D1F2DDD2C65865E3::get_offset_of_Code_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2964 = { sizeof (HttpDateParse_t4A1233E61431D860AD7B37695E231713DD89D524), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2964[41] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2965 = { sizeof (HeaderParser_t6B59FF0FD79FFD511A019AE5383DCEF641BA822E), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2966 = { sizeof (HeaderInfo_t08A38618F1A42BEE66373512B83B804DAEAF2EB8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2966[5] = 
{
	HeaderInfo_t08A38618F1A42BEE66373512B83B804DAEAF2EB8::get_offset_of_IsRequestRestricted_0(),
	HeaderInfo_t08A38618F1A42BEE66373512B83B804DAEAF2EB8::get_offset_of_IsResponseRestricted_1(),
	HeaderInfo_t08A38618F1A42BEE66373512B83B804DAEAF2EB8::get_offset_of_Parser_2(),
	HeaderInfo_t08A38618F1A42BEE66373512B83B804DAEAF2EB8::get_offset_of_HeaderName_3(),
	HeaderInfo_t08A38618F1A42BEE66373512B83B804DAEAF2EB8::get_offset_of_AllowMultiValues_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2967 = { sizeof (HeaderInfoTable_t16B4CA77715B871579C8DE60EBD92E8EDD332BAF), -1, sizeof(HeaderInfoTable_t16B4CA77715B871579C8DE60EBD92E8EDD332BAF_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2967[4] = 
{
	HeaderInfoTable_t16B4CA77715B871579C8DE60EBD92E8EDD332BAF_StaticFields::get_offset_of_HeaderHashTable_0(),
	HeaderInfoTable_t16B4CA77715B871579C8DE60EBD92E8EDD332BAF_StaticFields::get_offset_of_UnknownHeaderInfo_1(),
	HeaderInfoTable_t16B4CA77715B871579C8DE60EBD92E8EDD332BAF_StaticFields::get_offset_of_SingleParser_2(),
	HeaderInfoTable_t16B4CA77715B871579C8DE60EBD92E8EDD332BAF_StaticFields::get_offset_of_MultiParser_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2968 = { sizeof (CloseExState_t7AD30E3EACEBBAF7661B1AC45F7BC018DA33E429)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2968[4] = 
{
	CloseExState_t7AD30E3EACEBBAF7661B1AC45F7BC018DA33E429::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2969 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2970 = { sizeof (LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3), -1, 0, sizeof(LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3_ThreadStaticFields) };
extern const int32_t g_FieldOffsetTable2970[12] = 
{
	0,
	0,
	LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3_ThreadStaticFields::get_offset_of_t_ThreadContext_2() | THREAD_LOCAL_STATIC_MASK,
	LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3::get_offset_of_m_AsyncObject_3(),
	LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3::get_offset_of_m_AsyncState_4(),
	LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3::get_offset_of_m_AsyncCallback_5(),
	LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3::get_offset_of_m_Result_6(),
	LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3::get_offset_of_m_ErrorCode_7(),
	LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3::get_offset_of_m_IntCompleted_8(),
	LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3::get_offset_of_m_EndCalled_9(),
	LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3::get_offset_of_m_UserEvent_10(),
	LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3::get_offset_of_m_Event_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2971 = { sizeof (ThreadContext_tCC2E1DE0DDF550CCA67AE22CDF6F1AD426DC9082), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2971[1] = 
{
	ThreadContext_tCC2E1DE0DDF550CCA67AE22CDF6F1AD426DC9082::get_offset_of_m_NestedIOCount_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2972 = { sizeof (BaseLoggingObject_tE425700C69CB9F232260567801E833D111633A4A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2973 = { sizeof (ThreadKinds_t17B8CB7BF8789F46FD497AC5EDE85173280ADC20)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2973[16] = 
{
	ThreadKinds_t17B8CB7BF8789F46FD497AC5EDE85173280ADC20::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2974 = { sizeof (GlobalLog_tA300083DFE1CA3CDE1223FE5A71452FB24EF4AFD), -1, sizeof(GlobalLog_tA300083DFE1CA3CDE1223FE5A71452FB24EF4AFD_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2974[1] = 
{
	GlobalLog_tA300083DFE1CA3CDE1223FE5A71452FB24EF4AFD_StaticFields::get_offset_of_Logobject_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2975 = { sizeof (NetRes_tB18DF1FAF98D8D7505D72FA149E57F0D31E2653B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2976 = { sizeof (PooledStream_tA0A798FD29060971887DF12D77E4BB444BE34D1B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2976[14] = 
{
	PooledStream_tA0A798FD29060971887DF12D77E4BB444BE34D1B::get_offset_of_m_CheckLifetime_5(),
	PooledStream_tA0A798FD29060971887DF12D77E4BB444BE34D1B::get_offset_of_m_Lifetime_6(),
	PooledStream_tA0A798FD29060971887DF12D77E4BB444BE34D1B::get_offset_of_m_CreateTime_7(),
	PooledStream_tA0A798FD29060971887DF12D77E4BB444BE34D1B::get_offset_of_m_ConnectionIsDoomed_8(),
	PooledStream_tA0A798FD29060971887DF12D77E4BB444BE34D1B::get_offset_of_m_ConnectionPool_9(),
	PooledStream_tA0A798FD29060971887DF12D77E4BB444BE34D1B::get_offset_of_m_Owner_10(),
	PooledStream_tA0A798FD29060971887DF12D77E4BB444BE34D1B::get_offset_of_m_PooledCount_11(),
	PooledStream_tA0A798FD29060971887DF12D77E4BB444BE34D1B::get_offset_of_m_Initalizing_12(),
	PooledStream_tA0A798FD29060971887DF12D77E4BB444BE34D1B::get_offset_of_m_ServerAddress_13(),
	PooledStream_tA0A798FD29060971887DF12D77E4BB444BE34D1B::get_offset_of_m_NetworkStream_14(),
	PooledStream_tA0A798FD29060971887DF12D77E4BB444BE34D1B::get_offset_of_m_AbortSocket_15(),
	PooledStream_tA0A798FD29060971887DF12D77E4BB444BE34D1B::get_offset_of_m_AbortSocket6_16(),
	PooledStream_tA0A798FD29060971887DF12D77E4BB444BE34D1B::get_offset_of_m_JustConnected_17(),
	PooledStream_tA0A798FD29060971887DF12D77E4BB444BE34D1B::get_offset_of_m_AsyncCallback_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2977 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2978 = { sizeof (ProxyChain_t58CD16408B22DC2B56772ADD1414604ABEF5AA53), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2978[5] = 
{
	ProxyChain_t58CD16408B22DC2B56772ADD1414604ABEF5AA53::get_offset_of_m_Cache_0(),
	ProxyChain_t58CD16408B22DC2B56772ADD1414604ABEF5AA53::get_offset_of_m_CacheComplete_1(),
	ProxyChain_t58CD16408B22DC2B56772ADD1414604ABEF5AA53::get_offset_of_m_MainEnumerator_2(),
	ProxyChain_t58CD16408B22DC2B56772ADD1414604ABEF5AA53::get_offset_of_m_Destination_3(),
	ProxyChain_t58CD16408B22DC2B56772ADD1414604ABEF5AA53::get_offset_of_m_HttpAbortDelegate_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2979 = { sizeof (ProxyEnumerator_t92F0DA133B067B6E5BA335083CDE845D610B7016), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2979[4] = 
{
	ProxyEnumerator_t92F0DA133B067B6E5BA335083CDE845D610B7016::get_offset_of_m_Chain_0(),
	ProxyEnumerator_t92F0DA133B067B6E5BA335083CDE845D610B7016::get_offset_of_m_Finished_1(),
	ProxyEnumerator_t92F0DA133B067B6E5BA335083CDE845D610B7016::get_offset_of_m_CurrentIndex_2(),
	ProxyEnumerator_t92F0DA133B067B6E5BA335083CDE845D610B7016::get_offset_of_m_TriedDirect_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2980 = { sizeof (ProxyScriptChain_t3016818D00BD2D8517CBF113249542FE4CE2EF20), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2980[4] = 
{
	ProxyScriptChain_t3016818D00BD2D8517CBF113249542FE4CE2EF20::get_offset_of_m_Proxy_5(),
	ProxyScriptChain_t3016818D00BD2D8517CBF113249542FE4CE2EF20::get_offset_of_m_ScriptProxies_6(),
	ProxyScriptChain_t3016818D00BD2D8517CBF113249542FE4CE2EF20::get_offset_of_m_CurrentIndex_7(),
	ProxyScriptChain_t3016818D00BD2D8517CBF113249542FE4CE2EF20::get_offset_of_m_SyncStatus_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2981 = { sizeof (DirectProxy_t04EFD8672502A0DC9EC819DA854421367D6B4B2D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2981[1] = 
{
	DirectProxy_t04EFD8672502A0DC9EC819DA854421367D6B4B2D::get_offset_of_m_ProxyRetrieved_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2982 = { sizeof (StaticProxy_t5FDF2D06DD4DC75D5EEE6E7BC060A9A30FAF3711), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2982[1] = 
{
	StaticProxy_t5FDF2D06DD4DC75D5EEE6E7BC060A9A30FAF3711::get_offset_of_m_Proxy_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2983 = { sizeof (ScatterGatherBuffers_t5090EA13155952525395C6141ACA51BDBE255E6B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2983[5] = 
{
	ScatterGatherBuffers_t5090EA13155952525395C6141ACA51BDBE255E6B::get_offset_of_headChunk_0(),
	ScatterGatherBuffers_t5090EA13155952525395C6141ACA51BDBE255E6B::get_offset_of_currentChunk_1(),
	ScatterGatherBuffers_t5090EA13155952525395C6141ACA51BDBE255E6B::get_offset_of_nextChunkLength_2(),
	ScatterGatherBuffers_t5090EA13155952525395C6141ACA51BDBE255E6B::get_offset_of_totalLength_3(),
	ScatterGatherBuffers_t5090EA13155952525395C6141ACA51BDBE255E6B::get_offset_of_chunkCount_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2984 = { sizeof (MemoryChunk_t2B4F05956C1526847FF22066DC4E91A9A297951F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2984[3] = 
{
	MemoryChunk_t2B4F05956C1526847FF22066DC4E91A9A297951F::get_offset_of_Buffer_0(),
	MemoryChunk_t2B4F05956C1526847FF22066DC4E91A9A297951F::get_offset_of_FreeOffset_1(),
	MemoryChunk_t2B4F05956C1526847FF22066DC4E91A9A297951F::get_offset_of_Next_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2985 = { sizeof (Semaphore_t2C8E706636BE60950385992A48222B63F4165885), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2986 = { sizeof (ServiceNameStore_tF45F4346CE113F34D3E08E515FD32642512CEA37), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2986[2] = 
{
	ServiceNameStore_tF45F4346CE113F34D3E08E515FD32642512CEA37::get_offset_of_serviceNames_0(),
	ServiceNameStore_tF45F4346CE113F34D3E08E515FD32642512CEA37::get_offset_of_serviceNameCollection_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2987 = { sizeof (TimerThread_t834F44C51FF3F25350F8B4E03670F941F352DF01), -1, sizeof(TimerThread_t834F44C51FF3F25350F8B4E03670F941F352DF01_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2987[11] = 
{
	0,
	0,
	0,
	TimerThread_t834F44C51FF3F25350F8B4E03670F941F352DF01_StaticFields::get_offset_of_s_Queues_3(),
	TimerThread_t834F44C51FF3F25350F8B4E03670F941F352DF01_StaticFields::get_offset_of_s_NewQueues_4(),
	TimerThread_t834F44C51FF3F25350F8B4E03670F941F352DF01_StaticFields::get_offset_of_s_ThreadState_5(),
	TimerThread_t834F44C51FF3F25350F8B4E03670F941F352DF01_StaticFields::get_offset_of_s_ThreadReadyEvent_6(),
	TimerThread_t834F44C51FF3F25350F8B4E03670F941F352DF01_StaticFields::get_offset_of_s_ThreadShutdownEvent_7(),
	TimerThread_t834F44C51FF3F25350F8B4E03670F941F352DF01_StaticFields::get_offset_of_s_ThreadEvents_8(),
	TimerThread_t834F44C51FF3F25350F8B4E03670F941F352DF01_StaticFields::get_offset_of_s_CacheScanIteration_9(),
	TimerThread_t834F44C51FF3F25350F8B4E03670F941F352DF01_StaticFields::get_offset_of_s_QueuesCache_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2988 = { sizeof (Queue_tCCFF6A2FCF584216AEDA04A483FB808E2D493643), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2988[1] = 
{
	Queue_tCCFF6A2FCF584216AEDA04A483FB808E2D493643::get_offset_of_m_DurationMilliseconds_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2989 = { sizeof (Timer_t3B21B1013E27B5DC9FED14EC0921A5F51230D46F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2989[2] = 
{
	Timer_t3B21B1013E27B5DC9FED14EC0921A5F51230D46F::get_offset_of_m_StartTimeMilliseconds_0(),
	Timer_t3B21B1013E27B5DC9FED14EC0921A5F51230D46F::get_offset_of_m_DurationMilliseconds_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2990 = { sizeof (Callback_t8DF3FD84AB632709C486978BE28ED721EB3A40E3), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2991 = { sizeof (TimerThreadState_t47E2D1C00D1FBAACE6D01C4A9449E06D50A374F8)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2991[4] = 
{
	TimerThreadState_t47E2D1C00D1FBAACE6D01C4A9449E06D50A374F8::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2992 = { sizeof (TimerQueue_t8C40E5540B8DCC1AF23C12BC62F6D1D8061F754C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2992[2] = 
{
	TimerQueue_t8C40E5540B8DCC1AF23C12BC62F6D1D8061F754C::get_offset_of_m_ThisHandle_1(),
	TimerQueue_t8C40E5540B8DCC1AF23C12BC62F6D1D8061F754C::get_offset_of_m_Timers_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2993 = { sizeof (InfiniteTimerQueue_t141BA98635EDB34E2BAAFE8BA5C91795E7CCAB51), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2994 = { sizeof (TimerNode_t5596C0BAF3E85D0C2444ECA74917630FDEF5087B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2994[6] = 
{
	TimerNode_t5596C0BAF3E85D0C2444ECA74917630FDEF5087B::get_offset_of_m_TimerState_2(),
	TimerNode_t5596C0BAF3E85D0C2444ECA74917630FDEF5087B::get_offset_of_m_Callback_3(),
	TimerNode_t5596C0BAF3E85D0C2444ECA74917630FDEF5087B::get_offset_of_m_Context_4(),
	TimerNode_t5596C0BAF3E85D0C2444ECA74917630FDEF5087B::get_offset_of_m_QueueLock_5(),
	TimerNode_t5596C0BAF3E85D0C2444ECA74917630FDEF5087B::get_offset_of_next_6(),
	TimerNode_t5596C0BAF3E85D0C2444ECA74917630FDEF5087B::get_offset_of_prev_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2995 = { sizeof (TimerState_tD555FD971FFAFF7BE6F94885EC160D206354BD35)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2995[5] = 
{
	TimerState_tD555FD971FFAFF7BE6F94885EC160D206354BD35::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2996 = { sizeof (InfiniteTimer_t2DCA24E378A3B09EF224C9DCDC84EAA4B3CE1CC8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2996[1] = 
{
	InfiniteTimer_t2DCA24E378A3B09EF224C9DCDC84EAA4B3CE1CC8::get_offset_of_cancelled_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2997 = { sizeof (WebProxyDataBuilder_t8FB484D8DDC07224E75F151C09E491B702C1A75A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2997[5] = 
{
	0,
	0,
	0,
	WebProxyDataBuilder_t8FB484D8DDC07224E75F151C09E491B702C1A75A::get_offset_of_m_Result_3(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2998 = { sizeof (EmptyWebProxy_tF6CEF11A280246455534D46AD1710271B8BEE22D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2998[1] = 
{
	EmptyWebProxy_tF6CEF11A280246455534D46AD1710271B8BEE22D::get_offset_of_m_credentials_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2999 = { sizeof (GeneralAsyncDelegate_tD535232B4C5C7E31A01C18A2A072F80C1C085D92), sizeof(Il2CppMethodPointer), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
