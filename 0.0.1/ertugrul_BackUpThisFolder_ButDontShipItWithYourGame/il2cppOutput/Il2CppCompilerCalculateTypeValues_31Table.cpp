﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Mono.Net.Security.MonoTlsStream
struct MonoTlsStream_t15DF42240B3214CA6A4A8FD8671C173B572DF171;
// Mono.Security.Interface.MonoTlsProvider
struct MonoTlsProvider_tDCD056C5BBBE59ED6BAF63F25952B406C1143C27;
// Mono.Security.Interface.MonoTlsSettings
struct MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF;
// System.Action`1<System.IO.Stream>
struct Action_1_tC8BAB6C7B8E5508F10B3A5EF475B0FFAE7688621;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.ArrayList
struct ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4;
// System.Collections.Generic.Dictionary`2<System.Int32,System.String>
struct Dictionary_2_t4EFE6A1D6502662B911688316C6920444A18CF0C;
// System.Collections.Generic.Dictionary`2<System.Net.HttpConnection,System.Net.HttpConnection>
struct Dictionary_2_tE9B05628397C4CAFDAD92B83F0A416E933524B04;
// System.Collections.Generic.Dictionary`2<System.String,System.Net.WebConnectionGroup>
struct Dictionary_2_t4CAF579D576CCEDF0310DD80EFB19ACBE04267D8;
// System.Collections.Generic.Dictionary`2<System.Threading.Thread,System.Diagnostics.StackTrace>
struct Dictionary_2_t2B4A575938F12185D62CE7B381FC75D09F004B17;
// System.Collections.Generic.IList`1<System.ArraySegment`1<System.Byte>>
struct IList_1_t15A182E73791E22311F72DAC1B68F0D055C32E3A;
// System.Collections.Generic.LinkedList`1<System.Net.WebConnectionGroup/ConnectionState>
struct LinkedList_1_t0513C063019CC3F3A13FA4E2C35EAAE500C6CF8F;
// System.Collections.Generic.List`1<System.Security.Claims.Claim>
struct List_1_t923CD82D31ECB841C5A8A88828B38C59AA19786D;
// System.Collections.Generic.List`1<System.String>
struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3;
// System.Collections.Generic.List`1<System.Threading.Thread>
struct List_1_t584FFF54C798BF7768C5EA483641BADEC0CB963D;
// System.Collections.Hashtable
struct Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.Collections.ObjectModel.Collection`1<System.Collections.Generic.IEnumerable`1<System.Security.Claims.Claim>>
struct Collection_1_t9E2CBF3D821CAC8AE4D9F7E0542D5BF84F47191F;
// System.Collections.Queue
struct Queue_tEC6DE7527799C2E4224B469ECD0CDD2B25E8E4F3;
// System.Collections.Specialized.HybridDictionary
struct HybridDictionary_t885F953154C575D3408650DCE5D0B76F1EE4E549;
// System.Collections.Specialized.NameValueCollection
struct NameValueCollection_t7C7CED43E4C6E997E3C8012F1D2CC4027FAD10D1;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.EventHandler
struct EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C;
// System.EventHandler`1<System.Net.Sockets.SocketAsyncEventArgs>
struct EventHandler_1_tFBF7D1676A66F950822630E5E6DA5D6FFEAF7A34;
// System.Exception
struct Exception_t;
// System.Func`2<System.Net.SimpleAsyncResult,System.Boolean>
struct Func_2_tF6A6FE235E53230F712003180A1DBAF19C50FC61;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.IO.MemoryStream
struct MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C;
// System.IO.Stream
struct Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7;
// System.IO.Stream/ReadWriteTask
struct ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80;
// System.IO.StreamReader
struct StreamReader_t62E68063760DCD2FC036AE132DE69C24B7ED001E;
// System.IOAsyncCallback
struct IOAsyncCallback_tEAEB626A6DAC959F4C365B12136A80EE9AA17547;
// System.IOSelectorJob
struct IOSelectorJob_t2B03604D19B81660C4C1C06590C76BC8630DDE99;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.Net.AuthenticationSchemeSelector
struct AuthenticationSchemeSelector_t77F5166A2AFED823020E020C95E23973E5A86F74;
// System.Net.BindIPEndPoint
struct BindIPEndPoint_t6B179B1AD32AF233C8C8E6440DFEF78153A851B9;
// System.Net.Cache.RequestCacheBinding
struct RequestCacheBinding_tB84D71781C4BCEF43DEBC72165283C4543BA4724;
// System.Net.Cache.RequestCachePolicy
struct RequestCachePolicy_t30D7352C7E9D49EEADD492A70EC92C118D90CD61;
// System.Net.Cache.RequestCacheProtocol
struct RequestCacheProtocol_t51DE21412EAD66CAD600D3A6940942920340D35D;
// System.Net.CookieCollection
struct CookieCollection_t69ADF0ABD99419E54AB4740B341D94F443D995A3;
// System.Net.CookieContainer
struct CookieContainer_t7E062D04BAED9F3B30DDEC14B09660BB506A2A73;
// System.Net.EndPoint
struct EndPoint_tD87FCEF2780A951E8CE8D808C345FBF2C088D980;
// System.Net.EndPointListener
struct EndPointListener_tF1D36E022EAFCD075CD0398D2868363AA9FB10C2;
// System.Net.FtpAsyncResult
struct FtpAsyncResult_tB318D495766A9449055B1D8C8C801095CF2CDEA3;
// System.Net.FtpWebRequest
struct FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA;
// System.Net.FtpWebResponse
struct FtpWebResponse_t8775110950F0637C1A8A495892122865F05FC205;
// System.Net.HttpConnection
struct HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E;
// System.Net.HttpContinueDelegate
struct HttpContinueDelegate_t38DB016AD9C4FA9F4E9B4417278FB8D0594F37AC;
// System.Net.HttpListener
struct HttpListener_tAAE9DCF34EB3C09285BF833F390C8CC61C7D247E;
// System.Net.HttpListener/ExtendedProtectionSelector
struct ExtendedProtectionSelector_tC8A40F0217D1CF85224BBF5F398A081FA11C692C;
// System.Net.HttpListenerContext
struct HttpListenerContext_t438DA3609B8D775884622DF392BD7D72C651FCFA;
// System.Net.HttpListenerPrefixCollection
struct HttpListenerPrefixCollection_tCFDDEB0773E99D9DA834AB2FBF1AF0F2B1AC7B5A;
// System.Net.HttpListenerRequest
struct HttpListenerRequest_t4BD625F3BF4726345B12F4FF06DB23A5932FD8CE;
// System.Net.HttpListenerRequest/GCCDelegate
struct GCCDelegate_t2ED11BA19D14B2D5E74288CA763708D5E244EB46;
// System.Net.HttpListenerResponse
struct HttpListenerResponse_t34AC9D467928315CC4285B8A14517C6D5564224C;
// System.Net.HttpWebRequest
struct HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0;
// System.Net.HttpWebResponse
struct HttpWebResponse_t34CF6A40A4748A0F8694FEFEA3723D9AE3EF3951;
// System.Net.IAuthenticationModule
struct IAuthenticationModule_t1E0AD2E546CB748B20358E5CAA041AF3443CDA30;
// System.Net.ICertificatePolicy
struct ICertificatePolicy_tC17FB628273BC68D64CCF0F0F64D4290B084434E;
// System.Net.ICredentials
struct ICredentials_t1A41F1096B037CAB53AE01434DF0747881455344;
// System.Net.IPAddress[]
struct IPAddressU5BU5D_t7F25C4C038C43BFDA8EA84969112E82BADC38BC3;
// System.Net.IPEndPoint
struct IPEndPoint_tCD29981135F7B1989C3845BF455AD44EBC13DE3F;
// System.Net.IPHostEntry
struct IPHostEntry_tB00EABDF75DB19AEAD4F3E7D93BFD7BAE558149D;
// System.Net.IWebConnectionState
struct IWebConnectionState_tD5FA067BE4DD93CFA1B64DBFA5648893D9398710;
// System.Net.IWebProxy
struct IWebProxy_tA24C0862A1ACA35D20FD079E2672CA5786C1A67E;
// System.Net.ListenerPrefix
struct ListenerPrefix_t7E983ACD2D31FE4DC42DE02AE82EC24734DC57D7;
// System.Net.MonoChunkStream
struct MonoChunkStream_t33C2B7ECB208D77D1C94F0A9F48EB5AE27DF5AB5;
// System.Net.NetworkCredential
struct NetworkCredential_tA91C6E62EA0F0915E6E393F5DFD87D03FF2C3062;
// System.Net.RequestStream
struct RequestStream_t4BF6FD18AB7261094F557E867606B91FECF74D35;
// System.Net.ResponseStream
struct ResponseStream_tE537A0E20974BEB6629CBEBE42C27C9B97126032;
// System.Net.Security.SslStream
struct SslStream_t9CEE8F6E125C734DD807D9289C86860FFEE81087;
// System.Net.ServerCertValidationCallback
struct ServerCertValidationCallback_t431E949AECAE20901007813737F5B26311F5F9FB;
// System.Net.ServiceNameStore
struct ServiceNameStore_tF45F4346CE113F34D3E08E515FD32642512CEA37;
// System.Net.ServicePoint
struct ServicePoint_t5F42B1A9D56E09B4B051BE0968C81DE3128E3EB4;
// System.Net.SimpleAsyncCallback
struct SimpleAsyncCallback_t690665AFDCBDEBA379B6F4CD213CB4BB8570F83F;
// System.Net.SimpleAsyncResult
struct SimpleAsyncResult_tA572851810F8E279EE9E5378A6D9A538B1822FC6;
// System.Net.Sockets.NetworkStream
struct NetworkStream_t362D0CD0C74C2F5CBD02905C9422E4240872ADCA;
// System.Net.Sockets.SafeSocketHandle
struct SafeSocketHandle_t9A33B4DCE2012075A5D6D355D323A05E7F16329A;
// System.Net.Sockets.Socket
struct Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8;
// System.Net.Sockets.SocketAsyncResult
struct SocketAsyncResult_t63145D172556590482549D41328C0668E19CB69C;
// System.Net.TimerThread/Queue
struct Queue_tCCFF6A2FCF584216AEDA04A483FB808E2D493643;
// System.Net.WebAsyncResult
struct WebAsyncResult_tF700444B9ABA86C7CADBFA7B99DEC52D9FBD87EE;
// System.Net.WebConnection
struct WebConnection_tEB76AEE17361D28CBAD4033026A71DA89289C243;
// System.Net.WebConnection/AbortHelper
struct AbortHelper_t0DB9458211F015848382C4B5A007AC4947411E81;
// System.Net.WebConnectionData
struct WebConnectionData_tC9286455629F1E9E2BA0CA8AB6958DF931299CCC;
// System.Net.WebConnectionGroup
struct WebConnectionGroup_tBEAB5ED1DE321C0981F5CBABA020970C3D63E95F;
// System.Net.WebConnectionStream
struct WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC;
// System.Net.WebHeaderCollection
struct WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304;
// System.Net.WebRequest/DesignerWebRequestCreate
struct DesignerWebRequestCreate_t613DD91D4F07703DC65E847B367F4DCD5710E2A3;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.Security.Authentication.ExtendedProtection.ExtendedProtectionPolicy
struct ExtendedProtectionPolicy_t5DB5E76D5F3E2BF3CAE465DF85198A91DF30A8BC;
// System.Security.Cryptography.X509Certificates.X509Certificate
struct X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2;
// System.Security.Cryptography.X509Certificates.X509Certificate2
struct X509Certificate2_tC1C49EB4CFD571C2FFDE940C24BC69651A058F73;
// System.Security.Cryptography.X509Certificates.X509CertificateCollection
struct X509CertificateCollection_t824A6C58D0D1B4A7CAE30F26CE8EE4B23A8A1833;
// System.Security.Principal.IPrincipal
struct IPrincipal_t63FD7F58FBBE134C8FE4D31710AAEA00B000F0BF;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Text.Encoding
struct Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.Threading.ManualResetEvent
struct ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408;
// System.Threading.SemaphoreSlim
struct SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048;
// System.Threading.Timer
struct Timer_t67FAB8E41573B4FA09CA56AE30725AF4297C2553;
// System.Threading.WaitCallback
struct WaitCallback_t61C5F053CAC7A7FE923208EFA060693D7997B4EC;
// System.UInt16[]
struct UInt16U5BU5D_t2D4BB1F8C486FF4359FFA7E4A76A8708A684543E;
// System.Uri
struct Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E;
// System.Version
struct Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;

struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#define ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifndef EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#define EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventArgs
struct  EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E  : public RuntimeObject
{
public:

public:
};

struct EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields, ___Empty_0)); }
	inline EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef IOASYNCRESULT_TB02ABC485035B18A731F1B61FB27EE17F4B792AD_H
#define IOASYNCRESULT_TB02ABC485035B18A731F1B61FB27EE17F4B792AD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IOAsyncResult
struct  IOAsyncResult_tB02ABC485035B18A731F1B61FB27EE17F4B792AD  : public RuntimeObject
{
public:
	// System.AsyncCallback System.IOAsyncResult::async_callback
	AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___async_callback_0;
	// System.Object System.IOAsyncResult::async_state
	RuntimeObject * ___async_state_1;
	// System.Threading.ManualResetEvent System.IOAsyncResult::wait_handle
	ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * ___wait_handle_2;
	// System.Boolean System.IOAsyncResult::completed_synchronously
	bool ___completed_synchronously_3;
	// System.Boolean System.IOAsyncResult::completed
	bool ___completed_4;

public:
	inline static int32_t get_offset_of_async_callback_0() { return static_cast<int32_t>(offsetof(IOAsyncResult_tB02ABC485035B18A731F1B61FB27EE17F4B792AD, ___async_callback_0)); }
	inline AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * get_async_callback_0() const { return ___async_callback_0; }
	inline AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 ** get_address_of_async_callback_0() { return &___async_callback_0; }
	inline void set_async_callback_0(AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * value)
	{
		___async_callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___async_callback_0), value);
	}

	inline static int32_t get_offset_of_async_state_1() { return static_cast<int32_t>(offsetof(IOAsyncResult_tB02ABC485035B18A731F1B61FB27EE17F4B792AD, ___async_state_1)); }
	inline RuntimeObject * get_async_state_1() const { return ___async_state_1; }
	inline RuntimeObject ** get_address_of_async_state_1() { return &___async_state_1; }
	inline void set_async_state_1(RuntimeObject * value)
	{
		___async_state_1 = value;
		Il2CppCodeGenWriteBarrier((&___async_state_1), value);
	}

	inline static int32_t get_offset_of_wait_handle_2() { return static_cast<int32_t>(offsetof(IOAsyncResult_tB02ABC485035B18A731F1B61FB27EE17F4B792AD, ___wait_handle_2)); }
	inline ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * get_wait_handle_2() const { return ___wait_handle_2; }
	inline ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 ** get_address_of_wait_handle_2() { return &___wait_handle_2; }
	inline void set_wait_handle_2(ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * value)
	{
		___wait_handle_2 = value;
		Il2CppCodeGenWriteBarrier((&___wait_handle_2), value);
	}

	inline static int32_t get_offset_of_completed_synchronously_3() { return static_cast<int32_t>(offsetof(IOAsyncResult_tB02ABC485035B18A731F1B61FB27EE17F4B792AD, ___completed_synchronously_3)); }
	inline bool get_completed_synchronously_3() const { return ___completed_synchronously_3; }
	inline bool* get_address_of_completed_synchronously_3() { return &___completed_synchronously_3; }
	inline void set_completed_synchronously_3(bool value)
	{
		___completed_synchronously_3 = value;
	}

	inline static int32_t get_offset_of_completed_4() { return static_cast<int32_t>(offsetof(IOAsyncResult_tB02ABC485035B18A731F1B61FB27EE17F4B792AD, ___completed_4)); }
	inline bool get_completed_4() const { return ___completed_4; }
	inline bool* get_address_of_completed_4() { return &___completed_4; }
	inline void set_completed_4(bool value)
	{
		___completed_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.IOAsyncResult
struct IOAsyncResult_tB02ABC485035B18A731F1B61FB27EE17F4B792AD_marshaled_pinvoke
{
	Il2CppMethodPointer ___async_callback_0;
	Il2CppIUnknown* ___async_state_1;
	ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * ___wait_handle_2;
	int32_t ___completed_synchronously_3;
	int32_t ___completed_4;
};
// Native definition for COM marshalling of System.IOAsyncResult
struct IOAsyncResult_tB02ABC485035B18A731F1B61FB27EE17F4B792AD_marshaled_com
{
	Il2CppMethodPointer ___async_callback_0;
	Il2CppIUnknown* ___async_state_1;
	ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * ___wait_handle_2;
	int32_t ___completed_synchronously_3;
	int32_t ___completed_4;
};
#endif // IOASYNCRESULT_TB02ABC485035B18A731F1B61FB27EE17F4B792AD_H
#ifndef MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#define MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MarshalByRefObject
struct  MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF  : public RuntimeObject
{
public:
	// System.Object System.MarshalByRefObject::_identity
	RuntimeObject * ____identity_0;

public:
	inline static int32_t get_offset_of__identity_0() { return static_cast<int32_t>(offsetof(MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF, ____identity_0)); }
	inline RuntimeObject * get__identity_0() const { return ____identity_0; }
	inline RuntimeObject ** get_address_of__identity_0() { return &____identity_0; }
	inline void set__identity_0(RuntimeObject * value)
	{
		____identity_0 = value;
		Il2CppCodeGenWriteBarrier((&____identity_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF_marshaled_pinvoke
{
	Il2CppIUnknown* ____identity_0;
};
// Native definition for COM marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF_marshaled_com
{
	Il2CppIUnknown* ____identity_0;
};
#endif // MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#ifndef ENDPOINTLISTENER_TF1D36E022EAFCD075CD0398D2868363AA9FB10C2_H
#define ENDPOINTLISTENER_TF1D36E022EAFCD075CD0398D2868363AA9FB10C2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.EndPointListener
struct  EndPointListener_tF1D36E022EAFCD075CD0398D2868363AA9FB10C2  : public RuntimeObject
{
public:
	// System.Net.HttpListener System.Net.EndPointListener::listener
	HttpListener_tAAE9DCF34EB3C09285BF833F390C8CC61C7D247E * ___listener_0;
	// System.Net.IPEndPoint System.Net.EndPointListener::endpoint
	IPEndPoint_tCD29981135F7B1989C3845BF455AD44EBC13DE3F * ___endpoint_1;
	// System.Net.Sockets.Socket System.Net.EndPointListener::sock
	Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 * ___sock_2;
	// System.Collections.Hashtable System.Net.EndPointListener::prefixes
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___prefixes_3;
	// System.Collections.ArrayList System.Net.EndPointListener::unhandled
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ___unhandled_4;
	// System.Collections.ArrayList System.Net.EndPointListener::all
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ___all_5;
	// System.Security.Cryptography.X509Certificates.X509Certificate System.Net.EndPointListener::cert
	X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 * ___cert_6;
	// System.Boolean System.Net.EndPointListener::secure
	bool ___secure_7;
	// System.Collections.Generic.Dictionary`2<System.Net.HttpConnection,System.Net.HttpConnection> System.Net.EndPointListener::unregistered
	Dictionary_2_tE9B05628397C4CAFDAD92B83F0A416E933524B04 * ___unregistered_8;

public:
	inline static int32_t get_offset_of_listener_0() { return static_cast<int32_t>(offsetof(EndPointListener_tF1D36E022EAFCD075CD0398D2868363AA9FB10C2, ___listener_0)); }
	inline HttpListener_tAAE9DCF34EB3C09285BF833F390C8CC61C7D247E * get_listener_0() const { return ___listener_0; }
	inline HttpListener_tAAE9DCF34EB3C09285BF833F390C8CC61C7D247E ** get_address_of_listener_0() { return &___listener_0; }
	inline void set_listener_0(HttpListener_tAAE9DCF34EB3C09285BF833F390C8CC61C7D247E * value)
	{
		___listener_0 = value;
		Il2CppCodeGenWriteBarrier((&___listener_0), value);
	}

	inline static int32_t get_offset_of_endpoint_1() { return static_cast<int32_t>(offsetof(EndPointListener_tF1D36E022EAFCD075CD0398D2868363AA9FB10C2, ___endpoint_1)); }
	inline IPEndPoint_tCD29981135F7B1989C3845BF455AD44EBC13DE3F * get_endpoint_1() const { return ___endpoint_1; }
	inline IPEndPoint_tCD29981135F7B1989C3845BF455AD44EBC13DE3F ** get_address_of_endpoint_1() { return &___endpoint_1; }
	inline void set_endpoint_1(IPEndPoint_tCD29981135F7B1989C3845BF455AD44EBC13DE3F * value)
	{
		___endpoint_1 = value;
		Il2CppCodeGenWriteBarrier((&___endpoint_1), value);
	}

	inline static int32_t get_offset_of_sock_2() { return static_cast<int32_t>(offsetof(EndPointListener_tF1D36E022EAFCD075CD0398D2868363AA9FB10C2, ___sock_2)); }
	inline Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 * get_sock_2() const { return ___sock_2; }
	inline Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 ** get_address_of_sock_2() { return &___sock_2; }
	inline void set_sock_2(Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 * value)
	{
		___sock_2 = value;
		Il2CppCodeGenWriteBarrier((&___sock_2), value);
	}

	inline static int32_t get_offset_of_prefixes_3() { return static_cast<int32_t>(offsetof(EndPointListener_tF1D36E022EAFCD075CD0398D2868363AA9FB10C2, ___prefixes_3)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_prefixes_3() const { return ___prefixes_3; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_prefixes_3() { return &___prefixes_3; }
	inline void set_prefixes_3(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___prefixes_3 = value;
		Il2CppCodeGenWriteBarrier((&___prefixes_3), value);
	}

	inline static int32_t get_offset_of_unhandled_4() { return static_cast<int32_t>(offsetof(EndPointListener_tF1D36E022EAFCD075CD0398D2868363AA9FB10C2, ___unhandled_4)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get_unhandled_4() const { return ___unhandled_4; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of_unhandled_4() { return &___unhandled_4; }
	inline void set_unhandled_4(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		___unhandled_4 = value;
		Il2CppCodeGenWriteBarrier((&___unhandled_4), value);
	}

	inline static int32_t get_offset_of_all_5() { return static_cast<int32_t>(offsetof(EndPointListener_tF1D36E022EAFCD075CD0398D2868363AA9FB10C2, ___all_5)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get_all_5() const { return ___all_5; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of_all_5() { return &___all_5; }
	inline void set_all_5(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		___all_5 = value;
		Il2CppCodeGenWriteBarrier((&___all_5), value);
	}

	inline static int32_t get_offset_of_cert_6() { return static_cast<int32_t>(offsetof(EndPointListener_tF1D36E022EAFCD075CD0398D2868363AA9FB10C2, ___cert_6)); }
	inline X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 * get_cert_6() const { return ___cert_6; }
	inline X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 ** get_address_of_cert_6() { return &___cert_6; }
	inline void set_cert_6(X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 * value)
	{
		___cert_6 = value;
		Il2CppCodeGenWriteBarrier((&___cert_6), value);
	}

	inline static int32_t get_offset_of_secure_7() { return static_cast<int32_t>(offsetof(EndPointListener_tF1D36E022EAFCD075CD0398D2868363AA9FB10C2, ___secure_7)); }
	inline bool get_secure_7() const { return ___secure_7; }
	inline bool* get_address_of_secure_7() { return &___secure_7; }
	inline void set_secure_7(bool value)
	{
		___secure_7 = value;
	}

	inline static int32_t get_offset_of_unregistered_8() { return static_cast<int32_t>(offsetof(EndPointListener_tF1D36E022EAFCD075CD0398D2868363AA9FB10C2, ___unregistered_8)); }
	inline Dictionary_2_tE9B05628397C4CAFDAD92B83F0A416E933524B04 * get_unregistered_8() const { return ___unregistered_8; }
	inline Dictionary_2_tE9B05628397C4CAFDAD92B83F0A416E933524B04 ** get_address_of_unregistered_8() { return &___unregistered_8; }
	inline void set_unregistered_8(Dictionary_2_tE9B05628397C4CAFDAD92B83F0A416E933524B04 * value)
	{
		___unregistered_8 = value;
		Il2CppCodeGenWriteBarrier((&___unregistered_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENDPOINTLISTENER_TF1D36E022EAFCD075CD0398D2868363AA9FB10C2_H
#ifndef ENDPOINTMANAGER_TEA1F3FD50367B1217FA4DF7FEED29A42D70D71A2_H
#define ENDPOINTMANAGER_TEA1F3FD50367B1217FA4DF7FEED29A42D70D71A2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.EndPointManager
struct  EndPointManager_tEA1F3FD50367B1217FA4DF7FEED29A42D70D71A2  : public RuntimeObject
{
public:

public:
};

struct EndPointManager_tEA1F3FD50367B1217FA4DF7FEED29A42D70D71A2_StaticFields
{
public:
	// System.Collections.Hashtable System.Net.EndPointManager::ip_to_endpoints
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___ip_to_endpoints_0;

public:
	inline static int32_t get_offset_of_ip_to_endpoints_0() { return static_cast<int32_t>(offsetof(EndPointManager_tEA1F3FD50367B1217FA4DF7FEED29A42D70D71A2_StaticFields, ___ip_to_endpoints_0)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_ip_to_endpoints_0() const { return ___ip_to_endpoints_0; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_ip_to_endpoints_0() { return &___ip_to_endpoints_0; }
	inline void set_ip_to_endpoints_0(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___ip_to_endpoints_0 = value;
		Il2CppCodeGenWriteBarrier((&___ip_to_endpoints_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENDPOINTMANAGER_TEA1F3FD50367B1217FA4DF7FEED29A42D70D71A2_H
#ifndef FTPASYNCRESULT_TB318D495766A9449055B1D8C8C801095CF2CDEA3_H
#define FTPASYNCRESULT_TB318D495766A9449055B1D8C8C801095CF2CDEA3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.FtpAsyncResult
struct  FtpAsyncResult_tB318D495766A9449055B1D8C8C801095CF2CDEA3  : public RuntimeObject
{
public:
	// System.Net.FtpWebResponse System.Net.FtpAsyncResult::response
	FtpWebResponse_t8775110950F0637C1A8A495892122865F05FC205 * ___response_0;
	// System.Threading.ManualResetEvent System.Net.FtpAsyncResult::waitHandle
	ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * ___waitHandle_1;
	// System.Exception System.Net.FtpAsyncResult::exception
	Exception_t * ___exception_2;
	// System.AsyncCallback System.Net.FtpAsyncResult::callback
	AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___callback_3;
	// System.IO.Stream System.Net.FtpAsyncResult::stream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___stream_4;
	// System.Object System.Net.FtpAsyncResult::state
	RuntimeObject * ___state_5;
	// System.Boolean System.Net.FtpAsyncResult::completed
	bool ___completed_6;
	// System.Boolean System.Net.FtpAsyncResult::synch
	bool ___synch_7;
	// System.Object System.Net.FtpAsyncResult::locker
	RuntimeObject * ___locker_8;

public:
	inline static int32_t get_offset_of_response_0() { return static_cast<int32_t>(offsetof(FtpAsyncResult_tB318D495766A9449055B1D8C8C801095CF2CDEA3, ___response_0)); }
	inline FtpWebResponse_t8775110950F0637C1A8A495892122865F05FC205 * get_response_0() const { return ___response_0; }
	inline FtpWebResponse_t8775110950F0637C1A8A495892122865F05FC205 ** get_address_of_response_0() { return &___response_0; }
	inline void set_response_0(FtpWebResponse_t8775110950F0637C1A8A495892122865F05FC205 * value)
	{
		___response_0 = value;
		Il2CppCodeGenWriteBarrier((&___response_0), value);
	}

	inline static int32_t get_offset_of_waitHandle_1() { return static_cast<int32_t>(offsetof(FtpAsyncResult_tB318D495766A9449055B1D8C8C801095CF2CDEA3, ___waitHandle_1)); }
	inline ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * get_waitHandle_1() const { return ___waitHandle_1; }
	inline ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 ** get_address_of_waitHandle_1() { return &___waitHandle_1; }
	inline void set_waitHandle_1(ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * value)
	{
		___waitHandle_1 = value;
		Il2CppCodeGenWriteBarrier((&___waitHandle_1), value);
	}

	inline static int32_t get_offset_of_exception_2() { return static_cast<int32_t>(offsetof(FtpAsyncResult_tB318D495766A9449055B1D8C8C801095CF2CDEA3, ___exception_2)); }
	inline Exception_t * get_exception_2() const { return ___exception_2; }
	inline Exception_t ** get_address_of_exception_2() { return &___exception_2; }
	inline void set_exception_2(Exception_t * value)
	{
		___exception_2 = value;
		Il2CppCodeGenWriteBarrier((&___exception_2), value);
	}

	inline static int32_t get_offset_of_callback_3() { return static_cast<int32_t>(offsetof(FtpAsyncResult_tB318D495766A9449055B1D8C8C801095CF2CDEA3, ___callback_3)); }
	inline AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * get_callback_3() const { return ___callback_3; }
	inline AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 ** get_address_of_callback_3() { return &___callback_3; }
	inline void set_callback_3(AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * value)
	{
		___callback_3 = value;
		Il2CppCodeGenWriteBarrier((&___callback_3), value);
	}

	inline static int32_t get_offset_of_stream_4() { return static_cast<int32_t>(offsetof(FtpAsyncResult_tB318D495766A9449055B1D8C8C801095CF2CDEA3, ___stream_4)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_stream_4() const { return ___stream_4; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_stream_4() { return &___stream_4; }
	inline void set_stream_4(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___stream_4 = value;
		Il2CppCodeGenWriteBarrier((&___stream_4), value);
	}

	inline static int32_t get_offset_of_state_5() { return static_cast<int32_t>(offsetof(FtpAsyncResult_tB318D495766A9449055B1D8C8C801095CF2CDEA3, ___state_5)); }
	inline RuntimeObject * get_state_5() const { return ___state_5; }
	inline RuntimeObject ** get_address_of_state_5() { return &___state_5; }
	inline void set_state_5(RuntimeObject * value)
	{
		___state_5 = value;
		Il2CppCodeGenWriteBarrier((&___state_5), value);
	}

	inline static int32_t get_offset_of_completed_6() { return static_cast<int32_t>(offsetof(FtpAsyncResult_tB318D495766A9449055B1D8C8C801095CF2CDEA3, ___completed_6)); }
	inline bool get_completed_6() const { return ___completed_6; }
	inline bool* get_address_of_completed_6() { return &___completed_6; }
	inline void set_completed_6(bool value)
	{
		___completed_6 = value;
	}

	inline static int32_t get_offset_of_synch_7() { return static_cast<int32_t>(offsetof(FtpAsyncResult_tB318D495766A9449055B1D8C8C801095CF2CDEA3, ___synch_7)); }
	inline bool get_synch_7() const { return ___synch_7; }
	inline bool* get_address_of_synch_7() { return &___synch_7; }
	inline void set_synch_7(bool value)
	{
		___synch_7 = value;
	}

	inline static int32_t get_offset_of_locker_8() { return static_cast<int32_t>(offsetof(FtpAsyncResult_tB318D495766A9449055B1D8C8C801095CF2CDEA3, ___locker_8)); }
	inline RuntimeObject * get_locker_8() const { return ___locker_8; }
	inline RuntimeObject ** get_address_of_locker_8() { return &___locker_8; }
	inline void set_locker_8(RuntimeObject * value)
	{
		___locker_8 = value;
		Il2CppCodeGenWriteBarrier((&___locker_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FTPASYNCRESULT_TB318D495766A9449055B1D8C8C801095CF2CDEA3_H
#ifndef FTPREQUESTCREATOR_T2C5CC32221C790FB648AF6276DA861B4ABAC357F_H
#define FTPREQUESTCREATOR_T2C5CC32221C790FB648AF6276DA861B4ABAC357F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.FtpRequestCreator
struct  FtpRequestCreator_t2C5CC32221C790FB648AF6276DA861B4ABAC357F  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FTPREQUESTCREATOR_T2C5CC32221C790FB648AF6276DA861B4ABAC357F_H
#ifndef HTTPLISTENERCONTEXT_T438DA3609B8D775884622DF392BD7D72C651FCFA_H
#define HTTPLISTENERCONTEXT_T438DA3609B8D775884622DF392BD7D72C651FCFA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HttpListenerContext
struct  HttpListenerContext_t438DA3609B8D775884622DF392BD7D72C651FCFA  : public RuntimeObject
{
public:
	// System.Net.HttpListenerRequest System.Net.HttpListenerContext::request
	HttpListenerRequest_t4BD625F3BF4726345B12F4FF06DB23A5932FD8CE * ___request_0;
	// System.Net.HttpListenerResponse System.Net.HttpListenerContext::response
	HttpListenerResponse_t34AC9D467928315CC4285B8A14517C6D5564224C * ___response_1;
	// System.Security.Principal.IPrincipal System.Net.HttpListenerContext::user
	RuntimeObject* ___user_2;
	// System.Net.HttpConnection System.Net.HttpListenerContext::cnc
	HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E * ___cnc_3;
	// System.String System.Net.HttpListenerContext::error
	String_t* ___error_4;
	// System.Int32 System.Net.HttpListenerContext::err_status
	int32_t ___err_status_5;
	// System.Net.HttpListener System.Net.HttpListenerContext::Listener
	HttpListener_tAAE9DCF34EB3C09285BF833F390C8CC61C7D247E * ___Listener_6;

public:
	inline static int32_t get_offset_of_request_0() { return static_cast<int32_t>(offsetof(HttpListenerContext_t438DA3609B8D775884622DF392BD7D72C651FCFA, ___request_0)); }
	inline HttpListenerRequest_t4BD625F3BF4726345B12F4FF06DB23A5932FD8CE * get_request_0() const { return ___request_0; }
	inline HttpListenerRequest_t4BD625F3BF4726345B12F4FF06DB23A5932FD8CE ** get_address_of_request_0() { return &___request_0; }
	inline void set_request_0(HttpListenerRequest_t4BD625F3BF4726345B12F4FF06DB23A5932FD8CE * value)
	{
		___request_0 = value;
		Il2CppCodeGenWriteBarrier((&___request_0), value);
	}

	inline static int32_t get_offset_of_response_1() { return static_cast<int32_t>(offsetof(HttpListenerContext_t438DA3609B8D775884622DF392BD7D72C651FCFA, ___response_1)); }
	inline HttpListenerResponse_t34AC9D467928315CC4285B8A14517C6D5564224C * get_response_1() const { return ___response_1; }
	inline HttpListenerResponse_t34AC9D467928315CC4285B8A14517C6D5564224C ** get_address_of_response_1() { return &___response_1; }
	inline void set_response_1(HttpListenerResponse_t34AC9D467928315CC4285B8A14517C6D5564224C * value)
	{
		___response_1 = value;
		Il2CppCodeGenWriteBarrier((&___response_1), value);
	}

	inline static int32_t get_offset_of_user_2() { return static_cast<int32_t>(offsetof(HttpListenerContext_t438DA3609B8D775884622DF392BD7D72C651FCFA, ___user_2)); }
	inline RuntimeObject* get_user_2() const { return ___user_2; }
	inline RuntimeObject** get_address_of_user_2() { return &___user_2; }
	inline void set_user_2(RuntimeObject* value)
	{
		___user_2 = value;
		Il2CppCodeGenWriteBarrier((&___user_2), value);
	}

	inline static int32_t get_offset_of_cnc_3() { return static_cast<int32_t>(offsetof(HttpListenerContext_t438DA3609B8D775884622DF392BD7D72C651FCFA, ___cnc_3)); }
	inline HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E * get_cnc_3() const { return ___cnc_3; }
	inline HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E ** get_address_of_cnc_3() { return &___cnc_3; }
	inline void set_cnc_3(HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E * value)
	{
		___cnc_3 = value;
		Il2CppCodeGenWriteBarrier((&___cnc_3), value);
	}

	inline static int32_t get_offset_of_error_4() { return static_cast<int32_t>(offsetof(HttpListenerContext_t438DA3609B8D775884622DF392BD7D72C651FCFA, ___error_4)); }
	inline String_t* get_error_4() const { return ___error_4; }
	inline String_t** get_address_of_error_4() { return &___error_4; }
	inline void set_error_4(String_t* value)
	{
		___error_4 = value;
		Il2CppCodeGenWriteBarrier((&___error_4), value);
	}

	inline static int32_t get_offset_of_err_status_5() { return static_cast<int32_t>(offsetof(HttpListenerContext_t438DA3609B8D775884622DF392BD7D72C651FCFA, ___err_status_5)); }
	inline int32_t get_err_status_5() const { return ___err_status_5; }
	inline int32_t* get_address_of_err_status_5() { return &___err_status_5; }
	inline void set_err_status_5(int32_t value)
	{
		___err_status_5 = value;
	}

	inline static int32_t get_offset_of_Listener_6() { return static_cast<int32_t>(offsetof(HttpListenerContext_t438DA3609B8D775884622DF392BD7D72C651FCFA, ___Listener_6)); }
	inline HttpListener_tAAE9DCF34EB3C09285BF833F390C8CC61C7D247E * get_Listener_6() const { return ___Listener_6; }
	inline HttpListener_tAAE9DCF34EB3C09285BF833F390C8CC61C7D247E ** get_address_of_Listener_6() { return &___Listener_6; }
	inline void set_Listener_6(HttpListener_tAAE9DCF34EB3C09285BF833F390C8CC61C7D247E * value)
	{
		___Listener_6 = value;
		Il2CppCodeGenWriteBarrier((&___Listener_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPLISTENERCONTEXT_T438DA3609B8D775884622DF392BD7D72C651FCFA_H
#ifndef HTTPLISTENERPREFIXCOLLECTION_TCFDDEB0773E99D9DA834AB2FBF1AF0F2B1AC7B5A_H
#define HTTPLISTENERPREFIXCOLLECTION_TCFDDEB0773E99D9DA834AB2FBF1AF0F2B1AC7B5A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HttpListenerPrefixCollection
struct  HttpListenerPrefixCollection_tCFDDEB0773E99D9DA834AB2FBF1AF0F2B1AC7B5A  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.String> System.Net.HttpListenerPrefixCollection::prefixes
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___prefixes_0;
	// System.Net.HttpListener System.Net.HttpListenerPrefixCollection::listener
	HttpListener_tAAE9DCF34EB3C09285BF833F390C8CC61C7D247E * ___listener_1;

public:
	inline static int32_t get_offset_of_prefixes_0() { return static_cast<int32_t>(offsetof(HttpListenerPrefixCollection_tCFDDEB0773E99D9DA834AB2FBF1AF0F2B1AC7B5A, ___prefixes_0)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_prefixes_0() const { return ___prefixes_0; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_prefixes_0() { return &___prefixes_0; }
	inline void set_prefixes_0(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___prefixes_0 = value;
		Il2CppCodeGenWriteBarrier((&___prefixes_0), value);
	}

	inline static int32_t get_offset_of_listener_1() { return static_cast<int32_t>(offsetof(HttpListenerPrefixCollection_tCFDDEB0773E99D9DA834AB2FBF1AF0F2B1AC7B5A, ___listener_1)); }
	inline HttpListener_tAAE9DCF34EB3C09285BF833F390C8CC61C7D247E * get_listener_1() const { return ___listener_1; }
	inline HttpListener_tAAE9DCF34EB3C09285BF833F390C8CC61C7D247E ** get_address_of_listener_1() { return &___listener_1; }
	inline void set_listener_1(HttpListener_tAAE9DCF34EB3C09285BF833F390C8CC61C7D247E * value)
	{
		___listener_1 = value;
		Il2CppCodeGenWriteBarrier((&___listener_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPLISTENERPREFIXCOLLECTION_TCFDDEB0773E99D9DA834AB2FBF1AF0F2B1AC7B5A_H
#ifndef HTTPLISTENERREQUEST_T4BD625F3BF4726345B12F4FF06DB23A5932FD8CE_H
#define HTTPLISTENERREQUEST_T4BD625F3BF4726345B12F4FF06DB23A5932FD8CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HttpListenerRequest
struct  HttpListenerRequest_t4BD625F3BF4726345B12F4FF06DB23A5932FD8CE  : public RuntimeObject
{
public:
	// System.String[] System.Net.HttpListenerRequest::accept_types
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___accept_types_0;
	// System.Text.Encoding System.Net.HttpListenerRequest::content_encoding
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___content_encoding_1;
	// System.Int64 System.Net.HttpListenerRequest::content_length
	int64_t ___content_length_2;
	// System.Boolean System.Net.HttpListenerRequest::cl_set
	bool ___cl_set_3;
	// System.Net.CookieCollection System.Net.HttpListenerRequest::cookies
	CookieCollection_t69ADF0ABD99419E54AB4740B341D94F443D995A3 * ___cookies_4;
	// System.Net.WebHeaderCollection System.Net.HttpListenerRequest::headers
	WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304 * ___headers_5;
	// System.String System.Net.HttpListenerRequest::method
	String_t* ___method_6;
	// System.IO.Stream System.Net.HttpListenerRequest::input_stream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___input_stream_7;
	// System.Version System.Net.HttpListenerRequest::version
	Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD * ___version_8;
	// System.Collections.Specialized.NameValueCollection System.Net.HttpListenerRequest::query_string
	NameValueCollection_t7C7CED43E4C6E997E3C8012F1D2CC4027FAD10D1 * ___query_string_9;
	// System.String System.Net.HttpListenerRequest::raw_url
	String_t* ___raw_url_10;
	// System.Uri System.Net.HttpListenerRequest::url
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___url_11;
	// System.Uri System.Net.HttpListenerRequest::referrer
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___referrer_12;
	// System.String[] System.Net.HttpListenerRequest::user_languages
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___user_languages_13;
	// System.Net.HttpListenerContext System.Net.HttpListenerRequest::context
	HttpListenerContext_t438DA3609B8D775884622DF392BD7D72C651FCFA * ___context_14;
	// System.Boolean System.Net.HttpListenerRequest::is_chunked
	bool ___is_chunked_15;
	// System.Boolean System.Net.HttpListenerRequest::ka_set
	bool ___ka_set_16;
	// System.Boolean System.Net.HttpListenerRequest::keep_alive
	bool ___keep_alive_17;
	// System.Net.HttpListenerRequest_GCCDelegate System.Net.HttpListenerRequest::gcc_delegate
	GCCDelegate_t2ED11BA19D14B2D5E74288CA763708D5E244EB46 * ___gcc_delegate_18;

public:
	inline static int32_t get_offset_of_accept_types_0() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t4BD625F3BF4726345B12F4FF06DB23A5932FD8CE, ___accept_types_0)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_accept_types_0() const { return ___accept_types_0; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_accept_types_0() { return &___accept_types_0; }
	inline void set_accept_types_0(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___accept_types_0 = value;
		Il2CppCodeGenWriteBarrier((&___accept_types_0), value);
	}

	inline static int32_t get_offset_of_content_encoding_1() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t4BD625F3BF4726345B12F4FF06DB23A5932FD8CE, ___content_encoding_1)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get_content_encoding_1() const { return ___content_encoding_1; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of_content_encoding_1() { return &___content_encoding_1; }
	inline void set_content_encoding_1(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		___content_encoding_1 = value;
		Il2CppCodeGenWriteBarrier((&___content_encoding_1), value);
	}

	inline static int32_t get_offset_of_content_length_2() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t4BD625F3BF4726345B12F4FF06DB23A5932FD8CE, ___content_length_2)); }
	inline int64_t get_content_length_2() const { return ___content_length_2; }
	inline int64_t* get_address_of_content_length_2() { return &___content_length_2; }
	inline void set_content_length_2(int64_t value)
	{
		___content_length_2 = value;
	}

	inline static int32_t get_offset_of_cl_set_3() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t4BD625F3BF4726345B12F4FF06DB23A5932FD8CE, ___cl_set_3)); }
	inline bool get_cl_set_3() const { return ___cl_set_3; }
	inline bool* get_address_of_cl_set_3() { return &___cl_set_3; }
	inline void set_cl_set_3(bool value)
	{
		___cl_set_3 = value;
	}

	inline static int32_t get_offset_of_cookies_4() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t4BD625F3BF4726345B12F4FF06DB23A5932FD8CE, ___cookies_4)); }
	inline CookieCollection_t69ADF0ABD99419E54AB4740B341D94F443D995A3 * get_cookies_4() const { return ___cookies_4; }
	inline CookieCollection_t69ADF0ABD99419E54AB4740B341D94F443D995A3 ** get_address_of_cookies_4() { return &___cookies_4; }
	inline void set_cookies_4(CookieCollection_t69ADF0ABD99419E54AB4740B341D94F443D995A3 * value)
	{
		___cookies_4 = value;
		Il2CppCodeGenWriteBarrier((&___cookies_4), value);
	}

	inline static int32_t get_offset_of_headers_5() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t4BD625F3BF4726345B12F4FF06DB23A5932FD8CE, ___headers_5)); }
	inline WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304 * get_headers_5() const { return ___headers_5; }
	inline WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304 ** get_address_of_headers_5() { return &___headers_5; }
	inline void set_headers_5(WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304 * value)
	{
		___headers_5 = value;
		Il2CppCodeGenWriteBarrier((&___headers_5), value);
	}

	inline static int32_t get_offset_of_method_6() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t4BD625F3BF4726345B12F4FF06DB23A5932FD8CE, ___method_6)); }
	inline String_t* get_method_6() const { return ___method_6; }
	inline String_t** get_address_of_method_6() { return &___method_6; }
	inline void set_method_6(String_t* value)
	{
		___method_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_6), value);
	}

	inline static int32_t get_offset_of_input_stream_7() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t4BD625F3BF4726345B12F4FF06DB23A5932FD8CE, ___input_stream_7)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_input_stream_7() const { return ___input_stream_7; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_input_stream_7() { return &___input_stream_7; }
	inline void set_input_stream_7(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___input_stream_7 = value;
		Il2CppCodeGenWriteBarrier((&___input_stream_7), value);
	}

	inline static int32_t get_offset_of_version_8() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t4BD625F3BF4726345B12F4FF06DB23A5932FD8CE, ___version_8)); }
	inline Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD * get_version_8() const { return ___version_8; }
	inline Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD ** get_address_of_version_8() { return &___version_8; }
	inline void set_version_8(Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD * value)
	{
		___version_8 = value;
		Il2CppCodeGenWriteBarrier((&___version_8), value);
	}

	inline static int32_t get_offset_of_query_string_9() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t4BD625F3BF4726345B12F4FF06DB23A5932FD8CE, ___query_string_9)); }
	inline NameValueCollection_t7C7CED43E4C6E997E3C8012F1D2CC4027FAD10D1 * get_query_string_9() const { return ___query_string_9; }
	inline NameValueCollection_t7C7CED43E4C6E997E3C8012F1D2CC4027FAD10D1 ** get_address_of_query_string_9() { return &___query_string_9; }
	inline void set_query_string_9(NameValueCollection_t7C7CED43E4C6E997E3C8012F1D2CC4027FAD10D1 * value)
	{
		___query_string_9 = value;
		Il2CppCodeGenWriteBarrier((&___query_string_9), value);
	}

	inline static int32_t get_offset_of_raw_url_10() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t4BD625F3BF4726345B12F4FF06DB23A5932FD8CE, ___raw_url_10)); }
	inline String_t* get_raw_url_10() const { return ___raw_url_10; }
	inline String_t** get_address_of_raw_url_10() { return &___raw_url_10; }
	inline void set_raw_url_10(String_t* value)
	{
		___raw_url_10 = value;
		Il2CppCodeGenWriteBarrier((&___raw_url_10), value);
	}

	inline static int32_t get_offset_of_url_11() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t4BD625F3BF4726345B12F4FF06DB23A5932FD8CE, ___url_11)); }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * get_url_11() const { return ___url_11; }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E ** get_address_of_url_11() { return &___url_11; }
	inline void set_url_11(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * value)
	{
		___url_11 = value;
		Il2CppCodeGenWriteBarrier((&___url_11), value);
	}

	inline static int32_t get_offset_of_referrer_12() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t4BD625F3BF4726345B12F4FF06DB23A5932FD8CE, ___referrer_12)); }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * get_referrer_12() const { return ___referrer_12; }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E ** get_address_of_referrer_12() { return &___referrer_12; }
	inline void set_referrer_12(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * value)
	{
		___referrer_12 = value;
		Il2CppCodeGenWriteBarrier((&___referrer_12), value);
	}

	inline static int32_t get_offset_of_user_languages_13() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t4BD625F3BF4726345B12F4FF06DB23A5932FD8CE, ___user_languages_13)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_user_languages_13() const { return ___user_languages_13; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_user_languages_13() { return &___user_languages_13; }
	inline void set_user_languages_13(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___user_languages_13 = value;
		Il2CppCodeGenWriteBarrier((&___user_languages_13), value);
	}

	inline static int32_t get_offset_of_context_14() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t4BD625F3BF4726345B12F4FF06DB23A5932FD8CE, ___context_14)); }
	inline HttpListenerContext_t438DA3609B8D775884622DF392BD7D72C651FCFA * get_context_14() const { return ___context_14; }
	inline HttpListenerContext_t438DA3609B8D775884622DF392BD7D72C651FCFA ** get_address_of_context_14() { return &___context_14; }
	inline void set_context_14(HttpListenerContext_t438DA3609B8D775884622DF392BD7D72C651FCFA * value)
	{
		___context_14 = value;
		Il2CppCodeGenWriteBarrier((&___context_14), value);
	}

	inline static int32_t get_offset_of_is_chunked_15() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t4BD625F3BF4726345B12F4FF06DB23A5932FD8CE, ___is_chunked_15)); }
	inline bool get_is_chunked_15() const { return ___is_chunked_15; }
	inline bool* get_address_of_is_chunked_15() { return &___is_chunked_15; }
	inline void set_is_chunked_15(bool value)
	{
		___is_chunked_15 = value;
	}

	inline static int32_t get_offset_of_ka_set_16() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t4BD625F3BF4726345B12F4FF06DB23A5932FD8CE, ___ka_set_16)); }
	inline bool get_ka_set_16() const { return ___ka_set_16; }
	inline bool* get_address_of_ka_set_16() { return &___ka_set_16; }
	inline void set_ka_set_16(bool value)
	{
		___ka_set_16 = value;
	}

	inline static int32_t get_offset_of_keep_alive_17() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t4BD625F3BF4726345B12F4FF06DB23A5932FD8CE, ___keep_alive_17)); }
	inline bool get_keep_alive_17() const { return ___keep_alive_17; }
	inline bool* get_address_of_keep_alive_17() { return &___keep_alive_17; }
	inline void set_keep_alive_17(bool value)
	{
		___keep_alive_17 = value;
	}

	inline static int32_t get_offset_of_gcc_delegate_18() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t4BD625F3BF4726345B12F4FF06DB23A5932FD8CE, ___gcc_delegate_18)); }
	inline GCCDelegate_t2ED11BA19D14B2D5E74288CA763708D5E244EB46 * get_gcc_delegate_18() const { return ___gcc_delegate_18; }
	inline GCCDelegate_t2ED11BA19D14B2D5E74288CA763708D5E244EB46 ** get_address_of_gcc_delegate_18() { return &___gcc_delegate_18; }
	inline void set_gcc_delegate_18(GCCDelegate_t2ED11BA19D14B2D5E74288CA763708D5E244EB46 * value)
	{
		___gcc_delegate_18 = value;
		Il2CppCodeGenWriteBarrier((&___gcc_delegate_18), value);
	}
};

struct HttpListenerRequest_t4BD625F3BF4726345B12F4FF06DB23A5932FD8CE_StaticFields
{
public:
	// System.Byte[] System.Net.HttpListenerRequest::_100continue
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____100continue_19;
	// System.Char[] System.Net.HttpListenerRequest::separators
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___separators_20;

public:
	inline static int32_t get_offset_of__100continue_19() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t4BD625F3BF4726345B12F4FF06DB23A5932FD8CE_StaticFields, ____100continue_19)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__100continue_19() const { return ____100continue_19; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__100continue_19() { return &____100continue_19; }
	inline void set__100continue_19(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____100continue_19 = value;
		Il2CppCodeGenWriteBarrier((&____100continue_19), value);
	}

	inline static int32_t get_offset_of_separators_20() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t4BD625F3BF4726345B12F4FF06DB23A5932FD8CE_StaticFields, ___separators_20)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_separators_20() const { return ___separators_20; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_separators_20() { return &___separators_20; }
	inline void set_separators_20(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___separators_20 = value;
		Il2CppCodeGenWriteBarrier((&___separators_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPLISTENERREQUEST_T4BD625F3BF4726345B12F4FF06DB23A5932FD8CE_H
#ifndef HTTPLISTENERRESPONSE_T34AC9D467928315CC4285B8A14517C6D5564224C_H
#define HTTPLISTENERRESPONSE_T34AC9D467928315CC4285B8A14517C6D5564224C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HttpListenerResponse
struct  HttpListenerResponse_t34AC9D467928315CC4285B8A14517C6D5564224C  : public RuntimeObject
{
public:
	// System.Boolean System.Net.HttpListenerResponse::disposed
	bool ___disposed_0;
	// System.Text.Encoding System.Net.HttpListenerResponse::content_encoding
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___content_encoding_1;
	// System.Int64 System.Net.HttpListenerResponse::content_length
	int64_t ___content_length_2;
	// System.Boolean System.Net.HttpListenerResponse::cl_set
	bool ___cl_set_3;
	// System.String System.Net.HttpListenerResponse::content_type
	String_t* ___content_type_4;
	// System.Net.CookieCollection System.Net.HttpListenerResponse::cookies
	CookieCollection_t69ADF0ABD99419E54AB4740B341D94F443D995A3 * ___cookies_5;
	// System.Net.WebHeaderCollection System.Net.HttpListenerResponse::headers
	WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304 * ___headers_6;
	// System.Boolean System.Net.HttpListenerResponse::keep_alive
	bool ___keep_alive_7;
	// System.Net.ResponseStream System.Net.HttpListenerResponse::output_stream
	ResponseStream_tE537A0E20974BEB6629CBEBE42C27C9B97126032 * ___output_stream_8;
	// System.Version System.Net.HttpListenerResponse::version
	Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD * ___version_9;
	// System.String System.Net.HttpListenerResponse::location
	String_t* ___location_10;
	// System.Int32 System.Net.HttpListenerResponse::status_code
	int32_t ___status_code_11;
	// System.String System.Net.HttpListenerResponse::status_description
	String_t* ___status_description_12;
	// System.Boolean System.Net.HttpListenerResponse::chunked
	bool ___chunked_13;
	// System.Net.HttpListenerContext System.Net.HttpListenerResponse::context
	HttpListenerContext_t438DA3609B8D775884622DF392BD7D72C651FCFA * ___context_14;
	// System.Boolean System.Net.HttpListenerResponse::HeadersSent
	bool ___HeadersSent_15;
	// System.Object System.Net.HttpListenerResponse::headers_lock
	RuntimeObject * ___headers_lock_16;
	// System.Boolean System.Net.HttpListenerResponse::force_close_chunked
	bool ___force_close_chunked_17;

public:
	inline static int32_t get_offset_of_disposed_0() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t34AC9D467928315CC4285B8A14517C6D5564224C, ___disposed_0)); }
	inline bool get_disposed_0() const { return ___disposed_0; }
	inline bool* get_address_of_disposed_0() { return &___disposed_0; }
	inline void set_disposed_0(bool value)
	{
		___disposed_0 = value;
	}

	inline static int32_t get_offset_of_content_encoding_1() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t34AC9D467928315CC4285B8A14517C6D5564224C, ___content_encoding_1)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get_content_encoding_1() const { return ___content_encoding_1; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of_content_encoding_1() { return &___content_encoding_1; }
	inline void set_content_encoding_1(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		___content_encoding_1 = value;
		Il2CppCodeGenWriteBarrier((&___content_encoding_1), value);
	}

	inline static int32_t get_offset_of_content_length_2() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t34AC9D467928315CC4285B8A14517C6D5564224C, ___content_length_2)); }
	inline int64_t get_content_length_2() const { return ___content_length_2; }
	inline int64_t* get_address_of_content_length_2() { return &___content_length_2; }
	inline void set_content_length_2(int64_t value)
	{
		___content_length_2 = value;
	}

	inline static int32_t get_offset_of_cl_set_3() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t34AC9D467928315CC4285B8A14517C6D5564224C, ___cl_set_3)); }
	inline bool get_cl_set_3() const { return ___cl_set_3; }
	inline bool* get_address_of_cl_set_3() { return &___cl_set_3; }
	inline void set_cl_set_3(bool value)
	{
		___cl_set_3 = value;
	}

	inline static int32_t get_offset_of_content_type_4() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t34AC9D467928315CC4285B8A14517C6D5564224C, ___content_type_4)); }
	inline String_t* get_content_type_4() const { return ___content_type_4; }
	inline String_t** get_address_of_content_type_4() { return &___content_type_4; }
	inline void set_content_type_4(String_t* value)
	{
		___content_type_4 = value;
		Il2CppCodeGenWriteBarrier((&___content_type_4), value);
	}

	inline static int32_t get_offset_of_cookies_5() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t34AC9D467928315CC4285B8A14517C6D5564224C, ___cookies_5)); }
	inline CookieCollection_t69ADF0ABD99419E54AB4740B341D94F443D995A3 * get_cookies_5() const { return ___cookies_5; }
	inline CookieCollection_t69ADF0ABD99419E54AB4740B341D94F443D995A3 ** get_address_of_cookies_5() { return &___cookies_5; }
	inline void set_cookies_5(CookieCollection_t69ADF0ABD99419E54AB4740B341D94F443D995A3 * value)
	{
		___cookies_5 = value;
		Il2CppCodeGenWriteBarrier((&___cookies_5), value);
	}

	inline static int32_t get_offset_of_headers_6() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t34AC9D467928315CC4285B8A14517C6D5564224C, ___headers_6)); }
	inline WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304 * get_headers_6() const { return ___headers_6; }
	inline WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304 ** get_address_of_headers_6() { return &___headers_6; }
	inline void set_headers_6(WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304 * value)
	{
		___headers_6 = value;
		Il2CppCodeGenWriteBarrier((&___headers_6), value);
	}

	inline static int32_t get_offset_of_keep_alive_7() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t34AC9D467928315CC4285B8A14517C6D5564224C, ___keep_alive_7)); }
	inline bool get_keep_alive_7() const { return ___keep_alive_7; }
	inline bool* get_address_of_keep_alive_7() { return &___keep_alive_7; }
	inline void set_keep_alive_7(bool value)
	{
		___keep_alive_7 = value;
	}

	inline static int32_t get_offset_of_output_stream_8() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t34AC9D467928315CC4285B8A14517C6D5564224C, ___output_stream_8)); }
	inline ResponseStream_tE537A0E20974BEB6629CBEBE42C27C9B97126032 * get_output_stream_8() const { return ___output_stream_8; }
	inline ResponseStream_tE537A0E20974BEB6629CBEBE42C27C9B97126032 ** get_address_of_output_stream_8() { return &___output_stream_8; }
	inline void set_output_stream_8(ResponseStream_tE537A0E20974BEB6629CBEBE42C27C9B97126032 * value)
	{
		___output_stream_8 = value;
		Il2CppCodeGenWriteBarrier((&___output_stream_8), value);
	}

	inline static int32_t get_offset_of_version_9() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t34AC9D467928315CC4285B8A14517C6D5564224C, ___version_9)); }
	inline Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD * get_version_9() const { return ___version_9; }
	inline Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD ** get_address_of_version_9() { return &___version_9; }
	inline void set_version_9(Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD * value)
	{
		___version_9 = value;
		Il2CppCodeGenWriteBarrier((&___version_9), value);
	}

	inline static int32_t get_offset_of_location_10() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t34AC9D467928315CC4285B8A14517C6D5564224C, ___location_10)); }
	inline String_t* get_location_10() const { return ___location_10; }
	inline String_t** get_address_of_location_10() { return &___location_10; }
	inline void set_location_10(String_t* value)
	{
		___location_10 = value;
		Il2CppCodeGenWriteBarrier((&___location_10), value);
	}

	inline static int32_t get_offset_of_status_code_11() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t34AC9D467928315CC4285B8A14517C6D5564224C, ___status_code_11)); }
	inline int32_t get_status_code_11() const { return ___status_code_11; }
	inline int32_t* get_address_of_status_code_11() { return &___status_code_11; }
	inline void set_status_code_11(int32_t value)
	{
		___status_code_11 = value;
	}

	inline static int32_t get_offset_of_status_description_12() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t34AC9D467928315CC4285B8A14517C6D5564224C, ___status_description_12)); }
	inline String_t* get_status_description_12() const { return ___status_description_12; }
	inline String_t** get_address_of_status_description_12() { return &___status_description_12; }
	inline void set_status_description_12(String_t* value)
	{
		___status_description_12 = value;
		Il2CppCodeGenWriteBarrier((&___status_description_12), value);
	}

	inline static int32_t get_offset_of_chunked_13() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t34AC9D467928315CC4285B8A14517C6D5564224C, ___chunked_13)); }
	inline bool get_chunked_13() const { return ___chunked_13; }
	inline bool* get_address_of_chunked_13() { return &___chunked_13; }
	inline void set_chunked_13(bool value)
	{
		___chunked_13 = value;
	}

	inline static int32_t get_offset_of_context_14() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t34AC9D467928315CC4285B8A14517C6D5564224C, ___context_14)); }
	inline HttpListenerContext_t438DA3609B8D775884622DF392BD7D72C651FCFA * get_context_14() const { return ___context_14; }
	inline HttpListenerContext_t438DA3609B8D775884622DF392BD7D72C651FCFA ** get_address_of_context_14() { return &___context_14; }
	inline void set_context_14(HttpListenerContext_t438DA3609B8D775884622DF392BD7D72C651FCFA * value)
	{
		___context_14 = value;
		Il2CppCodeGenWriteBarrier((&___context_14), value);
	}

	inline static int32_t get_offset_of_HeadersSent_15() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t34AC9D467928315CC4285B8A14517C6D5564224C, ___HeadersSent_15)); }
	inline bool get_HeadersSent_15() const { return ___HeadersSent_15; }
	inline bool* get_address_of_HeadersSent_15() { return &___HeadersSent_15; }
	inline void set_HeadersSent_15(bool value)
	{
		___HeadersSent_15 = value;
	}

	inline static int32_t get_offset_of_headers_lock_16() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t34AC9D467928315CC4285B8A14517C6D5564224C, ___headers_lock_16)); }
	inline RuntimeObject * get_headers_lock_16() const { return ___headers_lock_16; }
	inline RuntimeObject ** get_address_of_headers_lock_16() { return &___headers_lock_16; }
	inline void set_headers_lock_16(RuntimeObject * value)
	{
		___headers_lock_16 = value;
		Il2CppCodeGenWriteBarrier((&___headers_lock_16), value);
	}

	inline static int32_t get_offset_of_force_close_chunked_17() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t34AC9D467928315CC4285B8A14517C6D5564224C, ___force_close_chunked_17)); }
	inline bool get_force_close_chunked_17() const { return ___force_close_chunked_17; }
	inline bool* get_address_of_force_close_chunked_17() { return &___force_close_chunked_17; }
	inline void set_force_close_chunked_17(bool value)
	{
		___force_close_chunked_17 = value;
	}
};

struct HttpListenerResponse_t34AC9D467928315CC4285B8A14517C6D5564224C_StaticFields
{
public:
	// System.String System.Net.HttpListenerResponse::tspecials
	String_t* ___tspecials_18;

public:
	inline static int32_t get_offset_of_tspecials_18() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t34AC9D467928315CC4285B8A14517C6D5564224C_StaticFields, ___tspecials_18)); }
	inline String_t* get_tspecials_18() const { return ___tspecials_18; }
	inline String_t** get_address_of_tspecials_18() { return &___tspecials_18; }
	inline void set_tspecials_18(String_t* value)
	{
		___tspecials_18 = value;
		Il2CppCodeGenWriteBarrier((&___tspecials_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPLISTENERRESPONSE_T34AC9D467928315CC4285B8A14517C6D5564224C_H
#ifndef HTTPLISTENERTIMEOUTMANAGER_T081C4ACA445B361CDF394A18CDD6171E1110C3EA_H
#define HTTPLISTENERTIMEOUTMANAGER_T081C4ACA445B361CDF394A18CDD6171E1110C3EA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HttpListenerTimeoutManager
struct  HttpListenerTimeoutManager_t081C4ACA445B361CDF394A18CDD6171E1110C3EA  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPLISTENERTIMEOUTMANAGER_T081C4ACA445B361CDF394A18CDD6171E1110C3EA_H
#ifndef HTTPREQUESTCREATOR_TE16C19B09EAACE12BEBA0427796314523601EB1D_H
#define HTTPREQUESTCREATOR_TE16C19B09EAACE12BEBA0427796314523601EB1D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HttpRequestCreator
struct  HttpRequestCreator_tE16C19B09EAACE12BEBA0427796314523601EB1D  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPREQUESTCREATOR_TE16C19B09EAACE12BEBA0427796314523601EB1D_H
#ifndef HTTPSTREAMASYNCRESULT_T3CCE017E175E89575780F29152E61D6DE5D10271_H
#define HTTPSTREAMASYNCRESULT_T3CCE017E175E89575780F29152E61D6DE5D10271_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HttpStreamAsyncResult
struct  HttpStreamAsyncResult_t3CCE017E175E89575780F29152E61D6DE5D10271  : public RuntimeObject
{
public:
	// System.Object System.Net.HttpStreamAsyncResult::locker
	RuntimeObject * ___locker_0;
	// System.Threading.ManualResetEvent System.Net.HttpStreamAsyncResult::handle
	ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * ___handle_1;
	// System.Boolean System.Net.HttpStreamAsyncResult::completed
	bool ___completed_2;
	// System.Byte[] System.Net.HttpStreamAsyncResult::Buffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___Buffer_3;
	// System.Int32 System.Net.HttpStreamAsyncResult::Offset
	int32_t ___Offset_4;
	// System.Int32 System.Net.HttpStreamAsyncResult::Count
	int32_t ___Count_5;
	// System.AsyncCallback System.Net.HttpStreamAsyncResult::Callback
	AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___Callback_6;
	// System.Object System.Net.HttpStreamAsyncResult::State
	RuntimeObject * ___State_7;
	// System.Int32 System.Net.HttpStreamAsyncResult::SynchRead
	int32_t ___SynchRead_8;
	// System.Exception System.Net.HttpStreamAsyncResult::Error
	Exception_t * ___Error_9;

public:
	inline static int32_t get_offset_of_locker_0() { return static_cast<int32_t>(offsetof(HttpStreamAsyncResult_t3CCE017E175E89575780F29152E61D6DE5D10271, ___locker_0)); }
	inline RuntimeObject * get_locker_0() const { return ___locker_0; }
	inline RuntimeObject ** get_address_of_locker_0() { return &___locker_0; }
	inline void set_locker_0(RuntimeObject * value)
	{
		___locker_0 = value;
		Il2CppCodeGenWriteBarrier((&___locker_0), value);
	}

	inline static int32_t get_offset_of_handle_1() { return static_cast<int32_t>(offsetof(HttpStreamAsyncResult_t3CCE017E175E89575780F29152E61D6DE5D10271, ___handle_1)); }
	inline ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * get_handle_1() const { return ___handle_1; }
	inline ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 ** get_address_of_handle_1() { return &___handle_1; }
	inline void set_handle_1(ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * value)
	{
		___handle_1 = value;
		Il2CppCodeGenWriteBarrier((&___handle_1), value);
	}

	inline static int32_t get_offset_of_completed_2() { return static_cast<int32_t>(offsetof(HttpStreamAsyncResult_t3CCE017E175E89575780F29152E61D6DE5D10271, ___completed_2)); }
	inline bool get_completed_2() const { return ___completed_2; }
	inline bool* get_address_of_completed_2() { return &___completed_2; }
	inline void set_completed_2(bool value)
	{
		___completed_2 = value;
	}

	inline static int32_t get_offset_of_Buffer_3() { return static_cast<int32_t>(offsetof(HttpStreamAsyncResult_t3CCE017E175E89575780F29152E61D6DE5D10271, ___Buffer_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_Buffer_3() const { return ___Buffer_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_Buffer_3() { return &___Buffer_3; }
	inline void set_Buffer_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___Buffer_3 = value;
		Il2CppCodeGenWriteBarrier((&___Buffer_3), value);
	}

	inline static int32_t get_offset_of_Offset_4() { return static_cast<int32_t>(offsetof(HttpStreamAsyncResult_t3CCE017E175E89575780F29152E61D6DE5D10271, ___Offset_4)); }
	inline int32_t get_Offset_4() const { return ___Offset_4; }
	inline int32_t* get_address_of_Offset_4() { return &___Offset_4; }
	inline void set_Offset_4(int32_t value)
	{
		___Offset_4 = value;
	}

	inline static int32_t get_offset_of_Count_5() { return static_cast<int32_t>(offsetof(HttpStreamAsyncResult_t3CCE017E175E89575780F29152E61D6DE5D10271, ___Count_5)); }
	inline int32_t get_Count_5() const { return ___Count_5; }
	inline int32_t* get_address_of_Count_5() { return &___Count_5; }
	inline void set_Count_5(int32_t value)
	{
		___Count_5 = value;
	}

	inline static int32_t get_offset_of_Callback_6() { return static_cast<int32_t>(offsetof(HttpStreamAsyncResult_t3CCE017E175E89575780F29152E61D6DE5D10271, ___Callback_6)); }
	inline AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * get_Callback_6() const { return ___Callback_6; }
	inline AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 ** get_address_of_Callback_6() { return &___Callback_6; }
	inline void set_Callback_6(AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * value)
	{
		___Callback_6 = value;
		Il2CppCodeGenWriteBarrier((&___Callback_6), value);
	}

	inline static int32_t get_offset_of_State_7() { return static_cast<int32_t>(offsetof(HttpStreamAsyncResult_t3CCE017E175E89575780F29152E61D6DE5D10271, ___State_7)); }
	inline RuntimeObject * get_State_7() const { return ___State_7; }
	inline RuntimeObject ** get_address_of_State_7() { return &___State_7; }
	inline void set_State_7(RuntimeObject * value)
	{
		___State_7 = value;
		Il2CppCodeGenWriteBarrier((&___State_7), value);
	}

	inline static int32_t get_offset_of_SynchRead_8() { return static_cast<int32_t>(offsetof(HttpStreamAsyncResult_t3CCE017E175E89575780F29152E61D6DE5D10271, ___SynchRead_8)); }
	inline int32_t get_SynchRead_8() const { return ___SynchRead_8; }
	inline int32_t* get_address_of_SynchRead_8() { return &___SynchRead_8; }
	inline void set_SynchRead_8(int32_t value)
	{
		___SynchRead_8 = value;
	}

	inline static int32_t get_offset_of_Error_9() { return static_cast<int32_t>(offsetof(HttpStreamAsyncResult_t3CCE017E175E89575780F29152E61D6DE5D10271, ___Error_9)); }
	inline Exception_t * get_Error_9() const { return ___Error_9; }
	inline Exception_t ** get_address_of_Error_9() { return &___Error_9; }
	inline void set_Error_9(Exception_t * value)
	{
		___Error_9 = value;
		Il2CppCodeGenWriteBarrier((&___Error_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPSTREAMASYNCRESULT_T3CCE017E175E89575780F29152E61D6DE5D10271_H
#ifndef U3CU3EC__DISPLAYCLASS238_0_T772E96E52BE401D5422C8A540FC1B812F2D9B87B_H
#define U3CU3EC__DISPLAYCLASS238_0_T772E96E52BE401D5422C8A540FC1B812F2D9B87B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HttpWebRequest_<>c__DisplayClass238_0
struct  U3CU3Ec__DisplayClass238_0_t772E96E52BE401D5422C8A540FC1B812F2D9B87B  : public RuntimeObject
{
public:
	// System.Net.WebAsyncResult System.Net.HttpWebRequest_<>c__DisplayClass238_0::aread
	WebAsyncResult_tF700444B9ABA86C7CADBFA7B99DEC52D9FBD87EE * ___aread_0;
	// System.Net.HttpWebRequest System.Net.HttpWebRequest_<>c__DisplayClass238_0::<>4__this
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_aread_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass238_0_t772E96E52BE401D5422C8A540FC1B812F2D9B87B, ___aread_0)); }
	inline WebAsyncResult_tF700444B9ABA86C7CADBFA7B99DEC52D9FBD87EE * get_aread_0() const { return ___aread_0; }
	inline WebAsyncResult_tF700444B9ABA86C7CADBFA7B99DEC52D9FBD87EE ** get_address_of_aread_0() { return &___aread_0; }
	inline void set_aread_0(WebAsyncResult_tF700444B9ABA86C7CADBFA7B99DEC52D9FBD87EE * value)
	{
		___aread_0 = value;
		Il2CppCodeGenWriteBarrier((&___aread_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass238_0_t772E96E52BE401D5422C8A540FC1B812F2D9B87B, ___U3CU3E4__this_1)); }
	inline HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS238_0_T772E96E52BE401D5422C8A540FC1B812F2D9B87B_H
#ifndef LISTENERASYNCRESULT_T0305382DE30FE5EF4FAD66545E460CD4BDDEA12F_H
#define LISTENERASYNCRESULT_T0305382DE30FE5EF4FAD66545E460CD4BDDEA12F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.ListenerAsyncResult
struct  ListenerAsyncResult_t0305382DE30FE5EF4FAD66545E460CD4BDDEA12F  : public RuntimeObject
{
public:
	// System.Threading.ManualResetEvent System.Net.ListenerAsyncResult::handle
	ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * ___handle_0;
	// System.Boolean System.Net.ListenerAsyncResult::synch
	bool ___synch_1;
	// System.Boolean System.Net.ListenerAsyncResult::completed
	bool ___completed_2;
	// System.AsyncCallback System.Net.ListenerAsyncResult::cb
	AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___cb_3;
	// System.Object System.Net.ListenerAsyncResult::state
	RuntimeObject * ___state_4;
	// System.Exception System.Net.ListenerAsyncResult::exception
	Exception_t * ___exception_5;
	// System.Net.HttpListenerContext System.Net.ListenerAsyncResult::context
	HttpListenerContext_t438DA3609B8D775884622DF392BD7D72C651FCFA * ___context_6;
	// System.Object System.Net.ListenerAsyncResult::locker
	RuntimeObject * ___locker_7;
	// System.Net.ListenerAsyncResult System.Net.ListenerAsyncResult::forward
	ListenerAsyncResult_t0305382DE30FE5EF4FAD66545E460CD4BDDEA12F * ___forward_8;
	// System.Boolean System.Net.ListenerAsyncResult::EndCalled
	bool ___EndCalled_9;
	// System.Boolean System.Net.ListenerAsyncResult::InGet
	bool ___InGet_10;

public:
	inline static int32_t get_offset_of_handle_0() { return static_cast<int32_t>(offsetof(ListenerAsyncResult_t0305382DE30FE5EF4FAD66545E460CD4BDDEA12F, ___handle_0)); }
	inline ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * get_handle_0() const { return ___handle_0; }
	inline ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 ** get_address_of_handle_0() { return &___handle_0; }
	inline void set_handle_0(ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * value)
	{
		___handle_0 = value;
		Il2CppCodeGenWriteBarrier((&___handle_0), value);
	}

	inline static int32_t get_offset_of_synch_1() { return static_cast<int32_t>(offsetof(ListenerAsyncResult_t0305382DE30FE5EF4FAD66545E460CD4BDDEA12F, ___synch_1)); }
	inline bool get_synch_1() const { return ___synch_1; }
	inline bool* get_address_of_synch_1() { return &___synch_1; }
	inline void set_synch_1(bool value)
	{
		___synch_1 = value;
	}

	inline static int32_t get_offset_of_completed_2() { return static_cast<int32_t>(offsetof(ListenerAsyncResult_t0305382DE30FE5EF4FAD66545E460CD4BDDEA12F, ___completed_2)); }
	inline bool get_completed_2() const { return ___completed_2; }
	inline bool* get_address_of_completed_2() { return &___completed_2; }
	inline void set_completed_2(bool value)
	{
		___completed_2 = value;
	}

	inline static int32_t get_offset_of_cb_3() { return static_cast<int32_t>(offsetof(ListenerAsyncResult_t0305382DE30FE5EF4FAD66545E460CD4BDDEA12F, ___cb_3)); }
	inline AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * get_cb_3() const { return ___cb_3; }
	inline AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 ** get_address_of_cb_3() { return &___cb_3; }
	inline void set_cb_3(AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * value)
	{
		___cb_3 = value;
		Il2CppCodeGenWriteBarrier((&___cb_3), value);
	}

	inline static int32_t get_offset_of_state_4() { return static_cast<int32_t>(offsetof(ListenerAsyncResult_t0305382DE30FE5EF4FAD66545E460CD4BDDEA12F, ___state_4)); }
	inline RuntimeObject * get_state_4() const { return ___state_4; }
	inline RuntimeObject ** get_address_of_state_4() { return &___state_4; }
	inline void set_state_4(RuntimeObject * value)
	{
		___state_4 = value;
		Il2CppCodeGenWriteBarrier((&___state_4), value);
	}

	inline static int32_t get_offset_of_exception_5() { return static_cast<int32_t>(offsetof(ListenerAsyncResult_t0305382DE30FE5EF4FAD66545E460CD4BDDEA12F, ___exception_5)); }
	inline Exception_t * get_exception_5() const { return ___exception_5; }
	inline Exception_t ** get_address_of_exception_5() { return &___exception_5; }
	inline void set_exception_5(Exception_t * value)
	{
		___exception_5 = value;
		Il2CppCodeGenWriteBarrier((&___exception_5), value);
	}

	inline static int32_t get_offset_of_context_6() { return static_cast<int32_t>(offsetof(ListenerAsyncResult_t0305382DE30FE5EF4FAD66545E460CD4BDDEA12F, ___context_6)); }
	inline HttpListenerContext_t438DA3609B8D775884622DF392BD7D72C651FCFA * get_context_6() const { return ___context_6; }
	inline HttpListenerContext_t438DA3609B8D775884622DF392BD7D72C651FCFA ** get_address_of_context_6() { return &___context_6; }
	inline void set_context_6(HttpListenerContext_t438DA3609B8D775884622DF392BD7D72C651FCFA * value)
	{
		___context_6 = value;
		Il2CppCodeGenWriteBarrier((&___context_6), value);
	}

	inline static int32_t get_offset_of_locker_7() { return static_cast<int32_t>(offsetof(ListenerAsyncResult_t0305382DE30FE5EF4FAD66545E460CD4BDDEA12F, ___locker_7)); }
	inline RuntimeObject * get_locker_7() const { return ___locker_7; }
	inline RuntimeObject ** get_address_of_locker_7() { return &___locker_7; }
	inline void set_locker_7(RuntimeObject * value)
	{
		___locker_7 = value;
		Il2CppCodeGenWriteBarrier((&___locker_7), value);
	}

	inline static int32_t get_offset_of_forward_8() { return static_cast<int32_t>(offsetof(ListenerAsyncResult_t0305382DE30FE5EF4FAD66545E460CD4BDDEA12F, ___forward_8)); }
	inline ListenerAsyncResult_t0305382DE30FE5EF4FAD66545E460CD4BDDEA12F * get_forward_8() const { return ___forward_8; }
	inline ListenerAsyncResult_t0305382DE30FE5EF4FAD66545E460CD4BDDEA12F ** get_address_of_forward_8() { return &___forward_8; }
	inline void set_forward_8(ListenerAsyncResult_t0305382DE30FE5EF4FAD66545E460CD4BDDEA12F * value)
	{
		___forward_8 = value;
		Il2CppCodeGenWriteBarrier((&___forward_8), value);
	}

	inline static int32_t get_offset_of_EndCalled_9() { return static_cast<int32_t>(offsetof(ListenerAsyncResult_t0305382DE30FE5EF4FAD66545E460CD4BDDEA12F, ___EndCalled_9)); }
	inline bool get_EndCalled_9() const { return ___EndCalled_9; }
	inline bool* get_address_of_EndCalled_9() { return &___EndCalled_9; }
	inline void set_EndCalled_9(bool value)
	{
		___EndCalled_9 = value;
	}

	inline static int32_t get_offset_of_InGet_10() { return static_cast<int32_t>(offsetof(ListenerAsyncResult_t0305382DE30FE5EF4FAD66545E460CD4BDDEA12F, ___InGet_10)); }
	inline bool get_InGet_10() const { return ___InGet_10; }
	inline bool* get_address_of_InGet_10() { return &___InGet_10; }
	inline void set_InGet_10(bool value)
	{
		___InGet_10 = value;
	}
};

struct ListenerAsyncResult_t0305382DE30FE5EF4FAD66545E460CD4BDDEA12F_StaticFields
{
public:
	// System.Threading.WaitCallback System.Net.ListenerAsyncResult::InvokeCB
	WaitCallback_t61C5F053CAC7A7FE923208EFA060693D7997B4EC * ___InvokeCB_11;

public:
	inline static int32_t get_offset_of_InvokeCB_11() { return static_cast<int32_t>(offsetof(ListenerAsyncResult_t0305382DE30FE5EF4FAD66545E460CD4BDDEA12F_StaticFields, ___InvokeCB_11)); }
	inline WaitCallback_t61C5F053CAC7A7FE923208EFA060693D7997B4EC * get_InvokeCB_11() const { return ___InvokeCB_11; }
	inline WaitCallback_t61C5F053CAC7A7FE923208EFA060693D7997B4EC ** get_address_of_InvokeCB_11() { return &___InvokeCB_11; }
	inline void set_InvokeCB_11(WaitCallback_t61C5F053CAC7A7FE923208EFA060693D7997B4EC * value)
	{
		___InvokeCB_11 = value;
		Il2CppCodeGenWriteBarrier((&___InvokeCB_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LISTENERASYNCRESULT_T0305382DE30FE5EF4FAD66545E460CD4BDDEA12F_H
#ifndef LISTENERPREFIX_T7E983ACD2D31FE4DC42DE02AE82EC24734DC57D7_H
#define LISTENERPREFIX_T7E983ACD2D31FE4DC42DE02AE82EC24734DC57D7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.ListenerPrefix
struct  ListenerPrefix_t7E983ACD2D31FE4DC42DE02AE82EC24734DC57D7  : public RuntimeObject
{
public:
	// System.String System.Net.ListenerPrefix::original
	String_t* ___original_0;
	// System.String System.Net.ListenerPrefix::host
	String_t* ___host_1;
	// System.UInt16 System.Net.ListenerPrefix::port
	uint16_t ___port_2;
	// System.String System.Net.ListenerPrefix::path
	String_t* ___path_3;
	// System.Boolean System.Net.ListenerPrefix::secure
	bool ___secure_4;
	// System.Net.IPAddress[] System.Net.ListenerPrefix::addresses
	IPAddressU5BU5D_t7F25C4C038C43BFDA8EA84969112E82BADC38BC3* ___addresses_5;
	// System.Net.HttpListener System.Net.ListenerPrefix::Listener
	HttpListener_tAAE9DCF34EB3C09285BF833F390C8CC61C7D247E * ___Listener_6;

public:
	inline static int32_t get_offset_of_original_0() { return static_cast<int32_t>(offsetof(ListenerPrefix_t7E983ACD2D31FE4DC42DE02AE82EC24734DC57D7, ___original_0)); }
	inline String_t* get_original_0() const { return ___original_0; }
	inline String_t** get_address_of_original_0() { return &___original_0; }
	inline void set_original_0(String_t* value)
	{
		___original_0 = value;
		Il2CppCodeGenWriteBarrier((&___original_0), value);
	}

	inline static int32_t get_offset_of_host_1() { return static_cast<int32_t>(offsetof(ListenerPrefix_t7E983ACD2D31FE4DC42DE02AE82EC24734DC57D7, ___host_1)); }
	inline String_t* get_host_1() const { return ___host_1; }
	inline String_t** get_address_of_host_1() { return &___host_1; }
	inline void set_host_1(String_t* value)
	{
		___host_1 = value;
		Il2CppCodeGenWriteBarrier((&___host_1), value);
	}

	inline static int32_t get_offset_of_port_2() { return static_cast<int32_t>(offsetof(ListenerPrefix_t7E983ACD2D31FE4DC42DE02AE82EC24734DC57D7, ___port_2)); }
	inline uint16_t get_port_2() const { return ___port_2; }
	inline uint16_t* get_address_of_port_2() { return &___port_2; }
	inline void set_port_2(uint16_t value)
	{
		___port_2 = value;
	}

	inline static int32_t get_offset_of_path_3() { return static_cast<int32_t>(offsetof(ListenerPrefix_t7E983ACD2D31FE4DC42DE02AE82EC24734DC57D7, ___path_3)); }
	inline String_t* get_path_3() const { return ___path_3; }
	inline String_t** get_address_of_path_3() { return &___path_3; }
	inline void set_path_3(String_t* value)
	{
		___path_3 = value;
		Il2CppCodeGenWriteBarrier((&___path_3), value);
	}

	inline static int32_t get_offset_of_secure_4() { return static_cast<int32_t>(offsetof(ListenerPrefix_t7E983ACD2D31FE4DC42DE02AE82EC24734DC57D7, ___secure_4)); }
	inline bool get_secure_4() const { return ___secure_4; }
	inline bool* get_address_of_secure_4() { return &___secure_4; }
	inline void set_secure_4(bool value)
	{
		___secure_4 = value;
	}

	inline static int32_t get_offset_of_addresses_5() { return static_cast<int32_t>(offsetof(ListenerPrefix_t7E983ACD2D31FE4DC42DE02AE82EC24734DC57D7, ___addresses_5)); }
	inline IPAddressU5BU5D_t7F25C4C038C43BFDA8EA84969112E82BADC38BC3* get_addresses_5() const { return ___addresses_5; }
	inline IPAddressU5BU5D_t7F25C4C038C43BFDA8EA84969112E82BADC38BC3** get_address_of_addresses_5() { return &___addresses_5; }
	inline void set_addresses_5(IPAddressU5BU5D_t7F25C4C038C43BFDA8EA84969112E82BADC38BC3* value)
	{
		___addresses_5 = value;
		Il2CppCodeGenWriteBarrier((&___addresses_5), value);
	}

	inline static int32_t get_offset_of_Listener_6() { return static_cast<int32_t>(offsetof(ListenerPrefix_t7E983ACD2D31FE4DC42DE02AE82EC24734DC57D7, ___Listener_6)); }
	inline HttpListener_tAAE9DCF34EB3C09285BF833F390C8CC61C7D247E * get_Listener_6() const { return ___Listener_6; }
	inline HttpListener_tAAE9DCF34EB3C09285BF833F390C8CC61C7D247E ** get_address_of_Listener_6() { return &___Listener_6; }
	inline void set_Listener_6(HttpListener_tAAE9DCF34EB3C09285BF833F390C8CC61C7D247E * value)
	{
		___Listener_6 = value;
		Il2CppCodeGenWriteBarrier((&___Listener_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LISTENERPREFIX_T7E983ACD2D31FE4DC42DE02AE82EC24734DC57D7_H
#ifndef ENCODEDSTREAMFACTORY_TE92297883C287D4B1B6D0FF021A496B991095BDC_H
#define ENCODEDSTREAMFACTORY_TE92297883C287D4B1B6D0FF021A496B991095BDC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Mime.EncodedStreamFactory
struct  EncodedStreamFactory_tE92297883C287D4B1B6D0FF021A496B991095BDC  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCODEDSTREAMFACTORY_TE92297883C287D4B1B6D0FF021A496B991095BDC_H
#ifndef WRITESTATEINFOBASE_T029DDB6613907E0AEF10D611B6E48422EC7778FD_H
#define WRITESTATEINFOBASE_T029DDB6613907E0AEF10D611B6E48422EC7778FD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Mime.WriteStateInfoBase
struct  WriteStateInfoBase_t029DDB6613907E0AEF10D611B6E48422EC7778FD  : public RuntimeObject
{
public:
	// System.Byte[] System.Net.Mime.WriteStateInfoBase::_header
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____header_0;
	// System.Byte[] System.Net.Mime.WriteStateInfoBase::_footer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____footer_1;
	// System.Int32 System.Net.Mime.WriteStateInfoBase::_maxLineLength
	int32_t ____maxLineLength_2;
	// System.Byte[] System.Net.Mime.WriteStateInfoBase::buffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___buffer_3;
	// System.Int32 System.Net.Mime.WriteStateInfoBase::_currentLineLength
	int32_t ____currentLineLength_4;
	// System.Int32 System.Net.Mime.WriteStateInfoBase::_currentBufferUsed
	int32_t ____currentBufferUsed_5;

public:
	inline static int32_t get_offset_of__header_0() { return static_cast<int32_t>(offsetof(WriteStateInfoBase_t029DDB6613907E0AEF10D611B6E48422EC7778FD, ____header_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__header_0() const { return ____header_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__header_0() { return &____header_0; }
	inline void set__header_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____header_0 = value;
		Il2CppCodeGenWriteBarrier((&____header_0), value);
	}

	inline static int32_t get_offset_of__footer_1() { return static_cast<int32_t>(offsetof(WriteStateInfoBase_t029DDB6613907E0AEF10D611B6E48422EC7778FD, ____footer_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__footer_1() const { return ____footer_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__footer_1() { return &____footer_1; }
	inline void set__footer_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____footer_1 = value;
		Il2CppCodeGenWriteBarrier((&____footer_1), value);
	}

	inline static int32_t get_offset_of__maxLineLength_2() { return static_cast<int32_t>(offsetof(WriteStateInfoBase_t029DDB6613907E0AEF10D611B6E48422EC7778FD, ____maxLineLength_2)); }
	inline int32_t get__maxLineLength_2() const { return ____maxLineLength_2; }
	inline int32_t* get_address_of__maxLineLength_2() { return &____maxLineLength_2; }
	inline void set__maxLineLength_2(int32_t value)
	{
		____maxLineLength_2 = value;
	}

	inline static int32_t get_offset_of_buffer_3() { return static_cast<int32_t>(offsetof(WriteStateInfoBase_t029DDB6613907E0AEF10D611B6E48422EC7778FD, ___buffer_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_buffer_3() const { return ___buffer_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_buffer_3() { return &___buffer_3; }
	inline void set_buffer_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___buffer_3 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_3), value);
	}

	inline static int32_t get_offset_of__currentLineLength_4() { return static_cast<int32_t>(offsetof(WriteStateInfoBase_t029DDB6613907E0AEF10D611B6E48422EC7778FD, ____currentLineLength_4)); }
	inline int32_t get__currentLineLength_4() const { return ____currentLineLength_4; }
	inline int32_t* get_address_of__currentLineLength_4() { return &____currentLineLength_4; }
	inline void set__currentLineLength_4(int32_t value)
	{
		____currentLineLength_4 = value;
	}

	inline static int32_t get_offset_of__currentBufferUsed_5() { return static_cast<int32_t>(offsetof(WriteStateInfoBase_t029DDB6613907E0AEF10D611B6E48422EC7778FD, ____currentBufferUsed_5)); }
	inline int32_t get__currentBufferUsed_5() const { return ____currentBufferUsed_5; }
	inline int32_t* get_address_of__currentBufferUsed_5() { return &____currentBufferUsed_5; }
	inline void set__currentBufferUsed_5(int32_t value)
	{
		____currentBufferUsed_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WRITESTATEINFOBASE_T029DDB6613907E0AEF10D611B6E48422EC7778FD_H
#ifndef CHUNK_TB367D5D805924164D239F587BC848162536B8AB7_H
#define CHUNK_TB367D5D805924164D239F587BC848162536B8AB7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.MonoChunkStream_Chunk
struct  Chunk_tB367D5D805924164D239F587BC848162536B8AB7  : public RuntimeObject
{
public:
	// System.Byte[] System.Net.MonoChunkStream_Chunk::Bytes
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___Bytes_0;
	// System.Int32 System.Net.MonoChunkStream_Chunk::Offset
	int32_t ___Offset_1;

public:
	inline static int32_t get_offset_of_Bytes_0() { return static_cast<int32_t>(offsetof(Chunk_tB367D5D805924164D239F587BC848162536B8AB7, ___Bytes_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_Bytes_0() const { return ___Bytes_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_Bytes_0() { return &___Bytes_0; }
	inline void set_Bytes_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___Bytes_0 = value;
		Il2CppCodeGenWriteBarrier((&___Bytes_0), value);
	}

	inline static int32_t get_offset_of_Offset_1() { return static_cast<int32_t>(offsetof(Chunk_tB367D5D805924164D239F587BC848162536B8AB7, ___Offset_1)); }
	inline int32_t get_Offset_1() const { return ___Offset_1; }
	inline int32_t* get_address_of_Offset_1() { return &___Offset_1; }
	inline void set_Offset_1(int32_t value)
	{
		___Offset_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHUNK_TB367D5D805924164D239F587BC848162536B8AB7_H
#ifndef MONOHTTPDATE_T81A2164242F9BBCAD77B26859F0D381E133CC9E1_H
#define MONOHTTPDATE_T81A2164242F9BBCAD77B26859F0D381E133CC9E1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.MonoHttpDate
struct  MonoHttpDate_t81A2164242F9BBCAD77B26859F0D381E133CC9E1  : public RuntimeObject
{
public:

public:
};

struct MonoHttpDate_t81A2164242F9BBCAD77B26859F0D381E133CC9E1_StaticFields
{
public:
	// System.String System.Net.MonoHttpDate::rfc1123_date
	String_t* ___rfc1123_date_0;
	// System.String System.Net.MonoHttpDate::rfc850_date
	String_t* ___rfc850_date_1;
	// System.String System.Net.MonoHttpDate::asctime_date
	String_t* ___asctime_date_2;
	// System.String[] System.Net.MonoHttpDate::formats
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___formats_3;

public:
	inline static int32_t get_offset_of_rfc1123_date_0() { return static_cast<int32_t>(offsetof(MonoHttpDate_t81A2164242F9BBCAD77B26859F0D381E133CC9E1_StaticFields, ___rfc1123_date_0)); }
	inline String_t* get_rfc1123_date_0() const { return ___rfc1123_date_0; }
	inline String_t** get_address_of_rfc1123_date_0() { return &___rfc1123_date_0; }
	inline void set_rfc1123_date_0(String_t* value)
	{
		___rfc1123_date_0 = value;
		Il2CppCodeGenWriteBarrier((&___rfc1123_date_0), value);
	}

	inline static int32_t get_offset_of_rfc850_date_1() { return static_cast<int32_t>(offsetof(MonoHttpDate_t81A2164242F9BBCAD77B26859F0D381E133CC9E1_StaticFields, ___rfc850_date_1)); }
	inline String_t* get_rfc850_date_1() const { return ___rfc850_date_1; }
	inline String_t** get_address_of_rfc850_date_1() { return &___rfc850_date_1; }
	inline void set_rfc850_date_1(String_t* value)
	{
		___rfc850_date_1 = value;
		Il2CppCodeGenWriteBarrier((&___rfc850_date_1), value);
	}

	inline static int32_t get_offset_of_asctime_date_2() { return static_cast<int32_t>(offsetof(MonoHttpDate_t81A2164242F9BBCAD77B26859F0D381E133CC9E1_StaticFields, ___asctime_date_2)); }
	inline String_t* get_asctime_date_2() const { return ___asctime_date_2; }
	inline String_t** get_address_of_asctime_date_2() { return &___asctime_date_2; }
	inline void set_asctime_date_2(String_t* value)
	{
		___asctime_date_2 = value;
		Il2CppCodeGenWriteBarrier((&___asctime_date_2), value);
	}

	inline static int32_t get_offset_of_formats_3() { return static_cast<int32_t>(offsetof(MonoHttpDate_t81A2164242F9BBCAD77B26859F0D381E133CC9E1_StaticFields, ___formats_3)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_formats_3() const { return ___formats_3; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_formats_3() { return &___formats_3; }
	inline void set_formats_3(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___formats_3 = value;
		Il2CppCodeGenWriteBarrier((&___formats_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOHTTPDATE_T81A2164242F9BBCAD77B26859F0D381E133CC9E1_H
#ifndef NETCONFIG_T1EDDC92F13548882CC6D0D0AA40C174D21878150_H
#define NETCONFIG_T1EDDC92F13548882CC6D0D0AA40C174D21878150_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.NetConfig
struct  NetConfig_t1EDDC92F13548882CC6D0D0AA40C174D21878150  : public RuntimeObject
{
public:
	// System.Boolean System.Net.NetConfig::ipv6Enabled
	bool ___ipv6Enabled_0;
	// System.Int32 System.Net.NetConfig::MaxResponseHeadersLength
	int32_t ___MaxResponseHeadersLength_1;

public:
	inline static int32_t get_offset_of_ipv6Enabled_0() { return static_cast<int32_t>(offsetof(NetConfig_t1EDDC92F13548882CC6D0D0AA40C174D21878150, ___ipv6Enabled_0)); }
	inline bool get_ipv6Enabled_0() const { return ___ipv6Enabled_0; }
	inline bool* get_address_of_ipv6Enabled_0() { return &___ipv6Enabled_0; }
	inline void set_ipv6Enabled_0(bool value)
	{
		___ipv6Enabled_0 = value;
	}

	inline static int32_t get_offset_of_MaxResponseHeadersLength_1() { return static_cast<int32_t>(offsetof(NetConfig_t1EDDC92F13548882CC6D0D0AA40C174D21878150, ___MaxResponseHeadersLength_1)); }
	inline int32_t get_MaxResponseHeadersLength_1() const { return ___MaxResponseHeadersLength_1; }
	inline int32_t* get_address_of_MaxResponseHeadersLength_1() { return &___MaxResponseHeadersLength_1; }
	inline void set_MaxResponseHeadersLength_1(int32_t value)
	{
		___MaxResponseHeadersLength_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETCONFIG_T1EDDC92F13548882CC6D0D0AA40C174D21878150_H
#ifndef NTLMCLIENT_TBCB5B9D27D758545CF0BB6490F1A5CE77F65B204_H
#define NTLMCLIENT_TBCB5B9D27D758545CF0BB6490F1A5CE77F65B204_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.NtlmClient
struct  NtlmClient_tBCB5B9D27D758545CF0BB6490F1A5CE77F65B204  : public RuntimeObject
{
public:
	// System.Net.IAuthenticationModule System.Net.NtlmClient::authObject
	RuntimeObject* ___authObject_0;

public:
	inline static int32_t get_offset_of_authObject_0() { return static_cast<int32_t>(offsetof(NtlmClient_tBCB5B9D27D758545CF0BB6490F1A5CE77F65B204, ___authObject_0)); }
	inline RuntimeObject* get_authObject_0() const { return ___authObject_0; }
	inline RuntimeObject** get_address_of_authObject_0() { return &___authObject_0; }
	inline void set_authObject_0(RuntimeObject* value)
	{
		___authObject_0 = value;
		Il2CppCodeGenWriteBarrier((&___authObject_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NTLMCLIENT_TBCB5B9D27D758545CF0BB6490F1A5CE77F65B204_H
#ifndef SPKEY_T11DD7899D8DCC8D651D31028474C6CD456FD4D63_H
#define SPKEY_T11DD7899D8DCC8D651D31028474C6CD456FD4D63_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.ServicePointManager_SPKey
struct  SPKey_t11DD7899D8DCC8D651D31028474C6CD456FD4D63  : public RuntimeObject
{
public:
	// System.Uri System.Net.ServicePointManager_SPKey::uri
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___uri_0;
	// System.Uri System.Net.ServicePointManager_SPKey::proxy
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___proxy_1;
	// System.Boolean System.Net.ServicePointManager_SPKey::use_connect
	bool ___use_connect_2;

public:
	inline static int32_t get_offset_of_uri_0() { return static_cast<int32_t>(offsetof(SPKey_t11DD7899D8DCC8D651D31028474C6CD456FD4D63, ___uri_0)); }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * get_uri_0() const { return ___uri_0; }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E ** get_address_of_uri_0() { return &___uri_0; }
	inline void set_uri_0(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * value)
	{
		___uri_0 = value;
		Il2CppCodeGenWriteBarrier((&___uri_0), value);
	}

	inline static int32_t get_offset_of_proxy_1() { return static_cast<int32_t>(offsetof(SPKey_t11DD7899D8DCC8D651D31028474C6CD456FD4D63, ___proxy_1)); }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * get_proxy_1() const { return ___proxy_1; }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E ** get_address_of_proxy_1() { return &___proxy_1; }
	inline void set_proxy_1(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * value)
	{
		___proxy_1 = value;
		Il2CppCodeGenWriteBarrier((&___proxy_1), value);
	}

	inline static int32_t get_offset_of_use_connect_2() { return static_cast<int32_t>(offsetof(SPKey_t11DD7899D8DCC8D651D31028474C6CD456FD4D63, ___use_connect_2)); }
	inline bool get_use_connect_2() const { return ___use_connect_2; }
	inline bool* get_address_of_use_connect_2() { return &___use_connect_2; }
	inline void set_use_connect_2(bool value)
	{
		___use_connect_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPKEY_T11DD7899D8DCC8D651D31028474C6CD456FD4D63_H
#ifndef U3CU3EC__DISPLAYCLASS11_0_T8C200E7B255B4336993B38FAC7E931F8414EF122_H
#define U3CU3EC__DISPLAYCLASS11_0_T8C200E7B255B4336993B38FAC7E931F8414EF122_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.SimpleAsyncResult_<>c__DisplayClass11_0
struct  U3CU3Ec__DisplayClass11_0_t8C200E7B255B4336993B38FAC7E931F8414EF122  : public RuntimeObject
{
public:
	// System.Func`2<System.Net.SimpleAsyncResult,System.Boolean> System.Net.SimpleAsyncResult_<>c__DisplayClass11_0::func
	Func_2_tF6A6FE235E53230F712003180A1DBAF19C50FC61 * ___func_0;
	// System.Object System.Net.SimpleAsyncResult_<>c__DisplayClass11_0::locker
	RuntimeObject * ___locker_1;
	// System.Net.SimpleAsyncCallback System.Net.SimpleAsyncResult_<>c__DisplayClass11_0::callback
	SimpleAsyncCallback_t690665AFDCBDEBA379B6F4CD213CB4BB8570F83F * ___callback_2;

public:
	inline static int32_t get_offset_of_func_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass11_0_t8C200E7B255B4336993B38FAC7E931F8414EF122, ___func_0)); }
	inline Func_2_tF6A6FE235E53230F712003180A1DBAF19C50FC61 * get_func_0() const { return ___func_0; }
	inline Func_2_tF6A6FE235E53230F712003180A1DBAF19C50FC61 ** get_address_of_func_0() { return &___func_0; }
	inline void set_func_0(Func_2_tF6A6FE235E53230F712003180A1DBAF19C50FC61 * value)
	{
		___func_0 = value;
		Il2CppCodeGenWriteBarrier((&___func_0), value);
	}

	inline static int32_t get_offset_of_locker_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass11_0_t8C200E7B255B4336993B38FAC7E931F8414EF122, ___locker_1)); }
	inline RuntimeObject * get_locker_1() const { return ___locker_1; }
	inline RuntimeObject ** get_address_of_locker_1() { return &___locker_1; }
	inline void set_locker_1(RuntimeObject * value)
	{
		___locker_1 = value;
		Il2CppCodeGenWriteBarrier((&___locker_1), value);
	}

	inline static int32_t get_offset_of_callback_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass11_0_t8C200E7B255B4336993B38FAC7E931F8414EF122, ___callback_2)); }
	inline SimpleAsyncCallback_t690665AFDCBDEBA379B6F4CD213CB4BB8570F83F * get_callback_2() const { return ___callback_2; }
	inline SimpleAsyncCallback_t690665AFDCBDEBA379B6F4CD213CB4BB8570F83F ** get_address_of_callback_2() { return &___callback_2; }
	inline void set_callback_2(SimpleAsyncCallback_t690665AFDCBDEBA379B6F4CD213CB4BB8570F83F * value)
	{
		___callback_2 = value;
		Il2CppCodeGenWriteBarrier((&___callback_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS11_0_T8C200E7B255B4336993B38FAC7E931F8414EF122_H
#ifndef U3CU3EC__DISPLAYCLASS9_0_T47DEAC5ADFEA0940FAE3D1B7BCB60D05496BC8E3_H
#define U3CU3EC__DISPLAYCLASS9_0_T47DEAC5ADFEA0940FAE3D1B7BCB60D05496BC8E3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.SimpleAsyncResult_<>c__DisplayClass9_0
struct  U3CU3Ec__DisplayClass9_0_t47DEAC5ADFEA0940FAE3D1B7BCB60D05496BC8E3  : public RuntimeObject
{
public:
	// System.AsyncCallback System.Net.SimpleAsyncResult_<>c__DisplayClass9_0::cb
	AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___cb_0;
	// System.Net.SimpleAsyncResult System.Net.SimpleAsyncResult_<>c__DisplayClass9_0::<>4__this
	SimpleAsyncResult_tA572851810F8E279EE9E5378A6D9A538B1822FC6 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_cb_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass9_0_t47DEAC5ADFEA0940FAE3D1B7BCB60D05496BC8E3, ___cb_0)); }
	inline AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * get_cb_0() const { return ___cb_0; }
	inline AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 ** get_address_of_cb_0() { return &___cb_0; }
	inline void set_cb_0(AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * value)
	{
		___cb_0 = value;
		Il2CppCodeGenWriteBarrier((&___cb_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass9_0_t47DEAC5ADFEA0940FAE3D1B7BCB60D05496BC8E3, ___U3CU3E4__this_1)); }
	inline SimpleAsyncResult_tA572851810F8E279EE9E5378A6D9A538B1822FC6 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline SimpleAsyncResult_tA572851810F8E279EE9E5378A6D9A538B1822FC6 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(SimpleAsyncResult_tA572851810F8E279EE9E5378A6D9A538B1822FC6 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS9_0_T47DEAC5ADFEA0940FAE3D1B7BCB60D05496BC8E3_H
#ifndef LINGEROPTION_TC6A8E9C30F48D9C07C38B2730012ECA6067723C7_H
#define LINGEROPTION_TC6A8E9C30F48D9C07C38B2730012ECA6067723C7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Sockets.LingerOption
struct  LingerOption_tC6A8E9C30F48D9C07C38B2730012ECA6067723C7  : public RuntimeObject
{
public:
	// System.Boolean System.Net.Sockets.LingerOption::enabled
	bool ___enabled_0;
	// System.Int32 System.Net.Sockets.LingerOption::lingerTime
	int32_t ___lingerTime_1;

public:
	inline static int32_t get_offset_of_enabled_0() { return static_cast<int32_t>(offsetof(LingerOption_tC6A8E9C30F48D9C07C38B2730012ECA6067723C7, ___enabled_0)); }
	inline bool get_enabled_0() const { return ___enabled_0; }
	inline bool* get_address_of_enabled_0() { return &___enabled_0; }
	inline void set_enabled_0(bool value)
	{
		___enabled_0 = value;
	}

	inline static int32_t get_offset_of_lingerTime_1() { return static_cast<int32_t>(offsetof(LingerOption_tC6A8E9C30F48D9C07C38B2730012ECA6067723C7, ___lingerTime_1)); }
	inline int32_t get_lingerTime_1() const { return ___lingerTime_1; }
	inline int32_t* get_address_of_lingerTime_1() { return &___lingerTime_1; }
	inline void set_lingerTime_1(int32_t value)
	{
		___lingerTime_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINGEROPTION_TC6A8E9C30F48D9C07C38B2730012ECA6067723C7_H
#ifndef MULTICASTOPTION_TB21F96F80CEA6CF2F8C72A081C74375B1CEC5999_H
#define MULTICASTOPTION_TB21F96F80CEA6CF2F8C72A081C74375B1CEC5999_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Sockets.MulticastOption
struct  MulticastOption_tB21F96F80CEA6CF2F8C72A081C74375B1CEC5999  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTOPTION_TB21F96F80CEA6CF2F8C72A081C74375B1CEC5999_H
#ifndef U3CU3EC_T25DDEAF3ECC3E1ECBECC88BCA00EE02098BC3F7E_H
#define U3CU3EC_T25DDEAF3ECC3E1ECBECC88BCA00EE02098BC3F7E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Sockets.Socket_<>c
struct  U3CU3Ec_t25DDEAF3ECC3E1ECBECC88BCA00EE02098BC3F7E  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t25DDEAF3ECC3E1ECBECC88BCA00EE02098BC3F7E_StaticFields
{
public:
	// System.Net.Sockets.Socket_<>c System.Net.Sockets.Socket_<>c::<>9
	U3CU3Ec_t25DDEAF3ECC3E1ECBECC88BCA00EE02098BC3F7E * ___U3CU3E9_0;
	// System.IOAsyncCallback System.Net.Sockets.Socket_<>c::<>9__239_0
	IOAsyncCallback_tEAEB626A6DAC959F4C365B12136A80EE9AA17547 * ___U3CU3E9__239_0_1;
	// System.IOAsyncCallback System.Net.Sockets.Socket_<>c::<>9__241_0
	IOAsyncCallback_tEAEB626A6DAC959F4C365B12136A80EE9AA17547 * ___U3CU3E9__241_0_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t25DDEAF3ECC3E1ECBECC88BCA00EE02098BC3F7E_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t25DDEAF3ECC3E1ECBECC88BCA00EE02098BC3F7E * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t25DDEAF3ECC3E1ECBECC88BCA00EE02098BC3F7E ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t25DDEAF3ECC3E1ECBECC88BCA00EE02098BC3F7E * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__239_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t25DDEAF3ECC3E1ECBECC88BCA00EE02098BC3F7E_StaticFields, ___U3CU3E9__239_0_1)); }
	inline IOAsyncCallback_tEAEB626A6DAC959F4C365B12136A80EE9AA17547 * get_U3CU3E9__239_0_1() const { return ___U3CU3E9__239_0_1; }
	inline IOAsyncCallback_tEAEB626A6DAC959F4C365B12136A80EE9AA17547 ** get_address_of_U3CU3E9__239_0_1() { return &___U3CU3E9__239_0_1; }
	inline void set_U3CU3E9__239_0_1(IOAsyncCallback_tEAEB626A6DAC959F4C365B12136A80EE9AA17547 * value)
	{
		___U3CU3E9__239_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__239_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__241_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t25DDEAF3ECC3E1ECBECC88BCA00EE02098BC3F7E_StaticFields, ___U3CU3E9__241_0_2)); }
	inline IOAsyncCallback_tEAEB626A6DAC959F4C365B12136A80EE9AA17547 * get_U3CU3E9__241_0_2() const { return ___U3CU3E9__241_0_2; }
	inline IOAsyncCallback_tEAEB626A6DAC959F4C365B12136A80EE9AA17547 ** get_address_of_U3CU3E9__241_0_2() { return &___U3CU3E9__241_0_2; }
	inline void set_U3CU3E9__241_0_2(IOAsyncCallback_tEAEB626A6DAC959F4C365B12136A80EE9AA17547 * value)
	{
		___U3CU3E9__241_0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__241_0_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T25DDEAF3ECC3E1ECBECC88BCA00EE02098BC3F7E_H
#ifndef U3CU3EC__DISPLAYCLASS242_0_TA32CA02257AF703718D32CE05809EB89C1614997_H
#define U3CU3EC__DISPLAYCLASS242_0_TA32CA02257AF703718D32CE05809EB89C1614997_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Sockets.Socket_<>c__DisplayClass242_0
struct  U3CU3Ec__DisplayClass242_0_tA32CA02257AF703718D32CE05809EB89C1614997  : public RuntimeObject
{
public:
	// System.Int32 System.Net.Sockets.Socket_<>c__DisplayClass242_0::sent_so_far
	int32_t ___sent_so_far_0;

public:
	inline static int32_t get_offset_of_sent_so_far_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass242_0_tA32CA02257AF703718D32CE05809EB89C1614997, ___sent_so_far_0)); }
	inline int32_t get_sent_so_far_0() const { return ___sent_so_far_0; }
	inline int32_t* get_address_of_sent_so_far_0() { return &___sent_so_far_0; }
	inline void set_sent_so_far_0(int32_t value)
	{
		___sent_so_far_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS242_0_TA32CA02257AF703718D32CE05809EB89C1614997_H
#ifndef U3CU3EC_TDC6DACE4FF21DC643EA6C91369A052690CF187DC_H
#define U3CU3EC_TDC6DACE4FF21DC643EA6C91369A052690CF187DC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Sockets.SocketAsyncResult_<>c
struct  U3CU3Ec_tDC6DACE4FF21DC643EA6C91369A052690CF187DC  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tDC6DACE4FF21DC643EA6C91369A052690CF187DC_StaticFields
{
public:
	// System.Net.Sockets.SocketAsyncResult_<>c System.Net.Sockets.SocketAsyncResult_<>c::<>9
	U3CU3Ec_tDC6DACE4FF21DC643EA6C91369A052690CF187DC * ___U3CU3E9_0;
	// System.Threading.WaitCallback System.Net.Sockets.SocketAsyncResult_<>c::<>9__27_0
	WaitCallback_t61C5F053CAC7A7FE923208EFA060693D7997B4EC * ___U3CU3E9__27_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tDC6DACE4FF21DC643EA6C91369A052690CF187DC_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tDC6DACE4FF21DC643EA6C91369A052690CF187DC * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tDC6DACE4FF21DC643EA6C91369A052690CF187DC ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tDC6DACE4FF21DC643EA6C91369A052690CF187DC * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__27_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tDC6DACE4FF21DC643EA6C91369A052690CF187DC_StaticFields, ___U3CU3E9__27_0_1)); }
	inline WaitCallback_t61C5F053CAC7A7FE923208EFA060693D7997B4EC * get_U3CU3E9__27_0_1() const { return ___U3CU3E9__27_0_1; }
	inline WaitCallback_t61C5F053CAC7A7FE923208EFA060693D7997B4EC ** get_address_of_U3CU3E9__27_0_1() { return &___U3CU3E9__27_0_1; }
	inline void set_U3CU3E9__27_0_1(WaitCallback_t61C5F053CAC7A7FE923208EFA060693D7997B4EC * value)
	{
		___U3CU3E9__27_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__27_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TDC6DACE4FF21DC643EA6C91369A052690CF187DC_H
#ifndef TRANSPORTCONTEXT_T1753CC7BFFA637B35BE353AAE4452C4371F89A9F_H
#define TRANSPORTCONTEXT_T1753CC7BFFA637B35BE353AAE4452C4371F89A9F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.TransportContext
struct  TransportContext_t1753CC7BFFA637B35BE353AAE4452C4371F89A9F  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSPORTCONTEXT_T1753CC7BFFA637B35BE353AAE4452C4371F89A9F_H
#ifndef ABORTHELPER_T0DB9458211F015848382C4B5A007AC4947411E81_H
#define ABORTHELPER_T0DB9458211F015848382C4B5A007AC4947411E81_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebConnection_AbortHelper
struct  AbortHelper_t0DB9458211F015848382C4B5A007AC4947411E81  : public RuntimeObject
{
public:
	// System.Net.WebConnection System.Net.WebConnection_AbortHelper::Connection
	WebConnection_tEB76AEE17361D28CBAD4033026A71DA89289C243 * ___Connection_0;

public:
	inline static int32_t get_offset_of_Connection_0() { return static_cast<int32_t>(offsetof(AbortHelper_t0DB9458211F015848382C4B5A007AC4947411E81, ___Connection_0)); }
	inline WebConnection_tEB76AEE17361D28CBAD4033026A71DA89289C243 * get_Connection_0() const { return ___Connection_0; }
	inline WebConnection_tEB76AEE17361D28CBAD4033026A71DA89289C243 ** get_address_of_Connection_0() { return &___Connection_0; }
	inline void set_Connection_0(WebConnection_tEB76AEE17361D28CBAD4033026A71DA89289C243 * value)
	{
		___Connection_0 = value;
		Il2CppCodeGenWriteBarrier((&___Connection_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABORTHELPER_T0DB9458211F015848382C4B5A007AC4947411E81_H
#ifndef WEBCONNECTIONGROUP_TBEAB5ED1DE321C0981F5CBABA020970C3D63E95F_H
#define WEBCONNECTIONGROUP_TBEAB5ED1DE321C0981F5CBABA020970C3D63E95F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebConnectionGroup
struct  WebConnectionGroup_tBEAB5ED1DE321C0981F5CBABA020970C3D63E95F  : public RuntimeObject
{
public:
	// System.Net.ServicePoint System.Net.WebConnectionGroup::sPoint
	ServicePoint_t5F42B1A9D56E09B4B051BE0968C81DE3128E3EB4 * ___sPoint_0;
	// System.String System.Net.WebConnectionGroup::name
	String_t* ___name_1;
	// System.Collections.Generic.LinkedList`1<System.Net.WebConnectionGroup_ConnectionState> System.Net.WebConnectionGroup::connections
	LinkedList_1_t0513C063019CC3F3A13FA4E2C35EAAE500C6CF8F * ___connections_2;
	// System.Collections.Queue System.Net.WebConnectionGroup::queue
	Queue_tEC6DE7527799C2E4224B469ECD0CDD2B25E8E4F3 * ___queue_3;
	// System.Boolean System.Net.WebConnectionGroup::closing
	bool ___closing_4;
	// System.EventHandler System.Net.WebConnectionGroup::ConnectionClosed
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * ___ConnectionClosed_5;

public:
	inline static int32_t get_offset_of_sPoint_0() { return static_cast<int32_t>(offsetof(WebConnectionGroup_tBEAB5ED1DE321C0981F5CBABA020970C3D63E95F, ___sPoint_0)); }
	inline ServicePoint_t5F42B1A9D56E09B4B051BE0968C81DE3128E3EB4 * get_sPoint_0() const { return ___sPoint_0; }
	inline ServicePoint_t5F42B1A9D56E09B4B051BE0968C81DE3128E3EB4 ** get_address_of_sPoint_0() { return &___sPoint_0; }
	inline void set_sPoint_0(ServicePoint_t5F42B1A9D56E09B4B051BE0968C81DE3128E3EB4 * value)
	{
		___sPoint_0 = value;
		Il2CppCodeGenWriteBarrier((&___sPoint_0), value);
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(WebConnectionGroup_tBEAB5ED1DE321C0981F5CBABA020970C3D63E95F, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}

	inline static int32_t get_offset_of_connections_2() { return static_cast<int32_t>(offsetof(WebConnectionGroup_tBEAB5ED1DE321C0981F5CBABA020970C3D63E95F, ___connections_2)); }
	inline LinkedList_1_t0513C063019CC3F3A13FA4E2C35EAAE500C6CF8F * get_connections_2() const { return ___connections_2; }
	inline LinkedList_1_t0513C063019CC3F3A13FA4E2C35EAAE500C6CF8F ** get_address_of_connections_2() { return &___connections_2; }
	inline void set_connections_2(LinkedList_1_t0513C063019CC3F3A13FA4E2C35EAAE500C6CF8F * value)
	{
		___connections_2 = value;
		Il2CppCodeGenWriteBarrier((&___connections_2), value);
	}

	inline static int32_t get_offset_of_queue_3() { return static_cast<int32_t>(offsetof(WebConnectionGroup_tBEAB5ED1DE321C0981F5CBABA020970C3D63E95F, ___queue_3)); }
	inline Queue_tEC6DE7527799C2E4224B469ECD0CDD2B25E8E4F3 * get_queue_3() const { return ___queue_3; }
	inline Queue_tEC6DE7527799C2E4224B469ECD0CDD2B25E8E4F3 ** get_address_of_queue_3() { return &___queue_3; }
	inline void set_queue_3(Queue_tEC6DE7527799C2E4224B469ECD0CDD2B25E8E4F3 * value)
	{
		___queue_3 = value;
		Il2CppCodeGenWriteBarrier((&___queue_3), value);
	}

	inline static int32_t get_offset_of_closing_4() { return static_cast<int32_t>(offsetof(WebConnectionGroup_tBEAB5ED1DE321C0981F5CBABA020970C3D63E95F, ___closing_4)); }
	inline bool get_closing_4() const { return ___closing_4; }
	inline bool* get_address_of_closing_4() { return &___closing_4; }
	inline void set_closing_4(bool value)
	{
		___closing_4 = value;
	}

	inline static int32_t get_offset_of_ConnectionClosed_5() { return static_cast<int32_t>(offsetof(WebConnectionGroup_tBEAB5ED1DE321C0981F5CBABA020970C3D63E95F, ___ConnectionClosed_5)); }
	inline EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * get_ConnectionClosed_5() const { return ___ConnectionClosed_5; }
	inline EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C ** get_address_of_ConnectionClosed_5() { return &___ConnectionClosed_5; }
	inline void set_ConnectionClosed_5(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * value)
	{
		___ConnectionClosed_5 = value;
		Il2CppCodeGenWriteBarrier((&___ConnectionClosed_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBCONNECTIONGROUP_TBEAB5ED1DE321C0981F5CBABA020970C3D63E95F_H
#ifndef U3CU3EC__DISPLAYCLASS75_0_TB08224AE141BA9F8046D1F0C267E2308BA8CABCB_H
#define U3CU3EC__DISPLAYCLASS75_0_TB08224AE141BA9F8046D1F0C267E2308BA8CABCB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebConnectionStream_<>c__DisplayClass75_0
struct  U3CU3Ec__DisplayClass75_0_tB08224AE141BA9F8046D1F0C267E2308BA8CABCB  : public RuntimeObject
{
public:
	// System.Net.WebConnectionStream System.Net.WebConnectionStream_<>c__DisplayClass75_0::<>4__this
	WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC * ___U3CU3E4__this_0;
	// System.Boolean System.Net.WebConnectionStream_<>c__DisplayClass75_0::setInternalLength
	bool ___setInternalLength_1;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass75_0_tB08224AE141BA9F8046D1F0C267E2308BA8CABCB, ___U3CU3E4__this_0)); }
	inline WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_setInternalLength_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass75_0_tB08224AE141BA9F8046D1F0C267E2308BA8CABCB, ___setInternalLength_1)); }
	inline bool get_setInternalLength_1() const { return ___setInternalLength_1; }
	inline bool* get_address_of_setInternalLength_1() { return &___setInternalLength_1; }
	inline void set_setInternalLength_1(bool value)
	{
		___setInternalLength_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS75_0_TB08224AE141BA9F8046D1F0C267E2308BA8CABCB_H
#ifndef U3CU3EC__DISPLAYCLASS76_0_T09438DD600601604F0E06AE0B2549E981049E350_H
#define U3CU3EC__DISPLAYCLASS76_0_T09438DD600601604F0E06AE0B2549E981049E350_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebConnectionStream_<>c__DisplayClass76_0
struct  U3CU3Ec__DisplayClass76_0_t09438DD600601604F0E06AE0B2549E981049E350  : public RuntimeObject
{
public:
	// System.Net.WebConnectionStream System.Net.WebConnectionStream_<>c__DisplayClass76_0::<>4__this
	WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC * ___U3CU3E4__this_0;
	// System.Net.SimpleAsyncResult System.Net.WebConnectionStream_<>c__DisplayClass76_0::result
	SimpleAsyncResult_tA572851810F8E279EE9E5378A6D9A538B1822FC6 * ___result_1;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass76_0_t09438DD600601604F0E06AE0B2549E981049E350, ___U3CU3E4__this_0)); }
	inline WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_result_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass76_0_t09438DD600601604F0E06AE0B2549E981049E350, ___result_1)); }
	inline SimpleAsyncResult_tA572851810F8E279EE9E5378A6D9A538B1822FC6 * get_result_1() const { return ___result_1; }
	inline SimpleAsyncResult_tA572851810F8E279EE9E5378A6D9A538B1822FC6 ** get_address_of_result_1() { return &___result_1; }
	inline void set_result_1(SimpleAsyncResult_tA572851810F8E279EE9E5378A6D9A538B1822FC6 * value)
	{
		___result_1 = value;
		Il2CppCodeGenWriteBarrier((&___result_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS76_0_T09438DD600601604F0E06AE0B2549E981049E350_H
#ifndef U3CU3EC__DISPLAYCLASS80_0_T7D1F20BB8EB27922CC970938E79BEBC5B485FEEE_H
#define U3CU3EC__DISPLAYCLASS80_0_T7D1F20BB8EB27922CC970938E79BEBC5B485FEEE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebConnectionStream_<>c__DisplayClass80_0
struct  U3CU3Ec__DisplayClass80_0_t7D1F20BB8EB27922CC970938E79BEBC5B485FEEE  : public RuntimeObject
{
public:
	// System.Net.SimpleAsyncResult System.Net.WebConnectionStream_<>c__DisplayClass80_0::result
	SimpleAsyncResult_tA572851810F8E279EE9E5378A6D9A538B1822FC6 * ___result_0;
	// System.Net.WebConnectionStream System.Net.WebConnectionStream_<>c__DisplayClass80_0::<>4__this
	WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC * ___U3CU3E4__this_1;
	// System.Int32 System.Net.WebConnectionStream_<>c__DisplayClass80_0::length
	int32_t ___length_2;
	// System.Byte[] System.Net.WebConnectionStream_<>c__DisplayClass80_0::bytes
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___bytes_3;
	// System.AsyncCallback System.Net.WebConnectionStream_<>c__DisplayClass80_0::<>9__1
	AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___U3CU3E9__1_4;

public:
	inline static int32_t get_offset_of_result_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass80_0_t7D1F20BB8EB27922CC970938E79BEBC5B485FEEE, ___result_0)); }
	inline SimpleAsyncResult_tA572851810F8E279EE9E5378A6D9A538B1822FC6 * get_result_0() const { return ___result_0; }
	inline SimpleAsyncResult_tA572851810F8E279EE9E5378A6D9A538B1822FC6 ** get_address_of_result_0() { return &___result_0; }
	inline void set_result_0(SimpleAsyncResult_tA572851810F8E279EE9E5378A6D9A538B1822FC6 * value)
	{
		___result_0 = value;
		Il2CppCodeGenWriteBarrier((&___result_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass80_0_t7D1F20BB8EB27922CC970938E79BEBC5B485FEEE, ___U3CU3E4__this_1)); }
	inline WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}

	inline static int32_t get_offset_of_length_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass80_0_t7D1F20BB8EB27922CC970938E79BEBC5B485FEEE, ___length_2)); }
	inline int32_t get_length_2() const { return ___length_2; }
	inline int32_t* get_address_of_length_2() { return &___length_2; }
	inline void set_length_2(int32_t value)
	{
		___length_2 = value;
	}

	inline static int32_t get_offset_of_bytes_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass80_0_t7D1F20BB8EB27922CC970938E79BEBC5B485FEEE, ___bytes_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_bytes_3() const { return ___bytes_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_bytes_3() { return &___bytes_3; }
	inline void set_bytes_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___bytes_3 = value;
		Il2CppCodeGenWriteBarrier((&___bytes_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__1_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass80_0_t7D1F20BB8EB27922CC970938E79BEBC5B485FEEE, ___U3CU3E9__1_4)); }
	inline AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * get_U3CU3E9__1_4() const { return ___U3CU3E9__1_4; }
	inline AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 ** get_address_of_U3CU3E9__1_4() { return &___U3CU3E9__1_4; }
	inline void set_U3CU3E9__1_4(AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * value)
	{
		___U3CU3E9__1_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__1_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS80_0_T7D1F20BB8EB27922CC970938E79BEBC5B485FEEE_H
#ifndef CRITICALFINALIZEROBJECT_T8B006E1DEE084E781F5C0F3283E9226E28894DD9_H
#define CRITICALFINALIZEROBJECT_T8B006E1DEE084E781F5C0F3283E9226E28894DD9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.ConstrainedExecution.CriticalFinalizerObject
struct  CriticalFinalizerObject_t8B006E1DEE084E781F5C0F3283E9226E28894DD9  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRITICALFINALIZEROBJECT_T8B006E1DEE084E781F5C0F3283E9226E28894DD9_H
#ifndef CLAIMSIDENTITY_TF122872107B424684DCA7290543475BEFE49DB08_H
#define CLAIMSIDENTITY_TF122872107B424684DCA7290543475BEFE49DB08_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Claims.ClaimsIdentity
struct  ClaimsIdentity_tF122872107B424684DCA7290543475BEFE49DB08  : public RuntimeObject
{
public:
	// System.Byte[] System.Security.Claims.ClaimsIdentity::m_userSerializationData
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___m_userSerializationData_0;
	// System.Collections.Generic.List`1<System.Security.Claims.Claim> System.Security.Claims.ClaimsIdentity::m_instanceClaims
	List_1_t923CD82D31ECB841C5A8A88828B38C59AA19786D * ___m_instanceClaims_1;
	// System.Collections.ObjectModel.Collection`1<System.Collections.Generic.IEnumerable`1<System.Security.Claims.Claim>> System.Security.Claims.ClaimsIdentity::m_externalClaims
	Collection_1_t9E2CBF3D821CAC8AE4D9F7E0542D5BF84F47191F * ___m_externalClaims_2;
	// System.String System.Security.Claims.ClaimsIdentity::m_nameType
	String_t* ___m_nameType_3;
	// System.String System.Security.Claims.ClaimsIdentity::m_roleType
	String_t* ___m_roleType_4;
	// System.String System.Security.Claims.ClaimsIdentity::m_version
	String_t* ___m_version_5;
	// System.Security.Claims.ClaimsIdentity System.Security.Claims.ClaimsIdentity::m_actor
	ClaimsIdentity_tF122872107B424684DCA7290543475BEFE49DB08 * ___m_actor_6;
	// System.String System.Security.Claims.ClaimsIdentity::m_authenticationType
	String_t* ___m_authenticationType_7;
	// System.Object System.Security.Claims.ClaimsIdentity::m_bootstrapContext
	RuntimeObject * ___m_bootstrapContext_8;
	// System.String System.Security.Claims.ClaimsIdentity::m_label
	String_t* ___m_label_9;
	// System.String System.Security.Claims.ClaimsIdentity::m_serializedNameType
	String_t* ___m_serializedNameType_10;
	// System.String System.Security.Claims.ClaimsIdentity::m_serializedRoleType
	String_t* ___m_serializedRoleType_11;
	// System.String System.Security.Claims.ClaimsIdentity::m_serializedClaims
	String_t* ___m_serializedClaims_12;

public:
	inline static int32_t get_offset_of_m_userSerializationData_0() { return static_cast<int32_t>(offsetof(ClaimsIdentity_tF122872107B424684DCA7290543475BEFE49DB08, ___m_userSerializationData_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_m_userSerializationData_0() const { return ___m_userSerializationData_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_m_userSerializationData_0() { return &___m_userSerializationData_0; }
	inline void set_m_userSerializationData_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___m_userSerializationData_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_userSerializationData_0), value);
	}

	inline static int32_t get_offset_of_m_instanceClaims_1() { return static_cast<int32_t>(offsetof(ClaimsIdentity_tF122872107B424684DCA7290543475BEFE49DB08, ___m_instanceClaims_1)); }
	inline List_1_t923CD82D31ECB841C5A8A88828B38C59AA19786D * get_m_instanceClaims_1() const { return ___m_instanceClaims_1; }
	inline List_1_t923CD82D31ECB841C5A8A88828B38C59AA19786D ** get_address_of_m_instanceClaims_1() { return &___m_instanceClaims_1; }
	inline void set_m_instanceClaims_1(List_1_t923CD82D31ECB841C5A8A88828B38C59AA19786D * value)
	{
		___m_instanceClaims_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_instanceClaims_1), value);
	}

	inline static int32_t get_offset_of_m_externalClaims_2() { return static_cast<int32_t>(offsetof(ClaimsIdentity_tF122872107B424684DCA7290543475BEFE49DB08, ___m_externalClaims_2)); }
	inline Collection_1_t9E2CBF3D821CAC8AE4D9F7E0542D5BF84F47191F * get_m_externalClaims_2() const { return ___m_externalClaims_2; }
	inline Collection_1_t9E2CBF3D821CAC8AE4D9F7E0542D5BF84F47191F ** get_address_of_m_externalClaims_2() { return &___m_externalClaims_2; }
	inline void set_m_externalClaims_2(Collection_1_t9E2CBF3D821CAC8AE4D9F7E0542D5BF84F47191F * value)
	{
		___m_externalClaims_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_externalClaims_2), value);
	}

	inline static int32_t get_offset_of_m_nameType_3() { return static_cast<int32_t>(offsetof(ClaimsIdentity_tF122872107B424684DCA7290543475BEFE49DB08, ___m_nameType_3)); }
	inline String_t* get_m_nameType_3() const { return ___m_nameType_3; }
	inline String_t** get_address_of_m_nameType_3() { return &___m_nameType_3; }
	inline void set_m_nameType_3(String_t* value)
	{
		___m_nameType_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_nameType_3), value);
	}

	inline static int32_t get_offset_of_m_roleType_4() { return static_cast<int32_t>(offsetof(ClaimsIdentity_tF122872107B424684DCA7290543475BEFE49DB08, ___m_roleType_4)); }
	inline String_t* get_m_roleType_4() const { return ___m_roleType_4; }
	inline String_t** get_address_of_m_roleType_4() { return &___m_roleType_4; }
	inline void set_m_roleType_4(String_t* value)
	{
		___m_roleType_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_roleType_4), value);
	}

	inline static int32_t get_offset_of_m_version_5() { return static_cast<int32_t>(offsetof(ClaimsIdentity_tF122872107B424684DCA7290543475BEFE49DB08, ___m_version_5)); }
	inline String_t* get_m_version_5() const { return ___m_version_5; }
	inline String_t** get_address_of_m_version_5() { return &___m_version_5; }
	inline void set_m_version_5(String_t* value)
	{
		___m_version_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_version_5), value);
	}

	inline static int32_t get_offset_of_m_actor_6() { return static_cast<int32_t>(offsetof(ClaimsIdentity_tF122872107B424684DCA7290543475BEFE49DB08, ___m_actor_6)); }
	inline ClaimsIdentity_tF122872107B424684DCA7290543475BEFE49DB08 * get_m_actor_6() const { return ___m_actor_6; }
	inline ClaimsIdentity_tF122872107B424684DCA7290543475BEFE49DB08 ** get_address_of_m_actor_6() { return &___m_actor_6; }
	inline void set_m_actor_6(ClaimsIdentity_tF122872107B424684DCA7290543475BEFE49DB08 * value)
	{
		___m_actor_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_actor_6), value);
	}

	inline static int32_t get_offset_of_m_authenticationType_7() { return static_cast<int32_t>(offsetof(ClaimsIdentity_tF122872107B424684DCA7290543475BEFE49DB08, ___m_authenticationType_7)); }
	inline String_t* get_m_authenticationType_7() const { return ___m_authenticationType_7; }
	inline String_t** get_address_of_m_authenticationType_7() { return &___m_authenticationType_7; }
	inline void set_m_authenticationType_7(String_t* value)
	{
		___m_authenticationType_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_authenticationType_7), value);
	}

	inline static int32_t get_offset_of_m_bootstrapContext_8() { return static_cast<int32_t>(offsetof(ClaimsIdentity_tF122872107B424684DCA7290543475BEFE49DB08, ___m_bootstrapContext_8)); }
	inline RuntimeObject * get_m_bootstrapContext_8() const { return ___m_bootstrapContext_8; }
	inline RuntimeObject ** get_address_of_m_bootstrapContext_8() { return &___m_bootstrapContext_8; }
	inline void set_m_bootstrapContext_8(RuntimeObject * value)
	{
		___m_bootstrapContext_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_bootstrapContext_8), value);
	}

	inline static int32_t get_offset_of_m_label_9() { return static_cast<int32_t>(offsetof(ClaimsIdentity_tF122872107B424684DCA7290543475BEFE49DB08, ___m_label_9)); }
	inline String_t* get_m_label_9() const { return ___m_label_9; }
	inline String_t** get_address_of_m_label_9() { return &___m_label_9; }
	inline void set_m_label_9(String_t* value)
	{
		___m_label_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_label_9), value);
	}

	inline static int32_t get_offset_of_m_serializedNameType_10() { return static_cast<int32_t>(offsetof(ClaimsIdentity_tF122872107B424684DCA7290543475BEFE49DB08, ___m_serializedNameType_10)); }
	inline String_t* get_m_serializedNameType_10() const { return ___m_serializedNameType_10; }
	inline String_t** get_address_of_m_serializedNameType_10() { return &___m_serializedNameType_10; }
	inline void set_m_serializedNameType_10(String_t* value)
	{
		___m_serializedNameType_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_serializedNameType_10), value);
	}

	inline static int32_t get_offset_of_m_serializedRoleType_11() { return static_cast<int32_t>(offsetof(ClaimsIdentity_tF122872107B424684DCA7290543475BEFE49DB08, ___m_serializedRoleType_11)); }
	inline String_t* get_m_serializedRoleType_11() const { return ___m_serializedRoleType_11; }
	inline String_t** get_address_of_m_serializedRoleType_11() { return &___m_serializedRoleType_11; }
	inline void set_m_serializedRoleType_11(String_t* value)
	{
		___m_serializedRoleType_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_serializedRoleType_11), value);
	}

	inline static int32_t get_offset_of_m_serializedClaims_12() { return static_cast<int32_t>(offsetof(ClaimsIdentity_tF122872107B424684DCA7290543475BEFE49DB08, ___m_serializedClaims_12)); }
	inline String_t* get_m_serializedClaims_12() const { return ___m_serializedClaims_12; }
	inline String_t** get_address_of_m_serializedClaims_12() { return &___m_serializedClaims_12; }
	inline void set_m_serializedClaims_12(String_t* value)
	{
		___m_serializedClaims_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_serializedClaims_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLAIMSIDENTITY_TF122872107B424684DCA7290543475BEFE49DB08_H
#ifndef CODEACCESSPERMISSION_T1825F96A910182C8630635E511546E40DC0FF5FB_H
#define CODEACCESSPERMISSION_T1825F96A910182C8630635E511546E40DC0FF5FB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.CodeAccessPermission
struct  CodeAccessPermission_t1825F96A910182C8630635E511546E40DC0FF5FB  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CODEACCESSPERMISSION_T1825F96A910182C8630635E511546E40DC0FF5FB_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#define BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifndef DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#define DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 
{
public:
	// System.UInt64 System.DateTime::dateData
	uint64_t ___dateData_44;

public:
	inline static int32_t get_offset_of_dateData_44() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132, ___dateData_44)); }
	inline uint64_t get_dateData_44() const { return ___dateData_44; }
	inline uint64_t* get_address_of_dateData_44() { return &___dateData_44; }
	inline void set_dateData_44(uint64_t value)
	{
		___dateData_44 = value;
	}
};

struct DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields
{
public:
	// System.Int32[] System.DateTime::DaysToMonth365
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth365_29;
	// System.Int32[] System.DateTime::DaysToMonth366
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth366_30;
	// System.DateTime System.DateTime::MinValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MinValue_31;
	// System.DateTime System.DateTime::MaxValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MaxValue_32;

public:
	inline static int32_t get_offset_of_DaysToMonth365_29() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth365_29)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth365_29() const { return ___DaysToMonth365_29; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth365_29() { return &___DaysToMonth365_29; }
	inline void set_DaysToMonth365_29(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth365_29 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth365_29), value);
	}

	inline static int32_t get_offset_of_DaysToMonth366_30() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth366_30)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth366_30() const { return ___DaysToMonth366_30; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth366_30() { return &___DaysToMonth366_30; }
	inline void set_DaysToMonth366_30(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth366_30 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth366_30), value);
	}

	inline static int32_t get_offset_of_MinValue_31() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MinValue_31)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MinValue_31() const { return ___MinValue_31; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MinValue_31() { return &___MinValue_31; }
	inline void set_MinValue_31(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MinValue_31 = value;
	}

	inline static int32_t get_offset_of_MaxValue_32() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MaxValue_32)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MaxValue_32() const { return ___MaxValue_32; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MaxValue_32() { return &___MaxValue_32; }
	inline void set_MaxValue_32(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MaxValue_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#define STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Stream
struct  Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7  : public MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF
{
public:
	// System.IO.Stream_ReadWriteTask System.IO.Stream::_activeReadWriteTask
	ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * ____activeReadWriteTask_3;
	// System.Threading.SemaphoreSlim System.IO.Stream::_asyncActiveSemaphore
	SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * ____asyncActiveSemaphore_4;

public:
	inline static int32_t get_offset_of__activeReadWriteTask_3() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7, ____activeReadWriteTask_3)); }
	inline ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * get__activeReadWriteTask_3() const { return ____activeReadWriteTask_3; }
	inline ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 ** get_address_of__activeReadWriteTask_3() { return &____activeReadWriteTask_3; }
	inline void set__activeReadWriteTask_3(ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * value)
	{
		____activeReadWriteTask_3 = value;
		Il2CppCodeGenWriteBarrier((&____activeReadWriteTask_3), value);
	}

	inline static int32_t get_offset_of__asyncActiveSemaphore_4() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7, ____asyncActiveSemaphore_4)); }
	inline SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * get__asyncActiveSemaphore_4() const { return ____asyncActiveSemaphore_4; }
	inline SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 ** get_address_of__asyncActiveSemaphore_4() { return &____asyncActiveSemaphore_4; }
	inline void set__asyncActiveSemaphore_4(SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * value)
	{
		____asyncActiveSemaphore_4 = value;
		Il2CppCodeGenWriteBarrier((&____asyncActiveSemaphore_4), value);
	}
};

struct Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7_StaticFields
{
public:
	// System.IO.Stream System.IO.Stream::Null
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___Null_1;

public:
	inline static int32_t get_offset_of_Null_1() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7_StaticFields, ___Null_1)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_Null_1() const { return ___Null_1; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_Null_1() { return &___Null_1; }
	inline void set_Null_1(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___Null_1 = value;
		Il2CppCodeGenWriteBarrier((&___Null_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#ifndef INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#define INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef CONTEXT_T125FB4B035D650236F7DBF42DAC5D58D85FAC512_H
#define CONTEXT_T125FB4B035D650236F7DBF42DAC5D58D85FAC512_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HttpListenerRequest_Context
struct  Context_t125FB4B035D650236F7DBF42DAC5D58D85FAC512  : public TransportContext_t1753CC7BFFA637B35BE353AAE4452C4371F89A9F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTEXT_T125FB4B035D650236F7DBF42DAC5D58D85FAC512_H
#ifndef IPV6ADDRESSFORMATTER_T451290B1C6FD64B6C59F95D99EDB4A9CC703BA90_H
#define IPV6ADDRESSFORMATTER_T451290B1C6FD64B6C59F95D99EDB4A9CC703BA90_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.IPv6AddressFormatter
struct  IPv6AddressFormatter_t451290B1C6FD64B6C59F95D99EDB4A9CC703BA90 
{
public:
	// System.UInt16[] System.Net.IPv6AddressFormatter::address
	UInt16U5BU5D_t2D4BB1F8C486FF4359FFA7E4A76A8708A684543E* ___address_0;
	// System.Int64 System.Net.IPv6AddressFormatter::scopeId
	int64_t ___scopeId_1;

public:
	inline static int32_t get_offset_of_address_0() { return static_cast<int32_t>(offsetof(IPv6AddressFormatter_t451290B1C6FD64B6C59F95D99EDB4A9CC703BA90, ___address_0)); }
	inline UInt16U5BU5D_t2D4BB1F8C486FF4359FFA7E4A76A8708A684543E* get_address_0() const { return ___address_0; }
	inline UInt16U5BU5D_t2D4BB1F8C486FF4359FFA7E4A76A8708A684543E** get_address_of_address_0() { return &___address_0; }
	inline void set_address_0(UInt16U5BU5D_t2D4BB1F8C486FF4359FFA7E4A76A8708A684543E* value)
	{
		___address_0 = value;
		Il2CppCodeGenWriteBarrier((&___address_0), value);
	}

	inline static int32_t get_offset_of_scopeId_1() { return static_cast<int32_t>(offsetof(IPv6AddressFormatter_t451290B1C6FD64B6C59F95D99EDB4A9CC703BA90, ___scopeId_1)); }
	inline int64_t get_scopeId_1() const { return ___scopeId_1; }
	inline int64_t* get_address_of_scopeId_1() { return &___scopeId_1; }
	inline void set_scopeId_1(int64_t value)
	{
		___scopeId_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IPV6ADDRESSFORMATTER_T451290B1C6FD64B6C59F95D99EDB4A9CC703BA90_H
#ifndef BASE64WRITESTATEINFO_TB45E408314AC5BFE8908E8E6C85BEB10EF29F366_H
#define BASE64WRITESTATEINFO_TB45E408314AC5BFE8908E8E6C85BEB10EF29F366_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Mime.Base64WriteStateInfo
struct  Base64WriteStateInfo_tB45E408314AC5BFE8908E8E6C85BEB10EF29F366  : public WriteStateInfoBase_t029DDB6613907E0AEF10D611B6E48422EC7778FD
{
public:
	// System.Int32 System.Net.Mime.Base64WriteStateInfo::<Padding>k__BackingField
	int32_t ___U3CPaddingU3Ek__BackingField_6;
	// System.Byte System.Net.Mime.Base64WriteStateInfo::<LastBits>k__BackingField
	uint8_t ___U3CLastBitsU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_U3CPaddingU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Base64WriteStateInfo_tB45E408314AC5BFE8908E8E6C85BEB10EF29F366, ___U3CPaddingU3Ek__BackingField_6)); }
	inline int32_t get_U3CPaddingU3Ek__BackingField_6() const { return ___U3CPaddingU3Ek__BackingField_6; }
	inline int32_t* get_address_of_U3CPaddingU3Ek__BackingField_6() { return &___U3CPaddingU3Ek__BackingField_6; }
	inline void set_U3CPaddingU3Ek__BackingField_6(int32_t value)
	{
		___U3CPaddingU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CLastBitsU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(Base64WriteStateInfo_tB45E408314AC5BFE8908E8E6C85BEB10EF29F366, ___U3CLastBitsU3Ek__BackingField_7)); }
	inline uint8_t get_U3CLastBitsU3Ek__BackingField_7() const { return ___U3CLastBitsU3Ek__BackingField_7; }
	inline uint8_t* get_address_of_U3CLastBitsU3Ek__BackingField_7() { return &___U3CLastBitsU3Ek__BackingField_7; }
	inline void set_U3CLastBitsU3Ek__BackingField_7(uint8_t value)
	{
		___U3CLastBitsU3Ek__BackingField_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASE64WRITESTATEINFO_TB45E408314AC5BFE8908E8E6C85BEB10EF29F366_H
#ifndef SOCKETPERMISSION_T1B5B253F95303B8DDD7F1539C60DE1B0ACA358C5_H
#define SOCKETPERMISSION_T1B5B253F95303B8DDD7F1539C60DE1B0ACA358C5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.SocketPermission
struct  SocketPermission_t1B5B253F95303B8DDD7F1539C60DE1B0ACA358C5  : public CodeAccessPermission_t1825F96A910182C8630635E511546E40DC0FF5FB
{
public:
	// System.Collections.ArrayList System.Net.SocketPermission::m_acceptList
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ___m_acceptList_0;
	// System.Collections.ArrayList System.Net.SocketPermission::m_connectList
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ___m_connectList_1;
	// System.Boolean System.Net.SocketPermission::m_noRestriction
	bool ___m_noRestriction_2;

public:
	inline static int32_t get_offset_of_m_acceptList_0() { return static_cast<int32_t>(offsetof(SocketPermission_t1B5B253F95303B8DDD7F1539C60DE1B0ACA358C5, ___m_acceptList_0)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get_m_acceptList_0() const { return ___m_acceptList_0; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of_m_acceptList_0() { return &___m_acceptList_0; }
	inline void set_m_acceptList_0(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		___m_acceptList_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_acceptList_0), value);
	}

	inline static int32_t get_offset_of_m_connectList_1() { return static_cast<int32_t>(offsetof(SocketPermission_t1B5B253F95303B8DDD7F1539C60DE1B0ACA358C5, ___m_connectList_1)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get_m_connectList_1() const { return ___m_connectList_1; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of_m_connectList_1() { return &___m_connectList_1; }
	inline void set_m_connectList_1(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		___m_connectList_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_connectList_1), value);
	}

	inline static int32_t get_offset_of_m_noRestriction_2() { return static_cast<int32_t>(offsetof(SocketPermission_t1B5B253F95303B8DDD7F1539C60DE1B0ACA358C5, ___m_noRestriction_2)); }
	inline bool get_m_noRestriction_2() const { return ___m_noRestriction_2; }
	inline bool* get_address_of_m_noRestriction_2() { return &___m_noRestriction_2; }
	inline void set_m_noRestriction_2(bool value)
	{
		___m_noRestriction_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOCKETPERMISSION_T1B5B253F95303B8DDD7F1539C60DE1B0ACA358C5_H
#ifndef WEBRESPONSE_T5C91B5B83E2FBA2EABC6FDF2A70E9AFD9BB059BD_H
#define WEBRESPONSE_T5C91B5B83E2FBA2EABC6FDF2A70E9AFD9BB059BD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebResponse
struct  WebResponse_t5C91B5B83E2FBA2EABC6FDF2A70E9AFD9BB059BD  : public MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF
{
public:
	// System.Boolean System.Net.WebResponse::m_IsCacheFresh
	bool ___m_IsCacheFresh_1;
	// System.Boolean System.Net.WebResponse::m_IsFromCache
	bool ___m_IsFromCache_2;

public:
	inline static int32_t get_offset_of_m_IsCacheFresh_1() { return static_cast<int32_t>(offsetof(WebResponse_t5C91B5B83E2FBA2EABC6FDF2A70E9AFD9BB059BD, ___m_IsCacheFresh_1)); }
	inline bool get_m_IsCacheFresh_1() const { return ___m_IsCacheFresh_1; }
	inline bool* get_address_of_m_IsCacheFresh_1() { return &___m_IsCacheFresh_1; }
	inline void set_m_IsCacheFresh_1(bool value)
	{
		___m_IsCacheFresh_1 = value;
	}

	inline static int32_t get_offset_of_m_IsFromCache_2() { return static_cast<int32_t>(offsetof(WebResponse_t5C91B5B83E2FBA2EABC6FDF2A70E9AFD9BB059BD, ___m_IsFromCache_2)); }
	inline bool get_m_IsFromCache_2() const { return ___m_IsFromCache_2; }
	inline bool* get_address_of_m_IsFromCache_2() { return &___m_IsFromCache_2; }
	inline void set_m_IsFromCache_2(bool value)
	{
		___m_IsFromCache_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBRESPONSE_T5C91B5B83E2FBA2EABC6FDF2A70E9AFD9BB059BD_H
#ifndef NULLABLE_1_T9E6A67BECE376F0623B5C857F5674A0311C41793_H
#define NULLABLE_1_T9E6A67BECE376F0623B5C857F5674A0311C41793_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Boolean>
struct  Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 
{
public:
	// T System.Nullable`1::value
	bool ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793, ___value_0)); }
	inline bool get_value_0() const { return ___value_0; }
	inline bool* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(bool value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T9E6A67BECE376F0623B5C857F5674A0311C41793_H
#ifndef GENERICIDENTITY_TA65BC143F6C41ACBB0AC6823D0C2BA0612620049_H
#define GENERICIDENTITY_TA65BC143F6C41ACBB0AC6823D0C2BA0612620049_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Principal.GenericIdentity
struct  GenericIdentity_tA65BC143F6C41ACBB0AC6823D0C2BA0612620049  : public ClaimsIdentity_tF122872107B424684DCA7290543475BEFE49DB08
{
public:
	// System.String System.Security.Principal.GenericIdentity::m_name
	String_t* ___m_name_13;
	// System.String System.Security.Principal.GenericIdentity::m_type
	String_t* ___m_type_14;

public:
	inline static int32_t get_offset_of_m_name_13() { return static_cast<int32_t>(offsetof(GenericIdentity_tA65BC143F6C41ACBB0AC6823D0C2BA0612620049, ___m_name_13)); }
	inline String_t* get_m_name_13() const { return ___m_name_13; }
	inline String_t** get_address_of_m_name_13() { return &___m_name_13; }
	inline void set_m_name_13(String_t* value)
	{
		___m_name_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_name_13), value);
	}

	inline static int32_t get_offset_of_m_type_14() { return static_cast<int32_t>(offsetof(GenericIdentity_tA65BC143F6C41ACBB0AC6823D0C2BA0612620049, ___m_type_14)); }
	inline String_t* get_m_type_14() const { return ___m_type_14; }
	inline String_t** get_address_of_m_type_14() { return &___m_type_14; }
	inline void set_m_type_14(String_t* value)
	{
		___m_type_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_type_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICIDENTITY_TA65BC143F6C41ACBB0AC6823D0C2BA0612620049_H
#ifndef SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#define SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef AUTHENTICATIONSCHEMES_T884FB94458AFBF2F2380DC4B0EB5588225A6C372_H
#define AUTHENTICATIONSCHEMES_T884FB94458AFBF2F2380DC4B0EB5588225A6C372_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.AuthenticationSchemes
struct  AuthenticationSchemes_t884FB94458AFBF2F2380DC4B0EB5588225A6C372 
{
public:
	// System.Int32 System.Net.AuthenticationSchemes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AuthenticationSchemes_t884FB94458AFBF2F2380DC4B0EB5588225A6C372, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTHENTICATIONSCHEMES_T884FB94458AFBF2F2380DC4B0EB5588225A6C372_H
#ifndef DECOMPRESSIONMETHODS_T828950DA24A3D2B4A635E51125685CDB629ED51D_H
#define DECOMPRESSIONMETHODS_T828950DA24A3D2B4A635E51125685CDB629ED51D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.DecompressionMethods
struct  DecompressionMethods_t828950DA24A3D2B4A635E51125685CDB629ED51D 
{
public:
	// System.Int32 System.Net.DecompressionMethods::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DecompressionMethods_t828950DA24A3D2B4A635E51125685CDB629ED51D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECOMPRESSIONMETHODS_T828950DA24A3D2B4A635E51125685CDB629ED51D_H
#ifndef FTPDATASTREAM_TBF423F55CA0947ED2BF909BEA79DA349338DD3B1_H
#define FTPDATASTREAM_TBF423F55CA0947ED2BF909BEA79DA349338DD3B1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.FtpDataStream
struct  FtpDataStream_tBF423F55CA0947ED2BF909BEA79DA349338DD3B1  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// System.Net.FtpWebRequest System.Net.FtpDataStream::request
	FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA * ___request_5;
	// System.IO.Stream System.Net.FtpDataStream::networkStream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___networkStream_6;
	// System.Boolean System.Net.FtpDataStream::disposed
	bool ___disposed_7;
	// System.Boolean System.Net.FtpDataStream::isRead
	bool ___isRead_8;
	// System.Int32 System.Net.FtpDataStream::totalRead
	int32_t ___totalRead_9;

public:
	inline static int32_t get_offset_of_request_5() { return static_cast<int32_t>(offsetof(FtpDataStream_tBF423F55CA0947ED2BF909BEA79DA349338DD3B1, ___request_5)); }
	inline FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA * get_request_5() const { return ___request_5; }
	inline FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA ** get_address_of_request_5() { return &___request_5; }
	inline void set_request_5(FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA * value)
	{
		___request_5 = value;
		Il2CppCodeGenWriteBarrier((&___request_5), value);
	}

	inline static int32_t get_offset_of_networkStream_6() { return static_cast<int32_t>(offsetof(FtpDataStream_tBF423F55CA0947ED2BF909BEA79DA349338DD3B1, ___networkStream_6)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_networkStream_6() const { return ___networkStream_6; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_networkStream_6() { return &___networkStream_6; }
	inline void set_networkStream_6(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___networkStream_6 = value;
		Il2CppCodeGenWriteBarrier((&___networkStream_6), value);
	}

	inline static int32_t get_offset_of_disposed_7() { return static_cast<int32_t>(offsetof(FtpDataStream_tBF423F55CA0947ED2BF909BEA79DA349338DD3B1, ___disposed_7)); }
	inline bool get_disposed_7() const { return ___disposed_7; }
	inline bool* get_address_of_disposed_7() { return &___disposed_7; }
	inline void set_disposed_7(bool value)
	{
		___disposed_7 = value;
	}

	inline static int32_t get_offset_of_isRead_8() { return static_cast<int32_t>(offsetof(FtpDataStream_tBF423F55CA0947ED2BF909BEA79DA349338DD3B1, ___isRead_8)); }
	inline bool get_isRead_8() const { return ___isRead_8; }
	inline bool* get_address_of_isRead_8() { return &___isRead_8; }
	inline void set_isRead_8(bool value)
	{
		___isRead_8 = value;
	}

	inline static int32_t get_offset_of_totalRead_9() { return static_cast<int32_t>(offsetof(FtpDataStream_tBF423F55CA0947ED2BF909BEA79DA349338DD3B1, ___totalRead_9)); }
	inline int32_t get_totalRead_9() const { return ___totalRead_9; }
	inline int32_t* get_address_of_totalRead_9() { return &___totalRead_9; }
	inline void set_totalRead_9(int32_t value)
	{
		___totalRead_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FTPDATASTREAM_TBF423F55CA0947ED2BF909BEA79DA349338DD3B1_H
#ifndef FTPSTATUSCODE_T25AB6DADF4DE44C0973C59F53A7D797F009F8C67_H
#define FTPSTATUSCODE_T25AB6DADF4DE44C0973C59F53A7D797F009F8C67_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.FtpStatusCode
struct  FtpStatusCode_t25AB6DADF4DE44C0973C59F53A7D797F009F8C67 
{
public:
	// System.Int32 System.Net.FtpStatusCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FtpStatusCode_t25AB6DADF4DE44C0973C59F53A7D797F009F8C67, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FTPSTATUSCODE_T25AB6DADF4DE44C0973C59F53A7D797F009F8C67_H
#ifndef REQUESTSTATE_T850C56F50136642DB235E32D764586B31C248731_H
#define REQUESTSTATE_T850C56F50136642DB235E32D764586B31C248731_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.FtpWebRequest_RequestState
struct  RequestState_t850C56F50136642DB235E32D764586B31C248731 
{
public:
	// System.Int32 System.Net.FtpWebRequest_RequestState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RequestState_t850C56F50136642DB235E32D764586B31C248731, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUESTSTATE_T850C56F50136642DB235E32D764586B31C248731_H
#ifndef INPUTSTATE_TC9C07C35B1B422E948BD606C0AF277DFDAD48D86_H
#define INPUTSTATE_TC9C07C35B1B422E948BD606C0AF277DFDAD48D86_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HttpConnection_InputState
struct  InputState_tC9C07C35B1B422E948BD606C0AF277DFDAD48D86 
{
public:
	// System.Int32 System.Net.HttpConnection_InputState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InputState_tC9C07C35B1B422E948BD606C0AF277DFDAD48D86, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTSTATE_TC9C07C35B1B422E948BD606C0AF277DFDAD48D86_H
#ifndef LINESTATE_TE99D1029981D06DF9C2614BBDEA270C09E2EA666_H
#define LINESTATE_TE99D1029981D06DF9C2614BBDEA270C09E2EA666_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HttpConnection_LineState
struct  LineState_tE99D1029981D06DF9C2614BBDEA270C09E2EA666 
{
public:
	// System.Int32 System.Net.HttpConnection_LineState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LineState_tE99D1029981D06DF9C2614BBDEA270C09E2EA666, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINESTATE_TE99D1029981D06DF9C2614BBDEA270C09E2EA666_H
#ifndef HTTPLISTENERBASICIDENTITY_T12A25C542707784381BC1A66CCB3344D98EA9398_H
#define HTTPLISTENERBASICIDENTITY_T12A25C542707784381BC1A66CCB3344D98EA9398_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HttpListenerBasicIdentity
struct  HttpListenerBasicIdentity_t12A25C542707784381BC1A66CCB3344D98EA9398  : public GenericIdentity_tA65BC143F6C41ACBB0AC6823D0C2BA0612620049
{
public:
	// System.String System.Net.HttpListenerBasicIdentity::password
	String_t* ___password_15;

public:
	inline static int32_t get_offset_of_password_15() { return static_cast<int32_t>(offsetof(HttpListenerBasicIdentity_t12A25C542707784381BC1A66CCB3344D98EA9398, ___password_15)); }
	inline String_t* get_password_15() const { return ___password_15; }
	inline String_t** get_address_of_password_15() { return &___password_15; }
	inline void set_password_15(String_t* value)
	{
		___password_15 = value;
		Il2CppCodeGenWriteBarrier((&___password_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPLISTENERBASICIDENTITY_T12A25C542707784381BC1A66CCB3344D98EA9398_H
#ifndef HTTPSTATUSCODE_TEEC31491D56EE5BDB252F07906878274FD22AC0C_H
#define HTTPSTATUSCODE_TEEC31491D56EE5BDB252F07906878274FD22AC0C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HttpStatusCode
struct  HttpStatusCode_tEEC31491D56EE5BDB252F07906878274FD22AC0C 
{
public:
	// System.Int32 System.Net.HttpStatusCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HttpStatusCode_tEEC31491D56EE5BDB252F07906878274FD22AC0C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPSTATUSCODE_TEEC31491D56EE5BDB252F07906878274FD22AC0C_H
#ifndef NTLMAUTHSTATE_TF501EE09345DFAE6FD7B4D8EBBE77292514DFA83_H
#define NTLMAUTHSTATE_TF501EE09345DFAE6FD7B4D8EBBE77292514DFA83_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HttpWebRequest_NtlmAuthState
struct  NtlmAuthState_tF501EE09345DFAE6FD7B4D8EBBE77292514DFA83 
{
public:
	// System.Int32 System.Net.HttpWebRequest_NtlmAuthState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NtlmAuthState_tF501EE09345DFAE6FD7B4D8EBBE77292514DFA83, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NTLMAUTHSTATE_TF501EE09345DFAE6FD7B4D8EBBE77292514DFA83_H
#ifndef SMTPDELIVERYFORMAT_T100736159C2DA0B86D1CC40D5B0FEA85250ECDB6_H
#define SMTPDELIVERYFORMAT_T100736159C2DA0B86D1CC40D5B0FEA85250ECDB6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Mail.SmtpDeliveryFormat
struct  SmtpDeliveryFormat_t100736159C2DA0B86D1CC40D5B0FEA85250ECDB6 
{
public:
	// System.Int32 System.Net.Mail.SmtpDeliveryFormat::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SmtpDeliveryFormat_t100736159C2DA0B86D1CC40D5B0FEA85250ECDB6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SMTPDELIVERYFORMAT_T100736159C2DA0B86D1CC40D5B0FEA85250ECDB6_H
#ifndef SMTPDELIVERYMETHOD_T5BEC6DACB33CA745F948B250D0E1E0028D2BBC41_H
#define SMTPDELIVERYMETHOD_T5BEC6DACB33CA745F948B250D0E1E0028D2BBC41_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Mail.SmtpDeliveryMethod
struct  SmtpDeliveryMethod_t5BEC6DACB33CA745F948B250D0E1E0028D2BBC41 
{
public:
	// System.Int32 System.Net.Mail.SmtpDeliveryMethod::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SmtpDeliveryMethod_t5BEC6DACB33CA745F948B250D0E1E0028D2BBC41, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SMTPDELIVERYMETHOD_T5BEC6DACB33CA745F948B250D0E1E0028D2BBC41_H
#ifndef STATE_T9575019D3583B1A375418BD20391992F28C37967_H
#define STATE_T9575019D3583B1A375418BD20391992F28C37967_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.MonoChunkStream_State
struct  State_t9575019D3583B1A375418BD20391992F28C37967 
{
public:
	// System.Int32 System.Net.MonoChunkStream_State::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(State_t9575019D3583B1A375418BD20391992F28C37967, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATE_T9575019D3583B1A375418BD20391992F28C37967_H
#ifndef READSTATE_T4A38DE8AC8A5473133060405B3A00021D4422114_H
#define READSTATE_T4A38DE8AC8A5473133060405B3A00021D4422114_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.ReadState
struct  ReadState_t4A38DE8AC8A5473133060405B3A00021D4422114 
{
public:
	// System.Int32 System.Net.ReadState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ReadState_t4A38DE8AC8A5473133060405B3A00021D4422114, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READSTATE_T4A38DE8AC8A5473133060405B3A00021D4422114_H
#ifndef REQUESTSTREAM_T4BF6FD18AB7261094F557E867606B91FECF74D35_H
#define REQUESTSTREAM_T4BF6FD18AB7261094F557E867606B91FECF74D35_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.RequestStream
struct  RequestStream_t4BF6FD18AB7261094F557E867606B91FECF74D35  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// System.Byte[] System.Net.RequestStream::buffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___buffer_5;
	// System.Int32 System.Net.RequestStream::offset
	int32_t ___offset_6;
	// System.Int32 System.Net.RequestStream::length
	int32_t ___length_7;
	// System.Int64 System.Net.RequestStream::remaining_body
	int64_t ___remaining_body_8;
	// System.Boolean System.Net.RequestStream::disposed
	bool ___disposed_9;
	// System.IO.Stream System.Net.RequestStream::stream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___stream_10;

public:
	inline static int32_t get_offset_of_buffer_5() { return static_cast<int32_t>(offsetof(RequestStream_t4BF6FD18AB7261094F557E867606B91FECF74D35, ___buffer_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_buffer_5() const { return ___buffer_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_buffer_5() { return &___buffer_5; }
	inline void set_buffer_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___buffer_5 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_5), value);
	}

	inline static int32_t get_offset_of_offset_6() { return static_cast<int32_t>(offsetof(RequestStream_t4BF6FD18AB7261094F557E867606B91FECF74D35, ___offset_6)); }
	inline int32_t get_offset_6() const { return ___offset_6; }
	inline int32_t* get_address_of_offset_6() { return &___offset_6; }
	inline void set_offset_6(int32_t value)
	{
		___offset_6 = value;
	}

	inline static int32_t get_offset_of_length_7() { return static_cast<int32_t>(offsetof(RequestStream_t4BF6FD18AB7261094F557E867606B91FECF74D35, ___length_7)); }
	inline int32_t get_length_7() const { return ___length_7; }
	inline int32_t* get_address_of_length_7() { return &___length_7; }
	inline void set_length_7(int32_t value)
	{
		___length_7 = value;
	}

	inline static int32_t get_offset_of_remaining_body_8() { return static_cast<int32_t>(offsetof(RequestStream_t4BF6FD18AB7261094F557E867606B91FECF74D35, ___remaining_body_8)); }
	inline int64_t get_remaining_body_8() const { return ___remaining_body_8; }
	inline int64_t* get_address_of_remaining_body_8() { return &___remaining_body_8; }
	inline void set_remaining_body_8(int64_t value)
	{
		___remaining_body_8 = value;
	}

	inline static int32_t get_offset_of_disposed_9() { return static_cast<int32_t>(offsetof(RequestStream_t4BF6FD18AB7261094F557E867606B91FECF74D35, ___disposed_9)); }
	inline bool get_disposed_9() const { return ___disposed_9; }
	inline bool* get_address_of_disposed_9() { return &___disposed_9; }
	inline void set_disposed_9(bool value)
	{
		___disposed_9 = value;
	}

	inline static int32_t get_offset_of_stream_10() { return static_cast<int32_t>(offsetof(RequestStream_t4BF6FD18AB7261094F557E867606B91FECF74D35, ___stream_10)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_stream_10() const { return ___stream_10; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_stream_10() { return &___stream_10; }
	inline void set_stream_10(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___stream_10 = value;
		Il2CppCodeGenWriteBarrier((&___stream_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUESTSTREAM_T4BF6FD18AB7261094F557E867606B91FECF74D35_H
#ifndef RESPONSESTREAM_TE537A0E20974BEB6629CBEBE42C27C9B97126032_H
#define RESPONSESTREAM_TE537A0E20974BEB6629CBEBE42C27C9B97126032_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.ResponseStream
struct  ResponseStream_tE537A0E20974BEB6629CBEBE42C27C9B97126032  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// System.Net.HttpListenerResponse System.Net.ResponseStream::response
	HttpListenerResponse_t34AC9D467928315CC4285B8A14517C6D5564224C * ___response_5;
	// System.Boolean System.Net.ResponseStream::ignore_errors
	bool ___ignore_errors_6;
	// System.Boolean System.Net.ResponseStream::disposed
	bool ___disposed_7;
	// System.Boolean System.Net.ResponseStream::trailer_sent
	bool ___trailer_sent_8;
	// System.IO.Stream System.Net.ResponseStream::stream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___stream_9;

public:
	inline static int32_t get_offset_of_response_5() { return static_cast<int32_t>(offsetof(ResponseStream_tE537A0E20974BEB6629CBEBE42C27C9B97126032, ___response_5)); }
	inline HttpListenerResponse_t34AC9D467928315CC4285B8A14517C6D5564224C * get_response_5() const { return ___response_5; }
	inline HttpListenerResponse_t34AC9D467928315CC4285B8A14517C6D5564224C ** get_address_of_response_5() { return &___response_5; }
	inline void set_response_5(HttpListenerResponse_t34AC9D467928315CC4285B8A14517C6D5564224C * value)
	{
		___response_5 = value;
		Il2CppCodeGenWriteBarrier((&___response_5), value);
	}

	inline static int32_t get_offset_of_ignore_errors_6() { return static_cast<int32_t>(offsetof(ResponseStream_tE537A0E20974BEB6629CBEBE42C27C9B97126032, ___ignore_errors_6)); }
	inline bool get_ignore_errors_6() const { return ___ignore_errors_6; }
	inline bool* get_address_of_ignore_errors_6() { return &___ignore_errors_6; }
	inline void set_ignore_errors_6(bool value)
	{
		___ignore_errors_6 = value;
	}

	inline static int32_t get_offset_of_disposed_7() { return static_cast<int32_t>(offsetof(ResponseStream_tE537A0E20974BEB6629CBEBE42C27C9B97126032, ___disposed_7)); }
	inline bool get_disposed_7() const { return ___disposed_7; }
	inline bool* get_address_of_disposed_7() { return &___disposed_7; }
	inline void set_disposed_7(bool value)
	{
		___disposed_7 = value;
	}

	inline static int32_t get_offset_of_trailer_sent_8() { return static_cast<int32_t>(offsetof(ResponseStream_tE537A0E20974BEB6629CBEBE42C27C9B97126032, ___trailer_sent_8)); }
	inline bool get_trailer_sent_8() const { return ___trailer_sent_8; }
	inline bool* get_address_of_trailer_sent_8() { return &___trailer_sent_8; }
	inline void set_trailer_sent_8(bool value)
	{
		___trailer_sent_8 = value;
	}

	inline static int32_t get_offset_of_stream_9() { return static_cast<int32_t>(offsetof(ResponseStream_tE537A0E20974BEB6629CBEBE42C27C9B97126032, ___stream_9)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_stream_9() const { return ___stream_9; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_stream_9() { return &___stream_9; }
	inline void set_stream_9(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___stream_9 = value;
		Il2CppCodeGenWriteBarrier((&___stream_9), value);
	}
};

struct ResponseStream_tE537A0E20974BEB6629CBEBE42C27C9B97126032_StaticFields
{
public:
	// System.Byte[] System.Net.ResponseStream::crlf
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___crlf_10;

public:
	inline static int32_t get_offset_of_crlf_10() { return static_cast<int32_t>(offsetof(ResponseStream_tE537A0E20974BEB6629CBEBE42C27C9B97126032_StaticFields, ___crlf_10)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_crlf_10() const { return ___crlf_10; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_crlf_10() { return &___crlf_10; }
	inline void set_crlf_10(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___crlf_10 = value;
		Il2CppCodeGenWriteBarrier((&___crlf_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESPONSESTREAM_TE537A0E20974BEB6629CBEBE42C27C9B97126032_H
#ifndef AUTHENTICATIONLEVEL_TC0FE8B3A1A9C4F39798DD6F6C024078BB137F52B_H
#define AUTHENTICATIONLEVEL_TC0FE8B3A1A9C4F39798DD6F6C024078BB137F52B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Security.AuthenticationLevel
struct  AuthenticationLevel_tC0FE8B3A1A9C4F39798DD6F6C024078BB137F52B 
{
public:
	// System.Int32 System.Net.Security.AuthenticationLevel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AuthenticationLevel_tC0FE8B3A1A9C4F39798DD6F6C024078BB137F52B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTHENTICATIONLEVEL_TC0FE8B3A1A9C4F39798DD6F6C024078BB137F52B_H
#ifndef SECURITYPROTOCOLTYPE_T5A4A36AC58D7FE75545BAB63E4FACE97C7FFE941_H
#define SECURITYPROTOCOLTYPE_T5A4A36AC58D7FE75545BAB63E4FACE97C7FFE941_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.SecurityProtocolType
struct  SecurityProtocolType_t5A4A36AC58D7FE75545BAB63E4FACE97C7FFE941 
{
public:
	// System.Int32 System.Net.SecurityProtocolType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SecurityProtocolType_t5A4A36AC58D7FE75545BAB63E4FACE97C7FFE941, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECURITYPROTOCOLTYPE_T5A4A36AC58D7FE75545BAB63E4FACE97C7FFE941_H
#ifndef SERVICEPOINT_T5F42B1A9D56E09B4B051BE0968C81DE3128E3EB4_H
#define SERVICEPOINT_T5F42B1A9D56E09B4B051BE0968C81DE3128E3EB4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.ServicePoint
struct  ServicePoint_t5F42B1A9D56E09B4B051BE0968C81DE3128E3EB4  : public RuntimeObject
{
public:
	// System.Uri System.Net.ServicePoint::uri
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___uri_0;
	// System.Int32 System.Net.ServicePoint::connectionLimit
	int32_t ___connectionLimit_1;
	// System.Int32 System.Net.ServicePoint::maxIdleTime
	int32_t ___maxIdleTime_2;
	// System.Int32 System.Net.ServicePoint::currentConnections
	int32_t ___currentConnections_3;
	// System.DateTime System.Net.ServicePoint::idleSince
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___idleSince_4;
	// System.DateTime System.Net.ServicePoint::lastDnsResolve
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___lastDnsResolve_5;
	// System.Version System.Net.ServicePoint::protocolVersion
	Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD * ___protocolVersion_6;
	// System.Net.IPHostEntry System.Net.ServicePoint::host
	IPHostEntry_tB00EABDF75DB19AEAD4F3E7D93BFD7BAE558149D * ___host_7;
	// System.Boolean System.Net.ServicePoint::usesProxy
	bool ___usesProxy_8;
	// System.Collections.Generic.Dictionary`2<System.String,System.Net.WebConnectionGroup> System.Net.ServicePoint::groups
	Dictionary_2_t4CAF579D576CCEDF0310DD80EFB19ACBE04267D8 * ___groups_9;
	// System.Boolean System.Net.ServicePoint::sendContinue
	bool ___sendContinue_10;
	// System.Boolean System.Net.ServicePoint::useConnect
	bool ___useConnect_11;
	// System.Object System.Net.ServicePoint::hostE
	RuntimeObject * ___hostE_12;
	// System.Boolean System.Net.ServicePoint::useNagle
	bool ___useNagle_13;
	// System.Net.BindIPEndPoint System.Net.ServicePoint::endPointCallback
	BindIPEndPoint_t6B179B1AD32AF233C8C8E6440DFEF78153A851B9 * ___endPointCallback_14;
	// System.Boolean System.Net.ServicePoint::tcp_keepalive
	bool ___tcp_keepalive_15;
	// System.Int32 System.Net.ServicePoint::tcp_keepalive_time
	int32_t ___tcp_keepalive_time_16;
	// System.Int32 System.Net.ServicePoint::tcp_keepalive_interval
	int32_t ___tcp_keepalive_interval_17;
	// System.Threading.Timer System.Net.ServicePoint::idleTimer
	Timer_t67FAB8E41573B4FA09CA56AE30725AF4297C2553 * ___idleTimer_18;
	// System.Object System.Net.ServicePoint::m_ServerCertificateOrBytes
	RuntimeObject * ___m_ServerCertificateOrBytes_19;
	// System.Object System.Net.ServicePoint::m_ClientCertificateOrBytes
	RuntimeObject * ___m_ClientCertificateOrBytes_20;

public:
	inline static int32_t get_offset_of_uri_0() { return static_cast<int32_t>(offsetof(ServicePoint_t5F42B1A9D56E09B4B051BE0968C81DE3128E3EB4, ___uri_0)); }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * get_uri_0() const { return ___uri_0; }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E ** get_address_of_uri_0() { return &___uri_0; }
	inline void set_uri_0(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * value)
	{
		___uri_0 = value;
		Il2CppCodeGenWriteBarrier((&___uri_0), value);
	}

	inline static int32_t get_offset_of_connectionLimit_1() { return static_cast<int32_t>(offsetof(ServicePoint_t5F42B1A9D56E09B4B051BE0968C81DE3128E3EB4, ___connectionLimit_1)); }
	inline int32_t get_connectionLimit_1() const { return ___connectionLimit_1; }
	inline int32_t* get_address_of_connectionLimit_1() { return &___connectionLimit_1; }
	inline void set_connectionLimit_1(int32_t value)
	{
		___connectionLimit_1 = value;
	}

	inline static int32_t get_offset_of_maxIdleTime_2() { return static_cast<int32_t>(offsetof(ServicePoint_t5F42B1A9D56E09B4B051BE0968C81DE3128E3EB4, ___maxIdleTime_2)); }
	inline int32_t get_maxIdleTime_2() const { return ___maxIdleTime_2; }
	inline int32_t* get_address_of_maxIdleTime_2() { return &___maxIdleTime_2; }
	inline void set_maxIdleTime_2(int32_t value)
	{
		___maxIdleTime_2 = value;
	}

	inline static int32_t get_offset_of_currentConnections_3() { return static_cast<int32_t>(offsetof(ServicePoint_t5F42B1A9D56E09B4B051BE0968C81DE3128E3EB4, ___currentConnections_3)); }
	inline int32_t get_currentConnections_3() const { return ___currentConnections_3; }
	inline int32_t* get_address_of_currentConnections_3() { return &___currentConnections_3; }
	inline void set_currentConnections_3(int32_t value)
	{
		___currentConnections_3 = value;
	}

	inline static int32_t get_offset_of_idleSince_4() { return static_cast<int32_t>(offsetof(ServicePoint_t5F42B1A9D56E09B4B051BE0968C81DE3128E3EB4, ___idleSince_4)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_idleSince_4() const { return ___idleSince_4; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_idleSince_4() { return &___idleSince_4; }
	inline void set_idleSince_4(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___idleSince_4 = value;
	}

	inline static int32_t get_offset_of_lastDnsResolve_5() { return static_cast<int32_t>(offsetof(ServicePoint_t5F42B1A9D56E09B4B051BE0968C81DE3128E3EB4, ___lastDnsResolve_5)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_lastDnsResolve_5() const { return ___lastDnsResolve_5; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_lastDnsResolve_5() { return &___lastDnsResolve_5; }
	inline void set_lastDnsResolve_5(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___lastDnsResolve_5 = value;
	}

	inline static int32_t get_offset_of_protocolVersion_6() { return static_cast<int32_t>(offsetof(ServicePoint_t5F42B1A9D56E09B4B051BE0968C81DE3128E3EB4, ___protocolVersion_6)); }
	inline Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD * get_protocolVersion_6() const { return ___protocolVersion_6; }
	inline Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD ** get_address_of_protocolVersion_6() { return &___protocolVersion_6; }
	inline void set_protocolVersion_6(Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD * value)
	{
		___protocolVersion_6 = value;
		Il2CppCodeGenWriteBarrier((&___protocolVersion_6), value);
	}

	inline static int32_t get_offset_of_host_7() { return static_cast<int32_t>(offsetof(ServicePoint_t5F42B1A9D56E09B4B051BE0968C81DE3128E3EB4, ___host_7)); }
	inline IPHostEntry_tB00EABDF75DB19AEAD4F3E7D93BFD7BAE558149D * get_host_7() const { return ___host_7; }
	inline IPHostEntry_tB00EABDF75DB19AEAD4F3E7D93BFD7BAE558149D ** get_address_of_host_7() { return &___host_7; }
	inline void set_host_7(IPHostEntry_tB00EABDF75DB19AEAD4F3E7D93BFD7BAE558149D * value)
	{
		___host_7 = value;
		Il2CppCodeGenWriteBarrier((&___host_7), value);
	}

	inline static int32_t get_offset_of_usesProxy_8() { return static_cast<int32_t>(offsetof(ServicePoint_t5F42B1A9D56E09B4B051BE0968C81DE3128E3EB4, ___usesProxy_8)); }
	inline bool get_usesProxy_8() const { return ___usesProxy_8; }
	inline bool* get_address_of_usesProxy_8() { return &___usesProxy_8; }
	inline void set_usesProxy_8(bool value)
	{
		___usesProxy_8 = value;
	}

	inline static int32_t get_offset_of_groups_9() { return static_cast<int32_t>(offsetof(ServicePoint_t5F42B1A9D56E09B4B051BE0968C81DE3128E3EB4, ___groups_9)); }
	inline Dictionary_2_t4CAF579D576CCEDF0310DD80EFB19ACBE04267D8 * get_groups_9() const { return ___groups_9; }
	inline Dictionary_2_t4CAF579D576CCEDF0310DD80EFB19ACBE04267D8 ** get_address_of_groups_9() { return &___groups_9; }
	inline void set_groups_9(Dictionary_2_t4CAF579D576CCEDF0310DD80EFB19ACBE04267D8 * value)
	{
		___groups_9 = value;
		Il2CppCodeGenWriteBarrier((&___groups_9), value);
	}

	inline static int32_t get_offset_of_sendContinue_10() { return static_cast<int32_t>(offsetof(ServicePoint_t5F42B1A9D56E09B4B051BE0968C81DE3128E3EB4, ___sendContinue_10)); }
	inline bool get_sendContinue_10() const { return ___sendContinue_10; }
	inline bool* get_address_of_sendContinue_10() { return &___sendContinue_10; }
	inline void set_sendContinue_10(bool value)
	{
		___sendContinue_10 = value;
	}

	inline static int32_t get_offset_of_useConnect_11() { return static_cast<int32_t>(offsetof(ServicePoint_t5F42B1A9D56E09B4B051BE0968C81DE3128E3EB4, ___useConnect_11)); }
	inline bool get_useConnect_11() const { return ___useConnect_11; }
	inline bool* get_address_of_useConnect_11() { return &___useConnect_11; }
	inline void set_useConnect_11(bool value)
	{
		___useConnect_11 = value;
	}

	inline static int32_t get_offset_of_hostE_12() { return static_cast<int32_t>(offsetof(ServicePoint_t5F42B1A9D56E09B4B051BE0968C81DE3128E3EB4, ___hostE_12)); }
	inline RuntimeObject * get_hostE_12() const { return ___hostE_12; }
	inline RuntimeObject ** get_address_of_hostE_12() { return &___hostE_12; }
	inline void set_hostE_12(RuntimeObject * value)
	{
		___hostE_12 = value;
		Il2CppCodeGenWriteBarrier((&___hostE_12), value);
	}

	inline static int32_t get_offset_of_useNagle_13() { return static_cast<int32_t>(offsetof(ServicePoint_t5F42B1A9D56E09B4B051BE0968C81DE3128E3EB4, ___useNagle_13)); }
	inline bool get_useNagle_13() const { return ___useNagle_13; }
	inline bool* get_address_of_useNagle_13() { return &___useNagle_13; }
	inline void set_useNagle_13(bool value)
	{
		___useNagle_13 = value;
	}

	inline static int32_t get_offset_of_endPointCallback_14() { return static_cast<int32_t>(offsetof(ServicePoint_t5F42B1A9D56E09B4B051BE0968C81DE3128E3EB4, ___endPointCallback_14)); }
	inline BindIPEndPoint_t6B179B1AD32AF233C8C8E6440DFEF78153A851B9 * get_endPointCallback_14() const { return ___endPointCallback_14; }
	inline BindIPEndPoint_t6B179B1AD32AF233C8C8E6440DFEF78153A851B9 ** get_address_of_endPointCallback_14() { return &___endPointCallback_14; }
	inline void set_endPointCallback_14(BindIPEndPoint_t6B179B1AD32AF233C8C8E6440DFEF78153A851B9 * value)
	{
		___endPointCallback_14 = value;
		Il2CppCodeGenWriteBarrier((&___endPointCallback_14), value);
	}

	inline static int32_t get_offset_of_tcp_keepalive_15() { return static_cast<int32_t>(offsetof(ServicePoint_t5F42B1A9D56E09B4B051BE0968C81DE3128E3EB4, ___tcp_keepalive_15)); }
	inline bool get_tcp_keepalive_15() const { return ___tcp_keepalive_15; }
	inline bool* get_address_of_tcp_keepalive_15() { return &___tcp_keepalive_15; }
	inline void set_tcp_keepalive_15(bool value)
	{
		___tcp_keepalive_15 = value;
	}

	inline static int32_t get_offset_of_tcp_keepalive_time_16() { return static_cast<int32_t>(offsetof(ServicePoint_t5F42B1A9D56E09B4B051BE0968C81DE3128E3EB4, ___tcp_keepalive_time_16)); }
	inline int32_t get_tcp_keepalive_time_16() const { return ___tcp_keepalive_time_16; }
	inline int32_t* get_address_of_tcp_keepalive_time_16() { return &___tcp_keepalive_time_16; }
	inline void set_tcp_keepalive_time_16(int32_t value)
	{
		___tcp_keepalive_time_16 = value;
	}

	inline static int32_t get_offset_of_tcp_keepalive_interval_17() { return static_cast<int32_t>(offsetof(ServicePoint_t5F42B1A9D56E09B4B051BE0968C81DE3128E3EB4, ___tcp_keepalive_interval_17)); }
	inline int32_t get_tcp_keepalive_interval_17() const { return ___tcp_keepalive_interval_17; }
	inline int32_t* get_address_of_tcp_keepalive_interval_17() { return &___tcp_keepalive_interval_17; }
	inline void set_tcp_keepalive_interval_17(int32_t value)
	{
		___tcp_keepalive_interval_17 = value;
	}

	inline static int32_t get_offset_of_idleTimer_18() { return static_cast<int32_t>(offsetof(ServicePoint_t5F42B1A9D56E09B4B051BE0968C81DE3128E3EB4, ___idleTimer_18)); }
	inline Timer_t67FAB8E41573B4FA09CA56AE30725AF4297C2553 * get_idleTimer_18() const { return ___idleTimer_18; }
	inline Timer_t67FAB8E41573B4FA09CA56AE30725AF4297C2553 ** get_address_of_idleTimer_18() { return &___idleTimer_18; }
	inline void set_idleTimer_18(Timer_t67FAB8E41573B4FA09CA56AE30725AF4297C2553 * value)
	{
		___idleTimer_18 = value;
		Il2CppCodeGenWriteBarrier((&___idleTimer_18), value);
	}

	inline static int32_t get_offset_of_m_ServerCertificateOrBytes_19() { return static_cast<int32_t>(offsetof(ServicePoint_t5F42B1A9D56E09B4B051BE0968C81DE3128E3EB4, ___m_ServerCertificateOrBytes_19)); }
	inline RuntimeObject * get_m_ServerCertificateOrBytes_19() const { return ___m_ServerCertificateOrBytes_19; }
	inline RuntimeObject ** get_address_of_m_ServerCertificateOrBytes_19() { return &___m_ServerCertificateOrBytes_19; }
	inline void set_m_ServerCertificateOrBytes_19(RuntimeObject * value)
	{
		___m_ServerCertificateOrBytes_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_ServerCertificateOrBytes_19), value);
	}

	inline static int32_t get_offset_of_m_ClientCertificateOrBytes_20() { return static_cast<int32_t>(offsetof(ServicePoint_t5F42B1A9D56E09B4B051BE0968C81DE3128E3EB4, ___m_ClientCertificateOrBytes_20)); }
	inline RuntimeObject * get_m_ClientCertificateOrBytes_20() const { return ___m_ClientCertificateOrBytes_20; }
	inline RuntimeObject ** get_address_of_m_ClientCertificateOrBytes_20() { return &___m_ClientCertificateOrBytes_20; }
	inline void set_m_ClientCertificateOrBytes_20(RuntimeObject * value)
	{
		___m_ClientCertificateOrBytes_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_ClientCertificateOrBytes_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERVICEPOINT_T5F42B1A9D56E09B4B051BE0968C81DE3128E3EB4_H
#ifndef SIMPLEASYNCRESULT_TA572851810F8E279EE9E5378A6D9A538B1822FC6_H
#define SIMPLEASYNCRESULT_TA572851810F8E279EE9E5378A6D9A538B1822FC6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.SimpleAsyncResult
struct  SimpleAsyncResult_tA572851810F8E279EE9E5378A6D9A538B1822FC6  : public RuntimeObject
{
public:
	// System.Threading.ManualResetEvent System.Net.SimpleAsyncResult::handle
	ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * ___handle_0;
	// System.Boolean System.Net.SimpleAsyncResult::synch
	bool ___synch_1;
	// System.Boolean System.Net.SimpleAsyncResult::isCompleted
	bool ___isCompleted_2;
	// System.Net.SimpleAsyncCallback System.Net.SimpleAsyncResult::cb
	SimpleAsyncCallback_t690665AFDCBDEBA379B6F4CD213CB4BB8570F83F * ___cb_3;
	// System.Object System.Net.SimpleAsyncResult::state
	RuntimeObject * ___state_4;
	// System.Boolean System.Net.SimpleAsyncResult::callbackDone
	bool ___callbackDone_5;
	// System.Exception System.Net.SimpleAsyncResult::exc
	Exception_t * ___exc_6;
	// System.Object System.Net.SimpleAsyncResult::locker
	RuntimeObject * ___locker_7;
	// System.Nullable`1<System.Boolean> System.Net.SimpleAsyncResult::user_read_synch
	Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  ___user_read_synch_8;

public:
	inline static int32_t get_offset_of_handle_0() { return static_cast<int32_t>(offsetof(SimpleAsyncResult_tA572851810F8E279EE9E5378A6D9A538B1822FC6, ___handle_0)); }
	inline ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * get_handle_0() const { return ___handle_0; }
	inline ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 ** get_address_of_handle_0() { return &___handle_0; }
	inline void set_handle_0(ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * value)
	{
		___handle_0 = value;
		Il2CppCodeGenWriteBarrier((&___handle_0), value);
	}

	inline static int32_t get_offset_of_synch_1() { return static_cast<int32_t>(offsetof(SimpleAsyncResult_tA572851810F8E279EE9E5378A6D9A538B1822FC6, ___synch_1)); }
	inline bool get_synch_1() const { return ___synch_1; }
	inline bool* get_address_of_synch_1() { return &___synch_1; }
	inline void set_synch_1(bool value)
	{
		___synch_1 = value;
	}

	inline static int32_t get_offset_of_isCompleted_2() { return static_cast<int32_t>(offsetof(SimpleAsyncResult_tA572851810F8E279EE9E5378A6D9A538B1822FC6, ___isCompleted_2)); }
	inline bool get_isCompleted_2() const { return ___isCompleted_2; }
	inline bool* get_address_of_isCompleted_2() { return &___isCompleted_2; }
	inline void set_isCompleted_2(bool value)
	{
		___isCompleted_2 = value;
	}

	inline static int32_t get_offset_of_cb_3() { return static_cast<int32_t>(offsetof(SimpleAsyncResult_tA572851810F8E279EE9E5378A6D9A538B1822FC6, ___cb_3)); }
	inline SimpleAsyncCallback_t690665AFDCBDEBA379B6F4CD213CB4BB8570F83F * get_cb_3() const { return ___cb_3; }
	inline SimpleAsyncCallback_t690665AFDCBDEBA379B6F4CD213CB4BB8570F83F ** get_address_of_cb_3() { return &___cb_3; }
	inline void set_cb_3(SimpleAsyncCallback_t690665AFDCBDEBA379B6F4CD213CB4BB8570F83F * value)
	{
		___cb_3 = value;
		Il2CppCodeGenWriteBarrier((&___cb_3), value);
	}

	inline static int32_t get_offset_of_state_4() { return static_cast<int32_t>(offsetof(SimpleAsyncResult_tA572851810F8E279EE9E5378A6D9A538B1822FC6, ___state_4)); }
	inline RuntimeObject * get_state_4() const { return ___state_4; }
	inline RuntimeObject ** get_address_of_state_4() { return &___state_4; }
	inline void set_state_4(RuntimeObject * value)
	{
		___state_4 = value;
		Il2CppCodeGenWriteBarrier((&___state_4), value);
	}

	inline static int32_t get_offset_of_callbackDone_5() { return static_cast<int32_t>(offsetof(SimpleAsyncResult_tA572851810F8E279EE9E5378A6D9A538B1822FC6, ___callbackDone_5)); }
	inline bool get_callbackDone_5() const { return ___callbackDone_5; }
	inline bool* get_address_of_callbackDone_5() { return &___callbackDone_5; }
	inline void set_callbackDone_5(bool value)
	{
		___callbackDone_5 = value;
	}

	inline static int32_t get_offset_of_exc_6() { return static_cast<int32_t>(offsetof(SimpleAsyncResult_tA572851810F8E279EE9E5378A6D9A538B1822FC6, ___exc_6)); }
	inline Exception_t * get_exc_6() const { return ___exc_6; }
	inline Exception_t ** get_address_of_exc_6() { return &___exc_6; }
	inline void set_exc_6(Exception_t * value)
	{
		___exc_6 = value;
		Il2CppCodeGenWriteBarrier((&___exc_6), value);
	}

	inline static int32_t get_offset_of_locker_7() { return static_cast<int32_t>(offsetof(SimpleAsyncResult_tA572851810F8E279EE9E5378A6D9A538B1822FC6, ___locker_7)); }
	inline RuntimeObject * get_locker_7() const { return ___locker_7; }
	inline RuntimeObject ** get_address_of_locker_7() { return &___locker_7; }
	inline void set_locker_7(RuntimeObject * value)
	{
		___locker_7 = value;
		Il2CppCodeGenWriteBarrier((&___locker_7), value);
	}

	inline static int32_t get_offset_of_user_read_synch_8() { return static_cast<int32_t>(offsetof(SimpleAsyncResult_tA572851810F8E279EE9E5378A6D9A538B1822FC6, ___user_read_synch_8)); }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  get_user_read_synch_8() const { return ___user_read_synch_8; }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 * get_address_of_user_read_synch_8() { return &___user_read_synch_8; }
	inline void set_user_read_synch_8(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  value)
	{
		___user_read_synch_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLEASYNCRESULT_TA572851810F8E279EE9E5378A6D9A538B1822FC6_H
#ifndef ADDRESSFAMILY_TFA4F79FA7F299EBDF507F4811E6E5C3EEBF0850E_H
#define ADDRESSFAMILY_TFA4F79FA7F299EBDF507F4811E6E5C3EEBF0850E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Sockets.AddressFamily
struct  AddressFamily_tFA4F79FA7F299EBDF507F4811E6E5C3EEBF0850E 
{
public:
	// System.Int32 System.Net.Sockets.AddressFamily::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AddressFamily_tFA4F79FA7F299EBDF507F4811E6E5C3EEBF0850E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADDRESSFAMILY_TFA4F79FA7F299EBDF507F4811E6E5C3EEBF0850E_H
#ifndef IOCONTROLCODE_T8A59BB74289B0C9BBB1659E249E54BC5A205D6B9_H
#define IOCONTROLCODE_T8A59BB74289B0C9BBB1659E249E54BC5A205D6B9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Sockets.IOControlCode
struct  IOControlCode_t8A59BB74289B0C9BBB1659E249E54BC5A205D6B9 
{
public:
	// System.Int64 System.Net.Sockets.IOControlCode::value__
	int64_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(IOControlCode_t8A59BB74289B0C9BBB1659E249E54BC5A205D6B9, ___value___2)); }
	inline int64_t get_value___2() const { return ___value___2; }
	inline int64_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int64_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOCONTROLCODE_T8A59BB74289B0C9BBB1659E249E54BC5A205D6B9_H
#ifndef IPPROTECTIONLEVEL_T63BF0274CCC5A1BFF42B658316B3092B8C0AA95E_H
#define IPPROTECTIONLEVEL_T63BF0274CCC5A1BFF42B658316B3092B8C0AA95E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Sockets.IPProtectionLevel
struct  IPProtectionLevel_t63BF0274CCC5A1BFF42B658316B3092B8C0AA95E 
{
public:
	// System.Int32 System.Net.Sockets.IPProtectionLevel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(IPProtectionLevel_t63BF0274CCC5A1BFF42B658316B3092B8C0AA95E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IPPROTECTIONLEVEL_T63BF0274CCC5A1BFF42B658316B3092B8C0AA95E_H
#ifndef NETWORKSTREAM_T362D0CD0C74C2F5CBD02905C9422E4240872ADCA_H
#define NETWORKSTREAM_T362D0CD0C74C2F5CBD02905C9422E4240872ADCA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Sockets.NetworkStream
struct  NetworkStream_t362D0CD0C74C2F5CBD02905C9422E4240872ADCA  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// System.Net.Sockets.Socket System.Net.Sockets.NetworkStream::m_StreamSocket
	Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 * ___m_StreamSocket_5;
	// System.Boolean System.Net.Sockets.NetworkStream::m_Readable
	bool ___m_Readable_6;
	// System.Boolean System.Net.Sockets.NetworkStream::m_Writeable
	bool ___m_Writeable_7;
	// System.Boolean System.Net.Sockets.NetworkStream::m_OwnsSocket
	bool ___m_OwnsSocket_8;
	// System.Int32 System.Net.Sockets.NetworkStream::m_CloseTimeout
	int32_t ___m_CloseTimeout_9;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.Net.Sockets.NetworkStream::m_CleanedUp
	bool ___m_CleanedUp_10;
	// System.Int32 System.Net.Sockets.NetworkStream::m_CurrentReadTimeout
	int32_t ___m_CurrentReadTimeout_11;
	// System.Int32 System.Net.Sockets.NetworkStream::m_CurrentWriteTimeout
	int32_t ___m_CurrentWriteTimeout_12;

public:
	inline static int32_t get_offset_of_m_StreamSocket_5() { return static_cast<int32_t>(offsetof(NetworkStream_t362D0CD0C74C2F5CBD02905C9422E4240872ADCA, ___m_StreamSocket_5)); }
	inline Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 * get_m_StreamSocket_5() const { return ___m_StreamSocket_5; }
	inline Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 ** get_address_of_m_StreamSocket_5() { return &___m_StreamSocket_5; }
	inline void set_m_StreamSocket_5(Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 * value)
	{
		___m_StreamSocket_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_StreamSocket_5), value);
	}

	inline static int32_t get_offset_of_m_Readable_6() { return static_cast<int32_t>(offsetof(NetworkStream_t362D0CD0C74C2F5CBD02905C9422E4240872ADCA, ___m_Readable_6)); }
	inline bool get_m_Readable_6() const { return ___m_Readable_6; }
	inline bool* get_address_of_m_Readable_6() { return &___m_Readable_6; }
	inline void set_m_Readable_6(bool value)
	{
		___m_Readable_6 = value;
	}

	inline static int32_t get_offset_of_m_Writeable_7() { return static_cast<int32_t>(offsetof(NetworkStream_t362D0CD0C74C2F5CBD02905C9422E4240872ADCA, ___m_Writeable_7)); }
	inline bool get_m_Writeable_7() const { return ___m_Writeable_7; }
	inline bool* get_address_of_m_Writeable_7() { return &___m_Writeable_7; }
	inline void set_m_Writeable_7(bool value)
	{
		___m_Writeable_7 = value;
	}

	inline static int32_t get_offset_of_m_OwnsSocket_8() { return static_cast<int32_t>(offsetof(NetworkStream_t362D0CD0C74C2F5CBD02905C9422E4240872ADCA, ___m_OwnsSocket_8)); }
	inline bool get_m_OwnsSocket_8() const { return ___m_OwnsSocket_8; }
	inline bool* get_address_of_m_OwnsSocket_8() { return &___m_OwnsSocket_8; }
	inline void set_m_OwnsSocket_8(bool value)
	{
		___m_OwnsSocket_8 = value;
	}

	inline static int32_t get_offset_of_m_CloseTimeout_9() { return static_cast<int32_t>(offsetof(NetworkStream_t362D0CD0C74C2F5CBD02905C9422E4240872ADCA, ___m_CloseTimeout_9)); }
	inline int32_t get_m_CloseTimeout_9() const { return ___m_CloseTimeout_9; }
	inline int32_t* get_address_of_m_CloseTimeout_9() { return &___m_CloseTimeout_9; }
	inline void set_m_CloseTimeout_9(int32_t value)
	{
		___m_CloseTimeout_9 = value;
	}

	inline static int32_t get_offset_of_m_CleanedUp_10() { return static_cast<int32_t>(offsetof(NetworkStream_t362D0CD0C74C2F5CBD02905C9422E4240872ADCA, ___m_CleanedUp_10)); }
	inline bool get_m_CleanedUp_10() const { return ___m_CleanedUp_10; }
	inline bool* get_address_of_m_CleanedUp_10() { return &___m_CleanedUp_10; }
	inline void set_m_CleanedUp_10(bool value)
	{
		___m_CleanedUp_10 = value;
	}

	inline static int32_t get_offset_of_m_CurrentReadTimeout_11() { return static_cast<int32_t>(offsetof(NetworkStream_t362D0CD0C74C2F5CBD02905C9422E4240872ADCA, ___m_CurrentReadTimeout_11)); }
	inline int32_t get_m_CurrentReadTimeout_11() const { return ___m_CurrentReadTimeout_11; }
	inline int32_t* get_address_of_m_CurrentReadTimeout_11() { return &___m_CurrentReadTimeout_11; }
	inline void set_m_CurrentReadTimeout_11(int32_t value)
	{
		___m_CurrentReadTimeout_11 = value;
	}

	inline static int32_t get_offset_of_m_CurrentWriteTimeout_12() { return static_cast<int32_t>(offsetof(NetworkStream_t362D0CD0C74C2F5CBD02905C9422E4240872ADCA, ___m_CurrentWriteTimeout_12)); }
	inline int32_t get_m_CurrentWriteTimeout_12() const { return ___m_CurrentWriteTimeout_12; }
	inline int32_t* get_address_of_m_CurrentWriteTimeout_12() { return &___m_CurrentWriteTimeout_12; }
	inline void set_m_CurrentWriteTimeout_12(int32_t value)
	{
		___m_CurrentWriteTimeout_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKSTREAM_T362D0CD0C74C2F5CBD02905C9422E4240872ADCA_H
#ifndef PROTOCOLTYPE_T20E72BC88D85E41793731DC987F8F04F312D66DD_H
#define PROTOCOLTYPE_T20E72BC88D85E41793731DC987F8F04F312D66DD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Sockets.ProtocolType
struct  ProtocolType_t20E72BC88D85E41793731DC987F8F04F312D66DD 
{
public:
	// System.Int32 System.Net.Sockets.ProtocolType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ProtocolType_t20E72BC88D85E41793731DC987F8F04F312D66DD, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROTOCOLTYPE_T20E72BC88D85E41793731DC987F8F04F312D66DD_H
#ifndef SELECTMODE_T384C0C7786507E841593ADDA6785DF0001C06B7B_H
#define SELECTMODE_T384C0C7786507E841593ADDA6785DF0001C06B7B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Sockets.SelectMode
struct  SelectMode_t384C0C7786507E841593ADDA6785DF0001C06B7B 
{
public:
	// System.Int32 System.Net.Sockets.SelectMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SelectMode_t384C0C7786507E841593ADDA6785DF0001C06B7B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTMODE_T384C0C7786507E841593ADDA6785DF0001C06B7B_H
#ifndef U3CU3EC__DISPLAYCLASS298_0_TCCC599BC4750E6215E11C91C2037B4694B3E23E7_H
#define U3CU3EC__DISPLAYCLASS298_0_TCCC599BC4750E6215E11C91C2037B4694B3E23E7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Sockets.Socket_<>c__DisplayClass298_0
struct  U3CU3Ec__DisplayClass298_0_tCCC599BC4750E6215E11C91C2037B4694B3E23E7  : public RuntimeObject
{
public:
	// System.Net.Sockets.Socket System.Net.Sockets.Socket_<>c__DisplayClass298_0::<>4__this
	Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 * ___U3CU3E4__this_0;
	// System.IOSelectorJob System.Net.Sockets.Socket_<>c__DisplayClass298_0::job
	IOSelectorJob_t2B03604D19B81660C4C1C06590C76BC8630DDE99 * ___job_1;
	// System.IntPtr System.Net.Sockets.Socket_<>c__DisplayClass298_0::handle
	intptr_t ___handle_2;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass298_0_tCCC599BC4750E6215E11C91C2037B4694B3E23E7, ___U3CU3E4__this_0)); }
	inline Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_job_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass298_0_tCCC599BC4750E6215E11C91C2037B4694B3E23E7, ___job_1)); }
	inline IOSelectorJob_t2B03604D19B81660C4C1C06590C76BC8630DDE99 * get_job_1() const { return ___job_1; }
	inline IOSelectorJob_t2B03604D19B81660C4C1C06590C76BC8630DDE99 ** get_address_of_job_1() { return &___job_1; }
	inline void set_job_1(IOSelectorJob_t2B03604D19B81660C4C1C06590C76BC8630DDE99 * value)
	{
		___job_1 = value;
		Il2CppCodeGenWriteBarrier((&___job_1), value);
	}

	inline static int32_t get_offset_of_handle_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass298_0_tCCC599BC4750E6215E11C91C2037B4694B3E23E7, ___handle_2)); }
	inline intptr_t get_handle_2() const { return ___handle_2; }
	inline intptr_t* get_address_of_handle_2() { return &___handle_2; }
	inline void set_handle_2(intptr_t value)
	{
		___handle_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS298_0_TCCC599BC4750E6215E11C91C2037B4694B3E23E7_H
#ifndef WSABUF_TFC99449E36506806B55A93B6293AC7D2D10D3CEE_H
#define WSABUF_TFC99449E36506806B55A93B6293AC7D2D10D3CEE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Sockets.Socket_WSABUF
struct  WSABUF_tFC99449E36506806B55A93B6293AC7D2D10D3CEE 
{
public:
	// System.Int32 System.Net.Sockets.Socket_WSABUF::len
	int32_t ___len_0;
	// System.IntPtr System.Net.Sockets.Socket_WSABUF::buf
	intptr_t ___buf_1;

public:
	inline static int32_t get_offset_of_len_0() { return static_cast<int32_t>(offsetof(WSABUF_tFC99449E36506806B55A93B6293AC7D2D10D3CEE, ___len_0)); }
	inline int32_t get_len_0() const { return ___len_0; }
	inline int32_t* get_address_of_len_0() { return &___len_0; }
	inline void set_len_0(int32_t value)
	{
		___len_0 = value;
	}

	inline static int32_t get_offset_of_buf_1() { return static_cast<int32_t>(offsetof(WSABUF_tFC99449E36506806B55A93B6293AC7D2D10D3CEE, ___buf_1)); }
	inline intptr_t get_buf_1() const { return ___buf_1; }
	inline intptr_t* get_address_of_buf_1() { return &___buf_1; }
	inline void set_buf_1(intptr_t value)
	{
		___buf_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WSABUF_TFC99449E36506806B55A93B6293AC7D2D10D3CEE_H
#ifndef SOCKETASYNCOPERATION_T160EA51A781C803FD186EFA21B78F2A76F603FAF_H
#define SOCKETASYNCOPERATION_T160EA51A781C803FD186EFA21B78F2A76F603FAF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Sockets.SocketAsyncOperation
struct  SocketAsyncOperation_t160EA51A781C803FD186EFA21B78F2A76F603FAF 
{
public:
	// System.Int32 System.Net.Sockets.SocketAsyncOperation::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SocketAsyncOperation_t160EA51A781C803FD186EFA21B78F2A76F603FAF, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOCKETASYNCOPERATION_T160EA51A781C803FD186EFA21B78F2A76F603FAF_H
#ifndef SOCKETERROR_T0157BEC7F0A26C8FC31D392B2B7C6CFCD695D5E7_H
#define SOCKETERROR_T0157BEC7F0A26C8FC31D392B2B7C6CFCD695D5E7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Sockets.SocketError
struct  SocketError_t0157BEC7F0A26C8FC31D392B2B7C6CFCD695D5E7 
{
public:
	// System.Int32 System.Net.Sockets.SocketError::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SocketError_t0157BEC7F0A26C8FC31D392B2B7C6CFCD695D5E7, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOCKETERROR_T0157BEC7F0A26C8FC31D392B2B7C6CFCD695D5E7_H
#ifndef SOCKETFLAGS_T77581B58FF9A1A1D3E3270EDE83E4CAD3947F809_H
#define SOCKETFLAGS_T77581B58FF9A1A1D3E3270EDE83E4CAD3947F809_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Sockets.SocketFlags
struct  SocketFlags_t77581B58FF9A1A1D3E3270EDE83E4CAD3947F809 
{
public:
	// System.Int32 System.Net.Sockets.SocketFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SocketFlags_t77581B58FF9A1A1D3E3270EDE83E4CAD3947F809, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOCKETFLAGS_T77581B58FF9A1A1D3E3270EDE83E4CAD3947F809_H
#ifndef SOCKETOPERATION_T5579D7030CDF83F0E9CAC0B6484D279F34F6193C_H
#define SOCKETOPERATION_T5579D7030CDF83F0E9CAC0B6484D279F34F6193C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Sockets.SocketOperation
struct  SocketOperation_t5579D7030CDF83F0E9CAC0B6484D279F34F6193C 
{
public:
	// System.Int32 System.Net.Sockets.SocketOperation::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SocketOperation_t5579D7030CDF83F0E9CAC0B6484D279F34F6193C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOCKETOPERATION_T5579D7030CDF83F0E9CAC0B6484D279F34F6193C_H
#ifndef SOCKETOPTIONLEVEL_T75F67243F6A4311CE8731B9A344FECD8186B3B21_H
#define SOCKETOPTIONLEVEL_T75F67243F6A4311CE8731B9A344FECD8186B3B21_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Sockets.SocketOptionLevel
struct  SocketOptionLevel_t75F67243F6A4311CE8731B9A344FECD8186B3B21 
{
public:
	// System.Int32 System.Net.Sockets.SocketOptionLevel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SocketOptionLevel_t75F67243F6A4311CE8731B9A344FECD8186B3B21, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOCKETOPTIONLEVEL_T75F67243F6A4311CE8731B9A344FECD8186B3B21_H
#ifndef SOCKETOPTIONNAME_T11A763BEFF673A081DA61B8A7B1DF11909153B28_H
#define SOCKETOPTIONNAME_T11A763BEFF673A081DA61B8A7B1DF11909153B28_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Sockets.SocketOptionName
struct  SocketOptionName_t11A763BEFF673A081DA61B8A7B1DF11909153B28 
{
public:
	// System.Int32 System.Net.Sockets.SocketOptionName::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SocketOptionName_t11A763BEFF673A081DA61B8A7B1DF11909153B28, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOCKETOPTIONNAME_T11A763BEFF673A081DA61B8A7B1DF11909153B28_H
#ifndef SOCKETSHUTDOWN_TC1C26BD51DCA13F2A5314DAA97701EF9B230D957_H
#define SOCKETSHUTDOWN_TC1C26BD51DCA13F2A5314DAA97701EF9B230D957_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Sockets.SocketShutdown
struct  SocketShutdown_tC1C26BD51DCA13F2A5314DAA97701EF9B230D957 
{
public:
	// System.Int32 System.Net.Sockets.SocketShutdown::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SocketShutdown_tC1C26BD51DCA13F2A5314DAA97701EF9B230D957, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOCKETSHUTDOWN_TC1C26BD51DCA13F2A5314DAA97701EF9B230D957_H
#ifndef SOCKETTYPE_TCD56A18D4C7B43BF166E5C8B4B456BD646DF5775_H
#define SOCKETTYPE_TCD56A18D4C7B43BF166E5C8B4B456BD646DF5775_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Sockets.SocketType
struct  SocketType_tCD56A18D4C7B43BF166E5C8B4B456BD646DF5775 
{
public:
	// System.Int32 System.Net.Sockets.SocketType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SocketType_tCD56A18D4C7B43BF166E5C8B4B456BD646DF5775, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOCKETTYPE_TCD56A18D4C7B43BF166E5C8B4B456BD646DF5775_H
#ifndef TRANSPORTTYPE_TE05CF39764C8131A3248837320DF17C2129E0490_H
#define TRANSPORTTYPE_TE05CF39764C8131A3248837320DF17C2129E0490_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.TransportType
struct  TransportType_tE05CF39764C8131A3248837320DF17C2129E0490 
{
public:
	// System.Int32 System.Net.TransportType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TransportType_tE05CF39764C8131A3248837320DF17C2129E0490, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSPORTTYPE_TE05CF39764C8131A3248837320DF17C2129E0490_H
#ifndef NTLMAUTHSTATE_TEDDC6AC65C3D7223EB1A1360D852CDEA2F3A251D_H
#define NTLMAUTHSTATE_TEDDC6AC65C3D7223EB1A1360D852CDEA2F3A251D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebConnection_NtlmAuthState
struct  NtlmAuthState_tEDDC6AC65C3D7223EB1A1360D852CDEA2F3A251D 
{
public:
	// System.Int32 System.Net.WebConnection_NtlmAuthState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NtlmAuthState_tEDDC6AC65C3D7223EB1A1360D852CDEA2F3A251D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NTLMAUTHSTATE_TEDDC6AC65C3D7223EB1A1360D852CDEA2F3A251D_H
#ifndef CONNECTIONSTATE_T9EAE3917A0743B4C6D40669D7B2BBE799490A0C6_H
#define CONNECTIONSTATE_T9EAE3917A0743B4C6D40669D7B2BBE799490A0C6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebConnectionGroup_ConnectionState
struct  ConnectionState_t9EAE3917A0743B4C6D40669D7B2BBE799490A0C6  : public RuntimeObject
{
public:
	// System.Net.WebConnection System.Net.WebConnectionGroup_ConnectionState::<Connection>k__BackingField
	WebConnection_tEB76AEE17361D28CBAD4033026A71DA89289C243 * ___U3CConnectionU3Ek__BackingField_0;
	// System.Net.WebConnectionGroup System.Net.WebConnectionGroup_ConnectionState::<Group>k__BackingField
	WebConnectionGroup_tBEAB5ED1DE321C0981F5CBABA020970C3D63E95F * ___U3CGroupU3Ek__BackingField_1;
	// System.Boolean System.Net.WebConnectionGroup_ConnectionState::busy
	bool ___busy_2;
	// System.DateTime System.Net.WebConnectionGroup_ConnectionState::idleSince
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___idleSince_3;

public:
	inline static int32_t get_offset_of_U3CConnectionU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ConnectionState_t9EAE3917A0743B4C6D40669D7B2BBE799490A0C6, ___U3CConnectionU3Ek__BackingField_0)); }
	inline WebConnection_tEB76AEE17361D28CBAD4033026A71DA89289C243 * get_U3CConnectionU3Ek__BackingField_0() const { return ___U3CConnectionU3Ek__BackingField_0; }
	inline WebConnection_tEB76AEE17361D28CBAD4033026A71DA89289C243 ** get_address_of_U3CConnectionU3Ek__BackingField_0() { return &___U3CConnectionU3Ek__BackingField_0; }
	inline void set_U3CConnectionU3Ek__BackingField_0(WebConnection_tEB76AEE17361D28CBAD4033026A71DA89289C243 * value)
	{
		___U3CConnectionU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CConnectionU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CGroupU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ConnectionState_t9EAE3917A0743B4C6D40669D7B2BBE799490A0C6, ___U3CGroupU3Ek__BackingField_1)); }
	inline WebConnectionGroup_tBEAB5ED1DE321C0981F5CBABA020970C3D63E95F * get_U3CGroupU3Ek__BackingField_1() const { return ___U3CGroupU3Ek__BackingField_1; }
	inline WebConnectionGroup_tBEAB5ED1DE321C0981F5CBABA020970C3D63E95F ** get_address_of_U3CGroupU3Ek__BackingField_1() { return &___U3CGroupU3Ek__BackingField_1; }
	inline void set_U3CGroupU3Ek__BackingField_1(WebConnectionGroup_tBEAB5ED1DE321C0981F5CBABA020970C3D63E95F * value)
	{
		___U3CGroupU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGroupU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_busy_2() { return static_cast<int32_t>(offsetof(ConnectionState_t9EAE3917A0743B4C6D40669D7B2BBE799490A0C6, ___busy_2)); }
	inline bool get_busy_2() const { return ___busy_2; }
	inline bool* get_address_of_busy_2() { return &___busy_2; }
	inline void set_busy_2(bool value)
	{
		___busy_2 = value;
	}

	inline static int32_t get_offset_of_idleSince_3() { return static_cast<int32_t>(offsetof(ConnectionState_t9EAE3917A0743B4C6D40669D7B2BBE799490A0C6, ___idleSince_3)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_idleSince_3() const { return ___idleSince_3; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_idleSince_3() { return &___idleSince_3; }
	inline void set_idleSince_3(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___idleSince_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONNECTIONSTATE_T9EAE3917A0743B4C6D40669D7B2BBE799490A0C6_H
#ifndef WEBCONNECTIONSTREAM_T537F33BF6D8999D67791D02F8E6DE6448F2A31FC_H
#define WEBCONNECTIONSTREAM_T537F33BF6D8999D67791D02F8E6DE6448F2A31FC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebConnectionStream
struct  WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// System.Boolean System.Net.WebConnectionStream::isRead
	bool ___isRead_6;
	// System.Net.WebConnection System.Net.WebConnectionStream::cnc
	WebConnection_tEB76AEE17361D28CBAD4033026A71DA89289C243 * ___cnc_7;
	// System.Net.HttpWebRequest System.Net.WebConnectionStream::request
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0 * ___request_8;
	// System.Byte[] System.Net.WebConnectionStream::readBuffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___readBuffer_9;
	// System.Int32 System.Net.WebConnectionStream::readBufferOffset
	int32_t ___readBufferOffset_10;
	// System.Int32 System.Net.WebConnectionStream::readBufferSize
	int32_t ___readBufferSize_11;
	// System.Int32 System.Net.WebConnectionStream::stream_length
	int32_t ___stream_length_12;
	// System.Int64 System.Net.WebConnectionStream::contentLength
	int64_t ___contentLength_13;
	// System.Int64 System.Net.WebConnectionStream::totalRead
	int64_t ___totalRead_14;
	// System.Int64 System.Net.WebConnectionStream::totalWritten
	int64_t ___totalWritten_15;
	// System.Boolean System.Net.WebConnectionStream::nextReadCalled
	bool ___nextReadCalled_16;
	// System.Int32 System.Net.WebConnectionStream::pendingReads
	int32_t ___pendingReads_17;
	// System.Int32 System.Net.WebConnectionStream::pendingWrites
	int32_t ___pendingWrites_18;
	// System.Threading.ManualResetEvent System.Net.WebConnectionStream::pending
	ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * ___pending_19;
	// System.Boolean System.Net.WebConnectionStream::allowBuffering
	bool ___allowBuffering_20;
	// System.Boolean System.Net.WebConnectionStream::sendChunked
	bool ___sendChunked_21;
	// System.IO.MemoryStream System.Net.WebConnectionStream::writeBuffer
	MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * ___writeBuffer_22;
	// System.Boolean System.Net.WebConnectionStream::requestWritten
	bool ___requestWritten_23;
	// System.Byte[] System.Net.WebConnectionStream::headers
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___headers_24;
	// System.Boolean System.Net.WebConnectionStream::disposed
	bool ___disposed_25;
	// System.Boolean System.Net.WebConnectionStream::headersSent
	bool ___headersSent_26;
	// System.Object System.Net.WebConnectionStream::locker
	RuntimeObject * ___locker_27;
	// System.Boolean System.Net.WebConnectionStream::initRead
	bool ___initRead_28;
	// System.Boolean System.Net.WebConnectionStream::read_eof
	bool ___read_eof_29;
	// System.Boolean System.Net.WebConnectionStream::complete_request_written
	bool ___complete_request_written_30;
	// System.Int32 System.Net.WebConnectionStream::read_timeout
	int32_t ___read_timeout_31;
	// System.Int32 System.Net.WebConnectionStream::write_timeout
	int32_t ___write_timeout_32;
	// System.AsyncCallback System.Net.WebConnectionStream::cb_wrapper
	AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___cb_wrapper_33;
	// System.Boolean System.Net.WebConnectionStream::IgnoreIOErrors
	bool ___IgnoreIOErrors_34;
	// System.Boolean System.Net.WebConnectionStream::<GetResponseOnClose>k__BackingField
	bool ___U3CGetResponseOnCloseU3Ek__BackingField_35;

public:
	inline static int32_t get_offset_of_isRead_6() { return static_cast<int32_t>(offsetof(WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC, ___isRead_6)); }
	inline bool get_isRead_6() const { return ___isRead_6; }
	inline bool* get_address_of_isRead_6() { return &___isRead_6; }
	inline void set_isRead_6(bool value)
	{
		___isRead_6 = value;
	}

	inline static int32_t get_offset_of_cnc_7() { return static_cast<int32_t>(offsetof(WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC, ___cnc_7)); }
	inline WebConnection_tEB76AEE17361D28CBAD4033026A71DA89289C243 * get_cnc_7() const { return ___cnc_7; }
	inline WebConnection_tEB76AEE17361D28CBAD4033026A71DA89289C243 ** get_address_of_cnc_7() { return &___cnc_7; }
	inline void set_cnc_7(WebConnection_tEB76AEE17361D28CBAD4033026A71DA89289C243 * value)
	{
		___cnc_7 = value;
		Il2CppCodeGenWriteBarrier((&___cnc_7), value);
	}

	inline static int32_t get_offset_of_request_8() { return static_cast<int32_t>(offsetof(WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC, ___request_8)); }
	inline HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0 * get_request_8() const { return ___request_8; }
	inline HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0 ** get_address_of_request_8() { return &___request_8; }
	inline void set_request_8(HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0 * value)
	{
		___request_8 = value;
		Il2CppCodeGenWriteBarrier((&___request_8), value);
	}

	inline static int32_t get_offset_of_readBuffer_9() { return static_cast<int32_t>(offsetof(WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC, ___readBuffer_9)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_readBuffer_9() const { return ___readBuffer_9; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_readBuffer_9() { return &___readBuffer_9; }
	inline void set_readBuffer_9(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___readBuffer_9 = value;
		Il2CppCodeGenWriteBarrier((&___readBuffer_9), value);
	}

	inline static int32_t get_offset_of_readBufferOffset_10() { return static_cast<int32_t>(offsetof(WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC, ___readBufferOffset_10)); }
	inline int32_t get_readBufferOffset_10() const { return ___readBufferOffset_10; }
	inline int32_t* get_address_of_readBufferOffset_10() { return &___readBufferOffset_10; }
	inline void set_readBufferOffset_10(int32_t value)
	{
		___readBufferOffset_10 = value;
	}

	inline static int32_t get_offset_of_readBufferSize_11() { return static_cast<int32_t>(offsetof(WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC, ___readBufferSize_11)); }
	inline int32_t get_readBufferSize_11() const { return ___readBufferSize_11; }
	inline int32_t* get_address_of_readBufferSize_11() { return &___readBufferSize_11; }
	inline void set_readBufferSize_11(int32_t value)
	{
		___readBufferSize_11 = value;
	}

	inline static int32_t get_offset_of_stream_length_12() { return static_cast<int32_t>(offsetof(WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC, ___stream_length_12)); }
	inline int32_t get_stream_length_12() const { return ___stream_length_12; }
	inline int32_t* get_address_of_stream_length_12() { return &___stream_length_12; }
	inline void set_stream_length_12(int32_t value)
	{
		___stream_length_12 = value;
	}

	inline static int32_t get_offset_of_contentLength_13() { return static_cast<int32_t>(offsetof(WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC, ___contentLength_13)); }
	inline int64_t get_contentLength_13() const { return ___contentLength_13; }
	inline int64_t* get_address_of_contentLength_13() { return &___contentLength_13; }
	inline void set_contentLength_13(int64_t value)
	{
		___contentLength_13 = value;
	}

	inline static int32_t get_offset_of_totalRead_14() { return static_cast<int32_t>(offsetof(WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC, ___totalRead_14)); }
	inline int64_t get_totalRead_14() const { return ___totalRead_14; }
	inline int64_t* get_address_of_totalRead_14() { return &___totalRead_14; }
	inline void set_totalRead_14(int64_t value)
	{
		___totalRead_14 = value;
	}

	inline static int32_t get_offset_of_totalWritten_15() { return static_cast<int32_t>(offsetof(WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC, ___totalWritten_15)); }
	inline int64_t get_totalWritten_15() const { return ___totalWritten_15; }
	inline int64_t* get_address_of_totalWritten_15() { return &___totalWritten_15; }
	inline void set_totalWritten_15(int64_t value)
	{
		___totalWritten_15 = value;
	}

	inline static int32_t get_offset_of_nextReadCalled_16() { return static_cast<int32_t>(offsetof(WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC, ___nextReadCalled_16)); }
	inline bool get_nextReadCalled_16() const { return ___nextReadCalled_16; }
	inline bool* get_address_of_nextReadCalled_16() { return &___nextReadCalled_16; }
	inline void set_nextReadCalled_16(bool value)
	{
		___nextReadCalled_16 = value;
	}

	inline static int32_t get_offset_of_pendingReads_17() { return static_cast<int32_t>(offsetof(WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC, ___pendingReads_17)); }
	inline int32_t get_pendingReads_17() const { return ___pendingReads_17; }
	inline int32_t* get_address_of_pendingReads_17() { return &___pendingReads_17; }
	inline void set_pendingReads_17(int32_t value)
	{
		___pendingReads_17 = value;
	}

	inline static int32_t get_offset_of_pendingWrites_18() { return static_cast<int32_t>(offsetof(WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC, ___pendingWrites_18)); }
	inline int32_t get_pendingWrites_18() const { return ___pendingWrites_18; }
	inline int32_t* get_address_of_pendingWrites_18() { return &___pendingWrites_18; }
	inline void set_pendingWrites_18(int32_t value)
	{
		___pendingWrites_18 = value;
	}

	inline static int32_t get_offset_of_pending_19() { return static_cast<int32_t>(offsetof(WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC, ___pending_19)); }
	inline ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * get_pending_19() const { return ___pending_19; }
	inline ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 ** get_address_of_pending_19() { return &___pending_19; }
	inline void set_pending_19(ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * value)
	{
		___pending_19 = value;
		Il2CppCodeGenWriteBarrier((&___pending_19), value);
	}

	inline static int32_t get_offset_of_allowBuffering_20() { return static_cast<int32_t>(offsetof(WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC, ___allowBuffering_20)); }
	inline bool get_allowBuffering_20() const { return ___allowBuffering_20; }
	inline bool* get_address_of_allowBuffering_20() { return &___allowBuffering_20; }
	inline void set_allowBuffering_20(bool value)
	{
		___allowBuffering_20 = value;
	}

	inline static int32_t get_offset_of_sendChunked_21() { return static_cast<int32_t>(offsetof(WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC, ___sendChunked_21)); }
	inline bool get_sendChunked_21() const { return ___sendChunked_21; }
	inline bool* get_address_of_sendChunked_21() { return &___sendChunked_21; }
	inline void set_sendChunked_21(bool value)
	{
		___sendChunked_21 = value;
	}

	inline static int32_t get_offset_of_writeBuffer_22() { return static_cast<int32_t>(offsetof(WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC, ___writeBuffer_22)); }
	inline MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * get_writeBuffer_22() const { return ___writeBuffer_22; }
	inline MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C ** get_address_of_writeBuffer_22() { return &___writeBuffer_22; }
	inline void set_writeBuffer_22(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * value)
	{
		___writeBuffer_22 = value;
		Il2CppCodeGenWriteBarrier((&___writeBuffer_22), value);
	}

	inline static int32_t get_offset_of_requestWritten_23() { return static_cast<int32_t>(offsetof(WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC, ___requestWritten_23)); }
	inline bool get_requestWritten_23() const { return ___requestWritten_23; }
	inline bool* get_address_of_requestWritten_23() { return &___requestWritten_23; }
	inline void set_requestWritten_23(bool value)
	{
		___requestWritten_23 = value;
	}

	inline static int32_t get_offset_of_headers_24() { return static_cast<int32_t>(offsetof(WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC, ___headers_24)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_headers_24() const { return ___headers_24; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_headers_24() { return &___headers_24; }
	inline void set_headers_24(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___headers_24 = value;
		Il2CppCodeGenWriteBarrier((&___headers_24), value);
	}

	inline static int32_t get_offset_of_disposed_25() { return static_cast<int32_t>(offsetof(WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC, ___disposed_25)); }
	inline bool get_disposed_25() const { return ___disposed_25; }
	inline bool* get_address_of_disposed_25() { return &___disposed_25; }
	inline void set_disposed_25(bool value)
	{
		___disposed_25 = value;
	}

	inline static int32_t get_offset_of_headersSent_26() { return static_cast<int32_t>(offsetof(WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC, ___headersSent_26)); }
	inline bool get_headersSent_26() const { return ___headersSent_26; }
	inline bool* get_address_of_headersSent_26() { return &___headersSent_26; }
	inline void set_headersSent_26(bool value)
	{
		___headersSent_26 = value;
	}

	inline static int32_t get_offset_of_locker_27() { return static_cast<int32_t>(offsetof(WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC, ___locker_27)); }
	inline RuntimeObject * get_locker_27() const { return ___locker_27; }
	inline RuntimeObject ** get_address_of_locker_27() { return &___locker_27; }
	inline void set_locker_27(RuntimeObject * value)
	{
		___locker_27 = value;
		Il2CppCodeGenWriteBarrier((&___locker_27), value);
	}

	inline static int32_t get_offset_of_initRead_28() { return static_cast<int32_t>(offsetof(WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC, ___initRead_28)); }
	inline bool get_initRead_28() const { return ___initRead_28; }
	inline bool* get_address_of_initRead_28() { return &___initRead_28; }
	inline void set_initRead_28(bool value)
	{
		___initRead_28 = value;
	}

	inline static int32_t get_offset_of_read_eof_29() { return static_cast<int32_t>(offsetof(WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC, ___read_eof_29)); }
	inline bool get_read_eof_29() const { return ___read_eof_29; }
	inline bool* get_address_of_read_eof_29() { return &___read_eof_29; }
	inline void set_read_eof_29(bool value)
	{
		___read_eof_29 = value;
	}

	inline static int32_t get_offset_of_complete_request_written_30() { return static_cast<int32_t>(offsetof(WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC, ___complete_request_written_30)); }
	inline bool get_complete_request_written_30() const { return ___complete_request_written_30; }
	inline bool* get_address_of_complete_request_written_30() { return &___complete_request_written_30; }
	inline void set_complete_request_written_30(bool value)
	{
		___complete_request_written_30 = value;
	}

	inline static int32_t get_offset_of_read_timeout_31() { return static_cast<int32_t>(offsetof(WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC, ___read_timeout_31)); }
	inline int32_t get_read_timeout_31() const { return ___read_timeout_31; }
	inline int32_t* get_address_of_read_timeout_31() { return &___read_timeout_31; }
	inline void set_read_timeout_31(int32_t value)
	{
		___read_timeout_31 = value;
	}

	inline static int32_t get_offset_of_write_timeout_32() { return static_cast<int32_t>(offsetof(WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC, ___write_timeout_32)); }
	inline int32_t get_write_timeout_32() const { return ___write_timeout_32; }
	inline int32_t* get_address_of_write_timeout_32() { return &___write_timeout_32; }
	inline void set_write_timeout_32(int32_t value)
	{
		___write_timeout_32 = value;
	}

	inline static int32_t get_offset_of_cb_wrapper_33() { return static_cast<int32_t>(offsetof(WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC, ___cb_wrapper_33)); }
	inline AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * get_cb_wrapper_33() const { return ___cb_wrapper_33; }
	inline AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 ** get_address_of_cb_wrapper_33() { return &___cb_wrapper_33; }
	inline void set_cb_wrapper_33(AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * value)
	{
		___cb_wrapper_33 = value;
		Il2CppCodeGenWriteBarrier((&___cb_wrapper_33), value);
	}

	inline static int32_t get_offset_of_IgnoreIOErrors_34() { return static_cast<int32_t>(offsetof(WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC, ___IgnoreIOErrors_34)); }
	inline bool get_IgnoreIOErrors_34() const { return ___IgnoreIOErrors_34; }
	inline bool* get_address_of_IgnoreIOErrors_34() { return &___IgnoreIOErrors_34; }
	inline void set_IgnoreIOErrors_34(bool value)
	{
		___IgnoreIOErrors_34 = value;
	}

	inline static int32_t get_offset_of_U3CGetResponseOnCloseU3Ek__BackingField_35() { return static_cast<int32_t>(offsetof(WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC, ___U3CGetResponseOnCloseU3Ek__BackingField_35)); }
	inline bool get_U3CGetResponseOnCloseU3Ek__BackingField_35() const { return ___U3CGetResponseOnCloseU3Ek__BackingField_35; }
	inline bool* get_address_of_U3CGetResponseOnCloseU3Ek__BackingField_35() { return &___U3CGetResponseOnCloseU3Ek__BackingField_35; }
	inline void set_U3CGetResponseOnCloseU3Ek__BackingField_35(bool value)
	{
		___U3CGetResponseOnCloseU3Ek__BackingField_35 = value;
	}
};

struct WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC_StaticFields
{
public:
	// System.Byte[] System.Net.WebConnectionStream::crlf
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___crlf_5;

public:
	inline static int32_t get_offset_of_crlf_5() { return static_cast<int32_t>(offsetof(WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC_StaticFields, ___crlf_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_crlf_5() const { return ___crlf_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_crlf_5() { return &___crlf_5; }
	inline void set_crlf_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___crlf_5 = value;
		Il2CppCodeGenWriteBarrier((&___crlf_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBCONNECTIONSTREAM_T537F33BF6D8999D67791D02F8E6DE6448F2A31FC_H
#ifndef WEBEXCEPTIONSTATUS_T97365CBADE462C1E2A1A0FACF18F3B111900F8DC_H
#define WEBEXCEPTIONSTATUS_T97365CBADE462C1E2A1A0FACF18F3B111900F8DC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebExceptionStatus
struct  WebExceptionStatus_t97365CBADE462C1E2A1A0FACF18F3B111900F8DC 
{
public:
	// System.Int32 System.Net.WebExceptionStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WebExceptionStatus_t97365CBADE462C1E2A1A0FACF18F3B111900F8DC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBEXCEPTIONSTATUS_T97365CBADE462C1E2A1A0FACF18F3B111900F8DC_H
#ifndef EXTERNALEXCEPTION_T68841FD169C0CB00CC950EDA7E2A59540D65B1CE_H
#define EXTERNALEXCEPTION_T68841FD169C0CB00CC950EDA7E2A59540D65B1CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.ExternalException
struct  ExternalException_t68841FD169C0CB00CC950EDA7E2A59540D65B1CE  : public SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTERNALEXCEPTION_T68841FD169C0CB00CC950EDA7E2A59540D65B1CE_H
#ifndef SAFEHANDLE_T1E326D75E23FD5BB6D40BA322298FDC6526CC383_H
#define SAFEHANDLE_T1E326D75E23FD5BB6D40BA322298FDC6526CC383_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.SafeHandle
struct  SafeHandle_t1E326D75E23FD5BB6D40BA322298FDC6526CC383  : public CriticalFinalizerObject_t8B006E1DEE084E781F5C0F3283E9226E28894DD9
{
public:
	// System.IntPtr System.Runtime.InteropServices.SafeHandle::handle
	intptr_t ___handle_0;
	// System.Int32 System.Runtime.InteropServices.SafeHandle::_state
	int32_t ____state_1;
	// System.Boolean System.Runtime.InteropServices.SafeHandle::_ownsHandle
	bool ____ownsHandle_2;
	// System.Boolean System.Runtime.InteropServices.SafeHandle::_fullyInitialized
	bool ____fullyInitialized_3;

public:
	inline static int32_t get_offset_of_handle_0() { return static_cast<int32_t>(offsetof(SafeHandle_t1E326D75E23FD5BB6D40BA322298FDC6526CC383, ___handle_0)); }
	inline intptr_t get_handle_0() const { return ___handle_0; }
	inline intptr_t* get_address_of_handle_0() { return &___handle_0; }
	inline void set_handle_0(intptr_t value)
	{
		___handle_0 = value;
	}

	inline static int32_t get_offset_of__state_1() { return static_cast<int32_t>(offsetof(SafeHandle_t1E326D75E23FD5BB6D40BA322298FDC6526CC383, ____state_1)); }
	inline int32_t get__state_1() const { return ____state_1; }
	inline int32_t* get_address_of__state_1() { return &____state_1; }
	inline void set__state_1(int32_t value)
	{
		____state_1 = value;
	}

	inline static int32_t get_offset_of__ownsHandle_2() { return static_cast<int32_t>(offsetof(SafeHandle_t1E326D75E23FD5BB6D40BA322298FDC6526CC383, ____ownsHandle_2)); }
	inline bool get__ownsHandle_2() const { return ____ownsHandle_2; }
	inline bool* get_address_of__ownsHandle_2() { return &____ownsHandle_2; }
	inline void set__ownsHandle_2(bool value)
	{
		____ownsHandle_2 = value;
	}

	inline static int32_t get_offset_of__fullyInitialized_3() { return static_cast<int32_t>(offsetof(SafeHandle_t1E326D75E23FD5BB6D40BA322298FDC6526CC383, ____fullyInitialized_3)); }
	inline bool get__fullyInitialized_3() const { return ____fullyInitialized_3; }
	inline bool* get_address_of__fullyInitialized_3() { return &____fullyInitialized_3; }
	inline void set__fullyInitialized_3(bool value)
	{
		____fullyInitialized_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAFEHANDLE_T1E326D75E23FD5BB6D40BA322298FDC6526CC383_H
#ifndef SECURITYACTION_T88130AC24A6765CFA3B9DF2906E8E8F9360EB029_H
#define SECURITYACTION_T88130AC24A6765CFA3B9DF2906E8E8F9360EB029_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Permissions.SecurityAction
struct  SecurityAction_t88130AC24A6765CFA3B9DF2906E8E8F9360EB029 
{
public:
	// System.Int32 System.Security.Permissions.SecurityAction::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SecurityAction_t88130AC24A6765CFA3B9DF2906E8E8F9360EB029, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECURITYACTION_T88130AC24A6765CFA3B9DF2906E8E8F9360EB029_H
#ifndef TOKENIMPERSONATIONLEVEL_TED478ED25688E978F79556E1A2335F7262023D26_H
#define TOKENIMPERSONATIONLEVEL_TED478ED25688E978F79556E1A2335F7262023D26_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Principal.TokenImpersonationLevel
struct  TokenImpersonationLevel_tED478ED25688E978F79556E1A2335F7262023D26 
{
public:
	// System.Int32 System.Security.Principal.TokenImpersonationLevel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TokenImpersonationLevel_tED478ED25688E978F79556E1A2335F7262023D26, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOKENIMPERSONATIONLEVEL_TED478ED25688E978F79556E1A2335F7262023D26_H
#ifndef SAFEHANDLEZEROORMINUSONEISINVALID_T779A965C82098677DF1ED10A134DBCDEC8AACB8E_H
#define SAFEHANDLEZEROORMINUSONEISINVALID_T779A965C82098677DF1ED10A134DBCDEC8AACB8E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.Win32.SafeHandles.SafeHandleZeroOrMinusOneIsInvalid
struct  SafeHandleZeroOrMinusOneIsInvalid_t779A965C82098677DF1ED10A134DBCDEC8AACB8E  : public SafeHandle_t1E326D75E23FD5BB6D40BA322298FDC6526CC383
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAFEHANDLEZEROORMINUSONEISINVALID_T779A965C82098677DF1ED10A134DBCDEC8AACB8E_H
#ifndef WIN32EXCEPTION_TB05BE97AB4CADD54DF96C0109689F0ECA7517668_H
#define WIN32EXCEPTION_TB05BE97AB4CADD54DF96C0109689F0ECA7517668_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.Win32Exception
struct  Win32Exception_tB05BE97AB4CADD54DF96C0109689F0ECA7517668  : public ExternalException_t68841FD169C0CB00CC950EDA7E2A59540D65B1CE
{
public:
	// System.Int32 System.ComponentModel.Win32Exception::nativeErrorCode
	int32_t ___nativeErrorCode_17;

public:
	inline static int32_t get_offset_of_nativeErrorCode_17() { return static_cast<int32_t>(offsetof(Win32Exception_tB05BE97AB4CADD54DF96C0109689F0ECA7517668, ___nativeErrorCode_17)); }
	inline int32_t get_nativeErrorCode_17() const { return ___nativeErrorCode_17; }
	inline int32_t* get_address_of_nativeErrorCode_17() { return &___nativeErrorCode_17; }
	inline void set_nativeErrorCode_17(int32_t value)
	{
		___nativeErrorCode_17 = value;
	}
};

struct Win32Exception_tB05BE97AB4CADD54DF96C0109689F0ECA7517668_StaticFields
{
public:
	// System.Boolean System.ComponentModel.Win32Exception::s_ErrorMessagesInitialized
	bool ___s_ErrorMessagesInitialized_18;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.String> System.ComponentModel.Win32Exception::s_ErrorMessage
	Dictionary_2_t4EFE6A1D6502662B911688316C6920444A18CF0C * ___s_ErrorMessage_19;

public:
	inline static int32_t get_offset_of_s_ErrorMessagesInitialized_18() { return static_cast<int32_t>(offsetof(Win32Exception_tB05BE97AB4CADD54DF96C0109689F0ECA7517668_StaticFields, ___s_ErrorMessagesInitialized_18)); }
	inline bool get_s_ErrorMessagesInitialized_18() const { return ___s_ErrorMessagesInitialized_18; }
	inline bool* get_address_of_s_ErrorMessagesInitialized_18() { return &___s_ErrorMessagesInitialized_18; }
	inline void set_s_ErrorMessagesInitialized_18(bool value)
	{
		___s_ErrorMessagesInitialized_18 = value;
	}

	inline static int32_t get_offset_of_s_ErrorMessage_19() { return static_cast<int32_t>(offsetof(Win32Exception_tB05BE97AB4CADD54DF96C0109689F0ECA7517668_StaticFields, ___s_ErrorMessage_19)); }
	inline Dictionary_2_t4EFE6A1D6502662B911688316C6920444A18CF0C * get_s_ErrorMessage_19() const { return ___s_ErrorMessage_19; }
	inline Dictionary_2_t4EFE6A1D6502662B911688316C6920444A18CF0C ** get_address_of_s_ErrorMessage_19() { return &___s_ErrorMessage_19; }
	inline void set_s_ErrorMessage_19(Dictionary_2_t4EFE6A1D6502662B911688316C6920444A18CF0C * value)
	{
		___s_ErrorMessage_19 = value;
		Il2CppCodeGenWriteBarrier((&___s_ErrorMessage_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WIN32EXCEPTION_TB05BE97AB4CADD54DF96C0109689F0ECA7517668_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef ENDPOINTPERMISSION_TA34E0AFD9B907F84A1B9ECF8E247696D1EEFC0BA_H
#define ENDPOINTPERMISSION_TA34E0AFD9B907F84A1B9ECF8E247696D1EEFC0BA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.EndpointPermission
struct  EndpointPermission_tA34E0AFD9B907F84A1B9ECF8E247696D1EEFC0BA  : public RuntimeObject
{
public:
	// System.String System.Net.EndpointPermission::hostname
	String_t* ___hostname_1;
	// System.Int32 System.Net.EndpointPermission::port
	int32_t ___port_2;
	// System.Net.TransportType System.Net.EndpointPermission::transport
	int32_t ___transport_3;
	// System.Boolean System.Net.EndpointPermission::resolved
	bool ___resolved_4;
	// System.Boolean System.Net.EndpointPermission::hasWildcard
	bool ___hasWildcard_5;
	// System.Net.IPAddress[] System.Net.EndpointPermission::addresses
	IPAddressU5BU5D_t7F25C4C038C43BFDA8EA84969112E82BADC38BC3* ___addresses_6;

public:
	inline static int32_t get_offset_of_hostname_1() { return static_cast<int32_t>(offsetof(EndpointPermission_tA34E0AFD9B907F84A1B9ECF8E247696D1EEFC0BA, ___hostname_1)); }
	inline String_t* get_hostname_1() const { return ___hostname_1; }
	inline String_t** get_address_of_hostname_1() { return &___hostname_1; }
	inline void set_hostname_1(String_t* value)
	{
		___hostname_1 = value;
		Il2CppCodeGenWriteBarrier((&___hostname_1), value);
	}

	inline static int32_t get_offset_of_port_2() { return static_cast<int32_t>(offsetof(EndpointPermission_tA34E0AFD9B907F84A1B9ECF8E247696D1EEFC0BA, ___port_2)); }
	inline int32_t get_port_2() const { return ___port_2; }
	inline int32_t* get_address_of_port_2() { return &___port_2; }
	inline void set_port_2(int32_t value)
	{
		___port_2 = value;
	}

	inline static int32_t get_offset_of_transport_3() { return static_cast<int32_t>(offsetof(EndpointPermission_tA34E0AFD9B907F84A1B9ECF8E247696D1EEFC0BA, ___transport_3)); }
	inline int32_t get_transport_3() const { return ___transport_3; }
	inline int32_t* get_address_of_transport_3() { return &___transport_3; }
	inline void set_transport_3(int32_t value)
	{
		___transport_3 = value;
	}

	inline static int32_t get_offset_of_resolved_4() { return static_cast<int32_t>(offsetof(EndpointPermission_tA34E0AFD9B907F84A1B9ECF8E247696D1EEFC0BA, ___resolved_4)); }
	inline bool get_resolved_4() const { return ___resolved_4; }
	inline bool* get_address_of_resolved_4() { return &___resolved_4; }
	inline void set_resolved_4(bool value)
	{
		___resolved_4 = value;
	}

	inline static int32_t get_offset_of_hasWildcard_5() { return static_cast<int32_t>(offsetof(EndpointPermission_tA34E0AFD9B907F84A1B9ECF8E247696D1EEFC0BA, ___hasWildcard_5)); }
	inline bool get_hasWildcard_5() const { return ___hasWildcard_5; }
	inline bool* get_address_of_hasWildcard_5() { return &___hasWildcard_5; }
	inline void set_hasWildcard_5(bool value)
	{
		___hasWildcard_5 = value;
	}

	inline static int32_t get_offset_of_addresses_6() { return static_cast<int32_t>(offsetof(EndpointPermission_tA34E0AFD9B907F84A1B9ECF8E247696D1EEFC0BA, ___addresses_6)); }
	inline IPAddressU5BU5D_t7F25C4C038C43BFDA8EA84969112E82BADC38BC3* get_addresses_6() const { return ___addresses_6; }
	inline IPAddressU5BU5D_t7F25C4C038C43BFDA8EA84969112E82BADC38BC3** get_address_of_addresses_6() { return &___addresses_6; }
	inline void set_addresses_6(IPAddressU5BU5D_t7F25C4C038C43BFDA8EA84969112E82BADC38BC3* value)
	{
		___addresses_6 = value;
		Il2CppCodeGenWriteBarrier((&___addresses_6), value);
	}
};

struct EndpointPermission_tA34E0AFD9B907F84A1B9ECF8E247696D1EEFC0BA_StaticFields
{
public:
	// System.Char[] System.Net.EndpointPermission::dot_char
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___dot_char_0;

public:
	inline static int32_t get_offset_of_dot_char_0() { return static_cast<int32_t>(offsetof(EndpointPermission_tA34E0AFD9B907F84A1B9ECF8E247696D1EEFC0BA_StaticFields, ___dot_char_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_dot_char_0() const { return ___dot_char_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_dot_char_0() { return &___dot_char_0; }
	inline void set_dot_char_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___dot_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___dot_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENDPOINTPERMISSION_TA34E0AFD9B907F84A1B9ECF8E247696D1EEFC0BA_H
#ifndef FTPSTATUS_TC736CA78D396A33659145A9183F15038E66B2876_H
#define FTPSTATUS_TC736CA78D396A33659145A9183F15038E66B2876_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.FtpStatus
struct  FtpStatus_tC736CA78D396A33659145A9183F15038E66B2876  : public RuntimeObject
{
public:
	// System.Net.FtpStatusCode System.Net.FtpStatus::statusCode
	int32_t ___statusCode_0;
	// System.String System.Net.FtpStatus::statusDescription
	String_t* ___statusDescription_1;

public:
	inline static int32_t get_offset_of_statusCode_0() { return static_cast<int32_t>(offsetof(FtpStatus_tC736CA78D396A33659145A9183F15038E66B2876, ___statusCode_0)); }
	inline int32_t get_statusCode_0() const { return ___statusCode_0; }
	inline int32_t* get_address_of_statusCode_0() { return &___statusCode_0; }
	inline void set_statusCode_0(int32_t value)
	{
		___statusCode_0 = value;
	}

	inline static int32_t get_offset_of_statusDescription_1() { return static_cast<int32_t>(offsetof(FtpStatus_tC736CA78D396A33659145A9183F15038E66B2876, ___statusDescription_1)); }
	inline String_t* get_statusDescription_1() const { return ___statusDescription_1; }
	inline String_t** get_address_of_statusDescription_1() { return &___statusDescription_1; }
	inline void set_statusDescription_1(String_t* value)
	{
		___statusDescription_1 = value;
		Il2CppCodeGenWriteBarrier((&___statusDescription_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FTPSTATUS_TC736CA78D396A33659145A9183F15038E66B2876_H
#ifndef FTPWEBRESPONSE_T8775110950F0637C1A8A495892122865F05FC205_H
#define FTPWEBRESPONSE_T8775110950F0637C1A8A495892122865F05FC205_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.FtpWebResponse
struct  FtpWebResponse_t8775110950F0637C1A8A495892122865F05FC205  : public WebResponse_t5C91B5B83E2FBA2EABC6FDF2A70E9AFD9BB059BD
{
public:
	// System.IO.Stream System.Net.FtpWebResponse::stream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___stream_3;
	// System.Uri System.Net.FtpWebResponse::uri
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___uri_4;
	// System.Net.FtpStatusCode System.Net.FtpWebResponse::statusCode
	int32_t ___statusCode_5;
	// System.DateTime System.Net.FtpWebResponse::lastModified
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___lastModified_6;
	// System.String System.Net.FtpWebResponse::bannerMessage
	String_t* ___bannerMessage_7;
	// System.String System.Net.FtpWebResponse::welcomeMessage
	String_t* ___welcomeMessage_8;
	// System.String System.Net.FtpWebResponse::exitMessage
	String_t* ___exitMessage_9;
	// System.String System.Net.FtpWebResponse::statusDescription
	String_t* ___statusDescription_10;
	// System.String System.Net.FtpWebResponse::method
	String_t* ___method_11;
	// System.Boolean System.Net.FtpWebResponse::disposed
	bool ___disposed_12;
	// System.Net.FtpWebRequest System.Net.FtpWebResponse::request
	FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA * ___request_13;
	// System.Int64 System.Net.FtpWebResponse::contentLength
	int64_t ___contentLength_14;

public:
	inline static int32_t get_offset_of_stream_3() { return static_cast<int32_t>(offsetof(FtpWebResponse_t8775110950F0637C1A8A495892122865F05FC205, ___stream_3)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_stream_3() const { return ___stream_3; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_stream_3() { return &___stream_3; }
	inline void set_stream_3(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___stream_3 = value;
		Il2CppCodeGenWriteBarrier((&___stream_3), value);
	}

	inline static int32_t get_offset_of_uri_4() { return static_cast<int32_t>(offsetof(FtpWebResponse_t8775110950F0637C1A8A495892122865F05FC205, ___uri_4)); }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * get_uri_4() const { return ___uri_4; }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E ** get_address_of_uri_4() { return &___uri_4; }
	inline void set_uri_4(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * value)
	{
		___uri_4 = value;
		Il2CppCodeGenWriteBarrier((&___uri_4), value);
	}

	inline static int32_t get_offset_of_statusCode_5() { return static_cast<int32_t>(offsetof(FtpWebResponse_t8775110950F0637C1A8A495892122865F05FC205, ___statusCode_5)); }
	inline int32_t get_statusCode_5() const { return ___statusCode_5; }
	inline int32_t* get_address_of_statusCode_5() { return &___statusCode_5; }
	inline void set_statusCode_5(int32_t value)
	{
		___statusCode_5 = value;
	}

	inline static int32_t get_offset_of_lastModified_6() { return static_cast<int32_t>(offsetof(FtpWebResponse_t8775110950F0637C1A8A495892122865F05FC205, ___lastModified_6)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_lastModified_6() const { return ___lastModified_6; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_lastModified_6() { return &___lastModified_6; }
	inline void set_lastModified_6(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___lastModified_6 = value;
	}

	inline static int32_t get_offset_of_bannerMessage_7() { return static_cast<int32_t>(offsetof(FtpWebResponse_t8775110950F0637C1A8A495892122865F05FC205, ___bannerMessage_7)); }
	inline String_t* get_bannerMessage_7() const { return ___bannerMessage_7; }
	inline String_t** get_address_of_bannerMessage_7() { return &___bannerMessage_7; }
	inline void set_bannerMessage_7(String_t* value)
	{
		___bannerMessage_7 = value;
		Il2CppCodeGenWriteBarrier((&___bannerMessage_7), value);
	}

	inline static int32_t get_offset_of_welcomeMessage_8() { return static_cast<int32_t>(offsetof(FtpWebResponse_t8775110950F0637C1A8A495892122865F05FC205, ___welcomeMessage_8)); }
	inline String_t* get_welcomeMessage_8() const { return ___welcomeMessage_8; }
	inline String_t** get_address_of_welcomeMessage_8() { return &___welcomeMessage_8; }
	inline void set_welcomeMessage_8(String_t* value)
	{
		___welcomeMessage_8 = value;
		Il2CppCodeGenWriteBarrier((&___welcomeMessage_8), value);
	}

	inline static int32_t get_offset_of_exitMessage_9() { return static_cast<int32_t>(offsetof(FtpWebResponse_t8775110950F0637C1A8A495892122865F05FC205, ___exitMessage_9)); }
	inline String_t* get_exitMessage_9() const { return ___exitMessage_9; }
	inline String_t** get_address_of_exitMessage_9() { return &___exitMessage_9; }
	inline void set_exitMessage_9(String_t* value)
	{
		___exitMessage_9 = value;
		Il2CppCodeGenWriteBarrier((&___exitMessage_9), value);
	}

	inline static int32_t get_offset_of_statusDescription_10() { return static_cast<int32_t>(offsetof(FtpWebResponse_t8775110950F0637C1A8A495892122865F05FC205, ___statusDescription_10)); }
	inline String_t* get_statusDescription_10() const { return ___statusDescription_10; }
	inline String_t** get_address_of_statusDescription_10() { return &___statusDescription_10; }
	inline void set_statusDescription_10(String_t* value)
	{
		___statusDescription_10 = value;
		Il2CppCodeGenWriteBarrier((&___statusDescription_10), value);
	}

	inline static int32_t get_offset_of_method_11() { return static_cast<int32_t>(offsetof(FtpWebResponse_t8775110950F0637C1A8A495892122865F05FC205, ___method_11)); }
	inline String_t* get_method_11() const { return ___method_11; }
	inline String_t** get_address_of_method_11() { return &___method_11; }
	inline void set_method_11(String_t* value)
	{
		___method_11 = value;
		Il2CppCodeGenWriteBarrier((&___method_11), value);
	}

	inline static int32_t get_offset_of_disposed_12() { return static_cast<int32_t>(offsetof(FtpWebResponse_t8775110950F0637C1A8A495892122865F05FC205, ___disposed_12)); }
	inline bool get_disposed_12() const { return ___disposed_12; }
	inline bool* get_address_of_disposed_12() { return &___disposed_12; }
	inline void set_disposed_12(bool value)
	{
		___disposed_12 = value;
	}

	inline static int32_t get_offset_of_request_13() { return static_cast<int32_t>(offsetof(FtpWebResponse_t8775110950F0637C1A8A495892122865F05FC205, ___request_13)); }
	inline FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA * get_request_13() const { return ___request_13; }
	inline FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA ** get_address_of_request_13() { return &___request_13; }
	inline void set_request_13(FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA * value)
	{
		___request_13 = value;
		Il2CppCodeGenWriteBarrier((&___request_13), value);
	}

	inline static int32_t get_offset_of_contentLength_14() { return static_cast<int32_t>(offsetof(FtpWebResponse_t8775110950F0637C1A8A495892122865F05FC205, ___contentLength_14)); }
	inline int64_t get_contentLength_14() const { return ___contentLength_14; }
	inline int64_t* get_address_of_contentLength_14() { return &___contentLength_14; }
	inline void set_contentLength_14(int64_t value)
	{
		___contentLength_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FTPWEBRESPONSE_T8775110950F0637C1A8A495892122865F05FC205_H
#ifndef HTTPCONNECTION_TD93CEDEFFD0DCBC29F9B704487A0A25D065D466E_H
#define HTTPCONNECTION_TD93CEDEFFD0DCBC29F9B704487A0A25D065D466E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HttpConnection
struct  HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E  : public RuntimeObject
{
public:
	// System.Net.Sockets.Socket System.Net.HttpConnection::sock
	Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 * ___sock_2;
	// System.IO.Stream System.Net.HttpConnection::stream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___stream_3;
	// System.Net.EndPointListener System.Net.HttpConnection::epl
	EndPointListener_tF1D36E022EAFCD075CD0398D2868363AA9FB10C2 * ___epl_4;
	// System.IO.MemoryStream System.Net.HttpConnection::ms
	MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * ___ms_5;
	// System.Byte[] System.Net.HttpConnection::buffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___buffer_6;
	// System.Net.HttpListenerContext System.Net.HttpConnection::context
	HttpListenerContext_t438DA3609B8D775884622DF392BD7D72C651FCFA * ___context_7;
	// System.Text.StringBuilder System.Net.HttpConnection::current_line
	StringBuilder_t * ___current_line_8;
	// System.Net.ListenerPrefix System.Net.HttpConnection::prefix
	ListenerPrefix_t7E983ACD2D31FE4DC42DE02AE82EC24734DC57D7 * ___prefix_9;
	// System.Net.RequestStream System.Net.HttpConnection::i_stream
	RequestStream_t4BF6FD18AB7261094F557E867606B91FECF74D35 * ___i_stream_10;
	// System.Net.ResponseStream System.Net.HttpConnection::o_stream
	ResponseStream_tE537A0E20974BEB6629CBEBE42C27C9B97126032 * ___o_stream_11;
	// System.Boolean System.Net.HttpConnection::chunked
	bool ___chunked_12;
	// System.Int32 System.Net.HttpConnection::reuses
	int32_t ___reuses_13;
	// System.Boolean System.Net.HttpConnection::context_bound
	bool ___context_bound_14;
	// System.Boolean System.Net.HttpConnection::secure
	bool ___secure_15;
	// System.Security.Cryptography.X509Certificates.X509Certificate System.Net.HttpConnection::cert
	X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 * ___cert_16;
	// System.Int32 System.Net.HttpConnection::s_timeout
	int32_t ___s_timeout_17;
	// System.Threading.Timer System.Net.HttpConnection::timer
	Timer_t67FAB8E41573B4FA09CA56AE30725AF4297C2553 * ___timer_18;
	// System.Net.IPEndPoint System.Net.HttpConnection::local_ep
	IPEndPoint_tCD29981135F7B1989C3845BF455AD44EBC13DE3F * ___local_ep_19;
	// System.Net.HttpListener System.Net.HttpConnection::last_listener
	HttpListener_tAAE9DCF34EB3C09285BF833F390C8CC61C7D247E * ___last_listener_20;
	// System.Int32[] System.Net.HttpConnection::client_cert_errors
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___client_cert_errors_21;
	// System.Security.Cryptography.X509Certificates.X509Certificate2 System.Net.HttpConnection::client_cert
	X509Certificate2_tC1C49EB4CFD571C2FFDE940C24BC69651A058F73 * ___client_cert_22;
	// System.Net.Security.SslStream System.Net.HttpConnection::ssl_stream
	SslStream_t9CEE8F6E125C734DD807D9289C86860FFEE81087 * ___ssl_stream_23;
	// System.Net.HttpConnection_InputState System.Net.HttpConnection::input_state
	int32_t ___input_state_24;
	// System.Net.HttpConnection_LineState System.Net.HttpConnection::line_state
	int32_t ___line_state_25;
	// System.Int32 System.Net.HttpConnection::position
	int32_t ___position_26;

public:
	inline static int32_t get_offset_of_sock_2() { return static_cast<int32_t>(offsetof(HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E, ___sock_2)); }
	inline Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 * get_sock_2() const { return ___sock_2; }
	inline Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 ** get_address_of_sock_2() { return &___sock_2; }
	inline void set_sock_2(Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 * value)
	{
		___sock_2 = value;
		Il2CppCodeGenWriteBarrier((&___sock_2), value);
	}

	inline static int32_t get_offset_of_stream_3() { return static_cast<int32_t>(offsetof(HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E, ___stream_3)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_stream_3() const { return ___stream_3; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_stream_3() { return &___stream_3; }
	inline void set_stream_3(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___stream_3 = value;
		Il2CppCodeGenWriteBarrier((&___stream_3), value);
	}

	inline static int32_t get_offset_of_epl_4() { return static_cast<int32_t>(offsetof(HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E, ___epl_4)); }
	inline EndPointListener_tF1D36E022EAFCD075CD0398D2868363AA9FB10C2 * get_epl_4() const { return ___epl_4; }
	inline EndPointListener_tF1D36E022EAFCD075CD0398D2868363AA9FB10C2 ** get_address_of_epl_4() { return &___epl_4; }
	inline void set_epl_4(EndPointListener_tF1D36E022EAFCD075CD0398D2868363AA9FB10C2 * value)
	{
		___epl_4 = value;
		Il2CppCodeGenWriteBarrier((&___epl_4), value);
	}

	inline static int32_t get_offset_of_ms_5() { return static_cast<int32_t>(offsetof(HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E, ___ms_5)); }
	inline MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * get_ms_5() const { return ___ms_5; }
	inline MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C ** get_address_of_ms_5() { return &___ms_5; }
	inline void set_ms_5(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * value)
	{
		___ms_5 = value;
		Il2CppCodeGenWriteBarrier((&___ms_5), value);
	}

	inline static int32_t get_offset_of_buffer_6() { return static_cast<int32_t>(offsetof(HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E, ___buffer_6)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_buffer_6() const { return ___buffer_6; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_buffer_6() { return &___buffer_6; }
	inline void set_buffer_6(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___buffer_6 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_6), value);
	}

	inline static int32_t get_offset_of_context_7() { return static_cast<int32_t>(offsetof(HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E, ___context_7)); }
	inline HttpListenerContext_t438DA3609B8D775884622DF392BD7D72C651FCFA * get_context_7() const { return ___context_7; }
	inline HttpListenerContext_t438DA3609B8D775884622DF392BD7D72C651FCFA ** get_address_of_context_7() { return &___context_7; }
	inline void set_context_7(HttpListenerContext_t438DA3609B8D775884622DF392BD7D72C651FCFA * value)
	{
		___context_7 = value;
		Il2CppCodeGenWriteBarrier((&___context_7), value);
	}

	inline static int32_t get_offset_of_current_line_8() { return static_cast<int32_t>(offsetof(HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E, ___current_line_8)); }
	inline StringBuilder_t * get_current_line_8() const { return ___current_line_8; }
	inline StringBuilder_t ** get_address_of_current_line_8() { return &___current_line_8; }
	inline void set_current_line_8(StringBuilder_t * value)
	{
		___current_line_8 = value;
		Il2CppCodeGenWriteBarrier((&___current_line_8), value);
	}

	inline static int32_t get_offset_of_prefix_9() { return static_cast<int32_t>(offsetof(HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E, ___prefix_9)); }
	inline ListenerPrefix_t7E983ACD2D31FE4DC42DE02AE82EC24734DC57D7 * get_prefix_9() const { return ___prefix_9; }
	inline ListenerPrefix_t7E983ACD2D31FE4DC42DE02AE82EC24734DC57D7 ** get_address_of_prefix_9() { return &___prefix_9; }
	inline void set_prefix_9(ListenerPrefix_t7E983ACD2D31FE4DC42DE02AE82EC24734DC57D7 * value)
	{
		___prefix_9 = value;
		Il2CppCodeGenWriteBarrier((&___prefix_9), value);
	}

	inline static int32_t get_offset_of_i_stream_10() { return static_cast<int32_t>(offsetof(HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E, ___i_stream_10)); }
	inline RequestStream_t4BF6FD18AB7261094F557E867606B91FECF74D35 * get_i_stream_10() const { return ___i_stream_10; }
	inline RequestStream_t4BF6FD18AB7261094F557E867606B91FECF74D35 ** get_address_of_i_stream_10() { return &___i_stream_10; }
	inline void set_i_stream_10(RequestStream_t4BF6FD18AB7261094F557E867606B91FECF74D35 * value)
	{
		___i_stream_10 = value;
		Il2CppCodeGenWriteBarrier((&___i_stream_10), value);
	}

	inline static int32_t get_offset_of_o_stream_11() { return static_cast<int32_t>(offsetof(HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E, ___o_stream_11)); }
	inline ResponseStream_tE537A0E20974BEB6629CBEBE42C27C9B97126032 * get_o_stream_11() const { return ___o_stream_11; }
	inline ResponseStream_tE537A0E20974BEB6629CBEBE42C27C9B97126032 ** get_address_of_o_stream_11() { return &___o_stream_11; }
	inline void set_o_stream_11(ResponseStream_tE537A0E20974BEB6629CBEBE42C27C9B97126032 * value)
	{
		___o_stream_11 = value;
		Il2CppCodeGenWriteBarrier((&___o_stream_11), value);
	}

	inline static int32_t get_offset_of_chunked_12() { return static_cast<int32_t>(offsetof(HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E, ___chunked_12)); }
	inline bool get_chunked_12() const { return ___chunked_12; }
	inline bool* get_address_of_chunked_12() { return &___chunked_12; }
	inline void set_chunked_12(bool value)
	{
		___chunked_12 = value;
	}

	inline static int32_t get_offset_of_reuses_13() { return static_cast<int32_t>(offsetof(HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E, ___reuses_13)); }
	inline int32_t get_reuses_13() const { return ___reuses_13; }
	inline int32_t* get_address_of_reuses_13() { return &___reuses_13; }
	inline void set_reuses_13(int32_t value)
	{
		___reuses_13 = value;
	}

	inline static int32_t get_offset_of_context_bound_14() { return static_cast<int32_t>(offsetof(HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E, ___context_bound_14)); }
	inline bool get_context_bound_14() const { return ___context_bound_14; }
	inline bool* get_address_of_context_bound_14() { return &___context_bound_14; }
	inline void set_context_bound_14(bool value)
	{
		___context_bound_14 = value;
	}

	inline static int32_t get_offset_of_secure_15() { return static_cast<int32_t>(offsetof(HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E, ___secure_15)); }
	inline bool get_secure_15() const { return ___secure_15; }
	inline bool* get_address_of_secure_15() { return &___secure_15; }
	inline void set_secure_15(bool value)
	{
		___secure_15 = value;
	}

	inline static int32_t get_offset_of_cert_16() { return static_cast<int32_t>(offsetof(HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E, ___cert_16)); }
	inline X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 * get_cert_16() const { return ___cert_16; }
	inline X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 ** get_address_of_cert_16() { return &___cert_16; }
	inline void set_cert_16(X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 * value)
	{
		___cert_16 = value;
		Il2CppCodeGenWriteBarrier((&___cert_16), value);
	}

	inline static int32_t get_offset_of_s_timeout_17() { return static_cast<int32_t>(offsetof(HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E, ___s_timeout_17)); }
	inline int32_t get_s_timeout_17() const { return ___s_timeout_17; }
	inline int32_t* get_address_of_s_timeout_17() { return &___s_timeout_17; }
	inline void set_s_timeout_17(int32_t value)
	{
		___s_timeout_17 = value;
	}

	inline static int32_t get_offset_of_timer_18() { return static_cast<int32_t>(offsetof(HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E, ___timer_18)); }
	inline Timer_t67FAB8E41573B4FA09CA56AE30725AF4297C2553 * get_timer_18() const { return ___timer_18; }
	inline Timer_t67FAB8E41573B4FA09CA56AE30725AF4297C2553 ** get_address_of_timer_18() { return &___timer_18; }
	inline void set_timer_18(Timer_t67FAB8E41573B4FA09CA56AE30725AF4297C2553 * value)
	{
		___timer_18 = value;
		Il2CppCodeGenWriteBarrier((&___timer_18), value);
	}

	inline static int32_t get_offset_of_local_ep_19() { return static_cast<int32_t>(offsetof(HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E, ___local_ep_19)); }
	inline IPEndPoint_tCD29981135F7B1989C3845BF455AD44EBC13DE3F * get_local_ep_19() const { return ___local_ep_19; }
	inline IPEndPoint_tCD29981135F7B1989C3845BF455AD44EBC13DE3F ** get_address_of_local_ep_19() { return &___local_ep_19; }
	inline void set_local_ep_19(IPEndPoint_tCD29981135F7B1989C3845BF455AD44EBC13DE3F * value)
	{
		___local_ep_19 = value;
		Il2CppCodeGenWriteBarrier((&___local_ep_19), value);
	}

	inline static int32_t get_offset_of_last_listener_20() { return static_cast<int32_t>(offsetof(HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E, ___last_listener_20)); }
	inline HttpListener_tAAE9DCF34EB3C09285BF833F390C8CC61C7D247E * get_last_listener_20() const { return ___last_listener_20; }
	inline HttpListener_tAAE9DCF34EB3C09285BF833F390C8CC61C7D247E ** get_address_of_last_listener_20() { return &___last_listener_20; }
	inline void set_last_listener_20(HttpListener_tAAE9DCF34EB3C09285BF833F390C8CC61C7D247E * value)
	{
		___last_listener_20 = value;
		Il2CppCodeGenWriteBarrier((&___last_listener_20), value);
	}

	inline static int32_t get_offset_of_client_cert_errors_21() { return static_cast<int32_t>(offsetof(HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E, ___client_cert_errors_21)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_client_cert_errors_21() const { return ___client_cert_errors_21; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_client_cert_errors_21() { return &___client_cert_errors_21; }
	inline void set_client_cert_errors_21(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___client_cert_errors_21 = value;
		Il2CppCodeGenWriteBarrier((&___client_cert_errors_21), value);
	}

	inline static int32_t get_offset_of_client_cert_22() { return static_cast<int32_t>(offsetof(HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E, ___client_cert_22)); }
	inline X509Certificate2_tC1C49EB4CFD571C2FFDE940C24BC69651A058F73 * get_client_cert_22() const { return ___client_cert_22; }
	inline X509Certificate2_tC1C49EB4CFD571C2FFDE940C24BC69651A058F73 ** get_address_of_client_cert_22() { return &___client_cert_22; }
	inline void set_client_cert_22(X509Certificate2_tC1C49EB4CFD571C2FFDE940C24BC69651A058F73 * value)
	{
		___client_cert_22 = value;
		Il2CppCodeGenWriteBarrier((&___client_cert_22), value);
	}

	inline static int32_t get_offset_of_ssl_stream_23() { return static_cast<int32_t>(offsetof(HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E, ___ssl_stream_23)); }
	inline SslStream_t9CEE8F6E125C734DD807D9289C86860FFEE81087 * get_ssl_stream_23() const { return ___ssl_stream_23; }
	inline SslStream_t9CEE8F6E125C734DD807D9289C86860FFEE81087 ** get_address_of_ssl_stream_23() { return &___ssl_stream_23; }
	inline void set_ssl_stream_23(SslStream_t9CEE8F6E125C734DD807D9289C86860FFEE81087 * value)
	{
		___ssl_stream_23 = value;
		Il2CppCodeGenWriteBarrier((&___ssl_stream_23), value);
	}

	inline static int32_t get_offset_of_input_state_24() { return static_cast<int32_t>(offsetof(HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E, ___input_state_24)); }
	inline int32_t get_input_state_24() const { return ___input_state_24; }
	inline int32_t* get_address_of_input_state_24() { return &___input_state_24; }
	inline void set_input_state_24(int32_t value)
	{
		___input_state_24 = value;
	}

	inline static int32_t get_offset_of_line_state_25() { return static_cast<int32_t>(offsetof(HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E, ___line_state_25)); }
	inline int32_t get_line_state_25() const { return ___line_state_25; }
	inline int32_t* get_address_of_line_state_25() { return &___line_state_25; }
	inline void set_line_state_25(int32_t value)
	{
		___line_state_25 = value;
	}

	inline static int32_t get_offset_of_position_26() { return static_cast<int32_t>(offsetof(HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E, ___position_26)); }
	inline int32_t get_position_26() const { return ___position_26; }
	inline int32_t* get_address_of_position_26() { return &___position_26; }
	inline void set_position_26(int32_t value)
	{
		___position_26 = value;
	}
};

struct HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E_StaticFields
{
public:
	// System.AsyncCallback System.Net.HttpConnection::onread_cb
	AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___onread_cb_0;

public:
	inline static int32_t get_offset_of_onread_cb_0() { return static_cast<int32_t>(offsetof(HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E_StaticFields, ___onread_cb_0)); }
	inline AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * get_onread_cb_0() const { return ___onread_cb_0; }
	inline AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 ** get_address_of_onread_cb_0() { return &___onread_cb_0; }
	inline void set_onread_cb_0(AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * value)
	{
		___onread_cb_0 = value;
		Il2CppCodeGenWriteBarrier((&___onread_cb_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPCONNECTION_TD93CEDEFFD0DCBC29F9B704487A0A25D065D466E_H
#ifndef HTTPLISTENER_TAAE9DCF34EB3C09285BF833F390C8CC61C7D247E_H
#define HTTPLISTENER_TAAE9DCF34EB3C09285BF833F390C8CC61C7D247E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HttpListener
struct  HttpListener_tAAE9DCF34EB3C09285BF833F390C8CC61C7D247E  : public RuntimeObject
{
public:
	// Mono.Security.Interface.MonoTlsProvider System.Net.HttpListener::tlsProvider
	MonoTlsProvider_tDCD056C5BBBE59ED6BAF63F25952B406C1143C27 * ___tlsProvider_0;
	// Mono.Security.Interface.MonoTlsSettings System.Net.HttpListener::tlsSettings
	MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF * ___tlsSettings_1;
	// System.Security.Cryptography.X509Certificates.X509Certificate System.Net.HttpListener::certificate
	X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 * ___certificate_2;
	// System.Net.AuthenticationSchemes System.Net.HttpListener::auth_schemes
	int32_t ___auth_schemes_3;
	// System.Net.HttpListenerPrefixCollection System.Net.HttpListener::prefixes
	HttpListenerPrefixCollection_tCFDDEB0773E99D9DA834AB2FBF1AF0F2B1AC7B5A * ___prefixes_4;
	// System.Net.AuthenticationSchemeSelector System.Net.HttpListener::auth_selector
	AuthenticationSchemeSelector_t77F5166A2AFED823020E020C95E23973E5A86F74 * ___auth_selector_5;
	// System.String System.Net.HttpListener::realm
	String_t* ___realm_6;
	// System.Boolean System.Net.HttpListener::ignore_write_exceptions
	bool ___ignore_write_exceptions_7;
	// System.Boolean System.Net.HttpListener::unsafe_ntlm_auth
	bool ___unsafe_ntlm_auth_8;
	// System.Boolean System.Net.HttpListener::listening
	bool ___listening_9;
	// System.Boolean System.Net.HttpListener::disposed
	bool ___disposed_10;
	// System.Object System.Net.HttpListener::_internalLock
	RuntimeObject * ____internalLock_11;
	// System.Collections.Hashtable System.Net.HttpListener::registry
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___registry_12;
	// System.Collections.ArrayList System.Net.HttpListener::ctx_queue
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ___ctx_queue_13;
	// System.Collections.ArrayList System.Net.HttpListener::wait_queue
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ___wait_queue_14;
	// System.Collections.Hashtable System.Net.HttpListener::connections
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___connections_15;
	// System.Net.ServiceNameStore System.Net.HttpListener::defaultServiceNames
	ServiceNameStore_tF45F4346CE113F34D3E08E515FD32642512CEA37 * ___defaultServiceNames_16;
	// System.Security.Authentication.ExtendedProtection.ExtendedProtectionPolicy System.Net.HttpListener::extendedProtectionPolicy
	ExtendedProtectionPolicy_t5DB5E76D5F3E2BF3CAE465DF85198A91DF30A8BC * ___extendedProtectionPolicy_17;
	// System.Net.HttpListener_ExtendedProtectionSelector System.Net.HttpListener::extendedProtectionSelectorDelegate
	ExtendedProtectionSelector_tC8A40F0217D1CF85224BBF5F398A081FA11C692C * ___extendedProtectionSelectorDelegate_18;

public:
	inline static int32_t get_offset_of_tlsProvider_0() { return static_cast<int32_t>(offsetof(HttpListener_tAAE9DCF34EB3C09285BF833F390C8CC61C7D247E, ___tlsProvider_0)); }
	inline MonoTlsProvider_tDCD056C5BBBE59ED6BAF63F25952B406C1143C27 * get_tlsProvider_0() const { return ___tlsProvider_0; }
	inline MonoTlsProvider_tDCD056C5BBBE59ED6BAF63F25952B406C1143C27 ** get_address_of_tlsProvider_0() { return &___tlsProvider_0; }
	inline void set_tlsProvider_0(MonoTlsProvider_tDCD056C5BBBE59ED6BAF63F25952B406C1143C27 * value)
	{
		___tlsProvider_0 = value;
		Il2CppCodeGenWriteBarrier((&___tlsProvider_0), value);
	}

	inline static int32_t get_offset_of_tlsSettings_1() { return static_cast<int32_t>(offsetof(HttpListener_tAAE9DCF34EB3C09285BF833F390C8CC61C7D247E, ___tlsSettings_1)); }
	inline MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF * get_tlsSettings_1() const { return ___tlsSettings_1; }
	inline MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF ** get_address_of_tlsSettings_1() { return &___tlsSettings_1; }
	inline void set_tlsSettings_1(MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF * value)
	{
		___tlsSettings_1 = value;
		Il2CppCodeGenWriteBarrier((&___tlsSettings_1), value);
	}

	inline static int32_t get_offset_of_certificate_2() { return static_cast<int32_t>(offsetof(HttpListener_tAAE9DCF34EB3C09285BF833F390C8CC61C7D247E, ___certificate_2)); }
	inline X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 * get_certificate_2() const { return ___certificate_2; }
	inline X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 ** get_address_of_certificate_2() { return &___certificate_2; }
	inline void set_certificate_2(X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 * value)
	{
		___certificate_2 = value;
		Il2CppCodeGenWriteBarrier((&___certificate_2), value);
	}

	inline static int32_t get_offset_of_auth_schemes_3() { return static_cast<int32_t>(offsetof(HttpListener_tAAE9DCF34EB3C09285BF833F390C8CC61C7D247E, ___auth_schemes_3)); }
	inline int32_t get_auth_schemes_3() const { return ___auth_schemes_3; }
	inline int32_t* get_address_of_auth_schemes_3() { return &___auth_schemes_3; }
	inline void set_auth_schemes_3(int32_t value)
	{
		___auth_schemes_3 = value;
	}

	inline static int32_t get_offset_of_prefixes_4() { return static_cast<int32_t>(offsetof(HttpListener_tAAE9DCF34EB3C09285BF833F390C8CC61C7D247E, ___prefixes_4)); }
	inline HttpListenerPrefixCollection_tCFDDEB0773E99D9DA834AB2FBF1AF0F2B1AC7B5A * get_prefixes_4() const { return ___prefixes_4; }
	inline HttpListenerPrefixCollection_tCFDDEB0773E99D9DA834AB2FBF1AF0F2B1AC7B5A ** get_address_of_prefixes_4() { return &___prefixes_4; }
	inline void set_prefixes_4(HttpListenerPrefixCollection_tCFDDEB0773E99D9DA834AB2FBF1AF0F2B1AC7B5A * value)
	{
		___prefixes_4 = value;
		Il2CppCodeGenWriteBarrier((&___prefixes_4), value);
	}

	inline static int32_t get_offset_of_auth_selector_5() { return static_cast<int32_t>(offsetof(HttpListener_tAAE9DCF34EB3C09285BF833F390C8CC61C7D247E, ___auth_selector_5)); }
	inline AuthenticationSchemeSelector_t77F5166A2AFED823020E020C95E23973E5A86F74 * get_auth_selector_5() const { return ___auth_selector_5; }
	inline AuthenticationSchemeSelector_t77F5166A2AFED823020E020C95E23973E5A86F74 ** get_address_of_auth_selector_5() { return &___auth_selector_5; }
	inline void set_auth_selector_5(AuthenticationSchemeSelector_t77F5166A2AFED823020E020C95E23973E5A86F74 * value)
	{
		___auth_selector_5 = value;
		Il2CppCodeGenWriteBarrier((&___auth_selector_5), value);
	}

	inline static int32_t get_offset_of_realm_6() { return static_cast<int32_t>(offsetof(HttpListener_tAAE9DCF34EB3C09285BF833F390C8CC61C7D247E, ___realm_6)); }
	inline String_t* get_realm_6() const { return ___realm_6; }
	inline String_t** get_address_of_realm_6() { return &___realm_6; }
	inline void set_realm_6(String_t* value)
	{
		___realm_6 = value;
		Il2CppCodeGenWriteBarrier((&___realm_6), value);
	}

	inline static int32_t get_offset_of_ignore_write_exceptions_7() { return static_cast<int32_t>(offsetof(HttpListener_tAAE9DCF34EB3C09285BF833F390C8CC61C7D247E, ___ignore_write_exceptions_7)); }
	inline bool get_ignore_write_exceptions_7() const { return ___ignore_write_exceptions_7; }
	inline bool* get_address_of_ignore_write_exceptions_7() { return &___ignore_write_exceptions_7; }
	inline void set_ignore_write_exceptions_7(bool value)
	{
		___ignore_write_exceptions_7 = value;
	}

	inline static int32_t get_offset_of_unsafe_ntlm_auth_8() { return static_cast<int32_t>(offsetof(HttpListener_tAAE9DCF34EB3C09285BF833F390C8CC61C7D247E, ___unsafe_ntlm_auth_8)); }
	inline bool get_unsafe_ntlm_auth_8() const { return ___unsafe_ntlm_auth_8; }
	inline bool* get_address_of_unsafe_ntlm_auth_8() { return &___unsafe_ntlm_auth_8; }
	inline void set_unsafe_ntlm_auth_8(bool value)
	{
		___unsafe_ntlm_auth_8 = value;
	}

	inline static int32_t get_offset_of_listening_9() { return static_cast<int32_t>(offsetof(HttpListener_tAAE9DCF34EB3C09285BF833F390C8CC61C7D247E, ___listening_9)); }
	inline bool get_listening_9() const { return ___listening_9; }
	inline bool* get_address_of_listening_9() { return &___listening_9; }
	inline void set_listening_9(bool value)
	{
		___listening_9 = value;
	}

	inline static int32_t get_offset_of_disposed_10() { return static_cast<int32_t>(offsetof(HttpListener_tAAE9DCF34EB3C09285BF833F390C8CC61C7D247E, ___disposed_10)); }
	inline bool get_disposed_10() const { return ___disposed_10; }
	inline bool* get_address_of_disposed_10() { return &___disposed_10; }
	inline void set_disposed_10(bool value)
	{
		___disposed_10 = value;
	}

	inline static int32_t get_offset_of__internalLock_11() { return static_cast<int32_t>(offsetof(HttpListener_tAAE9DCF34EB3C09285BF833F390C8CC61C7D247E, ____internalLock_11)); }
	inline RuntimeObject * get__internalLock_11() const { return ____internalLock_11; }
	inline RuntimeObject ** get_address_of__internalLock_11() { return &____internalLock_11; }
	inline void set__internalLock_11(RuntimeObject * value)
	{
		____internalLock_11 = value;
		Il2CppCodeGenWriteBarrier((&____internalLock_11), value);
	}

	inline static int32_t get_offset_of_registry_12() { return static_cast<int32_t>(offsetof(HttpListener_tAAE9DCF34EB3C09285BF833F390C8CC61C7D247E, ___registry_12)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_registry_12() const { return ___registry_12; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_registry_12() { return &___registry_12; }
	inline void set_registry_12(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___registry_12 = value;
		Il2CppCodeGenWriteBarrier((&___registry_12), value);
	}

	inline static int32_t get_offset_of_ctx_queue_13() { return static_cast<int32_t>(offsetof(HttpListener_tAAE9DCF34EB3C09285BF833F390C8CC61C7D247E, ___ctx_queue_13)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get_ctx_queue_13() const { return ___ctx_queue_13; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of_ctx_queue_13() { return &___ctx_queue_13; }
	inline void set_ctx_queue_13(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		___ctx_queue_13 = value;
		Il2CppCodeGenWriteBarrier((&___ctx_queue_13), value);
	}

	inline static int32_t get_offset_of_wait_queue_14() { return static_cast<int32_t>(offsetof(HttpListener_tAAE9DCF34EB3C09285BF833F390C8CC61C7D247E, ___wait_queue_14)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get_wait_queue_14() const { return ___wait_queue_14; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of_wait_queue_14() { return &___wait_queue_14; }
	inline void set_wait_queue_14(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		___wait_queue_14 = value;
		Il2CppCodeGenWriteBarrier((&___wait_queue_14), value);
	}

	inline static int32_t get_offset_of_connections_15() { return static_cast<int32_t>(offsetof(HttpListener_tAAE9DCF34EB3C09285BF833F390C8CC61C7D247E, ___connections_15)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_connections_15() const { return ___connections_15; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_connections_15() { return &___connections_15; }
	inline void set_connections_15(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___connections_15 = value;
		Il2CppCodeGenWriteBarrier((&___connections_15), value);
	}

	inline static int32_t get_offset_of_defaultServiceNames_16() { return static_cast<int32_t>(offsetof(HttpListener_tAAE9DCF34EB3C09285BF833F390C8CC61C7D247E, ___defaultServiceNames_16)); }
	inline ServiceNameStore_tF45F4346CE113F34D3E08E515FD32642512CEA37 * get_defaultServiceNames_16() const { return ___defaultServiceNames_16; }
	inline ServiceNameStore_tF45F4346CE113F34D3E08E515FD32642512CEA37 ** get_address_of_defaultServiceNames_16() { return &___defaultServiceNames_16; }
	inline void set_defaultServiceNames_16(ServiceNameStore_tF45F4346CE113F34D3E08E515FD32642512CEA37 * value)
	{
		___defaultServiceNames_16 = value;
		Il2CppCodeGenWriteBarrier((&___defaultServiceNames_16), value);
	}

	inline static int32_t get_offset_of_extendedProtectionPolicy_17() { return static_cast<int32_t>(offsetof(HttpListener_tAAE9DCF34EB3C09285BF833F390C8CC61C7D247E, ___extendedProtectionPolicy_17)); }
	inline ExtendedProtectionPolicy_t5DB5E76D5F3E2BF3CAE465DF85198A91DF30A8BC * get_extendedProtectionPolicy_17() const { return ___extendedProtectionPolicy_17; }
	inline ExtendedProtectionPolicy_t5DB5E76D5F3E2BF3CAE465DF85198A91DF30A8BC ** get_address_of_extendedProtectionPolicy_17() { return &___extendedProtectionPolicy_17; }
	inline void set_extendedProtectionPolicy_17(ExtendedProtectionPolicy_t5DB5E76D5F3E2BF3CAE465DF85198A91DF30A8BC * value)
	{
		___extendedProtectionPolicy_17 = value;
		Il2CppCodeGenWriteBarrier((&___extendedProtectionPolicy_17), value);
	}

	inline static int32_t get_offset_of_extendedProtectionSelectorDelegate_18() { return static_cast<int32_t>(offsetof(HttpListener_tAAE9DCF34EB3C09285BF833F390C8CC61C7D247E, ___extendedProtectionSelectorDelegate_18)); }
	inline ExtendedProtectionSelector_tC8A40F0217D1CF85224BBF5F398A081FA11C692C * get_extendedProtectionSelectorDelegate_18() const { return ___extendedProtectionSelectorDelegate_18; }
	inline ExtendedProtectionSelector_tC8A40F0217D1CF85224BBF5F398A081FA11C692C ** get_address_of_extendedProtectionSelectorDelegate_18() { return &___extendedProtectionSelectorDelegate_18; }
	inline void set_extendedProtectionSelectorDelegate_18(ExtendedProtectionSelector_tC8A40F0217D1CF85224BBF5F398A081FA11C692C * value)
	{
		___extendedProtectionSelectorDelegate_18 = value;
		Il2CppCodeGenWriteBarrier((&___extendedProtectionSelectorDelegate_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPLISTENER_TAAE9DCF34EB3C09285BF833F390C8CC61C7D247E_H
#ifndef AUTHORIZATIONSTATE_T5C342070F47B5DBAE0089B8B6A391FDEB6914AAB_H
#define AUTHORIZATIONSTATE_T5C342070F47B5DBAE0089B8B6A391FDEB6914AAB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HttpWebRequest_AuthorizationState
struct  AuthorizationState_t5C342070F47B5DBAE0089B8B6A391FDEB6914AAB 
{
public:
	// System.Net.HttpWebRequest System.Net.HttpWebRequest_AuthorizationState::request
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0 * ___request_0;
	// System.Boolean System.Net.HttpWebRequest_AuthorizationState::isProxy
	bool ___isProxy_1;
	// System.Boolean System.Net.HttpWebRequest_AuthorizationState::isCompleted
	bool ___isCompleted_2;
	// System.Net.HttpWebRequest_NtlmAuthState System.Net.HttpWebRequest_AuthorizationState::ntlm_auth_state
	int32_t ___ntlm_auth_state_3;

public:
	inline static int32_t get_offset_of_request_0() { return static_cast<int32_t>(offsetof(AuthorizationState_t5C342070F47B5DBAE0089B8B6A391FDEB6914AAB, ___request_0)); }
	inline HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0 * get_request_0() const { return ___request_0; }
	inline HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0 ** get_address_of_request_0() { return &___request_0; }
	inline void set_request_0(HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0 * value)
	{
		___request_0 = value;
		Il2CppCodeGenWriteBarrier((&___request_0), value);
	}

	inline static int32_t get_offset_of_isProxy_1() { return static_cast<int32_t>(offsetof(AuthorizationState_t5C342070F47B5DBAE0089B8B6A391FDEB6914AAB, ___isProxy_1)); }
	inline bool get_isProxy_1() const { return ___isProxy_1; }
	inline bool* get_address_of_isProxy_1() { return &___isProxy_1; }
	inline void set_isProxy_1(bool value)
	{
		___isProxy_1 = value;
	}

	inline static int32_t get_offset_of_isCompleted_2() { return static_cast<int32_t>(offsetof(AuthorizationState_t5C342070F47B5DBAE0089B8B6A391FDEB6914AAB, ___isCompleted_2)); }
	inline bool get_isCompleted_2() const { return ___isCompleted_2; }
	inline bool* get_address_of_isCompleted_2() { return &___isCompleted_2; }
	inline void set_isCompleted_2(bool value)
	{
		___isCompleted_2 = value;
	}

	inline static int32_t get_offset_of_ntlm_auth_state_3() { return static_cast<int32_t>(offsetof(AuthorizationState_t5C342070F47B5DBAE0089B8B6A391FDEB6914AAB, ___ntlm_auth_state_3)); }
	inline int32_t get_ntlm_auth_state_3() const { return ___ntlm_auth_state_3; }
	inline int32_t* get_address_of_ntlm_auth_state_3() { return &___ntlm_auth_state_3; }
	inline void set_ntlm_auth_state_3(int32_t value)
	{
		___ntlm_auth_state_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Net.HttpWebRequest/AuthorizationState
struct AuthorizationState_t5C342070F47B5DBAE0089B8B6A391FDEB6914AAB_marshaled_pinvoke
{
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0 * ___request_0;
	int32_t ___isProxy_1;
	int32_t ___isCompleted_2;
	int32_t ___ntlm_auth_state_3;
};
// Native definition for COM marshalling of System.Net.HttpWebRequest/AuthorizationState
struct AuthorizationState_t5C342070F47B5DBAE0089B8B6A391FDEB6914AAB_marshaled_com
{
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0 * ___request_0;
	int32_t ___isProxy_1;
	int32_t ___isCompleted_2;
	int32_t ___ntlm_auth_state_3;
};
#endif // AUTHORIZATIONSTATE_T5C342070F47B5DBAE0089B8B6A391FDEB6914AAB_H
#ifndef HTTPWEBRESPONSE_T34CF6A40A4748A0F8694FEFEA3723D9AE3EF3951_H
#define HTTPWEBRESPONSE_T34CF6A40A4748A0F8694FEFEA3723D9AE3EF3951_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HttpWebResponse
struct  HttpWebResponse_t34CF6A40A4748A0F8694FEFEA3723D9AE3EF3951  : public WebResponse_t5C91B5B83E2FBA2EABC6FDF2A70E9AFD9BB059BD
{
public:
	// System.Uri System.Net.HttpWebResponse::uri
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___uri_3;
	// System.Net.WebHeaderCollection System.Net.HttpWebResponse::webHeaders
	WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304 * ___webHeaders_4;
	// System.Net.CookieCollection System.Net.HttpWebResponse::cookieCollection
	CookieCollection_t69ADF0ABD99419E54AB4740B341D94F443D995A3 * ___cookieCollection_5;
	// System.String System.Net.HttpWebResponse::method
	String_t* ___method_6;
	// System.Version System.Net.HttpWebResponse::version
	Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD * ___version_7;
	// System.Net.HttpStatusCode System.Net.HttpWebResponse::statusCode
	int32_t ___statusCode_8;
	// System.String System.Net.HttpWebResponse::statusDescription
	String_t* ___statusDescription_9;
	// System.Int64 System.Net.HttpWebResponse::contentLength
	int64_t ___contentLength_10;
	// System.String System.Net.HttpWebResponse::contentType
	String_t* ___contentType_11;
	// System.Net.CookieContainer System.Net.HttpWebResponse::cookie_container
	CookieContainer_t7E062D04BAED9F3B30DDEC14B09660BB506A2A73 * ___cookie_container_12;
	// System.Boolean System.Net.HttpWebResponse::disposed
	bool ___disposed_13;
	// System.IO.Stream System.Net.HttpWebResponse::stream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___stream_14;

public:
	inline static int32_t get_offset_of_uri_3() { return static_cast<int32_t>(offsetof(HttpWebResponse_t34CF6A40A4748A0F8694FEFEA3723D9AE3EF3951, ___uri_3)); }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * get_uri_3() const { return ___uri_3; }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E ** get_address_of_uri_3() { return &___uri_3; }
	inline void set_uri_3(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * value)
	{
		___uri_3 = value;
		Il2CppCodeGenWriteBarrier((&___uri_3), value);
	}

	inline static int32_t get_offset_of_webHeaders_4() { return static_cast<int32_t>(offsetof(HttpWebResponse_t34CF6A40A4748A0F8694FEFEA3723D9AE3EF3951, ___webHeaders_4)); }
	inline WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304 * get_webHeaders_4() const { return ___webHeaders_4; }
	inline WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304 ** get_address_of_webHeaders_4() { return &___webHeaders_4; }
	inline void set_webHeaders_4(WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304 * value)
	{
		___webHeaders_4 = value;
		Il2CppCodeGenWriteBarrier((&___webHeaders_4), value);
	}

	inline static int32_t get_offset_of_cookieCollection_5() { return static_cast<int32_t>(offsetof(HttpWebResponse_t34CF6A40A4748A0F8694FEFEA3723D9AE3EF3951, ___cookieCollection_5)); }
	inline CookieCollection_t69ADF0ABD99419E54AB4740B341D94F443D995A3 * get_cookieCollection_5() const { return ___cookieCollection_5; }
	inline CookieCollection_t69ADF0ABD99419E54AB4740B341D94F443D995A3 ** get_address_of_cookieCollection_5() { return &___cookieCollection_5; }
	inline void set_cookieCollection_5(CookieCollection_t69ADF0ABD99419E54AB4740B341D94F443D995A3 * value)
	{
		___cookieCollection_5 = value;
		Il2CppCodeGenWriteBarrier((&___cookieCollection_5), value);
	}

	inline static int32_t get_offset_of_method_6() { return static_cast<int32_t>(offsetof(HttpWebResponse_t34CF6A40A4748A0F8694FEFEA3723D9AE3EF3951, ___method_6)); }
	inline String_t* get_method_6() const { return ___method_6; }
	inline String_t** get_address_of_method_6() { return &___method_6; }
	inline void set_method_6(String_t* value)
	{
		___method_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_6), value);
	}

	inline static int32_t get_offset_of_version_7() { return static_cast<int32_t>(offsetof(HttpWebResponse_t34CF6A40A4748A0F8694FEFEA3723D9AE3EF3951, ___version_7)); }
	inline Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD * get_version_7() const { return ___version_7; }
	inline Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD ** get_address_of_version_7() { return &___version_7; }
	inline void set_version_7(Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD * value)
	{
		___version_7 = value;
		Il2CppCodeGenWriteBarrier((&___version_7), value);
	}

	inline static int32_t get_offset_of_statusCode_8() { return static_cast<int32_t>(offsetof(HttpWebResponse_t34CF6A40A4748A0F8694FEFEA3723D9AE3EF3951, ___statusCode_8)); }
	inline int32_t get_statusCode_8() const { return ___statusCode_8; }
	inline int32_t* get_address_of_statusCode_8() { return &___statusCode_8; }
	inline void set_statusCode_8(int32_t value)
	{
		___statusCode_8 = value;
	}

	inline static int32_t get_offset_of_statusDescription_9() { return static_cast<int32_t>(offsetof(HttpWebResponse_t34CF6A40A4748A0F8694FEFEA3723D9AE3EF3951, ___statusDescription_9)); }
	inline String_t* get_statusDescription_9() const { return ___statusDescription_9; }
	inline String_t** get_address_of_statusDescription_9() { return &___statusDescription_9; }
	inline void set_statusDescription_9(String_t* value)
	{
		___statusDescription_9 = value;
		Il2CppCodeGenWriteBarrier((&___statusDescription_9), value);
	}

	inline static int32_t get_offset_of_contentLength_10() { return static_cast<int32_t>(offsetof(HttpWebResponse_t34CF6A40A4748A0F8694FEFEA3723D9AE3EF3951, ___contentLength_10)); }
	inline int64_t get_contentLength_10() const { return ___contentLength_10; }
	inline int64_t* get_address_of_contentLength_10() { return &___contentLength_10; }
	inline void set_contentLength_10(int64_t value)
	{
		___contentLength_10 = value;
	}

	inline static int32_t get_offset_of_contentType_11() { return static_cast<int32_t>(offsetof(HttpWebResponse_t34CF6A40A4748A0F8694FEFEA3723D9AE3EF3951, ___contentType_11)); }
	inline String_t* get_contentType_11() const { return ___contentType_11; }
	inline String_t** get_address_of_contentType_11() { return &___contentType_11; }
	inline void set_contentType_11(String_t* value)
	{
		___contentType_11 = value;
		Il2CppCodeGenWriteBarrier((&___contentType_11), value);
	}

	inline static int32_t get_offset_of_cookie_container_12() { return static_cast<int32_t>(offsetof(HttpWebResponse_t34CF6A40A4748A0F8694FEFEA3723D9AE3EF3951, ___cookie_container_12)); }
	inline CookieContainer_t7E062D04BAED9F3B30DDEC14B09660BB506A2A73 * get_cookie_container_12() const { return ___cookie_container_12; }
	inline CookieContainer_t7E062D04BAED9F3B30DDEC14B09660BB506A2A73 ** get_address_of_cookie_container_12() { return &___cookie_container_12; }
	inline void set_cookie_container_12(CookieContainer_t7E062D04BAED9F3B30DDEC14B09660BB506A2A73 * value)
	{
		___cookie_container_12 = value;
		Il2CppCodeGenWriteBarrier((&___cookie_container_12), value);
	}

	inline static int32_t get_offset_of_disposed_13() { return static_cast<int32_t>(offsetof(HttpWebResponse_t34CF6A40A4748A0F8694FEFEA3723D9AE3EF3951, ___disposed_13)); }
	inline bool get_disposed_13() const { return ___disposed_13; }
	inline bool* get_address_of_disposed_13() { return &___disposed_13; }
	inline void set_disposed_13(bool value)
	{
		___disposed_13 = value;
	}

	inline static int32_t get_offset_of_stream_14() { return static_cast<int32_t>(offsetof(HttpWebResponse_t34CF6A40A4748A0F8694FEFEA3723D9AE3EF3951, ___stream_14)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_stream_14() const { return ___stream_14; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_stream_14() { return &___stream_14; }
	inline void set_stream_14(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___stream_14 = value;
		Il2CppCodeGenWriteBarrier((&___stream_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPWEBRESPONSE_T34CF6A40A4748A0F8694FEFEA3723D9AE3EF3951_H
#ifndef MONOCHUNKSTREAM_T33C2B7ECB208D77D1C94F0A9F48EB5AE27DF5AB5_H
#define MONOCHUNKSTREAM_T33C2B7ECB208D77D1C94F0A9F48EB5AE27DF5AB5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.MonoChunkStream
struct  MonoChunkStream_t33C2B7ECB208D77D1C94F0A9F48EB5AE27DF5AB5  : public RuntimeObject
{
public:
	// System.Net.WebHeaderCollection System.Net.MonoChunkStream::headers
	WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304 * ___headers_0;
	// System.Int32 System.Net.MonoChunkStream::chunkSize
	int32_t ___chunkSize_1;
	// System.Int32 System.Net.MonoChunkStream::chunkRead
	int32_t ___chunkRead_2;
	// System.Int32 System.Net.MonoChunkStream::totalWritten
	int32_t ___totalWritten_3;
	// System.Net.MonoChunkStream_State System.Net.MonoChunkStream::state
	int32_t ___state_4;
	// System.Text.StringBuilder System.Net.MonoChunkStream::saved
	StringBuilder_t * ___saved_5;
	// System.Boolean System.Net.MonoChunkStream::sawCR
	bool ___sawCR_6;
	// System.Boolean System.Net.MonoChunkStream::gotit
	bool ___gotit_7;
	// System.Int32 System.Net.MonoChunkStream::trailerState
	int32_t ___trailerState_8;
	// System.Collections.ArrayList System.Net.MonoChunkStream::chunks
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ___chunks_9;

public:
	inline static int32_t get_offset_of_headers_0() { return static_cast<int32_t>(offsetof(MonoChunkStream_t33C2B7ECB208D77D1C94F0A9F48EB5AE27DF5AB5, ___headers_0)); }
	inline WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304 * get_headers_0() const { return ___headers_0; }
	inline WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304 ** get_address_of_headers_0() { return &___headers_0; }
	inline void set_headers_0(WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304 * value)
	{
		___headers_0 = value;
		Il2CppCodeGenWriteBarrier((&___headers_0), value);
	}

	inline static int32_t get_offset_of_chunkSize_1() { return static_cast<int32_t>(offsetof(MonoChunkStream_t33C2B7ECB208D77D1C94F0A9F48EB5AE27DF5AB5, ___chunkSize_1)); }
	inline int32_t get_chunkSize_1() const { return ___chunkSize_1; }
	inline int32_t* get_address_of_chunkSize_1() { return &___chunkSize_1; }
	inline void set_chunkSize_1(int32_t value)
	{
		___chunkSize_1 = value;
	}

	inline static int32_t get_offset_of_chunkRead_2() { return static_cast<int32_t>(offsetof(MonoChunkStream_t33C2B7ECB208D77D1C94F0A9F48EB5AE27DF5AB5, ___chunkRead_2)); }
	inline int32_t get_chunkRead_2() const { return ___chunkRead_2; }
	inline int32_t* get_address_of_chunkRead_2() { return &___chunkRead_2; }
	inline void set_chunkRead_2(int32_t value)
	{
		___chunkRead_2 = value;
	}

	inline static int32_t get_offset_of_totalWritten_3() { return static_cast<int32_t>(offsetof(MonoChunkStream_t33C2B7ECB208D77D1C94F0A9F48EB5AE27DF5AB5, ___totalWritten_3)); }
	inline int32_t get_totalWritten_3() const { return ___totalWritten_3; }
	inline int32_t* get_address_of_totalWritten_3() { return &___totalWritten_3; }
	inline void set_totalWritten_3(int32_t value)
	{
		___totalWritten_3 = value;
	}

	inline static int32_t get_offset_of_state_4() { return static_cast<int32_t>(offsetof(MonoChunkStream_t33C2B7ECB208D77D1C94F0A9F48EB5AE27DF5AB5, ___state_4)); }
	inline int32_t get_state_4() const { return ___state_4; }
	inline int32_t* get_address_of_state_4() { return &___state_4; }
	inline void set_state_4(int32_t value)
	{
		___state_4 = value;
	}

	inline static int32_t get_offset_of_saved_5() { return static_cast<int32_t>(offsetof(MonoChunkStream_t33C2B7ECB208D77D1C94F0A9F48EB5AE27DF5AB5, ___saved_5)); }
	inline StringBuilder_t * get_saved_5() const { return ___saved_5; }
	inline StringBuilder_t ** get_address_of_saved_5() { return &___saved_5; }
	inline void set_saved_5(StringBuilder_t * value)
	{
		___saved_5 = value;
		Il2CppCodeGenWriteBarrier((&___saved_5), value);
	}

	inline static int32_t get_offset_of_sawCR_6() { return static_cast<int32_t>(offsetof(MonoChunkStream_t33C2B7ECB208D77D1C94F0A9F48EB5AE27DF5AB5, ___sawCR_6)); }
	inline bool get_sawCR_6() const { return ___sawCR_6; }
	inline bool* get_address_of_sawCR_6() { return &___sawCR_6; }
	inline void set_sawCR_6(bool value)
	{
		___sawCR_6 = value;
	}

	inline static int32_t get_offset_of_gotit_7() { return static_cast<int32_t>(offsetof(MonoChunkStream_t33C2B7ECB208D77D1C94F0A9F48EB5AE27DF5AB5, ___gotit_7)); }
	inline bool get_gotit_7() const { return ___gotit_7; }
	inline bool* get_address_of_gotit_7() { return &___gotit_7; }
	inline void set_gotit_7(bool value)
	{
		___gotit_7 = value;
	}

	inline static int32_t get_offset_of_trailerState_8() { return static_cast<int32_t>(offsetof(MonoChunkStream_t33C2B7ECB208D77D1C94F0A9F48EB5AE27DF5AB5, ___trailerState_8)); }
	inline int32_t get_trailerState_8() const { return ___trailerState_8; }
	inline int32_t* get_address_of_trailerState_8() { return &___trailerState_8; }
	inline void set_trailerState_8(int32_t value)
	{
		___trailerState_8 = value;
	}

	inline static int32_t get_offset_of_chunks_9() { return static_cast<int32_t>(offsetof(MonoChunkStream_t33C2B7ECB208D77D1C94F0A9F48EB5AE27DF5AB5, ___chunks_9)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get_chunks_9() const { return ___chunks_9; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of_chunks_9() { return &___chunks_9; }
	inline void set_chunks_9(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		___chunks_9 = value;
		Il2CppCodeGenWriteBarrier((&___chunks_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOCHUNKSTREAM_T33C2B7ECB208D77D1C94F0A9F48EB5AE27DF5AB5_H
#ifndef SERVICEPOINTMANAGER_TB30C5869193569552EC4F0F454EF3ACF3908DC02_H
#define SERVICEPOINTMANAGER_TB30C5869193569552EC4F0F454EF3ACF3908DC02_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.ServicePointManager
struct  ServicePointManager_tB30C5869193569552EC4F0F454EF3ACF3908DC02  : public RuntimeObject
{
public:

public:
};

struct ServicePointManager_tB30C5869193569552EC4F0F454EF3ACF3908DC02_StaticFields
{
public:
	// System.Collections.Specialized.HybridDictionary System.Net.ServicePointManager::servicePoints
	HybridDictionary_t885F953154C575D3408650DCE5D0B76F1EE4E549 * ___servicePoints_0;
	// System.Net.ICertificatePolicy System.Net.ServicePointManager::policy
	RuntimeObject* ___policy_1;
	// System.Int32 System.Net.ServicePointManager::defaultConnectionLimit
	int32_t ___defaultConnectionLimit_2;
	// System.Int32 System.Net.ServicePointManager::maxServicePointIdleTime
	int32_t ___maxServicePointIdleTime_3;
	// System.Int32 System.Net.ServicePointManager::maxServicePoints
	int32_t ___maxServicePoints_4;
	// System.Int32 System.Net.ServicePointManager::dnsRefreshTimeout
	int32_t ___dnsRefreshTimeout_5;
	// System.Boolean System.Net.ServicePointManager::_checkCRL
	bool ____checkCRL_6;
	// System.Net.SecurityProtocolType System.Net.ServicePointManager::_securityProtocol
	int32_t ____securityProtocol_7;
	// System.Boolean System.Net.ServicePointManager::expectContinue
	bool ___expectContinue_8;
	// System.Boolean System.Net.ServicePointManager::useNagle
	bool ___useNagle_9;
	// System.Net.ServerCertValidationCallback System.Net.ServicePointManager::server_cert_cb
	ServerCertValidationCallback_t431E949AECAE20901007813737F5B26311F5F9FB * ___server_cert_cb_10;
	// System.Boolean System.Net.ServicePointManager::tcp_keepalive
	bool ___tcp_keepalive_11;
	// System.Int32 System.Net.ServicePointManager::tcp_keepalive_time
	int32_t ___tcp_keepalive_time_12;
	// System.Int32 System.Net.ServicePointManager::tcp_keepalive_interval
	int32_t ___tcp_keepalive_interval_13;

public:
	inline static int32_t get_offset_of_servicePoints_0() { return static_cast<int32_t>(offsetof(ServicePointManager_tB30C5869193569552EC4F0F454EF3ACF3908DC02_StaticFields, ___servicePoints_0)); }
	inline HybridDictionary_t885F953154C575D3408650DCE5D0B76F1EE4E549 * get_servicePoints_0() const { return ___servicePoints_0; }
	inline HybridDictionary_t885F953154C575D3408650DCE5D0B76F1EE4E549 ** get_address_of_servicePoints_0() { return &___servicePoints_0; }
	inline void set_servicePoints_0(HybridDictionary_t885F953154C575D3408650DCE5D0B76F1EE4E549 * value)
	{
		___servicePoints_0 = value;
		Il2CppCodeGenWriteBarrier((&___servicePoints_0), value);
	}

	inline static int32_t get_offset_of_policy_1() { return static_cast<int32_t>(offsetof(ServicePointManager_tB30C5869193569552EC4F0F454EF3ACF3908DC02_StaticFields, ___policy_1)); }
	inline RuntimeObject* get_policy_1() const { return ___policy_1; }
	inline RuntimeObject** get_address_of_policy_1() { return &___policy_1; }
	inline void set_policy_1(RuntimeObject* value)
	{
		___policy_1 = value;
		Il2CppCodeGenWriteBarrier((&___policy_1), value);
	}

	inline static int32_t get_offset_of_defaultConnectionLimit_2() { return static_cast<int32_t>(offsetof(ServicePointManager_tB30C5869193569552EC4F0F454EF3ACF3908DC02_StaticFields, ___defaultConnectionLimit_2)); }
	inline int32_t get_defaultConnectionLimit_2() const { return ___defaultConnectionLimit_2; }
	inline int32_t* get_address_of_defaultConnectionLimit_2() { return &___defaultConnectionLimit_2; }
	inline void set_defaultConnectionLimit_2(int32_t value)
	{
		___defaultConnectionLimit_2 = value;
	}

	inline static int32_t get_offset_of_maxServicePointIdleTime_3() { return static_cast<int32_t>(offsetof(ServicePointManager_tB30C5869193569552EC4F0F454EF3ACF3908DC02_StaticFields, ___maxServicePointIdleTime_3)); }
	inline int32_t get_maxServicePointIdleTime_3() const { return ___maxServicePointIdleTime_3; }
	inline int32_t* get_address_of_maxServicePointIdleTime_3() { return &___maxServicePointIdleTime_3; }
	inline void set_maxServicePointIdleTime_3(int32_t value)
	{
		___maxServicePointIdleTime_3 = value;
	}

	inline static int32_t get_offset_of_maxServicePoints_4() { return static_cast<int32_t>(offsetof(ServicePointManager_tB30C5869193569552EC4F0F454EF3ACF3908DC02_StaticFields, ___maxServicePoints_4)); }
	inline int32_t get_maxServicePoints_4() const { return ___maxServicePoints_4; }
	inline int32_t* get_address_of_maxServicePoints_4() { return &___maxServicePoints_4; }
	inline void set_maxServicePoints_4(int32_t value)
	{
		___maxServicePoints_4 = value;
	}

	inline static int32_t get_offset_of_dnsRefreshTimeout_5() { return static_cast<int32_t>(offsetof(ServicePointManager_tB30C5869193569552EC4F0F454EF3ACF3908DC02_StaticFields, ___dnsRefreshTimeout_5)); }
	inline int32_t get_dnsRefreshTimeout_5() const { return ___dnsRefreshTimeout_5; }
	inline int32_t* get_address_of_dnsRefreshTimeout_5() { return &___dnsRefreshTimeout_5; }
	inline void set_dnsRefreshTimeout_5(int32_t value)
	{
		___dnsRefreshTimeout_5 = value;
	}

	inline static int32_t get_offset_of__checkCRL_6() { return static_cast<int32_t>(offsetof(ServicePointManager_tB30C5869193569552EC4F0F454EF3ACF3908DC02_StaticFields, ____checkCRL_6)); }
	inline bool get__checkCRL_6() const { return ____checkCRL_6; }
	inline bool* get_address_of__checkCRL_6() { return &____checkCRL_6; }
	inline void set__checkCRL_6(bool value)
	{
		____checkCRL_6 = value;
	}

	inline static int32_t get_offset_of__securityProtocol_7() { return static_cast<int32_t>(offsetof(ServicePointManager_tB30C5869193569552EC4F0F454EF3ACF3908DC02_StaticFields, ____securityProtocol_7)); }
	inline int32_t get__securityProtocol_7() const { return ____securityProtocol_7; }
	inline int32_t* get_address_of__securityProtocol_7() { return &____securityProtocol_7; }
	inline void set__securityProtocol_7(int32_t value)
	{
		____securityProtocol_7 = value;
	}

	inline static int32_t get_offset_of_expectContinue_8() { return static_cast<int32_t>(offsetof(ServicePointManager_tB30C5869193569552EC4F0F454EF3ACF3908DC02_StaticFields, ___expectContinue_8)); }
	inline bool get_expectContinue_8() const { return ___expectContinue_8; }
	inline bool* get_address_of_expectContinue_8() { return &___expectContinue_8; }
	inline void set_expectContinue_8(bool value)
	{
		___expectContinue_8 = value;
	}

	inline static int32_t get_offset_of_useNagle_9() { return static_cast<int32_t>(offsetof(ServicePointManager_tB30C5869193569552EC4F0F454EF3ACF3908DC02_StaticFields, ___useNagle_9)); }
	inline bool get_useNagle_9() const { return ___useNagle_9; }
	inline bool* get_address_of_useNagle_9() { return &___useNagle_9; }
	inline void set_useNagle_9(bool value)
	{
		___useNagle_9 = value;
	}

	inline static int32_t get_offset_of_server_cert_cb_10() { return static_cast<int32_t>(offsetof(ServicePointManager_tB30C5869193569552EC4F0F454EF3ACF3908DC02_StaticFields, ___server_cert_cb_10)); }
	inline ServerCertValidationCallback_t431E949AECAE20901007813737F5B26311F5F9FB * get_server_cert_cb_10() const { return ___server_cert_cb_10; }
	inline ServerCertValidationCallback_t431E949AECAE20901007813737F5B26311F5F9FB ** get_address_of_server_cert_cb_10() { return &___server_cert_cb_10; }
	inline void set_server_cert_cb_10(ServerCertValidationCallback_t431E949AECAE20901007813737F5B26311F5F9FB * value)
	{
		___server_cert_cb_10 = value;
		Il2CppCodeGenWriteBarrier((&___server_cert_cb_10), value);
	}

	inline static int32_t get_offset_of_tcp_keepalive_11() { return static_cast<int32_t>(offsetof(ServicePointManager_tB30C5869193569552EC4F0F454EF3ACF3908DC02_StaticFields, ___tcp_keepalive_11)); }
	inline bool get_tcp_keepalive_11() const { return ___tcp_keepalive_11; }
	inline bool* get_address_of_tcp_keepalive_11() { return &___tcp_keepalive_11; }
	inline void set_tcp_keepalive_11(bool value)
	{
		___tcp_keepalive_11 = value;
	}

	inline static int32_t get_offset_of_tcp_keepalive_time_12() { return static_cast<int32_t>(offsetof(ServicePointManager_tB30C5869193569552EC4F0F454EF3ACF3908DC02_StaticFields, ___tcp_keepalive_time_12)); }
	inline int32_t get_tcp_keepalive_time_12() const { return ___tcp_keepalive_time_12; }
	inline int32_t* get_address_of_tcp_keepalive_time_12() { return &___tcp_keepalive_time_12; }
	inline void set_tcp_keepalive_time_12(int32_t value)
	{
		___tcp_keepalive_time_12 = value;
	}

	inline static int32_t get_offset_of_tcp_keepalive_interval_13() { return static_cast<int32_t>(offsetof(ServicePointManager_tB30C5869193569552EC4F0F454EF3ACF3908DC02_StaticFields, ___tcp_keepalive_interval_13)); }
	inline int32_t get_tcp_keepalive_interval_13() const { return ___tcp_keepalive_interval_13; }
	inline int32_t* get_address_of_tcp_keepalive_interval_13() { return &___tcp_keepalive_interval_13; }
	inline void set_tcp_keepalive_interval_13(int32_t value)
	{
		___tcp_keepalive_interval_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERVICEPOINTMANAGER_TB30C5869193569552EC4F0F454EF3ACF3908DC02_H
#ifndef SOCKET_T47148BFA7740C9C45A69F2F3722F734B9DCA45D8_H
#define SOCKET_T47148BFA7740C9C45A69F2F3722F734B9DCA45D8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Sockets.Socket
struct  Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8  : public RuntimeObject
{
public:
	// System.Boolean System.Net.Sockets.Socket::is_closed
	bool ___is_closed_6;
	// System.Boolean System.Net.Sockets.Socket::is_listening
	bool ___is_listening_7;
	// System.Boolean System.Net.Sockets.Socket::useOverlappedIO
	bool ___useOverlappedIO_8;
	// System.Int32 System.Net.Sockets.Socket::linger_timeout
	int32_t ___linger_timeout_9;
	// System.Net.Sockets.AddressFamily System.Net.Sockets.Socket::addressFamily
	int32_t ___addressFamily_10;
	// System.Net.Sockets.SocketType System.Net.Sockets.Socket::socketType
	int32_t ___socketType_11;
	// System.Net.Sockets.ProtocolType System.Net.Sockets.Socket::protocolType
	int32_t ___protocolType_12;
	// System.Net.Sockets.SafeSocketHandle System.Net.Sockets.Socket::m_Handle
	SafeSocketHandle_t9A33B4DCE2012075A5D6D355D323A05E7F16329A * ___m_Handle_13;
	// System.Net.EndPoint System.Net.Sockets.Socket::seed_endpoint
	EndPoint_tD87FCEF2780A951E8CE8D808C345FBF2C088D980 * ___seed_endpoint_14;
	// System.Threading.SemaphoreSlim System.Net.Sockets.Socket::ReadSem
	SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * ___ReadSem_15;
	// System.Threading.SemaphoreSlim System.Net.Sockets.Socket::WriteSem
	SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * ___WriteSem_16;
	// System.Boolean System.Net.Sockets.Socket::is_blocking
	bool ___is_blocking_17;
	// System.Boolean System.Net.Sockets.Socket::is_bound
	bool ___is_bound_18;
	// System.Boolean System.Net.Sockets.Socket::is_connected
	bool ___is_connected_19;
	// System.Int32 System.Net.Sockets.Socket::m_IntCleanedUp
	int32_t ___m_IntCleanedUp_20;
	// System.Boolean System.Net.Sockets.Socket::connect_in_progress
	bool ___connect_in_progress_21;

public:
	inline static int32_t get_offset_of_is_closed_6() { return static_cast<int32_t>(offsetof(Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8, ___is_closed_6)); }
	inline bool get_is_closed_6() const { return ___is_closed_6; }
	inline bool* get_address_of_is_closed_6() { return &___is_closed_6; }
	inline void set_is_closed_6(bool value)
	{
		___is_closed_6 = value;
	}

	inline static int32_t get_offset_of_is_listening_7() { return static_cast<int32_t>(offsetof(Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8, ___is_listening_7)); }
	inline bool get_is_listening_7() const { return ___is_listening_7; }
	inline bool* get_address_of_is_listening_7() { return &___is_listening_7; }
	inline void set_is_listening_7(bool value)
	{
		___is_listening_7 = value;
	}

	inline static int32_t get_offset_of_useOverlappedIO_8() { return static_cast<int32_t>(offsetof(Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8, ___useOverlappedIO_8)); }
	inline bool get_useOverlappedIO_8() const { return ___useOverlappedIO_8; }
	inline bool* get_address_of_useOverlappedIO_8() { return &___useOverlappedIO_8; }
	inline void set_useOverlappedIO_8(bool value)
	{
		___useOverlappedIO_8 = value;
	}

	inline static int32_t get_offset_of_linger_timeout_9() { return static_cast<int32_t>(offsetof(Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8, ___linger_timeout_9)); }
	inline int32_t get_linger_timeout_9() const { return ___linger_timeout_9; }
	inline int32_t* get_address_of_linger_timeout_9() { return &___linger_timeout_9; }
	inline void set_linger_timeout_9(int32_t value)
	{
		___linger_timeout_9 = value;
	}

	inline static int32_t get_offset_of_addressFamily_10() { return static_cast<int32_t>(offsetof(Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8, ___addressFamily_10)); }
	inline int32_t get_addressFamily_10() const { return ___addressFamily_10; }
	inline int32_t* get_address_of_addressFamily_10() { return &___addressFamily_10; }
	inline void set_addressFamily_10(int32_t value)
	{
		___addressFamily_10 = value;
	}

	inline static int32_t get_offset_of_socketType_11() { return static_cast<int32_t>(offsetof(Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8, ___socketType_11)); }
	inline int32_t get_socketType_11() const { return ___socketType_11; }
	inline int32_t* get_address_of_socketType_11() { return &___socketType_11; }
	inline void set_socketType_11(int32_t value)
	{
		___socketType_11 = value;
	}

	inline static int32_t get_offset_of_protocolType_12() { return static_cast<int32_t>(offsetof(Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8, ___protocolType_12)); }
	inline int32_t get_protocolType_12() const { return ___protocolType_12; }
	inline int32_t* get_address_of_protocolType_12() { return &___protocolType_12; }
	inline void set_protocolType_12(int32_t value)
	{
		___protocolType_12 = value;
	}

	inline static int32_t get_offset_of_m_Handle_13() { return static_cast<int32_t>(offsetof(Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8, ___m_Handle_13)); }
	inline SafeSocketHandle_t9A33B4DCE2012075A5D6D355D323A05E7F16329A * get_m_Handle_13() const { return ___m_Handle_13; }
	inline SafeSocketHandle_t9A33B4DCE2012075A5D6D355D323A05E7F16329A ** get_address_of_m_Handle_13() { return &___m_Handle_13; }
	inline void set_m_Handle_13(SafeSocketHandle_t9A33B4DCE2012075A5D6D355D323A05E7F16329A * value)
	{
		___m_Handle_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_Handle_13), value);
	}

	inline static int32_t get_offset_of_seed_endpoint_14() { return static_cast<int32_t>(offsetof(Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8, ___seed_endpoint_14)); }
	inline EndPoint_tD87FCEF2780A951E8CE8D808C345FBF2C088D980 * get_seed_endpoint_14() const { return ___seed_endpoint_14; }
	inline EndPoint_tD87FCEF2780A951E8CE8D808C345FBF2C088D980 ** get_address_of_seed_endpoint_14() { return &___seed_endpoint_14; }
	inline void set_seed_endpoint_14(EndPoint_tD87FCEF2780A951E8CE8D808C345FBF2C088D980 * value)
	{
		___seed_endpoint_14 = value;
		Il2CppCodeGenWriteBarrier((&___seed_endpoint_14), value);
	}

	inline static int32_t get_offset_of_ReadSem_15() { return static_cast<int32_t>(offsetof(Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8, ___ReadSem_15)); }
	inline SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * get_ReadSem_15() const { return ___ReadSem_15; }
	inline SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 ** get_address_of_ReadSem_15() { return &___ReadSem_15; }
	inline void set_ReadSem_15(SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * value)
	{
		___ReadSem_15 = value;
		Il2CppCodeGenWriteBarrier((&___ReadSem_15), value);
	}

	inline static int32_t get_offset_of_WriteSem_16() { return static_cast<int32_t>(offsetof(Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8, ___WriteSem_16)); }
	inline SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * get_WriteSem_16() const { return ___WriteSem_16; }
	inline SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 ** get_address_of_WriteSem_16() { return &___WriteSem_16; }
	inline void set_WriteSem_16(SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * value)
	{
		___WriteSem_16 = value;
		Il2CppCodeGenWriteBarrier((&___WriteSem_16), value);
	}

	inline static int32_t get_offset_of_is_blocking_17() { return static_cast<int32_t>(offsetof(Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8, ___is_blocking_17)); }
	inline bool get_is_blocking_17() const { return ___is_blocking_17; }
	inline bool* get_address_of_is_blocking_17() { return &___is_blocking_17; }
	inline void set_is_blocking_17(bool value)
	{
		___is_blocking_17 = value;
	}

	inline static int32_t get_offset_of_is_bound_18() { return static_cast<int32_t>(offsetof(Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8, ___is_bound_18)); }
	inline bool get_is_bound_18() const { return ___is_bound_18; }
	inline bool* get_address_of_is_bound_18() { return &___is_bound_18; }
	inline void set_is_bound_18(bool value)
	{
		___is_bound_18 = value;
	}

	inline static int32_t get_offset_of_is_connected_19() { return static_cast<int32_t>(offsetof(Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8, ___is_connected_19)); }
	inline bool get_is_connected_19() const { return ___is_connected_19; }
	inline bool* get_address_of_is_connected_19() { return &___is_connected_19; }
	inline void set_is_connected_19(bool value)
	{
		___is_connected_19 = value;
	}

	inline static int32_t get_offset_of_m_IntCleanedUp_20() { return static_cast<int32_t>(offsetof(Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8, ___m_IntCleanedUp_20)); }
	inline int32_t get_m_IntCleanedUp_20() const { return ___m_IntCleanedUp_20; }
	inline int32_t* get_address_of_m_IntCleanedUp_20() { return &___m_IntCleanedUp_20; }
	inline void set_m_IntCleanedUp_20(int32_t value)
	{
		___m_IntCleanedUp_20 = value;
	}

	inline static int32_t get_offset_of_connect_in_progress_21() { return static_cast<int32_t>(offsetof(Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8, ___connect_in_progress_21)); }
	inline bool get_connect_in_progress_21() const { return ___connect_in_progress_21; }
	inline bool* get_address_of_connect_in_progress_21() { return &___connect_in_progress_21; }
	inline void set_connect_in_progress_21(bool value)
	{
		___connect_in_progress_21 = value;
	}
};

struct Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8_StaticFields
{
public:
	// System.Object System.Net.Sockets.Socket::s_InternalSyncObject
	RuntimeObject * ___s_InternalSyncObject_0;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.Net.Sockets.Socket::s_SupportsIPv4
	bool ___s_SupportsIPv4_1;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.Net.Sockets.Socket::s_SupportsIPv6
	bool ___s_SupportsIPv6_2;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.Net.Sockets.Socket::s_OSSupportsIPv6
	bool ___s_OSSupportsIPv6_3;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.Net.Sockets.Socket::s_Initialized
	bool ___s_Initialized_4;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.Net.Sockets.Socket::s_LoggingEnabled
	bool ___s_LoggingEnabled_5;
	// System.AsyncCallback System.Net.Sockets.Socket::AcceptAsyncCallback
	AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___AcceptAsyncCallback_22;
	// System.IOAsyncCallback System.Net.Sockets.Socket::BeginAcceptCallback
	IOAsyncCallback_tEAEB626A6DAC959F4C365B12136A80EE9AA17547 * ___BeginAcceptCallback_23;
	// System.IOAsyncCallback System.Net.Sockets.Socket::BeginAcceptReceiveCallback
	IOAsyncCallback_tEAEB626A6DAC959F4C365B12136A80EE9AA17547 * ___BeginAcceptReceiveCallback_24;
	// System.AsyncCallback System.Net.Sockets.Socket::ConnectAsyncCallback
	AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___ConnectAsyncCallback_25;
	// System.IOAsyncCallback System.Net.Sockets.Socket::BeginConnectCallback
	IOAsyncCallback_tEAEB626A6DAC959F4C365B12136A80EE9AA17547 * ___BeginConnectCallback_26;
	// System.AsyncCallback System.Net.Sockets.Socket::DisconnectAsyncCallback
	AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___DisconnectAsyncCallback_27;
	// System.IOAsyncCallback System.Net.Sockets.Socket::BeginDisconnectCallback
	IOAsyncCallback_tEAEB626A6DAC959F4C365B12136A80EE9AA17547 * ___BeginDisconnectCallback_28;
	// System.AsyncCallback System.Net.Sockets.Socket::ReceiveAsyncCallback
	AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___ReceiveAsyncCallback_29;
	// System.IOAsyncCallback System.Net.Sockets.Socket::BeginReceiveCallback
	IOAsyncCallback_tEAEB626A6DAC959F4C365B12136A80EE9AA17547 * ___BeginReceiveCallback_30;
	// System.IOAsyncCallback System.Net.Sockets.Socket::BeginReceiveGenericCallback
	IOAsyncCallback_tEAEB626A6DAC959F4C365B12136A80EE9AA17547 * ___BeginReceiveGenericCallback_31;
	// System.AsyncCallback System.Net.Sockets.Socket::ReceiveFromAsyncCallback
	AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___ReceiveFromAsyncCallback_32;
	// System.IOAsyncCallback System.Net.Sockets.Socket::BeginReceiveFromCallback
	IOAsyncCallback_tEAEB626A6DAC959F4C365B12136A80EE9AA17547 * ___BeginReceiveFromCallback_33;
	// System.AsyncCallback System.Net.Sockets.Socket::SendAsyncCallback
	AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___SendAsyncCallback_34;
	// System.IOAsyncCallback System.Net.Sockets.Socket::BeginSendGenericCallback
	IOAsyncCallback_tEAEB626A6DAC959F4C365B12136A80EE9AA17547 * ___BeginSendGenericCallback_35;
	// System.AsyncCallback System.Net.Sockets.Socket::SendToAsyncCallback
	AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___SendToAsyncCallback_36;

public:
	inline static int32_t get_offset_of_s_InternalSyncObject_0() { return static_cast<int32_t>(offsetof(Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8_StaticFields, ___s_InternalSyncObject_0)); }
	inline RuntimeObject * get_s_InternalSyncObject_0() const { return ___s_InternalSyncObject_0; }
	inline RuntimeObject ** get_address_of_s_InternalSyncObject_0() { return &___s_InternalSyncObject_0; }
	inline void set_s_InternalSyncObject_0(RuntimeObject * value)
	{
		___s_InternalSyncObject_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_InternalSyncObject_0), value);
	}

	inline static int32_t get_offset_of_s_SupportsIPv4_1() { return static_cast<int32_t>(offsetof(Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8_StaticFields, ___s_SupportsIPv4_1)); }
	inline bool get_s_SupportsIPv4_1() const { return ___s_SupportsIPv4_1; }
	inline bool* get_address_of_s_SupportsIPv4_1() { return &___s_SupportsIPv4_1; }
	inline void set_s_SupportsIPv4_1(bool value)
	{
		___s_SupportsIPv4_1 = value;
	}

	inline static int32_t get_offset_of_s_SupportsIPv6_2() { return static_cast<int32_t>(offsetof(Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8_StaticFields, ___s_SupportsIPv6_2)); }
	inline bool get_s_SupportsIPv6_2() const { return ___s_SupportsIPv6_2; }
	inline bool* get_address_of_s_SupportsIPv6_2() { return &___s_SupportsIPv6_2; }
	inline void set_s_SupportsIPv6_2(bool value)
	{
		___s_SupportsIPv6_2 = value;
	}

	inline static int32_t get_offset_of_s_OSSupportsIPv6_3() { return static_cast<int32_t>(offsetof(Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8_StaticFields, ___s_OSSupportsIPv6_3)); }
	inline bool get_s_OSSupportsIPv6_3() const { return ___s_OSSupportsIPv6_3; }
	inline bool* get_address_of_s_OSSupportsIPv6_3() { return &___s_OSSupportsIPv6_3; }
	inline void set_s_OSSupportsIPv6_3(bool value)
	{
		___s_OSSupportsIPv6_3 = value;
	}

	inline static int32_t get_offset_of_s_Initialized_4() { return static_cast<int32_t>(offsetof(Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8_StaticFields, ___s_Initialized_4)); }
	inline bool get_s_Initialized_4() const { return ___s_Initialized_4; }
	inline bool* get_address_of_s_Initialized_4() { return &___s_Initialized_4; }
	inline void set_s_Initialized_4(bool value)
	{
		___s_Initialized_4 = value;
	}

	inline static int32_t get_offset_of_s_LoggingEnabled_5() { return static_cast<int32_t>(offsetof(Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8_StaticFields, ___s_LoggingEnabled_5)); }
	inline bool get_s_LoggingEnabled_5() const { return ___s_LoggingEnabled_5; }
	inline bool* get_address_of_s_LoggingEnabled_5() { return &___s_LoggingEnabled_5; }
	inline void set_s_LoggingEnabled_5(bool value)
	{
		___s_LoggingEnabled_5 = value;
	}

	inline static int32_t get_offset_of_AcceptAsyncCallback_22() { return static_cast<int32_t>(offsetof(Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8_StaticFields, ___AcceptAsyncCallback_22)); }
	inline AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * get_AcceptAsyncCallback_22() const { return ___AcceptAsyncCallback_22; }
	inline AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 ** get_address_of_AcceptAsyncCallback_22() { return &___AcceptAsyncCallback_22; }
	inline void set_AcceptAsyncCallback_22(AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * value)
	{
		___AcceptAsyncCallback_22 = value;
		Il2CppCodeGenWriteBarrier((&___AcceptAsyncCallback_22), value);
	}

	inline static int32_t get_offset_of_BeginAcceptCallback_23() { return static_cast<int32_t>(offsetof(Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8_StaticFields, ___BeginAcceptCallback_23)); }
	inline IOAsyncCallback_tEAEB626A6DAC959F4C365B12136A80EE9AA17547 * get_BeginAcceptCallback_23() const { return ___BeginAcceptCallback_23; }
	inline IOAsyncCallback_tEAEB626A6DAC959F4C365B12136A80EE9AA17547 ** get_address_of_BeginAcceptCallback_23() { return &___BeginAcceptCallback_23; }
	inline void set_BeginAcceptCallback_23(IOAsyncCallback_tEAEB626A6DAC959F4C365B12136A80EE9AA17547 * value)
	{
		___BeginAcceptCallback_23 = value;
		Il2CppCodeGenWriteBarrier((&___BeginAcceptCallback_23), value);
	}

	inline static int32_t get_offset_of_BeginAcceptReceiveCallback_24() { return static_cast<int32_t>(offsetof(Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8_StaticFields, ___BeginAcceptReceiveCallback_24)); }
	inline IOAsyncCallback_tEAEB626A6DAC959F4C365B12136A80EE9AA17547 * get_BeginAcceptReceiveCallback_24() const { return ___BeginAcceptReceiveCallback_24; }
	inline IOAsyncCallback_tEAEB626A6DAC959F4C365B12136A80EE9AA17547 ** get_address_of_BeginAcceptReceiveCallback_24() { return &___BeginAcceptReceiveCallback_24; }
	inline void set_BeginAcceptReceiveCallback_24(IOAsyncCallback_tEAEB626A6DAC959F4C365B12136A80EE9AA17547 * value)
	{
		___BeginAcceptReceiveCallback_24 = value;
		Il2CppCodeGenWriteBarrier((&___BeginAcceptReceiveCallback_24), value);
	}

	inline static int32_t get_offset_of_ConnectAsyncCallback_25() { return static_cast<int32_t>(offsetof(Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8_StaticFields, ___ConnectAsyncCallback_25)); }
	inline AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * get_ConnectAsyncCallback_25() const { return ___ConnectAsyncCallback_25; }
	inline AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 ** get_address_of_ConnectAsyncCallback_25() { return &___ConnectAsyncCallback_25; }
	inline void set_ConnectAsyncCallback_25(AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * value)
	{
		___ConnectAsyncCallback_25 = value;
		Il2CppCodeGenWriteBarrier((&___ConnectAsyncCallback_25), value);
	}

	inline static int32_t get_offset_of_BeginConnectCallback_26() { return static_cast<int32_t>(offsetof(Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8_StaticFields, ___BeginConnectCallback_26)); }
	inline IOAsyncCallback_tEAEB626A6DAC959F4C365B12136A80EE9AA17547 * get_BeginConnectCallback_26() const { return ___BeginConnectCallback_26; }
	inline IOAsyncCallback_tEAEB626A6DAC959F4C365B12136A80EE9AA17547 ** get_address_of_BeginConnectCallback_26() { return &___BeginConnectCallback_26; }
	inline void set_BeginConnectCallback_26(IOAsyncCallback_tEAEB626A6DAC959F4C365B12136A80EE9AA17547 * value)
	{
		___BeginConnectCallback_26 = value;
		Il2CppCodeGenWriteBarrier((&___BeginConnectCallback_26), value);
	}

	inline static int32_t get_offset_of_DisconnectAsyncCallback_27() { return static_cast<int32_t>(offsetof(Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8_StaticFields, ___DisconnectAsyncCallback_27)); }
	inline AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * get_DisconnectAsyncCallback_27() const { return ___DisconnectAsyncCallback_27; }
	inline AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 ** get_address_of_DisconnectAsyncCallback_27() { return &___DisconnectAsyncCallback_27; }
	inline void set_DisconnectAsyncCallback_27(AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * value)
	{
		___DisconnectAsyncCallback_27 = value;
		Il2CppCodeGenWriteBarrier((&___DisconnectAsyncCallback_27), value);
	}

	inline static int32_t get_offset_of_BeginDisconnectCallback_28() { return static_cast<int32_t>(offsetof(Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8_StaticFields, ___BeginDisconnectCallback_28)); }
	inline IOAsyncCallback_tEAEB626A6DAC959F4C365B12136A80EE9AA17547 * get_BeginDisconnectCallback_28() const { return ___BeginDisconnectCallback_28; }
	inline IOAsyncCallback_tEAEB626A6DAC959F4C365B12136A80EE9AA17547 ** get_address_of_BeginDisconnectCallback_28() { return &___BeginDisconnectCallback_28; }
	inline void set_BeginDisconnectCallback_28(IOAsyncCallback_tEAEB626A6DAC959F4C365B12136A80EE9AA17547 * value)
	{
		___BeginDisconnectCallback_28 = value;
		Il2CppCodeGenWriteBarrier((&___BeginDisconnectCallback_28), value);
	}

	inline static int32_t get_offset_of_ReceiveAsyncCallback_29() { return static_cast<int32_t>(offsetof(Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8_StaticFields, ___ReceiveAsyncCallback_29)); }
	inline AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * get_ReceiveAsyncCallback_29() const { return ___ReceiveAsyncCallback_29; }
	inline AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 ** get_address_of_ReceiveAsyncCallback_29() { return &___ReceiveAsyncCallback_29; }
	inline void set_ReceiveAsyncCallback_29(AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * value)
	{
		___ReceiveAsyncCallback_29 = value;
		Il2CppCodeGenWriteBarrier((&___ReceiveAsyncCallback_29), value);
	}

	inline static int32_t get_offset_of_BeginReceiveCallback_30() { return static_cast<int32_t>(offsetof(Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8_StaticFields, ___BeginReceiveCallback_30)); }
	inline IOAsyncCallback_tEAEB626A6DAC959F4C365B12136A80EE9AA17547 * get_BeginReceiveCallback_30() const { return ___BeginReceiveCallback_30; }
	inline IOAsyncCallback_tEAEB626A6DAC959F4C365B12136A80EE9AA17547 ** get_address_of_BeginReceiveCallback_30() { return &___BeginReceiveCallback_30; }
	inline void set_BeginReceiveCallback_30(IOAsyncCallback_tEAEB626A6DAC959F4C365B12136A80EE9AA17547 * value)
	{
		___BeginReceiveCallback_30 = value;
		Il2CppCodeGenWriteBarrier((&___BeginReceiveCallback_30), value);
	}

	inline static int32_t get_offset_of_BeginReceiveGenericCallback_31() { return static_cast<int32_t>(offsetof(Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8_StaticFields, ___BeginReceiveGenericCallback_31)); }
	inline IOAsyncCallback_tEAEB626A6DAC959F4C365B12136A80EE9AA17547 * get_BeginReceiveGenericCallback_31() const { return ___BeginReceiveGenericCallback_31; }
	inline IOAsyncCallback_tEAEB626A6DAC959F4C365B12136A80EE9AA17547 ** get_address_of_BeginReceiveGenericCallback_31() { return &___BeginReceiveGenericCallback_31; }
	inline void set_BeginReceiveGenericCallback_31(IOAsyncCallback_tEAEB626A6DAC959F4C365B12136A80EE9AA17547 * value)
	{
		___BeginReceiveGenericCallback_31 = value;
		Il2CppCodeGenWriteBarrier((&___BeginReceiveGenericCallback_31), value);
	}

	inline static int32_t get_offset_of_ReceiveFromAsyncCallback_32() { return static_cast<int32_t>(offsetof(Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8_StaticFields, ___ReceiveFromAsyncCallback_32)); }
	inline AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * get_ReceiveFromAsyncCallback_32() const { return ___ReceiveFromAsyncCallback_32; }
	inline AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 ** get_address_of_ReceiveFromAsyncCallback_32() { return &___ReceiveFromAsyncCallback_32; }
	inline void set_ReceiveFromAsyncCallback_32(AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * value)
	{
		___ReceiveFromAsyncCallback_32 = value;
		Il2CppCodeGenWriteBarrier((&___ReceiveFromAsyncCallback_32), value);
	}

	inline static int32_t get_offset_of_BeginReceiveFromCallback_33() { return static_cast<int32_t>(offsetof(Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8_StaticFields, ___BeginReceiveFromCallback_33)); }
	inline IOAsyncCallback_tEAEB626A6DAC959F4C365B12136A80EE9AA17547 * get_BeginReceiveFromCallback_33() const { return ___BeginReceiveFromCallback_33; }
	inline IOAsyncCallback_tEAEB626A6DAC959F4C365B12136A80EE9AA17547 ** get_address_of_BeginReceiveFromCallback_33() { return &___BeginReceiveFromCallback_33; }
	inline void set_BeginReceiveFromCallback_33(IOAsyncCallback_tEAEB626A6DAC959F4C365B12136A80EE9AA17547 * value)
	{
		___BeginReceiveFromCallback_33 = value;
		Il2CppCodeGenWriteBarrier((&___BeginReceiveFromCallback_33), value);
	}

	inline static int32_t get_offset_of_SendAsyncCallback_34() { return static_cast<int32_t>(offsetof(Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8_StaticFields, ___SendAsyncCallback_34)); }
	inline AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * get_SendAsyncCallback_34() const { return ___SendAsyncCallback_34; }
	inline AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 ** get_address_of_SendAsyncCallback_34() { return &___SendAsyncCallback_34; }
	inline void set_SendAsyncCallback_34(AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * value)
	{
		___SendAsyncCallback_34 = value;
		Il2CppCodeGenWriteBarrier((&___SendAsyncCallback_34), value);
	}

	inline static int32_t get_offset_of_BeginSendGenericCallback_35() { return static_cast<int32_t>(offsetof(Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8_StaticFields, ___BeginSendGenericCallback_35)); }
	inline IOAsyncCallback_tEAEB626A6DAC959F4C365B12136A80EE9AA17547 * get_BeginSendGenericCallback_35() const { return ___BeginSendGenericCallback_35; }
	inline IOAsyncCallback_tEAEB626A6DAC959F4C365B12136A80EE9AA17547 ** get_address_of_BeginSendGenericCallback_35() { return &___BeginSendGenericCallback_35; }
	inline void set_BeginSendGenericCallback_35(IOAsyncCallback_tEAEB626A6DAC959F4C365B12136A80EE9AA17547 * value)
	{
		___BeginSendGenericCallback_35 = value;
		Il2CppCodeGenWriteBarrier((&___BeginSendGenericCallback_35), value);
	}

	inline static int32_t get_offset_of_SendToAsyncCallback_36() { return static_cast<int32_t>(offsetof(Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8_StaticFields, ___SendToAsyncCallback_36)); }
	inline AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * get_SendToAsyncCallback_36() const { return ___SendToAsyncCallback_36; }
	inline AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 ** get_address_of_SendToAsyncCallback_36() { return &___SendToAsyncCallback_36; }
	inline void set_SendToAsyncCallback_36(AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * value)
	{
		___SendToAsyncCallback_36 = value;
		Il2CppCodeGenWriteBarrier((&___SendToAsyncCallback_36), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOCKET_T47148BFA7740C9C45A69F2F3722F734B9DCA45D8_H
#ifndef SOCKETASYNCEVENTARGS_T5E05ABA36B4740D22D5E746DC4E7763AA448CAA7_H
#define SOCKETASYNCEVENTARGS_T5E05ABA36B4740D22D5E746DC4E7763AA448CAA7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Sockets.SocketAsyncEventArgs
struct  SocketAsyncEventArgs_t5E05ABA36B4740D22D5E746DC4E7763AA448CAA7  : public EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E
{
public:
	// System.Boolean System.Net.Sockets.SocketAsyncEventArgs::disposed
	bool ___disposed_1;
	// System.Int32 modreq(System.Runtime.CompilerServices.IsVolatile) System.Net.Sockets.SocketAsyncEventArgs::in_progress
	int32_t ___in_progress_2;
	// System.Net.EndPoint System.Net.Sockets.SocketAsyncEventArgs::remote_ep
	EndPoint_tD87FCEF2780A951E8CE8D808C345FBF2C088D980 * ___remote_ep_3;
	// System.Net.Sockets.Socket System.Net.Sockets.SocketAsyncEventArgs::current_socket
	Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 * ___current_socket_4;
	// System.Net.Sockets.SocketAsyncResult System.Net.Sockets.SocketAsyncEventArgs::socket_async_result
	SocketAsyncResult_t63145D172556590482549D41328C0668E19CB69C * ___socket_async_result_5;
	// System.Exception System.Net.Sockets.SocketAsyncEventArgs::<ConnectByNameError>k__BackingField
	Exception_t * ___U3CConnectByNameErrorU3Ek__BackingField_6;
	// System.Net.Sockets.Socket System.Net.Sockets.SocketAsyncEventArgs::<AcceptSocket>k__BackingField
	Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 * ___U3CAcceptSocketU3Ek__BackingField_7;
	// System.Byte[] System.Net.Sockets.SocketAsyncEventArgs::<Buffer>k__BackingField
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___U3CBufferU3Ek__BackingField_8;
	// System.Collections.Generic.IList`1<System.ArraySegment`1<System.Byte>> System.Net.Sockets.SocketAsyncEventArgs::m_BufferList
	RuntimeObject* ___m_BufferList_9;
	// System.Int32 System.Net.Sockets.SocketAsyncEventArgs::<BytesTransferred>k__BackingField
	int32_t ___U3CBytesTransferredU3Ek__BackingField_10;
	// System.Int32 System.Net.Sockets.SocketAsyncEventArgs::<Count>k__BackingField
	int32_t ___U3CCountU3Ek__BackingField_11;
	// System.Net.Sockets.SocketAsyncOperation System.Net.Sockets.SocketAsyncEventArgs::<LastOperation>k__BackingField
	int32_t ___U3CLastOperationU3Ek__BackingField_12;
	// System.Int32 System.Net.Sockets.SocketAsyncEventArgs::<Offset>k__BackingField
	int32_t ___U3COffsetU3Ek__BackingField_13;
	// System.Int32 System.Net.Sockets.SocketAsyncEventArgs::<SendPacketsSendSize>k__BackingField
	int32_t ___U3CSendPacketsSendSizeU3Ek__BackingField_14;
	// System.Net.Sockets.SocketError System.Net.Sockets.SocketAsyncEventArgs::<SocketError>k__BackingField
	int32_t ___U3CSocketErrorU3Ek__BackingField_15;
	// System.Object System.Net.Sockets.SocketAsyncEventArgs::<UserToken>k__BackingField
	RuntimeObject * ___U3CUserTokenU3Ek__BackingField_16;
	// System.EventHandler`1<System.Net.Sockets.SocketAsyncEventArgs> System.Net.Sockets.SocketAsyncEventArgs::Completed
	EventHandler_1_tFBF7D1676A66F950822630E5E6DA5D6FFEAF7A34 * ___Completed_17;

public:
	inline static int32_t get_offset_of_disposed_1() { return static_cast<int32_t>(offsetof(SocketAsyncEventArgs_t5E05ABA36B4740D22D5E746DC4E7763AA448CAA7, ___disposed_1)); }
	inline bool get_disposed_1() const { return ___disposed_1; }
	inline bool* get_address_of_disposed_1() { return &___disposed_1; }
	inline void set_disposed_1(bool value)
	{
		___disposed_1 = value;
	}

	inline static int32_t get_offset_of_in_progress_2() { return static_cast<int32_t>(offsetof(SocketAsyncEventArgs_t5E05ABA36B4740D22D5E746DC4E7763AA448CAA7, ___in_progress_2)); }
	inline int32_t get_in_progress_2() const { return ___in_progress_2; }
	inline int32_t* get_address_of_in_progress_2() { return &___in_progress_2; }
	inline void set_in_progress_2(int32_t value)
	{
		___in_progress_2 = value;
	}

	inline static int32_t get_offset_of_remote_ep_3() { return static_cast<int32_t>(offsetof(SocketAsyncEventArgs_t5E05ABA36B4740D22D5E746DC4E7763AA448CAA7, ___remote_ep_3)); }
	inline EndPoint_tD87FCEF2780A951E8CE8D808C345FBF2C088D980 * get_remote_ep_3() const { return ___remote_ep_3; }
	inline EndPoint_tD87FCEF2780A951E8CE8D808C345FBF2C088D980 ** get_address_of_remote_ep_3() { return &___remote_ep_3; }
	inline void set_remote_ep_3(EndPoint_tD87FCEF2780A951E8CE8D808C345FBF2C088D980 * value)
	{
		___remote_ep_3 = value;
		Il2CppCodeGenWriteBarrier((&___remote_ep_3), value);
	}

	inline static int32_t get_offset_of_current_socket_4() { return static_cast<int32_t>(offsetof(SocketAsyncEventArgs_t5E05ABA36B4740D22D5E746DC4E7763AA448CAA7, ___current_socket_4)); }
	inline Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 * get_current_socket_4() const { return ___current_socket_4; }
	inline Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 ** get_address_of_current_socket_4() { return &___current_socket_4; }
	inline void set_current_socket_4(Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 * value)
	{
		___current_socket_4 = value;
		Il2CppCodeGenWriteBarrier((&___current_socket_4), value);
	}

	inline static int32_t get_offset_of_socket_async_result_5() { return static_cast<int32_t>(offsetof(SocketAsyncEventArgs_t5E05ABA36B4740D22D5E746DC4E7763AA448CAA7, ___socket_async_result_5)); }
	inline SocketAsyncResult_t63145D172556590482549D41328C0668E19CB69C * get_socket_async_result_5() const { return ___socket_async_result_5; }
	inline SocketAsyncResult_t63145D172556590482549D41328C0668E19CB69C ** get_address_of_socket_async_result_5() { return &___socket_async_result_5; }
	inline void set_socket_async_result_5(SocketAsyncResult_t63145D172556590482549D41328C0668E19CB69C * value)
	{
		___socket_async_result_5 = value;
		Il2CppCodeGenWriteBarrier((&___socket_async_result_5), value);
	}

	inline static int32_t get_offset_of_U3CConnectByNameErrorU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(SocketAsyncEventArgs_t5E05ABA36B4740D22D5E746DC4E7763AA448CAA7, ___U3CConnectByNameErrorU3Ek__BackingField_6)); }
	inline Exception_t * get_U3CConnectByNameErrorU3Ek__BackingField_6() const { return ___U3CConnectByNameErrorU3Ek__BackingField_6; }
	inline Exception_t ** get_address_of_U3CConnectByNameErrorU3Ek__BackingField_6() { return &___U3CConnectByNameErrorU3Ek__BackingField_6; }
	inline void set_U3CConnectByNameErrorU3Ek__BackingField_6(Exception_t * value)
	{
		___U3CConnectByNameErrorU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CConnectByNameErrorU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CAcceptSocketU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(SocketAsyncEventArgs_t5E05ABA36B4740D22D5E746DC4E7763AA448CAA7, ___U3CAcceptSocketU3Ek__BackingField_7)); }
	inline Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 * get_U3CAcceptSocketU3Ek__BackingField_7() const { return ___U3CAcceptSocketU3Ek__BackingField_7; }
	inline Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 ** get_address_of_U3CAcceptSocketU3Ek__BackingField_7() { return &___U3CAcceptSocketU3Ek__BackingField_7; }
	inline void set_U3CAcceptSocketU3Ek__BackingField_7(Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 * value)
	{
		___U3CAcceptSocketU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAcceptSocketU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CBufferU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(SocketAsyncEventArgs_t5E05ABA36B4740D22D5E746DC4E7763AA448CAA7, ___U3CBufferU3Ek__BackingField_8)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_U3CBufferU3Ek__BackingField_8() const { return ___U3CBufferU3Ek__BackingField_8; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_U3CBufferU3Ek__BackingField_8() { return &___U3CBufferU3Ek__BackingField_8; }
	inline void set_U3CBufferU3Ek__BackingField_8(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___U3CBufferU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBufferU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_m_BufferList_9() { return static_cast<int32_t>(offsetof(SocketAsyncEventArgs_t5E05ABA36B4740D22D5E746DC4E7763AA448CAA7, ___m_BufferList_9)); }
	inline RuntimeObject* get_m_BufferList_9() const { return ___m_BufferList_9; }
	inline RuntimeObject** get_address_of_m_BufferList_9() { return &___m_BufferList_9; }
	inline void set_m_BufferList_9(RuntimeObject* value)
	{
		___m_BufferList_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_BufferList_9), value);
	}

	inline static int32_t get_offset_of_U3CBytesTransferredU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(SocketAsyncEventArgs_t5E05ABA36B4740D22D5E746DC4E7763AA448CAA7, ___U3CBytesTransferredU3Ek__BackingField_10)); }
	inline int32_t get_U3CBytesTransferredU3Ek__BackingField_10() const { return ___U3CBytesTransferredU3Ek__BackingField_10; }
	inline int32_t* get_address_of_U3CBytesTransferredU3Ek__BackingField_10() { return &___U3CBytesTransferredU3Ek__BackingField_10; }
	inline void set_U3CBytesTransferredU3Ek__BackingField_10(int32_t value)
	{
		___U3CBytesTransferredU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CCountU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(SocketAsyncEventArgs_t5E05ABA36B4740D22D5E746DC4E7763AA448CAA7, ___U3CCountU3Ek__BackingField_11)); }
	inline int32_t get_U3CCountU3Ek__BackingField_11() const { return ___U3CCountU3Ek__BackingField_11; }
	inline int32_t* get_address_of_U3CCountU3Ek__BackingField_11() { return &___U3CCountU3Ek__BackingField_11; }
	inline void set_U3CCountU3Ek__BackingField_11(int32_t value)
	{
		___U3CCountU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CLastOperationU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(SocketAsyncEventArgs_t5E05ABA36B4740D22D5E746DC4E7763AA448CAA7, ___U3CLastOperationU3Ek__BackingField_12)); }
	inline int32_t get_U3CLastOperationU3Ek__BackingField_12() const { return ___U3CLastOperationU3Ek__BackingField_12; }
	inline int32_t* get_address_of_U3CLastOperationU3Ek__BackingField_12() { return &___U3CLastOperationU3Ek__BackingField_12; }
	inline void set_U3CLastOperationU3Ek__BackingField_12(int32_t value)
	{
		___U3CLastOperationU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3COffsetU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(SocketAsyncEventArgs_t5E05ABA36B4740D22D5E746DC4E7763AA448CAA7, ___U3COffsetU3Ek__BackingField_13)); }
	inline int32_t get_U3COffsetU3Ek__BackingField_13() const { return ___U3COffsetU3Ek__BackingField_13; }
	inline int32_t* get_address_of_U3COffsetU3Ek__BackingField_13() { return &___U3COffsetU3Ek__BackingField_13; }
	inline void set_U3COffsetU3Ek__BackingField_13(int32_t value)
	{
		___U3COffsetU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3CSendPacketsSendSizeU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(SocketAsyncEventArgs_t5E05ABA36B4740D22D5E746DC4E7763AA448CAA7, ___U3CSendPacketsSendSizeU3Ek__BackingField_14)); }
	inline int32_t get_U3CSendPacketsSendSizeU3Ek__BackingField_14() const { return ___U3CSendPacketsSendSizeU3Ek__BackingField_14; }
	inline int32_t* get_address_of_U3CSendPacketsSendSizeU3Ek__BackingField_14() { return &___U3CSendPacketsSendSizeU3Ek__BackingField_14; }
	inline void set_U3CSendPacketsSendSizeU3Ek__BackingField_14(int32_t value)
	{
		___U3CSendPacketsSendSizeU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CSocketErrorU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(SocketAsyncEventArgs_t5E05ABA36B4740D22D5E746DC4E7763AA448CAA7, ___U3CSocketErrorU3Ek__BackingField_15)); }
	inline int32_t get_U3CSocketErrorU3Ek__BackingField_15() const { return ___U3CSocketErrorU3Ek__BackingField_15; }
	inline int32_t* get_address_of_U3CSocketErrorU3Ek__BackingField_15() { return &___U3CSocketErrorU3Ek__BackingField_15; }
	inline void set_U3CSocketErrorU3Ek__BackingField_15(int32_t value)
	{
		___U3CSocketErrorU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3CUserTokenU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(SocketAsyncEventArgs_t5E05ABA36B4740D22D5E746DC4E7763AA448CAA7, ___U3CUserTokenU3Ek__BackingField_16)); }
	inline RuntimeObject * get_U3CUserTokenU3Ek__BackingField_16() const { return ___U3CUserTokenU3Ek__BackingField_16; }
	inline RuntimeObject ** get_address_of_U3CUserTokenU3Ek__BackingField_16() { return &___U3CUserTokenU3Ek__BackingField_16; }
	inline void set_U3CUserTokenU3Ek__BackingField_16(RuntimeObject * value)
	{
		___U3CUserTokenU3Ek__BackingField_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUserTokenU3Ek__BackingField_16), value);
	}

	inline static int32_t get_offset_of_Completed_17() { return static_cast<int32_t>(offsetof(SocketAsyncEventArgs_t5E05ABA36B4740D22D5E746DC4E7763AA448CAA7, ___Completed_17)); }
	inline EventHandler_1_tFBF7D1676A66F950822630E5E6DA5D6FFEAF7A34 * get_Completed_17() const { return ___Completed_17; }
	inline EventHandler_1_tFBF7D1676A66F950822630E5E6DA5D6FFEAF7A34 ** get_address_of_Completed_17() { return &___Completed_17; }
	inline void set_Completed_17(EventHandler_1_tFBF7D1676A66F950822630E5E6DA5D6FFEAF7A34 * value)
	{
		___Completed_17 = value;
		Il2CppCodeGenWriteBarrier((&___Completed_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOCKETASYNCEVENTARGS_T5E05ABA36B4740D22D5E746DC4E7763AA448CAA7_H
#ifndef SOCKETASYNCRESULT_T63145D172556590482549D41328C0668E19CB69C_H
#define SOCKETASYNCRESULT_T63145D172556590482549D41328C0668E19CB69C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Sockets.SocketAsyncResult
struct  SocketAsyncResult_t63145D172556590482549D41328C0668E19CB69C  : public IOAsyncResult_tB02ABC485035B18A731F1B61FB27EE17F4B792AD
{
public:
	// System.Net.Sockets.Socket System.Net.Sockets.SocketAsyncResult::socket
	Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 * ___socket_5;
	// System.Net.Sockets.SocketOperation System.Net.Sockets.SocketAsyncResult::operation
	int32_t ___operation_6;
	// System.Exception System.Net.Sockets.SocketAsyncResult::DelayedException
	Exception_t * ___DelayedException_7;
	// System.Net.EndPoint System.Net.Sockets.SocketAsyncResult::EndPoint
	EndPoint_tD87FCEF2780A951E8CE8D808C345FBF2C088D980 * ___EndPoint_8;
	// System.Byte[] System.Net.Sockets.SocketAsyncResult::Buffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___Buffer_9;
	// System.Int32 System.Net.Sockets.SocketAsyncResult::Offset
	int32_t ___Offset_10;
	// System.Int32 System.Net.Sockets.SocketAsyncResult::Size
	int32_t ___Size_11;
	// System.Net.Sockets.SocketFlags System.Net.Sockets.SocketAsyncResult::SockFlags
	int32_t ___SockFlags_12;
	// System.Net.Sockets.Socket System.Net.Sockets.SocketAsyncResult::AcceptSocket
	Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 * ___AcceptSocket_13;
	// System.Net.IPAddress[] System.Net.Sockets.SocketAsyncResult::Addresses
	IPAddressU5BU5D_t7F25C4C038C43BFDA8EA84969112E82BADC38BC3* ___Addresses_14;
	// System.Int32 System.Net.Sockets.SocketAsyncResult::Port
	int32_t ___Port_15;
	// System.Collections.Generic.IList`1<System.ArraySegment`1<System.Byte>> System.Net.Sockets.SocketAsyncResult::Buffers
	RuntimeObject* ___Buffers_16;
	// System.Boolean System.Net.Sockets.SocketAsyncResult::ReuseSocket
	bool ___ReuseSocket_17;
	// System.Int32 System.Net.Sockets.SocketAsyncResult::CurrentAddress
	int32_t ___CurrentAddress_18;
	// System.Net.Sockets.Socket System.Net.Sockets.SocketAsyncResult::AcceptedSocket
	Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 * ___AcceptedSocket_19;
	// System.Int32 System.Net.Sockets.SocketAsyncResult::Total
	int32_t ___Total_20;
	// System.Int32 System.Net.Sockets.SocketAsyncResult::error
	int32_t ___error_21;
	// System.Int32 System.Net.Sockets.SocketAsyncResult::EndCalled
	int32_t ___EndCalled_22;

public:
	inline static int32_t get_offset_of_socket_5() { return static_cast<int32_t>(offsetof(SocketAsyncResult_t63145D172556590482549D41328C0668E19CB69C, ___socket_5)); }
	inline Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 * get_socket_5() const { return ___socket_5; }
	inline Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 ** get_address_of_socket_5() { return &___socket_5; }
	inline void set_socket_5(Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 * value)
	{
		___socket_5 = value;
		Il2CppCodeGenWriteBarrier((&___socket_5), value);
	}

	inline static int32_t get_offset_of_operation_6() { return static_cast<int32_t>(offsetof(SocketAsyncResult_t63145D172556590482549D41328C0668E19CB69C, ___operation_6)); }
	inline int32_t get_operation_6() const { return ___operation_6; }
	inline int32_t* get_address_of_operation_6() { return &___operation_6; }
	inline void set_operation_6(int32_t value)
	{
		___operation_6 = value;
	}

	inline static int32_t get_offset_of_DelayedException_7() { return static_cast<int32_t>(offsetof(SocketAsyncResult_t63145D172556590482549D41328C0668E19CB69C, ___DelayedException_7)); }
	inline Exception_t * get_DelayedException_7() const { return ___DelayedException_7; }
	inline Exception_t ** get_address_of_DelayedException_7() { return &___DelayedException_7; }
	inline void set_DelayedException_7(Exception_t * value)
	{
		___DelayedException_7 = value;
		Il2CppCodeGenWriteBarrier((&___DelayedException_7), value);
	}

	inline static int32_t get_offset_of_EndPoint_8() { return static_cast<int32_t>(offsetof(SocketAsyncResult_t63145D172556590482549D41328C0668E19CB69C, ___EndPoint_8)); }
	inline EndPoint_tD87FCEF2780A951E8CE8D808C345FBF2C088D980 * get_EndPoint_8() const { return ___EndPoint_8; }
	inline EndPoint_tD87FCEF2780A951E8CE8D808C345FBF2C088D980 ** get_address_of_EndPoint_8() { return &___EndPoint_8; }
	inline void set_EndPoint_8(EndPoint_tD87FCEF2780A951E8CE8D808C345FBF2C088D980 * value)
	{
		___EndPoint_8 = value;
		Il2CppCodeGenWriteBarrier((&___EndPoint_8), value);
	}

	inline static int32_t get_offset_of_Buffer_9() { return static_cast<int32_t>(offsetof(SocketAsyncResult_t63145D172556590482549D41328C0668E19CB69C, ___Buffer_9)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_Buffer_9() const { return ___Buffer_9; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_Buffer_9() { return &___Buffer_9; }
	inline void set_Buffer_9(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___Buffer_9 = value;
		Il2CppCodeGenWriteBarrier((&___Buffer_9), value);
	}

	inline static int32_t get_offset_of_Offset_10() { return static_cast<int32_t>(offsetof(SocketAsyncResult_t63145D172556590482549D41328C0668E19CB69C, ___Offset_10)); }
	inline int32_t get_Offset_10() const { return ___Offset_10; }
	inline int32_t* get_address_of_Offset_10() { return &___Offset_10; }
	inline void set_Offset_10(int32_t value)
	{
		___Offset_10 = value;
	}

	inline static int32_t get_offset_of_Size_11() { return static_cast<int32_t>(offsetof(SocketAsyncResult_t63145D172556590482549D41328C0668E19CB69C, ___Size_11)); }
	inline int32_t get_Size_11() const { return ___Size_11; }
	inline int32_t* get_address_of_Size_11() { return &___Size_11; }
	inline void set_Size_11(int32_t value)
	{
		___Size_11 = value;
	}

	inline static int32_t get_offset_of_SockFlags_12() { return static_cast<int32_t>(offsetof(SocketAsyncResult_t63145D172556590482549D41328C0668E19CB69C, ___SockFlags_12)); }
	inline int32_t get_SockFlags_12() const { return ___SockFlags_12; }
	inline int32_t* get_address_of_SockFlags_12() { return &___SockFlags_12; }
	inline void set_SockFlags_12(int32_t value)
	{
		___SockFlags_12 = value;
	}

	inline static int32_t get_offset_of_AcceptSocket_13() { return static_cast<int32_t>(offsetof(SocketAsyncResult_t63145D172556590482549D41328C0668E19CB69C, ___AcceptSocket_13)); }
	inline Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 * get_AcceptSocket_13() const { return ___AcceptSocket_13; }
	inline Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 ** get_address_of_AcceptSocket_13() { return &___AcceptSocket_13; }
	inline void set_AcceptSocket_13(Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 * value)
	{
		___AcceptSocket_13 = value;
		Il2CppCodeGenWriteBarrier((&___AcceptSocket_13), value);
	}

	inline static int32_t get_offset_of_Addresses_14() { return static_cast<int32_t>(offsetof(SocketAsyncResult_t63145D172556590482549D41328C0668E19CB69C, ___Addresses_14)); }
	inline IPAddressU5BU5D_t7F25C4C038C43BFDA8EA84969112E82BADC38BC3* get_Addresses_14() const { return ___Addresses_14; }
	inline IPAddressU5BU5D_t7F25C4C038C43BFDA8EA84969112E82BADC38BC3** get_address_of_Addresses_14() { return &___Addresses_14; }
	inline void set_Addresses_14(IPAddressU5BU5D_t7F25C4C038C43BFDA8EA84969112E82BADC38BC3* value)
	{
		___Addresses_14 = value;
		Il2CppCodeGenWriteBarrier((&___Addresses_14), value);
	}

	inline static int32_t get_offset_of_Port_15() { return static_cast<int32_t>(offsetof(SocketAsyncResult_t63145D172556590482549D41328C0668E19CB69C, ___Port_15)); }
	inline int32_t get_Port_15() const { return ___Port_15; }
	inline int32_t* get_address_of_Port_15() { return &___Port_15; }
	inline void set_Port_15(int32_t value)
	{
		___Port_15 = value;
	}

	inline static int32_t get_offset_of_Buffers_16() { return static_cast<int32_t>(offsetof(SocketAsyncResult_t63145D172556590482549D41328C0668E19CB69C, ___Buffers_16)); }
	inline RuntimeObject* get_Buffers_16() const { return ___Buffers_16; }
	inline RuntimeObject** get_address_of_Buffers_16() { return &___Buffers_16; }
	inline void set_Buffers_16(RuntimeObject* value)
	{
		___Buffers_16 = value;
		Il2CppCodeGenWriteBarrier((&___Buffers_16), value);
	}

	inline static int32_t get_offset_of_ReuseSocket_17() { return static_cast<int32_t>(offsetof(SocketAsyncResult_t63145D172556590482549D41328C0668E19CB69C, ___ReuseSocket_17)); }
	inline bool get_ReuseSocket_17() const { return ___ReuseSocket_17; }
	inline bool* get_address_of_ReuseSocket_17() { return &___ReuseSocket_17; }
	inline void set_ReuseSocket_17(bool value)
	{
		___ReuseSocket_17 = value;
	}

	inline static int32_t get_offset_of_CurrentAddress_18() { return static_cast<int32_t>(offsetof(SocketAsyncResult_t63145D172556590482549D41328C0668E19CB69C, ___CurrentAddress_18)); }
	inline int32_t get_CurrentAddress_18() const { return ___CurrentAddress_18; }
	inline int32_t* get_address_of_CurrentAddress_18() { return &___CurrentAddress_18; }
	inline void set_CurrentAddress_18(int32_t value)
	{
		___CurrentAddress_18 = value;
	}

	inline static int32_t get_offset_of_AcceptedSocket_19() { return static_cast<int32_t>(offsetof(SocketAsyncResult_t63145D172556590482549D41328C0668E19CB69C, ___AcceptedSocket_19)); }
	inline Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 * get_AcceptedSocket_19() const { return ___AcceptedSocket_19; }
	inline Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 ** get_address_of_AcceptedSocket_19() { return &___AcceptedSocket_19; }
	inline void set_AcceptedSocket_19(Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 * value)
	{
		___AcceptedSocket_19 = value;
		Il2CppCodeGenWriteBarrier((&___AcceptedSocket_19), value);
	}

	inline static int32_t get_offset_of_Total_20() { return static_cast<int32_t>(offsetof(SocketAsyncResult_t63145D172556590482549D41328C0668E19CB69C, ___Total_20)); }
	inline int32_t get_Total_20() const { return ___Total_20; }
	inline int32_t* get_address_of_Total_20() { return &___Total_20; }
	inline void set_Total_20(int32_t value)
	{
		___Total_20 = value;
	}

	inline static int32_t get_offset_of_error_21() { return static_cast<int32_t>(offsetof(SocketAsyncResult_t63145D172556590482549D41328C0668E19CB69C, ___error_21)); }
	inline int32_t get_error_21() const { return ___error_21; }
	inline int32_t* get_address_of_error_21() { return &___error_21; }
	inline void set_error_21(int32_t value)
	{
		___error_21 = value;
	}

	inline static int32_t get_offset_of_EndCalled_22() { return static_cast<int32_t>(offsetof(SocketAsyncResult_t63145D172556590482549D41328C0668E19CB69C, ___EndCalled_22)); }
	inline int32_t get_EndCalled_22() const { return ___EndCalled_22; }
	inline int32_t* get_address_of_EndCalled_22() { return &___EndCalled_22; }
	inline void set_EndCalled_22(int32_t value)
	{
		___EndCalled_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Net.Sockets.SocketAsyncResult
struct SocketAsyncResult_t63145D172556590482549D41328C0668E19CB69C_marshaled_pinvoke : public IOAsyncResult_tB02ABC485035B18A731F1B61FB27EE17F4B792AD_marshaled_pinvoke
{
	Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 * ___socket_5;
	int32_t ___operation_6;
	Exception_t_marshaled_pinvoke* ___DelayedException_7;
	EndPoint_tD87FCEF2780A951E8CE8D808C345FBF2C088D980 * ___EndPoint_8;
	uint8_t* ___Buffer_9;
	int32_t ___Offset_10;
	int32_t ___Size_11;
	int32_t ___SockFlags_12;
	Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 * ___AcceptSocket_13;
	IPAddressU5BU5D_t7F25C4C038C43BFDA8EA84969112E82BADC38BC3* ___Addresses_14;
	int32_t ___Port_15;
	RuntimeObject* ___Buffers_16;
	int32_t ___ReuseSocket_17;
	int32_t ___CurrentAddress_18;
	Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 * ___AcceptedSocket_19;
	int32_t ___Total_20;
	int32_t ___error_21;
	int32_t ___EndCalled_22;
};
// Native definition for COM marshalling of System.Net.Sockets.SocketAsyncResult
struct SocketAsyncResult_t63145D172556590482549D41328C0668E19CB69C_marshaled_com : public IOAsyncResult_tB02ABC485035B18A731F1B61FB27EE17F4B792AD_marshaled_com
{
	Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 * ___socket_5;
	int32_t ___operation_6;
	Exception_t_marshaled_com* ___DelayedException_7;
	EndPoint_tD87FCEF2780A951E8CE8D808C345FBF2C088D980 * ___EndPoint_8;
	uint8_t* ___Buffer_9;
	int32_t ___Offset_10;
	int32_t ___Size_11;
	int32_t ___SockFlags_12;
	Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 * ___AcceptSocket_13;
	IPAddressU5BU5D_t7F25C4C038C43BFDA8EA84969112E82BADC38BC3* ___Addresses_14;
	int32_t ___Port_15;
	RuntimeObject* ___Buffers_16;
	int32_t ___ReuseSocket_17;
	int32_t ___CurrentAddress_18;
	Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 * ___AcceptedSocket_19;
	int32_t ___Total_20;
	int32_t ___error_21;
	int32_t ___EndCalled_22;
};
#endif // SOCKETASYNCRESULT_T63145D172556590482549D41328C0668E19CB69C_H
#ifndef TCPCLIENT_T8BC37A84681D1839590AE10B14C25BA473063EDB_H
#define TCPCLIENT_T8BC37A84681D1839590AE10B14C25BA473063EDB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Sockets.TcpClient
struct  TcpClient_t8BC37A84681D1839590AE10B14C25BA473063EDB  : public RuntimeObject
{
public:
	// System.Net.Sockets.Socket System.Net.Sockets.TcpClient::m_ClientSocket
	Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 * ___m_ClientSocket_0;
	// System.Boolean System.Net.Sockets.TcpClient::m_Active
	bool ___m_Active_1;
	// System.Net.Sockets.NetworkStream System.Net.Sockets.TcpClient::m_DataStream
	NetworkStream_t362D0CD0C74C2F5CBD02905C9422E4240872ADCA * ___m_DataStream_2;
	// System.Net.Sockets.AddressFamily System.Net.Sockets.TcpClient::m_Family
	int32_t ___m_Family_3;
	// System.Boolean System.Net.Sockets.TcpClient::m_CleanedUp
	bool ___m_CleanedUp_4;

public:
	inline static int32_t get_offset_of_m_ClientSocket_0() { return static_cast<int32_t>(offsetof(TcpClient_t8BC37A84681D1839590AE10B14C25BA473063EDB, ___m_ClientSocket_0)); }
	inline Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 * get_m_ClientSocket_0() const { return ___m_ClientSocket_0; }
	inline Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 ** get_address_of_m_ClientSocket_0() { return &___m_ClientSocket_0; }
	inline void set_m_ClientSocket_0(Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 * value)
	{
		___m_ClientSocket_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ClientSocket_0), value);
	}

	inline static int32_t get_offset_of_m_Active_1() { return static_cast<int32_t>(offsetof(TcpClient_t8BC37A84681D1839590AE10B14C25BA473063EDB, ___m_Active_1)); }
	inline bool get_m_Active_1() const { return ___m_Active_1; }
	inline bool* get_address_of_m_Active_1() { return &___m_Active_1; }
	inline void set_m_Active_1(bool value)
	{
		___m_Active_1 = value;
	}

	inline static int32_t get_offset_of_m_DataStream_2() { return static_cast<int32_t>(offsetof(TcpClient_t8BC37A84681D1839590AE10B14C25BA473063EDB, ___m_DataStream_2)); }
	inline NetworkStream_t362D0CD0C74C2F5CBD02905C9422E4240872ADCA * get_m_DataStream_2() const { return ___m_DataStream_2; }
	inline NetworkStream_t362D0CD0C74C2F5CBD02905C9422E4240872ADCA ** get_address_of_m_DataStream_2() { return &___m_DataStream_2; }
	inline void set_m_DataStream_2(NetworkStream_t362D0CD0C74C2F5CBD02905C9422E4240872ADCA * value)
	{
		___m_DataStream_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_DataStream_2), value);
	}

	inline static int32_t get_offset_of_m_Family_3() { return static_cast<int32_t>(offsetof(TcpClient_t8BC37A84681D1839590AE10B14C25BA473063EDB, ___m_Family_3)); }
	inline int32_t get_m_Family_3() const { return ___m_Family_3; }
	inline int32_t* get_address_of_m_Family_3() { return &___m_Family_3; }
	inline void set_m_Family_3(int32_t value)
	{
		___m_Family_3 = value;
	}

	inline static int32_t get_offset_of_m_CleanedUp_4() { return static_cast<int32_t>(offsetof(TcpClient_t8BC37A84681D1839590AE10B14C25BA473063EDB, ___m_CleanedUp_4)); }
	inline bool get_m_CleanedUp_4() const { return ___m_CleanedUp_4; }
	inline bool* get_address_of_m_CleanedUp_4() { return &___m_CleanedUp_4; }
	inline void set_m_CleanedUp_4(bool value)
	{
		___m_CleanedUp_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TCPCLIENT_T8BC37A84681D1839590AE10B14C25BA473063EDB_H
#ifndef UDPCLIENT_T94741C2FBA0D9E3270ABDDBA57811D00881D5641_H
#define UDPCLIENT_T94741C2FBA0D9E3270ABDDBA57811D00881D5641_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Sockets.UdpClient
struct  UdpClient_t94741C2FBA0D9E3270ABDDBA57811D00881D5641  : public RuntimeObject
{
public:
	// System.Net.Sockets.Socket System.Net.Sockets.UdpClient::m_ClientSocket
	Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 * ___m_ClientSocket_0;
	// System.Boolean System.Net.Sockets.UdpClient::m_Active
	bool ___m_Active_1;
	// System.Byte[] System.Net.Sockets.UdpClient::m_Buffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___m_Buffer_2;
	// System.Net.Sockets.AddressFamily System.Net.Sockets.UdpClient::m_Family
	int32_t ___m_Family_3;
	// System.Boolean System.Net.Sockets.UdpClient::m_CleanedUp
	bool ___m_CleanedUp_4;
	// System.Boolean System.Net.Sockets.UdpClient::m_IsBroadcast
	bool ___m_IsBroadcast_5;

public:
	inline static int32_t get_offset_of_m_ClientSocket_0() { return static_cast<int32_t>(offsetof(UdpClient_t94741C2FBA0D9E3270ABDDBA57811D00881D5641, ___m_ClientSocket_0)); }
	inline Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 * get_m_ClientSocket_0() const { return ___m_ClientSocket_0; }
	inline Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 ** get_address_of_m_ClientSocket_0() { return &___m_ClientSocket_0; }
	inline void set_m_ClientSocket_0(Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 * value)
	{
		___m_ClientSocket_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ClientSocket_0), value);
	}

	inline static int32_t get_offset_of_m_Active_1() { return static_cast<int32_t>(offsetof(UdpClient_t94741C2FBA0D9E3270ABDDBA57811D00881D5641, ___m_Active_1)); }
	inline bool get_m_Active_1() const { return ___m_Active_1; }
	inline bool* get_address_of_m_Active_1() { return &___m_Active_1; }
	inline void set_m_Active_1(bool value)
	{
		___m_Active_1 = value;
	}

	inline static int32_t get_offset_of_m_Buffer_2() { return static_cast<int32_t>(offsetof(UdpClient_t94741C2FBA0D9E3270ABDDBA57811D00881D5641, ___m_Buffer_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_m_Buffer_2() const { return ___m_Buffer_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_m_Buffer_2() { return &___m_Buffer_2; }
	inline void set_m_Buffer_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___m_Buffer_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Buffer_2), value);
	}

	inline static int32_t get_offset_of_m_Family_3() { return static_cast<int32_t>(offsetof(UdpClient_t94741C2FBA0D9E3270ABDDBA57811D00881D5641, ___m_Family_3)); }
	inline int32_t get_m_Family_3() const { return ___m_Family_3; }
	inline int32_t* get_address_of_m_Family_3() { return &___m_Family_3; }
	inline void set_m_Family_3(int32_t value)
	{
		___m_Family_3 = value;
	}

	inline static int32_t get_offset_of_m_CleanedUp_4() { return static_cast<int32_t>(offsetof(UdpClient_t94741C2FBA0D9E3270ABDDBA57811D00881D5641, ___m_CleanedUp_4)); }
	inline bool get_m_CleanedUp_4() const { return ___m_CleanedUp_4; }
	inline bool* get_address_of_m_CleanedUp_4() { return &___m_CleanedUp_4; }
	inline void set_m_CleanedUp_4(bool value)
	{
		___m_CleanedUp_4 = value;
	}

	inline static int32_t get_offset_of_m_IsBroadcast_5() { return static_cast<int32_t>(offsetof(UdpClient_t94741C2FBA0D9E3270ABDDBA57811D00881D5641, ___m_IsBroadcast_5)); }
	inline bool get_m_IsBroadcast_5() const { return ___m_IsBroadcast_5; }
	inline bool* get_address_of_m_IsBroadcast_5() { return &___m_IsBroadcast_5; }
	inline void set_m_IsBroadcast_5(bool value)
	{
		___m_IsBroadcast_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UDPCLIENT_T94741C2FBA0D9E3270ABDDBA57811D00881D5641_H
#ifndef WEBASYNCRESULT_TF700444B9ABA86C7CADBFA7B99DEC52D9FBD87EE_H
#define WEBASYNCRESULT_TF700444B9ABA86C7CADBFA7B99DEC52D9FBD87EE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebAsyncResult
struct  WebAsyncResult_tF700444B9ABA86C7CADBFA7B99DEC52D9FBD87EE  : public SimpleAsyncResult_tA572851810F8E279EE9E5378A6D9A538B1822FC6
{
public:
	// System.Int32 System.Net.WebAsyncResult::nbytes
	int32_t ___nbytes_9;
	// System.IAsyncResult System.Net.WebAsyncResult::innerAsyncResult
	RuntimeObject* ___innerAsyncResult_10;
	// System.Net.HttpWebResponse System.Net.WebAsyncResult::response
	HttpWebResponse_t34CF6A40A4748A0F8694FEFEA3723D9AE3EF3951 * ___response_11;
	// System.IO.Stream System.Net.WebAsyncResult::writeStream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___writeStream_12;
	// System.Byte[] System.Net.WebAsyncResult::buffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___buffer_13;
	// System.Int32 System.Net.WebAsyncResult::offset
	int32_t ___offset_14;
	// System.Int32 System.Net.WebAsyncResult::size
	int32_t ___size_15;
	// System.Boolean System.Net.WebAsyncResult::EndCalled
	bool ___EndCalled_16;
	// System.Boolean System.Net.WebAsyncResult::AsyncWriteAll
	bool ___AsyncWriteAll_17;
	// System.Net.HttpWebRequest System.Net.WebAsyncResult::AsyncObject
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0 * ___AsyncObject_18;

public:
	inline static int32_t get_offset_of_nbytes_9() { return static_cast<int32_t>(offsetof(WebAsyncResult_tF700444B9ABA86C7CADBFA7B99DEC52D9FBD87EE, ___nbytes_9)); }
	inline int32_t get_nbytes_9() const { return ___nbytes_9; }
	inline int32_t* get_address_of_nbytes_9() { return &___nbytes_9; }
	inline void set_nbytes_9(int32_t value)
	{
		___nbytes_9 = value;
	}

	inline static int32_t get_offset_of_innerAsyncResult_10() { return static_cast<int32_t>(offsetof(WebAsyncResult_tF700444B9ABA86C7CADBFA7B99DEC52D9FBD87EE, ___innerAsyncResult_10)); }
	inline RuntimeObject* get_innerAsyncResult_10() const { return ___innerAsyncResult_10; }
	inline RuntimeObject** get_address_of_innerAsyncResult_10() { return &___innerAsyncResult_10; }
	inline void set_innerAsyncResult_10(RuntimeObject* value)
	{
		___innerAsyncResult_10 = value;
		Il2CppCodeGenWriteBarrier((&___innerAsyncResult_10), value);
	}

	inline static int32_t get_offset_of_response_11() { return static_cast<int32_t>(offsetof(WebAsyncResult_tF700444B9ABA86C7CADBFA7B99DEC52D9FBD87EE, ___response_11)); }
	inline HttpWebResponse_t34CF6A40A4748A0F8694FEFEA3723D9AE3EF3951 * get_response_11() const { return ___response_11; }
	inline HttpWebResponse_t34CF6A40A4748A0F8694FEFEA3723D9AE3EF3951 ** get_address_of_response_11() { return &___response_11; }
	inline void set_response_11(HttpWebResponse_t34CF6A40A4748A0F8694FEFEA3723D9AE3EF3951 * value)
	{
		___response_11 = value;
		Il2CppCodeGenWriteBarrier((&___response_11), value);
	}

	inline static int32_t get_offset_of_writeStream_12() { return static_cast<int32_t>(offsetof(WebAsyncResult_tF700444B9ABA86C7CADBFA7B99DEC52D9FBD87EE, ___writeStream_12)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_writeStream_12() const { return ___writeStream_12; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_writeStream_12() { return &___writeStream_12; }
	inline void set_writeStream_12(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___writeStream_12 = value;
		Il2CppCodeGenWriteBarrier((&___writeStream_12), value);
	}

	inline static int32_t get_offset_of_buffer_13() { return static_cast<int32_t>(offsetof(WebAsyncResult_tF700444B9ABA86C7CADBFA7B99DEC52D9FBD87EE, ___buffer_13)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_buffer_13() const { return ___buffer_13; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_buffer_13() { return &___buffer_13; }
	inline void set_buffer_13(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___buffer_13 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_13), value);
	}

	inline static int32_t get_offset_of_offset_14() { return static_cast<int32_t>(offsetof(WebAsyncResult_tF700444B9ABA86C7CADBFA7B99DEC52D9FBD87EE, ___offset_14)); }
	inline int32_t get_offset_14() const { return ___offset_14; }
	inline int32_t* get_address_of_offset_14() { return &___offset_14; }
	inline void set_offset_14(int32_t value)
	{
		___offset_14 = value;
	}

	inline static int32_t get_offset_of_size_15() { return static_cast<int32_t>(offsetof(WebAsyncResult_tF700444B9ABA86C7CADBFA7B99DEC52D9FBD87EE, ___size_15)); }
	inline int32_t get_size_15() const { return ___size_15; }
	inline int32_t* get_address_of_size_15() { return &___size_15; }
	inline void set_size_15(int32_t value)
	{
		___size_15 = value;
	}

	inline static int32_t get_offset_of_EndCalled_16() { return static_cast<int32_t>(offsetof(WebAsyncResult_tF700444B9ABA86C7CADBFA7B99DEC52D9FBD87EE, ___EndCalled_16)); }
	inline bool get_EndCalled_16() const { return ___EndCalled_16; }
	inline bool* get_address_of_EndCalled_16() { return &___EndCalled_16; }
	inline void set_EndCalled_16(bool value)
	{
		___EndCalled_16 = value;
	}

	inline static int32_t get_offset_of_AsyncWriteAll_17() { return static_cast<int32_t>(offsetof(WebAsyncResult_tF700444B9ABA86C7CADBFA7B99DEC52D9FBD87EE, ___AsyncWriteAll_17)); }
	inline bool get_AsyncWriteAll_17() const { return ___AsyncWriteAll_17; }
	inline bool* get_address_of_AsyncWriteAll_17() { return &___AsyncWriteAll_17; }
	inline void set_AsyncWriteAll_17(bool value)
	{
		___AsyncWriteAll_17 = value;
	}

	inline static int32_t get_offset_of_AsyncObject_18() { return static_cast<int32_t>(offsetof(WebAsyncResult_tF700444B9ABA86C7CADBFA7B99DEC52D9FBD87EE, ___AsyncObject_18)); }
	inline HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0 * get_AsyncObject_18() const { return ___AsyncObject_18; }
	inline HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0 ** get_address_of_AsyncObject_18() { return &___AsyncObject_18; }
	inline void set_AsyncObject_18(HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0 * value)
	{
		___AsyncObject_18 = value;
		Il2CppCodeGenWriteBarrier((&___AsyncObject_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBASYNCRESULT_TF700444B9ABA86C7CADBFA7B99DEC52D9FBD87EE_H
#ifndef WEBCONNECTION_TEB76AEE17361D28CBAD4033026A71DA89289C243_H
#define WEBCONNECTION_TEB76AEE17361D28CBAD4033026A71DA89289C243_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebConnection
struct  WebConnection_tEB76AEE17361D28CBAD4033026A71DA89289C243  : public RuntimeObject
{
public:
	// System.Net.ServicePoint System.Net.WebConnection::sPoint
	ServicePoint_t5F42B1A9D56E09B4B051BE0968C81DE3128E3EB4 * ___sPoint_0;
	// System.IO.Stream System.Net.WebConnection::nstream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___nstream_1;
	// System.Net.Sockets.Socket System.Net.WebConnection::socket
	Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 * ___socket_2;
	// System.Object System.Net.WebConnection::socketLock
	RuntimeObject * ___socketLock_3;
	// System.Net.IWebConnectionState System.Net.WebConnection::state
	RuntimeObject* ___state_4;
	// System.Net.WebExceptionStatus System.Net.WebConnection::status
	int32_t ___status_5;
	// System.Boolean System.Net.WebConnection::keepAlive
	bool ___keepAlive_6;
	// System.Byte[] System.Net.WebConnection::buffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___buffer_7;
	// System.EventHandler System.Net.WebConnection::abortHandler
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * ___abortHandler_8;
	// System.Net.WebConnection_AbortHelper System.Net.WebConnection::abortHelper
	AbortHelper_t0DB9458211F015848382C4B5A007AC4947411E81 * ___abortHelper_9;
	// System.Net.WebConnectionData System.Net.WebConnection::Data
	WebConnectionData_tC9286455629F1E9E2BA0CA8AB6958DF931299CCC * ___Data_10;
	// System.Boolean System.Net.WebConnection::chunkedRead
	bool ___chunkedRead_11;
	// System.Net.MonoChunkStream System.Net.WebConnection::chunkStream
	MonoChunkStream_t33C2B7ECB208D77D1C94F0A9F48EB5AE27DF5AB5 * ___chunkStream_12;
	// System.Collections.Queue System.Net.WebConnection::queue
	Queue_tEC6DE7527799C2E4224B469ECD0CDD2B25E8E4F3 * ___queue_13;
	// System.Boolean System.Net.WebConnection::reused
	bool ___reused_14;
	// System.Int32 System.Net.WebConnection::position
	int32_t ___position_15;
	// System.Net.HttpWebRequest System.Net.WebConnection::priority_request
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0 * ___priority_request_16;
	// System.Net.NetworkCredential System.Net.WebConnection::ntlm_credentials
	NetworkCredential_tA91C6E62EA0F0915E6E393F5DFD87D03FF2C3062 * ___ntlm_credentials_17;
	// System.Boolean System.Net.WebConnection::ntlm_authenticated
	bool ___ntlm_authenticated_18;
	// System.Boolean System.Net.WebConnection::unsafe_sharing
	bool ___unsafe_sharing_19;
	// System.Net.WebConnection_NtlmAuthState System.Net.WebConnection::connect_ntlm_auth_state
	int32_t ___connect_ntlm_auth_state_20;
	// System.Net.HttpWebRequest System.Net.WebConnection::connect_request
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0 * ___connect_request_21;
	// System.Exception System.Net.WebConnection::connect_exception
	Exception_t * ___connect_exception_22;
	// Mono.Net.Security.MonoTlsStream System.Net.WebConnection::tlsStream
	MonoTlsStream_t15DF42240B3214CA6A4A8FD8671C173B572DF171 * ___tlsStream_23;

public:
	inline static int32_t get_offset_of_sPoint_0() { return static_cast<int32_t>(offsetof(WebConnection_tEB76AEE17361D28CBAD4033026A71DA89289C243, ___sPoint_0)); }
	inline ServicePoint_t5F42B1A9D56E09B4B051BE0968C81DE3128E3EB4 * get_sPoint_0() const { return ___sPoint_0; }
	inline ServicePoint_t5F42B1A9D56E09B4B051BE0968C81DE3128E3EB4 ** get_address_of_sPoint_0() { return &___sPoint_0; }
	inline void set_sPoint_0(ServicePoint_t5F42B1A9D56E09B4B051BE0968C81DE3128E3EB4 * value)
	{
		___sPoint_0 = value;
		Il2CppCodeGenWriteBarrier((&___sPoint_0), value);
	}

	inline static int32_t get_offset_of_nstream_1() { return static_cast<int32_t>(offsetof(WebConnection_tEB76AEE17361D28CBAD4033026A71DA89289C243, ___nstream_1)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_nstream_1() const { return ___nstream_1; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_nstream_1() { return &___nstream_1; }
	inline void set_nstream_1(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___nstream_1 = value;
		Il2CppCodeGenWriteBarrier((&___nstream_1), value);
	}

	inline static int32_t get_offset_of_socket_2() { return static_cast<int32_t>(offsetof(WebConnection_tEB76AEE17361D28CBAD4033026A71DA89289C243, ___socket_2)); }
	inline Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 * get_socket_2() const { return ___socket_2; }
	inline Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 ** get_address_of_socket_2() { return &___socket_2; }
	inline void set_socket_2(Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 * value)
	{
		___socket_2 = value;
		Il2CppCodeGenWriteBarrier((&___socket_2), value);
	}

	inline static int32_t get_offset_of_socketLock_3() { return static_cast<int32_t>(offsetof(WebConnection_tEB76AEE17361D28CBAD4033026A71DA89289C243, ___socketLock_3)); }
	inline RuntimeObject * get_socketLock_3() const { return ___socketLock_3; }
	inline RuntimeObject ** get_address_of_socketLock_3() { return &___socketLock_3; }
	inline void set_socketLock_3(RuntimeObject * value)
	{
		___socketLock_3 = value;
		Il2CppCodeGenWriteBarrier((&___socketLock_3), value);
	}

	inline static int32_t get_offset_of_state_4() { return static_cast<int32_t>(offsetof(WebConnection_tEB76AEE17361D28CBAD4033026A71DA89289C243, ___state_4)); }
	inline RuntimeObject* get_state_4() const { return ___state_4; }
	inline RuntimeObject** get_address_of_state_4() { return &___state_4; }
	inline void set_state_4(RuntimeObject* value)
	{
		___state_4 = value;
		Il2CppCodeGenWriteBarrier((&___state_4), value);
	}

	inline static int32_t get_offset_of_status_5() { return static_cast<int32_t>(offsetof(WebConnection_tEB76AEE17361D28CBAD4033026A71DA89289C243, ___status_5)); }
	inline int32_t get_status_5() const { return ___status_5; }
	inline int32_t* get_address_of_status_5() { return &___status_5; }
	inline void set_status_5(int32_t value)
	{
		___status_5 = value;
	}

	inline static int32_t get_offset_of_keepAlive_6() { return static_cast<int32_t>(offsetof(WebConnection_tEB76AEE17361D28CBAD4033026A71DA89289C243, ___keepAlive_6)); }
	inline bool get_keepAlive_6() const { return ___keepAlive_6; }
	inline bool* get_address_of_keepAlive_6() { return &___keepAlive_6; }
	inline void set_keepAlive_6(bool value)
	{
		___keepAlive_6 = value;
	}

	inline static int32_t get_offset_of_buffer_7() { return static_cast<int32_t>(offsetof(WebConnection_tEB76AEE17361D28CBAD4033026A71DA89289C243, ___buffer_7)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_buffer_7() const { return ___buffer_7; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_buffer_7() { return &___buffer_7; }
	inline void set_buffer_7(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___buffer_7 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_7), value);
	}

	inline static int32_t get_offset_of_abortHandler_8() { return static_cast<int32_t>(offsetof(WebConnection_tEB76AEE17361D28CBAD4033026A71DA89289C243, ___abortHandler_8)); }
	inline EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * get_abortHandler_8() const { return ___abortHandler_8; }
	inline EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C ** get_address_of_abortHandler_8() { return &___abortHandler_8; }
	inline void set_abortHandler_8(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * value)
	{
		___abortHandler_8 = value;
		Il2CppCodeGenWriteBarrier((&___abortHandler_8), value);
	}

	inline static int32_t get_offset_of_abortHelper_9() { return static_cast<int32_t>(offsetof(WebConnection_tEB76AEE17361D28CBAD4033026A71DA89289C243, ___abortHelper_9)); }
	inline AbortHelper_t0DB9458211F015848382C4B5A007AC4947411E81 * get_abortHelper_9() const { return ___abortHelper_9; }
	inline AbortHelper_t0DB9458211F015848382C4B5A007AC4947411E81 ** get_address_of_abortHelper_9() { return &___abortHelper_9; }
	inline void set_abortHelper_9(AbortHelper_t0DB9458211F015848382C4B5A007AC4947411E81 * value)
	{
		___abortHelper_9 = value;
		Il2CppCodeGenWriteBarrier((&___abortHelper_9), value);
	}

	inline static int32_t get_offset_of_Data_10() { return static_cast<int32_t>(offsetof(WebConnection_tEB76AEE17361D28CBAD4033026A71DA89289C243, ___Data_10)); }
	inline WebConnectionData_tC9286455629F1E9E2BA0CA8AB6958DF931299CCC * get_Data_10() const { return ___Data_10; }
	inline WebConnectionData_tC9286455629F1E9E2BA0CA8AB6958DF931299CCC ** get_address_of_Data_10() { return &___Data_10; }
	inline void set_Data_10(WebConnectionData_tC9286455629F1E9E2BA0CA8AB6958DF931299CCC * value)
	{
		___Data_10 = value;
		Il2CppCodeGenWriteBarrier((&___Data_10), value);
	}

	inline static int32_t get_offset_of_chunkedRead_11() { return static_cast<int32_t>(offsetof(WebConnection_tEB76AEE17361D28CBAD4033026A71DA89289C243, ___chunkedRead_11)); }
	inline bool get_chunkedRead_11() const { return ___chunkedRead_11; }
	inline bool* get_address_of_chunkedRead_11() { return &___chunkedRead_11; }
	inline void set_chunkedRead_11(bool value)
	{
		___chunkedRead_11 = value;
	}

	inline static int32_t get_offset_of_chunkStream_12() { return static_cast<int32_t>(offsetof(WebConnection_tEB76AEE17361D28CBAD4033026A71DA89289C243, ___chunkStream_12)); }
	inline MonoChunkStream_t33C2B7ECB208D77D1C94F0A9F48EB5AE27DF5AB5 * get_chunkStream_12() const { return ___chunkStream_12; }
	inline MonoChunkStream_t33C2B7ECB208D77D1C94F0A9F48EB5AE27DF5AB5 ** get_address_of_chunkStream_12() { return &___chunkStream_12; }
	inline void set_chunkStream_12(MonoChunkStream_t33C2B7ECB208D77D1C94F0A9F48EB5AE27DF5AB5 * value)
	{
		___chunkStream_12 = value;
		Il2CppCodeGenWriteBarrier((&___chunkStream_12), value);
	}

	inline static int32_t get_offset_of_queue_13() { return static_cast<int32_t>(offsetof(WebConnection_tEB76AEE17361D28CBAD4033026A71DA89289C243, ___queue_13)); }
	inline Queue_tEC6DE7527799C2E4224B469ECD0CDD2B25E8E4F3 * get_queue_13() const { return ___queue_13; }
	inline Queue_tEC6DE7527799C2E4224B469ECD0CDD2B25E8E4F3 ** get_address_of_queue_13() { return &___queue_13; }
	inline void set_queue_13(Queue_tEC6DE7527799C2E4224B469ECD0CDD2B25E8E4F3 * value)
	{
		___queue_13 = value;
		Il2CppCodeGenWriteBarrier((&___queue_13), value);
	}

	inline static int32_t get_offset_of_reused_14() { return static_cast<int32_t>(offsetof(WebConnection_tEB76AEE17361D28CBAD4033026A71DA89289C243, ___reused_14)); }
	inline bool get_reused_14() const { return ___reused_14; }
	inline bool* get_address_of_reused_14() { return &___reused_14; }
	inline void set_reused_14(bool value)
	{
		___reused_14 = value;
	}

	inline static int32_t get_offset_of_position_15() { return static_cast<int32_t>(offsetof(WebConnection_tEB76AEE17361D28CBAD4033026A71DA89289C243, ___position_15)); }
	inline int32_t get_position_15() const { return ___position_15; }
	inline int32_t* get_address_of_position_15() { return &___position_15; }
	inline void set_position_15(int32_t value)
	{
		___position_15 = value;
	}

	inline static int32_t get_offset_of_priority_request_16() { return static_cast<int32_t>(offsetof(WebConnection_tEB76AEE17361D28CBAD4033026A71DA89289C243, ___priority_request_16)); }
	inline HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0 * get_priority_request_16() const { return ___priority_request_16; }
	inline HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0 ** get_address_of_priority_request_16() { return &___priority_request_16; }
	inline void set_priority_request_16(HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0 * value)
	{
		___priority_request_16 = value;
		Il2CppCodeGenWriteBarrier((&___priority_request_16), value);
	}

	inline static int32_t get_offset_of_ntlm_credentials_17() { return static_cast<int32_t>(offsetof(WebConnection_tEB76AEE17361D28CBAD4033026A71DA89289C243, ___ntlm_credentials_17)); }
	inline NetworkCredential_tA91C6E62EA0F0915E6E393F5DFD87D03FF2C3062 * get_ntlm_credentials_17() const { return ___ntlm_credentials_17; }
	inline NetworkCredential_tA91C6E62EA0F0915E6E393F5DFD87D03FF2C3062 ** get_address_of_ntlm_credentials_17() { return &___ntlm_credentials_17; }
	inline void set_ntlm_credentials_17(NetworkCredential_tA91C6E62EA0F0915E6E393F5DFD87D03FF2C3062 * value)
	{
		___ntlm_credentials_17 = value;
		Il2CppCodeGenWriteBarrier((&___ntlm_credentials_17), value);
	}

	inline static int32_t get_offset_of_ntlm_authenticated_18() { return static_cast<int32_t>(offsetof(WebConnection_tEB76AEE17361D28CBAD4033026A71DA89289C243, ___ntlm_authenticated_18)); }
	inline bool get_ntlm_authenticated_18() const { return ___ntlm_authenticated_18; }
	inline bool* get_address_of_ntlm_authenticated_18() { return &___ntlm_authenticated_18; }
	inline void set_ntlm_authenticated_18(bool value)
	{
		___ntlm_authenticated_18 = value;
	}

	inline static int32_t get_offset_of_unsafe_sharing_19() { return static_cast<int32_t>(offsetof(WebConnection_tEB76AEE17361D28CBAD4033026A71DA89289C243, ___unsafe_sharing_19)); }
	inline bool get_unsafe_sharing_19() const { return ___unsafe_sharing_19; }
	inline bool* get_address_of_unsafe_sharing_19() { return &___unsafe_sharing_19; }
	inline void set_unsafe_sharing_19(bool value)
	{
		___unsafe_sharing_19 = value;
	}

	inline static int32_t get_offset_of_connect_ntlm_auth_state_20() { return static_cast<int32_t>(offsetof(WebConnection_tEB76AEE17361D28CBAD4033026A71DA89289C243, ___connect_ntlm_auth_state_20)); }
	inline int32_t get_connect_ntlm_auth_state_20() const { return ___connect_ntlm_auth_state_20; }
	inline int32_t* get_address_of_connect_ntlm_auth_state_20() { return &___connect_ntlm_auth_state_20; }
	inline void set_connect_ntlm_auth_state_20(int32_t value)
	{
		___connect_ntlm_auth_state_20 = value;
	}

	inline static int32_t get_offset_of_connect_request_21() { return static_cast<int32_t>(offsetof(WebConnection_tEB76AEE17361D28CBAD4033026A71DA89289C243, ___connect_request_21)); }
	inline HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0 * get_connect_request_21() const { return ___connect_request_21; }
	inline HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0 ** get_address_of_connect_request_21() { return &___connect_request_21; }
	inline void set_connect_request_21(HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0 * value)
	{
		___connect_request_21 = value;
		Il2CppCodeGenWriteBarrier((&___connect_request_21), value);
	}

	inline static int32_t get_offset_of_connect_exception_22() { return static_cast<int32_t>(offsetof(WebConnection_tEB76AEE17361D28CBAD4033026A71DA89289C243, ___connect_exception_22)); }
	inline Exception_t * get_connect_exception_22() const { return ___connect_exception_22; }
	inline Exception_t ** get_address_of_connect_exception_22() { return &___connect_exception_22; }
	inline void set_connect_exception_22(Exception_t * value)
	{
		___connect_exception_22 = value;
		Il2CppCodeGenWriteBarrier((&___connect_exception_22), value);
	}

	inline static int32_t get_offset_of_tlsStream_23() { return static_cast<int32_t>(offsetof(WebConnection_tEB76AEE17361D28CBAD4033026A71DA89289C243, ___tlsStream_23)); }
	inline MonoTlsStream_t15DF42240B3214CA6A4A8FD8671C173B572DF171 * get_tlsStream_23() const { return ___tlsStream_23; }
	inline MonoTlsStream_t15DF42240B3214CA6A4A8FD8671C173B572DF171 ** get_address_of_tlsStream_23() { return &___tlsStream_23; }
	inline void set_tlsStream_23(MonoTlsStream_t15DF42240B3214CA6A4A8FD8671C173B572DF171 * value)
	{
		___tlsStream_23 = value;
		Il2CppCodeGenWriteBarrier((&___tlsStream_23), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBCONNECTION_TEB76AEE17361D28CBAD4033026A71DA89289C243_H
#ifndef WEBCONNECTIONDATA_TC9286455629F1E9E2BA0CA8AB6958DF931299CCC_H
#define WEBCONNECTIONDATA_TC9286455629F1E9E2BA0CA8AB6958DF931299CCC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebConnectionData
struct  WebConnectionData_tC9286455629F1E9E2BA0CA8AB6958DF931299CCC  : public RuntimeObject
{
public:
	// System.Net.HttpWebRequest System.Net.WebConnectionData::_request
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0 * ____request_0;
	// System.Int32 System.Net.WebConnectionData::StatusCode
	int32_t ___StatusCode_1;
	// System.String System.Net.WebConnectionData::StatusDescription
	String_t* ___StatusDescription_2;
	// System.Net.WebHeaderCollection System.Net.WebConnectionData::Headers
	WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304 * ___Headers_3;
	// System.Version System.Net.WebConnectionData::Version
	Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD * ___Version_4;
	// System.Version System.Net.WebConnectionData::ProxyVersion
	Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD * ___ProxyVersion_5;
	// System.IO.Stream System.Net.WebConnectionData::stream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___stream_6;
	// System.String[] System.Net.WebConnectionData::Challenge
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___Challenge_7;
	// System.Net.ReadState System.Net.WebConnectionData::_readState
	int32_t ____readState_8;

public:
	inline static int32_t get_offset_of__request_0() { return static_cast<int32_t>(offsetof(WebConnectionData_tC9286455629F1E9E2BA0CA8AB6958DF931299CCC, ____request_0)); }
	inline HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0 * get__request_0() const { return ____request_0; }
	inline HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0 ** get_address_of__request_0() { return &____request_0; }
	inline void set__request_0(HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0 * value)
	{
		____request_0 = value;
		Il2CppCodeGenWriteBarrier((&____request_0), value);
	}

	inline static int32_t get_offset_of_StatusCode_1() { return static_cast<int32_t>(offsetof(WebConnectionData_tC9286455629F1E9E2BA0CA8AB6958DF931299CCC, ___StatusCode_1)); }
	inline int32_t get_StatusCode_1() const { return ___StatusCode_1; }
	inline int32_t* get_address_of_StatusCode_1() { return &___StatusCode_1; }
	inline void set_StatusCode_1(int32_t value)
	{
		___StatusCode_1 = value;
	}

	inline static int32_t get_offset_of_StatusDescription_2() { return static_cast<int32_t>(offsetof(WebConnectionData_tC9286455629F1E9E2BA0CA8AB6958DF931299CCC, ___StatusDescription_2)); }
	inline String_t* get_StatusDescription_2() const { return ___StatusDescription_2; }
	inline String_t** get_address_of_StatusDescription_2() { return &___StatusDescription_2; }
	inline void set_StatusDescription_2(String_t* value)
	{
		___StatusDescription_2 = value;
		Il2CppCodeGenWriteBarrier((&___StatusDescription_2), value);
	}

	inline static int32_t get_offset_of_Headers_3() { return static_cast<int32_t>(offsetof(WebConnectionData_tC9286455629F1E9E2BA0CA8AB6958DF931299CCC, ___Headers_3)); }
	inline WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304 * get_Headers_3() const { return ___Headers_3; }
	inline WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304 ** get_address_of_Headers_3() { return &___Headers_3; }
	inline void set_Headers_3(WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304 * value)
	{
		___Headers_3 = value;
		Il2CppCodeGenWriteBarrier((&___Headers_3), value);
	}

	inline static int32_t get_offset_of_Version_4() { return static_cast<int32_t>(offsetof(WebConnectionData_tC9286455629F1E9E2BA0CA8AB6958DF931299CCC, ___Version_4)); }
	inline Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD * get_Version_4() const { return ___Version_4; }
	inline Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD ** get_address_of_Version_4() { return &___Version_4; }
	inline void set_Version_4(Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD * value)
	{
		___Version_4 = value;
		Il2CppCodeGenWriteBarrier((&___Version_4), value);
	}

	inline static int32_t get_offset_of_ProxyVersion_5() { return static_cast<int32_t>(offsetof(WebConnectionData_tC9286455629F1E9E2BA0CA8AB6958DF931299CCC, ___ProxyVersion_5)); }
	inline Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD * get_ProxyVersion_5() const { return ___ProxyVersion_5; }
	inline Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD ** get_address_of_ProxyVersion_5() { return &___ProxyVersion_5; }
	inline void set_ProxyVersion_5(Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD * value)
	{
		___ProxyVersion_5 = value;
		Il2CppCodeGenWriteBarrier((&___ProxyVersion_5), value);
	}

	inline static int32_t get_offset_of_stream_6() { return static_cast<int32_t>(offsetof(WebConnectionData_tC9286455629F1E9E2BA0CA8AB6958DF931299CCC, ___stream_6)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_stream_6() const { return ___stream_6; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_stream_6() { return &___stream_6; }
	inline void set_stream_6(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___stream_6 = value;
		Il2CppCodeGenWriteBarrier((&___stream_6), value);
	}

	inline static int32_t get_offset_of_Challenge_7() { return static_cast<int32_t>(offsetof(WebConnectionData_tC9286455629F1E9E2BA0CA8AB6958DF931299CCC, ___Challenge_7)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_Challenge_7() const { return ___Challenge_7; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_Challenge_7() { return &___Challenge_7; }
	inline void set_Challenge_7(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___Challenge_7 = value;
		Il2CppCodeGenWriteBarrier((&___Challenge_7), value);
	}

	inline static int32_t get_offset_of__readState_8() { return static_cast<int32_t>(offsetof(WebConnectionData_tC9286455629F1E9E2BA0CA8AB6958DF931299CCC, ____readState_8)); }
	inline int32_t get__readState_8() const { return ____readState_8; }
	inline int32_t* get_address_of__readState_8() { return &____readState_8; }
	inline void set__readState_8(int32_t value)
	{
		____readState_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBCONNECTIONDATA_TC9286455629F1E9E2BA0CA8AB6958DF931299CCC_H
#ifndef WEBREQUEST_T5668DA48802E9FE2F1DE5F5A770B218608B918C8_H
#define WEBREQUEST_T5668DA48802E9FE2F1DE5F5A770B218608B918C8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebRequest
struct  WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8  : public MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF
{
public:
	// System.Net.Security.AuthenticationLevel System.Net.WebRequest::m_AuthenticationLevel
	int32_t ___m_AuthenticationLevel_5;
	// System.Security.Principal.TokenImpersonationLevel System.Net.WebRequest::m_ImpersonationLevel
	int32_t ___m_ImpersonationLevel_6;
	// System.Net.Cache.RequestCachePolicy System.Net.WebRequest::m_CachePolicy
	RequestCachePolicy_t30D7352C7E9D49EEADD492A70EC92C118D90CD61 * ___m_CachePolicy_7;
	// System.Net.Cache.RequestCacheProtocol System.Net.WebRequest::m_CacheProtocol
	RequestCacheProtocol_t51DE21412EAD66CAD600D3A6940942920340D35D * ___m_CacheProtocol_8;
	// System.Net.Cache.RequestCacheBinding System.Net.WebRequest::m_CacheBinding
	RequestCacheBinding_tB84D71781C4BCEF43DEBC72165283C4543BA4724 * ___m_CacheBinding_9;

public:
	inline static int32_t get_offset_of_m_AuthenticationLevel_5() { return static_cast<int32_t>(offsetof(WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8, ___m_AuthenticationLevel_5)); }
	inline int32_t get_m_AuthenticationLevel_5() const { return ___m_AuthenticationLevel_5; }
	inline int32_t* get_address_of_m_AuthenticationLevel_5() { return &___m_AuthenticationLevel_5; }
	inline void set_m_AuthenticationLevel_5(int32_t value)
	{
		___m_AuthenticationLevel_5 = value;
	}

	inline static int32_t get_offset_of_m_ImpersonationLevel_6() { return static_cast<int32_t>(offsetof(WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8, ___m_ImpersonationLevel_6)); }
	inline int32_t get_m_ImpersonationLevel_6() const { return ___m_ImpersonationLevel_6; }
	inline int32_t* get_address_of_m_ImpersonationLevel_6() { return &___m_ImpersonationLevel_6; }
	inline void set_m_ImpersonationLevel_6(int32_t value)
	{
		___m_ImpersonationLevel_6 = value;
	}

	inline static int32_t get_offset_of_m_CachePolicy_7() { return static_cast<int32_t>(offsetof(WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8, ___m_CachePolicy_7)); }
	inline RequestCachePolicy_t30D7352C7E9D49EEADD492A70EC92C118D90CD61 * get_m_CachePolicy_7() const { return ___m_CachePolicy_7; }
	inline RequestCachePolicy_t30D7352C7E9D49EEADD492A70EC92C118D90CD61 ** get_address_of_m_CachePolicy_7() { return &___m_CachePolicy_7; }
	inline void set_m_CachePolicy_7(RequestCachePolicy_t30D7352C7E9D49EEADD492A70EC92C118D90CD61 * value)
	{
		___m_CachePolicy_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_CachePolicy_7), value);
	}

	inline static int32_t get_offset_of_m_CacheProtocol_8() { return static_cast<int32_t>(offsetof(WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8, ___m_CacheProtocol_8)); }
	inline RequestCacheProtocol_t51DE21412EAD66CAD600D3A6940942920340D35D * get_m_CacheProtocol_8() const { return ___m_CacheProtocol_8; }
	inline RequestCacheProtocol_t51DE21412EAD66CAD600D3A6940942920340D35D ** get_address_of_m_CacheProtocol_8() { return &___m_CacheProtocol_8; }
	inline void set_m_CacheProtocol_8(RequestCacheProtocol_t51DE21412EAD66CAD600D3A6940942920340D35D * value)
	{
		___m_CacheProtocol_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_CacheProtocol_8), value);
	}

	inline static int32_t get_offset_of_m_CacheBinding_9() { return static_cast<int32_t>(offsetof(WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8, ___m_CacheBinding_9)); }
	inline RequestCacheBinding_tB84D71781C4BCEF43DEBC72165283C4543BA4724 * get_m_CacheBinding_9() const { return ___m_CacheBinding_9; }
	inline RequestCacheBinding_tB84D71781C4BCEF43DEBC72165283C4543BA4724 ** get_address_of_m_CacheBinding_9() { return &___m_CacheBinding_9; }
	inline void set_m_CacheBinding_9(RequestCacheBinding_tB84D71781C4BCEF43DEBC72165283C4543BA4724 * value)
	{
		___m_CacheBinding_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_CacheBinding_9), value);
	}
};

struct WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8_StaticFields
{
public:
	// System.Collections.ArrayList modreq(System.Runtime.CompilerServices.IsVolatile) System.Net.WebRequest::s_PrefixList
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ___s_PrefixList_2;
	// System.Object System.Net.WebRequest::s_InternalSyncObject
	RuntimeObject * ___s_InternalSyncObject_3;
	// System.Net.TimerThread_Queue System.Net.WebRequest::s_DefaultTimerQueue
	Queue_tCCFF6A2FCF584216AEDA04A483FB808E2D493643 * ___s_DefaultTimerQueue_4;
	// System.Net.WebRequest_DesignerWebRequestCreate System.Net.WebRequest::webRequestCreate
	DesignerWebRequestCreate_t613DD91D4F07703DC65E847B367F4DCD5710E2A3 * ___webRequestCreate_10;
	// System.Net.IWebProxy modreq(System.Runtime.CompilerServices.IsVolatile) System.Net.WebRequest::s_DefaultWebProxy
	RuntimeObject* ___s_DefaultWebProxy_11;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.Net.WebRequest::s_DefaultWebProxyInitialized
	bool ___s_DefaultWebProxyInitialized_12;

public:
	inline static int32_t get_offset_of_s_PrefixList_2() { return static_cast<int32_t>(offsetof(WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8_StaticFields, ___s_PrefixList_2)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get_s_PrefixList_2() const { return ___s_PrefixList_2; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of_s_PrefixList_2() { return &___s_PrefixList_2; }
	inline void set_s_PrefixList_2(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		___s_PrefixList_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_PrefixList_2), value);
	}

	inline static int32_t get_offset_of_s_InternalSyncObject_3() { return static_cast<int32_t>(offsetof(WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8_StaticFields, ___s_InternalSyncObject_3)); }
	inline RuntimeObject * get_s_InternalSyncObject_3() const { return ___s_InternalSyncObject_3; }
	inline RuntimeObject ** get_address_of_s_InternalSyncObject_3() { return &___s_InternalSyncObject_3; }
	inline void set_s_InternalSyncObject_3(RuntimeObject * value)
	{
		___s_InternalSyncObject_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_InternalSyncObject_3), value);
	}

	inline static int32_t get_offset_of_s_DefaultTimerQueue_4() { return static_cast<int32_t>(offsetof(WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8_StaticFields, ___s_DefaultTimerQueue_4)); }
	inline Queue_tCCFF6A2FCF584216AEDA04A483FB808E2D493643 * get_s_DefaultTimerQueue_4() const { return ___s_DefaultTimerQueue_4; }
	inline Queue_tCCFF6A2FCF584216AEDA04A483FB808E2D493643 ** get_address_of_s_DefaultTimerQueue_4() { return &___s_DefaultTimerQueue_4; }
	inline void set_s_DefaultTimerQueue_4(Queue_tCCFF6A2FCF584216AEDA04A483FB808E2D493643 * value)
	{
		___s_DefaultTimerQueue_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultTimerQueue_4), value);
	}

	inline static int32_t get_offset_of_webRequestCreate_10() { return static_cast<int32_t>(offsetof(WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8_StaticFields, ___webRequestCreate_10)); }
	inline DesignerWebRequestCreate_t613DD91D4F07703DC65E847B367F4DCD5710E2A3 * get_webRequestCreate_10() const { return ___webRequestCreate_10; }
	inline DesignerWebRequestCreate_t613DD91D4F07703DC65E847B367F4DCD5710E2A3 ** get_address_of_webRequestCreate_10() { return &___webRequestCreate_10; }
	inline void set_webRequestCreate_10(DesignerWebRequestCreate_t613DD91D4F07703DC65E847B367F4DCD5710E2A3 * value)
	{
		___webRequestCreate_10 = value;
		Il2CppCodeGenWriteBarrier((&___webRequestCreate_10), value);
	}

	inline static int32_t get_offset_of_s_DefaultWebProxy_11() { return static_cast<int32_t>(offsetof(WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8_StaticFields, ___s_DefaultWebProxy_11)); }
	inline RuntimeObject* get_s_DefaultWebProxy_11() const { return ___s_DefaultWebProxy_11; }
	inline RuntimeObject** get_address_of_s_DefaultWebProxy_11() { return &___s_DefaultWebProxy_11; }
	inline void set_s_DefaultWebProxy_11(RuntimeObject* value)
	{
		___s_DefaultWebProxy_11 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultWebProxy_11), value);
	}

	inline static int32_t get_offset_of_s_DefaultWebProxyInitialized_12() { return static_cast<int32_t>(offsetof(WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8_StaticFields, ___s_DefaultWebProxyInitialized_12)); }
	inline bool get_s_DefaultWebProxyInitialized_12() const { return ___s_DefaultWebProxyInitialized_12; }
	inline bool* get_address_of_s_DefaultWebProxyInitialized_12() { return &___s_DefaultWebProxyInitialized_12; }
	inline void set_s_DefaultWebProxyInitialized_12(bool value)
	{
		___s_DefaultWebProxyInitialized_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBREQUEST_T5668DA48802E9FE2F1DE5F5A770B218608B918C8_H
#ifndef SECURITYATTRIBUTE_T9F1DB5B572D6E2D518A2F186A21522667521B195_H
#define SECURITYATTRIBUTE_T9F1DB5B572D6E2D518A2F186A21522667521B195_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Permissions.SecurityAttribute
struct  SecurityAttribute_t9F1DB5B572D6E2D518A2F186A21522667521B195  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Security.Permissions.SecurityAction System.Security.Permissions.SecurityAttribute::m_Action
	int32_t ___m_Action_0;
	// System.Boolean System.Security.Permissions.SecurityAttribute::m_Unrestricted
	bool ___m_Unrestricted_1;

public:
	inline static int32_t get_offset_of_m_Action_0() { return static_cast<int32_t>(offsetof(SecurityAttribute_t9F1DB5B572D6E2D518A2F186A21522667521B195, ___m_Action_0)); }
	inline int32_t get_m_Action_0() const { return ___m_Action_0; }
	inline int32_t* get_address_of_m_Action_0() { return &___m_Action_0; }
	inline void set_m_Action_0(int32_t value)
	{
		___m_Action_0 = value;
	}

	inline static int32_t get_offset_of_m_Unrestricted_1() { return static_cast<int32_t>(offsetof(SecurityAttribute_t9F1DB5B572D6E2D518A2F186A21522667521B195, ___m_Unrestricted_1)); }
	inline bool get_m_Unrestricted_1() const { return ___m_Unrestricted_1; }
	inline bool* get_address_of_m_Unrestricted_1() { return &___m_Unrestricted_1; }
	inline void set_m_Unrestricted_1(bool value)
	{
		___m_Unrestricted_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECURITYATTRIBUTE_T9F1DB5B572D6E2D518A2F186A21522667521B195_H
#ifndef READDELEGATE_TBC77AE628966A21E63D8BB344BC3D7C79441A6DE_H
#define READDELEGATE_TBC77AE628966A21E63D8BB344BC3D7C79441A6DE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.FtpDataStream_ReadDelegate
struct  ReadDelegate_tBC77AE628966A21E63D8BB344BC3D7C79441A6DE  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READDELEGATE_TBC77AE628966A21E63D8BB344BC3D7C79441A6DE_H
#ifndef WRITEDELEGATE_TCA763F3444D2578FB21239EDFC1C3632E469FC49_H
#define WRITEDELEGATE_TCA763F3444D2578FB21239EDFC1C3632E469FC49_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.FtpDataStream_WriteDelegate
struct  WriteDelegate_tCA763F3444D2578FB21239EDFC1C3632E469FC49  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WRITEDELEGATE_TCA763F3444D2578FB21239EDFC1C3632E469FC49_H
#ifndef FTPWEBREQUEST_T9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA_H
#define FTPWEBREQUEST_T9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.FtpWebRequest
struct  FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA  : public WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8
{
public:
	// System.Uri System.Net.FtpWebRequest::requestUri
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___requestUri_13;
	// System.String System.Net.FtpWebRequest::file_name
	String_t* ___file_name_14;
	// System.Net.ServicePoint System.Net.FtpWebRequest::servicePoint
	ServicePoint_t5F42B1A9D56E09B4B051BE0968C81DE3128E3EB4 * ___servicePoint_15;
	// System.IO.Stream System.Net.FtpWebRequest::origDataStream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___origDataStream_16;
	// System.IO.Stream System.Net.FtpWebRequest::dataStream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___dataStream_17;
	// System.IO.Stream System.Net.FtpWebRequest::controlStream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___controlStream_18;
	// System.IO.StreamReader System.Net.FtpWebRequest::controlReader
	StreamReader_t62E68063760DCD2FC036AE132DE69C24B7ED001E * ___controlReader_19;
	// System.Net.NetworkCredential System.Net.FtpWebRequest::credentials
	NetworkCredential_tA91C6E62EA0F0915E6E393F5DFD87D03FF2C3062 * ___credentials_20;
	// System.Net.IPHostEntry System.Net.FtpWebRequest::hostEntry
	IPHostEntry_tB00EABDF75DB19AEAD4F3E7D93BFD7BAE558149D * ___hostEntry_21;
	// System.Net.IPEndPoint System.Net.FtpWebRequest::localEndPoint
	IPEndPoint_tCD29981135F7B1989C3845BF455AD44EBC13DE3F * ___localEndPoint_22;
	// System.Net.IPEndPoint System.Net.FtpWebRequest::remoteEndPoint
	IPEndPoint_tCD29981135F7B1989C3845BF455AD44EBC13DE3F * ___remoteEndPoint_23;
	// System.Net.IWebProxy System.Net.FtpWebRequest::proxy
	RuntimeObject* ___proxy_24;
	// System.Int32 System.Net.FtpWebRequest::timeout
	int32_t ___timeout_25;
	// System.Int32 System.Net.FtpWebRequest::rwTimeout
	int32_t ___rwTimeout_26;
	// System.Int64 System.Net.FtpWebRequest::offset
	int64_t ___offset_27;
	// System.Boolean System.Net.FtpWebRequest::binary
	bool ___binary_28;
	// System.Boolean System.Net.FtpWebRequest::enableSsl
	bool ___enableSsl_29;
	// System.Boolean System.Net.FtpWebRequest::usePassive
	bool ___usePassive_30;
	// System.Boolean System.Net.FtpWebRequest::keepAlive
	bool ___keepAlive_31;
	// System.String System.Net.FtpWebRequest::method
	String_t* ___method_32;
	// System.String System.Net.FtpWebRequest::renameTo
	String_t* ___renameTo_33;
	// System.Object System.Net.FtpWebRequest::locker
	RuntimeObject * ___locker_34;
	// System.Net.FtpWebRequest_RequestState System.Net.FtpWebRequest::requestState
	int32_t ___requestState_35;
	// System.Net.FtpAsyncResult System.Net.FtpWebRequest::asyncResult
	FtpAsyncResult_tB318D495766A9449055B1D8C8C801095CF2CDEA3 * ___asyncResult_36;
	// System.Net.FtpWebResponse System.Net.FtpWebRequest::ftpResponse
	FtpWebResponse_t8775110950F0637C1A8A495892122865F05FC205 * ___ftpResponse_37;
	// System.IO.Stream System.Net.FtpWebRequest::requestStream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___requestStream_38;
	// System.String System.Net.FtpWebRequest::initial_path
	String_t* ___initial_path_39;
	// System.Text.Encoding System.Net.FtpWebRequest::dataEncoding
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___dataEncoding_56;

public:
	inline static int32_t get_offset_of_requestUri_13() { return static_cast<int32_t>(offsetof(FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA, ___requestUri_13)); }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * get_requestUri_13() const { return ___requestUri_13; }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E ** get_address_of_requestUri_13() { return &___requestUri_13; }
	inline void set_requestUri_13(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * value)
	{
		___requestUri_13 = value;
		Il2CppCodeGenWriteBarrier((&___requestUri_13), value);
	}

	inline static int32_t get_offset_of_file_name_14() { return static_cast<int32_t>(offsetof(FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA, ___file_name_14)); }
	inline String_t* get_file_name_14() const { return ___file_name_14; }
	inline String_t** get_address_of_file_name_14() { return &___file_name_14; }
	inline void set_file_name_14(String_t* value)
	{
		___file_name_14 = value;
		Il2CppCodeGenWriteBarrier((&___file_name_14), value);
	}

	inline static int32_t get_offset_of_servicePoint_15() { return static_cast<int32_t>(offsetof(FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA, ___servicePoint_15)); }
	inline ServicePoint_t5F42B1A9D56E09B4B051BE0968C81DE3128E3EB4 * get_servicePoint_15() const { return ___servicePoint_15; }
	inline ServicePoint_t5F42B1A9D56E09B4B051BE0968C81DE3128E3EB4 ** get_address_of_servicePoint_15() { return &___servicePoint_15; }
	inline void set_servicePoint_15(ServicePoint_t5F42B1A9D56E09B4B051BE0968C81DE3128E3EB4 * value)
	{
		___servicePoint_15 = value;
		Il2CppCodeGenWriteBarrier((&___servicePoint_15), value);
	}

	inline static int32_t get_offset_of_origDataStream_16() { return static_cast<int32_t>(offsetof(FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA, ___origDataStream_16)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_origDataStream_16() const { return ___origDataStream_16; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_origDataStream_16() { return &___origDataStream_16; }
	inline void set_origDataStream_16(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___origDataStream_16 = value;
		Il2CppCodeGenWriteBarrier((&___origDataStream_16), value);
	}

	inline static int32_t get_offset_of_dataStream_17() { return static_cast<int32_t>(offsetof(FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA, ___dataStream_17)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_dataStream_17() const { return ___dataStream_17; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_dataStream_17() { return &___dataStream_17; }
	inline void set_dataStream_17(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___dataStream_17 = value;
		Il2CppCodeGenWriteBarrier((&___dataStream_17), value);
	}

	inline static int32_t get_offset_of_controlStream_18() { return static_cast<int32_t>(offsetof(FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA, ___controlStream_18)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_controlStream_18() const { return ___controlStream_18; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_controlStream_18() { return &___controlStream_18; }
	inline void set_controlStream_18(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___controlStream_18 = value;
		Il2CppCodeGenWriteBarrier((&___controlStream_18), value);
	}

	inline static int32_t get_offset_of_controlReader_19() { return static_cast<int32_t>(offsetof(FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA, ___controlReader_19)); }
	inline StreamReader_t62E68063760DCD2FC036AE132DE69C24B7ED001E * get_controlReader_19() const { return ___controlReader_19; }
	inline StreamReader_t62E68063760DCD2FC036AE132DE69C24B7ED001E ** get_address_of_controlReader_19() { return &___controlReader_19; }
	inline void set_controlReader_19(StreamReader_t62E68063760DCD2FC036AE132DE69C24B7ED001E * value)
	{
		___controlReader_19 = value;
		Il2CppCodeGenWriteBarrier((&___controlReader_19), value);
	}

	inline static int32_t get_offset_of_credentials_20() { return static_cast<int32_t>(offsetof(FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA, ___credentials_20)); }
	inline NetworkCredential_tA91C6E62EA0F0915E6E393F5DFD87D03FF2C3062 * get_credentials_20() const { return ___credentials_20; }
	inline NetworkCredential_tA91C6E62EA0F0915E6E393F5DFD87D03FF2C3062 ** get_address_of_credentials_20() { return &___credentials_20; }
	inline void set_credentials_20(NetworkCredential_tA91C6E62EA0F0915E6E393F5DFD87D03FF2C3062 * value)
	{
		___credentials_20 = value;
		Il2CppCodeGenWriteBarrier((&___credentials_20), value);
	}

	inline static int32_t get_offset_of_hostEntry_21() { return static_cast<int32_t>(offsetof(FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA, ___hostEntry_21)); }
	inline IPHostEntry_tB00EABDF75DB19AEAD4F3E7D93BFD7BAE558149D * get_hostEntry_21() const { return ___hostEntry_21; }
	inline IPHostEntry_tB00EABDF75DB19AEAD4F3E7D93BFD7BAE558149D ** get_address_of_hostEntry_21() { return &___hostEntry_21; }
	inline void set_hostEntry_21(IPHostEntry_tB00EABDF75DB19AEAD4F3E7D93BFD7BAE558149D * value)
	{
		___hostEntry_21 = value;
		Il2CppCodeGenWriteBarrier((&___hostEntry_21), value);
	}

	inline static int32_t get_offset_of_localEndPoint_22() { return static_cast<int32_t>(offsetof(FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA, ___localEndPoint_22)); }
	inline IPEndPoint_tCD29981135F7B1989C3845BF455AD44EBC13DE3F * get_localEndPoint_22() const { return ___localEndPoint_22; }
	inline IPEndPoint_tCD29981135F7B1989C3845BF455AD44EBC13DE3F ** get_address_of_localEndPoint_22() { return &___localEndPoint_22; }
	inline void set_localEndPoint_22(IPEndPoint_tCD29981135F7B1989C3845BF455AD44EBC13DE3F * value)
	{
		___localEndPoint_22 = value;
		Il2CppCodeGenWriteBarrier((&___localEndPoint_22), value);
	}

	inline static int32_t get_offset_of_remoteEndPoint_23() { return static_cast<int32_t>(offsetof(FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA, ___remoteEndPoint_23)); }
	inline IPEndPoint_tCD29981135F7B1989C3845BF455AD44EBC13DE3F * get_remoteEndPoint_23() const { return ___remoteEndPoint_23; }
	inline IPEndPoint_tCD29981135F7B1989C3845BF455AD44EBC13DE3F ** get_address_of_remoteEndPoint_23() { return &___remoteEndPoint_23; }
	inline void set_remoteEndPoint_23(IPEndPoint_tCD29981135F7B1989C3845BF455AD44EBC13DE3F * value)
	{
		___remoteEndPoint_23 = value;
		Il2CppCodeGenWriteBarrier((&___remoteEndPoint_23), value);
	}

	inline static int32_t get_offset_of_proxy_24() { return static_cast<int32_t>(offsetof(FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA, ___proxy_24)); }
	inline RuntimeObject* get_proxy_24() const { return ___proxy_24; }
	inline RuntimeObject** get_address_of_proxy_24() { return &___proxy_24; }
	inline void set_proxy_24(RuntimeObject* value)
	{
		___proxy_24 = value;
		Il2CppCodeGenWriteBarrier((&___proxy_24), value);
	}

	inline static int32_t get_offset_of_timeout_25() { return static_cast<int32_t>(offsetof(FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA, ___timeout_25)); }
	inline int32_t get_timeout_25() const { return ___timeout_25; }
	inline int32_t* get_address_of_timeout_25() { return &___timeout_25; }
	inline void set_timeout_25(int32_t value)
	{
		___timeout_25 = value;
	}

	inline static int32_t get_offset_of_rwTimeout_26() { return static_cast<int32_t>(offsetof(FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA, ___rwTimeout_26)); }
	inline int32_t get_rwTimeout_26() const { return ___rwTimeout_26; }
	inline int32_t* get_address_of_rwTimeout_26() { return &___rwTimeout_26; }
	inline void set_rwTimeout_26(int32_t value)
	{
		___rwTimeout_26 = value;
	}

	inline static int32_t get_offset_of_offset_27() { return static_cast<int32_t>(offsetof(FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA, ___offset_27)); }
	inline int64_t get_offset_27() const { return ___offset_27; }
	inline int64_t* get_address_of_offset_27() { return &___offset_27; }
	inline void set_offset_27(int64_t value)
	{
		___offset_27 = value;
	}

	inline static int32_t get_offset_of_binary_28() { return static_cast<int32_t>(offsetof(FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA, ___binary_28)); }
	inline bool get_binary_28() const { return ___binary_28; }
	inline bool* get_address_of_binary_28() { return &___binary_28; }
	inline void set_binary_28(bool value)
	{
		___binary_28 = value;
	}

	inline static int32_t get_offset_of_enableSsl_29() { return static_cast<int32_t>(offsetof(FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA, ___enableSsl_29)); }
	inline bool get_enableSsl_29() const { return ___enableSsl_29; }
	inline bool* get_address_of_enableSsl_29() { return &___enableSsl_29; }
	inline void set_enableSsl_29(bool value)
	{
		___enableSsl_29 = value;
	}

	inline static int32_t get_offset_of_usePassive_30() { return static_cast<int32_t>(offsetof(FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA, ___usePassive_30)); }
	inline bool get_usePassive_30() const { return ___usePassive_30; }
	inline bool* get_address_of_usePassive_30() { return &___usePassive_30; }
	inline void set_usePassive_30(bool value)
	{
		___usePassive_30 = value;
	}

	inline static int32_t get_offset_of_keepAlive_31() { return static_cast<int32_t>(offsetof(FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA, ___keepAlive_31)); }
	inline bool get_keepAlive_31() const { return ___keepAlive_31; }
	inline bool* get_address_of_keepAlive_31() { return &___keepAlive_31; }
	inline void set_keepAlive_31(bool value)
	{
		___keepAlive_31 = value;
	}

	inline static int32_t get_offset_of_method_32() { return static_cast<int32_t>(offsetof(FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA, ___method_32)); }
	inline String_t* get_method_32() const { return ___method_32; }
	inline String_t** get_address_of_method_32() { return &___method_32; }
	inline void set_method_32(String_t* value)
	{
		___method_32 = value;
		Il2CppCodeGenWriteBarrier((&___method_32), value);
	}

	inline static int32_t get_offset_of_renameTo_33() { return static_cast<int32_t>(offsetof(FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA, ___renameTo_33)); }
	inline String_t* get_renameTo_33() const { return ___renameTo_33; }
	inline String_t** get_address_of_renameTo_33() { return &___renameTo_33; }
	inline void set_renameTo_33(String_t* value)
	{
		___renameTo_33 = value;
		Il2CppCodeGenWriteBarrier((&___renameTo_33), value);
	}

	inline static int32_t get_offset_of_locker_34() { return static_cast<int32_t>(offsetof(FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA, ___locker_34)); }
	inline RuntimeObject * get_locker_34() const { return ___locker_34; }
	inline RuntimeObject ** get_address_of_locker_34() { return &___locker_34; }
	inline void set_locker_34(RuntimeObject * value)
	{
		___locker_34 = value;
		Il2CppCodeGenWriteBarrier((&___locker_34), value);
	}

	inline static int32_t get_offset_of_requestState_35() { return static_cast<int32_t>(offsetof(FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA, ___requestState_35)); }
	inline int32_t get_requestState_35() const { return ___requestState_35; }
	inline int32_t* get_address_of_requestState_35() { return &___requestState_35; }
	inline void set_requestState_35(int32_t value)
	{
		___requestState_35 = value;
	}

	inline static int32_t get_offset_of_asyncResult_36() { return static_cast<int32_t>(offsetof(FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA, ___asyncResult_36)); }
	inline FtpAsyncResult_tB318D495766A9449055B1D8C8C801095CF2CDEA3 * get_asyncResult_36() const { return ___asyncResult_36; }
	inline FtpAsyncResult_tB318D495766A9449055B1D8C8C801095CF2CDEA3 ** get_address_of_asyncResult_36() { return &___asyncResult_36; }
	inline void set_asyncResult_36(FtpAsyncResult_tB318D495766A9449055B1D8C8C801095CF2CDEA3 * value)
	{
		___asyncResult_36 = value;
		Il2CppCodeGenWriteBarrier((&___asyncResult_36), value);
	}

	inline static int32_t get_offset_of_ftpResponse_37() { return static_cast<int32_t>(offsetof(FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA, ___ftpResponse_37)); }
	inline FtpWebResponse_t8775110950F0637C1A8A495892122865F05FC205 * get_ftpResponse_37() const { return ___ftpResponse_37; }
	inline FtpWebResponse_t8775110950F0637C1A8A495892122865F05FC205 ** get_address_of_ftpResponse_37() { return &___ftpResponse_37; }
	inline void set_ftpResponse_37(FtpWebResponse_t8775110950F0637C1A8A495892122865F05FC205 * value)
	{
		___ftpResponse_37 = value;
		Il2CppCodeGenWriteBarrier((&___ftpResponse_37), value);
	}

	inline static int32_t get_offset_of_requestStream_38() { return static_cast<int32_t>(offsetof(FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA, ___requestStream_38)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_requestStream_38() const { return ___requestStream_38; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_requestStream_38() { return &___requestStream_38; }
	inline void set_requestStream_38(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___requestStream_38 = value;
		Il2CppCodeGenWriteBarrier((&___requestStream_38), value);
	}

	inline static int32_t get_offset_of_initial_path_39() { return static_cast<int32_t>(offsetof(FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA, ___initial_path_39)); }
	inline String_t* get_initial_path_39() const { return ___initial_path_39; }
	inline String_t** get_address_of_initial_path_39() { return &___initial_path_39; }
	inline void set_initial_path_39(String_t* value)
	{
		___initial_path_39 = value;
		Il2CppCodeGenWriteBarrier((&___initial_path_39), value);
	}

	inline static int32_t get_offset_of_dataEncoding_56() { return static_cast<int32_t>(offsetof(FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA, ___dataEncoding_56)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get_dataEncoding_56() const { return ___dataEncoding_56; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of_dataEncoding_56() { return &___dataEncoding_56; }
	inline void set_dataEncoding_56(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		___dataEncoding_56 = value;
		Il2CppCodeGenWriteBarrier((&___dataEncoding_56), value);
	}
};

struct FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA_StaticFields
{
public:
	// System.String[] System.Net.FtpWebRequest::supportedCommands
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___supportedCommands_55;

public:
	inline static int32_t get_offset_of_supportedCommands_55() { return static_cast<int32_t>(offsetof(FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA_StaticFields, ___supportedCommands_55)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_supportedCommands_55() const { return ___supportedCommands_55; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_supportedCommands_55() { return &___supportedCommands_55; }
	inline void set_supportedCommands_55(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___supportedCommands_55 = value;
		Il2CppCodeGenWriteBarrier((&___supportedCommands_55), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FTPWEBREQUEST_T9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA_H
#ifndef EXTENDEDPROTECTIONSELECTOR_TC8A40F0217D1CF85224BBF5F398A081FA11C692C_H
#define EXTENDEDPROTECTIONSELECTOR_TC8A40F0217D1CF85224BBF5F398A081FA11C692C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HttpListener_ExtendedProtectionSelector
struct  ExtendedProtectionSelector_tC8A40F0217D1CF85224BBF5F398A081FA11C692C  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENDEDPROTECTIONSELECTOR_TC8A40F0217D1CF85224BBF5F398A081FA11C692C_H
#ifndef GCCDELEGATE_T2ED11BA19D14B2D5E74288CA763708D5E244EB46_H
#define GCCDELEGATE_T2ED11BA19D14B2D5E74288CA763708D5E244EB46_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HttpListenerRequest_GCCDelegate
struct  GCCDelegate_t2ED11BA19D14B2D5E74288CA763708D5E244EB46  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GCCDELEGATE_T2ED11BA19D14B2D5E74288CA763708D5E244EB46_H
#ifndef HTTPWEBREQUEST_T5B5BFA163B5AD6134620F315940CE3631D7FAAE0_H
#define HTTPWEBREQUEST_T5B5BFA163B5AD6134620F315940CE3631D7FAAE0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HttpWebRequest
struct  HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0  : public WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8
{
public:
	// System.Uri System.Net.HttpWebRequest::requestUri
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___requestUri_13;
	// System.Uri System.Net.HttpWebRequest::actualUri
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___actualUri_14;
	// System.Boolean System.Net.HttpWebRequest::hostChanged
	bool ___hostChanged_15;
	// System.Boolean System.Net.HttpWebRequest::allowAutoRedirect
	bool ___allowAutoRedirect_16;
	// System.Boolean System.Net.HttpWebRequest::allowBuffering
	bool ___allowBuffering_17;
	// System.Security.Cryptography.X509Certificates.X509CertificateCollection System.Net.HttpWebRequest::certificates
	X509CertificateCollection_t824A6C58D0D1B4A7CAE30F26CE8EE4B23A8A1833 * ___certificates_18;
	// System.String System.Net.HttpWebRequest::connectionGroup
	String_t* ___connectionGroup_19;
	// System.Boolean System.Net.HttpWebRequest::haveContentLength
	bool ___haveContentLength_20;
	// System.Int64 System.Net.HttpWebRequest::contentLength
	int64_t ___contentLength_21;
	// System.Net.HttpContinueDelegate System.Net.HttpWebRequest::continueDelegate
	HttpContinueDelegate_t38DB016AD9C4FA9F4E9B4417278FB8D0594F37AC * ___continueDelegate_22;
	// System.Net.CookieContainer System.Net.HttpWebRequest::cookieContainer
	CookieContainer_t7E062D04BAED9F3B30DDEC14B09660BB506A2A73 * ___cookieContainer_23;
	// System.Net.ICredentials System.Net.HttpWebRequest::credentials
	RuntimeObject* ___credentials_24;
	// System.Boolean System.Net.HttpWebRequest::haveResponse
	bool ___haveResponse_25;
	// System.Boolean System.Net.HttpWebRequest::haveRequest
	bool ___haveRequest_26;
	// System.Boolean System.Net.HttpWebRequest::requestSent
	bool ___requestSent_27;
	// System.Net.WebHeaderCollection System.Net.HttpWebRequest::webHeaders
	WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304 * ___webHeaders_28;
	// System.Boolean System.Net.HttpWebRequest::keepAlive
	bool ___keepAlive_29;
	// System.Int32 System.Net.HttpWebRequest::maxAutoRedirect
	int32_t ___maxAutoRedirect_30;
	// System.String System.Net.HttpWebRequest::mediaType
	String_t* ___mediaType_31;
	// System.String System.Net.HttpWebRequest::method
	String_t* ___method_32;
	// System.String System.Net.HttpWebRequest::initialMethod
	String_t* ___initialMethod_33;
	// System.Boolean System.Net.HttpWebRequest::pipelined
	bool ___pipelined_34;
	// System.Boolean System.Net.HttpWebRequest::preAuthenticate
	bool ___preAuthenticate_35;
	// System.Boolean System.Net.HttpWebRequest::usedPreAuth
	bool ___usedPreAuth_36;
	// System.Version System.Net.HttpWebRequest::version
	Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD * ___version_37;
	// System.Boolean System.Net.HttpWebRequest::force_version
	bool ___force_version_38;
	// System.Version System.Net.HttpWebRequest::actualVersion
	Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD * ___actualVersion_39;
	// System.Net.IWebProxy System.Net.HttpWebRequest::proxy
	RuntimeObject* ___proxy_40;
	// System.Boolean System.Net.HttpWebRequest::sendChunked
	bool ___sendChunked_41;
	// System.Net.ServicePoint System.Net.HttpWebRequest::servicePoint
	ServicePoint_t5F42B1A9D56E09B4B051BE0968C81DE3128E3EB4 * ___servicePoint_42;
	// System.Int32 System.Net.HttpWebRequest::timeout
	int32_t ___timeout_43;
	// System.Net.WebConnectionStream System.Net.HttpWebRequest::writeStream
	WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC * ___writeStream_44;
	// System.Net.HttpWebResponse System.Net.HttpWebRequest::webResponse
	HttpWebResponse_t34CF6A40A4748A0F8694FEFEA3723D9AE3EF3951 * ___webResponse_45;
	// System.Net.WebAsyncResult System.Net.HttpWebRequest::asyncWrite
	WebAsyncResult_tF700444B9ABA86C7CADBFA7B99DEC52D9FBD87EE * ___asyncWrite_46;
	// System.Net.WebAsyncResult System.Net.HttpWebRequest::asyncRead
	WebAsyncResult_tF700444B9ABA86C7CADBFA7B99DEC52D9FBD87EE * ___asyncRead_47;
	// System.EventHandler System.Net.HttpWebRequest::abortHandler
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * ___abortHandler_48;
	// System.Int32 System.Net.HttpWebRequest::aborted
	int32_t ___aborted_49;
	// System.Boolean System.Net.HttpWebRequest::gotRequestStream
	bool ___gotRequestStream_50;
	// System.Int32 System.Net.HttpWebRequest::redirects
	int32_t ___redirects_51;
	// System.Boolean System.Net.HttpWebRequest::expectContinue
	bool ___expectContinue_52;
	// System.Byte[] System.Net.HttpWebRequest::bodyBuffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___bodyBuffer_53;
	// System.Int32 System.Net.HttpWebRequest::bodyBufferLength
	int32_t ___bodyBufferLength_54;
	// System.Boolean System.Net.HttpWebRequest::getResponseCalled
	bool ___getResponseCalled_55;
	// System.Exception System.Net.HttpWebRequest::saved_exc
	Exception_t * ___saved_exc_56;
	// System.Object System.Net.HttpWebRequest::locker
	RuntimeObject * ___locker_57;
	// System.Boolean System.Net.HttpWebRequest::finished_reading
	bool ___finished_reading_58;
	// System.Net.WebConnection System.Net.HttpWebRequest::WebConnection
	WebConnection_tEB76AEE17361D28CBAD4033026A71DA89289C243 * ___WebConnection_59;
	// System.Net.DecompressionMethods System.Net.HttpWebRequest::auto_decomp
	int32_t ___auto_decomp_60;
	// System.Int32 System.Net.HttpWebRequest::maxResponseHeadersLength
	int32_t ___maxResponseHeadersLength_61;
	// System.Int32 System.Net.HttpWebRequest::readWriteTimeout
	int32_t ___readWriteTimeout_63;
	// Mono.Security.Interface.MonoTlsProvider System.Net.HttpWebRequest::tlsProvider
	MonoTlsProvider_tDCD056C5BBBE59ED6BAF63F25952B406C1143C27 * ___tlsProvider_64;
	// Mono.Security.Interface.MonoTlsSettings System.Net.HttpWebRequest::tlsSettings
	MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF * ___tlsSettings_65;
	// System.Net.ServerCertValidationCallback System.Net.HttpWebRequest::certValidationCallback
	ServerCertValidationCallback_t431E949AECAE20901007813737F5B26311F5F9FB * ___certValidationCallback_66;
	// System.Net.HttpWebRequest_AuthorizationState System.Net.HttpWebRequest::auth_state
	AuthorizationState_t5C342070F47B5DBAE0089B8B6A391FDEB6914AAB  ___auth_state_67;
	// System.Net.HttpWebRequest_AuthorizationState System.Net.HttpWebRequest::proxy_auth_state
	AuthorizationState_t5C342070F47B5DBAE0089B8B6A391FDEB6914AAB  ___proxy_auth_state_68;
	// System.String System.Net.HttpWebRequest::host
	String_t* ___host_69;
	// System.Action`1<System.IO.Stream> System.Net.HttpWebRequest::ResendContentFactory
	Action_1_tC8BAB6C7B8E5508F10B3A5EF475B0FFAE7688621 * ___ResendContentFactory_70;
	// System.Boolean System.Net.HttpWebRequest::<ThrowOnError>k__BackingField
	bool ___U3CThrowOnErrorU3Ek__BackingField_71;
	// System.Boolean System.Net.HttpWebRequest::unsafe_auth_blah
	bool ___unsafe_auth_blah_72;
	// System.Boolean System.Net.HttpWebRequest::<ReuseConnection>k__BackingField
	bool ___U3CReuseConnectionU3Ek__BackingField_73;
	// System.Net.WebConnection System.Net.HttpWebRequest::StoredConnection
	WebConnection_tEB76AEE17361D28CBAD4033026A71DA89289C243 * ___StoredConnection_74;

public:
	inline static int32_t get_offset_of_requestUri_13() { return static_cast<int32_t>(offsetof(HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0, ___requestUri_13)); }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * get_requestUri_13() const { return ___requestUri_13; }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E ** get_address_of_requestUri_13() { return &___requestUri_13; }
	inline void set_requestUri_13(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * value)
	{
		___requestUri_13 = value;
		Il2CppCodeGenWriteBarrier((&___requestUri_13), value);
	}

	inline static int32_t get_offset_of_actualUri_14() { return static_cast<int32_t>(offsetof(HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0, ___actualUri_14)); }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * get_actualUri_14() const { return ___actualUri_14; }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E ** get_address_of_actualUri_14() { return &___actualUri_14; }
	inline void set_actualUri_14(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * value)
	{
		___actualUri_14 = value;
		Il2CppCodeGenWriteBarrier((&___actualUri_14), value);
	}

	inline static int32_t get_offset_of_hostChanged_15() { return static_cast<int32_t>(offsetof(HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0, ___hostChanged_15)); }
	inline bool get_hostChanged_15() const { return ___hostChanged_15; }
	inline bool* get_address_of_hostChanged_15() { return &___hostChanged_15; }
	inline void set_hostChanged_15(bool value)
	{
		___hostChanged_15 = value;
	}

	inline static int32_t get_offset_of_allowAutoRedirect_16() { return static_cast<int32_t>(offsetof(HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0, ___allowAutoRedirect_16)); }
	inline bool get_allowAutoRedirect_16() const { return ___allowAutoRedirect_16; }
	inline bool* get_address_of_allowAutoRedirect_16() { return &___allowAutoRedirect_16; }
	inline void set_allowAutoRedirect_16(bool value)
	{
		___allowAutoRedirect_16 = value;
	}

	inline static int32_t get_offset_of_allowBuffering_17() { return static_cast<int32_t>(offsetof(HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0, ___allowBuffering_17)); }
	inline bool get_allowBuffering_17() const { return ___allowBuffering_17; }
	inline bool* get_address_of_allowBuffering_17() { return &___allowBuffering_17; }
	inline void set_allowBuffering_17(bool value)
	{
		___allowBuffering_17 = value;
	}

	inline static int32_t get_offset_of_certificates_18() { return static_cast<int32_t>(offsetof(HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0, ___certificates_18)); }
	inline X509CertificateCollection_t824A6C58D0D1B4A7CAE30F26CE8EE4B23A8A1833 * get_certificates_18() const { return ___certificates_18; }
	inline X509CertificateCollection_t824A6C58D0D1B4A7CAE30F26CE8EE4B23A8A1833 ** get_address_of_certificates_18() { return &___certificates_18; }
	inline void set_certificates_18(X509CertificateCollection_t824A6C58D0D1B4A7CAE30F26CE8EE4B23A8A1833 * value)
	{
		___certificates_18 = value;
		Il2CppCodeGenWriteBarrier((&___certificates_18), value);
	}

	inline static int32_t get_offset_of_connectionGroup_19() { return static_cast<int32_t>(offsetof(HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0, ___connectionGroup_19)); }
	inline String_t* get_connectionGroup_19() const { return ___connectionGroup_19; }
	inline String_t** get_address_of_connectionGroup_19() { return &___connectionGroup_19; }
	inline void set_connectionGroup_19(String_t* value)
	{
		___connectionGroup_19 = value;
		Il2CppCodeGenWriteBarrier((&___connectionGroup_19), value);
	}

	inline static int32_t get_offset_of_haveContentLength_20() { return static_cast<int32_t>(offsetof(HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0, ___haveContentLength_20)); }
	inline bool get_haveContentLength_20() const { return ___haveContentLength_20; }
	inline bool* get_address_of_haveContentLength_20() { return &___haveContentLength_20; }
	inline void set_haveContentLength_20(bool value)
	{
		___haveContentLength_20 = value;
	}

	inline static int32_t get_offset_of_contentLength_21() { return static_cast<int32_t>(offsetof(HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0, ___contentLength_21)); }
	inline int64_t get_contentLength_21() const { return ___contentLength_21; }
	inline int64_t* get_address_of_contentLength_21() { return &___contentLength_21; }
	inline void set_contentLength_21(int64_t value)
	{
		___contentLength_21 = value;
	}

	inline static int32_t get_offset_of_continueDelegate_22() { return static_cast<int32_t>(offsetof(HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0, ___continueDelegate_22)); }
	inline HttpContinueDelegate_t38DB016AD9C4FA9F4E9B4417278FB8D0594F37AC * get_continueDelegate_22() const { return ___continueDelegate_22; }
	inline HttpContinueDelegate_t38DB016AD9C4FA9F4E9B4417278FB8D0594F37AC ** get_address_of_continueDelegate_22() { return &___continueDelegate_22; }
	inline void set_continueDelegate_22(HttpContinueDelegate_t38DB016AD9C4FA9F4E9B4417278FB8D0594F37AC * value)
	{
		___continueDelegate_22 = value;
		Il2CppCodeGenWriteBarrier((&___continueDelegate_22), value);
	}

	inline static int32_t get_offset_of_cookieContainer_23() { return static_cast<int32_t>(offsetof(HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0, ___cookieContainer_23)); }
	inline CookieContainer_t7E062D04BAED9F3B30DDEC14B09660BB506A2A73 * get_cookieContainer_23() const { return ___cookieContainer_23; }
	inline CookieContainer_t7E062D04BAED9F3B30DDEC14B09660BB506A2A73 ** get_address_of_cookieContainer_23() { return &___cookieContainer_23; }
	inline void set_cookieContainer_23(CookieContainer_t7E062D04BAED9F3B30DDEC14B09660BB506A2A73 * value)
	{
		___cookieContainer_23 = value;
		Il2CppCodeGenWriteBarrier((&___cookieContainer_23), value);
	}

	inline static int32_t get_offset_of_credentials_24() { return static_cast<int32_t>(offsetof(HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0, ___credentials_24)); }
	inline RuntimeObject* get_credentials_24() const { return ___credentials_24; }
	inline RuntimeObject** get_address_of_credentials_24() { return &___credentials_24; }
	inline void set_credentials_24(RuntimeObject* value)
	{
		___credentials_24 = value;
		Il2CppCodeGenWriteBarrier((&___credentials_24), value);
	}

	inline static int32_t get_offset_of_haveResponse_25() { return static_cast<int32_t>(offsetof(HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0, ___haveResponse_25)); }
	inline bool get_haveResponse_25() const { return ___haveResponse_25; }
	inline bool* get_address_of_haveResponse_25() { return &___haveResponse_25; }
	inline void set_haveResponse_25(bool value)
	{
		___haveResponse_25 = value;
	}

	inline static int32_t get_offset_of_haveRequest_26() { return static_cast<int32_t>(offsetof(HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0, ___haveRequest_26)); }
	inline bool get_haveRequest_26() const { return ___haveRequest_26; }
	inline bool* get_address_of_haveRequest_26() { return &___haveRequest_26; }
	inline void set_haveRequest_26(bool value)
	{
		___haveRequest_26 = value;
	}

	inline static int32_t get_offset_of_requestSent_27() { return static_cast<int32_t>(offsetof(HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0, ___requestSent_27)); }
	inline bool get_requestSent_27() const { return ___requestSent_27; }
	inline bool* get_address_of_requestSent_27() { return &___requestSent_27; }
	inline void set_requestSent_27(bool value)
	{
		___requestSent_27 = value;
	}

	inline static int32_t get_offset_of_webHeaders_28() { return static_cast<int32_t>(offsetof(HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0, ___webHeaders_28)); }
	inline WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304 * get_webHeaders_28() const { return ___webHeaders_28; }
	inline WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304 ** get_address_of_webHeaders_28() { return &___webHeaders_28; }
	inline void set_webHeaders_28(WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304 * value)
	{
		___webHeaders_28 = value;
		Il2CppCodeGenWriteBarrier((&___webHeaders_28), value);
	}

	inline static int32_t get_offset_of_keepAlive_29() { return static_cast<int32_t>(offsetof(HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0, ___keepAlive_29)); }
	inline bool get_keepAlive_29() const { return ___keepAlive_29; }
	inline bool* get_address_of_keepAlive_29() { return &___keepAlive_29; }
	inline void set_keepAlive_29(bool value)
	{
		___keepAlive_29 = value;
	}

	inline static int32_t get_offset_of_maxAutoRedirect_30() { return static_cast<int32_t>(offsetof(HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0, ___maxAutoRedirect_30)); }
	inline int32_t get_maxAutoRedirect_30() const { return ___maxAutoRedirect_30; }
	inline int32_t* get_address_of_maxAutoRedirect_30() { return &___maxAutoRedirect_30; }
	inline void set_maxAutoRedirect_30(int32_t value)
	{
		___maxAutoRedirect_30 = value;
	}

	inline static int32_t get_offset_of_mediaType_31() { return static_cast<int32_t>(offsetof(HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0, ___mediaType_31)); }
	inline String_t* get_mediaType_31() const { return ___mediaType_31; }
	inline String_t** get_address_of_mediaType_31() { return &___mediaType_31; }
	inline void set_mediaType_31(String_t* value)
	{
		___mediaType_31 = value;
		Il2CppCodeGenWriteBarrier((&___mediaType_31), value);
	}

	inline static int32_t get_offset_of_method_32() { return static_cast<int32_t>(offsetof(HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0, ___method_32)); }
	inline String_t* get_method_32() const { return ___method_32; }
	inline String_t** get_address_of_method_32() { return &___method_32; }
	inline void set_method_32(String_t* value)
	{
		___method_32 = value;
		Il2CppCodeGenWriteBarrier((&___method_32), value);
	}

	inline static int32_t get_offset_of_initialMethod_33() { return static_cast<int32_t>(offsetof(HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0, ___initialMethod_33)); }
	inline String_t* get_initialMethod_33() const { return ___initialMethod_33; }
	inline String_t** get_address_of_initialMethod_33() { return &___initialMethod_33; }
	inline void set_initialMethod_33(String_t* value)
	{
		___initialMethod_33 = value;
		Il2CppCodeGenWriteBarrier((&___initialMethod_33), value);
	}

	inline static int32_t get_offset_of_pipelined_34() { return static_cast<int32_t>(offsetof(HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0, ___pipelined_34)); }
	inline bool get_pipelined_34() const { return ___pipelined_34; }
	inline bool* get_address_of_pipelined_34() { return &___pipelined_34; }
	inline void set_pipelined_34(bool value)
	{
		___pipelined_34 = value;
	}

	inline static int32_t get_offset_of_preAuthenticate_35() { return static_cast<int32_t>(offsetof(HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0, ___preAuthenticate_35)); }
	inline bool get_preAuthenticate_35() const { return ___preAuthenticate_35; }
	inline bool* get_address_of_preAuthenticate_35() { return &___preAuthenticate_35; }
	inline void set_preAuthenticate_35(bool value)
	{
		___preAuthenticate_35 = value;
	}

	inline static int32_t get_offset_of_usedPreAuth_36() { return static_cast<int32_t>(offsetof(HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0, ___usedPreAuth_36)); }
	inline bool get_usedPreAuth_36() const { return ___usedPreAuth_36; }
	inline bool* get_address_of_usedPreAuth_36() { return &___usedPreAuth_36; }
	inline void set_usedPreAuth_36(bool value)
	{
		___usedPreAuth_36 = value;
	}

	inline static int32_t get_offset_of_version_37() { return static_cast<int32_t>(offsetof(HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0, ___version_37)); }
	inline Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD * get_version_37() const { return ___version_37; }
	inline Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD ** get_address_of_version_37() { return &___version_37; }
	inline void set_version_37(Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD * value)
	{
		___version_37 = value;
		Il2CppCodeGenWriteBarrier((&___version_37), value);
	}

	inline static int32_t get_offset_of_force_version_38() { return static_cast<int32_t>(offsetof(HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0, ___force_version_38)); }
	inline bool get_force_version_38() const { return ___force_version_38; }
	inline bool* get_address_of_force_version_38() { return &___force_version_38; }
	inline void set_force_version_38(bool value)
	{
		___force_version_38 = value;
	}

	inline static int32_t get_offset_of_actualVersion_39() { return static_cast<int32_t>(offsetof(HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0, ___actualVersion_39)); }
	inline Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD * get_actualVersion_39() const { return ___actualVersion_39; }
	inline Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD ** get_address_of_actualVersion_39() { return &___actualVersion_39; }
	inline void set_actualVersion_39(Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD * value)
	{
		___actualVersion_39 = value;
		Il2CppCodeGenWriteBarrier((&___actualVersion_39), value);
	}

	inline static int32_t get_offset_of_proxy_40() { return static_cast<int32_t>(offsetof(HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0, ___proxy_40)); }
	inline RuntimeObject* get_proxy_40() const { return ___proxy_40; }
	inline RuntimeObject** get_address_of_proxy_40() { return &___proxy_40; }
	inline void set_proxy_40(RuntimeObject* value)
	{
		___proxy_40 = value;
		Il2CppCodeGenWriteBarrier((&___proxy_40), value);
	}

	inline static int32_t get_offset_of_sendChunked_41() { return static_cast<int32_t>(offsetof(HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0, ___sendChunked_41)); }
	inline bool get_sendChunked_41() const { return ___sendChunked_41; }
	inline bool* get_address_of_sendChunked_41() { return &___sendChunked_41; }
	inline void set_sendChunked_41(bool value)
	{
		___sendChunked_41 = value;
	}

	inline static int32_t get_offset_of_servicePoint_42() { return static_cast<int32_t>(offsetof(HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0, ___servicePoint_42)); }
	inline ServicePoint_t5F42B1A9D56E09B4B051BE0968C81DE3128E3EB4 * get_servicePoint_42() const { return ___servicePoint_42; }
	inline ServicePoint_t5F42B1A9D56E09B4B051BE0968C81DE3128E3EB4 ** get_address_of_servicePoint_42() { return &___servicePoint_42; }
	inline void set_servicePoint_42(ServicePoint_t5F42B1A9D56E09B4B051BE0968C81DE3128E3EB4 * value)
	{
		___servicePoint_42 = value;
		Il2CppCodeGenWriteBarrier((&___servicePoint_42), value);
	}

	inline static int32_t get_offset_of_timeout_43() { return static_cast<int32_t>(offsetof(HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0, ___timeout_43)); }
	inline int32_t get_timeout_43() const { return ___timeout_43; }
	inline int32_t* get_address_of_timeout_43() { return &___timeout_43; }
	inline void set_timeout_43(int32_t value)
	{
		___timeout_43 = value;
	}

	inline static int32_t get_offset_of_writeStream_44() { return static_cast<int32_t>(offsetof(HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0, ___writeStream_44)); }
	inline WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC * get_writeStream_44() const { return ___writeStream_44; }
	inline WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC ** get_address_of_writeStream_44() { return &___writeStream_44; }
	inline void set_writeStream_44(WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC * value)
	{
		___writeStream_44 = value;
		Il2CppCodeGenWriteBarrier((&___writeStream_44), value);
	}

	inline static int32_t get_offset_of_webResponse_45() { return static_cast<int32_t>(offsetof(HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0, ___webResponse_45)); }
	inline HttpWebResponse_t34CF6A40A4748A0F8694FEFEA3723D9AE3EF3951 * get_webResponse_45() const { return ___webResponse_45; }
	inline HttpWebResponse_t34CF6A40A4748A0F8694FEFEA3723D9AE3EF3951 ** get_address_of_webResponse_45() { return &___webResponse_45; }
	inline void set_webResponse_45(HttpWebResponse_t34CF6A40A4748A0F8694FEFEA3723D9AE3EF3951 * value)
	{
		___webResponse_45 = value;
		Il2CppCodeGenWriteBarrier((&___webResponse_45), value);
	}

	inline static int32_t get_offset_of_asyncWrite_46() { return static_cast<int32_t>(offsetof(HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0, ___asyncWrite_46)); }
	inline WebAsyncResult_tF700444B9ABA86C7CADBFA7B99DEC52D9FBD87EE * get_asyncWrite_46() const { return ___asyncWrite_46; }
	inline WebAsyncResult_tF700444B9ABA86C7CADBFA7B99DEC52D9FBD87EE ** get_address_of_asyncWrite_46() { return &___asyncWrite_46; }
	inline void set_asyncWrite_46(WebAsyncResult_tF700444B9ABA86C7CADBFA7B99DEC52D9FBD87EE * value)
	{
		___asyncWrite_46 = value;
		Il2CppCodeGenWriteBarrier((&___asyncWrite_46), value);
	}

	inline static int32_t get_offset_of_asyncRead_47() { return static_cast<int32_t>(offsetof(HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0, ___asyncRead_47)); }
	inline WebAsyncResult_tF700444B9ABA86C7CADBFA7B99DEC52D9FBD87EE * get_asyncRead_47() const { return ___asyncRead_47; }
	inline WebAsyncResult_tF700444B9ABA86C7CADBFA7B99DEC52D9FBD87EE ** get_address_of_asyncRead_47() { return &___asyncRead_47; }
	inline void set_asyncRead_47(WebAsyncResult_tF700444B9ABA86C7CADBFA7B99DEC52D9FBD87EE * value)
	{
		___asyncRead_47 = value;
		Il2CppCodeGenWriteBarrier((&___asyncRead_47), value);
	}

	inline static int32_t get_offset_of_abortHandler_48() { return static_cast<int32_t>(offsetof(HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0, ___abortHandler_48)); }
	inline EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * get_abortHandler_48() const { return ___abortHandler_48; }
	inline EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C ** get_address_of_abortHandler_48() { return &___abortHandler_48; }
	inline void set_abortHandler_48(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * value)
	{
		___abortHandler_48 = value;
		Il2CppCodeGenWriteBarrier((&___abortHandler_48), value);
	}

	inline static int32_t get_offset_of_aborted_49() { return static_cast<int32_t>(offsetof(HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0, ___aborted_49)); }
	inline int32_t get_aborted_49() const { return ___aborted_49; }
	inline int32_t* get_address_of_aborted_49() { return &___aborted_49; }
	inline void set_aborted_49(int32_t value)
	{
		___aborted_49 = value;
	}

	inline static int32_t get_offset_of_gotRequestStream_50() { return static_cast<int32_t>(offsetof(HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0, ___gotRequestStream_50)); }
	inline bool get_gotRequestStream_50() const { return ___gotRequestStream_50; }
	inline bool* get_address_of_gotRequestStream_50() { return &___gotRequestStream_50; }
	inline void set_gotRequestStream_50(bool value)
	{
		___gotRequestStream_50 = value;
	}

	inline static int32_t get_offset_of_redirects_51() { return static_cast<int32_t>(offsetof(HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0, ___redirects_51)); }
	inline int32_t get_redirects_51() const { return ___redirects_51; }
	inline int32_t* get_address_of_redirects_51() { return &___redirects_51; }
	inline void set_redirects_51(int32_t value)
	{
		___redirects_51 = value;
	}

	inline static int32_t get_offset_of_expectContinue_52() { return static_cast<int32_t>(offsetof(HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0, ___expectContinue_52)); }
	inline bool get_expectContinue_52() const { return ___expectContinue_52; }
	inline bool* get_address_of_expectContinue_52() { return &___expectContinue_52; }
	inline void set_expectContinue_52(bool value)
	{
		___expectContinue_52 = value;
	}

	inline static int32_t get_offset_of_bodyBuffer_53() { return static_cast<int32_t>(offsetof(HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0, ___bodyBuffer_53)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_bodyBuffer_53() const { return ___bodyBuffer_53; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_bodyBuffer_53() { return &___bodyBuffer_53; }
	inline void set_bodyBuffer_53(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___bodyBuffer_53 = value;
		Il2CppCodeGenWriteBarrier((&___bodyBuffer_53), value);
	}

	inline static int32_t get_offset_of_bodyBufferLength_54() { return static_cast<int32_t>(offsetof(HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0, ___bodyBufferLength_54)); }
	inline int32_t get_bodyBufferLength_54() const { return ___bodyBufferLength_54; }
	inline int32_t* get_address_of_bodyBufferLength_54() { return &___bodyBufferLength_54; }
	inline void set_bodyBufferLength_54(int32_t value)
	{
		___bodyBufferLength_54 = value;
	}

	inline static int32_t get_offset_of_getResponseCalled_55() { return static_cast<int32_t>(offsetof(HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0, ___getResponseCalled_55)); }
	inline bool get_getResponseCalled_55() const { return ___getResponseCalled_55; }
	inline bool* get_address_of_getResponseCalled_55() { return &___getResponseCalled_55; }
	inline void set_getResponseCalled_55(bool value)
	{
		___getResponseCalled_55 = value;
	}

	inline static int32_t get_offset_of_saved_exc_56() { return static_cast<int32_t>(offsetof(HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0, ___saved_exc_56)); }
	inline Exception_t * get_saved_exc_56() const { return ___saved_exc_56; }
	inline Exception_t ** get_address_of_saved_exc_56() { return &___saved_exc_56; }
	inline void set_saved_exc_56(Exception_t * value)
	{
		___saved_exc_56 = value;
		Il2CppCodeGenWriteBarrier((&___saved_exc_56), value);
	}

	inline static int32_t get_offset_of_locker_57() { return static_cast<int32_t>(offsetof(HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0, ___locker_57)); }
	inline RuntimeObject * get_locker_57() const { return ___locker_57; }
	inline RuntimeObject ** get_address_of_locker_57() { return &___locker_57; }
	inline void set_locker_57(RuntimeObject * value)
	{
		___locker_57 = value;
		Il2CppCodeGenWriteBarrier((&___locker_57), value);
	}

	inline static int32_t get_offset_of_finished_reading_58() { return static_cast<int32_t>(offsetof(HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0, ___finished_reading_58)); }
	inline bool get_finished_reading_58() const { return ___finished_reading_58; }
	inline bool* get_address_of_finished_reading_58() { return &___finished_reading_58; }
	inline void set_finished_reading_58(bool value)
	{
		___finished_reading_58 = value;
	}

	inline static int32_t get_offset_of_WebConnection_59() { return static_cast<int32_t>(offsetof(HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0, ___WebConnection_59)); }
	inline WebConnection_tEB76AEE17361D28CBAD4033026A71DA89289C243 * get_WebConnection_59() const { return ___WebConnection_59; }
	inline WebConnection_tEB76AEE17361D28CBAD4033026A71DA89289C243 ** get_address_of_WebConnection_59() { return &___WebConnection_59; }
	inline void set_WebConnection_59(WebConnection_tEB76AEE17361D28CBAD4033026A71DA89289C243 * value)
	{
		___WebConnection_59 = value;
		Il2CppCodeGenWriteBarrier((&___WebConnection_59), value);
	}

	inline static int32_t get_offset_of_auto_decomp_60() { return static_cast<int32_t>(offsetof(HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0, ___auto_decomp_60)); }
	inline int32_t get_auto_decomp_60() const { return ___auto_decomp_60; }
	inline int32_t* get_address_of_auto_decomp_60() { return &___auto_decomp_60; }
	inline void set_auto_decomp_60(int32_t value)
	{
		___auto_decomp_60 = value;
	}

	inline static int32_t get_offset_of_maxResponseHeadersLength_61() { return static_cast<int32_t>(offsetof(HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0, ___maxResponseHeadersLength_61)); }
	inline int32_t get_maxResponseHeadersLength_61() const { return ___maxResponseHeadersLength_61; }
	inline int32_t* get_address_of_maxResponseHeadersLength_61() { return &___maxResponseHeadersLength_61; }
	inline void set_maxResponseHeadersLength_61(int32_t value)
	{
		___maxResponseHeadersLength_61 = value;
	}

	inline static int32_t get_offset_of_readWriteTimeout_63() { return static_cast<int32_t>(offsetof(HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0, ___readWriteTimeout_63)); }
	inline int32_t get_readWriteTimeout_63() const { return ___readWriteTimeout_63; }
	inline int32_t* get_address_of_readWriteTimeout_63() { return &___readWriteTimeout_63; }
	inline void set_readWriteTimeout_63(int32_t value)
	{
		___readWriteTimeout_63 = value;
	}

	inline static int32_t get_offset_of_tlsProvider_64() { return static_cast<int32_t>(offsetof(HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0, ___tlsProvider_64)); }
	inline MonoTlsProvider_tDCD056C5BBBE59ED6BAF63F25952B406C1143C27 * get_tlsProvider_64() const { return ___tlsProvider_64; }
	inline MonoTlsProvider_tDCD056C5BBBE59ED6BAF63F25952B406C1143C27 ** get_address_of_tlsProvider_64() { return &___tlsProvider_64; }
	inline void set_tlsProvider_64(MonoTlsProvider_tDCD056C5BBBE59ED6BAF63F25952B406C1143C27 * value)
	{
		___tlsProvider_64 = value;
		Il2CppCodeGenWriteBarrier((&___tlsProvider_64), value);
	}

	inline static int32_t get_offset_of_tlsSettings_65() { return static_cast<int32_t>(offsetof(HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0, ___tlsSettings_65)); }
	inline MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF * get_tlsSettings_65() const { return ___tlsSettings_65; }
	inline MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF ** get_address_of_tlsSettings_65() { return &___tlsSettings_65; }
	inline void set_tlsSettings_65(MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF * value)
	{
		___tlsSettings_65 = value;
		Il2CppCodeGenWriteBarrier((&___tlsSettings_65), value);
	}

	inline static int32_t get_offset_of_certValidationCallback_66() { return static_cast<int32_t>(offsetof(HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0, ___certValidationCallback_66)); }
	inline ServerCertValidationCallback_t431E949AECAE20901007813737F5B26311F5F9FB * get_certValidationCallback_66() const { return ___certValidationCallback_66; }
	inline ServerCertValidationCallback_t431E949AECAE20901007813737F5B26311F5F9FB ** get_address_of_certValidationCallback_66() { return &___certValidationCallback_66; }
	inline void set_certValidationCallback_66(ServerCertValidationCallback_t431E949AECAE20901007813737F5B26311F5F9FB * value)
	{
		___certValidationCallback_66 = value;
		Il2CppCodeGenWriteBarrier((&___certValidationCallback_66), value);
	}

	inline static int32_t get_offset_of_auth_state_67() { return static_cast<int32_t>(offsetof(HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0, ___auth_state_67)); }
	inline AuthorizationState_t5C342070F47B5DBAE0089B8B6A391FDEB6914AAB  get_auth_state_67() const { return ___auth_state_67; }
	inline AuthorizationState_t5C342070F47B5DBAE0089B8B6A391FDEB6914AAB * get_address_of_auth_state_67() { return &___auth_state_67; }
	inline void set_auth_state_67(AuthorizationState_t5C342070F47B5DBAE0089B8B6A391FDEB6914AAB  value)
	{
		___auth_state_67 = value;
	}

	inline static int32_t get_offset_of_proxy_auth_state_68() { return static_cast<int32_t>(offsetof(HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0, ___proxy_auth_state_68)); }
	inline AuthorizationState_t5C342070F47B5DBAE0089B8B6A391FDEB6914AAB  get_proxy_auth_state_68() const { return ___proxy_auth_state_68; }
	inline AuthorizationState_t5C342070F47B5DBAE0089B8B6A391FDEB6914AAB * get_address_of_proxy_auth_state_68() { return &___proxy_auth_state_68; }
	inline void set_proxy_auth_state_68(AuthorizationState_t5C342070F47B5DBAE0089B8B6A391FDEB6914AAB  value)
	{
		___proxy_auth_state_68 = value;
	}

	inline static int32_t get_offset_of_host_69() { return static_cast<int32_t>(offsetof(HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0, ___host_69)); }
	inline String_t* get_host_69() const { return ___host_69; }
	inline String_t** get_address_of_host_69() { return &___host_69; }
	inline void set_host_69(String_t* value)
	{
		___host_69 = value;
		Il2CppCodeGenWriteBarrier((&___host_69), value);
	}

	inline static int32_t get_offset_of_ResendContentFactory_70() { return static_cast<int32_t>(offsetof(HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0, ___ResendContentFactory_70)); }
	inline Action_1_tC8BAB6C7B8E5508F10B3A5EF475B0FFAE7688621 * get_ResendContentFactory_70() const { return ___ResendContentFactory_70; }
	inline Action_1_tC8BAB6C7B8E5508F10B3A5EF475B0FFAE7688621 ** get_address_of_ResendContentFactory_70() { return &___ResendContentFactory_70; }
	inline void set_ResendContentFactory_70(Action_1_tC8BAB6C7B8E5508F10B3A5EF475B0FFAE7688621 * value)
	{
		___ResendContentFactory_70 = value;
		Il2CppCodeGenWriteBarrier((&___ResendContentFactory_70), value);
	}

	inline static int32_t get_offset_of_U3CThrowOnErrorU3Ek__BackingField_71() { return static_cast<int32_t>(offsetof(HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0, ___U3CThrowOnErrorU3Ek__BackingField_71)); }
	inline bool get_U3CThrowOnErrorU3Ek__BackingField_71() const { return ___U3CThrowOnErrorU3Ek__BackingField_71; }
	inline bool* get_address_of_U3CThrowOnErrorU3Ek__BackingField_71() { return &___U3CThrowOnErrorU3Ek__BackingField_71; }
	inline void set_U3CThrowOnErrorU3Ek__BackingField_71(bool value)
	{
		___U3CThrowOnErrorU3Ek__BackingField_71 = value;
	}

	inline static int32_t get_offset_of_unsafe_auth_blah_72() { return static_cast<int32_t>(offsetof(HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0, ___unsafe_auth_blah_72)); }
	inline bool get_unsafe_auth_blah_72() const { return ___unsafe_auth_blah_72; }
	inline bool* get_address_of_unsafe_auth_blah_72() { return &___unsafe_auth_blah_72; }
	inline void set_unsafe_auth_blah_72(bool value)
	{
		___unsafe_auth_blah_72 = value;
	}

	inline static int32_t get_offset_of_U3CReuseConnectionU3Ek__BackingField_73() { return static_cast<int32_t>(offsetof(HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0, ___U3CReuseConnectionU3Ek__BackingField_73)); }
	inline bool get_U3CReuseConnectionU3Ek__BackingField_73() const { return ___U3CReuseConnectionU3Ek__BackingField_73; }
	inline bool* get_address_of_U3CReuseConnectionU3Ek__BackingField_73() { return &___U3CReuseConnectionU3Ek__BackingField_73; }
	inline void set_U3CReuseConnectionU3Ek__BackingField_73(bool value)
	{
		___U3CReuseConnectionU3Ek__BackingField_73 = value;
	}

	inline static int32_t get_offset_of_StoredConnection_74() { return static_cast<int32_t>(offsetof(HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0, ___StoredConnection_74)); }
	inline WebConnection_tEB76AEE17361D28CBAD4033026A71DA89289C243 * get_StoredConnection_74() const { return ___StoredConnection_74; }
	inline WebConnection_tEB76AEE17361D28CBAD4033026A71DA89289C243 ** get_address_of_StoredConnection_74() { return &___StoredConnection_74; }
	inline void set_StoredConnection_74(WebConnection_tEB76AEE17361D28CBAD4033026A71DA89289C243 * value)
	{
		___StoredConnection_74 = value;
		Il2CppCodeGenWriteBarrier((&___StoredConnection_74), value);
	}
};

struct HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0_StaticFields
{
public:
	// System.Int32 System.Net.HttpWebRequest::defaultMaxResponseHeadersLength
	int32_t ___defaultMaxResponseHeadersLength_62;

public:
	inline static int32_t get_offset_of_defaultMaxResponseHeadersLength_62() { return static_cast<int32_t>(offsetof(HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0_StaticFields, ___defaultMaxResponseHeadersLength_62)); }
	inline int32_t get_defaultMaxResponseHeadersLength_62() const { return ___defaultMaxResponseHeadersLength_62; }
	inline int32_t* get_address_of_defaultMaxResponseHeadersLength_62() { return &___defaultMaxResponseHeadersLength_62; }
	inline void set_defaultMaxResponseHeadersLength_62(int32_t value)
	{
		___defaultMaxResponseHeadersLength_62 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPWEBREQUEST_T5B5BFA163B5AD6134620F315940CE3631D7FAAE0_H
#ifndef SIMPLEASYNCCALLBACK_T690665AFDCBDEBA379B6F4CD213CB4BB8570F83F_H
#define SIMPLEASYNCCALLBACK_T690665AFDCBDEBA379B6F4CD213CB4BB8570F83F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.SimpleAsyncCallback
struct  SimpleAsyncCallback_t690665AFDCBDEBA379B6F4CD213CB4BB8570F83F  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLEASYNCCALLBACK_T690665AFDCBDEBA379B6F4CD213CB4BB8570F83F_H
#ifndef SAFESOCKETHANDLE_T9A33B4DCE2012075A5D6D355D323A05E7F16329A_H
#define SAFESOCKETHANDLE_T9A33B4DCE2012075A5D6D355D323A05E7F16329A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Sockets.SafeSocketHandle
struct  SafeSocketHandle_t9A33B4DCE2012075A5D6D355D323A05E7F16329A  : public SafeHandleZeroOrMinusOneIsInvalid_t779A965C82098677DF1ED10A134DBCDEC8AACB8E
{
public:
	// System.Collections.Generic.List`1<System.Threading.Thread> System.Net.Sockets.SafeSocketHandle::blocking_threads
	List_1_t584FFF54C798BF7768C5EA483641BADEC0CB963D * ___blocking_threads_6;
	// System.Collections.Generic.Dictionary`2<System.Threading.Thread,System.Diagnostics.StackTrace> System.Net.Sockets.SafeSocketHandle::threads_stacktraces
	Dictionary_2_t2B4A575938F12185D62CE7B381FC75D09F004B17 * ___threads_stacktraces_7;
	// System.Boolean System.Net.Sockets.SafeSocketHandle::in_cleanup
	bool ___in_cleanup_8;

public:
	inline static int32_t get_offset_of_blocking_threads_6() { return static_cast<int32_t>(offsetof(SafeSocketHandle_t9A33B4DCE2012075A5D6D355D323A05E7F16329A, ___blocking_threads_6)); }
	inline List_1_t584FFF54C798BF7768C5EA483641BADEC0CB963D * get_blocking_threads_6() const { return ___blocking_threads_6; }
	inline List_1_t584FFF54C798BF7768C5EA483641BADEC0CB963D ** get_address_of_blocking_threads_6() { return &___blocking_threads_6; }
	inline void set_blocking_threads_6(List_1_t584FFF54C798BF7768C5EA483641BADEC0CB963D * value)
	{
		___blocking_threads_6 = value;
		Il2CppCodeGenWriteBarrier((&___blocking_threads_6), value);
	}

	inline static int32_t get_offset_of_threads_stacktraces_7() { return static_cast<int32_t>(offsetof(SafeSocketHandle_t9A33B4DCE2012075A5D6D355D323A05E7F16329A, ___threads_stacktraces_7)); }
	inline Dictionary_2_t2B4A575938F12185D62CE7B381FC75D09F004B17 * get_threads_stacktraces_7() const { return ___threads_stacktraces_7; }
	inline Dictionary_2_t2B4A575938F12185D62CE7B381FC75D09F004B17 ** get_address_of_threads_stacktraces_7() { return &___threads_stacktraces_7; }
	inline void set_threads_stacktraces_7(Dictionary_2_t2B4A575938F12185D62CE7B381FC75D09F004B17 * value)
	{
		___threads_stacktraces_7 = value;
		Il2CppCodeGenWriteBarrier((&___threads_stacktraces_7), value);
	}

	inline static int32_t get_offset_of_in_cleanup_8() { return static_cast<int32_t>(offsetof(SafeSocketHandle_t9A33B4DCE2012075A5D6D355D323A05E7F16329A, ___in_cleanup_8)); }
	inline bool get_in_cleanup_8() const { return ___in_cleanup_8; }
	inline bool* get_address_of_in_cleanup_8() { return &___in_cleanup_8; }
	inline void set_in_cleanup_8(bool value)
	{
		___in_cleanup_8 = value;
	}
};

struct SafeSocketHandle_t9A33B4DCE2012075A5D6D355D323A05E7F16329A_StaticFields
{
public:
	// System.Boolean System.Net.Sockets.SafeSocketHandle::THROW_ON_ABORT_RETRIES
	bool ___THROW_ON_ABORT_RETRIES_9;

public:
	inline static int32_t get_offset_of_THROW_ON_ABORT_RETRIES_9() { return static_cast<int32_t>(offsetof(SafeSocketHandle_t9A33B4DCE2012075A5D6D355D323A05E7F16329A_StaticFields, ___THROW_ON_ABORT_RETRIES_9)); }
	inline bool get_THROW_ON_ABORT_RETRIES_9() const { return ___THROW_ON_ABORT_RETRIES_9; }
	inline bool* get_address_of_THROW_ON_ABORT_RETRIES_9() { return &___THROW_ON_ABORT_RETRIES_9; }
	inline void set_THROW_ON_ABORT_RETRIES_9(bool value)
	{
		___THROW_ON_ABORT_RETRIES_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAFESOCKETHANDLE_T9A33B4DCE2012075A5D6D355D323A05E7F16329A_H
#ifndef SOCKETEXCEPTION_T75481CF49BCAF5685A5A9E6933909E0B65E7E0A5_H
#define SOCKETEXCEPTION_T75481CF49BCAF5685A5A9E6933909E0B65E7E0A5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Sockets.SocketException
struct  SocketException_t75481CF49BCAF5685A5A9E6933909E0B65E7E0A5  : public Win32Exception_tB05BE97AB4CADD54DF96C0109689F0ECA7517668
{
public:
	// System.Net.EndPoint System.Net.Sockets.SocketException::m_EndPoint
	EndPoint_tD87FCEF2780A951E8CE8D808C345FBF2C088D980 * ___m_EndPoint_20;

public:
	inline static int32_t get_offset_of_m_EndPoint_20() { return static_cast<int32_t>(offsetof(SocketException_t75481CF49BCAF5685A5A9E6933909E0B65E7E0A5, ___m_EndPoint_20)); }
	inline EndPoint_tD87FCEF2780A951E8CE8D808C345FBF2C088D980 * get_m_EndPoint_20() const { return ___m_EndPoint_20; }
	inline EndPoint_tD87FCEF2780A951E8CE8D808C345FBF2C088D980 ** get_address_of_m_EndPoint_20() { return &___m_EndPoint_20; }
	inline void set_m_EndPoint_20(EndPoint_tD87FCEF2780A951E8CE8D808C345FBF2C088D980 * value)
	{
		___m_EndPoint_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_EndPoint_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOCKETEXCEPTION_T75481CF49BCAF5685A5A9E6933909E0B65E7E0A5_H
#ifndef CODEACCESSSECURITYATTRIBUTE_TBBF0EFA8FE4C0ED2D8E2469B658D1CDDFF8850E6_H
#define CODEACCESSSECURITYATTRIBUTE_TBBF0EFA8FE4C0ED2D8E2469B658D1CDDFF8850E6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Permissions.CodeAccessSecurityAttribute
struct  CodeAccessSecurityAttribute_tBBF0EFA8FE4C0ED2D8E2469B658D1CDDFF8850E6  : public SecurityAttribute_t9F1DB5B572D6E2D518A2F186A21522667521B195
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CODEACCESSSECURITYATTRIBUTE_TBBF0EFA8FE4C0ED2D8E2469B658D1CDDFF8850E6_H
#ifndef SOCKETPERMISSIONATTRIBUTE_T770C87915AD66FF25AE25E1B008B644177F50E64_H
#define SOCKETPERMISSIONATTRIBUTE_T770C87915AD66FF25AE25E1B008B644177F50E64_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.SocketPermissionAttribute
struct  SocketPermissionAttribute_t770C87915AD66FF25AE25E1B008B644177F50E64  : public CodeAccessSecurityAttribute_tBBF0EFA8FE4C0ED2D8E2469B658D1CDDFF8850E6
{
public:
	// System.String System.Net.SocketPermissionAttribute::m_access
	String_t* ___m_access_2;
	// System.String System.Net.SocketPermissionAttribute::m_host
	String_t* ___m_host_3;
	// System.String System.Net.SocketPermissionAttribute::m_port
	String_t* ___m_port_4;
	// System.String System.Net.SocketPermissionAttribute::m_transport
	String_t* ___m_transport_5;

public:
	inline static int32_t get_offset_of_m_access_2() { return static_cast<int32_t>(offsetof(SocketPermissionAttribute_t770C87915AD66FF25AE25E1B008B644177F50E64, ___m_access_2)); }
	inline String_t* get_m_access_2() const { return ___m_access_2; }
	inline String_t** get_address_of_m_access_2() { return &___m_access_2; }
	inline void set_m_access_2(String_t* value)
	{
		___m_access_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_access_2), value);
	}

	inline static int32_t get_offset_of_m_host_3() { return static_cast<int32_t>(offsetof(SocketPermissionAttribute_t770C87915AD66FF25AE25E1B008B644177F50E64, ___m_host_3)); }
	inline String_t* get_m_host_3() const { return ___m_host_3; }
	inline String_t** get_address_of_m_host_3() { return &___m_host_3; }
	inline void set_m_host_3(String_t* value)
	{
		___m_host_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_host_3), value);
	}

	inline static int32_t get_offset_of_m_port_4() { return static_cast<int32_t>(offsetof(SocketPermissionAttribute_t770C87915AD66FF25AE25E1B008B644177F50E64, ___m_port_4)); }
	inline String_t* get_m_port_4() const { return ___m_port_4; }
	inline String_t** get_address_of_m_port_4() { return &___m_port_4; }
	inline void set_m_port_4(String_t* value)
	{
		___m_port_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_port_4), value);
	}

	inline static int32_t get_offset_of_m_transport_5() { return static_cast<int32_t>(offsetof(SocketPermissionAttribute_t770C87915AD66FF25AE25E1B008B644177F50E64, ___m_transport_5)); }
	inline String_t* get_m_transport_5() const { return ___m_transport_5; }
	inline String_t** get_address_of_m_transport_5() { return &___m_transport_5; }
	inline void set_m_transport_5(String_t* value)
	{
		___m_transport_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_transport_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOCKETPERMISSIONATTRIBUTE_T770C87915AD66FF25AE25E1B008B644177F50E64_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3100 = { sizeof (EndPointListener_tF1D36E022EAFCD075CD0398D2868363AA9FB10C2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3100[9] = 
{
	EndPointListener_tF1D36E022EAFCD075CD0398D2868363AA9FB10C2::get_offset_of_listener_0(),
	EndPointListener_tF1D36E022EAFCD075CD0398D2868363AA9FB10C2::get_offset_of_endpoint_1(),
	EndPointListener_tF1D36E022EAFCD075CD0398D2868363AA9FB10C2::get_offset_of_sock_2(),
	EndPointListener_tF1D36E022EAFCD075CD0398D2868363AA9FB10C2::get_offset_of_prefixes_3(),
	EndPointListener_tF1D36E022EAFCD075CD0398D2868363AA9FB10C2::get_offset_of_unhandled_4(),
	EndPointListener_tF1D36E022EAFCD075CD0398D2868363AA9FB10C2::get_offset_of_all_5(),
	EndPointListener_tF1D36E022EAFCD075CD0398D2868363AA9FB10C2::get_offset_of_cert_6(),
	EndPointListener_tF1D36E022EAFCD075CD0398D2868363AA9FB10C2::get_offset_of_secure_7(),
	EndPointListener_tF1D36E022EAFCD075CD0398D2868363AA9FB10C2::get_offset_of_unregistered_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3101 = { sizeof (EndPointManager_tEA1F3FD50367B1217FA4DF7FEED29A42D70D71A2), -1, sizeof(EndPointManager_tEA1F3FD50367B1217FA4DF7FEED29A42D70D71A2_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3101[1] = 
{
	EndPointManager_tEA1F3FD50367B1217FA4DF7FEED29A42D70D71A2_StaticFields::get_offset_of_ip_to_endpoints_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3102 = { sizeof (EndpointPermission_tA34E0AFD9B907F84A1B9ECF8E247696D1EEFC0BA), -1, sizeof(EndpointPermission_tA34E0AFD9B907F84A1B9ECF8E247696D1EEFC0BA_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3102[7] = 
{
	EndpointPermission_tA34E0AFD9B907F84A1B9ECF8E247696D1EEFC0BA_StaticFields::get_offset_of_dot_char_0(),
	EndpointPermission_tA34E0AFD9B907F84A1B9ECF8E247696D1EEFC0BA::get_offset_of_hostname_1(),
	EndpointPermission_tA34E0AFD9B907F84A1B9ECF8E247696D1EEFC0BA::get_offset_of_port_2(),
	EndpointPermission_tA34E0AFD9B907F84A1B9ECF8E247696D1EEFC0BA::get_offset_of_transport_3(),
	EndpointPermission_tA34E0AFD9B907F84A1B9ECF8E247696D1EEFC0BA::get_offset_of_resolved_4(),
	EndpointPermission_tA34E0AFD9B907F84A1B9ECF8E247696D1EEFC0BA::get_offset_of_hasWildcard_5(),
	EndpointPermission_tA34E0AFD9B907F84A1B9ECF8E247696D1EEFC0BA::get_offset_of_addresses_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3103 = { sizeof (FtpAsyncResult_tB318D495766A9449055B1D8C8C801095CF2CDEA3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3103[9] = 
{
	FtpAsyncResult_tB318D495766A9449055B1D8C8C801095CF2CDEA3::get_offset_of_response_0(),
	FtpAsyncResult_tB318D495766A9449055B1D8C8C801095CF2CDEA3::get_offset_of_waitHandle_1(),
	FtpAsyncResult_tB318D495766A9449055B1D8C8C801095CF2CDEA3::get_offset_of_exception_2(),
	FtpAsyncResult_tB318D495766A9449055B1D8C8C801095CF2CDEA3::get_offset_of_callback_3(),
	FtpAsyncResult_tB318D495766A9449055B1D8C8C801095CF2CDEA3::get_offset_of_stream_4(),
	FtpAsyncResult_tB318D495766A9449055B1D8C8C801095CF2CDEA3::get_offset_of_state_5(),
	FtpAsyncResult_tB318D495766A9449055B1D8C8C801095CF2CDEA3::get_offset_of_completed_6(),
	FtpAsyncResult_tB318D495766A9449055B1D8C8C801095CF2CDEA3::get_offset_of_synch_7(),
	FtpAsyncResult_tB318D495766A9449055B1D8C8C801095CF2CDEA3::get_offset_of_locker_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3104 = { sizeof (FtpDataStream_tBF423F55CA0947ED2BF909BEA79DA349338DD3B1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3104[5] = 
{
	FtpDataStream_tBF423F55CA0947ED2BF909BEA79DA349338DD3B1::get_offset_of_request_5(),
	FtpDataStream_tBF423F55CA0947ED2BF909BEA79DA349338DD3B1::get_offset_of_networkStream_6(),
	FtpDataStream_tBF423F55CA0947ED2BF909BEA79DA349338DD3B1::get_offset_of_disposed_7(),
	FtpDataStream_tBF423F55CA0947ED2BF909BEA79DA349338DD3B1::get_offset_of_isRead_8(),
	FtpDataStream_tBF423F55CA0947ED2BF909BEA79DA349338DD3B1::get_offset_of_totalRead_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3105 = { sizeof (WriteDelegate_tCA763F3444D2578FB21239EDFC1C3632E469FC49), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3106 = { sizeof (ReadDelegate_tBC77AE628966A21E63D8BB344BC3D7C79441A6DE), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3107 = { sizeof (FtpRequestCreator_t2C5CC32221C790FB648AF6276DA861B4ABAC357F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3108 = { sizeof (FtpStatus_tC736CA78D396A33659145A9183F15038E66B2876), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3108[2] = 
{
	FtpStatus_tC736CA78D396A33659145A9183F15038E66B2876::get_offset_of_statusCode_0(),
	FtpStatus_tC736CA78D396A33659145A9183F15038E66B2876::get_offset_of_statusDescription_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3109 = { sizeof (FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA), -1, sizeof(FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3109[44] = 
{
	FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA::get_offset_of_requestUri_13(),
	FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA::get_offset_of_file_name_14(),
	FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA::get_offset_of_servicePoint_15(),
	FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA::get_offset_of_origDataStream_16(),
	FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA::get_offset_of_dataStream_17(),
	FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA::get_offset_of_controlStream_18(),
	FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA::get_offset_of_controlReader_19(),
	FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA::get_offset_of_credentials_20(),
	FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA::get_offset_of_hostEntry_21(),
	FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA::get_offset_of_localEndPoint_22(),
	FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA::get_offset_of_remoteEndPoint_23(),
	FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA::get_offset_of_proxy_24(),
	FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA::get_offset_of_timeout_25(),
	FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA::get_offset_of_rwTimeout_26(),
	FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA::get_offset_of_offset_27(),
	FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA::get_offset_of_binary_28(),
	FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA::get_offset_of_enableSsl_29(),
	FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA::get_offset_of_usePassive_30(),
	FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA::get_offset_of_keepAlive_31(),
	FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA::get_offset_of_method_32(),
	FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA::get_offset_of_renameTo_33(),
	FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA::get_offset_of_locker_34(),
	FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA::get_offset_of_requestState_35(),
	FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA::get_offset_of_asyncResult_36(),
	FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA::get_offset_of_ftpResponse_37(),
	FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA::get_offset_of_requestStream_38(),
	FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA::get_offset_of_initial_path_39(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA_StaticFields::get_offset_of_supportedCommands_55(),
	FtpWebRequest_t9444EC84DC89CB60CB8AA1FEEBA55408CCDC51BA::get_offset_of_dataEncoding_56(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3110 = { sizeof (RequestState_t850C56F50136642DB235E32D764586B31C248731)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3110[10] = 
{
	RequestState_t850C56F50136642DB235E32D764586B31C248731::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3111 = { sizeof (FtpWebResponse_t8775110950F0637C1A8A495892122865F05FC205), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3111[12] = 
{
	FtpWebResponse_t8775110950F0637C1A8A495892122865F05FC205::get_offset_of_stream_3(),
	FtpWebResponse_t8775110950F0637C1A8A495892122865F05FC205::get_offset_of_uri_4(),
	FtpWebResponse_t8775110950F0637C1A8A495892122865F05FC205::get_offset_of_statusCode_5(),
	FtpWebResponse_t8775110950F0637C1A8A495892122865F05FC205::get_offset_of_lastModified_6(),
	FtpWebResponse_t8775110950F0637C1A8A495892122865F05FC205::get_offset_of_bannerMessage_7(),
	FtpWebResponse_t8775110950F0637C1A8A495892122865F05FC205::get_offset_of_welcomeMessage_8(),
	FtpWebResponse_t8775110950F0637C1A8A495892122865F05FC205::get_offset_of_exitMessage_9(),
	FtpWebResponse_t8775110950F0637C1A8A495892122865F05FC205::get_offset_of_statusDescription_10(),
	FtpWebResponse_t8775110950F0637C1A8A495892122865F05FC205::get_offset_of_method_11(),
	FtpWebResponse_t8775110950F0637C1A8A495892122865F05FC205::get_offset_of_disposed_12(),
	FtpWebResponse_t8775110950F0637C1A8A495892122865F05FC205::get_offset_of_request_13(),
	FtpWebResponse_t8775110950F0637C1A8A495892122865F05FC205::get_offset_of_contentLength_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3112 = { sizeof (HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E), -1, sizeof(HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3112[27] = 
{
	HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E_StaticFields::get_offset_of_onread_cb_0(),
	0,
	HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E::get_offset_of_sock_2(),
	HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E::get_offset_of_stream_3(),
	HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E::get_offset_of_epl_4(),
	HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E::get_offset_of_ms_5(),
	HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E::get_offset_of_buffer_6(),
	HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E::get_offset_of_context_7(),
	HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E::get_offset_of_current_line_8(),
	HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E::get_offset_of_prefix_9(),
	HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E::get_offset_of_i_stream_10(),
	HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E::get_offset_of_o_stream_11(),
	HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E::get_offset_of_chunked_12(),
	HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E::get_offset_of_reuses_13(),
	HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E::get_offset_of_context_bound_14(),
	HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E::get_offset_of_secure_15(),
	HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E::get_offset_of_cert_16(),
	HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E::get_offset_of_s_timeout_17(),
	HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E::get_offset_of_timer_18(),
	HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E::get_offset_of_local_ep_19(),
	HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E::get_offset_of_last_listener_20(),
	HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E::get_offset_of_client_cert_errors_21(),
	HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E::get_offset_of_client_cert_22(),
	HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E::get_offset_of_ssl_stream_23(),
	HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E::get_offset_of_input_state_24(),
	HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E::get_offset_of_line_state_25(),
	HttpConnection_tD93CEDEFFD0DCBC29F9B704487A0A25D065D466E::get_offset_of_position_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3113 = { sizeof (InputState_tC9C07C35B1B422E948BD606C0AF277DFDAD48D86)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3113[3] = 
{
	InputState_tC9C07C35B1B422E948BD606C0AF277DFDAD48D86::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3114 = { sizeof (LineState_tE99D1029981D06DF9C2614BBDEA270C09E2EA666)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3114[4] = 
{
	LineState_tE99D1029981D06DF9C2614BBDEA270C09E2EA666::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3115 = { sizeof (HttpListener_tAAE9DCF34EB3C09285BF833F390C8CC61C7D247E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3115[19] = 
{
	HttpListener_tAAE9DCF34EB3C09285BF833F390C8CC61C7D247E::get_offset_of_tlsProvider_0(),
	HttpListener_tAAE9DCF34EB3C09285BF833F390C8CC61C7D247E::get_offset_of_tlsSettings_1(),
	HttpListener_tAAE9DCF34EB3C09285BF833F390C8CC61C7D247E::get_offset_of_certificate_2(),
	HttpListener_tAAE9DCF34EB3C09285BF833F390C8CC61C7D247E::get_offset_of_auth_schemes_3(),
	HttpListener_tAAE9DCF34EB3C09285BF833F390C8CC61C7D247E::get_offset_of_prefixes_4(),
	HttpListener_tAAE9DCF34EB3C09285BF833F390C8CC61C7D247E::get_offset_of_auth_selector_5(),
	HttpListener_tAAE9DCF34EB3C09285BF833F390C8CC61C7D247E::get_offset_of_realm_6(),
	HttpListener_tAAE9DCF34EB3C09285BF833F390C8CC61C7D247E::get_offset_of_ignore_write_exceptions_7(),
	HttpListener_tAAE9DCF34EB3C09285BF833F390C8CC61C7D247E::get_offset_of_unsafe_ntlm_auth_8(),
	HttpListener_tAAE9DCF34EB3C09285BF833F390C8CC61C7D247E::get_offset_of_listening_9(),
	HttpListener_tAAE9DCF34EB3C09285BF833F390C8CC61C7D247E::get_offset_of_disposed_10(),
	HttpListener_tAAE9DCF34EB3C09285BF833F390C8CC61C7D247E::get_offset_of__internalLock_11(),
	HttpListener_tAAE9DCF34EB3C09285BF833F390C8CC61C7D247E::get_offset_of_registry_12(),
	HttpListener_tAAE9DCF34EB3C09285BF833F390C8CC61C7D247E::get_offset_of_ctx_queue_13(),
	HttpListener_tAAE9DCF34EB3C09285BF833F390C8CC61C7D247E::get_offset_of_wait_queue_14(),
	HttpListener_tAAE9DCF34EB3C09285BF833F390C8CC61C7D247E::get_offset_of_connections_15(),
	HttpListener_tAAE9DCF34EB3C09285BF833F390C8CC61C7D247E::get_offset_of_defaultServiceNames_16(),
	HttpListener_tAAE9DCF34EB3C09285BF833F390C8CC61C7D247E::get_offset_of_extendedProtectionPolicy_17(),
	HttpListener_tAAE9DCF34EB3C09285BF833F390C8CC61C7D247E::get_offset_of_extendedProtectionSelectorDelegate_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3116 = { sizeof (ExtendedProtectionSelector_tC8A40F0217D1CF85224BBF5F398A081FA11C692C), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3117 = { sizeof (HttpListenerBasicIdentity_t12A25C542707784381BC1A66CCB3344D98EA9398), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3117[1] = 
{
	HttpListenerBasicIdentity_t12A25C542707784381BC1A66CCB3344D98EA9398::get_offset_of_password_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3118 = { sizeof (HttpListenerContext_t438DA3609B8D775884622DF392BD7D72C651FCFA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3118[7] = 
{
	HttpListenerContext_t438DA3609B8D775884622DF392BD7D72C651FCFA::get_offset_of_request_0(),
	HttpListenerContext_t438DA3609B8D775884622DF392BD7D72C651FCFA::get_offset_of_response_1(),
	HttpListenerContext_t438DA3609B8D775884622DF392BD7D72C651FCFA::get_offset_of_user_2(),
	HttpListenerContext_t438DA3609B8D775884622DF392BD7D72C651FCFA::get_offset_of_cnc_3(),
	HttpListenerContext_t438DA3609B8D775884622DF392BD7D72C651FCFA::get_offset_of_error_4(),
	HttpListenerContext_t438DA3609B8D775884622DF392BD7D72C651FCFA::get_offset_of_err_status_5(),
	HttpListenerContext_t438DA3609B8D775884622DF392BD7D72C651FCFA::get_offset_of_Listener_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3119 = { sizeof (HttpListenerPrefixCollection_tCFDDEB0773E99D9DA834AB2FBF1AF0F2B1AC7B5A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3119[2] = 
{
	HttpListenerPrefixCollection_tCFDDEB0773E99D9DA834AB2FBF1AF0F2B1AC7B5A::get_offset_of_prefixes_0(),
	HttpListenerPrefixCollection_tCFDDEB0773E99D9DA834AB2FBF1AF0F2B1AC7B5A::get_offset_of_listener_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3120 = { sizeof (HttpListenerRequest_t4BD625F3BF4726345B12F4FF06DB23A5932FD8CE), -1, sizeof(HttpListenerRequest_t4BD625F3BF4726345B12F4FF06DB23A5932FD8CE_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3120[21] = 
{
	HttpListenerRequest_t4BD625F3BF4726345B12F4FF06DB23A5932FD8CE::get_offset_of_accept_types_0(),
	HttpListenerRequest_t4BD625F3BF4726345B12F4FF06DB23A5932FD8CE::get_offset_of_content_encoding_1(),
	HttpListenerRequest_t4BD625F3BF4726345B12F4FF06DB23A5932FD8CE::get_offset_of_content_length_2(),
	HttpListenerRequest_t4BD625F3BF4726345B12F4FF06DB23A5932FD8CE::get_offset_of_cl_set_3(),
	HttpListenerRequest_t4BD625F3BF4726345B12F4FF06DB23A5932FD8CE::get_offset_of_cookies_4(),
	HttpListenerRequest_t4BD625F3BF4726345B12F4FF06DB23A5932FD8CE::get_offset_of_headers_5(),
	HttpListenerRequest_t4BD625F3BF4726345B12F4FF06DB23A5932FD8CE::get_offset_of_method_6(),
	HttpListenerRequest_t4BD625F3BF4726345B12F4FF06DB23A5932FD8CE::get_offset_of_input_stream_7(),
	HttpListenerRequest_t4BD625F3BF4726345B12F4FF06DB23A5932FD8CE::get_offset_of_version_8(),
	HttpListenerRequest_t4BD625F3BF4726345B12F4FF06DB23A5932FD8CE::get_offset_of_query_string_9(),
	HttpListenerRequest_t4BD625F3BF4726345B12F4FF06DB23A5932FD8CE::get_offset_of_raw_url_10(),
	HttpListenerRequest_t4BD625F3BF4726345B12F4FF06DB23A5932FD8CE::get_offset_of_url_11(),
	HttpListenerRequest_t4BD625F3BF4726345B12F4FF06DB23A5932FD8CE::get_offset_of_referrer_12(),
	HttpListenerRequest_t4BD625F3BF4726345B12F4FF06DB23A5932FD8CE::get_offset_of_user_languages_13(),
	HttpListenerRequest_t4BD625F3BF4726345B12F4FF06DB23A5932FD8CE::get_offset_of_context_14(),
	HttpListenerRequest_t4BD625F3BF4726345B12F4FF06DB23A5932FD8CE::get_offset_of_is_chunked_15(),
	HttpListenerRequest_t4BD625F3BF4726345B12F4FF06DB23A5932FD8CE::get_offset_of_ka_set_16(),
	HttpListenerRequest_t4BD625F3BF4726345B12F4FF06DB23A5932FD8CE::get_offset_of_keep_alive_17(),
	HttpListenerRequest_t4BD625F3BF4726345B12F4FF06DB23A5932FD8CE::get_offset_of_gcc_delegate_18(),
	HttpListenerRequest_t4BD625F3BF4726345B12F4FF06DB23A5932FD8CE_StaticFields::get_offset_of__100continue_19(),
	HttpListenerRequest_t4BD625F3BF4726345B12F4FF06DB23A5932FD8CE_StaticFields::get_offset_of_separators_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3121 = { sizeof (Context_t125FB4B035D650236F7DBF42DAC5D58D85FAC512), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3122 = { sizeof (GCCDelegate_t2ED11BA19D14B2D5E74288CA763708D5E244EB46), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3123 = { sizeof (HttpListenerResponse_t34AC9D467928315CC4285B8A14517C6D5564224C), -1, sizeof(HttpListenerResponse_t34AC9D467928315CC4285B8A14517C6D5564224C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3123[19] = 
{
	HttpListenerResponse_t34AC9D467928315CC4285B8A14517C6D5564224C::get_offset_of_disposed_0(),
	HttpListenerResponse_t34AC9D467928315CC4285B8A14517C6D5564224C::get_offset_of_content_encoding_1(),
	HttpListenerResponse_t34AC9D467928315CC4285B8A14517C6D5564224C::get_offset_of_content_length_2(),
	HttpListenerResponse_t34AC9D467928315CC4285B8A14517C6D5564224C::get_offset_of_cl_set_3(),
	HttpListenerResponse_t34AC9D467928315CC4285B8A14517C6D5564224C::get_offset_of_content_type_4(),
	HttpListenerResponse_t34AC9D467928315CC4285B8A14517C6D5564224C::get_offset_of_cookies_5(),
	HttpListenerResponse_t34AC9D467928315CC4285B8A14517C6D5564224C::get_offset_of_headers_6(),
	HttpListenerResponse_t34AC9D467928315CC4285B8A14517C6D5564224C::get_offset_of_keep_alive_7(),
	HttpListenerResponse_t34AC9D467928315CC4285B8A14517C6D5564224C::get_offset_of_output_stream_8(),
	HttpListenerResponse_t34AC9D467928315CC4285B8A14517C6D5564224C::get_offset_of_version_9(),
	HttpListenerResponse_t34AC9D467928315CC4285B8A14517C6D5564224C::get_offset_of_location_10(),
	HttpListenerResponse_t34AC9D467928315CC4285B8A14517C6D5564224C::get_offset_of_status_code_11(),
	HttpListenerResponse_t34AC9D467928315CC4285B8A14517C6D5564224C::get_offset_of_status_description_12(),
	HttpListenerResponse_t34AC9D467928315CC4285B8A14517C6D5564224C::get_offset_of_chunked_13(),
	HttpListenerResponse_t34AC9D467928315CC4285B8A14517C6D5564224C::get_offset_of_context_14(),
	HttpListenerResponse_t34AC9D467928315CC4285B8A14517C6D5564224C::get_offset_of_HeadersSent_15(),
	HttpListenerResponse_t34AC9D467928315CC4285B8A14517C6D5564224C::get_offset_of_headers_lock_16(),
	HttpListenerResponse_t34AC9D467928315CC4285B8A14517C6D5564224C::get_offset_of_force_close_chunked_17(),
	HttpListenerResponse_t34AC9D467928315CC4285B8A14517C6D5564224C_StaticFields::get_offset_of_tspecials_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3124 = { sizeof (HttpListenerTimeoutManager_t081C4ACA445B361CDF394A18CDD6171E1110C3EA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3125 = { sizeof (HttpRequestCreator_tE16C19B09EAACE12BEBA0427796314523601EB1D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3126 = { sizeof (HttpStreamAsyncResult_t3CCE017E175E89575780F29152E61D6DE5D10271), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3126[10] = 
{
	HttpStreamAsyncResult_t3CCE017E175E89575780F29152E61D6DE5D10271::get_offset_of_locker_0(),
	HttpStreamAsyncResult_t3CCE017E175E89575780F29152E61D6DE5D10271::get_offset_of_handle_1(),
	HttpStreamAsyncResult_t3CCE017E175E89575780F29152E61D6DE5D10271::get_offset_of_completed_2(),
	HttpStreamAsyncResult_t3CCE017E175E89575780F29152E61D6DE5D10271::get_offset_of_Buffer_3(),
	HttpStreamAsyncResult_t3CCE017E175E89575780F29152E61D6DE5D10271::get_offset_of_Offset_4(),
	HttpStreamAsyncResult_t3CCE017E175E89575780F29152E61D6DE5D10271::get_offset_of_Count_5(),
	HttpStreamAsyncResult_t3CCE017E175E89575780F29152E61D6DE5D10271::get_offset_of_Callback_6(),
	HttpStreamAsyncResult_t3CCE017E175E89575780F29152E61D6DE5D10271::get_offset_of_State_7(),
	HttpStreamAsyncResult_t3CCE017E175E89575780F29152E61D6DE5D10271::get_offset_of_SynchRead_8(),
	HttpStreamAsyncResult_t3CCE017E175E89575780F29152E61D6DE5D10271::get_offset_of_Error_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3127 = { sizeof (HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0), -1, sizeof(HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3127[62] = 
{
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0::get_offset_of_requestUri_13(),
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0::get_offset_of_actualUri_14(),
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0::get_offset_of_hostChanged_15(),
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0::get_offset_of_allowAutoRedirect_16(),
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0::get_offset_of_allowBuffering_17(),
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0::get_offset_of_certificates_18(),
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0::get_offset_of_connectionGroup_19(),
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0::get_offset_of_haveContentLength_20(),
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0::get_offset_of_contentLength_21(),
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0::get_offset_of_continueDelegate_22(),
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0::get_offset_of_cookieContainer_23(),
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0::get_offset_of_credentials_24(),
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0::get_offset_of_haveResponse_25(),
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0::get_offset_of_haveRequest_26(),
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0::get_offset_of_requestSent_27(),
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0::get_offset_of_webHeaders_28(),
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0::get_offset_of_keepAlive_29(),
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0::get_offset_of_maxAutoRedirect_30(),
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0::get_offset_of_mediaType_31(),
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0::get_offset_of_method_32(),
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0::get_offset_of_initialMethod_33(),
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0::get_offset_of_pipelined_34(),
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0::get_offset_of_preAuthenticate_35(),
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0::get_offset_of_usedPreAuth_36(),
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0::get_offset_of_version_37(),
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0::get_offset_of_force_version_38(),
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0::get_offset_of_actualVersion_39(),
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0::get_offset_of_proxy_40(),
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0::get_offset_of_sendChunked_41(),
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0::get_offset_of_servicePoint_42(),
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0::get_offset_of_timeout_43(),
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0::get_offset_of_writeStream_44(),
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0::get_offset_of_webResponse_45(),
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0::get_offset_of_asyncWrite_46(),
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0::get_offset_of_asyncRead_47(),
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0::get_offset_of_abortHandler_48(),
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0::get_offset_of_aborted_49(),
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0::get_offset_of_gotRequestStream_50(),
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0::get_offset_of_redirects_51(),
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0::get_offset_of_expectContinue_52(),
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0::get_offset_of_bodyBuffer_53(),
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0::get_offset_of_bodyBufferLength_54(),
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0::get_offset_of_getResponseCalled_55(),
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0::get_offset_of_saved_exc_56(),
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0::get_offset_of_locker_57(),
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0::get_offset_of_finished_reading_58(),
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0::get_offset_of_WebConnection_59(),
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0::get_offset_of_auto_decomp_60(),
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0::get_offset_of_maxResponseHeadersLength_61(),
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0_StaticFields::get_offset_of_defaultMaxResponseHeadersLength_62(),
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0::get_offset_of_readWriteTimeout_63(),
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0::get_offset_of_tlsProvider_64(),
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0::get_offset_of_tlsSettings_65(),
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0::get_offset_of_certValidationCallback_66(),
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0::get_offset_of_auth_state_67(),
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0::get_offset_of_proxy_auth_state_68(),
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0::get_offset_of_host_69(),
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0::get_offset_of_ResendContentFactory_70(),
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0::get_offset_of_U3CThrowOnErrorU3Ek__BackingField_71(),
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0::get_offset_of_unsafe_auth_blah_72(),
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0::get_offset_of_U3CReuseConnectionU3Ek__BackingField_73(),
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0::get_offset_of_StoredConnection_74(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3128 = { sizeof (NtlmAuthState_tF501EE09345DFAE6FD7B4D8EBBE77292514DFA83)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3128[4] = 
{
	NtlmAuthState_tF501EE09345DFAE6FD7B4D8EBBE77292514DFA83::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3129 = { sizeof (AuthorizationState_t5C342070F47B5DBAE0089B8B6A391FDEB6914AAB)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3129[4] = 
{
	AuthorizationState_t5C342070F47B5DBAE0089B8B6A391FDEB6914AAB::get_offset_of_request_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AuthorizationState_t5C342070F47B5DBAE0089B8B6A391FDEB6914AAB::get_offset_of_isProxy_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AuthorizationState_t5C342070F47B5DBAE0089B8B6A391FDEB6914AAB::get_offset_of_isCompleted_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AuthorizationState_t5C342070F47B5DBAE0089B8B6A391FDEB6914AAB::get_offset_of_ntlm_auth_state_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3130 = { sizeof (U3CU3Ec__DisplayClass238_0_t772E96E52BE401D5422C8A540FC1B812F2D9B87B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3130[2] = 
{
	U3CU3Ec__DisplayClass238_0_t772E96E52BE401D5422C8A540FC1B812F2D9B87B::get_offset_of_aread_0(),
	U3CU3Ec__DisplayClass238_0_t772E96E52BE401D5422C8A540FC1B812F2D9B87B::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3131 = { sizeof (HttpWebResponse_t34CF6A40A4748A0F8694FEFEA3723D9AE3EF3951), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3131[12] = 
{
	HttpWebResponse_t34CF6A40A4748A0F8694FEFEA3723D9AE3EF3951::get_offset_of_uri_3(),
	HttpWebResponse_t34CF6A40A4748A0F8694FEFEA3723D9AE3EF3951::get_offset_of_webHeaders_4(),
	HttpWebResponse_t34CF6A40A4748A0F8694FEFEA3723D9AE3EF3951::get_offset_of_cookieCollection_5(),
	HttpWebResponse_t34CF6A40A4748A0F8694FEFEA3723D9AE3EF3951::get_offset_of_method_6(),
	HttpWebResponse_t34CF6A40A4748A0F8694FEFEA3723D9AE3EF3951::get_offset_of_version_7(),
	HttpWebResponse_t34CF6A40A4748A0F8694FEFEA3723D9AE3EF3951::get_offset_of_statusCode_8(),
	HttpWebResponse_t34CF6A40A4748A0F8694FEFEA3723D9AE3EF3951::get_offset_of_statusDescription_9(),
	HttpWebResponse_t34CF6A40A4748A0F8694FEFEA3723D9AE3EF3951::get_offset_of_contentLength_10(),
	HttpWebResponse_t34CF6A40A4748A0F8694FEFEA3723D9AE3EF3951::get_offset_of_contentType_11(),
	HttpWebResponse_t34CF6A40A4748A0F8694FEFEA3723D9AE3EF3951::get_offset_of_cookie_container_12(),
	HttpWebResponse_t34CF6A40A4748A0F8694FEFEA3723D9AE3EF3951::get_offset_of_disposed_13(),
	HttpWebResponse_t34CF6A40A4748A0F8694FEFEA3723D9AE3EF3951::get_offset_of_stream_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3132 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3133 = { sizeof (IPv6AddressFormatter_t451290B1C6FD64B6C59F95D99EDB4A9CC703BA90)+ sizeof (RuntimeObject), sizeof(IPv6AddressFormatter_t451290B1C6FD64B6C59F95D99EDB4A9CC703BA90 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3133[2] = 
{
	IPv6AddressFormatter_t451290B1C6FD64B6C59F95D99EDB4A9CC703BA90::get_offset_of_address_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	IPv6AddressFormatter_t451290B1C6FD64B6C59F95D99EDB4A9CC703BA90::get_offset_of_scopeId_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3134 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3135 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3136 = { sizeof (ListenerAsyncResult_t0305382DE30FE5EF4FAD66545E460CD4BDDEA12F), -1, sizeof(ListenerAsyncResult_t0305382DE30FE5EF4FAD66545E460CD4BDDEA12F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3136[12] = 
{
	ListenerAsyncResult_t0305382DE30FE5EF4FAD66545E460CD4BDDEA12F::get_offset_of_handle_0(),
	ListenerAsyncResult_t0305382DE30FE5EF4FAD66545E460CD4BDDEA12F::get_offset_of_synch_1(),
	ListenerAsyncResult_t0305382DE30FE5EF4FAD66545E460CD4BDDEA12F::get_offset_of_completed_2(),
	ListenerAsyncResult_t0305382DE30FE5EF4FAD66545E460CD4BDDEA12F::get_offset_of_cb_3(),
	ListenerAsyncResult_t0305382DE30FE5EF4FAD66545E460CD4BDDEA12F::get_offset_of_state_4(),
	ListenerAsyncResult_t0305382DE30FE5EF4FAD66545E460CD4BDDEA12F::get_offset_of_exception_5(),
	ListenerAsyncResult_t0305382DE30FE5EF4FAD66545E460CD4BDDEA12F::get_offset_of_context_6(),
	ListenerAsyncResult_t0305382DE30FE5EF4FAD66545E460CD4BDDEA12F::get_offset_of_locker_7(),
	ListenerAsyncResult_t0305382DE30FE5EF4FAD66545E460CD4BDDEA12F::get_offset_of_forward_8(),
	ListenerAsyncResult_t0305382DE30FE5EF4FAD66545E460CD4BDDEA12F::get_offset_of_EndCalled_9(),
	ListenerAsyncResult_t0305382DE30FE5EF4FAD66545E460CD4BDDEA12F::get_offset_of_InGet_10(),
	ListenerAsyncResult_t0305382DE30FE5EF4FAD66545E460CD4BDDEA12F_StaticFields::get_offset_of_InvokeCB_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3137 = { sizeof (ListenerPrefix_t7E983ACD2D31FE4DC42DE02AE82EC24734DC57D7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3137[7] = 
{
	ListenerPrefix_t7E983ACD2D31FE4DC42DE02AE82EC24734DC57D7::get_offset_of_original_0(),
	ListenerPrefix_t7E983ACD2D31FE4DC42DE02AE82EC24734DC57D7::get_offset_of_host_1(),
	ListenerPrefix_t7E983ACD2D31FE4DC42DE02AE82EC24734DC57D7::get_offset_of_port_2(),
	ListenerPrefix_t7E983ACD2D31FE4DC42DE02AE82EC24734DC57D7::get_offset_of_path_3(),
	ListenerPrefix_t7E983ACD2D31FE4DC42DE02AE82EC24734DC57D7::get_offset_of_secure_4(),
	ListenerPrefix_t7E983ACD2D31FE4DC42DE02AE82EC24734DC57D7::get_offset_of_addresses_5(),
	ListenerPrefix_t7E983ACD2D31FE4DC42DE02AE82EC24734DC57D7::get_offset_of_Listener_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3138 = { sizeof (MonoChunkStream_t33C2B7ECB208D77D1C94F0A9F48EB5AE27DF5AB5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3138[10] = 
{
	MonoChunkStream_t33C2B7ECB208D77D1C94F0A9F48EB5AE27DF5AB5::get_offset_of_headers_0(),
	MonoChunkStream_t33C2B7ECB208D77D1C94F0A9F48EB5AE27DF5AB5::get_offset_of_chunkSize_1(),
	MonoChunkStream_t33C2B7ECB208D77D1C94F0A9F48EB5AE27DF5AB5::get_offset_of_chunkRead_2(),
	MonoChunkStream_t33C2B7ECB208D77D1C94F0A9F48EB5AE27DF5AB5::get_offset_of_totalWritten_3(),
	MonoChunkStream_t33C2B7ECB208D77D1C94F0A9F48EB5AE27DF5AB5::get_offset_of_state_4(),
	MonoChunkStream_t33C2B7ECB208D77D1C94F0A9F48EB5AE27DF5AB5::get_offset_of_saved_5(),
	MonoChunkStream_t33C2B7ECB208D77D1C94F0A9F48EB5AE27DF5AB5::get_offset_of_sawCR_6(),
	MonoChunkStream_t33C2B7ECB208D77D1C94F0A9F48EB5AE27DF5AB5::get_offset_of_gotit_7(),
	MonoChunkStream_t33C2B7ECB208D77D1C94F0A9F48EB5AE27DF5AB5::get_offset_of_trailerState_8(),
	MonoChunkStream_t33C2B7ECB208D77D1C94F0A9F48EB5AE27DF5AB5::get_offset_of_chunks_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3139 = { sizeof (State_t9575019D3583B1A375418BD20391992F28C37967)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3139[6] = 
{
	State_t9575019D3583B1A375418BD20391992F28C37967::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3140 = { sizeof (Chunk_tB367D5D805924164D239F587BC848162536B8AB7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3140[2] = 
{
	Chunk_tB367D5D805924164D239F587BC848162536B8AB7::get_offset_of_Bytes_0(),
	Chunk_tB367D5D805924164D239F587BC848162536B8AB7::get_offset_of_Offset_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3141 = { sizeof (MonoHttpDate_t81A2164242F9BBCAD77B26859F0D381E133CC9E1), -1, sizeof(MonoHttpDate_t81A2164242F9BBCAD77B26859F0D381E133CC9E1_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3141[4] = 
{
	MonoHttpDate_t81A2164242F9BBCAD77B26859F0D381E133CC9E1_StaticFields::get_offset_of_rfc1123_date_0(),
	MonoHttpDate_t81A2164242F9BBCAD77B26859F0D381E133CC9E1_StaticFields::get_offset_of_rfc850_date_1(),
	MonoHttpDate_t81A2164242F9BBCAD77B26859F0D381E133CC9E1_StaticFields::get_offset_of_asctime_date_2(),
	MonoHttpDate_t81A2164242F9BBCAD77B26859F0D381E133CC9E1_StaticFields::get_offset_of_formats_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3142 = { sizeof (NetConfig_t1EDDC92F13548882CC6D0D0AA40C174D21878150), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3142[2] = 
{
	NetConfig_t1EDDC92F13548882CC6D0D0AA40C174D21878150::get_offset_of_ipv6Enabled_0(),
	NetConfig_t1EDDC92F13548882CC6D0D0AA40C174D21878150::get_offset_of_MaxResponseHeadersLength_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3143 = { sizeof (NtlmClient_tBCB5B9D27D758545CF0BB6490F1A5CE77F65B204), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3143[1] = 
{
	NtlmClient_tBCB5B9D27D758545CF0BB6490F1A5CE77F65B204::get_offset_of_authObject_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3144 = { sizeof (RequestStream_t4BF6FD18AB7261094F557E867606B91FECF74D35), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3144[6] = 
{
	RequestStream_t4BF6FD18AB7261094F557E867606B91FECF74D35::get_offset_of_buffer_5(),
	RequestStream_t4BF6FD18AB7261094F557E867606B91FECF74D35::get_offset_of_offset_6(),
	RequestStream_t4BF6FD18AB7261094F557E867606B91FECF74D35::get_offset_of_length_7(),
	RequestStream_t4BF6FD18AB7261094F557E867606B91FECF74D35::get_offset_of_remaining_body_8(),
	RequestStream_t4BF6FD18AB7261094F557E867606B91FECF74D35::get_offset_of_disposed_9(),
	RequestStream_t4BF6FD18AB7261094F557E867606B91FECF74D35::get_offset_of_stream_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3145 = { sizeof (ResponseStream_tE537A0E20974BEB6629CBEBE42C27C9B97126032), -1, sizeof(ResponseStream_tE537A0E20974BEB6629CBEBE42C27C9B97126032_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3145[6] = 
{
	ResponseStream_tE537A0E20974BEB6629CBEBE42C27C9B97126032::get_offset_of_response_5(),
	ResponseStream_tE537A0E20974BEB6629CBEBE42C27C9B97126032::get_offset_of_ignore_errors_6(),
	ResponseStream_tE537A0E20974BEB6629CBEBE42C27C9B97126032::get_offset_of_disposed_7(),
	ResponseStream_tE537A0E20974BEB6629CBEBE42C27C9B97126032::get_offset_of_trailer_sent_8(),
	ResponseStream_tE537A0E20974BEB6629CBEBE42C27C9B97126032::get_offset_of_stream_9(),
	ResponseStream_tE537A0E20974BEB6629CBEBE42C27C9B97126032_StaticFields::get_offset_of_crlf_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3146 = { sizeof (ServicePoint_t5F42B1A9D56E09B4B051BE0968C81DE3128E3EB4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3146[21] = 
{
	ServicePoint_t5F42B1A9D56E09B4B051BE0968C81DE3128E3EB4::get_offset_of_uri_0(),
	ServicePoint_t5F42B1A9D56E09B4B051BE0968C81DE3128E3EB4::get_offset_of_connectionLimit_1(),
	ServicePoint_t5F42B1A9D56E09B4B051BE0968C81DE3128E3EB4::get_offset_of_maxIdleTime_2(),
	ServicePoint_t5F42B1A9D56E09B4B051BE0968C81DE3128E3EB4::get_offset_of_currentConnections_3(),
	ServicePoint_t5F42B1A9D56E09B4B051BE0968C81DE3128E3EB4::get_offset_of_idleSince_4(),
	ServicePoint_t5F42B1A9D56E09B4B051BE0968C81DE3128E3EB4::get_offset_of_lastDnsResolve_5(),
	ServicePoint_t5F42B1A9D56E09B4B051BE0968C81DE3128E3EB4::get_offset_of_protocolVersion_6(),
	ServicePoint_t5F42B1A9D56E09B4B051BE0968C81DE3128E3EB4::get_offset_of_host_7(),
	ServicePoint_t5F42B1A9D56E09B4B051BE0968C81DE3128E3EB4::get_offset_of_usesProxy_8(),
	ServicePoint_t5F42B1A9D56E09B4B051BE0968C81DE3128E3EB4::get_offset_of_groups_9(),
	ServicePoint_t5F42B1A9D56E09B4B051BE0968C81DE3128E3EB4::get_offset_of_sendContinue_10(),
	ServicePoint_t5F42B1A9D56E09B4B051BE0968C81DE3128E3EB4::get_offset_of_useConnect_11(),
	ServicePoint_t5F42B1A9D56E09B4B051BE0968C81DE3128E3EB4::get_offset_of_hostE_12(),
	ServicePoint_t5F42B1A9D56E09B4B051BE0968C81DE3128E3EB4::get_offset_of_useNagle_13(),
	ServicePoint_t5F42B1A9D56E09B4B051BE0968C81DE3128E3EB4::get_offset_of_endPointCallback_14(),
	ServicePoint_t5F42B1A9D56E09B4B051BE0968C81DE3128E3EB4::get_offset_of_tcp_keepalive_15(),
	ServicePoint_t5F42B1A9D56E09B4B051BE0968C81DE3128E3EB4::get_offset_of_tcp_keepalive_time_16(),
	ServicePoint_t5F42B1A9D56E09B4B051BE0968C81DE3128E3EB4::get_offset_of_tcp_keepalive_interval_17(),
	ServicePoint_t5F42B1A9D56E09B4B051BE0968C81DE3128E3EB4::get_offset_of_idleTimer_18(),
	ServicePoint_t5F42B1A9D56E09B4B051BE0968C81DE3128E3EB4::get_offset_of_m_ServerCertificateOrBytes_19(),
	ServicePoint_t5F42B1A9D56E09B4B051BE0968C81DE3128E3EB4::get_offset_of_m_ClientCertificateOrBytes_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3147 = { sizeof (ServicePointManager_tB30C5869193569552EC4F0F454EF3ACF3908DC02), -1, sizeof(ServicePointManager_tB30C5869193569552EC4F0F454EF3ACF3908DC02_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3147[16] = 
{
	ServicePointManager_tB30C5869193569552EC4F0F454EF3ACF3908DC02_StaticFields::get_offset_of_servicePoints_0(),
	ServicePointManager_tB30C5869193569552EC4F0F454EF3ACF3908DC02_StaticFields::get_offset_of_policy_1(),
	ServicePointManager_tB30C5869193569552EC4F0F454EF3ACF3908DC02_StaticFields::get_offset_of_defaultConnectionLimit_2(),
	ServicePointManager_tB30C5869193569552EC4F0F454EF3ACF3908DC02_StaticFields::get_offset_of_maxServicePointIdleTime_3(),
	ServicePointManager_tB30C5869193569552EC4F0F454EF3ACF3908DC02_StaticFields::get_offset_of_maxServicePoints_4(),
	ServicePointManager_tB30C5869193569552EC4F0F454EF3ACF3908DC02_StaticFields::get_offset_of_dnsRefreshTimeout_5(),
	ServicePointManager_tB30C5869193569552EC4F0F454EF3ACF3908DC02_StaticFields::get_offset_of__checkCRL_6(),
	ServicePointManager_tB30C5869193569552EC4F0F454EF3ACF3908DC02_StaticFields::get_offset_of__securityProtocol_7(),
	ServicePointManager_tB30C5869193569552EC4F0F454EF3ACF3908DC02_StaticFields::get_offset_of_expectContinue_8(),
	ServicePointManager_tB30C5869193569552EC4F0F454EF3ACF3908DC02_StaticFields::get_offset_of_useNagle_9(),
	ServicePointManager_tB30C5869193569552EC4F0F454EF3ACF3908DC02_StaticFields::get_offset_of_server_cert_cb_10(),
	ServicePointManager_tB30C5869193569552EC4F0F454EF3ACF3908DC02_StaticFields::get_offset_of_tcp_keepalive_11(),
	ServicePointManager_tB30C5869193569552EC4F0F454EF3ACF3908DC02_StaticFields::get_offset_of_tcp_keepalive_time_12(),
	ServicePointManager_tB30C5869193569552EC4F0F454EF3ACF3908DC02_StaticFields::get_offset_of_tcp_keepalive_interval_13(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3148 = { sizeof (SPKey_t11DD7899D8DCC8D651D31028474C6CD456FD4D63), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3148[3] = 
{
	SPKey_t11DD7899D8DCC8D651D31028474C6CD456FD4D63::get_offset_of_uri_0(),
	SPKey_t11DD7899D8DCC8D651D31028474C6CD456FD4D63::get_offset_of_proxy_1(),
	SPKey_t11DD7899D8DCC8D651D31028474C6CD456FD4D63::get_offset_of_use_connect_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3149 = { sizeof (SimpleAsyncCallback_t690665AFDCBDEBA379B6F4CD213CB4BB8570F83F), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3150 = { sizeof (SimpleAsyncResult_tA572851810F8E279EE9E5378A6D9A538B1822FC6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3150[9] = 
{
	SimpleAsyncResult_tA572851810F8E279EE9E5378A6D9A538B1822FC6::get_offset_of_handle_0(),
	SimpleAsyncResult_tA572851810F8E279EE9E5378A6D9A538B1822FC6::get_offset_of_synch_1(),
	SimpleAsyncResult_tA572851810F8E279EE9E5378A6D9A538B1822FC6::get_offset_of_isCompleted_2(),
	SimpleAsyncResult_tA572851810F8E279EE9E5378A6D9A538B1822FC6::get_offset_of_cb_3(),
	SimpleAsyncResult_tA572851810F8E279EE9E5378A6D9A538B1822FC6::get_offset_of_state_4(),
	SimpleAsyncResult_tA572851810F8E279EE9E5378A6D9A538B1822FC6::get_offset_of_callbackDone_5(),
	SimpleAsyncResult_tA572851810F8E279EE9E5378A6D9A538B1822FC6::get_offset_of_exc_6(),
	SimpleAsyncResult_tA572851810F8E279EE9E5378A6D9A538B1822FC6::get_offset_of_locker_7(),
	SimpleAsyncResult_tA572851810F8E279EE9E5378A6D9A538B1822FC6::get_offset_of_user_read_synch_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3151 = { sizeof (U3CU3Ec__DisplayClass9_0_t47DEAC5ADFEA0940FAE3D1B7BCB60D05496BC8E3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3151[2] = 
{
	U3CU3Ec__DisplayClass9_0_t47DEAC5ADFEA0940FAE3D1B7BCB60D05496BC8E3::get_offset_of_cb_0(),
	U3CU3Ec__DisplayClass9_0_t47DEAC5ADFEA0940FAE3D1B7BCB60D05496BC8E3::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3152 = { sizeof (U3CU3Ec__DisplayClass11_0_t8C200E7B255B4336993B38FAC7E931F8414EF122), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3152[3] = 
{
	U3CU3Ec__DisplayClass11_0_t8C200E7B255B4336993B38FAC7E931F8414EF122::get_offset_of_func_0(),
	U3CU3Ec__DisplayClass11_0_t8C200E7B255B4336993B38FAC7E931F8414EF122::get_offset_of_locker_1(),
	U3CU3Ec__DisplayClass11_0_t8C200E7B255B4336993B38FAC7E931F8414EF122::get_offset_of_callback_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3153 = { sizeof (SocketPermission_t1B5B253F95303B8DDD7F1539C60DE1B0ACA358C5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3153[4] = 
{
	SocketPermission_t1B5B253F95303B8DDD7F1539C60DE1B0ACA358C5::get_offset_of_m_acceptList_0(),
	SocketPermission_t1B5B253F95303B8DDD7F1539C60DE1B0ACA358C5::get_offset_of_m_connectList_1(),
	SocketPermission_t1B5B253F95303B8DDD7F1539C60DE1B0ACA358C5::get_offset_of_m_noRestriction_2(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3154 = { sizeof (SocketPermissionAttribute_t770C87915AD66FF25AE25E1B008B644177F50E64), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3154[4] = 
{
	SocketPermissionAttribute_t770C87915AD66FF25AE25E1B008B644177F50E64::get_offset_of_m_access_2(),
	SocketPermissionAttribute_t770C87915AD66FF25AE25E1B008B644177F50E64::get_offset_of_m_host_3(),
	SocketPermissionAttribute_t770C87915AD66FF25AE25E1B008B644177F50E64::get_offset_of_m_port_4(),
	SocketPermissionAttribute_t770C87915AD66FF25AE25E1B008B644177F50E64::get_offset_of_m_transport_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3155 = { sizeof (WebAsyncResult_tF700444B9ABA86C7CADBFA7B99DEC52D9FBD87EE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3155[10] = 
{
	WebAsyncResult_tF700444B9ABA86C7CADBFA7B99DEC52D9FBD87EE::get_offset_of_nbytes_9(),
	WebAsyncResult_tF700444B9ABA86C7CADBFA7B99DEC52D9FBD87EE::get_offset_of_innerAsyncResult_10(),
	WebAsyncResult_tF700444B9ABA86C7CADBFA7B99DEC52D9FBD87EE::get_offset_of_response_11(),
	WebAsyncResult_tF700444B9ABA86C7CADBFA7B99DEC52D9FBD87EE::get_offset_of_writeStream_12(),
	WebAsyncResult_tF700444B9ABA86C7CADBFA7B99DEC52D9FBD87EE::get_offset_of_buffer_13(),
	WebAsyncResult_tF700444B9ABA86C7CADBFA7B99DEC52D9FBD87EE::get_offset_of_offset_14(),
	WebAsyncResult_tF700444B9ABA86C7CADBFA7B99DEC52D9FBD87EE::get_offset_of_size_15(),
	WebAsyncResult_tF700444B9ABA86C7CADBFA7B99DEC52D9FBD87EE::get_offset_of_EndCalled_16(),
	WebAsyncResult_tF700444B9ABA86C7CADBFA7B99DEC52D9FBD87EE::get_offset_of_AsyncWriteAll_17(),
	WebAsyncResult_tF700444B9ABA86C7CADBFA7B99DEC52D9FBD87EE::get_offset_of_AsyncObject_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3156 = { sizeof (ReadState_t4A38DE8AC8A5473133060405B3A00021D4422114)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3156[6] = 
{
	ReadState_t4A38DE8AC8A5473133060405B3A00021D4422114::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3157 = { sizeof (WebConnection_tEB76AEE17361D28CBAD4033026A71DA89289C243), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3157[24] = 
{
	WebConnection_tEB76AEE17361D28CBAD4033026A71DA89289C243::get_offset_of_sPoint_0(),
	WebConnection_tEB76AEE17361D28CBAD4033026A71DA89289C243::get_offset_of_nstream_1(),
	WebConnection_tEB76AEE17361D28CBAD4033026A71DA89289C243::get_offset_of_socket_2(),
	WebConnection_tEB76AEE17361D28CBAD4033026A71DA89289C243::get_offset_of_socketLock_3(),
	WebConnection_tEB76AEE17361D28CBAD4033026A71DA89289C243::get_offset_of_state_4(),
	WebConnection_tEB76AEE17361D28CBAD4033026A71DA89289C243::get_offset_of_status_5(),
	WebConnection_tEB76AEE17361D28CBAD4033026A71DA89289C243::get_offset_of_keepAlive_6(),
	WebConnection_tEB76AEE17361D28CBAD4033026A71DA89289C243::get_offset_of_buffer_7(),
	WebConnection_tEB76AEE17361D28CBAD4033026A71DA89289C243::get_offset_of_abortHandler_8(),
	WebConnection_tEB76AEE17361D28CBAD4033026A71DA89289C243::get_offset_of_abortHelper_9(),
	WebConnection_tEB76AEE17361D28CBAD4033026A71DA89289C243::get_offset_of_Data_10(),
	WebConnection_tEB76AEE17361D28CBAD4033026A71DA89289C243::get_offset_of_chunkedRead_11(),
	WebConnection_tEB76AEE17361D28CBAD4033026A71DA89289C243::get_offset_of_chunkStream_12(),
	WebConnection_tEB76AEE17361D28CBAD4033026A71DA89289C243::get_offset_of_queue_13(),
	WebConnection_tEB76AEE17361D28CBAD4033026A71DA89289C243::get_offset_of_reused_14(),
	WebConnection_tEB76AEE17361D28CBAD4033026A71DA89289C243::get_offset_of_position_15(),
	WebConnection_tEB76AEE17361D28CBAD4033026A71DA89289C243::get_offset_of_priority_request_16(),
	WebConnection_tEB76AEE17361D28CBAD4033026A71DA89289C243::get_offset_of_ntlm_credentials_17(),
	WebConnection_tEB76AEE17361D28CBAD4033026A71DA89289C243::get_offset_of_ntlm_authenticated_18(),
	WebConnection_tEB76AEE17361D28CBAD4033026A71DA89289C243::get_offset_of_unsafe_sharing_19(),
	WebConnection_tEB76AEE17361D28CBAD4033026A71DA89289C243::get_offset_of_connect_ntlm_auth_state_20(),
	WebConnection_tEB76AEE17361D28CBAD4033026A71DA89289C243::get_offset_of_connect_request_21(),
	WebConnection_tEB76AEE17361D28CBAD4033026A71DA89289C243::get_offset_of_connect_exception_22(),
	WebConnection_tEB76AEE17361D28CBAD4033026A71DA89289C243::get_offset_of_tlsStream_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3158 = { sizeof (NtlmAuthState_tEDDC6AC65C3D7223EB1A1360D852CDEA2F3A251D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3158[4] = 
{
	NtlmAuthState_tEDDC6AC65C3D7223EB1A1360D852CDEA2F3A251D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3159 = { sizeof (AbortHelper_t0DB9458211F015848382C4B5A007AC4947411E81), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3159[1] = 
{
	AbortHelper_t0DB9458211F015848382C4B5A007AC4947411E81::get_offset_of_Connection_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3160 = { sizeof (WebConnectionData_tC9286455629F1E9E2BA0CA8AB6958DF931299CCC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3160[9] = 
{
	WebConnectionData_tC9286455629F1E9E2BA0CA8AB6958DF931299CCC::get_offset_of__request_0(),
	WebConnectionData_tC9286455629F1E9E2BA0CA8AB6958DF931299CCC::get_offset_of_StatusCode_1(),
	WebConnectionData_tC9286455629F1E9E2BA0CA8AB6958DF931299CCC::get_offset_of_StatusDescription_2(),
	WebConnectionData_tC9286455629F1E9E2BA0CA8AB6958DF931299CCC::get_offset_of_Headers_3(),
	WebConnectionData_tC9286455629F1E9E2BA0CA8AB6958DF931299CCC::get_offset_of_Version_4(),
	WebConnectionData_tC9286455629F1E9E2BA0CA8AB6958DF931299CCC::get_offset_of_ProxyVersion_5(),
	WebConnectionData_tC9286455629F1E9E2BA0CA8AB6958DF931299CCC::get_offset_of_stream_6(),
	WebConnectionData_tC9286455629F1E9E2BA0CA8AB6958DF931299CCC::get_offset_of_Challenge_7(),
	WebConnectionData_tC9286455629F1E9E2BA0CA8AB6958DF931299CCC::get_offset_of__readState_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3161 = { sizeof (WebConnectionGroup_tBEAB5ED1DE321C0981F5CBABA020970C3D63E95F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3161[6] = 
{
	WebConnectionGroup_tBEAB5ED1DE321C0981F5CBABA020970C3D63E95F::get_offset_of_sPoint_0(),
	WebConnectionGroup_tBEAB5ED1DE321C0981F5CBABA020970C3D63E95F::get_offset_of_name_1(),
	WebConnectionGroup_tBEAB5ED1DE321C0981F5CBABA020970C3D63E95F::get_offset_of_connections_2(),
	WebConnectionGroup_tBEAB5ED1DE321C0981F5CBABA020970C3D63E95F::get_offset_of_queue_3(),
	WebConnectionGroup_tBEAB5ED1DE321C0981F5CBABA020970C3D63E95F::get_offset_of_closing_4(),
	WebConnectionGroup_tBEAB5ED1DE321C0981F5CBABA020970C3D63E95F::get_offset_of_ConnectionClosed_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3162 = { sizeof (ConnectionState_t9EAE3917A0743B4C6D40669D7B2BBE799490A0C6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3162[4] = 
{
	ConnectionState_t9EAE3917A0743B4C6D40669D7B2BBE799490A0C6::get_offset_of_U3CConnectionU3Ek__BackingField_0(),
	ConnectionState_t9EAE3917A0743B4C6D40669D7B2BBE799490A0C6::get_offset_of_U3CGroupU3Ek__BackingField_1(),
	ConnectionState_t9EAE3917A0743B4C6D40669D7B2BBE799490A0C6::get_offset_of_busy_2(),
	ConnectionState_t9EAE3917A0743B4C6D40669D7B2BBE799490A0C6::get_offset_of_idleSince_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3163 = { sizeof (WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC), -1, sizeof(WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3163[31] = 
{
	WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC_StaticFields::get_offset_of_crlf_5(),
	WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC::get_offset_of_isRead_6(),
	WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC::get_offset_of_cnc_7(),
	WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC::get_offset_of_request_8(),
	WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC::get_offset_of_readBuffer_9(),
	WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC::get_offset_of_readBufferOffset_10(),
	WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC::get_offset_of_readBufferSize_11(),
	WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC::get_offset_of_stream_length_12(),
	WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC::get_offset_of_contentLength_13(),
	WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC::get_offset_of_totalRead_14(),
	WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC::get_offset_of_totalWritten_15(),
	WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC::get_offset_of_nextReadCalled_16(),
	WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC::get_offset_of_pendingReads_17(),
	WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC::get_offset_of_pendingWrites_18(),
	WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC::get_offset_of_pending_19(),
	WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC::get_offset_of_allowBuffering_20(),
	WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC::get_offset_of_sendChunked_21(),
	WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC::get_offset_of_writeBuffer_22(),
	WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC::get_offset_of_requestWritten_23(),
	WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC::get_offset_of_headers_24(),
	WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC::get_offset_of_disposed_25(),
	WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC::get_offset_of_headersSent_26(),
	WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC::get_offset_of_locker_27(),
	WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC::get_offset_of_initRead_28(),
	WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC::get_offset_of_read_eof_29(),
	WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC::get_offset_of_complete_request_written_30(),
	WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC::get_offset_of_read_timeout_31(),
	WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC::get_offset_of_write_timeout_32(),
	WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC::get_offset_of_cb_wrapper_33(),
	WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC::get_offset_of_IgnoreIOErrors_34(),
	WebConnectionStream_t537F33BF6D8999D67791D02F8E6DE6448F2A31FC::get_offset_of_U3CGetResponseOnCloseU3Ek__BackingField_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3164 = { sizeof (U3CU3Ec__DisplayClass75_0_tB08224AE141BA9F8046D1F0C267E2308BA8CABCB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3164[2] = 
{
	U3CU3Ec__DisplayClass75_0_tB08224AE141BA9F8046D1F0C267E2308BA8CABCB::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass75_0_tB08224AE141BA9F8046D1F0C267E2308BA8CABCB::get_offset_of_setInternalLength_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3165 = { sizeof (U3CU3Ec__DisplayClass76_0_t09438DD600601604F0E06AE0B2549E981049E350), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3165[2] = 
{
	U3CU3Ec__DisplayClass76_0_t09438DD600601604F0E06AE0B2549E981049E350::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass76_0_t09438DD600601604F0E06AE0B2549E981049E350::get_offset_of_result_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3166 = { sizeof (U3CU3Ec__DisplayClass80_0_t7D1F20BB8EB27922CC970938E79BEBC5B485FEEE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3166[5] = 
{
	U3CU3Ec__DisplayClass80_0_t7D1F20BB8EB27922CC970938E79BEBC5B485FEEE::get_offset_of_result_0(),
	U3CU3Ec__DisplayClass80_0_t7D1F20BB8EB27922CC970938E79BEBC5B485FEEE::get_offset_of_U3CU3E4__this_1(),
	U3CU3Ec__DisplayClass80_0_t7D1F20BB8EB27922CC970938E79BEBC5B485FEEE::get_offset_of_length_2(),
	U3CU3Ec__DisplayClass80_0_t7D1F20BB8EB27922CC970938E79BEBC5B485FEEE::get_offset_of_bytes_3(),
	U3CU3Ec__DisplayClass80_0_t7D1F20BB8EB27922CC970938E79BEBC5B485FEEE::get_offset_of_U3CU3E9__1_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3167 = { sizeof (SmtpDeliveryFormat_t100736159C2DA0B86D1CC40D5B0FEA85250ECDB6)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3167[3] = 
{
	SmtpDeliveryFormat_t100736159C2DA0B86D1CC40D5B0FEA85250ECDB6::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3168 = { sizeof (SmtpDeliveryMethod_t5BEC6DACB33CA745F948B250D0E1E0028D2BBC41)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3168[4] = 
{
	SmtpDeliveryMethod_t5BEC6DACB33CA745F948B250D0E1E0028D2BBC41::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3169 = { sizeof (Base64WriteStateInfo_tB45E408314AC5BFE8908E8E6C85BEB10EF29F366), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3169[2] = 
{
	Base64WriteStateInfo_tB45E408314AC5BFE8908E8E6C85BEB10EF29F366::get_offset_of_U3CPaddingU3Ek__BackingField_6(),
	Base64WriteStateInfo_tB45E408314AC5BFE8908E8E6C85BEB10EF29F366::get_offset_of_U3CLastBitsU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3170 = { sizeof (EncodedStreamFactory_tE92297883C287D4B1B6D0FF021A496B991095BDC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3171 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3172 = { sizeof (WriteStateInfoBase_t029DDB6613907E0AEF10D611B6E48422EC7778FD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3172[6] = 
{
	WriteStateInfoBase_t029DDB6613907E0AEF10D611B6E48422EC7778FD::get_offset_of__header_0(),
	WriteStateInfoBase_t029DDB6613907E0AEF10D611B6E48422EC7778FD::get_offset_of__footer_1(),
	WriteStateInfoBase_t029DDB6613907E0AEF10D611B6E48422EC7778FD::get_offset_of__maxLineLength_2(),
	WriteStateInfoBase_t029DDB6613907E0AEF10D611B6E48422EC7778FD::get_offset_of_buffer_3(),
	WriteStateInfoBase_t029DDB6613907E0AEF10D611B6E48422EC7778FD::get_offset_of__currentLineLength_4(),
	WriteStateInfoBase_t029DDB6613907E0AEF10D611B6E48422EC7778FD::get_offset_of__currentBufferUsed_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3173 = { sizeof (SocketException_t75481CF49BCAF5685A5A9E6933909E0B65E7E0A5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3173[1] = 
{
	SocketException_t75481CF49BCAF5685A5A9E6933909E0B65E7E0A5::get_offset_of_m_EndPoint_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3174 = { sizeof (AddressFamily_tFA4F79FA7F299EBDF507F4811E6E5C3EEBF0850E)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3174[32] = 
{
	AddressFamily_tFA4F79FA7F299EBDF507F4811E6E5C3EEBF0850E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3175 = { sizeof (IOControlCode_t8A59BB74289B0C9BBB1659E249E54BC5A205D6B9)+ sizeof (RuntimeObject), sizeof(int64_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3175[35] = 
{
	IOControlCode_t8A59BB74289B0C9BBB1659E249E54BC5A205D6B9::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3176 = { sizeof (IPProtectionLevel_t63BF0274CCC5A1BFF42B658316B3092B8C0AA95E)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3176[5] = 
{
	IPProtectionLevel_t63BF0274CCC5A1BFF42B658316B3092B8C0AA95E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3177 = { sizeof (LingerOption_tC6A8E9C30F48D9C07C38B2730012ECA6067723C7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3177[2] = 
{
	LingerOption_tC6A8E9C30F48D9C07C38B2730012ECA6067723C7::get_offset_of_enabled_0(),
	LingerOption_tC6A8E9C30F48D9C07C38B2730012ECA6067723C7::get_offset_of_lingerTime_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3178 = { sizeof (MulticastOption_tB21F96F80CEA6CF2F8C72A081C74375B1CEC5999), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3179 = { sizeof (NetworkStream_t362D0CD0C74C2F5CBD02905C9422E4240872ADCA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3179[8] = 
{
	NetworkStream_t362D0CD0C74C2F5CBD02905C9422E4240872ADCA::get_offset_of_m_StreamSocket_5(),
	NetworkStream_t362D0CD0C74C2F5CBD02905C9422E4240872ADCA::get_offset_of_m_Readable_6(),
	NetworkStream_t362D0CD0C74C2F5CBD02905C9422E4240872ADCA::get_offset_of_m_Writeable_7(),
	NetworkStream_t362D0CD0C74C2F5CBD02905C9422E4240872ADCA::get_offset_of_m_OwnsSocket_8(),
	NetworkStream_t362D0CD0C74C2F5CBD02905C9422E4240872ADCA::get_offset_of_m_CloseTimeout_9(),
	NetworkStream_t362D0CD0C74C2F5CBD02905C9422E4240872ADCA::get_offset_of_m_CleanedUp_10(),
	NetworkStream_t362D0CD0C74C2F5CBD02905C9422E4240872ADCA::get_offset_of_m_CurrentReadTimeout_11(),
	NetworkStream_t362D0CD0C74C2F5CBD02905C9422E4240872ADCA::get_offset_of_m_CurrentWriteTimeout_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3180 = { sizeof (ProtocolType_t20E72BC88D85E41793731DC987F8F04F312D66DD)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3180[26] = 
{
	ProtocolType_t20E72BC88D85E41793731DC987F8F04F312D66DD::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3181 = { sizeof (SelectMode_t384C0C7786507E841593ADDA6785DF0001C06B7B)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3181[4] = 
{
	SelectMode_t384C0C7786507E841593ADDA6785DF0001C06B7B::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3182 = { sizeof (Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8), -1, sizeof(Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3182[37] = 
{
	Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8_StaticFields::get_offset_of_s_InternalSyncObject_0(),
	Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8_StaticFields::get_offset_of_s_SupportsIPv4_1(),
	Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8_StaticFields::get_offset_of_s_SupportsIPv6_2(),
	Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8_StaticFields::get_offset_of_s_OSSupportsIPv6_3(),
	Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8_StaticFields::get_offset_of_s_Initialized_4(),
	Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8_StaticFields::get_offset_of_s_LoggingEnabled_5(),
	Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8::get_offset_of_is_closed_6(),
	Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8::get_offset_of_is_listening_7(),
	Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8::get_offset_of_useOverlappedIO_8(),
	Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8::get_offset_of_linger_timeout_9(),
	Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8::get_offset_of_addressFamily_10(),
	Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8::get_offset_of_socketType_11(),
	Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8::get_offset_of_protocolType_12(),
	Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8::get_offset_of_m_Handle_13(),
	Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8::get_offset_of_seed_endpoint_14(),
	Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8::get_offset_of_ReadSem_15(),
	Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8::get_offset_of_WriteSem_16(),
	Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8::get_offset_of_is_blocking_17(),
	Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8::get_offset_of_is_bound_18(),
	Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8::get_offset_of_is_connected_19(),
	Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8::get_offset_of_m_IntCleanedUp_20(),
	Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8::get_offset_of_connect_in_progress_21(),
	Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8_StaticFields::get_offset_of_AcceptAsyncCallback_22(),
	Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8_StaticFields::get_offset_of_BeginAcceptCallback_23(),
	Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8_StaticFields::get_offset_of_BeginAcceptReceiveCallback_24(),
	Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8_StaticFields::get_offset_of_ConnectAsyncCallback_25(),
	Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8_StaticFields::get_offset_of_BeginConnectCallback_26(),
	Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8_StaticFields::get_offset_of_DisconnectAsyncCallback_27(),
	Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8_StaticFields::get_offset_of_BeginDisconnectCallback_28(),
	Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8_StaticFields::get_offset_of_ReceiveAsyncCallback_29(),
	Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8_StaticFields::get_offset_of_BeginReceiveCallback_30(),
	Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8_StaticFields::get_offset_of_BeginReceiveGenericCallback_31(),
	Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8_StaticFields::get_offset_of_ReceiveFromAsyncCallback_32(),
	Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8_StaticFields::get_offset_of_BeginReceiveFromCallback_33(),
	Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8_StaticFields::get_offset_of_SendAsyncCallback_34(),
	Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8_StaticFields::get_offset_of_BeginSendGenericCallback_35(),
	Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8_StaticFields::get_offset_of_SendToAsyncCallback_36(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3183 = { sizeof (WSABUF_tFC99449E36506806B55A93B6293AC7D2D10D3CEE)+ sizeof (RuntimeObject), sizeof(WSABUF_tFC99449E36506806B55A93B6293AC7D2D10D3CEE ), 0, 0 };
extern const int32_t g_FieldOffsetTable3183[2] = 
{
	WSABUF_tFC99449E36506806B55A93B6293AC7D2D10D3CEE::get_offset_of_len_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WSABUF_tFC99449E36506806B55A93B6293AC7D2D10D3CEE::get_offset_of_buf_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3184 = { sizeof (U3CU3Ec_t25DDEAF3ECC3E1ECBECC88BCA00EE02098BC3F7E), -1, sizeof(U3CU3Ec_t25DDEAF3ECC3E1ECBECC88BCA00EE02098BC3F7E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3184[3] = 
{
	U3CU3Ec_t25DDEAF3ECC3E1ECBECC88BCA00EE02098BC3F7E_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t25DDEAF3ECC3E1ECBECC88BCA00EE02098BC3F7E_StaticFields::get_offset_of_U3CU3E9__239_0_1(),
	U3CU3Ec_t25DDEAF3ECC3E1ECBECC88BCA00EE02098BC3F7E_StaticFields::get_offset_of_U3CU3E9__241_0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3185 = { sizeof (U3CU3Ec__DisplayClass242_0_tA32CA02257AF703718D32CE05809EB89C1614997), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3185[1] = 
{
	U3CU3Ec__DisplayClass242_0_tA32CA02257AF703718D32CE05809EB89C1614997::get_offset_of_sent_so_far_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3186 = { sizeof (U3CU3Ec__DisplayClass298_0_tCCC599BC4750E6215E11C91C2037B4694B3E23E7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3186[3] = 
{
	U3CU3Ec__DisplayClass298_0_tCCC599BC4750E6215E11C91C2037B4694B3E23E7::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass298_0_tCCC599BC4750E6215E11C91C2037B4694B3E23E7::get_offset_of_job_1(),
	U3CU3Ec__DisplayClass298_0_tCCC599BC4750E6215E11C91C2037B4694B3E23E7::get_offset_of_handle_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3187 = { sizeof (SocketAsyncOperation_t160EA51A781C803FD186EFA21B78F2A76F603FAF)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3187[11] = 
{
	SocketAsyncOperation_t160EA51A781C803FD186EFA21B78F2A76F603FAF::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3188 = { sizeof (SocketError_t0157BEC7F0A26C8FC31D392B2B7C6CFCD695D5E7)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3188[48] = 
{
	SocketError_t0157BEC7F0A26C8FC31D392B2B7C6CFCD695D5E7::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3189 = { sizeof (SocketFlags_t77581B58FF9A1A1D3E3270EDE83E4CAD3947F809)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3189[11] = 
{
	SocketFlags_t77581B58FF9A1A1D3E3270EDE83E4CAD3947F809::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3190 = { sizeof (SocketOptionLevel_t75F67243F6A4311CE8731B9A344FECD8186B3B21)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3190[6] = 
{
	SocketOptionLevel_t75F67243F6A4311CE8731B9A344FECD8186B3B21::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3191 = { sizeof (SocketOptionName_t11A763BEFF673A081DA61B8A7B1DF11909153B28)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3191[47] = 
{
	SocketOptionName_t11A763BEFF673A081DA61B8A7B1DF11909153B28::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3192 = { sizeof (SocketShutdown_tC1C26BD51DCA13F2A5314DAA97701EF9B230D957)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3192[4] = 
{
	SocketShutdown_tC1C26BD51DCA13F2A5314DAA97701EF9B230D957::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3193 = { sizeof (SocketType_tCD56A18D4C7B43BF166E5C8B4B456BD646DF5775)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3193[7] = 
{
	SocketType_tCD56A18D4C7B43BF166E5C8B4B456BD646DF5775::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3194 = { sizeof (TcpClient_t8BC37A84681D1839590AE10B14C25BA473063EDB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3194[5] = 
{
	TcpClient_t8BC37A84681D1839590AE10B14C25BA473063EDB::get_offset_of_m_ClientSocket_0(),
	TcpClient_t8BC37A84681D1839590AE10B14C25BA473063EDB::get_offset_of_m_Active_1(),
	TcpClient_t8BC37A84681D1839590AE10B14C25BA473063EDB::get_offset_of_m_DataStream_2(),
	TcpClient_t8BC37A84681D1839590AE10B14C25BA473063EDB::get_offset_of_m_Family_3(),
	TcpClient_t8BC37A84681D1839590AE10B14C25BA473063EDB::get_offset_of_m_CleanedUp_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3195 = { sizeof (UdpClient_t94741C2FBA0D9E3270ABDDBA57811D00881D5641), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3195[6] = 
{
	UdpClient_t94741C2FBA0D9E3270ABDDBA57811D00881D5641::get_offset_of_m_ClientSocket_0(),
	UdpClient_t94741C2FBA0D9E3270ABDDBA57811D00881D5641::get_offset_of_m_Active_1(),
	UdpClient_t94741C2FBA0D9E3270ABDDBA57811D00881D5641::get_offset_of_m_Buffer_2(),
	UdpClient_t94741C2FBA0D9E3270ABDDBA57811D00881D5641::get_offset_of_m_Family_3(),
	UdpClient_t94741C2FBA0D9E3270ABDDBA57811D00881D5641::get_offset_of_m_CleanedUp_4(),
	UdpClient_t94741C2FBA0D9E3270ABDDBA57811D00881D5641::get_offset_of_m_IsBroadcast_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3196 = { sizeof (SafeSocketHandle_t9A33B4DCE2012075A5D6D355D323A05E7F16329A), sizeof(void*), sizeof(SafeSocketHandle_t9A33B4DCE2012075A5D6D355D323A05E7F16329A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3196[4] = 
{
	SafeSocketHandle_t9A33B4DCE2012075A5D6D355D323A05E7F16329A::get_offset_of_blocking_threads_6(),
	SafeSocketHandle_t9A33B4DCE2012075A5D6D355D323A05E7F16329A::get_offset_of_threads_stacktraces_7(),
	SafeSocketHandle_t9A33B4DCE2012075A5D6D355D323A05E7F16329A::get_offset_of_in_cleanup_8(),
	SafeSocketHandle_t9A33B4DCE2012075A5D6D355D323A05E7F16329A_StaticFields::get_offset_of_THROW_ON_ABORT_RETRIES_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3197 = { sizeof (SocketAsyncEventArgs_t5E05ABA36B4740D22D5E746DC4E7763AA448CAA7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3197[17] = 
{
	SocketAsyncEventArgs_t5E05ABA36B4740D22D5E746DC4E7763AA448CAA7::get_offset_of_disposed_1(),
	SocketAsyncEventArgs_t5E05ABA36B4740D22D5E746DC4E7763AA448CAA7::get_offset_of_in_progress_2(),
	SocketAsyncEventArgs_t5E05ABA36B4740D22D5E746DC4E7763AA448CAA7::get_offset_of_remote_ep_3(),
	SocketAsyncEventArgs_t5E05ABA36B4740D22D5E746DC4E7763AA448CAA7::get_offset_of_current_socket_4(),
	SocketAsyncEventArgs_t5E05ABA36B4740D22D5E746DC4E7763AA448CAA7::get_offset_of_socket_async_result_5(),
	SocketAsyncEventArgs_t5E05ABA36B4740D22D5E746DC4E7763AA448CAA7::get_offset_of_U3CConnectByNameErrorU3Ek__BackingField_6(),
	SocketAsyncEventArgs_t5E05ABA36B4740D22D5E746DC4E7763AA448CAA7::get_offset_of_U3CAcceptSocketU3Ek__BackingField_7(),
	SocketAsyncEventArgs_t5E05ABA36B4740D22D5E746DC4E7763AA448CAA7::get_offset_of_U3CBufferU3Ek__BackingField_8(),
	SocketAsyncEventArgs_t5E05ABA36B4740D22D5E746DC4E7763AA448CAA7::get_offset_of_m_BufferList_9(),
	SocketAsyncEventArgs_t5E05ABA36B4740D22D5E746DC4E7763AA448CAA7::get_offset_of_U3CBytesTransferredU3Ek__BackingField_10(),
	SocketAsyncEventArgs_t5E05ABA36B4740D22D5E746DC4E7763AA448CAA7::get_offset_of_U3CCountU3Ek__BackingField_11(),
	SocketAsyncEventArgs_t5E05ABA36B4740D22D5E746DC4E7763AA448CAA7::get_offset_of_U3CLastOperationU3Ek__BackingField_12(),
	SocketAsyncEventArgs_t5E05ABA36B4740D22D5E746DC4E7763AA448CAA7::get_offset_of_U3COffsetU3Ek__BackingField_13(),
	SocketAsyncEventArgs_t5E05ABA36B4740D22D5E746DC4E7763AA448CAA7::get_offset_of_U3CSendPacketsSendSizeU3Ek__BackingField_14(),
	SocketAsyncEventArgs_t5E05ABA36B4740D22D5E746DC4E7763AA448CAA7::get_offset_of_U3CSocketErrorU3Ek__BackingField_15(),
	SocketAsyncEventArgs_t5E05ABA36B4740D22D5E746DC4E7763AA448CAA7::get_offset_of_U3CUserTokenU3Ek__BackingField_16(),
	SocketAsyncEventArgs_t5E05ABA36B4740D22D5E746DC4E7763AA448CAA7::get_offset_of_Completed_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3198 = { sizeof (SocketAsyncResult_t63145D172556590482549D41328C0668E19CB69C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3198[18] = 
{
	SocketAsyncResult_t63145D172556590482549D41328C0668E19CB69C::get_offset_of_socket_5(),
	SocketAsyncResult_t63145D172556590482549D41328C0668E19CB69C::get_offset_of_operation_6(),
	SocketAsyncResult_t63145D172556590482549D41328C0668E19CB69C::get_offset_of_DelayedException_7(),
	SocketAsyncResult_t63145D172556590482549D41328C0668E19CB69C::get_offset_of_EndPoint_8(),
	SocketAsyncResult_t63145D172556590482549D41328C0668E19CB69C::get_offset_of_Buffer_9(),
	SocketAsyncResult_t63145D172556590482549D41328C0668E19CB69C::get_offset_of_Offset_10(),
	SocketAsyncResult_t63145D172556590482549D41328C0668E19CB69C::get_offset_of_Size_11(),
	SocketAsyncResult_t63145D172556590482549D41328C0668E19CB69C::get_offset_of_SockFlags_12(),
	SocketAsyncResult_t63145D172556590482549D41328C0668E19CB69C::get_offset_of_AcceptSocket_13(),
	SocketAsyncResult_t63145D172556590482549D41328C0668E19CB69C::get_offset_of_Addresses_14(),
	SocketAsyncResult_t63145D172556590482549D41328C0668E19CB69C::get_offset_of_Port_15(),
	SocketAsyncResult_t63145D172556590482549D41328C0668E19CB69C::get_offset_of_Buffers_16(),
	SocketAsyncResult_t63145D172556590482549D41328C0668E19CB69C::get_offset_of_ReuseSocket_17(),
	SocketAsyncResult_t63145D172556590482549D41328C0668E19CB69C::get_offset_of_CurrentAddress_18(),
	SocketAsyncResult_t63145D172556590482549D41328C0668E19CB69C::get_offset_of_AcceptedSocket_19(),
	SocketAsyncResult_t63145D172556590482549D41328C0668E19CB69C::get_offset_of_Total_20(),
	SocketAsyncResult_t63145D172556590482549D41328C0668E19CB69C::get_offset_of_error_21(),
	SocketAsyncResult_t63145D172556590482549D41328C0668E19CB69C::get_offset_of_EndCalled_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3199 = { sizeof (U3CU3Ec_tDC6DACE4FF21DC643EA6C91369A052690CF187DC), -1, sizeof(U3CU3Ec_tDC6DACE4FF21DC643EA6C91369A052690CF187DC_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3199[2] = 
{
	U3CU3Ec_tDC6DACE4FF21DC643EA6C91369A052690CF187DC_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tDC6DACE4FF21DC643EA6C91369A052690CF187DC_StaticFields::get_offset_of_U3CU3E9__27_0_1(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
