﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Org.BouncyCastle.Asn1.Asn1Encodable
struct Asn1Encodable_t390FCE82F5F52E41A7320BBF2E8BF18DA1622A8B;
// Org.BouncyCastle.Asn1.Asn1Object
struct Asn1Object_tB780E50883209D5C975E83B3D16257DFC3CCE36D;
// Org.BouncyCastle.Asn1.Asn1OctetString
struct Asn1OctetString_t2088F9E88E4C97BB9CF3B2FFC53CFBAF9B8B79CB;
// Org.BouncyCastle.Asn1.Asn1Sequence
struct Asn1Sequence_t292AE1C416511A0DE153DE285AEE498B50523B85;
// Org.BouncyCastle.Asn1.Asn1Set
struct Asn1Set_tD51325409687C84A095F7692B251BDE783A1A4A7;
// Org.BouncyCastle.Asn1.DerBitString
struct DerBitString_tCA371BCECC9BA7643D0127424F54EE10C1B19A64;
// Org.BouncyCastle.Asn1.DerBoolean
struct DerBoolean_tAF849A79A0BCE5C3F0DBB8BE9F6BA279E3D1A8CE;
// Org.BouncyCastle.Asn1.DerEnumerated[]
struct DerEnumeratedU5BU5D_tEBACB932DA336D53CCA9EEA8D863431522CD024D;
// Org.BouncyCastle.Asn1.DerInteger
struct DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A;
// Org.BouncyCastle.Asn1.DerObjectIdentifier
struct DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441;
// Org.BouncyCastle.Asn1.Ocsp.OcspResponseStatus
struct OcspResponseStatus_t40E3D3B60B45A3E985B8678DCD3E60445517912F;
// Org.BouncyCastle.Asn1.Ocsp.ResponseBytes
struct ResponseBytes_t0E85F6E803AB354A78B7EBABB885F182805BAE25;
// Org.BouncyCastle.Asn1.Pkcs.ContentInfo
struct ContentInfo_t97148D828054F7B5E1586B21837C138F9EDE4C08;
// Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier
struct AlgorithmIdentifier_tDE7123E83DAD8C219BBB99DAE8CAAD8A2AE64EFB;
// Org.BouncyCastle.Asn1.X509.SubjectPublicKeyInfo
struct SubjectPublicKeyInfo_tE61D9687E07635F3A249A3FBBBE6E2D9C4B73A7C;
// Org.BouncyCastle.Asn1.X509.TbsCertificateStructure
struct TbsCertificateStructure_tDA73857A74B4B8375807FCBE2099FDFFA361D598;
// Org.BouncyCastle.Asn1.X509.Time
struct Time_t80739D7454AE536E0F9A1C9D5B0620C3C8378307;
// Org.BouncyCastle.Asn1.X509.X509Extensions
struct X509Extensions_tBC4870829033B1A0E0F297F32C0B2B29C1618E44;
// Org.BouncyCastle.Asn1.X509.X509Name
struct X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E;
// Org.BouncyCastle.Asn1.X509.X509NameEntryConverter
struct X509NameEntryConverter_t6B3AE1DD9E7B91F1C0039575CEAC00CAE0972DED;
// Org.BouncyCastle.Asn1.X9.DHValidationParms
struct DHValidationParms_t8121E846FBD95D655031A424C5AECD365F767E34;
// Org.BouncyCastle.Asn1.X9.X9ECParameters
struct X9ECParameters_t818A7DD4D13D91F0D36622C6C117D7E9A10E51C3;
// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder
struct X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE;
// Org.BouncyCastle.Math.BigInteger
struct BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91;
// System.Boolean[]
struct BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Hashtable
struct Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.Collections.IList
struct IList_tA637AB426E16F84F84ACC2813BDCF3A0414AF0AA;
// System.String
struct String_t;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef ASN1ENCODABLE_T390FCE82F5F52E41A7320BBF2E8BF18DA1622A8B_H
#define ASN1ENCODABLE_T390FCE82F5F52E41A7320BBF2E8BF18DA1622A8B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.Asn1Encodable
struct  Asn1Encodable_t390FCE82F5F52E41A7320BBF2E8BF18DA1622A8B  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASN1ENCODABLE_T390FCE82F5F52E41A7320BBF2E8BF18DA1622A8B_H
#ifndef OIWOBJECTIDENTIFIERS_TC11A2DA17697C1B77A133921FED0B4C55F603DFE_H
#define OIWOBJECTIDENTIFIERS_TC11A2DA17697C1B77A133921FED0B4C55F603DFE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.Oiw.OiwObjectIdentifiers
struct  OiwObjectIdentifiers_tC11A2DA17697C1B77A133921FED0B4C55F603DFE  : public RuntimeObject
{
public:

public:
};

struct OiwObjectIdentifiers_tC11A2DA17697C1B77A133921FED0B4C55F603DFE_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Oiw.OiwObjectIdentifiers::MD4WithRsa
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___MD4WithRsa_0;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Oiw.OiwObjectIdentifiers::MD5WithRsa
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___MD5WithRsa_1;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Oiw.OiwObjectIdentifiers::MD4WithRsaEncryption
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___MD4WithRsaEncryption_2;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Oiw.OiwObjectIdentifiers::DesEcb
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___DesEcb_3;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Oiw.OiwObjectIdentifiers::DesCbc
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___DesCbc_4;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Oiw.OiwObjectIdentifiers::DesOfb
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___DesOfb_5;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Oiw.OiwObjectIdentifiers::DesCfb
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___DesCfb_6;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Oiw.OiwObjectIdentifiers::DesEde
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___DesEde_7;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Oiw.OiwObjectIdentifiers::IdSha1
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdSha1_8;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Oiw.OiwObjectIdentifiers::DsaWithSha1
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___DsaWithSha1_9;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Oiw.OiwObjectIdentifiers::Sha1WithRsa
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___Sha1WithRsa_10;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Oiw.OiwObjectIdentifiers::ElGamalAlgorithm
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___ElGamalAlgorithm_11;

public:
	inline static int32_t get_offset_of_MD4WithRsa_0() { return static_cast<int32_t>(offsetof(OiwObjectIdentifiers_tC11A2DA17697C1B77A133921FED0B4C55F603DFE_StaticFields, ___MD4WithRsa_0)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_MD4WithRsa_0() const { return ___MD4WithRsa_0; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_MD4WithRsa_0() { return &___MD4WithRsa_0; }
	inline void set_MD4WithRsa_0(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___MD4WithRsa_0 = value;
		Il2CppCodeGenWriteBarrier((&___MD4WithRsa_0), value);
	}

	inline static int32_t get_offset_of_MD5WithRsa_1() { return static_cast<int32_t>(offsetof(OiwObjectIdentifiers_tC11A2DA17697C1B77A133921FED0B4C55F603DFE_StaticFields, ___MD5WithRsa_1)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_MD5WithRsa_1() const { return ___MD5WithRsa_1; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_MD5WithRsa_1() { return &___MD5WithRsa_1; }
	inline void set_MD5WithRsa_1(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___MD5WithRsa_1 = value;
		Il2CppCodeGenWriteBarrier((&___MD5WithRsa_1), value);
	}

	inline static int32_t get_offset_of_MD4WithRsaEncryption_2() { return static_cast<int32_t>(offsetof(OiwObjectIdentifiers_tC11A2DA17697C1B77A133921FED0B4C55F603DFE_StaticFields, ___MD4WithRsaEncryption_2)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_MD4WithRsaEncryption_2() const { return ___MD4WithRsaEncryption_2; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_MD4WithRsaEncryption_2() { return &___MD4WithRsaEncryption_2; }
	inline void set_MD4WithRsaEncryption_2(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___MD4WithRsaEncryption_2 = value;
		Il2CppCodeGenWriteBarrier((&___MD4WithRsaEncryption_2), value);
	}

	inline static int32_t get_offset_of_DesEcb_3() { return static_cast<int32_t>(offsetof(OiwObjectIdentifiers_tC11A2DA17697C1B77A133921FED0B4C55F603DFE_StaticFields, ___DesEcb_3)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_DesEcb_3() const { return ___DesEcb_3; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_DesEcb_3() { return &___DesEcb_3; }
	inline void set_DesEcb_3(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___DesEcb_3 = value;
		Il2CppCodeGenWriteBarrier((&___DesEcb_3), value);
	}

	inline static int32_t get_offset_of_DesCbc_4() { return static_cast<int32_t>(offsetof(OiwObjectIdentifiers_tC11A2DA17697C1B77A133921FED0B4C55F603DFE_StaticFields, ___DesCbc_4)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_DesCbc_4() const { return ___DesCbc_4; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_DesCbc_4() { return &___DesCbc_4; }
	inline void set_DesCbc_4(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___DesCbc_4 = value;
		Il2CppCodeGenWriteBarrier((&___DesCbc_4), value);
	}

	inline static int32_t get_offset_of_DesOfb_5() { return static_cast<int32_t>(offsetof(OiwObjectIdentifiers_tC11A2DA17697C1B77A133921FED0B4C55F603DFE_StaticFields, ___DesOfb_5)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_DesOfb_5() const { return ___DesOfb_5; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_DesOfb_5() { return &___DesOfb_5; }
	inline void set_DesOfb_5(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___DesOfb_5 = value;
		Il2CppCodeGenWriteBarrier((&___DesOfb_5), value);
	}

	inline static int32_t get_offset_of_DesCfb_6() { return static_cast<int32_t>(offsetof(OiwObjectIdentifiers_tC11A2DA17697C1B77A133921FED0B4C55F603DFE_StaticFields, ___DesCfb_6)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_DesCfb_6() const { return ___DesCfb_6; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_DesCfb_6() { return &___DesCfb_6; }
	inline void set_DesCfb_6(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___DesCfb_6 = value;
		Il2CppCodeGenWriteBarrier((&___DesCfb_6), value);
	}

	inline static int32_t get_offset_of_DesEde_7() { return static_cast<int32_t>(offsetof(OiwObjectIdentifiers_tC11A2DA17697C1B77A133921FED0B4C55F603DFE_StaticFields, ___DesEde_7)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_DesEde_7() const { return ___DesEde_7; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_DesEde_7() { return &___DesEde_7; }
	inline void set_DesEde_7(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___DesEde_7 = value;
		Il2CppCodeGenWriteBarrier((&___DesEde_7), value);
	}

	inline static int32_t get_offset_of_IdSha1_8() { return static_cast<int32_t>(offsetof(OiwObjectIdentifiers_tC11A2DA17697C1B77A133921FED0B4C55F603DFE_StaticFields, ___IdSha1_8)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdSha1_8() const { return ___IdSha1_8; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdSha1_8() { return &___IdSha1_8; }
	inline void set_IdSha1_8(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdSha1_8 = value;
		Il2CppCodeGenWriteBarrier((&___IdSha1_8), value);
	}

	inline static int32_t get_offset_of_DsaWithSha1_9() { return static_cast<int32_t>(offsetof(OiwObjectIdentifiers_tC11A2DA17697C1B77A133921FED0B4C55F603DFE_StaticFields, ___DsaWithSha1_9)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_DsaWithSha1_9() const { return ___DsaWithSha1_9; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_DsaWithSha1_9() { return &___DsaWithSha1_9; }
	inline void set_DsaWithSha1_9(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___DsaWithSha1_9 = value;
		Il2CppCodeGenWriteBarrier((&___DsaWithSha1_9), value);
	}

	inline static int32_t get_offset_of_Sha1WithRsa_10() { return static_cast<int32_t>(offsetof(OiwObjectIdentifiers_tC11A2DA17697C1B77A133921FED0B4C55F603DFE_StaticFields, ___Sha1WithRsa_10)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_Sha1WithRsa_10() const { return ___Sha1WithRsa_10; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_Sha1WithRsa_10() { return &___Sha1WithRsa_10; }
	inline void set_Sha1WithRsa_10(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___Sha1WithRsa_10 = value;
		Il2CppCodeGenWriteBarrier((&___Sha1WithRsa_10), value);
	}

	inline static int32_t get_offset_of_ElGamalAlgorithm_11() { return static_cast<int32_t>(offsetof(OiwObjectIdentifiers_tC11A2DA17697C1B77A133921FED0B4C55F603DFE_StaticFields, ___ElGamalAlgorithm_11)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_ElGamalAlgorithm_11() const { return ___ElGamalAlgorithm_11; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_ElGamalAlgorithm_11() { return &___ElGamalAlgorithm_11; }
	inline void set_ElGamalAlgorithm_11(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___ElGamalAlgorithm_11 = value;
		Il2CppCodeGenWriteBarrier((&___ElGamalAlgorithm_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OIWOBJECTIDENTIFIERS_TC11A2DA17697C1B77A133921FED0B4C55F603DFE_H
#ifndef PKCSOBJECTIDENTIFIERS_T56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_H
#define PKCSOBJECTIDENTIFIERS_T56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers
struct  PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04  : public RuntimeObject
{
public:

public:
};

struct PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::RsaEncryption
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___RsaEncryption_0;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::MD2WithRsaEncryption
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___MD2WithRsaEncryption_1;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::MD4WithRsaEncryption
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___MD4WithRsaEncryption_2;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::MD5WithRsaEncryption
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___MD5WithRsaEncryption_3;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::Sha1WithRsaEncryption
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___Sha1WithRsaEncryption_4;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::SrsaOaepEncryptionSet
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___SrsaOaepEncryptionSet_5;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdRsaesOaep
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdRsaesOaep_6;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdMgf1
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdMgf1_7;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdPSpecified
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdPSpecified_8;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdRsassaPss
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdRsassaPss_9;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::Sha256WithRsaEncryption
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___Sha256WithRsaEncryption_10;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::Sha384WithRsaEncryption
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___Sha384WithRsaEncryption_11;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::Sha512WithRsaEncryption
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___Sha512WithRsaEncryption_12;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::Sha224WithRsaEncryption
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___Sha224WithRsaEncryption_13;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::DhKeyAgreement
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___DhKeyAgreement_14;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::PbeWithMD2AndDesCbc
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___PbeWithMD2AndDesCbc_15;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::PbeWithMD2AndRC2Cbc
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___PbeWithMD2AndRC2Cbc_16;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::PbeWithMD5AndDesCbc
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___PbeWithMD5AndDesCbc_17;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::PbeWithMD5AndRC2Cbc
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___PbeWithMD5AndRC2Cbc_18;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::PbeWithSha1AndDesCbc
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___PbeWithSha1AndDesCbc_19;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::PbeWithSha1AndRC2Cbc
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___PbeWithSha1AndRC2Cbc_20;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdPbeS2
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdPbeS2_21;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdPbkdf2
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdPbkdf2_22;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::DesEde3Cbc
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___DesEde3Cbc_23;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::RC2Cbc
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___RC2Cbc_24;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::MD2
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___MD2_25;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::MD4
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___MD4_26;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::MD5
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___MD5_27;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdHmacWithSha1
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdHmacWithSha1_28;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdHmacWithSha224
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdHmacWithSha224_29;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdHmacWithSha256
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdHmacWithSha256_30;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdHmacWithSha384
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdHmacWithSha384_31;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdHmacWithSha512
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdHmacWithSha512_32;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::Data
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___Data_33;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::SignedData
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___SignedData_34;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::EnvelopedData
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___EnvelopedData_35;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::SignedAndEnvelopedData
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___SignedAndEnvelopedData_36;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::DigestedData
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___DigestedData_37;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::EncryptedData
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___EncryptedData_38;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::Pkcs9AtEmailAddress
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___Pkcs9AtEmailAddress_39;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::Pkcs9AtUnstructuredName
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___Pkcs9AtUnstructuredName_40;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::Pkcs9AtContentType
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___Pkcs9AtContentType_41;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::Pkcs9AtMessageDigest
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___Pkcs9AtMessageDigest_42;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::Pkcs9AtSigningTime
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___Pkcs9AtSigningTime_43;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::Pkcs9AtCounterSignature
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___Pkcs9AtCounterSignature_44;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::Pkcs9AtChallengePassword
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___Pkcs9AtChallengePassword_45;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::Pkcs9AtUnstructuredAddress
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___Pkcs9AtUnstructuredAddress_46;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::Pkcs9AtExtendedCertificateAttributes
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___Pkcs9AtExtendedCertificateAttributes_47;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::Pkcs9AtSigningDescription
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___Pkcs9AtSigningDescription_48;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::Pkcs9AtExtensionRequest
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___Pkcs9AtExtensionRequest_49;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::Pkcs9AtSmimeCapabilities
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___Pkcs9AtSmimeCapabilities_50;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdSmime
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdSmime_51;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::Pkcs9AtFriendlyName
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___Pkcs9AtFriendlyName_52;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::Pkcs9AtLocalKeyID
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___Pkcs9AtLocalKeyID_53;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::X509CertType
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___X509CertType_54;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::X509Certificate
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___X509Certificate_55;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::SdsiCertificate
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___SdsiCertificate_56;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::X509Crl
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___X509Crl_57;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdAlg
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdAlg_58;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdAlgEsdh
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdAlgEsdh_59;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdAlgCms3DesWrap
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdAlgCms3DesWrap_60;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdAlgCmsRC2Wrap
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdAlgCmsRC2Wrap_61;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdAlgPwriKek
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdAlgPwriKek_62;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdAlgSsdh
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdAlgSsdh_63;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdRsaKem
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdRsaKem_64;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::PreferSignedData
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___PreferSignedData_65;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::CannotDecryptAny
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___CannotDecryptAny_66;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::SmimeCapabilitiesVersions
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___SmimeCapabilitiesVersions_67;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdAAReceiptRequest
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdAAReceiptRequest_68;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdCTAuthData
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdCTAuthData_69;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdCTTstInfo
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdCTTstInfo_70;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdCTCompressedData
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdCTCompressedData_71;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdCTAuthEnvelopedData
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdCTAuthEnvelopedData_72;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdCTTimestampedData
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdCTTimestampedData_73;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdCtiEtsProofOfOrigin
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdCtiEtsProofOfOrigin_74;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdCtiEtsProofOfReceipt
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdCtiEtsProofOfReceipt_75;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdCtiEtsProofOfDelivery
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdCtiEtsProofOfDelivery_76;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdCtiEtsProofOfSender
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdCtiEtsProofOfSender_77;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdCtiEtsProofOfApproval
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdCtiEtsProofOfApproval_78;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdCtiEtsProofOfCreation
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdCtiEtsProofOfCreation_79;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdAAContentHint
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdAAContentHint_80;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdAAMsgSigDigest
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdAAMsgSigDigest_81;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdAAContentReference
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdAAContentReference_82;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdAAEncrypKeyPref
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdAAEncrypKeyPref_83;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdAASigningCertificate
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdAASigningCertificate_84;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdAASigningCertificateV2
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdAASigningCertificateV2_85;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdAAContentIdentifier
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdAAContentIdentifier_86;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdAASignatureTimeStampToken
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdAASignatureTimeStampToken_87;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdAAEtsSigPolicyID
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdAAEtsSigPolicyID_88;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdAAEtsCommitmentType
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdAAEtsCommitmentType_89;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdAAEtsSignerLocation
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdAAEtsSignerLocation_90;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdAAEtsSignerAttr
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdAAEtsSignerAttr_91;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdAAEtsOtherSigCert
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdAAEtsOtherSigCert_92;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdAAEtsContentTimestamp
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdAAEtsContentTimestamp_93;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdAAEtsCertificateRefs
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdAAEtsCertificateRefs_94;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdAAEtsRevocationRefs
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdAAEtsRevocationRefs_95;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdAAEtsCertValues
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdAAEtsCertValues_96;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdAAEtsRevocationValues
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdAAEtsRevocationValues_97;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdAAEtsEscTimeStamp
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdAAEtsEscTimeStamp_98;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdAAEtsCertCrlTimestamp
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdAAEtsCertCrlTimestamp_99;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdAAEtsArchiveTimestamp
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdAAEtsArchiveTimestamp_100;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdAASigPolicyID
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdAASigPolicyID_101;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdAACommitmentType
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdAACommitmentType_102;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdAASignerLocation
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdAASignerLocation_103;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdAAOtherSigCert
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdAAOtherSigCert_104;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdSpqEtsUri
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdSpqEtsUri_105;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdSpqEtsUNotice
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdSpqEtsUNotice_106;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::KeyBag
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___KeyBag_107;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::Pkcs8ShroudedKeyBag
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___Pkcs8ShroudedKeyBag_108;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::CertBag
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___CertBag_109;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::CrlBag
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___CrlBag_110;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::SecretBag
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___SecretBag_111;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::SafeContentsBag
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___SafeContentsBag_112;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::PbeWithShaAnd128BitRC4
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___PbeWithShaAnd128BitRC4_113;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::PbeWithShaAnd40BitRC4
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___PbeWithShaAnd40BitRC4_114;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::PbeWithShaAnd3KeyTripleDesCbc
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___PbeWithShaAnd3KeyTripleDesCbc_115;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::PbeWithShaAnd2KeyTripleDesCbc
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___PbeWithShaAnd2KeyTripleDesCbc_116;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::PbeWithShaAnd128BitRC2Cbc
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___PbeWithShaAnd128BitRC2Cbc_117;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::PbewithShaAnd40BitRC2Cbc
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___PbewithShaAnd40BitRC2Cbc_118;

public:
	inline static int32_t get_offset_of_RsaEncryption_0() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___RsaEncryption_0)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_RsaEncryption_0() const { return ___RsaEncryption_0; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_RsaEncryption_0() { return &___RsaEncryption_0; }
	inline void set_RsaEncryption_0(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___RsaEncryption_0 = value;
		Il2CppCodeGenWriteBarrier((&___RsaEncryption_0), value);
	}

	inline static int32_t get_offset_of_MD2WithRsaEncryption_1() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___MD2WithRsaEncryption_1)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_MD2WithRsaEncryption_1() const { return ___MD2WithRsaEncryption_1; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_MD2WithRsaEncryption_1() { return &___MD2WithRsaEncryption_1; }
	inline void set_MD2WithRsaEncryption_1(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___MD2WithRsaEncryption_1 = value;
		Il2CppCodeGenWriteBarrier((&___MD2WithRsaEncryption_1), value);
	}

	inline static int32_t get_offset_of_MD4WithRsaEncryption_2() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___MD4WithRsaEncryption_2)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_MD4WithRsaEncryption_2() const { return ___MD4WithRsaEncryption_2; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_MD4WithRsaEncryption_2() { return &___MD4WithRsaEncryption_2; }
	inline void set_MD4WithRsaEncryption_2(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___MD4WithRsaEncryption_2 = value;
		Il2CppCodeGenWriteBarrier((&___MD4WithRsaEncryption_2), value);
	}

	inline static int32_t get_offset_of_MD5WithRsaEncryption_3() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___MD5WithRsaEncryption_3)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_MD5WithRsaEncryption_3() const { return ___MD5WithRsaEncryption_3; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_MD5WithRsaEncryption_3() { return &___MD5WithRsaEncryption_3; }
	inline void set_MD5WithRsaEncryption_3(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___MD5WithRsaEncryption_3 = value;
		Il2CppCodeGenWriteBarrier((&___MD5WithRsaEncryption_3), value);
	}

	inline static int32_t get_offset_of_Sha1WithRsaEncryption_4() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___Sha1WithRsaEncryption_4)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_Sha1WithRsaEncryption_4() const { return ___Sha1WithRsaEncryption_4; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_Sha1WithRsaEncryption_4() { return &___Sha1WithRsaEncryption_4; }
	inline void set_Sha1WithRsaEncryption_4(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___Sha1WithRsaEncryption_4 = value;
		Il2CppCodeGenWriteBarrier((&___Sha1WithRsaEncryption_4), value);
	}

	inline static int32_t get_offset_of_SrsaOaepEncryptionSet_5() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___SrsaOaepEncryptionSet_5)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_SrsaOaepEncryptionSet_5() const { return ___SrsaOaepEncryptionSet_5; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_SrsaOaepEncryptionSet_5() { return &___SrsaOaepEncryptionSet_5; }
	inline void set_SrsaOaepEncryptionSet_5(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___SrsaOaepEncryptionSet_5 = value;
		Il2CppCodeGenWriteBarrier((&___SrsaOaepEncryptionSet_5), value);
	}

	inline static int32_t get_offset_of_IdRsaesOaep_6() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___IdRsaesOaep_6)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdRsaesOaep_6() const { return ___IdRsaesOaep_6; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdRsaesOaep_6() { return &___IdRsaesOaep_6; }
	inline void set_IdRsaesOaep_6(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdRsaesOaep_6 = value;
		Il2CppCodeGenWriteBarrier((&___IdRsaesOaep_6), value);
	}

	inline static int32_t get_offset_of_IdMgf1_7() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___IdMgf1_7)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdMgf1_7() const { return ___IdMgf1_7; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdMgf1_7() { return &___IdMgf1_7; }
	inline void set_IdMgf1_7(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdMgf1_7 = value;
		Il2CppCodeGenWriteBarrier((&___IdMgf1_7), value);
	}

	inline static int32_t get_offset_of_IdPSpecified_8() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___IdPSpecified_8)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdPSpecified_8() const { return ___IdPSpecified_8; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdPSpecified_8() { return &___IdPSpecified_8; }
	inline void set_IdPSpecified_8(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdPSpecified_8 = value;
		Il2CppCodeGenWriteBarrier((&___IdPSpecified_8), value);
	}

	inline static int32_t get_offset_of_IdRsassaPss_9() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___IdRsassaPss_9)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdRsassaPss_9() const { return ___IdRsassaPss_9; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdRsassaPss_9() { return &___IdRsassaPss_9; }
	inline void set_IdRsassaPss_9(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdRsassaPss_9 = value;
		Il2CppCodeGenWriteBarrier((&___IdRsassaPss_9), value);
	}

	inline static int32_t get_offset_of_Sha256WithRsaEncryption_10() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___Sha256WithRsaEncryption_10)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_Sha256WithRsaEncryption_10() const { return ___Sha256WithRsaEncryption_10; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_Sha256WithRsaEncryption_10() { return &___Sha256WithRsaEncryption_10; }
	inline void set_Sha256WithRsaEncryption_10(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___Sha256WithRsaEncryption_10 = value;
		Il2CppCodeGenWriteBarrier((&___Sha256WithRsaEncryption_10), value);
	}

	inline static int32_t get_offset_of_Sha384WithRsaEncryption_11() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___Sha384WithRsaEncryption_11)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_Sha384WithRsaEncryption_11() const { return ___Sha384WithRsaEncryption_11; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_Sha384WithRsaEncryption_11() { return &___Sha384WithRsaEncryption_11; }
	inline void set_Sha384WithRsaEncryption_11(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___Sha384WithRsaEncryption_11 = value;
		Il2CppCodeGenWriteBarrier((&___Sha384WithRsaEncryption_11), value);
	}

	inline static int32_t get_offset_of_Sha512WithRsaEncryption_12() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___Sha512WithRsaEncryption_12)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_Sha512WithRsaEncryption_12() const { return ___Sha512WithRsaEncryption_12; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_Sha512WithRsaEncryption_12() { return &___Sha512WithRsaEncryption_12; }
	inline void set_Sha512WithRsaEncryption_12(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___Sha512WithRsaEncryption_12 = value;
		Il2CppCodeGenWriteBarrier((&___Sha512WithRsaEncryption_12), value);
	}

	inline static int32_t get_offset_of_Sha224WithRsaEncryption_13() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___Sha224WithRsaEncryption_13)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_Sha224WithRsaEncryption_13() const { return ___Sha224WithRsaEncryption_13; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_Sha224WithRsaEncryption_13() { return &___Sha224WithRsaEncryption_13; }
	inline void set_Sha224WithRsaEncryption_13(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___Sha224WithRsaEncryption_13 = value;
		Il2CppCodeGenWriteBarrier((&___Sha224WithRsaEncryption_13), value);
	}

	inline static int32_t get_offset_of_DhKeyAgreement_14() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___DhKeyAgreement_14)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_DhKeyAgreement_14() const { return ___DhKeyAgreement_14; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_DhKeyAgreement_14() { return &___DhKeyAgreement_14; }
	inline void set_DhKeyAgreement_14(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___DhKeyAgreement_14 = value;
		Il2CppCodeGenWriteBarrier((&___DhKeyAgreement_14), value);
	}

	inline static int32_t get_offset_of_PbeWithMD2AndDesCbc_15() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___PbeWithMD2AndDesCbc_15)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_PbeWithMD2AndDesCbc_15() const { return ___PbeWithMD2AndDesCbc_15; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_PbeWithMD2AndDesCbc_15() { return &___PbeWithMD2AndDesCbc_15; }
	inline void set_PbeWithMD2AndDesCbc_15(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___PbeWithMD2AndDesCbc_15 = value;
		Il2CppCodeGenWriteBarrier((&___PbeWithMD2AndDesCbc_15), value);
	}

	inline static int32_t get_offset_of_PbeWithMD2AndRC2Cbc_16() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___PbeWithMD2AndRC2Cbc_16)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_PbeWithMD2AndRC2Cbc_16() const { return ___PbeWithMD2AndRC2Cbc_16; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_PbeWithMD2AndRC2Cbc_16() { return &___PbeWithMD2AndRC2Cbc_16; }
	inline void set_PbeWithMD2AndRC2Cbc_16(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___PbeWithMD2AndRC2Cbc_16 = value;
		Il2CppCodeGenWriteBarrier((&___PbeWithMD2AndRC2Cbc_16), value);
	}

	inline static int32_t get_offset_of_PbeWithMD5AndDesCbc_17() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___PbeWithMD5AndDesCbc_17)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_PbeWithMD5AndDesCbc_17() const { return ___PbeWithMD5AndDesCbc_17; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_PbeWithMD5AndDesCbc_17() { return &___PbeWithMD5AndDesCbc_17; }
	inline void set_PbeWithMD5AndDesCbc_17(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___PbeWithMD5AndDesCbc_17 = value;
		Il2CppCodeGenWriteBarrier((&___PbeWithMD5AndDesCbc_17), value);
	}

	inline static int32_t get_offset_of_PbeWithMD5AndRC2Cbc_18() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___PbeWithMD5AndRC2Cbc_18)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_PbeWithMD5AndRC2Cbc_18() const { return ___PbeWithMD5AndRC2Cbc_18; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_PbeWithMD5AndRC2Cbc_18() { return &___PbeWithMD5AndRC2Cbc_18; }
	inline void set_PbeWithMD5AndRC2Cbc_18(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___PbeWithMD5AndRC2Cbc_18 = value;
		Il2CppCodeGenWriteBarrier((&___PbeWithMD5AndRC2Cbc_18), value);
	}

	inline static int32_t get_offset_of_PbeWithSha1AndDesCbc_19() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___PbeWithSha1AndDesCbc_19)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_PbeWithSha1AndDesCbc_19() const { return ___PbeWithSha1AndDesCbc_19; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_PbeWithSha1AndDesCbc_19() { return &___PbeWithSha1AndDesCbc_19; }
	inline void set_PbeWithSha1AndDesCbc_19(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___PbeWithSha1AndDesCbc_19 = value;
		Il2CppCodeGenWriteBarrier((&___PbeWithSha1AndDesCbc_19), value);
	}

	inline static int32_t get_offset_of_PbeWithSha1AndRC2Cbc_20() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___PbeWithSha1AndRC2Cbc_20)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_PbeWithSha1AndRC2Cbc_20() const { return ___PbeWithSha1AndRC2Cbc_20; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_PbeWithSha1AndRC2Cbc_20() { return &___PbeWithSha1AndRC2Cbc_20; }
	inline void set_PbeWithSha1AndRC2Cbc_20(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___PbeWithSha1AndRC2Cbc_20 = value;
		Il2CppCodeGenWriteBarrier((&___PbeWithSha1AndRC2Cbc_20), value);
	}

	inline static int32_t get_offset_of_IdPbeS2_21() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___IdPbeS2_21)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdPbeS2_21() const { return ___IdPbeS2_21; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdPbeS2_21() { return &___IdPbeS2_21; }
	inline void set_IdPbeS2_21(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdPbeS2_21 = value;
		Il2CppCodeGenWriteBarrier((&___IdPbeS2_21), value);
	}

	inline static int32_t get_offset_of_IdPbkdf2_22() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___IdPbkdf2_22)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdPbkdf2_22() const { return ___IdPbkdf2_22; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdPbkdf2_22() { return &___IdPbkdf2_22; }
	inline void set_IdPbkdf2_22(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdPbkdf2_22 = value;
		Il2CppCodeGenWriteBarrier((&___IdPbkdf2_22), value);
	}

	inline static int32_t get_offset_of_DesEde3Cbc_23() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___DesEde3Cbc_23)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_DesEde3Cbc_23() const { return ___DesEde3Cbc_23; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_DesEde3Cbc_23() { return &___DesEde3Cbc_23; }
	inline void set_DesEde3Cbc_23(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___DesEde3Cbc_23 = value;
		Il2CppCodeGenWriteBarrier((&___DesEde3Cbc_23), value);
	}

	inline static int32_t get_offset_of_RC2Cbc_24() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___RC2Cbc_24)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_RC2Cbc_24() const { return ___RC2Cbc_24; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_RC2Cbc_24() { return &___RC2Cbc_24; }
	inline void set_RC2Cbc_24(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___RC2Cbc_24 = value;
		Il2CppCodeGenWriteBarrier((&___RC2Cbc_24), value);
	}

	inline static int32_t get_offset_of_MD2_25() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___MD2_25)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_MD2_25() const { return ___MD2_25; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_MD2_25() { return &___MD2_25; }
	inline void set_MD2_25(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___MD2_25 = value;
		Il2CppCodeGenWriteBarrier((&___MD2_25), value);
	}

	inline static int32_t get_offset_of_MD4_26() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___MD4_26)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_MD4_26() const { return ___MD4_26; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_MD4_26() { return &___MD4_26; }
	inline void set_MD4_26(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___MD4_26 = value;
		Il2CppCodeGenWriteBarrier((&___MD4_26), value);
	}

	inline static int32_t get_offset_of_MD5_27() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___MD5_27)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_MD5_27() const { return ___MD5_27; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_MD5_27() { return &___MD5_27; }
	inline void set_MD5_27(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___MD5_27 = value;
		Il2CppCodeGenWriteBarrier((&___MD5_27), value);
	}

	inline static int32_t get_offset_of_IdHmacWithSha1_28() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___IdHmacWithSha1_28)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdHmacWithSha1_28() const { return ___IdHmacWithSha1_28; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdHmacWithSha1_28() { return &___IdHmacWithSha1_28; }
	inline void set_IdHmacWithSha1_28(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdHmacWithSha1_28 = value;
		Il2CppCodeGenWriteBarrier((&___IdHmacWithSha1_28), value);
	}

	inline static int32_t get_offset_of_IdHmacWithSha224_29() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___IdHmacWithSha224_29)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdHmacWithSha224_29() const { return ___IdHmacWithSha224_29; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdHmacWithSha224_29() { return &___IdHmacWithSha224_29; }
	inline void set_IdHmacWithSha224_29(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdHmacWithSha224_29 = value;
		Il2CppCodeGenWriteBarrier((&___IdHmacWithSha224_29), value);
	}

	inline static int32_t get_offset_of_IdHmacWithSha256_30() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___IdHmacWithSha256_30)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdHmacWithSha256_30() const { return ___IdHmacWithSha256_30; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdHmacWithSha256_30() { return &___IdHmacWithSha256_30; }
	inline void set_IdHmacWithSha256_30(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdHmacWithSha256_30 = value;
		Il2CppCodeGenWriteBarrier((&___IdHmacWithSha256_30), value);
	}

	inline static int32_t get_offset_of_IdHmacWithSha384_31() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___IdHmacWithSha384_31)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdHmacWithSha384_31() const { return ___IdHmacWithSha384_31; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdHmacWithSha384_31() { return &___IdHmacWithSha384_31; }
	inline void set_IdHmacWithSha384_31(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdHmacWithSha384_31 = value;
		Il2CppCodeGenWriteBarrier((&___IdHmacWithSha384_31), value);
	}

	inline static int32_t get_offset_of_IdHmacWithSha512_32() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___IdHmacWithSha512_32)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdHmacWithSha512_32() const { return ___IdHmacWithSha512_32; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdHmacWithSha512_32() { return &___IdHmacWithSha512_32; }
	inline void set_IdHmacWithSha512_32(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdHmacWithSha512_32 = value;
		Il2CppCodeGenWriteBarrier((&___IdHmacWithSha512_32), value);
	}

	inline static int32_t get_offset_of_Data_33() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___Data_33)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_Data_33() const { return ___Data_33; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_Data_33() { return &___Data_33; }
	inline void set_Data_33(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___Data_33 = value;
		Il2CppCodeGenWriteBarrier((&___Data_33), value);
	}

	inline static int32_t get_offset_of_SignedData_34() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___SignedData_34)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_SignedData_34() const { return ___SignedData_34; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_SignedData_34() { return &___SignedData_34; }
	inline void set_SignedData_34(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___SignedData_34 = value;
		Il2CppCodeGenWriteBarrier((&___SignedData_34), value);
	}

	inline static int32_t get_offset_of_EnvelopedData_35() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___EnvelopedData_35)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_EnvelopedData_35() const { return ___EnvelopedData_35; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_EnvelopedData_35() { return &___EnvelopedData_35; }
	inline void set_EnvelopedData_35(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___EnvelopedData_35 = value;
		Il2CppCodeGenWriteBarrier((&___EnvelopedData_35), value);
	}

	inline static int32_t get_offset_of_SignedAndEnvelopedData_36() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___SignedAndEnvelopedData_36)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_SignedAndEnvelopedData_36() const { return ___SignedAndEnvelopedData_36; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_SignedAndEnvelopedData_36() { return &___SignedAndEnvelopedData_36; }
	inline void set_SignedAndEnvelopedData_36(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___SignedAndEnvelopedData_36 = value;
		Il2CppCodeGenWriteBarrier((&___SignedAndEnvelopedData_36), value);
	}

	inline static int32_t get_offset_of_DigestedData_37() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___DigestedData_37)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_DigestedData_37() const { return ___DigestedData_37; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_DigestedData_37() { return &___DigestedData_37; }
	inline void set_DigestedData_37(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___DigestedData_37 = value;
		Il2CppCodeGenWriteBarrier((&___DigestedData_37), value);
	}

	inline static int32_t get_offset_of_EncryptedData_38() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___EncryptedData_38)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_EncryptedData_38() const { return ___EncryptedData_38; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_EncryptedData_38() { return &___EncryptedData_38; }
	inline void set_EncryptedData_38(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___EncryptedData_38 = value;
		Il2CppCodeGenWriteBarrier((&___EncryptedData_38), value);
	}

	inline static int32_t get_offset_of_Pkcs9AtEmailAddress_39() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___Pkcs9AtEmailAddress_39)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_Pkcs9AtEmailAddress_39() const { return ___Pkcs9AtEmailAddress_39; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_Pkcs9AtEmailAddress_39() { return &___Pkcs9AtEmailAddress_39; }
	inline void set_Pkcs9AtEmailAddress_39(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___Pkcs9AtEmailAddress_39 = value;
		Il2CppCodeGenWriteBarrier((&___Pkcs9AtEmailAddress_39), value);
	}

	inline static int32_t get_offset_of_Pkcs9AtUnstructuredName_40() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___Pkcs9AtUnstructuredName_40)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_Pkcs9AtUnstructuredName_40() const { return ___Pkcs9AtUnstructuredName_40; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_Pkcs9AtUnstructuredName_40() { return &___Pkcs9AtUnstructuredName_40; }
	inline void set_Pkcs9AtUnstructuredName_40(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___Pkcs9AtUnstructuredName_40 = value;
		Il2CppCodeGenWriteBarrier((&___Pkcs9AtUnstructuredName_40), value);
	}

	inline static int32_t get_offset_of_Pkcs9AtContentType_41() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___Pkcs9AtContentType_41)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_Pkcs9AtContentType_41() const { return ___Pkcs9AtContentType_41; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_Pkcs9AtContentType_41() { return &___Pkcs9AtContentType_41; }
	inline void set_Pkcs9AtContentType_41(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___Pkcs9AtContentType_41 = value;
		Il2CppCodeGenWriteBarrier((&___Pkcs9AtContentType_41), value);
	}

	inline static int32_t get_offset_of_Pkcs9AtMessageDigest_42() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___Pkcs9AtMessageDigest_42)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_Pkcs9AtMessageDigest_42() const { return ___Pkcs9AtMessageDigest_42; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_Pkcs9AtMessageDigest_42() { return &___Pkcs9AtMessageDigest_42; }
	inline void set_Pkcs9AtMessageDigest_42(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___Pkcs9AtMessageDigest_42 = value;
		Il2CppCodeGenWriteBarrier((&___Pkcs9AtMessageDigest_42), value);
	}

	inline static int32_t get_offset_of_Pkcs9AtSigningTime_43() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___Pkcs9AtSigningTime_43)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_Pkcs9AtSigningTime_43() const { return ___Pkcs9AtSigningTime_43; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_Pkcs9AtSigningTime_43() { return &___Pkcs9AtSigningTime_43; }
	inline void set_Pkcs9AtSigningTime_43(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___Pkcs9AtSigningTime_43 = value;
		Il2CppCodeGenWriteBarrier((&___Pkcs9AtSigningTime_43), value);
	}

	inline static int32_t get_offset_of_Pkcs9AtCounterSignature_44() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___Pkcs9AtCounterSignature_44)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_Pkcs9AtCounterSignature_44() const { return ___Pkcs9AtCounterSignature_44; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_Pkcs9AtCounterSignature_44() { return &___Pkcs9AtCounterSignature_44; }
	inline void set_Pkcs9AtCounterSignature_44(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___Pkcs9AtCounterSignature_44 = value;
		Il2CppCodeGenWriteBarrier((&___Pkcs9AtCounterSignature_44), value);
	}

	inline static int32_t get_offset_of_Pkcs9AtChallengePassword_45() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___Pkcs9AtChallengePassword_45)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_Pkcs9AtChallengePassword_45() const { return ___Pkcs9AtChallengePassword_45; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_Pkcs9AtChallengePassword_45() { return &___Pkcs9AtChallengePassword_45; }
	inline void set_Pkcs9AtChallengePassword_45(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___Pkcs9AtChallengePassword_45 = value;
		Il2CppCodeGenWriteBarrier((&___Pkcs9AtChallengePassword_45), value);
	}

	inline static int32_t get_offset_of_Pkcs9AtUnstructuredAddress_46() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___Pkcs9AtUnstructuredAddress_46)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_Pkcs9AtUnstructuredAddress_46() const { return ___Pkcs9AtUnstructuredAddress_46; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_Pkcs9AtUnstructuredAddress_46() { return &___Pkcs9AtUnstructuredAddress_46; }
	inline void set_Pkcs9AtUnstructuredAddress_46(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___Pkcs9AtUnstructuredAddress_46 = value;
		Il2CppCodeGenWriteBarrier((&___Pkcs9AtUnstructuredAddress_46), value);
	}

	inline static int32_t get_offset_of_Pkcs9AtExtendedCertificateAttributes_47() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___Pkcs9AtExtendedCertificateAttributes_47)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_Pkcs9AtExtendedCertificateAttributes_47() const { return ___Pkcs9AtExtendedCertificateAttributes_47; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_Pkcs9AtExtendedCertificateAttributes_47() { return &___Pkcs9AtExtendedCertificateAttributes_47; }
	inline void set_Pkcs9AtExtendedCertificateAttributes_47(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___Pkcs9AtExtendedCertificateAttributes_47 = value;
		Il2CppCodeGenWriteBarrier((&___Pkcs9AtExtendedCertificateAttributes_47), value);
	}

	inline static int32_t get_offset_of_Pkcs9AtSigningDescription_48() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___Pkcs9AtSigningDescription_48)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_Pkcs9AtSigningDescription_48() const { return ___Pkcs9AtSigningDescription_48; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_Pkcs9AtSigningDescription_48() { return &___Pkcs9AtSigningDescription_48; }
	inline void set_Pkcs9AtSigningDescription_48(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___Pkcs9AtSigningDescription_48 = value;
		Il2CppCodeGenWriteBarrier((&___Pkcs9AtSigningDescription_48), value);
	}

	inline static int32_t get_offset_of_Pkcs9AtExtensionRequest_49() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___Pkcs9AtExtensionRequest_49)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_Pkcs9AtExtensionRequest_49() const { return ___Pkcs9AtExtensionRequest_49; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_Pkcs9AtExtensionRequest_49() { return &___Pkcs9AtExtensionRequest_49; }
	inline void set_Pkcs9AtExtensionRequest_49(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___Pkcs9AtExtensionRequest_49 = value;
		Il2CppCodeGenWriteBarrier((&___Pkcs9AtExtensionRequest_49), value);
	}

	inline static int32_t get_offset_of_Pkcs9AtSmimeCapabilities_50() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___Pkcs9AtSmimeCapabilities_50)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_Pkcs9AtSmimeCapabilities_50() const { return ___Pkcs9AtSmimeCapabilities_50; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_Pkcs9AtSmimeCapabilities_50() { return &___Pkcs9AtSmimeCapabilities_50; }
	inline void set_Pkcs9AtSmimeCapabilities_50(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___Pkcs9AtSmimeCapabilities_50 = value;
		Il2CppCodeGenWriteBarrier((&___Pkcs9AtSmimeCapabilities_50), value);
	}

	inline static int32_t get_offset_of_IdSmime_51() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___IdSmime_51)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdSmime_51() const { return ___IdSmime_51; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdSmime_51() { return &___IdSmime_51; }
	inline void set_IdSmime_51(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdSmime_51 = value;
		Il2CppCodeGenWriteBarrier((&___IdSmime_51), value);
	}

	inline static int32_t get_offset_of_Pkcs9AtFriendlyName_52() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___Pkcs9AtFriendlyName_52)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_Pkcs9AtFriendlyName_52() const { return ___Pkcs9AtFriendlyName_52; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_Pkcs9AtFriendlyName_52() { return &___Pkcs9AtFriendlyName_52; }
	inline void set_Pkcs9AtFriendlyName_52(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___Pkcs9AtFriendlyName_52 = value;
		Il2CppCodeGenWriteBarrier((&___Pkcs9AtFriendlyName_52), value);
	}

	inline static int32_t get_offset_of_Pkcs9AtLocalKeyID_53() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___Pkcs9AtLocalKeyID_53)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_Pkcs9AtLocalKeyID_53() const { return ___Pkcs9AtLocalKeyID_53; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_Pkcs9AtLocalKeyID_53() { return &___Pkcs9AtLocalKeyID_53; }
	inline void set_Pkcs9AtLocalKeyID_53(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___Pkcs9AtLocalKeyID_53 = value;
		Il2CppCodeGenWriteBarrier((&___Pkcs9AtLocalKeyID_53), value);
	}

	inline static int32_t get_offset_of_X509CertType_54() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___X509CertType_54)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_X509CertType_54() const { return ___X509CertType_54; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_X509CertType_54() { return &___X509CertType_54; }
	inline void set_X509CertType_54(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___X509CertType_54 = value;
		Il2CppCodeGenWriteBarrier((&___X509CertType_54), value);
	}

	inline static int32_t get_offset_of_X509Certificate_55() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___X509Certificate_55)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_X509Certificate_55() const { return ___X509Certificate_55; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_X509Certificate_55() { return &___X509Certificate_55; }
	inline void set_X509Certificate_55(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___X509Certificate_55 = value;
		Il2CppCodeGenWriteBarrier((&___X509Certificate_55), value);
	}

	inline static int32_t get_offset_of_SdsiCertificate_56() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___SdsiCertificate_56)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_SdsiCertificate_56() const { return ___SdsiCertificate_56; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_SdsiCertificate_56() { return &___SdsiCertificate_56; }
	inline void set_SdsiCertificate_56(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___SdsiCertificate_56 = value;
		Il2CppCodeGenWriteBarrier((&___SdsiCertificate_56), value);
	}

	inline static int32_t get_offset_of_X509Crl_57() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___X509Crl_57)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_X509Crl_57() const { return ___X509Crl_57; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_X509Crl_57() { return &___X509Crl_57; }
	inline void set_X509Crl_57(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___X509Crl_57 = value;
		Il2CppCodeGenWriteBarrier((&___X509Crl_57), value);
	}

	inline static int32_t get_offset_of_IdAlg_58() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___IdAlg_58)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdAlg_58() const { return ___IdAlg_58; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdAlg_58() { return &___IdAlg_58; }
	inline void set_IdAlg_58(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdAlg_58 = value;
		Il2CppCodeGenWriteBarrier((&___IdAlg_58), value);
	}

	inline static int32_t get_offset_of_IdAlgEsdh_59() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___IdAlgEsdh_59)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdAlgEsdh_59() const { return ___IdAlgEsdh_59; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdAlgEsdh_59() { return &___IdAlgEsdh_59; }
	inline void set_IdAlgEsdh_59(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdAlgEsdh_59 = value;
		Il2CppCodeGenWriteBarrier((&___IdAlgEsdh_59), value);
	}

	inline static int32_t get_offset_of_IdAlgCms3DesWrap_60() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___IdAlgCms3DesWrap_60)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdAlgCms3DesWrap_60() const { return ___IdAlgCms3DesWrap_60; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdAlgCms3DesWrap_60() { return &___IdAlgCms3DesWrap_60; }
	inline void set_IdAlgCms3DesWrap_60(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdAlgCms3DesWrap_60 = value;
		Il2CppCodeGenWriteBarrier((&___IdAlgCms3DesWrap_60), value);
	}

	inline static int32_t get_offset_of_IdAlgCmsRC2Wrap_61() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___IdAlgCmsRC2Wrap_61)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdAlgCmsRC2Wrap_61() const { return ___IdAlgCmsRC2Wrap_61; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdAlgCmsRC2Wrap_61() { return &___IdAlgCmsRC2Wrap_61; }
	inline void set_IdAlgCmsRC2Wrap_61(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdAlgCmsRC2Wrap_61 = value;
		Il2CppCodeGenWriteBarrier((&___IdAlgCmsRC2Wrap_61), value);
	}

	inline static int32_t get_offset_of_IdAlgPwriKek_62() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___IdAlgPwriKek_62)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdAlgPwriKek_62() const { return ___IdAlgPwriKek_62; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdAlgPwriKek_62() { return &___IdAlgPwriKek_62; }
	inline void set_IdAlgPwriKek_62(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdAlgPwriKek_62 = value;
		Il2CppCodeGenWriteBarrier((&___IdAlgPwriKek_62), value);
	}

	inline static int32_t get_offset_of_IdAlgSsdh_63() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___IdAlgSsdh_63)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdAlgSsdh_63() const { return ___IdAlgSsdh_63; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdAlgSsdh_63() { return &___IdAlgSsdh_63; }
	inline void set_IdAlgSsdh_63(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdAlgSsdh_63 = value;
		Il2CppCodeGenWriteBarrier((&___IdAlgSsdh_63), value);
	}

	inline static int32_t get_offset_of_IdRsaKem_64() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___IdRsaKem_64)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdRsaKem_64() const { return ___IdRsaKem_64; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdRsaKem_64() { return &___IdRsaKem_64; }
	inline void set_IdRsaKem_64(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdRsaKem_64 = value;
		Il2CppCodeGenWriteBarrier((&___IdRsaKem_64), value);
	}

	inline static int32_t get_offset_of_PreferSignedData_65() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___PreferSignedData_65)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_PreferSignedData_65() const { return ___PreferSignedData_65; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_PreferSignedData_65() { return &___PreferSignedData_65; }
	inline void set_PreferSignedData_65(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___PreferSignedData_65 = value;
		Il2CppCodeGenWriteBarrier((&___PreferSignedData_65), value);
	}

	inline static int32_t get_offset_of_CannotDecryptAny_66() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___CannotDecryptAny_66)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_CannotDecryptAny_66() const { return ___CannotDecryptAny_66; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_CannotDecryptAny_66() { return &___CannotDecryptAny_66; }
	inline void set_CannotDecryptAny_66(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___CannotDecryptAny_66 = value;
		Il2CppCodeGenWriteBarrier((&___CannotDecryptAny_66), value);
	}

	inline static int32_t get_offset_of_SmimeCapabilitiesVersions_67() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___SmimeCapabilitiesVersions_67)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_SmimeCapabilitiesVersions_67() const { return ___SmimeCapabilitiesVersions_67; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_SmimeCapabilitiesVersions_67() { return &___SmimeCapabilitiesVersions_67; }
	inline void set_SmimeCapabilitiesVersions_67(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___SmimeCapabilitiesVersions_67 = value;
		Il2CppCodeGenWriteBarrier((&___SmimeCapabilitiesVersions_67), value);
	}

	inline static int32_t get_offset_of_IdAAReceiptRequest_68() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___IdAAReceiptRequest_68)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdAAReceiptRequest_68() const { return ___IdAAReceiptRequest_68; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdAAReceiptRequest_68() { return &___IdAAReceiptRequest_68; }
	inline void set_IdAAReceiptRequest_68(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdAAReceiptRequest_68 = value;
		Il2CppCodeGenWriteBarrier((&___IdAAReceiptRequest_68), value);
	}

	inline static int32_t get_offset_of_IdCTAuthData_69() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___IdCTAuthData_69)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdCTAuthData_69() const { return ___IdCTAuthData_69; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdCTAuthData_69() { return &___IdCTAuthData_69; }
	inline void set_IdCTAuthData_69(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdCTAuthData_69 = value;
		Il2CppCodeGenWriteBarrier((&___IdCTAuthData_69), value);
	}

	inline static int32_t get_offset_of_IdCTTstInfo_70() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___IdCTTstInfo_70)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdCTTstInfo_70() const { return ___IdCTTstInfo_70; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdCTTstInfo_70() { return &___IdCTTstInfo_70; }
	inline void set_IdCTTstInfo_70(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdCTTstInfo_70 = value;
		Il2CppCodeGenWriteBarrier((&___IdCTTstInfo_70), value);
	}

	inline static int32_t get_offset_of_IdCTCompressedData_71() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___IdCTCompressedData_71)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdCTCompressedData_71() const { return ___IdCTCompressedData_71; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdCTCompressedData_71() { return &___IdCTCompressedData_71; }
	inline void set_IdCTCompressedData_71(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdCTCompressedData_71 = value;
		Il2CppCodeGenWriteBarrier((&___IdCTCompressedData_71), value);
	}

	inline static int32_t get_offset_of_IdCTAuthEnvelopedData_72() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___IdCTAuthEnvelopedData_72)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdCTAuthEnvelopedData_72() const { return ___IdCTAuthEnvelopedData_72; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdCTAuthEnvelopedData_72() { return &___IdCTAuthEnvelopedData_72; }
	inline void set_IdCTAuthEnvelopedData_72(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdCTAuthEnvelopedData_72 = value;
		Il2CppCodeGenWriteBarrier((&___IdCTAuthEnvelopedData_72), value);
	}

	inline static int32_t get_offset_of_IdCTTimestampedData_73() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___IdCTTimestampedData_73)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdCTTimestampedData_73() const { return ___IdCTTimestampedData_73; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdCTTimestampedData_73() { return &___IdCTTimestampedData_73; }
	inline void set_IdCTTimestampedData_73(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdCTTimestampedData_73 = value;
		Il2CppCodeGenWriteBarrier((&___IdCTTimestampedData_73), value);
	}

	inline static int32_t get_offset_of_IdCtiEtsProofOfOrigin_74() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___IdCtiEtsProofOfOrigin_74)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdCtiEtsProofOfOrigin_74() const { return ___IdCtiEtsProofOfOrigin_74; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdCtiEtsProofOfOrigin_74() { return &___IdCtiEtsProofOfOrigin_74; }
	inline void set_IdCtiEtsProofOfOrigin_74(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdCtiEtsProofOfOrigin_74 = value;
		Il2CppCodeGenWriteBarrier((&___IdCtiEtsProofOfOrigin_74), value);
	}

	inline static int32_t get_offset_of_IdCtiEtsProofOfReceipt_75() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___IdCtiEtsProofOfReceipt_75)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdCtiEtsProofOfReceipt_75() const { return ___IdCtiEtsProofOfReceipt_75; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdCtiEtsProofOfReceipt_75() { return &___IdCtiEtsProofOfReceipt_75; }
	inline void set_IdCtiEtsProofOfReceipt_75(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdCtiEtsProofOfReceipt_75 = value;
		Il2CppCodeGenWriteBarrier((&___IdCtiEtsProofOfReceipt_75), value);
	}

	inline static int32_t get_offset_of_IdCtiEtsProofOfDelivery_76() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___IdCtiEtsProofOfDelivery_76)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdCtiEtsProofOfDelivery_76() const { return ___IdCtiEtsProofOfDelivery_76; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdCtiEtsProofOfDelivery_76() { return &___IdCtiEtsProofOfDelivery_76; }
	inline void set_IdCtiEtsProofOfDelivery_76(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdCtiEtsProofOfDelivery_76 = value;
		Il2CppCodeGenWriteBarrier((&___IdCtiEtsProofOfDelivery_76), value);
	}

	inline static int32_t get_offset_of_IdCtiEtsProofOfSender_77() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___IdCtiEtsProofOfSender_77)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdCtiEtsProofOfSender_77() const { return ___IdCtiEtsProofOfSender_77; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdCtiEtsProofOfSender_77() { return &___IdCtiEtsProofOfSender_77; }
	inline void set_IdCtiEtsProofOfSender_77(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdCtiEtsProofOfSender_77 = value;
		Il2CppCodeGenWriteBarrier((&___IdCtiEtsProofOfSender_77), value);
	}

	inline static int32_t get_offset_of_IdCtiEtsProofOfApproval_78() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___IdCtiEtsProofOfApproval_78)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdCtiEtsProofOfApproval_78() const { return ___IdCtiEtsProofOfApproval_78; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdCtiEtsProofOfApproval_78() { return &___IdCtiEtsProofOfApproval_78; }
	inline void set_IdCtiEtsProofOfApproval_78(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdCtiEtsProofOfApproval_78 = value;
		Il2CppCodeGenWriteBarrier((&___IdCtiEtsProofOfApproval_78), value);
	}

	inline static int32_t get_offset_of_IdCtiEtsProofOfCreation_79() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___IdCtiEtsProofOfCreation_79)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdCtiEtsProofOfCreation_79() const { return ___IdCtiEtsProofOfCreation_79; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdCtiEtsProofOfCreation_79() { return &___IdCtiEtsProofOfCreation_79; }
	inline void set_IdCtiEtsProofOfCreation_79(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdCtiEtsProofOfCreation_79 = value;
		Il2CppCodeGenWriteBarrier((&___IdCtiEtsProofOfCreation_79), value);
	}

	inline static int32_t get_offset_of_IdAAContentHint_80() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___IdAAContentHint_80)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdAAContentHint_80() const { return ___IdAAContentHint_80; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdAAContentHint_80() { return &___IdAAContentHint_80; }
	inline void set_IdAAContentHint_80(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdAAContentHint_80 = value;
		Il2CppCodeGenWriteBarrier((&___IdAAContentHint_80), value);
	}

	inline static int32_t get_offset_of_IdAAMsgSigDigest_81() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___IdAAMsgSigDigest_81)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdAAMsgSigDigest_81() const { return ___IdAAMsgSigDigest_81; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdAAMsgSigDigest_81() { return &___IdAAMsgSigDigest_81; }
	inline void set_IdAAMsgSigDigest_81(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdAAMsgSigDigest_81 = value;
		Il2CppCodeGenWriteBarrier((&___IdAAMsgSigDigest_81), value);
	}

	inline static int32_t get_offset_of_IdAAContentReference_82() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___IdAAContentReference_82)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdAAContentReference_82() const { return ___IdAAContentReference_82; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdAAContentReference_82() { return &___IdAAContentReference_82; }
	inline void set_IdAAContentReference_82(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdAAContentReference_82 = value;
		Il2CppCodeGenWriteBarrier((&___IdAAContentReference_82), value);
	}

	inline static int32_t get_offset_of_IdAAEncrypKeyPref_83() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___IdAAEncrypKeyPref_83)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdAAEncrypKeyPref_83() const { return ___IdAAEncrypKeyPref_83; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdAAEncrypKeyPref_83() { return &___IdAAEncrypKeyPref_83; }
	inline void set_IdAAEncrypKeyPref_83(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdAAEncrypKeyPref_83 = value;
		Il2CppCodeGenWriteBarrier((&___IdAAEncrypKeyPref_83), value);
	}

	inline static int32_t get_offset_of_IdAASigningCertificate_84() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___IdAASigningCertificate_84)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdAASigningCertificate_84() const { return ___IdAASigningCertificate_84; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdAASigningCertificate_84() { return &___IdAASigningCertificate_84; }
	inline void set_IdAASigningCertificate_84(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdAASigningCertificate_84 = value;
		Il2CppCodeGenWriteBarrier((&___IdAASigningCertificate_84), value);
	}

	inline static int32_t get_offset_of_IdAASigningCertificateV2_85() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___IdAASigningCertificateV2_85)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdAASigningCertificateV2_85() const { return ___IdAASigningCertificateV2_85; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdAASigningCertificateV2_85() { return &___IdAASigningCertificateV2_85; }
	inline void set_IdAASigningCertificateV2_85(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdAASigningCertificateV2_85 = value;
		Il2CppCodeGenWriteBarrier((&___IdAASigningCertificateV2_85), value);
	}

	inline static int32_t get_offset_of_IdAAContentIdentifier_86() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___IdAAContentIdentifier_86)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdAAContentIdentifier_86() const { return ___IdAAContentIdentifier_86; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdAAContentIdentifier_86() { return &___IdAAContentIdentifier_86; }
	inline void set_IdAAContentIdentifier_86(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdAAContentIdentifier_86 = value;
		Il2CppCodeGenWriteBarrier((&___IdAAContentIdentifier_86), value);
	}

	inline static int32_t get_offset_of_IdAASignatureTimeStampToken_87() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___IdAASignatureTimeStampToken_87)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdAASignatureTimeStampToken_87() const { return ___IdAASignatureTimeStampToken_87; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdAASignatureTimeStampToken_87() { return &___IdAASignatureTimeStampToken_87; }
	inline void set_IdAASignatureTimeStampToken_87(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdAASignatureTimeStampToken_87 = value;
		Il2CppCodeGenWriteBarrier((&___IdAASignatureTimeStampToken_87), value);
	}

	inline static int32_t get_offset_of_IdAAEtsSigPolicyID_88() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___IdAAEtsSigPolicyID_88)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdAAEtsSigPolicyID_88() const { return ___IdAAEtsSigPolicyID_88; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdAAEtsSigPolicyID_88() { return &___IdAAEtsSigPolicyID_88; }
	inline void set_IdAAEtsSigPolicyID_88(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdAAEtsSigPolicyID_88 = value;
		Il2CppCodeGenWriteBarrier((&___IdAAEtsSigPolicyID_88), value);
	}

	inline static int32_t get_offset_of_IdAAEtsCommitmentType_89() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___IdAAEtsCommitmentType_89)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdAAEtsCommitmentType_89() const { return ___IdAAEtsCommitmentType_89; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdAAEtsCommitmentType_89() { return &___IdAAEtsCommitmentType_89; }
	inline void set_IdAAEtsCommitmentType_89(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdAAEtsCommitmentType_89 = value;
		Il2CppCodeGenWriteBarrier((&___IdAAEtsCommitmentType_89), value);
	}

	inline static int32_t get_offset_of_IdAAEtsSignerLocation_90() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___IdAAEtsSignerLocation_90)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdAAEtsSignerLocation_90() const { return ___IdAAEtsSignerLocation_90; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdAAEtsSignerLocation_90() { return &___IdAAEtsSignerLocation_90; }
	inline void set_IdAAEtsSignerLocation_90(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdAAEtsSignerLocation_90 = value;
		Il2CppCodeGenWriteBarrier((&___IdAAEtsSignerLocation_90), value);
	}

	inline static int32_t get_offset_of_IdAAEtsSignerAttr_91() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___IdAAEtsSignerAttr_91)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdAAEtsSignerAttr_91() const { return ___IdAAEtsSignerAttr_91; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdAAEtsSignerAttr_91() { return &___IdAAEtsSignerAttr_91; }
	inline void set_IdAAEtsSignerAttr_91(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdAAEtsSignerAttr_91 = value;
		Il2CppCodeGenWriteBarrier((&___IdAAEtsSignerAttr_91), value);
	}

	inline static int32_t get_offset_of_IdAAEtsOtherSigCert_92() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___IdAAEtsOtherSigCert_92)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdAAEtsOtherSigCert_92() const { return ___IdAAEtsOtherSigCert_92; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdAAEtsOtherSigCert_92() { return &___IdAAEtsOtherSigCert_92; }
	inline void set_IdAAEtsOtherSigCert_92(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdAAEtsOtherSigCert_92 = value;
		Il2CppCodeGenWriteBarrier((&___IdAAEtsOtherSigCert_92), value);
	}

	inline static int32_t get_offset_of_IdAAEtsContentTimestamp_93() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___IdAAEtsContentTimestamp_93)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdAAEtsContentTimestamp_93() const { return ___IdAAEtsContentTimestamp_93; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdAAEtsContentTimestamp_93() { return &___IdAAEtsContentTimestamp_93; }
	inline void set_IdAAEtsContentTimestamp_93(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdAAEtsContentTimestamp_93 = value;
		Il2CppCodeGenWriteBarrier((&___IdAAEtsContentTimestamp_93), value);
	}

	inline static int32_t get_offset_of_IdAAEtsCertificateRefs_94() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___IdAAEtsCertificateRefs_94)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdAAEtsCertificateRefs_94() const { return ___IdAAEtsCertificateRefs_94; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdAAEtsCertificateRefs_94() { return &___IdAAEtsCertificateRefs_94; }
	inline void set_IdAAEtsCertificateRefs_94(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdAAEtsCertificateRefs_94 = value;
		Il2CppCodeGenWriteBarrier((&___IdAAEtsCertificateRefs_94), value);
	}

	inline static int32_t get_offset_of_IdAAEtsRevocationRefs_95() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___IdAAEtsRevocationRefs_95)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdAAEtsRevocationRefs_95() const { return ___IdAAEtsRevocationRefs_95; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdAAEtsRevocationRefs_95() { return &___IdAAEtsRevocationRefs_95; }
	inline void set_IdAAEtsRevocationRefs_95(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdAAEtsRevocationRefs_95 = value;
		Il2CppCodeGenWriteBarrier((&___IdAAEtsRevocationRefs_95), value);
	}

	inline static int32_t get_offset_of_IdAAEtsCertValues_96() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___IdAAEtsCertValues_96)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdAAEtsCertValues_96() const { return ___IdAAEtsCertValues_96; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdAAEtsCertValues_96() { return &___IdAAEtsCertValues_96; }
	inline void set_IdAAEtsCertValues_96(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdAAEtsCertValues_96 = value;
		Il2CppCodeGenWriteBarrier((&___IdAAEtsCertValues_96), value);
	}

	inline static int32_t get_offset_of_IdAAEtsRevocationValues_97() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___IdAAEtsRevocationValues_97)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdAAEtsRevocationValues_97() const { return ___IdAAEtsRevocationValues_97; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdAAEtsRevocationValues_97() { return &___IdAAEtsRevocationValues_97; }
	inline void set_IdAAEtsRevocationValues_97(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdAAEtsRevocationValues_97 = value;
		Il2CppCodeGenWriteBarrier((&___IdAAEtsRevocationValues_97), value);
	}

	inline static int32_t get_offset_of_IdAAEtsEscTimeStamp_98() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___IdAAEtsEscTimeStamp_98)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdAAEtsEscTimeStamp_98() const { return ___IdAAEtsEscTimeStamp_98; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdAAEtsEscTimeStamp_98() { return &___IdAAEtsEscTimeStamp_98; }
	inline void set_IdAAEtsEscTimeStamp_98(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdAAEtsEscTimeStamp_98 = value;
		Il2CppCodeGenWriteBarrier((&___IdAAEtsEscTimeStamp_98), value);
	}

	inline static int32_t get_offset_of_IdAAEtsCertCrlTimestamp_99() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___IdAAEtsCertCrlTimestamp_99)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdAAEtsCertCrlTimestamp_99() const { return ___IdAAEtsCertCrlTimestamp_99; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdAAEtsCertCrlTimestamp_99() { return &___IdAAEtsCertCrlTimestamp_99; }
	inline void set_IdAAEtsCertCrlTimestamp_99(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdAAEtsCertCrlTimestamp_99 = value;
		Il2CppCodeGenWriteBarrier((&___IdAAEtsCertCrlTimestamp_99), value);
	}

	inline static int32_t get_offset_of_IdAAEtsArchiveTimestamp_100() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___IdAAEtsArchiveTimestamp_100)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdAAEtsArchiveTimestamp_100() const { return ___IdAAEtsArchiveTimestamp_100; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdAAEtsArchiveTimestamp_100() { return &___IdAAEtsArchiveTimestamp_100; }
	inline void set_IdAAEtsArchiveTimestamp_100(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdAAEtsArchiveTimestamp_100 = value;
		Il2CppCodeGenWriteBarrier((&___IdAAEtsArchiveTimestamp_100), value);
	}

	inline static int32_t get_offset_of_IdAASigPolicyID_101() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___IdAASigPolicyID_101)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdAASigPolicyID_101() const { return ___IdAASigPolicyID_101; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdAASigPolicyID_101() { return &___IdAASigPolicyID_101; }
	inline void set_IdAASigPolicyID_101(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdAASigPolicyID_101 = value;
		Il2CppCodeGenWriteBarrier((&___IdAASigPolicyID_101), value);
	}

	inline static int32_t get_offset_of_IdAACommitmentType_102() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___IdAACommitmentType_102)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdAACommitmentType_102() const { return ___IdAACommitmentType_102; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdAACommitmentType_102() { return &___IdAACommitmentType_102; }
	inline void set_IdAACommitmentType_102(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdAACommitmentType_102 = value;
		Il2CppCodeGenWriteBarrier((&___IdAACommitmentType_102), value);
	}

	inline static int32_t get_offset_of_IdAASignerLocation_103() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___IdAASignerLocation_103)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdAASignerLocation_103() const { return ___IdAASignerLocation_103; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdAASignerLocation_103() { return &___IdAASignerLocation_103; }
	inline void set_IdAASignerLocation_103(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdAASignerLocation_103 = value;
		Il2CppCodeGenWriteBarrier((&___IdAASignerLocation_103), value);
	}

	inline static int32_t get_offset_of_IdAAOtherSigCert_104() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___IdAAOtherSigCert_104)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdAAOtherSigCert_104() const { return ___IdAAOtherSigCert_104; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdAAOtherSigCert_104() { return &___IdAAOtherSigCert_104; }
	inline void set_IdAAOtherSigCert_104(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdAAOtherSigCert_104 = value;
		Il2CppCodeGenWriteBarrier((&___IdAAOtherSigCert_104), value);
	}

	inline static int32_t get_offset_of_IdSpqEtsUri_105() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___IdSpqEtsUri_105)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdSpqEtsUri_105() const { return ___IdSpqEtsUri_105; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdSpqEtsUri_105() { return &___IdSpqEtsUri_105; }
	inline void set_IdSpqEtsUri_105(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdSpqEtsUri_105 = value;
		Il2CppCodeGenWriteBarrier((&___IdSpqEtsUri_105), value);
	}

	inline static int32_t get_offset_of_IdSpqEtsUNotice_106() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___IdSpqEtsUNotice_106)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdSpqEtsUNotice_106() const { return ___IdSpqEtsUNotice_106; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdSpqEtsUNotice_106() { return &___IdSpqEtsUNotice_106; }
	inline void set_IdSpqEtsUNotice_106(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdSpqEtsUNotice_106 = value;
		Il2CppCodeGenWriteBarrier((&___IdSpqEtsUNotice_106), value);
	}

	inline static int32_t get_offset_of_KeyBag_107() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___KeyBag_107)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_KeyBag_107() const { return ___KeyBag_107; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_KeyBag_107() { return &___KeyBag_107; }
	inline void set_KeyBag_107(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___KeyBag_107 = value;
		Il2CppCodeGenWriteBarrier((&___KeyBag_107), value);
	}

	inline static int32_t get_offset_of_Pkcs8ShroudedKeyBag_108() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___Pkcs8ShroudedKeyBag_108)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_Pkcs8ShroudedKeyBag_108() const { return ___Pkcs8ShroudedKeyBag_108; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_Pkcs8ShroudedKeyBag_108() { return &___Pkcs8ShroudedKeyBag_108; }
	inline void set_Pkcs8ShroudedKeyBag_108(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___Pkcs8ShroudedKeyBag_108 = value;
		Il2CppCodeGenWriteBarrier((&___Pkcs8ShroudedKeyBag_108), value);
	}

	inline static int32_t get_offset_of_CertBag_109() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___CertBag_109)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_CertBag_109() const { return ___CertBag_109; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_CertBag_109() { return &___CertBag_109; }
	inline void set_CertBag_109(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___CertBag_109 = value;
		Il2CppCodeGenWriteBarrier((&___CertBag_109), value);
	}

	inline static int32_t get_offset_of_CrlBag_110() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___CrlBag_110)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_CrlBag_110() const { return ___CrlBag_110; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_CrlBag_110() { return &___CrlBag_110; }
	inline void set_CrlBag_110(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___CrlBag_110 = value;
		Il2CppCodeGenWriteBarrier((&___CrlBag_110), value);
	}

	inline static int32_t get_offset_of_SecretBag_111() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___SecretBag_111)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_SecretBag_111() const { return ___SecretBag_111; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_SecretBag_111() { return &___SecretBag_111; }
	inline void set_SecretBag_111(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___SecretBag_111 = value;
		Il2CppCodeGenWriteBarrier((&___SecretBag_111), value);
	}

	inline static int32_t get_offset_of_SafeContentsBag_112() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___SafeContentsBag_112)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_SafeContentsBag_112() const { return ___SafeContentsBag_112; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_SafeContentsBag_112() { return &___SafeContentsBag_112; }
	inline void set_SafeContentsBag_112(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___SafeContentsBag_112 = value;
		Il2CppCodeGenWriteBarrier((&___SafeContentsBag_112), value);
	}

	inline static int32_t get_offset_of_PbeWithShaAnd128BitRC4_113() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___PbeWithShaAnd128BitRC4_113)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_PbeWithShaAnd128BitRC4_113() const { return ___PbeWithShaAnd128BitRC4_113; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_PbeWithShaAnd128BitRC4_113() { return &___PbeWithShaAnd128BitRC4_113; }
	inline void set_PbeWithShaAnd128BitRC4_113(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___PbeWithShaAnd128BitRC4_113 = value;
		Il2CppCodeGenWriteBarrier((&___PbeWithShaAnd128BitRC4_113), value);
	}

	inline static int32_t get_offset_of_PbeWithShaAnd40BitRC4_114() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___PbeWithShaAnd40BitRC4_114)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_PbeWithShaAnd40BitRC4_114() const { return ___PbeWithShaAnd40BitRC4_114; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_PbeWithShaAnd40BitRC4_114() { return &___PbeWithShaAnd40BitRC4_114; }
	inline void set_PbeWithShaAnd40BitRC4_114(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___PbeWithShaAnd40BitRC4_114 = value;
		Il2CppCodeGenWriteBarrier((&___PbeWithShaAnd40BitRC4_114), value);
	}

	inline static int32_t get_offset_of_PbeWithShaAnd3KeyTripleDesCbc_115() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___PbeWithShaAnd3KeyTripleDesCbc_115)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_PbeWithShaAnd3KeyTripleDesCbc_115() const { return ___PbeWithShaAnd3KeyTripleDesCbc_115; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_PbeWithShaAnd3KeyTripleDesCbc_115() { return &___PbeWithShaAnd3KeyTripleDesCbc_115; }
	inline void set_PbeWithShaAnd3KeyTripleDesCbc_115(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___PbeWithShaAnd3KeyTripleDesCbc_115 = value;
		Il2CppCodeGenWriteBarrier((&___PbeWithShaAnd3KeyTripleDesCbc_115), value);
	}

	inline static int32_t get_offset_of_PbeWithShaAnd2KeyTripleDesCbc_116() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___PbeWithShaAnd2KeyTripleDesCbc_116)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_PbeWithShaAnd2KeyTripleDesCbc_116() const { return ___PbeWithShaAnd2KeyTripleDesCbc_116; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_PbeWithShaAnd2KeyTripleDesCbc_116() { return &___PbeWithShaAnd2KeyTripleDesCbc_116; }
	inline void set_PbeWithShaAnd2KeyTripleDesCbc_116(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___PbeWithShaAnd2KeyTripleDesCbc_116 = value;
		Il2CppCodeGenWriteBarrier((&___PbeWithShaAnd2KeyTripleDesCbc_116), value);
	}

	inline static int32_t get_offset_of_PbeWithShaAnd128BitRC2Cbc_117() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___PbeWithShaAnd128BitRC2Cbc_117)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_PbeWithShaAnd128BitRC2Cbc_117() const { return ___PbeWithShaAnd128BitRC2Cbc_117; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_PbeWithShaAnd128BitRC2Cbc_117() { return &___PbeWithShaAnd128BitRC2Cbc_117; }
	inline void set_PbeWithShaAnd128BitRC2Cbc_117(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___PbeWithShaAnd128BitRC2Cbc_117 = value;
		Il2CppCodeGenWriteBarrier((&___PbeWithShaAnd128BitRC2Cbc_117), value);
	}

	inline static int32_t get_offset_of_PbewithShaAnd40BitRC2Cbc_118() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields, ___PbewithShaAnd40BitRC2Cbc_118)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_PbewithShaAnd40BitRC2Cbc_118() const { return ___PbewithShaAnd40BitRC2Cbc_118; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_PbewithShaAnd40BitRC2Cbc_118() { return &___PbewithShaAnd40BitRC2Cbc_118; }
	inline void set_PbewithShaAnd40BitRC2Cbc_118(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___PbewithShaAnd40BitRC2Cbc_118 = value;
		Il2CppCodeGenWriteBarrier((&___PbewithShaAnd40BitRC2Cbc_118), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKCSOBJECTIDENTIFIERS_T56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_H
#ifndef SECNAMEDCURVES_T2F8AEB8DA36D78A58FB27FB732FF1962C5DAF25D_H
#define SECNAMEDCURVES_T2F8AEB8DA36D78A58FB27FB732FF1962C5DAF25D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.Sec.SecNamedCurves
struct  SecNamedCurves_t2F8AEB8DA36D78A58FB27FB732FF1962C5DAF25D  : public RuntimeObject
{
public:

public:
};

struct SecNamedCurves_t2F8AEB8DA36D78A58FB27FB732FF1962C5DAF25D_StaticFields
{
public:
	// System.Collections.IDictionary Org.BouncyCastle.Asn1.Sec.SecNamedCurves::objIds
	RuntimeObject* ___objIds_0;
	// System.Collections.IDictionary Org.BouncyCastle.Asn1.Sec.SecNamedCurves::curves
	RuntimeObject* ___curves_1;
	// System.Collections.IDictionary Org.BouncyCastle.Asn1.Sec.SecNamedCurves::names
	RuntimeObject* ___names_2;

public:
	inline static int32_t get_offset_of_objIds_0() { return static_cast<int32_t>(offsetof(SecNamedCurves_t2F8AEB8DA36D78A58FB27FB732FF1962C5DAF25D_StaticFields, ___objIds_0)); }
	inline RuntimeObject* get_objIds_0() const { return ___objIds_0; }
	inline RuntimeObject** get_address_of_objIds_0() { return &___objIds_0; }
	inline void set_objIds_0(RuntimeObject* value)
	{
		___objIds_0 = value;
		Il2CppCodeGenWriteBarrier((&___objIds_0), value);
	}

	inline static int32_t get_offset_of_curves_1() { return static_cast<int32_t>(offsetof(SecNamedCurves_t2F8AEB8DA36D78A58FB27FB732FF1962C5DAF25D_StaticFields, ___curves_1)); }
	inline RuntimeObject* get_curves_1() const { return ___curves_1; }
	inline RuntimeObject** get_address_of_curves_1() { return &___curves_1; }
	inline void set_curves_1(RuntimeObject* value)
	{
		___curves_1 = value;
		Il2CppCodeGenWriteBarrier((&___curves_1), value);
	}

	inline static int32_t get_offset_of_names_2() { return static_cast<int32_t>(offsetof(SecNamedCurves_t2F8AEB8DA36D78A58FB27FB732FF1962C5DAF25D_StaticFields, ___names_2)); }
	inline RuntimeObject* get_names_2() const { return ___names_2; }
	inline RuntimeObject** get_address_of_names_2() { return &___names_2; }
	inline void set_names_2(RuntimeObject* value)
	{
		___names_2 = value;
		Il2CppCodeGenWriteBarrier((&___names_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECNAMEDCURVES_T2F8AEB8DA36D78A58FB27FB732FF1962C5DAF25D_H
#ifndef SECOBJECTIDENTIFIERS_T5FDF1759371F1AABBBC6F18586221092D3273EA4_H
#define SECOBJECTIDENTIFIERS_T5FDF1759371F1AABBBC6F18586221092D3273EA4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.Sec.SecObjectIdentifiers
struct  SecObjectIdentifiers_t5FDF1759371F1AABBBC6F18586221092D3273EA4  : public RuntimeObject
{
public:

public:
};

struct SecObjectIdentifiers_t5FDF1759371F1AABBBC6F18586221092D3273EA4_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Sec.SecObjectIdentifiers::EllipticCurve
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___EllipticCurve_0;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Sec.SecObjectIdentifiers::SecT163k1
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___SecT163k1_1;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Sec.SecObjectIdentifiers::SecT163r1
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___SecT163r1_2;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Sec.SecObjectIdentifiers::SecT239k1
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___SecT239k1_3;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Sec.SecObjectIdentifiers::SecT113r1
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___SecT113r1_4;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Sec.SecObjectIdentifiers::SecT113r2
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___SecT113r2_5;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Sec.SecObjectIdentifiers::SecP112r1
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___SecP112r1_6;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Sec.SecObjectIdentifiers::SecP112r2
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___SecP112r2_7;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Sec.SecObjectIdentifiers::SecP160r1
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___SecP160r1_8;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Sec.SecObjectIdentifiers::SecP160k1
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___SecP160k1_9;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Sec.SecObjectIdentifiers::SecP256k1
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___SecP256k1_10;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Sec.SecObjectIdentifiers::SecT163r2
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___SecT163r2_11;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Sec.SecObjectIdentifiers::SecT283k1
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___SecT283k1_12;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Sec.SecObjectIdentifiers::SecT283r1
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___SecT283r1_13;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Sec.SecObjectIdentifiers::SecT131r1
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___SecT131r1_14;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Sec.SecObjectIdentifiers::SecT131r2
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___SecT131r2_15;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Sec.SecObjectIdentifiers::SecT193r1
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___SecT193r1_16;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Sec.SecObjectIdentifiers::SecT193r2
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___SecT193r2_17;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Sec.SecObjectIdentifiers::SecT233k1
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___SecT233k1_18;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Sec.SecObjectIdentifiers::SecT233r1
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___SecT233r1_19;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Sec.SecObjectIdentifiers::SecP128r1
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___SecP128r1_20;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Sec.SecObjectIdentifiers::SecP128r2
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___SecP128r2_21;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Sec.SecObjectIdentifiers::SecP160r2
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___SecP160r2_22;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Sec.SecObjectIdentifiers::SecP192k1
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___SecP192k1_23;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Sec.SecObjectIdentifiers::SecP224k1
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___SecP224k1_24;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Sec.SecObjectIdentifiers::SecP224r1
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___SecP224r1_25;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Sec.SecObjectIdentifiers::SecP384r1
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___SecP384r1_26;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Sec.SecObjectIdentifiers::SecP521r1
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___SecP521r1_27;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Sec.SecObjectIdentifiers::SecT409k1
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___SecT409k1_28;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Sec.SecObjectIdentifiers::SecT409r1
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___SecT409r1_29;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Sec.SecObjectIdentifiers::SecT571k1
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___SecT571k1_30;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Sec.SecObjectIdentifiers::SecT571r1
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___SecT571r1_31;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Sec.SecObjectIdentifiers::SecP192r1
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___SecP192r1_32;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Sec.SecObjectIdentifiers::SecP256r1
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___SecP256r1_33;

public:
	inline static int32_t get_offset_of_EllipticCurve_0() { return static_cast<int32_t>(offsetof(SecObjectIdentifiers_t5FDF1759371F1AABBBC6F18586221092D3273EA4_StaticFields, ___EllipticCurve_0)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_EllipticCurve_0() const { return ___EllipticCurve_0; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_EllipticCurve_0() { return &___EllipticCurve_0; }
	inline void set_EllipticCurve_0(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___EllipticCurve_0 = value;
		Il2CppCodeGenWriteBarrier((&___EllipticCurve_0), value);
	}

	inline static int32_t get_offset_of_SecT163k1_1() { return static_cast<int32_t>(offsetof(SecObjectIdentifiers_t5FDF1759371F1AABBBC6F18586221092D3273EA4_StaticFields, ___SecT163k1_1)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_SecT163k1_1() const { return ___SecT163k1_1; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_SecT163k1_1() { return &___SecT163k1_1; }
	inline void set_SecT163k1_1(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___SecT163k1_1 = value;
		Il2CppCodeGenWriteBarrier((&___SecT163k1_1), value);
	}

	inline static int32_t get_offset_of_SecT163r1_2() { return static_cast<int32_t>(offsetof(SecObjectIdentifiers_t5FDF1759371F1AABBBC6F18586221092D3273EA4_StaticFields, ___SecT163r1_2)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_SecT163r1_2() const { return ___SecT163r1_2; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_SecT163r1_2() { return &___SecT163r1_2; }
	inline void set_SecT163r1_2(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___SecT163r1_2 = value;
		Il2CppCodeGenWriteBarrier((&___SecT163r1_2), value);
	}

	inline static int32_t get_offset_of_SecT239k1_3() { return static_cast<int32_t>(offsetof(SecObjectIdentifiers_t5FDF1759371F1AABBBC6F18586221092D3273EA4_StaticFields, ___SecT239k1_3)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_SecT239k1_3() const { return ___SecT239k1_3; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_SecT239k1_3() { return &___SecT239k1_3; }
	inline void set_SecT239k1_3(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___SecT239k1_3 = value;
		Il2CppCodeGenWriteBarrier((&___SecT239k1_3), value);
	}

	inline static int32_t get_offset_of_SecT113r1_4() { return static_cast<int32_t>(offsetof(SecObjectIdentifiers_t5FDF1759371F1AABBBC6F18586221092D3273EA4_StaticFields, ___SecT113r1_4)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_SecT113r1_4() const { return ___SecT113r1_4; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_SecT113r1_4() { return &___SecT113r1_4; }
	inline void set_SecT113r1_4(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___SecT113r1_4 = value;
		Il2CppCodeGenWriteBarrier((&___SecT113r1_4), value);
	}

	inline static int32_t get_offset_of_SecT113r2_5() { return static_cast<int32_t>(offsetof(SecObjectIdentifiers_t5FDF1759371F1AABBBC6F18586221092D3273EA4_StaticFields, ___SecT113r2_5)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_SecT113r2_5() const { return ___SecT113r2_5; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_SecT113r2_5() { return &___SecT113r2_5; }
	inline void set_SecT113r2_5(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___SecT113r2_5 = value;
		Il2CppCodeGenWriteBarrier((&___SecT113r2_5), value);
	}

	inline static int32_t get_offset_of_SecP112r1_6() { return static_cast<int32_t>(offsetof(SecObjectIdentifiers_t5FDF1759371F1AABBBC6F18586221092D3273EA4_StaticFields, ___SecP112r1_6)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_SecP112r1_6() const { return ___SecP112r1_6; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_SecP112r1_6() { return &___SecP112r1_6; }
	inline void set_SecP112r1_6(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___SecP112r1_6 = value;
		Il2CppCodeGenWriteBarrier((&___SecP112r1_6), value);
	}

	inline static int32_t get_offset_of_SecP112r2_7() { return static_cast<int32_t>(offsetof(SecObjectIdentifiers_t5FDF1759371F1AABBBC6F18586221092D3273EA4_StaticFields, ___SecP112r2_7)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_SecP112r2_7() const { return ___SecP112r2_7; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_SecP112r2_7() { return &___SecP112r2_7; }
	inline void set_SecP112r2_7(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___SecP112r2_7 = value;
		Il2CppCodeGenWriteBarrier((&___SecP112r2_7), value);
	}

	inline static int32_t get_offset_of_SecP160r1_8() { return static_cast<int32_t>(offsetof(SecObjectIdentifiers_t5FDF1759371F1AABBBC6F18586221092D3273EA4_StaticFields, ___SecP160r1_8)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_SecP160r1_8() const { return ___SecP160r1_8; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_SecP160r1_8() { return &___SecP160r1_8; }
	inline void set_SecP160r1_8(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___SecP160r1_8 = value;
		Il2CppCodeGenWriteBarrier((&___SecP160r1_8), value);
	}

	inline static int32_t get_offset_of_SecP160k1_9() { return static_cast<int32_t>(offsetof(SecObjectIdentifiers_t5FDF1759371F1AABBBC6F18586221092D3273EA4_StaticFields, ___SecP160k1_9)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_SecP160k1_9() const { return ___SecP160k1_9; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_SecP160k1_9() { return &___SecP160k1_9; }
	inline void set_SecP160k1_9(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___SecP160k1_9 = value;
		Il2CppCodeGenWriteBarrier((&___SecP160k1_9), value);
	}

	inline static int32_t get_offset_of_SecP256k1_10() { return static_cast<int32_t>(offsetof(SecObjectIdentifiers_t5FDF1759371F1AABBBC6F18586221092D3273EA4_StaticFields, ___SecP256k1_10)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_SecP256k1_10() const { return ___SecP256k1_10; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_SecP256k1_10() { return &___SecP256k1_10; }
	inline void set_SecP256k1_10(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___SecP256k1_10 = value;
		Il2CppCodeGenWriteBarrier((&___SecP256k1_10), value);
	}

	inline static int32_t get_offset_of_SecT163r2_11() { return static_cast<int32_t>(offsetof(SecObjectIdentifiers_t5FDF1759371F1AABBBC6F18586221092D3273EA4_StaticFields, ___SecT163r2_11)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_SecT163r2_11() const { return ___SecT163r2_11; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_SecT163r2_11() { return &___SecT163r2_11; }
	inline void set_SecT163r2_11(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___SecT163r2_11 = value;
		Il2CppCodeGenWriteBarrier((&___SecT163r2_11), value);
	}

	inline static int32_t get_offset_of_SecT283k1_12() { return static_cast<int32_t>(offsetof(SecObjectIdentifiers_t5FDF1759371F1AABBBC6F18586221092D3273EA4_StaticFields, ___SecT283k1_12)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_SecT283k1_12() const { return ___SecT283k1_12; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_SecT283k1_12() { return &___SecT283k1_12; }
	inline void set_SecT283k1_12(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___SecT283k1_12 = value;
		Il2CppCodeGenWriteBarrier((&___SecT283k1_12), value);
	}

	inline static int32_t get_offset_of_SecT283r1_13() { return static_cast<int32_t>(offsetof(SecObjectIdentifiers_t5FDF1759371F1AABBBC6F18586221092D3273EA4_StaticFields, ___SecT283r1_13)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_SecT283r1_13() const { return ___SecT283r1_13; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_SecT283r1_13() { return &___SecT283r1_13; }
	inline void set_SecT283r1_13(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___SecT283r1_13 = value;
		Il2CppCodeGenWriteBarrier((&___SecT283r1_13), value);
	}

	inline static int32_t get_offset_of_SecT131r1_14() { return static_cast<int32_t>(offsetof(SecObjectIdentifiers_t5FDF1759371F1AABBBC6F18586221092D3273EA4_StaticFields, ___SecT131r1_14)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_SecT131r1_14() const { return ___SecT131r1_14; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_SecT131r1_14() { return &___SecT131r1_14; }
	inline void set_SecT131r1_14(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___SecT131r1_14 = value;
		Il2CppCodeGenWriteBarrier((&___SecT131r1_14), value);
	}

	inline static int32_t get_offset_of_SecT131r2_15() { return static_cast<int32_t>(offsetof(SecObjectIdentifiers_t5FDF1759371F1AABBBC6F18586221092D3273EA4_StaticFields, ___SecT131r2_15)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_SecT131r2_15() const { return ___SecT131r2_15; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_SecT131r2_15() { return &___SecT131r2_15; }
	inline void set_SecT131r2_15(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___SecT131r2_15 = value;
		Il2CppCodeGenWriteBarrier((&___SecT131r2_15), value);
	}

	inline static int32_t get_offset_of_SecT193r1_16() { return static_cast<int32_t>(offsetof(SecObjectIdentifiers_t5FDF1759371F1AABBBC6F18586221092D3273EA4_StaticFields, ___SecT193r1_16)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_SecT193r1_16() const { return ___SecT193r1_16; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_SecT193r1_16() { return &___SecT193r1_16; }
	inline void set_SecT193r1_16(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___SecT193r1_16 = value;
		Il2CppCodeGenWriteBarrier((&___SecT193r1_16), value);
	}

	inline static int32_t get_offset_of_SecT193r2_17() { return static_cast<int32_t>(offsetof(SecObjectIdentifiers_t5FDF1759371F1AABBBC6F18586221092D3273EA4_StaticFields, ___SecT193r2_17)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_SecT193r2_17() const { return ___SecT193r2_17; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_SecT193r2_17() { return &___SecT193r2_17; }
	inline void set_SecT193r2_17(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___SecT193r2_17 = value;
		Il2CppCodeGenWriteBarrier((&___SecT193r2_17), value);
	}

	inline static int32_t get_offset_of_SecT233k1_18() { return static_cast<int32_t>(offsetof(SecObjectIdentifiers_t5FDF1759371F1AABBBC6F18586221092D3273EA4_StaticFields, ___SecT233k1_18)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_SecT233k1_18() const { return ___SecT233k1_18; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_SecT233k1_18() { return &___SecT233k1_18; }
	inline void set_SecT233k1_18(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___SecT233k1_18 = value;
		Il2CppCodeGenWriteBarrier((&___SecT233k1_18), value);
	}

	inline static int32_t get_offset_of_SecT233r1_19() { return static_cast<int32_t>(offsetof(SecObjectIdentifiers_t5FDF1759371F1AABBBC6F18586221092D3273EA4_StaticFields, ___SecT233r1_19)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_SecT233r1_19() const { return ___SecT233r1_19; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_SecT233r1_19() { return &___SecT233r1_19; }
	inline void set_SecT233r1_19(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___SecT233r1_19 = value;
		Il2CppCodeGenWriteBarrier((&___SecT233r1_19), value);
	}

	inline static int32_t get_offset_of_SecP128r1_20() { return static_cast<int32_t>(offsetof(SecObjectIdentifiers_t5FDF1759371F1AABBBC6F18586221092D3273EA4_StaticFields, ___SecP128r1_20)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_SecP128r1_20() const { return ___SecP128r1_20; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_SecP128r1_20() { return &___SecP128r1_20; }
	inline void set_SecP128r1_20(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___SecP128r1_20 = value;
		Il2CppCodeGenWriteBarrier((&___SecP128r1_20), value);
	}

	inline static int32_t get_offset_of_SecP128r2_21() { return static_cast<int32_t>(offsetof(SecObjectIdentifiers_t5FDF1759371F1AABBBC6F18586221092D3273EA4_StaticFields, ___SecP128r2_21)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_SecP128r2_21() const { return ___SecP128r2_21; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_SecP128r2_21() { return &___SecP128r2_21; }
	inline void set_SecP128r2_21(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___SecP128r2_21 = value;
		Il2CppCodeGenWriteBarrier((&___SecP128r2_21), value);
	}

	inline static int32_t get_offset_of_SecP160r2_22() { return static_cast<int32_t>(offsetof(SecObjectIdentifiers_t5FDF1759371F1AABBBC6F18586221092D3273EA4_StaticFields, ___SecP160r2_22)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_SecP160r2_22() const { return ___SecP160r2_22; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_SecP160r2_22() { return &___SecP160r2_22; }
	inline void set_SecP160r2_22(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___SecP160r2_22 = value;
		Il2CppCodeGenWriteBarrier((&___SecP160r2_22), value);
	}

	inline static int32_t get_offset_of_SecP192k1_23() { return static_cast<int32_t>(offsetof(SecObjectIdentifiers_t5FDF1759371F1AABBBC6F18586221092D3273EA4_StaticFields, ___SecP192k1_23)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_SecP192k1_23() const { return ___SecP192k1_23; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_SecP192k1_23() { return &___SecP192k1_23; }
	inline void set_SecP192k1_23(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___SecP192k1_23 = value;
		Il2CppCodeGenWriteBarrier((&___SecP192k1_23), value);
	}

	inline static int32_t get_offset_of_SecP224k1_24() { return static_cast<int32_t>(offsetof(SecObjectIdentifiers_t5FDF1759371F1AABBBC6F18586221092D3273EA4_StaticFields, ___SecP224k1_24)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_SecP224k1_24() const { return ___SecP224k1_24; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_SecP224k1_24() { return &___SecP224k1_24; }
	inline void set_SecP224k1_24(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___SecP224k1_24 = value;
		Il2CppCodeGenWriteBarrier((&___SecP224k1_24), value);
	}

	inline static int32_t get_offset_of_SecP224r1_25() { return static_cast<int32_t>(offsetof(SecObjectIdentifiers_t5FDF1759371F1AABBBC6F18586221092D3273EA4_StaticFields, ___SecP224r1_25)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_SecP224r1_25() const { return ___SecP224r1_25; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_SecP224r1_25() { return &___SecP224r1_25; }
	inline void set_SecP224r1_25(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___SecP224r1_25 = value;
		Il2CppCodeGenWriteBarrier((&___SecP224r1_25), value);
	}

	inline static int32_t get_offset_of_SecP384r1_26() { return static_cast<int32_t>(offsetof(SecObjectIdentifiers_t5FDF1759371F1AABBBC6F18586221092D3273EA4_StaticFields, ___SecP384r1_26)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_SecP384r1_26() const { return ___SecP384r1_26; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_SecP384r1_26() { return &___SecP384r1_26; }
	inline void set_SecP384r1_26(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___SecP384r1_26 = value;
		Il2CppCodeGenWriteBarrier((&___SecP384r1_26), value);
	}

	inline static int32_t get_offset_of_SecP521r1_27() { return static_cast<int32_t>(offsetof(SecObjectIdentifiers_t5FDF1759371F1AABBBC6F18586221092D3273EA4_StaticFields, ___SecP521r1_27)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_SecP521r1_27() const { return ___SecP521r1_27; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_SecP521r1_27() { return &___SecP521r1_27; }
	inline void set_SecP521r1_27(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___SecP521r1_27 = value;
		Il2CppCodeGenWriteBarrier((&___SecP521r1_27), value);
	}

	inline static int32_t get_offset_of_SecT409k1_28() { return static_cast<int32_t>(offsetof(SecObjectIdentifiers_t5FDF1759371F1AABBBC6F18586221092D3273EA4_StaticFields, ___SecT409k1_28)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_SecT409k1_28() const { return ___SecT409k1_28; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_SecT409k1_28() { return &___SecT409k1_28; }
	inline void set_SecT409k1_28(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___SecT409k1_28 = value;
		Il2CppCodeGenWriteBarrier((&___SecT409k1_28), value);
	}

	inline static int32_t get_offset_of_SecT409r1_29() { return static_cast<int32_t>(offsetof(SecObjectIdentifiers_t5FDF1759371F1AABBBC6F18586221092D3273EA4_StaticFields, ___SecT409r1_29)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_SecT409r1_29() const { return ___SecT409r1_29; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_SecT409r1_29() { return &___SecT409r1_29; }
	inline void set_SecT409r1_29(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___SecT409r1_29 = value;
		Il2CppCodeGenWriteBarrier((&___SecT409r1_29), value);
	}

	inline static int32_t get_offset_of_SecT571k1_30() { return static_cast<int32_t>(offsetof(SecObjectIdentifiers_t5FDF1759371F1AABBBC6F18586221092D3273EA4_StaticFields, ___SecT571k1_30)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_SecT571k1_30() const { return ___SecT571k1_30; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_SecT571k1_30() { return &___SecT571k1_30; }
	inline void set_SecT571k1_30(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___SecT571k1_30 = value;
		Il2CppCodeGenWriteBarrier((&___SecT571k1_30), value);
	}

	inline static int32_t get_offset_of_SecT571r1_31() { return static_cast<int32_t>(offsetof(SecObjectIdentifiers_t5FDF1759371F1AABBBC6F18586221092D3273EA4_StaticFields, ___SecT571r1_31)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_SecT571r1_31() const { return ___SecT571r1_31; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_SecT571r1_31() { return &___SecT571r1_31; }
	inline void set_SecT571r1_31(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___SecT571r1_31 = value;
		Il2CppCodeGenWriteBarrier((&___SecT571r1_31), value);
	}

	inline static int32_t get_offset_of_SecP192r1_32() { return static_cast<int32_t>(offsetof(SecObjectIdentifiers_t5FDF1759371F1AABBBC6F18586221092D3273EA4_StaticFields, ___SecP192r1_32)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_SecP192r1_32() const { return ___SecP192r1_32; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_SecP192r1_32() { return &___SecP192r1_32; }
	inline void set_SecP192r1_32(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___SecP192r1_32 = value;
		Il2CppCodeGenWriteBarrier((&___SecP192r1_32), value);
	}

	inline static int32_t get_offset_of_SecP256r1_33() { return static_cast<int32_t>(offsetof(SecObjectIdentifiers_t5FDF1759371F1AABBBC6F18586221092D3273EA4_StaticFields, ___SecP256r1_33)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_SecP256r1_33() const { return ___SecP256r1_33; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_SecP256r1_33() { return &___SecP256r1_33; }
	inline void set_SecP256r1_33(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___SecP256r1_33 = value;
		Il2CppCodeGenWriteBarrier((&___SecP256r1_33), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECOBJECTIDENTIFIERS_T5FDF1759371F1AABBBC6F18586221092D3273EA4_H
#ifndef TELETRUSTNAMEDCURVES_TD7727093AD23C392E666ED02F75BFC723CECDAA1_H
#define TELETRUSTNAMEDCURVES_TD7727093AD23C392E666ED02F75BFC723CECDAA1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTNamedCurves
struct  TeleTrusTNamedCurves_tD7727093AD23C392E666ED02F75BFC723CECDAA1  : public RuntimeObject
{
public:

public:
};

struct TeleTrusTNamedCurves_tD7727093AD23C392E666ED02F75BFC723CECDAA1_StaticFields
{
public:
	// System.Collections.IDictionary Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTNamedCurves::objIds
	RuntimeObject* ___objIds_0;
	// System.Collections.IDictionary Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTNamedCurves::curves
	RuntimeObject* ___curves_1;
	// System.Collections.IDictionary Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTNamedCurves::names
	RuntimeObject* ___names_2;

public:
	inline static int32_t get_offset_of_objIds_0() { return static_cast<int32_t>(offsetof(TeleTrusTNamedCurves_tD7727093AD23C392E666ED02F75BFC723CECDAA1_StaticFields, ___objIds_0)); }
	inline RuntimeObject* get_objIds_0() const { return ___objIds_0; }
	inline RuntimeObject** get_address_of_objIds_0() { return &___objIds_0; }
	inline void set_objIds_0(RuntimeObject* value)
	{
		___objIds_0 = value;
		Il2CppCodeGenWriteBarrier((&___objIds_0), value);
	}

	inline static int32_t get_offset_of_curves_1() { return static_cast<int32_t>(offsetof(TeleTrusTNamedCurves_tD7727093AD23C392E666ED02F75BFC723CECDAA1_StaticFields, ___curves_1)); }
	inline RuntimeObject* get_curves_1() const { return ___curves_1; }
	inline RuntimeObject** get_address_of_curves_1() { return &___curves_1; }
	inline void set_curves_1(RuntimeObject* value)
	{
		___curves_1 = value;
		Il2CppCodeGenWriteBarrier((&___curves_1), value);
	}

	inline static int32_t get_offset_of_names_2() { return static_cast<int32_t>(offsetof(TeleTrusTNamedCurves_tD7727093AD23C392E666ED02F75BFC723CECDAA1_StaticFields, ___names_2)); }
	inline RuntimeObject* get_names_2() const { return ___names_2; }
	inline RuntimeObject** get_address_of_names_2() { return &___names_2; }
	inline void set_names_2(RuntimeObject* value)
	{
		___names_2 = value;
		Il2CppCodeGenWriteBarrier((&___names_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TELETRUSTNAMEDCURVES_TD7727093AD23C392E666ED02F75BFC723CECDAA1_H
#ifndef TELETRUSTOBJECTIDENTIFIERS_T6BCD3C3595B30AF2BE84D9A55DE2C0AAA498F5D6_H
#define TELETRUSTOBJECTIDENTIFIERS_T6BCD3C3595B30AF2BE84D9A55DE2C0AAA498F5D6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTObjectIdentifiers
struct  TeleTrusTObjectIdentifiers_t6BCD3C3595B30AF2BE84D9A55DE2C0AAA498F5D6  : public RuntimeObject
{
public:

public:
};

struct TeleTrusTObjectIdentifiers_t6BCD3C3595B30AF2BE84D9A55DE2C0AAA498F5D6_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTObjectIdentifiers::TeleTrusTAlgorithm
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___TeleTrusTAlgorithm_0;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTObjectIdentifiers::RipeMD160
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___RipeMD160_1;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTObjectIdentifiers::RipeMD128
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___RipeMD128_2;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTObjectIdentifiers::RipeMD256
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___RipeMD256_3;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTObjectIdentifiers::TeleTrusTRsaSignatureAlgorithm
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___TeleTrusTRsaSignatureAlgorithm_4;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTObjectIdentifiers::RsaSignatureWithRipeMD160
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___RsaSignatureWithRipeMD160_5;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTObjectIdentifiers::RsaSignatureWithRipeMD128
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___RsaSignatureWithRipeMD128_6;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTObjectIdentifiers::RsaSignatureWithRipeMD256
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___RsaSignatureWithRipeMD256_7;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTObjectIdentifiers::ECSign
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___ECSign_8;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTObjectIdentifiers::ECSignWithSha1
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___ECSignWithSha1_9;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTObjectIdentifiers::ECSignWithRipeMD160
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___ECSignWithRipeMD160_10;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTObjectIdentifiers::EccBrainpool
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___EccBrainpool_11;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTObjectIdentifiers::EllipticCurve
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___EllipticCurve_12;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTObjectIdentifiers::VersionOne
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___VersionOne_13;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTObjectIdentifiers::BrainpoolP160R1
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___BrainpoolP160R1_14;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTObjectIdentifiers::BrainpoolP160T1
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___BrainpoolP160T1_15;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTObjectIdentifiers::BrainpoolP192R1
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___BrainpoolP192R1_16;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTObjectIdentifiers::BrainpoolP192T1
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___BrainpoolP192T1_17;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTObjectIdentifiers::BrainpoolP224R1
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___BrainpoolP224R1_18;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTObjectIdentifiers::BrainpoolP224T1
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___BrainpoolP224T1_19;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTObjectIdentifiers::BrainpoolP256R1
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___BrainpoolP256R1_20;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTObjectIdentifiers::BrainpoolP256T1
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___BrainpoolP256T1_21;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTObjectIdentifiers::BrainpoolP320R1
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___BrainpoolP320R1_22;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTObjectIdentifiers::BrainpoolP320T1
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___BrainpoolP320T1_23;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTObjectIdentifiers::BrainpoolP384R1
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___BrainpoolP384R1_24;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTObjectIdentifiers::BrainpoolP384T1
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___BrainpoolP384T1_25;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTObjectIdentifiers::BrainpoolP512R1
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___BrainpoolP512R1_26;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTObjectIdentifiers::BrainpoolP512T1
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___BrainpoolP512T1_27;

public:
	inline static int32_t get_offset_of_TeleTrusTAlgorithm_0() { return static_cast<int32_t>(offsetof(TeleTrusTObjectIdentifiers_t6BCD3C3595B30AF2BE84D9A55DE2C0AAA498F5D6_StaticFields, ___TeleTrusTAlgorithm_0)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_TeleTrusTAlgorithm_0() const { return ___TeleTrusTAlgorithm_0; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_TeleTrusTAlgorithm_0() { return &___TeleTrusTAlgorithm_0; }
	inline void set_TeleTrusTAlgorithm_0(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___TeleTrusTAlgorithm_0 = value;
		Il2CppCodeGenWriteBarrier((&___TeleTrusTAlgorithm_0), value);
	}

	inline static int32_t get_offset_of_RipeMD160_1() { return static_cast<int32_t>(offsetof(TeleTrusTObjectIdentifiers_t6BCD3C3595B30AF2BE84D9A55DE2C0AAA498F5D6_StaticFields, ___RipeMD160_1)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_RipeMD160_1() const { return ___RipeMD160_1; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_RipeMD160_1() { return &___RipeMD160_1; }
	inline void set_RipeMD160_1(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___RipeMD160_1 = value;
		Il2CppCodeGenWriteBarrier((&___RipeMD160_1), value);
	}

	inline static int32_t get_offset_of_RipeMD128_2() { return static_cast<int32_t>(offsetof(TeleTrusTObjectIdentifiers_t6BCD3C3595B30AF2BE84D9A55DE2C0AAA498F5D6_StaticFields, ___RipeMD128_2)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_RipeMD128_2() const { return ___RipeMD128_2; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_RipeMD128_2() { return &___RipeMD128_2; }
	inline void set_RipeMD128_2(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___RipeMD128_2 = value;
		Il2CppCodeGenWriteBarrier((&___RipeMD128_2), value);
	}

	inline static int32_t get_offset_of_RipeMD256_3() { return static_cast<int32_t>(offsetof(TeleTrusTObjectIdentifiers_t6BCD3C3595B30AF2BE84D9A55DE2C0AAA498F5D6_StaticFields, ___RipeMD256_3)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_RipeMD256_3() const { return ___RipeMD256_3; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_RipeMD256_3() { return &___RipeMD256_3; }
	inline void set_RipeMD256_3(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___RipeMD256_3 = value;
		Il2CppCodeGenWriteBarrier((&___RipeMD256_3), value);
	}

	inline static int32_t get_offset_of_TeleTrusTRsaSignatureAlgorithm_4() { return static_cast<int32_t>(offsetof(TeleTrusTObjectIdentifiers_t6BCD3C3595B30AF2BE84D9A55DE2C0AAA498F5D6_StaticFields, ___TeleTrusTRsaSignatureAlgorithm_4)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_TeleTrusTRsaSignatureAlgorithm_4() const { return ___TeleTrusTRsaSignatureAlgorithm_4; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_TeleTrusTRsaSignatureAlgorithm_4() { return &___TeleTrusTRsaSignatureAlgorithm_4; }
	inline void set_TeleTrusTRsaSignatureAlgorithm_4(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___TeleTrusTRsaSignatureAlgorithm_4 = value;
		Il2CppCodeGenWriteBarrier((&___TeleTrusTRsaSignatureAlgorithm_4), value);
	}

	inline static int32_t get_offset_of_RsaSignatureWithRipeMD160_5() { return static_cast<int32_t>(offsetof(TeleTrusTObjectIdentifiers_t6BCD3C3595B30AF2BE84D9A55DE2C0AAA498F5D6_StaticFields, ___RsaSignatureWithRipeMD160_5)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_RsaSignatureWithRipeMD160_5() const { return ___RsaSignatureWithRipeMD160_5; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_RsaSignatureWithRipeMD160_5() { return &___RsaSignatureWithRipeMD160_5; }
	inline void set_RsaSignatureWithRipeMD160_5(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___RsaSignatureWithRipeMD160_5 = value;
		Il2CppCodeGenWriteBarrier((&___RsaSignatureWithRipeMD160_5), value);
	}

	inline static int32_t get_offset_of_RsaSignatureWithRipeMD128_6() { return static_cast<int32_t>(offsetof(TeleTrusTObjectIdentifiers_t6BCD3C3595B30AF2BE84D9A55DE2C0AAA498F5D6_StaticFields, ___RsaSignatureWithRipeMD128_6)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_RsaSignatureWithRipeMD128_6() const { return ___RsaSignatureWithRipeMD128_6; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_RsaSignatureWithRipeMD128_6() { return &___RsaSignatureWithRipeMD128_6; }
	inline void set_RsaSignatureWithRipeMD128_6(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___RsaSignatureWithRipeMD128_6 = value;
		Il2CppCodeGenWriteBarrier((&___RsaSignatureWithRipeMD128_6), value);
	}

	inline static int32_t get_offset_of_RsaSignatureWithRipeMD256_7() { return static_cast<int32_t>(offsetof(TeleTrusTObjectIdentifiers_t6BCD3C3595B30AF2BE84D9A55DE2C0AAA498F5D6_StaticFields, ___RsaSignatureWithRipeMD256_7)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_RsaSignatureWithRipeMD256_7() const { return ___RsaSignatureWithRipeMD256_7; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_RsaSignatureWithRipeMD256_7() { return &___RsaSignatureWithRipeMD256_7; }
	inline void set_RsaSignatureWithRipeMD256_7(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___RsaSignatureWithRipeMD256_7 = value;
		Il2CppCodeGenWriteBarrier((&___RsaSignatureWithRipeMD256_7), value);
	}

	inline static int32_t get_offset_of_ECSign_8() { return static_cast<int32_t>(offsetof(TeleTrusTObjectIdentifiers_t6BCD3C3595B30AF2BE84D9A55DE2C0AAA498F5D6_StaticFields, ___ECSign_8)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_ECSign_8() const { return ___ECSign_8; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_ECSign_8() { return &___ECSign_8; }
	inline void set_ECSign_8(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___ECSign_8 = value;
		Il2CppCodeGenWriteBarrier((&___ECSign_8), value);
	}

	inline static int32_t get_offset_of_ECSignWithSha1_9() { return static_cast<int32_t>(offsetof(TeleTrusTObjectIdentifiers_t6BCD3C3595B30AF2BE84D9A55DE2C0AAA498F5D6_StaticFields, ___ECSignWithSha1_9)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_ECSignWithSha1_9() const { return ___ECSignWithSha1_9; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_ECSignWithSha1_9() { return &___ECSignWithSha1_9; }
	inline void set_ECSignWithSha1_9(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___ECSignWithSha1_9 = value;
		Il2CppCodeGenWriteBarrier((&___ECSignWithSha1_9), value);
	}

	inline static int32_t get_offset_of_ECSignWithRipeMD160_10() { return static_cast<int32_t>(offsetof(TeleTrusTObjectIdentifiers_t6BCD3C3595B30AF2BE84D9A55DE2C0AAA498F5D6_StaticFields, ___ECSignWithRipeMD160_10)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_ECSignWithRipeMD160_10() const { return ___ECSignWithRipeMD160_10; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_ECSignWithRipeMD160_10() { return &___ECSignWithRipeMD160_10; }
	inline void set_ECSignWithRipeMD160_10(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___ECSignWithRipeMD160_10 = value;
		Il2CppCodeGenWriteBarrier((&___ECSignWithRipeMD160_10), value);
	}

	inline static int32_t get_offset_of_EccBrainpool_11() { return static_cast<int32_t>(offsetof(TeleTrusTObjectIdentifiers_t6BCD3C3595B30AF2BE84D9A55DE2C0AAA498F5D6_StaticFields, ___EccBrainpool_11)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_EccBrainpool_11() const { return ___EccBrainpool_11; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_EccBrainpool_11() { return &___EccBrainpool_11; }
	inline void set_EccBrainpool_11(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___EccBrainpool_11 = value;
		Il2CppCodeGenWriteBarrier((&___EccBrainpool_11), value);
	}

	inline static int32_t get_offset_of_EllipticCurve_12() { return static_cast<int32_t>(offsetof(TeleTrusTObjectIdentifiers_t6BCD3C3595B30AF2BE84D9A55DE2C0AAA498F5D6_StaticFields, ___EllipticCurve_12)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_EllipticCurve_12() const { return ___EllipticCurve_12; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_EllipticCurve_12() { return &___EllipticCurve_12; }
	inline void set_EllipticCurve_12(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___EllipticCurve_12 = value;
		Il2CppCodeGenWriteBarrier((&___EllipticCurve_12), value);
	}

	inline static int32_t get_offset_of_VersionOne_13() { return static_cast<int32_t>(offsetof(TeleTrusTObjectIdentifiers_t6BCD3C3595B30AF2BE84D9A55DE2C0AAA498F5D6_StaticFields, ___VersionOne_13)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_VersionOne_13() const { return ___VersionOne_13; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_VersionOne_13() { return &___VersionOne_13; }
	inline void set_VersionOne_13(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___VersionOne_13 = value;
		Il2CppCodeGenWriteBarrier((&___VersionOne_13), value);
	}

	inline static int32_t get_offset_of_BrainpoolP160R1_14() { return static_cast<int32_t>(offsetof(TeleTrusTObjectIdentifiers_t6BCD3C3595B30AF2BE84D9A55DE2C0AAA498F5D6_StaticFields, ___BrainpoolP160R1_14)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_BrainpoolP160R1_14() const { return ___BrainpoolP160R1_14; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_BrainpoolP160R1_14() { return &___BrainpoolP160R1_14; }
	inline void set_BrainpoolP160R1_14(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___BrainpoolP160R1_14 = value;
		Il2CppCodeGenWriteBarrier((&___BrainpoolP160R1_14), value);
	}

	inline static int32_t get_offset_of_BrainpoolP160T1_15() { return static_cast<int32_t>(offsetof(TeleTrusTObjectIdentifiers_t6BCD3C3595B30AF2BE84D9A55DE2C0AAA498F5D6_StaticFields, ___BrainpoolP160T1_15)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_BrainpoolP160T1_15() const { return ___BrainpoolP160T1_15; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_BrainpoolP160T1_15() { return &___BrainpoolP160T1_15; }
	inline void set_BrainpoolP160T1_15(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___BrainpoolP160T1_15 = value;
		Il2CppCodeGenWriteBarrier((&___BrainpoolP160T1_15), value);
	}

	inline static int32_t get_offset_of_BrainpoolP192R1_16() { return static_cast<int32_t>(offsetof(TeleTrusTObjectIdentifiers_t6BCD3C3595B30AF2BE84D9A55DE2C0AAA498F5D6_StaticFields, ___BrainpoolP192R1_16)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_BrainpoolP192R1_16() const { return ___BrainpoolP192R1_16; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_BrainpoolP192R1_16() { return &___BrainpoolP192R1_16; }
	inline void set_BrainpoolP192R1_16(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___BrainpoolP192R1_16 = value;
		Il2CppCodeGenWriteBarrier((&___BrainpoolP192R1_16), value);
	}

	inline static int32_t get_offset_of_BrainpoolP192T1_17() { return static_cast<int32_t>(offsetof(TeleTrusTObjectIdentifiers_t6BCD3C3595B30AF2BE84D9A55DE2C0AAA498F5D6_StaticFields, ___BrainpoolP192T1_17)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_BrainpoolP192T1_17() const { return ___BrainpoolP192T1_17; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_BrainpoolP192T1_17() { return &___BrainpoolP192T1_17; }
	inline void set_BrainpoolP192T1_17(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___BrainpoolP192T1_17 = value;
		Il2CppCodeGenWriteBarrier((&___BrainpoolP192T1_17), value);
	}

	inline static int32_t get_offset_of_BrainpoolP224R1_18() { return static_cast<int32_t>(offsetof(TeleTrusTObjectIdentifiers_t6BCD3C3595B30AF2BE84D9A55DE2C0AAA498F5D6_StaticFields, ___BrainpoolP224R1_18)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_BrainpoolP224R1_18() const { return ___BrainpoolP224R1_18; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_BrainpoolP224R1_18() { return &___BrainpoolP224R1_18; }
	inline void set_BrainpoolP224R1_18(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___BrainpoolP224R1_18 = value;
		Il2CppCodeGenWriteBarrier((&___BrainpoolP224R1_18), value);
	}

	inline static int32_t get_offset_of_BrainpoolP224T1_19() { return static_cast<int32_t>(offsetof(TeleTrusTObjectIdentifiers_t6BCD3C3595B30AF2BE84D9A55DE2C0AAA498F5D6_StaticFields, ___BrainpoolP224T1_19)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_BrainpoolP224T1_19() const { return ___BrainpoolP224T1_19; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_BrainpoolP224T1_19() { return &___BrainpoolP224T1_19; }
	inline void set_BrainpoolP224T1_19(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___BrainpoolP224T1_19 = value;
		Il2CppCodeGenWriteBarrier((&___BrainpoolP224T1_19), value);
	}

	inline static int32_t get_offset_of_BrainpoolP256R1_20() { return static_cast<int32_t>(offsetof(TeleTrusTObjectIdentifiers_t6BCD3C3595B30AF2BE84D9A55DE2C0AAA498F5D6_StaticFields, ___BrainpoolP256R1_20)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_BrainpoolP256R1_20() const { return ___BrainpoolP256R1_20; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_BrainpoolP256R1_20() { return &___BrainpoolP256R1_20; }
	inline void set_BrainpoolP256R1_20(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___BrainpoolP256R1_20 = value;
		Il2CppCodeGenWriteBarrier((&___BrainpoolP256R1_20), value);
	}

	inline static int32_t get_offset_of_BrainpoolP256T1_21() { return static_cast<int32_t>(offsetof(TeleTrusTObjectIdentifiers_t6BCD3C3595B30AF2BE84D9A55DE2C0AAA498F5D6_StaticFields, ___BrainpoolP256T1_21)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_BrainpoolP256T1_21() const { return ___BrainpoolP256T1_21; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_BrainpoolP256T1_21() { return &___BrainpoolP256T1_21; }
	inline void set_BrainpoolP256T1_21(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___BrainpoolP256T1_21 = value;
		Il2CppCodeGenWriteBarrier((&___BrainpoolP256T1_21), value);
	}

	inline static int32_t get_offset_of_BrainpoolP320R1_22() { return static_cast<int32_t>(offsetof(TeleTrusTObjectIdentifiers_t6BCD3C3595B30AF2BE84D9A55DE2C0AAA498F5D6_StaticFields, ___BrainpoolP320R1_22)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_BrainpoolP320R1_22() const { return ___BrainpoolP320R1_22; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_BrainpoolP320R1_22() { return &___BrainpoolP320R1_22; }
	inline void set_BrainpoolP320R1_22(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___BrainpoolP320R1_22 = value;
		Il2CppCodeGenWriteBarrier((&___BrainpoolP320R1_22), value);
	}

	inline static int32_t get_offset_of_BrainpoolP320T1_23() { return static_cast<int32_t>(offsetof(TeleTrusTObjectIdentifiers_t6BCD3C3595B30AF2BE84D9A55DE2C0AAA498F5D6_StaticFields, ___BrainpoolP320T1_23)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_BrainpoolP320T1_23() const { return ___BrainpoolP320T1_23; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_BrainpoolP320T1_23() { return &___BrainpoolP320T1_23; }
	inline void set_BrainpoolP320T1_23(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___BrainpoolP320T1_23 = value;
		Il2CppCodeGenWriteBarrier((&___BrainpoolP320T1_23), value);
	}

	inline static int32_t get_offset_of_BrainpoolP384R1_24() { return static_cast<int32_t>(offsetof(TeleTrusTObjectIdentifiers_t6BCD3C3595B30AF2BE84D9A55DE2C0AAA498F5D6_StaticFields, ___BrainpoolP384R1_24)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_BrainpoolP384R1_24() const { return ___BrainpoolP384R1_24; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_BrainpoolP384R1_24() { return &___BrainpoolP384R1_24; }
	inline void set_BrainpoolP384R1_24(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___BrainpoolP384R1_24 = value;
		Il2CppCodeGenWriteBarrier((&___BrainpoolP384R1_24), value);
	}

	inline static int32_t get_offset_of_BrainpoolP384T1_25() { return static_cast<int32_t>(offsetof(TeleTrusTObjectIdentifiers_t6BCD3C3595B30AF2BE84D9A55DE2C0AAA498F5D6_StaticFields, ___BrainpoolP384T1_25)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_BrainpoolP384T1_25() const { return ___BrainpoolP384T1_25; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_BrainpoolP384T1_25() { return &___BrainpoolP384T1_25; }
	inline void set_BrainpoolP384T1_25(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___BrainpoolP384T1_25 = value;
		Il2CppCodeGenWriteBarrier((&___BrainpoolP384T1_25), value);
	}

	inline static int32_t get_offset_of_BrainpoolP512R1_26() { return static_cast<int32_t>(offsetof(TeleTrusTObjectIdentifiers_t6BCD3C3595B30AF2BE84D9A55DE2C0AAA498F5D6_StaticFields, ___BrainpoolP512R1_26)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_BrainpoolP512R1_26() const { return ___BrainpoolP512R1_26; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_BrainpoolP512R1_26() { return &___BrainpoolP512R1_26; }
	inline void set_BrainpoolP512R1_26(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___BrainpoolP512R1_26 = value;
		Il2CppCodeGenWriteBarrier((&___BrainpoolP512R1_26), value);
	}

	inline static int32_t get_offset_of_BrainpoolP512T1_27() { return static_cast<int32_t>(offsetof(TeleTrusTObjectIdentifiers_t6BCD3C3595B30AF2BE84D9A55DE2C0AAA498F5D6_StaticFields, ___BrainpoolP512T1_27)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_BrainpoolP512T1_27() const { return ___BrainpoolP512T1_27; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_BrainpoolP512T1_27() { return &___BrainpoolP512T1_27; }
	inline void set_BrainpoolP512T1_27(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___BrainpoolP512T1_27 = value;
		Il2CppCodeGenWriteBarrier((&___BrainpoolP512T1_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TELETRUSTOBJECTIDENTIFIERS_T6BCD3C3595B30AF2BE84D9A55DE2C0AAA498F5D6_H
#ifndef ASN1DUMP_T8190340D0A1311E1019091CF6D7D55A6A5B45C3F_H
#define ASN1DUMP_T8190340D0A1311E1019091CF6D7D55A6A5B45C3F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.Utilities.Asn1Dump
struct  Asn1Dump_t8190340D0A1311E1019091CF6D7D55A6A5B45C3F  : public RuntimeObject
{
public:

public:
};

struct Asn1Dump_t8190340D0A1311E1019091CF6D7D55A6A5B45C3F_StaticFields
{
public:
	// System.String Org.BouncyCastle.Asn1.Utilities.Asn1Dump::NewLine
	String_t* ___NewLine_0;

public:
	inline static int32_t get_offset_of_NewLine_0() { return static_cast<int32_t>(offsetof(Asn1Dump_t8190340D0A1311E1019091CF6D7D55A6A5B45C3F_StaticFields, ___NewLine_0)); }
	inline String_t* get_NewLine_0() const { return ___NewLine_0; }
	inline String_t** get_address_of_NewLine_0() { return &___NewLine_0; }
	inline void set_NewLine_0(String_t* value)
	{
		___NewLine_0 = value;
		Il2CppCodeGenWriteBarrier((&___NewLine_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASN1DUMP_T8190340D0A1311E1019091CF6D7D55A6A5B45C3F_H
#ifndef X509EXTENSION_T523236F1B9FC5A5FE68AE968262556435B4E8E5A_H
#define X509EXTENSION_T523236F1B9FC5A5FE68AE968262556435B4E8E5A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.X509.X509Extension
struct  X509Extension_t523236F1B9FC5A5FE68AE968262556435B4E8E5A  : public RuntimeObject
{
public:
	// System.Boolean Org.BouncyCastle.Asn1.X509.X509Extension::critical
	bool ___critical_0;
	// Org.BouncyCastle.Asn1.Asn1OctetString Org.BouncyCastle.Asn1.X509.X509Extension::value
	Asn1OctetString_t2088F9E88E4C97BB9CF3B2FFC53CFBAF9B8B79CB * ___value_1;

public:
	inline static int32_t get_offset_of_critical_0() { return static_cast<int32_t>(offsetof(X509Extension_t523236F1B9FC5A5FE68AE968262556435B4E8E5A, ___critical_0)); }
	inline bool get_critical_0() const { return ___critical_0; }
	inline bool* get_address_of_critical_0() { return &___critical_0; }
	inline void set_critical_0(bool value)
	{
		___critical_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(X509Extension_t523236F1B9FC5A5FE68AE968262556435B4E8E5A, ___value_1)); }
	inline Asn1OctetString_t2088F9E88E4C97BB9CF3B2FFC53CFBAF9B8B79CB * get_value_1() const { return ___value_1; }
	inline Asn1OctetString_t2088F9E88E4C97BB9CF3B2FFC53CFBAF9B8B79CB ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(Asn1OctetString_t2088F9E88E4C97BB9CF3B2FFC53CFBAF9B8B79CB * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509EXTENSION_T523236F1B9FC5A5FE68AE968262556435B4E8E5A_H
#ifndef X509NAMEENTRYCONVERTER_T6B3AE1DD9E7B91F1C0039575CEAC00CAE0972DED_H
#define X509NAMEENTRYCONVERTER_T6B3AE1DD9E7B91F1C0039575CEAC00CAE0972DED_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.X509.X509NameEntryConverter
struct  X509NameEntryConverter_t6B3AE1DD9E7B91F1C0039575CEAC00CAE0972DED  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509NAMEENTRYCONVERTER_T6B3AE1DD9E7B91F1C0039575CEAC00CAE0972DED_H
#ifndef X509OBJECTIDENTIFIERS_T94E84E96A76E1618C0058E4FE97D3A245E4B4154_H
#define X509OBJECTIDENTIFIERS_T94E84E96A76E1618C0058E4FE97D3A245E4B4154_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.X509.X509ObjectIdentifiers
struct  X509ObjectIdentifiers_t94E84E96A76E1618C0058E4FE97D3A245E4B4154  : public RuntimeObject
{
public:

public:
};

struct X509ObjectIdentifiers_t94E84E96A76E1618C0058E4FE97D3A245E4B4154_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509ObjectIdentifiers::CommonName
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___CommonName_0;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509ObjectIdentifiers::CountryName
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___CountryName_1;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509ObjectIdentifiers::LocalityName
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___LocalityName_2;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509ObjectIdentifiers::StateOrProvinceName
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___StateOrProvinceName_3;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509ObjectIdentifiers::Organization
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___Organization_4;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509ObjectIdentifiers::OrganizationalUnitName
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___OrganizationalUnitName_5;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509ObjectIdentifiers::id_at_telephoneNumber
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___id_at_telephoneNumber_6;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509ObjectIdentifiers::id_at_name
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___id_at_name_7;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509ObjectIdentifiers::IdSha1
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdSha1_8;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509ObjectIdentifiers::RipeMD160
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___RipeMD160_9;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509ObjectIdentifiers::RipeMD160WithRsaEncryption
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___RipeMD160WithRsaEncryption_10;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509ObjectIdentifiers::IdEARsa
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdEARsa_11;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509ObjectIdentifiers::IdPkix
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdPkix_12;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509ObjectIdentifiers::IdPE
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdPE_13;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509ObjectIdentifiers::IdAD
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdAD_14;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509ObjectIdentifiers::IdADCAIssuers
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdADCAIssuers_15;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509ObjectIdentifiers::IdADOcsp
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdADOcsp_16;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509ObjectIdentifiers::OcspAccessMethod
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___OcspAccessMethod_17;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509ObjectIdentifiers::CrlAccessMethod
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___CrlAccessMethod_18;

public:
	inline static int32_t get_offset_of_CommonName_0() { return static_cast<int32_t>(offsetof(X509ObjectIdentifiers_t94E84E96A76E1618C0058E4FE97D3A245E4B4154_StaticFields, ___CommonName_0)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_CommonName_0() const { return ___CommonName_0; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_CommonName_0() { return &___CommonName_0; }
	inline void set_CommonName_0(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___CommonName_0 = value;
		Il2CppCodeGenWriteBarrier((&___CommonName_0), value);
	}

	inline static int32_t get_offset_of_CountryName_1() { return static_cast<int32_t>(offsetof(X509ObjectIdentifiers_t94E84E96A76E1618C0058E4FE97D3A245E4B4154_StaticFields, ___CountryName_1)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_CountryName_1() const { return ___CountryName_1; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_CountryName_1() { return &___CountryName_1; }
	inline void set_CountryName_1(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___CountryName_1 = value;
		Il2CppCodeGenWriteBarrier((&___CountryName_1), value);
	}

	inline static int32_t get_offset_of_LocalityName_2() { return static_cast<int32_t>(offsetof(X509ObjectIdentifiers_t94E84E96A76E1618C0058E4FE97D3A245E4B4154_StaticFields, ___LocalityName_2)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_LocalityName_2() const { return ___LocalityName_2; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_LocalityName_2() { return &___LocalityName_2; }
	inline void set_LocalityName_2(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___LocalityName_2 = value;
		Il2CppCodeGenWriteBarrier((&___LocalityName_2), value);
	}

	inline static int32_t get_offset_of_StateOrProvinceName_3() { return static_cast<int32_t>(offsetof(X509ObjectIdentifiers_t94E84E96A76E1618C0058E4FE97D3A245E4B4154_StaticFields, ___StateOrProvinceName_3)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_StateOrProvinceName_3() const { return ___StateOrProvinceName_3; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_StateOrProvinceName_3() { return &___StateOrProvinceName_3; }
	inline void set_StateOrProvinceName_3(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___StateOrProvinceName_3 = value;
		Il2CppCodeGenWriteBarrier((&___StateOrProvinceName_3), value);
	}

	inline static int32_t get_offset_of_Organization_4() { return static_cast<int32_t>(offsetof(X509ObjectIdentifiers_t94E84E96A76E1618C0058E4FE97D3A245E4B4154_StaticFields, ___Organization_4)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_Organization_4() const { return ___Organization_4; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_Organization_4() { return &___Organization_4; }
	inline void set_Organization_4(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___Organization_4 = value;
		Il2CppCodeGenWriteBarrier((&___Organization_4), value);
	}

	inline static int32_t get_offset_of_OrganizationalUnitName_5() { return static_cast<int32_t>(offsetof(X509ObjectIdentifiers_t94E84E96A76E1618C0058E4FE97D3A245E4B4154_StaticFields, ___OrganizationalUnitName_5)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_OrganizationalUnitName_5() const { return ___OrganizationalUnitName_5; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_OrganizationalUnitName_5() { return &___OrganizationalUnitName_5; }
	inline void set_OrganizationalUnitName_5(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___OrganizationalUnitName_5 = value;
		Il2CppCodeGenWriteBarrier((&___OrganizationalUnitName_5), value);
	}

	inline static int32_t get_offset_of_id_at_telephoneNumber_6() { return static_cast<int32_t>(offsetof(X509ObjectIdentifiers_t94E84E96A76E1618C0058E4FE97D3A245E4B4154_StaticFields, ___id_at_telephoneNumber_6)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_id_at_telephoneNumber_6() const { return ___id_at_telephoneNumber_6; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_id_at_telephoneNumber_6() { return &___id_at_telephoneNumber_6; }
	inline void set_id_at_telephoneNumber_6(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___id_at_telephoneNumber_6 = value;
		Il2CppCodeGenWriteBarrier((&___id_at_telephoneNumber_6), value);
	}

	inline static int32_t get_offset_of_id_at_name_7() { return static_cast<int32_t>(offsetof(X509ObjectIdentifiers_t94E84E96A76E1618C0058E4FE97D3A245E4B4154_StaticFields, ___id_at_name_7)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_id_at_name_7() const { return ___id_at_name_7; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_id_at_name_7() { return &___id_at_name_7; }
	inline void set_id_at_name_7(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___id_at_name_7 = value;
		Il2CppCodeGenWriteBarrier((&___id_at_name_7), value);
	}

	inline static int32_t get_offset_of_IdSha1_8() { return static_cast<int32_t>(offsetof(X509ObjectIdentifiers_t94E84E96A76E1618C0058E4FE97D3A245E4B4154_StaticFields, ___IdSha1_8)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdSha1_8() const { return ___IdSha1_8; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdSha1_8() { return &___IdSha1_8; }
	inline void set_IdSha1_8(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdSha1_8 = value;
		Il2CppCodeGenWriteBarrier((&___IdSha1_8), value);
	}

	inline static int32_t get_offset_of_RipeMD160_9() { return static_cast<int32_t>(offsetof(X509ObjectIdentifiers_t94E84E96A76E1618C0058E4FE97D3A245E4B4154_StaticFields, ___RipeMD160_9)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_RipeMD160_9() const { return ___RipeMD160_9; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_RipeMD160_9() { return &___RipeMD160_9; }
	inline void set_RipeMD160_9(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___RipeMD160_9 = value;
		Il2CppCodeGenWriteBarrier((&___RipeMD160_9), value);
	}

	inline static int32_t get_offset_of_RipeMD160WithRsaEncryption_10() { return static_cast<int32_t>(offsetof(X509ObjectIdentifiers_t94E84E96A76E1618C0058E4FE97D3A245E4B4154_StaticFields, ___RipeMD160WithRsaEncryption_10)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_RipeMD160WithRsaEncryption_10() const { return ___RipeMD160WithRsaEncryption_10; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_RipeMD160WithRsaEncryption_10() { return &___RipeMD160WithRsaEncryption_10; }
	inline void set_RipeMD160WithRsaEncryption_10(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___RipeMD160WithRsaEncryption_10 = value;
		Il2CppCodeGenWriteBarrier((&___RipeMD160WithRsaEncryption_10), value);
	}

	inline static int32_t get_offset_of_IdEARsa_11() { return static_cast<int32_t>(offsetof(X509ObjectIdentifiers_t94E84E96A76E1618C0058E4FE97D3A245E4B4154_StaticFields, ___IdEARsa_11)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdEARsa_11() const { return ___IdEARsa_11; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdEARsa_11() { return &___IdEARsa_11; }
	inline void set_IdEARsa_11(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdEARsa_11 = value;
		Il2CppCodeGenWriteBarrier((&___IdEARsa_11), value);
	}

	inline static int32_t get_offset_of_IdPkix_12() { return static_cast<int32_t>(offsetof(X509ObjectIdentifiers_t94E84E96A76E1618C0058E4FE97D3A245E4B4154_StaticFields, ___IdPkix_12)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdPkix_12() const { return ___IdPkix_12; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdPkix_12() { return &___IdPkix_12; }
	inline void set_IdPkix_12(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdPkix_12 = value;
		Il2CppCodeGenWriteBarrier((&___IdPkix_12), value);
	}

	inline static int32_t get_offset_of_IdPE_13() { return static_cast<int32_t>(offsetof(X509ObjectIdentifiers_t94E84E96A76E1618C0058E4FE97D3A245E4B4154_StaticFields, ___IdPE_13)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdPE_13() const { return ___IdPE_13; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdPE_13() { return &___IdPE_13; }
	inline void set_IdPE_13(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdPE_13 = value;
		Il2CppCodeGenWriteBarrier((&___IdPE_13), value);
	}

	inline static int32_t get_offset_of_IdAD_14() { return static_cast<int32_t>(offsetof(X509ObjectIdentifiers_t94E84E96A76E1618C0058E4FE97D3A245E4B4154_StaticFields, ___IdAD_14)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdAD_14() const { return ___IdAD_14; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdAD_14() { return &___IdAD_14; }
	inline void set_IdAD_14(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdAD_14 = value;
		Il2CppCodeGenWriteBarrier((&___IdAD_14), value);
	}

	inline static int32_t get_offset_of_IdADCAIssuers_15() { return static_cast<int32_t>(offsetof(X509ObjectIdentifiers_t94E84E96A76E1618C0058E4FE97D3A245E4B4154_StaticFields, ___IdADCAIssuers_15)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdADCAIssuers_15() const { return ___IdADCAIssuers_15; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdADCAIssuers_15() { return &___IdADCAIssuers_15; }
	inline void set_IdADCAIssuers_15(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdADCAIssuers_15 = value;
		Il2CppCodeGenWriteBarrier((&___IdADCAIssuers_15), value);
	}

	inline static int32_t get_offset_of_IdADOcsp_16() { return static_cast<int32_t>(offsetof(X509ObjectIdentifiers_t94E84E96A76E1618C0058E4FE97D3A245E4B4154_StaticFields, ___IdADOcsp_16)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdADOcsp_16() const { return ___IdADOcsp_16; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdADOcsp_16() { return &___IdADOcsp_16; }
	inline void set_IdADOcsp_16(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdADOcsp_16 = value;
		Il2CppCodeGenWriteBarrier((&___IdADOcsp_16), value);
	}

	inline static int32_t get_offset_of_OcspAccessMethod_17() { return static_cast<int32_t>(offsetof(X509ObjectIdentifiers_t94E84E96A76E1618C0058E4FE97D3A245E4B4154_StaticFields, ___OcspAccessMethod_17)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_OcspAccessMethod_17() const { return ___OcspAccessMethod_17; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_OcspAccessMethod_17() { return &___OcspAccessMethod_17; }
	inline void set_OcspAccessMethod_17(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___OcspAccessMethod_17 = value;
		Il2CppCodeGenWriteBarrier((&___OcspAccessMethod_17), value);
	}

	inline static int32_t get_offset_of_CrlAccessMethod_18() { return static_cast<int32_t>(offsetof(X509ObjectIdentifiers_t94E84E96A76E1618C0058E4FE97D3A245E4B4154_StaticFields, ___CrlAccessMethod_18)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_CrlAccessMethod_18() const { return ___CrlAccessMethod_18; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_CrlAccessMethod_18() { return &___CrlAccessMethod_18; }
	inline void set_CrlAccessMethod_18(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___CrlAccessMethod_18 = value;
		Il2CppCodeGenWriteBarrier((&___CrlAccessMethod_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509OBJECTIDENTIFIERS_T94E84E96A76E1618C0058E4FE97D3A245E4B4154_H
#ifndef ECNAMEDCURVETABLE_T7A2393B7906F6758968480CFCE8759D5A24BF13B_H
#define ECNAMEDCURVETABLE_T7A2393B7906F6758968480CFCE8759D5A24BF13B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.X9.ECNamedCurveTable
struct  ECNamedCurveTable_t7A2393B7906F6758968480CFCE8759D5A24BF13B  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECNAMEDCURVETABLE_T7A2393B7906F6758968480CFCE8759D5A24BF13B_H
#ifndef X962NAMEDCURVES_T4B7ADF52483079BBA8E4518D8B32C1164430AF1B_H
#define X962NAMEDCURVES_T4B7ADF52483079BBA8E4518D8B32C1164430AF1B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.X9.X962NamedCurves
struct  X962NamedCurves_t4B7ADF52483079BBA8E4518D8B32C1164430AF1B  : public RuntimeObject
{
public:

public:
};

struct X962NamedCurves_t4B7ADF52483079BBA8E4518D8B32C1164430AF1B_StaticFields
{
public:
	// System.Collections.IDictionary Org.BouncyCastle.Asn1.X9.X962NamedCurves::objIds
	RuntimeObject* ___objIds_0;
	// System.Collections.IDictionary Org.BouncyCastle.Asn1.X9.X962NamedCurves::curves
	RuntimeObject* ___curves_1;
	// System.Collections.IDictionary Org.BouncyCastle.Asn1.X9.X962NamedCurves::names
	RuntimeObject* ___names_2;

public:
	inline static int32_t get_offset_of_objIds_0() { return static_cast<int32_t>(offsetof(X962NamedCurves_t4B7ADF52483079BBA8E4518D8B32C1164430AF1B_StaticFields, ___objIds_0)); }
	inline RuntimeObject* get_objIds_0() const { return ___objIds_0; }
	inline RuntimeObject** get_address_of_objIds_0() { return &___objIds_0; }
	inline void set_objIds_0(RuntimeObject* value)
	{
		___objIds_0 = value;
		Il2CppCodeGenWriteBarrier((&___objIds_0), value);
	}

	inline static int32_t get_offset_of_curves_1() { return static_cast<int32_t>(offsetof(X962NamedCurves_t4B7ADF52483079BBA8E4518D8B32C1164430AF1B_StaticFields, ___curves_1)); }
	inline RuntimeObject* get_curves_1() const { return ___curves_1; }
	inline RuntimeObject** get_address_of_curves_1() { return &___curves_1; }
	inline void set_curves_1(RuntimeObject* value)
	{
		___curves_1 = value;
		Il2CppCodeGenWriteBarrier((&___curves_1), value);
	}

	inline static int32_t get_offset_of_names_2() { return static_cast<int32_t>(offsetof(X962NamedCurves_t4B7ADF52483079BBA8E4518D8B32C1164430AF1B_StaticFields, ___names_2)); }
	inline RuntimeObject* get_names_2() const { return ___names_2; }
	inline RuntimeObject** get_address_of_names_2() { return &___names_2; }
	inline void set_names_2(RuntimeObject* value)
	{
		___names_2 = value;
		Il2CppCodeGenWriteBarrier((&___names_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X962NAMEDCURVES_T4B7ADF52483079BBA8E4518D8B32C1164430AF1B_H
#ifndef X9ECPARAMETERSHOLDER_TB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE_H
#define X9ECPARAMETERSHOLDER_TB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder
struct  X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE  : public RuntimeObject
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParameters Org.BouncyCastle.Asn1.X9.X9ECParametersHolder::parameters
	X9ECParameters_t818A7DD4D13D91F0D36622C6C117D7E9A10E51C3 * ___parameters_0;

public:
	inline static int32_t get_offset_of_parameters_0() { return static_cast<int32_t>(offsetof(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE, ___parameters_0)); }
	inline X9ECParameters_t818A7DD4D13D91F0D36622C6C117D7E9A10E51C3 * get_parameters_0() const { return ___parameters_0; }
	inline X9ECParameters_t818A7DD4D13D91F0D36622C6C117D7E9A10E51C3 ** get_address_of_parameters_0() { return &___parameters_0; }
	inline void set_parameters_0(X9ECParameters_t818A7DD4D13D91F0D36622C6C117D7E9A10E51C3 * value)
	{
		___parameters_0 = value;
		Il2CppCodeGenWriteBarrier((&___parameters_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X9ECPARAMETERSHOLDER_TB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE_H
#ifndef ASN1OBJECT_TB780E50883209D5C975E83B3D16257DFC3CCE36D_H
#define ASN1OBJECT_TB780E50883209D5C975E83B3D16257DFC3CCE36D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.Asn1Object
struct  Asn1Object_tB780E50883209D5C975E83B3D16257DFC3CCE36D  : public Asn1Encodable_t390FCE82F5F52E41A7320BBF2E8BF18DA1622A8B
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASN1OBJECT_TB780E50883209D5C975E83B3D16257DFC3CCE36D_H
#ifndef OCSPRESPONSE_TF9B1DEA63B2DD9043941E80847BDF860FD518211_H
#define OCSPRESPONSE_TF9B1DEA63B2DD9043941E80847BDF860FD518211_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.Ocsp.OcspResponse
struct  OcspResponse_tF9B1DEA63B2DD9043941E80847BDF860FD518211  : public Asn1Encodable_t390FCE82F5F52E41A7320BBF2E8BF18DA1622A8B
{
public:
	// Org.BouncyCastle.Asn1.Ocsp.OcspResponseStatus Org.BouncyCastle.Asn1.Ocsp.OcspResponse::responseStatus
	OcspResponseStatus_t40E3D3B60B45A3E985B8678DCD3E60445517912F * ___responseStatus_0;
	// Org.BouncyCastle.Asn1.Ocsp.ResponseBytes Org.BouncyCastle.Asn1.Ocsp.OcspResponse::responseBytes
	ResponseBytes_t0E85F6E803AB354A78B7EBABB885F182805BAE25 * ___responseBytes_1;

public:
	inline static int32_t get_offset_of_responseStatus_0() { return static_cast<int32_t>(offsetof(OcspResponse_tF9B1DEA63B2DD9043941E80847BDF860FD518211, ___responseStatus_0)); }
	inline OcspResponseStatus_t40E3D3B60B45A3E985B8678DCD3E60445517912F * get_responseStatus_0() const { return ___responseStatus_0; }
	inline OcspResponseStatus_t40E3D3B60B45A3E985B8678DCD3E60445517912F ** get_address_of_responseStatus_0() { return &___responseStatus_0; }
	inline void set_responseStatus_0(OcspResponseStatus_t40E3D3B60B45A3E985B8678DCD3E60445517912F * value)
	{
		___responseStatus_0 = value;
		Il2CppCodeGenWriteBarrier((&___responseStatus_0), value);
	}

	inline static int32_t get_offset_of_responseBytes_1() { return static_cast<int32_t>(offsetof(OcspResponse_tF9B1DEA63B2DD9043941E80847BDF860FD518211, ___responseBytes_1)); }
	inline ResponseBytes_t0E85F6E803AB354A78B7EBABB885F182805BAE25 * get_responseBytes_1() const { return ___responseBytes_1; }
	inline ResponseBytes_t0E85F6E803AB354A78B7EBABB885F182805BAE25 ** get_address_of_responseBytes_1() { return &___responseBytes_1; }
	inline void set_responseBytes_1(ResponseBytes_t0E85F6E803AB354A78B7EBABB885F182805BAE25 * value)
	{
		___responseBytes_1 = value;
		Il2CppCodeGenWriteBarrier((&___responseBytes_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OCSPRESPONSE_TF9B1DEA63B2DD9043941E80847BDF860FD518211_H
#ifndef RESPONSEBYTES_T0E85F6E803AB354A78B7EBABB885F182805BAE25_H
#define RESPONSEBYTES_T0E85F6E803AB354A78B7EBABB885F182805BAE25_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.Ocsp.ResponseBytes
struct  ResponseBytes_t0E85F6E803AB354A78B7EBABB885F182805BAE25  : public Asn1Encodable_t390FCE82F5F52E41A7320BBF2E8BF18DA1622A8B
{
public:
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Ocsp.ResponseBytes::responseType
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___responseType_0;
	// Org.BouncyCastle.Asn1.Asn1OctetString Org.BouncyCastle.Asn1.Ocsp.ResponseBytes::response
	Asn1OctetString_t2088F9E88E4C97BB9CF3B2FFC53CFBAF9B8B79CB * ___response_1;

public:
	inline static int32_t get_offset_of_responseType_0() { return static_cast<int32_t>(offsetof(ResponseBytes_t0E85F6E803AB354A78B7EBABB885F182805BAE25, ___responseType_0)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_responseType_0() const { return ___responseType_0; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_responseType_0() { return &___responseType_0; }
	inline void set_responseType_0(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___responseType_0 = value;
		Il2CppCodeGenWriteBarrier((&___responseType_0), value);
	}

	inline static int32_t get_offset_of_response_1() { return static_cast<int32_t>(offsetof(ResponseBytes_t0E85F6E803AB354A78B7EBABB885F182805BAE25, ___response_1)); }
	inline Asn1OctetString_t2088F9E88E4C97BB9CF3B2FFC53CFBAF9B8B79CB * get_response_1() const { return ___response_1; }
	inline Asn1OctetString_t2088F9E88E4C97BB9CF3B2FFC53CFBAF9B8B79CB ** get_address_of_response_1() { return &___response_1; }
	inline void set_response_1(Asn1OctetString_t2088F9E88E4C97BB9CF3B2FFC53CFBAF9B8B79CB * value)
	{
		___response_1 = value;
		Il2CppCodeGenWriteBarrier((&___response_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESPONSEBYTES_T0E85F6E803AB354A78B7EBABB885F182805BAE25_H
#ifndef ELGAMALPARAMETER_T81ED5105E6B28F0030F87FD61C4474CA463D2391_H
#define ELGAMALPARAMETER_T81ED5105E6B28F0030F87FD61C4474CA463D2391_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.Oiw.ElGamalParameter
struct  ElGamalParameter_t81ED5105E6B28F0030F87FD61C4474CA463D2391  : public Asn1Encodable_t390FCE82F5F52E41A7320BBF2E8BF18DA1622A8B
{
public:
	// Org.BouncyCastle.Asn1.DerInteger Org.BouncyCastle.Asn1.Oiw.ElGamalParameter::p
	DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A * ___p_0;
	// Org.BouncyCastle.Asn1.DerInteger Org.BouncyCastle.Asn1.Oiw.ElGamalParameter::g
	DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A * ___g_1;

public:
	inline static int32_t get_offset_of_p_0() { return static_cast<int32_t>(offsetof(ElGamalParameter_t81ED5105E6B28F0030F87FD61C4474CA463D2391, ___p_0)); }
	inline DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A * get_p_0() const { return ___p_0; }
	inline DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A ** get_address_of_p_0() { return &___p_0; }
	inline void set_p_0(DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A * value)
	{
		___p_0 = value;
		Il2CppCodeGenWriteBarrier((&___p_0), value);
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(ElGamalParameter_t81ED5105E6B28F0030F87FD61C4474CA463D2391, ___g_1)); }
	inline DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A * get_g_1() const { return ___g_1; }
	inline DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A ** get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A * value)
	{
		___g_1 = value;
		Il2CppCodeGenWriteBarrier((&___g_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELGAMALPARAMETER_T81ED5105E6B28F0030F87FD61C4474CA463D2391_H
#ifndef CONTENTINFO_T97148D828054F7B5E1586B21837C138F9EDE4C08_H
#define CONTENTINFO_T97148D828054F7B5E1586B21837C138F9EDE4C08_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.Pkcs.ContentInfo
struct  ContentInfo_t97148D828054F7B5E1586B21837C138F9EDE4C08  : public Asn1Encodable_t390FCE82F5F52E41A7320BBF2E8BF18DA1622A8B
{
public:
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Pkcs.ContentInfo::contentType
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___contentType_0;
	// Org.BouncyCastle.Asn1.Asn1Encodable Org.BouncyCastle.Asn1.Pkcs.ContentInfo::content
	Asn1Encodable_t390FCE82F5F52E41A7320BBF2E8BF18DA1622A8B * ___content_1;

public:
	inline static int32_t get_offset_of_contentType_0() { return static_cast<int32_t>(offsetof(ContentInfo_t97148D828054F7B5E1586B21837C138F9EDE4C08, ___contentType_0)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_contentType_0() const { return ___contentType_0; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_contentType_0() { return &___contentType_0; }
	inline void set_contentType_0(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___contentType_0 = value;
		Il2CppCodeGenWriteBarrier((&___contentType_0), value);
	}

	inline static int32_t get_offset_of_content_1() { return static_cast<int32_t>(offsetof(ContentInfo_t97148D828054F7B5E1586B21837C138F9EDE4C08, ___content_1)); }
	inline Asn1Encodable_t390FCE82F5F52E41A7320BBF2E8BF18DA1622A8B * get_content_1() const { return ___content_1; }
	inline Asn1Encodable_t390FCE82F5F52E41A7320BBF2E8BF18DA1622A8B ** get_address_of_content_1() { return &___content_1; }
	inline void set_content_1(Asn1Encodable_t390FCE82F5F52E41A7320BBF2E8BF18DA1622A8B * value)
	{
		___content_1 = value;
		Il2CppCodeGenWriteBarrier((&___content_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTENTINFO_T97148D828054F7B5E1586B21837C138F9EDE4C08_H
#ifndef DHPARAMETER_TB37DF4F0CA6CEDE5DE4F5B50619E3FD4ED63A106_H
#define DHPARAMETER_TB37DF4F0CA6CEDE5DE4F5B50619E3FD4ED63A106_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.Pkcs.DHParameter
struct  DHParameter_tB37DF4F0CA6CEDE5DE4F5B50619E3FD4ED63A106  : public Asn1Encodable_t390FCE82F5F52E41A7320BBF2E8BF18DA1622A8B
{
public:
	// Org.BouncyCastle.Asn1.DerInteger Org.BouncyCastle.Asn1.Pkcs.DHParameter::p
	DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A * ___p_0;
	// Org.BouncyCastle.Asn1.DerInteger Org.BouncyCastle.Asn1.Pkcs.DHParameter::g
	DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A * ___g_1;
	// Org.BouncyCastle.Asn1.DerInteger Org.BouncyCastle.Asn1.Pkcs.DHParameter::l
	DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A * ___l_2;

public:
	inline static int32_t get_offset_of_p_0() { return static_cast<int32_t>(offsetof(DHParameter_tB37DF4F0CA6CEDE5DE4F5B50619E3FD4ED63A106, ___p_0)); }
	inline DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A * get_p_0() const { return ___p_0; }
	inline DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A ** get_address_of_p_0() { return &___p_0; }
	inline void set_p_0(DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A * value)
	{
		___p_0 = value;
		Il2CppCodeGenWriteBarrier((&___p_0), value);
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(DHParameter_tB37DF4F0CA6CEDE5DE4F5B50619E3FD4ED63A106, ___g_1)); }
	inline DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A * get_g_1() const { return ___g_1; }
	inline DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A ** get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A * value)
	{
		___g_1 = value;
		Il2CppCodeGenWriteBarrier((&___g_1), value);
	}

	inline static int32_t get_offset_of_l_2() { return static_cast<int32_t>(offsetof(DHParameter_tB37DF4F0CA6CEDE5DE4F5B50619E3FD4ED63A106, ___l_2)); }
	inline DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A * get_l_2() const { return ___l_2; }
	inline DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A ** get_address_of_l_2() { return &___l_2; }
	inline void set_l_2(DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A * value)
	{
		___l_2 = value;
		Il2CppCodeGenWriteBarrier((&___l_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DHPARAMETER_TB37DF4F0CA6CEDE5DE4F5B50619E3FD4ED63A106_H
#ifndef SIGNEDDATA_TC530B1ACD0C32802BB3EFE180B486D0564B30129_H
#define SIGNEDDATA_TC530B1ACD0C32802BB3EFE180B486D0564B30129_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.Pkcs.SignedData
struct  SignedData_tC530B1ACD0C32802BB3EFE180B486D0564B30129  : public Asn1Encodable_t390FCE82F5F52E41A7320BBF2E8BF18DA1622A8B
{
public:
	// Org.BouncyCastle.Asn1.DerInteger Org.BouncyCastle.Asn1.Pkcs.SignedData::version
	DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A * ___version_0;
	// Org.BouncyCastle.Asn1.Asn1Set Org.BouncyCastle.Asn1.Pkcs.SignedData::digestAlgorithms
	Asn1Set_tD51325409687C84A095F7692B251BDE783A1A4A7 * ___digestAlgorithms_1;
	// Org.BouncyCastle.Asn1.Pkcs.ContentInfo Org.BouncyCastle.Asn1.Pkcs.SignedData::contentInfo
	ContentInfo_t97148D828054F7B5E1586B21837C138F9EDE4C08 * ___contentInfo_2;
	// Org.BouncyCastle.Asn1.Asn1Set Org.BouncyCastle.Asn1.Pkcs.SignedData::certificates
	Asn1Set_tD51325409687C84A095F7692B251BDE783A1A4A7 * ___certificates_3;
	// Org.BouncyCastle.Asn1.Asn1Set Org.BouncyCastle.Asn1.Pkcs.SignedData::crls
	Asn1Set_tD51325409687C84A095F7692B251BDE783A1A4A7 * ___crls_4;
	// Org.BouncyCastle.Asn1.Asn1Set Org.BouncyCastle.Asn1.Pkcs.SignedData::signerInfos
	Asn1Set_tD51325409687C84A095F7692B251BDE783A1A4A7 * ___signerInfos_5;

public:
	inline static int32_t get_offset_of_version_0() { return static_cast<int32_t>(offsetof(SignedData_tC530B1ACD0C32802BB3EFE180B486D0564B30129, ___version_0)); }
	inline DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A * get_version_0() const { return ___version_0; }
	inline DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A ** get_address_of_version_0() { return &___version_0; }
	inline void set_version_0(DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A * value)
	{
		___version_0 = value;
		Il2CppCodeGenWriteBarrier((&___version_0), value);
	}

	inline static int32_t get_offset_of_digestAlgorithms_1() { return static_cast<int32_t>(offsetof(SignedData_tC530B1ACD0C32802BB3EFE180B486D0564B30129, ___digestAlgorithms_1)); }
	inline Asn1Set_tD51325409687C84A095F7692B251BDE783A1A4A7 * get_digestAlgorithms_1() const { return ___digestAlgorithms_1; }
	inline Asn1Set_tD51325409687C84A095F7692B251BDE783A1A4A7 ** get_address_of_digestAlgorithms_1() { return &___digestAlgorithms_1; }
	inline void set_digestAlgorithms_1(Asn1Set_tD51325409687C84A095F7692B251BDE783A1A4A7 * value)
	{
		___digestAlgorithms_1 = value;
		Il2CppCodeGenWriteBarrier((&___digestAlgorithms_1), value);
	}

	inline static int32_t get_offset_of_contentInfo_2() { return static_cast<int32_t>(offsetof(SignedData_tC530B1ACD0C32802BB3EFE180B486D0564B30129, ___contentInfo_2)); }
	inline ContentInfo_t97148D828054F7B5E1586B21837C138F9EDE4C08 * get_contentInfo_2() const { return ___contentInfo_2; }
	inline ContentInfo_t97148D828054F7B5E1586B21837C138F9EDE4C08 ** get_address_of_contentInfo_2() { return &___contentInfo_2; }
	inline void set_contentInfo_2(ContentInfo_t97148D828054F7B5E1586B21837C138F9EDE4C08 * value)
	{
		___contentInfo_2 = value;
		Il2CppCodeGenWriteBarrier((&___contentInfo_2), value);
	}

	inline static int32_t get_offset_of_certificates_3() { return static_cast<int32_t>(offsetof(SignedData_tC530B1ACD0C32802BB3EFE180B486D0564B30129, ___certificates_3)); }
	inline Asn1Set_tD51325409687C84A095F7692B251BDE783A1A4A7 * get_certificates_3() const { return ___certificates_3; }
	inline Asn1Set_tD51325409687C84A095F7692B251BDE783A1A4A7 ** get_address_of_certificates_3() { return &___certificates_3; }
	inline void set_certificates_3(Asn1Set_tD51325409687C84A095F7692B251BDE783A1A4A7 * value)
	{
		___certificates_3 = value;
		Il2CppCodeGenWriteBarrier((&___certificates_3), value);
	}

	inline static int32_t get_offset_of_crls_4() { return static_cast<int32_t>(offsetof(SignedData_tC530B1ACD0C32802BB3EFE180B486D0564B30129, ___crls_4)); }
	inline Asn1Set_tD51325409687C84A095F7692B251BDE783A1A4A7 * get_crls_4() const { return ___crls_4; }
	inline Asn1Set_tD51325409687C84A095F7692B251BDE783A1A4A7 ** get_address_of_crls_4() { return &___crls_4; }
	inline void set_crls_4(Asn1Set_tD51325409687C84A095F7692B251BDE783A1A4A7 * value)
	{
		___crls_4 = value;
		Il2CppCodeGenWriteBarrier((&___crls_4), value);
	}

	inline static int32_t get_offset_of_signerInfos_5() { return static_cast<int32_t>(offsetof(SignedData_tC530B1ACD0C32802BB3EFE180B486D0564B30129, ___signerInfos_5)); }
	inline Asn1Set_tD51325409687C84A095F7692B251BDE783A1A4A7 * get_signerInfos_5() const { return ___signerInfos_5; }
	inline Asn1Set_tD51325409687C84A095F7692B251BDE783A1A4A7 ** get_address_of_signerInfos_5() { return &___signerInfos_5; }
	inline void set_signerInfos_5(Asn1Set_tD51325409687C84A095F7692B251BDE783A1A4A7 * value)
	{
		___signerInfos_5 = value;
		Il2CppCodeGenWriteBarrier((&___signerInfos_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGNEDDATA_TC530B1ACD0C32802BB3EFE180B486D0564B30129_H
#ifndef SECP112R1HOLDER_TC54B4FC67BFD8E33467AE24AD87AA2A10B23364E_H
#define SECP112R1HOLDER_TC54B4FC67BFD8E33467AE24AD87AA2A10B23364E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Secp112r1Holder
struct  Secp112r1Holder_tC54B4FC67BFD8E33467AE24AD87AA2A10B23364E  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct Secp112r1Holder_tC54B4FC67BFD8E33467AE24AD87AA2A10B23364E_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Secp112r1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Secp112r1Holder_tC54B4FC67BFD8E33467AE24AD87AA2A10B23364E_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP112R1HOLDER_TC54B4FC67BFD8E33467AE24AD87AA2A10B23364E_H
#ifndef SECP112R2HOLDER_T4C7868E8D8131BC693C6C66E71A0B1DCFB506995_H
#define SECP112R2HOLDER_T4C7868E8D8131BC693C6C66E71A0B1DCFB506995_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Secp112r2Holder
struct  Secp112r2Holder_t4C7868E8D8131BC693C6C66E71A0B1DCFB506995  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct Secp112r2Holder_t4C7868E8D8131BC693C6C66E71A0B1DCFB506995_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Secp112r2Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Secp112r2Holder_t4C7868E8D8131BC693C6C66E71A0B1DCFB506995_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP112R2HOLDER_T4C7868E8D8131BC693C6C66E71A0B1DCFB506995_H
#ifndef SECP128R1HOLDER_T65B90262D1B9E503E102526BF86F7D7D0F16D83C_H
#define SECP128R1HOLDER_T65B90262D1B9E503E102526BF86F7D7D0F16D83C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Secp128r1Holder
struct  Secp128r1Holder_t65B90262D1B9E503E102526BF86F7D7D0F16D83C  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct Secp128r1Holder_t65B90262D1B9E503E102526BF86F7D7D0F16D83C_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Secp128r1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Secp128r1Holder_t65B90262D1B9E503E102526BF86F7D7D0F16D83C_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP128R1HOLDER_T65B90262D1B9E503E102526BF86F7D7D0F16D83C_H
#ifndef SECP128R2HOLDER_T78AC4293F68BED8EBB3BBF27AC237E6A8C7DEC33_H
#define SECP128R2HOLDER_T78AC4293F68BED8EBB3BBF27AC237E6A8C7DEC33_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Secp128r2Holder
struct  Secp128r2Holder_t78AC4293F68BED8EBB3BBF27AC237E6A8C7DEC33  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct Secp128r2Holder_t78AC4293F68BED8EBB3BBF27AC237E6A8C7DEC33_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Secp128r2Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Secp128r2Holder_t78AC4293F68BED8EBB3BBF27AC237E6A8C7DEC33_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP128R2HOLDER_T78AC4293F68BED8EBB3BBF27AC237E6A8C7DEC33_H
#ifndef SECP160K1HOLDER_T0B6014424F493DAE1FC6EE5EACFA999DC681AD7C_H
#define SECP160K1HOLDER_T0B6014424F493DAE1FC6EE5EACFA999DC681AD7C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Secp160k1Holder
struct  Secp160k1Holder_t0B6014424F493DAE1FC6EE5EACFA999DC681AD7C  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct Secp160k1Holder_t0B6014424F493DAE1FC6EE5EACFA999DC681AD7C_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Secp160k1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Secp160k1Holder_t0B6014424F493DAE1FC6EE5EACFA999DC681AD7C_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP160K1HOLDER_T0B6014424F493DAE1FC6EE5EACFA999DC681AD7C_H
#ifndef SECP160R1HOLDER_T55D82C82E8F55749FF82BC9BEC6D0399586EE3E5_H
#define SECP160R1HOLDER_T55D82C82E8F55749FF82BC9BEC6D0399586EE3E5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Secp160r1Holder
struct  Secp160r1Holder_t55D82C82E8F55749FF82BC9BEC6D0399586EE3E5  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct Secp160r1Holder_t55D82C82E8F55749FF82BC9BEC6D0399586EE3E5_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Secp160r1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Secp160r1Holder_t55D82C82E8F55749FF82BC9BEC6D0399586EE3E5_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP160R1HOLDER_T55D82C82E8F55749FF82BC9BEC6D0399586EE3E5_H
#ifndef SECP160R2HOLDER_T116ACDAC724FA69EAFF45AA9269854FA1297F55F_H
#define SECP160R2HOLDER_T116ACDAC724FA69EAFF45AA9269854FA1297F55F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Secp160r2Holder
struct  Secp160r2Holder_t116ACDAC724FA69EAFF45AA9269854FA1297F55F  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct Secp160r2Holder_t116ACDAC724FA69EAFF45AA9269854FA1297F55F_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Secp160r2Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Secp160r2Holder_t116ACDAC724FA69EAFF45AA9269854FA1297F55F_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP160R2HOLDER_T116ACDAC724FA69EAFF45AA9269854FA1297F55F_H
#ifndef SECP192K1HOLDER_T0A75771288CF0EAE385C36B9C2076CD3672B4D25_H
#define SECP192K1HOLDER_T0A75771288CF0EAE385C36B9C2076CD3672B4D25_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Secp192k1Holder
struct  Secp192k1Holder_t0A75771288CF0EAE385C36B9C2076CD3672B4D25  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct Secp192k1Holder_t0A75771288CF0EAE385C36B9C2076CD3672B4D25_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Secp192k1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Secp192k1Holder_t0A75771288CF0EAE385C36B9C2076CD3672B4D25_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP192K1HOLDER_T0A75771288CF0EAE385C36B9C2076CD3672B4D25_H
#ifndef SECP192R1HOLDER_T420112C8CEE5161975553EE6F42A3831F36E042B_H
#define SECP192R1HOLDER_T420112C8CEE5161975553EE6F42A3831F36E042B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Secp192r1Holder
struct  Secp192r1Holder_t420112C8CEE5161975553EE6F42A3831F36E042B  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct Secp192r1Holder_t420112C8CEE5161975553EE6F42A3831F36E042B_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Secp192r1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Secp192r1Holder_t420112C8CEE5161975553EE6F42A3831F36E042B_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP192R1HOLDER_T420112C8CEE5161975553EE6F42A3831F36E042B_H
#ifndef SECP224K1HOLDER_T60397E0A6FDB5CC54E6A9004BE41522F7961A42F_H
#define SECP224K1HOLDER_T60397E0A6FDB5CC54E6A9004BE41522F7961A42F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Secp224k1Holder
struct  Secp224k1Holder_t60397E0A6FDB5CC54E6A9004BE41522F7961A42F  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct Secp224k1Holder_t60397E0A6FDB5CC54E6A9004BE41522F7961A42F_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Secp224k1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Secp224k1Holder_t60397E0A6FDB5CC54E6A9004BE41522F7961A42F_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP224K1HOLDER_T60397E0A6FDB5CC54E6A9004BE41522F7961A42F_H
#ifndef SECP224R1HOLDER_T500592A02B46F69885B3C68F403EDC8D768E2D78_H
#define SECP224R1HOLDER_T500592A02B46F69885B3C68F403EDC8D768E2D78_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Secp224r1Holder
struct  Secp224r1Holder_t500592A02B46F69885B3C68F403EDC8D768E2D78  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct Secp224r1Holder_t500592A02B46F69885B3C68F403EDC8D768E2D78_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Secp224r1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Secp224r1Holder_t500592A02B46F69885B3C68F403EDC8D768E2D78_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP224R1HOLDER_T500592A02B46F69885B3C68F403EDC8D768E2D78_H
#ifndef SECP256K1HOLDER_T8FD3B27002A5E01009A0C96C366740CC7E26EAC7_H
#define SECP256K1HOLDER_T8FD3B27002A5E01009A0C96C366740CC7E26EAC7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Secp256k1Holder
struct  Secp256k1Holder_t8FD3B27002A5E01009A0C96C366740CC7E26EAC7  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct Secp256k1Holder_t8FD3B27002A5E01009A0C96C366740CC7E26EAC7_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Secp256k1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Secp256k1Holder_t8FD3B27002A5E01009A0C96C366740CC7E26EAC7_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP256K1HOLDER_T8FD3B27002A5E01009A0C96C366740CC7E26EAC7_H
#ifndef SECP256R1HOLDER_T653A01D606FF539C6AD6DCC88FE1AF1235271F01_H
#define SECP256R1HOLDER_T653A01D606FF539C6AD6DCC88FE1AF1235271F01_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Secp256r1Holder
struct  Secp256r1Holder_t653A01D606FF539C6AD6DCC88FE1AF1235271F01  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct Secp256r1Holder_t653A01D606FF539C6AD6DCC88FE1AF1235271F01_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Secp256r1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Secp256r1Holder_t653A01D606FF539C6AD6DCC88FE1AF1235271F01_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP256R1HOLDER_T653A01D606FF539C6AD6DCC88FE1AF1235271F01_H
#ifndef SECP384R1HOLDER_T32CBCC2586FB975E9284C8BE25154CFEECF77802_H
#define SECP384R1HOLDER_T32CBCC2586FB975E9284C8BE25154CFEECF77802_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Secp384r1Holder
struct  Secp384r1Holder_t32CBCC2586FB975E9284C8BE25154CFEECF77802  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct Secp384r1Holder_t32CBCC2586FB975E9284C8BE25154CFEECF77802_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Secp384r1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Secp384r1Holder_t32CBCC2586FB975E9284C8BE25154CFEECF77802_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP384R1HOLDER_T32CBCC2586FB975E9284C8BE25154CFEECF77802_H
#ifndef SECP521R1HOLDER_T14968318336105E30C47D25B899013C684CB14C3_H
#define SECP521R1HOLDER_T14968318336105E30C47D25B899013C684CB14C3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Secp521r1Holder
struct  Secp521r1Holder_t14968318336105E30C47D25B899013C684CB14C3  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct Secp521r1Holder_t14968318336105E30C47D25B899013C684CB14C3_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Secp521r1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Secp521r1Holder_t14968318336105E30C47D25B899013C684CB14C3_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP521R1HOLDER_T14968318336105E30C47D25B899013C684CB14C3_H
#ifndef SECT113R1HOLDER_T8209BCD3276440FBE22CEA18086CD1B8EF44BA4D_H
#define SECT113R1HOLDER_T8209BCD3276440FBE22CEA18086CD1B8EF44BA4D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Sect113r1Holder
struct  Sect113r1Holder_t8209BCD3276440FBE22CEA18086CD1B8EF44BA4D  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct Sect113r1Holder_t8209BCD3276440FBE22CEA18086CD1B8EF44BA4D_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Sect113r1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Sect113r1Holder_t8209BCD3276440FBE22CEA18086CD1B8EF44BA4D_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT113R1HOLDER_T8209BCD3276440FBE22CEA18086CD1B8EF44BA4D_H
#ifndef SECT113R2HOLDER_T18F3A8ADC8D106BAC1BCD695007AF72992B413E7_H
#define SECT113R2HOLDER_T18F3A8ADC8D106BAC1BCD695007AF72992B413E7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Sect113r2Holder
struct  Sect113r2Holder_t18F3A8ADC8D106BAC1BCD695007AF72992B413E7  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct Sect113r2Holder_t18F3A8ADC8D106BAC1BCD695007AF72992B413E7_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Sect113r2Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Sect113r2Holder_t18F3A8ADC8D106BAC1BCD695007AF72992B413E7_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT113R2HOLDER_T18F3A8ADC8D106BAC1BCD695007AF72992B413E7_H
#ifndef SECT131R1HOLDER_T570219AFED5F63CA2F7B004A6CAF1AC2581C4A48_H
#define SECT131R1HOLDER_T570219AFED5F63CA2F7B004A6CAF1AC2581C4A48_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Sect131r1Holder
struct  Sect131r1Holder_t570219AFED5F63CA2F7B004A6CAF1AC2581C4A48  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct Sect131r1Holder_t570219AFED5F63CA2F7B004A6CAF1AC2581C4A48_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Sect131r1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Sect131r1Holder_t570219AFED5F63CA2F7B004A6CAF1AC2581C4A48_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT131R1HOLDER_T570219AFED5F63CA2F7B004A6CAF1AC2581C4A48_H
#ifndef SECT131R2HOLDER_TF43364C6E9F7E4D94B0E483176EA7ECC7A153D6E_H
#define SECT131R2HOLDER_TF43364C6E9F7E4D94B0E483176EA7ECC7A153D6E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Sect131r2Holder
struct  Sect131r2Holder_tF43364C6E9F7E4D94B0E483176EA7ECC7A153D6E  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct Sect131r2Holder_tF43364C6E9F7E4D94B0E483176EA7ECC7A153D6E_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Sect131r2Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Sect131r2Holder_tF43364C6E9F7E4D94B0E483176EA7ECC7A153D6E_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT131R2HOLDER_TF43364C6E9F7E4D94B0E483176EA7ECC7A153D6E_H
#ifndef SECT163K1HOLDER_T35CEEE60F75E2743BD424DE4444D264CE34F8913_H
#define SECT163K1HOLDER_T35CEEE60F75E2743BD424DE4444D264CE34F8913_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Sect163k1Holder
struct  Sect163k1Holder_t35CEEE60F75E2743BD424DE4444D264CE34F8913  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct Sect163k1Holder_t35CEEE60F75E2743BD424DE4444D264CE34F8913_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Sect163k1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Sect163k1Holder_t35CEEE60F75E2743BD424DE4444D264CE34F8913_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT163K1HOLDER_T35CEEE60F75E2743BD424DE4444D264CE34F8913_H
#ifndef SECT163R1HOLDER_T8509EAD889DB5CCBB450662E79208614E0A0A4D7_H
#define SECT163R1HOLDER_T8509EAD889DB5CCBB450662E79208614E0A0A4D7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Sect163r1Holder
struct  Sect163r1Holder_t8509EAD889DB5CCBB450662E79208614E0A0A4D7  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct Sect163r1Holder_t8509EAD889DB5CCBB450662E79208614E0A0A4D7_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Sect163r1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Sect163r1Holder_t8509EAD889DB5CCBB450662E79208614E0A0A4D7_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT163R1HOLDER_T8509EAD889DB5CCBB450662E79208614E0A0A4D7_H
#ifndef SECT163R2HOLDER_TB77257EC78B3D41C7CCA55DA97C671C11571F639_H
#define SECT163R2HOLDER_TB77257EC78B3D41C7CCA55DA97C671C11571F639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Sect163r2Holder
struct  Sect163r2Holder_tB77257EC78B3D41C7CCA55DA97C671C11571F639  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct Sect163r2Holder_tB77257EC78B3D41C7CCA55DA97C671C11571F639_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Sect163r2Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Sect163r2Holder_tB77257EC78B3D41C7CCA55DA97C671C11571F639_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT163R2HOLDER_TB77257EC78B3D41C7CCA55DA97C671C11571F639_H
#ifndef SECT193R1HOLDER_T9677FC2C8BB3CD55F3EF8EE28DDE9EEA581B6C19_H
#define SECT193R1HOLDER_T9677FC2C8BB3CD55F3EF8EE28DDE9EEA581B6C19_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Sect193r1Holder
struct  Sect193r1Holder_t9677FC2C8BB3CD55F3EF8EE28DDE9EEA581B6C19  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct Sect193r1Holder_t9677FC2C8BB3CD55F3EF8EE28DDE9EEA581B6C19_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Sect193r1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Sect193r1Holder_t9677FC2C8BB3CD55F3EF8EE28DDE9EEA581B6C19_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT193R1HOLDER_T9677FC2C8BB3CD55F3EF8EE28DDE9EEA581B6C19_H
#ifndef SECT193R2HOLDER_TA537581BA1C0F502E8078DF803078D9AF0878AEB_H
#define SECT193R2HOLDER_TA537581BA1C0F502E8078DF803078D9AF0878AEB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Sect193r2Holder
struct  Sect193r2Holder_tA537581BA1C0F502E8078DF803078D9AF0878AEB  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct Sect193r2Holder_tA537581BA1C0F502E8078DF803078D9AF0878AEB_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Sect193r2Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Sect193r2Holder_tA537581BA1C0F502E8078DF803078D9AF0878AEB_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT193R2HOLDER_TA537581BA1C0F502E8078DF803078D9AF0878AEB_H
#ifndef SECT233K1HOLDER_TEFBB7AFB624A626C4C79771CE25672FBE219E967_H
#define SECT233K1HOLDER_TEFBB7AFB624A626C4C79771CE25672FBE219E967_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Sect233k1Holder
struct  Sect233k1Holder_tEFBB7AFB624A626C4C79771CE25672FBE219E967  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct Sect233k1Holder_tEFBB7AFB624A626C4C79771CE25672FBE219E967_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Sect233k1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Sect233k1Holder_tEFBB7AFB624A626C4C79771CE25672FBE219E967_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT233K1HOLDER_TEFBB7AFB624A626C4C79771CE25672FBE219E967_H
#ifndef SECT233R1HOLDER_T2B0112E8612CA3BC9DDD876ED1823FBFF028AEE0_H
#define SECT233R1HOLDER_T2B0112E8612CA3BC9DDD876ED1823FBFF028AEE0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Sect233r1Holder
struct  Sect233r1Holder_t2B0112E8612CA3BC9DDD876ED1823FBFF028AEE0  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct Sect233r1Holder_t2B0112E8612CA3BC9DDD876ED1823FBFF028AEE0_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Sect233r1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Sect233r1Holder_t2B0112E8612CA3BC9DDD876ED1823FBFF028AEE0_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT233R1HOLDER_T2B0112E8612CA3BC9DDD876ED1823FBFF028AEE0_H
#ifndef SECT239K1HOLDER_T1089DC5B36A677DCCBCFB08AA93F32F46CFB01CA_H
#define SECT239K1HOLDER_T1089DC5B36A677DCCBCFB08AA93F32F46CFB01CA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Sect239k1Holder
struct  Sect239k1Holder_t1089DC5B36A677DCCBCFB08AA93F32F46CFB01CA  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct Sect239k1Holder_t1089DC5B36A677DCCBCFB08AA93F32F46CFB01CA_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Sect239k1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Sect239k1Holder_t1089DC5B36A677DCCBCFB08AA93F32F46CFB01CA_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT239K1HOLDER_T1089DC5B36A677DCCBCFB08AA93F32F46CFB01CA_H
#ifndef SECT283K1HOLDER_T3762D17EB2EB3377FD63D88DD3BAD73587598505_H
#define SECT283K1HOLDER_T3762D17EB2EB3377FD63D88DD3BAD73587598505_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Sect283k1Holder
struct  Sect283k1Holder_t3762D17EB2EB3377FD63D88DD3BAD73587598505  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct Sect283k1Holder_t3762D17EB2EB3377FD63D88DD3BAD73587598505_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Sect283k1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Sect283k1Holder_t3762D17EB2EB3377FD63D88DD3BAD73587598505_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT283K1HOLDER_T3762D17EB2EB3377FD63D88DD3BAD73587598505_H
#ifndef SECT283R1HOLDER_T438A2E827E312C528D1FB550484FC4F680DDA5EF_H
#define SECT283R1HOLDER_T438A2E827E312C528D1FB550484FC4F680DDA5EF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Sect283r1Holder
struct  Sect283r1Holder_t438A2E827E312C528D1FB550484FC4F680DDA5EF  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct Sect283r1Holder_t438A2E827E312C528D1FB550484FC4F680DDA5EF_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Sect283r1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Sect283r1Holder_t438A2E827E312C528D1FB550484FC4F680DDA5EF_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT283R1HOLDER_T438A2E827E312C528D1FB550484FC4F680DDA5EF_H
#ifndef SECT409K1HOLDER_T07E69564F68EDAB038993C5BC3EBDDAA171BE0DD_H
#define SECT409K1HOLDER_T07E69564F68EDAB038993C5BC3EBDDAA171BE0DD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Sect409k1Holder
struct  Sect409k1Holder_t07E69564F68EDAB038993C5BC3EBDDAA171BE0DD  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct Sect409k1Holder_t07E69564F68EDAB038993C5BC3EBDDAA171BE0DD_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Sect409k1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Sect409k1Holder_t07E69564F68EDAB038993C5BC3EBDDAA171BE0DD_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT409K1HOLDER_T07E69564F68EDAB038993C5BC3EBDDAA171BE0DD_H
#ifndef SECT409R1HOLDER_TFE35E4DBC01B34180F27768B2F455C3E0532D7F8_H
#define SECT409R1HOLDER_TFE35E4DBC01B34180F27768B2F455C3E0532D7F8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Sect409r1Holder
struct  Sect409r1Holder_tFE35E4DBC01B34180F27768B2F455C3E0532D7F8  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct Sect409r1Holder_tFE35E4DBC01B34180F27768B2F455C3E0532D7F8_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Sect409r1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Sect409r1Holder_tFE35E4DBC01B34180F27768B2F455C3E0532D7F8_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT409R1HOLDER_TFE35E4DBC01B34180F27768B2F455C3E0532D7F8_H
#ifndef SECT571K1HOLDER_TF2BC31E4E8790EFEBE510043C5590487E5797AD6_H
#define SECT571K1HOLDER_TF2BC31E4E8790EFEBE510043C5590487E5797AD6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Sect571k1Holder
struct  Sect571k1Holder_tF2BC31E4E8790EFEBE510043C5590487E5797AD6  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct Sect571k1Holder_tF2BC31E4E8790EFEBE510043C5590487E5797AD6_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Sect571k1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Sect571k1Holder_tF2BC31E4E8790EFEBE510043C5590487E5797AD6_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT571K1HOLDER_TF2BC31E4E8790EFEBE510043C5590487E5797AD6_H
#ifndef SECT571R1HOLDER_TDD00E3E0375A4B39892BDAC16ED196648AB57D21_H
#define SECT571R1HOLDER_TDD00E3E0375A4B39892BDAC16ED196648AB57D21_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Sect571r1Holder
struct  Sect571r1Holder_tDD00E3E0375A4B39892BDAC16ED196648AB57D21  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct Sect571r1Holder_tDD00E3E0375A4B39892BDAC16ED196648AB57D21_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Sect571r1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Sect571r1Holder_tDD00E3E0375A4B39892BDAC16ED196648AB57D21_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT571R1HOLDER_TDD00E3E0375A4B39892BDAC16ED196648AB57D21_H
#ifndef BRAINPOOLP160R1HOLDER_TB00765DBFC854B8CDA2D4E78972A80D3FE74F27A_H
#define BRAINPOOLP160R1HOLDER_TB00765DBFC854B8CDA2D4E78972A80D3FE74F27A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTNamedCurves_BrainpoolP160r1Holder
struct  BrainpoolP160r1Holder_tB00765DBFC854B8CDA2D4E78972A80D3FE74F27A  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct BrainpoolP160r1Holder_tB00765DBFC854B8CDA2D4E78972A80D3FE74F27A_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTNamedCurves_BrainpoolP160r1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(BrainpoolP160r1Holder_tB00765DBFC854B8CDA2D4E78972A80D3FE74F27A_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BRAINPOOLP160R1HOLDER_TB00765DBFC854B8CDA2D4E78972A80D3FE74F27A_H
#ifndef BRAINPOOLP160T1HOLDER_T9D96D581E7074E77DAD9FD9F3079EB83660EB8FC_H
#define BRAINPOOLP160T1HOLDER_T9D96D581E7074E77DAD9FD9F3079EB83660EB8FC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTNamedCurves_BrainpoolP160t1Holder
struct  BrainpoolP160t1Holder_t9D96D581E7074E77DAD9FD9F3079EB83660EB8FC  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct BrainpoolP160t1Holder_t9D96D581E7074E77DAD9FD9F3079EB83660EB8FC_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTNamedCurves_BrainpoolP160t1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(BrainpoolP160t1Holder_t9D96D581E7074E77DAD9FD9F3079EB83660EB8FC_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BRAINPOOLP160T1HOLDER_T9D96D581E7074E77DAD9FD9F3079EB83660EB8FC_H
#ifndef BRAINPOOLP192R1HOLDER_T771274FE9E2EE8CC870E49124DF103B7A8ECD577_H
#define BRAINPOOLP192R1HOLDER_T771274FE9E2EE8CC870E49124DF103B7A8ECD577_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTNamedCurves_BrainpoolP192r1Holder
struct  BrainpoolP192r1Holder_t771274FE9E2EE8CC870E49124DF103B7A8ECD577  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct BrainpoolP192r1Holder_t771274FE9E2EE8CC870E49124DF103B7A8ECD577_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTNamedCurves_BrainpoolP192r1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(BrainpoolP192r1Holder_t771274FE9E2EE8CC870E49124DF103B7A8ECD577_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BRAINPOOLP192R1HOLDER_T771274FE9E2EE8CC870E49124DF103B7A8ECD577_H
#ifndef BRAINPOOLP192T1HOLDER_T54B80D472F33C4D6A78017EC49A23913DD04B430_H
#define BRAINPOOLP192T1HOLDER_T54B80D472F33C4D6A78017EC49A23913DD04B430_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTNamedCurves_BrainpoolP192t1Holder
struct  BrainpoolP192t1Holder_t54B80D472F33C4D6A78017EC49A23913DD04B430  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct BrainpoolP192t1Holder_t54B80D472F33C4D6A78017EC49A23913DD04B430_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTNamedCurves_BrainpoolP192t1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(BrainpoolP192t1Holder_t54B80D472F33C4D6A78017EC49A23913DD04B430_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BRAINPOOLP192T1HOLDER_T54B80D472F33C4D6A78017EC49A23913DD04B430_H
#ifndef BRAINPOOLP224R1HOLDER_TE796B60E32E29D4DB30F849AC34E1E54E4C28FCC_H
#define BRAINPOOLP224R1HOLDER_TE796B60E32E29D4DB30F849AC34E1E54E4C28FCC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTNamedCurves_BrainpoolP224r1Holder
struct  BrainpoolP224r1Holder_tE796B60E32E29D4DB30F849AC34E1E54E4C28FCC  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct BrainpoolP224r1Holder_tE796B60E32E29D4DB30F849AC34E1E54E4C28FCC_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTNamedCurves_BrainpoolP224r1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(BrainpoolP224r1Holder_tE796B60E32E29D4DB30F849AC34E1E54E4C28FCC_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BRAINPOOLP224R1HOLDER_TE796B60E32E29D4DB30F849AC34E1E54E4C28FCC_H
#ifndef BRAINPOOLP224T1HOLDER_T17820B576543990213F53A8E8CB267DD52614532_H
#define BRAINPOOLP224T1HOLDER_T17820B576543990213F53A8E8CB267DD52614532_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTNamedCurves_BrainpoolP224t1Holder
struct  BrainpoolP224t1Holder_t17820B576543990213F53A8E8CB267DD52614532  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct BrainpoolP224t1Holder_t17820B576543990213F53A8E8CB267DD52614532_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTNamedCurves_BrainpoolP224t1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(BrainpoolP224t1Holder_t17820B576543990213F53A8E8CB267DD52614532_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BRAINPOOLP224T1HOLDER_T17820B576543990213F53A8E8CB267DD52614532_H
#ifndef BRAINPOOLP256R1HOLDER_TABC5A55FC3CC25BD0B521511E2D7CF930D89CB4C_H
#define BRAINPOOLP256R1HOLDER_TABC5A55FC3CC25BD0B521511E2D7CF930D89CB4C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTNamedCurves_BrainpoolP256r1Holder
struct  BrainpoolP256r1Holder_tABC5A55FC3CC25BD0B521511E2D7CF930D89CB4C  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct BrainpoolP256r1Holder_tABC5A55FC3CC25BD0B521511E2D7CF930D89CB4C_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTNamedCurves_BrainpoolP256r1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(BrainpoolP256r1Holder_tABC5A55FC3CC25BD0B521511E2D7CF930D89CB4C_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BRAINPOOLP256R1HOLDER_TABC5A55FC3CC25BD0B521511E2D7CF930D89CB4C_H
#ifndef BRAINPOOLP256T1HOLDER_T3E07DA34380DCA555D3175AA6A32C213B0B46891_H
#define BRAINPOOLP256T1HOLDER_T3E07DA34380DCA555D3175AA6A32C213B0B46891_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTNamedCurves_BrainpoolP256t1Holder
struct  BrainpoolP256t1Holder_t3E07DA34380DCA555D3175AA6A32C213B0B46891  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct BrainpoolP256t1Holder_t3E07DA34380DCA555D3175AA6A32C213B0B46891_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTNamedCurves_BrainpoolP256t1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(BrainpoolP256t1Holder_t3E07DA34380DCA555D3175AA6A32C213B0B46891_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BRAINPOOLP256T1HOLDER_T3E07DA34380DCA555D3175AA6A32C213B0B46891_H
#ifndef BRAINPOOLP320R1HOLDER_TF858083D9EC65C21F9086A5629E801934F81A82F_H
#define BRAINPOOLP320R1HOLDER_TF858083D9EC65C21F9086A5629E801934F81A82F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTNamedCurves_BrainpoolP320r1Holder
struct  BrainpoolP320r1Holder_tF858083D9EC65C21F9086A5629E801934F81A82F  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct BrainpoolP320r1Holder_tF858083D9EC65C21F9086A5629E801934F81A82F_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTNamedCurves_BrainpoolP320r1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(BrainpoolP320r1Holder_tF858083D9EC65C21F9086A5629E801934F81A82F_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BRAINPOOLP320R1HOLDER_TF858083D9EC65C21F9086A5629E801934F81A82F_H
#ifndef BRAINPOOLP320T1HOLDER_TF58B643166F97C26E85380DB6341EC778E04C772_H
#define BRAINPOOLP320T1HOLDER_TF58B643166F97C26E85380DB6341EC778E04C772_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTNamedCurves_BrainpoolP320t1Holder
struct  BrainpoolP320t1Holder_tF58B643166F97C26E85380DB6341EC778E04C772  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct BrainpoolP320t1Holder_tF58B643166F97C26E85380DB6341EC778E04C772_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTNamedCurves_BrainpoolP320t1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(BrainpoolP320t1Holder_tF58B643166F97C26E85380DB6341EC778E04C772_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BRAINPOOLP320T1HOLDER_TF58B643166F97C26E85380DB6341EC778E04C772_H
#ifndef BRAINPOOLP384R1HOLDER_TEB77B2724A14AFB2B02BBB723638144F6C9B155A_H
#define BRAINPOOLP384R1HOLDER_TEB77B2724A14AFB2B02BBB723638144F6C9B155A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTNamedCurves_BrainpoolP384r1Holder
struct  BrainpoolP384r1Holder_tEB77B2724A14AFB2B02BBB723638144F6C9B155A  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct BrainpoolP384r1Holder_tEB77B2724A14AFB2B02BBB723638144F6C9B155A_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTNamedCurves_BrainpoolP384r1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(BrainpoolP384r1Holder_tEB77B2724A14AFB2B02BBB723638144F6C9B155A_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BRAINPOOLP384R1HOLDER_TEB77B2724A14AFB2B02BBB723638144F6C9B155A_H
#ifndef BRAINPOOLP384T1HOLDER_TDDE31E2E321288ECC960740D2900555C0217668F_H
#define BRAINPOOLP384T1HOLDER_TDDE31E2E321288ECC960740D2900555C0217668F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTNamedCurves_BrainpoolP384t1Holder
struct  BrainpoolP384t1Holder_tDDE31E2E321288ECC960740D2900555C0217668F  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct BrainpoolP384t1Holder_tDDE31E2E321288ECC960740D2900555C0217668F_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTNamedCurves_BrainpoolP384t1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(BrainpoolP384t1Holder_tDDE31E2E321288ECC960740D2900555C0217668F_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BRAINPOOLP384T1HOLDER_TDDE31E2E321288ECC960740D2900555C0217668F_H
#ifndef BRAINPOOLP512R1HOLDER_T42A031BB31B58F65E2C95769DB80332D1A2B13D3_H
#define BRAINPOOLP512R1HOLDER_T42A031BB31B58F65E2C95769DB80332D1A2B13D3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTNamedCurves_BrainpoolP512r1Holder
struct  BrainpoolP512r1Holder_t42A031BB31B58F65E2C95769DB80332D1A2B13D3  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct BrainpoolP512r1Holder_t42A031BB31B58F65E2C95769DB80332D1A2B13D3_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTNamedCurves_BrainpoolP512r1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(BrainpoolP512r1Holder_t42A031BB31B58F65E2C95769DB80332D1A2B13D3_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BRAINPOOLP512R1HOLDER_T42A031BB31B58F65E2C95769DB80332D1A2B13D3_H
#ifndef BRAINPOOLP512T1HOLDER_T87E55A19950DDFA17A82E6C6F09C0BCBAED882C0_H
#define BRAINPOOLP512T1HOLDER_T87E55A19950DDFA17A82E6C6F09C0BCBAED882C0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTNamedCurves_BrainpoolP512t1Holder
struct  BrainpoolP512t1Holder_t87E55A19950DDFA17A82E6C6F09C0BCBAED882C0  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct BrainpoolP512t1Holder_t87E55A19950DDFA17A82E6C6F09C0BCBAED882C0_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTNamedCurves_BrainpoolP512t1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(BrainpoolP512t1Holder_t87E55A19950DDFA17A82E6C6F09C0BCBAED882C0_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BRAINPOOLP512T1HOLDER_T87E55A19950DDFA17A82E6C6F09C0BCBAED882C0_H
#ifndef ALGORITHMIDENTIFIER_TDE7123E83DAD8C219BBB99DAE8CAAD8A2AE64EFB_H
#define ALGORITHMIDENTIFIER_TDE7123E83DAD8C219BBB99DAE8CAAD8A2AE64EFB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier
struct  AlgorithmIdentifier_tDE7123E83DAD8C219BBB99DAE8CAAD8A2AE64EFB  : public Asn1Encodable_t390FCE82F5F52E41A7320BBF2E8BF18DA1622A8B
{
public:
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier::algorithm
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___algorithm_0;
	// Org.BouncyCastle.Asn1.Asn1Encodable Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier::parameters
	Asn1Encodable_t390FCE82F5F52E41A7320BBF2E8BF18DA1622A8B * ___parameters_1;

public:
	inline static int32_t get_offset_of_algorithm_0() { return static_cast<int32_t>(offsetof(AlgorithmIdentifier_tDE7123E83DAD8C219BBB99DAE8CAAD8A2AE64EFB, ___algorithm_0)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_algorithm_0() const { return ___algorithm_0; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_algorithm_0() { return &___algorithm_0; }
	inline void set_algorithm_0(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___algorithm_0 = value;
		Il2CppCodeGenWriteBarrier((&___algorithm_0), value);
	}

	inline static int32_t get_offset_of_parameters_1() { return static_cast<int32_t>(offsetof(AlgorithmIdentifier_tDE7123E83DAD8C219BBB99DAE8CAAD8A2AE64EFB, ___parameters_1)); }
	inline Asn1Encodable_t390FCE82F5F52E41A7320BBF2E8BF18DA1622A8B * get_parameters_1() const { return ___parameters_1; }
	inline Asn1Encodable_t390FCE82F5F52E41A7320BBF2E8BF18DA1622A8B ** get_address_of_parameters_1() { return &___parameters_1; }
	inline void set_parameters_1(Asn1Encodable_t390FCE82F5F52E41A7320BBF2E8BF18DA1622A8B * value)
	{
		___parameters_1 = value;
		Il2CppCodeGenWriteBarrier((&___parameters_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALGORITHMIDENTIFIER_TDE7123E83DAD8C219BBB99DAE8CAAD8A2AE64EFB_H
#ifndef BASICCONSTRAINTS_T199BCC0E7A6950B11A098A0422FDEB8E73E2380E_H
#define BASICCONSTRAINTS_T199BCC0E7A6950B11A098A0422FDEB8E73E2380E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.X509.BasicConstraints
struct  BasicConstraints_t199BCC0E7A6950B11A098A0422FDEB8E73E2380E  : public Asn1Encodable_t390FCE82F5F52E41A7320BBF2E8BF18DA1622A8B
{
public:
	// Org.BouncyCastle.Asn1.DerBoolean Org.BouncyCastle.Asn1.X509.BasicConstraints::cA
	DerBoolean_tAF849A79A0BCE5C3F0DBB8BE9F6BA279E3D1A8CE * ___cA_0;
	// Org.BouncyCastle.Asn1.DerInteger Org.BouncyCastle.Asn1.X509.BasicConstraints::pathLenConstraint
	DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A * ___pathLenConstraint_1;

public:
	inline static int32_t get_offset_of_cA_0() { return static_cast<int32_t>(offsetof(BasicConstraints_t199BCC0E7A6950B11A098A0422FDEB8E73E2380E, ___cA_0)); }
	inline DerBoolean_tAF849A79A0BCE5C3F0DBB8BE9F6BA279E3D1A8CE * get_cA_0() const { return ___cA_0; }
	inline DerBoolean_tAF849A79A0BCE5C3F0DBB8BE9F6BA279E3D1A8CE ** get_address_of_cA_0() { return &___cA_0; }
	inline void set_cA_0(DerBoolean_tAF849A79A0BCE5C3F0DBB8BE9F6BA279E3D1A8CE * value)
	{
		___cA_0 = value;
		Il2CppCodeGenWriteBarrier((&___cA_0), value);
	}

	inline static int32_t get_offset_of_pathLenConstraint_1() { return static_cast<int32_t>(offsetof(BasicConstraints_t199BCC0E7A6950B11A098A0422FDEB8E73E2380E, ___pathLenConstraint_1)); }
	inline DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A * get_pathLenConstraint_1() const { return ___pathLenConstraint_1; }
	inline DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A ** get_address_of_pathLenConstraint_1() { return &___pathLenConstraint_1; }
	inline void set_pathLenConstraint_1(DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A * value)
	{
		___pathLenConstraint_1 = value;
		Il2CppCodeGenWriteBarrier((&___pathLenConstraint_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICCONSTRAINTS_T199BCC0E7A6950B11A098A0422FDEB8E73E2380E_H
#ifndef DIGESTINFO_T12BB846B9E621A6FF47DC65A5F94EC7E734E07FA_H
#define DIGESTINFO_T12BB846B9E621A6FF47DC65A5F94EC7E734E07FA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.X509.DigestInfo
struct  DigestInfo_t12BB846B9E621A6FF47DC65A5F94EC7E734E07FA  : public Asn1Encodable_t390FCE82F5F52E41A7320BBF2E8BF18DA1622A8B
{
public:
	// System.Byte[] Org.BouncyCastle.Asn1.X509.DigestInfo::digest
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___digest_0;
	// Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier Org.BouncyCastle.Asn1.X509.DigestInfo::algID
	AlgorithmIdentifier_tDE7123E83DAD8C219BBB99DAE8CAAD8A2AE64EFB * ___algID_1;

public:
	inline static int32_t get_offset_of_digest_0() { return static_cast<int32_t>(offsetof(DigestInfo_t12BB846B9E621A6FF47DC65A5F94EC7E734E07FA, ___digest_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_digest_0() const { return ___digest_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_digest_0() { return &___digest_0; }
	inline void set_digest_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___digest_0 = value;
		Il2CppCodeGenWriteBarrier((&___digest_0), value);
	}

	inline static int32_t get_offset_of_algID_1() { return static_cast<int32_t>(offsetof(DigestInfo_t12BB846B9E621A6FF47DC65A5F94EC7E734E07FA, ___algID_1)); }
	inline AlgorithmIdentifier_tDE7123E83DAD8C219BBB99DAE8CAAD8A2AE64EFB * get_algID_1() const { return ___algID_1; }
	inline AlgorithmIdentifier_tDE7123E83DAD8C219BBB99DAE8CAAD8A2AE64EFB ** get_address_of_algID_1() { return &___algID_1; }
	inline void set_algID_1(AlgorithmIdentifier_tDE7123E83DAD8C219BBB99DAE8CAAD8A2AE64EFB * value)
	{
		___algID_1 = value;
		Il2CppCodeGenWriteBarrier((&___algID_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIGESTINFO_T12BB846B9E621A6FF47DC65A5F94EC7E734E07FA_H
#ifndef DSAPARAMETER_T292FF5A951F42D3279875CE69233BD3A39685D73_H
#define DSAPARAMETER_T292FF5A951F42D3279875CE69233BD3A39685D73_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.X509.DsaParameter
struct  DsaParameter_t292FF5A951F42D3279875CE69233BD3A39685D73  : public Asn1Encodable_t390FCE82F5F52E41A7320BBF2E8BF18DA1622A8B
{
public:
	// Org.BouncyCastle.Asn1.DerInteger Org.BouncyCastle.Asn1.X509.DsaParameter::p
	DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A * ___p_0;
	// Org.BouncyCastle.Asn1.DerInteger Org.BouncyCastle.Asn1.X509.DsaParameter::q
	DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A * ___q_1;
	// Org.BouncyCastle.Asn1.DerInteger Org.BouncyCastle.Asn1.X509.DsaParameter::g
	DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A * ___g_2;

public:
	inline static int32_t get_offset_of_p_0() { return static_cast<int32_t>(offsetof(DsaParameter_t292FF5A951F42D3279875CE69233BD3A39685D73, ___p_0)); }
	inline DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A * get_p_0() const { return ___p_0; }
	inline DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A ** get_address_of_p_0() { return &___p_0; }
	inline void set_p_0(DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A * value)
	{
		___p_0 = value;
		Il2CppCodeGenWriteBarrier((&___p_0), value);
	}

	inline static int32_t get_offset_of_q_1() { return static_cast<int32_t>(offsetof(DsaParameter_t292FF5A951F42D3279875CE69233BD3A39685D73, ___q_1)); }
	inline DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A * get_q_1() const { return ___q_1; }
	inline DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A ** get_address_of_q_1() { return &___q_1; }
	inline void set_q_1(DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A * value)
	{
		___q_1 = value;
		Il2CppCodeGenWriteBarrier((&___q_1), value);
	}

	inline static int32_t get_offset_of_g_2() { return static_cast<int32_t>(offsetof(DsaParameter_t292FF5A951F42D3279875CE69233BD3A39685D73, ___g_2)); }
	inline DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A * get_g_2() const { return ___g_2; }
	inline DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A ** get_address_of_g_2() { return &___g_2; }
	inline void set_g_2(DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A * value)
	{
		___g_2 = value;
		Il2CppCodeGenWriteBarrier((&___g_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DSAPARAMETER_T292FF5A951F42D3279875CE69233BD3A39685D73_H
#ifndef RSAPUBLICKEYSTRUCTURE_T6F015FCA225AF3E16C7C545F0A1B0DB7FAF86567_H
#define RSAPUBLICKEYSTRUCTURE_T6F015FCA225AF3E16C7C545F0A1B0DB7FAF86567_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.X509.RsaPublicKeyStructure
struct  RsaPublicKeyStructure_t6F015FCA225AF3E16C7C545F0A1B0DB7FAF86567  : public Asn1Encodable_t390FCE82F5F52E41A7320BBF2E8BF18DA1622A8B
{
public:
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Asn1.X509.RsaPublicKeyStructure::modulus
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___modulus_0;
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Asn1.X509.RsaPublicKeyStructure::publicExponent
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___publicExponent_1;

public:
	inline static int32_t get_offset_of_modulus_0() { return static_cast<int32_t>(offsetof(RsaPublicKeyStructure_t6F015FCA225AF3E16C7C545F0A1B0DB7FAF86567, ___modulus_0)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_modulus_0() const { return ___modulus_0; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_modulus_0() { return &___modulus_0; }
	inline void set_modulus_0(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___modulus_0 = value;
		Il2CppCodeGenWriteBarrier((&___modulus_0), value);
	}

	inline static int32_t get_offset_of_publicExponent_1() { return static_cast<int32_t>(offsetof(RsaPublicKeyStructure_t6F015FCA225AF3E16C7C545F0A1B0DB7FAF86567, ___publicExponent_1)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_publicExponent_1() const { return ___publicExponent_1; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_publicExponent_1() { return &___publicExponent_1; }
	inline void set_publicExponent_1(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___publicExponent_1 = value;
		Il2CppCodeGenWriteBarrier((&___publicExponent_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RSAPUBLICKEYSTRUCTURE_T6F015FCA225AF3E16C7C545F0A1B0DB7FAF86567_H
#ifndef SUBJECTPUBLICKEYINFO_TE61D9687E07635F3A249A3FBBBE6E2D9C4B73A7C_H
#define SUBJECTPUBLICKEYINFO_TE61D9687E07635F3A249A3FBBBE6E2D9C4B73A7C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.X509.SubjectPublicKeyInfo
struct  SubjectPublicKeyInfo_tE61D9687E07635F3A249A3FBBBE6E2D9C4B73A7C  : public Asn1Encodable_t390FCE82F5F52E41A7320BBF2E8BF18DA1622A8B
{
public:
	// Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier Org.BouncyCastle.Asn1.X509.SubjectPublicKeyInfo::algID
	AlgorithmIdentifier_tDE7123E83DAD8C219BBB99DAE8CAAD8A2AE64EFB * ___algID_0;
	// Org.BouncyCastle.Asn1.DerBitString Org.BouncyCastle.Asn1.X509.SubjectPublicKeyInfo::keyData
	DerBitString_tCA371BCECC9BA7643D0127424F54EE10C1B19A64 * ___keyData_1;

public:
	inline static int32_t get_offset_of_algID_0() { return static_cast<int32_t>(offsetof(SubjectPublicKeyInfo_tE61D9687E07635F3A249A3FBBBE6E2D9C4B73A7C, ___algID_0)); }
	inline AlgorithmIdentifier_tDE7123E83DAD8C219BBB99DAE8CAAD8A2AE64EFB * get_algID_0() const { return ___algID_0; }
	inline AlgorithmIdentifier_tDE7123E83DAD8C219BBB99DAE8CAAD8A2AE64EFB ** get_address_of_algID_0() { return &___algID_0; }
	inline void set_algID_0(AlgorithmIdentifier_tDE7123E83DAD8C219BBB99DAE8CAAD8A2AE64EFB * value)
	{
		___algID_0 = value;
		Il2CppCodeGenWriteBarrier((&___algID_0), value);
	}

	inline static int32_t get_offset_of_keyData_1() { return static_cast<int32_t>(offsetof(SubjectPublicKeyInfo_tE61D9687E07635F3A249A3FBBBE6E2D9C4B73A7C, ___keyData_1)); }
	inline DerBitString_tCA371BCECC9BA7643D0127424F54EE10C1B19A64 * get_keyData_1() const { return ___keyData_1; }
	inline DerBitString_tCA371BCECC9BA7643D0127424F54EE10C1B19A64 ** get_address_of_keyData_1() { return &___keyData_1; }
	inline void set_keyData_1(DerBitString_tCA371BCECC9BA7643D0127424F54EE10C1B19A64 * value)
	{
		___keyData_1 = value;
		Il2CppCodeGenWriteBarrier((&___keyData_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBJECTPUBLICKEYINFO_TE61D9687E07635F3A249A3FBBBE6E2D9C4B73A7C_H
#ifndef TBSCERTIFICATESTRUCTURE_TDA73857A74B4B8375807FCBE2099FDFFA361D598_H
#define TBSCERTIFICATESTRUCTURE_TDA73857A74B4B8375807FCBE2099FDFFA361D598_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.X509.TbsCertificateStructure
struct  TbsCertificateStructure_tDA73857A74B4B8375807FCBE2099FDFFA361D598  : public Asn1Encodable_t390FCE82F5F52E41A7320BBF2E8BF18DA1622A8B
{
public:
	// Org.BouncyCastle.Asn1.Asn1Sequence Org.BouncyCastle.Asn1.X509.TbsCertificateStructure::seq
	Asn1Sequence_t292AE1C416511A0DE153DE285AEE498B50523B85 * ___seq_0;
	// Org.BouncyCastle.Asn1.DerInteger Org.BouncyCastle.Asn1.X509.TbsCertificateStructure::version
	DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A * ___version_1;
	// Org.BouncyCastle.Asn1.DerInteger Org.BouncyCastle.Asn1.X509.TbsCertificateStructure::serialNumber
	DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A * ___serialNumber_2;
	// Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier Org.BouncyCastle.Asn1.X509.TbsCertificateStructure::signature
	AlgorithmIdentifier_tDE7123E83DAD8C219BBB99DAE8CAAD8A2AE64EFB * ___signature_3;
	// Org.BouncyCastle.Asn1.X509.X509Name Org.BouncyCastle.Asn1.X509.TbsCertificateStructure::issuer
	X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E * ___issuer_4;
	// Org.BouncyCastle.Asn1.X509.Time Org.BouncyCastle.Asn1.X509.TbsCertificateStructure::startDate
	Time_t80739D7454AE536E0F9A1C9D5B0620C3C8378307 * ___startDate_5;
	// Org.BouncyCastle.Asn1.X509.Time Org.BouncyCastle.Asn1.X509.TbsCertificateStructure::endDate
	Time_t80739D7454AE536E0F9A1C9D5B0620C3C8378307 * ___endDate_6;
	// Org.BouncyCastle.Asn1.X509.X509Name Org.BouncyCastle.Asn1.X509.TbsCertificateStructure::subject
	X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E * ___subject_7;
	// Org.BouncyCastle.Asn1.X509.SubjectPublicKeyInfo Org.BouncyCastle.Asn1.X509.TbsCertificateStructure::subjectPublicKeyInfo
	SubjectPublicKeyInfo_tE61D9687E07635F3A249A3FBBBE6E2D9C4B73A7C * ___subjectPublicKeyInfo_8;
	// Org.BouncyCastle.Asn1.DerBitString Org.BouncyCastle.Asn1.X509.TbsCertificateStructure::issuerUniqueID
	DerBitString_tCA371BCECC9BA7643D0127424F54EE10C1B19A64 * ___issuerUniqueID_9;
	// Org.BouncyCastle.Asn1.DerBitString Org.BouncyCastle.Asn1.X509.TbsCertificateStructure::subjectUniqueID
	DerBitString_tCA371BCECC9BA7643D0127424F54EE10C1B19A64 * ___subjectUniqueID_10;
	// Org.BouncyCastle.Asn1.X509.X509Extensions Org.BouncyCastle.Asn1.X509.TbsCertificateStructure::extensions
	X509Extensions_tBC4870829033B1A0E0F297F32C0B2B29C1618E44 * ___extensions_11;

public:
	inline static int32_t get_offset_of_seq_0() { return static_cast<int32_t>(offsetof(TbsCertificateStructure_tDA73857A74B4B8375807FCBE2099FDFFA361D598, ___seq_0)); }
	inline Asn1Sequence_t292AE1C416511A0DE153DE285AEE498B50523B85 * get_seq_0() const { return ___seq_0; }
	inline Asn1Sequence_t292AE1C416511A0DE153DE285AEE498B50523B85 ** get_address_of_seq_0() { return &___seq_0; }
	inline void set_seq_0(Asn1Sequence_t292AE1C416511A0DE153DE285AEE498B50523B85 * value)
	{
		___seq_0 = value;
		Il2CppCodeGenWriteBarrier((&___seq_0), value);
	}

	inline static int32_t get_offset_of_version_1() { return static_cast<int32_t>(offsetof(TbsCertificateStructure_tDA73857A74B4B8375807FCBE2099FDFFA361D598, ___version_1)); }
	inline DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A * get_version_1() const { return ___version_1; }
	inline DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A ** get_address_of_version_1() { return &___version_1; }
	inline void set_version_1(DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A * value)
	{
		___version_1 = value;
		Il2CppCodeGenWriteBarrier((&___version_1), value);
	}

	inline static int32_t get_offset_of_serialNumber_2() { return static_cast<int32_t>(offsetof(TbsCertificateStructure_tDA73857A74B4B8375807FCBE2099FDFFA361D598, ___serialNumber_2)); }
	inline DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A * get_serialNumber_2() const { return ___serialNumber_2; }
	inline DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A ** get_address_of_serialNumber_2() { return &___serialNumber_2; }
	inline void set_serialNumber_2(DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A * value)
	{
		___serialNumber_2 = value;
		Il2CppCodeGenWriteBarrier((&___serialNumber_2), value);
	}

	inline static int32_t get_offset_of_signature_3() { return static_cast<int32_t>(offsetof(TbsCertificateStructure_tDA73857A74B4B8375807FCBE2099FDFFA361D598, ___signature_3)); }
	inline AlgorithmIdentifier_tDE7123E83DAD8C219BBB99DAE8CAAD8A2AE64EFB * get_signature_3() const { return ___signature_3; }
	inline AlgorithmIdentifier_tDE7123E83DAD8C219BBB99DAE8CAAD8A2AE64EFB ** get_address_of_signature_3() { return &___signature_3; }
	inline void set_signature_3(AlgorithmIdentifier_tDE7123E83DAD8C219BBB99DAE8CAAD8A2AE64EFB * value)
	{
		___signature_3 = value;
		Il2CppCodeGenWriteBarrier((&___signature_3), value);
	}

	inline static int32_t get_offset_of_issuer_4() { return static_cast<int32_t>(offsetof(TbsCertificateStructure_tDA73857A74B4B8375807FCBE2099FDFFA361D598, ___issuer_4)); }
	inline X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E * get_issuer_4() const { return ___issuer_4; }
	inline X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E ** get_address_of_issuer_4() { return &___issuer_4; }
	inline void set_issuer_4(X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E * value)
	{
		___issuer_4 = value;
		Il2CppCodeGenWriteBarrier((&___issuer_4), value);
	}

	inline static int32_t get_offset_of_startDate_5() { return static_cast<int32_t>(offsetof(TbsCertificateStructure_tDA73857A74B4B8375807FCBE2099FDFFA361D598, ___startDate_5)); }
	inline Time_t80739D7454AE536E0F9A1C9D5B0620C3C8378307 * get_startDate_5() const { return ___startDate_5; }
	inline Time_t80739D7454AE536E0F9A1C9D5B0620C3C8378307 ** get_address_of_startDate_5() { return &___startDate_5; }
	inline void set_startDate_5(Time_t80739D7454AE536E0F9A1C9D5B0620C3C8378307 * value)
	{
		___startDate_5 = value;
		Il2CppCodeGenWriteBarrier((&___startDate_5), value);
	}

	inline static int32_t get_offset_of_endDate_6() { return static_cast<int32_t>(offsetof(TbsCertificateStructure_tDA73857A74B4B8375807FCBE2099FDFFA361D598, ___endDate_6)); }
	inline Time_t80739D7454AE536E0F9A1C9D5B0620C3C8378307 * get_endDate_6() const { return ___endDate_6; }
	inline Time_t80739D7454AE536E0F9A1C9D5B0620C3C8378307 ** get_address_of_endDate_6() { return &___endDate_6; }
	inline void set_endDate_6(Time_t80739D7454AE536E0F9A1C9D5B0620C3C8378307 * value)
	{
		___endDate_6 = value;
		Il2CppCodeGenWriteBarrier((&___endDate_6), value);
	}

	inline static int32_t get_offset_of_subject_7() { return static_cast<int32_t>(offsetof(TbsCertificateStructure_tDA73857A74B4B8375807FCBE2099FDFFA361D598, ___subject_7)); }
	inline X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E * get_subject_7() const { return ___subject_7; }
	inline X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E ** get_address_of_subject_7() { return &___subject_7; }
	inline void set_subject_7(X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E * value)
	{
		___subject_7 = value;
		Il2CppCodeGenWriteBarrier((&___subject_7), value);
	}

	inline static int32_t get_offset_of_subjectPublicKeyInfo_8() { return static_cast<int32_t>(offsetof(TbsCertificateStructure_tDA73857A74B4B8375807FCBE2099FDFFA361D598, ___subjectPublicKeyInfo_8)); }
	inline SubjectPublicKeyInfo_tE61D9687E07635F3A249A3FBBBE6E2D9C4B73A7C * get_subjectPublicKeyInfo_8() const { return ___subjectPublicKeyInfo_8; }
	inline SubjectPublicKeyInfo_tE61D9687E07635F3A249A3FBBBE6E2D9C4B73A7C ** get_address_of_subjectPublicKeyInfo_8() { return &___subjectPublicKeyInfo_8; }
	inline void set_subjectPublicKeyInfo_8(SubjectPublicKeyInfo_tE61D9687E07635F3A249A3FBBBE6E2D9C4B73A7C * value)
	{
		___subjectPublicKeyInfo_8 = value;
		Il2CppCodeGenWriteBarrier((&___subjectPublicKeyInfo_8), value);
	}

	inline static int32_t get_offset_of_issuerUniqueID_9() { return static_cast<int32_t>(offsetof(TbsCertificateStructure_tDA73857A74B4B8375807FCBE2099FDFFA361D598, ___issuerUniqueID_9)); }
	inline DerBitString_tCA371BCECC9BA7643D0127424F54EE10C1B19A64 * get_issuerUniqueID_9() const { return ___issuerUniqueID_9; }
	inline DerBitString_tCA371BCECC9BA7643D0127424F54EE10C1B19A64 ** get_address_of_issuerUniqueID_9() { return &___issuerUniqueID_9; }
	inline void set_issuerUniqueID_9(DerBitString_tCA371BCECC9BA7643D0127424F54EE10C1B19A64 * value)
	{
		___issuerUniqueID_9 = value;
		Il2CppCodeGenWriteBarrier((&___issuerUniqueID_9), value);
	}

	inline static int32_t get_offset_of_subjectUniqueID_10() { return static_cast<int32_t>(offsetof(TbsCertificateStructure_tDA73857A74B4B8375807FCBE2099FDFFA361D598, ___subjectUniqueID_10)); }
	inline DerBitString_tCA371BCECC9BA7643D0127424F54EE10C1B19A64 * get_subjectUniqueID_10() const { return ___subjectUniqueID_10; }
	inline DerBitString_tCA371BCECC9BA7643D0127424F54EE10C1B19A64 ** get_address_of_subjectUniqueID_10() { return &___subjectUniqueID_10; }
	inline void set_subjectUniqueID_10(DerBitString_tCA371BCECC9BA7643D0127424F54EE10C1B19A64 * value)
	{
		___subjectUniqueID_10 = value;
		Il2CppCodeGenWriteBarrier((&___subjectUniqueID_10), value);
	}

	inline static int32_t get_offset_of_extensions_11() { return static_cast<int32_t>(offsetof(TbsCertificateStructure_tDA73857A74B4B8375807FCBE2099FDFFA361D598, ___extensions_11)); }
	inline X509Extensions_tBC4870829033B1A0E0F297F32C0B2B29C1618E44 * get_extensions_11() const { return ___extensions_11; }
	inline X509Extensions_tBC4870829033B1A0E0F297F32C0B2B29C1618E44 ** get_address_of_extensions_11() { return &___extensions_11; }
	inline void set_extensions_11(X509Extensions_tBC4870829033B1A0E0F297F32C0B2B29C1618E44 * value)
	{
		___extensions_11 = value;
		Il2CppCodeGenWriteBarrier((&___extensions_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TBSCERTIFICATESTRUCTURE_TDA73857A74B4B8375807FCBE2099FDFFA361D598_H
#ifndef TIME_T80739D7454AE536E0F9A1C9D5B0620C3C8378307_H
#define TIME_T80739D7454AE536E0F9A1C9D5B0620C3C8378307_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.X509.Time
struct  Time_t80739D7454AE536E0F9A1C9D5B0620C3C8378307  : public Asn1Encodable_t390FCE82F5F52E41A7320BBF2E8BF18DA1622A8B
{
public:
	// Org.BouncyCastle.Asn1.Asn1Object Org.BouncyCastle.Asn1.X509.Time::time
	Asn1Object_tB780E50883209D5C975E83B3D16257DFC3CCE36D * ___time_0;

public:
	inline static int32_t get_offset_of_time_0() { return static_cast<int32_t>(offsetof(Time_t80739D7454AE536E0F9A1C9D5B0620C3C8378307, ___time_0)); }
	inline Asn1Object_tB780E50883209D5C975E83B3D16257DFC3CCE36D * get_time_0() const { return ___time_0; }
	inline Asn1Object_tB780E50883209D5C975E83B3D16257DFC3CCE36D ** get_address_of_time_0() { return &___time_0; }
	inline void set_time_0(Asn1Object_tB780E50883209D5C975E83B3D16257DFC3CCE36D * value)
	{
		___time_0 = value;
		Il2CppCodeGenWriteBarrier((&___time_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIME_T80739D7454AE536E0F9A1C9D5B0620C3C8378307_H
#ifndef X509CERTIFICATESTRUCTURE_TEF05F4D8F02C2BC366AC06A3E2BBDB4C35D54FE1_H
#define X509CERTIFICATESTRUCTURE_TEF05F4D8F02C2BC366AC06A3E2BBDB4C35D54FE1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.X509.X509CertificateStructure
struct  X509CertificateStructure_tEF05F4D8F02C2BC366AC06A3E2BBDB4C35D54FE1  : public Asn1Encodable_t390FCE82F5F52E41A7320BBF2E8BF18DA1622A8B
{
public:
	// Org.BouncyCastle.Asn1.X509.TbsCertificateStructure Org.BouncyCastle.Asn1.X509.X509CertificateStructure::tbsCert
	TbsCertificateStructure_tDA73857A74B4B8375807FCBE2099FDFFA361D598 * ___tbsCert_0;
	// Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier Org.BouncyCastle.Asn1.X509.X509CertificateStructure::sigAlgID
	AlgorithmIdentifier_tDE7123E83DAD8C219BBB99DAE8CAAD8A2AE64EFB * ___sigAlgID_1;
	// Org.BouncyCastle.Asn1.DerBitString Org.BouncyCastle.Asn1.X509.X509CertificateStructure::sig
	DerBitString_tCA371BCECC9BA7643D0127424F54EE10C1B19A64 * ___sig_2;

public:
	inline static int32_t get_offset_of_tbsCert_0() { return static_cast<int32_t>(offsetof(X509CertificateStructure_tEF05F4D8F02C2BC366AC06A3E2BBDB4C35D54FE1, ___tbsCert_0)); }
	inline TbsCertificateStructure_tDA73857A74B4B8375807FCBE2099FDFFA361D598 * get_tbsCert_0() const { return ___tbsCert_0; }
	inline TbsCertificateStructure_tDA73857A74B4B8375807FCBE2099FDFFA361D598 ** get_address_of_tbsCert_0() { return &___tbsCert_0; }
	inline void set_tbsCert_0(TbsCertificateStructure_tDA73857A74B4B8375807FCBE2099FDFFA361D598 * value)
	{
		___tbsCert_0 = value;
		Il2CppCodeGenWriteBarrier((&___tbsCert_0), value);
	}

	inline static int32_t get_offset_of_sigAlgID_1() { return static_cast<int32_t>(offsetof(X509CertificateStructure_tEF05F4D8F02C2BC366AC06A3E2BBDB4C35D54FE1, ___sigAlgID_1)); }
	inline AlgorithmIdentifier_tDE7123E83DAD8C219BBB99DAE8CAAD8A2AE64EFB * get_sigAlgID_1() const { return ___sigAlgID_1; }
	inline AlgorithmIdentifier_tDE7123E83DAD8C219BBB99DAE8CAAD8A2AE64EFB ** get_address_of_sigAlgID_1() { return &___sigAlgID_1; }
	inline void set_sigAlgID_1(AlgorithmIdentifier_tDE7123E83DAD8C219BBB99DAE8CAAD8A2AE64EFB * value)
	{
		___sigAlgID_1 = value;
		Il2CppCodeGenWriteBarrier((&___sigAlgID_1), value);
	}

	inline static int32_t get_offset_of_sig_2() { return static_cast<int32_t>(offsetof(X509CertificateStructure_tEF05F4D8F02C2BC366AC06A3E2BBDB4C35D54FE1, ___sig_2)); }
	inline DerBitString_tCA371BCECC9BA7643D0127424F54EE10C1B19A64 * get_sig_2() const { return ___sig_2; }
	inline DerBitString_tCA371BCECC9BA7643D0127424F54EE10C1B19A64 ** get_address_of_sig_2() { return &___sig_2; }
	inline void set_sig_2(DerBitString_tCA371BCECC9BA7643D0127424F54EE10C1B19A64 * value)
	{
		___sig_2 = value;
		Il2CppCodeGenWriteBarrier((&___sig_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CERTIFICATESTRUCTURE_TEF05F4D8F02C2BC366AC06A3E2BBDB4C35D54FE1_H
#ifndef X509EXTENSIONS_TBC4870829033B1A0E0F297F32C0B2B29C1618E44_H
#define X509EXTENSIONS_TBC4870829033B1A0E0F297F32C0B2B29C1618E44_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.X509.X509Extensions
struct  X509Extensions_tBC4870829033B1A0E0F297F32C0B2B29C1618E44  : public Asn1Encodable_t390FCE82F5F52E41A7320BBF2E8BF18DA1622A8B
{
public:
	// System.Collections.IDictionary Org.BouncyCastle.Asn1.X509.X509Extensions::extensions
	RuntimeObject* ___extensions_31;
	// System.Collections.IList Org.BouncyCastle.Asn1.X509.X509Extensions::ordering
	RuntimeObject* ___ordering_32;

public:
	inline static int32_t get_offset_of_extensions_31() { return static_cast<int32_t>(offsetof(X509Extensions_tBC4870829033B1A0E0F297F32C0B2B29C1618E44, ___extensions_31)); }
	inline RuntimeObject* get_extensions_31() const { return ___extensions_31; }
	inline RuntimeObject** get_address_of_extensions_31() { return &___extensions_31; }
	inline void set_extensions_31(RuntimeObject* value)
	{
		___extensions_31 = value;
		Il2CppCodeGenWriteBarrier((&___extensions_31), value);
	}

	inline static int32_t get_offset_of_ordering_32() { return static_cast<int32_t>(offsetof(X509Extensions_tBC4870829033B1A0E0F297F32C0B2B29C1618E44, ___ordering_32)); }
	inline RuntimeObject* get_ordering_32() const { return ___ordering_32; }
	inline RuntimeObject** get_address_of_ordering_32() { return &___ordering_32; }
	inline void set_ordering_32(RuntimeObject* value)
	{
		___ordering_32 = value;
		Il2CppCodeGenWriteBarrier((&___ordering_32), value);
	}
};

struct X509Extensions_tBC4870829033B1A0E0F297F32C0B2B29C1618E44_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509Extensions::SubjectDirectoryAttributes
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___SubjectDirectoryAttributes_0;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509Extensions::SubjectKeyIdentifier
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___SubjectKeyIdentifier_1;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509Extensions::KeyUsage
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___KeyUsage_2;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509Extensions::PrivateKeyUsagePeriod
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___PrivateKeyUsagePeriod_3;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509Extensions::SubjectAlternativeName
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___SubjectAlternativeName_4;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509Extensions::IssuerAlternativeName
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IssuerAlternativeName_5;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509Extensions::BasicConstraints
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___BasicConstraints_6;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509Extensions::CrlNumber
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___CrlNumber_7;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509Extensions::ReasonCode
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___ReasonCode_8;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509Extensions::InstructionCode
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___InstructionCode_9;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509Extensions::InvalidityDate
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___InvalidityDate_10;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509Extensions::DeltaCrlIndicator
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___DeltaCrlIndicator_11;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509Extensions::IssuingDistributionPoint
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IssuingDistributionPoint_12;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509Extensions::CertificateIssuer
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___CertificateIssuer_13;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509Extensions::NameConstraints
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___NameConstraints_14;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509Extensions::CrlDistributionPoints
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___CrlDistributionPoints_15;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509Extensions::CertificatePolicies
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___CertificatePolicies_16;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509Extensions::PolicyMappings
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___PolicyMappings_17;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509Extensions::AuthorityKeyIdentifier
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___AuthorityKeyIdentifier_18;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509Extensions::PolicyConstraints
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___PolicyConstraints_19;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509Extensions::ExtendedKeyUsage
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___ExtendedKeyUsage_20;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509Extensions::FreshestCrl
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___FreshestCrl_21;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509Extensions::InhibitAnyPolicy
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___InhibitAnyPolicy_22;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509Extensions::AuthorityInfoAccess
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___AuthorityInfoAccess_23;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509Extensions::SubjectInfoAccess
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___SubjectInfoAccess_24;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509Extensions::LogoType
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___LogoType_25;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509Extensions::BiometricInfo
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___BiometricInfo_26;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509Extensions::QCStatements
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___QCStatements_27;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509Extensions::AuditIdentity
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___AuditIdentity_28;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509Extensions::NoRevAvail
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___NoRevAvail_29;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509Extensions::TargetInformation
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___TargetInformation_30;

public:
	inline static int32_t get_offset_of_SubjectDirectoryAttributes_0() { return static_cast<int32_t>(offsetof(X509Extensions_tBC4870829033B1A0E0F297F32C0B2B29C1618E44_StaticFields, ___SubjectDirectoryAttributes_0)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_SubjectDirectoryAttributes_0() const { return ___SubjectDirectoryAttributes_0; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_SubjectDirectoryAttributes_0() { return &___SubjectDirectoryAttributes_0; }
	inline void set_SubjectDirectoryAttributes_0(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___SubjectDirectoryAttributes_0 = value;
		Il2CppCodeGenWriteBarrier((&___SubjectDirectoryAttributes_0), value);
	}

	inline static int32_t get_offset_of_SubjectKeyIdentifier_1() { return static_cast<int32_t>(offsetof(X509Extensions_tBC4870829033B1A0E0F297F32C0B2B29C1618E44_StaticFields, ___SubjectKeyIdentifier_1)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_SubjectKeyIdentifier_1() const { return ___SubjectKeyIdentifier_1; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_SubjectKeyIdentifier_1() { return &___SubjectKeyIdentifier_1; }
	inline void set_SubjectKeyIdentifier_1(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___SubjectKeyIdentifier_1 = value;
		Il2CppCodeGenWriteBarrier((&___SubjectKeyIdentifier_1), value);
	}

	inline static int32_t get_offset_of_KeyUsage_2() { return static_cast<int32_t>(offsetof(X509Extensions_tBC4870829033B1A0E0F297F32C0B2B29C1618E44_StaticFields, ___KeyUsage_2)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_KeyUsage_2() const { return ___KeyUsage_2; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_KeyUsage_2() { return &___KeyUsage_2; }
	inline void set_KeyUsage_2(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___KeyUsage_2 = value;
		Il2CppCodeGenWriteBarrier((&___KeyUsage_2), value);
	}

	inline static int32_t get_offset_of_PrivateKeyUsagePeriod_3() { return static_cast<int32_t>(offsetof(X509Extensions_tBC4870829033B1A0E0F297F32C0B2B29C1618E44_StaticFields, ___PrivateKeyUsagePeriod_3)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_PrivateKeyUsagePeriod_3() const { return ___PrivateKeyUsagePeriod_3; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_PrivateKeyUsagePeriod_3() { return &___PrivateKeyUsagePeriod_3; }
	inline void set_PrivateKeyUsagePeriod_3(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___PrivateKeyUsagePeriod_3 = value;
		Il2CppCodeGenWriteBarrier((&___PrivateKeyUsagePeriod_3), value);
	}

	inline static int32_t get_offset_of_SubjectAlternativeName_4() { return static_cast<int32_t>(offsetof(X509Extensions_tBC4870829033B1A0E0F297F32C0B2B29C1618E44_StaticFields, ___SubjectAlternativeName_4)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_SubjectAlternativeName_4() const { return ___SubjectAlternativeName_4; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_SubjectAlternativeName_4() { return &___SubjectAlternativeName_4; }
	inline void set_SubjectAlternativeName_4(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___SubjectAlternativeName_4 = value;
		Il2CppCodeGenWriteBarrier((&___SubjectAlternativeName_4), value);
	}

	inline static int32_t get_offset_of_IssuerAlternativeName_5() { return static_cast<int32_t>(offsetof(X509Extensions_tBC4870829033B1A0E0F297F32C0B2B29C1618E44_StaticFields, ___IssuerAlternativeName_5)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IssuerAlternativeName_5() const { return ___IssuerAlternativeName_5; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IssuerAlternativeName_5() { return &___IssuerAlternativeName_5; }
	inline void set_IssuerAlternativeName_5(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IssuerAlternativeName_5 = value;
		Il2CppCodeGenWriteBarrier((&___IssuerAlternativeName_5), value);
	}

	inline static int32_t get_offset_of_BasicConstraints_6() { return static_cast<int32_t>(offsetof(X509Extensions_tBC4870829033B1A0E0F297F32C0B2B29C1618E44_StaticFields, ___BasicConstraints_6)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_BasicConstraints_6() const { return ___BasicConstraints_6; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_BasicConstraints_6() { return &___BasicConstraints_6; }
	inline void set_BasicConstraints_6(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___BasicConstraints_6 = value;
		Il2CppCodeGenWriteBarrier((&___BasicConstraints_6), value);
	}

	inline static int32_t get_offset_of_CrlNumber_7() { return static_cast<int32_t>(offsetof(X509Extensions_tBC4870829033B1A0E0F297F32C0B2B29C1618E44_StaticFields, ___CrlNumber_7)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_CrlNumber_7() const { return ___CrlNumber_7; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_CrlNumber_7() { return &___CrlNumber_7; }
	inline void set_CrlNumber_7(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___CrlNumber_7 = value;
		Il2CppCodeGenWriteBarrier((&___CrlNumber_7), value);
	}

	inline static int32_t get_offset_of_ReasonCode_8() { return static_cast<int32_t>(offsetof(X509Extensions_tBC4870829033B1A0E0F297F32C0B2B29C1618E44_StaticFields, ___ReasonCode_8)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_ReasonCode_8() const { return ___ReasonCode_8; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_ReasonCode_8() { return &___ReasonCode_8; }
	inline void set_ReasonCode_8(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___ReasonCode_8 = value;
		Il2CppCodeGenWriteBarrier((&___ReasonCode_8), value);
	}

	inline static int32_t get_offset_of_InstructionCode_9() { return static_cast<int32_t>(offsetof(X509Extensions_tBC4870829033B1A0E0F297F32C0B2B29C1618E44_StaticFields, ___InstructionCode_9)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_InstructionCode_9() const { return ___InstructionCode_9; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_InstructionCode_9() { return &___InstructionCode_9; }
	inline void set_InstructionCode_9(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___InstructionCode_9 = value;
		Il2CppCodeGenWriteBarrier((&___InstructionCode_9), value);
	}

	inline static int32_t get_offset_of_InvalidityDate_10() { return static_cast<int32_t>(offsetof(X509Extensions_tBC4870829033B1A0E0F297F32C0B2B29C1618E44_StaticFields, ___InvalidityDate_10)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_InvalidityDate_10() const { return ___InvalidityDate_10; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_InvalidityDate_10() { return &___InvalidityDate_10; }
	inline void set_InvalidityDate_10(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___InvalidityDate_10 = value;
		Il2CppCodeGenWriteBarrier((&___InvalidityDate_10), value);
	}

	inline static int32_t get_offset_of_DeltaCrlIndicator_11() { return static_cast<int32_t>(offsetof(X509Extensions_tBC4870829033B1A0E0F297F32C0B2B29C1618E44_StaticFields, ___DeltaCrlIndicator_11)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_DeltaCrlIndicator_11() const { return ___DeltaCrlIndicator_11; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_DeltaCrlIndicator_11() { return &___DeltaCrlIndicator_11; }
	inline void set_DeltaCrlIndicator_11(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___DeltaCrlIndicator_11 = value;
		Il2CppCodeGenWriteBarrier((&___DeltaCrlIndicator_11), value);
	}

	inline static int32_t get_offset_of_IssuingDistributionPoint_12() { return static_cast<int32_t>(offsetof(X509Extensions_tBC4870829033B1A0E0F297F32C0B2B29C1618E44_StaticFields, ___IssuingDistributionPoint_12)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IssuingDistributionPoint_12() const { return ___IssuingDistributionPoint_12; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IssuingDistributionPoint_12() { return &___IssuingDistributionPoint_12; }
	inline void set_IssuingDistributionPoint_12(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IssuingDistributionPoint_12 = value;
		Il2CppCodeGenWriteBarrier((&___IssuingDistributionPoint_12), value);
	}

	inline static int32_t get_offset_of_CertificateIssuer_13() { return static_cast<int32_t>(offsetof(X509Extensions_tBC4870829033B1A0E0F297F32C0B2B29C1618E44_StaticFields, ___CertificateIssuer_13)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_CertificateIssuer_13() const { return ___CertificateIssuer_13; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_CertificateIssuer_13() { return &___CertificateIssuer_13; }
	inline void set_CertificateIssuer_13(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___CertificateIssuer_13 = value;
		Il2CppCodeGenWriteBarrier((&___CertificateIssuer_13), value);
	}

	inline static int32_t get_offset_of_NameConstraints_14() { return static_cast<int32_t>(offsetof(X509Extensions_tBC4870829033B1A0E0F297F32C0B2B29C1618E44_StaticFields, ___NameConstraints_14)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_NameConstraints_14() const { return ___NameConstraints_14; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_NameConstraints_14() { return &___NameConstraints_14; }
	inline void set_NameConstraints_14(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___NameConstraints_14 = value;
		Il2CppCodeGenWriteBarrier((&___NameConstraints_14), value);
	}

	inline static int32_t get_offset_of_CrlDistributionPoints_15() { return static_cast<int32_t>(offsetof(X509Extensions_tBC4870829033B1A0E0F297F32C0B2B29C1618E44_StaticFields, ___CrlDistributionPoints_15)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_CrlDistributionPoints_15() const { return ___CrlDistributionPoints_15; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_CrlDistributionPoints_15() { return &___CrlDistributionPoints_15; }
	inline void set_CrlDistributionPoints_15(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___CrlDistributionPoints_15 = value;
		Il2CppCodeGenWriteBarrier((&___CrlDistributionPoints_15), value);
	}

	inline static int32_t get_offset_of_CertificatePolicies_16() { return static_cast<int32_t>(offsetof(X509Extensions_tBC4870829033B1A0E0F297F32C0B2B29C1618E44_StaticFields, ___CertificatePolicies_16)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_CertificatePolicies_16() const { return ___CertificatePolicies_16; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_CertificatePolicies_16() { return &___CertificatePolicies_16; }
	inline void set_CertificatePolicies_16(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___CertificatePolicies_16 = value;
		Il2CppCodeGenWriteBarrier((&___CertificatePolicies_16), value);
	}

	inline static int32_t get_offset_of_PolicyMappings_17() { return static_cast<int32_t>(offsetof(X509Extensions_tBC4870829033B1A0E0F297F32C0B2B29C1618E44_StaticFields, ___PolicyMappings_17)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_PolicyMappings_17() const { return ___PolicyMappings_17; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_PolicyMappings_17() { return &___PolicyMappings_17; }
	inline void set_PolicyMappings_17(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___PolicyMappings_17 = value;
		Il2CppCodeGenWriteBarrier((&___PolicyMappings_17), value);
	}

	inline static int32_t get_offset_of_AuthorityKeyIdentifier_18() { return static_cast<int32_t>(offsetof(X509Extensions_tBC4870829033B1A0E0F297F32C0B2B29C1618E44_StaticFields, ___AuthorityKeyIdentifier_18)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_AuthorityKeyIdentifier_18() const { return ___AuthorityKeyIdentifier_18; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_AuthorityKeyIdentifier_18() { return &___AuthorityKeyIdentifier_18; }
	inline void set_AuthorityKeyIdentifier_18(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___AuthorityKeyIdentifier_18 = value;
		Il2CppCodeGenWriteBarrier((&___AuthorityKeyIdentifier_18), value);
	}

	inline static int32_t get_offset_of_PolicyConstraints_19() { return static_cast<int32_t>(offsetof(X509Extensions_tBC4870829033B1A0E0F297F32C0B2B29C1618E44_StaticFields, ___PolicyConstraints_19)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_PolicyConstraints_19() const { return ___PolicyConstraints_19; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_PolicyConstraints_19() { return &___PolicyConstraints_19; }
	inline void set_PolicyConstraints_19(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___PolicyConstraints_19 = value;
		Il2CppCodeGenWriteBarrier((&___PolicyConstraints_19), value);
	}

	inline static int32_t get_offset_of_ExtendedKeyUsage_20() { return static_cast<int32_t>(offsetof(X509Extensions_tBC4870829033B1A0E0F297F32C0B2B29C1618E44_StaticFields, ___ExtendedKeyUsage_20)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_ExtendedKeyUsage_20() const { return ___ExtendedKeyUsage_20; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_ExtendedKeyUsage_20() { return &___ExtendedKeyUsage_20; }
	inline void set_ExtendedKeyUsage_20(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___ExtendedKeyUsage_20 = value;
		Il2CppCodeGenWriteBarrier((&___ExtendedKeyUsage_20), value);
	}

	inline static int32_t get_offset_of_FreshestCrl_21() { return static_cast<int32_t>(offsetof(X509Extensions_tBC4870829033B1A0E0F297F32C0B2B29C1618E44_StaticFields, ___FreshestCrl_21)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_FreshestCrl_21() const { return ___FreshestCrl_21; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_FreshestCrl_21() { return &___FreshestCrl_21; }
	inline void set_FreshestCrl_21(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___FreshestCrl_21 = value;
		Il2CppCodeGenWriteBarrier((&___FreshestCrl_21), value);
	}

	inline static int32_t get_offset_of_InhibitAnyPolicy_22() { return static_cast<int32_t>(offsetof(X509Extensions_tBC4870829033B1A0E0F297F32C0B2B29C1618E44_StaticFields, ___InhibitAnyPolicy_22)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_InhibitAnyPolicy_22() const { return ___InhibitAnyPolicy_22; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_InhibitAnyPolicy_22() { return &___InhibitAnyPolicy_22; }
	inline void set_InhibitAnyPolicy_22(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___InhibitAnyPolicy_22 = value;
		Il2CppCodeGenWriteBarrier((&___InhibitAnyPolicy_22), value);
	}

	inline static int32_t get_offset_of_AuthorityInfoAccess_23() { return static_cast<int32_t>(offsetof(X509Extensions_tBC4870829033B1A0E0F297F32C0B2B29C1618E44_StaticFields, ___AuthorityInfoAccess_23)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_AuthorityInfoAccess_23() const { return ___AuthorityInfoAccess_23; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_AuthorityInfoAccess_23() { return &___AuthorityInfoAccess_23; }
	inline void set_AuthorityInfoAccess_23(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___AuthorityInfoAccess_23 = value;
		Il2CppCodeGenWriteBarrier((&___AuthorityInfoAccess_23), value);
	}

	inline static int32_t get_offset_of_SubjectInfoAccess_24() { return static_cast<int32_t>(offsetof(X509Extensions_tBC4870829033B1A0E0F297F32C0B2B29C1618E44_StaticFields, ___SubjectInfoAccess_24)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_SubjectInfoAccess_24() const { return ___SubjectInfoAccess_24; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_SubjectInfoAccess_24() { return &___SubjectInfoAccess_24; }
	inline void set_SubjectInfoAccess_24(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___SubjectInfoAccess_24 = value;
		Il2CppCodeGenWriteBarrier((&___SubjectInfoAccess_24), value);
	}

	inline static int32_t get_offset_of_LogoType_25() { return static_cast<int32_t>(offsetof(X509Extensions_tBC4870829033B1A0E0F297F32C0B2B29C1618E44_StaticFields, ___LogoType_25)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_LogoType_25() const { return ___LogoType_25; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_LogoType_25() { return &___LogoType_25; }
	inline void set_LogoType_25(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___LogoType_25 = value;
		Il2CppCodeGenWriteBarrier((&___LogoType_25), value);
	}

	inline static int32_t get_offset_of_BiometricInfo_26() { return static_cast<int32_t>(offsetof(X509Extensions_tBC4870829033B1A0E0F297F32C0B2B29C1618E44_StaticFields, ___BiometricInfo_26)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_BiometricInfo_26() const { return ___BiometricInfo_26; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_BiometricInfo_26() { return &___BiometricInfo_26; }
	inline void set_BiometricInfo_26(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___BiometricInfo_26 = value;
		Il2CppCodeGenWriteBarrier((&___BiometricInfo_26), value);
	}

	inline static int32_t get_offset_of_QCStatements_27() { return static_cast<int32_t>(offsetof(X509Extensions_tBC4870829033B1A0E0F297F32C0B2B29C1618E44_StaticFields, ___QCStatements_27)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_QCStatements_27() const { return ___QCStatements_27; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_QCStatements_27() { return &___QCStatements_27; }
	inline void set_QCStatements_27(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___QCStatements_27 = value;
		Il2CppCodeGenWriteBarrier((&___QCStatements_27), value);
	}

	inline static int32_t get_offset_of_AuditIdentity_28() { return static_cast<int32_t>(offsetof(X509Extensions_tBC4870829033B1A0E0F297F32C0B2B29C1618E44_StaticFields, ___AuditIdentity_28)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_AuditIdentity_28() const { return ___AuditIdentity_28; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_AuditIdentity_28() { return &___AuditIdentity_28; }
	inline void set_AuditIdentity_28(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___AuditIdentity_28 = value;
		Il2CppCodeGenWriteBarrier((&___AuditIdentity_28), value);
	}

	inline static int32_t get_offset_of_NoRevAvail_29() { return static_cast<int32_t>(offsetof(X509Extensions_tBC4870829033B1A0E0F297F32C0B2B29C1618E44_StaticFields, ___NoRevAvail_29)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_NoRevAvail_29() const { return ___NoRevAvail_29; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_NoRevAvail_29() { return &___NoRevAvail_29; }
	inline void set_NoRevAvail_29(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___NoRevAvail_29 = value;
		Il2CppCodeGenWriteBarrier((&___NoRevAvail_29), value);
	}

	inline static int32_t get_offset_of_TargetInformation_30() { return static_cast<int32_t>(offsetof(X509Extensions_tBC4870829033B1A0E0F297F32C0B2B29C1618E44_StaticFields, ___TargetInformation_30)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_TargetInformation_30() const { return ___TargetInformation_30; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_TargetInformation_30() { return &___TargetInformation_30; }
	inline void set_TargetInformation_30(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___TargetInformation_30 = value;
		Il2CppCodeGenWriteBarrier((&___TargetInformation_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509EXTENSIONS_TBC4870829033B1A0E0F297F32C0B2B29C1618E44_H
#ifndef X509NAME_T00D18BE10190B7EC9BD6B234477771FBE4CA304E_H
#define X509NAME_T00D18BE10190B7EC9BD6B234477771FBE4CA304E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.X509.X509Name
struct  X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E  : public Asn1Encodable_t390FCE82F5F52E41A7320BBF2E8BF18DA1622A8B
{
public:
	// System.Collections.IList Org.BouncyCastle.Asn1.X509.X509Name::ordering
	RuntimeObject* ___ordering_39;
	// Org.BouncyCastle.Asn1.X509.X509NameEntryConverter Org.BouncyCastle.Asn1.X509.X509Name::converter
	X509NameEntryConverter_t6B3AE1DD9E7B91F1C0039575CEAC00CAE0972DED * ___converter_40;
	// System.Collections.IList Org.BouncyCastle.Asn1.X509.X509Name::values
	RuntimeObject* ___values_41;
	// System.Collections.IList Org.BouncyCastle.Asn1.X509.X509Name::added
	RuntimeObject* ___added_42;
	// Org.BouncyCastle.Asn1.Asn1Sequence Org.BouncyCastle.Asn1.X509.X509Name::seq
	Asn1Sequence_t292AE1C416511A0DE153DE285AEE498B50523B85 * ___seq_43;

public:
	inline static int32_t get_offset_of_ordering_39() { return static_cast<int32_t>(offsetof(X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E, ___ordering_39)); }
	inline RuntimeObject* get_ordering_39() const { return ___ordering_39; }
	inline RuntimeObject** get_address_of_ordering_39() { return &___ordering_39; }
	inline void set_ordering_39(RuntimeObject* value)
	{
		___ordering_39 = value;
		Il2CppCodeGenWriteBarrier((&___ordering_39), value);
	}

	inline static int32_t get_offset_of_converter_40() { return static_cast<int32_t>(offsetof(X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E, ___converter_40)); }
	inline X509NameEntryConverter_t6B3AE1DD9E7B91F1C0039575CEAC00CAE0972DED * get_converter_40() const { return ___converter_40; }
	inline X509NameEntryConverter_t6B3AE1DD9E7B91F1C0039575CEAC00CAE0972DED ** get_address_of_converter_40() { return &___converter_40; }
	inline void set_converter_40(X509NameEntryConverter_t6B3AE1DD9E7B91F1C0039575CEAC00CAE0972DED * value)
	{
		___converter_40 = value;
		Il2CppCodeGenWriteBarrier((&___converter_40), value);
	}

	inline static int32_t get_offset_of_values_41() { return static_cast<int32_t>(offsetof(X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E, ___values_41)); }
	inline RuntimeObject* get_values_41() const { return ___values_41; }
	inline RuntimeObject** get_address_of_values_41() { return &___values_41; }
	inline void set_values_41(RuntimeObject* value)
	{
		___values_41 = value;
		Il2CppCodeGenWriteBarrier((&___values_41), value);
	}

	inline static int32_t get_offset_of_added_42() { return static_cast<int32_t>(offsetof(X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E, ___added_42)); }
	inline RuntimeObject* get_added_42() const { return ___added_42; }
	inline RuntimeObject** get_address_of_added_42() { return &___added_42; }
	inline void set_added_42(RuntimeObject* value)
	{
		___added_42 = value;
		Il2CppCodeGenWriteBarrier((&___added_42), value);
	}

	inline static int32_t get_offset_of_seq_43() { return static_cast<int32_t>(offsetof(X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E, ___seq_43)); }
	inline Asn1Sequence_t292AE1C416511A0DE153DE285AEE498B50523B85 * get_seq_43() const { return ___seq_43; }
	inline Asn1Sequence_t292AE1C416511A0DE153DE285AEE498B50523B85 ** get_address_of_seq_43() { return &___seq_43; }
	inline void set_seq_43(Asn1Sequence_t292AE1C416511A0DE153DE285AEE498B50523B85 * value)
	{
		___seq_43 = value;
		Il2CppCodeGenWriteBarrier((&___seq_43), value);
	}
};

struct X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509Name::C
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___C_0;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509Name::O
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___O_1;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509Name::OU
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___OU_2;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509Name::T
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___T_3;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509Name::CN
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___CN_4;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509Name::Street
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___Street_5;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509Name::SerialNumber
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___SerialNumber_6;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509Name::L
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___L_7;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509Name::ST
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___ST_8;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509Name::Surname
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___Surname_9;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509Name::GivenName
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___GivenName_10;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509Name::Initials
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___Initials_11;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509Name::Generation
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___Generation_12;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509Name::UniqueIdentifier
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___UniqueIdentifier_13;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509Name::BusinessCategory
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___BusinessCategory_14;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509Name::PostalCode
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___PostalCode_15;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509Name::DnQualifier
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___DnQualifier_16;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509Name::Pseudonym
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___Pseudonym_17;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509Name::DateOfBirth
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___DateOfBirth_18;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509Name::PlaceOfBirth
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___PlaceOfBirth_19;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509Name::Gender
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___Gender_20;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509Name::CountryOfCitizenship
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___CountryOfCitizenship_21;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509Name::CountryOfResidence
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___CountryOfResidence_22;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509Name::NameAtBirth
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___NameAtBirth_23;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509Name::PostalAddress
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___PostalAddress_24;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509Name::DmdName
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___DmdName_25;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509Name::TelephoneNumber
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___TelephoneNumber_26;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509Name::Name
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___Name_27;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509Name::EmailAddress
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___EmailAddress_28;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509Name::UnstructuredName
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___UnstructuredName_29;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509Name::UnstructuredAddress
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___UnstructuredAddress_30;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509Name::E
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___E_31;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509Name::DC
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___DC_32;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X509.X509Name::UID
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___UID_33;
	// System.Boolean[] Org.BouncyCastle.Asn1.X509.X509Name::defaultReverse
	BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* ___defaultReverse_34;
	// System.Collections.Hashtable Org.BouncyCastle.Asn1.X509.X509Name::DefaultSymbols
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___DefaultSymbols_35;
	// System.Collections.Hashtable Org.BouncyCastle.Asn1.X509.X509Name::RFC2253Symbols
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___RFC2253Symbols_36;
	// System.Collections.Hashtable Org.BouncyCastle.Asn1.X509.X509Name::RFC1779Symbols
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___RFC1779Symbols_37;
	// System.Collections.Hashtable Org.BouncyCastle.Asn1.X509.X509Name::DefaultLookup
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___DefaultLookup_38;

public:
	inline static int32_t get_offset_of_C_0() { return static_cast<int32_t>(offsetof(X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields, ___C_0)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_C_0() const { return ___C_0; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_C_0() { return &___C_0; }
	inline void set_C_0(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___C_0 = value;
		Il2CppCodeGenWriteBarrier((&___C_0), value);
	}

	inline static int32_t get_offset_of_O_1() { return static_cast<int32_t>(offsetof(X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields, ___O_1)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_O_1() const { return ___O_1; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_O_1() { return &___O_1; }
	inline void set_O_1(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___O_1 = value;
		Il2CppCodeGenWriteBarrier((&___O_1), value);
	}

	inline static int32_t get_offset_of_OU_2() { return static_cast<int32_t>(offsetof(X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields, ___OU_2)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_OU_2() const { return ___OU_2; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_OU_2() { return &___OU_2; }
	inline void set_OU_2(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___OU_2 = value;
		Il2CppCodeGenWriteBarrier((&___OU_2), value);
	}

	inline static int32_t get_offset_of_T_3() { return static_cast<int32_t>(offsetof(X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields, ___T_3)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_T_3() const { return ___T_3; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_T_3() { return &___T_3; }
	inline void set_T_3(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___T_3 = value;
		Il2CppCodeGenWriteBarrier((&___T_3), value);
	}

	inline static int32_t get_offset_of_CN_4() { return static_cast<int32_t>(offsetof(X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields, ___CN_4)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_CN_4() const { return ___CN_4; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_CN_4() { return &___CN_4; }
	inline void set_CN_4(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___CN_4 = value;
		Il2CppCodeGenWriteBarrier((&___CN_4), value);
	}

	inline static int32_t get_offset_of_Street_5() { return static_cast<int32_t>(offsetof(X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields, ___Street_5)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_Street_5() const { return ___Street_5; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_Street_5() { return &___Street_5; }
	inline void set_Street_5(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___Street_5 = value;
		Il2CppCodeGenWriteBarrier((&___Street_5), value);
	}

	inline static int32_t get_offset_of_SerialNumber_6() { return static_cast<int32_t>(offsetof(X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields, ___SerialNumber_6)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_SerialNumber_6() const { return ___SerialNumber_6; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_SerialNumber_6() { return &___SerialNumber_6; }
	inline void set_SerialNumber_6(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___SerialNumber_6 = value;
		Il2CppCodeGenWriteBarrier((&___SerialNumber_6), value);
	}

	inline static int32_t get_offset_of_L_7() { return static_cast<int32_t>(offsetof(X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields, ___L_7)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_L_7() const { return ___L_7; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_L_7() { return &___L_7; }
	inline void set_L_7(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___L_7 = value;
		Il2CppCodeGenWriteBarrier((&___L_7), value);
	}

	inline static int32_t get_offset_of_ST_8() { return static_cast<int32_t>(offsetof(X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields, ___ST_8)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_ST_8() const { return ___ST_8; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_ST_8() { return &___ST_8; }
	inline void set_ST_8(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___ST_8 = value;
		Il2CppCodeGenWriteBarrier((&___ST_8), value);
	}

	inline static int32_t get_offset_of_Surname_9() { return static_cast<int32_t>(offsetof(X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields, ___Surname_9)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_Surname_9() const { return ___Surname_9; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_Surname_9() { return &___Surname_9; }
	inline void set_Surname_9(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___Surname_9 = value;
		Il2CppCodeGenWriteBarrier((&___Surname_9), value);
	}

	inline static int32_t get_offset_of_GivenName_10() { return static_cast<int32_t>(offsetof(X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields, ___GivenName_10)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_GivenName_10() const { return ___GivenName_10; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_GivenName_10() { return &___GivenName_10; }
	inline void set_GivenName_10(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___GivenName_10 = value;
		Il2CppCodeGenWriteBarrier((&___GivenName_10), value);
	}

	inline static int32_t get_offset_of_Initials_11() { return static_cast<int32_t>(offsetof(X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields, ___Initials_11)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_Initials_11() const { return ___Initials_11; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_Initials_11() { return &___Initials_11; }
	inline void set_Initials_11(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___Initials_11 = value;
		Il2CppCodeGenWriteBarrier((&___Initials_11), value);
	}

	inline static int32_t get_offset_of_Generation_12() { return static_cast<int32_t>(offsetof(X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields, ___Generation_12)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_Generation_12() const { return ___Generation_12; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_Generation_12() { return &___Generation_12; }
	inline void set_Generation_12(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___Generation_12 = value;
		Il2CppCodeGenWriteBarrier((&___Generation_12), value);
	}

	inline static int32_t get_offset_of_UniqueIdentifier_13() { return static_cast<int32_t>(offsetof(X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields, ___UniqueIdentifier_13)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_UniqueIdentifier_13() const { return ___UniqueIdentifier_13; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_UniqueIdentifier_13() { return &___UniqueIdentifier_13; }
	inline void set_UniqueIdentifier_13(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___UniqueIdentifier_13 = value;
		Il2CppCodeGenWriteBarrier((&___UniqueIdentifier_13), value);
	}

	inline static int32_t get_offset_of_BusinessCategory_14() { return static_cast<int32_t>(offsetof(X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields, ___BusinessCategory_14)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_BusinessCategory_14() const { return ___BusinessCategory_14; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_BusinessCategory_14() { return &___BusinessCategory_14; }
	inline void set_BusinessCategory_14(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___BusinessCategory_14 = value;
		Il2CppCodeGenWriteBarrier((&___BusinessCategory_14), value);
	}

	inline static int32_t get_offset_of_PostalCode_15() { return static_cast<int32_t>(offsetof(X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields, ___PostalCode_15)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_PostalCode_15() const { return ___PostalCode_15; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_PostalCode_15() { return &___PostalCode_15; }
	inline void set_PostalCode_15(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___PostalCode_15 = value;
		Il2CppCodeGenWriteBarrier((&___PostalCode_15), value);
	}

	inline static int32_t get_offset_of_DnQualifier_16() { return static_cast<int32_t>(offsetof(X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields, ___DnQualifier_16)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_DnQualifier_16() const { return ___DnQualifier_16; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_DnQualifier_16() { return &___DnQualifier_16; }
	inline void set_DnQualifier_16(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___DnQualifier_16 = value;
		Il2CppCodeGenWriteBarrier((&___DnQualifier_16), value);
	}

	inline static int32_t get_offset_of_Pseudonym_17() { return static_cast<int32_t>(offsetof(X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields, ___Pseudonym_17)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_Pseudonym_17() const { return ___Pseudonym_17; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_Pseudonym_17() { return &___Pseudonym_17; }
	inline void set_Pseudonym_17(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___Pseudonym_17 = value;
		Il2CppCodeGenWriteBarrier((&___Pseudonym_17), value);
	}

	inline static int32_t get_offset_of_DateOfBirth_18() { return static_cast<int32_t>(offsetof(X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields, ___DateOfBirth_18)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_DateOfBirth_18() const { return ___DateOfBirth_18; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_DateOfBirth_18() { return &___DateOfBirth_18; }
	inline void set_DateOfBirth_18(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___DateOfBirth_18 = value;
		Il2CppCodeGenWriteBarrier((&___DateOfBirth_18), value);
	}

	inline static int32_t get_offset_of_PlaceOfBirth_19() { return static_cast<int32_t>(offsetof(X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields, ___PlaceOfBirth_19)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_PlaceOfBirth_19() const { return ___PlaceOfBirth_19; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_PlaceOfBirth_19() { return &___PlaceOfBirth_19; }
	inline void set_PlaceOfBirth_19(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___PlaceOfBirth_19 = value;
		Il2CppCodeGenWriteBarrier((&___PlaceOfBirth_19), value);
	}

	inline static int32_t get_offset_of_Gender_20() { return static_cast<int32_t>(offsetof(X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields, ___Gender_20)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_Gender_20() const { return ___Gender_20; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_Gender_20() { return &___Gender_20; }
	inline void set_Gender_20(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___Gender_20 = value;
		Il2CppCodeGenWriteBarrier((&___Gender_20), value);
	}

	inline static int32_t get_offset_of_CountryOfCitizenship_21() { return static_cast<int32_t>(offsetof(X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields, ___CountryOfCitizenship_21)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_CountryOfCitizenship_21() const { return ___CountryOfCitizenship_21; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_CountryOfCitizenship_21() { return &___CountryOfCitizenship_21; }
	inline void set_CountryOfCitizenship_21(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___CountryOfCitizenship_21 = value;
		Il2CppCodeGenWriteBarrier((&___CountryOfCitizenship_21), value);
	}

	inline static int32_t get_offset_of_CountryOfResidence_22() { return static_cast<int32_t>(offsetof(X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields, ___CountryOfResidence_22)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_CountryOfResidence_22() const { return ___CountryOfResidence_22; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_CountryOfResidence_22() { return &___CountryOfResidence_22; }
	inline void set_CountryOfResidence_22(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___CountryOfResidence_22 = value;
		Il2CppCodeGenWriteBarrier((&___CountryOfResidence_22), value);
	}

	inline static int32_t get_offset_of_NameAtBirth_23() { return static_cast<int32_t>(offsetof(X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields, ___NameAtBirth_23)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_NameAtBirth_23() const { return ___NameAtBirth_23; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_NameAtBirth_23() { return &___NameAtBirth_23; }
	inline void set_NameAtBirth_23(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___NameAtBirth_23 = value;
		Il2CppCodeGenWriteBarrier((&___NameAtBirth_23), value);
	}

	inline static int32_t get_offset_of_PostalAddress_24() { return static_cast<int32_t>(offsetof(X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields, ___PostalAddress_24)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_PostalAddress_24() const { return ___PostalAddress_24; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_PostalAddress_24() { return &___PostalAddress_24; }
	inline void set_PostalAddress_24(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___PostalAddress_24 = value;
		Il2CppCodeGenWriteBarrier((&___PostalAddress_24), value);
	}

	inline static int32_t get_offset_of_DmdName_25() { return static_cast<int32_t>(offsetof(X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields, ___DmdName_25)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_DmdName_25() const { return ___DmdName_25; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_DmdName_25() { return &___DmdName_25; }
	inline void set_DmdName_25(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___DmdName_25 = value;
		Il2CppCodeGenWriteBarrier((&___DmdName_25), value);
	}

	inline static int32_t get_offset_of_TelephoneNumber_26() { return static_cast<int32_t>(offsetof(X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields, ___TelephoneNumber_26)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_TelephoneNumber_26() const { return ___TelephoneNumber_26; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_TelephoneNumber_26() { return &___TelephoneNumber_26; }
	inline void set_TelephoneNumber_26(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___TelephoneNumber_26 = value;
		Il2CppCodeGenWriteBarrier((&___TelephoneNumber_26), value);
	}

	inline static int32_t get_offset_of_Name_27() { return static_cast<int32_t>(offsetof(X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields, ___Name_27)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_Name_27() const { return ___Name_27; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_Name_27() { return &___Name_27; }
	inline void set_Name_27(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___Name_27 = value;
		Il2CppCodeGenWriteBarrier((&___Name_27), value);
	}

	inline static int32_t get_offset_of_EmailAddress_28() { return static_cast<int32_t>(offsetof(X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields, ___EmailAddress_28)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_EmailAddress_28() const { return ___EmailAddress_28; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_EmailAddress_28() { return &___EmailAddress_28; }
	inline void set_EmailAddress_28(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___EmailAddress_28 = value;
		Il2CppCodeGenWriteBarrier((&___EmailAddress_28), value);
	}

	inline static int32_t get_offset_of_UnstructuredName_29() { return static_cast<int32_t>(offsetof(X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields, ___UnstructuredName_29)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_UnstructuredName_29() const { return ___UnstructuredName_29; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_UnstructuredName_29() { return &___UnstructuredName_29; }
	inline void set_UnstructuredName_29(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___UnstructuredName_29 = value;
		Il2CppCodeGenWriteBarrier((&___UnstructuredName_29), value);
	}

	inline static int32_t get_offset_of_UnstructuredAddress_30() { return static_cast<int32_t>(offsetof(X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields, ___UnstructuredAddress_30)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_UnstructuredAddress_30() const { return ___UnstructuredAddress_30; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_UnstructuredAddress_30() { return &___UnstructuredAddress_30; }
	inline void set_UnstructuredAddress_30(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___UnstructuredAddress_30 = value;
		Il2CppCodeGenWriteBarrier((&___UnstructuredAddress_30), value);
	}

	inline static int32_t get_offset_of_E_31() { return static_cast<int32_t>(offsetof(X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields, ___E_31)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_E_31() const { return ___E_31; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_E_31() { return &___E_31; }
	inline void set_E_31(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___E_31 = value;
		Il2CppCodeGenWriteBarrier((&___E_31), value);
	}

	inline static int32_t get_offset_of_DC_32() { return static_cast<int32_t>(offsetof(X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields, ___DC_32)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_DC_32() const { return ___DC_32; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_DC_32() { return &___DC_32; }
	inline void set_DC_32(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___DC_32 = value;
		Il2CppCodeGenWriteBarrier((&___DC_32), value);
	}

	inline static int32_t get_offset_of_UID_33() { return static_cast<int32_t>(offsetof(X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields, ___UID_33)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_UID_33() const { return ___UID_33; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_UID_33() { return &___UID_33; }
	inline void set_UID_33(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___UID_33 = value;
		Il2CppCodeGenWriteBarrier((&___UID_33), value);
	}

	inline static int32_t get_offset_of_defaultReverse_34() { return static_cast<int32_t>(offsetof(X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields, ___defaultReverse_34)); }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* get_defaultReverse_34() const { return ___defaultReverse_34; }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040** get_address_of_defaultReverse_34() { return &___defaultReverse_34; }
	inline void set_defaultReverse_34(BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* value)
	{
		___defaultReverse_34 = value;
		Il2CppCodeGenWriteBarrier((&___defaultReverse_34), value);
	}

	inline static int32_t get_offset_of_DefaultSymbols_35() { return static_cast<int32_t>(offsetof(X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields, ___DefaultSymbols_35)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_DefaultSymbols_35() const { return ___DefaultSymbols_35; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_DefaultSymbols_35() { return &___DefaultSymbols_35; }
	inline void set_DefaultSymbols_35(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___DefaultSymbols_35 = value;
		Il2CppCodeGenWriteBarrier((&___DefaultSymbols_35), value);
	}

	inline static int32_t get_offset_of_RFC2253Symbols_36() { return static_cast<int32_t>(offsetof(X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields, ___RFC2253Symbols_36)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_RFC2253Symbols_36() const { return ___RFC2253Symbols_36; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_RFC2253Symbols_36() { return &___RFC2253Symbols_36; }
	inline void set_RFC2253Symbols_36(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___RFC2253Symbols_36 = value;
		Il2CppCodeGenWriteBarrier((&___RFC2253Symbols_36), value);
	}

	inline static int32_t get_offset_of_RFC1779Symbols_37() { return static_cast<int32_t>(offsetof(X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields, ___RFC1779Symbols_37)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_RFC1779Symbols_37() const { return ___RFC1779Symbols_37; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_RFC1779Symbols_37() { return &___RFC1779Symbols_37; }
	inline void set_RFC1779Symbols_37(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___RFC1779Symbols_37 = value;
		Il2CppCodeGenWriteBarrier((&___RFC1779Symbols_37), value);
	}

	inline static int32_t get_offset_of_DefaultLookup_38() { return static_cast<int32_t>(offsetof(X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields, ___DefaultLookup_38)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_DefaultLookup_38() const { return ___DefaultLookup_38; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_DefaultLookup_38() { return &___DefaultLookup_38; }
	inline void set_DefaultLookup_38(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___DefaultLookup_38 = value;
		Il2CppCodeGenWriteBarrier((&___DefaultLookup_38), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509NAME_T00D18BE10190B7EC9BD6B234477771FBE4CA304E_H
#ifndef DHDOMAINPARAMETERS_T542D48B2F5FDB54471E254C9C3B42BA7CDA1FA54_H
#define DHDOMAINPARAMETERS_T542D48B2F5FDB54471E254C9C3B42BA7CDA1FA54_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.X9.DHDomainParameters
struct  DHDomainParameters_t542D48B2F5FDB54471E254C9C3B42BA7CDA1FA54  : public Asn1Encodable_t390FCE82F5F52E41A7320BBF2E8BF18DA1622A8B
{
public:
	// Org.BouncyCastle.Asn1.DerInteger Org.BouncyCastle.Asn1.X9.DHDomainParameters::p
	DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A * ___p_0;
	// Org.BouncyCastle.Asn1.DerInteger Org.BouncyCastle.Asn1.X9.DHDomainParameters::g
	DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A * ___g_1;
	// Org.BouncyCastle.Asn1.DerInteger Org.BouncyCastle.Asn1.X9.DHDomainParameters::q
	DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A * ___q_2;
	// Org.BouncyCastle.Asn1.DerInteger Org.BouncyCastle.Asn1.X9.DHDomainParameters::j
	DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A * ___j_3;
	// Org.BouncyCastle.Asn1.X9.DHValidationParms Org.BouncyCastle.Asn1.X9.DHDomainParameters::validationParms
	DHValidationParms_t8121E846FBD95D655031A424C5AECD365F767E34 * ___validationParms_4;

public:
	inline static int32_t get_offset_of_p_0() { return static_cast<int32_t>(offsetof(DHDomainParameters_t542D48B2F5FDB54471E254C9C3B42BA7CDA1FA54, ___p_0)); }
	inline DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A * get_p_0() const { return ___p_0; }
	inline DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A ** get_address_of_p_0() { return &___p_0; }
	inline void set_p_0(DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A * value)
	{
		___p_0 = value;
		Il2CppCodeGenWriteBarrier((&___p_0), value);
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(DHDomainParameters_t542D48B2F5FDB54471E254C9C3B42BA7CDA1FA54, ___g_1)); }
	inline DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A * get_g_1() const { return ___g_1; }
	inline DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A ** get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A * value)
	{
		___g_1 = value;
		Il2CppCodeGenWriteBarrier((&___g_1), value);
	}

	inline static int32_t get_offset_of_q_2() { return static_cast<int32_t>(offsetof(DHDomainParameters_t542D48B2F5FDB54471E254C9C3B42BA7CDA1FA54, ___q_2)); }
	inline DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A * get_q_2() const { return ___q_2; }
	inline DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A ** get_address_of_q_2() { return &___q_2; }
	inline void set_q_2(DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A * value)
	{
		___q_2 = value;
		Il2CppCodeGenWriteBarrier((&___q_2), value);
	}

	inline static int32_t get_offset_of_j_3() { return static_cast<int32_t>(offsetof(DHDomainParameters_t542D48B2F5FDB54471E254C9C3B42BA7CDA1FA54, ___j_3)); }
	inline DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A * get_j_3() const { return ___j_3; }
	inline DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A ** get_address_of_j_3() { return &___j_3; }
	inline void set_j_3(DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A * value)
	{
		___j_3 = value;
		Il2CppCodeGenWriteBarrier((&___j_3), value);
	}

	inline static int32_t get_offset_of_validationParms_4() { return static_cast<int32_t>(offsetof(DHDomainParameters_t542D48B2F5FDB54471E254C9C3B42BA7CDA1FA54, ___validationParms_4)); }
	inline DHValidationParms_t8121E846FBD95D655031A424C5AECD365F767E34 * get_validationParms_4() const { return ___validationParms_4; }
	inline DHValidationParms_t8121E846FBD95D655031A424C5AECD365F767E34 ** get_address_of_validationParms_4() { return &___validationParms_4; }
	inline void set_validationParms_4(DHValidationParms_t8121E846FBD95D655031A424C5AECD365F767E34 * value)
	{
		___validationParms_4 = value;
		Il2CppCodeGenWriteBarrier((&___validationParms_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DHDOMAINPARAMETERS_T542D48B2F5FDB54471E254C9C3B42BA7CDA1FA54_H
#ifndef DHPUBLICKEY_TD8B4B760470471953CC93F6C061B8B5008E1FABB_H
#define DHPUBLICKEY_TD8B4B760470471953CC93F6C061B8B5008E1FABB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.X9.DHPublicKey
struct  DHPublicKey_tD8B4B760470471953CC93F6C061B8B5008E1FABB  : public Asn1Encodable_t390FCE82F5F52E41A7320BBF2E8BF18DA1622A8B
{
public:
	// Org.BouncyCastle.Asn1.DerInteger Org.BouncyCastle.Asn1.X9.DHPublicKey::y
	DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A * ___y_0;

public:
	inline static int32_t get_offset_of_y_0() { return static_cast<int32_t>(offsetof(DHPublicKey_tD8B4B760470471953CC93F6C061B8B5008E1FABB, ___y_0)); }
	inline DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A * get_y_0() const { return ___y_0; }
	inline DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A ** get_address_of_y_0() { return &___y_0; }
	inline void set_y_0(DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A * value)
	{
		___y_0 = value;
		Il2CppCodeGenWriteBarrier((&___y_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DHPUBLICKEY_TD8B4B760470471953CC93F6C061B8B5008E1FABB_H
#ifndef DHVALIDATIONPARMS_T8121E846FBD95D655031A424C5AECD365F767E34_H
#define DHVALIDATIONPARMS_T8121E846FBD95D655031A424C5AECD365F767E34_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.X9.DHValidationParms
struct  DHValidationParms_t8121E846FBD95D655031A424C5AECD365F767E34  : public Asn1Encodable_t390FCE82F5F52E41A7320BBF2E8BF18DA1622A8B
{
public:
	// Org.BouncyCastle.Asn1.DerBitString Org.BouncyCastle.Asn1.X9.DHValidationParms::seed
	DerBitString_tCA371BCECC9BA7643D0127424F54EE10C1B19A64 * ___seed_0;
	// Org.BouncyCastle.Asn1.DerInteger Org.BouncyCastle.Asn1.X9.DHValidationParms::pgenCounter
	DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A * ___pgenCounter_1;

public:
	inline static int32_t get_offset_of_seed_0() { return static_cast<int32_t>(offsetof(DHValidationParms_t8121E846FBD95D655031A424C5AECD365F767E34, ___seed_0)); }
	inline DerBitString_tCA371BCECC9BA7643D0127424F54EE10C1B19A64 * get_seed_0() const { return ___seed_0; }
	inline DerBitString_tCA371BCECC9BA7643D0127424F54EE10C1B19A64 ** get_address_of_seed_0() { return &___seed_0; }
	inline void set_seed_0(DerBitString_tCA371BCECC9BA7643D0127424F54EE10C1B19A64 * value)
	{
		___seed_0 = value;
		Il2CppCodeGenWriteBarrier((&___seed_0), value);
	}

	inline static int32_t get_offset_of_pgenCounter_1() { return static_cast<int32_t>(offsetof(DHValidationParms_t8121E846FBD95D655031A424C5AECD365F767E34, ___pgenCounter_1)); }
	inline DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A * get_pgenCounter_1() const { return ___pgenCounter_1; }
	inline DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A ** get_address_of_pgenCounter_1() { return &___pgenCounter_1; }
	inline void set_pgenCounter_1(DerInteger_tAFB56211E7C3C0E309C4E109687773710F2B227A * value)
	{
		___pgenCounter_1 = value;
		Il2CppCodeGenWriteBarrier((&___pgenCounter_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DHVALIDATIONPARMS_T8121E846FBD95D655031A424C5AECD365F767E34_H
#ifndef C2PNB163V1HOLDER_TF5B7D56693BDB71BE195BFE14975620CD4B60144_H
#define C2PNB163V1HOLDER_TF5B7D56693BDB71BE195BFE14975620CD4B60144_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.X9.X962NamedCurves_C2pnb163v1Holder
struct  C2pnb163v1Holder_tF5B7D56693BDB71BE195BFE14975620CD4B60144  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct C2pnb163v1Holder_tF5B7D56693BDB71BE195BFE14975620CD4B60144_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Asn1.X9.X962NamedCurves_C2pnb163v1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(C2pnb163v1Holder_tF5B7D56693BDB71BE195BFE14975620CD4B60144_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // C2PNB163V1HOLDER_TF5B7D56693BDB71BE195BFE14975620CD4B60144_H
#ifndef C2PNB163V2HOLDER_T04F5B069D18CA40B24A16F3AD10E202B559216E9_H
#define C2PNB163V2HOLDER_T04F5B069D18CA40B24A16F3AD10E202B559216E9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.X9.X962NamedCurves_C2pnb163v2Holder
struct  C2pnb163v2Holder_t04F5B069D18CA40B24A16F3AD10E202B559216E9  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct C2pnb163v2Holder_t04F5B069D18CA40B24A16F3AD10E202B559216E9_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Asn1.X9.X962NamedCurves_C2pnb163v2Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(C2pnb163v2Holder_t04F5B069D18CA40B24A16F3AD10E202B559216E9_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // C2PNB163V2HOLDER_T04F5B069D18CA40B24A16F3AD10E202B559216E9_H
#ifndef C2PNB163V3HOLDER_TAD7A38AA45BC75741AFC9599709E7F6CA8191C39_H
#define C2PNB163V3HOLDER_TAD7A38AA45BC75741AFC9599709E7F6CA8191C39_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.X9.X962NamedCurves_C2pnb163v3Holder
struct  C2pnb163v3Holder_tAD7A38AA45BC75741AFC9599709E7F6CA8191C39  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct C2pnb163v3Holder_tAD7A38AA45BC75741AFC9599709E7F6CA8191C39_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Asn1.X9.X962NamedCurves_C2pnb163v3Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(C2pnb163v3Holder_tAD7A38AA45BC75741AFC9599709E7F6CA8191C39_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // C2PNB163V3HOLDER_TAD7A38AA45BC75741AFC9599709E7F6CA8191C39_H
#ifndef C2PNB176W1HOLDER_T059096933FE8BC6A05C2B4CEF77DB412E0824D9B_H
#define C2PNB176W1HOLDER_T059096933FE8BC6A05C2B4CEF77DB412E0824D9B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.X9.X962NamedCurves_C2pnb176w1Holder
struct  C2pnb176w1Holder_t059096933FE8BC6A05C2B4CEF77DB412E0824D9B  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct C2pnb176w1Holder_t059096933FE8BC6A05C2B4CEF77DB412E0824D9B_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Asn1.X9.X962NamedCurves_C2pnb176w1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(C2pnb176w1Holder_t059096933FE8BC6A05C2B4CEF77DB412E0824D9B_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // C2PNB176W1HOLDER_T059096933FE8BC6A05C2B4CEF77DB412E0824D9B_H
#ifndef C2PNB208W1HOLDER_T23D2554ED2D2EB475F88254BB131480E0ACB1846_H
#define C2PNB208W1HOLDER_T23D2554ED2D2EB475F88254BB131480E0ACB1846_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.X9.X962NamedCurves_C2pnb208w1Holder
struct  C2pnb208w1Holder_t23D2554ED2D2EB475F88254BB131480E0ACB1846  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct C2pnb208w1Holder_t23D2554ED2D2EB475F88254BB131480E0ACB1846_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Asn1.X9.X962NamedCurves_C2pnb208w1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(C2pnb208w1Holder_t23D2554ED2D2EB475F88254BB131480E0ACB1846_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // C2PNB208W1HOLDER_T23D2554ED2D2EB475F88254BB131480E0ACB1846_H
#ifndef C2PNB272W1HOLDER_T19A534B555E0797174E8D382F79F893177B28658_H
#define C2PNB272W1HOLDER_T19A534B555E0797174E8D382F79F893177B28658_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.X9.X962NamedCurves_C2pnb272w1Holder
struct  C2pnb272w1Holder_t19A534B555E0797174E8D382F79F893177B28658  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct C2pnb272w1Holder_t19A534B555E0797174E8D382F79F893177B28658_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Asn1.X9.X962NamedCurves_C2pnb272w1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(C2pnb272w1Holder_t19A534B555E0797174E8D382F79F893177B28658_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // C2PNB272W1HOLDER_T19A534B555E0797174E8D382F79F893177B28658_H
#ifndef C2TNB191V1HOLDER_TF9B7D41ED4A9582A7932599824825F1008EF66CC_H
#define C2TNB191V1HOLDER_TF9B7D41ED4A9582A7932599824825F1008EF66CC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.X9.X962NamedCurves_C2tnb191v1Holder
struct  C2tnb191v1Holder_tF9B7D41ED4A9582A7932599824825F1008EF66CC  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct C2tnb191v1Holder_tF9B7D41ED4A9582A7932599824825F1008EF66CC_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Asn1.X9.X962NamedCurves_C2tnb191v1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(C2tnb191v1Holder_tF9B7D41ED4A9582A7932599824825F1008EF66CC_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // C2TNB191V1HOLDER_TF9B7D41ED4A9582A7932599824825F1008EF66CC_H
#ifndef C2TNB191V2HOLDER_T64E7B8D81A8513F0C247D4B7DC30867AAC555DEA_H
#define C2TNB191V2HOLDER_T64E7B8D81A8513F0C247D4B7DC30867AAC555DEA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.X9.X962NamedCurves_C2tnb191v2Holder
struct  C2tnb191v2Holder_t64E7B8D81A8513F0C247D4B7DC30867AAC555DEA  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct C2tnb191v2Holder_t64E7B8D81A8513F0C247D4B7DC30867AAC555DEA_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Asn1.X9.X962NamedCurves_C2tnb191v2Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(C2tnb191v2Holder_t64E7B8D81A8513F0C247D4B7DC30867AAC555DEA_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // C2TNB191V2HOLDER_T64E7B8D81A8513F0C247D4B7DC30867AAC555DEA_H
#ifndef C2TNB191V3HOLDER_TB4F76AFEE78EBFE755694A0A651120302A0ACFCC_H
#define C2TNB191V3HOLDER_TB4F76AFEE78EBFE755694A0A651120302A0ACFCC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.X9.X962NamedCurves_C2tnb191v3Holder
struct  C2tnb191v3Holder_tB4F76AFEE78EBFE755694A0A651120302A0ACFCC  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct C2tnb191v3Holder_tB4F76AFEE78EBFE755694A0A651120302A0ACFCC_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Asn1.X9.X962NamedCurves_C2tnb191v3Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(C2tnb191v3Holder_tB4F76AFEE78EBFE755694A0A651120302A0ACFCC_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // C2TNB191V3HOLDER_TB4F76AFEE78EBFE755694A0A651120302A0ACFCC_H
#ifndef C2TNB239V1HOLDER_TF33876C71DB5BB3455BF9D5116948A52A179C8F9_H
#define C2TNB239V1HOLDER_TF33876C71DB5BB3455BF9D5116948A52A179C8F9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.X9.X962NamedCurves_C2tnb239v1Holder
struct  C2tnb239v1Holder_tF33876C71DB5BB3455BF9D5116948A52A179C8F9  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct C2tnb239v1Holder_tF33876C71DB5BB3455BF9D5116948A52A179C8F9_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Asn1.X9.X962NamedCurves_C2tnb239v1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(C2tnb239v1Holder_tF33876C71DB5BB3455BF9D5116948A52A179C8F9_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // C2TNB239V1HOLDER_TF33876C71DB5BB3455BF9D5116948A52A179C8F9_H
#ifndef C2TNB239V2HOLDER_T572C9CCFFDDCB764D14C758E0A6AFD1E90D65779_H
#define C2TNB239V2HOLDER_T572C9CCFFDDCB764D14C758E0A6AFD1E90D65779_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.X9.X962NamedCurves_C2tnb239v2Holder
struct  C2tnb239v2Holder_t572C9CCFFDDCB764D14C758E0A6AFD1E90D65779  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct C2tnb239v2Holder_t572C9CCFFDDCB764D14C758E0A6AFD1E90D65779_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Asn1.X9.X962NamedCurves_C2tnb239v2Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(C2tnb239v2Holder_t572C9CCFFDDCB764D14C758E0A6AFD1E90D65779_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // C2TNB239V2HOLDER_T572C9CCFFDDCB764D14C758E0A6AFD1E90D65779_H
#ifndef C2TNB239V3HOLDER_T74D9672D126B3F8E74B4820FAD111FC9E4B2C4A5_H
#define C2TNB239V3HOLDER_T74D9672D126B3F8E74B4820FAD111FC9E4B2C4A5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.X9.X962NamedCurves_C2tnb239v3Holder
struct  C2tnb239v3Holder_t74D9672D126B3F8E74B4820FAD111FC9E4B2C4A5  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct C2tnb239v3Holder_t74D9672D126B3F8E74B4820FAD111FC9E4B2C4A5_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Asn1.X9.X962NamedCurves_C2tnb239v3Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(C2tnb239v3Holder_t74D9672D126B3F8E74B4820FAD111FC9E4B2C4A5_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // C2TNB239V3HOLDER_T74D9672D126B3F8E74B4820FAD111FC9E4B2C4A5_H
#ifndef PRIME192V1HOLDER_TCC8F0D1A3AB0579680C9312DD08E0E0F6C4BD673_H
#define PRIME192V1HOLDER_TCC8F0D1A3AB0579680C9312DD08E0E0F6C4BD673_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.X9.X962NamedCurves_Prime192v1Holder
struct  Prime192v1Holder_tCC8F0D1A3AB0579680C9312DD08E0E0F6C4BD673  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct Prime192v1Holder_tCC8F0D1A3AB0579680C9312DD08E0E0F6C4BD673_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Asn1.X9.X962NamedCurves_Prime192v1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Prime192v1Holder_tCC8F0D1A3AB0579680C9312DD08E0E0F6C4BD673_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRIME192V1HOLDER_TCC8F0D1A3AB0579680C9312DD08E0E0F6C4BD673_H
#ifndef PRIME192V2HOLDER_T653A143572A0315D80625BA8FA78FC46F74DF0E7_H
#define PRIME192V2HOLDER_T653A143572A0315D80625BA8FA78FC46F74DF0E7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.X9.X962NamedCurves_Prime192v2Holder
struct  Prime192v2Holder_t653A143572A0315D80625BA8FA78FC46F74DF0E7  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct Prime192v2Holder_t653A143572A0315D80625BA8FA78FC46F74DF0E7_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Asn1.X9.X962NamedCurves_Prime192v2Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Prime192v2Holder_t653A143572A0315D80625BA8FA78FC46F74DF0E7_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRIME192V2HOLDER_T653A143572A0315D80625BA8FA78FC46F74DF0E7_H
#ifndef PRIME192V3HOLDER_TF67FD19C286C5531B2DE2D96FFC7B58632D3152D_H
#define PRIME192V3HOLDER_TF67FD19C286C5531B2DE2D96FFC7B58632D3152D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.X9.X962NamedCurves_Prime192v3Holder
struct  Prime192v3Holder_tF67FD19C286C5531B2DE2D96FFC7B58632D3152D  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct Prime192v3Holder_tF67FD19C286C5531B2DE2D96FFC7B58632D3152D_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Asn1.X9.X962NamedCurves_Prime192v3Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Prime192v3Holder_tF67FD19C286C5531B2DE2D96FFC7B58632D3152D_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRIME192V3HOLDER_TF67FD19C286C5531B2DE2D96FFC7B58632D3152D_H
#ifndef PRIME239V1HOLDER_TBDB0E3D4C175164F93C36E48744A5F04C044CFE4_H
#define PRIME239V1HOLDER_TBDB0E3D4C175164F93C36E48744A5F04C044CFE4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.X9.X962NamedCurves_Prime239v1Holder
struct  Prime239v1Holder_tBDB0E3D4C175164F93C36E48744A5F04C044CFE4  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct Prime239v1Holder_tBDB0E3D4C175164F93C36E48744A5F04C044CFE4_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Asn1.X9.X962NamedCurves_Prime239v1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Prime239v1Holder_tBDB0E3D4C175164F93C36E48744A5F04C044CFE4_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRIME239V1HOLDER_TBDB0E3D4C175164F93C36E48744A5F04C044CFE4_H
#ifndef PRIME239V2HOLDER_T028E299B68F7F12751EB22C6F9A6ECD3A353FAD1_H
#define PRIME239V2HOLDER_T028E299B68F7F12751EB22C6F9A6ECD3A353FAD1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.X9.X962NamedCurves_Prime239v2Holder
struct  Prime239v2Holder_t028E299B68F7F12751EB22C6F9A6ECD3A353FAD1  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct Prime239v2Holder_t028E299B68F7F12751EB22C6F9A6ECD3A353FAD1_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Asn1.X9.X962NamedCurves_Prime239v2Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Prime239v2Holder_t028E299B68F7F12751EB22C6F9A6ECD3A353FAD1_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRIME239V2HOLDER_T028E299B68F7F12751EB22C6F9A6ECD3A353FAD1_H
#ifndef PRIME239V3HOLDER_TE7165C4CC814A6D9A72B3350432C2A43A4DEEAA1_H
#define PRIME239V3HOLDER_TE7165C4CC814A6D9A72B3350432C2A43A4DEEAA1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.X9.X962NamedCurves_Prime239v3Holder
struct  Prime239v3Holder_tE7165C4CC814A6D9A72B3350432C2A43A4DEEAA1  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct Prime239v3Holder_tE7165C4CC814A6D9A72B3350432C2A43A4DEEAA1_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Asn1.X9.X962NamedCurves_Prime239v3Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Prime239v3Holder_tE7165C4CC814A6D9A72B3350432C2A43A4DEEAA1_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRIME239V3HOLDER_TE7165C4CC814A6D9A72B3350432C2A43A4DEEAA1_H
#ifndef PRIME256V1HOLDER_TE8FB8FB3A61FC9C3DD02DC0B88E2BAB23C1FEE61_H
#define PRIME256V1HOLDER_TE8FB8FB3A61FC9C3DD02DC0B88E2BAB23C1FEE61_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.X9.X962NamedCurves_Prime256v1Holder
struct  Prime256v1Holder_tE8FB8FB3A61FC9C3DD02DC0B88E2BAB23C1FEE61  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct Prime256v1Holder_tE8FB8FB3A61FC9C3DD02DC0B88E2BAB23C1FEE61_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Asn1.X9.X962NamedCurves_Prime256v1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Prime256v1Holder_tE8FB8FB3A61FC9C3DD02DC0B88E2BAB23C1FEE61_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRIME256V1HOLDER_TE8FB8FB3A61FC9C3DD02DC0B88E2BAB23C1FEE61_H
#ifndef DERENUMERATED_TA6A732BA209AA4B10E55871BB22A8ACC7CFBCEC0_H
#define DERENUMERATED_TA6A732BA209AA4B10E55871BB22A8ACC7CFBCEC0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.DerEnumerated
struct  DerEnumerated_tA6A732BA209AA4B10E55871BB22A8ACC7CFBCEC0  : public Asn1Object_tB780E50883209D5C975E83B3D16257DFC3CCE36D
{
public:
	// System.Byte[] Org.BouncyCastle.Asn1.DerEnumerated::bytes
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___bytes_0;

public:
	inline static int32_t get_offset_of_bytes_0() { return static_cast<int32_t>(offsetof(DerEnumerated_tA6A732BA209AA4B10E55871BB22A8ACC7CFBCEC0, ___bytes_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_bytes_0() const { return ___bytes_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_bytes_0() { return &___bytes_0; }
	inline void set_bytes_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___bytes_0 = value;
		Il2CppCodeGenWriteBarrier((&___bytes_0), value);
	}
};

struct DerEnumerated_tA6A732BA209AA4B10E55871BB22A8ACC7CFBCEC0_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.DerEnumerated[] Org.BouncyCastle.Asn1.DerEnumerated::cache
	DerEnumeratedU5BU5D_tEBACB932DA336D53CCA9EEA8D863431522CD024D* ___cache_1;

public:
	inline static int32_t get_offset_of_cache_1() { return static_cast<int32_t>(offsetof(DerEnumerated_tA6A732BA209AA4B10E55871BB22A8ACC7CFBCEC0_StaticFields, ___cache_1)); }
	inline DerEnumeratedU5BU5D_tEBACB932DA336D53CCA9EEA8D863431522CD024D* get_cache_1() const { return ___cache_1; }
	inline DerEnumeratedU5BU5D_tEBACB932DA336D53CCA9EEA8D863431522CD024D** get_address_of_cache_1() { return &___cache_1; }
	inline void set_cache_1(DerEnumeratedU5BU5D_tEBACB932DA336D53CCA9EEA8D863431522CD024D* value)
	{
		___cache_1 = value;
		Il2CppCodeGenWriteBarrier((&___cache_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERENUMERATED_TA6A732BA209AA4B10E55871BB22A8ACC7CFBCEC0_H
#ifndef DERSTRINGBASE_T56EF0CDEB7AF4C8CE9701F3940380FF0EAC26A01_H
#define DERSTRINGBASE_T56EF0CDEB7AF4C8CE9701F3940380FF0EAC26A01_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.DerStringBase
struct  DerStringBase_t56EF0CDEB7AF4C8CE9701F3940380FF0EAC26A01  : public Asn1Object_tB780E50883209D5C975E83B3D16257DFC3CCE36D
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERSTRINGBASE_T56EF0CDEB7AF4C8CE9701F3940380FF0EAC26A01_H
#ifndef DERBITSTRING_TCA371BCECC9BA7643D0127424F54EE10C1B19A64_H
#define DERBITSTRING_TCA371BCECC9BA7643D0127424F54EE10C1B19A64_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.DerBitString
struct  DerBitString_tCA371BCECC9BA7643D0127424F54EE10C1B19A64  : public DerStringBase_t56EF0CDEB7AF4C8CE9701F3940380FF0EAC26A01
{
public:
	// System.Byte[] Org.BouncyCastle.Asn1.DerBitString::mData
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mData_1;
	// System.Int32 Org.BouncyCastle.Asn1.DerBitString::mPadBits
	int32_t ___mPadBits_2;

public:
	inline static int32_t get_offset_of_mData_1() { return static_cast<int32_t>(offsetof(DerBitString_tCA371BCECC9BA7643D0127424F54EE10C1B19A64, ___mData_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mData_1() const { return ___mData_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mData_1() { return &___mData_1; }
	inline void set_mData_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mData_1 = value;
		Il2CppCodeGenWriteBarrier((&___mData_1), value);
	}

	inline static int32_t get_offset_of_mPadBits_2() { return static_cast<int32_t>(offsetof(DerBitString_tCA371BCECC9BA7643D0127424F54EE10C1B19A64, ___mPadBits_2)); }
	inline int32_t get_mPadBits_2() const { return ___mPadBits_2; }
	inline int32_t* get_address_of_mPadBits_2() { return &___mPadBits_2; }
	inline void set_mPadBits_2(int32_t value)
	{
		___mPadBits_2 = value;
	}
};

struct DerBitString_tCA371BCECC9BA7643D0127424F54EE10C1B19A64_StaticFields
{
public:
	// System.Char[] Org.BouncyCastle.Asn1.DerBitString::table
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___table_0;

public:
	inline static int32_t get_offset_of_table_0() { return static_cast<int32_t>(offsetof(DerBitString_tCA371BCECC9BA7643D0127424F54EE10C1B19A64_StaticFields, ___table_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_table_0() const { return ___table_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_table_0() { return &___table_0; }
	inline void set_table_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___table_0 = value;
		Il2CppCodeGenWriteBarrier((&___table_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERBITSTRING_TCA371BCECC9BA7643D0127424F54EE10C1B19A64_H
#ifndef OCSPRESPONSESTATUS_T40E3D3B60B45A3E985B8678DCD3E60445517912F_H
#define OCSPRESPONSESTATUS_T40E3D3B60B45A3E985B8678DCD3E60445517912F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.Ocsp.OcspResponseStatus
struct  OcspResponseStatus_t40E3D3B60B45A3E985B8678DCD3E60445517912F  : public DerEnumerated_tA6A732BA209AA4B10E55871BB22A8ACC7CFBCEC0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OCSPRESPONSESTATUS_T40E3D3B60B45A3E985B8678DCD3E60445517912F_H
#ifndef KEYUSAGE_TC2C79804034CEA4E31409EEF3C2A7A8660A6008D_H
#define KEYUSAGE_TC2C79804034CEA4E31409EEF3C2A7A8660A6008D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.X509.KeyUsage
struct  KeyUsage_tC2C79804034CEA4E31409EEF3C2A7A8660A6008D  : public DerBitString_tCA371BCECC9BA7643D0127424F54EE10C1B19A64
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYUSAGE_TC2C79804034CEA4E31409EEF3C2A7A8660A6008D_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4900 = { sizeof (OcspResponse_tF9B1DEA63B2DD9043941E80847BDF860FD518211), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4900[2] = 
{
	OcspResponse_tF9B1DEA63B2DD9043941E80847BDF860FD518211::get_offset_of_responseStatus_0(),
	OcspResponse_tF9B1DEA63B2DD9043941E80847BDF860FD518211::get_offset_of_responseBytes_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4901 = { sizeof (OcspResponseStatus_t40E3D3B60B45A3E985B8678DCD3E60445517912F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4902 = { sizeof (ResponseBytes_t0E85F6E803AB354A78B7EBABB885F182805BAE25), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4902[2] = 
{
	ResponseBytes_t0E85F6E803AB354A78B7EBABB885F182805BAE25::get_offset_of_responseType_0(),
	ResponseBytes_t0E85F6E803AB354A78B7EBABB885F182805BAE25::get_offset_of_response_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4903 = { sizeof (ElGamalParameter_t81ED5105E6B28F0030F87FD61C4474CA463D2391), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4903[2] = 
{
	ElGamalParameter_t81ED5105E6B28F0030F87FD61C4474CA463D2391::get_offset_of_p_0(),
	ElGamalParameter_t81ED5105E6B28F0030F87FD61C4474CA463D2391::get_offset_of_g_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4904 = { sizeof (OiwObjectIdentifiers_tC11A2DA17697C1B77A133921FED0B4C55F603DFE), -1, sizeof(OiwObjectIdentifiers_tC11A2DA17697C1B77A133921FED0B4C55F603DFE_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4904[12] = 
{
	OiwObjectIdentifiers_tC11A2DA17697C1B77A133921FED0B4C55F603DFE_StaticFields::get_offset_of_MD4WithRsa_0(),
	OiwObjectIdentifiers_tC11A2DA17697C1B77A133921FED0B4C55F603DFE_StaticFields::get_offset_of_MD5WithRsa_1(),
	OiwObjectIdentifiers_tC11A2DA17697C1B77A133921FED0B4C55F603DFE_StaticFields::get_offset_of_MD4WithRsaEncryption_2(),
	OiwObjectIdentifiers_tC11A2DA17697C1B77A133921FED0B4C55F603DFE_StaticFields::get_offset_of_DesEcb_3(),
	OiwObjectIdentifiers_tC11A2DA17697C1B77A133921FED0B4C55F603DFE_StaticFields::get_offset_of_DesCbc_4(),
	OiwObjectIdentifiers_tC11A2DA17697C1B77A133921FED0B4C55F603DFE_StaticFields::get_offset_of_DesOfb_5(),
	OiwObjectIdentifiers_tC11A2DA17697C1B77A133921FED0B4C55F603DFE_StaticFields::get_offset_of_DesCfb_6(),
	OiwObjectIdentifiers_tC11A2DA17697C1B77A133921FED0B4C55F603DFE_StaticFields::get_offset_of_DesEde_7(),
	OiwObjectIdentifiers_tC11A2DA17697C1B77A133921FED0B4C55F603DFE_StaticFields::get_offset_of_IdSha1_8(),
	OiwObjectIdentifiers_tC11A2DA17697C1B77A133921FED0B4C55F603DFE_StaticFields::get_offset_of_DsaWithSha1_9(),
	OiwObjectIdentifiers_tC11A2DA17697C1B77A133921FED0B4C55F603DFE_StaticFields::get_offset_of_Sha1WithRsa_10(),
	OiwObjectIdentifiers_tC11A2DA17697C1B77A133921FED0B4C55F603DFE_StaticFields::get_offset_of_ElGamalAlgorithm_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4905 = { sizeof (ContentInfo_t97148D828054F7B5E1586B21837C138F9EDE4C08), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4905[2] = 
{
	ContentInfo_t97148D828054F7B5E1586B21837C138F9EDE4C08::get_offset_of_contentType_0(),
	ContentInfo_t97148D828054F7B5E1586B21837C138F9EDE4C08::get_offset_of_content_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4906 = { sizeof (DHParameter_tB37DF4F0CA6CEDE5DE4F5B50619E3FD4ED63A106), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4906[3] = 
{
	DHParameter_tB37DF4F0CA6CEDE5DE4F5B50619E3FD4ED63A106::get_offset_of_p_0(),
	DHParameter_tB37DF4F0CA6CEDE5DE4F5B50619E3FD4ED63A106::get_offset_of_g_1(),
	DHParameter_tB37DF4F0CA6CEDE5DE4F5B50619E3FD4ED63A106::get_offset_of_l_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4907 = { sizeof (PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04), -1, sizeof(PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4907[119] = 
{
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_RsaEncryption_0(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_MD2WithRsaEncryption_1(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_MD4WithRsaEncryption_2(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_MD5WithRsaEncryption_3(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_Sha1WithRsaEncryption_4(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_SrsaOaepEncryptionSet_5(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_IdRsaesOaep_6(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_IdMgf1_7(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_IdPSpecified_8(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_IdRsassaPss_9(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_Sha256WithRsaEncryption_10(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_Sha384WithRsaEncryption_11(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_Sha512WithRsaEncryption_12(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_Sha224WithRsaEncryption_13(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_DhKeyAgreement_14(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_PbeWithMD2AndDesCbc_15(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_PbeWithMD2AndRC2Cbc_16(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_PbeWithMD5AndDesCbc_17(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_PbeWithMD5AndRC2Cbc_18(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_PbeWithSha1AndDesCbc_19(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_PbeWithSha1AndRC2Cbc_20(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_IdPbeS2_21(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_IdPbkdf2_22(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_DesEde3Cbc_23(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_RC2Cbc_24(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_MD2_25(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_MD4_26(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_MD5_27(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_IdHmacWithSha1_28(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_IdHmacWithSha224_29(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_IdHmacWithSha256_30(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_IdHmacWithSha384_31(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_IdHmacWithSha512_32(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_Data_33(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_SignedData_34(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_EnvelopedData_35(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_SignedAndEnvelopedData_36(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_DigestedData_37(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_EncryptedData_38(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_Pkcs9AtEmailAddress_39(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_Pkcs9AtUnstructuredName_40(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_Pkcs9AtContentType_41(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_Pkcs9AtMessageDigest_42(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_Pkcs9AtSigningTime_43(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_Pkcs9AtCounterSignature_44(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_Pkcs9AtChallengePassword_45(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_Pkcs9AtUnstructuredAddress_46(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_Pkcs9AtExtendedCertificateAttributes_47(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_Pkcs9AtSigningDescription_48(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_Pkcs9AtExtensionRequest_49(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_Pkcs9AtSmimeCapabilities_50(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_IdSmime_51(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_Pkcs9AtFriendlyName_52(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_Pkcs9AtLocalKeyID_53(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_X509CertType_54(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_X509Certificate_55(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_SdsiCertificate_56(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_X509Crl_57(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_IdAlg_58(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_IdAlgEsdh_59(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_IdAlgCms3DesWrap_60(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_IdAlgCmsRC2Wrap_61(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_IdAlgPwriKek_62(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_IdAlgSsdh_63(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_IdRsaKem_64(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_PreferSignedData_65(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_CannotDecryptAny_66(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_SmimeCapabilitiesVersions_67(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_IdAAReceiptRequest_68(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_IdCTAuthData_69(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_IdCTTstInfo_70(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_IdCTCompressedData_71(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_IdCTAuthEnvelopedData_72(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_IdCTTimestampedData_73(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_IdCtiEtsProofOfOrigin_74(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_IdCtiEtsProofOfReceipt_75(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_IdCtiEtsProofOfDelivery_76(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_IdCtiEtsProofOfSender_77(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_IdCtiEtsProofOfApproval_78(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_IdCtiEtsProofOfCreation_79(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_IdAAContentHint_80(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_IdAAMsgSigDigest_81(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_IdAAContentReference_82(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_IdAAEncrypKeyPref_83(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_IdAASigningCertificate_84(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_IdAASigningCertificateV2_85(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_IdAAContentIdentifier_86(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_IdAASignatureTimeStampToken_87(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_IdAAEtsSigPolicyID_88(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_IdAAEtsCommitmentType_89(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_IdAAEtsSignerLocation_90(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_IdAAEtsSignerAttr_91(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_IdAAEtsOtherSigCert_92(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_IdAAEtsContentTimestamp_93(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_IdAAEtsCertificateRefs_94(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_IdAAEtsRevocationRefs_95(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_IdAAEtsCertValues_96(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_IdAAEtsRevocationValues_97(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_IdAAEtsEscTimeStamp_98(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_IdAAEtsCertCrlTimestamp_99(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_IdAAEtsArchiveTimestamp_100(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_IdAASigPolicyID_101(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_IdAACommitmentType_102(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_IdAASignerLocation_103(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_IdAAOtherSigCert_104(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_IdSpqEtsUri_105(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_IdSpqEtsUNotice_106(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_KeyBag_107(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_Pkcs8ShroudedKeyBag_108(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_CertBag_109(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_CrlBag_110(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_SecretBag_111(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_SafeContentsBag_112(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_PbeWithShaAnd128BitRC4_113(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_PbeWithShaAnd40BitRC4_114(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_PbeWithShaAnd3KeyTripleDesCbc_115(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_PbeWithShaAnd2KeyTripleDesCbc_116(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_PbeWithShaAnd128BitRC2Cbc_117(),
	PkcsObjectIdentifiers_t56CE1A1A1C18551C5ECB5DF38F705B6C15890F04_StaticFields::get_offset_of_PbewithShaAnd40BitRC2Cbc_118(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4908 = { sizeof (SignedData_tC530B1ACD0C32802BB3EFE180B486D0564B30129), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4908[6] = 
{
	SignedData_tC530B1ACD0C32802BB3EFE180B486D0564B30129::get_offset_of_version_0(),
	SignedData_tC530B1ACD0C32802BB3EFE180B486D0564B30129::get_offset_of_digestAlgorithms_1(),
	SignedData_tC530B1ACD0C32802BB3EFE180B486D0564B30129::get_offset_of_contentInfo_2(),
	SignedData_tC530B1ACD0C32802BB3EFE180B486D0564B30129::get_offset_of_certificates_3(),
	SignedData_tC530B1ACD0C32802BB3EFE180B486D0564B30129::get_offset_of_crls_4(),
	SignedData_tC530B1ACD0C32802BB3EFE180B486D0564B30129::get_offset_of_signerInfos_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4909 = { sizeof (SecNamedCurves_t2F8AEB8DA36D78A58FB27FB732FF1962C5DAF25D), -1, sizeof(SecNamedCurves_t2F8AEB8DA36D78A58FB27FB732FF1962C5DAF25D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4909[3] = 
{
	SecNamedCurves_t2F8AEB8DA36D78A58FB27FB732FF1962C5DAF25D_StaticFields::get_offset_of_objIds_0(),
	SecNamedCurves_t2F8AEB8DA36D78A58FB27FB732FF1962C5DAF25D_StaticFields::get_offset_of_curves_1(),
	SecNamedCurves_t2F8AEB8DA36D78A58FB27FB732FF1962C5DAF25D_StaticFields::get_offset_of_names_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4910 = { sizeof (Secp112r1Holder_tC54B4FC67BFD8E33467AE24AD87AA2A10B23364E), -1, sizeof(Secp112r1Holder_tC54B4FC67BFD8E33467AE24AD87AA2A10B23364E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4910[1] = 
{
	Secp112r1Holder_tC54B4FC67BFD8E33467AE24AD87AA2A10B23364E_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4911 = { sizeof (Secp112r2Holder_t4C7868E8D8131BC693C6C66E71A0B1DCFB506995), -1, sizeof(Secp112r2Holder_t4C7868E8D8131BC693C6C66E71A0B1DCFB506995_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4911[1] = 
{
	Secp112r2Holder_t4C7868E8D8131BC693C6C66E71A0B1DCFB506995_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4912 = { sizeof (Secp128r1Holder_t65B90262D1B9E503E102526BF86F7D7D0F16D83C), -1, sizeof(Secp128r1Holder_t65B90262D1B9E503E102526BF86F7D7D0F16D83C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4912[1] = 
{
	Secp128r1Holder_t65B90262D1B9E503E102526BF86F7D7D0F16D83C_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4913 = { sizeof (Secp128r2Holder_t78AC4293F68BED8EBB3BBF27AC237E6A8C7DEC33), -1, sizeof(Secp128r2Holder_t78AC4293F68BED8EBB3BBF27AC237E6A8C7DEC33_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4913[1] = 
{
	Secp128r2Holder_t78AC4293F68BED8EBB3BBF27AC237E6A8C7DEC33_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4914 = { sizeof (Secp160k1Holder_t0B6014424F493DAE1FC6EE5EACFA999DC681AD7C), -1, sizeof(Secp160k1Holder_t0B6014424F493DAE1FC6EE5EACFA999DC681AD7C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4914[1] = 
{
	Secp160k1Holder_t0B6014424F493DAE1FC6EE5EACFA999DC681AD7C_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4915 = { sizeof (Secp160r1Holder_t55D82C82E8F55749FF82BC9BEC6D0399586EE3E5), -1, sizeof(Secp160r1Holder_t55D82C82E8F55749FF82BC9BEC6D0399586EE3E5_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4915[1] = 
{
	Secp160r1Holder_t55D82C82E8F55749FF82BC9BEC6D0399586EE3E5_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4916 = { sizeof (Secp160r2Holder_t116ACDAC724FA69EAFF45AA9269854FA1297F55F), -1, sizeof(Secp160r2Holder_t116ACDAC724FA69EAFF45AA9269854FA1297F55F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4916[1] = 
{
	Secp160r2Holder_t116ACDAC724FA69EAFF45AA9269854FA1297F55F_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4917 = { sizeof (Secp192k1Holder_t0A75771288CF0EAE385C36B9C2076CD3672B4D25), -1, sizeof(Secp192k1Holder_t0A75771288CF0EAE385C36B9C2076CD3672B4D25_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4917[1] = 
{
	Secp192k1Holder_t0A75771288CF0EAE385C36B9C2076CD3672B4D25_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4918 = { sizeof (Secp192r1Holder_t420112C8CEE5161975553EE6F42A3831F36E042B), -1, sizeof(Secp192r1Holder_t420112C8CEE5161975553EE6F42A3831F36E042B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4918[1] = 
{
	Secp192r1Holder_t420112C8CEE5161975553EE6F42A3831F36E042B_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4919 = { sizeof (Secp224k1Holder_t60397E0A6FDB5CC54E6A9004BE41522F7961A42F), -1, sizeof(Secp224k1Holder_t60397E0A6FDB5CC54E6A9004BE41522F7961A42F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4919[1] = 
{
	Secp224k1Holder_t60397E0A6FDB5CC54E6A9004BE41522F7961A42F_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4920 = { sizeof (Secp224r1Holder_t500592A02B46F69885B3C68F403EDC8D768E2D78), -1, sizeof(Secp224r1Holder_t500592A02B46F69885B3C68F403EDC8D768E2D78_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4920[1] = 
{
	Secp224r1Holder_t500592A02B46F69885B3C68F403EDC8D768E2D78_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4921 = { sizeof (Secp256k1Holder_t8FD3B27002A5E01009A0C96C366740CC7E26EAC7), -1, sizeof(Secp256k1Holder_t8FD3B27002A5E01009A0C96C366740CC7E26EAC7_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4921[1] = 
{
	Secp256k1Holder_t8FD3B27002A5E01009A0C96C366740CC7E26EAC7_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4922 = { sizeof (Secp256r1Holder_t653A01D606FF539C6AD6DCC88FE1AF1235271F01), -1, sizeof(Secp256r1Holder_t653A01D606FF539C6AD6DCC88FE1AF1235271F01_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4922[1] = 
{
	Secp256r1Holder_t653A01D606FF539C6AD6DCC88FE1AF1235271F01_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4923 = { sizeof (Secp384r1Holder_t32CBCC2586FB975E9284C8BE25154CFEECF77802), -1, sizeof(Secp384r1Holder_t32CBCC2586FB975E9284C8BE25154CFEECF77802_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4923[1] = 
{
	Secp384r1Holder_t32CBCC2586FB975E9284C8BE25154CFEECF77802_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4924 = { sizeof (Secp521r1Holder_t14968318336105E30C47D25B899013C684CB14C3), -1, sizeof(Secp521r1Holder_t14968318336105E30C47D25B899013C684CB14C3_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4924[1] = 
{
	Secp521r1Holder_t14968318336105E30C47D25B899013C684CB14C3_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4925 = { sizeof (Sect113r1Holder_t8209BCD3276440FBE22CEA18086CD1B8EF44BA4D), -1, sizeof(Sect113r1Holder_t8209BCD3276440FBE22CEA18086CD1B8EF44BA4D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4925[1] = 
{
	Sect113r1Holder_t8209BCD3276440FBE22CEA18086CD1B8EF44BA4D_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4926 = { sizeof (Sect113r2Holder_t18F3A8ADC8D106BAC1BCD695007AF72992B413E7), -1, sizeof(Sect113r2Holder_t18F3A8ADC8D106BAC1BCD695007AF72992B413E7_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4926[1] = 
{
	Sect113r2Holder_t18F3A8ADC8D106BAC1BCD695007AF72992B413E7_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4927 = { sizeof (Sect131r1Holder_t570219AFED5F63CA2F7B004A6CAF1AC2581C4A48), -1, sizeof(Sect131r1Holder_t570219AFED5F63CA2F7B004A6CAF1AC2581C4A48_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4927[1] = 
{
	Sect131r1Holder_t570219AFED5F63CA2F7B004A6CAF1AC2581C4A48_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4928 = { sizeof (Sect131r2Holder_tF43364C6E9F7E4D94B0E483176EA7ECC7A153D6E), -1, sizeof(Sect131r2Holder_tF43364C6E9F7E4D94B0E483176EA7ECC7A153D6E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4928[1] = 
{
	Sect131r2Holder_tF43364C6E9F7E4D94B0E483176EA7ECC7A153D6E_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4929 = { sizeof (Sect163k1Holder_t35CEEE60F75E2743BD424DE4444D264CE34F8913), -1, sizeof(Sect163k1Holder_t35CEEE60F75E2743BD424DE4444D264CE34F8913_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4929[1] = 
{
	Sect163k1Holder_t35CEEE60F75E2743BD424DE4444D264CE34F8913_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4930 = { sizeof (Sect163r1Holder_t8509EAD889DB5CCBB450662E79208614E0A0A4D7), -1, sizeof(Sect163r1Holder_t8509EAD889DB5CCBB450662E79208614E0A0A4D7_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4930[1] = 
{
	Sect163r1Holder_t8509EAD889DB5CCBB450662E79208614E0A0A4D7_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4931 = { sizeof (Sect163r2Holder_tB77257EC78B3D41C7CCA55DA97C671C11571F639), -1, sizeof(Sect163r2Holder_tB77257EC78B3D41C7CCA55DA97C671C11571F639_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4931[1] = 
{
	Sect163r2Holder_tB77257EC78B3D41C7CCA55DA97C671C11571F639_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4932 = { sizeof (Sect193r1Holder_t9677FC2C8BB3CD55F3EF8EE28DDE9EEA581B6C19), -1, sizeof(Sect193r1Holder_t9677FC2C8BB3CD55F3EF8EE28DDE9EEA581B6C19_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4932[1] = 
{
	Sect193r1Holder_t9677FC2C8BB3CD55F3EF8EE28DDE9EEA581B6C19_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4933 = { sizeof (Sect193r2Holder_tA537581BA1C0F502E8078DF803078D9AF0878AEB), -1, sizeof(Sect193r2Holder_tA537581BA1C0F502E8078DF803078D9AF0878AEB_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4933[1] = 
{
	Sect193r2Holder_tA537581BA1C0F502E8078DF803078D9AF0878AEB_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4934 = { sizeof (Sect233k1Holder_tEFBB7AFB624A626C4C79771CE25672FBE219E967), -1, sizeof(Sect233k1Holder_tEFBB7AFB624A626C4C79771CE25672FBE219E967_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4934[1] = 
{
	Sect233k1Holder_tEFBB7AFB624A626C4C79771CE25672FBE219E967_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4935 = { sizeof (Sect233r1Holder_t2B0112E8612CA3BC9DDD876ED1823FBFF028AEE0), -1, sizeof(Sect233r1Holder_t2B0112E8612CA3BC9DDD876ED1823FBFF028AEE0_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4935[1] = 
{
	Sect233r1Holder_t2B0112E8612CA3BC9DDD876ED1823FBFF028AEE0_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4936 = { sizeof (Sect239k1Holder_t1089DC5B36A677DCCBCFB08AA93F32F46CFB01CA), -1, sizeof(Sect239k1Holder_t1089DC5B36A677DCCBCFB08AA93F32F46CFB01CA_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4936[1] = 
{
	Sect239k1Holder_t1089DC5B36A677DCCBCFB08AA93F32F46CFB01CA_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4937 = { sizeof (Sect283k1Holder_t3762D17EB2EB3377FD63D88DD3BAD73587598505), -1, sizeof(Sect283k1Holder_t3762D17EB2EB3377FD63D88DD3BAD73587598505_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4937[1] = 
{
	Sect283k1Holder_t3762D17EB2EB3377FD63D88DD3BAD73587598505_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4938 = { sizeof (Sect283r1Holder_t438A2E827E312C528D1FB550484FC4F680DDA5EF), -1, sizeof(Sect283r1Holder_t438A2E827E312C528D1FB550484FC4F680DDA5EF_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4938[1] = 
{
	Sect283r1Holder_t438A2E827E312C528D1FB550484FC4F680DDA5EF_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4939 = { sizeof (Sect409k1Holder_t07E69564F68EDAB038993C5BC3EBDDAA171BE0DD), -1, sizeof(Sect409k1Holder_t07E69564F68EDAB038993C5BC3EBDDAA171BE0DD_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4939[1] = 
{
	Sect409k1Holder_t07E69564F68EDAB038993C5BC3EBDDAA171BE0DD_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4940 = { sizeof (Sect409r1Holder_tFE35E4DBC01B34180F27768B2F455C3E0532D7F8), -1, sizeof(Sect409r1Holder_tFE35E4DBC01B34180F27768B2F455C3E0532D7F8_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4940[1] = 
{
	Sect409r1Holder_tFE35E4DBC01B34180F27768B2F455C3E0532D7F8_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4941 = { sizeof (Sect571k1Holder_tF2BC31E4E8790EFEBE510043C5590487E5797AD6), -1, sizeof(Sect571k1Holder_tF2BC31E4E8790EFEBE510043C5590487E5797AD6_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4941[1] = 
{
	Sect571k1Holder_tF2BC31E4E8790EFEBE510043C5590487E5797AD6_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4942 = { sizeof (Sect571r1Holder_tDD00E3E0375A4B39892BDAC16ED196648AB57D21), -1, sizeof(Sect571r1Holder_tDD00E3E0375A4B39892BDAC16ED196648AB57D21_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4942[1] = 
{
	Sect571r1Holder_tDD00E3E0375A4B39892BDAC16ED196648AB57D21_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4943 = { sizeof (SecObjectIdentifiers_t5FDF1759371F1AABBBC6F18586221092D3273EA4), -1, sizeof(SecObjectIdentifiers_t5FDF1759371F1AABBBC6F18586221092D3273EA4_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4943[34] = 
{
	SecObjectIdentifiers_t5FDF1759371F1AABBBC6F18586221092D3273EA4_StaticFields::get_offset_of_EllipticCurve_0(),
	SecObjectIdentifiers_t5FDF1759371F1AABBBC6F18586221092D3273EA4_StaticFields::get_offset_of_SecT163k1_1(),
	SecObjectIdentifiers_t5FDF1759371F1AABBBC6F18586221092D3273EA4_StaticFields::get_offset_of_SecT163r1_2(),
	SecObjectIdentifiers_t5FDF1759371F1AABBBC6F18586221092D3273EA4_StaticFields::get_offset_of_SecT239k1_3(),
	SecObjectIdentifiers_t5FDF1759371F1AABBBC6F18586221092D3273EA4_StaticFields::get_offset_of_SecT113r1_4(),
	SecObjectIdentifiers_t5FDF1759371F1AABBBC6F18586221092D3273EA4_StaticFields::get_offset_of_SecT113r2_5(),
	SecObjectIdentifiers_t5FDF1759371F1AABBBC6F18586221092D3273EA4_StaticFields::get_offset_of_SecP112r1_6(),
	SecObjectIdentifiers_t5FDF1759371F1AABBBC6F18586221092D3273EA4_StaticFields::get_offset_of_SecP112r2_7(),
	SecObjectIdentifiers_t5FDF1759371F1AABBBC6F18586221092D3273EA4_StaticFields::get_offset_of_SecP160r1_8(),
	SecObjectIdentifiers_t5FDF1759371F1AABBBC6F18586221092D3273EA4_StaticFields::get_offset_of_SecP160k1_9(),
	SecObjectIdentifiers_t5FDF1759371F1AABBBC6F18586221092D3273EA4_StaticFields::get_offset_of_SecP256k1_10(),
	SecObjectIdentifiers_t5FDF1759371F1AABBBC6F18586221092D3273EA4_StaticFields::get_offset_of_SecT163r2_11(),
	SecObjectIdentifiers_t5FDF1759371F1AABBBC6F18586221092D3273EA4_StaticFields::get_offset_of_SecT283k1_12(),
	SecObjectIdentifiers_t5FDF1759371F1AABBBC6F18586221092D3273EA4_StaticFields::get_offset_of_SecT283r1_13(),
	SecObjectIdentifiers_t5FDF1759371F1AABBBC6F18586221092D3273EA4_StaticFields::get_offset_of_SecT131r1_14(),
	SecObjectIdentifiers_t5FDF1759371F1AABBBC6F18586221092D3273EA4_StaticFields::get_offset_of_SecT131r2_15(),
	SecObjectIdentifiers_t5FDF1759371F1AABBBC6F18586221092D3273EA4_StaticFields::get_offset_of_SecT193r1_16(),
	SecObjectIdentifiers_t5FDF1759371F1AABBBC6F18586221092D3273EA4_StaticFields::get_offset_of_SecT193r2_17(),
	SecObjectIdentifiers_t5FDF1759371F1AABBBC6F18586221092D3273EA4_StaticFields::get_offset_of_SecT233k1_18(),
	SecObjectIdentifiers_t5FDF1759371F1AABBBC6F18586221092D3273EA4_StaticFields::get_offset_of_SecT233r1_19(),
	SecObjectIdentifiers_t5FDF1759371F1AABBBC6F18586221092D3273EA4_StaticFields::get_offset_of_SecP128r1_20(),
	SecObjectIdentifiers_t5FDF1759371F1AABBBC6F18586221092D3273EA4_StaticFields::get_offset_of_SecP128r2_21(),
	SecObjectIdentifiers_t5FDF1759371F1AABBBC6F18586221092D3273EA4_StaticFields::get_offset_of_SecP160r2_22(),
	SecObjectIdentifiers_t5FDF1759371F1AABBBC6F18586221092D3273EA4_StaticFields::get_offset_of_SecP192k1_23(),
	SecObjectIdentifiers_t5FDF1759371F1AABBBC6F18586221092D3273EA4_StaticFields::get_offset_of_SecP224k1_24(),
	SecObjectIdentifiers_t5FDF1759371F1AABBBC6F18586221092D3273EA4_StaticFields::get_offset_of_SecP224r1_25(),
	SecObjectIdentifiers_t5FDF1759371F1AABBBC6F18586221092D3273EA4_StaticFields::get_offset_of_SecP384r1_26(),
	SecObjectIdentifiers_t5FDF1759371F1AABBBC6F18586221092D3273EA4_StaticFields::get_offset_of_SecP521r1_27(),
	SecObjectIdentifiers_t5FDF1759371F1AABBBC6F18586221092D3273EA4_StaticFields::get_offset_of_SecT409k1_28(),
	SecObjectIdentifiers_t5FDF1759371F1AABBBC6F18586221092D3273EA4_StaticFields::get_offset_of_SecT409r1_29(),
	SecObjectIdentifiers_t5FDF1759371F1AABBBC6F18586221092D3273EA4_StaticFields::get_offset_of_SecT571k1_30(),
	SecObjectIdentifiers_t5FDF1759371F1AABBBC6F18586221092D3273EA4_StaticFields::get_offset_of_SecT571r1_31(),
	SecObjectIdentifiers_t5FDF1759371F1AABBBC6F18586221092D3273EA4_StaticFields::get_offset_of_SecP192r1_32(),
	SecObjectIdentifiers_t5FDF1759371F1AABBBC6F18586221092D3273EA4_StaticFields::get_offset_of_SecP256r1_33(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4944 = { sizeof (TeleTrusTNamedCurves_tD7727093AD23C392E666ED02F75BFC723CECDAA1), -1, sizeof(TeleTrusTNamedCurves_tD7727093AD23C392E666ED02F75BFC723CECDAA1_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4944[3] = 
{
	TeleTrusTNamedCurves_tD7727093AD23C392E666ED02F75BFC723CECDAA1_StaticFields::get_offset_of_objIds_0(),
	TeleTrusTNamedCurves_tD7727093AD23C392E666ED02F75BFC723CECDAA1_StaticFields::get_offset_of_curves_1(),
	TeleTrusTNamedCurves_tD7727093AD23C392E666ED02F75BFC723CECDAA1_StaticFields::get_offset_of_names_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4945 = { sizeof (BrainpoolP160r1Holder_tB00765DBFC854B8CDA2D4E78972A80D3FE74F27A), -1, sizeof(BrainpoolP160r1Holder_tB00765DBFC854B8CDA2D4E78972A80D3FE74F27A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4945[1] = 
{
	BrainpoolP160r1Holder_tB00765DBFC854B8CDA2D4E78972A80D3FE74F27A_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4946 = { sizeof (BrainpoolP160t1Holder_t9D96D581E7074E77DAD9FD9F3079EB83660EB8FC), -1, sizeof(BrainpoolP160t1Holder_t9D96D581E7074E77DAD9FD9F3079EB83660EB8FC_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4946[1] = 
{
	BrainpoolP160t1Holder_t9D96D581E7074E77DAD9FD9F3079EB83660EB8FC_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4947 = { sizeof (BrainpoolP192r1Holder_t771274FE9E2EE8CC870E49124DF103B7A8ECD577), -1, sizeof(BrainpoolP192r1Holder_t771274FE9E2EE8CC870E49124DF103B7A8ECD577_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4947[1] = 
{
	BrainpoolP192r1Holder_t771274FE9E2EE8CC870E49124DF103B7A8ECD577_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4948 = { sizeof (BrainpoolP192t1Holder_t54B80D472F33C4D6A78017EC49A23913DD04B430), -1, sizeof(BrainpoolP192t1Holder_t54B80D472F33C4D6A78017EC49A23913DD04B430_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4948[1] = 
{
	BrainpoolP192t1Holder_t54B80D472F33C4D6A78017EC49A23913DD04B430_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4949 = { sizeof (BrainpoolP224r1Holder_tE796B60E32E29D4DB30F849AC34E1E54E4C28FCC), -1, sizeof(BrainpoolP224r1Holder_tE796B60E32E29D4DB30F849AC34E1E54E4C28FCC_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4949[1] = 
{
	BrainpoolP224r1Holder_tE796B60E32E29D4DB30F849AC34E1E54E4C28FCC_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4950 = { sizeof (BrainpoolP224t1Holder_t17820B576543990213F53A8E8CB267DD52614532), -1, sizeof(BrainpoolP224t1Holder_t17820B576543990213F53A8E8CB267DD52614532_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4950[1] = 
{
	BrainpoolP224t1Holder_t17820B576543990213F53A8E8CB267DD52614532_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4951 = { sizeof (BrainpoolP256r1Holder_tABC5A55FC3CC25BD0B521511E2D7CF930D89CB4C), -1, sizeof(BrainpoolP256r1Holder_tABC5A55FC3CC25BD0B521511E2D7CF930D89CB4C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4951[1] = 
{
	BrainpoolP256r1Holder_tABC5A55FC3CC25BD0B521511E2D7CF930D89CB4C_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4952 = { sizeof (BrainpoolP256t1Holder_t3E07DA34380DCA555D3175AA6A32C213B0B46891), -1, sizeof(BrainpoolP256t1Holder_t3E07DA34380DCA555D3175AA6A32C213B0B46891_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4952[1] = 
{
	BrainpoolP256t1Holder_t3E07DA34380DCA555D3175AA6A32C213B0B46891_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4953 = { sizeof (BrainpoolP320r1Holder_tF858083D9EC65C21F9086A5629E801934F81A82F), -1, sizeof(BrainpoolP320r1Holder_tF858083D9EC65C21F9086A5629E801934F81A82F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4953[1] = 
{
	BrainpoolP320r1Holder_tF858083D9EC65C21F9086A5629E801934F81A82F_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4954 = { sizeof (BrainpoolP320t1Holder_tF58B643166F97C26E85380DB6341EC778E04C772), -1, sizeof(BrainpoolP320t1Holder_tF58B643166F97C26E85380DB6341EC778E04C772_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4954[1] = 
{
	BrainpoolP320t1Holder_tF58B643166F97C26E85380DB6341EC778E04C772_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4955 = { sizeof (BrainpoolP384r1Holder_tEB77B2724A14AFB2B02BBB723638144F6C9B155A), -1, sizeof(BrainpoolP384r1Holder_tEB77B2724A14AFB2B02BBB723638144F6C9B155A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4955[1] = 
{
	BrainpoolP384r1Holder_tEB77B2724A14AFB2B02BBB723638144F6C9B155A_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4956 = { sizeof (BrainpoolP384t1Holder_tDDE31E2E321288ECC960740D2900555C0217668F), -1, sizeof(BrainpoolP384t1Holder_tDDE31E2E321288ECC960740D2900555C0217668F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4956[1] = 
{
	BrainpoolP384t1Holder_tDDE31E2E321288ECC960740D2900555C0217668F_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4957 = { sizeof (BrainpoolP512r1Holder_t42A031BB31B58F65E2C95769DB80332D1A2B13D3), -1, sizeof(BrainpoolP512r1Holder_t42A031BB31B58F65E2C95769DB80332D1A2B13D3_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4957[1] = 
{
	BrainpoolP512r1Holder_t42A031BB31B58F65E2C95769DB80332D1A2B13D3_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4958 = { sizeof (BrainpoolP512t1Holder_t87E55A19950DDFA17A82E6C6F09C0BCBAED882C0), -1, sizeof(BrainpoolP512t1Holder_t87E55A19950DDFA17A82E6C6F09C0BCBAED882C0_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4958[1] = 
{
	BrainpoolP512t1Holder_t87E55A19950DDFA17A82E6C6F09C0BCBAED882C0_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4959 = { sizeof (TeleTrusTObjectIdentifiers_t6BCD3C3595B30AF2BE84D9A55DE2C0AAA498F5D6), -1, sizeof(TeleTrusTObjectIdentifiers_t6BCD3C3595B30AF2BE84D9A55DE2C0AAA498F5D6_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4959[28] = 
{
	TeleTrusTObjectIdentifiers_t6BCD3C3595B30AF2BE84D9A55DE2C0AAA498F5D6_StaticFields::get_offset_of_TeleTrusTAlgorithm_0(),
	TeleTrusTObjectIdentifiers_t6BCD3C3595B30AF2BE84D9A55DE2C0AAA498F5D6_StaticFields::get_offset_of_RipeMD160_1(),
	TeleTrusTObjectIdentifiers_t6BCD3C3595B30AF2BE84D9A55DE2C0AAA498F5D6_StaticFields::get_offset_of_RipeMD128_2(),
	TeleTrusTObjectIdentifiers_t6BCD3C3595B30AF2BE84D9A55DE2C0AAA498F5D6_StaticFields::get_offset_of_RipeMD256_3(),
	TeleTrusTObjectIdentifiers_t6BCD3C3595B30AF2BE84D9A55DE2C0AAA498F5D6_StaticFields::get_offset_of_TeleTrusTRsaSignatureAlgorithm_4(),
	TeleTrusTObjectIdentifiers_t6BCD3C3595B30AF2BE84D9A55DE2C0AAA498F5D6_StaticFields::get_offset_of_RsaSignatureWithRipeMD160_5(),
	TeleTrusTObjectIdentifiers_t6BCD3C3595B30AF2BE84D9A55DE2C0AAA498F5D6_StaticFields::get_offset_of_RsaSignatureWithRipeMD128_6(),
	TeleTrusTObjectIdentifiers_t6BCD3C3595B30AF2BE84D9A55DE2C0AAA498F5D6_StaticFields::get_offset_of_RsaSignatureWithRipeMD256_7(),
	TeleTrusTObjectIdentifiers_t6BCD3C3595B30AF2BE84D9A55DE2C0AAA498F5D6_StaticFields::get_offset_of_ECSign_8(),
	TeleTrusTObjectIdentifiers_t6BCD3C3595B30AF2BE84D9A55DE2C0AAA498F5D6_StaticFields::get_offset_of_ECSignWithSha1_9(),
	TeleTrusTObjectIdentifiers_t6BCD3C3595B30AF2BE84D9A55DE2C0AAA498F5D6_StaticFields::get_offset_of_ECSignWithRipeMD160_10(),
	TeleTrusTObjectIdentifiers_t6BCD3C3595B30AF2BE84D9A55DE2C0AAA498F5D6_StaticFields::get_offset_of_EccBrainpool_11(),
	TeleTrusTObjectIdentifiers_t6BCD3C3595B30AF2BE84D9A55DE2C0AAA498F5D6_StaticFields::get_offset_of_EllipticCurve_12(),
	TeleTrusTObjectIdentifiers_t6BCD3C3595B30AF2BE84D9A55DE2C0AAA498F5D6_StaticFields::get_offset_of_VersionOne_13(),
	TeleTrusTObjectIdentifiers_t6BCD3C3595B30AF2BE84D9A55DE2C0AAA498F5D6_StaticFields::get_offset_of_BrainpoolP160R1_14(),
	TeleTrusTObjectIdentifiers_t6BCD3C3595B30AF2BE84D9A55DE2C0AAA498F5D6_StaticFields::get_offset_of_BrainpoolP160T1_15(),
	TeleTrusTObjectIdentifiers_t6BCD3C3595B30AF2BE84D9A55DE2C0AAA498F5D6_StaticFields::get_offset_of_BrainpoolP192R1_16(),
	TeleTrusTObjectIdentifiers_t6BCD3C3595B30AF2BE84D9A55DE2C0AAA498F5D6_StaticFields::get_offset_of_BrainpoolP192T1_17(),
	TeleTrusTObjectIdentifiers_t6BCD3C3595B30AF2BE84D9A55DE2C0AAA498F5D6_StaticFields::get_offset_of_BrainpoolP224R1_18(),
	TeleTrusTObjectIdentifiers_t6BCD3C3595B30AF2BE84D9A55DE2C0AAA498F5D6_StaticFields::get_offset_of_BrainpoolP224T1_19(),
	TeleTrusTObjectIdentifiers_t6BCD3C3595B30AF2BE84D9A55DE2C0AAA498F5D6_StaticFields::get_offset_of_BrainpoolP256R1_20(),
	TeleTrusTObjectIdentifiers_t6BCD3C3595B30AF2BE84D9A55DE2C0AAA498F5D6_StaticFields::get_offset_of_BrainpoolP256T1_21(),
	TeleTrusTObjectIdentifiers_t6BCD3C3595B30AF2BE84D9A55DE2C0AAA498F5D6_StaticFields::get_offset_of_BrainpoolP320R1_22(),
	TeleTrusTObjectIdentifiers_t6BCD3C3595B30AF2BE84D9A55DE2C0AAA498F5D6_StaticFields::get_offset_of_BrainpoolP320T1_23(),
	TeleTrusTObjectIdentifiers_t6BCD3C3595B30AF2BE84D9A55DE2C0AAA498F5D6_StaticFields::get_offset_of_BrainpoolP384R1_24(),
	TeleTrusTObjectIdentifiers_t6BCD3C3595B30AF2BE84D9A55DE2C0AAA498F5D6_StaticFields::get_offset_of_BrainpoolP384T1_25(),
	TeleTrusTObjectIdentifiers_t6BCD3C3595B30AF2BE84D9A55DE2C0AAA498F5D6_StaticFields::get_offset_of_BrainpoolP512R1_26(),
	TeleTrusTObjectIdentifiers_t6BCD3C3595B30AF2BE84D9A55DE2C0AAA498F5D6_StaticFields::get_offset_of_BrainpoolP512T1_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4960 = { sizeof (Asn1Dump_t8190340D0A1311E1019091CF6D7D55A6A5B45C3F), -1, sizeof(Asn1Dump_t8190340D0A1311E1019091CF6D7D55A6A5B45C3F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4960[1] = 
{
	Asn1Dump_t8190340D0A1311E1019091CF6D7D55A6A5B45C3F_StaticFields::get_offset_of_NewLine_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4961 = { sizeof (AlgorithmIdentifier_tDE7123E83DAD8C219BBB99DAE8CAAD8A2AE64EFB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4961[2] = 
{
	AlgorithmIdentifier_tDE7123E83DAD8C219BBB99DAE8CAAD8A2AE64EFB::get_offset_of_algorithm_0(),
	AlgorithmIdentifier_tDE7123E83DAD8C219BBB99DAE8CAAD8A2AE64EFB::get_offset_of_parameters_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4962 = { sizeof (BasicConstraints_t199BCC0E7A6950B11A098A0422FDEB8E73E2380E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4962[2] = 
{
	BasicConstraints_t199BCC0E7A6950B11A098A0422FDEB8E73E2380E::get_offset_of_cA_0(),
	BasicConstraints_t199BCC0E7A6950B11A098A0422FDEB8E73E2380E::get_offset_of_pathLenConstraint_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4963 = { sizeof (DsaParameter_t292FF5A951F42D3279875CE69233BD3A39685D73), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4963[3] = 
{
	DsaParameter_t292FF5A951F42D3279875CE69233BD3A39685D73::get_offset_of_p_0(),
	DsaParameter_t292FF5A951F42D3279875CE69233BD3A39685D73::get_offset_of_q_1(),
	DsaParameter_t292FF5A951F42D3279875CE69233BD3A39685D73::get_offset_of_g_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4964 = { sizeof (DigestInfo_t12BB846B9E621A6FF47DC65A5F94EC7E734E07FA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4964[2] = 
{
	DigestInfo_t12BB846B9E621A6FF47DC65A5F94EC7E734E07FA::get_offset_of_digest_0(),
	DigestInfo_t12BB846B9E621A6FF47DC65A5F94EC7E734E07FA::get_offset_of_algID_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4965 = { sizeof (KeyUsage_tC2C79804034CEA4E31409EEF3C2A7A8660A6008D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4966 = { sizeof (RsaPublicKeyStructure_t6F015FCA225AF3E16C7C545F0A1B0DB7FAF86567), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4966[2] = 
{
	RsaPublicKeyStructure_t6F015FCA225AF3E16C7C545F0A1B0DB7FAF86567::get_offset_of_modulus_0(),
	RsaPublicKeyStructure_t6F015FCA225AF3E16C7C545F0A1B0DB7FAF86567::get_offset_of_publicExponent_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4967 = { sizeof (SubjectPublicKeyInfo_tE61D9687E07635F3A249A3FBBBE6E2D9C4B73A7C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4967[2] = 
{
	SubjectPublicKeyInfo_tE61D9687E07635F3A249A3FBBBE6E2D9C4B73A7C::get_offset_of_algID_0(),
	SubjectPublicKeyInfo_tE61D9687E07635F3A249A3FBBBE6E2D9C4B73A7C::get_offset_of_keyData_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4968 = { sizeof (TbsCertificateStructure_tDA73857A74B4B8375807FCBE2099FDFFA361D598), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4968[12] = 
{
	TbsCertificateStructure_tDA73857A74B4B8375807FCBE2099FDFFA361D598::get_offset_of_seq_0(),
	TbsCertificateStructure_tDA73857A74B4B8375807FCBE2099FDFFA361D598::get_offset_of_version_1(),
	TbsCertificateStructure_tDA73857A74B4B8375807FCBE2099FDFFA361D598::get_offset_of_serialNumber_2(),
	TbsCertificateStructure_tDA73857A74B4B8375807FCBE2099FDFFA361D598::get_offset_of_signature_3(),
	TbsCertificateStructure_tDA73857A74B4B8375807FCBE2099FDFFA361D598::get_offset_of_issuer_4(),
	TbsCertificateStructure_tDA73857A74B4B8375807FCBE2099FDFFA361D598::get_offset_of_startDate_5(),
	TbsCertificateStructure_tDA73857A74B4B8375807FCBE2099FDFFA361D598::get_offset_of_endDate_6(),
	TbsCertificateStructure_tDA73857A74B4B8375807FCBE2099FDFFA361D598::get_offset_of_subject_7(),
	TbsCertificateStructure_tDA73857A74B4B8375807FCBE2099FDFFA361D598::get_offset_of_subjectPublicKeyInfo_8(),
	TbsCertificateStructure_tDA73857A74B4B8375807FCBE2099FDFFA361D598::get_offset_of_issuerUniqueID_9(),
	TbsCertificateStructure_tDA73857A74B4B8375807FCBE2099FDFFA361D598::get_offset_of_subjectUniqueID_10(),
	TbsCertificateStructure_tDA73857A74B4B8375807FCBE2099FDFFA361D598::get_offset_of_extensions_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4969 = { sizeof (Time_t80739D7454AE536E0F9A1C9D5B0620C3C8378307), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4969[1] = 
{
	Time_t80739D7454AE536E0F9A1C9D5B0620C3C8378307::get_offset_of_time_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4970 = { sizeof (X509CertificateStructure_tEF05F4D8F02C2BC366AC06A3E2BBDB4C35D54FE1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4970[3] = 
{
	X509CertificateStructure_tEF05F4D8F02C2BC366AC06A3E2BBDB4C35D54FE1::get_offset_of_tbsCert_0(),
	X509CertificateStructure_tEF05F4D8F02C2BC366AC06A3E2BBDB4C35D54FE1::get_offset_of_sigAlgID_1(),
	X509CertificateStructure_tEF05F4D8F02C2BC366AC06A3E2BBDB4C35D54FE1::get_offset_of_sig_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4971 = { sizeof (X509Extension_t523236F1B9FC5A5FE68AE968262556435B4E8E5A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4971[2] = 
{
	X509Extension_t523236F1B9FC5A5FE68AE968262556435B4E8E5A::get_offset_of_critical_0(),
	X509Extension_t523236F1B9FC5A5FE68AE968262556435B4E8E5A::get_offset_of_value_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4972 = { sizeof (X509Extensions_tBC4870829033B1A0E0F297F32C0B2B29C1618E44), -1, sizeof(X509Extensions_tBC4870829033B1A0E0F297F32C0B2B29C1618E44_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4972[33] = 
{
	X509Extensions_tBC4870829033B1A0E0F297F32C0B2B29C1618E44_StaticFields::get_offset_of_SubjectDirectoryAttributes_0(),
	X509Extensions_tBC4870829033B1A0E0F297F32C0B2B29C1618E44_StaticFields::get_offset_of_SubjectKeyIdentifier_1(),
	X509Extensions_tBC4870829033B1A0E0F297F32C0B2B29C1618E44_StaticFields::get_offset_of_KeyUsage_2(),
	X509Extensions_tBC4870829033B1A0E0F297F32C0B2B29C1618E44_StaticFields::get_offset_of_PrivateKeyUsagePeriod_3(),
	X509Extensions_tBC4870829033B1A0E0F297F32C0B2B29C1618E44_StaticFields::get_offset_of_SubjectAlternativeName_4(),
	X509Extensions_tBC4870829033B1A0E0F297F32C0B2B29C1618E44_StaticFields::get_offset_of_IssuerAlternativeName_5(),
	X509Extensions_tBC4870829033B1A0E0F297F32C0B2B29C1618E44_StaticFields::get_offset_of_BasicConstraints_6(),
	X509Extensions_tBC4870829033B1A0E0F297F32C0B2B29C1618E44_StaticFields::get_offset_of_CrlNumber_7(),
	X509Extensions_tBC4870829033B1A0E0F297F32C0B2B29C1618E44_StaticFields::get_offset_of_ReasonCode_8(),
	X509Extensions_tBC4870829033B1A0E0F297F32C0B2B29C1618E44_StaticFields::get_offset_of_InstructionCode_9(),
	X509Extensions_tBC4870829033B1A0E0F297F32C0B2B29C1618E44_StaticFields::get_offset_of_InvalidityDate_10(),
	X509Extensions_tBC4870829033B1A0E0F297F32C0B2B29C1618E44_StaticFields::get_offset_of_DeltaCrlIndicator_11(),
	X509Extensions_tBC4870829033B1A0E0F297F32C0B2B29C1618E44_StaticFields::get_offset_of_IssuingDistributionPoint_12(),
	X509Extensions_tBC4870829033B1A0E0F297F32C0B2B29C1618E44_StaticFields::get_offset_of_CertificateIssuer_13(),
	X509Extensions_tBC4870829033B1A0E0F297F32C0B2B29C1618E44_StaticFields::get_offset_of_NameConstraints_14(),
	X509Extensions_tBC4870829033B1A0E0F297F32C0B2B29C1618E44_StaticFields::get_offset_of_CrlDistributionPoints_15(),
	X509Extensions_tBC4870829033B1A0E0F297F32C0B2B29C1618E44_StaticFields::get_offset_of_CertificatePolicies_16(),
	X509Extensions_tBC4870829033B1A0E0F297F32C0B2B29C1618E44_StaticFields::get_offset_of_PolicyMappings_17(),
	X509Extensions_tBC4870829033B1A0E0F297F32C0B2B29C1618E44_StaticFields::get_offset_of_AuthorityKeyIdentifier_18(),
	X509Extensions_tBC4870829033B1A0E0F297F32C0B2B29C1618E44_StaticFields::get_offset_of_PolicyConstraints_19(),
	X509Extensions_tBC4870829033B1A0E0F297F32C0B2B29C1618E44_StaticFields::get_offset_of_ExtendedKeyUsage_20(),
	X509Extensions_tBC4870829033B1A0E0F297F32C0B2B29C1618E44_StaticFields::get_offset_of_FreshestCrl_21(),
	X509Extensions_tBC4870829033B1A0E0F297F32C0B2B29C1618E44_StaticFields::get_offset_of_InhibitAnyPolicy_22(),
	X509Extensions_tBC4870829033B1A0E0F297F32C0B2B29C1618E44_StaticFields::get_offset_of_AuthorityInfoAccess_23(),
	X509Extensions_tBC4870829033B1A0E0F297F32C0B2B29C1618E44_StaticFields::get_offset_of_SubjectInfoAccess_24(),
	X509Extensions_tBC4870829033B1A0E0F297F32C0B2B29C1618E44_StaticFields::get_offset_of_LogoType_25(),
	X509Extensions_tBC4870829033B1A0E0F297F32C0B2B29C1618E44_StaticFields::get_offset_of_BiometricInfo_26(),
	X509Extensions_tBC4870829033B1A0E0F297F32C0B2B29C1618E44_StaticFields::get_offset_of_QCStatements_27(),
	X509Extensions_tBC4870829033B1A0E0F297F32C0B2B29C1618E44_StaticFields::get_offset_of_AuditIdentity_28(),
	X509Extensions_tBC4870829033B1A0E0F297F32C0B2B29C1618E44_StaticFields::get_offset_of_NoRevAvail_29(),
	X509Extensions_tBC4870829033B1A0E0F297F32C0B2B29C1618E44_StaticFields::get_offset_of_TargetInformation_30(),
	X509Extensions_tBC4870829033B1A0E0F297F32C0B2B29C1618E44::get_offset_of_extensions_31(),
	X509Extensions_tBC4870829033B1A0E0F297F32C0B2B29C1618E44::get_offset_of_ordering_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4973 = { sizeof (X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E), -1, sizeof(X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4973[44] = 
{
	X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields::get_offset_of_C_0(),
	X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields::get_offset_of_O_1(),
	X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields::get_offset_of_OU_2(),
	X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields::get_offset_of_T_3(),
	X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields::get_offset_of_CN_4(),
	X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields::get_offset_of_Street_5(),
	X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields::get_offset_of_SerialNumber_6(),
	X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields::get_offset_of_L_7(),
	X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields::get_offset_of_ST_8(),
	X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields::get_offset_of_Surname_9(),
	X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields::get_offset_of_GivenName_10(),
	X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields::get_offset_of_Initials_11(),
	X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields::get_offset_of_Generation_12(),
	X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields::get_offset_of_UniqueIdentifier_13(),
	X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields::get_offset_of_BusinessCategory_14(),
	X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields::get_offset_of_PostalCode_15(),
	X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields::get_offset_of_DnQualifier_16(),
	X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields::get_offset_of_Pseudonym_17(),
	X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields::get_offset_of_DateOfBirth_18(),
	X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields::get_offset_of_PlaceOfBirth_19(),
	X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields::get_offset_of_Gender_20(),
	X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields::get_offset_of_CountryOfCitizenship_21(),
	X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields::get_offset_of_CountryOfResidence_22(),
	X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields::get_offset_of_NameAtBirth_23(),
	X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields::get_offset_of_PostalAddress_24(),
	X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields::get_offset_of_DmdName_25(),
	X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields::get_offset_of_TelephoneNumber_26(),
	X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields::get_offset_of_Name_27(),
	X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields::get_offset_of_EmailAddress_28(),
	X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields::get_offset_of_UnstructuredName_29(),
	X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields::get_offset_of_UnstructuredAddress_30(),
	X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields::get_offset_of_E_31(),
	X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields::get_offset_of_DC_32(),
	X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields::get_offset_of_UID_33(),
	X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields::get_offset_of_defaultReverse_34(),
	X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields::get_offset_of_DefaultSymbols_35(),
	X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields::get_offset_of_RFC2253Symbols_36(),
	X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields::get_offset_of_RFC1779Symbols_37(),
	X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E_StaticFields::get_offset_of_DefaultLookup_38(),
	X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E::get_offset_of_ordering_39(),
	X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E::get_offset_of_converter_40(),
	X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E::get_offset_of_values_41(),
	X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E::get_offset_of_added_42(),
	X509Name_t00D18BE10190B7EC9BD6B234477771FBE4CA304E::get_offset_of_seq_43(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4974 = { sizeof (X509NameEntryConverter_t6B3AE1DD9E7B91F1C0039575CEAC00CAE0972DED), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4975 = { sizeof (X509ObjectIdentifiers_t94E84E96A76E1618C0058E4FE97D3A245E4B4154), -1, sizeof(X509ObjectIdentifiers_t94E84E96A76E1618C0058E4FE97D3A245E4B4154_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4975[19] = 
{
	X509ObjectIdentifiers_t94E84E96A76E1618C0058E4FE97D3A245E4B4154_StaticFields::get_offset_of_CommonName_0(),
	X509ObjectIdentifiers_t94E84E96A76E1618C0058E4FE97D3A245E4B4154_StaticFields::get_offset_of_CountryName_1(),
	X509ObjectIdentifiers_t94E84E96A76E1618C0058E4FE97D3A245E4B4154_StaticFields::get_offset_of_LocalityName_2(),
	X509ObjectIdentifiers_t94E84E96A76E1618C0058E4FE97D3A245E4B4154_StaticFields::get_offset_of_StateOrProvinceName_3(),
	X509ObjectIdentifiers_t94E84E96A76E1618C0058E4FE97D3A245E4B4154_StaticFields::get_offset_of_Organization_4(),
	X509ObjectIdentifiers_t94E84E96A76E1618C0058E4FE97D3A245E4B4154_StaticFields::get_offset_of_OrganizationalUnitName_5(),
	X509ObjectIdentifiers_t94E84E96A76E1618C0058E4FE97D3A245E4B4154_StaticFields::get_offset_of_id_at_telephoneNumber_6(),
	X509ObjectIdentifiers_t94E84E96A76E1618C0058E4FE97D3A245E4B4154_StaticFields::get_offset_of_id_at_name_7(),
	X509ObjectIdentifiers_t94E84E96A76E1618C0058E4FE97D3A245E4B4154_StaticFields::get_offset_of_IdSha1_8(),
	X509ObjectIdentifiers_t94E84E96A76E1618C0058E4FE97D3A245E4B4154_StaticFields::get_offset_of_RipeMD160_9(),
	X509ObjectIdentifiers_t94E84E96A76E1618C0058E4FE97D3A245E4B4154_StaticFields::get_offset_of_RipeMD160WithRsaEncryption_10(),
	X509ObjectIdentifiers_t94E84E96A76E1618C0058E4FE97D3A245E4B4154_StaticFields::get_offset_of_IdEARsa_11(),
	X509ObjectIdentifiers_t94E84E96A76E1618C0058E4FE97D3A245E4B4154_StaticFields::get_offset_of_IdPkix_12(),
	X509ObjectIdentifiers_t94E84E96A76E1618C0058E4FE97D3A245E4B4154_StaticFields::get_offset_of_IdPE_13(),
	X509ObjectIdentifiers_t94E84E96A76E1618C0058E4FE97D3A245E4B4154_StaticFields::get_offset_of_IdAD_14(),
	X509ObjectIdentifiers_t94E84E96A76E1618C0058E4FE97D3A245E4B4154_StaticFields::get_offset_of_IdADCAIssuers_15(),
	X509ObjectIdentifiers_t94E84E96A76E1618C0058E4FE97D3A245E4B4154_StaticFields::get_offset_of_IdADOcsp_16(),
	X509ObjectIdentifiers_t94E84E96A76E1618C0058E4FE97D3A245E4B4154_StaticFields::get_offset_of_OcspAccessMethod_17(),
	X509ObjectIdentifiers_t94E84E96A76E1618C0058E4FE97D3A245E4B4154_StaticFields::get_offset_of_CrlAccessMethod_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4976 = { sizeof (DHDomainParameters_t542D48B2F5FDB54471E254C9C3B42BA7CDA1FA54), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4976[5] = 
{
	DHDomainParameters_t542D48B2F5FDB54471E254C9C3B42BA7CDA1FA54::get_offset_of_p_0(),
	DHDomainParameters_t542D48B2F5FDB54471E254C9C3B42BA7CDA1FA54::get_offset_of_g_1(),
	DHDomainParameters_t542D48B2F5FDB54471E254C9C3B42BA7CDA1FA54::get_offset_of_q_2(),
	DHDomainParameters_t542D48B2F5FDB54471E254C9C3B42BA7CDA1FA54::get_offset_of_j_3(),
	DHDomainParameters_t542D48B2F5FDB54471E254C9C3B42BA7CDA1FA54::get_offset_of_validationParms_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4977 = { sizeof (DHPublicKey_tD8B4B760470471953CC93F6C061B8B5008E1FABB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4977[1] = 
{
	DHPublicKey_tD8B4B760470471953CC93F6C061B8B5008E1FABB::get_offset_of_y_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4978 = { sizeof (DHValidationParms_t8121E846FBD95D655031A424C5AECD365F767E34), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4978[2] = 
{
	DHValidationParms_t8121E846FBD95D655031A424C5AECD365F767E34::get_offset_of_seed_0(),
	DHValidationParms_t8121E846FBD95D655031A424C5AECD365F767E34::get_offset_of_pgenCounter_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4979 = { sizeof (ECNamedCurveTable_t7A2393B7906F6758968480CFCE8759D5A24BF13B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4980 = { sizeof (X962NamedCurves_t4B7ADF52483079BBA8E4518D8B32C1164430AF1B), -1, sizeof(X962NamedCurves_t4B7ADF52483079BBA8E4518D8B32C1164430AF1B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4980[3] = 
{
	X962NamedCurves_t4B7ADF52483079BBA8E4518D8B32C1164430AF1B_StaticFields::get_offset_of_objIds_0(),
	X962NamedCurves_t4B7ADF52483079BBA8E4518D8B32C1164430AF1B_StaticFields::get_offset_of_curves_1(),
	X962NamedCurves_t4B7ADF52483079BBA8E4518D8B32C1164430AF1B_StaticFields::get_offset_of_names_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4981 = { sizeof (Prime192v1Holder_tCC8F0D1A3AB0579680C9312DD08E0E0F6C4BD673), -1, sizeof(Prime192v1Holder_tCC8F0D1A3AB0579680C9312DD08E0E0F6C4BD673_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4981[1] = 
{
	Prime192v1Holder_tCC8F0D1A3AB0579680C9312DD08E0E0F6C4BD673_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4982 = { sizeof (Prime192v2Holder_t653A143572A0315D80625BA8FA78FC46F74DF0E7), -1, sizeof(Prime192v2Holder_t653A143572A0315D80625BA8FA78FC46F74DF0E7_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4982[1] = 
{
	Prime192v2Holder_t653A143572A0315D80625BA8FA78FC46F74DF0E7_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4983 = { sizeof (Prime192v3Holder_tF67FD19C286C5531B2DE2D96FFC7B58632D3152D), -1, sizeof(Prime192v3Holder_tF67FD19C286C5531B2DE2D96FFC7B58632D3152D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4983[1] = 
{
	Prime192v3Holder_tF67FD19C286C5531B2DE2D96FFC7B58632D3152D_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4984 = { sizeof (Prime239v1Holder_tBDB0E3D4C175164F93C36E48744A5F04C044CFE4), -1, sizeof(Prime239v1Holder_tBDB0E3D4C175164F93C36E48744A5F04C044CFE4_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4984[1] = 
{
	Prime239v1Holder_tBDB0E3D4C175164F93C36E48744A5F04C044CFE4_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4985 = { sizeof (Prime239v2Holder_t028E299B68F7F12751EB22C6F9A6ECD3A353FAD1), -1, sizeof(Prime239v2Holder_t028E299B68F7F12751EB22C6F9A6ECD3A353FAD1_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4985[1] = 
{
	Prime239v2Holder_t028E299B68F7F12751EB22C6F9A6ECD3A353FAD1_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4986 = { sizeof (Prime239v3Holder_tE7165C4CC814A6D9A72B3350432C2A43A4DEEAA1), -1, sizeof(Prime239v3Holder_tE7165C4CC814A6D9A72B3350432C2A43A4DEEAA1_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4986[1] = 
{
	Prime239v3Holder_tE7165C4CC814A6D9A72B3350432C2A43A4DEEAA1_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4987 = { sizeof (Prime256v1Holder_tE8FB8FB3A61FC9C3DD02DC0B88E2BAB23C1FEE61), -1, sizeof(Prime256v1Holder_tE8FB8FB3A61FC9C3DD02DC0B88E2BAB23C1FEE61_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4987[1] = 
{
	Prime256v1Holder_tE8FB8FB3A61FC9C3DD02DC0B88E2BAB23C1FEE61_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4988 = { sizeof (C2pnb163v1Holder_tF5B7D56693BDB71BE195BFE14975620CD4B60144), -1, sizeof(C2pnb163v1Holder_tF5B7D56693BDB71BE195BFE14975620CD4B60144_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4988[1] = 
{
	C2pnb163v1Holder_tF5B7D56693BDB71BE195BFE14975620CD4B60144_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4989 = { sizeof (C2pnb163v2Holder_t04F5B069D18CA40B24A16F3AD10E202B559216E9), -1, sizeof(C2pnb163v2Holder_t04F5B069D18CA40B24A16F3AD10E202B559216E9_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4989[1] = 
{
	C2pnb163v2Holder_t04F5B069D18CA40B24A16F3AD10E202B559216E9_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4990 = { sizeof (C2pnb163v3Holder_tAD7A38AA45BC75741AFC9599709E7F6CA8191C39), -1, sizeof(C2pnb163v3Holder_tAD7A38AA45BC75741AFC9599709E7F6CA8191C39_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4990[1] = 
{
	C2pnb163v3Holder_tAD7A38AA45BC75741AFC9599709E7F6CA8191C39_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4991 = { sizeof (C2pnb176w1Holder_t059096933FE8BC6A05C2B4CEF77DB412E0824D9B), -1, sizeof(C2pnb176w1Holder_t059096933FE8BC6A05C2B4CEF77DB412E0824D9B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4991[1] = 
{
	C2pnb176w1Holder_t059096933FE8BC6A05C2B4CEF77DB412E0824D9B_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4992 = { sizeof (C2tnb191v1Holder_tF9B7D41ED4A9582A7932599824825F1008EF66CC), -1, sizeof(C2tnb191v1Holder_tF9B7D41ED4A9582A7932599824825F1008EF66CC_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4992[1] = 
{
	C2tnb191v1Holder_tF9B7D41ED4A9582A7932599824825F1008EF66CC_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4993 = { sizeof (C2tnb191v2Holder_t64E7B8D81A8513F0C247D4B7DC30867AAC555DEA), -1, sizeof(C2tnb191v2Holder_t64E7B8D81A8513F0C247D4B7DC30867AAC555DEA_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4993[1] = 
{
	C2tnb191v2Holder_t64E7B8D81A8513F0C247D4B7DC30867AAC555DEA_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4994 = { sizeof (C2tnb191v3Holder_tB4F76AFEE78EBFE755694A0A651120302A0ACFCC), -1, sizeof(C2tnb191v3Holder_tB4F76AFEE78EBFE755694A0A651120302A0ACFCC_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4994[1] = 
{
	C2tnb191v3Holder_tB4F76AFEE78EBFE755694A0A651120302A0ACFCC_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4995 = { sizeof (C2pnb208w1Holder_t23D2554ED2D2EB475F88254BB131480E0ACB1846), -1, sizeof(C2pnb208w1Holder_t23D2554ED2D2EB475F88254BB131480E0ACB1846_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4995[1] = 
{
	C2pnb208w1Holder_t23D2554ED2D2EB475F88254BB131480E0ACB1846_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4996 = { sizeof (C2tnb239v1Holder_tF33876C71DB5BB3455BF9D5116948A52A179C8F9), -1, sizeof(C2tnb239v1Holder_tF33876C71DB5BB3455BF9D5116948A52A179C8F9_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4996[1] = 
{
	C2tnb239v1Holder_tF33876C71DB5BB3455BF9D5116948A52A179C8F9_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4997 = { sizeof (C2tnb239v2Holder_t572C9CCFFDDCB764D14C758E0A6AFD1E90D65779), -1, sizeof(C2tnb239v2Holder_t572C9CCFFDDCB764D14C758E0A6AFD1E90D65779_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4997[1] = 
{
	C2tnb239v2Holder_t572C9CCFFDDCB764D14C758E0A6AFD1E90D65779_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4998 = { sizeof (C2tnb239v3Holder_t74D9672D126B3F8E74B4820FAD111FC9E4B2C4A5), -1, sizeof(C2tnb239v3Holder_t74D9672D126B3F8E74B4820FAD111FC9E4B2C4A5_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4998[1] = 
{
	C2tnb239v3Holder_t74D9672D126B3F8E74B4820FAD111FC9E4B2C4A5_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4999 = { sizeof (C2pnb272w1Holder_t19A534B555E0797174E8D382F79F893177B28658), -1, sizeof(C2pnb272w1Holder_t19A534B555E0797174E8D382F79F893177B28658_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4999[1] = 
{
	C2pnb272w1Holder_t19A534B555E0797174E8D382F79F893177B28658_StaticFields::get_offset_of_Instance_1(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
