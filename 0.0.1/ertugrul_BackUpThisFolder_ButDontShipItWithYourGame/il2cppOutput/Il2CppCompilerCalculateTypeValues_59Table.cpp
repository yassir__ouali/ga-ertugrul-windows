﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// GameSparks.Core.GSConnection
struct GSConnection_tDCDCFD82EA16BD7E6FEF1CE33FD247F30661E56C;
// GameSparks.Core.GSConnection/<>c__DisplayClass7
struct U3CU3Ec__DisplayClass7_t0E3A1828A4229CA277C0866A600A81766B353B9B;
// GameSparks.Core.GSData
struct GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937;
// GameSparks.Core.GSInstance
struct GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774;
// GameSparks.Core.GSObject
struct GSObject_t05160F6677D08913C6CD9AB7C7DD9454D3B12E88;
// GameSparks.Core.GSRequest
struct GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7;
// GameSparks.Core.IGSPlatform
struct IGSPlatform_tE0AFAF9FEBCD3E24A01BD5210D69F4AC6DAE11D1;
// GameSparks.IGameSparksWebSocket
struct IGameSparksWebSocket_t32A323869928152727E263740F551C07E6E438C6;
// GameSparks.RT.BinaryWriteMemoryStream
struct BinaryWriteMemoryStream_t9503BBB30B8671EB7F1F21C98D1A5B0CBFD6B84A;
// GameSparks.RT.Commands.RTRequest
struct RTRequest_tAB65BDCB5A0862353CF86D457AA4901F2E32DCC1;
// GameSparks.RT.Connection.FastConnection
struct FastConnection_t9E83C1C0DD332A4579DA0512357037F285CA25A0;
// GameSparks.RT.Connection.ReliableConnection
struct ReliableConnection_t237BB0704E0E315A9E262CD04956628C1D2435B0;
// GameSparks.RT.Connection.ReliableWSConnection
struct ReliableWSConnection_tBE9804C4155A51FB7107363A89A38F9C68847554;
// GameSparks.RT.IRTCommand
struct IRTCommand_t37E3DEE05243474F6D13709B55B6F65BBCA87B75;
// GameSparks.RT.IRTSession
struct IRTSession_t72226F530281A73FC4B198F0ED8984FF6AF19244;
// GameSparks.RT.IRTSessionInternal
struct IRTSessionInternal_t89242BAC51743D3F51713AD0CC3C973E3F97F47D;
// GameSparks.RT.IRTSessionListener
struct IRTSessionListener_tADECD806E9D2F14D3D50B781FDE695ABF674D3C1;
// GameSparks.RT.Pools.ObjectPool`1<Com.Gamesparks.Realtime.Proto.LoginResult>
struct ObjectPool_1_t51A9BB45150D1669D720D6197A10CBBBF8406FCC;
// GameSparks.RT.Pools.ObjectPool`1<Com.Gamesparks.Realtime.Proto.PingResult>
struct ObjectPool_1_t8F0C222FCC56B64A48DAB3A567700ABA0399B52F;
// GameSparks.RT.Pools.ObjectPool`1<Com.Gamesparks.Realtime.Proto.PlayerConnectMessage>
struct ObjectPool_1_t7DB42E1475B30FA37D53C7FF70B8B9CFA9F5CF96;
// GameSparks.RT.Pools.ObjectPool`1<Com.Gamesparks.Realtime.Proto.PlayerDisconnectMessage>
struct ObjectPool_1_t9D58C721119925F211D6FDB5EDB5CE0475FA6CC4;
// GameSparks.RT.Pools.ObjectPool`1<Com.Gamesparks.Realtime.Proto.UDPConnectMessage>
struct ObjectPool_1_t996F5BA919CE3D0E3F28EBBF78FC7F14DC28B1D4;
// GameSparks.RT.Pools.ObjectPool`1<GameSparks.RT.ActionCommand>
struct ObjectPool_1_t7A3D7F441643094188CBBEFF79A6E0A7CF27CD6A;
// GameSparks.RT.Pools.ObjectPool`1<GameSparks.RT.CustomCommand>
struct ObjectPool_1_tC87E9C08820885D50C4DA65B14AC6CA831C999D9;
// GameSparks.RT.Pools.ObjectPool`1<GameSparks.RT.CustomRequest>
struct ObjectPool_1_t99EEABFA3208E2FCCE778699E9356B641EB3D3E0;
// GameSparks.RT.Pools.ObjectPool`1<GameSparks.RT.LogCommand>
struct ObjectPool_1_tC8B4FEB0616D82A9717C7BF763BC1F503354C101;
// GameSparks.RT.Pools.ObjectPool`1<GameSparks.RT.PositionStream>
struct ObjectPool_1_t687E4BDB9561517BD2A6A770F8F018BD8A09672B;
// GameSparks.RT.Pools.ObjectPool`1<GameSparks.RT.Proto.LimitedPositionStream>
struct ObjectPool_1_t35B625DF2B797C78F590E17AD376C64BC732A5D7;
// GameSparks.RT.Pools.ObjectPool`1<GameSparks.RT.Proto.Packet>
struct ObjectPool_1_t8C96F276E9A155DB89566AE06CE0A1FC52D8CA11;
// GameSparks.RT.Pools.ObjectPool`1<GameSparks.RT.RTData>
struct ObjectPool_1_t09E1265B298142F1911B97FA131C330D38470C3B;
// GameSparks.RT.Pools.ObjectPool`1<System.Byte[]>
struct ObjectPool_1_tCB76812153C38C9C5A0CABEC2F052F40CC031052;
// GameSparks.RT.Pools.ObjectPool`1<System.IO.MemoryStream>
struct ObjectPool_1_t967B8446B741B8199B5557F4A8796A30BD1B69CC;
// GameSparks.RT.Proto.LimitedPositionStream
struct LimitedPositionStream_t39D3BF1EF4DFE19E62D02F20AA326007FD4A2853;
// GameSparks.RT.Proto.Packet
struct Packet_tA4653355695B640FDF2DC08872C1BA9D1AB35302;
// GameSparks.RT.Proto.RTVal[]
struct RTValU5BU5D_tE5FC34AC1C2A0E59370CB42297155A93630BA90C;
// GameSparks.RT.RTData
struct RTData_tAAEBF5D36A61DACA40791F6B7E2D59B7FAF099B5;
// GameSparks.RT.RTSessionImpl
struct RTSessionImpl_t4277EBD12AF635CB4E621DF4B4C4731EF1A452D9;
// Org.BouncyCastle.Crypto.Tls.TlsCipherFactory
struct TlsCipherFactory_t5995BB8E2767BA0519D3BA1629F238C499C47DE9;
// Org.BouncyCastle.Crypto.Tls.TlsClientContext
struct TlsClientContext_tE01FA93AE9165EE22FA37C2E6B3C32E99C17B989;
// Org.BouncyCastle.X509.X509Certificate
struct X509Certificate_t9350C69721F95FA663C0FD6ED97CDF09B8037786;
// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.Action`1<Com.Gamesparks.Realtime.Proto.LoginResult>
struct Action_1_t8E19666AC943A7CE517BCF84AF96BCC79138A56A;
// System.Action`1<Com.Gamesparks.Realtime.Proto.PlayerConnectMessage>
struct Action_1_t1723845D1C9251C8FA6C5CBFBB84466C0CE68229;
// System.Action`1<Com.Gamesparks.Realtime.Proto.PlayerDisconnectMessage>
struct Action_1_tE2B49A1830FEAB9AA68E5DEEC099689FD03BC41B;
// System.Action`1<GameSparks.Api.Messages.GSMessage>
struct Action_1_tC7AE881848A9CC14DD6D276EF7D979608CC4B230;
// System.Action`1<GameSparks.Core.GSObject>
struct Action_1_t56370BF80900566597FF1377284D989943C5D99B;
// System.Action`1<GameSparks.RT.CustomRequest>
struct Action_1_t197ECDDFA194D1F002079DDE8314AE721A381509;
// System.Action`1<GameSparks.RT.Proto.Packet>
struct Action_1_t12A5A30426390F1E09FD7F9F5DF7BA10DBF8F644;
// System.Action`1<GameSparks.RT.Proto.RTVal>
struct Action_1_t5C9104C9FF0C575595F817B528EEEFF321E339DA;
// System.Action`1<GameSparks.RT.RTData>
struct Action_1_tE2593A351801F7E00C62A5439E6E5E9E1DBA93EB;
// System.Action`1<System.IO.MemoryStream>
struct Action_1_t9976A126D9E8071B700A6C53DBE8F643532E81B9;
// System.Action`1<System.String>
struct Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.IDictionary`2<System.Int32,System.Int32>
struct IDictionary_2_t4BA061A629B701D5E5FAF7870E20AF0C68331CD5;
// System.Collections.Generic.IDictionary`2<System.String,GameSparks.Core.GSRequest>
struct IDictionary_2_t2383A6A6E79846F0CE5DBCFCC5FE1174DD34297E;
// System.Collections.Generic.IDictionary`2<System.String,GameSparks.RT.GameSparksRT/LogLevel>
struct IDictionary_2_t02ADC368569374B6AEB91AC70296A6CFA0A56B31;
// System.Collections.Generic.IDictionary`2<System.String,System.Object>
struct IDictionary_2_t1E5681AFFD86E8189077B1EE929272E2AF245A91;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226;
// System.Collections.Generic.Queue`1<GameSparks.RT.IRTCommand>
struct Queue_1_tE5149A28B77E4439EB38C948F8E6D0B1E40B6077;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.Collections.IList
struct IList_tA637AB426E16F84F84ACC2813BDCF3A0414AF0AA;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.Func`1<Com.Gamesparks.Realtime.Proto.LoginResult>
struct Func_1_tBB3636679D74D021FCD8D4CA1CD998DA620A85F3;
// System.Func`1<Com.Gamesparks.Realtime.Proto.PingResult>
struct Func_1_t57B3C29C4D2B68BA53394E06D0F5C21047A15B22;
// System.Func`1<Com.Gamesparks.Realtime.Proto.PlayerConnectMessage>
struct Func_1_t8F6CDB5B6B554B4CE0B6D17AF35C472CADCF43B6;
// System.Func`1<Com.Gamesparks.Realtime.Proto.PlayerDisconnectMessage>
struct Func_1_tA06E28D621F9C910491509E129922CCCB571331F;
// System.Func`1<Com.Gamesparks.Realtime.Proto.UDPConnectMessage>
struct Func_1_t31BC4BB9CACE00A4655732308C4BAEBFB3D29A09;
// System.Func`1<GameSparks.RT.ActionCommand>
struct Func_1_tB02AF8569C4330A1EA8B2913D033F15317DC70A2;
// System.Func`1<GameSparks.RT.CustomCommand>
struct Func_1_t3E7D37C35670ED400D34C0A6EB45A27F67A03EA2;
// System.Func`1<GameSparks.RT.CustomRequest>
struct Func_1_tE2B79A974D5313D75912E5CF0AE834349DA9E88A;
// System.Func`1<GameSparks.RT.LogCommand>
struct Func_1_t37ED4AEA3737F590CC17C3E757B5281A1CAEE93A;
// System.Func`1<GameSparks.RT.PositionStream>
struct Func_1_t81E2F21B4E1E83A5964A4D5467CB08F66F5BA6E5;
// System.Func`1<GameSparks.RT.Proto.LimitedPositionStream>
struct Func_1_t34CAFB160E22484916C3F64AC903AF24ED90A2F4;
// System.Func`1<GameSparks.RT.Proto.Packet>
struct Func_1_tE9D78C499252D04A9430D89E9FA8A6B575B25DDB;
// System.Func`1<GameSparks.RT.RTData>
struct Func_1_t0A82329474D3D4D2B1C381EA917058A8236AB71A;
// System.Func`1<System.Byte[]>
struct Func_1_t53A6D1525C5DAC36936621EB7D2555B8CF99F00B;
// System.Func`1<System.IO.MemoryStream>
struct Func_1_t53720039ECB1D625D6FB185F4B11B761738A110A;
// System.Func`2<GameSparks.Core.GSData,GameSparks.Api.Responses.LeaderboardDataResponse/_LeaderboardData>
struct Func_2_t08297BFF4A7645AA75684B072ECC0AF4D43AC2EF;
// System.Func`2<GameSparks.Core.GSData,GameSparks.Api.Responses.ListAchievementsResponse/_Achievement>
struct Func_2_t5629C21721ADCAE00DF8837F21427A89252D0F91;
// System.Func`2<GameSparks.Core.GSData,GameSparks.Api.Responses.ListChallengeTypeResponse/_ChallengeType>
struct Func_2_t5C8807919A4FE1E185DE9E06963B3D039D5A2A60;
// System.Func`2<GameSparks.Core.GSData,GameSparks.Api.Responses.ListGameFriendsResponse/_Player>
struct Func_2_t5F4F2DC35BE82525069649BE26778B1BA06CDCEF;
// System.Func`2<GameSparks.Core.GSData,GameSparks.Api.Responses.ListVirtualGoodsResponse/_VirtualGood>
struct Func_2_t3B1B6C18C5B135652246DF62DA2EFE21C6FC2926;
// System.Func`2<GameSparks.Core.GSData,GameSparks.Core.GSData>
struct Func_2_t703C40526E5E529BF24AFBA1F56E645E730ABC2F;
// System.Func`2<Org.BouncyCastle.Crypto.Tls.Certificate,System.String>
struct Func_2_t81AB5F1E16530F25286C7FCBA77049466BE9C90A;
// System.IO.BinaryReader
struct BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969;
// System.IO.BinaryWriter
struct BinaryWriter_t1B8E78B6CACFBFB57BF140FB4DECE19AFD249CC3;
// System.IO.MemoryStream
struct MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C;
// System.IO.Stream
struct Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7;
// System.IO.Stream/ReadWriteTask
struct ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80;
// System.IO.StringReader
struct StringReader_t3095DEB3D26F40D1A7F9B76835D80AFE70E47E12;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.Net.IPEndPoint
struct IPEndPoint_tCD29981135F7B1989C3845BF455AD44EBC13DE3F;
// System.Net.Sockets.TcpClient
struct TcpClient_t8BC37A84681D1839590AE10B14C25BA473063EDB;
// System.Net.Sockets.UdpClient
struct UdpClient_t94741C2FBA0D9E3270ABDDBA57811D00881D5641;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Random
struct Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.String
struct String_t;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.Threading.SemaphoreSlim
struct SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048;
// System.Threading.Tasks.Task`1<System.Int32>
struct Task_1_t640F0CBB720BB9CD14B90B7B81624471A9F56D87;

struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;



#ifndef U3CMODULEU3E_T8653A0D7DD0F6B987B0267644F0D933874D889DB_H
#define U3CMODULEU3E_T8653A0D7DD0F6B987B0267644F0D933874D889DB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t8653A0D7DD0F6B987B0267644F0D933874D889DB 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T8653A0D7DD0F6B987B0267644F0D933874D889DB_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef GSMESSAGEHANDLER_T99F7908B45FA124CC5B990D5C1EBC9E61ED6AD47_H
#define GSMESSAGEHANDLER_T99F7908B45FA124CC5B990D5C1EBC9E61ED6AD47_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.GSMessageHandler
struct  GSMessageHandler_t99F7908B45FA124CC5B990D5C1EBC9E61ED6AD47  : public RuntimeObject
{
public:

public:
};

struct GSMessageHandler_t99F7908B45FA124CC5B990D5C1EBC9E61ED6AD47_StaticFields
{
public:
	// System.Action`1<GameSparks.Api.Messages.GSMessage> GameSparks.Api.GSMessageHandler::_AllMessages
	Action_1_tC7AE881848A9CC14DD6D276EF7D979608CC4B230 * ____AllMessages_0;

public:
	inline static int32_t get_offset_of__AllMessages_0() { return static_cast<int32_t>(offsetof(GSMessageHandler_t99F7908B45FA124CC5B990D5C1EBC9E61ED6AD47_StaticFields, ____AllMessages_0)); }
	inline Action_1_tC7AE881848A9CC14DD6D276EF7D979608CC4B230 * get__AllMessages_0() const { return ____AllMessages_0; }
	inline Action_1_tC7AE881848A9CC14DD6D276EF7D979608CC4B230 ** get_address_of__AllMessages_0() { return &____AllMessages_0; }
	inline void set__AllMessages_0(Action_1_tC7AE881848A9CC14DD6D276EF7D979608CC4B230 * value)
	{
		____AllMessages_0 = value;
		Il2CppCodeGenWriteBarrier((&____AllMessages_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GSMESSAGEHANDLER_T99F7908B45FA124CC5B990D5C1EBC9E61ED6AD47_H
#ifndef QUEUEPERSISTOR_TDF28BC34A9B083A5F2AF6ADA15BA4678741BFBE8_H
#define QUEUEPERSISTOR_TDF28BC34A9B083A5F2AF6ADA15BA4678741BFBE8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.QueuePersistor
struct  QueuePersistor_tDF28BC34A9B083A5F2AF6ADA15BA4678741BFBE8  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUEUEPERSISTOR_TDF28BC34A9B083A5F2AF6ADA15BA4678741BFBE8_H
#ifndef GS_TEA670BED248D00788819C2E9B0FE944F560383AD_H
#define GS_TEA670BED248D00788819C2E9B0FE944F560383AD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Core.GS
struct  GS_tEA670BED248D00788819C2E9B0FE944F560383AD  : public RuntimeObject
{
public:

public:
};

struct GS_tEA670BED248D00788819C2E9B0FE944F560383AD_StaticFields
{
public:
	// GameSparks.Core.GSInstance GameSparks.Core.GS::_instance
	GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774 * ____instance_0;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(GS_tEA670BED248D00788819C2E9B0FE944F560383AD_StaticFields, ____instance_0)); }
	inline GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774 * get__instance_0() const { return ____instance_0; }
	inline GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774 ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774 * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier((&____instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GS_TEA670BED248D00788819C2E9B0FE944F560383AD_H
#ifndef GSCONNECTION_TDCDCFD82EA16BD7E6FEF1CE33FD247F30661E56C_H
#define GSCONNECTION_TDCDCFD82EA16BD7E6FEF1CE33FD247F30661E56C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Core.GSConnection
struct  GSConnection_tDCDCFD82EA16BD7E6FEF1CE33FD247F30661E56C  : public RuntimeObject
{
public:
	// System.Collections.Generic.IDictionary`2<System.String,GameSparks.Core.GSRequest> GameSparks.Core.GSConnection::_pendingRequests
	RuntimeObject* ____pendingRequests_0;
	// System.Boolean GameSparks.Core.GSConnection::_stopped
	bool ____stopped_1;
	// System.String GameSparks.Core.GSConnection::url
	String_t* ___url_2;
	// GameSparks.IGameSparksWebSocket GameSparks.Core.GSConnection::_WebSocketClient
	RuntimeObject* ____WebSocketClient_3;
	// GameSparks.Core.GSInstance GameSparks.Core.GSConnection::_gs
	GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774 * ____gs_4;
	// GameSparks.Core.IGSPlatform GameSparks.Core.GSConnection::_gSPlatform
	RuntimeObject* ____gSPlatform_5;
	// System.Int64 GameSparks.Core.GSConnection::mustConnectBy
	int64_t ___mustConnectBy_6;
	// System.Boolean GameSparks.Core.GSConnection::_initialised
	bool ____initialised_7;
	// System.String GameSparks.Core.GSConnection::<SessionId>k__BackingField
	String_t* ___U3CSessionIdU3Ek__BackingField_8;

public:
	inline static int32_t get_offset_of__pendingRequests_0() { return static_cast<int32_t>(offsetof(GSConnection_tDCDCFD82EA16BD7E6FEF1CE33FD247F30661E56C, ____pendingRequests_0)); }
	inline RuntimeObject* get__pendingRequests_0() const { return ____pendingRequests_0; }
	inline RuntimeObject** get_address_of__pendingRequests_0() { return &____pendingRequests_0; }
	inline void set__pendingRequests_0(RuntimeObject* value)
	{
		____pendingRequests_0 = value;
		Il2CppCodeGenWriteBarrier((&____pendingRequests_0), value);
	}

	inline static int32_t get_offset_of__stopped_1() { return static_cast<int32_t>(offsetof(GSConnection_tDCDCFD82EA16BD7E6FEF1CE33FD247F30661E56C, ____stopped_1)); }
	inline bool get__stopped_1() const { return ____stopped_1; }
	inline bool* get_address_of__stopped_1() { return &____stopped_1; }
	inline void set__stopped_1(bool value)
	{
		____stopped_1 = value;
	}

	inline static int32_t get_offset_of_url_2() { return static_cast<int32_t>(offsetof(GSConnection_tDCDCFD82EA16BD7E6FEF1CE33FD247F30661E56C, ___url_2)); }
	inline String_t* get_url_2() const { return ___url_2; }
	inline String_t** get_address_of_url_2() { return &___url_2; }
	inline void set_url_2(String_t* value)
	{
		___url_2 = value;
		Il2CppCodeGenWriteBarrier((&___url_2), value);
	}

	inline static int32_t get_offset_of__WebSocketClient_3() { return static_cast<int32_t>(offsetof(GSConnection_tDCDCFD82EA16BD7E6FEF1CE33FD247F30661E56C, ____WebSocketClient_3)); }
	inline RuntimeObject* get__WebSocketClient_3() const { return ____WebSocketClient_3; }
	inline RuntimeObject** get_address_of__WebSocketClient_3() { return &____WebSocketClient_3; }
	inline void set__WebSocketClient_3(RuntimeObject* value)
	{
		____WebSocketClient_3 = value;
		Il2CppCodeGenWriteBarrier((&____WebSocketClient_3), value);
	}

	inline static int32_t get_offset_of__gs_4() { return static_cast<int32_t>(offsetof(GSConnection_tDCDCFD82EA16BD7E6FEF1CE33FD247F30661E56C, ____gs_4)); }
	inline GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774 * get__gs_4() const { return ____gs_4; }
	inline GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774 ** get_address_of__gs_4() { return &____gs_4; }
	inline void set__gs_4(GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774 * value)
	{
		____gs_4 = value;
		Il2CppCodeGenWriteBarrier((&____gs_4), value);
	}

	inline static int32_t get_offset_of__gSPlatform_5() { return static_cast<int32_t>(offsetof(GSConnection_tDCDCFD82EA16BD7E6FEF1CE33FD247F30661E56C, ____gSPlatform_5)); }
	inline RuntimeObject* get__gSPlatform_5() const { return ____gSPlatform_5; }
	inline RuntimeObject** get_address_of__gSPlatform_5() { return &____gSPlatform_5; }
	inline void set__gSPlatform_5(RuntimeObject* value)
	{
		____gSPlatform_5 = value;
		Il2CppCodeGenWriteBarrier((&____gSPlatform_5), value);
	}

	inline static int32_t get_offset_of_mustConnectBy_6() { return static_cast<int32_t>(offsetof(GSConnection_tDCDCFD82EA16BD7E6FEF1CE33FD247F30661E56C, ___mustConnectBy_6)); }
	inline int64_t get_mustConnectBy_6() const { return ___mustConnectBy_6; }
	inline int64_t* get_address_of_mustConnectBy_6() { return &___mustConnectBy_6; }
	inline void set_mustConnectBy_6(int64_t value)
	{
		___mustConnectBy_6 = value;
	}

	inline static int32_t get_offset_of__initialised_7() { return static_cast<int32_t>(offsetof(GSConnection_tDCDCFD82EA16BD7E6FEF1CE33FD247F30661E56C, ____initialised_7)); }
	inline bool get__initialised_7() const { return ____initialised_7; }
	inline bool* get_address_of__initialised_7() { return &____initialised_7; }
	inline void set__initialised_7(bool value)
	{
		____initialised_7 = value;
	}

	inline static int32_t get_offset_of_U3CSessionIdU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(GSConnection_tDCDCFD82EA16BD7E6FEF1CE33FD247F30661E56C, ___U3CSessionIdU3Ek__BackingField_8)); }
	inline String_t* get_U3CSessionIdU3Ek__BackingField_8() const { return ___U3CSessionIdU3Ek__BackingField_8; }
	inline String_t** get_address_of_U3CSessionIdU3Ek__BackingField_8() { return &___U3CSessionIdU3Ek__BackingField_8; }
	inline void set_U3CSessionIdU3Ek__BackingField_8(String_t* value)
	{
		___U3CSessionIdU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSessionIdU3Ek__BackingField_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GSCONNECTION_TDCDCFD82EA16BD7E6FEF1CE33FD247F30661E56C_H
#ifndef U3CU3EC__DISPLAYCLASS2_T9E1A033FB6AA77062D78CFFFF5E451797715224C_H
#define U3CU3EC__DISPLAYCLASS2_T9E1A033FB6AA77062D78CFFFF5E451797715224C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Core.GSConnection_<>c__DisplayClass2
struct  U3CU3Ec__DisplayClass2_t9E1A033FB6AA77062D78CFFFF5E451797715224C  : public RuntimeObject
{
public:
	// GameSparks.Core.GSConnection GameSparks.Core.GSConnection_<>c__DisplayClass2::<>4__this
	GSConnection_tDCDCFD82EA16BD7E6FEF1CE33FD247F30661E56C * ___U3CU3E4__this_0;
	// System.String GameSparks.Core.GSConnection_<>c__DisplayClass2::errorMessage
	String_t* ___errorMessage_1;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_t9E1A033FB6AA77062D78CFFFF5E451797715224C, ___U3CU3E4__this_0)); }
	inline GSConnection_tDCDCFD82EA16BD7E6FEF1CE33FD247F30661E56C * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline GSConnection_tDCDCFD82EA16BD7E6FEF1CE33FD247F30661E56C ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(GSConnection_tDCDCFD82EA16BD7E6FEF1CE33FD247F30661E56C * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_errorMessage_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_t9E1A033FB6AA77062D78CFFFF5E451797715224C, ___errorMessage_1)); }
	inline String_t* get_errorMessage_1() const { return ___errorMessage_1; }
	inline String_t** get_address_of_errorMessage_1() { return &___errorMessage_1; }
	inline void set_errorMessage_1(String_t* value)
	{
		___errorMessage_1 = value;
		Il2CppCodeGenWriteBarrier((&___errorMessage_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS2_T9E1A033FB6AA77062D78CFFFF5E451797715224C_H
#ifndef U3CU3EC__DISPLAYCLASS7_T0E3A1828A4229CA277C0866A600A81766B353B9B_H
#define U3CU3EC__DISPLAYCLASS7_T0E3A1828A4229CA277C0866A600A81766B353B9B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Core.GSConnection_<>c__DisplayClass7
struct  U3CU3Ec__DisplayClass7_t0E3A1828A4229CA277C0866A600A81766B353B9B  : public RuntimeObject
{
public:
	// GameSparks.Core.GSConnection GameSparks.Core.GSConnection_<>c__DisplayClass7::<>4__this
	GSConnection_tDCDCFD82EA16BD7E6FEF1CE33FD247F30661E56C * ___U3CU3E4__this_0;
	// System.String GameSparks.Core.GSConnection_<>c__DisplayClass7::message
	String_t* ___message_1;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass7_t0E3A1828A4229CA277C0866A600A81766B353B9B, ___U3CU3E4__this_0)); }
	inline GSConnection_tDCDCFD82EA16BD7E6FEF1CE33FD247F30661E56C * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline GSConnection_tDCDCFD82EA16BD7E6FEF1CE33FD247F30661E56C ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(GSConnection_tDCDCFD82EA16BD7E6FEF1CE33FD247F30661E56C * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_message_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass7_t0E3A1828A4229CA277C0866A600A81766B353B9B, ___message_1)); }
	inline String_t* get_message_1() const { return ___message_1; }
	inline String_t** get_address_of_message_1() { return &___message_1; }
	inline void set_message_1(String_t* value)
	{
		___message_1 = value;
		Il2CppCodeGenWriteBarrier((&___message_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS7_T0E3A1828A4229CA277C0866A600A81766B353B9B_H
#ifndef U3CU3EC__DISPLAYCLASSA_T3DBD47D1B85DBAB0C86707553F3B135711C434AD_H
#define U3CU3EC__DISPLAYCLASSA_T3DBD47D1B85DBAB0C86707553F3B135711C434AD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Core.GSConnection_<>c__DisplayClassa
struct  U3CU3Ec__DisplayClassa_t3DBD47D1B85DBAB0C86707553F3B135711C434AD  : public RuntimeObject
{
public:
	// GameSparks.Core.GSConnection_<>c__DisplayClass7 GameSparks.Core.GSConnection_<>c__DisplayClassa::CSU24<>8__locals8
	U3CU3Ec__DisplayClass7_t0E3A1828A4229CA277C0866A600A81766B353B9B * ___CSU24U3CU3E8__locals8_0;
	// System.String GameSparks.Core.GSConnection_<>c__DisplayClassa::json
	String_t* ___json_1;

public:
	inline static int32_t get_offset_of_CSU24U3CU3E8__locals8_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClassa_t3DBD47D1B85DBAB0C86707553F3B135711C434AD, ___CSU24U3CU3E8__locals8_0)); }
	inline U3CU3Ec__DisplayClass7_t0E3A1828A4229CA277C0866A600A81766B353B9B * get_CSU24U3CU3E8__locals8_0() const { return ___CSU24U3CU3E8__locals8_0; }
	inline U3CU3Ec__DisplayClass7_t0E3A1828A4229CA277C0866A600A81766B353B9B ** get_address_of_CSU24U3CU3E8__locals8_0() { return &___CSU24U3CU3E8__locals8_0; }
	inline void set_CSU24U3CU3E8__locals8_0(U3CU3Ec__DisplayClass7_t0E3A1828A4229CA277C0866A600A81766B353B9B * value)
	{
		___CSU24U3CU3E8__locals8_0 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E8__locals8_0), value);
	}

	inline static int32_t get_offset_of_json_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClassa_t3DBD47D1B85DBAB0C86707553F3B135711C434AD, ___json_1)); }
	inline String_t* get_json_1() const { return ___json_1; }
	inline String_t** get_address_of_json_1() { return &___json_1; }
	inline void set_json_1(String_t* value)
	{
		___json_1 = value;
		Il2CppCodeGenWriteBarrier((&___json_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASSA_T3DBD47D1B85DBAB0C86707553F3B135711C434AD_H
#ifndef GSDATA_T122BC20340935FE4E4E6F79E9A4E2F7C48844937_H
#define GSDATA_T122BC20340935FE4E4E6F79E9A4E2F7C48844937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Core.GSData
struct  GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937  : public RuntimeObject
{
public:
	// System.Collections.Generic.IDictionary`2<System.String,System.Object> GameSparks.Core.GSData::_data
	RuntimeObject* ____data_0;

public:
	inline static int32_t get_offset_of__data_0() { return static_cast<int32_t>(offsetof(GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937, ____data_0)); }
	inline RuntimeObject* get__data_0() const { return ____data_0; }
	inline RuntimeObject** get_address_of__data_0() { return &____data_0; }
	inline void set__data_0(RuntimeObject* value)
	{
		____data_0 = value;
		Il2CppCodeGenWriteBarrier((&____data_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GSDATA_T122BC20340935FE4E4E6F79E9A4E2F7C48844937_H
#ifndef GSJSON_T737B8292A68C0137A1F0EEE67C5EFB7632718151_H
#define GSJSON_T737B8292A68C0137A1F0EEE67C5EFB7632718151_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Core.GSJson
struct  GSJson_t737B8292A68C0137A1F0EEE67C5EFB7632718151  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GSJSON_T737B8292A68C0137A1F0EEE67C5EFB7632718151_H
#ifndef PARSER_TE75E1835B1CFAC9CB4174AC7D57131286EE6793D_H
#define PARSER_TE75E1835B1CFAC9CB4174AC7D57131286EE6793D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Core.GSJson_Parser
struct  Parser_tE75E1835B1CFAC9CB4174AC7D57131286EE6793D  : public RuntimeObject
{
public:
	// System.Int32 GameSparks.Core.GSJson_Parser::depth
	int32_t ___depth_0;
	// System.IO.StringReader GameSparks.Core.GSJson_Parser::json
	StringReader_t3095DEB3D26F40D1A7F9B76835D80AFE70E47E12 * ___json_1;

public:
	inline static int32_t get_offset_of_depth_0() { return static_cast<int32_t>(offsetof(Parser_tE75E1835B1CFAC9CB4174AC7D57131286EE6793D, ___depth_0)); }
	inline int32_t get_depth_0() const { return ___depth_0; }
	inline int32_t* get_address_of_depth_0() { return &___depth_0; }
	inline void set_depth_0(int32_t value)
	{
		___depth_0 = value;
	}

	inline static int32_t get_offset_of_json_1() { return static_cast<int32_t>(offsetof(Parser_tE75E1835B1CFAC9CB4174AC7D57131286EE6793D, ___json_1)); }
	inline StringReader_t3095DEB3D26F40D1A7F9B76835D80AFE70E47E12 * get_json_1() const { return ___json_1; }
	inline StringReader_t3095DEB3D26F40D1A7F9B76835D80AFE70E47E12 ** get_address_of_json_1() { return &___json_1; }
	inline void set_json_1(StringReader_t3095DEB3D26F40D1A7F9B76835D80AFE70E47E12 * value)
	{
		___json_1 = value;
		Il2CppCodeGenWriteBarrier((&___json_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARSER_TE75E1835B1CFAC9CB4174AC7D57131286EE6793D_H
#ifndef SERIALIZER_TEC0B3F9938321DCC6C37DF0A6F23B67C318D095A_H
#define SERIALIZER_TEC0B3F9938321DCC6C37DF0A6F23B67C318D095A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Core.GSJson_Serializer
struct  Serializer_tEC0B3F9938321DCC6C37DF0A6F23B67C318D095A  : public RuntimeObject
{
public:
	// System.Text.StringBuilder GameSparks.Core.GSJson_Serializer::builder
	StringBuilder_t * ___builder_0;

public:
	inline static int32_t get_offset_of_builder_0() { return static_cast<int32_t>(offsetof(Serializer_tEC0B3F9938321DCC6C37DF0A6F23B67C318D095A, ___builder_0)); }
	inline StringBuilder_t * get_builder_0() const { return ___builder_0; }
	inline StringBuilder_t ** get_address_of_builder_0() { return &___builder_0; }
	inline void set_builder_0(StringBuilder_t * value)
	{
		___builder_0 = value;
		Il2CppCodeGenWriteBarrier((&___builder_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZER_TEC0B3F9938321DCC6C37DF0A6F23B67C318D095A_H
#ifndef U3CU3EC__DISPLAYCLASS4_TA8F6C234BF13DB45BB88E9AD09C4E3CBE301EAC1_H
#define U3CU3EC__DISPLAYCLASS4_TA8F6C234BF13DB45BB88E9AD09C4E3CBE301EAC1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Core.GSRequest_<>c__DisplayClass4
struct  U3CU3Ec__DisplayClass4_tA8F6C234BF13DB45BB88E9AD09C4E3CBE301EAC1  : public RuntimeObject
{
public:
	// GameSparks.Core.GSRequest GameSparks.Core.GSRequest_<>c__DisplayClass4::<>4__this
	GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 * ___U3CU3E4__this_0;
	// GameSparks.Core.GSObject GameSparks.Core.GSRequest_<>c__DisplayClass4::response
	GSObject_t05160F6677D08913C6CD9AB7C7DD9454D3B12E88 * ___response_1;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_tA8F6C234BF13DB45BB88E9AD09C4E3CBE301EAC1, ___U3CU3E4__this_0)); }
	inline GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_response_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_tA8F6C234BF13DB45BB88E9AD09C4E3CBE301EAC1, ___response_1)); }
	inline GSObject_t05160F6677D08913C6CD9AB7C7DD9454D3B12E88 * get_response_1() const { return ___response_1; }
	inline GSObject_t05160F6677D08913C6CD9AB7C7DD9454D3B12E88 ** get_address_of_response_1() { return &___response_1; }
	inline void set_response_1(GSObject_t05160F6677D08913C6CD9AB7C7DD9454D3B12E88 * value)
	{
		___response_1 = value;
		Il2CppCodeGenWriteBarrier((&___response_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS4_TA8F6C234BF13DB45BB88E9AD09C4E3CBE301EAC1_H
#ifndef GSTYPEDREQUEST_2_TB77B5A1314948016CD9A781F0F16C1DAB6D9DBDB_H
#define GSTYPEDREQUEST_2_TB77B5A1314948016CD9A781F0F16C1DAB6D9DBDB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Core.GSTypedRequest`2<GameSparks.Api.Requests.GetMyTeamsRequest,GameSparks.Api.Responses.GetMyTeamsResponse>
struct  GSTypedRequest_2_tB77B5A1314948016CD9A781F0F16C1DAB6D9DBDB  : public RuntimeObject
{
public:
	// GameSparks.Core.GSRequest GameSparks.Core.GSTypedRequest`2::request
	GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 * ___request_0;

public:
	inline static int32_t get_offset_of_request_0() { return static_cast<int32_t>(offsetof(GSTypedRequest_2_tB77B5A1314948016CD9A781F0F16C1DAB6D9DBDB, ___request_0)); }
	inline GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 * get_request_0() const { return ___request_0; }
	inline GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 ** get_address_of_request_0() { return &___request_0; }
	inline void set_request_0(GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 * value)
	{
		___request_0 = value;
		Il2CppCodeGenWriteBarrier((&___request_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GSTYPEDREQUEST_2_TB77B5A1314948016CD9A781F0F16C1DAB6D9DBDB_H
#ifndef GSTYPEDREQUEST_2_T5E8DFDFF992B87841B32CD0C82F09EC155DE2488_H
#define GSTYPEDREQUEST_2_T5E8DFDFF992B87841B32CD0C82F09EC155DE2488_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Core.GSTypedRequest`2<GameSparks.Api.Requests.JoinTeamRequest,GameSparks.Api.Responses.JoinTeamResponse>
struct  GSTypedRequest_2_t5E8DFDFF992B87841B32CD0C82F09EC155DE2488  : public RuntimeObject
{
public:
	// GameSparks.Core.GSRequest GameSparks.Core.GSTypedRequest`2::request
	GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 * ___request_0;

public:
	inline static int32_t get_offset_of_request_0() { return static_cast<int32_t>(offsetof(GSTypedRequest_2_t5E8DFDFF992B87841B32CD0C82F09EC155DE2488, ___request_0)); }
	inline GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 * get_request_0() const { return ___request_0; }
	inline GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 ** get_address_of_request_0() { return &___request_0; }
	inline void set_request_0(GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 * value)
	{
		___request_0 = value;
		Il2CppCodeGenWriteBarrier((&___request_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GSTYPEDREQUEST_2_T5E8DFDFF992B87841B32CD0C82F09EC155DE2488_H
#ifndef GSTYPEDREQUEST_2_T1253C2CE913AEA6571181BED5EC386D3A3571430_H
#define GSTYPEDREQUEST_2_T1253C2CE913AEA6571181BED5EC386D3A3571430_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Core.GSTypedRequest`2<GameSparks.Api.Requests.LeaderboardDataRequest,GameSparks.Api.Responses.LeaderboardDataResponse>
struct  GSTypedRequest_2_t1253C2CE913AEA6571181BED5EC386D3A3571430  : public RuntimeObject
{
public:
	// GameSparks.Core.GSRequest GameSparks.Core.GSTypedRequest`2::request
	GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 * ___request_0;

public:
	inline static int32_t get_offset_of_request_0() { return static_cast<int32_t>(offsetof(GSTypedRequest_2_t1253C2CE913AEA6571181BED5EC386D3A3571430, ___request_0)); }
	inline GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 * get_request_0() const { return ___request_0; }
	inline GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 ** get_address_of_request_0() { return &___request_0; }
	inline void set_request_0(GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 * value)
	{
		___request_0 = value;
		Il2CppCodeGenWriteBarrier((&___request_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GSTYPEDREQUEST_2_T1253C2CE913AEA6571181BED5EC386D3A3571430_H
#ifndef GSTYPEDREQUEST_2_T1ED25D52A386079CC863B6B34027AB2F8E1F32F1_H
#define GSTYPEDREQUEST_2_T1ED25D52A386079CC863B6B34027AB2F8E1F32F1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Core.GSTypedRequest`2<GameSparks.Api.Requests.ListAchievementsRequest,GameSparks.Api.Responses.ListAchievementsResponse>
struct  GSTypedRequest_2_t1ED25D52A386079CC863B6B34027AB2F8E1F32F1  : public RuntimeObject
{
public:
	// GameSparks.Core.GSRequest GameSparks.Core.GSTypedRequest`2::request
	GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 * ___request_0;

public:
	inline static int32_t get_offset_of_request_0() { return static_cast<int32_t>(offsetof(GSTypedRequest_2_t1ED25D52A386079CC863B6B34027AB2F8E1F32F1, ___request_0)); }
	inline GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 * get_request_0() const { return ___request_0; }
	inline GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 ** get_address_of_request_0() { return &___request_0; }
	inline void set_request_0(GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 * value)
	{
		___request_0 = value;
		Il2CppCodeGenWriteBarrier((&___request_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GSTYPEDREQUEST_2_T1ED25D52A386079CC863B6B34027AB2F8E1F32F1_H
#ifndef GSTYPEDREQUEST_2_TD9074F9C135154AB5683DFDA28C5665B4587C7B2_H
#define GSTYPEDREQUEST_2_TD9074F9C135154AB5683DFDA28C5665B4587C7B2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Core.GSTypedRequest`2<GameSparks.Api.Requests.ListChallengeTypeRequest,GameSparks.Api.Responses.ListChallengeTypeResponse>
struct  GSTypedRequest_2_tD9074F9C135154AB5683DFDA28C5665B4587C7B2  : public RuntimeObject
{
public:
	// GameSparks.Core.GSRequest GameSparks.Core.GSTypedRequest`2::request
	GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 * ___request_0;

public:
	inline static int32_t get_offset_of_request_0() { return static_cast<int32_t>(offsetof(GSTypedRequest_2_tD9074F9C135154AB5683DFDA28C5665B4587C7B2, ___request_0)); }
	inline GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 * get_request_0() const { return ___request_0; }
	inline GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 ** get_address_of_request_0() { return &___request_0; }
	inline void set_request_0(GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 * value)
	{
		___request_0 = value;
		Il2CppCodeGenWriteBarrier((&___request_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GSTYPEDREQUEST_2_TD9074F9C135154AB5683DFDA28C5665B4587C7B2_H
#ifndef GSTYPEDREQUEST_2_TF2BFB2ACEACD3E6DC6BFD69D79565B6F63840CC8_H
#define GSTYPEDREQUEST_2_TF2BFB2ACEACD3E6DC6BFD69D79565B6F63840CC8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Core.GSTypedRequest`2<GameSparks.Api.Requests.ListGameFriendsRequest,GameSparks.Api.Responses.ListGameFriendsResponse>
struct  GSTypedRequest_2_tF2BFB2ACEACD3E6DC6BFD69D79565B6F63840CC8  : public RuntimeObject
{
public:
	// GameSparks.Core.GSRequest GameSparks.Core.GSTypedRequest`2::request
	GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 * ___request_0;

public:
	inline static int32_t get_offset_of_request_0() { return static_cast<int32_t>(offsetof(GSTypedRequest_2_tF2BFB2ACEACD3E6DC6BFD69D79565B6F63840CC8, ___request_0)); }
	inline GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 * get_request_0() const { return ___request_0; }
	inline GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 ** get_address_of_request_0() { return &___request_0; }
	inline void set_request_0(GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 * value)
	{
		___request_0 = value;
		Il2CppCodeGenWriteBarrier((&___request_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GSTYPEDREQUEST_2_TF2BFB2ACEACD3E6DC6BFD69D79565B6F63840CC8_H
#ifndef GSTYPEDREQUEST_2_T7CD2FA80FAD739262E447D9A106760D88D78E5BC_H
#define GSTYPEDREQUEST_2_T7CD2FA80FAD739262E447D9A106760D88D78E5BC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Core.GSTypedRequest`2<GameSparks.Api.Requests.ListMessageRequest,GameSparks.Api.Responses.ListMessageResponse>
struct  GSTypedRequest_2_t7CD2FA80FAD739262E447D9A106760D88D78E5BC  : public RuntimeObject
{
public:
	// GameSparks.Core.GSRequest GameSparks.Core.GSTypedRequest`2::request
	GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 * ___request_0;

public:
	inline static int32_t get_offset_of_request_0() { return static_cast<int32_t>(offsetof(GSTypedRequest_2_t7CD2FA80FAD739262E447D9A106760D88D78E5BC, ___request_0)); }
	inline GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 * get_request_0() const { return ___request_0; }
	inline GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 ** get_address_of_request_0() { return &___request_0; }
	inline void set_request_0(GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 * value)
	{
		___request_0 = value;
		Il2CppCodeGenWriteBarrier((&___request_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GSTYPEDREQUEST_2_T7CD2FA80FAD739262E447D9A106760D88D78E5BC_H
#ifndef GSTYPEDREQUEST_2_TE6D4BFE07E547DA5A6D25BD1F1876809F844FE5C_H
#define GSTYPEDREQUEST_2_TE6D4BFE07E547DA5A6D25BD1F1876809F844FE5C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Core.GSTypedRequest`2<GameSparks.Api.Requests.ListTeamChatRequest,GameSparks.Api.Responses.ListTeamChatResponse>
struct  GSTypedRequest_2_tE6D4BFE07E547DA5A6D25BD1F1876809F844FE5C  : public RuntimeObject
{
public:
	// GameSparks.Core.GSRequest GameSparks.Core.GSTypedRequest`2::request
	GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 * ___request_0;

public:
	inline static int32_t get_offset_of_request_0() { return static_cast<int32_t>(offsetof(GSTypedRequest_2_tE6D4BFE07E547DA5A6D25BD1F1876809F844FE5C, ___request_0)); }
	inline GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 * get_request_0() const { return ___request_0; }
	inline GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 ** get_address_of_request_0() { return &___request_0; }
	inline void set_request_0(GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 * value)
	{
		___request_0 = value;
		Il2CppCodeGenWriteBarrier((&___request_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GSTYPEDREQUEST_2_TE6D4BFE07E547DA5A6D25BD1F1876809F844FE5C_H
#ifndef GSTYPEDREQUEST_2_TA8F45AAB4253EF9A6EC90379068B188E366D8BDD_H
#define GSTYPEDREQUEST_2_TA8F45AAB4253EF9A6EC90379068B188E366D8BDD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Core.GSTypedRequest`2<GameSparks.Api.Requests.ListVirtualGoodsRequest,GameSparks.Api.Responses.ListVirtualGoodsResponse>
struct  GSTypedRequest_2_tA8F45AAB4253EF9A6EC90379068B188E366D8BDD  : public RuntimeObject
{
public:
	// GameSparks.Core.GSRequest GameSparks.Core.GSTypedRequest`2::request
	GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 * ___request_0;

public:
	inline static int32_t get_offset_of_request_0() { return static_cast<int32_t>(offsetof(GSTypedRequest_2_tA8F45AAB4253EF9A6EC90379068B188E366D8BDD, ___request_0)); }
	inline GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 * get_request_0() const { return ___request_0; }
	inline GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 ** get_address_of_request_0() { return &___request_0; }
	inline void set_request_0(GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 * value)
	{
		___request_0 = value;
		Il2CppCodeGenWriteBarrier((&___request_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GSTYPEDREQUEST_2_TA8F45AAB4253EF9A6EC90379068B188E366D8BDD_H
#ifndef GSTYPEDREQUEST_2_TEF69F13CA92461C90382DE543D9F0AF4FCEB9B47_H
#define GSTYPEDREQUEST_2_TEF69F13CA92461C90382DE543D9F0AF4FCEB9B47_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Core.GSTypedRequest`2<GameSparks.Api.Requests.LogEventRequest,GameSparks.Api.Responses.LogEventResponse>
struct  GSTypedRequest_2_tEF69F13CA92461C90382DE543D9F0AF4FCEB9B47  : public RuntimeObject
{
public:
	// GameSparks.Core.GSRequest GameSparks.Core.GSTypedRequest`2::request
	GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 * ___request_0;

public:
	inline static int32_t get_offset_of_request_0() { return static_cast<int32_t>(offsetof(GSTypedRequest_2_tEF69F13CA92461C90382DE543D9F0AF4FCEB9B47, ___request_0)); }
	inline GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 * get_request_0() const { return ___request_0; }
	inline GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 ** get_address_of_request_0() { return &___request_0; }
	inline void set_request_0(GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 * value)
	{
		___request_0 = value;
		Il2CppCodeGenWriteBarrier((&___request_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GSTYPEDREQUEST_2_TEF69F13CA92461C90382DE543D9F0AF4FCEB9B47_H
#ifndef GSTYPEDREQUEST_2_T430DD9176857443640FCA8A9934E1CA93AD44440_H
#define GSTYPEDREQUEST_2_T430DD9176857443640FCA8A9934E1CA93AD44440_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Core.GSTypedRequest`2<GameSparks.Api.Requests.SendTeamChatMessageRequest,GameSparks.Api.Responses.SendTeamChatMessageResponse>
struct  GSTypedRequest_2_t430DD9176857443640FCA8A9934E1CA93AD44440  : public RuntimeObject
{
public:
	// GameSparks.Core.GSRequest GameSparks.Core.GSTypedRequest`2::request
	GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 * ___request_0;

public:
	inline static int32_t get_offset_of_request_0() { return static_cast<int32_t>(offsetof(GSTypedRequest_2_t430DD9176857443640FCA8A9934E1CA93AD44440, ___request_0)); }
	inline GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 * get_request_0() const { return ___request_0; }
	inline GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 ** get_address_of_request_0() { return &___request_0; }
	inline void set_request_0(GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 * value)
	{
		___request_0 = value;
		Il2CppCodeGenWriteBarrier((&___request_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GSTYPEDREQUEST_2_T430DD9176857443640FCA8A9934E1CA93AD44440_H
#ifndef GSTYPEDREQUEST_2_TAA9DF7DA46AE4FF76CBE1779C3BA28556C574920_H
#define GSTYPEDREQUEST_2_TAA9DF7DA46AE4FF76CBE1779C3BA28556C574920_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Core.GSTypedRequest`2<GameSparks.Api.Requests.SocialLeaderboardDataRequest,GameSparks.Api.Responses.LeaderboardDataResponse>
struct  GSTypedRequest_2_tAA9DF7DA46AE4FF76CBE1779C3BA28556C574920  : public RuntimeObject
{
public:
	// GameSparks.Core.GSRequest GameSparks.Core.GSTypedRequest`2::request
	GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 * ___request_0;

public:
	inline static int32_t get_offset_of_request_0() { return static_cast<int32_t>(offsetof(GSTypedRequest_2_tAA9DF7DA46AE4FF76CBE1779C3BA28556C574920, ___request_0)); }
	inline GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 * get_request_0() const { return ___request_0; }
	inline GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 ** get_address_of_request_0() { return &___request_0; }
	inline void set_request_0(GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 * value)
	{
		___request_0 = value;
		Il2CppCodeGenWriteBarrier((&___request_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GSTYPEDREQUEST_2_TAA9DF7DA46AE4FF76CBE1779C3BA28556C574920_H
#ifndef GSTYPEDRESPONSE_T8D7A8C3784E1A1542ED006C5A97AF8C6D9E849DA_H
#define GSTYPEDRESPONSE_T8D7A8C3784E1A1542ED006C5A97AF8C6D9E849DA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Core.GSTypedResponse
struct  GSTypedResponse_t8D7A8C3784E1A1542ED006C5A97AF8C6D9E849DA  : public RuntimeObject
{
public:
	// GameSparks.Core.GSData GameSparks.Core.GSTypedResponse::response
	GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 * ___response_0;

public:
	inline static int32_t get_offset_of_response_0() { return static_cast<int32_t>(offsetof(GSTypedResponse_t8D7A8C3784E1A1542ED006C5A97AF8C6D9E849DA, ___response_0)); }
	inline GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 * get_response_0() const { return ___response_0; }
	inline GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 ** get_address_of_response_0() { return &___response_0; }
	inline void set_response_0(GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 * value)
	{
		___response_0 = value;
		Il2CppCodeGenWriteBarrier((&___response_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GSTYPEDRESPONSE_T8D7A8C3784E1A1542ED006C5A97AF8C6D9E849DA_H
#ifndef ABSTRACTRESULT_TB9A3C663F42A72B8FCA862FF115309B197235C96_H
#define ABSTRACTRESULT_TB9A3C663F42A72B8FCA862FF115309B197235C96_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.RT.AbstractResult
struct  AbstractResult_tB9A3C663F42A72B8FCA862FF115309B197235C96  : public RuntimeObject
{
public:
	// GameSparks.RT.Proto.Packet GameSparks.RT.AbstractResult::packet
	Packet_tA4653355695B640FDF2DC08872C1BA9D1AB35302 * ___packet_0;
	// GameSparks.RT.IRTSessionInternal GameSparks.RT.AbstractResult::session
	RuntimeObject* ___session_1;

public:
	inline static int32_t get_offset_of_packet_0() { return static_cast<int32_t>(offsetof(AbstractResult_tB9A3C663F42A72B8FCA862FF115309B197235C96, ___packet_0)); }
	inline Packet_tA4653355695B640FDF2DC08872C1BA9D1AB35302 * get_packet_0() const { return ___packet_0; }
	inline Packet_tA4653355695B640FDF2DC08872C1BA9D1AB35302 ** get_address_of_packet_0() { return &___packet_0; }
	inline void set_packet_0(Packet_tA4653355695B640FDF2DC08872C1BA9D1AB35302 * value)
	{
		___packet_0 = value;
		Il2CppCodeGenWriteBarrier((&___packet_0), value);
	}

	inline static int32_t get_offset_of_session_1() { return static_cast<int32_t>(offsetof(AbstractResult_tB9A3C663F42A72B8FCA862FF115309B197235C96, ___session_1)); }
	inline RuntimeObject* get_session_1() const { return ___session_1; }
	inline RuntimeObject** get_address_of_session_1() { return &___session_1; }
	inline void set_session_1(RuntimeObject* value)
	{
		___session_1 = value;
		Il2CppCodeGenWriteBarrier((&___session_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTRESULT_TB9A3C663F42A72B8FCA862FF115309B197235C96_H
#ifndef ACTIONCOMMAND_TFC2779E4CB5E203B26B01E72149FC02CCBC0415E_H
#define ACTIONCOMMAND_TFC2779E4CB5E203B26B01E72149FC02CCBC0415E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.RT.ActionCommand
struct  ActionCommand_tFC2779E4CB5E203B26B01E72149FC02CCBC0415E  : public RuntimeObject
{
public:
	// System.Action GameSparks.RT.ActionCommand::action
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___action_1;

public:
	inline static int32_t get_offset_of_action_1() { return static_cast<int32_t>(offsetof(ActionCommand_tFC2779E4CB5E203B26B01E72149FC02CCBC0415E, ___action_1)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_action_1() const { return ___action_1; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_action_1() { return &___action_1; }
	inline void set_action_1(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___action_1 = value;
		Il2CppCodeGenWriteBarrier((&___action_1), value);
	}
};

struct ActionCommand_tFC2779E4CB5E203B26B01E72149FC02CCBC0415E_StaticFields
{
public:
	// GameSparks.RT.Pools.ObjectPool`1<GameSparks.RT.ActionCommand> GameSparks.RT.ActionCommand::pool
	ObjectPool_1_t7A3D7F441643094188CBBEFF79A6E0A7CF27CD6A * ___pool_0;
	// System.Func`1<GameSparks.RT.ActionCommand> GameSparks.RT.ActionCommand::CSU24<>9__CachedAnonymousMethodDelegate1
	Func_1_tB02AF8569C4330A1EA8B2913D033F15317DC70A2 * ___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_2;

public:
	inline static int32_t get_offset_of_pool_0() { return static_cast<int32_t>(offsetof(ActionCommand_tFC2779E4CB5E203B26B01E72149FC02CCBC0415E_StaticFields, ___pool_0)); }
	inline ObjectPool_1_t7A3D7F441643094188CBBEFF79A6E0A7CF27CD6A * get_pool_0() const { return ___pool_0; }
	inline ObjectPool_1_t7A3D7F441643094188CBBEFF79A6E0A7CF27CD6A ** get_address_of_pool_0() { return &___pool_0; }
	inline void set_pool_0(ObjectPool_1_t7A3D7F441643094188CBBEFF79A6E0A7CF27CD6A * value)
	{
		___pool_0 = value;
		Il2CppCodeGenWriteBarrier((&___pool_0), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_2() { return static_cast<int32_t>(offsetof(ActionCommand_tFC2779E4CB5E203B26B01E72149FC02CCBC0415E_StaticFields, ___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_2)); }
	inline Func_1_tB02AF8569C4330A1EA8B2913D033F15317DC70A2 * get_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_2() const { return ___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_2; }
	inline Func_1_tB02AF8569C4330A1EA8B2913D033F15317DC70A2 ** get_address_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_2() { return &___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_2; }
	inline void set_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_2(Func_1_tB02AF8569C4330A1EA8B2913D033F15317DC70A2 * value)
	{
		___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_2 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIONCOMMAND_TFC2779E4CB5E203B26B01E72149FC02CCBC0415E_H
#ifndef COMMANDFACTORY_T0183E1BC06011BBEB4BAD35F78DA214D2448D509_H
#define COMMANDFACTORY_T0183E1BC06011BBEB4BAD35F78DA214D2448D509_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.RT.Commands.CommandFactory
struct  CommandFactory_t0183E1BC06011BBEB4BAD35F78DA214D2448D509  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMMANDFACTORY_T0183E1BC06011BBEB4BAD35F78DA214D2448D509_H
#ifndef CONNECTION_TBC3E695B64A7864140245909D74866C1FE9A2818_H
#define CONNECTION_TBC3E695B64A7864140245909D74866C1FE9A2818_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.RT.Connection.Connection
struct  Connection_tBC3E695B64A7864140245909D74866C1FE9A2818  : public RuntimeObject
{
public:
	// System.Net.IPEndPoint GameSparks.RT.Connection.Connection::remoteEndPoint
	IPEndPoint_tCD29981135F7B1989C3845BF455AD44EBC13DE3F * ___remoteEndPoint_0;
	// GameSparks.RT.IRTSessionInternal GameSparks.RT.Connection.Connection::session
	RuntimeObject* ___session_1;
	// System.Boolean GameSparks.RT.Connection.Connection::stopped
	bool ___stopped_2;

public:
	inline static int32_t get_offset_of_remoteEndPoint_0() { return static_cast<int32_t>(offsetof(Connection_tBC3E695B64A7864140245909D74866C1FE9A2818, ___remoteEndPoint_0)); }
	inline IPEndPoint_tCD29981135F7B1989C3845BF455AD44EBC13DE3F * get_remoteEndPoint_0() const { return ___remoteEndPoint_0; }
	inline IPEndPoint_tCD29981135F7B1989C3845BF455AD44EBC13DE3F ** get_address_of_remoteEndPoint_0() { return &___remoteEndPoint_0; }
	inline void set_remoteEndPoint_0(IPEndPoint_tCD29981135F7B1989C3845BF455AD44EBC13DE3F * value)
	{
		___remoteEndPoint_0 = value;
		Il2CppCodeGenWriteBarrier((&___remoteEndPoint_0), value);
	}

	inline static int32_t get_offset_of_session_1() { return static_cast<int32_t>(offsetof(Connection_tBC3E695B64A7864140245909D74866C1FE9A2818, ___session_1)); }
	inline RuntimeObject* get_session_1() const { return ___session_1; }
	inline RuntimeObject** get_address_of_session_1() { return &___session_1; }
	inline void set_session_1(RuntimeObject* value)
	{
		___session_1 = value;
		Il2CppCodeGenWriteBarrier((&___session_1), value);
	}

	inline static int32_t get_offset_of_stopped_2() { return static_cast<int32_t>(offsetof(Connection_tBC3E695B64A7864140245909D74866C1FE9A2818, ___stopped_2)); }
	inline bool get_stopped_2() const { return ___stopped_2; }
	inline bool* get_address_of_stopped_2() { return &___stopped_2; }
	inline void set_stopped_2(bool value)
	{
		___stopped_2 = value;
	}
};

struct Connection_tBC3E695B64A7864140245909D74866C1FE9A2818_StaticFields
{
public:
	// GameSparks.RT.Proto.LimitedPositionStream GameSparks.RT.Connection.Connection::emptyStream
	LimitedPositionStream_t39D3BF1EF4DFE19E62D02F20AA326007FD4A2853 * ___emptyStream_3;

public:
	inline static int32_t get_offset_of_emptyStream_3() { return static_cast<int32_t>(offsetof(Connection_tBC3E695B64A7864140245909D74866C1FE9A2818_StaticFields, ___emptyStream_3)); }
	inline LimitedPositionStream_t39D3BF1EF4DFE19E62D02F20AA326007FD4A2853 * get_emptyStream_3() const { return ___emptyStream_3; }
	inline LimitedPositionStream_t39D3BF1EF4DFE19E62D02F20AA326007FD4A2853 ** get_address_of_emptyStream_3() { return &___emptyStream_3; }
	inline void set_emptyStream_3(LimitedPositionStream_t39D3BF1EF4DFE19E62D02F20AA326007FD4A2853 * value)
	{
		___emptyStream_3 = value;
		Il2CppCodeGenWriteBarrier((&___emptyStream_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONNECTION_TBC3E695B64A7864140245909D74866C1FE9A2818_H
#ifndef CUSTOMCOMMAND_T20AC0410E03BBF162CF04C2A64CB1154916BA4E2_H
#define CUSTOMCOMMAND_T20AC0410E03BBF162CF04C2A64CB1154916BA4E2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.RT.CustomCommand
struct  CustomCommand_t20AC0410E03BBF162CF04C2A64CB1154916BA4E2  : public RuntimeObject
{
public:
	// GameSparks.RT.IRTSessionInternal GameSparks.RT.CustomCommand::session
	RuntimeObject* ___session_1;
	// System.Int32 GameSparks.RT.CustomCommand::opCode
	int32_t ___opCode_2;
	// System.Int32 GameSparks.RT.CustomCommand::sender
	int32_t ___sender_3;
	// System.Int32 GameSparks.RT.CustomCommand::limit
	int32_t ___limit_4;
	// System.Int32 GameSparks.RT.CustomCommand::packetSize
	int32_t ___packetSize_5;
	// System.IO.MemoryStream GameSparks.RT.CustomCommand::ms
	MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * ___ms_6;
	// GameSparks.RT.Proto.LimitedPositionStream GameSparks.RT.CustomCommand::limitedStream
	LimitedPositionStream_t39D3BF1EF4DFE19E62D02F20AA326007FD4A2853 * ___limitedStream_7;
	// GameSparks.RT.RTData GameSparks.RT.CustomCommand::data
	RTData_tAAEBF5D36A61DACA40791F6B7E2D59B7FAF099B5 * ___data_8;

public:
	inline static int32_t get_offset_of_session_1() { return static_cast<int32_t>(offsetof(CustomCommand_t20AC0410E03BBF162CF04C2A64CB1154916BA4E2, ___session_1)); }
	inline RuntimeObject* get_session_1() const { return ___session_1; }
	inline RuntimeObject** get_address_of_session_1() { return &___session_1; }
	inline void set_session_1(RuntimeObject* value)
	{
		___session_1 = value;
		Il2CppCodeGenWriteBarrier((&___session_1), value);
	}

	inline static int32_t get_offset_of_opCode_2() { return static_cast<int32_t>(offsetof(CustomCommand_t20AC0410E03BBF162CF04C2A64CB1154916BA4E2, ___opCode_2)); }
	inline int32_t get_opCode_2() const { return ___opCode_2; }
	inline int32_t* get_address_of_opCode_2() { return &___opCode_2; }
	inline void set_opCode_2(int32_t value)
	{
		___opCode_2 = value;
	}

	inline static int32_t get_offset_of_sender_3() { return static_cast<int32_t>(offsetof(CustomCommand_t20AC0410E03BBF162CF04C2A64CB1154916BA4E2, ___sender_3)); }
	inline int32_t get_sender_3() const { return ___sender_3; }
	inline int32_t* get_address_of_sender_3() { return &___sender_3; }
	inline void set_sender_3(int32_t value)
	{
		___sender_3 = value;
	}

	inline static int32_t get_offset_of_limit_4() { return static_cast<int32_t>(offsetof(CustomCommand_t20AC0410E03BBF162CF04C2A64CB1154916BA4E2, ___limit_4)); }
	inline int32_t get_limit_4() const { return ___limit_4; }
	inline int32_t* get_address_of_limit_4() { return &___limit_4; }
	inline void set_limit_4(int32_t value)
	{
		___limit_4 = value;
	}

	inline static int32_t get_offset_of_packetSize_5() { return static_cast<int32_t>(offsetof(CustomCommand_t20AC0410E03BBF162CF04C2A64CB1154916BA4E2, ___packetSize_5)); }
	inline int32_t get_packetSize_5() const { return ___packetSize_5; }
	inline int32_t* get_address_of_packetSize_5() { return &___packetSize_5; }
	inline void set_packetSize_5(int32_t value)
	{
		___packetSize_5 = value;
	}

	inline static int32_t get_offset_of_ms_6() { return static_cast<int32_t>(offsetof(CustomCommand_t20AC0410E03BBF162CF04C2A64CB1154916BA4E2, ___ms_6)); }
	inline MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * get_ms_6() const { return ___ms_6; }
	inline MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C ** get_address_of_ms_6() { return &___ms_6; }
	inline void set_ms_6(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * value)
	{
		___ms_6 = value;
		Il2CppCodeGenWriteBarrier((&___ms_6), value);
	}

	inline static int32_t get_offset_of_limitedStream_7() { return static_cast<int32_t>(offsetof(CustomCommand_t20AC0410E03BBF162CF04C2A64CB1154916BA4E2, ___limitedStream_7)); }
	inline LimitedPositionStream_t39D3BF1EF4DFE19E62D02F20AA326007FD4A2853 * get_limitedStream_7() const { return ___limitedStream_7; }
	inline LimitedPositionStream_t39D3BF1EF4DFE19E62D02F20AA326007FD4A2853 ** get_address_of_limitedStream_7() { return &___limitedStream_7; }
	inline void set_limitedStream_7(LimitedPositionStream_t39D3BF1EF4DFE19E62D02F20AA326007FD4A2853 * value)
	{
		___limitedStream_7 = value;
		Il2CppCodeGenWriteBarrier((&___limitedStream_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(CustomCommand_t20AC0410E03BBF162CF04C2A64CB1154916BA4E2, ___data_8)); }
	inline RTData_tAAEBF5D36A61DACA40791F6B7E2D59B7FAF099B5 * get_data_8() const { return ___data_8; }
	inline RTData_tAAEBF5D36A61DACA40791F6B7E2D59B7FAF099B5 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(RTData_tAAEBF5D36A61DACA40791F6B7E2D59B7FAF099B5 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

struct CustomCommand_t20AC0410E03BBF162CF04C2A64CB1154916BA4E2_StaticFields
{
public:
	// GameSparks.RT.Pools.ObjectPool`1<GameSparks.RT.CustomCommand> GameSparks.RT.CustomCommand::pool
	ObjectPool_1_tC87E9C08820885D50C4DA65B14AC6CA831C999D9 * ___pool_0;
	// System.Func`1<GameSparks.RT.CustomCommand> GameSparks.RT.CustomCommand::CSU24<>9__CachedAnonymousMethodDelegate1
	Func_1_t3E7D37C35670ED400D34C0A6EB45A27F67A03EA2 * ___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_9;

public:
	inline static int32_t get_offset_of_pool_0() { return static_cast<int32_t>(offsetof(CustomCommand_t20AC0410E03BBF162CF04C2A64CB1154916BA4E2_StaticFields, ___pool_0)); }
	inline ObjectPool_1_tC87E9C08820885D50C4DA65B14AC6CA831C999D9 * get_pool_0() const { return ___pool_0; }
	inline ObjectPool_1_tC87E9C08820885D50C4DA65B14AC6CA831C999D9 ** get_address_of_pool_0() { return &___pool_0; }
	inline void set_pool_0(ObjectPool_1_tC87E9C08820885D50C4DA65B14AC6CA831C999D9 * value)
	{
		___pool_0 = value;
		Il2CppCodeGenWriteBarrier((&___pool_0), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_9() { return static_cast<int32_t>(offsetof(CustomCommand_t20AC0410E03BBF162CF04C2A64CB1154916BA4E2_StaticFields, ___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_9)); }
	inline Func_1_t3E7D37C35670ED400D34C0A6EB45A27F67A03EA2 * get_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_9() const { return ___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_9; }
	inline Func_1_t3E7D37C35670ED400D34C0A6EB45A27F67A03EA2 ** get_address_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_9() { return &___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_9; }
	inline void set_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_9(Func_1_t3E7D37C35670ED400D34C0A6EB45A27F67A03EA2 * value)
	{
		___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_9 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMCOMMAND_T20AC0410E03BBF162CF04C2A64CB1154916BA4E2_H
#ifndef GSTLSAUTHENTICATION_T046151E273242DA47443043454D7D45E716C40E2_H
#define GSTLSAUTHENTICATION_T046151E273242DA47443043454D7D45E716C40E2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.RT.GSTlsAuthentication
struct  GSTlsAuthentication_t046151E273242DA47443043454D7D45E716C40E2  : public RuntimeObject
{
public:
	// System.Collections.IList GameSparks.RT.GSTlsAuthentication::validCertNames
	RuntimeObject* ___validCertNames_1;

public:
	inline static int32_t get_offset_of_validCertNames_1() { return static_cast<int32_t>(offsetof(GSTlsAuthentication_t046151E273242DA47443043454D7D45E716C40E2, ___validCertNames_1)); }
	inline RuntimeObject* get_validCertNames_1() const { return ___validCertNames_1; }
	inline RuntimeObject** get_address_of_validCertNames_1() { return &___validCertNames_1; }
	inline void set_validCertNames_1(RuntimeObject* value)
	{
		___validCertNames_1 = value;
		Il2CppCodeGenWriteBarrier((&___validCertNames_1), value);
	}
};

struct GSTlsAuthentication_t046151E273242DA47443043454D7D45E716C40E2_StaticFields
{
public:
	// Org.BouncyCastle.X509.X509Certificate GameSparks.RT.GSTlsAuthentication::rootCert
	X509Certificate_t9350C69721F95FA663C0FD6ED97CDF09B8037786 * ___rootCert_0;

public:
	inline static int32_t get_offset_of_rootCert_0() { return static_cast<int32_t>(offsetof(GSTlsAuthentication_t046151E273242DA47443043454D7D45E716C40E2_StaticFields, ___rootCert_0)); }
	inline X509Certificate_t9350C69721F95FA663C0FD6ED97CDF09B8037786 * get_rootCert_0() const { return ___rootCert_0; }
	inline X509Certificate_t9350C69721F95FA663C0FD6ED97CDF09B8037786 ** get_address_of_rootCert_0() { return &___rootCert_0; }
	inline void set_rootCert_0(X509Certificate_t9350C69721F95FA663C0FD6ED97CDF09B8037786 * value)
	{
		___rootCert_0 = value;
		Il2CppCodeGenWriteBarrier((&___rootCert_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GSTLSAUTHENTICATION_T046151E273242DA47443043454D7D45E716C40E2_H
#ifndef GSTLSVERIFYCERTIFICATERT_T0AFC95E24504B6445A71C51411605DE74F16FE2B_H
#define GSTLSVERIFYCERTIFICATERT_T0AFC95E24504B6445A71C51411605DE74F16FE2B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.RT.GSTlsVerifyCertificateRT
struct  GSTlsVerifyCertificateRT_t0AFC95E24504B6445A71C51411605DE74F16FE2B  : public RuntimeObject
{
public:

public:
};

struct GSTlsVerifyCertificateRT_t0AFC95E24504B6445A71C51411605DE74F16FE2B_StaticFields
{
public:
	// System.Func`2<Org.BouncyCastle.Crypto.Tls.Certificate,System.String> GameSparks.RT.GSTlsVerifyCertificateRT::<OnVerifyCertificate>k__BackingField
	Func_2_t81AB5F1E16530F25286C7FCBA77049466BE9C90A * ___U3COnVerifyCertificateU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3COnVerifyCertificateU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(GSTlsVerifyCertificateRT_t0AFC95E24504B6445A71C51411605DE74F16FE2B_StaticFields, ___U3COnVerifyCertificateU3Ek__BackingField_0)); }
	inline Func_2_t81AB5F1E16530F25286C7FCBA77049466BE9C90A * get_U3COnVerifyCertificateU3Ek__BackingField_0() const { return ___U3COnVerifyCertificateU3Ek__BackingField_0; }
	inline Func_2_t81AB5F1E16530F25286C7FCBA77049466BE9C90A ** get_address_of_U3COnVerifyCertificateU3Ek__BackingField_0() { return &___U3COnVerifyCertificateU3Ek__BackingField_0; }
	inline void set_U3COnVerifyCertificateU3Ek__BackingField_0(Func_2_t81AB5F1E16530F25286C7FCBA77049466BE9C90A * value)
	{
		___U3COnVerifyCertificateU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3COnVerifyCertificateU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GSTLSVERIFYCERTIFICATERT_T0AFC95E24504B6445A71C51411605DE74F16FE2B_H
#ifndef GAMESPARKSRTSESSIONBUILDER_T9F558E7F87523BFA846A216DA10AF5713522C0A7_H
#define GAMESPARKSRTSESSIONBUILDER_T9F558E7F87523BFA846A216DA10AF5713522C0A7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.RT.GameSparksRTSessionBuilder
struct  GameSparksRTSessionBuilder_t9F558E7F87523BFA846A216DA10AF5713522C0A7  : public RuntimeObject
{
public:
	// System.String GameSparks.RT.GameSparksRTSessionBuilder::connectToken
	String_t* ___connectToken_0;
	// System.String GameSparks.RT.GameSparksRTSessionBuilder::host
	String_t* ___host_1;
	// System.Int32 GameSparks.RT.GameSparksRTSessionBuilder::port
	int32_t ___port_2;
	// GameSparks.RT.IRTSessionListener GameSparks.RT.GameSparksRTSessionBuilder::listener
	RuntimeObject* ___listener_3;
	// System.Boolean GameSparks.RT.GameSparksRTSessionBuilder::useOnlyWebSockets
	bool ___useOnlyWebSockets_4;

public:
	inline static int32_t get_offset_of_connectToken_0() { return static_cast<int32_t>(offsetof(GameSparksRTSessionBuilder_t9F558E7F87523BFA846A216DA10AF5713522C0A7, ___connectToken_0)); }
	inline String_t* get_connectToken_0() const { return ___connectToken_0; }
	inline String_t** get_address_of_connectToken_0() { return &___connectToken_0; }
	inline void set_connectToken_0(String_t* value)
	{
		___connectToken_0 = value;
		Il2CppCodeGenWriteBarrier((&___connectToken_0), value);
	}

	inline static int32_t get_offset_of_host_1() { return static_cast<int32_t>(offsetof(GameSparksRTSessionBuilder_t9F558E7F87523BFA846A216DA10AF5713522C0A7, ___host_1)); }
	inline String_t* get_host_1() const { return ___host_1; }
	inline String_t** get_address_of_host_1() { return &___host_1; }
	inline void set_host_1(String_t* value)
	{
		___host_1 = value;
		Il2CppCodeGenWriteBarrier((&___host_1), value);
	}

	inline static int32_t get_offset_of_port_2() { return static_cast<int32_t>(offsetof(GameSparksRTSessionBuilder_t9F558E7F87523BFA846A216DA10AF5713522C0A7, ___port_2)); }
	inline int32_t get_port_2() const { return ___port_2; }
	inline int32_t* get_address_of_port_2() { return &___port_2; }
	inline void set_port_2(int32_t value)
	{
		___port_2 = value;
	}

	inline static int32_t get_offset_of_listener_3() { return static_cast<int32_t>(offsetof(GameSparksRTSessionBuilder_t9F558E7F87523BFA846A216DA10AF5713522C0A7, ___listener_3)); }
	inline RuntimeObject* get_listener_3() const { return ___listener_3; }
	inline RuntimeObject** get_address_of_listener_3() { return &___listener_3; }
	inline void set_listener_3(RuntimeObject* value)
	{
		___listener_3 = value;
		Il2CppCodeGenWriteBarrier((&___listener_3), value);
	}

	inline static int32_t get_offset_of_useOnlyWebSockets_4() { return static_cast<int32_t>(offsetof(GameSparksRTSessionBuilder_t9F558E7F87523BFA846A216DA10AF5713522C0A7, ___useOnlyWebSockets_4)); }
	inline bool get_useOnlyWebSockets_4() const { return ___useOnlyWebSockets_4; }
	inline bool* get_address_of_useOnlyWebSockets_4() { return &___useOnlyWebSockets_4; }
	inline void set_useOnlyWebSockets_4(bool value)
	{
		___useOnlyWebSockets_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMESPARKSRTSESSIONBUILDER_T9F558E7F87523BFA846A216DA10AF5713522C0A7_H
#ifndef POOLEDOBJECTS_T7D516C3A58DE32F419C7820AFEB7F948693A13F6_H
#define POOLEDOBJECTS_T7D516C3A58DE32F419C7820AFEB7F948693A13F6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.RT.Pools.PooledObjects
struct  PooledObjects_t7D516C3A58DE32F419C7820AFEB7F948693A13F6  : public RuntimeObject
{
public:

public:
};

struct PooledObjects_t7D516C3A58DE32F419C7820AFEB7F948693A13F6_StaticFields
{
public:
	// GameSparks.RT.Pools.ObjectPool`1<System.IO.MemoryStream> GameSparks.RT.Pools.PooledObjects::<MemoryStreamPool>k__BackingField
	ObjectPool_1_t967B8446B741B8199B5557F4A8796A30BD1B69CC * ___U3CMemoryStreamPoolU3Ek__BackingField_0;
	// GameSparks.RT.Pools.ObjectPool`1<GameSparks.RT.CustomRequest> GameSparks.RT.Pools.PooledObjects::<CustomRequestPool>k__BackingField
	ObjectPool_1_t99EEABFA3208E2FCCE778699E9356B641EB3D3E0 * ___U3CCustomRequestPoolU3Ek__BackingField_1;
	// GameSparks.RT.Pools.ObjectPool`1<GameSparks.RT.PositionStream> GameSparks.RT.Pools.PooledObjects::<PositionStreamPool>k__BackingField
	ObjectPool_1_t687E4BDB9561517BD2A6A770F8F018BD8A09672B * ___U3CPositionStreamPoolU3Ek__BackingField_2;
	// GameSparks.RT.Pools.ObjectPool`1<GameSparks.RT.Proto.LimitedPositionStream> GameSparks.RT.Pools.PooledObjects::<LimitedPositionStreamPool>k__BackingField
	ObjectPool_1_t35B625DF2B797C78F590E17AD376C64BC732A5D7 * ___U3CLimitedPositionStreamPoolU3Ek__BackingField_3;
	// GameSparks.RT.Pools.ObjectPool`1<System.Byte[]> GameSparks.RT.Pools.PooledObjects::<ByteBufferPool>k__BackingField
	ObjectPool_1_tCB76812153C38C9C5A0CABEC2F052F40CC031052 * ___U3CByteBufferPoolU3Ek__BackingField_4;
	// GameSparks.RT.Pools.ObjectPool`1<GameSparks.RT.Proto.Packet> GameSparks.RT.Pools.PooledObjects::<PacketPool>k__BackingField
	ObjectPool_1_t8C96F276E9A155DB89566AE06CE0A1FC52D8CA11 * ___U3CPacketPoolU3Ek__BackingField_5;
	// System.Func`1<GameSparks.RT.Proto.Packet> GameSparks.RT.Pools.PooledObjects::CSU24<>9__CachedAnonymousMethodDelegate9
	Func_1_tE9D78C499252D04A9430D89E9FA8A6B575B25DDB * ___CSU24U3CU3E9__CachedAnonymousMethodDelegate9_6;
	// System.Action`1<GameSparks.RT.Proto.Packet> GameSparks.RT.Pools.PooledObjects::CSU24<>9__CachedAnonymousMethodDelegatea
	Action_1_t12A5A30426390F1E09FD7F9F5DF7BA10DBF8F644 * ___CSU24U3CU3E9__CachedAnonymousMethodDelegatea_7;
	// System.Func`1<System.IO.MemoryStream> GameSparks.RT.Pools.PooledObjects::CSU24<>9__CachedAnonymousMethodDelegateb
	Func_1_t53720039ECB1D625D6FB185F4B11B761738A110A * ___CSU24U3CU3E9__CachedAnonymousMethodDelegateb_8;
	// System.Action`1<System.IO.MemoryStream> GameSparks.RT.Pools.PooledObjects::CSU24<>9__CachedAnonymousMethodDelegatec
	Action_1_t9976A126D9E8071B700A6C53DBE8F643532E81B9 * ___CSU24U3CU3E9__CachedAnonymousMethodDelegatec_9;
	// System.Func`1<GameSparks.RT.CustomRequest> GameSparks.RT.Pools.PooledObjects::CSU24<>9__CachedAnonymousMethodDelegated
	Func_1_tE2B79A974D5313D75912E5CF0AE834349DA9E88A * ___CSU24U3CU3E9__CachedAnonymousMethodDelegated_10;
	// System.Action`1<GameSparks.RT.CustomRequest> GameSparks.RT.Pools.PooledObjects::CSU24<>9__CachedAnonymousMethodDelegatee
	Action_1_t197ECDDFA194D1F002079DDE8314AE721A381509 * ___CSU24U3CU3E9__CachedAnonymousMethodDelegatee_11;
	// System.Func`1<GameSparks.RT.PositionStream> GameSparks.RT.Pools.PooledObjects::CSU24<>9__CachedAnonymousMethodDelegatef
	Func_1_t81E2F21B4E1E83A5964A4D5467CB08F66F5BA6E5 * ___CSU24U3CU3E9__CachedAnonymousMethodDelegatef_12;
	// System.Func`1<GameSparks.RT.Proto.LimitedPositionStream> GameSparks.RT.Pools.PooledObjects::CSU24<>9__CachedAnonymousMethodDelegate10
	Func_1_t34CAFB160E22484916C3F64AC903AF24ED90A2F4 * ___CSU24U3CU3E9__CachedAnonymousMethodDelegate10_13;
	// System.Func`1<System.Byte[]> GameSparks.RT.Pools.PooledObjects::CSU24<>9__CachedAnonymousMethodDelegate11
	Func_1_t53A6D1525C5DAC36936621EB7D2555B8CF99F00B * ___CSU24U3CU3E9__CachedAnonymousMethodDelegate11_14;

public:
	inline static int32_t get_offset_of_U3CMemoryStreamPoolU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(PooledObjects_t7D516C3A58DE32F419C7820AFEB7F948693A13F6_StaticFields, ___U3CMemoryStreamPoolU3Ek__BackingField_0)); }
	inline ObjectPool_1_t967B8446B741B8199B5557F4A8796A30BD1B69CC * get_U3CMemoryStreamPoolU3Ek__BackingField_0() const { return ___U3CMemoryStreamPoolU3Ek__BackingField_0; }
	inline ObjectPool_1_t967B8446B741B8199B5557F4A8796A30BD1B69CC ** get_address_of_U3CMemoryStreamPoolU3Ek__BackingField_0() { return &___U3CMemoryStreamPoolU3Ek__BackingField_0; }
	inline void set_U3CMemoryStreamPoolU3Ek__BackingField_0(ObjectPool_1_t967B8446B741B8199B5557F4A8796A30BD1B69CC * value)
	{
		___U3CMemoryStreamPoolU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMemoryStreamPoolU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CCustomRequestPoolU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PooledObjects_t7D516C3A58DE32F419C7820AFEB7F948693A13F6_StaticFields, ___U3CCustomRequestPoolU3Ek__BackingField_1)); }
	inline ObjectPool_1_t99EEABFA3208E2FCCE778699E9356B641EB3D3E0 * get_U3CCustomRequestPoolU3Ek__BackingField_1() const { return ___U3CCustomRequestPoolU3Ek__BackingField_1; }
	inline ObjectPool_1_t99EEABFA3208E2FCCE778699E9356B641EB3D3E0 ** get_address_of_U3CCustomRequestPoolU3Ek__BackingField_1() { return &___U3CCustomRequestPoolU3Ek__BackingField_1; }
	inline void set_U3CCustomRequestPoolU3Ek__BackingField_1(ObjectPool_1_t99EEABFA3208E2FCCE778699E9356B641EB3D3E0 * value)
	{
		___U3CCustomRequestPoolU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCustomRequestPoolU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CPositionStreamPoolU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(PooledObjects_t7D516C3A58DE32F419C7820AFEB7F948693A13F6_StaticFields, ___U3CPositionStreamPoolU3Ek__BackingField_2)); }
	inline ObjectPool_1_t687E4BDB9561517BD2A6A770F8F018BD8A09672B * get_U3CPositionStreamPoolU3Ek__BackingField_2() const { return ___U3CPositionStreamPoolU3Ek__BackingField_2; }
	inline ObjectPool_1_t687E4BDB9561517BD2A6A770F8F018BD8A09672B ** get_address_of_U3CPositionStreamPoolU3Ek__BackingField_2() { return &___U3CPositionStreamPoolU3Ek__BackingField_2; }
	inline void set_U3CPositionStreamPoolU3Ek__BackingField_2(ObjectPool_1_t687E4BDB9561517BD2A6A770F8F018BD8A09672B * value)
	{
		___U3CPositionStreamPoolU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPositionStreamPoolU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CLimitedPositionStreamPoolU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(PooledObjects_t7D516C3A58DE32F419C7820AFEB7F948693A13F6_StaticFields, ___U3CLimitedPositionStreamPoolU3Ek__BackingField_3)); }
	inline ObjectPool_1_t35B625DF2B797C78F590E17AD376C64BC732A5D7 * get_U3CLimitedPositionStreamPoolU3Ek__BackingField_3() const { return ___U3CLimitedPositionStreamPoolU3Ek__BackingField_3; }
	inline ObjectPool_1_t35B625DF2B797C78F590E17AD376C64BC732A5D7 ** get_address_of_U3CLimitedPositionStreamPoolU3Ek__BackingField_3() { return &___U3CLimitedPositionStreamPoolU3Ek__BackingField_3; }
	inline void set_U3CLimitedPositionStreamPoolU3Ek__BackingField_3(ObjectPool_1_t35B625DF2B797C78F590E17AD376C64BC732A5D7 * value)
	{
		___U3CLimitedPositionStreamPoolU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLimitedPositionStreamPoolU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CByteBufferPoolU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(PooledObjects_t7D516C3A58DE32F419C7820AFEB7F948693A13F6_StaticFields, ___U3CByteBufferPoolU3Ek__BackingField_4)); }
	inline ObjectPool_1_tCB76812153C38C9C5A0CABEC2F052F40CC031052 * get_U3CByteBufferPoolU3Ek__BackingField_4() const { return ___U3CByteBufferPoolU3Ek__BackingField_4; }
	inline ObjectPool_1_tCB76812153C38C9C5A0CABEC2F052F40CC031052 ** get_address_of_U3CByteBufferPoolU3Ek__BackingField_4() { return &___U3CByteBufferPoolU3Ek__BackingField_4; }
	inline void set_U3CByteBufferPoolU3Ek__BackingField_4(ObjectPool_1_tCB76812153C38C9C5A0CABEC2F052F40CC031052 * value)
	{
		___U3CByteBufferPoolU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CByteBufferPoolU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CPacketPoolU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(PooledObjects_t7D516C3A58DE32F419C7820AFEB7F948693A13F6_StaticFields, ___U3CPacketPoolU3Ek__BackingField_5)); }
	inline ObjectPool_1_t8C96F276E9A155DB89566AE06CE0A1FC52D8CA11 * get_U3CPacketPoolU3Ek__BackingField_5() const { return ___U3CPacketPoolU3Ek__BackingField_5; }
	inline ObjectPool_1_t8C96F276E9A155DB89566AE06CE0A1FC52D8CA11 ** get_address_of_U3CPacketPoolU3Ek__BackingField_5() { return &___U3CPacketPoolU3Ek__BackingField_5; }
	inline void set_U3CPacketPoolU3Ek__BackingField_5(ObjectPool_1_t8C96F276E9A155DB89566AE06CE0A1FC52D8CA11 * value)
	{
		___U3CPacketPoolU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPacketPoolU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate9_6() { return static_cast<int32_t>(offsetof(PooledObjects_t7D516C3A58DE32F419C7820AFEB7F948693A13F6_StaticFields, ___CSU24U3CU3E9__CachedAnonymousMethodDelegate9_6)); }
	inline Func_1_tE9D78C499252D04A9430D89E9FA8A6B575B25DDB * get_CSU24U3CU3E9__CachedAnonymousMethodDelegate9_6() const { return ___CSU24U3CU3E9__CachedAnonymousMethodDelegate9_6; }
	inline Func_1_tE9D78C499252D04A9430D89E9FA8A6B575B25DDB ** get_address_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate9_6() { return &___CSU24U3CU3E9__CachedAnonymousMethodDelegate9_6; }
	inline void set_CSU24U3CU3E9__CachedAnonymousMethodDelegate9_6(Func_1_tE9D78C499252D04A9430D89E9FA8A6B575B25DDB * value)
	{
		___CSU24U3CU3E9__CachedAnonymousMethodDelegate9_6 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E9__CachedAnonymousMethodDelegate9_6), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegatea_7() { return static_cast<int32_t>(offsetof(PooledObjects_t7D516C3A58DE32F419C7820AFEB7F948693A13F6_StaticFields, ___CSU24U3CU3E9__CachedAnonymousMethodDelegatea_7)); }
	inline Action_1_t12A5A30426390F1E09FD7F9F5DF7BA10DBF8F644 * get_CSU24U3CU3E9__CachedAnonymousMethodDelegatea_7() const { return ___CSU24U3CU3E9__CachedAnonymousMethodDelegatea_7; }
	inline Action_1_t12A5A30426390F1E09FD7F9F5DF7BA10DBF8F644 ** get_address_of_CSU24U3CU3E9__CachedAnonymousMethodDelegatea_7() { return &___CSU24U3CU3E9__CachedAnonymousMethodDelegatea_7; }
	inline void set_CSU24U3CU3E9__CachedAnonymousMethodDelegatea_7(Action_1_t12A5A30426390F1E09FD7F9F5DF7BA10DBF8F644 * value)
	{
		___CSU24U3CU3E9__CachedAnonymousMethodDelegatea_7 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E9__CachedAnonymousMethodDelegatea_7), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegateb_8() { return static_cast<int32_t>(offsetof(PooledObjects_t7D516C3A58DE32F419C7820AFEB7F948693A13F6_StaticFields, ___CSU24U3CU3E9__CachedAnonymousMethodDelegateb_8)); }
	inline Func_1_t53720039ECB1D625D6FB185F4B11B761738A110A * get_CSU24U3CU3E9__CachedAnonymousMethodDelegateb_8() const { return ___CSU24U3CU3E9__CachedAnonymousMethodDelegateb_8; }
	inline Func_1_t53720039ECB1D625D6FB185F4B11B761738A110A ** get_address_of_CSU24U3CU3E9__CachedAnonymousMethodDelegateb_8() { return &___CSU24U3CU3E9__CachedAnonymousMethodDelegateb_8; }
	inline void set_CSU24U3CU3E9__CachedAnonymousMethodDelegateb_8(Func_1_t53720039ECB1D625D6FB185F4B11B761738A110A * value)
	{
		___CSU24U3CU3E9__CachedAnonymousMethodDelegateb_8 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E9__CachedAnonymousMethodDelegateb_8), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegatec_9() { return static_cast<int32_t>(offsetof(PooledObjects_t7D516C3A58DE32F419C7820AFEB7F948693A13F6_StaticFields, ___CSU24U3CU3E9__CachedAnonymousMethodDelegatec_9)); }
	inline Action_1_t9976A126D9E8071B700A6C53DBE8F643532E81B9 * get_CSU24U3CU3E9__CachedAnonymousMethodDelegatec_9() const { return ___CSU24U3CU3E9__CachedAnonymousMethodDelegatec_9; }
	inline Action_1_t9976A126D9E8071B700A6C53DBE8F643532E81B9 ** get_address_of_CSU24U3CU3E9__CachedAnonymousMethodDelegatec_9() { return &___CSU24U3CU3E9__CachedAnonymousMethodDelegatec_9; }
	inline void set_CSU24U3CU3E9__CachedAnonymousMethodDelegatec_9(Action_1_t9976A126D9E8071B700A6C53DBE8F643532E81B9 * value)
	{
		___CSU24U3CU3E9__CachedAnonymousMethodDelegatec_9 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E9__CachedAnonymousMethodDelegatec_9), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegated_10() { return static_cast<int32_t>(offsetof(PooledObjects_t7D516C3A58DE32F419C7820AFEB7F948693A13F6_StaticFields, ___CSU24U3CU3E9__CachedAnonymousMethodDelegated_10)); }
	inline Func_1_tE2B79A974D5313D75912E5CF0AE834349DA9E88A * get_CSU24U3CU3E9__CachedAnonymousMethodDelegated_10() const { return ___CSU24U3CU3E9__CachedAnonymousMethodDelegated_10; }
	inline Func_1_tE2B79A974D5313D75912E5CF0AE834349DA9E88A ** get_address_of_CSU24U3CU3E9__CachedAnonymousMethodDelegated_10() { return &___CSU24U3CU3E9__CachedAnonymousMethodDelegated_10; }
	inline void set_CSU24U3CU3E9__CachedAnonymousMethodDelegated_10(Func_1_tE2B79A974D5313D75912E5CF0AE834349DA9E88A * value)
	{
		___CSU24U3CU3E9__CachedAnonymousMethodDelegated_10 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E9__CachedAnonymousMethodDelegated_10), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegatee_11() { return static_cast<int32_t>(offsetof(PooledObjects_t7D516C3A58DE32F419C7820AFEB7F948693A13F6_StaticFields, ___CSU24U3CU3E9__CachedAnonymousMethodDelegatee_11)); }
	inline Action_1_t197ECDDFA194D1F002079DDE8314AE721A381509 * get_CSU24U3CU3E9__CachedAnonymousMethodDelegatee_11() const { return ___CSU24U3CU3E9__CachedAnonymousMethodDelegatee_11; }
	inline Action_1_t197ECDDFA194D1F002079DDE8314AE721A381509 ** get_address_of_CSU24U3CU3E9__CachedAnonymousMethodDelegatee_11() { return &___CSU24U3CU3E9__CachedAnonymousMethodDelegatee_11; }
	inline void set_CSU24U3CU3E9__CachedAnonymousMethodDelegatee_11(Action_1_t197ECDDFA194D1F002079DDE8314AE721A381509 * value)
	{
		___CSU24U3CU3E9__CachedAnonymousMethodDelegatee_11 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E9__CachedAnonymousMethodDelegatee_11), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegatef_12() { return static_cast<int32_t>(offsetof(PooledObjects_t7D516C3A58DE32F419C7820AFEB7F948693A13F6_StaticFields, ___CSU24U3CU3E9__CachedAnonymousMethodDelegatef_12)); }
	inline Func_1_t81E2F21B4E1E83A5964A4D5467CB08F66F5BA6E5 * get_CSU24U3CU3E9__CachedAnonymousMethodDelegatef_12() const { return ___CSU24U3CU3E9__CachedAnonymousMethodDelegatef_12; }
	inline Func_1_t81E2F21B4E1E83A5964A4D5467CB08F66F5BA6E5 ** get_address_of_CSU24U3CU3E9__CachedAnonymousMethodDelegatef_12() { return &___CSU24U3CU3E9__CachedAnonymousMethodDelegatef_12; }
	inline void set_CSU24U3CU3E9__CachedAnonymousMethodDelegatef_12(Func_1_t81E2F21B4E1E83A5964A4D5467CB08F66F5BA6E5 * value)
	{
		___CSU24U3CU3E9__CachedAnonymousMethodDelegatef_12 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E9__CachedAnonymousMethodDelegatef_12), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate10_13() { return static_cast<int32_t>(offsetof(PooledObjects_t7D516C3A58DE32F419C7820AFEB7F948693A13F6_StaticFields, ___CSU24U3CU3E9__CachedAnonymousMethodDelegate10_13)); }
	inline Func_1_t34CAFB160E22484916C3F64AC903AF24ED90A2F4 * get_CSU24U3CU3E9__CachedAnonymousMethodDelegate10_13() const { return ___CSU24U3CU3E9__CachedAnonymousMethodDelegate10_13; }
	inline Func_1_t34CAFB160E22484916C3F64AC903AF24ED90A2F4 ** get_address_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate10_13() { return &___CSU24U3CU3E9__CachedAnonymousMethodDelegate10_13; }
	inline void set_CSU24U3CU3E9__CachedAnonymousMethodDelegate10_13(Func_1_t34CAFB160E22484916C3F64AC903AF24ED90A2F4 * value)
	{
		___CSU24U3CU3E9__CachedAnonymousMethodDelegate10_13 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E9__CachedAnonymousMethodDelegate10_13), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate11_14() { return static_cast<int32_t>(offsetof(PooledObjects_t7D516C3A58DE32F419C7820AFEB7F948693A13F6_StaticFields, ___CSU24U3CU3E9__CachedAnonymousMethodDelegate11_14)); }
	inline Func_1_t53A6D1525C5DAC36936621EB7D2555B8CF99F00B * get_CSU24U3CU3E9__CachedAnonymousMethodDelegate11_14() const { return ___CSU24U3CU3E9__CachedAnonymousMethodDelegate11_14; }
	inline Func_1_t53A6D1525C5DAC36936621EB7D2555B8CF99F00B ** get_address_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate11_14() { return &___CSU24U3CU3E9__CachedAnonymousMethodDelegate11_14; }
	inline void set_CSU24U3CU3E9__CachedAnonymousMethodDelegate11_14(Func_1_t53A6D1525C5DAC36936621EB7D2555B8CF99F00B * value)
	{
		___CSU24U3CU3E9__CachedAnonymousMethodDelegate11_14 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E9__CachedAnonymousMethodDelegate11_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POOLEDOBJECTS_T7D516C3A58DE32F419C7820AFEB7F948693A13F6_H
#ifndef PROTOCOLPARSER_T1ED493ABBA3FEDB52F9F775CAA5A6E66FFDD6116_H
#define PROTOCOLPARSER_T1ED493ABBA3FEDB52F9F775CAA5A6E66FFDD6116_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.RT.Proto.ProtocolParser
struct  ProtocolParser_t1ED493ABBA3FEDB52F9F775CAA5A6E66FFDD6116  : public RuntimeObject
{
public:

public:
};

struct ProtocolParser_t1ED493ABBA3FEDB52F9F775CAA5A6E66FFDD6116_StaticFields
{
public:
	// GameSparks.RT.Pools.ObjectPool`1<System.IO.MemoryStream> GameSparks.RT.Proto.ProtocolParser::Stack
	ObjectPool_1_t967B8446B741B8199B5557F4A8796A30BD1B69CC * ___Stack_0;

public:
	inline static int32_t get_offset_of_Stack_0() { return static_cast<int32_t>(offsetof(ProtocolParser_t1ED493ABBA3FEDB52F9F775CAA5A6E66FFDD6116_StaticFields, ___Stack_0)); }
	inline ObjectPool_1_t967B8446B741B8199B5557F4A8796A30BD1B69CC * get_Stack_0() const { return ___Stack_0; }
	inline ObjectPool_1_t967B8446B741B8199B5557F4A8796A30BD1B69CC ** get_address_of_Stack_0() { return &___Stack_0; }
	inline void set_Stack_0(ObjectPool_1_t967B8446B741B8199B5557F4A8796A30BD1B69CC * value)
	{
		___Stack_0 = value;
		Il2CppCodeGenWriteBarrier((&___Stack_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROTOCOLPARSER_T1ED493ABBA3FEDB52F9F775CAA5A6E66FFDD6116_H
#ifndef RTDATASERIALIZER_TDE80F537AACC3D9B029F6C2BAAD026AC4F11D1F3_H
#define RTDATASERIALIZER_TDE80F537AACC3D9B029F6C2BAAD026AC4F11D1F3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.RT.Proto.RTDataSerializer
struct  RTDataSerializer_tDE80F537AACC3D9B029F6C2BAAD026AC4F11D1F3  : public RuntimeObject
{
public:

public:
};

struct RTDataSerializer_tDE80F537AACC3D9B029F6C2BAAD026AC4F11D1F3_StaticFields
{
public:
	// GameSparks.RT.Pools.ObjectPool`1<GameSparks.RT.RTData> GameSparks.RT.Proto.RTDataSerializer::cache
	ObjectPool_1_t09E1265B298142F1911B97FA131C330D38470C3B * ___cache_0;
	// System.Func`1<GameSparks.RT.RTData> GameSparks.RT.Proto.RTDataSerializer::CSU24<>9__CachedAnonymousMethodDelegate3
	Func_1_t0A82329474D3D4D2B1C381EA917058A8236AB71A * ___CSU24U3CU3E9__CachedAnonymousMethodDelegate3_1;
	// System.Action`1<GameSparks.RT.RTData> GameSparks.RT.Proto.RTDataSerializer::CSU24<>9__CachedAnonymousMethodDelegate4
	Action_1_tE2593A351801F7E00C62A5439E6E5E9E1DBA93EB * ___CSU24U3CU3E9__CachedAnonymousMethodDelegate4_2;
	// System.Action`1<GameSparks.RT.Proto.RTVal> GameSparks.RT.Proto.RTDataSerializer::CSU24<>9__CachedAnonymousMethodDelegate5
	Action_1_t5C9104C9FF0C575595F817B528EEEFF321E339DA * ___CSU24U3CU3E9__CachedAnonymousMethodDelegate5_3;

public:
	inline static int32_t get_offset_of_cache_0() { return static_cast<int32_t>(offsetof(RTDataSerializer_tDE80F537AACC3D9B029F6C2BAAD026AC4F11D1F3_StaticFields, ___cache_0)); }
	inline ObjectPool_1_t09E1265B298142F1911B97FA131C330D38470C3B * get_cache_0() const { return ___cache_0; }
	inline ObjectPool_1_t09E1265B298142F1911B97FA131C330D38470C3B ** get_address_of_cache_0() { return &___cache_0; }
	inline void set_cache_0(ObjectPool_1_t09E1265B298142F1911B97FA131C330D38470C3B * value)
	{
		___cache_0 = value;
		Il2CppCodeGenWriteBarrier((&___cache_0), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate3_1() { return static_cast<int32_t>(offsetof(RTDataSerializer_tDE80F537AACC3D9B029F6C2BAAD026AC4F11D1F3_StaticFields, ___CSU24U3CU3E9__CachedAnonymousMethodDelegate3_1)); }
	inline Func_1_t0A82329474D3D4D2B1C381EA917058A8236AB71A * get_CSU24U3CU3E9__CachedAnonymousMethodDelegate3_1() const { return ___CSU24U3CU3E9__CachedAnonymousMethodDelegate3_1; }
	inline Func_1_t0A82329474D3D4D2B1C381EA917058A8236AB71A ** get_address_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate3_1() { return &___CSU24U3CU3E9__CachedAnonymousMethodDelegate3_1; }
	inline void set_CSU24U3CU3E9__CachedAnonymousMethodDelegate3_1(Func_1_t0A82329474D3D4D2B1C381EA917058A8236AB71A * value)
	{
		___CSU24U3CU3E9__CachedAnonymousMethodDelegate3_1 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E9__CachedAnonymousMethodDelegate3_1), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate4_2() { return static_cast<int32_t>(offsetof(RTDataSerializer_tDE80F537AACC3D9B029F6C2BAAD026AC4F11D1F3_StaticFields, ___CSU24U3CU3E9__CachedAnonymousMethodDelegate4_2)); }
	inline Action_1_tE2593A351801F7E00C62A5439E6E5E9E1DBA93EB * get_CSU24U3CU3E9__CachedAnonymousMethodDelegate4_2() const { return ___CSU24U3CU3E9__CachedAnonymousMethodDelegate4_2; }
	inline Action_1_tE2593A351801F7E00C62A5439E6E5E9E1DBA93EB ** get_address_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate4_2() { return &___CSU24U3CU3E9__CachedAnonymousMethodDelegate4_2; }
	inline void set_CSU24U3CU3E9__CachedAnonymousMethodDelegate4_2(Action_1_tE2593A351801F7E00C62A5439E6E5E9E1DBA93EB * value)
	{
		___CSU24U3CU3E9__CachedAnonymousMethodDelegate4_2 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E9__CachedAnonymousMethodDelegate4_2), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate5_3() { return static_cast<int32_t>(offsetof(RTDataSerializer_tDE80F537AACC3D9B029F6C2BAAD026AC4F11D1F3_StaticFields, ___CSU24U3CU3E9__CachedAnonymousMethodDelegate5_3)); }
	inline Action_1_t5C9104C9FF0C575595F817B528EEEFF321E339DA * get_CSU24U3CU3E9__CachedAnonymousMethodDelegate5_3() const { return ___CSU24U3CU3E9__CachedAnonymousMethodDelegate5_3; }
	inline Action_1_t5C9104C9FF0C575595F817B528EEEFF321E339DA ** get_address_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate5_3() { return &___CSU24U3CU3E9__CachedAnonymousMethodDelegate5_3; }
	inline void set_CSU24U3CU3E9__CachedAnonymousMethodDelegate5_3(Action_1_t5C9104C9FF0C575595F817B528EEEFF321E339DA * value)
	{
		___CSU24U3CU3E9__CachedAnonymousMethodDelegate5_3 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E9__CachedAnonymousMethodDelegate5_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RTDATASERIALIZER_TDE80F537AACC3D9B029F6C2BAAD026AC4F11D1F3_H
#ifndef RTVALSERIALIZER_T70F1BD5D867AE0A87413C06C27C97D22AF73D879_H
#define RTVALSERIALIZER_T70F1BD5D867AE0A87413C06C27C97D22AF73D879_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.RT.Proto.RTValSerializer
struct  RTValSerializer_t70F1BD5D867AE0A87413C06C27C97D22AF73D879  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RTVALSERIALIZER_T70F1BD5D867AE0A87413C06C27C97D22AF73D879_H
#ifndef RTDATA_TAAEBF5D36A61DACA40791F6B7E2D59B7FAF099B5_H
#define RTDATA_TAAEBF5D36A61DACA40791F6B7E2D59B7FAF099B5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.RT.RTData
struct  RTData_tAAEBF5D36A61DACA40791F6B7E2D59B7FAF099B5  : public RuntimeObject
{
public:
	// GameSparks.RT.Proto.RTVal[] GameSparks.RT.RTData::data
	RTValU5BU5D_tE5FC34AC1C2A0E59370CB42297155A93630BA90C* ___data_0;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(RTData_tAAEBF5D36A61DACA40791F6B7E2D59B7FAF099B5, ___data_0)); }
	inline RTValU5BU5D_tE5FC34AC1C2A0E59370CB42297155A93630BA90C* get_data_0() const { return ___data_0; }
	inline RTValU5BU5D_tE5FC34AC1C2A0E59370CB42297155A93630BA90C** get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(RTValU5BU5D_tE5FC34AC1C2A0E59370CB42297155A93630BA90C* value)
	{
		___data_0 = value;
		Il2CppCodeGenWriteBarrier((&___data_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RTDATA_TAAEBF5D36A61DACA40791F6B7E2D59B7FAF099B5_H
#ifndef U3CU3EC__DISPLAYCLASS2_T12447CD2E3C28EEFFC550BA4102D850BB2BEC1C3_H
#define U3CU3EC__DISPLAYCLASS2_T12447CD2E3C28EEFFC550BA4102D850BB2BEC1C3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.RT.RTSessionImpl_<>c__DisplayClass2
struct  U3CU3Ec__DisplayClass2_t12447CD2E3C28EEFFC550BA4102D850BB2BEC1C3  : public RuntimeObject
{
public:
	// GameSparks.RT.RTSessionImpl GameSparks.RT.RTSessionImpl_<>c__DisplayClass2::<>4__this
	RTSessionImpl_t4277EBD12AF635CB4E621DF4B4C4731EF1A452D9 * ___U3CU3E4__this_0;
	// System.Boolean GameSparks.RT.RTSessionImpl_<>c__DisplayClass2::ready
	bool ___ready_1;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_t12447CD2E3C28EEFFC550BA4102D850BB2BEC1C3, ___U3CU3E4__this_0)); }
	inline RTSessionImpl_t4277EBD12AF635CB4E621DF4B4C4731EF1A452D9 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline RTSessionImpl_t4277EBD12AF635CB4E621DF4B4C4731EF1A452D9 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(RTSessionImpl_t4277EBD12AF635CB4E621DF4B4C4731EF1A452D9 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_ready_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_t12447CD2E3C28EEFFC550BA4102D850BB2BEC1C3, ___ready_1)); }
	inline bool get_ready_1() const { return ___ready_1; }
	inline bool* get_address_of_ready_1() { return &___ready_1; }
	inline void set_ready_1(bool value)
	{
		___ready_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS2_T12447CD2E3C28EEFFC550BA4102D850BB2BEC1C3_H
#ifndef ABSTRACTTLSPEER_T26CA3EF032C693BEB7331E2A0972A8620596CC9F_H
#define ABSTRACTTLSPEER_T26CA3EF032C693BEB7331E2A0972A8620596CC9F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.AbstractTlsPeer
struct  AbstractTlsPeer_t26CA3EF032C693BEB7331E2A0972A8620596CC9F  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTTLSPEER_T26CA3EF032C693BEB7331E2A0972A8620596CC9F_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#define MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MarshalByRefObject
struct  MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF  : public RuntimeObject
{
public:
	// System.Object System.MarshalByRefObject::_identity
	RuntimeObject * ____identity_0;

public:
	inline static int32_t get_offset_of__identity_0() { return static_cast<int32_t>(offsetof(MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF, ____identity_0)); }
	inline RuntimeObject * get__identity_0() const { return ____identity_0; }
	inline RuntimeObject ** get_address_of__identity_0() { return &____identity_0; }
	inline void set__identity_0(RuntimeObject * value)
	{
		____identity_0 = value;
		Il2CppCodeGenWriteBarrier((&____identity_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF_marshaled_pinvoke
{
	Il2CppIUnknown* ____identity_0;
};
// Native definition for COM marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF_marshaled_com
{
	Il2CppIUnknown* ____identity_0;
};
#endif // MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef PINGRESULT_TDBCE93222A19AE547042A11B208C29DCF7C268D8_H
#define PINGRESULT_TDBCE93222A19AE547042A11B208C29DCF7C268D8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Com.Gamesparks.Realtime.Proto.PingResult
struct  PingResult_tDBCE93222A19AE547042A11B208C29DCF7C268D8  : public AbstractResult_tB9A3C663F42A72B8FCA862FF115309B197235C96
{
public:

public:
};

struct PingResult_tDBCE93222A19AE547042A11B208C29DCF7C268D8_StaticFields
{
public:
	// GameSparks.RT.Pools.ObjectPool`1<Com.Gamesparks.Realtime.Proto.PingResult> Com.Gamesparks.Realtime.Proto.PingResult::pool
	ObjectPool_1_t8F0C222FCC56B64A48DAB3A567700ABA0399B52F * ___pool_2;
	// System.Func`1<Com.Gamesparks.Realtime.Proto.PingResult> Com.Gamesparks.Realtime.Proto.PingResult::CSU24<>9__CachedAnonymousMethodDelegate1
	Func_1_t57B3C29C4D2B68BA53394E06D0F5C21047A15B22 * ___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_3;

public:
	inline static int32_t get_offset_of_pool_2() { return static_cast<int32_t>(offsetof(PingResult_tDBCE93222A19AE547042A11B208C29DCF7C268D8_StaticFields, ___pool_2)); }
	inline ObjectPool_1_t8F0C222FCC56B64A48DAB3A567700ABA0399B52F * get_pool_2() const { return ___pool_2; }
	inline ObjectPool_1_t8F0C222FCC56B64A48DAB3A567700ABA0399B52F ** get_address_of_pool_2() { return &___pool_2; }
	inline void set_pool_2(ObjectPool_1_t8F0C222FCC56B64A48DAB3A567700ABA0399B52F * value)
	{
		___pool_2 = value;
		Il2CppCodeGenWriteBarrier((&___pool_2), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_3() { return static_cast<int32_t>(offsetof(PingResult_tDBCE93222A19AE547042A11B208C29DCF7C268D8_StaticFields, ___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_3)); }
	inline Func_1_t57B3C29C4D2B68BA53394E06D0F5C21047A15B22 * get_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_3() const { return ___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_3; }
	inline Func_1_t57B3C29C4D2B68BA53394E06D0F5C21047A15B22 ** get_address_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_3() { return &___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_3; }
	inline void set_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_3(Func_1_t57B3C29C4D2B68BA53394E06D0F5C21047A15B22 * value)
	{
		___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_3 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PINGRESULT_TDBCE93222A19AE547042A11B208C29DCF7C268D8_H
#ifndef PLAYERCONNECTMESSAGE_TB1914D36571FEE52F9F5AD7A5BD971C886C53B12_H
#define PLAYERCONNECTMESSAGE_TB1914D36571FEE52F9F5AD7A5BD971C886C53B12_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Com.Gamesparks.Realtime.Proto.PlayerConnectMessage
struct  PlayerConnectMessage_tB1914D36571FEE52F9F5AD7A5BD971C886C53B12  : public AbstractResult_tB9A3C663F42A72B8FCA862FF115309B197235C96
{
public:
	// System.Int32 Com.Gamesparks.Realtime.Proto.PlayerConnectMessage::<PeerId>k__BackingField
	int32_t ___U3CPeerIdU3Ek__BackingField_3;
	// System.Collections.Generic.List`1<System.Int32> Com.Gamesparks.Realtime.Proto.PlayerConnectMessage::<ActivePeers>k__BackingField
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ___U3CActivePeersU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CPeerIdU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(PlayerConnectMessage_tB1914D36571FEE52F9F5AD7A5BD971C886C53B12, ___U3CPeerIdU3Ek__BackingField_3)); }
	inline int32_t get_U3CPeerIdU3Ek__BackingField_3() const { return ___U3CPeerIdU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CPeerIdU3Ek__BackingField_3() { return &___U3CPeerIdU3Ek__BackingField_3; }
	inline void set_U3CPeerIdU3Ek__BackingField_3(int32_t value)
	{
		___U3CPeerIdU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CActivePeersU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(PlayerConnectMessage_tB1914D36571FEE52F9F5AD7A5BD971C886C53B12, ___U3CActivePeersU3Ek__BackingField_4)); }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * get_U3CActivePeersU3Ek__BackingField_4() const { return ___U3CActivePeersU3Ek__BackingField_4; }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 ** get_address_of_U3CActivePeersU3Ek__BackingField_4() { return &___U3CActivePeersU3Ek__BackingField_4; }
	inline void set_U3CActivePeersU3Ek__BackingField_4(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * value)
	{
		___U3CActivePeersU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CActivePeersU3Ek__BackingField_4), value);
	}
};

struct PlayerConnectMessage_tB1914D36571FEE52F9F5AD7A5BD971C886C53B12_StaticFields
{
public:
	// GameSparks.RT.Pools.ObjectPool`1<Com.Gamesparks.Realtime.Proto.PlayerConnectMessage> Com.Gamesparks.Realtime.Proto.PlayerConnectMessage::pool
	ObjectPool_1_t7DB42E1475B30FA37D53C7FF70B8B9CFA9F5CF96 * ___pool_2;
	// System.Func`1<Com.Gamesparks.Realtime.Proto.PlayerConnectMessage> Com.Gamesparks.Realtime.Proto.PlayerConnectMessage::CSU24<>9__CachedAnonymousMethodDelegate2
	Func_1_t8F6CDB5B6B554B4CE0B6D17AF35C472CADCF43B6 * ___CSU24U3CU3E9__CachedAnonymousMethodDelegate2_5;
	// System.Action`1<Com.Gamesparks.Realtime.Proto.PlayerConnectMessage> Com.Gamesparks.Realtime.Proto.PlayerConnectMessage::CSU24<>9__CachedAnonymousMethodDelegate3
	Action_1_t1723845D1C9251C8FA6C5CBFBB84466C0CE68229 * ___CSU24U3CU3E9__CachedAnonymousMethodDelegate3_6;

public:
	inline static int32_t get_offset_of_pool_2() { return static_cast<int32_t>(offsetof(PlayerConnectMessage_tB1914D36571FEE52F9F5AD7A5BD971C886C53B12_StaticFields, ___pool_2)); }
	inline ObjectPool_1_t7DB42E1475B30FA37D53C7FF70B8B9CFA9F5CF96 * get_pool_2() const { return ___pool_2; }
	inline ObjectPool_1_t7DB42E1475B30FA37D53C7FF70B8B9CFA9F5CF96 ** get_address_of_pool_2() { return &___pool_2; }
	inline void set_pool_2(ObjectPool_1_t7DB42E1475B30FA37D53C7FF70B8B9CFA9F5CF96 * value)
	{
		___pool_2 = value;
		Il2CppCodeGenWriteBarrier((&___pool_2), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate2_5() { return static_cast<int32_t>(offsetof(PlayerConnectMessage_tB1914D36571FEE52F9F5AD7A5BD971C886C53B12_StaticFields, ___CSU24U3CU3E9__CachedAnonymousMethodDelegate2_5)); }
	inline Func_1_t8F6CDB5B6B554B4CE0B6D17AF35C472CADCF43B6 * get_CSU24U3CU3E9__CachedAnonymousMethodDelegate2_5() const { return ___CSU24U3CU3E9__CachedAnonymousMethodDelegate2_5; }
	inline Func_1_t8F6CDB5B6B554B4CE0B6D17AF35C472CADCF43B6 ** get_address_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate2_5() { return &___CSU24U3CU3E9__CachedAnonymousMethodDelegate2_5; }
	inline void set_CSU24U3CU3E9__CachedAnonymousMethodDelegate2_5(Func_1_t8F6CDB5B6B554B4CE0B6D17AF35C472CADCF43B6 * value)
	{
		___CSU24U3CU3E9__CachedAnonymousMethodDelegate2_5 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E9__CachedAnonymousMethodDelegate2_5), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate3_6() { return static_cast<int32_t>(offsetof(PlayerConnectMessage_tB1914D36571FEE52F9F5AD7A5BD971C886C53B12_StaticFields, ___CSU24U3CU3E9__CachedAnonymousMethodDelegate3_6)); }
	inline Action_1_t1723845D1C9251C8FA6C5CBFBB84466C0CE68229 * get_CSU24U3CU3E9__CachedAnonymousMethodDelegate3_6() const { return ___CSU24U3CU3E9__CachedAnonymousMethodDelegate3_6; }
	inline Action_1_t1723845D1C9251C8FA6C5CBFBB84466C0CE68229 ** get_address_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate3_6() { return &___CSU24U3CU3E9__CachedAnonymousMethodDelegate3_6; }
	inline void set_CSU24U3CU3E9__CachedAnonymousMethodDelegate3_6(Action_1_t1723845D1C9251C8FA6C5CBFBB84466C0CE68229 * value)
	{
		___CSU24U3CU3E9__CachedAnonymousMethodDelegate3_6 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E9__CachedAnonymousMethodDelegate3_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERCONNECTMESSAGE_TB1914D36571FEE52F9F5AD7A5BD971C886C53B12_H
#ifndef PLAYERDISCONNECTMESSAGE_TA44AFA9474033AA2BBCEC8DB2298F1B644A1D471_H
#define PLAYERDISCONNECTMESSAGE_TA44AFA9474033AA2BBCEC8DB2298F1B644A1D471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Com.Gamesparks.Realtime.Proto.PlayerDisconnectMessage
struct  PlayerDisconnectMessage_tA44AFA9474033AA2BBCEC8DB2298F1B644A1D471  : public AbstractResult_tB9A3C663F42A72B8FCA862FF115309B197235C96
{
public:
	// System.Int32 Com.Gamesparks.Realtime.Proto.PlayerDisconnectMessage::<PeerId>k__BackingField
	int32_t ___U3CPeerIdU3Ek__BackingField_3;
	// System.Collections.Generic.List`1<System.Int32> Com.Gamesparks.Realtime.Proto.PlayerDisconnectMessage::<ActivePeers>k__BackingField
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ___U3CActivePeersU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CPeerIdU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(PlayerDisconnectMessage_tA44AFA9474033AA2BBCEC8DB2298F1B644A1D471, ___U3CPeerIdU3Ek__BackingField_3)); }
	inline int32_t get_U3CPeerIdU3Ek__BackingField_3() const { return ___U3CPeerIdU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CPeerIdU3Ek__BackingField_3() { return &___U3CPeerIdU3Ek__BackingField_3; }
	inline void set_U3CPeerIdU3Ek__BackingField_3(int32_t value)
	{
		___U3CPeerIdU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CActivePeersU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(PlayerDisconnectMessage_tA44AFA9474033AA2BBCEC8DB2298F1B644A1D471, ___U3CActivePeersU3Ek__BackingField_4)); }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * get_U3CActivePeersU3Ek__BackingField_4() const { return ___U3CActivePeersU3Ek__BackingField_4; }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 ** get_address_of_U3CActivePeersU3Ek__BackingField_4() { return &___U3CActivePeersU3Ek__BackingField_4; }
	inline void set_U3CActivePeersU3Ek__BackingField_4(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * value)
	{
		___U3CActivePeersU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CActivePeersU3Ek__BackingField_4), value);
	}
};

struct PlayerDisconnectMessage_tA44AFA9474033AA2BBCEC8DB2298F1B644A1D471_StaticFields
{
public:
	// GameSparks.RT.Pools.ObjectPool`1<Com.Gamesparks.Realtime.Proto.PlayerDisconnectMessage> Com.Gamesparks.Realtime.Proto.PlayerDisconnectMessage::pool
	ObjectPool_1_t9D58C721119925F211D6FDB5EDB5CE0475FA6CC4 * ___pool_2;
	// System.Func`1<Com.Gamesparks.Realtime.Proto.PlayerDisconnectMessage> Com.Gamesparks.Realtime.Proto.PlayerDisconnectMessage::CSU24<>9__CachedAnonymousMethodDelegate2
	Func_1_tA06E28D621F9C910491509E129922CCCB571331F * ___CSU24U3CU3E9__CachedAnonymousMethodDelegate2_5;
	// System.Action`1<Com.Gamesparks.Realtime.Proto.PlayerDisconnectMessage> Com.Gamesparks.Realtime.Proto.PlayerDisconnectMessage::CSU24<>9__CachedAnonymousMethodDelegate3
	Action_1_tE2B49A1830FEAB9AA68E5DEEC099689FD03BC41B * ___CSU24U3CU3E9__CachedAnonymousMethodDelegate3_6;

public:
	inline static int32_t get_offset_of_pool_2() { return static_cast<int32_t>(offsetof(PlayerDisconnectMessage_tA44AFA9474033AA2BBCEC8DB2298F1B644A1D471_StaticFields, ___pool_2)); }
	inline ObjectPool_1_t9D58C721119925F211D6FDB5EDB5CE0475FA6CC4 * get_pool_2() const { return ___pool_2; }
	inline ObjectPool_1_t9D58C721119925F211D6FDB5EDB5CE0475FA6CC4 ** get_address_of_pool_2() { return &___pool_2; }
	inline void set_pool_2(ObjectPool_1_t9D58C721119925F211D6FDB5EDB5CE0475FA6CC4 * value)
	{
		___pool_2 = value;
		Il2CppCodeGenWriteBarrier((&___pool_2), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate2_5() { return static_cast<int32_t>(offsetof(PlayerDisconnectMessage_tA44AFA9474033AA2BBCEC8DB2298F1B644A1D471_StaticFields, ___CSU24U3CU3E9__CachedAnonymousMethodDelegate2_5)); }
	inline Func_1_tA06E28D621F9C910491509E129922CCCB571331F * get_CSU24U3CU3E9__CachedAnonymousMethodDelegate2_5() const { return ___CSU24U3CU3E9__CachedAnonymousMethodDelegate2_5; }
	inline Func_1_tA06E28D621F9C910491509E129922CCCB571331F ** get_address_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate2_5() { return &___CSU24U3CU3E9__CachedAnonymousMethodDelegate2_5; }
	inline void set_CSU24U3CU3E9__CachedAnonymousMethodDelegate2_5(Func_1_tA06E28D621F9C910491509E129922CCCB571331F * value)
	{
		___CSU24U3CU3E9__CachedAnonymousMethodDelegate2_5 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E9__CachedAnonymousMethodDelegate2_5), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate3_6() { return static_cast<int32_t>(offsetof(PlayerDisconnectMessage_tA44AFA9474033AA2BBCEC8DB2298F1B644A1D471_StaticFields, ___CSU24U3CU3E9__CachedAnonymousMethodDelegate3_6)); }
	inline Action_1_tE2B49A1830FEAB9AA68E5DEEC099689FD03BC41B * get_CSU24U3CU3E9__CachedAnonymousMethodDelegate3_6() const { return ___CSU24U3CU3E9__CachedAnonymousMethodDelegate3_6; }
	inline Action_1_tE2B49A1830FEAB9AA68E5DEEC099689FD03BC41B ** get_address_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate3_6() { return &___CSU24U3CU3E9__CachedAnonymousMethodDelegate3_6; }
	inline void set_CSU24U3CU3E9__CachedAnonymousMethodDelegate3_6(Action_1_tE2B49A1830FEAB9AA68E5DEEC099689FD03BC41B * value)
	{
		___CSU24U3CU3E9__CachedAnonymousMethodDelegate3_6 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E9__CachedAnonymousMethodDelegate3_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERDISCONNECTMESSAGE_TA44AFA9474033AA2BBCEC8DB2298F1B644A1D471_H
#ifndef UDPCONNECTMESSAGE_TF627E88C457DC9DD4B0826ED6C908EFB2A326B54_H
#define UDPCONNECTMESSAGE_TF627E88C457DC9DD4B0826ED6C908EFB2A326B54_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Com.Gamesparks.Realtime.Proto.UDPConnectMessage
struct  UDPConnectMessage_tF627E88C457DC9DD4B0826ED6C908EFB2A326B54  : public AbstractResult_tB9A3C663F42A72B8FCA862FF115309B197235C96
{
public:

public:
};

struct UDPConnectMessage_tF627E88C457DC9DD4B0826ED6C908EFB2A326B54_StaticFields
{
public:
	// GameSparks.RT.Pools.ObjectPool`1<Com.Gamesparks.Realtime.Proto.UDPConnectMessage> Com.Gamesparks.Realtime.Proto.UDPConnectMessage::pool
	ObjectPool_1_t996F5BA919CE3D0E3F28EBBF78FC7F14DC28B1D4 * ___pool_2;
	// System.Func`1<Com.Gamesparks.Realtime.Proto.UDPConnectMessage> Com.Gamesparks.Realtime.Proto.UDPConnectMessage::CSU24<>9__CachedAnonymousMethodDelegate1
	Func_1_t31BC4BB9CACE00A4655732308C4BAEBFB3D29A09 * ___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_3;

public:
	inline static int32_t get_offset_of_pool_2() { return static_cast<int32_t>(offsetof(UDPConnectMessage_tF627E88C457DC9DD4B0826ED6C908EFB2A326B54_StaticFields, ___pool_2)); }
	inline ObjectPool_1_t996F5BA919CE3D0E3F28EBBF78FC7F14DC28B1D4 * get_pool_2() const { return ___pool_2; }
	inline ObjectPool_1_t996F5BA919CE3D0E3F28EBBF78FC7F14DC28B1D4 ** get_address_of_pool_2() { return &___pool_2; }
	inline void set_pool_2(ObjectPool_1_t996F5BA919CE3D0E3F28EBBF78FC7F14DC28B1D4 * value)
	{
		___pool_2 = value;
		Il2CppCodeGenWriteBarrier((&___pool_2), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_3() { return static_cast<int32_t>(offsetof(UDPConnectMessage_tF627E88C457DC9DD4B0826ED6C908EFB2A326B54_StaticFields, ___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_3)); }
	inline Func_1_t31BC4BB9CACE00A4655732308C4BAEBFB3D29A09 * get_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_3() const { return ___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_3; }
	inline Func_1_t31BC4BB9CACE00A4655732308C4BAEBFB3D29A09 ** get_address_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_3() { return &___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_3; }
	inline void set_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_3(Func_1_t31BC4BB9CACE00A4655732308C4BAEBFB3D29A09 * value)
	{
		___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_3 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UDPCONNECTMESSAGE_TF627E88C457DC9DD4B0826ED6C908EFB2A326B54_H
#ifndef GETMYTEAMSREQUEST_TFF4E47D9BE1A521AD6EB4B4B0F58E9926604A4EE_H
#define GETMYTEAMSREQUEST_TFF4E47D9BE1A521AD6EB4B4B0F58E9926604A4EE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Requests.GetMyTeamsRequest
struct  GetMyTeamsRequest_tFF4E47D9BE1A521AD6EB4B4B0F58E9926604A4EE  : public GSTypedRequest_2_tB77B5A1314948016CD9A781F0F16C1DAB6D9DBDB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETMYTEAMSREQUEST_TFF4E47D9BE1A521AD6EB4B4B0F58E9926604A4EE_H
#ifndef JOINTEAMREQUEST_TB9F60B85C89BD9F883B390350895C15020905DB6_H
#define JOINTEAMREQUEST_TB9F60B85C89BD9F883B390350895C15020905DB6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Requests.JoinTeamRequest
struct  JoinTeamRequest_tB9F60B85C89BD9F883B390350895C15020905DB6  : public GSTypedRequest_2_t5E8DFDFF992B87841B32CD0C82F09EC155DE2488
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOINTEAMREQUEST_TB9F60B85C89BD9F883B390350895C15020905DB6_H
#ifndef LEADERBOARDDATAREQUEST_T0E5DCA12818A3EEEB53F10A09281802647765037_H
#define LEADERBOARDDATAREQUEST_T0E5DCA12818A3EEEB53F10A09281802647765037_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Requests.LeaderboardDataRequest
struct  LeaderboardDataRequest_t0E5DCA12818A3EEEB53F10A09281802647765037  : public GSTypedRequest_2_t1253C2CE913AEA6571181BED5EC386D3A3571430
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEADERBOARDDATAREQUEST_T0E5DCA12818A3EEEB53F10A09281802647765037_H
#ifndef LISTACHIEVEMENTSREQUEST_TC80CE14B1C3624CE579024E89D0D41F43D177A81_H
#define LISTACHIEVEMENTSREQUEST_TC80CE14B1C3624CE579024E89D0D41F43D177A81_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Requests.ListAchievementsRequest
struct  ListAchievementsRequest_tC80CE14B1C3624CE579024E89D0D41F43D177A81  : public GSTypedRequest_2_t1ED25D52A386079CC863B6B34027AB2F8E1F32F1
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LISTACHIEVEMENTSREQUEST_TC80CE14B1C3624CE579024E89D0D41F43D177A81_H
#ifndef LISTCHALLENGETYPEREQUEST_TFA4418905CA4E75B7BA212DE275B765D5C3ADB3B_H
#define LISTCHALLENGETYPEREQUEST_TFA4418905CA4E75B7BA212DE275B765D5C3ADB3B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Requests.ListChallengeTypeRequest
struct  ListChallengeTypeRequest_tFA4418905CA4E75B7BA212DE275B765D5C3ADB3B  : public GSTypedRequest_2_tD9074F9C135154AB5683DFDA28C5665B4587C7B2
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LISTCHALLENGETYPEREQUEST_TFA4418905CA4E75B7BA212DE275B765D5C3ADB3B_H
#ifndef LISTGAMEFRIENDSREQUEST_T2E9A57E4C6D61AA5A82101615D6E440E5BBA87D3_H
#define LISTGAMEFRIENDSREQUEST_T2E9A57E4C6D61AA5A82101615D6E440E5BBA87D3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Requests.ListGameFriendsRequest
struct  ListGameFriendsRequest_t2E9A57E4C6D61AA5A82101615D6E440E5BBA87D3  : public GSTypedRequest_2_tF2BFB2ACEACD3E6DC6BFD69D79565B6F63840CC8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LISTGAMEFRIENDSREQUEST_T2E9A57E4C6D61AA5A82101615D6E440E5BBA87D3_H
#ifndef LISTMESSAGEREQUEST_T9BF2D3D720DA1D4D87A1E7CF67CCF334A52D3CE9_H
#define LISTMESSAGEREQUEST_T9BF2D3D720DA1D4D87A1E7CF67CCF334A52D3CE9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Requests.ListMessageRequest
struct  ListMessageRequest_t9BF2D3D720DA1D4D87A1E7CF67CCF334A52D3CE9  : public GSTypedRequest_2_t7CD2FA80FAD739262E447D9A106760D88D78E5BC
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LISTMESSAGEREQUEST_T9BF2D3D720DA1D4D87A1E7CF67CCF334A52D3CE9_H
#ifndef LISTTEAMCHATREQUEST_TDF5BE8FE552C2F16CFC403FA50BE7C6A99F394C9_H
#define LISTTEAMCHATREQUEST_TDF5BE8FE552C2F16CFC403FA50BE7C6A99F394C9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Requests.ListTeamChatRequest
struct  ListTeamChatRequest_tDF5BE8FE552C2F16CFC403FA50BE7C6A99F394C9  : public GSTypedRequest_2_tE6D4BFE07E547DA5A6D25BD1F1876809F844FE5C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LISTTEAMCHATREQUEST_TDF5BE8FE552C2F16CFC403FA50BE7C6A99F394C9_H
#ifndef LISTVIRTUALGOODSREQUEST_T4C319ABF7081B9DC2E7A64E916182C1F6599C43E_H
#define LISTVIRTUALGOODSREQUEST_T4C319ABF7081B9DC2E7A64E916182C1F6599C43E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Requests.ListVirtualGoodsRequest
struct  ListVirtualGoodsRequest_t4C319ABF7081B9DC2E7A64E916182C1F6599C43E  : public GSTypedRequest_2_tA8F45AAB4253EF9A6EC90379068B188E366D8BDD
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LISTVIRTUALGOODSREQUEST_T4C319ABF7081B9DC2E7A64E916182C1F6599C43E_H
#ifndef LOGEVENTREQUEST_TA73A4996425712D9A6F1145173915FE77C6C7BAB_H
#define LOGEVENTREQUEST_TA73A4996425712D9A6F1145173915FE77C6C7BAB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Requests.LogEventRequest
struct  LogEventRequest_tA73A4996425712D9A6F1145173915FE77C6C7BAB  : public GSTypedRequest_2_tEF69F13CA92461C90382DE543D9F0AF4FCEB9B47
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGEVENTREQUEST_TA73A4996425712D9A6F1145173915FE77C6C7BAB_H
#ifndef SENDTEAMCHATMESSAGEREQUEST_TC434B49B9BBF6B2165710960210C810C94BFD119_H
#define SENDTEAMCHATMESSAGEREQUEST_TC434B49B9BBF6B2165710960210C810C94BFD119_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Requests.SendTeamChatMessageRequest
struct  SendTeamChatMessageRequest_tC434B49B9BBF6B2165710960210C810C94BFD119  : public GSTypedRequest_2_t430DD9176857443640FCA8A9934E1CA93AD44440
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SENDTEAMCHATMESSAGEREQUEST_TC434B49B9BBF6B2165710960210C810C94BFD119_H
#ifndef SOCIALLEADERBOARDDATAREQUEST_T0742C19E9372B583E63F265F2D422841BFF8FFAF_H
#define SOCIALLEADERBOARDDATAREQUEST_T0742C19E9372B583E63F265F2D422841BFF8FFAF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Requests.SocialLeaderboardDataRequest
struct  SocialLeaderboardDataRequest_t0742C19E9372B583E63F265F2D422841BFF8FFAF  : public GSTypedRequest_2_tAA9DF7DA46AE4FF76CBE1779C3BA28556C574920
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOCIALLEADERBOARDDATAREQUEST_T0742C19E9372B583E63F265F2D422841BFF8FFAF_H
#ifndef ACCOUNTDETAILSRESPONSE_T1601479BB6FED07AC4C422B9488DCD9D92A064E4_H
#define ACCOUNTDETAILSRESPONSE_T1601479BB6FED07AC4C422B9488DCD9D92A064E4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Responses.AccountDetailsResponse
struct  AccountDetailsResponse_t1601479BB6FED07AC4C422B9488DCD9D92A064E4  : public GSTypedResponse_t8D7A8C3784E1A1542ED006C5A97AF8C6D9E849DA
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACCOUNTDETAILSRESPONSE_T1601479BB6FED07AC4C422B9488DCD9D92A064E4_H
#ifndef _LOCATION_T2EAC90C5902D9FD46A7DF9602F098EDA593D78DC_H
#define _LOCATION_T2EAC90C5902D9FD46A7DF9602F098EDA593D78DC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Responses.AccountDetailsResponse__Location
struct  _Location_t2EAC90C5902D9FD46A7DF9602F098EDA593D78DC  : public GSTypedResponse_t8D7A8C3784E1A1542ED006C5A97AF8C6D9E849DA
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // _LOCATION_T2EAC90C5902D9FD46A7DF9602F098EDA593D78DC_H
#ifndef AUTHENTICATIONRESPONSE_TB52C005AD2DB9DDC3021B03A35E01367A2A9EFB9_H
#define AUTHENTICATIONRESPONSE_TB52C005AD2DB9DDC3021B03A35E01367A2A9EFB9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Responses.AuthenticationResponse
struct  AuthenticationResponse_tB52C005AD2DB9DDC3021B03A35E01367A2A9EFB9  : public GSTypedResponse_t8D7A8C3784E1A1542ED006C5A97AF8C6D9E849DA
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTHENTICATIONRESPONSE_TB52C005AD2DB9DDC3021B03A35E01367A2A9EFB9_H
#ifndef CHANGEUSERDETAILSRESPONSE_T7790E1D7F3118AF6CE2E3750AF75A1CCEF579ED2_H
#define CHANGEUSERDETAILSRESPONSE_T7790E1D7F3118AF6CE2E3750AF75A1CCEF579ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Responses.ChangeUserDetailsResponse
struct  ChangeUserDetailsResponse_t7790E1D7F3118AF6CE2E3750AF75A1CCEF579ED2  : public GSTypedResponse_t8D7A8C3784E1A1542ED006C5A97AF8C6D9E849DA
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHANGEUSERDETAILSRESPONSE_T7790E1D7F3118AF6CE2E3750AF75A1CCEF579ED2_H
#ifndef CREATETEAMRESPONSE_TDEC65B8F3FB8F7D890E5D653FFFE0128A6BA21BA_H
#define CREATETEAMRESPONSE_TDEC65B8F3FB8F7D890E5D653FFFE0128A6BA21BA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Responses.CreateTeamResponse
struct  CreateTeamResponse_tDEC65B8F3FB8F7D890E5D653FFFE0128A6BA21BA  : public GSTypedResponse_t8D7A8C3784E1A1542ED006C5A97AF8C6D9E849DA
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CREATETEAMRESPONSE_TDEC65B8F3FB8F7D890E5D653FFFE0128A6BA21BA_H
#ifndef DISMISSMESSAGERESPONSE_TC68E9EB2A34FE24A4BC53E884651C627921456F5_H
#define DISMISSMESSAGERESPONSE_TC68E9EB2A34FE24A4BC53E884651C627921456F5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Responses.DismissMessageResponse
struct  DismissMessageResponse_tC68E9EB2A34FE24A4BC53E884651C627921456F5  : public GSTypedResponse_t8D7A8C3784E1A1542ED006C5A97AF8C6D9E849DA
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISMISSMESSAGERESPONSE_TC68E9EB2A34FE24A4BC53E884651C627921456F5_H
#ifndef DROPTEAMRESPONSE_T26B2367C7A22F108371C9B92E337399830EE23C5_H
#define DROPTEAMRESPONSE_T26B2367C7A22F108371C9B92E337399830EE23C5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Responses.DropTeamResponse
struct  DropTeamResponse_t26B2367C7A22F108371C9B92E337399830EE23C5  : public GSTypedResponse_t8D7A8C3784E1A1542ED006C5A97AF8C6D9E849DA
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DROPTEAMRESPONSE_T26B2367C7A22F108371C9B92E337399830EE23C5_H
#ifndef ENDSESSIONRESPONSE_TE6BB94FB1025BB5F4A27A1ACF7A94DC77CB93CCB_H
#define ENDSESSIONRESPONSE_TE6BB94FB1025BB5F4A27A1ACF7A94DC77CB93CCB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Responses.EndSessionResponse
struct  EndSessionResponse_tE6BB94FB1025BB5F4A27A1ACF7A94DC77CB93CCB  : public GSTypedResponse_t8D7A8C3784E1A1542ED006C5A97AF8C6D9E849DA
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENDSESSIONRESPONSE_TE6BB94FB1025BB5F4A27A1ACF7A94DC77CB93CCB_H
#ifndef FINDMATCHRESPONSE_TB39FC743E9B82D6FE9BDF049968DE775103A0DF2_H
#define FINDMATCHRESPONSE_TB39FC743E9B82D6FE9BDF049968DE775103A0DF2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Responses.FindMatchResponse
struct  FindMatchResponse_tB39FC743E9B82D6FE9BDF049968DE775103A0DF2  : public GSTypedResponse_t8D7A8C3784E1A1542ED006C5A97AF8C6D9E849DA
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FINDMATCHRESPONSE_TB39FC743E9B82D6FE9BDF049968DE775103A0DF2_H
#ifndef GETDOWNLOADABLERESPONSE_T3529C34B7070C4153AA9ADFC5797F5B0385DEADB_H
#define GETDOWNLOADABLERESPONSE_T3529C34B7070C4153AA9ADFC5797F5B0385DEADB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Responses.GetDownloadableResponse
struct  GetDownloadableResponse_t3529C34B7070C4153AA9ADFC5797F5B0385DEADB  : public GSTypedResponse_t8D7A8C3784E1A1542ED006C5A97AF8C6D9E849DA
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETDOWNLOADABLERESPONSE_T3529C34B7070C4153AA9ADFC5797F5B0385DEADB_H
#ifndef GETMYTEAMSRESPONSE_T89103C0D0553AE1745CD4A29AFF261484A03F629_H
#define GETMYTEAMSRESPONSE_T89103C0D0553AE1745CD4A29AFF261484A03F629_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Responses.GetMyTeamsResponse
struct  GetMyTeamsResponse_t89103C0D0553AE1745CD4A29AFF261484A03F629  : public GSTypedResponse_t8D7A8C3784E1A1542ED006C5A97AF8C6D9E849DA
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETMYTEAMSRESPONSE_T89103C0D0553AE1745CD4A29AFF261484A03F629_H
#ifndef JOINTEAMRESPONSE_T58422C45325928E10E9719DB632FA119E38CBD11_H
#define JOINTEAMRESPONSE_T58422C45325928E10E9719DB632FA119E38CBD11_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Responses.JoinTeamResponse
struct  JoinTeamResponse_t58422C45325928E10E9719DB632FA119E38CBD11  : public GSTypedResponse_t8D7A8C3784E1A1542ED006C5A97AF8C6D9E849DA
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOINTEAMRESPONSE_T58422C45325928E10E9719DB632FA119E38CBD11_H
#ifndef LEADERBOARDDATARESPONSE_T58F860D186FBA6DA0B2EE7B029B213A2C99F8D01_H
#define LEADERBOARDDATARESPONSE_T58F860D186FBA6DA0B2EE7B029B213A2C99F8D01_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Responses.LeaderboardDataResponse
struct  LeaderboardDataResponse_t58F860D186FBA6DA0B2EE7B029B213A2C99F8D01  : public GSTypedResponse_t8D7A8C3784E1A1542ED006C5A97AF8C6D9E849DA
{
public:

public:
};

struct LeaderboardDataResponse_t58F860D186FBA6DA0B2EE7B029B213A2C99F8D01_StaticFields
{
public:
	// System.Func`2<GameSparks.Core.GSData,GameSparks.Api.Responses.LeaderboardDataResponse__LeaderboardData> GameSparks.Api.Responses.LeaderboardDataResponse::CSU24<>9__CachedAnonymousMethodDelegate1
	Func_2_t08297BFF4A7645AA75684B072ECC0AF4D43AC2EF * ___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1;

public:
	inline static int32_t get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1() { return static_cast<int32_t>(offsetof(LeaderboardDataResponse_t58F860D186FBA6DA0B2EE7B029B213A2C99F8D01_StaticFields, ___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1)); }
	inline Func_2_t08297BFF4A7645AA75684B072ECC0AF4D43AC2EF * get_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1() const { return ___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1; }
	inline Func_2_t08297BFF4A7645AA75684B072ECC0AF4D43AC2EF ** get_address_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1() { return &___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1; }
	inline void set_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1(Func_2_t08297BFF4A7645AA75684B072ECC0AF4D43AC2EF * value)
	{
		___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEADERBOARDDATARESPONSE_T58F860D186FBA6DA0B2EE7B029B213A2C99F8D01_H
#ifndef _LEADERBOARDDATA_T5FFE9A7732DE9DCC6DC0DF3B367B84738FD4A31D_H
#define _LEADERBOARDDATA_T5FFE9A7732DE9DCC6DC0DF3B367B84738FD4A31D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Responses.LeaderboardDataResponse__LeaderboardData
struct  _LeaderboardData_t5FFE9A7732DE9DCC6DC0DF3B367B84738FD4A31D  : public GSTypedResponse_t8D7A8C3784E1A1542ED006C5A97AF8C6D9E849DA
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // _LEADERBOARDDATA_T5FFE9A7732DE9DCC6DC0DF3B367B84738FD4A31D_H
#ifndef LISTACHIEVEMENTSRESPONSE_TD7A98A8A65DF8F9E4F254DA2C35D7A8FE27C0A77_H
#define LISTACHIEVEMENTSRESPONSE_TD7A98A8A65DF8F9E4F254DA2C35D7A8FE27C0A77_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Responses.ListAchievementsResponse
struct  ListAchievementsResponse_tD7A98A8A65DF8F9E4F254DA2C35D7A8FE27C0A77  : public GSTypedResponse_t8D7A8C3784E1A1542ED006C5A97AF8C6D9E849DA
{
public:

public:
};

struct ListAchievementsResponse_tD7A98A8A65DF8F9E4F254DA2C35D7A8FE27C0A77_StaticFields
{
public:
	// System.Func`2<GameSparks.Core.GSData,GameSparks.Api.Responses.ListAchievementsResponse__Achievement> GameSparks.Api.Responses.ListAchievementsResponse::CSU24<>9__CachedAnonymousMethodDelegate1
	Func_2_t5629C21721ADCAE00DF8837F21427A89252D0F91 * ___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1;

public:
	inline static int32_t get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1() { return static_cast<int32_t>(offsetof(ListAchievementsResponse_tD7A98A8A65DF8F9E4F254DA2C35D7A8FE27C0A77_StaticFields, ___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1)); }
	inline Func_2_t5629C21721ADCAE00DF8837F21427A89252D0F91 * get_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1() const { return ___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1; }
	inline Func_2_t5629C21721ADCAE00DF8837F21427A89252D0F91 ** get_address_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1() { return &___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1; }
	inline void set_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1(Func_2_t5629C21721ADCAE00DF8837F21427A89252D0F91 * value)
	{
		___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LISTACHIEVEMENTSRESPONSE_TD7A98A8A65DF8F9E4F254DA2C35D7A8FE27C0A77_H
#ifndef _ACHIEVEMENT_T5A67D51560E727D78FB8978C5B14D302D298A621_H
#define _ACHIEVEMENT_T5A67D51560E727D78FB8978C5B14D302D298A621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Responses.ListAchievementsResponse__Achievement
struct  _Achievement_t5A67D51560E727D78FB8978C5B14D302D298A621  : public GSTypedResponse_t8D7A8C3784E1A1542ED006C5A97AF8C6D9E849DA
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // _ACHIEVEMENT_T5A67D51560E727D78FB8978C5B14D302D298A621_H
#ifndef LISTCHALLENGETYPERESPONSE_TCDDFA2D040622E0C10B656AE02D62FDF7C48684B_H
#define LISTCHALLENGETYPERESPONSE_TCDDFA2D040622E0C10B656AE02D62FDF7C48684B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Responses.ListChallengeTypeResponse
struct  ListChallengeTypeResponse_tCDDFA2D040622E0C10B656AE02D62FDF7C48684B  : public GSTypedResponse_t8D7A8C3784E1A1542ED006C5A97AF8C6D9E849DA
{
public:

public:
};

struct ListChallengeTypeResponse_tCDDFA2D040622E0C10B656AE02D62FDF7C48684B_StaticFields
{
public:
	// System.Func`2<GameSparks.Core.GSData,GameSparks.Api.Responses.ListChallengeTypeResponse__ChallengeType> GameSparks.Api.Responses.ListChallengeTypeResponse::CSU24<>9__CachedAnonymousMethodDelegate1
	Func_2_t5C8807919A4FE1E185DE9E06963B3D039D5A2A60 * ___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1;

public:
	inline static int32_t get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1() { return static_cast<int32_t>(offsetof(ListChallengeTypeResponse_tCDDFA2D040622E0C10B656AE02D62FDF7C48684B_StaticFields, ___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1)); }
	inline Func_2_t5C8807919A4FE1E185DE9E06963B3D039D5A2A60 * get_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1() const { return ___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1; }
	inline Func_2_t5C8807919A4FE1E185DE9E06963B3D039D5A2A60 ** get_address_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1() { return &___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1; }
	inline void set_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1(Func_2_t5C8807919A4FE1E185DE9E06963B3D039D5A2A60 * value)
	{
		___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LISTCHALLENGETYPERESPONSE_TCDDFA2D040622E0C10B656AE02D62FDF7C48684B_H
#ifndef _CHALLENGETYPE_TBEECFFB505E10736A7F160CC45BD4C0DDEA0948C_H
#define _CHALLENGETYPE_TBEECFFB505E10736A7F160CC45BD4C0DDEA0948C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Responses.ListChallengeTypeResponse__ChallengeType
struct  _ChallengeType_tBEECFFB505E10736A7F160CC45BD4C0DDEA0948C  : public GSTypedResponse_t8D7A8C3784E1A1542ED006C5A97AF8C6D9E849DA
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // _CHALLENGETYPE_TBEECFFB505E10736A7F160CC45BD4C0DDEA0948C_H
#ifndef LISTGAMEFRIENDSRESPONSE_TCF46E5547CDBCE9ED28E1E2F226F6D525880F44A_H
#define LISTGAMEFRIENDSRESPONSE_TCF46E5547CDBCE9ED28E1E2F226F6D525880F44A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Responses.ListGameFriendsResponse
struct  ListGameFriendsResponse_tCF46E5547CDBCE9ED28E1E2F226F6D525880F44A  : public GSTypedResponse_t8D7A8C3784E1A1542ED006C5A97AF8C6D9E849DA
{
public:

public:
};

struct ListGameFriendsResponse_tCF46E5547CDBCE9ED28E1E2F226F6D525880F44A_StaticFields
{
public:
	// System.Func`2<GameSparks.Core.GSData,GameSparks.Api.Responses.ListGameFriendsResponse__Player> GameSparks.Api.Responses.ListGameFriendsResponse::CSU24<>9__CachedAnonymousMethodDelegate1
	Func_2_t5F4F2DC35BE82525069649BE26778B1BA06CDCEF * ___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1;

public:
	inline static int32_t get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1() { return static_cast<int32_t>(offsetof(ListGameFriendsResponse_tCF46E5547CDBCE9ED28E1E2F226F6D525880F44A_StaticFields, ___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1)); }
	inline Func_2_t5F4F2DC35BE82525069649BE26778B1BA06CDCEF * get_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1() const { return ___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1; }
	inline Func_2_t5F4F2DC35BE82525069649BE26778B1BA06CDCEF ** get_address_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1() { return &___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1; }
	inline void set_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1(Func_2_t5F4F2DC35BE82525069649BE26778B1BA06CDCEF * value)
	{
		___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LISTGAMEFRIENDSRESPONSE_TCF46E5547CDBCE9ED28E1E2F226F6D525880F44A_H
#ifndef _PLAYER_TBC635FE551F99944D122C94DEE9E08627307E8C0_H
#define _PLAYER_TBC635FE551F99944D122C94DEE9E08627307E8C0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Responses.ListGameFriendsResponse__Player
struct  _Player_tBC635FE551F99944D122C94DEE9E08627307E8C0  : public GSTypedResponse_t8D7A8C3784E1A1542ED006C5A97AF8C6D9E849DA
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // _PLAYER_TBC635FE551F99944D122C94DEE9E08627307E8C0_H
#ifndef LISTMESSAGERESPONSE_T9862CD65548D08EFFB2BA69D0C3B93635021C05E_H
#define LISTMESSAGERESPONSE_T9862CD65548D08EFFB2BA69D0C3B93635021C05E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Responses.ListMessageResponse
struct  ListMessageResponse_t9862CD65548D08EFFB2BA69D0C3B93635021C05E  : public GSTypedResponse_t8D7A8C3784E1A1542ED006C5A97AF8C6D9E849DA
{
public:

public:
};

struct ListMessageResponse_t9862CD65548D08EFFB2BA69D0C3B93635021C05E_StaticFields
{
public:
	// System.Func`2<GameSparks.Core.GSData,GameSparks.Core.GSData> GameSparks.Api.Responses.ListMessageResponse::CSU24<>9__CachedAnonymousMethodDelegate1
	Func_2_t703C40526E5E529BF24AFBA1F56E645E730ABC2F * ___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1;

public:
	inline static int32_t get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1() { return static_cast<int32_t>(offsetof(ListMessageResponse_t9862CD65548D08EFFB2BA69D0C3B93635021C05E_StaticFields, ___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1)); }
	inline Func_2_t703C40526E5E529BF24AFBA1F56E645E730ABC2F * get_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1() const { return ___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1; }
	inline Func_2_t703C40526E5E529BF24AFBA1F56E645E730ABC2F ** get_address_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1() { return &___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1; }
	inline void set_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1(Func_2_t703C40526E5E529BF24AFBA1F56E645E730ABC2F * value)
	{
		___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LISTMESSAGERESPONSE_T9862CD65548D08EFFB2BA69D0C3B93635021C05E_H
#ifndef LISTTEAMCHATRESPONSE_TB86A205B7C490408D298552A6C63AA174E59EAB4_H
#define LISTTEAMCHATRESPONSE_TB86A205B7C490408D298552A6C63AA174E59EAB4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Responses.ListTeamChatResponse
struct  ListTeamChatResponse_tB86A205B7C490408D298552A6C63AA174E59EAB4  : public GSTypedResponse_t8D7A8C3784E1A1542ED006C5A97AF8C6D9E849DA
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LISTTEAMCHATRESPONSE_TB86A205B7C490408D298552A6C63AA174E59EAB4_H
#ifndef LISTVIRTUALGOODSRESPONSE_TB0E0F35F7DB05784B9C10DB619FCDCF0F0FE942D_H
#define LISTVIRTUALGOODSRESPONSE_TB0E0F35F7DB05784B9C10DB619FCDCF0F0FE942D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Responses.ListVirtualGoodsResponse
struct  ListVirtualGoodsResponse_tB0E0F35F7DB05784B9C10DB619FCDCF0F0FE942D  : public GSTypedResponse_t8D7A8C3784E1A1542ED006C5A97AF8C6D9E849DA
{
public:

public:
};

struct ListVirtualGoodsResponse_tB0E0F35F7DB05784B9C10DB619FCDCF0F0FE942D_StaticFields
{
public:
	// System.Func`2<GameSparks.Core.GSData,GameSparks.Api.Responses.ListVirtualGoodsResponse__VirtualGood> GameSparks.Api.Responses.ListVirtualGoodsResponse::CSU24<>9__CachedAnonymousMethodDelegate1
	Func_2_t3B1B6C18C5B135652246DF62DA2EFE21C6FC2926 * ___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1;

public:
	inline static int32_t get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1() { return static_cast<int32_t>(offsetof(ListVirtualGoodsResponse_tB0E0F35F7DB05784B9C10DB619FCDCF0F0FE942D_StaticFields, ___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1)); }
	inline Func_2_t3B1B6C18C5B135652246DF62DA2EFE21C6FC2926 * get_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1() const { return ___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1; }
	inline Func_2_t3B1B6C18C5B135652246DF62DA2EFE21C6FC2926 ** get_address_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1() { return &___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1; }
	inline void set_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1(Func_2_t3B1B6C18C5B135652246DF62DA2EFE21C6FC2926 * value)
	{
		___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LISTVIRTUALGOODSRESPONSE_TB0E0F35F7DB05784B9C10DB619FCDCF0F0FE942D_H
#ifndef _VIRTUALGOOD_T4A3F1C7313A837E0E4C40BD8625ADEBD02FC3465_H
#define _VIRTUALGOOD_T4A3F1C7313A837E0E4C40BD8625ADEBD02FC3465_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Responses.ListVirtualGoodsResponse__VirtualGood
struct  _VirtualGood_t4A3F1C7313A837E0E4C40BD8625ADEBD02FC3465  : public GSTypedResponse_t8D7A8C3784E1A1542ED006C5A97AF8C6D9E849DA
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // _VIRTUALGOOD_T4A3F1C7313A837E0E4C40BD8625ADEBD02FC3465_H
#ifndef LOGEVENTRESPONSE_TBF49C2850F329381D50C71DA2B2FFC22FEAA3FE7_H
#define LOGEVENTRESPONSE_TBF49C2850F329381D50C71DA2B2FFC22FEAA3FE7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Responses.LogEventResponse
struct  LogEventResponse_tBF49C2850F329381D50C71DA2B2FFC22FEAA3FE7  : public GSTypedResponse_t8D7A8C3784E1A1542ED006C5A97AF8C6D9E849DA
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGEVENTRESPONSE_TBF49C2850F329381D50C71DA2B2FFC22FEAA3FE7_H
#ifndef SENDTEAMCHATMESSAGERESPONSE_T91BB9AD9970803E40D7C310D2ECF6796704DADA8_H
#define SENDTEAMCHATMESSAGERESPONSE_T91BB9AD9970803E40D7C310D2ECF6796704DADA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Responses.SendTeamChatMessageResponse
struct  SendTeamChatMessageResponse_t91BB9AD9970803E40D7C310D2ECF6796704DADA8  : public GSTypedResponse_t8D7A8C3784E1A1542ED006C5A97AF8C6D9E849DA
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SENDTEAMCHATMESSAGERESPONSE_T91BB9AD9970803E40D7C310D2ECF6796704DADA8_H
#ifndef GSREQUESTDATA_T84713ABAAB257F300A1DB26F09C91AB0519109F8_H
#define GSREQUESTDATA_T84713ABAAB257F300A1DB26F09C91AB0519109F8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Core.GSRequestData
struct  GSRequestData_t84713ABAAB257F300A1DB26F09C91AB0519109F8  : public GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GSREQUESTDATA_T84713ABAAB257F300A1DB26F09C91AB0519109F8_H
#ifndef FASTCONNECTION_T9E83C1C0DD332A4579DA0512357037F285CA25A0_H
#define FASTCONNECTION_T9E83C1C0DD332A4579DA0512357037F285CA25A0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.RT.Connection.FastConnection
struct  FastConnection_t9E83C1C0DD332A4579DA0512357037F285CA25A0  : public Connection_tBC3E695B64A7864140245909D74866C1FE9A2818
{
public:
	// System.Net.Sockets.UdpClient GameSparks.RT.Connection.FastConnection::client
	UdpClient_t94741C2FBA0D9E3270ABDDBA57811D00881D5641 * ___client_4;
	// System.AsyncCallback GameSparks.RT.Connection.FastConnection::callback
	AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___callback_5;
	// System.Byte[] GameSparks.RT.Connection.FastConnection::buffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___buffer_6;
	// GameSparks.RT.BinaryWriteMemoryStream GameSparks.RT.Connection.FastConnection::ms
	BinaryWriteMemoryStream_t9503BBB30B8671EB7F1F21C98D1A5B0CBFD6B84A * ___ms_7;
	// System.Int32 GameSparks.RT.Connection.FastConnection::connectionAttempts
	int32_t ___connectionAttempts_8;

public:
	inline static int32_t get_offset_of_client_4() { return static_cast<int32_t>(offsetof(FastConnection_t9E83C1C0DD332A4579DA0512357037F285CA25A0, ___client_4)); }
	inline UdpClient_t94741C2FBA0D9E3270ABDDBA57811D00881D5641 * get_client_4() const { return ___client_4; }
	inline UdpClient_t94741C2FBA0D9E3270ABDDBA57811D00881D5641 ** get_address_of_client_4() { return &___client_4; }
	inline void set_client_4(UdpClient_t94741C2FBA0D9E3270ABDDBA57811D00881D5641 * value)
	{
		___client_4 = value;
		Il2CppCodeGenWriteBarrier((&___client_4), value);
	}

	inline static int32_t get_offset_of_callback_5() { return static_cast<int32_t>(offsetof(FastConnection_t9E83C1C0DD332A4579DA0512357037F285CA25A0, ___callback_5)); }
	inline AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * get_callback_5() const { return ___callback_5; }
	inline AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 ** get_address_of_callback_5() { return &___callback_5; }
	inline void set_callback_5(AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * value)
	{
		___callback_5 = value;
		Il2CppCodeGenWriteBarrier((&___callback_5), value);
	}

	inline static int32_t get_offset_of_buffer_6() { return static_cast<int32_t>(offsetof(FastConnection_t9E83C1C0DD332A4579DA0512357037F285CA25A0, ___buffer_6)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_buffer_6() const { return ___buffer_6; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_buffer_6() { return &___buffer_6; }
	inline void set_buffer_6(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___buffer_6 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_6), value);
	}

	inline static int32_t get_offset_of_ms_7() { return static_cast<int32_t>(offsetof(FastConnection_t9E83C1C0DD332A4579DA0512357037F285CA25A0, ___ms_7)); }
	inline BinaryWriteMemoryStream_t9503BBB30B8671EB7F1F21C98D1A5B0CBFD6B84A * get_ms_7() const { return ___ms_7; }
	inline BinaryWriteMemoryStream_t9503BBB30B8671EB7F1F21C98D1A5B0CBFD6B84A ** get_address_of_ms_7() { return &___ms_7; }
	inline void set_ms_7(BinaryWriteMemoryStream_t9503BBB30B8671EB7F1F21C98D1A5B0CBFD6B84A * value)
	{
		___ms_7 = value;
		Il2CppCodeGenWriteBarrier((&___ms_7), value);
	}

	inline static int32_t get_offset_of_connectionAttempts_8() { return static_cast<int32_t>(offsetof(FastConnection_t9E83C1C0DD332A4579DA0512357037F285CA25A0, ___connectionAttempts_8)); }
	inline int32_t get_connectionAttempts_8() const { return ___connectionAttempts_8; }
	inline int32_t* get_address_of_connectionAttempts_8() { return &___connectionAttempts_8; }
	inline void set_connectionAttempts_8(int32_t value)
	{
		___connectionAttempts_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FASTCONNECTION_T9E83C1C0DD332A4579DA0512357037F285CA25A0_H
#ifndef RELIABLECONNECTION_T237BB0704E0E315A9E262CD04956628C1D2435B0_H
#define RELIABLECONNECTION_T237BB0704E0E315A9E262CD04956628C1D2435B0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.RT.Connection.ReliableConnection
struct  ReliableConnection_t237BB0704E0E315A9E262CD04956628C1D2435B0  : public Connection_tBC3E695B64A7864140245909D74866C1FE9A2818
{
public:
	// System.Net.Sockets.TcpClient GameSparks.RT.Connection.ReliableConnection::client
	TcpClient_t8BC37A84681D1839590AE10B14C25BA473063EDB * ___client_4;
	// System.IO.Stream GameSparks.RT.Connection.ReliableConnection::clientStream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___clientStream_5;
	// System.String GameSparks.RT.Connection.ReliableConnection::remotehost
	String_t* ___remotehost_6;

public:
	inline static int32_t get_offset_of_client_4() { return static_cast<int32_t>(offsetof(ReliableConnection_t237BB0704E0E315A9E262CD04956628C1D2435B0, ___client_4)); }
	inline TcpClient_t8BC37A84681D1839590AE10B14C25BA473063EDB * get_client_4() const { return ___client_4; }
	inline TcpClient_t8BC37A84681D1839590AE10B14C25BA473063EDB ** get_address_of_client_4() { return &___client_4; }
	inline void set_client_4(TcpClient_t8BC37A84681D1839590AE10B14C25BA473063EDB * value)
	{
		___client_4 = value;
		Il2CppCodeGenWriteBarrier((&___client_4), value);
	}

	inline static int32_t get_offset_of_clientStream_5() { return static_cast<int32_t>(offsetof(ReliableConnection_t237BB0704E0E315A9E262CD04956628C1D2435B0, ___clientStream_5)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_clientStream_5() const { return ___clientStream_5; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_clientStream_5() { return &___clientStream_5; }
	inline void set_clientStream_5(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___clientStream_5 = value;
		Il2CppCodeGenWriteBarrier((&___clientStream_5), value);
	}

	inline static int32_t get_offset_of_remotehost_6() { return static_cast<int32_t>(offsetof(ReliableConnection_t237BB0704E0E315A9E262CD04956628C1D2435B0, ___remotehost_6)); }
	inline String_t* get_remotehost_6() const { return ___remotehost_6; }
	inline String_t** get_address_of_remotehost_6() { return &___remotehost_6; }
	inline void set_remotehost_6(String_t* value)
	{
		___remotehost_6 = value;
		Il2CppCodeGenWriteBarrier((&___remotehost_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RELIABLECONNECTION_T237BB0704E0E315A9E262CD04956628C1D2435B0_H
#ifndef PROTOCOLBUFFEREXCEPTION_TC3A6856D10B5F85950B0BECA2A9F8683362FA29F_H
#define PROTOCOLBUFFEREXCEPTION_TC3A6856D10B5F85950B0BECA2A9F8683362FA29F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.RT.Proto.ProtocolBufferException
struct  ProtocolBufferException_tC3A6856D10B5F85950B0BECA2A9F8683362FA29F  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROTOCOLBUFFEREXCEPTION_TC3A6856D10B5F85950B0BECA2A9F8683362FA29F_H
#ifndef RTPACKET_T7C1F65EE13D0F8BC65B8DFD3A3FE92C4171A4BC4_H
#define RTPACKET_T7C1F65EE13D0F8BC65B8DFD3A3FE92C4171A4BC4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.RT.RTPacket
struct  RTPacket_t7C1F65EE13D0F8BC65B8DFD3A3FE92C4171A4BC4 
{
public:
	// System.Int32 GameSparks.RT.RTPacket::OpCode
	int32_t ___OpCode_0;
	// System.Int32 GameSparks.RT.RTPacket::Sender
	int32_t ___Sender_1;
	// System.IO.Stream GameSparks.RT.RTPacket::Stream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___Stream_2;
	// System.Int32 GameSparks.RT.RTPacket::StreamLength
	int32_t ___StreamLength_3;
	// GameSparks.RT.RTData GameSparks.RT.RTPacket::Data
	RTData_tAAEBF5D36A61DACA40791F6B7E2D59B7FAF099B5 * ___Data_4;
	// System.Int32 GameSparks.RT.RTPacket::PacketSize
	int32_t ___PacketSize_5;

public:
	inline static int32_t get_offset_of_OpCode_0() { return static_cast<int32_t>(offsetof(RTPacket_t7C1F65EE13D0F8BC65B8DFD3A3FE92C4171A4BC4, ___OpCode_0)); }
	inline int32_t get_OpCode_0() const { return ___OpCode_0; }
	inline int32_t* get_address_of_OpCode_0() { return &___OpCode_0; }
	inline void set_OpCode_0(int32_t value)
	{
		___OpCode_0 = value;
	}

	inline static int32_t get_offset_of_Sender_1() { return static_cast<int32_t>(offsetof(RTPacket_t7C1F65EE13D0F8BC65B8DFD3A3FE92C4171A4BC4, ___Sender_1)); }
	inline int32_t get_Sender_1() const { return ___Sender_1; }
	inline int32_t* get_address_of_Sender_1() { return &___Sender_1; }
	inline void set_Sender_1(int32_t value)
	{
		___Sender_1 = value;
	}

	inline static int32_t get_offset_of_Stream_2() { return static_cast<int32_t>(offsetof(RTPacket_t7C1F65EE13D0F8BC65B8DFD3A3FE92C4171A4BC4, ___Stream_2)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_Stream_2() const { return ___Stream_2; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_Stream_2() { return &___Stream_2; }
	inline void set_Stream_2(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___Stream_2 = value;
		Il2CppCodeGenWriteBarrier((&___Stream_2), value);
	}

	inline static int32_t get_offset_of_StreamLength_3() { return static_cast<int32_t>(offsetof(RTPacket_t7C1F65EE13D0F8BC65B8DFD3A3FE92C4171A4BC4, ___StreamLength_3)); }
	inline int32_t get_StreamLength_3() const { return ___StreamLength_3; }
	inline int32_t* get_address_of_StreamLength_3() { return &___StreamLength_3; }
	inline void set_StreamLength_3(int32_t value)
	{
		___StreamLength_3 = value;
	}

	inline static int32_t get_offset_of_Data_4() { return static_cast<int32_t>(offsetof(RTPacket_t7C1F65EE13D0F8BC65B8DFD3A3FE92C4171A4BC4, ___Data_4)); }
	inline RTData_tAAEBF5D36A61DACA40791F6B7E2D59B7FAF099B5 * get_Data_4() const { return ___Data_4; }
	inline RTData_tAAEBF5D36A61DACA40791F6B7E2D59B7FAF099B5 ** get_address_of_Data_4() { return &___Data_4; }
	inline void set_Data_4(RTData_tAAEBF5D36A61DACA40791F6B7E2D59B7FAF099B5 * value)
	{
		___Data_4 = value;
		Il2CppCodeGenWriteBarrier((&___Data_4), value);
	}

	inline static int32_t get_offset_of_PacketSize_5() { return static_cast<int32_t>(offsetof(RTPacket_t7C1F65EE13D0F8BC65B8DFD3A3FE92C4171A4BC4, ___PacketSize_5)); }
	inline int32_t get_PacketSize_5() const { return ___PacketSize_5; }
	inline int32_t* get_address_of_PacketSize_5() { return &___PacketSize_5; }
	inline void set_PacketSize_5(int32_t value)
	{
		___PacketSize_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of GameSparks.RT.RTPacket
struct RTPacket_t7C1F65EE13D0F8BC65B8DFD3A3FE92C4171A4BC4_marshaled_pinvoke
{
	int32_t ___OpCode_0;
	int32_t ___Sender_1;
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___Stream_2;
	int32_t ___StreamLength_3;
	RTData_tAAEBF5D36A61DACA40791F6B7E2D59B7FAF099B5 * ___Data_4;
	int32_t ___PacketSize_5;
};
// Native definition for COM marshalling of GameSparks.RT.RTPacket
struct RTPacket_t7C1F65EE13D0F8BC65B8DFD3A3FE92C4171A4BC4_marshaled_com
{
	int32_t ___OpCode_0;
	int32_t ___Sender_1;
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___Stream_2;
	int32_t ___StreamLength_3;
	RTData_tAAEBF5D36A61DACA40791F6B7E2D59B7FAF099B5 * ___Data_4;
	int32_t ___PacketSize_5;
};
#endif // RTPACKET_T7C1F65EE13D0F8BC65B8DFD3A3FE92C4171A4BC4_H
#ifndef ABSTRACTTLSCLIENT_TC6DAEE1B4537D4C522014C4971FC936BD5C964F7_H
#define ABSTRACTTLSCLIENT_TC6DAEE1B4537D4C522014C4971FC936BD5C964F7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.AbstractTlsClient
struct  AbstractTlsClient_tC6DAEE1B4537D4C522014C4971FC936BD5C964F7  : public AbstractTlsPeer_t26CA3EF032C693BEB7331E2A0972A8620596CC9F
{
public:
	// Org.BouncyCastle.Crypto.Tls.TlsCipherFactory Org.BouncyCastle.Crypto.Tls.AbstractTlsClient::mCipherFactory
	RuntimeObject* ___mCipherFactory_0;
	// Org.BouncyCastle.Crypto.Tls.TlsClientContext Org.BouncyCastle.Crypto.Tls.AbstractTlsClient::mContext
	RuntimeObject* ___mContext_1;
	// System.Collections.IList Org.BouncyCastle.Crypto.Tls.AbstractTlsClient::mSupportedSignatureAlgorithms
	RuntimeObject* ___mSupportedSignatureAlgorithms_2;
	// System.Int32[] Org.BouncyCastle.Crypto.Tls.AbstractTlsClient::mNamedCurves
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___mNamedCurves_3;
	// System.Byte[] Org.BouncyCastle.Crypto.Tls.AbstractTlsClient::mClientECPointFormats
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mClientECPointFormats_4;
	// System.Byte[] Org.BouncyCastle.Crypto.Tls.AbstractTlsClient::mServerECPointFormats
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mServerECPointFormats_5;
	// System.Int32 Org.BouncyCastle.Crypto.Tls.AbstractTlsClient::mSelectedCipherSuite
	int32_t ___mSelectedCipherSuite_6;
	// System.Int16 Org.BouncyCastle.Crypto.Tls.AbstractTlsClient::mSelectedCompressionMethod
	int16_t ___mSelectedCompressionMethod_7;

public:
	inline static int32_t get_offset_of_mCipherFactory_0() { return static_cast<int32_t>(offsetof(AbstractTlsClient_tC6DAEE1B4537D4C522014C4971FC936BD5C964F7, ___mCipherFactory_0)); }
	inline RuntimeObject* get_mCipherFactory_0() const { return ___mCipherFactory_0; }
	inline RuntimeObject** get_address_of_mCipherFactory_0() { return &___mCipherFactory_0; }
	inline void set_mCipherFactory_0(RuntimeObject* value)
	{
		___mCipherFactory_0 = value;
		Il2CppCodeGenWriteBarrier((&___mCipherFactory_0), value);
	}

	inline static int32_t get_offset_of_mContext_1() { return static_cast<int32_t>(offsetof(AbstractTlsClient_tC6DAEE1B4537D4C522014C4971FC936BD5C964F7, ___mContext_1)); }
	inline RuntimeObject* get_mContext_1() const { return ___mContext_1; }
	inline RuntimeObject** get_address_of_mContext_1() { return &___mContext_1; }
	inline void set_mContext_1(RuntimeObject* value)
	{
		___mContext_1 = value;
		Il2CppCodeGenWriteBarrier((&___mContext_1), value);
	}

	inline static int32_t get_offset_of_mSupportedSignatureAlgorithms_2() { return static_cast<int32_t>(offsetof(AbstractTlsClient_tC6DAEE1B4537D4C522014C4971FC936BD5C964F7, ___mSupportedSignatureAlgorithms_2)); }
	inline RuntimeObject* get_mSupportedSignatureAlgorithms_2() const { return ___mSupportedSignatureAlgorithms_2; }
	inline RuntimeObject** get_address_of_mSupportedSignatureAlgorithms_2() { return &___mSupportedSignatureAlgorithms_2; }
	inline void set_mSupportedSignatureAlgorithms_2(RuntimeObject* value)
	{
		___mSupportedSignatureAlgorithms_2 = value;
		Il2CppCodeGenWriteBarrier((&___mSupportedSignatureAlgorithms_2), value);
	}

	inline static int32_t get_offset_of_mNamedCurves_3() { return static_cast<int32_t>(offsetof(AbstractTlsClient_tC6DAEE1B4537D4C522014C4971FC936BD5C964F7, ___mNamedCurves_3)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_mNamedCurves_3() const { return ___mNamedCurves_3; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_mNamedCurves_3() { return &___mNamedCurves_3; }
	inline void set_mNamedCurves_3(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___mNamedCurves_3 = value;
		Il2CppCodeGenWriteBarrier((&___mNamedCurves_3), value);
	}

	inline static int32_t get_offset_of_mClientECPointFormats_4() { return static_cast<int32_t>(offsetof(AbstractTlsClient_tC6DAEE1B4537D4C522014C4971FC936BD5C964F7, ___mClientECPointFormats_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mClientECPointFormats_4() const { return ___mClientECPointFormats_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mClientECPointFormats_4() { return &___mClientECPointFormats_4; }
	inline void set_mClientECPointFormats_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mClientECPointFormats_4 = value;
		Il2CppCodeGenWriteBarrier((&___mClientECPointFormats_4), value);
	}

	inline static int32_t get_offset_of_mServerECPointFormats_5() { return static_cast<int32_t>(offsetof(AbstractTlsClient_tC6DAEE1B4537D4C522014C4971FC936BD5C964F7, ___mServerECPointFormats_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mServerECPointFormats_5() const { return ___mServerECPointFormats_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mServerECPointFormats_5() { return &___mServerECPointFormats_5; }
	inline void set_mServerECPointFormats_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mServerECPointFormats_5 = value;
		Il2CppCodeGenWriteBarrier((&___mServerECPointFormats_5), value);
	}

	inline static int32_t get_offset_of_mSelectedCipherSuite_6() { return static_cast<int32_t>(offsetof(AbstractTlsClient_tC6DAEE1B4537D4C522014C4971FC936BD5C964F7, ___mSelectedCipherSuite_6)); }
	inline int32_t get_mSelectedCipherSuite_6() const { return ___mSelectedCipherSuite_6; }
	inline int32_t* get_address_of_mSelectedCipherSuite_6() { return &___mSelectedCipherSuite_6; }
	inline void set_mSelectedCipherSuite_6(int32_t value)
	{
		___mSelectedCipherSuite_6 = value;
	}

	inline static int32_t get_offset_of_mSelectedCompressionMethod_7() { return static_cast<int32_t>(offsetof(AbstractTlsClient_tC6DAEE1B4537D4C522014C4971FC936BD5C964F7, ___mSelectedCompressionMethod_7)); }
	inline int16_t get_mSelectedCompressionMethod_7() const { return ___mSelectedCompressionMethod_7; }
	inline int16_t* get_address_of_mSelectedCompressionMethod_7() { return &___mSelectedCompressionMethod_7; }
	inline void set_mSelectedCompressionMethod_7(int16_t value)
	{
		___mSelectedCompressionMethod_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTTLSCLIENT_TC6DAEE1B4537D4C522014C4971FC936BD5C964F7_H
#ifndef ARRAYSEGMENT_1_T5B17204266E698CC035E2A7F6435A4F78286D0FA_H
#define ARRAYSEGMENT_1_T5B17204266E698CC035E2A7F6435A4F78286D0FA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArraySegment`1<System.Byte>
struct  ArraySegment_1_t5B17204266E698CC035E2A7F6435A4F78286D0FA 
{
public:
	// T[] System.ArraySegment`1::_array
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____array_0;
	// System.Int32 System.ArraySegment`1::_offset
	int32_t ____offset_1;
	// System.Int32 System.ArraySegment`1::_count
	int32_t ____count_2;

public:
	inline static int32_t get_offset_of__array_0() { return static_cast<int32_t>(offsetof(ArraySegment_1_t5B17204266E698CC035E2A7F6435A4F78286D0FA, ____array_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__array_0() const { return ____array_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__array_0() { return &____array_0; }
	inline void set__array_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____array_0 = value;
		Il2CppCodeGenWriteBarrier((&____array_0), value);
	}

	inline static int32_t get_offset_of__offset_1() { return static_cast<int32_t>(offsetof(ArraySegment_1_t5B17204266E698CC035E2A7F6435A4F78286D0FA, ____offset_1)); }
	inline int32_t get__offset_1() const { return ____offset_1; }
	inline int32_t* get_address_of__offset_1() { return &____offset_1; }
	inline void set__offset_1(int32_t value)
	{
		____offset_1 = value;
	}

	inline static int32_t get_offset_of__count_2() { return static_cast<int32_t>(offsetof(ArraySegment_1_t5B17204266E698CC035E2A7F6435A4F78286D0FA, ____count_2)); }
	inline int32_t get__count_2() const { return ____count_2; }
	inline int32_t* get_address_of__count_2() { return &____count_2; }
	inline void set__count_2(int32_t value)
	{
		____count_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRAYSEGMENT_1_T5B17204266E698CC035E2A7F6435A4F78286D0FA_H
#ifndef DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#define DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 
{
public:
	// System.UInt64 System.DateTime::dateData
	uint64_t ___dateData_44;

public:
	inline static int32_t get_offset_of_dateData_44() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132, ___dateData_44)); }
	inline uint64_t get_dateData_44() const { return ___dateData_44; }
	inline uint64_t* get_address_of_dateData_44() { return &___dateData_44; }
	inline void set_dateData_44(uint64_t value)
	{
		___dateData_44 = value;
	}
};

struct DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields
{
public:
	// System.Int32[] System.DateTime::DaysToMonth365
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth365_29;
	// System.Int32[] System.DateTime::DaysToMonth366
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth366_30;
	// System.DateTime System.DateTime::MinValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MinValue_31;
	// System.DateTime System.DateTime::MaxValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MaxValue_32;

public:
	inline static int32_t get_offset_of_DaysToMonth365_29() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth365_29)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth365_29() const { return ___DaysToMonth365_29; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth365_29() { return &___DaysToMonth365_29; }
	inline void set_DaysToMonth365_29(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth365_29 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth365_29), value);
	}

	inline static int32_t get_offset_of_DaysToMonth366_30() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth366_30)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth366_30() const { return ___DaysToMonth366_30; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth366_30() { return &___DaysToMonth366_30; }
	inline void set_DaysToMonth366_30(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth366_30 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth366_30), value);
	}

	inline static int32_t get_offset_of_MinValue_31() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MinValue_31)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MinValue_31() const { return ___MinValue_31; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MinValue_31() { return &___MinValue_31; }
	inline void set_MinValue_31(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MinValue_31 = value;
	}

	inline static int32_t get_offset_of_MaxValue_32() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MaxValue_32)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MaxValue_32() const { return ___MaxValue_32; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MaxValue_32() { return &___MaxValue_32; }
	inline void set_MaxValue_32(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MaxValue_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#define STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Stream
struct  Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7  : public MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF
{
public:
	// System.IO.Stream_ReadWriteTask System.IO.Stream::_activeReadWriteTask
	ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * ____activeReadWriteTask_3;
	// System.Threading.SemaphoreSlim System.IO.Stream::_asyncActiveSemaphore
	SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * ____asyncActiveSemaphore_4;

public:
	inline static int32_t get_offset_of__activeReadWriteTask_3() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7, ____activeReadWriteTask_3)); }
	inline ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * get__activeReadWriteTask_3() const { return ____activeReadWriteTask_3; }
	inline ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 ** get_address_of__activeReadWriteTask_3() { return &____activeReadWriteTask_3; }
	inline void set__activeReadWriteTask_3(ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * value)
	{
		____activeReadWriteTask_3 = value;
		Il2CppCodeGenWriteBarrier((&____activeReadWriteTask_3), value);
	}

	inline static int32_t get_offset_of__asyncActiveSemaphore_4() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7, ____asyncActiveSemaphore_4)); }
	inline SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * get__asyncActiveSemaphore_4() const { return ____asyncActiveSemaphore_4; }
	inline SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 ** get_address_of__asyncActiveSemaphore_4() { return &____asyncActiveSemaphore_4; }
	inline void set__asyncActiveSemaphore_4(SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * value)
	{
		____asyncActiveSemaphore_4 = value;
		Il2CppCodeGenWriteBarrier((&____asyncActiveSemaphore_4), value);
	}
};

struct Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7_StaticFields
{
public:
	// System.IO.Stream System.IO.Stream::Null
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___Null_1;

public:
	inline static int32_t get_offset_of_Null_1() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7_StaticFields, ___Null_1)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_Null_1() const { return ___Null_1; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_Null_1() { return &___Null_1; }
	inline void set_Null_1(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___Null_1 = value;
		Il2CppCodeGenWriteBarrier((&___Null_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#ifndef NULLABLE_1_T9E6A67BECE376F0623B5C857F5674A0311C41793_H
#define NULLABLE_1_T9E6A67BECE376F0623B5C857F5674A0311C41793_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Boolean>
struct  Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 
{
public:
	// T System.Nullable`1::value
	bool ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793, ___value_0)); }
	inline bool get_value_0() const { return ___value_0; }
	inline bool* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(bool value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T9E6A67BECE376F0623B5C857F5674A0311C41793_H
#ifndef NULLABLE_1_TBFE8022F4FD60FAE6E29634776F92F0C7DDF0ECF_H
#define NULLABLE_1_TBFE8022F4FD60FAE6E29634776F92F0C7DDF0ECF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Double>
struct  Nullable_1_tBFE8022F4FD60FAE6E29634776F92F0C7DDF0ECF 
{
public:
	// T System.Nullable`1::value
	double ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_tBFE8022F4FD60FAE6E29634776F92F0C7DDF0ECF, ___value_0)); }
	inline double get_value_0() const { return ___value_0; }
	inline double* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(double value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_tBFE8022F4FD60FAE6E29634776F92F0C7DDF0ECF, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_TBFE8022F4FD60FAE6E29634776F92F0C7DDF0ECF_H
#ifndef NULLABLE_1_T0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB_H
#define NULLABLE_1_T0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Int32>
struct  Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB_H
#ifndef NULLABLE_1_T8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5_H
#define NULLABLE_1_T8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Int64>
struct  Nullable_1_t8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5 
{
public:
	// T System.Nullable`1::value
	int64_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5, ___value_0)); }
	inline int64_t get_value_0() const { return ___value_0; }
	inline int64_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int64_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5_H
#ifndef NULLABLE_1_TE4EDC8D5ED2772A911F67696644E6C77FA716DC0_H
#define NULLABLE_1_TE4EDC8D5ED2772A911F67696644E6C77FA716DC0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Single>
struct  Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0 
{
public:
	// T System.Nullable`1::value
	float ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0, ___value_0)); }
	inline float get_value_0() const { return ___value_0; }
	inline float* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(float value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_TE4EDC8D5ED2772A911F67696644E6C77FA716DC0_H
#ifndef LOGINRESULT_T26281FAE92728CAE243C870E4453A5E36C1E7591_H
#define LOGINRESULT_T26281FAE92728CAE243C870E4453A5E36C1E7591_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Com.Gamesparks.Realtime.Proto.LoginResult
struct  LoginResult_t26281FAE92728CAE243C870E4453A5E36C1E7591  : public AbstractResult_tB9A3C663F42A72B8FCA862FF115309B197235C96
{
public:
	// System.Boolean Com.Gamesparks.Realtime.Proto.LoginResult::<Success>k__BackingField
	bool ___U3CSuccessU3Ek__BackingField_3;
	// System.String Com.Gamesparks.Realtime.Proto.LoginResult::<ReconnectToken>k__BackingField
	String_t* ___U3CReconnectTokenU3Ek__BackingField_4;
	// System.Nullable`1<System.Int32> Com.Gamesparks.Realtime.Proto.LoginResult::<PeerId>k__BackingField
	Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  ___U3CPeerIdU3Ek__BackingField_5;
	// System.Collections.Generic.List`1<System.Int32> Com.Gamesparks.Realtime.Proto.LoginResult::<ActivePeers>k__BackingField
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ___U3CActivePeersU3Ek__BackingField_6;
	// System.Nullable`1<System.Int32> Com.Gamesparks.Realtime.Proto.LoginResult::<FastPort>k__BackingField
	Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  ___U3CFastPortU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_U3CSuccessU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(LoginResult_t26281FAE92728CAE243C870E4453A5E36C1E7591, ___U3CSuccessU3Ek__BackingField_3)); }
	inline bool get_U3CSuccessU3Ek__BackingField_3() const { return ___U3CSuccessU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CSuccessU3Ek__BackingField_3() { return &___U3CSuccessU3Ek__BackingField_3; }
	inline void set_U3CSuccessU3Ek__BackingField_3(bool value)
	{
		___U3CSuccessU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CReconnectTokenU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(LoginResult_t26281FAE92728CAE243C870E4453A5E36C1E7591, ___U3CReconnectTokenU3Ek__BackingField_4)); }
	inline String_t* get_U3CReconnectTokenU3Ek__BackingField_4() const { return ___U3CReconnectTokenU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CReconnectTokenU3Ek__BackingField_4() { return &___U3CReconnectTokenU3Ek__BackingField_4; }
	inline void set_U3CReconnectTokenU3Ek__BackingField_4(String_t* value)
	{
		___U3CReconnectTokenU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CReconnectTokenU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CPeerIdU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(LoginResult_t26281FAE92728CAE243C870E4453A5E36C1E7591, ___U3CPeerIdU3Ek__BackingField_5)); }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  get_U3CPeerIdU3Ek__BackingField_5() const { return ___U3CPeerIdU3Ek__BackingField_5; }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB * get_address_of_U3CPeerIdU3Ek__BackingField_5() { return &___U3CPeerIdU3Ek__BackingField_5; }
	inline void set_U3CPeerIdU3Ek__BackingField_5(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  value)
	{
		___U3CPeerIdU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CActivePeersU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(LoginResult_t26281FAE92728CAE243C870E4453A5E36C1E7591, ___U3CActivePeersU3Ek__BackingField_6)); }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * get_U3CActivePeersU3Ek__BackingField_6() const { return ___U3CActivePeersU3Ek__BackingField_6; }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 ** get_address_of_U3CActivePeersU3Ek__BackingField_6() { return &___U3CActivePeersU3Ek__BackingField_6; }
	inline void set_U3CActivePeersU3Ek__BackingField_6(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * value)
	{
		___U3CActivePeersU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CActivePeersU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CFastPortU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(LoginResult_t26281FAE92728CAE243C870E4453A5E36C1E7591, ___U3CFastPortU3Ek__BackingField_7)); }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  get_U3CFastPortU3Ek__BackingField_7() const { return ___U3CFastPortU3Ek__BackingField_7; }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB * get_address_of_U3CFastPortU3Ek__BackingField_7() { return &___U3CFastPortU3Ek__BackingField_7; }
	inline void set_U3CFastPortU3Ek__BackingField_7(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  value)
	{
		___U3CFastPortU3Ek__BackingField_7 = value;
	}
};

struct LoginResult_t26281FAE92728CAE243C870E4453A5E36C1E7591_StaticFields
{
public:
	// GameSparks.RT.Pools.ObjectPool`1<Com.Gamesparks.Realtime.Proto.LoginResult> Com.Gamesparks.Realtime.Proto.LoginResult::pool
	ObjectPool_1_t51A9BB45150D1669D720D6197A10CBBBF8406FCC * ___pool_2;
	// System.Func`1<Com.Gamesparks.Realtime.Proto.LoginResult> Com.Gamesparks.Realtime.Proto.LoginResult::CSU24<>9__CachedAnonymousMethodDelegate2
	Func_1_tBB3636679D74D021FCD8D4CA1CD998DA620A85F3 * ___CSU24U3CU3E9__CachedAnonymousMethodDelegate2_8;
	// System.Action`1<Com.Gamesparks.Realtime.Proto.LoginResult> Com.Gamesparks.Realtime.Proto.LoginResult::CSU24<>9__CachedAnonymousMethodDelegate3
	Action_1_t8E19666AC943A7CE517BCF84AF96BCC79138A56A * ___CSU24U3CU3E9__CachedAnonymousMethodDelegate3_9;

public:
	inline static int32_t get_offset_of_pool_2() { return static_cast<int32_t>(offsetof(LoginResult_t26281FAE92728CAE243C870E4453A5E36C1E7591_StaticFields, ___pool_2)); }
	inline ObjectPool_1_t51A9BB45150D1669D720D6197A10CBBBF8406FCC * get_pool_2() const { return ___pool_2; }
	inline ObjectPool_1_t51A9BB45150D1669D720D6197A10CBBBF8406FCC ** get_address_of_pool_2() { return &___pool_2; }
	inline void set_pool_2(ObjectPool_1_t51A9BB45150D1669D720D6197A10CBBBF8406FCC * value)
	{
		___pool_2 = value;
		Il2CppCodeGenWriteBarrier((&___pool_2), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate2_8() { return static_cast<int32_t>(offsetof(LoginResult_t26281FAE92728CAE243C870E4453A5E36C1E7591_StaticFields, ___CSU24U3CU3E9__CachedAnonymousMethodDelegate2_8)); }
	inline Func_1_tBB3636679D74D021FCD8D4CA1CD998DA620A85F3 * get_CSU24U3CU3E9__CachedAnonymousMethodDelegate2_8() const { return ___CSU24U3CU3E9__CachedAnonymousMethodDelegate2_8; }
	inline Func_1_tBB3636679D74D021FCD8D4CA1CD998DA620A85F3 ** get_address_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate2_8() { return &___CSU24U3CU3E9__CachedAnonymousMethodDelegate2_8; }
	inline void set_CSU24U3CU3E9__CachedAnonymousMethodDelegate2_8(Func_1_tBB3636679D74D021FCD8D4CA1CD998DA620A85F3 * value)
	{
		___CSU24U3CU3E9__CachedAnonymousMethodDelegate2_8 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E9__CachedAnonymousMethodDelegate2_8), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate3_9() { return static_cast<int32_t>(offsetof(LoginResult_t26281FAE92728CAE243C870E4453A5E36C1E7591_StaticFields, ___CSU24U3CU3E9__CachedAnonymousMethodDelegate3_9)); }
	inline Action_1_t8E19666AC943A7CE517BCF84AF96BCC79138A56A * get_CSU24U3CU3E9__CachedAnonymousMethodDelegate3_9() const { return ___CSU24U3CU3E9__CachedAnonymousMethodDelegate3_9; }
	inline Action_1_t8E19666AC943A7CE517BCF84AF96BCC79138A56A ** get_address_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate3_9() { return &___CSU24U3CU3E9__CachedAnonymousMethodDelegate3_9; }
	inline void set_CSU24U3CU3E9__CachedAnonymousMethodDelegate3_9(Action_1_t8E19666AC943A7CE517BCF84AF96BCC79138A56A * value)
	{
		___CSU24U3CU3E9__CachedAnonymousMethodDelegate3_9 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E9__CachedAnonymousMethodDelegate3_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGINRESULT_T26281FAE92728CAE243C870E4453A5E36C1E7591_H
#ifndef TOKEN_T4CE701F1A10DBCC26C87B634B024F0B8DC00FAFF_H
#define TOKEN_T4CE701F1A10DBCC26C87B634B024F0B8DC00FAFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Core.GSJson_Parser_TOKEN
struct  TOKEN_t4CE701F1A10DBCC26C87B634B024F0B8DC00FAFF 
{
public:
	// System.Int32 GameSparks.Core.GSJson_Parser_TOKEN::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TOKEN_t4CE701F1A10DBCC26C87B634B024F0B8DC00FAFF, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOKEN_T4CE701F1A10DBCC26C87B634B024F0B8DC00FAFF_H
#ifndef GSOBJECT_T05160F6677D08913C6CD9AB7C7DD9454D3B12E88_H
#define GSOBJECT_T05160F6677D08913C6CD9AB7C7DD9454D3B12E88_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Core.GSObject
struct  GSObject_t05160F6677D08913C6CD9AB7C7DD9454D3B12E88  : public GSRequestData_t84713ABAAB257F300A1DB26F09C91AB0519109F8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GSOBJECT_T05160F6677D08913C6CD9AB7C7DD9454D3B12E88_H
#ifndef DUPLEXTLSSTREAM_T0D65185B89BA59E923BFA3A990E535540924D12E_H
#define DUPLEXTLSSTREAM_T0D65185B89BA59E923BFA3A990E535540924D12E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.RT.DuplexTlsStream
struct  DuplexTlsStream_t0D65185B89BA59E923BFA3A990E535540924D12E  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// System.IO.Stream GameSparks.RT.DuplexTlsStream::wrapped
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___wrapped_5;

public:
	inline static int32_t get_offset_of_wrapped_5() { return static_cast<int32_t>(offsetof(DuplexTlsStream_t0D65185B89BA59E923BFA3A990E535540924D12E, ___wrapped_5)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_wrapped_5() const { return ___wrapped_5; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_wrapped_5() { return &___wrapped_5; }
	inline void set_wrapped_5(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___wrapped_5 = value;
		Il2CppCodeGenWriteBarrier((&___wrapped_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DUPLEXTLSSTREAM_T0D65185B89BA59E923BFA3A990E535540924D12E_H
#ifndef CONNECTSTATE_T40B33F90F5E6F36C6AF9DEDA6A8D1DE696343B1B_H
#define CONNECTSTATE_T40B33F90F5E6F36C6AF9DEDA6A8D1DE696343B1B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.RT.GameSparksRT_ConnectState
struct  ConnectState_t40B33F90F5E6F36C6AF9DEDA6A8D1DE696343B1B 
{
public:
	// System.Int32 GameSparks.RT.GameSparksRT_ConnectState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ConnectState_t40B33F90F5E6F36C6AF9DEDA6A8D1DE696343B1B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONNECTSTATE_T40B33F90F5E6F36C6AF9DEDA6A8D1DE696343B1B_H
#ifndef DELIVERYINTENT_TA8A9B56F5F6509947ED27972D07F5117171C55A4_H
#define DELIVERYINTENT_TA8A9B56F5F6509947ED27972D07F5117171C55A4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.RT.GameSparksRT_DeliveryIntent
struct  DeliveryIntent_tA8A9B56F5F6509947ED27972D07F5117171C55A4 
{
public:
	// System.Int32 GameSparks.RT.GameSparksRT_DeliveryIntent::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DeliveryIntent_tA8A9B56F5F6509947ED27972D07F5117171C55A4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELIVERYINTENT_TA8A9B56F5F6509947ED27972D07F5117171C55A4_H
#ifndef LOGLEVEL_T3740A24AA8E97312F00C7B832E3887C611193A9D_H
#define LOGLEVEL_T3740A24AA8E97312F00C7B832E3887C611193A9D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.RT.GameSparksRT_LogLevel
struct  LogLevel_t3740A24AA8E97312F00C7B832E3887C611193A9D 
{
public:
	// System.Int32 GameSparks.RT.GameSparksRT_LogLevel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LogLevel_t3740A24AA8E97312F00C7B832E3887C611193A9D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGLEVEL_T3740A24AA8E97312F00C7B832E3887C611193A9D_H
#ifndef POSITIONSTREAM_TF8B0CE7A2A52EE9A986DECBA4F07F5BFA773228D_H
#define POSITIONSTREAM_TF8B0CE7A2A52EE9A986DECBA4F07F5BFA773228D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.RT.PositionStream
struct  PositionStream_tF8B0CE7A2A52EE9A986DECBA4F07F5BFA773228D  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// System.IO.Stream GameSparks.RT.PositionStream::stream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___stream_5;
	// System.Byte[] GameSparks.RT.PositionStream::readByte
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___readByte_6;
	// System.IO.BinaryReader GameSparks.RT.PositionStream::BinaryReader
	BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969 * ___BinaryReader_7;
	// System.Int32 GameSparks.RT.PositionStream::<BytesRead>k__BackingField
	int32_t ___U3CBytesReadU3Ek__BackingField_8;

public:
	inline static int32_t get_offset_of_stream_5() { return static_cast<int32_t>(offsetof(PositionStream_tF8B0CE7A2A52EE9A986DECBA4F07F5BFA773228D, ___stream_5)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_stream_5() const { return ___stream_5; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_stream_5() { return &___stream_5; }
	inline void set_stream_5(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___stream_5 = value;
		Il2CppCodeGenWriteBarrier((&___stream_5), value);
	}

	inline static int32_t get_offset_of_readByte_6() { return static_cast<int32_t>(offsetof(PositionStream_tF8B0CE7A2A52EE9A986DECBA4F07F5BFA773228D, ___readByte_6)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_readByte_6() const { return ___readByte_6; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_readByte_6() { return &___readByte_6; }
	inline void set_readByte_6(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___readByte_6 = value;
		Il2CppCodeGenWriteBarrier((&___readByte_6), value);
	}

	inline static int32_t get_offset_of_BinaryReader_7() { return static_cast<int32_t>(offsetof(PositionStream_tF8B0CE7A2A52EE9A986DECBA4F07F5BFA773228D, ___BinaryReader_7)); }
	inline BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969 * get_BinaryReader_7() const { return ___BinaryReader_7; }
	inline BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969 ** get_address_of_BinaryReader_7() { return &___BinaryReader_7; }
	inline void set_BinaryReader_7(BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969 * value)
	{
		___BinaryReader_7 = value;
		Il2CppCodeGenWriteBarrier((&___BinaryReader_7), value);
	}

	inline static int32_t get_offset_of_U3CBytesReadU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(PositionStream_tF8B0CE7A2A52EE9A986DECBA4F07F5BFA773228D, ___U3CBytesReadU3Ek__BackingField_8)); }
	inline int32_t get_U3CBytesReadU3Ek__BackingField_8() const { return ___U3CBytesReadU3Ek__BackingField_8; }
	inline int32_t* get_address_of_U3CBytesReadU3Ek__BackingField_8() { return &___U3CBytesReadU3Ek__BackingField_8; }
	inline void set_U3CBytesReadU3Ek__BackingField_8(int32_t value)
	{
		___U3CBytesReadU3Ek__BackingField_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSITIONSTREAM_TF8B0CE7A2A52EE9A986DECBA4F07F5BFA773228D_H
#ifndef PACKET_TA4653355695B640FDF2DC08872C1BA9D1AB35302_H
#define PACKET_TA4653355695B640FDF2DC08872C1BA9D1AB35302_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.RT.Proto.Packet
struct  Packet_tA4653355695B640FDF2DC08872C1BA9D1AB35302  : public RuntimeObject
{
public:
	// System.Boolean GameSparks.RT.Proto.Packet::hasPayload
	bool ___hasPayload_0;
	// System.Int32 GameSparks.RT.Proto.Packet::<OpCode>k__BackingField
	int32_t ___U3COpCodeU3Ek__BackingField_1;
	// System.Nullable`1<System.Int32> GameSparks.RT.Proto.Packet::<SequenceNumber>k__BackingField
	Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  ___U3CSequenceNumberU3Ek__BackingField_2;
	// System.Nullable`1<System.Int32> GameSparks.RT.Proto.Packet::<RequestId>k__BackingField
	Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  ___U3CRequestIdU3Ek__BackingField_3;
	// System.Collections.Generic.List`1<System.Int32> GameSparks.RT.Proto.Packet::<TargetPlayers>k__BackingField
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ___U3CTargetPlayersU3Ek__BackingField_4;
	// System.Nullable`1<System.Int32> GameSparks.RT.Proto.Packet::<Sender>k__BackingField
	Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  ___U3CSenderU3Ek__BackingField_5;
	// System.Nullable`1<System.Boolean> GameSparks.RT.Proto.Packet::<Reliable>k__BackingField
	Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  ___U3CReliableU3Ek__BackingField_6;
	// GameSparks.RT.RTData GameSparks.RT.Proto.Packet::<Data>k__BackingField
	RTData_tAAEBF5D36A61DACA40791F6B7E2D59B7FAF099B5 * ___U3CDataU3Ek__BackingField_7;
	// System.Byte[] GameSparks.RT.Proto.Packet::<Payload>k__BackingField
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___U3CPayloadU3Ek__BackingField_8;
	// GameSparks.RT.Commands.RTRequest GameSparks.RT.Proto.Packet::<Request>k__BackingField
	RTRequest_tAB65BDCB5A0862353CF86D457AA4901F2E32DCC1 * ___U3CRequestU3Ek__BackingField_9;
	// GameSparks.RT.IRTCommand GameSparks.RT.Proto.Packet::<Command>k__BackingField
	RuntimeObject* ___U3CCommandU3Ek__BackingField_10;
	// GameSparks.RT.IRTSessionInternal GameSparks.RT.Proto.Packet::<Session>k__BackingField
	RuntimeObject* ___U3CSessionU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of_hasPayload_0() { return static_cast<int32_t>(offsetof(Packet_tA4653355695B640FDF2DC08872C1BA9D1AB35302, ___hasPayload_0)); }
	inline bool get_hasPayload_0() const { return ___hasPayload_0; }
	inline bool* get_address_of_hasPayload_0() { return &___hasPayload_0; }
	inline void set_hasPayload_0(bool value)
	{
		___hasPayload_0 = value;
	}

	inline static int32_t get_offset_of_U3COpCodeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Packet_tA4653355695B640FDF2DC08872C1BA9D1AB35302, ___U3COpCodeU3Ek__BackingField_1)); }
	inline int32_t get_U3COpCodeU3Ek__BackingField_1() const { return ___U3COpCodeU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3COpCodeU3Ek__BackingField_1() { return &___U3COpCodeU3Ek__BackingField_1; }
	inline void set_U3COpCodeU3Ek__BackingField_1(int32_t value)
	{
		___U3COpCodeU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CSequenceNumberU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Packet_tA4653355695B640FDF2DC08872C1BA9D1AB35302, ___U3CSequenceNumberU3Ek__BackingField_2)); }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  get_U3CSequenceNumberU3Ek__BackingField_2() const { return ___U3CSequenceNumberU3Ek__BackingField_2; }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB * get_address_of_U3CSequenceNumberU3Ek__BackingField_2() { return &___U3CSequenceNumberU3Ek__BackingField_2; }
	inline void set_U3CSequenceNumberU3Ek__BackingField_2(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  value)
	{
		___U3CSequenceNumberU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CRequestIdU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Packet_tA4653355695B640FDF2DC08872C1BA9D1AB35302, ___U3CRequestIdU3Ek__BackingField_3)); }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  get_U3CRequestIdU3Ek__BackingField_3() const { return ___U3CRequestIdU3Ek__BackingField_3; }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB * get_address_of_U3CRequestIdU3Ek__BackingField_3() { return &___U3CRequestIdU3Ek__BackingField_3; }
	inline void set_U3CRequestIdU3Ek__BackingField_3(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  value)
	{
		___U3CRequestIdU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CTargetPlayersU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Packet_tA4653355695B640FDF2DC08872C1BA9D1AB35302, ___U3CTargetPlayersU3Ek__BackingField_4)); }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * get_U3CTargetPlayersU3Ek__BackingField_4() const { return ___U3CTargetPlayersU3Ek__BackingField_4; }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 ** get_address_of_U3CTargetPlayersU3Ek__BackingField_4() { return &___U3CTargetPlayersU3Ek__BackingField_4; }
	inline void set_U3CTargetPlayersU3Ek__BackingField_4(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * value)
	{
		___U3CTargetPlayersU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTargetPlayersU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CSenderU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Packet_tA4653355695B640FDF2DC08872C1BA9D1AB35302, ___U3CSenderU3Ek__BackingField_5)); }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  get_U3CSenderU3Ek__BackingField_5() const { return ___U3CSenderU3Ek__BackingField_5; }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB * get_address_of_U3CSenderU3Ek__BackingField_5() { return &___U3CSenderU3Ek__BackingField_5; }
	inline void set_U3CSenderU3Ek__BackingField_5(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  value)
	{
		___U3CSenderU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CReliableU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Packet_tA4653355695B640FDF2DC08872C1BA9D1AB35302, ___U3CReliableU3Ek__BackingField_6)); }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  get_U3CReliableU3Ek__BackingField_6() const { return ___U3CReliableU3Ek__BackingField_6; }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 * get_address_of_U3CReliableU3Ek__BackingField_6() { return &___U3CReliableU3Ek__BackingField_6; }
	inline void set_U3CReliableU3Ek__BackingField_6(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  value)
	{
		___U3CReliableU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(Packet_tA4653355695B640FDF2DC08872C1BA9D1AB35302, ___U3CDataU3Ek__BackingField_7)); }
	inline RTData_tAAEBF5D36A61DACA40791F6B7E2D59B7FAF099B5 * get_U3CDataU3Ek__BackingField_7() const { return ___U3CDataU3Ek__BackingField_7; }
	inline RTData_tAAEBF5D36A61DACA40791F6B7E2D59B7FAF099B5 ** get_address_of_U3CDataU3Ek__BackingField_7() { return &___U3CDataU3Ek__BackingField_7; }
	inline void set_U3CDataU3Ek__BackingField_7(RTData_tAAEBF5D36A61DACA40791F6B7E2D59B7FAF099B5 * value)
	{
		___U3CDataU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CPayloadU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(Packet_tA4653355695B640FDF2DC08872C1BA9D1AB35302, ___U3CPayloadU3Ek__BackingField_8)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_U3CPayloadU3Ek__BackingField_8() const { return ___U3CPayloadU3Ek__BackingField_8; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_U3CPayloadU3Ek__BackingField_8() { return &___U3CPayloadU3Ek__BackingField_8; }
	inline void set_U3CPayloadU3Ek__BackingField_8(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___U3CPayloadU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPayloadU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3CRequestU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(Packet_tA4653355695B640FDF2DC08872C1BA9D1AB35302, ___U3CRequestU3Ek__BackingField_9)); }
	inline RTRequest_tAB65BDCB5A0862353CF86D457AA4901F2E32DCC1 * get_U3CRequestU3Ek__BackingField_9() const { return ___U3CRequestU3Ek__BackingField_9; }
	inline RTRequest_tAB65BDCB5A0862353CF86D457AA4901F2E32DCC1 ** get_address_of_U3CRequestU3Ek__BackingField_9() { return &___U3CRequestU3Ek__BackingField_9; }
	inline void set_U3CRequestU3Ek__BackingField_9(RTRequest_tAB65BDCB5A0862353CF86D457AA4901F2E32DCC1 * value)
	{
		___U3CRequestU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRequestU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3CCommandU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(Packet_tA4653355695B640FDF2DC08872C1BA9D1AB35302, ___U3CCommandU3Ek__BackingField_10)); }
	inline RuntimeObject* get_U3CCommandU3Ek__BackingField_10() const { return ___U3CCommandU3Ek__BackingField_10; }
	inline RuntimeObject** get_address_of_U3CCommandU3Ek__BackingField_10() { return &___U3CCommandU3Ek__BackingField_10; }
	inline void set_U3CCommandU3Ek__BackingField_10(RuntimeObject* value)
	{
		___U3CCommandU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCommandU3Ek__BackingField_10), value);
	}

	inline static int32_t get_offset_of_U3CSessionU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(Packet_tA4653355695B640FDF2DC08872C1BA9D1AB35302, ___U3CSessionU3Ek__BackingField_11)); }
	inline RuntimeObject* get_U3CSessionU3Ek__BackingField_11() const { return ___U3CSessionU3Ek__BackingField_11; }
	inline RuntimeObject** get_address_of_U3CSessionU3Ek__BackingField_11() { return &___U3CSessionU3Ek__BackingField_11; }
	inline void set_U3CSessionU3Ek__BackingField_11(RuntimeObject* value)
	{
		___U3CSessionU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSessionU3Ek__BackingField_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PACKET_TA4653355695B640FDF2DC08872C1BA9D1AB35302_H
#ifndef WIRE_TF49C8E7023E09914F4A8A9495AB171955348B941_H
#define WIRE_TF49C8E7023E09914F4A8A9495AB171955348B941_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.RT.Proto.Wire
struct  Wire_tF49C8E7023E09914F4A8A9495AB171955348B941 
{
public:
	// System.Int32 GameSparks.RT.Proto.Wire::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Wire_tF49C8E7023E09914F4A8A9495AB171955348B941, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WIRE_TF49C8E7023E09914F4A8A9495AB171955348B941_H
#ifndef RTVECTOR_TF61BCFCD2BC84E278968EE3D89A39BD51AB09026_H
#define RTVECTOR_TF61BCFCD2BC84E278968EE3D89A39BD51AB09026_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.RT.RTVector
struct  RTVector_tF61BCFCD2BC84E278968EE3D89A39BD51AB09026 
{
public:
	// System.Nullable`1<System.Single> GameSparks.RT.RTVector::x
	Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0  ___x_0;
	// System.Nullable`1<System.Single> GameSparks.RT.RTVector::y
	Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0  ___y_1;
	// System.Nullable`1<System.Single> GameSparks.RT.RTVector::z
	Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0  ___z_2;
	// System.Nullable`1<System.Single> GameSparks.RT.RTVector::w
	Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0  ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(RTVector_tF61BCFCD2BC84E278968EE3D89A39BD51AB09026, ___x_0)); }
	inline Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0  get_x_0() const { return ___x_0; }
	inline Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0 * get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0  value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(RTVector_tF61BCFCD2BC84E278968EE3D89A39BD51AB09026, ___y_1)); }
	inline Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0  get_y_1() const { return ___y_1; }
	inline Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0 * get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0  value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(RTVector_tF61BCFCD2BC84E278968EE3D89A39BD51AB09026, ___z_2)); }
	inline Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0  get_z_2() const { return ___z_2; }
	inline Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0 * get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0  value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(RTVector_tF61BCFCD2BC84E278968EE3D89A39BD51AB09026, ___w_3)); }
	inline Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0  get_w_3() const { return ___w_3; }
	inline Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0 * get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0  value)
	{
		___w_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of GameSparks.RT.RTVector
struct RTVector_tF61BCFCD2BC84E278968EE3D89A39BD51AB09026_marshaled_pinvoke
{
	Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0  ___x_0;
	Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0  ___y_1;
	Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0  ___z_2;
	Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0  ___w_3;
};
// Native definition for COM marshalling of GameSparks.RT.RTVector
struct RTVector_tF61BCFCD2BC84E278968EE3D89A39BD51AB09026_marshaled_com
{
	Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0  ___x_0;
	Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0  ___y_1;
	Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0  ___z_2;
	Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0  ___w_3;
};
#endif // RTVECTOR_TF61BCFCD2BC84E278968EE3D89A39BD51AB09026_H
#ifndef DEFAULTTLSCLIENT_T3AEEC6138E8000BF0EB194E242413CB61B9EFF0E_H
#define DEFAULTTLSCLIENT_T3AEEC6138E8000BF0EB194E242413CB61B9EFF0E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.DefaultTlsClient
struct  DefaultTlsClient_t3AEEC6138E8000BF0EB194E242413CB61B9EFF0E  : public AbstractTlsClient_tC6DAEE1B4537D4C522014C4971FC936BD5C964F7
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTTLSCLIENT_T3AEEC6138E8000BF0EB194E242413CB61B9EFF0E_H
#ifndef MEMORYSTREAM_T495F44B85E6B4DDE2BB7E17DE963256A74E2298C_H
#define MEMORYSTREAM_T495F44B85E6B4DDE2BB7E17DE963256A74E2298C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.MemoryStream
struct  MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// System.Byte[] System.IO.MemoryStream::_buffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____buffer_5;
	// System.Int32 System.IO.MemoryStream::_origin
	int32_t ____origin_6;
	// System.Int32 System.IO.MemoryStream::_position
	int32_t ____position_7;
	// System.Int32 System.IO.MemoryStream::_length
	int32_t ____length_8;
	// System.Int32 System.IO.MemoryStream::_capacity
	int32_t ____capacity_9;
	// System.Boolean System.IO.MemoryStream::_expandable
	bool ____expandable_10;
	// System.Boolean System.IO.MemoryStream::_writable
	bool ____writable_11;
	// System.Boolean System.IO.MemoryStream::_exposable
	bool ____exposable_12;
	// System.Boolean System.IO.MemoryStream::_isOpen
	bool ____isOpen_13;
	// System.Threading.Tasks.Task`1<System.Int32> System.IO.MemoryStream::_lastReadTask
	Task_1_t640F0CBB720BB9CD14B90B7B81624471A9F56D87 * ____lastReadTask_14;

public:
	inline static int32_t get_offset_of__buffer_5() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____buffer_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__buffer_5() const { return ____buffer_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__buffer_5() { return &____buffer_5; }
	inline void set__buffer_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____buffer_5 = value;
		Il2CppCodeGenWriteBarrier((&____buffer_5), value);
	}

	inline static int32_t get_offset_of__origin_6() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____origin_6)); }
	inline int32_t get__origin_6() const { return ____origin_6; }
	inline int32_t* get_address_of__origin_6() { return &____origin_6; }
	inline void set__origin_6(int32_t value)
	{
		____origin_6 = value;
	}

	inline static int32_t get_offset_of__position_7() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____position_7)); }
	inline int32_t get__position_7() const { return ____position_7; }
	inline int32_t* get_address_of__position_7() { return &____position_7; }
	inline void set__position_7(int32_t value)
	{
		____position_7 = value;
	}

	inline static int32_t get_offset_of__length_8() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____length_8)); }
	inline int32_t get__length_8() const { return ____length_8; }
	inline int32_t* get_address_of__length_8() { return &____length_8; }
	inline void set__length_8(int32_t value)
	{
		____length_8 = value;
	}

	inline static int32_t get_offset_of__capacity_9() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____capacity_9)); }
	inline int32_t get__capacity_9() const { return ____capacity_9; }
	inline int32_t* get_address_of__capacity_9() { return &____capacity_9; }
	inline void set__capacity_9(int32_t value)
	{
		____capacity_9 = value;
	}

	inline static int32_t get_offset_of__expandable_10() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____expandable_10)); }
	inline bool get__expandable_10() const { return ____expandable_10; }
	inline bool* get_address_of__expandable_10() { return &____expandable_10; }
	inline void set__expandable_10(bool value)
	{
		____expandable_10 = value;
	}

	inline static int32_t get_offset_of__writable_11() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____writable_11)); }
	inline bool get__writable_11() const { return ____writable_11; }
	inline bool* get_address_of__writable_11() { return &____writable_11; }
	inline void set__writable_11(bool value)
	{
		____writable_11 = value;
	}

	inline static int32_t get_offset_of__exposable_12() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____exposable_12)); }
	inline bool get__exposable_12() const { return ____exposable_12; }
	inline bool* get_address_of__exposable_12() { return &____exposable_12; }
	inline void set__exposable_12(bool value)
	{
		____exposable_12 = value;
	}

	inline static int32_t get_offset_of__isOpen_13() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____isOpen_13)); }
	inline bool get__isOpen_13() const { return ____isOpen_13; }
	inline bool* get_address_of__isOpen_13() { return &____isOpen_13; }
	inline void set__isOpen_13(bool value)
	{
		____isOpen_13 = value;
	}

	inline static int32_t get_offset_of__lastReadTask_14() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____lastReadTask_14)); }
	inline Task_1_t640F0CBB720BB9CD14B90B7B81624471A9F56D87 * get__lastReadTask_14() const { return ____lastReadTask_14; }
	inline Task_1_t640F0CBB720BB9CD14B90B7B81624471A9F56D87 ** get_address_of__lastReadTask_14() { return &____lastReadTask_14; }
	inline void set__lastReadTask_14(Task_1_t640F0CBB720BB9CD14B90B7B81624471A9F56D87 * value)
	{
		____lastReadTask_14 = value;
		Il2CppCodeGenWriteBarrier((&____lastReadTask_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMORYSTREAM_T495F44B85E6B4DDE2BB7E17DE963256A74E2298C_H
#ifndef NULLABLE_1_T5F0FEFF3704A8BB5D4D681A8F9D3DE452FAF72E1_H
#define NULLABLE_1_T5F0FEFF3704A8BB5D4D681A8F9D3DE452FAF72E1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.ArraySegment`1<System.Byte>>
struct  Nullable_1_t5F0FEFF3704A8BB5D4D681A8F9D3DE452FAF72E1 
{
public:
	// T System.Nullable`1::value
	ArraySegment_1_t5B17204266E698CC035E2A7F6435A4F78286D0FA  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t5F0FEFF3704A8BB5D4D681A8F9D3DE452FAF72E1, ___value_0)); }
	inline ArraySegment_1_t5B17204266E698CC035E2A7F6435A4F78286D0FA  get_value_0() const { return ___value_0; }
	inline ArraySegment_1_t5B17204266E698CC035E2A7F6435A4F78286D0FA * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(ArraySegment_1_t5B17204266E698CC035E2A7F6435A4F78286D0FA  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t5F0FEFF3704A8BB5D4D681A8F9D3DE452FAF72E1, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T5F0FEFF3704A8BB5D4D681A8F9D3DE452FAF72E1_H
#ifndef GSREQUEST_T12ABAE67A9A983F16B9D82B7050265C35911B1B7_H
#define GSREQUEST_T12ABAE67A9A983F16B9D82B7050265C35911B1B7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Core.GSRequest
struct  GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7  : public GSObject_t05160F6677D08913C6CD9AB7C7DD9454D3B12E88
{
public:
	// GameSparks.Core.GSObject GameSparks.Core.GSRequest::_response
	GSObject_t05160F6677D08913C6CD9AB7C7DD9454D3B12E88 * ____response_1;
	// System.Action`1<GameSparks.Core.GSObject> GameSparks.Core.GSRequest::_callback
	Action_1_t56370BF80900566597FF1377284D989943C5D99B * ____callback_2;
	// System.Action`1<GameSparks.Core.GSObject> GameSparks.Core.GSRequest::_errorCallback
	Action_1_t56370BF80900566597FF1377284D989943C5D99B * ____errorCallback_3;
	// System.Action`1<GameSparks.Core.GSObject> GameSparks.Core.GSRequest::_completer
	Action_1_t56370BF80900566597FF1377284D989943C5D99B * ____completer_4;
	// System.Boolean GameSparks.Core.GSRequest::Durable
	bool ___Durable_5;
	// GameSparks.Core.GSInstance GameSparks.Core.GSRequest::gsInstance
	GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774 * ___gsInstance_6;
	// System.Int64 GameSparks.Core.GSRequest::<WaitForResponseTicks>k__BackingField
	int64_t ___U3CWaitForResponseTicksU3Ek__BackingField_7;
	// System.Int64 GameSparks.Core.GSRequest::<RequestExpiresAt>k__BackingField
	int64_t ___U3CRequestExpiresAtU3Ek__BackingField_8;
	// System.Int32 GameSparks.Core.GSRequest::<DurableAttempts>k__BackingField
	int32_t ___U3CDurableAttemptsU3Ek__BackingField_9;
	// System.Int32 GameSparks.Core.GSRequest::<MaxResponseTimeInMillis>k__BackingField
	int32_t ___U3CMaxResponseTimeInMillisU3Ek__BackingField_10;

public:
	inline static int32_t get_offset_of__response_1() { return static_cast<int32_t>(offsetof(GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7, ____response_1)); }
	inline GSObject_t05160F6677D08913C6CD9AB7C7DD9454D3B12E88 * get__response_1() const { return ____response_1; }
	inline GSObject_t05160F6677D08913C6CD9AB7C7DD9454D3B12E88 ** get_address_of__response_1() { return &____response_1; }
	inline void set__response_1(GSObject_t05160F6677D08913C6CD9AB7C7DD9454D3B12E88 * value)
	{
		____response_1 = value;
		Il2CppCodeGenWriteBarrier((&____response_1), value);
	}

	inline static int32_t get_offset_of__callback_2() { return static_cast<int32_t>(offsetof(GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7, ____callback_2)); }
	inline Action_1_t56370BF80900566597FF1377284D989943C5D99B * get__callback_2() const { return ____callback_2; }
	inline Action_1_t56370BF80900566597FF1377284D989943C5D99B ** get_address_of__callback_2() { return &____callback_2; }
	inline void set__callback_2(Action_1_t56370BF80900566597FF1377284D989943C5D99B * value)
	{
		____callback_2 = value;
		Il2CppCodeGenWriteBarrier((&____callback_2), value);
	}

	inline static int32_t get_offset_of__errorCallback_3() { return static_cast<int32_t>(offsetof(GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7, ____errorCallback_3)); }
	inline Action_1_t56370BF80900566597FF1377284D989943C5D99B * get__errorCallback_3() const { return ____errorCallback_3; }
	inline Action_1_t56370BF80900566597FF1377284D989943C5D99B ** get_address_of__errorCallback_3() { return &____errorCallback_3; }
	inline void set__errorCallback_3(Action_1_t56370BF80900566597FF1377284D989943C5D99B * value)
	{
		____errorCallback_3 = value;
		Il2CppCodeGenWriteBarrier((&____errorCallback_3), value);
	}

	inline static int32_t get_offset_of__completer_4() { return static_cast<int32_t>(offsetof(GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7, ____completer_4)); }
	inline Action_1_t56370BF80900566597FF1377284D989943C5D99B * get__completer_4() const { return ____completer_4; }
	inline Action_1_t56370BF80900566597FF1377284D989943C5D99B ** get_address_of__completer_4() { return &____completer_4; }
	inline void set__completer_4(Action_1_t56370BF80900566597FF1377284D989943C5D99B * value)
	{
		____completer_4 = value;
		Il2CppCodeGenWriteBarrier((&____completer_4), value);
	}

	inline static int32_t get_offset_of_Durable_5() { return static_cast<int32_t>(offsetof(GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7, ___Durable_5)); }
	inline bool get_Durable_5() const { return ___Durable_5; }
	inline bool* get_address_of_Durable_5() { return &___Durable_5; }
	inline void set_Durable_5(bool value)
	{
		___Durable_5 = value;
	}

	inline static int32_t get_offset_of_gsInstance_6() { return static_cast<int32_t>(offsetof(GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7, ___gsInstance_6)); }
	inline GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774 * get_gsInstance_6() const { return ___gsInstance_6; }
	inline GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774 ** get_address_of_gsInstance_6() { return &___gsInstance_6; }
	inline void set_gsInstance_6(GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774 * value)
	{
		___gsInstance_6 = value;
		Il2CppCodeGenWriteBarrier((&___gsInstance_6), value);
	}

	inline static int32_t get_offset_of_U3CWaitForResponseTicksU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7, ___U3CWaitForResponseTicksU3Ek__BackingField_7)); }
	inline int64_t get_U3CWaitForResponseTicksU3Ek__BackingField_7() const { return ___U3CWaitForResponseTicksU3Ek__BackingField_7; }
	inline int64_t* get_address_of_U3CWaitForResponseTicksU3Ek__BackingField_7() { return &___U3CWaitForResponseTicksU3Ek__BackingField_7; }
	inline void set_U3CWaitForResponseTicksU3Ek__BackingField_7(int64_t value)
	{
		___U3CWaitForResponseTicksU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CRequestExpiresAtU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7, ___U3CRequestExpiresAtU3Ek__BackingField_8)); }
	inline int64_t get_U3CRequestExpiresAtU3Ek__BackingField_8() const { return ___U3CRequestExpiresAtU3Ek__BackingField_8; }
	inline int64_t* get_address_of_U3CRequestExpiresAtU3Ek__BackingField_8() { return &___U3CRequestExpiresAtU3Ek__BackingField_8; }
	inline void set_U3CRequestExpiresAtU3Ek__BackingField_8(int64_t value)
	{
		___U3CRequestExpiresAtU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CDurableAttemptsU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7, ___U3CDurableAttemptsU3Ek__BackingField_9)); }
	inline int32_t get_U3CDurableAttemptsU3Ek__BackingField_9() const { return ___U3CDurableAttemptsU3Ek__BackingField_9; }
	inline int32_t* get_address_of_U3CDurableAttemptsU3Ek__BackingField_9() { return &___U3CDurableAttemptsU3Ek__BackingField_9; }
	inline void set_U3CDurableAttemptsU3Ek__BackingField_9(int32_t value)
	{
		___U3CDurableAttemptsU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CMaxResponseTimeInMillisU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7, ___U3CMaxResponseTimeInMillisU3Ek__BackingField_10)); }
	inline int32_t get_U3CMaxResponseTimeInMillisU3Ek__BackingField_10() const { return ___U3CMaxResponseTimeInMillisU3Ek__BackingField_10; }
	inline int32_t* get_address_of_U3CMaxResponseTimeInMillisU3Ek__BackingField_10() { return &___U3CMaxResponseTimeInMillisU3Ek__BackingField_10; }
	inline void set_U3CMaxResponseTimeInMillisU3Ek__BackingField_10(int32_t value)
	{
		___U3CMaxResponseTimeInMillisU3Ek__BackingField_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GSREQUEST_T12ABAE67A9A983F16B9D82B7050265C35911B1B7_H
#ifndef BINARYWRITEMEMORYSTREAM_T9503BBB30B8671EB7F1F21C98D1A5B0CBFD6B84A_H
#define BINARYWRITEMEMORYSTREAM_T9503BBB30B8671EB7F1F21C98D1A5B0CBFD6B84A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.RT.BinaryWriteMemoryStream
struct  BinaryWriteMemoryStream_t9503BBB30B8671EB7F1F21C98D1A5B0CBFD6B84A  : public MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C
{
public:
	// System.IO.BinaryReader GameSparks.RT.BinaryWriteMemoryStream::BinaryReader
	BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969 * ___BinaryReader_15;
	// System.IO.BinaryWriter GameSparks.RT.BinaryWriteMemoryStream::BinaryWriter
	BinaryWriter_t1B8E78B6CACFBFB57BF140FB4DECE19AFD249CC3 * ___BinaryWriter_16;

public:
	inline static int32_t get_offset_of_BinaryReader_15() { return static_cast<int32_t>(offsetof(BinaryWriteMemoryStream_t9503BBB30B8671EB7F1F21C98D1A5B0CBFD6B84A, ___BinaryReader_15)); }
	inline BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969 * get_BinaryReader_15() const { return ___BinaryReader_15; }
	inline BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969 ** get_address_of_BinaryReader_15() { return &___BinaryReader_15; }
	inline void set_BinaryReader_15(BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969 * value)
	{
		___BinaryReader_15 = value;
		Il2CppCodeGenWriteBarrier((&___BinaryReader_15), value);
	}

	inline static int32_t get_offset_of_BinaryWriter_16() { return static_cast<int32_t>(offsetof(BinaryWriteMemoryStream_t9503BBB30B8671EB7F1F21C98D1A5B0CBFD6B84A, ___BinaryWriter_16)); }
	inline BinaryWriter_t1B8E78B6CACFBFB57BF140FB4DECE19AFD249CC3 * get_BinaryWriter_16() const { return ___BinaryWriter_16; }
	inline BinaryWriter_t1B8E78B6CACFBFB57BF140FB4DECE19AFD249CC3 ** get_address_of_BinaryWriter_16() { return &___BinaryWriter_16; }
	inline void set_BinaryWriter_16(BinaryWriter_t1B8E78B6CACFBFB57BF140FB4DECE19AFD249CC3 * value)
	{
		___BinaryWriter_16 = value;
		Il2CppCodeGenWriteBarrier((&___BinaryWriter_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINARYWRITEMEMORYSTREAM_T9503BBB30B8671EB7F1F21C98D1A5B0CBFD6B84A_H
#ifndef RTREQUEST_TAB65BDCB5A0862353CF86D457AA4901F2E32DCC1_H
#define RTREQUEST_TAB65BDCB5A0862353CF86D457AA4901F2E32DCC1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.RT.Commands.RTRequest
struct  RTRequest_tAB65BDCB5A0862353CF86D457AA4901F2E32DCC1  : public RuntimeObject
{
public:
	// GameSparks.RT.RTData GameSparks.RT.Commands.RTRequest::<Data>k__BackingField
	RTData_tAAEBF5D36A61DACA40791F6B7E2D59B7FAF099B5 * ___U3CDataU3Ek__BackingField_0;
	// System.Int32 GameSparks.RT.Commands.RTRequest::<OpCode>k__BackingField
	int32_t ___U3COpCodeU3Ek__BackingField_1;
	// System.Collections.Generic.List`1<System.Int32> GameSparks.RT.Commands.RTRequest::<TargetPlayers>k__BackingField
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ___U3CTargetPlayersU3Ek__BackingField_2;
	// GameSparks.RT.GameSparksRT_DeliveryIntent GameSparks.RT.Commands.RTRequest::<intent>k__BackingField
	int32_t ___U3CintentU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RTRequest_tAB65BDCB5A0862353CF86D457AA4901F2E32DCC1, ___U3CDataU3Ek__BackingField_0)); }
	inline RTData_tAAEBF5D36A61DACA40791F6B7E2D59B7FAF099B5 * get_U3CDataU3Ek__BackingField_0() const { return ___U3CDataU3Ek__BackingField_0; }
	inline RTData_tAAEBF5D36A61DACA40791F6B7E2D59B7FAF099B5 ** get_address_of_U3CDataU3Ek__BackingField_0() { return &___U3CDataU3Ek__BackingField_0; }
	inline void set_U3CDataU3Ek__BackingField_0(RTData_tAAEBF5D36A61DACA40791F6B7E2D59B7FAF099B5 * value)
	{
		___U3CDataU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3COpCodeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RTRequest_tAB65BDCB5A0862353CF86D457AA4901F2E32DCC1, ___U3COpCodeU3Ek__BackingField_1)); }
	inline int32_t get_U3COpCodeU3Ek__BackingField_1() const { return ___U3COpCodeU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3COpCodeU3Ek__BackingField_1() { return &___U3COpCodeU3Ek__BackingField_1; }
	inline void set_U3COpCodeU3Ek__BackingField_1(int32_t value)
	{
		___U3COpCodeU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CTargetPlayersU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(RTRequest_tAB65BDCB5A0862353CF86D457AA4901F2E32DCC1, ___U3CTargetPlayersU3Ek__BackingField_2)); }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * get_U3CTargetPlayersU3Ek__BackingField_2() const { return ___U3CTargetPlayersU3Ek__BackingField_2; }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 ** get_address_of_U3CTargetPlayersU3Ek__BackingField_2() { return &___U3CTargetPlayersU3Ek__BackingField_2; }
	inline void set_U3CTargetPlayersU3Ek__BackingField_2(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * value)
	{
		___U3CTargetPlayersU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTargetPlayersU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CintentU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(RTRequest_tAB65BDCB5A0862353CF86D457AA4901F2E32DCC1, ___U3CintentU3Ek__BackingField_3)); }
	inline int32_t get_U3CintentU3Ek__BackingField_3() const { return ___U3CintentU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CintentU3Ek__BackingField_3() { return &___U3CintentU3Ek__BackingField_3; }
	inline void set_U3CintentU3Ek__BackingField_3(int32_t value)
	{
		___U3CintentU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RTREQUEST_TAB65BDCB5A0862353CF86D457AA4901F2E32DCC1_H
#ifndef GSTLSCLIENT_TE067E7168EBE292131479D0AA6C0B50D3077D750_H
#define GSTLSCLIENT_TE067E7168EBE292131479D0AA6C0B50D3077D750_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.RT.GSTlsClient
struct  GSTlsClient_tE067E7168EBE292131479D0AA6C0B50D3077D750  : public DefaultTlsClient_t3AEEC6138E8000BF0EB194E242413CB61B9EFF0E
{
public:
	// System.String GameSparks.RT.GSTlsClient::hostName
	String_t* ___hostName_8;

public:
	inline static int32_t get_offset_of_hostName_8() { return static_cast<int32_t>(offsetof(GSTlsClient_tE067E7168EBE292131479D0AA6C0B50D3077D750, ___hostName_8)); }
	inline String_t* get_hostName_8() const { return ___hostName_8; }
	inline String_t** get_address_of_hostName_8() { return &___hostName_8; }
	inline void set_hostName_8(String_t* value)
	{
		___hostName_8 = value;
		Il2CppCodeGenWriteBarrier((&___hostName_8), value);
	}
};

struct GSTlsClient_tE067E7168EBE292131479D0AA6C0B50D3077D750_StaticFields
{
public:
	// System.Action`1<System.String> GameSparks.RT.GSTlsClient::logger
	Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * ___logger_9;

public:
	inline static int32_t get_offset_of_logger_9() { return static_cast<int32_t>(offsetof(GSTlsClient_tE067E7168EBE292131479D0AA6C0B50D3077D750_StaticFields, ___logger_9)); }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * get_logger_9() const { return ___logger_9; }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 ** get_address_of_logger_9() { return &___logger_9; }
	inline void set_logger_9(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * value)
	{
		___logger_9 = value;
		Il2CppCodeGenWriteBarrier((&___logger_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GSTLSCLIENT_TE067E7168EBE292131479D0AA6C0B50D3077D750_H
#ifndef GAMESPARKSRT_T67B264905B4578BF48D58FF7A623F647F136F3A1_H
#define GAMESPARKSRT_T67B264905B4578BF48D58FF7A623F647F136F3A1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.RT.GameSparksRT
struct  GameSparksRT_t67B264905B4578BF48D58FF7A623F647F136F3A1  : public RuntimeObject
{
public:

public:
};

struct GameSparksRT_t67B264905B4578BF48D58FF7A623F647F136F3A1_StaticFields
{
public:
	// GameSparks.RT.GameSparksRT_LogLevel GameSparks.RT.GameSparksRT::logLevel
	int32_t ___logLevel_0;
	// System.Collections.Generic.IDictionary`2<System.String,GameSparks.RT.GameSparksRT_LogLevel> GameSparks.RT.GameSparksRT::tagLevels
	RuntimeObject* ___tagLevels_1;
	// System.Random GameSparks.RT.GameSparksRT::random
	Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F * ___random_2;
	// System.Action`1<System.String> GameSparks.RT.GameSparksRT::<Logger>k__BackingField
	Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * ___U3CLoggerU3Ek__BackingField_3;
	// System.Action`1<System.String> GameSparks.RT.GameSparksRT::CSU24<>9__CachedAnonymousMethodDelegate1
	Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * ___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_4;

public:
	inline static int32_t get_offset_of_logLevel_0() { return static_cast<int32_t>(offsetof(GameSparksRT_t67B264905B4578BF48D58FF7A623F647F136F3A1_StaticFields, ___logLevel_0)); }
	inline int32_t get_logLevel_0() const { return ___logLevel_0; }
	inline int32_t* get_address_of_logLevel_0() { return &___logLevel_0; }
	inline void set_logLevel_0(int32_t value)
	{
		___logLevel_0 = value;
	}

	inline static int32_t get_offset_of_tagLevels_1() { return static_cast<int32_t>(offsetof(GameSparksRT_t67B264905B4578BF48D58FF7A623F647F136F3A1_StaticFields, ___tagLevels_1)); }
	inline RuntimeObject* get_tagLevels_1() const { return ___tagLevels_1; }
	inline RuntimeObject** get_address_of_tagLevels_1() { return &___tagLevels_1; }
	inline void set_tagLevels_1(RuntimeObject* value)
	{
		___tagLevels_1 = value;
		Il2CppCodeGenWriteBarrier((&___tagLevels_1), value);
	}

	inline static int32_t get_offset_of_random_2() { return static_cast<int32_t>(offsetof(GameSparksRT_t67B264905B4578BF48D58FF7A623F647F136F3A1_StaticFields, ___random_2)); }
	inline Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F * get_random_2() const { return ___random_2; }
	inline Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F ** get_address_of_random_2() { return &___random_2; }
	inline void set_random_2(Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F * value)
	{
		___random_2 = value;
		Il2CppCodeGenWriteBarrier((&___random_2), value);
	}

	inline static int32_t get_offset_of_U3CLoggerU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(GameSparksRT_t67B264905B4578BF48D58FF7A623F647F136F3A1_StaticFields, ___U3CLoggerU3Ek__BackingField_3)); }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * get_U3CLoggerU3Ek__BackingField_3() const { return ___U3CLoggerU3Ek__BackingField_3; }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 ** get_address_of_U3CLoggerU3Ek__BackingField_3() { return &___U3CLoggerU3Ek__BackingField_3; }
	inline void set_U3CLoggerU3Ek__BackingField_3(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * value)
	{
		___U3CLoggerU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLoggerU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_4() { return static_cast<int32_t>(offsetof(GameSparksRT_t67B264905B4578BF48D58FF7A623F647F136F3A1_StaticFields, ___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_4)); }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * get_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_4() const { return ___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_4; }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 ** get_address_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_4() { return &___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_4; }
	inline void set_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_4(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * value)
	{
		___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_4 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMESPARKSRT_T67B264905B4578BF48D58FF7A623F647F136F3A1_H
#ifndef LOGCOMMAND_T7558700959C71C5086C24D058AE7AFE8B0E15DE8_H
#define LOGCOMMAND_T7558700959C71C5086C24D058AE7AFE8B0E15DE8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.RT.LogCommand
struct  LogCommand_t7558700959C71C5086C24D058AE7AFE8B0E15DE8  : public RuntimeObject
{
public:
	// System.String GameSparks.RT.LogCommand::tag
	String_t* ___tag_1;
	// System.String GameSparks.RT.LogCommand::msg
	String_t* ___msg_2;
	// GameSparks.RT.GameSparksRT_LogLevel GameSparks.RT.LogCommand::level
	int32_t ___level_3;
	// System.Object[] GameSparks.RT.LogCommand::formatParams
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___formatParams_4;
	// GameSparks.RT.IRTSession GameSparks.RT.LogCommand::session
	RuntimeObject* ___session_5;

public:
	inline static int32_t get_offset_of_tag_1() { return static_cast<int32_t>(offsetof(LogCommand_t7558700959C71C5086C24D058AE7AFE8B0E15DE8, ___tag_1)); }
	inline String_t* get_tag_1() const { return ___tag_1; }
	inline String_t** get_address_of_tag_1() { return &___tag_1; }
	inline void set_tag_1(String_t* value)
	{
		___tag_1 = value;
		Il2CppCodeGenWriteBarrier((&___tag_1), value);
	}

	inline static int32_t get_offset_of_msg_2() { return static_cast<int32_t>(offsetof(LogCommand_t7558700959C71C5086C24D058AE7AFE8B0E15DE8, ___msg_2)); }
	inline String_t* get_msg_2() const { return ___msg_2; }
	inline String_t** get_address_of_msg_2() { return &___msg_2; }
	inline void set_msg_2(String_t* value)
	{
		___msg_2 = value;
		Il2CppCodeGenWriteBarrier((&___msg_2), value);
	}

	inline static int32_t get_offset_of_level_3() { return static_cast<int32_t>(offsetof(LogCommand_t7558700959C71C5086C24D058AE7AFE8B0E15DE8, ___level_3)); }
	inline int32_t get_level_3() const { return ___level_3; }
	inline int32_t* get_address_of_level_3() { return &___level_3; }
	inline void set_level_3(int32_t value)
	{
		___level_3 = value;
	}

	inline static int32_t get_offset_of_formatParams_4() { return static_cast<int32_t>(offsetof(LogCommand_t7558700959C71C5086C24D058AE7AFE8B0E15DE8, ___formatParams_4)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_formatParams_4() const { return ___formatParams_4; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_formatParams_4() { return &___formatParams_4; }
	inline void set_formatParams_4(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___formatParams_4 = value;
		Il2CppCodeGenWriteBarrier((&___formatParams_4), value);
	}

	inline static int32_t get_offset_of_session_5() { return static_cast<int32_t>(offsetof(LogCommand_t7558700959C71C5086C24D058AE7AFE8B0E15DE8, ___session_5)); }
	inline RuntimeObject* get_session_5() const { return ___session_5; }
	inline RuntimeObject** get_address_of_session_5() { return &___session_5; }
	inline void set_session_5(RuntimeObject* value)
	{
		___session_5 = value;
		Il2CppCodeGenWriteBarrier((&___session_5), value);
	}
};

struct LogCommand_t7558700959C71C5086C24D058AE7AFE8B0E15DE8_StaticFields
{
public:
	// GameSparks.RT.Pools.ObjectPool`1<GameSparks.RT.LogCommand> GameSparks.RT.LogCommand::pool
	ObjectPool_1_tC8B4FEB0616D82A9717C7BF763BC1F503354C101 * ___pool_0;
	// System.Func`1<GameSparks.RT.LogCommand> GameSparks.RT.LogCommand::CSU24<>9__CachedAnonymousMethodDelegate1
	Func_1_t37ED4AEA3737F590CC17C3E757B5281A1CAEE93A * ___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_6;

public:
	inline static int32_t get_offset_of_pool_0() { return static_cast<int32_t>(offsetof(LogCommand_t7558700959C71C5086C24D058AE7AFE8B0E15DE8_StaticFields, ___pool_0)); }
	inline ObjectPool_1_tC8B4FEB0616D82A9717C7BF763BC1F503354C101 * get_pool_0() const { return ___pool_0; }
	inline ObjectPool_1_tC8B4FEB0616D82A9717C7BF763BC1F503354C101 ** get_address_of_pool_0() { return &___pool_0; }
	inline void set_pool_0(ObjectPool_1_tC8B4FEB0616D82A9717C7BF763BC1F503354C101 * value)
	{
		___pool_0 = value;
		Il2CppCodeGenWriteBarrier((&___pool_0), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_6() { return static_cast<int32_t>(offsetof(LogCommand_t7558700959C71C5086C24D058AE7AFE8B0E15DE8_StaticFields, ___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_6)); }
	inline Func_1_t37ED4AEA3737F590CC17C3E757B5281A1CAEE93A * get_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_6() const { return ___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_6; }
	inline Func_1_t37ED4AEA3737F590CC17C3E757B5281A1CAEE93A ** get_address_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_6() { return &___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_6; }
	inline void set_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_6(Func_1_t37ED4AEA3737F590CC17C3E757B5281A1CAEE93A * value)
	{
		___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_6 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGCOMMAND_T7558700959C71C5086C24D058AE7AFE8B0E15DE8_H
#ifndef KEY_T81D05210B6137A274B92DCDE6C756DB9134BEF44_H
#define KEY_T81D05210B6137A274B92DCDE6C756DB9134BEF44_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.RT.Proto.Key
struct  Key_t81D05210B6137A274B92DCDE6C756DB9134BEF44 
{
public:
	// System.UInt32 GameSparks.RT.Proto.Key::<Field>k__BackingField
	uint32_t ___U3CFieldU3Ek__BackingField_0;
	// GameSparks.RT.Proto.Wire GameSparks.RT.Proto.Key::<WireType>k__BackingField
	int32_t ___U3CWireTypeU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CFieldU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Key_t81D05210B6137A274B92DCDE6C756DB9134BEF44, ___U3CFieldU3Ek__BackingField_0)); }
	inline uint32_t get_U3CFieldU3Ek__BackingField_0() const { return ___U3CFieldU3Ek__BackingField_0; }
	inline uint32_t* get_address_of_U3CFieldU3Ek__BackingField_0() { return &___U3CFieldU3Ek__BackingField_0; }
	inline void set_U3CFieldU3Ek__BackingField_0(uint32_t value)
	{
		___U3CFieldU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CWireTypeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Key_t81D05210B6137A274B92DCDE6C756DB9134BEF44, ___U3CWireTypeU3Ek__BackingField_1)); }
	inline int32_t get_U3CWireTypeU3Ek__BackingField_1() const { return ___U3CWireTypeU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CWireTypeU3Ek__BackingField_1() { return &___U3CWireTypeU3Ek__BackingField_1; }
	inline void set_U3CWireTypeU3Ek__BackingField_1(int32_t value)
	{
		___U3CWireTypeU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEY_T81D05210B6137A274B92DCDE6C756DB9134BEF44_H
#ifndef LIMITEDPOSITIONSTREAM_T39D3BF1EF4DFE19E62D02F20AA326007FD4A2853_H
#define LIMITEDPOSITIONSTREAM_T39D3BF1EF4DFE19E62D02F20AA326007FD4A2853_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.RT.Proto.LimitedPositionStream
struct  LimitedPositionStream_t39D3BF1EF4DFE19E62D02F20AA326007FD4A2853  : public PositionStream_tF8B0CE7A2A52EE9A986DECBA4F07F5BFA773228D
{
public:
	// System.Int64 GameSparks.RT.Proto.LimitedPositionStream::limit
	int64_t ___limit_9;

public:
	inline static int32_t get_offset_of_limit_9() { return static_cast<int32_t>(offsetof(LimitedPositionStream_t39D3BF1EF4DFE19E62D02F20AA326007FD4A2853, ___limit_9)); }
	inline int64_t get_limit_9() const { return ___limit_9; }
	inline int64_t* get_address_of_limit_9() { return &___limit_9; }
	inline void set_limit_9(int64_t value)
	{
		___limit_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIMITEDPOSITIONSTREAM_T39D3BF1EF4DFE19E62D02F20AA326007FD4A2853_H
#ifndef RTSESSIONIMPL_T4277EBD12AF635CB4E621DF4B4C4731EF1A452D9_H
#define RTSESSIONIMPL_T4277EBD12AF635CB4E621DF4B4C4731EF1A452D9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.RT.RTSessionImpl
struct  RTSessionImpl_t4277EBD12AF635CB4E621DF4B4C4731EF1A452D9  : public RuntimeObject
{
public:
	// GameSparks.RT.Connection.ReliableConnection GameSparks.RT.RTSessionImpl::reliableConnection
	ReliableConnection_t237BB0704E0E315A9E262CD04956628C1D2435B0 * ___reliableConnection_0;
	// GameSparks.RT.Connection.ReliableWSConnection GameSparks.RT.RTSessionImpl::reliableWSConnection
	ReliableWSConnection_tBE9804C4155A51FB7107363A89A38F9C68847554 * ___reliableWSConnection_1;
	// GameSparks.RT.Connection.FastConnection GameSparks.RT.RTSessionImpl::fastConnection
	FastConnection_t9E83C1C0DD332A4579DA0512357037F285CA25A0 * ___fastConnection_2;
	// System.String GameSparks.RT.RTSessionImpl::hostName
	String_t* ___hostName_3;
	// System.Int32 GameSparks.RT.RTSessionImpl::TcpPort
	int32_t ___TcpPort_4;
	// System.Boolean GameSparks.RT.RTSessionImpl::running
	bool ___running_5;
	// System.Boolean GameSparks.RT.RTSessionImpl::useOnlyWebSockets
	bool ___useOnlyWebSockets_6;
	// System.Int32 GameSparks.RT.RTSessionImpl::connectionAttempts
	int32_t ___connectionAttempts_7;
	// System.DateTime GameSparks.RT.RTSessionImpl::mustConnectBy
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___mustConnectBy_8;
	// GameSparks.RT.GameSparksRT_ConnectState modreq(System.Runtime.CompilerServices.IsVolatile) GameSparks.RT.RTSessionImpl::internalState
	int32_t ___internalState_9;
	// System.Collections.Generic.Queue`1<GameSparks.RT.IRTCommand> GameSparks.RT.RTSessionImpl::actionQueue
	Queue_1_tE5149A28B77E4439EB38C948F8E6D0B1E40B6077 * ___actionQueue_10;
	// System.Collections.Generic.IDictionary`2<System.Int32,System.Int32> GameSparks.RT.RTSessionImpl::peerMaxSequenceNumbers
	RuntimeObject* ___peerMaxSequenceNumbers_11;
	// System.Int32 GameSparks.RT.RTSessionImpl::sequenceNumber
	int32_t ___sequenceNumber_12;
	// System.Int32 GameSparks.RT.RTSessionImpl::<FastPort>k__BackingField
	int32_t ___U3CFastPortU3Ek__BackingField_13;
	// System.String GameSparks.RT.RTSessionImpl::<ConnectToken>k__BackingField
	String_t* ___U3CConnectTokenU3Ek__BackingField_14;
	// System.Boolean GameSparks.RT.RTSessionImpl::<Ready>k__BackingField
	bool ___U3CReadyU3Ek__BackingField_15;
	// System.Nullable`1<System.Int32> GameSparks.RT.RTSessionImpl::<PeerId>k__BackingField
	Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  ___U3CPeerIdU3Ek__BackingField_16;
	// System.Collections.Generic.List`1<System.Int32> GameSparks.RT.RTSessionImpl::<ActivePeers>k__BackingField
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ___U3CActivePeersU3Ek__BackingField_17;
	// GameSparks.RT.IRTSessionListener GameSparks.RT.RTSessionImpl::<SessionListener>k__BackingField
	RuntimeObject* ___U3CSessionListenerU3Ek__BackingField_18;

public:
	inline static int32_t get_offset_of_reliableConnection_0() { return static_cast<int32_t>(offsetof(RTSessionImpl_t4277EBD12AF635CB4E621DF4B4C4731EF1A452D9, ___reliableConnection_0)); }
	inline ReliableConnection_t237BB0704E0E315A9E262CD04956628C1D2435B0 * get_reliableConnection_0() const { return ___reliableConnection_0; }
	inline ReliableConnection_t237BB0704E0E315A9E262CD04956628C1D2435B0 ** get_address_of_reliableConnection_0() { return &___reliableConnection_0; }
	inline void set_reliableConnection_0(ReliableConnection_t237BB0704E0E315A9E262CD04956628C1D2435B0 * value)
	{
		___reliableConnection_0 = value;
		Il2CppCodeGenWriteBarrier((&___reliableConnection_0), value);
	}

	inline static int32_t get_offset_of_reliableWSConnection_1() { return static_cast<int32_t>(offsetof(RTSessionImpl_t4277EBD12AF635CB4E621DF4B4C4731EF1A452D9, ___reliableWSConnection_1)); }
	inline ReliableWSConnection_tBE9804C4155A51FB7107363A89A38F9C68847554 * get_reliableWSConnection_1() const { return ___reliableWSConnection_1; }
	inline ReliableWSConnection_tBE9804C4155A51FB7107363A89A38F9C68847554 ** get_address_of_reliableWSConnection_1() { return &___reliableWSConnection_1; }
	inline void set_reliableWSConnection_1(ReliableWSConnection_tBE9804C4155A51FB7107363A89A38F9C68847554 * value)
	{
		___reliableWSConnection_1 = value;
		Il2CppCodeGenWriteBarrier((&___reliableWSConnection_1), value);
	}

	inline static int32_t get_offset_of_fastConnection_2() { return static_cast<int32_t>(offsetof(RTSessionImpl_t4277EBD12AF635CB4E621DF4B4C4731EF1A452D9, ___fastConnection_2)); }
	inline FastConnection_t9E83C1C0DD332A4579DA0512357037F285CA25A0 * get_fastConnection_2() const { return ___fastConnection_2; }
	inline FastConnection_t9E83C1C0DD332A4579DA0512357037F285CA25A0 ** get_address_of_fastConnection_2() { return &___fastConnection_2; }
	inline void set_fastConnection_2(FastConnection_t9E83C1C0DD332A4579DA0512357037F285CA25A0 * value)
	{
		___fastConnection_2 = value;
		Il2CppCodeGenWriteBarrier((&___fastConnection_2), value);
	}

	inline static int32_t get_offset_of_hostName_3() { return static_cast<int32_t>(offsetof(RTSessionImpl_t4277EBD12AF635CB4E621DF4B4C4731EF1A452D9, ___hostName_3)); }
	inline String_t* get_hostName_3() const { return ___hostName_3; }
	inline String_t** get_address_of_hostName_3() { return &___hostName_3; }
	inline void set_hostName_3(String_t* value)
	{
		___hostName_3 = value;
		Il2CppCodeGenWriteBarrier((&___hostName_3), value);
	}

	inline static int32_t get_offset_of_TcpPort_4() { return static_cast<int32_t>(offsetof(RTSessionImpl_t4277EBD12AF635CB4E621DF4B4C4731EF1A452D9, ___TcpPort_4)); }
	inline int32_t get_TcpPort_4() const { return ___TcpPort_4; }
	inline int32_t* get_address_of_TcpPort_4() { return &___TcpPort_4; }
	inline void set_TcpPort_4(int32_t value)
	{
		___TcpPort_4 = value;
	}

	inline static int32_t get_offset_of_running_5() { return static_cast<int32_t>(offsetof(RTSessionImpl_t4277EBD12AF635CB4E621DF4B4C4731EF1A452D9, ___running_5)); }
	inline bool get_running_5() const { return ___running_5; }
	inline bool* get_address_of_running_5() { return &___running_5; }
	inline void set_running_5(bool value)
	{
		___running_5 = value;
	}

	inline static int32_t get_offset_of_useOnlyWebSockets_6() { return static_cast<int32_t>(offsetof(RTSessionImpl_t4277EBD12AF635CB4E621DF4B4C4731EF1A452D9, ___useOnlyWebSockets_6)); }
	inline bool get_useOnlyWebSockets_6() const { return ___useOnlyWebSockets_6; }
	inline bool* get_address_of_useOnlyWebSockets_6() { return &___useOnlyWebSockets_6; }
	inline void set_useOnlyWebSockets_6(bool value)
	{
		___useOnlyWebSockets_6 = value;
	}

	inline static int32_t get_offset_of_connectionAttempts_7() { return static_cast<int32_t>(offsetof(RTSessionImpl_t4277EBD12AF635CB4E621DF4B4C4731EF1A452D9, ___connectionAttempts_7)); }
	inline int32_t get_connectionAttempts_7() const { return ___connectionAttempts_7; }
	inline int32_t* get_address_of_connectionAttempts_7() { return &___connectionAttempts_7; }
	inline void set_connectionAttempts_7(int32_t value)
	{
		___connectionAttempts_7 = value;
	}

	inline static int32_t get_offset_of_mustConnectBy_8() { return static_cast<int32_t>(offsetof(RTSessionImpl_t4277EBD12AF635CB4E621DF4B4C4731EF1A452D9, ___mustConnectBy_8)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_mustConnectBy_8() const { return ___mustConnectBy_8; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_mustConnectBy_8() { return &___mustConnectBy_8; }
	inline void set_mustConnectBy_8(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___mustConnectBy_8 = value;
	}

	inline static int32_t get_offset_of_internalState_9() { return static_cast<int32_t>(offsetof(RTSessionImpl_t4277EBD12AF635CB4E621DF4B4C4731EF1A452D9, ___internalState_9)); }
	inline int32_t get_internalState_9() const { return ___internalState_9; }
	inline int32_t* get_address_of_internalState_9() { return &___internalState_9; }
	inline void set_internalState_9(int32_t value)
	{
		___internalState_9 = value;
	}

	inline static int32_t get_offset_of_actionQueue_10() { return static_cast<int32_t>(offsetof(RTSessionImpl_t4277EBD12AF635CB4E621DF4B4C4731EF1A452D9, ___actionQueue_10)); }
	inline Queue_1_tE5149A28B77E4439EB38C948F8E6D0B1E40B6077 * get_actionQueue_10() const { return ___actionQueue_10; }
	inline Queue_1_tE5149A28B77E4439EB38C948F8E6D0B1E40B6077 ** get_address_of_actionQueue_10() { return &___actionQueue_10; }
	inline void set_actionQueue_10(Queue_1_tE5149A28B77E4439EB38C948F8E6D0B1E40B6077 * value)
	{
		___actionQueue_10 = value;
		Il2CppCodeGenWriteBarrier((&___actionQueue_10), value);
	}

	inline static int32_t get_offset_of_peerMaxSequenceNumbers_11() { return static_cast<int32_t>(offsetof(RTSessionImpl_t4277EBD12AF635CB4E621DF4B4C4731EF1A452D9, ___peerMaxSequenceNumbers_11)); }
	inline RuntimeObject* get_peerMaxSequenceNumbers_11() const { return ___peerMaxSequenceNumbers_11; }
	inline RuntimeObject** get_address_of_peerMaxSequenceNumbers_11() { return &___peerMaxSequenceNumbers_11; }
	inline void set_peerMaxSequenceNumbers_11(RuntimeObject* value)
	{
		___peerMaxSequenceNumbers_11 = value;
		Il2CppCodeGenWriteBarrier((&___peerMaxSequenceNumbers_11), value);
	}

	inline static int32_t get_offset_of_sequenceNumber_12() { return static_cast<int32_t>(offsetof(RTSessionImpl_t4277EBD12AF635CB4E621DF4B4C4731EF1A452D9, ___sequenceNumber_12)); }
	inline int32_t get_sequenceNumber_12() const { return ___sequenceNumber_12; }
	inline int32_t* get_address_of_sequenceNumber_12() { return &___sequenceNumber_12; }
	inline void set_sequenceNumber_12(int32_t value)
	{
		___sequenceNumber_12 = value;
	}

	inline static int32_t get_offset_of_U3CFastPortU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(RTSessionImpl_t4277EBD12AF635CB4E621DF4B4C4731EF1A452D9, ___U3CFastPortU3Ek__BackingField_13)); }
	inline int32_t get_U3CFastPortU3Ek__BackingField_13() const { return ___U3CFastPortU3Ek__BackingField_13; }
	inline int32_t* get_address_of_U3CFastPortU3Ek__BackingField_13() { return &___U3CFastPortU3Ek__BackingField_13; }
	inline void set_U3CFastPortU3Ek__BackingField_13(int32_t value)
	{
		___U3CFastPortU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3CConnectTokenU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(RTSessionImpl_t4277EBD12AF635CB4E621DF4B4C4731EF1A452D9, ___U3CConnectTokenU3Ek__BackingField_14)); }
	inline String_t* get_U3CConnectTokenU3Ek__BackingField_14() const { return ___U3CConnectTokenU3Ek__BackingField_14; }
	inline String_t** get_address_of_U3CConnectTokenU3Ek__BackingField_14() { return &___U3CConnectTokenU3Ek__BackingField_14; }
	inline void set_U3CConnectTokenU3Ek__BackingField_14(String_t* value)
	{
		___U3CConnectTokenU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CConnectTokenU3Ek__BackingField_14), value);
	}

	inline static int32_t get_offset_of_U3CReadyU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(RTSessionImpl_t4277EBD12AF635CB4E621DF4B4C4731EF1A452D9, ___U3CReadyU3Ek__BackingField_15)); }
	inline bool get_U3CReadyU3Ek__BackingField_15() const { return ___U3CReadyU3Ek__BackingField_15; }
	inline bool* get_address_of_U3CReadyU3Ek__BackingField_15() { return &___U3CReadyU3Ek__BackingField_15; }
	inline void set_U3CReadyU3Ek__BackingField_15(bool value)
	{
		___U3CReadyU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3CPeerIdU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(RTSessionImpl_t4277EBD12AF635CB4E621DF4B4C4731EF1A452D9, ___U3CPeerIdU3Ek__BackingField_16)); }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  get_U3CPeerIdU3Ek__BackingField_16() const { return ___U3CPeerIdU3Ek__BackingField_16; }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB * get_address_of_U3CPeerIdU3Ek__BackingField_16() { return &___U3CPeerIdU3Ek__BackingField_16; }
	inline void set_U3CPeerIdU3Ek__BackingField_16(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  value)
	{
		___U3CPeerIdU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CActivePeersU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(RTSessionImpl_t4277EBD12AF635CB4E621DF4B4C4731EF1A452D9, ___U3CActivePeersU3Ek__BackingField_17)); }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * get_U3CActivePeersU3Ek__BackingField_17() const { return ___U3CActivePeersU3Ek__BackingField_17; }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 ** get_address_of_U3CActivePeersU3Ek__BackingField_17() { return &___U3CActivePeersU3Ek__BackingField_17; }
	inline void set_U3CActivePeersU3Ek__BackingField_17(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * value)
	{
		___U3CActivePeersU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CActivePeersU3Ek__BackingField_17), value);
	}

	inline static int32_t get_offset_of_U3CSessionListenerU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(RTSessionImpl_t4277EBD12AF635CB4E621DF4B4C4731EF1A452D9, ___U3CSessionListenerU3Ek__BackingField_18)); }
	inline RuntimeObject* get_U3CSessionListenerU3Ek__BackingField_18() const { return ___U3CSessionListenerU3Ek__BackingField_18; }
	inline RuntimeObject** get_address_of_U3CSessionListenerU3Ek__BackingField_18() { return &___U3CSessionListenerU3Ek__BackingField_18; }
	inline void set_U3CSessionListenerU3Ek__BackingField_18(RuntimeObject* value)
	{
		___U3CSessionListenerU3Ek__BackingField_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSessionListenerU3Ek__BackingField_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RTSESSIONIMPL_T4277EBD12AF635CB4E621DF4B4C4731EF1A452D9_H
#ifndef NULLABLE_1_T3639FE42D4B9BE59AC9B5C18609E0F8C7BF77D78_H
#define NULLABLE_1_T3639FE42D4B9BE59AC9B5C18609E0F8C7BF77D78_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<GameSparks.RT.RTVector>
struct  Nullable_1_t3639FE42D4B9BE59AC9B5C18609E0F8C7BF77D78 
{
public:
	// T System.Nullable`1::value
	RTVector_tF61BCFCD2BC84E278968EE3D89A39BD51AB09026  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t3639FE42D4B9BE59AC9B5C18609E0F8C7BF77D78, ___value_0)); }
	inline RTVector_tF61BCFCD2BC84E278968EE3D89A39BD51AB09026  get_value_0() const { return ___value_0; }
	inline RTVector_tF61BCFCD2BC84E278968EE3D89A39BD51AB09026 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(RTVector_tF61BCFCD2BC84E278968EE3D89A39BD51AB09026  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t3639FE42D4B9BE59AC9B5C18609E0F8C7BF77D78, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T3639FE42D4B9BE59AC9B5C18609E0F8C7BF77D78_H
#ifndef LOGINCOMMAND_T745C41CD1854E2E34D0F013332BBFE2A9130A307_H
#define LOGINCOMMAND_T745C41CD1854E2E34D0F013332BBFE2A9130A307_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Com.Gamesparks.Realtime.Proto.LoginCommand
struct  LoginCommand_t745C41CD1854E2E34D0F013332BBFE2A9130A307  : public RTRequest_tAB65BDCB5A0862353CF86D457AA4901F2E32DCC1
{
public:
	// System.String Com.Gamesparks.Realtime.Proto.LoginCommand::<Token>k__BackingField
	String_t* ___U3CTokenU3Ek__BackingField_4;
	// System.Nullable`1<System.Int32> Com.Gamesparks.Realtime.Proto.LoginCommand::<ClientVersion>k__BackingField
	Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  ___U3CClientVersionU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CTokenU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(LoginCommand_t745C41CD1854E2E34D0F013332BBFE2A9130A307, ___U3CTokenU3Ek__BackingField_4)); }
	inline String_t* get_U3CTokenU3Ek__BackingField_4() const { return ___U3CTokenU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CTokenU3Ek__BackingField_4() { return &___U3CTokenU3Ek__BackingField_4; }
	inline void set_U3CTokenU3Ek__BackingField_4(String_t* value)
	{
		___U3CTokenU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTokenU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CClientVersionU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(LoginCommand_t745C41CD1854E2E34D0F013332BBFE2A9130A307, ___U3CClientVersionU3Ek__BackingField_5)); }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  get_U3CClientVersionU3Ek__BackingField_5() const { return ___U3CClientVersionU3Ek__BackingField_5; }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB * get_address_of_U3CClientVersionU3Ek__BackingField_5() { return &___U3CClientVersionU3Ek__BackingField_5; }
	inline void set_U3CClientVersionU3Ek__BackingField_5(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  value)
	{
		___U3CClientVersionU3Ek__BackingField_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGINCOMMAND_T745C41CD1854E2E34D0F013332BBFE2A9130A307_H
#ifndef CUSTOMREQUEST_TB8C49CE439702FB57452DB82012DE2C60EDA86F2_H
#define CUSTOMREQUEST_TB8C49CE439702FB57452DB82012DE2C60EDA86F2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.RT.CustomRequest
struct  CustomRequest_tB8C49CE439702FB57452DB82012DE2C60EDA86F2  : public RTRequest_tAB65BDCB5A0862353CF86D457AA4901F2E32DCC1
{
public:
	// System.Nullable`1<System.ArraySegment`1<System.Byte>> GameSparks.RT.CustomRequest::payload
	Nullable_1_t5F0FEFF3704A8BB5D4D681A8F9D3DE452FAF72E1  ___payload_4;

public:
	inline static int32_t get_offset_of_payload_4() { return static_cast<int32_t>(offsetof(CustomRequest_tB8C49CE439702FB57452DB82012DE2C60EDA86F2, ___payload_4)); }
	inline Nullable_1_t5F0FEFF3704A8BB5D4D681A8F9D3DE452FAF72E1  get_payload_4() const { return ___payload_4; }
	inline Nullable_1_t5F0FEFF3704A8BB5D4D681A8F9D3DE452FAF72E1 * get_address_of_payload_4() { return &___payload_4; }
	inline void set_payload_4(Nullable_1_t5F0FEFF3704A8BB5D4D681A8F9D3DE452FAF72E1  value)
	{
		___payload_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMREQUEST_TB8C49CE439702FB57452DB82012DE2C60EDA86F2_H
#ifndef RTVAL_TD2AB0B5959D5AE7F251253034BEF83F306A04D0E_H
#define RTVAL_TD2AB0B5959D5AE7F251253034BEF83F306A04D0E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.RT.Proto.RTVal
struct  RTVal_tD2AB0B5959D5AE7F251253034BEF83F306A04D0E 
{
public:
	// System.Nullable`1<System.Int64> GameSparks.RT.Proto.RTVal::long_val
	Nullable_1_t8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5  ___long_val_0;
	// System.Nullable`1<System.Single> GameSparks.RT.Proto.RTVal::float_val
	Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0  ___float_val_1;
	// System.Nullable`1<System.Double> GameSparks.RT.Proto.RTVal::double_val
	Nullable_1_tBFE8022F4FD60FAE6E29634776F92F0C7DDF0ECF  ___double_val_2;
	// GameSparks.RT.RTData GameSparks.RT.Proto.RTVal::data_val
	RTData_tAAEBF5D36A61DACA40791F6B7E2D59B7FAF099B5 * ___data_val_3;
	// System.String GameSparks.RT.Proto.RTVal::string_val
	String_t* ___string_val_4;
	// System.Nullable`1<GameSparks.RT.RTVector> GameSparks.RT.Proto.RTVal::vec_val
	Nullable_1_t3639FE42D4B9BE59AC9B5C18609E0F8C7BF77D78  ___vec_val_5;

public:
	inline static int32_t get_offset_of_long_val_0() { return static_cast<int32_t>(offsetof(RTVal_tD2AB0B5959D5AE7F251253034BEF83F306A04D0E, ___long_val_0)); }
	inline Nullable_1_t8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5  get_long_val_0() const { return ___long_val_0; }
	inline Nullable_1_t8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5 * get_address_of_long_val_0() { return &___long_val_0; }
	inline void set_long_val_0(Nullable_1_t8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5  value)
	{
		___long_val_0 = value;
	}

	inline static int32_t get_offset_of_float_val_1() { return static_cast<int32_t>(offsetof(RTVal_tD2AB0B5959D5AE7F251253034BEF83F306A04D0E, ___float_val_1)); }
	inline Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0  get_float_val_1() const { return ___float_val_1; }
	inline Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0 * get_address_of_float_val_1() { return &___float_val_1; }
	inline void set_float_val_1(Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0  value)
	{
		___float_val_1 = value;
	}

	inline static int32_t get_offset_of_double_val_2() { return static_cast<int32_t>(offsetof(RTVal_tD2AB0B5959D5AE7F251253034BEF83F306A04D0E, ___double_val_2)); }
	inline Nullable_1_tBFE8022F4FD60FAE6E29634776F92F0C7DDF0ECF  get_double_val_2() const { return ___double_val_2; }
	inline Nullable_1_tBFE8022F4FD60FAE6E29634776F92F0C7DDF0ECF * get_address_of_double_val_2() { return &___double_val_2; }
	inline void set_double_val_2(Nullable_1_tBFE8022F4FD60FAE6E29634776F92F0C7DDF0ECF  value)
	{
		___double_val_2 = value;
	}

	inline static int32_t get_offset_of_data_val_3() { return static_cast<int32_t>(offsetof(RTVal_tD2AB0B5959D5AE7F251253034BEF83F306A04D0E, ___data_val_3)); }
	inline RTData_tAAEBF5D36A61DACA40791F6B7E2D59B7FAF099B5 * get_data_val_3() const { return ___data_val_3; }
	inline RTData_tAAEBF5D36A61DACA40791F6B7E2D59B7FAF099B5 ** get_address_of_data_val_3() { return &___data_val_3; }
	inline void set_data_val_3(RTData_tAAEBF5D36A61DACA40791F6B7E2D59B7FAF099B5 * value)
	{
		___data_val_3 = value;
		Il2CppCodeGenWriteBarrier((&___data_val_3), value);
	}

	inline static int32_t get_offset_of_string_val_4() { return static_cast<int32_t>(offsetof(RTVal_tD2AB0B5959D5AE7F251253034BEF83F306A04D0E, ___string_val_4)); }
	inline String_t* get_string_val_4() const { return ___string_val_4; }
	inline String_t** get_address_of_string_val_4() { return &___string_val_4; }
	inline void set_string_val_4(String_t* value)
	{
		___string_val_4 = value;
		Il2CppCodeGenWriteBarrier((&___string_val_4), value);
	}

	inline static int32_t get_offset_of_vec_val_5() { return static_cast<int32_t>(offsetof(RTVal_tD2AB0B5959D5AE7F251253034BEF83F306A04D0E, ___vec_val_5)); }
	inline Nullable_1_t3639FE42D4B9BE59AC9B5C18609E0F8C7BF77D78  get_vec_val_5() const { return ___vec_val_5; }
	inline Nullable_1_t3639FE42D4B9BE59AC9B5C18609E0F8C7BF77D78 * get_address_of_vec_val_5() { return &___vec_val_5; }
	inline void set_vec_val_5(Nullable_1_t3639FE42D4B9BE59AC9B5C18609E0F8C7BF77D78  value)
	{
		___vec_val_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of GameSparks.RT.Proto.RTVal
struct RTVal_tD2AB0B5959D5AE7F251253034BEF83F306A04D0E_marshaled_pinvoke
{
	Nullable_1_t8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5  ___long_val_0;
	Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0  ___float_val_1;
	Nullable_1_tBFE8022F4FD60FAE6E29634776F92F0C7DDF0ECF  ___double_val_2;
	RTData_tAAEBF5D36A61DACA40791F6B7E2D59B7FAF099B5 * ___data_val_3;
	char* ___string_val_4;
	Nullable_1_t3639FE42D4B9BE59AC9B5C18609E0F8C7BF77D78  ___vec_val_5;
};
// Native definition for COM marshalling of GameSparks.RT.Proto.RTVal
struct RTVal_tD2AB0B5959D5AE7F251253034BEF83F306A04D0E_marshaled_com
{
	Nullable_1_t8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5  ___long_val_0;
	Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0  ___float_val_1;
	Nullable_1_tBFE8022F4FD60FAE6E29634776F92F0C7DDF0ECF  ___double_val_2;
	RTData_tAAEBF5D36A61DACA40791F6B7E2D59B7FAF099B5 * ___data_val_3;
	Il2CppChar* ___string_val_4;
	Nullable_1_t3639FE42D4B9BE59AC9B5C18609E0F8C7BF77D78  ___vec_val_5;
};
#endif // RTVAL_TD2AB0B5959D5AE7F251253034BEF83F306A04D0E_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5900 = { sizeof (GetMyTeamsRequest_tFF4E47D9BE1A521AD6EB4B4B0F58E9926604A4EE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5901 = { sizeof (JoinTeamRequest_tB9F60B85C89BD9F883B390350895C15020905DB6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5902 = { sizeof (LeaderboardDataRequest_t0E5DCA12818A3EEEB53F10A09281802647765037), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5903 = { sizeof (ListAchievementsRequest_tC80CE14B1C3624CE579024E89D0D41F43D177A81), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5904 = { sizeof (ListChallengeTypeRequest_tFA4418905CA4E75B7BA212DE275B765D5C3ADB3B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5905 = { sizeof (ListGameFriendsRequest_t2E9A57E4C6D61AA5A82101615D6E440E5BBA87D3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5906 = { sizeof (ListMessageRequest_t9BF2D3D720DA1D4D87A1E7CF67CCF334A52D3CE9), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5907 = { sizeof (ListTeamChatRequest_tDF5BE8FE552C2F16CFC403FA50BE7C6A99F394C9), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5908 = { sizeof (ListVirtualGoodsRequest_t4C319ABF7081B9DC2E7A64E916182C1F6599C43E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5909 = { sizeof (LogEventRequest_tA73A4996425712D9A6F1145173915FE77C6C7BAB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5910 = { sizeof (SendTeamChatMessageRequest_tC434B49B9BBF6B2165710960210C810C94BFD119), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5911 = { sizeof (SocialLeaderboardDataRequest_t0742C19E9372B583E63F265F2D422841BFF8FFAF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5912 = { sizeof (AccountDetailsResponse_t1601479BB6FED07AC4C422B9488DCD9D92A064E4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5913 = { sizeof (_Location_t2EAC90C5902D9FD46A7DF9602F098EDA593D78DC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5914 = { sizeof (AuthenticationResponse_tB52C005AD2DB9DDC3021B03A35E01367A2A9EFB9), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5915 = { sizeof (ChangeUserDetailsResponse_t7790E1D7F3118AF6CE2E3750AF75A1CCEF579ED2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5916 = { sizeof (CreateTeamResponse_tDEC65B8F3FB8F7D890E5D653FFFE0128A6BA21BA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5917 = { sizeof (DismissMessageResponse_tC68E9EB2A34FE24A4BC53E884651C627921456F5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5918 = { sizeof (DropTeamResponse_t26B2367C7A22F108371C9B92E337399830EE23C5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5919 = { sizeof (EndSessionResponse_tE6BB94FB1025BB5F4A27A1ACF7A94DC77CB93CCB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5920 = { sizeof (FindMatchResponse_tB39FC743E9B82D6FE9BDF049968DE775103A0DF2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5921 = { sizeof (GetDownloadableResponse_t3529C34B7070C4153AA9ADFC5797F5B0385DEADB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5922 = { sizeof (GetMyTeamsResponse_t89103C0D0553AE1745CD4A29AFF261484A03F629), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5923 = { sizeof (JoinTeamResponse_t58422C45325928E10E9719DB632FA119E38CBD11), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5924 = { sizeof (LeaderboardDataResponse_t58F860D186FBA6DA0B2EE7B029B213A2C99F8D01), -1, sizeof(LeaderboardDataResponse_t58F860D186FBA6DA0B2EE7B029B213A2C99F8D01_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5924[1] = 
{
	LeaderboardDataResponse_t58F860D186FBA6DA0B2EE7B029B213A2C99F8D01_StaticFields::get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5925 = { sizeof (_LeaderboardData_t5FFE9A7732DE9DCC6DC0DF3B367B84738FD4A31D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5926 = { sizeof (ListAchievementsResponse_tD7A98A8A65DF8F9E4F254DA2C35D7A8FE27C0A77), -1, sizeof(ListAchievementsResponse_tD7A98A8A65DF8F9E4F254DA2C35D7A8FE27C0A77_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5926[1] = 
{
	ListAchievementsResponse_tD7A98A8A65DF8F9E4F254DA2C35D7A8FE27C0A77_StaticFields::get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5927 = { sizeof (_Achievement_t5A67D51560E727D78FB8978C5B14D302D298A621), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5928 = { sizeof (ListChallengeTypeResponse_tCDDFA2D040622E0C10B656AE02D62FDF7C48684B), -1, sizeof(ListChallengeTypeResponse_tCDDFA2D040622E0C10B656AE02D62FDF7C48684B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5928[1] = 
{
	ListChallengeTypeResponse_tCDDFA2D040622E0C10B656AE02D62FDF7C48684B_StaticFields::get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5929 = { sizeof (_ChallengeType_tBEECFFB505E10736A7F160CC45BD4C0DDEA0948C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5930 = { sizeof (ListGameFriendsResponse_tCF46E5547CDBCE9ED28E1E2F226F6D525880F44A), -1, sizeof(ListGameFriendsResponse_tCF46E5547CDBCE9ED28E1E2F226F6D525880F44A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5930[1] = 
{
	ListGameFriendsResponse_tCF46E5547CDBCE9ED28E1E2F226F6D525880F44A_StaticFields::get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5931 = { sizeof (_Player_tBC635FE551F99944D122C94DEE9E08627307E8C0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5932 = { sizeof (ListMessageResponse_t9862CD65548D08EFFB2BA69D0C3B93635021C05E), -1, sizeof(ListMessageResponse_t9862CD65548D08EFFB2BA69D0C3B93635021C05E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5932[1] = 
{
	ListMessageResponse_t9862CD65548D08EFFB2BA69D0C3B93635021C05E_StaticFields::get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5933 = { sizeof (ListTeamChatResponse_tB86A205B7C490408D298552A6C63AA174E59EAB4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5934 = { sizeof (ListVirtualGoodsResponse_tB0E0F35F7DB05784B9C10DB619FCDCF0F0FE942D), -1, sizeof(ListVirtualGoodsResponse_tB0E0F35F7DB05784B9C10DB619FCDCF0F0FE942D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5934[1] = 
{
	ListVirtualGoodsResponse_tB0E0F35F7DB05784B9C10DB619FCDCF0F0FE942D_StaticFields::get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5935 = { sizeof (_VirtualGood_t4A3F1C7313A837E0E4C40BD8625ADEBD02FC3465), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5936 = { sizeof (LogEventResponse_tBF49C2850F329381D50C71DA2B2FFC22FEAA3FE7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5937 = { sizeof (SendTeamChatMessageResponse_t91BB9AD9970803E40D7C310D2ECF6796704DADA8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5938 = { sizeof (GS_tEA670BED248D00788819C2E9B0FE944F560383AD), -1, sizeof(GS_tEA670BED248D00788819C2E9B0FE944F560383AD_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5938[1] = 
{
	GS_tEA670BED248D00788819C2E9B0FE944F560383AD_StaticFields::get_offset_of__instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5939 = { sizeof (GSConnection_tDCDCFD82EA16BD7E6FEF1CE33FD247F30661E56C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5939[9] = 
{
	GSConnection_tDCDCFD82EA16BD7E6FEF1CE33FD247F30661E56C::get_offset_of__pendingRequests_0(),
	GSConnection_tDCDCFD82EA16BD7E6FEF1CE33FD247F30661E56C::get_offset_of__stopped_1(),
	GSConnection_tDCDCFD82EA16BD7E6FEF1CE33FD247F30661E56C::get_offset_of_url_2(),
	GSConnection_tDCDCFD82EA16BD7E6FEF1CE33FD247F30661E56C::get_offset_of__WebSocketClient_3(),
	GSConnection_tDCDCFD82EA16BD7E6FEF1CE33FD247F30661E56C::get_offset_of__gs_4(),
	GSConnection_tDCDCFD82EA16BD7E6FEF1CE33FD247F30661E56C::get_offset_of__gSPlatform_5(),
	GSConnection_tDCDCFD82EA16BD7E6FEF1CE33FD247F30661E56C::get_offset_of_mustConnectBy_6(),
	GSConnection_tDCDCFD82EA16BD7E6FEF1CE33FD247F30661E56C::get_offset_of__initialised_7(),
	GSConnection_tDCDCFD82EA16BD7E6FEF1CE33FD247F30661E56C::get_offset_of_U3CSessionIdU3Ek__BackingField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5940 = { sizeof (U3CU3Ec__DisplayClass2_t9E1A033FB6AA77062D78CFFFF5E451797715224C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5940[2] = 
{
	U3CU3Ec__DisplayClass2_t9E1A033FB6AA77062D78CFFFF5E451797715224C::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass2_t9E1A033FB6AA77062D78CFFFF5E451797715224C::get_offset_of_errorMessage_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5941 = { sizeof (U3CU3Ec__DisplayClass7_t0E3A1828A4229CA277C0866A600A81766B353B9B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5941[2] = 
{
	U3CU3Ec__DisplayClass7_t0E3A1828A4229CA277C0866A600A81766B353B9B::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass7_t0E3A1828A4229CA277C0866A600A81766B353B9B::get_offset_of_message_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5942 = { sizeof (U3CU3Ec__DisplayClassa_t3DBD47D1B85DBAB0C86707553F3B135711C434AD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5942[2] = 
{
	U3CU3Ec__DisplayClassa_t3DBD47D1B85DBAB0C86707553F3B135711C434AD::get_offset_of_CSU24U3CU3E8__locals8_0(),
	U3CU3Ec__DisplayClassa_t3DBD47D1B85DBAB0C86707553F3B135711C434AD::get_offset_of_json_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5943 = { sizeof (GSJson_t737B8292A68C0137A1F0EEE67C5EFB7632718151), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5944 = { sizeof (Parser_tE75E1835B1CFAC9CB4174AC7D57131286EE6793D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5944[2] = 
{
	Parser_tE75E1835B1CFAC9CB4174AC7D57131286EE6793D::get_offset_of_depth_0(),
	Parser_tE75E1835B1CFAC9CB4174AC7D57131286EE6793D::get_offset_of_json_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5945 = { sizeof (TOKEN_t4CE701F1A10DBCC26C87B634B024F0B8DC00FAFF)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5945[13] = 
{
	TOKEN_t4CE701F1A10DBCC26C87B634B024F0B8DC00FAFF::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5946 = { sizeof (Serializer_tEC0B3F9938321DCC6C37DF0A6F23B67C318D095A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5946[1] = 
{
	Serializer_tEC0B3F9938321DCC6C37DF0A6F23B67C318D095A::get_offset_of_builder_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5947 = { sizeof (GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5947[10] = 
{
	GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7::get_offset_of__response_1(),
	GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7::get_offset_of__callback_2(),
	GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7::get_offset_of__errorCallback_3(),
	GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7::get_offset_of__completer_4(),
	GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7::get_offset_of_Durable_5(),
	GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7::get_offset_of_gsInstance_6(),
	GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7::get_offset_of_U3CWaitForResponseTicksU3Ek__BackingField_7(),
	GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7::get_offset_of_U3CRequestExpiresAtU3Ek__BackingField_8(),
	GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7::get_offset_of_U3CDurableAttemptsU3Ek__BackingField_9(),
	GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7::get_offset_of_U3CMaxResponseTimeInMillisU3Ek__BackingField_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5948 = { sizeof (U3CU3Ec__DisplayClass4_tA8F6C234BF13DB45BB88E9AD09C4E3CBE301EAC1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5948[2] = 
{
	U3CU3Ec__DisplayClass4_tA8F6C234BF13DB45BB88E9AD09C4E3CBE301EAC1::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass4_tA8F6C234BF13DB45BB88E9AD09C4E3CBE301EAC1::get_offset_of_response_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5949 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5950 = { sizeof (GSMessageHandler_t99F7908B45FA124CC5B990D5C1EBC9E61ED6AD47), -1, sizeof(GSMessageHandler_t99F7908B45FA124CC5B990D5C1EBC9E61ED6AD47_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5950[1] = 
{
	GSMessageHandler_t99F7908B45FA124CC5B990D5C1EBC9E61ED6AD47_StaticFields::get_offset_of__AllMessages_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5951 = { sizeof (QueuePersistor_tDF28BC34A9B083A5F2AF6ADA15BA4678741BFBE8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5952 = { sizeof (U3CModuleU3E_t8653A0D7DD0F6B987B0267644F0D933874D889DB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5953 = { sizeof (Packet_tA4653355695B640FDF2DC08872C1BA9D1AB35302), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5953[12] = 
{
	Packet_tA4653355695B640FDF2DC08872C1BA9D1AB35302::get_offset_of_hasPayload_0(),
	Packet_tA4653355695B640FDF2DC08872C1BA9D1AB35302::get_offset_of_U3COpCodeU3Ek__BackingField_1(),
	Packet_tA4653355695B640FDF2DC08872C1BA9D1AB35302::get_offset_of_U3CSequenceNumberU3Ek__BackingField_2(),
	Packet_tA4653355695B640FDF2DC08872C1BA9D1AB35302::get_offset_of_U3CRequestIdU3Ek__BackingField_3(),
	Packet_tA4653355695B640FDF2DC08872C1BA9D1AB35302::get_offset_of_U3CTargetPlayersU3Ek__BackingField_4(),
	Packet_tA4653355695B640FDF2DC08872C1BA9D1AB35302::get_offset_of_U3CSenderU3Ek__BackingField_5(),
	Packet_tA4653355695B640FDF2DC08872C1BA9D1AB35302::get_offset_of_U3CReliableU3Ek__BackingField_6(),
	Packet_tA4653355695B640FDF2DC08872C1BA9D1AB35302::get_offset_of_U3CDataU3Ek__BackingField_7(),
	Packet_tA4653355695B640FDF2DC08872C1BA9D1AB35302::get_offset_of_U3CPayloadU3Ek__BackingField_8(),
	Packet_tA4653355695B640FDF2DC08872C1BA9D1AB35302::get_offset_of_U3CRequestU3Ek__BackingField_9(),
	Packet_tA4653355695B640FDF2DC08872C1BA9D1AB35302::get_offset_of_U3CCommandU3Ek__BackingField_10(),
	Packet_tA4653355695B640FDF2DC08872C1BA9D1AB35302::get_offset_of_U3CSessionU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5954 = { sizeof (Wire_tF49C8E7023E09914F4A8A9495AB171955348B941)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5954[5] = 
{
	Wire_tF49C8E7023E09914F4A8A9495AB171955348B941::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5955 = { sizeof (Key_t81D05210B6137A274B92DCDE6C756DB9134BEF44)+ sizeof (RuntimeObject), sizeof(Key_t81D05210B6137A274B92DCDE6C756DB9134BEF44 ), 0, 0 };
extern const int32_t g_FieldOffsetTable5955[2] = 
{
	Key_t81D05210B6137A274B92DCDE6C756DB9134BEF44::get_offset_of_U3CFieldU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Key_t81D05210B6137A274B92DCDE6C756DB9134BEF44::get_offset_of_U3CWireTypeU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5956 = { sizeof (ProtocolParser_t1ED493ABBA3FEDB52F9F775CAA5A6E66FFDD6116), -1, sizeof(ProtocolParser_t1ED493ABBA3FEDB52F9F775CAA5A6E66FFDD6116_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5956[1] = 
{
	ProtocolParser_t1ED493ABBA3FEDB52F9F775CAA5A6E66FFDD6116_StaticFields::get_offset_of_Stack_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5957 = { sizeof (RTRequest_tAB65BDCB5A0862353CF86D457AA4901F2E32DCC1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5957[4] = 
{
	RTRequest_tAB65BDCB5A0862353CF86D457AA4901F2E32DCC1::get_offset_of_U3CDataU3Ek__BackingField_0(),
	RTRequest_tAB65BDCB5A0862353CF86D457AA4901F2E32DCC1::get_offset_of_U3COpCodeU3Ek__BackingField_1(),
	RTRequest_tAB65BDCB5A0862353CF86D457AA4901F2E32DCC1::get_offset_of_U3CTargetPlayersU3Ek__BackingField_2(),
	RTRequest_tAB65BDCB5A0862353CF86D457AA4901F2E32DCC1::get_offset_of_U3CintentU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5958 = { sizeof (LoginCommand_t745C41CD1854E2E34D0F013332BBFE2A9130A307), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5958[2] = 
{
	LoginCommand_t745C41CD1854E2E34D0F013332BBFE2A9130A307::get_offset_of_U3CTokenU3Ek__BackingField_4(),
	LoginCommand_t745C41CD1854E2E34D0F013332BBFE2A9130A307::get_offset_of_U3CClientVersionU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5959 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5960 = { sizeof (AbstractResult_tB9A3C663F42A72B8FCA862FF115309B197235C96), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5960[2] = 
{
	AbstractResult_tB9A3C663F42A72B8FCA862FF115309B197235C96::get_offset_of_packet_0(),
	AbstractResult_tB9A3C663F42A72B8FCA862FF115309B197235C96::get_offset_of_session_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5961 = { sizeof (LoginResult_t26281FAE92728CAE243C870E4453A5E36C1E7591), -1, sizeof(LoginResult_t26281FAE92728CAE243C870E4453A5E36C1E7591_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5961[8] = 
{
	LoginResult_t26281FAE92728CAE243C870E4453A5E36C1E7591_StaticFields::get_offset_of_pool_2(),
	LoginResult_t26281FAE92728CAE243C870E4453A5E36C1E7591::get_offset_of_U3CSuccessU3Ek__BackingField_3(),
	LoginResult_t26281FAE92728CAE243C870E4453A5E36C1E7591::get_offset_of_U3CReconnectTokenU3Ek__BackingField_4(),
	LoginResult_t26281FAE92728CAE243C870E4453A5E36C1E7591::get_offset_of_U3CPeerIdU3Ek__BackingField_5(),
	LoginResult_t26281FAE92728CAE243C870E4453A5E36C1E7591::get_offset_of_U3CActivePeersU3Ek__BackingField_6(),
	LoginResult_t26281FAE92728CAE243C870E4453A5E36C1E7591::get_offset_of_U3CFastPortU3Ek__BackingField_7(),
	LoginResult_t26281FAE92728CAE243C870E4453A5E36C1E7591_StaticFields::get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate2_8(),
	LoginResult_t26281FAE92728CAE243C870E4453A5E36C1E7591_StaticFields::get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate3_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5962 = { sizeof (PingResult_tDBCE93222A19AE547042A11B208C29DCF7C268D8), -1, sizeof(PingResult_tDBCE93222A19AE547042A11B208C29DCF7C268D8_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5962[2] = 
{
	PingResult_tDBCE93222A19AE547042A11B208C29DCF7C268D8_StaticFields::get_offset_of_pool_2(),
	PingResult_tDBCE93222A19AE547042A11B208C29DCF7C268D8_StaticFields::get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5963 = { sizeof (UDPConnectMessage_tF627E88C457DC9DD4B0826ED6C908EFB2A326B54), -1, sizeof(UDPConnectMessage_tF627E88C457DC9DD4B0826ED6C908EFB2A326B54_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5963[2] = 
{
	UDPConnectMessage_tF627E88C457DC9DD4B0826ED6C908EFB2A326B54_StaticFields::get_offset_of_pool_2(),
	UDPConnectMessage_tF627E88C457DC9DD4B0826ED6C908EFB2A326B54_StaticFields::get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5964 = { sizeof (PlayerConnectMessage_tB1914D36571FEE52F9F5AD7A5BD971C886C53B12), -1, sizeof(PlayerConnectMessage_tB1914D36571FEE52F9F5AD7A5BD971C886C53B12_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5964[5] = 
{
	PlayerConnectMessage_tB1914D36571FEE52F9F5AD7A5BD971C886C53B12_StaticFields::get_offset_of_pool_2(),
	PlayerConnectMessage_tB1914D36571FEE52F9F5AD7A5BD971C886C53B12::get_offset_of_U3CPeerIdU3Ek__BackingField_3(),
	PlayerConnectMessage_tB1914D36571FEE52F9F5AD7A5BD971C886C53B12::get_offset_of_U3CActivePeersU3Ek__BackingField_4(),
	PlayerConnectMessage_tB1914D36571FEE52F9F5AD7A5BD971C886C53B12_StaticFields::get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate2_5(),
	PlayerConnectMessage_tB1914D36571FEE52F9F5AD7A5BD971C886C53B12_StaticFields::get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate3_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5965 = { sizeof (PlayerDisconnectMessage_tA44AFA9474033AA2BBCEC8DB2298F1B644A1D471), -1, sizeof(PlayerDisconnectMessage_tA44AFA9474033AA2BBCEC8DB2298F1B644A1D471_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5965[5] = 
{
	PlayerDisconnectMessage_tA44AFA9474033AA2BBCEC8DB2298F1B644A1D471_StaticFields::get_offset_of_pool_2(),
	PlayerDisconnectMessage_tA44AFA9474033AA2BBCEC8DB2298F1B644A1D471::get_offset_of_U3CPeerIdU3Ek__BackingField_3(),
	PlayerDisconnectMessage_tA44AFA9474033AA2BBCEC8DB2298F1B644A1D471::get_offset_of_U3CActivePeersU3Ek__BackingField_4(),
	PlayerDisconnectMessage_tA44AFA9474033AA2BBCEC8DB2298F1B644A1D471_StaticFields::get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate2_5(),
	PlayerDisconnectMessage_tA44AFA9474033AA2BBCEC8DB2298F1B644A1D471_StaticFields::get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate3_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5966 = { sizeof (PositionStream_tF8B0CE7A2A52EE9A986DECBA4F07F5BFA773228D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5966[4] = 
{
	PositionStream_tF8B0CE7A2A52EE9A986DECBA4F07F5BFA773228D::get_offset_of_stream_5(),
	PositionStream_tF8B0CE7A2A52EE9A986DECBA4F07F5BFA773228D::get_offset_of_readByte_6(),
	PositionStream_tF8B0CE7A2A52EE9A986DECBA4F07F5BFA773228D::get_offset_of_BinaryReader_7(),
	PositionStream_tF8B0CE7A2A52EE9A986DECBA4F07F5BFA773228D::get_offset_of_U3CBytesReadU3Ek__BackingField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5967 = { sizeof (LimitedPositionStream_t39D3BF1EF4DFE19E62D02F20AA326007FD4A2853), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5967[1] = 
{
	LimitedPositionStream_t39D3BF1EF4DFE19E62D02F20AA326007FD4A2853::get_offset_of_limit_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5968 = { sizeof (PooledObjects_t7D516C3A58DE32F419C7820AFEB7F948693A13F6), -1, sizeof(PooledObjects_t7D516C3A58DE32F419C7820AFEB7F948693A13F6_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5968[15] = 
{
	PooledObjects_t7D516C3A58DE32F419C7820AFEB7F948693A13F6_StaticFields::get_offset_of_U3CMemoryStreamPoolU3Ek__BackingField_0(),
	PooledObjects_t7D516C3A58DE32F419C7820AFEB7F948693A13F6_StaticFields::get_offset_of_U3CCustomRequestPoolU3Ek__BackingField_1(),
	PooledObjects_t7D516C3A58DE32F419C7820AFEB7F948693A13F6_StaticFields::get_offset_of_U3CPositionStreamPoolU3Ek__BackingField_2(),
	PooledObjects_t7D516C3A58DE32F419C7820AFEB7F948693A13F6_StaticFields::get_offset_of_U3CLimitedPositionStreamPoolU3Ek__BackingField_3(),
	PooledObjects_t7D516C3A58DE32F419C7820AFEB7F948693A13F6_StaticFields::get_offset_of_U3CByteBufferPoolU3Ek__BackingField_4(),
	PooledObjects_t7D516C3A58DE32F419C7820AFEB7F948693A13F6_StaticFields::get_offset_of_U3CPacketPoolU3Ek__BackingField_5(),
	PooledObjects_t7D516C3A58DE32F419C7820AFEB7F948693A13F6_StaticFields::get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate9_6(),
	PooledObjects_t7D516C3A58DE32F419C7820AFEB7F948693A13F6_StaticFields::get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegatea_7(),
	PooledObjects_t7D516C3A58DE32F419C7820AFEB7F948693A13F6_StaticFields::get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegateb_8(),
	PooledObjects_t7D516C3A58DE32F419C7820AFEB7F948693A13F6_StaticFields::get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegatec_9(),
	PooledObjects_t7D516C3A58DE32F419C7820AFEB7F948693A13F6_StaticFields::get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegated_10(),
	PooledObjects_t7D516C3A58DE32F419C7820AFEB7F948693A13F6_StaticFields::get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegatee_11(),
	PooledObjects_t7D516C3A58DE32F419C7820AFEB7F948693A13F6_StaticFields::get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegatef_12(),
	PooledObjects_t7D516C3A58DE32F419C7820AFEB7F948693A13F6_StaticFields::get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate10_13(),
	PooledObjects_t7D516C3A58DE32F419C7820AFEB7F948693A13F6_StaticFields::get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate11_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5969 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5969[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5970 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5971 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5972 = { sizeof (CommandFactory_t0183E1BC06011BBEB4BAD35F78DA214D2448D509), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5973 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5974 = { sizeof (RTSessionImpl_t4277EBD12AF635CB4E621DF4B4C4731EF1A452D9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5974[19] = 
{
	RTSessionImpl_t4277EBD12AF635CB4E621DF4B4C4731EF1A452D9::get_offset_of_reliableConnection_0(),
	RTSessionImpl_t4277EBD12AF635CB4E621DF4B4C4731EF1A452D9::get_offset_of_reliableWSConnection_1(),
	RTSessionImpl_t4277EBD12AF635CB4E621DF4B4C4731EF1A452D9::get_offset_of_fastConnection_2(),
	RTSessionImpl_t4277EBD12AF635CB4E621DF4B4C4731EF1A452D9::get_offset_of_hostName_3(),
	RTSessionImpl_t4277EBD12AF635CB4E621DF4B4C4731EF1A452D9::get_offset_of_TcpPort_4(),
	RTSessionImpl_t4277EBD12AF635CB4E621DF4B4C4731EF1A452D9::get_offset_of_running_5(),
	RTSessionImpl_t4277EBD12AF635CB4E621DF4B4C4731EF1A452D9::get_offset_of_useOnlyWebSockets_6(),
	RTSessionImpl_t4277EBD12AF635CB4E621DF4B4C4731EF1A452D9::get_offset_of_connectionAttempts_7(),
	RTSessionImpl_t4277EBD12AF635CB4E621DF4B4C4731EF1A452D9::get_offset_of_mustConnectBy_8(),
	RTSessionImpl_t4277EBD12AF635CB4E621DF4B4C4731EF1A452D9::get_offset_of_internalState_9(),
	RTSessionImpl_t4277EBD12AF635CB4E621DF4B4C4731EF1A452D9::get_offset_of_actionQueue_10(),
	RTSessionImpl_t4277EBD12AF635CB4E621DF4B4C4731EF1A452D9::get_offset_of_peerMaxSequenceNumbers_11(),
	RTSessionImpl_t4277EBD12AF635CB4E621DF4B4C4731EF1A452D9::get_offset_of_sequenceNumber_12(),
	RTSessionImpl_t4277EBD12AF635CB4E621DF4B4C4731EF1A452D9::get_offset_of_U3CFastPortU3Ek__BackingField_13(),
	RTSessionImpl_t4277EBD12AF635CB4E621DF4B4C4731EF1A452D9::get_offset_of_U3CConnectTokenU3Ek__BackingField_14(),
	RTSessionImpl_t4277EBD12AF635CB4E621DF4B4C4731EF1A452D9::get_offset_of_U3CReadyU3Ek__BackingField_15(),
	RTSessionImpl_t4277EBD12AF635CB4E621DF4B4C4731EF1A452D9::get_offset_of_U3CPeerIdU3Ek__BackingField_16(),
	RTSessionImpl_t4277EBD12AF635CB4E621DF4B4C4731EF1A452D9::get_offset_of_U3CActivePeersU3Ek__BackingField_17(),
	RTSessionImpl_t4277EBD12AF635CB4E621DF4B4C4731EF1A452D9::get_offset_of_U3CSessionListenerU3Ek__BackingField_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5975 = { sizeof (U3CU3Ec__DisplayClass2_t12447CD2E3C28EEFFC550BA4102D850BB2BEC1C3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5975[2] = 
{
	U3CU3Ec__DisplayClass2_t12447CD2E3C28EEFFC550BA4102D850BB2BEC1C3::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass2_t12447CD2E3C28EEFFC550BA4102D850BB2BEC1C3::get_offset_of_ready_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5976 = { sizeof (ProtocolBufferException_tC3A6856D10B5F85950B0BECA2A9F8683362FA29F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5977 = { sizeof (Connection_tBC3E695B64A7864140245909D74866C1FE9A2818), -1, sizeof(Connection_tBC3E695B64A7864140245909D74866C1FE9A2818_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5977[4] = 
{
	Connection_tBC3E695B64A7864140245909D74866C1FE9A2818::get_offset_of_remoteEndPoint_0(),
	Connection_tBC3E695B64A7864140245909D74866C1FE9A2818::get_offset_of_session_1(),
	Connection_tBC3E695B64A7864140245909D74866C1FE9A2818::get_offset_of_stopped_2(),
	Connection_tBC3E695B64A7864140245909D74866C1FE9A2818_StaticFields::get_offset_of_emptyStream_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5978 = { sizeof (FastConnection_t9E83C1C0DD332A4579DA0512357037F285CA25A0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5978[5] = 
{
	FastConnection_t9E83C1C0DD332A4579DA0512357037F285CA25A0::get_offset_of_client_4(),
	FastConnection_t9E83C1C0DD332A4579DA0512357037F285CA25A0::get_offset_of_callback_5(),
	FastConnection_t9E83C1C0DD332A4579DA0512357037F285CA25A0::get_offset_of_buffer_6(),
	FastConnection_t9E83C1C0DD332A4579DA0512357037F285CA25A0::get_offset_of_ms_7(),
	FastConnection_t9E83C1C0DD332A4579DA0512357037F285CA25A0::get_offset_of_connectionAttempts_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5979 = { sizeof (ReliableConnection_t237BB0704E0E315A9E262CD04956628C1D2435B0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5979[3] = 
{
	ReliableConnection_t237BB0704E0E315A9E262CD04956628C1D2435B0::get_offset_of_client_4(),
	ReliableConnection_t237BB0704E0E315A9E262CD04956628C1D2435B0::get_offset_of_clientStream_5(),
	ReliableConnection_t237BB0704E0E315A9E262CD04956628C1D2435B0::get_offset_of_remotehost_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5980 = { sizeof (GameSparksRT_t67B264905B4578BF48D58FF7A623F647F136F3A1), -1, sizeof(GameSparksRT_t67B264905B4578BF48D58FF7A623F647F136F3A1_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5980[5] = 
{
	GameSparksRT_t67B264905B4578BF48D58FF7A623F647F136F3A1_StaticFields::get_offset_of_logLevel_0(),
	GameSparksRT_t67B264905B4578BF48D58FF7A623F647F136F3A1_StaticFields::get_offset_of_tagLevels_1(),
	GameSparksRT_t67B264905B4578BF48D58FF7A623F647F136F3A1_StaticFields::get_offset_of_random_2(),
	GameSparksRT_t67B264905B4578BF48D58FF7A623F647F136F3A1_StaticFields::get_offset_of_U3CLoggerU3Ek__BackingField_3(),
	GameSparksRT_t67B264905B4578BF48D58FF7A623F647F136F3A1_StaticFields::get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5981 = { sizeof (LogLevel_t3740A24AA8E97312F00C7B832E3887C611193A9D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5981[5] = 
{
	LogLevel_t3740A24AA8E97312F00C7B832E3887C611193A9D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5982 = { sizeof (ConnectState_t40B33F90F5E6F36C6AF9DEDA6A8D1DE696343B1B)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5982[6] = 
{
	ConnectState_t40B33F90F5E6F36C6AF9DEDA6A8D1DE696343B1B::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5983 = { sizeof (DeliveryIntent_tA8A9B56F5F6509947ED27972D07F5117171C55A4)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5983[4] = 
{
	DeliveryIntent_tA8A9B56F5F6509947ED27972D07F5117171C55A4::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5984 = { sizeof (GameSparksRTSessionBuilder_t9F558E7F87523BFA846A216DA10AF5713522C0A7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5984[5] = 
{
	GameSparksRTSessionBuilder_t9F558E7F87523BFA846A216DA10AF5713522C0A7::get_offset_of_connectToken_0(),
	GameSparksRTSessionBuilder_t9F558E7F87523BFA846A216DA10AF5713522C0A7::get_offset_of_host_1(),
	GameSparksRTSessionBuilder_t9F558E7F87523BFA846A216DA10AF5713522C0A7::get_offset_of_port_2(),
	GameSparksRTSessionBuilder_t9F558E7F87523BFA846A216DA10AF5713522C0A7::get_offset_of_listener_3(),
	GameSparksRTSessionBuilder_t9F558E7F87523BFA846A216DA10AF5713522C0A7::get_offset_of_useOnlyWebSockets_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5985 = { sizeof (LogCommand_t7558700959C71C5086C24D058AE7AFE8B0E15DE8), -1, sizeof(LogCommand_t7558700959C71C5086C24D058AE7AFE8B0E15DE8_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5985[7] = 
{
	LogCommand_t7558700959C71C5086C24D058AE7AFE8B0E15DE8_StaticFields::get_offset_of_pool_0(),
	LogCommand_t7558700959C71C5086C24D058AE7AFE8B0E15DE8::get_offset_of_tag_1(),
	LogCommand_t7558700959C71C5086C24D058AE7AFE8B0E15DE8::get_offset_of_msg_2(),
	LogCommand_t7558700959C71C5086C24D058AE7AFE8B0E15DE8::get_offset_of_level_3(),
	LogCommand_t7558700959C71C5086C24D058AE7AFE8B0E15DE8::get_offset_of_formatParams_4(),
	LogCommand_t7558700959C71C5086C24D058AE7AFE8B0E15DE8::get_offset_of_session_5(),
	LogCommand_t7558700959C71C5086C24D058AE7AFE8B0E15DE8_StaticFields::get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5986 = { sizeof (CustomRequest_tB8C49CE439702FB57452DB82012DE2C60EDA86F2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5986[1] = 
{
	CustomRequest_tB8C49CE439702FB57452DB82012DE2C60EDA86F2::get_offset_of_payload_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5987 = { sizeof (ActionCommand_tFC2779E4CB5E203B26B01E72149FC02CCBC0415E), -1, sizeof(ActionCommand_tFC2779E4CB5E203B26B01E72149FC02CCBC0415E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5987[3] = 
{
	ActionCommand_tFC2779E4CB5E203B26B01E72149FC02CCBC0415E_StaticFields::get_offset_of_pool_0(),
	ActionCommand_tFC2779E4CB5E203B26B01E72149FC02CCBC0415E::get_offset_of_action_1(),
	ActionCommand_tFC2779E4CB5E203B26B01E72149FC02CCBC0415E_StaticFields::get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5988 = { sizeof (RTVal_tD2AB0B5959D5AE7F251253034BEF83F306A04D0E)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5988[6] = 
{
	RTVal_tD2AB0B5959D5AE7F251253034BEF83F306A04D0E::get_offset_of_long_val_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RTVal_tD2AB0B5959D5AE7F251253034BEF83F306A04D0E::get_offset_of_float_val_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RTVal_tD2AB0B5959D5AE7F251253034BEF83F306A04D0E::get_offset_of_double_val_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RTVal_tD2AB0B5959D5AE7F251253034BEF83F306A04D0E::get_offset_of_data_val_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RTVal_tD2AB0B5959D5AE7F251253034BEF83F306A04D0E::get_offset_of_string_val_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RTVal_tD2AB0B5959D5AE7F251253034BEF83F306A04D0E::get_offset_of_vec_val_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5989 = { sizeof (BinaryWriteMemoryStream_t9503BBB30B8671EB7F1F21C98D1A5B0CBFD6B84A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5989[2] = 
{
	BinaryWriteMemoryStream_t9503BBB30B8671EB7F1F21C98D1A5B0CBFD6B84A::get_offset_of_BinaryReader_15(),
	BinaryWriteMemoryStream_t9503BBB30B8671EB7F1F21C98D1A5B0CBFD6B84A::get_offset_of_BinaryWriter_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5990 = { sizeof (RTValSerializer_t70F1BD5D867AE0A87413C06C27C97D22AF73D879), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5991 = { sizeof (RTDataSerializer_tDE80F537AACC3D9B029F6C2BAAD026AC4F11D1F3), -1, sizeof(RTDataSerializer_tDE80F537AACC3D9B029F6C2BAAD026AC4F11D1F3_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5991[4] = 
{
	RTDataSerializer_tDE80F537AACC3D9B029F6C2BAAD026AC4F11D1F3_StaticFields::get_offset_of_cache_0(),
	RTDataSerializer_tDE80F537AACC3D9B029F6C2BAAD026AC4F11D1F3_StaticFields::get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate3_1(),
	RTDataSerializer_tDE80F537AACC3D9B029F6C2BAAD026AC4F11D1F3_StaticFields::get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate4_2(),
	RTDataSerializer_tDE80F537AACC3D9B029F6C2BAAD026AC4F11D1F3_StaticFields::get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate5_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5992 = { sizeof (RTPacket_t7C1F65EE13D0F8BC65B8DFD3A3FE92C4171A4BC4)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5992[6] = 
{
	RTPacket_t7C1F65EE13D0F8BC65B8DFD3A3FE92C4171A4BC4::get_offset_of_OpCode_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RTPacket_t7C1F65EE13D0F8BC65B8DFD3A3FE92C4171A4BC4::get_offset_of_Sender_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RTPacket_t7C1F65EE13D0F8BC65B8DFD3A3FE92C4171A4BC4::get_offset_of_Stream_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RTPacket_t7C1F65EE13D0F8BC65B8DFD3A3FE92C4171A4BC4::get_offset_of_StreamLength_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RTPacket_t7C1F65EE13D0F8BC65B8DFD3A3FE92C4171A4BC4::get_offset_of_Data_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RTPacket_t7C1F65EE13D0F8BC65B8DFD3A3FE92C4171A4BC4::get_offset_of_PacketSize_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5993 = { sizeof (RTVector_tF61BCFCD2BC84E278968EE3D89A39BD51AB09026)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5993[4] = 
{
	RTVector_tF61BCFCD2BC84E278968EE3D89A39BD51AB09026::get_offset_of_x_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RTVector_tF61BCFCD2BC84E278968EE3D89A39BD51AB09026::get_offset_of_y_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RTVector_tF61BCFCD2BC84E278968EE3D89A39BD51AB09026::get_offset_of_z_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RTVector_tF61BCFCD2BC84E278968EE3D89A39BD51AB09026::get_offset_of_w_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5994 = { sizeof (RTData_tAAEBF5D36A61DACA40791F6B7E2D59B7FAF099B5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5994[1] = 
{
	RTData_tAAEBF5D36A61DACA40791F6B7E2D59B7FAF099B5::get_offset_of_data_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5995 = { sizeof (CustomCommand_t20AC0410E03BBF162CF04C2A64CB1154916BA4E2), -1, sizeof(CustomCommand_t20AC0410E03BBF162CF04C2A64CB1154916BA4E2_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5995[10] = 
{
	CustomCommand_t20AC0410E03BBF162CF04C2A64CB1154916BA4E2_StaticFields::get_offset_of_pool_0(),
	CustomCommand_t20AC0410E03BBF162CF04C2A64CB1154916BA4E2::get_offset_of_session_1(),
	CustomCommand_t20AC0410E03BBF162CF04C2A64CB1154916BA4E2::get_offset_of_opCode_2(),
	CustomCommand_t20AC0410E03BBF162CF04C2A64CB1154916BA4E2::get_offset_of_sender_3(),
	CustomCommand_t20AC0410E03BBF162CF04C2A64CB1154916BA4E2::get_offset_of_limit_4(),
	CustomCommand_t20AC0410E03BBF162CF04C2A64CB1154916BA4E2::get_offset_of_packetSize_5(),
	CustomCommand_t20AC0410E03BBF162CF04C2A64CB1154916BA4E2::get_offset_of_ms_6(),
	CustomCommand_t20AC0410E03BBF162CF04C2A64CB1154916BA4E2::get_offset_of_limitedStream_7(),
	CustomCommand_t20AC0410E03BBF162CF04C2A64CB1154916BA4E2::get_offset_of_data_8(),
	CustomCommand_t20AC0410E03BBF162CF04C2A64CB1154916BA4E2_StaticFields::get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5996 = { sizeof (GSTlsVerifyCertificateRT_t0AFC95E24504B6445A71C51411605DE74F16FE2B), -1, sizeof(GSTlsVerifyCertificateRT_t0AFC95E24504B6445A71C51411605DE74F16FE2B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5996[1] = 
{
	GSTlsVerifyCertificateRT_t0AFC95E24504B6445A71C51411605DE74F16FE2B_StaticFields::get_offset_of_U3COnVerifyCertificateU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5997 = { sizeof (GSTlsClient_tE067E7168EBE292131479D0AA6C0B50D3077D750), -1, sizeof(GSTlsClient_tE067E7168EBE292131479D0AA6C0B50D3077D750_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5997[2] = 
{
	GSTlsClient_tE067E7168EBE292131479D0AA6C0B50D3077D750::get_offset_of_hostName_8(),
	GSTlsClient_tE067E7168EBE292131479D0AA6C0B50D3077D750_StaticFields::get_offset_of_logger_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5998 = { sizeof (GSTlsAuthentication_t046151E273242DA47443043454D7D45E716C40E2), -1, sizeof(GSTlsAuthentication_t046151E273242DA47443043454D7D45E716C40E2_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5998[2] = 
{
	GSTlsAuthentication_t046151E273242DA47443043454D7D45E716C40E2_StaticFields::get_offset_of_rootCert_0(),
	GSTlsAuthentication_t046151E273242DA47443043454D7D45E716C40E2::get_offset_of_validCertNames_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5999 = { sizeof (DuplexTlsStream_t0D65185B89BA59E923BFA3A990E535540924D12E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5999[1] = 
{
	DuplexTlsStream_t0D65185B89BA59E923BFA3A990E535540924D12E::get_offset_of_wrapped_5(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
