﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Newtonsoft.Json.Bson.BsonBinaryWriter
struct BsonBinaryWriter_t3572084F4EE9D0ACA3EAE31EF1C0DF358D080D80;
// Newtonsoft.Json.Bson.BsonReader/ContainerContext
struct ContainerContext_t863E55417E974D1C50EA7E90F62077FC15D5241F;
// Newtonsoft.Json.Bson.BsonString
struct BsonString_t87A7877CE01E3E2FA2B118C4A617FA16159CBC0A;
// Newtonsoft.Json.Bson.BsonToken
struct BsonToken_tEB681A5D0E53F8005DC31BAEFDE6481BBE80C2D3;
// Newtonsoft.Json.JsonWriter/State[][]
struct StateU5BU5DU5BU5D_t9EFAA051C5ADACB3E88E72C4C49B2F2CDA249867;
// Newtonsoft.Json.Linq.JArray
struct JArray_t1CE13821116F9B501573275C6BDD9FB254E65F11;
// Newtonsoft.Json.Linq.JContainer
struct JContainer_tF4CD2E574503C709DEF18A04B79B264B83746DAB;
// Newtonsoft.Json.Linq.JObject
struct JObject_t786AF07B1009334856B0362BBC48EEF68C81C585;
// Newtonsoft.Json.Linq.JProperty/JPropertyList
struct JPropertyList_tAC5DCFB03C7879ED6FCFD74977E76474C918200F;
// Newtonsoft.Json.Linq.JPropertyKeyedCollection
struct JPropertyKeyedCollection_t013DFBF6A88616F3C011C00B60291F2EAB75483D;
// Newtonsoft.Json.Linq.JToken
struct JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02;
// Newtonsoft.Json.Linq.JTokenType[]
struct JTokenTypeU5BU5D_t95EDBAB8E7D19E83A29F71593B6FEC1F54E17344;
// Newtonsoft.Json.Linq.JValue
struct JValue_t69F069DA12BD51A8FEB36C2EDA5D258B7FE2C4BF;
// Newtonsoft.Json.Linq.JsonPath.ArrayIndexFilter
struct ArrayIndexFilter_t4B8AB90AF78B3976E09BEA34466153A1CDE2E12B;
// Newtonsoft.Json.Linq.JsonPath.ArrayMultipleIndexFilter
struct ArrayMultipleIndexFilter_tF9FF764DDCB79A819281D799112F6E930B75E99F;
// Newtonsoft.Json.Linq.JsonPath.ArraySliceFilter
struct ArraySliceFilter_tC5035758E75DB3EF9EAA182D561A726F46397BE3;
// Newtonsoft.Json.Linq.JsonPath.FieldFilter
struct FieldFilter_t2B4992D5A1D10B7A905F49C62609097AED91A13B;
// Newtonsoft.Json.Linq.JsonPath.FieldMultipleFilter
struct FieldMultipleFilter_tE61362AE319B8C510FD11900AD0A0B79EC08CD6B;
// Newtonsoft.Json.Linq.JsonPath.QueryExpression
struct QueryExpression_t08B8769F946D3E05F6E9DB46393F0B9828244E7F;
// Newtonsoft.Json.Linq.JsonPath.QueryFilter
struct QueryFilter_t6BBC6AA8C49E2AEED63087A9FCEB306F022C22AF;
// Newtonsoft.Json.Linq.JsonPath.ScanFilter
struct ScanFilter_t1FC1A02EA817F7D53FCA21E7FBB5DF326FC03F3D;
// Newtonsoft.Json.Utilities.ReflectionObject
struct ReflectionObject_t3EDC4C62AF6FE6E5246ED34686ED44305157331D;
// Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Type,Newtonsoft.Json.Utilities.ReflectionObject>
struct ThreadSafeStore_2_t383F3E9472520F192BABC81C35BAC871CE4CC93E;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JToken>
struct IEnumerable_1_t133B7916368C2F565BDFF2674430B992DB49577F;
// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken>
struct IEnumerator_1_tA3DFFFD6133C0C5607FFA19B3E4E7395E872113C;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Linq.JToken>>
struct IEnumerator_1_tD9B1737A1A9261F2D2838143F686F007E8555CF4;
// System.Collections.Generic.List`1<Newtonsoft.Json.Bson.BsonProperty>
struct List_1_t2496F27A4736C5D9542D33FE9FF2F0A3AF8F2566;
// System.Collections.Generic.List`1<Newtonsoft.Json.Bson.BsonReader/ContainerContext>
struct List_1_t7B3D3377EC84D5093BE9B1EEA7E5A83E25858361;
// System.Collections.Generic.List`1<Newtonsoft.Json.Bson.BsonToken>
struct List_1_t68BC38D388C4DC6640F9F3B1E87BDAE4EFA5E6E3;
// System.Collections.Generic.List`1<Newtonsoft.Json.Converters.IXmlNode>
struct List_1_tE8856893C3BB37F3EF523D597A4152D5DCD8E400;
// System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition>
struct List_1_tBF641BBAF7DFF674220860102C874E20DB3EB403;
// System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JToken>
struct List_1_t29FF67BC2CD2EEBC234D6E03E125122CC8730B5C;
// System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JsonPath.PathFilter>
struct List_1_t36919C030DB41B8F0BE34CC4C96991F1D1379557;
// System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JsonPath.QueryExpression>
struct List_1_tACC9B8F07DB2C436197EF4D23781FF22DD6ED615;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226;
// System.Collections.Generic.List`1<System.String>
struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3;
// System.ComponentModel.AddingNewEventHandler
struct AddingNewEventHandler_t01FE6DE3A526F77E211D7DB02B76A07888E605A7;
// System.ComponentModel.ListChangedEventHandler
struct ListChangedEventHandler_t8081F1428D22013519901C16884C5ACE86A72A88;
// System.ComponentModel.PropertyChangedEventHandler
struct PropertyChangedEventHandler_t617E98E1876A8EB394D2B329340CE02D21FFFC82;
// System.ComponentModel.PropertyChangingEventHandler
struct PropertyChangingEventHandler_tEE60268216C5CB90375F55934CCCDEF654BA7EA3;
// System.Func`2<System.String,System.String>
struct Func_2_tD9EBF5FAD46B4F561FE5ABE814DC60E6253B8B96;
// System.Globalization.CultureInfo
struct CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F;
// System.IO.BinaryReader
struct BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969;
// System.IO.BinaryWriter
struct BinaryWriter_t1B8E78B6CACFBFB57BF140FB4DECE19AFD249CC3;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.String
struct String_t;
// System.Text.Encoding
struct Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4;
// System.Type
struct Type_t;
// System.Xml.Linq.XDeclaration
struct XDeclaration_tB17C002AEFF0D8802A09677E246F555FDD1F8512;
// System.Xml.Linq.XDocumentType
struct XDocumentType_tB3120C99DFF0207A8DDAF94868BA84AD2296E444;
// System.Xml.Linq.XObject
struct XObject_t0DA241208A0EC65CA16AEA485DD80F21A693EDEF;
// System.Xml.XmlDeclaration
struct XmlDeclaration_tB67A8B98F543947C6EB990C44A212527B9B51200;
// System.Xml.XmlDocument
struct XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97;
// System.Xml.XmlDocumentType
struct XmlDocumentType_t9AB8D5C3C27B699B885D941D4B4735A6EE039136;
// System.Xml.XmlElement
struct XmlElement_t05D8C7971DE016A354D86028E7FFD84CD9DDDFDC;
// System.Xml.XmlNode
struct XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef BSONOBJECTID_TF24F67A9B09CB213513B5218DF725B4E9AF3379C_H
#define BSONOBJECTID_TF24F67A9B09CB213513B5218DF725B4E9AF3379C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Bson.BsonObjectId
struct  BsonObjectId_tF24F67A9B09CB213513B5218DF725B4E9AF3379C  : public RuntimeObject
{
public:
	// System.Byte[] Newtonsoft.Json.Bson.BsonObjectId::<Value>k__BackingField
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___U3CValueU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CValueU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(BsonObjectId_tF24F67A9B09CB213513B5218DF725B4E9AF3379C, ___U3CValueU3Ek__BackingField_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_U3CValueU3Ek__BackingField_0() const { return ___U3CValueU3Ek__BackingField_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_U3CValueU3Ek__BackingField_0() { return &___U3CValueU3Ek__BackingField_0; }
	inline void set_U3CValueU3Ek__BackingField_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___U3CValueU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CValueU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONOBJECTID_TF24F67A9B09CB213513B5218DF725B4E9AF3379C_H
#ifndef BSONPROPERTY_T82747119CE439C13F6809CD9F8D815C3CE429583_H
#define BSONPROPERTY_T82747119CE439C13F6809CD9F8D815C3CE429583_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Bson.BsonProperty
struct  BsonProperty_t82747119CE439C13F6809CD9F8D815C3CE429583  : public RuntimeObject
{
public:
	// Newtonsoft.Json.Bson.BsonString Newtonsoft.Json.Bson.BsonProperty::<Name>k__BackingField
	BsonString_t87A7877CE01E3E2FA2B118C4A617FA16159CBC0A * ___U3CNameU3Ek__BackingField_0;
	// Newtonsoft.Json.Bson.BsonToken Newtonsoft.Json.Bson.BsonProperty::<Value>k__BackingField
	BsonToken_tEB681A5D0E53F8005DC31BAEFDE6481BBE80C2D3 * ___U3CValueU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(BsonProperty_t82747119CE439C13F6809CD9F8D815C3CE429583, ___U3CNameU3Ek__BackingField_0)); }
	inline BsonString_t87A7877CE01E3E2FA2B118C4A617FA16159CBC0A * get_U3CNameU3Ek__BackingField_0() const { return ___U3CNameU3Ek__BackingField_0; }
	inline BsonString_t87A7877CE01E3E2FA2B118C4A617FA16159CBC0A ** get_address_of_U3CNameU3Ek__BackingField_0() { return &___U3CNameU3Ek__BackingField_0; }
	inline void set_U3CNameU3Ek__BackingField_0(BsonString_t87A7877CE01E3E2FA2B118C4A617FA16159CBC0A * value)
	{
		___U3CNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CValueU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(BsonProperty_t82747119CE439C13F6809CD9F8D815C3CE429583, ___U3CValueU3Ek__BackingField_1)); }
	inline BsonToken_tEB681A5D0E53F8005DC31BAEFDE6481BBE80C2D3 * get_U3CValueU3Ek__BackingField_1() const { return ___U3CValueU3Ek__BackingField_1; }
	inline BsonToken_tEB681A5D0E53F8005DC31BAEFDE6481BBE80C2D3 ** get_address_of_U3CValueU3Ek__BackingField_1() { return &___U3CValueU3Ek__BackingField_1; }
	inline void set_U3CValueU3Ek__BackingField_1(BsonToken_tEB681A5D0E53F8005DC31BAEFDE6481BBE80C2D3 * value)
	{
		___U3CValueU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CValueU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONPROPERTY_T82747119CE439C13F6809CD9F8D815C3CE429583_H
#ifndef BSONTOKEN_TEB681A5D0E53F8005DC31BAEFDE6481BBE80C2D3_H
#define BSONTOKEN_TEB681A5D0E53F8005DC31BAEFDE6481BBE80C2D3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Bson.BsonToken
struct  BsonToken_tEB681A5D0E53F8005DC31BAEFDE6481BBE80C2D3  : public RuntimeObject
{
public:
	// Newtonsoft.Json.Bson.BsonToken Newtonsoft.Json.Bson.BsonToken::<Parent>k__BackingField
	BsonToken_tEB681A5D0E53F8005DC31BAEFDE6481BBE80C2D3 * ___U3CParentU3Ek__BackingField_0;
	// System.Int32 Newtonsoft.Json.Bson.BsonToken::<CalculatedSize>k__BackingField
	int32_t ___U3CCalculatedSizeU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CParentU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(BsonToken_tEB681A5D0E53F8005DC31BAEFDE6481BBE80C2D3, ___U3CParentU3Ek__BackingField_0)); }
	inline BsonToken_tEB681A5D0E53F8005DC31BAEFDE6481BBE80C2D3 * get_U3CParentU3Ek__BackingField_0() const { return ___U3CParentU3Ek__BackingField_0; }
	inline BsonToken_tEB681A5D0E53F8005DC31BAEFDE6481BBE80C2D3 ** get_address_of_U3CParentU3Ek__BackingField_0() { return &___U3CParentU3Ek__BackingField_0; }
	inline void set_U3CParentU3Ek__BackingField_0(BsonToken_tEB681A5D0E53F8005DC31BAEFDE6481BBE80C2D3 * value)
	{
		___U3CParentU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CParentU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CCalculatedSizeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(BsonToken_tEB681A5D0E53F8005DC31BAEFDE6481BBE80C2D3, ___U3CCalculatedSizeU3Ek__BackingField_1)); }
	inline int32_t get_U3CCalculatedSizeU3Ek__BackingField_1() const { return ___U3CCalculatedSizeU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CCalculatedSizeU3Ek__BackingField_1() { return &___U3CCalculatedSizeU3Ek__BackingField_1; }
	inline void set_U3CCalculatedSizeU3Ek__BackingField_1(int32_t value)
	{
		___U3CCalculatedSizeU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONTOKEN_TEB681A5D0E53F8005DC31BAEFDE6481BBE80C2D3_H
#ifndef XOBJECTWRAPPER_TAB3FD0F91A7839A07C2FD1C40020F14201650305_H
#define XOBJECTWRAPPER_TAB3FD0F91A7839A07C2FD1C40020F14201650305_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.XObjectWrapper
struct  XObjectWrapper_tAB3FD0F91A7839A07C2FD1C40020F14201650305  : public RuntimeObject
{
public:
	// System.Xml.Linq.XObject Newtonsoft.Json.Converters.XObjectWrapper::_xmlObject
	XObject_t0DA241208A0EC65CA16AEA485DD80F21A693EDEF * ____xmlObject_1;

public:
	inline static int32_t get_offset_of__xmlObject_1() { return static_cast<int32_t>(offsetof(XObjectWrapper_tAB3FD0F91A7839A07C2FD1C40020F14201650305, ____xmlObject_1)); }
	inline XObject_t0DA241208A0EC65CA16AEA485DD80F21A693EDEF * get__xmlObject_1() const { return ____xmlObject_1; }
	inline XObject_t0DA241208A0EC65CA16AEA485DD80F21A693EDEF ** get_address_of__xmlObject_1() { return &____xmlObject_1; }
	inline void set__xmlObject_1(XObject_t0DA241208A0EC65CA16AEA485DD80F21A693EDEF * value)
	{
		____xmlObject_1 = value;
		Il2CppCodeGenWriteBarrier((&____xmlObject_1), value);
	}
};

struct XObjectWrapper_tAB3FD0F91A7839A07C2FD1C40020F14201650305_StaticFields
{
public:
	// System.Collections.Generic.List`1<Newtonsoft.Json.Converters.IXmlNode> Newtonsoft.Json.Converters.XObjectWrapper::EmptyChildNodes
	List_1_tE8856893C3BB37F3EF523D597A4152D5DCD8E400 * ___EmptyChildNodes_0;

public:
	inline static int32_t get_offset_of_EmptyChildNodes_0() { return static_cast<int32_t>(offsetof(XObjectWrapper_tAB3FD0F91A7839A07C2FD1C40020F14201650305_StaticFields, ___EmptyChildNodes_0)); }
	inline List_1_tE8856893C3BB37F3EF523D597A4152D5DCD8E400 * get_EmptyChildNodes_0() const { return ___EmptyChildNodes_0; }
	inline List_1_tE8856893C3BB37F3EF523D597A4152D5DCD8E400 ** get_address_of_EmptyChildNodes_0() { return &___EmptyChildNodes_0; }
	inline void set_EmptyChildNodes_0(List_1_tE8856893C3BB37F3EF523D597A4152D5DCD8E400 * value)
	{
		___EmptyChildNodes_0 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyChildNodes_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XOBJECTWRAPPER_TAB3FD0F91A7839A07C2FD1C40020F14201650305_H
#ifndef XMLNODEWRAPPER_T3C0D443DF6B193527862D3D680F0BD1979668C3F_H
#define XMLNODEWRAPPER_T3C0D443DF6B193527862D3D680F0BD1979668C3F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.XmlNodeWrapper
struct  XmlNodeWrapper_t3C0D443DF6B193527862D3D680F0BD1979668C3F  : public RuntimeObject
{
public:
	// System.Xml.XmlNode Newtonsoft.Json.Converters.XmlNodeWrapper::_node
	XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * ____node_0;
	// System.Collections.Generic.List`1<Newtonsoft.Json.Converters.IXmlNode> Newtonsoft.Json.Converters.XmlNodeWrapper::_childNodes
	List_1_tE8856893C3BB37F3EF523D597A4152D5DCD8E400 * ____childNodes_1;
	// System.Collections.Generic.List`1<Newtonsoft.Json.Converters.IXmlNode> Newtonsoft.Json.Converters.XmlNodeWrapper::_attributes
	List_1_tE8856893C3BB37F3EF523D597A4152D5DCD8E400 * ____attributes_2;

public:
	inline static int32_t get_offset_of__node_0() { return static_cast<int32_t>(offsetof(XmlNodeWrapper_t3C0D443DF6B193527862D3D680F0BD1979668C3F, ____node_0)); }
	inline XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * get__node_0() const { return ____node_0; }
	inline XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB ** get_address_of__node_0() { return &____node_0; }
	inline void set__node_0(XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * value)
	{
		____node_0 = value;
		Il2CppCodeGenWriteBarrier((&____node_0), value);
	}

	inline static int32_t get_offset_of__childNodes_1() { return static_cast<int32_t>(offsetof(XmlNodeWrapper_t3C0D443DF6B193527862D3D680F0BD1979668C3F, ____childNodes_1)); }
	inline List_1_tE8856893C3BB37F3EF523D597A4152D5DCD8E400 * get__childNodes_1() const { return ____childNodes_1; }
	inline List_1_tE8856893C3BB37F3EF523D597A4152D5DCD8E400 ** get_address_of__childNodes_1() { return &____childNodes_1; }
	inline void set__childNodes_1(List_1_tE8856893C3BB37F3EF523D597A4152D5DCD8E400 * value)
	{
		____childNodes_1 = value;
		Il2CppCodeGenWriteBarrier((&____childNodes_1), value);
	}

	inline static int32_t get_offset_of__attributes_2() { return static_cast<int32_t>(offsetof(XmlNodeWrapper_t3C0D443DF6B193527862D3D680F0BD1979668C3F, ____attributes_2)); }
	inline List_1_tE8856893C3BB37F3EF523D597A4152D5DCD8E400 * get__attributes_2() const { return ____attributes_2; }
	inline List_1_tE8856893C3BB37F3EF523D597A4152D5DCD8E400 ** get_address_of__attributes_2() { return &____attributes_2; }
	inline void set__attributes_2(List_1_tE8856893C3BB37F3EF523D597A4152D5DCD8E400 * value)
	{
		____attributes_2 = value;
		Il2CppCodeGenWriteBarrier((&____attributes_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNODEWRAPPER_T3C0D443DF6B193527862D3D680F0BD1979668C3F_H
#ifndef JSONCONVERTER_T61C31ABA417A4314A2582BF0404CE41267A61BE6_H
#define JSONCONVERTER_T61C31ABA417A4314A2582BF0404CE41267A61BE6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonConverter
struct  JsonConverter_t61C31ABA417A4314A2582BF0404CE41267A61BE6  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONCONVERTER_T61C31ABA417A4314A2582BF0404CE41267A61BE6_H
#ifndef U3CU3EC_T37838CC19F295B6E657DDBC4D19E47C423BE14DF_H
#define U3CU3EC_T37838CC19F295B6E657DDBC4D19E47C423BE14DF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.Extensions_<>c
struct  U3CU3Ec_t37838CC19F295B6E657DDBC4D19E47C423BE14DF  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t37838CC19F295B6E657DDBC4D19E47C423BE14DF_StaticFields
{
public:
	// Newtonsoft.Json.Linq.Extensions_<>c Newtonsoft.Json.Linq.Extensions_<>c::<>9
	U3CU3Ec_t37838CC19F295B6E657DDBC4D19E47C423BE14DF * ___U3CU3E9_0;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t37838CC19F295B6E657DDBC4D19E47C423BE14DF_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t37838CC19F295B6E657DDBC4D19E47C423BE14DF * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t37838CC19F295B6E657DDBC4D19E47C423BE14DF ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t37838CC19F295B6E657DDBC4D19E47C423BE14DF * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T37838CC19F295B6E657DDBC4D19E47C423BE14DF_H
#ifndef U3CGETDESCENDANTSU3ED__29_T02A1778B5FFF602EE20BEB519454C18870DDEDC4_H
#define U3CGETDESCENDANTSU3ED__29_T02A1778B5FFF602EE20BEB519454C18870DDEDC4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JContainer_<GetDescendants>d__29
struct  U3CGetDescendantsU3Ed__29_t02A1778B5FFF602EE20BEB519454C18870DDEDC4  : public RuntimeObject
{
public:
	// System.Int32 Newtonsoft.Json.Linq.JContainer_<GetDescendants>d__29::<>1__state
	int32_t ___U3CU3E1__state_0;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JContainer_<GetDescendants>d__29::<>2__current
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * ___U3CU3E2__current_1;
	// System.Int32 Newtonsoft.Json.Linq.JContainer_<GetDescendants>d__29::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// System.Boolean Newtonsoft.Json.Linq.JContainer_<GetDescendants>d__29::self
	bool ___self_3;
	// System.Boolean Newtonsoft.Json.Linq.JContainer_<GetDescendants>d__29::<>3__self
	bool ___U3CU3E3__self_4;
	// Newtonsoft.Json.Linq.JContainer Newtonsoft.Json.Linq.JContainer_<GetDescendants>d__29::<>4__this
	JContainer_tF4CD2E574503C709DEF18A04B79B264B83746DAB * ___U3CU3E4__this_5;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JContainer_<GetDescendants>d__29::<o>5__1
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * ___U3CoU3E5__1_6;
	// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JContainer_<GetDescendants>d__29::<>7__wrap1
	RuntimeObject* ___U3CU3E7__wrap1_7;
	// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JContainer_<GetDescendants>d__29::<>7__wrap2
	RuntimeObject* ___U3CU3E7__wrap2_8;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetDescendantsU3Ed__29_t02A1778B5FFF602EE20BEB519454C18870DDEDC4, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetDescendantsU3Ed__29_t02A1778B5FFF602EE20BEB519454C18870DDEDC4, ___U3CU3E2__current_1)); }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CGetDescendantsU3Ed__29_t02A1778B5FFF602EE20BEB519454C18870DDEDC4, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_self_3() { return static_cast<int32_t>(offsetof(U3CGetDescendantsU3Ed__29_t02A1778B5FFF602EE20BEB519454C18870DDEDC4, ___self_3)); }
	inline bool get_self_3() const { return ___self_3; }
	inline bool* get_address_of_self_3() { return &___self_3; }
	inline void set_self_3(bool value)
	{
		___self_3 = value;
	}

	inline static int32_t get_offset_of_U3CU3E3__self_4() { return static_cast<int32_t>(offsetof(U3CGetDescendantsU3Ed__29_t02A1778B5FFF602EE20BEB519454C18870DDEDC4, ___U3CU3E3__self_4)); }
	inline bool get_U3CU3E3__self_4() const { return ___U3CU3E3__self_4; }
	inline bool* get_address_of_U3CU3E3__self_4() { return &___U3CU3E3__self_4; }
	inline void set_U3CU3E3__self_4(bool value)
	{
		___U3CU3E3__self_4 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_5() { return static_cast<int32_t>(offsetof(U3CGetDescendantsU3Ed__29_t02A1778B5FFF602EE20BEB519454C18870DDEDC4, ___U3CU3E4__this_5)); }
	inline JContainer_tF4CD2E574503C709DEF18A04B79B264B83746DAB * get_U3CU3E4__this_5() const { return ___U3CU3E4__this_5; }
	inline JContainer_tF4CD2E574503C709DEF18A04B79B264B83746DAB ** get_address_of_U3CU3E4__this_5() { return &___U3CU3E4__this_5; }
	inline void set_U3CU3E4__this_5(JContainer_tF4CD2E574503C709DEF18A04B79B264B83746DAB * value)
	{
		___U3CU3E4__this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_5), value);
	}

	inline static int32_t get_offset_of_U3CoU3E5__1_6() { return static_cast<int32_t>(offsetof(U3CGetDescendantsU3Ed__29_t02A1778B5FFF602EE20BEB519454C18870DDEDC4, ___U3CoU3E5__1_6)); }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * get_U3CoU3E5__1_6() const { return ___U3CoU3E5__1_6; }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 ** get_address_of_U3CoU3E5__1_6() { return &___U3CoU3E5__1_6; }
	inline void set_U3CoU3E5__1_6(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * value)
	{
		___U3CoU3E5__1_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CoU3E5__1_6), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_7() { return static_cast<int32_t>(offsetof(U3CGetDescendantsU3Ed__29_t02A1778B5FFF602EE20BEB519454C18870DDEDC4, ___U3CU3E7__wrap1_7)); }
	inline RuntimeObject* get_U3CU3E7__wrap1_7() const { return ___U3CU3E7__wrap1_7; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap1_7() { return &___U3CU3E7__wrap1_7; }
	inline void set_U3CU3E7__wrap1_7(RuntimeObject* value)
	{
		___U3CU3E7__wrap1_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap1_7), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap2_8() { return static_cast<int32_t>(offsetof(U3CGetDescendantsU3Ed__29_t02A1778B5FFF602EE20BEB519454C18870DDEDC4, ___U3CU3E7__wrap2_8)); }
	inline RuntimeObject* get_U3CU3E7__wrap2_8() const { return ___U3CU3E7__wrap2_8; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap2_8() { return &___U3CU3E7__wrap2_8; }
	inline void set_U3CU3E7__wrap2_8(RuntimeObject* value)
	{
		___U3CU3E7__wrap2_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap2_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETDESCENDANTSU3ED__29_T02A1778B5FFF602EE20BEB519454C18870DDEDC4_H
#ifndef U3CU3EC_TE2C0FE447E68411655C2FA71B9D6FF79390E8E5A_H
#define U3CU3EC_TE2C0FE447E68411655C2FA71B9D6FF79390E8E5A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JObject_<>c
struct  U3CU3Ec_tE2C0FE447E68411655C2FA71B9D6FF79390E8E5A  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tE2C0FE447E68411655C2FA71B9D6FF79390E8E5A_StaticFields
{
public:
	// Newtonsoft.Json.Linq.JObject_<>c Newtonsoft.Json.Linq.JObject_<>c::<>9
	U3CU3Ec_tE2C0FE447E68411655C2FA71B9D6FF79390E8E5A * ___U3CU3E9_0;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tE2C0FE447E68411655C2FA71B9D6FF79390E8E5A_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tE2C0FE447E68411655C2FA71B9D6FF79390E8E5A * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tE2C0FE447E68411655C2FA71B9D6FF79390E8E5A ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tE2C0FE447E68411655C2FA71B9D6FF79390E8E5A * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TE2C0FE447E68411655C2FA71B9D6FF79390E8E5A_H
#ifndef JPROPERTYLIST_TAC5DCFB03C7879ED6FCFD74977E76474C918200F_H
#define JPROPERTYLIST_TAC5DCFB03C7879ED6FCFD74977E76474C918200F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JProperty_JPropertyList
struct  JPropertyList_tAC5DCFB03C7879ED6FCFD74977E76474C918200F  : public RuntimeObject
{
public:
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JProperty_JPropertyList::_token
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * ____token_0;

public:
	inline static int32_t get_offset_of__token_0() { return static_cast<int32_t>(offsetof(JPropertyList_tAC5DCFB03C7879ED6FCFD74977E76474C918200F, ____token_0)); }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * get__token_0() const { return ____token_0; }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 ** get_address_of__token_0() { return &____token_0; }
	inline void set__token_0(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * value)
	{
		____token_0 = value;
		Il2CppCodeGenWriteBarrier((&____token_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JPROPERTYLIST_TAC5DCFB03C7879ED6FCFD74977E76474C918200F_H
#ifndef U3CGETENUMERATORU3ED__1_T74F96992BB129149414620E86F10CFDF4C199AF8_H
#define U3CGETENUMERATORU3ED__1_T74F96992BB129149414620E86F10CFDF4C199AF8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JProperty_JPropertyList_<GetEnumerator>d__1
struct  U3CGetEnumeratorU3Ed__1_t74F96992BB129149414620E86F10CFDF4C199AF8  : public RuntimeObject
{
public:
	// System.Int32 Newtonsoft.Json.Linq.JProperty_JPropertyList_<GetEnumerator>d__1::<>1__state
	int32_t ___U3CU3E1__state_0;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JProperty_JPropertyList_<GetEnumerator>d__1::<>2__current
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * ___U3CU3E2__current_1;
	// Newtonsoft.Json.Linq.JProperty_JPropertyList Newtonsoft.Json.Linq.JProperty_JPropertyList_<GetEnumerator>d__1::<>4__this
	JPropertyList_tAC5DCFB03C7879ED6FCFD74977E76474C918200F * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__1_t74F96992BB129149414620E86F10CFDF4C199AF8, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__1_t74F96992BB129149414620E86F10CFDF4C199AF8, ___U3CU3E2__current_1)); }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__1_t74F96992BB129149414620E86F10CFDF4C199AF8, ___U3CU3E4__this_2)); }
	inline JPropertyList_tAC5DCFB03C7879ED6FCFD74977E76474C918200F * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline JPropertyList_tAC5DCFB03C7879ED6FCFD74977E76474C918200F ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(JPropertyList_tAC5DCFB03C7879ED6FCFD74977E76474C918200F * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETENUMERATORU3ED__1_T74F96992BB129149414620E86F10CFDF4C199AF8_H
#ifndef JTOKEN_TE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02_H
#define JTOKEN_TE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JToken
struct  JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02  : public RuntimeObject
{
public:
	// Newtonsoft.Json.Linq.JContainer Newtonsoft.Json.Linq.JToken::_parent
	JContainer_tF4CD2E574503C709DEF18A04B79B264B83746DAB * ____parent_0;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::_previous
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * ____previous_1;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::_next
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * ____next_2;
	// System.Object Newtonsoft.Json.Linq.JToken::_annotations
	RuntimeObject * ____annotations_3;

public:
	inline static int32_t get_offset_of__parent_0() { return static_cast<int32_t>(offsetof(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02, ____parent_0)); }
	inline JContainer_tF4CD2E574503C709DEF18A04B79B264B83746DAB * get__parent_0() const { return ____parent_0; }
	inline JContainer_tF4CD2E574503C709DEF18A04B79B264B83746DAB ** get_address_of__parent_0() { return &____parent_0; }
	inline void set__parent_0(JContainer_tF4CD2E574503C709DEF18A04B79B264B83746DAB * value)
	{
		____parent_0 = value;
		Il2CppCodeGenWriteBarrier((&____parent_0), value);
	}

	inline static int32_t get_offset_of__previous_1() { return static_cast<int32_t>(offsetof(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02, ____previous_1)); }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * get__previous_1() const { return ____previous_1; }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 ** get_address_of__previous_1() { return &____previous_1; }
	inline void set__previous_1(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * value)
	{
		____previous_1 = value;
		Il2CppCodeGenWriteBarrier((&____previous_1), value);
	}

	inline static int32_t get_offset_of__next_2() { return static_cast<int32_t>(offsetof(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02, ____next_2)); }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * get__next_2() const { return ____next_2; }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 ** get_address_of__next_2() { return &____next_2; }
	inline void set__next_2(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * value)
	{
		____next_2 = value;
		Il2CppCodeGenWriteBarrier((&____next_2), value);
	}

	inline static int32_t get_offset_of__annotations_3() { return static_cast<int32_t>(offsetof(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02, ____annotations_3)); }
	inline RuntimeObject * get__annotations_3() const { return ____annotations_3; }
	inline RuntimeObject ** get_address_of__annotations_3() { return &____annotations_3; }
	inline void set__annotations_3(RuntimeObject * value)
	{
		____annotations_3 = value;
		Il2CppCodeGenWriteBarrier((&____annotations_3), value);
	}
};

struct JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02_StaticFields
{
public:
	// Newtonsoft.Json.Linq.JTokenType[] Newtonsoft.Json.Linq.JToken::BooleanTypes
	JTokenTypeU5BU5D_t95EDBAB8E7D19E83A29F71593B6FEC1F54E17344* ___BooleanTypes_4;
	// Newtonsoft.Json.Linq.JTokenType[] Newtonsoft.Json.Linq.JToken::NumberTypes
	JTokenTypeU5BU5D_t95EDBAB8E7D19E83A29F71593B6FEC1F54E17344* ___NumberTypes_5;
	// Newtonsoft.Json.Linq.JTokenType[] Newtonsoft.Json.Linq.JToken::StringTypes
	JTokenTypeU5BU5D_t95EDBAB8E7D19E83A29F71593B6FEC1F54E17344* ___StringTypes_6;
	// Newtonsoft.Json.Linq.JTokenType[] Newtonsoft.Json.Linq.JToken::GuidTypes
	JTokenTypeU5BU5D_t95EDBAB8E7D19E83A29F71593B6FEC1F54E17344* ___GuidTypes_7;
	// Newtonsoft.Json.Linq.JTokenType[] Newtonsoft.Json.Linq.JToken::TimeSpanTypes
	JTokenTypeU5BU5D_t95EDBAB8E7D19E83A29F71593B6FEC1F54E17344* ___TimeSpanTypes_8;
	// Newtonsoft.Json.Linq.JTokenType[] Newtonsoft.Json.Linq.JToken::UriTypes
	JTokenTypeU5BU5D_t95EDBAB8E7D19E83A29F71593B6FEC1F54E17344* ___UriTypes_9;
	// Newtonsoft.Json.Linq.JTokenType[] Newtonsoft.Json.Linq.JToken::CharTypes
	JTokenTypeU5BU5D_t95EDBAB8E7D19E83A29F71593B6FEC1F54E17344* ___CharTypes_10;
	// Newtonsoft.Json.Linq.JTokenType[] Newtonsoft.Json.Linq.JToken::DateTimeTypes
	JTokenTypeU5BU5D_t95EDBAB8E7D19E83A29F71593B6FEC1F54E17344* ___DateTimeTypes_11;
	// Newtonsoft.Json.Linq.JTokenType[] Newtonsoft.Json.Linq.JToken::BytesTypes
	JTokenTypeU5BU5D_t95EDBAB8E7D19E83A29F71593B6FEC1F54E17344* ___BytesTypes_12;

public:
	inline static int32_t get_offset_of_BooleanTypes_4() { return static_cast<int32_t>(offsetof(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02_StaticFields, ___BooleanTypes_4)); }
	inline JTokenTypeU5BU5D_t95EDBAB8E7D19E83A29F71593B6FEC1F54E17344* get_BooleanTypes_4() const { return ___BooleanTypes_4; }
	inline JTokenTypeU5BU5D_t95EDBAB8E7D19E83A29F71593B6FEC1F54E17344** get_address_of_BooleanTypes_4() { return &___BooleanTypes_4; }
	inline void set_BooleanTypes_4(JTokenTypeU5BU5D_t95EDBAB8E7D19E83A29F71593B6FEC1F54E17344* value)
	{
		___BooleanTypes_4 = value;
		Il2CppCodeGenWriteBarrier((&___BooleanTypes_4), value);
	}

	inline static int32_t get_offset_of_NumberTypes_5() { return static_cast<int32_t>(offsetof(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02_StaticFields, ___NumberTypes_5)); }
	inline JTokenTypeU5BU5D_t95EDBAB8E7D19E83A29F71593B6FEC1F54E17344* get_NumberTypes_5() const { return ___NumberTypes_5; }
	inline JTokenTypeU5BU5D_t95EDBAB8E7D19E83A29F71593B6FEC1F54E17344** get_address_of_NumberTypes_5() { return &___NumberTypes_5; }
	inline void set_NumberTypes_5(JTokenTypeU5BU5D_t95EDBAB8E7D19E83A29F71593B6FEC1F54E17344* value)
	{
		___NumberTypes_5 = value;
		Il2CppCodeGenWriteBarrier((&___NumberTypes_5), value);
	}

	inline static int32_t get_offset_of_StringTypes_6() { return static_cast<int32_t>(offsetof(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02_StaticFields, ___StringTypes_6)); }
	inline JTokenTypeU5BU5D_t95EDBAB8E7D19E83A29F71593B6FEC1F54E17344* get_StringTypes_6() const { return ___StringTypes_6; }
	inline JTokenTypeU5BU5D_t95EDBAB8E7D19E83A29F71593B6FEC1F54E17344** get_address_of_StringTypes_6() { return &___StringTypes_6; }
	inline void set_StringTypes_6(JTokenTypeU5BU5D_t95EDBAB8E7D19E83A29F71593B6FEC1F54E17344* value)
	{
		___StringTypes_6 = value;
		Il2CppCodeGenWriteBarrier((&___StringTypes_6), value);
	}

	inline static int32_t get_offset_of_GuidTypes_7() { return static_cast<int32_t>(offsetof(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02_StaticFields, ___GuidTypes_7)); }
	inline JTokenTypeU5BU5D_t95EDBAB8E7D19E83A29F71593B6FEC1F54E17344* get_GuidTypes_7() const { return ___GuidTypes_7; }
	inline JTokenTypeU5BU5D_t95EDBAB8E7D19E83A29F71593B6FEC1F54E17344** get_address_of_GuidTypes_7() { return &___GuidTypes_7; }
	inline void set_GuidTypes_7(JTokenTypeU5BU5D_t95EDBAB8E7D19E83A29F71593B6FEC1F54E17344* value)
	{
		___GuidTypes_7 = value;
		Il2CppCodeGenWriteBarrier((&___GuidTypes_7), value);
	}

	inline static int32_t get_offset_of_TimeSpanTypes_8() { return static_cast<int32_t>(offsetof(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02_StaticFields, ___TimeSpanTypes_8)); }
	inline JTokenTypeU5BU5D_t95EDBAB8E7D19E83A29F71593B6FEC1F54E17344* get_TimeSpanTypes_8() const { return ___TimeSpanTypes_8; }
	inline JTokenTypeU5BU5D_t95EDBAB8E7D19E83A29F71593B6FEC1F54E17344** get_address_of_TimeSpanTypes_8() { return &___TimeSpanTypes_8; }
	inline void set_TimeSpanTypes_8(JTokenTypeU5BU5D_t95EDBAB8E7D19E83A29F71593B6FEC1F54E17344* value)
	{
		___TimeSpanTypes_8 = value;
		Il2CppCodeGenWriteBarrier((&___TimeSpanTypes_8), value);
	}

	inline static int32_t get_offset_of_UriTypes_9() { return static_cast<int32_t>(offsetof(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02_StaticFields, ___UriTypes_9)); }
	inline JTokenTypeU5BU5D_t95EDBAB8E7D19E83A29F71593B6FEC1F54E17344* get_UriTypes_9() const { return ___UriTypes_9; }
	inline JTokenTypeU5BU5D_t95EDBAB8E7D19E83A29F71593B6FEC1F54E17344** get_address_of_UriTypes_9() { return &___UriTypes_9; }
	inline void set_UriTypes_9(JTokenTypeU5BU5D_t95EDBAB8E7D19E83A29F71593B6FEC1F54E17344* value)
	{
		___UriTypes_9 = value;
		Il2CppCodeGenWriteBarrier((&___UriTypes_9), value);
	}

	inline static int32_t get_offset_of_CharTypes_10() { return static_cast<int32_t>(offsetof(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02_StaticFields, ___CharTypes_10)); }
	inline JTokenTypeU5BU5D_t95EDBAB8E7D19E83A29F71593B6FEC1F54E17344* get_CharTypes_10() const { return ___CharTypes_10; }
	inline JTokenTypeU5BU5D_t95EDBAB8E7D19E83A29F71593B6FEC1F54E17344** get_address_of_CharTypes_10() { return &___CharTypes_10; }
	inline void set_CharTypes_10(JTokenTypeU5BU5D_t95EDBAB8E7D19E83A29F71593B6FEC1F54E17344* value)
	{
		___CharTypes_10 = value;
		Il2CppCodeGenWriteBarrier((&___CharTypes_10), value);
	}

	inline static int32_t get_offset_of_DateTimeTypes_11() { return static_cast<int32_t>(offsetof(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02_StaticFields, ___DateTimeTypes_11)); }
	inline JTokenTypeU5BU5D_t95EDBAB8E7D19E83A29F71593B6FEC1F54E17344* get_DateTimeTypes_11() const { return ___DateTimeTypes_11; }
	inline JTokenTypeU5BU5D_t95EDBAB8E7D19E83A29F71593B6FEC1F54E17344** get_address_of_DateTimeTypes_11() { return &___DateTimeTypes_11; }
	inline void set_DateTimeTypes_11(JTokenTypeU5BU5D_t95EDBAB8E7D19E83A29F71593B6FEC1F54E17344* value)
	{
		___DateTimeTypes_11 = value;
		Il2CppCodeGenWriteBarrier((&___DateTimeTypes_11), value);
	}

	inline static int32_t get_offset_of_BytesTypes_12() { return static_cast<int32_t>(offsetof(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02_StaticFields, ___BytesTypes_12)); }
	inline JTokenTypeU5BU5D_t95EDBAB8E7D19E83A29F71593B6FEC1F54E17344* get_BytesTypes_12() const { return ___BytesTypes_12; }
	inline JTokenTypeU5BU5D_t95EDBAB8E7D19E83A29F71593B6FEC1F54E17344** get_address_of_BytesTypes_12() { return &___BytesTypes_12; }
	inline void set_BytesTypes_12(JTokenTypeU5BU5D_t95EDBAB8E7D19E83A29F71593B6FEC1F54E17344* value)
	{
		___BytesTypes_12 = value;
		Il2CppCodeGenWriteBarrier((&___BytesTypes_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JTOKEN_TE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02_H
#ifndef U3CAFTERSELFU3ED__42_T195C851E6A37082B7270B7533D256D1F4055BD94_H
#define U3CAFTERSELFU3ED__42_T195C851E6A37082B7270B7533D256D1F4055BD94_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JToken_<AfterSelf>d__42
struct  U3CAfterSelfU3Ed__42_t195C851E6A37082B7270B7533D256D1F4055BD94  : public RuntimeObject
{
public:
	// System.Int32 Newtonsoft.Json.Linq.JToken_<AfterSelf>d__42::<>1__state
	int32_t ___U3CU3E1__state_0;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken_<AfterSelf>d__42::<>2__current
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * ___U3CU3E2__current_1;
	// System.Int32 Newtonsoft.Json.Linq.JToken_<AfterSelf>d__42::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken_<AfterSelf>d__42::<>4__this
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * ___U3CU3E4__this_3;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken_<AfterSelf>d__42::<o>5__1
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * ___U3CoU3E5__1_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CAfterSelfU3Ed__42_t195C851E6A37082B7270B7533D256D1F4055BD94, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CAfterSelfU3Ed__42_t195C851E6A37082B7270B7533D256D1F4055BD94, ___U3CU3E2__current_1)); }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CAfterSelfU3Ed__42_t195C851E6A37082B7270B7533D256D1F4055BD94, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CAfterSelfU3Ed__42_t195C851E6A37082B7270B7533D256D1F4055BD94, ___U3CU3E4__this_3)); }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_U3CoU3E5__1_4() { return static_cast<int32_t>(offsetof(U3CAfterSelfU3Ed__42_t195C851E6A37082B7270B7533D256D1F4055BD94, ___U3CoU3E5__1_4)); }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * get_U3CoU3E5__1_4() const { return ___U3CoU3E5__1_4; }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 ** get_address_of_U3CoU3E5__1_4() { return &___U3CoU3E5__1_4; }
	inline void set_U3CoU3E5__1_4(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * value)
	{
		___U3CoU3E5__1_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CoU3E5__1_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CAFTERSELFU3ED__42_T195C851E6A37082B7270B7533D256D1F4055BD94_H
#ifndef U3CANNOTATIONSU3ED__172_T556B0F18E6073D7ED7C30446020DA51153E7931D_H
#define U3CANNOTATIONSU3ED__172_T556B0F18E6073D7ED7C30446020DA51153E7931D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JToken_<Annotations>d__172
struct  U3CAnnotationsU3Ed__172_t556B0F18E6073D7ED7C30446020DA51153E7931D  : public RuntimeObject
{
public:
	// System.Int32 Newtonsoft.Json.Linq.JToken_<Annotations>d__172::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Newtonsoft.Json.Linq.JToken_<Annotations>d__172::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Int32 Newtonsoft.Json.Linq.JToken_<Annotations>d__172::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// System.Type Newtonsoft.Json.Linq.JToken_<Annotations>d__172::type
	Type_t * ___type_3;
	// System.Type Newtonsoft.Json.Linq.JToken_<Annotations>d__172::<>3__type
	Type_t * ___U3CU3E3__type_4;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken_<Annotations>d__172::<>4__this
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * ___U3CU3E4__this_5;
	// System.Object[] Newtonsoft.Json.Linq.JToken_<Annotations>d__172::<annotations>5__1
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___U3CannotationsU3E5__1_6;
	// System.Int32 Newtonsoft.Json.Linq.JToken_<Annotations>d__172::<i>5__2
	int32_t ___U3CiU3E5__2_7;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CAnnotationsU3Ed__172_t556B0F18E6073D7ED7C30446020DA51153E7931D, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CAnnotationsU3Ed__172_t556B0F18E6073D7ED7C30446020DA51153E7931D, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CAnnotationsU3Ed__172_t556B0F18E6073D7ED7C30446020DA51153E7931D, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(U3CAnnotationsU3Ed__172_t556B0F18E6073D7ED7C30446020DA51153E7931D, ___type_3)); }
	inline Type_t * get_type_3() const { return ___type_3; }
	inline Type_t ** get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(Type_t * value)
	{
		___type_3 = value;
		Il2CppCodeGenWriteBarrier((&___type_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__type_4() { return static_cast<int32_t>(offsetof(U3CAnnotationsU3Ed__172_t556B0F18E6073D7ED7C30446020DA51153E7931D, ___U3CU3E3__type_4)); }
	inline Type_t * get_U3CU3E3__type_4() const { return ___U3CU3E3__type_4; }
	inline Type_t ** get_address_of_U3CU3E3__type_4() { return &___U3CU3E3__type_4; }
	inline void set_U3CU3E3__type_4(Type_t * value)
	{
		___U3CU3E3__type_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__type_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_5() { return static_cast<int32_t>(offsetof(U3CAnnotationsU3Ed__172_t556B0F18E6073D7ED7C30446020DA51153E7931D, ___U3CU3E4__this_5)); }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * get_U3CU3E4__this_5() const { return ___U3CU3E4__this_5; }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 ** get_address_of_U3CU3E4__this_5() { return &___U3CU3E4__this_5; }
	inline void set_U3CU3E4__this_5(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * value)
	{
		___U3CU3E4__this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_5), value);
	}

	inline static int32_t get_offset_of_U3CannotationsU3E5__1_6() { return static_cast<int32_t>(offsetof(U3CAnnotationsU3Ed__172_t556B0F18E6073D7ED7C30446020DA51153E7931D, ___U3CannotationsU3E5__1_6)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_U3CannotationsU3E5__1_6() const { return ___U3CannotationsU3E5__1_6; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_U3CannotationsU3E5__1_6() { return &___U3CannotationsU3E5__1_6; }
	inline void set_U3CannotationsU3E5__1_6(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___U3CannotationsU3E5__1_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CannotationsU3E5__1_6), value);
	}

	inline static int32_t get_offset_of_U3CiU3E5__2_7() { return static_cast<int32_t>(offsetof(U3CAnnotationsU3Ed__172_t556B0F18E6073D7ED7C30446020DA51153E7931D, ___U3CiU3E5__2_7)); }
	inline int32_t get_U3CiU3E5__2_7() const { return ___U3CiU3E5__2_7; }
	inline int32_t* get_address_of_U3CiU3E5__2_7() { return &___U3CiU3E5__2_7; }
	inline void set_U3CiU3E5__2_7(int32_t value)
	{
		___U3CiU3E5__2_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CANNOTATIONSU3ED__172_T556B0F18E6073D7ED7C30446020DA51153E7931D_H
#ifndef U3CBEFORESELFU3ED__43_T6CF3F6C7954C3E7EE291671EB2B9422DC3D3F1B4_H
#define U3CBEFORESELFU3ED__43_T6CF3F6C7954C3E7EE291671EB2B9422DC3D3F1B4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JToken_<BeforeSelf>d__43
struct  U3CBeforeSelfU3Ed__43_t6CF3F6C7954C3E7EE291671EB2B9422DC3D3F1B4  : public RuntimeObject
{
public:
	// System.Int32 Newtonsoft.Json.Linq.JToken_<BeforeSelf>d__43::<>1__state
	int32_t ___U3CU3E1__state_0;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken_<BeforeSelf>d__43::<>2__current
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * ___U3CU3E2__current_1;
	// System.Int32 Newtonsoft.Json.Linq.JToken_<BeforeSelf>d__43::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken_<BeforeSelf>d__43::<>4__this
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * ___U3CU3E4__this_3;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken_<BeforeSelf>d__43::<o>5__1
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * ___U3CoU3E5__1_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CBeforeSelfU3Ed__43_t6CF3F6C7954C3E7EE291671EB2B9422DC3D3F1B4, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CBeforeSelfU3Ed__43_t6CF3F6C7954C3E7EE291671EB2B9422DC3D3F1B4, ___U3CU3E2__current_1)); }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CBeforeSelfU3Ed__43_t6CF3F6C7954C3E7EE291671EB2B9422DC3D3F1B4, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CBeforeSelfU3Ed__43_t6CF3F6C7954C3E7EE291671EB2B9422DC3D3F1B4, ___U3CU3E4__this_3)); }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_U3CoU3E5__1_4() { return static_cast<int32_t>(offsetof(U3CBeforeSelfU3Ed__43_t6CF3F6C7954C3E7EE291671EB2B9422DC3D3F1B4, ___U3CoU3E5__1_4)); }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * get_U3CoU3E5__1_4() const { return ___U3CoU3E5__1_4; }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 ** get_address_of_U3CoU3E5__1_4() { return &___U3CoU3E5__1_4; }
	inline void set_U3CoU3E5__1_4(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * value)
	{
		___U3CoU3E5__1_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CoU3E5__1_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CBEFORESELFU3ED__43_T6CF3F6C7954C3E7EE291671EB2B9422DC3D3F1B4_H
#ifndef U3CGETANCESTORSU3ED__41_TA86F8A2E067567D67E95D02FD20F096E7F14B840_H
#define U3CGETANCESTORSU3ED__41_TA86F8A2E067567D67E95D02FD20F096E7F14B840_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JToken_<GetAncestors>d__41
struct  U3CGetAncestorsU3Ed__41_tA86F8A2E067567D67E95D02FD20F096E7F14B840  : public RuntimeObject
{
public:
	// System.Int32 Newtonsoft.Json.Linq.JToken_<GetAncestors>d__41::<>1__state
	int32_t ___U3CU3E1__state_0;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken_<GetAncestors>d__41::<>2__current
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * ___U3CU3E2__current_1;
	// System.Int32 Newtonsoft.Json.Linq.JToken_<GetAncestors>d__41::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// System.Boolean Newtonsoft.Json.Linq.JToken_<GetAncestors>d__41::self
	bool ___self_3;
	// System.Boolean Newtonsoft.Json.Linq.JToken_<GetAncestors>d__41::<>3__self
	bool ___U3CU3E3__self_4;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken_<GetAncestors>d__41::<>4__this
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * ___U3CU3E4__this_5;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken_<GetAncestors>d__41::<current>5__1
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * ___U3CcurrentU3E5__1_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetAncestorsU3Ed__41_tA86F8A2E067567D67E95D02FD20F096E7F14B840, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetAncestorsU3Ed__41_tA86F8A2E067567D67E95D02FD20F096E7F14B840, ___U3CU3E2__current_1)); }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CGetAncestorsU3Ed__41_tA86F8A2E067567D67E95D02FD20F096E7F14B840, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_self_3() { return static_cast<int32_t>(offsetof(U3CGetAncestorsU3Ed__41_tA86F8A2E067567D67E95D02FD20F096E7F14B840, ___self_3)); }
	inline bool get_self_3() const { return ___self_3; }
	inline bool* get_address_of_self_3() { return &___self_3; }
	inline void set_self_3(bool value)
	{
		___self_3 = value;
	}

	inline static int32_t get_offset_of_U3CU3E3__self_4() { return static_cast<int32_t>(offsetof(U3CGetAncestorsU3Ed__41_tA86F8A2E067567D67E95D02FD20F096E7F14B840, ___U3CU3E3__self_4)); }
	inline bool get_U3CU3E3__self_4() const { return ___U3CU3E3__self_4; }
	inline bool* get_address_of_U3CU3E3__self_4() { return &___U3CU3E3__self_4; }
	inline void set_U3CU3E3__self_4(bool value)
	{
		___U3CU3E3__self_4 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_5() { return static_cast<int32_t>(offsetof(U3CGetAncestorsU3Ed__41_tA86F8A2E067567D67E95D02FD20F096E7F14B840, ___U3CU3E4__this_5)); }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * get_U3CU3E4__this_5() const { return ___U3CU3E4__this_5; }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 ** get_address_of_U3CU3E4__this_5() { return &___U3CU3E4__this_5; }
	inline void set_U3CU3E4__this_5(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * value)
	{
		___U3CU3E4__this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_5), value);
	}

	inline static int32_t get_offset_of_U3CcurrentU3E5__1_6() { return static_cast<int32_t>(offsetof(U3CGetAncestorsU3Ed__41_tA86F8A2E067567D67E95D02FD20F096E7F14B840, ___U3CcurrentU3E5__1_6)); }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * get_U3CcurrentU3E5__1_6() const { return ___U3CcurrentU3E5__1_6; }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 ** get_address_of_U3CcurrentU3E5__1_6() { return &___U3CcurrentU3E5__1_6; }
	inline void set_U3CcurrentU3E5__1_6(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * value)
	{
		___U3CcurrentU3E5__1_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcurrentU3E5__1_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETANCESTORSU3ED__41_TA86F8A2E067567D67E95D02FD20F096E7F14B840_H
#ifndef LINEINFOANNOTATION_T275C937C347C8A3C93FFD9CAC915903CDEDDF40D_H
#define LINEINFOANNOTATION_T275C937C347C8A3C93FFD9CAC915903CDEDDF40D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JToken_LineInfoAnnotation
struct  LineInfoAnnotation_t275C937C347C8A3C93FFD9CAC915903CDEDDF40D  : public RuntimeObject
{
public:
	// System.Int32 Newtonsoft.Json.Linq.JToken_LineInfoAnnotation::LineNumber
	int32_t ___LineNumber_0;
	// System.Int32 Newtonsoft.Json.Linq.JToken_LineInfoAnnotation::LinePosition
	int32_t ___LinePosition_1;

public:
	inline static int32_t get_offset_of_LineNumber_0() { return static_cast<int32_t>(offsetof(LineInfoAnnotation_t275C937C347C8A3C93FFD9CAC915903CDEDDF40D, ___LineNumber_0)); }
	inline int32_t get_LineNumber_0() const { return ___LineNumber_0; }
	inline int32_t* get_address_of_LineNumber_0() { return &___LineNumber_0; }
	inline void set_LineNumber_0(int32_t value)
	{
		___LineNumber_0 = value;
	}

	inline static int32_t get_offset_of_LinePosition_1() { return static_cast<int32_t>(offsetof(LineInfoAnnotation_t275C937C347C8A3C93FFD9CAC915903CDEDDF40D, ___LinePosition_1)); }
	inline int32_t get_LinePosition_1() const { return ___LinePosition_1; }
	inline int32_t* get_address_of_LinePosition_1() { return &___LinePosition_1; }
	inline void set_LinePosition_1(int32_t value)
	{
		___LinePosition_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINEINFOANNOTATION_T275C937C347C8A3C93FFD9CAC915903CDEDDF40D_H
#ifndef U3CEXECUTEFILTERU3ED__4_T49283F96CCAA85028EA2C8F9402DA5D35998E1D7_H
#define U3CEXECUTEFILTERU3ED__4_T49283F96CCAA85028EA2C8F9402DA5D35998E1D7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JsonPath.ArrayIndexFilter_<ExecuteFilter>d__4
struct  U3CExecuteFilterU3Ed__4_t49283F96CCAA85028EA2C8F9402DA5D35998E1D7  : public RuntimeObject
{
public:
	// System.Int32 Newtonsoft.Json.Linq.JsonPath.ArrayIndexFilter_<ExecuteFilter>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JsonPath.ArrayIndexFilter_<ExecuteFilter>d__4::<>2__current
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * ___U3CU3E2__current_1;
	// System.Int32 Newtonsoft.Json.Linq.JsonPath.ArrayIndexFilter_<ExecuteFilter>d__4::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JsonPath.ArrayIndexFilter_<ExecuteFilter>d__4::current
	RuntimeObject* ___current_3;
	// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JsonPath.ArrayIndexFilter_<ExecuteFilter>d__4::<>3__current
	RuntimeObject* ___U3CU3E3__current_4;
	// Newtonsoft.Json.Linq.JsonPath.ArrayIndexFilter Newtonsoft.Json.Linq.JsonPath.ArrayIndexFilter_<ExecuteFilter>d__4::<>4__this
	ArrayIndexFilter_t4B8AB90AF78B3976E09BEA34466153A1CDE2E12B * ___U3CU3E4__this_5;
	// System.Boolean Newtonsoft.Json.Linq.JsonPath.ArrayIndexFilter_<ExecuteFilter>d__4::errorWhenNoMatch
	bool ___errorWhenNoMatch_6;
	// System.Boolean Newtonsoft.Json.Linq.JsonPath.ArrayIndexFilter_<ExecuteFilter>d__4::<>3__errorWhenNoMatch
	bool ___U3CU3E3__errorWhenNoMatch_7;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JsonPath.ArrayIndexFilter_<ExecuteFilter>d__4::<t>5__1
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * ___U3CtU3E5__1_8;
	// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JsonPath.ArrayIndexFilter_<ExecuteFilter>d__4::<>7__wrap1
	RuntimeObject* ___U3CU3E7__wrap1_9;
	// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JsonPath.ArrayIndexFilter_<ExecuteFilter>d__4::<>7__wrap2
	RuntimeObject* ___U3CU3E7__wrap2_10;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t49283F96CCAA85028EA2C8F9402DA5D35998E1D7, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t49283F96CCAA85028EA2C8F9402DA5D35998E1D7, ___U3CU3E2__current_1)); }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t49283F96CCAA85028EA2C8F9402DA5D35998E1D7, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t49283F96CCAA85028EA2C8F9402DA5D35998E1D7, ___current_3)); }
	inline RuntimeObject* get_current_3() const { return ___current_3; }
	inline RuntimeObject** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject* value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__current_4() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t49283F96CCAA85028EA2C8F9402DA5D35998E1D7, ___U3CU3E3__current_4)); }
	inline RuntimeObject* get_U3CU3E3__current_4() const { return ___U3CU3E3__current_4; }
	inline RuntimeObject** get_address_of_U3CU3E3__current_4() { return &___U3CU3E3__current_4; }
	inline void set_U3CU3E3__current_4(RuntimeObject* value)
	{
		___U3CU3E3__current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__current_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_5() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t49283F96CCAA85028EA2C8F9402DA5D35998E1D7, ___U3CU3E4__this_5)); }
	inline ArrayIndexFilter_t4B8AB90AF78B3976E09BEA34466153A1CDE2E12B * get_U3CU3E4__this_5() const { return ___U3CU3E4__this_5; }
	inline ArrayIndexFilter_t4B8AB90AF78B3976E09BEA34466153A1CDE2E12B ** get_address_of_U3CU3E4__this_5() { return &___U3CU3E4__this_5; }
	inline void set_U3CU3E4__this_5(ArrayIndexFilter_t4B8AB90AF78B3976E09BEA34466153A1CDE2E12B * value)
	{
		___U3CU3E4__this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_5), value);
	}

	inline static int32_t get_offset_of_errorWhenNoMatch_6() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t49283F96CCAA85028EA2C8F9402DA5D35998E1D7, ___errorWhenNoMatch_6)); }
	inline bool get_errorWhenNoMatch_6() const { return ___errorWhenNoMatch_6; }
	inline bool* get_address_of_errorWhenNoMatch_6() { return &___errorWhenNoMatch_6; }
	inline void set_errorWhenNoMatch_6(bool value)
	{
		___errorWhenNoMatch_6 = value;
	}

	inline static int32_t get_offset_of_U3CU3E3__errorWhenNoMatch_7() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t49283F96CCAA85028EA2C8F9402DA5D35998E1D7, ___U3CU3E3__errorWhenNoMatch_7)); }
	inline bool get_U3CU3E3__errorWhenNoMatch_7() const { return ___U3CU3E3__errorWhenNoMatch_7; }
	inline bool* get_address_of_U3CU3E3__errorWhenNoMatch_7() { return &___U3CU3E3__errorWhenNoMatch_7; }
	inline void set_U3CU3E3__errorWhenNoMatch_7(bool value)
	{
		___U3CU3E3__errorWhenNoMatch_7 = value;
	}

	inline static int32_t get_offset_of_U3CtU3E5__1_8() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t49283F96CCAA85028EA2C8F9402DA5D35998E1D7, ___U3CtU3E5__1_8)); }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * get_U3CtU3E5__1_8() const { return ___U3CtU3E5__1_8; }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 ** get_address_of_U3CtU3E5__1_8() { return &___U3CtU3E5__1_8; }
	inline void set_U3CtU3E5__1_8(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * value)
	{
		___U3CtU3E5__1_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtU3E5__1_8), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_9() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t49283F96CCAA85028EA2C8F9402DA5D35998E1D7, ___U3CU3E7__wrap1_9)); }
	inline RuntimeObject* get_U3CU3E7__wrap1_9() const { return ___U3CU3E7__wrap1_9; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap1_9() { return &___U3CU3E7__wrap1_9; }
	inline void set_U3CU3E7__wrap1_9(RuntimeObject* value)
	{
		___U3CU3E7__wrap1_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap1_9), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap2_10() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t49283F96CCAA85028EA2C8F9402DA5D35998E1D7, ___U3CU3E7__wrap2_10)); }
	inline RuntimeObject* get_U3CU3E7__wrap2_10() const { return ___U3CU3E7__wrap2_10; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap2_10() { return &___U3CU3E7__wrap2_10; }
	inline void set_U3CU3E7__wrap2_10(RuntimeObject* value)
	{
		___U3CU3E7__wrap2_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap2_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CEXECUTEFILTERU3ED__4_T49283F96CCAA85028EA2C8F9402DA5D35998E1D7_H
#ifndef U3CEXECUTEFILTERU3ED__12_TBCB91730C7554E4E134042CDDD1550F32438D3F8_H
#define U3CEXECUTEFILTERU3ED__12_TBCB91730C7554E4E134042CDDD1550F32438D3F8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JsonPath.ArraySliceFilter_<ExecuteFilter>d__12
struct  U3CExecuteFilterU3Ed__12_tBCB91730C7554E4E134042CDDD1550F32438D3F8  : public RuntimeObject
{
public:
	// System.Int32 Newtonsoft.Json.Linq.JsonPath.ArraySliceFilter_<ExecuteFilter>d__12::<>1__state
	int32_t ___U3CU3E1__state_0;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JsonPath.ArraySliceFilter_<ExecuteFilter>d__12::<>2__current
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * ___U3CU3E2__current_1;
	// System.Int32 Newtonsoft.Json.Linq.JsonPath.ArraySliceFilter_<ExecuteFilter>d__12::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// Newtonsoft.Json.Linq.JsonPath.ArraySliceFilter Newtonsoft.Json.Linq.JsonPath.ArraySliceFilter_<ExecuteFilter>d__12::<>4__this
	ArraySliceFilter_tC5035758E75DB3EF9EAA182D561A726F46397BE3 * ___U3CU3E4__this_3;
	// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JsonPath.ArraySliceFilter_<ExecuteFilter>d__12::current
	RuntimeObject* ___current_4;
	// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JsonPath.ArraySliceFilter_<ExecuteFilter>d__12::<>3__current
	RuntimeObject* ___U3CU3E3__current_5;
	// Newtonsoft.Json.Linq.JArray Newtonsoft.Json.Linq.JsonPath.ArraySliceFilter_<ExecuteFilter>d__12::<a>5__1
	JArray_t1CE13821116F9B501573275C6BDD9FB254E65F11 * ___U3CaU3E5__1_6;
	// System.Int32 Newtonsoft.Json.Linq.JsonPath.ArraySliceFilter_<ExecuteFilter>d__12::<i>5__2
	int32_t ___U3CiU3E5__2_7;
	// System.Int32 Newtonsoft.Json.Linq.JsonPath.ArraySliceFilter_<ExecuteFilter>d__12::<stepCount>5__3
	int32_t ___U3CstepCountU3E5__3_8;
	// System.Int32 Newtonsoft.Json.Linq.JsonPath.ArraySliceFilter_<ExecuteFilter>d__12::<stopIndex>5__4
	int32_t ___U3CstopIndexU3E5__4_9;
	// System.Boolean Newtonsoft.Json.Linq.JsonPath.ArraySliceFilter_<ExecuteFilter>d__12::<positiveStep>5__5
	bool ___U3CpositiveStepU3E5__5_10;
	// System.Boolean Newtonsoft.Json.Linq.JsonPath.ArraySliceFilter_<ExecuteFilter>d__12::errorWhenNoMatch
	bool ___errorWhenNoMatch_11;
	// System.Boolean Newtonsoft.Json.Linq.JsonPath.ArraySliceFilter_<ExecuteFilter>d__12::<>3__errorWhenNoMatch
	bool ___U3CU3E3__errorWhenNoMatch_12;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JsonPath.ArraySliceFilter_<ExecuteFilter>d__12::<t>5__6
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * ___U3CtU3E5__6_13;
	// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JsonPath.ArraySliceFilter_<ExecuteFilter>d__12::<>7__wrap1
	RuntimeObject* ___U3CU3E7__wrap1_14;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__12_tBCB91730C7554E4E134042CDDD1550F32438D3F8, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__12_tBCB91730C7554E4E134042CDDD1550F32438D3F8, ___U3CU3E2__current_1)); }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__12_tBCB91730C7554E4E134042CDDD1550F32438D3F8, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__12_tBCB91730C7554E4E134042CDDD1550F32438D3F8, ___U3CU3E4__this_3)); }
	inline ArraySliceFilter_tC5035758E75DB3EF9EAA182D561A726F46397BE3 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline ArraySliceFilter_tC5035758E75DB3EF9EAA182D561A726F46397BE3 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(ArraySliceFilter_tC5035758E75DB3EF9EAA182D561A726F46397BE3 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_current_4() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__12_tBCB91730C7554E4E134042CDDD1550F32438D3F8, ___current_4)); }
	inline RuntimeObject* get_current_4() const { return ___current_4; }
	inline RuntimeObject** get_address_of_current_4() { return &___current_4; }
	inline void set_current_4(RuntimeObject* value)
	{
		___current_4 = value;
		Il2CppCodeGenWriteBarrier((&___current_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__current_5() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__12_tBCB91730C7554E4E134042CDDD1550F32438D3F8, ___U3CU3E3__current_5)); }
	inline RuntimeObject* get_U3CU3E3__current_5() const { return ___U3CU3E3__current_5; }
	inline RuntimeObject** get_address_of_U3CU3E3__current_5() { return &___U3CU3E3__current_5; }
	inline void set_U3CU3E3__current_5(RuntimeObject* value)
	{
		___U3CU3E3__current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__current_5), value);
	}

	inline static int32_t get_offset_of_U3CaU3E5__1_6() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__12_tBCB91730C7554E4E134042CDDD1550F32438D3F8, ___U3CaU3E5__1_6)); }
	inline JArray_t1CE13821116F9B501573275C6BDD9FB254E65F11 * get_U3CaU3E5__1_6() const { return ___U3CaU3E5__1_6; }
	inline JArray_t1CE13821116F9B501573275C6BDD9FB254E65F11 ** get_address_of_U3CaU3E5__1_6() { return &___U3CaU3E5__1_6; }
	inline void set_U3CaU3E5__1_6(JArray_t1CE13821116F9B501573275C6BDD9FB254E65F11 * value)
	{
		___U3CaU3E5__1_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CaU3E5__1_6), value);
	}

	inline static int32_t get_offset_of_U3CiU3E5__2_7() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__12_tBCB91730C7554E4E134042CDDD1550F32438D3F8, ___U3CiU3E5__2_7)); }
	inline int32_t get_U3CiU3E5__2_7() const { return ___U3CiU3E5__2_7; }
	inline int32_t* get_address_of_U3CiU3E5__2_7() { return &___U3CiU3E5__2_7; }
	inline void set_U3CiU3E5__2_7(int32_t value)
	{
		___U3CiU3E5__2_7 = value;
	}

	inline static int32_t get_offset_of_U3CstepCountU3E5__3_8() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__12_tBCB91730C7554E4E134042CDDD1550F32438D3F8, ___U3CstepCountU3E5__3_8)); }
	inline int32_t get_U3CstepCountU3E5__3_8() const { return ___U3CstepCountU3E5__3_8; }
	inline int32_t* get_address_of_U3CstepCountU3E5__3_8() { return &___U3CstepCountU3E5__3_8; }
	inline void set_U3CstepCountU3E5__3_8(int32_t value)
	{
		___U3CstepCountU3E5__3_8 = value;
	}

	inline static int32_t get_offset_of_U3CstopIndexU3E5__4_9() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__12_tBCB91730C7554E4E134042CDDD1550F32438D3F8, ___U3CstopIndexU3E5__4_9)); }
	inline int32_t get_U3CstopIndexU3E5__4_9() const { return ___U3CstopIndexU3E5__4_9; }
	inline int32_t* get_address_of_U3CstopIndexU3E5__4_9() { return &___U3CstopIndexU3E5__4_9; }
	inline void set_U3CstopIndexU3E5__4_9(int32_t value)
	{
		___U3CstopIndexU3E5__4_9 = value;
	}

	inline static int32_t get_offset_of_U3CpositiveStepU3E5__5_10() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__12_tBCB91730C7554E4E134042CDDD1550F32438D3F8, ___U3CpositiveStepU3E5__5_10)); }
	inline bool get_U3CpositiveStepU3E5__5_10() const { return ___U3CpositiveStepU3E5__5_10; }
	inline bool* get_address_of_U3CpositiveStepU3E5__5_10() { return &___U3CpositiveStepU3E5__5_10; }
	inline void set_U3CpositiveStepU3E5__5_10(bool value)
	{
		___U3CpositiveStepU3E5__5_10 = value;
	}

	inline static int32_t get_offset_of_errorWhenNoMatch_11() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__12_tBCB91730C7554E4E134042CDDD1550F32438D3F8, ___errorWhenNoMatch_11)); }
	inline bool get_errorWhenNoMatch_11() const { return ___errorWhenNoMatch_11; }
	inline bool* get_address_of_errorWhenNoMatch_11() { return &___errorWhenNoMatch_11; }
	inline void set_errorWhenNoMatch_11(bool value)
	{
		___errorWhenNoMatch_11 = value;
	}

	inline static int32_t get_offset_of_U3CU3E3__errorWhenNoMatch_12() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__12_tBCB91730C7554E4E134042CDDD1550F32438D3F8, ___U3CU3E3__errorWhenNoMatch_12)); }
	inline bool get_U3CU3E3__errorWhenNoMatch_12() const { return ___U3CU3E3__errorWhenNoMatch_12; }
	inline bool* get_address_of_U3CU3E3__errorWhenNoMatch_12() { return &___U3CU3E3__errorWhenNoMatch_12; }
	inline void set_U3CU3E3__errorWhenNoMatch_12(bool value)
	{
		___U3CU3E3__errorWhenNoMatch_12 = value;
	}

	inline static int32_t get_offset_of_U3CtU3E5__6_13() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__12_tBCB91730C7554E4E134042CDDD1550F32438D3F8, ___U3CtU3E5__6_13)); }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * get_U3CtU3E5__6_13() const { return ___U3CtU3E5__6_13; }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 ** get_address_of_U3CtU3E5__6_13() { return &___U3CtU3E5__6_13; }
	inline void set_U3CtU3E5__6_13(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * value)
	{
		___U3CtU3E5__6_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtU3E5__6_13), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_14() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__12_tBCB91730C7554E4E134042CDDD1550F32438D3F8, ___U3CU3E7__wrap1_14)); }
	inline RuntimeObject* get_U3CU3E7__wrap1_14() const { return ___U3CU3E7__wrap1_14; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap1_14() { return &___U3CU3E7__wrap1_14; }
	inline void set_U3CU3E7__wrap1_14(RuntimeObject* value)
	{
		___U3CU3E7__wrap1_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap1_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CEXECUTEFILTERU3ED__12_TBCB91730C7554E4E134042CDDD1550F32438D3F8_H
#ifndef U3CEXECUTEFILTERU3ED__4_TAF0C3B976817618E87E3152BBD9B3A1DF5D92610_H
#define U3CEXECUTEFILTERU3ED__4_TAF0C3B976817618E87E3152BBD9B3A1DF5D92610_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JsonPath.FieldFilter_<ExecuteFilter>d__4
struct  U3CExecuteFilterU3Ed__4_tAF0C3B976817618E87E3152BBD9B3A1DF5D92610  : public RuntimeObject
{
public:
	// System.Int32 Newtonsoft.Json.Linq.JsonPath.FieldFilter_<ExecuteFilter>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JsonPath.FieldFilter_<ExecuteFilter>d__4::<>2__current
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * ___U3CU3E2__current_1;
	// System.Int32 Newtonsoft.Json.Linq.JsonPath.FieldFilter_<ExecuteFilter>d__4::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JsonPath.FieldFilter_<ExecuteFilter>d__4::current
	RuntimeObject* ___current_3;
	// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JsonPath.FieldFilter_<ExecuteFilter>d__4::<>3__current
	RuntimeObject* ___U3CU3E3__current_4;
	// Newtonsoft.Json.Linq.JsonPath.FieldFilter Newtonsoft.Json.Linq.JsonPath.FieldFilter_<ExecuteFilter>d__4::<>4__this
	FieldFilter_t2B4992D5A1D10B7A905F49C62609097AED91A13B * ___U3CU3E4__this_5;
	// System.Boolean Newtonsoft.Json.Linq.JsonPath.FieldFilter_<ExecuteFilter>d__4::errorWhenNoMatch
	bool ___errorWhenNoMatch_6;
	// System.Boolean Newtonsoft.Json.Linq.JsonPath.FieldFilter_<ExecuteFilter>d__4::<>3__errorWhenNoMatch
	bool ___U3CU3E3__errorWhenNoMatch_7;
	// Newtonsoft.Json.Linq.JObject Newtonsoft.Json.Linq.JsonPath.FieldFilter_<ExecuteFilter>d__4::<o>5__1
	JObject_t786AF07B1009334856B0362BBC48EEF68C81C585 * ___U3CoU3E5__1_8;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JsonPath.FieldFilter_<ExecuteFilter>d__4::<t>5__2
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * ___U3CtU3E5__2_9;
	// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JsonPath.FieldFilter_<ExecuteFilter>d__4::<>7__wrap1
	RuntimeObject* ___U3CU3E7__wrap1_10;
	// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Linq.JToken>> Newtonsoft.Json.Linq.JsonPath.FieldFilter_<ExecuteFilter>d__4::<>7__wrap2
	RuntimeObject* ___U3CU3E7__wrap2_11;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_tAF0C3B976817618E87E3152BBD9B3A1DF5D92610, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_tAF0C3B976817618E87E3152BBD9B3A1DF5D92610, ___U3CU3E2__current_1)); }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_tAF0C3B976817618E87E3152BBD9B3A1DF5D92610, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_tAF0C3B976817618E87E3152BBD9B3A1DF5D92610, ___current_3)); }
	inline RuntimeObject* get_current_3() const { return ___current_3; }
	inline RuntimeObject** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject* value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__current_4() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_tAF0C3B976817618E87E3152BBD9B3A1DF5D92610, ___U3CU3E3__current_4)); }
	inline RuntimeObject* get_U3CU3E3__current_4() const { return ___U3CU3E3__current_4; }
	inline RuntimeObject** get_address_of_U3CU3E3__current_4() { return &___U3CU3E3__current_4; }
	inline void set_U3CU3E3__current_4(RuntimeObject* value)
	{
		___U3CU3E3__current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__current_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_5() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_tAF0C3B976817618E87E3152BBD9B3A1DF5D92610, ___U3CU3E4__this_5)); }
	inline FieldFilter_t2B4992D5A1D10B7A905F49C62609097AED91A13B * get_U3CU3E4__this_5() const { return ___U3CU3E4__this_5; }
	inline FieldFilter_t2B4992D5A1D10B7A905F49C62609097AED91A13B ** get_address_of_U3CU3E4__this_5() { return &___U3CU3E4__this_5; }
	inline void set_U3CU3E4__this_5(FieldFilter_t2B4992D5A1D10B7A905F49C62609097AED91A13B * value)
	{
		___U3CU3E4__this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_5), value);
	}

	inline static int32_t get_offset_of_errorWhenNoMatch_6() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_tAF0C3B976817618E87E3152BBD9B3A1DF5D92610, ___errorWhenNoMatch_6)); }
	inline bool get_errorWhenNoMatch_6() const { return ___errorWhenNoMatch_6; }
	inline bool* get_address_of_errorWhenNoMatch_6() { return &___errorWhenNoMatch_6; }
	inline void set_errorWhenNoMatch_6(bool value)
	{
		___errorWhenNoMatch_6 = value;
	}

	inline static int32_t get_offset_of_U3CU3E3__errorWhenNoMatch_7() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_tAF0C3B976817618E87E3152BBD9B3A1DF5D92610, ___U3CU3E3__errorWhenNoMatch_7)); }
	inline bool get_U3CU3E3__errorWhenNoMatch_7() const { return ___U3CU3E3__errorWhenNoMatch_7; }
	inline bool* get_address_of_U3CU3E3__errorWhenNoMatch_7() { return &___U3CU3E3__errorWhenNoMatch_7; }
	inline void set_U3CU3E3__errorWhenNoMatch_7(bool value)
	{
		___U3CU3E3__errorWhenNoMatch_7 = value;
	}

	inline static int32_t get_offset_of_U3CoU3E5__1_8() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_tAF0C3B976817618E87E3152BBD9B3A1DF5D92610, ___U3CoU3E5__1_8)); }
	inline JObject_t786AF07B1009334856B0362BBC48EEF68C81C585 * get_U3CoU3E5__1_8() const { return ___U3CoU3E5__1_8; }
	inline JObject_t786AF07B1009334856B0362BBC48EEF68C81C585 ** get_address_of_U3CoU3E5__1_8() { return &___U3CoU3E5__1_8; }
	inline void set_U3CoU3E5__1_8(JObject_t786AF07B1009334856B0362BBC48EEF68C81C585 * value)
	{
		___U3CoU3E5__1_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CoU3E5__1_8), value);
	}

	inline static int32_t get_offset_of_U3CtU3E5__2_9() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_tAF0C3B976817618E87E3152BBD9B3A1DF5D92610, ___U3CtU3E5__2_9)); }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * get_U3CtU3E5__2_9() const { return ___U3CtU3E5__2_9; }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 ** get_address_of_U3CtU3E5__2_9() { return &___U3CtU3E5__2_9; }
	inline void set_U3CtU3E5__2_9(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * value)
	{
		___U3CtU3E5__2_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtU3E5__2_9), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_10() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_tAF0C3B976817618E87E3152BBD9B3A1DF5D92610, ___U3CU3E7__wrap1_10)); }
	inline RuntimeObject* get_U3CU3E7__wrap1_10() const { return ___U3CU3E7__wrap1_10; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap1_10() { return &___U3CU3E7__wrap1_10; }
	inline void set_U3CU3E7__wrap1_10(RuntimeObject* value)
	{
		___U3CU3E7__wrap1_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap1_10), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap2_11() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_tAF0C3B976817618E87E3152BBD9B3A1DF5D92610, ___U3CU3E7__wrap2_11)); }
	inline RuntimeObject* get_U3CU3E7__wrap2_11() const { return ___U3CU3E7__wrap2_11; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap2_11() { return &___U3CU3E7__wrap2_11; }
	inline void set_U3CU3E7__wrap2_11(RuntimeObject* value)
	{
		___U3CU3E7__wrap2_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap2_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CEXECUTEFILTERU3ED__4_TAF0C3B976817618E87E3152BBD9B3A1DF5D92610_H
#ifndef U3CU3EC_T7AF93AC964FCE6A240C24249983367E897CC068D_H
#define U3CU3EC_T7AF93AC964FCE6A240C24249983367E897CC068D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JsonPath.FieldMultipleFilter_<>c
struct  U3CU3Ec_t7AF93AC964FCE6A240C24249983367E897CC068D  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t7AF93AC964FCE6A240C24249983367E897CC068D_StaticFields
{
public:
	// Newtonsoft.Json.Linq.JsonPath.FieldMultipleFilter_<>c Newtonsoft.Json.Linq.JsonPath.FieldMultipleFilter_<>c::<>9
	U3CU3Ec_t7AF93AC964FCE6A240C24249983367E897CC068D * ___U3CU3E9_0;
	// System.Func`2<System.String,System.String> Newtonsoft.Json.Linq.JsonPath.FieldMultipleFilter_<>c::<>9__4_0
	Func_2_tD9EBF5FAD46B4F561FE5ABE814DC60E6253B8B96 * ___U3CU3E9__4_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t7AF93AC964FCE6A240C24249983367E897CC068D_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t7AF93AC964FCE6A240C24249983367E897CC068D * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t7AF93AC964FCE6A240C24249983367E897CC068D ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t7AF93AC964FCE6A240C24249983367E897CC068D * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__4_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t7AF93AC964FCE6A240C24249983367E897CC068D_StaticFields, ___U3CU3E9__4_0_1)); }
	inline Func_2_tD9EBF5FAD46B4F561FE5ABE814DC60E6253B8B96 * get_U3CU3E9__4_0_1() const { return ___U3CU3E9__4_0_1; }
	inline Func_2_tD9EBF5FAD46B4F561FE5ABE814DC60E6253B8B96 ** get_address_of_U3CU3E9__4_0_1() { return &___U3CU3E9__4_0_1; }
	inline void set_U3CU3E9__4_0_1(Func_2_tD9EBF5FAD46B4F561FE5ABE814DC60E6253B8B96 * value)
	{
		___U3CU3E9__4_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__4_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T7AF93AC964FCE6A240C24249983367E897CC068D_H
#ifndef JPATH_T3A0A64CF9AA620DDBE9D3C20AE75A7AE1E9225D7_H
#define JPATH_T3A0A64CF9AA620DDBE9D3C20AE75A7AE1E9225D7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JsonPath.JPath
struct  JPath_t3A0A64CF9AA620DDBE9D3C20AE75A7AE1E9225D7  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JPATH_T3A0A64CF9AA620DDBE9D3C20AE75A7AE1E9225D7_H
#ifndef PATHFILTER_TF860A9811AE814CCD54914A44C2A08AA07FECA80_H
#define PATHFILTER_TF860A9811AE814CCD54914A44C2A08AA07FECA80_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JsonPath.PathFilter
struct  PathFilter_tF860A9811AE814CCD54914A44C2A08AA07FECA80  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATHFILTER_TF860A9811AE814CCD54914A44C2A08AA07FECA80_H
#ifndef U3CEXECUTEFILTERU3ED__4_T1092E9A17F5C737B43A85A54B82042D32C7E0683_H
#define U3CEXECUTEFILTERU3ED__4_T1092E9A17F5C737B43A85A54B82042D32C7E0683_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JsonPath.QueryFilter_<ExecuteFilter>d__4
struct  U3CExecuteFilterU3Ed__4_t1092E9A17F5C737B43A85A54B82042D32C7E0683  : public RuntimeObject
{
public:
	// System.Int32 Newtonsoft.Json.Linq.JsonPath.QueryFilter_<ExecuteFilter>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JsonPath.QueryFilter_<ExecuteFilter>d__4::<>2__current
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * ___U3CU3E2__current_1;
	// System.Int32 Newtonsoft.Json.Linq.JsonPath.QueryFilter_<ExecuteFilter>d__4::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JsonPath.QueryFilter_<ExecuteFilter>d__4::current
	RuntimeObject* ___current_3;
	// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JsonPath.QueryFilter_<ExecuteFilter>d__4::<>3__current
	RuntimeObject* ___U3CU3E3__current_4;
	// Newtonsoft.Json.Linq.JsonPath.QueryFilter Newtonsoft.Json.Linq.JsonPath.QueryFilter_<ExecuteFilter>d__4::<>4__this
	QueryFilter_t6BBC6AA8C49E2AEED63087A9FCEB306F022C22AF * ___U3CU3E4__this_5;
	// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JsonPath.QueryFilter_<ExecuteFilter>d__4::<>7__wrap1
	RuntimeObject* ___U3CU3E7__wrap1_6;
	// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JsonPath.QueryFilter_<ExecuteFilter>d__4::<>7__wrap2
	RuntimeObject* ___U3CU3E7__wrap2_7;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t1092E9A17F5C737B43A85A54B82042D32C7E0683, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t1092E9A17F5C737B43A85A54B82042D32C7E0683, ___U3CU3E2__current_1)); }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t1092E9A17F5C737B43A85A54B82042D32C7E0683, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t1092E9A17F5C737B43A85A54B82042D32C7E0683, ___current_3)); }
	inline RuntimeObject* get_current_3() const { return ___current_3; }
	inline RuntimeObject** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject* value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__current_4() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t1092E9A17F5C737B43A85A54B82042D32C7E0683, ___U3CU3E3__current_4)); }
	inline RuntimeObject* get_U3CU3E3__current_4() const { return ___U3CU3E3__current_4; }
	inline RuntimeObject** get_address_of_U3CU3E3__current_4() { return &___U3CU3E3__current_4; }
	inline void set_U3CU3E3__current_4(RuntimeObject* value)
	{
		___U3CU3E3__current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__current_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_5() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t1092E9A17F5C737B43A85A54B82042D32C7E0683, ___U3CU3E4__this_5)); }
	inline QueryFilter_t6BBC6AA8C49E2AEED63087A9FCEB306F022C22AF * get_U3CU3E4__this_5() const { return ___U3CU3E4__this_5; }
	inline QueryFilter_t6BBC6AA8C49E2AEED63087A9FCEB306F022C22AF ** get_address_of_U3CU3E4__this_5() { return &___U3CU3E4__this_5; }
	inline void set_U3CU3E4__this_5(QueryFilter_t6BBC6AA8C49E2AEED63087A9FCEB306F022C22AF * value)
	{
		___U3CU3E4__this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_5), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_6() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t1092E9A17F5C737B43A85A54B82042D32C7E0683, ___U3CU3E7__wrap1_6)); }
	inline RuntimeObject* get_U3CU3E7__wrap1_6() const { return ___U3CU3E7__wrap1_6; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap1_6() { return &___U3CU3E7__wrap1_6; }
	inline void set_U3CU3E7__wrap1_6(RuntimeObject* value)
	{
		___U3CU3E7__wrap1_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap1_6), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap2_7() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t1092E9A17F5C737B43A85A54B82042D32C7E0683, ___U3CU3E7__wrap2_7)); }
	inline RuntimeObject* get_U3CU3E7__wrap2_7() const { return ___U3CU3E7__wrap2_7; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap2_7() { return &___U3CU3E7__wrap2_7; }
	inline void set_U3CU3E7__wrap2_7(RuntimeObject* value)
	{
		___U3CU3E7__wrap2_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap2_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CEXECUTEFILTERU3ED__4_T1092E9A17F5C737B43A85A54B82042D32C7E0683_H
#ifndef U3CEXECUTEFILTERU3ED__4_T69C2964074761EFD7077E24C57036C7DC5E33510_H
#define U3CEXECUTEFILTERU3ED__4_T69C2964074761EFD7077E24C57036C7DC5E33510_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JsonPath.ScanFilter_<ExecuteFilter>d__4
struct  U3CExecuteFilterU3Ed__4_t69C2964074761EFD7077E24C57036C7DC5E33510  : public RuntimeObject
{
public:
	// System.Int32 Newtonsoft.Json.Linq.JsonPath.ScanFilter_<ExecuteFilter>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JsonPath.ScanFilter_<ExecuteFilter>d__4::<>2__current
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * ___U3CU3E2__current_1;
	// System.Int32 Newtonsoft.Json.Linq.JsonPath.ScanFilter_<ExecuteFilter>d__4::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JsonPath.ScanFilter_<ExecuteFilter>d__4::current
	RuntimeObject* ___current_3;
	// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JsonPath.ScanFilter_<ExecuteFilter>d__4::<>3__current
	RuntimeObject* ___U3CU3E3__current_4;
	// Newtonsoft.Json.Linq.JsonPath.ScanFilter Newtonsoft.Json.Linq.JsonPath.ScanFilter_<ExecuteFilter>d__4::<>4__this
	ScanFilter_t1FC1A02EA817F7D53FCA21E7FBB5DF326FC03F3D * ___U3CU3E4__this_5;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JsonPath.ScanFilter_<ExecuteFilter>d__4::<root>5__1
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * ___U3CrootU3E5__1_6;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JsonPath.ScanFilter_<ExecuteFilter>d__4::<value>5__2
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * ___U3CvalueU3E5__2_7;
	// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JsonPath.ScanFilter_<ExecuteFilter>d__4::<>7__wrap1
	RuntimeObject* ___U3CU3E7__wrap1_8;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t69C2964074761EFD7077E24C57036C7DC5E33510, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t69C2964074761EFD7077E24C57036C7DC5E33510, ___U3CU3E2__current_1)); }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t69C2964074761EFD7077E24C57036C7DC5E33510, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t69C2964074761EFD7077E24C57036C7DC5E33510, ___current_3)); }
	inline RuntimeObject* get_current_3() const { return ___current_3; }
	inline RuntimeObject** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject* value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__current_4() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t69C2964074761EFD7077E24C57036C7DC5E33510, ___U3CU3E3__current_4)); }
	inline RuntimeObject* get_U3CU3E3__current_4() const { return ___U3CU3E3__current_4; }
	inline RuntimeObject** get_address_of_U3CU3E3__current_4() { return &___U3CU3E3__current_4; }
	inline void set_U3CU3E3__current_4(RuntimeObject* value)
	{
		___U3CU3E3__current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__current_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_5() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t69C2964074761EFD7077E24C57036C7DC5E33510, ___U3CU3E4__this_5)); }
	inline ScanFilter_t1FC1A02EA817F7D53FCA21E7FBB5DF326FC03F3D * get_U3CU3E4__this_5() const { return ___U3CU3E4__this_5; }
	inline ScanFilter_t1FC1A02EA817F7D53FCA21E7FBB5DF326FC03F3D ** get_address_of_U3CU3E4__this_5() { return &___U3CU3E4__this_5; }
	inline void set_U3CU3E4__this_5(ScanFilter_t1FC1A02EA817F7D53FCA21E7FBB5DF326FC03F3D * value)
	{
		___U3CU3E4__this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_5), value);
	}

	inline static int32_t get_offset_of_U3CrootU3E5__1_6() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t69C2964074761EFD7077E24C57036C7DC5E33510, ___U3CrootU3E5__1_6)); }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * get_U3CrootU3E5__1_6() const { return ___U3CrootU3E5__1_6; }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 ** get_address_of_U3CrootU3E5__1_6() { return &___U3CrootU3E5__1_6; }
	inline void set_U3CrootU3E5__1_6(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * value)
	{
		___U3CrootU3E5__1_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrootU3E5__1_6), value);
	}

	inline static int32_t get_offset_of_U3CvalueU3E5__2_7() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t69C2964074761EFD7077E24C57036C7DC5E33510, ___U3CvalueU3E5__2_7)); }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * get_U3CvalueU3E5__2_7() const { return ___U3CvalueU3E5__2_7; }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 ** get_address_of_U3CvalueU3E5__2_7() { return &___U3CvalueU3E5__2_7; }
	inline void set_U3CvalueU3E5__2_7(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * value)
	{
		___U3CvalueU3E5__2_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CvalueU3E5__2_7), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_8() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t69C2964074761EFD7077E24C57036C7DC5E33510, ___U3CU3E7__wrap1_8)); }
	inline RuntimeObject* get_U3CU3E7__wrap1_8() const { return ___U3CU3E7__wrap1_8; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap1_8() { return &___U3CU3E7__wrap1_8; }
	inline void set_U3CU3E7__wrap1_8(RuntimeObject* value)
	{
		___U3CU3E7__wrap1_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap1_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CEXECUTEFILTERU3ED__4_T69C2964074761EFD7077E24C57036C7DC5E33510_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef __STATICARRAYINITTYPESIZEU3D10_T5D70094105EE31D680149823953357EE017EEE87_H
#define __STATICARRAYINITTYPESIZEU3D10_T5D70094105EE31D680149823953357EE017EEE87_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D10
struct  __StaticArrayInitTypeSizeU3D10_t5D70094105EE31D680149823953357EE017EEE87 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D10_t5D70094105EE31D680149823953357EE017EEE87__padding[10];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D10_T5D70094105EE31D680149823953357EE017EEE87_H
#ifndef __STATICARRAYINITTYPESIZEU3D12_TD27AE99D2D1EEDB86AC51A63B9AD0CA1DB6D8637_H
#define __STATICARRAYINITTYPESIZEU3D12_TD27AE99D2D1EEDB86AC51A63B9AD0CA1DB6D8637_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12
struct  __StaticArrayInitTypeSizeU3D12_tD27AE99D2D1EEDB86AC51A63B9AD0CA1DB6D8637 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D12_tD27AE99D2D1EEDB86AC51A63B9AD0CA1DB6D8637__padding[12];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D12_TD27AE99D2D1EEDB86AC51A63B9AD0CA1DB6D8637_H
#ifndef __STATICARRAYINITTYPESIZEU3D28_T5F1B9C4994D974B6D60CDFBEA3BB033669C9D472_H
#define __STATICARRAYINITTYPESIZEU3D28_T5F1B9C4994D974B6D60CDFBEA3BB033669C9D472_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D28
struct  __StaticArrayInitTypeSizeU3D28_t5F1B9C4994D974B6D60CDFBEA3BB033669C9D472 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D28_t5F1B9C4994D974B6D60CDFBEA3BB033669C9D472__padding[28];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D28_T5F1B9C4994D974B6D60CDFBEA3BB033669C9D472_H
#ifndef __STATICARRAYINITTYPESIZEU3D52_T8E069480386AD2369CDA8BC3B64741395333F064_H
#define __STATICARRAYINITTYPESIZEU3D52_T8E069480386AD2369CDA8BC3B64741395333F064_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D52
struct  __StaticArrayInitTypeSizeU3D52_t8E069480386AD2369CDA8BC3B64741395333F064 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D52_t8E069480386AD2369CDA8BC3B64741395333F064__padding[52];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D52_T8E069480386AD2369CDA8BC3B64741395333F064_H
#ifndef BSONARRAY_TFFCE8804FBA87DECA9A84178C668305D8E89B6A7_H
#define BSONARRAY_TFFCE8804FBA87DECA9A84178C668305D8E89B6A7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Bson.BsonArray
struct  BsonArray_tFFCE8804FBA87DECA9A84178C668305D8E89B6A7  : public BsonToken_tEB681A5D0E53F8005DC31BAEFDE6481BBE80C2D3
{
public:
	// System.Collections.Generic.List`1<Newtonsoft.Json.Bson.BsonToken> Newtonsoft.Json.Bson.BsonArray::_children
	List_1_t68BC38D388C4DC6640F9F3B1E87BDAE4EFA5E6E3 * ____children_2;

public:
	inline static int32_t get_offset_of__children_2() { return static_cast<int32_t>(offsetof(BsonArray_tFFCE8804FBA87DECA9A84178C668305D8E89B6A7, ____children_2)); }
	inline List_1_t68BC38D388C4DC6640F9F3B1E87BDAE4EFA5E6E3 * get__children_2() const { return ____children_2; }
	inline List_1_t68BC38D388C4DC6640F9F3B1E87BDAE4EFA5E6E3 ** get_address_of__children_2() { return &____children_2; }
	inline void set__children_2(List_1_t68BC38D388C4DC6640F9F3B1E87BDAE4EFA5E6E3 * value)
	{
		____children_2 = value;
		Il2CppCodeGenWriteBarrier((&____children_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONARRAY_TFFCE8804FBA87DECA9A84178C668305D8E89B6A7_H
#ifndef BSONOBJECT_TFB8AF55378FE99FCE476FF65B1667A0D45F93518_H
#define BSONOBJECT_TFB8AF55378FE99FCE476FF65B1667A0D45F93518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Bson.BsonObject
struct  BsonObject_tFB8AF55378FE99FCE476FF65B1667A0D45F93518  : public BsonToken_tEB681A5D0E53F8005DC31BAEFDE6481BBE80C2D3
{
public:
	// System.Collections.Generic.List`1<Newtonsoft.Json.Bson.BsonProperty> Newtonsoft.Json.Bson.BsonObject::_children
	List_1_t2496F27A4736C5D9542D33FE9FF2F0A3AF8F2566 * ____children_2;

public:
	inline static int32_t get_offset_of__children_2() { return static_cast<int32_t>(offsetof(BsonObject_tFB8AF55378FE99FCE476FF65B1667A0D45F93518, ____children_2)); }
	inline List_1_t2496F27A4736C5D9542D33FE9FF2F0A3AF8F2566 * get__children_2() const { return ____children_2; }
	inline List_1_t2496F27A4736C5D9542D33FE9FF2F0A3AF8F2566 ** get_address_of__children_2() { return &____children_2; }
	inline void set__children_2(List_1_t2496F27A4736C5D9542D33FE9FF2F0A3AF8F2566 * value)
	{
		____children_2 = value;
		Il2CppCodeGenWriteBarrier((&____children_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONOBJECT_TFB8AF55378FE99FCE476FF65B1667A0D45F93518_H
#ifndef BSONREGEX_T5C64E5D5DABD795CAC97069165054859A0B938D3_H
#define BSONREGEX_T5C64E5D5DABD795CAC97069165054859A0B938D3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Bson.BsonRegex
struct  BsonRegex_t5C64E5D5DABD795CAC97069165054859A0B938D3  : public BsonToken_tEB681A5D0E53F8005DC31BAEFDE6481BBE80C2D3
{
public:
	// Newtonsoft.Json.Bson.BsonString Newtonsoft.Json.Bson.BsonRegex::<Pattern>k__BackingField
	BsonString_t87A7877CE01E3E2FA2B118C4A617FA16159CBC0A * ___U3CPatternU3Ek__BackingField_2;
	// Newtonsoft.Json.Bson.BsonString Newtonsoft.Json.Bson.BsonRegex::<Options>k__BackingField
	BsonString_t87A7877CE01E3E2FA2B118C4A617FA16159CBC0A * ___U3COptionsU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CPatternU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(BsonRegex_t5C64E5D5DABD795CAC97069165054859A0B938D3, ___U3CPatternU3Ek__BackingField_2)); }
	inline BsonString_t87A7877CE01E3E2FA2B118C4A617FA16159CBC0A * get_U3CPatternU3Ek__BackingField_2() const { return ___U3CPatternU3Ek__BackingField_2; }
	inline BsonString_t87A7877CE01E3E2FA2B118C4A617FA16159CBC0A ** get_address_of_U3CPatternU3Ek__BackingField_2() { return &___U3CPatternU3Ek__BackingField_2; }
	inline void set_U3CPatternU3Ek__BackingField_2(BsonString_t87A7877CE01E3E2FA2B118C4A617FA16159CBC0A * value)
	{
		___U3CPatternU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPatternU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3COptionsU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(BsonRegex_t5C64E5D5DABD795CAC97069165054859A0B938D3, ___U3COptionsU3Ek__BackingField_3)); }
	inline BsonString_t87A7877CE01E3E2FA2B118C4A617FA16159CBC0A * get_U3COptionsU3Ek__BackingField_3() const { return ___U3COptionsU3Ek__BackingField_3; }
	inline BsonString_t87A7877CE01E3E2FA2B118C4A617FA16159CBC0A ** get_address_of_U3COptionsU3Ek__BackingField_3() { return &___U3COptionsU3Ek__BackingField_3; }
	inline void set_U3COptionsU3Ek__BackingField_3(BsonString_t87A7877CE01E3E2FA2B118C4A617FA16159CBC0A * value)
	{
		___U3COptionsU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3COptionsU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONREGEX_T5C64E5D5DABD795CAC97069165054859A0B938D3_H
#ifndef BINARYCONVERTER_T9DA25409355A29DFE356701CAD581E231D13DEF6_H
#define BINARYCONVERTER_T9DA25409355A29DFE356701CAD581E231D13DEF6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.BinaryConverter
struct  BinaryConverter_t9DA25409355A29DFE356701CAD581E231D13DEF6  : public JsonConverter_t61C31ABA417A4314A2582BF0404CE41267A61BE6
{
public:
	// Newtonsoft.Json.Utilities.ReflectionObject Newtonsoft.Json.Converters.BinaryConverter::_reflectionObject
	ReflectionObject_t3EDC4C62AF6FE6E5246ED34686ED44305157331D * ____reflectionObject_0;

public:
	inline static int32_t get_offset_of__reflectionObject_0() { return static_cast<int32_t>(offsetof(BinaryConverter_t9DA25409355A29DFE356701CAD581E231D13DEF6, ____reflectionObject_0)); }
	inline ReflectionObject_t3EDC4C62AF6FE6E5246ED34686ED44305157331D * get__reflectionObject_0() const { return ____reflectionObject_0; }
	inline ReflectionObject_t3EDC4C62AF6FE6E5246ED34686ED44305157331D ** get_address_of__reflectionObject_0() { return &____reflectionObject_0; }
	inline void set__reflectionObject_0(ReflectionObject_t3EDC4C62AF6FE6E5246ED34686ED44305157331D * value)
	{
		____reflectionObject_0 = value;
		Il2CppCodeGenWriteBarrier((&____reflectionObject_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINARYCONVERTER_T9DA25409355A29DFE356701CAD581E231D13DEF6_H
#ifndef BSONOBJECTIDCONVERTER_T9DA35F1AB243B9FEA9CF962B4C3AAD720EB7A812_H
#define BSONOBJECTIDCONVERTER_T9DA35F1AB243B9FEA9CF962B4C3AAD720EB7A812_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.BsonObjectIdConverter
struct  BsonObjectIdConverter_t9DA35F1AB243B9FEA9CF962B4C3AAD720EB7A812  : public JsonConverter_t61C31ABA417A4314A2582BF0404CE41267A61BE6
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONOBJECTIDCONVERTER_T9DA35F1AB243B9FEA9CF962B4C3AAD720EB7A812_H
#ifndef DATETIMECONVERTERBASE_TC0C56E1B50018AB9DF9B2F7F22FA0001AC1B1E71_H
#define DATETIMECONVERTERBASE_TC0C56E1B50018AB9DF9B2F7F22FA0001AC1B1E71_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.DateTimeConverterBase
struct  DateTimeConverterBase_tC0C56E1B50018AB9DF9B2F7F22FA0001AC1B1E71  : public JsonConverter_t61C31ABA417A4314A2582BF0404CE41267A61BE6
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMECONVERTERBASE_TC0C56E1B50018AB9DF9B2F7F22FA0001AC1B1E71_H
#ifndef HASHSETCONVERTER_TF86B72BCF0F72DCF91313ED0D2D8E572841EEF55_H
#define HASHSETCONVERTER_TF86B72BCF0F72DCF91313ED0D2D8E572841EEF55_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.HashSetConverter
struct  HashSetConverter_tF86B72BCF0F72DCF91313ED0D2D8E572841EEF55  : public JsonConverter_t61C31ABA417A4314A2582BF0404CE41267A61BE6
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HASHSETCONVERTER_TF86B72BCF0F72DCF91313ED0D2D8E572841EEF55_H
#ifndef KEYVALUEPAIRCONVERTER_TD36E6790020AC069084D9D08BA6159A38E5E5557_H
#define KEYVALUEPAIRCONVERTER_TD36E6790020AC069084D9D08BA6159A38E5E5557_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.KeyValuePairConverter
struct  KeyValuePairConverter_tD36E6790020AC069084D9D08BA6159A38E5E5557  : public JsonConverter_t61C31ABA417A4314A2582BF0404CE41267A61BE6
{
public:

public:
};

struct KeyValuePairConverter_tD36E6790020AC069084D9D08BA6159A38E5E5557_StaticFields
{
public:
	// Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Type,Newtonsoft.Json.Utilities.ReflectionObject> Newtonsoft.Json.Converters.KeyValuePairConverter::ReflectionObjectPerType
	ThreadSafeStore_2_t383F3E9472520F192BABC81C35BAC871CE4CC93E * ___ReflectionObjectPerType_0;

public:
	inline static int32_t get_offset_of_ReflectionObjectPerType_0() { return static_cast<int32_t>(offsetof(KeyValuePairConverter_tD36E6790020AC069084D9D08BA6159A38E5E5557_StaticFields, ___ReflectionObjectPerType_0)); }
	inline ThreadSafeStore_2_t383F3E9472520F192BABC81C35BAC871CE4CC93E * get_ReflectionObjectPerType_0() const { return ___ReflectionObjectPerType_0; }
	inline ThreadSafeStore_2_t383F3E9472520F192BABC81C35BAC871CE4CC93E ** get_address_of_ReflectionObjectPerType_0() { return &___ReflectionObjectPerType_0; }
	inline void set_ReflectionObjectPerType_0(ThreadSafeStore_2_t383F3E9472520F192BABC81C35BAC871CE4CC93E * value)
	{
		___ReflectionObjectPerType_0 = value;
		Il2CppCodeGenWriteBarrier((&___ReflectionObjectPerType_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIRCONVERTER_TD36E6790020AC069084D9D08BA6159A38E5E5557_H
#ifndef REGEXCONVERTER_T043C392623207631E6876907924EEA55F1AC05EE_H
#define REGEXCONVERTER_T043C392623207631E6876907924EEA55F1AC05EE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.RegexConverter
struct  RegexConverter_t043C392623207631E6876907924EEA55F1AC05EE  : public JsonConverter_t61C31ABA417A4314A2582BF0404CE41267A61BE6
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGEXCONVERTER_T043C392623207631E6876907924EEA55F1AC05EE_H
#ifndef STRINGENUMCONVERTER_T83AC1D5A05449B4E310BC9F16FD23D2BFF16E360_H
#define STRINGENUMCONVERTER_T83AC1D5A05449B4E310BC9F16FD23D2BFF16E360_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.StringEnumConverter
struct  StringEnumConverter_t83AC1D5A05449B4E310BC9F16FD23D2BFF16E360  : public JsonConverter_t61C31ABA417A4314A2582BF0404CE41267A61BE6
{
public:
	// System.Boolean Newtonsoft.Json.Converters.StringEnumConverter::<CamelCaseText>k__BackingField
	bool ___U3CCamelCaseTextU3Ek__BackingField_0;
	// System.Boolean Newtonsoft.Json.Converters.StringEnumConverter::<AllowIntegerValues>k__BackingField
	bool ___U3CAllowIntegerValuesU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CCamelCaseTextU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StringEnumConverter_t83AC1D5A05449B4E310BC9F16FD23D2BFF16E360, ___U3CCamelCaseTextU3Ek__BackingField_0)); }
	inline bool get_U3CCamelCaseTextU3Ek__BackingField_0() const { return ___U3CCamelCaseTextU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CCamelCaseTextU3Ek__BackingField_0() { return &___U3CCamelCaseTextU3Ek__BackingField_0; }
	inline void set_U3CCamelCaseTextU3Ek__BackingField_0(bool value)
	{
		___U3CCamelCaseTextU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CAllowIntegerValuesU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(StringEnumConverter_t83AC1D5A05449B4E310BC9F16FD23D2BFF16E360, ___U3CAllowIntegerValuesU3Ek__BackingField_1)); }
	inline bool get_U3CAllowIntegerValuesU3Ek__BackingField_1() const { return ___U3CAllowIntegerValuesU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CAllowIntegerValuesU3Ek__BackingField_1() { return &___U3CAllowIntegerValuesU3Ek__BackingField_1; }
	inline void set_U3CAllowIntegerValuesU3Ek__BackingField_1(bool value)
	{
		___U3CAllowIntegerValuesU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGENUMCONVERTER_T83AC1D5A05449B4E310BC9F16FD23D2BFF16E360_H
#ifndef VECTORCONVERTER_TEC7B5B3CB2C15E4301869F3429AEB966F050461E_H
#define VECTORCONVERTER_TEC7B5B3CB2C15E4301869F3429AEB966F050461E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.VectorConverter
struct  VectorConverter_tEC7B5B3CB2C15E4301869F3429AEB966F050461E  : public JsonConverter_t61C31ABA417A4314A2582BF0404CE41267A61BE6
{
public:
	// System.Boolean Newtonsoft.Json.Converters.VectorConverter::<EnableVector2>k__BackingField
	bool ___U3CEnableVector2U3Ek__BackingField_3;
	// System.Boolean Newtonsoft.Json.Converters.VectorConverter::<EnableVector3>k__BackingField
	bool ___U3CEnableVector3U3Ek__BackingField_4;
	// System.Boolean Newtonsoft.Json.Converters.VectorConverter::<EnableVector4>k__BackingField
	bool ___U3CEnableVector4U3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CEnableVector2U3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(VectorConverter_tEC7B5B3CB2C15E4301869F3429AEB966F050461E, ___U3CEnableVector2U3Ek__BackingField_3)); }
	inline bool get_U3CEnableVector2U3Ek__BackingField_3() const { return ___U3CEnableVector2U3Ek__BackingField_3; }
	inline bool* get_address_of_U3CEnableVector2U3Ek__BackingField_3() { return &___U3CEnableVector2U3Ek__BackingField_3; }
	inline void set_U3CEnableVector2U3Ek__BackingField_3(bool value)
	{
		___U3CEnableVector2U3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CEnableVector3U3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(VectorConverter_tEC7B5B3CB2C15E4301869F3429AEB966F050461E, ___U3CEnableVector3U3Ek__BackingField_4)); }
	inline bool get_U3CEnableVector3U3Ek__BackingField_4() const { return ___U3CEnableVector3U3Ek__BackingField_4; }
	inline bool* get_address_of_U3CEnableVector3U3Ek__BackingField_4() { return &___U3CEnableVector3U3Ek__BackingField_4; }
	inline void set_U3CEnableVector3U3Ek__BackingField_4(bool value)
	{
		___U3CEnableVector3U3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CEnableVector4U3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(VectorConverter_tEC7B5B3CB2C15E4301869F3429AEB966F050461E, ___U3CEnableVector4U3Ek__BackingField_5)); }
	inline bool get_U3CEnableVector4U3Ek__BackingField_5() const { return ___U3CEnableVector4U3Ek__BackingField_5; }
	inline bool* get_address_of_U3CEnableVector4U3Ek__BackingField_5() { return &___U3CEnableVector4U3Ek__BackingField_5; }
	inline void set_U3CEnableVector4U3Ek__BackingField_5(bool value)
	{
		___U3CEnableVector4U3Ek__BackingField_5 = value;
	}
};

struct VectorConverter_tEC7B5B3CB2C15E4301869F3429AEB966F050461E_StaticFields
{
public:
	// System.Type Newtonsoft.Json.Converters.VectorConverter::V2
	Type_t * ___V2_0;
	// System.Type Newtonsoft.Json.Converters.VectorConverter::V3
	Type_t * ___V3_1;
	// System.Type Newtonsoft.Json.Converters.VectorConverter::V4
	Type_t * ___V4_2;

public:
	inline static int32_t get_offset_of_V2_0() { return static_cast<int32_t>(offsetof(VectorConverter_tEC7B5B3CB2C15E4301869F3429AEB966F050461E_StaticFields, ___V2_0)); }
	inline Type_t * get_V2_0() const { return ___V2_0; }
	inline Type_t ** get_address_of_V2_0() { return &___V2_0; }
	inline void set_V2_0(Type_t * value)
	{
		___V2_0 = value;
		Il2CppCodeGenWriteBarrier((&___V2_0), value);
	}

	inline static int32_t get_offset_of_V3_1() { return static_cast<int32_t>(offsetof(VectorConverter_tEC7B5B3CB2C15E4301869F3429AEB966F050461E_StaticFields, ___V3_1)); }
	inline Type_t * get_V3_1() const { return ___V3_1; }
	inline Type_t ** get_address_of_V3_1() { return &___V3_1; }
	inline void set_V3_1(Type_t * value)
	{
		___V3_1 = value;
		Il2CppCodeGenWriteBarrier((&___V3_1), value);
	}

	inline static int32_t get_offset_of_V4_2() { return static_cast<int32_t>(offsetof(VectorConverter_tEC7B5B3CB2C15E4301869F3429AEB966F050461E_StaticFields, ___V4_2)); }
	inline Type_t * get_V4_2() const { return ___V4_2; }
	inline Type_t ** get_address_of_V4_2() { return &___V4_2; }
	inline void set_V4_2(Type_t * value)
	{
		___V4_2 = value;
		Il2CppCodeGenWriteBarrier((&___V4_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTORCONVERTER_TEC7B5B3CB2C15E4301869F3429AEB966F050461E_H
#ifndef VERSIONCONVERTER_T4139946CA5A33692FD4B29D8DBF59786785A9D0A_H
#define VERSIONCONVERTER_T4139946CA5A33692FD4B29D8DBF59786785A9D0A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.VersionConverter
struct  VersionConverter_t4139946CA5A33692FD4B29D8DBF59786785A9D0A  : public JsonConverter_t61C31ABA417A4314A2582BF0404CE41267A61BE6
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERSIONCONVERTER_T4139946CA5A33692FD4B29D8DBF59786785A9D0A_H
#ifndef XATTRIBUTEWRAPPER_TB94EBF566DA78B2D721A00F66EBFFAB9B0748E84_H
#define XATTRIBUTEWRAPPER_TB94EBF566DA78B2D721A00F66EBFFAB9B0748E84_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.XAttributeWrapper
struct  XAttributeWrapper_tB94EBF566DA78B2D721A00F66EBFFAB9B0748E84  : public XObjectWrapper_tAB3FD0F91A7839A07C2FD1C40020F14201650305
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XATTRIBUTEWRAPPER_TB94EBF566DA78B2D721A00F66EBFFAB9B0748E84_H
#ifndef XCOMMENTWRAPPER_TA349A28180A9192EC8765859993F634E7648505C_H
#define XCOMMENTWRAPPER_TA349A28180A9192EC8765859993F634E7648505C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.XCommentWrapper
struct  XCommentWrapper_tA349A28180A9192EC8765859993F634E7648505C  : public XObjectWrapper_tAB3FD0F91A7839A07C2FD1C40020F14201650305
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XCOMMENTWRAPPER_TA349A28180A9192EC8765859993F634E7648505C_H
#ifndef XCONTAINERWRAPPER_T3FC803A38484BB3B2C7A61D5498A5EE31C9AFC58_H
#define XCONTAINERWRAPPER_T3FC803A38484BB3B2C7A61D5498A5EE31C9AFC58_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.XContainerWrapper
struct  XContainerWrapper_t3FC803A38484BB3B2C7A61D5498A5EE31C9AFC58  : public XObjectWrapper_tAB3FD0F91A7839A07C2FD1C40020F14201650305
{
public:
	// System.Collections.Generic.List`1<Newtonsoft.Json.Converters.IXmlNode> Newtonsoft.Json.Converters.XContainerWrapper::_childNodes
	List_1_tE8856893C3BB37F3EF523D597A4152D5DCD8E400 * ____childNodes_2;

public:
	inline static int32_t get_offset_of__childNodes_2() { return static_cast<int32_t>(offsetof(XContainerWrapper_t3FC803A38484BB3B2C7A61D5498A5EE31C9AFC58, ____childNodes_2)); }
	inline List_1_tE8856893C3BB37F3EF523D597A4152D5DCD8E400 * get__childNodes_2() const { return ____childNodes_2; }
	inline List_1_tE8856893C3BB37F3EF523D597A4152D5DCD8E400 ** get_address_of__childNodes_2() { return &____childNodes_2; }
	inline void set__childNodes_2(List_1_tE8856893C3BB37F3EF523D597A4152D5DCD8E400 * value)
	{
		____childNodes_2 = value;
		Il2CppCodeGenWriteBarrier((&____childNodes_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XCONTAINERWRAPPER_T3FC803A38484BB3B2C7A61D5498A5EE31C9AFC58_H
#ifndef XDECLARATIONWRAPPER_T0D5F3B36907DE90FC2F7CE9764B78E1B6A7DE5EA_H
#define XDECLARATIONWRAPPER_T0D5F3B36907DE90FC2F7CE9764B78E1B6A7DE5EA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.XDeclarationWrapper
struct  XDeclarationWrapper_t0D5F3B36907DE90FC2F7CE9764B78E1B6A7DE5EA  : public XObjectWrapper_tAB3FD0F91A7839A07C2FD1C40020F14201650305
{
public:
	// System.Xml.Linq.XDeclaration Newtonsoft.Json.Converters.XDeclarationWrapper::<Declaration>k__BackingField
	XDeclaration_tB17C002AEFF0D8802A09677E246F555FDD1F8512 * ___U3CDeclarationU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CDeclarationU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(XDeclarationWrapper_t0D5F3B36907DE90FC2F7CE9764B78E1B6A7DE5EA, ___U3CDeclarationU3Ek__BackingField_2)); }
	inline XDeclaration_tB17C002AEFF0D8802A09677E246F555FDD1F8512 * get_U3CDeclarationU3Ek__BackingField_2() const { return ___U3CDeclarationU3Ek__BackingField_2; }
	inline XDeclaration_tB17C002AEFF0D8802A09677E246F555FDD1F8512 ** get_address_of_U3CDeclarationU3Ek__BackingField_2() { return &___U3CDeclarationU3Ek__BackingField_2; }
	inline void set_U3CDeclarationU3Ek__BackingField_2(XDeclaration_tB17C002AEFF0D8802A09677E246F555FDD1F8512 * value)
	{
		___U3CDeclarationU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDeclarationU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XDECLARATIONWRAPPER_T0D5F3B36907DE90FC2F7CE9764B78E1B6A7DE5EA_H
#ifndef XDOCUMENTTYPEWRAPPER_T62001F61701C5B2FB3301351C1C124BBCDB40591_H
#define XDOCUMENTTYPEWRAPPER_T62001F61701C5B2FB3301351C1C124BBCDB40591_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.XDocumentTypeWrapper
struct  XDocumentTypeWrapper_t62001F61701C5B2FB3301351C1C124BBCDB40591  : public XObjectWrapper_tAB3FD0F91A7839A07C2FD1C40020F14201650305
{
public:
	// System.Xml.Linq.XDocumentType Newtonsoft.Json.Converters.XDocumentTypeWrapper::_documentType
	XDocumentType_tB3120C99DFF0207A8DDAF94868BA84AD2296E444 * ____documentType_2;

public:
	inline static int32_t get_offset_of__documentType_2() { return static_cast<int32_t>(offsetof(XDocumentTypeWrapper_t62001F61701C5B2FB3301351C1C124BBCDB40591, ____documentType_2)); }
	inline XDocumentType_tB3120C99DFF0207A8DDAF94868BA84AD2296E444 * get__documentType_2() const { return ____documentType_2; }
	inline XDocumentType_tB3120C99DFF0207A8DDAF94868BA84AD2296E444 ** get_address_of__documentType_2() { return &____documentType_2; }
	inline void set__documentType_2(XDocumentType_tB3120C99DFF0207A8DDAF94868BA84AD2296E444 * value)
	{
		____documentType_2 = value;
		Il2CppCodeGenWriteBarrier((&____documentType_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XDOCUMENTTYPEWRAPPER_T62001F61701C5B2FB3301351C1C124BBCDB40591_H
#ifndef XPROCESSINGINSTRUCTIONWRAPPER_TC2AAC446F0106BCD342DB3DB9834D7FBDD2C1A61_H
#define XPROCESSINGINSTRUCTIONWRAPPER_TC2AAC446F0106BCD342DB3DB9834D7FBDD2C1A61_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.XProcessingInstructionWrapper
struct  XProcessingInstructionWrapper_tC2AAC446F0106BCD342DB3DB9834D7FBDD2C1A61  : public XObjectWrapper_tAB3FD0F91A7839A07C2FD1C40020F14201650305
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPROCESSINGINSTRUCTIONWRAPPER_TC2AAC446F0106BCD342DB3DB9834D7FBDD2C1A61_H
#ifndef XTEXTWRAPPER_T519A345B2A497D9E340113365C8397861F0869CA_H
#define XTEXTWRAPPER_T519A345B2A497D9E340113365C8397861F0869CA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.XTextWrapper
struct  XTextWrapper_t519A345B2A497D9E340113365C8397861F0869CA  : public XObjectWrapper_tAB3FD0F91A7839A07C2FD1C40020F14201650305
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XTEXTWRAPPER_T519A345B2A497D9E340113365C8397861F0869CA_H
#ifndef XMLDECLARATIONWRAPPER_TE2CA38569645B999EC3B89B8F301A1AAF4DA1065_H
#define XMLDECLARATIONWRAPPER_TE2CA38569645B999EC3B89B8F301A1AAF4DA1065_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.XmlDeclarationWrapper
struct  XmlDeclarationWrapper_tE2CA38569645B999EC3B89B8F301A1AAF4DA1065  : public XmlNodeWrapper_t3C0D443DF6B193527862D3D680F0BD1979668C3F
{
public:
	// System.Xml.XmlDeclaration Newtonsoft.Json.Converters.XmlDeclarationWrapper::_declaration
	XmlDeclaration_tB67A8B98F543947C6EB990C44A212527B9B51200 * ____declaration_3;

public:
	inline static int32_t get_offset_of__declaration_3() { return static_cast<int32_t>(offsetof(XmlDeclarationWrapper_tE2CA38569645B999EC3B89B8F301A1AAF4DA1065, ____declaration_3)); }
	inline XmlDeclaration_tB67A8B98F543947C6EB990C44A212527B9B51200 * get__declaration_3() const { return ____declaration_3; }
	inline XmlDeclaration_tB67A8B98F543947C6EB990C44A212527B9B51200 ** get_address_of__declaration_3() { return &____declaration_3; }
	inline void set__declaration_3(XmlDeclaration_tB67A8B98F543947C6EB990C44A212527B9B51200 * value)
	{
		____declaration_3 = value;
		Il2CppCodeGenWriteBarrier((&____declaration_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLDECLARATIONWRAPPER_TE2CA38569645B999EC3B89B8F301A1AAF4DA1065_H
#ifndef XMLDOCUMENTTYPEWRAPPER_T7A371AEA41218C3DAB1D3C75BC81B46D4DAA3E40_H
#define XMLDOCUMENTTYPEWRAPPER_T7A371AEA41218C3DAB1D3C75BC81B46D4DAA3E40_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.XmlDocumentTypeWrapper
struct  XmlDocumentTypeWrapper_t7A371AEA41218C3DAB1D3C75BC81B46D4DAA3E40  : public XmlNodeWrapper_t3C0D443DF6B193527862D3D680F0BD1979668C3F
{
public:
	// System.Xml.XmlDocumentType Newtonsoft.Json.Converters.XmlDocumentTypeWrapper::_documentType
	XmlDocumentType_t9AB8D5C3C27B699B885D941D4B4735A6EE039136 * ____documentType_3;

public:
	inline static int32_t get_offset_of__documentType_3() { return static_cast<int32_t>(offsetof(XmlDocumentTypeWrapper_t7A371AEA41218C3DAB1D3C75BC81B46D4DAA3E40, ____documentType_3)); }
	inline XmlDocumentType_t9AB8D5C3C27B699B885D941D4B4735A6EE039136 * get__documentType_3() const { return ____documentType_3; }
	inline XmlDocumentType_t9AB8D5C3C27B699B885D941D4B4735A6EE039136 ** get_address_of__documentType_3() { return &____documentType_3; }
	inline void set__documentType_3(XmlDocumentType_t9AB8D5C3C27B699B885D941D4B4735A6EE039136 * value)
	{
		____documentType_3 = value;
		Il2CppCodeGenWriteBarrier((&____documentType_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLDOCUMENTTYPEWRAPPER_T7A371AEA41218C3DAB1D3C75BC81B46D4DAA3E40_H
#ifndef XMLDOCUMENTWRAPPER_TE2D345B4D840F71EC53B07B538A461FA713B189F_H
#define XMLDOCUMENTWRAPPER_TE2D345B4D840F71EC53B07B538A461FA713B189F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.XmlDocumentWrapper
struct  XmlDocumentWrapper_tE2D345B4D840F71EC53B07B538A461FA713B189F  : public XmlNodeWrapper_t3C0D443DF6B193527862D3D680F0BD1979668C3F
{
public:
	// System.Xml.XmlDocument Newtonsoft.Json.Converters.XmlDocumentWrapper::_document
	XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 * ____document_3;

public:
	inline static int32_t get_offset_of__document_3() { return static_cast<int32_t>(offsetof(XmlDocumentWrapper_tE2D345B4D840F71EC53B07B538A461FA713B189F, ____document_3)); }
	inline XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 * get__document_3() const { return ____document_3; }
	inline XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 ** get_address_of__document_3() { return &____document_3; }
	inline void set__document_3(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 * value)
	{
		____document_3 = value;
		Il2CppCodeGenWriteBarrier((&____document_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLDOCUMENTWRAPPER_TE2D345B4D840F71EC53B07B538A461FA713B189F_H
#ifndef XMLELEMENTWRAPPER_T5B1B956EEE32EA198D705DBE93B1A8ADC2159C2A_H
#define XMLELEMENTWRAPPER_T5B1B956EEE32EA198D705DBE93B1A8ADC2159C2A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.XmlElementWrapper
struct  XmlElementWrapper_t5B1B956EEE32EA198D705DBE93B1A8ADC2159C2A  : public XmlNodeWrapper_t3C0D443DF6B193527862D3D680F0BD1979668C3F
{
public:
	// System.Xml.XmlElement Newtonsoft.Json.Converters.XmlElementWrapper::_element
	XmlElement_t05D8C7971DE016A354D86028E7FFD84CD9DDDFDC * ____element_3;

public:
	inline static int32_t get_offset_of__element_3() { return static_cast<int32_t>(offsetof(XmlElementWrapper_t5B1B956EEE32EA198D705DBE93B1A8ADC2159C2A, ____element_3)); }
	inline XmlElement_t05D8C7971DE016A354D86028E7FFD84CD9DDDFDC * get__element_3() const { return ____element_3; }
	inline XmlElement_t05D8C7971DE016A354D86028E7FFD84CD9DDDFDC ** get_address_of__element_3() { return &____element_3; }
	inline void set__element_3(XmlElement_t05D8C7971DE016A354D86028E7FFD84CD9DDDFDC * value)
	{
		____element_3 = value;
		Il2CppCodeGenWriteBarrier((&____element_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLELEMENTWRAPPER_T5B1B956EEE32EA198D705DBE93B1A8ADC2159C2A_H
#ifndef XMLNODECONVERTER_TE53C7D00E9CBED8B2F7DCE9D4D2E5DFD3E8AB68B_H
#define XMLNODECONVERTER_TE53C7D00E9CBED8B2F7DCE9D4D2E5DFD3E8AB68B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.XmlNodeConverter
struct  XmlNodeConverter_tE53C7D00E9CBED8B2F7DCE9D4D2E5DFD3E8AB68B  : public JsonConverter_t61C31ABA417A4314A2582BF0404CE41267A61BE6
{
public:
	// System.String Newtonsoft.Json.Converters.XmlNodeConverter::<DeserializeRootElementName>k__BackingField
	String_t* ___U3CDeserializeRootElementNameU3Ek__BackingField_0;
	// System.Boolean Newtonsoft.Json.Converters.XmlNodeConverter::<WriteArrayAttribute>k__BackingField
	bool ___U3CWriteArrayAttributeU3Ek__BackingField_1;
	// System.Boolean Newtonsoft.Json.Converters.XmlNodeConverter::<OmitRootObject>k__BackingField
	bool ___U3COmitRootObjectU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CDeserializeRootElementNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(XmlNodeConverter_tE53C7D00E9CBED8B2F7DCE9D4D2E5DFD3E8AB68B, ___U3CDeserializeRootElementNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CDeserializeRootElementNameU3Ek__BackingField_0() const { return ___U3CDeserializeRootElementNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CDeserializeRootElementNameU3Ek__BackingField_0() { return &___U3CDeserializeRootElementNameU3Ek__BackingField_0; }
	inline void set_U3CDeserializeRootElementNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CDeserializeRootElementNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDeserializeRootElementNameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CWriteArrayAttributeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(XmlNodeConverter_tE53C7D00E9CBED8B2F7DCE9D4D2E5DFD3E8AB68B, ___U3CWriteArrayAttributeU3Ek__BackingField_1)); }
	inline bool get_U3CWriteArrayAttributeU3Ek__BackingField_1() const { return ___U3CWriteArrayAttributeU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CWriteArrayAttributeU3Ek__BackingField_1() { return &___U3CWriteArrayAttributeU3Ek__BackingField_1; }
	inline void set_U3CWriteArrayAttributeU3Ek__BackingField_1(bool value)
	{
		___U3CWriteArrayAttributeU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3COmitRootObjectU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(XmlNodeConverter_tE53C7D00E9CBED8B2F7DCE9D4D2E5DFD3E8AB68B, ___U3COmitRootObjectU3Ek__BackingField_2)); }
	inline bool get_U3COmitRootObjectU3Ek__BackingField_2() const { return ___U3COmitRootObjectU3Ek__BackingField_2; }
	inline bool* get_address_of_U3COmitRootObjectU3Ek__BackingField_2() { return &___U3COmitRootObjectU3Ek__BackingField_2; }
	inline void set_U3COmitRootObjectU3Ek__BackingField_2(bool value)
	{
		___U3COmitRootObjectU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNODECONVERTER_TE53C7D00E9CBED8B2F7DCE9D4D2E5DFD3E8AB68B_H
#ifndef JCONTAINER_TF4CD2E574503C709DEF18A04B79B264B83746DAB_H
#define JCONTAINER_TF4CD2E574503C709DEF18A04B79B264B83746DAB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JContainer
struct  JContainer_tF4CD2E574503C709DEF18A04B79B264B83746DAB  : public JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02
{
public:
	// System.ComponentModel.ListChangedEventHandler Newtonsoft.Json.Linq.JContainer::_listChanged
	ListChangedEventHandler_t8081F1428D22013519901C16884C5ACE86A72A88 * ____listChanged_13;
	// System.ComponentModel.AddingNewEventHandler Newtonsoft.Json.Linq.JContainer::_addingNew
	AddingNewEventHandler_t01FE6DE3A526F77E211D7DB02B76A07888E605A7 * ____addingNew_14;
	// System.Object Newtonsoft.Json.Linq.JContainer::_syncRoot
	RuntimeObject * ____syncRoot_15;
	// System.Boolean Newtonsoft.Json.Linq.JContainer::_busy
	bool ____busy_16;

public:
	inline static int32_t get_offset_of__listChanged_13() { return static_cast<int32_t>(offsetof(JContainer_tF4CD2E574503C709DEF18A04B79B264B83746DAB, ____listChanged_13)); }
	inline ListChangedEventHandler_t8081F1428D22013519901C16884C5ACE86A72A88 * get__listChanged_13() const { return ____listChanged_13; }
	inline ListChangedEventHandler_t8081F1428D22013519901C16884C5ACE86A72A88 ** get_address_of__listChanged_13() { return &____listChanged_13; }
	inline void set__listChanged_13(ListChangedEventHandler_t8081F1428D22013519901C16884C5ACE86A72A88 * value)
	{
		____listChanged_13 = value;
		Il2CppCodeGenWriteBarrier((&____listChanged_13), value);
	}

	inline static int32_t get_offset_of__addingNew_14() { return static_cast<int32_t>(offsetof(JContainer_tF4CD2E574503C709DEF18A04B79B264B83746DAB, ____addingNew_14)); }
	inline AddingNewEventHandler_t01FE6DE3A526F77E211D7DB02B76A07888E605A7 * get__addingNew_14() const { return ____addingNew_14; }
	inline AddingNewEventHandler_t01FE6DE3A526F77E211D7DB02B76A07888E605A7 ** get_address_of__addingNew_14() { return &____addingNew_14; }
	inline void set__addingNew_14(AddingNewEventHandler_t01FE6DE3A526F77E211D7DB02B76A07888E605A7 * value)
	{
		____addingNew_14 = value;
		Il2CppCodeGenWriteBarrier((&____addingNew_14), value);
	}

	inline static int32_t get_offset_of__syncRoot_15() { return static_cast<int32_t>(offsetof(JContainer_tF4CD2E574503C709DEF18A04B79B264B83746DAB, ____syncRoot_15)); }
	inline RuntimeObject * get__syncRoot_15() const { return ____syncRoot_15; }
	inline RuntimeObject ** get_address_of__syncRoot_15() { return &____syncRoot_15; }
	inline void set__syncRoot_15(RuntimeObject * value)
	{
		____syncRoot_15 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_15), value);
	}

	inline static int32_t get_offset_of__busy_16() { return static_cast<int32_t>(offsetof(JContainer_tF4CD2E574503C709DEF18A04B79B264B83746DAB, ____busy_16)); }
	inline bool get__busy_16() const { return ____busy_16; }
	inline bool* get_address_of__busy_16() { return &____busy_16; }
	inline void set__busy_16(bool value)
	{
		____busy_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JCONTAINER_TF4CD2E574503C709DEF18A04B79B264B83746DAB_H
#ifndef ARRAYMULTIPLEINDEXFILTER_TF9FF764DDCB79A819281D799112F6E930B75E99F_H
#define ARRAYMULTIPLEINDEXFILTER_TF9FF764DDCB79A819281D799112F6E930B75E99F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JsonPath.ArrayMultipleIndexFilter
struct  ArrayMultipleIndexFilter_tF9FF764DDCB79A819281D799112F6E930B75E99F  : public PathFilter_tF860A9811AE814CCD54914A44C2A08AA07FECA80
{
public:
	// System.Collections.Generic.List`1<System.Int32> Newtonsoft.Json.Linq.JsonPath.ArrayMultipleIndexFilter::<Indexes>k__BackingField
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ___U3CIndexesU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CIndexesU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ArrayMultipleIndexFilter_tF9FF764DDCB79A819281D799112F6E930B75E99F, ___U3CIndexesU3Ek__BackingField_0)); }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * get_U3CIndexesU3Ek__BackingField_0() const { return ___U3CIndexesU3Ek__BackingField_0; }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 ** get_address_of_U3CIndexesU3Ek__BackingField_0() { return &___U3CIndexesU3Ek__BackingField_0; }
	inline void set_U3CIndexesU3Ek__BackingField_0(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * value)
	{
		___U3CIndexesU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CIndexesU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRAYMULTIPLEINDEXFILTER_TF9FF764DDCB79A819281D799112F6E930B75E99F_H
#ifndef FIELDFILTER_T2B4992D5A1D10B7A905F49C62609097AED91A13B_H
#define FIELDFILTER_T2B4992D5A1D10B7A905F49C62609097AED91A13B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JsonPath.FieldFilter
struct  FieldFilter_t2B4992D5A1D10B7A905F49C62609097AED91A13B  : public PathFilter_tF860A9811AE814CCD54914A44C2A08AA07FECA80
{
public:
	// System.String Newtonsoft.Json.Linq.JsonPath.FieldFilter::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(FieldFilter_t2B4992D5A1D10B7A905F49C62609097AED91A13B, ___U3CNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CNameU3Ek__BackingField_0() const { return ___U3CNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_0() { return &___U3CNameU3Ek__BackingField_0; }
	inline void set_U3CNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIELDFILTER_T2B4992D5A1D10B7A905F49C62609097AED91A13B_H
#ifndef FIELDMULTIPLEFILTER_TE61362AE319B8C510FD11900AD0A0B79EC08CD6B_H
#define FIELDMULTIPLEFILTER_TE61362AE319B8C510FD11900AD0A0B79EC08CD6B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JsonPath.FieldMultipleFilter
struct  FieldMultipleFilter_tE61362AE319B8C510FD11900AD0A0B79EC08CD6B  : public PathFilter_tF860A9811AE814CCD54914A44C2A08AA07FECA80
{
public:
	// System.Collections.Generic.List`1<System.String> Newtonsoft.Json.Linq.JsonPath.FieldMultipleFilter::<Names>k__BackingField
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___U3CNamesU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CNamesU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(FieldMultipleFilter_tE61362AE319B8C510FD11900AD0A0B79EC08CD6B, ___U3CNamesU3Ek__BackingField_0)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_U3CNamesU3Ek__BackingField_0() const { return ___U3CNamesU3Ek__BackingField_0; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_U3CNamesU3Ek__BackingField_0() { return &___U3CNamesU3Ek__BackingField_0; }
	inline void set_U3CNamesU3Ek__BackingField_0(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___U3CNamesU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNamesU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIELDMULTIPLEFILTER_TE61362AE319B8C510FD11900AD0A0B79EC08CD6B_H
#ifndef QUERYFILTER_T6BBC6AA8C49E2AEED63087A9FCEB306F022C22AF_H
#define QUERYFILTER_T6BBC6AA8C49E2AEED63087A9FCEB306F022C22AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JsonPath.QueryFilter
struct  QueryFilter_t6BBC6AA8C49E2AEED63087A9FCEB306F022C22AF  : public PathFilter_tF860A9811AE814CCD54914A44C2A08AA07FECA80
{
public:
	// Newtonsoft.Json.Linq.JsonPath.QueryExpression Newtonsoft.Json.Linq.JsonPath.QueryFilter::<Expression>k__BackingField
	QueryExpression_t08B8769F946D3E05F6E9DB46393F0B9828244E7F * ___U3CExpressionU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CExpressionU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(QueryFilter_t6BBC6AA8C49E2AEED63087A9FCEB306F022C22AF, ___U3CExpressionU3Ek__BackingField_0)); }
	inline QueryExpression_t08B8769F946D3E05F6E9DB46393F0B9828244E7F * get_U3CExpressionU3Ek__BackingField_0() const { return ___U3CExpressionU3Ek__BackingField_0; }
	inline QueryExpression_t08B8769F946D3E05F6E9DB46393F0B9828244E7F ** get_address_of_U3CExpressionU3Ek__BackingField_0() { return &___U3CExpressionU3Ek__BackingField_0; }
	inline void set_U3CExpressionU3Ek__BackingField_0(QueryExpression_t08B8769F946D3E05F6E9DB46393F0B9828244E7F * value)
	{
		___U3CExpressionU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CExpressionU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUERYFILTER_T6BBC6AA8C49E2AEED63087A9FCEB306F022C22AF_H
#ifndef SCANFILTER_T1FC1A02EA817F7D53FCA21E7FBB5DF326FC03F3D_H
#define SCANFILTER_T1FC1A02EA817F7D53FCA21E7FBB5DF326FC03F3D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JsonPath.ScanFilter
struct  ScanFilter_t1FC1A02EA817F7D53FCA21E7FBB5DF326FC03F3D  : public PathFilter_tF860A9811AE814CCD54914A44C2A08AA07FECA80
{
public:
	// System.String Newtonsoft.Json.Linq.JsonPath.ScanFilter::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ScanFilter_t1FC1A02EA817F7D53FCA21E7FBB5DF326FC03F3D, ___U3CNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CNameU3Ek__BackingField_0() const { return ___U3CNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_0() { return &___U3CNameU3Ek__BackingField_0; }
	inline void set_U3CNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCANFILTER_T1FC1A02EA817F7D53FCA21E7FBB5DF326FC03F3D_H
#ifndef KEYVALUEPAIR_2_TFAF037D8F6143C33EA6D34A46D518C184C5FAC3C_H
#define KEYVALUEPAIR_2_TFAF037D8F6143C33EA6D34A46D518C184C5FAC3C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Linq.JToken>
struct  KeyValuePair_2_tFAF037D8F6143C33EA6D34A46D518C184C5FAC3C 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_tFAF037D8F6143C33EA6D34A46D518C184C5FAC3C, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_tFAF037D8F6143C33EA6D34A46D518C184C5FAC3C, ___value_1)); }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * get_value_1() const { return ___value_1; }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_TFAF037D8F6143C33EA6D34A46D518C184C5FAC3C_H
#ifndef ENUMERATOR_T52B74DE77FB2834C7A6E4DEFC01F9E5818235AB2_H
#define ENUMERATOR_T52B74DE77FB2834C7A6E4DEFC01F9E5818235AB2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1_Enumerator<System.Int32>
struct  Enumerator_t52B74DE77FB2834C7A6E4DEFC01F9E5818235AB2 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1_Enumerator::list
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1_Enumerator::current
	int32_t ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_t52B74DE77FB2834C7A6E4DEFC01F9E5818235AB2, ___list_0)); }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * get_list_0() const { return ___list_0; }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_t52B74DE77FB2834C7A6E4DEFC01F9E5818235AB2, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_t52B74DE77FB2834C7A6E4DEFC01F9E5818235AB2, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t52B74DE77FB2834C7A6E4DEFC01F9E5818235AB2, ___current_3)); }
	inline int32_t get_current_3() const { return ___current_3; }
	inline int32_t* get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(int32_t value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T52B74DE77FB2834C7A6E4DEFC01F9E5818235AB2_H
#ifndef ENUMERATOR_TBBAAE521602D26DCD42E467CF939632DC01EF813_H
#define ENUMERATOR_TBBAAE521602D26DCD42E467CF939632DC01EF813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1_Enumerator<System.String>
struct  Enumerator_tBBAAE521602D26DCD42E467CF939632DC01EF813 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1_Enumerator::list
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1_Enumerator::current
	String_t* ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tBBAAE521602D26DCD42E467CF939632DC01EF813, ___list_0)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_list_0() const { return ___list_0; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tBBAAE521602D26DCD42E467CF939632DC01EF813, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tBBAAE521602D26DCD42E467CF939632DC01EF813, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tBBAAE521602D26DCD42E467CF939632DC01EF813, ___current_3)); }
	inline String_t* get_current_3() const { return ___current_3; }
	inline String_t** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(String_t* value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_TBBAAE521602D26DCD42E467CF939632DC01EF813_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef NULLABLE_1_T0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB_H
#define NULLABLE_1_T0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Int32>
struct  Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1D1ACC86E2F3D4A2C7CA568000402E9AC4556D7E_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1D1ACC86E2F3D4A2C7CA568000402E9AC4556D7E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t1D1ACC86E2F3D4A2C7CA568000402E9AC4556D7E  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t1D1ACC86E2F3D4A2C7CA568000402E9AC4556D7E_StaticFields
{
public:
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D28 <PrivateImplementationDetails>::9E31F24F64765FCAA589F589324D17C9FCF6A06D
	__StaticArrayInitTypeSizeU3D28_t5F1B9C4994D974B6D60CDFBEA3BB033669C9D472  ___9E31F24F64765FCAA589F589324D17C9FCF6A06D_0;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12 <PrivateImplementationDetails>::ADFD2E1C801C825415DD53F4F2F72A13B389313C
	__StaticArrayInitTypeSizeU3D12_tD27AE99D2D1EEDB86AC51A63B9AD0CA1DB6D8637  ___ADFD2E1C801C825415DD53F4F2F72A13B389313C_1;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D10 <PrivateImplementationDetails>::D40004AB0E92BF6C8DFE481B56BE3D04ABDA76EB
	__StaticArrayInitTypeSizeU3D10_t5D70094105EE31D680149823953357EE017EEE87  ___D40004AB0E92BF6C8DFE481B56BE3D04ABDA76EB_2;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D52 <PrivateImplementationDetails>::DD3AEFEADB1CD615F3017763F1568179FEE640B0
	__StaticArrayInitTypeSizeU3D52_t8E069480386AD2369CDA8BC3B64741395333F064  ___DD3AEFEADB1CD615F3017763F1568179FEE640B0_3;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D52 <PrivateImplementationDetails>::E92B39D8233061927D9ACDE54665E68E7535635A
	__StaticArrayInitTypeSizeU3D52_t8E069480386AD2369CDA8BC3B64741395333F064  ___E92B39D8233061927D9ACDE54665E68E7535635A_4;

public:
	inline static int32_t get_offset_of_U39E31F24F64765FCAA589F589324D17C9FCF6A06D_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1D1ACC86E2F3D4A2C7CA568000402E9AC4556D7E_StaticFields, ___9E31F24F64765FCAA589F589324D17C9FCF6A06D_0)); }
	inline __StaticArrayInitTypeSizeU3D28_t5F1B9C4994D974B6D60CDFBEA3BB033669C9D472  get_U39E31F24F64765FCAA589F589324D17C9FCF6A06D_0() const { return ___9E31F24F64765FCAA589F589324D17C9FCF6A06D_0; }
	inline __StaticArrayInitTypeSizeU3D28_t5F1B9C4994D974B6D60CDFBEA3BB033669C9D472 * get_address_of_U39E31F24F64765FCAA589F589324D17C9FCF6A06D_0() { return &___9E31F24F64765FCAA589F589324D17C9FCF6A06D_0; }
	inline void set_U39E31F24F64765FCAA589F589324D17C9FCF6A06D_0(__StaticArrayInitTypeSizeU3D28_t5F1B9C4994D974B6D60CDFBEA3BB033669C9D472  value)
	{
		___9E31F24F64765FCAA589F589324D17C9FCF6A06D_0 = value;
	}

	inline static int32_t get_offset_of_ADFD2E1C801C825415DD53F4F2F72A13B389313C_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1D1ACC86E2F3D4A2C7CA568000402E9AC4556D7E_StaticFields, ___ADFD2E1C801C825415DD53F4F2F72A13B389313C_1)); }
	inline __StaticArrayInitTypeSizeU3D12_tD27AE99D2D1EEDB86AC51A63B9AD0CA1DB6D8637  get_ADFD2E1C801C825415DD53F4F2F72A13B389313C_1() const { return ___ADFD2E1C801C825415DD53F4F2F72A13B389313C_1; }
	inline __StaticArrayInitTypeSizeU3D12_tD27AE99D2D1EEDB86AC51A63B9AD0CA1DB6D8637 * get_address_of_ADFD2E1C801C825415DD53F4F2F72A13B389313C_1() { return &___ADFD2E1C801C825415DD53F4F2F72A13B389313C_1; }
	inline void set_ADFD2E1C801C825415DD53F4F2F72A13B389313C_1(__StaticArrayInitTypeSizeU3D12_tD27AE99D2D1EEDB86AC51A63B9AD0CA1DB6D8637  value)
	{
		___ADFD2E1C801C825415DD53F4F2F72A13B389313C_1 = value;
	}

	inline static int32_t get_offset_of_D40004AB0E92BF6C8DFE481B56BE3D04ABDA76EB_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1D1ACC86E2F3D4A2C7CA568000402E9AC4556D7E_StaticFields, ___D40004AB0E92BF6C8DFE481B56BE3D04ABDA76EB_2)); }
	inline __StaticArrayInitTypeSizeU3D10_t5D70094105EE31D680149823953357EE017EEE87  get_D40004AB0E92BF6C8DFE481B56BE3D04ABDA76EB_2() const { return ___D40004AB0E92BF6C8DFE481B56BE3D04ABDA76EB_2; }
	inline __StaticArrayInitTypeSizeU3D10_t5D70094105EE31D680149823953357EE017EEE87 * get_address_of_D40004AB0E92BF6C8DFE481B56BE3D04ABDA76EB_2() { return &___D40004AB0E92BF6C8DFE481B56BE3D04ABDA76EB_2; }
	inline void set_D40004AB0E92BF6C8DFE481B56BE3D04ABDA76EB_2(__StaticArrayInitTypeSizeU3D10_t5D70094105EE31D680149823953357EE017EEE87  value)
	{
		___D40004AB0E92BF6C8DFE481B56BE3D04ABDA76EB_2 = value;
	}

	inline static int32_t get_offset_of_DD3AEFEADB1CD615F3017763F1568179FEE640B0_3() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1D1ACC86E2F3D4A2C7CA568000402E9AC4556D7E_StaticFields, ___DD3AEFEADB1CD615F3017763F1568179FEE640B0_3)); }
	inline __StaticArrayInitTypeSizeU3D52_t8E069480386AD2369CDA8BC3B64741395333F064  get_DD3AEFEADB1CD615F3017763F1568179FEE640B0_3() const { return ___DD3AEFEADB1CD615F3017763F1568179FEE640B0_3; }
	inline __StaticArrayInitTypeSizeU3D52_t8E069480386AD2369CDA8BC3B64741395333F064 * get_address_of_DD3AEFEADB1CD615F3017763F1568179FEE640B0_3() { return &___DD3AEFEADB1CD615F3017763F1568179FEE640B0_3; }
	inline void set_DD3AEFEADB1CD615F3017763F1568179FEE640B0_3(__StaticArrayInitTypeSizeU3D52_t8E069480386AD2369CDA8BC3B64741395333F064  value)
	{
		___DD3AEFEADB1CD615F3017763F1568179FEE640B0_3 = value;
	}

	inline static int32_t get_offset_of_E92B39D8233061927D9ACDE54665E68E7535635A_4() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1D1ACC86E2F3D4A2C7CA568000402E9AC4556D7E_StaticFields, ___E92B39D8233061927D9ACDE54665E68E7535635A_4)); }
	inline __StaticArrayInitTypeSizeU3D52_t8E069480386AD2369CDA8BC3B64741395333F064  get_E92B39D8233061927D9ACDE54665E68E7535635A_4() const { return ___E92B39D8233061927D9ACDE54665E68E7535635A_4; }
	inline __StaticArrayInitTypeSizeU3D52_t8E069480386AD2369CDA8BC3B64741395333F064 * get_address_of_E92B39D8233061927D9ACDE54665E68E7535635A_4() { return &___E92B39D8233061927D9ACDE54665E68E7535635A_4; }
	inline void set_E92B39D8233061927D9ACDE54665E68E7535635A_4(__StaticArrayInitTypeSizeU3D52_t8E069480386AD2369CDA8BC3B64741395333F064  value)
	{
		___E92B39D8233061927D9ACDE54665E68E7535635A_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1D1ACC86E2F3D4A2C7CA568000402E9AC4556D7E_H
#ifndef BSONBINARYTYPE_T82EF959604654BA62C48C95135EC2F8A896928E3_H
#define BSONBINARYTYPE_T82EF959604654BA62C48C95135EC2F8A896928E3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Bson.BsonBinaryType
struct  BsonBinaryType_t82EF959604654BA62C48C95135EC2F8A896928E3 
{
public:
	// System.Byte Newtonsoft.Json.Bson.BsonBinaryType::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BsonBinaryType_t82EF959604654BA62C48C95135EC2F8A896928E3, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONBINARYTYPE_T82EF959604654BA62C48C95135EC2F8A896928E3_H
#ifndef BSONREADERSTATE_TF76E9F238D6B7DD2C4492104D599ABBD209C6C81_H
#define BSONREADERSTATE_TF76E9F238D6B7DD2C4492104D599ABBD209C6C81_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Bson.BsonReader_BsonReaderState
struct  BsonReaderState_tF76E9F238D6B7DD2C4492104D599ABBD209C6C81 
{
public:
	// System.Int32 Newtonsoft.Json.Bson.BsonReader_BsonReaderState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BsonReaderState_tF76E9F238D6B7DD2C4492104D599ABBD209C6C81, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONREADERSTATE_TF76E9F238D6B7DD2C4492104D599ABBD209C6C81_H
#ifndef BSONTYPE_TBF3253E8D497F6E8F668EB77C7B6345854BE5538_H
#define BSONTYPE_TBF3253E8D497F6E8F668EB77C7B6345854BE5538_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Bson.BsonType
struct  BsonType_tBF3253E8D497F6E8F668EB77C7B6345854BE5538 
{
public:
	// System.SByte Newtonsoft.Json.Bson.BsonType::value__
	int8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BsonType_tBF3253E8D497F6E8F668EB77C7B6345854BE5538, ___value___2)); }
	inline int8_t get_value___2() const { return ___value___2; }
	inline int8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int8_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONTYPE_TBF3253E8D497F6E8F668EB77C7B6345854BE5538_H
#ifndef JAVASCRIPTDATETIMECONVERTER_T854D8700203B04F436CCD4AF9C2CCC5E8D7CB654_H
#define JAVASCRIPTDATETIMECONVERTER_T854D8700203B04F436CCD4AF9C2CCC5E8D7CB654_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.JavaScriptDateTimeConverter
struct  JavaScriptDateTimeConverter_t854D8700203B04F436CCD4AF9C2CCC5E8D7CB654  : public DateTimeConverterBase_tC0C56E1B50018AB9DF9B2F7F22FA0001AC1B1E71
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JAVASCRIPTDATETIMECONVERTER_T854D8700203B04F436CCD4AF9C2CCC5E8D7CB654_H
#ifndef XDOCUMENTWRAPPER_T675AE9E51FF9329C72D5445BE0005388421EB163_H
#define XDOCUMENTWRAPPER_T675AE9E51FF9329C72D5445BE0005388421EB163_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.XDocumentWrapper
struct  XDocumentWrapper_t675AE9E51FF9329C72D5445BE0005388421EB163  : public XContainerWrapper_t3FC803A38484BB3B2C7A61D5498A5EE31C9AFC58
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XDOCUMENTWRAPPER_T675AE9E51FF9329C72D5445BE0005388421EB163_H
#ifndef XELEMENTWRAPPER_T4C78ACAA2626B439978C1C03F9B61CFDF26557A9_H
#define XELEMENTWRAPPER_T4C78ACAA2626B439978C1C03F9B61CFDF26557A9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.XElementWrapper
struct  XElementWrapper_t4C78ACAA2626B439978C1C03F9B61CFDF26557A9  : public XContainerWrapper_t3FC803A38484BB3B2C7A61D5498A5EE31C9AFC58
{
public:
	// System.Collections.Generic.List`1<Newtonsoft.Json.Converters.IXmlNode> Newtonsoft.Json.Converters.XElementWrapper::_attributes
	List_1_tE8856893C3BB37F3EF523D597A4152D5DCD8E400 * ____attributes_3;

public:
	inline static int32_t get_offset_of__attributes_3() { return static_cast<int32_t>(offsetof(XElementWrapper_t4C78ACAA2626B439978C1C03F9B61CFDF26557A9, ____attributes_3)); }
	inline List_1_tE8856893C3BB37F3EF523D597A4152D5DCD8E400 * get__attributes_3() const { return ____attributes_3; }
	inline List_1_tE8856893C3BB37F3EF523D597A4152D5DCD8E400 ** get_address_of__attributes_3() { return &____attributes_3; }
	inline void set__attributes_3(List_1_tE8856893C3BB37F3EF523D597A4152D5DCD8E400 * value)
	{
		____attributes_3 = value;
		Il2CppCodeGenWriteBarrier((&____attributes_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XELEMENTWRAPPER_T4C78ACAA2626B439978C1C03F9B61CFDF26557A9_H
#ifndef DATEFORMATHANDLING_TA9EAA5500D8763CC5596E54D538B8FFB782596FC_H
#define DATEFORMATHANDLING_TA9EAA5500D8763CC5596E54D538B8FFB782596FC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.DateFormatHandling
struct  DateFormatHandling_tA9EAA5500D8763CC5596E54D538B8FFB782596FC 
{
public:
	// System.Int32 Newtonsoft.Json.DateFormatHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DateFormatHandling_tA9EAA5500D8763CC5596E54D538B8FFB782596FC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATEFORMATHANDLING_TA9EAA5500D8763CC5596E54D538B8FFB782596FC_H
#ifndef DATEPARSEHANDLING_T92CAAF933C60203330E9D46525F2AB358233C385_H
#define DATEPARSEHANDLING_T92CAAF933C60203330E9D46525F2AB358233C385_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.DateParseHandling
struct  DateParseHandling_t92CAAF933C60203330E9D46525F2AB358233C385 
{
public:
	// System.Int32 Newtonsoft.Json.DateParseHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DateParseHandling_t92CAAF933C60203330E9D46525F2AB358233C385, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATEPARSEHANDLING_T92CAAF933C60203330E9D46525F2AB358233C385_H
#ifndef DATETIMEZONEHANDLING_T22BF997585D5EF7D990271F7353DF334589ACA33_H
#define DATETIMEZONEHANDLING_T22BF997585D5EF7D990271F7353DF334589ACA33_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.DateTimeZoneHandling
struct  DateTimeZoneHandling_t22BF997585D5EF7D990271F7353DF334589ACA33 
{
public:
	// System.Int32 Newtonsoft.Json.DateTimeZoneHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DateTimeZoneHandling_t22BF997585D5EF7D990271F7353DF334589ACA33, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEZONEHANDLING_T22BF997585D5EF7D990271F7353DF334589ACA33_H
#ifndef FLOATFORMATHANDLING_TD068FF718F516DEA58B961767C771E62A3BA4748_H
#define FLOATFORMATHANDLING_TD068FF718F516DEA58B961767C771E62A3BA4748_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.FloatFormatHandling
struct  FloatFormatHandling_tD068FF718F516DEA58B961767C771E62A3BA4748 
{
public:
	// System.Int32 Newtonsoft.Json.FloatFormatHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FloatFormatHandling_tD068FF718F516DEA58B961767C771E62A3BA4748, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATFORMATHANDLING_TD068FF718F516DEA58B961767C771E62A3BA4748_H
#ifndef FLOATPARSEHANDLING_T30D6C6D7BFF162AB10E59EE83DA342E206B6F205_H
#define FLOATPARSEHANDLING_T30D6C6D7BFF162AB10E59EE83DA342E206B6F205_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.FloatParseHandling
struct  FloatParseHandling_t30D6C6D7BFF162AB10E59EE83DA342E206B6F205 
{
public:
	// System.Int32 Newtonsoft.Json.FloatParseHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FloatParseHandling_t30D6C6D7BFF162AB10E59EE83DA342E206B6F205, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATPARSEHANDLING_T30D6C6D7BFF162AB10E59EE83DA342E206B6F205_H
#ifndef FORMATTING_TB318308F245B2BF136DFEFB2DDF54F7574396AD1_H
#define FORMATTING_TB318308F245B2BF136DFEFB2DDF54F7574396AD1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Formatting
struct  Formatting_tB318308F245B2BF136DFEFB2DDF54F7574396AD1 
{
public:
	// System.Int32 Newtonsoft.Json.Formatting::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Formatting_tB318308F245B2BF136DFEFB2DDF54F7574396AD1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORMATTING_TB318308F245B2BF136DFEFB2DDF54F7574396AD1_H
#ifndef JSONCONTAINERTYPE_T38001D9B9783470AD8F34ABFC658687DCFC7AC8A_H
#define JSONCONTAINERTYPE_T38001D9B9783470AD8F34ABFC658687DCFC7AC8A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonContainerType
struct  JsonContainerType_t38001D9B9783470AD8F34ABFC658687DCFC7AC8A 
{
public:
	// System.Int32 Newtonsoft.Json.JsonContainerType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(JsonContainerType_t38001D9B9783470AD8F34ABFC658687DCFC7AC8A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONCONTAINERTYPE_T38001D9B9783470AD8F34ABFC658687DCFC7AC8A_H
#ifndef STATE_TF808A376CD3F052C3C1BE586C60C34C534ADB010_H
#define STATE_TF808A376CD3F052C3C1BE586C60C34C534ADB010_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonReader_State
struct  State_tF808A376CD3F052C3C1BE586C60C34C534ADB010 
{
public:
	// System.Int32 Newtonsoft.Json.JsonReader_State::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(State_tF808A376CD3F052C3C1BE586C60C34C534ADB010, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATE_TF808A376CD3F052C3C1BE586C60C34C534ADB010_H
#ifndef JSONTOKEN_TF87828040BB18D17E612A0D4AA51B0995156561B_H
#define JSONTOKEN_TF87828040BB18D17E612A0D4AA51B0995156561B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonToken
struct  JsonToken_tF87828040BB18D17E612A0D4AA51B0995156561B 
{
public:
	// System.Int32 Newtonsoft.Json.JsonToken::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(JsonToken_tF87828040BB18D17E612A0D4AA51B0995156561B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONTOKEN_TF87828040BB18D17E612A0D4AA51B0995156561B_H
#ifndef STATE_T78B6EEE0137E4B552989C494AB9B9A6AE7F15F26_H
#define STATE_T78B6EEE0137E4B552989C494AB9B9A6AE7F15F26_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonWriter_State
struct  State_t78B6EEE0137E4B552989C494AB9B9A6AE7F15F26 
{
public:
	// System.Int32 Newtonsoft.Json.JsonWriter_State::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(State_t78B6EEE0137E4B552989C494AB9B9A6AE7F15F26, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATE_T78B6EEE0137E4B552989C494AB9B9A6AE7F15F26_H
#ifndef JARRAY_T1CE13821116F9B501573275C6BDD9FB254E65F11_H
#define JARRAY_T1CE13821116F9B501573275C6BDD9FB254E65F11_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JArray
struct  JArray_t1CE13821116F9B501573275C6BDD9FB254E65F11  : public JContainer_tF4CD2E574503C709DEF18A04B79B264B83746DAB
{
public:
	// System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JArray::_values
	List_1_t29FF67BC2CD2EEBC234D6E03E125122CC8730B5C * ____values_17;

public:
	inline static int32_t get_offset_of__values_17() { return static_cast<int32_t>(offsetof(JArray_t1CE13821116F9B501573275C6BDD9FB254E65F11, ____values_17)); }
	inline List_1_t29FF67BC2CD2EEBC234D6E03E125122CC8730B5C * get__values_17() const { return ____values_17; }
	inline List_1_t29FF67BC2CD2EEBC234D6E03E125122CC8730B5C ** get_address_of__values_17() { return &____values_17; }
	inline void set__values_17(List_1_t29FF67BC2CD2EEBC234D6E03E125122CC8730B5C * value)
	{
		____values_17 = value;
		Il2CppCodeGenWriteBarrier((&____values_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JARRAY_T1CE13821116F9B501573275C6BDD9FB254E65F11_H
#ifndef JCONSTRUCTOR_T9F741A9F4DC238BBAC6980237CBBEA9730BA9775_H
#define JCONSTRUCTOR_T9F741A9F4DC238BBAC6980237CBBEA9730BA9775_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JConstructor
struct  JConstructor_t9F741A9F4DC238BBAC6980237CBBEA9730BA9775  : public JContainer_tF4CD2E574503C709DEF18A04B79B264B83746DAB
{
public:
	// System.String Newtonsoft.Json.Linq.JConstructor::_name
	String_t* ____name_17;
	// System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JConstructor::_values
	List_1_t29FF67BC2CD2EEBC234D6E03E125122CC8730B5C * ____values_18;

public:
	inline static int32_t get_offset_of__name_17() { return static_cast<int32_t>(offsetof(JConstructor_t9F741A9F4DC238BBAC6980237CBBEA9730BA9775, ____name_17)); }
	inline String_t* get__name_17() const { return ____name_17; }
	inline String_t** get_address_of__name_17() { return &____name_17; }
	inline void set__name_17(String_t* value)
	{
		____name_17 = value;
		Il2CppCodeGenWriteBarrier((&____name_17), value);
	}

	inline static int32_t get_offset_of__values_18() { return static_cast<int32_t>(offsetof(JConstructor_t9F741A9F4DC238BBAC6980237CBBEA9730BA9775, ____values_18)); }
	inline List_1_t29FF67BC2CD2EEBC234D6E03E125122CC8730B5C * get__values_18() const { return ____values_18; }
	inline List_1_t29FF67BC2CD2EEBC234D6E03E125122CC8730B5C ** get_address_of__values_18() { return &____values_18; }
	inline void set__values_18(List_1_t29FF67BC2CD2EEBC234D6E03E125122CC8730B5C * value)
	{
		____values_18 = value;
		Il2CppCodeGenWriteBarrier((&____values_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JCONSTRUCTOR_T9F741A9F4DC238BBAC6980237CBBEA9730BA9775_H
#ifndef JOBJECT_T786AF07B1009334856B0362BBC48EEF68C81C585_H
#define JOBJECT_T786AF07B1009334856B0362BBC48EEF68C81C585_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JObject
struct  JObject_t786AF07B1009334856B0362BBC48EEF68C81C585  : public JContainer_tF4CD2E574503C709DEF18A04B79B264B83746DAB
{
public:
	// Newtonsoft.Json.Linq.JPropertyKeyedCollection Newtonsoft.Json.Linq.JObject::_properties
	JPropertyKeyedCollection_t013DFBF6A88616F3C011C00B60291F2EAB75483D * ____properties_17;
	// System.ComponentModel.PropertyChangedEventHandler Newtonsoft.Json.Linq.JObject::PropertyChanged
	PropertyChangedEventHandler_t617E98E1876A8EB394D2B329340CE02D21FFFC82 * ___PropertyChanged_18;
	// System.ComponentModel.PropertyChangingEventHandler Newtonsoft.Json.Linq.JObject::PropertyChanging
	PropertyChangingEventHandler_tEE60268216C5CB90375F55934CCCDEF654BA7EA3 * ___PropertyChanging_19;

public:
	inline static int32_t get_offset_of__properties_17() { return static_cast<int32_t>(offsetof(JObject_t786AF07B1009334856B0362BBC48EEF68C81C585, ____properties_17)); }
	inline JPropertyKeyedCollection_t013DFBF6A88616F3C011C00B60291F2EAB75483D * get__properties_17() const { return ____properties_17; }
	inline JPropertyKeyedCollection_t013DFBF6A88616F3C011C00B60291F2EAB75483D ** get_address_of__properties_17() { return &____properties_17; }
	inline void set__properties_17(JPropertyKeyedCollection_t013DFBF6A88616F3C011C00B60291F2EAB75483D * value)
	{
		____properties_17 = value;
		Il2CppCodeGenWriteBarrier((&____properties_17), value);
	}

	inline static int32_t get_offset_of_PropertyChanged_18() { return static_cast<int32_t>(offsetof(JObject_t786AF07B1009334856B0362BBC48EEF68C81C585, ___PropertyChanged_18)); }
	inline PropertyChangedEventHandler_t617E98E1876A8EB394D2B329340CE02D21FFFC82 * get_PropertyChanged_18() const { return ___PropertyChanged_18; }
	inline PropertyChangedEventHandler_t617E98E1876A8EB394D2B329340CE02D21FFFC82 ** get_address_of_PropertyChanged_18() { return &___PropertyChanged_18; }
	inline void set_PropertyChanged_18(PropertyChangedEventHandler_t617E98E1876A8EB394D2B329340CE02D21FFFC82 * value)
	{
		___PropertyChanged_18 = value;
		Il2CppCodeGenWriteBarrier((&___PropertyChanged_18), value);
	}

	inline static int32_t get_offset_of_PropertyChanging_19() { return static_cast<int32_t>(offsetof(JObject_t786AF07B1009334856B0362BBC48EEF68C81C585, ___PropertyChanging_19)); }
	inline PropertyChangingEventHandler_tEE60268216C5CB90375F55934CCCDEF654BA7EA3 * get_PropertyChanging_19() const { return ___PropertyChanging_19; }
	inline PropertyChangingEventHandler_tEE60268216C5CB90375F55934CCCDEF654BA7EA3 ** get_address_of_PropertyChanging_19() { return &___PropertyChanging_19; }
	inline void set_PropertyChanging_19(PropertyChangingEventHandler_tEE60268216C5CB90375F55934CCCDEF654BA7EA3 * value)
	{
		___PropertyChanging_19 = value;
		Il2CppCodeGenWriteBarrier((&___PropertyChanging_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOBJECT_T786AF07B1009334856B0362BBC48EEF68C81C585_H
#ifndef U3CGETENUMERATORU3ED__58_TF9BB600B8070D5230EC56222EB5B2CA2802A7FB8_H
#define U3CGETENUMERATORU3ED__58_TF9BB600B8070D5230EC56222EB5B2CA2802A7FB8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JObject_<GetEnumerator>d__58
struct  U3CGetEnumeratorU3Ed__58_tF9BB600B8070D5230EC56222EB5B2CA2802A7FB8  : public RuntimeObject
{
public:
	// System.Int32 Newtonsoft.Json.Linq.JObject_<GetEnumerator>d__58::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JObject_<GetEnumerator>d__58::<>2__current
	KeyValuePair_2_tFAF037D8F6143C33EA6D34A46D518C184C5FAC3C  ___U3CU3E2__current_1;
	// Newtonsoft.Json.Linq.JObject Newtonsoft.Json.Linq.JObject_<GetEnumerator>d__58::<>4__this
	JObject_t786AF07B1009334856B0362BBC48EEF68C81C585 * ___U3CU3E4__this_2;
	// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JObject_<GetEnumerator>d__58::<>7__wrap1
	RuntimeObject* ___U3CU3E7__wrap1_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__58_tF9BB600B8070D5230EC56222EB5B2CA2802A7FB8, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__58_tF9BB600B8070D5230EC56222EB5B2CA2802A7FB8, ___U3CU3E2__current_1)); }
	inline KeyValuePair_2_tFAF037D8F6143C33EA6D34A46D518C184C5FAC3C  get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline KeyValuePair_2_tFAF037D8F6143C33EA6D34A46D518C184C5FAC3C * get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(KeyValuePair_2_tFAF037D8F6143C33EA6D34A46D518C184C5FAC3C  value)
	{
		___U3CU3E2__current_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__58_tF9BB600B8070D5230EC56222EB5B2CA2802A7FB8, ___U3CU3E4__this_2)); }
	inline JObject_t786AF07B1009334856B0362BBC48EEF68C81C585 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline JObject_t786AF07B1009334856B0362BBC48EEF68C81C585 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(JObject_t786AF07B1009334856B0362BBC48EEF68C81C585 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_3() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__58_tF9BB600B8070D5230EC56222EB5B2CA2802A7FB8, ___U3CU3E7__wrap1_3)); }
	inline RuntimeObject* get_U3CU3E7__wrap1_3() const { return ___U3CU3E7__wrap1_3; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap1_3() { return &___U3CU3E7__wrap1_3; }
	inline void set_U3CU3E7__wrap1_3(RuntimeObject* value)
	{
		___U3CU3E7__wrap1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap1_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETENUMERATORU3ED__58_TF9BB600B8070D5230EC56222EB5B2CA2802A7FB8_H
#ifndef JPROPERTY_T127765B5AB6D281C5B77FAF5A4F26BB33C89398A_H
#define JPROPERTY_T127765B5AB6D281C5B77FAF5A4F26BB33C89398A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JProperty
struct  JProperty_t127765B5AB6D281C5B77FAF5A4F26BB33C89398A  : public JContainer_tF4CD2E574503C709DEF18A04B79B264B83746DAB
{
public:
	// Newtonsoft.Json.Linq.JProperty_JPropertyList Newtonsoft.Json.Linq.JProperty::_content
	JPropertyList_tAC5DCFB03C7879ED6FCFD74977E76474C918200F * ____content_17;
	// System.String Newtonsoft.Json.Linq.JProperty::_name
	String_t* ____name_18;

public:
	inline static int32_t get_offset_of__content_17() { return static_cast<int32_t>(offsetof(JProperty_t127765B5AB6D281C5B77FAF5A4F26BB33C89398A, ____content_17)); }
	inline JPropertyList_tAC5DCFB03C7879ED6FCFD74977E76474C918200F * get__content_17() const { return ____content_17; }
	inline JPropertyList_tAC5DCFB03C7879ED6FCFD74977E76474C918200F ** get_address_of__content_17() { return &____content_17; }
	inline void set__content_17(JPropertyList_tAC5DCFB03C7879ED6FCFD74977E76474C918200F * value)
	{
		____content_17 = value;
		Il2CppCodeGenWriteBarrier((&____content_17), value);
	}

	inline static int32_t get_offset_of__name_18() { return static_cast<int32_t>(offsetof(JProperty_t127765B5AB6D281C5B77FAF5A4F26BB33C89398A, ____name_18)); }
	inline String_t* get__name_18() const { return ____name_18; }
	inline String_t** get_address_of__name_18() { return &____name_18; }
	inline void set__name_18(String_t* value)
	{
		____name_18 = value;
		Il2CppCodeGenWriteBarrier((&____name_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JPROPERTY_T127765B5AB6D281C5B77FAF5A4F26BB33C89398A_H
#ifndef JTOKENTYPE_T6E74963952426FE99DA37BAEFD84A99710412C9E_H
#define JTOKENTYPE_T6E74963952426FE99DA37BAEFD84A99710412C9E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JTokenType
struct  JTokenType_t6E74963952426FE99DA37BAEFD84A99710412C9E 
{
public:
	// System.Int32 Newtonsoft.Json.Linq.JTokenType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(JTokenType_t6E74963952426FE99DA37BAEFD84A99710412C9E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JTOKENTYPE_T6E74963952426FE99DA37BAEFD84A99710412C9E_H
#ifndef ARRAYINDEXFILTER_T4B8AB90AF78B3976E09BEA34466153A1CDE2E12B_H
#define ARRAYINDEXFILTER_T4B8AB90AF78B3976E09BEA34466153A1CDE2E12B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JsonPath.ArrayIndexFilter
struct  ArrayIndexFilter_t4B8AB90AF78B3976E09BEA34466153A1CDE2E12B  : public PathFilter_tF860A9811AE814CCD54914A44C2A08AA07FECA80
{
public:
	// System.Nullable`1<System.Int32> Newtonsoft.Json.Linq.JsonPath.ArrayIndexFilter::<Index>k__BackingField
	Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  ___U3CIndexU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CIndexU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ArrayIndexFilter_t4B8AB90AF78B3976E09BEA34466153A1CDE2E12B, ___U3CIndexU3Ek__BackingField_0)); }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  get_U3CIndexU3Ek__BackingField_0() const { return ___U3CIndexU3Ek__BackingField_0; }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB * get_address_of_U3CIndexU3Ek__BackingField_0() { return &___U3CIndexU3Ek__BackingField_0; }
	inline void set_U3CIndexU3Ek__BackingField_0(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  value)
	{
		___U3CIndexU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRAYINDEXFILTER_T4B8AB90AF78B3976E09BEA34466153A1CDE2E12B_H
#ifndef U3CEXECUTEFILTERU3ED__4_TC82AE708D5CE2B8917E4AA90413E6CA8BA574131_H
#define U3CEXECUTEFILTERU3ED__4_TC82AE708D5CE2B8917E4AA90413E6CA8BA574131_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JsonPath.ArrayMultipleIndexFilter_<ExecuteFilter>d__4
struct  U3CExecuteFilterU3Ed__4_tC82AE708D5CE2B8917E4AA90413E6CA8BA574131  : public RuntimeObject
{
public:
	// System.Int32 Newtonsoft.Json.Linq.JsonPath.ArrayMultipleIndexFilter_<ExecuteFilter>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JsonPath.ArrayMultipleIndexFilter_<ExecuteFilter>d__4::<>2__current
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * ___U3CU3E2__current_1;
	// System.Int32 Newtonsoft.Json.Linq.JsonPath.ArrayMultipleIndexFilter_<ExecuteFilter>d__4::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JsonPath.ArrayMultipleIndexFilter_<ExecuteFilter>d__4::current
	RuntimeObject* ___current_3;
	// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JsonPath.ArrayMultipleIndexFilter_<ExecuteFilter>d__4::<>3__current
	RuntimeObject* ___U3CU3E3__current_4;
	// Newtonsoft.Json.Linq.JsonPath.ArrayMultipleIndexFilter Newtonsoft.Json.Linq.JsonPath.ArrayMultipleIndexFilter_<ExecuteFilter>d__4::<>4__this
	ArrayMultipleIndexFilter_tF9FF764DDCB79A819281D799112F6E930B75E99F * ___U3CU3E4__this_5;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JsonPath.ArrayMultipleIndexFilter_<ExecuteFilter>d__4::<t>5__1
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * ___U3CtU3E5__1_6;
	// System.Boolean Newtonsoft.Json.Linq.JsonPath.ArrayMultipleIndexFilter_<ExecuteFilter>d__4::errorWhenNoMatch
	bool ___errorWhenNoMatch_7;
	// System.Boolean Newtonsoft.Json.Linq.JsonPath.ArrayMultipleIndexFilter_<ExecuteFilter>d__4::<>3__errorWhenNoMatch
	bool ___U3CU3E3__errorWhenNoMatch_8;
	// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JsonPath.ArrayMultipleIndexFilter_<ExecuteFilter>d__4::<>7__wrap1
	RuntimeObject* ___U3CU3E7__wrap1_9;
	// System.Collections.Generic.List`1_Enumerator<System.Int32> Newtonsoft.Json.Linq.JsonPath.ArrayMultipleIndexFilter_<ExecuteFilter>d__4::<>7__wrap2
	Enumerator_t52B74DE77FB2834C7A6E4DEFC01F9E5818235AB2  ___U3CU3E7__wrap2_10;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_tC82AE708D5CE2B8917E4AA90413E6CA8BA574131, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_tC82AE708D5CE2B8917E4AA90413E6CA8BA574131, ___U3CU3E2__current_1)); }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_tC82AE708D5CE2B8917E4AA90413E6CA8BA574131, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_tC82AE708D5CE2B8917E4AA90413E6CA8BA574131, ___current_3)); }
	inline RuntimeObject* get_current_3() const { return ___current_3; }
	inline RuntimeObject** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject* value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__current_4() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_tC82AE708D5CE2B8917E4AA90413E6CA8BA574131, ___U3CU3E3__current_4)); }
	inline RuntimeObject* get_U3CU3E3__current_4() const { return ___U3CU3E3__current_4; }
	inline RuntimeObject** get_address_of_U3CU3E3__current_4() { return &___U3CU3E3__current_4; }
	inline void set_U3CU3E3__current_4(RuntimeObject* value)
	{
		___U3CU3E3__current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__current_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_5() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_tC82AE708D5CE2B8917E4AA90413E6CA8BA574131, ___U3CU3E4__this_5)); }
	inline ArrayMultipleIndexFilter_tF9FF764DDCB79A819281D799112F6E930B75E99F * get_U3CU3E4__this_5() const { return ___U3CU3E4__this_5; }
	inline ArrayMultipleIndexFilter_tF9FF764DDCB79A819281D799112F6E930B75E99F ** get_address_of_U3CU3E4__this_5() { return &___U3CU3E4__this_5; }
	inline void set_U3CU3E4__this_5(ArrayMultipleIndexFilter_tF9FF764DDCB79A819281D799112F6E930B75E99F * value)
	{
		___U3CU3E4__this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_5), value);
	}

	inline static int32_t get_offset_of_U3CtU3E5__1_6() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_tC82AE708D5CE2B8917E4AA90413E6CA8BA574131, ___U3CtU3E5__1_6)); }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * get_U3CtU3E5__1_6() const { return ___U3CtU3E5__1_6; }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 ** get_address_of_U3CtU3E5__1_6() { return &___U3CtU3E5__1_6; }
	inline void set_U3CtU3E5__1_6(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * value)
	{
		___U3CtU3E5__1_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtU3E5__1_6), value);
	}

	inline static int32_t get_offset_of_errorWhenNoMatch_7() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_tC82AE708D5CE2B8917E4AA90413E6CA8BA574131, ___errorWhenNoMatch_7)); }
	inline bool get_errorWhenNoMatch_7() const { return ___errorWhenNoMatch_7; }
	inline bool* get_address_of_errorWhenNoMatch_7() { return &___errorWhenNoMatch_7; }
	inline void set_errorWhenNoMatch_7(bool value)
	{
		___errorWhenNoMatch_7 = value;
	}

	inline static int32_t get_offset_of_U3CU3E3__errorWhenNoMatch_8() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_tC82AE708D5CE2B8917E4AA90413E6CA8BA574131, ___U3CU3E3__errorWhenNoMatch_8)); }
	inline bool get_U3CU3E3__errorWhenNoMatch_8() const { return ___U3CU3E3__errorWhenNoMatch_8; }
	inline bool* get_address_of_U3CU3E3__errorWhenNoMatch_8() { return &___U3CU3E3__errorWhenNoMatch_8; }
	inline void set_U3CU3E3__errorWhenNoMatch_8(bool value)
	{
		___U3CU3E3__errorWhenNoMatch_8 = value;
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_9() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_tC82AE708D5CE2B8917E4AA90413E6CA8BA574131, ___U3CU3E7__wrap1_9)); }
	inline RuntimeObject* get_U3CU3E7__wrap1_9() const { return ___U3CU3E7__wrap1_9; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap1_9() { return &___U3CU3E7__wrap1_9; }
	inline void set_U3CU3E7__wrap1_9(RuntimeObject* value)
	{
		___U3CU3E7__wrap1_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap1_9), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap2_10() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_tC82AE708D5CE2B8917E4AA90413E6CA8BA574131, ___U3CU3E7__wrap2_10)); }
	inline Enumerator_t52B74DE77FB2834C7A6E4DEFC01F9E5818235AB2  get_U3CU3E7__wrap2_10() const { return ___U3CU3E7__wrap2_10; }
	inline Enumerator_t52B74DE77FB2834C7A6E4DEFC01F9E5818235AB2 * get_address_of_U3CU3E7__wrap2_10() { return &___U3CU3E7__wrap2_10; }
	inline void set_U3CU3E7__wrap2_10(Enumerator_t52B74DE77FB2834C7A6E4DEFC01F9E5818235AB2  value)
	{
		___U3CU3E7__wrap2_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CEXECUTEFILTERU3ED__4_TC82AE708D5CE2B8917E4AA90413E6CA8BA574131_H
#ifndef ARRAYSLICEFILTER_TC5035758E75DB3EF9EAA182D561A726F46397BE3_H
#define ARRAYSLICEFILTER_TC5035758E75DB3EF9EAA182D561A726F46397BE3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JsonPath.ArraySliceFilter
struct  ArraySliceFilter_tC5035758E75DB3EF9EAA182D561A726F46397BE3  : public PathFilter_tF860A9811AE814CCD54914A44C2A08AA07FECA80
{
public:
	// System.Nullable`1<System.Int32> Newtonsoft.Json.Linq.JsonPath.ArraySliceFilter::<Start>k__BackingField
	Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  ___U3CStartU3Ek__BackingField_0;
	// System.Nullable`1<System.Int32> Newtonsoft.Json.Linq.JsonPath.ArraySliceFilter::<End>k__BackingField
	Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  ___U3CEndU3Ek__BackingField_1;
	// System.Nullable`1<System.Int32> Newtonsoft.Json.Linq.JsonPath.ArraySliceFilter::<Step>k__BackingField
	Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  ___U3CStepU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CStartU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ArraySliceFilter_tC5035758E75DB3EF9EAA182D561A726F46397BE3, ___U3CStartU3Ek__BackingField_0)); }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  get_U3CStartU3Ek__BackingField_0() const { return ___U3CStartU3Ek__BackingField_0; }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB * get_address_of_U3CStartU3Ek__BackingField_0() { return &___U3CStartU3Ek__BackingField_0; }
	inline void set_U3CStartU3Ek__BackingField_0(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  value)
	{
		___U3CStartU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CEndU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ArraySliceFilter_tC5035758E75DB3EF9EAA182D561A726F46397BE3, ___U3CEndU3Ek__BackingField_1)); }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  get_U3CEndU3Ek__BackingField_1() const { return ___U3CEndU3Ek__BackingField_1; }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB * get_address_of_U3CEndU3Ek__BackingField_1() { return &___U3CEndU3Ek__BackingField_1; }
	inline void set_U3CEndU3Ek__BackingField_1(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  value)
	{
		___U3CEndU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CStepU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ArraySliceFilter_tC5035758E75DB3EF9EAA182D561A726F46397BE3, ___U3CStepU3Ek__BackingField_2)); }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  get_U3CStepU3Ek__BackingField_2() const { return ___U3CStepU3Ek__BackingField_2; }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB * get_address_of_U3CStepU3Ek__BackingField_2() { return &___U3CStepU3Ek__BackingField_2; }
	inline void set_U3CStepU3Ek__BackingField_2(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  value)
	{
		___U3CStepU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRAYSLICEFILTER_TC5035758E75DB3EF9EAA182D561A726F46397BE3_H
#ifndef U3CEXECUTEFILTERU3ED__4_T0C1DEC1C597C26BA75BE873F98008D01240AE9BB_H
#define U3CEXECUTEFILTERU3ED__4_T0C1DEC1C597C26BA75BE873F98008D01240AE9BB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JsonPath.FieldMultipleFilter_<ExecuteFilter>d__4
struct  U3CExecuteFilterU3Ed__4_t0C1DEC1C597C26BA75BE873F98008D01240AE9BB  : public RuntimeObject
{
public:
	// System.Int32 Newtonsoft.Json.Linq.JsonPath.FieldMultipleFilter_<ExecuteFilter>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JsonPath.FieldMultipleFilter_<ExecuteFilter>d__4::<>2__current
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * ___U3CU3E2__current_1;
	// System.Int32 Newtonsoft.Json.Linq.JsonPath.FieldMultipleFilter_<ExecuteFilter>d__4::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JsonPath.FieldMultipleFilter_<ExecuteFilter>d__4::current
	RuntimeObject* ___current_3;
	// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JsonPath.FieldMultipleFilter_<ExecuteFilter>d__4::<>3__current
	RuntimeObject* ___U3CU3E3__current_4;
	// Newtonsoft.Json.Linq.JsonPath.FieldMultipleFilter Newtonsoft.Json.Linq.JsonPath.FieldMultipleFilter_<ExecuteFilter>d__4::<>4__this
	FieldMultipleFilter_tE61362AE319B8C510FD11900AD0A0B79EC08CD6B * ___U3CU3E4__this_5;
	// Newtonsoft.Json.Linq.JObject Newtonsoft.Json.Linq.JsonPath.FieldMultipleFilter_<ExecuteFilter>d__4::<o>5__1
	JObject_t786AF07B1009334856B0362BBC48EEF68C81C585 * ___U3CoU3E5__1_6;
	// System.Boolean Newtonsoft.Json.Linq.JsonPath.FieldMultipleFilter_<ExecuteFilter>d__4::errorWhenNoMatch
	bool ___errorWhenNoMatch_7;
	// System.Boolean Newtonsoft.Json.Linq.JsonPath.FieldMultipleFilter_<ExecuteFilter>d__4::<>3__errorWhenNoMatch
	bool ___U3CU3E3__errorWhenNoMatch_8;
	// System.String Newtonsoft.Json.Linq.JsonPath.FieldMultipleFilter_<ExecuteFilter>d__4::<name>5__2
	String_t* ___U3CnameU3E5__2_9;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JsonPath.FieldMultipleFilter_<ExecuteFilter>d__4::<t>5__3
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * ___U3CtU3E5__3_10;
	// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JsonPath.FieldMultipleFilter_<ExecuteFilter>d__4::<>7__wrap1
	RuntimeObject* ___U3CU3E7__wrap1_11;
	// System.Collections.Generic.List`1_Enumerator<System.String> Newtonsoft.Json.Linq.JsonPath.FieldMultipleFilter_<ExecuteFilter>d__4::<>7__wrap2
	Enumerator_tBBAAE521602D26DCD42E467CF939632DC01EF813  ___U3CU3E7__wrap2_12;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t0C1DEC1C597C26BA75BE873F98008D01240AE9BB, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t0C1DEC1C597C26BA75BE873F98008D01240AE9BB, ___U3CU3E2__current_1)); }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t0C1DEC1C597C26BA75BE873F98008D01240AE9BB, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t0C1DEC1C597C26BA75BE873F98008D01240AE9BB, ___current_3)); }
	inline RuntimeObject* get_current_3() const { return ___current_3; }
	inline RuntimeObject** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject* value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__current_4() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t0C1DEC1C597C26BA75BE873F98008D01240AE9BB, ___U3CU3E3__current_4)); }
	inline RuntimeObject* get_U3CU3E3__current_4() const { return ___U3CU3E3__current_4; }
	inline RuntimeObject** get_address_of_U3CU3E3__current_4() { return &___U3CU3E3__current_4; }
	inline void set_U3CU3E3__current_4(RuntimeObject* value)
	{
		___U3CU3E3__current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__current_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_5() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t0C1DEC1C597C26BA75BE873F98008D01240AE9BB, ___U3CU3E4__this_5)); }
	inline FieldMultipleFilter_tE61362AE319B8C510FD11900AD0A0B79EC08CD6B * get_U3CU3E4__this_5() const { return ___U3CU3E4__this_5; }
	inline FieldMultipleFilter_tE61362AE319B8C510FD11900AD0A0B79EC08CD6B ** get_address_of_U3CU3E4__this_5() { return &___U3CU3E4__this_5; }
	inline void set_U3CU3E4__this_5(FieldMultipleFilter_tE61362AE319B8C510FD11900AD0A0B79EC08CD6B * value)
	{
		___U3CU3E4__this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_5), value);
	}

	inline static int32_t get_offset_of_U3CoU3E5__1_6() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t0C1DEC1C597C26BA75BE873F98008D01240AE9BB, ___U3CoU3E5__1_6)); }
	inline JObject_t786AF07B1009334856B0362BBC48EEF68C81C585 * get_U3CoU3E5__1_6() const { return ___U3CoU3E5__1_6; }
	inline JObject_t786AF07B1009334856B0362BBC48EEF68C81C585 ** get_address_of_U3CoU3E5__1_6() { return &___U3CoU3E5__1_6; }
	inline void set_U3CoU3E5__1_6(JObject_t786AF07B1009334856B0362BBC48EEF68C81C585 * value)
	{
		___U3CoU3E5__1_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CoU3E5__1_6), value);
	}

	inline static int32_t get_offset_of_errorWhenNoMatch_7() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t0C1DEC1C597C26BA75BE873F98008D01240AE9BB, ___errorWhenNoMatch_7)); }
	inline bool get_errorWhenNoMatch_7() const { return ___errorWhenNoMatch_7; }
	inline bool* get_address_of_errorWhenNoMatch_7() { return &___errorWhenNoMatch_7; }
	inline void set_errorWhenNoMatch_7(bool value)
	{
		___errorWhenNoMatch_7 = value;
	}

	inline static int32_t get_offset_of_U3CU3E3__errorWhenNoMatch_8() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t0C1DEC1C597C26BA75BE873F98008D01240AE9BB, ___U3CU3E3__errorWhenNoMatch_8)); }
	inline bool get_U3CU3E3__errorWhenNoMatch_8() const { return ___U3CU3E3__errorWhenNoMatch_8; }
	inline bool* get_address_of_U3CU3E3__errorWhenNoMatch_8() { return &___U3CU3E3__errorWhenNoMatch_8; }
	inline void set_U3CU3E3__errorWhenNoMatch_8(bool value)
	{
		___U3CU3E3__errorWhenNoMatch_8 = value;
	}

	inline static int32_t get_offset_of_U3CnameU3E5__2_9() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t0C1DEC1C597C26BA75BE873F98008D01240AE9BB, ___U3CnameU3E5__2_9)); }
	inline String_t* get_U3CnameU3E5__2_9() const { return ___U3CnameU3E5__2_9; }
	inline String_t** get_address_of_U3CnameU3E5__2_9() { return &___U3CnameU3E5__2_9; }
	inline void set_U3CnameU3E5__2_9(String_t* value)
	{
		___U3CnameU3E5__2_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3E5__2_9), value);
	}

	inline static int32_t get_offset_of_U3CtU3E5__3_10() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t0C1DEC1C597C26BA75BE873F98008D01240AE9BB, ___U3CtU3E5__3_10)); }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * get_U3CtU3E5__3_10() const { return ___U3CtU3E5__3_10; }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 ** get_address_of_U3CtU3E5__3_10() { return &___U3CtU3E5__3_10; }
	inline void set_U3CtU3E5__3_10(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * value)
	{
		___U3CtU3E5__3_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtU3E5__3_10), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_11() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t0C1DEC1C597C26BA75BE873F98008D01240AE9BB, ___U3CU3E7__wrap1_11)); }
	inline RuntimeObject* get_U3CU3E7__wrap1_11() const { return ___U3CU3E7__wrap1_11; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap1_11() { return &___U3CU3E7__wrap1_11; }
	inline void set_U3CU3E7__wrap1_11(RuntimeObject* value)
	{
		___U3CU3E7__wrap1_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap1_11), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap2_12() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t0C1DEC1C597C26BA75BE873F98008D01240AE9BB, ___U3CU3E7__wrap2_12)); }
	inline Enumerator_tBBAAE521602D26DCD42E467CF939632DC01EF813  get_U3CU3E7__wrap2_12() const { return ___U3CU3E7__wrap2_12; }
	inline Enumerator_tBBAAE521602D26DCD42E467CF939632DC01EF813 * get_address_of_U3CU3E7__wrap2_12() { return &___U3CU3E7__wrap2_12; }
	inline void set_U3CU3E7__wrap2_12(Enumerator_tBBAAE521602D26DCD42E467CF939632DC01EF813  value)
	{
		___U3CU3E7__wrap2_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CEXECUTEFILTERU3ED__4_T0C1DEC1C597C26BA75BE873F98008D01240AE9BB_H
#ifndef QUERYOPERATOR_T91AB4C16055C42C3D23EBCE0B21564266F4B8130_H
#define QUERYOPERATOR_T91AB4C16055C42C3D23EBCE0B21564266F4B8130_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JsonPath.QueryOperator
struct  QueryOperator_t91AB4C16055C42C3D23EBCE0B21564266F4B8130 
{
public:
	// System.Int32 Newtonsoft.Json.Linq.JsonPath.QueryOperator::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(QueryOperator_t91AB4C16055C42C3D23EBCE0B21564266F4B8130, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUERYOPERATOR_T91AB4C16055C42C3D23EBCE0B21564266F4B8130_H
#ifndef STRINGESCAPEHANDLING_T3C4B7E5DC42939F797058D218DD0312E45D71C93_H
#define STRINGESCAPEHANDLING_T3C4B7E5DC42939F797058D218DD0312E45D71C93_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.StringEscapeHandling
struct  StringEscapeHandling_t3C4B7E5DC42939F797058D218DD0312E45D71C93 
{
public:
	// System.Int32 Newtonsoft.Json.StringEscapeHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StringEscapeHandling_t3C4B7E5DC42939F797058D218DD0312E45D71C93, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGESCAPEHANDLING_T3C4B7E5DC42939F797058D218DD0312E45D71C93_H
#ifndef DATETIMEKIND_T6BC23532930B812ABFCCEB2B61BC233712B180EE_H
#define DATETIMEKIND_T6BC23532930B812ABFCCEB2B61BC233712B180EE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeKind
struct  DateTimeKind_t6BC23532930B812ABFCCEB2B61BC233712B180EE 
{
public:
	// System.Int32 System.DateTimeKind::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DateTimeKind_t6BC23532930B812ABFCCEB2B61BC233712B180EE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEKIND_T6BC23532930B812ABFCCEB2B61BC233712B180EE_H
#ifndef DATETIMESTYLES_TD09B34DB3747CD91D8AAA1238C7595845715301E_H
#define DATETIMESTYLES_TD09B34DB3747CD91D8AAA1238C7595845715301E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Globalization.DateTimeStyles
struct  DateTimeStyles_tD09B34DB3747CD91D8AAA1238C7595845715301E 
{
public:
	// System.Int32 System.Globalization.DateTimeStyles::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DateTimeStyles_tD09B34DB3747CD91D8AAA1238C7595845715301E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMESTYLES_TD09B34DB3747CD91D8AAA1238C7595845715301E_H
#ifndef BSONBINARYWRITER_T3572084F4EE9D0ACA3EAE31EF1C0DF358D080D80_H
#define BSONBINARYWRITER_T3572084F4EE9D0ACA3EAE31EF1C0DF358D080D80_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Bson.BsonBinaryWriter
struct  BsonBinaryWriter_t3572084F4EE9D0ACA3EAE31EF1C0DF358D080D80  : public RuntimeObject
{
public:
	// System.IO.BinaryWriter Newtonsoft.Json.Bson.BsonBinaryWriter::_writer
	BinaryWriter_t1B8E78B6CACFBFB57BF140FB4DECE19AFD249CC3 * ____writer_1;
	// System.Byte[] Newtonsoft.Json.Bson.BsonBinaryWriter::_largeByteBuffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____largeByteBuffer_2;
	// System.DateTimeKind Newtonsoft.Json.Bson.BsonBinaryWriter::<DateTimeKindHandling>k__BackingField
	int32_t ___U3CDateTimeKindHandlingU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of__writer_1() { return static_cast<int32_t>(offsetof(BsonBinaryWriter_t3572084F4EE9D0ACA3EAE31EF1C0DF358D080D80, ____writer_1)); }
	inline BinaryWriter_t1B8E78B6CACFBFB57BF140FB4DECE19AFD249CC3 * get__writer_1() const { return ____writer_1; }
	inline BinaryWriter_t1B8E78B6CACFBFB57BF140FB4DECE19AFD249CC3 ** get_address_of__writer_1() { return &____writer_1; }
	inline void set__writer_1(BinaryWriter_t1B8E78B6CACFBFB57BF140FB4DECE19AFD249CC3 * value)
	{
		____writer_1 = value;
		Il2CppCodeGenWriteBarrier((&____writer_1), value);
	}

	inline static int32_t get_offset_of__largeByteBuffer_2() { return static_cast<int32_t>(offsetof(BsonBinaryWriter_t3572084F4EE9D0ACA3EAE31EF1C0DF358D080D80, ____largeByteBuffer_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__largeByteBuffer_2() const { return ____largeByteBuffer_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__largeByteBuffer_2() { return &____largeByteBuffer_2; }
	inline void set__largeByteBuffer_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____largeByteBuffer_2 = value;
		Il2CppCodeGenWriteBarrier((&____largeByteBuffer_2), value);
	}

	inline static int32_t get_offset_of_U3CDateTimeKindHandlingU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(BsonBinaryWriter_t3572084F4EE9D0ACA3EAE31EF1C0DF358D080D80, ___U3CDateTimeKindHandlingU3Ek__BackingField_3)); }
	inline int32_t get_U3CDateTimeKindHandlingU3Ek__BackingField_3() const { return ___U3CDateTimeKindHandlingU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CDateTimeKindHandlingU3Ek__BackingField_3() { return &___U3CDateTimeKindHandlingU3Ek__BackingField_3; }
	inline void set_U3CDateTimeKindHandlingU3Ek__BackingField_3(int32_t value)
	{
		___U3CDateTimeKindHandlingU3Ek__BackingField_3 = value;
	}
};

struct BsonBinaryWriter_t3572084F4EE9D0ACA3EAE31EF1C0DF358D080D80_StaticFields
{
public:
	// System.Text.Encoding Newtonsoft.Json.Bson.BsonBinaryWriter::Encoding
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___Encoding_0;

public:
	inline static int32_t get_offset_of_Encoding_0() { return static_cast<int32_t>(offsetof(BsonBinaryWriter_t3572084F4EE9D0ACA3EAE31EF1C0DF358D080D80_StaticFields, ___Encoding_0)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get_Encoding_0() const { return ___Encoding_0; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of_Encoding_0() { return &___Encoding_0; }
	inline void set_Encoding_0(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		___Encoding_0 = value;
		Il2CppCodeGenWriteBarrier((&___Encoding_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONBINARYWRITER_T3572084F4EE9D0ACA3EAE31EF1C0DF358D080D80_H
#ifndef CONTAINERCONTEXT_T863E55417E974D1C50EA7E90F62077FC15D5241F_H
#define CONTAINERCONTEXT_T863E55417E974D1C50EA7E90F62077FC15D5241F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Bson.BsonReader_ContainerContext
struct  ContainerContext_t863E55417E974D1C50EA7E90F62077FC15D5241F  : public RuntimeObject
{
public:
	// Newtonsoft.Json.Bson.BsonType Newtonsoft.Json.Bson.BsonReader_ContainerContext::Type
	int8_t ___Type_0;
	// System.Int32 Newtonsoft.Json.Bson.BsonReader_ContainerContext::Length
	int32_t ___Length_1;
	// System.Int32 Newtonsoft.Json.Bson.BsonReader_ContainerContext::Position
	int32_t ___Position_2;

public:
	inline static int32_t get_offset_of_Type_0() { return static_cast<int32_t>(offsetof(ContainerContext_t863E55417E974D1C50EA7E90F62077FC15D5241F, ___Type_0)); }
	inline int8_t get_Type_0() const { return ___Type_0; }
	inline int8_t* get_address_of_Type_0() { return &___Type_0; }
	inline void set_Type_0(int8_t value)
	{
		___Type_0 = value;
	}

	inline static int32_t get_offset_of_Length_1() { return static_cast<int32_t>(offsetof(ContainerContext_t863E55417E974D1C50EA7E90F62077FC15D5241F, ___Length_1)); }
	inline int32_t get_Length_1() const { return ___Length_1; }
	inline int32_t* get_address_of_Length_1() { return &___Length_1; }
	inline void set_Length_1(int32_t value)
	{
		___Length_1 = value;
	}

	inline static int32_t get_offset_of_Position_2() { return static_cast<int32_t>(offsetof(ContainerContext_t863E55417E974D1C50EA7E90F62077FC15D5241F, ___Position_2)); }
	inline int32_t get_Position_2() const { return ___Position_2; }
	inline int32_t* get_address_of_Position_2() { return &___Position_2; }
	inline void set_Position_2(int32_t value)
	{
		___Position_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTAINERCONTEXT_T863E55417E974D1C50EA7E90F62077FC15D5241F_H
#ifndef BSONVALUE_TB2A4632B6C4CAF0DB566AA14D13E0C0526328CF0_H
#define BSONVALUE_TB2A4632B6C4CAF0DB566AA14D13E0C0526328CF0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Bson.BsonValue
struct  BsonValue_tB2A4632B6C4CAF0DB566AA14D13E0C0526328CF0  : public BsonToken_tEB681A5D0E53F8005DC31BAEFDE6481BBE80C2D3
{
public:
	// System.Object Newtonsoft.Json.Bson.BsonValue::_value
	RuntimeObject * ____value_2;
	// Newtonsoft.Json.Bson.BsonType Newtonsoft.Json.Bson.BsonValue::_type
	int8_t ____type_3;

public:
	inline static int32_t get_offset_of__value_2() { return static_cast<int32_t>(offsetof(BsonValue_tB2A4632B6C4CAF0DB566AA14D13E0C0526328CF0, ____value_2)); }
	inline RuntimeObject * get__value_2() const { return ____value_2; }
	inline RuntimeObject ** get_address_of__value_2() { return &____value_2; }
	inline void set__value_2(RuntimeObject * value)
	{
		____value_2 = value;
		Il2CppCodeGenWriteBarrier((&____value_2), value);
	}

	inline static int32_t get_offset_of__type_3() { return static_cast<int32_t>(offsetof(BsonValue_tB2A4632B6C4CAF0DB566AA14D13E0C0526328CF0, ____type_3)); }
	inline int8_t get__type_3() const { return ____type_3; }
	inline int8_t* get_address_of__type_3() { return &____type_3; }
	inline void set__type_3(int8_t value)
	{
		____type_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONVALUE_TB2A4632B6C4CAF0DB566AA14D13E0C0526328CF0_H
#ifndef ISODATETIMECONVERTER_T6DC548D85347941EBC77B040DFD331DF40DE45DF_H
#define ISODATETIMECONVERTER_T6DC548D85347941EBC77B040DFD331DF40DE45DF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.IsoDateTimeConverter
struct  IsoDateTimeConverter_t6DC548D85347941EBC77B040DFD331DF40DE45DF  : public DateTimeConverterBase_tC0C56E1B50018AB9DF9B2F7F22FA0001AC1B1E71
{
public:
	// System.Globalization.DateTimeStyles Newtonsoft.Json.Converters.IsoDateTimeConverter::_dateTimeStyles
	int32_t ____dateTimeStyles_0;
	// System.String Newtonsoft.Json.Converters.IsoDateTimeConverter::_dateTimeFormat
	String_t* ____dateTimeFormat_1;
	// System.Globalization.CultureInfo Newtonsoft.Json.Converters.IsoDateTimeConverter::_culture
	CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * ____culture_2;

public:
	inline static int32_t get_offset_of__dateTimeStyles_0() { return static_cast<int32_t>(offsetof(IsoDateTimeConverter_t6DC548D85347941EBC77B040DFD331DF40DE45DF, ____dateTimeStyles_0)); }
	inline int32_t get__dateTimeStyles_0() const { return ____dateTimeStyles_0; }
	inline int32_t* get_address_of__dateTimeStyles_0() { return &____dateTimeStyles_0; }
	inline void set__dateTimeStyles_0(int32_t value)
	{
		____dateTimeStyles_0 = value;
	}

	inline static int32_t get_offset_of__dateTimeFormat_1() { return static_cast<int32_t>(offsetof(IsoDateTimeConverter_t6DC548D85347941EBC77B040DFD331DF40DE45DF, ____dateTimeFormat_1)); }
	inline String_t* get__dateTimeFormat_1() const { return ____dateTimeFormat_1; }
	inline String_t** get_address_of__dateTimeFormat_1() { return &____dateTimeFormat_1; }
	inline void set__dateTimeFormat_1(String_t* value)
	{
		____dateTimeFormat_1 = value;
		Il2CppCodeGenWriteBarrier((&____dateTimeFormat_1), value);
	}

	inline static int32_t get_offset_of__culture_2() { return static_cast<int32_t>(offsetof(IsoDateTimeConverter_t6DC548D85347941EBC77B040DFD331DF40DE45DF, ____culture_2)); }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * get__culture_2() const { return ____culture_2; }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F ** get_address_of__culture_2() { return &____culture_2; }
	inline void set__culture_2(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * value)
	{
		____culture_2 = value;
		Il2CppCodeGenWriteBarrier((&____culture_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ISODATETIMECONVERTER_T6DC548D85347941EBC77B040DFD331DF40DE45DF_H
#ifndef JSONPOSITION_TC0F9C56D38393456580595F4C40C0C62D6C4AD62_H
#define JSONPOSITION_TC0F9C56D38393456580595F4C40C0C62D6C4AD62_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonPosition
struct  JsonPosition_tC0F9C56D38393456580595F4C40C0C62D6C4AD62 
{
public:
	// Newtonsoft.Json.JsonContainerType Newtonsoft.Json.JsonPosition::Type
	int32_t ___Type_1;
	// System.Int32 Newtonsoft.Json.JsonPosition::Position
	int32_t ___Position_2;
	// System.String Newtonsoft.Json.JsonPosition::PropertyName
	String_t* ___PropertyName_3;
	// System.Boolean Newtonsoft.Json.JsonPosition::HasIndex
	bool ___HasIndex_4;

public:
	inline static int32_t get_offset_of_Type_1() { return static_cast<int32_t>(offsetof(JsonPosition_tC0F9C56D38393456580595F4C40C0C62D6C4AD62, ___Type_1)); }
	inline int32_t get_Type_1() const { return ___Type_1; }
	inline int32_t* get_address_of_Type_1() { return &___Type_1; }
	inline void set_Type_1(int32_t value)
	{
		___Type_1 = value;
	}

	inline static int32_t get_offset_of_Position_2() { return static_cast<int32_t>(offsetof(JsonPosition_tC0F9C56D38393456580595F4C40C0C62D6C4AD62, ___Position_2)); }
	inline int32_t get_Position_2() const { return ___Position_2; }
	inline int32_t* get_address_of_Position_2() { return &___Position_2; }
	inline void set_Position_2(int32_t value)
	{
		___Position_2 = value;
	}

	inline static int32_t get_offset_of_PropertyName_3() { return static_cast<int32_t>(offsetof(JsonPosition_tC0F9C56D38393456580595F4C40C0C62D6C4AD62, ___PropertyName_3)); }
	inline String_t* get_PropertyName_3() const { return ___PropertyName_3; }
	inline String_t** get_address_of_PropertyName_3() { return &___PropertyName_3; }
	inline void set_PropertyName_3(String_t* value)
	{
		___PropertyName_3 = value;
		Il2CppCodeGenWriteBarrier((&___PropertyName_3), value);
	}

	inline static int32_t get_offset_of_HasIndex_4() { return static_cast<int32_t>(offsetof(JsonPosition_tC0F9C56D38393456580595F4C40C0C62D6C4AD62, ___HasIndex_4)); }
	inline bool get_HasIndex_4() const { return ___HasIndex_4; }
	inline bool* get_address_of_HasIndex_4() { return &___HasIndex_4; }
	inline void set_HasIndex_4(bool value)
	{
		___HasIndex_4 = value;
	}
};

struct JsonPosition_tC0F9C56D38393456580595F4C40C0C62D6C4AD62_StaticFields
{
public:
	// System.Char[] Newtonsoft.Json.JsonPosition::SpecialCharacters
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___SpecialCharacters_0;

public:
	inline static int32_t get_offset_of_SpecialCharacters_0() { return static_cast<int32_t>(offsetof(JsonPosition_tC0F9C56D38393456580595F4C40C0C62D6C4AD62_StaticFields, ___SpecialCharacters_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_SpecialCharacters_0() const { return ___SpecialCharacters_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_SpecialCharacters_0() { return &___SpecialCharacters_0; }
	inline void set_SpecialCharacters_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___SpecialCharacters_0 = value;
		Il2CppCodeGenWriteBarrier((&___SpecialCharacters_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Newtonsoft.Json.JsonPosition
struct JsonPosition_tC0F9C56D38393456580595F4C40C0C62D6C4AD62_marshaled_pinvoke
{
	int32_t ___Type_1;
	int32_t ___Position_2;
	char* ___PropertyName_3;
	int32_t ___HasIndex_4;
};
// Native definition for COM marshalling of Newtonsoft.Json.JsonPosition
struct JsonPosition_tC0F9C56D38393456580595F4C40C0C62D6C4AD62_marshaled_com
{
	int32_t ___Type_1;
	int32_t ___Position_2;
	Il2CppChar* ___PropertyName_3;
	int32_t ___HasIndex_4;
};
#endif // JSONPOSITION_TC0F9C56D38393456580595F4C40C0C62D6C4AD62_H
#ifndef JVALUE_T69F069DA12BD51A8FEB36C2EDA5D258B7FE2C4BF_H
#define JVALUE_T69F069DA12BD51A8FEB36C2EDA5D258B7FE2C4BF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JValue
struct  JValue_t69F069DA12BD51A8FEB36C2EDA5D258B7FE2C4BF  : public JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02
{
public:
	// Newtonsoft.Json.Linq.JTokenType Newtonsoft.Json.Linq.JValue::_valueType
	int32_t ____valueType_13;
	// System.Object Newtonsoft.Json.Linq.JValue::_value
	RuntimeObject * ____value_14;

public:
	inline static int32_t get_offset_of__valueType_13() { return static_cast<int32_t>(offsetof(JValue_t69F069DA12BD51A8FEB36C2EDA5D258B7FE2C4BF, ____valueType_13)); }
	inline int32_t get__valueType_13() const { return ____valueType_13; }
	inline int32_t* get_address_of__valueType_13() { return &____valueType_13; }
	inline void set__valueType_13(int32_t value)
	{
		____valueType_13 = value;
	}

	inline static int32_t get_offset_of__value_14() { return static_cast<int32_t>(offsetof(JValue_t69F069DA12BD51A8FEB36C2EDA5D258B7FE2C4BF, ____value_14)); }
	inline RuntimeObject * get__value_14() const { return ____value_14; }
	inline RuntimeObject ** get_address_of__value_14() { return &____value_14; }
	inline void set__value_14(RuntimeObject * value)
	{
		____value_14 = value;
		Il2CppCodeGenWriteBarrier((&____value_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JVALUE_T69F069DA12BD51A8FEB36C2EDA5D258B7FE2C4BF_H
#ifndef QUERYEXPRESSION_T08B8769F946D3E05F6E9DB46393F0B9828244E7F_H
#define QUERYEXPRESSION_T08B8769F946D3E05F6E9DB46393F0B9828244E7F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JsonPath.QueryExpression
struct  QueryExpression_t08B8769F946D3E05F6E9DB46393F0B9828244E7F  : public RuntimeObject
{
public:
	// Newtonsoft.Json.Linq.JsonPath.QueryOperator Newtonsoft.Json.Linq.JsonPath.QueryExpression::<Operator>k__BackingField
	int32_t ___U3COperatorU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3COperatorU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(QueryExpression_t08B8769F946D3E05F6E9DB46393F0B9828244E7F, ___U3COperatorU3Ek__BackingField_0)); }
	inline int32_t get_U3COperatorU3Ek__BackingField_0() const { return ___U3COperatorU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3COperatorU3Ek__BackingField_0() { return &___U3COperatorU3Ek__BackingField_0; }
	inline void set_U3COperatorU3Ek__BackingField_0(int32_t value)
	{
		___U3COperatorU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUERYEXPRESSION_T08B8769F946D3E05F6E9DB46393F0B9828244E7F_H
#ifndef BSONBINARY_TD19C8F767A5AADB1A3B81A4BFA2ABECBC8082982_H
#define BSONBINARY_TD19C8F767A5AADB1A3B81A4BFA2ABECBC8082982_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Bson.BsonBinary
struct  BsonBinary_tD19C8F767A5AADB1A3B81A4BFA2ABECBC8082982  : public BsonValue_tB2A4632B6C4CAF0DB566AA14D13E0C0526328CF0
{
public:
	// Newtonsoft.Json.Bson.BsonBinaryType Newtonsoft.Json.Bson.BsonBinary::<BinaryType>k__BackingField
	uint8_t ___U3CBinaryTypeU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CBinaryTypeU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(BsonBinary_tD19C8F767A5AADB1A3B81A4BFA2ABECBC8082982, ___U3CBinaryTypeU3Ek__BackingField_4)); }
	inline uint8_t get_U3CBinaryTypeU3Ek__BackingField_4() const { return ___U3CBinaryTypeU3Ek__BackingField_4; }
	inline uint8_t* get_address_of_U3CBinaryTypeU3Ek__BackingField_4() { return &___U3CBinaryTypeU3Ek__BackingField_4; }
	inline void set_U3CBinaryTypeU3Ek__BackingField_4(uint8_t value)
	{
		___U3CBinaryTypeU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONBINARY_TD19C8F767A5AADB1A3B81A4BFA2ABECBC8082982_H
#ifndef BSONSTRING_T87A7877CE01E3E2FA2B118C4A617FA16159CBC0A_H
#define BSONSTRING_T87A7877CE01E3E2FA2B118C4A617FA16159CBC0A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Bson.BsonString
struct  BsonString_t87A7877CE01E3E2FA2B118C4A617FA16159CBC0A  : public BsonValue_tB2A4632B6C4CAF0DB566AA14D13E0C0526328CF0
{
public:
	// System.Int32 Newtonsoft.Json.Bson.BsonString::<ByteCount>k__BackingField
	int32_t ___U3CByteCountU3Ek__BackingField_4;
	// System.Boolean Newtonsoft.Json.Bson.BsonString::<IncludeLength>k__BackingField
	bool ___U3CIncludeLengthU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CByteCountU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(BsonString_t87A7877CE01E3E2FA2B118C4A617FA16159CBC0A, ___U3CByteCountU3Ek__BackingField_4)); }
	inline int32_t get_U3CByteCountU3Ek__BackingField_4() const { return ___U3CByteCountU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CByteCountU3Ek__BackingField_4() { return &___U3CByteCountU3Ek__BackingField_4; }
	inline void set_U3CByteCountU3Ek__BackingField_4(int32_t value)
	{
		___U3CByteCountU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CIncludeLengthU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(BsonString_t87A7877CE01E3E2FA2B118C4A617FA16159CBC0A, ___U3CIncludeLengthU3Ek__BackingField_5)); }
	inline bool get_U3CIncludeLengthU3Ek__BackingField_5() const { return ___U3CIncludeLengthU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CIncludeLengthU3Ek__BackingField_5() { return &___U3CIncludeLengthU3Ek__BackingField_5; }
	inline void set_U3CIncludeLengthU3Ek__BackingField_5(bool value)
	{
		___U3CIncludeLengthU3Ek__BackingField_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONSTRING_T87A7877CE01E3E2FA2B118C4A617FA16159CBC0A_H
#ifndef JSONREADER_T95FAA240CCEA60ED65C0B29721A2DA20512B5636_H
#define JSONREADER_T95FAA240CCEA60ED65C0B29721A2DA20512B5636_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonReader
struct  JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636  : public RuntimeObject
{
public:
	// Newtonsoft.Json.JsonToken Newtonsoft.Json.JsonReader::_tokenType
	int32_t ____tokenType_0;
	// System.Object Newtonsoft.Json.JsonReader::_value
	RuntimeObject * ____value_1;
	// System.Char Newtonsoft.Json.JsonReader::_quoteChar
	Il2CppChar ____quoteChar_2;
	// Newtonsoft.Json.JsonReader_State Newtonsoft.Json.JsonReader::_currentState
	int32_t ____currentState_3;
	// Newtonsoft.Json.JsonPosition Newtonsoft.Json.JsonReader::_currentPosition
	JsonPosition_tC0F9C56D38393456580595F4C40C0C62D6C4AD62  ____currentPosition_4;
	// System.Globalization.CultureInfo Newtonsoft.Json.JsonReader::_culture
	CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * ____culture_5;
	// Newtonsoft.Json.DateTimeZoneHandling Newtonsoft.Json.JsonReader::_dateTimeZoneHandling
	int32_t ____dateTimeZoneHandling_6;
	// System.Nullable`1<System.Int32> Newtonsoft.Json.JsonReader::_maxDepth
	Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  ____maxDepth_7;
	// System.Boolean Newtonsoft.Json.JsonReader::_hasExceededMaxDepth
	bool ____hasExceededMaxDepth_8;
	// Newtonsoft.Json.DateParseHandling Newtonsoft.Json.JsonReader::_dateParseHandling
	int32_t ____dateParseHandling_9;
	// Newtonsoft.Json.FloatParseHandling Newtonsoft.Json.JsonReader::_floatParseHandling
	int32_t ____floatParseHandling_10;
	// System.String Newtonsoft.Json.JsonReader::_dateFormatString
	String_t* ____dateFormatString_11;
	// System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition> Newtonsoft.Json.JsonReader::_stack
	List_1_tBF641BBAF7DFF674220860102C874E20DB3EB403 * ____stack_12;
	// System.Boolean Newtonsoft.Json.JsonReader::<CloseInput>k__BackingField
	bool ___U3CCloseInputU3Ek__BackingField_13;
	// System.Boolean Newtonsoft.Json.JsonReader::<SupportMultipleContent>k__BackingField
	bool ___U3CSupportMultipleContentU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of__tokenType_0() { return static_cast<int32_t>(offsetof(JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636, ____tokenType_0)); }
	inline int32_t get__tokenType_0() const { return ____tokenType_0; }
	inline int32_t* get_address_of__tokenType_0() { return &____tokenType_0; }
	inline void set__tokenType_0(int32_t value)
	{
		____tokenType_0 = value;
	}

	inline static int32_t get_offset_of__value_1() { return static_cast<int32_t>(offsetof(JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636, ____value_1)); }
	inline RuntimeObject * get__value_1() const { return ____value_1; }
	inline RuntimeObject ** get_address_of__value_1() { return &____value_1; }
	inline void set__value_1(RuntimeObject * value)
	{
		____value_1 = value;
		Il2CppCodeGenWriteBarrier((&____value_1), value);
	}

	inline static int32_t get_offset_of__quoteChar_2() { return static_cast<int32_t>(offsetof(JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636, ____quoteChar_2)); }
	inline Il2CppChar get__quoteChar_2() const { return ____quoteChar_2; }
	inline Il2CppChar* get_address_of__quoteChar_2() { return &____quoteChar_2; }
	inline void set__quoteChar_2(Il2CppChar value)
	{
		____quoteChar_2 = value;
	}

	inline static int32_t get_offset_of__currentState_3() { return static_cast<int32_t>(offsetof(JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636, ____currentState_3)); }
	inline int32_t get__currentState_3() const { return ____currentState_3; }
	inline int32_t* get_address_of__currentState_3() { return &____currentState_3; }
	inline void set__currentState_3(int32_t value)
	{
		____currentState_3 = value;
	}

	inline static int32_t get_offset_of__currentPosition_4() { return static_cast<int32_t>(offsetof(JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636, ____currentPosition_4)); }
	inline JsonPosition_tC0F9C56D38393456580595F4C40C0C62D6C4AD62  get__currentPosition_4() const { return ____currentPosition_4; }
	inline JsonPosition_tC0F9C56D38393456580595F4C40C0C62D6C4AD62 * get_address_of__currentPosition_4() { return &____currentPosition_4; }
	inline void set__currentPosition_4(JsonPosition_tC0F9C56D38393456580595F4C40C0C62D6C4AD62  value)
	{
		____currentPosition_4 = value;
	}

	inline static int32_t get_offset_of__culture_5() { return static_cast<int32_t>(offsetof(JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636, ____culture_5)); }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * get__culture_5() const { return ____culture_5; }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F ** get_address_of__culture_5() { return &____culture_5; }
	inline void set__culture_5(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * value)
	{
		____culture_5 = value;
		Il2CppCodeGenWriteBarrier((&____culture_5), value);
	}

	inline static int32_t get_offset_of__dateTimeZoneHandling_6() { return static_cast<int32_t>(offsetof(JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636, ____dateTimeZoneHandling_6)); }
	inline int32_t get__dateTimeZoneHandling_6() const { return ____dateTimeZoneHandling_6; }
	inline int32_t* get_address_of__dateTimeZoneHandling_6() { return &____dateTimeZoneHandling_6; }
	inline void set__dateTimeZoneHandling_6(int32_t value)
	{
		____dateTimeZoneHandling_6 = value;
	}

	inline static int32_t get_offset_of__maxDepth_7() { return static_cast<int32_t>(offsetof(JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636, ____maxDepth_7)); }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  get__maxDepth_7() const { return ____maxDepth_7; }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB * get_address_of__maxDepth_7() { return &____maxDepth_7; }
	inline void set__maxDepth_7(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  value)
	{
		____maxDepth_7 = value;
	}

	inline static int32_t get_offset_of__hasExceededMaxDepth_8() { return static_cast<int32_t>(offsetof(JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636, ____hasExceededMaxDepth_8)); }
	inline bool get__hasExceededMaxDepth_8() const { return ____hasExceededMaxDepth_8; }
	inline bool* get_address_of__hasExceededMaxDepth_8() { return &____hasExceededMaxDepth_8; }
	inline void set__hasExceededMaxDepth_8(bool value)
	{
		____hasExceededMaxDepth_8 = value;
	}

	inline static int32_t get_offset_of__dateParseHandling_9() { return static_cast<int32_t>(offsetof(JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636, ____dateParseHandling_9)); }
	inline int32_t get__dateParseHandling_9() const { return ____dateParseHandling_9; }
	inline int32_t* get_address_of__dateParseHandling_9() { return &____dateParseHandling_9; }
	inline void set__dateParseHandling_9(int32_t value)
	{
		____dateParseHandling_9 = value;
	}

	inline static int32_t get_offset_of__floatParseHandling_10() { return static_cast<int32_t>(offsetof(JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636, ____floatParseHandling_10)); }
	inline int32_t get__floatParseHandling_10() const { return ____floatParseHandling_10; }
	inline int32_t* get_address_of__floatParseHandling_10() { return &____floatParseHandling_10; }
	inline void set__floatParseHandling_10(int32_t value)
	{
		____floatParseHandling_10 = value;
	}

	inline static int32_t get_offset_of__dateFormatString_11() { return static_cast<int32_t>(offsetof(JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636, ____dateFormatString_11)); }
	inline String_t* get__dateFormatString_11() const { return ____dateFormatString_11; }
	inline String_t** get_address_of__dateFormatString_11() { return &____dateFormatString_11; }
	inline void set__dateFormatString_11(String_t* value)
	{
		____dateFormatString_11 = value;
		Il2CppCodeGenWriteBarrier((&____dateFormatString_11), value);
	}

	inline static int32_t get_offset_of__stack_12() { return static_cast<int32_t>(offsetof(JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636, ____stack_12)); }
	inline List_1_tBF641BBAF7DFF674220860102C874E20DB3EB403 * get__stack_12() const { return ____stack_12; }
	inline List_1_tBF641BBAF7DFF674220860102C874E20DB3EB403 ** get_address_of__stack_12() { return &____stack_12; }
	inline void set__stack_12(List_1_tBF641BBAF7DFF674220860102C874E20DB3EB403 * value)
	{
		____stack_12 = value;
		Il2CppCodeGenWriteBarrier((&____stack_12), value);
	}

	inline static int32_t get_offset_of_U3CCloseInputU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636, ___U3CCloseInputU3Ek__BackingField_13)); }
	inline bool get_U3CCloseInputU3Ek__BackingField_13() const { return ___U3CCloseInputU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CCloseInputU3Ek__BackingField_13() { return &___U3CCloseInputU3Ek__BackingField_13; }
	inline void set_U3CCloseInputU3Ek__BackingField_13(bool value)
	{
		___U3CCloseInputU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3CSupportMultipleContentU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636, ___U3CSupportMultipleContentU3Ek__BackingField_14)); }
	inline bool get_U3CSupportMultipleContentU3Ek__BackingField_14() const { return ___U3CSupportMultipleContentU3Ek__BackingField_14; }
	inline bool* get_address_of_U3CSupportMultipleContentU3Ek__BackingField_14() { return &___U3CSupportMultipleContentU3Ek__BackingField_14; }
	inline void set_U3CSupportMultipleContentU3Ek__BackingField_14(bool value)
	{
		___U3CSupportMultipleContentU3Ek__BackingField_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONREADER_T95FAA240CCEA60ED65C0B29721A2DA20512B5636_H
#ifndef JSONWRITER_T9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76_H
#define JSONWRITER_T9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonWriter
struct  JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition> Newtonsoft.Json.JsonWriter::_stack
	List_1_tBF641BBAF7DFF674220860102C874E20DB3EB403 * ____stack_2;
	// Newtonsoft.Json.JsonPosition Newtonsoft.Json.JsonWriter::_currentPosition
	JsonPosition_tC0F9C56D38393456580595F4C40C0C62D6C4AD62  ____currentPosition_3;
	// Newtonsoft.Json.JsonWriter_State Newtonsoft.Json.JsonWriter::_currentState
	int32_t ____currentState_4;
	// Newtonsoft.Json.Formatting Newtonsoft.Json.JsonWriter::_formatting
	int32_t ____formatting_5;
	// System.Boolean Newtonsoft.Json.JsonWriter::<CloseOutput>k__BackingField
	bool ___U3CCloseOutputU3Ek__BackingField_6;
	// Newtonsoft.Json.DateFormatHandling Newtonsoft.Json.JsonWriter::_dateFormatHandling
	int32_t ____dateFormatHandling_7;
	// Newtonsoft.Json.DateTimeZoneHandling Newtonsoft.Json.JsonWriter::_dateTimeZoneHandling
	int32_t ____dateTimeZoneHandling_8;
	// Newtonsoft.Json.StringEscapeHandling Newtonsoft.Json.JsonWriter::_stringEscapeHandling
	int32_t ____stringEscapeHandling_9;
	// Newtonsoft.Json.FloatFormatHandling Newtonsoft.Json.JsonWriter::_floatFormatHandling
	int32_t ____floatFormatHandling_10;
	// System.String Newtonsoft.Json.JsonWriter::_dateFormatString
	String_t* ____dateFormatString_11;
	// System.Globalization.CultureInfo Newtonsoft.Json.JsonWriter::_culture
	CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * ____culture_12;

public:
	inline static int32_t get_offset_of__stack_2() { return static_cast<int32_t>(offsetof(JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76, ____stack_2)); }
	inline List_1_tBF641BBAF7DFF674220860102C874E20DB3EB403 * get__stack_2() const { return ____stack_2; }
	inline List_1_tBF641BBAF7DFF674220860102C874E20DB3EB403 ** get_address_of__stack_2() { return &____stack_2; }
	inline void set__stack_2(List_1_tBF641BBAF7DFF674220860102C874E20DB3EB403 * value)
	{
		____stack_2 = value;
		Il2CppCodeGenWriteBarrier((&____stack_2), value);
	}

	inline static int32_t get_offset_of__currentPosition_3() { return static_cast<int32_t>(offsetof(JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76, ____currentPosition_3)); }
	inline JsonPosition_tC0F9C56D38393456580595F4C40C0C62D6C4AD62  get__currentPosition_3() const { return ____currentPosition_3; }
	inline JsonPosition_tC0F9C56D38393456580595F4C40C0C62D6C4AD62 * get_address_of__currentPosition_3() { return &____currentPosition_3; }
	inline void set__currentPosition_3(JsonPosition_tC0F9C56D38393456580595F4C40C0C62D6C4AD62  value)
	{
		____currentPosition_3 = value;
	}

	inline static int32_t get_offset_of__currentState_4() { return static_cast<int32_t>(offsetof(JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76, ____currentState_4)); }
	inline int32_t get__currentState_4() const { return ____currentState_4; }
	inline int32_t* get_address_of__currentState_4() { return &____currentState_4; }
	inline void set__currentState_4(int32_t value)
	{
		____currentState_4 = value;
	}

	inline static int32_t get_offset_of__formatting_5() { return static_cast<int32_t>(offsetof(JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76, ____formatting_5)); }
	inline int32_t get__formatting_5() const { return ____formatting_5; }
	inline int32_t* get_address_of__formatting_5() { return &____formatting_5; }
	inline void set__formatting_5(int32_t value)
	{
		____formatting_5 = value;
	}

	inline static int32_t get_offset_of_U3CCloseOutputU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76, ___U3CCloseOutputU3Ek__BackingField_6)); }
	inline bool get_U3CCloseOutputU3Ek__BackingField_6() const { return ___U3CCloseOutputU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CCloseOutputU3Ek__BackingField_6() { return &___U3CCloseOutputU3Ek__BackingField_6; }
	inline void set_U3CCloseOutputU3Ek__BackingField_6(bool value)
	{
		___U3CCloseOutputU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of__dateFormatHandling_7() { return static_cast<int32_t>(offsetof(JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76, ____dateFormatHandling_7)); }
	inline int32_t get__dateFormatHandling_7() const { return ____dateFormatHandling_7; }
	inline int32_t* get_address_of__dateFormatHandling_7() { return &____dateFormatHandling_7; }
	inline void set__dateFormatHandling_7(int32_t value)
	{
		____dateFormatHandling_7 = value;
	}

	inline static int32_t get_offset_of__dateTimeZoneHandling_8() { return static_cast<int32_t>(offsetof(JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76, ____dateTimeZoneHandling_8)); }
	inline int32_t get__dateTimeZoneHandling_8() const { return ____dateTimeZoneHandling_8; }
	inline int32_t* get_address_of__dateTimeZoneHandling_8() { return &____dateTimeZoneHandling_8; }
	inline void set__dateTimeZoneHandling_8(int32_t value)
	{
		____dateTimeZoneHandling_8 = value;
	}

	inline static int32_t get_offset_of__stringEscapeHandling_9() { return static_cast<int32_t>(offsetof(JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76, ____stringEscapeHandling_9)); }
	inline int32_t get__stringEscapeHandling_9() const { return ____stringEscapeHandling_9; }
	inline int32_t* get_address_of__stringEscapeHandling_9() { return &____stringEscapeHandling_9; }
	inline void set__stringEscapeHandling_9(int32_t value)
	{
		____stringEscapeHandling_9 = value;
	}

	inline static int32_t get_offset_of__floatFormatHandling_10() { return static_cast<int32_t>(offsetof(JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76, ____floatFormatHandling_10)); }
	inline int32_t get__floatFormatHandling_10() const { return ____floatFormatHandling_10; }
	inline int32_t* get_address_of__floatFormatHandling_10() { return &____floatFormatHandling_10; }
	inline void set__floatFormatHandling_10(int32_t value)
	{
		____floatFormatHandling_10 = value;
	}

	inline static int32_t get_offset_of__dateFormatString_11() { return static_cast<int32_t>(offsetof(JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76, ____dateFormatString_11)); }
	inline String_t* get__dateFormatString_11() const { return ____dateFormatString_11; }
	inline String_t** get_address_of__dateFormatString_11() { return &____dateFormatString_11; }
	inline void set__dateFormatString_11(String_t* value)
	{
		____dateFormatString_11 = value;
		Il2CppCodeGenWriteBarrier((&____dateFormatString_11), value);
	}

	inline static int32_t get_offset_of__culture_12() { return static_cast<int32_t>(offsetof(JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76, ____culture_12)); }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * get__culture_12() const { return ____culture_12; }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F ** get_address_of__culture_12() { return &____culture_12; }
	inline void set__culture_12(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * value)
	{
		____culture_12 = value;
		Il2CppCodeGenWriteBarrier((&____culture_12), value);
	}
};

struct JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76_StaticFields
{
public:
	// Newtonsoft.Json.JsonWriter_State[][] Newtonsoft.Json.JsonWriter::StateArray
	StateU5BU5DU5BU5D_t9EFAA051C5ADACB3E88E72C4C49B2F2CDA249867* ___StateArray_0;
	// Newtonsoft.Json.JsonWriter_State[][] Newtonsoft.Json.JsonWriter::StateArrayTempate
	StateU5BU5DU5BU5D_t9EFAA051C5ADACB3E88E72C4C49B2F2CDA249867* ___StateArrayTempate_1;

public:
	inline static int32_t get_offset_of_StateArray_0() { return static_cast<int32_t>(offsetof(JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76_StaticFields, ___StateArray_0)); }
	inline StateU5BU5DU5BU5D_t9EFAA051C5ADACB3E88E72C4C49B2F2CDA249867* get_StateArray_0() const { return ___StateArray_0; }
	inline StateU5BU5DU5BU5D_t9EFAA051C5ADACB3E88E72C4C49B2F2CDA249867** get_address_of_StateArray_0() { return &___StateArray_0; }
	inline void set_StateArray_0(StateU5BU5DU5BU5D_t9EFAA051C5ADACB3E88E72C4C49B2F2CDA249867* value)
	{
		___StateArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___StateArray_0), value);
	}

	inline static int32_t get_offset_of_StateArrayTempate_1() { return static_cast<int32_t>(offsetof(JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76_StaticFields, ___StateArrayTempate_1)); }
	inline StateU5BU5DU5BU5D_t9EFAA051C5ADACB3E88E72C4C49B2F2CDA249867* get_StateArrayTempate_1() const { return ___StateArrayTempate_1; }
	inline StateU5BU5DU5BU5D_t9EFAA051C5ADACB3E88E72C4C49B2F2CDA249867** get_address_of_StateArrayTempate_1() { return &___StateArrayTempate_1; }
	inline void set_StateArrayTempate_1(StateU5BU5DU5BU5D_t9EFAA051C5ADACB3E88E72C4C49B2F2CDA249867* value)
	{
		___StateArrayTempate_1 = value;
		Il2CppCodeGenWriteBarrier((&___StateArrayTempate_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONWRITER_T9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76_H
#ifndef BOOLEANQUERYEXPRESSION_T6F2B15128729297EA0C686FD823C18D7616D667F_H
#define BOOLEANQUERYEXPRESSION_T6F2B15128729297EA0C686FD823C18D7616D667F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JsonPath.BooleanQueryExpression
struct  BooleanQueryExpression_t6F2B15128729297EA0C686FD823C18D7616D667F  : public QueryExpression_t08B8769F946D3E05F6E9DB46393F0B9828244E7F
{
public:
	// System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JsonPath.PathFilter> Newtonsoft.Json.Linq.JsonPath.BooleanQueryExpression::<Path>k__BackingField
	List_1_t36919C030DB41B8F0BE34CC4C96991F1D1379557 * ___U3CPathU3Ek__BackingField_1;
	// Newtonsoft.Json.Linq.JValue Newtonsoft.Json.Linq.JsonPath.BooleanQueryExpression::<Value>k__BackingField
	JValue_t69F069DA12BD51A8FEB36C2EDA5D258B7FE2C4BF * ___U3CValueU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CPathU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(BooleanQueryExpression_t6F2B15128729297EA0C686FD823C18D7616D667F, ___U3CPathU3Ek__BackingField_1)); }
	inline List_1_t36919C030DB41B8F0BE34CC4C96991F1D1379557 * get_U3CPathU3Ek__BackingField_1() const { return ___U3CPathU3Ek__BackingField_1; }
	inline List_1_t36919C030DB41B8F0BE34CC4C96991F1D1379557 ** get_address_of_U3CPathU3Ek__BackingField_1() { return &___U3CPathU3Ek__BackingField_1; }
	inline void set_U3CPathU3Ek__BackingField_1(List_1_t36919C030DB41B8F0BE34CC4C96991F1D1379557 * value)
	{
		___U3CPathU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPathU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CValueU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(BooleanQueryExpression_t6F2B15128729297EA0C686FD823C18D7616D667F, ___U3CValueU3Ek__BackingField_2)); }
	inline JValue_t69F069DA12BD51A8FEB36C2EDA5D258B7FE2C4BF * get_U3CValueU3Ek__BackingField_2() const { return ___U3CValueU3Ek__BackingField_2; }
	inline JValue_t69F069DA12BD51A8FEB36C2EDA5D258B7FE2C4BF ** get_address_of_U3CValueU3Ek__BackingField_2() { return &___U3CValueU3Ek__BackingField_2; }
	inline void set_U3CValueU3Ek__BackingField_2(JValue_t69F069DA12BD51A8FEB36C2EDA5D258B7FE2C4BF * value)
	{
		___U3CValueU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CValueU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEANQUERYEXPRESSION_T6F2B15128729297EA0C686FD823C18D7616D667F_H
#ifndef COMPOSITEEXPRESSION_T3153D0FCDDFBADAA4ED86DCB1527031D389F91E9_H
#define COMPOSITEEXPRESSION_T3153D0FCDDFBADAA4ED86DCB1527031D389F91E9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JsonPath.CompositeExpression
struct  CompositeExpression_t3153D0FCDDFBADAA4ED86DCB1527031D389F91E9  : public QueryExpression_t08B8769F946D3E05F6E9DB46393F0B9828244E7F
{
public:
	// System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JsonPath.QueryExpression> Newtonsoft.Json.Linq.JsonPath.CompositeExpression::<Expressions>k__BackingField
	List_1_tACC9B8F07DB2C436197EF4D23781FF22DD6ED615 * ___U3CExpressionsU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CExpressionsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CompositeExpression_t3153D0FCDDFBADAA4ED86DCB1527031D389F91E9, ___U3CExpressionsU3Ek__BackingField_1)); }
	inline List_1_tACC9B8F07DB2C436197EF4D23781FF22DD6ED615 * get_U3CExpressionsU3Ek__BackingField_1() const { return ___U3CExpressionsU3Ek__BackingField_1; }
	inline List_1_tACC9B8F07DB2C436197EF4D23781FF22DD6ED615 ** get_address_of_U3CExpressionsU3Ek__BackingField_1() { return &___U3CExpressionsU3Ek__BackingField_1; }
	inline void set_U3CExpressionsU3Ek__BackingField_1(List_1_tACC9B8F07DB2C436197EF4D23781FF22DD6ED615 * value)
	{
		___U3CExpressionsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CExpressionsU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPOSITEEXPRESSION_T3153D0FCDDFBADAA4ED86DCB1527031D389F91E9_H
#ifndef BSONREADER_T4D59365861A2E8FA5D010B3FE20B696CDE3D53A0_H
#define BSONREADER_T4D59365861A2E8FA5D010B3FE20B696CDE3D53A0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Bson.BsonReader
struct  BsonReader_t4D59365861A2E8FA5D010B3FE20B696CDE3D53A0  : public JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636
{
public:
	// System.IO.BinaryReader Newtonsoft.Json.Bson.BsonReader::_reader
	BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969 * ____reader_19;
	// System.Collections.Generic.List`1<Newtonsoft.Json.Bson.BsonReader_ContainerContext> Newtonsoft.Json.Bson.BsonReader::_stack
	List_1_t7B3D3377EC84D5093BE9B1EEA7E5A83E25858361 * ____stack_20;
	// System.Byte[] Newtonsoft.Json.Bson.BsonReader::_byteBuffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____byteBuffer_21;
	// System.Char[] Newtonsoft.Json.Bson.BsonReader::_charBuffer
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ____charBuffer_22;
	// Newtonsoft.Json.Bson.BsonType Newtonsoft.Json.Bson.BsonReader::_currentElementType
	int8_t ____currentElementType_23;
	// Newtonsoft.Json.Bson.BsonReader_BsonReaderState Newtonsoft.Json.Bson.BsonReader::_bsonReaderState
	int32_t ____bsonReaderState_24;
	// Newtonsoft.Json.Bson.BsonReader_ContainerContext Newtonsoft.Json.Bson.BsonReader::_currentContext
	ContainerContext_t863E55417E974D1C50EA7E90F62077FC15D5241F * ____currentContext_25;
	// System.Boolean Newtonsoft.Json.Bson.BsonReader::_readRootValueAsArray
	bool ____readRootValueAsArray_26;
	// System.Boolean Newtonsoft.Json.Bson.BsonReader::_jsonNet35BinaryCompatibility
	bool ____jsonNet35BinaryCompatibility_27;
	// System.DateTimeKind Newtonsoft.Json.Bson.BsonReader::_dateTimeKindHandling
	int32_t ____dateTimeKindHandling_28;

public:
	inline static int32_t get_offset_of__reader_19() { return static_cast<int32_t>(offsetof(BsonReader_t4D59365861A2E8FA5D010B3FE20B696CDE3D53A0, ____reader_19)); }
	inline BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969 * get__reader_19() const { return ____reader_19; }
	inline BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969 ** get_address_of__reader_19() { return &____reader_19; }
	inline void set__reader_19(BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969 * value)
	{
		____reader_19 = value;
		Il2CppCodeGenWriteBarrier((&____reader_19), value);
	}

	inline static int32_t get_offset_of__stack_20() { return static_cast<int32_t>(offsetof(BsonReader_t4D59365861A2E8FA5D010B3FE20B696CDE3D53A0, ____stack_20)); }
	inline List_1_t7B3D3377EC84D5093BE9B1EEA7E5A83E25858361 * get__stack_20() const { return ____stack_20; }
	inline List_1_t7B3D3377EC84D5093BE9B1EEA7E5A83E25858361 ** get_address_of__stack_20() { return &____stack_20; }
	inline void set__stack_20(List_1_t7B3D3377EC84D5093BE9B1EEA7E5A83E25858361 * value)
	{
		____stack_20 = value;
		Il2CppCodeGenWriteBarrier((&____stack_20), value);
	}

	inline static int32_t get_offset_of__byteBuffer_21() { return static_cast<int32_t>(offsetof(BsonReader_t4D59365861A2E8FA5D010B3FE20B696CDE3D53A0, ____byteBuffer_21)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__byteBuffer_21() const { return ____byteBuffer_21; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__byteBuffer_21() { return &____byteBuffer_21; }
	inline void set__byteBuffer_21(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____byteBuffer_21 = value;
		Il2CppCodeGenWriteBarrier((&____byteBuffer_21), value);
	}

	inline static int32_t get_offset_of__charBuffer_22() { return static_cast<int32_t>(offsetof(BsonReader_t4D59365861A2E8FA5D010B3FE20B696CDE3D53A0, ____charBuffer_22)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get__charBuffer_22() const { return ____charBuffer_22; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of__charBuffer_22() { return &____charBuffer_22; }
	inline void set__charBuffer_22(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		____charBuffer_22 = value;
		Il2CppCodeGenWriteBarrier((&____charBuffer_22), value);
	}

	inline static int32_t get_offset_of__currentElementType_23() { return static_cast<int32_t>(offsetof(BsonReader_t4D59365861A2E8FA5D010B3FE20B696CDE3D53A0, ____currentElementType_23)); }
	inline int8_t get__currentElementType_23() const { return ____currentElementType_23; }
	inline int8_t* get_address_of__currentElementType_23() { return &____currentElementType_23; }
	inline void set__currentElementType_23(int8_t value)
	{
		____currentElementType_23 = value;
	}

	inline static int32_t get_offset_of__bsonReaderState_24() { return static_cast<int32_t>(offsetof(BsonReader_t4D59365861A2E8FA5D010B3FE20B696CDE3D53A0, ____bsonReaderState_24)); }
	inline int32_t get__bsonReaderState_24() const { return ____bsonReaderState_24; }
	inline int32_t* get_address_of__bsonReaderState_24() { return &____bsonReaderState_24; }
	inline void set__bsonReaderState_24(int32_t value)
	{
		____bsonReaderState_24 = value;
	}

	inline static int32_t get_offset_of__currentContext_25() { return static_cast<int32_t>(offsetof(BsonReader_t4D59365861A2E8FA5D010B3FE20B696CDE3D53A0, ____currentContext_25)); }
	inline ContainerContext_t863E55417E974D1C50EA7E90F62077FC15D5241F * get__currentContext_25() const { return ____currentContext_25; }
	inline ContainerContext_t863E55417E974D1C50EA7E90F62077FC15D5241F ** get_address_of__currentContext_25() { return &____currentContext_25; }
	inline void set__currentContext_25(ContainerContext_t863E55417E974D1C50EA7E90F62077FC15D5241F * value)
	{
		____currentContext_25 = value;
		Il2CppCodeGenWriteBarrier((&____currentContext_25), value);
	}

	inline static int32_t get_offset_of__readRootValueAsArray_26() { return static_cast<int32_t>(offsetof(BsonReader_t4D59365861A2E8FA5D010B3FE20B696CDE3D53A0, ____readRootValueAsArray_26)); }
	inline bool get__readRootValueAsArray_26() const { return ____readRootValueAsArray_26; }
	inline bool* get_address_of__readRootValueAsArray_26() { return &____readRootValueAsArray_26; }
	inline void set__readRootValueAsArray_26(bool value)
	{
		____readRootValueAsArray_26 = value;
	}

	inline static int32_t get_offset_of__jsonNet35BinaryCompatibility_27() { return static_cast<int32_t>(offsetof(BsonReader_t4D59365861A2E8FA5D010B3FE20B696CDE3D53A0, ____jsonNet35BinaryCompatibility_27)); }
	inline bool get__jsonNet35BinaryCompatibility_27() const { return ____jsonNet35BinaryCompatibility_27; }
	inline bool* get_address_of__jsonNet35BinaryCompatibility_27() { return &____jsonNet35BinaryCompatibility_27; }
	inline void set__jsonNet35BinaryCompatibility_27(bool value)
	{
		____jsonNet35BinaryCompatibility_27 = value;
	}

	inline static int32_t get_offset_of__dateTimeKindHandling_28() { return static_cast<int32_t>(offsetof(BsonReader_t4D59365861A2E8FA5D010B3FE20B696CDE3D53A0, ____dateTimeKindHandling_28)); }
	inline int32_t get__dateTimeKindHandling_28() const { return ____dateTimeKindHandling_28; }
	inline int32_t* get_address_of__dateTimeKindHandling_28() { return &____dateTimeKindHandling_28; }
	inline void set__dateTimeKindHandling_28(int32_t value)
	{
		____dateTimeKindHandling_28 = value;
	}
};

struct BsonReader_t4D59365861A2E8FA5D010B3FE20B696CDE3D53A0_StaticFields
{
public:
	// System.Byte[] Newtonsoft.Json.Bson.BsonReader::SeqRange1
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___SeqRange1_15;
	// System.Byte[] Newtonsoft.Json.Bson.BsonReader::SeqRange2
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___SeqRange2_16;
	// System.Byte[] Newtonsoft.Json.Bson.BsonReader::SeqRange3
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___SeqRange3_17;
	// System.Byte[] Newtonsoft.Json.Bson.BsonReader::SeqRange4
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___SeqRange4_18;

public:
	inline static int32_t get_offset_of_SeqRange1_15() { return static_cast<int32_t>(offsetof(BsonReader_t4D59365861A2E8FA5D010B3FE20B696CDE3D53A0_StaticFields, ___SeqRange1_15)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_SeqRange1_15() const { return ___SeqRange1_15; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_SeqRange1_15() { return &___SeqRange1_15; }
	inline void set_SeqRange1_15(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___SeqRange1_15 = value;
		Il2CppCodeGenWriteBarrier((&___SeqRange1_15), value);
	}

	inline static int32_t get_offset_of_SeqRange2_16() { return static_cast<int32_t>(offsetof(BsonReader_t4D59365861A2E8FA5D010B3FE20B696CDE3D53A0_StaticFields, ___SeqRange2_16)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_SeqRange2_16() const { return ___SeqRange2_16; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_SeqRange2_16() { return &___SeqRange2_16; }
	inline void set_SeqRange2_16(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___SeqRange2_16 = value;
		Il2CppCodeGenWriteBarrier((&___SeqRange2_16), value);
	}

	inline static int32_t get_offset_of_SeqRange3_17() { return static_cast<int32_t>(offsetof(BsonReader_t4D59365861A2E8FA5D010B3FE20B696CDE3D53A0_StaticFields, ___SeqRange3_17)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_SeqRange3_17() const { return ___SeqRange3_17; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_SeqRange3_17() { return &___SeqRange3_17; }
	inline void set_SeqRange3_17(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___SeqRange3_17 = value;
		Il2CppCodeGenWriteBarrier((&___SeqRange3_17), value);
	}

	inline static int32_t get_offset_of_SeqRange4_18() { return static_cast<int32_t>(offsetof(BsonReader_t4D59365861A2E8FA5D010B3FE20B696CDE3D53A0_StaticFields, ___SeqRange4_18)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_SeqRange4_18() const { return ___SeqRange4_18; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_SeqRange4_18() { return &___SeqRange4_18; }
	inline void set_SeqRange4_18(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___SeqRange4_18 = value;
		Il2CppCodeGenWriteBarrier((&___SeqRange4_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONREADER_T4D59365861A2E8FA5D010B3FE20B696CDE3D53A0_H
#ifndef BSONWRITER_TD3EA6BE4366DF3B2C0E30C83318262FED969CE11_H
#define BSONWRITER_TD3EA6BE4366DF3B2C0E30C83318262FED969CE11_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Bson.BsonWriter
struct  BsonWriter_tD3EA6BE4366DF3B2C0E30C83318262FED969CE11  : public JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76
{
public:
	// Newtonsoft.Json.Bson.BsonBinaryWriter Newtonsoft.Json.Bson.BsonWriter::_writer
	BsonBinaryWriter_t3572084F4EE9D0ACA3EAE31EF1C0DF358D080D80 * ____writer_13;
	// Newtonsoft.Json.Bson.BsonToken Newtonsoft.Json.Bson.BsonWriter::_root
	BsonToken_tEB681A5D0E53F8005DC31BAEFDE6481BBE80C2D3 * ____root_14;
	// Newtonsoft.Json.Bson.BsonToken Newtonsoft.Json.Bson.BsonWriter::_parent
	BsonToken_tEB681A5D0E53F8005DC31BAEFDE6481BBE80C2D3 * ____parent_15;
	// System.String Newtonsoft.Json.Bson.BsonWriter::_propertyName
	String_t* ____propertyName_16;

public:
	inline static int32_t get_offset_of__writer_13() { return static_cast<int32_t>(offsetof(BsonWriter_tD3EA6BE4366DF3B2C0E30C83318262FED969CE11, ____writer_13)); }
	inline BsonBinaryWriter_t3572084F4EE9D0ACA3EAE31EF1C0DF358D080D80 * get__writer_13() const { return ____writer_13; }
	inline BsonBinaryWriter_t3572084F4EE9D0ACA3EAE31EF1C0DF358D080D80 ** get_address_of__writer_13() { return &____writer_13; }
	inline void set__writer_13(BsonBinaryWriter_t3572084F4EE9D0ACA3EAE31EF1C0DF358D080D80 * value)
	{
		____writer_13 = value;
		Il2CppCodeGenWriteBarrier((&____writer_13), value);
	}

	inline static int32_t get_offset_of__root_14() { return static_cast<int32_t>(offsetof(BsonWriter_tD3EA6BE4366DF3B2C0E30C83318262FED969CE11, ____root_14)); }
	inline BsonToken_tEB681A5D0E53F8005DC31BAEFDE6481BBE80C2D3 * get__root_14() const { return ____root_14; }
	inline BsonToken_tEB681A5D0E53F8005DC31BAEFDE6481BBE80C2D3 ** get_address_of__root_14() { return &____root_14; }
	inline void set__root_14(BsonToken_tEB681A5D0E53F8005DC31BAEFDE6481BBE80C2D3 * value)
	{
		____root_14 = value;
		Il2CppCodeGenWriteBarrier((&____root_14), value);
	}

	inline static int32_t get_offset_of__parent_15() { return static_cast<int32_t>(offsetof(BsonWriter_tD3EA6BE4366DF3B2C0E30C83318262FED969CE11, ____parent_15)); }
	inline BsonToken_tEB681A5D0E53F8005DC31BAEFDE6481BBE80C2D3 * get__parent_15() const { return ____parent_15; }
	inline BsonToken_tEB681A5D0E53F8005DC31BAEFDE6481BBE80C2D3 ** get_address_of__parent_15() { return &____parent_15; }
	inline void set__parent_15(BsonToken_tEB681A5D0E53F8005DC31BAEFDE6481BBE80C2D3 * value)
	{
		____parent_15 = value;
		Il2CppCodeGenWriteBarrier((&____parent_15), value);
	}

	inline static int32_t get_offset_of__propertyName_16() { return static_cast<int32_t>(offsetof(BsonWriter_tD3EA6BE4366DF3B2C0E30C83318262FED969CE11, ____propertyName_16)); }
	inline String_t* get__propertyName_16() const { return ____propertyName_16; }
	inline String_t** get_address_of__propertyName_16() { return &____propertyName_16; }
	inline void set__propertyName_16(String_t* value)
	{
		____propertyName_16 = value;
		Il2CppCodeGenWriteBarrier((&____propertyName_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONWRITER_TD3EA6BE4366DF3B2C0E30C83318262FED969CE11_H
#ifndef JTOKENREADER_T64F9A91FCD01D0A1FB881D2E8AD41AEC2E59D4C3_H
#define JTOKENREADER_T64F9A91FCD01D0A1FB881D2E8AD41AEC2E59D4C3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JTokenReader
struct  JTokenReader_t64F9A91FCD01D0A1FB881D2E8AD41AEC2E59D4C3  : public JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636
{
public:
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JTokenReader::_root
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * ____root_15;
	// System.String Newtonsoft.Json.Linq.JTokenReader::_initialPath
	String_t* ____initialPath_16;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JTokenReader::_parent
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * ____parent_17;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JTokenReader::_current
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * ____current_18;

public:
	inline static int32_t get_offset_of__root_15() { return static_cast<int32_t>(offsetof(JTokenReader_t64F9A91FCD01D0A1FB881D2E8AD41AEC2E59D4C3, ____root_15)); }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * get__root_15() const { return ____root_15; }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 ** get_address_of__root_15() { return &____root_15; }
	inline void set__root_15(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * value)
	{
		____root_15 = value;
		Il2CppCodeGenWriteBarrier((&____root_15), value);
	}

	inline static int32_t get_offset_of__initialPath_16() { return static_cast<int32_t>(offsetof(JTokenReader_t64F9A91FCD01D0A1FB881D2E8AD41AEC2E59D4C3, ____initialPath_16)); }
	inline String_t* get__initialPath_16() const { return ____initialPath_16; }
	inline String_t** get_address_of__initialPath_16() { return &____initialPath_16; }
	inline void set__initialPath_16(String_t* value)
	{
		____initialPath_16 = value;
		Il2CppCodeGenWriteBarrier((&____initialPath_16), value);
	}

	inline static int32_t get_offset_of__parent_17() { return static_cast<int32_t>(offsetof(JTokenReader_t64F9A91FCD01D0A1FB881D2E8AD41AEC2E59D4C3, ____parent_17)); }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * get__parent_17() const { return ____parent_17; }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 ** get_address_of__parent_17() { return &____parent_17; }
	inline void set__parent_17(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * value)
	{
		____parent_17 = value;
		Il2CppCodeGenWriteBarrier((&____parent_17), value);
	}

	inline static int32_t get_offset_of__current_18() { return static_cast<int32_t>(offsetof(JTokenReader_t64F9A91FCD01D0A1FB881D2E8AD41AEC2E59D4C3, ____current_18)); }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * get__current_18() const { return ____current_18; }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 ** get_address_of__current_18() { return &____current_18; }
	inline void set__current_18(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * value)
	{
		____current_18 = value;
		Il2CppCodeGenWriteBarrier((&____current_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JTOKENREADER_T64F9A91FCD01D0A1FB881D2E8AD41AEC2E59D4C3_H
#ifndef JTOKENWRITER_TC1A2B10A61DBDF1F970ADA08BE3976BE7662BFE4_H
#define JTOKENWRITER_TC1A2B10A61DBDF1F970ADA08BE3976BE7662BFE4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JTokenWriter
struct  JTokenWriter_tC1A2B10A61DBDF1F970ADA08BE3976BE7662BFE4  : public JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76
{
public:
	// Newtonsoft.Json.Linq.JContainer Newtonsoft.Json.Linq.JTokenWriter::_token
	JContainer_tF4CD2E574503C709DEF18A04B79B264B83746DAB * ____token_13;
	// Newtonsoft.Json.Linq.JContainer Newtonsoft.Json.Linq.JTokenWriter::_parent
	JContainer_tF4CD2E574503C709DEF18A04B79B264B83746DAB * ____parent_14;
	// Newtonsoft.Json.Linq.JValue Newtonsoft.Json.Linq.JTokenWriter::_value
	JValue_t69F069DA12BD51A8FEB36C2EDA5D258B7FE2C4BF * ____value_15;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JTokenWriter::_current
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * ____current_16;

public:
	inline static int32_t get_offset_of__token_13() { return static_cast<int32_t>(offsetof(JTokenWriter_tC1A2B10A61DBDF1F970ADA08BE3976BE7662BFE4, ____token_13)); }
	inline JContainer_tF4CD2E574503C709DEF18A04B79B264B83746DAB * get__token_13() const { return ____token_13; }
	inline JContainer_tF4CD2E574503C709DEF18A04B79B264B83746DAB ** get_address_of__token_13() { return &____token_13; }
	inline void set__token_13(JContainer_tF4CD2E574503C709DEF18A04B79B264B83746DAB * value)
	{
		____token_13 = value;
		Il2CppCodeGenWriteBarrier((&____token_13), value);
	}

	inline static int32_t get_offset_of__parent_14() { return static_cast<int32_t>(offsetof(JTokenWriter_tC1A2B10A61DBDF1F970ADA08BE3976BE7662BFE4, ____parent_14)); }
	inline JContainer_tF4CD2E574503C709DEF18A04B79B264B83746DAB * get__parent_14() const { return ____parent_14; }
	inline JContainer_tF4CD2E574503C709DEF18A04B79B264B83746DAB ** get_address_of__parent_14() { return &____parent_14; }
	inline void set__parent_14(JContainer_tF4CD2E574503C709DEF18A04B79B264B83746DAB * value)
	{
		____parent_14 = value;
		Il2CppCodeGenWriteBarrier((&____parent_14), value);
	}

	inline static int32_t get_offset_of__value_15() { return static_cast<int32_t>(offsetof(JTokenWriter_tC1A2B10A61DBDF1F970ADA08BE3976BE7662BFE4, ____value_15)); }
	inline JValue_t69F069DA12BD51A8FEB36C2EDA5D258B7FE2C4BF * get__value_15() const { return ____value_15; }
	inline JValue_t69F069DA12BD51A8FEB36C2EDA5D258B7FE2C4BF ** get_address_of__value_15() { return &____value_15; }
	inline void set__value_15(JValue_t69F069DA12BD51A8FEB36C2EDA5D258B7FE2C4BF * value)
	{
		____value_15 = value;
		Il2CppCodeGenWriteBarrier((&____value_15), value);
	}

	inline static int32_t get_offset_of__current_16() { return static_cast<int32_t>(offsetof(JTokenWriter_tC1A2B10A61DBDF1F970ADA08BE3976BE7662BFE4, ____current_16)); }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * get__current_16() const { return ____current_16; }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 ** get_address_of__current_16() { return &____current_16; }
	inline void set__current_16(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * value)
	{
		____current_16 = value;
		Il2CppCodeGenWriteBarrier((&____current_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JTOKENWRITER_TC1A2B10A61DBDF1F970ADA08BE3976BE7662BFE4_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6200 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6200[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6201 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6201[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6202 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6202[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6203 = { sizeof (U3CU3Ec_t37838CC19F295B6E657DDBC4D19E47C423BE14DF), -1, sizeof(U3CU3Ec_t37838CC19F295B6E657DDBC4D19E47C423BE14DF_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6203[1] = 
{
	U3CU3Ec_t37838CC19F295B6E657DDBC4D19E47C423BE14DF_StaticFields::get_offset_of_U3CU3E9_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6204 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6204[10] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6205 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6205[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6206 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6206[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6207 = { sizeof (JConstructor_t9F741A9F4DC238BBAC6980237CBBEA9730BA9775), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6207[2] = 
{
	JConstructor_t9F741A9F4DC238BBAC6980237CBBEA9730BA9775::get_offset_of__name_17(),
	JConstructor_t9F741A9F4DC238BBAC6980237CBBEA9730BA9775::get_offset_of__values_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6208 = { sizeof (JContainer_tF4CD2E574503C709DEF18A04B79B264B83746DAB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6208[4] = 
{
	JContainer_tF4CD2E574503C709DEF18A04B79B264B83746DAB::get_offset_of__listChanged_13(),
	JContainer_tF4CD2E574503C709DEF18A04B79B264B83746DAB::get_offset_of__addingNew_14(),
	JContainer_tF4CD2E574503C709DEF18A04B79B264B83746DAB::get_offset_of__syncRoot_15(),
	JContainer_tF4CD2E574503C709DEF18A04B79B264B83746DAB::get_offset_of__busy_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6209 = { sizeof (U3CGetDescendantsU3Ed__29_t02A1778B5FFF602EE20BEB519454C18870DDEDC4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6209[9] = 
{
	U3CGetDescendantsU3Ed__29_t02A1778B5FFF602EE20BEB519454C18870DDEDC4::get_offset_of_U3CU3E1__state_0(),
	U3CGetDescendantsU3Ed__29_t02A1778B5FFF602EE20BEB519454C18870DDEDC4::get_offset_of_U3CU3E2__current_1(),
	U3CGetDescendantsU3Ed__29_t02A1778B5FFF602EE20BEB519454C18870DDEDC4::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CGetDescendantsU3Ed__29_t02A1778B5FFF602EE20BEB519454C18870DDEDC4::get_offset_of_self_3(),
	U3CGetDescendantsU3Ed__29_t02A1778B5FFF602EE20BEB519454C18870DDEDC4::get_offset_of_U3CU3E3__self_4(),
	U3CGetDescendantsU3Ed__29_t02A1778B5FFF602EE20BEB519454C18870DDEDC4::get_offset_of_U3CU3E4__this_5(),
	U3CGetDescendantsU3Ed__29_t02A1778B5FFF602EE20BEB519454C18870DDEDC4::get_offset_of_U3CoU3E5__1_6(),
	U3CGetDescendantsU3Ed__29_t02A1778B5FFF602EE20BEB519454C18870DDEDC4::get_offset_of_U3CU3E7__wrap1_7(),
	U3CGetDescendantsU3Ed__29_t02A1778B5FFF602EE20BEB519454C18870DDEDC4::get_offset_of_U3CU3E7__wrap2_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6210 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6210[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6211 = { sizeof (JObject_t786AF07B1009334856B0362BBC48EEF68C81C585), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6211[3] = 
{
	JObject_t786AF07B1009334856B0362BBC48EEF68C81C585::get_offset_of__properties_17(),
	JObject_t786AF07B1009334856B0362BBC48EEF68C81C585::get_offset_of_PropertyChanged_18(),
	JObject_t786AF07B1009334856B0362BBC48EEF68C81C585::get_offset_of_PropertyChanging_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6212 = { sizeof (U3CU3Ec_tE2C0FE447E68411655C2FA71B9D6FF79390E8E5A), -1, sizeof(U3CU3Ec_tE2C0FE447E68411655C2FA71B9D6FF79390E8E5A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6212[1] = 
{
	U3CU3Ec_tE2C0FE447E68411655C2FA71B9D6FF79390E8E5A_StaticFields::get_offset_of_U3CU3E9_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6213 = { sizeof (U3CGetEnumeratorU3Ed__58_tF9BB600B8070D5230EC56222EB5B2CA2802A7FB8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6213[4] = 
{
	U3CGetEnumeratorU3Ed__58_tF9BB600B8070D5230EC56222EB5B2CA2802A7FB8::get_offset_of_U3CU3E1__state_0(),
	U3CGetEnumeratorU3Ed__58_tF9BB600B8070D5230EC56222EB5B2CA2802A7FB8::get_offset_of_U3CU3E2__current_1(),
	U3CGetEnumeratorU3Ed__58_tF9BB600B8070D5230EC56222EB5B2CA2802A7FB8::get_offset_of_U3CU3E4__this_2(),
	U3CGetEnumeratorU3Ed__58_tF9BB600B8070D5230EC56222EB5B2CA2802A7FB8::get_offset_of_U3CU3E7__wrap1_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6214 = { sizeof (JArray_t1CE13821116F9B501573275C6BDD9FB254E65F11), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6214[1] = 
{
	JArray_t1CE13821116F9B501573275C6BDD9FB254E65F11::get_offset_of__values_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6215 = { sizeof (JTokenReader_t64F9A91FCD01D0A1FB881D2E8AD41AEC2E59D4C3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6215[4] = 
{
	JTokenReader_t64F9A91FCD01D0A1FB881D2E8AD41AEC2E59D4C3::get_offset_of__root_15(),
	JTokenReader_t64F9A91FCD01D0A1FB881D2E8AD41AEC2E59D4C3::get_offset_of__initialPath_16(),
	JTokenReader_t64F9A91FCD01D0A1FB881D2E8AD41AEC2E59D4C3::get_offset_of__parent_17(),
	JTokenReader_t64F9A91FCD01D0A1FB881D2E8AD41AEC2E59D4C3::get_offset_of__current_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6216 = { sizeof (JTokenWriter_tC1A2B10A61DBDF1F970ADA08BE3976BE7662BFE4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6216[4] = 
{
	JTokenWriter_tC1A2B10A61DBDF1F970ADA08BE3976BE7662BFE4::get_offset_of__token_13(),
	JTokenWriter_tC1A2B10A61DBDF1F970ADA08BE3976BE7662BFE4::get_offset_of__parent_14(),
	JTokenWriter_tC1A2B10A61DBDF1F970ADA08BE3976BE7662BFE4::get_offset_of__value_15(),
	JTokenWriter_tC1A2B10A61DBDF1F970ADA08BE3976BE7662BFE4::get_offset_of__current_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6217 = { sizeof (JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02), -1, sizeof(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6217[13] = 
{
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02::get_offset_of__parent_0(),
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02::get_offset_of__previous_1(),
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02::get_offset_of__next_2(),
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02::get_offset_of__annotations_3(),
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02_StaticFields::get_offset_of_BooleanTypes_4(),
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02_StaticFields::get_offset_of_NumberTypes_5(),
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02_StaticFields::get_offset_of_StringTypes_6(),
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02_StaticFields::get_offset_of_GuidTypes_7(),
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02_StaticFields::get_offset_of_TimeSpanTypes_8(),
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02_StaticFields::get_offset_of_UriTypes_9(),
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02_StaticFields::get_offset_of_CharTypes_10(),
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02_StaticFields::get_offset_of_DateTimeTypes_11(),
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02_StaticFields::get_offset_of_BytesTypes_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6218 = { sizeof (LineInfoAnnotation_t275C937C347C8A3C93FFD9CAC915903CDEDDF40D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6218[2] = 
{
	LineInfoAnnotation_t275C937C347C8A3C93FFD9CAC915903CDEDDF40D::get_offset_of_LineNumber_0(),
	LineInfoAnnotation_t275C937C347C8A3C93FFD9CAC915903CDEDDF40D::get_offset_of_LinePosition_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6219 = { sizeof (U3CGetAncestorsU3Ed__41_tA86F8A2E067567D67E95D02FD20F096E7F14B840), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6219[7] = 
{
	U3CGetAncestorsU3Ed__41_tA86F8A2E067567D67E95D02FD20F096E7F14B840::get_offset_of_U3CU3E1__state_0(),
	U3CGetAncestorsU3Ed__41_tA86F8A2E067567D67E95D02FD20F096E7F14B840::get_offset_of_U3CU3E2__current_1(),
	U3CGetAncestorsU3Ed__41_tA86F8A2E067567D67E95D02FD20F096E7F14B840::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CGetAncestorsU3Ed__41_tA86F8A2E067567D67E95D02FD20F096E7F14B840::get_offset_of_self_3(),
	U3CGetAncestorsU3Ed__41_tA86F8A2E067567D67E95D02FD20F096E7F14B840::get_offset_of_U3CU3E3__self_4(),
	U3CGetAncestorsU3Ed__41_tA86F8A2E067567D67E95D02FD20F096E7F14B840::get_offset_of_U3CU3E4__this_5(),
	U3CGetAncestorsU3Ed__41_tA86F8A2E067567D67E95D02FD20F096E7F14B840::get_offset_of_U3CcurrentU3E5__1_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6220 = { sizeof (U3CAfterSelfU3Ed__42_t195C851E6A37082B7270B7533D256D1F4055BD94), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6220[5] = 
{
	U3CAfterSelfU3Ed__42_t195C851E6A37082B7270B7533D256D1F4055BD94::get_offset_of_U3CU3E1__state_0(),
	U3CAfterSelfU3Ed__42_t195C851E6A37082B7270B7533D256D1F4055BD94::get_offset_of_U3CU3E2__current_1(),
	U3CAfterSelfU3Ed__42_t195C851E6A37082B7270B7533D256D1F4055BD94::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CAfterSelfU3Ed__42_t195C851E6A37082B7270B7533D256D1F4055BD94::get_offset_of_U3CU3E4__this_3(),
	U3CAfterSelfU3Ed__42_t195C851E6A37082B7270B7533D256D1F4055BD94::get_offset_of_U3CoU3E5__1_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6221 = { sizeof (U3CBeforeSelfU3Ed__43_t6CF3F6C7954C3E7EE291671EB2B9422DC3D3F1B4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6221[5] = 
{
	U3CBeforeSelfU3Ed__43_t6CF3F6C7954C3E7EE291671EB2B9422DC3D3F1B4::get_offset_of_U3CU3E1__state_0(),
	U3CBeforeSelfU3Ed__43_t6CF3F6C7954C3E7EE291671EB2B9422DC3D3F1B4::get_offset_of_U3CU3E2__current_1(),
	U3CBeforeSelfU3Ed__43_t6CF3F6C7954C3E7EE291671EB2B9422DC3D3F1B4::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CBeforeSelfU3Ed__43_t6CF3F6C7954C3E7EE291671EB2B9422DC3D3F1B4::get_offset_of_U3CU3E4__this_3(),
	U3CBeforeSelfU3Ed__43_t6CF3F6C7954C3E7EE291671EB2B9422DC3D3F1B4::get_offset_of_U3CoU3E5__1_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6222 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6222[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6223 = { sizeof (U3CAnnotationsU3Ed__172_t556B0F18E6073D7ED7C30446020DA51153E7931D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6223[8] = 
{
	U3CAnnotationsU3Ed__172_t556B0F18E6073D7ED7C30446020DA51153E7931D::get_offset_of_U3CU3E1__state_0(),
	U3CAnnotationsU3Ed__172_t556B0F18E6073D7ED7C30446020DA51153E7931D::get_offset_of_U3CU3E2__current_1(),
	U3CAnnotationsU3Ed__172_t556B0F18E6073D7ED7C30446020DA51153E7931D::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CAnnotationsU3Ed__172_t556B0F18E6073D7ED7C30446020DA51153E7931D::get_offset_of_type_3(),
	U3CAnnotationsU3Ed__172_t556B0F18E6073D7ED7C30446020DA51153E7931D::get_offset_of_U3CU3E3__type_4(),
	U3CAnnotationsU3Ed__172_t556B0F18E6073D7ED7C30446020DA51153E7931D::get_offset_of_U3CU3E4__this_5(),
	U3CAnnotationsU3Ed__172_t556B0F18E6073D7ED7C30446020DA51153E7931D::get_offset_of_U3CannotationsU3E5__1_6(),
	U3CAnnotationsU3Ed__172_t556B0F18E6073D7ED7C30446020DA51153E7931D::get_offset_of_U3CiU3E5__2_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6224 = { sizeof (JProperty_t127765B5AB6D281C5B77FAF5A4F26BB33C89398A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6224[2] = 
{
	JProperty_t127765B5AB6D281C5B77FAF5A4F26BB33C89398A::get_offset_of__content_17(),
	JProperty_t127765B5AB6D281C5B77FAF5A4F26BB33C89398A::get_offset_of__name_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6225 = { sizeof (JPropertyList_tAC5DCFB03C7879ED6FCFD74977E76474C918200F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6225[1] = 
{
	JPropertyList_tAC5DCFB03C7879ED6FCFD74977E76474C918200F::get_offset_of__token_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6226 = { sizeof (U3CGetEnumeratorU3Ed__1_t74F96992BB129149414620E86F10CFDF4C199AF8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6226[3] = 
{
	U3CGetEnumeratorU3Ed__1_t74F96992BB129149414620E86F10CFDF4C199AF8::get_offset_of_U3CU3E1__state_0(),
	U3CGetEnumeratorU3Ed__1_t74F96992BB129149414620E86F10CFDF4C199AF8::get_offset_of_U3CU3E2__current_1(),
	U3CGetEnumeratorU3Ed__1_t74F96992BB129149414620E86F10CFDF4C199AF8::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6227 = { sizeof (JTokenType_t6E74963952426FE99DA37BAEFD84A99710412C9E)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6227[19] = 
{
	JTokenType_t6E74963952426FE99DA37BAEFD84A99710412C9E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6228 = { sizeof (JValue_t69F069DA12BD51A8FEB36C2EDA5D258B7FE2C4BF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6228[2] = 
{
	JValue_t69F069DA12BD51A8FEB36C2EDA5D258B7FE2C4BF::get_offset_of__valueType_13(),
	JValue_t69F069DA12BD51A8FEB36C2EDA5D258B7FE2C4BF::get_offset_of__value_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6229 = { sizeof (ArrayIndexFilter_t4B8AB90AF78B3976E09BEA34466153A1CDE2E12B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6229[1] = 
{
	ArrayIndexFilter_t4B8AB90AF78B3976E09BEA34466153A1CDE2E12B::get_offset_of_U3CIndexU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6230 = { sizeof (U3CExecuteFilterU3Ed__4_t49283F96CCAA85028EA2C8F9402DA5D35998E1D7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6230[11] = 
{
	U3CExecuteFilterU3Ed__4_t49283F96CCAA85028EA2C8F9402DA5D35998E1D7::get_offset_of_U3CU3E1__state_0(),
	U3CExecuteFilterU3Ed__4_t49283F96CCAA85028EA2C8F9402DA5D35998E1D7::get_offset_of_U3CU3E2__current_1(),
	U3CExecuteFilterU3Ed__4_t49283F96CCAA85028EA2C8F9402DA5D35998E1D7::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CExecuteFilterU3Ed__4_t49283F96CCAA85028EA2C8F9402DA5D35998E1D7::get_offset_of_current_3(),
	U3CExecuteFilterU3Ed__4_t49283F96CCAA85028EA2C8F9402DA5D35998E1D7::get_offset_of_U3CU3E3__current_4(),
	U3CExecuteFilterU3Ed__4_t49283F96CCAA85028EA2C8F9402DA5D35998E1D7::get_offset_of_U3CU3E4__this_5(),
	U3CExecuteFilterU3Ed__4_t49283F96CCAA85028EA2C8F9402DA5D35998E1D7::get_offset_of_errorWhenNoMatch_6(),
	U3CExecuteFilterU3Ed__4_t49283F96CCAA85028EA2C8F9402DA5D35998E1D7::get_offset_of_U3CU3E3__errorWhenNoMatch_7(),
	U3CExecuteFilterU3Ed__4_t49283F96CCAA85028EA2C8F9402DA5D35998E1D7::get_offset_of_U3CtU3E5__1_8(),
	U3CExecuteFilterU3Ed__4_t49283F96CCAA85028EA2C8F9402DA5D35998E1D7::get_offset_of_U3CU3E7__wrap1_9(),
	U3CExecuteFilterU3Ed__4_t49283F96CCAA85028EA2C8F9402DA5D35998E1D7::get_offset_of_U3CU3E7__wrap2_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6231 = { sizeof (ArrayMultipleIndexFilter_tF9FF764DDCB79A819281D799112F6E930B75E99F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6231[1] = 
{
	ArrayMultipleIndexFilter_tF9FF764DDCB79A819281D799112F6E930B75E99F::get_offset_of_U3CIndexesU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6232 = { sizeof (U3CExecuteFilterU3Ed__4_tC82AE708D5CE2B8917E4AA90413E6CA8BA574131), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6232[11] = 
{
	U3CExecuteFilterU3Ed__4_tC82AE708D5CE2B8917E4AA90413E6CA8BA574131::get_offset_of_U3CU3E1__state_0(),
	U3CExecuteFilterU3Ed__4_tC82AE708D5CE2B8917E4AA90413E6CA8BA574131::get_offset_of_U3CU3E2__current_1(),
	U3CExecuteFilterU3Ed__4_tC82AE708D5CE2B8917E4AA90413E6CA8BA574131::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CExecuteFilterU3Ed__4_tC82AE708D5CE2B8917E4AA90413E6CA8BA574131::get_offset_of_current_3(),
	U3CExecuteFilterU3Ed__4_tC82AE708D5CE2B8917E4AA90413E6CA8BA574131::get_offset_of_U3CU3E3__current_4(),
	U3CExecuteFilterU3Ed__4_tC82AE708D5CE2B8917E4AA90413E6CA8BA574131::get_offset_of_U3CU3E4__this_5(),
	U3CExecuteFilterU3Ed__4_tC82AE708D5CE2B8917E4AA90413E6CA8BA574131::get_offset_of_U3CtU3E5__1_6(),
	U3CExecuteFilterU3Ed__4_tC82AE708D5CE2B8917E4AA90413E6CA8BA574131::get_offset_of_errorWhenNoMatch_7(),
	U3CExecuteFilterU3Ed__4_tC82AE708D5CE2B8917E4AA90413E6CA8BA574131::get_offset_of_U3CU3E3__errorWhenNoMatch_8(),
	U3CExecuteFilterU3Ed__4_tC82AE708D5CE2B8917E4AA90413E6CA8BA574131::get_offset_of_U3CU3E7__wrap1_9(),
	U3CExecuteFilterU3Ed__4_tC82AE708D5CE2B8917E4AA90413E6CA8BA574131::get_offset_of_U3CU3E7__wrap2_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6233 = { sizeof (ArraySliceFilter_tC5035758E75DB3EF9EAA182D561A726F46397BE3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6233[3] = 
{
	ArraySliceFilter_tC5035758E75DB3EF9EAA182D561A726F46397BE3::get_offset_of_U3CStartU3Ek__BackingField_0(),
	ArraySliceFilter_tC5035758E75DB3EF9EAA182D561A726F46397BE3::get_offset_of_U3CEndU3Ek__BackingField_1(),
	ArraySliceFilter_tC5035758E75DB3EF9EAA182D561A726F46397BE3::get_offset_of_U3CStepU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6234 = { sizeof (U3CExecuteFilterU3Ed__12_tBCB91730C7554E4E134042CDDD1550F32438D3F8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6234[15] = 
{
	U3CExecuteFilterU3Ed__12_tBCB91730C7554E4E134042CDDD1550F32438D3F8::get_offset_of_U3CU3E1__state_0(),
	U3CExecuteFilterU3Ed__12_tBCB91730C7554E4E134042CDDD1550F32438D3F8::get_offset_of_U3CU3E2__current_1(),
	U3CExecuteFilterU3Ed__12_tBCB91730C7554E4E134042CDDD1550F32438D3F8::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CExecuteFilterU3Ed__12_tBCB91730C7554E4E134042CDDD1550F32438D3F8::get_offset_of_U3CU3E4__this_3(),
	U3CExecuteFilterU3Ed__12_tBCB91730C7554E4E134042CDDD1550F32438D3F8::get_offset_of_current_4(),
	U3CExecuteFilterU3Ed__12_tBCB91730C7554E4E134042CDDD1550F32438D3F8::get_offset_of_U3CU3E3__current_5(),
	U3CExecuteFilterU3Ed__12_tBCB91730C7554E4E134042CDDD1550F32438D3F8::get_offset_of_U3CaU3E5__1_6(),
	U3CExecuteFilterU3Ed__12_tBCB91730C7554E4E134042CDDD1550F32438D3F8::get_offset_of_U3CiU3E5__2_7(),
	U3CExecuteFilterU3Ed__12_tBCB91730C7554E4E134042CDDD1550F32438D3F8::get_offset_of_U3CstepCountU3E5__3_8(),
	U3CExecuteFilterU3Ed__12_tBCB91730C7554E4E134042CDDD1550F32438D3F8::get_offset_of_U3CstopIndexU3E5__4_9(),
	U3CExecuteFilterU3Ed__12_tBCB91730C7554E4E134042CDDD1550F32438D3F8::get_offset_of_U3CpositiveStepU3E5__5_10(),
	U3CExecuteFilterU3Ed__12_tBCB91730C7554E4E134042CDDD1550F32438D3F8::get_offset_of_errorWhenNoMatch_11(),
	U3CExecuteFilterU3Ed__12_tBCB91730C7554E4E134042CDDD1550F32438D3F8::get_offset_of_U3CU3E3__errorWhenNoMatch_12(),
	U3CExecuteFilterU3Ed__12_tBCB91730C7554E4E134042CDDD1550F32438D3F8::get_offset_of_U3CtU3E5__6_13(),
	U3CExecuteFilterU3Ed__12_tBCB91730C7554E4E134042CDDD1550F32438D3F8::get_offset_of_U3CU3E7__wrap1_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6235 = { sizeof (FieldFilter_t2B4992D5A1D10B7A905F49C62609097AED91A13B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6235[1] = 
{
	FieldFilter_t2B4992D5A1D10B7A905F49C62609097AED91A13B::get_offset_of_U3CNameU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6236 = { sizeof (U3CExecuteFilterU3Ed__4_tAF0C3B976817618E87E3152BBD9B3A1DF5D92610), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6236[12] = 
{
	U3CExecuteFilterU3Ed__4_tAF0C3B976817618E87E3152BBD9B3A1DF5D92610::get_offset_of_U3CU3E1__state_0(),
	U3CExecuteFilterU3Ed__4_tAF0C3B976817618E87E3152BBD9B3A1DF5D92610::get_offset_of_U3CU3E2__current_1(),
	U3CExecuteFilterU3Ed__4_tAF0C3B976817618E87E3152BBD9B3A1DF5D92610::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CExecuteFilterU3Ed__4_tAF0C3B976817618E87E3152BBD9B3A1DF5D92610::get_offset_of_current_3(),
	U3CExecuteFilterU3Ed__4_tAF0C3B976817618E87E3152BBD9B3A1DF5D92610::get_offset_of_U3CU3E3__current_4(),
	U3CExecuteFilterU3Ed__4_tAF0C3B976817618E87E3152BBD9B3A1DF5D92610::get_offset_of_U3CU3E4__this_5(),
	U3CExecuteFilterU3Ed__4_tAF0C3B976817618E87E3152BBD9B3A1DF5D92610::get_offset_of_errorWhenNoMatch_6(),
	U3CExecuteFilterU3Ed__4_tAF0C3B976817618E87E3152BBD9B3A1DF5D92610::get_offset_of_U3CU3E3__errorWhenNoMatch_7(),
	U3CExecuteFilterU3Ed__4_tAF0C3B976817618E87E3152BBD9B3A1DF5D92610::get_offset_of_U3CoU3E5__1_8(),
	U3CExecuteFilterU3Ed__4_tAF0C3B976817618E87E3152BBD9B3A1DF5D92610::get_offset_of_U3CtU3E5__2_9(),
	U3CExecuteFilterU3Ed__4_tAF0C3B976817618E87E3152BBD9B3A1DF5D92610::get_offset_of_U3CU3E7__wrap1_10(),
	U3CExecuteFilterU3Ed__4_tAF0C3B976817618E87E3152BBD9B3A1DF5D92610::get_offset_of_U3CU3E7__wrap2_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6237 = { sizeof (FieldMultipleFilter_tE61362AE319B8C510FD11900AD0A0B79EC08CD6B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6237[1] = 
{
	FieldMultipleFilter_tE61362AE319B8C510FD11900AD0A0B79EC08CD6B::get_offset_of_U3CNamesU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6238 = { sizeof (U3CU3Ec_t7AF93AC964FCE6A240C24249983367E897CC068D), -1, sizeof(U3CU3Ec_t7AF93AC964FCE6A240C24249983367E897CC068D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6238[2] = 
{
	U3CU3Ec_t7AF93AC964FCE6A240C24249983367E897CC068D_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t7AF93AC964FCE6A240C24249983367E897CC068D_StaticFields::get_offset_of_U3CU3E9__4_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6239 = { sizeof (U3CExecuteFilterU3Ed__4_t0C1DEC1C597C26BA75BE873F98008D01240AE9BB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6239[13] = 
{
	U3CExecuteFilterU3Ed__4_t0C1DEC1C597C26BA75BE873F98008D01240AE9BB::get_offset_of_U3CU3E1__state_0(),
	U3CExecuteFilterU3Ed__4_t0C1DEC1C597C26BA75BE873F98008D01240AE9BB::get_offset_of_U3CU3E2__current_1(),
	U3CExecuteFilterU3Ed__4_t0C1DEC1C597C26BA75BE873F98008D01240AE9BB::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CExecuteFilterU3Ed__4_t0C1DEC1C597C26BA75BE873F98008D01240AE9BB::get_offset_of_current_3(),
	U3CExecuteFilterU3Ed__4_t0C1DEC1C597C26BA75BE873F98008D01240AE9BB::get_offset_of_U3CU3E3__current_4(),
	U3CExecuteFilterU3Ed__4_t0C1DEC1C597C26BA75BE873F98008D01240AE9BB::get_offset_of_U3CU3E4__this_5(),
	U3CExecuteFilterU3Ed__4_t0C1DEC1C597C26BA75BE873F98008D01240AE9BB::get_offset_of_U3CoU3E5__1_6(),
	U3CExecuteFilterU3Ed__4_t0C1DEC1C597C26BA75BE873F98008D01240AE9BB::get_offset_of_errorWhenNoMatch_7(),
	U3CExecuteFilterU3Ed__4_t0C1DEC1C597C26BA75BE873F98008D01240AE9BB::get_offset_of_U3CU3E3__errorWhenNoMatch_8(),
	U3CExecuteFilterU3Ed__4_t0C1DEC1C597C26BA75BE873F98008D01240AE9BB::get_offset_of_U3CnameU3E5__2_9(),
	U3CExecuteFilterU3Ed__4_t0C1DEC1C597C26BA75BE873F98008D01240AE9BB::get_offset_of_U3CtU3E5__3_10(),
	U3CExecuteFilterU3Ed__4_t0C1DEC1C597C26BA75BE873F98008D01240AE9BB::get_offset_of_U3CU3E7__wrap1_11(),
	U3CExecuteFilterU3Ed__4_t0C1DEC1C597C26BA75BE873F98008D01240AE9BB::get_offset_of_U3CU3E7__wrap2_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6240 = { sizeof (JPath_t3A0A64CF9AA620DDBE9D3C20AE75A7AE1E9225D7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6241 = { sizeof (PathFilter_tF860A9811AE814CCD54914A44C2A08AA07FECA80), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6242 = { sizeof (QueryOperator_t91AB4C16055C42C3D23EBCE0B21564266F4B8130)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6242[11] = 
{
	QueryOperator_t91AB4C16055C42C3D23EBCE0B21564266F4B8130::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6243 = { sizeof (QueryExpression_t08B8769F946D3E05F6E9DB46393F0B9828244E7F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6243[1] = 
{
	QueryExpression_t08B8769F946D3E05F6E9DB46393F0B9828244E7F::get_offset_of_U3COperatorU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6244 = { sizeof (CompositeExpression_t3153D0FCDDFBADAA4ED86DCB1527031D389F91E9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6244[1] = 
{
	CompositeExpression_t3153D0FCDDFBADAA4ED86DCB1527031D389F91E9::get_offset_of_U3CExpressionsU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6245 = { sizeof (BooleanQueryExpression_t6F2B15128729297EA0C686FD823C18D7616D667F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6245[2] = 
{
	BooleanQueryExpression_t6F2B15128729297EA0C686FD823C18D7616D667F::get_offset_of_U3CPathU3Ek__BackingField_1(),
	BooleanQueryExpression_t6F2B15128729297EA0C686FD823C18D7616D667F::get_offset_of_U3CValueU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6246 = { sizeof (QueryFilter_t6BBC6AA8C49E2AEED63087A9FCEB306F022C22AF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6246[1] = 
{
	QueryFilter_t6BBC6AA8C49E2AEED63087A9FCEB306F022C22AF::get_offset_of_U3CExpressionU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6247 = { sizeof (U3CExecuteFilterU3Ed__4_t1092E9A17F5C737B43A85A54B82042D32C7E0683), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6247[8] = 
{
	U3CExecuteFilterU3Ed__4_t1092E9A17F5C737B43A85A54B82042D32C7E0683::get_offset_of_U3CU3E1__state_0(),
	U3CExecuteFilterU3Ed__4_t1092E9A17F5C737B43A85A54B82042D32C7E0683::get_offset_of_U3CU3E2__current_1(),
	U3CExecuteFilterU3Ed__4_t1092E9A17F5C737B43A85A54B82042D32C7E0683::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CExecuteFilterU3Ed__4_t1092E9A17F5C737B43A85A54B82042D32C7E0683::get_offset_of_current_3(),
	U3CExecuteFilterU3Ed__4_t1092E9A17F5C737B43A85A54B82042D32C7E0683::get_offset_of_U3CU3E3__current_4(),
	U3CExecuteFilterU3Ed__4_t1092E9A17F5C737B43A85A54B82042D32C7E0683::get_offset_of_U3CU3E4__this_5(),
	U3CExecuteFilterU3Ed__4_t1092E9A17F5C737B43A85A54B82042D32C7E0683::get_offset_of_U3CU3E7__wrap1_6(),
	U3CExecuteFilterU3Ed__4_t1092E9A17F5C737B43A85A54B82042D32C7E0683::get_offset_of_U3CU3E7__wrap2_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6248 = { sizeof (ScanFilter_t1FC1A02EA817F7D53FCA21E7FBB5DF326FC03F3D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6248[1] = 
{
	ScanFilter_t1FC1A02EA817F7D53FCA21E7FBB5DF326FC03F3D::get_offset_of_U3CNameU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6249 = { sizeof (U3CExecuteFilterU3Ed__4_t69C2964074761EFD7077E24C57036C7DC5E33510), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6249[9] = 
{
	U3CExecuteFilterU3Ed__4_t69C2964074761EFD7077E24C57036C7DC5E33510::get_offset_of_U3CU3E1__state_0(),
	U3CExecuteFilterU3Ed__4_t69C2964074761EFD7077E24C57036C7DC5E33510::get_offset_of_U3CU3E2__current_1(),
	U3CExecuteFilterU3Ed__4_t69C2964074761EFD7077E24C57036C7DC5E33510::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CExecuteFilterU3Ed__4_t69C2964074761EFD7077E24C57036C7DC5E33510::get_offset_of_current_3(),
	U3CExecuteFilterU3Ed__4_t69C2964074761EFD7077E24C57036C7DC5E33510::get_offset_of_U3CU3E3__current_4(),
	U3CExecuteFilterU3Ed__4_t69C2964074761EFD7077E24C57036C7DC5E33510::get_offset_of_U3CU3E4__this_5(),
	U3CExecuteFilterU3Ed__4_t69C2964074761EFD7077E24C57036C7DC5E33510::get_offset_of_U3CrootU3E5__1_6(),
	U3CExecuteFilterU3Ed__4_t69C2964074761EFD7077E24C57036C7DC5E33510::get_offset_of_U3CvalueU3E5__2_7(),
	U3CExecuteFilterU3Ed__4_t69C2964074761EFD7077E24C57036C7DC5E33510::get_offset_of_U3CU3E7__wrap1_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6250 = { sizeof (BinaryConverter_t9DA25409355A29DFE356701CAD581E231D13DEF6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6250[1] = 
{
	BinaryConverter_t9DA25409355A29DFE356701CAD581E231D13DEF6::get_offset_of__reflectionObject_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6251 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6252 = { sizeof (DateTimeConverterBase_tC0C56E1B50018AB9DF9B2F7F22FA0001AC1B1E71), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6253 = { sizeof (HashSetConverter_tF86B72BCF0F72DCF91313ED0D2D8E572841EEF55), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6254 = { sizeof (KeyValuePairConverter_tD36E6790020AC069084D9D08BA6159A38E5E5557), -1, sizeof(KeyValuePairConverter_tD36E6790020AC069084D9D08BA6159A38E5E5557_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6254[1] = 
{
	KeyValuePairConverter_tD36E6790020AC069084D9D08BA6159A38E5E5557_StaticFields::get_offset_of_ReflectionObjectPerType_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6255 = { sizeof (BsonObjectIdConverter_t9DA35F1AB243B9FEA9CF962B4C3AAD720EB7A812), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6256 = { sizeof (RegexConverter_t043C392623207631E6876907924EEA55F1AC05EE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6257 = { sizeof (StringEnumConverter_t83AC1D5A05449B4E310BC9F16FD23D2BFF16E360), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6257[2] = 
{
	StringEnumConverter_t83AC1D5A05449B4E310BC9F16FD23D2BFF16E360::get_offset_of_U3CCamelCaseTextU3Ek__BackingField_0(),
	StringEnumConverter_t83AC1D5A05449B4E310BC9F16FD23D2BFF16E360::get_offset_of_U3CAllowIntegerValuesU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6258 = { sizeof (VectorConverter_tEC7B5B3CB2C15E4301869F3429AEB966F050461E), -1, sizeof(VectorConverter_tEC7B5B3CB2C15E4301869F3429AEB966F050461E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6258[6] = 
{
	VectorConverter_tEC7B5B3CB2C15E4301869F3429AEB966F050461E_StaticFields::get_offset_of_V2_0(),
	VectorConverter_tEC7B5B3CB2C15E4301869F3429AEB966F050461E_StaticFields::get_offset_of_V3_1(),
	VectorConverter_tEC7B5B3CB2C15E4301869F3429AEB966F050461E_StaticFields::get_offset_of_V4_2(),
	VectorConverter_tEC7B5B3CB2C15E4301869F3429AEB966F050461E::get_offset_of_U3CEnableVector2U3Ek__BackingField_3(),
	VectorConverter_tEC7B5B3CB2C15E4301869F3429AEB966F050461E::get_offset_of_U3CEnableVector3U3Ek__BackingField_4(),
	VectorConverter_tEC7B5B3CB2C15E4301869F3429AEB966F050461E::get_offset_of_U3CEnableVector4U3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6259 = { sizeof (VersionConverter_t4139946CA5A33692FD4B29D8DBF59786785A9D0A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6260 = { sizeof (IsoDateTimeConverter_t6DC548D85347941EBC77B040DFD331DF40DE45DF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6260[3] = 
{
	IsoDateTimeConverter_t6DC548D85347941EBC77B040DFD331DF40DE45DF::get_offset_of__dateTimeStyles_0(),
	IsoDateTimeConverter_t6DC548D85347941EBC77B040DFD331DF40DE45DF::get_offset_of__dateTimeFormat_1(),
	IsoDateTimeConverter_t6DC548D85347941EBC77B040DFD331DF40DE45DF::get_offset_of__culture_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6261 = { sizeof (JavaScriptDateTimeConverter_t854D8700203B04F436CCD4AF9C2CCC5E8D7CB654), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6262 = { sizeof (XmlDocumentWrapper_tE2D345B4D840F71EC53B07B538A461FA713B189F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6262[1] = 
{
	XmlDocumentWrapper_tE2D345B4D840F71EC53B07B538A461FA713B189F::get_offset_of__document_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6263 = { sizeof (XmlElementWrapper_t5B1B956EEE32EA198D705DBE93B1A8ADC2159C2A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6263[1] = 
{
	XmlElementWrapper_t5B1B956EEE32EA198D705DBE93B1A8ADC2159C2A::get_offset_of__element_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6264 = { sizeof (XmlDeclarationWrapper_tE2CA38569645B999EC3B89B8F301A1AAF4DA1065), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6264[1] = 
{
	XmlDeclarationWrapper_tE2CA38569645B999EC3B89B8F301A1AAF4DA1065::get_offset_of__declaration_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6265 = { sizeof (XmlDocumentTypeWrapper_t7A371AEA41218C3DAB1D3C75BC81B46D4DAA3E40), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6265[1] = 
{
	XmlDocumentTypeWrapper_t7A371AEA41218C3DAB1D3C75BC81B46D4DAA3E40::get_offset_of__documentType_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6266 = { sizeof (XmlNodeWrapper_t3C0D443DF6B193527862D3D680F0BD1979668C3F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6266[3] = 
{
	XmlNodeWrapper_t3C0D443DF6B193527862D3D680F0BD1979668C3F::get_offset_of__node_0(),
	XmlNodeWrapper_t3C0D443DF6B193527862D3D680F0BD1979668C3F::get_offset_of__childNodes_1(),
	XmlNodeWrapper_t3C0D443DF6B193527862D3D680F0BD1979668C3F::get_offset_of__attributes_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6267 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6268 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6269 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6270 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6271 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6272 = { sizeof (XDeclarationWrapper_t0D5F3B36907DE90FC2F7CE9764B78E1B6A7DE5EA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6272[1] = 
{
	XDeclarationWrapper_t0D5F3B36907DE90FC2F7CE9764B78E1B6A7DE5EA::get_offset_of_U3CDeclarationU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6273 = { sizeof (XDocumentTypeWrapper_t62001F61701C5B2FB3301351C1C124BBCDB40591), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6273[1] = 
{
	XDocumentTypeWrapper_t62001F61701C5B2FB3301351C1C124BBCDB40591::get_offset_of__documentType_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6274 = { sizeof (XDocumentWrapper_t675AE9E51FF9329C72D5445BE0005388421EB163), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6275 = { sizeof (XTextWrapper_t519A345B2A497D9E340113365C8397861F0869CA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6276 = { sizeof (XCommentWrapper_tA349A28180A9192EC8765859993F634E7648505C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6277 = { sizeof (XProcessingInstructionWrapper_tC2AAC446F0106BCD342DB3DB9834D7FBDD2C1A61), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6278 = { sizeof (XContainerWrapper_t3FC803A38484BB3B2C7A61D5498A5EE31C9AFC58), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6278[1] = 
{
	XContainerWrapper_t3FC803A38484BB3B2C7A61D5498A5EE31C9AFC58::get_offset_of__childNodes_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6279 = { sizeof (XObjectWrapper_tAB3FD0F91A7839A07C2FD1C40020F14201650305), -1, sizeof(XObjectWrapper_tAB3FD0F91A7839A07C2FD1C40020F14201650305_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6279[2] = 
{
	XObjectWrapper_tAB3FD0F91A7839A07C2FD1C40020F14201650305_StaticFields::get_offset_of_EmptyChildNodes_0(),
	XObjectWrapper_tAB3FD0F91A7839A07C2FD1C40020F14201650305::get_offset_of__xmlObject_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6280 = { sizeof (XAttributeWrapper_tB94EBF566DA78B2D721A00F66EBFFAB9B0748E84), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6281 = { sizeof (XElementWrapper_t4C78ACAA2626B439978C1C03F9B61CFDF26557A9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6281[1] = 
{
	XElementWrapper_t4C78ACAA2626B439978C1C03F9B61CFDF26557A9::get_offset_of__attributes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6282 = { sizeof (XmlNodeConverter_tE53C7D00E9CBED8B2F7DCE9D4D2E5DFD3E8AB68B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6282[3] = 
{
	XmlNodeConverter_tE53C7D00E9CBED8B2F7DCE9D4D2E5DFD3E8AB68B::get_offset_of_U3CDeserializeRootElementNameU3Ek__BackingField_0(),
	XmlNodeConverter_tE53C7D00E9CBED8B2F7DCE9D4D2E5DFD3E8AB68B::get_offset_of_U3CWriteArrayAttributeU3Ek__BackingField_1(),
	XmlNodeConverter_tE53C7D00E9CBED8B2F7DCE9D4D2E5DFD3E8AB68B::get_offset_of_U3COmitRootObjectU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6283 = { sizeof (BsonBinaryType_t82EF959604654BA62C48C95135EC2F8A896928E3)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6283[8] = 
{
	BsonBinaryType_t82EF959604654BA62C48C95135EC2F8A896928E3::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6284 = { sizeof (BsonBinaryWriter_t3572084F4EE9D0ACA3EAE31EF1C0DF358D080D80), -1, sizeof(BsonBinaryWriter_t3572084F4EE9D0ACA3EAE31EF1C0DF358D080D80_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6284[4] = 
{
	BsonBinaryWriter_t3572084F4EE9D0ACA3EAE31EF1C0DF358D080D80_StaticFields::get_offset_of_Encoding_0(),
	BsonBinaryWriter_t3572084F4EE9D0ACA3EAE31EF1C0DF358D080D80::get_offset_of__writer_1(),
	BsonBinaryWriter_t3572084F4EE9D0ACA3EAE31EF1C0DF358D080D80::get_offset_of__largeByteBuffer_2(),
	BsonBinaryWriter_t3572084F4EE9D0ACA3EAE31EF1C0DF358D080D80::get_offset_of_U3CDateTimeKindHandlingU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6285 = { sizeof (BsonReader_t4D59365861A2E8FA5D010B3FE20B696CDE3D53A0), -1, sizeof(BsonReader_t4D59365861A2E8FA5D010B3FE20B696CDE3D53A0_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6285[14] = 
{
	BsonReader_t4D59365861A2E8FA5D010B3FE20B696CDE3D53A0_StaticFields::get_offset_of_SeqRange1_15(),
	BsonReader_t4D59365861A2E8FA5D010B3FE20B696CDE3D53A0_StaticFields::get_offset_of_SeqRange2_16(),
	BsonReader_t4D59365861A2E8FA5D010B3FE20B696CDE3D53A0_StaticFields::get_offset_of_SeqRange3_17(),
	BsonReader_t4D59365861A2E8FA5D010B3FE20B696CDE3D53A0_StaticFields::get_offset_of_SeqRange4_18(),
	BsonReader_t4D59365861A2E8FA5D010B3FE20B696CDE3D53A0::get_offset_of__reader_19(),
	BsonReader_t4D59365861A2E8FA5D010B3FE20B696CDE3D53A0::get_offset_of__stack_20(),
	BsonReader_t4D59365861A2E8FA5D010B3FE20B696CDE3D53A0::get_offset_of__byteBuffer_21(),
	BsonReader_t4D59365861A2E8FA5D010B3FE20B696CDE3D53A0::get_offset_of__charBuffer_22(),
	BsonReader_t4D59365861A2E8FA5D010B3FE20B696CDE3D53A0::get_offset_of__currentElementType_23(),
	BsonReader_t4D59365861A2E8FA5D010B3FE20B696CDE3D53A0::get_offset_of__bsonReaderState_24(),
	BsonReader_t4D59365861A2E8FA5D010B3FE20B696CDE3D53A0::get_offset_of__currentContext_25(),
	BsonReader_t4D59365861A2E8FA5D010B3FE20B696CDE3D53A0::get_offset_of__readRootValueAsArray_26(),
	BsonReader_t4D59365861A2E8FA5D010B3FE20B696CDE3D53A0::get_offset_of__jsonNet35BinaryCompatibility_27(),
	BsonReader_t4D59365861A2E8FA5D010B3FE20B696CDE3D53A0::get_offset_of__dateTimeKindHandling_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6286 = { sizeof (BsonReaderState_tF76E9F238D6B7DD2C4492104D599ABBD209C6C81)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6286[10] = 
{
	BsonReaderState_tF76E9F238D6B7DD2C4492104D599ABBD209C6C81::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6287 = { sizeof (ContainerContext_t863E55417E974D1C50EA7E90F62077FC15D5241F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6287[3] = 
{
	ContainerContext_t863E55417E974D1C50EA7E90F62077FC15D5241F::get_offset_of_Type_0(),
	ContainerContext_t863E55417E974D1C50EA7E90F62077FC15D5241F::get_offset_of_Length_1(),
	ContainerContext_t863E55417E974D1C50EA7E90F62077FC15D5241F::get_offset_of_Position_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6288 = { sizeof (BsonToken_tEB681A5D0E53F8005DC31BAEFDE6481BBE80C2D3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6288[2] = 
{
	BsonToken_tEB681A5D0E53F8005DC31BAEFDE6481BBE80C2D3::get_offset_of_U3CParentU3Ek__BackingField_0(),
	BsonToken_tEB681A5D0E53F8005DC31BAEFDE6481BBE80C2D3::get_offset_of_U3CCalculatedSizeU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6289 = { sizeof (BsonObject_tFB8AF55378FE99FCE476FF65B1667A0D45F93518), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6289[1] = 
{
	BsonObject_tFB8AF55378FE99FCE476FF65B1667A0D45F93518::get_offset_of__children_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6290 = { sizeof (BsonArray_tFFCE8804FBA87DECA9A84178C668305D8E89B6A7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6290[1] = 
{
	BsonArray_tFFCE8804FBA87DECA9A84178C668305D8E89B6A7::get_offset_of__children_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6291 = { sizeof (BsonValue_tB2A4632B6C4CAF0DB566AA14D13E0C0526328CF0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6291[2] = 
{
	BsonValue_tB2A4632B6C4CAF0DB566AA14D13E0C0526328CF0::get_offset_of__value_2(),
	BsonValue_tB2A4632B6C4CAF0DB566AA14D13E0C0526328CF0::get_offset_of__type_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6292 = { sizeof (BsonString_t87A7877CE01E3E2FA2B118C4A617FA16159CBC0A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6292[2] = 
{
	BsonString_t87A7877CE01E3E2FA2B118C4A617FA16159CBC0A::get_offset_of_U3CByteCountU3Ek__BackingField_4(),
	BsonString_t87A7877CE01E3E2FA2B118C4A617FA16159CBC0A::get_offset_of_U3CIncludeLengthU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6293 = { sizeof (BsonBinary_tD19C8F767A5AADB1A3B81A4BFA2ABECBC8082982), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6293[1] = 
{
	BsonBinary_tD19C8F767A5AADB1A3B81A4BFA2ABECBC8082982::get_offset_of_U3CBinaryTypeU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6294 = { sizeof (BsonRegex_t5C64E5D5DABD795CAC97069165054859A0B938D3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6294[2] = 
{
	BsonRegex_t5C64E5D5DABD795CAC97069165054859A0B938D3::get_offset_of_U3CPatternU3Ek__BackingField_2(),
	BsonRegex_t5C64E5D5DABD795CAC97069165054859A0B938D3::get_offset_of_U3COptionsU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6295 = { sizeof (BsonProperty_t82747119CE439C13F6809CD9F8D815C3CE429583), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6295[2] = 
{
	BsonProperty_t82747119CE439C13F6809CD9F8D815C3CE429583::get_offset_of_U3CNameU3Ek__BackingField_0(),
	BsonProperty_t82747119CE439C13F6809CD9F8D815C3CE429583::get_offset_of_U3CValueU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6296 = { sizeof (BsonType_tBF3253E8D497F6E8F668EB77C7B6345854BE5538)+ sizeof (RuntimeObject), sizeof(int8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6296[21] = 
{
	BsonType_tBF3253E8D497F6E8F668EB77C7B6345854BE5538::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6297 = { sizeof (BsonWriter_tD3EA6BE4366DF3B2C0E30C83318262FED969CE11), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6297[4] = 
{
	BsonWriter_tD3EA6BE4366DF3B2C0E30C83318262FED969CE11::get_offset_of__writer_13(),
	BsonWriter_tD3EA6BE4366DF3B2C0E30C83318262FED969CE11::get_offset_of__root_14(),
	BsonWriter_tD3EA6BE4366DF3B2C0E30C83318262FED969CE11::get_offset_of__parent_15(),
	BsonWriter_tD3EA6BE4366DF3B2C0E30C83318262FED969CE11::get_offset_of__propertyName_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6298 = { sizeof (BsonObjectId_tF24F67A9B09CB213513B5218DF725B4E9AF3379C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6298[1] = 
{
	BsonObjectId_tF24F67A9B09CB213513B5218DF725B4E9AF3379C::get_offset_of_U3CValueU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6299 = { sizeof (U3CPrivateImplementationDetailsU3E_t1D1ACC86E2F3D4A2C7CA568000402E9AC4556D7E), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1D1ACC86E2F3D4A2C7CA568000402E9AC4556D7E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6299[5] = 
{
	U3CPrivateImplementationDetailsU3E_t1D1ACC86E2F3D4A2C7CA568000402E9AC4556D7E_StaticFields::get_offset_of_U39E31F24F64765FCAA589F589324D17C9FCF6A06D_0(),
	U3CPrivateImplementationDetailsU3E_t1D1ACC86E2F3D4A2C7CA568000402E9AC4556D7E_StaticFields::get_offset_of_ADFD2E1C801C825415DD53F4F2F72A13B389313C_1(),
	U3CPrivateImplementationDetailsU3E_t1D1ACC86E2F3D4A2C7CA568000402E9AC4556D7E_StaticFields::get_offset_of_D40004AB0E92BF6C8DFE481B56BE3D04ABDA76EB_2(),
	U3CPrivateImplementationDetailsU3E_t1D1ACC86E2F3D4A2C7CA568000402E9AC4556D7E_StaticFields::get_offset_of_DD3AEFEADB1CD615F3017763F1568179FEE640B0_3(),
	U3CPrivateImplementationDetailsU3E_t1D1ACC86E2F3D4A2C7CA568000402E9AC4556D7E_StaticFields::get_offset_of_E92B39D8233061927D9ACDE54665E68E7535635A_4(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
