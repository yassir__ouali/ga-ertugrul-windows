﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Microsoft.Win32.SafeHandles.SafeProcessHandle
struct SafeProcessHandle_tEF75BF77F5F4E121334E2A97EE8E1F6685F38CF7;
// Microsoft.Win32.SafeHandles.SafeWaitHandle
struct SafeWaitHandle_t51DB35FF382E636FF3B868D87816733894D46CF2;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Attribute[]
struct AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.ArrayList
struct ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB;
// System.Collections.Generic.Dictionary`2<System.String,System.Resources.ResourceSet>
struct Dictionary_2_tDE0FFCE2C110EEFB68C37CEA54DBCA577AFC1CE6;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226;
// System.Collections.Generic.List`1<System.String>
struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3;
// System.Collections.Generic.List`1<System.Text.RegularExpressions.RegexNode>
struct List_1_tA5CDE89671B691180A7422F86077A0D047AD4059;
// System.Collections.Generic.List`1<System.Text.RegularExpressions.RegexOptions>
struct List_1_t85142A16ADC23C13E223599A626015FD40FF076A;
// System.Collections.Generic.List`1<System.WeakReference>
struct List_1_t0B19BE4139518EFD1F11815FD931281B09EA15EF;
// System.Collections.Hashtable
struct Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.ComponentModel.AddingNewEventArgs
struct AddingNewEventArgs_t848D637EA4E72C12479C42DF55305280B37948D1;
// System.ComponentModel.AsyncCompletedEventArgs
struct AsyncCompletedEventArgs_tADCBE47368F4D8B198B46D332B27EE38C8B5F8B7;
// System.ComponentModel.AsyncOperation
struct AsyncOperation_tFA5B90911F8ABE8B07A3058301687DDDF5F78748;
// System.ComponentModel.AttributeCollection
struct AttributeCollection_tBE6941BB802EDE34B7F986C14A7D7E3A4E135EBE;
// System.ComponentModel.AttributeCollection/AttributeEntry[]
struct AttributeEntryU5BU5D_t82EF07E3984B106E346A6B35336772424C0FCB5A;
// System.ComponentModel.BackgroundWorker/WorkerThreadStartDelegate
struct WorkerThreadStartDelegate_tEC42174597C1FB97F214AE61808E6F0CE75BF7B6;
// System.ComponentModel.CancelEventArgs
struct CancelEventArgs_t2843141F2893A01C11535CD7E38072CA26D7794D;
// System.ComponentModel.CategoryAttribute
struct CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80;
// System.ComponentModel.CollectionChangeEventArgs
struct CollectionChangeEventArgs_t63CA165C1F7D765B04CB139EB6577577479E57B0;
// System.ComponentModel.ComponentCollection
struct ComponentCollection_tCA923631B2E55E6A831F9D5CF5231113764CB895;
// System.ComponentModel.Container
struct Container_tC98CD69632DEA16FC061454A770CC7D11FFFEEF1;
// System.ComponentModel.ContainerFilterService
struct ContainerFilterService_t4E2FD208B28F83EA50EBC66500B7DCA29D38FA19;
// System.ComponentModel.CultureInfoConverter
struct CultureInfoConverter_t6BD4316CF7C9163A01C35EC9382BC3D1C01FB6C0;
// System.ComponentModel.EventHandlerList
struct EventHandlerList_tFE9EF79E85419EBB2C206CF475E29A9960699BE4;
// System.ComponentModel.IComponent
struct IComponent_t2227ADCB5304EAFD55C447E3EE8453437941B4C6;
// System.ComponentModel.ICustomTypeDescriptor
struct ICustomTypeDescriptor_tAF366F5EE1A787B4D24D9570FFFFE6C5DC27C384;
// System.ComponentModel.ISite
struct ISite_t6804B48BC23ABB5F4141903F878589BCEF6097A2;
// System.ComponentModel.ISite[]
struct ISiteU5BU5D_t26A1BA57EE8683FC59C3BDD15CA00512F5A520A8;
// System.ComponentModel.ISynchronizeInvoke
struct ISynchronizeInvoke_t7A89CE9A5D792F694D7A5C33B2716937C39E783A;
// System.ComponentModel.TypeConverter
struct TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB;
// System.ComponentModel.TypeConverter/StandardValuesCollection
struct StandardValuesCollection_t929677712574EF02F5C4CF4C38E070841C20BDA3;
// System.ComponentModel.TypeDescriptionProvider/EmptyCustomTypeDescriptor
struct EmptyCustomTypeDescriptor_tDDCC5072613D7D0405BCCF3D49F3EF7B4E1E7DB8;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Diagnostics.AsyncStreamReader
struct AsyncStreamReader_t2C28E845971B756383AF73AEF2A86C7545E5C485;
// System.Diagnostics.BooleanSwitch
struct BooleanSwitch_t2B51F0E7C50E7DAA74282B51EF9107A2E54723E0;
// System.Diagnostics.CorrelationManager
struct CorrelationManager_t791B7FE5E821DC5746198251B11EDB293CB60120;
// System.Diagnostics.ProcessModuleCollection
struct ProcessModuleCollection_t93E76B9948E84325744E8C57A525FD465D78DB3F;
// System.Diagnostics.ProcessThreadCollection
struct ProcessThreadCollection_t6D1D2E676ED1F65087080729F91410724CA74DA7;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.Diagnostics.TraceFilter
struct TraceFilter_t5BA76D3899B80AEA894D4040364099DC7C47C6F1;
// System.Diagnostics.TraceListenerCollection
struct TraceListenerCollection_t392DC090EA67B680F4818E494FB5FE85AA82FBD6;
// System.Diagnostics.TraceSwitch
struct TraceSwitch_t32D210D5C9B05D9E555925260EEC3767BA895EC8;
// System.EventHandler
struct EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C;
// System.Exception
struct Exception_t;
// System.Globalization.CultureInfo
struct CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.IO.Stream
struct Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7;
// System.IO.StreamReader
struct StreamReader_t62E68063760DCD2FC036AE132DE69C24B7ED001E;
// System.IO.StreamWriter
struct StreamWriter_t989B894EF3BFCDF6FF5F5F068402A4F835FC8E8E;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Int32[][]
struct Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Reflection.Assembly
struct Assembly_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Reflection.RuntimeAssembly
struct RuntimeAssembly_t5EE9CD749D82345AE5635B9665665C31A3308EB1;
// System.Resources.IResourceGroveler
struct IResourceGroveler_tCEF78094E38045CAD9EFB296E179CCDFDCB94C44;
// System.Resources.ResourceManager/CultureNameResourceSetPair
struct CultureNameResourceSetPair_t77328DA298FCF741DE21CC5B3E19F160D7060074;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Text.Decoder
struct Decoder_tEEF45EB6F965222036C49E8EC6BA8A0692AA1F26;
// System.Text.Encoding
struct Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4;
// System.Text.RegularExpressions.GroupCollection
struct GroupCollection_tD9051ED1A991E3666439262B517FDD2F968C064F;
// System.Text.RegularExpressions.Match
struct Match_tE447871AB59EED3642F31EB9559D162C2977EBB5;
// System.Text.RegularExpressions.MatchCollection
struct MatchCollection_t8B954CE579727B076276B514F08040EE4D26844A;
// System.Text.RegularExpressions.Regex
struct Regex_tFD46E63A462E852189FD6AB4E2B0B67C4D8FDBDF;
// System.Text.RegularExpressions.RegexNode
struct RegexNode_tF92FC16590D5B00965BABFC709BA677DA0FC3F75;
// System.Threading.ManualResetEvent
struct ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408;
// System.Threading.RegisteredWaitHandle
struct RegisteredWaitHandle_t25AAC0B53C62CFA0B3F9BFFA87DDA3638F4308C0;
// System.Threading.SendOrPostCallback
struct SendOrPostCallback_t3F9C0164860E4AA5138DF8B4488DFB0D33147F01;
// System.Threading.SynchronizationContext
struct SynchronizationContext_t06AEFE2C7CFCFC242D0A5729A74464AF18CF84E7;
// System.Threading.WaitHandle
struct WaitHandle_tFD46B5B45A6BB296EA3A104C91DF2A7C03C10AC6;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F;
// System.Version
struct Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;

struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#define ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifndef READONLYCOLLECTIONBASE_TFD695167917CE6DF4FA18A906FA530880B9B8772_H
#define READONLYCOLLECTIONBASE_TFD695167917CE6DF4FA18A906FA530880B9B8772_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.ReadOnlyCollectionBase
struct  ReadOnlyCollectionBase_tFD695167917CE6DF4FA18A906FA530880B9B8772  : public RuntimeObject
{
public:
	// System.Collections.ArrayList System.Collections.ReadOnlyCollectionBase::list
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ___list_0;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(ReadOnlyCollectionBase_tFD695167917CE6DF4FA18A906FA530880B9B8772, ___list_0)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get_list_0() const { return ___list_0; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READONLYCOLLECTIONBASE_TFD695167917CE6DF4FA18A906FA530880B9B8772_H
#ifndef ARRAYSUBSETENUMERATOR_TA39E27E02B80287A51F09D2875F1A3AF86768BDA_H
#define ARRAYSUBSETENUMERATOR_TA39E27E02B80287A51F09D2875F1A3AF86768BDA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.ArraySubsetEnumerator
struct  ArraySubsetEnumerator_tA39E27E02B80287A51F09D2875F1A3AF86768BDA  : public RuntimeObject
{
public:
	// System.Array System.ComponentModel.ArraySubsetEnumerator::array
	RuntimeArray * ___array_0;
	// System.Int32 System.ComponentModel.ArraySubsetEnumerator::total
	int32_t ___total_1;
	// System.Int32 System.ComponentModel.ArraySubsetEnumerator::current
	int32_t ___current_2;

public:
	inline static int32_t get_offset_of_array_0() { return static_cast<int32_t>(offsetof(ArraySubsetEnumerator_tA39E27E02B80287A51F09D2875F1A3AF86768BDA, ___array_0)); }
	inline RuntimeArray * get_array_0() const { return ___array_0; }
	inline RuntimeArray ** get_address_of_array_0() { return &___array_0; }
	inline void set_array_0(RuntimeArray * value)
	{
		___array_0 = value;
		Il2CppCodeGenWriteBarrier((&___array_0), value);
	}

	inline static int32_t get_offset_of_total_1() { return static_cast<int32_t>(offsetof(ArraySubsetEnumerator_tA39E27E02B80287A51F09D2875F1A3AF86768BDA, ___total_1)); }
	inline int32_t get_total_1() const { return ___total_1; }
	inline int32_t* get_address_of_total_1() { return &___total_1; }
	inline void set_total_1(int32_t value)
	{
		___total_1 = value;
	}

	inline static int32_t get_offset_of_current_2() { return static_cast<int32_t>(offsetof(ArraySubsetEnumerator_tA39E27E02B80287A51F09D2875F1A3AF86768BDA, ___current_2)); }
	inline int32_t get_current_2() const { return ___current_2; }
	inline int32_t* get_address_of_current_2() { return &___current_2; }
	inline void set_current_2(int32_t value)
	{
		___current_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRAYSUBSETENUMERATOR_TA39E27E02B80287A51F09D2875F1A3AF86768BDA_H
#ifndef ASYNCOPERATION_TFA5B90911F8ABE8B07A3058301687DDDF5F78748_H
#define ASYNCOPERATION_TFA5B90911F8ABE8B07A3058301687DDDF5F78748_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.AsyncOperation
struct  AsyncOperation_tFA5B90911F8ABE8B07A3058301687DDDF5F78748  : public RuntimeObject
{
public:
	// System.Threading.SynchronizationContext System.ComponentModel.AsyncOperation::syncContext
	SynchronizationContext_t06AEFE2C7CFCFC242D0A5729A74464AF18CF84E7 * ___syncContext_0;
	// System.Object System.ComponentModel.AsyncOperation::userSuppliedState
	RuntimeObject * ___userSuppliedState_1;
	// System.Boolean System.ComponentModel.AsyncOperation::alreadyCompleted
	bool ___alreadyCompleted_2;

public:
	inline static int32_t get_offset_of_syncContext_0() { return static_cast<int32_t>(offsetof(AsyncOperation_tFA5B90911F8ABE8B07A3058301687DDDF5F78748, ___syncContext_0)); }
	inline SynchronizationContext_t06AEFE2C7CFCFC242D0A5729A74464AF18CF84E7 * get_syncContext_0() const { return ___syncContext_0; }
	inline SynchronizationContext_t06AEFE2C7CFCFC242D0A5729A74464AF18CF84E7 ** get_address_of_syncContext_0() { return &___syncContext_0; }
	inline void set_syncContext_0(SynchronizationContext_t06AEFE2C7CFCFC242D0A5729A74464AF18CF84E7 * value)
	{
		___syncContext_0 = value;
		Il2CppCodeGenWriteBarrier((&___syncContext_0), value);
	}

	inline static int32_t get_offset_of_userSuppliedState_1() { return static_cast<int32_t>(offsetof(AsyncOperation_tFA5B90911F8ABE8B07A3058301687DDDF5F78748, ___userSuppliedState_1)); }
	inline RuntimeObject * get_userSuppliedState_1() const { return ___userSuppliedState_1; }
	inline RuntimeObject ** get_address_of_userSuppliedState_1() { return &___userSuppliedState_1; }
	inline void set_userSuppliedState_1(RuntimeObject * value)
	{
		___userSuppliedState_1 = value;
		Il2CppCodeGenWriteBarrier((&___userSuppliedState_1), value);
	}

	inline static int32_t get_offset_of_alreadyCompleted_2() { return static_cast<int32_t>(offsetof(AsyncOperation_tFA5B90911F8ABE8B07A3058301687DDDF5F78748, ___alreadyCompleted_2)); }
	inline bool get_alreadyCompleted_2() const { return ___alreadyCompleted_2; }
	inline bool* get_address_of_alreadyCompleted_2() { return &___alreadyCompleted_2; }
	inline void set_alreadyCompleted_2(bool value)
	{
		___alreadyCompleted_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCOPERATION_TFA5B90911F8ABE8B07A3058301687DDDF5F78748_H
#ifndef ASYNCOPERATIONMANAGER_T000ED3690BB5C16399184208344B865AF5E7CE7A_H
#define ASYNCOPERATIONMANAGER_T000ED3690BB5C16399184208344B865AF5E7CE7A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.AsyncOperationManager
struct  AsyncOperationManager_t000ED3690BB5C16399184208344B865AF5E7CE7A  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCOPERATIONMANAGER_T000ED3690BB5C16399184208344B865AF5E7CE7A_H
#ifndef ATTRIBUTECOLLECTION_TBE6941BB802EDE34B7F986C14A7D7E3A4E135EBE_H
#define ATTRIBUTECOLLECTION_TBE6941BB802EDE34B7F986C14A7D7E3A4E135EBE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.AttributeCollection
struct  AttributeCollection_tBE6941BB802EDE34B7F986C14A7D7E3A4E135EBE  : public RuntimeObject
{
public:
	// System.Attribute[] System.ComponentModel.AttributeCollection::_attributes
	AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17* ____attributes_2;
	// System.ComponentModel.AttributeCollection_AttributeEntry[] System.ComponentModel.AttributeCollection::_foundAttributeTypes
	AttributeEntryU5BU5D_t82EF07E3984B106E346A6B35336772424C0FCB5A* ____foundAttributeTypes_5;
	// System.Int32 System.ComponentModel.AttributeCollection::_index
	int32_t ____index_6;

public:
	inline static int32_t get_offset_of__attributes_2() { return static_cast<int32_t>(offsetof(AttributeCollection_tBE6941BB802EDE34B7F986C14A7D7E3A4E135EBE, ____attributes_2)); }
	inline AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17* get__attributes_2() const { return ____attributes_2; }
	inline AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17** get_address_of__attributes_2() { return &____attributes_2; }
	inline void set__attributes_2(AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17* value)
	{
		____attributes_2 = value;
		Il2CppCodeGenWriteBarrier((&____attributes_2), value);
	}

	inline static int32_t get_offset_of__foundAttributeTypes_5() { return static_cast<int32_t>(offsetof(AttributeCollection_tBE6941BB802EDE34B7F986C14A7D7E3A4E135EBE, ____foundAttributeTypes_5)); }
	inline AttributeEntryU5BU5D_t82EF07E3984B106E346A6B35336772424C0FCB5A* get__foundAttributeTypes_5() const { return ____foundAttributeTypes_5; }
	inline AttributeEntryU5BU5D_t82EF07E3984B106E346A6B35336772424C0FCB5A** get_address_of__foundAttributeTypes_5() { return &____foundAttributeTypes_5; }
	inline void set__foundAttributeTypes_5(AttributeEntryU5BU5D_t82EF07E3984B106E346A6B35336772424C0FCB5A* value)
	{
		____foundAttributeTypes_5 = value;
		Il2CppCodeGenWriteBarrier((&____foundAttributeTypes_5), value);
	}

	inline static int32_t get_offset_of__index_6() { return static_cast<int32_t>(offsetof(AttributeCollection_tBE6941BB802EDE34B7F986C14A7D7E3A4E135EBE, ____index_6)); }
	inline int32_t get__index_6() const { return ____index_6; }
	inline int32_t* get_address_of__index_6() { return &____index_6; }
	inline void set__index_6(int32_t value)
	{
		____index_6 = value;
	}
};

struct AttributeCollection_tBE6941BB802EDE34B7F986C14A7D7E3A4E135EBE_StaticFields
{
public:
	// System.ComponentModel.AttributeCollection System.ComponentModel.AttributeCollection::Empty
	AttributeCollection_tBE6941BB802EDE34B7F986C14A7D7E3A4E135EBE * ___Empty_0;
	// System.Collections.Hashtable System.ComponentModel.AttributeCollection::_defaultAttributes
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ____defaultAttributes_1;
	// System.Object System.ComponentModel.AttributeCollection::internalSyncObject
	RuntimeObject * ___internalSyncObject_3;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(AttributeCollection_tBE6941BB802EDE34B7F986C14A7D7E3A4E135EBE_StaticFields, ___Empty_0)); }
	inline AttributeCollection_tBE6941BB802EDE34B7F986C14A7D7E3A4E135EBE * get_Empty_0() const { return ___Empty_0; }
	inline AttributeCollection_tBE6941BB802EDE34B7F986C14A7D7E3A4E135EBE ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(AttributeCollection_tBE6941BB802EDE34B7F986C14A7D7E3A4E135EBE * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}

	inline static int32_t get_offset_of__defaultAttributes_1() { return static_cast<int32_t>(offsetof(AttributeCollection_tBE6941BB802EDE34B7F986C14A7D7E3A4E135EBE_StaticFields, ____defaultAttributes_1)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get__defaultAttributes_1() const { return ____defaultAttributes_1; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of__defaultAttributes_1() { return &____defaultAttributes_1; }
	inline void set__defaultAttributes_1(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		____defaultAttributes_1 = value;
		Il2CppCodeGenWriteBarrier((&____defaultAttributes_1), value);
	}

	inline static int32_t get_offset_of_internalSyncObject_3() { return static_cast<int32_t>(offsetof(AttributeCollection_tBE6941BB802EDE34B7F986C14A7D7E3A4E135EBE_StaticFields, ___internalSyncObject_3)); }
	inline RuntimeObject * get_internalSyncObject_3() const { return ___internalSyncObject_3; }
	inline RuntimeObject ** get_address_of_internalSyncObject_3() { return &___internalSyncObject_3; }
	inline void set_internalSyncObject_3(RuntimeObject * value)
	{
		___internalSyncObject_3 = value;
		Il2CppCodeGenWriteBarrier((&___internalSyncObject_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTECOLLECTION_TBE6941BB802EDE34B7F986C14A7D7E3A4E135EBE_H
#ifndef COMPMODSWITCHES_TB835655C9DCFB714C970F60C9B98519276BC499D_H
#define COMPMODSWITCHES_TB835655C9DCFB714C970F60C9B98519276BC499D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.CompModSwitches
struct  CompModSwitches_tB835655C9DCFB714C970F60C9B98519276BC499D  : public RuntimeObject
{
public:

public:
};

struct CompModSwitches_tB835655C9DCFB714C970F60C9B98519276BC499D_StaticFields
{
public:
	// System.Diagnostics.BooleanSwitch modreq(System.Runtime.CompilerServices.IsVolatile) System.ComponentModel.CompModSwitches::commonDesignerServices
	BooleanSwitch_t2B51F0E7C50E7DAA74282B51EF9107A2E54723E0 * ___commonDesignerServices_0;
	// System.Diagnostics.TraceSwitch modreq(System.Runtime.CompilerServices.IsVolatile) System.ComponentModel.CompModSwitches::eventLog
	TraceSwitch_t32D210D5C9B05D9E555925260EEC3767BA895EC8 * ___eventLog_1;

public:
	inline static int32_t get_offset_of_commonDesignerServices_0() { return static_cast<int32_t>(offsetof(CompModSwitches_tB835655C9DCFB714C970F60C9B98519276BC499D_StaticFields, ___commonDesignerServices_0)); }
	inline BooleanSwitch_t2B51F0E7C50E7DAA74282B51EF9107A2E54723E0 * get_commonDesignerServices_0() const { return ___commonDesignerServices_0; }
	inline BooleanSwitch_t2B51F0E7C50E7DAA74282B51EF9107A2E54723E0 ** get_address_of_commonDesignerServices_0() { return &___commonDesignerServices_0; }
	inline void set_commonDesignerServices_0(BooleanSwitch_t2B51F0E7C50E7DAA74282B51EF9107A2E54723E0 * value)
	{
		___commonDesignerServices_0 = value;
		Il2CppCodeGenWriteBarrier((&___commonDesignerServices_0), value);
	}

	inline static int32_t get_offset_of_eventLog_1() { return static_cast<int32_t>(offsetof(CompModSwitches_tB835655C9DCFB714C970F60C9B98519276BC499D_StaticFields, ___eventLog_1)); }
	inline TraceSwitch_t32D210D5C9B05D9E555925260EEC3767BA895EC8 * get_eventLog_1() const { return ___eventLog_1; }
	inline TraceSwitch_t32D210D5C9B05D9E555925260EEC3767BA895EC8 ** get_address_of_eventLog_1() { return &___eventLog_1; }
	inline void set_eventLog_1(TraceSwitch_t32D210D5C9B05D9E555925260EEC3767BA895EC8 * value)
	{
		___eventLog_1 = value;
		Il2CppCodeGenWriteBarrier((&___eventLog_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPMODSWITCHES_TB835655C9DCFB714C970F60C9B98519276BC499D_H
#ifndef COMPONENTEDITOR_T10B6B1D4888C4D6AA6D11C071EE7536EEA69FD2B_H
#define COMPONENTEDITOR_T10B6B1D4888C4D6AA6D11C071EE7536EEA69FD2B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.ComponentEditor
struct  ComponentEditor_t10B6B1D4888C4D6AA6D11C071EE7536EEA69FD2B  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENTEDITOR_T10B6B1D4888C4D6AA6D11C071EE7536EEA69FD2B_H
#ifndef CONTAINER_TC98CD69632DEA16FC061454A770CC7D11FFFEEF1_H
#define CONTAINER_TC98CD69632DEA16FC061454A770CC7D11FFFEEF1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.Container
struct  Container_tC98CD69632DEA16FC061454A770CC7D11FFFEEF1  : public RuntimeObject
{
public:
	// System.ComponentModel.ISite[] System.ComponentModel.Container::sites
	ISiteU5BU5D_t26A1BA57EE8683FC59C3BDD15CA00512F5A520A8* ___sites_0;
	// System.Int32 System.ComponentModel.Container::siteCount
	int32_t ___siteCount_1;
	// System.ComponentModel.ComponentCollection System.ComponentModel.Container::components
	ComponentCollection_tCA923631B2E55E6A831F9D5CF5231113764CB895 * ___components_2;
	// System.ComponentModel.ContainerFilterService System.ComponentModel.Container::filter
	ContainerFilterService_t4E2FD208B28F83EA50EBC66500B7DCA29D38FA19 * ___filter_3;
	// System.Boolean System.ComponentModel.Container::checkedFilter
	bool ___checkedFilter_4;
	// System.Object System.ComponentModel.Container::syncObj
	RuntimeObject * ___syncObj_5;

public:
	inline static int32_t get_offset_of_sites_0() { return static_cast<int32_t>(offsetof(Container_tC98CD69632DEA16FC061454A770CC7D11FFFEEF1, ___sites_0)); }
	inline ISiteU5BU5D_t26A1BA57EE8683FC59C3BDD15CA00512F5A520A8* get_sites_0() const { return ___sites_0; }
	inline ISiteU5BU5D_t26A1BA57EE8683FC59C3BDD15CA00512F5A520A8** get_address_of_sites_0() { return &___sites_0; }
	inline void set_sites_0(ISiteU5BU5D_t26A1BA57EE8683FC59C3BDD15CA00512F5A520A8* value)
	{
		___sites_0 = value;
		Il2CppCodeGenWriteBarrier((&___sites_0), value);
	}

	inline static int32_t get_offset_of_siteCount_1() { return static_cast<int32_t>(offsetof(Container_tC98CD69632DEA16FC061454A770CC7D11FFFEEF1, ___siteCount_1)); }
	inline int32_t get_siteCount_1() const { return ___siteCount_1; }
	inline int32_t* get_address_of_siteCount_1() { return &___siteCount_1; }
	inline void set_siteCount_1(int32_t value)
	{
		___siteCount_1 = value;
	}

	inline static int32_t get_offset_of_components_2() { return static_cast<int32_t>(offsetof(Container_tC98CD69632DEA16FC061454A770CC7D11FFFEEF1, ___components_2)); }
	inline ComponentCollection_tCA923631B2E55E6A831F9D5CF5231113764CB895 * get_components_2() const { return ___components_2; }
	inline ComponentCollection_tCA923631B2E55E6A831F9D5CF5231113764CB895 ** get_address_of_components_2() { return &___components_2; }
	inline void set_components_2(ComponentCollection_tCA923631B2E55E6A831F9D5CF5231113764CB895 * value)
	{
		___components_2 = value;
		Il2CppCodeGenWriteBarrier((&___components_2), value);
	}

	inline static int32_t get_offset_of_filter_3() { return static_cast<int32_t>(offsetof(Container_tC98CD69632DEA16FC061454A770CC7D11FFFEEF1, ___filter_3)); }
	inline ContainerFilterService_t4E2FD208B28F83EA50EBC66500B7DCA29D38FA19 * get_filter_3() const { return ___filter_3; }
	inline ContainerFilterService_t4E2FD208B28F83EA50EBC66500B7DCA29D38FA19 ** get_address_of_filter_3() { return &___filter_3; }
	inline void set_filter_3(ContainerFilterService_t4E2FD208B28F83EA50EBC66500B7DCA29D38FA19 * value)
	{
		___filter_3 = value;
		Il2CppCodeGenWriteBarrier((&___filter_3), value);
	}

	inline static int32_t get_offset_of_checkedFilter_4() { return static_cast<int32_t>(offsetof(Container_tC98CD69632DEA16FC061454A770CC7D11FFFEEF1, ___checkedFilter_4)); }
	inline bool get_checkedFilter_4() const { return ___checkedFilter_4; }
	inline bool* get_address_of_checkedFilter_4() { return &___checkedFilter_4; }
	inline void set_checkedFilter_4(bool value)
	{
		___checkedFilter_4 = value;
	}

	inline static int32_t get_offset_of_syncObj_5() { return static_cast<int32_t>(offsetof(Container_tC98CD69632DEA16FC061454A770CC7D11FFFEEF1, ___syncObj_5)); }
	inline RuntimeObject * get_syncObj_5() const { return ___syncObj_5; }
	inline RuntimeObject ** get_address_of_syncObj_5() { return &___syncObj_5; }
	inline void set_syncObj_5(RuntimeObject * value)
	{
		___syncObj_5 = value;
		Il2CppCodeGenWriteBarrier((&___syncObj_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTAINER_TC98CD69632DEA16FC061454A770CC7D11FFFEEF1_H
#ifndef SITE_TD5DE03CC5F94838106C1E4E65AA851553A96335B_H
#define SITE_TD5DE03CC5F94838106C1E4E65AA851553A96335B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.Container_Site
struct  Site_tD5DE03CC5F94838106C1E4E65AA851553A96335B  : public RuntimeObject
{
public:
	// System.ComponentModel.IComponent System.ComponentModel.Container_Site::component
	RuntimeObject* ___component_0;
	// System.ComponentModel.Container System.ComponentModel.Container_Site::container
	Container_tC98CD69632DEA16FC061454A770CC7D11FFFEEF1 * ___container_1;
	// System.String System.ComponentModel.Container_Site::name
	String_t* ___name_2;

public:
	inline static int32_t get_offset_of_component_0() { return static_cast<int32_t>(offsetof(Site_tD5DE03CC5F94838106C1E4E65AA851553A96335B, ___component_0)); }
	inline RuntimeObject* get_component_0() const { return ___component_0; }
	inline RuntimeObject** get_address_of_component_0() { return &___component_0; }
	inline void set_component_0(RuntimeObject* value)
	{
		___component_0 = value;
		Il2CppCodeGenWriteBarrier((&___component_0), value);
	}

	inline static int32_t get_offset_of_container_1() { return static_cast<int32_t>(offsetof(Site_tD5DE03CC5F94838106C1E4E65AA851553A96335B, ___container_1)); }
	inline Container_tC98CD69632DEA16FC061454A770CC7D11FFFEEF1 * get_container_1() const { return ___container_1; }
	inline Container_tC98CD69632DEA16FC061454A770CC7D11FFFEEF1 ** get_address_of_container_1() { return &___container_1; }
	inline void set_container_1(Container_tC98CD69632DEA16FC061454A770CC7D11FFFEEF1 * value)
	{
		___container_1 = value;
		Il2CppCodeGenWriteBarrier((&___container_1), value);
	}

	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(Site_tD5DE03CC5F94838106C1E4E65AA851553A96335B, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier((&___name_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SITE_TD5DE03CC5F94838106C1E4E65AA851553A96335B_H
#ifndef CONTAINERFILTERSERVICE_T4E2FD208B28F83EA50EBC66500B7DCA29D38FA19_H
#define CONTAINERFILTERSERVICE_T4E2FD208B28F83EA50EBC66500B7DCA29D38FA19_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.ContainerFilterService
struct  ContainerFilterService_t4E2FD208B28F83EA50EBC66500B7DCA29D38FA19  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTAINERFILTERSERVICE_T4E2FD208B28F83EA50EBC66500B7DCA29D38FA19_H
#ifndef CULTURECOMPARER_T19BC07A3DF37B4602E0AA7FA4B92795FE47A409E_H
#define CULTURECOMPARER_T19BC07A3DF37B4602E0AA7FA4B92795FE47A409E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.CultureInfoConverter_CultureComparer
struct  CultureComparer_t19BC07A3DF37B4602E0AA7FA4B92795FE47A409E  : public RuntimeObject
{
public:
	// System.ComponentModel.CultureInfoConverter System.ComponentModel.CultureInfoConverter_CultureComparer::converter
	CultureInfoConverter_t6BD4316CF7C9163A01C35EC9382BC3D1C01FB6C0 * ___converter_0;

public:
	inline static int32_t get_offset_of_converter_0() { return static_cast<int32_t>(offsetof(CultureComparer_t19BC07A3DF37B4602E0AA7FA4B92795FE47A409E, ___converter_0)); }
	inline CultureInfoConverter_t6BD4316CF7C9163A01C35EC9382BC3D1C01FB6C0 * get_converter_0() const { return ___converter_0; }
	inline CultureInfoConverter_t6BD4316CF7C9163A01C35EC9382BC3D1C01FB6C0 ** get_address_of_converter_0() { return &___converter_0; }
	inline void set_converter_0(CultureInfoConverter_t6BD4316CF7C9163A01C35EC9382BC3D1C01FB6C0 * value)
	{
		___converter_0 = value;
		Il2CppCodeGenWriteBarrier((&___converter_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CULTURECOMPARER_T19BC07A3DF37B4602E0AA7FA4B92795FE47A409E_H
#ifndef CULTUREINFOMAPPER_TB7486496AE7B37C5908168F2272AED9DFB0C1A94_H
#define CULTUREINFOMAPPER_TB7486496AE7B37C5908168F2272AED9DFB0C1A94_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.CultureInfoConverter_CultureInfoMapper
struct  CultureInfoMapper_tB7486496AE7B37C5908168F2272AED9DFB0C1A94  : public RuntimeObject
{
public:

public:
};

struct CultureInfoMapper_tB7486496AE7B37C5908168F2272AED9DFB0C1A94_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.String> modreq(System.Runtime.CompilerServices.IsVolatile) System.ComponentModel.CultureInfoConverter_CultureInfoMapper::cultureInfoNameMap
	Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * ___cultureInfoNameMap_0;

public:
	inline static int32_t get_offset_of_cultureInfoNameMap_0() { return static_cast<int32_t>(offsetof(CultureInfoMapper_tB7486496AE7B37C5908168F2272AED9DFB0C1A94_StaticFields, ___cultureInfoNameMap_0)); }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * get_cultureInfoNameMap_0() const { return ___cultureInfoNameMap_0; }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC ** get_address_of_cultureInfoNameMap_0() { return &___cultureInfoNameMap_0; }
	inline void set_cultureInfoNameMap_0(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * value)
	{
		___cultureInfoNameMap_0 = value;
		Il2CppCodeGenWriteBarrier((&___cultureInfoNameMap_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CULTUREINFOMAPPER_TB7486496AE7B37C5908168F2272AED9DFB0C1A94_H
#ifndef CUSTOMTYPEDESCRIPTOR_TF8665CD45DFFA622F7EB328A2F77067DD2147689_H
#define CUSTOMTYPEDESCRIPTOR_TF8665CD45DFFA622F7EB328A2F77067DD2147689_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.CustomTypeDescriptor
struct  CustomTypeDescriptor_tF8665CD45DFFA622F7EB328A2F77067DD2147689  : public RuntimeObject
{
public:
	// System.ComponentModel.ICustomTypeDescriptor System.ComponentModel.CustomTypeDescriptor::_parent
	RuntimeObject* ____parent_0;

public:
	inline static int32_t get_offset_of__parent_0() { return static_cast<int32_t>(offsetof(CustomTypeDescriptor_tF8665CD45DFFA622F7EB328A2F77067DD2147689, ____parent_0)); }
	inline RuntimeObject* get__parent_0() const { return ____parent_0; }
	inline RuntimeObject** get_address_of__parent_0() { return &____parent_0; }
	inline void set__parent_0(RuntimeObject* value)
	{
		____parent_0 = value;
		Il2CppCodeGenWriteBarrier((&____parent_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMTYPEDESCRIPTOR_TF8665CD45DFFA622F7EB328A2F77067DD2147689_H
#ifndef MEMBERDESCRIPTOR_T9540F11CAE19431295C2582699585264E053B8F8_H
#define MEMBERDESCRIPTOR_T9540F11CAE19431295C2582699585264E053B8F8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.MemberDescriptor
struct  MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8  : public RuntimeObject
{
public:
	// System.String System.ComponentModel.MemberDescriptor::name
	String_t* ___name_0;
	// System.String System.ComponentModel.MemberDescriptor::displayName
	String_t* ___displayName_1;
	// System.Int32 System.ComponentModel.MemberDescriptor::nameHash
	int32_t ___nameHash_2;
	// System.ComponentModel.AttributeCollection System.ComponentModel.MemberDescriptor::attributeCollection
	AttributeCollection_tBE6941BB802EDE34B7F986C14A7D7E3A4E135EBE * ___attributeCollection_3;
	// System.Attribute[] System.ComponentModel.MemberDescriptor::attributes
	AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17* ___attributes_4;
	// System.Attribute[] System.ComponentModel.MemberDescriptor::originalAttributes
	AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17* ___originalAttributes_5;
	// System.Boolean System.ComponentModel.MemberDescriptor::attributesFiltered
	bool ___attributesFiltered_6;
	// System.Boolean System.ComponentModel.MemberDescriptor::attributesFilled
	bool ___attributesFilled_7;
	// System.Int32 System.ComponentModel.MemberDescriptor::metadataVersion
	int32_t ___metadataVersion_8;
	// System.String System.ComponentModel.MemberDescriptor::category
	String_t* ___category_9;
	// System.String System.ComponentModel.MemberDescriptor::description
	String_t* ___description_10;
	// System.Object System.ComponentModel.MemberDescriptor::lockCookie
	RuntimeObject * ___lockCookie_11;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_displayName_1() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___displayName_1)); }
	inline String_t* get_displayName_1() const { return ___displayName_1; }
	inline String_t** get_address_of_displayName_1() { return &___displayName_1; }
	inline void set_displayName_1(String_t* value)
	{
		___displayName_1 = value;
		Il2CppCodeGenWriteBarrier((&___displayName_1), value);
	}

	inline static int32_t get_offset_of_nameHash_2() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___nameHash_2)); }
	inline int32_t get_nameHash_2() const { return ___nameHash_2; }
	inline int32_t* get_address_of_nameHash_2() { return &___nameHash_2; }
	inline void set_nameHash_2(int32_t value)
	{
		___nameHash_2 = value;
	}

	inline static int32_t get_offset_of_attributeCollection_3() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___attributeCollection_3)); }
	inline AttributeCollection_tBE6941BB802EDE34B7F986C14A7D7E3A4E135EBE * get_attributeCollection_3() const { return ___attributeCollection_3; }
	inline AttributeCollection_tBE6941BB802EDE34B7F986C14A7D7E3A4E135EBE ** get_address_of_attributeCollection_3() { return &___attributeCollection_3; }
	inline void set_attributeCollection_3(AttributeCollection_tBE6941BB802EDE34B7F986C14A7D7E3A4E135EBE * value)
	{
		___attributeCollection_3 = value;
		Il2CppCodeGenWriteBarrier((&___attributeCollection_3), value);
	}

	inline static int32_t get_offset_of_attributes_4() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___attributes_4)); }
	inline AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17* get_attributes_4() const { return ___attributes_4; }
	inline AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17** get_address_of_attributes_4() { return &___attributes_4; }
	inline void set_attributes_4(AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17* value)
	{
		___attributes_4 = value;
		Il2CppCodeGenWriteBarrier((&___attributes_4), value);
	}

	inline static int32_t get_offset_of_originalAttributes_5() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___originalAttributes_5)); }
	inline AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17* get_originalAttributes_5() const { return ___originalAttributes_5; }
	inline AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17** get_address_of_originalAttributes_5() { return &___originalAttributes_5; }
	inline void set_originalAttributes_5(AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17* value)
	{
		___originalAttributes_5 = value;
		Il2CppCodeGenWriteBarrier((&___originalAttributes_5), value);
	}

	inline static int32_t get_offset_of_attributesFiltered_6() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___attributesFiltered_6)); }
	inline bool get_attributesFiltered_6() const { return ___attributesFiltered_6; }
	inline bool* get_address_of_attributesFiltered_6() { return &___attributesFiltered_6; }
	inline void set_attributesFiltered_6(bool value)
	{
		___attributesFiltered_6 = value;
	}

	inline static int32_t get_offset_of_attributesFilled_7() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___attributesFilled_7)); }
	inline bool get_attributesFilled_7() const { return ___attributesFilled_7; }
	inline bool* get_address_of_attributesFilled_7() { return &___attributesFilled_7; }
	inline void set_attributesFilled_7(bool value)
	{
		___attributesFilled_7 = value;
	}

	inline static int32_t get_offset_of_metadataVersion_8() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___metadataVersion_8)); }
	inline int32_t get_metadataVersion_8() const { return ___metadataVersion_8; }
	inline int32_t* get_address_of_metadataVersion_8() { return &___metadataVersion_8; }
	inline void set_metadataVersion_8(int32_t value)
	{
		___metadataVersion_8 = value;
	}

	inline static int32_t get_offset_of_category_9() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___category_9)); }
	inline String_t* get_category_9() const { return ___category_9; }
	inline String_t** get_address_of_category_9() { return &___category_9; }
	inline void set_category_9(String_t* value)
	{
		___category_9 = value;
		Il2CppCodeGenWriteBarrier((&___category_9), value);
	}

	inline static int32_t get_offset_of_description_10() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___description_10)); }
	inline String_t* get_description_10() const { return ___description_10; }
	inline String_t** get_address_of_description_10() { return &___description_10; }
	inline void set_description_10(String_t* value)
	{
		___description_10 = value;
		Il2CppCodeGenWriteBarrier((&___description_10), value);
	}

	inline static int32_t get_offset_of_lockCookie_11() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___lockCookie_11)); }
	inline RuntimeObject * get_lockCookie_11() const { return ___lockCookie_11; }
	inline RuntimeObject ** get_address_of_lockCookie_11() { return &___lockCookie_11; }
	inline void set_lockCookie_11(RuntimeObject * value)
	{
		___lockCookie_11 = value;
		Il2CppCodeGenWriteBarrier((&___lockCookie_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERDESCRIPTOR_T9540F11CAE19431295C2582699585264E053B8F8_H
#ifndef TYPEDESCRIPTIONPROVIDER_TE390829A953C44525366CA2A733E92642B97B591_H
#define TYPEDESCRIPTIONPROVIDER_TE390829A953C44525366CA2A733E92642B97B591_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.TypeDescriptionProvider
struct  TypeDescriptionProvider_tE390829A953C44525366CA2A733E92642B97B591  : public RuntimeObject
{
public:
	// System.ComponentModel.TypeDescriptionProvider System.ComponentModel.TypeDescriptionProvider::_parent
	TypeDescriptionProvider_tE390829A953C44525366CA2A733E92642B97B591 * ____parent_0;
	// System.ComponentModel.TypeDescriptionProvider_EmptyCustomTypeDescriptor System.ComponentModel.TypeDescriptionProvider::_emptyDescriptor
	EmptyCustomTypeDescriptor_tDDCC5072613D7D0405BCCF3D49F3EF7B4E1E7DB8 * ____emptyDescriptor_1;

public:
	inline static int32_t get_offset_of__parent_0() { return static_cast<int32_t>(offsetof(TypeDescriptionProvider_tE390829A953C44525366CA2A733E92642B97B591, ____parent_0)); }
	inline TypeDescriptionProvider_tE390829A953C44525366CA2A733E92642B97B591 * get__parent_0() const { return ____parent_0; }
	inline TypeDescriptionProvider_tE390829A953C44525366CA2A733E92642B97B591 ** get_address_of__parent_0() { return &____parent_0; }
	inline void set__parent_0(TypeDescriptionProvider_tE390829A953C44525366CA2A733E92642B97B591 * value)
	{
		____parent_0 = value;
		Il2CppCodeGenWriteBarrier((&____parent_0), value);
	}

	inline static int32_t get_offset_of__emptyDescriptor_1() { return static_cast<int32_t>(offsetof(TypeDescriptionProvider_tE390829A953C44525366CA2A733E92642B97B591, ____emptyDescriptor_1)); }
	inline EmptyCustomTypeDescriptor_tDDCC5072613D7D0405BCCF3D49F3EF7B4E1E7DB8 * get__emptyDescriptor_1() const { return ____emptyDescriptor_1; }
	inline EmptyCustomTypeDescriptor_tDDCC5072613D7D0405BCCF3D49F3EF7B4E1E7DB8 ** get_address_of__emptyDescriptor_1() { return &____emptyDescriptor_1; }
	inline void set__emptyDescriptor_1(EmptyCustomTypeDescriptor_tDDCC5072613D7D0405BCCF3D49F3EF7B4E1E7DB8 * value)
	{
		____emptyDescriptor_1 = value;
		Il2CppCodeGenWriteBarrier((&____emptyDescriptor_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEDESCRIPTIONPROVIDER_TE390829A953C44525366CA2A733E92642B97B591_H
#ifndef ASYNCSTREAMREADER_T2C28E845971B756383AF73AEF2A86C7545E5C485_H
#define ASYNCSTREAMREADER_T2C28E845971B756383AF73AEF2A86C7545E5C485_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.AsyncStreamReader
struct  AsyncStreamReader_t2C28E845971B756383AF73AEF2A86C7545E5C485  : public RuntimeObject
{
public:
	// System.IO.Stream System.Diagnostics.AsyncStreamReader::stream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___stream_0;
	// System.Text.Encoding System.Diagnostics.AsyncStreamReader::encoding
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___encoding_1;
	// System.Text.Decoder System.Diagnostics.AsyncStreamReader::decoder
	Decoder_tEEF45EB6F965222036C49E8EC6BA8A0692AA1F26 * ___decoder_2;
	// System.Byte[] System.Diagnostics.AsyncStreamReader::byteBuffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___byteBuffer_3;
	// System.Char[] System.Diagnostics.AsyncStreamReader::charBuffer
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___charBuffer_4;
	// System.Boolean System.Diagnostics.AsyncStreamReader::cancelOperation
	bool ___cancelOperation_5;
	// System.Threading.ManualResetEvent System.Diagnostics.AsyncStreamReader::eofEvent
	ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * ___eofEvent_6;
	// System.Object System.Diagnostics.AsyncStreamReader::syncObject
	RuntimeObject * ___syncObject_7;

public:
	inline static int32_t get_offset_of_stream_0() { return static_cast<int32_t>(offsetof(AsyncStreamReader_t2C28E845971B756383AF73AEF2A86C7545E5C485, ___stream_0)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_stream_0() const { return ___stream_0; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_stream_0() { return &___stream_0; }
	inline void set_stream_0(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___stream_0 = value;
		Il2CppCodeGenWriteBarrier((&___stream_0), value);
	}

	inline static int32_t get_offset_of_encoding_1() { return static_cast<int32_t>(offsetof(AsyncStreamReader_t2C28E845971B756383AF73AEF2A86C7545E5C485, ___encoding_1)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get_encoding_1() const { return ___encoding_1; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of_encoding_1() { return &___encoding_1; }
	inline void set_encoding_1(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		___encoding_1 = value;
		Il2CppCodeGenWriteBarrier((&___encoding_1), value);
	}

	inline static int32_t get_offset_of_decoder_2() { return static_cast<int32_t>(offsetof(AsyncStreamReader_t2C28E845971B756383AF73AEF2A86C7545E5C485, ___decoder_2)); }
	inline Decoder_tEEF45EB6F965222036C49E8EC6BA8A0692AA1F26 * get_decoder_2() const { return ___decoder_2; }
	inline Decoder_tEEF45EB6F965222036C49E8EC6BA8A0692AA1F26 ** get_address_of_decoder_2() { return &___decoder_2; }
	inline void set_decoder_2(Decoder_tEEF45EB6F965222036C49E8EC6BA8A0692AA1F26 * value)
	{
		___decoder_2 = value;
		Il2CppCodeGenWriteBarrier((&___decoder_2), value);
	}

	inline static int32_t get_offset_of_byteBuffer_3() { return static_cast<int32_t>(offsetof(AsyncStreamReader_t2C28E845971B756383AF73AEF2A86C7545E5C485, ___byteBuffer_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_byteBuffer_3() const { return ___byteBuffer_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_byteBuffer_3() { return &___byteBuffer_3; }
	inline void set_byteBuffer_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___byteBuffer_3 = value;
		Il2CppCodeGenWriteBarrier((&___byteBuffer_3), value);
	}

	inline static int32_t get_offset_of_charBuffer_4() { return static_cast<int32_t>(offsetof(AsyncStreamReader_t2C28E845971B756383AF73AEF2A86C7545E5C485, ___charBuffer_4)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_charBuffer_4() const { return ___charBuffer_4; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_charBuffer_4() { return &___charBuffer_4; }
	inline void set_charBuffer_4(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___charBuffer_4 = value;
		Il2CppCodeGenWriteBarrier((&___charBuffer_4), value);
	}

	inline static int32_t get_offset_of_cancelOperation_5() { return static_cast<int32_t>(offsetof(AsyncStreamReader_t2C28E845971B756383AF73AEF2A86C7545E5C485, ___cancelOperation_5)); }
	inline bool get_cancelOperation_5() const { return ___cancelOperation_5; }
	inline bool* get_address_of_cancelOperation_5() { return &___cancelOperation_5; }
	inline void set_cancelOperation_5(bool value)
	{
		___cancelOperation_5 = value;
	}

	inline static int32_t get_offset_of_eofEvent_6() { return static_cast<int32_t>(offsetof(AsyncStreamReader_t2C28E845971B756383AF73AEF2A86C7545E5C485, ___eofEvent_6)); }
	inline ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * get_eofEvent_6() const { return ___eofEvent_6; }
	inline ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 ** get_address_of_eofEvent_6() { return &___eofEvent_6; }
	inline void set_eofEvent_6(ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * value)
	{
		___eofEvent_6 = value;
		Il2CppCodeGenWriteBarrier((&___eofEvent_6), value);
	}

	inline static int32_t get_offset_of_syncObject_7() { return static_cast<int32_t>(offsetof(AsyncStreamReader_t2C28E845971B756383AF73AEF2A86C7545E5C485, ___syncObject_7)); }
	inline RuntimeObject * get_syncObject_7() const { return ___syncObject_7; }
	inline RuntimeObject ** get_address_of_syncObject_7() { return &___syncObject_7; }
	inline void set_syncObject_7(RuntimeObject * value)
	{
		___syncObject_7 = value;
		Il2CppCodeGenWriteBarrier((&___syncObject_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCSTREAMREADER_T2C28E845971B756383AF73AEF2A86C7545E5C485_H
#ifndef CORRELATIONMANAGER_T791B7FE5E821DC5746198251B11EDB293CB60120_H
#define CORRELATIONMANAGER_T791B7FE5E821DC5746198251B11EDB293CB60120_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.CorrelationManager
struct  CorrelationManager_t791B7FE5E821DC5746198251B11EDB293CB60120  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CORRELATIONMANAGER_T791B7FE5E821DC5746198251B11EDB293CB60120_H
#ifndef PROCESSINFO_T2A1AA6533946D6FDFBCA83CDBE0C62089F2EDEF7_H
#define PROCESSINFO_T2A1AA6533946D6FDFBCA83CDBE0C62089F2EDEF7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.ProcessInfo
struct  ProcessInfo_t2A1AA6533946D6FDFBCA83CDBE0C62089F2EDEF7  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROCESSINFO_T2A1AA6533946D6FDFBCA83CDBE0C62089F2EDEF7_H
#ifndef PROCESSTHREADTIMES_TCFD8DB0EA1A7E180E7C1FC125AF94546AD650468_H
#define PROCESSTHREADTIMES_TCFD8DB0EA1A7E180E7C1FC125AF94546AD650468_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.ProcessThreadTimes
struct  ProcessThreadTimes_tCFD8DB0EA1A7E180E7C1FC125AF94546AD650468  : public RuntimeObject
{
public:
	// System.Int64 System.Diagnostics.ProcessThreadTimes::create
	int64_t ___create_0;
	// System.Int64 System.Diagnostics.ProcessThreadTimes::exit
	int64_t ___exit_1;
	// System.Int64 System.Diagnostics.ProcessThreadTimes::kernel
	int64_t ___kernel_2;
	// System.Int64 System.Diagnostics.ProcessThreadTimes::user
	int64_t ___user_3;

public:
	inline static int32_t get_offset_of_create_0() { return static_cast<int32_t>(offsetof(ProcessThreadTimes_tCFD8DB0EA1A7E180E7C1FC125AF94546AD650468, ___create_0)); }
	inline int64_t get_create_0() const { return ___create_0; }
	inline int64_t* get_address_of_create_0() { return &___create_0; }
	inline void set_create_0(int64_t value)
	{
		___create_0 = value;
	}

	inline static int32_t get_offset_of_exit_1() { return static_cast<int32_t>(offsetof(ProcessThreadTimes_tCFD8DB0EA1A7E180E7C1FC125AF94546AD650468, ___exit_1)); }
	inline int64_t get_exit_1() const { return ___exit_1; }
	inline int64_t* get_address_of_exit_1() { return &___exit_1; }
	inline void set_exit_1(int64_t value)
	{
		___exit_1 = value;
	}

	inline static int32_t get_offset_of_kernel_2() { return static_cast<int32_t>(offsetof(ProcessThreadTimes_tCFD8DB0EA1A7E180E7C1FC125AF94546AD650468, ___kernel_2)); }
	inline int64_t get_kernel_2() const { return ___kernel_2; }
	inline int64_t* get_address_of_kernel_2() { return &___kernel_2; }
	inline void set_kernel_2(int64_t value)
	{
		___kernel_2 = value;
	}

	inline static int32_t get_offset_of_user_3() { return static_cast<int32_t>(offsetof(ProcessThreadTimes_tCFD8DB0EA1A7E180E7C1FC125AF94546AD650468, ___user_3)); }
	inline int64_t get_user_3() const { return ___user_3; }
	inline int64_t* get_address_of_user_3() { return &___user_3; }
	inline void set_user_3(int64_t value)
	{
		___user_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROCESSTHREADTIMES_TCFD8DB0EA1A7E180E7C1FC125AF94546AD650468_H
#ifndef STOPWATCH_T0778B5C8DF8FE1D87FC57A2411DA695850BD64D4_H
#define STOPWATCH_T0778B5C8DF8FE1D87FC57A2411DA695850BD64D4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.Stopwatch
struct  Stopwatch_t0778B5C8DF8FE1D87FC57A2411DA695850BD64D4  : public RuntimeObject
{
public:
	// System.Int64 System.Diagnostics.Stopwatch::elapsed
	int64_t ___elapsed_2;
	// System.Int64 System.Diagnostics.Stopwatch::started
	int64_t ___started_3;
	// System.Boolean System.Diagnostics.Stopwatch::is_running
	bool ___is_running_4;

public:
	inline static int32_t get_offset_of_elapsed_2() { return static_cast<int32_t>(offsetof(Stopwatch_t0778B5C8DF8FE1D87FC57A2411DA695850BD64D4, ___elapsed_2)); }
	inline int64_t get_elapsed_2() const { return ___elapsed_2; }
	inline int64_t* get_address_of_elapsed_2() { return &___elapsed_2; }
	inline void set_elapsed_2(int64_t value)
	{
		___elapsed_2 = value;
	}

	inline static int32_t get_offset_of_started_3() { return static_cast<int32_t>(offsetof(Stopwatch_t0778B5C8DF8FE1D87FC57A2411DA695850BD64D4, ___started_3)); }
	inline int64_t get_started_3() const { return ___started_3; }
	inline int64_t* get_address_of_started_3() { return &___started_3; }
	inline void set_started_3(int64_t value)
	{
		___started_3 = value;
	}

	inline static int32_t get_offset_of_is_running_4() { return static_cast<int32_t>(offsetof(Stopwatch_t0778B5C8DF8FE1D87FC57A2411DA695850BD64D4, ___is_running_4)); }
	inline bool get_is_running_4() const { return ___is_running_4; }
	inline bool* get_address_of_is_running_4() { return &___is_running_4; }
	inline void set_is_running_4(bool value)
	{
		___is_running_4 = value;
	}
};

struct Stopwatch_t0778B5C8DF8FE1D87FC57A2411DA695850BD64D4_StaticFields
{
public:
	// System.Int64 System.Diagnostics.Stopwatch::Frequency
	int64_t ___Frequency_0;
	// System.Boolean System.Diagnostics.Stopwatch::IsHighResolution
	bool ___IsHighResolution_1;

public:
	inline static int32_t get_offset_of_Frequency_0() { return static_cast<int32_t>(offsetof(Stopwatch_t0778B5C8DF8FE1D87FC57A2411DA695850BD64D4_StaticFields, ___Frequency_0)); }
	inline int64_t get_Frequency_0() const { return ___Frequency_0; }
	inline int64_t* get_address_of_Frequency_0() { return &___Frequency_0; }
	inline void set_Frequency_0(int64_t value)
	{
		___Frequency_0 = value;
	}

	inline static int32_t get_offset_of_IsHighResolution_1() { return static_cast<int32_t>(offsetof(Stopwatch_t0778B5C8DF8FE1D87FC57A2411DA695850BD64D4_StaticFields, ___IsHighResolution_1)); }
	inline bool get_IsHighResolution_1() const { return ___IsHighResolution_1; }
	inline bool* get_address_of_IsHighResolution_1() { return &___IsHighResolution_1; }
	inline void set_IsHighResolution_1(bool value)
	{
		___IsHighResolution_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STOPWATCH_T0778B5C8DF8FE1D87FC57A2411DA695850BD64D4_H
#ifndef SWITCH_T9DAA0B271A8AAEC0A2EFE1EE30299CECB4C2002F_H
#define SWITCH_T9DAA0B271A8AAEC0A2EFE1EE30299CECB4C2002F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.Switch
struct  Switch_t9DAA0B271A8AAEC0A2EFE1EE30299CECB4C2002F  : public RuntimeObject
{
public:
	// System.String System.Diagnostics.Switch::description
	String_t* ___description_0;
	// System.String System.Diagnostics.Switch::displayName
	String_t* ___displayName_1;
	// System.String modreq(System.Runtime.CompilerServices.IsVolatile) System.Diagnostics.Switch::switchValueString
	String_t* ___switchValueString_2;
	// System.String System.Diagnostics.Switch::defaultValue
	String_t* ___defaultValue_3;

public:
	inline static int32_t get_offset_of_description_0() { return static_cast<int32_t>(offsetof(Switch_t9DAA0B271A8AAEC0A2EFE1EE30299CECB4C2002F, ___description_0)); }
	inline String_t* get_description_0() const { return ___description_0; }
	inline String_t** get_address_of_description_0() { return &___description_0; }
	inline void set_description_0(String_t* value)
	{
		___description_0 = value;
		Il2CppCodeGenWriteBarrier((&___description_0), value);
	}

	inline static int32_t get_offset_of_displayName_1() { return static_cast<int32_t>(offsetof(Switch_t9DAA0B271A8AAEC0A2EFE1EE30299CECB4C2002F, ___displayName_1)); }
	inline String_t* get_displayName_1() const { return ___displayName_1; }
	inline String_t** get_address_of_displayName_1() { return &___displayName_1; }
	inline void set_displayName_1(String_t* value)
	{
		___displayName_1 = value;
		Il2CppCodeGenWriteBarrier((&___displayName_1), value);
	}

	inline static int32_t get_offset_of_switchValueString_2() { return static_cast<int32_t>(offsetof(Switch_t9DAA0B271A8AAEC0A2EFE1EE30299CECB4C2002F, ___switchValueString_2)); }
	inline String_t* get_switchValueString_2() const { return ___switchValueString_2; }
	inline String_t** get_address_of_switchValueString_2() { return &___switchValueString_2; }
	inline void set_switchValueString_2(String_t* value)
	{
		___switchValueString_2 = value;
		Il2CppCodeGenWriteBarrier((&___switchValueString_2), value);
	}

	inline static int32_t get_offset_of_defaultValue_3() { return static_cast<int32_t>(offsetof(Switch_t9DAA0B271A8AAEC0A2EFE1EE30299CECB4C2002F, ___defaultValue_3)); }
	inline String_t* get_defaultValue_3() const { return ___defaultValue_3; }
	inline String_t** get_address_of_defaultValue_3() { return &___defaultValue_3; }
	inline void set_defaultValue_3(String_t* value)
	{
		___defaultValue_3 = value;
		Il2CppCodeGenWriteBarrier((&___defaultValue_3), value);
	}
};

struct Switch_t9DAA0B271A8AAEC0A2EFE1EE30299CECB4C2002F_StaticFields
{
public:
	// System.Collections.Generic.List`1<System.WeakReference> System.Diagnostics.Switch::switches
	List_1_t0B19BE4139518EFD1F11815FD931281B09EA15EF * ___switches_4;
	// System.Int32 System.Diagnostics.Switch::s_LastCollectionCount
	int32_t ___s_LastCollectionCount_5;

public:
	inline static int32_t get_offset_of_switches_4() { return static_cast<int32_t>(offsetof(Switch_t9DAA0B271A8AAEC0A2EFE1EE30299CECB4C2002F_StaticFields, ___switches_4)); }
	inline List_1_t0B19BE4139518EFD1F11815FD931281B09EA15EF * get_switches_4() const { return ___switches_4; }
	inline List_1_t0B19BE4139518EFD1F11815FD931281B09EA15EF ** get_address_of_switches_4() { return &___switches_4; }
	inline void set_switches_4(List_1_t0B19BE4139518EFD1F11815FD931281B09EA15EF * value)
	{
		___switches_4 = value;
		Il2CppCodeGenWriteBarrier((&___switches_4), value);
	}

	inline static int32_t get_offset_of_s_LastCollectionCount_5() { return static_cast<int32_t>(offsetof(Switch_t9DAA0B271A8AAEC0A2EFE1EE30299CECB4C2002F_StaticFields, ___s_LastCollectionCount_5)); }
	inline int32_t get_s_LastCollectionCount_5() const { return ___s_LastCollectionCount_5; }
	inline int32_t* get_address_of_s_LastCollectionCount_5() { return &___s_LastCollectionCount_5; }
	inline void set_s_LastCollectionCount_5(int32_t value)
	{
		___s_LastCollectionCount_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWITCH_T9DAA0B271A8AAEC0A2EFE1EE30299CECB4C2002F_H
#ifndef TRACE_T8A039E0C52E7CC7C3A4622E704FAA75479F12EEF_H
#define TRACE_T8A039E0C52E7CC7C3A4622E704FAA75479F12EEF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.Trace
struct  Trace_t8A039E0C52E7CC7C3A4622E704FAA75479F12EEF  : public RuntimeObject
{
public:

public:
};

struct Trace_t8A039E0C52E7CC7C3A4622E704FAA75479F12EEF_StaticFields
{
public:
	// System.Diagnostics.CorrelationManager modreq(System.Runtime.CompilerServices.IsVolatile) System.Diagnostics.Trace::correlationManager
	CorrelationManager_t791B7FE5E821DC5746198251B11EDB293CB60120 * ___correlationManager_0;

public:
	inline static int32_t get_offset_of_correlationManager_0() { return static_cast<int32_t>(offsetof(Trace_t8A039E0C52E7CC7C3A4622E704FAA75479F12EEF_StaticFields, ___correlationManager_0)); }
	inline CorrelationManager_t791B7FE5E821DC5746198251B11EDB293CB60120 * get_correlationManager_0() const { return ___correlationManager_0; }
	inline CorrelationManager_t791B7FE5E821DC5746198251B11EDB293CB60120 ** get_address_of_correlationManager_0() { return &___correlationManager_0; }
	inline void set_correlationManager_0(CorrelationManager_t791B7FE5E821DC5746198251B11EDB293CB60120 * value)
	{
		___correlationManager_0 = value;
		Il2CppCodeGenWriteBarrier((&___correlationManager_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACE_T8A039E0C52E7CC7C3A4622E704FAA75479F12EEF_H
#ifndef TRACEFILTER_T5BA76D3899B80AEA894D4040364099DC7C47C6F1_H
#define TRACEFILTER_T5BA76D3899B80AEA894D4040364099DC7C47C6F1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.TraceFilter
struct  TraceFilter_t5BA76D3899B80AEA894D4040364099DC7C47C6F1  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACEFILTER_T5BA76D3899B80AEA894D4040364099DC7C47C6F1_H
#ifndef TRACELISTENERCOLLECTION_T392DC090EA67B680F4818E494FB5FE85AA82FBD6_H
#define TRACELISTENERCOLLECTION_T392DC090EA67B680F4818E494FB5FE85AA82FBD6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.TraceListenerCollection
struct  TraceListenerCollection_t392DC090EA67B680F4818E494FB5FE85AA82FBD6  : public RuntimeObject
{
public:
	// System.Collections.ArrayList System.Diagnostics.TraceListenerCollection::list
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ___list_0;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(TraceListenerCollection_t392DC090EA67B680F4818E494FB5FE85AA82FBD6, ___list_0)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get_list_0() const { return ___list_0; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACELISTENERCOLLECTION_T392DC090EA67B680F4818E494FB5FE85AA82FBD6_H
#ifndef EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#define EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventArgs
struct  EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E  : public RuntimeObject
{
public:

public:
};

struct EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields, ___Empty_0)); }
	inline EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#define MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MarshalByRefObject
struct  MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF  : public RuntimeObject
{
public:
	// System.Object System.MarshalByRefObject::_identity
	RuntimeObject * ____identity_0;

public:
	inline static int32_t get_offset_of__identity_0() { return static_cast<int32_t>(offsetof(MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF, ____identity_0)); }
	inline RuntimeObject * get__identity_0() const { return ____identity_0; }
	inline RuntimeObject ** get_address_of__identity_0() { return &____identity_0; }
	inline void set__identity_0(RuntimeObject * value)
	{
		____identity_0 = value;
		Il2CppCodeGenWriteBarrier((&____identity_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF_marshaled_pinvoke
{
	Il2CppIUnknown* ____identity_0;
};
// Native definition for COM marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF_marshaled_com
{
	Il2CppIUnknown* ____identity_0;
};
#endif // MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#ifndef CAPTURE_TF4475248CCF3EFF914844BE2C993FC609D41DB73_H
#define CAPTURE_TF4475248CCF3EFF914844BE2C993FC609D41DB73_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Capture
struct  Capture_tF4475248CCF3EFF914844BE2C993FC609D41DB73  : public RuntimeObject
{
public:
	// System.String System.Text.RegularExpressions.Capture::_text
	String_t* ____text_0;
	// System.Int32 System.Text.RegularExpressions.Capture::_index
	int32_t ____index_1;
	// System.Int32 System.Text.RegularExpressions.Capture::_length
	int32_t ____length_2;

public:
	inline static int32_t get_offset_of__text_0() { return static_cast<int32_t>(offsetof(Capture_tF4475248CCF3EFF914844BE2C993FC609D41DB73, ____text_0)); }
	inline String_t* get__text_0() const { return ____text_0; }
	inline String_t** get_address_of__text_0() { return &____text_0; }
	inline void set__text_0(String_t* value)
	{
		____text_0 = value;
		Il2CppCodeGenWriteBarrier((&____text_0), value);
	}

	inline static int32_t get_offset_of__index_1() { return static_cast<int32_t>(offsetof(Capture_tF4475248CCF3EFF914844BE2C993FC609D41DB73, ____index_1)); }
	inline int32_t get__index_1() const { return ____index_1; }
	inline int32_t* get_address_of__index_1() { return &____index_1; }
	inline void set__index_1(int32_t value)
	{
		____index_1 = value;
	}

	inline static int32_t get_offset_of__length_2() { return static_cast<int32_t>(offsetof(Capture_tF4475248CCF3EFF914844BE2C993FC609D41DB73, ____length_2)); }
	inline int32_t get__length_2() const { return ____length_2; }
	inline int32_t* get_address_of__length_2() { return &____length_2; }
	inline void set__length_2(int32_t value)
	{
		____length_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAPTURE_TF4475248CCF3EFF914844BE2C993FC609D41DB73_H
#ifndef MATCHCOLLECTION_T8B954CE579727B076276B514F08040EE4D26844A_H
#define MATCHCOLLECTION_T8B954CE579727B076276B514F08040EE4D26844A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.MatchCollection
struct  MatchCollection_t8B954CE579727B076276B514F08040EE4D26844A  : public RuntimeObject
{
public:
	// System.Text.RegularExpressions.Regex System.Text.RegularExpressions.MatchCollection::_regex
	Regex_tFD46E63A462E852189FD6AB4E2B0B67C4D8FDBDF * ____regex_0;
	// System.Collections.ArrayList System.Text.RegularExpressions.MatchCollection::_matches
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ____matches_1;
	// System.Boolean System.Text.RegularExpressions.MatchCollection::_done
	bool ____done_2;
	// System.String System.Text.RegularExpressions.MatchCollection::_input
	String_t* ____input_3;
	// System.Int32 System.Text.RegularExpressions.MatchCollection::_beginning
	int32_t ____beginning_4;
	// System.Int32 System.Text.RegularExpressions.MatchCollection::_length
	int32_t ____length_5;
	// System.Int32 System.Text.RegularExpressions.MatchCollection::_startat
	int32_t ____startat_6;
	// System.Int32 System.Text.RegularExpressions.MatchCollection::_prevlen
	int32_t ____prevlen_7;

public:
	inline static int32_t get_offset_of__regex_0() { return static_cast<int32_t>(offsetof(MatchCollection_t8B954CE579727B076276B514F08040EE4D26844A, ____regex_0)); }
	inline Regex_tFD46E63A462E852189FD6AB4E2B0B67C4D8FDBDF * get__regex_0() const { return ____regex_0; }
	inline Regex_tFD46E63A462E852189FD6AB4E2B0B67C4D8FDBDF ** get_address_of__regex_0() { return &____regex_0; }
	inline void set__regex_0(Regex_tFD46E63A462E852189FD6AB4E2B0B67C4D8FDBDF * value)
	{
		____regex_0 = value;
		Il2CppCodeGenWriteBarrier((&____regex_0), value);
	}

	inline static int32_t get_offset_of__matches_1() { return static_cast<int32_t>(offsetof(MatchCollection_t8B954CE579727B076276B514F08040EE4D26844A, ____matches_1)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get__matches_1() const { return ____matches_1; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of__matches_1() { return &____matches_1; }
	inline void set__matches_1(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		____matches_1 = value;
		Il2CppCodeGenWriteBarrier((&____matches_1), value);
	}

	inline static int32_t get_offset_of__done_2() { return static_cast<int32_t>(offsetof(MatchCollection_t8B954CE579727B076276B514F08040EE4D26844A, ____done_2)); }
	inline bool get__done_2() const { return ____done_2; }
	inline bool* get_address_of__done_2() { return &____done_2; }
	inline void set__done_2(bool value)
	{
		____done_2 = value;
	}

	inline static int32_t get_offset_of__input_3() { return static_cast<int32_t>(offsetof(MatchCollection_t8B954CE579727B076276B514F08040EE4D26844A, ____input_3)); }
	inline String_t* get__input_3() const { return ____input_3; }
	inline String_t** get_address_of__input_3() { return &____input_3; }
	inline void set__input_3(String_t* value)
	{
		____input_3 = value;
		Il2CppCodeGenWriteBarrier((&____input_3), value);
	}

	inline static int32_t get_offset_of__beginning_4() { return static_cast<int32_t>(offsetof(MatchCollection_t8B954CE579727B076276B514F08040EE4D26844A, ____beginning_4)); }
	inline int32_t get__beginning_4() const { return ____beginning_4; }
	inline int32_t* get_address_of__beginning_4() { return &____beginning_4; }
	inline void set__beginning_4(int32_t value)
	{
		____beginning_4 = value;
	}

	inline static int32_t get_offset_of__length_5() { return static_cast<int32_t>(offsetof(MatchCollection_t8B954CE579727B076276B514F08040EE4D26844A, ____length_5)); }
	inline int32_t get__length_5() const { return ____length_5; }
	inline int32_t* get_address_of__length_5() { return &____length_5; }
	inline void set__length_5(int32_t value)
	{
		____length_5 = value;
	}

	inline static int32_t get_offset_of__startat_6() { return static_cast<int32_t>(offsetof(MatchCollection_t8B954CE579727B076276B514F08040EE4D26844A, ____startat_6)); }
	inline int32_t get__startat_6() const { return ____startat_6; }
	inline int32_t* get_address_of__startat_6() { return &____startat_6; }
	inline void set__startat_6(int32_t value)
	{
		____startat_6 = value;
	}

	inline static int32_t get_offset_of__prevlen_7() { return static_cast<int32_t>(offsetof(MatchCollection_t8B954CE579727B076276B514F08040EE4D26844A, ____prevlen_7)); }
	inline int32_t get__prevlen_7() const { return ____prevlen_7; }
	inline int32_t* get_address_of__prevlen_7() { return &____prevlen_7; }
	inline void set__prevlen_7(int32_t value)
	{
		____prevlen_7 = value;
	}
};

struct MatchCollection_t8B954CE579727B076276B514F08040EE4D26844A_StaticFields
{
public:
	// System.Int32 System.Text.RegularExpressions.MatchCollection::infinite
	int32_t ___infinite_8;

public:
	inline static int32_t get_offset_of_infinite_8() { return static_cast<int32_t>(offsetof(MatchCollection_t8B954CE579727B076276B514F08040EE4D26844A_StaticFields, ___infinite_8)); }
	inline int32_t get_infinite_8() const { return ___infinite_8; }
	inline int32_t* get_address_of_infinite_8() { return &___infinite_8; }
	inline void set_infinite_8(int32_t value)
	{
		___infinite_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATCHCOLLECTION_T8B954CE579727B076276B514F08040EE4D26844A_H
#ifndef MATCHENUMERATOR_T1B798B870C2F61E31782D27D183A307A0A6E6FC4_H
#define MATCHENUMERATOR_T1B798B870C2F61E31782D27D183A307A0A6E6FC4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.MatchEnumerator
struct  MatchEnumerator_t1B798B870C2F61E31782D27D183A307A0A6E6FC4  : public RuntimeObject
{
public:
	// System.Text.RegularExpressions.MatchCollection System.Text.RegularExpressions.MatchEnumerator::_matchcoll
	MatchCollection_t8B954CE579727B076276B514F08040EE4D26844A * ____matchcoll_0;
	// System.Text.RegularExpressions.Match System.Text.RegularExpressions.MatchEnumerator::_match
	Match_tE447871AB59EED3642F31EB9559D162C2977EBB5 * ____match_1;
	// System.Int32 System.Text.RegularExpressions.MatchEnumerator::_curindex
	int32_t ____curindex_2;
	// System.Boolean System.Text.RegularExpressions.MatchEnumerator::_done
	bool ____done_3;

public:
	inline static int32_t get_offset_of__matchcoll_0() { return static_cast<int32_t>(offsetof(MatchEnumerator_t1B798B870C2F61E31782D27D183A307A0A6E6FC4, ____matchcoll_0)); }
	inline MatchCollection_t8B954CE579727B076276B514F08040EE4D26844A * get__matchcoll_0() const { return ____matchcoll_0; }
	inline MatchCollection_t8B954CE579727B076276B514F08040EE4D26844A ** get_address_of__matchcoll_0() { return &____matchcoll_0; }
	inline void set__matchcoll_0(MatchCollection_t8B954CE579727B076276B514F08040EE4D26844A * value)
	{
		____matchcoll_0 = value;
		Il2CppCodeGenWriteBarrier((&____matchcoll_0), value);
	}

	inline static int32_t get_offset_of__match_1() { return static_cast<int32_t>(offsetof(MatchEnumerator_t1B798B870C2F61E31782D27D183A307A0A6E6FC4, ____match_1)); }
	inline Match_tE447871AB59EED3642F31EB9559D162C2977EBB5 * get__match_1() const { return ____match_1; }
	inline Match_tE447871AB59EED3642F31EB9559D162C2977EBB5 ** get_address_of__match_1() { return &____match_1; }
	inline void set__match_1(Match_tE447871AB59EED3642F31EB9559D162C2977EBB5 * value)
	{
		____match_1 = value;
		Il2CppCodeGenWriteBarrier((&____match_1), value);
	}

	inline static int32_t get_offset_of__curindex_2() { return static_cast<int32_t>(offsetof(MatchEnumerator_t1B798B870C2F61E31782D27D183A307A0A6E6FC4, ____curindex_2)); }
	inline int32_t get__curindex_2() const { return ____curindex_2; }
	inline int32_t* get_address_of__curindex_2() { return &____curindex_2; }
	inline void set__curindex_2(int32_t value)
	{
		____curindex_2 = value;
	}

	inline static int32_t get_offset_of__done_3() { return static_cast<int32_t>(offsetof(MatchEnumerator_t1B798B870C2F61E31782D27D183A307A0A6E6FC4, ____done_3)); }
	inline bool get__done_3() const { return ____done_3; }
	inline bool* get_address_of__done_3() { return &____done_3; }
	inline void set__done_3(bool value)
	{
		____done_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATCHENUMERATOR_T1B798B870C2F61E31782D27D183A307A0A6E6FC4_H
#ifndef REGEXREPLACEMENT_T2A1098B910D9E68191221914C5BBCBA813295359_H
#define REGEXREPLACEMENT_T2A1098B910D9E68191221914C5BBCBA813295359_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexReplacement
struct  RegexReplacement_t2A1098B910D9E68191221914C5BBCBA813295359  : public RuntimeObject
{
public:
	// System.String System.Text.RegularExpressions.RegexReplacement::_rep
	String_t* ____rep_0;
	// System.Collections.Generic.List`1<System.String> System.Text.RegularExpressions.RegexReplacement::_strings
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ____strings_1;
	// System.Collections.Generic.List`1<System.Int32> System.Text.RegularExpressions.RegexReplacement::_rules
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ____rules_2;

public:
	inline static int32_t get_offset_of__rep_0() { return static_cast<int32_t>(offsetof(RegexReplacement_t2A1098B910D9E68191221914C5BBCBA813295359, ____rep_0)); }
	inline String_t* get__rep_0() const { return ____rep_0; }
	inline String_t** get_address_of__rep_0() { return &____rep_0; }
	inline void set__rep_0(String_t* value)
	{
		____rep_0 = value;
		Il2CppCodeGenWriteBarrier((&____rep_0), value);
	}

	inline static int32_t get_offset_of__strings_1() { return static_cast<int32_t>(offsetof(RegexReplacement_t2A1098B910D9E68191221914C5BBCBA813295359, ____strings_1)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get__strings_1() const { return ____strings_1; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of__strings_1() { return &____strings_1; }
	inline void set__strings_1(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		____strings_1 = value;
		Il2CppCodeGenWriteBarrier((&____strings_1), value);
	}

	inline static int32_t get_offset_of__rules_2() { return static_cast<int32_t>(offsetof(RegexReplacement_t2A1098B910D9E68191221914C5BBCBA813295359, ____rules_2)); }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * get__rules_2() const { return ____rules_2; }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 ** get_address_of__rules_2() { return &____rules_2; }
	inline void set__rules_2(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * value)
	{
		____rules_2 = value;
		Il2CppCodeGenWriteBarrier((&____rules_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGEXREPLACEMENT_T2A1098B910D9E68191221914C5BBCBA813295359_H
#ifndef REGEXRUNNER_TBA888C4E3D3BA80EEE14878E4A330461730446B0_H
#define REGEXRUNNER_TBA888C4E3D3BA80EEE14878E4A330461730446B0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexRunner
struct  RegexRunner_tBA888C4E3D3BA80EEE14878E4A330461730446B0  : public RuntimeObject
{
public:
	// System.Int32 System.Text.RegularExpressions.RegexRunner::runtextbeg
	int32_t ___runtextbeg_0;
	// System.Int32 System.Text.RegularExpressions.RegexRunner::runtextend
	int32_t ___runtextend_1;
	// System.Int32 System.Text.RegularExpressions.RegexRunner::runtextstart
	int32_t ___runtextstart_2;
	// System.String System.Text.RegularExpressions.RegexRunner::runtext
	String_t* ___runtext_3;
	// System.Int32 System.Text.RegularExpressions.RegexRunner::runtextpos
	int32_t ___runtextpos_4;
	// System.Int32[] System.Text.RegularExpressions.RegexRunner::runtrack
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___runtrack_5;
	// System.Int32 System.Text.RegularExpressions.RegexRunner::runtrackpos
	int32_t ___runtrackpos_6;
	// System.Int32[] System.Text.RegularExpressions.RegexRunner::runstack
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___runstack_7;
	// System.Int32 System.Text.RegularExpressions.RegexRunner::runstackpos
	int32_t ___runstackpos_8;
	// System.Int32[] System.Text.RegularExpressions.RegexRunner::runcrawl
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___runcrawl_9;
	// System.Int32 System.Text.RegularExpressions.RegexRunner::runcrawlpos
	int32_t ___runcrawlpos_10;
	// System.Int32 System.Text.RegularExpressions.RegexRunner::runtrackcount
	int32_t ___runtrackcount_11;
	// System.Text.RegularExpressions.Match System.Text.RegularExpressions.RegexRunner::runmatch
	Match_tE447871AB59EED3642F31EB9559D162C2977EBB5 * ___runmatch_12;
	// System.Text.RegularExpressions.Regex System.Text.RegularExpressions.RegexRunner::runregex
	Regex_tFD46E63A462E852189FD6AB4E2B0B67C4D8FDBDF * ___runregex_13;
	// System.Int32 System.Text.RegularExpressions.RegexRunner::timeout
	int32_t ___timeout_14;
	// System.Boolean System.Text.RegularExpressions.RegexRunner::ignoreTimeout
	bool ___ignoreTimeout_15;
	// System.Int32 System.Text.RegularExpressions.RegexRunner::timeoutOccursAt
	int32_t ___timeoutOccursAt_16;
	// System.Int32 System.Text.RegularExpressions.RegexRunner::timeoutChecksToSkip
	int32_t ___timeoutChecksToSkip_17;

public:
	inline static int32_t get_offset_of_runtextbeg_0() { return static_cast<int32_t>(offsetof(RegexRunner_tBA888C4E3D3BA80EEE14878E4A330461730446B0, ___runtextbeg_0)); }
	inline int32_t get_runtextbeg_0() const { return ___runtextbeg_0; }
	inline int32_t* get_address_of_runtextbeg_0() { return &___runtextbeg_0; }
	inline void set_runtextbeg_0(int32_t value)
	{
		___runtextbeg_0 = value;
	}

	inline static int32_t get_offset_of_runtextend_1() { return static_cast<int32_t>(offsetof(RegexRunner_tBA888C4E3D3BA80EEE14878E4A330461730446B0, ___runtextend_1)); }
	inline int32_t get_runtextend_1() const { return ___runtextend_1; }
	inline int32_t* get_address_of_runtextend_1() { return &___runtextend_1; }
	inline void set_runtextend_1(int32_t value)
	{
		___runtextend_1 = value;
	}

	inline static int32_t get_offset_of_runtextstart_2() { return static_cast<int32_t>(offsetof(RegexRunner_tBA888C4E3D3BA80EEE14878E4A330461730446B0, ___runtextstart_2)); }
	inline int32_t get_runtextstart_2() const { return ___runtextstart_2; }
	inline int32_t* get_address_of_runtextstart_2() { return &___runtextstart_2; }
	inline void set_runtextstart_2(int32_t value)
	{
		___runtextstart_2 = value;
	}

	inline static int32_t get_offset_of_runtext_3() { return static_cast<int32_t>(offsetof(RegexRunner_tBA888C4E3D3BA80EEE14878E4A330461730446B0, ___runtext_3)); }
	inline String_t* get_runtext_3() const { return ___runtext_3; }
	inline String_t** get_address_of_runtext_3() { return &___runtext_3; }
	inline void set_runtext_3(String_t* value)
	{
		___runtext_3 = value;
		Il2CppCodeGenWriteBarrier((&___runtext_3), value);
	}

	inline static int32_t get_offset_of_runtextpos_4() { return static_cast<int32_t>(offsetof(RegexRunner_tBA888C4E3D3BA80EEE14878E4A330461730446B0, ___runtextpos_4)); }
	inline int32_t get_runtextpos_4() const { return ___runtextpos_4; }
	inline int32_t* get_address_of_runtextpos_4() { return &___runtextpos_4; }
	inline void set_runtextpos_4(int32_t value)
	{
		___runtextpos_4 = value;
	}

	inline static int32_t get_offset_of_runtrack_5() { return static_cast<int32_t>(offsetof(RegexRunner_tBA888C4E3D3BA80EEE14878E4A330461730446B0, ___runtrack_5)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_runtrack_5() const { return ___runtrack_5; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_runtrack_5() { return &___runtrack_5; }
	inline void set_runtrack_5(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___runtrack_5 = value;
		Il2CppCodeGenWriteBarrier((&___runtrack_5), value);
	}

	inline static int32_t get_offset_of_runtrackpos_6() { return static_cast<int32_t>(offsetof(RegexRunner_tBA888C4E3D3BA80EEE14878E4A330461730446B0, ___runtrackpos_6)); }
	inline int32_t get_runtrackpos_6() const { return ___runtrackpos_6; }
	inline int32_t* get_address_of_runtrackpos_6() { return &___runtrackpos_6; }
	inline void set_runtrackpos_6(int32_t value)
	{
		___runtrackpos_6 = value;
	}

	inline static int32_t get_offset_of_runstack_7() { return static_cast<int32_t>(offsetof(RegexRunner_tBA888C4E3D3BA80EEE14878E4A330461730446B0, ___runstack_7)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_runstack_7() const { return ___runstack_7; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_runstack_7() { return &___runstack_7; }
	inline void set_runstack_7(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___runstack_7 = value;
		Il2CppCodeGenWriteBarrier((&___runstack_7), value);
	}

	inline static int32_t get_offset_of_runstackpos_8() { return static_cast<int32_t>(offsetof(RegexRunner_tBA888C4E3D3BA80EEE14878E4A330461730446B0, ___runstackpos_8)); }
	inline int32_t get_runstackpos_8() const { return ___runstackpos_8; }
	inline int32_t* get_address_of_runstackpos_8() { return &___runstackpos_8; }
	inline void set_runstackpos_8(int32_t value)
	{
		___runstackpos_8 = value;
	}

	inline static int32_t get_offset_of_runcrawl_9() { return static_cast<int32_t>(offsetof(RegexRunner_tBA888C4E3D3BA80EEE14878E4A330461730446B0, ___runcrawl_9)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_runcrawl_9() const { return ___runcrawl_9; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_runcrawl_9() { return &___runcrawl_9; }
	inline void set_runcrawl_9(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___runcrawl_9 = value;
		Il2CppCodeGenWriteBarrier((&___runcrawl_9), value);
	}

	inline static int32_t get_offset_of_runcrawlpos_10() { return static_cast<int32_t>(offsetof(RegexRunner_tBA888C4E3D3BA80EEE14878E4A330461730446B0, ___runcrawlpos_10)); }
	inline int32_t get_runcrawlpos_10() const { return ___runcrawlpos_10; }
	inline int32_t* get_address_of_runcrawlpos_10() { return &___runcrawlpos_10; }
	inline void set_runcrawlpos_10(int32_t value)
	{
		___runcrawlpos_10 = value;
	}

	inline static int32_t get_offset_of_runtrackcount_11() { return static_cast<int32_t>(offsetof(RegexRunner_tBA888C4E3D3BA80EEE14878E4A330461730446B0, ___runtrackcount_11)); }
	inline int32_t get_runtrackcount_11() const { return ___runtrackcount_11; }
	inline int32_t* get_address_of_runtrackcount_11() { return &___runtrackcount_11; }
	inline void set_runtrackcount_11(int32_t value)
	{
		___runtrackcount_11 = value;
	}

	inline static int32_t get_offset_of_runmatch_12() { return static_cast<int32_t>(offsetof(RegexRunner_tBA888C4E3D3BA80EEE14878E4A330461730446B0, ___runmatch_12)); }
	inline Match_tE447871AB59EED3642F31EB9559D162C2977EBB5 * get_runmatch_12() const { return ___runmatch_12; }
	inline Match_tE447871AB59EED3642F31EB9559D162C2977EBB5 ** get_address_of_runmatch_12() { return &___runmatch_12; }
	inline void set_runmatch_12(Match_tE447871AB59EED3642F31EB9559D162C2977EBB5 * value)
	{
		___runmatch_12 = value;
		Il2CppCodeGenWriteBarrier((&___runmatch_12), value);
	}

	inline static int32_t get_offset_of_runregex_13() { return static_cast<int32_t>(offsetof(RegexRunner_tBA888C4E3D3BA80EEE14878E4A330461730446B0, ___runregex_13)); }
	inline Regex_tFD46E63A462E852189FD6AB4E2B0B67C4D8FDBDF * get_runregex_13() const { return ___runregex_13; }
	inline Regex_tFD46E63A462E852189FD6AB4E2B0B67C4D8FDBDF ** get_address_of_runregex_13() { return &___runregex_13; }
	inline void set_runregex_13(Regex_tFD46E63A462E852189FD6AB4E2B0B67C4D8FDBDF * value)
	{
		___runregex_13 = value;
		Il2CppCodeGenWriteBarrier((&___runregex_13), value);
	}

	inline static int32_t get_offset_of_timeout_14() { return static_cast<int32_t>(offsetof(RegexRunner_tBA888C4E3D3BA80EEE14878E4A330461730446B0, ___timeout_14)); }
	inline int32_t get_timeout_14() const { return ___timeout_14; }
	inline int32_t* get_address_of_timeout_14() { return &___timeout_14; }
	inline void set_timeout_14(int32_t value)
	{
		___timeout_14 = value;
	}

	inline static int32_t get_offset_of_ignoreTimeout_15() { return static_cast<int32_t>(offsetof(RegexRunner_tBA888C4E3D3BA80EEE14878E4A330461730446B0, ___ignoreTimeout_15)); }
	inline bool get_ignoreTimeout_15() const { return ___ignoreTimeout_15; }
	inline bool* get_address_of_ignoreTimeout_15() { return &___ignoreTimeout_15; }
	inline void set_ignoreTimeout_15(bool value)
	{
		___ignoreTimeout_15 = value;
	}

	inline static int32_t get_offset_of_timeoutOccursAt_16() { return static_cast<int32_t>(offsetof(RegexRunner_tBA888C4E3D3BA80EEE14878E4A330461730446B0, ___timeoutOccursAt_16)); }
	inline int32_t get_timeoutOccursAt_16() const { return ___timeoutOccursAt_16; }
	inline int32_t* get_address_of_timeoutOccursAt_16() { return &___timeoutOccursAt_16; }
	inline void set_timeoutOccursAt_16(int32_t value)
	{
		___timeoutOccursAt_16 = value;
	}

	inline static int32_t get_offset_of_timeoutChecksToSkip_17() { return static_cast<int32_t>(offsetof(RegexRunner_tBA888C4E3D3BA80EEE14878E4A330461730446B0, ___timeoutChecksToSkip_17)); }
	inline int32_t get_timeoutChecksToSkip_17() const { return ___timeoutChecksToSkip_17; }
	inline int32_t* get_address_of_timeoutChecksToSkip_17() { return &___timeoutChecksToSkip_17; }
	inline void set_timeoutChecksToSkip_17(int32_t value)
	{
		___timeoutChecksToSkip_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGEXRUNNER_TBA888C4E3D3BA80EEE14878E4A330461730446B0_H
#ifndef REGEXRUNNERFACTORY_T0703F390E2102623B0189DEC095DB182698E404B_H
#define REGEXRUNNERFACTORY_T0703F390E2102623B0189DEC095DB182698E404B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexRunnerFactory
struct  RegexRunnerFactory_t0703F390E2102623B0189DEC095DB182698E404B  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGEXRUNNERFACTORY_T0703F390E2102623B0189DEC095DB182698E404B_H
#ifndef REGEXWRITER_T5F8CEA1FC9A4AC32A95BAC6E49EF51D7DF051AA6_H
#define REGEXWRITER_T5F8CEA1FC9A4AC32A95BAC6E49EF51D7DF051AA6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexWriter
struct  RegexWriter_t5F8CEA1FC9A4AC32A95BAC6E49EF51D7DF051AA6  : public RuntimeObject
{
public:
	// System.Int32[] System.Text.RegularExpressions.RegexWriter::_intStack
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ____intStack_0;
	// System.Int32 System.Text.RegularExpressions.RegexWriter::_depth
	int32_t ____depth_1;
	// System.Int32[] System.Text.RegularExpressions.RegexWriter::_emitted
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ____emitted_2;
	// System.Int32 System.Text.RegularExpressions.RegexWriter::_curpos
	int32_t ____curpos_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Text.RegularExpressions.RegexWriter::_stringhash
	Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * ____stringhash_4;
	// System.Collections.Generic.List`1<System.String> System.Text.RegularExpressions.RegexWriter::_stringtable
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ____stringtable_5;
	// System.Boolean System.Text.RegularExpressions.RegexWriter::_counting
	bool ____counting_6;
	// System.Int32 System.Text.RegularExpressions.RegexWriter::_count
	int32_t ____count_7;
	// System.Int32 System.Text.RegularExpressions.RegexWriter::_trackcount
	int32_t ____trackcount_8;
	// System.Collections.Hashtable System.Text.RegularExpressions.RegexWriter::_caps
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ____caps_9;

public:
	inline static int32_t get_offset_of__intStack_0() { return static_cast<int32_t>(offsetof(RegexWriter_t5F8CEA1FC9A4AC32A95BAC6E49EF51D7DF051AA6, ____intStack_0)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get__intStack_0() const { return ____intStack_0; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of__intStack_0() { return &____intStack_0; }
	inline void set__intStack_0(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		____intStack_0 = value;
		Il2CppCodeGenWriteBarrier((&____intStack_0), value);
	}

	inline static int32_t get_offset_of__depth_1() { return static_cast<int32_t>(offsetof(RegexWriter_t5F8CEA1FC9A4AC32A95BAC6E49EF51D7DF051AA6, ____depth_1)); }
	inline int32_t get__depth_1() const { return ____depth_1; }
	inline int32_t* get_address_of__depth_1() { return &____depth_1; }
	inline void set__depth_1(int32_t value)
	{
		____depth_1 = value;
	}

	inline static int32_t get_offset_of__emitted_2() { return static_cast<int32_t>(offsetof(RegexWriter_t5F8CEA1FC9A4AC32A95BAC6E49EF51D7DF051AA6, ____emitted_2)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get__emitted_2() const { return ____emitted_2; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of__emitted_2() { return &____emitted_2; }
	inline void set__emitted_2(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		____emitted_2 = value;
		Il2CppCodeGenWriteBarrier((&____emitted_2), value);
	}

	inline static int32_t get_offset_of__curpos_3() { return static_cast<int32_t>(offsetof(RegexWriter_t5F8CEA1FC9A4AC32A95BAC6E49EF51D7DF051AA6, ____curpos_3)); }
	inline int32_t get__curpos_3() const { return ____curpos_3; }
	inline int32_t* get_address_of__curpos_3() { return &____curpos_3; }
	inline void set__curpos_3(int32_t value)
	{
		____curpos_3 = value;
	}

	inline static int32_t get_offset_of__stringhash_4() { return static_cast<int32_t>(offsetof(RegexWriter_t5F8CEA1FC9A4AC32A95BAC6E49EF51D7DF051AA6, ____stringhash_4)); }
	inline Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * get__stringhash_4() const { return ____stringhash_4; }
	inline Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB ** get_address_of__stringhash_4() { return &____stringhash_4; }
	inline void set__stringhash_4(Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * value)
	{
		____stringhash_4 = value;
		Il2CppCodeGenWriteBarrier((&____stringhash_4), value);
	}

	inline static int32_t get_offset_of__stringtable_5() { return static_cast<int32_t>(offsetof(RegexWriter_t5F8CEA1FC9A4AC32A95BAC6E49EF51D7DF051AA6, ____stringtable_5)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get__stringtable_5() const { return ____stringtable_5; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of__stringtable_5() { return &____stringtable_5; }
	inline void set__stringtable_5(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		____stringtable_5 = value;
		Il2CppCodeGenWriteBarrier((&____stringtable_5), value);
	}

	inline static int32_t get_offset_of__counting_6() { return static_cast<int32_t>(offsetof(RegexWriter_t5F8CEA1FC9A4AC32A95BAC6E49EF51D7DF051AA6, ____counting_6)); }
	inline bool get__counting_6() const { return ____counting_6; }
	inline bool* get_address_of__counting_6() { return &____counting_6; }
	inline void set__counting_6(bool value)
	{
		____counting_6 = value;
	}

	inline static int32_t get_offset_of__count_7() { return static_cast<int32_t>(offsetof(RegexWriter_t5F8CEA1FC9A4AC32A95BAC6E49EF51D7DF051AA6, ____count_7)); }
	inline int32_t get__count_7() const { return ____count_7; }
	inline int32_t* get_address_of__count_7() { return &____count_7; }
	inline void set__count_7(int32_t value)
	{
		____count_7 = value;
	}

	inline static int32_t get_offset_of__trackcount_8() { return static_cast<int32_t>(offsetof(RegexWriter_t5F8CEA1FC9A4AC32A95BAC6E49EF51D7DF051AA6, ____trackcount_8)); }
	inline int32_t get__trackcount_8() const { return ____trackcount_8; }
	inline int32_t* get_address_of__trackcount_8() { return &____trackcount_8; }
	inline void set__trackcount_8(int32_t value)
	{
		____trackcount_8 = value;
	}

	inline static int32_t get_offset_of__caps_9() { return static_cast<int32_t>(offsetof(RegexWriter_t5F8CEA1FC9A4AC32A95BAC6E49EF51D7DF051AA6, ____caps_9)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get__caps_9() const { return ____caps_9; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of__caps_9() { return &____caps_9; }
	inline void set__caps_9(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		____caps_9 = value;
		Il2CppCodeGenWriteBarrier((&____caps_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGEXWRITER_T5F8CEA1FC9A4AC32A95BAC6E49EF51D7DF051AA6_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#define BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifndef ADDINGNEWEVENTARGS_T848D637EA4E72C12479C42DF55305280B37948D1_H
#define ADDINGNEWEVENTARGS_T848D637EA4E72C12479C42DF55305280B37948D1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.AddingNewEventArgs
struct  AddingNewEventArgs_t848D637EA4E72C12479C42DF55305280B37948D1  : public EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E
{
public:
	// System.Object System.ComponentModel.AddingNewEventArgs::newObject
	RuntimeObject * ___newObject_1;

public:
	inline static int32_t get_offset_of_newObject_1() { return static_cast<int32_t>(offsetof(AddingNewEventArgs_t848D637EA4E72C12479C42DF55305280B37948D1, ___newObject_1)); }
	inline RuntimeObject * get_newObject_1() const { return ___newObject_1; }
	inline RuntimeObject ** get_address_of_newObject_1() { return &___newObject_1; }
	inline void set_newObject_1(RuntimeObject * value)
	{
		___newObject_1 = value;
		Il2CppCodeGenWriteBarrier((&___newObject_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADDINGNEWEVENTARGS_T848D637EA4E72C12479C42DF55305280B37948D1_H
#ifndef AMBIENTVALUEATTRIBUTE_T9B10538B74BF90773C9F720E1D1E1B10BC63715E_H
#define AMBIENTVALUEATTRIBUTE_T9B10538B74BF90773C9F720E1D1E1B10BC63715E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.AmbientValueAttribute
struct  AmbientValueAttribute_t9B10538B74BF90773C9F720E1D1E1B10BC63715E  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Object System.ComponentModel.AmbientValueAttribute::value
	RuntimeObject * ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(AmbientValueAttribute_t9B10538B74BF90773C9F720E1D1E1B10BC63715E, ___value_0)); }
	inline RuntimeObject * get_value_0() const { return ___value_0; }
	inline RuntimeObject ** get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(RuntimeObject * value)
	{
		___value_0 = value;
		Il2CppCodeGenWriteBarrier((&___value_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AMBIENTVALUEATTRIBUTE_T9B10538B74BF90773C9F720E1D1E1B10BC63715E_H
#ifndef ASYNCCOMPLETEDEVENTARGS_TADCBE47368F4D8B198B46D332B27EE38C8B5F8B7_H
#define ASYNCCOMPLETEDEVENTARGS_TADCBE47368F4D8B198B46D332B27EE38C8B5F8B7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.AsyncCompletedEventArgs
struct  AsyncCompletedEventArgs_tADCBE47368F4D8B198B46D332B27EE38C8B5F8B7  : public EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E
{
public:
	// System.Exception System.ComponentModel.AsyncCompletedEventArgs::error
	Exception_t * ___error_1;
	// System.Boolean System.ComponentModel.AsyncCompletedEventArgs::cancelled
	bool ___cancelled_2;
	// System.Object System.ComponentModel.AsyncCompletedEventArgs::userState
	RuntimeObject * ___userState_3;

public:
	inline static int32_t get_offset_of_error_1() { return static_cast<int32_t>(offsetof(AsyncCompletedEventArgs_tADCBE47368F4D8B198B46D332B27EE38C8B5F8B7, ___error_1)); }
	inline Exception_t * get_error_1() const { return ___error_1; }
	inline Exception_t ** get_address_of_error_1() { return &___error_1; }
	inline void set_error_1(Exception_t * value)
	{
		___error_1 = value;
		Il2CppCodeGenWriteBarrier((&___error_1), value);
	}

	inline static int32_t get_offset_of_cancelled_2() { return static_cast<int32_t>(offsetof(AsyncCompletedEventArgs_tADCBE47368F4D8B198B46D332B27EE38C8B5F8B7, ___cancelled_2)); }
	inline bool get_cancelled_2() const { return ___cancelled_2; }
	inline bool* get_address_of_cancelled_2() { return &___cancelled_2; }
	inline void set_cancelled_2(bool value)
	{
		___cancelled_2 = value;
	}

	inline static int32_t get_offset_of_userState_3() { return static_cast<int32_t>(offsetof(AsyncCompletedEventArgs_tADCBE47368F4D8B198B46D332B27EE38C8B5F8B7, ___userState_3)); }
	inline RuntimeObject * get_userState_3() const { return ___userState_3; }
	inline RuntimeObject ** get_address_of_userState_3() { return &___userState_3; }
	inline void set_userState_3(RuntimeObject * value)
	{
		___userState_3 = value;
		Il2CppCodeGenWriteBarrier((&___userState_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCCOMPLETEDEVENTARGS_TADCBE47368F4D8B198B46D332B27EE38C8B5F8B7_H
#ifndef ATTRIBUTEENTRY_T03E9BFE6BF6BE56EA2568359ACD774FE8C8661DD_H
#define ATTRIBUTEENTRY_T03E9BFE6BF6BE56EA2568359ACD774FE8C8661DD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.AttributeCollection_AttributeEntry
struct  AttributeEntry_t03E9BFE6BF6BE56EA2568359ACD774FE8C8661DD 
{
public:
	// System.Type System.ComponentModel.AttributeCollection_AttributeEntry::type
	Type_t * ___type_0;
	// System.Int32 System.ComponentModel.AttributeCollection_AttributeEntry::index
	int32_t ___index_1;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(AttributeEntry_t03E9BFE6BF6BE56EA2568359ACD774FE8C8661DD, ___type_0)); }
	inline Type_t * get_type_0() const { return ___type_0; }
	inline Type_t ** get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(Type_t * value)
	{
		___type_0 = value;
		Il2CppCodeGenWriteBarrier((&___type_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(AttributeEntry_t03E9BFE6BF6BE56EA2568359ACD774FE8C8661DD, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ComponentModel.AttributeCollection/AttributeEntry
struct AttributeEntry_t03E9BFE6BF6BE56EA2568359ACD774FE8C8661DD_marshaled_pinvoke
{
	Type_t * ___type_0;
	int32_t ___index_1;
};
// Native definition for COM marshalling of System.ComponentModel.AttributeCollection/AttributeEntry
struct AttributeEntry_t03E9BFE6BF6BE56EA2568359ACD774FE8C8661DD_marshaled_com
{
	Type_t * ___type_0;
	int32_t ___index_1;
};
#endif // ATTRIBUTEENTRY_T03E9BFE6BF6BE56EA2568359ACD774FE8C8661DD_H
#ifndef ATTRIBUTEPROVIDERATTRIBUTE_T8EC0C13032FE51365470EF1C6FDE9E8B1C33B068_H
#define ATTRIBUTEPROVIDERATTRIBUTE_T8EC0C13032FE51365470EF1C6FDE9E8B1C33B068_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.AttributeProviderAttribute
struct  AttributeProviderAttribute_t8EC0C13032FE51365470EF1C6FDE9E8B1C33B068  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String System.ComponentModel.AttributeProviderAttribute::_typeName
	String_t* ____typeName_0;
	// System.String System.ComponentModel.AttributeProviderAttribute::_propertyName
	String_t* ____propertyName_1;

public:
	inline static int32_t get_offset_of__typeName_0() { return static_cast<int32_t>(offsetof(AttributeProviderAttribute_t8EC0C13032FE51365470EF1C6FDE9E8B1C33B068, ____typeName_0)); }
	inline String_t* get__typeName_0() const { return ____typeName_0; }
	inline String_t** get_address_of__typeName_0() { return &____typeName_0; }
	inline void set__typeName_0(String_t* value)
	{
		____typeName_0 = value;
		Il2CppCodeGenWriteBarrier((&____typeName_0), value);
	}

	inline static int32_t get_offset_of__propertyName_1() { return static_cast<int32_t>(offsetof(AttributeProviderAttribute_t8EC0C13032FE51365470EF1C6FDE9E8B1C33B068, ____propertyName_1)); }
	inline String_t* get__propertyName_1() const { return ____propertyName_1; }
	inline String_t** get_address_of__propertyName_1() { return &____propertyName_1; }
	inline void set__propertyName_1(String_t* value)
	{
		____propertyName_1 = value;
		Il2CppCodeGenWriteBarrier((&____propertyName_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTEPROVIDERATTRIBUTE_T8EC0C13032FE51365470EF1C6FDE9E8B1C33B068_H
#ifndef BROWSABLEATTRIBUTE_T8A1A514FEE5735ADED64CCFE6E06A42E5CD872D1_H
#define BROWSABLEATTRIBUTE_T8A1A514FEE5735ADED64CCFE6E06A42E5CD872D1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.BrowsableAttribute
struct  BrowsableAttribute_t8A1A514FEE5735ADED64CCFE6E06A42E5CD872D1  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Boolean System.ComponentModel.BrowsableAttribute::browsable
	bool ___browsable_3;

public:
	inline static int32_t get_offset_of_browsable_3() { return static_cast<int32_t>(offsetof(BrowsableAttribute_t8A1A514FEE5735ADED64CCFE6E06A42E5CD872D1, ___browsable_3)); }
	inline bool get_browsable_3() const { return ___browsable_3; }
	inline bool* get_address_of_browsable_3() { return &___browsable_3; }
	inline void set_browsable_3(bool value)
	{
		___browsable_3 = value;
	}
};

struct BrowsableAttribute_t8A1A514FEE5735ADED64CCFE6E06A42E5CD872D1_StaticFields
{
public:
	// System.ComponentModel.BrowsableAttribute System.ComponentModel.BrowsableAttribute::Yes
	BrowsableAttribute_t8A1A514FEE5735ADED64CCFE6E06A42E5CD872D1 * ___Yes_0;
	// System.ComponentModel.BrowsableAttribute System.ComponentModel.BrowsableAttribute::No
	BrowsableAttribute_t8A1A514FEE5735ADED64CCFE6E06A42E5CD872D1 * ___No_1;
	// System.ComponentModel.BrowsableAttribute System.ComponentModel.BrowsableAttribute::Default
	BrowsableAttribute_t8A1A514FEE5735ADED64CCFE6E06A42E5CD872D1 * ___Default_2;

public:
	inline static int32_t get_offset_of_Yes_0() { return static_cast<int32_t>(offsetof(BrowsableAttribute_t8A1A514FEE5735ADED64CCFE6E06A42E5CD872D1_StaticFields, ___Yes_0)); }
	inline BrowsableAttribute_t8A1A514FEE5735ADED64CCFE6E06A42E5CD872D1 * get_Yes_0() const { return ___Yes_0; }
	inline BrowsableAttribute_t8A1A514FEE5735ADED64CCFE6E06A42E5CD872D1 ** get_address_of_Yes_0() { return &___Yes_0; }
	inline void set_Yes_0(BrowsableAttribute_t8A1A514FEE5735ADED64CCFE6E06A42E5CD872D1 * value)
	{
		___Yes_0 = value;
		Il2CppCodeGenWriteBarrier((&___Yes_0), value);
	}

	inline static int32_t get_offset_of_No_1() { return static_cast<int32_t>(offsetof(BrowsableAttribute_t8A1A514FEE5735ADED64CCFE6E06A42E5CD872D1_StaticFields, ___No_1)); }
	inline BrowsableAttribute_t8A1A514FEE5735ADED64CCFE6E06A42E5CD872D1 * get_No_1() const { return ___No_1; }
	inline BrowsableAttribute_t8A1A514FEE5735ADED64CCFE6E06A42E5CD872D1 ** get_address_of_No_1() { return &___No_1; }
	inline void set_No_1(BrowsableAttribute_t8A1A514FEE5735ADED64CCFE6E06A42E5CD872D1 * value)
	{
		___No_1 = value;
		Il2CppCodeGenWriteBarrier((&___No_1), value);
	}

	inline static int32_t get_offset_of_Default_2() { return static_cast<int32_t>(offsetof(BrowsableAttribute_t8A1A514FEE5735ADED64CCFE6E06A42E5CD872D1_StaticFields, ___Default_2)); }
	inline BrowsableAttribute_t8A1A514FEE5735ADED64CCFE6E06A42E5CD872D1 * get_Default_2() const { return ___Default_2; }
	inline BrowsableAttribute_t8A1A514FEE5735ADED64CCFE6E06A42E5CD872D1 ** get_address_of_Default_2() { return &___Default_2; }
	inline void set_Default_2(BrowsableAttribute_t8A1A514FEE5735ADED64CCFE6E06A42E5CD872D1 * value)
	{
		___Default_2 = value;
		Il2CppCodeGenWriteBarrier((&___Default_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BROWSABLEATTRIBUTE_T8A1A514FEE5735ADED64CCFE6E06A42E5CD872D1_H
#ifndef CANCELEVENTARGS_T2843141F2893A01C11535CD7E38072CA26D7794D_H
#define CANCELEVENTARGS_T2843141F2893A01C11535CD7E38072CA26D7794D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.CancelEventArgs
struct  CancelEventArgs_t2843141F2893A01C11535CD7E38072CA26D7794D  : public EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E
{
public:
	// System.Boolean System.ComponentModel.CancelEventArgs::cancel
	bool ___cancel_1;

public:
	inline static int32_t get_offset_of_cancel_1() { return static_cast<int32_t>(offsetof(CancelEventArgs_t2843141F2893A01C11535CD7E38072CA26D7794D, ___cancel_1)); }
	inline bool get_cancel_1() const { return ___cancel_1; }
	inline bool* get_address_of_cancel_1() { return &___cancel_1; }
	inline void set_cancel_1(bool value)
	{
		___cancel_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANCELEVENTARGS_T2843141F2893A01C11535CD7E38072CA26D7794D_H
#ifndef CATEGORYATTRIBUTE_T89C58D266A4A65CD58E04FF63344AA3E0AF57B80_H
#define CATEGORYATTRIBUTE_T89C58D266A4A65CD58E04FF63344AA3E0AF57B80_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.CategoryAttribute
struct  CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Boolean System.ComponentModel.CategoryAttribute::localized
	bool ___localized_14;
	// System.String System.ComponentModel.CategoryAttribute::categoryValue
	String_t* ___categoryValue_15;

public:
	inline static int32_t get_offset_of_localized_14() { return static_cast<int32_t>(offsetof(CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80, ___localized_14)); }
	inline bool get_localized_14() const { return ___localized_14; }
	inline bool* get_address_of_localized_14() { return &___localized_14; }
	inline void set_localized_14(bool value)
	{
		___localized_14 = value;
	}

	inline static int32_t get_offset_of_categoryValue_15() { return static_cast<int32_t>(offsetof(CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80, ___categoryValue_15)); }
	inline String_t* get_categoryValue_15() const { return ___categoryValue_15; }
	inline String_t** get_address_of_categoryValue_15() { return &___categoryValue_15; }
	inline void set_categoryValue_15(String_t* value)
	{
		___categoryValue_15 = value;
		Il2CppCodeGenWriteBarrier((&___categoryValue_15), value);
	}
};

struct CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80_StaticFields
{
public:
	// System.ComponentModel.CategoryAttribute modreq(System.Runtime.CompilerServices.IsVolatile) System.ComponentModel.CategoryAttribute::appearance
	CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * ___appearance_0;
	// System.ComponentModel.CategoryAttribute modreq(System.Runtime.CompilerServices.IsVolatile) System.ComponentModel.CategoryAttribute::asynchronous
	CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * ___asynchronous_1;
	// System.ComponentModel.CategoryAttribute modreq(System.Runtime.CompilerServices.IsVolatile) System.ComponentModel.CategoryAttribute::behavior
	CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * ___behavior_2;
	// System.ComponentModel.CategoryAttribute modreq(System.Runtime.CompilerServices.IsVolatile) System.ComponentModel.CategoryAttribute::data
	CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * ___data_3;
	// System.ComponentModel.CategoryAttribute modreq(System.Runtime.CompilerServices.IsVolatile) System.ComponentModel.CategoryAttribute::design
	CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * ___design_4;
	// System.ComponentModel.CategoryAttribute modreq(System.Runtime.CompilerServices.IsVolatile) System.ComponentModel.CategoryAttribute::action
	CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * ___action_5;
	// System.ComponentModel.CategoryAttribute modreq(System.Runtime.CompilerServices.IsVolatile) System.ComponentModel.CategoryAttribute::format
	CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * ___format_6;
	// System.ComponentModel.CategoryAttribute modreq(System.Runtime.CompilerServices.IsVolatile) System.ComponentModel.CategoryAttribute::layout
	CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * ___layout_7;
	// System.ComponentModel.CategoryAttribute modreq(System.Runtime.CompilerServices.IsVolatile) System.ComponentModel.CategoryAttribute::mouse
	CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * ___mouse_8;
	// System.ComponentModel.CategoryAttribute modreq(System.Runtime.CompilerServices.IsVolatile) System.ComponentModel.CategoryAttribute::key
	CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * ___key_9;
	// System.ComponentModel.CategoryAttribute modreq(System.Runtime.CompilerServices.IsVolatile) System.ComponentModel.CategoryAttribute::focus
	CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * ___focus_10;
	// System.ComponentModel.CategoryAttribute modreq(System.Runtime.CompilerServices.IsVolatile) System.ComponentModel.CategoryAttribute::windowStyle
	CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * ___windowStyle_11;
	// System.ComponentModel.CategoryAttribute modreq(System.Runtime.CompilerServices.IsVolatile) System.ComponentModel.CategoryAttribute::dragDrop
	CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * ___dragDrop_12;
	// System.ComponentModel.CategoryAttribute modreq(System.Runtime.CompilerServices.IsVolatile) System.ComponentModel.CategoryAttribute::defAttr
	CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * ___defAttr_13;

public:
	inline static int32_t get_offset_of_appearance_0() { return static_cast<int32_t>(offsetof(CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80_StaticFields, ___appearance_0)); }
	inline CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * get_appearance_0() const { return ___appearance_0; }
	inline CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 ** get_address_of_appearance_0() { return &___appearance_0; }
	inline void set_appearance_0(CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * value)
	{
		___appearance_0 = value;
		Il2CppCodeGenWriteBarrier((&___appearance_0), value);
	}

	inline static int32_t get_offset_of_asynchronous_1() { return static_cast<int32_t>(offsetof(CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80_StaticFields, ___asynchronous_1)); }
	inline CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * get_asynchronous_1() const { return ___asynchronous_1; }
	inline CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 ** get_address_of_asynchronous_1() { return &___asynchronous_1; }
	inline void set_asynchronous_1(CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * value)
	{
		___asynchronous_1 = value;
		Il2CppCodeGenWriteBarrier((&___asynchronous_1), value);
	}

	inline static int32_t get_offset_of_behavior_2() { return static_cast<int32_t>(offsetof(CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80_StaticFields, ___behavior_2)); }
	inline CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * get_behavior_2() const { return ___behavior_2; }
	inline CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 ** get_address_of_behavior_2() { return &___behavior_2; }
	inline void set_behavior_2(CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * value)
	{
		___behavior_2 = value;
		Il2CppCodeGenWriteBarrier((&___behavior_2), value);
	}

	inline static int32_t get_offset_of_data_3() { return static_cast<int32_t>(offsetof(CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80_StaticFields, ___data_3)); }
	inline CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * get_data_3() const { return ___data_3; }
	inline CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 ** get_address_of_data_3() { return &___data_3; }
	inline void set_data_3(CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * value)
	{
		___data_3 = value;
		Il2CppCodeGenWriteBarrier((&___data_3), value);
	}

	inline static int32_t get_offset_of_design_4() { return static_cast<int32_t>(offsetof(CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80_StaticFields, ___design_4)); }
	inline CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * get_design_4() const { return ___design_4; }
	inline CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 ** get_address_of_design_4() { return &___design_4; }
	inline void set_design_4(CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * value)
	{
		___design_4 = value;
		Il2CppCodeGenWriteBarrier((&___design_4), value);
	}

	inline static int32_t get_offset_of_action_5() { return static_cast<int32_t>(offsetof(CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80_StaticFields, ___action_5)); }
	inline CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * get_action_5() const { return ___action_5; }
	inline CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 ** get_address_of_action_5() { return &___action_5; }
	inline void set_action_5(CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * value)
	{
		___action_5 = value;
		Il2CppCodeGenWriteBarrier((&___action_5), value);
	}

	inline static int32_t get_offset_of_format_6() { return static_cast<int32_t>(offsetof(CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80_StaticFields, ___format_6)); }
	inline CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * get_format_6() const { return ___format_6; }
	inline CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 ** get_address_of_format_6() { return &___format_6; }
	inline void set_format_6(CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * value)
	{
		___format_6 = value;
		Il2CppCodeGenWriteBarrier((&___format_6), value);
	}

	inline static int32_t get_offset_of_layout_7() { return static_cast<int32_t>(offsetof(CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80_StaticFields, ___layout_7)); }
	inline CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * get_layout_7() const { return ___layout_7; }
	inline CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 ** get_address_of_layout_7() { return &___layout_7; }
	inline void set_layout_7(CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * value)
	{
		___layout_7 = value;
		Il2CppCodeGenWriteBarrier((&___layout_7), value);
	}

	inline static int32_t get_offset_of_mouse_8() { return static_cast<int32_t>(offsetof(CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80_StaticFields, ___mouse_8)); }
	inline CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * get_mouse_8() const { return ___mouse_8; }
	inline CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 ** get_address_of_mouse_8() { return &___mouse_8; }
	inline void set_mouse_8(CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * value)
	{
		___mouse_8 = value;
		Il2CppCodeGenWriteBarrier((&___mouse_8), value);
	}

	inline static int32_t get_offset_of_key_9() { return static_cast<int32_t>(offsetof(CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80_StaticFields, ___key_9)); }
	inline CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * get_key_9() const { return ___key_9; }
	inline CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 ** get_address_of_key_9() { return &___key_9; }
	inline void set_key_9(CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * value)
	{
		___key_9 = value;
		Il2CppCodeGenWriteBarrier((&___key_9), value);
	}

	inline static int32_t get_offset_of_focus_10() { return static_cast<int32_t>(offsetof(CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80_StaticFields, ___focus_10)); }
	inline CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * get_focus_10() const { return ___focus_10; }
	inline CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 ** get_address_of_focus_10() { return &___focus_10; }
	inline void set_focus_10(CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * value)
	{
		___focus_10 = value;
		Il2CppCodeGenWriteBarrier((&___focus_10), value);
	}

	inline static int32_t get_offset_of_windowStyle_11() { return static_cast<int32_t>(offsetof(CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80_StaticFields, ___windowStyle_11)); }
	inline CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * get_windowStyle_11() const { return ___windowStyle_11; }
	inline CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 ** get_address_of_windowStyle_11() { return &___windowStyle_11; }
	inline void set_windowStyle_11(CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * value)
	{
		___windowStyle_11 = value;
		Il2CppCodeGenWriteBarrier((&___windowStyle_11), value);
	}

	inline static int32_t get_offset_of_dragDrop_12() { return static_cast<int32_t>(offsetof(CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80_StaticFields, ___dragDrop_12)); }
	inline CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * get_dragDrop_12() const { return ___dragDrop_12; }
	inline CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 ** get_address_of_dragDrop_12() { return &___dragDrop_12; }
	inline void set_dragDrop_12(CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * value)
	{
		___dragDrop_12 = value;
		Il2CppCodeGenWriteBarrier((&___dragDrop_12), value);
	}

	inline static int32_t get_offset_of_defAttr_13() { return static_cast<int32_t>(offsetof(CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80_StaticFields, ___defAttr_13)); }
	inline CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * get_defAttr_13() const { return ___defAttr_13; }
	inline CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 ** get_address_of_defAttr_13() { return &___defAttr_13; }
	inline void set_defAttr_13(CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * value)
	{
		___defAttr_13 = value;
		Il2CppCodeGenWriteBarrier((&___defAttr_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CATEGORYATTRIBUTE_T89C58D266A4A65CD58E04FF63344AA3E0AF57B80_H
#ifndef COMPLEXBINDINGPROPERTIESATTRIBUTE_TA6DB48D4B4B6A8880F1F37C7F5860CB9789A0CEC_H
#define COMPLEXBINDINGPROPERTIESATTRIBUTE_TA6DB48D4B4B6A8880F1F37C7F5860CB9789A0CEC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.ComplexBindingPropertiesAttribute
struct  ComplexBindingPropertiesAttribute_tA6DB48D4B4B6A8880F1F37C7F5860CB9789A0CEC  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String System.ComponentModel.ComplexBindingPropertiesAttribute::dataSource
	String_t* ___dataSource_0;
	// System.String System.ComponentModel.ComplexBindingPropertiesAttribute::dataMember
	String_t* ___dataMember_1;

public:
	inline static int32_t get_offset_of_dataSource_0() { return static_cast<int32_t>(offsetof(ComplexBindingPropertiesAttribute_tA6DB48D4B4B6A8880F1F37C7F5860CB9789A0CEC, ___dataSource_0)); }
	inline String_t* get_dataSource_0() const { return ___dataSource_0; }
	inline String_t** get_address_of_dataSource_0() { return &___dataSource_0; }
	inline void set_dataSource_0(String_t* value)
	{
		___dataSource_0 = value;
		Il2CppCodeGenWriteBarrier((&___dataSource_0), value);
	}

	inline static int32_t get_offset_of_dataMember_1() { return static_cast<int32_t>(offsetof(ComplexBindingPropertiesAttribute_tA6DB48D4B4B6A8880F1F37C7F5860CB9789A0CEC, ___dataMember_1)); }
	inline String_t* get_dataMember_1() const { return ___dataMember_1; }
	inline String_t** get_address_of_dataMember_1() { return &___dataMember_1; }
	inline void set_dataMember_1(String_t* value)
	{
		___dataMember_1 = value;
		Il2CppCodeGenWriteBarrier((&___dataMember_1), value);
	}
};

struct ComplexBindingPropertiesAttribute_tA6DB48D4B4B6A8880F1F37C7F5860CB9789A0CEC_StaticFields
{
public:
	// System.ComponentModel.ComplexBindingPropertiesAttribute System.ComponentModel.ComplexBindingPropertiesAttribute::Default
	ComplexBindingPropertiesAttribute_tA6DB48D4B4B6A8880F1F37C7F5860CB9789A0CEC * ___Default_2;

public:
	inline static int32_t get_offset_of_Default_2() { return static_cast<int32_t>(offsetof(ComplexBindingPropertiesAttribute_tA6DB48D4B4B6A8880F1F37C7F5860CB9789A0CEC_StaticFields, ___Default_2)); }
	inline ComplexBindingPropertiesAttribute_tA6DB48D4B4B6A8880F1F37C7F5860CB9789A0CEC * get_Default_2() const { return ___Default_2; }
	inline ComplexBindingPropertiesAttribute_tA6DB48D4B4B6A8880F1F37C7F5860CB9789A0CEC ** get_address_of_Default_2() { return &___Default_2; }
	inline void set_Default_2(ComplexBindingPropertiesAttribute_tA6DB48D4B4B6A8880F1F37C7F5860CB9789A0CEC * value)
	{
		___Default_2 = value;
		Il2CppCodeGenWriteBarrier((&___Default_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPLEXBINDINGPROPERTIESATTRIBUTE_TA6DB48D4B4B6A8880F1F37C7F5860CB9789A0CEC_H
#ifndef COMPONENT_T7AEFE153F6778CF52E1981BC3E811A9604B29473_H
#define COMPONENT_T7AEFE153F6778CF52E1981BC3E811A9604B29473_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.Component
struct  Component_t7AEFE153F6778CF52E1981BC3E811A9604B29473  : public MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF
{
public:
	// System.ComponentModel.ISite System.ComponentModel.Component::site
	RuntimeObject* ___site_2;
	// System.ComponentModel.EventHandlerList System.ComponentModel.Component::events
	EventHandlerList_tFE9EF79E85419EBB2C206CF475E29A9960699BE4 * ___events_3;

public:
	inline static int32_t get_offset_of_site_2() { return static_cast<int32_t>(offsetof(Component_t7AEFE153F6778CF52E1981BC3E811A9604B29473, ___site_2)); }
	inline RuntimeObject* get_site_2() const { return ___site_2; }
	inline RuntimeObject** get_address_of_site_2() { return &___site_2; }
	inline void set_site_2(RuntimeObject* value)
	{
		___site_2 = value;
		Il2CppCodeGenWriteBarrier((&___site_2), value);
	}

	inline static int32_t get_offset_of_events_3() { return static_cast<int32_t>(offsetof(Component_t7AEFE153F6778CF52E1981BC3E811A9604B29473, ___events_3)); }
	inline EventHandlerList_tFE9EF79E85419EBB2C206CF475E29A9960699BE4 * get_events_3() const { return ___events_3; }
	inline EventHandlerList_tFE9EF79E85419EBB2C206CF475E29A9960699BE4 ** get_address_of_events_3() { return &___events_3; }
	inline void set_events_3(EventHandlerList_tFE9EF79E85419EBB2C206CF475E29A9960699BE4 * value)
	{
		___events_3 = value;
		Il2CppCodeGenWriteBarrier((&___events_3), value);
	}
};

struct Component_t7AEFE153F6778CF52E1981BC3E811A9604B29473_StaticFields
{
public:
	// System.Object System.ComponentModel.Component::EventDisposed
	RuntimeObject * ___EventDisposed_1;

public:
	inline static int32_t get_offset_of_EventDisposed_1() { return static_cast<int32_t>(offsetof(Component_t7AEFE153F6778CF52E1981BC3E811A9604B29473_StaticFields, ___EventDisposed_1)); }
	inline RuntimeObject * get_EventDisposed_1() const { return ___EventDisposed_1; }
	inline RuntimeObject ** get_address_of_EventDisposed_1() { return &___EventDisposed_1; }
	inline void set_EventDisposed_1(RuntimeObject * value)
	{
		___EventDisposed_1 = value;
		Il2CppCodeGenWriteBarrier((&___EventDisposed_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T7AEFE153F6778CF52E1981BC3E811A9604B29473_H
#ifndef COMPONENTCOLLECTION_TCA923631B2E55E6A831F9D5CF5231113764CB895_H
#define COMPONENTCOLLECTION_TCA923631B2E55E6A831F9D5CF5231113764CB895_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.ComponentCollection
struct  ComponentCollection_tCA923631B2E55E6A831F9D5CF5231113764CB895  : public ReadOnlyCollectionBase_tFD695167917CE6DF4FA18A906FA530880B9B8772
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENTCOLLECTION_TCA923631B2E55E6A831F9D5CF5231113764CB895_H
#ifndef DATAERRORSCHANGEDEVENTARGS_T83896D84C96C5BC0478ACF7E99C7703D464BE11F_H
#define DATAERRORSCHANGEDEVENTARGS_T83896D84C96C5BC0478ACF7E99C7703D464BE11F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.DataErrorsChangedEventArgs
struct  DataErrorsChangedEventArgs_t83896D84C96C5BC0478ACF7E99C7703D464BE11F  : public EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E
{
public:
	// System.String System.ComponentModel.DataErrorsChangedEventArgs::propertyName
	String_t* ___propertyName_1;

public:
	inline static int32_t get_offset_of_propertyName_1() { return static_cast<int32_t>(offsetof(DataErrorsChangedEventArgs_t83896D84C96C5BC0478ACF7E99C7703D464BE11F, ___propertyName_1)); }
	inline String_t* get_propertyName_1() const { return ___propertyName_1; }
	inline String_t** get_address_of_propertyName_1() { return &___propertyName_1; }
	inline void set_propertyName_1(String_t* value)
	{
		___propertyName_1 = value;
		Il2CppCodeGenWriteBarrier((&___propertyName_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATAERRORSCHANGEDEVENTARGS_T83896D84C96C5BC0478ACF7E99C7703D464BE11F_H
#ifndef DATAOBJECTATTRIBUTE_T831D53B857103EE776C8F8EBB60D27BFB96CC441_H
#define DATAOBJECTATTRIBUTE_T831D53B857103EE776C8F8EBB60D27BFB96CC441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.DataObjectAttribute
struct  DataObjectAttribute_t831D53B857103EE776C8F8EBB60D27BFB96CC441  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Boolean System.ComponentModel.DataObjectAttribute::_isDataObject
	bool ____isDataObject_3;

public:
	inline static int32_t get_offset_of__isDataObject_3() { return static_cast<int32_t>(offsetof(DataObjectAttribute_t831D53B857103EE776C8F8EBB60D27BFB96CC441, ____isDataObject_3)); }
	inline bool get__isDataObject_3() const { return ____isDataObject_3; }
	inline bool* get_address_of__isDataObject_3() { return &____isDataObject_3; }
	inline void set__isDataObject_3(bool value)
	{
		____isDataObject_3 = value;
	}
};

struct DataObjectAttribute_t831D53B857103EE776C8F8EBB60D27BFB96CC441_StaticFields
{
public:
	// System.ComponentModel.DataObjectAttribute System.ComponentModel.DataObjectAttribute::DataObject
	DataObjectAttribute_t831D53B857103EE776C8F8EBB60D27BFB96CC441 * ___DataObject_0;
	// System.ComponentModel.DataObjectAttribute System.ComponentModel.DataObjectAttribute::NonDataObject
	DataObjectAttribute_t831D53B857103EE776C8F8EBB60D27BFB96CC441 * ___NonDataObject_1;
	// System.ComponentModel.DataObjectAttribute System.ComponentModel.DataObjectAttribute::Default
	DataObjectAttribute_t831D53B857103EE776C8F8EBB60D27BFB96CC441 * ___Default_2;

public:
	inline static int32_t get_offset_of_DataObject_0() { return static_cast<int32_t>(offsetof(DataObjectAttribute_t831D53B857103EE776C8F8EBB60D27BFB96CC441_StaticFields, ___DataObject_0)); }
	inline DataObjectAttribute_t831D53B857103EE776C8F8EBB60D27BFB96CC441 * get_DataObject_0() const { return ___DataObject_0; }
	inline DataObjectAttribute_t831D53B857103EE776C8F8EBB60D27BFB96CC441 ** get_address_of_DataObject_0() { return &___DataObject_0; }
	inline void set_DataObject_0(DataObjectAttribute_t831D53B857103EE776C8F8EBB60D27BFB96CC441 * value)
	{
		___DataObject_0 = value;
		Il2CppCodeGenWriteBarrier((&___DataObject_0), value);
	}

	inline static int32_t get_offset_of_NonDataObject_1() { return static_cast<int32_t>(offsetof(DataObjectAttribute_t831D53B857103EE776C8F8EBB60D27BFB96CC441_StaticFields, ___NonDataObject_1)); }
	inline DataObjectAttribute_t831D53B857103EE776C8F8EBB60D27BFB96CC441 * get_NonDataObject_1() const { return ___NonDataObject_1; }
	inline DataObjectAttribute_t831D53B857103EE776C8F8EBB60D27BFB96CC441 ** get_address_of_NonDataObject_1() { return &___NonDataObject_1; }
	inline void set_NonDataObject_1(DataObjectAttribute_t831D53B857103EE776C8F8EBB60D27BFB96CC441 * value)
	{
		___NonDataObject_1 = value;
		Il2CppCodeGenWriteBarrier((&___NonDataObject_1), value);
	}

	inline static int32_t get_offset_of_Default_2() { return static_cast<int32_t>(offsetof(DataObjectAttribute_t831D53B857103EE776C8F8EBB60D27BFB96CC441_StaticFields, ___Default_2)); }
	inline DataObjectAttribute_t831D53B857103EE776C8F8EBB60D27BFB96CC441 * get_Default_2() const { return ___Default_2; }
	inline DataObjectAttribute_t831D53B857103EE776C8F8EBB60D27BFB96CC441 ** get_address_of_Default_2() { return &___Default_2; }
	inline void set_Default_2(DataObjectAttribute_t831D53B857103EE776C8F8EBB60D27BFB96CC441 * value)
	{
		___Default_2 = value;
		Il2CppCodeGenWriteBarrier((&___Default_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATAOBJECTATTRIBUTE_T831D53B857103EE776C8F8EBB60D27BFB96CC441_H
#ifndef DATAOBJECTFIELDATTRIBUTE_T62CD1DDC49C79543C2A0AD51F56ED922420F83DB_H
#define DATAOBJECTFIELDATTRIBUTE_T62CD1DDC49C79543C2A0AD51F56ED922420F83DB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.DataObjectFieldAttribute
struct  DataObjectFieldAttribute_t62CD1DDC49C79543C2A0AD51F56ED922420F83DB  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Boolean System.ComponentModel.DataObjectFieldAttribute::_primaryKey
	bool ____primaryKey_0;
	// System.Boolean System.ComponentModel.DataObjectFieldAttribute::_isIdentity
	bool ____isIdentity_1;
	// System.Boolean System.ComponentModel.DataObjectFieldAttribute::_isNullable
	bool ____isNullable_2;
	// System.Int32 System.ComponentModel.DataObjectFieldAttribute::_length
	int32_t ____length_3;

public:
	inline static int32_t get_offset_of__primaryKey_0() { return static_cast<int32_t>(offsetof(DataObjectFieldAttribute_t62CD1DDC49C79543C2A0AD51F56ED922420F83DB, ____primaryKey_0)); }
	inline bool get__primaryKey_0() const { return ____primaryKey_0; }
	inline bool* get_address_of__primaryKey_0() { return &____primaryKey_0; }
	inline void set__primaryKey_0(bool value)
	{
		____primaryKey_0 = value;
	}

	inline static int32_t get_offset_of__isIdentity_1() { return static_cast<int32_t>(offsetof(DataObjectFieldAttribute_t62CD1DDC49C79543C2A0AD51F56ED922420F83DB, ____isIdentity_1)); }
	inline bool get__isIdentity_1() const { return ____isIdentity_1; }
	inline bool* get_address_of__isIdentity_1() { return &____isIdentity_1; }
	inline void set__isIdentity_1(bool value)
	{
		____isIdentity_1 = value;
	}

	inline static int32_t get_offset_of__isNullable_2() { return static_cast<int32_t>(offsetof(DataObjectFieldAttribute_t62CD1DDC49C79543C2A0AD51F56ED922420F83DB, ____isNullable_2)); }
	inline bool get__isNullable_2() const { return ____isNullable_2; }
	inline bool* get_address_of__isNullable_2() { return &____isNullable_2; }
	inline void set__isNullable_2(bool value)
	{
		____isNullable_2 = value;
	}

	inline static int32_t get_offset_of__length_3() { return static_cast<int32_t>(offsetof(DataObjectFieldAttribute_t62CD1DDC49C79543C2A0AD51F56ED922420F83DB, ____length_3)); }
	inline int32_t get__length_3() const { return ____length_3; }
	inline int32_t* get_address_of__length_3() { return &____length_3; }
	inline void set__length_3(int32_t value)
	{
		____length_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATAOBJECTFIELDATTRIBUTE_T62CD1DDC49C79543C2A0AD51F56ED922420F83DB_H
#ifndef DEFAULTBINDINGPROPERTYATTRIBUTE_T12924F2E55ACA393FEE4AC9CB280FA7325D9F211_H
#define DEFAULTBINDINGPROPERTYATTRIBUTE_T12924F2E55ACA393FEE4AC9CB280FA7325D9F211_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.DefaultBindingPropertyAttribute
struct  DefaultBindingPropertyAttribute_t12924F2E55ACA393FEE4AC9CB280FA7325D9F211  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String System.ComponentModel.DefaultBindingPropertyAttribute::name
	String_t* ___name_0;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(DefaultBindingPropertyAttribute_t12924F2E55ACA393FEE4AC9CB280FA7325D9F211, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}
};

struct DefaultBindingPropertyAttribute_t12924F2E55ACA393FEE4AC9CB280FA7325D9F211_StaticFields
{
public:
	// System.ComponentModel.DefaultBindingPropertyAttribute System.ComponentModel.DefaultBindingPropertyAttribute::Default
	DefaultBindingPropertyAttribute_t12924F2E55ACA393FEE4AC9CB280FA7325D9F211 * ___Default_1;

public:
	inline static int32_t get_offset_of_Default_1() { return static_cast<int32_t>(offsetof(DefaultBindingPropertyAttribute_t12924F2E55ACA393FEE4AC9CB280FA7325D9F211_StaticFields, ___Default_1)); }
	inline DefaultBindingPropertyAttribute_t12924F2E55ACA393FEE4AC9CB280FA7325D9F211 * get_Default_1() const { return ___Default_1; }
	inline DefaultBindingPropertyAttribute_t12924F2E55ACA393FEE4AC9CB280FA7325D9F211 ** get_address_of_Default_1() { return &___Default_1; }
	inline void set_Default_1(DefaultBindingPropertyAttribute_t12924F2E55ACA393FEE4AC9CB280FA7325D9F211 * value)
	{
		___Default_1 = value;
		Il2CppCodeGenWriteBarrier((&___Default_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTBINDINGPROPERTYATTRIBUTE_T12924F2E55ACA393FEE4AC9CB280FA7325D9F211_H
#ifndef DEFAULTEVENTATTRIBUTE_TD1A10417C052CE865C43023F6DCC33CF54D3D846_H
#define DEFAULTEVENTATTRIBUTE_TD1A10417C052CE865C43023F6DCC33CF54D3D846_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.DefaultEventAttribute
struct  DefaultEventAttribute_tD1A10417C052CE865C43023F6DCC33CF54D3D846  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String System.ComponentModel.DefaultEventAttribute::name
	String_t* ___name_0;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(DefaultEventAttribute_tD1A10417C052CE865C43023F6DCC33CF54D3D846, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}
};

struct DefaultEventAttribute_tD1A10417C052CE865C43023F6DCC33CF54D3D846_StaticFields
{
public:
	// System.ComponentModel.DefaultEventAttribute System.ComponentModel.DefaultEventAttribute::Default
	DefaultEventAttribute_tD1A10417C052CE865C43023F6DCC33CF54D3D846 * ___Default_1;

public:
	inline static int32_t get_offset_of_Default_1() { return static_cast<int32_t>(offsetof(DefaultEventAttribute_tD1A10417C052CE865C43023F6DCC33CF54D3D846_StaticFields, ___Default_1)); }
	inline DefaultEventAttribute_tD1A10417C052CE865C43023F6DCC33CF54D3D846 * get_Default_1() const { return ___Default_1; }
	inline DefaultEventAttribute_tD1A10417C052CE865C43023F6DCC33CF54D3D846 ** get_address_of_Default_1() { return &___Default_1; }
	inline void set_Default_1(DefaultEventAttribute_tD1A10417C052CE865C43023F6DCC33CF54D3D846 * value)
	{
		___Default_1 = value;
		Il2CppCodeGenWriteBarrier((&___Default_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTEVENTATTRIBUTE_TD1A10417C052CE865C43023F6DCC33CF54D3D846_H
#ifndef DEFAULTPROPERTYATTRIBUTE_T4C049F2905F3ABDE9B9592627B6133AE49050AA7_H
#define DEFAULTPROPERTYATTRIBUTE_T4C049F2905F3ABDE9B9592627B6133AE49050AA7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.DefaultPropertyAttribute
struct  DefaultPropertyAttribute_t4C049F2905F3ABDE9B9592627B6133AE49050AA7  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String System.ComponentModel.DefaultPropertyAttribute::name
	String_t* ___name_0;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(DefaultPropertyAttribute_t4C049F2905F3ABDE9B9592627B6133AE49050AA7, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}
};

struct DefaultPropertyAttribute_t4C049F2905F3ABDE9B9592627B6133AE49050AA7_StaticFields
{
public:
	// System.ComponentModel.DefaultPropertyAttribute System.ComponentModel.DefaultPropertyAttribute::Default
	DefaultPropertyAttribute_t4C049F2905F3ABDE9B9592627B6133AE49050AA7 * ___Default_1;

public:
	inline static int32_t get_offset_of_Default_1() { return static_cast<int32_t>(offsetof(DefaultPropertyAttribute_t4C049F2905F3ABDE9B9592627B6133AE49050AA7_StaticFields, ___Default_1)); }
	inline DefaultPropertyAttribute_t4C049F2905F3ABDE9B9592627B6133AE49050AA7 * get_Default_1() const { return ___Default_1; }
	inline DefaultPropertyAttribute_t4C049F2905F3ABDE9B9592627B6133AE49050AA7 ** get_address_of_Default_1() { return &___Default_1; }
	inline void set_Default_1(DefaultPropertyAttribute_t4C049F2905F3ABDE9B9592627B6133AE49050AA7 * value)
	{
		___Default_1 = value;
		Il2CppCodeGenWriteBarrier((&___Default_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTPROPERTYATTRIBUTE_T4C049F2905F3ABDE9B9592627B6133AE49050AA7_H
#ifndef DEFAULTVALUEATTRIBUTE_T03B1F51B35271D50779D31234C9C6845BC9626EC_H
#define DEFAULTVALUEATTRIBUTE_T03B1F51B35271D50779D31234C9C6845BC9626EC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.DefaultValueAttribute
struct  DefaultValueAttribute_t03B1F51B35271D50779D31234C9C6845BC9626EC  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Object System.ComponentModel.DefaultValueAttribute::value
	RuntimeObject * ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(DefaultValueAttribute_t03B1F51B35271D50779D31234C9C6845BC9626EC, ___value_0)); }
	inline RuntimeObject * get_value_0() const { return ___value_0; }
	inline RuntimeObject ** get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(RuntimeObject * value)
	{
		___value_0 = value;
		Il2CppCodeGenWriteBarrier((&___value_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTVALUEATTRIBUTE_T03B1F51B35271D50779D31234C9C6845BC9626EC_H
#ifndef DELEGATINGTYPEDESCRIPTIONPROVIDER_T1479D886AEC7B0D4A74E19212E904337D5505A56_H
#define DELEGATINGTYPEDESCRIPTIONPROVIDER_T1479D886AEC7B0D4A74E19212E904337D5505A56_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.DelegatingTypeDescriptionProvider
struct  DelegatingTypeDescriptionProvider_t1479D886AEC7B0D4A74E19212E904337D5505A56  : public TypeDescriptionProvider_tE390829A953C44525366CA2A733E92642B97B591
{
public:
	// System.Type System.ComponentModel.DelegatingTypeDescriptionProvider::_type
	Type_t * ____type_2;

public:
	inline static int32_t get_offset_of__type_2() { return static_cast<int32_t>(offsetof(DelegatingTypeDescriptionProvider_t1479D886AEC7B0D4A74E19212E904337D5505A56, ____type_2)); }
	inline Type_t * get__type_2() const { return ____type_2; }
	inline Type_t ** get_address_of__type_2() { return &____type_2; }
	inline void set__type_2(Type_t * value)
	{
		____type_2 = value;
		Il2CppCodeGenWriteBarrier((&____type_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATINGTYPEDESCRIPTIONPROVIDER_T1479D886AEC7B0D4A74E19212E904337D5505A56_H
#ifndef DESCRIPTIONATTRIBUTE_T112C5FEAA03342D05BF40C1713ABF1C1848DEE75_H
#define DESCRIPTIONATTRIBUTE_T112C5FEAA03342D05BF40C1713ABF1C1848DEE75_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.DescriptionAttribute
struct  DescriptionAttribute_t112C5FEAA03342D05BF40C1713ABF1C1848DEE75  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String System.ComponentModel.DescriptionAttribute::description
	String_t* ___description_1;

public:
	inline static int32_t get_offset_of_description_1() { return static_cast<int32_t>(offsetof(DescriptionAttribute_t112C5FEAA03342D05BF40C1713ABF1C1848DEE75, ___description_1)); }
	inline String_t* get_description_1() const { return ___description_1; }
	inline String_t** get_address_of_description_1() { return &___description_1; }
	inline void set_description_1(String_t* value)
	{
		___description_1 = value;
		Il2CppCodeGenWriteBarrier((&___description_1), value);
	}
};

struct DescriptionAttribute_t112C5FEAA03342D05BF40C1713ABF1C1848DEE75_StaticFields
{
public:
	// System.ComponentModel.DescriptionAttribute System.ComponentModel.DescriptionAttribute::Default
	DescriptionAttribute_t112C5FEAA03342D05BF40C1713ABF1C1848DEE75 * ___Default_0;

public:
	inline static int32_t get_offset_of_Default_0() { return static_cast<int32_t>(offsetof(DescriptionAttribute_t112C5FEAA03342D05BF40C1713ABF1C1848DEE75_StaticFields, ___Default_0)); }
	inline DescriptionAttribute_t112C5FEAA03342D05BF40C1713ABF1C1848DEE75 * get_Default_0() const { return ___Default_0; }
	inline DescriptionAttribute_t112C5FEAA03342D05BF40C1713ABF1C1848DEE75 ** get_address_of_Default_0() { return &___Default_0; }
	inline void set_Default_0(DescriptionAttribute_t112C5FEAA03342D05BF40C1713ABF1C1848DEE75 * value)
	{
		___Default_0 = value;
		Il2CppCodeGenWriteBarrier((&___Default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESCRIPTIONATTRIBUTE_T112C5FEAA03342D05BF40C1713ABF1C1848DEE75_H
#ifndef DESIGNONLYATTRIBUTE_TF2F25F5BECE1C4E68B1408838F04C5E096169D06_H
#define DESIGNONLYATTRIBUTE_TF2F25F5BECE1C4E68B1408838F04C5E096169D06_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.DesignOnlyAttribute
struct  DesignOnlyAttribute_tF2F25F5BECE1C4E68B1408838F04C5E096169D06  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Boolean System.ComponentModel.DesignOnlyAttribute::isDesignOnly
	bool ___isDesignOnly_0;

public:
	inline static int32_t get_offset_of_isDesignOnly_0() { return static_cast<int32_t>(offsetof(DesignOnlyAttribute_tF2F25F5BECE1C4E68B1408838F04C5E096169D06, ___isDesignOnly_0)); }
	inline bool get_isDesignOnly_0() const { return ___isDesignOnly_0; }
	inline bool* get_address_of_isDesignOnly_0() { return &___isDesignOnly_0; }
	inline void set_isDesignOnly_0(bool value)
	{
		___isDesignOnly_0 = value;
	}
};

struct DesignOnlyAttribute_tF2F25F5BECE1C4E68B1408838F04C5E096169D06_StaticFields
{
public:
	// System.ComponentModel.DesignOnlyAttribute System.ComponentModel.DesignOnlyAttribute::Yes
	DesignOnlyAttribute_tF2F25F5BECE1C4E68B1408838F04C5E096169D06 * ___Yes_1;
	// System.ComponentModel.DesignOnlyAttribute System.ComponentModel.DesignOnlyAttribute::No
	DesignOnlyAttribute_tF2F25F5BECE1C4E68B1408838F04C5E096169D06 * ___No_2;
	// System.ComponentModel.DesignOnlyAttribute System.ComponentModel.DesignOnlyAttribute::Default
	DesignOnlyAttribute_tF2F25F5BECE1C4E68B1408838F04C5E096169D06 * ___Default_3;

public:
	inline static int32_t get_offset_of_Yes_1() { return static_cast<int32_t>(offsetof(DesignOnlyAttribute_tF2F25F5BECE1C4E68B1408838F04C5E096169D06_StaticFields, ___Yes_1)); }
	inline DesignOnlyAttribute_tF2F25F5BECE1C4E68B1408838F04C5E096169D06 * get_Yes_1() const { return ___Yes_1; }
	inline DesignOnlyAttribute_tF2F25F5BECE1C4E68B1408838F04C5E096169D06 ** get_address_of_Yes_1() { return &___Yes_1; }
	inline void set_Yes_1(DesignOnlyAttribute_tF2F25F5BECE1C4E68B1408838F04C5E096169D06 * value)
	{
		___Yes_1 = value;
		Il2CppCodeGenWriteBarrier((&___Yes_1), value);
	}

	inline static int32_t get_offset_of_No_2() { return static_cast<int32_t>(offsetof(DesignOnlyAttribute_tF2F25F5BECE1C4E68B1408838F04C5E096169D06_StaticFields, ___No_2)); }
	inline DesignOnlyAttribute_tF2F25F5BECE1C4E68B1408838F04C5E096169D06 * get_No_2() const { return ___No_2; }
	inline DesignOnlyAttribute_tF2F25F5BECE1C4E68B1408838F04C5E096169D06 ** get_address_of_No_2() { return &___No_2; }
	inline void set_No_2(DesignOnlyAttribute_tF2F25F5BECE1C4E68B1408838F04C5E096169D06 * value)
	{
		___No_2 = value;
		Il2CppCodeGenWriteBarrier((&___No_2), value);
	}

	inline static int32_t get_offset_of_Default_3() { return static_cast<int32_t>(offsetof(DesignOnlyAttribute_tF2F25F5BECE1C4E68B1408838F04C5E096169D06_StaticFields, ___Default_3)); }
	inline DesignOnlyAttribute_tF2F25F5BECE1C4E68B1408838F04C5E096169D06 * get_Default_3() const { return ___Default_3; }
	inline DesignOnlyAttribute_tF2F25F5BECE1C4E68B1408838F04C5E096169D06 ** get_address_of_Default_3() { return &___Default_3; }
	inline void set_Default_3(DesignOnlyAttribute_tF2F25F5BECE1C4E68B1408838F04C5E096169D06 * value)
	{
		___Default_3 = value;
		Il2CppCodeGenWriteBarrier((&___Default_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESIGNONLYATTRIBUTE_TF2F25F5BECE1C4E68B1408838F04C5E096169D06_H
#ifndef DESIGNTIMEVISIBLEATTRIBUTE_T4DD8962EAB8CFD8617D29303B7CFAD11E94B7466_H
#define DESIGNTIMEVISIBLEATTRIBUTE_T4DD8962EAB8CFD8617D29303B7CFAD11E94B7466_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.DesignTimeVisibleAttribute
struct  DesignTimeVisibleAttribute_t4DD8962EAB8CFD8617D29303B7CFAD11E94B7466  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Boolean System.ComponentModel.DesignTimeVisibleAttribute::visible
	bool ___visible_0;

public:
	inline static int32_t get_offset_of_visible_0() { return static_cast<int32_t>(offsetof(DesignTimeVisibleAttribute_t4DD8962EAB8CFD8617D29303B7CFAD11E94B7466, ___visible_0)); }
	inline bool get_visible_0() const { return ___visible_0; }
	inline bool* get_address_of_visible_0() { return &___visible_0; }
	inline void set_visible_0(bool value)
	{
		___visible_0 = value;
	}
};

struct DesignTimeVisibleAttribute_t4DD8962EAB8CFD8617D29303B7CFAD11E94B7466_StaticFields
{
public:
	// System.ComponentModel.DesignTimeVisibleAttribute System.ComponentModel.DesignTimeVisibleAttribute::Yes
	DesignTimeVisibleAttribute_t4DD8962EAB8CFD8617D29303B7CFAD11E94B7466 * ___Yes_1;
	// System.ComponentModel.DesignTimeVisibleAttribute System.ComponentModel.DesignTimeVisibleAttribute::No
	DesignTimeVisibleAttribute_t4DD8962EAB8CFD8617D29303B7CFAD11E94B7466 * ___No_2;
	// System.ComponentModel.DesignTimeVisibleAttribute System.ComponentModel.DesignTimeVisibleAttribute::Default
	DesignTimeVisibleAttribute_t4DD8962EAB8CFD8617D29303B7CFAD11E94B7466 * ___Default_3;

public:
	inline static int32_t get_offset_of_Yes_1() { return static_cast<int32_t>(offsetof(DesignTimeVisibleAttribute_t4DD8962EAB8CFD8617D29303B7CFAD11E94B7466_StaticFields, ___Yes_1)); }
	inline DesignTimeVisibleAttribute_t4DD8962EAB8CFD8617D29303B7CFAD11E94B7466 * get_Yes_1() const { return ___Yes_1; }
	inline DesignTimeVisibleAttribute_t4DD8962EAB8CFD8617D29303B7CFAD11E94B7466 ** get_address_of_Yes_1() { return &___Yes_1; }
	inline void set_Yes_1(DesignTimeVisibleAttribute_t4DD8962EAB8CFD8617D29303B7CFAD11E94B7466 * value)
	{
		___Yes_1 = value;
		Il2CppCodeGenWriteBarrier((&___Yes_1), value);
	}

	inline static int32_t get_offset_of_No_2() { return static_cast<int32_t>(offsetof(DesignTimeVisibleAttribute_t4DD8962EAB8CFD8617D29303B7CFAD11E94B7466_StaticFields, ___No_2)); }
	inline DesignTimeVisibleAttribute_t4DD8962EAB8CFD8617D29303B7CFAD11E94B7466 * get_No_2() const { return ___No_2; }
	inline DesignTimeVisibleAttribute_t4DD8962EAB8CFD8617D29303B7CFAD11E94B7466 ** get_address_of_No_2() { return &___No_2; }
	inline void set_No_2(DesignTimeVisibleAttribute_t4DD8962EAB8CFD8617D29303B7CFAD11E94B7466 * value)
	{
		___No_2 = value;
		Il2CppCodeGenWriteBarrier((&___No_2), value);
	}

	inline static int32_t get_offset_of_Default_3() { return static_cast<int32_t>(offsetof(DesignTimeVisibleAttribute_t4DD8962EAB8CFD8617D29303B7CFAD11E94B7466_StaticFields, ___Default_3)); }
	inline DesignTimeVisibleAttribute_t4DD8962EAB8CFD8617D29303B7CFAD11E94B7466 * get_Default_3() const { return ___Default_3; }
	inline DesignTimeVisibleAttribute_t4DD8962EAB8CFD8617D29303B7CFAD11E94B7466 ** get_address_of_Default_3() { return &___Default_3; }
	inline void set_Default_3(DesignTimeVisibleAttribute_t4DD8962EAB8CFD8617D29303B7CFAD11E94B7466 * value)
	{
		___Default_3 = value;
		Il2CppCodeGenWriteBarrier((&___Default_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESIGNTIMEVISIBLEATTRIBUTE_T4DD8962EAB8CFD8617D29303B7CFAD11E94B7466_H
#ifndef DESIGNERATTRIBUTE_T55268910CFC6D82065C1A2F68D05DCD3858933D0_H
#define DESIGNERATTRIBUTE_T55268910CFC6D82065C1A2F68D05DCD3858933D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.DesignerAttribute
struct  DesignerAttribute_t55268910CFC6D82065C1A2F68D05DCD3858933D0  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String System.ComponentModel.DesignerAttribute::designerTypeName
	String_t* ___designerTypeName_0;
	// System.String System.ComponentModel.DesignerAttribute::designerBaseTypeName
	String_t* ___designerBaseTypeName_1;
	// System.String System.ComponentModel.DesignerAttribute::typeId
	String_t* ___typeId_2;

public:
	inline static int32_t get_offset_of_designerTypeName_0() { return static_cast<int32_t>(offsetof(DesignerAttribute_t55268910CFC6D82065C1A2F68D05DCD3858933D0, ___designerTypeName_0)); }
	inline String_t* get_designerTypeName_0() const { return ___designerTypeName_0; }
	inline String_t** get_address_of_designerTypeName_0() { return &___designerTypeName_0; }
	inline void set_designerTypeName_0(String_t* value)
	{
		___designerTypeName_0 = value;
		Il2CppCodeGenWriteBarrier((&___designerTypeName_0), value);
	}

	inline static int32_t get_offset_of_designerBaseTypeName_1() { return static_cast<int32_t>(offsetof(DesignerAttribute_t55268910CFC6D82065C1A2F68D05DCD3858933D0, ___designerBaseTypeName_1)); }
	inline String_t* get_designerBaseTypeName_1() const { return ___designerBaseTypeName_1; }
	inline String_t** get_address_of_designerBaseTypeName_1() { return &___designerBaseTypeName_1; }
	inline void set_designerBaseTypeName_1(String_t* value)
	{
		___designerBaseTypeName_1 = value;
		Il2CppCodeGenWriteBarrier((&___designerBaseTypeName_1), value);
	}

	inline static int32_t get_offset_of_typeId_2() { return static_cast<int32_t>(offsetof(DesignerAttribute_t55268910CFC6D82065C1A2F68D05DCD3858933D0, ___typeId_2)); }
	inline String_t* get_typeId_2() const { return ___typeId_2; }
	inline String_t** get_address_of_typeId_2() { return &___typeId_2; }
	inline void set_typeId_2(String_t* value)
	{
		___typeId_2 = value;
		Il2CppCodeGenWriteBarrier((&___typeId_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESIGNERATTRIBUTE_T55268910CFC6D82065C1A2F68D05DCD3858933D0_H
#ifndef PROPERTYDESCRIPTOR_TBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D_H
#define PROPERTYDESCRIPTOR_TBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.PropertyDescriptor
struct  PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D  : public MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8
{
public:
	// System.ComponentModel.TypeConverter System.ComponentModel.PropertyDescriptor::converter
	TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB * ___converter_12;
	// System.Collections.Hashtable System.ComponentModel.PropertyDescriptor::valueChangedHandlers
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___valueChangedHandlers_13;
	// System.Object[] System.ComponentModel.PropertyDescriptor::editors
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___editors_14;
	// System.Type[] System.ComponentModel.PropertyDescriptor::editorTypes
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___editorTypes_15;
	// System.Int32 System.ComponentModel.PropertyDescriptor::editorCount
	int32_t ___editorCount_16;

public:
	inline static int32_t get_offset_of_converter_12() { return static_cast<int32_t>(offsetof(PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D, ___converter_12)); }
	inline TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB * get_converter_12() const { return ___converter_12; }
	inline TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB ** get_address_of_converter_12() { return &___converter_12; }
	inline void set_converter_12(TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB * value)
	{
		___converter_12 = value;
		Il2CppCodeGenWriteBarrier((&___converter_12), value);
	}

	inline static int32_t get_offset_of_valueChangedHandlers_13() { return static_cast<int32_t>(offsetof(PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D, ___valueChangedHandlers_13)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_valueChangedHandlers_13() const { return ___valueChangedHandlers_13; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_valueChangedHandlers_13() { return &___valueChangedHandlers_13; }
	inline void set_valueChangedHandlers_13(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___valueChangedHandlers_13 = value;
		Il2CppCodeGenWriteBarrier((&___valueChangedHandlers_13), value);
	}

	inline static int32_t get_offset_of_editors_14() { return static_cast<int32_t>(offsetof(PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D, ___editors_14)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_editors_14() const { return ___editors_14; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_editors_14() { return &___editors_14; }
	inline void set_editors_14(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___editors_14 = value;
		Il2CppCodeGenWriteBarrier((&___editors_14), value);
	}

	inline static int32_t get_offset_of_editorTypes_15() { return static_cast<int32_t>(offsetof(PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D, ___editorTypes_15)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get_editorTypes_15() const { return ___editorTypes_15; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of_editorTypes_15() { return &___editorTypes_15; }
	inline void set_editorTypes_15(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		___editorTypes_15 = value;
		Il2CppCodeGenWriteBarrier((&___editorTypes_15), value);
	}

	inline static int32_t get_offset_of_editorCount_16() { return static_cast<int32_t>(offsetof(PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D, ___editorCount_16)); }
	inline int32_t get_editorCount_16() const { return ___editorCount_16; }
	inline int32_t* get_address_of_editorCount_16() { return &___editorCount_16; }
	inline void set_editorCount_16(int32_t value)
	{
		___editorCount_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYDESCRIPTOR_TBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D_H
#ifndef DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#define DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 
{
public:
	// System.UInt64 System.DateTime::dateData
	uint64_t ___dateData_44;

public:
	inline static int32_t get_offset_of_dateData_44() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132, ___dateData_44)); }
	inline uint64_t get_dateData_44() const { return ___dateData_44; }
	inline uint64_t* get_address_of_dateData_44() { return &___dateData_44; }
	inline void set_dateData_44(uint64_t value)
	{
		___dateData_44 = value;
	}
};

struct DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields
{
public:
	// System.Int32[] System.DateTime::DaysToMonth365
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth365_29;
	// System.Int32[] System.DateTime::DaysToMonth366
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth366_30;
	// System.DateTime System.DateTime::MinValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MinValue_31;
	// System.DateTime System.DateTime::MaxValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MaxValue_32;

public:
	inline static int32_t get_offset_of_DaysToMonth365_29() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth365_29)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth365_29() const { return ___DaysToMonth365_29; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth365_29() { return &___DaysToMonth365_29; }
	inline void set_DaysToMonth365_29(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth365_29 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth365_29), value);
	}

	inline static int32_t get_offset_of_DaysToMonth366_30() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth366_30)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth366_30() const { return ___DaysToMonth366_30; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth366_30() { return &___DaysToMonth366_30; }
	inline void set_DaysToMonth366_30(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth366_30 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth366_30), value);
	}

	inline static int32_t get_offset_of_MinValue_31() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MinValue_31)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MinValue_31() const { return ___MinValue_31; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MinValue_31() { return &___MinValue_31; }
	inline void set_MinValue_31(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MinValue_31 = value;
	}

	inline static int32_t get_offset_of_MaxValue_32() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MaxValue_32)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MaxValue_32() const { return ___MaxValue_32; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MaxValue_32() { return &___MaxValue_32; }
	inline void set_MaxValue_32(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MaxValue_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifndef BOOLEANSWITCH_T2B51F0E7C50E7DAA74282B51EF9107A2E54723E0_H
#define BOOLEANSWITCH_T2B51F0E7C50E7DAA74282B51EF9107A2E54723E0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.BooleanSwitch
struct  BooleanSwitch_t2B51F0E7C50E7DAA74282B51EF9107A2E54723E0  : public Switch_t9DAA0B271A8AAEC0A2EFE1EE30299CECB4C2002F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEANSWITCH_T2B51F0E7C50E7DAA74282B51EF9107A2E54723E0_H
#ifndef EXCLUDEFROMCODECOVERAGEATTRIBUTE_TC52BD26DD63AE2AF8B3E1C22E5281B3EB0ACF867_H
#define EXCLUDEFROMCODECOVERAGEATTRIBUTE_TC52BD26DD63AE2AF8B3E1C22E5281B3EB0ACF867_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute
struct  ExcludeFromCodeCoverageAttribute_tC52BD26DD63AE2AF8B3E1C22E5281B3EB0ACF867  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCLUDEFROMCODECOVERAGEATTRIBUTE_TC52BD26DD63AE2AF8B3E1C22E5281B3EB0ACF867_H
#ifndef PROCESSMODULECOLLECTION_T93E76B9948E84325744E8C57A525FD465D78DB3F_H
#define PROCESSMODULECOLLECTION_T93E76B9948E84325744E8C57A525FD465D78DB3F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.ProcessModuleCollection
struct  ProcessModuleCollection_t93E76B9948E84325744E8C57A525FD465D78DB3F  : public ReadOnlyCollectionBase_tFD695167917CE6DF4FA18A906FA530880B9B8772
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROCESSMODULECOLLECTION_T93E76B9948E84325744E8C57A525FD465D78DB3F_H
#ifndef PROCESSTHREADCOLLECTION_T6D1D2E676ED1F65087080729F91410724CA74DA7_H
#define PROCESSTHREADCOLLECTION_T6D1D2E676ED1F65087080729F91410724CA74DA7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.ProcessThreadCollection
struct  ProcessThreadCollection_t6D1D2E676ED1F65087080729F91410724CA74DA7  : public ReadOnlyCollectionBase_tFD695167917CE6DF4FA18A906FA530880B9B8772
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROCESSTHREADCOLLECTION_T6D1D2E676ED1F65087080729F91410724CA74DA7_H
#ifndef SWITCHLEVELATTRIBUTE_T5B9AF957556A203ACC71409CA1A08B4DE3D3EAA6_H
#define SWITCHLEVELATTRIBUTE_T5B9AF957556A203ACC71409CA1A08B4DE3D3EAA6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.SwitchLevelAttribute
struct  SwitchLevelAttribute_t5B9AF957556A203ACC71409CA1A08B4DE3D3EAA6  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Type System.Diagnostics.SwitchLevelAttribute::type
	Type_t * ___type_0;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(SwitchLevelAttribute_t5B9AF957556A203ACC71409CA1A08B4DE3D3EAA6, ___type_0)); }
	inline Type_t * get_type_0() const { return ___type_0; }
	inline Type_t ** get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(Type_t * value)
	{
		___type_0 = value;
		Il2CppCodeGenWriteBarrier((&___type_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWITCHLEVELATTRIBUTE_T5B9AF957556A203ACC71409CA1A08B4DE3D3EAA6_H
#ifndef TRACESWITCH_T32D210D5C9B05D9E555925260EEC3767BA895EC8_H
#define TRACESWITCH_T32D210D5C9B05D9E555925260EEC3767BA895EC8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.TraceSwitch
struct  TraceSwitch_t32D210D5C9B05D9E555925260EEC3767BA895EC8  : public Switch_t9DAA0B271A8AAEC0A2EFE1EE30299CECB4C2002F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACESWITCH_T32D210D5C9B05D9E555925260EEC3767BA895EC8_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#define INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#define SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#ifndef GROUP_TB4759D0385925B2C8C14ED3FCD5D2F43CFBD0443_H
#define GROUP_TB4759D0385925B2C8C14ED3FCD5D2F43CFBD0443_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Group
struct  Group_tB4759D0385925B2C8C14ED3FCD5D2F43CFBD0443  : public Capture_tF4475248CCF3EFF914844BE2C993FC609D41DB73
{
public:
	// System.Int32[] System.Text.RegularExpressions.Group::_caps
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ____caps_4;
	// System.Int32 System.Text.RegularExpressions.Group::_capcount
	int32_t ____capcount_5;
	// System.String System.Text.RegularExpressions.Group::_name
	String_t* ____name_6;

public:
	inline static int32_t get_offset_of__caps_4() { return static_cast<int32_t>(offsetof(Group_tB4759D0385925B2C8C14ED3FCD5D2F43CFBD0443, ____caps_4)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get__caps_4() const { return ____caps_4; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of__caps_4() { return &____caps_4; }
	inline void set__caps_4(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		____caps_4 = value;
		Il2CppCodeGenWriteBarrier((&____caps_4), value);
	}

	inline static int32_t get_offset_of__capcount_5() { return static_cast<int32_t>(offsetof(Group_tB4759D0385925B2C8C14ED3FCD5D2F43CFBD0443, ____capcount_5)); }
	inline int32_t get__capcount_5() const { return ____capcount_5; }
	inline int32_t* get_address_of__capcount_5() { return &____capcount_5; }
	inline void set__capcount_5(int32_t value)
	{
		____capcount_5 = value;
	}

	inline static int32_t get_offset_of__name_6() { return static_cast<int32_t>(offsetof(Group_tB4759D0385925B2C8C14ED3FCD5D2F43CFBD0443, ____name_6)); }
	inline String_t* get__name_6() const { return ____name_6; }
	inline String_t** get_address_of__name_6() { return &____name_6; }
	inline void set__name_6(String_t* value)
	{
		____name_6 = value;
		Il2CppCodeGenWriteBarrier((&____name_6), value);
	}
};

struct Group_tB4759D0385925B2C8C14ED3FCD5D2F43CFBD0443_StaticFields
{
public:
	// System.Text.RegularExpressions.Group System.Text.RegularExpressions.Group::_emptygroup
	Group_tB4759D0385925B2C8C14ED3FCD5D2F43CFBD0443 * ____emptygroup_3;

public:
	inline static int32_t get_offset_of__emptygroup_3() { return static_cast<int32_t>(offsetof(Group_tB4759D0385925B2C8C14ED3FCD5D2F43CFBD0443_StaticFields, ____emptygroup_3)); }
	inline Group_tB4759D0385925B2C8C14ED3FCD5D2F43CFBD0443 * get__emptygroup_3() const { return ____emptygroup_3; }
	inline Group_tB4759D0385925B2C8C14ED3FCD5D2F43CFBD0443 ** get_address_of__emptygroup_3() { return &____emptygroup_3; }
	inline void set__emptygroup_3(Group_tB4759D0385925B2C8C14ED3FCD5D2F43CFBD0443 * value)
	{
		____emptygroup_3 = value;
		Il2CppCodeGenWriteBarrier((&____emptygroup_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GROUP_TB4759D0385925B2C8C14ED3FCD5D2F43CFBD0443_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef BACKGROUNDWORKER_T2E2BF26D42C991BCA6B8ECAF8830D0CA2D4DA13A_H
#define BACKGROUNDWORKER_T2E2BF26D42C991BCA6B8ECAF8830D0CA2D4DA13A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.BackgroundWorker
struct  BackgroundWorker_t2E2BF26D42C991BCA6B8ECAF8830D0CA2D4DA13A  : public Component_t7AEFE153F6778CF52E1981BC3E811A9604B29473
{
public:
	// System.Boolean System.ComponentModel.BackgroundWorker::canCancelWorker
	bool ___canCancelWorker_7;
	// System.Boolean System.ComponentModel.BackgroundWorker::workerReportsProgress
	bool ___workerReportsProgress_8;
	// System.Boolean System.ComponentModel.BackgroundWorker::cancellationPending
	bool ___cancellationPending_9;
	// System.Boolean System.ComponentModel.BackgroundWorker::isRunning
	bool ___isRunning_10;
	// System.ComponentModel.AsyncOperation System.ComponentModel.BackgroundWorker::asyncOperation
	AsyncOperation_tFA5B90911F8ABE8B07A3058301687DDDF5F78748 * ___asyncOperation_11;
	// System.ComponentModel.BackgroundWorker_WorkerThreadStartDelegate System.ComponentModel.BackgroundWorker::threadStart
	WorkerThreadStartDelegate_tEC42174597C1FB97F214AE61808E6F0CE75BF7B6 * ___threadStart_12;
	// System.Threading.SendOrPostCallback System.ComponentModel.BackgroundWorker::operationCompleted
	SendOrPostCallback_t3F9C0164860E4AA5138DF8B4488DFB0D33147F01 * ___operationCompleted_13;
	// System.Threading.SendOrPostCallback System.ComponentModel.BackgroundWorker::progressReporter
	SendOrPostCallback_t3F9C0164860E4AA5138DF8B4488DFB0D33147F01 * ___progressReporter_14;

public:
	inline static int32_t get_offset_of_canCancelWorker_7() { return static_cast<int32_t>(offsetof(BackgroundWorker_t2E2BF26D42C991BCA6B8ECAF8830D0CA2D4DA13A, ___canCancelWorker_7)); }
	inline bool get_canCancelWorker_7() const { return ___canCancelWorker_7; }
	inline bool* get_address_of_canCancelWorker_7() { return &___canCancelWorker_7; }
	inline void set_canCancelWorker_7(bool value)
	{
		___canCancelWorker_7 = value;
	}

	inline static int32_t get_offset_of_workerReportsProgress_8() { return static_cast<int32_t>(offsetof(BackgroundWorker_t2E2BF26D42C991BCA6B8ECAF8830D0CA2D4DA13A, ___workerReportsProgress_8)); }
	inline bool get_workerReportsProgress_8() const { return ___workerReportsProgress_8; }
	inline bool* get_address_of_workerReportsProgress_8() { return &___workerReportsProgress_8; }
	inline void set_workerReportsProgress_8(bool value)
	{
		___workerReportsProgress_8 = value;
	}

	inline static int32_t get_offset_of_cancellationPending_9() { return static_cast<int32_t>(offsetof(BackgroundWorker_t2E2BF26D42C991BCA6B8ECAF8830D0CA2D4DA13A, ___cancellationPending_9)); }
	inline bool get_cancellationPending_9() const { return ___cancellationPending_9; }
	inline bool* get_address_of_cancellationPending_9() { return &___cancellationPending_9; }
	inline void set_cancellationPending_9(bool value)
	{
		___cancellationPending_9 = value;
	}

	inline static int32_t get_offset_of_isRunning_10() { return static_cast<int32_t>(offsetof(BackgroundWorker_t2E2BF26D42C991BCA6B8ECAF8830D0CA2D4DA13A, ___isRunning_10)); }
	inline bool get_isRunning_10() const { return ___isRunning_10; }
	inline bool* get_address_of_isRunning_10() { return &___isRunning_10; }
	inline void set_isRunning_10(bool value)
	{
		___isRunning_10 = value;
	}

	inline static int32_t get_offset_of_asyncOperation_11() { return static_cast<int32_t>(offsetof(BackgroundWorker_t2E2BF26D42C991BCA6B8ECAF8830D0CA2D4DA13A, ___asyncOperation_11)); }
	inline AsyncOperation_tFA5B90911F8ABE8B07A3058301687DDDF5F78748 * get_asyncOperation_11() const { return ___asyncOperation_11; }
	inline AsyncOperation_tFA5B90911F8ABE8B07A3058301687DDDF5F78748 ** get_address_of_asyncOperation_11() { return &___asyncOperation_11; }
	inline void set_asyncOperation_11(AsyncOperation_tFA5B90911F8ABE8B07A3058301687DDDF5F78748 * value)
	{
		___asyncOperation_11 = value;
		Il2CppCodeGenWriteBarrier((&___asyncOperation_11), value);
	}

	inline static int32_t get_offset_of_threadStart_12() { return static_cast<int32_t>(offsetof(BackgroundWorker_t2E2BF26D42C991BCA6B8ECAF8830D0CA2D4DA13A, ___threadStart_12)); }
	inline WorkerThreadStartDelegate_tEC42174597C1FB97F214AE61808E6F0CE75BF7B6 * get_threadStart_12() const { return ___threadStart_12; }
	inline WorkerThreadStartDelegate_tEC42174597C1FB97F214AE61808E6F0CE75BF7B6 ** get_address_of_threadStart_12() { return &___threadStart_12; }
	inline void set_threadStart_12(WorkerThreadStartDelegate_tEC42174597C1FB97F214AE61808E6F0CE75BF7B6 * value)
	{
		___threadStart_12 = value;
		Il2CppCodeGenWriteBarrier((&___threadStart_12), value);
	}

	inline static int32_t get_offset_of_operationCompleted_13() { return static_cast<int32_t>(offsetof(BackgroundWorker_t2E2BF26D42C991BCA6B8ECAF8830D0CA2D4DA13A, ___operationCompleted_13)); }
	inline SendOrPostCallback_t3F9C0164860E4AA5138DF8B4488DFB0D33147F01 * get_operationCompleted_13() const { return ___operationCompleted_13; }
	inline SendOrPostCallback_t3F9C0164860E4AA5138DF8B4488DFB0D33147F01 ** get_address_of_operationCompleted_13() { return &___operationCompleted_13; }
	inline void set_operationCompleted_13(SendOrPostCallback_t3F9C0164860E4AA5138DF8B4488DFB0D33147F01 * value)
	{
		___operationCompleted_13 = value;
		Il2CppCodeGenWriteBarrier((&___operationCompleted_13), value);
	}

	inline static int32_t get_offset_of_progressReporter_14() { return static_cast<int32_t>(offsetof(BackgroundWorker_t2E2BF26D42C991BCA6B8ECAF8830D0CA2D4DA13A, ___progressReporter_14)); }
	inline SendOrPostCallback_t3F9C0164860E4AA5138DF8B4488DFB0D33147F01 * get_progressReporter_14() const { return ___progressReporter_14; }
	inline SendOrPostCallback_t3F9C0164860E4AA5138DF8B4488DFB0D33147F01 ** get_address_of_progressReporter_14() { return &___progressReporter_14; }
	inline void set_progressReporter_14(SendOrPostCallback_t3F9C0164860E4AA5138DF8B4488DFB0D33147F01 * value)
	{
		___progressReporter_14 = value;
		Il2CppCodeGenWriteBarrier((&___progressReporter_14), value);
	}
};

struct BackgroundWorker_t2E2BF26D42C991BCA6B8ECAF8830D0CA2D4DA13A_StaticFields
{
public:
	// System.Object System.ComponentModel.BackgroundWorker::doWorkKey
	RuntimeObject * ___doWorkKey_4;
	// System.Object System.ComponentModel.BackgroundWorker::runWorkerCompletedKey
	RuntimeObject * ___runWorkerCompletedKey_5;
	// System.Object System.ComponentModel.BackgroundWorker::progressChangedKey
	RuntimeObject * ___progressChangedKey_6;

public:
	inline static int32_t get_offset_of_doWorkKey_4() { return static_cast<int32_t>(offsetof(BackgroundWorker_t2E2BF26D42C991BCA6B8ECAF8830D0CA2D4DA13A_StaticFields, ___doWorkKey_4)); }
	inline RuntimeObject * get_doWorkKey_4() const { return ___doWorkKey_4; }
	inline RuntimeObject ** get_address_of_doWorkKey_4() { return &___doWorkKey_4; }
	inline void set_doWorkKey_4(RuntimeObject * value)
	{
		___doWorkKey_4 = value;
		Il2CppCodeGenWriteBarrier((&___doWorkKey_4), value);
	}

	inline static int32_t get_offset_of_runWorkerCompletedKey_5() { return static_cast<int32_t>(offsetof(BackgroundWorker_t2E2BF26D42C991BCA6B8ECAF8830D0CA2D4DA13A_StaticFields, ___runWorkerCompletedKey_5)); }
	inline RuntimeObject * get_runWorkerCompletedKey_5() const { return ___runWorkerCompletedKey_5; }
	inline RuntimeObject ** get_address_of_runWorkerCompletedKey_5() { return &___runWorkerCompletedKey_5; }
	inline void set_runWorkerCompletedKey_5(RuntimeObject * value)
	{
		___runWorkerCompletedKey_5 = value;
		Il2CppCodeGenWriteBarrier((&___runWorkerCompletedKey_5), value);
	}

	inline static int32_t get_offset_of_progressChangedKey_6() { return static_cast<int32_t>(offsetof(BackgroundWorker_t2E2BF26D42C991BCA6B8ECAF8830D0CA2D4DA13A_StaticFields, ___progressChangedKey_6)); }
	inline RuntimeObject * get_progressChangedKey_6() const { return ___progressChangedKey_6; }
	inline RuntimeObject ** get_address_of_progressChangedKey_6() { return &___progressChangedKey_6; }
	inline void set_progressChangedKey_6(RuntimeObject * value)
	{
		___progressChangedKey_6 = value;
		Il2CppCodeGenWriteBarrier((&___progressChangedKey_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BACKGROUNDWORKER_T2E2BF26D42C991BCA6B8ECAF8830D0CA2D4DA13A_H
#ifndef BINDABLESUPPORT_T08F85344F8C2B7F5F78146A953644FF6972D4E29_H
#define BINDABLESUPPORT_T08F85344F8C2B7F5F78146A953644FF6972D4E29_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.BindableSupport
struct  BindableSupport_t08F85344F8C2B7F5F78146A953644FF6972D4E29 
{
public:
	// System.Int32 System.ComponentModel.BindableSupport::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindableSupport_t08F85344F8C2B7F5F78146A953644FF6972D4E29, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDABLESUPPORT_T08F85344F8C2B7F5F78146A953644FF6972D4E29_H
#ifndef BINDINGDIRECTION_T01A86D4887E469A94D1B5B7B55D82DD88A6725FF_H
#define BINDINGDIRECTION_T01A86D4887E469A94D1B5B7B55D82DD88A6725FF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.BindingDirection
struct  BindingDirection_t01A86D4887E469A94D1B5B7B55D82DD88A6725FF 
{
public:
	// System.Int32 System.ComponentModel.BindingDirection::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingDirection_t01A86D4887E469A94D1B5B7B55D82DD88A6725FF, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGDIRECTION_T01A86D4887E469A94D1B5B7B55D82DD88A6725FF_H
#ifndef COLLECTIONCHANGEACTION_TEF925321EBE6F00D384F1E5D046E601F7190043D_H
#define COLLECTIONCHANGEACTION_TEF925321EBE6F00D384F1E5D046E601F7190043D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.CollectionChangeAction
struct  CollectionChangeAction_tEF925321EBE6F00D384F1E5D046E601F7190043D 
{
public:
	// System.Int32 System.ComponentModel.CollectionChangeAction::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CollectionChangeAction_tEF925321EBE6F00D384F1E5D046E601F7190043D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTIONCHANGEACTION_TEF925321EBE6F00D384F1E5D046E601F7190043D_H
#ifndef DATAOBJECTMETHODTYPE_T0B00930F4E899CF8E4709594FF8D0F171CF4E0D0_H
#define DATAOBJECTMETHODTYPE_T0B00930F4E899CF8E4709594FF8D0F171CF4E0D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.DataObjectMethodType
struct  DataObjectMethodType_t0B00930F4E899CF8E4709594FF8D0F171CF4E0D0 
{
public:
	// System.Int32 System.ComponentModel.DataObjectMethodType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DataObjectMethodType_t0B00930F4E899CF8E4709594FF8D0F171CF4E0D0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATAOBJECTMETHODTYPE_T0B00930F4E899CF8E4709594FF8D0F171CF4E0D0_H
#ifndef TYPECONVERTER_T8306AE03734853B551DDF089C1F17836A7764DBB_H
#define TYPECONVERTER_T8306AE03734853B551DDF089C1F17836A7764DBB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.TypeConverter
struct  TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB  : public RuntimeObject
{
public:

public:
};

struct TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB_StaticFields
{
public:
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.ComponentModel.TypeConverter::useCompatibleTypeConversion
	bool ___useCompatibleTypeConversion_1;

public:
	inline static int32_t get_offset_of_useCompatibleTypeConversion_1() { return static_cast<int32_t>(offsetof(TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB_StaticFields, ___useCompatibleTypeConversion_1)); }
	inline bool get_useCompatibleTypeConversion_1() const { return ___useCompatibleTypeConversion_1; }
	inline bool* get_address_of_useCompatibleTypeConversion_1() { return &___useCompatibleTypeConversion_1; }
	inline void set_useCompatibleTypeConversion_1(bool value)
	{
		___useCompatibleTypeConversion_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPECONVERTER_T8306AE03734853B551DDF089C1F17836A7764DBB_H
#ifndef SIMPLEPROPERTYDESCRIPTOR_TCF596E6470E66A92F1E91E28AB918097701E7CEA_H
#define SIMPLEPROPERTYDESCRIPTOR_TCF596E6470E66A92F1E91E28AB918097701E7CEA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.TypeConverter_SimplePropertyDescriptor
struct  SimplePropertyDescriptor_tCF596E6470E66A92F1E91E28AB918097701E7CEA  : public PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D
{
public:
	// System.Type System.ComponentModel.TypeConverter_SimplePropertyDescriptor::componentType
	Type_t * ___componentType_17;
	// System.Type System.ComponentModel.TypeConverter_SimplePropertyDescriptor::propertyType
	Type_t * ___propertyType_18;

public:
	inline static int32_t get_offset_of_componentType_17() { return static_cast<int32_t>(offsetof(SimplePropertyDescriptor_tCF596E6470E66A92F1E91E28AB918097701E7CEA, ___componentType_17)); }
	inline Type_t * get_componentType_17() const { return ___componentType_17; }
	inline Type_t ** get_address_of_componentType_17() { return &___componentType_17; }
	inline void set_componentType_17(Type_t * value)
	{
		___componentType_17 = value;
		Il2CppCodeGenWriteBarrier((&___componentType_17), value);
	}

	inline static int32_t get_offset_of_propertyType_18() { return static_cast<int32_t>(offsetof(SimplePropertyDescriptor_tCF596E6470E66A92F1E91E28AB918097701E7CEA, ___propertyType_18)); }
	inline Type_t * get_propertyType_18() const { return ___propertyType_18; }
	inline Type_t ** get_address_of_propertyType_18() { return &___propertyType_18; }
	inline void set_propertyType_18(Type_t * value)
	{
		___propertyType_18 = value;
		Il2CppCodeGenWriteBarrier((&___propertyType_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLEPROPERTYDESCRIPTOR_TCF596E6470E66A92F1E91E28AB918097701E7CEA_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef MONITORINGDESCRIPTIONATTRIBUTE_T8DA43522AE8E1B65A028830FCEB729567997069C_H
#define MONITORINGDESCRIPTIONATTRIBUTE_T8DA43522AE8E1B65A028830FCEB729567997069C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.MonitoringDescriptionAttribute
struct  MonitoringDescriptionAttribute_t8DA43522AE8E1B65A028830FCEB729567997069C  : public DescriptionAttribute_t112C5FEAA03342D05BF40C1713ABF1C1848DEE75
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONITORINGDESCRIPTIONATTRIBUTE_T8DA43522AE8E1B65A028830FCEB729567997069C_H
#ifndef STATE_T8FD6C2E8B4611EF5CB660B3E19DA19E940488826_H
#define STATE_T8FD6C2E8B4611EF5CB660B3E19DA19E940488826_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.Process_State
struct  State_t8FD6C2E8B4611EF5CB660B3E19DA19E940488826 
{
public:
	// System.Int32 System.Diagnostics.Process_State::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(State_t8FD6C2E8B4611EF5CB660B3E19DA19E940488826, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATE_T8FD6C2E8B4611EF5CB660B3E19DA19E940488826_H
#ifndef STREAMREADMODE_T36A3F7301F96A513D4D1C0BFC8A814952F52C3CA_H
#define STREAMREADMODE_T36A3F7301F96A513D4D1C0BFC8A814952F52C3CA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.Process_StreamReadMode
struct  StreamReadMode_t36A3F7301F96A513D4D1C0BFC8A814952F52C3CA 
{
public:
	// System.Int32 System.Diagnostics.Process_StreamReadMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StreamReadMode_t36A3F7301F96A513D4D1C0BFC8A814952F52C3CA, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAMREADMODE_T36A3F7301F96A513D4D1C0BFC8A814952F52C3CA_H
#ifndef TRACEEVENTCACHE_T69391771C5AACB85654A0AA5FBE8C86C45DA378D_H
#define TRACEEVENTCACHE_T69391771C5AACB85654A0AA5FBE8C86C45DA378D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.TraceEventCache
struct  TraceEventCache_t69391771C5AACB85654A0AA5FBE8C86C45DA378D  : public RuntimeObject
{
public:
	// System.Int64 System.Diagnostics.TraceEventCache::timeStamp
	int64_t ___timeStamp_2;
	// System.DateTime System.Diagnostics.TraceEventCache::dateTime
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___dateTime_3;
	// System.String System.Diagnostics.TraceEventCache::stackTrace
	String_t* ___stackTrace_4;

public:
	inline static int32_t get_offset_of_timeStamp_2() { return static_cast<int32_t>(offsetof(TraceEventCache_t69391771C5AACB85654A0AA5FBE8C86C45DA378D, ___timeStamp_2)); }
	inline int64_t get_timeStamp_2() const { return ___timeStamp_2; }
	inline int64_t* get_address_of_timeStamp_2() { return &___timeStamp_2; }
	inline void set_timeStamp_2(int64_t value)
	{
		___timeStamp_2 = value;
	}

	inline static int32_t get_offset_of_dateTime_3() { return static_cast<int32_t>(offsetof(TraceEventCache_t69391771C5AACB85654A0AA5FBE8C86C45DA378D, ___dateTime_3)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_dateTime_3() const { return ___dateTime_3; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_dateTime_3() { return &___dateTime_3; }
	inline void set_dateTime_3(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___dateTime_3 = value;
	}

	inline static int32_t get_offset_of_stackTrace_4() { return static_cast<int32_t>(offsetof(TraceEventCache_t69391771C5AACB85654A0AA5FBE8C86C45DA378D, ___stackTrace_4)); }
	inline String_t* get_stackTrace_4() const { return ___stackTrace_4; }
	inline String_t** get_address_of_stackTrace_4() { return &___stackTrace_4; }
	inline void set_stackTrace_4(String_t* value)
	{
		___stackTrace_4 = value;
		Il2CppCodeGenWriteBarrier((&___stackTrace_4), value);
	}
};

struct TraceEventCache_t69391771C5AACB85654A0AA5FBE8C86C45DA378D_StaticFields
{
public:
	// System.Int32 modreq(System.Runtime.CompilerServices.IsVolatile) System.Diagnostics.TraceEventCache::processId
	int32_t ___processId_0;
	// System.String modreq(System.Runtime.CompilerServices.IsVolatile) System.Diagnostics.TraceEventCache::processName
	String_t* ___processName_1;

public:
	inline static int32_t get_offset_of_processId_0() { return static_cast<int32_t>(offsetof(TraceEventCache_t69391771C5AACB85654A0AA5FBE8C86C45DA378D_StaticFields, ___processId_0)); }
	inline int32_t get_processId_0() const { return ___processId_0; }
	inline int32_t* get_address_of_processId_0() { return &___processId_0; }
	inline void set_processId_0(int32_t value)
	{
		___processId_0 = value;
	}

	inline static int32_t get_offset_of_processName_1() { return static_cast<int32_t>(offsetof(TraceEventCache_t69391771C5AACB85654A0AA5FBE8C86C45DA378D_StaticFields, ___processName_1)); }
	inline String_t* get_processName_1() const { return ___processName_1; }
	inline String_t** get_address_of_processName_1() { return &___processName_1; }
	inline void set_processName_1(String_t* value)
	{
		___processName_1 = value;
		Il2CppCodeGenWriteBarrier((&___processName_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACEEVENTCACHE_T69391771C5AACB85654A0AA5FBE8C86C45DA378D_H
#ifndef TRACEEVENTTYPE_TBFCE92C8BA1E8E4A82F4AF951EC8F62C7CA68E4A_H
#define TRACEEVENTTYPE_TBFCE92C8BA1E8E4A82F4AF951EC8F62C7CA68E4A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.TraceEventType
struct  TraceEventType_tBFCE92C8BA1E8E4A82F4AF951EC8F62C7CA68E4A 
{
public:
	// System.Int32 System.Diagnostics.TraceEventType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TraceEventType_tBFCE92C8BA1E8E4A82F4AF951EC8F62C7CA68E4A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACEEVENTTYPE_TBFCE92C8BA1E8E4A82F4AF951EC8F62C7CA68E4A_H
#ifndef TRACEINTERNAL_T2995A83FC0EF1D60E6D049F69A5A658536C92952_H
#define TRACEINTERNAL_T2995A83FC0EF1D60E6D049F69A5A658536C92952_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.TraceInternal
struct  TraceInternal_t2995A83FC0EF1D60E6D049F69A5A658536C92952  : public RuntimeObject
{
public:

public:
};

struct TraceInternal_t2995A83FC0EF1D60E6D049F69A5A658536C92952_StaticFields
{
public:
	// System.String modreq(System.Runtime.CompilerServices.IsVolatile) System.Diagnostics.TraceInternal::appName
	String_t* ___appName_0;
	// System.Diagnostics.TraceListenerCollection modreq(System.Runtime.CompilerServices.IsVolatile) System.Diagnostics.TraceInternal::listeners
	TraceListenerCollection_t392DC090EA67B680F4818E494FB5FE85AA82FBD6 * ___listeners_1;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.Diagnostics.TraceInternal::autoFlush
	bool ___autoFlush_2;
	// System.Int32 modreq(System.Runtime.CompilerServices.IsVolatile) System.Diagnostics.TraceInternal::indentSize
	int32_t ___indentSize_4;
	// System.Object System.Diagnostics.TraceInternal::critSec
	RuntimeObject * ___critSec_5;

public:
	inline static int32_t get_offset_of_appName_0() { return static_cast<int32_t>(offsetof(TraceInternal_t2995A83FC0EF1D60E6D049F69A5A658536C92952_StaticFields, ___appName_0)); }
	inline String_t* get_appName_0() const { return ___appName_0; }
	inline String_t** get_address_of_appName_0() { return &___appName_0; }
	inline void set_appName_0(String_t* value)
	{
		___appName_0 = value;
		Il2CppCodeGenWriteBarrier((&___appName_0), value);
	}

	inline static int32_t get_offset_of_listeners_1() { return static_cast<int32_t>(offsetof(TraceInternal_t2995A83FC0EF1D60E6D049F69A5A658536C92952_StaticFields, ___listeners_1)); }
	inline TraceListenerCollection_t392DC090EA67B680F4818E494FB5FE85AA82FBD6 * get_listeners_1() const { return ___listeners_1; }
	inline TraceListenerCollection_t392DC090EA67B680F4818E494FB5FE85AA82FBD6 ** get_address_of_listeners_1() { return &___listeners_1; }
	inline void set_listeners_1(TraceListenerCollection_t392DC090EA67B680F4818E494FB5FE85AA82FBD6 * value)
	{
		___listeners_1 = value;
		Il2CppCodeGenWriteBarrier((&___listeners_1), value);
	}

	inline static int32_t get_offset_of_autoFlush_2() { return static_cast<int32_t>(offsetof(TraceInternal_t2995A83FC0EF1D60E6D049F69A5A658536C92952_StaticFields, ___autoFlush_2)); }
	inline bool get_autoFlush_2() const { return ___autoFlush_2; }
	inline bool* get_address_of_autoFlush_2() { return &___autoFlush_2; }
	inline void set_autoFlush_2(bool value)
	{
		___autoFlush_2 = value;
	}

	inline static int32_t get_offset_of_indentSize_4() { return static_cast<int32_t>(offsetof(TraceInternal_t2995A83FC0EF1D60E6D049F69A5A658536C92952_StaticFields, ___indentSize_4)); }
	inline int32_t get_indentSize_4() const { return ___indentSize_4; }
	inline int32_t* get_address_of_indentSize_4() { return &___indentSize_4; }
	inline void set_indentSize_4(int32_t value)
	{
		___indentSize_4 = value;
	}

	inline static int32_t get_offset_of_critSec_5() { return static_cast<int32_t>(offsetof(TraceInternal_t2995A83FC0EF1D60E6D049F69A5A658536C92952_StaticFields, ___critSec_5)); }
	inline RuntimeObject * get_critSec_5() const { return ___critSec_5; }
	inline RuntimeObject ** get_address_of_critSec_5() { return &___critSec_5; }
	inline void set_critSec_5(RuntimeObject * value)
	{
		___critSec_5 = value;
		Il2CppCodeGenWriteBarrier((&___critSec_5), value);
	}
};

struct TraceInternal_t2995A83FC0EF1D60E6D049F69A5A658536C92952_ThreadStaticFields
{
public:
	// System.Int32 System.Diagnostics.TraceInternal::indentLevel
	int32_t ___indentLevel_3;

public:
	inline static int32_t get_offset_of_indentLevel_3() { return static_cast<int32_t>(offsetof(TraceInternal_t2995A83FC0EF1D60E6D049F69A5A658536C92952_ThreadStaticFields, ___indentLevel_3)); }
	inline int32_t get_indentLevel_3() const { return ___indentLevel_3; }
	inline int32_t* get_address_of_indentLevel_3() { return &___indentLevel_3; }
	inline void set_indentLevel_3(int32_t value)
	{
		___indentLevel_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACEINTERNAL_T2995A83FC0EF1D60E6D049F69A5A658536C92952_H
#ifndef TRACELEVEL_TF5EBC1CE1930BF1CD079855557C8CBD884D323B4_H
#define TRACELEVEL_TF5EBC1CE1930BF1CD079855557C8CBD884D323B4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.TraceLevel
struct  TraceLevel_tF5EBC1CE1930BF1CD079855557C8CBD884D323B4 
{
public:
	// System.Int32 System.Diagnostics.TraceLevel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TraceLevel_tF5EBC1CE1930BF1CD079855557C8CBD884D323B4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACELEVEL_TF5EBC1CE1930BF1CD079855557C8CBD884D323B4_H
#ifndef TRACEOPTIONS_TC2D35679E788FD9B8E3AE4FA8FA12EDBCC2EF411_H
#define TRACEOPTIONS_TC2D35679E788FD9B8E3AE4FA8FA12EDBCC2EF411_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.TraceOptions
struct  TraceOptions_tC2D35679E788FD9B8E3AE4FA8FA12EDBCC2EF411 
{
public:
	// System.Int32 System.Diagnostics.TraceOptions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TraceOptions_tC2D35679E788FD9B8E3AE4FA8FA12EDBCC2EF411, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACEOPTIONS_TC2D35679E788FD9B8E3AE4FA8FA12EDBCC2EF411_H
#ifndef ULTIMATERESOURCEFALLBACKLOCATION_T9E7495B2ADC328EB99FD80EDE68A2E5781D2882E_H
#define ULTIMATERESOURCEFALLBACKLOCATION_T9E7495B2ADC328EB99FD80EDE68A2E5781D2882E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Resources.UltimateResourceFallbackLocation
struct  UltimateResourceFallbackLocation_t9E7495B2ADC328EB99FD80EDE68A2E5781D2882E 
{
public:
	// System.Int32 System.Resources.UltimateResourceFallbackLocation::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UltimateResourceFallbackLocation_t9E7495B2ADC328EB99FD80EDE68A2E5781D2882E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ULTIMATERESOURCEFALLBACKLOCATION_T9E7495B2ADC328EB99FD80EDE68A2E5781D2882E_H
#ifndef MATCH_TE447871AB59EED3642F31EB9559D162C2977EBB5_H
#define MATCH_TE447871AB59EED3642F31EB9559D162C2977EBB5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Match
struct  Match_tE447871AB59EED3642F31EB9559D162C2977EBB5  : public Group_tB4759D0385925B2C8C14ED3FCD5D2F43CFBD0443
{
public:
	// System.Text.RegularExpressions.GroupCollection System.Text.RegularExpressions.Match::_groupcoll
	GroupCollection_tD9051ED1A991E3666439262B517FDD2F968C064F * ____groupcoll_8;
	// System.Text.RegularExpressions.Regex System.Text.RegularExpressions.Match::_regex
	Regex_tFD46E63A462E852189FD6AB4E2B0B67C4D8FDBDF * ____regex_9;
	// System.Int32 System.Text.RegularExpressions.Match::_textbeg
	int32_t ____textbeg_10;
	// System.Int32 System.Text.RegularExpressions.Match::_textpos
	int32_t ____textpos_11;
	// System.Int32 System.Text.RegularExpressions.Match::_textend
	int32_t ____textend_12;
	// System.Int32 System.Text.RegularExpressions.Match::_textstart
	int32_t ____textstart_13;
	// System.Int32[][] System.Text.RegularExpressions.Match::_matches
	Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* ____matches_14;
	// System.Int32[] System.Text.RegularExpressions.Match::_matchcount
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ____matchcount_15;
	// System.Boolean System.Text.RegularExpressions.Match::_balancing
	bool ____balancing_16;

public:
	inline static int32_t get_offset_of__groupcoll_8() { return static_cast<int32_t>(offsetof(Match_tE447871AB59EED3642F31EB9559D162C2977EBB5, ____groupcoll_8)); }
	inline GroupCollection_tD9051ED1A991E3666439262B517FDD2F968C064F * get__groupcoll_8() const { return ____groupcoll_8; }
	inline GroupCollection_tD9051ED1A991E3666439262B517FDD2F968C064F ** get_address_of__groupcoll_8() { return &____groupcoll_8; }
	inline void set__groupcoll_8(GroupCollection_tD9051ED1A991E3666439262B517FDD2F968C064F * value)
	{
		____groupcoll_8 = value;
		Il2CppCodeGenWriteBarrier((&____groupcoll_8), value);
	}

	inline static int32_t get_offset_of__regex_9() { return static_cast<int32_t>(offsetof(Match_tE447871AB59EED3642F31EB9559D162C2977EBB5, ____regex_9)); }
	inline Regex_tFD46E63A462E852189FD6AB4E2B0B67C4D8FDBDF * get__regex_9() const { return ____regex_9; }
	inline Regex_tFD46E63A462E852189FD6AB4E2B0B67C4D8FDBDF ** get_address_of__regex_9() { return &____regex_9; }
	inline void set__regex_9(Regex_tFD46E63A462E852189FD6AB4E2B0B67C4D8FDBDF * value)
	{
		____regex_9 = value;
		Il2CppCodeGenWriteBarrier((&____regex_9), value);
	}

	inline static int32_t get_offset_of__textbeg_10() { return static_cast<int32_t>(offsetof(Match_tE447871AB59EED3642F31EB9559D162C2977EBB5, ____textbeg_10)); }
	inline int32_t get__textbeg_10() const { return ____textbeg_10; }
	inline int32_t* get_address_of__textbeg_10() { return &____textbeg_10; }
	inline void set__textbeg_10(int32_t value)
	{
		____textbeg_10 = value;
	}

	inline static int32_t get_offset_of__textpos_11() { return static_cast<int32_t>(offsetof(Match_tE447871AB59EED3642F31EB9559D162C2977EBB5, ____textpos_11)); }
	inline int32_t get__textpos_11() const { return ____textpos_11; }
	inline int32_t* get_address_of__textpos_11() { return &____textpos_11; }
	inline void set__textpos_11(int32_t value)
	{
		____textpos_11 = value;
	}

	inline static int32_t get_offset_of__textend_12() { return static_cast<int32_t>(offsetof(Match_tE447871AB59EED3642F31EB9559D162C2977EBB5, ____textend_12)); }
	inline int32_t get__textend_12() const { return ____textend_12; }
	inline int32_t* get_address_of__textend_12() { return &____textend_12; }
	inline void set__textend_12(int32_t value)
	{
		____textend_12 = value;
	}

	inline static int32_t get_offset_of__textstart_13() { return static_cast<int32_t>(offsetof(Match_tE447871AB59EED3642F31EB9559D162C2977EBB5, ____textstart_13)); }
	inline int32_t get__textstart_13() const { return ____textstart_13; }
	inline int32_t* get_address_of__textstart_13() { return &____textstart_13; }
	inline void set__textstart_13(int32_t value)
	{
		____textstart_13 = value;
	}

	inline static int32_t get_offset_of__matches_14() { return static_cast<int32_t>(offsetof(Match_tE447871AB59EED3642F31EB9559D162C2977EBB5, ____matches_14)); }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* get__matches_14() const { return ____matches_14; }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43** get_address_of__matches_14() { return &____matches_14; }
	inline void set__matches_14(Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* value)
	{
		____matches_14 = value;
		Il2CppCodeGenWriteBarrier((&____matches_14), value);
	}

	inline static int32_t get_offset_of__matchcount_15() { return static_cast<int32_t>(offsetof(Match_tE447871AB59EED3642F31EB9559D162C2977EBB5, ____matchcount_15)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get__matchcount_15() const { return ____matchcount_15; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of__matchcount_15() { return &____matchcount_15; }
	inline void set__matchcount_15(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		____matchcount_15 = value;
		Il2CppCodeGenWriteBarrier((&____matchcount_15), value);
	}

	inline static int32_t get_offset_of__balancing_16() { return static_cast<int32_t>(offsetof(Match_tE447871AB59EED3642F31EB9559D162C2977EBB5, ____balancing_16)); }
	inline bool get__balancing_16() const { return ____balancing_16; }
	inline bool* get_address_of__balancing_16() { return &____balancing_16; }
	inline void set__balancing_16(bool value)
	{
		____balancing_16 = value;
	}
};

struct Match_tE447871AB59EED3642F31EB9559D162C2977EBB5_StaticFields
{
public:
	// System.Text.RegularExpressions.Match System.Text.RegularExpressions.Match::_empty
	Match_tE447871AB59EED3642F31EB9559D162C2977EBB5 * ____empty_7;

public:
	inline static int32_t get_offset_of__empty_7() { return static_cast<int32_t>(offsetof(Match_tE447871AB59EED3642F31EB9559D162C2977EBB5_StaticFields, ____empty_7)); }
	inline Match_tE447871AB59EED3642F31EB9559D162C2977EBB5 * get__empty_7() const { return ____empty_7; }
	inline Match_tE447871AB59EED3642F31EB9559D162C2977EBB5 ** get_address_of__empty_7() { return &____empty_7; }
	inline void set__empty_7(Match_tE447871AB59EED3642F31EB9559D162C2977EBB5 * value)
	{
		____empty_7 = value;
		Il2CppCodeGenWriteBarrier((&____empty_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATCH_TE447871AB59EED3642F31EB9559D162C2977EBB5_H
#ifndef REGEXOPTIONS_T9A6138CDA9C60924D503C584095349F008C52EA1_H
#define REGEXOPTIONS_T9A6138CDA9C60924D503C584095349F008C52EA1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexOptions
struct  RegexOptions_t9A6138CDA9C60924D503C584095349F008C52EA1 
{
public:
	// System.Int32 System.Text.RegularExpressions.RegexOptions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RegexOptions_t9A6138CDA9C60924D503C584095349F008C52EA1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGEXOPTIONS_T9A6138CDA9C60924D503C584095349F008C52EA1_H
#ifndef WAITHANDLE_TFD46B5B45A6BB296EA3A104C91DF2A7C03C10AC6_H
#define WAITHANDLE_TFD46B5B45A6BB296EA3A104C91DF2A7C03C10AC6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.WaitHandle
struct  WaitHandle_tFD46B5B45A6BB296EA3A104C91DF2A7C03C10AC6  : public MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF
{
public:
	// System.IntPtr System.Threading.WaitHandle::waitHandle
	intptr_t ___waitHandle_3;
	// Microsoft.Win32.SafeHandles.SafeWaitHandle modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.WaitHandle::safeWaitHandle
	SafeWaitHandle_t51DB35FF382E636FF3B868D87816733894D46CF2 * ___safeWaitHandle_4;
	// System.Boolean System.Threading.WaitHandle::hasThreadAffinity
	bool ___hasThreadAffinity_5;

public:
	inline static int32_t get_offset_of_waitHandle_3() { return static_cast<int32_t>(offsetof(WaitHandle_tFD46B5B45A6BB296EA3A104C91DF2A7C03C10AC6, ___waitHandle_3)); }
	inline intptr_t get_waitHandle_3() const { return ___waitHandle_3; }
	inline intptr_t* get_address_of_waitHandle_3() { return &___waitHandle_3; }
	inline void set_waitHandle_3(intptr_t value)
	{
		___waitHandle_3 = value;
	}

	inline static int32_t get_offset_of_safeWaitHandle_4() { return static_cast<int32_t>(offsetof(WaitHandle_tFD46B5B45A6BB296EA3A104C91DF2A7C03C10AC6, ___safeWaitHandle_4)); }
	inline SafeWaitHandle_t51DB35FF382E636FF3B868D87816733894D46CF2 * get_safeWaitHandle_4() const { return ___safeWaitHandle_4; }
	inline SafeWaitHandle_t51DB35FF382E636FF3B868D87816733894D46CF2 ** get_address_of_safeWaitHandle_4() { return &___safeWaitHandle_4; }
	inline void set_safeWaitHandle_4(SafeWaitHandle_t51DB35FF382E636FF3B868D87816733894D46CF2 * value)
	{
		___safeWaitHandle_4 = value;
		Il2CppCodeGenWriteBarrier((&___safeWaitHandle_4), value);
	}

	inline static int32_t get_offset_of_hasThreadAffinity_5() { return static_cast<int32_t>(offsetof(WaitHandle_tFD46B5B45A6BB296EA3A104C91DF2A7C03C10AC6, ___hasThreadAffinity_5)); }
	inline bool get_hasThreadAffinity_5() const { return ___hasThreadAffinity_5; }
	inline bool* get_address_of_hasThreadAffinity_5() { return &___hasThreadAffinity_5; }
	inline void set_hasThreadAffinity_5(bool value)
	{
		___hasThreadAffinity_5 = value;
	}
};

struct WaitHandle_tFD46B5B45A6BB296EA3A104C91DF2A7C03C10AC6_StaticFields
{
public:
	// System.IntPtr System.Threading.WaitHandle::InvalidHandle
	intptr_t ___InvalidHandle_10;

public:
	inline static int32_t get_offset_of_InvalidHandle_10() { return static_cast<int32_t>(offsetof(WaitHandle_tFD46B5B45A6BB296EA3A104C91DF2A7C03C10AC6_StaticFields, ___InvalidHandle_10)); }
	inline intptr_t get_InvalidHandle_10() const { return ___InvalidHandle_10; }
	inline intptr_t* get_address_of_InvalidHandle_10() { return &___InvalidHandle_10; }
	inline void set_InvalidHandle_10(intptr_t value)
	{
		___InvalidHandle_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Threading.WaitHandle
struct WaitHandle_tFD46B5B45A6BB296EA3A104C91DF2A7C03C10AC6_marshaled_pinvoke : public MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF_marshaled_pinvoke
{
	intptr_t ___waitHandle_3;
	void* ___safeWaitHandle_4;
	int32_t ___hasThreadAffinity_5;
};
// Native definition for COM marshalling of System.Threading.WaitHandle
struct WaitHandle_tFD46B5B45A6BB296EA3A104C91DF2A7C03C10AC6_marshaled_com : public MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF_marshaled_com
{
	intptr_t ___waitHandle_3;
	void* ___safeWaitHandle_4;
	int32_t ___hasThreadAffinity_5;
};
#endif // WAITHANDLE_TFD46B5B45A6BB296EA3A104C91DF2A7C03C10AC6_H
#ifndef TIMESPAN_TA8069278ACE8A74D6DF7D514A9CD4432433F64C4_H
#define TIMESPAN_TA8069278ACE8A74D6DF7D514A9CD4432433F64C4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_22;

public:
	inline static int32_t get_offset_of__ticks_22() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4, ____ticks_22)); }
	inline int64_t get__ticks_22() const { return ____ticks_22; }
	inline int64_t* get_address_of__ticks_22() { return &____ticks_22; }
	inline void set__ticks_22(int64_t value)
	{
		____ticks_22 = value;
	}
};

struct TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___Zero_19;
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___MaxValue_20;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___MinValue_21;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.TimeSpan::_legacyConfigChecked
	bool ____legacyConfigChecked_23;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.TimeSpan::_legacyMode
	bool ____legacyMode_24;

public:
	inline static int32_t get_offset_of_Zero_19() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ___Zero_19)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_Zero_19() const { return ___Zero_19; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_Zero_19() { return &___Zero_19; }
	inline void set_Zero_19(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___Zero_19 = value;
	}

	inline static int32_t get_offset_of_MaxValue_20() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ___MaxValue_20)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_MaxValue_20() const { return ___MaxValue_20; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_MaxValue_20() { return &___MaxValue_20; }
	inline void set_MaxValue_20(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___MaxValue_20 = value;
	}

	inline static int32_t get_offset_of_MinValue_21() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ___MinValue_21)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_MinValue_21() const { return ___MinValue_21; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_MinValue_21() { return &___MinValue_21; }
	inline void set_MinValue_21(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___MinValue_21 = value;
	}

	inline static int32_t get_offset_of__legacyConfigChecked_23() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ____legacyConfigChecked_23)); }
	inline bool get__legacyConfigChecked_23() const { return ____legacyConfigChecked_23; }
	inline bool* get_address_of__legacyConfigChecked_23() { return &____legacyConfigChecked_23; }
	inline void set__legacyConfigChecked_23(bool value)
	{
		____legacyConfigChecked_23 = value;
	}

	inline static int32_t get_offset_of__legacyMode_24() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ____legacyMode_24)); }
	inline bool get__legacyMode_24() const { return ____legacyMode_24; }
	inline bool* get_address_of__legacyMode_24() { return &____legacyMode_24; }
	inline void set__legacyMode_24(bool value)
	{
		____legacyMode_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_TA8069278ACE8A74D6DF7D514A9CD4432433F64C4_H
#ifndef TIMEOUTEXCEPTION_T15A6E9A2A5819966712B5CFAF756BAEA40E3B1B7_H
#define TIMEOUTEXCEPTION_T15A6E9A2A5819966712B5CFAF756BAEA40E3B1B7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeoutException
struct  TimeoutException_t15A6E9A2A5819966712B5CFAF756BAEA40E3B1B7  : public SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMEOUTEXCEPTION_T15A6E9A2A5819966712B5CFAF756BAEA40E3B1B7_H
#ifndef ARRAYPROPERTYDESCRIPTOR_T16F320E68277A32D9DFEBC21F76C2BCA279F10A2_H
#define ARRAYPROPERTYDESCRIPTOR_T16F320E68277A32D9DFEBC21F76C2BCA279F10A2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.ArrayConverter_ArrayPropertyDescriptor
struct  ArrayPropertyDescriptor_t16F320E68277A32D9DFEBC21F76C2BCA279F10A2  : public SimplePropertyDescriptor_tCF596E6470E66A92F1E91E28AB918097701E7CEA
{
public:
	// System.Int32 System.ComponentModel.ArrayConverter_ArrayPropertyDescriptor::index
	int32_t ___index_19;

public:
	inline static int32_t get_offset_of_index_19() { return static_cast<int32_t>(offsetof(ArrayPropertyDescriptor_t16F320E68277A32D9DFEBC21F76C2BCA279F10A2, ___index_19)); }
	inline int32_t get_index_19() const { return ___index_19; }
	inline int32_t* get_address_of_index_19() { return &___index_19; }
	inline void set_index_19(int32_t value)
	{
		___index_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRAYPROPERTYDESCRIPTOR_T16F320E68277A32D9DFEBC21F76C2BCA279F10A2_H
#ifndef BASENUMBERCONVERTER_T6AF36A2503E7BABF7FB9A8EC05DF8B828491AC63_H
#define BASENUMBERCONVERTER_T6AF36A2503E7BABF7FB9A8EC05DF8B828491AC63_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.BaseNumberConverter
struct  BaseNumberConverter_t6AF36A2503E7BABF7FB9A8EC05DF8B828491AC63  : public TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASENUMBERCONVERTER_T6AF36A2503E7BABF7FB9A8EC05DF8B828491AC63_H
#ifndef BINDABLEATTRIBUTE_TFDECD8DB181D7647712EFB507BA39E1B47B6108C_H
#define BINDABLEATTRIBUTE_TFDECD8DB181D7647712EFB507BA39E1B47B6108C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.BindableAttribute
struct  BindableAttribute_tFDECD8DB181D7647712EFB507BA39E1B47B6108C  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Boolean System.ComponentModel.BindableAttribute::bindable
	bool ___bindable_3;
	// System.Boolean System.ComponentModel.BindableAttribute::isDefault
	bool ___isDefault_4;
	// System.ComponentModel.BindingDirection System.ComponentModel.BindableAttribute::direction
	int32_t ___direction_5;

public:
	inline static int32_t get_offset_of_bindable_3() { return static_cast<int32_t>(offsetof(BindableAttribute_tFDECD8DB181D7647712EFB507BA39E1B47B6108C, ___bindable_3)); }
	inline bool get_bindable_3() const { return ___bindable_3; }
	inline bool* get_address_of_bindable_3() { return &___bindable_3; }
	inline void set_bindable_3(bool value)
	{
		___bindable_3 = value;
	}

	inline static int32_t get_offset_of_isDefault_4() { return static_cast<int32_t>(offsetof(BindableAttribute_tFDECD8DB181D7647712EFB507BA39E1B47B6108C, ___isDefault_4)); }
	inline bool get_isDefault_4() const { return ___isDefault_4; }
	inline bool* get_address_of_isDefault_4() { return &___isDefault_4; }
	inline void set_isDefault_4(bool value)
	{
		___isDefault_4 = value;
	}

	inline static int32_t get_offset_of_direction_5() { return static_cast<int32_t>(offsetof(BindableAttribute_tFDECD8DB181D7647712EFB507BA39E1B47B6108C, ___direction_5)); }
	inline int32_t get_direction_5() const { return ___direction_5; }
	inline int32_t* get_address_of_direction_5() { return &___direction_5; }
	inline void set_direction_5(int32_t value)
	{
		___direction_5 = value;
	}
};

struct BindableAttribute_tFDECD8DB181D7647712EFB507BA39E1B47B6108C_StaticFields
{
public:
	// System.ComponentModel.BindableAttribute System.ComponentModel.BindableAttribute::Yes
	BindableAttribute_tFDECD8DB181D7647712EFB507BA39E1B47B6108C * ___Yes_0;
	// System.ComponentModel.BindableAttribute System.ComponentModel.BindableAttribute::No
	BindableAttribute_tFDECD8DB181D7647712EFB507BA39E1B47B6108C * ___No_1;
	// System.ComponentModel.BindableAttribute System.ComponentModel.BindableAttribute::Default
	BindableAttribute_tFDECD8DB181D7647712EFB507BA39E1B47B6108C * ___Default_2;

public:
	inline static int32_t get_offset_of_Yes_0() { return static_cast<int32_t>(offsetof(BindableAttribute_tFDECD8DB181D7647712EFB507BA39E1B47B6108C_StaticFields, ___Yes_0)); }
	inline BindableAttribute_tFDECD8DB181D7647712EFB507BA39E1B47B6108C * get_Yes_0() const { return ___Yes_0; }
	inline BindableAttribute_tFDECD8DB181D7647712EFB507BA39E1B47B6108C ** get_address_of_Yes_0() { return &___Yes_0; }
	inline void set_Yes_0(BindableAttribute_tFDECD8DB181D7647712EFB507BA39E1B47B6108C * value)
	{
		___Yes_0 = value;
		Il2CppCodeGenWriteBarrier((&___Yes_0), value);
	}

	inline static int32_t get_offset_of_No_1() { return static_cast<int32_t>(offsetof(BindableAttribute_tFDECD8DB181D7647712EFB507BA39E1B47B6108C_StaticFields, ___No_1)); }
	inline BindableAttribute_tFDECD8DB181D7647712EFB507BA39E1B47B6108C * get_No_1() const { return ___No_1; }
	inline BindableAttribute_tFDECD8DB181D7647712EFB507BA39E1B47B6108C ** get_address_of_No_1() { return &___No_1; }
	inline void set_No_1(BindableAttribute_tFDECD8DB181D7647712EFB507BA39E1B47B6108C * value)
	{
		___No_1 = value;
		Il2CppCodeGenWriteBarrier((&___No_1), value);
	}

	inline static int32_t get_offset_of_Default_2() { return static_cast<int32_t>(offsetof(BindableAttribute_tFDECD8DB181D7647712EFB507BA39E1B47B6108C_StaticFields, ___Default_2)); }
	inline BindableAttribute_tFDECD8DB181D7647712EFB507BA39E1B47B6108C * get_Default_2() const { return ___Default_2; }
	inline BindableAttribute_tFDECD8DB181D7647712EFB507BA39E1B47B6108C ** get_address_of_Default_2() { return &___Default_2; }
	inline void set_Default_2(BindableAttribute_tFDECD8DB181D7647712EFB507BA39E1B47B6108C * value)
	{
		___Default_2 = value;
		Il2CppCodeGenWriteBarrier((&___Default_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDABLEATTRIBUTE_TFDECD8DB181D7647712EFB507BA39E1B47B6108C_H
#ifndef BOOLEANCONVERTER_TD0D177A9F577915FAA9F6749229672CE8EBAA94C_H
#define BOOLEANCONVERTER_TD0D177A9F577915FAA9F6749229672CE8EBAA94C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.BooleanConverter
struct  BooleanConverter_tD0D177A9F577915FAA9F6749229672CE8EBAA94C  : public TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB
{
public:

public:
};

struct BooleanConverter_tD0D177A9F577915FAA9F6749229672CE8EBAA94C_StaticFields
{
public:
	// System.ComponentModel.TypeConverter_StandardValuesCollection modreq(System.Runtime.CompilerServices.IsVolatile) System.ComponentModel.BooleanConverter::values
	StandardValuesCollection_t929677712574EF02F5C4CF4C38E070841C20BDA3 * ___values_2;

public:
	inline static int32_t get_offset_of_values_2() { return static_cast<int32_t>(offsetof(BooleanConverter_tD0D177A9F577915FAA9F6749229672CE8EBAA94C_StaticFields, ___values_2)); }
	inline StandardValuesCollection_t929677712574EF02F5C4CF4C38E070841C20BDA3 * get_values_2() const { return ___values_2; }
	inline StandardValuesCollection_t929677712574EF02F5C4CF4C38E070841C20BDA3 ** get_address_of_values_2() { return &___values_2; }
	inline void set_values_2(StandardValuesCollection_t929677712574EF02F5C4CF4C38E070841C20BDA3 * value)
	{
		___values_2 = value;
		Il2CppCodeGenWriteBarrier((&___values_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEANCONVERTER_TD0D177A9F577915FAA9F6749229672CE8EBAA94C_H
#ifndef CHARCONVERTER_TFD013540F3AFDBF6DB36FEE066025D778FAED95A_H
#define CHARCONVERTER_TFD013540F3AFDBF6DB36FEE066025D778FAED95A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.CharConverter
struct  CharConverter_tFD013540F3AFDBF6DB36FEE066025D778FAED95A  : public TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARCONVERTER_TFD013540F3AFDBF6DB36FEE066025D778FAED95A_H
#ifndef COLLECTIONCHANGEEVENTARGS_T63CA165C1F7D765B04CB139EB6577577479E57B0_H
#define COLLECTIONCHANGEEVENTARGS_T63CA165C1F7D765B04CB139EB6577577479E57B0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.CollectionChangeEventArgs
struct  CollectionChangeEventArgs_t63CA165C1F7D765B04CB139EB6577577479E57B0  : public EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E
{
public:
	// System.ComponentModel.CollectionChangeAction System.ComponentModel.CollectionChangeEventArgs::action
	int32_t ___action_1;
	// System.Object System.ComponentModel.CollectionChangeEventArgs::element
	RuntimeObject * ___element_2;

public:
	inline static int32_t get_offset_of_action_1() { return static_cast<int32_t>(offsetof(CollectionChangeEventArgs_t63CA165C1F7D765B04CB139EB6577577479E57B0, ___action_1)); }
	inline int32_t get_action_1() const { return ___action_1; }
	inline int32_t* get_address_of_action_1() { return &___action_1; }
	inline void set_action_1(int32_t value)
	{
		___action_1 = value;
	}

	inline static int32_t get_offset_of_element_2() { return static_cast<int32_t>(offsetof(CollectionChangeEventArgs_t63CA165C1F7D765B04CB139EB6577577479E57B0, ___element_2)); }
	inline RuntimeObject * get_element_2() const { return ___element_2; }
	inline RuntimeObject ** get_address_of_element_2() { return &___element_2; }
	inline void set_element_2(RuntimeObject * value)
	{
		___element_2 = value;
		Il2CppCodeGenWriteBarrier((&___element_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTIONCHANGEEVENTARGS_T63CA165C1F7D765B04CB139EB6577577479E57B0_H
#ifndef COLLECTIONCONVERTER_T039E15C433996B0F0F0EB78BEE81F6DE0705F184_H
#define COLLECTIONCONVERTER_T039E15C433996B0F0F0EB78BEE81F6DE0705F184_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.CollectionConverter
struct  CollectionConverter_t039E15C433996B0F0F0EB78BEE81F6DE0705F184  : public TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTIONCONVERTER_T039E15C433996B0F0F0EB78BEE81F6DE0705F184_H
#ifndef CULTUREINFOCONVERTER_T6BD4316CF7C9163A01C35EC9382BC3D1C01FB6C0_H
#define CULTUREINFOCONVERTER_T6BD4316CF7C9163A01C35EC9382BC3D1C01FB6C0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.CultureInfoConverter
struct  CultureInfoConverter_t6BD4316CF7C9163A01C35EC9382BC3D1C01FB6C0  : public TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB
{
public:
	// System.ComponentModel.TypeConverter_StandardValuesCollection System.ComponentModel.CultureInfoConverter::values
	StandardValuesCollection_t929677712574EF02F5C4CF4C38E070841C20BDA3 * ___values_2;

public:
	inline static int32_t get_offset_of_values_2() { return static_cast<int32_t>(offsetof(CultureInfoConverter_t6BD4316CF7C9163A01C35EC9382BC3D1C01FB6C0, ___values_2)); }
	inline StandardValuesCollection_t929677712574EF02F5C4CF4C38E070841C20BDA3 * get_values_2() const { return ___values_2; }
	inline StandardValuesCollection_t929677712574EF02F5C4CF4C38E070841C20BDA3 ** get_address_of_values_2() { return &___values_2; }
	inline void set_values_2(StandardValuesCollection_t929677712574EF02F5C4CF4C38E070841C20BDA3 * value)
	{
		___values_2 = value;
		Il2CppCodeGenWriteBarrier((&___values_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CULTUREINFOCONVERTER_T6BD4316CF7C9163A01C35EC9382BC3D1C01FB6C0_H
#ifndef DATAOBJECTMETHODATTRIBUTE_T13EFB10B080110C75356535458A36736DF2AC249_H
#define DATAOBJECTMETHODATTRIBUTE_T13EFB10B080110C75356535458A36736DF2AC249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.DataObjectMethodAttribute
struct  DataObjectMethodAttribute_t13EFB10B080110C75356535458A36736DF2AC249  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Boolean System.ComponentModel.DataObjectMethodAttribute::_isDefault
	bool ____isDefault_0;
	// System.ComponentModel.DataObjectMethodType System.ComponentModel.DataObjectMethodAttribute::_methodType
	int32_t ____methodType_1;

public:
	inline static int32_t get_offset_of__isDefault_0() { return static_cast<int32_t>(offsetof(DataObjectMethodAttribute_t13EFB10B080110C75356535458A36736DF2AC249, ____isDefault_0)); }
	inline bool get__isDefault_0() const { return ____isDefault_0; }
	inline bool* get_address_of__isDefault_0() { return &____isDefault_0; }
	inline void set__isDefault_0(bool value)
	{
		____isDefault_0 = value;
	}

	inline static int32_t get_offset_of__methodType_1() { return static_cast<int32_t>(offsetof(DataObjectMethodAttribute_t13EFB10B080110C75356535458A36736DF2AC249, ____methodType_1)); }
	inline int32_t get__methodType_1() const { return ____methodType_1; }
	inline int32_t* get_address_of__methodType_1() { return &____methodType_1; }
	inline void set__methodType_1(int32_t value)
	{
		____methodType_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATAOBJECTMETHODATTRIBUTE_T13EFB10B080110C75356535458A36736DF2AC249_H
#ifndef DATETIMECONVERTER_TE35DE01AAE1A29D50B4B0DC6467C9219CCE04DE1_H
#define DATETIMECONVERTER_TE35DE01AAE1A29D50B4B0DC6467C9219CCE04DE1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.DateTimeConverter
struct  DateTimeConverter_tE35DE01AAE1A29D50B4B0DC6467C9219CCE04DE1  : public TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMECONVERTER_TE35DE01AAE1A29D50B4B0DC6467C9219CCE04DE1_H
#ifndef DATETIMEOFFSETCONVERTER_TD9E7BEFD22CBB5DA01F50C0D51CDD60DF29F1D33_H
#define DATETIMEOFFSETCONVERTER_TD9E7BEFD22CBB5DA01F50C0D51CDD60DF29F1D33_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.DateTimeOffsetConverter
struct  DateTimeOffsetConverter_tD9E7BEFD22CBB5DA01F50C0D51CDD60DF29F1D33  : public TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEOFFSETCONVERTER_TD9E7BEFD22CBB5DA01F50C0D51CDD60DF29F1D33_H
#ifndef REFERENCECONVERTER_T5080472EE999A1F00721E6C5C97013762C85C7E4_H
#define REFERENCECONVERTER_T5080472EE999A1F00721E6C5C97013762C85C7E4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.ReferenceConverter
struct  ReferenceConverter_t5080472EE999A1F00721E6C5C97013762C85C7E4  : public TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB
{
public:
	// System.Type System.ComponentModel.ReferenceConverter::type
	Type_t * ___type_3;

public:
	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(ReferenceConverter_t5080472EE999A1F00721E6C5C97013762C85C7E4, ___type_3)); }
	inline Type_t * get_type_3() const { return ___type_3; }
	inline Type_t ** get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(Type_t * value)
	{
		___type_3 = value;
		Il2CppCodeGenWriteBarrier((&___type_3), value);
	}
};

struct ReferenceConverter_t5080472EE999A1F00721E6C5C97013762C85C7E4_StaticFields
{
public:
	// System.String System.ComponentModel.ReferenceConverter::none
	String_t* ___none_2;

public:
	inline static int32_t get_offset_of_none_2() { return static_cast<int32_t>(offsetof(ReferenceConverter_t5080472EE999A1F00721E6C5C97013762C85C7E4_StaticFields, ___none_2)); }
	inline String_t* get_none_2() const { return ___none_2; }
	inline String_t** get_address_of_none_2() { return &___none_2; }
	inline void set_none_2(String_t* value)
	{
		___none_2 = value;
		Il2CppCodeGenWriteBarrier((&___none_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFERENCECONVERTER_T5080472EE999A1F00721E6C5C97013762C85C7E4_H
#ifndef PROCESS_T7F28AE318E6CCF89716D05E78E2CBD669767D6A1_H
#define PROCESS_T7F28AE318E6CCF89716D05E78E2CBD669767D6A1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.Process
struct  Process_t7F28AE318E6CCF89716D05E78E2CBD669767D6A1  : public Component_t7AEFE153F6778CF52E1981BC3E811A9604B29473
{
public:
	// System.Boolean System.Diagnostics.Process::haveProcessId
	bool ___haveProcessId_4;
	// System.Int32 System.Diagnostics.Process::processId
	int32_t ___processId_5;
	// System.Boolean System.Diagnostics.Process::haveProcessHandle
	bool ___haveProcessHandle_6;
	// Microsoft.Win32.SafeHandles.SafeProcessHandle System.Diagnostics.Process::m_processHandle
	SafeProcessHandle_tEF75BF77F5F4E121334E2A97EE8E1F6685F38CF7 * ___m_processHandle_7;
	// System.Boolean System.Diagnostics.Process::isRemoteMachine
	bool ___isRemoteMachine_8;
	// System.String System.Diagnostics.Process::machineName
	String_t* ___machineName_9;
	// System.Int32 System.Diagnostics.Process::m_processAccess
	int32_t ___m_processAccess_10;
	// System.Diagnostics.ProcessThreadCollection System.Diagnostics.Process::threads
	ProcessThreadCollection_t6D1D2E676ED1F65087080729F91410724CA74DA7 * ___threads_11;
	// System.Diagnostics.ProcessModuleCollection System.Diagnostics.Process::modules
	ProcessModuleCollection_t93E76B9948E84325744E8C57A525FD465D78DB3F * ___modules_12;
	// System.Boolean System.Diagnostics.Process::haveWorkingSetLimits
	bool ___haveWorkingSetLimits_13;
	// System.Boolean System.Diagnostics.Process::havePriorityClass
	bool ___havePriorityClass_14;
	// System.Boolean System.Diagnostics.Process::watchForExit
	bool ___watchForExit_15;
	// System.Boolean System.Diagnostics.Process::watchingForExit
	bool ___watchingForExit_16;
	// System.EventHandler System.Diagnostics.Process::onExited
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * ___onExited_17;
	// System.Boolean System.Diagnostics.Process::exited
	bool ___exited_18;
	// System.Int32 System.Diagnostics.Process::exitCode
	int32_t ___exitCode_19;
	// System.Boolean System.Diagnostics.Process::signaled
	bool ___signaled_20;
	// System.Boolean System.Diagnostics.Process::haveExitTime
	bool ___haveExitTime_21;
	// System.Boolean System.Diagnostics.Process::raisedOnExited
	bool ___raisedOnExited_22;
	// System.Threading.RegisteredWaitHandle System.Diagnostics.Process::registeredWaitHandle
	RegisteredWaitHandle_t25AAC0B53C62CFA0B3F9BFFA87DDA3638F4308C0 * ___registeredWaitHandle_23;
	// System.Threading.WaitHandle System.Diagnostics.Process::waitHandle
	WaitHandle_tFD46B5B45A6BB296EA3A104C91DF2A7C03C10AC6 * ___waitHandle_24;
	// System.ComponentModel.ISynchronizeInvoke System.Diagnostics.Process::synchronizingObject
	RuntimeObject* ___synchronizingObject_25;
	// System.IO.StreamReader System.Diagnostics.Process::standardOutput
	StreamReader_t62E68063760DCD2FC036AE132DE69C24B7ED001E * ___standardOutput_26;
	// System.IO.StreamWriter System.Diagnostics.Process::standardInput
	StreamWriter_t989B894EF3BFCDF6FF5F5F068402A4F835FC8E8E * ___standardInput_27;
	// System.IO.StreamReader System.Diagnostics.Process::standardError
	StreamReader_t62E68063760DCD2FC036AE132DE69C24B7ED001E * ___standardError_28;
	// System.Boolean System.Diagnostics.Process::disposed
	bool ___disposed_29;
	// System.Diagnostics.Process_StreamReadMode System.Diagnostics.Process::outputStreamReadMode
	int32_t ___outputStreamReadMode_30;
	// System.Diagnostics.Process_StreamReadMode System.Diagnostics.Process::errorStreamReadMode
	int32_t ___errorStreamReadMode_31;
	// System.Diagnostics.Process_StreamReadMode System.Diagnostics.Process::inputStreamReadMode
	int32_t ___inputStreamReadMode_32;
	// System.Diagnostics.AsyncStreamReader System.Diagnostics.Process::output
	AsyncStreamReader_t2C28E845971B756383AF73AEF2A86C7545E5C485 * ___output_33;
	// System.Diagnostics.AsyncStreamReader System.Diagnostics.Process::error
	AsyncStreamReader_t2C28E845971B756383AF73AEF2A86C7545E5C485 * ___error_34;
	// System.String System.Diagnostics.Process::process_name
	String_t* ___process_name_35;

public:
	inline static int32_t get_offset_of_haveProcessId_4() { return static_cast<int32_t>(offsetof(Process_t7F28AE318E6CCF89716D05E78E2CBD669767D6A1, ___haveProcessId_4)); }
	inline bool get_haveProcessId_4() const { return ___haveProcessId_4; }
	inline bool* get_address_of_haveProcessId_4() { return &___haveProcessId_4; }
	inline void set_haveProcessId_4(bool value)
	{
		___haveProcessId_4 = value;
	}

	inline static int32_t get_offset_of_processId_5() { return static_cast<int32_t>(offsetof(Process_t7F28AE318E6CCF89716D05E78E2CBD669767D6A1, ___processId_5)); }
	inline int32_t get_processId_5() const { return ___processId_5; }
	inline int32_t* get_address_of_processId_5() { return &___processId_5; }
	inline void set_processId_5(int32_t value)
	{
		___processId_5 = value;
	}

	inline static int32_t get_offset_of_haveProcessHandle_6() { return static_cast<int32_t>(offsetof(Process_t7F28AE318E6CCF89716D05E78E2CBD669767D6A1, ___haveProcessHandle_6)); }
	inline bool get_haveProcessHandle_6() const { return ___haveProcessHandle_6; }
	inline bool* get_address_of_haveProcessHandle_6() { return &___haveProcessHandle_6; }
	inline void set_haveProcessHandle_6(bool value)
	{
		___haveProcessHandle_6 = value;
	}

	inline static int32_t get_offset_of_m_processHandle_7() { return static_cast<int32_t>(offsetof(Process_t7F28AE318E6CCF89716D05E78E2CBD669767D6A1, ___m_processHandle_7)); }
	inline SafeProcessHandle_tEF75BF77F5F4E121334E2A97EE8E1F6685F38CF7 * get_m_processHandle_7() const { return ___m_processHandle_7; }
	inline SafeProcessHandle_tEF75BF77F5F4E121334E2A97EE8E1F6685F38CF7 ** get_address_of_m_processHandle_7() { return &___m_processHandle_7; }
	inline void set_m_processHandle_7(SafeProcessHandle_tEF75BF77F5F4E121334E2A97EE8E1F6685F38CF7 * value)
	{
		___m_processHandle_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_processHandle_7), value);
	}

	inline static int32_t get_offset_of_isRemoteMachine_8() { return static_cast<int32_t>(offsetof(Process_t7F28AE318E6CCF89716D05E78E2CBD669767D6A1, ___isRemoteMachine_8)); }
	inline bool get_isRemoteMachine_8() const { return ___isRemoteMachine_8; }
	inline bool* get_address_of_isRemoteMachine_8() { return &___isRemoteMachine_8; }
	inline void set_isRemoteMachine_8(bool value)
	{
		___isRemoteMachine_8 = value;
	}

	inline static int32_t get_offset_of_machineName_9() { return static_cast<int32_t>(offsetof(Process_t7F28AE318E6CCF89716D05E78E2CBD669767D6A1, ___machineName_9)); }
	inline String_t* get_machineName_9() const { return ___machineName_9; }
	inline String_t** get_address_of_machineName_9() { return &___machineName_9; }
	inline void set_machineName_9(String_t* value)
	{
		___machineName_9 = value;
		Il2CppCodeGenWriteBarrier((&___machineName_9), value);
	}

	inline static int32_t get_offset_of_m_processAccess_10() { return static_cast<int32_t>(offsetof(Process_t7F28AE318E6CCF89716D05E78E2CBD669767D6A1, ___m_processAccess_10)); }
	inline int32_t get_m_processAccess_10() const { return ___m_processAccess_10; }
	inline int32_t* get_address_of_m_processAccess_10() { return &___m_processAccess_10; }
	inline void set_m_processAccess_10(int32_t value)
	{
		___m_processAccess_10 = value;
	}

	inline static int32_t get_offset_of_threads_11() { return static_cast<int32_t>(offsetof(Process_t7F28AE318E6CCF89716D05E78E2CBD669767D6A1, ___threads_11)); }
	inline ProcessThreadCollection_t6D1D2E676ED1F65087080729F91410724CA74DA7 * get_threads_11() const { return ___threads_11; }
	inline ProcessThreadCollection_t6D1D2E676ED1F65087080729F91410724CA74DA7 ** get_address_of_threads_11() { return &___threads_11; }
	inline void set_threads_11(ProcessThreadCollection_t6D1D2E676ED1F65087080729F91410724CA74DA7 * value)
	{
		___threads_11 = value;
		Il2CppCodeGenWriteBarrier((&___threads_11), value);
	}

	inline static int32_t get_offset_of_modules_12() { return static_cast<int32_t>(offsetof(Process_t7F28AE318E6CCF89716D05E78E2CBD669767D6A1, ___modules_12)); }
	inline ProcessModuleCollection_t93E76B9948E84325744E8C57A525FD465D78DB3F * get_modules_12() const { return ___modules_12; }
	inline ProcessModuleCollection_t93E76B9948E84325744E8C57A525FD465D78DB3F ** get_address_of_modules_12() { return &___modules_12; }
	inline void set_modules_12(ProcessModuleCollection_t93E76B9948E84325744E8C57A525FD465D78DB3F * value)
	{
		___modules_12 = value;
		Il2CppCodeGenWriteBarrier((&___modules_12), value);
	}

	inline static int32_t get_offset_of_haveWorkingSetLimits_13() { return static_cast<int32_t>(offsetof(Process_t7F28AE318E6CCF89716D05E78E2CBD669767D6A1, ___haveWorkingSetLimits_13)); }
	inline bool get_haveWorkingSetLimits_13() const { return ___haveWorkingSetLimits_13; }
	inline bool* get_address_of_haveWorkingSetLimits_13() { return &___haveWorkingSetLimits_13; }
	inline void set_haveWorkingSetLimits_13(bool value)
	{
		___haveWorkingSetLimits_13 = value;
	}

	inline static int32_t get_offset_of_havePriorityClass_14() { return static_cast<int32_t>(offsetof(Process_t7F28AE318E6CCF89716D05E78E2CBD669767D6A1, ___havePriorityClass_14)); }
	inline bool get_havePriorityClass_14() const { return ___havePriorityClass_14; }
	inline bool* get_address_of_havePriorityClass_14() { return &___havePriorityClass_14; }
	inline void set_havePriorityClass_14(bool value)
	{
		___havePriorityClass_14 = value;
	}

	inline static int32_t get_offset_of_watchForExit_15() { return static_cast<int32_t>(offsetof(Process_t7F28AE318E6CCF89716D05E78E2CBD669767D6A1, ___watchForExit_15)); }
	inline bool get_watchForExit_15() const { return ___watchForExit_15; }
	inline bool* get_address_of_watchForExit_15() { return &___watchForExit_15; }
	inline void set_watchForExit_15(bool value)
	{
		___watchForExit_15 = value;
	}

	inline static int32_t get_offset_of_watchingForExit_16() { return static_cast<int32_t>(offsetof(Process_t7F28AE318E6CCF89716D05E78E2CBD669767D6A1, ___watchingForExit_16)); }
	inline bool get_watchingForExit_16() const { return ___watchingForExit_16; }
	inline bool* get_address_of_watchingForExit_16() { return &___watchingForExit_16; }
	inline void set_watchingForExit_16(bool value)
	{
		___watchingForExit_16 = value;
	}

	inline static int32_t get_offset_of_onExited_17() { return static_cast<int32_t>(offsetof(Process_t7F28AE318E6CCF89716D05E78E2CBD669767D6A1, ___onExited_17)); }
	inline EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * get_onExited_17() const { return ___onExited_17; }
	inline EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C ** get_address_of_onExited_17() { return &___onExited_17; }
	inline void set_onExited_17(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * value)
	{
		___onExited_17 = value;
		Il2CppCodeGenWriteBarrier((&___onExited_17), value);
	}

	inline static int32_t get_offset_of_exited_18() { return static_cast<int32_t>(offsetof(Process_t7F28AE318E6CCF89716D05E78E2CBD669767D6A1, ___exited_18)); }
	inline bool get_exited_18() const { return ___exited_18; }
	inline bool* get_address_of_exited_18() { return &___exited_18; }
	inline void set_exited_18(bool value)
	{
		___exited_18 = value;
	}

	inline static int32_t get_offset_of_exitCode_19() { return static_cast<int32_t>(offsetof(Process_t7F28AE318E6CCF89716D05E78E2CBD669767D6A1, ___exitCode_19)); }
	inline int32_t get_exitCode_19() const { return ___exitCode_19; }
	inline int32_t* get_address_of_exitCode_19() { return &___exitCode_19; }
	inline void set_exitCode_19(int32_t value)
	{
		___exitCode_19 = value;
	}

	inline static int32_t get_offset_of_signaled_20() { return static_cast<int32_t>(offsetof(Process_t7F28AE318E6CCF89716D05E78E2CBD669767D6A1, ___signaled_20)); }
	inline bool get_signaled_20() const { return ___signaled_20; }
	inline bool* get_address_of_signaled_20() { return &___signaled_20; }
	inline void set_signaled_20(bool value)
	{
		___signaled_20 = value;
	}

	inline static int32_t get_offset_of_haveExitTime_21() { return static_cast<int32_t>(offsetof(Process_t7F28AE318E6CCF89716D05E78E2CBD669767D6A1, ___haveExitTime_21)); }
	inline bool get_haveExitTime_21() const { return ___haveExitTime_21; }
	inline bool* get_address_of_haveExitTime_21() { return &___haveExitTime_21; }
	inline void set_haveExitTime_21(bool value)
	{
		___haveExitTime_21 = value;
	}

	inline static int32_t get_offset_of_raisedOnExited_22() { return static_cast<int32_t>(offsetof(Process_t7F28AE318E6CCF89716D05E78E2CBD669767D6A1, ___raisedOnExited_22)); }
	inline bool get_raisedOnExited_22() const { return ___raisedOnExited_22; }
	inline bool* get_address_of_raisedOnExited_22() { return &___raisedOnExited_22; }
	inline void set_raisedOnExited_22(bool value)
	{
		___raisedOnExited_22 = value;
	}

	inline static int32_t get_offset_of_registeredWaitHandle_23() { return static_cast<int32_t>(offsetof(Process_t7F28AE318E6CCF89716D05E78E2CBD669767D6A1, ___registeredWaitHandle_23)); }
	inline RegisteredWaitHandle_t25AAC0B53C62CFA0B3F9BFFA87DDA3638F4308C0 * get_registeredWaitHandle_23() const { return ___registeredWaitHandle_23; }
	inline RegisteredWaitHandle_t25AAC0B53C62CFA0B3F9BFFA87DDA3638F4308C0 ** get_address_of_registeredWaitHandle_23() { return &___registeredWaitHandle_23; }
	inline void set_registeredWaitHandle_23(RegisteredWaitHandle_t25AAC0B53C62CFA0B3F9BFFA87DDA3638F4308C0 * value)
	{
		___registeredWaitHandle_23 = value;
		Il2CppCodeGenWriteBarrier((&___registeredWaitHandle_23), value);
	}

	inline static int32_t get_offset_of_waitHandle_24() { return static_cast<int32_t>(offsetof(Process_t7F28AE318E6CCF89716D05E78E2CBD669767D6A1, ___waitHandle_24)); }
	inline WaitHandle_tFD46B5B45A6BB296EA3A104C91DF2A7C03C10AC6 * get_waitHandle_24() const { return ___waitHandle_24; }
	inline WaitHandle_tFD46B5B45A6BB296EA3A104C91DF2A7C03C10AC6 ** get_address_of_waitHandle_24() { return &___waitHandle_24; }
	inline void set_waitHandle_24(WaitHandle_tFD46B5B45A6BB296EA3A104C91DF2A7C03C10AC6 * value)
	{
		___waitHandle_24 = value;
		Il2CppCodeGenWriteBarrier((&___waitHandle_24), value);
	}

	inline static int32_t get_offset_of_synchronizingObject_25() { return static_cast<int32_t>(offsetof(Process_t7F28AE318E6CCF89716D05E78E2CBD669767D6A1, ___synchronizingObject_25)); }
	inline RuntimeObject* get_synchronizingObject_25() const { return ___synchronizingObject_25; }
	inline RuntimeObject** get_address_of_synchronizingObject_25() { return &___synchronizingObject_25; }
	inline void set_synchronizingObject_25(RuntimeObject* value)
	{
		___synchronizingObject_25 = value;
		Il2CppCodeGenWriteBarrier((&___synchronizingObject_25), value);
	}

	inline static int32_t get_offset_of_standardOutput_26() { return static_cast<int32_t>(offsetof(Process_t7F28AE318E6CCF89716D05E78E2CBD669767D6A1, ___standardOutput_26)); }
	inline StreamReader_t62E68063760DCD2FC036AE132DE69C24B7ED001E * get_standardOutput_26() const { return ___standardOutput_26; }
	inline StreamReader_t62E68063760DCD2FC036AE132DE69C24B7ED001E ** get_address_of_standardOutput_26() { return &___standardOutput_26; }
	inline void set_standardOutput_26(StreamReader_t62E68063760DCD2FC036AE132DE69C24B7ED001E * value)
	{
		___standardOutput_26 = value;
		Il2CppCodeGenWriteBarrier((&___standardOutput_26), value);
	}

	inline static int32_t get_offset_of_standardInput_27() { return static_cast<int32_t>(offsetof(Process_t7F28AE318E6CCF89716D05E78E2CBD669767D6A1, ___standardInput_27)); }
	inline StreamWriter_t989B894EF3BFCDF6FF5F5F068402A4F835FC8E8E * get_standardInput_27() const { return ___standardInput_27; }
	inline StreamWriter_t989B894EF3BFCDF6FF5F5F068402A4F835FC8E8E ** get_address_of_standardInput_27() { return &___standardInput_27; }
	inline void set_standardInput_27(StreamWriter_t989B894EF3BFCDF6FF5F5F068402A4F835FC8E8E * value)
	{
		___standardInput_27 = value;
		Il2CppCodeGenWriteBarrier((&___standardInput_27), value);
	}

	inline static int32_t get_offset_of_standardError_28() { return static_cast<int32_t>(offsetof(Process_t7F28AE318E6CCF89716D05E78E2CBD669767D6A1, ___standardError_28)); }
	inline StreamReader_t62E68063760DCD2FC036AE132DE69C24B7ED001E * get_standardError_28() const { return ___standardError_28; }
	inline StreamReader_t62E68063760DCD2FC036AE132DE69C24B7ED001E ** get_address_of_standardError_28() { return &___standardError_28; }
	inline void set_standardError_28(StreamReader_t62E68063760DCD2FC036AE132DE69C24B7ED001E * value)
	{
		___standardError_28 = value;
		Il2CppCodeGenWriteBarrier((&___standardError_28), value);
	}

	inline static int32_t get_offset_of_disposed_29() { return static_cast<int32_t>(offsetof(Process_t7F28AE318E6CCF89716D05E78E2CBD669767D6A1, ___disposed_29)); }
	inline bool get_disposed_29() const { return ___disposed_29; }
	inline bool* get_address_of_disposed_29() { return &___disposed_29; }
	inline void set_disposed_29(bool value)
	{
		___disposed_29 = value;
	}

	inline static int32_t get_offset_of_outputStreamReadMode_30() { return static_cast<int32_t>(offsetof(Process_t7F28AE318E6CCF89716D05E78E2CBD669767D6A1, ___outputStreamReadMode_30)); }
	inline int32_t get_outputStreamReadMode_30() const { return ___outputStreamReadMode_30; }
	inline int32_t* get_address_of_outputStreamReadMode_30() { return &___outputStreamReadMode_30; }
	inline void set_outputStreamReadMode_30(int32_t value)
	{
		___outputStreamReadMode_30 = value;
	}

	inline static int32_t get_offset_of_errorStreamReadMode_31() { return static_cast<int32_t>(offsetof(Process_t7F28AE318E6CCF89716D05E78E2CBD669767D6A1, ___errorStreamReadMode_31)); }
	inline int32_t get_errorStreamReadMode_31() const { return ___errorStreamReadMode_31; }
	inline int32_t* get_address_of_errorStreamReadMode_31() { return &___errorStreamReadMode_31; }
	inline void set_errorStreamReadMode_31(int32_t value)
	{
		___errorStreamReadMode_31 = value;
	}

	inline static int32_t get_offset_of_inputStreamReadMode_32() { return static_cast<int32_t>(offsetof(Process_t7F28AE318E6CCF89716D05E78E2CBD669767D6A1, ___inputStreamReadMode_32)); }
	inline int32_t get_inputStreamReadMode_32() const { return ___inputStreamReadMode_32; }
	inline int32_t* get_address_of_inputStreamReadMode_32() { return &___inputStreamReadMode_32; }
	inline void set_inputStreamReadMode_32(int32_t value)
	{
		___inputStreamReadMode_32 = value;
	}

	inline static int32_t get_offset_of_output_33() { return static_cast<int32_t>(offsetof(Process_t7F28AE318E6CCF89716D05E78E2CBD669767D6A1, ___output_33)); }
	inline AsyncStreamReader_t2C28E845971B756383AF73AEF2A86C7545E5C485 * get_output_33() const { return ___output_33; }
	inline AsyncStreamReader_t2C28E845971B756383AF73AEF2A86C7545E5C485 ** get_address_of_output_33() { return &___output_33; }
	inline void set_output_33(AsyncStreamReader_t2C28E845971B756383AF73AEF2A86C7545E5C485 * value)
	{
		___output_33 = value;
		Il2CppCodeGenWriteBarrier((&___output_33), value);
	}

	inline static int32_t get_offset_of_error_34() { return static_cast<int32_t>(offsetof(Process_t7F28AE318E6CCF89716D05E78E2CBD669767D6A1, ___error_34)); }
	inline AsyncStreamReader_t2C28E845971B756383AF73AEF2A86C7545E5C485 * get_error_34() const { return ___error_34; }
	inline AsyncStreamReader_t2C28E845971B756383AF73AEF2A86C7545E5C485 ** get_address_of_error_34() { return &___error_34; }
	inline void set_error_34(AsyncStreamReader_t2C28E845971B756383AF73AEF2A86C7545E5C485 * value)
	{
		___error_34 = value;
		Il2CppCodeGenWriteBarrier((&___error_34), value);
	}

	inline static int32_t get_offset_of_process_name_35() { return static_cast<int32_t>(offsetof(Process_t7F28AE318E6CCF89716D05E78E2CBD669767D6A1, ___process_name_35)); }
	inline String_t* get_process_name_35() const { return ___process_name_35; }
	inline String_t** get_address_of_process_name_35() { return &___process_name_35; }
	inline void set_process_name_35(String_t* value)
	{
		___process_name_35 = value;
		Il2CppCodeGenWriteBarrier((&___process_name_35), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROCESS_T7F28AE318E6CCF89716D05E78E2CBD669767D6A1_H
#ifndef PROCESSWAITHANDLE_T3FBAA284E0F9A0751AFBC0A8BF7718DEF2654761_H
#define PROCESSWAITHANDLE_T3FBAA284E0F9A0751AFBC0A8BF7718DEF2654761_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.ProcessWaitHandle
struct  ProcessWaitHandle_t3FBAA284E0F9A0751AFBC0A8BF7718DEF2654761  : public WaitHandle_tFD46B5B45A6BB296EA3A104C91DF2A7C03C10AC6
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROCESSWAITHANDLE_T3FBAA284E0F9A0751AFBC0A8BF7718DEF2654761_H
#ifndef TRACELISTENER_TAA208D26D2197E835400A6EE284410A35F436111_H
#define TRACELISTENER_TAA208D26D2197E835400A6EE284410A35F436111_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.TraceListener
struct  TraceListener_tAA208D26D2197E835400A6EE284410A35F436111  : public MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF
{
public:
	// System.Int32 System.Diagnostics.TraceListener::indentLevel
	int32_t ___indentLevel_1;
	// System.Int32 System.Diagnostics.TraceListener::indentSize
	int32_t ___indentSize_2;
	// System.Diagnostics.TraceOptions System.Diagnostics.TraceListener::traceOptions
	int32_t ___traceOptions_3;
	// System.Boolean System.Diagnostics.TraceListener::needIndent
	bool ___needIndent_4;
	// System.String System.Diagnostics.TraceListener::listenerName
	String_t* ___listenerName_5;
	// System.Diagnostics.TraceFilter System.Diagnostics.TraceListener::filter
	TraceFilter_t5BA76D3899B80AEA894D4040364099DC7C47C6F1 * ___filter_6;

public:
	inline static int32_t get_offset_of_indentLevel_1() { return static_cast<int32_t>(offsetof(TraceListener_tAA208D26D2197E835400A6EE284410A35F436111, ___indentLevel_1)); }
	inline int32_t get_indentLevel_1() const { return ___indentLevel_1; }
	inline int32_t* get_address_of_indentLevel_1() { return &___indentLevel_1; }
	inline void set_indentLevel_1(int32_t value)
	{
		___indentLevel_1 = value;
	}

	inline static int32_t get_offset_of_indentSize_2() { return static_cast<int32_t>(offsetof(TraceListener_tAA208D26D2197E835400A6EE284410A35F436111, ___indentSize_2)); }
	inline int32_t get_indentSize_2() const { return ___indentSize_2; }
	inline int32_t* get_address_of_indentSize_2() { return &___indentSize_2; }
	inline void set_indentSize_2(int32_t value)
	{
		___indentSize_2 = value;
	}

	inline static int32_t get_offset_of_traceOptions_3() { return static_cast<int32_t>(offsetof(TraceListener_tAA208D26D2197E835400A6EE284410A35F436111, ___traceOptions_3)); }
	inline int32_t get_traceOptions_3() const { return ___traceOptions_3; }
	inline int32_t* get_address_of_traceOptions_3() { return &___traceOptions_3; }
	inline void set_traceOptions_3(int32_t value)
	{
		___traceOptions_3 = value;
	}

	inline static int32_t get_offset_of_needIndent_4() { return static_cast<int32_t>(offsetof(TraceListener_tAA208D26D2197E835400A6EE284410A35F436111, ___needIndent_4)); }
	inline bool get_needIndent_4() const { return ___needIndent_4; }
	inline bool* get_address_of_needIndent_4() { return &___needIndent_4; }
	inline void set_needIndent_4(bool value)
	{
		___needIndent_4 = value;
	}

	inline static int32_t get_offset_of_listenerName_5() { return static_cast<int32_t>(offsetof(TraceListener_tAA208D26D2197E835400A6EE284410A35F436111, ___listenerName_5)); }
	inline String_t* get_listenerName_5() const { return ___listenerName_5; }
	inline String_t** get_address_of_listenerName_5() { return &___listenerName_5; }
	inline void set_listenerName_5(String_t* value)
	{
		___listenerName_5 = value;
		Il2CppCodeGenWriteBarrier((&___listenerName_5), value);
	}

	inline static int32_t get_offset_of_filter_6() { return static_cast<int32_t>(offsetof(TraceListener_tAA208D26D2197E835400A6EE284410A35F436111, ___filter_6)); }
	inline TraceFilter_t5BA76D3899B80AEA894D4040364099DC7C47C6F1 * get_filter_6() const { return ___filter_6; }
	inline TraceFilter_t5BA76D3899B80AEA894D4040364099DC7C47C6F1 ** get_address_of_filter_6() { return &___filter_6; }
	inline void set_filter_6(TraceFilter_t5BA76D3899B80AEA894D4040364099DC7C47C6F1 * value)
	{
		___filter_6 = value;
		Il2CppCodeGenWriteBarrier((&___filter_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACELISTENER_TAA208D26D2197E835400A6EE284410A35F436111_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef RESOURCEMANAGER_T966CE0B6B59F36DD8797BDC20B5EEFACE0A883FF_H
#define RESOURCEMANAGER_T966CE0B6B59F36DD8797BDC20B5EEFACE0A883FF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Resources.ResourceManager
struct  ResourceManager_t966CE0B6B59F36DD8797BDC20B5EEFACE0A883FF  : public RuntimeObject
{
public:
	// System.String System.Resources.ResourceManager::BaseNameField
	String_t* ___BaseNameField_0;
	// System.Collections.Hashtable System.Resources.ResourceManager::ResourceSets
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___ResourceSets_1;
	// System.Collections.Generic.Dictionary`2<System.String,System.Resources.ResourceSet> System.Resources.ResourceManager::_resourceSets
	Dictionary_2_tDE0FFCE2C110EEFB68C37CEA54DBCA577AFC1CE6 * ____resourceSets_2;
	// System.String System.Resources.ResourceManager::moduleDir
	String_t* ___moduleDir_3;
	// System.Reflection.Assembly System.Resources.ResourceManager::MainAssembly
	Assembly_t * ___MainAssembly_4;
	// System.Type System.Resources.ResourceManager::_locationInfo
	Type_t * ____locationInfo_5;
	// System.Type System.Resources.ResourceManager::_userResourceSet
	Type_t * ____userResourceSet_6;
	// System.Globalization.CultureInfo System.Resources.ResourceManager::_neutralResourcesCulture
	CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * ____neutralResourcesCulture_7;
	// System.Resources.ResourceManager_CultureNameResourceSetPair System.Resources.ResourceManager::_lastUsedResourceCache
	CultureNameResourceSetPair_t77328DA298FCF741DE21CC5B3E19F160D7060074 * ____lastUsedResourceCache_8;
	// System.Boolean System.Resources.ResourceManager::_ignoreCase
	bool ____ignoreCase_9;
	// System.Boolean System.Resources.ResourceManager::UseManifest
	bool ___UseManifest_10;
	// System.Boolean System.Resources.ResourceManager::UseSatelliteAssem
	bool ___UseSatelliteAssem_11;
	// System.Resources.UltimateResourceFallbackLocation System.Resources.ResourceManager::_fallbackLoc
	int32_t ____fallbackLoc_12;
	// System.Version System.Resources.ResourceManager::_satelliteContractVersion
	Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD * ____satelliteContractVersion_13;
	// System.Boolean System.Resources.ResourceManager::_lookedForSatelliteContractVersion
	bool ____lookedForSatelliteContractVersion_14;
	// System.Reflection.Assembly System.Resources.ResourceManager::_callingAssembly
	Assembly_t * ____callingAssembly_15;
	// System.Reflection.RuntimeAssembly System.Resources.ResourceManager::m_callingAssembly
	RuntimeAssembly_t5EE9CD749D82345AE5635B9665665C31A3308EB1 * ___m_callingAssembly_16;
	// System.Resources.IResourceGroveler System.Resources.ResourceManager::resourceGroveler
	RuntimeObject* ___resourceGroveler_17;

public:
	inline static int32_t get_offset_of_BaseNameField_0() { return static_cast<int32_t>(offsetof(ResourceManager_t966CE0B6B59F36DD8797BDC20B5EEFACE0A883FF, ___BaseNameField_0)); }
	inline String_t* get_BaseNameField_0() const { return ___BaseNameField_0; }
	inline String_t** get_address_of_BaseNameField_0() { return &___BaseNameField_0; }
	inline void set_BaseNameField_0(String_t* value)
	{
		___BaseNameField_0 = value;
		Il2CppCodeGenWriteBarrier((&___BaseNameField_0), value);
	}

	inline static int32_t get_offset_of_ResourceSets_1() { return static_cast<int32_t>(offsetof(ResourceManager_t966CE0B6B59F36DD8797BDC20B5EEFACE0A883FF, ___ResourceSets_1)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_ResourceSets_1() const { return ___ResourceSets_1; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_ResourceSets_1() { return &___ResourceSets_1; }
	inline void set_ResourceSets_1(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___ResourceSets_1 = value;
		Il2CppCodeGenWriteBarrier((&___ResourceSets_1), value);
	}

	inline static int32_t get_offset_of__resourceSets_2() { return static_cast<int32_t>(offsetof(ResourceManager_t966CE0B6B59F36DD8797BDC20B5EEFACE0A883FF, ____resourceSets_2)); }
	inline Dictionary_2_tDE0FFCE2C110EEFB68C37CEA54DBCA577AFC1CE6 * get__resourceSets_2() const { return ____resourceSets_2; }
	inline Dictionary_2_tDE0FFCE2C110EEFB68C37CEA54DBCA577AFC1CE6 ** get_address_of__resourceSets_2() { return &____resourceSets_2; }
	inline void set__resourceSets_2(Dictionary_2_tDE0FFCE2C110EEFB68C37CEA54DBCA577AFC1CE6 * value)
	{
		____resourceSets_2 = value;
		Il2CppCodeGenWriteBarrier((&____resourceSets_2), value);
	}

	inline static int32_t get_offset_of_moduleDir_3() { return static_cast<int32_t>(offsetof(ResourceManager_t966CE0B6B59F36DD8797BDC20B5EEFACE0A883FF, ___moduleDir_3)); }
	inline String_t* get_moduleDir_3() const { return ___moduleDir_3; }
	inline String_t** get_address_of_moduleDir_3() { return &___moduleDir_3; }
	inline void set_moduleDir_3(String_t* value)
	{
		___moduleDir_3 = value;
		Il2CppCodeGenWriteBarrier((&___moduleDir_3), value);
	}

	inline static int32_t get_offset_of_MainAssembly_4() { return static_cast<int32_t>(offsetof(ResourceManager_t966CE0B6B59F36DD8797BDC20B5EEFACE0A883FF, ___MainAssembly_4)); }
	inline Assembly_t * get_MainAssembly_4() const { return ___MainAssembly_4; }
	inline Assembly_t ** get_address_of_MainAssembly_4() { return &___MainAssembly_4; }
	inline void set_MainAssembly_4(Assembly_t * value)
	{
		___MainAssembly_4 = value;
		Il2CppCodeGenWriteBarrier((&___MainAssembly_4), value);
	}

	inline static int32_t get_offset_of__locationInfo_5() { return static_cast<int32_t>(offsetof(ResourceManager_t966CE0B6B59F36DD8797BDC20B5EEFACE0A883FF, ____locationInfo_5)); }
	inline Type_t * get__locationInfo_5() const { return ____locationInfo_5; }
	inline Type_t ** get_address_of__locationInfo_5() { return &____locationInfo_5; }
	inline void set__locationInfo_5(Type_t * value)
	{
		____locationInfo_5 = value;
		Il2CppCodeGenWriteBarrier((&____locationInfo_5), value);
	}

	inline static int32_t get_offset_of__userResourceSet_6() { return static_cast<int32_t>(offsetof(ResourceManager_t966CE0B6B59F36DD8797BDC20B5EEFACE0A883FF, ____userResourceSet_6)); }
	inline Type_t * get__userResourceSet_6() const { return ____userResourceSet_6; }
	inline Type_t ** get_address_of__userResourceSet_6() { return &____userResourceSet_6; }
	inline void set__userResourceSet_6(Type_t * value)
	{
		____userResourceSet_6 = value;
		Il2CppCodeGenWriteBarrier((&____userResourceSet_6), value);
	}

	inline static int32_t get_offset_of__neutralResourcesCulture_7() { return static_cast<int32_t>(offsetof(ResourceManager_t966CE0B6B59F36DD8797BDC20B5EEFACE0A883FF, ____neutralResourcesCulture_7)); }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * get__neutralResourcesCulture_7() const { return ____neutralResourcesCulture_7; }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F ** get_address_of__neutralResourcesCulture_7() { return &____neutralResourcesCulture_7; }
	inline void set__neutralResourcesCulture_7(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * value)
	{
		____neutralResourcesCulture_7 = value;
		Il2CppCodeGenWriteBarrier((&____neutralResourcesCulture_7), value);
	}

	inline static int32_t get_offset_of__lastUsedResourceCache_8() { return static_cast<int32_t>(offsetof(ResourceManager_t966CE0B6B59F36DD8797BDC20B5EEFACE0A883FF, ____lastUsedResourceCache_8)); }
	inline CultureNameResourceSetPair_t77328DA298FCF741DE21CC5B3E19F160D7060074 * get__lastUsedResourceCache_8() const { return ____lastUsedResourceCache_8; }
	inline CultureNameResourceSetPair_t77328DA298FCF741DE21CC5B3E19F160D7060074 ** get_address_of__lastUsedResourceCache_8() { return &____lastUsedResourceCache_8; }
	inline void set__lastUsedResourceCache_8(CultureNameResourceSetPair_t77328DA298FCF741DE21CC5B3E19F160D7060074 * value)
	{
		____lastUsedResourceCache_8 = value;
		Il2CppCodeGenWriteBarrier((&____lastUsedResourceCache_8), value);
	}

	inline static int32_t get_offset_of__ignoreCase_9() { return static_cast<int32_t>(offsetof(ResourceManager_t966CE0B6B59F36DD8797BDC20B5EEFACE0A883FF, ____ignoreCase_9)); }
	inline bool get__ignoreCase_9() const { return ____ignoreCase_9; }
	inline bool* get_address_of__ignoreCase_9() { return &____ignoreCase_9; }
	inline void set__ignoreCase_9(bool value)
	{
		____ignoreCase_9 = value;
	}

	inline static int32_t get_offset_of_UseManifest_10() { return static_cast<int32_t>(offsetof(ResourceManager_t966CE0B6B59F36DD8797BDC20B5EEFACE0A883FF, ___UseManifest_10)); }
	inline bool get_UseManifest_10() const { return ___UseManifest_10; }
	inline bool* get_address_of_UseManifest_10() { return &___UseManifest_10; }
	inline void set_UseManifest_10(bool value)
	{
		___UseManifest_10 = value;
	}

	inline static int32_t get_offset_of_UseSatelliteAssem_11() { return static_cast<int32_t>(offsetof(ResourceManager_t966CE0B6B59F36DD8797BDC20B5EEFACE0A883FF, ___UseSatelliteAssem_11)); }
	inline bool get_UseSatelliteAssem_11() const { return ___UseSatelliteAssem_11; }
	inline bool* get_address_of_UseSatelliteAssem_11() { return &___UseSatelliteAssem_11; }
	inline void set_UseSatelliteAssem_11(bool value)
	{
		___UseSatelliteAssem_11 = value;
	}

	inline static int32_t get_offset_of__fallbackLoc_12() { return static_cast<int32_t>(offsetof(ResourceManager_t966CE0B6B59F36DD8797BDC20B5EEFACE0A883FF, ____fallbackLoc_12)); }
	inline int32_t get__fallbackLoc_12() const { return ____fallbackLoc_12; }
	inline int32_t* get_address_of__fallbackLoc_12() { return &____fallbackLoc_12; }
	inline void set__fallbackLoc_12(int32_t value)
	{
		____fallbackLoc_12 = value;
	}

	inline static int32_t get_offset_of__satelliteContractVersion_13() { return static_cast<int32_t>(offsetof(ResourceManager_t966CE0B6B59F36DD8797BDC20B5EEFACE0A883FF, ____satelliteContractVersion_13)); }
	inline Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD * get__satelliteContractVersion_13() const { return ____satelliteContractVersion_13; }
	inline Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD ** get_address_of__satelliteContractVersion_13() { return &____satelliteContractVersion_13; }
	inline void set__satelliteContractVersion_13(Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD * value)
	{
		____satelliteContractVersion_13 = value;
		Il2CppCodeGenWriteBarrier((&____satelliteContractVersion_13), value);
	}

	inline static int32_t get_offset_of__lookedForSatelliteContractVersion_14() { return static_cast<int32_t>(offsetof(ResourceManager_t966CE0B6B59F36DD8797BDC20B5EEFACE0A883FF, ____lookedForSatelliteContractVersion_14)); }
	inline bool get__lookedForSatelliteContractVersion_14() const { return ____lookedForSatelliteContractVersion_14; }
	inline bool* get_address_of__lookedForSatelliteContractVersion_14() { return &____lookedForSatelliteContractVersion_14; }
	inline void set__lookedForSatelliteContractVersion_14(bool value)
	{
		____lookedForSatelliteContractVersion_14 = value;
	}

	inline static int32_t get_offset_of__callingAssembly_15() { return static_cast<int32_t>(offsetof(ResourceManager_t966CE0B6B59F36DD8797BDC20B5EEFACE0A883FF, ____callingAssembly_15)); }
	inline Assembly_t * get__callingAssembly_15() const { return ____callingAssembly_15; }
	inline Assembly_t ** get_address_of__callingAssembly_15() { return &____callingAssembly_15; }
	inline void set__callingAssembly_15(Assembly_t * value)
	{
		____callingAssembly_15 = value;
		Il2CppCodeGenWriteBarrier((&____callingAssembly_15), value);
	}

	inline static int32_t get_offset_of_m_callingAssembly_16() { return static_cast<int32_t>(offsetof(ResourceManager_t966CE0B6B59F36DD8797BDC20B5EEFACE0A883FF, ___m_callingAssembly_16)); }
	inline RuntimeAssembly_t5EE9CD749D82345AE5635B9665665C31A3308EB1 * get_m_callingAssembly_16() const { return ___m_callingAssembly_16; }
	inline RuntimeAssembly_t5EE9CD749D82345AE5635B9665665C31A3308EB1 ** get_address_of_m_callingAssembly_16() { return &___m_callingAssembly_16; }
	inline void set_m_callingAssembly_16(RuntimeAssembly_t5EE9CD749D82345AE5635B9665665C31A3308EB1 * value)
	{
		___m_callingAssembly_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_callingAssembly_16), value);
	}

	inline static int32_t get_offset_of_resourceGroveler_17() { return static_cast<int32_t>(offsetof(ResourceManager_t966CE0B6B59F36DD8797BDC20B5EEFACE0A883FF, ___resourceGroveler_17)); }
	inline RuntimeObject* get_resourceGroveler_17() const { return ___resourceGroveler_17; }
	inline RuntimeObject** get_address_of_resourceGroveler_17() { return &___resourceGroveler_17; }
	inline void set_resourceGroveler_17(RuntimeObject* value)
	{
		___resourceGroveler_17 = value;
		Il2CppCodeGenWriteBarrier((&___resourceGroveler_17), value);
	}
};

struct ResourceManager_t966CE0B6B59F36DD8797BDC20B5EEFACE0A883FF_StaticFields
{
public:
	// System.Int32 System.Resources.ResourceManager::MagicNumber
	int32_t ___MagicNumber_18;
	// System.Int32 System.Resources.ResourceManager::HeaderVersionNumber
	int32_t ___HeaderVersionNumber_19;
	// System.Type System.Resources.ResourceManager::_minResourceSet
	Type_t * ____minResourceSet_20;
	// System.String System.Resources.ResourceManager::ResReaderTypeName
	String_t* ___ResReaderTypeName_21;
	// System.String System.Resources.ResourceManager::ResSetTypeName
	String_t* ___ResSetTypeName_22;
	// System.String System.Resources.ResourceManager::MscorlibName
	String_t* ___MscorlibName_23;
	// System.Int32 System.Resources.ResourceManager::DEBUG
	int32_t ___DEBUG_24;

public:
	inline static int32_t get_offset_of_MagicNumber_18() { return static_cast<int32_t>(offsetof(ResourceManager_t966CE0B6B59F36DD8797BDC20B5EEFACE0A883FF_StaticFields, ___MagicNumber_18)); }
	inline int32_t get_MagicNumber_18() const { return ___MagicNumber_18; }
	inline int32_t* get_address_of_MagicNumber_18() { return &___MagicNumber_18; }
	inline void set_MagicNumber_18(int32_t value)
	{
		___MagicNumber_18 = value;
	}

	inline static int32_t get_offset_of_HeaderVersionNumber_19() { return static_cast<int32_t>(offsetof(ResourceManager_t966CE0B6B59F36DD8797BDC20B5EEFACE0A883FF_StaticFields, ___HeaderVersionNumber_19)); }
	inline int32_t get_HeaderVersionNumber_19() const { return ___HeaderVersionNumber_19; }
	inline int32_t* get_address_of_HeaderVersionNumber_19() { return &___HeaderVersionNumber_19; }
	inline void set_HeaderVersionNumber_19(int32_t value)
	{
		___HeaderVersionNumber_19 = value;
	}

	inline static int32_t get_offset_of__minResourceSet_20() { return static_cast<int32_t>(offsetof(ResourceManager_t966CE0B6B59F36DD8797BDC20B5EEFACE0A883FF_StaticFields, ____minResourceSet_20)); }
	inline Type_t * get__minResourceSet_20() const { return ____minResourceSet_20; }
	inline Type_t ** get_address_of__minResourceSet_20() { return &____minResourceSet_20; }
	inline void set__minResourceSet_20(Type_t * value)
	{
		____minResourceSet_20 = value;
		Il2CppCodeGenWriteBarrier((&____minResourceSet_20), value);
	}

	inline static int32_t get_offset_of_ResReaderTypeName_21() { return static_cast<int32_t>(offsetof(ResourceManager_t966CE0B6B59F36DD8797BDC20B5EEFACE0A883FF_StaticFields, ___ResReaderTypeName_21)); }
	inline String_t* get_ResReaderTypeName_21() const { return ___ResReaderTypeName_21; }
	inline String_t** get_address_of_ResReaderTypeName_21() { return &___ResReaderTypeName_21; }
	inline void set_ResReaderTypeName_21(String_t* value)
	{
		___ResReaderTypeName_21 = value;
		Il2CppCodeGenWriteBarrier((&___ResReaderTypeName_21), value);
	}

	inline static int32_t get_offset_of_ResSetTypeName_22() { return static_cast<int32_t>(offsetof(ResourceManager_t966CE0B6B59F36DD8797BDC20B5EEFACE0A883FF_StaticFields, ___ResSetTypeName_22)); }
	inline String_t* get_ResSetTypeName_22() const { return ___ResSetTypeName_22; }
	inline String_t** get_address_of_ResSetTypeName_22() { return &___ResSetTypeName_22; }
	inline void set_ResSetTypeName_22(String_t* value)
	{
		___ResSetTypeName_22 = value;
		Il2CppCodeGenWriteBarrier((&___ResSetTypeName_22), value);
	}

	inline static int32_t get_offset_of_MscorlibName_23() { return static_cast<int32_t>(offsetof(ResourceManager_t966CE0B6B59F36DD8797BDC20B5EEFACE0A883FF_StaticFields, ___MscorlibName_23)); }
	inline String_t* get_MscorlibName_23() const { return ___MscorlibName_23; }
	inline String_t** get_address_of_MscorlibName_23() { return &___MscorlibName_23; }
	inline void set_MscorlibName_23(String_t* value)
	{
		___MscorlibName_23 = value;
		Il2CppCodeGenWriteBarrier((&___MscorlibName_23), value);
	}

	inline static int32_t get_offset_of_DEBUG_24() { return static_cast<int32_t>(offsetof(ResourceManager_t966CE0B6B59F36DD8797BDC20B5EEFACE0A883FF_StaticFields, ___DEBUG_24)); }
	inline int32_t get_DEBUG_24() const { return ___DEBUG_24; }
	inline int32_t* get_address_of_DEBUG_24() { return &___DEBUG_24; }
	inline void set_DEBUG_24(int32_t value)
	{
		___DEBUG_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESOURCEMANAGER_T966CE0B6B59F36DD8797BDC20B5EEFACE0A883FF_H
#ifndef MATCHSPARSE_T73BEE39B7EBE30B7460558DCA846B704C94B571C_H
#define MATCHSPARSE_T73BEE39B7EBE30B7460558DCA846B704C94B571C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.MatchSparse
struct  MatchSparse_t73BEE39B7EBE30B7460558DCA846B704C94B571C  : public Match_tE447871AB59EED3642F31EB9559D162C2977EBB5
{
public:
	// System.Collections.Hashtable System.Text.RegularExpressions.MatchSparse::_caps
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ____caps_17;

public:
	inline static int32_t get_offset_of__caps_17() { return static_cast<int32_t>(offsetof(MatchSparse_t73BEE39B7EBE30B7460558DCA846B704C94B571C, ____caps_17)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get__caps_17() const { return ____caps_17; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of__caps_17() { return &____caps_17; }
	inline void set__caps_17(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		____caps_17 = value;
		Il2CppCodeGenWriteBarrier((&____caps_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATCHSPARSE_T73BEE39B7EBE30B7460558DCA846B704C94B571C_H
#ifndef REGEXMATCHTIMEOUTEXCEPTION_T78D3102CF3A9DEE18561827EDD878176482A6C7C_H
#define REGEXMATCHTIMEOUTEXCEPTION_T78D3102CF3A9DEE18561827EDD878176482A6C7C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexMatchTimeoutException
struct  RegexMatchTimeoutException_t78D3102CF3A9DEE18561827EDD878176482A6C7C  : public TimeoutException_t15A6E9A2A5819966712B5CFAF756BAEA40E3B1B7
{
public:
	// System.String System.Text.RegularExpressions.RegexMatchTimeoutException::regexInput
	String_t* ___regexInput_17;
	// System.String System.Text.RegularExpressions.RegexMatchTimeoutException::regexPattern
	String_t* ___regexPattern_18;
	// System.TimeSpan System.Text.RegularExpressions.RegexMatchTimeoutException::matchTimeout
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___matchTimeout_19;

public:
	inline static int32_t get_offset_of_regexInput_17() { return static_cast<int32_t>(offsetof(RegexMatchTimeoutException_t78D3102CF3A9DEE18561827EDD878176482A6C7C, ___regexInput_17)); }
	inline String_t* get_regexInput_17() const { return ___regexInput_17; }
	inline String_t** get_address_of_regexInput_17() { return &___regexInput_17; }
	inline void set_regexInput_17(String_t* value)
	{
		___regexInput_17 = value;
		Il2CppCodeGenWriteBarrier((&___regexInput_17), value);
	}

	inline static int32_t get_offset_of_regexPattern_18() { return static_cast<int32_t>(offsetof(RegexMatchTimeoutException_t78D3102CF3A9DEE18561827EDD878176482A6C7C, ___regexPattern_18)); }
	inline String_t* get_regexPattern_18() const { return ___regexPattern_18; }
	inline String_t** get_address_of_regexPattern_18() { return &___regexPattern_18; }
	inline void set_regexPattern_18(String_t* value)
	{
		___regexPattern_18 = value;
		Il2CppCodeGenWriteBarrier((&___regexPattern_18), value);
	}

	inline static int32_t get_offset_of_matchTimeout_19() { return static_cast<int32_t>(offsetof(RegexMatchTimeoutException_t78D3102CF3A9DEE18561827EDD878176482A6C7C, ___matchTimeout_19)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_matchTimeout_19() const { return ___matchTimeout_19; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_matchTimeout_19() { return &___matchTimeout_19; }
	inline void set_matchTimeout_19(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___matchTimeout_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGEXMATCHTIMEOUTEXCEPTION_T78D3102CF3A9DEE18561827EDD878176482A6C7C_H
#ifndef REGEXNODE_TF92FC16590D5B00965BABFC709BA677DA0FC3F75_H
#define REGEXNODE_TF92FC16590D5B00965BABFC709BA677DA0FC3F75_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexNode
struct  RegexNode_tF92FC16590D5B00965BABFC709BA677DA0FC3F75  : public RuntimeObject
{
public:
	// System.Int32 System.Text.RegularExpressions.RegexNode::_type
	int32_t ____type_0;
	// System.Collections.Generic.List`1<System.Text.RegularExpressions.RegexNode> System.Text.RegularExpressions.RegexNode::_children
	List_1_tA5CDE89671B691180A7422F86077A0D047AD4059 * ____children_1;
	// System.String System.Text.RegularExpressions.RegexNode::_str
	String_t* ____str_2;
	// System.Char System.Text.RegularExpressions.RegexNode::_ch
	Il2CppChar ____ch_3;
	// System.Int32 System.Text.RegularExpressions.RegexNode::_m
	int32_t ____m_4;
	// System.Int32 System.Text.RegularExpressions.RegexNode::_n
	int32_t ____n_5;
	// System.Text.RegularExpressions.RegexOptions System.Text.RegularExpressions.RegexNode::_options
	int32_t ____options_6;
	// System.Text.RegularExpressions.RegexNode System.Text.RegularExpressions.RegexNode::_next
	RegexNode_tF92FC16590D5B00965BABFC709BA677DA0FC3F75 * ____next_7;

public:
	inline static int32_t get_offset_of__type_0() { return static_cast<int32_t>(offsetof(RegexNode_tF92FC16590D5B00965BABFC709BA677DA0FC3F75, ____type_0)); }
	inline int32_t get__type_0() const { return ____type_0; }
	inline int32_t* get_address_of__type_0() { return &____type_0; }
	inline void set__type_0(int32_t value)
	{
		____type_0 = value;
	}

	inline static int32_t get_offset_of__children_1() { return static_cast<int32_t>(offsetof(RegexNode_tF92FC16590D5B00965BABFC709BA677DA0FC3F75, ____children_1)); }
	inline List_1_tA5CDE89671B691180A7422F86077A0D047AD4059 * get__children_1() const { return ____children_1; }
	inline List_1_tA5CDE89671B691180A7422F86077A0D047AD4059 ** get_address_of__children_1() { return &____children_1; }
	inline void set__children_1(List_1_tA5CDE89671B691180A7422F86077A0D047AD4059 * value)
	{
		____children_1 = value;
		Il2CppCodeGenWriteBarrier((&____children_1), value);
	}

	inline static int32_t get_offset_of__str_2() { return static_cast<int32_t>(offsetof(RegexNode_tF92FC16590D5B00965BABFC709BA677DA0FC3F75, ____str_2)); }
	inline String_t* get__str_2() const { return ____str_2; }
	inline String_t** get_address_of__str_2() { return &____str_2; }
	inline void set__str_2(String_t* value)
	{
		____str_2 = value;
		Il2CppCodeGenWriteBarrier((&____str_2), value);
	}

	inline static int32_t get_offset_of__ch_3() { return static_cast<int32_t>(offsetof(RegexNode_tF92FC16590D5B00965BABFC709BA677DA0FC3F75, ____ch_3)); }
	inline Il2CppChar get__ch_3() const { return ____ch_3; }
	inline Il2CppChar* get_address_of__ch_3() { return &____ch_3; }
	inline void set__ch_3(Il2CppChar value)
	{
		____ch_3 = value;
	}

	inline static int32_t get_offset_of__m_4() { return static_cast<int32_t>(offsetof(RegexNode_tF92FC16590D5B00965BABFC709BA677DA0FC3F75, ____m_4)); }
	inline int32_t get__m_4() const { return ____m_4; }
	inline int32_t* get_address_of__m_4() { return &____m_4; }
	inline void set__m_4(int32_t value)
	{
		____m_4 = value;
	}

	inline static int32_t get_offset_of__n_5() { return static_cast<int32_t>(offsetof(RegexNode_tF92FC16590D5B00965BABFC709BA677DA0FC3F75, ____n_5)); }
	inline int32_t get__n_5() const { return ____n_5; }
	inline int32_t* get_address_of__n_5() { return &____n_5; }
	inline void set__n_5(int32_t value)
	{
		____n_5 = value;
	}

	inline static int32_t get_offset_of__options_6() { return static_cast<int32_t>(offsetof(RegexNode_tF92FC16590D5B00965BABFC709BA677DA0FC3F75, ____options_6)); }
	inline int32_t get__options_6() const { return ____options_6; }
	inline int32_t* get_address_of__options_6() { return &____options_6; }
	inline void set__options_6(int32_t value)
	{
		____options_6 = value;
	}

	inline static int32_t get_offset_of__next_7() { return static_cast<int32_t>(offsetof(RegexNode_tF92FC16590D5B00965BABFC709BA677DA0FC3F75, ____next_7)); }
	inline RegexNode_tF92FC16590D5B00965BABFC709BA677DA0FC3F75 * get__next_7() const { return ____next_7; }
	inline RegexNode_tF92FC16590D5B00965BABFC709BA677DA0FC3F75 ** get_address_of__next_7() { return &____next_7; }
	inline void set__next_7(RegexNode_tF92FC16590D5B00965BABFC709BA677DA0FC3F75 * value)
	{
		____next_7 = value;
		Il2CppCodeGenWriteBarrier((&____next_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGEXNODE_TF92FC16590D5B00965BABFC709BA677DA0FC3F75_H
#ifndef REGEXPARSER_T9576D89D31260EF04DE583287FFC127132051FEE_H
#define REGEXPARSER_T9576D89D31260EF04DE583287FFC127132051FEE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexParser
struct  RegexParser_t9576D89D31260EF04DE583287FFC127132051FEE  : public RuntimeObject
{
public:
	// System.Text.RegularExpressions.RegexNode System.Text.RegularExpressions.RegexParser::_stack
	RegexNode_tF92FC16590D5B00965BABFC709BA677DA0FC3F75 * ____stack_0;
	// System.Text.RegularExpressions.RegexNode System.Text.RegularExpressions.RegexParser::_group
	RegexNode_tF92FC16590D5B00965BABFC709BA677DA0FC3F75 * ____group_1;
	// System.Text.RegularExpressions.RegexNode System.Text.RegularExpressions.RegexParser::_alternation
	RegexNode_tF92FC16590D5B00965BABFC709BA677DA0FC3F75 * ____alternation_2;
	// System.Text.RegularExpressions.RegexNode System.Text.RegularExpressions.RegexParser::_concatenation
	RegexNode_tF92FC16590D5B00965BABFC709BA677DA0FC3F75 * ____concatenation_3;
	// System.Text.RegularExpressions.RegexNode System.Text.RegularExpressions.RegexParser::_unit
	RegexNode_tF92FC16590D5B00965BABFC709BA677DA0FC3F75 * ____unit_4;
	// System.String System.Text.RegularExpressions.RegexParser::_pattern
	String_t* ____pattern_5;
	// System.Int32 System.Text.RegularExpressions.RegexParser::_currentPos
	int32_t ____currentPos_6;
	// System.Globalization.CultureInfo System.Text.RegularExpressions.RegexParser::_culture
	CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * ____culture_7;
	// System.Int32 System.Text.RegularExpressions.RegexParser::_autocap
	int32_t ____autocap_8;
	// System.Int32 System.Text.RegularExpressions.RegexParser::_capcount
	int32_t ____capcount_9;
	// System.Int32 System.Text.RegularExpressions.RegexParser::_captop
	int32_t ____captop_10;
	// System.Int32 System.Text.RegularExpressions.RegexParser::_capsize
	int32_t ____capsize_11;
	// System.Collections.Hashtable System.Text.RegularExpressions.RegexParser::_caps
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ____caps_12;
	// System.Collections.Hashtable System.Text.RegularExpressions.RegexParser::_capnames
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ____capnames_13;
	// System.Int32[] System.Text.RegularExpressions.RegexParser::_capnumlist
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ____capnumlist_14;
	// System.Collections.Generic.List`1<System.String> System.Text.RegularExpressions.RegexParser::_capnamelist
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ____capnamelist_15;
	// System.Text.RegularExpressions.RegexOptions System.Text.RegularExpressions.RegexParser::_options
	int32_t ____options_16;
	// System.Collections.Generic.List`1<System.Text.RegularExpressions.RegexOptions> System.Text.RegularExpressions.RegexParser::_optionsStack
	List_1_t85142A16ADC23C13E223599A626015FD40FF076A * ____optionsStack_17;
	// System.Boolean System.Text.RegularExpressions.RegexParser::_ignoreNextParen
	bool ____ignoreNextParen_18;

public:
	inline static int32_t get_offset_of__stack_0() { return static_cast<int32_t>(offsetof(RegexParser_t9576D89D31260EF04DE583287FFC127132051FEE, ____stack_0)); }
	inline RegexNode_tF92FC16590D5B00965BABFC709BA677DA0FC3F75 * get__stack_0() const { return ____stack_0; }
	inline RegexNode_tF92FC16590D5B00965BABFC709BA677DA0FC3F75 ** get_address_of__stack_0() { return &____stack_0; }
	inline void set__stack_0(RegexNode_tF92FC16590D5B00965BABFC709BA677DA0FC3F75 * value)
	{
		____stack_0 = value;
		Il2CppCodeGenWriteBarrier((&____stack_0), value);
	}

	inline static int32_t get_offset_of__group_1() { return static_cast<int32_t>(offsetof(RegexParser_t9576D89D31260EF04DE583287FFC127132051FEE, ____group_1)); }
	inline RegexNode_tF92FC16590D5B00965BABFC709BA677DA0FC3F75 * get__group_1() const { return ____group_1; }
	inline RegexNode_tF92FC16590D5B00965BABFC709BA677DA0FC3F75 ** get_address_of__group_1() { return &____group_1; }
	inline void set__group_1(RegexNode_tF92FC16590D5B00965BABFC709BA677DA0FC3F75 * value)
	{
		____group_1 = value;
		Il2CppCodeGenWriteBarrier((&____group_1), value);
	}

	inline static int32_t get_offset_of__alternation_2() { return static_cast<int32_t>(offsetof(RegexParser_t9576D89D31260EF04DE583287FFC127132051FEE, ____alternation_2)); }
	inline RegexNode_tF92FC16590D5B00965BABFC709BA677DA0FC3F75 * get__alternation_2() const { return ____alternation_2; }
	inline RegexNode_tF92FC16590D5B00965BABFC709BA677DA0FC3F75 ** get_address_of__alternation_2() { return &____alternation_2; }
	inline void set__alternation_2(RegexNode_tF92FC16590D5B00965BABFC709BA677DA0FC3F75 * value)
	{
		____alternation_2 = value;
		Il2CppCodeGenWriteBarrier((&____alternation_2), value);
	}

	inline static int32_t get_offset_of__concatenation_3() { return static_cast<int32_t>(offsetof(RegexParser_t9576D89D31260EF04DE583287FFC127132051FEE, ____concatenation_3)); }
	inline RegexNode_tF92FC16590D5B00965BABFC709BA677DA0FC3F75 * get__concatenation_3() const { return ____concatenation_3; }
	inline RegexNode_tF92FC16590D5B00965BABFC709BA677DA0FC3F75 ** get_address_of__concatenation_3() { return &____concatenation_3; }
	inline void set__concatenation_3(RegexNode_tF92FC16590D5B00965BABFC709BA677DA0FC3F75 * value)
	{
		____concatenation_3 = value;
		Il2CppCodeGenWriteBarrier((&____concatenation_3), value);
	}

	inline static int32_t get_offset_of__unit_4() { return static_cast<int32_t>(offsetof(RegexParser_t9576D89D31260EF04DE583287FFC127132051FEE, ____unit_4)); }
	inline RegexNode_tF92FC16590D5B00965BABFC709BA677DA0FC3F75 * get__unit_4() const { return ____unit_4; }
	inline RegexNode_tF92FC16590D5B00965BABFC709BA677DA0FC3F75 ** get_address_of__unit_4() { return &____unit_4; }
	inline void set__unit_4(RegexNode_tF92FC16590D5B00965BABFC709BA677DA0FC3F75 * value)
	{
		____unit_4 = value;
		Il2CppCodeGenWriteBarrier((&____unit_4), value);
	}

	inline static int32_t get_offset_of__pattern_5() { return static_cast<int32_t>(offsetof(RegexParser_t9576D89D31260EF04DE583287FFC127132051FEE, ____pattern_5)); }
	inline String_t* get__pattern_5() const { return ____pattern_5; }
	inline String_t** get_address_of__pattern_5() { return &____pattern_5; }
	inline void set__pattern_5(String_t* value)
	{
		____pattern_5 = value;
		Il2CppCodeGenWriteBarrier((&____pattern_5), value);
	}

	inline static int32_t get_offset_of__currentPos_6() { return static_cast<int32_t>(offsetof(RegexParser_t9576D89D31260EF04DE583287FFC127132051FEE, ____currentPos_6)); }
	inline int32_t get__currentPos_6() const { return ____currentPos_6; }
	inline int32_t* get_address_of__currentPos_6() { return &____currentPos_6; }
	inline void set__currentPos_6(int32_t value)
	{
		____currentPos_6 = value;
	}

	inline static int32_t get_offset_of__culture_7() { return static_cast<int32_t>(offsetof(RegexParser_t9576D89D31260EF04DE583287FFC127132051FEE, ____culture_7)); }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * get__culture_7() const { return ____culture_7; }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F ** get_address_of__culture_7() { return &____culture_7; }
	inline void set__culture_7(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * value)
	{
		____culture_7 = value;
		Il2CppCodeGenWriteBarrier((&____culture_7), value);
	}

	inline static int32_t get_offset_of__autocap_8() { return static_cast<int32_t>(offsetof(RegexParser_t9576D89D31260EF04DE583287FFC127132051FEE, ____autocap_8)); }
	inline int32_t get__autocap_8() const { return ____autocap_8; }
	inline int32_t* get_address_of__autocap_8() { return &____autocap_8; }
	inline void set__autocap_8(int32_t value)
	{
		____autocap_8 = value;
	}

	inline static int32_t get_offset_of__capcount_9() { return static_cast<int32_t>(offsetof(RegexParser_t9576D89D31260EF04DE583287FFC127132051FEE, ____capcount_9)); }
	inline int32_t get__capcount_9() const { return ____capcount_9; }
	inline int32_t* get_address_of__capcount_9() { return &____capcount_9; }
	inline void set__capcount_9(int32_t value)
	{
		____capcount_9 = value;
	}

	inline static int32_t get_offset_of__captop_10() { return static_cast<int32_t>(offsetof(RegexParser_t9576D89D31260EF04DE583287FFC127132051FEE, ____captop_10)); }
	inline int32_t get__captop_10() const { return ____captop_10; }
	inline int32_t* get_address_of__captop_10() { return &____captop_10; }
	inline void set__captop_10(int32_t value)
	{
		____captop_10 = value;
	}

	inline static int32_t get_offset_of__capsize_11() { return static_cast<int32_t>(offsetof(RegexParser_t9576D89D31260EF04DE583287FFC127132051FEE, ____capsize_11)); }
	inline int32_t get__capsize_11() const { return ____capsize_11; }
	inline int32_t* get_address_of__capsize_11() { return &____capsize_11; }
	inline void set__capsize_11(int32_t value)
	{
		____capsize_11 = value;
	}

	inline static int32_t get_offset_of__caps_12() { return static_cast<int32_t>(offsetof(RegexParser_t9576D89D31260EF04DE583287FFC127132051FEE, ____caps_12)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get__caps_12() const { return ____caps_12; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of__caps_12() { return &____caps_12; }
	inline void set__caps_12(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		____caps_12 = value;
		Il2CppCodeGenWriteBarrier((&____caps_12), value);
	}

	inline static int32_t get_offset_of__capnames_13() { return static_cast<int32_t>(offsetof(RegexParser_t9576D89D31260EF04DE583287FFC127132051FEE, ____capnames_13)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get__capnames_13() const { return ____capnames_13; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of__capnames_13() { return &____capnames_13; }
	inline void set__capnames_13(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		____capnames_13 = value;
		Il2CppCodeGenWriteBarrier((&____capnames_13), value);
	}

	inline static int32_t get_offset_of__capnumlist_14() { return static_cast<int32_t>(offsetof(RegexParser_t9576D89D31260EF04DE583287FFC127132051FEE, ____capnumlist_14)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get__capnumlist_14() const { return ____capnumlist_14; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of__capnumlist_14() { return &____capnumlist_14; }
	inline void set__capnumlist_14(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		____capnumlist_14 = value;
		Il2CppCodeGenWriteBarrier((&____capnumlist_14), value);
	}

	inline static int32_t get_offset_of__capnamelist_15() { return static_cast<int32_t>(offsetof(RegexParser_t9576D89D31260EF04DE583287FFC127132051FEE, ____capnamelist_15)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get__capnamelist_15() const { return ____capnamelist_15; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of__capnamelist_15() { return &____capnamelist_15; }
	inline void set__capnamelist_15(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		____capnamelist_15 = value;
		Il2CppCodeGenWriteBarrier((&____capnamelist_15), value);
	}

	inline static int32_t get_offset_of__options_16() { return static_cast<int32_t>(offsetof(RegexParser_t9576D89D31260EF04DE583287FFC127132051FEE, ____options_16)); }
	inline int32_t get__options_16() const { return ____options_16; }
	inline int32_t* get_address_of__options_16() { return &____options_16; }
	inline void set__options_16(int32_t value)
	{
		____options_16 = value;
	}

	inline static int32_t get_offset_of__optionsStack_17() { return static_cast<int32_t>(offsetof(RegexParser_t9576D89D31260EF04DE583287FFC127132051FEE, ____optionsStack_17)); }
	inline List_1_t85142A16ADC23C13E223599A626015FD40FF076A * get__optionsStack_17() const { return ____optionsStack_17; }
	inline List_1_t85142A16ADC23C13E223599A626015FD40FF076A ** get_address_of__optionsStack_17() { return &____optionsStack_17; }
	inline void set__optionsStack_17(List_1_t85142A16ADC23C13E223599A626015FD40FF076A * value)
	{
		____optionsStack_17 = value;
		Il2CppCodeGenWriteBarrier((&____optionsStack_17), value);
	}

	inline static int32_t get_offset_of__ignoreNextParen_18() { return static_cast<int32_t>(offsetof(RegexParser_t9576D89D31260EF04DE583287FFC127132051FEE, ____ignoreNextParen_18)); }
	inline bool get__ignoreNextParen_18() const { return ____ignoreNextParen_18; }
	inline bool* get_address_of__ignoreNextParen_18() { return &____ignoreNextParen_18; }
	inline void set__ignoreNextParen_18(bool value)
	{
		____ignoreNextParen_18 = value;
	}
};

struct RegexParser_t9576D89D31260EF04DE583287FFC127132051FEE_StaticFields
{
public:
	// System.Byte[] System.Text.RegularExpressions.RegexParser::_category
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____category_19;

public:
	inline static int32_t get_offset_of__category_19() { return static_cast<int32_t>(offsetof(RegexParser_t9576D89D31260EF04DE583287FFC127132051FEE_StaticFields, ____category_19)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__category_19() const { return ____category_19; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__category_19() { return &____category_19; }
	inline void set__category_19(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____category_19 = value;
		Il2CppCodeGenWriteBarrier((&____category_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGEXPARSER_T9576D89D31260EF04DE583287FFC127132051FEE_H
#ifndef REGEXTREE_T8FE2EC649AB50FDA90239EA1410A881F278B47B6_H
#define REGEXTREE_T8FE2EC649AB50FDA90239EA1410A881F278B47B6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexTree
struct  RegexTree_t8FE2EC649AB50FDA90239EA1410A881F278B47B6  : public RuntimeObject
{
public:
	// System.Text.RegularExpressions.RegexNode System.Text.RegularExpressions.RegexTree::_root
	RegexNode_tF92FC16590D5B00965BABFC709BA677DA0FC3F75 * ____root_0;
	// System.Collections.Hashtable System.Text.RegularExpressions.RegexTree::_caps
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ____caps_1;
	// System.Int32[] System.Text.RegularExpressions.RegexTree::_capnumlist
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ____capnumlist_2;
	// System.Collections.Hashtable System.Text.RegularExpressions.RegexTree::_capnames
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ____capnames_3;
	// System.String[] System.Text.RegularExpressions.RegexTree::_capslist
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ____capslist_4;
	// System.Text.RegularExpressions.RegexOptions System.Text.RegularExpressions.RegexTree::_options
	int32_t ____options_5;
	// System.Int32 System.Text.RegularExpressions.RegexTree::_captop
	int32_t ____captop_6;

public:
	inline static int32_t get_offset_of__root_0() { return static_cast<int32_t>(offsetof(RegexTree_t8FE2EC649AB50FDA90239EA1410A881F278B47B6, ____root_0)); }
	inline RegexNode_tF92FC16590D5B00965BABFC709BA677DA0FC3F75 * get__root_0() const { return ____root_0; }
	inline RegexNode_tF92FC16590D5B00965BABFC709BA677DA0FC3F75 ** get_address_of__root_0() { return &____root_0; }
	inline void set__root_0(RegexNode_tF92FC16590D5B00965BABFC709BA677DA0FC3F75 * value)
	{
		____root_0 = value;
		Il2CppCodeGenWriteBarrier((&____root_0), value);
	}

	inline static int32_t get_offset_of__caps_1() { return static_cast<int32_t>(offsetof(RegexTree_t8FE2EC649AB50FDA90239EA1410A881F278B47B6, ____caps_1)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get__caps_1() const { return ____caps_1; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of__caps_1() { return &____caps_1; }
	inline void set__caps_1(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		____caps_1 = value;
		Il2CppCodeGenWriteBarrier((&____caps_1), value);
	}

	inline static int32_t get_offset_of__capnumlist_2() { return static_cast<int32_t>(offsetof(RegexTree_t8FE2EC649AB50FDA90239EA1410A881F278B47B6, ____capnumlist_2)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get__capnumlist_2() const { return ____capnumlist_2; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of__capnumlist_2() { return &____capnumlist_2; }
	inline void set__capnumlist_2(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		____capnumlist_2 = value;
		Il2CppCodeGenWriteBarrier((&____capnumlist_2), value);
	}

	inline static int32_t get_offset_of__capnames_3() { return static_cast<int32_t>(offsetof(RegexTree_t8FE2EC649AB50FDA90239EA1410A881F278B47B6, ____capnames_3)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get__capnames_3() const { return ____capnames_3; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of__capnames_3() { return &____capnames_3; }
	inline void set__capnames_3(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		____capnames_3 = value;
		Il2CppCodeGenWriteBarrier((&____capnames_3), value);
	}

	inline static int32_t get_offset_of__capslist_4() { return static_cast<int32_t>(offsetof(RegexTree_t8FE2EC649AB50FDA90239EA1410A881F278B47B6, ____capslist_4)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get__capslist_4() const { return ____capslist_4; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of__capslist_4() { return &____capslist_4; }
	inline void set__capslist_4(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		____capslist_4 = value;
		Il2CppCodeGenWriteBarrier((&____capslist_4), value);
	}

	inline static int32_t get_offset_of__options_5() { return static_cast<int32_t>(offsetof(RegexTree_t8FE2EC649AB50FDA90239EA1410A881F278B47B6, ____options_5)); }
	inline int32_t get__options_5() const { return ____options_5; }
	inline int32_t* get_address_of__options_5() { return &____options_5; }
	inline void set__options_5(int32_t value)
	{
		____options_5 = value;
	}

	inline static int32_t get_offset_of__captop_6() { return static_cast<int32_t>(offsetof(RegexTree_t8FE2EC649AB50FDA90239EA1410A881F278B47B6, ____captop_6)); }
	inline int32_t get__captop_6() const { return ____captop_6; }
	inline int32_t* get_address_of__captop_6() { return &____captop_6; }
	inline void set__captop_6(int32_t value)
	{
		____captop_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGEXTREE_T8FE2EC649AB50FDA90239EA1410A881F278B47B6_H
#ifndef ADDINGNEWEVENTHANDLER_TC4A113DB05465EF2A66E68EF98945E280F25C1FE_H
#define ADDINGNEWEVENTHANDLER_TC4A113DB05465EF2A66E68EF98945E280F25C1FE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.AddingNewEventHandler
struct  AddingNewEventHandler_tC4A113DB05465EF2A66E68EF98945E280F25C1FE  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADDINGNEWEVENTHANDLER_TC4A113DB05465EF2A66E68EF98945E280F25C1FE_H
#ifndef ARRAYCONVERTER_TAAD8F39711C6ECD39D31226FA1D140DD38752B57_H
#define ARRAYCONVERTER_TAAD8F39711C6ECD39D31226FA1D140DD38752B57_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.ArrayConverter
struct  ArrayConverter_tAAD8F39711C6ECD39D31226FA1D140DD38752B57  : public CollectionConverter_t039E15C433996B0F0F0EB78BEE81F6DE0705F184
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRAYCONVERTER_TAAD8F39711C6ECD39D31226FA1D140DD38752B57_H
#ifndef ASYNCCOMPLETEDEVENTHANDLER_TEC686A4F246B63CF3F780209D5DAFA051DF81DB6_H
#define ASYNCCOMPLETEDEVENTHANDLER_TEC686A4F246B63CF3F780209D5DAFA051DF81DB6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.AsyncCompletedEventHandler
struct  AsyncCompletedEventHandler_tEC686A4F246B63CF3F780209D5DAFA051DF81DB6  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCCOMPLETEDEVENTHANDLER_TEC686A4F246B63CF3F780209D5DAFA051DF81DB6_H
#ifndef WORKERTHREADSTARTDELEGATE_TEC42174597C1FB97F214AE61808E6F0CE75BF7B6_H
#define WORKERTHREADSTARTDELEGATE_TEC42174597C1FB97F214AE61808E6F0CE75BF7B6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.BackgroundWorker_WorkerThreadStartDelegate
struct  WorkerThreadStartDelegate_tEC42174597C1FB97F214AE61808E6F0CE75BF7B6  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORKERTHREADSTARTDELEGATE_TEC42174597C1FB97F214AE61808E6F0CE75BF7B6_H
#ifndef BYTECONVERTER_T59E5742D740228F5B9AB91169C2EA907A023328A_H
#define BYTECONVERTER_T59E5742D740228F5B9AB91169C2EA907A023328A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.ByteConverter
struct  ByteConverter_t59E5742D740228F5B9AB91169C2EA907A023328A  : public BaseNumberConverter_t6AF36A2503E7BABF7FB9A8EC05DF8B828491AC63
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTECONVERTER_T59E5742D740228F5B9AB91169C2EA907A023328A_H
#ifndef CANCELEVENTHANDLER_T0F5842A94E02DFA026B793A034C1AA362D6D0A2B_H
#define CANCELEVENTHANDLER_T0F5842A94E02DFA026B793A034C1AA362D6D0A2B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.CancelEventHandler
struct  CancelEventHandler_t0F5842A94E02DFA026B793A034C1AA362D6D0A2B  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANCELEVENTHANDLER_T0F5842A94E02DFA026B793A034C1AA362D6D0A2B_H
#ifndef COLLECTIONCHANGEEVENTHANDLER_T8AE127943B452074A0FE2755F20B399D733EBCB2_H
#define COLLECTIONCHANGEEVENTHANDLER_T8AE127943B452074A0FE2755F20B399D733EBCB2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.CollectionChangeEventHandler
struct  CollectionChangeEventHandler_t8AE127943B452074A0FE2755F20B399D733EBCB2  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTIONCHANGEEVENTHANDLER_T8AE127943B452074A0FE2755F20B399D733EBCB2_H
#ifndef COMPONENTCONVERTER_TAFCE49784F59197CB5E92C8ED566B069D1A5766E_H
#define COMPONENTCONVERTER_TAFCE49784F59197CB5E92C8ED566B069D1A5766E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.ComponentConverter
struct  ComponentConverter_tAFCE49784F59197CB5E92C8ED566B069D1A5766E  : public ReferenceConverter_t5080472EE999A1F00721E6C5C97013762C85C7E4
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENTCONVERTER_TAFCE49784F59197CB5E92C8ED566B069D1A5766E_H
#ifndef COMPONENTRESOURCEMANAGER_T48833C1DFC68FB7031F4E102B50BE346D8811065_H
#define COMPONENTRESOURCEMANAGER_T48833C1DFC68FB7031F4E102B50BE346D8811065_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.ComponentResourceManager
struct  ComponentResourceManager_t48833C1DFC68FB7031F4E102B50BE346D8811065  : public ResourceManager_t966CE0B6B59F36DD8797BDC20B5EEFACE0A883FF
{
public:
	// System.Collections.Hashtable System.ComponentModel.ComponentResourceManager::_resourceSets
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ____resourceSets_25;
	// System.Globalization.CultureInfo System.ComponentModel.ComponentResourceManager::_neutralResourcesCulture
	CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * ____neutralResourcesCulture_26;

public:
	inline static int32_t get_offset_of__resourceSets_25() { return static_cast<int32_t>(offsetof(ComponentResourceManager_t48833C1DFC68FB7031F4E102B50BE346D8811065, ____resourceSets_25)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get__resourceSets_25() const { return ____resourceSets_25; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of__resourceSets_25() { return &____resourceSets_25; }
	inline void set__resourceSets_25(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		____resourceSets_25 = value;
		Il2CppCodeGenWriteBarrier((&____resourceSets_25), value);
	}

	inline static int32_t get_offset_of__neutralResourcesCulture_26() { return static_cast<int32_t>(offsetof(ComponentResourceManager_t48833C1DFC68FB7031F4E102B50BE346D8811065, ____neutralResourcesCulture_26)); }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * get__neutralResourcesCulture_26() const { return ____neutralResourcesCulture_26; }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F ** get_address_of__neutralResourcesCulture_26() { return &____neutralResourcesCulture_26; }
	inline void set__neutralResourcesCulture_26(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * value)
	{
		____neutralResourcesCulture_26 = value;
		Il2CppCodeGenWriteBarrier((&____neutralResourcesCulture_26), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENTRESOURCEMANAGER_T48833C1DFC68FB7031F4E102B50BE346D8811065_H
#ifndef DECIMALCONVERTER_T10232B897580B6DE599BB375BE8C0F4E1C30B0C1_H
#define DECIMALCONVERTER_T10232B897580B6DE599BB375BE8C0F4E1C30B0C1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.DecimalConverter
struct  DecimalConverter_t10232B897580B6DE599BB375BE8C0F4E1C30B0C1  : public BaseNumberConverter_t6AF36A2503E7BABF7FB9A8EC05DF8B828491AC63
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECIMALCONVERTER_T10232B897580B6DE599BB375BE8C0F4E1C30B0C1_H
#ifndef DEFAULTTRACELISTENER_T1B44C909F5A4023CCFB5B885406F9EC2FE5C5B7D_H
#define DEFAULTTRACELISTENER_T1B44C909F5A4023CCFB5B885406F9EC2FE5C5B7D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.DefaultTraceListener
struct  DefaultTraceListener_t1B44C909F5A4023CCFB5B885406F9EC2FE5C5B7D  : public TraceListener_tAA208D26D2197E835400A6EE284410A35F436111
{
public:
	// System.String System.Diagnostics.DefaultTraceListener::logFileName
	String_t* ___logFileName_10;

public:
	inline static int32_t get_offset_of_logFileName_10() { return static_cast<int32_t>(offsetof(DefaultTraceListener_t1B44C909F5A4023CCFB5B885406F9EC2FE5C5B7D, ___logFileName_10)); }
	inline String_t* get_logFileName_10() const { return ___logFileName_10; }
	inline String_t** get_address_of_logFileName_10() { return &___logFileName_10; }
	inline void set_logFileName_10(String_t* value)
	{
		___logFileName_10 = value;
		Il2CppCodeGenWriteBarrier((&___logFileName_10), value);
	}
};

struct DefaultTraceListener_t1B44C909F5A4023CCFB5B885406F9EC2FE5C5B7D_StaticFields
{
public:
	// System.Boolean System.Diagnostics.DefaultTraceListener::OnWin32
	bool ___OnWin32_7;
	// System.String System.Diagnostics.DefaultTraceListener::MonoTracePrefix
	String_t* ___MonoTracePrefix_8;
	// System.String System.Diagnostics.DefaultTraceListener::MonoTraceFile
	String_t* ___MonoTraceFile_9;

public:
	inline static int32_t get_offset_of_OnWin32_7() { return static_cast<int32_t>(offsetof(DefaultTraceListener_t1B44C909F5A4023CCFB5B885406F9EC2FE5C5B7D_StaticFields, ___OnWin32_7)); }
	inline bool get_OnWin32_7() const { return ___OnWin32_7; }
	inline bool* get_address_of_OnWin32_7() { return &___OnWin32_7; }
	inline void set_OnWin32_7(bool value)
	{
		___OnWin32_7 = value;
	}

	inline static int32_t get_offset_of_MonoTracePrefix_8() { return static_cast<int32_t>(offsetof(DefaultTraceListener_t1B44C909F5A4023CCFB5B885406F9EC2FE5C5B7D_StaticFields, ___MonoTracePrefix_8)); }
	inline String_t* get_MonoTracePrefix_8() const { return ___MonoTracePrefix_8; }
	inline String_t** get_address_of_MonoTracePrefix_8() { return &___MonoTracePrefix_8; }
	inline void set_MonoTracePrefix_8(String_t* value)
	{
		___MonoTracePrefix_8 = value;
		Il2CppCodeGenWriteBarrier((&___MonoTracePrefix_8), value);
	}

	inline static int32_t get_offset_of_MonoTraceFile_9() { return static_cast<int32_t>(offsetof(DefaultTraceListener_t1B44C909F5A4023CCFB5B885406F9EC2FE5C5B7D_StaticFields, ___MonoTraceFile_9)); }
	inline String_t* get_MonoTraceFile_9() const { return ___MonoTraceFile_9; }
	inline String_t** get_address_of_MonoTraceFile_9() { return &___MonoTraceFile_9; }
	inline void set_MonoTraceFile_9(String_t* value)
	{
		___MonoTraceFile_9 = value;
		Il2CppCodeGenWriteBarrier((&___MonoTraceFile_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTTRACELISTENER_T1B44C909F5A4023CCFB5B885406F9EC2FE5C5B7D_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2500 = { sizeof (MatchSparse_t73BEE39B7EBE30B7460558DCA846B704C94B571C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2500[1] = 
{
	MatchSparse_t73BEE39B7EBE30B7460558DCA846B704C94B571C::get_offset_of__caps_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2501 = { sizeof (MatchCollection_t8B954CE579727B076276B514F08040EE4D26844A), -1, sizeof(MatchCollection_t8B954CE579727B076276B514F08040EE4D26844A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2501[9] = 
{
	MatchCollection_t8B954CE579727B076276B514F08040EE4D26844A::get_offset_of__regex_0(),
	MatchCollection_t8B954CE579727B076276B514F08040EE4D26844A::get_offset_of__matches_1(),
	MatchCollection_t8B954CE579727B076276B514F08040EE4D26844A::get_offset_of__done_2(),
	MatchCollection_t8B954CE579727B076276B514F08040EE4D26844A::get_offset_of__input_3(),
	MatchCollection_t8B954CE579727B076276B514F08040EE4D26844A::get_offset_of__beginning_4(),
	MatchCollection_t8B954CE579727B076276B514F08040EE4D26844A::get_offset_of__length_5(),
	MatchCollection_t8B954CE579727B076276B514F08040EE4D26844A::get_offset_of__startat_6(),
	MatchCollection_t8B954CE579727B076276B514F08040EE4D26844A::get_offset_of__prevlen_7(),
	MatchCollection_t8B954CE579727B076276B514F08040EE4D26844A_StaticFields::get_offset_of_infinite_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2502 = { sizeof (MatchEnumerator_t1B798B870C2F61E31782D27D183A307A0A6E6FC4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2502[4] = 
{
	MatchEnumerator_t1B798B870C2F61E31782D27D183A307A0A6E6FC4::get_offset_of__matchcoll_0(),
	MatchEnumerator_t1B798B870C2F61E31782D27D183A307A0A6E6FC4::get_offset_of__match_1(),
	MatchEnumerator_t1B798B870C2F61E31782D27D183A307A0A6E6FC4::get_offset_of__curindex_2(),
	MatchEnumerator_t1B798B870C2F61E31782D27D183A307A0A6E6FC4::get_offset_of__done_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2503 = { sizeof (RegexMatchTimeoutException_t78D3102CF3A9DEE18561827EDD878176482A6C7C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2503[3] = 
{
	RegexMatchTimeoutException_t78D3102CF3A9DEE18561827EDD878176482A6C7C::get_offset_of_regexInput_17(),
	RegexMatchTimeoutException_t78D3102CF3A9DEE18561827EDD878176482A6C7C::get_offset_of_regexPattern_18(),
	RegexMatchTimeoutException_t78D3102CF3A9DEE18561827EDD878176482A6C7C::get_offset_of_matchTimeout_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2504 = { sizeof (RegexNode_tF92FC16590D5B00965BABFC709BA677DA0FC3F75), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2504[8] = 
{
	RegexNode_tF92FC16590D5B00965BABFC709BA677DA0FC3F75::get_offset_of__type_0(),
	RegexNode_tF92FC16590D5B00965BABFC709BA677DA0FC3F75::get_offset_of__children_1(),
	RegexNode_tF92FC16590D5B00965BABFC709BA677DA0FC3F75::get_offset_of__str_2(),
	RegexNode_tF92FC16590D5B00965BABFC709BA677DA0FC3F75::get_offset_of__ch_3(),
	RegexNode_tF92FC16590D5B00965BABFC709BA677DA0FC3F75::get_offset_of__m_4(),
	RegexNode_tF92FC16590D5B00965BABFC709BA677DA0FC3F75::get_offset_of__n_5(),
	RegexNode_tF92FC16590D5B00965BABFC709BA677DA0FC3F75::get_offset_of__options_6(),
	RegexNode_tF92FC16590D5B00965BABFC709BA677DA0FC3F75::get_offset_of__next_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2505 = { sizeof (RegexOptions_t9A6138CDA9C60924D503C584095349F008C52EA1)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2505[11] = 
{
	RegexOptions_t9A6138CDA9C60924D503C584095349F008C52EA1::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2506 = { sizeof (RegexParser_t9576D89D31260EF04DE583287FFC127132051FEE), -1, sizeof(RegexParser_t9576D89D31260EF04DE583287FFC127132051FEE_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2506[20] = 
{
	RegexParser_t9576D89D31260EF04DE583287FFC127132051FEE::get_offset_of__stack_0(),
	RegexParser_t9576D89D31260EF04DE583287FFC127132051FEE::get_offset_of__group_1(),
	RegexParser_t9576D89D31260EF04DE583287FFC127132051FEE::get_offset_of__alternation_2(),
	RegexParser_t9576D89D31260EF04DE583287FFC127132051FEE::get_offset_of__concatenation_3(),
	RegexParser_t9576D89D31260EF04DE583287FFC127132051FEE::get_offset_of__unit_4(),
	RegexParser_t9576D89D31260EF04DE583287FFC127132051FEE::get_offset_of__pattern_5(),
	RegexParser_t9576D89D31260EF04DE583287FFC127132051FEE::get_offset_of__currentPos_6(),
	RegexParser_t9576D89D31260EF04DE583287FFC127132051FEE::get_offset_of__culture_7(),
	RegexParser_t9576D89D31260EF04DE583287FFC127132051FEE::get_offset_of__autocap_8(),
	RegexParser_t9576D89D31260EF04DE583287FFC127132051FEE::get_offset_of__capcount_9(),
	RegexParser_t9576D89D31260EF04DE583287FFC127132051FEE::get_offset_of__captop_10(),
	RegexParser_t9576D89D31260EF04DE583287FFC127132051FEE::get_offset_of__capsize_11(),
	RegexParser_t9576D89D31260EF04DE583287FFC127132051FEE::get_offset_of__caps_12(),
	RegexParser_t9576D89D31260EF04DE583287FFC127132051FEE::get_offset_of__capnames_13(),
	RegexParser_t9576D89D31260EF04DE583287FFC127132051FEE::get_offset_of__capnumlist_14(),
	RegexParser_t9576D89D31260EF04DE583287FFC127132051FEE::get_offset_of__capnamelist_15(),
	RegexParser_t9576D89D31260EF04DE583287FFC127132051FEE::get_offset_of__options_16(),
	RegexParser_t9576D89D31260EF04DE583287FFC127132051FEE::get_offset_of__optionsStack_17(),
	RegexParser_t9576D89D31260EF04DE583287FFC127132051FEE::get_offset_of__ignoreNextParen_18(),
	RegexParser_t9576D89D31260EF04DE583287FFC127132051FEE_StaticFields::get_offset_of__category_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2507 = { sizeof (RegexReplacement_t2A1098B910D9E68191221914C5BBCBA813295359), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2507[3] = 
{
	RegexReplacement_t2A1098B910D9E68191221914C5BBCBA813295359::get_offset_of__rep_0(),
	RegexReplacement_t2A1098B910D9E68191221914C5BBCBA813295359::get_offset_of__strings_1(),
	RegexReplacement_t2A1098B910D9E68191221914C5BBCBA813295359::get_offset_of__rules_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2508 = { sizeof (RegexRunner_tBA888C4E3D3BA80EEE14878E4A330461730446B0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2508[18] = 
{
	RegexRunner_tBA888C4E3D3BA80EEE14878E4A330461730446B0::get_offset_of_runtextbeg_0(),
	RegexRunner_tBA888C4E3D3BA80EEE14878E4A330461730446B0::get_offset_of_runtextend_1(),
	RegexRunner_tBA888C4E3D3BA80EEE14878E4A330461730446B0::get_offset_of_runtextstart_2(),
	RegexRunner_tBA888C4E3D3BA80EEE14878E4A330461730446B0::get_offset_of_runtext_3(),
	RegexRunner_tBA888C4E3D3BA80EEE14878E4A330461730446B0::get_offset_of_runtextpos_4(),
	RegexRunner_tBA888C4E3D3BA80EEE14878E4A330461730446B0::get_offset_of_runtrack_5(),
	RegexRunner_tBA888C4E3D3BA80EEE14878E4A330461730446B0::get_offset_of_runtrackpos_6(),
	RegexRunner_tBA888C4E3D3BA80EEE14878E4A330461730446B0::get_offset_of_runstack_7(),
	RegexRunner_tBA888C4E3D3BA80EEE14878E4A330461730446B0::get_offset_of_runstackpos_8(),
	RegexRunner_tBA888C4E3D3BA80EEE14878E4A330461730446B0::get_offset_of_runcrawl_9(),
	RegexRunner_tBA888C4E3D3BA80EEE14878E4A330461730446B0::get_offset_of_runcrawlpos_10(),
	RegexRunner_tBA888C4E3D3BA80EEE14878E4A330461730446B0::get_offset_of_runtrackcount_11(),
	RegexRunner_tBA888C4E3D3BA80EEE14878E4A330461730446B0::get_offset_of_runmatch_12(),
	RegexRunner_tBA888C4E3D3BA80EEE14878E4A330461730446B0::get_offset_of_runregex_13(),
	RegexRunner_tBA888C4E3D3BA80EEE14878E4A330461730446B0::get_offset_of_timeout_14(),
	RegexRunner_tBA888C4E3D3BA80EEE14878E4A330461730446B0::get_offset_of_ignoreTimeout_15(),
	RegexRunner_tBA888C4E3D3BA80EEE14878E4A330461730446B0::get_offset_of_timeoutOccursAt_16(),
	RegexRunner_tBA888C4E3D3BA80EEE14878E4A330461730446B0::get_offset_of_timeoutChecksToSkip_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2509 = { sizeof (RegexRunnerFactory_t0703F390E2102623B0189DEC095DB182698E404B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2510 = { sizeof (RegexTree_t8FE2EC649AB50FDA90239EA1410A881F278B47B6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2510[7] = 
{
	RegexTree_t8FE2EC649AB50FDA90239EA1410A881F278B47B6::get_offset_of__root_0(),
	RegexTree_t8FE2EC649AB50FDA90239EA1410A881F278B47B6::get_offset_of__caps_1(),
	RegexTree_t8FE2EC649AB50FDA90239EA1410A881F278B47B6::get_offset_of__capnumlist_2(),
	RegexTree_t8FE2EC649AB50FDA90239EA1410A881F278B47B6::get_offset_of__capnames_3(),
	RegexTree_t8FE2EC649AB50FDA90239EA1410A881F278B47B6::get_offset_of__capslist_4(),
	RegexTree_t8FE2EC649AB50FDA90239EA1410A881F278B47B6::get_offset_of__options_5(),
	RegexTree_t8FE2EC649AB50FDA90239EA1410A881F278B47B6::get_offset_of__captop_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2511 = { sizeof (RegexWriter_t5F8CEA1FC9A4AC32A95BAC6E49EF51D7DF051AA6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2511[10] = 
{
	RegexWriter_t5F8CEA1FC9A4AC32A95BAC6E49EF51D7DF051AA6::get_offset_of__intStack_0(),
	RegexWriter_t5F8CEA1FC9A4AC32A95BAC6E49EF51D7DF051AA6::get_offset_of__depth_1(),
	RegexWriter_t5F8CEA1FC9A4AC32A95BAC6E49EF51D7DF051AA6::get_offset_of__emitted_2(),
	RegexWriter_t5F8CEA1FC9A4AC32A95BAC6E49EF51D7DF051AA6::get_offset_of__curpos_3(),
	RegexWriter_t5F8CEA1FC9A4AC32A95BAC6E49EF51D7DF051AA6::get_offset_of__stringhash_4(),
	RegexWriter_t5F8CEA1FC9A4AC32A95BAC6E49EF51D7DF051AA6::get_offset_of__stringtable_5(),
	RegexWriter_t5F8CEA1FC9A4AC32A95BAC6E49EF51D7DF051AA6::get_offset_of__counting_6(),
	RegexWriter_t5F8CEA1FC9A4AC32A95BAC6E49EF51D7DF051AA6::get_offset_of__count_7(),
	RegexWriter_t5F8CEA1FC9A4AC32A95BAC6E49EF51D7DF051AA6::get_offset_of__trackcount_8(),
	RegexWriter_t5F8CEA1FC9A4AC32A95BAC6E49EF51D7DF051AA6::get_offset_of__caps_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2512 = { sizeof (BooleanSwitch_t2B51F0E7C50E7DAA74282B51EF9107A2E54723E0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2513 = { sizeof (CorrelationManager_t791B7FE5E821DC5746198251B11EDB293CB60120), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2514 = { sizeof (Switch_t9DAA0B271A8AAEC0A2EFE1EE30299CECB4C2002F), -1, sizeof(Switch_t9DAA0B271A8AAEC0A2EFE1EE30299CECB4C2002F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2514[6] = 
{
	Switch_t9DAA0B271A8AAEC0A2EFE1EE30299CECB4C2002F::get_offset_of_description_0(),
	Switch_t9DAA0B271A8AAEC0A2EFE1EE30299CECB4C2002F::get_offset_of_displayName_1(),
	Switch_t9DAA0B271A8AAEC0A2EFE1EE30299CECB4C2002F::get_offset_of_switchValueString_2(),
	Switch_t9DAA0B271A8AAEC0A2EFE1EE30299CECB4C2002F::get_offset_of_defaultValue_3(),
	Switch_t9DAA0B271A8AAEC0A2EFE1EE30299CECB4C2002F_StaticFields::get_offset_of_switches_4(),
	Switch_t9DAA0B271A8AAEC0A2EFE1EE30299CECB4C2002F_StaticFields::get_offset_of_s_LastCollectionCount_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2515 = { sizeof (SwitchLevelAttribute_t5B9AF957556A203ACC71409CA1A08B4DE3D3EAA6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2515[1] = 
{
	SwitchLevelAttribute_t5B9AF957556A203ACC71409CA1A08B4DE3D3EAA6::get_offset_of_type_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2516 = { sizeof (Trace_t8A039E0C52E7CC7C3A4622E704FAA75479F12EEF), -1, sizeof(Trace_t8A039E0C52E7CC7C3A4622E704FAA75479F12EEF_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2516[1] = 
{
	Trace_t8A039E0C52E7CC7C3A4622E704FAA75479F12EEF_StaticFields::get_offset_of_correlationManager_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2517 = { sizeof (TraceEventCache_t69391771C5AACB85654A0AA5FBE8C86C45DA378D), -1, sizeof(TraceEventCache_t69391771C5AACB85654A0AA5FBE8C86C45DA378D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2517[5] = 
{
	TraceEventCache_t69391771C5AACB85654A0AA5FBE8C86C45DA378D_StaticFields::get_offset_of_processId_0(),
	TraceEventCache_t69391771C5AACB85654A0AA5FBE8C86C45DA378D_StaticFields::get_offset_of_processName_1(),
	TraceEventCache_t69391771C5AACB85654A0AA5FBE8C86C45DA378D::get_offset_of_timeStamp_2(),
	TraceEventCache_t69391771C5AACB85654A0AA5FBE8C86C45DA378D::get_offset_of_dateTime_3(),
	TraceEventCache_t69391771C5AACB85654A0AA5FBE8C86C45DA378D::get_offset_of_stackTrace_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2518 = { sizeof (TraceEventType_tBFCE92C8BA1E8E4A82F4AF951EC8F62C7CA68E4A)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2518[11] = 
{
	TraceEventType_tBFCE92C8BA1E8E4A82F4AF951EC8F62C7CA68E4A::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2519 = { sizeof (TraceFilter_t5BA76D3899B80AEA894D4040364099DC7C47C6F1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2520 = { sizeof (TraceInternal_t2995A83FC0EF1D60E6D049F69A5A658536C92952), -1, sizeof(TraceInternal_t2995A83FC0EF1D60E6D049F69A5A658536C92952_StaticFields), sizeof(TraceInternal_t2995A83FC0EF1D60E6D049F69A5A658536C92952_ThreadStaticFields) };
extern const int32_t g_FieldOffsetTable2520[6] = 
{
	TraceInternal_t2995A83FC0EF1D60E6D049F69A5A658536C92952_StaticFields::get_offset_of_appName_0(),
	TraceInternal_t2995A83FC0EF1D60E6D049F69A5A658536C92952_StaticFields::get_offset_of_listeners_1(),
	TraceInternal_t2995A83FC0EF1D60E6D049F69A5A658536C92952_StaticFields::get_offset_of_autoFlush_2(),
	TraceInternal_t2995A83FC0EF1D60E6D049F69A5A658536C92952_ThreadStaticFields::get_offset_of_indentLevel_3() | THREAD_LOCAL_STATIC_MASK,
	TraceInternal_t2995A83FC0EF1D60E6D049F69A5A658536C92952_StaticFields::get_offset_of_indentSize_4(),
	TraceInternal_t2995A83FC0EF1D60E6D049F69A5A658536C92952_StaticFields::get_offset_of_critSec_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2521 = { sizeof (TraceLevel_tF5EBC1CE1930BF1CD079855557C8CBD884D323B4)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2521[6] = 
{
	TraceLevel_tF5EBC1CE1930BF1CD079855557C8CBD884D323B4::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2522 = { sizeof (TraceListener_tAA208D26D2197E835400A6EE284410A35F436111), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2522[6] = 
{
	TraceListener_tAA208D26D2197E835400A6EE284410A35F436111::get_offset_of_indentLevel_1(),
	TraceListener_tAA208D26D2197E835400A6EE284410A35F436111::get_offset_of_indentSize_2(),
	TraceListener_tAA208D26D2197E835400A6EE284410A35F436111::get_offset_of_traceOptions_3(),
	TraceListener_tAA208D26D2197E835400A6EE284410A35F436111::get_offset_of_needIndent_4(),
	TraceListener_tAA208D26D2197E835400A6EE284410A35F436111::get_offset_of_listenerName_5(),
	TraceListener_tAA208D26D2197E835400A6EE284410A35F436111::get_offset_of_filter_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2523 = { sizeof (TraceListenerCollection_t392DC090EA67B680F4818E494FB5FE85AA82FBD6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2523[1] = 
{
	TraceListenerCollection_t392DC090EA67B680F4818E494FB5FE85AA82FBD6::get_offset_of_list_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2524 = { sizeof (TraceOptions_tC2D35679E788FD9B8E3AE4FA8FA12EDBCC2EF411)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2524[8] = 
{
	TraceOptions_tC2D35679E788FD9B8E3AE4FA8FA12EDBCC2EF411::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2525 = { sizeof (TraceSwitch_t32D210D5C9B05D9E555925260EEC3767BA895EC8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2526 = { sizeof (AsyncStreamReader_t2C28E845971B756383AF73AEF2A86C7545E5C485), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2526[8] = 
{
	AsyncStreamReader_t2C28E845971B756383AF73AEF2A86C7545E5C485::get_offset_of_stream_0(),
	AsyncStreamReader_t2C28E845971B756383AF73AEF2A86C7545E5C485::get_offset_of_encoding_1(),
	AsyncStreamReader_t2C28E845971B756383AF73AEF2A86C7545E5C485::get_offset_of_decoder_2(),
	AsyncStreamReader_t2C28E845971B756383AF73AEF2A86C7545E5C485::get_offset_of_byteBuffer_3(),
	AsyncStreamReader_t2C28E845971B756383AF73AEF2A86C7545E5C485::get_offset_of_charBuffer_4(),
	AsyncStreamReader_t2C28E845971B756383AF73AEF2A86C7545E5C485::get_offset_of_cancelOperation_5(),
	AsyncStreamReader_t2C28E845971B756383AF73AEF2A86C7545E5C485::get_offset_of_eofEvent_6(),
	AsyncStreamReader_t2C28E845971B756383AF73AEF2A86C7545E5C485::get_offset_of_syncObject_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2527 = { sizeof (Process_t7F28AE318E6CCF89716D05E78E2CBD669767D6A1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2527[32] = 
{
	Process_t7F28AE318E6CCF89716D05E78E2CBD669767D6A1::get_offset_of_haveProcessId_4(),
	Process_t7F28AE318E6CCF89716D05E78E2CBD669767D6A1::get_offset_of_processId_5(),
	Process_t7F28AE318E6CCF89716D05E78E2CBD669767D6A1::get_offset_of_haveProcessHandle_6(),
	Process_t7F28AE318E6CCF89716D05E78E2CBD669767D6A1::get_offset_of_m_processHandle_7(),
	Process_t7F28AE318E6CCF89716D05E78E2CBD669767D6A1::get_offset_of_isRemoteMachine_8(),
	Process_t7F28AE318E6CCF89716D05E78E2CBD669767D6A1::get_offset_of_machineName_9(),
	Process_t7F28AE318E6CCF89716D05E78E2CBD669767D6A1::get_offset_of_m_processAccess_10(),
	Process_t7F28AE318E6CCF89716D05E78E2CBD669767D6A1::get_offset_of_threads_11(),
	Process_t7F28AE318E6CCF89716D05E78E2CBD669767D6A1::get_offset_of_modules_12(),
	Process_t7F28AE318E6CCF89716D05E78E2CBD669767D6A1::get_offset_of_haveWorkingSetLimits_13(),
	Process_t7F28AE318E6CCF89716D05E78E2CBD669767D6A1::get_offset_of_havePriorityClass_14(),
	Process_t7F28AE318E6CCF89716D05E78E2CBD669767D6A1::get_offset_of_watchForExit_15(),
	Process_t7F28AE318E6CCF89716D05E78E2CBD669767D6A1::get_offset_of_watchingForExit_16(),
	Process_t7F28AE318E6CCF89716D05E78E2CBD669767D6A1::get_offset_of_onExited_17(),
	Process_t7F28AE318E6CCF89716D05E78E2CBD669767D6A1::get_offset_of_exited_18(),
	Process_t7F28AE318E6CCF89716D05E78E2CBD669767D6A1::get_offset_of_exitCode_19(),
	Process_t7F28AE318E6CCF89716D05E78E2CBD669767D6A1::get_offset_of_signaled_20(),
	Process_t7F28AE318E6CCF89716D05E78E2CBD669767D6A1::get_offset_of_haveExitTime_21(),
	Process_t7F28AE318E6CCF89716D05E78E2CBD669767D6A1::get_offset_of_raisedOnExited_22(),
	Process_t7F28AE318E6CCF89716D05E78E2CBD669767D6A1::get_offset_of_registeredWaitHandle_23(),
	Process_t7F28AE318E6CCF89716D05E78E2CBD669767D6A1::get_offset_of_waitHandle_24(),
	Process_t7F28AE318E6CCF89716D05E78E2CBD669767D6A1::get_offset_of_synchronizingObject_25(),
	Process_t7F28AE318E6CCF89716D05E78E2CBD669767D6A1::get_offset_of_standardOutput_26(),
	Process_t7F28AE318E6CCF89716D05E78E2CBD669767D6A1::get_offset_of_standardInput_27(),
	Process_t7F28AE318E6CCF89716D05E78E2CBD669767D6A1::get_offset_of_standardError_28(),
	Process_t7F28AE318E6CCF89716D05E78E2CBD669767D6A1::get_offset_of_disposed_29(),
	Process_t7F28AE318E6CCF89716D05E78E2CBD669767D6A1::get_offset_of_outputStreamReadMode_30(),
	Process_t7F28AE318E6CCF89716D05E78E2CBD669767D6A1::get_offset_of_errorStreamReadMode_31(),
	Process_t7F28AE318E6CCF89716D05E78E2CBD669767D6A1::get_offset_of_inputStreamReadMode_32(),
	Process_t7F28AE318E6CCF89716D05E78E2CBD669767D6A1::get_offset_of_output_33(),
	Process_t7F28AE318E6CCF89716D05E78E2CBD669767D6A1::get_offset_of_error_34(),
	Process_t7F28AE318E6CCF89716D05E78E2CBD669767D6A1::get_offset_of_process_name_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2528 = { sizeof (StreamReadMode_t36A3F7301F96A513D4D1C0BFC8A814952F52C3CA)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2528[4] = 
{
	StreamReadMode_t36A3F7301F96A513D4D1C0BFC8A814952F52C3CA::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2529 = { sizeof (State_t8FD6C2E8B4611EF5CB660B3E19DA19E940488826)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2529[9] = 
{
	State_t8FD6C2E8B4611EF5CB660B3E19DA19E940488826::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2530 = { sizeof (ProcessInfo_t2A1AA6533946D6FDFBCA83CDBE0C62089F2EDEF7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2531 = { sizeof (ProcessThreadTimes_tCFD8DB0EA1A7E180E7C1FC125AF94546AD650468), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2531[4] = 
{
	ProcessThreadTimes_tCFD8DB0EA1A7E180E7C1FC125AF94546AD650468::get_offset_of_create_0(),
	ProcessThreadTimes_tCFD8DB0EA1A7E180E7C1FC125AF94546AD650468::get_offset_of_exit_1(),
	ProcessThreadTimes_tCFD8DB0EA1A7E180E7C1FC125AF94546AD650468::get_offset_of_kernel_2(),
	ProcessThreadTimes_tCFD8DB0EA1A7E180E7C1FC125AF94546AD650468::get_offset_of_user_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2532 = { sizeof (ProcessModuleCollection_t93E76B9948E84325744E8C57A525FD465D78DB3F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2533 = { sizeof (ProcessThreadCollection_t6D1D2E676ED1F65087080729F91410724CA74DA7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2534 = { sizeof (ProcessWaitHandle_t3FBAA284E0F9A0751AFBC0A8BF7718DEF2654761), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2535 = { sizeof (DefaultTraceListener_t1B44C909F5A4023CCFB5B885406F9EC2FE5C5B7D), -1, sizeof(DefaultTraceListener_t1B44C909F5A4023CCFB5B885406F9EC2FE5C5B7D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2535[4] = 
{
	DefaultTraceListener_t1B44C909F5A4023CCFB5B885406F9EC2FE5C5B7D_StaticFields::get_offset_of_OnWin32_7(),
	DefaultTraceListener_t1B44C909F5A4023CCFB5B885406F9EC2FE5C5B7D_StaticFields::get_offset_of_MonoTracePrefix_8(),
	DefaultTraceListener_t1B44C909F5A4023CCFB5B885406F9EC2FE5C5B7D_StaticFields::get_offset_of_MonoTraceFile_9(),
	DefaultTraceListener_t1B44C909F5A4023CCFB5B885406F9EC2FE5C5B7D::get_offset_of_logFileName_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2536 = { sizeof (MonitoringDescriptionAttribute_t8DA43522AE8E1B65A028830FCEB729567997069C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2537 = { sizeof (Stopwatch_t0778B5C8DF8FE1D87FC57A2411DA695850BD64D4), -1, sizeof(Stopwatch_t0778B5C8DF8FE1D87FC57A2411DA695850BD64D4_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2537[5] = 
{
	Stopwatch_t0778B5C8DF8FE1D87FC57A2411DA695850BD64D4_StaticFields::get_offset_of_Frequency_0(),
	Stopwatch_t0778B5C8DF8FE1D87FC57A2411DA695850BD64D4_StaticFields::get_offset_of_IsHighResolution_1(),
	Stopwatch_t0778B5C8DF8FE1D87FC57A2411DA695850BD64D4::get_offset_of_elapsed_2(),
	Stopwatch_t0778B5C8DF8FE1D87FC57A2411DA695850BD64D4::get_offset_of_started_3(),
	Stopwatch_t0778B5C8DF8FE1D87FC57A2411DA695850BD64D4::get_offset_of_is_running_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2538 = { sizeof (ExcludeFromCodeCoverageAttribute_tC52BD26DD63AE2AF8B3E1C22E5281B3EB0ACF867), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2539 = { sizeof (AddingNewEventArgs_t848D637EA4E72C12479C42DF55305280B37948D1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2539[1] = 
{
	AddingNewEventArgs_t848D637EA4E72C12479C42DF55305280B37948D1::get_offset_of_newObject_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2540 = { sizeof (AddingNewEventHandler_tC4A113DB05465EF2A66E68EF98945E280F25C1FE), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2541 = { sizeof (AmbientValueAttribute_t9B10538B74BF90773C9F720E1D1E1B10BC63715E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2541[1] = 
{
	AmbientValueAttribute_t9B10538B74BF90773C9F720E1D1E1B10BC63715E::get_offset_of_value_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2542 = { sizeof (ArrayConverter_tAAD8F39711C6ECD39D31226FA1D140DD38752B57), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2543 = { sizeof (ArrayPropertyDescriptor_t16F320E68277A32D9DFEBC21F76C2BCA279F10A2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2543[1] = 
{
	ArrayPropertyDescriptor_t16F320E68277A32D9DFEBC21F76C2BCA279F10A2::get_offset_of_index_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2544 = { sizeof (ArraySubsetEnumerator_tA39E27E02B80287A51F09D2875F1A3AF86768BDA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2544[3] = 
{
	ArraySubsetEnumerator_tA39E27E02B80287A51F09D2875F1A3AF86768BDA::get_offset_of_array_0(),
	ArraySubsetEnumerator_tA39E27E02B80287A51F09D2875F1A3AF86768BDA::get_offset_of_total_1(),
	ArraySubsetEnumerator_tA39E27E02B80287A51F09D2875F1A3AF86768BDA::get_offset_of_current_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2545 = { sizeof (AsyncCompletedEventArgs_tADCBE47368F4D8B198B46D332B27EE38C8B5F8B7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2545[3] = 
{
	AsyncCompletedEventArgs_tADCBE47368F4D8B198B46D332B27EE38C8B5F8B7::get_offset_of_error_1(),
	AsyncCompletedEventArgs_tADCBE47368F4D8B198B46D332B27EE38C8B5F8B7::get_offset_of_cancelled_2(),
	AsyncCompletedEventArgs_tADCBE47368F4D8B198B46D332B27EE38C8B5F8B7::get_offset_of_userState_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2546 = { sizeof (AsyncCompletedEventHandler_tEC686A4F246B63CF3F780209D5DAFA051DF81DB6), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2547 = { sizeof (AsyncOperation_tFA5B90911F8ABE8B07A3058301687DDDF5F78748), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2547[3] = 
{
	AsyncOperation_tFA5B90911F8ABE8B07A3058301687DDDF5F78748::get_offset_of_syncContext_0(),
	AsyncOperation_tFA5B90911F8ABE8B07A3058301687DDDF5F78748::get_offset_of_userSuppliedState_1(),
	AsyncOperation_tFA5B90911F8ABE8B07A3058301687DDDF5F78748::get_offset_of_alreadyCompleted_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2548 = { sizeof (AsyncOperationManager_t000ED3690BB5C16399184208344B865AF5E7CE7A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2549 = { sizeof (AttributeCollection_tBE6941BB802EDE34B7F986C14A7D7E3A4E135EBE), -1, sizeof(AttributeCollection_tBE6941BB802EDE34B7F986C14A7D7E3A4E135EBE_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2549[7] = 
{
	AttributeCollection_tBE6941BB802EDE34B7F986C14A7D7E3A4E135EBE_StaticFields::get_offset_of_Empty_0(),
	AttributeCollection_tBE6941BB802EDE34B7F986C14A7D7E3A4E135EBE_StaticFields::get_offset_of__defaultAttributes_1(),
	AttributeCollection_tBE6941BB802EDE34B7F986C14A7D7E3A4E135EBE::get_offset_of__attributes_2(),
	AttributeCollection_tBE6941BB802EDE34B7F986C14A7D7E3A4E135EBE_StaticFields::get_offset_of_internalSyncObject_3(),
	0,
	AttributeCollection_tBE6941BB802EDE34B7F986C14A7D7E3A4E135EBE::get_offset_of__foundAttributeTypes_5(),
	AttributeCollection_tBE6941BB802EDE34B7F986C14A7D7E3A4E135EBE::get_offset_of__index_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2550 = { sizeof (AttributeEntry_t03E9BFE6BF6BE56EA2568359ACD774FE8C8661DD)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2550[2] = 
{
	AttributeEntry_t03E9BFE6BF6BE56EA2568359ACD774FE8C8661DD::get_offset_of_type_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AttributeEntry_t03E9BFE6BF6BE56EA2568359ACD774FE8C8661DD::get_offset_of_index_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2551 = { sizeof (AttributeProviderAttribute_t8EC0C13032FE51365470EF1C6FDE9E8B1C33B068), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2551[2] = 
{
	AttributeProviderAttribute_t8EC0C13032FE51365470EF1C6FDE9E8B1C33B068::get_offset_of__typeName_0(),
	AttributeProviderAttribute_t8EC0C13032FE51365470EF1C6FDE9E8B1C33B068::get_offset_of__propertyName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2552 = { sizeof (BackgroundWorker_t2E2BF26D42C991BCA6B8ECAF8830D0CA2D4DA13A), -1, sizeof(BackgroundWorker_t2E2BF26D42C991BCA6B8ECAF8830D0CA2D4DA13A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2552[11] = 
{
	BackgroundWorker_t2E2BF26D42C991BCA6B8ECAF8830D0CA2D4DA13A_StaticFields::get_offset_of_doWorkKey_4(),
	BackgroundWorker_t2E2BF26D42C991BCA6B8ECAF8830D0CA2D4DA13A_StaticFields::get_offset_of_runWorkerCompletedKey_5(),
	BackgroundWorker_t2E2BF26D42C991BCA6B8ECAF8830D0CA2D4DA13A_StaticFields::get_offset_of_progressChangedKey_6(),
	BackgroundWorker_t2E2BF26D42C991BCA6B8ECAF8830D0CA2D4DA13A::get_offset_of_canCancelWorker_7(),
	BackgroundWorker_t2E2BF26D42C991BCA6B8ECAF8830D0CA2D4DA13A::get_offset_of_workerReportsProgress_8(),
	BackgroundWorker_t2E2BF26D42C991BCA6B8ECAF8830D0CA2D4DA13A::get_offset_of_cancellationPending_9(),
	BackgroundWorker_t2E2BF26D42C991BCA6B8ECAF8830D0CA2D4DA13A::get_offset_of_isRunning_10(),
	BackgroundWorker_t2E2BF26D42C991BCA6B8ECAF8830D0CA2D4DA13A::get_offset_of_asyncOperation_11(),
	BackgroundWorker_t2E2BF26D42C991BCA6B8ECAF8830D0CA2D4DA13A::get_offset_of_threadStart_12(),
	BackgroundWorker_t2E2BF26D42C991BCA6B8ECAF8830D0CA2D4DA13A::get_offset_of_operationCompleted_13(),
	BackgroundWorker_t2E2BF26D42C991BCA6B8ECAF8830D0CA2D4DA13A::get_offset_of_progressReporter_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2553 = { sizeof (WorkerThreadStartDelegate_tEC42174597C1FB97F214AE61808E6F0CE75BF7B6), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2554 = { sizeof (ComponentEditor_t10B6B1D4888C4D6AA6D11C071EE7536EEA69FD2B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2555 = { sizeof (BindableAttribute_tFDECD8DB181D7647712EFB507BA39E1B47B6108C), -1, sizeof(BindableAttribute_tFDECD8DB181D7647712EFB507BA39E1B47B6108C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2555[6] = 
{
	BindableAttribute_tFDECD8DB181D7647712EFB507BA39E1B47B6108C_StaticFields::get_offset_of_Yes_0(),
	BindableAttribute_tFDECD8DB181D7647712EFB507BA39E1B47B6108C_StaticFields::get_offset_of_No_1(),
	BindableAttribute_tFDECD8DB181D7647712EFB507BA39E1B47B6108C_StaticFields::get_offset_of_Default_2(),
	BindableAttribute_tFDECD8DB181D7647712EFB507BA39E1B47B6108C::get_offset_of_bindable_3(),
	BindableAttribute_tFDECD8DB181D7647712EFB507BA39E1B47B6108C::get_offset_of_isDefault_4(),
	BindableAttribute_tFDECD8DB181D7647712EFB507BA39E1B47B6108C::get_offset_of_direction_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2556 = { sizeof (BindableSupport_t08F85344F8C2B7F5F78146A953644FF6972D4E29)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2556[4] = 
{
	BindableSupport_t08F85344F8C2B7F5F78146A953644FF6972D4E29::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2557 = { sizeof (BindingDirection_t01A86D4887E469A94D1B5B7B55D82DD88A6725FF)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2557[3] = 
{
	BindingDirection_t01A86D4887E469A94D1B5B7B55D82DD88A6725FF::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2558 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2558[12] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2559 = { sizeof (BooleanConverter_tD0D177A9F577915FAA9F6749229672CE8EBAA94C), -1, sizeof(BooleanConverter_tD0D177A9F577915FAA9F6749229672CE8EBAA94C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2559[1] = 
{
	BooleanConverter_tD0D177A9F577915FAA9F6749229672CE8EBAA94C_StaticFields::get_offset_of_values_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2560 = { sizeof (BrowsableAttribute_t8A1A514FEE5735ADED64CCFE6E06A42E5CD872D1), -1, sizeof(BrowsableAttribute_t8A1A514FEE5735ADED64CCFE6E06A42E5CD872D1_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2560[4] = 
{
	BrowsableAttribute_t8A1A514FEE5735ADED64CCFE6E06A42E5CD872D1_StaticFields::get_offset_of_Yes_0(),
	BrowsableAttribute_t8A1A514FEE5735ADED64CCFE6E06A42E5CD872D1_StaticFields::get_offset_of_No_1(),
	BrowsableAttribute_t8A1A514FEE5735ADED64CCFE6E06A42E5CD872D1_StaticFields::get_offset_of_Default_2(),
	BrowsableAttribute_t8A1A514FEE5735ADED64CCFE6E06A42E5CD872D1::get_offset_of_browsable_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2561 = { sizeof (ByteConverter_t59E5742D740228F5B9AB91169C2EA907A023328A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2562 = { sizeof (CancelEventArgs_t2843141F2893A01C11535CD7E38072CA26D7794D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2562[1] = 
{
	CancelEventArgs_t2843141F2893A01C11535CD7E38072CA26D7794D::get_offset_of_cancel_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2563 = { sizeof (CancelEventHandler_t0F5842A94E02DFA026B793A034C1AA362D6D0A2B), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2564 = { sizeof (CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80), -1, sizeof(CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2564[16] = 
{
	CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80_StaticFields::get_offset_of_appearance_0(),
	CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80_StaticFields::get_offset_of_asynchronous_1(),
	CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80_StaticFields::get_offset_of_behavior_2(),
	CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80_StaticFields::get_offset_of_data_3(),
	CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80_StaticFields::get_offset_of_design_4(),
	CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80_StaticFields::get_offset_of_action_5(),
	CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80_StaticFields::get_offset_of_format_6(),
	CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80_StaticFields::get_offset_of_layout_7(),
	CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80_StaticFields::get_offset_of_mouse_8(),
	CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80_StaticFields::get_offset_of_key_9(),
	CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80_StaticFields::get_offset_of_focus_10(),
	CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80_StaticFields::get_offset_of_windowStyle_11(),
	CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80_StaticFields::get_offset_of_dragDrop_12(),
	CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80_StaticFields::get_offset_of_defAttr_13(),
	CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80::get_offset_of_localized_14(),
	CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80::get_offset_of_categoryValue_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2565 = { sizeof (CharConverter_tFD013540F3AFDBF6DB36FEE066025D778FAED95A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2566 = { sizeof (CollectionChangeAction_tEF925321EBE6F00D384F1E5D046E601F7190043D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2566[4] = 
{
	CollectionChangeAction_tEF925321EBE6F00D384F1E5D046E601F7190043D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2567 = { sizeof (CollectionChangeEventArgs_t63CA165C1F7D765B04CB139EB6577577479E57B0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2567[2] = 
{
	CollectionChangeEventArgs_t63CA165C1F7D765B04CB139EB6577577479E57B0::get_offset_of_action_1(),
	CollectionChangeEventArgs_t63CA165C1F7D765B04CB139EB6577577479E57B0::get_offset_of_element_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2568 = { sizeof (CollectionChangeEventHandler_t8AE127943B452074A0FE2755F20B399D733EBCB2), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2569 = { sizeof (CollectionConverter_t039E15C433996B0F0F0EB78BEE81F6DE0705F184), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2570 = { sizeof (CompModSwitches_tB835655C9DCFB714C970F60C9B98519276BC499D), -1, sizeof(CompModSwitches_tB835655C9DCFB714C970F60C9B98519276BC499D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2570[2] = 
{
	CompModSwitches_tB835655C9DCFB714C970F60C9B98519276BC499D_StaticFields::get_offset_of_commonDesignerServices_0(),
	CompModSwitches_tB835655C9DCFB714C970F60C9B98519276BC499D_StaticFields::get_offset_of_eventLog_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2571 = { sizeof (ComplexBindingPropertiesAttribute_tA6DB48D4B4B6A8880F1F37C7F5860CB9789A0CEC), -1, sizeof(ComplexBindingPropertiesAttribute_tA6DB48D4B4B6A8880F1F37C7F5860CB9789A0CEC_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2571[3] = 
{
	ComplexBindingPropertiesAttribute_tA6DB48D4B4B6A8880F1F37C7F5860CB9789A0CEC::get_offset_of_dataSource_0(),
	ComplexBindingPropertiesAttribute_tA6DB48D4B4B6A8880F1F37C7F5860CB9789A0CEC::get_offset_of_dataMember_1(),
	ComplexBindingPropertiesAttribute_tA6DB48D4B4B6A8880F1F37C7F5860CB9789A0CEC_StaticFields::get_offset_of_Default_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2572 = { sizeof (Component_t7AEFE153F6778CF52E1981BC3E811A9604B29473), -1, sizeof(Component_t7AEFE153F6778CF52E1981BC3E811A9604B29473_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2572[3] = 
{
	Component_t7AEFE153F6778CF52E1981BC3E811A9604B29473_StaticFields::get_offset_of_EventDisposed_1(),
	Component_t7AEFE153F6778CF52E1981BC3E811A9604B29473::get_offset_of_site_2(),
	Component_t7AEFE153F6778CF52E1981BC3E811A9604B29473::get_offset_of_events_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2573 = { sizeof (ComponentCollection_tCA923631B2E55E6A831F9D5CF5231113764CB895), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2574 = { sizeof (ComponentConverter_tAFCE49784F59197CB5E92C8ED566B069D1A5766E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2575 = { sizeof (ComponentResourceManager_t48833C1DFC68FB7031F4E102B50BE346D8811065), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2575[2] = 
{
	ComponentResourceManager_t48833C1DFC68FB7031F4E102B50BE346D8811065::get_offset_of__resourceSets_25(),
	ComponentResourceManager_t48833C1DFC68FB7031F4E102B50BE346D8811065::get_offset_of__neutralResourcesCulture_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2576 = { sizeof (Container_tC98CD69632DEA16FC061454A770CC7D11FFFEEF1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2576[6] = 
{
	Container_tC98CD69632DEA16FC061454A770CC7D11FFFEEF1::get_offset_of_sites_0(),
	Container_tC98CD69632DEA16FC061454A770CC7D11FFFEEF1::get_offset_of_siteCount_1(),
	Container_tC98CD69632DEA16FC061454A770CC7D11FFFEEF1::get_offset_of_components_2(),
	Container_tC98CD69632DEA16FC061454A770CC7D11FFFEEF1::get_offset_of_filter_3(),
	Container_tC98CD69632DEA16FC061454A770CC7D11FFFEEF1::get_offset_of_checkedFilter_4(),
	Container_tC98CD69632DEA16FC061454A770CC7D11FFFEEF1::get_offset_of_syncObj_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2577 = { sizeof (Site_tD5DE03CC5F94838106C1E4E65AA851553A96335B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2577[3] = 
{
	Site_tD5DE03CC5F94838106C1E4E65AA851553A96335B::get_offset_of_component_0(),
	Site_tD5DE03CC5F94838106C1E4E65AA851553A96335B::get_offset_of_container_1(),
	Site_tD5DE03CC5F94838106C1E4E65AA851553A96335B::get_offset_of_name_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2578 = { sizeof (ContainerFilterService_t4E2FD208B28F83EA50EBC66500B7DCA29D38FA19), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2579 = { sizeof (CultureInfoConverter_t6BD4316CF7C9163A01C35EC9382BC3D1C01FB6C0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2579[1] = 
{
	CultureInfoConverter_t6BD4316CF7C9163A01C35EC9382BC3D1C01FB6C0::get_offset_of_values_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2580 = { sizeof (CultureComparer_t19BC07A3DF37B4602E0AA7FA4B92795FE47A409E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2580[1] = 
{
	CultureComparer_t19BC07A3DF37B4602E0AA7FA4B92795FE47A409E::get_offset_of_converter_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2581 = { sizeof (CultureInfoMapper_tB7486496AE7B37C5908168F2272AED9DFB0C1A94), -1, sizeof(CultureInfoMapper_tB7486496AE7B37C5908168F2272AED9DFB0C1A94_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2581[1] = 
{
	CultureInfoMapper_tB7486496AE7B37C5908168F2272AED9DFB0C1A94_StaticFields::get_offset_of_cultureInfoNameMap_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2582 = { sizeof (CustomTypeDescriptor_tF8665CD45DFFA622F7EB328A2F77067DD2147689), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2582[1] = 
{
	CustomTypeDescriptor_tF8665CD45DFFA622F7EB328A2F77067DD2147689::get_offset_of__parent_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2583 = { sizeof (DataErrorsChangedEventArgs_t83896D84C96C5BC0478ACF7E99C7703D464BE11F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2583[1] = 
{
	DataErrorsChangedEventArgs_t83896D84C96C5BC0478ACF7E99C7703D464BE11F::get_offset_of_propertyName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2584 = { sizeof (DataObjectAttribute_t831D53B857103EE776C8F8EBB60D27BFB96CC441), -1, sizeof(DataObjectAttribute_t831D53B857103EE776C8F8EBB60D27BFB96CC441_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2584[4] = 
{
	DataObjectAttribute_t831D53B857103EE776C8F8EBB60D27BFB96CC441_StaticFields::get_offset_of_DataObject_0(),
	DataObjectAttribute_t831D53B857103EE776C8F8EBB60D27BFB96CC441_StaticFields::get_offset_of_NonDataObject_1(),
	DataObjectAttribute_t831D53B857103EE776C8F8EBB60D27BFB96CC441_StaticFields::get_offset_of_Default_2(),
	DataObjectAttribute_t831D53B857103EE776C8F8EBB60D27BFB96CC441::get_offset_of__isDataObject_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2585 = { sizeof (DataObjectFieldAttribute_t62CD1DDC49C79543C2A0AD51F56ED922420F83DB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2585[4] = 
{
	DataObjectFieldAttribute_t62CD1DDC49C79543C2A0AD51F56ED922420F83DB::get_offset_of__primaryKey_0(),
	DataObjectFieldAttribute_t62CD1DDC49C79543C2A0AD51F56ED922420F83DB::get_offset_of__isIdentity_1(),
	DataObjectFieldAttribute_t62CD1DDC49C79543C2A0AD51F56ED922420F83DB::get_offset_of__isNullable_2(),
	DataObjectFieldAttribute_t62CD1DDC49C79543C2A0AD51F56ED922420F83DB::get_offset_of__length_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2586 = { sizeof (DataObjectMethodAttribute_t13EFB10B080110C75356535458A36736DF2AC249), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2586[2] = 
{
	DataObjectMethodAttribute_t13EFB10B080110C75356535458A36736DF2AC249::get_offset_of__isDefault_0(),
	DataObjectMethodAttribute_t13EFB10B080110C75356535458A36736DF2AC249::get_offset_of__methodType_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2587 = { sizeof (DataObjectMethodType_t0B00930F4E899CF8E4709594FF8D0F171CF4E0D0)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2587[6] = 
{
	DataObjectMethodType_t0B00930F4E899CF8E4709594FF8D0F171CF4E0D0::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2588 = { sizeof (DateTimeConverter_tE35DE01AAE1A29D50B4B0DC6467C9219CCE04DE1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2589 = { sizeof (DateTimeOffsetConverter_tD9E7BEFD22CBB5DA01F50C0D51CDD60DF29F1D33), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2590 = { sizeof (DecimalConverter_t10232B897580B6DE599BB375BE8C0F4E1C30B0C1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2591 = { sizeof (DefaultBindingPropertyAttribute_t12924F2E55ACA393FEE4AC9CB280FA7325D9F211), -1, sizeof(DefaultBindingPropertyAttribute_t12924F2E55ACA393FEE4AC9CB280FA7325D9F211_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2591[2] = 
{
	DefaultBindingPropertyAttribute_t12924F2E55ACA393FEE4AC9CB280FA7325D9F211::get_offset_of_name_0(),
	DefaultBindingPropertyAttribute_t12924F2E55ACA393FEE4AC9CB280FA7325D9F211_StaticFields::get_offset_of_Default_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2592 = { sizeof (DefaultEventAttribute_tD1A10417C052CE865C43023F6DCC33CF54D3D846), -1, sizeof(DefaultEventAttribute_tD1A10417C052CE865C43023F6DCC33CF54D3D846_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2592[2] = 
{
	DefaultEventAttribute_tD1A10417C052CE865C43023F6DCC33CF54D3D846::get_offset_of_name_0(),
	DefaultEventAttribute_tD1A10417C052CE865C43023F6DCC33CF54D3D846_StaticFields::get_offset_of_Default_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2593 = { sizeof (DefaultPropertyAttribute_t4C049F2905F3ABDE9B9592627B6133AE49050AA7), -1, sizeof(DefaultPropertyAttribute_t4C049F2905F3ABDE9B9592627B6133AE49050AA7_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2593[2] = 
{
	DefaultPropertyAttribute_t4C049F2905F3ABDE9B9592627B6133AE49050AA7::get_offset_of_name_0(),
	DefaultPropertyAttribute_t4C049F2905F3ABDE9B9592627B6133AE49050AA7_StaticFields::get_offset_of_Default_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2594 = { sizeof (DefaultValueAttribute_t03B1F51B35271D50779D31234C9C6845BC9626EC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2594[1] = 
{
	DefaultValueAttribute_t03B1F51B35271D50779D31234C9C6845BC9626EC::get_offset_of_value_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2595 = { sizeof (DelegatingTypeDescriptionProvider_t1479D886AEC7B0D4A74E19212E904337D5505A56), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2595[1] = 
{
	DelegatingTypeDescriptionProvider_t1479D886AEC7B0D4A74E19212E904337D5505A56::get_offset_of__type_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2596 = { sizeof (DescriptionAttribute_t112C5FEAA03342D05BF40C1713ABF1C1848DEE75), -1, sizeof(DescriptionAttribute_t112C5FEAA03342D05BF40C1713ABF1C1848DEE75_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2596[2] = 
{
	DescriptionAttribute_t112C5FEAA03342D05BF40C1713ABF1C1848DEE75_StaticFields::get_offset_of_Default_0(),
	DescriptionAttribute_t112C5FEAA03342D05BF40C1713ABF1C1848DEE75::get_offset_of_description_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2597 = { sizeof (DesignOnlyAttribute_tF2F25F5BECE1C4E68B1408838F04C5E096169D06), -1, sizeof(DesignOnlyAttribute_tF2F25F5BECE1C4E68B1408838F04C5E096169D06_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2597[4] = 
{
	DesignOnlyAttribute_tF2F25F5BECE1C4E68B1408838F04C5E096169D06::get_offset_of_isDesignOnly_0(),
	DesignOnlyAttribute_tF2F25F5BECE1C4E68B1408838F04C5E096169D06_StaticFields::get_offset_of_Yes_1(),
	DesignOnlyAttribute_tF2F25F5BECE1C4E68B1408838F04C5E096169D06_StaticFields::get_offset_of_No_2(),
	DesignOnlyAttribute_tF2F25F5BECE1C4E68B1408838F04C5E096169D06_StaticFields::get_offset_of_Default_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2598 = { sizeof (DesignTimeVisibleAttribute_t4DD8962EAB8CFD8617D29303B7CFAD11E94B7466), -1, sizeof(DesignTimeVisibleAttribute_t4DD8962EAB8CFD8617D29303B7CFAD11E94B7466_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2598[4] = 
{
	DesignTimeVisibleAttribute_t4DD8962EAB8CFD8617D29303B7CFAD11E94B7466::get_offset_of_visible_0(),
	DesignTimeVisibleAttribute_t4DD8962EAB8CFD8617D29303B7CFAD11E94B7466_StaticFields::get_offset_of_Yes_1(),
	DesignTimeVisibleAttribute_t4DD8962EAB8CFD8617D29303B7CFAD11E94B7466_StaticFields::get_offset_of_No_2(),
	DesignTimeVisibleAttribute_t4DD8962EAB8CFD8617D29303B7CFAD11E94B7466_StaticFields::get_offset_of_Default_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2599 = { sizeof (DesignerAttribute_t55268910CFC6D82065C1A2F68D05DCD3858933D0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2599[3] = 
{
	DesignerAttribute_t55268910CFC6D82065C1A2F68D05DCD3858933D0::get_offset_of_designerTypeName_0(),
	DesignerAttribute_t55268910CFC6D82065C1A2F68D05DCD3858933D0::get_offset_of_designerBaseTypeName_1(),
	DesignerAttribute_t55268910CFC6D82065C1A2F68D05DCD3858933D0::get_offset_of_typeId_2(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
