﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Action`1<Zenject.DiContainer>
struct Action_1_t47FB75EB7E275E7EC20C1ADC32AC936BD0BD4E84;
// System.Action`2<System.Object,System.Object>
struct Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.IEnumerable`1<Zenject.DiContainer>
struct IEnumerable_1_tF49BC792153BEBAC6DB7A25673D94710BE5F0056;
// System.Collections.Generic.IEnumerator`1<Zenject.InjectContext>
struct IEnumerator_1_t50D78FA51F30D303518C134A5EA0816FB402A4CD;
// System.Collections.Generic.List`1<System.String>
struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3;
// System.Collections.Generic.List`1<System.Type>
struct List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0;
// System.Collections.Generic.List`1<Zenject.InstallerBase>
struct List_1_t0579372F26330F2A4603BEBDDF54A2BF6791CF8B;
// System.Collections.Generic.List`1<Zenject.MonoInstaller>
struct List_1_tCF773BC8FB84889055B7C7EDDE5C5D83FA2D4F08;
// System.Collections.Generic.List`1<Zenject.SceneDecoratorContext>
struct List_1_t3341302071D2A75F070E9984D7E1F72328EFE65B;
// System.Collections.Generic.List`1<Zenject.ScriptableObjectInstaller>
struct List_1_t8B49C95109DF1CE321982E768E438471621D0733;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.Func`2<System.Object,Zenject.TypeValuePair>
struct Func_2_t8E2E0C08A0978F2FB285DAC528D584AA264A57F8;
// System.Func`2<System.Type,System.Boolean>
struct Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9;
// System.Func`2<System.Type,System.String>
struct Func_2_t42E15778FCD2FCC12128FA2916CE06E4E70655E5;
// System.Func`2<System.Type,System.Type>
struct Func_2_t9A30598E63DAE9805F56679563D875589D41D2CF;
// System.Func`3<Zenject.DiContainer,System.Type,Zenject.IProvider>
struct Func_3_tF2BBB0DC92895D84974BAECD6D1B2AE7FFC65AAA;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0;
// Zenject.BindInfo
struct BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991;
// Zenject.DiContainer
struct DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5;
// Zenject.GameObjectCreationParameters
struct GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A;
// Zenject.ISubContainerCreator
struct ISubContainerCreator_t51D3BD7DE31FCD1DC85EEDEF44461B7C6FCBB45A;
// Zenject.InjectContext
struct InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF;
// Zenject.MonoKernel
struct MonoKernel_t297C7FA263D67830E721CBF0F9C909FAFB569412;
// Zenject.ScopableBindingFinalizer
struct ScopableBindingFinalizer_t1278F9BE4EE396C7D8B5CA5625C540EC3582632C;
// Zenject.SubContainerCreatorCached
struct SubContainerCreatorCached_t8C3674F84167B2FA6DC79C8B318D605610F10EA4;
// Zenject.SubContainerInstallerBindingFinalizer
struct SubContainerInstallerBindingFinalizer_t7E7503CA806862CA81C5A2A86E199B15532DC82E;
// Zenject.SubContainerInstallerBindingFinalizer/<>c__DisplayClass5_0
struct U3CU3Ec__DisplayClass5_0_t64DDCD52B9790DA5BB3DF411F4F3726FD18DA5B1;
// Zenject.SubContainerInstallerBindingFinalizer/<>c__DisplayClass6_0
struct U3CU3Ec__DisplayClass6_0_t2FC5487E39FC7D6C3B76178185AD1118043CB391;
// Zenject.SubContainerMethodBindingFinalizer
struct SubContainerMethodBindingFinalizer_tCC3590F54EDCD60ECB9CC8F1D324DB07191CEB31;
// Zenject.SubContainerMethodBindingFinalizer/<>c__DisplayClass4_0
struct U3CU3Ec__DisplayClass4_0_t3528FF845AC546F07044CFEFE66CFDCBB5899B69;
// Zenject.SubContainerMethodBindingFinalizer/<>c__DisplayClass5_0
struct U3CU3Ec__DisplayClass5_0_t7988CB076649CF11F3F91091ECAEEA8E2BD0126A;
// Zenject.SubContainerPrefabBindingFinalizer
struct SubContainerPrefabBindingFinalizer_t909CFA4349A1C6B422478412C874D33CFE229618;
// Zenject.SubContainerPrefabBindingFinalizer/<>c__DisplayClass5_0
struct U3CU3Ec__DisplayClass5_0_t9F9EF9AD88205286601A08217B9F885C6CB7FFAE;
// Zenject.SubContainerPrefabBindingFinalizer/<>c__DisplayClass6_0
struct U3CU3Ec__DisplayClass6_0_t303E2EE042B2111EC378D3D496EA2074D6A83847;
// Zenject.SubContainerPrefabResourceBindingFinalizer
struct SubContainerPrefabResourceBindingFinalizer_t25917592BD3C28B58C7F81D40A8F7D2FABD0E1CC;
// Zenject.SubContainerPrefabResourceBindingFinalizer/<>c__DisplayClass5_0
struct U3CU3Ec__DisplayClass5_0_tCC18CA0B7418964E47FFC397F2B05D1C5D411977;
// Zenject.SubContainerPrefabResourceBindingFinalizer/<>c__DisplayClass6_0
struct U3CU3Ec__DisplayClass6_0_t29364056A3133169CB10C6EC6C948A84D0F3C4B2;

struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef U3CU3EC_T373679F74E78B676C1348147F36AFB0F2878189B_H
#define U3CU3EC_T373679F74E78B676C1348147F36AFB0F2878189B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.Context_<>c
struct  U3CU3Ec_t373679F74E78B676C1348147F36AFB0F2878189B  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t373679F74E78B676C1348147F36AFB0F2878189B_StaticFields
{
public:
	// Zenject.Context_<>c Zenject.Context_<>c::<>9
	U3CU3Ec_t373679F74E78B676C1348147F36AFB0F2878189B * ___U3CU3E9_0;
	// System.Func`2<System.Type,System.Boolean> Zenject.Context_<>c::<>9__16_0
	Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 * ___U3CU3E9__16_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t373679F74E78B676C1348147F36AFB0F2878189B_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t373679F74E78B676C1348147F36AFB0F2878189B * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t373679F74E78B676C1348147F36AFB0F2878189B ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t373679F74E78B676C1348147F36AFB0F2878189B * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__16_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t373679F74E78B676C1348147F36AFB0F2878189B_StaticFields, ___U3CU3E9__16_0_1)); }
	inline Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 * get_U3CU3E9__16_0_1() const { return ___U3CU3E9__16_0_1; }
	inline Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 ** get_address_of_U3CU3E9__16_0_1() { return &___U3CU3E9__16_0_1; }
	inline void set_U3CU3E9__16_0_1(Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 * value)
	{
		___U3CU3E9__16_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__16_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T373679F74E78B676C1348147F36AFB0F2878189B_H
#ifndef GAMEOBJECTFACTORY_T9D51611DDE961B006FF2D9E5C5F857EB47C2E596_H
#define GAMEOBJECTFACTORY_T9D51611DDE961B006FF2D9E5C5F857EB47C2E596_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.GameObjectFactory
struct  GameObjectFactory_t9D51611DDE961B006FF2D9E5C5F857EB47C2E596  : public RuntimeObject
{
public:
	// Zenject.DiContainer Zenject.GameObjectFactory::_container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____container_0;
	// UnityEngine.Object Zenject.GameObjectFactory::_prefab
	Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ____prefab_1;

public:
	inline static int32_t get_offset_of__container_0() { return static_cast<int32_t>(offsetof(GameObjectFactory_t9D51611DDE961B006FF2D9E5C5F857EB47C2E596, ____container_0)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__container_0() const { return ____container_0; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__container_0() { return &____container_0; }
	inline void set__container_0(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____container_0 = value;
		Il2CppCodeGenWriteBarrier((&____container_0), value);
	}

	inline static int32_t get_offset_of__prefab_1() { return static_cast<int32_t>(offsetof(GameObjectFactory_t9D51611DDE961B006FF2D9E5C5F857EB47C2E596, ____prefab_1)); }
	inline Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * get__prefab_1() const { return ____prefab_1; }
	inline Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 ** get_address_of__prefab_1() { return &____prefab_1; }
	inline void set__prefab_1(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * value)
	{
		____prefab_1 = value;
		Il2CppCodeGenWriteBarrier((&____prefab_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECTFACTORY_T9D51611DDE961B006FF2D9E5C5F857EB47C2E596_H
#ifndef U3CGET_ALLOBJECTTYPESU3ED__53_T3F12867B877F1245F012C89AD5B62519B3C1E5D7_H
#define U3CGET_ALLOBJECTTYPESU3ED__53_T3F12867B877F1245F012C89AD5B62519B3C1E5D7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.InjectContext_<get_AllObjectTypes>d__53
struct  U3Cget_AllObjectTypesU3Ed__53_t3F12867B877F1245F012C89AD5B62519B3C1E5D7  : public RuntimeObject
{
public:
	// System.Int32 Zenject.InjectContext_<get_AllObjectTypes>d__53::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Type Zenject.InjectContext_<get_AllObjectTypes>d__53::<>2__current
	Type_t * ___U3CU3E2__current_1;
	// System.Int32 Zenject.InjectContext_<get_AllObjectTypes>d__53::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// Zenject.InjectContext Zenject.InjectContext_<get_AllObjectTypes>d__53::<>4__this
	InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * ___U3CU3E4__this_3;
	// System.Collections.Generic.IEnumerator`1<Zenject.InjectContext> Zenject.InjectContext_<get_AllObjectTypes>d__53::<>7__wrap1
	RuntimeObject* ___U3CU3E7__wrap1_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3Cget_AllObjectTypesU3Ed__53_t3F12867B877F1245F012C89AD5B62519B3C1E5D7, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3Cget_AllObjectTypesU3Ed__53_t3F12867B877F1245F012C89AD5B62519B3C1E5D7, ___U3CU3E2__current_1)); }
	inline Type_t * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline Type_t ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(Type_t * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3Cget_AllObjectTypesU3Ed__53_t3F12867B877F1245F012C89AD5B62519B3C1E5D7, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3Cget_AllObjectTypesU3Ed__53_t3F12867B877F1245F012C89AD5B62519B3C1E5D7, ___U3CU3E4__this_3)); }
	inline InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_4() { return static_cast<int32_t>(offsetof(U3Cget_AllObjectTypesU3Ed__53_t3F12867B877F1245F012C89AD5B62519B3C1E5D7, ___U3CU3E7__wrap1_4)); }
	inline RuntimeObject* get_U3CU3E7__wrap1_4() const { return ___U3CU3E7__wrap1_4; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap1_4() { return &___U3CU3E7__wrap1_4; }
	inline void set_U3CU3E7__wrap1_4(RuntimeObject* value)
	{
		___U3CU3E7__wrap1_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap1_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGET_ALLOBJECTTYPESU3ED__53_T3F12867B877F1245F012C89AD5B62519B3C1E5D7_H
#ifndef U3CGET_PARENTCONTEXTSU3ED__49_T6FE44C4158F6B0806A0D7069F1415DBD0CFC2C4C_H
#define U3CGET_PARENTCONTEXTSU3ED__49_T6FE44C4158F6B0806A0D7069F1415DBD0CFC2C4C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.InjectContext_<get_ParentContexts>d__49
struct  U3Cget_ParentContextsU3Ed__49_t6FE44C4158F6B0806A0D7069F1415DBD0CFC2C4C  : public RuntimeObject
{
public:
	// System.Int32 Zenject.InjectContext_<get_ParentContexts>d__49::<>1__state
	int32_t ___U3CU3E1__state_0;
	// Zenject.InjectContext Zenject.InjectContext_<get_ParentContexts>d__49::<>2__current
	InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * ___U3CU3E2__current_1;
	// System.Int32 Zenject.InjectContext_<get_ParentContexts>d__49::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// Zenject.InjectContext Zenject.InjectContext_<get_ParentContexts>d__49::<>4__this
	InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * ___U3CU3E4__this_3;
	// System.Collections.Generic.IEnumerator`1<Zenject.InjectContext> Zenject.InjectContext_<get_ParentContexts>d__49::<>7__wrap1
	RuntimeObject* ___U3CU3E7__wrap1_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3Cget_ParentContextsU3Ed__49_t6FE44C4158F6B0806A0D7069F1415DBD0CFC2C4C, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3Cget_ParentContextsU3Ed__49_t6FE44C4158F6B0806A0D7069F1415DBD0CFC2C4C, ___U3CU3E2__current_1)); }
	inline InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3Cget_ParentContextsU3Ed__49_t6FE44C4158F6B0806A0D7069F1415DBD0CFC2C4C, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3Cget_ParentContextsU3Ed__49_t6FE44C4158F6B0806A0D7069F1415DBD0CFC2C4C, ___U3CU3E4__this_3)); }
	inline InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_4() { return static_cast<int32_t>(offsetof(U3Cget_ParentContextsU3Ed__49_t6FE44C4158F6B0806A0D7069F1415DBD0CFC2C4C, ___U3CU3E7__wrap1_4)); }
	inline RuntimeObject* get_U3CU3E7__wrap1_4() const { return ___U3CU3E7__wrap1_4; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap1_4() { return &___U3CU3E7__wrap1_4; }
	inline void set_U3CU3E7__wrap1_4(RuntimeObject* value)
	{
		___U3CU3E7__wrap1_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap1_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGET_PARENTCONTEXTSU3ED__49_T6FE44C4158F6B0806A0D7069F1415DBD0CFC2C4C_H
#ifndef U3CGET_PARENTCONTEXTSANDSELFU3ED__51_T7762F27E1E3E53C1E19B0EB2A69F072222AAF3F3_H
#define U3CGET_PARENTCONTEXTSANDSELFU3ED__51_T7762F27E1E3E53C1E19B0EB2A69F072222AAF3F3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.InjectContext_<get_ParentContextsAndSelf>d__51
struct  U3Cget_ParentContextsAndSelfU3Ed__51_t7762F27E1E3E53C1E19B0EB2A69F072222AAF3F3  : public RuntimeObject
{
public:
	// System.Int32 Zenject.InjectContext_<get_ParentContextsAndSelf>d__51::<>1__state
	int32_t ___U3CU3E1__state_0;
	// Zenject.InjectContext Zenject.InjectContext_<get_ParentContextsAndSelf>d__51::<>2__current
	InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * ___U3CU3E2__current_1;
	// System.Int32 Zenject.InjectContext_<get_ParentContextsAndSelf>d__51::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// Zenject.InjectContext Zenject.InjectContext_<get_ParentContextsAndSelf>d__51::<>4__this
	InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * ___U3CU3E4__this_3;
	// System.Collections.Generic.IEnumerator`1<Zenject.InjectContext> Zenject.InjectContext_<get_ParentContextsAndSelf>d__51::<>7__wrap1
	RuntimeObject* ___U3CU3E7__wrap1_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3Cget_ParentContextsAndSelfU3Ed__51_t7762F27E1E3E53C1E19B0EB2A69F072222AAF3F3, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3Cget_ParentContextsAndSelfU3Ed__51_t7762F27E1E3E53C1E19B0EB2A69F072222AAF3F3, ___U3CU3E2__current_1)); }
	inline InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3Cget_ParentContextsAndSelfU3Ed__51_t7762F27E1E3E53C1E19B0EB2A69F072222AAF3F3, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3Cget_ParentContextsAndSelfU3Ed__51_t7762F27E1E3E53C1E19B0EB2A69F072222AAF3F3, ___U3CU3E4__this_3)); }
	inline InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_4() { return static_cast<int32_t>(offsetof(U3Cget_ParentContextsAndSelfU3Ed__51_t7762F27E1E3E53C1E19B0EB2A69F072222AAF3F3, ___U3CU3E7__wrap1_4)); }
	inline RuntimeObject* get_U3CU3E7__wrap1_4() const { return ___U3CU3E7__wrap1_4; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap1_4() { return &___U3CU3E7__wrap1_4; }
	inline void set_U3CU3E7__wrap1_4(RuntimeObject* value)
	{
		___U3CU3E7__wrap1_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap1_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGET_PARENTCONTEXTSANDSELFU3ED__51_T7762F27E1E3E53C1E19B0EB2A69F072222AAF3F3_H
#ifndef INJECTUTIL_T0C34AD40453372DB40929117C9254504D6F619FD_H
#define INJECTUTIL_T0C34AD40453372DB40929117C9254504D6F619FD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.InjectUtil
struct  InjectUtil_t0C34AD40453372DB40929117C9254504D6F619FD  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INJECTUTIL_T0C34AD40453372DB40929117C9254504D6F619FD_H
#ifndef U3CU3EC_TFC0B9F2D5D75ECB5126320F842BE4D9212EFC5F7_H
#define U3CU3EC_TFC0B9F2D5D75ECB5126320F842BE4D9212EFC5F7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.InjectUtil_<>c
struct  U3CU3Ec_tFC0B9F2D5D75ECB5126320F842BE4D9212EFC5F7  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tFC0B9F2D5D75ECB5126320F842BE4D9212EFC5F7_StaticFields
{
public:
	// Zenject.InjectUtil_<>c Zenject.InjectUtil_<>c::<>9
	U3CU3Ec_tFC0B9F2D5D75ECB5126320F842BE4D9212EFC5F7 * ___U3CU3E9_0;
	// System.Func`2<System.Object,Zenject.TypeValuePair> Zenject.InjectUtil_<>c::<>9__0_0
	Func_2_t8E2E0C08A0978F2FB285DAC528D584AA264A57F8 * ___U3CU3E9__0_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tFC0B9F2D5D75ECB5126320F842BE4D9212EFC5F7_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tFC0B9F2D5D75ECB5126320F842BE4D9212EFC5F7 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tFC0B9F2D5D75ECB5126320F842BE4D9212EFC5F7 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tFC0B9F2D5D75ECB5126320F842BE4D9212EFC5F7 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__0_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tFC0B9F2D5D75ECB5126320F842BE4D9212EFC5F7_StaticFields, ___U3CU3E9__0_0_1)); }
	inline Func_2_t8E2E0C08A0978F2FB285DAC528D584AA264A57F8 * get_U3CU3E9__0_0_1() const { return ___U3CU3E9__0_0_1; }
	inline Func_2_t8E2E0C08A0978F2FB285DAC528D584AA264A57F8 ** get_address_of_U3CU3E9__0_0_1() { return &___U3CU3E9__0_0_1; }
	inline void set_U3CU3E9__0_0_1(Func_2_t8E2E0C08A0978F2FB285DAC528D584AA264A57F8 * value)
	{
		___U3CU3E9__0_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__0_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TFC0B9F2D5D75ECB5126320F842BE4D9212EFC5F7_H
#ifndef U3CU3EC__DISPLAYCLASS8_0_TE41A303884A7DD72E4B1D252D8D7396FA7F64FC8_H
#define U3CU3EC__DISPLAYCLASS8_0_TE41A303884A7DD72E4B1D252D8D7396FA7F64FC8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.InjectUtil_<>c__DisplayClass8_0
struct  U3CU3Ec__DisplayClass8_0_tE41A303884A7DD72E4B1D252D8D7396FA7F64FC8  : public RuntimeObject
{
public:
	// System.Type Zenject.InjectUtil_<>c__DisplayClass8_0::injectedFieldType
	Type_t * ___injectedFieldType_0;

public:
	inline static int32_t get_offset_of_injectedFieldType_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_0_tE41A303884A7DD72E4B1D252D8D7396FA7F64FC8, ___injectedFieldType_0)); }
	inline Type_t * get_injectedFieldType_0() const { return ___injectedFieldType_0; }
	inline Type_t ** get_address_of_injectedFieldType_0() { return &___injectedFieldType_0; }
	inline void set_injectedFieldType_0(Type_t * value)
	{
		___injectedFieldType_0 = value;
		Il2CppCodeGenWriteBarrier((&___injectedFieldType_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS8_0_TE41A303884A7DD72E4B1D252D8D7396FA7F64FC8_H
#ifndef PROVIDERBINDINGFINALIZER_T3C2E6FBD89B98A057674B926F42EF13E4ED1EAAE_H
#define PROVIDERBINDINGFINALIZER_T3C2E6FBD89B98A057674B926F42EF13E4ED1EAAE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ProviderBindingFinalizer
struct  ProviderBindingFinalizer_t3C2E6FBD89B98A057674B926F42EF13E4ED1EAAE  : public RuntimeObject
{
public:
	// Zenject.BindInfo Zenject.ProviderBindingFinalizer::<BindInfo>k__BackingField
	BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * ___U3CBindInfoU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CBindInfoU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ProviderBindingFinalizer_t3C2E6FBD89B98A057674B926F42EF13E4ED1EAAE, ___U3CBindInfoU3Ek__BackingField_0)); }
	inline BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * get_U3CBindInfoU3Ek__BackingField_0() const { return ___U3CBindInfoU3Ek__BackingField_0; }
	inline BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 ** get_address_of_U3CBindInfoU3Ek__BackingField_0() { return &___U3CBindInfoU3Ek__BackingField_0; }
	inline void set_U3CBindInfoU3Ek__BackingField_0(BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * value)
	{
		___U3CBindInfoU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBindInfoU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROVIDERBINDINGFINALIZER_T3C2E6FBD89B98A057674B926F42EF13E4ED1EAAE_H
#ifndef U3CU3EC_TC6B573730C45D20BE802AC70FD90DA2F39667BC1_H
#define U3CU3EC_TC6B573730C45D20BE802AC70FD90DA2F39667BC1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ProviderBindingFinalizer_<>c
struct  U3CU3Ec_tC6B573730C45D20BE802AC70FD90DA2F39667BC1  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tC6B573730C45D20BE802AC70FD90DA2F39667BC1_StaticFields
{
public:
	// Zenject.ProviderBindingFinalizer_<>c Zenject.ProviderBindingFinalizer_<>c::<>9
	U3CU3Ec_tC6B573730C45D20BE802AC70FD90DA2F39667BC1 * ___U3CU3E9_0;
	// System.Func`2<System.Type,System.String> Zenject.ProviderBindingFinalizer_<>c::<>9__7_0
	Func_2_t42E15778FCD2FCC12128FA2916CE06E4E70655E5 * ___U3CU3E9__7_0_1;
	// System.Func`2<System.Type,System.Type> Zenject.ProviderBindingFinalizer_<>c::<>9__16_0
	Func_2_t9A30598E63DAE9805F56679563D875589D41D2CF * ___U3CU3E9__16_0_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tC6B573730C45D20BE802AC70FD90DA2F39667BC1_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tC6B573730C45D20BE802AC70FD90DA2F39667BC1 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tC6B573730C45D20BE802AC70FD90DA2F39667BC1 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tC6B573730C45D20BE802AC70FD90DA2F39667BC1 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__7_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tC6B573730C45D20BE802AC70FD90DA2F39667BC1_StaticFields, ___U3CU3E9__7_0_1)); }
	inline Func_2_t42E15778FCD2FCC12128FA2916CE06E4E70655E5 * get_U3CU3E9__7_0_1() const { return ___U3CU3E9__7_0_1; }
	inline Func_2_t42E15778FCD2FCC12128FA2916CE06E4E70655E5 ** get_address_of_U3CU3E9__7_0_1() { return &___U3CU3E9__7_0_1; }
	inline void set_U3CU3E9__7_0_1(Func_2_t42E15778FCD2FCC12128FA2916CE06E4E70655E5 * value)
	{
		___U3CU3E9__7_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__7_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__16_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_tC6B573730C45D20BE802AC70FD90DA2F39667BC1_StaticFields, ___U3CU3E9__16_0_2)); }
	inline Func_2_t9A30598E63DAE9805F56679563D875589D41D2CF * get_U3CU3E9__16_0_2() const { return ___U3CU3E9__16_0_2; }
	inline Func_2_t9A30598E63DAE9805F56679563D875589D41D2CF ** get_address_of_U3CU3E9__16_0_2() { return &___U3CU3E9__16_0_2; }
	inline void set_U3CU3E9__16_0_2(Func_2_t9A30598E63DAE9805F56679563D875589D41D2CF * value)
	{
		___U3CU3E9__16_0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__16_0_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TC6B573730C45D20BE802AC70FD90DA2F39667BC1_H
#ifndef U3CU3EC__DISPLAYCLASS16_0_T793513EA7BB7C1E3E34717EFA6502F85704AE2B6_H
#define U3CU3EC__DISPLAYCLASS16_0_T793513EA7BB7C1E3E34717EFA6502F85704AE2B6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ProviderBindingFinalizer_<>c__DisplayClass16_0
struct  U3CU3Ec__DisplayClass16_0_t793513EA7BB7C1E3E34717EFA6502F85704AE2B6  : public RuntimeObject
{
public:
	// System.Func`3<Zenject.DiContainer,System.Type,Zenject.IProvider> Zenject.ProviderBindingFinalizer_<>c__DisplayClass16_0::providerFunc
	Func_3_tF2BBB0DC92895D84974BAECD6D1B2AE7FFC65AAA * ___providerFunc_0;
	// Zenject.DiContainer Zenject.ProviderBindingFinalizer_<>c__DisplayClass16_0::container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ___container_1;

public:
	inline static int32_t get_offset_of_providerFunc_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass16_0_t793513EA7BB7C1E3E34717EFA6502F85704AE2B6, ___providerFunc_0)); }
	inline Func_3_tF2BBB0DC92895D84974BAECD6D1B2AE7FFC65AAA * get_providerFunc_0() const { return ___providerFunc_0; }
	inline Func_3_tF2BBB0DC92895D84974BAECD6D1B2AE7FFC65AAA ** get_address_of_providerFunc_0() { return &___providerFunc_0; }
	inline void set_providerFunc_0(Func_3_tF2BBB0DC92895D84974BAECD6D1B2AE7FFC65AAA * value)
	{
		___providerFunc_0 = value;
		Il2CppCodeGenWriteBarrier((&___providerFunc_0), value);
	}

	inline static int32_t get_offset_of_container_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass16_0_t793513EA7BB7C1E3E34717EFA6502F85704AE2B6, ___container_1)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get_container_1() const { return ___container_1; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of_container_1() { return &___container_1; }
	inline void set_container_1(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		___container_1 = value;
		Il2CppCodeGenWriteBarrier((&___container_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS16_0_T793513EA7BB7C1E3E34717EFA6502F85704AE2B6_H
#ifndef U3CU3EC__DISPLAYCLASS5_0_T7E3ECC94D523D36C05D1BB6635F0BAA0D5C19BB7_H
#define U3CU3EC__DISPLAYCLASS5_0_T7E3ECC94D523D36C05D1BB6635F0BAA0D5C19BB7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ScopableBindingFinalizer_<>c__DisplayClass5_0
struct  U3CU3Ec__DisplayClass5_0_t7E3ECC94D523D36C05D1BB6635F0BAA0D5C19BB7  : public RuntimeObject
{
public:
	// Zenject.DiContainer Zenject.ScopableBindingFinalizer_<>c__DisplayClass5_0::container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ___container_0;
	// Zenject.ScopableBindingFinalizer Zenject.ScopableBindingFinalizer_<>c__DisplayClass5_0::<>4__this
	ScopableBindingFinalizer_t1278F9BE4EE396C7D8B5CA5625C540EC3582632C * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_container_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_t7E3ECC94D523D36C05D1BB6635F0BAA0D5C19BB7, ___container_0)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get_container_0() const { return ___container_0; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of_container_0() { return &___container_0; }
	inline void set_container_0(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		___container_0 = value;
		Il2CppCodeGenWriteBarrier((&___container_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_t7E3ECC94D523D36C05D1BB6635F0BAA0D5C19BB7, ___U3CU3E4__this_1)); }
	inline ScopableBindingFinalizer_t1278F9BE4EE396C7D8B5CA5625C540EC3582632C * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline ScopableBindingFinalizer_t1278F9BE4EE396C7D8B5CA5625C540EC3582632C ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(ScopableBindingFinalizer_t1278F9BE4EE396C7D8B5CA5625C540EC3582632C * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS5_0_T7E3ECC94D523D36C05D1BB6635F0BAA0D5C19BB7_H
#ifndef U3CU3EC__DISPLAYCLASS6_0_T00BC48A39C1F7CA3867EDE947A14D53639A1C439_H
#define U3CU3EC__DISPLAYCLASS6_0_T00BC48A39C1F7CA3867EDE947A14D53639A1C439_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ScopableBindingFinalizer_<>c__DisplayClass6_0
struct  U3CU3Ec__DisplayClass6_0_t00BC48A39C1F7CA3867EDE947A14D53639A1C439  : public RuntimeObject
{
public:
	// Zenject.DiContainer Zenject.ScopableBindingFinalizer_<>c__DisplayClass6_0::container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ___container_0;
	// Zenject.ScopableBindingFinalizer Zenject.ScopableBindingFinalizer_<>c__DisplayClass6_0::<>4__this
	ScopableBindingFinalizer_t1278F9BE4EE396C7D8B5CA5625C540EC3582632C * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_container_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_t00BC48A39C1F7CA3867EDE947A14D53639A1C439, ___container_0)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get_container_0() const { return ___container_0; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of_container_0() { return &___container_0; }
	inline void set_container_0(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		___container_0 = value;
		Il2CppCodeGenWriteBarrier((&___container_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_t00BC48A39C1F7CA3867EDE947A14D53639A1C439, ___U3CU3E4__this_1)); }
	inline ScopableBindingFinalizer_t1278F9BE4EE396C7D8B5CA5625C540EC3582632C * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline ScopableBindingFinalizer_t1278F9BE4EE396C7D8B5CA5625C540EC3582632C ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(ScopableBindingFinalizer_t1278F9BE4EE396C7D8B5CA5625C540EC3582632C * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS6_0_T00BC48A39C1F7CA3867EDE947A14D53639A1C439_H
#ifndef U3CU3EC__DISPLAYCLASS5_0_T64DDCD52B9790DA5BB3DF411F4F3726FD18DA5B1_H
#define U3CU3EC__DISPLAYCLASS5_0_T64DDCD52B9790DA5BB3DF411F4F3726FD18DA5B1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerInstallerBindingFinalizer_<>c__DisplayClass5_0
struct  U3CU3Ec__DisplayClass5_0_t64DDCD52B9790DA5BB3DF411F4F3726FD18DA5B1  : public RuntimeObject
{
public:
	// Zenject.DiContainer Zenject.SubContainerInstallerBindingFinalizer_<>c__DisplayClass5_0::container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ___container_0;
	// Zenject.SubContainerInstallerBindingFinalizer Zenject.SubContainerInstallerBindingFinalizer_<>c__DisplayClass5_0::<>4__this
	SubContainerInstallerBindingFinalizer_t7E7503CA806862CA81C5A2A86E199B15532DC82E * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_container_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_t64DDCD52B9790DA5BB3DF411F4F3726FD18DA5B1, ___container_0)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get_container_0() const { return ___container_0; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of_container_0() { return &___container_0; }
	inline void set_container_0(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		___container_0 = value;
		Il2CppCodeGenWriteBarrier((&___container_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_t64DDCD52B9790DA5BB3DF411F4F3726FD18DA5B1, ___U3CU3E4__this_1)); }
	inline SubContainerInstallerBindingFinalizer_t7E7503CA806862CA81C5A2A86E199B15532DC82E * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline SubContainerInstallerBindingFinalizer_t7E7503CA806862CA81C5A2A86E199B15532DC82E ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(SubContainerInstallerBindingFinalizer_t7E7503CA806862CA81C5A2A86E199B15532DC82E * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS5_0_T64DDCD52B9790DA5BB3DF411F4F3726FD18DA5B1_H
#ifndef U3CU3EC__DISPLAYCLASS5_1_TACDA7E6A2C16247A0B943DA9B6379EC9163AAC02_H
#define U3CU3EC__DISPLAYCLASS5_1_TACDA7E6A2C16247A0B943DA9B6379EC9163AAC02_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerInstallerBindingFinalizer_<>c__DisplayClass5_1
struct  U3CU3Ec__DisplayClass5_1_tACDA7E6A2C16247A0B943DA9B6379EC9163AAC02  : public RuntimeObject
{
public:
	// Zenject.ISubContainerCreator Zenject.SubContainerInstallerBindingFinalizer_<>c__DisplayClass5_1::containerCreator
	RuntimeObject* ___containerCreator_0;
	// Zenject.SubContainerInstallerBindingFinalizer_<>c__DisplayClass5_0 Zenject.SubContainerInstallerBindingFinalizer_<>c__DisplayClass5_1::CSU24<>8__locals1
	U3CU3Ec__DisplayClass5_0_t64DDCD52B9790DA5BB3DF411F4F3726FD18DA5B1 * ___CSU24U3CU3E8__locals1_1;

public:
	inline static int32_t get_offset_of_containerCreator_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_1_tACDA7E6A2C16247A0B943DA9B6379EC9163AAC02, ___containerCreator_0)); }
	inline RuntimeObject* get_containerCreator_0() const { return ___containerCreator_0; }
	inline RuntimeObject** get_address_of_containerCreator_0() { return &___containerCreator_0; }
	inline void set_containerCreator_0(RuntimeObject* value)
	{
		___containerCreator_0 = value;
		Il2CppCodeGenWriteBarrier((&___containerCreator_0), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E8__locals1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_1_tACDA7E6A2C16247A0B943DA9B6379EC9163AAC02, ___CSU24U3CU3E8__locals1_1)); }
	inline U3CU3Ec__DisplayClass5_0_t64DDCD52B9790DA5BB3DF411F4F3726FD18DA5B1 * get_CSU24U3CU3E8__locals1_1() const { return ___CSU24U3CU3E8__locals1_1; }
	inline U3CU3Ec__DisplayClass5_0_t64DDCD52B9790DA5BB3DF411F4F3726FD18DA5B1 ** get_address_of_CSU24U3CU3E8__locals1_1() { return &___CSU24U3CU3E8__locals1_1; }
	inline void set_CSU24U3CU3E8__locals1_1(U3CU3Ec__DisplayClass5_0_t64DDCD52B9790DA5BB3DF411F4F3726FD18DA5B1 * value)
	{
		___CSU24U3CU3E8__locals1_1 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E8__locals1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS5_1_TACDA7E6A2C16247A0B943DA9B6379EC9163AAC02_H
#ifndef U3CU3EC__DISPLAYCLASS6_0_T2FC5487E39FC7D6C3B76178185AD1118043CB391_H
#define U3CU3EC__DISPLAYCLASS6_0_T2FC5487E39FC7D6C3B76178185AD1118043CB391_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerInstallerBindingFinalizer_<>c__DisplayClass6_0
struct  U3CU3Ec__DisplayClass6_0_t2FC5487E39FC7D6C3B76178185AD1118043CB391  : public RuntimeObject
{
public:
	// Zenject.DiContainer Zenject.SubContainerInstallerBindingFinalizer_<>c__DisplayClass6_0::container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ___container_0;
	// Zenject.SubContainerInstallerBindingFinalizer Zenject.SubContainerInstallerBindingFinalizer_<>c__DisplayClass6_0::<>4__this
	SubContainerInstallerBindingFinalizer_t7E7503CA806862CA81C5A2A86E199B15532DC82E * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_container_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_t2FC5487E39FC7D6C3B76178185AD1118043CB391, ___container_0)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get_container_0() const { return ___container_0; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of_container_0() { return &___container_0; }
	inline void set_container_0(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		___container_0 = value;
		Il2CppCodeGenWriteBarrier((&___container_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_t2FC5487E39FC7D6C3B76178185AD1118043CB391, ___U3CU3E4__this_1)); }
	inline SubContainerInstallerBindingFinalizer_t7E7503CA806862CA81C5A2A86E199B15532DC82E * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline SubContainerInstallerBindingFinalizer_t7E7503CA806862CA81C5A2A86E199B15532DC82E ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(SubContainerInstallerBindingFinalizer_t7E7503CA806862CA81C5A2A86E199B15532DC82E * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS6_0_T2FC5487E39FC7D6C3B76178185AD1118043CB391_H
#ifndef U3CU3EC__DISPLAYCLASS6_1_T1236B8429368F99007623955034CA74B654BB03A_H
#define U3CU3EC__DISPLAYCLASS6_1_T1236B8429368F99007623955034CA74B654BB03A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerInstallerBindingFinalizer_<>c__DisplayClass6_1
struct  U3CU3Ec__DisplayClass6_1_t1236B8429368F99007623955034CA74B654BB03A  : public RuntimeObject
{
public:
	// Zenject.ISubContainerCreator Zenject.SubContainerInstallerBindingFinalizer_<>c__DisplayClass6_1::containerCreator
	RuntimeObject* ___containerCreator_0;
	// Zenject.SubContainerInstallerBindingFinalizer_<>c__DisplayClass6_0 Zenject.SubContainerInstallerBindingFinalizer_<>c__DisplayClass6_1::CSU24<>8__locals1
	U3CU3Ec__DisplayClass6_0_t2FC5487E39FC7D6C3B76178185AD1118043CB391 * ___CSU24U3CU3E8__locals1_1;

public:
	inline static int32_t get_offset_of_containerCreator_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_1_t1236B8429368F99007623955034CA74B654BB03A, ___containerCreator_0)); }
	inline RuntimeObject* get_containerCreator_0() const { return ___containerCreator_0; }
	inline RuntimeObject** get_address_of_containerCreator_0() { return &___containerCreator_0; }
	inline void set_containerCreator_0(RuntimeObject* value)
	{
		___containerCreator_0 = value;
		Il2CppCodeGenWriteBarrier((&___containerCreator_0), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E8__locals1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_1_t1236B8429368F99007623955034CA74B654BB03A, ___CSU24U3CU3E8__locals1_1)); }
	inline U3CU3Ec__DisplayClass6_0_t2FC5487E39FC7D6C3B76178185AD1118043CB391 * get_CSU24U3CU3E8__locals1_1() const { return ___CSU24U3CU3E8__locals1_1; }
	inline U3CU3Ec__DisplayClass6_0_t2FC5487E39FC7D6C3B76178185AD1118043CB391 ** get_address_of_CSU24U3CU3E8__locals1_1() { return &___CSU24U3CU3E8__locals1_1; }
	inline void set_CSU24U3CU3E8__locals1_1(U3CU3Ec__DisplayClass6_0_t2FC5487E39FC7D6C3B76178185AD1118043CB391 * value)
	{
		___CSU24U3CU3E8__locals1_1 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E8__locals1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS6_1_T1236B8429368F99007623955034CA74B654BB03A_H
#ifndef U3CU3EC__DISPLAYCLASS4_0_T3528FF845AC546F07044CFEFE66CFDCBB5899B69_H
#define U3CU3EC__DISPLAYCLASS4_0_T3528FF845AC546F07044CFEFE66CFDCBB5899B69_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerMethodBindingFinalizer_<>c__DisplayClass4_0
struct  U3CU3Ec__DisplayClass4_0_t3528FF845AC546F07044CFEFE66CFDCBB5899B69  : public RuntimeObject
{
public:
	// Zenject.DiContainer Zenject.SubContainerMethodBindingFinalizer_<>c__DisplayClass4_0::container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ___container_0;
	// Zenject.SubContainerMethodBindingFinalizer Zenject.SubContainerMethodBindingFinalizer_<>c__DisplayClass4_0::<>4__this
	SubContainerMethodBindingFinalizer_tCC3590F54EDCD60ECB9CC8F1D324DB07191CEB31 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_container_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_t3528FF845AC546F07044CFEFE66CFDCBB5899B69, ___container_0)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get_container_0() const { return ___container_0; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of_container_0() { return &___container_0; }
	inline void set_container_0(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		___container_0 = value;
		Il2CppCodeGenWriteBarrier((&___container_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_t3528FF845AC546F07044CFEFE66CFDCBB5899B69, ___U3CU3E4__this_1)); }
	inline SubContainerMethodBindingFinalizer_tCC3590F54EDCD60ECB9CC8F1D324DB07191CEB31 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline SubContainerMethodBindingFinalizer_tCC3590F54EDCD60ECB9CC8F1D324DB07191CEB31 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(SubContainerMethodBindingFinalizer_tCC3590F54EDCD60ECB9CC8F1D324DB07191CEB31 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS4_0_T3528FF845AC546F07044CFEFE66CFDCBB5899B69_H
#ifndef U3CU3EC__DISPLAYCLASS4_1_T1F379E2C0C54A977ADE5AA93660177E4D3073C59_H
#define U3CU3EC__DISPLAYCLASS4_1_T1F379E2C0C54A977ADE5AA93660177E4D3073C59_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerMethodBindingFinalizer_<>c__DisplayClass4_1
struct  U3CU3Ec__DisplayClass4_1_t1F379E2C0C54A977ADE5AA93660177E4D3073C59  : public RuntimeObject
{
public:
	// Zenject.SubContainerCreatorCached Zenject.SubContainerMethodBindingFinalizer_<>c__DisplayClass4_1::creator
	SubContainerCreatorCached_t8C3674F84167B2FA6DC79C8B318D605610F10EA4 * ___creator_0;
	// Zenject.SubContainerMethodBindingFinalizer_<>c__DisplayClass4_0 Zenject.SubContainerMethodBindingFinalizer_<>c__DisplayClass4_1::CSU24<>8__locals1
	U3CU3Ec__DisplayClass4_0_t3528FF845AC546F07044CFEFE66CFDCBB5899B69 * ___CSU24U3CU3E8__locals1_1;

public:
	inline static int32_t get_offset_of_creator_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_1_t1F379E2C0C54A977ADE5AA93660177E4D3073C59, ___creator_0)); }
	inline SubContainerCreatorCached_t8C3674F84167B2FA6DC79C8B318D605610F10EA4 * get_creator_0() const { return ___creator_0; }
	inline SubContainerCreatorCached_t8C3674F84167B2FA6DC79C8B318D605610F10EA4 ** get_address_of_creator_0() { return &___creator_0; }
	inline void set_creator_0(SubContainerCreatorCached_t8C3674F84167B2FA6DC79C8B318D605610F10EA4 * value)
	{
		___creator_0 = value;
		Il2CppCodeGenWriteBarrier((&___creator_0), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E8__locals1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_1_t1F379E2C0C54A977ADE5AA93660177E4D3073C59, ___CSU24U3CU3E8__locals1_1)); }
	inline U3CU3Ec__DisplayClass4_0_t3528FF845AC546F07044CFEFE66CFDCBB5899B69 * get_CSU24U3CU3E8__locals1_1() const { return ___CSU24U3CU3E8__locals1_1; }
	inline U3CU3Ec__DisplayClass4_0_t3528FF845AC546F07044CFEFE66CFDCBB5899B69 ** get_address_of_CSU24U3CU3E8__locals1_1() { return &___CSU24U3CU3E8__locals1_1; }
	inline void set_CSU24U3CU3E8__locals1_1(U3CU3Ec__DisplayClass4_0_t3528FF845AC546F07044CFEFE66CFDCBB5899B69 * value)
	{
		___CSU24U3CU3E8__locals1_1 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E8__locals1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS4_1_T1F379E2C0C54A977ADE5AA93660177E4D3073C59_H
#ifndef U3CU3EC__DISPLAYCLASS5_0_T7988CB076649CF11F3F91091ECAEEA8E2BD0126A_H
#define U3CU3EC__DISPLAYCLASS5_0_T7988CB076649CF11F3F91091ECAEEA8E2BD0126A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerMethodBindingFinalizer_<>c__DisplayClass5_0
struct  U3CU3Ec__DisplayClass5_0_t7988CB076649CF11F3F91091ECAEEA8E2BD0126A  : public RuntimeObject
{
public:
	// Zenject.DiContainer Zenject.SubContainerMethodBindingFinalizer_<>c__DisplayClass5_0::container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ___container_0;
	// Zenject.SubContainerMethodBindingFinalizer Zenject.SubContainerMethodBindingFinalizer_<>c__DisplayClass5_0::<>4__this
	SubContainerMethodBindingFinalizer_tCC3590F54EDCD60ECB9CC8F1D324DB07191CEB31 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_container_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_t7988CB076649CF11F3F91091ECAEEA8E2BD0126A, ___container_0)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get_container_0() const { return ___container_0; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of_container_0() { return &___container_0; }
	inline void set_container_0(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		___container_0 = value;
		Il2CppCodeGenWriteBarrier((&___container_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_t7988CB076649CF11F3F91091ECAEEA8E2BD0126A, ___U3CU3E4__this_1)); }
	inline SubContainerMethodBindingFinalizer_tCC3590F54EDCD60ECB9CC8F1D324DB07191CEB31 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline SubContainerMethodBindingFinalizer_tCC3590F54EDCD60ECB9CC8F1D324DB07191CEB31 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(SubContainerMethodBindingFinalizer_tCC3590F54EDCD60ECB9CC8F1D324DB07191CEB31 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS5_0_T7988CB076649CF11F3F91091ECAEEA8E2BD0126A_H
#ifndef U3CU3EC__DISPLAYCLASS5_1_T54FF71ACE296A718878232C08591AAD0513AA4D5_H
#define U3CU3EC__DISPLAYCLASS5_1_T54FF71ACE296A718878232C08591AAD0513AA4D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerMethodBindingFinalizer_<>c__DisplayClass5_1
struct  U3CU3Ec__DisplayClass5_1_t54FF71ACE296A718878232C08591AAD0513AA4D5  : public RuntimeObject
{
public:
	// Zenject.SubContainerCreatorCached Zenject.SubContainerMethodBindingFinalizer_<>c__DisplayClass5_1::containerCreator
	SubContainerCreatorCached_t8C3674F84167B2FA6DC79C8B318D605610F10EA4 * ___containerCreator_0;
	// Zenject.SubContainerMethodBindingFinalizer_<>c__DisplayClass5_0 Zenject.SubContainerMethodBindingFinalizer_<>c__DisplayClass5_1::CSU24<>8__locals1
	U3CU3Ec__DisplayClass5_0_t7988CB076649CF11F3F91091ECAEEA8E2BD0126A * ___CSU24U3CU3E8__locals1_1;

public:
	inline static int32_t get_offset_of_containerCreator_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_1_t54FF71ACE296A718878232C08591AAD0513AA4D5, ___containerCreator_0)); }
	inline SubContainerCreatorCached_t8C3674F84167B2FA6DC79C8B318D605610F10EA4 * get_containerCreator_0() const { return ___containerCreator_0; }
	inline SubContainerCreatorCached_t8C3674F84167B2FA6DC79C8B318D605610F10EA4 ** get_address_of_containerCreator_0() { return &___containerCreator_0; }
	inline void set_containerCreator_0(SubContainerCreatorCached_t8C3674F84167B2FA6DC79C8B318D605610F10EA4 * value)
	{
		___containerCreator_0 = value;
		Il2CppCodeGenWriteBarrier((&___containerCreator_0), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E8__locals1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_1_t54FF71ACE296A718878232C08591AAD0513AA4D5, ___CSU24U3CU3E8__locals1_1)); }
	inline U3CU3Ec__DisplayClass5_0_t7988CB076649CF11F3F91091ECAEEA8E2BD0126A * get_CSU24U3CU3E8__locals1_1() const { return ___CSU24U3CU3E8__locals1_1; }
	inline U3CU3Ec__DisplayClass5_0_t7988CB076649CF11F3F91091ECAEEA8E2BD0126A ** get_address_of_CSU24U3CU3E8__locals1_1() { return &___CSU24U3CU3E8__locals1_1; }
	inline void set_CSU24U3CU3E8__locals1_1(U3CU3Ec__DisplayClass5_0_t7988CB076649CF11F3F91091ECAEEA8E2BD0126A * value)
	{
		___CSU24U3CU3E8__locals1_1 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E8__locals1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS5_1_T54FF71ACE296A718878232C08591AAD0513AA4D5_H
#ifndef U3CU3EC__DISPLAYCLASS5_0_T9F9EF9AD88205286601A08217B9F885C6CB7FFAE_H
#define U3CU3EC__DISPLAYCLASS5_0_T9F9EF9AD88205286601A08217B9F885C6CB7FFAE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerPrefabBindingFinalizer_<>c__DisplayClass5_0
struct  U3CU3Ec__DisplayClass5_0_t9F9EF9AD88205286601A08217B9F885C6CB7FFAE  : public RuntimeObject
{
public:
	// Zenject.DiContainer Zenject.SubContainerPrefabBindingFinalizer_<>c__DisplayClass5_0::container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ___container_0;
	// Zenject.SubContainerPrefabBindingFinalizer Zenject.SubContainerPrefabBindingFinalizer_<>c__DisplayClass5_0::<>4__this
	SubContainerPrefabBindingFinalizer_t909CFA4349A1C6B422478412C874D33CFE229618 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_container_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_t9F9EF9AD88205286601A08217B9F885C6CB7FFAE, ___container_0)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get_container_0() const { return ___container_0; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of_container_0() { return &___container_0; }
	inline void set_container_0(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		___container_0 = value;
		Il2CppCodeGenWriteBarrier((&___container_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_t9F9EF9AD88205286601A08217B9F885C6CB7FFAE, ___U3CU3E4__this_1)); }
	inline SubContainerPrefabBindingFinalizer_t909CFA4349A1C6B422478412C874D33CFE229618 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline SubContainerPrefabBindingFinalizer_t909CFA4349A1C6B422478412C874D33CFE229618 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(SubContainerPrefabBindingFinalizer_t909CFA4349A1C6B422478412C874D33CFE229618 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS5_0_T9F9EF9AD88205286601A08217B9F885C6CB7FFAE_H
#ifndef U3CU3EC__DISPLAYCLASS5_1_T14D58B66D54B64167F80F9304E4B19CB2E83A431_H
#define U3CU3EC__DISPLAYCLASS5_1_T14D58B66D54B64167F80F9304E4B19CB2E83A431_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerPrefabBindingFinalizer_<>c__DisplayClass5_1
struct  U3CU3Ec__DisplayClass5_1_t14D58B66D54B64167F80F9304E4B19CB2E83A431  : public RuntimeObject
{
public:
	// Zenject.SubContainerCreatorCached Zenject.SubContainerPrefabBindingFinalizer_<>c__DisplayClass5_1::containerCreator
	SubContainerCreatorCached_t8C3674F84167B2FA6DC79C8B318D605610F10EA4 * ___containerCreator_0;
	// Zenject.SubContainerPrefabBindingFinalizer_<>c__DisplayClass5_0 Zenject.SubContainerPrefabBindingFinalizer_<>c__DisplayClass5_1::CSU24<>8__locals1
	U3CU3Ec__DisplayClass5_0_t9F9EF9AD88205286601A08217B9F885C6CB7FFAE * ___CSU24U3CU3E8__locals1_1;

public:
	inline static int32_t get_offset_of_containerCreator_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_1_t14D58B66D54B64167F80F9304E4B19CB2E83A431, ___containerCreator_0)); }
	inline SubContainerCreatorCached_t8C3674F84167B2FA6DC79C8B318D605610F10EA4 * get_containerCreator_0() const { return ___containerCreator_0; }
	inline SubContainerCreatorCached_t8C3674F84167B2FA6DC79C8B318D605610F10EA4 ** get_address_of_containerCreator_0() { return &___containerCreator_0; }
	inline void set_containerCreator_0(SubContainerCreatorCached_t8C3674F84167B2FA6DC79C8B318D605610F10EA4 * value)
	{
		___containerCreator_0 = value;
		Il2CppCodeGenWriteBarrier((&___containerCreator_0), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E8__locals1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_1_t14D58B66D54B64167F80F9304E4B19CB2E83A431, ___CSU24U3CU3E8__locals1_1)); }
	inline U3CU3Ec__DisplayClass5_0_t9F9EF9AD88205286601A08217B9F885C6CB7FFAE * get_CSU24U3CU3E8__locals1_1() const { return ___CSU24U3CU3E8__locals1_1; }
	inline U3CU3Ec__DisplayClass5_0_t9F9EF9AD88205286601A08217B9F885C6CB7FFAE ** get_address_of_CSU24U3CU3E8__locals1_1() { return &___CSU24U3CU3E8__locals1_1; }
	inline void set_CSU24U3CU3E8__locals1_1(U3CU3Ec__DisplayClass5_0_t9F9EF9AD88205286601A08217B9F885C6CB7FFAE * value)
	{
		___CSU24U3CU3E8__locals1_1 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E8__locals1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS5_1_T14D58B66D54B64167F80F9304E4B19CB2E83A431_H
#ifndef U3CU3EC__DISPLAYCLASS6_0_T303E2EE042B2111EC378D3D496EA2074D6A83847_H
#define U3CU3EC__DISPLAYCLASS6_0_T303E2EE042B2111EC378D3D496EA2074D6A83847_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerPrefabBindingFinalizer_<>c__DisplayClass6_0
struct  U3CU3Ec__DisplayClass6_0_t303E2EE042B2111EC378D3D496EA2074D6A83847  : public RuntimeObject
{
public:
	// Zenject.DiContainer Zenject.SubContainerPrefabBindingFinalizer_<>c__DisplayClass6_0::container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ___container_0;
	// Zenject.SubContainerPrefabBindingFinalizer Zenject.SubContainerPrefabBindingFinalizer_<>c__DisplayClass6_0::<>4__this
	SubContainerPrefabBindingFinalizer_t909CFA4349A1C6B422478412C874D33CFE229618 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_container_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_t303E2EE042B2111EC378D3D496EA2074D6A83847, ___container_0)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get_container_0() const { return ___container_0; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of_container_0() { return &___container_0; }
	inline void set_container_0(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		___container_0 = value;
		Il2CppCodeGenWriteBarrier((&___container_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_t303E2EE042B2111EC378D3D496EA2074D6A83847, ___U3CU3E4__this_1)); }
	inline SubContainerPrefabBindingFinalizer_t909CFA4349A1C6B422478412C874D33CFE229618 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline SubContainerPrefabBindingFinalizer_t909CFA4349A1C6B422478412C874D33CFE229618 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(SubContainerPrefabBindingFinalizer_t909CFA4349A1C6B422478412C874D33CFE229618 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS6_0_T303E2EE042B2111EC378D3D496EA2074D6A83847_H
#ifndef U3CU3EC__DISPLAYCLASS6_1_TEAB5325C21578C9D69B1DDACFA38A9751CB6A4E4_H
#define U3CU3EC__DISPLAYCLASS6_1_TEAB5325C21578C9D69B1DDACFA38A9751CB6A4E4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerPrefabBindingFinalizer_<>c__DisplayClass6_1
struct  U3CU3Ec__DisplayClass6_1_tEAB5325C21578C9D69B1DDACFA38A9751CB6A4E4  : public RuntimeObject
{
public:
	// Zenject.SubContainerCreatorCached Zenject.SubContainerPrefabBindingFinalizer_<>c__DisplayClass6_1::containerCreator
	SubContainerCreatorCached_t8C3674F84167B2FA6DC79C8B318D605610F10EA4 * ___containerCreator_0;
	// Zenject.SubContainerPrefabBindingFinalizer_<>c__DisplayClass6_0 Zenject.SubContainerPrefabBindingFinalizer_<>c__DisplayClass6_1::CSU24<>8__locals1
	U3CU3Ec__DisplayClass6_0_t303E2EE042B2111EC378D3D496EA2074D6A83847 * ___CSU24U3CU3E8__locals1_1;

public:
	inline static int32_t get_offset_of_containerCreator_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_1_tEAB5325C21578C9D69B1DDACFA38A9751CB6A4E4, ___containerCreator_0)); }
	inline SubContainerCreatorCached_t8C3674F84167B2FA6DC79C8B318D605610F10EA4 * get_containerCreator_0() const { return ___containerCreator_0; }
	inline SubContainerCreatorCached_t8C3674F84167B2FA6DC79C8B318D605610F10EA4 ** get_address_of_containerCreator_0() { return &___containerCreator_0; }
	inline void set_containerCreator_0(SubContainerCreatorCached_t8C3674F84167B2FA6DC79C8B318D605610F10EA4 * value)
	{
		___containerCreator_0 = value;
		Il2CppCodeGenWriteBarrier((&___containerCreator_0), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E8__locals1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_1_tEAB5325C21578C9D69B1DDACFA38A9751CB6A4E4, ___CSU24U3CU3E8__locals1_1)); }
	inline U3CU3Ec__DisplayClass6_0_t303E2EE042B2111EC378D3D496EA2074D6A83847 * get_CSU24U3CU3E8__locals1_1() const { return ___CSU24U3CU3E8__locals1_1; }
	inline U3CU3Ec__DisplayClass6_0_t303E2EE042B2111EC378D3D496EA2074D6A83847 ** get_address_of_CSU24U3CU3E8__locals1_1() { return &___CSU24U3CU3E8__locals1_1; }
	inline void set_CSU24U3CU3E8__locals1_1(U3CU3Ec__DisplayClass6_0_t303E2EE042B2111EC378D3D496EA2074D6A83847 * value)
	{
		___CSU24U3CU3E8__locals1_1 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E8__locals1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS6_1_TEAB5325C21578C9D69B1DDACFA38A9751CB6A4E4_H
#ifndef U3CU3EC__DISPLAYCLASS5_0_TCC18CA0B7418964E47FFC397F2B05D1C5D411977_H
#define U3CU3EC__DISPLAYCLASS5_0_TCC18CA0B7418964E47FFC397F2B05D1C5D411977_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerPrefabResourceBindingFinalizer_<>c__DisplayClass5_0
struct  U3CU3Ec__DisplayClass5_0_tCC18CA0B7418964E47FFC397F2B05D1C5D411977  : public RuntimeObject
{
public:
	// Zenject.DiContainer Zenject.SubContainerPrefabResourceBindingFinalizer_<>c__DisplayClass5_0::container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ___container_0;
	// Zenject.SubContainerPrefabResourceBindingFinalizer Zenject.SubContainerPrefabResourceBindingFinalizer_<>c__DisplayClass5_0::<>4__this
	SubContainerPrefabResourceBindingFinalizer_t25917592BD3C28B58C7F81D40A8F7D2FABD0E1CC * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_container_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_tCC18CA0B7418964E47FFC397F2B05D1C5D411977, ___container_0)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get_container_0() const { return ___container_0; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of_container_0() { return &___container_0; }
	inline void set_container_0(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		___container_0 = value;
		Il2CppCodeGenWriteBarrier((&___container_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_tCC18CA0B7418964E47FFC397F2B05D1C5D411977, ___U3CU3E4__this_1)); }
	inline SubContainerPrefabResourceBindingFinalizer_t25917592BD3C28B58C7F81D40A8F7D2FABD0E1CC * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline SubContainerPrefabResourceBindingFinalizer_t25917592BD3C28B58C7F81D40A8F7D2FABD0E1CC ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(SubContainerPrefabResourceBindingFinalizer_t25917592BD3C28B58C7F81D40A8F7D2FABD0E1CC * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS5_0_TCC18CA0B7418964E47FFC397F2B05D1C5D411977_H
#ifndef U3CU3EC__DISPLAYCLASS5_1_TAC4960C5B15B3D4E694F1A346E3B9A1D17D5EAF5_H
#define U3CU3EC__DISPLAYCLASS5_1_TAC4960C5B15B3D4E694F1A346E3B9A1D17D5EAF5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerPrefabResourceBindingFinalizer_<>c__DisplayClass5_1
struct  U3CU3Ec__DisplayClass5_1_tAC4960C5B15B3D4E694F1A346E3B9A1D17D5EAF5  : public RuntimeObject
{
public:
	// Zenject.SubContainerCreatorCached Zenject.SubContainerPrefabResourceBindingFinalizer_<>c__DisplayClass5_1::containerCreator
	SubContainerCreatorCached_t8C3674F84167B2FA6DC79C8B318D605610F10EA4 * ___containerCreator_0;
	// Zenject.SubContainerPrefabResourceBindingFinalizer_<>c__DisplayClass5_0 Zenject.SubContainerPrefabResourceBindingFinalizer_<>c__DisplayClass5_1::CSU24<>8__locals1
	U3CU3Ec__DisplayClass5_0_tCC18CA0B7418964E47FFC397F2B05D1C5D411977 * ___CSU24U3CU3E8__locals1_1;

public:
	inline static int32_t get_offset_of_containerCreator_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_1_tAC4960C5B15B3D4E694F1A346E3B9A1D17D5EAF5, ___containerCreator_0)); }
	inline SubContainerCreatorCached_t8C3674F84167B2FA6DC79C8B318D605610F10EA4 * get_containerCreator_0() const { return ___containerCreator_0; }
	inline SubContainerCreatorCached_t8C3674F84167B2FA6DC79C8B318D605610F10EA4 ** get_address_of_containerCreator_0() { return &___containerCreator_0; }
	inline void set_containerCreator_0(SubContainerCreatorCached_t8C3674F84167B2FA6DC79C8B318D605610F10EA4 * value)
	{
		___containerCreator_0 = value;
		Il2CppCodeGenWriteBarrier((&___containerCreator_0), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E8__locals1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_1_tAC4960C5B15B3D4E694F1A346E3B9A1D17D5EAF5, ___CSU24U3CU3E8__locals1_1)); }
	inline U3CU3Ec__DisplayClass5_0_tCC18CA0B7418964E47FFC397F2B05D1C5D411977 * get_CSU24U3CU3E8__locals1_1() const { return ___CSU24U3CU3E8__locals1_1; }
	inline U3CU3Ec__DisplayClass5_0_tCC18CA0B7418964E47FFC397F2B05D1C5D411977 ** get_address_of_CSU24U3CU3E8__locals1_1() { return &___CSU24U3CU3E8__locals1_1; }
	inline void set_CSU24U3CU3E8__locals1_1(U3CU3Ec__DisplayClass5_0_tCC18CA0B7418964E47FFC397F2B05D1C5D411977 * value)
	{
		___CSU24U3CU3E8__locals1_1 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E8__locals1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS5_1_TAC4960C5B15B3D4E694F1A346E3B9A1D17D5EAF5_H
#ifndef U3CU3EC__DISPLAYCLASS6_0_T29364056A3133169CB10C6EC6C948A84D0F3C4B2_H
#define U3CU3EC__DISPLAYCLASS6_0_T29364056A3133169CB10C6EC6C948A84D0F3C4B2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerPrefabResourceBindingFinalizer_<>c__DisplayClass6_0
struct  U3CU3Ec__DisplayClass6_0_t29364056A3133169CB10C6EC6C948A84D0F3C4B2  : public RuntimeObject
{
public:
	// Zenject.DiContainer Zenject.SubContainerPrefabResourceBindingFinalizer_<>c__DisplayClass6_0::container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ___container_0;
	// Zenject.SubContainerPrefabResourceBindingFinalizer Zenject.SubContainerPrefabResourceBindingFinalizer_<>c__DisplayClass6_0::<>4__this
	SubContainerPrefabResourceBindingFinalizer_t25917592BD3C28B58C7F81D40A8F7D2FABD0E1CC * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_container_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_t29364056A3133169CB10C6EC6C948A84D0F3C4B2, ___container_0)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get_container_0() const { return ___container_0; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of_container_0() { return &___container_0; }
	inline void set_container_0(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		___container_0 = value;
		Il2CppCodeGenWriteBarrier((&___container_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_t29364056A3133169CB10C6EC6C948A84D0F3C4B2, ___U3CU3E4__this_1)); }
	inline SubContainerPrefabResourceBindingFinalizer_t25917592BD3C28B58C7F81D40A8F7D2FABD0E1CC * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline SubContainerPrefabResourceBindingFinalizer_t25917592BD3C28B58C7F81D40A8F7D2FABD0E1CC ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(SubContainerPrefabResourceBindingFinalizer_t25917592BD3C28B58C7F81D40A8F7D2FABD0E1CC * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS6_0_T29364056A3133169CB10C6EC6C948A84D0F3C4B2_H
#ifndef U3CU3EC__DISPLAYCLASS6_1_TB080A1DD076310B54FFD7002DFBD5EDAC7843BBF_H
#define U3CU3EC__DISPLAYCLASS6_1_TB080A1DD076310B54FFD7002DFBD5EDAC7843BBF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerPrefabResourceBindingFinalizer_<>c__DisplayClass6_1
struct  U3CU3Ec__DisplayClass6_1_tB080A1DD076310B54FFD7002DFBD5EDAC7843BBF  : public RuntimeObject
{
public:
	// Zenject.SubContainerCreatorCached Zenject.SubContainerPrefabResourceBindingFinalizer_<>c__DisplayClass6_1::containerCreator
	SubContainerCreatorCached_t8C3674F84167B2FA6DC79C8B318D605610F10EA4 * ___containerCreator_0;
	// Zenject.SubContainerPrefabResourceBindingFinalizer_<>c__DisplayClass6_0 Zenject.SubContainerPrefabResourceBindingFinalizer_<>c__DisplayClass6_1::CSU24<>8__locals1
	U3CU3Ec__DisplayClass6_0_t29364056A3133169CB10C6EC6C948A84D0F3C4B2 * ___CSU24U3CU3E8__locals1_1;

public:
	inline static int32_t get_offset_of_containerCreator_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_1_tB080A1DD076310B54FFD7002DFBD5EDAC7843BBF, ___containerCreator_0)); }
	inline SubContainerCreatorCached_t8C3674F84167B2FA6DC79C8B318D605610F10EA4 * get_containerCreator_0() const { return ___containerCreator_0; }
	inline SubContainerCreatorCached_t8C3674F84167B2FA6DC79C8B318D605610F10EA4 ** get_address_of_containerCreator_0() { return &___containerCreator_0; }
	inline void set_containerCreator_0(SubContainerCreatorCached_t8C3674F84167B2FA6DC79C8B318D605610F10EA4 * value)
	{
		___containerCreator_0 = value;
		Il2CppCodeGenWriteBarrier((&___containerCreator_0), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E8__locals1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_1_tB080A1DD076310B54FFD7002DFBD5EDAC7843BBF, ___CSU24U3CU3E8__locals1_1)); }
	inline U3CU3Ec__DisplayClass6_0_t29364056A3133169CB10C6EC6C948A84D0F3C4B2 * get_CSU24U3CU3E8__locals1_1() const { return ___CSU24U3CU3E8__locals1_1; }
	inline U3CU3Ec__DisplayClass6_0_t29364056A3133169CB10C6EC6C948A84D0F3C4B2 ** get_address_of_CSU24U3CU3E8__locals1_1() { return &___CSU24U3CU3E8__locals1_1; }
	inline void set_CSU24U3CU3E8__locals1_1(U3CU3Ec__DisplayClass6_0_t29364056A3133169CB10C6EC6C948A84D0F3C4B2 * value)
	{
		___CSU24U3CU3E8__locals1_1 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E8__locals1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS6_1_TB080A1DD076310B54FFD7002DFBD5EDAC7843BBF_H
#ifndef TYPEVALUEPAIR_TD309CDC658476FDDF08345C92D3F4D7A0A1F9792_H
#define TYPEVALUEPAIR_TD309CDC658476FDDF08345C92D3F4D7A0A1F9792_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.TypeValuePair
struct  TypeValuePair_tD309CDC658476FDDF08345C92D3F4D7A0A1F9792  : public RuntimeObject
{
public:
	// System.Type Zenject.TypeValuePair::Type
	Type_t * ___Type_0;
	// System.Object Zenject.TypeValuePair::Value
	RuntimeObject * ___Value_1;

public:
	inline static int32_t get_offset_of_Type_0() { return static_cast<int32_t>(offsetof(TypeValuePair_tD309CDC658476FDDF08345C92D3F4D7A0A1F9792, ___Type_0)); }
	inline Type_t * get_Type_0() const { return ___Type_0; }
	inline Type_t ** get_address_of_Type_0() { return &___Type_0; }
	inline void set_Type_0(Type_t * value)
	{
		___Type_0 = value;
		Il2CppCodeGenWriteBarrier((&___Type_0), value);
	}

	inline static int32_t get_offset_of_Value_1() { return static_cast<int32_t>(offsetof(TypeValuePair_tD309CDC658476FDDF08345C92D3F4D7A0A1F9792, ___Value_1)); }
	inline RuntimeObject * get_Value_1() const { return ___Value_1; }
	inline RuntimeObject ** get_address_of_Value_1() { return &___Value_1; }
	inline void set_Value_1(RuntimeObject * value)
	{
		___Value_1 = value;
		Il2CppCodeGenWriteBarrier((&___Value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEVALUEPAIR_TD309CDC658476FDDF08345C92D3F4D7A0A1F9792_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef POOLEXCEEDEDFIXEDSIZEEXCEPTION_T0B1C487A895130032B04F4CE17810CDF192B9012_H
#define POOLEXCEEDEDFIXEDSIZEEXCEPTION_T0B1C487A895130032B04F4CE17810CDF192B9012_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.PoolExceededFixedSizeException
struct  PoolExceededFixedSizeException_t0B1C487A895130032B04F4CE17810CDF192B9012  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POOLEXCEEDEDFIXEDSIZEEXCEPTION_T0B1C487A895130032B04F4CE17810CDF192B9012_H
#ifndef SINGLEPROVIDERBINDINGFINALIZER_TF4B4915E023EC3754F27FCDD815D93844002C5BB_H
#define SINGLEPROVIDERBINDINGFINALIZER_TF4B4915E023EC3754F27FCDD815D93844002C5BB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SingleProviderBindingFinalizer
struct  SingleProviderBindingFinalizer_tF4B4915E023EC3754F27FCDD815D93844002C5BB  : public ProviderBindingFinalizer_t3C2E6FBD89B98A057674B926F42EF13E4ED1EAAE
{
public:
	// System.Func`3<Zenject.DiContainer,System.Type,Zenject.IProvider> Zenject.SingleProviderBindingFinalizer::_providerFactory
	Func_3_tF2BBB0DC92895D84974BAECD6D1B2AE7FFC65AAA * ____providerFactory_1;

public:
	inline static int32_t get_offset_of__providerFactory_1() { return static_cast<int32_t>(offsetof(SingleProviderBindingFinalizer_tF4B4915E023EC3754F27FCDD815D93844002C5BB, ____providerFactory_1)); }
	inline Func_3_tF2BBB0DC92895D84974BAECD6D1B2AE7FFC65AAA * get__providerFactory_1() const { return ____providerFactory_1; }
	inline Func_3_tF2BBB0DC92895D84974BAECD6D1B2AE7FFC65AAA ** get_address_of__providerFactory_1() { return &____providerFactory_1; }
	inline void set__providerFactory_1(Func_3_tF2BBB0DC92895D84974BAECD6D1B2AE7FFC65AAA * value)
	{
		____providerFactory_1 = value;
		Il2CppCodeGenWriteBarrier((&____providerFactory_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLEPROVIDERBINDINGFINALIZER_TF4B4915E023EC3754F27FCDD815D93844002C5BB_H
#ifndef SUBCONTAINERINSTALLERBINDINGFINALIZER_T7E7503CA806862CA81C5A2A86E199B15532DC82E_H
#define SUBCONTAINERINSTALLERBINDINGFINALIZER_T7E7503CA806862CA81C5A2A86E199B15532DC82E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerInstallerBindingFinalizer
struct  SubContainerInstallerBindingFinalizer_t7E7503CA806862CA81C5A2A86E199B15532DC82E  : public ProviderBindingFinalizer_t3C2E6FBD89B98A057674B926F42EF13E4ED1EAAE
{
public:
	// System.Object Zenject.SubContainerInstallerBindingFinalizer::_subIdentifier
	RuntimeObject * ____subIdentifier_1;
	// System.Type Zenject.SubContainerInstallerBindingFinalizer::_installerType
	Type_t * ____installerType_2;

public:
	inline static int32_t get_offset_of__subIdentifier_1() { return static_cast<int32_t>(offsetof(SubContainerInstallerBindingFinalizer_t7E7503CA806862CA81C5A2A86E199B15532DC82E, ____subIdentifier_1)); }
	inline RuntimeObject * get__subIdentifier_1() const { return ____subIdentifier_1; }
	inline RuntimeObject ** get_address_of__subIdentifier_1() { return &____subIdentifier_1; }
	inline void set__subIdentifier_1(RuntimeObject * value)
	{
		____subIdentifier_1 = value;
		Il2CppCodeGenWriteBarrier((&____subIdentifier_1), value);
	}

	inline static int32_t get_offset_of__installerType_2() { return static_cast<int32_t>(offsetof(SubContainerInstallerBindingFinalizer_t7E7503CA806862CA81C5A2A86E199B15532DC82E, ____installerType_2)); }
	inline Type_t * get__installerType_2() const { return ____installerType_2; }
	inline Type_t ** get_address_of__installerType_2() { return &____installerType_2; }
	inline void set__installerType_2(Type_t * value)
	{
		____installerType_2 = value;
		Il2CppCodeGenWriteBarrier((&____installerType_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBCONTAINERINSTALLERBINDINGFINALIZER_T7E7503CA806862CA81C5A2A86E199B15532DC82E_H
#ifndef SUBCONTAINERMETHODBINDINGFINALIZER_TCC3590F54EDCD60ECB9CC8F1D324DB07191CEB31_H
#define SUBCONTAINERMETHODBINDINGFINALIZER_TCC3590F54EDCD60ECB9CC8F1D324DB07191CEB31_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerMethodBindingFinalizer
struct  SubContainerMethodBindingFinalizer_tCC3590F54EDCD60ECB9CC8F1D324DB07191CEB31  : public ProviderBindingFinalizer_t3C2E6FBD89B98A057674B926F42EF13E4ED1EAAE
{
public:
	// System.Object Zenject.SubContainerMethodBindingFinalizer::_subIdentifier
	RuntimeObject * ____subIdentifier_1;
	// System.Action`1<Zenject.DiContainer> Zenject.SubContainerMethodBindingFinalizer::_installMethod
	Action_1_t47FB75EB7E275E7EC20C1ADC32AC936BD0BD4E84 * ____installMethod_2;

public:
	inline static int32_t get_offset_of__subIdentifier_1() { return static_cast<int32_t>(offsetof(SubContainerMethodBindingFinalizer_tCC3590F54EDCD60ECB9CC8F1D324DB07191CEB31, ____subIdentifier_1)); }
	inline RuntimeObject * get__subIdentifier_1() const { return ____subIdentifier_1; }
	inline RuntimeObject ** get_address_of__subIdentifier_1() { return &____subIdentifier_1; }
	inline void set__subIdentifier_1(RuntimeObject * value)
	{
		____subIdentifier_1 = value;
		Il2CppCodeGenWriteBarrier((&____subIdentifier_1), value);
	}

	inline static int32_t get_offset_of__installMethod_2() { return static_cast<int32_t>(offsetof(SubContainerMethodBindingFinalizer_tCC3590F54EDCD60ECB9CC8F1D324DB07191CEB31, ____installMethod_2)); }
	inline Action_1_t47FB75EB7E275E7EC20C1ADC32AC936BD0BD4E84 * get__installMethod_2() const { return ____installMethod_2; }
	inline Action_1_t47FB75EB7E275E7EC20C1ADC32AC936BD0BD4E84 ** get_address_of__installMethod_2() { return &____installMethod_2; }
	inline void set__installMethod_2(Action_1_t47FB75EB7E275E7EC20C1ADC32AC936BD0BD4E84 * value)
	{
		____installMethod_2 = value;
		Il2CppCodeGenWriteBarrier((&____installMethod_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBCONTAINERMETHODBINDINGFINALIZER_TCC3590F54EDCD60ECB9CC8F1D324DB07191CEB31_H
#ifndef SUBCONTAINERPREFABBINDINGFINALIZER_T909CFA4349A1C6B422478412C874D33CFE229618_H
#define SUBCONTAINERPREFABBINDINGFINALIZER_T909CFA4349A1C6B422478412C874D33CFE229618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerPrefabBindingFinalizer
struct  SubContainerPrefabBindingFinalizer_t909CFA4349A1C6B422478412C874D33CFE229618  : public ProviderBindingFinalizer_t3C2E6FBD89B98A057674B926F42EF13E4ED1EAAE
{
public:
	// UnityEngine.Object Zenject.SubContainerPrefabBindingFinalizer::_prefab
	Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ____prefab_1;
	// System.Object Zenject.SubContainerPrefabBindingFinalizer::_subIdentifier
	RuntimeObject * ____subIdentifier_2;
	// Zenject.GameObjectCreationParameters Zenject.SubContainerPrefabBindingFinalizer::_gameObjectBindInfo
	GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A * ____gameObjectBindInfo_3;

public:
	inline static int32_t get_offset_of__prefab_1() { return static_cast<int32_t>(offsetof(SubContainerPrefabBindingFinalizer_t909CFA4349A1C6B422478412C874D33CFE229618, ____prefab_1)); }
	inline Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * get__prefab_1() const { return ____prefab_1; }
	inline Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 ** get_address_of__prefab_1() { return &____prefab_1; }
	inline void set__prefab_1(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * value)
	{
		____prefab_1 = value;
		Il2CppCodeGenWriteBarrier((&____prefab_1), value);
	}

	inline static int32_t get_offset_of__subIdentifier_2() { return static_cast<int32_t>(offsetof(SubContainerPrefabBindingFinalizer_t909CFA4349A1C6B422478412C874D33CFE229618, ____subIdentifier_2)); }
	inline RuntimeObject * get__subIdentifier_2() const { return ____subIdentifier_2; }
	inline RuntimeObject ** get_address_of__subIdentifier_2() { return &____subIdentifier_2; }
	inline void set__subIdentifier_2(RuntimeObject * value)
	{
		____subIdentifier_2 = value;
		Il2CppCodeGenWriteBarrier((&____subIdentifier_2), value);
	}

	inline static int32_t get_offset_of__gameObjectBindInfo_3() { return static_cast<int32_t>(offsetof(SubContainerPrefabBindingFinalizer_t909CFA4349A1C6B422478412C874D33CFE229618, ____gameObjectBindInfo_3)); }
	inline GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A * get__gameObjectBindInfo_3() const { return ____gameObjectBindInfo_3; }
	inline GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A ** get_address_of__gameObjectBindInfo_3() { return &____gameObjectBindInfo_3; }
	inline void set__gameObjectBindInfo_3(GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A * value)
	{
		____gameObjectBindInfo_3 = value;
		Il2CppCodeGenWriteBarrier((&____gameObjectBindInfo_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBCONTAINERPREFABBINDINGFINALIZER_T909CFA4349A1C6B422478412C874D33CFE229618_H
#ifndef SUBCONTAINERPREFABRESOURCEBINDINGFINALIZER_T25917592BD3C28B58C7F81D40A8F7D2FABD0E1CC_H
#define SUBCONTAINERPREFABRESOURCEBINDINGFINALIZER_T25917592BD3C28B58C7F81D40A8F7D2FABD0E1CC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerPrefabResourceBindingFinalizer
struct  SubContainerPrefabResourceBindingFinalizer_t25917592BD3C28B58C7F81D40A8F7D2FABD0E1CC  : public ProviderBindingFinalizer_t3C2E6FBD89B98A057674B926F42EF13E4ED1EAAE
{
public:
	// System.String Zenject.SubContainerPrefabResourceBindingFinalizer::_resourcePath
	String_t* ____resourcePath_1;
	// System.Object Zenject.SubContainerPrefabResourceBindingFinalizer::_subIdentifier
	RuntimeObject * ____subIdentifier_2;
	// Zenject.GameObjectCreationParameters Zenject.SubContainerPrefabResourceBindingFinalizer::_gameObjectBindInfo
	GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A * ____gameObjectBindInfo_3;

public:
	inline static int32_t get_offset_of__resourcePath_1() { return static_cast<int32_t>(offsetof(SubContainerPrefabResourceBindingFinalizer_t25917592BD3C28B58C7F81D40A8F7D2FABD0E1CC, ____resourcePath_1)); }
	inline String_t* get__resourcePath_1() const { return ____resourcePath_1; }
	inline String_t** get_address_of__resourcePath_1() { return &____resourcePath_1; }
	inline void set__resourcePath_1(String_t* value)
	{
		____resourcePath_1 = value;
		Il2CppCodeGenWriteBarrier((&____resourcePath_1), value);
	}

	inline static int32_t get_offset_of__subIdentifier_2() { return static_cast<int32_t>(offsetof(SubContainerPrefabResourceBindingFinalizer_t25917592BD3C28B58C7F81D40A8F7D2FABD0E1CC, ____subIdentifier_2)); }
	inline RuntimeObject * get__subIdentifier_2() const { return ____subIdentifier_2; }
	inline RuntimeObject ** get_address_of__subIdentifier_2() { return &____subIdentifier_2; }
	inline void set__subIdentifier_2(RuntimeObject * value)
	{
		____subIdentifier_2 = value;
		Il2CppCodeGenWriteBarrier((&____subIdentifier_2), value);
	}

	inline static int32_t get_offset_of__gameObjectBindInfo_3() { return static_cast<int32_t>(offsetof(SubContainerPrefabResourceBindingFinalizer_t25917592BD3C28B58C7F81D40A8F7D2FABD0E1CC, ____gameObjectBindInfo_3)); }
	inline GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A * get__gameObjectBindInfo_3() const { return ____gameObjectBindInfo_3; }
	inline GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A ** get_address_of__gameObjectBindInfo_3() { return &____gameObjectBindInfo_3; }
	inline void set__gameObjectBindInfo_3(GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A * value)
	{
		____gameObjectBindInfo_3 = value;
		Il2CppCodeGenWriteBarrier((&____gameObjectBindInfo_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBCONTAINERPREFABRESOURCEBINDINGFINALIZER_T25917592BD3C28B58C7F81D40A8F7D2FABD0E1CC_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef INJECTSOURCES_T643ECF8A27899CEE7D40760762395F6C3E359049_H
#define INJECTSOURCES_T643ECF8A27899CEE7D40760762395F6C3E359049_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.InjectSources
struct  InjectSources_t643ECF8A27899CEE7D40760762395F6C3E359049 
{
public:
	// System.Int32 Zenject.InjectSources::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InjectSources_t643ECF8A27899CEE7D40760762395F6C3E359049, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INJECTSOURCES_T643ECF8A27899CEE7D40760762395F6C3E359049_H
#ifndef POOLEXPANDMETHODS_T880D0B909A07FAA5A93BBE2918D20E825322B07E_H
#define POOLEXPANDMETHODS_T880D0B909A07FAA5A93BBE2918D20E825322B07E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.PoolExpandMethods
struct  PoolExpandMethods_t880D0B909A07FAA5A93BBE2918D20E825322B07E 
{
public:
	// System.Int32 Zenject.PoolExpandMethods::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PoolExpandMethods_t880D0B909A07FAA5A93BBE2918D20E825322B07E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POOLEXPANDMETHODS_T880D0B909A07FAA5A93BBE2918D20E825322B07E_H
#ifndef SINGLETONTYPES_T95744D53A57B5D7318E65315B811CD90544AEF62_H
#define SINGLETONTYPES_T95744D53A57B5D7318E65315B811CD90544AEF62_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SingletonTypes
struct  SingletonTypes_t95744D53A57B5D7318E65315B811CD90544AEF62 
{
public:
	// System.Int32 Zenject.SingletonTypes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SingletonTypes_t95744D53A57B5D7318E65315B811CD90544AEF62, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETONTYPES_T95744D53A57B5D7318E65315B811CD90544AEF62_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef INJECTCONTEXT_TD1BDE94C49785956950BCFD5E1693537A7008FFF_H
#define INJECTCONTEXT_TD1BDE94C49785956950BCFD5E1693537A7008FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.InjectContext
struct  InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF  : public RuntimeObject
{
public:
	// System.Type Zenject.InjectContext::<ObjectType>k__BackingField
	Type_t * ___U3CObjectTypeU3Ek__BackingField_0;
	// Zenject.InjectContext Zenject.InjectContext::<ParentContext>k__BackingField
	InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * ___U3CParentContextU3Ek__BackingField_1;
	// System.Object Zenject.InjectContext::<ObjectInstance>k__BackingField
	RuntimeObject * ___U3CObjectInstanceU3Ek__BackingField_2;
	// System.Object Zenject.InjectContext::<Identifier>k__BackingField
	RuntimeObject * ___U3CIdentifierU3Ek__BackingField_3;
	// System.Object Zenject.InjectContext::<ConcreteIdentifier>k__BackingField
	RuntimeObject * ___U3CConcreteIdentifierU3Ek__BackingField_4;
	// System.String Zenject.InjectContext::<MemberName>k__BackingField
	String_t* ___U3CMemberNameU3Ek__BackingField_5;
	// System.Type Zenject.InjectContext::<MemberType>k__BackingField
	Type_t * ___U3CMemberTypeU3Ek__BackingField_6;
	// System.Boolean Zenject.InjectContext::<Optional>k__BackingField
	bool ___U3COptionalU3Ek__BackingField_7;
	// Zenject.InjectSources Zenject.InjectContext::<SourceType>k__BackingField
	int32_t ___U3CSourceTypeU3Ek__BackingField_8;
	// System.Object Zenject.InjectContext::<FallBackValue>k__BackingField
	RuntimeObject * ___U3CFallBackValueU3Ek__BackingField_9;
	// Zenject.DiContainer Zenject.InjectContext::<Container>k__BackingField
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ___U3CContainerU3Ek__BackingField_10;

public:
	inline static int32_t get_offset_of_U3CObjectTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF, ___U3CObjectTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CObjectTypeU3Ek__BackingField_0() const { return ___U3CObjectTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CObjectTypeU3Ek__BackingField_0() { return &___U3CObjectTypeU3Ek__BackingField_0; }
	inline void set_U3CObjectTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CObjectTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CObjectTypeU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CParentContextU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF, ___U3CParentContextU3Ek__BackingField_1)); }
	inline InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * get_U3CParentContextU3Ek__BackingField_1() const { return ___U3CParentContextU3Ek__BackingField_1; }
	inline InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF ** get_address_of_U3CParentContextU3Ek__BackingField_1() { return &___U3CParentContextU3Ek__BackingField_1; }
	inline void set_U3CParentContextU3Ek__BackingField_1(InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * value)
	{
		___U3CParentContextU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CParentContextU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CObjectInstanceU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF, ___U3CObjectInstanceU3Ek__BackingField_2)); }
	inline RuntimeObject * get_U3CObjectInstanceU3Ek__BackingField_2() const { return ___U3CObjectInstanceU3Ek__BackingField_2; }
	inline RuntimeObject ** get_address_of_U3CObjectInstanceU3Ek__BackingField_2() { return &___U3CObjectInstanceU3Ek__BackingField_2; }
	inline void set_U3CObjectInstanceU3Ek__BackingField_2(RuntimeObject * value)
	{
		___U3CObjectInstanceU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CObjectInstanceU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CIdentifierU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF, ___U3CIdentifierU3Ek__BackingField_3)); }
	inline RuntimeObject * get_U3CIdentifierU3Ek__BackingField_3() const { return ___U3CIdentifierU3Ek__BackingField_3; }
	inline RuntimeObject ** get_address_of_U3CIdentifierU3Ek__BackingField_3() { return &___U3CIdentifierU3Ek__BackingField_3; }
	inline void set_U3CIdentifierU3Ek__BackingField_3(RuntimeObject * value)
	{
		___U3CIdentifierU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CIdentifierU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CConcreteIdentifierU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF, ___U3CConcreteIdentifierU3Ek__BackingField_4)); }
	inline RuntimeObject * get_U3CConcreteIdentifierU3Ek__BackingField_4() const { return ___U3CConcreteIdentifierU3Ek__BackingField_4; }
	inline RuntimeObject ** get_address_of_U3CConcreteIdentifierU3Ek__BackingField_4() { return &___U3CConcreteIdentifierU3Ek__BackingField_4; }
	inline void set_U3CConcreteIdentifierU3Ek__BackingField_4(RuntimeObject * value)
	{
		___U3CConcreteIdentifierU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CConcreteIdentifierU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CMemberNameU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF, ___U3CMemberNameU3Ek__BackingField_5)); }
	inline String_t* get_U3CMemberNameU3Ek__BackingField_5() const { return ___U3CMemberNameU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CMemberNameU3Ek__BackingField_5() { return &___U3CMemberNameU3Ek__BackingField_5; }
	inline void set_U3CMemberNameU3Ek__BackingField_5(String_t* value)
	{
		___U3CMemberNameU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMemberNameU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CMemberTypeU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF, ___U3CMemberTypeU3Ek__BackingField_6)); }
	inline Type_t * get_U3CMemberTypeU3Ek__BackingField_6() const { return ___U3CMemberTypeU3Ek__BackingField_6; }
	inline Type_t ** get_address_of_U3CMemberTypeU3Ek__BackingField_6() { return &___U3CMemberTypeU3Ek__BackingField_6; }
	inline void set_U3CMemberTypeU3Ek__BackingField_6(Type_t * value)
	{
		___U3CMemberTypeU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMemberTypeU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3COptionalU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF, ___U3COptionalU3Ek__BackingField_7)); }
	inline bool get_U3COptionalU3Ek__BackingField_7() const { return ___U3COptionalU3Ek__BackingField_7; }
	inline bool* get_address_of_U3COptionalU3Ek__BackingField_7() { return &___U3COptionalU3Ek__BackingField_7; }
	inline void set_U3COptionalU3Ek__BackingField_7(bool value)
	{
		___U3COptionalU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CSourceTypeU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF, ___U3CSourceTypeU3Ek__BackingField_8)); }
	inline int32_t get_U3CSourceTypeU3Ek__BackingField_8() const { return ___U3CSourceTypeU3Ek__BackingField_8; }
	inline int32_t* get_address_of_U3CSourceTypeU3Ek__BackingField_8() { return &___U3CSourceTypeU3Ek__BackingField_8; }
	inline void set_U3CSourceTypeU3Ek__BackingField_8(int32_t value)
	{
		___U3CSourceTypeU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CFallBackValueU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF, ___U3CFallBackValueU3Ek__BackingField_9)); }
	inline RuntimeObject * get_U3CFallBackValueU3Ek__BackingField_9() const { return ___U3CFallBackValueU3Ek__BackingField_9; }
	inline RuntimeObject ** get_address_of_U3CFallBackValueU3Ek__BackingField_9() { return &___U3CFallBackValueU3Ek__BackingField_9; }
	inline void set_U3CFallBackValueU3Ek__BackingField_9(RuntimeObject * value)
	{
		___U3CFallBackValueU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFallBackValueU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3CContainerU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF, ___U3CContainerU3Ek__BackingField_10)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get_U3CContainerU3Ek__BackingField_10() const { return ___U3CContainerU3Ek__BackingField_10; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of_U3CContainerU3Ek__BackingField_10() { return &___U3CContainerU3Ek__BackingField_10; }
	inline void set_U3CContainerU3Ek__BackingField_10(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		___U3CContainerU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CContainerU3Ek__BackingField_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INJECTCONTEXT_TD1BDE94C49785956950BCFD5E1693537A7008FFF_H
#ifndef INJECTABLEINFO_T8B0167EA3F54639C2211BD1BEAF54D2C4063390E_H
#define INJECTABLEINFO_T8B0167EA3F54639C2211BD1BEAF54D2C4063390E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.InjectableInfo
struct  InjectableInfo_t8B0167EA3F54639C2211BD1BEAF54D2C4063390E  : public RuntimeObject
{
public:
	// System.Boolean Zenject.InjectableInfo::Optional
	bool ___Optional_0;
	// System.Object Zenject.InjectableInfo::Identifier
	RuntimeObject * ___Identifier_1;
	// Zenject.InjectSources Zenject.InjectableInfo::SourceType
	int32_t ___SourceType_2;
	// System.String Zenject.InjectableInfo::MemberName
	String_t* ___MemberName_3;
	// System.Type Zenject.InjectableInfo::MemberType
	Type_t * ___MemberType_4;
	// System.Type Zenject.InjectableInfo::ObjectType
	Type_t * ___ObjectType_5;
	// System.Action`2<System.Object,System.Object> Zenject.InjectableInfo::Setter
	Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C * ___Setter_6;
	// System.Object Zenject.InjectableInfo::DefaultValue
	RuntimeObject * ___DefaultValue_7;

public:
	inline static int32_t get_offset_of_Optional_0() { return static_cast<int32_t>(offsetof(InjectableInfo_t8B0167EA3F54639C2211BD1BEAF54D2C4063390E, ___Optional_0)); }
	inline bool get_Optional_0() const { return ___Optional_0; }
	inline bool* get_address_of_Optional_0() { return &___Optional_0; }
	inline void set_Optional_0(bool value)
	{
		___Optional_0 = value;
	}

	inline static int32_t get_offset_of_Identifier_1() { return static_cast<int32_t>(offsetof(InjectableInfo_t8B0167EA3F54639C2211BD1BEAF54D2C4063390E, ___Identifier_1)); }
	inline RuntimeObject * get_Identifier_1() const { return ___Identifier_1; }
	inline RuntimeObject ** get_address_of_Identifier_1() { return &___Identifier_1; }
	inline void set_Identifier_1(RuntimeObject * value)
	{
		___Identifier_1 = value;
		Il2CppCodeGenWriteBarrier((&___Identifier_1), value);
	}

	inline static int32_t get_offset_of_SourceType_2() { return static_cast<int32_t>(offsetof(InjectableInfo_t8B0167EA3F54639C2211BD1BEAF54D2C4063390E, ___SourceType_2)); }
	inline int32_t get_SourceType_2() const { return ___SourceType_2; }
	inline int32_t* get_address_of_SourceType_2() { return &___SourceType_2; }
	inline void set_SourceType_2(int32_t value)
	{
		___SourceType_2 = value;
	}

	inline static int32_t get_offset_of_MemberName_3() { return static_cast<int32_t>(offsetof(InjectableInfo_t8B0167EA3F54639C2211BD1BEAF54D2C4063390E, ___MemberName_3)); }
	inline String_t* get_MemberName_3() const { return ___MemberName_3; }
	inline String_t** get_address_of_MemberName_3() { return &___MemberName_3; }
	inline void set_MemberName_3(String_t* value)
	{
		___MemberName_3 = value;
		Il2CppCodeGenWriteBarrier((&___MemberName_3), value);
	}

	inline static int32_t get_offset_of_MemberType_4() { return static_cast<int32_t>(offsetof(InjectableInfo_t8B0167EA3F54639C2211BD1BEAF54D2C4063390E, ___MemberType_4)); }
	inline Type_t * get_MemberType_4() const { return ___MemberType_4; }
	inline Type_t ** get_address_of_MemberType_4() { return &___MemberType_4; }
	inline void set_MemberType_4(Type_t * value)
	{
		___MemberType_4 = value;
		Il2CppCodeGenWriteBarrier((&___MemberType_4), value);
	}

	inline static int32_t get_offset_of_ObjectType_5() { return static_cast<int32_t>(offsetof(InjectableInfo_t8B0167EA3F54639C2211BD1BEAF54D2C4063390E, ___ObjectType_5)); }
	inline Type_t * get_ObjectType_5() const { return ___ObjectType_5; }
	inline Type_t ** get_address_of_ObjectType_5() { return &___ObjectType_5; }
	inline void set_ObjectType_5(Type_t * value)
	{
		___ObjectType_5 = value;
		Il2CppCodeGenWriteBarrier((&___ObjectType_5), value);
	}

	inline static int32_t get_offset_of_Setter_6() { return static_cast<int32_t>(offsetof(InjectableInfo_t8B0167EA3F54639C2211BD1BEAF54D2C4063390E, ___Setter_6)); }
	inline Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C * get_Setter_6() const { return ___Setter_6; }
	inline Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C ** get_address_of_Setter_6() { return &___Setter_6; }
	inline void set_Setter_6(Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C * value)
	{
		___Setter_6 = value;
		Il2CppCodeGenWriteBarrier((&___Setter_6), value);
	}

	inline static int32_t get_offset_of_DefaultValue_7() { return static_cast<int32_t>(offsetof(InjectableInfo_t8B0167EA3F54639C2211BD1BEAF54D2C4063390E, ___DefaultValue_7)); }
	inline RuntimeObject * get_DefaultValue_7() const { return ___DefaultValue_7; }
	inline RuntimeObject ** get_address_of_DefaultValue_7() { return &___DefaultValue_7; }
	inline void set_DefaultValue_7(RuntimeObject * value)
	{
		___DefaultValue_7 = value;
		Il2CppCodeGenWriteBarrier((&___DefaultValue_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INJECTABLEINFO_T8B0167EA3F54639C2211BD1BEAF54D2C4063390E_H
#ifndef MEMORYPOOLSETTINGS_TC33180DF15EF5B1103FB5191CFC0569BB3C74AA5_H
#define MEMORYPOOLSETTINGS_TC33180DF15EF5B1103FB5191CFC0569BB3C74AA5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.MemoryPoolSettings
struct  MemoryPoolSettings_tC33180DF15EF5B1103FB5191CFC0569BB3C74AA5  : public RuntimeObject
{
public:
	// System.Int32 Zenject.MemoryPoolSettings::InitialSize
	int32_t ___InitialSize_0;
	// Zenject.PoolExpandMethods Zenject.MemoryPoolSettings::ExpandMethod
	int32_t ___ExpandMethod_1;

public:
	inline static int32_t get_offset_of_InitialSize_0() { return static_cast<int32_t>(offsetof(MemoryPoolSettings_tC33180DF15EF5B1103FB5191CFC0569BB3C74AA5, ___InitialSize_0)); }
	inline int32_t get_InitialSize_0() const { return ___InitialSize_0; }
	inline int32_t* get_address_of_InitialSize_0() { return &___InitialSize_0; }
	inline void set_InitialSize_0(int32_t value)
	{
		___InitialSize_0 = value;
	}

	inline static int32_t get_offset_of_ExpandMethod_1() { return static_cast<int32_t>(offsetof(MemoryPoolSettings_tC33180DF15EF5B1103FB5191CFC0569BB3C74AA5, ___ExpandMethod_1)); }
	inline int32_t get_ExpandMethod_1() const { return ___ExpandMethod_1; }
	inline int32_t* get_address_of_ExpandMethod_1() { return &___ExpandMethod_1; }
	inline void set_ExpandMethod_1(int32_t value)
	{
		___ExpandMethod_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMORYPOOLSETTINGS_TC33180DF15EF5B1103FB5191CFC0569BB3C74AA5_H
#ifndef SCOPABLEBINDINGFINALIZER_T1278F9BE4EE396C7D8B5CA5625C540EC3582632C_H
#define SCOPABLEBINDINGFINALIZER_T1278F9BE4EE396C7D8B5CA5625C540EC3582632C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ScopableBindingFinalizer
struct  ScopableBindingFinalizer_t1278F9BE4EE396C7D8B5CA5625C540EC3582632C  : public ProviderBindingFinalizer_t3C2E6FBD89B98A057674B926F42EF13E4ED1EAAE
{
public:
	// Zenject.SingletonTypes Zenject.ScopableBindingFinalizer::_singletonType
	int32_t ____singletonType_1;
	// System.Func`3<Zenject.DiContainer,System.Type,Zenject.IProvider> Zenject.ScopableBindingFinalizer::_providerFactory
	Func_3_tF2BBB0DC92895D84974BAECD6D1B2AE7FFC65AAA * ____providerFactory_2;
	// System.Object Zenject.ScopableBindingFinalizer::_singletonSpecificId
	RuntimeObject * ____singletonSpecificId_3;

public:
	inline static int32_t get_offset_of__singletonType_1() { return static_cast<int32_t>(offsetof(ScopableBindingFinalizer_t1278F9BE4EE396C7D8B5CA5625C540EC3582632C, ____singletonType_1)); }
	inline int32_t get__singletonType_1() const { return ____singletonType_1; }
	inline int32_t* get_address_of__singletonType_1() { return &____singletonType_1; }
	inline void set__singletonType_1(int32_t value)
	{
		____singletonType_1 = value;
	}

	inline static int32_t get_offset_of__providerFactory_2() { return static_cast<int32_t>(offsetof(ScopableBindingFinalizer_t1278F9BE4EE396C7D8B5CA5625C540EC3582632C, ____providerFactory_2)); }
	inline Func_3_tF2BBB0DC92895D84974BAECD6D1B2AE7FFC65AAA * get__providerFactory_2() const { return ____providerFactory_2; }
	inline Func_3_tF2BBB0DC92895D84974BAECD6D1B2AE7FFC65AAA ** get_address_of__providerFactory_2() { return &____providerFactory_2; }
	inline void set__providerFactory_2(Func_3_tF2BBB0DC92895D84974BAECD6D1B2AE7FFC65AAA * value)
	{
		____providerFactory_2 = value;
		Il2CppCodeGenWriteBarrier((&____providerFactory_2), value);
	}

	inline static int32_t get_offset_of__singletonSpecificId_3() { return static_cast<int32_t>(offsetof(ScopableBindingFinalizer_t1278F9BE4EE396C7D8B5CA5625C540EC3582632C, ____singletonSpecificId_3)); }
	inline RuntimeObject * get__singletonSpecificId_3() const { return ____singletonSpecificId_3; }
	inline RuntimeObject ** get_address_of__singletonSpecificId_3() { return &____singletonSpecificId_3; }
	inline void set__singletonSpecificId_3(RuntimeObject * value)
	{
		____singletonSpecificId_3 = value;
		Il2CppCodeGenWriteBarrier((&____singletonSpecificId_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCOPABLEBINDINGFINALIZER_T1278F9BE4EE396C7D8B5CA5625C540EC3582632C_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef CONTEXT_T04459FF3A5B7E8AF25B965F5717F535AE8FA6DBA_H
#define CONTEXT_T04459FF3A5B7E8AF25B965F5717F535AE8FA6DBA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.Context
struct  Context_t04459FF3A5B7E8AF25B965F5717F535AE8FA6DBA  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Collections.Generic.List`1<Zenject.MonoInstaller> Zenject.Context::_installers
	List_1_tCF773BC8FB84889055B7C7EDDE5C5D83FA2D4F08 * ____installers_4;
	// System.Collections.Generic.List`1<Zenject.MonoInstaller> Zenject.Context::_installerPrefabs
	List_1_tCF773BC8FB84889055B7C7EDDE5C5D83FA2D4F08 * ____installerPrefabs_5;
	// System.Collections.Generic.List`1<Zenject.ScriptableObjectInstaller> Zenject.Context::_scriptableObjectInstallers
	List_1_t8B49C95109DF1CE321982E768E438471621D0733 * ____scriptableObjectInstallers_6;
	// System.Collections.Generic.List`1<Zenject.InstallerBase> Zenject.Context::_normalInstallers
	List_1_t0579372F26330F2A4603BEBDDF54A2BF6791CF8B * ____normalInstallers_7;
	// System.Collections.Generic.List`1<System.Type> Zenject.Context::_normalInstallerTypes
	List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0 * ____normalInstallerTypes_8;

public:
	inline static int32_t get_offset_of__installers_4() { return static_cast<int32_t>(offsetof(Context_t04459FF3A5B7E8AF25B965F5717F535AE8FA6DBA, ____installers_4)); }
	inline List_1_tCF773BC8FB84889055B7C7EDDE5C5D83FA2D4F08 * get__installers_4() const { return ____installers_4; }
	inline List_1_tCF773BC8FB84889055B7C7EDDE5C5D83FA2D4F08 ** get_address_of__installers_4() { return &____installers_4; }
	inline void set__installers_4(List_1_tCF773BC8FB84889055B7C7EDDE5C5D83FA2D4F08 * value)
	{
		____installers_4 = value;
		Il2CppCodeGenWriteBarrier((&____installers_4), value);
	}

	inline static int32_t get_offset_of__installerPrefabs_5() { return static_cast<int32_t>(offsetof(Context_t04459FF3A5B7E8AF25B965F5717F535AE8FA6DBA, ____installerPrefabs_5)); }
	inline List_1_tCF773BC8FB84889055B7C7EDDE5C5D83FA2D4F08 * get__installerPrefabs_5() const { return ____installerPrefabs_5; }
	inline List_1_tCF773BC8FB84889055B7C7EDDE5C5D83FA2D4F08 ** get_address_of__installerPrefabs_5() { return &____installerPrefabs_5; }
	inline void set__installerPrefabs_5(List_1_tCF773BC8FB84889055B7C7EDDE5C5D83FA2D4F08 * value)
	{
		____installerPrefabs_5 = value;
		Il2CppCodeGenWriteBarrier((&____installerPrefabs_5), value);
	}

	inline static int32_t get_offset_of__scriptableObjectInstallers_6() { return static_cast<int32_t>(offsetof(Context_t04459FF3A5B7E8AF25B965F5717F535AE8FA6DBA, ____scriptableObjectInstallers_6)); }
	inline List_1_t8B49C95109DF1CE321982E768E438471621D0733 * get__scriptableObjectInstallers_6() const { return ____scriptableObjectInstallers_6; }
	inline List_1_t8B49C95109DF1CE321982E768E438471621D0733 ** get_address_of__scriptableObjectInstallers_6() { return &____scriptableObjectInstallers_6; }
	inline void set__scriptableObjectInstallers_6(List_1_t8B49C95109DF1CE321982E768E438471621D0733 * value)
	{
		____scriptableObjectInstallers_6 = value;
		Il2CppCodeGenWriteBarrier((&____scriptableObjectInstallers_6), value);
	}

	inline static int32_t get_offset_of__normalInstallers_7() { return static_cast<int32_t>(offsetof(Context_t04459FF3A5B7E8AF25B965F5717F535AE8FA6DBA, ____normalInstallers_7)); }
	inline List_1_t0579372F26330F2A4603BEBDDF54A2BF6791CF8B * get__normalInstallers_7() const { return ____normalInstallers_7; }
	inline List_1_t0579372F26330F2A4603BEBDDF54A2BF6791CF8B ** get_address_of__normalInstallers_7() { return &____normalInstallers_7; }
	inline void set__normalInstallers_7(List_1_t0579372F26330F2A4603BEBDDF54A2BF6791CF8B * value)
	{
		____normalInstallers_7 = value;
		Il2CppCodeGenWriteBarrier((&____normalInstallers_7), value);
	}

	inline static int32_t get_offset_of__normalInstallerTypes_8() { return static_cast<int32_t>(offsetof(Context_t04459FF3A5B7E8AF25B965F5717F535AE8FA6DBA, ____normalInstallerTypes_8)); }
	inline List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0 * get__normalInstallerTypes_8() const { return ____normalInstallerTypes_8; }
	inline List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0 ** get_address_of__normalInstallerTypes_8() { return &____normalInstallerTypes_8; }
	inline void set__normalInstallerTypes_8(List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0 * value)
	{
		____normalInstallerTypes_8 = value;
		Il2CppCodeGenWriteBarrier((&____normalInstallerTypes_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTEXT_T04459FF3A5B7E8AF25B965F5717F535AE8FA6DBA_H
#ifndef PROJECTCONTEXT_TC25F8B4277654D0C24D603FD01EF5D6A7F450270_H
#define PROJECTCONTEXT_TC25F8B4277654D0C24D603FD01EF5D6A7F450270_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ProjectContext
struct  ProjectContext_tC25F8B4277654D0C24D603FD01EF5D6A7F450270  : public Context_t04459FF3A5B7E8AF25B965F5717F535AE8FA6DBA
{
public:
	// Zenject.DiContainer Zenject.ProjectContext::_container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____container_12;

public:
	inline static int32_t get_offset_of__container_12() { return static_cast<int32_t>(offsetof(ProjectContext_tC25F8B4277654D0C24D603FD01EF5D6A7F450270, ____container_12)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__container_12() const { return ____container_12; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__container_12() { return &____container_12; }
	inline void set__container_12(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____container_12 = value;
		Il2CppCodeGenWriteBarrier((&____container_12), value);
	}
};

struct ProjectContext_tC25F8B4277654D0C24D603FD01EF5D6A7F450270_StaticFields
{
public:
	// Zenject.ProjectContext Zenject.ProjectContext::_instance
	ProjectContext_tC25F8B4277654D0C24D603FD01EF5D6A7F450270 * ____instance_11;

public:
	inline static int32_t get_offset_of__instance_11() { return static_cast<int32_t>(offsetof(ProjectContext_tC25F8B4277654D0C24D603FD01EF5D6A7F450270_StaticFields, ____instance_11)); }
	inline ProjectContext_tC25F8B4277654D0C24D603FD01EF5D6A7F450270 * get__instance_11() const { return ____instance_11; }
	inline ProjectContext_tC25F8B4277654D0C24D603FD01EF5D6A7F450270 ** get_address_of__instance_11() { return &____instance_11; }
	inline void set__instance_11(ProjectContext_tC25F8B4277654D0C24D603FD01EF5D6A7F450270 * value)
	{
		____instance_11 = value;
		Il2CppCodeGenWriteBarrier((&____instance_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROJECTCONTEXT_TC25F8B4277654D0C24D603FD01EF5D6A7F450270_H
#ifndef RUNNABLECONTEXT_TE38018DE42EEBD1F6511E890CE2066A44C6FDBAA_H
#define RUNNABLECONTEXT_TE38018DE42EEBD1F6511E890CE2066A44C6FDBAA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.RunnableContext
struct  RunnableContext_tE38018DE42EEBD1F6511E890CE2066A44C6FDBAA  : public Context_t04459FF3A5B7E8AF25B965F5717F535AE8FA6DBA
{
public:
	// System.Boolean Zenject.RunnableContext::_autoRun
	bool ____autoRun_9;
	// System.Boolean Zenject.RunnableContext::<Initialized>k__BackingField
	bool ___U3CInitializedU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of__autoRun_9() { return static_cast<int32_t>(offsetof(RunnableContext_tE38018DE42EEBD1F6511E890CE2066A44C6FDBAA, ____autoRun_9)); }
	inline bool get__autoRun_9() const { return ____autoRun_9; }
	inline bool* get_address_of__autoRun_9() { return &____autoRun_9; }
	inline void set__autoRun_9(bool value)
	{
		____autoRun_9 = value;
	}

	inline static int32_t get_offset_of_U3CInitializedU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(RunnableContext_tE38018DE42EEBD1F6511E890CE2066A44C6FDBAA, ___U3CInitializedU3Ek__BackingField_11)); }
	inline bool get_U3CInitializedU3Ek__BackingField_11() const { return ___U3CInitializedU3Ek__BackingField_11; }
	inline bool* get_address_of_U3CInitializedU3Ek__BackingField_11() { return &___U3CInitializedU3Ek__BackingField_11; }
	inline void set_U3CInitializedU3Ek__BackingField_11(bool value)
	{
		___U3CInitializedU3Ek__BackingField_11 = value;
	}
};

struct RunnableContext_tE38018DE42EEBD1F6511E890CE2066A44C6FDBAA_StaticFields
{
public:
	// System.Boolean Zenject.RunnableContext::_staticAutoRun
	bool ____staticAutoRun_10;

public:
	inline static int32_t get_offset_of__staticAutoRun_10() { return static_cast<int32_t>(offsetof(RunnableContext_tE38018DE42EEBD1F6511E890CE2066A44C6FDBAA_StaticFields, ____staticAutoRun_10)); }
	inline bool get__staticAutoRun_10() const { return ____staticAutoRun_10; }
	inline bool* get_address_of__staticAutoRun_10() { return &____staticAutoRun_10; }
	inline void set__staticAutoRun_10(bool value)
	{
		____staticAutoRun_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNNABLECONTEXT_TE38018DE42EEBD1F6511E890CE2066A44C6FDBAA_H
#ifndef GAMEOBJECTCONTEXT_TFE72D7164DF22B1604E0C36954AEC6708654CB1E_H
#define GAMEOBJECTCONTEXT_TFE72D7164DF22B1604E0C36954AEC6708654CB1E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.GameObjectContext
struct  GameObjectContext_tFE72D7164DF22B1604E0C36954AEC6708654CB1E  : public RunnableContext_tE38018DE42EEBD1F6511E890CE2066A44C6FDBAA
{
public:
	// Zenject.MonoKernel Zenject.GameObjectContext::_kernel
	MonoKernel_t297C7FA263D67830E721CBF0F9C909FAFB569412 * ____kernel_12;
	// Zenject.DiContainer Zenject.GameObjectContext::_container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____container_13;

public:
	inline static int32_t get_offset_of__kernel_12() { return static_cast<int32_t>(offsetof(GameObjectContext_tFE72D7164DF22B1604E0C36954AEC6708654CB1E, ____kernel_12)); }
	inline MonoKernel_t297C7FA263D67830E721CBF0F9C909FAFB569412 * get__kernel_12() const { return ____kernel_12; }
	inline MonoKernel_t297C7FA263D67830E721CBF0F9C909FAFB569412 ** get_address_of__kernel_12() { return &____kernel_12; }
	inline void set__kernel_12(MonoKernel_t297C7FA263D67830E721CBF0F9C909FAFB569412 * value)
	{
		____kernel_12 = value;
		Il2CppCodeGenWriteBarrier((&____kernel_12), value);
	}

	inline static int32_t get_offset_of__container_13() { return static_cast<int32_t>(offsetof(GameObjectContext_tFE72D7164DF22B1604E0C36954AEC6708654CB1E, ____container_13)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__container_13() const { return ____container_13; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__container_13() { return &____container_13; }
	inline void set__container_13(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____container_13 = value;
		Il2CppCodeGenWriteBarrier((&____container_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECTCONTEXT_TFE72D7164DF22B1604E0C36954AEC6708654CB1E_H
#ifndef SCENECONTEXT_TA69C18F583C410BB2DA86B5C1F86F27459DA8CA2_H
#define SCENECONTEXT_TA69C18F583C410BB2DA86B5C1F86F27459DA8CA2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SceneContext
struct  SceneContext_tA69C18F583C410BB2DA86B5C1F86F27459DA8CA2  : public RunnableContext_tE38018DE42EEBD1F6511E890CE2066A44C6FDBAA
{
public:
	// System.Boolean Zenject.SceneContext::_parentNewObjectsUnderRoot
	bool ____parentNewObjectsUnderRoot_15;
	// System.Collections.Generic.List`1<System.String> Zenject.SceneContext::_contractNames
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ____contractNames_16;
	// System.String Zenject.SceneContext::_parentContractName
	String_t* ____parentContractName_17;
	// System.Collections.Generic.List`1<System.String> Zenject.SceneContext::_parentContractNames
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ____parentContractNames_18;
	// Zenject.DiContainer Zenject.SceneContext::_container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____container_19;
	// System.Collections.Generic.List`1<Zenject.SceneDecoratorContext> Zenject.SceneContext::_decoratorContexts
	List_1_t3341302071D2A75F070E9984D7E1F72328EFE65B * ____decoratorContexts_20;
	// System.Boolean Zenject.SceneContext::_hasInstalled
	bool ____hasInstalled_21;
	// System.Boolean Zenject.SceneContext::_hasResolved
	bool ____hasResolved_22;

public:
	inline static int32_t get_offset_of__parentNewObjectsUnderRoot_15() { return static_cast<int32_t>(offsetof(SceneContext_tA69C18F583C410BB2DA86B5C1F86F27459DA8CA2, ____parentNewObjectsUnderRoot_15)); }
	inline bool get__parentNewObjectsUnderRoot_15() const { return ____parentNewObjectsUnderRoot_15; }
	inline bool* get_address_of__parentNewObjectsUnderRoot_15() { return &____parentNewObjectsUnderRoot_15; }
	inline void set__parentNewObjectsUnderRoot_15(bool value)
	{
		____parentNewObjectsUnderRoot_15 = value;
	}

	inline static int32_t get_offset_of__contractNames_16() { return static_cast<int32_t>(offsetof(SceneContext_tA69C18F583C410BB2DA86B5C1F86F27459DA8CA2, ____contractNames_16)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get__contractNames_16() const { return ____contractNames_16; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of__contractNames_16() { return &____contractNames_16; }
	inline void set__contractNames_16(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		____contractNames_16 = value;
		Il2CppCodeGenWriteBarrier((&____contractNames_16), value);
	}

	inline static int32_t get_offset_of__parentContractName_17() { return static_cast<int32_t>(offsetof(SceneContext_tA69C18F583C410BB2DA86B5C1F86F27459DA8CA2, ____parentContractName_17)); }
	inline String_t* get__parentContractName_17() const { return ____parentContractName_17; }
	inline String_t** get_address_of__parentContractName_17() { return &____parentContractName_17; }
	inline void set__parentContractName_17(String_t* value)
	{
		____parentContractName_17 = value;
		Il2CppCodeGenWriteBarrier((&____parentContractName_17), value);
	}

	inline static int32_t get_offset_of__parentContractNames_18() { return static_cast<int32_t>(offsetof(SceneContext_tA69C18F583C410BB2DA86B5C1F86F27459DA8CA2, ____parentContractNames_18)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get__parentContractNames_18() const { return ____parentContractNames_18; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of__parentContractNames_18() { return &____parentContractNames_18; }
	inline void set__parentContractNames_18(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		____parentContractNames_18 = value;
		Il2CppCodeGenWriteBarrier((&____parentContractNames_18), value);
	}

	inline static int32_t get_offset_of__container_19() { return static_cast<int32_t>(offsetof(SceneContext_tA69C18F583C410BB2DA86B5C1F86F27459DA8CA2, ____container_19)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__container_19() const { return ____container_19; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__container_19() { return &____container_19; }
	inline void set__container_19(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____container_19 = value;
		Il2CppCodeGenWriteBarrier((&____container_19), value);
	}

	inline static int32_t get_offset_of__decoratorContexts_20() { return static_cast<int32_t>(offsetof(SceneContext_tA69C18F583C410BB2DA86B5C1F86F27459DA8CA2, ____decoratorContexts_20)); }
	inline List_1_t3341302071D2A75F070E9984D7E1F72328EFE65B * get__decoratorContexts_20() const { return ____decoratorContexts_20; }
	inline List_1_t3341302071D2A75F070E9984D7E1F72328EFE65B ** get_address_of__decoratorContexts_20() { return &____decoratorContexts_20; }
	inline void set__decoratorContexts_20(List_1_t3341302071D2A75F070E9984D7E1F72328EFE65B * value)
	{
		____decoratorContexts_20 = value;
		Il2CppCodeGenWriteBarrier((&____decoratorContexts_20), value);
	}

	inline static int32_t get_offset_of__hasInstalled_21() { return static_cast<int32_t>(offsetof(SceneContext_tA69C18F583C410BB2DA86B5C1F86F27459DA8CA2, ____hasInstalled_21)); }
	inline bool get__hasInstalled_21() const { return ____hasInstalled_21; }
	inline bool* get_address_of__hasInstalled_21() { return &____hasInstalled_21; }
	inline void set__hasInstalled_21(bool value)
	{
		____hasInstalled_21 = value;
	}

	inline static int32_t get_offset_of__hasResolved_22() { return static_cast<int32_t>(offsetof(SceneContext_tA69C18F583C410BB2DA86B5C1F86F27459DA8CA2, ____hasResolved_22)); }
	inline bool get__hasResolved_22() const { return ____hasResolved_22; }
	inline bool* get_address_of__hasResolved_22() { return &____hasResolved_22; }
	inline void set__hasResolved_22(bool value)
	{
		____hasResolved_22 = value;
	}
};

struct SceneContext_tA69C18F583C410BB2DA86B5C1F86F27459DA8CA2_StaticFields
{
public:
	// System.Action`1<Zenject.DiContainer> Zenject.SceneContext::ExtraBindingsInstallMethod
	Action_1_t47FB75EB7E275E7EC20C1ADC32AC936BD0BD4E84 * ___ExtraBindingsInstallMethod_12;
	// System.Action`1<Zenject.DiContainer> Zenject.SceneContext::ExtraBindingsLateInstallMethod
	Action_1_t47FB75EB7E275E7EC20C1ADC32AC936BD0BD4E84 * ___ExtraBindingsLateInstallMethod_13;
	// System.Collections.Generic.IEnumerable`1<Zenject.DiContainer> Zenject.SceneContext::ParentContainers
	RuntimeObject* ___ParentContainers_14;

public:
	inline static int32_t get_offset_of_ExtraBindingsInstallMethod_12() { return static_cast<int32_t>(offsetof(SceneContext_tA69C18F583C410BB2DA86B5C1F86F27459DA8CA2_StaticFields, ___ExtraBindingsInstallMethod_12)); }
	inline Action_1_t47FB75EB7E275E7EC20C1ADC32AC936BD0BD4E84 * get_ExtraBindingsInstallMethod_12() const { return ___ExtraBindingsInstallMethod_12; }
	inline Action_1_t47FB75EB7E275E7EC20C1ADC32AC936BD0BD4E84 ** get_address_of_ExtraBindingsInstallMethod_12() { return &___ExtraBindingsInstallMethod_12; }
	inline void set_ExtraBindingsInstallMethod_12(Action_1_t47FB75EB7E275E7EC20C1ADC32AC936BD0BD4E84 * value)
	{
		___ExtraBindingsInstallMethod_12 = value;
		Il2CppCodeGenWriteBarrier((&___ExtraBindingsInstallMethod_12), value);
	}

	inline static int32_t get_offset_of_ExtraBindingsLateInstallMethod_13() { return static_cast<int32_t>(offsetof(SceneContext_tA69C18F583C410BB2DA86B5C1F86F27459DA8CA2_StaticFields, ___ExtraBindingsLateInstallMethod_13)); }
	inline Action_1_t47FB75EB7E275E7EC20C1ADC32AC936BD0BD4E84 * get_ExtraBindingsLateInstallMethod_13() const { return ___ExtraBindingsLateInstallMethod_13; }
	inline Action_1_t47FB75EB7E275E7EC20C1ADC32AC936BD0BD4E84 ** get_address_of_ExtraBindingsLateInstallMethod_13() { return &___ExtraBindingsLateInstallMethod_13; }
	inline void set_ExtraBindingsLateInstallMethod_13(Action_1_t47FB75EB7E275E7EC20C1ADC32AC936BD0BD4E84 * value)
	{
		___ExtraBindingsLateInstallMethod_13 = value;
		Il2CppCodeGenWriteBarrier((&___ExtraBindingsLateInstallMethod_13), value);
	}

	inline static int32_t get_offset_of_ParentContainers_14() { return static_cast<int32_t>(offsetof(SceneContext_tA69C18F583C410BB2DA86B5C1F86F27459DA8CA2_StaticFields, ___ParentContainers_14)); }
	inline RuntimeObject* get_ParentContainers_14() const { return ___ParentContainers_14; }
	inline RuntimeObject** get_address_of_ParentContainers_14() { return &___ParentContainers_14; }
	inline void set_ParentContainers_14(RuntimeObject* value)
	{
		___ParentContainers_14 = value;
		Il2CppCodeGenWriteBarrier((&___ParentContainers_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENECONTEXT_TA69C18F583C410BB2DA86B5C1F86F27459DA8CA2_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7200 = { sizeof (U3CU3Ec_tC6B573730C45D20BE802AC70FD90DA2F39667BC1), -1, sizeof(U3CU3Ec_tC6B573730C45D20BE802AC70FD90DA2F39667BC1_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7200[3] = 
{
	U3CU3Ec_tC6B573730C45D20BE802AC70FD90DA2F39667BC1_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tC6B573730C45D20BE802AC70FD90DA2F39667BC1_StaticFields::get_offset_of_U3CU3E9__7_0_1(),
	U3CU3Ec_tC6B573730C45D20BE802AC70FD90DA2F39667BC1_StaticFields::get_offset_of_U3CU3E9__16_0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7201 = { sizeof (U3CU3Ec__DisplayClass16_0_t793513EA7BB7C1E3E34717EFA6502F85704AE2B6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7201[2] = 
{
	U3CU3Ec__DisplayClass16_0_t793513EA7BB7C1E3E34717EFA6502F85704AE2B6::get_offset_of_providerFunc_0(),
	U3CU3Ec__DisplayClass16_0_t793513EA7BB7C1E3E34717EFA6502F85704AE2B6::get_offset_of_container_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7202 = { sizeof (ScopableBindingFinalizer_t1278F9BE4EE396C7D8B5CA5625C540EC3582632C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7202[3] = 
{
	ScopableBindingFinalizer_t1278F9BE4EE396C7D8B5CA5625C540EC3582632C::get_offset_of__singletonType_1(),
	ScopableBindingFinalizer_t1278F9BE4EE396C7D8B5CA5625C540EC3582632C::get_offset_of__providerFactory_2(),
	ScopableBindingFinalizer_t1278F9BE4EE396C7D8B5CA5625C540EC3582632C::get_offset_of__singletonSpecificId_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7203 = { sizeof (U3CU3Ec__DisplayClass5_0_t7E3ECC94D523D36C05D1BB6635F0BAA0D5C19BB7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7203[2] = 
{
	U3CU3Ec__DisplayClass5_0_t7E3ECC94D523D36C05D1BB6635F0BAA0D5C19BB7::get_offset_of_container_0(),
	U3CU3Ec__DisplayClass5_0_t7E3ECC94D523D36C05D1BB6635F0BAA0D5C19BB7::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7204 = { sizeof (U3CU3Ec__DisplayClass6_0_t00BC48A39C1F7CA3867EDE947A14D53639A1C439), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7204[2] = 
{
	U3CU3Ec__DisplayClass6_0_t00BC48A39C1F7CA3867EDE947A14D53639A1C439::get_offset_of_container_0(),
	U3CU3Ec__DisplayClass6_0_t00BC48A39C1F7CA3867EDE947A14D53639A1C439::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7205 = { sizeof (SingleProviderBindingFinalizer_tF4B4915E023EC3754F27FCDD815D93844002C5BB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7205[1] = 
{
	SingleProviderBindingFinalizer_tF4B4915E023EC3754F27FCDD815D93844002C5BB::get_offset_of__providerFactory_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7206 = { sizeof (SubContainerInstallerBindingFinalizer_t7E7503CA806862CA81C5A2A86E199B15532DC82E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7206[2] = 
{
	SubContainerInstallerBindingFinalizer_t7E7503CA806862CA81C5A2A86E199B15532DC82E::get_offset_of__subIdentifier_1(),
	SubContainerInstallerBindingFinalizer_t7E7503CA806862CA81C5A2A86E199B15532DC82E::get_offset_of__installerType_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7207 = { sizeof (U3CU3Ec__DisplayClass5_0_t64DDCD52B9790DA5BB3DF411F4F3726FD18DA5B1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7207[2] = 
{
	U3CU3Ec__DisplayClass5_0_t64DDCD52B9790DA5BB3DF411F4F3726FD18DA5B1::get_offset_of_container_0(),
	U3CU3Ec__DisplayClass5_0_t64DDCD52B9790DA5BB3DF411F4F3726FD18DA5B1::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7208 = { sizeof (U3CU3Ec__DisplayClass5_1_tACDA7E6A2C16247A0B943DA9B6379EC9163AAC02), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7208[2] = 
{
	U3CU3Ec__DisplayClass5_1_tACDA7E6A2C16247A0B943DA9B6379EC9163AAC02::get_offset_of_containerCreator_0(),
	U3CU3Ec__DisplayClass5_1_tACDA7E6A2C16247A0B943DA9B6379EC9163AAC02::get_offset_of_CSU24U3CU3E8__locals1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7209 = { sizeof (U3CU3Ec__DisplayClass6_0_t2FC5487E39FC7D6C3B76178185AD1118043CB391), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7209[2] = 
{
	U3CU3Ec__DisplayClass6_0_t2FC5487E39FC7D6C3B76178185AD1118043CB391::get_offset_of_container_0(),
	U3CU3Ec__DisplayClass6_0_t2FC5487E39FC7D6C3B76178185AD1118043CB391::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7210 = { sizeof (U3CU3Ec__DisplayClass6_1_t1236B8429368F99007623955034CA74B654BB03A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7210[2] = 
{
	U3CU3Ec__DisplayClass6_1_t1236B8429368F99007623955034CA74B654BB03A::get_offset_of_containerCreator_0(),
	U3CU3Ec__DisplayClass6_1_t1236B8429368F99007623955034CA74B654BB03A::get_offset_of_CSU24U3CU3E8__locals1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7211 = { sizeof (SubContainerMethodBindingFinalizer_tCC3590F54EDCD60ECB9CC8F1D324DB07191CEB31), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7211[2] = 
{
	SubContainerMethodBindingFinalizer_tCC3590F54EDCD60ECB9CC8F1D324DB07191CEB31::get_offset_of__subIdentifier_1(),
	SubContainerMethodBindingFinalizer_tCC3590F54EDCD60ECB9CC8F1D324DB07191CEB31::get_offset_of__installMethod_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7212 = { sizeof (U3CU3Ec__DisplayClass4_0_t3528FF845AC546F07044CFEFE66CFDCBB5899B69), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7212[2] = 
{
	U3CU3Ec__DisplayClass4_0_t3528FF845AC546F07044CFEFE66CFDCBB5899B69::get_offset_of_container_0(),
	U3CU3Ec__DisplayClass4_0_t3528FF845AC546F07044CFEFE66CFDCBB5899B69::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7213 = { sizeof (U3CU3Ec__DisplayClass4_1_t1F379E2C0C54A977ADE5AA93660177E4D3073C59), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7213[2] = 
{
	U3CU3Ec__DisplayClass4_1_t1F379E2C0C54A977ADE5AA93660177E4D3073C59::get_offset_of_creator_0(),
	U3CU3Ec__DisplayClass4_1_t1F379E2C0C54A977ADE5AA93660177E4D3073C59::get_offset_of_CSU24U3CU3E8__locals1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7214 = { sizeof (U3CU3Ec__DisplayClass5_0_t7988CB076649CF11F3F91091ECAEEA8E2BD0126A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7214[2] = 
{
	U3CU3Ec__DisplayClass5_0_t7988CB076649CF11F3F91091ECAEEA8E2BD0126A::get_offset_of_container_0(),
	U3CU3Ec__DisplayClass5_0_t7988CB076649CF11F3F91091ECAEEA8E2BD0126A::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7215 = { sizeof (U3CU3Ec__DisplayClass5_1_t54FF71ACE296A718878232C08591AAD0513AA4D5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7215[2] = 
{
	U3CU3Ec__DisplayClass5_1_t54FF71ACE296A718878232C08591AAD0513AA4D5::get_offset_of_containerCreator_0(),
	U3CU3Ec__DisplayClass5_1_t54FF71ACE296A718878232C08591AAD0513AA4D5::get_offset_of_CSU24U3CU3E8__locals1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7216 = { sizeof (SubContainerPrefabBindingFinalizer_t909CFA4349A1C6B422478412C874D33CFE229618), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7216[3] = 
{
	SubContainerPrefabBindingFinalizer_t909CFA4349A1C6B422478412C874D33CFE229618::get_offset_of__prefab_1(),
	SubContainerPrefabBindingFinalizer_t909CFA4349A1C6B422478412C874D33CFE229618::get_offset_of__subIdentifier_2(),
	SubContainerPrefabBindingFinalizer_t909CFA4349A1C6B422478412C874D33CFE229618::get_offset_of__gameObjectBindInfo_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7217 = { sizeof (U3CU3Ec__DisplayClass5_0_t9F9EF9AD88205286601A08217B9F885C6CB7FFAE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7217[2] = 
{
	U3CU3Ec__DisplayClass5_0_t9F9EF9AD88205286601A08217B9F885C6CB7FFAE::get_offset_of_container_0(),
	U3CU3Ec__DisplayClass5_0_t9F9EF9AD88205286601A08217B9F885C6CB7FFAE::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7218 = { sizeof (U3CU3Ec__DisplayClass5_1_t14D58B66D54B64167F80F9304E4B19CB2E83A431), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7218[2] = 
{
	U3CU3Ec__DisplayClass5_1_t14D58B66D54B64167F80F9304E4B19CB2E83A431::get_offset_of_containerCreator_0(),
	U3CU3Ec__DisplayClass5_1_t14D58B66D54B64167F80F9304E4B19CB2E83A431::get_offset_of_CSU24U3CU3E8__locals1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7219 = { sizeof (U3CU3Ec__DisplayClass6_0_t303E2EE042B2111EC378D3D496EA2074D6A83847), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7219[2] = 
{
	U3CU3Ec__DisplayClass6_0_t303E2EE042B2111EC378D3D496EA2074D6A83847::get_offset_of_container_0(),
	U3CU3Ec__DisplayClass6_0_t303E2EE042B2111EC378D3D496EA2074D6A83847::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7220 = { sizeof (U3CU3Ec__DisplayClass6_1_tEAB5325C21578C9D69B1DDACFA38A9751CB6A4E4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7220[2] = 
{
	U3CU3Ec__DisplayClass6_1_tEAB5325C21578C9D69B1DDACFA38A9751CB6A4E4::get_offset_of_containerCreator_0(),
	U3CU3Ec__DisplayClass6_1_tEAB5325C21578C9D69B1DDACFA38A9751CB6A4E4::get_offset_of_CSU24U3CU3E8__locals1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7221 = { sizeof (SubContainerPrefabResourceBindingFinalizer_t25917592BD3C28B58C7F81D40A8F7D2FABD0E1CC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7221[3] = 
{
	SubContainerPrefabResourceBindingFinalizer_t25917592BD3C28B58C7F81D40A8F7D2FABD0E1CC::get_offset_of__resourcePath_1(),
	SubContainerPrefabResourceBindingFinalizer_t25917592BD3C28B58C7F81D40A8F7D2FABD0E1CC::get_offset_of__subIdentifier_2(),
	SubContainerPrefabResourceBindingFinalizer_t25917592BD3C28B58C7F81D40A8F7D2FABD0E1CC::get_offset_of__gameObjectBindInfo_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7222 = { sizeof (U3CU3Ec__DisplayClass5_0_tCC18CA0B7418964E47FFC397F2B05D1C5D411977), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7222[2] = 
{
	U3CU3Ec__DisplayClass5_0_tCC18CA0B7418964E47FFC397F2B05D1C5D411977::get_offset_of_container_0(),
	U3CU3Ec__DisplayClass5_0_tCC18CA0B7418964E47FFC397F2B05D1C5D411977::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7223 = { sizeof (U3CU3Ec__DisplayClass5_1_tAC4960C5B15B3D4E694F1A346E3B9A1D17D5EAF5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7223[2] = 
{
	U3CU3Ec__DisplayClass5_1_tAC4960C5B15B3D4E694F1A346E3B9A1D17D5EAF5::get_offset_of_containerCreator_0(),
	U3CU3Ec__DisplayClass5_1_tAC4960C5B15B3D4E694F1A346E3B9A1D17D5EAF5::get_offset_of_CSU24U3CU3E8__locals1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7224 = { sizeof (U3CU3Ec__DisplayClass6_0_t29364056A3133169CB10C6EC6C948A84D0F3C4B2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7224[2] = 
{
	U3CU3Ec__DisplayClass6_0_t29364056A3133169CB10C6EC6C948A84D0F3C4B2::get_offset_of_container_0(),
	U3CU3Ec__DisplayClass6_0_t29364056A3133169CB10C6EC6C948A84D0F3C4B2::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7225 = { sizeof (U3CU3Ec__DisplayClass6_1_tB080A1DD076310B54FFD7002DFBD5EDAC7843BBF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7225[2] = 
{
	U3CU3Ec__DisplayClass6_1_tB080A1DD076310B54FFD7002DFBD5EDAC7843BBF::get_offset_of_containerCreator_0(),
	U3CU3Ec__DisplayClass6_1_tB080A1DD076310B54FFD7002DFBD5EDAC7843BBF::get_offset_of_CSU24U3CU3E8__locals1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7226 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7227 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7227[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7228 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7229 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7229[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7230 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7231 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7231[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7232 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7233 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7233[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7234 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7235 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7235[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7236 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7237 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7237[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7238 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7238[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7239 = { sizeof (GameObjectFactory_t9D51611DDE961B006FF2D9E5C5F857EB47C2E596), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7239[2] = 
{
	GameObjectFactory_t9D51611DDE961B006FF2D9E5C5F857EB47C2E596::get_offset_of__container_0(),
	GameObjectFactory_t9D51611DDE961B006FF2D9E5C5F857EB47C2E596::get_offset_of__prefab_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7240 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7241 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7242 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7243 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7244 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7245 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7246 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7247 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7247[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7248 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7248[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7249 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7250 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7251 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7252 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7253 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7254 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7255 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7255[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7256 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7257 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7258 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7259 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7260 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7261 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7262 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7263 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7264 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7265 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7266 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7267 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7268 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7269 = { sizeof (PoolExceededFixedSizeException_t0B1C487A895130032B04F4CE17810CDF192B9012), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7270 = { sizeof (MemoryPoolSettings_tC33180DF15EF5B1103FB5191CFC0569BB3C74AA5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7270[2] = 
{
	MemoryPoolSettings_tC33180DF15EF5B1103FB5191CFC0569BB3C74AA5::get_offset_of_InitialSize_0(),
	MemoryPoolSettings_tC33180DF15EF5B1103FB5191CFC0569BB3C74AA5::get_offset_of_ExpandMethod_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7271 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7271[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7272 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7273 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7274 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7275 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7276 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7277 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7278 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7278[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7279 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7279[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7280 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7280[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7281 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7281[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7282 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7282[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7283 = { sizeof (InjectableInfo_t8B0167EA3F54639C2211BD1BEAF54D2C4063390E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7283[8] = 
{
	InjectableInfo_t8B0167EA3F54639C2211BD1BEAF54D2C4063390E::get_offset_of_Optional_0(),
	InjectableInfo_t8B0167EA3F54639C2211BD1BEAF54D2C4063390E::get_offset_of_Identifier_1(),
	InjectableInfo_t8B0167EA3F54639C2211BD1BEAF54D2C4063390E::get_offset_of_SourceType_2(),
	InjectableInfo_t8B0167EA3F54639C2211BD1BEAF54D2C4063390E::get_offset_of_MemberName_3(),
	InjectableInfo_t8B0167EA3F54639C2211BD1BEAF54D2C4063390E::get_offset_of_MemberType_4(),
	InjectableInfo_t8B0167EA3F54639C2211BD1BEAF54D2C4063390E::get_offset_of_ObjectType_5(),
	InjectableInfo_t8B0167EA3F54639C2211BD1BEAF54D2C4063390E::get_offset_of_Setter_6(),
	InjectableInfo_t8B0167EA3F54639C2211BD1BEAF54D2C4063390E::get_offset_of_DefaultValue_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7284 = { sizeof (InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7284[11] = 
{
	InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF::get_offset_of_U3CObjectTypeU3Ek__BackingField_0(),
	InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF::get_offset_of_U3CParentContextU3Ek__BackingField_1(),
	InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF::get_offset_of_U3CObjectInstanceU3Ek__BackingField_2(),
	InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF::get_offset_of_U3CIdentifierU3Ek__BackingField_3(),
	InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF::get_offset_of_U3CConcreteIdentifierU3Ek__BackingField_4(),
	InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF::get_offset_of_U3CMemberNameU3Ek__BackingField_5(),
	InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF::get_offset_of_U3CMemberTypeU3Ek__BackingField_6(),
	InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF::get_offset_of_U3COptionalU3Ek__BackingField_7(),
	InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF::get_offset_of_U3CSourceTypeU3Ek__BackingField_8(),
	InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF::get_offset_of_U3CFallBackValueU3Ek__BackingField_9(),
	InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF::get_offset_of_U3CContainerU3Ek__BackingField_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7285 = { sizeof (U3Cget_ParentContextsU3Ed__49_t6FE44C4158F6B0806A0D7069F1415DBD0CFC2C4C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7285[5] = 
{
	U3Cget_ParentContextsU3Ed__49_t6FE44C4158F6B0806A0D7069F1415DBD0CFC2C4C::get_offset_of_U3CU3E1__state_0(),
	U3Cget_ParentContextsU3Ed__49_t6FE44C4158F6B0806A0D7069F1415DBD0CFC2C4C::get_offset_of_U3CU3E2__current_1(),
	U3Cget_ParentContextsU3Ed__49_t6FE44C4158F6B0806A0D7069F1415DBD0CFC2C4C::get_offset_of_U3CU3El__initialThreadId_2(),
	U3Cget_ParentContextsU3Ed__49_t6FE44C4158F6B0806A0D7069F1415DBD0CFC2C4C::get_offset_of_U3CU3E4__this_3(),
	U3Cget_ParentContextsU3Ed__49_t6FE44C4158F6B0806A0D7069F1415DBD0CFC2C4C::get_offset_of_U3CU3E7__wrap1_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7286 = { sizeof (U3Cget_ParentContextsAndSelfU3Ed__51_t7762F27E1E3E53C1E19B0EB2A69F072222AAF3F3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7286[5] = 
{
	U3Cget_ParentContextsAndSelfU3Ed__51_t7762F27E1E3E53C1E19B0EB2A69F072222AAF3F3::get_offset_of_U3CU3E1__state_0(),
	U3Cget_ParentContextsAndSelfU3Ed__51_t7762F27E1E3E53C1E19B0EB2A69F072222AAF3F3::get_offset_of_U3CU3E2__current_1(),
	U3Cget_ParentContextsAndSelfU3Ed__51_t7762F27E1E3E53C1E19B0EB2A69F072222AAF3F3::get_offset_of_U3CU3El__initialThreadId_2(),
	U3Cget_ParentContextsAndSelfU3Ed__51_t7762F27E1E3E53C1E19B0EB2A69F072222AAF3F3::get_offset_of_U3CU3E4__this_3(),
	U3Cget_ParentContextsAndSelfU3Ed__51_t7762F27E1E3E53C1E19B0EB2A69F072222AAF3F3::get_offset_of_U3CU3E7__wrap1_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7287 = { sizeof (U3Cget_AllObjectTypesU3Ed__53_t3F12867B877F1245F012C89AD5B62519B3C1E5D7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7287[5] = 
{
	U3Cget_AllObjectTypesU3Ed__53_t3F12867B877F1245F012C89AD5B62519B3C1E5D7::get_offset_of_U3CU3E1__state_0(),
	U3Cget_AllObjectTypesU3Ed__53_t3F12867B877F1245F012C89AD5B62519B3C1E5D7::get_offset_of_U3CU3E2__current_1(),
	U3Cget_AllObjectTypesU3Ed__53_t3F12867B877F1245F012C89AD5B62519B3C1E5D7::get_offset_of_U3CU3El__initialThreadId_2(),
	U3Cget_AllObjectTypesU3Ed__53_t3F12867B877F1245F012C89AD5B62519B3C1E5D7::get_offset_of_U3CU3E4__this_3(),
	U3Cget_AllObjectTypesU3Ed__53_t3F12867B877F1245F012C89AD5B62519B3C1E5D7::get_offset_of_U3CU3E7__wrap1_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7288 = { sizeof (TypeValuePair_tD309CDC658476FDDF08345C92D3F4D7A0A1F9792), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7288[2] = 
{
	TypeValuePair_tD309CDC658476FDDF08345C92D3F4D7A0A1F9792::get_offset_of_Type_0(),
	TypeValuePair_tD309CDC658476FDDF08345C92D3F4D7A0A1F9792::get_offset_of_Value_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7289 = { sizeof (InjectUtil_t0C34AD40453372DB40929117C9254504D6F619FD), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7290 = { sizeof (U3CU3Ec_tFC0B9F2D5D75ECB5126320F842BE4D9212EFC5F7), -1, sizeof(U3CU3Ec_tFC0B9F2D5D75ECB5126320F842BE4D9212EFC5F7_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7290[2] = 
{
	U3CU3Ec_tFC0B9F2D5D75ECB5126320F842BE4D9212EFC5F7_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tFC0B9F2D5D75ECB5126320F842BE4D9212EFC5F7_StaticFields::get_offset_of_U3CU3E9__0_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7291 = { sizeof (U3CU3Ec__DisplayClass8_0_tE41A303884A7DD72E4B1D252D8D7396FA7F64FC8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7291[1] = 
{
	U3CU3Ec__DisplayClass8_0_tE41A303884A7DD72E4B1D252D8D7396FA7F64FC8::get_offset_of_injectedFieldType_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7292 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7293 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7293[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7294 = { sizeof (Context_t04459FF3A5B7E8AF25B965F5717F535AE8FA6DBA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7294[5] = 
{
	Context_t04459FF3A5B7E8AF25B965F5717F535AE8FA6DBA::get_offset_of__installers_4(),
	Context_t04459FF3A5B7E8AF25B965F5717F535AE8FA6DBA::get_offset_of__installerPrefabs_5(),
	Context_t04459FF3A5B7E8AF25B965F5717F535AE8FA6DBA::get_offset_of__scriptableObjectInstallers_6(),
	Context_t04459FF3A5B7E8AF25B965F5717F535AE8FA6DBA::get_offset_of__normalInstallers_7(),
	Context_t04459FF3A5B7E8AF25B965F5717F535AE8FA6DBA::get_offset_of__normalInstallerTypes_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7295 = { sizeof (U3CU3Ec_t373679F74E78B676C1348147F36AFB0F2878189B), -1, sizeof(U3CU3Ec_t373679F74E78B676C1348147F36AFB0F2878189B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7295[2] = 
{
	U3CU3Ec_t373679F74E78B676C1348147F36AFB0F2878189B_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t373679F74E78B676C1348147F36AFB0F2878189B_StaticFields::get_offset_of_U3CU3E9__16_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7296 = { sizeof (GameObjectContext_tFE72D7164DF22B1604E0C36954AEC6708654CB1E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7296[2] = 
{
	GameObjectContext_tFE72D7164DF22B1604E0C36954AEC6708654CB1E::get_offset_of__kernel_12(),
	GameObjectContext_tFE72D7164DF22B1604E0C36954AEC6708654CB1E::get_offset_of__container_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7297 = { sizeof (ProjectContext_tC25F8B4277654D0C24D603FD01EF5D6A7F450270), -1, sizeof(ProjectContext_tC25F8B4277654D0C24D603FD01EF5D6A7F450270_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7297[4] = 
{
	0,
	0,
	ProjectContext_tC25F8B4277654D0C24D603FD01EF5D6A7F450270_StaticFields::get_offset_of__instance_11(),
	ProjectContext_tC25F8B4277654D0C24D603FD01EF5D6A7F450270::get_offset_of__container_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7298 = { sizeof (RunnableContext_tE38018DE42EEBD1F6511E890CE2066A44C6FDBAA), -1, sizeof(RunnableContext_tE38018DE42EEBD1F6511E890CE2066A44C6FDBAA_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7298[3] = 
{
	RunnableContext_tE38018DE42EEBD1F6511E890CE2066A44C6FDBAA::get_offset_of__autoRun_9(),
	RunnableContext_tE38018DE42EEBD1F6511E890CE2066A44C6FDBAA_StaticFields::get_offset_of__staticAutoRun_10(),
	RunnableContext_tE38018DE42EEBD1F6511E890CE2066A44C6FDBAA::get_offset_of_U3CInitializedU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7299 = { sizeof (SceneContext_tA69C18F583C410BB2DA86B5C1F86F27459DA8CA2), -1, sizeof(SceneContext_tA69C18F583C410BB2DA86B5C1F86F27459DA8CA2_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7299[11] = 
{
	SceneContext_tA69C18F583C410BB2DA86B5C1F86F27459DA8CA2_StaticFields::get_offset_of_ExtraBindingsInstallMethod_12(),
	SceneContext_tA69C18F583C410BB2DA86B5C1F86F27459DA8CA2_StaticFields::get_offset_of_ExtraBindingsLateInstallMethod_13(),
	SceneContext_tA69C18F583C410BB2DA86B5C1F86F27459DA8CA2_StaticFields::get_offset_of_ParentContainers_14(),
	SceneContext_tA69C18F583C410BB2DA86B5C1F86F27459DA8CA2::get_offset_of__parentNewObjectsUnderRoot_15(),
	SceneContext_tA69C18F583C410BB2DA86B5C1F86F27459DA8CA2::get_offset_of__contractNames_16(),
	SceneContext_tA69C18F583C410BB2DA86B5C1F86F27459DA8CA2::get_offset_of__parentContractName_17(),
	SceneContext_tA69C18F583C410BB2DA86B5C1F86F27459DA8CA2::get_offset_of__parentContractNames_18(),
	SceneContext_tA69C18F583C410BB2DA86B5C1F86F27459DA8CA2::get_offset_of__container_19(),
	SceneContext_tA69C18F583C410BB2DA86B5C1F86F27459DA8CA2::get_offset_of__decoratorContexts_20(),
	SceneContext_tA69C18F583C410BB2DA86B5C1F86F27459DA8CA2::get_offset_of__hasInstalled_21(),
	SceneContext_tA69C18F583C410BB2DA86B5C1F86F27459DA8CA2::get_offset_of__hasResolved_22(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
