﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// NotificationSamples.IGameNotification
struct IGameNotification_t0B5B18AC9D2FBD4EE0F4CBBB1C07624F45BF794A;
// NotificationSamples.IGameNotificationsPlatform
struct IGameNotificationsPlatform_t0C0586A05DB85474B1275B26088A998CC2567D10;
// NotificationSamples.IPendingNotificationsSerializer
struct IPendingNotificationsSerializer_tCDCE81D5B53E152C81C7CC3676B1E658B3E9BBD2;
// System.Action`1<NotificationSamples.PendingNotification>
struct Action_1_tF8CCF5268A443AE2FC0E1CC856753980670A6A09;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.IEnumerator`1<System.Reflection.FieldInfo>
struct IEnumerator_1_tFF29F758EE2F45C6B96A41F6379516A23E129A8C;
// System.Collections.Generic.IEnumerator`1<System.Reflection.PropertyInfo>
struct IEnumerator_1_t537D797E2644AF9046B1E4CAAC405D8CE9D01534;
// System.Collections.Generic.IEnumerator`1<UnityEngine.SceneManagement.Scene>
struct IEnumerator_1_tD380563E1B2946325932A78A8B0091DD21D78B8D;
// System.Collections.Generic.List`1<NotificationSamples.PendingNotification>
struct List_1_tD4AB95F0286C0D90E2D91E8BD2380724938469CB;
// System.Collections.Generic.List`1<System.Action>
struct List_1_t9E7F53A878191B703A76E69CE36AA90A372800E7;
// System.Collections.Generic.List`1<System.Reflection.FieldInfo>
struct List_1_t0F52CF0559ED3BE13F054035F8208B5FDEB9D9DA;
// System.Collections.Generic.List`1<System.Type>
struct List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0;
// System.Collections.Generic.List`1<Tayx.Graphy.GraphyDebugger/DebugPacket>
struct List_1_tF47A57C0BAB879ECB9F887A704B0308283D36180;
// System.Collections.Generic.List`1<Zenject.InjectableInfo>
struct List_1_t19FD687049A90924DCEA81F81C85555F15021A68;
// System.Collections.Generic.List`1<Zenject.PostInjectableInfo>
struct List_1_t8AA9EFA21EF1045CBB5A905EB31B2D66A23339FB;
// System.Collections.Generic.List`1<Zenject.SpaceFighter.IEnemyState>
struct List_1_t9DDEECAEE13EF4EB757430774E50AF9385022D8A;
// System.Collections.Generic.Stack`1<Zenject.SpaceFighter.Bullet>
struct Stack_1_tB77C1D3187C24DCF32ADB3F72120A533AA3382B8;
// System.Collections.Generic.Stack`1<Zenject.SpaceFighter.EnemyFacade>
struct Stack_1_t25806F0F92C39F84EA0915B1CB7846F658DFB9D7;
// System.Collections.Generic.Stack`1<Zenject.SpaceFighter.Explosion>
struct Stack_1_tF195700F3001F99D6106CFA1D2972886772FB81A;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.Comparison`1<NotificationSamples.PendingNotification>
struct Comparison_1_tF6A3951CF7C688C2D3C17BFB2905A271F48CA16D;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.Func`2<NotificationSamples.PendingNotification,System.Boolean>
struct Func_2_tE6774A9EB2E94FEF6343DB1910A6147FEE9DF278;
// System.Func`2<System.Int64,System.Int32>
struct Func_2_tF2233F288DD2403923F69A3E8248EBBA6D14BB0F;
// System.Func`2<System.Reflection.ConstructorInfo,System.Boolean>
struct Func_2_tE50BC1D78F3969472F5276AA4812403240D166E3;
// System.Func`2<System.Reflection.FieldInfo,System.Boolean>
struct Func_2_t8B737A62DCD6BCDB37818A2DFF357973A9317E8D;
// System.Func`2<System.Reflection.FieldInfo,System.String>
struct Func_2_tB05273985A55D6B76307D55CDF577A8EE8D2E3E2;
// System.Func`2<System.Reflection.MethodInfo,System.Boolean>
struct Func_2_t25BDD008BABA5EB0719A205177AD1FB36DC6A438;
// System.Func`2<System.Reflection.ParameterInfo,Zenject.InjectableInfo>
struct Func_2_t2ED5D3B2C08D780C0E2B094DBB8E41935D130170;
// System.Func`2<System.Reflection.PropertyInfo,System.Boolean>
struct Func_2_tF059A6F3E6CDF153D0E7F4B9A9B8AB75DBA4BB7C;
// System.Func`2<System.Type,Zenject.TypeValuePair>
struct Func_2_t1A79A9E174B5F4FD27B0662B857E9836068933B3;
// System.Func`2<UnityEngine.GameObject,System.Boolean>
struct Func_2_t9A23FB973C3AFF530C708F9A738041D32958B273;
// System.Func`2<UnityEngine.GameObject,System.Collections.Generic.IEnumerable`1<Zenject.SceneContext>>
struct Func_2_t6D143D5B850346EA82D99E2A69981B807B171CF6;
// System.Func`2<Zenject.PostInjectableInfo,System.Collections.Generic.IEnumerable`1<Zenject.InjectableInfo>>
struct Func_2_t28651F76A116BE0870CD3719ED8679E661C0C90D;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.Reflection.ConstructorInfo
struct ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF;
// System.Reflection.FieldInfo
struct FieldInfo_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Reflection.PropertyInfo
struct PropertyInfo_t;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// Tayx.Graphy.Audio.G_AudioMonitor
struct G_AudioMonitor_t386A2D77995058DA7241FC1C90AF9FF6DE38EA2E;
// Tayx.Graphy.Fps.G_FpsMonitor
struct G_FpsMonitor_tDCDEDA2708585BE6B0202F2B3230D8C5F6820672;
// Tayx.Graphy.GraphyDebugger
struct GraphyDebugger_t1DC748A4B53549510635E53605A906AC607CC4C9;
// Tayx.Graphy.Ram.G_RamMonitor
struct G_RamMonitor_tCE95C3ECC0C03781E405698F56E20B12BC2650CA;
// UnityEngine.AudioClip
struct AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051;
// UnityEngine.Camera
struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34;
// UnityEngine.Collider
struct Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.Material
struct Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598;
// UnityEngine.MeshRenderer
struct MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED;
// UnityEngine.ParticleSystem
struct ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D;
// UnityEngine.Rigidbody
struct Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5;
// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C;
// Zenject.BindingId
struct BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA;
// Zenject.DiContainer
struct DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5;
// Zenject.IFactory`1<Zenject.SpaceFighter.Bullet>
struct IFactory_1_t2D36FE2E188C59A821E641C79AEC07C5C2773819;
// Zenject.IFactory`1<Zenject.SpaceFighter.EnemyFacade>
struct IFactory_1_t6337EDB14DE454C6E54F7ECA9F2548A8984A8754;
// Zenject.IFactory`1<Zenject.SpaceFighter.Explosion>
struct IFactory_1_t4FEEE1886D74E7979DEE30D2D893564F08B0A9F3;
// Zenject.InjectableInfo
struct InjectableInfo_t8B0167EA3F54639C2211BD1BEAF54D2C4063390E;
// Zenject.MemoryPoolSettings
struct MemoryPoolSettings_tC33180DF15EF5B1103FB5191CFC0569BB3C74AA5;
// Zenject.ProjectKernel
struct ProjectKernel_tC9D59436FEAD42E4DE77F148A6A66D5E3A7DEC65;
// Zenject.SceneContext
struct SceneContext_tA69C18F583C410BB2DA86B5C1F86F27459DA8CA2;
// Zenject.SignalManager
struct SignalManager_t38E64F258F79FF33F3FDBB9423271BEFF03D7673;
// Zenject.SignalSettings
struct SignalSettings_tD753AA3D16CEA36F97CDAEC5428C5217F1EB6540;
// Zenject.SpaceFighter.Bullet/Pool
struct Pool_t28D3C271D1214C1C919D02B9C38A6A5698F231F1;
// Zenject.SpaceFighter.Enemy
struct Enemy_tC40CA91F878B23445D874D13243C2824315DE3D2;
// Zenject.SpaceFighter.EnemyCommonSettings
struct EnemyCommonSettings_tF1C89E066A85E57A479925E334EDE12BEAAE767B;
// Zenject.SpaceFighter.EnemyDeathHandler
struct EnemyDeathHandler_t87BF96335A31C0712F7B9E80B22587838EE9B4C5;
// Zenject.SpaceFighter.EnemyDeathHandler/Settings
struct Settings_tCDA69B4C2B2FC2E7E5DC396996019836DE3ED1E2;
// Zenject.SpaceFighter.EnemyFacade
struct EnemyFacade_t1D5DD8AEDE16BD5F9C2C32C4C0AB954CE1811FC1;
// Zenject.SpaceFighter.EnemyFacade/Pool
struct Pool_t735C24F8E4685CE9F8814136D7E4B3548882F387;
// Zenject.SpaceFighter.EnemyInstaller/Settings
struct Settings_tD2755B3F26D35678AF75CBB0C5BF9C282206FF91;
// Zenject.SpaceFighter.EnemyKilledSignal
struct EnemyKilledSignal_t5291603135B3B8F73E6ED1317431EE5091D78DC8;
// Zenject.SpaceFighter.EnemyRotationHandler/Settings
struct Settings_t3480554AD22B7730290AB95863B9EDC48610BCE9;
// Zenject.SpaceFighter.EnemySpawner/Settings
struct Settings_tD4EAF29A0539BDE0CFDC096ED188196E8DD744C2;
// Zenject.SpaceFighter.EnemyStateAttack/Settings
struct Settings_t3ABF5D501B5AFEE2903D5F846E8EC24317DB3528;
// Zenject.SpaceFighter.EnemyStateFollow/Settings
struct Settings_tC265A1DB51490C02FF21B3A9733F230E53104BAC;
// Zenject.SpaceFighter.EnemyStateIdle/Settings
struct Settings_t6A845C637F62D4448CE7764CFBBFBDDDA7951042;
// Zenject.SpaceFighter.EnemyStateManager
struct EnemyStateManager_tD3E2662DA02D660CB7356CE3563F928B0E220C9F;
// Zenject.SpaceFighter.EnemyTunables
struct EnemyTunables_tD9997725AFF614521DD7088FFF23C1D8DD06DE18;
// Zenject.SpaceFighter.Explosion/Pool
struct Pool_tF6D222503E0FF0BBA73DDF77321A0C8B6CA1D7C7;
// Zenject.SpaceFighter.GameInstaller/Settings
struct Settings_tDDDA6539A297D2F0F5790F2BE702EABB8BC23312;
// Zenject.SpaceFighter.GameRestartHandler/Settings
struct Settings_tDA43D3BAE8BE0B223AEFEE4FC2782ACB7169D1C2;
// Zenject.SpaceFighter.GameSettingsInstaller/EnemySettings
struct EnemySettings_t87F1C13D725FC80A97C27012C240D7E9150A5785;
// Zenject.SpaceFighter.GameSettingsInstaller/PlayerSettings
struct PlayerSettings_t50B498D03A02BD7CD7E2367EFDAD0428FBBC1058;
// Zenject.SpaceFighter.IAudioPlayer
struct IAudioPlayer_t33C1660995FB96B1250697D8E5305A016266DD87;
// Zenject.SpaceFighter.IEnemyState
struct IEnemyState_tC0714B635EB0D28F5550F13826510F043C6BFC0C;
// Zenject.SpaceFighter.IPlayer
struct IPlayer_t5CF8AC53D2AF36BC15C4DDFA6D50EEFDCB5207F7;
// Zenject.SpaceFighter.LevelBoundary
struct LevelBoundary_t8E706FF3A27DD22A30FE8E79EF745B6B7D78577B;
// Zenject.SpaceFighter.Player
struct Player_t0644AFD1050593EF42B2459D30EA133F19E7A402;
// Zenject.SpaceFighter.PlayerDamageHandler
struct PlayerDamageHandler_tFF3D703F066B3204ADD106FCC8B95D7EF61B4E33;
// Zenject.SpaceFighter.PlayerDamageHandler/Settings
struct Settings_t48778C45589CCBEC26A7E67F855609F50926CB44;
// Zenject.SpaceFighter.PlayerDiedSignal
struct PlayerDiedSignal_tD47ED6EA743FEE4E5D7B3289450C3EA59E018C7F;
// Zenject.SpaceFighter.PlayerHealthWatcher/Settings
struct Settings_t83D6CE3845CA378293BC213A44067B48638017A4;
// Zenject.SpaceFighter.PlayerInputState
struct PlayerInputState_t4B9BA63DFF6982E3A6FFB060DDE8C17DC0A83CF8;
// Zenject.SpaceFighter.PlayerInstaller/Settings
struct Settings_tA79246F22F290206CED693068D5770E633DD5CEE;
// Zenject.SpaceFighter.PlayerMoveHandler/Settings
struct Settings_t5D29F620DD8E2C4750D3670B9D88BD2F6F4C6FC5;
// Zenject.SpaceFighter.PlayerShootHandler/Settings
struct Settings_t59B948D04E16E5F614372F81DC0B8066A189FF9D;

struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef DEFAULTSERIALIZER_T3DD2A46B85DA76C65C72F3330014F854FAC13B42_H
#define DEFAULTSERIALIZER_T3DD2A46B85DA76C65C72F3330014F854FAC13B42_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NotificationSamples.DefaultSerializer
struct  DefaultSerializer_t3DD2A46B85DA76C65C72F3330014F854FAC13B42  : public RuntimeObject
{
public:
	// System.String NotificationSamples.DefaultSerializer::filename
	String_t* ___filename_1;

public:
	inline static int32_t get_offset_of_filename_1() { return static_cast<int32_t>(offsetof(DefaultSerializer_t3DD2A46B85DA76C65C72F3330014F854FAC13B42, ___filename_1)); }
	inline String_t* get_filename_1() const { return ___filename_1; }
	inline String_t** get_address_of_filename_1() { return &___filename_1; }
	inline void set_filename_1(String_t* value)
	{
		___filename_1 = value;
		Il2CppCodeGenWriteBarrier((&___filename_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTSERIALIZER_T3DD2A46B85DA76C65C72F3330014F854FAC13B42_H
#ifndef U3CU3EC_T6C3ABF12175CFC6A4EDCA3C8EBCD0FE21E14957E_H
#define U3CU3EC_T6C3ABF12175CFC6A4EDCA3C8EBCD0FE21E14957E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NotificationSamples.GameNotificationChannel_<>c
struct  U3CU3Ec_t6C3ABF12175CFC6A4EDCA3C8EBCD0FE21E14957E  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t6C3ABF12175CFC6A4EDCA3C8EBCD0FE21E14957E_StaticFields
{
public:
	// NotificationSamples.GameNotificationChannel_<>c NotificationSamples.GameNotificationChannel_<>c::<>9
	U3CU3Ec_t6C3ABF12175CFC6A4EDCA3C8EBCD0FE21E14957E * ___U3CU3E9_0;
	// System.Func`2<System.Int64,System.Int32> NotificationSamples.GameNotificationChannel_<>c::<>9__13_0
	Func_2_tF2233F288DD2403923F69A3E8248EBBA6D14BB0F * ___U3CU3E9__13_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t6C3ABF12175CFC6A4EDCA3C8EBCD0FE21E14957E_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t6C3ABF12175CFC6A4EDCA3C8EBCD0FE21E14957E * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t6C3ABF12175CFC6A4EDCA3C8EBCD0FE21E14957E ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t6C3ABF12175CFC6A4EDCA3C8EBCD0FE21E14957E * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__13_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t6C3ABF12175CFC6A4EDCA3C8EBCD0FE21E14957E_StaticFields, ___U3CU3E9__13_0_1)); }
	inline Func_2_tF2233F288DD2403923F69A3E8248EBBA6D14BB0F * get_U3CU3E9__13_0_1() const { return ___U3CU3E9__13_0_1; }
	inline Func_2_tF2233F288DD2403923F69A3E8248EBBA6D14BB0F ** get_address_of_U3CU3E9__13_0_1() { return &___U3CU3E9__13_0_1; }
	inline void set_U3CU3E9__13_0_1(Func_2_tF2233F288DD2403923F69A3E8248EBBA6D14BB0F * value)
	{
		___U3CU3E9__13_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__13_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T6C3ABF12175CFC6A4EDCA3C8EBCD0FE21E14957E_H
#ifndef U3CU3EC_TD764979F67CF10610B4505779C0F4A2EB108DF89_H
#define U3CU3EC_TD764979F67CF10610B4505779C0F4A2EB108DF89_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NotificationSamples.GameNotificationsManager_<>c
struct  U3CU3Ec_tD764979F67CF10610B4505779C0F4A2EB108DF89  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tD764979F67CF10610B4505779C0F4A2EB108DF89_StaticFields
{
public:
	// NotificationSamples.GameNotificationsManager_<>c NotificationSamples.GameNotificationsManager_<>c::<>9
	U3CU3Ec_tD764979F67CF10610B4505779C0F4A2EB108DF89 * ___U3CU3E9_0;
	// System.Func`2<NotificationSamples.PendingNotification,System.Boolean> NotificationSamples.GameNotificationsManager_<>c::<>9__34_0
	Func_2_tE6774A9EB2E94FEF6343DB1910A6147FEE9DF278 * ___U3CU3E9__34_0_1;
	// System.Comparison`1<NotificationSamples.PendingNotification> NotificationSamples.GameNotificationsManager_<>c::<>9__34_1
	Comparison_1_tF6A3951CF7C688C2D3C17BFB2905A271F48CA16D * ___U3CU3E9__34_1_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tD764979F67CF10610B4505779C0F4A2EB108DF89_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tD764979F67CF10610B4505779C0F4A2EB108DF89 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tD764979F67CF10610B4505779C0F4A2EB108DF89 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tD764979F67CF10610B4505779C0F4A2EB108DF89 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__34_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tD764979F67CF10610B4505779C0F4A2EB108DF89_StaticFields, ___U3CU3E9__34_0_1)); }
	inline Func_2_tE6774A9EB2E94FEF6343DB1910A6147FEE9DF278 * get_U3CU3E9__34_0_1() const { return ___U3CU3E9__34_0_1; }
	inline Func_2_tE6774A9EB2E94FEF6343DB1910A6147FEE9DF278 ** get_address_of_U3CU3E9__34_0_1() { return &___U3CU3E9__34_0_1; }
	inline void set_U3CU3E9__34_0_1(Func_2_tE6774A9EB2E94FEF6343DB1910A6147FEE9DF278 * value)
	{
		___U3CU3E9__34_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__34_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__34_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_tD764979F67CF10610B4505779C0F4A2EB108DF89_StaticFields, ___U3CU3E9__34_1_2)); }
	inline Comparison_1_tF6A3951CF7C688C2D3C17BFB2905A271F48CA16D * get_U3CU3E9__34_1_2() const { return ___U3CU3E9__34_1_2; }
	inline Comparison_1_tF6A3951CF7C688C2D3C17BFB2905A271F48CA16D ** get_address_of_U3CU3E9__34_1_2() { return &___U3CU3E9__34_1_2; }
	inline void set_U3CU3E9__34_1_2(Comparison_1_tF6A3951CF7C688C2D3C17BFB2905A271F48CA16D * value)
	{
		___U3CU3E9__34_1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__34_1_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TD764979F67CF10610B4505779C0F4A2EB108DF89_H
#ifndef U3CU3EC__DISPLAYCLASS38_0_T8098AD21654110DD42EAC10D5249C9B09F6413AC_H
#define U3CU3EC__DISPLAYCLASS38_0_T8098AD21654110DD42EAC10D5249C9B09F6413AC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NotificationSamples.GameNotificationsManager_<>c__DisplayClass38_0
struct  U3CU3Ec__DisplayClass38_0_t8098AD21654110DD42EAC10D5249C9B09F6413AC  : public RuntimeObject
{
public:
	// System.Int32 NotificationSamples.GameNotificationsManager_<>c__DisplayClass38_0::notificationId
	int32_t ___notificationId_0;

public:
	inline static int32_t get_offset_of_notificationId_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass38_0_t8098AD21654110DD42EAC10D5249C9B09F6413AC, ___notificationId_0)); }
	inline int32_t get_notificationId_0() const { return ___notificationId_0; }
	inline int32_t* get_address_of_notificationId_0() { return &___notificationId_0; }
	inline void set_notificationId_0(int32_t value)
	{
		___notificationId_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS38_0_T8098AD21654110DD42EAC10D5249C9B09F6413AC_H
#ifndef U3CU3EC__DISPLAYCLASS42_0_TBB6778E6F27425013423D76334657524233BFBC6_H
#define U3CU3EC__DISPLAYCLASS42_0_TBB6778E6F27425013423D76334657524233BFBC6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NotificationSamples.GameNotificationsManager_<>c__DisplayClass42_0
struct  U3CU3Ec__DisplayClass42_0_tBB6778E6F27425013423D76334657524233BFBC6  : public RuntimeObject
{
public:
	// NotificationSamples.IGameNotification NotificationSamples.GameNotificationsManager_<>c__DisplayClass42_0::deliveredNotification
	RuntimeObject* ___deliveredNotification_0;

public:
	inline static int32_t get_offset_of_deliveredNotification_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass42_0_tBB6778E6F27425013423D76334657524233BFBC6, ___deliveredNotification_0)); }
	inline RuntimeObject* get_deliveredNotification_0() const { return ___deliveredNotification_0; }
	inline RuntimeObject** get_address_of_deliveredNotification_0() { return &___deliveredNotification_0; }
	inline void set_deliveredNotification_0(RuntimeObject* value)
	{
		___deliveredNotification_0 = value;
		Il2CppCodeGenWriteBarrier((&___deliveredNotification_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS42_0_TBB6778E6F27425013423D76334657524233BFBC6_H
#ifndef PENDINGNOTIFICATION_TB499878D49AF89331495B709217BBE577A95FB87_H
#define PENDINGNOTIFICATION_TB499878D49AF89331495B709217BBE577A95FB87_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NotificationSamples.PendingNotification
struct  PendingNotification_tB499878D49AF89331495B709217BBE577A95FB87  : public RuntimeObject
{
public:
	// System.Boolean NotificationSamples.PendingNotification::Reschedule
	bool ___Reschedule_0;
	// NotificationSamples.IGameNotification NotificationSamples.PendingNotification::Notification
	RuntimeObject* ___Notification_1;

public:
	inline static int32_t get_offset_of_Reschedule_0() { return static_cast<int32_t>(offsetof(PendingNotification_tB499878D49AF89331495B709217BBE577A95FB87, ___Reschedule_0)); }
	inline bool get_Reschedule_0() const { return ___Reschedule_0; }
	inline bool* get_address_of_Reschedule_0() { return &___Reschedule_0; }
	inline void set_Reschedule_0(bool value)
	{
		___Reschedule_0 = value;
	}

	inline static int32_t get_offset_of_Notification_1() { return static_cast<int32_t>(offsetof(PendingNotification_tB499878D49AF89331495B709217BBE577A95FB87, ___Notification_1)); }
	inline RuntimeObject* get_Notification_1() const { return ___Notification_1; }
	inline RuntimeObject** get_address_of_Notification_1() { return &___Notification_1; }
	inline void set_Notification_1(RuntimeObject* value)
	{
		___Notification_1 = value;
		Il2CppCodeGenWriteBarrier((&___Notification_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PENDINGNOTIFICATION_TB499878D49AF89331495B709217BBE577A95FB87_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef INSTALLERBASE_T4F464ACE8FEB8CEEE7B4352A271F09894C28AD79_H
#define INSTALLERBASE_T4F464ACE8FEB8CEEE7B4352A271F09894C28AD79_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.InstallerBase
struct  InstallerBase_t4F464ACE8FEB8CEEE7B4352A271F09894C28AD79  : public RuntimeObject
{
public:
	// Zenject.DiContainer Zenject.InstallerBase::_container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____container_0;

public:
	inline static int32_t get_offset_of__container_0() { return static_cast<int32_t>(offsetof(InstallerBase_t4F464ACE8FEB8CEEE7B4352A271F09894C28AD79, ____container_0)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__container_0() const { return ____container_0; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__container_0() { return &____container_0; }
	inline void set__container_0(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____container_0 = value;
		Il2CppCodeGenWriteBarrier((&____container_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTALLERBASE_T4F464ACE8FEB8CEEE7B4352A271F09894C28AD79_H
#ifndef ZENUTILINTERNAL_T9F6EE1BF65F53C2F9521E247F9627CA2721C0D2F_H
#define ZENUTILINTERNAL_T9F6EE1BF65F53C2F9521E247F9627CA2721C0D2F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.Internal.ZenUtilInternal
struct  ZenUtilInternal_t9F6EE1BF65F53C2F9521E247F9627CA2721C0D2F  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZENUTILINTERNAL_T9F6EE1BF65F53C2F9521E247F9627CA2721C0D2F_H
#ifndef U3CU3EC_T0BEBDED76D743DFB6185223F22CC6D460B79DF63_H
#define U3CU3EC_T0BEBDED76D743DFB6185223F22CC6D460B79DF63_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.Internal.ZenUtilInternal_<>c
struct  U3CU3Ec_t0BEBDED76D743DFB6185223F22CC6D460B79DF63  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t0BEBDED76D743DFB6185223F22CC6D460B79DF63_StaticFields
{
public:
	// Zenject.Internal.ZenUtilInternal_<>c Zenject.Internal.ZenUtilInternal_<>c::<>9
	U3CU3Ec_t0BEBDED76D743DFB6185223F22CC6D460B79DF63 * ___U3CU3E9_0;
	// System.Func`2<UnityEngine.GameObject,System.Collections.Generic.IEnumerable`1<Zenject.SceneContext>> Zenject.Internal.ZenUtilInternal_<>c::<>9__3_0
	Func_2_t6D143D5B850346EA82D99E2A69981B807B171CF6 * ___U3CU3E9__3_0_1;
	// System.Func`2<UnityEngine.GameObject,System.Boolean> Zenject.Internal.ZenUtilInternal_<>c::<>9__7_0
	Func_2_t9A23FB973C3AFF530C708F9A738041D32958B273 * ___U3CU3E9__7_0_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t0BEBDED76D743DFB6185223F22CC6D460B79DF63_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t0BEBDED76D743DFB6185223F22CC6D460B79DF63 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t0BEBDED76D743DFB6185223F22CC6D460B79DF63 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t0BEBDED76D743DFB6185223F22CC6D460B79DF63 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__3_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t0BEBDED76D743DFB6185223F22CC6D460B79DF63_StaticFields, ___U3CU3E9__3_0_1)); }
	inline Func_2_t6D143D5B850346EA82D99E2A69981B807B171CF6 * get_U3CU3E9__3_0_1() const { return ___U3CU3E9__3_0_1; }
	inline Func_2_t6D143D5B850346EA82D99E2A69981B807B171CF6 ** get_address_of_U3CU3E9__3_0_1() { return &___U3CU3E9__3_0_1; }
	inline void set_U3CU3E9__3_0_1(Func_2_t6D143D5B850346EA82D99E2A69981B807B171CF6 * value)
	{
		___U3CU3E9__3_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__3_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__7_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t0BEBDED76D743DFB6185223F22CC6D460B79DF63_StaticFields, ___U3CU3E9__7_0_2)); }
	inline Func_2_t9A23FB973C3AFF530C708F9A738041D32958B273 * get_U3CU3E9__7_0_2() const { return ___U3CU3E9__7_0_2; }
	inline Func_2_t9A23FB973C3AFF530C708F9A738041D32958B273 ** get_address_of_U3CU3E9__7_0_2() { return &___U3CU3E9__7_0_2; }
	inline void set_U3CU3E9__7_0_2(Func_2_t9A23FB973C3AFF530C708F9A738041D32958B273 * value)
	{
		___U3CU3E9__7_0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__7_0_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T0BEBDED76D743DFB6185223F22CC6D460B79DF63_H
#ifndef U3CGETALLSCENECONTEXTSU3ED__3_T51F57C904E207984C55541E310AC69674F084373_H
#define U3CGETALLSCENECONTEXTSU3ED__3_T51F57C904E207984C55541E310AC69674F084373_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.Internal.ZenUtilInternal_<GetAllSceneContexts>d__3
struct  U3CGetAllSceneContextsU3Ed__3_t51F57C904E207984C55541E310AC69674F084373  : public RuntimeObject
{
public:
	// System.Int32 Zenject.Internal.ZenUtilInternal_<GetAllSceneContexts>d__3::<>1__state
	int32_t ___U3CU3E1__state_0;
	// Zenject.SceneContext Zenject.Internal.ZenUtilInternal_<GetAllSceneContexts>d__3::<>2__current
	SceneContext_tA69C18F583C410BB2DA86B5C1F86F27459DA8CA2 * ___U3CU3E2__current_1;
	// System.Int32 Zenject.Internal.ZenUtilInternal_<GetAllSceneContexts>d__3::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// System.Collections.Generic.IEnumerator`1<UnityEngine.SceneManagement.Scene> Zenject.Internal.ZenUtilInternal_<GetAllSceneContexts>d__3::<>7__wrap1
	RuntimeObject* ___U3CU3E7__wrap1_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetAllSceneContextsU3Ed__3_t51F57C904E207984C55541E310AC69674F084373, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetAllSceneContextsU3Ed__3_t51F57C904E207984C55541E310AC69674F084373, ___U3CU3E2__current_1)); }
	inline SceneContext_tA69C18F583C410BB2DA86B5C1F86F27459DA8CA2 * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline SceneContext_tA69C18F583C410BB2DA86B5C1F86F27459DA8CA2 ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(SceneContext_tA69C18F583C410BB2DA86B5C1F86F27459DA8CA2 * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CGetAllSceneContextsU3Ed__3_t51F57C904E207984C55541E310AC69674F084373, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_3() { return static_cast<int32_t>(offsetof(U3CGetAllSceneContextsU3Ed__3_t51F57C904E207984C55541E310AC69674F084373, ___U3CU3E7__wrap1_3)); }
	inline RuntimeObject* get_U3CU3E7__wrap1_3() const { return ___U3CU3E7__wrap1_3; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap1_3() { return &___U3CU3E7__wrap1_3; }
	inline void set_U3CU3E7__wrap1_3(RuntimeObject* value)
	{
		___U3CU3E7__wrap1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap1_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETALLSCENECONTEXTSU3ED__3_T51F57C904E207984C55541E310AC69674F084373_H
#ifndef MEMORYPOOLBASE_1_T02B1468D062FD8CD3AF2529A5FD170C9590739E9_H
#define MEMORYPOOLBASE_1_T02B1468D062FD8CD3AF2529A5FD170C9590739E9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.MemoryPoolBase`1<Zenject.SpaceFighter.Bullet>
struct  MemoryPoolBase_1_t02B1468D062FD8CD3AF2529A5FD170C9590739E9  : public RuntimeObject
{
public:
	// System.Collections.Generic.Stack`1<TContract> Zenject.MemoryPoolBase`1::_inactiveItems
	Stack_1_tB77C1D3187C24DCF32ADB3F72120A533AA3382B8 * ____inactiveItems_0;
	// Zenject.IFactory`1<TContract> Zenject.MemoryPoolBase`1::_factory
	RuntimeObject* ____factory_1;
	// Zenject.MemoryPoolSettings Zenject.MemoryPoolBase`1::_settings
	MemoryPoolSettings_tC33180DF15EF5B1103FB5191CFC0569BB3C74AA5 * ____settings_2;
	// System.Int32 Zenject.MemoryPoolBase`1::_activeCount
	int32_t ____activeCount_3;

public:
	inline static int32_t get_offset_of__inactiveItems_0() { return static_cast<int32_t>(offsetof(MemoryPoolBase_1_t02B1468D062FD8CD3AF2529A5FD170C9590739E9, ____inactiveItems_0)); }
	inline Stack_1_tB77C1D3187C24DCF32ADB3F72120A533AA3382B8 * get__inactiveItems_0() const { return ____inactiveItems_0; }
	inline Stack_1_tB77C1D3187C24DCF32ADB3F72120A533AA3382B8 ** get_address_of__inactiveItems_0() { return &____inactiveItems_0; }
	inline void set__inactiveItems_0(Stack_1_tB77C1D3187C24DCF32ADB3F72120A533AA3382B8 * value)
	{
		____inactiveItems_0 = value;
		Il2CppCodeGenWriteBarrier((&____inactiveItems_0), value);
	}

	inline static int32_t get_offset_of__factory_1() { return static_cast<int32_t>(offsetof(MemoryPoolBase_1_t02B1468D062FD8CD3AF2529A5FD170C9590739E9, ____factory_1)); }
	inline RuntimeObject* get__factory_1() const { return ____factory_1; }
	inline RuntimeObject** get_address_of__factory_1() { return &____factory_1; }
	inline void set__factory_1(RuntimeObject* value)
	{
		____factory_1 = value;
		Il2CppCodeGenWriteBarrier((&____factory_1), value);
	}

	inline static int32_t get_offset_of__settings_2() { return static_cast<int32_t>(offsetof(MemoryPoolBase_1_t02B1468D062FD8CD3AF2529A5FD170C9590739E9, ____settings_2)); }
	inline MemoryPoolSettings_tC33180DF15EF5B1103FB5191CFC0569BB3C74AA5 * get__settings_2() const { return ____settings_2; }
	inline MemoryPoolSettings_tC33180DF15EF5B1103FB5191CFC0569BB3C74AA5 ** get_address_of__settings_2() { return &____settings_2; }
	inline void set__settings_2(MemoryPoolSettings_tC33180DF15EF5B1103FB5191CFC0569BB3C74AA5 * value)
	{
		____settings_2 = value;
		Il2CppCodeGenWriteBarrier((&____settings_2), value);
	}

	inline static int32_t get_offset_of__activeCount_3() { return static_cast<int32_t>(offsetof(MemoryPoolBase_1_t02B1468D062FD8CD3AF2529A5FD170C9590739E9, ____activeCount_3)); }
	inline int32_t get__activeCount_3() const { return ____activeCount_3; }
	inline int32_t* get_address_of__activeCount_3() { return &____activeCount_3; }
	inline void set__activeCount_3(int32_t value)
	{
		____activeCount_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMORYPOOLBASE_1_T02B1468D062FD8CD3AF2529A5FD170C9590739E9_H
#ifndef MEMORYPOOLBASE_1_T09D148DA18FA8D291CDA958BE927BAEC42E65423_H
#define MEMORYPOOLBASE_1_T09D148DA18FA8D291CDA958BE927BAEC42E65423_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.MemoryPoolBase`1<Zenject.SpaceFighter.EnemyFacade>
struct  MemoryPoolBase_1_t09D148DA18FA8D291CDA958BE927BAEC42E65423  : public RuntimeObject
{
public:
	// System.Collections.Generic.Stack`1<TContract> Zenject.MemoryPoolBase`1::_inactiveItems
	Stack_1_t25806F0F92C39F84EA0915B1CB7846F658DFB9D7 * ____inactiveItems_0;
	// Zenject.IFactory`1<TContract> Zenject.MemoryPoolBase`1::_factory
	RuntimeObject* ____factory_1;
	// Zenject.MemoryPoolSettings Zenject.MemoryPoolBase`1::_settings
	MemoryPoolSettings_tC33180DF15EF5B1103FB5191CFC0569BB3C74AA5 * ____settings_2;
	// System.Int32 Zenject.MemoryPoolBase`1::_activeCount
	int32_t ____activeCount_3;

public:
	inline static int32_t get_offset_of__inactiveItems_0() { return static_cast<int32_t>(offsetof(MemoryPoolBase_1_t09D148DA18FA8D291CDA958BE927BAEC42E65423, ____inactiveItems_0)); }
	inline Stack_1_t25806F0F92C39F84EA0915B1CB7846F658DFB9D7 * get__inactiveItems_0() const { return ____inactiveItems_0; }
	inline Stack_1_t25806F0F92C39F84EA0915B1CB7846F658DFB9D7 ** get_address_of__inactiveItems_0() { return &____inactiveItems_0; }
	inline void set__inactiveItems_0(Stack_1_t25806F0F92C39F84EA0915B1CB7846F658DFB9D7 * value)
	{
		____inactiveItems_0 = value;
		Il2CppCodeGenWriteBarrier((&____inactiveItems_0), value);
	}

	inline static int32_t get_offset_of__factory_1() { return static_cast<int32_t>(offsetof(MemoryPoolBase_1_t09D148DA18FA8D291CDA958BE927BAEC42E65423, ____factory_1)); }
	inline RuntimeObject* get__factory_1() const { return ____factory_1; }
	inline RuntimeObject** get_address_of__factory_1() { return &____factory_1; }
	inline void set__factory_1(RuntimeObject* value)
	{
		____factory_1 = value;
		Il2CppCodeGenWriteBarrier((&____factory_1), value);
	}

	inline static int32_t get_offset_of__settings_2() { return static_cast<int32_t>(offsetof(MemoryPoolBase_1_t09D148DA18FA8D291CDA958BE927BAEC42E65423, ____settings_2)); }
	inline MemoryPoolSettings_tC33180DF15EF5B1103FB5191CFC0569BB3C74AA5 * get__settings_2() const { return ____settings_2; }
	inline MemoryPoolSettings_tC33180DF15EF5B1103FB5191CFC0569BB3C74AA5 ** get_address_of__settings_2() { return &____settings_2; }
	inline void set__settings_2(MemoryPoolSettings_tC33180DF15EF5B1103FB5191CFC0569BB3C74AA5 * value)
	{
		____settings_2 = value;
		Il2CppCodeGenWriteBarrier((&____settings_2), value);
	}

	inline static int32_t get_offset_of__activeCount_3() { return static_cast<int32_t>(offsetof(MemoryPoolBase_1_t09D148DA18FA8D291CDA958BE927BAEC42E65423, ____activeCount_3)); }
	inline int32_t get__activeCount_3() const { return ____activeCount_3; }
	inline int32_t* get_address_of__activeCount_3() { return &____activeCount_3; }
	inline void set__activeCount_3(int32_t value)
	{
		____activeCount_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMORYPOOLBASE_1_T09D148DA18FA8D291CDA958BE927BAEC42E65423_H
#ifndef MEMORYPOOLBASE_1_T2A7971FB11557D9305E361EDED8A39048A02BEB1_H
#define MEMORYPOOLBASE_1_T2A7971FB11557D9305E361EDED8A39048A02BEB1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.MemoryPoolBase`1<Zenject.SpaceFighter.Explosion>
struct  MemoryPoolBase_1_t2A7971FB11557D9305E361EDED8A39048A02BEB1  : public RuntimeObject
{
public:
	// System.Collections.Generic.Stack`1<TContract> Zenject.MemoryPoolBase`1::_inactiveItems
	Stack_1_tF195700F3001F99D6106CFA1D2972886772FB81A * ____inactiveItems_0;
	// Zenject.IFactory`1<TContract> Zenject.MemoryPoolBase`1::_factory
	RuntimeObject* ____factory_1;
	// Zenject.MemoryPoolSettings Zenject.MemoryPoolBase`1::_settings
	MemoryPoolSettings_tC33180DF15EF5B1103FB5191CFC0569BB3C74AA5 * ____settings_2;
	// System.Int32 Zenject.MemoryPoolBase`1::_activeCount
	int32_t ____activeCount_3;

public:
	inline static int32_t get_offset_of__inactiveItems_0() { return static_cast<int32_t>(offsetof(MemoryPoolBase_1_t2A7971FB11557D9305E361EDED8A39048A02BEB1, ____inactiveItems_0)); }
	inline Stack_1_tF195700F3001F99D6106CFA1D2972886772FB81A * get__inactiveItems_0() const { return ____inactiveItems_0; }
	inline Stack_1_tF195700F3001F99D6106CFA1D2972886772FB81A ** get_address_of__inactiveItems_0() { return &____inactiveItems_0; }
	inline void set__inactiveItems_0(Stack_1_tF195700F3001F99D6106CFA1D2972886772FB81A * value)
	{
		____inactiveItems_0 = value;
		Il2CppCodeGenWriteBarrier((&____inactiveItems_0), value);
	}

	inline static int32_t get_offset_of__factory_1() { return static_cast<int32_t>(offsetof(MemoryPoolBase_1_t2A7971FB11557D9305E361EDED8A39048A02BEB1, ____factory_1)); }
	inline RuntimeObject* get__factory_1() const { return ____factory_1; }
	inline RuntimeObject** get_address_of__factory_1() { return &____factory_1; }
	inline void set__factory_1(RuntimeObject* value)
	{
		____factory_1 = value;
		Il2CppCodeGenWriteBarrier((&____factory_1), value);
	}

	inline static int32_t get_offset_of__settings_2() { return static_cast<int32_t>(offsetof(MemoryPoolBase_1_t2A7971FB11557D9305E361EDED8A39048A02BEB1, ____settings_2)); }
	inline MemoryPoolSettings_tC33180DF15EF5B1103FB5191CFC0569BB3C74AA5 * get__settings_2() const { return ____settings_2; }
	inline MemoryPoolSettings_tC33180DF15EF5B1103FB5191CFC0569BB3C74AA5 ** get_address_of__settings_2() { return &____settings_2; }
	inline void set__settings_2(MemoryPoolSettings_tC33180DF15EF5B1103FB5191CFC0569BB3C74AA5 * value)
	{
		____settings_2 = value;
		Il2CppCodeGenWriteBarrier((&____settings_2), value);
	}

	inline static int32_t get_offset_of__activeCount_3() { return static_cast<int32_t>(offsetof(MemoryPoolBase_1_t2A7971FB11557D9305E361EDED8A39048A02BEB1, ____activeCount_3)); }
	inline int32_t get__activeCount_3() const { return ____activeCount_3; }
	inline int32_t* get_address_of__activeCount_3() { return &____activeCount_3; }
	inline void set__activeCount_3(int32_t value)
	{
		____activeCount_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMORYPOOLBASE_1_T2A7971FB11557D9305E361EDED8A39048A02BEB1_H
#ifndef POSTINJECTABLEINFO_T30E18115C14A086F02203F1FF8952C21A74F0CF1_H
#define POSTINJECTABLEINFO_T30E18115C14A086F02203F1FF8952C21A74F0CF1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.PostInjectableInfo
struct  PostInjectableInfo_t30E18115C14A086F02203F1FF8952C21A74F0CF1  : public RuntimeObject
{
public:
	// System.Reflection.MethodInfo Zenject.PostInjectableInfo::_methodInfo
	MethodInfo_t * ____methodInfo_0;
	// System.Collections.Generic.List`1<Zenject.InjectableInfo> Zenject.PostInjectableInfo::_injectableInfo
	List_1_t19FD687049A90924DCEA81F81C85555F15021A68 * ____injectableInfo_1;

public:
	inline static int32_t get_offset_of__methodInfo_0() { return static_cast<int32_t>(offsetof(PostInjectableInfo_t30E18115C14A086F02203F1FF8952C21A74F0CF1, ____methodInfo_0)); }
	inline MethodInfo_t * get__methodInfo_0() const { return ____methodInfo_0; }
	inline MethodInfo_t ** get_address_of__methodInfo_0() { return &____methodInfo_0; }
	inline void set__methodInfo_0(MethodInfo_t * value)
	{
		____methodInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&____methodInfo_0), value);
	}

	inline static int32_t get_offset_of__injectableInfo_1() { return static_cast<int32_t>(offsetof(PostInjectableInfo_t30E18115C14A086F02203F1FF8952C21A74F0CF1, ____injectableInfo_1)); }
	inline List_1_t19FD687049A90924DCEA81F81C85555F15021A68 * get__injectableInfo_1() const { return ____injectableInfo_1; }
	inline List_1_t19FD687049A90924DCEA81F81C85555F15021A68 ** get_address_of__injectableInfo_1() { return &____injectableInfo_1; }
	inline void set__injectableInfo_1(List_1_t19FD687049A90924DCEA81F81C85555F15021A68 * value)
	{
		____injectableInfo_1 = value;
		Il2CppCodeGenWriteBarrier((&____injectableInfo_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTINJECTABLEINFO_T30E18115C14A086F02203F1FF8952C21A74F0CF1_H
#ifndef SIGNALBASE_T1A82E76F19A3098BB4AF7EDD41C29E6CAD3F95B3_H
#define SIGNALBASE_T1A82E76F19A3098BB4AF7EDD41C29E6CAD3F95B3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SignalBase
struct  SignalBase_t1A82E76F19A3098BB4AF7EDD41C29E6CAD3F95B3  : public RuntimeObject
{
public:
	// Zenject.SignalManager Zenject.SignalBase::_manager
	SignalManager_t38E64F258F79FF33F3FDBB9423271BEFF03D7673 * ____manager_0;
	// Zenject.BindingId Zenject.SignalBase::<SignalId>k__BackingField
	BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA * ___U3CSignalIdU3Ek__BackingField_1;
	// Zenject.SignalSettings Zenject.SignalBase::<Settings>k__BackingField
	SignalSettings_tD753AA3D16CEA36F97CDAEC5428C5217F1EB6540 * ___U3CSettingsU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of__manager_0() { return static_cast<int32_t>(offsetof(SignalBase_t1A82E76F19A3098BB4AF7EDD41C29E6CAD3F95B3, ____manager_0)); }
	inline SignalManager_t38E64F258F79FF33F3FDBB9423271BEFF03D7673 * get__manager_0() const { return ____manager_0; }
	inline SignalManager_t38E64F258F79FF33F3FDBB9423271BEFF03D7673 ** get_address_of__manager_0() { return &____manager_0; }
	inline void set__manager_0(SignalManager_t38E64F258F79FF33F3FDBB9423271BEFF03D7673 * value)
	{
		____manager_0 = value;
		Il2CppCodeGenWriteBarrier((&____manager_0), value);
	}

	inline static int32_t get_offset_of_U3CSignalIdU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(SignalBase_t1A82E76F19A3098BB4AF7EDD41C29E6CAD3F95B3, ___U3CSignalIdU3Ek__BackingField_1)); }
	inline BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA * get_U3CSignalIdU3Ek__BackingField_1() const { return ___U3CSignalIdU3Ek__BackingField_1; }
	inline BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA ** get_address_of_U3CSignalIdU3Ek__BackingField_1() { return &___U3CSignalIdU3Ek__BackingField_1; }
	inline void set_U3CSignalIdU3Ek__BackingField_1(BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA * value)
	{
		___U3CSignalIdU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSignalIdU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CSettingsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(SignalBase_t1A82E76F19A3098BB4AF7EDD41C29E6CAD3F95B3, ___U3CSettingsU3Ek__BackingField_2)); }
	inline SignalSettings_tD753AA3D16CEA36F97CDAEC5428C5217F1EB6540 * get_U3CSettingsU3Ek__BackingField_2() const { return ___U3CSettingsU3Ek__BackingField_2; }
	inline SignalSettings_tD753AA3D16CEA36F97CDAEC5428C5217F1EB6540 ** get_address_of_U3CSettingsU3Ek__BackingField_2() { return &___U3CSettingsU3Ek__BackingField_2; }
	inline void set_U3CSettingsU3Ek__BackingField_2(SignalSettings_tD753AA3D16CEA36F97CDAEC5428C5217F1EB6540 * value)
	{
		___U3CSettingsU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSettingsU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGNALBASE_T1A82E76F19A3098BB4AF7EDD41C29E6CAD3F95B3_H
#ifndef AUDIOPLAYER_TD4C21B023CD7243688AE5FA1540F5C8ED94C49AA_H
#define AUDIOPLAYER_TD4C21B023CD7243688AE5FA1540F5C8ED94C49AA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SpaceFighter.AudioPlayer
struct  AudioPlayer_tD4C21B023CD7243688AE5FA1540F5C8ED94C49AA  : public RuntimeObject
{
public:
	// UnityEngine.Camera Zenject.SpaceFighter.AudioPlayer::_camera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ____camera_0;

public:
	inline static int32_t get_offset_of__camera_0() { return static_cast<int32_t>(offsetof(AudioPlayer_tD4C21B023CD7243688AE5FA1540F5C8ED94C49AA, ____camera_0)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get__camera_0() const { return ____camera_0; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of__camera_0() { return &____camera_0; }
	inline void set__camera_0(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		____camera_0 = value;
		Il2CppCodeGenWriteBarrier((&____camera_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOPLAYER_TD4C21B023CD7243688AE5FA1540F5C8ED94C49AA_H
#ifndef ENEMYCOMMONSETTINGS_TF1C89E066A85E57A479925E334EDE12BEAAE767B_H
#define ENEMYCOMMONSETTINGS_TF1C89E066A85E57A479925E334EDE12BEAAE767B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SpaceFighter.EnemyCommonSettings
struct  EnemyCommonSettings_tF1C89E066A85E57A479925E334EDE12BEAAE767B  : public RuntimeObject
{
public:
	// System.Single Zenject.SpaceFighter.EnemyCommonSettings::AttackDistance
	float ___AttackDistance_0;

public:
	inline static int32_t get_offset_of_AttackDistance_0() { return static_cast<int32_t>(offsetof(EnemyCommonSettings_tF1C89E066A85E57A479925E334EDE12BEAAE767B, ___AttackDistance_0)); }
	inline float get_AttackDistance_0() const { return ___AttackDistance_0; }
	inline float* get_address_of_AttackDistance_0() { return &___AttackDistance_0; }
	inline void set_AttackDistance_0(float value)
	{
		___AttackDistance_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENEMYCOMMONSETTINGS_TF1C89E066A85E57A479925E334EDE12BEAAE767B_H
#ifndef ENEMYDEATHHANDLER_T87BF96335A31C0712F7B9E80B22587838EE9B4C5_H
#define ENEMYDEATHHANDLER_T87BF96335A31C0712F7B9E80B22587838EE9B4C5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SpaceFighter.EnemyDeathHandler
struct  EnemyDeathHandler_t87BF96335A31C0712F7B9E80B22587838EE9B4C5  : public RuntimeObject
{
public:
	// Zenject.SpaceFighter.EnemyKilledSignal Zenject.SpaceFighter.EnemyDeathHandler::_enemyKilledSignal
	EnemyKilledSignal_t5291603135B3B8F73E6ED1317431EE5091D78DC8 * ____enemyKilledSignal_0;
	// Zenject.SpaceFighter.EnemyFacade_Pool Zenject.SpaceFighter.EnemyDeathHandler::_selfFactory
	Pool_t735C24F8E4685CE9F8814136D7E4B3548882F387 * ____selfFactory_1;
	// Zenject.SpaceFighter.EnemyDeathHandler_Settings Zenject.SpaceFighter.EnemyDeathHandler::_settings
	Settings_tCDA69B4C2B2FC2E7E5DC396996019836DE3ED1E2 * ____settings_2;
	// Zenject.SpaceFighter.Explosion_Pool Zenject.SpaceFighter.EnemyDeathHandler::_explosionPool
	Pool_tF6D222503E0FF0BBA73DDF77321A0C8B6CA1D7C7 * ____explosionPool_3;
	// Zenject.SpaceFighter.IAudioPlayer Zenject.SpaceFighter.EnemyDeathHandler::_audioPlayer
	RuntimeObject* ____audioPlayer_4;
	// Zenject.SpaceFighter.Enemy Zenject.SpaceFighter.EnemyDeathHandler::_enemy
	Enemy_tC40CA91F878B23445D874D13243C2824315DE3D2 * ____enemy_5;
	// Zenject.SpaceFighter.EnemyFacade Zenject.SpaceFighter.EnemyDeathHandler::_facade
	EnemyFacade_t1D5DD8AEDE16BD5F9C2C32C4C0AB954CE1811FC1 * ____facade_6;

public:
	inline static int32_t get_offset_of__enemyKilledSignal_0() { return static_cast<int32_t>(offsetof(EnemyDeathHandler_t87BF96335A31C0712F7B9E80B22587838EE9B4C5, ____enemyKilledSignal_0)); }
	inline EnemyKilledSignal_t5291603135B3B8F73E6ED1317431EE5091D78DC8 * get__enemyKilledSignal_0() const { return ____enemyKilledSignal_0; }
	inline EnemyKilledSignal_t5291603135B3B8F73E6ED1317431EE5091D78DC8 ** get_address_of__enemyKilledSignal_0() { return &____enemyKilledSignal_0; }
	inline void set__enemyKilledSignal_0(EnemyKilledSignal_t5291603135B3B8F73E6ED1317431EE5091D78DC8 * value)
	{
		____enemyKilledSignal_0 = value;
		Il2CppCodeGenWriteBarrier((&____enemyKilledSignal_0), value);
	}

	inline static int32_t get_offset_of__selfFactory_1() { return static_cast<int32_t>(offsetof(EnemyDeathHandler_t87BF96335A31C0712F7B9E80B22587838EE9B4C5, ____selfFactory_1)); }
	inline Pool_t735C24F8E4685CE9F8814136D7E4B3548882F387 * get__selfFactory_1() const { return ____selfFactory_1; }
	inline Pool_t735C24F8E4685CE9F8814136D7E4B3548882F387 ** get_address_of__selfFactory_1() { return &____selfFactory_1; }
	inline void set__selfFactory_1(Pool_t735C24F8E4685CE9F8814136D7E4B3548882F387 * value)
	{
		____selfFactory_1 = value;
		Il2CppCodeGenWriteBarrier((&____selfFactory_1), value);
	}

	inline static int32_t get_offset_of__settings_2() { return static_cast<int32_t>(offsetof(EnemyDeathHandler_t87BF96335A31C0712F7B9E80B22587838EE9B4C5, ____settings_2)); }
	inline Settings_tCDA69B4C2B2FC2E7E5DC396996019836DE3ED1E2 * get__settings_2() const { return ____settings_2; }
	inline Settings_tCDA69B4C2B2FC2E7E5DC396996019836DE3ED1E2 ** get_address_of__settings_2() { return &____settings_2; }
	inline void set__settings_2(Settings_tCDA69B4C2B2FC2E7E5DC396996019836DE3ED1E2 * value)
	{
		____settings_2 = value;
		Il2CppCodeGenWriteBarrier((&____settings_2), value);
	}

	inline static int32_t get_offset_of__explosionPool_3() { return static_cast<int32_t>(offsetof(EnemyDeathHandler_t87BF96335A31C0712F7B9E80B22587838EE9B4C5, ____explosionPool_3)); }
	inline Pool_tF6D222503E0FF0BBA73DDF77321A0C8B6CA1D7C7 * get__explosionPool_3() const { return ____explosionPool_3; }
	inline Pool_tF6D222503E0FF0BBA73DDF77321A0C8B6CA1D7C7 ** get_address_of__explosionPool_3() { return &____explosionPool_3; }
	inline void set__explosionPool_3(Pool_tF6D222503E0FF0BBA73DDF77321A0C8B6CA1D7C7 * value)
	{
		____explosionPool_3 = value;
		Il2CppCodeGenWriteBarrier((&____explosionPool_3), value);
	}

	inline static int32_t get_offset_of__audioPlayer_4() { return static_cast<int32_t>(offsetof(EnemyDeathHandler_t87BF96335A31C0712F7B9E80B22587838EE9B4C5, ____audioPlayer_4)); }
	inline RuntimeObject* get__audioPlayer_4() const { return ____audioPlayer_4; }
	inline RuntimeObject** get_address_of__audioPlayer_4() { return &____audioPlayer_4; }
	inline void set__audioPlayer_4(RuntimeObject* value)
	{
		____audioPlayer_4 = value;
		Il2CppCodeGenWriteBarrier((&____audioPlayer_4), value);
	}

	inline static int32_t get_offset_of__enemy_5() { return static_cast<int32_t>(offsetof(EnemyDeathHandler_t87BF96335A31C0712F7B9E80B22587838EE9B4C5, ____enemy_5)); }
	inline Enemy_tC40CA91F878B23445D874D13243C2824315DE3D2 * get__enemy_5() const { return ____enemy_5; }
	inline Enemy_tC40CA91F878B23445D874D13243C2824315DE3D2 ** get_address_of__enemy_5() { return &____enemy_5; }
	inline void set__enemy_5(Enemy_tC40CA91F878B23445D874D13243C2824315DE3D2 * value)
	{
		____enemy_5 = value;
		Il2CppCodeGenWriteBarrier((&____enemy_5), value);
	}

	inline static int32_t get_offset_of__facade_6() { return static_cast<int32_t>(offsetof(EnemyDeathHandler_t87BF96335A31C0712F7B9E80B22587838EE9B4C5, ____facade_6)); }
	inline EnemyFacade_t1D5DD8AEDE16BD5F9C2C32C4C0AB954CE1811FC1 * get__facade_6() const { return ____facade_6; }
	inline EnemyFacade_t1D5DD8AEDE16BD5F9C2C32C4C0AB954CE1811FC1 ** get_address_of__facade_6() { return &____facade_6; }
	inline void set__facade_6(EnemyFacade_t1D5DD8AEDE16BD5F9C2C32C4C0AB954CE1811FC1 * value)
	{
		____facade_6 = value;
		Il2CppCodeGenWriteBarrier((&____facade_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENEMYDEATHHANDLER_T87BF96335A31C0712F7B9E80B22587838EE9B4C5_H
#ifndef SETTINGS_TCDA69B4C2B2FC2E7E5DC396996019836DE3ED1E2_H
#define SETTINGS_TCDA69B4C2B2FC2E7E5DC396996019836DE3ED1E2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SpaceFighter.EnemyDeathHandler_Settings
struct  Settings_tCDA69B4C2B2FC2E7E5DC396996019836DE3ED1E2  : public RuntimeObject
{
public:
	// UnityEngine.AudioClip Zenject.SpaceFighter.EnemyDeathHandler_Settings::DeathSound
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___DeathSound_0;
	// System.Single Zenject.SpaceFighter.EnemyDeathHandler_Settings::DeathSoundVolume
	float ___DeathSoundVolume_1;

public:
	inline static int32_t get_offset_of_DeathSound_0() { return static_cast<int32_t>(offsetof(Settings_tCDA69B4C2B2FC2E7E5DC396996019836DE3ED1E2, ___DeathSound_0)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_DeathSound_0() const { return ___DeathSound_0; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_DeathSound_0() { return &___DeathSound_0; }
	inline void set_DeathSound_0(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___DeathSound_0 = value;
		Il2CppCodeGenWriteBarrier((&___DeathSound_0), value);
	}

	inline static int32_t get_offset_of_DeathSoundVolume_1() { return static_cast<int32_t>(offsetof(Settings_tCDA69B4C2B2FC2E7E5DC396996019836DE3ED1E2, ___DeathSoundVolume_1)); }
	inline float get_DeathSoundVolume_1() const { return ___DeathSoundVolume_1; }
	inline float* get_address_of_DeathSoundVolume_1() { return &___DeathSoundVolume_1; }
	inline void set_DeathSoundVolume_1(float value)
	{
		___DeathSoundVolume_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETTINGS_TCDA69B4C2B2FC2E7E5DC396996019836DE3ED1E2_H
#ifndef SETTINGS_TD2755B3F26D35678AF75CBB0C5BF9C282206FF91_H
#define SETTINGS_TD2755B3F26D35678AF75CBB0C5BF9C282206FF91_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SpaceFighter.EnemyInstaller_Settings
struct  Settings_tD2755B3F26D35678AF75CBB0C5BF9C282206FF91  : public RuntimeObject
{
public:
	// UnityEngine.GameObject Zenject.SpaceFighter.EnemyInstaller_Settings::RootObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___RootObject_0;
	// UnityEngine.Rigidbody Zenject.SpaceFighter.EnemyInstaller_Settings::Rigidbody
	Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * ___Rigidbody_1;
	// UnityEngine.Collider Zenject.SpaceFighter.EnemyInstaller_Settings::Collider
	Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * ___Collider_2;
	// UnityEngine.MeshRenderer Zenject.SpaceFighter.EnemyInstaller_Settings::Renderer
	MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * ___Renderer_3;

public:
	inline static int32_t get_offset_of_RootObject_0() { return static_cast<int32_t>(offsetof(Settings_tD2755B3F26D35678AF75CBB0C5BF9C282206FF91, ___RootObject_0)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_RootObject_0() const { return ___RootObject_0; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_RootObject_0() { return &___RootObject_0; }
	inline void set_RootObject_0(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___RootObject_0 = value;
		Il2CppCodeGenWriteBarrier((&___RootObject_0), value);
	}

	inline static int32_t get_offset_of_Rigidbody_1() { return static_cast<int32_t>(offsetof(Settings_tD2755B3F26D35678AF75CBB0C5BF9C282206FF91, ___Rigidbody_1)); }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * get_Rigidbody_1() const { return ___Rigidbody_1; }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 ** get_address_of_Rigidbody_1() { return &___Rigidbody_1; }
	inline void set_Rigidbody_1(Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * value)
	{
		___Rigidbody_1 = value;
		Il2CppCodeGenWriteBarrier((&___Rigidbody_1), value);
	}

	inline static int32_t get_offset_of_Collider_2() { return static_cast<int32_t>(offsetof(Settings_tD2755B3F26D35678AF75CBB0C5BF9C282206FF91, ___Collider_2)); }
	inline Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * get_Collider_2() const { return ___Collider_2; }
	inline Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF ** get_address_of_Collider_2() { return &___Collider_2; }
	inline void set_Collider_2(Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * value)
	{
		___Collider_2 = value;
		Il2CppCodeGenWriteBarrier((&___Collider_2), value);
	}

	inline static int32_t get_offset_of_Renderer_3() { return static_cast<int32_t>(offsetof(Settings_tD2755B3F26D35678AF75CBB0C5BF9C282206FF91, ___Renderer_3)); }
	inline MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * get_Renderer_3() const { return ___Renderer_3; }
	inline MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED ** get_address_of_Renderer_3() { return &___Renderer_3; }
	inline void set_Renderer_3(MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * value)
	{
		___Renderer_3 = value;
		Il2CppCodeGenWriteBarrier((&___Renderer_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETTINGS_TD2755B3F26D35678AF75CBB0C5BF9C282206FF91_H
#ifndef ENEMYROTATIONHANDLER_T20FB73A75100F454B07CC9BB0C3E6255706F896B_H
#define ENEMYROTATIONHANDLER_T20FB73A75100F454B07CC9BB0C3E6255706F896B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SpaceFighter.EnemyRotationHandler
struct  EnemyRotationHandler_t20FB73A75100F454B07CC9BB0C3E6255706F896B  : public RuntimeObject
{
public:
	// Zenject.SpaceFighter.EnemyRotationHandler_Settings Zenject.SpaceFighter.EnemyRotationHandler::_settings
	Settings_t3480554AD22B7730290AB95863B9EDC48610BCE9 * ____settings_0;
	// Zenject.SpaceFighter.Enemy Zenject.SpaceFighter.EnemyRotationHandler::_enemy
	Enemy_tC40CA91F878B23445D874D13243C2824315DE3D2 * ____enemy_1;

public:
	inline static int32_t get_offset_of__settings_0() { return static_cast<int32_t>(offsetof(EnemyRotationHandler_t20FB73A75100F454B07CC9BB0C3E6255706F896B, ____settings_0)); }
	inline Settings_t3480554AD22B7730290AB95863B9EDC48610BCE9 * get__settings_0() const { return ____settings_0; }
	inline Settings_t3480554AD22B7730290AB95863B9EDC48610BCE9 ** get_address_of__settings_0() { return &____settings_0; }
	inline void set__settings_0(Settings_t3480554AD22B7730290AB95863B9EDC48610BCE9 * value)
	{
		____settings_0 = value;
		Il2CppCodeGenWriteBarrier((&____settings_0), value);
	}

	inline static int32_t get_offset_of__enemy_1() { return static_cast<int32_t>(offsetof(EnemyRotationHandler_t20FB73A75100F454B07CC9BB0C3E6255706F896B, ____enemy_1)); }
	inline Enemy_tC40CA91F878B23445D874D13243C2824315DE3D2 * get__enemy_1() const { return ____enemy_1; }
	inline Enemy_tC40CA91F878B23445D874D13243C2824315DE3D2 ** get_address_of__enemy_1() { return &____enemy_1; }
	inline void set__enemy_1(Enemy_tC40CA91F878B23445D874D13243C2824315DE3D2 * value)
	{
		____enemy_1 = value;
		Il2CppCodeGenWriteBarrier((&____enemy_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENEMYROTATIONHANDLER_T20FB73A75100F454B07CC9BB0C3E6255706F896B_H
#ifndef SETTINGS_T3480554AD22B7730290AB95863B9EDC48610BCE9_H
#define SETTINGS_T3480554AD22B7730290AB95863B9EDC48610BCE9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SpaceFighter.EnemyRotationHandler_Settings
struct  Settings_t3480554AD22B7730290AB95863B9EDC48610BCE9  : public RuntimeObject
{
public:
	// System.Single Zenject.SpaceFighter.EnemyRotationHandler_Settings::TurnSpeed
	float ___TurnSpeed_0;

public:
	inline static int32_t get_offset_of_TurnSpeed_0() { return static_cast<int32_t>(offsetof(Settings_t3480554AD22B7730290AB95863B9EDC48610BCE9, ___TurnSpeed_0)); }
	inline float get_TurnSpeed_0() const { return ___TurnSpeed_0; }
	inline float* get_address_of_TurnSpeed_0() { return &___TurnSpeed_0; }
	inline void set_TurnSpeed_0(float value)
	{
		___TurnSpeed_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETTINGS_T3480554AD22B7730290AB95863B9EDC48610BCE9_H
#ifndef ENEMYSPAWNER_T7844A5EBF2D0675CE13484749052C3F440652F6A_H
#define ENEMYSPAWNER_T7844A5EBF2D0675CE13484749052C3F440652F6A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SpaceFighter.EnemySpawner
struct  EnemySpawner_t7844A5EBF2D0675CE13484749052C3F440652F6A  : public RuntimeObject
{
public:
	// Zenject.SpaceFighter.LevelBoundary Zenject.SpaceFighter.EnemySpawner::_levelBoundary
	LevelBoundary_t8E706FF3A27DD22A30FE8E79EF745B6B7D78577B * ____levelBoundary_0;
	// Zenject.SpaceFighter.EnemyFacade_Pool Zenject.SpaceFighter.EnemySpawner::_enemyFactory
	Pool_t735C24F8E4685CE9F8814136D7E4B3548882F387 * ____enemyFactory_1;
	// Zenject.SpaceFighter.EnemySpawner_Settings Zenject.SpaceFighter.EnemySpawner::_settings
	Settings_tD4EAF29A0539BDE0CFDC096ED188196E8DD744C2 * ____settings_2;
	// Zenject.SpaceFighter.EnemyKilledSignal Zenject.SpaceFighter.EnemySpawner::_enemyKilledSignal
	EnemyKilledSignal_t5291603135B3B8F73E6ED1317431EE5091D78DC8 * ____enemyKilledSignal_3;
	// System.Single Zenject.SpaceFighter.EnemySpawner::_desiredNumEnemies
	float ____desiredNumEnemies_4;
	// System.Int32 Zenject.SpaceFighter.EnemySpawner::_enemyCount
	int32_t ____enemyCount_5;
	// System.Single Zenject.SpaceFighter.EnemySpawner::_lastSpawnTime
	float ____lastSpawnTime_6;

public:
	inline static int32_t get_offset_of__levelBoundary_0() { return static_cast<int32_t>(offsetof(EnemySpawner_t7844A5EBF2D0675CE13484749052C3F440652F6A, ____levelBoundary_0)); }
	inline LevelBoundary_t8E706FF3A27DD22A30FE8E79EF745B6B7D78577B * get__levelBoundary_0() const { return ____levelBoundary_0; }
	inline LevelBoundary_t8E706FF3A27DD22A30FE8E79EF745B6B7D78577B ** get_address_of__levelBoundary_0() { return &____levelBoundary_0; }
	inline void set__levelBoundary_0(LevelBoundary_t8E706FF3A27DD22A30FE8E79EF745B6B7D78577B * value)
	{
		____levelBoundary_0 = value;
		Il2CppCodeGenWriteBarrier((&____levelBoundary_0), value);
	}

	inline static int32_t get_offset_of__enemyFactory_1() { return static_cast<int32_t>(offsetof(EnemySpawner_t7844A5EBF2D0675CE13484749052C3F440652F6A, ____enemyFactory_1)); }
	inline Pool_t735C24F8E4685CE9F8814136D7E4B3548882F387 * get__enemyFactory_1() const { return ____enemyFactory_1; }
	inline Pool_t735C24F8E4685CE9F8814136D7E4B3548882F387 ** get_address_of__enemyFactory_1() { return &____enemyFactory_1; }
	inline void set__enemyFactory_1(Pool_t735C24F8E4685CE9F8814136D7E4B3548882F387 * value)
	{
		____enemyFactory_1 = value;
		Il2CppCodeGenWriteBarrier((&____enemyFactory_1), value);
	}

	inline static int32_t get_offset_of__settings_2() { return static_cast<int32_t>(offsetof(EnemySpawner_t7844A5EBF2D0675CE13484749052C3F440652F6A, ____settings_2)); }
	inline Settings_tD4EAF29A0539BDE0CFDC096ED188196E8DD744C2 * get__settings_2() const { return ____settings_2; }
	inline Settings_tD4EAF29A0539BDE0CFDC096ED188196E8DD744C2 ** get_address_of__settings_2() { return &____settings_2; }
	inline void set__settings_2(Settings_tD4EAF29A0539BDE0CFDC096ED188196E8DD744C2 * value)
	{
		____settings_2 = value;
		Il2CppCodeGenWriteBarrier((&____settings_2), value);
	}

	inline static int32_t get_offset_of__enemyKilledSignal_3() { return static_cast<int32_t>(offsetof(EnemySpawner_t7844A5EBF2D0675CE13484749052C3F440652F6A, ____enemyKilledSignal_3)); }
	inline EnemyKilledSignal_t5291603135B3B8F73E6ED1317431EE5091D78DC8 * get__enemyKilledSignal_3() const { return ____enemyKilledSignal_3; }
	inline EnemyKilledSignal_t5291603135B3B8F73E6ED1317431EE5091D78DC8 ** get_address_of__enemyKilledSignal_3() { return &____enemyKilledSignal_3; }
	inline void set__enemyKilledSignal_3(EnemyKilledSignal_t5291603135B3B8F73E6ED1317431EE5091D78DC8 * value)
	{
		____enemyKilledSignal_3 = value;
		Il2CppCodeGenWriteBarrier((&____enemyKilledSignal_3), value);
	}

	inline static int32_t get_offset_of__desiredNumEnemies_4() { return static_cast<int32_t>(offsetof(EnemySpawner_t7844A5EBF2D0675CE13484749052C3F440652F6A, ____desiredNumEnemies_4)); }
	inline float get__desiredNumEnemies_4() const { return ____desiredNumEnemies_4; }
	inline float* get_address_of__desiredNumEnemies_4() { return &____desiredNumEnemies_4; }
	inline void set__desiredNumEnemies_4(float value)
	{
		____desiredNumEnemies_4 = value;
	}

	inline static int32_t get_offset_of__enemyCount_5() { return static_cast<int32_t>(offsetof(EnemySpawner_t7844A5EBF2D0675CE13484749052C3F440652F6A, ____enemyCount_5)); }
	inline int32_t get__enemyCount_5() const { return ____enemyCount_5; }
	inline int32_t* get_address_of__enemyCount_5() { return &____enemyCount_5; }
	inline void set__enemyCount_5(int32_t value)
	{
		____enemyCount_5 = value;
	}

	inline static int32_t get_offset_of__lastSpawnTime_6() { return static_cast<int32_t>(offsetof(EnemySpawner_t7844A5EBF2D0675CE13484749052C3F440652F6A, ____lastSpawnTime_6)); }
	inline float get__lastSpawnTime_6() const { return ____lastSpawnTime_6; }
	inline float* get_address_of__lastSpawnTime_6() { return &____lastSpawnTime_6; }
	inline void set__lastSpawnTime_6(float value)
	{
		____lastSpawnTime_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENEMYSPAWNER_T7844A5EBF2D0675CE13484749052C3F440652F6A_H
#ifndef SETTINGS_TD4EAF29A0539BDE0CFDC096ED188196E8DD744C2_H
#define SETTINGS_TD4EAF29A0539BDE0CFDC096ED188196E8DD744C2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SpaceFighter.EnemySpawner_Settings
struct  Settings_tD4EAF29A0539BDE0CFDC096ED188196E8DD744C2  : public RuntimeObject
{
public:
	// System.Single Zenject.SpaceFighter.EnemySpawner_Settings::SpeedMin
	float ___SpeedMin_0;
	// System.Single Zenject.SpaceFighter.EnemySpawner_Settings::SpeedMax
	float ___SpeedMax_1;
	// System.Single Zenject.SpaceFighter.EnemySpawner_Settings::AccuracyMin
	float ___AccuracyMin_2;
	// System.Single Zenject.SpaceFighter.EnemySpawner_Settings::AccuracyMax
	float ___AccuracyMax_3;
	// System.Single Zenject.SpaceFighter.EnemySpawner_Settings::NumEnemiesIncreaseRate
	float ___NumEnemiesIncreaseRate_4;
	// System.Single Zenject.SpaceFighter.EnemySpawner_Settings::NumEnemiesStartAmount
	float ___NumEnemiesStartAmount_5;
	// System.Single Zenject.SpaceFighter.EnemySpawner_Settings::MinDelayBetweenSpawns
	float ___MinDelayBetweenSpawns_6;

public:
	inline static int32_t get_offset_of_SpeedMin_0() { return static_cast<int32_t>(offsetof(Settings_tD4EAF29A0539BDE0CFDC096ED188196E8DD744C2, ___SpeedMin_0)); }
	inline float get_SpeedMin_0() const { return ___SpeedMin_0; }
	inline float* get_address_of_SpeedMin_0() { return &___SpeedMin_0; }
	inline void set_SpeedMin_0(float value)
	{
		___SpeedMin_0 = value;
	}

	inline static int32_t get_offset_of_SpeedMax_1() { return static_cast<int32_t>(offsetof(Settings_tD4EAF29A0539BDE0CFDC096ED188196E8DD744C2, ___SpeedMax_1)); }
	inline float get_SpeedMax_1() const { return ___SpeedMax_1; }
	inline float* get_address_of_SpeedMax_1() { return &___SpeedMax_1; }
	inline void set_SpeedMax_1(float value)
	{
		___SpeedMax_1 = value;
	}

	inline static int32_t get_offset_of_AccuracyMin_2() { return static_cast<int32_t>(offsetof(Settings_tD4EAF29A0539BDE0CFDC096ED188196E8DD744C2, ___AccuracyMin_2)); }
	inline float get_AccuracyMin_2() const { return ___AccuracyMin_2; }
	inline float* get_address_of_AccuracyMin_2() { return &___AccuracyMin_2; }
	inline void set_AccuracyMin_2(float value)
	{
		___AccuracyMin_2 = value;
	}

	inline static int32_t get_offset_of_AccuracyMax_3() { return static_cast<int32_t>(offsetof(Settings_tD4EAF29A0539BDE0CFDC096ED188196E8DD744C2, ___AccuracyMax_3)); }
	inline float get_AccuracyMax_3() const { return ___AccuracyMax_3; }
	inline float* get_address_of_AccuracyMax_3() { return &___AccuracyMax_3; }
	inline void set_AccuracyMax_3(float value)
	{
		___AccuracyMax_3 = value;
	}

	inline static int32_t get_offset_of_NumEnemiesIncreaseRate_4() { return static_cast<int32_t>(offsetof(Settings_tD4EAF29A0539BDE0CFDC096ED188196E8DD744C2, ___NumEnemiesIncreaseRate_4)); }
	inline float get_NumEnemiesIncreaseRate_4() const { return ___NumEnemiesIncreaseRate_4; }
	inline float* get_address_of_NumEnemiesIncreaseRate_4() { return &___NumEnemiesIncreaseRate_4; }
	inline void set_NumEnemiesIncreaseRate_4(float value)
	{
		___NumEnemiesIncreaseRate_4 = value;
	}

	inline static int32_t get_offset_of_NumEnemiesStartAmount_5() { return static_cast<int32_t>(offsetof(Settings_tD4EAF29A0539BDE0CFDC096ED188196E8DD744C2, ___NumEnemiesStartAmount_5)); }
	inline float get_NumEnemiesStartAmount_5() const { return ___NumEnemiesStartAmount_5; }
	inline float* get_address_of_NumEnemiesStartAmount_5() { return &___NumEnemiesStartAmount_5; }
	inline void set_NumEnemiesStartAmount_5(float value)
	{
		___NumEnemiesStartAmount_5 = value;
	}

	inline static int32_t get_offset_of_MinDelayBetweenSpawns_6() { return static_cast<int32_t>(offsetof(Settings_tD4EAF29A0539BDE0CFDC096ED188196E8DD744C2, ___MinDelayBetweenSpawns_6)); }
	inline float get_MinDelayBetweenSpawns_6() const { return ___MinDelayBetweenSpawns_6; }
	inline float* get_address_of_MinDelayBetweenSpawns_6() { return &___MinDelayBetweenSpawns_6; }
	inline void set_MinDelayBetweenSpawns_6(float value)
	{
		___MinDelayBetweenSpawns_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETTINGS_TD4EAF29A0539BDE0CFDC096ED188196E8DD744C2_H
#ifndef ENEMYSTATEATTACK_TC0E53565C8E9746FBE18E7521390DB4D26D05A9A_H
#define ENEMYSTATEATTACK_TC0E53565C8E9746FBE18E7521390DB4D26D05A9A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SpaceFighter.EnemyStateAttack
struct  EnemyStateAttack_tC0E53565C8E9746FBE18E7521390DB4D26D05A9A  : public RuntimeObject
{
public:
	// Zenject.SpaceFighter.EnemyCommonSettings Zenject.SpaceFighter.EnemyStateAttack::_commonSettings
	EnemyCommonSettings_tF1C89E066A85E57A479925E334EDE12BEAAE767B * ____commonSettings_0;
	// Zenject.SpaceFighter.IAudioPlayer Zenject.SpaceFighter.EnemyStateAttack::_audioPlayer
	RuntimeObject* ____audioPlayer_1;
	// Zenject.SpaceFighter.EnemyTunables Zenject.SpaceFighter.EnemyStateAttack::_tunables
	EnemyTunables_tD9997725AFF614521DD7088FFF23C1D8DD06DE18 * ____tunables_2;
	// Zenject.SpaceFighter.EnemyStateManager Zenject.SpaceFighter.EnemyStateAttack::_stateManager
	EnemyStateManager_tD3E2662DA02D660CB7356CE3563F928B0E220C9F * ____stateManager_3;
	// Zenject.SpaceFighter.IPlayer Zenject.SpaceFighter.EnemyStateAttack::_player
	RuntimeObject* ____player_4;
	// Zenject.SpaceFighter.EnemyStateAttack_Settings Zenject.SpaceFighter.EnemyStateAttack::_settings
	Settings_t3ABF5D501B5AFEE2903D5F846E8EC24317DB3528 * ____settings_5;
	// Zenject.SpaceFighter.Enemy Zenject.SpaceFighter.EnemyStateAttack::_enemy
	Enemy_tC40CA91F878B23445D874D13243C2824315DE3D2 * ____enemy_6;
	// Zenject.SpaceFighter.Bullet_Pool Zenject.SpaceFighter.EnemyStateAttack::_bulletPool
	Pool_t28D3C271D1214C1C919D02B9C38A6A5698F231F1 * ____bulletPool_7;
	// System.Single Zenject.SpaceFighter.EnemyStateAttack::_lastShootTime
	float ____lastShootTime_8;
	// System.Boolean Zenject.SpaceFighter.EnemyStateAttack::_strafeRight
	bool ____strafeRight_9;
	// System.Single Zenject.SpaceFighter.EnemyStateAttack::_lastStrafeChangeTime
	float ____lastStrafeChangeTime_10;

public:
	inline static int32_t get_offset_of__commonSettings_0() { return static_cast<int32_t>(offsetof(EnemyStateAttack_tC0E53565C8E9746FBE18E7521390DB4D26D05A9A, ____commonSettings_0)); }
	inline EnemyCommonSettings_tF1C89E066A85E57A479925E334EDE12BEAAE767B * get__commonSettings_0() const { return ____commonSettings_0; }
	inline EnemyCommonSettings_tF1C89E066A85E57A479925E334EDE12BEAAE767B ** get_address_of__commonSettings_0() { return &____commonSettings_0; }
	inline void set__commonSettings_0(EnemyCommonSettings_tF1C89E066A85E57A479925E334EDE12BEAAE767B * value)
	{
		____commonSettings_0 = value;
		Il2CppCodeGenWriteBarrier((&____commonSettings_0), value);
	}

	inline static int32_t get_offset_of__audioPlayer_1() { return static_cast<int32_t>(offsetof(EnemyStateAttack_tC0E53565C8E9746FBE18E7521390DB4D26D05A9A, ____audioPlayer_1)); }
	inline RuntimeObject* get__audioPlayer_1() const { return ____audioPlayer_1; }
	inline RuntimeObject** get_address_of__audioPlayer_1() { return &____audioPlayer_1; }
	inline void set__audioPlayer_1(RuntimeObject* value)
	{
		____audioPlayer_1 = value;
		Il2CppCodeGenWriteBarrier((&____audioPlayer_1), value);
	}

	inline static int32_t get_offset_of__tunables_2() { return static_cast<int32_t>(offsetof(EnemyStateAttack_tC0E53565C8E9746FBE18E7521390DB4D26D05A9A, ____tunables_2)); }
	inline EnemyTunables_tD9997725AFF614521DD7088FFF23C1D8DD06DE18 * get__tunables_2() const { return ____tunables_2; }
	inline EnemyTunables_tD9997725AFF614521DD7088FFF23C1D8DD06DE18 ** get_address_of__tunables_2() { return &____tunables_2; }
	inline void set__tunables_2(EnemyTunables_tD9997725AFF614521DD7088FFF23C1D8DD06DE18 * value)
	{
		____tunables_2 = value;
		Il2CppCodeGenWriteBarrier((&____tunables_2), value);
	}

	inline static int32_t get_offset_of__stateManager_3() { return static_cast<int32_t>(offsetof(EnemyStateAttack_tC0E53565C8E9746FBE18E7521390DB4D26D05A9A, ____stateManager_3)); }
	inline EnemyStateManager_tD3E2662DA02D660CB7356CE3563F928B0E220C9F * get__stateManager_3() const { return ____stateManager_3; }
	inline EnemyStateManager_tD3E2662DA02D660CB7356CE3563F928B0E220C9F ** get_address_of__stateManager_3() { return &____stateManager_3; }
	inline void set__stateManager_3(EnemyStateManager_tD3E2662DA02D660CB7356CE3563F928B0E220C9F * value)
	{
		____stateManager_3 = value;
		Il2CppCodeGenWriteBarrier((&____stateManager_3), value);
	}

	inline static int32_t get_offset_of__player_4() { return static_cast<int32_t>(offsetof(EnemyStateAttack_tC0E53565C8E9746FBE18E7521390DB4D26D05A9A, ____player_4)); }
	inline RuntimeObject* get__player_4() const { return ____player_4; }
	inline RuntimeObject** get_address_of__player_4() { return &____player_4; }
	inline void set__player_4(RuntimeObject* value)
	{
		____player_4 = value;
		Il2CppCodeGenWriteBarrier((&____player_4), value);
	}

	inline static int32_t get_offset_of__settings_5() { return static_cast<int32_t>(offsetof(EnemyStateAttack_tC0E53565C8E9746FBE18E7521390DB4D26D05A9A, ____settings_5)); }
	inline Settings_t3ABF5D501B5AFEE2903D5F846E8EC24317DB3528 * get__settings_5() const { return ____settings_5; }
	inline Settings_t3ABF5D501B5AFEE2903D5F846E8EC24317DB3528 ** get_address_of__settings_5() { return &____settings_5; }
	inline void set__settings_5(Settings_t3ABF5D501B5AFEE2903D5F846E8EC24317DB3528 * value)
	{
		____settings_5 = value;
		Il2CppCodeGenWriteBarrier((&____settings_5), value);
	}

	inline static int32_t get_offset_of__enemy_6() { return static_cast<int32_t>(offsetof(EnemyStateAttack_tC0E53565C8E9746FBE18E7521390DB4D26D05A9A, ____enemy_6)); }
	inline Enemy_tC40CA91F878B23445D874D13243C2824315DE3D2 * get__enemy_6() const { return ____enemy_6; }
	inline Enemy_tC40CA91F878B23445D874D13243C2824315DE3D2 ** get_address_of__enemy_6() { return &____enemy_6; }
	inline void set__enemy_6(Enemy_tC40CA91F878B23445D874D13243C2824315DE3D2 * value)
	{
		____enemy_6 = value;
		Il2CppCodeGenWriteBarrier((&____enemy_6), value);
	}

	inline static int32_t get_offset_of__bulletPool_7() { return static_cast<int32_t>(offsetof(EnemyStateAttack_tC0E53565C8E9746FBE18E7521390DB4D26D05A9A, ____bulletPool_7)); }
	inline Pool_t28D3C271D1214C1C919D02B9C38A6A5698F231F1 * get__bulletPool_7() const { return ____bulletPool_7; }
	inline Pool_t28D3C271D1214C1C919D02B9C38A6A5698F231F1 ** get_address_of__bulletPool_7() { return &____bulletPool_7; }
	inline void set__bulletPool_7(Pool_t28D3C271D1214C1C919D02B9C38A6A5698F231F1 * value)
	{
		____bulletPool_7 = value;
		Il2CppCodeGenWriteBarrier((&____bulletPool_7), value);
	}

	inline static int32_t get_offset_of__lastShootTime_8() { return static_cast<int32_t>(offsetof(EnemyStateAttack_tC0E53565C8E9746FBE18E7521390DB4D26D05A9A, ____lastShootTime_8)); }
	inline float get__lastShootTime_8() const { return ____lastShootTime_8; }
	inline float* get_address_of__lastShootTime_8() { return &____lastShootTime_8; }
	inline void set__lastShootTime_8(float value)
	{
		____lastShootTime_8 = value;
	}

	inline static int32_t get_offset_of__strafeRight_9() { return static_cast<int32_t>(offsetof(EnemyStateAttack_tC0E53565C8E9746FBE18E7521390DB4D26D05A9A, ____strafeRight_9)); }
	inline bool get__strafeRight_9() const { return ____strafeRight_9; }
	inline bool* get_address_of__strafeRight_9() { return &____strafeRight_9; }
	inline void set__strafeRight_9(bool value)
	{
		____strafeRight_9 = value;
	}

	inline static int32_t get_offset_of__lastStrafeChangeTime_10() { return static_cast<int32_t>(offsetof(EnemyStateAttack_tC0E53565C8E9746FBE18E7521390DB4D26D05A9A, ____lastStrafeChangeTime_10)); }
	inline float get__lastStrafeChangeTime_10() const { return ____lastStrafeChangeTime_10; }
	inline float* get_address_of__lastStrafeChangeTime_10() { return &____lastStrafeChangeTime_10; }
	inline void set__lastStrafeChangeTime_10(float value)
	{
		____lastStrafeChangeTime_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENEMYSTATEATTACK_TC0E53565C8E9746FBE18E7521390DB4D26D05A9A_H
#ifndef SETTINGS_T3ABF5D501B5AFEE2903D5F846E8EC24317DB3528_H
#define SETTINGS_T3ABF5D501B5AFEE2903D5F846E8EC24317DB3528_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SpaceFighter.EnemyStateAttack_Settings
struct  Settings_t3ABF5D501B5AFEE2903D5F846E8EC24317DB3528  : public RuntimeObject
{
public:
	// UnityEngine.AudioClip Zenject.SpaceFighter.EnemyStateAttack_Settings::ShootSound
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___ShootSound_0;
	// System.Single Zenject.SpaceFighter.EnemyStateAttack_Settings::ShootSoundVolume
	float ___ShootSoundVolume_1;
	// System.Single Zenject.SpaceFighter.EnemyStateAttack_Settings::BulletLifetime
	float ___BulletLifetime_2;
	// System.Single Zenject.SpaceFighter.EnemyStateAttack_Settings::BulletSpeed
	float ___BulletSpeed_3;
	// System.Single Zenject.SpaceFighter.EnemyStateAttack_Settings::BulletOffsetDistance
	float ___BulletOffsetDistance_4;
	// System.Single Zenject.SpaceFighter.EnemyStateAttack_Settings::ShootInterval
	float ___ShootInterval_5;
	// System.Single Zenject.SpaceFighter.EnemyStateAttack_Settings::ErrorRangeTheta
	float ___ErrorRangeTheta_6;
	// System.Single Zenject.SpaceFighter.EnemyStateAttack_Settings::AttackRangeBuffer
	float ___AttackRangeBuffer_7;
	// System.Single Zenject.SpaceFighter.EnemyStateAttack_Settings::StrafeMultiplier
	float ___StrafeMultiplier_8;
	// System.Single Zenject.SpaceFighter.EnemyStateAttack_Settings::StrafeChangeInterval
	float ___StrafeChangeInterval_9;

public:
	inline static int32_t get_offset_of_ShootSound_0() { return static_cast<int32_t>(offsetof(Settings_t3ABF5D501B5AFEE2903D5F846E8EC24317DB3528, ___ShootSound_0)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_ShootSound_0() const { return ___ShootSound_0; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_ShootSound_0() { return &___ShootSound_0; }
	inline void set_ShootSound_0(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___ShootSound_0 = value;
		Il2CppCodeGenWriteBarrier((&___ShootSound_0), value);
	}

	inline static int32_t get_offset_of_ShootSoundVolume_1() { return static_cast<int32_t>(offsetof(Settings_t3ABF5D501B5AFEE2903D5F846E8EC24317DB3528, ___ShootSoundVolume_1)); }
	inline float get_ShootSoundVolume_1() const { return ___ShootSoundVolume_1; }
	inline float* get_address_of_ShootSoundVolume_1() { return &___ShootSoundVolume_1; }
	inline void set_ShootSoundVolume_1(float value)
	{
		___ShootSoundVolume_1 = value;
	}

	inline static int32_t get_offset_of_BulletLifetime_2() { return static_cast<int32_t>(offsetof(Settings_t3ABF5D501B5AFEE2903D5F846E8EC24317DB3528, ___BulletLifetime_2)); }
	inline float get_BulletLifetime_2() const { return ___BulletLifetime_2; }
	inline float* get_address_of_BulletLifetime_2() { return &___BulletLifetime_2; }
	inline void set_BulletLifetime_2(float value)
	{
		___BulletLifetime_2 = value;
	}

	inline static int32_t get_offset_of_BulletSpeed_3() { return static_cast<int32_t>(offsetof(Settings_t3ABF5D501B5AFEE2903D5F846E8EC24317DB3528, ___BulletSpeed_3)); }
	inline float get_BulletSpeed_3() const { return ___BulletSpeed_3; }
	inline float* get_address_of_BulletSpeed_3() { return &___BulletSpeed_3; }
	inline void set_BulletSpeed_3(float value)
	{
		___BulletSpeed_3 = value;
	}

	inline static int32_t get_offset_of_BulletOffsetDistance_4() { return static_cast<int32_t>(offsetof(Settings_t3ABF5D501B5AFEE2903D5F846E8EC24317DB3528, ___BulletOffsetDistance_4)); }
	inline float get_BulletOffsetDistance_4() const { return ___BulletOffsetDistance_4; }
	inline float* get_address_of_BulletOffsetDistance_4() { return &___BulletOffsetDistance_4; }
	inline void set_BulletOffsetDistance_4(float value)
	{
		___BulletOffsetDistance_4 = value;
	}

	inline static int32_t get_offset_of_ShootInterval_5() { return static_cast<int32_t>(offsetof(Settings_t3ABF5D501B5AFEE2903D5F846E8EC24317DB3528, ___ShootInterval_5)); }
	inline float get_ShootInterval_5() const { return ___ShootInterval_5; }
	inline float* get_address_of_ShootInterval_5() { return &___ShootInterval_5; }
	inline void set_ShootInterval_5(float value)
	{
		___ShootInterval_5 = value;
	}

	inline static int32_t get_offset_of_ErrorRangeTheta_6() { return static_cast<int32_t>(offsetof(Settings_t3ABF5D501B5AFEE2903D5F846E8EC24317DB3528, ___ErrorRangeTheta_6)); }
	inline float get_ErrorRangeTheta_6() const { return ___ErrorRangeTheta_6; }
	inline float* get_address_of_ErrorRangeTheta_6() { return &___ErrorRangeTheta_6; }
	inline void set_ErrorRangeTheta_6(float value)
	{
		___ErrorRangeTheta_6 = value;
	}

	inline static int32_t get_offset_of_AttackRangeBuffer_7() { return static_cast<int32_t>(offsetof(Settings_t3ABF5D501B5AFEE2903D5F846E8EC24317DB3528, ___AttackRangeBuffer_7)); }
	inline float get_AttackRangeBuffer_7() const { return ___AttackRangeBuffer_7; }
	inline float* get_address_of_AttackRangeBuffer_7() { return &___AttackRangeBuffer_7; }
	inline void set_AttackRangeBuffer_7(float value)
	{
		___AttackRangeBuffer_7 = value;
	}

	inline static int32_t get_offset_of_StrafeMultiplier_8() { return static_cast<int32_t>(offsetof(Settings_t3ABF5D501B5AFEE2903D5F846E8EC24317DB3528, ___StrafeMultiplier_8)); }
	inline float get_StrafeMultiplier_8() const { return ___StrafeMultiplier_8; }
	inline float* get_address_of_StrafeMultiplier_8() { return &___StrafeMultiplier_8; }
	inline void set_StrafeMultiplier_8(float value)
	{
		___StrafeMultiplier_8 = value;
	}

	inline static int32_t get_offset_of_StrafeChangeInterval_9() { return static_cast<int32_t>(offsetof(Settings_t3ABF5D501B5AFEE2903D5F846E8EC24317DB3528, ___StrafeChangeInterval_9)); }
	inline float get_StrafeChangeInterval_9() const { return ___StrafeChangeInterval_9; }
	inline float* get_address_of_StrafeChangeInterval_9() { return &___StrafeChangeInterval_9; }
	inline void set_StrafeChangeInterval_9(float value)
	{
		___StrafeChangeInterval_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETTINGS_T3ABF5D501B5AFEE2903D5F846E8EC24317DB3528_H
#ifndef ENEMYSTATEFOLLOW_T4B768EEDF04FA03A6B009A38DC3069021C5ECFF2_H
#define ENEMYSTATEFOLLOW_T4B768EEDF04FA03A6B009A38DC3069021C5ECFF2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SpaceFighter.EnemyStateFollow
struct  EnemyStateFollow_t4B768EEDF04FA03A6B009A38DC3069021C5ECFF2  : public RuntimeObject
{
public:
	// Zenject.SpaceFighter.EnemyCommonSettings Zenject.SpaceFighter.EnemyStateFollow::_commonSettings
	EnemyCommonSettings_tF1C89E066A85E57A479925E334EDE12BEAAE767B * ____commonSettings_0;
	// Zenject.SpaceFighter.EnemyStateFollow_Settings Zenject.SpaceFighter.EnemyStateFollow::_settings
	Settings_tC265A1DB51490C02FF21B3A9733F230E53104BAC * ____settings_1;
	// Zenject.SpaceFighter.EnemyTunables Zenject.SpaceFighter.EnemyStateFollow::_tunables
	EnemyTunables_tD9997725AFF614521DD7088FFF23C1D8DD06DE18 * ____tunables_2;
	// Zenject.SpaceFighter.EnemyStateManager Zenject.SpaceFighter.EnemyStateFollow::_stateManager
	EnemyStateManager_tD3E2662DA02D660CB7356CE3563F928B0E220C9F * ____stateManager_3;
	// Zenject.SpaceFighter.Enemy Zenject.SpaceFighter.EnemyStateFollow::_enemy
	Enemy_tC40CA91F878B23445D874D13243C2824315DE3D2 * ____enemy_4;
	// Zenject.SpaceFighter.IPlayer Zenject.SpaceFighter.EnemyStateFollow::_player
	RuntimeObject* ____player_5;
	// System.Boolean Zenject.SpaceFighter.EnemyStateFollow::_strafeRight
	bool ____strafeRight_6;
	// System.Single Zenject.SpaceFighter.EnemyStateFollow::_lastStrafeChangeTime
	float ____lastStrafeChangeTime_7;

public:
	inline static int32_t get_offset_of__commonSettings_0() { return static_cast<int32_t>(offsetof(EnemyStateFollow_t4B768EEDF04FA03A6B009A38DC3069021C5ECFF2, ____commonSettings_0)); }
	inline EnemyCommonSettings_tF1C89E066A85E57A479925E334EDE12BEAAE767B * get__commonSettings_0() const { return ____commonSettings_0; }
	inline EnemyCommonSettings_tF1C89E066A85E57A479925E334EDE12BEAAE767B ** get_address_of__commonSettings_0() { return &____commonSettings_0; }
	inline void set__commonSettings_0(EnemyCommonSettings_tF1C89E066A85E57A479925E334EDE12BEAAE767B * value)
	{
		____commonSettings_0 = value;
		Il2CppCodeGenWriteBarrier((&____commonSettings_0), value);
	}

	inline static int32_t get_offset_of__settings_1() { return static_cast<int32_t>(offsetof(EnemyStateFollow_t4B768EEDF04FA03A6B009A38DC3069021C5ECFF2, ____settings_1)); }
	inline Settings_tC265A1DB51490C02FF21B3A9733F230E53104BAC * get__settings_1() const { return ____settings_1; }
	inline Settings_tC265A1DB51490C02FF21B3A9733F230E53104BAC ** get_address_of__settings_1() { return &____settings_1; }
	inline void set__settings_1(Settings_tC265A1DB51490C02FF21B3A9733F230E53104BAC * value)
	{
		____settings_1 = value;
		Il2CppCodeGenWriteBarrier((&____settings_1), value);
	}

	inline static int32_t get_offset_of__tunables_2() { return static_cast<int32_t>(offsetof(EnemyStateFollow_t4B768EEDF04FA03A6B009A38DC3069021C5ECFF2, ____tunables_2)); }
	inline EnemyTunables_tD9997725AFF614521DD7088FFF23C1D8DD06DE18 * get__tunables_2() const { return ____tunables_2; }
	inline EnemyTunables_tD9997725AFF614521DD7088FFF23C1D8DD06DE18 ** get_address_of__tunables_2() { return &____tunables_2; }
	inline void set__tunables_2(EnemyTunables_tD9997725AFF614521DD7088FFF23C1D8DD06DE18 * value)
	{
		____tunables_2 = value;
		Il2CppCodeGenWriteBarrier((&____tunables_2), value);
	}

	inline static int32_t get_offset_of__stateManager_3() { return static_cast<int32_t>(offsetof(EnemyStateFollow_t4B768EEDF04FA03A6B009A38DC3069021C5ECFF2, ____stateManager_3)); }
	inline EnemyStateManager_tD3E2662DA02D660CB7356CE3563F928B0E220C9F * get__stateManager_3() const { return ____stateManager_3; }
	inline EnemyStateManager_tD3E2662DA02D660CB7356CE3563F928B0E220C9F ** get_address_of__stateManager_3() { return &____stateManager_3; }
	inline void set__stateManager_3(EnemyStateManager_tD3E2662DA02D660CB7356CE3563F928B0E220C9F * value)
	{
		____stateManager_3 = value;
		Il2CppCodeGenWriteBarrier((&____stateManager_3), value);
	}

	inline static int32_t get_offset_of__enemy_4() { return static_cast<int32_t>(offsetof(EnemyStateFollow_t4B768EEDF04FA03A6B009A38DC3069021C5ECFF2, ____enemy_4)); }
	inline Enemy_tC40CA91F878B23445D874D13243C2824315DE3D2 * get__enemy_4() const { return ____enemy_4; }
	inline Enemy_tC40CA91F878B23445D874D13243C2824315DE3D2 ** get_address_of__enemy_4() { return &____enemy_4; }
	inline void set__enemy_4(Enemy_tC40CA91F878B23445D874D13243C2824315DE3D2 * value)
	{
		____enemy_4 = value;
		Il2CppCodeGenWriteBarrier((&____enemy_4), value);
	}

	inline static int32_t get_offset_of__player_5() { return static_cast<int32_t>(offsetof(EnemyStateFollow_t4B768EEDF04FA03A6B009A38DC3069021C5ECFF2, ____player_5)); }
	inline RuntimeObject* get__player_5() const { return ____player_5; }
	inline RuntimeObject** get_address_of__player_5() { return &____player_5; }
	inline void set__player_5(RuntimeObject* value)
	{
		____player_5 = value;
		Il2CppCodeGenWriteBarrier((&____player_5), value);
	}

	inline static int32_t get_offset_of__strafeRight_6() { return static_cast<int32_t>(offsetof(EnemyStateFollow_t4B768EEDF04FA03A6B009A38DC3069021C5ECFF2, ____strafeRight_6)); }
	inline bool get__strafeRight_6() const { return ____strafeRight_6; }
	inline bool* get_address_of__strafeRight_6() { return &____strafeRight_6; }
	inline void set__strafeRight_6(bool value)
	{
		____strafeRight_6 = value;
	}

	inline static int32_t get_offset_of__lastStrafeChangeTime_7() { return static_cast<int32_t>(offsetof(EnemyStateFollow_t4B768EEDF04FA03A6B009A38DC3069021C5ECFF2, ____lastStrafeChangeTime_7)); }
	inline float get__lastStrafeChangeTime_7() const { return ____lastStrafeChangeTime_7; }
	inline float* get_address_of__lastStrafeChangeTime_7() { return &____lastStrafeChangeTime_7; }
	inline void set__lastStrafeChangeTime_7(float value)
	{
		____lastStrafeChangeTime_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENEMYSTATEFOLLOW_T4B768EEDF04FA03A6B009A38DC3069021C5ECFF2_H
#ifndef SETTINGS_TC265A1DB51490C02FF21B3A9733F230E53104BAC_H
#define SETTINGS_TC265A1DB51490C02FF21B3A9733F230E53104BAC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SpaceFighter.EnemyStateFollow_Settings
struct  Settings_tC265A1DB51490C02FF21B3A9733F230E53104BAC  : public RuntimeObject
{
public:
	// System.Single Zenject.SpaceFighter.EnemyStateFollow_Settings::StrafeMultiplier
	float ___StrafeMultiplier_0;
	// System.Single Zenject.SpaceFighter.EnemyStateFollow_Settings::StrafeChangeInterval
	float ___StrafeChangeInterval_1;
	// System.Single Zenject.SpaceFighter.EnemyStateFollow_Settings::TeleportNewDistance
	float ___TeleportNewDistance_2;

public:
	inline static int32_t get_offset_of_StrafeMultiplier_0() { return static_cast<int32_t>(offsetof(Settings_tC265A1DB51490C02FF21B3A9733F230E53104BAC, ___StrafeMultiplier_0)); }
	inline float get_StrafeMultiplier_0() const { return ___StrafeMultiplier_0; }
	inline float* get_address_of_StrafeMultiplier_0() { return &___StrafeMultiplier_0; }
	inline void set_StrafeMultiplier_0(float value)
	{
		___StrafeMultiplier_0 = value;
	}

	inline static int32_t get_offset_of_StrafeChangeInterval_1() { return static_cast<int32_t>(offsetof(Settings_tC265A1DB51490C02FF21B3A9733F230E53104BAC, ___StrafeChangeInterval_1)); }
	inline float get_StrafeChangeInterval_1() const { return ___StrafeChangeInterval_1; }
	inline float* get_address_of_StrafeChangeInterval_1() { return &___StrafeChangeInterval_1; }
	inline void set_StrafeChangeInterval_1(float value)
	{
		___StrafeChangeInterval_1 = value;
	}

	inline static int32_t get_offset_of_TeleportNewDistance_2() { return static_cast<int32_t>(offsetof(Settings_tC265A1DB51490C02FF21B3A9733F230E53104BAC, ___TeleportNewDistance_2)); }
	inline float get_TeleportNewDistance_2() const { return ___TeleportNewDistance_2; }
	inline float* get_address_of_TeleportNewDistance_2() { return &___TeleportNewDistance_2; }
	inline void set_TeleportNewDistance_2(float value)
	{
		___TeleportNewDistance_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETTINGS_TC265A1DB51490C02FF21B3A9733F230E53104BAC_H
#ifndef SETTINGS_T6A845C637F62D4448CE7764CFBBFBDDDA7951042_H
#define SETTINGS_T6A845C637F62D4448CE7764CFBBFBDDDA7951042_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SpaceFighter.EnemyStateIdle_Settings
struct  Settings_t6A845C637F62D4448CE7764CFBBFBDDDA7951042  : public RuntimeObject
{
public:
	// System.Single Zenject.SpaceFighter.EnemyStateIdle_Settings::Amplitude
	float ___Amplitude_0;
	// System.Single Zenject.SpaceFighter.EnemyStateIdle_Settings::Frequency
	float ___Frequency_1;

public:
	inline static int32_t get_offset_of_Amplitude_0() { return static_cast<int32_t>(offsetof(Settings_t6A845C637F62D4448CE7764CFBBFBDDDA7951042, ___Amplitude_0)); }
	inline float get_Amplitude_0() const { return ___Amplitude_0; }
	inline float* get_address_of_Amplitude_0() { return &___Amplitude_0; }
	inline void set_Amplitude_0(float value)
	{
		___Amplitude_0 = value;
	}

	inline static int32_t get_offset_of_Frequency_1() { return static_cast<int32_t>(offsetof(Settings_t6A845C637F62D4448CE7764CFBBFBDDDA7951042, ___Frequency_1)); }
	inline float get_Frequency_1() const { return ___Frequency_1; }
	inline float* get_address_of_Frequency_1() { return &___Frequency_1; }
	inline void set_Frequency_1(float value)
	{
		___Frequency_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETTINGS_T6A845C637F62D4448CE7764CFBBFBDDDA7951042_H
#ifndef ENEMYTUNABLES_TD9997725AFF614521DD7088FFF23C1D8DD06DE18_H
#define ENEMYTUNABLES_TD9997725AFF614521DD7088FFF23C1D8DD06DE18_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SpaceFighter.EnemyTunables
struct  EnemyTunables_tD9997725AFF614521DD7088FFF23C1D8DD06DE18  : public RuntimeObject
{
public:
	// System.Single Zenject.SpaceFighter.EnemyTunables::Accuracy
	float ___Accuracy_0;
	// System.Single Zenject.SpaceFighter.EnemyTunables::Speed
	float ___Speed_1;

public:
	inline static int32_t get_offset_of_Accuracy_0() { return static_cast<int32_t>(offsetof(EnemyTunables_tD9997725AFF614521DD7088FFF23C1D8DD06DE18, ___Accuracy_0)); }
	inline float get_Accuracy_0() const { return ___Accuracy_0; }
	inline float* get_address_of_Accuracy_0() { return &___Accuracy_0; }
	inline void set_Accuracy_0(float value)
	{
		___Accuracy_0 = value;
	}

	inline static int32_t get_offset_of_Speed_1() { return static_cast<int32_t>(offsetof(EnemyTunables_tD9997725AFF614521DD7088FFF23C1D8DD06DE18, ___Speed_1)); }
	inline float get_Speed_1() const { return ___Speed_1; }
	inline float* get_address_of_Speed_1() { return &___Speed_1; }
	inline void set_Speed_1(float value)
	{
		___Speed_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENEMYTUNABLES_TD9997725AFF614521DD7088FFF23C1D8DD06DE18_H
#ifndef SETTINGS_TDDDA6539A297D2F0F5790F2BE702EABB8BC23312_H
#define SETTINGS_TDDDA6539A297D2F0F5790F2BE702EABB8BC23312_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SpaceFighter.GameInstaller_Settings
struct  Settings_tDDDA6539A297D2F0F5790F2BE702EABB8BC23312  : public RuntimeObject
{
public:
	// UnityEngine.GameObject Zenject.SpaceFighter.GameInstaller_Settings::EnemyFacadePrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___EnemyFacadePrefab_0;
	// UnityEngine.GameObject Zenject.SpaceFighter.GameInstaller_Settings::BulletPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___BulletPrefab_1;
	// UnityEngine.GameObject Zenject.SpaceFighter.GameInstaller_Settings::ExplosionPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___ExplosionPrefab_2;

public:
	inline static int32_t get_offset_of_EnemyFacadePrefab_0() { return static_cast<int32_t>(offsetof(Settings_tDDDA6539A297D2F0F5790F2BE702EABB8BC23312, ___EnemyFacadePrefab_0)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_EnemyFacadePrefab_0() const { return ___EnemyFacadePrefab_0; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_EnemyFacadePrefab_0() { return &___EnemyFacadePrefab_0; }
	inline void set_EnemyFacadePrefab_0(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___EnemyFacadePrefab_0 = value;
		Il2CppCodeGenWriteBarrier((&___EnemyFacadePrefab_0), value);
	}

	inline static int32_t get_offset_of_BulletPrefab_1() { return static_cast<int32_t>(offsetof(Settings_tDDDA6539A297D2F0F5790F2BE702EABB8BC23312, ___BulletPrefab_1)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_BulletPrefab_1() const { return ___BulletPrefab_1; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_BulletPrefab_1() { return &___BulletPrefab_1; }
	inline void set_BulletPrefab_1(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___BulletPrefab_1 = value;
		Il2CppCodeGenWriteBarrier((&___BulletPrefab_1), value);
	}

	inline static int32_t get_offset_of_ExplosionPrefab_2() { return static_cast<int32_t>(offsetof(Settings_tDDDA6539A297D2F0F5790F2BE702EABB8BC23312, ___ExplosionPrefab_2)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_ExplosionPrefab_2() const { return ___ExplosionPrefab_2; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_ExplosionPrefab_2() { return &___ExplosionPrefab_2; }
	inline void set_ExplosionPrefab_2(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___ExplosionPrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___ExplosionPrefab_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETTINGS_TDDDA6539A297D2F0F5790F2BE702EABB8BC23312_H
#ifndef GAMERESTARTHANDLER_T6A892235F071977C1E69DE19C8B5F983D8E7EAEE_H
#define GAMERESTARTHANDLER_T6A892235F071977C1E69DE19C8B5F983D8E7EAEE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SpaceFighter.GameRestartHandler
struct  GameRestartHandler_t6A892235F071977C1E69DE19C8B5F983D8E7EAEE  : public RuntimeObject
{
public:
	// Zenject.SpaceFighter.GameRestartHandler_Settings Zenject.SpaceFighter.GameRestartHandler::_settings
	Settings_tDA43D3BAE8BE0B223AEFEE4FC2782ACB7169D1C2 * ____settings_0;
	// Zenject.SpaceFighter.PlayerDiedSignal Zenject.SpaceFighter.GameRestartHandler::_playerDiedSignal
	PlayerDiedSignal_tD47ED6EA743FEE4E5D7B3289450C3EA59E018C7F * ____playerDiedSignal_1;
	// System.Boolean Zenject.SpaceFighter.GameRestartHandler::_isDelaying
	bool ____isDelaying_2;
	// System.Single Zenject.SpaceFighter.GameRestartHandler::_delayStartTime
	float ____delayStartTime_3;

public:
	inline static int32_t get_offset_of__settings_0() { return static_cast<int32_t>(offsetof(GameRestartHandler_t6A892235F071977C1E69DE19C8B5F983D8E7EAEE, ____settings_0)); }
	inline Settings_tDA43D3BAE8BE0B223AEFEE4FC2782ACB7169D1C2 * get__settings_0() const { return ____settings_0; }
	inline Settings_tDA43D3BAE8BE0B223AEFEE4FC2782ACB7169D1C2 ** get_address_of__settings_0() { return &____settings_0; }
	inline void set__settings_0(Settings_tDA43D3BAE8BE0B223AEFEE4FC2782ACB7169D1C2 * value)
	{
		____settings_0 = value;
		Il2CppCodeGenWriteBarrier((&____settings_0), value);
	}

	inline static int32_t get_offset_of__playerDiedSignal_1() { return static_cast<int32_t>(offsetof(GameRestartHandler_t6A892235F071977C1E69DE19C8B5F983D8E7EAEE, ____playerDiedSignal_1)); }
	inline PlayerDiedSignal_tD47ED6EA743FEE4E5D7B3289450C3EA59E018C7F * get__playerDiedSignal_1() const { return ____playerDiedSignal_1; }
	inline PlayerDiedSignal_tD47ED6EA743FEE4E5D7B3289450C3EA59E018C7F ** get_address_of__playerDiedSignal_1() { return &____playerDiedSignal_1; }
	inline void set__playerDiedSignal_1(PlayerDiedSignal_tD47ED6EA743FEE4E5D7B3289450C3EA59E018C7F * value)
	{
		____playerDiedSignal_1 = value;
		Il2CppCodeGenWriteBarrier((&____playerDiedSignal_1), value);
	}

	inline static int32_t get_offset_of__isDelaying_2() { return static_cast<int32_t>(offsetof(GameRestartHandler_t6A892235F071977C1E69DE19C8B5F983D8E7EAEE, ____isDelaying_2)); }
	inline bool get__isDelaying_2() const { return ____isDelaying_2; }
	inline bool* get_address_of__isDelaying_2() { return &____isDelaying_2; }
	inline void set__isDelaying_2(bool value)
	{
		____isDelaying_2 = value;
	}

	inline static int32_t get_offset_of__delayStartTime_3() { return static_cast<int32_t>(offsetof(GameRestartHandler_t6A892235F071977C1E69DE19C8B5F983D8E7EAEE, ____delayStartTime_3)); }
	inline float get__delayStartTime_3() const { return ____delayStartTime_3; }
	inline float* get_address_of__delayStartTime_3() { return &____delayStartTime_3; }
	inline void set__delayStartTime_3(float value)
	{
		____delayStartTime_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMERESTARTHANDLER_T6A892235F071977C1E69DE19C8B5F983D8E7EAEE_H
#ifndef SETTINGS_TDA43D3BAE8BE0B223AEFEE4FC2782ACB7169D1C2_H
#define SETTINGS_TDA43D3BAE8BE0B223AEFEE4FC2782ACB7169D1C2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SpaceFighter.GameRestartHandler_Settings
struct  Settings_tDA43D3BAE8BE0B223AEFEE4FC2782ACB7169D1C2  : public RuntimeObject
{
public:
	// System.Single Zenject.SpaceFighter.GameRestartHandler_Settings::RestartDelay
	float ___RestartDelay_0;

public:
	inline static int32_t get_offset_of_RestartDelay_0() { return static_cast<int32_t>(offsetof(Settings_tDA43D3BAE8BE0B223AEFEE4FC2782ACB7169D1C2, ___RestartDelay_0)); }
	inline float get_RestartDelay_0() const { return ___RestartDelay_0; }
	inline float* get_address_of_RestartDelay_0() { return &___RestartDelay_0; }
	inline void set_RestartDelay_0(float value)
	{
		___RestartDelay_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETTINGS_TDA43D3BAE8BE0B223AEFEE4FC2782ACB7169D1C2_H
#ifndef ENEMYSETTINGS_T87F1C13D725FC80A97C27012C240D7E9150A5785_H
#define ENEMYSETTINGS_T87F1C13D725FC80A97C27012C240D7E9150A5785_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SpaceFighter.GameSettingsInstaller_EnemySettings
struct  EnemySettings_t87F1C13D725FC80A97C27012C240D7E9150A5785  : public RuntimeObject
{
public:
	// Zenject.SpaceFighter.EnemyTunables Zenject.SpaceFighter.GameSettingsInstaller_EnemySettings::DefaultSettings
	EnemyTunables_tD9997725AFF614521DD7088FFF23C1D8DD06DE18 * ___DefaultSettings_0;
	// Zenject.SpaceFighter.EnemyStateIdle_Settings Zenject.SpaceFighter.GameSettingsInstaller_EnemySettings::EnemyStateIdle
	Settings_t6A845C637F62D4448CE7764CFBBFBDDDA7951042 * ___EnemyStateIdle_1;
	// Zenject.SpaceFighter.EnemyRotationHandler_Settings Zenject.SpaceFighter.GameSettingsInstaller_EnemySettings::EnemyRotationHandler
	Settings_t3480554AD22B7730290AB95863B9EDC48610BCE9 * ___EnemyRotationHandler_2;
	// Zenject.SpaceFighter.EnemyStateFollow_Settings Zenject.SpaceFighter.GameSettingsInstaller_EnemySettings::EnemyStateFollow
	Settings_tC265A1DB51490C02FF21B3A9733F230E53104BAC * ___EnemyStateFollow_3;
	// Zenject.SpaceFighter.EnemyStateAttack_Settings Zenject.SpaceFighter.GameSettingsInstaller_EnemySettings::EnemyStateAttack
	Settings_t3ABF5D501B5AFEE2903D5F846E8EC24317DB3528 * ___EnemyStateAttack_4;
	// Zenject.SpaceFighter.EnemyDeathHandler_Settings Zenject.SpaceFighter.GameSettingsInstaller_EnemySettings::EnemyHealthWatcher
	Settings_tCDA69B4C2B2FC2E7E5DC396996019836DE3ED1E2 * ___EnemyHealthWatcher_5;
	// Zenject.SpaceFighter.EnemyCommonSettings Zenject.SpaceFighter.GameSettingsInstaller_EnemySettings::EnemyCommonSettings
	EnemyCommonSettings_tF1C89E066A85E57A479925E334EDE12BEAAE767B * ___EnemyCommonSettings_6;

public:
	inline static int32_t get_offset_of_DefaultSettings_0() { return static_cast<int32_t>(offsetof(EnemySettings_t87F1C13D725FC80A97C27012C240D7E9150A5785, ___DefaultSettings_0)); }
	inline EnemyTunables_tD9997725AFF614521DD7088FFF23C1D8DD06DE18 * get_DefaultSettings_0() const { return ___DefaultSettings_0; }
	inline EnemyTunables_tD9997725AFF614521DD7088FFF23C1D8DD06DE18 ** get_address_of_DefaultSettings_0() { return &___DefaultSettings_0; }
	inline void set_DefaultSettings_0(EnemyTunables_tD9997725AFF614521DD7088FFF23C1D8DD06DE18 * value)
	{
		___DefaultSettings_0 = value;
		Il2CppCodeGenWriteBarrier((&___DefaultSettings_0), value);
	}

	inline static int32_t get_offset_of_EnemyStateIdle_1() { return static_cast<int32_t>(offsetof(EnemySettings_t87F1C13D725FC80A97C27012C240D7E9150A5785, ___EnemyStateIdle_1)); }
	inline Settings_t6A845C637F62D4448CE7764CFBBFBDDDA7951042 * get_EnemyStateIdle_1() const { return ___EnemyStateIdle_1; }
	inline Settings_t6A845C637F62D4448CE7764CFBBFBDDDA7951042 ** get_address_of_EnemyStateIdle_1() { return &___EnemyStateIdle_1; }
	inline void set_EnemyStateIdle_1(Settings_t6A845C637F62D4448CE7764CFBBFBDDDA7951042 * value)
	{
		___EnemyStateIdle_1 = value;
		Il2CppCodeGenWriteBarrier((&___EnemyStateIdle_1), value);
	}

	inline static int32_t get_offset_of_EnemyRotationHandler_2() { return static_cast<int32_t>(offsetof(EnemySettings_t87F1C13D725FC80A97C27012C240D7E9150A5785, ___EnemyRotationHandler_2)); }
	inline Settings_t3480554AD22B7730290AB95863B9EDC48610BCE9 * get_EnemyRotationHandler_2() const { return ___EnemyRotationHandler_2; }
	inline Settings_t3480554AD22B7730290AB95863B9EDC48610BCE9 ** get_address_of_EnemyRotationHandler_2() { return &___EnemyRotationHandler_2; }
	inline void set_EnemyRotationHandler_2(Settings_t3480554AD22B7730290AB95863B9EDC48610BCE9 * value)
	{
		___EnemyRotationHandler_2 = value;
		Il2CppCodeGenWriteBarrier((&___EnemyRotationHandler_2), value);
	}

	inline static int32_t get_offset_of_EnemyStateFollow_3() { return static_cast<int32_t>(offsetof(EnemySettings_t87F1C13D725FC80A97C27012C240D7E9150A5785, ___EnemyStateFollow_3)); }
	inline Settings_tC265A1DB51490C02FF21B3A9733F230E53104BAC * get_EnemyStateFollow_3() const { return ___EnemyStateFollow_3; }
	inline Settings_tC265A1DB51490C02FF21B3A9733F230E53104BAC ** get_address_of_EnemyStateFollow_3() { return &___EnemyStateFollow_3; }
	inline void set_EnemyStateFollow_3(Settings_tC265A1DB51490C02FF21B3A9733F230E53104BAC * value)
	{
		___EnemyStateFollow_3 = value;
		Il2CppCodeGenWriteBarrier((&___EnemyStateFollow_3), value);
	}

	inline static int32_t get_offset_of_EnemyStateAttack_4() { return static_cast<int32_t>(offsetof(EnemySettings_t87F1C13D725FC80A97C27012C240D7E9150A5785, ___EnemyStateAttack_4)); }
	inline Settings_t3ABF5D501B5AFEE2903D5F846E8EC24317DB3528 * get_EnemyStateAttack_4() const { return ___EnemyStateAttack_4; }
	inline Settings_t3ABF5D501B5AFEE2903D5F846E8EC24317DB3528 ** get_address_of_EnemyStateAttack_4() { return &___EnemyStateAttack_4; }
	inline void set_EnemyStateAttack_4(Settings_t3ABF5D501B5AFEE2903D5F846E8EC24317DB3528 * value)
	{
		___EnemyStateAttack_4 = value;
		Il2CppCodeGenWriteBarrier((&___EnemyStateAttack_4), value);
	}

	inline static int32_t get_offset_of_EnemyHealthWatcher_5() { return static_cast<int32_t>(offsetof(EnemySettings_t87F1C13D725FC80A97C27012C240D7E9150A5785, ___EnemyHealthWatcher_5)); }
	inline Settings_tCDA69B4C2B2FC2E7E5DC396996019836DE3ED1E2 * get_EnemyHealthWatcher_5() const { return ___EnemyHealthWatcher_5; }
	inline Settings_tCDA69B4C2B2FC2E7E5DC396996019836DE3ED1E2 ** get_address_of_EnemyHealthWatcher_5() { return &___EnemyHealthWatcher_5; }
	inline void set_EnemyHealthWatcher_5(Settings_tCDA69B4C2B2FC2E7E5DC396996019836DE3ED1E2 * value)
	{
		___EnemyHealthWatcher_5 = value;
		Il2CppCodeGenWriteBarrier((&___EnemyHealthWatcher_5), value);
	}

	inline static int32_t get_offset_of_EnemyCommonSettings_6() { return static_cast<int32_t>(offsetof(EnemySettings_t87F1C13D725FC80A97C27012C240D7E9150A5785, ___EnemyCommonSettings_6)); }
	inline EnemyCommonSettings_tF1C89E066A85E57A479925E334EDE12BEAAE767B * get_EnemyCommonSettings_6() const { return ___EnemyCommonSettings_6; }
	inline EnemyCommonSettings_tF1C89E066A85E57A479925E334EDE12BEAAE767B ** get_address_of_EnemyCommonSettings_6() { return &___EnemyCommonSettings_6; }
	inline void set_EnemyCommonSettings_6(EnemyCommonSettings_tF1C89E066A85E57A479925E334EDE12BEAAE767B * value)
	{
		___EnemyCommonSettings_6 = value;
		Il2CppCodeGenWriteBarrier((&___EnemyCommonSettings_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENEMYSETTINGS_T87F1C13D725FC80A97C27012C240D7E9150A5785_H
#ifndef PLAYERSETTINGS_T50B498D03A02BD7CD7E2367EFDAD0428FBBC1058_H
#define PLAYERSETTINGS_T50B498D03A02BD7CD7E2367EFDAD0428FBBC1058_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SpaceFighter.GameSettingsInstaller_PlayerSettings
struct  PlayerSettings_t50B498D03A02BD7CD7E2367EFDAD0428FBBC1058  : public RuntimeObject
{
public:
	// Zenject.SpaceFighter.PlayerMoveHandler_Settings Zenject.SpaceFighter.GameSettingsInstaller_PlayerSettings::PlayerMoveHandler
	Settings_t5D29F620DD8E2C4750D3670B9D88BD2F6F4C6FC5 * ___PlayerMoveHandler_0;
	// Zenject.SpaceFighter.PlayerShootHandler_Settings Zenject.SpaceFighter.GameSettingsInstaller_PlayerSettings::PlayerShootHandler
	Settings_t59B948D04E16E5F614372F81DC0B8066A189FF9D * ___PlayerShootHandler_1;
	// Zenject.SpaceFighter.PlayerDamageHandler_Settings Zenject.SpaceFighter.GameSettingsInstaller_PlayerSettings::PlayerCollisionHandler
	Settings_t48778C45589CCBEC26A7E67F855609F50926CB44 * ___PlayerCollisionHandler_2;
	// Zenject.SpaceFighter.PlayerHealthWatcher_Settings Zenject.SpaceFighter.GameSettingsInstaller_PlayerSettings::PlayerHealthWatcher
	Settings_t83D6CE3845CA378293BC213A44067B48638017A4 * ___PlayerHealthWatcher_3;

public:
	inline static int32_t get_offset_of_PlayerMoveHandler_0() { return static_cast<int32_t>(offsetof(PlayerSettings_t50B498D03A02BD7CD7E2367EFDAD0428FBBC1058, ___PlayerMoveHandler_0)); }
	inline Settings_t5D29F620DD8E2C4750D3670B9D88BD2F6F4C6FC5 * get_PlayerMoveHandler_0() const { return ___PlayerMoveHandler_0; }
	inline Settings_t5D29F620DD8E2C4750D3670B9D88BD2F6F4C6FC5 ** get_address_of_PlayerMoveHandler_0() { return &___PlayerMoveHandler_0; }
	inline void set_PlayerMoveHandler_0(Settings_t5D29F620DD8E2C4750D3670B9D88BD2F6F4C6FC5 * value)
	{
		___PlayerMoveHandler_0 = value;
		Il2CppCodeGenWriteBarrier((&___PlayerMoveHandler_0), value);
	}

	inline static int32_t get_offset_of_PlayerShootHandler_1() { return static_cast<int32_t>(offsetof(PlayerSettings_t50B498D03A02BD7CD7E2367EFDAD0428FBBC1058, ___PlayerShootHandler_1)); }
	inline Settings_t59B948D04E16E5F614372F81DC0B8066A189FF9D * get_PlayerShootHandler_1() const { return ___PlayerShootHandler_1; }
	inline Settings_t59B948D04E16E5F614372F81DC0B8066A189FF9D ** get_address_of_PlayerShootHandler_1() { return &___PlayerShootHandler_1; }
	inline void set_PlayerShootHandler_1(Settings_t59B948D04E16E5F614372F81DC0B8066A189FF9D * value)
	{
		___PlayerShootHandler_1 = value;
		Il2CppCodeGenWriteBarrier((&___PlayerShootHandler_1), value);
	}

	inline static int32_t get_offset_of_PlayerCollisionHandler_2() { return static_cast<int32_t>(offsetof(PlayerSettings_t50B498D03A02BD7CD7E2367EFDAD0428FBBC1058, ___PlayerCollisionHandler_2)); }
	inline Settings_t48778C45589CCBEC26A7E67F855609F50926CB44 * get_PlayerCollisionHandler_2() const { return ___PlayerCollisionHandler_2; }
	inline Settings_t48778C45589CCBEC26A7E67F855609F50926CB44 ** get_address_of_PlayerCollisionHandler_2() { return &___PlayerCollisionHandler_2; }
	inline void set_PlayerCollisionHandler_2(Settings_t48778C45589CCBEC26A7E67F855609F50926CB44 * value)
	{
		___PlayerCollisionHandler_2 = value;
		Il2CppCodeGenWriteBarrier((&___PlayerCollisionHandler_2), value);
	}

	inline static int32_t get_offset_of_PlayerHealthWatcher_3() { return static_cast<int32_t>(offsetof(PlayerSettings_t50B498D03A02BD7CD7E2367EFDAD0428FBBC1058, ___PlayerHealthWatcher_3)); }
	inline Settings_t83D6CE3845CA378293BC213A44067B48638017A4 * get_PlayerHealthWatcher_3() const { return ___PlayerHealthWatcher_3; }
	inline Settings_t83D6CE3845CA378293BC213A44067B48638017A4 ** get_address_of_PlayerHealthWatcher_3() { return &___PlayerHealthWatcher_3; }
	inline void set_PlayerHealthWatcher_3(Settings_t83D6CE3845CA378293BC213A44067B48638017A4 * value)
	{
		___PlayerHealthWatcher_3 = value;
		Il2CppCodeGenWriteBarrier((&___PlayerHealthWatcher_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERSETTINGS_T50B498D03A02BD7CD7E2367EFDAD0428FBBC1058_H
#ifndef LEVELBOUNDARY_T8E706FF3A27DD22A30FE8E79EF745B6B7D78577B_H
#define LEVELBOUNDARY_T8E706FF3A27DD22A30FE8E79EF745B6B7D78577B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SpaceFighter.LevelBoundary
struct  LevelBoundary_t8E706FF3A27DD22A30FE8E79EF745B6B7D78577B  : public RuntimeObject
{
public:
	// UnityEngine.Camera Zenject.SpaceFighter.LevelBoundary::_camera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ____camera_0;

public:
	inline static int32_t get_offset_of__camera_0() { return static_cast<int32_t>(offsetof(LevelBoundary_t8E706FF3A27DD22A30FE8E79EF745B6B7D78577B, ____camera_0)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get__camera_0() const { return ____camera_0; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of__camera_0() { return &____camera_0; }
	inline void set__camera_0(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		____camera_0 = value;
		Il2CppCodeGenWriteBarrier((&____camera_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELBOUNDARY_T8E706FF3A27DD22A30FE8E79EF745B6B7D78577B_H
#ifndef PLAYER_T0644AFD1050593EF42B2459D30EA133F19E7A402_H
#define PLAYER_T0644AFD1050593EF42B2459D30EA133F19E7A402_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SpaceFighter.Player
struct  Player_t0644AFD1050593EF42B2459D30EA133F19E7A402  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody Zenject.SpaceFighter.Player::_rigidBody
	Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * ____rigidBody_0;
	// UnityEngine.MeshRenderer Zenject.SpaceFighter.Player::_renderer
	MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * ____renderer_1;
	// System.Single Zenject.SpaceFighter.Player::_health
	float ____health_2;
	// System.Boolean Zenject.SpaceFighter.Player::<IsDead>k__BackingField
	bool ___U3CIsDeadU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of__rigidBody_0() { return static_cast<int32_t>(offsetof(Player_t0644AFD1050593EF42B2459D30EA133F19E7A402, ____rigidBody_0)); }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * get__rigidBody_0() const { return ____rigidBody_0; }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 ** get_address_of__rigidBody_0() { return &____rigidBody_0; }
	inline void set__rigidBody_0(Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * value)
	{
		____rigidBody_0 = value;
		Il2CppCodeGenWriteBarrier((&____rigidBody_0), value);
	}

	inline static int32_t get_offset_of__renderer_1() { return static_cast<int32_t>(offsetof(Player_t0644AFD1050593EF42B2459D30EA133F19E7A402, ____renderer_1)); }
	inline MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * get__renderer_1() const { return ____renderer_1; }
	inline MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED ** get_address_of__renderer_1() { return &____renderer_1; }
	inline void set__renderer_1(MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * value)
	{
		____renderer_1 = value;
		Il2CppCodeGenWriteBarrier((&____renderer_1), value);
	}

	inline static int32_t get_offset_of__health_2() { return static_cast<int32_t>(offsetof(Player_t0644AFD1050593EF42B2459D30EA133F19E7A402, ____health_2)); }
	inline float get__health_2() const { return ____health_2; }
	inline float* get_address_of__health_2() { return &____health_2; }
	inline void set__health_2(float value)
	{
		____health_2 = value;
	}

	inline static int32_t get_offset_of_U3CIsDeadU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Player_t0644AFD1050593EF42B2459D30EA133F19E7A402, ___U3CIsDeadU3Ek__BackingField_3)); }
	inline bool get_U3CIsDeadU3Ek__BackingField_3() const { return ___U3CIsDeadU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CIsDeadU3Ek__BackingField_3() { return &___U3CIsDeadU3Ek__BackingField_3; }
	inline void set_U3CIsDeadU3Ek__BackingField_3(bool value)
	{
		___U3CIsDeadU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYER_T0644AFD1050593EF42B2459D30EA133F19E7A402_H
#ifndef PLAYERDAMAGEHANDLER_TFF3D703F066B3204ADD106FCC8B95D7EF61B4E33_H
#define PLAYERDAMAGEHANDLER_TFF3D703F066B3204ADD106FCC8B95D7EF61B4E33_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SpaceFighter.PlayerDamageHandler
struct  PlayerDamageHandler_tFF3D703F066B3204ADD106FCC8B95D7EF61B4E33  : public RuntimeObject
{
public:
	// Zenject.SpaceFighter.IAudioPlayer Zenject.SpaceFighter.PlayerDamageHandler::_audioPlayer
	RuntimeObject* ____audioPlayer_0;
	// Zenject.SpaceFighter.PlayerDamageHandler_Settings Zenject.SpaceFighter.PlayerDamageHandler::_settings
	Settings_t48778C45589CCBEC26A7E67F855609F50926CB44 * ____settings_1;
	// Zenject.SpaceFighter.Player Zenject.SpaceFighter.PlayerDamageHandler::_player
	Player_t0644AFD1050593EF42B2459D30EA133F19E7A402 * ____player_2;

public:
	inline static int32_t get_offset_of__audioPlayer_0() { return static_cast<int32_t>(offsetof(PlayerDamageHandler_tFF3D703F066B3204ADD106FCC8B95D7EF61B4E33, ____audioPlayer_0)); }
	inline RuntimeObject* get__audioPlayer_0() const { return ____audioPlayer_0; }
	inline RuntimeObject** get_address_of__audioPlayer_0() { return &____audioPlayer_0; }
	inline void set__audioPlayer_0(RuntimeObject* value)
	{
		____audioPlayer_0 = value;
		Il2CppCodeGenWriteBarrier((&____audioPlayer_0), value);
	}

	inline static int32_t get_offset_of__settings_1() { return static_cast<int32_t>(offsetof(PlayerDamageHandler_tFF3D703F066B3204ADD106FCC8B95D7EF61B4E33, ____settings_1)); }
	inline Settings_t48778C45589CCBEC26A7E67F855609F50926CB44 * get__settings_1() const { return ____settings_1; }
	inline Settings_t48778C45589CCBEC26A7E67F855609F50926CB44 ** get_address_of__settings_1() { return &____settings_1; }
	inline void set__settings_1(Settings_t48778C45589CCBEC26A7E67F855609F50926CB44 * value)
	{
		____settings_1 = value;
		Il2CppCodeGenWriteBarrier((&____settings_1), value);
	}

	inline static int32_t get_offset_of__player_2() { return static_cast<int32_t>(offsetof(PlayerDamageHandler_tFF3D703F066B3204ADD106FCC8B95D7EF61B4E33, ____player_2)); }
	inline Player_t0644AFD1050593EF42B2459D30EA133F19E7A402 * get__player_2() const { return ____player_2; }
	inline Player_t0644AFD1050593EF42B2459D30EA133F19E7A402 ** get_address_of__player_2() { return &____player_2; }
	inline void set__player_2(Player_t0644AFD1050593EF42B2459D30EA133F19E7A402 * value)
	{
		____player_2 = value;
		Il2CppCodeGenWriteBarrier((&____player_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERDAMAGEHANDLER_TFF3D703F066B3204ADD106FCC8B95D7EF61B4E33_H
#ifndef SETTINGS_T48778C45589CCBEC26A7E67F855609F50926CB44_H
#define SETTINGS_T48778C45589CCBEC26A7E67F855609F50926CB44_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SpaceFighter.PlayerDamageHandler_Settings
struct  Settings_t48778C45589CCBEC26A7E67F855609F50926CB44  : public RuntimeObject
{
public:
	// System.Single Zenject.SpaceFighter.PlayerDamageHandler_Settings::HealthLoss
	float ___HealthLoss_0;
	// System.Single Zenject.SpaceFighter.PlayerDamageHandler_Settings::HitForce
	float ___HitForce_1;
	// UnityEngine.AudioClip Zenject.SpaceFighter.PlayerDamageHandler_Settings::HitSound
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___HitSound_2;
	// System.Single Zenject.SpaceFighter.PlayerDamageHandler_Settings::HitSoundVolume
	float ___HitSoundVolume_3;

public:
	inline static int32_t get_offset_of_HealthLoss_0() { return static_cast<int32_t>(offsetof(Settings_t48778C45589CCBEC26A7E67F855609F50926CB44, ___HealthLoss_0)); }
	inline float get_HealthLoss_0() const { return ___HealthLoss_0; }
	inline float* get_address_of_HealthLoss_0() { return &___HealthLoss_0; }
	inline void set_HealthLoss_0(float value)
	{
		___HealthLoss_0 = value;
	}

	inline static int32_t get_offset_of_HitForce_1() { return static_cast<int32_t>(offsetof(Settings_t48778C45589CCBEC26A7E67F855609F50926CB44, ___HitForce_1)); }
	inline float get_HitForce_1() const { return ___HitForce_1; }
	inline float* get_address_of_HitForce_1() { return &___HitForce_1; }
	inline void set_HitForce_1(float value)
	{
		___HitForce_1 = value;
	}

	inline static int32_t get_offset_of_HitSound_2() { return static_cast<int32_t>(offsetof(Settings_t48778C45589CCBEC26A7E67F855609F50926CB44, ___HitSound_2)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_HitSound_2() const { return ___HitSound_2; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_HitSound_2() { return &___HitSound_2; }
	inline void set_HitSound_2(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___HitSound_2 = value;
		Il2CppCodeGenWriteBarrier((&___HitSound_2), value);
	}

	inline static int32_t get_offset_of_HitSoundVolume_3() { return static_cast<int32_t>(offsetof(Settings_t48778C45589CCBEC26A7E67F855609F50926CB44, ___HitSoundVolume_3)); }
	inline float get_HitSoundVolume_3() const { return ___HitSoundVolume_3; }
	inline float* get_address_of_HitSoundVolume_3() { return &___HitSoundVolume_3; }
	inline void set_HitSoundVolume_3(float value)
	{
		___HitSoundVolume_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETTINGS_T48778C45589CCBEC26A7E67F855609F50926CB44_H
#ifndef PLAYERDIRECTIONHANDLER_T52F4A906A8020B45FCAB026F77360D1DC2A94987_H
#define PLAYERDIRECTIONHANDLER_T52F4A906A8020B45FCAB026F77360D1DC2A94987_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SpaceFighter.PlayerDirectionHandler
struct  PlayerDirectionHandler_t52F4A906A8020B45FCAB026F77360D1DC2A94987  : public RuntimeObject
{
public:
	// Zenject.SpaceFighter.Player Zenject.SpaceFighter.PlayerDirectionHandler::_player
	Player_t0644AFD1050593EF42B2459D30EA133F19E7A402 * ____player_0;
	// UnityEngine.Camera Zenject.SpaceFighter.PlayerDirectionHandler::_mainCamera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ____mainCamera_1;

public:
	inline static int32_t get_offset_of__player_0() { return static_cast<int32_t>(offsetof(PlayerDirectionHandler_t52F4A906A8020B45FCAB026F77360D1DC2A94987, ____player_0)); }
	inline Player_t0644AFD1050593EF42B2459D30EA133F19E7A402 * get__player_0() const { return ____player_0; }
	inline Player_t0644AFD1050593EF42B2459D30EA133F19E7A402 ** get_address_of__player_0() { return &____player_0; }
	inline void set__player_0(Player_t0644AFD1050593EF42B2459D30EA133F19E7A402 * value)
	{
		____player_0 = value;
		Il2CppCodeGenWriteBarrier((&____player_0), value);
	}

	inline static int32_t get_offset_of__mainCamera_1() { return static_cast<int32_t>(offsetof(PlayerDirectionHandler_t52F4A906A8020B45FCAB026F77360D1DC2A94987, ____mainCamera_1)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get__mainCamera_1() const { return ____mainCamera_1; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of__mainCamera_1() { return &____mainCamera_1; }
	inline void set__mainCamera_1(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		____mainCamera_1 = value;
		Il2CppCodeGenWriteBarrier((&____mainCamera_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERDIRECTIONHANDLER_T52F4A906A8020B45FCAB026F77360D1DC2A94987_H
#ifndef PLAYERHEALTHWATCHER_T6A90E0F2AF3D5C2C35AEB220E5BE9B375BCE4D96_H
#define PLAYERHEALTHWATCHER_T6A90E0F2AF3D5C2C35AEB220E5BE9B375BCE4D96_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SpaceFighter.PlayerHealthWatcher
struct  PlayerHealthWatcher_t6A90E0F2AF3D5C2C35AEB220E5BE9B375BCE4D96  : public RuntimeObject
{
public:
	// Zenject.SpaceFighter.PlayerDiedSignal Zenject.SpaceFighter.PlayerHealthWatcher::_playerDiedSignal
	PlayerDiedSignal_tD47ED6EA743FEE4E5D7B3289450C3EA59E018C7F * ____playerDiedSignal_0;
	// Zenject.SpaceFighter.IAudioPlayer Zenject.SpaceFighter.PlayerHealthWatcher::_audioPlayer
	RuntimeObject* ____audioPlayer_1;
	// Zenject.SpaceFighter.PlayerHealthWatcher_Settings Zenject.SpaceFighter.PlayerHealthWatcher::_settings
	Settings_t83D6CE3845CA378293BC213A44067B48638017A4 * ____settings_2;
	// Zenject.SpaceFighter.Explosion_Pool Zenject.SpaceFighter.PlayerHealthWatcher::_explosionPool
	Pool_tF6D222503E0FF0BBA73DDF77321A0C8B6CA1D7C7 * ____explosionPool_3;
	// Zenject.SpaceFighter.Player Zenject.SpaceFighter.PlayerHealthWatcher::_player
	Player_t0644AFD1050593EF42B2459D30EA133F19E7A402 * ____player_4;

public:
	inline static int32_t get_offset_of__playerDiedSignal_0() { return static_cast<int32_t>(offsetof(PlayerHealthWatcher_t6A90E0F2AF3D5C2C35AEB220E5BE9B375BCE4D96, ____playerDiedSignal_0)); }
	inline PlayerDiedSignal_tD47ED6EA743FEE4E5D7B3289450C3EA59E018C7F * get__playerDiedSignal_0() const { return ____playerDiedSignal_0; }
	inline PlayerDiedSignal_tD47ED6EA743FEE4E5D7B3289450C3EA59E018C7F ** get_address_of__playerDiedSignal_0() { return &____playerDiedSignal_0; }
	inline void set__playerDiedSignal_0(PlayerDiedSignal_tD47ED6EA743FEE4E5D7B3289450C3EA59E018C7F * value)
	{
		____playerDiedSignal_0 = value;
		Il2CppCodeGenWriteBarrier((&____playerDiedSignal_0), value);
	}

	inline static int32_t get_offset_of__audioPlayer_1() { return static_cast<int32_t>(offsetof(PlayerHealthWatcher_t6A90E0F2AF3D5C2C35AEB220E5BE9B375BCE4D96, ____audioPlayer_1)); }
	inline RuntimeObject* get__audioPlayer_1() const { return ____audioPlayer_1; }
	inline RuntimeObject** get_address_of__audioPlayer_1() { return &____audioPlayer_1; }
	inline void set__audioPlayer_1(RuntimeObject* value)
	{
		____audioPlayer_1 = value;
		Il2CppCodeGenWriteBarrier((&____audioPlayer_1), value);
	}

	inline static int32_t get_offset_of__settings_2() { return static_cast<int32_t>(offsetof(PlayerHealthWatcher_t6A90E0F2AF3D5C2C35AEB220E5BE9B375BCE4D96, ____settings_2)); }
	inline Settings_t83D6CE3845CA378293BC213A44067B48638017A4 * get__settings_2() const { return ____settings_2; }
	inline Settings_t83D6CE3845CA378293BC213A44067B48638017A4 ** get_address_of__settings_2() { return &____settings_2; }
	inline void set__settings_2(Settings_t83D6CE3845CA378293BC213A44067B48638017A4 * value)
	{
		____settings_2 = value;
		Il2CppCodeGenWriteBarrier((&____settings_2), value);
	}

	inline static int32_t get_offset_of__explosionPool_3() { return static_cast<int32_t>(offsetof(PlayerHealthWatcher_t6A90E0F2AF3D5C2C35AEB220E5BE9B375BCE4D96, ____explosionPool_3)); }
	inline Pool_tF6D222503E0FF0BBA73DDF77321A0C8B6CA1D7C7 * get__explosionPool_3() const { return ____explosionPool_3; }
	inline Pool_tF6D222503E0FF0BBA73DDF77321A0C8B6CA1D7C7 ** get_address_of__explosionPool_3() { return &____explosionPool_3; }
	inline void set__explosionPool_3(Pool_tF6D222503E0FF0BBA73DDF77321A0C8B6CA1D7C7 * value)
	{
		____explosionPool_3 = value;
		Il2CppCodeGenWriteBarrier((&____explosionPool_3), value);
	}

	inline static int32_t get_offset_of__player_4() { return static_cast<int32_t>(offsetof(PlayerHealthWatcher_t6A90E0F2AF3D5C2C35AEB220E5BE9B375BCE4D96, ____player_4)); }
	inline Player_t0644AFD1050593EF42B2459D30EA133F19E7A402 * get__player_4() const { return ____player_4; }
	inline Player_t0644AFD1050593EF42B2459D30EA133F19E7A402 ** get_address_of__player_4() { return &____player_4; }
	inline void set__player_4(Player_t0644AFD1050593EF42B2459D30EA133F19E7A402 * value)
	{
		____player_4 = value;
		Il2CppCodeGenWriteBarrier((&____player_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERHEALTHWATCHER_T6A90E0F2AF3D5C2C35AEB220E5BE9B375BCE4D96_H
#ifndef SETTINGS_T83D6CE3845CA378293BC213A44067B48638017A4_H
#define SETTINGS_T83D6CE3845CA378293BC213A44067B48638017A4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SpaceFighter.PlayerHealthWatcher_Settings
struct  Settings_t83D6CE3845CA378293BC213A44067B48638017A4  : public RuntimeObject
{
public:
	// UnityEngine.AudioClip Zenject.SpaceFighter.PlayerHealthWatcher_Settings::DeathSound
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___DeathSound_0;
	// System.Single Zenject.SpaceFighter.PlayerHealthWatcher_Settings::DeathSoundVolume
	float ___DeathSoundVolume_1;

public:
	inline static int32_t get_offset_of_DeathSound_0() { return static_cast<int32_t>(offsetof(Settings_t83D6CE3845CA378293BC213A44067B48638017A4, ___DeathSound_0)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_DeathSound_0() const { return ___DeathSound_0; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_DeathSound_0() { return &___DeathSound_0; }
	inline void set_DeathSound_0(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___DeathSound_0 = value;
		Il2CppCodeGenWriteBarrier((&___DeathSound_0), value);
	}

	inline static int32_t get_offset_of_DeathSoundVolume_1() { return static_cast<int32_t>(offsetof(Settings_t83D6CE3845CA378293BC213A44067B48638017A4, ___DeathSoundVolume_1)); }
	inline float get_DeathSoundVolume_1() const { return ___DeathSoundVolume_1; }
	inline float* get_address_of_DeathSoundVolume_1() { return &___DeathSoundVolume_1; }
	inline void set_DeathSoundVolume_1(float value)
	{
		___DeathSoundVolume_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETTINGS_T83D6CE3845CA378293BC213A44067B48638017A4_H
#ifndef PLAYERINPUTHANDLER_T39362CD94847EC5A56BAA3E06B877601EFCB8424_H
#define PLAYERINPUTHANDLER_T39362CD94847EC5A56BAA3E06B877601EFCB8424_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SpaceFighter.PlayerInputHandler
struct  PlayerInputHandler_t39362CD94847EC5A56BAA3E06B877601EFCB8424  : public RuntimeObject
{
public:
	// Zenject.SpaceFighter.PlayerInputState Zenject.SpaceFighter.PlayerInputHandler::_inputState
	PlayerInputState_t4B9BA63DFF6982E3A6FFB060DDE8C17DC0A83CF8 * ____inputState_0;

public:
	inline static int32_t get_offset_of__inputState_0() { return static_cast<int32_t>(offsetof(PlayerInputHandler_t39362CD94847EC5A56BAA3E06B877601EFCB8424, ____inputState_0)); }
	inline PlayerInputState_t4B9BA63DFF6982E3A6FFB060DDE8C17DC0A83CF8 * get__inputState_0() const { return ____inputState_0; }
	inline PlayerInputState_t4B9BA63DFF6982E3A6FFB060DDE8C17DC0A83CF8 ** get_address_of__inputState_0() { return &____inputState_0; }
	inline void set__inputState_0(PlayerInputState_t4B9BA63DFF6982E3A6FFB060DDE8C17DC0A83CF8 * value)
	{
		____inputState_0 = value;
		Il2CppCodeGenWriteBarrier((&____inputState_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERINPUTHANDLER_T39362CD94847EC5A56BAA3E06B877601EFCB8424_H
#ifndef PLAYERINPUTSTATE_T4B9BA63DFF6982E3A6FFB060DDE8C17DC0A83CF8_H
#define PLAYERINPUTSTATE_T4B9BA63DFF6982E3A6FFB060DDE8C17DC0A83CF8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SpaceFighter.PlayerInputState
struct  PlayerInputState_t4B9BA63DFF6982E3A6FFB060DDE8C17DC0A83CF8  : public RuntimeObject
{
public:
	// System.Boolean Zenject.SpaceFighter.PlayerInputState::<IsMovingLeft>k__BackingField
	bool ___U3CIsMovingLeftU3Ek__BackingField_0;
	// System.Boolean Zenject.SpaceFighter.PlayerInputState::<IsMovingRight>k__BackingField
	bool ___U3CIsMovingRightU3Ek__BackingField_1;
	// System.Boolean Zenject.SpaceFighter.PlayerInputState::<IsMovingUp>k__BackingField
	bool ___U3CIsMovingUpU3Ek__BackingField_2;
	// System.Boolean Zenject.SpaceFighter.PlayerInputState::<IsMovingDown>k__BackingField
	bool ___U3CIsMovingDownU3Ek__BackingField_3;
	// System.Boolean Zenject.SpaceFighter.PlayerInputState::<IsFiring>k__BackingField
	bool ___U3CIsFiringU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CIsMovingLeftU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(PlayerInputState_t4B9BA63DFF6982E3A6FFB060DDE8C17DC0A83CF8, ___U3CIsMovingLeftU3Ek__BackingField_0)); }
	inline bool get_U3CIsMovingLeftU3Ek__BackingField_0() const { return ___U3CIsMovingLeftU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CIsMovingLeftU3Ek__BackingField_0() { return &___U3CIsMovingLeftU3Ek__BackingField_0; }
	inline void set_U3CIsMovingLeftU3Ek__BackingField_0(bool value)
	{
		___U3CIsMovingLeftU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CIsMovingRightU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PlayerInputState_t4B9BA63DFF6982E3A6FFB060DDE8C17DC0A83CF8, ___U3CIsMovingRightU3Ek__BackingField_1)); }
	inline bool get_U3CIsMovingRightU3Ek__BackingField_1() const { return ___U3CIsMovingRightU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CIsMovingRightU3Ek__BackingField_1() { return &___U3CIsMovingRightU3Ek__BackingField_1; }
	inline void set_U3CIsMovingRightU3Ek__BackingField_1(bool value)
	{
		___U3CIsMovingRightU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CIsMovingUpU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(PlayerInputState_t4B9BA63DFF6982E3A6FFB060DDE8C17DC0A83CF8, ___U3CIsMovingUpU3Ek__BackingField_2)); }
	inline bool get_U3CIsMovingUpU3Ek__BackingField_2() const { return ___U3CIsMovingUpU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CIsMovingUpU3Ek__BackingField_2() { return &___U3CIsMovingUpU3Ek__BackingField_2; }
	inline void set_U3CIsMovingUpU3Ek__BackingField_2(bool value)
	{
		___U3CIsMovingUpU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CIsMovingDownU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(PlayerInputState_t4B9BA63DFF6982E3A6FFB060DDE8C17DC0A83CF8, ___U3CIsMovingDownU3Ek__BackingField_3)); }
	inline bool get_U3CIsMovingDownU3Ek__BackingField_3() const { return ___U3CIsMovingDownU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CIsMovingDownU3Ek__BackingField_3() { return &___U3CIsMovingDownU3Ek__BackingField_3; }
	inline void set_U3CIsMovingDownU3Ek__BackingField_3(bool value)
	{
		___U3CIsMovingDownU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CIsFiringU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(PlayerInputState_t4B9BA63DFF6982E3A6FFB060DDE8C17DC0A83CF8, ___U3CIsFiringU3Ek__BackingField_4)); }
	inline bool get_U3CIsFiringU3Ek__BackingField_4() const { return ___U3CIsFiringU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CIsFiringU3Ek__BackingField_4() { return &___U3CIsFiringU3Ek__BackingField_4; }
	inline void set_U3CIsFiringU3Ek__BackingField_4(bool value)
	{
		___U3CIsFiringU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERINPUTSTATE_T4B9BA63DFF6982E3A6FFB060DDE8C17DC0A83CF8_H
#ifndef SETTINGS_TA79246F22F290206CED693068D5770E633DD5CEE_H
#define SETTINGS_TA79246F22F290206CED693068D5770E633DD5CEE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SpaceFighter.PlayerInstaller_Settings
struct  Settings_tA79246F22F290206CED693068D5770E633DD5CEE  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody Zenject.SpaceFighter.PlayerInstaller_Settings::Rigidbody
	Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * ___Rigidbody_0;
	// UnityEngine.MeshRenderer Zenject.SpaceFighter.PlayerInstaller_Settings::MeshRenderer
	MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * ___MeshRenderer_1;

public:
	inline static int32_t get_offset_of_Rigidbody_0() { return static_cast<int32_t>(offsetof(Settings_tA79246F22F290206CED693068D5770E633DD5CEE, ___Rigidbody_0)); }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * get_Rigidbody_0() const { return ___Rigidbody_0; }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 ** get_address_of_Rigidbody_0() { return &___Rigidbody_0; }
	inline void set_Rigidbody_0(Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * value)
	{
		___Rigidbody_0 = value;
		Il2CppCodeGenWriteBarrier((&___Rigidbody_0), value);
	}

	inline static int32_t get_offset_of_MeshRenderer_1() { return static_cast<int32_t>(offsetof(Settings_tA79246F22F290206CED693068D5770E633DD5CEE, ___MeshRenderer_1)); }
	inline MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * get_MeshRenderer_1() const { return ___MeshRenderer_1; }
	inline MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED ** get_address_of_MeshRenderer_1() { return &___MeshRenderer_1; }
	inline void set_MeshRenderer_1(MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * value)
	{
		___MeshRenderer_1 = value;
		Il2CppCodeGenWriteBarrier((&___MeshRenderer_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETTINGS_TA79246F22F290206CED693068D5770E633DD5CEE_H
#ifndef PLAYERMOVEHANDLER_T55E7B67AABCC9E514AA8C9BF19D61505BFA7CCDD_H
#define PLAYERMOVEHANDLER_T55E7B67AABCC9E514AA8C9BF19D61505BFA7CCDD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SpaceFighter.PlayerMoveHandler
struct  PlayerMoveHandler_t55E7B67AABCC9E514AA8C9BF19D61505BFA7CCDD  : public RuntimeObject
{
public:
	// Zenject.SpaceFighter.LevelBoundary Zenject.SpaceFighter.PlayerMoveHandler::_levelBoundary
	LevelBoundary_t8E706FF3A27DD22A30FE8E79EF745B6B7D78577B * ____levelBoundary_0;
	// Zenject.SpaceFighter.PlayerMoveHandler_Settings Zenject.SpaceFighter.PlayerMoveHandler::_settings
	Settings_t5D29F620DD8E2C4750D3670B9D88BD2F6F4C6FC5 * ____settings_1;
	// Zenject.SpaceFighter.Player Zenject.SpaceFighter.PlayerMoveHandler::_player
	Player_t0644AFD1050593EF42B2459D30EA133F19E7A402 * ____player_2;
	// Zenject.SpaceFighter.PlayerInputState Zenject.SpaceFighter.PlayerMoveHandler::_inputState
	PlayerInputState_t4B9BA63DFF6982E3A6FFB060DDE8C17DC0A83CF8 * ____inputState_3;

public:
	inline static int32_t get_offset_of__levelBoundary_0() { return static_cast<int32_t>(offsetof(PlayerMoveHandler_t55E7B67AABCC9E514AA8C9BF19D61505BFA7CCDD, ____levelBoundary_0)); }
	inline LevelBoundary_t8E706FF3A27DD22A30FE8E79EF745B6B7D78577B * get__levelBoundary_0() const { return ____levelBoundary_0; }
	inline LevelBoundary_t8E706FF3A27DD22A30FE8E79EF745B6B7D78577B ** get_address_of__levelBoundary_0() { return &____levelBoundary_0; }
	inline void set__levelBoundary_0(LevelBoundary_t8E706FF3A27DD22A30FE8E79EF745B6B7D78577B * value)
	{
		____levelBoundary_0 = value;
		Il2CppCodeGenWriteBarrier((&____levelBoundary_0), value);
	}

	inline static int32_t get_offset_of__settings_1() { return static_cast<int32_t>(offsetof(PlayerMoveHandler_t55E7B67AABCC9E514AA8C9BF19D61505BFA7CCDD, ____settings_1)); }
	inline Settings_t5D29F620DD8E2C4750D3670B9D88BD2F6F4C6FC5 * get__settings_1() const { return ____settings_1; }
	inline Settings_t5D29F620DD8E2C4750D3670B9D88BD2F6F4C6FC5 ** get_address_of__settings_1() { return &____settings_1; }
	inline void set__settings_1(Settings_t5D29F620DD8E2C4750D3670B9D88BD2F6F4C6FC5 * value)
	{
		____settings_1 = value;
		Il2CppCodeGenWriteBarrier((&____settings_1), value);
	}

	inline static int32_t get_offset_of__player_2() { return static_cast<int32_t>(offsetof(PlayerMoveHandler_t55E7B67AABCC9E514AA8C9BF19D61505BFA7CCDD, ____player_2)); }
	inline Player_t0644AFD1050593EF42B2459D30EA133F19E7A402 * get__player_2() const { return ____player_2; }
	inline Player_t0644AFD1050593EF42B2459D30EA133F19E7A402 ** get_address_of__player_2() { return &____player_2; }
	inline void set__player_2(Player_t0644AFD1050593EF42B2459D30EA133F19E7A402 * value)
	{
		____player_2 = value;
		Il2CppCodeGenWriteBarrier((&____player_2), value);
	}

	inline static int32_t get_offset_of__inputState_3() { return static_cast<int32_t>(offsetof(PlayerMoveHandler_t55E7B67AABCC9E514AA8C9BF19D61505BFA7CCDD, ____inputState_3)); }
	inline PlayerInputState_t4B9BA63DFF6982E3A6FFB060DDE8C17DC0A83CF8 * get__inputState_3() const { return ____inputState_3; }
	inline PlayerInputState_t4B9BA63DFF6982E3A6FFB060DDE8C17DC0A83CF8 ** get_address_of__inputState_3() { return &____inputState_3; }
	inline void set__inputState_3(PlayerInputState_t4B9BA63DFF6982E3A6FFB060DDE8C17DC0A83CF8 * value)
	{
		____inputState_3 = value;
		Il2CppCodeGenWriteBarrier((&____inputState_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERMOVEHANDLER_T55E7B67AABCC9E514AA8C9BF19D61505BFA7CCDD_H
#ifndef SETTINGS_T5D29F620DD8E2C4750D3670B9D88BD2F6F4C6FC5_H
#define SETTINGS_T5D29F620DD8E2C4750D3670B9D88BD2F6F4C6FC5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SpaceFighter.PlayerMoveHandler_Settings
struct  Settings_t5D29F620DD8E2C4750D3670B9D88BD2F6F4C6FC5  : public RuntimeObject
{
public:
	// System.Single Zenject.SpaceFighter.PlayerMoveHandler_Settings::BoundaryBuffer
	float ___BoundaryBuffer_0;
	// System.Single Zenject.SpaceFighter.PlayerMoveHandler_Settings::BoundaryAdjustForce
	float ___BoundaryAdjustForce_1;
	// System.Single Zenject.SpaceFighter.PlayerMoveHandler_Settings::MoveSpeed
	float ___MoveSpeed_2;
	// System.Single Zenject.SpaceFighter.PlayerMoveHandler_Settings::SlowDownSpeed
	float ___SlowDownSpeed_3;

public:
	inline static int32_t get_offset_of_BoundaryBuffer_0() { return static_cast<int32_t>(offsetof(Settings_t5D29F620DD8E2C4750D3670B9D88BD2F6F4C6FC5, ___BoundaryBuffer_0)); }
	inline float get_BoundaryBuffer_0() const { return ___BoundaryBuffer_0; }
	inline float* get_address_of_BoundaryBuffer_0() { return &___BoundaryBuffer_0; }
	inline void set_BoundaryBuffer_0(float value)
	{
		___BoundaryBuffer_0 = value;
	}

	inline static int32_t get_offset_of_BoundaryAdjustForce_1() { return static_cast<int32_t>(offsetof(Settings_t5D29F620DD8E2C4750D3670B9D88BD2F6F4C6FC5, ___BoundaryAdjustForce_1)); }
	inline float get_BoundaryAdjustForce_1() const { return ___BoundaryAdjustForce_1; }
	inline float* get_address_of_BoundaryAdjustForce_1() { return &___BoundaryAdjustForce_1; }
	inline void set_BoundaryAdjustForce_1(float value)
	{
		___BoundaryAdjustForce_1 = value;
	}

	inline static int32_t get_offset_of_MoveSpeed_2() { return static_cast<int32_t>(offsetof(Settings_t5D29F620DD8E2C4750D3670B9D88BD2F6F4C6FC5, ___MoveSpeed_2)); }
	inline float get_MoveSpeed_2() const { return ___MoveSpeed_2; }
	inline float* get_address_of_MoveSpeed_2() { return &___MoveSpeed_2; }
	inline void set_MoveSpeed_2(float value)
	{
		___MoveSpeed_2 = value;
	}

	inline static int32_t get_offset_of_SlowDownSpeed_3() { return static_cast<int32_t>(offsetof(Settings_t5D29F620DD8E2C4750D3670B9D88BD2F6F4C6FC5, ___SlowDownSpeed_3)); }
	inline float get_SlowDownSpeed_3() const { return ___SlowDownSpeed_3; }
	inline float* get_address_of_SlowDownSpeed_3() { return &___SlowDownSpeed_3; }
	inline void set_SlowDownSpeed_3(float value)
	{
		___SlowDownSpeed_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETTINGS_T5D29F620DD8E2C4750D3670B9D88BD2F6F4C6FC5_H
#ifndef PLAYERSHOOTHANDLER_T682E0E91FADE0258EA860E263BC6B6EBDA21F202_H
#define PLAYERSHOOTHANDLER_T682E0E91FADE0258EA860E263BC6B6EBDA21F202_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SpaceFighter.PlayerShootHandler
struct  PlayerShootHandler_t682E0E91FADE0258EA860E263BC6B6EBDA21F202  : public RuntimeObject
{
public:
	// Zenject.SpaceFighter.IAudioPlayer Zenject.SpaceFighter.PlayerShootHandler::_audioPlayer
	RuntimeObject* ____audioPlayer_0;
	// Zenject.SpaceFighter.Player Zenject.SpaceFighter.PlayerShootHandler::_player
	Player_t0644AFD1050593EF42B2459D30EA133F19E7A402 * ____player_1;
	// Zenject.SpaceFighter.PlayerShootHandler_Settings Zenject.SpaceFighter.PlayerShootHandler::_settings
	Settings_t59B948D04E16E5F614372F81DC0B8066A189FF9D * ____settings_2;
	// Zenject.SpaceFighter.Bullet_Pool Zenject.SpaceFighter.PlayerShootHandler::_bulletPool
	Pool_t28D3C271D1214C1C919D02B9C38A6A5698F231F1 * ____bulletPool_3;
	// Zenject.SpaceFighter.PlayerInputState Zenject.SpaceFighter.PlayerShootHandler::_inputState
	PlayerInputState_t4B9BA63DFF6982E3A6FFB060DDE8C17DC0A83CF8 * ____inputState_4;
	// System.Single Zenject.SpaceFighter.PlayerShootHandler::_lastFireTime
	float ____lastFireTime_5;

public:
	inline static int32_t get_offset_of__audioPlayer_0() { return static_cast<int32_t>(offsetof(PlayerShootHandler_t682E0E91FADE0258EA860E263BC6B6EBDA21F202, ____audioPlayer_0)); }
	inline RuntimeObject* get__audioPlayer_0() const { return ____audioPlayer_0; }
	inline RuntimeObject** get_address_of__audioPlayer_0() { return &____audioPlayer_0; }
	inline void set__audioPlayer_0(RuntimeObject* value)
	{
		____audioPlayer_0 = value;
		Il2CppCodeGenWriteBarrier((&____audioPlayer_0), value);
	}

	inline static int32_t get_offset_of__player_1() { return static_cast<int32_t>(offsetof(PlayerShootHandler_t682E0E91FADE0258EA860E263BC6B6EBDA21F202, ____player_1)); }
	inline Player_t0644AFD1050593EF42B2459D30EA133F19E7A402 * get__player_1() const { return ____player_1; }
	inline Player_t0644AFD1050593EF42B2459D30EA133F19E7A402 ** get_address_of__player_1() { return &____player_1; }
	inline void set__player_1(Player_t0644AFD1050593EF42B2459D30EA133F19E7A402 * value)
	{
		____player_1 = value;
		Il2CppCodeGenWriteBarrier((&____player_1), value);
	}

	inline static int32_t get_offset_of__settings_2() { return static_cast<int32_t>(offsetof(PlayerShootHandler_t682E0E91FADE0258EA860E263BC6B6EBDA21F202, ____settings_2)); }
	inline Settings_t59B948D04E16E5F614372F81DC0B8066A189FF9D * get__settings_2() const { return ____settings_2; }
	inline Settings_t59B948D04E16E5F614372F81DC0B8066A189FF9D ** get_address_of__settings_2() { return &____settings_2; }
	inline void set__settings_2(Settings_t59B948D04E16E5F614372F81DC0B8066A189FF9D * value)
	{
		____settings_2 = value;
		Il2CppCodeGenWriteBarrier((&____settings_2), value);
	}

	inline static int32_t get_offset_of__bulletPool_3() { return static_cast<int32_t>(offsetof(PlayerShootHandler_t682E0E91FADE0258EA860E263BC6B6EBDA21F202, ____bulletPool_3)); }
	inline Pool_t28D3C271D1214C1C919D02B9C38A6A5698F231F1 * get__bulletPool_3() const { return ____bulletPool_3; }
	inline Pool_t28D3C271D1214C1C919D02B9C38A6A5698F231F1 ** get_address_of__bulletPool_3() { return &____bulletPool_3; }
	inline void set__bulletPool_3(Pool_t28D3C271D1214C1C919D02B9C38A6A5698F231F1 * value)
	{
		____bulletPool_3 = value;
		Il2CppCodeGenWriteBarrier((&____bulletPool_3), value);
	}

	inline static int32_t get_offset_of__inputState_4() { return static_cast<int32_t>(offsetof(PlayerShootHandler_t682E0E91FADE0258EA860E263BC6B6EBDA21F202, ____inputState_4)); }
	inline PlayerInputState_t4B9BA63DFF6982E3A6FFB060DDE8C17DC0A83CF8 * get__inputState_4() const { return ____inputState_4; }
	inline PlayerInputState_t4B9BA63DFF6982E3A6FFB060DDE8C17DC0A83CF8 ** get_address_of__inputState_4() { return &____inputState_4; }
	inline void set__inputState_4(PlayerInputState_t4B9BA63DFF6982E3A6FFB060DDE8C17DC0A83CF8 * value)
	{
		____inputState_4 = value;
		Il2CppCodeGenWriteBarrier((&____inputState_4), value);
	}

	inline static int32_t get_offset_of__lastFireTime_5() { return static_cast<int32_t>(offsetof(PlayerShootHandler_t682E0E91FADE0258EA860E263BC6B6EBDA21F202, ____lastFireTime_5)); }
	inline float get__lastFireTime_5() const { return ____lastFireTime_5; }
	inline float* get_address_of__lastFireTime_5() { return &____lastFireTime_5; }
	inline void set__lastFireTime_5(float value)
	{
		____lastFireTime_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERSHOOTHANDLER_T682E0E91FADE0258EA860E263BC6B6EBDA21F202_H
#ifndef SETTINGS_T59B948D04E16E5F614372F81DC0B8066A189FF9D_H
#define SETTINGS_T59B948D04E16E5F614372F81DC0B8066A189FF9D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SpaceFighter.PlayerShootHandler_Settings
struct  Settings_t59B948D04E16E5F614372F81DC0B8066A189FF9D  : public RuntimeObject
{
public:
	// UnityEngine.AudioClip Zenject.SpaceFighter.PlayerShootHandler_Settings::Laser
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___Laser_0;
	// System.Single Zenject.SpaceFighter.PlayerShootHandler_Settings::LaserVolume
	float ___LaserVolume_1;
	// System.Single Zenject.SpaceFighter.PlayerShootHandler_Settings::BulletLifetime
	float ___BulletLifetime_2;
	// System.Single Zenject.SpaceFighter.PlayerShootHandler_Settings::BulletSpeed
	float ___BulletSpeed_3;
	// System.Single Zenject.SpaceFighter.PlayerShootHandler_Settings::MaxShootInterval
	float ___MaxShootInterval_4;
	// System.Single Zenject.SpaceFighter.PlayerShootHandler_Settings::BulletOffsetDistance
	float ___BulletOffsetDistance_5;

public:
	inline static int32_t get_offset_of_Laser_0() { return static_cast<int32_t>(offsetof(Settings_t59B948D04E16E5F614372F81DC0B8066A189FF9D, ___Laser_0)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_Laser_0() const { return ___Laser_0; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_Laser_0() { return &___Laser_0; }
	inline void set_Laser_0(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___Laser_0 = value;
		Il2CppCodeGenWriteBarrier((&___Laser_0), value);
	}

	inline static int32_t get_offset_of_LaserVolume_1() { return static_cast<int32_t>(offsetof(Settings_t59B948D04E16E5F614372F81DC0B8066A189FF9D, ___LaserVolume_1)); }
	inline float get_LaserVolume_1() const { return ___LaserVolume_1; }
	inline float* get_address_of_LaserVolume_1() { return &___LaserVolume_1; }
	inline void set_LaserVolume_1(float value)
	{
		___LaserVolume_1 = value;
	}

	inline static int32_t get_offset_of_BulletLifetime_2() { return static_cast<int32_t>(offsetof(Settings_t59B948D04E16E5F614372F81DC0B8066A189FF9D, ___BulletLifetime_2)); }
	inline float get_BulletLifetime_2() const { return ___BulletLifetime_2; }
	inline float* get_address_of_BulletLifetime_2() { return &___BulletLifetime_2; }
	inline void set_BulletLifetime_2(float value)
	{
		___BulletLifetime_2 = value;
	}

	inline static int32_t get_offset_of_BulletSpeed_3() { return static_cast<int32_t>(offsetof(Settings_t59B948D04E16E5F614372F81DC0B8066A189FF9D, ___BulletSpeed_3)); }
	inline float get_BulletSpeed_3() const { return ___BulletSpeed_3; }
	inline float* get_address_of_BulletSpeed_3() { return &___BulletSpeed_3; }
	inline void set_BulletSpeed_3(float value)
	{
		___BulletSpeed_3 = value;
	}

	inline static int32_t get_offset_of_MaxShootInterval_4() { return static_cast<int32_t>(offsetof(Settings_t59B948D04E16E5F614372F81DC0B8066A189FF9D, ___MaxShootInterval_4)); }
	inline float get_MaxShootInterval_4() const { return ___MaxShootInterval_4; }
	inline float* get_address_of_MaxShootInterval_4() { return &___MaxShootInterval_4; }
	inline void set_MaxShootInterval_4(float value)
	{
		___MaxShootInterval_4 = value;
	}

	inline static int32_t get_offset_of_BulletOffsetDistance_5() { return static_cast<int32_t>(offsetof(Settings_t59B948D04E16E5F614372F81DC0B8066A189FF9D, ___BulletOffsetDistance_5)); }
	inline float get_BulletOffsetDistance_5() const { return ___BulletOffsetDistance_5; }
	inline float* get_address_of_BulletOffsetDistance_5() { return &___BulletOffsetDistance_5; }
	inline void set_BulletOffsetDistance_5(float value)
	{
		___BulletOffsetDistance_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETTINGS_T59B948D04E16E5F614372F81DC0B8066A189FF9D_H
#ifndef U3CU3EC_T6B8229F9C3E925210F1B9248E9FAE279D6350DA5_H
#define U3CU3EC_T6B8229F9C3E925210F1B9248E9FAE279D6350DA5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.TypeAnalyzer_<>c
struct  U3CU3Ec_t6B8229F9C3E925210F1B9248E9FAE279D6350DA5  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t6B8229F9C3E925210F1B9248E9FAE279D6350DA5_StaticFields
{
public:
	// Zenject.TypeAnalyzer_<>c Zenject.TypeAnalyzer_<>c::<>9
	U3CU3Ec_t6B8229F9C3E925210F1B9248E9FAE279D6350DA5 * ___U3CU3E9_0;
	// System.Func`2<System.Reflection.MethodInfo,System.Boolean> Zenject.TypeAnalyzer_<>c::<>9__6_0
	Func_2_t25BDD008BABA5EB0719A205177AD1FB36DC6A438 * ___U3CU3E9__6_0_1;
	// System.Func`2<System.Reflection.PropertyInfo,System.Boolean> Zenject.TypeAnalyzer_<>c::<>9__7_0
	Func_2_tF059A6F3E6CDF153D0E7F4B9A9B8AB75DBA4BB7C * ___U3CU3E9__7_0_2;
	// System.Func`2<System.Reflection.FieldInfo,System.Boolean> Zenject.TypeAnalyzer_<>c::<>9__8_0
	Func_2_t8B737A62DCD6BCDB37818A2DFF357973A9317E8D * ___U3CU3E9__8_0_3;
	// System.Func`2<System.Reflection.FieldInfo,System.String> Zenject.TypeAnalyzer_<>c::<>9__10_1
	Func_2_tB05273985A55D6B76307D55CDF577A8EE8D2E3E2 * ___U3CU3E9__10_1_4;
	// System.Func`2<System.Reflection.ConstructorInfo,System.Boolean> Zenject.TypeAnalyzer_<>c::<>9__12_0
	Func_2_tE50BC1D78F3969472F5276AA4812403240D166E3 * ___U3CU3E9__12_0_5;
	// System.Func`2<System.Reflection.ConstructorInfo,System.Boolean> Zenject.TypeAnalyzer_<>c::<>9__12_1
	Func_2_tE50BC1D78F3969472F5276AA4812403240D166E3 * ___U3CU3E9__12_1_6;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t6B8229F9C3E925210F1B9248E9FAE279D6350DA5_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t6B8229F9C3E925210F1B9248E9FAE279D6350DA5 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t6B8229F9C3E925210F1B9248E9FAE279D6350DA5 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t6B8229F9C3E925210F1B9248E9FAE279D6350DA5 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__6_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t6B8229F9C3E925210F1B9248E9FAE279D6350DA5_StaticFields, ___U3CU3E9__6_0_1)); }
	inline Func_2_t25BDD008BABA5EB0719A205177AD1FB36DC6A438 * get_U3CU3E9__6_0_1() const { return ___U3CU3E9__6_0_1; }
	inline Func_2_t25BDD008BABA5EB0719A205177AD1FB36DC6A438 ** get_address_of_U3CU3E9__6_0_1() { return &___U3CU3E9__6_0_1; }
	inline void set_U3CU3E9__6_0_1(Func_2_t25BDD008BABA5EB0719A205177AD1FB36DC6A438 * value)
	{
		___U3CU3E9__6_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__6_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__7_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t6B8229F9C3E925210F1B9248E9FAE279D6350DA5_StaticFields, ___U3CU3E9__7_0_2)); }
	inline Func_2_tF059A6F3E6CDF153D0E7F4B9A9B8AB75DBA4BB7C * get_U3CU3E9__7_0_2() const { return ___U3CU3E9__7_0_2; }
	inline Func_2_tF059A6F3E6CDF153D0E7F4B9A9B8AB75DBA4BB7C ** get_address_of_U3CU3E9__7_0_2() { return &___U3CU3E9__7_0_2; }
	inline void set_U3CU3E9__7_0_2(Func_2_tF059A6F3E6CDF153D0E7F4B9A9B8AB75DBA4BB7C * value)
	{
		___U3CU3E9__7_0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__7_0_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__8_0_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t6B8229F9C3E925210F1B9248E9FAE279D6350DA5_StaticFields, ___U3CU3E9__8_0_3)); }
	inline Func_2_t8B737A62DCD6BCDB37818A2DFF357973A9317E8D * get_U3CU3E9__8_0_3() const { return ___U3CU3E9__8_0_3; }
	inline Func_2_t8B737A62DCD6BCDB37818A2DFF357973A9317E8D ** get_address_of_U3CU3E9__8_0_3() { return &___U3CU3E9__8_0_3; }
	inline void set_U3CU3E9__8_0_3(Func_2_t8B737A62DCD6BCDB37818A2DFF357973A9317E8D * value)
	{
		___U3CU3E9__8_0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__8_0_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__10_1_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_t6B8229F9C3E925210F1B9248E9FAE279D6350DA5_StaticFields, ___U3CU3E9__10_1_4)); }
	inline Func_2_tB05273985A55D6B76307D55CDF577A8EE8D2E3E2 * get_U3CU3E9__10_1_4() const { return ___U3CU3E9__10_1_4; }
	inline Func_2_tB05273985A55D6B76307D55CDF577A8EE8D2E3E2 ** get_address_of_U3CU3E9__10_1_4() { return &___U3CU3E9__10_1_4; }
	inline void set_U3CU3E9__10_1_4(Func_2_tB05273985A55D6B76307D55CDF577A8EE8D2E3E2 * value)
	{
		___U3CU3E9__10_1_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__10_1_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__12_0_5() { return static_cast<int32_t>(offsetof(U3CU3Ec_t6B8229F9C3E925210F1B9248E9FAE279D6350DA5_StaticFields, ___U3CU3E9__12_0_5)); }
	inline Func_2_tE50BC1D78F3969472F5276AA4812403240D166E3 * get_U3CU3E9__12_0_5() const { return ___U3CU3E9__12_0_5; }
	inline Func_2_tE50BC1D78F3969472F5276AA4812403240D166E3 ** get_address_of_U3CU3E9__12_0_5() { return &___U3CU3E9__12_0_5; }
	inline void set_U3CU3E9__12_0_5(Func_2_tE50BC1D78F3969472F5276AA4812403240D166E3 * value)
	{
		___U3CU3E9__12_0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__12_0_5), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__12_1_6() { return static_cast<int32_t>(offsetof(U3CU3Ec_t6B8229F9C3E925210F1B9248E9FAE279D6350DA5_StaticFields, ___U3CU3E9__12_1_6)); }
	inline Func_2_tE50BC1D78F3969472F5276AA4812403240D166E3 * get_U3CU3E9__12_1_6() const { return ___U3CU3E9__12_1_6; }
	inline Func_2_tE50BC1D78F3969472F5276AA4812403240D166E3 ** get_address_of_U3CU3E9__12_1_6() { return &___U3CU3E9__12_1_6; }
	inline void set_U3CU3E9__12_1_6(Func_2_tE50BC1D78F3969472F5276AA4812403240D166E3 * value)
	{
		___U3CU3E9__12_1_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__12_1_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T6B8229F9C3E925210F1B9248E9FAE279D6350DA5_H
#ifndef U3CU3EC__DISPLAYCLASS10_0_T6BD5346515FBB2211D02553E1640EFBC794B9921_H
#define U3CU3EC__DISPLAYCLASS10_0_T6BD5346515FBB2211D02553E1640EFBC794B9921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.TypeAnalyzer_<>c__DisplayClass10_0
struct  U3CU3Ec__DisplayClass10_0_t6BD5346515FBB2211D02553E1640EFBC794B9921  : public RuntimeObject
{
public:
	// System.String Zenject.TypeAnalyzer_<>c__DisplayClass10_0::propertyName
	String_t* ___propertyName_0;
	// System.Collections.Generic.List`1<System.Reflection.FieldInfo> Zenject.TypeAnalyzer_<>c__DisplayClass10_0::writeableFields
	List_1_t0F52CF0559ED3BE13F054035F8208B5FDEB9D9DA * ___writeableFields_1;

public:
	inline static int32_t get_offset_of_propertyName_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass10_0_t6BD5346515FBB2211D02553E1640EFBC794B9921, ___propertyName_0)); }
	inline String_t* get_propertyName_0() const { return ___propertyName_0; }
	inline String_t** get_address_of_propertyName_0() { return &___propertyName_0; }
	inline void set_propertyName_0(String_t* value)
	{
		___propertyName_0 = value;
		Il2CppCodeGenWriteBarrier((&___propertyName_0), value);
	}

	inline static int32_t get_offset_of_writeableFields_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass10_0_t6BD5346515FBB2211D02553E1640EFBC794B9921, ___writeableFields_1)); }
	inline List_1_t0F52CF0559ED3BE13F054035F8208B5FDEB9D9DA * get_writeableFields_1() const { return ___writeableFields_1; }
	inline List_1_t0F52CF0559ED3BE13F054035F8208B5FDEB9D9DA ** get_address_of_writeableFields_1() { return &___writeableFields_1; }
	inline void set_writeableFields_1(List_1_t0F52CF0559ED3BE13F054035F8208B5FDEB9D9DA * value)
	{
		___writeableFields_1 = value;
		Il2CppCodeGenWriteBarrier((&___writeableFields_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS10_0_T6BD5346515FBB2211D02553E1640EFBC794B9921_H
#ifndef U3CU3EC__DISPLAYCLASS10_1_TF40BC40F6F52827485B66F6FD029EADADFACBD08_H
#define U3CU3EC__DISPLAYCLASS10_1_TF40BC40F6F52827485B66F6FD029EADADFACBD08_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.TypeAnalyzer_<>c__DisplayClass10_1
struct  U3CU3Ec__DisplayClass10_1_tF40BC40F6F52827485B66F6FD029EADADFACBD08  : public RuntimeObject
{
public:
	// System.Object Zenject.TypeAnalyzer_<>c__DisplayClass10_1::injectable
	RuntimeObject * ___injectable_0;
	// System.Object Zenject.TypeAnalyzer_<>c__DisplayClass10_1::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_injectable_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass10_1_tF40BC40F6F52827485B66F6FD029EADADFACBD08, ___injectable_0)); }
	inline RuntimeObject * get_injectable_0() const { return ___injectable_0; }
	inline RuntimeObject ** get_address_of_injectable_0() { return &___injectable_0; }
	inline void set_injectable_0(RuntimeObject * value)
	{
		___injectable_0 = value;
		Il2CppCodeGenWriteBarrier((&___injectable_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass10_1_tF40BC40F6F52827485B66F6FD029EADADFACBD08, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS10_1_TF40BC40F6F52827485B66F6FD029EADADFACBD08_H
#ifndef U3CU3EC__DISPLAYCLASS11_0_T1F43124971BB2CF876F2FBA075366F26838B7AED_H
#define U3CU3EC__DISPLAYCLASS11_0_T1F43124971BB2CF876F2FBA075366F26838B7AED_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.TypeAnalyzer_<>c__DisplayClass11_0
struct  U3CU3Ec__DisplayClass11_0_t1F43124971BB2CF876F2FBA075366F26838B7AED  : public RuntimeObject
{
public:
	// System.Reflection.FieldInfo Zenject.TypeAnalyzer_<>c__DisplayClass11_0::fieldInfo
	FieldInfo_t * ___fieldInfo_0;

public:
	inline static int32_t get_offset_of_fieldInfo_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass11_0_t1F43124971BB2CF876F2FBA075366F26838B7AED, ___fieldInfo_0)); }
	inline FieldInfo_t * get_fieldInfo_0() const { return ___fieldInfo_0; }
	inline FieldInfo_t ** get_address_of_fieldInfo_0() { return &___fieldInfo_0; }
	inline void set_fieldInfo_0(FieldInfo_t * value)
	{
		___fieldInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___fieldInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS11_0_T1F43124971BB2CF876F2FBA075366F26838B7AED_H
#ifndef U3CU3EC__DISPLAYCLASS11_1_T4B2232A5D21750B3EE736D56D4B58B689651FA06_H
#define U3CU3EC__DISPLAYCLASS11_1_T4B2232A5D21750B3EE736D56D4B58B689651FA06_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.TypeAnalyzer_<>c__DisplayClass11_1
struct  U3CU3Ec__DisplayClass11_1_t4B2232A5D21750B3EE736D56D4B58B689651FA06  : public RuntimeObject
{
public:
	// System.Reflection.PropertyInfo Zenject.TypeAnalyzer_<>c__DisplayClass11_1::propInfo
	PropertyInfo_t * ___propInfo_0;

public:
	inline static int32_t get_offset_of_propInfo_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass11_1_t4B2232A5D21750B3EE736D56D4B58B689651FA06, ___propInfo_0)); }
	inline PropertyInfo_t * get_propInfo_0() const { return ___propInfo_0; }
	inline PropertyInfo_t ** get_address_of_propInfo_0() { return &___propInfo_0; }
	inline void set_propInfo_0(PropertyInfo_t * value)
	{
		___propInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___propInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS11_1_T4B2232A5D21750B3EE736D56D4B58B689651FA06_H
#ifndef U3CU3EC__DISPLAYCLASS4_0_TA9861CADB22C325532D04EAECC151A608112DB1D_H
#define U3CU3EC__DISPLAYCLASS4_0_TA9861CADB22C325532D04EAECC151A608112DB1D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.TypeAnalyzer_<>c__DisplayClass4_0
struct  U3CU3Ec__DisplayClass4_0_tA9861CADB22C325532D04EAECC151A608112DB1D  : public RuntimeObject
{
public:
	// System.Type Zenject.TypeAnalyzer_<>c__DisplayClass4_0::parentType
	Type_t * ___parentType_0;

public:
	inline static int32_t get_offset_of_parentType_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_tA9861CADB22C325532D04EAECC151A608112DB1D, ___parentType_0)); }
	inline Type_t * get_parentType_0() const { return ___parentType_0; }
	inline Type_t ** get_address_of_parentType_0() { return &___parentType_0; }
	inline void set_parentType_0(Type_t * value)
	{
		___parentType_0 = value;
		Il2CppCodeGenWriteBarrier((&___parentType_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS4_0_TA9861CADB22C325532D04EAECC151A608112DB1D_H
#ifndef U3CU3EC__DISPLAYCLASS6_0_T3B07096FF650FEB05B494E1A1B3C564BF7A788F8_H
#define U3CU3EC__DISPLAYCLASS6_0_T3B07096FF650FEB05B494E1A1B3C564BF7A788F8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.TypeAnalyzer_<>c__DisplayClass6_0
struct  U3CU3Ec__DisplayClass6_0_t3B07096FF650FEB05B494E1A1B3C564BF7A788F8  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.Type> Zenject.TypeAnalyzer_<>c__DisplayClass6_0::heirarchyList
	List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0 * ___heirarchyList_0;
	// System.Type Zenject.TypeAnalyzer_<>c__DisplayClass6_0::type
	Type_t * ___type_1;
	// System.Func`2<System.Reflection.ParameterInfo,Zenject.InjectableInfo> Zenject.TypeAnalyzer_<>c__DisplayClass6_0::<>9__2
	Func_2_t2ED5D3B2C08D780C0E2B094DBB8E41935D130170 * ___U3CU3E9__2_2;

public:
	inline static int32_t get_offset_of_heirarchyList_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_t3B07096FF650FEB05B494E1A1B3C564BF7A788F8, ___heirarchyList_0)); }
	inline List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0 * get_heirarchyList_0() const { return ___heirarchyList_0; }
	inline List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0 ** get_address_of_heirarchyList_0() { return &___heirarchyList_0; }
	inline void set_heirarchyList_0(List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0 * value)
	{
		___heirarchyList_0 = value;
		Il2CppCodeGenWriteBarrier((&___heirarchyList_0), value);
	}

	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_t3B07096FF650FEB05B494E1A1B3C564BF7A788F8, ___type_1)); }
	inline Type_t * get_type_1() const { return ___type_1; }
	inline Type_t ** get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(Type_t * value)
	{
		___type_1 = value;
		Il2CppCodeGenWriteBarrier((&___type_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__2_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_t3B07096FF650FEB05B494E1A1B3C564BF7A788F8, ___U3CU3E9__2_2)); }
	inline Func_2_t2ED5D3B2C08D780C0E2B094DBB8E41935D130170 * get_U3CU3E9__2_2() const { return ___U3CU3E9__2_2; }
	inline Func_2_t2ED5D3B2C08D780C0E2B094DBB8E41935D130170 ** get_address_of_U3CU3E9__2_2() { return &___U3CU3E9__2_2; }
	inline void set_U3CU3E9__2_2(Func_2_t2ED5D3B2C08D780C0E2B094DBB8E41935D130170 * value)
	{
		___U3CU3E9__2_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__2_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS6_0_T3B07096FF650FEB05B494E1A1B3C564BF7A788F8_H
#ifndef U3CGETFIELDINJECTABLESU3ED__8_T83A647F0B621C28753334136E5E1FB06B179929B_H
#define U3CGETFIELDINJECTABLESU3ED__8_T83A647F0B621C28753334136E5E1FB06B179929B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.TypeAnalyzer_<GetFieldInjectables>d__8
struct  U3CGetFieldInjectablesU3Ed__8_t83A647F0B621C28753334136E5E1FB06B179929B  : public RuntimeObject
{
public:
	// System.Int32 Zenject.TypeAnalyzer_<GetFieldInjectables>d__8::<>1__state
	int32_t ___U3CU3E1__state_0;
	// Zenject.InjectableInfo Zenject.TypeAnalyzer_<GetFieldInjectables>d__8::<>2__current
	InjectableInfo_t8B0167EA3F54639C2211BD1BEAF54D2C4063390E * ___U3CU3E2__current_1;
	// System.Int32 Zenject.TypeAnalyzer_<GetFieldInjectables>d__8::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// System.Type Zenject.TypeAnalyzer_<GetFieldInjectables>d__8::type
	Type_t * ___type_3;
	// System.Type Zenject.TypeAnalyzer_<GetFieldInjectables>d__8::<>3__type
	Type_t * ___U3CU3E3__type_4;
	// System.Collections.Generic.IEnumerator`1<System.Reflection.FieldInfo> Zenject.TypeAnalyzer_<GetFieldInjectables>d__8::<>7__wrap1
	RuntimeObject* ___U3CU3E7__wrap1_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetFieldInjectablesU3Ed__8_t83A647F0B621C28753334136E5E1FB06B179929B, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetFieldInjectablesU3Ed__8_t83A647F0B621C28753334136E5E1FB06B179929B, ___U3CU3E2__current_1)); }
	inline InjectableInfo_t8B0167EA3F54639C2211BD1BEAF54D2C4063390E * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline InjectableInfo_t8B0167EA3F54639C2211BD1BEAF54D2C4063390E ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(InjectableInfo_t8B0167EA3F54639C2211BD1BEAF54D2C4063390E * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CGetFieldInjectablesU3Ed__8_t83A647F0B621C28753334136E5E1FB06B179929B, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(U3CGetFieldInjectablesU3Ed__8_t83A647F0B621C28753334136E5E1FB06B179929B, ___type_3)); }
	inline Type_t * get_type_3() const { return ___type_3; }
	inline Type_t ** get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(Type_t * value)
	{
		___type_3 = value;
		Il2CppCodeGenWriteBarrier((&___type_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__type_4() { return static_cast<int32_t>(offsetof(U3CGetFieldInjectablesU3Ed__8_t83A647F0B621C28753334136E5E1FB06B179929B, ___U3CU3E3__type_4)); }
	inline Type_t * get_U3CU3E3__type_4() const { return ___U3CU3E3__type_4; }
	inline Type_t ** get_address_of_U3CU3E3__type_4() { return &___U3CU3E3__type_4; }
	inline void set_U3CU3E3__type_4(Type_t * value)
	{
		___U3CU3E3__type_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__type_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_5() { return static_cast<int32_t>(offsetof(U3CGetFieldInjectablesU3Ed__8_t83A647F0B621C28753334136E5E1FB06B179929B, ___U3CU3E7__wrap1_5)); }
	inline RuntimeObject* get_U3CU3E7__wrap1_5() const { return ___U3CU3E7__wrap1_5; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap1_5() { return &___U3CU3E7__wrap1_5; }
	inline void set_U3CU3E7__wrap1_5(RuntimeObject* value)
	{
		___U3CU3E7__wrap1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap1_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETFIELDINJECTABLESU3ED__8_T83A647F0B621C28753334136E5E1FB06B179929B_H
#ifndef U3CGETPROPERTYINJECTABLESU3ED__7_TD24015F2106C6C3A64F5DB6861681AFFFD586FAB_H
#define U3CGETPROPERTYINJECTABLESU3ED__7_TD24015F2106C6C3A64F5DB6861681AFFFD586FAB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.TypeAnalyzer_<GetPropertyInjectables>d__7
struct  U3CGetPropertyInjectablesU3Ed__7_tD24015F2106C6C3A64F5DB6861681AFFFD586FAB  : public RuntimeObject
{
public:
	// System.Int32 Zenject.TypeAnalyzer_<GetPropertyInjectables>d__7::<>1__state
	int32_t ___U3CU3E1__state_0;
	// Zenject.InjectableInfo Zenject.TypeAnalyzer_<GetPropertyInjectables>d__7::<>2__current
	InjectableInfo_t8B0167EA3F54639C2211BD1BEAF54D2C4063390E * ___U3CU3E2__current_1;
	// System.Int32 Zenject.TypeAnalyzer_<GetPropertyInjectables>d__7::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// System.Type Zenject.TypeAnalyzer_<GetPropertyInjectables>d__7::type
	Type_t * ___type_3;
	// System.Type Zenject.TypeAnalyzer_<GetPropertyInjectables>d__7::<>3__type
	Type_t * ___U3CU3E3__type_4;
	// System.Collections.Generic.IEnumerator`1<System.Reflection.PropertyInfo> Zenject.TypeAnalyzer_<GetPropertyInjectables>d__7::<>7__wrap1
	RuntimeObject* ___U3CU3E7__wrap1_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetPropertyInjectablesU3Ed__7_tD24015F2106C6C3A64F5DB6861681AFFFD586FAB, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetPropertyInjectablesU3Ed__7_tD24015F2106C6C3A64F5DB6861681AFFFD586FAB, ___U3CU3E2__current_1)); }
	inline InjectableInfo_t8B0167EA3F54639C2211BD1BEAF54D2C4063390E * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline InjectableInfo_t8B0167EA3F54639C2211BD1BEAF54D2C4063390E ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(InjectableInfo_t8B0167EA3F54639C2211BD1BEAF54D2C4063390E * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CGetPropertyInjectablesU3Ed__7_tD24015F2106C6C3A64F5DB6861681AFFFD586FAB, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(U3CGetPropertyInjectablesU3Ed__7_tD24015F2106C6C3A64F5DB6861681AFFFD586FAB, ___type_3)); }
	inline Type_t * get_type_3() const { return ___type_3; }
	inline Type_t ** get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(Type_t * value)
	{
		___type_3 = value;
		Il2CppCodeGenWriteBarrier((&___type_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__type_4() { return static_cast<int32_t>(offsetof(U3CGetPropertyInjectablesU3Ed__7_tD24015F2106C6C3A64F5DB6861681AFFFD586FAB, ___U3CU3E3__type_4)); }
	inline Type_t * get_U3CU3E3__type_4() const { return ___U3CU3E3__type_4; }
	inline Type_t ** get_address_of_U3CU3E3__type_4() { return &___U3CU3E3__type_4; }
	inline void set_U3CU3E3__type_4(Type_t * value)
	{
		___U3CU3E3__type_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__type_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_5() { return static_cast<int32_t>(offsetof(U3CGetPropertyInjectablesU3Ed__7_tD24015F2106C6C3A64F5DB6861681AFFFD586FAB, ___U3CU3E7__wrap1_5)); }
	inline RuntimeObject* get_U3CU3E7__wrap1_5() const { return ___U3CU3E7__wrap1_5; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap1_5() { return &___U3CU3E7__wrap1_5; }
	inline void set_U3CU3E7__wrap1_5(RuntimeObject* value)
	{
		___U3CU3E7__wrap1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap1_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETPROPERTYINJECTABLESU3ED__7_TD24015F2106C6C3A64F5DB6861681AFFFD586FAB_H
#ifndef VALIDATIONMARKER_T48A3FF2F869BB352F9B6AFD415A0517795A16782_H
#define VALIDATIONMARKER_T48A3FF2F869BB352F9B6AFD415A0517795A16782_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ValidationMarker
struct  ValidationMarker_t48A3FF2F869BB352F9B6AFD415A0517795A16782  : public RuntimeObject
{
public:
	// System.Boolean Zenject.ValidationMarker::<InstantiateFailed>k__BackingField
	bool ___U3CInstantiateFailedU3Ek__BackingField_0;
	// System.Type Zenject.ValidationMarker::<MarkedType>k__BackingField
	Type_t * ___U3CMarkedTypeU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CInstantiateFailedU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ValidationMarker_t48A3FF2F869BB352F9B6AFD415A0517795A16782, ___U3CInstantiateFailedU3Ek__BackingField_0)); }
	inline bool get_U3CInstantiateFailedU3Ek__BackingField_0() const { return ___U3CInstantiateFailedU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CInstantiateFailedU3Ek__BackingField_0() { return &___U3CInstantiateFailedU3Ek__BackingField_0; }
	inline void set_U3CInstantiateFailedU3Ek__BackingField_0(bool value)
	{
		___U3CInstantiateFailedU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CMarkedTypeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ValidationMarker_t48A3FF2F869BB352F9B6AFD415A0517795A16782, ___U3CMarkedTypeU3Ek__BackingField_1)); }
	inline Type_t * get_U3CMarkedTypeU3Ek__BackingField_1() const { return ___U3CMarkedTypeU3Ek__BackingField_1; }
	inline Type_t ** get_address_of_U3CMarkedTypeU3Ek__BackingField_1() { return &___U3CMarkedTypeU3Ek__BackingField_1; }
	inline void set_U3CMarkedTypeU3Ek__BackingField_1(Type_t * value)
	{
		___U3CMarkedTypeU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMarkedTypeU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALIDATIONMARKER_T48A3FF2F869BB352F9B6AFD415A0517795A16782_H
#ifndef VALIDATIONUTIL_T3B87626A5619AB0232CEF653A4ADDB3ED1ADFFF4_H
#define VALIDATIONUTIL_T3B87626A5619AB0232CEF653A4ADDB3ED1ADFFF4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ValidationUtil
struct  ValidationUtil_t3B87626A5619AB0232CEF653A4ADDB3ED1ADFFF4  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALIDATIONUTIL_T3B87626A5619AB0232CEF653A4ADDB3ED1ADFFF4_H
#ifndef U3CU3EC_TA7BA396619C37FF5968906EBAF37F56A27235BE9_H
#define U3CU3EC_TA7BA396619C37FF5968906EBAF37F56A27235BE9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ValidationUtil_<>c
struct  U3CU3Ec_tA7BA396619C37FF5968906EBAF37F56A27235BE9  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tA7BA396619C37FF5968906EBAF37F56A27235BE9_StaticFields
{
public:
	// Zenject.ValidationUtil_<>c Zenject.ValidationUtil_<>c::<>9
	U3CU3Ec_tA7BA396619C37FF5968906EBAF37F56A27235BE9 * ___U3CU3E9_0;
	// System.Func`2<System.Type,Zenject.TypeValuePair> Zenject.ValidationUtil_<>c::<>9__0_0
	Func_2_t1A79A9E174B5F4FD27B0662B857E9836068933B3 * ___U3CU3E9__0_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tA7BA396619C37FF5968906EBAF37F56A27235BE9_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tA7BA396619C37FF5968906EBAF37F56A27235BE9 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tA7BA396619C37FF5968906EBAF37F56A27235BE9 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tA7BA396619C37FF5968906EBAF37F56A27235BE9 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__0_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tA7BA396619C37FF5968906EBAF37F56A27235BE9_StaticFields, ___U3CU3E9__0_0_1)); }
	inline Func_2_t1A79A9E174B5F4FD27B0662B857E9836068933B3 * get_U3CU3E9__0_0_1() const { return ___U3CU3E9__0_0_1; }
	inline Func_2_t1A79A9E174B5F4FD27B0662B857E9836068933B3 ** get_address_of_U3CU3E9__0_0_1() { return &___U3CU3E9__0_0_1; }
	inline void set_U3CU3E9__0_0_1(Func_2_t1A79A9E174B5F4FD27B0662B857E9836068933B3 * value)
	{
		___U3CU3E9__0_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__0_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TA7BA396619C37FF5968906EBAF37F56A27235BE9_H
#ifndef ZENJECTSCENELOADER_TF1587A647EB3ED2821ED3298C3C9A3F82E73CD54_H
#define ZENJECTSCENELOADER_TF1587A647EB3ED2821ED3298C3C9A3F82E73CD54_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ZenjectSceneLoader
struct  ZenjectSceneLoader_tF1587A647EB3ED2821ED3298C3C9A3F82E73CD54  : public RuntimeObject
{
public:
	// Zenject.ProjectKernel Zenject.ZenjectSceneLoader::_projectKernel
	ProjectKernel_tC9D59436FEAD42E4DE77F148A6A66D5E3A7DEC65 * ____projectKernel_0;
	// Zenject.DiContainer Zenject.ZenjectSceneLoader::_sceneContainer
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____sceneContainer_1;

public:
	inline static int32_t get_offset_of__projectKernel_0() { return static_cast<int32_t>(offsetof(ZenjectSceneLoader_tF1587A647EB3ED2821ED3298C3C9A3F82E73CD54, ____projectKernel_0)); }
	inline ProjectKernel_tC9D59436FEAD42E4DE77F148A6A66D5E3A7DEC65 * get__projectKernel_0() const { return ____projectKernel_0; }
	inline ProjectKernel_tC9D59436FEAD42E4DE77F148A6A66D5E3A7DEC65 ** get_address_of__projectKernel_0() { return &____projectKernel_0; }
	inline void set__projectKernel_0(ProjectKernel_tC9D59436FEAD42E4DE77F148A6A66D5E3A7DEC65 * value)
	{
		____projectKernel_0 = value;
		Il2CppCodeGenWriteBarrier((&____projectKernel_0), value);
	}

	inline static int32_t get_offset_of__sceneContainer_1() { return static_cast<int32_t>(offsetof(ZenjectSceneLoader_tF1587A647EB3ED2821ED3298C3C9A3F82E73CD54, ____sceneContainer_1)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__sceneContainer_1() const { return ____sceneContainer_1; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__sceneContainer_1() { return &____sceneContainer_1; }
	inline void set__sceneContainer_1(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____sceneContainer_1 = value;
		Il2CppCodeGenWriteBarrier((&____sceneContainer_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZENJECTSCENELOADER_TF1587A647EB3ED2821ED3298C3C9A3F82E73CD54_H
#ifndef ZENJECTTYPEINFO_T2866A662C0FDA5B357BCC135021933EE8914535E_H
#define ZENJECTTYPEINFO_T2866A662C0FDA5B357BCC135021933EE8914535E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ZenjectTypeInfo
struct  ZenjectTypeInfo_t2866A662C0FDA5B357BCC135021933EE8914535E  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Zenject.PostInjectableInfo> Zenject.ZenjectTypeInfo::_postInjectMethods
	List_1_t8AA9EFA21EF1045CBB5A905EB31B2D66A23339FB * ____postInjectMethods_0;
	// System.Collections.Generic.List`1<Zenject.InjectableInfo> Zenject.ZenjectTypeInfo::_constructorInjectables
	List_1_t19FD687049A90924DCEA81F81C85555F15021A68 * ____constructorInjectables_1;
	// System.Collections.Generic.List`1<Zenject.InjectableInfo> Zenject.ZenjectTypeInfo::_fieldInjectables
	List_1_t19FD687049A90924DCEA81F81C85555F15021A68 * ____fieldInjectables_2;
	// System.Collections.Generic.List`1<Zenject.InjectableInfo> Zenject.ZenjectTypeInfo::_propertyInjectables
	List_1_t19FD687049A90924DCEA81F81C85555F15021A68 * ____propertyInjectables_3;
	// System.Reflection.ConstructorInfo Zenject.ZenjectTypeInfo::_injectConstructor
	ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF * ____injectConstructor_4;
	// System.Type Zenject.ZenjectTypeInfo::_typeAnalyzed
	Type_t * ____typeAnalyzed_5;

public:
	inline static int32_t get_offset_of__postInjectMethods_0() { return static_cast<int32_t>(offsetof(ZenjectTypeInfo_t2866A662C0FDA5B357BCC135021933EE8914535E, ____postInjectMethods_0)); }
	inline List_1_t8AA9EFA21EF1045CBB5A905EB31B2D66A23339FB * get__postInjectMethods_0() const { return ____postInjectMethods_0; }
	inline List_1_t8AA9EFA21EF1045CBB5A905EB31B2D66A23339FB ** get_address_of__postInjectMethods_0() { return &____postInjectMethods_0; }
	inline void set__postInjectMethods_0(List_1_t8AA9EFA21EF1045CBB5A905EB31B2D66A23339FB * value)
	{
		____postInjectMethods_0 = value;
		Il2CppCodeGenWriteBarrier((&____postInjectMethods_0), value);
	}

	inline static int32_t get_offset_of__constructorInjectables_1() { return static_cast<int32_t>(offsetof(ZenjectTypeInfo_t2866A662C0FDA5B357BCC135021933EE8914535E, ____constructorInjectables_1)); }
	inline List_1_t19FD687049A90924DCEA81F81C85555F15021A68 * get__constructorInjectables_1() const { return ____constructorInjectables_1; }
	inline List_1_t19FD687049A90924DCEA81F81C85555F15021A68 ** get_address_of__constructorInjectables_1() { return &____constructorInjectables_1; }
	inline void set__constructorInjectables_1(List_1_t19FD687049A90924DCEA81F81C85555F15021A68 * value)
	{
		____constructorInjectables_1 = value;
		Il2CppCodeGenWriteBarrier((&____constructorInjectables_1), value);
	}

	inline static int32_t get_offset_of__fieldInjectables_2() { return static_cast<int32_t>(offsetof(ZenjectTypeInfo_t2866A662C0FDA5B357BCC135021933EE8914535E, ____fieldInjectables_2)); }
	inline List_1_t19FD687049A90924DCEA81F81C85555F15021A68 * get__fieldInjectables_2() const { return ____fieldInjectables_2; }
	inline List_1_t19FD687049A90924DCEA81F81C85555F15021A68 ** get_address_of__fieldInjectables_2() { return &____fieldInjectables_2; }
	inline void set__fieldInjectables_2(List_1_t19FD687049A90924DCEA81F81C85555F15021A68 * value)
	{
		____fieldInjectables_2 = value;
		Il2CppCodeGenWriteBarrier((&____fieldInjectables_2), value);
	}

	inline static int32_t get_offset_of__propertyInjectables_3() { return static_cast<int32_t>(offsetof(ZenjectTypeInfo_t2866A662C0FDA5B357BCC135021933EE8914535E, ____propertyInjectables_3)); }
	inline List_1_t19FD687049A90924DCEA81F81C85555F15021A68 * get__propertyInjectables_3() const { return ____propertyInjectables_3; }
	inline List_1_t19FD687049A90924DCEA81F81C85555F15021A68 ** get_address_of__propertyInjectables_3() { return &____propertyInjectables_3; }
	inline void set__propertyInjectables_3(List_1_t19FD687049A90924DCEA81F81C85555F15021A68 * value)
	{
		____propertyInjectables_3 = value;
		Il2CppCodeGenWriteBarrier((&____propertyInjectables_3), value);
	}

	inline static int32_t get_offset_of__injectConstructor_4() { return static_cast<int32_t>(offsetof(ZenjectTypeInfo_t2866A662C0FDA5B357BCC135021933EE8914535E, ____injectConstructor_4)); }
	inline ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF * get__injectConstructor_4() const { return ____injectConstructor_4; }
	inline ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF ** get_address_of__injectConstructor_4() { return &____injectConstructor_4; }
	inline void set__injectConstructor_4(ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF * value)
	{
		____injectConstructor_4 = value;
		Il2CppCodeGenWriteBarrier((&____injectConstructor_4), value);
	}

	inline static int32_t get_offset_of__typeAnalyzed_5() { return static_cast<int32_t>(offsetof(ZenjectTypeInfo_t2866A662C0FDA5B357BCC135021933EE8914535E, ____typeAnalyzed_5)); }
	inline Type_t * get__typeAnalyzed_5() const { return ____typeAnalyzed_5; }
	inline Type_t ** get_address_of__typeAnalyzed_5() { return &____typeAnalyzed_5; }
	inline void set__typeAnalyzed_5(Type_t * value)
	{
		____typeAnalyzed_5 = value;
		Il2CppCodeGenWriteBarrier((&____typeAnalyzed_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZENJECTTYPEINFO_T2866A662C0FDA5B357BCC135021933EE8914535E_H
#ifndef U3CU3EC_T6A7B1D29E32D4B0F2FD866225301192DA5428736_H
#define U3CU3EC_T6A7B1D29E32D4B0F2FD866225301192DA5428736_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ZenjectTypeInfo_<>c
struct  U3CU3Ec_t6A7B1D29E32D4B0F2FD866225301192DA5428736  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t6A7B1D29E32D4B0F2FD866225301192DA5428736_StaticFields
{
public:
	// Zenject.ZenjectTypeInfo_<>c Zenject.ZenjectTypeInfo_<>c::<>9
	U3CU3Ec_t6A7B1D29E32D4B0F2FD866225301192DA5428736 * ___U3CU3E9_0;
	// System.Func`2<Zenject.PostInjectableInfo,System.Collections.Generic.IEnumerable`1<Zenject.InjectableInfo>> Zenject.ZenjectTypeInfo_<>c::<>9__12_0
	Func_2_t28651F76A116BE0870CD3719ED8679E661C0C90D * ___U3CU3E9__12_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t6A7B1D29E32D4B0F2FD866225301192DA5428736_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t6A7B1D29E32D4B0F2FD866225301192DA5428736 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t6A7B1D29E32D4B0F2FD866225301192DA5428736 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t6A7B1D29E32D4B0F2FD866225301192DA5428736 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__12_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t6A7B1D29E32D4B0F2FD866225301192DA5428736_StaticFields, ___U3CU3E9__12_0_1)); }
	inline Func_2_t28651F76A116BE0870CD3719ED8679E661C0C90D * get_U3CU3E9__12_0_1() const { return ___U3CU3E9__12_0_1; }
	inline Func_2_t28651F76A116BE0870CD3719ED8679E661C0C90D ** get_address_of_U3CU3E9__12_0_1() { return &___U3CU3E9__12_0_1; }
	inline void set_U3CU3E9__12_0_1(Func_2_t28651F76A116BE0870CD3719ED8679E661C0C90D * value)
	{
		___U3CU3E9__12_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__12_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T6A7B1D29E32D4B0F2FD866225301192DA5428736_H
#ifndef BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#define BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#define COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifndef SCENE_T942E023788C2BC9FBB7EC8356B4FB0088B2CFED2_H
#define SCENE_T942E023788C2BC9FBB7EC8356B4FB0088B2CFED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SceneManagement.Scene
struct  Scene_t942E023788C2BC9FBB7EC8356B4FB0088B2CFED2 
{
public:
	// System.Int32 UnityEngine.SceneManagement.Scene::m_Handle
	int32_t ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(Scene_t942E023788C2BC9FBB7EC8356B4FB0088B2CFED2, ___m_Handle_0)); }
	inline int32_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline int32_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(int32_t value)
	{
		___m_Handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENE_T942E023788C2BC9FBB7EC8356B4FB0088B2CFED2_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef INSTALLER_1_TB72068DCC87B4B3433D61AE825870047E08E0C3C_H
#define INSTALLER_1_TB72068DCC87B4B3433D61AE825870047E08E0C3C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.Installer`1<Zenject.SpaceFighter.GameSignalsInstaller>
struct  Installer_1_tB72068DCC87B4B3433D61AE825870047E08E0C3C  : public InstallerBase_t4F464ACE8FEB8CEEE7B4352A271F09894C28AD79
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTALLER_1_TB72068DCC87B4B3433D61AE825870047E08E0C3C_H
#ifndef MEMORYPOOL_1_TC370EFCBA78EEC8A1644FFBEC5DC5161EDA78E39_H
#define MEMORYPOOL_1_TC370EFCBA78EEC8A1644FFBEC5DC5161EDA78E39_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.MemoryPool`1<Zenject.SpaceFighter.Explosion>
struct  MemoryPool_1_tC370EFCBA78EEC8A1644FFBEC5DC5161EDA78E39  : public MemoryPoolBase_1_t2A7971FB11557D9305E361EDED8A39048A02BEB1
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMORYPOOL_1_TC370EFCBA78EEC8A1644FFBEC5DC5161EDA78E39_H
#ifndef MEMORYPOOL_3_TCE5E2DD4639462ED8F261B3CFE069D337E12EF86_H
#define MEMORYPOOL_3_TCE5E2DD4639462ED8F261B3CFE069D337E12EF86_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.MemoryPool`3<System.Single,System.Single,Zenject.SpaceFighter.EnemyFacade>
struct  MemoryPool_3_tCE5E2DD4639462ED8F261B3CFE069D337E12EF86  : public MemoryPoolBase_1_t09D148DA18FA8D291CDA958BE927BAEC42E65423
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMORYPOOL_3_TCE5E2DD4639462ED8F261B3CFE069D337E12EF86_H
#ifndef MEMORYPOOL_4_T450DE1D65375F7C02043E6CF75051441F90B0F76_H
#define MEMORYPOOL_4_T450DE1D65375F7C02043E6CF75051441F90B0F76_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.MemoryPool`4<System.Single,System.Single,Zenject.SpaceFighter.BulletTypes,Zenject.SpaceFighter.Bullet>
struct  MemoryPool_4_t450DE1D65375F7C02043E6CF75051441F90B0F76  : public MemoryPoolBase_1_t02B1468D062FD8CD3AF2529A5FD170C9590739E9
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMORYPOOL_4_T450DE1D65375F7C02043E6CF75051441F90B0F76_H
#ifndef SIGNAL_1_T4A208D324BB9D49C5F02E4E7A2E5EE9FBA5DA008_H
#define SIGNAL_1_T4A208D324BB9D49C5F02E4E7A2E5EE9FBA5DA008_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.Signal`1<Zenject.SpaceFighter.EnemyKilledSignal>
struct  Signal_1_t4A208D324BB9D49C5F02E4E7A2E5EE9FBA5DA008  : public SignalBase_t1A82E76F19A3098BB4AF7EDD41C29E6CAD3F95B3
{
public:
	// System.Collections.Generic.List`1<System.Action> Zenject.Signal`1::_listeners
	List_1_t9E7F53A878191B703A76E69CE36AA90A372800E7 * ____listeners_3;
	// System.Collections.Generic.List`1<System.Action> Zenject.Signal`1::_tempListeners
	List_1_t9E7F53A878191B703A76E69CE36AA90A372800E7 * ____tempListeners_4;

public:
	inline static int32_t get_offset_of__listeners_3() { return static_cast<int32_t>(offsetof(Signal_1_t4A208D324BB9D49C5F02E4E7A2E5EE9FBA5DA008, ____listeners_3)); }
	inline List_1_t9E7F53A878191B703A76E69CE36AA90A372800E7 * get__listeners_3() const { return ____listeners_3; }
	inline List_1_t9E7F53A878191B703A76E69CE36AA90A372800E7 ** get_address_of__listeners_3() { return &____listeners_3; }
	inline void set__listeners_3(List_1_t9E7F53A878191B703A76E69CE36AA90A372800E7 * value)
	{
		____listeners_3 = value;
		Il2CppCodeGenWriteBarrier((&____listeners_3), value);
	}

	inline static int32_t get_offset_of__tempListeners_4() { return static_cast<int32_t>(offsetof(Signal_1_t4A208D324BB9D49C5F02E4E7A2E5EE9FBA5DA008, ____tempListeners_4)); }
	inline List_1_t9E7F53A878191B703A76E69CE36AA90A372800E7 * get__tempListeners_4() const { return ____tempListeners_4; }
	inline List_1_t9E7F53A878191B703A76E69CE36AA90A372800E7 ** get_address_of__tempListeners_4() { return &____tempListeners_4; }
	inline void set__tempListeners_4(List_1_t9E7F53A878191B703A76E69CE36AA90A372800E7 * value)
	{
		____tempListeners_4 = value;
		Il2CppCodeGenWriteBarrier((&____tempListeners_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGNAL_1_T4A208D324BB9D49C5F02E4E7A2E5EE9FBA5DA008_H
#ifndef SIGNAL_1_TA7CAF14E96359144039B986EB8EEE4E0674F770F_H
#define SIGNAL_1_TA7CAF14E96359144039B986EB8EEE4E0674F770F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.Signal`1<Zenject.SpaceFighter.PlayerDiedSignal>
struct  Signal_1_tA7CAF14E96359144039B986EB8EEE4E0674F770F  : public SignalBase_t1A82E76F19A3098BB4AF7EDD41C29E6CAD3F95B3
{
public:
	// System.Collections.Generic.List`1<System.Action> Zenject.Signal`1::_listeners
	List_1_t9E7F53A878191B703A76E69CE36AA90A372800E7 * ____listeners_3;
	// System.Collections.Generic.List`1<System.Action> Zenject.Signal`1::_tempListeners
	List_1_t9E7F53A878191B703A76E69CE36AA90A372800E7 * ____tempListeners_4;

public:
	inline static int32_t get_offset_of__listeners_3() { return static_cast<int32_t>(offsetof(Signal_1_tA7CAF14E96359144039B986EB8EEE4E0674F770F, ____listeners_3)); }
	inline List_1_t9E7F53A878191B703A76E69CE36AA90A372800E7 * get__listeners_3() const { return ____listeners_3; }
	inline List_1_t9E7F53A878191B703A76E69CE36AA90A372800E7 ** get_address_of__listeners_3() { return &____listeners_3; }
	inline void set__listeners_3(List_1_t9E7F53A878191B703A76E69CE36AA90A372800E7 * value)
	{
		____listeners_3 = value;
		Il2CppCodeGenWriteBarrier((&____listeners_3), value);
	}

	inline static int32_t get_offset_of__tempListeners_4() { return static_cast<int32_t>(offsetof(Signal_1_tA7CAF14E96359144039B986EB8EEE4E0674F770F, ____tempListeners_4)); }
	inline List_1_t9E7F53A878191B703A76E69CE36AA90A372800E7 * get__tempListeners_4() const { return ____tempListeners_4; }
	inline List_1_t9E7F53A878191B703A76E69CE36AA90A372800E7 ** get_address_of__tempListeners_4() { return &____tempListeners_4; }
	inline void set__tempListeners_4(List_1_t9E7F53A878191B703A76E69CE36AA90A372800E7 * value)
	{
		____tempListeners_4 = value;
		Il2CppCodeGenWriteBarrier((&____tempListeners_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGNAL_1_TA7CAF14E96359144039B986EB8EEE4E0674F770F_H
#ifndef ZENJECTEXCEPTION_T7986195562CCDF5A2E4A08728A3969178A0B818F_H
#define ZENJECTEXCEPTION_T7986195562CCDF5A2E4A08728A3969178A0B818F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ZenjectException
struct  ZenjectException_t7986195562CCDF5A2E4A08728A3969178A0B818F  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZENJECTEXCEPTION_T7986195562CCDF5A2E4A08728A3969178A0B818F_H
#ifndef NOTIFICATIONSTYLE_T75380F00FCDE270371559DCCCCAEBCB63B49BB8E_H
#define NOTIFICATIONSTYLE_T75380F00FCDE270371559DCCCCAEBCB63B49BB8E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NotificationSamples.GameNotificationChannel_NotificationStyle
struct  NotificationStyle_t75380F00FCDE270371559DCCCCAEBCB63B49BB8E 
{
public:
	// System.Int32 NotificationSamples.GameNotificationChannel_NotificationStyle::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NotificationStyle_t75380F00FCDE270371559DCCCCAEBCB63B49BB8E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTIFICATIONSTYLE_T75380F00FCDE270371559DCCCCAEBCB63B49BB8E_H
#ifndef PRIVACYMODE_TCFB7939A853B89398E82960B73571EDDDA281167_H
#define PRIVACYMODE_TCFB7939A853B89398E82960B73571EDDDA281167_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NotificationSamples.GameNotificationChannel_PrivacyMode
struct  PrivacyMode_tCFB7939A853B89398E82960B73571EDDDA281167 
{
public:
	// System.Int32 NotificationSamples.GameNotificationChannel_PrivacyMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PrivacyMode_tCFB7939A853B89398E82960B73571EDDDA281167, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRIVACYMODE_TCFB7939A853B89398E82960B73571EDDDA281167_H
#ifndef OPERATINGMODE_TB9F22EC0B982B1982063A1A4EBEF7A6DBFA54B95_H
#define OPERATINGMODE_TB9F22EC0B982B1982063A1A4EBEF7A6DBFA54B95_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NotificationSamples.GameNotificationsManager_OperatingMode
struct  OperatingMode_tB9F22EC0B982B1982063A1A4EBEF7A6DBFA54B95 
{
public:
	// System.Int32 NotificationSamples.GameNotificationsManager_OperatingMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(OperatingMode_tB9F22EC0B982B1982063A1A4EBEF7A6DBFA54B95, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPERATINGMODE_TB9F22EC0B982B1982063A1A4EBEF7A6DBFA54B95_H
#ifndef TIMESPAN_TA8069278ACE8A74D6DF7D514A9CD4432433F64C4_H
#define TIMESPAN_TA8069278ACE8A74D6DF7D514A9CD4432433F64C4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_22;

public:
	inline static int32_t get_offset_of__ticks_22() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4, ____ticks_22)); }
	inline int64_t get__ticks_22() const { return ____ticks_22; }
	inline int64_t* get_address_of__ticks_22() { return &____ticks_22; }
	inline void set__ticks_22(int64_t value)
	{
		____ticks_22 = value;
	}
};

struct TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___Zero_19;
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___MaxValue_20;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___MinValue_21;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.TimeSpan::_legacyConfigChecked
	bool ____legacyConfigChecked_23;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.TimeSpan::_legacyMode
	bool ____legacyMode_24;

public:
	inline static int32_t get_offset_of_Zero_19() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ___Zero_19)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_Zero_19() const { return ___Zero_19; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_Zero_19() { return &___Zero_19; }
	inline void set_Zero_19(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___Zero_19 = value;
	}

	inline static int32_t get_offset_of_MaxValue_20() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ___MaxValue_20)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_MaxValue_20() const { return ___MaxValue_20; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_MaxValue_20() { return &___MaxValue_20; }
	inline void set_MaxValue_20(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___MaxValue_20 = value;
	}

	inline static int32_t get_offset_of_MinValue_21() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ___MinValue_21)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_MinValue_21() const { return ___MinValue_21; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_MinValue_21() { return &___MinValue_21; }
	inline void set_MinValue_21(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___MinValue_21 = value;
	}

	inline static int32_t get_offset_of__legacyConfigChecked_23() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ____legacyConfigChecked_23)); }
	inline bool get__legacyConfigChecked_23() const { return ____legacyConfigChecked_23; }
	inline bool* get_address_of__legacyConfigChecked_23() { return &____legacyConfigChecked_23; }
	inline void set__legacyConfigChecked_23(bool value)
	{
		____legacyConfigChecked_23 = value;
	}

	inline static int32_t get_offset_of__legacyMode_24() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ____legacyMode_24)); }
	inline bool get__legacyMode_24() const { return ____legacyMode_24; }
	inline bool* get_address_of__legacyMode_24() { return &____legacyMode_24; }
	inline void set__legacyMode_24(bool value)
	{
		____legacyMode_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_TA8069278ACE8A74D6DF7D514A9CD4432433F64C4_H
#ifndef DEBUGVARIABLE_T2DB5EEB46D7C214A30CC66E93BC7D3C85015B42C_H
#define DEBUGVARIABLE_T2DB5EEB46D7C214A30CC66E93BC7D3C85015B42C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayx.Graphy.GraphyDebugger_DebugVariable
struct  DebugVariable_t2DB5EEB46D7C214A30CC66E93BC7D3C85015B42C 
{
public:
	// System.Int32 Tayx.Graphy.GraphyDebugger_DebugVariable::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebugVariable_t2DB5EEB46D7C214A30CC66E93BC7D3C85015B42C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGVARIABLE_T2DB5EEB46D7C214A30CC66E93BC7D3C85015B42C_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef U3CU3EC__DISPLAYCLASS7_0_T1F8DD433041F1C5A6D5805320617520438071D1A_H
#define U3CU3EC__DISPLAYCLASS7_0_T1F8DD433041F1C5A6D5805320617520438071D1A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.Internal.ZenUtilInternal_<>c__DisplayClass7_0
struct  U3CU3Ec__DisplayClass7_0_t1F8DD433041F1C5A6D5805320617520438071D1A  : public RuntimeObject
{
public:
	// UnityEngine.SceneManagement.Scene Zenject.Internal.ZenUtilInternal_<>c__DisplayClass7_0::scene
	Scene_t942E023788C2BC9FBB7EC8356B4FB0088B2CFED2  ___scene_0;

public:
	inline static int32_t get_offset_of_scene_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass7_0_t1F8DD433041F1C5A6D5805320617520438071D1A, ___scene_0)); }
	inline Scene_t942E023788C2BC9FBB7EC8356B4FB0088B2CFED2  get_scene_0() const { return ___scene_0; }
	inline Scene_t942E023788C2BC9FBB7EC8356B4FB0088B2CFED2 * get_address_of_scene_0() { return &___scene_0; }
	inline void set_scene_0(Scene_t942E023788C2BC9FBB7EC8356B4FB0088B2CFED2  value)
	{
		___scene_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS7_0_T1F8DD433041F1C5A6D5805320617520438071D1A_H
#ifndef LOADSCENERELATIONSHIP_T070553E2B76C8CC43C1C9D6FB4C28579ADFF0A58_H
#define LOADSCENERELATIONSHIP_T070553E2B76C8CC43C1C9D6FB4C28579ADFF0A58_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.LoadSceneRelationship
struct  LoadSceneRelationship_t070553E2B76C8CC43C1C9D6FB4C28579ADFF0A58 
{
public:
	// System.Int32 Zenject.LoadSceneRelationship::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LoadSceneRelationship_t070553E2B76C8CC43C1C9D6FB4C28579ADFF0A58, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADSCENERELATIONSHIP_T070553E2B76C8CC43C1C9D6FB4C28579ADFF0A58_H
#ifndef MONOMEMORYPOOL_1_T64F585F109BA7579416BBE270DFDCD5C95BDA280_H
#define MONOMEMORYPOOL_1_T64F585F109BA7579416BBE270DFDCD5C95BDA280_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.MonoMemoryPool`1<Zenject.SpaceFighter.Explosion>
struct  MonoMemoryPool_1_t64F585F109BA7579416BBE270DFDCD5C95BDA280  : public MemoryPool_1_tC370EFCBA78EEC8A1644FFBEC5DC5161EDA78E39
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOMEMORYPOOL_1_T64F585F109BA7579416BBE270DFDCD5C95BDA280_H
#ifndef MONOMEMORYPOOL_3_T9FF718195957220659C2B59DBE80DE47B3C807EE_H
#define MONOMEMORYPOOL_3_T9FF718195957220659C2B59DBE80DE47B3C807EE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.MonoMemoryPool`3<System.Single,System.Single,Zenject.SpaceFighter.EnemyFacade>
struct  MonoMemoryPool_3_t9FF718195957220659C2B59DBE80DE47B3C807EE  : public MemoryPool_3_tCE5E2DD4639462ED8F261B3CFE069D337E12EF86
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOMEMORYPOOL_3_T9FF718195957220659C2B59DBE80DE47B3C807EE_H
#ifndef MONOMEMORYPOOL_4_T543573B841F0CE54215C39B67C7A156298B21277_H
#define MONOMEMORYPOOL_4_T543573B841F0CE54215C39B67C7A156298B21277_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.MonoMemoryPool`4<System.Single,System.Single,Zenject.SpaceFighter.BulletTypes,Zenject.SpaceFighter.Bullet>
struct  MonoMemoryPool_4_t543573B841F0CE54215C39B67C7A156298B21277  : public MemoryPool_4_t450DE1D65375F7C02043E6CF75051441F90B0F76
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOMEMORYPOOL_4_T543573B841F0CE54215C39B67C7A156298B21277_H
#ifndef BULLETTYPES_T5EDC76B12065FDE17E4384C7F1B92F11B06E05D4_H
#define BULLETTYPES_T5EDC76B12065FDE17E4384C7F1B92F11B06E05D4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SpaceFighter.BulletTypes
struct  BulletTypes_t5EDC76B12065FDE17E4384C7F1B92F11B06E05D4 
{
public:
	// System.Int32 Zenject.SpaceFighter.BulletTypes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BulletTypes_t5EDC76B12065FDE17E4384C7F1B92F11B06E05D4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BULLETTYPES_T5EDC76B12065FDE17E4384C7F1B92F11B06E05D4_H
#ifndef ENEMY_TC40CA91F878B23445D874D13243C2824315DE3D2_H
#define ENEMY_TC40CA91F878B23445D874D13243C2824315DE3D2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SpaceFighter.Enemy
struct  Enemy_tC40CA91F878B23445D874D13243C2824315DE3D2  : public RuntimeObject
{
public:
	// UnityEngine.MeshRenderer Zenject.SpaceFighter.Enemy::_renderer
	MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * ____renderer_0;
	// UnityEngine.Collider Zenject.SpaceFighter.Enemy::_collider
	Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * ____collider_1;
	// UnityEngine.Rigidbody Zenject.SpaceFighter.Enemy::_rigidBody
	Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * ____rigidBody_2;
	// UnityEngine.Vector3 Zenject.SpaceFighter.Enemy::<DesiredLookDir>k__BackingField
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CDesiredLookDirU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of__renderer_0() { return static_cast<int32_t>(offsetof(Enemy_tC40CA91F878B23445D874D13243C2824315DE3D2, ____renderer_0)); }
	inline MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * get__renderer_0() const { return ____renderer_0; }
	inline MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED ** get_address_of__renderer_0() { return &____renderer_0; }
	inline void set__renderer_0(MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * value)
	{
		____renderer_0 = value;
		Il2CppCodeGenWriteBarrier((&____renderer_0), value);
	}

	inline static int32_t get_offset_of__collider_1() { return static_cast<int32_t>(offsetof(Enemy_tC40CA91F878B23445D874D13243C2824315DE3D2, ____collider_1)); }
	inline Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * get__collider_1() const { return ____collider_1; }
	inline Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF ** get_address_of__collider_1() { return &____collider_1; }
	inline void set__collider_1(Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * value)
	{
		____collider_1 = value;
		Il2CppCodeGenWriteBarrier((&____collider_1), value);
	}

	inline static int32_t get_offset_of__rigidBody_2() { return static_cast<int32_t>(offsetof(Enemy_tC40CA91F878B23445D874D13243C2824315DE3D2, ____rigidBody_2)); }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * get__rigidBody_2() const { return ____rigidBody_2; }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 ** get_address_of__rigidBody_2() { return &____rigidBody_2; }
	inline void set__rigidBody_2(Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * value)
	{
		____rigidBody_2 = value;
		Il2CppCodeGenWriteBarrier((&____rigidBody_2), value);
	}

	inline static int32_t get_offset_of_U3CDesiredLookDirU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Enemy_tC40CA91F878B23445D874D13243C2824315DE3D2, ___U3CDesiredLookDirU3Ek__BackingField_3)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CDesiredLookDirU3Ek__BackingField_3() const { return ___U3CDesiredLookDirU3Ek__BackingField_3; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CDesiredLookDirU3Ek__BackingField_3() { return &___U3CDesiredLookDirU3Ek__BackingField_3; }
	inline void set_U3CDesiredLookDirU3Ek__BackingField_3(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CDesiredLookDirU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENEMY_TC40CA91F878B23445D874D13243C2824315DE3D2_H
#ifndef ENEMYKILLEDSIGNAL_T5291603135B3B8F73E6ED1317431EE5091D78DC8_H
#define ENEMYKILLEDSIGNAL_T5291603135B3B8F73E6ED1317431EE5091D78DC8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SpaceFighter.EnemyKilledSignal
struct  EnemyKilledSignal_t5291603135B3B8F73E6ED1317431EE5091D78DC8  : public Signal_1_t4A208D324BB9D49C5F02E4E7A2E5EE9FBA5DA008
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENEMYKILLEDSIGNAL_T5291603135B3B8F73E6ED1317431EE5091D78DC8_H
#ifndef ENEMYSTATEIDLE_T384A3EE550CBFEB78AEBA1F4C636153F53E4E07D_H
#define ENEMYSTATEIDLE_T384A3EE550CBFEB78AEBA1F4C636153F53E4E07D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SpaceFighter.EnemyStateIdle
struct  EnemyStateIdle_t384A3EE550CBFEB78AEBA1F4C636153F53E4E07D  : public RuntimeObject
{
public:
	// Zenject.SpaceFighter.EnemyStateIdle_Settings Zenject.SpaceFighter.EnemyStateIdle::_settings
	Settings_t6A845C637F62D4448CE7764CFBBFBDDDA7951042 * ____settings_0;
	// Zenject.SpaceFighter.Enemy Zenject.SpaceFighter.EnemyStateIdle::_enemy
	Enemy_tC40CA91F878B23445D874D13243C2824315DE3D2 * ____enemy_1;
	// UnityEngine.Vector3 Zenject.SpaceFighter.EnemyStateIdle::_startPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ____startPos_2;
	// System.Single Zenject.SpaceFighter.EnemyStateIdle::_theta
	float ____theta_3;
	// UnityEngine.Vector3 Zenject.SpaceFighter.EnemyStateIdle::_startLookDir
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ____startLookDir_4;

public:
	inline static int32_t get_offset_of__settings_0() { return static_cast<int32_t>(offsetof(EnemyStateIdle_t384A3EE550CBFEB78AEBA1F4C636153F53E4E07D, ____settings_0)); }
	inline Settings_t6A845C637F62D4448CE7764CFBBFBDDDA7951042 * get__settings_0() const { return ____settings_0; }
	inline Settings_t6A845C637F62D4448CE7764CFBBFBDDDA7951042 ** get_address_of__settings_0() { return &____settings_0; }
	inline void set__settings_0(Settings_t6A845C637F62D4448CE7764CFBBFBDDDA7951042 * value)
	{
		____settings_0 = value;
		Il2CppCodeGenWriteBarrier((&____settings_0), value);
	}

	inline static int32_t get_offset_of__enemy_1() { return static_cast<int32_t>(offsetof(EnemyStateIdle_t384A3EE550CBFEB78AEBA1F4C636153F53E4E07D, ____enemy_1)); }
	inline Enemy_tC40CA91F878B23445D874D13243C2824315DE3D2 * get__enemy_1() const { return ____enemy_1; }
	inline Enemy_tC40CA91F878B23445D874D13243C2824315DE3D2 ** get_address_of__enemy_1() { return &____enemy_1; }
	inline void set__enemy_1(Enemy_tC40CA91F878B23445D874D13243C2824315DE3D2 * value)
	{
		____enemy_1 = value;
		Il2CppCodeGenWriteBarrier((&____enemy_1), value);
	}

	inline static int32_t get_offset_of__startPos_2() { return static_cast<int32_t>(offsetof(EnemyStateIdle_t384A3EE550CBFEB78AEBA1F4C636153F53E4E07D, ____startPos_2)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get__startPos_2() const { return ____startPos_2; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of__startPos_2() { return &____startPos_2; }
	inline void set__startPos_2(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		____startPos_2 = value;
	}

	inline static int32_t get_offset_of__theta_3() { return static_cast<int32_t>(offsetof(EnemyStateIdle_t384A3EE550CBFEB78AEBA1F4C636153F53E4E07D, ____theta_3)); }
	inline float get__theta_3() const { return ____theta_3; }
	inline float* get_address_of__theta_3() { return &____theta_3; }
	inline void set__theta_3(float value)
	{
		____theta_3 = value;
	}

	inline static int32_t get_offset_of__startLookDir_4() { return static_cast<int32_t>(offsetof(EnemyStateIdle_t384A3EE550CBFEB78AEBA1F4C636153F53E4E07D, ____startLookDir_4)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get__startLookDir_4() const { return ____startLookDir_4; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of__startLookDir_4() { return &____startLookDir_4; }
	inline void set__startLookDir_4(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		____startLookDir_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENEMYSTATEIDLE_T384A3EE550CBFEB78AEBA1F4C636153F53E4E07D_H
#ifndef ENEMYSTATES_T472981EB20C199308DC21B66E41DEC5D8719EF54_H
#define ENEMYSTATES_T472981EB20C199308DC21B66E41DEC5D8719EF54_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SpaceFighter.EnemyStates
struct  EnemyStates_t472981EB20C199308DC21B66E41DEC5D8719EF54 
{
public:
	// System.Int32 Zenject.SpaceFighter.EnemyStates::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EnemyStates_t472981EB20C199308DC21B66E41DEC5D8719EF54, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENEMYSTATES_T472981EB20C199308DC21B66E41DEC5D8719EF54_H
#ifndef GAMESIGNALSINSTALLER_TF11CCBEDED82401BF3C0D10AEC62BC6B9C444549_H
#define GAMESIGNALSINSTALLER_TF11CCBEDED82401BF3C0D10AEC62BC6B9C444549_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SpaceFighter.GameSignalsInstaller
struct  GameSignalsInstaller_tF11CCBEDED82401BF3C0D10AEC62BC6B9C444549  : public Installer_1_tB72068DCC87B4B3433D61AE825870047E08E0C3C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMESIGNALSINSTALLER_TF11CCBEDED82401BF3C0D10AEC62BC6B9C444549_H
#ifndef PLAYERDIEDSIGNAL_TD47ED6EA743FEE4E5D7B3289450C3EA59E018C7F_H
#define PLAYERDIEDSIGNAL_TD47ED6EA743FEE4E5D7B3289450C3EA59E018C7F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SpaceFighter.PlayerDiedSignal
struct  PlayerDiedSignal_tD47ED6EA743FEE4E5D7B3289450C3EA59E018C7F  : public Signal_1_tA7CAF14E96359144039B986EB8EEE4E0674F770F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERDIEDSIGNAL_TD47ED6EA743FEE4E5D7B3289450C3EA59E018C7F_H
#ifndef CONTAINERSOURCES_TDE70B7FAC33EE303EC700E587DD993EE9D3DC41D_H
#define CONTAINERSOURCES_TDE70B7FAC33EE303EC700E587DD993EE9D3DC41D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ZenAutoInjecter_ContainerSources
struct  ContainerSources_tDE70B7FAC33EE303EC700E587DD993EE9D3DC41D 
{
public:
	// System.Int32 Zenject.ZenAutoInjecter_ContainerSources::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ContainerSources_tDE70B7FAC33EE303EC700E587DD993EE9D3DC41D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTAINERSOURCES_TDE70B7FAC33EE303EC700E587DD993EE9D3DC41D_H
#ifndef GAMENOTIFICATIONCHANNEL_TC51F6355BB3F1A5A6FDAF48D77FA7AE8702F4B79_H
#define GAMENOTIFICATIONCHANNEL_TC51F6355BB3F1A5A6FDAF48D77FA7AE8702F4B79_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NotificationSamples.GameNotificationChannel
struct  GameNotificationChannel_tC51F6355BB3F1A5A6FDAF48D77FA7AE8702F4B79 
{
public:
	// System.String NotificationSamples.GameNotificationChannel::Id
	String_t* ___Id_0;
	// System.String NotificationSamples.GameNotificationChannel::Name
	String_t* ___Name_1;
	// System.String NotificationSamples.GameNotificationChannel::Description
	String_t* ___Description_2;
	// System.Boolean NotificationSamples.GameNotificationChannel::ShowsBadge
	bool ___ShowsBadge_3;
	// System.Boolean NotificationSamples.GameNotificationChannel::ShowLights
	bool ___ShowLights_4;
	// System.Boolean NotificationSamples.GameNotificationChannel::Vibrates
	bool ___Vibrates_5;
	// System.Boolean NotificationSamples.GameNotificationChannel::HighPriority
	bool ___HighPriority_6;
	// NotificationSamples.GameNotificationChannel_NotificationStyle NotificationSamples.GameNotificationChannel::Style
	int32_t ___Style_7;
	// NotificationSamples.GameNotificationChannel_PrivacyMode NotificationSamples.GameNotificationChannel::Privacy
	int32_t ___Privacy_8;
	// System.Int32[] NotificationSamples.GameNotificationChannel::VibrationPattern
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___VibrationPattern_9;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(GameNotificationChannel_tC51F6355BB3F1A5A6FDAF48D77FA7AE8702F4B79, ___Id_0)); }
	inline String_t* get_Id_0() const { return ___Id_0; }
	inline String_t** get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(String_t* value)
	{
		___Id_0 = value;
		Il2CppCodeGenWriteBarrier((&___Id_0), value);
	}

	inline static int32_t get_offset_of_Name_1() { return static_cast<int32_t>(offsetof(GameNotificationChannel_tC51F6355BB3F1A5A6FDAF48D77FA7AE8702F4B79, ___Name_1)); }
	inline String_t* get_Name_1() const { return ___Name_1; }
	inline String_t** get_address_of_Name_1() { return &___Name_1; }
	inline void set_Name_1(String_t* value)
	{
		___Name_1 = value;
		Il2CppCodeGenWriteBarrier((&___Name_1), value);
	}

	inline static int32_t get_offset_of_Description_2() { return static_cast<int32_t>(offsetof(GameNotificationChannel_tC51F6355BB3F1A5A6FDAF48D77FA7AE8702F4B79, ___Description_2)); }
	inline String_t* get_Description_2() const { return ___Description_2; }
	inline String_t** get_address_of_Description_2() { return &___Description_2; }
	inline void set_Description_2(String_t* value)
	{
		___Description_2 = value;
		Il2CppCodeGenWriteBarrier((&___Description_2), value);
	}

	inline static int32_t get_offset_of_ShowsBadge_3() { return static_cast<int32_t>(offsetof(GameNotificationChannel_tC51F6355BB3F1A5A6FDAF48D77FA7AE8702F4B79, ___ShowsBadge_3)); }
	inline bool get_ShowsBadge_3() const { return ___ShowsBadge_3; }
	inline bool* get_address_of_ShowsBadge_3() { return &___ShowsBadge_3; }
	inline void set_ShowsBadge_3(bool value)
	{
		___ShowsBadge_3 = value;
	}

	inline static int32_t get_offset_of_ShowLights_4() { return static_cast<int32_t>(offsetof(GameNotificationChannel_tC51F6355BB3F1A5A6FDAF48D77FA7AE8702F4B79, ___ShowLights_4)); }
	inline bool get_ShowLights_4() const { return ___ShowLights_4; }
	inline bool* get_address_of_ShowLights_4() { return &___ShowLights_4; }
	inline void set_ShowLights_4(bool value)
	{
		___ShowLights_4 = value;
	}

	inline static int32_t get_offset_of_Vibrates_5() { return static_cast<int32_t>(offsetof(GameNotificationChannel_tC51F6355BB3F1A5A6FDAF48D77FA7AE8702F4B79, ___Vibrates_5)); }
	inline bool get_Vibrates_5() const { return ___Vibrates_5; }
	inline bool* get_address_of_Vibrates_5() { return &___Vibrates_5; }
	inline void set_Vibrates_5(bool value)
	{
		___Vibrates_5 = value;
	}

	inline static int32_t get_offset_of_HighPriority_6() { return static_cast<int32_t>(offsetof(GameNotificationChannel_tC51F6355BB3F1A5A6FDAF48D77FA7AE8702F4B79, ___HighPriority_6)); }
	inline bool get_HighPriority_6() const { return ___HighPriority_6; }
	inline bool* get_address_of_HighPriority_6() { return &___HighPriority_6; }
	inline void set_HighPriority_6(bool value)
	{
		___HighPriority_6 = value;
	}

	inline static int32_t get_offset_of_Style_7() { return static_cast<int32_t>(offsetof(GameNotificationChannel_tC51F6355BB3F1A5A6FDAF48D77FA7AE8702F4B79, ___Style_7)); }
	inline int32_t get_Style_7() const { return ___Style_7; }
	inline int32_t* get_address_of_Style_7() { return &___Style_7; }
	inline void set_Style_7(int32_t value)
	{
		___Style_7 = value;
	}

	inline static int32_t get_offset_of_Privacy_8() { return static_cast<int32_t>(offsetof(GameNotificationChannel_tC51F6355BB3F1A5A6FDAF48D77FA7AE8702F4B79, ___Privacy_8)); }
	inline int32_t get_Privacy_8() const { return ___Privacy_8; }
	inline int32_t* get_address_of_Privacy_8() { return &___Privacy_8; }
	inline void set_Privacy_8(int32_t value)
	{
		___Privacy_8 = value;
	}

	inline static int32_t get_offset_of_VibrationPattern_9() { return static_cast<int32_t>(offsetof(GameNotificationChannel_tC51F6355BB3F1A5A6FDAF48D77FA7AE8702F4B79, ___VibrationPattern_9)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_VibrationPattern_9() const { return ___VibrationPattern_9; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_VibrationPattern_9() { return &___VibrationPattern_9; }
	inline void set_VibrationPattern_9(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___VibrationPattern_9 = value;
		Il2CppCodeGenWriteBarrier((&___VibrationPattern_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of NotificationSamples.GameNotificationChannel
struct GameNotificationChannel_tC51F6355BB3F1A5A6FDAF48D77FA7AE8702F4B79_marshaled_pinvoke
{
	char* ___Id_0;
	char* ___Name_1;
	char* ___Description_2;
	int32_t ___ShowsBadge_3;
	int32_t ___ShowLights_4;
	int32_t ___Vibrates_5;
	int32_t ___HighPriority_6;
	int32_t ___Style_7;
	int32_t ___Privacy_8;
	int32_t* ___VibrationPattern_9;
};
// Native definition for COM marshalling of NotificationSamples.GameNotificationChannel
struct GameNotificationChannel_tC51F6355BB3F1A5A6FDAF48D77FA7AE8702F4B79_marshaled_com
{
	Il2CppChar* ___Id_0;
	Il2CppChar* ___Name_1;
	Il2CppChar* ___Description_2;
	int32_t ___ShowsBadge_3;
	int32_t ___ShowLights_4;
	int32_t ___Vibrates_5;
	int32_t ___HighPriority_6;
	int32_t ___Style_7;
	int32_t ___Privacy_8;
	int32_t* ___VibrationPattern_9;
};
#endif // GAMENOTIFICATIONCHANNEL_TC51F6355BB3F1A5A6FDAF48D77FA7AE8702F4B79_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#define SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_pinvoke : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_com : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifndef POOL_T28D3C271D1214C1C919D02B9C38A6A5698F231F1_H
#define POOL_T28D3C271D1214C1C919D02B9C38A6A5698F231F1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SpaceFighter.Bullet_Pool
struct  Pool_t28D3C271D1214C1C919D02B9C38A6A5698F231F1  : public MonoMemoryPool_4_t543573B841F0CE54215C39B67C7A156298B21277
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POOL_T28D3C271D1214C1C919D02B9C38A6A5698F231F1_H
#ifndef POOL_T735C24F8E4685CE9F8814136D7E4B3548882F387_H
#define POOL_T735C24F8E4685CE9F8814136D7E4B3548882F387_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SpaceFighter.EnemyFacade_Pool
struct  Pool_t735C24F8E4685CE9F8814136D7E4B3548882F387  : public MonoMemoryPool_3_t9FF718195957220659C2B59DBE80DE47B3C807EE
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POOL_T735C24F8E4685CE9F8814136D7E4B3548882F387_H
#ifndef ENEMYSTATEMANAGER_TD3E2662DA02D660CB7356CE3563F928B0E220C9F_H
#define ENEMYSTATEMANAGER_TD3E2662DA02D660CB7356CE3563F928B0E220C9F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SpaceFighter.EnemyStateManager
struct  EnemyStateManager_tD3E2662DA02D660CB7356CE3563F928B0E220C9F  : public RuntimeObject
{
public:
	// Zenject.SpaceFighter.IEnemyState Zenject.SpaceFighter.EnemyStateManager::_currentStateHandler
	RuntimeObject* ____currentStateHandler_0;
	// Zenject.SpaceFighter.EnemyStates Zenject.SpaceFighter.EnemyStateManager::_currentState
	int32_t ____currentState_1;
	// System.Collections.Generic.List`1<Zenject.SpaceFighter.IEnemyState> Zenject.SpaceFighter.EnemyStateManager::_states
	List_1_t9DDEECAEE13EF4EB757430774E50AF9385022D8A * ____states_2;

public:
	inline static int32_t get_offset_of__currentStateHandler_0() { return static_cast<int32_t>(offsetof(EnemyStateManager_tD3E2662DA02D660CB7356CE3563F928B0E220C9F, ____currentStateHandler_0)); }
	inline RuntimeObject* get__currentStateHandler_0() const { return ____currentStateHandler_0; }
	inline RuntimeObject** get_address_of__currentStateHandler_0() { return &____currentStateHandler_0; }
	inline void set__currentStateHandler_0(RuntimeObject* value)
	{
		____currentStateHandler_0 = value;
		Il2CppCodeGenWriteBarrier((&____currentStateHandler_0), value);
	}

	inline static int32_t get_offset_of__currentState_1() { return static_cast<int32_t>(offsetof(EnemyStateManager_tD3E2662DA02D660CB7356CE3563F928B0E220C9F, ____currentState_1)); }
	inline int32_t get__currentState_1() const { return ____currentState_1; }
	inline int32_t* get_address_of__currentState_1() { return &____currentState_1; }
	inline void set__currentState_1(int32_t value)
	{
		____currentState_1 = value;
	}

	inline static int32_t get_offset_of__states_2() { return static_cast<int32_t>(offsetof(EnemyStateManager_tD3E2662DA02D660CB7356CE3563F928B0E220C9F, ____states_2)); }
	inline List_1_t9DDEECAEE13EF4EB757430774E50AF9385022D8A * get__states_2() const { return ____states_2; }
	inline List_1_t9DDEECAEE13EF4EB757430774E50AF9385022D8A ** get_address_of__states_2() { return &____states_2; }
	inline void set__states_2(List_1_t9DDEECAEE13EF4EB757430774E50AF9385022D8A * value)
	{
		____states_2 = value;
		Il2CppCodeGenWriteBarrier((&____states_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENEMYSTATEMANAGER_TD3E2662DA02D660CB7356CE3563F928B0E220C9F_H
#ifndef POOL_TF6D222503E0FF0BBA73DDF77321A0C8B6CA1D7C7_H
#define POOL_TF6D222503E0FF0BBA73DDF77321A0C8B6CA1D7C7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SpaceFighter.Explosion_Pool
struct  Pool_tF6D222503E0FF0BBA73DDF77321A0C8B6CA1D7C7  : public MonoMemoryPool_1_t64F585F109BA7579416BBE270DFDCD5C95BDA280
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POOL_TF6D222503E0FF0BBA73DDF77321A0C8B6CA1D7C7_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef SCRIPTABLEOBJECTINSTALLERBASE_T2B6C9A132878FE8B711E2E733FB10E227A48708A_H
#define SCRIPTABLEOBJECTINSTALLERBASE_T2B6C9A132878FE8B711E2E733FB10E227A48708A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ScriptableObjectInstallerBase
struct  ScriptableObjectInstallerBase_t2B6C9A132878FE8B711E2E733FB10E227A48708A  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// Zenject.DiContainer Zenject.ScriptableObjectInstallerBase::_container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____container_4;

public:
	inline static int32_t get_offset_of__container_4() { return static_cast<int32_t>(offsetof(ScriptableObjectInstallerBase_t2B6C9A132878FE8B711E2E733FB10E227A48708A, ____container_4)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__container_4() const { return ____container_4; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__container_4() { return &____container_4; }
	inline void set__container_4(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____container_4 = value;
		Il2CppCodeGenWriteBarrier((&____container_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRIPTABLEOBJECTINSTALLERBASE_T2B6C9A132878FE8B711E2E733FB10E227A48708A_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef SCRIPTABLEOBJECTINSTALLER_TAC791A8E2F09B0B1D8D61690E70CEDB55F1AD8D6_H
#define SCRIPTABLEOBJECTINSTALLER_TAC791A8E2F09B0B1D8D61690E70CEDB55F1AD8D6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ScriptableObjectInstaller
struct  ScriptableObjectInstaller_tAC791A8E2F09B0B1D8D61690E70CEDB55F1AD8D6  : public ScriptableObjectInstallerBase_t2B6C9A132878FE8B711E2E733FB10E227A48708A
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRIPTABLEOBJECTINSTALLER_TAC791A8E2F09B0B1D8D61690E70CEDB55F1AD8D6_H
#ifndef GAMENOTIFICATIONSMANAGER_TA91A2FAF80490C54B58C02C8B5652AFA290D6B9F_H
#define GAMENOTIFICATIONSMANAGER_TA91A2FAF80490C54B58C02C8B5652AFA290D6B9F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NotificationSamples.GameNotificationsManager
struct  GameNotificationsManager_tA91A2FAF80490C54B58C02C8B5652AFA290D6B9F  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// NotificationSamples.GameNotificationsManager_OperatingMode NotificationSamples.GameNotificationsManager::mode
	int32_t ___mode_6;
	// System.Boolean NotificationSamples.GameNotificationsManager::autoBadging
	bool ___autoBadging_7;
	// System.Action`1<NotificationSamples.PendingNotification> NotificationSamples.GameNotificationsManager::LocalNotificationDelivered
	Action_1_tF8CCF5268A443AE2FC0E1CC856753980670A6A09 * ___LocalNotificationDelivered_8;
	// System.Action`1<NotificationSamples.PendingNotification> NotificationSamples.GameNotificationsManager::LocalNotificationExpired
	Action_1_tF8CCF5268A443AE2FC0E1CC856753980670A6A09 * ___LocalNotificationExpired_9;
	// NotificationSamples.IGameNotificationsPlatform NotificationSamples.GameNotificationsManager::<Platform>k__BackingField
	RuntimeObject* ___U3CPlatformU3Ek__BackingField_10;
	// System.Collections.Generic.List`1<NotificationSamples.PendingNotification> NotificationSamples.GameNotificationsManager::<PendingNotifications>k__BackingField
	List_1_tD4AB95F0286C0D90E2D91E8BD2380724938469CB * ___U3CPendingNotificationsU3Ek__BackingField_11;
	// NotificationSamples.IPendingNotificationsSerializer NotificationSamples.GameNotificationsManager::<Serializer>k__BackingField
	RuntimeObject* ___U3CSerializerU3Ek__BackingField_12;
	// System.Boolean NotificationSamples.GameNotificationsManager::<Initialized>k__BackingField
	bool ___U3CInitializedU3Ek__BackingField_13;
	// System.Boolean NotificationSamples.GameNotificationsManager::inForeground
	bool ___inForeground_14;

public:
	inline static int32_t get_offset_of_mode_6() { return static_cast<int32_t>(offsetof(GameNotificationsManager_tA91A2FAF80490C54B58C02C8B5652AFA290D6B9F, ___mode_6)); }
	inline int32_t get_mode_6() const { return ___mode_6; }
	inline int32_t* get_address_of_mode_6() { return &___mode_6; }
	inline void set_mode_6(int32_t value)
	{
		___mode_6 = value;
	}

	inline static int32_t get_offset_of_autoBadging_7() { return static_cast<int32_t>(offsetof(GameNotificationsManager_tA91A2FAF80490C54B58C02C8B5652AFA290D6B9F, ___autoBadging_7)); }
	inline bool get_autoBadging_7() const { return ___autoBadging_7; }
	inline bool* get_address_of_autoBadging_7() { return &___autoBadging_7; }
	inline void set_autoBadging_7(bool value)
	{
		___autoBadging_7 = value;
	}

	inline static int32_t get_offset_of_LocalNotificationDelivered_8() { return static_cast<int32_t>(offsetof(GameNotificationsManager_tA91A2FAF80490C54B58C02C8B5652AFA290D6B9F, ___LocalNotificationDelivered_8)); }
	inline Action_1_tF8CCF5268A443AE2FC0E1CC856753980670A6A09 * get_LocalNotificationDelivered_8() const { return ___LocalNotificationDelivered_8; }
	inline Action_1_tF8CCF5268A443AE2FC0E1CC856753980670A6A09 ** get_address_of_LocalNotificationDelivered_8() { return &___LocalNotificationDelivered_8; }
	inline void set_LocalNotificationDelivered_8(Action_1_tF8CCF5268A443AE2FC0E1CC856753980670A6A09 * value)
	{
		___LocalNotificationDelivered_8 = value;
		Il2CppCodeGenWriteBarrier((&___LocalNotificationDelivered_8), value);
	}

	inline static int32_t get_offset_of_LocalNotificationExpired_9() { return static_cast<int32_t>(offsetof(GameNotificationsManager_tA91A2FAF80490C54B58C02C8B5652AFA290D6B9F, ___LocalNotificationExpired_9)); }
	inline Action_1_tF8CCF5268A443AE2FC0E1CC856753980670A6A09 * get_LocalNotificationExpired_9() const { return ___LocalNotificationExpired_9; }
	inline Action_1_tF8CCF5268A443AE2FC0E1CC856753980670A6A09 ** get_address_of_LocalNotificationExpired_9() { return &___LocalNotificationExpired_9; }
	inline void set_LocalNotificationExpired_9(Action_1_tF8CCF5268A443AE2FC0E1CC856753980670A6A09 * value)
	{
		___LocalNotificationExpired_9 = value;
		Il2CppCodeGenWriteBarrier((&___LocalNotificationExpired_9), value);
	}

	inline static int32_t get_offset_of_U3CPlatformU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(GameNotificationsManager_tA91A2FAF80490C54B58C02C8B5652AFA290D6B9F, ___U3CPlatformU3Ek__BackingField_10)); }
	inline RuntimeObject* get_U3CPlatformU3Ek__BackingField_10() const { return ___U3CPlatformU3Ek__BackingField_10; }
	inline RuntimeObject** get_address_of_U3CPlatformU3Ek__BackingField_10() { return &___U3CPlatformU3Ek__BackingField_10; }
	inline void set_U3CPlatformU3Ek__BackingField_10(RuntimeObject* value)
	{
		___U3CPlatformU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPlatformU3Ek__BackingField_10), value);
	}

	inline static int32_t get_offset_of_U3CPendingNotificationsU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GameNotificationsManager_tA91A2FAF80490C54B58C02C8B5652AFA290D6B9F, ___U3CPendingNotificationsU3Ek__BackingField_11)); }
	inline List_1_tD4AB95F0286C0D90E2D91E8BD2380724938469CB * get_U3CPendingNotificationsU3Ek__BackingField_11() const { return ___U3CPendingNotificationsU3Ek__BackingField_11; }
	inline List_1_tD4AB95F0286C0D90E2D91E8BD2380724938469CB ** get_address_of_U3CPendingNotificationsU3Ek__BackingField_11() { return &___U3CPendingNotificationsU3Ek__BackingField_11; }
	inline void set_U3CPendingNotificationsU3Ek__BackingField_11(List_1_tD4AB95F0286C0D90E2D91E8BD2380724938469CB * value)
	{
		___U3CPendingNotificationsU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPendingNotificationsU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CSerializerU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GameNotificationsManager_tA91A2FAF80490C54B58C02C8B5652AFA290D6B9F, ___U3CSerializerU3Ek__BackingField_12)); }
	inline RuntimeObject* get_U3CSerializerU3Ek__BackingField_12() const { return ___U3CSerializerU3Ek__BackingField_12; }
	inline RuntimeObject** get_address_of_U3CSerializerU3Ek__BackingField_12() { return &___U3CSerializerU3Ek__BackingField_12; }
	inline void set_U3CSerializerU3Ek__BackingField_12(RuntimeObject* value)
	{
		___U3CSerializerU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSerializerU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CInitializedU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(GameNotificationsManager_tA91A2FAF80490C54B58C02C8B5652AFA290D6B9F, ___U3CInitializedU3Ek__BackingField_13)); }
	inline bool get_U3CInitializedU3Ek__BackingField_13() const { return ___U3CInitializedU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CInitializedU3Ek__BackingField_13() { return &___U3CInitializedU3Ek__BackingField_13; }
	inline void set_U3CInitializedU3Ek__BackingField_13(bool value)
	{
		___U3CInitializedU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_inForeground_14() { return static_cast<int32_t>(offsetof(GameNotificationsManager_tA91A2FAF80490C54B58C02C8B5652AFA290D6B9F, ___inForeground_14)); }
	inline bool get_inForeground_14() const { return ___inForeground_14; }
	inline bool* get_address_of_inForeground_14() { return &___inForeground_14; }
	inline void set_inForeground_14(bool value)
	{
		___inForeground_14 = value;
	}
};

struct GameNotificationsManager_tA91A2FAF80490C54B58C02C8B5652AFA290D6B9F_StaticFields
{
public:
	// System.TimeSpan NotificationSamples.GameNotificationsManager::MinimumNotificationTime
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___MinimumNotificationTime_5;

public:
	inline static int32_t get_offset_of_MinimumNotificationTime_5() { return static_cast<int32_t>(offsetof(GameNotificationsManager_tA91A2FAF80490C54B58C02C8B5652AFA290D6B9F_StaticFields, ___MinimumNotificationTime_5)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_MinimumNotificationTime_5() const { return ___MinimumNotificationTime_5; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_MinimumNotificationTime_5() { return &___MinimumNotificationTime_5; }
	inline void set_MinimumNotificationTime_5(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___MinimumNotificationTime_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMENOTIFICATIONSMANAGER_TA91A2FAF80490C54B58C02C8B5652AFA290D6B9F_H
#ifndef G_SINGLETON_1_TB7DCC6CAA1F046A6E198AB11B1A0D335CEE90291_H
#define G_SINGLETON_1_TB7DCC6CAA1F046A6E198AB11B1A0D335CEE90291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayx.Graphy.Utils.G_Singleton`1<Tayx.Graphy.GraphyDebugger>
struct  G_Singleton_1_tB7DCC6CAA1F046A6E198AB11B1A0D335CEE90291  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct G_Singleton_1_tB7DCC6CAA1F046A6E198AB11B1A0D335CEE90291_StaticFields
{
public:
	// T Tayx.Graphy.Utils.G_Singleton`1::_instance
	GraphyDebugger_t1DC748A4B53549510635E53605A906AC607CC4C9 * ____instance_4;
	// System.Object Tayx.Graphy.Utils.G_Singleton`1::_lock
	RuntimeObject * ____lock_5;
	// System.Boolean Tayx.Graphy.Utils.G_Singleton`1::_applicationIsQuitting
	bool ____applicationIsQuitting_6;

public:
	inline static int32_t get_offset_of__instance_4() { return static_cast<int32_t>(offsetof(G_Singleton_1_tB7DCC6CAA1F046A6E198AB11B1A0D335CEE90291_StaticFields, ____instance_4)); }
	inline GraphyDebugger_t1DC748A4B53549510635E53605A906AC607CC4C9 * get__instance_4() const { return ____instance_4; }
	inline GraphyDebugger_t1DC748A4B53549510635E53605A906AC607CC4C9 ** get_address_of__instance_4() { return &____instance_4; }
	inline void set__instance_4(GraphyDebugger_t1DC748A4B53549510635E53605A906AC607CC4C9 * value)
	{
		____instance_4 = value;
		Il2CppCodeGenWriteBarrier((&____instance_4), value);
	}

	inline static int32_t get_offset_of__lock_5() { return static_cast<int32_t>(offsetof(G_Singleton_1_tB7DCC6CAA1F046A6E198AB11B1A0D335CEE90291_StaticFields, ____lock_5)); }
	inline RuntimeObject * get__lock_5() const { return ____lock_5; }
	inline RuntimeObject ** get_address_of__lock_5() { return &____lock_5; }
	inline void set__lock_5(RuntimeObject * value)
	{
		____lock_5 = value;
		Il2CppCodeGenWriteBarrier((&____lock_5), value);
	}

	inline static int32_t get_offset_of__applicationIsQuitting_6() { return static_cast<int32_t>(offsetof(G_Singleton_1_tB7DCC6CAA1F046A6E198AB11B1A0D335CEE90291_StaticFields, ____applicationIsQuitting_6)); }
	inline bool get__applicationIsQuitting_6() const { return ____applicationIsQuitting_6; }
	inline bool* get_address_of__applicationIsQuitting_6() { return &____applicationIsQuitting_6; }
	inline void set__applicationIsQuitting_6(bool value)
	{
		____applicationIsQuitting_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // G_SINGLETON_1_TB7DCC6CAA1F046A6E198AB11B1A0D335CEE90291_H
#ifndef MONOINSTALLERBASE_T91D926958396F3B2A0F3F126540C446F01105597_H
#define MONOINSTALLERBASE_T91D926958396F3B2A0F3F126540C446F01105597_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.MonoInstallerBase
struct  MonoInstallerBase_t91D926958396F3B2A0F3F126540C446F01105597  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Zenject.DiContainer Zenject.MonoInstallerBase::_container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____container_4;

public:
	inline static int32_t get_offset_of__container_4() { return static_cast<int32_t>(offsetof(MonoInstallerBase_t91D926958396F3B2A0F3F126540C446F01105597, ____container_4)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__container_4() const { return ____container_4; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__container_4() { return &____container_4; }
	inline void set__container_4(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____container_4 = value;
		Il2CppCodeGenWriteBarrier((&____container_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOINSTALLERBASE_T91D926958396F3B2A0F3F126540C446F01105597_H
#ifndef SCRIPTABLEOBJECTINSTALLER_1_T90B50CF087F2D92E1C0DE5226F8AEF487CC25DED_H
#define SCRIPTABLEOBJECTINSTALLER_1_T90B50CF087F2D92E1C0DE5226F8AEF487CC25DED_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ScriptableObjectInstaller`1<Zenject.SpaceFighter.GameSettingsInstaller>
struct  ScriptableObjectInstaller_1_t90B50CF087F2D92E1C0DE5226F8AEF487CC25DED  : public ScriptableObjectInstaller_tAC791A8E2F09B0B1D8D61690E70CEDB55F1AD8D6
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRIPTABLEOBJECTINSTALLER_1_T90B50CF087F2D92E1C0DE5226F8AEF487CC25DED_H
#ifndef BULLET_TF89BF91D9A75D2C600AC3DCDDC5AF8FCC6F1C6A9_H
#define BULLET_TF89BF91D9A75D2C600AC3DCDDC5AF8FCC6F1C6A9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SpaceFighter.Bullet
struct  Bullet_tF89BF91D9A75D2C600AC3DCDDC5AF8FCC6F1C6A9  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single Zenject.SpaceFighter.Bullet::_startTime
	float ____startTime_4;
	// Zenject.SpaceFighter.BulletTypes Zenject.SpaceFighter.Bullet::_type
	int32_t ____type_5;
	// System.Single Zenject.SpaceFighter.Bullet::_speed
	float ____speed_6;
	// System.Single Zenject.SpaceFighter.Bullet::_lifeTime
	float ____lifeTime_7;
	// UnityEngine.MeshRenderer Zenject.SpaceFighter.Bullet::_renderer
	MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * ____renderer_8;
	// UnityEngine.Material Zenject.SpaceFighter.Bullet::_playerMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ____playerMaterial_9;
	// UnityEngine.Material Zenject.SpaceFighter.Bullet::_enemyMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ____enemyMaterial_10;
	// Zenject.SpaceFighter.Bullet_Pool Zenject.SpaceFighter.Bullet::_bulletPool
	Pool_t28D3C271D1214C1C919D02B9C38A6A5698F231F1 * ____bulletPool_11;

public:
	inline static int32_t get_offset_of__startTime_4() { return static_cast<int32_t>(offsetof(Bullet_tF89BF91D9A75D2C600AC3DCDDC5AF8FCC6F1C6A9, ____startTime_4)); }
	inline float get__startTime_4() const { return ____startTime_4; }
	inline float* get_address_of__startTime_4() { return &____startTime_4; }
	inline void set__startTime_4(float value)
	{
		____startTime_4 = value;
	}

	inline static int32_t get_offset_of__type_5() { return static_cast<int32_t>(offsetof(Bullet_tF89BF91D9A75D2C600AC3DCDDC5AF8FCC6F1C6A9, ____type_5)); }
	inline int32_t get__type_5() const { return ____type_5; }
	inline int32_t* get_address_of__type_5() { return &____type_5; }
	inline void set__type_5(int32_t value)
	{
		____type_5 = value;
	}

	inline static int32_t get_offset_of__speed_6() { return static_cast<int32_t>(offsetof(Bullet_tF89BF91D9A75D2C600AC3DCDDC5AF8FCC6F1C6A9, ____speed_6)); }
	inline float get__speed_6() const { return ____speed_6; }
	inline float* get_address_of__speed_6() { return &____speed_6; }
	inline void set__speed_6(float value)
	{
		____speed_6 = value;
	}

	inline static int32_t get_offset_of__lifeTime_7() { return static_cast<int32_t>(offsetof(Bullet_tF89BF91D9A75D2C600AC3DCDDC5AF8FCC6F1C6A9, ____lifeTime_7)); }
	inline float get__lifeTime_7() const { return ____lifeTime_7; }
	inline float* get_address_of__lifeTime_7() { return &____lifeTime_7; }
	inline void set__lifeTime_7(float value)
	{
		____lifeTime_7 = value;
	}

	inline static int32_t get_offset_of__renderer_8() { return static_cast<int32_t>(offsetof(Bullet_tF89BF91D9A75D2C600AC3DCDDC5AF8FCC6F1C6A9, ____renderer_8)); }
	inline MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * get__renderer_8() const { return ____renderer_8; }
	inline MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED ** get_address_of__renderer_8() { return &____renderer_8; }
	inline void set__renderer_8(MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * value)
	{
		____renderer_8 = value;
		Il2CppCodeGenWriteBarrier((&____renderer_8), value);
	}

	inline static int32_t get_offset_of__playerMaterial_9() { return static_cast<int32_t>(offsetof(Bullet_tF89BF91D9A75D2C600AC3DCDDC5AF8FCC6F1C6A9, ____playerMaterial_9)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get__playerMaterial_9() const { return ____playerMaterial_9; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of__playerMaterial_9() { return &____playerMaterial_9; }
	inline void set__playerMaterial_9(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		____playerMaterial_9 = value;
		Il2CppCodeGenWriteBarrier((&____playerMaterial_9), value);
	}

	inline static int32_t get_offset_of__enemyMaterial_10() { return static_cast<int32_t>(offsetof(Bullet_tF89BF91D9A75D2C600AC3DCDDC5AF8FCC6F1C6A9, ____enemyMaterial_10)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get__enemyMaterial_10() const { return ____enemyMaterial_10; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of__enemyMaterial_10() { return &____enemyMaterial_10; }
	inline void set__enemyMaterial_10(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		____enemyMaterial_10 = value;
		Il2CppCodeGenWriteBarrier((&____enemyMaterial_10), value);
	}

	inline static int32_t get_offset_of__bulletPool_11() { return static_cast<int32_t>(offsetof(Bullet_tF89BF91D9A75D2C600AC3DCDDC5AF8FCC6F1C6A9, ____bulletPool_11)); }
	inline Pool_t28D3C271D1214C1C919D02B9C38A6A5698F231F1 * get__bulletPool_11() const { return ____bulletPool_11; }
	inline Pool_t28D3C271D1214C1C919D02B9C38A6A5698F231F1 ** get_address_of__bulletPool_11() { return &____bulletPool_11; }
	inline void set__bulletPool_11(Pool_t28D3C271D1214C1C919D02B9C38A6A5698F231F1 * value)
	{
		____bulletPool_11 = value;
		Il2CppCodeGenWriteBarrier((&____bulletPool_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BULLET_TF89BF91D9A75D2C600AC3DCDDC5AF8FCC6F1C6A9_H
#ifndef CONTROLSDISPLAY_T40059FC98A1A0C996E943225FFB10945BB5B75FC_H
#define CONTROLSDISPLAY_T40059FC98A1A0C996E943225FFB10945BB5B75FC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SpaceFighter.ControlsDisplay
struct  ControlsDisplay_t40059FC98A1A0C996E943225FFB10945BB5B75FC  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single Zenject.SpaceFighter.ControlsDisplay::_leftPadding
	float ____leftPadding_4;
	// System.Single Zenject.SpaceFighter.ControlsDisplay::_topPadding
	float ____topPadding_5;
	// System.Single Zenject.SpaceFighter.ControlsDisplay::_width
	float ____width_6;
	// System.Single Zenject.SpaceFighter.ControlsDisplay::_height
	float ____height_7;

public:
	inline static int32_t get_offset_of__leftPadding_4() { return static_cast<int32_t>(offsetof(ControlsDisplay_t40059FC98A1A0C996E943225FFB10945BB5B75FC, ____leftPadding_4)); }
	inline float get__leftPadding_4() const { return ____leftPadding_4; }
	inline float* get_address_of__leftPadding_4() { return &____leftPadding_4; }
	inline void set__leftPadding_4(float value)
	{
		____leftPadding_4 = value;
	}

	inline static int32_t get_offset_of__topPadding_5() { return static_cast<int32_t>(offsetof(ControlsDisplay_t40059FC98A1A0C996E943225FFB10945BB5B75FC, ____topPadding_5)); }
	inline float get__topPadding_5() const { return ____topPadding_5; }
	inline float* get_address_of__topPadding_5() { return &____topPadding_5; }
	inline void set__topPadding_5(float value)
	{
		____topPadding_5 = value;
	}

	inline static int32_t get_offset_of__width_6() { return static_cast<int32_t>(offsetof(ControlsDisplay_t40059FC98A1A0C996E943225FFB10945BB5B75FC, ____width_6)); }
	inline float get__width_6() const { return ____width_6; }
	inline float* get_address_of__width_6() { return &____width_6; }
	inline void set__width_6(float value)
	{
		____width_6 = value;
	}

	inline static int32_t get_offset_of__height_7() { return static_cast<int32_t>(offsetof(ControlsDisplay_t40059FC98A1A0C996E943225FFB10945BB5B75FC, ____height_7)); }
	inline float get__height_7() const { return ____height_7; }
	inline float* get_address_of__height_7() { return &____height_7; }
	inline void set__height_7(float value)
	{
		____height_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLSDISPLAY_T40059FC98A1A0C996E943225FFB10945BB5B75FC_H
#ifndef ENEMYFACADE_T1D5DD8AEDE16BD5F9C2C32C4C0AB954CE1811FC1_H
#define ENEMYFACADE_T1D5DD8AEDE16BD5F9C2C32C4C0AB954CE1811FC1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SpaceFighter.EnemyFacade
struct  EnemyFacade_t1D5DD8AEDE16BD5F9C2C32C4C0AB954CE1811FC1  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Zenject.SpaceFighter.Enemy Zenject.SpaceFighter.EnemyFacade::_enemy
	Enemy_tC40CA91F878B23445D874D13243C2824315DE3D2 * ____enemy_4;
	// Zenject.SpaceFighter.EnemyTunables Zenject.SpaceFighter.EnemyFacade::_tunables
	EnemyTunables_tD9997725AFF614521DD7088FFF23C1D8DD06DE18 * ____tunables_5;
	// Zenject.SpaceFighter.EnemyDeathHandler Zenject.SpaceFighter.EnemyFacade::_deathHandler
	EnemyDeathHandler_t87BF96335A31C0712F7B9E80B22587838EE9B4C5 * ____deathHandler_6;
	// Zenject.SpaceFighter.EnemyStateManager Zenject.SpaceFighter.EnemyFacade::_stateManager
	EnemyStateManager_tD3E2662DA02D660CB7356CE3563F928B0E220C9F * ____stateManager_7;

public:
	inline static int32_t get_offset_of__enemy_4() { return static_cast<int32_t>(offsetof(EnemyFacade_t1D5DD8AEDE16BD5F9C2C32C4C0AB954CE1811FC1, ____enemy_4)); }
	inline Enemy_tC40CA91F878B23445D874D13243C2824315DE3D2 * get__enemy_4() const { return ____enemy_4; }
	inline Enemy_tC40CA91F878B23445D874D13243C2824315DE3D2 ** get_address_of__enemy_4() { return &____enemy_4; }
	inline void set__enemy_4(Enemy_tC40CA91F878B23445D874D13243C2824315DE3D2 * value)
	{
		____enemy_4 = value;
		Il2CppCodeGenWriteBarrier((&____enemy_4), value);
	}

	inline static int32_t get_offset_of__tunables_5() { return static_cast<int32_t>(offsetof(EnemyFacade_t1D5DD8AEDE16BD5F9C2C32C4C0AB954CE1811FC1, ____tunables_5)); }
	inline EnemyTunables_tD9997725AFF614521DD7088FFF23C1D8DD06DE18 * get__tunables_5() const { return ____tunables_5; }
	inline EnemyTunables_tD9997725AFF614521DD7088FFF23C1D8DD06DE18 ** get_address_of__tunables_5() { return &____tunables_5; }
	inline void set__tunables_5(EnemyTunables_tD9997725AFF614521DD7088FFF23C1D8DD06DE18 * value)
	{
		____tunables_5 = value;
		Il2CppCodeGenWriteBarrier((&____tunables_5), value);
	}

	inline static int32_t get_offset_of__deathHandler_6() { return static_cast<int32_t>(offsetof(EnemyFacade_t1D5DD8AEDE16BD5F9C2C32C4C0AB954CE1811FC1, ____deathHandler_6)); }
	inline EnemyDeathHandler_t87BF96335A31C0712F7B9E80B22587838EE9B4C5 * get__deathHandler_6() const { return ____deathHandler_6; }
	inline EnemyDeathHandler_t87BF96335A31C0712F7B9E80B22587838EE9B4C5 ** get_address_of__deathHandler_6() { return &____deathHandler_6; }
	inline void set__deathHandler_6(EnemyDeathHandler_t87BF96335A31C0712F7B9E80B22587838EE9B4C5 * value)
	{
		____deathHandler_6 = value;
		Il2CppCodeGenWriteBarrier((&____deathHandler_6), value);
	}

	inline static int32_t get_offset_of__stateManager_7() { return static_cast<int32_t>(offsetof(EnemyFacade_t1D5DD8AEDE16BD5F9C2C32C4C0AB954CE1811FC1, ____stateManager_7)); }
	inline EnemyStateManager_tD3E2662DA02D660CB7356CE3563F928B0E220C9F * get__stateManager_7() const { return ____stateManager_7; }
	inline EnemyStateManager_tD3E2662DA02D660CB7356CE3563F928B0E220C9F ** get_address_of__stateManager_7() { return &____stateManager_7; }
	inline void set__stateManager_7(EnemyStateManager_tD3E2662DA02D660CB7356CE3563F928B0E220C9F * value)
	{
		____stateManager_7 = value;
		Il2CppCodeGenWriteBarrier((&____stateManager_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENEMYFACADE_T1D5DD8AEDE16BD5F9C2C32C4C0AB954CE1811FC1_H
#ifndef EXPLOSION_T6C30B8B388089AA2C76F253AAFDE0F7CC0E05B86_H
#define EXPLOSION_T6C30B8B388089AA2C76F253AAFDE0F7CC0E05B86_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SpaceFighter.Explosion
struct  Explosion_t6C30B8B388089AA2C76F253AAFDE0F7CC0E05B86  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single Zenject.SpaceFighter.Explosion::_lifeTime
	float ____lifeTime_4;
	// UnityEngine.ParticleSystem Zenject.SpaceFighter.Explosion::_particleSystem
	ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * ____particleSystem_5;
	// UnityEngine.AudioClip Zenject.SpaceFighter.Explosion::_sound
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ____sound_6;
	// System.Single Zenject.SpaceFighter.Explosion::_soundVolume
	float ____soundVolume_7;
	// System.Single Zenject.SpaceFighter.Explosion::_startTime
	float ____startTime_8;
	// Zenject.SpaceFighter.Explosion_Pool Zenject.SpaceFighter.Explosion::_pool
	Pool_tF6D222503E0FF0BBA73DDF77321A0C8B6CA1D7C7 * ____pool_9;

public:
	inline static int32_t get_offset_of__lifeTime_4() { return static_cast<int32_t>(offsetof(Explosion_t6C30B8B388089AA2C76F253AAFDE0F7CC0E05B86, ____lifeTime_4)); }
	inline float get__lifeTime_4() const { return ____lifeTime_4; }
	inline float* get_address_of__lifeTime_4() { return &____lifeTime_4; }
	inline void set__lifeTime_4(float value)
	{
		____lifeTime_4 = value;
	}

	inline static int32_t get_offset_of__particleSystem_5() { return static_cast<int32_t>(offsetof(Explosion_t6C30B8B388089AA2C76F253AAFDE0F7CC0E05B86, ____particleSystem_5)); }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * get__particleSystem_5() const { return ____particleSystem_5; }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D ** get_address_of__particleSystem_5() { return &____particleSystem_5; }
	inline void set__particleSystem_5(ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * value)
	{
		____particleSystem_5 = value;
		Il2CppCodeGenWriteBarrier((&____particleSystem_5), value);
	}

	inline static int32_t get_offset_of__sound_6() { return static_cast<int32_t>(offsetof(Explosion_t6C30B8B388089AA2C76F253AAFDE0F7CC0E05B86, ____sound_6)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get__sound_6() const { return ____sound_6; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of__sound_6() { return &____sound_6; }
	inline void set__sound_6(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		____sound_6 = value;
		Il2CppCodeGenWriteBarrier((&____sound_6), value);
	}

	inline static int32_t get_offset_of__soundVolume_7() { return static_cast<int32_t>(offsetof(Explosion_t6C30B8B388089AA2C76F253AAFDE0F7CC0E05B86, ____soundVolume_7)); }
	inline float get__soundVolume_7() const { return ____soundVolume_7; }
	inline float* get_address_of__soundVolume_7() { return &____soundVolume_7; }
	inline void set__soundVolume_7(float value)
	{
		____soundVolume_7 = value;
	}

	inline static int32_t get_offset_of__startTime_8() { return static_cast<int32_t>(offsetof(Explosion_t6C30B8B388089AA2C76F253AAFDE0F7CC0E05B86, ____startTime_8)); }
	inline float get__startTime_8() const { return ____startTime_8; }
	inline float* get_address_of__startTime_8() { return &____startTime_8; }
	inline void set__startTime_8(float value)
	{
		____startTime_8 = value;
	}

	inline static int32_t get_offset_of__pool_9() { return static_cast<int32_t>(offsetof(Explosion_t6C30B8B388089AA2C76F253AAFDE0F7CC0E05B86, ____pool_9)); }
	inline Pool_tF6D222503E0FF0BBA73DDF77321A0C8B6CA1D7C7 * get__pool_9() const { return ____pool_9; }
	inline Pool_tF6D222503E0FF0BBA73DDF77321A0C8B6CA1D7C7 ** get_address_of__pool_9() { return &____pool_9; }
	inline void set__pool_9(Pool_tF6D222503E0FF0BBA73DDF77321A0C8B6CA1D7C7 * value)
	{
		____pool_9 = value;
		Il2CppCodeGenWriteBarrier((&____pool_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPLOSION_T6C30B8B388089AA2C76F253AAFDE0F7CC0E05B86_H
#ifndef PLAYERFACADE_T3868059ED0F5190576FE5901D6D602737C505B91_H
#define PLAYERFACADE_T3868059ED0F5190576FE5901D6D602737C505B91_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SpaceFighter.PlayerFacade
struct  PlayerFacade_t3868059ED0F5190576FE5901D6D602737C505B91  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Zenject.SpaceFighter.Player Zenject.SpaceFighter.PlayerFacade::_model
	Player_t0644AFD1050593EF42B2459D30EA133F19E7A402 * ____model_4;
	// Zenject.SpaceFighter.PlayerDamageHandler Zenject.SpaceFighter.PlayerFacade::_hitHandler
	PlayerDamageHandler_tFF3D703F066B3204ADD106FCC8B95D7EF61B4E33 * ____hitHandler_5;

public:
	inline static int32_t get_offset_of__model_4() { return static_cast<int32_t>(offsetof(PlayerFacade_t3868059ED0F5190576FE5901D6D602737C505B91, ____model_4)); }
	inline Player_t0644AFD1050593EF42B2459D30EA133F19E7A402 * get__model_4() const { return ____model_4; }
	inline Player_t0644AFD1050593EF42B2459D30EA133F19E7A402 ** get_address_of__model_4() { return &____model_4; }
	inline void set__model_4(Player_t0644AFD1050593EF42B2459D30EA133F19E7A402 * value)
	{
		____model_4 = value;
		Il2CppCodeGenWriteBarrier((&____model_4), value);
	}

	inline static int32_t get_offset_of__hitHandler_5() { return static_cast<int32_t>(offsetof(PlayerFacade_t3868059ED0F5190576FE5901D6D602737C505B91, ____hitHandler_5)); }
	inline PlayerDamageHandler_tFF3D703F066B3204ADD106FCC8B95D7EF61B4E33 * get__hitHandler_5() const { return ____hitHandler_5; }
	inline PlayerDamageHandler_tFF3D703F066B3204ADD106FCC8B95D7EF61B4E33 ** get_address_of__hitHandler_5() { return &____hitHandler_5; }
	inline void set__hitHandler_5(PlayerDamageHandler_tFF3D703F066B3204ADD106FCC8B95D7EF61B4E33 * value)
	{
		____hitHandler_5 = value;
		Il2CppCodeGenWriteBarrier((&____hitHandler_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERFACADE_T3868059ED0F5190576FE5901D6D602737C505B91_H
#ifndef PLAYERGUI_T4C3FE8F85E138707CA0C84CD109F303379432D96_H
#define PLAYERGUI_T4C3FE8F85E138707CA0C84CD109F303379432D96_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SpaceFighter.PlayerGui
struct  PlayerGui_t4C3FE8F85E138707CA0C84CD109F303379432D96  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single Zenject.SpaceFighter.PlayerGui::_leftPadding
	float ____leftPadding_4;
	// System.Single Zenject.SpaceFighter.PlayerGui::_bottomPadding
	float ____bottomPadding_5;
	// System.Single Zenject.SpaceFighter.PlayerGui::_labelWidth
	float ____labelWidth_6;
	// System.Single Zenject.SpaceFighter.PlayerGui::_labelHeight
	float ____labelHeight_7;
	// System.Single Zenject.SpaceFighter.PlayerGui::_textureWidth
	float ____textureWidth_8;
	// System.Single Zenject.SpaceFighter.PlayerGui::_textureHeight
	float ____textureHeight_9;
	// System.Single Zenject.SpaceFighter.PlayerGui::_killCountOffset
	float ____killCountOffset_10;
	// UnityEngine.Color Zenject.SpaceFighter.PlayerGui::_foregroundColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ____foregroundColor_11;
	// UnityEngine.Color Zenject.SpaceFighter.PlayerGui::_backgroundColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ____backgroundColor_12;
	// Zenject.SpaceFighter.Player Zenject.SpaceFighter.PlayerGui::_player
	Player_t0644AFD1050593EF42B2459D30EA133F19E7A402 * ____player_13;
	// UnityEngine.Texture2D Zenject.SpaceFighter.PlayerGui::_textureForeground
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ____textureForeground_14;
	// UnityEngine.Texture2D Zenject.SpaceFighter.PlayerGui::_textureBackground
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ____textureBackground_15;
	// System.Int32 Zenject.SpaceFighter.PlayerGui::_killCount
	int32_t ____killCount_16;

public:
	inline static int32_t get_offset_of__leftPadding_4() { return static_cast<int32_t>(offsetof(PlayerGui_t4C3FE8F85E138707CA0C84CD109F303379432D96, ____leftPadding_4)); }
	inline float get__leftPadding_4() const { return ____leftPadding_4; }
	inline float* get_address_of__leftPadding_4() { return &____leftPadding_4; }
	inline void set__leftPadding_4(float value)
	{
		____leftPadding_4 = value;
	}

	inline static int32_t get_offset_of__bottomPadding_5() { return static_cast<int32_t>(offsetof(PlayerGui_t4C3FE8F85E138707CA0C84CD109F303379432D96, ____bottomPadding_5)); }
	inline float get__bottomPadding_5() const { return ____bottomPadding_5; }
	inline float* get_address_of__bottomPadding_5() { return &____bottomPadding_5; }
	inline void set__bottomPadding_5(float value)
	{
		____bottomPadding_5 = value;
	}

	inline static int32_t get_offset_of__labelWidth_6() { return static_cast<int32_t>(offsetof(PlayerGui_t4C3FE8F85E138707CA0C84CD109F303379432D96, ____labelWidth_6)); }
	inline float get__labelWidth_6() const { return ____labelWidth_6; }
	inline float* get_address_of__labelWidth_6() { return &____labelWidth_6; }
	inline void set__labelWidth_6(float value)
	{
		____labelWidth_6 = value;
	}

	inline static int32_t get_offset_of__labelHeight_7() { return static_cast<int32_t>(offsetof(PlayerGui_t4C3FE8F85E138707CA0C84CD109F303379432D96, ____labelHeight_7)); }
	inline float get__labelHeight_7() const { return ____labelHeight_7; }
	inline float* get_address_of__labelHeight_7() { return &____labelHeight_7; }
	inline void set__labelHeight_7(float value)
	{
		____labelHeight_7 = value;
	}

	inline static int32_t get_offset_of__textureWidth_8() { return static_cast<int32_t>(offsetof(PlayerGui_t4C3FE8F85E138707CA0C84CD109F303379432D96, ____textureWidth_8)); }
	inline float get__textureWidth_8() const { return ____textureWidth_8; }
	inline float* get_address_of__textureWidth_8() { return &____textureWidth_8; }
	inline void set__textureWidth_8(float value)
	{
		____textureWidth_8 = value;
	}

	inline static int32_t get_offset_of__textureHeight_9() { return static_cast<int32_t>(offsetof(PlayerGui_t4C3FE8F85E138707CA0C84CD109F303379432D96, ____textureHeight_9)); }
	inline float get__textureHeight_9() const { return ____textureHeight_9; }
	inline float* get_address_of__textureHeight_9() { return &____textureHeight_9; }
	inline void set__textureHeight_9(float value)
	{
		____textureHeight_9 = value;
	}

	inline static int32_t get_offset_of__killCountOffset_10() { return static_cast<int32_t>(offsetof(PlayerGui_t4C3FE8F85E138707CA0C84CD109F303379432D96, ____killCountOffset_10)); }
	inline float get__killCountOffset_10() const { return ____killCountOffset_10; }
	inline float* get_address_of__killCountOffset_10() { return &____killCountOffset_10; }
	inline void set__killCountOffset_10(float value)
	{
		____killCountOffset_10 = value;
	}

	inline static int32_t get_offset_of__foregroundColor_11() { return static_cast<int32_t>(offsetof(PlayerGui_t4C3FE8F85E138707CA0C84CD109F303379432D96, ____foregroundColor_11)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get__foregroundColor_11() const { return ____foregroundColor_11; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of__foregroundColor_11() { return &____foregroundColor_11; }
	inline void set__foregroundColor_11(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		____foregroundColor_11 = value;
	}

	inline static int32_t get_offset_of__backgroundColor_12() { return static_cast<int32_t>(offsetof(PlayerGui_t4C3FE8F85E138707CA0C84CD109F303379432D96, ____backgroundColor_12)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get__backgroundColor_12() const { return ____backgroundColor_12; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of__backgroundColor_12() { return &____backgroundColor_12; }
	inline void set__backgroundColor_12(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		____backgroundColor_12 = value;
	}

	inline static int32_t get_offset_of__player_13() { return static_cast<int32_t>(offsetof(PlayerGui_t4C3FE8F85E138707CA0C84CD109F303379432D96, ____player_13)); }
	inline Player_t0644AFD1050593EF42B2459D30EA133F19E7A402 * get__player_13() const { return ____player_13; }
	inline Player_t0644AFD1050593EF42B2459D30EA133F19E7A402 ** get_address_of__player_13() { return &____player_13; }
	inline void set__player_13(Player_t0644AFD1050593EF42B2459D30EA133F19E7A402 * value)
	{
		____player_13 = value;
		Il2CppCodeGenWriteBarrier((&____player_13), value);
	}

	inline static int32_t get_offset_of__textureForeground_14() { return static_cast<int32_t>(offsetof(PlayerGui_t4C3FE8F85E138707CA0C84CD109F303379432D96, ____textureForeground_14)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get__textureForeground_14() const { return ____textureForeground_14; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of__textureForeground_14() { return &____textureForeground_14; }
	inline void set__textureForeground_14(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		____textureForeground_14 = value;
		Il2CppCodeGenWriteBarrier((&____textureForeground_14), value);
	}

	inline static int32_t get_offset_of__textureBackground_15() { return static_cast<int32_t>(offsetof(PlayerGui_t4C3FE8F85E138707CA0C84CD109F303379432D96, ____textureBackground_15)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get__textureBackground_15() const { return ____textureBackground_15; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of__textureBackground_15() { return &____textureBackground_15; }
	inline void set__textureBackground_15(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		____textureBackground_15 = value;
		Il2CppCodeGenWriteBarrier((&____textureBackground_15), value);
	}

	inline static int32_t get_offset_of__killCount_16() { return static_cast<int32_t>(offsetof(PlayerGui_t4C3FE8F85E138707CA0C84CD109F303379432D96, ____killCount_16)); }
	inline int32_t get__killCount_16() const { return ____killCount_16; }
	inline int32_t* get_address_of__killCount_16() { return &____killCount_16; }
	inline void set__killCount_16(int32_t value)
	{
		____killCount_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERGUI_T4C3FE8F85E138707CA0C84CD109F303379432D96_H
#ifndef ZENAUTOINJECTER_T9688C54CDC7F85464B4A5AA87184871DC3C1B236_H
#define ZENAUTOINJECTER_T9688C54CDC7F85464B4A5AA87184871DC3C1B236_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ZenAutoInjecter
struct  ZenAutoInjecter_t9688C54CDC7F85464B4A5AA87184871DC3C1B236  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Zenject.ZenAutoInjecter_ContainerSources Zenject.ZenAutoInjecter::_containerSource
	int32_t ____containerSource_4;
	// System.Boolean Zenject.ZenAutoInjecter::_hasStarted
	bool ____hasStarted_5;

public:
	inline static int32_t get_offset_of__containerSource_4() { return static_cast<int32_t>(offsetof(ZenAutoInjecter_t9688C54CDC7F85464B4A5AA87184871DC3C1B236, ____containerSource_4)); }
	inline int32_t get__containerSource_4() const { return ____containerSource_4; }
	inline int32_t* get_address_of__containerSource_4() { return &____containerSource_4; }
	inline void set__containerSource_4(int32_t value)
	{
		____containerSource_4 = value;
	}

	inline static int32_t get_offset_of__hasStarted_5() { return static_cast<int32_t>(offsetof(ZenAutoInjecter_t9688C54CDC7F85464B4A5AA87184871DC3C1B236, ____hasStarted_5)); }
	inline bool get__hasStarted_5() const { return ____hasStarted_5; }
	inline bool* get_address_of__hasStarted_5() { return &____hasStarted_5; }
	inline void set__hasStarted_5(bool value)
	{
		____hasStarted_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZENAUTOINJECTER_T9688C54CDC7F85464B4A5AA87184871DC3C1B236_H
#ifndef GRAPHYDEBUGGER_T1DC748A4B53549510635E53605A906AC607CC4C9_H
#define GRAPHYDEBUGGER_T1DC748A4B53549510635E53605A906AC607CC4C9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayx.Graphy.GraphyDebugger
struct  GraphyDebugger_t1DC748A4B53549510635E53605A906AC607CC4C9  : public G_Singleton_1_tB7DCC6CAA1F046A6E198AB11B1A0D335CEE90291
{
public:
	// System.Collections.Generic.List`1<Tayx.Graphy.GraphyDebugger_DebugPacket> Tayx.Graphy.GraphyDebugger::m_debugPackets
	List_1_tF47A57C0BAB879ECB9F887A704B0308283D36180 * ___m_debugPackets_7;
	// Tayx.Graphy.Fps.G_FpsMonitor Tayx.Graphy.GraphyDebugger::m_fpsMonitor
	G_FpsMonitor_tDCDEDA2708585BE6B0202F2B3230D8C5F6820672 * ___m_fpsMonitor_8;
	// Tayx.Graphy.Ram.G_RamMonitor Tayx.Graphy.GraphyDebugger::m_ramMonitor
	G_RamMonitor_tCE95C3ECC0C03781E405698F56E20B12BC2650CA * ___m_ramMonitor_9;
	// Tayx.Graphy.Audio.G_AudioMonitor Tayx.Graphy.GraphyDebugger::m_audioMonitor
	G_AudioMonitor_t386A2D77995058DA7241FC1C90AF9FF6DE38EA2E * ___m_audioMonitor_10;

public:
	inline static int32_t get_offset_of_m_debugPackets_7() { return static_cast<int32_t>(offsetof(GraphyDebugger_t1DC748A4B53549510635E53605A906AC607CC4C9, ___m_debugPackets_7)); }
	inline List_1_tF47A57C0BAB879ECB9F887A704B0308283D36180 * get_m_debugPackets_7() const { return ___m_debugPackets_7; }
	inline List_1_tF47A57C0BAB879ECB9F887A704B0308283D36180 ** get_address_of_m_debugPackets_7() { return &___m_debugPackets_7; }
	inline void set_m_debugPackets_7(List_1_tF47A57C0BAB879ECB9F887A704B0308283D36180 * value)
	{
		___m_debugPackets_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_debugPackets_7), value);
	}

	inline static int32_t get_offset_of_m_fpsMonitor_8() { return static_cast<int32_t>(offsetof(GraphyDebugger_t1DC748A4B53549510635E53605A906AC607CC4C9, ___m_fpsMonitor_8)); }
	inline G_FpsMonitor_tDCDEDA2708585BE6B0202F2B3230D8C5F6820672 * get_m_fpsMonitor_8() const { return ___m_fpsMonitor_8; }
	inline G_FpsMonitor_tDCDEDA2708585BE6B0202F2B3230D8C5F6820672 ** get_address_of_m_fpsMonitor_8() { return &___m_fpsMonitor_8; }
	inline void set_m_fpsMonitor_8(G_FpsMonitor_tDCDEDA2708585BE6B0202F2B3230D8C5F6820672 * value)
	{
		___m_fpsMonitor_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_fpsMonitor_8), value);
	}

	inline static int32_t get_offset_of_m_ramMonitor_9() { return static_cast<int32_t>(offsetof(GraphyDebugger_t1DC748A4B53549510635E53605A906AC607CC4C9, ___m_ramMonitor_9)); }
	inline G_RamMonitor_tCE95C3ECC0C03781E405698F56E20B12BC2650CA * get_m_ramMonitor_9() const { return ___m_ramMonitor_9; }
	inline G_RamMonitor_tCE95C3ECC0C03781E405698F56E20B12BC2650CA ** get_address_of_m_ramMonitor_9() { return &___m_ramMonitor_9; }
	inline void set_m_ramMonitor_9(G_RamMonitor_tCE95C3ECC0C03781E405698F56E20B12BC2650CA * value)
	{
		___m_ramMonitor_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_ramMonitor_9), value);
	}

	inline static int32_t get_offset_of_m_audioMonitor_10() { return static_cast<int32_t>(offsetof(GraphyDebugger_t1DC748A4B53549510635E53605A906AC607CC4C9, ___m_audioMonitor_10)); }
	inline G_AudioMonitor_t386A2D77995058DA7241FC1C90AF9FF6DE38EA2E * get_m_audioMonitor_10() const { return ___m_audioMonitor_10; }
	inline G_AudioMonitor_t386A2D77995058DA7241FC1C90AF9FF6DE38EA2E ** get_address_of_m_audioMonitor_10() { return &___m_audioMonitor_10; }
	inline void set_m_audioMonitor_10(G_AudioMonitor_t386A2D77995058DA7241FC1C90AF9FF6DE38EA2E * value)
	{
		___m_audioMonitor_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_audioMonitor_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHYDEBUGGER_T1DC748A4B53549510635E53605A906AC607CC4C9_H
#ifndef MONOINSTALLER_T9D57364413F95D7A946A3B7901B67365D562A343_H
#define MONOINSTALLER_T9D57364413F95D7A946A3B7901B67365D562A343_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.MonoInstaller
struct  MonoInstaller_t9D57364413F95D7A946A3B7901B67365D562A343  : public MonoInstallerBase_t91D926958396F3B2A0F3F126540C446F01105597
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOINSTALLER_T9D57364413F95D7A946A3B7901B67365D562A343_H
#ifndef GAMESETTINGSINSTALLER_T81335697D090EA540B42B2092934CF7177DB634E_H
#define GAMESETTINGSINSTALLER_T81335697D090EA540B42B2092934CF7177DB634E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SpaceFighter.GameSettingsInstaller
struct  GameSettingsInstaller_t81335697D090EA540B42B2092934CF7177DB634E  : public ScriptableObjectInstaller_1_t90B50CF087F2D92E1C0DE5226F8AEF487CC25DED
{
public:
	// Zenject.SpaceFighter.EnemySpawner_Settings Zenject.SpaceFighter.GameSettingsInstaller::EnemySpawner
	Settings_tD4EAF29A0539BDE0CFDC096ED188196E8DD744C2 * ___EnemySpawner_5;
	// Zenject.SpaceFighter.GameRestartHandler_Settings Zenject.SpaceFighter.GameSettingsInstaller::GameRestartHandler
	Settings_tDA43D3BAE8BE0B223AEFEE4FC2782ACB7169D1C2 * ___GameRestartHandler_6;
	// Zenject.SpaceFighter.GameInstaller_Settings Zenject.SpaceFighter.GameSettingsInstaller::GameInstaller
	Settings_tDDDA6539A297D2F0F5790F2BE702EABB8BC23312 * ___GameInstaller_7;
	// Zenject.SpaceFighter.GameSettingsInstaller_PlayerSettings Zenject.SpaceFighter.GameSettingsInstaller::Player
	PlayerSettings_t50B498D03A02BD7CD7E2367EFDAD0428FBBC1058 * ___Player_8;
	// Zenject.SpaceFighter.GameSettingsInstaller_EnemySettings Zenject.SpaceFighter.GameSettingsInstaller::Enemy
	EnemySettings_t87F1C13D725FC80A97C27012C240D7E9150A5785 * ___Enemy_9;

public:
	inline static int32_t get_offset_of_EnemySpawner_5() { return static_cast<int32_t>(offsetof(GameSettingsInstaller_t81335697D090EA540B42B2092934CF7177DB634E, ___EnemySpawner_5)); }
	inline Settings_tD4EAF29A0539BDE0CFDC096ED188196E8DD744C2 * get_EnemySpawner_5() const { return ___EnemySpawner_5; }
	inline Settings_tD4EAF29A0539BDE0CFDC096ED188196E8DD744C2 ** get_address_of_EnemySpawner_5() { return &___EnemySpawner_5; }
	inline void set_EnemySpawner_5(Settings_tD4EAF29A0539BDE0CFDC096ED188196E8DD744C2 * value)
	{
		___EnemySpawner_5 = value;
		Il2CppCodeGenWriteBarrier((&___EnemySpawner_5), value);
	}

	inline static int32_t get_offset_of_GameRestartHandler_6() { return static_cast<int32_t>(offsetof(GameSettingsInstaller_t81335697D090EA540B42B2092934CF7177DB634E, ___GameRestartHandler_6)); }
	inline Settings_tDA43D3BAE8BE0B223AEFEE4FC2782ACB7169D1C2 * get_GameRestartHandler_6() const { return ___GameRestartHandler_6; }
	inline Settings_tDA43D3BAE8BE0B223AEFEE4FC2782ACB7169D1C2 ** get_address_of_GameRestartHandler_6() { return &___GameRestartHandler_6; }
	inline void set_GameRestartHandler_6(Settings_tDA43D3BAE8BE0B223AEFEE4FC2782ACB7169D1C2 * value)
	{
		___GameRestartHandler_6 = value;
		Il2CppCodeGenWriteBarrier((&___GameRestartHandler_6), value);
	}

	inline static int32_t get_offset_of_GameInstaller_7() { return static_cast<int32_t>(offsetof(GameSettingsInstaller_t81335697D090EA540B42B2092934CF7177DB634E, ___GameInstaller_7)); }
	inline Settings_tDDDA6539A297D2F0F5790F2BE702EABB8BC23312 * get_GameInstaller_7() const { return ___GameInstaller_7; }
	inline Settings_tDDDA6539A297D2F0F5790F2BE702EABB8BC23312 ** get_address_of_GameInstaller_7() { return &___GameInstaller_7; }
	inline void set_GameInstaller_7(Settings_tDDDA6539A297D2F0F5790F2BE702EABB8BC23312 * value)
	{
		___GameInstaller_7 = value;
		Il2CppCodeGenWriteBarrier((&___GameInstaller_7), value);
	}

	inline static int32_t get_offset_of_Player_8() { return static_cast<int32_t>(offsetof(GameSettingsInstaller_t81335697D090EA540B42B2092934CF7177DB634E, ___Player_8)); }
	inline PlayerSettings_t50B498D03A02BD7CD7E2367EFDAD0428FBBC1058 * get_Player_8() const { return ___Player_8; }
	inline PlayerSettings_t50B498D03A02BD7CD7E2367EFDAD0428FBBC1058 ** get_address_of_Player_8() { return &___Player_8; }
	inline void set_Player_8(PlayerSettings_t50B498D03A02BD7CD7E2367EFDAD0428FBBC1058 * value)
	{
		___Player_8 = value;
		Il2CppCodeGenWriteBarrier((&___Player_8), value);
	}

	inline static int32_t get_offset_of_Enemy_9() { return static_cast<int32_t>(offsetof(GameSettingsInstaller_t81335697D090EA540B42B2092934CF7177DB634E, ___Enemy_9)); }
	inline EnemySettings_t87F1C13D725FC80A97C27012C240D7E9150A5785 * get_Enemy_9() const { return ___Enemy_9; }
	inline EnemySettings_t87F1C13D725FC80A97C27012C240D7E9150A5785 ** get_address_of_Enemy_9() { return &___Enemy_9; }
	inline void set_Enemy_9(EnemySettings_t87F1C13D725FC80A97C27012C240D7E9150A5785 * value)
	{
		___Enemy_9 = value;
		Il2CppCodeGenWriteBarrier((&___Enemy_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMESETTINGSINSTALLER_T81335697D090EA540B42B2092934CF7177DB634E_H
#ifndef ENEMYINSTALLER_T2A05C806CDF4D1E60B9E3BDB910AD512928695C7_H
#define ENEMYINSTALLER_T2A05C806CDF4D1E60B9E3BDB910AD512928695C7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SpaceFighter.EnemyInstaller
struct  EnemyInstaller_t2A05C806CDF4D1E60B9E3BDB910AD512928695C7  : public MonoInstaller_t9D57364413F95D7A946A3B7901B67365D562A343
{
public:
	// Zenject.SpaceFighter.EnemyInstaller_Settings Zenject.SpaceFighter.EnemyInstaller::_settings
	Settings_tD2755B3F26D35678AF75CBB0C5BF9C282206FF91 * ____settings_5;

public:
	inline static int32_t get_offset_of__settings_5() { return static_cast<int32_t>(offsetof(EnemyInstaller_t2A05C806CDF4D1E60B9E3BDB910AD512928695C7, ____settings_5)); }
	inline Settings_tD2755B3F26D35678AF75CBB0C5BF9C282206FF91 * get__settings_5() const { return ____settings_5; }
	inline Settings_tD2755B3F26D35678AF75CBB0C5BF9C282206FF91 ** get_address_of__settings_5() { return &____settings_5; }
	inline void set__settings_5(Settings_tD2755B3F26D35678AF75CBB0C5BF9C282206FF91 * value)
	{
		____settings_5 = value;
		Il2CppCodeGenWriteBarrier((&____settings_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENEMYINSTALLER_T2A05C806CDF4D1E60B9E3BDB910AD512928695C7_H
#ifndef GAMEINSTALLER_T9A03FD076BF550D8511C194043C234A62EB2C2BC_H
#define GAMEINSTALLER_T9A03FD076BF550D8511C194043C234A62EB2C2BC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SpaceFighter.GameInstaller
struct  GameInstaller_t9A03FD076BF550D8511C194043C234A62EB2C2BC  : public MonoInstaller_t9D57364413F95D7A946A3B7901B67365D562A343
{
public:
	// Zenject.SpaceFighter.GameInstaller_Settings Zenject.SpaceFighter.GameInstaller::_settings
	Settings_tDDDA6539A297D2F0F5790F2BE702EABB8BC23312 * ____settings_5;

public:
	inline static int32_t get_offset_of__settings_5() { return static_cast<int32_t>(offsetof(GameInstaller_t9A03FD076BF550D8511C194043C234A62EB2C2BC, ____settings_5)); }
	inline Settings_tDDDA6539A297D2F0F5790F2BE702EABB8BC23312 * get__settings_5() const { return ____settings_5; }
	inline Settings_tDDDA6539A297D2F0F5790F2BE702EABB8BC23312 ** get_address_of__settings_5() { return &____settings_5; }
	inline void set__settings_5(Settings_tDDDA6539A297D2F0F5790F2BE702EABB8BC23312 * value)
	{
		____settings_5 = value;
		Il2CppCodeGenWriteBarrier((&____settings_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEINSTALLER_T9A03FD076BF550D8511C194043C234A62EB2C2BC_H
#ifndef PLAYERINSTALLER_T0AE84D945F4D18F81323F24C912C0437C4939607_H
#define PLAYERINSTALLER_T0AE84D945F4D18F81323F24C912C0437C4939607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SpaceFighter.PlayerInstaller
struct  PlayerInstaller_t0AE84D945F4D18F81323F24C912C0437C4939607  : public MonoInstaller_t9D57364413F95D7A946A3B7901B67365D562A343
{
public:
	// Zenject.SpaceFighter.PlayerInstaller_Settings Zenject.SpaceFighter.PlayerInstaller::_settings
	Settings_tA79246F22F290206CED693068D5770E633DD5CEE * ____settings_5;

public:
	inline static int32_t get_offset_of__settings_5() { return static_cast<int32_t>(offsetof(PlayerInstaller_t0AE84D945F4D18F81323F24C912C0437C4939607, ____settings_5)); }
	inline Settings_tA79246F22F290206CED693068D5770E633DD5CEE * get__settings_5() const { return ____settings_5; }
	inline Settings_tA79246F22F290206CED693068D5770E633DD5CEE ** get_address_of__settings_5() { return &____settings_5; }
	inline void set__settings_5(Settings_tA79246F22F290206CED693068D5770E633DD5CEE * value)
	{
		____settings_5 = value;
		Il2CppCodeGenWriteBarrier((&____settings_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERINSTALLER_T0AE84D945F4D18F81323F24C912C0437C4939607_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7600 = { sizeof (U3CU3Ec__DisplayClass4_0_tA9861CADB22C325532D04EAECC151A608112DB1D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7600[1] = 
{
	U3CU3Ec__DisplayClass4_0_tA9861CADB22C325532D04EAECC151A608112DB1D::get_offset_of_parentType_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7601 = { sizeof (U3CU3Ec__DisplayClass6_0_t3B07096FF650FEB05B494E1A1B3C564BF7A788F8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7601[3] = 
{
	U3CU3Ec__DisplayClass6_0_t3B07096FF650FEB05B494E1A1B3C564BF7A788F8::get_offset_of_heirarchyList_0(),
	U3CU3Ec__DisplayClass6_0_t3B07096FF650FEB05B494E1A1B3C564BF7A788F8::get_offset_of_type_1(),
	U3CU3Ec__DisplayClass6_0_t3B07096FF650FEB05B494E1A1B3C564BF7A788F8::get_offset_of_U3CU3E9__2_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7602 = { sizeof (U3CU3Ec_t6B8229F9C3E925210F1B9248E9FAE279D6350DA5), -1, sizeof(U3CU3Ec_t6B8229F9C3E925210F1B9248E9FAE279D6350DA5_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7602[7] = 
{
	U3CU3Ec_t6B8229F9C3E925210F1B9248E9FAE279D6350DA5_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t6B8229F9C3E925210F1B9248E9FAE279D6350DA5_StaticFields::get_offset_of_U3CU3E9__6_0_1(),
	U3CU3Ec_t6B8229F9C3E925210F1B9248E9FAE279D6350DA5_StaticFields::get_offset_of_U3CU3E9__7_0_2(),
	U3CU3Ec_t6B8229F9C3E925210F1B9248E9FAE279D6350DA5_StaticFields::get_offset_of_U3CU3E9__8_0_3(),
	U3CU3Ec_t6B8229F9C3E925210F1B9248E9FAE279D6350DA5_StaticFields::get_offset_of_U3CU3E9__10_1_4(),
	U3CU3Ec_t6B8229F9C3E925210F1B9248E9FAE279D6350DA5_StaticFields::get_offset_of_U3CU3E9__12_0_5(),
	U3CU3Ec_t6B8229F9C3E925210F1B9248E9FAE279D6350DA5_StaticFields::get_offset_of_U3CU3E9__12_1_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7603 = { sizeof (U3CGetPropertyInjectablesU3Ed__7_tD24015F2106C6C3A64F5DB6861681AFFFD586FAB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7603[6] = 
{
	U3CGetPropertyInjectablesU3Ed__7_tD24015F2106C6C3A64F5DB6861681AFFFD586FAB::get_offset_of_U3CU3E1__state_0(),
	U3CGetPropertyInjectablesU3Ed__7_tD24015F2106C6C3A64F5DB6861681AFFFD586FAB::get_offset_of_U3CU3E2__current_1(),
	U3CGetPropertyInjectablesU3Ed__7_tD24015F2106C6C3A64F5DB6861681AFFFD586FAB::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CGetPropertyInjectablesU3Ed__7_tD24015F2106C6C3A64F5DB6861681AFFFD586FAB::get_offset_of_type_3(),
	U3CGetPropertyInjectablesU3Ed__7_tD24015F2106C6C3A64F5DB6861681AFFFD586FAB::get_offset_of_U3CU3E3__type_4(),
	U3CGetPropertyInjectablesU3Ed__7_tD24015F2106C6C3A64F5DB6861681AFFFD586FAB::get_offset_of_U3CU3E7__wrap1_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7604 = { sizeof (U3CGetFieldInjectablesU3Ed__8_t83A647F0B621C28753334136E5E1FB06B179929B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7604[6] = 
{
	U3CGetFieldInjectablesU3Ed__8_t83A647F0B621C28753334136E5E1FB06B179929B::get_offset_of_U3CU3E1__state_0(),
	U3CGetFieldInjectablesU3Ed__8_t83A647F0B621C28753334136E5E1FB06B179929B::get_offset_of_U3CU3E2__current_1(),
	U3CGetFieldInjectablesU3Ed__8_t83A647F0B621C28753334136E5E1FB06B179929B::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CGetFieldInjectablesU3Ed__8_t83A647F0B621C28753334136E5E1FB06B179929B::get_offset_of_type_3(),
	U3CGetFieldInjectablesU3Ed__8_t83A647F0B621C28753334136E5E1FB06B179929B::get_offset_of_U3CU3E3__type_4(),
	U3CGetFieldInjectablesU3Ed__8_t83A647F0B621C28753334136E5E1FB06B179929B::get_offset_of_U3CU3E7__wrap1_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7605 = { sizeof (U3CU3Ec__DisplayClass10_0_t6BD5346515FBB2211D02553E1640EFBC794B9921), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7605[2] = 
{
	U3CU3Ec__DisplayClass10_0_t6BD5346515FBB2211D02553E1640EFBC794B9921::get_offset_of_propertyName_0(),
	U3CU3Ec__DisplayClass10_0_t6BD5346515FBB2211D02553E1640EFBC794B9921::get_offset_of_writeableFields_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7606 = { sizeof (U3CU3Ec__DisplayClass10_1_tF40BC40F6F52827485B66F6FD029EADADFACBD08), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7606[2] = 
{
	U3CU3Ec__DisplayClass10_1_tF40BC40F6F52827485B66F6FD029EADADFACBD08::get_offset_of_injectable_0(),
	U3CU3Ec__DisplayClass10_1_tF40BC40F6F52827485B66F6FD029EADADFACBD08::get_offset_of_value_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7607 = { sizeof (U3CU3Ec__DisplayClass11_0_t1F43124971BB2CF876F2FBA075366F26838B7AED), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7607[1] = 
{
	U3CU3Ec__DisplayClass11_0_t1F43124971BB2CF876F2FBA075366F26838B7AED::get_offset_of_fieldInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7608 = { sizeof (U3CU3Ec__DisplayClass11_1_t4B2232A5D21750B3EE736D56D4B58B689651FA06), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7608[1] = 
{
	U3CU3Ec__DisplayClass11_1_t4B2232A5D21750B3EE736D56D4B58B689651FA06::get_offset_of_propInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7609 = { sizeof (ValidationUtil_t3B87626A5619AB0232CEF653A4ADDB3ED1ADFFF4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7610 = { sizeof (U3CU3Ec_tA7BA396619C37FF5968906EBAF37F56A27235BE9), -1, sizeof(U3CU3Ec_tA7BA396619C37FF5968906EBAF37F56A27235BE9_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7610[2] = 
{
	U3CU3Ec_tA7BA396619C37FF5968906EBAF37F56A27235BE9_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tA7BA396619C37FF5968906EBAF37F56A27235BE9_StaticFields::get_offset_of_U3CU3E9__0_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7611 = { sizeof (ZenAutoInjecter_t9688C54CDC7F85464B4A5AA87184871DC3C1B236), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7611[2] = 
{
	ZenAutoInjecter_t9688C54CDC7F85464B4A5AA87184871DC3C1B236::get_offset_of__containerSource_4(),
	ZenAutoInjecter_t9688C54CDC7F85464B4A5AA87184871DC3C1B236::get_offset_of__hasStarted_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7612 = { sizeof (ContainerSources_tDE70B7FAC33EE303EC700E587DD993EE9D3DC41D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7612[3] = 
{
	ContainerSources_tDE70B7FAC33EE303EC700E587DD993EE9D3DC41D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7613 = { sizeof (ZenjectException_t7986195562CCDF5A2E4A08728A3969178A0B818F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7614 = { sizeof (LoadSceneRelationship_t070553E2B76C8CC43C1C9D6FB4C28579ADFF0A58)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7614[4] = 
{
	LoadSceneRelationship_t070553E2B76C8CC43C1C9D6FB4C28579ADFF0A58::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7615 = { sizeof (ZenjectSceneLoader_tF1587A647EB3ED2821ED3298C3C9A3F82E73CD54), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7615[2] = 
{
	ZenjectSceneLoader_tF1587A647EB3ED2821ED3298C3C9A3F82E73CD54::get_offset_of__projectKernel_0(),
	ZenjectSceneLoader_tF1587A647EB3ED2821ED3298C3C9A3F82E73CD54::get_offset_of__sceneContainer_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7616 = { sizeof (PostInjectableInfo_t30E18115C14A086F02203F1FF8952C21A74F0CF1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7616[2] = 
{
	PostInjectableInfo_t30E18115C14A086F02203F1FF8952C21A74F0CF1::get_offset_of__methodInfo_0(),
	PostInjectableInfo_t30E18115C14A086F02203F1FF8952C21A74F0CF1::get_offset_of__injectableInfo_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7617 = { sizeof (ZenjectTypeInfo_t2866A662C0FDA5B357BCC135021933EE8914535E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7617[6] = 
{
	ZenjectTypeInfo_t2866A662C0FDA5B357BCC135021933EE8914535E::get_offset_of__postInjectMethods_0(),
	ZenjectTypeInfo_t2866A662C0FDA5B357BCC135021933EE8914535E::get_offset_of__constructorInjectables_1(),
	ZenjectTypeInfo_t2866A662C0FDA5B357BCC135021933EE8914535E::get_offset_of__fieldInjectables_2(),
	ZenjectTypeInfo_t2866A662C0FDA5B357BCC135021933EE8914535E::get_offset_of__propertyInjectables_3(),
	ZenjectTypeInfo_t2866A662C0FDA5B357BCC135021933EE8914535E::get_offset_of__injectConstructor_4(),
	ZenjectTypeInfo_t2866A662C0FDA5B357BCC135021933EE8914535E::get_offset_of__typeAnalyzed_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7618 = { sizeof (U3CU3Ec_t6A7B1D29E32D4B0F2FD866225301192DA5428736), -1, sizeof(U3CU3Ec_t6A7B1D29E32D4B0F2FD866225301192DA5428736_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7618[2] = 
{
	U3CU3Ec_t6A7B1D29E32D4B0F2FD866225301192DA5428736_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t6A7B1D29E32D4B0F2FD866225301192DA5428736_StaticFields::get_offset_of_U3CU3E9__12_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7619 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7620 = { sizeof (ValidationMarker_t48A3FF2F869BB352F9B6AFD415A0517795A16782), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7620[2] = 
{
	ValidationMarker_t48A3FF2F869BB352F9B6AFD415A0517795A16782::get_offset_of_U3CInstantiateFailedU3Ek__BackingField_0(),
	ValidationMarker_t48A3FF2F869BB352F9B6AFD415A0517795A16782::get_offset_of_U3CMarkedTypeU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7621 = { sizeof (ZenUtilInternal_t9F6EE1BF65F53C2F9521E247F9627CA2721C0D2F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7622 = { sizeof (U3CU3Ec_t0BEBDED76D743DFB6185223F22CC6D460B79DF63), -1, sizeof(U3CU3Ec_t0BEBDED76D743DFB6185223F22CC6D460B79DF63_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7622[3] = 
{
	U3CU3Ec_t0BEBDED76D743DFB6185223F22CC6D460B79DF63_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t0BEBDED76D743DFB6185223F22CC6D460B79DF63_StaticFields::get_offset_of_U3CU3E9__3_0_1(),
	U3CU3Ec_t0BEBDED76D743DFB6185223F22CC6D460B79DF63_StaticFields::get_offset_of_U3CU3E9__7_0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7623 = { sizeof (U3CGetAllSceneContextsU3Ed__3_t51F57C904E207984C55541E310AC69674F084373), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7623[4] = 
{
	U3CGetAllSceneContextsU3Ed__3_t51F57C904E207984C55541E310AC69674F084373::get_offset_of_U3CU3E1__state_0(),
	U3CGetAllSceneContextsU3Ed__3_t51F57C904E207984C55541E310AC69674F084373::get_offset_of_U3CU3E2__current_1(),
	U3CGetAllSceneContextsU3Ed__3_t51F57C904E207984C55541E310AC69674F084373::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CGetAllSceneContextsU3Ed__3_t51F57C904E207984C55541E310AC69674F084373::get_offset_of_U3CU3E7__wrap1_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7624 = { sizeof (U3CU3Ec__DisplayClass7_0_t1F8DD433041F1C5A6D5805320617520438071D1A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7624[1] = 
{
	U3CU3Ec__DisplayClass7_0_t1F8DD433041F1C5A6D5805320617520438071D1A::get_offset_of_scene_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7625 = { sizeof (Enemy_tC40CA91F878B23445D874D13243C2824315DE3D2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7625[4] = 
{
	Enemy_tC40CA91F878B23445D874D13243C2824315DE3D2::get_offset_of__renderer_0(),
	Enemy_tC40CA91F878B23445D874D13243C2824315DE3D2::get_offset_of__collider_1(),
	Enemy_tC40CA91F878B23445D874D13243C2824315DE3D2::get_offset_of__rigidBody_2(),
	Enemy_tC40CA91F878B23445D874D13243C2824315DE3D2::get_offset_of_U3CDesiredLookDirU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7626 = { sizeof (EnemyCommonSettings_tF1C89E066A85E57A479925E334EDE12BEAAE767B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7626[1] = 
{
	EnemyCommonSettings_tF1C89E066A85E57A479925E334EDE12BEAAE767B::get_offset_of_AttackDistance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7627 = { sizeof (EnemyDeathHandler_t87BF96335A31C0712F7B9E80B22587838EE9B4C5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7627[7] = 
{
	EnemyDeathHandler_t87BF96335A31C0712F7B9E80B22587838EE9B4C5::get_offset_of__enemyKilledSignal_0(),
	EnemyDeathHandler_t87BF96335A31C0712F7B9E80B22587838EE9B4C5::get_offset_of__selfFactory_1(),
	EnemyDeathHandler_t87BF96335A31C0712F7B9E80B22587838EE9B4C5::get_offset_of__settings_2(),
	EnemyDeathHandler_t87BF96335A31C0712F7B9E80B22587838EE9B4C5::get_offset_of__explosionPool_3(),
	EnemyDeathHandler_t87BF96335A31C0712F7B9E80B22587838EE9B4C5::get_offset_of__audioPlayer_4(),
	EnemyDeathHandler_t87BF96335A31C0712F7B9E80B22587838EE9B4C5::get_offset_of__enemy_5(),
	EnemyDeathHandler_t87BF96335A31C0712F7B9E80B22587838EE9B4C5::get_offset_of__facade_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7628 = { sizeof (Settings_tCDA69B4C2B2FC2E7E5DC396996019836DE3ED1E2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7628[2] = 
{
	Settings_tCDA69B4C2B2FC2E7E5DC396996019836DE3ED1E2::get_offset_of_DeathSound_0(),
	Settings_tCDA69B4C2B2FC2E7E5DC396996019836DE3ED1E2::get_offset_of_DeathSoundVolume_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7629 = { sizeof (EnemyFacade_t1D5DD8AEDE16BD5F9C2C32C4C0AB954CE1811FC1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7629[4] = 
{
	EnemyFacade_t1D5DD8AEDE16BD5F9C2C32C4C0AB954CE1811FC1::get_offset_of__enemy_4(),
	EnemyFacade_t1D5DD8AEDE16BD5F9C2C32C4C0AB954CE1811FC1::get_offset_of__tunables_5(),
	EnemyFacade_t1D5DD8AEDE16BD5F9C2C32C4C0AB954CE1811FC1::get_offset_of__deathHandler_6(),
	EnemyFacade_t1D5DD8AEDE16BD5F9C2C32C4C0AB954CE1811FC1::get_offset_of__stateManager_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7630 = { sizeof (Pool_t735C24F8E4685CE9F8814136D7E4B3548882F387), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7631 = { sizeof (EnemyRotationHandler_t20FB73A75100F454B07CC9BB0C3E6255706F896B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7631[2] = 
{
	EnemyRotationHandler_t20FB73A75100F454B07CC9BB0C3E6255706F896B::get_offset_of__settings_0(),
	EnemyRotationHandler_t20FB73A75100F454B07CC9BB0C3E6255706F896B::get_offset_of__enemy_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7632 = { sizeof (Settings_t3480554AD22B7730290AB95863B9EDC48610BCE9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7632[1] = 
{
	Settings_t3480554AD22B7730290AB95863B9EDC48610BCE9::get_offset_of_TurnSpeed_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7633 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7634 = { sizeof (EnemyStates_t472981EB20C199308DC21B66E41DEC5D8719EF54)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7634[5] = 
{
	EnemyStates_t472981EB20C199308DC21B66E41DEC5D8719EF54::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7635 = { sizeof (EnemyStateManager_tD3E2662DA02D660CB7356CE3563F928B0E220C9F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7635[3] = 
{
	EnemyStateManager_tD3E2662DA02D660CB7356CE3563F928B0E220C9F::get_offset_of__currentStateHandler_0(),
	EnemyStateManager_tD3E2662DA02D660CB7356CE3563F928B0E220C9F::get_offset_of__currentState_1(),
	EnemyStateManager_tD3E2662DA02D660CB7356CE3563F928B0E220C9F::get_offset_of__states_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7636 = { sizeof (EnemyTunables_tD9997725AFF614521DD7088FFF23C1D8DD06DE18), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7636[2] = 
{
	EnemyTunables_tD9997725AFF614521DD7088FFF23C1D8DD06DE18::get_offset_of_Accuracy_0(),
	EnemyTunables_tD9997725AFF614521DD7088FFF23C1D8DD06DE18::get_offset_of_Speed_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7637 = { sizeof (PlayerDiedSignal_tD47ED6EA743FEE4E5D7B3289450C3EA59E018C7F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7638 = { sizeof (EnemyKilledSignal_t5291603135B3B8F73E6ED1317431EE5091D78DC8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7639 = { sizeof (EnemyStateAttack_tC0E53565C8E9746FBE18E7521390DB4D26D05A9A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7639[11] = 
{
	EnemyStateAttack_tC0E53565C8E9746FBE18E7521390DB4D26D05A9A::get_offset_of__commonSettings_0(),
	EnemyStateAttack_tC0E53565C8E9746FBE18E7521390DB4D26D05A9A::get_offset_of__audioPlayer_1(),
	EnemyStateAttack_tC0E53565C8E9746FBE18E7521390DB4D26D05A9A::get_offset_of__tunables_2(),
	EnemyStateAttack_tC0E53565C8E9746FBE18E7521390DB4D26D05A9A::get_offset_of__stateManager_3(),
	EnemyStateAttack_tC0E53565C8E9746FBE18E7521390DB4D26D05A9A::get_offset_of__player_4(),
	EnemyStateAttack_tC0E53565C8E9746FBE18E7521390DB4D26D05A9A::get_offset_of__settings_5(),
	EnemyStateAttack_tC0E53565C8E9746FBE18E7521390DB4D26D05A9A::get_offset_of__enemy_6(),
	EnemyStateAttack_tC0E53565C8E9746FBE18E7521390DB4D26D05A9A::get_offset_of__bulletPool_7(),
	EnemyStateAttack_tC0E53565C8E9746FBE18E7521390DB4D26D05A9A::get_offset_of__lastShootTime_8(),
	EnemyStateAttack_tC0E53565C8E9746FBE18E7521390DB4D26D05A9A::get_offset_of__strafeRight_9(),
	EnemyStateAttack_tC0E53565C8E9746FBE18E7521390DB4D26D05A9A::get_offset_of__lastStrafeChangeTime_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7640 = { sizeof (Settings_t3ABF5D501B5AFEE2903D5F846E8EC24317DB3528), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7640[10] = 
{
	Settings_t3ABF5D501B5AFEE2903D5F846E8EC24317DB3528::get_offset_of_ShootSound_0(),
	Settings_t3ABF5D501B5AFEE2903D5F846E8EC24317DB3528::get_offset_of_ShootSoundVolume_1(),
	Settings_t3ABF5D501B5AFEE2903D5F846E8EC24317DB3528::get_offset_of_BulletLifetime_2(),
	Settings_t3ABF5D501B5AFEE2903D5F846E8EC24317DB3528::get_offset_of_BulletSpeed_3(),
	Settings_t3ABF5D501B5AFEE2903D5F846E8EC24317DB3528::get_offset_of_BulletOffsetDistance_4(),
	Settings_t3ABF5D501B5AFEE2903D5F846E8EC24317DB3528::get_offset_of_ShootInterval_5(),
	Settings_t3ABF5D501B5AFEE2903D5F846E8EC24317DB3528::get_offset_of_ErrorRangeTheta_6(),
	Settings_t3ABF5D501B5AFEE2903D5F846E8EC24317DB3528::get_offset_of_AttackRangeBuffer_7(),
	Settings_t3ABF5D501B5AFEE2903D5F846E8EC24317DB3528::get_offset_of_StrafeMultiplier_8(),
	Settings_t3ABF5D501B5AFEE2903D5F846E8EC24317DB3528::get_offset_of_StrafeChangeInterval_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7641 = { sizeof (EnemyStateFollow_t4B768EEDF04FA03A6B009A38DC3069021C5ECFF2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7641[8] = 
{
	EnemyStateFollow_t4B768EEDF04FA03A6B009A38DC3069021C5ECFF2::get_offset_of__commonSettings_0(),
	EnemyStateFollow_t4B768EEDF04FA03A6B009A38DC3069021C5ECFF2::get_offset_of__settings_1(),
	EnemyStateFollow_t4B768EEDF04FA03A6B009A38DC3069021C5ECFF2::get_offset_of__tunables_2(),
	EnemyStateFollow_t4B768EEDF04FA03A6B009A38DC3069021C5ECFF2::get_offset_of__stateManager_3(),
	EnemyStateFollow_t4B768EEDF04FA03A6B009A38DC3069021C5ECFF2::get_offset_of__enemy_4(),
	EnemyStateFollow_t4B768EEDF04FA03A6B009A38DC3069021C5ECFF2::get_offset_of__player_5(),
	EnemyStateFollow_t4B768EEDF04FA03A6B009A38DC3069021C5ECFF2::get_offset_of__strafeRight_6(),
	EnemyStateFollow_t4B768EEDF04FA03A6B009A38DC3069021C5ECFF2::get_offset_of__lastStrafeChangeTime_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7642 = { sizeof (Settings_tC265A1DB51490C02FF21B3A9733F230E53104BAC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7642[3] = 
{
	Settings_tC265A1DB51490C02FF21B3A9733F230E53104BAC::get_offset_of_StrafeMultiplier_0(),
	Settings_tC265A1DB51490C02FF21B3A9733F230E53104BAC::get_offset_of_StrafeChangeInterval_1(),
	Settings_tC265A1DB51490C02FF21B3A9733F230E53104BAC::get_offset_of_TeleportNewDistance_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7643 = { sizeof (EnemyStateIdle_t384A3EE550CBFEB78AEBA1F4C636153F53E4E07D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7643[5] = 
{
	EnemyStateIdle_t384A3EE550CBFEB78AEBA1F4C636153F53E4E07D::get_offset_of__settings_0(),
	EnemyStateIdle_t384A3EE550CBFEB78AEBA1F4C636153F53E4E07D::get_offset_of__enemy_1(),
	EnemyStateIdle_t384A3EE550CBFEB78AEBA1F4C636153F53E4E07D::get_offset_of__startPos_2(),
	EnemyStateIdle_t384A3EE550CBFEB78AEBA1F4C636153F53E4E07D::get_offset_of__theta_3(),
	EnemyStateIdle_t384A3EE550CBFEB78AEBA1F4C636153F53E4E07D::get_offset_of__startLookDir_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7644 = { sizeof (Settings_t6A845C637F62D4448CE7764CFBBFBDDDA7951042), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7644[2] = 
{
	Settings_t6A845C637F62D4448CE7764CFBBFBDDDA7951042::get_offset_of_Amplitude_0(),
	Settings_t6A845C637F62D4448CE7764CFBBFBDDDA7951042::get_offset_of_Frequency_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7645 = { sizeof (EnemyInstaller_t2A05C806CDF4D1E60B9E3BDB910AD512928695C7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7645[1] = 
{
	EnemyInstaller_t2A05C806CDF4D1E60B9E3BDB910AD512928695C7::get_offset_of__settings_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7646 = { sizeof (Settings_tD2755B3F26D35678AF75CBB0C5BF9C282206FF91), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7646[4] = 
{
	Settings_tD2755B3F26D35678AF75CBB0C5BF9C282206FF91::get_offset_of_RootObject_0(),
	Settings_tD2755B3F26D35678AF75CBB0C5BF9C282206FF91::get_offset_of_Rigidbody_1(),
	Settings_tD2755B3F26D35678AF75CBB0C5BF9C282206FF91::get_offset_of_Collider_2(),
	Settings_tD2755B3F26D35678AF75CBB0C5BF9C282206FF91::get_offset_of_Renderer_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7647 = { sizeof (GameInstaller_t9A03FD076BF550D8511C194043C234A62EB2C2BC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7647[1] = 
{
	GameInstaller_t9A03FD076BF550D8511C194043C234A62EB2C2BC::get_offset_of__settings_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7648 = { sizeof (Settings_tDDDA6539A297D2F0F5790F2BE702EABB8BC23312), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7648[3] = 
{
	Settings_tDDDA6539A297D2F0F5790F2BE702EABB8BC23312::get_offset_of_EnemyFacadePrefab_0(),
	Settings_tDDDA6539A297D2F0F5790F2BE702EABB8BC23312::get_offset_of_BulletPrefab_1(),
	Settings_tDDDA6539A297D2F0F5790F2BE702EABB8BC23312::get_offset_of_ExplosionPrefab_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7649 = { sizeof (GameSettingsInstaller_t81335697D090EA540B42B2092934CF7177DB634E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7649[5] = 
{
	GameSettingsInstaller_t81335697D090EA540B42B2092934CF7177DB634E::get_offset_of_EnemySpawner_5(),
	GameSettingsInstaller_t81335697D090EA540B42B2092934CF7177DB634E::get_offset_of_GameRestartHandler_6(),
	GameSettingsInstaller_t81335697D090EA540B42B2092934CF7177DB634E::get_offset_of_GameInstaller_7(),
	GameSettingsInstaller_t81335697D090EA540B42B2092934CF7177DB634E::get_offset_of_Player_8(),
	GameSettingsInstaller_t81335697D090EA540B42B2092934CF7177DB634E::get_offset_of_Enemy_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7650 = { sizeof (PlayerSettings_t50B498D03A02BD7CD7E2367EFDAD0428FBBC1058), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7650[4] = 
{
	PlayerSettings_t50B498D03A02BD7CD7E2367EFDAD0428FBBC1058::get_offset_of_PlayerMoveHandler_0(),
	PlayerSettings_t50B498D03A02BD7CD7E2367EFDAD0428FBBC1058::get_offset_of_PlayerShootHandler_1(),
	PlayerSettings_t50B498D03A02BD7CD7E2367EFDAD0428FBBC1058::get_offset_of_PlayerCollisionHandler_2(),
	PlayerSettings_t50B498D03A02BD7CD7E2367EFDAD0428FBBC1058::get_offset_of_PlayerHealthWatcher_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7651 = { sizeof (EnemySettings_t87F1C13D725FC80A97C27012C240D7E9150A5785), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7651[7] = 
{
	EnemySettings_t87F1C13D725FC80A97C27012C240D7E9150A5785::get_offset_of_DefaultSettings_0(),
	EnemySettings_t87F1C13D725FC80A97C27012C240D7E9150A5785::get_offset_of_EnemyStateIdle_1(),
	EnemySettings_t87F1C13D725FC80A97C27012C240D7E9150A5785::get_offset_of_EnemyRotationHandler_2(),
	EnemySettings_t87F1C13D725FC80A97C27012C240D7E9150A5785::get_offset_of_EnemyStateFollow_3(),
	EnemySettings_t87F1C13D725FC80A97C27012C240D7E9150A5785::get_offset_of_EnemyStateAttack_4(),
	EnemySettings_t87F1C13D725FC80A97C27012C240D7E9150A5785::get_offset_of_EnemyHealthWatcher_5(),
	EnemySettings_t87F1C13D725FC80A97C27012C240D7E9150A5785::get_offset_of_EnemyCommonSettings_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7652 = { sizeof (GameSignalsInstaller_tF11CCBEDED82401BF3C0D10AEC62BC6B9C444549), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7653 = { sizeof (PlayerInstaller_t0AE84D945F4D18F81323F24C912C0437C4939607), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7653[1] = 
{
	PlayerInstaller_t0AE84D945F4D18F81323F24C912C0437C4939607::get_offset_of__settings_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7654 = { sizeof (Settings_tA79246F22F290206CED693068D5770E633DD5CEE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7654[2] = 
{
	Settings_tA79246F22F290206CED693068D5770E633DD5CEE::get_offset_of_Rigidbody_0(),
	Settings_tA79246F22F290206CED693068D5770E633DD5CEE::get_offset_of_MeshRenderer_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7655 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7656 = { sizeof (AudioPlayer_tD4C21B023CD7243688AE5FA1540F5C8ED94C49AA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7656[1] = 
{
	AudioPlayer_tD4C21B023CD7243688AE5FA1540F5C8ED94C49AA::get_offset_of__camera_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7657 = { sizeof (BulletTypes_t5EDC76B12065FDE17E4384C7F1B92F11B06E05D4)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7657[3] = 
{
	BulletTypes_t5EDC76B12065FDE17E4384C7F1B92F11B06E05D4::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7658 = { sizeof (Bullet_tF89BF91D9A75D2C600AC3DCDDC5AF8FCC6F1C6A9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7658[8] = 
{
	Bullet_tF89BF91D9A75D2C600AC3DCDDC5AF8FCC6F1C6A9::get_offset_of__startTime_4(),
	Bullet_tF89BF91D9A75D2C600AC3DCDDC5AF8FCC6F1C6A9::get_offset_of__type_5(),
	Bullet_tF89BF91D9A75D2C600AC3DCDDC5AF8FCC6F1C6A9::get_offset_of__speed_6(),
	Bullet_tF89BF91D9A75D2C600AC3DCDDC5AF8FCC6F1C6A9::get_offset_of__lifeTime_7(),
	Bullet_tF89BF91D9A75D2C600AC3DCDDC5AF8FCC6F1C6A9::get_offset_of__renderer_8(),
	Bullet_tF89BF91D9A75D2C600AC3DCDDC5AF8FCC6F1C6A9::get_offset_of__playerMaterial_9(),
	Bullet_tF89BF91D9A75D2C600AC3DCDDC5AF8FCC6F1C6A9::get_offset_of__enemyMaterial_10(),
	Bullet_tF89BF91D9A75D2C600AC3DCDDC5AF8FCC6F1C6A9::get_offset_of__bulletPool_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7659 = { sizeof (Pool_t28D3C271D1214C1C919D02B9C38A6A5698F231F1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7660 = { sizeof (ControlsDisplay_t40059FC98A1A0C996E943225FFB10945BB5B75FC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7660[4] = 
{
	ControlsDisplay_t40059FC98A1A0C996E943225FFB10945BB5B75FC::get_offset_of__leftPadding_4(),
	ControlsDisplay_t40059FC98A1A0C996E943225FFB10945BB5B75FC::get_offset_of__topPadding_5(),
	ControlsDisplay_t40059FC98A1A0C996E943225FFB10945BB5B75FC::get_offset_of__width_6(),
	ControlsDisplay_t40059FC98A1A0C996E943225FFB10945BB5B75FC::get_offset_of__height_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7661 = { sizeof (EnemySpawner_t7844A5EBF2D0675CE13484749052C3F440652F6A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7661[7] = 
{
	EnemySpawner_t7844A5EBF2D0675CE13484749052C3F440652F6A::get_offset_of__levelBoundary_0(),
	EnemySpawner_t7844A5EBF2D0675CE13484749052C3F440652F6A::get_offset_of__enemyFactory_1(),
	EnemySpawner_t7844A5EBF2D0675CE13484749052C3F440652F6A::get_offset_of__settings_2(),
	EnemySpawner_t7844A5EBF2D0675CE13484749052C3F440652F6A::get_offset_of__enemyKilledSignal_3(),
	EnemySpawner_t7844A5EBF2D0675CE13484749052C3F440652F6A::get_offset_of__desiredNumEnemies_4(),
	EnemySpawner_t7844A5EBF2D0675CE13484749052C3F440652F6A::get_offset_of__enemyCount_5(),
	EnemySpawner_t7844A5EBF2D0675CE13484749052C3F440652F6A::get_offset_of__lastSpawnTime_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7662 = { sizeof (Settings_tD4EAF29A0539BDE0CFDC096ED188196E8DD744C2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7662[7] = 
{
	Settings_tD4EAF29A0539BDE0CFDC096ED188196E8DD744C2::get_offset_of_SpeedMin_0(),
	Settings_tD4EAF29A0539BDE0CFDC096ED188196E8DD744C2::get_offset_of_SpeedMax_1(),
	Settings_tD4EAF29A0539BDE0CFDC096ED188196E8DD744C2::get_offset_of_AccuracyMin_2(),
	Settings_tD4EAF29A0539BDE0CFDC096ED188196E8DD744C2::get_offset_of_AccuracyMax_3(),
	Settings_tD4EAF29A0539BDE0CFDC096ED188196E8DD744C2::get_offset_of_NumEnemiesIncreaseRate_4(),
	Settings_tD4EAF29A0539BDE0CFDC096ED188196E8DD744C2::get_offset_of_NumEnemiesStartAmount_5(),
	Settings_tD4EAF29A0539BDE0CFDC096ED188196E8DD744C2::get_offset_of_MinDelayBetweenSpawns_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7663 = { sizeof (Explosion_t6C30B8B388089AA2C76F253AAFDE0F7CC0E05B86), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7663[6] = 
{
	Explosion_t6C30B8B388089AA2C76F253AAFDE0F7CC0E05B86::get_offset_of__lifeTime_4(),
	Explosion_t6C30B8B388089AA2C76F253AAFDE0F7CC0E05B86::get_offset_of__particleSystem_5(),
	Explosion_t6C30B8B388089AA2C76F253AAFDE0F7CC0E05B86::get_offset_of__sound_6(),
	Explosion_t6C30B8B388089AA2C76F253AAFDE0F7CC0E05B86::get_offset_of__soundVolume_7(),
	Explosion_t6C30B8B388089AA2C76F253AAFDE0F7CC0E05B86::get_offset_of__startTime_8(),
	Explosion_t6C30B8B388089AA2C76F253AAFDE0F7CC0E05B86::get_offset_of__pool_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7664 = { sizeof (Pool_tF6D222503E0FF0BBA73DDF77321A0C8B6CA1D7C7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7665 = { sizeof (GameRestartHandler_t6A892235F071977C1E69DE19C8B5F983D8E7EAEE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7665[4] = 
{
	GameRestartHandler_t6A892235F071977C1E69DE19C8B5F983D8E7EAEE::get_offset_of__settings_0(),
	GameRestartHandler_t6A892235F071977C1E69DE19C8B5F983D8E7EAEE::get_offset_of__playerDiedSignal_1(),
	GameRestartHandler_t6A892235F071977C1E69DE19C8B5F983D8E7EAEE::get_offset_of__isDelaying_2(),
	GameRestartHandler_t6A892235F071977C1E69DE19C8B5F983D8E7EAEE::get_offset_of__delayStartTime_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7666 = { sizeof (Settings_tDA43D3BAE8BE0B223AEFEE4FC2782ACB7169D1C2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7666[1] = 
{
	Settings_tDA43D3BAE8BE0B223AEFEE4FC2782ACB7169D1C2::get_offset_of_RestartDelay_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7667 = { sizeof (LevelBoundary_t8E706FF3A27DD22A30FE8E79EF745B6B7D78577B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7667[1] = 
{
	LevelBoundary_t8E706FF3A27DD22A30FE8E79EF745B6B7D78577B::get_offset_of__camera_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7668 = { sizeof (PlayerDamageHandler_tFF3D703F066B3204ADD106FCC8B95D7EF61B4E33), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7668[3] = 
{
	PlayerDamageHandler_tFF3D703F066B3204ADD106FCC8B95D7EF61B4E33::get_offset_of__audioPlayer_0(),
	PlayerDamageHandler_tFF3D703F066B3204ADD106FCC8B95D7EF61B4E33::get_offset_of__settings_1(),
	PlayerDamageHandler_tFF3D703F066B3204ADD106FCC8B95D7EF61B4E33::get_offset_of__player_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7669 = { sizeof (Settings_t48778C45589CCBEC26A7E67F855609F50926CB44), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7669[4] = 
{
	Settings_t48778C45589CCBEC26A7E67F855609F50926CB44::get_offset_of_HealthLoss_0(),
	Settings_t48778C45589CCBEC26A7E67F855609F50926CB44::get_offset_of_HitForce_1(),
	Settings_t48778C45589CCBEC26A7E67F855609F50926CB44::get_offset_of_HitSound_2(),
	Settings_t48778C45589CCBEC26A7E67F855609F50926CB44::get_offset_of_HitSoundVolume_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7670 = { sizeof (PlayerDirectionHandler_t52F4A906A8020B45FCAB026F77360D1DC2A94987), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7670[2] = 
{
	PlayerDirectionHandler_t52F4A906A8020B45FCAB026F77360D1DC2A94987::get_offset_of__player_0(),
	PlayerDirectionHandler_t52F4A906A8020B45FCAB026F77360D1DC2A94987::get_offset_of__mainCamera_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7671 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7672 = { sizeof (PlayerFacade_t3868059ED0F5190576FE5901D6D602737C505B91), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7672[2] = 
{
	PlayerFacade_t3868059ED0F5190576FE5901D6D602737C505B91::get_offset_of__model_4(),
	PlayerFacade_t3868059ED0F5190576FE5901D6D602737C505B91::get_offset_of__hitHandler_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7673 = { sizeof (PlayerGui_t4C3FE8F85E138707CA0C84CD109F303379432D96), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7673[13] = 
{
	PlayerGui_t4C3FE8F85E138707CA0C84CD109F303379432D96::get_offset_of__leftPadding_4(),
	PlayerGui_t4C3FE8F85E138707CA0C84CD109F303379432D96::get_offset_of__bottomPadding_5(),
	PlayerGui_t4C3FE8F85E138707CA0C84CD109F303379432D96::get_offset_of__labelWidth_6(),
	PlayerGui_t4C3FE8F85E138707CA0C84CD109F303379432D96::get_offset_of__labelHeight_7(),
	PlayerGui_t4C3FE8F85E138707CA0C84CD109F303379432D96::get_offset_of__textureWidth_8(),
	PlayerGui_t4C3FE8F85E138707CA0C84CD109F303379432D96::get_offset_of__textureHeight_9(),
	PlayerGui_t4C3FE8F85E138707CA0C84CD109F303379432D96::get_offset_of__killCountOffset_10(),
	PlayerGui_t4C3FE8F85E138707CA0C84CD109F303379432D96::get_offset_of__foregroundColor_11(),
	PlayerGui_t4C3FE8F85E138707CA0C84CD109F303379432D96::get_offset_of__backgroundColor_12(),
	PlayerGui_t4C3FE8F85E138707CA0C84CD109F303379432D96::get_offset_of__player_13(),
	PlayerGui_t4C3FE8F85E138707CA0C84CD109F303379432D96::get_offset_of__textureForeground_14(),
	PlayerGui_t4C3FE8F85E138707CA0C84CD109F303379432D96::get_offset_of__textureBackground_15(),
	PlayerGui_t4C3FE8F85E138707CA0C84CD109F303379432D96::get_offset_of__killCount_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7674 = { sizeof (PlayerHealthWatcher_t6A90E0F2AF3D5C2C35AEB220E5BE9B375BCE4D96), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7674[5] = 
{
	PlayerHealthWatcher_t6A90E0F2AF3D5C2C35AEB220E5BE9B375BCE4D96::get_offset_of__playerDiedSignal_0(),
	PlayerHealthWatcher_t6A90E0F2AF3D5C2C35AEB220E5BE9B375BCE4D96::get_offset_of__audioPlayer_1(),
	PlayerHealthWatcher_t6A90E0F2AF3D5C2C35AEB220E5BE9B375BCE4D96::get_offset_of__settings_2(),
	PlayerHealthWatcher_t6A90E0F2AF3D5C2C35AEB220E5BE9B375BCE4D96::get_offset_of__explosionPool_3(),
	PlayerHealthWatcher_t6A90E0F2AF3D5C2C35AEB220E5BE9B375BCE4D96::get_offset_of__player_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7675 = { sizeof (Settings_t83D6CE3845CA378293BC213A44067B48638017A4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7675[2] = 
{
	Settings_t83D6CE3845CA378293BC213A44067B48638017A4::get_offset_of_DeathSound_0(),
	Settings_t83D6CE3845CA378293BC213A44067B48638017A4::get_offset_of_DeathSoundVolume_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7676 = { sizeof (PlayerInputHandler_t39362CD94847EC5A56BAA3E06B877601EFCB8424), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7676[1] = 
{
	PlayerInputHandler_t39362CD94847EC5A56BAA3E06B877601EFCB8424::get_offset_of__inputState_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7677 = { sizeof (PlayerInputState_t4B9BA63DFF6982E3A6FFB060DDE8C17DC0A83CF8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7677[5] = 
{
	PlayerInputState_t4B9BA63DFF6982E3A6FFB060DDE8C17DC0A83CF8::get_offset_of_U3CIsMovingLeftU3Ek__BackingField_0(),
	PlayerInputState_t4B9BA63DFF6982E3A6FFB060DDE8C17DC0A83CF8::get_offset_of_U3CIsMovingRightU3Ek__BackingField_1(),
	PlayerInputState_t4B9BA63DFF6982E3A6FFB060DDE8C17DC0A83CF8::get_offset_of_U3CIsMovingUpU3Ek__BackingField_2(),
	PlayerInputState_t4B9BA63DFF6982E3A6FFB060DDE8C17DC0A83CF8::get_offset_of_U3CIsMovingDownU3Ek__BackingField_3(),
	PlayerInputState_t4B9BA63DFF6982E3A6FFB060DDE8C17DC0A83CF8::get_offset_of_U3CIsFiringU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7678 = { sizeof (Player_t0644AFD1050593EF42B2459D30EA133F19E7A402), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7678[4] = 
{
	Player_t0644AFD1050593EF42B2459D30EA133F19E7A402::get_offset_of__rigidBody_0(),
	Player_t0644AFD1050593EF42B2459D30EA133F19E7A402::get_offset_of__renderer_1(),
	Player_t0644AFD1050593EF42B2459D30EA133F19E7A402::get_offset_of__health_2(),
	Player_t0644AFD1050593EF42B2459D30EA133F19E7A402::get_offset_of_U3CIsDeadU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7679 = { sizeof (PlayerMoveHandler_t55E7B67AABCC9E514AA8C9BF19D61505BFA7CCDD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7679[4] = 
{
	PlayerMoveHandler_t55E7B67AABCC9E514AA8C9BF19D61505BFA7CCDD::get_offset_of__levelBoundary_0(),
	PlayerMoveHandler_t55E7B67AABCC9E514AA8C9BF19D61505BFA7CCDD::get_offset_of__settings_1(),
	PlayerMoveHandler_t55E7B67AABCC9E514AA8C9BF19D61505BFA7CCDD::get_offset_of__player_2(),
	PlayerMoveHandler_t55E7B67AABCC9E514AA8C9BF19D61505BFA7CCDD::get_offset_of__inputState_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7680 = { sizeof (Settings_t5D29F620DD8E2C4750D3670B9D88BD2F6F4C6FC5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7680[4] = 
{
	Settings_t5D29F620DD8E2C4750D3670B9D88BD2F6F4C6FC5::get_offset_of_BoundaryBuffer_0(),
	Settings_t5D29F620DD8E2C4750D3670B9D88BD2F6F4C6FC5::get_offset_of_BoundaryAdjustForce_1(),
	Settings_t5D29F620DD8E2C4750D3670B9D88BD2F6F4C6FC5::get_offset_of_MoveSpeed_2(),
	Settings_t5D29F620DD8E2C4750D3670B9D88BD2F6F4C6FC5::get_offset_of_SlowDownSpeed_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7681 = { sizeof (PlayerShootHandler_t682E0E91FADE0258EA860E263BC6B6EBDA21F202), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7681[6] = 
{
	PlayerShootHandler_t682E0E91FADE0258EA860E263BC6B6EBDA21F202::get_offset_of__audioPlayer_0(),
	PlayerShootHandler_t682E0E91FADE0258EA860E263BC6B6EBDA21F202::get_offset_of__player_1(),
	PlayerShootHandler_t682E0E91FADE0258EA860E263BC6B6EBDA21F202::get_offset_of__settings_2(),
	PlayerShootHandler_t682E0E91FADE0258EA860E263BC6B6EBDA21F202::get_offset_of__bulletPool_3(),
	PlayerShootHandler_t682E0E91FADE0258EA860E263BC6B6EBDA21F202::get_offset_of__inputState_4(),
	PlayerShootHandler_t682E0E91FADE0258EA860E263BC6B6EBDA21F202::get_offset_of__lastFireTime_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7682 = { sizeof (Settings_t59B948D04E16E5F614372F81DC0B8066A189FF9D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7682[6] = 
{
	Settings_t59B948D04E16E5F614372F81DC0B8066A189FF9D::get_offset_of_Laser_0(),
	Settings_t59B948D04E16E5F614372F81DC0B8066A189FF9D::get_offset_of_LaserVolume_1(),
	Settings_t59B948D04E16E5F614372F81DC0B8066A189FF9D::get_offset_of_BulletLifetime_2(),
	Settings_t59B948D04E16E5F614372F81DC0B8066A189FF9D::get_offset_of_BulletSpeed_3(),
	Settings_t59B948D04E16E5F614372F81DC0B8066A189FF9D::get_offset_of_MaxShootInterval_4(),
	Settings_t59B948D04E16E5F614372F81DC0B8066A189FF9D::get_offset_of_BulletOffsetDistance_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7683 = { sizeof (DefaultSerializer_t3DD2A46B85DA76C65C72F3330014F854FAC13B42), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7683[2] = 
{
	0,
	DefaultSerializer_t3DD2A46B85DA76C65C72F3330014F854FAC13B42::get_offset_of_filename_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7684 = { sizeof (GameNotificationChannel_tC51F6355BB3F1A5A6FDAF48D77FA7AE8702F4B79)+ sizeof (RuntimeObject), sizeof(GameNotificationChannel_tC51F6355BB3F1A5A6FDAF48D77FA7AE8702F4B79_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable7684[10] = 
{
	GameNotificationChannel_tC51F6355BB3F1A5A6FDAF48D77FA7AE8702F4B79::get_offset_of_Id_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GameNotificationChannel_tC51F6355BB3F1A5A6FDAF48D77FA7AE8702F4B79::get_offset_of_Name_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GameNotificationChannel_tC51F6355BB3F1A5A6FDAF48D77FA7AE8702F4B79::get_offset_of_Description_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GameNotificationChannel_tC51F6355BB3F1A5A6FDAF48D77FA7AE8702F4B79::get_offset_of_ShowsBadge_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GameNotificationChannel_tC51F6355BB3F1A5A6FDAF48D77FA7AE8702F4B79::get_offset_of_ShowLights_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GameNotificationChannel_tC51F6355BB3F1A5A6FDAF48D77FA7AE8702F4B79::get_offset_of_Vibrates_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GameNotificationChannel_tC51F6355BB3F1A5A6FDAF48D77FA7AE8702F4B79::get_offset_of_HighPriority_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GameNotificationChannel_tC51F6355BB3F1A5A6FDAF48D77FA7AE8702F4B79::get_offset_of_Style_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GameNotificationChannel_tC51F6355BB3F1A5A6FDAF48D77FA7AE8702F4B79::get_offset_of_Privacy_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GameNotificationChannel_tC51F6355BB3F1A5A6FDAF48D77FA7AE8702F4B79::get_offset_of_VibrationPattern_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7685 = { sizeof (NotificationStyle_t75380F00FCDE270371559DCCCCAEBCB63B49BB8E)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7685[5] = 
{
	NotificationStyle_t75380F00FCDE270371559DCCCCAEBCB63B49BB8E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7686 = { sizeof (PrivacyMode_tCFB7939A853B89398E82960B73571EDDDA281167)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7686[4] = 
{
	PrivacyMode_tCFB7939A853B89398E82960B73571EDDDA281167::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7687 = { sizeof (U3CU3Ec_t6C3ABF12175CFC6A4EDCA3C8EBCD0FE21E14957E), -1, sizeof(U3CU3Ec_t6C3ABF12175CFC6A4EDCA3C8EBCD0FE21E14957E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7687[2] = 
{
	U3CU3Ec_t6C3ABF12175CFC6A4EDCA3C8EBCD0FE21E14957E_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t6C3ABF12175CFC6A4EDCA3C8EBCD0FE21E14957E_StaticFields::get_offset_of_U3CU3E9__13_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7688 = { sizeof (GameNotificationsManager_tA91A2FAF80490C54B58C02C8B5652AFA290D6B9F), -1, sizeof(GameNotificationsManager_tA91A2FAF80490C54B58C02C8B5652AFA290D6B9F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7688[11] = 
{
	0,
	GameNotificationsManager_tA91A2FAF80490C54B58C02C8B5652AFA290D6B9F_StaticFields::get_offset_of_MinimumNotificationTime_5(),
	GameNotificationsManager_tA91A2FAF80490C54B58C02C8B5652AFA290D6B9F::get_offset_of_mode_6(),
	GameNotificationsManager_tA91A2FAF80490C54B58C02C8B5652AFA290D6B9F::get_offset_of_autoBadging_7(),
	GameNotificationsManager_tA91A2FAF80490C54B58C02C8B5652AFA290D6B9F::get_offset_of_LocalNotificationDelivered_8(),
	GameNotificationsManager_tA91A2FAF80490C54B58C02C8B5652AFA290D6B9F::get_offset_of_LocalNotificationExpired_9(),
	GameNotificationsManager_tA91A2FAF80490C54B58C02C8B5652AFA290D6B9F::get_offset_of_U3CPlatformU3Ek__BackingField_10(),
	GameNotificationsManager_tA91A2FAF80490C54B58C02C8B5652AFA290D6B9F::get_offset_of_U3CPendingNotificationsU3Ek__BackingField_11(),
	GameNotificationsManager_tA91A2FAF80490C54B58C02C8B5652AFA290D6B9F::get_offset_of_U3CSerializerU3Ek__BackingField_12(),
	GameNotificationsManager_tA91A2FAF80490C54B58C02C8B5652AFA290D6B9F::get_offset_of_U3CInitializedU3Ek__BackingField_13(),
	GameNotificationsManager_tA91A2FAF80490C54B58C02C8B5652AFA290D6B9F::get_offset_of_inForeground_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7689 = { sizeof (OperatingMode_tB9F22EC0B982B1982063A1A4EBEF7A6DBFA54B95)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7689[7] = 
{
	OperatingMode_tB9F22EC0B982B1982063A1A4EBEF7A6DBFA54B95::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7690 = { sizeof (U3CU3Ec_tD764979F67CF10610B4505779C0F4A2EB108DF89), -1, sizeof(U3CU3Ec_tD764979F67CF10610B4505779C0F4A2EB108DF89_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7690[3] = 
{
	U3CU3Ec_tD764979F67CF10610B4505779C0F4A2EB108DF89_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tD764979F67CF10610B4505779C0F4A2EB108DF89_StaticFields::get_offset_of_U3CU3E9__34_0_1(),
	U3CU3Ec_tD764979F67CF10610B4505779C0F4A2EB108DF89_StaticFields::get_offset_of_U3CU3E9__34_1_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7691 = { sizeof (U3CU3Ec__DisplayClass38_0_t8098AD21654110DD42EAC10D5249C9B09F6413AC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7691[1] = 
{
	U3CU3Ec__DisplayClass38_0_t8098AD21654110DD42EAC10D5249C9B09F6413AC::get_offset_of_notificationId_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7692 = { sizeof (U3CU3Ec__DisplayClass42_0_tBB6778E6F27425013423D76334657524233BFBC6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7692[1] = 
{
	U3CU3Ec__DisplayClass42_0_tBB6778E6F27425013423D76334657524233BFBC6::get_offset_of_deliveredNotification_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7693 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7694 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7695 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7696 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7697 = { sizeof (PendingNotification_tB499878D49AF89331495B709217BBE577A95FB87), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7697[2] = 
{
	PendingNotification_tB499878D49AF89331495B709217BBE577A95FB87::get_offset_of_Reschedule_0(),
	PendingNotification_tB499878D49AF89331495B709217BBE577A95FB87::get_offset_of_Notification_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7698 = { sizeof (GraphyDebugger_t1DC748A4B53549510635E53605A906AC607CC4C9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7698[4] = 
{
	GraphyDebugger_t1DC748A4B53549510635E53605A906AC607CC4C9::get_offset_of_m_debugPackets_7(),
	GraphyDebugger_t1DC748A4B53549510635E53605A906AC607CC4C9::get_offset_of_m_fpsMonitor_8(),
	GraphyDebugger_t1DC748A4B53549510635E53605A906AC607CC4C9::get_offset_of_m_ramMonitor_9(),
	GraphyDebugger_t1DC748A4B53549510635E53605A906AC607CC4C9::get_offset_of_m_audioMonitor_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7699 = { sizeof (DebugVariable_t2DB5EEB46D7C214A30CC66E93BC7D3C85015B42C)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7699[9] = 
{
	DebugVariable_t2DB5EEB46D7C214A30CC66E93BC7D3C85015B42C::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
