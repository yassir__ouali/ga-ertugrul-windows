﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Collections.Generic.Dictionary`2<System.Linq.Expressions.ParameterExpression,System.Linq.Expressions.Interpreter.LocalVariable>
struct Dictionary_2_tC8E0B27D96320233525C7F0E36266BE030D852B2;
// System.Collections.Generic.Stack`1<System.Collections.Generic.HashSet`1<System.Linq.Expressions.ParameterExpression>>
struct Stack_1_t60D719A2274AB28AA2E53FC3CCEE3CE8E41D8ECF;
// System.Dynamic.Utils.CacheDict`2<System.Reflection.MethodBase,System.Reflection.ParameterInfo[]>
struct CacheDict_2_t6D77DA1BE8D572DDEB115C919FAC08466CA8C2BB;
// System.Func`2<System.Type,System.Boolean>
struct Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Linq.Expressions.Expression
struct Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F;
// System.Linq.Expressions.Interpreter.Instruction
struct Instruction_t235F1D5246CE88164576679572E0E858988436C3;
// System.Linq.Expressions.Interpreter.InterpretedFrame
struct InterpretedFrame_t9A6E1D0F21363A3BF21977829F9C3D88E5CF0A90;
// System.Linq.Expressions.Interpreter.LightDelegateCreator
struct LightDelegateCreator_t8B2B6E125B98DB397DDF08153B3FD2E34C7FF132;
// System.Reflection.Assembly
struct Assembly_t;
// System.Runtime.CompilerServices.IRuntimeVariables
struct IRuntimeVariables_tDEAB5A363710C8201323E7CD24EDD7B4275D002D;
// System.Runtime.CompilerServices.IStrongBox[]
struct IStrongBoxU5BU5D_tDEC7974E58EF362C9F3BFE2402DC5F4FE7129A27;
// System.String
struct String_t;
// System.Type
struct Type_t;




#ifndef U3CMODULEU3E_TDD607E0208590BE5D73D68EB7825AD7A1FBDFCC3_H
#define U3CMODULEU3E_TDD607E0208590BE5D73D68EB7825AD7A1FBDFCC3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_tDD607E0208590BE5D73D68EB7825AD7A1FBDFCC3 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_TDD607E0208590BE5D73D68EB7825AD7A1FBDFCC3_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#define ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifndef ENUMERABLEHELPERS_T91DA7E2949102CFE16D0A151F7C355F72753F1FD_H
#define ENUMERABLEHELPERS_T91DA7E2949102CFE16D0A151F7C355F72753F1FD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EnumerableHelpers
struct  EnumerableHelpers_t91DA7E2949102CFE16D0A151F7C355F72753F1FD  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERABLEHELPERS_T91DA7E2949102CFE16D0A151F7C355F72753F1FD_H
#ifndef COLLECTIONEXTENSIONS_T04790A89E5724082B570A72C66DCBD3BA4458F41_H
#define COLLECTIONEXTENSIONS_T04790A89E5724082B570A72C66DCBD3BA4458F41_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Dynamic.Utils.CollectionExtensions
struct  CollectionExtensions_t04790A89E5724082B570A72C66DCBD3BA4458F41  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTIONEXTENSIONS_T04790A89E5724082B570A72C66DCBD3BA4458F41_H
#ifndef CONTRACTUTILS_T089DD55E5FFF91E0C61FCECE26F30D46DF46E6DF_H
#define CONTRACTUTILS_T089DD55E5FFF91E0C61FCECE26F30D46DF46E6DF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Dynamic.Utils.ContractUtils
struct  ContractUtils_t089DD55E5FFF91E0C61FCECE26F30D46DF46E6DF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTRACTUTILS_T089DD55E5FFF91E0C61FCECE26F30D46DF46E6DF_H
#ifndef EXPRESSIONUTILS_T94C272C47F1094D7F585B1E39FDC53B1D2E09E9C_H
#define EXPRESSIONUTILS_T94C272C47F1094D7F585B1E39FDC53B1D2E09E9C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Dynamic.Utils.ExpressionUtils
struct  ExpressionUtils_t94C272C47F1094D7F585B1E39FDC53B1D2E09E9C  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPRESSIONUTILS_T94C272C47F1094D7F585B1E39FDC53B1D2E09E9C_H
#ifndef EXPRESSIONVISITORUTILS_TD71C93539A5AD1CBEF16EDA9C1373CA0ECE10EED_H
#define EXPRESSIONVISITORUTILS_TD71C93539A5AD1CBEF16EDA9C1373CA0ECE10EED_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Dynamic.Utils.ExpressionVisitorUtils
struct  ExpressionVisitorUtils_tD71C93539A5AD1CBEF16EDA9C1373CA0ECE10EED  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPRESSIONVISITORUTILS_TD71C93539A5AD1CBEF16EDA9C1373CA0ECE10EED_H
#ifndef TYPEEXTENSIONS_T19D651DD86C56EB81663789F768C4114E51CE9F8_H
#define TYPEEXTENSIONS_T19D651DD86C56EB81663789F768C4114E51CE9F8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Dynamic.Utils.TypeExtensions
struct  TypeExtensions_t19D651DD86C56EB81663789F768C4114E51CE9F8  : public RuntimeObject
{
public:

public:
};

struct TypeExtensions_t19D651DD86C56EB81663789F768C4114E51CE9F8_StaticFields
{
public:
	// System.Dynamic.Utils.CacheDict`2<System.Reflection.MethodBase,System.Reflection.ParameterInfo[]> System.Dynamic.Utils.TypeExtensions::s_paramInfoCache
	CacheDict_2_t6D77DA1BE8D572DDEB115C919FAC08466CA8C2BB * ___s_paramInfoCache_0;

public:
	inline static int32_t get_offset_of_s_paramInfoCache_0() { return static_cast<int32_t>(offsetof(TypeExtensions_t19D651DD86C56EB81663789F768C4114E51CE9F8_StaticFields, ___s_paramInfoCache_0)); }
	inline CacheDict_2_t6D77DA1BE8D572DDEB115C919FAC08466CA8C2BB * get_s_paramInfoCache_0() const { return ___s_paramInfoCache_0; }
	inline CacheDict_2_t6D77DA1BE8D572DDEB115C919FAC08466CA8C2BB ** get_address_of_s_paramInfoCache_0() { return &___s_paramInfoCache_0; }
	inline void set_s_paramInfoCache_0(CacheDict_2_t6D77DA1BE8D572DDEB115C919FAC08466CA8C2BB * value)
	{
		___s_paramInfoCache_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_paramInfoCache_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEEXTENSIONS_T19D651DD86C56EB81663789F768C4114E51CE9F8_H
#ifndef TYPEUTILS_TF994AD398D39478A4587CFB05D295215C77B7402_H
#define TYPEUTILS_TF994AD398D39478A4587CFB05D295215C77B7402_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Dynamic.Utils.TypeUtils
struct  TypeUtils_tF994AD398D39478A4587CFB05D295215C77B7402  : public RuntimeObject
{
public:

public:
};

struct TypeUtils_tF994AD398D39478A4587CFB05D295215C77B7402_StaticFields
{
public:
	// System.Reflection.Assembly System.Dynamic.Utils.TypeUtils::s_mscorlib
	Assembly_t * ___s_mscorlib_0;

public:
	inline static int32_t get_offset_of_s_mscorlib_0() { return static_cast<int32_t>(offsetof(TypeUtils_tF994AD398D39478A4587CFB05D295215C77B7402_StaticFields, ___s_mscorlib_0)); }
	inline Assembly_t * get_s_mscorlib_0() const { return ___s_mscorlib_0; }
	inline Assembly_t ** get_address_of_s_mscorlib_0() { return &___s_mscorlib_0; }
	inline void set_s_mscorlib_0(Assembly_t * value)
	{
		___s_mscorlib_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_mscorlib_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEUTILS_TF994AD398D39478A4587CFB05D295215C77B7402_H
#ifndef EXPRESSIONVISITOR_T2AF012F40B4D4B6FEEB5A6D687B929CF3E0D88AF_H
#define EXPRESSIONVISITOR_T2AF012F40B4D4B6FEEB5A6D687B929CF3E0D88AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.ExpressionVisitor
struct  ExpressionVisitor_t2AF012F40B4D4B6FEEB5A6D687B929CF3E0D88AF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPRESSIONVISITOR_T2AF012F40B4D4B6FEEB5A6D687B929CF3E0D88AF_H
#ifndef DELEGATEHELPERS_TC220AEB31E24035C65BDC2A87576B293B189EBF5_H
#define DELEGATEHELPERS_TC220AEB31E24035C65BDC2A87576B293B189EBF5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.DelegateHelpers
struct  DelegateHelpers_tC220AEB31E24035C65BDC2A87576B293B189EBF5  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATEHELPERS_TC220AEB31E24035C65BDC2A87576B293B189EBF5_H
#ifndef U3CU3EC_TFB9335A435065C66EE55EDA65A70B1197E95FA40_H
#define U3CU3EC_TFB9335A435065C66EE55EDA65A70B1197E95FA40_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.DelegateHelpers_<>c
struct  U3CU3Ec_tFB9335A435065C66EE55EDA65A70B1197E95FA40  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tFB9335A435065C66EE55EDA65A70B1197E95FA40_StaticFields
{
public:
	// System.Linq.Expressions.Interpreter.DelegateHelpers_<>c System.Linq.Expressions.Interpreter.DelegateHelpers_<>c::<>9
	U3CU3Ec_tFB9335A435065C66EE55EDA65A70B1197E95FA40 * ___U3CU3E9_0;
	// System.Func`2<System.Type,System.Boolean> System.Linq.Expressions.Interpreter.DelegateHelpers_<>c::<>9__1_0
	Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 * ___U3CU3E9__1_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tFB9335A435065C66EE55EDA65A70B1197E95FA40_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tFB9335A435065C66EE55EDA65A70B1197E95FA40 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tFB9335A435065C66EE55EDA65A70B1197E95FA40 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tFB9335A435065C66EE55EDA65A70B1197E95FA40 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__1_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tFB9335A435065C66EE55EDA65A70B1197E95FA40_StaticFields, ___U3CU3E9__1_0_1)); }
	inline Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 * get_U3CU3E9__1_0_1() const { return ___U3CU3E9__1_0_1; }
	inline Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 ** get_address_of_U3CU3E9__1_0_1() { return &___U3CU3E9__1_0_1; }
	inline void set_U3CU3E9__1_0_1(Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 * value)
	{
		___U3CU3E9__1_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__1_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TFB9335A435065C66EE55EDA65A70B1197E95FA40_H
#ifndef EXCEPTIONHELPERS_T5E8A8C882B8B390FA25BB54BF282A31251678BB9_H
#define EXCEPTIONHELPERS_T5E8A8C882B8B390FA25BB54BF282A31251678BB9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.ExceptionHelpers
struct  ExceptionHelpers_t5E8A8C882B8B390FA25BB54BF282A31251678BB9  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTIONHELPERS_T5E8A8C882B8B390FA25BB54BF282A31251678BB9_H
#ifndef INSTRUCTION_T235F1D5246CE88164576679572E0E858988436C3_H
#define INSTRUCTION_T235F1D5246CE88164576679572E0E858988436C3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.Instruction
struct  Instruction_t235F1D5246CE88164576679572E0E858988436C3  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTRUCTION_T235F1D5246CE88164576679572E0E858988436C3_H
#ifndef RUNTIMEVARIABLES_T80F25BDAAAD3795F8687E62A259E14D255AC4E5F_H
#define RUNTIMEVARIABLES_T80F25BDAAAD3795F8687E62A259E14D255AC4E5F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.RuntimeVariables
struct  RuntimeVariables_t80F25BDAAAD3795F8687E62A259E14D255AC4E5F  : public RuntimeObject
{
public:
	// System.Runtime.CompilerServices.IStrongBox[] System.Linq.Expressions.Interpreter.RuntimeVariables::_boxes
	IStrongBoxU5BU5D_tDEC7974E58EF362C9F3BFE2402DC5F4FE7129A27* ____boxes_0;

public:
	inline static int32_t get_offset_of__boxes_0() { return static_cast<int32_t>(offsetof(RuntimeVariables_t80F25BDAAAD3795F8687E62A259E14D255AC4E5F, ____boxes_0)); }
	inline IStrongBoxU5BU5D_tDEC7974E58EF362C9F3BFE2402DC5F4FE7129A27* get__boxes_0() const { return ____boxes_0; }
	inline IStrongBoxU5BU5D_tDEC7974E58EF362C9F3BFE2402DC5F4FE7129A27** get_address_of__boxes_0() { return &____boxes_0; }
	inline void set__boxes_0(IStrongBoxU5BU5D_tDEC7974E58EF362C9F3BFE2402DC5F4FE7129A27* value)
	{
		____boxes_0 = value;
		Il2CppCodeGenWriteBarrier((&____boxes_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEVARIABLES_T80F25BDAAAD3795F8687E62A259E14D255AC4E5F_H
#ifndef SCRIPTINGRUNTIMEHELPERS_T5EBE625EFE604DEF42AFBCA8F9EF3A808FF38DC7_H
#define SCRIPTINGRUNTIMEHELPERS_T5EBE625EFE604DEF42AFBCA8F9EF3A808FF38DC7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.ScriptingRuntimeHelpers
struct  ScriptingRuntimeHelpers_t5EBE625EFE604DEF42AFBCA8F9EF3A808FF38DC7  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRIPTINGRUNTIMEHELPERS_T5EBE625EFE604DEF42AFBCA8F9EF3A808FF38DC7_H
#ifndef DEBUGINFOGENERATOR_TD7E0E52E58589C53D21CDCA37EB68E89E040ECDF_H
#define DEBUGINFOGENERATOR_TD7E0E52E58589C53D21CDCA37EB68E89E040ECDF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.DebugInfoGenerator
struct  DebugInfoGenerator_tD7E0E52E58589C53D21CDCA37EB68E89E040ECDF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGINFOGENERATOR_TD7E0E52E58589C53D21CDCA37EB68E89E040ECDF_H
#ifndef RUNTIMEOPS_T4AB498C75C54378274A510D5CD7DC9152A23E777_H
#define RUNTIMEOPS_T4AB498C75C54378274A510D5CD7DC9152A23E777_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.RuntimeOps
struct  RuntimeOps_t4AB498C75C54378274A510D5CD7DC9152A23E777  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOPS_T4AB498C75C54378274A510D5CD7DC9152A23E777_H
#ifndef MERGEDRUNTIMEVARIABLES_T8BC0546349BF4A11B383C0C2B6D81EC1C1AB2DDD_H
#define MERGEDRUNTIMEVARIABLES_T8BC0546349BF4A11B383C0C2B6D81EC1C1AB2DDD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.RuntimeOps_MergedRuntimeVariables
struct  MergedRuntimeVariables_t8BC0546349BF4A11B383C0C2B6D81EC1C1AB2DDD  : public RuntimeObject
{
public:
	// System.Runtime.CompilerServices.IRuntimeVariables System.Runtime.CompilerServices.RuntimeOps_MergedRuntimeVariables::_first
	RuntimeObject* ____first_0;
	// System.Runtime.CompilerServices.IRuntimeVariables System.Runtime.CompilerServices.RuntimeOps_MergedRuntimeVariables::_second
	RuntimeObject* ____second_1;
	// System.Int32[] System.Runtime.CompilerServices.RuntimeOps_MergedRuntimeVariables::_indexes
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ____indexes_2;

public:
	inline static int32_t get_offset_of__first_0() { return static_cast<int32_t>(offsetof(MergedRuntimeVariables_t8BC0546349BF4A11B383C0C2B6D81EC1C1AB2DDD, ____first_0)); }
	inline RuntimeObject* get__first_0() const { return ____first_0; }
	inline RuntimeObject** get_address_of__first_0() { return &____first_0; }
	inline void set__first_0(RuntimeObject* value)
	{
		____first_0 = value;
		Il2CppCodeGenWriteBarrier((&____first_0), value);
	}

	inline static int32_t get_offset_of__second_1() { return static_cast<int32_t>(offsetof(MergedRuntimeVariables_t8BC0546349BF4A11B383C0C2B6D81EC1C1AB2DDD, ____second_1)); }
	inline RuntimeObject* get__second_1() const { return ____second_1; }
	inline RuntimeObject** get_address_of__second_1() { return &____second_1; }
	inline void set__second_1(RuntimeObject* value)
	{
		____second_1 = value;
		Il2CppCodeGenWriteBarrier((&____second_1), value);
	}

	inline static int32_t get_offset_of__indexes_2() { return static_cast<int32_t>(offsetof(MergedRuntimeVariables_t8BC0546349BF4A11B383C0C2B6D81EC1C1AB2DDD, ____indexes_2)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get__indexes_2() const { return ____indexes_2; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of__indexes_2() { return &____indexes_2; }
	inline void set__indexes_2(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		____indexes_2 = value;
		Il2CppCodeGenWriteBarrier((&____indexes_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MERGEDRUNTIMEVARIABLES_T8BC0546349BF4A11B383C0C2B6D81EC1C1AB2DDD_H
#ifndef RUNTIMEVARIABLES_T08E07CA7021C6692E793278352675C63B744D61A_H
#define RUNTIMEVARIABLES_T08E07CA7021C6692E793278352675C63B744D61A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.RuntimeOps_RuntimeVariables
struct  RuntimeVariables_t08E07CA7021C6692E793278352675C63B744D61A  : public RuntimeObject
{
public:
	// System.Runtime.CompilerServices.IStrongBox[] System.Runtime.CompilerServices.RuntimeOps_RuntimeVariables::_boxes
	IStrongBoxU5BU5D_tDEC7974E58EF362C9F3BFE2402DC5F4FE7129A27* ____boxes_0;

public:
	inline static int32_t get_offset_of__boxes_0() { return static_cast<int32_t>(offsetof(RuntimeVariables_t08E07CA7021C6692E793278352675C63B744D61A, ____boxes_0)); }
	inline IStrongBoxU5BU5D_tDEC7974E58EF362C9F3BFE2402DC5F4FE7129A27* get__boxes_0() const { return ____boxes_0; }
	inline IStrongBoxU5BU5D_tDEC7974E58EF362C9F3BFE2402DC5F4FE7129A27** get_address_of__boxes_0() { return &____boxes_0; }
	inline void set__boxes_0(IStrongBoxU5BU5D_tDEC7974E58EF362C9F3BFE2402DC5F4FE7129A27* value)
	{
		____boxes_0 = value;
		Il2CppCodeGenWriteBarrier((&____boxes_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEVARIABLES_T08E07CA7021C6692E793278352675C63B744D61A_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef __STATICARRAYINITTYPESIZEU3D1024_T336389AC57307AEC77791F09CF655CD3EF917B7C_H
#define __STATICARRAYINITTYPESIZEU3D1024_T336389AC57307AEC77791F09CF655CD3EF917B7C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D1024
struct  __StaticArrayInitTypeSizeU3D1024_t336389AC57307AEC77791F09CF655CD3EF917B7C 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D1024_t336389AC57307AEC77791F09CF655CD3EF917B7C__padding[1024];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D1024_T336389AC57307AEC77791F09CF655CD3EF917B7C_H
#ifndef __STATICARRAYINITTYPESIZEU3D120_T83E233123DD538AA6101D1CB5AE4F5DC639EBC9D_H
#define __STATICARRAYINITTYPESIZEU3D120_T83E233123DD538AA6101D1CB5AE4F5DC639EBC9D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D120
struct  __StaticArrayInitTypeSizeU3D120_t83E233123DD538AA6101D1CB5AE4F5DC639EBC9D 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D120_t83E233123DD538AA6101D1CB5AE4F5DC639EBC9D__padding[120];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D120_T83E233123DD538AA6101D1CB5AE4F5DC639EBC9D_H
#ifndef __STATICARRAYINITTYPESIZEU3D256_TCCCC326240ED0A827344379DD68205BF9340FF47_H
#define __STATICARRAYINITTYPESIZEU3D256_TCCCC326240ED0A827344379DD68205BF9340FF47_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D256
struct  __StaticArrayInitTypeSizeU3D256_tCCCC326240ED0A827344379DD68205BF9340FF47 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D256_tCCCC326240ED0A827344379DD68205BF9340FF47__padding[256];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D256_TCCCC326240ED0A827344379DD68205BF9340FF47_H
#ifndef CASTINSTRUCTION_T9D7DD2D7AFA96134CE216AE247702DADA774E6BC_H
#define CASTINSTRUCTION_T9D7DD2D7AFA96134CE216AE247702DADA774E6BC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.CastInstruction
struct  CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC  : public Instruction_t235F1D5246CE88164576679572E0E858988436C3
{
public:

public:
};

struct CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields
{
public:
	// System.Linq.Expressions.Interpreter.CastInstruction System.Linq.Expressions.Interpreter.CastInstruction::s_Boolean
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * ___s_Boolean_0;
	// System.Linq.Expressions.Interpreter.CastInstruction System.Linq.Expressions.Interpreter.CastInstruction::s_Byte
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * ___s_Byte_1;
	// System.Linq.Expressions.Interpreter.CastInstruction System.Linq.Expressions.Interpreter.CastInstruction::s_Char
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * ___s_Char_2;
	// System.Linq.Expressions.Interpreter.CastInstruction System.Linq.Expressions.Interpreter.CastInstruction::s_DateTime
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * ___s_DateTime_3;
	// System.Linq.Expressions.Interpreter.CastInstruction System.Linq.Expressions.Interpreter.CastInstruction::s_Decimal
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * ___s_Decimal_4;
	// System.Linq.Expressions.Interpreter.CastInstruction System.Linq.Expressions.Interpreter.CastInstruction::s_Double
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * ___s_Double_5;
	// System.Linq.Expressions.Interpreter.CastInstruction System.Linq.Expressions.Interpreter.CastInstruction::s_Int16
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * ___s_Int16_6;
	// System.Linq.Expressions.Interpreter.CastInstruction System.Linq.Expressions.Interpreter.CastInstruction::s_Int32
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * ___s_Int32_7;
	// System.Linq.Expressions.Interpreter.CastInstruction System.Linq.Expressions.Interpreter.CastInstruction::s_Int64
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * ___s_Int64_8;
	// System.Linq.Expressions.Interpreter.CastInstruction System.Linq.Expressions.Interpreter.CastInstruction::s_SByte
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * ___s_SByte_9;
	// System.Linq.Expressions.Interpreter.CastInstruction System.Linq.Expressions.Interpreter.CastInstruction::s_Single
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * ___s_Single_10;
	// System.Linq.Expressions.Interpreter.CastInstruction System.Linq.Expressions.Interpreter.CastInstruction::s_String
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * ___s_String_11;
	// System.Linq.Expressions.Interpreter.CastInstruction System.Linq.Expressions.Interpreter.CastInstruction::s_UInt16
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * ___s_UInt16_12;
	// System.Linq.Expressions.Interpreter.CastInstruction System.Linq.Expressions.Interpreter.CastInstruction::s_UInt32
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * ___s_UInt32_13;
	// System.Linq.Expressions.Interpreter.CastInstruction System.Linq.Expressions.Interpreter.CastInstruction::s_UInt64
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * ___s_UInt64_14;

public:
	inline static int32_t get_offset_of_s_Boolean_0() { return static_cast<int32_t>(offsetof(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields, ___s_Boolean_0)); }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * get_s_Boolean_0() const { return ___s_Boolean_0; }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC ** get_address_of_s_Boolean_0() { return &___s_Boolean_0; }
	inline void set_s_Boolean_0(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * value)
	{
		___s_Boolean_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Boolean_0), value);
	}

	inline static int32_t get_offset_of_s_Byte_1() { return static_cast<int32_t>(offsetof(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields, ___s_Byte_1)); }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * get_s_Byte_1() const { return ___s_Byte_1; }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC ** get_address_of_s_Byte_1() { return &___s_Byte_1; }
	inline void set_s_Byte_1(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * value)
	{
		___s_Byte_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_Byte_1), value);
	}

	inline static int32_t get_offset_of_s_Char_2() { return static_cast<int32_t>(offsetof(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields, ___s_Char_2)); }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * get_s_Char_2() const { return ___s_Char_2; }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC ** get_address_of_s_Char_2() { return &___s_Char_2; }
	inline void set_s_Char_2(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * value)
	{
		___s_Char_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Char_2), value);
	}

	inline static int32_t get_offset_of_s_DateTime_3() { return static_cast<int32_t>(offsetof(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields, ___s_DateTime_3)); }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * get_s_DateTime_3() const { return ___s_DateTime_3; }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC ** get_address_of_s_DateTime_3() { return &___s_DateTime_3; }
	inline void set_s_DateTime_3(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * value)
	{
		___s_DateTime_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_DateTime_3), value);
	}

	inline static int32_t get_offset_of_s_Decimal_4() { return static_cast<int32_t>(offsetof(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields, ___s_Decimal_4)); }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * get_s_Decimal_4() const { return ___s_Decimal_4; }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC ** get_address_of_s_Decimal_4() { return &___s_Decimal_4; }
	inline void set_s_Decimal_4(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * value)
	{
		___s_Decimal_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_Decimal_4), value);
	}

	inline static int32_t get_offset_of_s_Double_5() { return static_cast<int32_t>(offsetof(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields, ___s_Double_5)); }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * get_s_Double_5() const { return ___s_Double_5; }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC ** get_address_of_s_Double_5() { return &___s_Double_5; }
	inline void set_s_Double_5(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * value)
	{
		___s_Double_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_Double_5), value);
	}

	inline static int32_t get_offset_of_s_Int16_6() { return static_cast<int32_t>(offsetof(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields, ___s_Int16_6)); }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * get_s_Int16_6() const { return ___s_Int16_6; }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC ** get_address_of_s_Int16_6() { return &___s_Int16_6; }
	inline void set_s_Int16_6(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * value)
	{
		___s_Int16_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_Int16_6), value);
	}

	inline static int32_t get_offset_of_s_Int32_7() { return static_cast<int32_t>(offsetof(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields, ___s_Int32_7)); }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * get_s_Int32_7() const { return ___s_Int32_7; }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC ** get_address_of_s_Int32_7() { return &___s_Int32_7; }
	inline void set_s_Int32_7(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * value)
	{
		___s_Int32_7 = value;
		Il2CppCodeGenWriteBarrier((&___s_Int32_7), value);
	}

	inline static int32_t get_offset_of_s_Int64_8() { return static_cast<int32_t>(offsetof(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields, ___s_Int64_8)); }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * get_s_Int64_8() const { return ___s_Int64_8; }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC ** get_address_of_s_Int64_8() { return &___s_Int64_8; }
	inline void set_s_Int64_8(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * value)
	{
		___s_Int64_8 = value;
		Il2CppCodeGenWriteBarrier((&___s_Int64_8), value);
	}

	inline static int32_t get_offset_of_s_SByte_9() { return static_cast<int32_t>(offsetof(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields, ___s_SByte_9)); }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * get_s_SByte_9() const { return ___s_SByte_9; }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC ** get_address_of_s_SByte_9() { return &___s_SByte_9; }
	inline void set_s_SByte_9(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * value)
	{
		___s_SByte_9 = value;
		Il2CppCodeGenWriteBarrier((&___s_SByte_9), value);
	}

	inline static int32_t get_offset_of_s_Single_10() { return static_cast<int32_t>(offsetof(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields, ___s_Single_10)); }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * get_s_Single_10() const { return ___s_Single_10; }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC ** get_address_of_s_Single_10() { return &___s_Single_10; }
	inline void set_s_Single_10(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * value)
	{
		___s_Single_10 = value;
		Il2CppCodeGenWriteBarrier((&___s_Single_10), value);
	}

	inline static int32_t get_offset_of_s_String_11() { return static_cast<int32_t>(offsetof(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields, ___s_String_11)); }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * get_s_String_11() const { return ___s_String_11; }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC ** get_address_of_s_String_11() { return &___s_String_11; }
	inline void set_s_String_11(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * value)
	{
		___s_String_11 = value;
		Il2CppCodeGenWriteBarrier((&___s_String_11), value);
	}

	inline static int32_t get_offset_of_s_UInt16_12() { return static_cast<int32_t>(offsetof(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields, ___s_UInt16_12)); }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * get_s_UInt16_12() const { return ___s_UInt16_12; }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC ** get_address_of_s_UInt16_12() { return &___s_UInt16_12; }
	inline void set_s_UInt16_12(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * value)
	{
		___s_UInt16_12 = value;
		Il2CppCodeGenWriteBarrier((&___s_UInt16_12), value);
	}

	inline static int32_t get_offset_of_s_UInt32_13() { return static_cast<int32_t>(offsetof(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields, ___s_UInt32_13)); }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * get_s_UInt32_13() const { return ___s_UInt32_13; }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC ** get_address_of_s_UInt32_13() { return &___s_UInt32_13; }
	inline void set_s_UInt32_13(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * value)
	{
		___s_UInt32_13 = value;
		Il2CppCodeGenWriteBarrier((&___s_UInt32_13), value);
	}

	inline static int32_t get_offset_of_s_UInt64_14() { return static_cast<int32_t>(offsetof(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields, ___s_UInt64_14)); }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * get_s_UInt64_14() const { return ___s_UInt64_14; }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC ** get_address_of_s_UInt64_14() { return &___s_UInt64_14; }
	inline void set_s_UInt64_14(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * value)
	{
		___s_UInt64_14 = value;
		Il2CppCodeGenWriteBarrier((&___s_UInt64_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CASTINSTRUCTION_T9D7DD2D7AFA96134CE216AE247702DADA774E6BC_H
#ifndef CREATEDELEGATEINSTRUCTION_TF6B6DE5E99CEC35C22720C4BA3F2F16602284AB5_H
#define CREATEDELEGATEINSTRUCTION_TF6B6DE5E99CEC35C22720C4BA3F2F16602284AB5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.CreateDelegateInstruction
struct  CreateDelegateInstruction_tF6B6DE5E99CEC35C22720C4BA3F2F16602284AB5  : public Instruction_t235F1D5246CE88164576679572E0E858988436C3
{
public:
	// System.Linq.Expressions.Interpreter.LightDelegateCreator System.Linq.Expressions.Interpreter.CreateDelegateInstruction::_creator
	LightDelegateCreator_t8B2B6E125B98DB397DDF08153B3FD2E34C7FF132 * ____creator_0;

public:
	inline static int32_t get_offset_of__creator_0() { return static_cast<int32_t>(offsetof(CreateDelegateInstruction_tF6B6DE5E99CEC35C22720C4BA3F2F16602284AB5, ____creator_0)); }
	inline LightDelegateCreator_t8B2B6E125B98DB397DDF08153B3FD2E34C7FF132 * get__creator_0() const { return ____creator_0; }
	inline LightDelegateCreator_t8B2B6E125B98DB397DDF08153B3FD2E34C7FF132 ** get_address_of__creator_0() { return &____creator_0; }
	inline void set__creator_0(LightDelegateCreator_t8B2B6E125B98DB397DDF08153B3FD2E34C7FF132 * value)
	{
		____creator_0 = value;
		Il2CppCodeGenWriteBarrier((&____creator_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CREATEDELEGATEINSTRUCTION_TF6B6DE5E99CEC35C22720C4BA3F2F16602284AB5_H
#ifndef DUPINSTRUCTION_T35C73C83F8421A299FBC38A6C9189E88F7FD2512_H
#define DUPINSTRUCTION_T35C73C83F8421A299FBC38A6C9189E88F7FD2512_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.DupInstruction
struct  DupInstruction_t35C73C83F8421A299FBC38A6C9189E88F7FD2512  : public Instruction_t235F1D5246CE88164576679572E0E858988436C3
{
public:

public:
};

struct DupInstruction_t35C73C83F8421A299FBC38A6C9189E88F7FD2512_StaticFields
{
public:
	// System.Linq.Expressions.Interpreter.DupInstruction System.Linq.Expressions.Interpreter.DupInstruction::Instance
	DupInstruction_t35C73C83F8421A299FBC38A6C9189E88F7FD2512 * ___Instance_0;

public:
	inline static int32_t get_offset_of_Instance_0() { return static_cast<int32_t>(offsetof(DupInstruction_t35C73C83F8421A299FBC38A6C9189E88F7FD2512_StaticFields, ___Instance_0)); }
	inline DupInstruction_t35C73C83F8421A299FBC38A6C9189E88F7FD2512 * get_Instance_0() const { return ___Instance_0; }
	inline DupInstruction_t35C73C83F8421A299FBC38A6C9189E88F7FD2512 ** get_address_of_Instance_0() { return &___Instance_0; }
	inline void set_Instance_0(DupInstruction_t35C73C83F8421A299FBC38A6C9189E88F7FD2512 * value)
	{
		___Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DUPINSTRUCTION_T35C73C83F8421A299FBC38A6C9189E88F7FD2512_H
#ifndef LOADCACHEDOBJECTINSTRUCTION_T70C07C702DAC18B1BB6BE378B9036A440B359D32_H
#define LOADCACHEDOBJECTINSTRUCTION_T70C07C702DAC18B1BB6BE378B9036A440B359D32_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.LoadCachedObjectInstruction
struct  LoadCachedObjectInstruction_t70C07C702DAC18B1BB6BE378B9036A440B359D32  : public Instruction_t235F1D5246CE88164576679572E0E858988436C3
{
public:
	// System.UInt32 System.Linq.Expressions.Interpreter.LoadCachedObjectInstruction::_index
	uint32_t ____index_0;

public:
	inline static int32_t get_offset_of__index_0() { return static_cast<int32_t>(offsetof(LoadCachedObjectInstruction_t70C07C702DAC18B1BB6BE378B9036A440B359D32, ____index_0)); }
	inline uint32_t get__index_0() const { return ____index_0; }
	inline uint32_t* get_address_of__index_0() { return &____index_0; }
	inline void set__index_0(uint32_t value)
	{
		____index_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADCACHEDOBJECTINSTRUCTION_T70C07C702DAC18B1BB6BE378B9036A440B359D32_H
#ifndef LOADOBJECTINSTRUCTION_T2C8D43685496B24000A7E069FDAEE73D4E72F1A7_H
#define LOADOBJECTINSTRUCTION_T2C8D43685496B24000A7E069FDAEE73D4E72F1A7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.LoadObjectInstruction
struct  LoadObjectInstruction_t2C8D43685496B24000A7E069FDAEE73D4E72F1A7  : public Instruction_t235F1D5246CE88164576679572E0E858988436C3
{
public:
	// System.Object System.Linq.Expressions.Interpreter.LoadObjectInstruction::_value
	RuntimeObject * ____value_0;

public:
	inline static int32_t get_offset_of__value_0() { return static_cast<int32_t>(offsetof(LoadObjectInstruction_t2C8D43685496B24000A7E069FDAEE73D4E72F1A7, ____value_0)); }
	inline RuntimeObject * get__value_0() const { return ____value_0; }
	inline RuntimeObject ** get_address_of__value_0() { return &____value_0; }
	inline void set__value_0(RuntimeObject * value)
	{
		____value_0 = value;
		Il2CppCodeGenWriteBarrier((&____value_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADOBJECTINSTRUCTION_T2C8D43685496B24000A7E069FDAEE73D4E72F1A7_H
#ifndef NULLABLEMETHODCALLINSTRUCTION_TB3AB48D3CD968DEC637BC849D02E10345BAAE6DB_H
#define NULLABLEMETHODCALLINSTRUCTION_TB3AB48D3CD968DEC637BC849D02E10345BAAE6DB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.NullableMethodCallInstruction
struct  NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB  : public Instruction_t235F1D5246CE88164576679572E0E858988436C3
{
public:

public:
};

struct NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB_StaticFields
{
public:
	// System.Linq.Expressions.Interpreter.NullableMethodCallInstruction System.Linq.Expressions.Interpreter.NullableMethodCallInstruction::s_hasValue
	NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB * ___s_hasValue_0;
	// System.Linq.Expressions.Interpreter.NullableMethodCallInstruction System.Linq.Expressions.Interpreter.NullableMethodCallInstruction::s_value
	NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB * ___s_value_1;
	// System.Linq.Expressions.Interpreter.NullableMethodCallInstruction System.Linq.Expressions.Interpreter.NullableMethodCallInstruction::s_equals
	NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB * ___s_equals_2;
	// System.Linq.Expressions.Interpreter.NullableMethodCallInstruction System.Linq.Expressions.Interpreter.NullableMethodCallInstruction::s_getHashCode
	NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB * ___s_getHashCode_3;
	// System.Linq.Expressions.Interpreter.NullableMethodCallInstruction System.Linq.Expressions.Interpreter.NullableMethodCallInstruction::s_getValueOrDefault1
	NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB * ___s_getValueOrDefault1_4;
	// System.Linq.Expressions.Interpreter.NullableMethodCallInstruction System.Linq.Expressions.Interpreter.NullableMethodCallInstruction::s_toString
	NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB * ___s_toString_5;

public:
	inline static int32_t get_offset_of_s_hasValue_0() { return static_cast<int32_t>(offsetof(NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB_StaticFields, ___s_hasValue_0)); }
	inline NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB * get_s_hasValue_0() const { return ___s_hasValue_0; }
	inline NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB ** get_address_of_s_hasValue_0() { return &___s_hasValue_0; }
	inline void set_s_hasValue_0(NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB * value)
	{
		___s_hasValue_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_hasValue_0), value);
	}

	inline static int32_t get_offset_of_s_value_1() { return static_cast<int32_t>(offsetof(NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB_StaticFields, ___s_value_1)); }
	inline NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB * get_s_value_1() const { return ___s_value_1; }
	inline NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB ** get_address_of_s_value_1() { return &___s_value_1; }
	inline void set_s_value_1(NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB * value)
	{
		___s_value_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_value_1), value);
	}

	inline static int32_t get_offset_of_s_equals_2() { return static_cast<int32_t>(offsetof(NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB_StaticFields, ___s_equals_2)); }
	inline NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB * get_s_equals_2() const { return ___s_equals_2; }
	inline NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB ** get_address_of_s_equals_2() { return &___s_equals_2; }
	inline void set_s_equals_2(NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB * value)
	{
		___s_equals_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_equals_2), value);
	}

	inline static int32_t get_offset_of_s_getHashCode_3() { return static_cast<int32_t>(offsetof(NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB_StaticFields, ___s_getHashCode_3)); }
	inline NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB * get_s_getHashCode_3() const { return ___s_getHashCode_3; }
	inline NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB ** get_address_of_s_getHashCode_3() { return &___s_getHashCode_3; }
	inline void set_s_getHashCode_3(NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB * value)
	{
		___s_getHashCode_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_getHashCode_3), value);
	}

	inline static int32_t get_offset_of_s_getValueOrDefault1_4() { return static_cast<int32_t>(offsetof(NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB_StaticFields, ___s_getValueOrDefault1_4)); }
	inline NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB * get_s_getValueOrDefault1_4() const { return ___s_getValueOrDefault1_4; }
	inline NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB ** get_address_of_s_getValueOrDefault1_4() { return &___s_getValueOrDefault1_4; }
	inline void set_s_getValueOrDefault1_4(NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB * value)
	{
		___s_getValueOrDefault1_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_getValueOrDefault1_4), value);
	}

	inline static int32_t get_offset_of_s_toString_5() { return static_cast<int32_t>(offsetof(NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB_StaticFields, ___s_toString_5)); }
	inline NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB * get_s_toString_5() const { return ___s_toString_5; }
	inline NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB ** get_address_of_s_toString_5() { return &___s_toString_5; }
	inline void set_s_toString_5(NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB * value)
	{
		___s_toString_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_toString_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLEMETHODCALLINSTRUCTION_TB3AB48D3CD968DEC637BC849D02E10345BAAE6DB_H
#ifndef ORINSTRUCTION_T18E2D320D39C7EB49874032BF1A037C6F4249625_H
#define ORINSTRUCTION_T18E2D320D39C7EB49874032BF1A037C6F4249625_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.OrInstruction
struct  OrInstruction_t18E2D320D39C7EB49874032BF1A037C6F4249625  : public Instruction_t235F1D5246CE88164576679572E0E858988436C3
{
public:

public:
};

struct OrInstruction_t18E2D320D39C7EB49874032BF1A037C6F4249625_StaticFields
{
public:
	// System.Linq.Expressions.Interpreter.Instruction System.Linq.Expressions.Interpreter.OrInstruction::s_SByte
	Instruction_t235F1D5246CE88164576679572E0E858988436C3 * ___s_SByte_0;
	// System.Linq.Expressions.Interpreter.Instruction System.Linq.Expressions.Interpreter.OrInstruction::s_Int16
	Instruction_t235F1D5246CE88164576679572E0E858988436C3 * ___s_Int16_1;
	// System.Linq.Expressions.Interpreter.Instruction System.Linq.Expressions.Interpreter.OrInstruction::s_Int32
	Instruction_t235F1D5246CE88164576679572E0E858988436C3 * ___s_Int32_2;
	// System.Linq.Expressions.Interpreter.Instruction System.Linq.Expressions.Interpreter.OrInstruction::s_Int64
	Instruction_t235F1D5246CE88164576679572E0E858988436C3 * ___s_Int64_3;
	// System.Linq.Expressions.Interpreter.Instruction System.Linq.Expressions.Interpreter.OrInstruction::s_Byte
	Instruction_t235F1D5246CE88164576679572E0E858988436C3 * ___s_Byte_4;
	// System.Linq.Expressions.Interpreter.Instruction System.Linq.Expressions.Interpreter.OrInstruction::s_UInt16
	Instruction_t235F1D5246CE88164576679572E0E858988436C3 * ___s_UInt16_5;
	// System.Linq.Expressions.Interpreter.Instruction System.Linq.Expressions.Interpreter.OrInstruction::s_UInt32
	Instruction_t235F1D5246CE88164576679572E0E858988436C3 * ___s_UInt32_6;
	// System.Linq.Expressions.Interpreter.Instruction System.Linq.Expressions.Interpreter.OrInstruction::s_UInt64
	Instruction_t235F1D5246CE88164576679572E0E858988436C3 * ___s_UInt64_7;
	// System.Linq.Expressions.Interpreter.Instruction System.Linq.Expressions.Interpreter.OrInstruction::s_Boolean
	Instruction_t235F1D5246CE88164576679572E0E858988436C3 * ___s_Boolean_8;

public:
	inline static int32_t get_offset_of_s_SByte_0() { return static_cast<int32_t>(offsetof(OrInstruction_t18E2D320D39C7EB49874032BF1A037C6F4249625_StaticFields, ___s_SByte_0)); }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 * get_s_SByte_0() const { return ___s_SByte_0; }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 ** get_address_of_s_SByte_0() { return &___s_SByte_0; }
	inline void set_s_SByte_0(Instruction_t235F1D5246CE88164576679572E0E858988436C3 * value)
	{
		___s_SByte_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_SByte_0), value);
	}

	inline static int32_t get_offset_of_s_Int16_1() { return static_cast<int32_t>(offsetof(OrInstruction_t18E2D320D39C7EB49874032BF1A037C6F4249625_StaticFields, ___s_Int16_1)); }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 * get_s_Int16_1() const { return ___s_Int16_1; }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 ** get_address_of_s_Int16_1() { return &___s_Int16_1; }
	inline void set_s_Int16_1(Instruction_t235F1D5246CE88164576679572E0E858988436C3 * value)
	{
		___s_Int16_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_Int16_1), value);
	}

	inline static int32_t get_offset_of_s_Int32_2() { return static_cast<int32_t>(offsetof(OrInstruction_t18E2D320D39C7EB49874032BF1A037C6F4249625_StaticFields, ___s_Int32_2)); }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 * get_s_Int32_2() const { return ___s_Int32_2; }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 ** get_address_of_s_Int32_2() { return &___s_Int32_2; }
	inline void set_s_Int32_2(Instruction_t235F1D5246CE88164576679572E0E858988436C3 * value)
	{
		___s_Int32_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Int32_2), value);
	}

	inline static int32_t get_offset_of_s_Int64_3() { return static_cast<int32_t>(offsetof(OrInstruction_t18E2D320D39C7EB49874032BF1A037C6F4249625_StaticFields, ___s_Int64_3)); }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 * get_s_Int64_3() const { return ___s_Int64_3; }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 ** get_address_of_s_Int64_3() { return &___s_Int64_3; }
	inline void set_s_Int64_3(Instruction_t235F1D5246CE88164576679572E0E858988436C3 * value)
	{
		___s_Int64_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_Int64_3), value);
	}

	inline static int32_t get_offset_of_s_Byte_4() { return static_cast<int32_t>(offsetof(OrInstruction_t18E2D320D39C7EB49874032BF1A037C6F4249625_StaticFields, ___s_Byte_4)); }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 * get_s_Byte_4() const { return ___s_Byte_4; }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 ** get_address_of_s_Byte_4() { return &___s_Byte_4; }
	inline void set_s_Byte_4(Instruction_t235F1D5246CE88164576679572E0E858988436C3 * value)
	{
		___s_Byte_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_Byte_4), value);
	}

	inline static int32_t get_offset_of_s_UInt16_5() { return static_cast<int32_t>(offsetof(OrInstruction_t18E2D320D39C7EB49874032BF1A037C6F4249625_StaticFields, ___s_UInt16_5)); }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 * get_s_UInt16_5() const { return ___s_UInt16_5; }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 ** get_address_of_s_UInt16_5() { return &___s_UInt16_5; }
	inline void set_s_UInt16_5(Instruction_t235F1D5246CE88164576679572E0E858988436C3 * value)
	{
		___s_UInt16_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_UInt16_5), value);
	}

	inline static int32_t get_offset_of_s_UInt32_6() { return static_cast<int32_t>(offsetof(OrInstruction_t18E2D320D39C7EB49874032BF1A037C6F4249625_StaticFields, ___s_UInt32_6)); }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 * get_s_UInt32_6() const { return ___s_UInt32_6; }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 ** get_address_of_s_UInt32_6() { return &___s_UInt32_6; }
	inline void set_s_UInt32_6(Instruction_t235F1D5246CE88164576679572E0E858988436C3 * value)
	{
		___s_UInt32_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_UInt32_6), value);
	}

	inline static int32_t get_offset_of_s_UInt64_7() { return static_cast<int32_t>(offsetof(OrInstruction_t18E2D320D39C7EB49874032BF1A037C6F4249625_StaticFields, ___s_UInt64_7)); }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 * get_s_UInt64_7() const { return ___s_UInt64_7; }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 ** get_address_of_s_UInt64_7() { return &___s_UInt64_7; }
	inline void set_s_UInt64_7(Instruction_t235F1D5246CE88164576679572E0E858988436C3 * value)
	{
		___s_UInt64_7 = value;
		Il2CppCodeGenWriteBarrier((&___s_UInt64_7), value);
	}

	inline static int32_t get_offset_of_s_Boolean_8() { return static_cast<int32_t>(offsetof(OrInstruction_t18E2D320D39C7EB49874032BF1A037C6F4249625_StaticFields, ___s_Boolean_8)); }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 * get_s_Boolean_8() const { return ___s_Boolean_8; }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 ** get_address_of_s_Boolean_8() { return &___s_Boolean_8; }
	inline void set_s_Boolean_8(Instruction_t235F1D5246CE88164576679572E0E858988436C3 * value)
	{
		___s_Boolean_8 = value;
		Il2CppCodeGenWriteBarrier((&___s_Boolean_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ORINSTRUCTION_T18E2D320D39C7EB49874032BF1A037C6F4249625_H
#ifndef POPINSTRUCTION_TE203CE250FAF056C574B7AAE9D6422EE7A735C52_H
#define POPINSTRUCTION_TE203CE250FAF056C574B7AAE9D6422EE7A735C52_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.PopInstruction
struct  PopInstruction_tE203CE250FAF056C574B7AAE9D6422EE7A735C52  : public Instruction_t235F1D5246CE88164576679572E0E858988436C3
{
public:

public:
};

struct PopInstruction_tE203CE250FAF056C574B7AAE9D6422EE7A735C52_StaticFields
{
public:
	// System.Linq.Expressions.Interpreter.PopInstruction System.Linq.Expressions.Interpreter.PopInstruction::Instance
	PopInstruction_tE203CE250FAF056C574B7AAE9D6422EE7A735C52 * ___Instance_0;

public:
	inline static int32_t get_offset_of_Instance_0() { return static_cast<int32_t>(offsetof(PopInstruction_tE203CE250FAF056C574B7AAE9D6422EE7A735C52_StaticFields, ___Instance_0)); }
	inline PopInstruction_tE203CE250FAF056C574B7AAE9D6422EE7A735C52 * get_Instance_0() const { return ___Instance_0; }
	inline PopInstruction_tE203CE250FAF056C574B7AAE9D6422EE7A735C52 ** get_address_of_Instance_0() { return &___Instance_0; }
	inline void set_Instance_0(PopInstruction_tE203CE250FAF056C574B7AAE9D6422EE7A735C52 * value)
	{
		___Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POPINSTRUCTION_TE203CE250FAF056C574B7AAE9D6422EE7A735C52_H
#ifndef QUOTEINSTRUCTION_TCC0594851C52F750FDFF3185E0001E3D60CA9C1B_H
#define QUOTEINSTRUCTION_TCC0594851C52F750FDFF3185E0001E3D60CA9C1B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.QuoteInstruction
struct  QuoteInstruction_tCC0594851C52F750FDFF3185E0001E3D60CA9C1B  : public Instruction_t235F1D5246CE88164576679572E0E858988436C3
{
public:
	// System.Linq.Expressions.Expression System.Linq.Expressions.Interpreter.QuoteInstruction::_operand
	Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * ____operand_0;
	// System.Collections.Generic.Dictionary`2<System.Linq.Expressions.ParameterExpression,System.Linq.Expressions.Interpreter.LocalVariable> System.Linq.Expressions.Interpreter.QuoteInstruction::_hoistedVariables
	Dictionary_2_tC8E0B27D96320233525C7F0E36266BE030D852B2 * ____hoistedVariables_1;

public:
	inline static int32_t get_offset_of__operand_0() { return static_cast<int32_t>(offsetof(QuoteInstruction_tCC0594851C52F750FDFF3185E0001E3D60CA9C1B, ____operand_0)); }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * get__operand_0() const { return ____operand_0; }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F ** get_address_of__operand_0() { return &____operand_0; }
	inline void set__operand_0(Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * value)
	{
		____operand_0 = value;
		Il2CppCodeGenWriteBarrier((&____operand_0), value);
	}

	inline static int32_t get_offset_of__hoistedVariables_1() { return static_cast<int32_t>(offsetof(QuoteInstruction_tCC0594851C52F750FDFF3185E0001E3D60CA9C1B, ____hoistedVariables_1)); }
	inline Dictionary_2_tC8E0B27D96320233525C7F0E36266BE030D852B2 * get__hoistedVariables_1() const { return ____hoistedVariables_1; }
	inline Dictionary_2_tC8E0B27D96320233525C7F0E36266BE030D852B2 ** get_address_of__hoistedVariables_1() { return &____hoistedVariables_1; }
	inline void set__hoistedVariables_1(Dictionary_2_tC8E0B27D96320233525C7F0E36266BE030D852B2 * value)
	{
		____hoistedVariables_1 = value;
		Il2CppCodeGenWriteBarrier((&____hoistedVariables_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUOTEINSTRUCTION_TCC0594851C52F750FDFF3185E0001E3D60CA9C1B_H
#ifndef EXPRESSIONQUOTER_TEAE5B88BBC6EB0B78A277DE39515AF585236DC08_H
#define EXPRESSIONQUOTER_TEAE5B88BBC6EB0B78A277DE39515AF585236DC08_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.QuoteInstruction_ExpressionQuoter
struct  ExpressionQuoter_tEAE5B88BBC6EB0B78A277DE39515AF585236DC08  : public ExpressionVisitor_t2AF012F40B4D4B6FEEB5A6D687B929CF3E0D88AF
{
public:
	// System.Collections.Generic.Dictionary`2<System.Linq.Expressions.ParameterExpression,System.Linq.Expressions.Interpreter.LocalVariable> System.Linq.Expressions.Interpreter.QuoteInstruction_ExpressionQuoter::_variables
	Dictionary_2_tC8E0B27D96320233525C7F0E36266BE030D852B2 * ____variables_0;
	// System.Linq.Expressions.Interpreter.InterpretedFrame System.Linq.Expressions.Interpreter.QuoteInstruction_ExpressionQuoter::_frame
	InterpretedFrame_t9A6E1D0F21363A3BF21977829F9C3D88E5CF0A90 * ____frame_1;
	// System.Collections.Generic.Stack`1<System.Collections.Generic.HashSet`1<System.Linq.Expressions.ParameterExpression>> System.Linq.Expressions.Interpreter.QuoteInstruction_ExpressionQuoter::_shadowedVars
	Stack_1_t60D719A2274AB28AA2E53FC3CCEE3CE8E41D8ECF * ____shadowedVars_2;

public:
	inline static int32_t get_offset_of__variables_0() { return static_cast<int32_t>(offsetof(ExpressionQuoter_tEAE5B88BBC6EB0B78A277DE39515AF585236DC08, ____variables_0)); }
	inline Dictionary_2_tC8E0B27D96320233525C7F0E36266BE030D852B2 * get__variables_0() const { return ____variables_0; }
	inline Dictionary_2_tC8E0B27D96320233525C7F0E36266BE030D852B2 ** get_address_of__variables_0() { return &____variables_0; }
	inline void set__variables_0(Dictionary_2_tC8E0B27D96320233525C7F0E36266BE030D852B2 * value)
	{
		____variables_0 = value;
		Il2CppCodeGenWriteBarrier((&____variables_0), value);
	}

	inline static int32_t get_offset_of__frame_1() { return static_cast<int32_t>(offsetof(ExpressionQuoter_tEAE5B88BBC6EB0B78A277DE39515AF585236DC08, ____frame_1)); }
	inline InterpretedFrame_t9A6E1D0F21363A3BF21977829F9C3D88E5CF0A90 * get__frame_1() const { return ____frame_1; }
	inline InterpretedFrame_t9A6E1D0F21363A3BF21977829F9C3D88E5CF0A90 ** get_address_of__frame_1() { return &____frame_1; }
	inline void set__frame_1(InterpretedFrame_t9A6E1D0F21363A3BF21977829F9C3D88E5CF0A90 * value)
	{
		____frame_1 = value;
		Il2CppCodeGenWriteBarrier((&____frame_1), value);
	}

	inline static int32_t get_offset_of__shadowedVars_2() { return static_cast<int32_t>(offsetof(ExpressionQuoter_tEAE5B88BBC6EB0B78A277DE39515AF585236DC08, ____shadowedVars_2)); }
	inline Stack_1_t60D719A2274AB28AA2E53FC3CCEE3CE8E41D8ECF * get__shadowedVars_2() const { return ____shadowedVars_2; }
	inline Stack_1_t60D719A2274AB28AA2E53FC3CCEE3CE8E41D8ECF ** get_address_of__shadowedVars_2() { return &____shadowedVars_2; }
	inline void set__shadowedVars_2(Stack_1_t60D719A2274AB28AA2E53FC3CCEE3CE8E41D8ECF * value)
	{
		____shadowedVars_2 = value;
		Il2CppCodeGenWriteBarrier((&____shadowedVars_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPRESSIONQUOTER_TEAE5B88BBC6EB0B78A277DE39515AF585236DC08_H
#ifndef RIGHTSHIFTINSTRUCTION_T5BCE4FCF5332FE9B144DA762AE3E7B9E39BFD927_H
#define RIGHTSHIFTINSTRUCTION_T5BCE4FCF5332FE9B144DA762AE3E7B9E39BFD927_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.RightShiftInstruction
struct  RightShiftInstruction_t5BCE4FCF5332FE9B144DA762AE3E7B9E39BFD927  : public Instruction_t235F1D5246CE88164576679572E0E858988436C3
{
public:

public:
};

struct RightShiftInstruction_t5BCE4FCF5332FE9B144DA762AE3E7B9E39BFD927_StaticFields
{
public:
	// System.Linq.Expressions.Interpreter.Instruction System.Linq.Expressions.Interpreter.RightShiftInstruction::s_SByte
	Instruction_t235F1D5246CE88164576679572E0E858988436C3 * ___s_SByte_0;
	// System.Linq.Expressions.Interpreter.Instruction System.Linq.Expressions.Interpreter.RightShiftInstruction::s_Int16
	Instruction_t235F1D5246CE88164576679572E0E858988436C3 * ___s_Int16_1;
	// System.Linq.Expressions.Interpreter.Instruction System.Linq.Expressions.Interpreter.RightShiftInstruction::s_Int32
	Instruction_t235F1D5246CE88164576679572E0E858988436C3 * ___s_Int32_2;
	// System.Linq.Expressions.Interpreter.Instruction System.Linq.Expressions.Interpreter.RightShiftInstruction::s_Int64
	Instruction_t235F1D5246CE88164576679572E0E858988436C3 * ___s_Int64_3;
	// System.Linq.Expressions.Interpreter.Instruction System.Linq.Expressions.Interpreter.RightShiftInstruction::s_Byte
	Instruction_t235F1D5246CE88164576679572E0E858988436C3 * ___s_Byte_4;
	// System.Linq.Expressions.Interpreter.Instruction System.Linq.Expressions.Interpreter.RightShiftInstruction::s_UInt16
	Instruction_t235F1D5246CE88164576679572E0E858988436C3 * ___s_UInt16_5;
	// System.Linq.Expressions.Interpreter.Instruction System.Linq.Expressions.Interpreter.RightShiftInstruction::s_UInt32
	Instruction_t235F1D5246CE88164576679572E0E858988436C3 * ___s_UInt32_6;
	// System.Linq.Expressions.Interpreter.Instruction System.Linq.Expressions.Interpreter.RightShiftInstruction::s_UInt64
	Instruction_t235F1D5246CE88164576679572E0E858988436C3 * ___s_UInt64_7;

public:
	inline static int32_t get_offset_of_s_SByte_0() { return static_cast<int32_t>(offsetof(RightShiftInstruction_t5BCE4FCF5332FE9B144DA762AE3E7B9E39BFD927_StaticFields, ___s_SByte_0)); }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 * get_s_SByte_0() const { return ___s_SByte_0; }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 ** get_address_of_s_SByte_0() { return &___s_SByte_0; }
	inline void set_s_SByte_0(Instruction_t235F1D5246CE88164576679572E0E858988436C3 * value)
	{
		___s_SByte_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_SByte_0), value);
	}

	inline static int32_t get_offset_of_s_Int16_1() { return static_cast<int32_t>(offsetof(RightShiftInstruction_t5BCE4FCF5332FE9B144DA762AE3E7B9E39BFD927_StaticFields, ___s_Int16_1)); }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 * get_s_Int16_1() const { return ___s_Int16_1; }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 ** get_address_of_s_Int16_1() { return &___s_Int16_1; }
	inline void set_s_Int16_1(Instruction_t235F1D5246CE88164576679572E0E858988436C3 * value)
	{
		___s_Int16_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_Int16_1), value);
	}

	inline static int32_t get_offset_of_s_Int32_2() { return static_cast<int32_t>(offsetof(RightShiftInstruction_t5BCE4FCF5332FE9B144DA762AE3E7B9E39BFD927_StaticFields, ___s_Int32_2)); }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 * get_s_Int32_2() const { return ___s_Int32_2; }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 ** get_address_of_s_Int32_2() { return &___s_Int32_2; }
	inline void set_s_Int32_2(Instruction_t235F1D5246CE88164576679572E0E858988436C3 * value)
	{
		___s_Int32_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Int32_2), value);
	}

	inline static int32_t get_offset_of_s_Int64_3() { return static_cast<int32_t>(offsetof(RightShiftInstruction_t5BCE4FCF5332FE9B144DA762AE3E7B9E39BFD927_StaticFields, ___s_Int64_3)); }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 * get_s_Int64_3() const { return ___s_Int64_3; }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 ** get_address_of_s_Int64_3() { return &___s_Int64_3; }
	inline void set_s_Int64_3(Instruction_t235F1D5246CE88164576679572E0E858988436C3 * value)
	{
		___s_Int64_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_Int64_3), value);
	}

	inline static int32_t get_offset_of_s_Byte_4() { return static_cast<int32_t>(offsetof(RightShiftInstruction_t5BCE4FCF5332FE9B144DA762AE3E7B9E39BFD927_StaticFields, ___s_Byte_4)); }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 * get_s_Byte_4() const { return ___s_Byte_4; }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 ** get_address_of_s_Byte_4() { return &___s_Byte_4; }
	inline void set_s_Byte_4(Instruction_t235F1D5246CE88164576679572E0E858988436C3 * value)
	{
		___s_Byte_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_Byte_4), value);
	}

	inline static int32_t get_offset_of_s_UInt16_5() { return static_cast<int32_t>(offsetof(RightShiftInstruction_t5BCE4FCF5332FE9B144DA762AE3E7B9E39BFD927_StaticFields, ___s_UInt16_5)); }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 * get_s_UInt16_5() const { return ___s_UInt16_5; }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 ** get_address_of_s_UInt16_5() { return &___s_UInt16_5; }
	inline void set_s_UInt16_5(Instruction_t235F1D5246CE88164576679572E0E858988436C3 * value)
	{
		___s_UInt16_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_UInt16_5), value);
	}

	inline static int32_t get_offset_of_s_UInt32_6() { return static_cast<int32_t>(offsetof(RightShiftInstruction_t5BCE4FCF5332FE9B144DA762AE3E7B9E39BFD927_StaticFields, ___s_UInt32_6)); }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 * get_s_UInt32_6() const { return ___s_UInt32_6; }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 ** get_address_of_s_UInt32_6() { return &___s_UInt32_6; }
	inline void set_s_UInt32_6(Instruction_t235F1D5246CE88164576679572E0E858988436C3 * value)
	{
		___s_UInt32_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_UInt32_6), value);
	}

	inline static int32_t get_offset_of_s_UInt64_7() { return static_cast<int32_t>(offsetof(RightShiftInstruction_t5BCE4FCF5332FE9B144DA762AE3E7B9E39BFD927_StaticFields, ___s_UInt64_7)); }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 * get_s_UInt64_7() const { return ___s_UInt64_7; }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 ** get_address_of_s_UInt64_7() { return &___s_UInt64_7; }
	inline void set_s_UInt64_7(Instruction_t235F1D5246CE88164576679572E0E858988436C3 * value)
	{
		___s_UInt64_7 = value;
		Il2CppCodeGenWriteBarrier((&___s_UInt64_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RIGHTSHIFTINSTRUCTION_T5BCE4FCF5332FE9B144DA762AE3E7B9E39BFD927_H
#ifndef SUBINSTRUCTION_T734617B532D4FE50053C3CC64466A2599DEFD3F6_H
#define SUBINSTRUCTION_T734617B532D4FE50053C3CC64466A2599DEFD3F6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.SubInstruction
struct  SubInstruction_t734617B532D4FE50053C3CC64466A2599DEFD3F6  : public Instruction_t235F1D5246CE88164576679572E0E858988436C3
{
public:

public:
};

struct SubInstruction_t734617B532D4FE50053C3CC64466A2599DEFD3F6_StaticFields
{
public:
	// System.Linq.Expressions.Interpreter.Instruction System.Linq.Expressions.Interpreter.SubInstruction::s_Int16
	Instruction_t235F1D5246CE88164576679572E0E858988436C3 * ___s_Int16_0;
	// System.Linq.Expressions.Interpreter.Instruction System.Linq.Expressions.Interpreter.SubInstruction::s_Int32
	Instruction_t235F1D5246CE88164576679572E0E858988436C3 * ___s_Int32_1;
	// System.Linq.Expressions.Interpreter.Instruction System.Linq.Expressions.Interpreter.SubInstruction::s_Int64
	Instruction_t235F1D5246CE88164576679572E0E858988436C3 * ___s_Int64_2;
	// System.Linq.Expressions.Interpreter.Instruction System.Linq.Expressions.Interpreter.SubInstruction::s_UInt16
	Instruction_t235F1D5246CE88164576679572E0E858988436C3 * ___s_UInt16_3;
	// System.Linq.Expressions.Interpreter.Instruction System.Linq.Expressions.Interpreter.SubInstruction::s_UInt32
	Instruction_t235F1D5246CE88164576679572E0E858988436C3 * ___s_UInt32_4;
	// System.Linq.Expressions.Interpreter.Instruction System.Linq.Expressions.Interpreter.SubInstruction::s_UInt64
	Instruction_t235F1D5246CE88164576679572E0E858988436C3 * ___s_UInt64_5;
	// System.Linq.Expressions.Interpreter.Instruction System.Linq.Expressions.Interpreter.SubInstruction::s_Single
	Instruction_t235F1D5246CE88164576679572E0E858988436C3 * ___s_Single_6;
	// System.Linq.Expressions.Interpreter.Instruction System.Linq.Expressions.Interpreter.SubInstruction::s_Double
	Instruction_t235F1D5246CE88164576679572E0E858988436C3 * ___s_Double_7;

public:
	inline static int32_t get_offset_of_s_Int16_0() { return static_cast<int32_t>(offsetof(SubInstruction_t734617B532D4FE50053C3CC64466A2599DEFD3F6_StaticFields, ___s_Int16_0)); }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 * get_s_Int16_0() const { return ___s_Int16_0; }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 ** get_address_of_s_Int16_0() { return &___s_Int16_0; }
	inline void set_s_Int16_0(Instruction_t235F1D5246CE88164576679572E0E858988436C3 * value)
	{
		___s_Int16_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Int16_0), value);
	}

	inline static int32_t get_offset_of_s_Int32_1() { return static_cast<int32_t>(offsetof(SubInstruction_t734617B532D4FE50053C3CC64466A2599DEFD3F6_StaticFields, ___s_Int32_1)); }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 * get_s_Int32_1() const { return ___s_Int32_1; }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 ** get_address_of_s_Int32_1() { return &___s_Int32_1; }
	inline void set_s_Int32_1(Instruction_t235F1D5246CE88164576679572E0E858988436C3 * value)
	{
		___s_Int32_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_Int32_1), value);
	}

	inline static int32_t get_offset_of_s_Int64_2() { return static_cast<int32_t>(offsetof(SubInstruction_t734617B532D4FE50053C3CC64466A2599DEFD3F6_StaticFields, ___s_Int64_2)); }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 * get_s_Int64_2() const { return ___s_Int64_2; }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 ** get_address_of_s_Int64_2() { return &___s_Int64_2; }
	inline void set_s_Int64_2(Instruction_t235F1D5246CE88164576679572E0E858988436C3 * value)
	{
		___s_Int64_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Int64_2), value);
	}

	inline static int32_t get_offset_of_s_UInt16_3() { return static_cast<int32_t>(offsetof(SubInstruction_t734617B532D4FE50053C3CC64466A2599DEFD3F6_StaticFields, ___s_UInt16_3)); }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 * get_s_UInt16_3() const { return ___s_UInt16_3; }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 ** get_address_of_s_UInt16_3() { return &___s_UInt16_3; }
	inline void set_s_UInt16_3(Instruction_t235F1D5246CE88164576679572E0E858988436C3 * value)
	{
		___s_UInt16_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_UInt16_3), value);
	}

	inline static int32_t get_offset_of_s_UInt32_4() { return static_cast<int32_t>(offsetof(SubInstruction_t734617B532D4FE50053C3CC64466A2599DEFD3F6_StaticFields, ___s_UInt32_4)); }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 * get_s_UInt32_4() const { return ___s_UInt32_4; }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 ** get_address_of_s_UInt32_4() { return &___s_UInt32_4; }
	inline void set_s_UInt32_4(Instruction_t235F1D5246CE88164576679572E0E858988436C3 * value)
	{
		___s_UInt32_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_UInt32_4), value);
	}

	inline static int32_t get_offset_of_s_UInt64_5() { return static_cast<int32_t>(offsetof(SubInstruction_t734617B532D4FE50053C3CC64466A2599DEFD3F6_StaticFields, ___s_UInt64_5)); }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 * get_s_UInt64_5() const { return ___s_UInt64_5; }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 ** get_address_of_s_UInt64_5() { return &___s_UInt64_5; }
	inline void set_s_UInt64_5(Instruction_t235F1D5246CE88164576679572E0E858988436C3 * value)
	{
		___s_UInt64_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_UInt64_5), value);
	}

	inline static int32_t get_offset_of_s_Single_6() { return static_cast<int32_t>(offsetof(SubInstruction_t734617B532D4FE50053C3CC64466A2599DEFD3F6_StaticFields, ___s_Single_6)); }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 * get_s_Single_6() const { return ___s_Single_6; }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 ** get_address_of_s_Single_6() { return &___s_Single_6; }
	inline void set_s_Single_6(Instruction_t235F1D5246CE88164576679572E0E858988436C3 * value)
	{
		___s_Single_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_Single_6), value);
	}

	inline static int32_t get_offset_of_s_Double_7() { return static_cast<int32_t>(offsetof(SubInstruction_t734617B532D4FE50053C3CC64466A2599DEFD3F6_StaticFields, ___s_Double_7)); }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 * get_s_Double_7() const { return ___s_Double_7; }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 ** get_address_of_s_Double_7() { return &___s_Double_7; }
	inline void set_s_Double_7(Instruction_t235F1D5246CE88164576679572E0E858988436C3 * value)
	{
		___s_Double_7 = value;
		Il2CppCodeGenWriteBarrier((&___s_Double_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBINSTRUCTION_T734617B532D4FE50053C3CC64466A2599DEFD3F6_H
#ifndef SUBOVFINSTRUCTION_TC360B9BB53E1001E5636FD18D7240A1A507216C3_H
#define SUBOVFINSTRUCTION_TC360B9BB53E1001E5636FD18D7240A1A507216C3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.SubOvfInstruction
struct  SubOvfInstruction_tC360B9BB53E1001E5636FD18D7240A1A507216C3  : public Instruction_t235F1D5246CE88164576679572E0E858988436C3
{
public:

public:
};

struct SubOvfInstruction_tC360B9BB53E1001E5636FD18D7240A1A507216C3_StaticFields
{
public:
	// System.Linq.Expressions.Interpreter.Instruction System.Linq.Expressions.Interpreter.SubOvfInstruction::s_Int16
	Instruction_t235F1D5246CE88164576679572E0E858988436C3 * ___s_Int16_0;
	// System.Linq.Expressions.Interpreter.Instruction System.Linq.Expressions.Interpreter.SubOvfInstruction::s_Int32
	Instruction_t235F1D5246CE88164576679572E0E858988436C3 * ___s_Int32_1;
	// System.Linq.Expressions.Interpreter.Instruction System.Linq.Expressions.Interpreter.SubOvfInstruction::s_Int64
	Instruction_t235F1D5246CE88164576679572E0E858988436C3 * ___s_Int64_2;
	// System.Linq.Expressions.Interpreter.Instruction System.Linq.Expressions.Interpreter.SubOvfInstruction::s_UInt16
	Instruction_t235F1D5246CE88164576679572E0E858988436C3 * ___s_UInt16_3;
	// System.Linq.Expressions.Interpreter.Instruction System.Linq.Expressions.Interpreter.SubOvfInstruction::s_UInt32
	Instruction_t235F1D5246CE88164576679572E0E858988436C3 * ___s_UInt32_4;
	// System.Linq.Expressions.Interpreter.Instruction System.Linq.Expressions.Interpreter.SubOvfInstruction::s_UInt64
	Instruction_t235F1D5246CE88164576679572E0E858988436C3 * ___s_UInt64_5;

public:
	inline static int32_t get_offset_of_s_Int16_0() { return static_cast<int32_t>(offsetof(SubOvfInstruction_tC360B9BB53E1001E5636FD18D7240A1A507216C3_StaticFields, ___s_Int16_0)); }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 * get_s_Int16_0() const { return ___s_Int16_0; }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 ** get_address_of_s_Int16_0() { return &___s_Int16_0; }
	inline void set_s_Int16_0(Instruction_t235F1D5246CE88164576679572E0E858988436C3 * value)
	{
		___s_Int16_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Int16_0), value);
	}

	inline static int32_t get_offset_of_s_Int32_1() { return static_cast<int32_t>(offsetof(SubOvfInstruction_tC360B9BB53E1001E5636FD18D7240A1A507216C3_StaticFields, ___s_Int32_1)); }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 * get_s_Int32_1() const { return ___s_Int32_1; }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 ** get_address_of_s_Int32_1() { return &___s_Int32_1; }
	inline void set_s_Int32_1(Instruction_t235F1D5246CE88164576679572E0E858988436C3 * value)
	{
		___s_Int32_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_Int32_1), value);
	}

	inline static int32_t get_offset_of_s_Int64_2() { return static_cast<int32_t>(offsetof(SubOvfInstruction_tC360B9BB53E1001E5636FD18D7240A1A507216C3_StaticFields, ___s_Int64_2)); }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 * get_s_Int64_2() const { return ___s_Int64_2; }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 ** get_address_of_s_Int64_2() { return &___s_Int64_2; }
	inline void set_s_Int64_2(Instruction_t235F1D5246CE88164576679572E0E858988436C3 * value)
	{
		___s_Int64_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Int64_2), value);
	}

	inline static int32_t get_offset_of_s_UInt16_3() { return static_cast<int32_t>(offsetof(SubOvfInstruction_tC360B9BB53E1001E5636FD18D7240A1A507216C3_StaticFields, ___s_UInt16_3)); }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 * get_s_UInt16_3() const { return ___s_UInt16_3; }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 ** get_address_of_s_UInt16_3() { return &___s_UInt16_3; }
	inline void set_s_UInt16_3(Instruction_t235F1D5246CE88164576679572E0E858988436C3 * value)
	{
		___s_UInt16_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_UInt16_3), value);
	}

	inline static int32_t get_offset_of_s_UInt32_4() { return static_cast<int32_t>(offsetof(SubOvfInstruction_tC360B9BB53E1001E5636FD18D7240A1A507216C3_StaticFields, ___s_UInt32_4)); }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 * get_s_UInt32_4() const { return ___s_UInt32_4; }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 ** get_address_of_s_UInt32_4() { return &___s_UInt32_4; }
	inline void set_s_UInt32_4(Instruction_t235F1D5246CE88164576679572E0E858988436C3 * value)
	{
		___s_UInt32_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_UInt32_4), value);
	}

	inline static int32_t get_offset_of_s_UInt64_5() { return static_cast<int32_t>(offsetof(SubOvfInstruction_tC360B9BB53E1001E5636FD18D7240A1A507216C3_StaticFields, ___s_UInt64_5)); }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 * get_s_UInt64_5() const { return ___s_UInt64_5; }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 ** get_address_of_s_UInt64_5() { return &___s_UInt64_5; }
	inline void set_s_UInt64_5(Instruction_t235F1D5246CE88164576679572E0E858988436C3 * value)
	{
		___s_UInt64_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_UInt64_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBOVFINSTRUCTION_TC360B9BB53E1001E5636FD18D7240A1A507216C3_H
#ifndef TYPEASINSTRUCTION_TCF3FC69CAA49AC26C4C9BF96F989F7CDB046134E_H
#define TYPEASINSTRUCTION_TCF3FC69CAA49AC26C4C9BF96F989F7CDB046134E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.TypeAsInstruction
struct  TypeAsInstruction_tCF3FC69CAA49AC26C4C9BF96F989F7CDB046134E  : public Instruction_t235F1D5246CE88164576679572E0E858988436C3
{
public:
	// System.Type System.Linq.Expressions.Interpreter.TypeAsInstruction::_type
	Type_t * ____type_0;

public:
	inline static int32_t get_offset_of__type_0() { return static_cast<int32_t>(offsetof(TypeAsInstruction_tCF3FC69CAA49AC26C4C9BF96F989F7CDB046134E, ____type_0)); }
	inline Type_t * get__type_0() const { return ____type_0; }
	inline Type_t ** get_address_of__type_0() { return &____type_0; }
	inline void set__type_0(Type_t * value)
	{
		____type_0 = value;
		Il2CppCodeGenWriteBarrier((&____type_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEASINSTRUCTION_TCF3FC69CAA49AC26C4C9BF96F989F7CDB046134E_H
#ifndef TYPEEQUALSINSTRUCTION_TCF7B3DA0050CA5B84DD1B7A209D9CC66AD81F260_H
#define TYPEEQUALSINSTRUCTION_TCF7B3DA0050CA5B84DD1B7A209D9CC66AD81F260_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.TypeEqualsInstruction
struct  TypeEqualsInstruction_tCF7B3DA0050CA5B84DD1B7A209D9CC66AD81F260  : public Instruction_t235F1D5246CE88164576679572E0E858988436C3
{
public:

public:
};

struct TypeEqualsInstruction_tCF7B3DA0050CA5B84DD1B7A209D9CC66AD81F260_StaticFields
{
public:
	// System.Linq.Expressions.Interpreter.TypeEqualsInstruction System.Linq.Expressions.Interpreter.TypeEqualsInstruction::Instance
	TypeEqualsInstruction_tCF7B3DA0050CA5B84DD1B7A209D9CC66AD81F260 * ___Instance_0;

public:
	inline static int32_t get_offset_of_Instance_0() { return static_cast<int32_t>(offsetof(TypeEqualsInstruction_tCF7B3DA0050CA5B84DD1B7A209D9CC66AD81F260_StaticFields, ___Instance_0)); }
	inline TypeEqualsInstruction_tCF7B3DA0050CA5B84DD1B7A209D9CC66AD81F260 * get_Instance_0() const { return ___Instance_0; }
	inline TypeEqualsInstruction_tCF7B3DA0050CA5B84DD1B7A209D9CC66AD81F260 ** get_address_of_Instance_0() { return &___Instance_0; }
	inline void set_Instance_0(TypeEqualsInstruction_tCF7B3DA0050CA5B84DD1B7A209D9CC66AD81F260 * value)
	{
		___Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEEQUALSINSTRUCTION_TCF7B3DA0050CA5B84DD1B7A209D9CC66AD81F260_H
#ifndef TYPEISINSTRUCTION_T9F7EF0BB05E8F7A713E452A49E9C1979344032D8_H
#define TYPEISINSTRUCTION_T9F7EF0BB05E8F7A713E452A49E9C1979344032D8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.TypeIsInstruction
struct  TypeIsInstruction_t9F7EF0BB05E8F7A713E452A49E9C1979344032D8  : public Instruction_t235F1D5246CE88164576679572E0E858988436C3
{
public:
	// System.Type System.Linq.Expressions.Interpreter.TypeIsInstruction::_type
	Type_t * ____type_0;

public:
	inline static int32_t get_offset_of__type_0() { return static_cast<int32_t>(offsetof(TypeIsInstruction_t9F7EF0BB05E8F7A713E452A49E9C1979344032D8, ____type_0)); }
	inline Type_t * get__type_0() const { return ____type_0; }
	inline Type_t ** get_address_of__type_0() { return &____type_0; }
	inline void set__type_0(Type_t * value)
	{
		____type_0 = value;
		Il2CppCodeGenWriteBarrier((&____type_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEISINSTRUCTION_T9F7EF0BB05E8F7A713E452A49E9C1979344032D8_H
#ifndef REQUIREDBYNATIVECODEATTRIBUTE_T949320E827C2BD269B3E686FE317A18835670AAE_H
#define REQUIREDBYNATIVECODEATTRIBUTE_T949320E827C2BD269B3E686FE317A18835670AAE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Scripting.RequiredByNativeCodeAttribute
struct  RequiredByNativeCodeAttribute_t949320E827C2BD269B3E686FE317A18835670AAE  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String UnityEngine.Scripting.RequiredByNativeCodeAttribute::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
	// System.Boolean UnityEngine.Scripting.RequiredByNativeCodeAttribute::<Optional>k__BackingField
	bool ___U3COptionalU3Ek__BackingField_1;
	// System.Boolean UnityEngine.Scripting.RequiredByNativeCodeAttribute::<GenerateProxy>k__BackingField
	bool ___U3CGenerateProxyU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RequiredByNativeCodeAttribute_t949320E827C2BD269B3E686FE317A18835670AAE, ___U3CNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CNameU3Ek__BackingField_0() const { return ___U3CNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_0() { return &___U3CNameU3Ek__BackingField_0; }
	inline void set_U3CNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3COptionalU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RequiredByNativeCodeAttribute_t949320E827C2BD269B3E686FE317A18835670AAE, ___U3COptionalU3Ek__BackingField_1)); }
	inline bool get_U3COptionalU3Ek__BackingField_1() const { return ___U3COptionalU3Ek__BackingField_1; }
	inline bool* get_address_of_U3COptionalU3Ek__BackingField_1() { return &___U3COptionalU3Ek__BackingField_1; }
	inline void set_U3COptionalU3Ek__BackingField_1(bool value)
	{
		___U3COptionalU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CGenerateProxyU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(RequiredByNativeCodeAttribute_t949320E827C2BD269B3E686FE317A18835670AAE, ___U3CGenerateProxyU3Ek__BackingField_2)); }
	inline bool get_U3CGenerateProxyU3Ek__BackingField_2() const { return ___U3CGenerateProxyU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CGenerateProxyU3Ek__BackingField_2() { return &___U3CGenerateProxyU3Ek__BackingField_2; }
	inline void set_U3CGenerateProxyU3Ek__BackingField_2(bool value)
	{
		___U3CGenerateProxyU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUIREDBYNATIVECODEATTRIBUTE_T949320E827C2BD269B3E686FE317A18835670AAE_H
#ifndef USEDBYNATIVECODEATTRIBUTE_T923F9A140847AF2F193AD1AB33143B8774797912_H
#define USEDBYNATIVECODEATTRIBUTE_T923F9A140847AF2F193AD1AB33143B8774797912_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Scripting.UsedByNativeCodeAttribute
struct  UsedByNativeCodeAttribute_t923F9A140847AF2F193AD1AB33143B8774797912  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String UnityEngine.Scripting.UsedByNativeCodeAttribute::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(UsedByNativeCodeAttribute_t923F9A140847AF2F193AD1AB33143B8774797912, ___U3CNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CNameU3Ek__BackingField_0() const { return ___U3CNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_0() { return &___U3CNameU3Ek__BackingField_0; }
	inline void set_U3CNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USEDBYNATIVECODEATTRIBUTE_T923F9A140847AF2F193AD1AB33143B8774797912_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T14917ACC6E0C738A985023D2ECB9D4BAC153CB5C_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T14917ACC6E0C738A985023D2ECB9D4BAC153CB5C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t14917ACC6E0C738A985023D2ECB9D4BAC153CB5C  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t14917ACC6E0C738A985023D2ECB9D4BAC153CB5C_StaticFields
{
public:
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D120 <PrivateImplementationDetails>::0AA802CD6847EB893FE786B5EA5168B2FDCD7B93
	__StaticArrayInitTypeSizeU3D120_t83E233123DD538AA6101D1CB5AE4F5DC639EBC9D  ___0AA802CD6847EB893FE786B5EA5168B2FDCD7B93_0;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D256 <PrivateImplementationDetails>::0C4110BC17D746F018F47B49E0EB0D6590F69939
	__StaticArrayInitTypeSizeU3D256_tCCCC326240ED0A827344379DD68205BF9340FF47  ___0C4110BC17D746F018F47B49E0EB0D6590F69939_1;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D1024 <PrivateImplementationDetails>::20733E1283D873EBE47133A95C233E11B76F5F11
	__StaticArrayInitTypeSizeU3D1024_t336389AC57307AEC77791F09CF655CD3EF917B7C  ___20733E1283D873EBE47133A95C233E11B76F5F11_2;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D1024 <PrivateImplementationDetails>::21F4CBF8283FF1CAEB4A39316A97FC1D6DF1D35E
	__StaticArrayInitTypeSizeU3D1024_t336389AC57307AEC77791F09CF655CD3EF917B7C  ___21F4CBF8283FF1CAEB4A39316A97FC1D6DF1D35E_3;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D1024 <PrivateImplementationDetails>::23DFDCA6F045D4257BF5AC8CB1CF2EFADAFE9B94
	__StaticArrayInitTypeSizeU3D1024_t336389AC57307AEC77791F09CF655CD3EF917B7C  ___23DFDCA6F045D4257BF5AC8CB1CF2EFADAFE9B94_4;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D1024 <PrivateImplementationDetails>::30A0358B25B1372DD598BB4B1AC56AD6B8F08A47
	__StaticArrayInitTypeSizeU3D1024_t336389AC57307AEC77791F09CF655CD3EF917B7C  ___30A0358B25B1372DD598BB4B1AC56AD6B8F08A47_5;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D1024 <PrivateImplementationDetails>::5B5DF5A459E902D96F7DB0FB235A25346CA85C5D
	__StaticArrayInitTypeSizeU3D1024_t336389AC57307AEC77791F09CF655CD3EF917B7C  ___5B5DF5A459E902D96F7DB0FB235A25346CA85C5D_6;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D1024 <PrivateImplementationDetails>::5BE411F1438EAEF33726D855E99011D5FECDDD4E
	__StaticArrayInitTypeSizeU3D1024_t336389AC57307AEC77791F09CF655CD3EF917B7C  ___5BE411F1438EAEF33726D855E99011D5FECDDD4E_7;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D256 <PrivateImplementationDetails>::8F22C9ECE1331718CBD268A9BBFD2F5E451441E3
	__StaticArrayInitTypeSizeU3D256_tCCCC326240ED0A827344379DD68205BF9340FF47  ___8F22C9ECE1331718CBD268A9BBFD2F5E451441E3_8;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D1024 <PrivateImplementationDetails>::A02DD1D8604EA8EC2D2BDA717A93A4EE85F13E53
	__StaticArrayInitTypeSizeU3D1024_t336389AC57307AEC77791F09CF655CD3EF917B7C  ___A02DD1D8604EA8EC2D2BDA717A93A4EE85F13E53_9;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D1024 <PrivateImplementationDetails>::AE2F76ECEF8B08F0BC7EA95DCFE945E1727927C9
	__StaticArrayInitTypeSizeU3D1024_t336389AC57307AEC77791F09CF655CD3EF917B7C  ___AE2F76ECEF8B08F0BC7EA95DCFE945E1727927C9_10;

public:
	inline static int32_t get_offset_of_U30AA802CD6847EB893FE786B5EA5168B2FDCD7B93_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t14917ACC6E0C738A985023D2ECB9D4BAC153CB5C_StaticFields, ___0AA802CD6847EB893FE786B5EA5168B2FDCD7B93_0)); }
	inline __StaticArrayInitTypeSizeU3D120_t83E233123DD538AA6101D1CB5AE4F5DC639EBC9D  get_U30AA802CD6847EB893FE786B5EA5168B2FDCD7B93_0() const { return ___0AA802CD6847EB893FE786B5EA5168B2FDCD7B93_0; }
	inline __StaticArrayInitTypeSizeU3D120_t83E233123DD538AA6101D1CB5AE4F5DC639EBC9D * get_address_of_U30AA802CD6847EB893FE786B5EA5168B2FDCD7B93_0() { return &___0AA802CD6847EB893FE786B5EA5168B2FDCD7B93_0; }
	inline void set_U30AA802CD6847EB893FE786B5EA5168B2FDCD7B93_0(__StaticArrayInitTypeSizeU3D120_t83E233123DD538AA6101D1CB5AE4F5DC639EBC9D  value)
	{
		___0AA802CD6847EB893FE786B5EA5168B2FDCD7B93_0 = value;
	}

	inline static int32_t get_offset_of_U30C4110BC17D746F018F47B49E0EB0D6590F69939_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t14917ACC6E0C738A985023D2ECB9D4BAC153CB5C_StaticFields, ___0C4110BC17D746F018F47B49E0EB0D6590F69939_1)); }
	inline __StaticArrayInitTypeSizeU3D256_tCCCC326240ED0A827344379DD68205BF9340FF47  get_U30C4110BC17D746F018F47B49E0EB0D6590F69939_1() const { return ___0C4110BC17D746F018F47B49E0EB0D6590F69939_1; }
	inline __StaticArrayInitTypeSizeU3D256_tCCCC326240ED0A827344379DD68205BF9340FF47 * get_address_of_U30C4110BC17D746F018F47B49E0EB0D6590F69939_1() { return &___0C4110BC17D746F018F47B49E0EB0D6590F69939_1; }
	inline void set_U30C4110BC17D746F018F47B49E0EB0D6590F69939_1(__StaticArrayInitTypeSizeU3D256_tCCCC326240ED0A827344379DD68205BF9340FF47  value)
	{
		___0C4110BC17D746F018F47B49E0EB0D6590F69939_1 = value;
	}

	inline static int32_t get_offset_of_U320733E1283D873EBE47133A95C233E11B76F5F11_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t14917ACC6E0C738A985023D2ECB9D4BAC153CB5C_StaticFields, ___20733E1283D873EBE47133A95C233E11B76F5F11_2)); }
	inline __StaticArrayInitTypeSizeU3D1024_t336389AC57307AEC77791F09CF655CD3EF917B7C  get_U320733E1283D873EBE47133A95C233E11B76F5F11_2() const { return ___20733E1283D873EBE47133A95C233E11B76F5F11_2; }
	inline __StaticArrayInitTypeSizeU3D1024_t336389AC57307AEC77791F09CF655CD3EF917B7C * get_address_of_U320733E1283D873EBE47133A95C233E11B76F5F11_2() { return &___20733E1283D873EBE47133A95C233E11B76F5F11_2; }
	inline void set_U320733E1283D873EBE47133A95C233E11B76F5F11_2(__StaticArrayInitTypeSizeU3D1024_t336389AC57307AEC77791F09CF655CD3EF917B7C  value)
	{
		___20733E1283D873EBE47133A95C233E11B76F5F11_2 = value;
	}

	inline static int32_t get_offset_of_U321F4CBF8283FF1CAEB4A39316A97FC1D6DF1D35E_3() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t14917ACC6E0C738A985023D2ECB9D4BAC153CB5C_StaticFields, ___21F4CBF8283FF1CAEB4A39316A97FC1D6DF1D35E_3)); }
	inline __StaticArrayInitTypeSizeU3D1024_t336389AC57307AEC77791F09CF655CD3EF917B7C  get_U321F4CBF8283FF1CAEB4A39316A97FC1D6DF1D35E_3() const { return ___21F4CBF8283FF1CAEB4A39316A97FC1D6DF1D35E_3; }
	inline __StaticArrayInitTypeSizeU3D1024_t336389AC57307AEC77791F09CF655CD3EF917B7C * get_address_of_U321F4CBF8283FF1CAEB4A39316A97FC1D6DF1D35E_3() { return &___21F4CBF8283FF1CAEB4A39316A97FC1D6DF1D35E_3; }
	inline void set_U321F4CBF8283FF1CAEB4A39316A97FC1D6DF1D35E_3(__StaticArrayInitTypeSizeU3D1024_t336389AC57307AEC77791F09CF655CD3EF917B7C  value)
	{
		___21F4CBF8283FF1CAEB4A39316A97FC1D6DF1D35E_3 = value;
	}

	inline static int32_t get_offset_of_U323DFDCA6F045D4257BF5AC8CB1CF2EFADAFE9B94_4() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t14917ACC6E0C738A985023D2ECB9D4BAC153CB5C_StaticFields, ___23DFDCA6F045D4257BF5AC8CB1CF2EFADAFE9B94_4)); }
	inline __StaticArrayInitTypeSizeU3D1024_t336389AC57307AEC77791F09CF655CD3EF917B7C  get_U323DFDCA6F045D4257BF5AC8CB1CF2EFADAFE9B94_4() const { return ___23DFDCA6F045D4257BF5AC8CB1CF2EFADAFE9B94_4; }
	inline __StaticArrayInitTypeSizeU3D1024_t336389AC57307AEC77791F09CF655CD3EF917B7C * get_address_of_U323DFDCA6F045D4257BF5AC8CB1CF2EFADAFE9B94_4() { return &___23DFDCA6F045D4257BF5AC8CB1CF2EFADAFE9B94_4; }
	inline void set_U323DFDCA6F045D4257BF5AC8CB1CF2EFADAFE9B94_4(__StaticArrayInitTypeSizeU3D1024_t336389AC57307AEC77791F09CF655CD3EF917B7C  value)
	{
		___23DFDCA6F045D4257BF5AC8CB1CF2EFADAFE9B94_4 = value;
	}

	inline static int32_t get_offset_of_U330A0358B25B1372DD598BB4B1AC56AD6B8F08A47_5() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t14917ACC6E0C738A985023D2ECB9D4BAC153CB5C_StaticFields, ___30A0358B25B1372DD598BB4B1AC56AD6B8F08A47_5)); }
	inline __StaticArrayInitTypeSizeU3D1024_t336389AC57307AEC77791F09CF655CD3EF917B7C  get_U330A0358B25B1372DD598BB4B1AC56AD6B8F08A47_5() const { return ___30A0358B25B1372DD598BB4B1AC56AD6B8F08A47_5; }
	inline __StaticArrayInitTypeSizeU3D1024_t336389AC57307AEC77791F09CF655CD3EF917B7C * get_address_of_U330A0358B25B1372DD598BB4B1AC56AD6B8F08A47_5() { return &___30A0358B25B1372DD598BB4B1AC56AD6B8F08A47_5; }
	inline void set_U330A0358B25B1372DD598BB4B1AC56AD6B8F08A47_5(__StaticArrayInitTypeSizeU3D1024_t336389AC57307AEC77791F09CF655CD3EF917B7C  value)
	{
		___30A0358B25B1372DD598BB4B1AC56AD6B8F08A47_5 = value;
	}

	inline static int32_t get_offset_of_U35B5DF5A459E902D96F7DB0FB235A25346CA85C5D_6() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t14917ACC6E0C738A985023D2ECB9D4BAC153CB5C_StaticFields, ___5B5DF5A459E902D96F7DB0FB235A25346CA85C5D_6)); }
	inline __StaticArrayInitTypeSizeU3D1024_t336389AC57307AEC77791F09CF655CD3EF917B7C  get_U35B5DF5A459E902D96F7DB0FB235A25346CA85C5D_6() const { return ___5B5DF5A459E902D96F7DB0FB235A25346CA85C5D_6; }
	inline __StaticArrayInitTypeSizeU3D1024_t336389AC57307AEC77791F09CF655CD3EF917B7C * get_address_of_U35B5DF5A459E902D96F7DB0FB235A25346CA85C5D_6() { return &___5B5DF5A459E902D96F7DB0FB235A25346CA85C5D_6; }
	inline void set_U35B5DF5A459E902D96F7DB0FB235A25346CA85C5D_6(__StaticArrayInitTypeSizeU3D1024_t336389AC57307AEC77791F09CF655CD3EF917B7C  value)
	{
		___5B5DF5A459E902D96F7DB0FB235A25346CA85C5D_6 = value;
	}

	inline static int32_t get_offset_of_U35BE411F1438EAEF33726D855E99011D5FECDDD4E_7() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t14917ACC6E0C738A985023D2ECB9D4BAC153CB5C_StaticFields, ___5BE411F1438EAEF33726D855E99011D5FECDDD4E_7)); }
	inline __StaticArrayInitTypeSizeU3D1024_t336389AC57307AEC77791F09CF655CD3EF917B7C  get_U35BE411F1438EAEF33726D855E99011D5FECDDD4E_7() const { return ___5BE411F1438EAEF33726D855E99011D5FECDDD4E_7; }
	inline __StaticArrayInitTypeSizeU3D1024_t336389AC57307AEC77791F09CF655CD3EF917B7C * get_address_of_U35BE411F1438EAEF33726D855E99011D5FECDDD4E_7() { return &___5BE411F1438EAEF33726D855E99011D5FECDDD4E_7; }
	inline void set_U35BE411F1438EAEF33726D855E99011D5FECDDD4E_7(__StaticArrayInitTypeSizeU3D1024_t336389AC57307AEC77791F09CF655CD3EF917B7C  value)
	{
		___5BE411F1438EAEF33726D855E99011D5FECDDD4E_7 = value;
	}

	inline static int32_t get_offset_of_U38F22C9ECE1331718CBD268A9BBFD2F5E451441E3_8() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t14917ACC6E0C738A985023D2ECB9D4BAC153CB5C_StaticFields, ___8F22C9ECE1331718CBD268A9BBFD2F5E451441E3_8)); }
	inline __StaticArrayInitTypeSizeU3D256_tCCCC326240ED0A827344379DD68205BF9340FF47  get_U38F22C9ECE1331718CBD268A9BBFD2F5E451441E3_8() const { return ___8F22C9ECE1331718CBD268A9BBFD2F5E451441E3_8; }
	inline __StaticArrayInitTypeSizeU3D256_tCCCC326240ED0A827344379DD68205BF9340FF47 * get_address_of_U38F22C9ECE1331718CBD268A9BBFD2F5E451441E3_8() { return &___8F22C9ECE1331718CBD268A9BBFD2F5E451441E3_8; }
	inline void set_U38F22C9ECE1331718CBD268A9BBFD2F5E451441E3_8(__StaticArrayInitTypeSizeU3D256_tCCCC326240ED0A827344379DD68205BF9340FF47  value)
	{
		___8F22C9ECE1331718CBD268A9BBFD2F5E451441E3_8 = value;
	}

	inline static int32_t get_offset_of_A02DD1D8604EA8EC2D2BDA717A93A4EE85F13E53_9() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t14917ACC6E0C738A985023D2ECB9D4BAC153CB5C_StaticFields, ___A02DD1D8604EA8EC2D2BDA717A93A4EE85F13E53_9)); }
	inline __StaticArrayInitTypeSizeU3D1024_t336389AC57307AEC77791F09CF655CD3EF917B7C  get_A02DD1D8604EA8EC2D2BDA717A93A4EE85F13E53_9() const { return ___A02DD1D8604EA8EC2D2BDA717A93A4EE85F13E53_9; }
	inline __StaticArrayInitTypeSizeU3D1024_t336389AC57307AEC77791F09CF655CD3EF917B7C * get_address_of_A02DD1D8604EA8EC2D2BDA717A93A4EE85F13E53_9() { return &___A02DD1D8604EA8EC2D2BDA717A93A4EE85F13E53_9; }
	inline void set_A02DD1D8604EA8EC2D2BDA717A93A4EE85F13E53_9(__StaticArrayInitTypeSizeU3D1024_t336389AC57307AEC77791F09CF655CD3EF917B7C  value)
	{
		___A02DD1D8604EA8EC2D2BDA717A93A4EE85F13E53_9 = value;
	}

	inline static int32_t get_offset_of_AE2F76ECEF8B08F0BC7EA95DCFE945E1727927C9_10() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t14917ACC6E0C738A985023D2ECB9D4BAC153CB5C_StaticFields, ___AE2F76ECEF8B08F0BC7EA95DCFE945E1727927C9_10)); }
	inline __StaticArrayInitTypeSizeU3D1024_t336389AC57307AEC77791F09CF655CD3EF917B7C  get_AE2F76ECEF8B08F0BC7EA95DCFE945E1727927C9_10() const { return ___AE2F76ECEF8B08F0BC7EA95DCFE945E1727927C9_10; }
	inline __StaticArrayInitTypeSizeU3D1024_t336389AC57307AEC77791F09CF655CD3EF917B7C * get_address_of_AE2F76ECEF8B08F0BC7EA95DCFE945E1727927C9_10() { return &___AE2F76ECEF8B08F0BC7EA95DCFE945E1727927C9_10; }
	inline void set_AE2F76ECEF8B08F0BC7EA95DCFE945E1727927C9_10(__StaticArrayInitTypeSizeU3D1024_t336389AC57307AEC77791F09CF655CD3EF917B7C  value)
	{
		___AE2F76ECEF8B08F0BC7EA95DCFE945E1727927C9_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T14917ACC6E0C738A985023D2ECB9D4BAC153CB5C_H
#ifndef CASTINSTRUCTIONNOT_T62341A20143F79E1E73E12DC86CA05B2EFFB953F_H
#define CASTINSTRUCTIONNOT_T62341A20143F79E1E73E12DC86CA05B2EFFB953F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.CastInstruction_CastInstructionNoT
struct  CastInstructionNoT_t62341A20143F79E1E73E12DC86CA05B2EFFB953F  : public CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC
{
public:
	// System.Type System.Linq.Expressions.Interpreter.CastInstruction_CastInstructionNoT::_t
	Type_t * ____t_15;

public:
	inline static int32_t get_offset_of__t_15() { return static_cast<int32_t>(offsetof(CastInstructionNoT_t62341A20143F79E1E73E12DC86CA05B2EFFB953F, ____t_15)); }
	inline Type_t * get__t_15() const { return ____t_15; }
	inline Type_t ** get_address_of__t_15() { return &____t_15; }
	inline void set__t_15(Type_t * value)
	{
		____t_15 = value;
		Il2CppCodeGenWriteBarrier((&____t_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CASTINSTRUCTIONNOT_T62341A20143F79E1E73E12DC86CA05B2EFFB953F_H
#ifndef CASTREFERENCETOENUMINSTRUCTION_T00D3025D526CCB80A0AC143B9FE9FCBCF5146FD3_H
#define CASTREFERENCETOENUMINSTRUCTION_T00D3025D526CCB80A0AC143B9FE9FCBCF5146FD3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.CastReferenceToEnumInstruction
struct  CastReferenceToEnumInstruction_t00D3025D526CCB80A0AC143B9FE9FCBCF5146FD3  : public CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC
{
public:
	// System.Type System.Linq.Expressions.Interpreter.CastReferenceToEnumInstruction::_t
	Type_t * ____t_15;

public:
	inline static int32_t get_offset_of__t_15() { return static_cast<int32_t>(offsetof(CastReferenceToEnumInstruction_t00D3025D526CCB80A0AC143B9FE9FCBCF5146FD3, ____t_15)); }
	inline Type_t * get__t_15() const { return ____t_15; }
	inline Type_t ** get_address_of__t_15() { return &____t_15; }
	inline void set__t_15(Type_t * value)
	{
		____t_15 = value;
		Il2CppCodeGenWriteBarrier((&____t_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CASTREFERENCETOENUMINSTRUCTION_T00D3025D526CCB80A0AC143B9FE9FCBCF5146FD3_H
#ifndef CASTTOENUMINSTRUCTION_T0B2C6839A4237E8483BA3BFE32E258949D84E6C9_H
#define CASTTOENUMINSTRUCTION_T0B2C6839A4237E8483BA3BFE32E258949D84E6C9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.CastToEnumInstruction
struct  CastToEnumInstruction_t0B2C6839A4237E8483BA3BFE32E258949D84E6C9  : public CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC
{
public:
	// System.Type System.Linq.Expressions.Interpreter.CastToEnumInstruction::_t
	Type_t * ____t_15;

public:
	inline static int32_t get_offset_of__t_15() { return static_cast<int32_t>(offsetof(CastToEnumInstruction_t0B2C6839A4237E8483BA3BFE32E258949D84E6C9, ____t_15)); }
	inline Type_t * get__t_15() const { return ____t_15; }
	inline Type_t ** get_address_of__t_15() { return &____t_15; }
	inline void set__t_15(Type_t * value)
	{
		____t_15 = value;
		Il2CppCodeGenWriteBarrier((&____t_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CASTTOENUMINSTRUCTION_T0B2C6839A4237E8483BA3BFE32E258949D84E6C9_H
#ifndef EQUALSCLASS_TFDAE59D269E7605CA0801DA0EDD418A8697CED40_H
#define EQUALSCLASS_TFDAE59D269E7605CA0801DA0EDD418A8697CED40_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.NullableMethodCallInstruction_EqualsClass
struct  EqualsClass_tFDAE59D269E7605CA0801DA0EDD418A8697CED40  : public NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALSCLASS_TFDAE59D269E7605CA0801DA0EDD418A8697CED40_H
#ifndef GETHASHCODECLASS_T28E38FB0F34088B773CA466C021B6958F21BF4E4_H
#define GETHASHCODECLASS_T28E38FB0F34088B773CA466C021B6958F21BF4E4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.NullableMethodCallInstruction_GetHashCodeClass
struct  GetHashCodeClass_t28E38FB0F34088B773CA466C021B6958F21BF4E4  : public NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETHASHCODECLASS_T28E38FB0F34088B773CA466C021B6958F21BF4E4_H
#ifndef GETVALUE_T7E9842D3FCF7CC597E92DC0FC1A17E978BB02C46_H
#define GETVALUE_T7E9842D3FCF7CC597E92DC0FC1A17E978BB02C46_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.NullableMethodCallInstruction_GetValue
struct  GetValue_t7E9842D3FCF7CC597E92DC0FC1A17E978BB02C46  : public NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETVALUE_T7E9842D3FCF7CC597E92DC0FC1A17E978BB02C46_H
#ifndef GETVALUEORDEFAULT_TEE5791D009B0981C214341396175F911C5823575_H
#define GETVALUEORDEFAULT_TEE5791D009B0981C214341396175F911C5823575_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.NullableMethodCallInstruction_GetValueOrDefault
struct  GetValueOrDefault_tEE5791D009B0981C214341396175F911C5823575  : public NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB
{
public:
	// System.Type System.Linq.Expressions.Interpreter.NullableMethodCallInstruction_GetValueOrDefault::defaultValueType
	Type_t * ___defaultValueType_6;

public:
	inline static int32_t get_offset_of_defaultValueType_6() { return static_cast<int32_t>(offsetof(GetValueOrDefault_tEE5791D009B0981C214341396175F911C5823575, ___defaultValueType_6)); }
	inline Type_t * get_defaultValueType_6() const { return ___defaultValueType_6; }
	inline Type_t ** get_address_of_defaultValueType_6() { return &___defaultValueType_6; }
	inline void set_defaultValueType_6(Type_t * value)
	{
		___defaultValueType_6 = value;
		Il2CppCodeGenWriteBarrier((&___defaultValueType_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETVALUEORDEFAULT_TEE5791D009B0981C214341396175F911C5823575_H
#ifndef GETVALUEORDEFAULT1_TC8BCC21B8BB18D7FFBB5A1ADA7D7237E1A7142ED_H
#define GETVALUEORDEFAULT1_TC8BCC21B8BB18D7FFBB5A1ADA7D7237E1A7142ED_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.NullableMethodCallInstruction_GetValueOrDefault1
struct  GetValueOrDefault1_tC8BCC21B8BB18D7FFBB5A1ADA7D7237E1A7142ED  : public NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETVALUEORDEFAULT1_TC8BCC21B8BB18D7FFBB5A1ADA7D7237E1A7142ED_H
#ifndef HASVALUE_T41E8E385119E2017CD420D5625324106BE6B10FE_H
#define HASVALUE_T41E8E385119E2017CD420D5625324106BE6B10FE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.NullableMethodCallInstruction_HasValue
struct  HasValue_t41E8E385119E2017CD420D5625324106BE6B10FE  : public NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HASVALUE_T41E8E385119E2017CD420D5625324106BE6B10FE_H
#ifndef TOSTRINGCLASS_TBAABC22667C2755D0BC05C445B7102D611ECAFBB_H
#define TOSTRINGCLASS_TBAABC22667C2755D0BC05C445B7102D611ECAFBB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.NullableMethodCallInstruction_ToStringClass
struct  ToStringClass_tBAABC22667C2755D0BC05C445B7102D611ECAFBB  : public NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOSTRINGCLASS_TBAABC22667C2755D0BC05C445B7102D611ECAFBB_H
#ifndef ORBOOLEAN_T1039738749678468737C117FDB8C1BFE7485CAFB_H
#define ORBOOLEAN_T1039738749678468737C117FDB8C1BFE7485CAFB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.OrInstruction_OrBoolean
struct  OrBoolean_t1039738749678468737C117FDB8C1BFE7485CAFB  : public OrInstruction_t18E2D320D39C7EB49874032BF1A037C6F4249625
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ORBOOLEAN_T1039738749678468737C117FDB8C1BFE7485CAFB_H
#ifndef ORBYTE_TBECB369C539229929352374B158FA67F9123D176_H
#define ORBYTE_TBECB369C539229929352374B158FA67F9123D176_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.OrInstruction_OrByte
struct  OrByte_tBECB369C539229929352374B158FA67F9123D176  : public OrInstruction_t18E2D320D39C7EB49874032BF1A037C6F4249625
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ORBYTE_TBECB369C539229929352374B158FA67F9123D176_H
#ifndef ORINT16_T6ECC408BAC43953970832D4A255A664C3253452E_H
#define ORINT16_T6ECC408BAC43953970832D4A255A664C3253452E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.OrInstruction_OrInt16
struct  OrInt16_t6ECC408BAC43953970832D4A255A664C3253452E  : public OrInstruction_t18E2D320D39C7EB49874032BF1A037C6F4249625
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ORINT16_T6ECC408BAC43953970832D4A255A664C3253452E_H
#ifndef ORINT32_T0745C69A182B7F3A53A3F033EDC82A22977C5936_H
#define ORINT32_T0745C69A182B7F3A53A3F033EDC82A22977C5936_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.OrInstruction_OrInt32
struct  OrInt32_t0745C69A182B7F3A53A3F033EDC82A22977C5936  : public OrInstruction_t18E2D320D39C7EB49874032BF1A037C6F4249625
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ORINT32_T0745C69A182B7F3A53A3F033EDC82A22977C5936_H
#ifndef ORINT64_TBD5F3BD969D747DB860EBE6363AA36C262243ED9_H
#define ORINT64_TBD5F3BD969D747DB860EBE6363AA36C262243ED9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.OrInstruction_OrInt64
struct  OrInt64_tBD5F3BD969D747DB860EBE6363AA36C262243ED9  : public OrInstruction_t18E2D320D39C7EB49874032BF1A037C6F4249625
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ORINT64_TBD5F3BD969D747DB860EBE6363AA36C262243ED9_H
#ifndef ORSBYTE_T74AAF4C0057FC7CEFF6688AC4916899C0F63C994_H
#define ORSBYTE_T74AAF4C0057FC7CEFF6688AC4916899C0F63C994_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.OrInstruction_OrSByte
struct  OrSByte_t74AAF4C0057FC7CEFF6688AC4916899C0F63C994  : public OrInstruction_t18E2D320D39C7EB49874032BF1A037C6F4249625
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ORSBYTE_T74AAF4C0057FC7CEFF6688AC4916899C0F63C994_H
#ifndef ORUINT16_TD70158A2A4B7497DA50C420AF7EF7A39A9C03E0B_H
#define ORUINT16_TD70158A2A4B7497DA50C420AF7EF7A39A9C03E0B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.OrInstruction_OrUInt16
struct  OrUInt16_tD70158A2A4B7497DA50C420AF7EF7A39A9C03E0B  : public OrInstruction_t18E2D320D39C7EB49874032BF1A037C6F4249625
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ORUINT16_TD70158A2A4B7497DA50C420AF7EF7A39A9C03E0B_H
#ifndef ORUINT32_TFEEE3EE6CD3932444285006DCE158ECB468177F4_H
#define ORUINT32_TFEEE3EE6CD3932444285006DCE158ECB468177F4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.OrInstruction_OrUInt32
struct  OrUInt32_tFEEE3EE6CD3932444285006DCE158ECB468177F4  : public OrInstruction_t18E2D320D39C7EB49874032BF1A037C6F4249625
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ORUINT32_TFEEE3EE6CD3932444285006DCE158ECB468177F4_H
#ifndef ORUINT64_TB8D2DBC8BE28EA41C6497F245D0662BC43F26CE1_H
#define ORUINT64_TB8D2DBC8BE28EA41C6497F245D0662BC43F26CE1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.OrInstruction_OrUInt64
struct  OrUInt64_tB8D2DBC8BE28EA41C6497F245D0662BC43F26CE1  : public OrInstruction_t18E2D320D39C7EB49874032BF1A037C6F4249625
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ORUINT64_TB8D2DBC8BE28EA41C6497F245D0662BC43F26CE1_H
#ifndef RIGHTSHIFTBYTE_T84B22E9B669FFDBAD4FDFDCBA3673A0BFC2F1534_H
#define RIGHTSHIFTBYTE_T84B22E9B669FFDBAD4FDFDCBA3673A0BFC2F1534_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.RightShiftInstruction_RightShiftByte
struct  RightShiftByte_t84B22E9B669FFDBAD4FDFDCBA3673A0BFC2F1534  : public RightShiftInstruction_t5BCE4FCF5332FE9B144DA762AE3E7B9E39BFD927
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RIGHTSHIFTBYTE_T84B22E9B669FFDBAD4FDFDCBA3673A0BFC2F1534_H
#ifndef RIGHTSHIFTINT16_T6C93BD08D424C8C54850BFC98CED6E3567A447C4_H
#define RIGHTSHIFTINT16_T6C93BD08D424C8C54850BFC98CED6E3567A447C4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.RightShiftInstruction_RightShiftInt16
struct  RightShiftInt16_t6C93BD08D424C8C54850BFC98CED6E3567A447C4  : public RightShiftInstruction_t5BCE4FCF5332FE9B144DA762AE3E7B9E39BFD927
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RIGHTSHIFTINT16_T6C93BD08D424C8C54850BFC98CED6E3567A447C4_H
#ifndef RIGHTSHIFTINT32_T053C723574F9CE85AFCF435A4C070FB9A0978C5D_H
#define RIGHTSHIFTINT32_T053C723574F9CE85AFCF435A4C070FB9A0978C5D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.RightShiftInstruction_RightShiftInt32
struct  RightShiftInt32_t053C723574F9CE85AFCF435A4C070FB9A0978C5D  : public RightShiftInstruction_t5BCE4FCF5332FE9B144DA762AE3E7B9E39BFD927
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RIGHTSHIFTINT32_T053C723574F9CE85AFCF435A4C070FB9A0978C5D_H
#ifndef RIGHTSHIFTINT64_TD8DB36379DB16D0170A179FAFD139988E24B5FB9_H
#define RIGHTSHIFTINT64_TD8DB36379DB16D0170A179FAFD139988E24B5FB9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.RightShiftInstruction_RightShiftInt64
struct  RightShiftInt64_tD8DB36379DB16D0170A179FAFD139988E24B5FB9  : public RightShiftInstruction_t5BCE4FCF5332FE9B144DA762AE3E7B9E39BFD927
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RIGHTSHIFTINT64_TD8DB36379DB16D0170A179FAFD139988E24B5FB9_H
#ifndef RIGHTSHIFTSBYTE_T086738887A4343714374FE4D55A6C35D800AF3E8_H
#define RIGHTSHIFTSBYTE_T086738887A4343714374FE4D55A6C35D800AF3E8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.RightShiftInstruction_RightShiftSByte
struct  RightShiftSByte_t086738887A4343714374FE4D55A6C35D800AF3E8  : public RightShiftInstruction_t5BCE4FCF5332FE9B144DA762AE3E7B9E39BFD927
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RIGHTSHIFTSBYTE_T086738887A4343714374FE4D55A6C35D800AF3E8_H
#ifndef RIGHTSHIFTUINT16_T87ECF24BB0B9A1274076BFA37DA497CE0C28A412_H
#define RIGHTSHIFTUINT16_T87ECF24BB0B9A1274076BFA37DA497CE0C28A412_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.RightShiftInstruction_RightShiftUInt16
struct  RightShiftUInt16_t87ECF24BB0B9A1274076BFA37DA497CE0C28A412  : public RightShiftInstruction_t5BCE4FCF5332FE9B144DA762AE3E7B9E39BFD927
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RIGHTSHIFTUINT16_T87ECF24BB0B9A1274076BFA37DA497CE0C28A412_H
#ifndef RIGHTSHIFTUINT32_T11C80109EF04C50C4101929B224EA7B0EAF235D2_H
#define RIGHTSHIFTUINT32_T11C80109EF04C50C4101929B224EA7B0EAF235D2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.RightShiftInstruction_RightShiftUInt32
struct  RightShiftUInt32_t11C80109EF04C50C4101929B224EA7B0EAF235D2  : public RightShiftInstruction_t5BCE4FCF5332FE9B144DA762AE3E7B9E39BFD927
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RIGHTSHIFTUINT32_T11C80109EF04C50C4101929B224EA7B0EAF235D2_H
#ifndef RIGHTSHIFTUINT64_TC8842CC358181C4115BA9BE4CD965CA444830FBD_H
#define RIGHTSHIFTUINT64_TC8842CC358181C4115BA9BE4CD965CA444830FBD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.RightShiftInstruction_RightShiftUInt64
struct  RightShiftUInt64_tC8842CC358181C4115BA9BE4CD965CA444830FBD  : public RightShiftInstruction_t5BCE4FCF5332FE9B144DA762AE3E7B9E39BFD927
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RIGHTSHIFTUINT64_TC8842CC358181C4115BA9BE4CD965CA444830FBD_H
#ifndef SUBDOUBLE_TB2C29B9EAD9C37321CFD15C141A7ADBEB44C87F1_H
#define SUBDOUBLE_TB2C29B9EAD9C37321CFD15C141A7ADBEB44C87F1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.SubInstruction_SubDouble
struct  SubDouble_tB2C29B9EAD9C37321CFD15C141A7ADBEB44C87F1  : public SubInstruction_t734617B532D4FE50053C3CC64466A2599DEFD3F6
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBDOUBLE_TB2C29B9EAD9C37321CFD15C141A7ADBEB44C87F1_H
#ifndef SUBINT16_T92DD86AE94B50DE3BE5D3E2EDD5E6DC7FA8EB345_H
#define SUBINT16_T92DD86AE94B50DE3BE5D3E2EDD5E6DC7FA8EB345_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.SubInstruction_SubInt16
struct  SubInt16_t92DD86AE94B50DE3BE5D3E2EDD5E6DC7FA8EB345  : public SubInstruction_t734617B532D4FE50053C3CC64466A2599DEFD3F6
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBINT16_T92DD86AE94B50DE3BE5D3E2EDD5E6DC7FA8EB345_H
#ifndef SUBINT32_T95C12EAB5EB227515DABCB151A9C6ADA102B688E_H
#define SUBINT32_T95C12EAB5EB227515DABCB151A9C6ADA102B688E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.SubInstruction_SubInt32
struct  SubInt32_t95C12EAB5EB227515DABCB151A9C6ADA102B688E  : public SubInstruction_t734617B532D4FE50053C3CC64466A2599DEFD3F6
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBINT32_T95C12EAB5EB227515DABCB151A9C6ADA102B688E_H
#ifndef SUBINT64_T2FD1CE07CD14C7B5077AD6650EB7A0A000AA2EB0_H
#define SUBINT64_T2FD1CE07CD14C7B5077AD6650EB7A0A000AA2EB0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.SubInstruction_SubInt64
struct  SubInt64_t2FD1CE07CD14C7B5077AD6650EB7A0A000AA2EB0  : public SubInstruction_t734617B532D4FE50053C3CC64466A2599DEFD3F6
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBINT64_T2FD1CE07CD14C7B5077AD6650EB7A0A000AA2EB0_H
#ifndef SUBSINGLE_T700531C30E61E060FE8E30B1B3F29EB7585CFFD7_H
#define SUBSINGLE_T700531C30E61E060FE8E30B1B3F29EB7585CFFD7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.SubInstruction_SubSingle
struct  SubSingle_t700531C30E61E060FE8E30B1B3F29EB7585CFFD7  : public SubInstruction_t734617B532D4FE50053C3CC64466A2599DEFD3F6
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBSINGLE_T700531C30E61E060FE8E30B1B3F29EB7585CFFD7_H
#ifndef SUBUINT16_TD048821DF287E4980F7C7946674CC2B198746656_H
#define SUBUINT16_TD048821DF287E4980F7C7946674CC2B198746656_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.SubInstruction_SubUInt16
struct  SubUInt16_tD048821DF287E4980F7C7946674CC2B198746656  : public SubInstruction_t734617B532D4FE50053C3CC64466A2599DEFD3F6
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBUINT16_TD048821DF287E4980F7C7946674CC2B198746656_H
#ifndef SUBUINT32_TBBCF390E857963D94E338E564300D0AD3CFCAB28_H
#define SUBUINT32_TBBCF390E857963D94E338E564300D0AD3CFCAB28_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.SubInstruction_SubUInt32
struct  SubUInt32_tBBCF390E857963D94E338E564300D0AD3CFCAB28  : public SubInstruction_t734617B532D4FE50053C3CC64466A2599DEFD3F6
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBUINT32_TBBCF390E857963D94E338E564300D0AD3CFCAB28_H
#ifndef SUBUINT64_T8D003A5B96186A8A435EB4EC14D9D116E6D4590B_H
#define SUBUINT64_T8D003A5B96186A8A435EB4EC14D9D116E6D4590B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.SubInstruction_SubUInt64
struct  SubUInt64_t8D003A5B96186A8A435EB4EC14D9D116E6D4590B  : public SubInstruction_t734617B532D4FE50053C3CC64466A2599DEFD3F6
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBUINT64_T8D003A5B96186A8A435EB4EC14D9D116E6D4590B_H
#ifndef SUBOVFINT16_T36FFF7E11B91ACC6CAEE70C3EB9E82F13AA62489_H
#define SUBOVFINT16_T36FFF7E11B91ACC6CAEE70C3EB9E82F13AA62489_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.SubOvfInstruction_SubOvfInt16
struct  SubOvfInt16_t36FFF7E11B91ACC6CAEE70C3EB9E82F13AA62489  : public SubOvfInstruction_tC360B9BB53E1001E5636FD18D7240A1A507216C3
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBOVFINT16_T36FFF7E11B91ACC6CAEE70C3EB9E82F13AA62489_H
#ifndef SUBOVFINT32_T1D3DFA3EC8958835551A37DFF188E6A5AF42450A_H
#define SUBOVFINT32_T1D3DFA3EC8958835551A37DFF188E6A5AF42450A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.SubOvfInstruction_SubOvfInt32
struct  SubOvfInt32_t1D3DFA3EC8958835551A37DFF188E6A5AF42450A  : public SubOvfInstruction_tC360B9BB53E1001E5636FD18D7240A1A507216C3
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBOVFINT32_T1D3DFA3EC8958835551A37DFF188E6A5AF42450A_H
#ifndef SUBOVFINT64_T5BD992E8D4E5730D3BD7348529B0CF08D1A1F499_H
#define SUBOVFINT64_T5BD992E8D4E5730D3BD7348529B0CF08D1A1F499_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.SubOvfInstruction_SubOvfInt64
struct  SubOvfInt64_t5BD992E8D4E5730D3BD7348529B0CF08D1A1F499  : public SubOvfInstruction_tC360B9BB53E1001E5636FD18D7240A1A507216C3
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBOVFINT64_T5BD992E8D4E5730D3BD7348529B0CF08D1A1F499_H
#ifndef SUBOVFUINT16_T8C9521D689F8B7FFC881B9FFBCC90770E145908F_H
#define SUBOVFUINT16_T8C9521D689F8B7FFC881B9FFBCC90770E145908F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.SubOvfInstruction_SubOvfUInt16
struct  SubOvfUInt16_t8C9521D689F8B7FFC881B9FFBCC90770E145908F  : public SubOvfInstruction_tC360B9BB53E1001E5636FD18D7240A1A507216C3
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBOVFUINT16_T8C9521D689F8B7FFC881B9FFBCC90770E145908F_H
#ifndef SUBOVFUINT32_T8077FE8705CE9D497059032FA4900E705A37D4B1_H
#define SUBOVFUINT32_T8077FE8705CE9D497059032FA4900E705A37D4B1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.SubOvfInstruction_SubOvfUInt32
struct  SubOvfUInt32_t8077FE8705CE9D497059032FA4900E705A37D4B1  : public SubOvfInstruction_tC360B9BB53E1001E5636FD18D7240A1A507216C3
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBOVFUINT32_T8077FE8705CE9D497059032FA4900E705A37D4B1_H
#ifndef SUBOVFUINT64_TA6DEA75AC26C5092FEE80671B02C9201193EA87B_H
#define SUBOVFUINT64_TA6DEA75AC26C5092FEE80671B02C9201193EA87B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.SubOvfInstruction_SubOvfUInt64
struct  SubOvfUInt64_tA6DEA75AC26C5092FEE80671B02C9201193EA87B  : public SubOvfInstruction_tC360B9BB53E1001E5636FD18D7240A1A507216C3
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBOVFUINT64_TA6DEA75AC26C5092FEE80671B02C9201193EA87B_H
#ifndef REF_T02288254DB73F4EED20FD86423CF79F84C4D895C_H
#define REF_T02288254DB73F4EED20FD86423CF79F84C4D895C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.CastInstruction_CastInstructionNoT_Ref
struct  Ref_t02288254DB73F4EED20FD86423CF79F84C4D895C  : public CastInstructionNoT_t62341A20143F79E1E73E12DC86CA05B2EFFB953F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REF_T02288254DB73F4EED20FD86423CF79F84C4D895C_H
#ifndef VALUE_T7062A897C92AA41B6F15AD68D8AE96108CC0BD6B_H
#define VALUE_T7062A897C92AA41B6F15AD68D8AE96108CC0BD6B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.CastInstruction_CastInstructionNoT_Value
struct  Value_t7062A897C92AA41B6F15AD68D8AE96108CC0BD6B  : public CastInstructionNoT_t62341A20143F79E1E73E12DC86CA05B2EFFB953F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALUE_T7062A897C92AA41B6F15AD68D8AE96108CC0BD6B_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3900 = { sizeof (OrInstruction_t18E2D320D39C7EB49874032BF1A037C6F4249625), -1, sizeof(OrInstruction_t18E2D320D39C7EB49874032BF1A037C6F4249625_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3900[9] = 
{
	OrInstruction_t18E2D320D39C7EB49874032BF1A037C6F4249625_StaticFields::get_offset_of_s_SByte_0(),
	OrInstruction_t18E2D320D39C7EB49874032BF1A037C6F4249625_StaticFields::get_offset_of_s_Int16_1(),
	OrInstruction_t18E2D320D39C7EB49874032BF1A037C6F4249625_StaticFields::get_offset_of_s_Int32_2(),
	OrInstruction_t18E2D320D39C7EB49874032BF1A037C6F4249625_StaticFields::get_offset_of_s_Int64_3(),
	OrInstruction_t18E2D320D39C7EB49874032BF1A037C6F4249625_StaticFields::get_offset_of_s_Byte_4(),
	OrInstruction_t18E2D320D39C7EB49874032BF1A037C6F4249625_StaticFields::get_offset_of_s_UInt16_5(),
	OrInstruction_t18E2D320D39C7EB49874032BF1A037C6F4249625_StaticFields::get_offset_of_s_UInt32_6(),
	OrInstruction_t18E2D320D39C7EB49874032BF1A037C6F4249625_StaticFields::get_offset_of_s_UInt64_7(),
	OrInstruction_t18E2D320D39C7EB49874032BF1A037C6F4249625_StaticFields::get_offset_of_s_Boolean_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3901 = { sizeof (OrSByte_t74AAF4C0057FC7CEFF6688AC4916899C0F63C994), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3902 = { sizeof (OrInt16_t6ECC408BAC43953970832D4A255A664C3253452E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3903 = { sizeof (OrInt32_t0745C69A182B7F3A53A3F033EDC82A22977C5936), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3904 = { sizeof (OrInt64_tBD5F3BD969D747DB860EBE6363AA36C262243ED9), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3905 = { sizeof (OrByte_tBECB369C539229929352374B158FA67F9123D176), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3906 = { sizeof (OrUInt16_tD70158A2A4B7497DA50C420AF7EF7A39A9C03E0B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3907 = { sizeof (OrUInt32_tFEEE3EE6CD3932444285006DCE158ECB468177F4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3908 = { sizeof (OrUInt64_tB8D2DBC8BE28EA41C6497F245D0662BC43F26CE1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3909 = { sizeof (OrBoolean_t1039738749678468737C117FDB8C1BFE7485CAFB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3910 = { sizeof (RightShiftInstruction_t5BCE4FCF5332FE9B144DA762AE3E7B9E39BFD927), -1, sizeof(RightShiftInstruction_t5BCE4FCF5332FE9B144DA762AE3E7B9E39BFD927_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3910[8] = 
{
	RightShiftInstruction_t5BCE4FCF5332FE9B144DA762AE3E7B9E39BFD927_StaticFields::get_offset_of_s_SByte_0(),
	RightShiftInstruction_t5BCE4FCF5332FE9B144DA762AE3E7B9E39BFD927_StaticFields::get_offset_of_s_Int16_1(),
	RightShiftInstruction_t5BCE4FCF5332FE9B144DA762AE3E7B9E39BFD927_StaticFields::get_offset_of_s_Int32_2(),
	RightShiftInstruction_t5BCE4FCF5332FE9B144DA762AE3E7B9E39BFD927_StaticFields::get_offset_of_s_Int64_3(),
	RightShiftInstruction_t5BCE4FCF5332FE9B144DA762AE3E7B9E39BFD927_StaticFields::get_offset_of_s_Byte_4(),
	RightShiftInstruction_t5BCE4FCF5332FE9B144DA762AE3E7B9E39BFD927_StaticFields::get_offset_of_s_UInt16_5(),
	RightShiftInstruction_t5BCE4FCF5332FE9B144DA762AE3E7B9E39BFD927_StaticFields::get_offset_of_s_UInt32_6(),
	RightShiftInstruction_t5BCE4FCF5332FE9B144DA762AE3E7B9E39BFD927_StaticFields::get_offset_of_s_UInt64_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3911 = { sizeof (RightShiftSByte_t086738887A4343714374FE4D55A6C35D800AF3E8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3912 = { sizeof (RightShiftInt16_t6C93BD08D424C8C54850BFC98CED6E3567A447C4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3913 = { sizeof (RightShiftInt32_t053C723574F9CE85AFCF435A4C070FB9A0978C5D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3914 = { sizeof (RightShiftInt64_tD8DB36379DB16D0170A179FAFD139988E24B5FB9), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3915 = { sizeof (RightShiftByte_t84B22E9B669FFDBAD4FDFDCBA3673A0BFC2F1534), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3916 = { sizeof (RightShiftUInt16_t87ECF24BB0B9A1274076BFA37DA497CE0C28A412), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3917 = { sizeof (RightShiftUInt32_t11C80109EF04C50C4101929B224EA7B0EAF235D2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3918 = { sizeof (RightShiftUInt64_tC8842CC358181C4115BA9BE4CD965CA444830FBD), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3919 = { sizeof (RuntimeVariables_t80F25BDAAAD3795F8687E62A259E14D255AC4E5F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3919[1] = 
{
	RuntimeVariables_t80F25BDAAAD3795F8687E62A259E14D255AC4E5F::get_offset_of__boxes_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3920 = { sizeof (LoadObjectInstruction_t2C8D43685496B24000A7E069FDAEE73D4E72F1A7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3920[1] = 
{
	LoadObjectInstruction_t2C8D43685496B24000A7E069FDAEE73D4E72F1A7::get_offset_of__value_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3921 = { sizeof (LoadCachedObjectInstruction_t70C07C702DAC18B1BB6BE378B9036A440B359D32), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3921[1] = 
{
	LoadCachedObjectInstruction_t70C07C702DAC18B1BB6BE378B9036A440B359D32::get_offset_of__index_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3922 = { sizeof (PopInstruction_tE203CE250FAF056C574B7AAE9D6422EE7A735C52), -1, sizeof(PopInstruction_tE203CE250FAF056C574B7AAE9D6422EE7A735C52_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3922[1] = 
{
	PopInstruction_tE203CE250FAF056C574B7AAE9D6422EE7A735C52_StaticFields::get_offset_of_Instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3923 = { sizeof (DupInstruction_t35C73C83F8421A299FBC38A6C9189E88F7FD2512), -1, sizeof(DupInstruction_t35C73C83F8421A299FBC38A6C9189E88F7FD2512_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3923[1] = 
{
	DupInstruction_t35C73C83F8421A299FBC38A6C9189E88F7FD2512_StaticFields::get_offset_of_Instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3924 = { sizeof (SubInstruction_t734617B532D4FE50053C3CC64466A2599DEFD3F6), -1, sizeof(SubInstruction_t734617B532D4FE50053C3CC64466A2599DEFD3F6_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3924[8] = 
{
	SubInstruction_t734617B532D4FE50053C3CC64466A2599DEFD3F6_StaticFields::get_offset_of_s_Int16_0(),
	SubInstruction_t734617B532D4FE50053C3CC64466A2599DEFD3F6_StaticFields::get_offset_of_s_Int32_1(),
	SubInstruction_t734617B532D4FE50053C3CC64466A2599DEFD3F6_StaticFields::get_offset_of_s_Int64_2(),
	SubInstruction_t734617B532D4FE50053C3CC64466A2599DEFD3F6_StaticFields::get_offset_of_s_UInt16_3(),
	SubInstruction_t734617B532D4FE50053C3CC64466A2599DEFD3F6_StaticFields::get_offset_of_s_UInt32_4(),
	SubInstruction_t734617B532D4FE50053C3CC64466A2599DEFD3F6_StaticFields::get_offset_of_s_UInt64_5(),
	SubInstruction_t734617B532D4FE50053C3CC64466A2599DEFD3F6_StaticFields::get_offset_of_s_Single_6(),
	SubInstruction_t734617B532D4FE50053C3CC64466A2599DEFD3F6_StaticFields::get_offset_of_s_Double_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3925 = { sizeof (SubInt16_t92DD86AE94B50DE3BE5D3E2EDD5E6DC7FA8EB345), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3926 = { sizeof (SubInt32_t95C12EAB5EB227515DABCB151A9C6ADA102B688E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3927 = { sizeof (SubInt64_t2FD1CE07CD14C7B5077AD6650EB7A0A000AA2EB0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3928 = { sizeof (SubUInt16_tD048821DF287E4980F7C7946674CC2B198746656), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3929 = { sizeof (SubUInt32_tBBCF390E857963D94E338E564300D0AD3CFCAB28), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3930 = { sizeof (SubUInt64_t8D003A5B96186A8A435EB4EC14D9D116E6D4590B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3931 = { sizeof (SubSingle_t700531C30E61E060FE8E30B1B3F29EB7585CFFD7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3932 = { sizeof (SubDouble_tB2C29B9EAD9C37321CFD15C141A7ADBEB44C87F1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3933 = { sizeof (SubOvfInstruction_tC360B9BB53E1001E5636FD18D7240A1A507216C3), -1, sizeof(SubOvfInstruction_tC360B9BB53E1001E5636FD18D7240A1A507216C3_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3933[6] = 
{
	SubOvfInstruction_tC360B9BB53E1001E5636FD18D7240A1A507216C3_StaticFields::get_offset_of_s_Int16_0(),
	SubOvfInstruction_tC360B9BB53E1001E5636FD18D7240A1A507216C3_StaticFields::get_offset_of_s_Int32_1(),
	SubOvfInstruction_tC360B9BB53E1001E5636FD18D7240A1A507216C3_StaticFields::get_offset_of_s_Int64_2(),
	SubOvfInstruction_tC360B9BB53E1001E5636FD18D7240A1A507216C3_StaticFields::get_offset_of_s_UInt16_3(),
	SubOvfInstruction_tC360B9BB53E1001E5636FD18D7240A1A507216C3_StaticFields::get_offset_of_s_UInt32_4(),
	SubOvfInstruction_tC360B9BB53E1001E5636FD18D7240A1A507216C3_StaticFields::get_offset_of_s_UInt64_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3934 = { sizeof (SubOvfInt16_t36FFF7E11B91ACC6CAEE70C3EB9E82F13AA62489), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3935 = { sizeof (SubOvfInt32_t1D3DFA3EC8958835551A37DFF188E6A5AF42450A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3936 = { sizeof (SubOvfInt64_t5BD992E8D4E5730D3BD7348529B0CF08D1A1F499), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3937 = { sizeof (SubOvfUInt16_t8C9521D689F8B7FFC881B9FFBCC90770E145908F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3938 = { sizeof (SubOvfUInt32_t8077FE8705CE9D497059032FA4900E705A37D4B1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3939 = { sizeof (SubOvfUInt64_tA6DEA75AC26C5092FEE80671B02C9201193EA87B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3940 = { sizeof (CreateDelegateInstruction_tF6B6DE5E99CEC35C22720C4BA3F2F16602284AB5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3940[1] = 
{
	CreateDelegateInstruction_tF6B6DE5E99CEC35C22720C4BA3F2F16602284AB5::get_offset_of__creator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3941 = { sizeof (TypeIsInstruction_t9F7EF0BB05E8F7A713E452A49E9C1979344032D8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3941[1] = 
{
	TypeIsInstruction_t9F7EF0BB05E8F7A713E452A49E9C1979344032D8::get_offset_of__type_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3942 = { sizeof (TypeAsInstruction_tCF3FC69CAA49AC26C4C9BF96F989F7CDB046134E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3942[1] = 
{
	TypeAsInstruction_tCF3FC69CAA49AC26C4C9BF96F989F7CDB046134E::get_offset_of__type_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3943 = { sizeof (TypeEqualsInstruction_tCF7B3DA0050CA5B84DD1B7A209D9CC66AD81F260), -1, sizeof(TypeEqualsInstruction_tCF7B3DA0050CA5B84DD1B7A209D9CC66AD81F260_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3943[1] = 
{
	TypeEqualsInstruction_tCF7B3DA0050CA5B84DD1B7A209D9CC66AD81F260_StaticFields::get_offset_of_Instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3944 = { sizeof (NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB), -1, sizeof(NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3944[6] = 
{
	NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB_StaticFields::get_offset_of_s_hasValue_0(),
	NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB_StaticFields::get_offset_of_s_value_1(),
	NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB_StaticFields::get_offset_of_s_equals_2(),
	NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB_StaticFields::get_offset_of_s_getHashCode_3(),
	NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB_StaticFields::get_offset_of_s_getValueOrDefault1_4(),
	NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB_StaticFields::get_offset_of_s_toString_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3945 = { sizeof (HasValue_t41E8E385119E2017CD420D5625324106BE6B10FE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3946 = { sizeof (GetValue_t7E9842D3FCF7CC597E92DC0FC1A17E978BB02C46), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3947 = { sizeof (GetValueOrDefault_tEE5791D009B0981C214341396175F911C5823575), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3947[1] = 
{
	GetValueOrDefault_tEE5791D009B0981C214341396175F911C5823575::get_offset_of_defaultValueType_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3948 = { sizeof (GetValueOrDefault1_tC8BCC21B8BB18D7FFBB5A1ADA7D7237E1A7142ED), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3949 = { sizeof (EqualsClass_tFDAE59D269E7605CA0801DA0EDD418A8697CED40), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3950 = { sizeof (ToStringClass_tBAABC22667C2755D0BC05C445B7102D611ECAFBB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3951 = { sizeof (GetHashCodeClass_t28E38FB0F34088B773CA466C021B6958F21BF4E4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3952 = { sizeof (CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC), -1, sizeof(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3952[15] = 
{
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields::get_offset_of_s_Boolean_0(),
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields::get_offset_of_s_Byte_1(),
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields::get_offset_of_s_Char_2(),
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields::get_offset_of_s_DateTime_3(),
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields::get_offset_of_s_Decimal_4(),
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields::get_offset_of_s_Double_5(),
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields::get_offset_of_s_Int16_6(),
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields::get_offset_of_s_Int32_7(),
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields::get_offset_of_s_Int64_8(),
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields::get_offset_of_s_SByte_9(),
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields::get_offset_of_s_Single_10(),
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields::get_offset_of_s_String_11(),
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields::get_offset_of_s_UInt16_12(),
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields::get_offset_of_s_UInt32_13(),
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields::get_offset_of_s_UInt64_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3953 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3954 = { sizeof (CastInstructionNoT_t62341A20143F79E1E73E12DC86CA05B2EFFB953F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3954[1] = 
{
	CastInstructionNoT_t62341A20143F79E1E73E12DC86CA05B2EFFB953F::get_offset_of__t_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3955 = { sizeof (Ref_t02288254DB73F4EED20FD86423CF79F84C4D895C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3956 = { sizeof (Value_t7062A897C92AA41B6F15AD68D8AE96108CC0BD6B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3957 = { sizeof (CastToEnumInstruction_t0B2C6839A4237E8483BA3BFE32E258949D84E6C9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3957[1] = 
{
	CastToEnumInstruction_t0B2C6839A4237E8483BA3BFE32E258949D84E6C9::get_offset_of__t_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3958 = { sizeof (CastReferenceToEnumInstruction_t00D3025D526CCB80A0AC143B9FE9FCBCF5146FD3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3958[1] = 
{
	CastReferenceToEnumInstruction_t00D3025D526CCB80A0AC143B9FE9FCBCF5146FD3::get_offset_of__t_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3959 = { sizeof (QuoteInstruction_tCC0594851C52F750FDFF3185E0001E3D60CA9C1B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3959[2] = 
{
	QuoteInstruction_tCC0594851C52F750FDFF3185E0001E3D60CA9C1B::get_offset_of__operand_0(),
	QuoteInstruction_tCC0594851C52F750FDFF3185E0001E3D60CA9C1B::get_offset_of__hoistedVariables_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3960 = { sizeof (ExpressionQuoter_tEAE5B88BBC6EB0B78A277DE39515AF585236DC08), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3960[3] = 
{
	ExpressionQuoter_tEAE5B88BBC6EB0B78A277DE39515AF585236DC08::get_offset_of__variables_0(),
	ExpressionQuoter_tEAE5B88BBC6EB0B78A277DE39515AF585236DC08::get_offset_of__frame_1(),
	ExpressionQuoter_tEAE5B88BBC6EB0B78A277DE39515AF585236DC08::get_offset_of__shadowedVars_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3961 = { sizeof (DelegateHelpers_tC220AEB31E24035C65BDC2A87576B293B189EBF5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3962 = { sizeof (U3CU3Ec_tFB9335A435065C66EE55EDA65A70B1197E95FA40), -1, sizeof(U3CU3Ec_tFB9335A435065C66EE55EDA65A70B1197E95FA40_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3962[2] = 
{
	U3CU3Ec_tFB9335A435065C66EE55EDA65A70B1197E95FA40_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tFB9335A435065C66EE55EDA65A70B1197E95FA40_StaticFields::get_offset_of_U3CU3E9__1_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3963 = { sizeof (ScriptingRuntimeHelpers_t5EBE625EFE604DEF42AFBCA8F9EF3A808FF38DC7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3964 = { sizeof (ExceptionHelpers_t5E8A8C882B8B390FA25BB54BF282A31251678BB9), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3965 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3965[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3966 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3966[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3967 = { sizeof (RuntimeOps_t4AB498C75C54378274A510D5CD7DC9152A23E777), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3968 = { sizeof (MergedRuntimeVariables_t8BC0546349BF4A11B383C0C2B6D81EC1C1AB2DDD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3968[3] = 
{
	MergedRuntimeVariables_t8BC0546349BF4A11B383C0C2B6D81EC1C1AB2DDD::get_offset_of__first_0(),
	MergedRuntimeVariables_t8BC0546349BF4A11B383C0C2B6D81EC1C1AB2DDD::get_offset_of__second_1(),
	MergedRuntimeVariables_t8BC0546349BF4A11B383C0C2B6D81EC1C1AB2DDD::get_offset_of__indexes_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3969 = { sizeof (RuntimeVariables_t08E07CA7021C6692E793278352675C63B744D61A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3969[1] = 
{
	RuntimeVariables_t08E07CA7021C6692E793278352675C63B744D61A::get_offset_of__boxes_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3970 = { sizeof (DebugInfoGenerator_tD7E0E52E58589C53D21CDCA37EB68E89E040ECDF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3971 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3972 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3972[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3973 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3973[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3974 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3975 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3975[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3976 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3977 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3977[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3978 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3978[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3979 = { sizeof (CollectionExtensions_t04790A89E5724082B570A72C66DCBD3BA4458F41), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3980 = { sizeof (ContractUtils_t089DD55E5FFF91E0C61FCECE26F30D46DF46E6DF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3981 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3981[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3982 = { sizeof (ExpressionUtils_t94C272C47F1094D7F585B1E39FDC53B1D2E09E9C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3983 = { sizeof (ExpressionVisitorUtils_tD71C93539A5AD1CBEF16EDA9C1373CA0ECE10EED), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3984 = { sizeof (TypeExtensions_t19D651DD86C56EB81663789F768C4114E51CE9F8), -1, sizeof(TypeExtensions_t19D651DD86C56EB81663789F768C4114E51CE9F8_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3984[1] = 
{
	TypeExtensions_t19D651DD86C56EB81663789F768C4114E51CE9F8_StaticFields::get_offset_of_s_paramInfoCache_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3985 = { sizeof (TypeUtils_tF994AD398D39478A4587CFB05D295215C77B7402), -1, sizeof(TypeUtils_tF994AD398D39478A4587CFB05D295215C77B7402_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3985[1] = 
{
	TypeUtils_tF994AD398D39478A4587CFB05D295215C77B7402_StaticFields::get_offset_of_s_mscorlib_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3986 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3986[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3987 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3987[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3988 = { sizeof (EnumerableHelpers_t91DA7E2949102CFE16D0A151F7C355F72753F1FD), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3989 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3989[15] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3990 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3990[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3991 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3991[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3992 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3993 = { sizeof (U3CPrivateImplementationDetailsU3E_t14917ACC6E0C738A985023D2ECB9D4BAC153CB5C), -1, sizeof(U3CPrivateImplementationDetailsU3E_t14917ACC6E0C738A985023D2ECB9D4BAC153CB5C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3993[11] = 
{
	U3CPrivateImplementationDetailsU3E_t14917ACC6E0C738A985023D2ECB9D4BAC153CB5C_StaticFields::get_offset_of_U30AA802CD6847EB893FE786B5EA5168B2FDCD7B93_0(),
	U3CPrivateImplementationDetailsU3E_t14917ACC6E0C738A985023D2ECB9D4BAC153CB5C_StaticFields::get_offset_of_U30C4110BC17D746F018F47B49E0EB0D6590F69939_1(),
	U3CPrivateImplementationDetailsU3E_t14917ACC6E0C738A985023D2ECB9D4BAC153CB5C_StaticFields::get_offset_of_U320733E1283D873EBE47133A95C233E11B76F5F11_2(),
	U3CPrivateImplementationDetailsU3E_t14917ACC6E0C738A985023D2ECB9D4BAC153CB5C_StaticFields::get_offset_of_U321F4CBF8283FF1CAEB4A39316A97FC1D6DF1D35E_3(),
	U3CPrivateImplementationDetailsU3E_t14917ACC6E0C738A985023D2ECB9D4BAC153CB5C_StaticFields::get_offset_of_U323DFDCA6F045D4257BF5AC8CB1CF2EFADAFE9B94_4(),
	U3CPrivateImplementationDetailsU3E_t14917ACC6E0C738A985023D2ECB9D4BAC153CB5C_StaticFields::get_offset_of_U330A0358B25B1372DD598BB4B1AC56AD6B8F08A47_5(),
	U3CPrivateImplementationDetailsU3E_t14917ACC6E0C738A985023D2ECB9D4BAC153CB5C_StaticFields::get_offset_of_U35B5DF5A459E902D96F7DB0FB235A25346CA85C5D_6(),
	U3CPrivateImplementationDetailsU3E_t14917ACC6E0C738A985023D2ECB9D4BAC153CB5C_StaticFields::get_offset_of_U35BE411F1438EAEF33726D855E99011D5FECDDD4E_7(),
	U3CPrivateImplementationDetailsU3E_t14917ACC6E0C738A985023D2ECB9D4BAC153CB5C_StaticFields::get_offset_of_U38F22C9ECE1331718CBD268A9BBFD2F5E451441E3_8(),
	U3CPrivateImplementationDetailsU3E_t14917ACC6E0C738A985023D2ECB9D4BAC153CB5C_StaticFields::get_offset_of_A02DD1D8604EA8EC2D2BDA717A93A4EE85F13E53_9(),
	U3CPrivateImplementationDetailsU3E_t14917ACC6E0C738A985023D2ECB9D4BAC153CB5C_StaticFields::get_offset_of_AE2F76ECEF8B08F0BC7EA95DCFE945E1727927C9_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3994 = { sizeof (__StaticArrayInitTypeSizeU3D120_t83E233123DD538AA6101D1CB5AE4F5DC639EBC9D)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D120_t83E233123DD538AA6101D1CB5AE4F5DC639EBC9D ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3995 = { sizeof (__StaticArrayInitTypeSizeU3D256_tCCCC326240ED0A827344379DD68205BF9340FF47)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D256_tCCCC326240ED0A827344379DD68205BF9340FF47 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3996 = { sizeof (__StaticArrayInitTypeSizeU3D1024_t336389AC57307AEC77791F09CF655CD3EF917B7C)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D1024_t336389AC57307AEC77791F09CF655CD3EF917B7C ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3997 = { sizeof (U3CModuleU3E_tDD607E0208590BE5D73D68EB7825AD7A1FBDFCC3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3998 = { sizeof (UsedByNativeCodeAttribute_t923F9A140847AF2F193AD1AB33143B8774797912), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3998[1] = 
{
	UsedByNativeCodeAttribute_t923F9A140847AF2F193AD1AB33143B8774797912::get_offset_of_U3CNameU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3999 = { sizeof (RequiredByNativeCodeAttribute_t949320E827C2BD269B3E686FE317A18835670AAE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3999[3] = 
{
	RequiredByNativeCodeAttribute_t949320E827C2BD269B3E686FE317A18835670AAE::get_offset_of_U3CNameU3Ek__BackingField_0(),
	RequiredByNativeCodeAttribute_t949320E827C2BD269B3E686FE317A18835670AAE::get_offset_of_U3COptionalU3Ek__BackingField_1(),
	RequiredByNativeCodeAttribute_t949320E827C2BD269B3E686FE317A18835670AAE::get_offset_of_U3CGenerateProxyU3Ek__BackingField_2(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
