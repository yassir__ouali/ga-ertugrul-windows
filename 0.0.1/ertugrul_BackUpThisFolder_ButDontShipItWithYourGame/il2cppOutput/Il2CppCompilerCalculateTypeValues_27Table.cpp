﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Attribute[]
struct AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.ArrayList
struct ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4;
// System.Collections.Generic.Dictionary`2<System.Int32,System.String>
struct Dictionary_2_t4EFE6A1D6502662B911688316C6920444A18CF0C;
// System.Collections.Hashtable
struct Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9;
// System.Collections.Hashtable/bucket[]
struct bucketU5BU5D_t6FF2C2C4B21F2206885CD19A78F68B874C8DC84A;
// System.Collections.ICollection
struct ICollection_tA3BAB2482E28132A7CA9E0E21393027353C28B54;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.Collections.IEqualityComparer
struct IEqualityComparer_t3102D0F5BABD60224F6DFF4815BCA1045831FB7C;
// System.ComponentModel.AttributeCollection
struct AttributeCollection_tBE6941BB802EDE34B7F986C14A7D7E3A4E135EBE;
// System.ComponentModel.CategoryAttribute
struct CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80;
// System.ComponentModel.Design.ITypeDescriptorFilterService
struct ITypeDescriptorFilterService_t7F3A790DAB15141C69DE8916436C6AAD67C7EFDA;
// System.ComponentModel.EventDescriptor
struct EventDescriptor_tAB488D66C0409A1889EE56355848CDA43ED95222;
// System.ComponentModel.EventDescriptorCollection
struct EventDescriptorCollection_tB9FC461177F5D7FE0B79268C77569B36F4E1FF37;
// System.ComponentModel.IComNativeDescriptorHandler
struct IComNativeDescriptorHandler_t37AAAFAEE49C1CEB4F9AF7AAC05A955D1DD70678;
// System.ComponentModel.ICustomTypeDescriptor
struct ICustomTypeDescriptor_tAF366F5EE1A787B4D24D9570FFFFE6C5DC27C384;
// System.ComponentModel.PropertyDescriptorCollection
struct PropertyDescriptorCollection_t19FEFDD6CEF7609BB10282A4B52C3C09A04B41A2;
// System.ComponentModel.PropertyTabScope[]
struct PropertyTabScopeU5BU5D_t06DD8B6326FEE6EB77AA915202CFE3C2ABAC7260;
// System.ComponentModel.ReferenceConverter
struct ReferenceConverter_t5080472EE999A1F00721E6C5C97013762C85C7E4;
// System.ComponentModel.RefreshEventArgs
struct RefreshEventArgs_tAC29F8AEEF1CB6B4ADD8BC4DD0A66366A4700650;
// System.ComponentModel.RefreshEventHandler
struct RefreshEventHandler_tE1B96D1B5C9580F60558220F9DEBA92333849F27;
// System.ComponentModel.RunWorkerCompletedEventArgs
struct RunWorkerCompletedEventArgs_tFAFA9B2ED339BE1F33AF8845D46A251EB5C6950D;
// System.ComponentModel.TypeConverter
struct TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB;
// System.ComponentModel.TypeConverter/StandardValuesCollection
struct StandardValuesCollection_t929677712574EF02F5C4CF4C38E070841C20BDA3;
// System.ComponentModel.TypeDescriptionProvider
struct TypeDescriptionProvider_tE390829A953C44525366CA2A733E92642B97B591;
// System.ComponentModel.TypeDescriptionProvider/EmptyCustomTypeDescriptor
struct EmptyCustomTypeDescriptor_tDDCC5072613D7D0405BCCF3D49F3EF7B4E1E7DB8;
// System.ComponentModel.TypeDescriptor/TypeDescriptionNode
struct TypeDescriptionNode_t8C5D3A02DB612FFE1DE12251E49EC265DE6B81E1;
// System.ComponentModel.WeakHashtable
struct WeakHashtable_t230AE0A1A5D5EE8C97A2280405572D83D757B2CF;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Diagnostics.BooleanSwitch
struct BooleanSwitch_t2B51F0E7C50E7DAA74282B51EF9107A2E54723E0;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.Diagnostics.TraceSwitch
struct TraceSwitch_t32D210D5C9B05D9E555925260EEC3767BA895EC8;
// System.Exception
struct Exception_t;
// System.Guid[]
struct GuidU5BU5D_t5CC024A2CAE5304311E0B961142A216C0972B0FF;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Reflection.EventInfo
struct EventInfo_t;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Reflection.PropertyInfo
struct PropertyInfo_t;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.Security.Cryptography.Oid
struct Oid_tC00A10270EAF16BBF0F2619B9AEC883E0CFE6137;
// System.Security.Cryptography.OidCollection
struct OidCollection_tEB423F1150E53DCF513BF5A699F911586A31B94E;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;

struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#define ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifndef READONLYCOLLECTIONBASE_TFD695167917CE6DF4FA18A906FA530880B9B8772_H
#define READONLYCOLLECTIONBASE_TFD695167917CE6DF4FA18A906FA530880B9B8772_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.ReadOnlyCollectionBase
struct  ReadOnlyCollectionBase_tFD695167917CE6DF4FA18A906FA530880B9B8772  : public RuntimeObject
{
public:
	// System.Collections.ArrayList System.Collections.ReadOnlyCollectionBase::list
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ___list_0;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(ReadOnlyCollectionBase_tFD695167917CE6DF4FA18A906FA530880B9B8772, ___list_0)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get_list_0() const { return ___list_0; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READONLYCOLLECTIONBASE_TFD695167917CE6DF4FA18A906FA530880B9B8772_H
#ifndef CUSTOMTYPEDESCRIPTOR_TF8665CD45DFFA622F7EB328A2F77067DD2147689_H
#define CUSTOMTYPEDESCRIPTOR_TF8665CD45DFFA622F7EB328A2F77067DD2147689_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.CustomTypeDescriptor
struct  CustomTypeDescriptor_tF8665CD45DFFA622F7EB328A2F77067DD2147689  : public RuntimeObject
{
public:
	// System.ComponentModel.ICustomTypeDescriptor System.ComponentModel.CustomTypeDescriptor::_parent
	RuntimeObject* ____parent_0;

public:
	inline static int32_t get_offset_of__parent_0() { return static_cast<int32_t>(offsetof(CustomTypeDescriptor_tF8665CD45DFFA622F7EB328A2F77067DD2147689, ____parent_0)); }
	inline RuntimeObject* get__parent_0() const { return ____parent_0; }
	inline RuntimeObject** get_address_of__parent_0() { return &____parent_0; }
	inline void set__parent_0(RuntimeObject* value)
	{
		____parent_0 = value;
		Il2CppCodeGenWriteBarrier((&____parent_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMTYPEDESCRIPTOR_TF8665CD45DFFA622F7EB328A2F77067DD2147689_H
#ifndef DESIGNTIMELICENSECONTEXTSERIALIZER_T613DD72FEBA8711EB6154D704B01A70E11201B5D_H
#define DESIGNTIMELICENSECONTEXTSERIALIZER_T613DD72FEBA8711EB6154D704B01A70E11201B5D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.Design.DesigntimeLicenseContextSerializer
struct  DesigntimeLicenseContextSerializer_t613DD72FEBA8711EB6154D704B01A70E11201B5D  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESIGNTIMELICENSECONTEXTSERIALIZER_T613DD72FEBA8711EB6154D704B01A70E11201B5D_H
#ifndef INSTANCEDESCRIPTOR_T964F3DAD1E093CF941D315A157F6DE4FF6E52EF6_H
#define INSTANCEDESCRIPTOR_T964F3DAD1E093CF941D315A157F6DE4FF6E52EF6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.Design.Serialization.InstanceDescriptor
struct  InstanceDescriptor_t964F3DAD1E093CF941D315A157F6DE4FF6E52EF6  : public RuntimeObject
{
public:
	// System.Reflection.MemberInfo System.ComponentModel.Design.Serialization.InstanceDescriptor::member
	MemberInfo_t * ___member_0;
	// System.Collections.ICollection System.ComponentModel.Design.Serialization.InstanceDescriptor::arguments
	RuntimeObject* ___arguments_1;
	// System.Boolean System.ComponentModel.Design.Serialization.InstanceDescriptor::isComplete
	bool ___isComplete_2;

public:
	inline static int32_t get_offset_of_member_0() { return static_cast<int32_t>(offsetof(InstanceDescriptor_t964F3DAD1E093CF941D315A157F6DE4FF6E52EF6, ___member_0)); }
	inline MemberInfo_t * get_member_0() const { return ___member_0; }
	inline MemberInfo_t ** get_address_of_member_0() { return &___member_0; }
	inline void set_member_0(MemberInfo_t * value)
	{
		___member_0 = value;
		Il2CppCodeGenWriteBarrier((&___member_0), value);
	}

	inline static int32_t get_offset_of_arguments_1() { return static_cast<int32_t>(offsetof(InstanceDescriptor_t964F3DAD1E093CF941D315A157F6DE4FF6E52EF6, ___arguments_1)); }
	inline RuntimeObject* get_arguments_1() const { return ___arguments_1; }
	inline RuntimeObject** get_address_of_arguments_1() { return &___arguments_1; }
	inline void set_arguments_1(RuntimeObject* value)
	{
		___arguments_1 = value;
		Il2CppCodeGenWriteBarrier((&___arguments_1), value);
	}

	inline static int32_t get_offset_of_isComplete_2() { return static_cast<int32_t>(offsetof(InstanceDescriptor_t964F3DAD1E093CF941D315A157F6DE4FF6E52EF6, ___isComplete_2)); }
	inline bool get_isComplete_2() const { return ___isComplete_2; }
	inline bool* get_address_of_isComplete_2() { return &___isComplete_2; }
	inline void set_isComplete_2(bool value)
	{
		___isComplete_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTANCEDESCRIPTOR_T964F3DAD1E093CF941D315A157F6DE4FF6E52EF6_H
#ifndef LICENSECONTEXT_TE7068766A5D105EA910974E3F08D0AF7534A806A_H
#define LICENSECONTEXT_TE7068766A5D105EA910974E3F08D0AF7534A806A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.LicenseContext
struct  LicenseContext_tE7068766A5D105EA910974E3F08D0AF7534A806A  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LICENSECONTEXT_TE7068766A5D105EA910974E3F08D0AF7534A806A_H
#ifndef MEMBERDESCRIPTOR_T9540F11CAE19431295C2582699585264E053B8F8_H
#define MEMBERDESCRIPTOR_T9540F11CAE19431295C2582699585264E053B8F8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.MemberDescriptor
struct  MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8  : public RuntimeObject
{
public:
	// System.String System.ComponentModel.MemberDescriptor::name
	String_t* ___name_0;
	// System.String System.ComponentModel.MemberDescriptor::displayName
	String_t* ___displayName_1;
	// System.Int32 System.ComponentModel.MemberDescriptor::nameHash
	int32_t ___nameHash_2;
	// System.ComponentModel.AttributeCollection System.ComponentModel.MemberDescriptor::attributeCollection
	AttributeCollection_tBE6941BB802EDE34B7F986C14A7D7E3A4E135EBE * ___attributeCollection_3;
	// System.Attribute[] System.ComponentModel.MemberDescriptor::attributes
	AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17* ___attributes_4;
	// System.Attribute[] System.ComponentModel.MemberDescriptor::originalAttributes
	AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17* ___originalAttributes_5;
	// System.Boolean System.ComponentModel.MemberDescriptor::attributesFiltered
	bool ___attributesFiltered_6;
	// System.Boolean System.ComponentModel.MemberDescriptor::attributesFilled
	bool ___attributesFilled_7;
	// System.Int32 System.ComponentModel.MemberDescriptor::metadataVersion
	int32_t ___metadataVersion_8;
	// System.String System.ComponentModel.MemberDescriptor::category
	String_t* ___category_9;
	// System.String System.ComponentModel.MemberDescriptor::description
	String_t* ___description_10;
	// System.Object System.ComponentModel.MemberDescriptor::lockCookie
	RuntimeObject * ___lockCookie_11;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_displayName_1() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___displayName_1)); }
	inline String_t* get_displayName_1() const { return ___displayName_1; }
	inline String_t** get_address_of_displayName_1() { return &___displayName_1; }
	inline void set_displayName_1(String_t* value)
	{
		___displayName_1 = value;
		Il2CppCodeGenWriteBarrier((&___displayName_1), value);
	}

	inline static int32_t get_offset_of_nameHash_2() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___nameHash_2)); }
	inline int32_t get_nameHash_2() const { return ___nameHash_2; }
	inline int32_t* get_address_of_nameHash_2() { return &___nameHash_2; }
	inline void set_nameHash_2(int32_t value)
	{
		___nameHash_2 = value;
	}

	inline static int32_t get_offset_of_attributeCollection_3() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___attributeCollection_3)); }
	inline AttributeCollection_tBE6941BB802EDE34B7F986C14A7D7E3A4E135EBE * get_attributeCollection_3() const { return ___attributeCollection_3; }
	inline AttributeCollection_tBE6941BB802EDE34B7F986C14A7D7E3A4E135EBE ** get_address_of_attributeCollection_3() { return &___attributeCollection_3; }
	inline void set_attributeCollection_3(AttributeCollection_tBE6941BB802EDE34B7F986C14A7D7E3A4E135EBE * value)
	{
		___attributeCollection_3 = value;
		Il2CppCodeGenWriteBarrier((&___attributeCollection_3), value);
	}

	inline static int32_t get_offset_of_attributes_4() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___attributes_4)); }
	inline AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17* get_attributes_4() const { return ___attributes_4; }
	inline AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17** get_address_of_attributes_4() { return &___attributes_4; }
	inline void set_attributes_4(AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17* value)
	{
		___attributes_4 = value;
		Il2CppCodeGenWriteBarrier((&___attributes_4), value);
	}

	inline static int32_t get_offset_of_originalAttributes_5() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___originalAttributes_5)); }
	inline AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17* get_originalAttributes_5() const { return ___originalAttributes_5; }
	inline AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17** get_address_of_originalAttributes_5() { return &___originalAttributes_5; }
	inline void set_originalAttributes_5(AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17* value)
	{
		___originalAttributes_5 = value;
		Il2CppCodeGenWriteBarrier((&___originalAttributes_5), value);
	}

	inline static int32_t get_offset_of_attributesFiltered_6() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___attributesFiltered_6)); }
	inline bool get_attributesFiltered_6() const { return ___attributesFiltered_6; }
	inline bool* get_address_of_attributesFiltered_6() { return &___attributesFiltered_6; }
	inline void set_attributesFiltered_6(bool value)
	{
		___attributesFiltered_6 = value;
	}

	inline static int32_t get_offset_of_attributesFilled_7() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___attributesFilled_7)); }
	inline bool get_attributesFilled_7() const { return ___attributesFilled_7; }
	inline bool* get_address_of_attributesFilled_7() { return &___attributesFilled_7; }
	inline void set_attributesFilled_7(bool value)
	{
		___attributesFilled_7 = value;
	}

	inline static int32_t get_offset_of_metadataVersion_8() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___metadataVersion_8)); }
	inline int32_t get_metadataVersion_8() const { return ___metadataVersion_8; }
	inline int32_t* get_address_of_metadataVersion_8() { return &___metadataVersion_8; }
	inline void set_metadataVersion_8(int32_t value)
	{
		___metadataVersion_8 = value;
	}

	inline static int32_t get_offset_of_category_9() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___category_9)); }
	inline String_t* get_category_9() const { return ___category_9; }
	inline String_t** get_address_of_category_9() { return &___category_9; }
	inline void set_category_9(String_t* value)
	{
		___category_9 = value;
		Il2CppCodeGenWriteBarrier((&___category_9), value);
	}

	inline static int32_t get_offset_of_description_10() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___description_10)); }
	inline String_t* get_description_10() const { return ___description_10; }
	inline String_t** get_address_of_description_10() { return &___description_10; }
	inline void set_description_10(String_t* value)
	{
		___description_10 = value;
		Il2CppCodeGenWriteBarrier((&___description_10), value);
	}

	inline static int32_t get_offset_of_lockCookie_11() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___lockCookie_11)); }
	inline RuntimeObject * get_lockCookie_11() const { return ___lockCookie_11; }
	inline RuntimeObject ** get_address_of_lockCookie_11() { return &___lockCookie_11; }
	inline void set_lockCookie_11(RuntimeObject * value)
	{
		___lockCookie_11 = value;
		Il2CppCodeGenWriteBarrier((&___lockCookie_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERDESCRIPTOR_T9540F11CAE19431295C2582699585264E053B8F8_H
#ifndef REFERENCECOMPARER_T3DEA0E67FDC8BA34C167CC4401A1FCC3CFE0B33A_H
#define REFERENCECOMPARER_T3DEA0E67FDC8BA34C167CC4401A1FCC3CFE0B33A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.ReferenceConverter_ReferenceComparer
struct  ReferenceComparer_t3DEA0E67FDC8BA34C167CC4401A1FCC3CFE0B33A  : public RuntimeObject
{
public:
	// System.ComponentModel.ReferenceConverter System.ComponentModel.ReferenceConverter_ReferenceComparer::converter
	ReferenceConverter_t5080472EE999A1F00721E6C5C97013762C85C7E4 * ___converter_0;

public:
	inline static int32_t get_offset_of_converter_0() { return static_cast<int32_t>(offsetof(ReferenceComparer_t3DEA0E67FDC8BA34C167CC4401A1FCC3CFE0B33A, ___converter_0)); }
	inline ReferenceConverter_t5080472EE999A1F00721E6C5C97013762C85C7E4 * get_converter_0() const { return ___converter_0; }
	inline ReferenceConverter_t5080472EE999A1F00721E6C5C97013762C85C7E4 ** get_address_of_converter_0() { return &___converter_0; }
	inline void set_converter_0(ReferenceConverter_t5080472EE999A1F00721E6C5C97013762C85C7E4 * value)
	{
		___converter_0 = value;
		Il2CppCodeGenWriteBarrier((&___converter_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFERENCECOMPARER_T3DEA0E67FDC8BA34C167CC4401A1FCC3CFE0B33A_H
#ifndef REFLECTEDTYPEDATA_T71CC6CDF94E72051C2A9A5CA4052C16DF8EE0A96_H
#define REFLECTEDTYPEDATA_T71CC6CDF94E72051C2A9A5CA4052C16DF8EE0A96_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.ReflectTypeDescriptionProvider_ReflectedTypeData
struct  ReflectedTypeData_t71CC6CDF94E72051C2A9A5CA4052C16DF8EE0A96  : public RuntimeObject
{
public:
	// System.Type System.ComponentModel.ReflectTypeDescriptionProvider_ReflectedTypeData::_type
	Type_t * ____type_0;
	// System.ComponentModel.AttributeCollection System.ComponentModel.ReflectTypeDescriptionProvider_ReflectedTypeData::_attributes
	AttributeCollection_tBE6941BB802EDE34B7F986C14A7D7E3A4E135EBE * ____attributes_1;
	// System.ComponentModel.EventDescriptorCollection System.ComponentModel.ReflectTypeDescriptionProvider_ReflectedTypeData::_events
	EventDescriptorCollection_tB9FC461177F5D7FE0B79268C77569B36F4E1FF37 * ____events_2;
	// System.ComponentModel.PropertyDescriptorCollection System.ComponentModel.ReflectTypeDescriptionProvider_ReflectedTypeData::_properties
	PropertyDescriptorCollection_t19FEFDD6CEF7609BB10282A4B52C3C09A04B41A2 * ____properties_3;
	// System.ComponentModel.TypeConverter System.ComponentModel.ReflectTypeDescriptionProvider_ReflectedTypeData::_converter
	TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB * ____converter_4;
	// System.Object[] System.ComponentModel.ReflectTypeDescriptionProvider_ReflectedTypeData::_editors
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ____editors_5;
	// System.Type[] System.ComponentModel.ReflectTypeDescriptionProvider_ReflectedTypeData::_editorTypes
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ____editorTypes_6;
	// System.Int32 System.ComponentModel.ReflectTypeDescriptionProvider_ReflectedTypeData::_editorCount
	int32_t ____editorCount_7;

public:
	inline static int32_t get_offset_of__type_0() { return static_cast<int32_t>(offsetof(ReflectedTypeData_t71CC6CDF94E72051C2A9A5CA4052C16DF8EE0A96, ____type_0)); }
	inline Type_t * get__type_0() const { return ____type_0; }
	inline Type_t ** get_address_of__type_0() { return &____type_0; }
	inline void set__type_0(Type_t * value)
	{
		____type_0 = value;
		Il2CppCodeGenWriteBarrier((&____type_0), value);
	}

	inline static int32_t get_offset_of__attributes_1() { return static_cast<int32_t>(offsetof(ReflectedTypeData_t71CC6CDF94E72051C2A9A5CA4052C16DF8EE0A96, ____attributes_1)); }
	inline AttributeCollection_tBE6941BB802EDE34B7F986C14A7D7E3A4E135EBE * get__attributes_1() const { return ____attributes_1; }
	inline AttributeCollection_tBE6941BB802EDE34B7F986C14A7D7E3A4E135EBE ** get_address_of__attributes_1() { return &____attributes_1; }
	inline void set__attributes_1(AttributeCollection_tBE6941BB802EDE34B7F986C14A7D7E3A4E135EBE * value)
	{
		____attributes_1 = value;
		Il2CppCodeGenWriteBarrier((&____attributes_1), value);
	}

	inline static int32_t get_offset_of__events_2() { return static_cast<int32_t>(offsetof(ReflectedTypeData_t71CC6CDF94E72051C2A9A5CA4052C16DF8EE0A96, ____events_2)); }
	inline EventDescriptorCollection_tB9FC461177F5D7FE0B79268C77569B36F4E1FF37 * get__events_2() const { return ____events_2; }
	inline EventDescriptorCollection_tB9FC461177F5D7FE0B79268C77569B36F4E1FF37 ** get_address_of__events_2() { return &____events_2; }
	inline void set__events_2(EventDescriptorCollection_tB9FC461177F5D7FE0B79268C77569B36F4E1FF37 * value)
	{
		____events_2 = value;
		Il2CppCodeGenWriteBarrier((&____events_2), value);
	}

	inline static int32_t get_offset_of__properties_3() { return static_cast<int32_t>(offsetof(ReflectedTypeData_t71CC6CDF94E72051C2A9A5CA4052C16DF8EE0A96, ____properties_3)); }
	inline PropertyDescriptorCollection_t19FEFDD6CEF7609BB10282A4B52C3C09A04B41A2 * get__properties_3() const { return ____properties_3; }
	inline PropertyDescriptorCollection_t19FEFDD6CEF7609BB10282A4B52C3C09A04B41A2 ** get_address_of__properties_3() { return &____properties_3; }
	inline void set__properties_3(PropertyDescriptorCollection_t19FEFDD6CEF7609BB10282A4B52C3C09A04B41A2 * value)
	{
		____properties_3 = value;
		Il2CppCodeGenWriteBarrier((&____properties_3), value);
	}

	inline static int32_t get_offset_of__converter_4() { return static_cast<int32_t>(offsetof(ReflectedTypeData_t71CC6CDF94E72051C2A9A5CA4052C16DF8EE0A96, ____converter_4)); }
	inline TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB * get__converter_4() const { return ____converter_4; }
	inline TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB ** get_address_of__converter_4() { return &____converter_4; }
	inline void set__converter_4(TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB * value)
	{
		____converter_4 = value;
		Il2CppCodeGenWriteBarrier((&____converter_4), value);
	}

	inline static int32_t get_offset_of__editors_5() { return static_cast<int32_t>(offsetof(ReflectedTypeData_t71CC6CDF94E72051C2A9A5CA4052C16DF8EE0A96, ____editors_5)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get__editors_5() const { return ____editors_5; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of__editors_5() { return &____editors_5; }
	inline void set__editors_5(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		____editors_5 = value;
		Il2CppCodeGenWriteBarrier((&____editors_5), value);
	}

	inline static int32_t get_offset_of__editorTypes_6() { return static_cast<int32_t>(offsetof(ReflectedTypeData_t71CC6CDF94E72051C2A9A5CA4052C16DF8EE0A96, ____editorTypes_6)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get__editorTypes_6() const { return ____editorTypes_6; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of__editorTypes_6() { return &____editorTypes_6; }
	inline void set__editorTypes_6(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		____editorTypes_6 = value;
		Il2CppCodeGenWriteBarrier((&____editorTypes_6), value);
	}

	inline static int32_t get_offset_of__editorCount_7() { return static_cast<int32_t>(offsetof(ReflectedTypeData_t71CC6CDF94E72051C2A9A5CA4052C16DF8EE0A96, ____editorCount_7)); }
	inline int32_t get__editorCount_7() const { return ____editorCount_7; }
	inline int32_t* get_address_of__editorCount_7() { return &____editorCount_7; }
	inline void set__editorCount_7(int32_t value)
	{
		____editorCount_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFLECTEDTYPEDATA_T71CC6CDF94E72051C2A9A5CA4052C16DF8EE0A96_H
#ifndef SYNTAXCHECK_TDC1D921DE89C089B7EF818689C16587F234ABD79_H
#define SYNTAXCHECK_TDC1D921DE89C089B7EF818689C16587F234ABD79_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.SyntaxCheck
struct  SyntaxCheck_tDC1D921DE89C089B7EF818689C16587F234ABD79  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYNTAXCHECK_TDC1D921DE89C089B7EF818689C16587F234ABD79_H
#ifndef STANDARDVALUESCOLLECTION_T929677712574EF02F5C4CF4C38E070841C20BDA3_H
#define STANDARDVALUESCOLLECTION_T929677712574EF02F5C4CF4C38E070841C20BDA3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.TypeConverter_StandardValuesCollection
struct  StandardValuesCollection_t929677712574EF02F5C4CF4C38E070841C20BDA3  : public RuntimeObject
{
public:
	// System.Collections.ICollection System.ComponentModel.TypeConverter_StandardValuesCollection::values
	RuntimeObject* ___values_0;
	// System.Array System.ComponentModel.TypeConverter_StandardValuesCollection::valueArray
	RuntimeArray * ___valueArray_1;

public:
	inline static int32_t get_offset_of_values_0() { return static_cast<int32_t>(offsetof(StandardValuesCollection_t929677712574EF02F5C4CF4C38E070841C20BDA3, ___values_0)); }
	inline RuntimeObject* get_values_0() const { return ___values_0; }
	inline RuntimeObject** get_address_of_values_0() { return &___values_0; }
	inline void set_values_0(RuntimeObject* value)
	{
		___values_0 = value;
		Il2CppCodeGenWriteBarrier((&___values_0), value);
	}

	inline static int32_t get_offset_of_valueArray_1() { return static_cast<int32_t>(offsetof(StandardValuesCollection_t929677712574EF02F5C4CF4C38E070841C20BDA3, ___valueArray_1)); }
	inline RuntimeArray * get_valueArray_1() const { return ___valueArray_1; }
	inline RuntimeArray ** get_address_of_valueArray_1() { return &___valueArray_1; }
	inline void set_valueArray_1(RuntimeArray * value)
	{
		___valueArray_1 = value;
		Il2CppCodeGenWriteBarrier((&___valueArray_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STANDARDVALUESCOLLECTION_T929677712574EF02F5C4CF4C38E070841C20BDA3_H
#ifndef TYPEDESCRIPTIONPROVIDER_TE390829A953C44525366CA2A733E92642B97B591_H
#define TYPEDESCRIPTIONPROVIDER_TE390829A953C44525366CA2A733E92642B97B591_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.TypeDescriptionProvider
struct  TypeDescriptionProvider_tE390829A953C44525366CA2A733E92642B97B591  : public RuntimeObject
{
public:
	// System.ComponentModel.TypeDescriptionProvider System.ComponentModel.TypeDescriptionProvider::_parent
	TypeDescriptionProvider_tE390829A953C44525366CA2A733E92642B97B591 * ____parent_0;
	// System.ComponentModel.TypeDescriptionProvider_EmptyCustomTypeDescriptor System.ComponentModel.TypeDescriptionProvider::_emptyDescriptor
	EmptyCustomTypeDescriptor_tDDCC5072613D7D0405BCCF3D49F3EF7B4E1E7DB8 * ____emptyDescriptor_1;

public:
	inline static int32_t get_offset_of__parent_0() { return static_cast<int32_t>(offsetof(TypeDescriptionProvider_tE390829A953C44525366CA2A733E92642B97B591, ____parent_0)); }
	inline TypeDescriptionProvider_tE390829A953C44525366CA2A733E92642B97B591 * get__parent_0() const { return ____parent_0; }
	inline TypeDescriptionProvider_tE390829A953C44525366CA2A733E92642B97B591 ** get_address_of__parent_0() { return &____parent_0; }
	inline void set__parent_0(TypeDescriptionProvider_tE390829A953C44525366CA2A733E92642B97B591 * value)
	{
		____parent_0 = value;
		Il2CppCodeGenWriteBarrier((&____parent_0), value);
	}

	inline static int32_t get_offset_of__emptyDescriptor_1() { return static_cast<int32_t>(offsetof(TypeDescriptionProvider_tE390829A953C44525366CA2A733E92642B97B591, ____emptyDescriptor_1)); }
	inline EmptyCustomTypeDescriptor_tDDCC5072613D7D0405BCCF3D49F3EF7B4E1E7DB8 * get__emptyDescriptor_1() const { return ____emptyDescriptor_1; }
	inline EmptyCustomTypeDescriptor_tDDCC5072613D7D0405BCCF3D49F3EF7B4E1E7DB8 ** get_address_of__emptyDescriptor_1() { return &____emptyDescriptor_1; }
	inline void set__emptyDescriptor_1(EmptyCustomTypeDescriptor_tDDCC5072613D7D0405BCCF3D49F3EF7B4E1E7DB8 * value)
	{
		____emptyDescriptor_1 = value;
		Il2CppCodeGenWriteBarrier((&____emptyDescriptor_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEDESCRIPTIONPROVIDER_TE390829A953C44525366CA2A733E92642B97B591_H
#ifndef TYPEDESCRIPTOR_TEA9B846104F636343B5CBF281E8E7BE6349843AF_H
#define TYPEDESCRIPTOR_TEA9B846104F636343B5CBF281E8E7BE6349843AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.TypeDescriptor
struct  TypeDescriptor_tEA9B846104F636343B5CBF281E8E7BE6349843AF  : public RuntimeObject
{
public:

public:
};

struct TypeDescriptor_tEA9B846104F636343B5CBF281E8E7BE6349843AF_StaticFields
{
public:
	// System.ComponentModel.WeakHashtable System.ComponentModel.TypeDescriptor::_providerTable
	WeakHashtable_t230AE0A1A5D5EE8C97A2280405572D83D757B2CF * ____providerTable_0;
	// System.Collections.Hashtable System.ComponentModel.TypeDescriptor::_providerTypeTable
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ____providerTypeTable_1;
	// System.Collections.Hashtable modreq(System.Runtime.CompilerServices.IsVolatile) System.ComponentModel.TypeDescriptor::_defaultProviders
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ____defaultProviders_2;
	// System.ComponentModel.WeakHashtable modreq(System.Runtime.CompilerServices.IsVolatile) System.ComponentModel.TypeDescriptor::_associationTable
	WeakHashtable_t230AE0A1A5D5EE8C97A2280405572D83D757B2CF * ____associationTable_3;
	// System.Int32 System.ComponentModel.TypeDescriptor::_metadataVersion
	int32_t ____metadataVersion_4;
	// System.Int32 System.ComponentModel.TypeDescriptor::_collisionIndex
	int32_t ____collisionIndex_5;
	// System.Diagnostics.BooleanSwitch System.ComponentModel.TypeDescriptor::TraceDescriptor
	BooleanSwitch_t2B51F0E7C50E7DAA74282B51EF9107A2E54723E0 * ___TraceDescriptor_6;
	// System.Guid[] System.ComponentModel.TypeDescriptor::_pipelineInitializeKeys
	GuidU5BU5D_t5CC024A2CAE5304311E0B961142A216C0972B0FF* ____pipelineInitializeKeys_10;
	// System.Guid[] System.ComponentModel.TypeDescriptor::_pipelineMergeKeys
	GuidU5BU5D_t5CC024A2CAE5304311E0B961142A216C0972B0FF* ____pipelineMergeKeys_11;
	// System.Guid[] System.ComponentModel.TypeDescriptor::_pipelineFilterKeys
	GuidU5BU5D_t5CC024A2CAE5304311E0B961142A216C0972B0FF* ____pipelineFilterKeys_12;
	// System.Guid[] System.ComponentModel.TypeDescriptor::_pipelineAttributeFilterKeys
	GuidU5BU5D_t5CC024A2CAE5304311E0B961142A216C0972B0FF* ____pipelineAttributeFilterKeys_13;
	// System.Object System.ComponentModel.TypeDescriptor::_internalSyncObject
	RuntimeObject * ____internalSyncObject_14;
	// System.ComponentModel.RefreshEventHandler System.ComponentModel.TypeDescriptor::Refreshed
	RefreshEventHandler_tE1B96D1B5C9580F60558220F9DEBA92333849F27 * ___Refreshed_15;

public:
	inline static int32_t get_offset_of__providerTable_0() { return static_cast<int32_t>(offsetof(TypeDescriptor_tEA9B846104F636343B5CBF281E8E7BE6349843AF_StaticFields, ____providerTable_0)); }
	inline WeakHashtable_t230AE0A1A5D5EE8C97A2280405572D83D757B2CF * get__providerTable_0() const { return ____providerTable_0; }
	inline WeakHashtable_t230AE0A1A5D5EE8C97A2280405572D83D757B2CF ** get_address_of__providerTable_0() { return &____providerTable_0; }
	inline void set__providerTable_0(WeakHashtable_t230AE0A1A5D5EE8C97A2280405572D83D757B2CF * value)
	{
		____providerTable_0 = value;
		Il2CppCodeGenWriteBarrier((&____providerTable_0), value);
	}

	inline static int32_t get_offset_of__providerTypeTable_1() { return static_cast<int32_t>(offsetof(TypeDescriptor_tEA9B846104F636343B5CBF281E8E7BE6349843AF_StaticFields, ____providerTypeTable_1)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get__providerTypeTable_1() const { return ____providerTypeTable_1; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of__providerTypeTable_1() { return &____providerTypeTable_1; }
	inline void set__providerTypeTable_1(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		____providerTypeTable_1 = value;
		Il2CppCodeGenWriteBarrier((&____providerTypeTable_1), value);
	}

	inline static int32_t get_offset_of__defaultProviders_2() { return static_cast<int32_t>(offsetof(TypeDescriptor_tEA9B846104F636343B5CBF281E8E7BE6349843AF_StaticFields, ____defaultProviders_2)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get__defaultProviders_2() const { return ____defaultProviders_2; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of__defaultProviders_2() { return &____defaultProviders_2; }
	inline void set__defaultProviders_2(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		____defaultProviders_2 = value;
		Il2CppCodeGenWriteBarrier((&____defaultProviders_2), value);
	}

	inline static int32_t get_offset_of__associationTable_3() { return static_cast<int32_t>(offsetof(TypeDescriptor_tEA9B846104F636343B5CBF281E8E7BE6349843AF_StaticFields, ____associationTable_3)); }
	inline WeakHashtable_t230AE0A1A5D5EE8C97A2280405572D83D757B2CF * get__associationTable_3() const { return ____associationTable_3; }
	inline WeakHashtable_t230AE0A1A5D5EE8C97A2280405572D83D757B2CF ** get_address_of__associationTable_3() { return &____associationTable_3; }
	inline void set__associationTable_3(WeakHashtable_t230AE0A1A5D5EE8C97A2280405572D83D757B2CF * value)
	{
		____associationTable_3 = value;
		Il2CppCodeGenWriteBarrier((&____associationTable_3), value);
	}

	inline static int32_t get_offset_of__metadataVersion_4() { return static_cast<int32_t>(offsetof(TypeDescriptor_tEA9B846104F636343B5CBF281E8E7BE6349843AF_StaticFields, ____metadataVersion_4)); }
	inline int32_t get__metadataVersion_4() const { return ____metadataVersion_4; }
	inline int32_t* get_address_of__metadataVersion_4() { return &____metadataVersion_4; }
	inline void set__metadataVersion_4(int32_t value)
	{
		____metadataVersion_4 = value;
	}

	inline static int32_t get_offset_of__collisionIndex_5() { return static_cast<int32_t>(offsetof(TypeDescriptor_tEA9B846104F636343B5CBF281E8E7BE6349843AF_StaticFields, ____collisionIndex_5)); }
	inline int32_t get__collisionIndex_5() const { return ____collisionIndex_5; }
	inline int32_t* get_address_of__collisionIndex_5() { return &____collisionIndex_5; }
	inline void set__collisionIndex_5(int32_t value)
	{
		____collisionIndex_5 = value;
	}

	inline static int32_t get_offset_of_TraceDescriptor_6() { return static_cast<int32_t>(offsetof(TypeDescriptor_tEA9B846104F636343B5CBF281E8E7BE6349843AF_StaticFields, ___TraceDescriptor_6)); }
	inline BooleanSwitch_t2B51F0E7C50E7DAA74282B51EF9107A2E54723E0 * get_TraceDescriptor_6() const { return ___TraceDescriptor_6; }
	inline BooleanSwitch_t2B51F0E7C50E7DAA74282B51EF9107A2E54723E0 ** get_address_of_TraceDescriptor_6() { return &___TraceDescriptor_6; }
	inline void set_TraceDescriptor_6(BooleanSwitch_t2B51F0E7C50E7DAA74282B51EF9107A2E54723E0 * value)
	{
		___TraceDescriptor_6 = value;
		Il2CppCodeGenWriteBarrier((&___TraceDescriptor_6), value);
	}

	inline static int32_t get_offset_of__pipelineInitializeKeys_10() { return static_cast<int32_t>(offsetof(TypeDescriptor_tEA9B846104F636343B5CBF281E8E7BE6349843AF_StaticFields, ____pipelineInitializeKeys_10)); }
	inline GuidU5BU5D_t5CC024A2CAE5304311E0B961142A216C0972B0FF* get__pipelineInitializeKeys_10() const { return ____pipelineInitializeKeys_10; }
	inline GuidU5BU5D_t5CC024A2CAE5304311E0B961142A216C0972B0FF** get_address_of__pipelineInitializeKeys_10() { return &____pipelineInitializeKeys_10; }
	inline void set__pipelineInitializeKeys_10(GuidU5BU5D_t5CC024A2CAE5304311E0B961142A216C0972B0FF* value)
	{
		____pipelineInitializeKeys_10 = value;
		Il2CppCodeGenWriteBarrier((&____pipelineInitializeKeys_10), value);
	}

	inline static int32_t get_offset_of__pipelineMergeKeys_11() { return static_cast<int32_t>(offsetof(TypeDescriptor_tEA9B846104F636343B5CBF281E8E7BE6349843AF_StaticFields, ____pipelineMergeKeys_11)); }
	inline GuidU5BU5D_t5CC024A2CAE5304311E0B961142A216C0972B0FF* get__pipelineMergeKeys_11() const { return ____pipelineMergeKeys_11; }
	inline GuidU5BU5D_t5CC024A2CAE5304311E0B961142A216C0972B0FF** get_address_of__pipelineMergeKeys_11() { return &____pipelineMergeKeys_11; }
	inline void set__pipelineMergeKeys_11(GuidU5BU5D_t5CC024A2CAE5304311E0B961142A216C0972B0FF* value)
	{
		____pipelineMergeKeys_11 = value;
		Il2CppCodeGenWriteBarrier((&____pipelineMergeKeys_11), value);
	}

	inline static int32_t get_offset_of__pipelineFilterKeys_12() { return static_cast<int32_t>(offsetof(TypeDescriptor_tEA9B846104F636343B5CBF281E8E7BE6349843AF_StaticFields, ____pipelineFilterKeys_12)); }
	inline GuidU5BU5D_t5CC024A2CAE5304311E0B961142A216C0972B0FF* get__pipelineFilterKeys_12() const { return ____pipelineFilterKeys_12; }
	inline GuidU5BU5D_t5CC024A2CAE5304311E0B961142A216C0972B0FF** get_address_of__pipelineFilterKeys_12() { return &____pipelineFilterKeys_12; }
	inline void set__pipelineFilterKeys_12(GuidU5BU5D_t5CC024A2CAE5304311E0B961142A216C0972B0FF* value)
	{
		____pipelineFilterKeys_12 = value;
		Il2CppCodeGenWriteBarrier((&____pipelineFilterKeys_12), value);
	}

	inline static int32_t get_offset_of__pipelineAttributeFilterKeys_13() { return static_cast<int32_t>(offsetof(TypeDescriptor_tEA9B846104F636343B5CBF281E8E7BE6349843AF_StaticFields, ____pipelineAttributeFilterKeys_13)); }
	inline GuidU5BU5D_t5CC024A2CAE5304311E0B961142A216C0972B0FF* get__pipelineAttributeFilterKeys_13() const { return ____pipelineAttributeFilterKeys_13; }
	inline GuidU5BU5D_t5CC024A2CAE5304311E0B961142A216C0972B0FF** get_address_of__pipelineAttributeFilterKeys_13() { return &____pipelineAttributeFilterKeys_13; }
	inline void set__pipelineAttributeFilterKeys_13(GuidU5BU5D_t5CC024A2CAE5304311E0B961142A216C0972B0FF* value)
	{
		____pipelineAttributeFilterKeys_13 = value;
		Il2CppCodeGenWriteBarrier((&____pipelineAttributeFilterKeys_13), value);
	}

	inline static int32_t get_offset_of__internalSyncObject_14() { return static_cast<int32_t>(offsetof(TypeDescriptor_tEA9B846104F636343B5CBF281E8E7BE6349843AF_StaticFields, ____internalSyncObject_14)); }
	inline RuntimeObject * get__internalSyncObject_14() const { return ____internalSyncObject_14; }
	inline RuntimeObject ** get_address_of__internalSyncObject_14() { return &____internalSyncObject_14; }
	inline void set__internalSyncObject_14(RuntimeObject * value)
	{
		____internalSyncObject_14 = value;
		Il2CppCodeGenWriteBarrier((&____internalSyncObject_14), value);
	}

	inline static int32_t get_offset_of_Refreshed_15() { return static_cast<int32_t>(offsetof(TypeDescriptor_tEA9B846104F636343B5CBF281E8E7BE6349843AF_StaticFields, ___Refreshed_15)); }
	inline RefreshEventHandler_tE1B96D1B5C9580F60558220F9DEBA92333849F27 * get_Refreshed_15() const { return ___Refreshed_15; }
	inline RefreshEventHandler_tE1B96D1B5C9580F60558220F9DEBA92333849F27 ** get_address_of_Refreshed_15() { return &___Refreshed_15; }
	inline void set_Refreshed_15(RefreshEventHandler_tE1B96D1B5C9580F60558220F9DEBA92333849F27 * value)
	{
		___Refreshed_15 = value;
		Il2CppCodeGenWriteBarrier((&___Refreshed_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEDESCRIPTOR_TEA9B846104F636343B5CBF281E8E7BE6349843AF_H
#ifndef ATTRIBUTEFILTERCACHEITEM_T8E137227F1585B34CFC9BEC30C7D155203FA0D01_H
#define ATTRIBUTEFILTERCACHEITEM_T8E137227F1585B34CFC9BEC30C7D155203FA0D01_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.TypeDescriptor_AttributeFilterCacheItem
struct  AttributeFilterCacheItem_t8E137227F1585B34CFC9BEC30C7D155203FA0D01  : public RuntimeObject
{
public:
	// System.Attribute[] System.ComponentModel.TypeDescriptor_AttributeFilterCacheItem::_filter
	AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17* ____filter_0;
	// System.Collections.ICollection System.ComponentModel.TypeDescriptor_AttributeFilterCacheItem::FilteredMembers
	RuntimeObject* ___FilteredMembers_1;

public:
	inline static int32_t get_offset_of__filter_0() { return static_cast<int32_t>(offsetof(AttributeFilterCacheItem_t8E137227F1585B34CFC9BEC30C7D155203FA0D01, ____filter_0)); }
	inline AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17* get__filter_0() const { return ____filter_0; }
	inline AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17** get_address_of__filter_0() { return &____filter_0; }
	inline void set__filter_0(AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17* value)
	{
		____filter_0 = value;
		Il2CppCodeGenWriteBarrier((&____filter_0), value);
	}

	inline static int32_t get_offset_of_FilteredMembers_1() { return static_cast<int32_t>(offsetof(AttributeFilterCacheItem_t8E137227F1585B34CFC9BEC30C7D155203FA0D01, ___FilteredMembers_1)); }
	inline RuntimeObject* get_FilteredMembers_1() const { return ___FilteredMembers_1; }
	inline RuntimeObject** get_address_of_FilteredMembers_1() { return &___FilteredMembers_1; }
	inline void set_FilteredMembers_1(RuntimeObject* value)
	{
		___FilteredMembers_1 = value;
		Il2CppCodeGenWriteBarrier((&___FilteredMembers_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTEFILTERCACHEITEM_T8E137227F1585B34CFC9BEC30C7D155203FA0D01_H
#ifndef COMNATIVETYPEDESCRIPTOR_T44C4B23471F1F0C1174FD5E9EB670AD77A974A9D_H
#define COMNATIVETYPEDESCRIPTOR_T44C4B23471F1F0C1174FD5E9EB670AD77A974A9D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.TypeDescriptor_ComNativeDescriptionProvider_ComNativeTypeDescriptor
struct  ComNativeTypeDescriptor_t44C4B23471F1F0C1174FD5E9EB670AD77A974A9D  : public RuntimeObject
{
public:
	// System.ComponentModel.IComNativeDescriptorHandler System.ComponentModel.TypeDescriptor_ComNativeDescriptionProvider_ComNativeTypeDescriptor::_handler
	RuntimeObject* ____handler_0;
	// System.Object System.ComponentModel.TypeDescriptor_ComNativeDescriptionProvider_ComNativeTypeDescriptor::_instance
	RuntimeObject * ____instance_1;

public:
	inline static int32_t get_offset_of__handler_0() { return static_cast<int32_t>(offsetof(ComNativeTypeDescriptor_t44C4B23471F1F0C1174FD5E9EB670AD77A974A9D, ____handler_0)); }
	inline RuntimeObject* get__handler_0() const { return ____handler_0; }
	inline RuntimeObject** get_address_of__handler_0() { return &____handler_0; }
	inline void set__handler_0(RuntimeObject* value)
	{
		____handler_0 = value;
		Il2CppCodeGenWriteBarrier((&____handler_0), value);
	}

	inline static int32_t get_offset_of__instance_1() { return static_cast<int32_t>(offsetof(ComNativeTypeDescriptor_t44C4B23471F1F0C1174FD5E9EB670AD77A974A9D, ____instance_1)); }
	inline RuntimeObject * get__instance_1() const { return ____instance_1; }
	inline RuntimeObject ** get_address_of__instance_1() { return &____instance_1; }
	inline void set__instance_1(RuntimeObject * value)
	{
		____instance_1 = value;
		Il2CppCodeGenWriteBarrier((&____instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMNATIVETYPEDESCRIPTOR_T44C4B23471F1F0C1174FD5E9EB670AD77A974A9D_H
#ifndef FILTERCACHEITEM_TDA029760025F0503AD352DE639152D69CA138DA4_H
#define FILTERCACHEITEM_TDA029760025F0503AD352DE639152D69CA138DA4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.TypeDescriptor_FilterCacheItem
struct  FilterCacheItem_tDA029760025F0503AD352DE639152D69CA138DA4  : public RuntimeObject
{
public:
	// System.ComponentModel.Design.ITypeDescriptorFilterService System.ComponentModel.TypeDescriptor_FilterCacheItem::_filterService
	RuntimeObject* ____filterService_0;
	// System.Collections.ICollection System.ComponentModel.TypeDescriptor_FilterCacheItem::FilteredMembers
	RuntimeObject* ___FilteredMembers_1;

public:
	inline static int32_t get_offset_of__filterService_0() { return static_cast<int32_t>(offsetof(FilterCacheItem_tDA029760025F0503AD352DE639152D69CA138DA4, ____filterService_0)); }
	inline RuntimeObject* get__filterService_0() const { return ____filterService_0; }
	inline RuntimeObject** get_address_of__filterService_0() { return &____filterService_0; }
	inline void set__filterService_0(RuntimeObject* value)
	{
		____filterService_0 = value;
		Il2CppCodeGenWriteBarrier((&____filterService_0), value);
	}

	inline static int32_t get_offset_of_FilteredMembers_1() { return static_cast<int32_t>(offsetof(FilterCacheItem_tDA029760025F0503AD352DE639152D69CA138DA4, ___FilteredMembers_1)); }
	inline RuntimeObject* get_FilteredMembers_1() const { return ___FilteredMembers_1; }
	inline RuntimeObject** get_address_of_FilteredMembers_1() { return &___FilteredMembers_1; }
	inline void set_FilteredMembers_1(RuntimeObject* value)
	{
		___FilteredMembers_1 = value;
		Il2CppCodeGenWriteBarrier((&___FilteredMembers_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILTERCACHEITEM_TDA029760025F0503AD352DE639152D69CA138DA4_H
#ifndef MEMBERDESCRIPTORCOMPARER_T08208596E93DE2FB4472AB31B0316F98B288506D_H
#define MEMBERDESCRIPTORCOMPARER_T08208596E93DE2FB4472AB31B0316F98B288506D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.TypeDescriptor_MemberDescriptorComparer
struct  MemberDescriptorComparer_t08208596E93DE2FB4472AB31B0316F98B288506D  : public RuntimeObject
{
public:

public:
};

struct MemberDescriptorComparer_t08208596E93DE2FB4472AB31B0316F98B288506D_StaticFields
{
public:
	// System.ComponentModel.TypeDescriptor_MemberDescriptorComparer System.ComponentModel.TypeDescriptor_MemberDescriptorComparer::Instance
	MemberDescriptorComparer_t08208596E93DE2FB4472AB31B0316F98B288506D * ___Instance_0;

public:
	inline static int32_t get_offset_of_Instance_0() { return static_cast<int32_t>(offsetof(MemberDescriptorComparer_t08208596E93DE2FB4472AB31B0316F98B288506D_StaticFields, ___Instance_0)); }
	inline MemberDescriptorComparer_t08208596E93DE2FB4472AB31B0316F98B288506D * get_Instance_0() const { return ___Instance_0; }
	inline MemberDescriptorComparer_t08208596E93DE2FB4472AB31B0316F98B288506D ** get_address_of_Instance_0() { return &___Instance_0; }
	inline void set_Instance_0(MemberDescriptorComparer_t08208596E93DE2FB4472AB31B0316F98B288506D * value)
	{
		___Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERDESCRIPTORCOMPARER_T08208596E93DE2FB4472AB31B0316F98B288506D_H
#ifndef MERGEDTYPEDESCRIPTOR_T9395D9B4F23C94B7997A9ED698CBEF7995EB162B_H
#define MERGEDTYPEDESCRIPTOR_T9395D9B4F23C94B7997A9ED698CBEF7995EB162B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.TypeDescriptor_MergedTypeDescriptor
struct  MergedTypeDescriptor_t9395D9B4F23C94B7997A9ED698CBEF7995EB162B  : public RuntimeObject
{
public:
	// System.ComponentModel.ICustomTypeDescriptor System.ComponentModel.TypeDescriptor_MergedTypeDescriptor::_primary
	RuntimeObject* ____primary_0;
	// System.ComponentModel.ICustomTypeDescriptor System.ComponentModel.TypeDescriptor_MergedTypeDescriptor::_secondary
	RuntimeObject* ____secondary_1;

public:
	inline static int32_t get_offset_of__primary_0() { return static_cast<int32_t>(offsetof(MergedTypeDescriptor_t9395D9B4F23C94B7997A9ED698CBEF7995EB162B, ____primary_0)); }
	inline RuntimeObject* get__primary_0() const { return ____primary_0; }
	inline RuntimeObject** get_address_of__primary_0() { return &____primary_0; }
	inline void set__primary_0(RuntimeObject* value)
	{
		____primary_0 = value;
		Il2CppCodeGenWriteBarrier((&____primary_0), value);
	}

	inline static int32_t get_offset_of__secondary_1() { return static_cast<int32_t>(offsetof(MergedTypeDescriptor_t9395D9B4F23C94B7997A9ED698CBEF7995EB162B, ____secondary_1)); }
	inline RuntimeObject* get__secondary_1() const { return ____secondary_1; }
	inline RuntimeObject** get_address_of__secondary_1() { return &____secondary_1; }
	inline void set__secondary_1(RuntimeObject* value)
	{
		____secondary_1 = value;
		Il2CppCodeGenWriteBarrier((&____secondary_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MERGEDTYPEDESCRIPTOR_T9395D9B4F23C94B7997A9ED698CBEF7995EB162B_H
#ifndef TYPEDESCRIPTORCOMOBJECT_T531EE8D60EE83A09B6539D079E837E68DC289B76_H
#define TYPEDESCRIPTORCOMOBJECT_T531EE8D60EE83A09B6539D079E837E68DC289B76_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.TypeDescriptor_TypeDescriptorComObject
struct  TypeDescriptorComObject_t531EE8D60EE83A09B6539D079E837E68DC289B76  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEDESCRIPTORCOMOBJECT_T531EE8D60EE83A09B6539D079E837E68DC289B76_H
#ifndef TYPEDESCRIPTORINTERFACE_T872F528EF86C66103396D4D796A932A0FF88C4D7_H
#define TYPEDESCRIPTORINTERFACE_T872F528EF86C66103396D4D796A932A0FF88C4D7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.TypeDescriptor_TypeDescriptorInterface
struct  TypeDescriptorInterface_t872F528EF86C66103396D4D796A932A0FF88C4D7  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEDESCRIPTORINTERFACE_T872F528EF86C66103396D4D796A932A0FF88C4D7_H
#ifndef WEAKKEYCOMPARER_TFF7B45BE010723C01B3F3205FEBCC915543996B3_H
#define WEAKKEYCOMPARER_TFF7B45BE010723C01B3F3205FEBCC915543996B3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.WeakHashtable_WeakKeyComparer
struct  WeakKeyComparer_tFF7B45BE010723C01B3F3205FEBCC915543996B3  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEAKKEYCOMPARER_TFF7B45BE010723C01B3F3205FEBCC915543996B3_H
#ifndef EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#define EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventArgs
struct  EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E  : public RuntimeObject
{
public:

public:
};

struct EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields, ___Empty_0)); }
	inline EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef CRITICALFINALIZEROBJECT_T8B006E1DEE084E781F5C0F3283E9226E28894DD9_H
#define CRITICALFINALIZEROBJECT_T8B006E1DEE084E781F5C0F3283E9226E28894DD9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.ConstrainedExecution.CriticalFinalizerObject
struct  CriticalFinalizerObject_t8B006E1DEE084E781F5C0F3283E9226E28894DD9  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRITICALFINALIZEROBJECT_T8B006E1DEE084E781F5C0F3283E9226E28894DD9_H
#ifndef HANDLECOLLECTOR_TF2D4643449777DCA95FF738A225CC0BEB284D232_H
#define HANDLECOLLECTOR_TF2D4643449777DCA95FF738A225CC0BEB284D232_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.HandleCollector
struct  HandleCollector_tF2D4643449777DCA95FF738A225CC0BEB284D232  : public RuntimeObject
{
public:
	// System.String System.Runtime.InteropServices.HandleCollector::name
	String_t* ___name_1;
	// System.Int32 System.Runtime.InteropServices.HandleCollector::initialThreshold
	int32_t ___initialThreshold_2;
	// System.Int32 System.Runtime.InteropServices.HandleCollector::maximumThreshold
	int32_t ___maximumThreshold_3;
	// System.Int32 System.Runtime.InteropServices.HandleCollector::threshold
	int32_t ___threshold_4;
	// System.Int32 System.Runtime.InteropServices.HandleCollector::handleCount
	int32_t ___handleCount_5;
	// System.Int32[] System.Runtime.InteropServices.HandleCollector::gc_counts
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___gc_counts_6;
	// System.Int32 System.Runtime.InteropServices.HandleCollector::gc_gen
	int32_t ___gc_gen_7;

public:
	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(HandleCollector_tF2D4643449777DCA95FF738A225CC0BEB284D232, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}

	inline static int32_t get_offset_of_initialThreshold_2() { return static_cast<int32_t>(offsetof(HandleCollector_tF2D4643449777DCA95FF738A225CC0BEB284D232, ___initialThreshold_2)); }
	inline int32_t get_initialThreshold_2() const { return ___initialThreshold_2; }
	inline int32_t* get_address_of_initialThreshold_2() { return &___initialThreshold_2; }
	inline void set_initialThreshold_2(int32_t value)
	{
		___initialThreshold_2 = value;
	}

	inline static int32_t get_offset_of_maximumThreshold_3() { return static_cast<int32_t>(offsetof(HandleCollector_tF2D4643449777DCA95FF738A225CC0BEB284D232, ___maximumThreshold_3)); }
	inline int32_t get_maximumThreshold_3() const { return ___maximumThreshold_3; }
	inline int32_t* get_address_of_maximumThreshold_3() { return &___maximumThreshold_3; }
	inline void set_maximumThreshold_3(int32_t value)
	{
		___maximumThreshold_3 = value;
	}

	inline static int32_t get_offset_of_threshold_4() { return static_cast<int32_t>(offsetof(HandleCollector_tF2D4643449777DCA95FF738A225CC0BEB284D232, ___threshold_4)); }
	inline int32_t get_threshold_4() const { return ___threshold_4; }
	inline int32_t* get_address_of_threshold_4() { return &___threshold_4; }
	inline void set_threshold_4(int32_t value)
	{
		___threshold_4 = value;
	}

	inline static int32_t get_offset_of_handleCount_5() { return static_cast<int32_t>(offsetof(HandleCollector_tF2D4643449777DCA95FF738A225CC0BEB284D232, ___handleCount_5)); }
	inline int32_t get_handleCount_5() const { return ___handleCount_5; }
	inline int32_t* get_address_of_handleCount_5() { return &___handleCount_5; }
	inline void set_handleCount_5(int32_t value)
	{
		___handleCount_5 = value;
	}

	inline static int32_t get_offset_of_gc_counts_6() { return static_cast<int32_t>(offsetof(HandleCollector_tF2D4643449777DCA95FF738A225CC0BEB284D232, ___gc_counts_6)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_gc_counts_6() const { return ___gc_counts_6; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_gc_counts_6() { return &___gc_counts_6; }
	inline void set_gc_counts_6(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___gc_counts_6 = value;
		Il2CppCodeGenWriteBarrier((&___gc_counts_6), value);
	}

	inline static int32_t get_offset_of_gc_gen_7() { return static_cast<int32_t>(offsetof(HandleCollector_tF2D4643449777DCA95FF738A225CC0BEB284D232, ___gc_gen_7)); }
	inline int32_t get_gc_gen_7() const { return ___gc_gen_7; }
	inline int32_t* get_address_of_gc_gen_7() { return &___gc_gen_7; }
	inline void set_gc_gen_7(int32_t value)
	{
		___gc_gen_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HANDLECOLLECTOR_TF2D4643449777DCA95FF738A225CC0BEB284D232_H
#ifndef EXTENDEDPROTECTIONPOLICY_T5DB5E76D5F3E2BF3CAE465DF85198A91DF30A8BC_H
#define EXTENDEDPROTECTIONPOLICY_T5DB5E76D5F3E2BF3CAE465DF85198A91DF30A8BC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Authentication.ExtendedProtection.ExtendedProtectionPolicy
struct  ExtendedProtectionPolicy_t5DB5E76D5F3E2BF3CAE465DF85198A91DF30A8BC  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENDEDPROTECTIONPOLICY_T5DB5E76D5F3E2BF3CAE465DF85198A91DF30A8BC_H
#ifndef TOKENBINDING_TC9B02488C1CAC225092E43AD9D09F7213588313B_H
#define TOKENBINDING_TC9B02488C1CAC225092E43AD9D09F7213588313B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Authentication.ExtendedProtection.TokenBinding
struct  TokenBinding_tC9B02488C1CAC225092E43AD9D09F7213588313B  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOKENBINDING_TC9B02488C1CAC225092E43AD9D09F7213588313B_H
#ifndef ASNENCODEDDATA_T7D5EF5337DCAF507CAD7D750552C943F037A9D65_H
#define ASNENCODEDDATA_T7D5EF5337DCAF507CAD7D750552C943F037A9D65_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.AsnEncodedData
struct  AsnEncodedData_t7D5EF5337DCAF507CAD7D750552C943F037A9D65  : public RuntimeObject
{
public:
	// System.Security.Cryptography.Oid System.Security.Cryptography.AsnEncodedData::_oid
	Oid_tC00A10270EAF16BBF0F2619B9AEC883E0CFE6137 * ____oid_0;
	// System.Byte[] System.Security.Cryptography.AsnEncodedData::_raw
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____raw_1;

public:
	inline static int32_t get_offset_of__oid_0() { return static_cast<int32_t>(offsetof(AsnEncodedData_t7D5EF5337DCAF507CAD7D750552C943F037A9D65, ____oid_0)); }
	inline Oid_tC00A10270EAF16BBF0F2619B9AEC883E0CFE6137 * get__oid_0() const { return ____oid_0; }
	inline Oid_tC00A10270EAF16BBF0F2619B9AEC883E0CFE6137 ** get_address_of__oid_0() { return &____oid_0; }
	inline void set__oid_0(Oid_tC00A10270EAF16BBF0F2619B9AEC883E0CFE6137 * value)
	{
		____oid_0 = value;
		Il2CppCodeGenWriteBarrier((&____oid_0), value);
	}

	inline static int32_t get_offset_of__raw_1() { return static_cast<int32_t>(offsetof(AsnEncodedData_t7D5EF5337DCAF507CAD7D750552C943F037A9D65, ____raw_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__raw_1() const { return ____raw_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__raw_1() { return &____raw_1; }
	inline void set__raw_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____raw_1 = value;
		Il2CppCodeGenWriteBarrier((&____raw_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASNENCODEDDATA_T7D5EF5337DCAF507CAD7D750552C943F037A9D65_H
#ifndef CAPI_TEA68010AC3470FFEBC91FC9D3C13E7D7064C3267_H
#define CAPI_TEA68010AC3470FFEBC91FC9D3C13E7D7064C3267_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.CAPI
struct  CAPI_tEA68010AC3470FFEBC91FC9D3C13E7D7064C3267  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAPI_TEA68010AC3470FFEBC91FC9D3C13E7D7064C3267_H
#ifndef OIDCOLLECTION_TEB423F1150E53DCF513BF5A699F911586A31B94E_H
#define OIDCOLLECTION_TEB423F1150E53DCF513BF5A699F911586A31B94E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.OidCollection
struct  OidCollection_tEB423F1150E53DCF513BF5A699F911586A31B94E  : public RuntimeObject
{
public:
	// System.Collections.ArrayList System.Security.Cryptography.OidCollection::m_list
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ___m_list_0;

public:
	inline static int32_t get_offset_of_m_list_0() { return static_cast<int32_t>(offsetof(OidCollection_tEB423F1150E53DCF513BF5A699F911586A31B94E, ___m_list_0)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get_m_list_0() const { return ___m_list_0; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of_m_list_0() { return &___m_list_0; }
	inline void set_m_list_0(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		___m_list_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_list_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OIDCOLLECTION_TEB423F1150E53DCF513BF5A699F911586A31B94E_H
#ifndef OIDENUMERATOR_TC2DB288576C575B69F7934274DDD8A5868CEF97C_H
#define OIDENUMERATOR_TC2DB288576C575B69F7934274DDD8A5868CEF97C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.OidEnumerator
struct  OidEnumerator_tC2DB288576C575B69F7934274DDD8A5868CEF97C  : public RuntimeObject
{
public:
	// System.Security.Cryptography.OidCollection System.Security.Cryptography.OidEnumerator::m_oids
	OidCollection_tEB423F1150E53DCF513BF5A699F911586A31B94E * ___m_oids_0;
	// System.Int32 System.Security.Cryptography.OidEnumerator::m_current
	int32_t ___m_current_1;

public:
	inline static int32_t get_offset_of_m_oids_0() { return static_cast<int32_t>(offsetof(OidEnumerator_tC2DB288576C575B69F7934274DDD8A5868CEF97C, ___m_oids_0)); }
	inline OidCollection_tEB423F1150E53DCF513BF5A699F911586A31B94E * get_m_oids_0() const { return ___m_oids_0; }
	inline OidCollection_tEB423F1150E53DCF513BF5A699F911586A31B94E ** get_address_of_m_oids_0() { return &___m_oids_0; }
	inline void set_m_oids_0(OidCollection_tEB423F1150E53DCF513BF5A699F911586A31B94E * value)
	{
		___m_oids_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_oids_0), value);
	}

	inline static int32_t get_offset_of_m_current_1() { return static_cast<int32_t>(offsetof(OidEnumerator_tC2DB288576C575B69F7934274DDD8A5868CEF97C, ___m_current_1)); }
	inline int32_t get_m_current_1() const { return ___m_current_1; }
	inline int32_t* get_address_of_m_current_1() { return &___m_current_1; }
	inline void set_m_current_1(int32_t value)
	{
		___m_current_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OIDENUMERATOR_TC2DB288576C575B69F7934274DDD8A5868CEF97C_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#define BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifndef BITVECTOR32_TDE1C7920F117A283D9595A597572E8910691A0F1_H
#define BITVECTOR32_TDE1C7920F117A283D9595A597572E8910691A0F1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Specialized.BitVector32
struct  BitVector32_tDE1C7920F117A283D9595A597572E8910691A0F1 
{
public:
	// System.UInt32 System.Collections.Specialized.BitVector32::data
	uint32_t ___data_0;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(BitVector32_tDE1C7920F117A283D9595A597572E8910691A0F1, ___data_0)); }
	inline uint32_t get_data_0() const { return ___data_0; }
	inline uint32_t* get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(uint32_t value)
	{
		___data_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BITVECTOR32_TDE1C7920F117A283D9595A597572E8910691A0F1_H
#ifndef ASYNCCOMPLETEDEVENTARGS_TADCBE47368F4D8B198B46D332B27EE38C8B5F8B7_H
#define ASYNCCOMPLETEDEVENTARGS_TADCBE47368F4D8B198B46D332B27EE38C8B5F8B7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.AsyncCompletedEventArgs
struct  AsyncCompletedEventArgs_tADCBE47368F4D8B198B46D332B27EE38C8B5F8B7  : public EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E
{
public:
	// System.Exception System.ComponentModel.AsyncCompletedEventArgs::error
	Exception_t * ___error_1;
	// System.Boolean System.ComponentModel.AsyncCompletedEventArgs::cancelled
	bool ___cancelled_2;
	// System.Object System.ComponentModel.AsyncCompletedEventArgs::userState
	RuntimeObject * ___userState_3;

public:
	inline static int32_t get_offset_of_error_1() { return static_cast<int32_t>(offsetof(AsyncCompletedEventArgs_tADCBE47368F4D8B198B46D332B27EE38C8B5F8B7, ___error_1)); }
	inline Exception_t * get_error_1() const { return ___error_1; }
	inline Exception_t ** get_address_of_error_1() { return &___error_1; }
	inline void set_error_1(Exception_t * value)
	{
		___error_1 = value;
		Il2CppCodeGenWriteBarrier((&___error_1), value);
	}

	inline static int32_t get_offset_of_cancelled_2() { return static_cast<int32_t>(offsetof(AsyncCompletedEventArgs_tADCBE47368F4D8B198B46D332B27EE38C8B5F8B7, ___cancelled_2)); }
	inline bool get_cancelled_2() const { return ___cancelled_2; }
	inline bool* get_address_of_cancelled_2() { return &___cancelled_2; }
	inline void set_cancelled_2(bool value)
	{
		___cancelled_2 = value;
	}

	inline static int32_t get_offset_of_userState_3() { return static_cast<int32_t>(offsetof(AsyncCompletedEventArgs_tADCBE47368F4D8B198B46D332B27EE38C8B5F8B7, ___userState_3)); }
	inline RuntimeObject * get_userState_3() const { return ___userState_3; }
	inline RuntimeObject ** get_address_of_userState_3() { return &___userState_3; }
	inline void set_userState_3(RuntimeObject * value)
	{
		___userState_3 = value;
		Il2CppCodeGenWriteBarrier((&___userState_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCCOMPLETEDEVENTARGS_TADCBE47368F4D8B198B46D332B27EE38C8B5F8B7_H
#ifndef CATEGORYATTRIBUTE_T89C58D266A4A65CD58E04FF63344AA3E0AF57B80_H
#define CATEGORYATTRIBUTE_T89C58D266A4A65CD58E04FF63344AA3E0AF57B80_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.CategoryAttribute
struct  CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Boolean System.ComponentModel.CategoryAttribute::localized
	bool ___localized_14;
	// System.String System.ComponentModel.CategoryAttribute::categoryValue
	String_t* ___categoryValue_15;

public:
	inline static int32_t get_offset_of_localized_14() { return static_cast<int32_t>(offsetof(CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80, ___localized_14)); }
	inline bool get_localized_14() const { return ___localized_14; }
	inline bool* get_address_of_localized_14() { return &___localized_14; }
	inline void set_localized_14(bool value)
	{
		___localized_14 = value;
	}

	inline static int32_t get_offset_of_categoryValue_15() { return static_cast<int32_t>(offsetof(CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80, ___categoryValue_15)); }
	inline String_t* get_categoryValue_15() const { return ___categoryValue_15; }
	inline String_t** get_address_of_categoryValue_15() { return &___categoryValue_15; }
	inline void set_categoryValue_15(String_t* value)
	{
		___categoryValue_15 = value;
		Il2CppCodeGenWriteBarrier((&___categoryValue_15), value);
	}
};

struct CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80_StaticFields
{
public:
	// System.ComponentModel.CategoryAttribute modreq(System.Runtime.CompilerServices.IsVolatile) System.ComponentModel.CategoryAttribute::appearance
	CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * ___appearance_0;
	// System.ComponentModel.CategoryAttribute modreq(System.Runtime.CompilerServices.IsVolatile) System.ComponentModel.CategoryAttribute::asynchronous
	CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * ___asynchronous_1;
	// System.ComponentModel.CategoryAttribute modreq(System.Runtime.CompilerServices.IsVolatile) System.ComponentModel.CategoryAttribute::behavior
	CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * ___behavior_2;
	// System.ComponentModel.CategoryAttribute modreq(System.Runtime.CompilerServices.IsVolatile) System.ComponentModel.CategoryAttribute::data
	CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * ___data_3;
	// System.ComponentModel.CategoryAttribute modreq(System.Runtime.CompilerServices.IsVolatile) System.ComponentModel.CategoryAttribute::design
	CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * ___design_4;
	// System.ComponentModel.CategoryAttribute modreq(System.Runtime.CompilerServices.IsVolatile) System.ComponentModel.CategoryAttribute::action
	CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * ___action_5;
	// System.ComponentModel.CategoryAttribute modreq(System.Runtime.CompilerServices.IsVolatile) System.ComponentModel.CategoryAttribute::format
	CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * ___format_6;
	// System.ComponentModel.CategoryAttribute modreq(System.Runtime.CompilerServices.IsVolatile) System.ComponentModel.CategoryAttribute::layout
	CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * ___layout_7;
	// System.ComponentModel.CategoryAttribute modreq(System.Runtime.CompilerServices.IsVolatile) System.ComponentModel.CategoryAttribute::mouse
	CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * ___mouse_8;
	// System.ComponentModel.CategoryAttribute modreq(System.Runtime.CompilerServices.IsVolatile) System.ComponentModel.CategoryAttribute::key
	CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * ___key_9;
	// System.ComponentModel.CategoryAttribute modreq(System.Runtime.CompilerServices.IsVolatile) System.ComponentModel.CategoryAttribute::focus
	CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * ___focus_10;
	// System.ComponentModel.CategoryAttribute modreq(System.Runtime.CompilerServices.IsVolatile) System.ComponentModel.CategoryAttribute::windowStyle
	CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * ___windowStyle_11;
	// System.ComponentModel.CategoryAttribute modreq(System.Runtime.CompilerServices.IsVolatile) System.ComponentModel.CategoryAttribute::dragDrop
	CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * ___dragDrop_12;
	// System.ComponentModel.CategoryAttribute modreq(System.Runtime.CompilerServices.IsVolatile) System.ComponentModel.CategoryAttribute::defAttr
	CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * ___defAttr_13;

public:
	inline static int32_t get_offset_of_appearance_0() { return static_cast<int32_t>(offsetof(CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80_StaticFields, ___appearance_0)); }
	inline CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * get_appearance_0() const { return ___appearance_0; }
	inline CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 ** get_address_of_appearance_0() { return &___appearance_0; }
	inline void set_appearance_0(CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * value)
	{
		___appearance_0 = value;
		Il2CppCodeGenWriteBarrier((&___appearance_0), value);
	}

	inline static int32_t get_offset_of_asynchronous_1() { return static_cast<int32_t>(offsetof(CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80_StaticFields, ___asynchronous_1)); }
	inline CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * get_asynchronous_1() const { return ___asynchronous_1; }
	inline CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 ** get_address_of_asynchronous_1() { return &___asynchronous_1; }
	inline void set_asynchronous_1(CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * value)
	{
		___asynchronous_1 = value;
		Il2CppCodeGenWriteBarrier((&___asynchronous_1), value);
	}

	inline static int32_t get_offset_of_behavior_2() { return static_cast<int32_t>(offsetof(CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80_StaticFields, ___behavior_2)); }
	inline CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * get_behavior_2() const { return ___behavior_2; }
	inline CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 ** get_address_of_behavior_2() { return &___behavior_2; }
	inline void set_behavior_2(CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * value)
	{
		___behavior_2 = value;
		Il2CppCodeGenWriteBarrier((&___behavior_2), value);
	}

	inline static int32_t get_offset_of_data_3() { return static_cast<int32_t>(offsetof(CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80_StaticFields, ___data_3)); }
	inline CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * get_data_3() const { return ___data_3; }
	inline CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 ** get_address_of_data_3() { return &___data_3; }
	inline void set_data_3(CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * value)
	{
		___data_3 = value;
		Il2CppCodeGenWriteBarrier((&___data_3), value);
	}

	inline static int32_t get_offset_of_design_4() { return static_cast<int32_t>(offsetof(CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80_StaticFields, ___design_4)); }
	inline CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * get_design_4() const { return ___design_4; }
	inline CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 ** get_address_of_design_4() { return &___design_4; }
	inline void set_design_4(CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * value)
	{
		___design_4 = value;
		Il2CppCodeGenWriteBarrier((&___design_4), value);
	}

	inline static int32_t get_offset_of_action_5() { return static_cast<int32_t>(offsetof(CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80_StaticFields, ___action_5)); }
	inline CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * get_action_5() const { return ___action_5; }
	inline CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 ** get_address_of_action_5() { return &___action_5; }
	inline void set_action_5(CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * value)
	{
		___action_5 = value;
		Il2CppCodeGenWriteBarrier((&___action_5), value);
	}

	inline static int32_t get_offset_of_format_6() { return static_cast<int32_t>(offsetof(CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80_StaticFields, ___format_6)); }
	inline CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * get_format_6() const { return ___format_6; }
	inline CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 ** get_address_of_format_6() { return &___format_6; }
	inline void set_format_6(CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * value)
	{
		___format_6 = value;
		Il2CppCodeGenWriteBarrier((&___format_6), value);
	}

	inline static int32_t get_offset_of_layout_7() { return static_cast<int32_t>(offsetof(CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80_StaticFields, ___layout_7)); }
	inline CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * get_layout_7() const { return ___layout_7; }
	inline CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 ** get_address_of_layout_7() { return &___layout_7; }
	inline void set_layout_7(CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * value)
	{
		___layout_7 = value;
		Il2CppCodeGenWriteBarrier((&___layout_7), value);
	}

	inline static int32_t get_offset_of_mouse_8() { return static_cast<int32_t>(offsetof(CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80_StaticFields, ___mouse_8)); }
	inline CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * get_mouse_8() const { return ___mouse_8; }
	inline CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 ** get_address_of_mouse_8() { return &___mouse_8; }
	inline void set_mouse_8(CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * value)
	{
		___mouse_8 = value;
		Il2CppCodeGenWriteBarrier((&___mouse_8), value);
	}

	inline static int32_t get_offset_of_key_9() { return static_cast<int32_t>(offsetof(CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80_StaticFields, ___key_9)); }
	inline CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * get_key_9() const { return ___key_9; }
	inline CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 ** get_address_of_key_9() { return &___key_9; }
	inline void set_key_9(CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * value)
	{
		___key_9 = value;
		Il2CppCodeGenWriteBarrier((&___key_9), value);
	}

	inline static int32_t get_offset_of_focus_10() { return static_cast<int32_t>(offsetof(CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80_StaticFields, ___focus_10)); }
	inline CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * get_focus_10() const { return ___focus_10; }
	inline CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 ** get_address_of_focus_10() { return &___focus_10; }
	inline void set_focus_10(CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * value)
	{
		___focus_10 = value;
		Il2CppCodeGenWriteBarrier((&___focus_10), value);
	}

	inline static int32_t get_offset_of_windowStyle_11() { return static_cast<int32_t>(offsetof(CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80_StaticFields, ___windowStyle_11)); }
	inline CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * get_windowStyle_11() const { return ___windowStyle_11; }
	inline CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 ** get_address_of_windowStyle_11() { return &___windowStyle_11; }
	inline void set_windowStyle_11(CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * value)
	{
		___windowStyle_11 = value;
		Il2CppCodeGenWriteBarrier((&___windowStyle_11), value);
	}

	inline static int32_t get_offset_of_dragDrop_12() { return static_cast<int32_t>(offsetof(CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80_StaticFields, ___dragDrop_12)); }
	inline CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * get_dragDrop_12() const { return ___dragDrop_12; }
	inline CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 ** get_address_of_dragDrop_12() { return &___dragDrop_12; }
	inline void set_dragDrop_12(CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * value)
	{
		___dragDrop_12 = value;
		Il2CppCodeGenWriteBarrier((&___dragDrop_12), value);
	}

	inline static int32_t get_offset_of_defAttr_13() { return static_cast<int32_t>(offsetof(CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80_StaticFields, ___defAttr_13)); }
	inline CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * get_defAttr_13() const { return ___defAttr_13; }
	inline CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 ** get_address_of_defAttr_13() { return &___defAttr_13; }
	inline void set_defAttr_13(CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80 * value)
	{
		___defAttr_13 = value;
		Il2CppCodeGenWriteBarrier((&___defAttr_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CATEGORYATTRIBUTE_T89C58D266A4A65CD58E04FF63344AA3E0AF57B80_H
#ifndef DESIGNTIMELICENSECONTEXT_TFBEF4E72A1E0E7F03A01D35ADFEE86A23F848AA1_H
#define DESIGNTIMELICENSECONTEXT_TFBEF4E72A1E0E7F03A01D35ADFEE86A23F848AA1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.Design.DesigntimeLicenseContext
struct  DesigntimeLicenseContext_tFBEF4E72A1E0E7F03A01D35ADFEE86A23F848AA1  : public LicenseContext_tE7068766A5D105EA910974E3F08D0AF7534A806A
{
public:
	// System.Collections.Hashtable System.ComponentModel.Design.DesigntimeLicenseContext::savedLicenseKeys
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___savedLicenseKeys_0;

public:
	inline static int32_t get_offset_of_savedLicenseKeys_0() { return static_cast<int32_t>(offsetof(DesigntimeLicenseContext_tFBEF4E72A1E0E7F03A01D35ADFEE86A23F848AA1, ___savedLicenseKeys_0)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_savedLicenseKeys_0() const { return ___savedLicenseKeys_0; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_savedLicenseKeys_0() { return &___savedLicenseKeys_0; }
	inline void set_savedLicenseKeys_0(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___savedLicenseKeys_0 = value;
		Il2CppCodeGenWriteBarrier((&___savedLicenseKeys_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESIGNTIMELICENSECONTEXT_TFBEF4E72A1E0E7F03A01D35ADFEE86A23F848AA1_H
#ifndef RUNTIMELICENSECONTEXT_T1367392EED5E28C90B5128BF01CA0B846120C641_H
#define RUNTIMELICENSECONTEXT_T1367392EED5E28C90B5128BF01CA0B846120C641_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.Design.RuntimeLicenseContext
struct  RuntimeLicenseContext_t1367392EED5E28C90B5128BF01CA0B846120C641  : public LicenseContext_tE7068766A5D105EA910974E3F08D0AF7534A806A
{
public:
	// System.Collections.Hashtable System.ComponentModel.Design.RuntimeLicenseContext::savedLicenseKeys
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___savedLicenseKeys_1;

public:
	inline static int32_t get_offset_of_savedLicenseKeys_1() { return static_cast<int32_t>(offsetof(RuntimeLicenseContext_t1367392EED5E28C90B5128BF01CA0B846120C641, ___savedLicenseKeys_1)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_savedLicenseKeys_1() const { return ___savedLicenseKeys_1; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_savedLicenseKeys_1() { return &___savedLicenseKeys_1; }
	inline void set_savedLicenseKeys_1(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___savedLicenseKeys_1 = value;
		Il2CppCodeGenWriteBarrier((&___savedLicenseKeys_1), value);
	}
};

struct RuntimeLicenseContext_t1367392EED5E28C90B5128BF01CA0B846120C641_StaticFields
{
public:
	// System.Diagnostics.TraceSwitch System.ComponentModel.Design.RuntimeLicenseContext::RuntimeLicenseContextSwitch
	TraceSwitch_t32D210D5C9B05D9E555925260EEC3767BA895EC8 * ___RuntimeLicenseContextSwitch_0;

public:
	inline static int32_t get_offset_of_RuntimeLicenseContextSwitch_0() { return static_cast<int32_t>(offsetof(RuntimeLicenseContext_t1367392EED5E28C90B5128BF01CA0B846120C641_StaticFields, ___RuntimeLicenseContextSwitch_0)); }
	inline TraceSwitch_t32D210D5C9B05D9E555925260EEC3767BA895EC8 * get_RuntimeLicenseContextSwitch_0() const { return ___RuntimeLicenseContextSwitch_0; }
	inline TraceSwitch_t32D210D5C9B05D9E555925260EEC3767BA895EC8 ** get_address_of_RuntimeLicenseContextSwitch_0() { return &___RuntimeLicenseContextSwitch_0; }
	inline void set_RuntimeLicenseContextSwitch_0(TraceSwitch_t32D210D5C9B05D9E555925260EEC3767BA895EC8 * value)
	{
		___RuntimeLicenseContextSwitch_0 = value;
		Il2CppCodeGenWriteBarrier((&___RuntimeLicenseContextSwitch_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMELICENSECONTEXT_T1367392EED5E28C90B5128BF01CA0B846120C641_H
#ifndef DESIGNERSERIALIZERATTRIBUTE_T2EA1CB464E794E9A45402A813B5702E3ACE609DE_H
#define DESIGNERSERIALIZERATTRIBUTE_T2EA1CB464E794E9A45402A813B5702E3ACE609DE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.Design.Serialization.DesignerSerializerAttribute
struct  DesignerSerializerAttribute_t2EA1CB464E794E9A45402A813B5702E3ACE609DE  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String System.ComponentModel.Design.Serialization.DesignerSerializerAttribute::serializerTypeName
	String_t* ___serializerTypeName_0;
	// System.String System.ComponentModel.Design.Serialization.DesignerSerializerAttribute::serializerBaseTypeName
	String_t* ___serializerBaseTypeName_1;
	// System.String System.ComponentModel.Design.Serialization.DesignerSerializerAttribute::typeId
	String_t* ___typeId_2;

public:
	inline static int32_t get_offset_of_serializerTypeName_0() { return static_cast<int32_t>(offsetof(DesignerSerializerAttribute_t2EA1CB464E794E9A45402A813B5702E3ACE609DE, ___serializerTypeName_0)); }
	inline String_t* get_serializerTypeName_0() const { return ___serializerTypeName_0; }
	inline String_t** get_address_of_serializerTypeName_0() { return &___serializerTypeName_0; }
	inline void set_serializerTypeName_0(String_t* value)
	{
		___serializerTypeName_0 = value;
		Il2CppCodeGenWriteBarrier((&___serializerTypeName_0), value);
	}

	inline static int32_t get_offset_of_serializerBaseTypeName_1() { return static_cast<int32_t>(offsetof(DesignerSerializerAttribute_t2EA1CB464E794E9A45402A813B5702E3ACE609DE, ___serializerBaseTypeName_1)); }
	inline String_t* get_serializerBaseTypeName_1() const { return ___serializerBaseTypeName_1; }
	inline String_t** get_address_of_serializerBaseTypeName_1() { return &___serializerBaseTypeName_1; }
	inline void set_serializerBaseTypeName_1(String_t* value)
	{
		___serializerBaseTypeName_1 = value;
		Il2CppCodeGenWriteBarrier((&___serializerBaseTypeName_1), value);
	}

	inline static int32_t get_offset_of_typeId_2() { return static_cast<int32_t>(offsetof(DesignerSerializerAttribute_t2EA1CB464E794E9A45402A813B5702E3ACE609DE, ___typeId_2)); }
	inline String_t* get_typeId_2() const { return ___typeId_2; }
	inline String_t** get_address_of_typeId_2() { return &___typeId_2; }
	inline void set_typeId_2(String_t* value)
	{
		___typeId_2 = value;
		Il2CppCodeGenWriteBarrier((&___typeId_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESIGNERSERIALIZERATTRIBUTE_T2EA1CB464E794E9A45402A813B5702E3ACE609DE_H
#ifndef ROOTDESIGNERSERIALIZERATTRIBUTE_TD5A87C7E5CB002D859780E1BEF96D7E1214CC0AA_H
#define ROOTDESIGNERSERIALIZERATTRIBUTE_TD5A87C7E5CB002D859780E1BEF96D7E1214CC0AA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.Design.Serialization.RootDesignerSerializerAttribute
struct  RootDesignerSerializerAttribute_tD5A87C7E5CB002D859780E1BEF96D7E1214CC0AA  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Boolean System.ComponentModel.Design.Serialization.RootDesignerSerializerAttribute::reloadable
	bool ___reloadable_0;
	// System.String System.ComponentModel.Design.Serialization.RootDesignerSerializerAttribute::serializerTypeName
	String_t* ___serializerTypeName_1;
	// System.String System.ComponentModel.Design.Serialization.RootDesignerSerializerAttribute::serializerBaseTypeName
	String_t* ___serializerBaseTypeName_2;
	// System.String System.ComponentModel.Design.Serialization.RootDesignerSerializerAttribute::typeId
	String_t* ___typeId_3;

public:
	inline static int32_t get_offset_of_reloadable_0() { return static_cast<int32_t>(offsetof(RootDesignerSerializerAttribute_tD5A87C7E5CB002D859780E1BEF96D7E1214CC0AA, ___reloadable_0)); }
	inline bool get_reloadable_0() const { return ___reloadable_0; }
	inline bool* get_address_of_reloadable_0() { return &___reloadable_0; }
	inline void set_reloadable_0(bool value)
	{
		___reloadable_0 = value;
	}

	inline static int32_t get_offset_of_serializerTypeName_1() { return static_cast<int32_t>(offsetof(RootDesignerSerializerAttribute_tD5A87C7E5CB002D859780E1BEF96D7E1214CC0AA, ___serializerTypeName_1)); }
	inline String_t* get_serializerTypeName_1() const { return ___serializerTypeName_1; }
	inline String_t** get_address_of_serializerTypeName_1() { return &___serializerTypeName_1; }
	inline void set_serializerTypeName_1(String_t* value)
	{
		___serializerTypeName_1 = value;
		Il2CppCodeGenWriteBarrier((&___serializerTypeName_1), value);
	}

	inline static int32_t get_offset_of_serializerBaseTypeName_2() { return static_cast<int32_t>(offsetof(RootDesignerSerializerAttribute_tD5A87C7E5CB002D859780E1BEF96D7E1214CC0AA, ___serializerBaseTypeName_2)); }
	inline String_t* get_serializerBaseTypeName_2() const { return ___serializerBaseTypeName_2; }
	inline String_t** get_address_of_serializerBaseTypeName_2() { return &___serializerBaseTypeName_2; }
	inline void set_serializerBaseTypeName_2(String_t* value)
	{
		___serializerBaseTypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___serializerBaseTypeName_2), value);
	}

	inline static int32_t get_offset_of_typeId_3() { return static_cast<int32_t>(offsetof(RootDesignerSerializerAttribute_tD5A87C7E5CB002D859780E1BEF96D7E1214CC0AA, ___typeId_3)); }
	inline String_t* get_typeId_3() const { return ___typeId_3; }
	inline String_t** get_address_of_typeId_3() { return &___typeId_3; }
	inline void set_typeId_3(String_t* value)
	{
		___typeId_3 = value;
		Il2CppCodeGenWriteBarrier((&___typeId_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROOTDESIGNERSERIALIZERATTRIBUTE_TD5A87C7E5CB002D859780E1BEF96D7E1214CC0AA_H
#ifndef EVENTDESCRIPTOR_TAB488D66C0409A1889EE56355848CDA43ED95222_H
#define EVENTDESCRIPTOR_TAB488D66C0409A1889EE56355848CDA43ED95222_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.EventDescriptor
struct  EventDescriptor_tAB488D66C0409A1889EE56355848CDA43ED95222  : public MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTDESCRIPTOR_TAB488D66C0409A1889EE56355848CDA43ED95222_H
#ifndef NOTIFYPARENTPROPERTYATTRIBUTE_TCC257721622A29BC31EF1BFDC645E020EC7F4D68_H
#define NOTIFYPARENTPROPERTYATTRIBUTE_TCC257721622A29BC31EF1BFDC645E020EC7F4D68_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.NotifyParentPropertyAttribute
struct  NotifyParentPropertyAttribute_tCC257721622A29BC31EF1BFDC645E020EC7F4D68  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Boolean System.ComponentModel.NotifyParentPropertyAttribute::notifyParent
	bool ___notifyParent_3;

public:
	inline static int32_t get_offset_of_notifyParent_3() { return static_cast<int32_t>(offsetof(NotifyParentPropertyAttribute_tCC257721622A29BC31EF1BFDC645E020EC7F4D68, ___notifyParent_3)); }
	inline bool get_notifyParent_3() const { return ___notifyParent_3; }
	inline bool* get_address_of_notifyParent_3() { return &___notifyParent_3; }
	inline void set_notifyParent_3(bool value)
	{
		___notifyParent_3 = value;
	}
};

struct NotifyParentPropertyAttribute_tCC257721622A29BC31EF1BFDC645E020EC7F4D68_StaticFields
{
public:
	// System.ComponentModel.NotifyParentPropertyAttribute System.ComponentModel.NotifyParentPropertyAttribute::Yes
	NotifyParentPropertyAttribute_tCC257721622A29BC31EF1BFDC645E020EC7F4D68 * ___Yes_0;
	// System.ComponentModel.NotifyParentPropertyAttribute System.ComponentModel.NotifyParentPropertyAttribute::No
	NotifyParentPropertyAttribute_tCC257721622A29BC31EF1BFDC645E020EC7F4D68 * ___No_1;
	// System.ComponentModel.NotifyParentPropertyAttribute System.ComponentModel.NotifyParentPropertyAttribute::Default
	NotifyParentPropertyAttribute_tCC257721622A29BC31EF1BFDC645E020EC7F4D68 * ___Default_2;

public:
	inline static int32_t get_offset_of_Yes_0() { return static_cast<int32_t>(offsetof(NotifyParentPropertyAttribute_tCC257721622A29BC31EF1BFDC645E020EC7F4D68_StaticFields, ___Yes_0)); }
	inline NotifyParentPropertyAttribute_tCC257721622A29BC31EF1BFDC645E020EC7F4D68 * get_Yes_0() const { return ___Yes_0; }
	inline NotifyParentPropertyAttribute_tCC257721622A29BC31EF1BFDC645E020EC7F4D68 ** get_address_of_Yes_0() { return &___Yes_0; }
	inline void set_Yes_0(NotifyParentPropertyAttribute_tCC257721622A29BC31EF1BFDC645E020EC7F4D68 * value)
	{
		___Yes_0 = value;
		Il2CppCodeGenWriteBarrier((&___Yes_0), value);
	}

	inline static int32_t get_offset_of_No_1() { return static_cast<int32_t>(offsetof(NotifyParentPropertyAttribute_tCC257721622A29BC31EF1BFDC645E020EC7F4D68_StaticFields, ___No_1)); }
	inline NotifyParentPropertyAttribute_tCC257721622A29BC31EF1BFDC645E020EC7F4D68 * get_No_1() const { return ___No_1; }
	inline NotifyParentPropertyAttribute_tCC257721622A29BC31EF1BFDC645E020EC7F4D68 ** get_address_of_No_1() { return &___No_1; }
	inline void set_No_1(NotifyParentPropertyAttribute_tCC257721622A29BC31EF1BFDC645E020EC7F4D68 * value)
	{
		___No_1 = value;
		Il2CppCodeGenWriteBarrier((&___No_1), value);
	}

	inline static int32_t get_offset_of_Default_2() { return static_cast<int32_t>(offsetof(NotifyParentPropertyAttribute_tCC257721622A29BC31EF1BFDC645E020EC7F4D68_StaticFields, ___Default_2)); }
	inline NotifyParentPropertyAttribute_tCC257721622A29BC31EF1BFDC645E020EC7F4D68 * get_Default_2() const { return ___Default_2; }
	inline NotifyParentPropertyAttribute_tCC257721622A29BC31EF1BFDC645E020EC7F4D68 ** get_address_of_Default_2() { return &___Default_2; }
	inline void set_Default_2(NotifyParentPropertyAttribute_tCC257721622A29BC31EF1BFDC645E020EC7F4D68 * value)
	{
		___Default_2 = value;
		Il2CppCodeGenWriteBarrier((&___Default_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTIFYPARENTPROPERTYATTRIBUTE_TCC257721622A29BC31EF1BFDC645E020EC7F4D68_H
#ifndef PARENTHESIZEPROPERTYNAMEATTRIBUTE_T8C7EB4B9459246DAF7283E2D585D1D59F9886114_H
#define PARENTHESIZEPROPERTYNAMEATTRIBUTE_T8C7EB4B9459246DAF7283E2D585D1D59F9886114_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.ParenthesizePropertyNameAttribute
struct  ParenthesizePropertyNameAttribute_t8C7EB4B9459246DAF7283E2D585D1D59F9886114  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Boolean System.ComponentModel.ParenthesizePropertyNameAttribute::needParenthesis
	bool ___needParenthesis_1;

public:
	inline static int32_t get_offset_of_needParenthesis_1() { return static_cast<int32_t>(offsetof(ParenthesizePropertyNameAttribute_t8C7EB4B9459246DAF7283E2D585D1D59F9886114, ___needParenthesis_1)); }
	inline bool get_needParenthesis_1() const { return ___needParenthesis_1; }
	inline bool* get_address_of_needParenthesis_1() { return &___needParenthesis_1; }
	inline void set_needParenthesis_1(bool value)
	{
		___needParenthesis_1 = value;
	}
};

struct ParenthesizePropertyNameAttribute_t8C7EB4B9459246DAF7283E2D585D1D59F9886114_StaticFields
{
public:
	// System.ComponentModel.ParenthesizePropertyNameAttribute System.ComponentModel.ParenthesizePropertyNameAttribute::Default
	ParenthesizePropertyNameAttribute_t8C7EB4B9459246DAF7283E2D585D1D59F9886114 * ___Default_0;

public:
	inline static int32_t get_offset_of_Default_0() { return static_cast<int32_t>(offsetof(ParenthesizePropertyNameAttribute_t8C7EB4B9459246DAF7283E2D585D1D59F9886114_StaticFields, ___Default_0)); }
	inline ParenthesizePropertyNameAttribute_t8C7EB4B9459246DAF7283E2D585D1D59F9886114 * get_Default_0() const { return ___Default_0; }
	inline ParenthesizePropertyNameAttribute_t8C7EB4B9459246DAF7283E2D585D1D59F9886114 ** get_address_of_Default_0() { return &___Default_0; }
	inline void set_Default_0(ParenthesizePropertyNameAttribute_t8C7EB4B9459246DAF7283E2D585D1D59F9886114 * value)
	{
		___Default_0 = value;
		Il2CppCodeGenWriteBarrier((&___Default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARENTHESIZEPROPERTYNAMEATTRIBUTE_T8C7EB4B9459246DAF7283E2D585D1D59F9886114_H
#ifndef PROPERTYDESCRIPTOR_TBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D_H
#define PROPERTYDESCRIPTOR_TBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.PropertyDescriptor
struct  PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D  : public MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8
{
public:
	// System.ComponentModel.TypeConverter System.ComponentModel.PropertyDescriptor::converter
	TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB * ___converter_12;
	// System.Collections.Hashtable System.ComponentModel.PropertyDescriptor::valueChangedHandlers
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___valueChangedHandlers_13;
	// System.Object[] System.ComponentModel.PropertyDescriptor::editors
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___editors_14;
	// System.Type[] System.ComponentModel.PropertyDescriptor::editorTypes
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___editorTypes_15;
	// System.Int32 System.ComponentModel.PropertyDescriptor::editorCount
	int32_t ___editorCount_16;

public:
	inline static int32_t get_offset_of_converter_12() { return static_cast<int32_t>(offsetof(PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D, ___converter_12)); }
	inline TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB * get_converter_12() const { return ___converter_12; }
	inline TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB ** get_address_of_converter_12() { return &___converter_12; }
	inline void set_converter_12(TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB * value)
	{
		___converter_12 = value;
		Il2CppCodeGenWriteBarrier((&___converter_12), value);
	}

	inline static int32_t get_offset_of_valueChangedHandlers_13() { return static_cast<int32_t>(offsetof(PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D, ___valueChangedHandlers_13)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_valueChangedHandlers_13() const { return ___valueChangedHandlers_13; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_valueChangedHandlers_13() { return &___valueChangedHandlers_13; }
	inline void set_valueChangedHandlers_13(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___valueChangedHandlers_13 = value;
		Il2CppCodeGenWriteBarrier((&___valueChangedHandlers_13), value);
	}

	inline static int32_t get_offset_of_editors_14() { return static_cast<int32_t>(offsetof(PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D, ___editors_14)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_editors_14() const { return ___editors_14; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_editors_14() { return &___editors_14; }
	inline void set_editors_14(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___editors_14 = value;
		Il2CppCodeGenWriteBarrier((&___editors_14), value);
	}

	inline static int32_t get_offset_of_editorTypes_15() { return static_cast<int32_t>(offsetof(PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D, ___editorTypes_15)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get_editorTypes_15() const { return ___editorTypes_15; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of_editorTypes_15() { return &___editorTypes_15; }
	inline void set_editorTypes_15(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		___editorTypes_15 = value;
		Il2CppCodeGenWriteBarrier((&___editorTypes_15), value);
	}

	inline static int32_t get_offset_of_editorCount_16() { return static_cast<int32_t>(offsetof(PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D, ___editorCount_16)); }
	inline int32_t get_editorCount_16() const { return ___editorCount_16; }
	inline int32_t* get_address_of_editorCount_16() { return &___editorCount_16; }
	inline void set_editorCount_16(int32_t value)
	{
		___editorCount_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYDESCRIPTOR_TBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D_H
#ifndef PROPERTYTABATTRIBUTE_TCC9C7766C8A49D5FEDB5C51D2391A9301F3CCBE8_H
#define PROPERTYTABATTRIBUTE_TCC9C7766C8A49D5FEDB5C51D2391A9301F3CCBE8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.PropertyTabAttribute
struct  PropertyTabAttribute_tCC9C7766C8A49D5FEDB5C51D2391A9301F3CCBE8  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.ComponentModel.PropertyTabScope[] System.ComponentModel.PropertyTabAttribute::tabScopes
	PropertyTabScopeU5BU5D_t06DD8B6326FEE6EB77AA915202CFE3C2ABAC7260* ___tabScopes_0;
	// System.Type[] System.ComponentModel.PropertyTabAttribute::tabClasses
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___tabClasses_1;
	// System.String[] System.ComponentModel.PropertyTabAttribute::tabClassNames
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___tabClassNames_2;

public:
	inline static int32_t get_offset_of_tabScopes_0() { return static_cast<int32_t>(offsetof(PropertyTabAttribute_tCC9C7766C8A49D5FEDB5C51D2391A9301F3CCBE8, ___tabScopes_0)); }
	inline PropertyTabScopeU5BU5D_t06DD8B6326FEE6EB77AA915202CFE3C2ABAC7260* get_tabScopes_0() const { return ___tabScopes_0; }
	inline PropertyTabScopeU5BU5D_t06DD8B6326FEE6EB77AA915202CFE3C2ABAC7260** get_address_of_tabScopes_0() { return &___tabScopes_0; }
	inline void set_tabScopes_0(PropertyTabScopeU5BU5D_t06DD8B6326FEE6EB77AA915202CFE3C2ABAC7260* value)
	{
		___tabScopes_0 = value;
		Il2CppCodeGenWriteBarrier((&___tabScopes_0), value);
	}

	inline static int32_t get_offset_of_tabClasses_1() { return static_cast<int32_t>(offsetof(PropertyTabAttribute_tCC9C7766C8A49D5FEDB5C51D2391A9301F3CCBE8, ___tabClasses_1)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get_tabClasses_1() const { return ___tabClasses_1; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of_tabClasses_1() { return &___tabClasses_1; }
	inline void set_tabClasses_1(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		___tabClasses_1 = value;
		Il2CppCodeGenWriteBarrier((&___tabClasses_1), value);
	}

	inline static int32_t get_offset_of_tabClassNames_2() { return static_cast<int32_t>(offsetof(PropertyTabAttribute_tCC9C7766C8A49D5FEDB5C51D2391A9301F3CCBE8, ___tabClassNames_2)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_tabClassNames_2() const { return ___tabClassNames_2; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_tabClassNames_2() { return &___tabClassNames_2; }
	inline void set_tabClassNames_2(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___tabClassNames_2 = value;
		Il2CppCodeGenWriteBarrier((&___tabClassNames_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYTABATTRIBUTE_TCC9C7766C8A49D5FEDB5C51D2391A9301F3CCBE8_H
#ifndef READONLYATTRIBUTE_T5805F62303A0DEDAE657066BA289E3249FA6A35B_H
#define READONLYATTRIBUTE_T5805F62303A0DEDAE657066BA289E3249FA6A35B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.ReadOnlyAttribute
struct  ReadOnlyAttribute_t5805F62303A0DEDAE657066BA289E3249FA6A35B  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Boolean System.ComponentModel.ReadOnlyAttribute::isReadOnly
	bool ___isReadOnly_0;

public:
	inline static int32_t get_offset_of_isReadOnly_0() { return static_cast<int32_t>(offsetof(ReadOnlyAttribute_t5805F62303A0DEDAE657066BA289E3249FA6A35B, ___isReadOnly_0)); }
	inline bool get_isReadOnly_0() const { return ___isReadOnly_0; }
	inline bool* get_address_of_isReadOnly_0() { return &___isReadOnly_0; }
	inline void set_isReadOnly_0(bool value)
	{
		___isReadOnly_0 = value;
	}
};

struct ReadOnlyAttribute_t5805F62303A0DEDAE657066BA289E3249FA6A35B_StaticFields
{
public:
	// System.ComponentModel.ReadOnlyAttribute System.ComponentModel.ReadOnlyAttribute::Yes
	ReadOnlyAttribute_t5805F62303A0DEDAE657066BA289E3249FA6A35B * ___Yes_1;
	// System.ComponentModel.ReadOnlyAttribute System.ComponentModel.ReadOnlyAttribute::No
	ReadOnlyAttribute_t5805F62303A0DEDAE657066BA289E3249FA6A35B * ___No_2;
	// System.ComponentModel.ReadOnlyAttribute System.ComponentModel.ReadOnlyAttribute::Default
	ReadOnlyAttribute_t5805F62303A0DEDAE657066BA289E3249FA6A35B * ___Default_3;

public:
	inline static int32_t get_offset_of_Yes_1() { return static_cast<int32_t>(offsetof(ReadOnlyAttribute_t5805F62303A0DEDAE657066BA289E3249FA6A35B_StaticFields, ___Yes_1)); }
	inline ReadOnlyAttribute_t5805F62303A0DEDAE657066BA289E3249FA6A35B * get_Yes_1() const { return ___Yes_1; }
	inline ReadOnlyAttribute_t5805F62303A0DEDAE657066BA289E3249FA6A35B ** get_address_of_Yes_1() { return &___Yes_1; }
	inline void set_Yes_1(ReadOnlyAttribute_t5805F62303A0DEDAE657066BA289E3249FA6A35B * value)
	{
		___Yes_1 = value;
		Il2CppCodeGenWriteBarrier((&___Yes_1), value);
	}

	inline static int32_t get_offset_of_No_2() { return static_cast<int32_t>(offsetof(ReadOnlyAttribute_t5805F62303A0DEDAE657066BA289E3249FA6A35B_StaticFields, ___No_2)); }
	inline ReadOnlyAttribute_t5805F62303A0DEDAE657066BA289E3249FA6A35B * get_No_2() const { return ___No_2; }
	inline ReadOnlyAttribute_t5805F62303A0DEDAE657066BA289E3249FA6A35B ** get_address_of_No_2() { return &___No_2; }
	inline void set_No_2(ReadOnlyAttribute_t5805F62303A0DEDAE657066BA289E3249FA6A35B * value)
	{
		___No_2 = value;
		Il2CppCodeGenWriteBarrier((&___No_2), value);
	}

	inline static int32_t get_offset_of_Default_3() { return static_cast<int32_t>(offsetof(ReadOnlyAttribute_t5805F62303A0DEDAE657066BA289E3249FA6A35B_StaticFields, ___Default_3)); }
	inline ReadOnlyAttribute_t5805F62303A0DEDAE657066BA289E3249FA6A35B * get_Default_3() const { return ___Default_3; }
	inline ReadOnlyAttribute_t5805F62303A0DEDAE657066BA289E3249FA6A35B ** get_address_of_Default_3() { return &___Default_3; }
	inline void set_Default_3(ReadOnlyAttribute_t5805F62303A0DEDAE657066BA289E3249FA6A35B * value)
	{
		___Default_3 = value;
		Il2CppCodeGenWriteBarrier((&___Default_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READONLYATTRIBUTE_T5805F62303A0DEDAE657066BA289E3249FA6A35B_H
#ifndef RECOMMENDEDASCONFIGURABLEATTRIBUTE_T193A3D07F4735EB535DD6960352C6C9A38E12B55_H
#define RECOMMENDEDASCONFIGURABLEATTRIBUTE_T193A3D07F4735EB535DD6960352C6C9A38E12B55_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.RecommendedAsConfigurableAttribute
struct  RecommendedAsConfigurableAttribute_t193A3D07F4735EB535DD6960352C6C9A38E12B55  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Boolean System.ComponentModel.RecommendedAsConfigurableAttribute::recommendedAsConfigurable
	bool ___recommendedAsConfigurable_0;

public:
	inline static int32_t get_offset_of_recommendedAsConfigurable_0() { return static_cast<int32_t>(offsetof(RecommendedAsConfigurableAttribute_t193A3D07F4735EB535DD6960352C6C9A38E12B55, ___recommendedAsConfigurable_0)); }
	inline bool get_recommendedAsConfigurable_0() const { return ___recommendedAsConfigurable_0; }
	inline bool* get_address_of_recommendedAsConfigurable_0() { return &___recommendedAsConfigurable_0; }
	inline void set_recommendedAsConfigurable_0(bool value)
	{
		___recommendedAsConfigurable_0 = value;
	}
};

struct RecommendedAsConfigurableAttribute_t193A3D07F4735EB535DD6960352C6C9A38E12B55_StaticFields
{
public:
	// System.ComponentModel.RecommendedAsConfigurableAttribute System.ComponentModel.RecommendedAsConfigurableAttribute::No
	RecommendedAsConfigurableAttribute_t193A3D07F4735EB535DD6960352C6C9A38E12B55 * ___No_1;
	// System.ComponentModel.RecommendedAsConfigurableAttribute System.ComponentModel.RecommendedAsConfigurableAttribute::Yes
	RecommendedAsConfigurableAttribute_t193A3D07F4735EB535DD6960352C6C9A38E12B55 * ___Yes_2;
	// System.ComponentModel.RecommendedAsConfigurableAttribute System.ComponentModel.RecommendedAsConfigurableAttribute::Default
	RecommendedAsConfigurableAttribute_t193A3D07F4735EB535DD6960352C6C9A38E12B55 * ___Default_3;

public:
	inline static int32_t get_offset_of_No_1() { return static_cast<int32_t>(offsetof(RecommendedAsConfigurableAttribute_t193A3D07F4735EB535DD6960352C6C9A38E12B55_StaticFields, ___No_1)); }
	inline RecommendedAsConfigurableAttribute_t193A3D07F4735EB535DD6960352C6C9A38E12B55 * get_No_1() const { return ___No_1; }
	inline RecommendedAsConfigurableAttribute_t193A3D07F4735EB535DD6960352C6C9A38E12B55 ** get_address_of_No_1() { return &___No_1; }
	inline void set_No_1(RecommendedAsConfigurableAttribute_t193A3D07F4735EB535DD6960352C6C9A38E12B55 * value)
	{
		___No_1 = value;
		Il2CppCodeGenWriteBarrier((&___No_1), value);
	}

	inline static int32_t get_offset_of_Yes_2() { return static_cast<int32_t>(offsetof(RecommendedAsConfigurableAttribute_t193A3D07F4735EB535DD6960352C6C9A38E12B55_StaticFields, ___Yes_2)); }
	inline RecommendedAsConfigurableAttribute_t193A3D07F4735EB535DD6960352C6C9A38E12B55 * get_Yes_2() const { return ___Yes_2; }
	inline RecommendedAsConfigurableAttribute_t193A3D07F4735EB535DD6960352C6C9A38E12B55 ** get_address_of_Yes_2() { return &___Yes_2; }
	inline void set_Yes_2(RecommendedAsConfigurableAttribute_t193A3D07F4735EB535DD6960352C6C9A38E12B55 * value)
	{
		___Yes_2 = value;
		Il2CppCodeGenWriteBarrier((&___Yes_2), value);
	}

	inline static int32_t get_offset_of_Default_3() { return static_cast<int32_t>(offsetof(RecommendedAsConfigurableAttribute_t193A3D07F4735EB535DD6960352C6C9A38E12B55_StaticFields, ___Default_3)); }
	inline RecommendedAsConfigurableAttribute_t193A3D07F4735EB535DD6960352C6C9A38E12B55 * get_Default_3() const { return ___Default_3; }
	inline RecommendedAsConfigurableAttribute_t193A3D07F4735EB535DD6960352C6C9A38E12B55 ** get_address_of_Default_3() { return &___Default_3; }
	inline void set_Default_3(RecommendedAsConfigurableAttribute_t193A3D07F4735EB535DD6960352C6C9A38E12B55 * value)
	{
		___Default_3 = value;
		Il2CppCodeGenWriteBarrier((&___Default_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECOMMENDEDASCONFIGURABLEATTRIBUTE_T193A3D07F4735EB535DD6960352C6C9A38E12B55_H
#ifndef REFRESHEVENTARGS_TAC29F8AEEF1CB6B4ADD8BC4DD0A66366A4700650_H
#define REFRESHEVENTARGS_TAC29F8AEEF1CB6B4ADD8BC4DD0A66366A4700650_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.RefreshEventArgs
struct  RefreshEventArgs_tAC29F8AEEF1CB6B4ADD8BC4DD0A66366A4700650  : public EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E
{
public:
	// System.Object System.ComponentModel.RefreshEventArgs::componentChanged
	RuntimeObject * ___componentChanged_1;
	// System.Type System.ComponentModel.RefreshEventArgs::typeChanged
	Type_t * ___typeChanged_2;

public:
	inline static int32_t get_offset_of_componentChanged_1() { return static_cast<int32_t>(offsetof(RefreshEventArgs_tAC29F8AEEF1CB6B4ADD8BC4DD0A66366A4700650, ___componentChanged_1)); }
	inline RuntimeObject * get_componentChanged_1() const { return ___componentChanged_1; }
	inline RuntimeObject ** get_address_of_componentChanged_1() { return &___componentChanged_1; }
	inline void set_componentChanged_1(RuntimeObject * value)
	{
		___componentChanged_1 = value;
		Il2CppCodeGenWriteBarrier((&___componentChanged_1), value);
	}

	inline static int32_t get_offset_of_typeChanged_2() { return static_cast<int32_t>(offsetof(RefreshEventArgs_tAC29F8AEEF1CB6B4ADD8BC4DD0A66366A4700650, ___typeChanged_2)); }
	inline Type_t * get_typeChanged_2() const { return ___typeChanged_2; }
	inline Type_t ** get_address_of_typeChanged_2() { return &___typeChanged_2; }
	inline void set_typeChanged_2(Type_t * value)
	{
		___typeChanged_2 = value;
		Il2CppCodeGenWriteBarrier((&___typeChanged_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFRESHEVENTARGS_TAC29F8AEEF1CB6B4ADD8BC4DD0A66366A4700650_H
#ifndef RUNINSTALLERATTRIBUTE_T40A160B1D7133CC1797AD183B581E785D6F99CB3_H
#define RUNINSTALLERATTRIBUTE_T40A160B1D7133CC1797AD183B581E785D6F99CB3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.RunInstallerAttribute
struct  RunInstallerAttribute_t40A160B1D7133CC1797AD183B581E785D6F99CB3  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Boolean System.ComponentModel.RunInstallerAttribute::runInstaller
	bool ___runInstaller_0;

public:
	inline static int32_t get_offset_of_runInstaller_0() { return static_cast<int32_t>(offsetof(RunInstallerAttribute_t40A160B1D7133CC1797AD183B581E785D6F99CB3, ___runInstaller_0)); }
	inline bool get_runInstaller_0() const { return ___runInstaller_0; }
	inline bool* get_address_of_runInstaller_0() { return &___runInstaller_0; }
	inline void set_runInstaller_0(bool value)
	{
		___runInstaller_0 = value;
	}
};

struct RunInstallerAttribute_t40A160B1D7133CC1797AD183B581E785D6F99CB3_StaticFields
{
public:
	// System.ComponentModel.RunInstallerAttribute System.ComponentModel.RunInstallerAttribute::Yes
	RunInstallerAttribute_t40A160B1D7133CC1797AD183B581E785D6F99CB3 * ___Yes_1;
	// System.ComponentModel.RunInstallerAttribute System.ComponentModel.RunInstallerAttribute::No
	RunInstallerAttribute_t40A160B1D7133CC1797AD183B581E785D6F99CB3 * ___No_2;
	// System.ComponentModel.RunInstallerAttribute System.ComponentModel.RunInstallerAttribute::Default
	RunInstallerAttribute_t40A160B1D7133CC1797AD183B581E785D6F99CB3 * ___Default_3;

public:
	inline static int32_t get_offset_of_Yes_1() { return static_cast<int32_t>(offsetof(RunInstallerAttribute_t40A160B1D7133CC1797AD183B581E785D6F99CB3_StaticFields, ___Yes_1)); }
	inline RunInstallerAttribute_t40A160B1D7133CC1797AD183B581E785D6F99CB3 * get_Yes_1() const { return ___Yes_1; }
	inline RunInstallerAttribute_t40A160B1D7133CC1797AD183B581E785D6F99CB3 ** get_address_of_Yes_1() { return &___Yes_1; }
	inline void set_Yes_1(RunInstallerAttribute_t40A160B1D7133CC1797AD183B581E785D6F99CB3 * value)
	{
		___Yes_1 = value;
		Il2CppCodeGenWriteBarrier((&___Yes_1), value);
	}

	inline static int32_t get_offset_of_No_2() { return static_cast<int32_t>(offsetof(RunInstallerAttribute_t40A160B1D7133CC1797AD183B581E785D6F99CB3_StaticFields, ___No_2)); }
	inline RunInstallerAttribute_t40A160B1D7133CC1797AD183B581E785D6F99CB3 * get_No_2() const { return ___No_2; }
	inline RunInstallerAttribute_t40A160B1D7133CC1797AD183B581E785D6F99CB3 ** get_address_of_No_2() { return &___No_2; }
	inline void set_No_2(RunInstallerAttribute_t40A160B1D7133CC1797AD183B581E785D6F99CB3 * value)
	{
		___No_2 = value;
		Il2CppCodeGenWriteBarrier((&___No_2), value);
	}

	inline static int32_t get_offset_of_Default_3() { return static_cast<int32_t>(offsetof(RunInstallerAttribute_t40A160B1D7133CC1797AD183B581E785D6F99CB3_StaticFields, ___Default_3)); }
	inline RunInstallerAttribute_t40A160B1D7133CC1797AD183B581E785D6F99CB3 * get_Default_3() const { return ___Default_3; }
	inline RunInstallerAttribute_t40A160B1D7133CC1797AD183B581E785D6F99CB3 ** get_address_of_Default_3() { return &___Default_3; }
	inline void set_Default_3(RunInstallerAttribute_t40A160B1D7133CC1797AD183B581E785D6F99CB3 * value)
	{
		___Default_3 = value;
		Il2CppCodeGenWriteBarrier((&___Default_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNINSTALLERATTRIBUTE_T40A160B1D7133CC1797AD183B581E785D6F99CB3_H
#ifndef SETTINGSBINDABLEATTRIBUTE_T85FF311AAFAED1C12B3773CB10D8D29DAB791664_H
#define SETTINGSBINDABLEATTRIBUTE_T85FF311AAFAED1C12B3773CB10D8D29DAB791664_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.SettingsBindableAttribute
struct  SettingsBindableAttribute_t85FF311AAFAED1C12B3773CB10D8D29DAB791664  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Boolean System.ComponentModel.SettingsBindableAttribute::_bindable
	bool ____bindable_2;

public:
	inline static int32_t get_offset_of__bindable_2() { return static_cast<int32_t>(offsetof(SettingsBindableAttribute_t85FF311AAFAED1C12B3773CB10D8D29DAB791664, ____bindable_2)); }
	inline bool get__bindable_2() const { return ____bindable_2; }
	inline bool* get_address_of__bindable_2() { return &____bindable_2; }
	inline void set__bindable_2(bool value)
	{
		____bindable_2 = value;
	}
};

struct SettingsBindableAttribute_t85FF311AAFAED1C12B3773CB10D8D29DAB791664_StaticFields
{
public:
	// System.ComponentModel.SettingsBindableAttribute System.ComponentModel.SettingsBindableAttribute::Yes
	SettingsBindableAttribute_t85FF311AAFAED1C12B3773CB10D8D29DAB791664 * ___Yes_0;
	// System.ComponentModel.SettingsBindableAttribute System.ComponentModel.SettingsBindableAttribute::No
	SettingsBindableAttribute_t85FF311AAFAED1C12B3773CB10D8D29DAB791664 * ___No_1;

public:
	inline static int32_t get_offset_of_Yes_0() { return static_cast<int32_t>(offsetof(SettingsBindableAttribute_t85FF311AAFAED1C12B3773CB10D8D29DAB791664_StaticFields, ___Yes_0)); }
	inline SettingsBindableAttribute_t85FF311AAFAED1C12B3773CB10D8D29DAB791664 * get_Yes_0() const { return ___Yes_0; }
	inline SettingsBindableAttribute_t85FF311AAFAED1C12B3773CB10D8D29DAB791664 ** get_address_of_Yes_0() { return &___Yes_0; }
	inline void set_Yes_0(SettingsBindableAttribute_t85FF311AAFAED1C12B3773CB10D8D29DAB791664 * value)
	{
		___Yes_0 = value;
		Il2CppCodeGenWriteBarrier((&___Yes_0), value);
	}

	inline static int32_t get_offset_of_No_1() { return static_cast<int32_t>(offsetof(SettingsBindableAttribute_t85FF311AAFAED1C12B3773CB10D8D29DAB791664_StaticFields, ___No_1)); }
	inline SettingsBindableAttribute_t85FF311AAFAED1C12B3773CB10D8D29DAB791664 * get_No_1() const { return ___No_1; }
	inline SettingsBindableAttribute_t85FF311AAFAED1C12B3773CB10D8D29DAB791664 ** get_address_of_No_1() { return &___No_1; }
	inline void set_No_1(SettingsBindableAttribute_t85FF311AAFAED1C12B3773CB10D8D29DAB791664 * value)
	{
		___No_1 = value;
		Il2CppCodeGenWriteBarrier((&___No_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETTINGSBINDABLEATTRIBUTE_T85FF311AAFAED1C12B3773CB10D8D29DAB791664_H
#ifndef TOOLBOXITEMATTRIBUTE_T06EC671268D222B3B4F3BE1DF5874E9B3166269C_H
#define TOOLBOXITEMATTRIBUTE_T06EC671268D222B3B4F3BE1DF5874E9B3166269C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.ToolboxItemAttribute
struct  ToolboxItemAttribute_t06EC671268D222B3B4F3BE1DF5874E9B3166269C  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Type System.ComponentModel.ToolboxItemAttribute::toolboxItemType
	Type_t * ___toolboxItemType_0;
	// System.String System.ComponentModel.ToolboxItemAttribute::toolboxItemTypeName
	String_t* ___toolboxItemTypeName_1;

public:
	inline static int32_t get_offset_of_toolboxItemType_0() { return static_cast<int32_t>(offsetof(ToolboxItemAttribute_t06EC671268D222B3B4F3BE1DF5874E9B3166269C, ___toolboxItemType_0)); }
	inline Type_t * get_toolboxItemType_0() const { return ___toolboxItemType_0; }
	inline Type_t ** get_address_of_toolboxItemType_0() { return &___toolboxItemType_0; }
	inline void set_toolboxItemType_0(Type_t * value)
	{
		___toolboxItemType_0 = value;
		Il2CppCodeGenWriteBarrier((&___toolboxItemType_0), value);
	}

	inline static int32_t get_offset_of_toolboxItemTypeName_1() { return static_cast<int32_t>(offsetof(ToolboxItemAttribute_t06EC671268D222B3B4F3BE1DF5874E9B3166269C, ___toolboxItemTypeName_1)); }
	inline String_t* get_toolboxItemTypeName_1() const { return ___toolboxItemTypeName_1; }
	inline String_t** get_address_of_toolboxItemTypeName_1() { return &___toolboxItemTypeName_1; }
	inline void set_toolboxItemTypeName_1(String_t* value)
	{
		___toolboxItemTypeName_1 = value;
		Il2CppCodeGenWriteBarrier((&___toolboxItemTypeName_1), value);
	}
};

struct ToolboxItemAttribute_t06EC671268D222B3B4F3BE1DF5874E9B3166269C_StaticFields
{
public:
	// System.ComponentModel.ToolboxItemAttribute System.ComponentModel.ToolboxItemAttribute::Default
	ToolboxItemAttribute_t06EC671268D222B3B4F3BE1DF5874E9B3166269C * ___Default_2;
	// System.ComponentModel.ToolboxItemAttribute System.ComponentModel.ToolboxItemAttribute::None
	ToolboxItemAttribute_t06EC671268D222B3B4F3BE1DF5874E9B3166269C * ___None_3;

public:
	inline static int32_t get_offset_of_Default_2() { return static_cast<int32_t>(offsetof(ToolboxItemAttribute_t06EC671268D222B3B4F3BE1DF5874E9B3166269C_StaticFields, ___Default_2)); }
	inline ToolboxItemAttribute_t06EC671268D222B3B4F3BE1DF5874E9B3166269C * get_Default_2() const { return ___Default_2; }
	inline ToolboxItemAttribute_t06EC671268D222B3B4F3BE1DF5874E9B3166269C ** get_address_of_Default_2() { return &___Default_2; }
	inline void set_Default_2(ToolboxItemAttribute_t06EC671268D222B3B4F3BE1DF5874E9B3166269C * value)
	{
		___Default_2 = value;
		Il2CppCodeGenWriteBarrier((&___Default_2), value);
	}

	inline static int32_t get_offset_of_None_3() { return static_cast<int32_t>(offsetof(ToolboxItemAttribute_t06EC671268D222B3B4F3BE1DF5874E9B3166269C_StaticFields, ___None_3)); }
	inline ToolboxItemAttribute_t06EC671268D222B3B4F3BE1DF5874E9B3166269C * get_None_3() const { return ___None_3; }
	inline ToolboxItemAttribute_t06EC671268D222B3B4F3BE1DF5874E9B3166269C ** get_address_of_None_3() { return &___None_3; }
	inline void set_None_3(ToolboxItemAttribute_t06EC671268D222B3B4F3BE1DF5874E9B3166269C * value)
	{
		___None_3 = value;
		Il2CppCodeGenWriteBarrier((&___None_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOOLBOXITEMATTRIBUTE_T06EC671268D222B3B4F3BE1DF5874E9B3166269C_H
#ifndef TYPECONVERTERATTRIBUTE_TA0B22E1BE9471741D2CD2A078A2C9A5FF882C2F8_H
#define TYPECONVERTERATTRIBUTE_TA0B22E1BE9471741D2CD2A078A2C9A5FF882C2F8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.TypeConverterAttribute
struct  TypeConverterAttribute_tA0B22E1BE9471741D2CD2A078A2C9A5FF882C2F8  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String System.ComponentModel.TypeConverterAttribute::typeName
	String_t* ___typeName_0;

public:
	inline static int32_t get_offset_of_typeName_0() { return static_cast<int32_t>(offsetof(TypeConverterAttribute_tA0B22E1BE9471741D2CD2A078A2C9A5FF882C2F8, ___typeName_0)); }
	inline String_t* get_typeName_0() const { return ___typeName_0; }
	inline String_t** get_address_of_typeName_0() { return &___typeName_0; }
	inline void set_typeName_0(String_t* value)
	{
		___typeName_0 = value;
		Il2CppCodeGenWriteBarrier((&___typeName_0), value);
	}
};

struct TypeConverterAttribute_tA0B22E1BE9471741D2CD2A078A2C9A5FF882C2F8_StaticFields
{
public:
	// System.ComponentModel.TypeConverterAttribute System.ComponentModel.TypeConverterAttribute::Default
	TypeConverterAttribute_tA0B22E1BE9471741D2CD2A078A2C9A5FF882C2F8 * ___Default_1;

public:
	inline static int32_t get_offset_of_Default_1() { return static_cast<int32_t>(offsetof(TypeConverterAttribute_tA0B22E1BE9471741D2CD2A078A2C9A5FF882C2F8_StaticFields, ___Default_1)); }
	inline TypeConverterAttribute_tA0B22E1BE9471741D2CD2A078A2C9A5FF882C2F8 * get_Default_1() const { return ___Default_1; }
	inline TypeConverterAttribute_tA0B22E1BE9471741D2CD2A078A2C9A5FF882C2F8 ** get_address_of_Default_1() { return &___Default_1; }
	inline void set_Default_1(TypeConverterAttribute_tA0B22E1BE9471741D2CD2A078A2C9A5FF882C2F8 * value)
	{
		___Default_1 = value;
		Il2CppCodeGenWriteBarrier((&___Default_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPECONVERTERATTRIBUTE_TA0B22E1BE9471741D2CD2A078A2C9A5FF882C2F8_H
#ifndef EMPTYCUSTOMTYPEDESCRIPTOR_TDDCC5072613D7D0405BCCF3D49F3EF7B4E1E7DB8_H
#define EMPTYCUSTOMTYPEDESCRIPTOR_TDDCC5072613D7D0405BCCF3D49F3EF7B4E1E7DB8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.TypeDescriptionProvider_EmptyCustomTypeDescriptor
struct  EmptyCustomTypeDescriptor_tDDCC5072613D7D0405BCCF3D49F3EF7B4E1E7DB8  : public CustomTypeDescriptor_tF8665CD45DFFA622F7EB328A2F77067DD2147689
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMPTYCUSTOMTYPEDESCRIPTOR_TDDCC5072613D7D0405BCCF3D49F3EF7B4E1E7DB8_H
#ifndef TYPEDESCRIPTIONPROVIDERATTRIBUTE_T387A574F4C3912AEA8435C46CC377BDA93F29034_H
#define TYPEDESCRIPTIONPROVIDERATTRIBUTE_T387A574F4C3912AEA8435C46CC377BDA93F29034_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.TypeDescriptionProviderAttribute
struct  TypeDescriptionProviderAttribute_t387A574F4C3912AEA8435C46CC377BDA93F29034  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String System.ComponentModel.TypeDescriptionProviderAttribute::_typeName
	String_t* ____typeName_0;

public:
	inline static int32_t get_offset_of__typeName_0() { return static_cast<int32_t>(offsetof(TypeDescriptionProviderAttribute_t387A574F4C3912AEA8435C46CC377BDA93F29034, ____typeName_0)); }
	inline String_t* get__typeName_0() const { return ____typeName_0; }
	inline String_t** get_address_of__typeName_0() { return &____typeName_0; }
	inline void set__typeName_0(String_t* value)
	{
		____typeName_0 = value;
		Il2CppCodeGenWriteBarrier((&____typeName_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEDESCRIPTIONPROVIDERATTRIBUTE_T387A574F4C3912AEA8435C46CC377BDA93F29034_H
#ifndef ATTRIBUTEPROVIDER_T67BCE87062C434FBC0E369F34CCB196661DABFDF_H
#define ATTRIBUTEPROVIDER_T67BCE87062C434FBC0E369F34CCB196661DABFDF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.TypeDescriptor_AttributeProvider
struct  AttributeProvider_t67BCE87062C434FBC0E369F34CCB196661DABFDF  : public TypeDescriptionProvider_tE390829A953C44525366CA2A733E92642B97B591
{
public:
	// System.Attribute[] System.ComponentModel.TypeDescriptor_AttributeProvider::_attrs
	AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17* ____attrs_2;

public:
	inline static int32_t get_offset_of__attrs_2() { return static_cast<int32_t>(offsetof(AttributeProvider_t67BCE87062C434FBC0E369F34CCB196661DABFDF, ____attrs_2)); }
	inline AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17* get__attrs_2() const { return ____attrs_2; }
	inline AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17** get_address_of__attrs_2() { return &____attrs_2; }
	inline void set__attrs_2(AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17* value)
	{
		____attrs_2 = value;
		Il2CppCodeGenWriteBarrier((&____attrs_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTEPROVIDER_T67BCE87062C434FBC0E369F34CCB196661DABFDF_H
#ifndef ATTRIBUTETYPEDESCRIPTOR_T4D6F2E9913F8B001ADF799DA77361BAEF574A6DE_H
#define ATTRIBUTETYPEDESCRIPTOR_T4D6F2E9913F8B001ADF799DA77361BAEF574A6DE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.TypeDescriptor_AttributeProvider_AttributeTypeDescriptor
struct  AttributeTypeDescriptor_t4D6F2E9913F8B001ADF799DA77361BAEF574A6DE  : public CustomTypeDescriptor_tF8665CD45DFFA622F7EB328A2F77067DD2147689
{
public:
	// System.Attribute[] System.ComponentModel.TypeDescriptor_AttributeProvider_AttributeTypeDescriptor::_attributeArray
	AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17* ____attributeArray_1;

public:
	inline static int32_t get_offset_of__attributeArray_1() { return static_cast<int32_t>(offsetof(AttributeTypeDescriptor_t4D6F2E9913F8B001ADF799DA77361BAEF574A6DE, ____attributeArray_1)); }
	inline AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17* get__attributeArray_1() const { return ____attributeArray_1; }
	inline AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17** get_address_of__attributeArray_1() { return &____attributeArray_1; }
	inline void set__attributeArray_1(AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17* value)
	{
		____attributeArray_1 = value;
		Il2CppCodeGenWriteBarrier((&____attributeArray_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTETYPEDESCRIPTOR_T4D6F2E9913F8B001ADF799DA77361BAEF574A6DE_H
#ifndef COMNATIVEDESCRIPTIONPROVIDER_TC63084A6A9080BAA8FB16E20E2847C18E5E3907D_H
#define COMNATIVEDESCRIPTIONPROVIDER_TC63084A6A9080BAA8FB16E20E2847C18E5E3907D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.TypeDescriptor_ComNativeDescriptionProvider
struct  ComNativeDescriptionProvider_tC63084A6A9080BAA8FB16E20E2847C18E5E3907D  : public TypeDescriptionProvider_tE390829A953C44525366CA2A733E92642B97B591
{
public:
	// System.ComponentModel.IComNativeDescriptorHandler System.ComponentModel.TypeDescriptor_ComNativeDescriptionProvider::_handler
	RuntimeObject* ____handler_2;

public:
	inline static int32_t get_offset_of__handler_2() { return static_cast<int32_t>(offsetof(ComNativeDescriptionProvider_tC63084A6A9080BAA8FB16E20E2847C18E5E3907D, ____handler_2)); }
	inline RuntimeObject* get__handler_2() const { return ____handler_2; }
	inline RuntimeObject** get_address_of__handler_2() { return &____handler_2; }
	inline void set__handler_2(RuntimeObject* value)
	{
		____handler_2 = value;
		Il2CppCodeGenWriteBarrier((&____handler_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMNATIVEDESCRIPTIONPROVIDER_TC63084A6A9080BAA8FB16E20E2847C18E5E3907D_H
#ifndef TYPEDESCRIPTIONNODE_T8C5D3A02DB612FFE1DE12251E49EC265DE6B81E1_H
#define TYPEDESCRIPTIONNODE_T8C5D3A02DB612FFE1DE12251E49EC265DE6B81E1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.TypeDescriptor_TypeDescriptionNode
struct  TypeDescriptionNode_t8C5D3A02DB612FFE1DE12251E49EC265DE6B81E1  : public TypeDescriptionProvider_tE390829A953C44525366CA2A733E92642B97B591
{
public:
	// System.ComponentModel.TypeDescriptor_TypeDescriptionNode System.ComponentModel.TypeDescriptor_TypeDescriptionNode::Next
	TypeDescriptionNode_t8C5D3A02DB612FFE1DE12251E49EC265DE6B81E1 * ___Next_2;
	// System.ComponentModel.TypeDescriptionProvider System.ComponentModel.TypeDescriptor_TypeDescriptionNode::Provider
	TypeDescriptionProvider_tE390829A953C44525366CA2A733E92642B97B591 * ___Provider_3;

public:
	inline static int32_t get_offset_of_Next_2() { return static_cast<int32_t>(offsetof(TypeDescriptionNode_t8C5D3A02DB612FFE1DE12251E49EC265DE6B81E1, ___Next_2)); }
	inline TypeDescriptionNode_t8C5D3A02DB612FFE1DE12251E49EC265DE6B81E1 * get_Next_2() const { return ___Next_2; }
	inline TypeDescriptionNode_t8C5D3A02DB612FFE1DE12251E49EC265DE6B81E1 ** get_address_of_Next_2() { return &___Next_2; }
	inline void set_Next_2(TypeDescriptionNode_t8C5D3A02DB612FFE1DE12251E49EC265DE6B81E1 * value)
	{
		___Next_2 = value;
		Il2CppCodeGenWriteBarrier((&___Next_2), value);
	}

	inline static int32_t get_offset_of_Provider_3() { return static_cast<int32_t>(offsetof(TypeDescriptionNode_t8C5D3A02DB612FFE1DE12251E49EC265DE6B81E1, ___Provider_3)); }
	inline TypeDescriptionProvider_tE390829A953C44525366CA2A733E92642B97B591 * get_Provider_3() const { return ___Provider_3; }
	inline TypeDescriptionProvider_tE390829A953C44525366CA2A733E92642B97B591 ** get_address_of_Provider_3() { return &___Provider_3; }
	inline void set_Provider_3(TypeDescriptionProvider_tE390829A953C44525366CA2A733E92642B97B591 * value)
	{
		___Provider_3 = value;
		Il2CppCodeGenWriteBarrier((&___Provider_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEDESCRIPTIONNODE_T8C5D3A02DB612FFE1DE12251E49EC265DE6B81E1_H
#ifndef DEFAULTEXTENDEDTYPEDESCRIPTOR_T89890252F6A685D141ECBB0C2878C6E913883ECE_H
#define DEFAULTEXTENDEDTYPEDESCRIPTOR_T89890252F6A685D141ECBB0C2878C6E913883ECE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.TypeDescriptor_TypeDescriptionNode_DefaultExtendedTypeDescriptor
struct  DefaultExtendedTypeDescriptor_t89890252F6A685D141ECBB0C2878C6E913883ECE 
{
public:
	// System.ComponentModel.TypeDescriptor_TypeDescriptionNode System.ComponentModel.TypeDescriptor_TypeDescriptionNode_DefaultExtendedTypeDescriptor::_node
	TypeDescriptionNode_t8C5D3A02DB612FFE1DE12251E49EC265DE6B81E1 * ____node_0;
	// System.Object System.ComponentModel.TypeDescriptor_TypeDescriptionNode_DefaultExtendedTypeDescriptor::_instance
	RuntimeObject * ____instance_1;

public:
	inline static int32_t get_offset_of__node_0() { return static_cast<int32_t>(offsetof(DefaultExtendedTypeDescriptor_t89890252F6A685D141ECBB0C2878C6E913883ECE, ____node_0)); }
	inline TypeDescriptionNode_t8C5D3A02DB612FFE1DE12251E49EC265DE6B81E1 * get__node_0() const { return ____node_0; }
	inline TypeDescriptionNode_t8C5D3A02DB612FFE1DE12251E49EC265DE6B81E1 ** get_address_of__node_0() { return &____node_0; }
	inline void set__node_0(TypeDescriptionNode_t8C5D3A02DB612FFE1DE12251E49EC265DE6B81E1 * value)
	{
		____node_0 = value;
		Il2CppCodeGenWriteBarrier((&____node_0), value);
	}

	inline static int32_t get_offset_of__instance_1() { return static_cast<int32_t>(offsetof(DefaultExtendedTypeDescriptor_t89890252F6A685D141ECBB0C2878C6E913883ECE, ____instance_1)); }
	inline RuntimeObject * get__instance_1() const { return ____instance_1; }
	inline RuntimeObject ** get_address_of__instance_1() { return &____instance_1; }
	inline void set__instance_1(RuntimeObject * value)
	{
		____instance_1 = value;
		Il2CppCodeGenWriteBarrier((&____instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ComponentModel.TypeDescriptor/TypeDescriptionNode/DefaultExtendedTypeDescriptor
struct DefaultExtendedTypeDescriptor_t89890252F6A685D141ECBB0C2878C6E913883ECE_marshaled_pinvoke
{
	TypeDescriptionNode_t8C5D3A02DB612FFE1DE12251E49EC265DE6B81E1 * ____node_0;
	Il2CppIUnknown* ____instance_1;
};
// Native definition for COM marshalling of System.ComponentModel.TypeDescriptor/TypeDescriptionNode/DefaultExtendedTypeDescriptor
struct DefaultExtendedTypeDescriptor_t89890252F6A685D141ECBB0C2878C6E913883ECE_marshaled_com
{
	TypeDescriptionNode_t8C5D3A02DB612FFE1DE12251E49EC265DE6B81E1 * ____node_0;
	Il2CppIUnknown* ____instance_1;
};
#endif // DEFAULTEXTENDEDTYPEDESCRIPTOR_T89890252F6A685D141ECBB0C2878C6E913883ECE_H
#ifndef DEFAULTTYPEDESCRIPTOR_T45C7CF272F02817B0F1C69470B4E786E746996F4_H
#define DEFAULTTYPEDESCRIPTOR_T45C7CF272F02817B0F1C69470B4E786E746996F4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.TypeDescriptor_TypeDescriptionNode_DefaultTypeDescriptor
struct  DefaultTypeDescriptor_t45C7CF272F02817B0F1C69470B4E786E746996F4 
{
public:
	// System.ComponentModel.TypeDescriptor_TypeDescriptionNode System.ComponentModel.TypeDescriptor_TypeDescriptionNode_DefaultTypeDescriptor::_node
	TypeDescriptionNode_t8C5D3A02DB612FFE1DE12251E49EC265DE6B81E1 * ____node_0;
	// System.Type System.ComponentModel.TypeDescriptor_TypeDescriptionNode_DefaultTypeDescriptor::_objectType
	Type_t * ____objectType_1;
	// System.Object System.ComponentModel.TypeDescriptor_TypeDescriptionNode_DefaultTypeDescriptor::_instance
	RuntimeObject * ____instance_2;

public:
	inline static int32_t get_offset_of__node_0() { return static_cast<int32_t>(offsetof(DefaultTypeDescriptor_t45C7CF272F02817B0F1C69470B4E786E746996F4, ____node_0)); }
	inline TypeDescriptionNode_t8C5D3A02DB612FFE1DE12251E49EC265DE6B81E1 * get__node_0() const { return ____node_0; }
	inline TypeDescriptionNode_t8C5D3A02DB612FFE1DE12251E49EC265DE6B81E1 ** get_address_of__node_0() { return &____node_0; }
	inline void set__node_0(TypeDescriptionNode_t8C5D3A02DB612FFE1DE12251E49EC265DE6B81E1 * value)
	{
		____node_0 = value;
		Il2CppCodeGenWriteBarrier((&____node_0), value);
	}

	inline static int32_t get_offset_of__objectType_1() { return static_cast<int32_t>(offsetof(DefaultTypeDescriptor_t45C7CF272F02817B0F1C69470B4E786E746996F4, ____objectType_1)); }
	inline Type_t * get__objectType_1() const { return ____objectType_1; }
	inline Type_t ** get_address_of__objectType_1() { return &____objectType_1; }
	inline void set__objectType_1(Type_t * value)
	{
		____objectType_1 = value;
		Il2CppCodeGenWriteBarrier((&____objectType_1), value);
	}

	inline static int32_t get_offset_of__instance_2() { return static_cast<int32_t>(offsetof(DefaultTypeDescriptor_t45C7CF272F02817B0F1C69470B4E786E746996F4, ____instance_2)); }
	inline RuntimeObject * get__instance_2() const { return ____instance_2; }
	inline RuntimeObject ** get_address_of__instance_2() { return &____instance_2; }
	inline void set__instance_2(RuntimeObject * value)
	{
		____instance_2 = value;
		Il2CppCodeGenWriteBarrier((&____instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ComponentModel.TypeDescriptor/TypeDescriptionNode/DefaultTypeDescriptor
struct DefaultTypeDescriptor_t45C7CF272F02817B0F1C69470B4E786E746996F4_marshaled_pinvoke
{
	TypeDescriptionNode_t8C5D3A02DB612FFE1DE12251E49EC265DE6B81E1 * ____node_0;
	Type_t * ____objectType_1;
	Il2CppIUnknown* ____instance_2;
};
// Native definition for COM marshalling of System.ComponentModel.TypeDescriptor/TypeDescriptionNode/DefaultTypeDescriptor
struct DefaultTypeDescriptor_t45C7CF272F02817B0F1C69470B4E786E746996F4_marshaled_com
{
	TypeDescriptionNode_t8C5D3A02DB612FFE1DE12251E49EC265DE6B81E1 * ____node_0;
	Type_t * ____objectType_1;
	Il2CppIUnknown* ____instance_2;
};
#endif // DEFAULTTYPEDESCRIPTOR_T45C7CF272F02817B0F1C69470B4E786E746996F4_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef GUID_T_H
#define GUID_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Guid
struct  Guid_t 
{
public:
	// System.Int32 System.Guid::_a
	int32_t ____a_1;
	// System.Int16 System.Guid::_b
	int16_t ____b_2;
	// System.Int16 System.Guid::_c
	int16_t ____c_3;
	// System.Byte System.Guid::_d
	uint8_t ____d_4;
	// System.Byte System.Guid::_e
	uint8_t ____e_5;
	// System.Byte System.Guid::_f
	uint8_t ____f_6;
	// System.Byte System.Guid::_g
	uint8_t ____g_7;
	// System.Byte System.Guid::_h
	uint8_t ____h_8;
	// System.Byte System.Guid::_i
	uint8_t ____i_9;
	// System.Byte System.Guid::_j
	uint8_t ____j_10;
	// System.Byte System.Guid::_k
	uint8_t ____k_11;

public:
	inline static int32_t get_offset_of__a_1() { return static_cast<int32_t>(offsetof(Guid_t, ____a_1)); }
	inline int32_t get__a_1() const { return ____a_1; }
	inline int32_t* get_address_of__a_1() { return &____a_1; }
	inline void set__a_1(int32_t value)
	{
		____a_1 = value;
	}

	inline static int32_t get_offset_of__b_2() { return static_cast<int32_t>(offsetof(Guid_t, ____b_2)); }
	inline int16_t get__b_2() const { return ____b_2; }
	inline int16_t* get_address_of__b_2() { return &____b_2; }
	inline void set__b_2(int16_t value)
	{
		____b_2 = value;
	}

	inline static int32_t get_offset_of__c_3() { return static_cast<int32_t>(offsetof(Guid_t, ____c_3)); }
	inline int16_t get__c_3() const { return ____c_3; }
	inline int16_t* get_address_of__c_3() { return &____c_3; }
	inline void set__c_3(int16_t value)
	{
		____c_3 = value;
	}

	inline static int32_t get_offset_of__d_4() { return static_cast<int32_t>(offsetof(Guid_t, ____d_4)); }
	inline uint8_t get__d_4() const { return ____d_4; }
	inline uint8_t* get_address_of__d_4() { return &____d_4; }
	inline void set__d_4(uint8_t value)
	{
		____d_4 = value;
	}

	inline static int32_t get_offset_of__e_5() { return static_cast<int32_t>(offsetof(Guid_t, ____e_5)); }
	inline uint8_t get__e_5() const { return ____e_5; }
	inline uint8_t* get_address_of__e_5() { return &____e_5; }
	inline void set__e_5(uint8_t value)
	{
		____e_5 = value;
	}

	inline static int32_t get_offset_of__f_6() { return static_cast<int32_t>(offsetof(Guid_t, ____f_6)); }
	inline uint8_t get__f_6() const { return ____f_6; }
	inline uint8_t* get_address_of__f_6() { return &____f_6; }
	inline void set__f_6(uint8_t value)
	{
		____f_6 = value;
	}

	inline static int32_t get_offset_of__g_7() { return static_cast<int32_t>(offsetof(Guid_t, ____g_7)); }
	inline uint8_t get__g_7() const { return ____g_7; }
	inline uint8_t* get_address_of__g_7() { return &____g_7; }
	inline void set__g_7(uint8_t value)
	{
		____g_7 = value;
	}

	inline static int32_t get_offset_of__h_8() { return static_cast<int32_t>(offsetof(Guid_t, ____h_8)); }
	inline uint8_t get__h_8() const { return ____h_8; }
	inline uint8_t* get_address_of__h_8() { return &____h_8; }
	inline void set__h_8(uint8_t value)
	{
		____h_8 = value;
	}

	inline static int32_t get_offset_of__i_9() { return static_cast<int32_t>(offsetof(Guid_t, ____i_9)); }
	inline uint8_t get__i_9() const { return ____i_9; }
	inline uint8_t* get_address_of__i_9() { return &____i_9; }
	inline void set__i_9(uint8_t value)
	{
		____i_9 = value;
	}

	inline static int32_t get_offset_of__j_10() { return static_cast<int32_t>(offsetof(Guid_t, ____j_10)); }
	inline uint8_t get__j_10() const { return ____j_10; }
	inline uint8_t* get_address_of__j_10() { return &____j_10; }
	inline void set__j_10(uint8_t value)
	{
		____j_10 = value;
	}

	inline static int32_t get_offset_of__k_11() { return static_cast<int32_t>(offsetof(Guid_t, ____k_11)); }
	inline uint8_t get__k_11() const { return ____k_11; }
	inline uint8_t* get_address_of__k_11() { return &____k_11; }
	inline void set__k_11(uint8_t value)
	{
		____k_11 = value;
	}
};

struct Guid_t_StaticFields
{
public:
	// System.Guid System.Guid::Empty
	Guid_t  ___Empty_0;
	// System.Object System.Guid::_rngAccess
	RuntimeObject * ____rngAccess_12;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_rng
	RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * ____rng_13;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ___Empty_0)); }
	inline Guid_t  get_Empty_0() const { return ___Empty_0; }
	inline Guid_t * get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(Guid_t  value)
	{
		___Empty_0 = value;
	}

	inline static int32_t get_offset_of__rngAccess_12() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rngAccess_12)); }
	inline RuntimeObject * get__rngAccess_12() const { return ____rngAccess_12; }
	inline RuntimeObject ** get_address_of__rngAccess_12() { return &____rngAccess_12; }
	inline void set__rngAccess_12(RuntimeObject * value)
	{
		____rngAccess_12 = value;
		Il2CppCodeGenWriteBarrier((&____rngAccess_12), value);
	}

	inline static int32_t get_offset_of__rng_13() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rng_13)); }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * get__rng_13() const { return ____rng_13; }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 ** get_address_of__rng_13() { return &____rng_13; }
	inline void set__rng_13(RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * value)
	{
		____rng_13 = value;
		Il2CppCodeGenWriteBarrier((&____rng_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUID_T_H
#ifndef INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#define INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef FRIENDACCESSALLOWEDATTRIBUTE_T9EADAA524AB7E0A3138B0AE43226C19809A90CA3_H
#define FRIENDACCESSALLOWEDATTRIBUTE_T9EADAA524AB7E0A3138B0AE43226C19809A90CA3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.FriendAccessAllowedAttribute
struct  FriendAccessAllowedAttribute_t9EADAA524AB7E0A3138B0AE43226C19809A90CA3  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FRIENDACCESSALLOWEDATTRIBUTE_T9EADAA524AB7E0A3138B0AE43226C19809A90CA3_H
#ifndef DEFAULTPARAMETERVALUEATTRIBUTE_T289E7B39A1085788DCB9930E09487C28F5F9B040_H
#define DEFAULTPARAMETERVALUEATTRIBUTE_T289E7B39A1085788DCB9930E09487C28F5F9B040_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.DefaultParameterValueAttribute
struct  DefaultParameterValueAttribute_t289E7B39A1085788DCB9930E09487C28F5F9B040  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Object System.Runtime.InteropServices.DefaultParameterValueAttribute::value
	RuntimeObject * ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(DefaultParameterValueAttribute_t289E7B39A1085788DCB9930E09487C28F5F9B040, ___value_0)); }
	inline RuntimeObject * get_value_0() const { return ___value_0; }
	inline RuntimeObject ** get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(RuntimeObject * value)
	{
		___value_0 = value;
		Il2CppCodeGenWriteBarrier((&___value_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTPARAMETERVALUEATTRIBUTE_T289E7B39A1085788DCB9930E09487C28F5F9B040_H
#ifndef GCHANDLE_T39FAEE3EA592432C93B574A31DD83B87F1847DE3_H
#define GCHANDLE_T39FAEE3EA592432C93B574A31DD83B87F1847DE3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.GCHandle
struct  GCHandle_t39FAEE3EA592432C93B574A31DD83B87F1847DE3 
{
public:
	// System.Int32 System.Runtime.InteropServices.GCHandle::handle
	int32_t ___handle_0;

public:
	inline static int32_t get_offset_of_handle_0() { return static_cast<int32_t>(offsetof(GCHandle_t39FAEE3EA592432C93B574A31DD83B87F1847DE3, ___handle_0)); }
	inline int32_t get_handle_0() const { return ___handle_0; }
	inline int32_t* get_address_of_handle_0() { return &___handle_0; }
	inline void set_handle_0(int32_t value)
	{
		___handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GCHANDLE_T39FAEE3EA592432C93B574A31DD83B87F1847DE3_H
#ifndef SERVICENAMECOLLECTION_T3C60F59BA95A4A706BB70439AE03C07D09796C2C_H
#define SERVICENAMECOLLECTION_T3C60F59BA95A4A706BB70439AE03C07D09796C2C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Authentication.ExtendedProtection.ServiceNameCollection
struct  ServiceNameCollection_t3C60F59BA95A4A706BB70439AE03C07D09796C2C  : public ReadOnlyCollectionBase_tFD695167917CE6DF4FA18A906FA530880B9B8772
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERVICENAMECOLLECTION_T3C60F59BA95A4A706BB70439AE03C07D09796C2C_H
#ifndef SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#define SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef HASHTABLE_T978F65B8006C8F5504B286526AEC6608FF983FC9_H
#define HASHTABLE_T978F65B8006C8F5504B286526AEC6608FF983FC9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Hashtable
struct  Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9  : public RuntimeObject
{
public:
	// System.Collections.Hashtable_bucket[] System.Collections.Hashtable::buckets
	bucketU5BU5D_t6FF2C2C4B21F2206885CD19A78F68B874C8DC84A* ___buckets_10;
	// System.Int32 System.Collections.Hashtable::count
	int32_t ___count_11;
	// System.Int32 System.Collections.Hashtable::occupancy
	int32_t ___occupancy_12;
	// System.Int32 System.Collections.Hashtable::loadsize
	int32_t ___loadsize_13;
	// System.Single System.Collections.Hashtable::loadFactor
	float ___loadFactor_14;
	// System.Int32 modreq(System.Runtime.CompilerServices.IsVolatile) System.Collections.Hashtable::version
	int32_t ___version_15;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.Collections.Hashtable::isWriterInProgress
	bool ___isWriterInProgress_16;
	// System.Collections.ICollection System.Collections.Hashtable::keys
	RuntimeObject* ___keys_17;
	// System.Collections.ICollection System.Collections.Hashtable::values
	RuntimeObject* ___values_18;
	// System.Collections.IEqualityComparer System.Collections.Hashtable::_keycomparer
	RuntimeObject* ____keycomparer_19;
	// System.Object System.Collections.Hashtable::_syncRoot
	RuntimeObject * ____syncRoot_20;

public:
	inline static int32_t get_offset_of_buckets_10() { return static_cast<int32_t>(offsetof(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9, ___buckets_10)); }
	inline bucketU5BU5D_t6FF2C2C4B21F2206885CD19A78F68B874C8DC84A* get_buckets_10() const { return ___buckets_10; }
	inline bucketU5BU5D_t6FF2C2C4B21F2206885CD19A78F68B874C8DC84A** get_address_of_buckets_10() { return &___buckets_10; }
	inline void set_buckets_10(bucketU5BU5D_t6FF2C2C4B21F2206885CD19A78F68B874C8DC84A* value)
	{
		___buckets_10 = value;
		Il2CppCodeGenWriteBarrier((&___buckets_10), value);
	}

	inline static int32_t get_offset_of_count_11() { return static_cast<int32_t>(offsetof(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9, ___count_11)); }
	inline int32_t get_count_11() const { return ___count_11; }
	inline int32_t* get_address_of_count_11() { return &___count_11; }
	inline void set_count_11(int32_t value)
	{
		___count_11 = value;
	}

	inline static int32_t get_offset_of_occupancy_12() { return static_cast<int32_t>(offsetof(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9, ___occupancy_12)); }
	inline int32_t get_occupancy_12() const { return ___occupancy_12; }
	inline int32_t* get_address_of_occupancy_12() { return &___occupancy_12; }
	inline void set_occupancy_12(int32_t value)
	{
		___occupancy_12 = value;
	}

	inline static int32_t get_offset_of_loadsize_13() { return static_cast<int32_t>(offsetof(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9, ___loadsize_13)); }
	inline int32_t get_loadsize_13() const { return ___loadsize_13; }
	inline int32_t* get_address_of_loadsize_13() { return &___loadsize_13; }
	inline void set_loadsize_13(int32_t value)
	{
		___loadsize_13 = value;
	}

	inline static int32_t get_offset_of_loadFactor_14() { return static_cast<int32_t>(offsetof(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9, ___loadFactor_14)); }
	inline float get_loadFactor_14() const { return ___loadFactor_14; }
	inline float* get_address_of_loadFactor_14() { return &___loadFactor_14; }
	inline void set_loadFactor_14(float value)
	{
		___loadFactor_14 = value;
	}

	inline static int32_t get_offset_of_version_15() { return static_cast<int32_t>(offsetof(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9, ___version_15)); }
	inline int32_t get_version_15() const { return ___version_15; }
	inline int32_t* get_address_of_version_15() { return &___version_15; }
	inline void set_version_15(int32_t value)
	{
		___version_15 = value;
	}

	inline static int32_t get_offset_of_isWriterInProgress_16() { return static_cast<int32_t>(offsetof(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9, ___isWriterInProgress_16)); }
	inline bool get_isWriterInProgress_16() const { return ___isWriterInProgress_16; }
	inline bool* get_address_of_isWriterInProgress_16() { return &___isWriterInProgress_16; }
	inline void set_isWriterInProgress_16(bool value)
	{
		___isWriterInProgress_16 = value;
	}

	inline static int32_t get_offset_of_keys_17() { return static_cast<int32_t>(offsetof(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9, ___keys_17)); }
	inline RuntimeObject* get_keys_17() const { return ___keys_17; }
	inline RuntimeObject** get_address_of_keys_17() { return &___keys_17; }
	inline void set_keys_17(RuntimeObject* value)
	{
		___keys_17 = value;
		Il2CppCodeGenWriteBarrier((&___keys_17), value);
	}

	inline static int32_t get_offset_of_values_18() { return static_cast<int32_t>(offsetof(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9, ___values_18)); }
	inline RuntimeObject* get_values_18() const { return ___values_18; }
	inline RuntimeObject** get_address_of_values_18() { return &___values_18; }
	inline void set_values_18(RuntimeObject* value)
	{
		___values_18 = value;
		Il2CppCodeGenWriteBarrier((&___values_18), value);
	}

	inline static int32_t get_offset_of__keycomparer_19() { return static_cast<int32_t>(offsetof(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9, ____keycomparer_19)); }
	inline RuntimeObject* get__keycomparer_19() const { return ____keycomparer_19; }
	inline RuntimeObject** get_address_of__keycomparer_19() { return &____keycomparer_19; }
	inline void set__keycomparer_19(RuntimeObject* value)
	{
		____keycomparer_19 = value;
		Il2CppCodeGenWriteBarrier((&____keycomparer_19), value);
	}

	inline static int32_t get_offset_of__syncRoot_20() { return static_cast<int32_t>(offsetof(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9, ____syncRoot_20)); }
	inline RuntimeObject * get__syncRoot_20() const { return ____syncRoot_20; }
	inline RuntimeObject ** get_address_of__syncRoot_20() { return &____syncRoot_20; }
	inline void set__syncRoot_20(RuntimeObject * value)
	{
		____syncRoot_20 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HASHTABLE_T978F65B8006C8F5504B286526AEC6608FF983FC9_H
#ifndef INHERITANCELEVEL_T7A3CA26A46C1759F0D0D9BD78A7B298490C1DAD6_H
#define INHERITANCELEVEL_T7A3CA26A46C1759F0D0D9BD78A7B298490C1DAD6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.InheritanceLevel
struct  InheritanceLevel_t7A3CA26A46C1759F0D0D9BD78A7B298490C1DAD6 
{
public:
	// System.Int32 System.ComponentModel.InheritanceLevel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InheritanceLevel_t7A3CA26A46C1759F0D0D9BD78A7B298490C1DAD6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INHERITANCELEVEL_T7A3CA26A46C1759F0D0D9BD78A7B298490C1DAD6_H
#ifndef PROPERTYTABSCOPE_T7A2ECEC4F4367AC32D472E9B17F35F5F7C0D1836_H
#define PROPERTYTABSCOPE_T7A2ECEC4F4367AC32D472E9B17F35F5F7C0D1836_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.PropertyTabScope
struct  PropertyTabScope_t7A2ECEC4F4367AC32D472E9B17F35F5F7C0D1836 
{
public:
	// System.Int32 System.ComponentModel.PropertyTabScope::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PropertyTabScope_t7A2ECEC4F4367AC32D472E9B17F35F5F7C0D1836, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYTABSCOPE_T7A2ECEC4F4367AC32D472E9B17F35F5F7C0D1836_H
#ifndef REFLECTEVENTDESCRIPTOR_T36C4780808918CFDD00CE2A7E6368FAEFD4DD360_H
#define REFLECTEVENTDESCRIPTOR_T36C4780808918CFDD00CE2A7E6368FAEFD4DD360_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.ReflectEventDescriptor
struct  ReflectEventDescriptor_t36C4780808918CFDD00CE2A7E6368FAEFD4DD360  : public EventDescriptor_tAB488D66C0409A1889EE56355848CDA43ED95222
{
public:
	// System.Type System.ComponentModel.ReflectEventDescriptor::type
	Type_t * ___type_12;
	// System.Type System.ComponentModel.ReflectEventDescriptor::componentClass
	Type_t * ___componentClass_13;
	// System.Reflection.MethodInfo System.ComponentModel.ReflectEventDescriptor::addMethod
	MethodInfo_t * ___addMethod_14;
	// System.Reflection.MethodInfo System.ComponentModel.ReflectEventDescriptor::removeMethod
	MethodInfo_t * ___removeMethod_15;
	// System.Reflection.EventInfo System.ComponentModel.ReflectEventDescriptor::realEvent
	EventInfo_t * ___realEvent_16;
	// System.Boolean System.ComponentModel.ReflectEventDescriptor::filledMethods
	bool ___filledMethods_17;

public:
	inline static int32_t get_offset_of_type_12() { return static_cast<int32_t>(offsetof(ReflectEventDescriptor_t36C4780808918CFDD00CE2A7E6368FAEFD4DD360, ___type_12)); }
	inline Type_t * get_type_12() const { return ___type_12; }
	inline Type_t ** get_address_of_type_12() { return &___type_12; }
	inline void set_type_12(Type_t * value)
	{
		___type_12 = value;
		Il2CppCodeGenWriteBarrier((&___type_12), value);
	}

	inline static int32_t get_offset_of_componentClass_13() { return static_cast<int32_t>(offsetof(ReflectEventDescriptor_t36C4780808918CFDD00CE2A7E6368FAEFD4DD360, ___componentClass_13)); }
	inline Type_t * get_componentClass_13() const { return ___componentClass_13; }
	inline Type_t ** get_address_of_componentClass_13() { return &___componentClass_13; }
	inline void set_componentClass_13(Type_t * value)
	{
		___componentClass_13 = value;
		Il2CppCodeGenWriteBarrier((&___componentClass_13), value);
	}

	inline static int32_t get_offset_of_addMethod_14() { return static_cast<int32_t>(offsetof(ReflectEventDescriptor_t36C4780808918CFDD00CE2A7E6368FAEFD4DD360, ___addMethod_14)); }
	inline MethodInfo_t * get_addMethod_14() const { return ___addMethod_14; }
	inline MethodInfo_t ** get_address_of_addMethod_14() { return &___addMethod_14; }
	inline void set_addMethod_14(MethodInfo_t * value)
	{
		___addMethod_14 = value;
		Il2CppCodeGenWriteBarrier((&___addMethod_14), value);
	}

	inline static int32_t get_offset_of_removeMethod_15() { return static_cast<int32_t>(offsetof(ReflectEventDescriptor_t36C4780808918CFDD00CE2A7E6368FAEFD4DD360, ___removeMethod_15)); }
	inline MethodInfo_t * get_removeMethod_15() const { return ___removeMethod_15; }
	inline MethodInfo_t ** get_address_of_removeMethod_15() { return &___removeMethod_15; }
	inline void set_removeMethod_15(MethodInfo_t * value)
	{
		___removeMethod_15 = value;
		Il2CppCodeGenWriteBarrier((&___removeMethod_15), value);
	}

	inline static int32_t get_offset_of_realEvent_16() { return static_cast<int32_t>(offsetof(ReflectEventDescriptor_t36C4780808918CFDD00CE2A7E6368FAEFD4DD360, ___realEvent_16)); }
	inline EventInfo_t * get_realEvent_16() const { return ___realEvent_16; }
	inline EventInfo_t ** get_address_of_realEvent_16() { return &___realEvent_16; }
	inline void set_realEvent_16(EventInfo_t * value)
	{
		___realEvent_16 = value;
		Il2CppCodeGenWriteBarrier((&___realEvent_16), value);
	}

	inline static int32_t get_offset_of_filledMethods_17() { return static_cast<int32_t>(offsetof(ReflectEventDescriptor_t36C4780808918CFDD00CE2A7E6368FAEFD4DD360, ___filledMethods_17)); }
	inline bool get_filledMethods_17() const { return ___filledMethods_17; }
	inline bool* get_address_of_filledMethods_17() { return &___filledMethods_17; }
	inline void set_filledMethods_17(bool value)
	{
		___filledMethods_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFLECTEVENTDESCRIPTOR_T36C4780808918CFDD00CE2A7E6368FAEFD4DD360_H
#ifndef REFLECTPROPERTYDESCRIPTOR_T4DC7A4205276D400462B36386EEDEEBA6289C11E_H
#define REFLECTPROPERTYDESCRIPTOR_T4DC7A4205276D400462B36386EEDEEBA6289C11E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.ReflectPropertyDescriptor
struct  ReflectPropertyDescriptor_t4DC7A4205276D400462B36386EEDEEBA6289C11E  : public PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D
{
public:
	// System.Collections.Specialized.BitVector32 System.ComponentModel.ReflectPropertyDescriptor::state
	BitVector32_tDE1C7920F117A283D9595A597572E8910691A0F1  ___state_31;
	// System.Type System.ComponentModel.ReflectPropertyDescriptor::componentClass
	Type_t * ___componentClass_32;
	// System.Type System.ComponentModel.ReflectPropertyDescriptor::type
	Type_t * ___type_33;
	// System.Object System.ComponentModel.ReflectPropertyDescriptor::defaultValue
	RuntimeObject * ___defaultValue_34;
	// System.Object System.ComponentModel.ReflectPropertyDescriptor::ambientValue
	RuntimeObject * ___ambientValue_35;
	// System.Reflection.PropertyInfo System.ComponentModel.ReflectPropertyDescriptor::propInfo
	PropertyInfo_t * ___propInfo_36;
	// System.Reflection.MethodInfo System.ComponentModel.ReflectPropertyDescriptor::getMethod
	MethodInfo_t * ___getMethod_37;
	// System.Reflection.MethodInfo System.ComponentModel.ReflectPropertyDescriptor::setMethod
	MethodInfo_t * ___setMethod_38;
	// System.Reflection.MethodInfo System.ComponentModel.ReflectPropertyDescriptor::shouldSerializeMethod
	MethodInfo_t * ___shouldSerializeMethod_39;
	// System.Reflection.MethodInfo System.ComponentModel.ReflectPropertyDescriptor::resetMethod
	MethodInfo_t * ___resetMethod_40;
	// System.ComponentModel.EventDescriptor System.ComponentModel.ReflectPropertyDescriptor::realChangedEvent
	EventDescriptor_tAB488D66C0409A1889EE56355848CDA43ED95222 * ___realChangedEvent_41;
	// System.ComponentModel.EventDescriptor System.ComponentModel.ReflectPropertyDescriptor::realIPropChangedEvent
	EventDescriptor_tAB488D66C0409A1889EE56355848CDA43ED95222 * ___realIPropChangedEvent_42;
	// System.Type System.ComponentModel.ReflectPropertyDescriptor::receiverType
	Type_t * ___receiverType_43;

public:
	inline static int32_t get_offset_of_state_31() { return static_cast<int32_t>(offsetof(ReflectPropertyDescriptor_t4DC7A4205276D400462B36386EEDEEBA6289C11E, ___state_31)); }
	inline BitVector32_tDE1C7920F117A283D9595A597572E8910691A0F1  get_state_31() const { return ___state_31; }
	inline BitVector32_tDE1C7920F117A283D9595A597572E8910691A0F1 * get_address_of_state_31() { return &___state_31; }
	inline void set_state_31(BitVector32_tDE1C7920F117A283D9595A597572E8910691A0F1  value)
	{
		___state_31 = value;
	}

	inline static int32_t get_offset_of_componentClass_32() { return static_cast<int32_t>(offsetof(ReflectPropertyDescriptor_t4DC7A4205276D400462B36386EEDEEBA6289C11E, ___componentClass_32)); }
	inline Type_t * get_componentClass_32() const { return ___componentClass_32; }
	inline Type_t ** get_address_of_componentClass_32() { return &___componentClass_32; }
	inline void set_componentClass_32(Type_t * value)
	{
		___componentClass_32 = value;
		Il2CppCodeGenWriteBarrier((&___componentClass_32), value);
	}

	inline static int32_t get_offset_of_type_33() { return static_cast<int32_t>(offsetof(ReflectPropertyDescriptor_t4DC7A4205276D400462B36386EEDEEBA6289C11E, ___type_33)); }
	inline Type_t * get_type_33() const { return ___type_33; }
	inline Type_t ** get_address_of_type_33() { return &___type_33; }
	inline void set_type_33(Type_t * value)
	{
		___type_33 = value;
		Il2CppCodeGenWriteBarrier((&___type_33), value);
	}

	inline static int32_t get_offset_of_defaultValue_34() { return static_cast<int32_t>(offsetof(ReflectPropertyDescriptor_t4DC7A4205276D400462B36386EEDEEBA6289C11E, ___defaultValue_34)); }
	inline RuntimeObject * get_defaultValue_34() const { return ___defaultValue_34; }
	inline RuntimeObject ** get_address_of_defaultValue_34() { return &___defaultValue_34; }
	inline void set_defaultValue_34(RuntimeObject * value)
	{
		___defaultValue_34 = value;
		Il2CppCodeGenWriteBarrier((&___defaultValue_34), value);
	}

	inline static int32_t get_offset_of_ambientValue_35() { return static_cast<int32_t>(offsetof(ReflectPropertyDescriptor_t4DC7A4205276D400462B36386EEDEEBA6289C11E, ___ambientValue_35)); }
	inline RuntimeObject * get_ambientValue_35() const { return ___ambientValue_35; }
	inline RuntimeObject ** get_address_of_ambientValue_35() { return &___ambientValue_35; }
	inline void set_ambientValue_35(RuntimeObject * value)
	{
		___ambientValue_35 = value;
		Il2CppCodeGenWriteBarrier((&___ambientValue_35), value);
	}

	inline static int32_t get_offset_of_propInfo_36() { return static_cast<int32_t>(offsetof(ReflectPropertyDescriptor_t4DC7A4205276D400462B36386EEDEEBA6289C11E, ___propInfo_36)); }
	inline PropertyInfo_t * get_propInfo_36() const { return ___propInfo_36; }
	inline PropertyInfo_t ** get_address_of_propInfo_36() { return &___propInfo_36; }
	inline void set_propInfo_36(PropertyInfo_t * value)
	{
		___propInfo_36 = value;
		Il2CppCodeGenWriteBarrier((&___propInfo_36), value);
	}

	inline static int32_t get_offset_of_getMethod_37() { return static_cast<int32_t>(offsetof(ReflectPropertyDescriptor_t4DC7A4205276D400462B36386EEDEEBA6289C11E, ___getMethod_37)); }
	inline MethodInfo_t * get_getMethod_37() const { return ___getMethod_37; }
	inline MethodInfo_t ** get_address_of_getMethod_37() { return &___getMethod_37; }
	inline void set_getMethod_37(MethodInfo_t * value)
	{
		___getMethod_37 = value;
		Il2CppCodeGenWriteBarrier((&___getMethod_37), value);
	}

	inline static int32_t get_offset_of_setMethod_38() { return static_cast<int32_t>(offsetof(ReflectPropertyDescriptor_t4DC7A4205276D400462B36386EEDEEBA6289C11E, ___setMethod_38)); }
	inline MethodInfo_t * get_setMethod_38() const { return ___setMethod_38; }
	inline MethodInfo_t ** get_address_of_setMethod_38() { return &___setMethod_38; }
	inline void set_setMethod_38(MethodInfo_t * value)
	{
		___setMethod_38 = value;
		Il2CppCodeGenWriteBarrier((&___setMethod_38), value);
	}

	inline static int32_t get_offset_of_shouldSerializeMethod_39() { return static_cast<int32_t>(offsetof(ReflectPropertyDescriptor_t4DC7A4205276D400462B36386EEDEEBA6289C11E, ___shouldSerializeMethod_39)); }
	inline MethodInfo_t * get_shouldSerializeMethod_39() const { return ___shouldSerializeMethod_39; }
	inline MethodInfo_t ** get_address_of_shouldSerializeMethod_39() { return &___shouldSerializeMethod_39; }
	inline void set_shouldSerializeMethod_39(MethodInfo_t * value)
	{
		___shouldSerializeMethod_39 = value;
		Il2CppCodeGenWriteBarrier((&___shouldSerializeMethod_39), value);
	}

	inline static int32_t get_offset_of_resetMethod_40() { return static_cast<int32_t>(offsetof(ReflectPropertyDescriptor_t4DC7A4205276D400462B36386EEDEEBA6289C11E, ___resetMethod_40)); }
	inline MethodInfo_t * get_resetMethod_40() const { return ___resetMethod_40; }
	inline MethodInfo_t ** get_address_of_resetMethod_40() { return &___resetMethod_40; }
	inline void set_resetMethod_40(MethodInfo_t * value)
	{
		___resetMethod_40 = value;
		Il2CppCodeGenWriteBarrier((&___resetMethod_40), value);
	}

	inline static int32_t get_offset_of_realChangedEvent_41() { return static_cast<int32_t>(offsetof(ReflectPropertyDescriptor_t4DC7A4205276D400462B36386EEDEEBA6289C11E, ___realChangedEvent_41)); }
	inline EventDescriptor_tAB488D66C0409A1889EE56355848CDA43ED95222 * get_realChangedEvent_41() const { return ___realChangedEvent_41; }
	inline EventDescriptor_tAB488D66C0409A1889EE56355848CDA43ED95222 ** get_address_of_realChangedEvent_41() { return &___realChangedEvent_41; }
	inline void set_realChangedEvent_41(EventDescriptor_tAB488D66C0409A1889EE56355848CDA43ED95222 * value)
	{
		___realChangedEvent_41 = value;
		Il2CppCodeGenWriteBarrier((&___realChangedEvent_41), value);
	}

	inline static int32_t get_offset_of_realIPropChangedEvent_42() { return static_cast<int32_t>(offsetof(ReflectPropertyDescriptor_t4DC7A4205276D400462B36386EEDEEBA6289C11E, ___realIPropChangedEvent_42)); }
	inline EventDescriptor_tAB488D66C0409A1889EE56355848CDA43ED95222 * get_realIPropChangedEvent_42() const { return ___realIPropChangedEvent_42; }
	inline EventDescriptor_tAB488D66C0409A1889EE56355848CDA43ED95222 ** get_address_of_realIPropChangedEvent_42() { return &___realIPropChangedEvent_42; }
	inline void set_realIPropChangedEvent_42(EventDescriptor_tAB488D66C0409A1889EE56355848CDA43ED95222 * value)
	{
		___realIPropChangedEvent_42 = value;
		Il2CppCodeGenWriteBarrier((&___realIPropChangedEvent_42), value);
	}

	inline static int32_t get_offset_of_receiverType_43() { return static_cast<int32_t>(offsetof(ReflectPropertyDescriptor_t4DC7A4205276D400462B36386EEDEEBA6289C11E, ___receiverType_43)); }
	inline Type_t * get_receiverType_43() const { return ___receiverType_43; }
	inline Type_t ** get_address_of_receiverType_43() { return &___receiverType_43; }
	inline void set_receiverType_43(Type_t * value)
	{
		___receiverType_43 = value;
		Il2CppCodeGenWriteBarrier((&___receiverType_43), value);
	}
};

struct ReflectPropertyDescriptor_t4DC7A4205276D400462B36386EEDEEBA6289C11E_StaticFields
{
public:
	// System.Type[] System.ComponentModel.ReflectPropertyDescriptor::argsNone
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___argsNone_17;
	// System.Object System.ComponentModel.ReflectPropertyDescriptor::noValue
	RuntimeObject * ___noValue_18;
	// System.Diagnostics.TraceSwitch System.ComponentModel.ReflectPropertyDescriptor::PropDescCreateSwitch
	TraceSwitch_t32D210D5C9B05D9E555925260EEC3767BA895EC8 * ___PropDescCreateSwitch_19;
	// System.Diagnostics.TraceSwitch System.ComponentModel.ReflectPropertyDescriptor::PropDescUsageSwitch
	TraceSwitch_t32D210D5C9B05D9E555925260EEC3767BA895EC8 * ___PropDescUsageSwitch_20;
	// System.Int32 System.ComponentModel.ReflectPropertyDescriptor::BitDefaultValueQueried
	int32_t ___BitDefaultValueQueried_21;
	// System.Int32 System.ComponentModel.ReflectPropertyDescriptor::BitGetQueried
	int32_t ___BitGetQueried_22;
	// System.Int32 System.ComponentModel.ReflectPropertyDescriptor::BitSetQueried
	int32_t ___BitSetQueried_23;
	// System.Int32 System.ComponentModel.ReflectPropertyDescriptor::BitShouldSerializeQueried
	int32_t ___BitShouldSerializeQueried_24;
	// System.Int32 System.ComponentModel.ReflectPropertyDescriptor::BitResetQueried
	int32_t ___BitResetQueried_25;
	// System.Int32 System.ComponentModel.ReflectPropertyDescriptor::BitChangedQueried
	int32_t ___BitChangedQueried_26;
	// System.Int32 System.ComponentModel.ReflectPropertyDescriptor::BitIPropChangedQueried
	int32_t ___BitIPropChangedQueried_27;
	// System.Int32 System.ComponentModel.ReflectPropertyDescriptor::BitReadOnlyChecked
	int32_t ___BitReadOnlyChecked_28;
	// System.Int32 System.ComponentModel.ReflectPropertyDescriptor::BitAmbientValueQueried
	int32_t ___BitAmbientValueQueried_29;
	// System.Int32 System.ComponentModel.ReflectPropertyDescriptor::BitSetOnDemand
	int32_t ___BitSetOnDemand_30;

public:
	inline static int32_t get_offset_of_argsNone_17() { return static_cast<int32_t>(offsetof(ReflectPropertyDescriptor_t4DC7A4205276D400462B36386EEDEEBA6289C11E_StaticFields, ___argsNone_17)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get_argsNone_17() const { return ___argsNone_17; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of_argsNone_17() { return &___argsNone_17; }
	inline void set_argsNone_17(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		___argsNone_17 = value;
		Il2CppCodeGenWriteBarrier((&___argsNone_17), value);
	}

	inline static int32_t get_offset_of_noValue_18() { return static_cast<int32_t>(offsetof(ReflectPropertyDescriptor_t4DC7A4205276D400462B36386EEDEEBA6289C11E_StaticFields, ___noValue_18)); }
	inline RuntimeObject * get_noValue_18() const { return ___noValue_18; }
	inline RuntimeObject ** get_address_of_noValue_18() { return &___noValue_18; }
	inline void set_noValue_18(RuntimeObject * value)
	{
		___noValue_18 = value;
		Il2CppCodeGenWriteBarrier((&___noValue_18), value);
	}

	inline static int32_t get_offset_of_PropDescCreateSwitch_19() { return static_cast<int32_t>(offsetof(ReflectPropertyDescriptor_t4DC7A4205276D400462B36386EEDEEBA6289C11E_StaticFields, ___PropDescCreateSwitch_19)); }
	inline TraceSwitch_t32D210D5C9B05D9E555925260EEC3767BA895EC8 * get_PropDescCreateSwitch_19() const { return ___PropDescCreateSwitch_19; }
	inline TraceSwitch_t32D210D5C9B05D9E555925260EEC3767BA895EC8 ** get_address_of_PropDescCreateSwitch_19() { return &___PropDescCreateSwitch_19; }
	inline void set_PropDescCreateSwitch_19(TraceSwitch_t32D210D5C9B05D9E555925260EEC3767BA895EC8 * value)
	{
		___PropDescCreateSwitch_19 = value;
		Il2CppCodeGenWriteBarrier((&___PropDescCreateSwitch_19), value);
	}

	inline static int32_t get_offset_of_PropDescUsageSwitch_20() { return static_cast<int32_t>(offsetof(ReflectPropertyDescriptor_t4DC7A4205276D400462B36386EEDEEBA6289C11E_StaticFields, ___PropDescUsageSwitch_20)); }
	inline TraceSwitch_t32D210D5C9B05D9E555925260EEC3767BA895EC8 * get_PropDescUsageSwitch_20() const { return ___PropDescUsageSwitch_20; }
	inline TraceSwitch_t32D210D5C9B05D9E555925260EEC3767BA895EC8 ** get_address_of_PropDescUsageSwitch_20() { return &___PropDescUsageSwitch_20; }
	inline void set_PropDescUsageSwitch_20(TraceSwitch_t32D210D5C9B05D9E555925260EEC3767BA895EC8 * value)
	{
		___PropDescUsageSwitch_20 = value;
		Il2CppCodeGenWriteBarrier((&___PropDescUsageSwitch_20), value);
	}

	inline static int32_t get_offset_of_BitDefaultValueQueried_21() { return static_cast<int32_t>(offsetof(ReflectPropertyDescriptor_t4DC7A4205276D400462B36386EEDEEBA6289C11E_StaticFields, ___BitDefaultValueQueried_21)); }
	inline int32_t get_BitDefaultValueQueried_21() const { return ___BitDefaultValueQueried_21; }
	inline int32_t* get_address_of_BitDefaultValueQueried_21() { return &___BitDefaultValueQueried_21; }
	inline void set_BitDefaultValueQueried_21(int32_t value)
	{
		___BitDefaultValueQueried_21 = value;
	}

	inline static int32_t get_offset_of_BitGetQueried_22() { return static_cast<int32_t>(offsetof(ReflectPropertyDescriptor_t4DC7A4205276D400462B36386EEDEEBA6289C11E_StaticFields, ___BitGetQueried_22)); }
	inline int32_t get_BitGetQueried_22() const { return ___BitGetQueried_22; }
	inline int32_t* get_address_of_BitGetQueried_22() { return &___BitGetQueried_22; }
	inline void set_BitGetQueried_22(int32_t value)
	{
		___BitGetQueried_22 = value;
	}

	inline static int32_t get_offset_of_BitSetQueried_23() { return static_cast<int32_t>(offsetof(ReflectPropertyDescriptor_t4DC7A4205276D400462B36386EEDEEBA6289C11E_StaticFields, ___BitSetQueried_23)); }
	inline int32_t get_BitSetQueried_23() const { return ___BitSetQueried_23; }
	inline int32_t* get_address_of_BitSetQueried_23() { return &___BitSetQueried_23; }
	inline void set_BitSetQueried_23(int32_t value)
	{
		___BitSetQueried_23 = value;
	}

	inline static int32_t get_offset_of_BitShouldSerializeQueried_24() { return static_cast<int32_t>(offsetof(ReflectPropertyDescriptor_t4DC7A4205276D400462B36386EEDEEBA6289C11E_StaticFields, ___BitShouldSerializeQueried_24)); }
	inline int32_t get_BitShouldSerializeQueried_24() const { return ___BitShouldSerializeQueried_24; }
	inline int32_t* get_address_of_BitShouldSerializeQueried_24() { return &___BitShouldSerializeQueried_24; }
	inline void set_BitShouldSerializeQueried_24(int32_t value)
	{
		___BitShouldSerializeQueried_24 = value;
	}

	inline static int32_t get_offset_of_BitResetQueried_25() { return static_cast<int32_t>(offsetof(ReflectPropertyDescriptor_t4DC7A4205276D400462B36386EEDEEBA6289C11E_StaticFields, ___BitResetQueried_25)); }
	inline int32_t get_BitResetQueried_25() const { return ___BitResetQueried_25; }
	inline int32_t* get_address_of_BitResetQueried_25() { return &___BitResetQueried_25; }
	inline void set_BitResetQueried_25(int32_t value)
	{
		___BitResetQueried_25 = value;
	}

	inline static int32_t get_offset_of_BitChangedQueried_26() { return static_cast<int32_t>(offsetof(ReflectPropertyDescriptor_t4DC7A4205276D400462B36386EEDEEBA6289C11E_StaticFields, ___BitChangedQueried_26)); }
	inline int32_t get_BitChangedQueried_26() const { return ___BitChangedQueried_26; }
	inline int32_t* get_address_of_BitChangedQueried_26() { return &___BitChangedQueried_26; }
	inline void set_BitChangedQueried_26(int32_t value)
	{
		___BitChangedQueried_26 = value;
	}

	inline static int32_t get_offset_of_BitIPropChangedQueried_27() { return static_cast<int32_t>(offsetof(ReflectPropertyDescriptor_t4DC7A4205276D400462B36386EEDEEBA6289C11E_StaticFields, ___BitIPropChangedQueried_27)); }
	inline int32_t get_BitIPropChangedQueried_27() const { return ___BitIPropChangedQueried_27; }
	inline int32_t* get_address_of_BitIPropChangedQueried_27() { return &___BitIPropChangedQueried_27; }
	inline void set_BitIPropChangedQueried_27(int32_t value)
	{
		___BitIPropChangedQueried_27 = value;
	}

	inline static int32_t get_offset_of_BitReadOnlyChecked_28() { return static_cast<int32_t>(offsetof(ReflectPropertyDescriptor_t4DC7A4205276D400462B36386EEDEEBA6289C11E_StaticFields, ___BitReadOnlyChecked_28)); }
	inline int32_t get_BitReadOnlyChecked_28() const { return ___BitReadOnlyChecked_28; }
	inline int32_t* get_address_of_BitReadOnlyChecked_28() { return &___BitReadOnlyChecked_28; }
	inline void set_BitReadOnlyChecked_28(int32_t value)
	{
		___BitReadOnlyChecked_28 = value;
	}

	inline static int32_t get_offset_of_BitAmbientValueQueried_29() { return static_cast<int32_t>(offsetof(ReflectPropertyDescriptor_t4DC7A4205276D400462B36386EEDEEBA6289C11E_StaticFields, ___BitAmbientValueQueried_29)); }
	inline int32_t get_BitAmbientValueQueried_29() const { return ___BitAmbientValueQueried_29; }
	inline int32_t* get_address_of_BitAmbientValueQueried_29() { return &___BitAmbientValueQueried_29; }
	inline void set_BitAmbientValueQueried_29(int32_t value)
	{
		___BitAmbientValueQueried_29 = value;
	}

	inline static int32_t get_offset_of_BitSetOnDemand_30() { return static_cast<int32_t>(offsetof(ReflectPropertyDescriptor_t4DC7A4205276D400462B36386EEDEEBA6289C11E_StaticFields, ___BitSetOnDemand_30)); }
	inline int32_t get_BitSetOnDemand_30() const { return ___BitSetOnDemand_30; }
	inline int32_t* get_address_of_BitSetOnDemand_30() { return &___BitSetOnDemand_30; }
	inline void set_BitSetOnDemand_30(int32_t value)
	{
		___BitSetOnDemand_30 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFLECTPROPERTYDESCRIPTOR_T4DC7A4205276D400462B36386EEDEEBA6289C11E_H
#ifndef REFLECTTYPEDESCRIPTIONPROVIDER_T5736798A27F452CE006C4DBBCC78EC7928B79675_H
#define REFLECTTYPEDESCRIPTIONPROVIDER_T5736798A27F452CE006C4DBBCC78EC7928B79675_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.ReflectTypeDescriptionProvider
struct  ReflectTypeDescriptionProvider_t5736798A27F452CE006C4DBBCC78EC7928B79675  : public TypeDescriptionProvider_tE390829A953C44525366CA2A733E92642B97B591
{
public:
	// System.Collections.Hashtable System.ComponentModel.ReflectTypeDescriptionProvider::_typeData
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ____typeData_2;

public:
	inline static int32_t get_offset_of__typeData_2() { return static_cast<int32_t>(offsetof(ReflectTypeDescriptionProvider_t5736798A27F452CE006C4DBBCC78EC7928B79675, ____typeData_2)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get__typeData_2() const { return ____typeData_2; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of__typeData_2() { return &____typeData_2; }
	inline void set__typeData_2(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		____typeData_2 = value;
		Il2CppCodeGenWriteBarrier((&____typeData_2), value);
	}
};

struct ReflectTypeDescriptionProvider_t5736798A27F452CE006C4DBBCC78EC7928B79675_StaticFields
{
public:
	// System.Type[] System.ComponentModel.ReflectTypeDescriptionProvider::_typeConstructor
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ____typeConstructor_3;
	// System.Collections.Hashtable modreq(System.Runtime.CompilerServices.IsVolatile) System.ComponentModel.ReflectTypeDescriptionProvider::_editorTables
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ____editorTables_4;
	// System.Collections.Hashtable modreq(System.Runtime.CompilerServices.IsVolatile) System.ComponentModel.ReflectTypeDescriptionProvider::_intrinsicTypeConverters
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ____intrinsicTypeConverters_5;
	// System.Object System.ComponentModel.ReflectTypeDescriptionProvider::_intrinsicReferenceKey
	RuntimeObject * ____intrinsicReferenceKey_6;
	// System.Object System.ComponentModel.ReflectTypeDescriptionProvider::_intrinsicNullableKey
	RuntimeObject * ____intrinsicNullableKey_7;
	// System.Object System.ComponentModel.ReflectTypeDescriptionProvider::_dictionaryKey
	RuntimeObject * ____dictionaryKey_8;
	// System.Collections.Hashtable modreq(System.Runtime.CompilerServices.IsVolatile) System.ComponentModel.ReflectTypeDescriptionProvider::_propertyCache
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ____propertyCache_9;
	// System.Collections.Hashtable modreq(System.Runtime.CompilerServices.IsVolatile) System.ComponentModel.ReflectTypeDescriptionProvider::_eventCache
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ____eventCache_10;
	// System.Collections.Hashtable modreq(System.Runtime.CompilerServices.IsVolatile) System.ComponentModel.ReflectTypeDescriptionProvider::_attributeCache
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ____attributeCache_11;
	// System.Collections.Hashtable modreq(System.Runtime.CompilerServices.IsVolatile) System.ComponentModel.ReflectTypeDescriptionProvider::_extendedPropertyCache
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ____extendedPropertyCache_12;
	// System.Guid System.ComponentModel.ReflectTypeDescriptionProvider::_extenderProviderKey
	Guid_t  ____extenderProviderKey_13;
	// System.Guid System.ComponentModel.ReflectTypeDescriptionProvider::_extenderPropertiesKey
	Guid_t  ____extenderPropertiesKey_14;
	// System.Guid System.ComponentModel.ReflectTypeDescriptionProvider::_extenderProviderPropertiesKey
	Guid_t  ____extenderProviderPropertiesKey_15;
	// System.Type[] System.ComponentModel.ReflectTypeDescriptionProvider::_skipInterfaceAttributeList
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ____skipInterfaceAttributeList_16;
	// System.Object System.ComponentModel.ReflectTypeDescriptionProvider::_internalSyncObject
	RuntimeObject * ____internalSyncObject_17;

public:
	inline static int32_t get_offset_of__typeConstructor_3() { return static_cast<int32_t>(offsetof(ReflectTypeDescriptionProvider_t5736798A27F452CE006C4DBBCC78EC7928B79675_StaticFields, ____typeConstructor_3)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get__typeConstructor_3() const { return ____typeConstructor_3; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of__typeConstructor_3() { return &____typeConstructor_3; }
	inline void set__typeConstructor_3(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		____typeConstructor_3 = value;
		Il2CppCodeGenWriteBarrier((&____typeConstructor_3), value);
	}

	inline static int32_t get_offset_of__editorTables_4() { return static_cast<int32_t>(offsetof(ReflectTypeDescriptionProvider_t5736798A27F452CE006C4DBBCC78EC7928B79675_StaticFields, ____editorTables_4)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get__editorTables_4() const { return ____editorTables_4; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of__editorTables_4() { return &____editorTables_4; }
	inline void set__editorTables_4(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		____editorTables_4 = value;
		Il2CppCodeGenWriteBarrier((&____editorTables_4), value);
	}

	inline static int32_t get_offset_of__intrinsicTypeConverters_5() { return static_cast<int32_t>(offsetof(ReflectTypeDescriptionProvider_t5736798A27F452CE006C4DBBCC78EC7928B79675_StaticFields, ____intrinsicTypeConverters_5)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get__intrinsicTypeConverters_5() const { return ____intrinsicTypeConverters_5; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of__intrinsicTypeConverters_5() { return &____intrinsicTypeConverters_5; }
	inline void set__intrinsicTypeConverters_5(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		____intrinsicTypeConverters_5 = value;
		Il2CppCodeGenWriteBarrier((&____intrinsicTypeConverters_5), value);
	}

	inline static int32_t get_offset_of__intrinsicReferenceKey_6() { return static_cast<int32_t>(offsetof(ReflectTypeDescriptionProvider_t5736798A27F452CE006C4DBBCC78EC7928B79675_StaticFields, ____intrinsicReferenceKey_6)); }
	inline RuntimeObject * get__intrinsicReferenceKey_6() const { return ____intrinsicReferenceKey_6; }
	inline RuntimeObject ** get_address_of__intrinsicReferenceKey_6() { return &____intrinsicReferenceKey_6; }
	inline void set__intrinsicReferenceKey_6(RuntimeObject * value)
	{
		____intrinsicReferenceKey_6 = value;
		Il2CppCodeGenWriteBarrier((&____intrinsicReferenceKey_6), value);
	}

	inline static int32_t get_offset_of__intrinsicNullableKey_7() { return static_cast<int32_t>(offsetof(ReflectTypeDescriptionProvider_t5736798A27F452CE006C4DBBCC78EC7928B79675_StaticFields, ____intrinsicNullableKey_7)); }
	inline RuntimeObject * get__intrinsicNullableKey_7() const { return ____intrinsicNullableKey_7; }
	inline RuntimeObject ** get_address_of__intrinsicNullableKey_7() { return &____intrinsicNullableKey_7; }
	inline void set__intrinsicNullableKey_7(RuntimeObject * value)
	{
		____intrinsicNullableKey_7 = value;
		Il2CppCodeGenWriteBarrier((&____intrinsicNullableKey_7), value);
	}

	inline static int32_t get_offset_of__dictionaryKey_8() { return static_cast<int32_t>(offsetof(ReflectTypeDescriptionProvider_t5736798A27F452CE006C4DBBCC78EC7928B79675_StaticFields, ____dictionaryKey_8)); }
	inline RuntimeObject * get__dictionaryKey_8() const { return ____dictionaryKey_8; }
	inline RuntimeObject ** get_address_of__dictionaryKey_8() { return &____dictionaryKey_8; }
	inline void set__dictionaryKey_8(RuntimeObject * value)
	{
		____dictionaryKey_8 = value;
		Il2CppCodeGenWriteBarrier((&____dictionaryKey_8), value);
	}

	inline static int32_t get_offset_of__propertyCache_9() { return static_cast<int32_t>(offsetof(ReflectTypeDescriptionProvider_t5736798A27F452CE006C4DBBCC78EC7928B79675_StaticFields, ____propertyCache_9)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get__propertyCache_9() const { return ____propertyCache_9; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of__propertyCache_9() { return &____propertyCache_9; }
	inline void set__propertyCache_9(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		____propertyCache_9 = value;
		Il2CppCodeGenWriteBarrier((&____propertyCache_9), value);
	}

	inline static int32_t get_offset_of__eventCache_10() { return static_cast<int32_t>(offsetof(ReflectTypeDescriptionProvider_t5736798A27F452CE006C4DBBCC78EC7928B79675_StaticFields, ____eventCache_10)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get__eventCache_10() const { return ____eventCache_10; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of__eventCache_10() { return &____eventCache_10; }
	inline void set__eventCache_10(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		____eventCache_10 = value;
		Il2CppCodeGenWriteBarrier((&____eventCache_10), value);
	}

	inline static int32_t get_offset_of__attributeCache_11() { return static_cast<int32_t>(offsetof(ReflectTypeDescriptionProvider_t5736798A27F452CE006C4DBBCC78EC7928B79675_StaticFields, ____attributeCache_11)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get__attributeCache_11() const { return ____attributeCache_11; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of__attributeCache_11() { return &____attributeCache_11; }
	inline void set__attributeCache_11(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		____attributeCache_11 = value;
		Il2CppCodeGenWriteBarrier((&____attributeCache_11), value);
	}

	inline static int32_t get_offset_of__extendedPropertyCache_12() { return static_cast<int32_t>(offsetof(ReflectTypeDescriptionProvider_t5736798A27F452CE006C4DBBCC78EC7928B79675_StaticFields, ____extendedPropertyCache_12)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get__extendedPropertyCache_12() const { return ____extendedPropertyCache_12; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of__extendedPropertyCache_12() { return &____extendedPropertyCache_12; }
	inline void set__extendedPropertyCache_12(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		____extendedPropertyCache_12 = value;
		Il2CppCodeGenWriteBarrier((&____extendedPropertyCache_12), value);
	}

	inline static int32_t get_offset_of__extenderProviderKey_13() { return static_cast<int32_t>(offsetof(ReflectTypeDescriptionProvider_t5736798A27F452CE006C4DBBCC78EC7928B79675_StaticFields, ____extenderProviderKey_13)); }
	inline Guid_t  get__extenderProviderKey_13() const { return ____extenderProviderKey_13; }
	inline Guid_t * get_address_of__extenderProviderKey_13() { return &____extenderProviderKey_13; }
	inline void set__extenderProviderKey_13(Guid_t  value)
	{
		____extenderProviderKey_13 = value;
	}

	inline static int32_t get_offset_of__extenderPropertiesKey_14() { return static_cast<int32_t>(offsetof(ReflectTypeDescriptionProvider_t5736798A27F452CE006C4DBBCC78EC7928B79675_StaticFields, ____extenderPropertiesKey_14)); }
	inline Guid_t  get__extenderPropertiesKey_14() const { return ____extenderPropertiesKey_14; }
	inline Guid_t * get_address_of__extenderPropertiesKey_14() { return &____extenderPropertiesKey_14; }
	inline void set__extenderPropertiesKey_14(Guid_t  value)
	{
		____extenderPropertiesKey_14 = value;
	}

	inline static int32_t get_offset_of__extenderProviderPropertiesKey_15() { return static_cast<int32_t>(offsetof(ReflectTypeDescriptionProvider_t5736798A27F452CE006C4DBBCC78EC7928B79675_StaticFields, ____extenderProviderPropertiesKey_15)); }
	inline Guid_t  get__extenderProviderPropertiesKey_15() const { return ____extenderProviderPropertiesKey_15; }
	inline Guid_t * get_address_of__extenderProviderPropertiesKey_15() { return &____extenderProviderPropertiesKey_15; }
	inline void set__extenderProviderPropertiesKey_15(Guid_t  value)
	{
		____extenderProviderPropertiesKey_15 = value;
	}

	inline static int32_t get_offset_of__skipInterfaceAttributeList_16() { return static_cast<int32_t>(offsetof(ReflectTypeDescriptionProvider_t5736798A27F452CE006C4DBBCC78EC7928B79675_StaticFields, ____skipInterfaceAttributeList_16)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get__skipInterfaceAttributeList_16() const { return ____skipInterfaceAttributeList_16; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of__skipInterfaceAttributeList_16() { return &____skipInterfaceAttributeList_16; }
	inline void set__skipInterfaceAttributeList_16(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		____skipInterfaceAttributeList_16 = value;
		Il2CppCodeGenWriteBarrier((&____skipInterfaceAttributeList_16), value);
	}

	inline static int32_t get_offset_of__internalSyncObject_17() { return static_cast<int32_t>(offsetof(ReflectTypeDescriptionProvider_t5736798A27F452CE006C4DBBCC78EC7928B79675_StaticFields, ____internalSyncObject_17)); }
	inline RuntimeObject * get__internalSyncObject_17() const { return ____internalSyncObject_17; }
	inline RuntimeObject ** get_address_of__internalSyncObject_17() { return &____internalSyncObject_17; }
	inline void set__internalSyncObject_17(RuntimeObject * value)
	{
		____internalSyncObject_17 = value;
		Il2CppCodeGenWriteBarrier((&____internalSyncObject_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFLECTTYPEDESCRIPTIONPROVIDER_T5736798A27F452CE006C4DBBCC78EC7928B79675_H
#ifndef REFRESHPROPERTIES_T1DCC9EF1AA18F3609AA7B68B33D19F22A8473788_H
#define REFRESHPROPERTIES_T1DCC9EF1AA18F3609AA7B68B33D19F22A8473788_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.RefreshProperties
struct  RefreshProperties_t1DCC9EF1AA18F3609AA7B68B33D19F22A8473788 
{
public:
	// System.Int32 System.ComponentModel.RefreshProperties::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RefreshProperties_t1DCC9EF1AA18F3609AA7B68B33D19F22A8473788, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFRESHPROPERTIES_T1DCC9EF1AA18F3609AA7B68B33D19F22A8473788_H
#ifndef RUNWORKERCOMPLETEDEVENTARGS_TFAFA9B2ED339BE1F33AF8845D46A251EB5C6950D_H
#define RUNWORKERCOMPLETEDEVENTARGS_TFAFA9B2ED339BE1F33AF8845D46A251EB5C6950D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.RunWorkerCompletedEventArgs
struct  RunWorkerCompletedEventArgs_tFAFA9B2ED339BE1F33AF8845D46A251EB5C6950D  : public AsyncCompletedEventArgs_tADCBE47368F4D8B198B46D332B27EE38C8B5F8B7
{
public:
	// System.Object System.ComponentModel.RunWorkerCompletedEventArgs::result
	RuntimeObject * ___result_4;

public:
	inline static int32_t get_offset_of_result_4() { return static_cast<int32_t>(offsetof(RunWorkerCompletedEventArgs_tFAFA9B2ED339BE1F33AF8845D46A251EB5C6950D, ___result_4)); }
	inline RuntimeObject * get_result_4() const { return ___result_4; }
	inline RuntimeObject ** get_address_of_result_4() { return &___result_4; }
	inline void set_result_4(RuntimeObject * value)
	{
		___result_4 = value;
		Il2CppCodeGenWriteBarrier((&___result_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNWORKERCOMPLETEDEVENTARGS_TFAFA9B2ED339BE1F33AF8845D46A251EB5C6950D_H
#ifndef SRCATEGORYATTRIBUTE_T1C136A51E836D96E811610B3BB0ADFE784A7CF65_H
#define SRCATEGORYATTRIBUTE_T1C136A51E836D96E811610B3BB0ADFE784A7CF65_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.SRCategoryAttribute
struct  SRCategoryAttribute_t1C136A51E836D96E811610B3BB0ADFE784A7CF65  : public CategoryAttribute_t89C58D266A4A65CD58E04FF63344AA3E0AF57B80
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SRCATEGORYATTRIBUTE_T1C136A51E836D96E811610B3BB0ADFE784A7CF65_H
#ifndef TOOLBOXITEMFILTERTYPE_T55CD1A1F306F891450DFC2290FE851E94CC3B8C5_H
#define TOOLBOXITEMFILTERTYPE_T55CD1A1F306F891450DFC2290FE851E94CC3B8C5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.ToolboxItemFilterType
struct  ToolboxItemFilterType_t55CD1A1F306F891450DFC2290FE851E94CC3B8C5 
{
public:
	// System.Int32 System.ComponentModel.ToolboxItemFilterType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ToolboxItemFilterType_t55CD1A1F306F891450DFC2290FE851E94CC3B8C5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOOLBOXITEMFILTERTYPE_T55CD1A1F306F891450DFC2290FE851E94CC3B8C5_H
#ifndef TYPECONVERTER_T8306AE03734853B551DDF089C1F17836A7764DBB_H
#define TYPECONVERTER_T8306AE03734853B551DDF089C1F17836A7764DBB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.TypeConverter
struct  TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB  : public RuntimeObject
{
public:

public:
};

struct TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB_StaticFields
{
public:
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.ComponentModel.TypeConverter::useCompatibleTypeConversion
	bool ___useCompatibleTypeConversion_1;

public:
	inline static int32_t get_offset_of_useCompatibleTypeConversion_1() { return static_cast<int32_t>(offsetof(TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB_StaticFields, ___useCompatibleTypeConversion_1)); }
	inline bool get_useCompatibleTypeConversion_1() const { return ___useCompatibleTypeConversion_1; }
	inline bool* get_address_of_useCompatibleTypeConversion_1() { return &___useCompatibleTypeConversion_1; }
	inline void set_useCompatibleTypeConversion_1(bool value)
	{
		___useCompatibleTypeConversion_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPECONVERTER_T8306AE03734853B551DDF089C1F17836A7764DBB_H
#ifndef SIMPLEPROPERTYDESCRIPTOR_TCF596E6470E66A92F1E91E28AB918097701E7CEA_H
#define SIMPLEPROPERTYDESCRIPTOR_TCF596E6470E66A92F1E91E28AB918097701E7CEA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.TypeConverter_SimplePropertyDescriptor
struct  SimplePropertyDescriptor_tCF596E6470E66A92F1E91E28AB918097701E7CEA  : public PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D
{
public:
	// System.Type System.ComponentModel.TypeConverter_SimplePropertyDescriptor::componentType
	Type_t * ___componentType_17;
	// System.Type System.ComponentModel.TypeConverter_SimplePropertyDescriptor::propertyType
	Type_t * ___propertyType_18;

public:
	inline static int32_t get_offset_of_componentType_17() { return static_cast<int32_t>(offsetof(SimplePropertyDescriptor_tCF596E6470E66A92F1E91E28AB918097701E7CEA, ___componentType_17)); }
	inline Type_t * get_componentType_17() const { return ___componentType_17; }
	inline Type_t ** get_address_of_componentType_17() { return &___componentType_17; }
	inline void set_componentType_17(Type_t * value)
	{
		___componentType_17 = value;
		Il2CppCodeGenWriteBarrier((&___componentType_17), value);
	}

	inline static int32_t get_offset_of_propertyType_18() { return static_cast<int32_t>(offsetof(SimplePropertyDescriptor_tCF596E6470E66A92F1E91E28AB918097701E7CEA, ___propertyType_18)); }
	inline Type_t * get_propertyType_18() const { return ___propertyType_18; }
	inline Type_t ** get_address_of_propertyType_18() { return &___propertyType_18; }
	inline void set_propertyType_18(Type_t * value)
	{
		___propertyType_18 = value;
		Il2CppCodeGenWriteBarrier((&___propertyType_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLEPROPERTYDESCRIPTOR_TCF596E6470E66A92F1E91E28AB918097701E7CEA_H
#ifndef WARNINGEXCEPTION_T8CDDEA53879B798257932E94B4880AC9B9AA8CD9_H
#define WARNINGEXCEPTION_T8CDDEA53879B798257932E94B4880AC9B9AA8CD9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.WarningException
struct  WarningException_t8CDDEA53879B798257932E94B4880AC9B9AA8CD9  : public SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782
{
public:
	// System.String System.ComponentModel.WarningException::helpUrl
	String_t* ___helpUrl_17;
	// System.String System.ComponentModel.WarningException::helpTopic
	String_t* ___helpTopic_18;

public:
	inline static int32_t get_offset_of_helpUrl_17() { return static_cast<int32_t>(offsetof(WarningException_t8CDDEA53879B798257932E94B4880AC9B9AA8CD9, ___helpUrl_17)); }
	inline String_t* get_helpUrl_17() const { return ___helpUrl_17; }
	inline String_t** get_address_of_helpUrl_17() { return &___helpUrl_17; }
	inline void set_helpUrl_17(String_t* value)
	{
		___helpUrl_17 = value;
		Il2CppCodeGenWriteBarrier((&___helpUrl_17), value);
	}

	inline static int32_t get_offset_of_helpTopic_18() { return static_cast<int32_t>(offsetof(WarningException_t8CDDEA53879B798257932E94B4880AC9B9AA8CD9, ___helpTopic_18)); }
	inline String_t* get_helpTopic_18() const { return ___helpTopic_18; }
	inline String_t** get_address_of_helpTopic_18() { return &___helpTopic_18; }
	inline void set_helpTopic_18(String_t* value)
	{
		___helpTopic_18 = value;
		Il2CppCodeGenWriteBarrier((&___helpTopic_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WARNINGEXCEPTION_T8CDDEA53879B798257932E94B4880AC9B9AA8CD9_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef EXTERNALEXCEPTION_T68841FD169C0CB00CC950EDA7E2A59540D65B1CE_H
#define EXTERNALEXCEPTION_T68841FD169C0CB00CC950EDA7E2A59540D65B1CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.ExternalException
struct  ExternalException_t68841FD169C0CB00CC950EDA7E2A59540D65B1CE  : public SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTERNALEXCEPTION_T68841FD169C0CB00CC950EDA7E2A59540D65B1CE_H
#ifndef SAFEHANDLE_T1E326D75E23FD5BB6D40BA322298FDC6526CC383_H
#define SAFEHANDLE_T1E326D75E23FD5BB6D40BA322298FDC6526CC383_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.SafeHandle
struct  SafeHandle_t1E326D75E23FD5BB6D40BA322298FDC6526CC383  : public CriticalFinalizerObject_t8B006E1DEE084E781F5C0F3283E9226E28894DD9
{
public:
	// System.IntPtr System.Runtime.InteropServices.SafeHandle::handle
	intptr_t ___handle_0;
	// System.Int32 System.Runtime.InteropServices.SafeHandle::_state
	int32_t ____state_1;
	// System.Boolean System.Runtime.InteropServices.SafeHandle::_ownsHandle
	bool ____ownsHandle_2;
	// System.Boolean System.Runtime.InteropServices.SafeHandle::_fullyInitialized
	bool ____fullyInitialized_3;

public:
	inline static int32_t get_offset_of_handle_0() { return static_cast<int32_t>(offsetof(SafeHandle_t1E326D75E23FD5BB6D40BA322298FDC6526CC383, ___handle_0)); }
	inline intptr_t get_handle_0() const { return ___handle_0; }
	inline intptr_t* get_address_of_handle_0() { return &___handle_0; }
	inline void set_handle_0(intptr_t value)
	{
		___handle_0 = value;
	}

	inline static int32_t get_offset_of__state_1() { return static_cast<int32_t>(offsetof(SafeHandle_t1E326D75E23FD5BB6D40BA322298FDC6526CC383, ____state_1)); }
	inline int32_t get__state_1() const { return ____state_1; }
	inline int32_t* get_address_of__state_1() { return &____state_1; }
	inline void set__state_1(int32_t value)
	{
		____state_1 = value;
	}

	inline static int32_t get_offset_of__ownsHandle_2() { return static_cast<int32_t>(offsetof(SafeHandle_t1E326D75E23FD5BB6D40BA322298FDC6526CC383, ____ownsHandle_2)); }
	inline bool get__ownsHandle_2() const { return ____ownsHandle_2; }
	inline bool* get_address_of__ownsHandle_2() { return &____ownsHandle_2; }
	inline void set__ownsHandle_2(bool value)
	{
		____ownsHandle_2 = value;
	}

	inline static int32_t get_offset_of__fullyInitialized_3() { return static_cast<int32_t>(offsetof(SafeHandle_t1E326D75E23FD5BB6D40BA322298FDC6526CC383, ____fullyInitialized_3)); }
	inline bool get__fullyInitialized_3() const { return ____fullyInitialized_3; }
	inline bool* get_address_of__fullyInitialized_3() { return &____fullyInitialized_3; }
	inline void set__fullyInitialized_3(bool value)
	{
		____fullyInitialized_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAFEHANDLE_T1E326D75E23FD5BB6D40BA322298FDC6526CC383_H
#ifndef AUTHENTICATIONEXCEPTION_TE24BF2E2AD351F0C9586A59191F01918659A7516_H
#define AUTHENTICATIONEXCEPTION_TE24BF2E2AD351F0C9586A59191F01918659A7516_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Authentication.AuthenticationException
struct  AuthenticationException_tE24BF2E2AD351F0C9586A59191F01918659A7516  : public SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTHENTICATIONEXCEPTION_TE24BF2E2AD351F0C9586A59191F01918659A7516_H
#ifndef CHANNELBINDINGKIND_T4BB80CCD48C24F639DA5A09B8DA799EA101ACA54_H
#define CHANNELBINDINGKIND_T4BB80CCD48C24F639DA5A09B8DA799EA101ACA54_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Authentication.ExtendedProtection.ChannelBindingKind
struct  ChannelBindingKind_t4BB80CCD48C24F639DA5A09B8DA799EA101ACA54 
{
public:
	// System.Int32 System.Security.Authentication.ExtendedProtection.ChannelBindingKind::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ChannelBindingKind_t4BB80CCD48C24F639DA5A09B8DA799EA101ACA54, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHANNELBINDINGKIND_T4BB80CCD48C24F639DA5A09B8DA799EA101ACA54_H
#ifndef POLICYENFORCEMENT_T8E75D3581050E49BD8C764B18CE4937D5B024E19_H
#define POLICYENFORCEMENT_T8E75D3581050E49BD8C764B18CE4937D5B024E19_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Authentication.ExtendedProtection.PolicyEnforcement
struct  PolicyEnforcement_t8E75D3581050E49BD8C764B18CE4937D5B024E19 
{
public:
	// System.Int32 System.Security.Authentication.ExtendedProtection.PolicyEnforcement::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PolicyEnforcement_t8E75D3581050E49BD8C764B18CE4937D5B024E19, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLICYENFORCEMENT_T8E75D3581050E49BD8C764B18CE4937D5B024E19_H
#ifndef SSLPROTOCOLS_TDD37F8F06AD19BDAF27AEA484EC06820FE3107AE_H
#define SSLPROTOCOLS_TDD37F8F06AD19BDAF27AEA484EC06820FE3107AE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Authentication.SslProtocols
struct  SslProtocols_tDD37F8F06AD19BDAF27AEA484EC06820FE3107AE 
{
public:
	// System.Int32 System.Security.Authentication.SslProtocols::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SslProtocols_tDD37F8F06AD19BDAF27AEA484EC06820FE3107AE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SSLPROTOCOLS_TDD37F8F06AD19BDAF27AEA484EC06820FE3107AE_H
#ifndef ASNDECODESTATUS_T83139F58FFE08CE7DBCB990C9F30D2F2CA5BC0BB_H
#define ASNDECODESTATUS_T83139F58FFE08CE7DBCB990C9F30D2F2CA5BC0BB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.AsnDecodeStatus
struct  AsnDecodeStatus_t83139F58FFE08CE7DBCB990C9F30D2F2CA5BC0BB 
{
public:
	// System.Int32 System.Security.Cryptography.AsnDecodeStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AsnDecodeStatus_t83139F58FFE08CE7DBCB990C9F30D2F2CA5BC0BB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASNDECODESTATUS_T83139F58FFE08CE7DBCB990C9F30D2F2CA5BC0BB_H
#ifndef OIDGROUP_T9A99D3013C1B94CB060656F30C39E893E75FAD6B_H
#define OIDGROUP_T9A99D3013C1B94CB060656F30C39E893E75FAD6B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.OidGroup
struct  OidGroup_t9A99D3013C1B94CB060656F30C39E893E75FAD6B 
{
public:
	// System.Int32 System.Security.Cryptography.OidGroup::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(OidGroup_t9A99D3013C1B94CB060656F30C39E893E75FAD6B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OIDGROUP_T9A99D3013C1B94CB060656F30C39E893E75FAD6B_H
#ifndef OPENFLAGS_T498C8F28F409EF03967CA8451E8F7D191EE84596_H
#define OPENFLAGS_T498C8F28F409EF03967CA8451E8F7D191EE84596_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.OpenFlags
struct  OpenFlags_t498C8F28F409EF03967CA8451E8F7D191EE84596 
{
public:
	// System.Int32 System.Security.Cryptography.X509Certificates.OpenFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(OpenFlags_t498C8F28F409EF03967CA8451E8F7D191EE84596, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPENFLAGS_T498C8F28F409EF03967CA8451E8F7D191EE84596_H
#ifndef WEAKREFERENCE_T0495CC81CD6403E662B7700B802443F6F730E39D_H
#define WEAKREFERENCE_T0495CC81CD6403E662B7700B802443F6F730E39D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.WeakReference
struct  WeakReference_t0495CC81CD6403E662B7700B802443F6F730E39D  : public RuntimeObject
{
public:
	// System.Boolean System.WeakReference::isLongReference
	bool ___isLongReference_0;
	// System.Runtime.InteropServices.GCHandle System.WeakReference::gcHandle
	GCHandle_t39FAEE3EA592432C93B574A31DD83B87F1847DE3  ___gcHandle_1;

public:
	inline static int32_t get_offset_of_isLongReference_0() { return static_cast<int32_t>(offsetof(WeakReference_t0495CC81CD6403E662B7700B802443F6F730E39D, ___isLongReference_0)); }
	inline bool get_isLongReference_0() const { return ___isLongReference_0; }
	inline bool* get_address_of_isLongReference_0() { return &___isLongReference_0; }
	inline void set_isLongReference_0(bool value)
	{
		___isLongReference_0 = value;
	}

	inline static int32_t get_offset_of_gcHandle_1() { return static_cast<int32_t>(offsetof(WeakReference_t0495CC81CD6403E662B7700B802443F6F730E39D, ___gcHandle_1)); }
	inline GCHandle_t39FAEE3EA592432C93B574A31DD83B87F1847DE3  get_gcHandle_1() const { return ___gcHandle_1; }
	inline GCHandle_t39FAEE3EA592432C93B574A31DD83B87F1847DE3 * get_address_of_gcHandle_1() { return &___gcHandle_1; }
	inline void set_gcHandle_1(GCHandle_t39FAEE3EA592432C93B574A31DD83B87F1847DE3  value)
	{
		___gcHandle_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEAKREFERENCE_T0495CC81CD6403E662B7700B802443F6F730E39D_H
#ifndef SAFEHANDLEZEROORMINUSONEISINVALID_T779A965C82098677DF1ED10A134DBCDEC8AACB8E_H
#define SAFEHANDLEZEROORMINUSONEISINVALID_T779A965C82098677DF1ED10A134DBCDEC8AACB8E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.Win32.SafeHandles.SafeHandleZeroOrMinusOneIsInvalid
struct  SafeHandleZeroOrMinusOneIsInvalid_t779A965C82098677DF1ED10A134DBCDEC8AACB8E  : public SafeHandle_t1E326D75E23FD5BB6D40BA322298FDC6526CC383
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAFEHANDLEZEROORMINUSONEISINVALID_T779A965C82098677DF1ED10A134DBCDEC8AACB8E_H
#ifndef BASENUMBERCONVERTER_T6AF36A2503E7BABF7FB9A8EC05DF8B828491AC63_H
#define BASENUMBERCONVERTER_T6AF36A2503E7BABF7FB9A8EC05DF8B828491AC63_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.BaseNumberConverter
struct  BaseNumberConverter_t6AF36A2503E7BABF7FB9A8EC05DF8B828491AC63  : public TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASENUMBERCONVERTER_T6AF36A2503E7BABF7FB9A8EC05DF8B828491AC63_H
#ifndef CHECKOUTEXCEPTION_TF657C421DAEECE88F94372CEA043DE1D9CCAC9C5_H
#define CHECKOUTEXCEPTION_TF657C421DAEECE88F94372CEA043DE1D9CCAC9C5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.Design.CheckoutException
struct  CheckoutException_tF657C421DAEECE88F94372CEA043DE1D9CCAC9C5  : public ExternalException_t68841FD169C0CB00CC950EDA7E2A59540D65B1CE
{
public:

public:
};

struct CheckoutException_tF657C421DAEECE88F94372CEA043DE1D9CCAC9C5_StaticFields
{
public:
	// System.ComponentModel.Design.CheckoutException System.ComponentModel.Design.CheckoutException::Canceled
	CheckoutException_tF657C421DAEECE88F94372CEA043DE1D9CCAC9C5 * ___Canceled_17;

public:
	inline static int32_t get_offset_of_Canceled_17() { return static_cast<int32_t>(offsetof(CheckoutException_tF657C421DAEECE88F94372CEA043DE1D9CCAC9C5_StaticFields, ___Canceled_17)); }
	inline CheckoutException_tF657C421DAEECE88F94372CEA043DE1D9CCAC9C5 * get_Canceled_17() const { return ___Canceled_17; }
	inline CheckoutException_tF657C421DAEECE88F94372CEA043DE1D9CCAC9C5 ** get_address_of_Canceled_17() { return &___Canceled_17; }
	inline void set_Canceled_17(CheckoutException_tF657C421DAEECE88F94372CEA043DE1D9CCAC9C5 * value)
	{
		___Canceled_17 = value;
		Il2CppCodeGenWriteBarrier((&___Canceled_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHECKOUTEXCEPTION_TF657C421DAEECE88F94372CEA043DE1D9CCAC9C5_H
#ifndef INHERITANCEATTRIBUTE_TBB0E10C86B9ECC652035969169FC556F1C6C8AC4_H
#define INHERITANCEATTRIBUTE_TBB0E10C86B9ECC652035969169FC556F1C6C8AC4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.InheritanceAttribute
struct  InheritanceAttribute_tBB0E10C86B9ECC652035969169FC556F1C6C8AC4  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.ComponentModel.InheritanceLevel System.ComponentModel.InheritanceAttribute::inheritanceLevel
	int32_t ___inheritanceLevel_0;

public:
	inline static int32_t get_offset_of_inheritanceLevel_0() { return static_cast<int32_t>(offsetof(InheritanceAttribute_tBB0E10C86B9ECC652035969169FC556F1C6C8AC4, ___inheritanceLevel_0)); }
	inline int32_t get_inheritanceLevel_0() const { return ___inheritanceLevel_0; }
	inline int32_t* get_address_of_inheritanceLevel_0() { return &___inheritanceLevel_0; }
	inline void set_inheritanceLevel_0(int32_t value)
	{
		___inheritanceLevel_0 = value;
	}
};

struct InheritanceAttribute_tBB0E10C86B9ECC652035969169FC556F1C6C8AC4_StaticFields
{
public:
	// System.ComponentModel.InheritanceAttribute System.ComponentModel.InheritanceAttribute::Inherited
	InheritanceAttribute_tBB0E10C86B9ECC652035969169FC556F1C6C8AC4 * ___Inherited_1;
	// System.ComponentModel.InheritanceAttribute System.ComponentModel.InheritanceAttribute::InheritedReadOnly
	InheritanceAttribute_tBB0E10C86B9ECC652035969169FC556F1C6C8AC4 * ___InheritedReadOnly_2;
	// System.ComponentModel.InheritanceAttribute System.ComponentModel.InheritanceAttribute::NotInherited
	InheritanceAttribute_tBB0E10C86B9ECC652035969169FC556F1C6C8AC4 * ___NotInherited_3;
	// System.ComponentModel.InheritanceAttribute System.ComponentModel.InheritanceAttribute::Default
	InheritanceAttribute_tBB0E10C86B9ECC652035969169FC556F1C6C8AC4 * ___Default_4;

public:
	inline static int32_t get_offset_of_Inherited_1() { return static_cast<int32_t>(offsetof(InheritanceAttribute_tBB0E10C86B9ECC652035969169FC556F1C6C8AC4_StaticFields, ___Inherited_1)); }
	inline InheritanceAttribute_tBB0E10C86B9ECC652035969169FC556F1C6C8AC4 * get_Inherited_1() const { return ___Inherited_1; }
	inline InheritanceAttribute_tBB0E10C86B9ECC652035969169FC556F1C6C8AC4 ** get_address_of_Inherited_1() { return &___Inherited_1; }
	inline void set_Inherited_1(InheritanceAttribute_tBB0E10C86B9ECC652035969169FC556F1C6C8AC4 * value)
	{
		___Inherited_1 = value;
		Il2CppCodeGenWriteBarrier((&___Inherited_1), value);
	}

	inline static int32_t get_offset_of_InheritedReadOnly_2() { return static_cast<int32_t>(offsetof(InheritanceAttribute_tBB0E10C86B9ECC652035969169FC556F1C6C8AC4_StaticFields, ___InheritedReadOnly_2)); }
	inline InheritanceAttribute_tBB0E10C86B9ECC652035969169FC556F1C6C8AC4 * get_InheritedReadOnly_2() const { return ___InheritedReadOnly_2; }
	inline InheritanceAttribute_tBB0E10C86B9ECC652035969169FC556F1C6C8AC4 ** get_address_of_InheritedReadOnly_2() { return &___InheritedReadOnly_2; }
	inline void set_InheritedReadOnly_2(InheritanceAttribute_tBB0E10C86B9ECC652035969169FC556F1C6C8AC4 * value)
	{
		___InheritedReadOnly_2 = value;
		Il2CppCodeGenWriteBarrier((&___InheritedReadOnly_2), value);
	}

	inline static int32_t get_offset_of_NotInherited_3() { return static_cast<int32_t>(offsetof(InheritanceAttribute_tBB0E10C86B9ECC652035969169FC556F1C6C8AC4_StaticFields, ___NotInherited_3)); }
	inline InheritanceAttribute_tBB0E10C86B9ECC652035969169FC556F1C6C8AC4 * get_NotInherited_3() const { return ___NotInherited_3; }
	inline InheritanceAttribute_tBB0E10C86B9ECC652035969169FC556F1C6C8AC4 ** get_address_of_NotInherited_3() { return &___NotInherited_3; }
	inline void set_NotInherited_3(InheritanceAttribute_tBB0E10C86B9ECC652035969169FC556F1C6C8AC4 * value)
	{
		___NotInherited_3 = value;
		Il2CppCodeGenWriteBarrier((&___NotInherited_3), value);
	}

	inline static int32_t get_offset_of_Default_4() { return static_cast<int32_t>(offsetof(InheritanceAttribute_tBB0E10C86B9ECC652035969169FC556F1C6C8AC4_StaticFields, ___Default_4)); }
	inline InheritanceAttribute_tBB0E10C86B9ECC652035969169FC556F1C6C8AC4 * get_Default_4() const { return ___Default_4; }
	inline InheritanceAttribute_tBB0E10C86B9ECC652035969169FC556F1C6C8AC4 ** get_address_of_Default_4() { return &___Default_4; }
	inline void set_Default_4(InheritanceAttribute_tBB0E10C86B9ECC652035969169FC556F1C6C8AC4 * value)
	{
		___Default_4 = value;
		Il2CppCodeGenWriteBarrier((&___Default_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INHERITANCEATTRIBUTE_TBB0E10C86B9ECC652035969169FC556F1C6C8AC4_H
#ifndef REFERENCECONVERTER_T5080472EE999A1F00721E6C5C97013762C85C7E4_H
#define REFERENCECONVERTER_T5080472EE999A1F00721E6C5C97013762C85C7E4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.ReferenceConverter
struct  ReferenceConverter_t5080472EE999A1F00721E6C5C97013762C85C7E4  : public TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB
{
public:
	// System.Type System.ComponentModel.ReferenceConverter::type
	Type_t * ___type_3;

public:
	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(ReferenceConverter_t5080472EE999A1F00721E6C5C97013762C85C7E4, ___type_3)); }
	inline Type_t * get_type_3() const { return ___type_3; }
	inline Type_t ** get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(Type_t * value)
	{
		___type_3 = value;
		Il2CppCodeGenWriteBarrier((&___type_3), value);
	}
};

struct ReferenceConverter_t5080472EE999A1F00721E6C5C97013762C85C7E4_StaticFields
{
public:
	// System.String System.ComponentModel.ReferenceConverter::none
	String_t* ___none_2;

public:
	inline static int32_t get_offset_of_none_2() { return static_cast<int32_t>(offsetof(ReferenceConverter_t5080472EE999A1F00721E6C5C97013762C85C7E4_StaticFields, ___none_2)); }
	inline String_t* get_none_2() const { return ___none_2; }
	inline String_t** get_address_of_none_2() { return &___none_2; }
	inline void set_none_2(String_t* value)
	{
		___none_2 = value;
		Il2CppCodeGenWriteBarrier((&___none_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFERENCECONVERTER_T5080472EE999A1F00721E6C5C97013762C85C7E4_H
#ifndef REFRESHPROPERTIESATTRIBUTE_TC6E685EE61C7BFE437F9ABAE7AB9B5B83FEB3DC1_H
#define REFRESHPROPERTIESATTRIBUTE_TC6E685EE61C7BFE437F9ABAE7AB9B5B83FEB3DC1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.RefreshPropertiesAttribute
struct  RefreshPropertiesAttribute_tC6E685EE61C7BFE437F9ABAE7AB9B5B83FEB3DC1  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.ComponentModel.RefreshProperties System.ComponentModel.RefreshPropertiesAttribute::refresh
	int32_t ___refresh_3;

public:
	inline static int32_t get_offset_of_refresh_3() { return static_cast<int32_t>(offsetof(RefreshPropertiesAttribute_tC6E685EE61C7BFE437F9ABAE7AB9B5B83FEB3DC1, ___refresh_3)); }
	inline int32_t get_refresh_3() const { return ___refresh_3; }
	inline int32_t* get_address_of_refresh_3() { return &___refresh_3; }
	inline void set_refresh_3(int32_t value)
	{
		___refresh_3 = value;
	}
};

struct RefreshPropertiesAttribute_tC6E685EE61C7BFE437F9ABAE7AB9B5B83FEB3DC1_StaticFields
{
public:
	// System.ComponentModel.RefreshPropertiesAttribute System.ComponentModel.RefreshPropertiesAttribute::All
	RefreshPropertiesAttribute_tC6E685EE61C7BFE437F9ABAE7AB9B5B83FEB3DC1 * ___All_0;
	// System.ComponentModel.RefreshPropertiesAttribute System.ComponentModel.RefreshPropertiesAttribute::Repaint
	RefreshPropertiesAttribute_tC6E685EE61C7BFE437F9ABAE7AB9B5B83FEB3DC1 * ___Repaint_1;
	// System.ComponentModel.RefreshPropertiesAttribute System.ComponentModel.RefreshPropertiesAttribute::Default
	RefreshPropertiesAttribute_tC6E685EE61C7BFE437F9ABAE7AB9B5B83FEB3DC1 * ___Default_2;

public:
	inline static int32_t get_offset_of_All_0() { return static_cast<int32_t>(offsetof(RefreshPropertiesAttribute_tC6E685EE61C7BFE437F9ABAE7AB9B5B83FEB3DC1_StaticFields, ___All_0)); }
	inline RefreshPropertiesAttribute_tC6E685EE61C7BFE437F9ABAE7AB9B5B83FEB3DC1 * get_All_0() const { return ___All_0; }
	inline RefreshPropertiesAttribute_tC6E685EE61C7BFE437F9ABAE7AB9B5B83FEB3DC1 ** get_address_of_All_0() { return &___All_0; }
	inline void set_All_0(RefreshPropertiesAttribute_tC6E685EE61C7BFE437F9ABAE7AB9B5B83FEB3DC1 * value)
	{
		___All_0 = value;
		Il2CppCodeGenWriteBarrier((&___All_0), value);
	}

	inline static int32_t get_offset_of_Repaint_1() { return static_cast<int32_t>(offsetof(RefreshPropertiesAttribute_tC6E685EE61C7BFE437F9ABAE7AB9B5B83FEB3DC1_StaticFields, ___Repaint_1)); }
	inline RefreshPropertiesAttribute_tC6E685EE61C7BFE437F9ABAE7AB9B5B83FEB3DC1 * get_Repaint_1() const { return ___Repaint_1; }
	inline RefreshPropertiesAttribute_tC6E685EE61C7BFE437F9ABAE7AB9B5B83FEB3DC1 ** get_address_of_Repaint_1() { return &___Repaint_1; }
	inline void set_Repaint_1(RefreshPropertiesAttribute_tC6E685EE61C7BFE437F9ABAE7AB9B5B83FEB3DC1 * value)
	{
		___Repaint_1 = value;
		Il2CppCodeGenWriteBarrier((&___Repaint_1), value);
	}

	inline static int32_t get_offset_of_Default_2() { return static_cast<int32_t>(offsetof(RefreshPropertiesAttribute_tC6E685EE61C7BFE437F9ABAE7AB9B5B83FEB3DC1_StaticFields, ___Default_2)); }
	inline RefreshPropertiesAttribute_tC6E685EE61C7BFE437F9ABAE7AB9B5B83FEB3DC1 * get_Default_2() const { return ___Default_2; }
	inline RefreshPropertiesAttribute_tC6E685EE61C7BFE437F9ABAE7AB9B5B83FEB3DC1 ** get_address_of_Default_2() { return &___Default_2; }
	inline void set_Default_2(RefreshPropertiesAttribute_tC6E685EE61C7BFE437F9ABAE7AB9B5B83FEB3DC1 * value)
	{
		___Default_2 = value;
		Il2CppCodeGenWriteBarrier((&___Default_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFRESHPROPERTIESATTRIBUTE_TC6E685EE61C7BFE437F9ABAE7AB9B5B83FEB3DC1_H
#ifndef STRINGCONVERTER_T054FA0796F4C8E951208AFA052D99BCB8E68BED7_H
#define STRINGCONVERTER_T054FA0796F4C8E951208AFA052D99BCB8E68BED7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.StringConverter
struct  StringConverter_t054FA0796F4C8E951208AFA052D99BCB8E68BED7  : public TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGCONVERTER_T054FA0796F4C8E951208AFA052D99BCB8E68BED7_H
#ifndef TIMESPANCONVERTER_T4025A0861C52420BC73D837729E5EFA6ECDE07C7_H
#define TIMESPANCONVERTER_T4025A0861C52420BC73D837729E5EFA6ECDE07C7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.TimeSpanConverter
struct  TimeSpanConverter_t4025A0861C52420BC73D837729E5EFA6ECDE07C7  : public TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPANCONVERTER_T4025A0861C52420BC73D837729E5EFA6ECDE07C7_H
#ifndef TOOLBOXITEMFILTERATTRIBUTE_TB15560039BF68E3DC316F8718947E3E7A54C6910_H
#define TOOLBOXITEMFILTERATTRIBUTE_TB15560039BF68E3DC316F8718947E3E7A54C6910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.ToolboxItemFilterAttribute
struct  ToolboxItemFilterAttribute_tB15560039BF68E3DC316F8718947E3E7A54C6910  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.ComponentModel.ToolboxItemFilterType System.ComponentModel.ToolboxItemFilterAttribute::filterType
	int32_t ___filterType_0;
	// System.String System.ComponentModel.ToolboxItemFilterAttribute::filterString
	String_t* ___filterString_1;
	// System.String System.ComponentModel.ToolboxItemFilterAttribute::typeId
	String_t* ___typeId_2;

public:
	inline static int32_t get_offset_of_filterType_0() { return static_cast<int32_t>(offsetof(ToolboxItemFilterAttribute_tB15560039BF68E3DC316F8718947E3E7A54C6910, ___filterType_0)); }
	inline int32_t get_filterType_0() const { return ___filterType_0; }
	inline int32_t* get_address_of_filterType_0() { return &___filterType_0; }
	inline void set_filterType_0(int32_t value)
	{
		___filterType_0 = value;
	}

	inline static int32_t get_offset_of_filterString_1() { return static_cast<int32_t>(offsetof(ToolboxItemFilterAttribute_tB15560039BF68E3DC316F8718947E3E7A54C6910, ___filterString_1)); }
	inline String_t* get_filterString_1() const { return ___filterString_1; }
	inline String_t** get_address_of_filterString_1() { return &___filterString_1; }
	inline void set_filterString_1(String_t* value)
	{
		___filterString_1 = value;
		Il2CppCodeGenWriteBarrier((&___filterString_1), value);
	}

	inline static int32_t get_offset_of_typeId_2() { return static_cast<int32_t>(offsetof(ToolboxItemFilterAttribute_tB15560039BF68E3DC316F8718947E3E7A54C6910, ___typeId_2)); }
	inline String_t* get_typeId_2() const { return ___typeId_2; }
	inline String_t** get_address_of_typeId_2() { return &___typeId_2; }
	inline void set_typeId_2(String_t* value)
	{
		___typeId_2 = value;
		Il2CppCodeGenWriteBarrier((&___typeId_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOOLBOXITEMFILTERATTRIBUTE_TB15560039BF68E3DC316F8718947E3E7A54C6910_H
#ifndef TYPELISTCONVERTER_TD02D4A8E7829B15240C8A2C2F54D5672513B05C0_H
#define TYPELISTCONVERTER_TD02D4A8E7829B15240C8A2C2F54D5672513B05C0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.TypeListConverter
struct  TypeListConverter_tD02D4A8E7829B15240C8A2C2F54D5672513B05C0  : public TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB
{
public:
	// System.Type[] System.ComponentModel.TypeListConverter::types
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___types_2;
	// System.ComponentModel.TypeConverter_StandardValuesCollection System.ComponentModel.TypeListConverter::values
	StandardValuesCollection_t929677712574EF02F5C4CF4C38E070841C20BDA3 * ___values_3;

public:
	inline static int32_t get_offset_of_types_2() { return static_cast<int32_t>(offsetof(TypeListConverter_tD02D4A8E7829B15240C8A2C2F54D5672513B05C0, ___types_2)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get_types_2() const { return ___types_2; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of_types_2() { return &___types_2; }
	inline void set_types_2(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		___types_2 = value;
		Il2CppCodeGenWriteBarrier((&___types_2), value);
	}

	inline static int32_t get_offset_of_values_3() { return static_cast<int32_t>(offsetof(TypeListConverter_tD02D4A8E7829B15240C8A2C2F54D5672513B05C0, ___values_3)); }
	inline StandardValuesCollection_t929677712574EF02F5C4CF4C38E070841C20BDA3 * get_values_3() const { return ___values_3; }
	inline StandardValuesCollection_t929677712574EF02F5C4CF4C38E070841C20BDA3 ** get_address_of_values_3() { return &___values_3; }
	inline void set_values_3(StandardValuesCollection_t929677712574EF02F5C4CF4C38E070841C20BDA3 * value)
	{
		___values_3 = value;
		Il2CppCodeGenWriteBarrier((&___values_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPELISTCONVERTER_TD02D4A8E7829B15240C8A2C2F54D5672513B05C0_H
#ifndef WEAKHASHTABLE_T230AE0A1A5D5EE8C97A2280405572D83D757B2CF_H
#define WEAKHASHTABLE_T230AE0A1A5D5EE8C97A2280405572D83D757B2CF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.WeakHashtable
struct  WeakHashtable_t230AE0A1A5D5EE8C97A2280405572D83D757B2CF  : public Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9
{
public:
	// System.Int64 System.ComponentModel.WeakHashtable::_lastGlobalMem
	int64_t ____lastGlobalMem_22;
	// System.Int32 System.ComponentModel.WeakHashtable::_lastHashCount
	int32_t ____lastHashCount_23;

public:
	inline static int32_t get_offset_of__lastGlobalMem_22() { return static_cast<int32_t>(offsetof(WeakHashtable_t230AE0A1A5D5EE8C97A2280405572D83D757B2CF, ____lastGlobalMem_22)); }
	inline int64_t get__lastGlobalMem_22() const { return ____lastGlobalMem_22; }
	inline int64_t* get_address_of__lastGlobalMem_22() { return &____lastGlobalMem_22; }
	inline void set__lastGlobalMem_22(int64_t value)
	{
		____lastGlobalMem_22 = value;
	}

	inline static int32_t get_offset_of__lastHashCount_23() { return static_cast<int32_t>(offsetof(WeakHashtable_t230AE0A1A5D5EE8C97A2280405572D83D757B2CF, ____lastHashCount_23)); }
	inline int32_t get__lastHashCount_23() const { return ____lastHashCount_23; }
	inline int32_t* get_address_of__lastHashCount_23() { return &____lastHashCount_23; }
	inline void set__lastHashCount_23(int32_t value)
	{
		____lastHashCount_23 = value;
	}
};

struct WeakHashtable_t230AE0A1A5D5EE8C97A2280405572D83D757B2CF_StaticFields
{
public:
	// System.Collections.IEqualityComparer System.ComponentModel.WeakHashtable::_comparer
	RuntimeObject* ____comparer_21;

public:
	inline static int32_t get_offset_of__comparer_21() { return static_cast<int32_t>(offsetof(WeakHashtable_t230AE0A1A5D5EE8C97A2280405572D83D757B2CF_StaticFields, ____comparer_21)); }
	inline RuntimeObject* get__comparer_21() const { return ____comparer_21; }
	inline RuntimeObject** get_address_of__comparer_21() { return &____comparer_21; }
	inline void set__comparer_21(RuntimeObject* value)
	{
		____comparer_21 = value;
		Il2CppCodeGenWriteBarrier((&____comparer_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEAKHASHTABLE_T230AE0A1A5D5EE8C97A2280405572D83D757B2CF_H
#ifndef EQUALITYWEAKREFERENCE_T1A132A1151E232922D8016D93B3C6484D7792B54_H
#define EQUALITYWEAKREFERENCE_T1A132A1151E232922D8016D93B3C6484D7792B54_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.WeakHashtable_EqualityWeakReference
struct  EqualityWeakReference_t1A132A1151E232922D8016D93B3C6484D7792B54  : public WeakReference_t0495CC81CD6403E662B7700B802443F6F730E39D
{
public:
	// System.Int32 System.ComponentModel.WeakHashtable_EqualityWeakReference::_hashCode
	int32_t ____hashCode_2;

public:
	inline static int32_t get_offset_of__hashCode_2() { return static_cast<int32_t>(offsetof(EqualityWeakReference_t1A132A1151E232922D8016D93B3C6484D7792B54, ____hashCode_2)); }
	inline int32_t get__hashCode_2() const { return ____hashCode_2; }
	inline int32_t* get_address_of__hashCode_2() { return &____hashCode_2; }
	inline void set__hashCode_2(int32_t value)
	{
		____hashCode_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYWEAKREFERENCE_T1A132A1151E232922D8016D93B3C6484D7792B54_H
#ifndef WIN32EXCEPTION_TB05BE97AB4CADD54DF96C0109689F0ECA7517668_H
#define WIN32EXCEPTION_TB05BE97AB4CADD54DF96C0109689F0ECA7517668_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.Win32Exception
struct  Win32Exception_tB05BE97AB4CADD54DF96C0109689F0ECA7517668  : public ExternalException_t68841FD169C0CB00CC950EDA7E2A59540D65B1CE
{
public:
	// System.Int32 System.ComponentModel.Win32Exception::nativeErrorCode
	int32_t ___nativeErrorCode_17;

public:
	inline static int32_t get_offset_of_nativeErrorCode_17() { return static_cast<int32_t>(offsetof(Win32Exception_tB05BE97AB4CADD54DF96C0109689F0ECA7517668, ___nativeErrorCode_17)); }
	inline int32_t get_nativeErrorCode_17() const { return ___nativeErrorCode_17; }
	inline int32_t* get_address_of_nativeErrorCode_17() { return &___nativeErrorCode_17; }
	inline void set_nativeErrorCode_17(int32_t value)
	{
		___nativeErrorCode_17 = value;
	}
};

struct Win32Exception_tB05BE97AB4CADD54DF96C0109689F0ECA7517668_StaticFields
{
public:
	// System.Boolean System.ComponentModel.Win32Exception::s_ErrorMessagesInitialized
	bool ___s_ErrorMessagesInitialized_18;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.String> System.ComponentModel.Win32Exception::s_ErrorMessage
	Dictionary_2_t4EFE6A1D6502662B911688316C6920444A18CF0C * ___s_ErrorMessage_19;

public:
	inline static int32_t get_offset_of_s_ErrorMessagesInitialized_18() { return static_cast<int32_t>(offsetof(Win32Exception_tB05BE97AB4CADD54DF96C0109689F0ECA7517668_StaticFields, ___s_ErrorMessagesInitialized_18)); }
	inline bool get_s_ErrorMessagesInitialized_18() const { return ___s_ErrorMessagesInitialized_18; }
	inline bool* get_address_of_s_ErrorMessagesInitialized_18() { return &___s_ErrorMessagesInitialized_18; }
	inline void set_s_ErrorMessagesInitialized_18(bool value)
	{
		___s_ErrorMessagesInitialized_18 = value;
	}

	inline static int32_t get_offset_of_s_ErrorMessage_19() { return static_cast<int32_t>(offsetof(Win32Exception_tB05BE97AB4CADD54DF96C0109689F0ECA7517668_StaticFields, ___s_ErrorMessage_19)); }
	inline Dictionary_2_t4EFE6A1D6502662B911688316C6920444A18CF0C * get_s_ErrorMessage_19() const { return ___s_ErrorMessage_19; }
	inline Dictionary_2_t4EFE6A1D6502662B911688316C6920444A18CF0C ** get_address_of_s_ErrorMessage_19() { return &___s_ErrorMessage_19; }
	inline void set_s_ErrorMessage_19(Dictionary_2_t4EFE6A1D6502662B911688316C6920444A18CF0C * value)
	{
		___s_ErrorMessage_19 = value;
		Il2CppCodeGenWriteBarrier((&___s_ErrorMessage_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WIN32EXCEPTION_TB05BE97AB4CADD54DF96C0109689F0ECA7517668_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef EXTENDEDPROTECTIONPOLICYTYPECONVERTER_T8B9718D9F32564C56F139A2E86F268BFEF7A9804_H
#define EXTENDEDPROTECTIONPOLICYTYPECONVERTER_T8B9718D9F32564C56F139A2E86F268BFEF7A9804_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Authentication.ExtendedProtection.ExtendedProtectionPolicyTypeConverter
struct  ExtendedProtectionPolicyTypeConverter_t8B9718D9F32564C56F139A2E86F268BFEF7A9804  : public TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENDEDPROTECTIONPOLICYTYPECONVERTER_T8B9718D9F32564C56F139A2E86F268BFEF7A9804_H
#ifndef OID_TC00A10270EAF16BBF0F2619B9AEC883E0CFE6137_H
#define OID_TC00A10270EAF16BBF0F2619B9AEC883E0CFE6137_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.Oid
struct  Oid_tC00A10270EAF16BBF0F2619B9AEC883E0CFE6137  : public RuntimeObject
{
public:
	// System.String System.Security.Cryptography.Oid::m_value
	String_t* ___m_value_0;
	// System.String System.Security.Cryptography.Oid::m_friendlyName
	String_t* ___m_friendlyName_1;
	// System.Security.Cryptography.OidGroup System.Security.Cryptography.Oid::m_group
	int32_t ___m_group_2;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Oid_tC00A10270EAF16BBF0F2619B9AEC883E0CFE6137, ___m_value_0)); }
	inline String_t* get_m_value_0() const { return ___m_value_0; }
	inline String_t** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(String_t* value)
	{
		___m_value_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_value_0), value);
	}

	inline static int32_t get_offset_of_m_friendlyName_1() { return static_cast<int32_t>(offsetof(Oid_tC00A10270EAF16BBF0F2619B9AEC883E0CFE6137, ___m_friendlyName_1)); }
	inline String_t* get_m_friendlyName_1() const { return ___m_friendlyName_1; }
	inline String_t** get_address_of_m_friendlyName_1() { return &___m_friendlyName_1; }
	inline void set_m_friendlyName_1(String_t* value)
	{
		___m_friendlyName_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_friendlyName_1), value);
	}

	inline static int32_t get_offset_of_m_group_2() { return static_cast<int32_t>(offsetof(Oid_tC00A10270EAF16BBF0F2619B9AEC883E0CFE6137, ___m_group_2)); }
	inline int32_t get_m_group_2() const { return ___m_group_2; }
	inline int32_t* get_address_of_m_group_2() { return &___m_group_2; }
	inline void set_m_group_2(int32_t value)
	{
		___m_group_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OID_TC00A10270EAF16BBF0F2619B9AEC883E0CFE6137_H
#ifndef REFRESHEVENTHANDLER_TE1B96D1B5C9580F60558220F9DEBA92333849F27_H
#define REFRESHEVENTHANDLER_TE1B96D1B5C9580F60558220F9DEBA92333849F27_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.RefreshEventHandler
struct  RefreshEventHandler_tE1B96D1B5C9580F60558220F9DEBA92333849F27  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFRESHEVENTHANDLER_TE1B96D1B5C9580F60558220F9DEBA92333849F27_H
#ifndef RUNWORKERCOMPLETEDEVENTHANDLER_T8E766853412F70C3B8826718D59377F7F464BCAB_H
#define RUNWORKERCOMPLETEDEVENTHANDLER_T8E766853412F70C3B8826718D59377F7F464BCAB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.RunWorkerCompletedEventHandler
struct  RunWorkerCompletedEventHandler_t8E766853412F70C3B8826718D59377F7F464BCAB  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNWORKERCOMPLETEDEVENTHANDLER_T8E766853412F70C3B8826718D59377F7F464BCAB_H
#ifndef SBYTECONVERTER_T99F8032C93CF0F34D8C8BB8AA744D39E794AA4B7_H
#define SBYTECONVERTER_T99F8032C93CF0F34D8C8BB8AA744D39E794AA4B7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.SByteConverter
struct  SByteConverter_t99F8032C93CF0F34D8C8BB8AA744D39E794AA4B7  : public BaseNumberConverter_t6AF36A2503E7BABF7FB9A8EC05DF8B828491AC63
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SBYTECONVERTER_T99F8032C93CF0F34D8C8BB8AA744D39E794AA4B7_H
#ifndef SINGLECONVERTER_T86A24FBD46D753B99344470E9566584F15538902_H
#define SINGLECONVERTER_T86A24FBD46D753B99344470E9566584F15538902_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.SingleConverter
struct  SingleConverter_t86A24FBD46D753B99344470E9566584F15538902  : public BaseNumberConverter_t6AF36A2503E7BABF7FB9A8EC05DF8B828491AC63
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLECONVERTER_T86A24FBD46D753B99344470E9566584F15538902_H
#ifndef UINT16CONVERTER_T4BC0EAF1F93E418E020D9F6636F89276782C6803_H
#define UINT16CONVERTER_T4BC0EAF1F93E418E020D9F6636F89276782C6803_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.UInt16Converter
struct  UInt16Converter_t4BC0EAF1F93E418E020D9F6636F89276782C6803  : public BaseNumberConverter_t6AF36A2503E7BABF7FB9A8EC05DF8B828491AC63
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT16CONVERTER_T4BC0EAF1F93E418E020D9F6636F89276782C6803_H
#ifndef UINT32CONVERTER_T13F2DD92CDD0CD6A0EFD9D0F45AF88B3893DCA65_H
#define UINT32CONVERTER_T13F2DD92CDD0CD6A0EFD9D0F45AF88B3893DCA65_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.UInt32Converter
struct  UInt32Converter_t13F2DD92CDD0CD6A0EFD9D0F45AF88B3893DCA65  : public BaseNumberConverter_t6AF36A2503E7BABF7FB9A8EC05DF8B828491AC63
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT32CONVERTER_T13F2DD92CDD0CD6A0EFD9D0F45AF88B3893DCA65_H
#ifndef UINT64CONVERTER_T0A91AD27C69DB74D2C09B17006EFE2F873566F6E_H
#define UINT64CONVERTER_T0A91AD27C69DB74D2C09B17006EFE2F873566F6E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.UInt64Converter
struct  UInt64Converter_t0A91AD27C69DB74D2C09B17006EFE2F873566F6E  : public BaseNumberConverter_t6AF36A2503E7BABF7FB9A8EC05DF8B828491AC63
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT64CONVERTER_T0A91AD27C69DB74D2C09B17006EFE2F873566F6E_H
#ifndef CHANNELBINDING_T008B670203CDC7E4926CE95D32EB19E30AFE5E2C_H
#define CHANNELBINDING_T008B670203CDC7E4926CE95D32EB19E30AFE5E2C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Authentication.ExtendedProtection.ChannelBinding
struct  ChannelBinding_t008B670203CDC7E4926CE95D32EB19E30AFE5E2C  : public SafeHandleZeroOrMinusOneIsInvalid_t779A965C82098677DF1ED10A134DBCDEC8AACB8E
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHANNELBINDING_T008B670203CDC7E4926CE95D32EB19E30AFE5E2C_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2700 = { sizeof (ReadOnlyAttribute_t5805F62303A0DEDAE657066BA289E3249FA6A35B), -1, sizeof(ReadOnlyAttribute_t5805F62303A0DEDAE657066BA289E3249FA6A35B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2700[4] = 
{
	ReadOnlyAttribute_t5805F62303A0DEDAE657066BA289E3249FA6A35B::get_offset_of_isReadOnly_0(),
	ReadOnlyAttribute_t5805F62303A0DEDAE657066BA289E3249FA6A35B_StaticFields::get_offset_of_Yes_1(),
	ReadOnlyAttribute_t5805F62303A0DEDAE657066BA289E3249FA6A35B_StaticFields::get_offset_of_No_2(),
	ReadOnlyAttribute_t5805F62303A0DEDAE657066BA289E3249FA6A35B_StaticFields::get_offset_of_Default_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2701 = { sizeof (RecommendedAsConfigurableAttribute_t193A3D07F4735EB535DD6960352C6C9A38E12B55), -1, sizeof(RecommendedAsConfigurableAttribute_t193A3D07F4735EB535DD6960352C6C9A38E12B55_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2701[4] = 
{
	RecommendedAsConfigurableAttribute_t193A3D07F4735EB535DD6960352C6C9A38E12B55::get_offset_of_recommendedAsConfigurable_0(),
	RecommendedAsConfigurableAttribute_t193A3D07F4735EB535DD6960352C6C9A38E12B55_StaticFields::get_offset_of_No_1(),
	RecommendedAsConfigurableAttribute_t193A3D07F4735EB535DD6960352C6C9A38E12B55_StaticFields::get_offset_of_Yes_2(),
	RecommendedAsConfigurableAttribute_t193A3D07F4735EB535DD6960352C6C9A38E12B55_StaticFields::get_offset_of_Default_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2702 = { sizeof (ReferenceConverter_t5080472EE999A1F00721E6C5C97013762C85C7E4), -1, sizeof(ReferenceConverter_t5080472EE999A1F00721E6C5C97013762C85C7E4_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2702[2] = 
{
	ReferenceConverter_t5080472EE999A1F00721E6C5C97013762C85C7E4_StaticFields::get_offset_of_none_2(),
	ReferenceConverter_t5080472EE999A1F00721E6C5C97013762C85C7E4::get_offset_of_type_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2703 = { sizeof (ReferenceComparer_t3DEA0E67FDC8BA34C167CC4401A1FCC3CFE0B33A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2703[1] = 
{
	ReferenceComparer_t3DEA0E67FDC8BA34C167CC4401A1FCC3CFE0B33A::get_offset_of_converter_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2704 = { sizeof (ReflectEventDescriptor_t36C4780808918CFDD00CE2A7E6368FAEFD4DD360), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2704[6] = 
{
	ReflectEventDescriptor_t36C4780808918CFDD00CE2A7E6368FAEFD4DD360::get_offset_of_type_12(),
	ReflectEventDescriptor_t36C4780808918CFDD00CE2A7E6368FAEFD4DD360::get_offset_of_componentClass_13(),
	ReflectEventDescriptor_t36C4780808918CFDD00CE2A7E6368FAEFD4DD360::get_offset_of_addMethod_14(),
	ReflectEventDescriptor_t36C4780808918CFDD00CE2A7E6368FAEFD4DD360::get_offset_of_removeMethod_15(),
	ReflectEventDescriptor_t36C4780808918CFDD00CE2A7E6368FAEFD4DD360::get_offset_of_realEvent_16(),
	ReflectEventDescriptor_t36C4780808918CFDD00CE2A7E6368FAEFD4DD360::get_offset_of_filledMethods_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2705 = { sizeof (ReflectPropertyDescriptor_t4DC7A4205276D400462B36386EEDEEBA6289C11E), -1, sizeof(ReflectPropertyDescriptor_t4DC7A4205276D400462B36386EEDEEBA6289C11E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2705[27] = 
{
	ReflectPropertyDescriptor_t4DC7A4205276D400462B36386EEDEEBA6289C11E_StaticFields::get_offset_of_argsNone_17(),
	ReflectPropertyDescriptor_t4DC7A4205276D400462B36386EEDEEBA6289C11E_StaticFields::get_offset_of_noValue_18(),
	ReflectPropertyDescriptor_t4DC7A4205276D400462B36386EEDEEBA6289C11E_StaticFields::get_offset_of_PropDescCreateSwitch_19(),
	ReflectPropertyDescriptor_t4DC7A4205276D400462B36386EEDEEBA6289C11E_StaticFields::get_offset_of_PropDescUsageSwitch_20(),
	ReflectPropertyDescriptor_t4DC7A4205276D400462B36386EEDEEBA6289C11E_StaticFields::get_offset_of_BitDefaultValueQueried_21(),
	ReflectPropertyDescriptor_t4DC7A4205276D400462B36386EEDEEBA6289C11E_StaticFields::get_offset_of_BitGetQueried_22(),
	ReflectPropertyDescriptor_t4DC7A4205276D400462B36386EEDEEBA6289C11E_StaticFields::get_offset_of_BitSetQueried_23(),
	ReflectPropertyDescriptor_t4DC7A4205276D400462B36386EEDEEBA6289C11E_StaticFields::get_offset_of_BitShouldSerializeQueried_24(),
	ReflectPropertyDescriptor_t4DC7A4205276D400462B36386EEDEEBA6289C11E_StaticFields::get_offset_of_BitResetQueried_25(),
	ReflectPropertyDescriptor_t4DC7A4205276D400462B36386EEDEEBA6289C11E_StaticFields::get_offset_of_BitChangedQueried_26(),
	ReflectPropertyDescriptor_t4DC7A4205276D400462B36386EEDEEBA6289C11E_StaticFields::get_offset_of_BitIPropChangedQueried_27(),
	ReflectPropertyDescriptor_t4DC7A4205276D400462B36386EEDEEBA6289C11E_StaticFields::get_offset_of_BitReadOnlyChecked_28(),
	ReflectPropertyDescriptor_t4DC7A4205276D400462B36386EEDEEBA6289C11E_StaticFields::get_offset_of_BitAmbientValueQueried_29(),
	ReflectPropertyDescriptor_t4DC7A4205276D400462B36386EEDEEBA6289C11E_StaticFields::get_offset_of_BitSetOnDemand_30(),
	ReflectPropertyDescriptor_t4DC7A4205276D400462B36386EEDEEBA6289C11E::get_offset_of_state_31(),
	ReflectPropertyDescriptor_t4DC7A4205276D400462B36386EEDEEBA6289C11E::get_offset_of_componentClass_32(),
	ReflectPropertyDescriptor_t4DC7A4205276D400462B36386EEDEEBA6289C11E::get_offset_of_type_33(),
	ReflectPropertyDescriptor_t4DC7A4205276D400462B36386EEDEEBA6289C11E::get_offset_of_defaultValue_34(),
	ReflectPropertyDescriptor_t4DC7A4205276D400462B36386EEDEEBA6289C11E::get_offset_of_ambientValue_35(),
	ReflectPropertyDescriptor_t4DC7A4205276D400462B36386EEDEEBA6289C11E::get_offset_of_propInfo_36(),
	ReflectPropertyDescriptor_t4DC7A4205276D400462B36386EEDEEBA6289C11E::get_offset_of_getMethod_37(),
	ReflectPropertyDescriptor_t4DC7A4205276D400462B36386EEDEEBA6289C11E::get_offset_of_setMethod_38(),
	ReflectPropertyDescriptor_t4DC7A4205276D400462B36386EEDEEBA6289C11E::get_offset_of_shouldSerializeMethod_39(),
	ReflectPropertyDescriptor_t4DC7A4205276D400462B36386EEDEEBA6289C11E::get_offset_of_resetMethod_40(),
	ReflectPropertyDescriptor_t4DC7A4205276D400462B36386EEDEEBA6289C11E::get_offset_of_realChangedEvent_41(),
	ReflectPropertyDescriptor_t4DC7A4205276D400462B36386EEDEEBA6289C11E::get_offset_of_realIPropChangedEvent_42(),
	ReflectPropertyDescriptor_t4DC7A4205276D400462B36386EEDEEBA6289C11E::get_offset_of_receiverType_43(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2706 = { sizeof (ReflectTypeDescriptionProvider_t5736798A27F452CE006C4DBBCC78EC7928B79675), -1, sizeof(ReflectTypeDescriptionProvider_t5736798A27F452CE006C4DBBCC78EC7928B79675_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2706[16] = 
{
	ReflectTypeDescriptionProvider_t5736798A27F452CE006C4DBBCC78EC7928B79675::get_offset_of__typeData_2(),
	ReflectTypeDescriptionProvider_t5736798A27F452CE006C4DBBCC78EC7928B79675_StaticFields::get_offset_of__typeConstructor_3(),
	ReflectTypeDescriptionProvider_t5736798A27F452CE006C4DBBCC78EC7928B79675_StaticFields::get_offset_of__editorTables_4(),
	ReflectTypeDescriptionProvider_t5736798A27F452CE006C4DBBCC78EC7928B79675_StaticFields::get_offset_of__intrinsicTypeConverters_5(),
	ReflectTypeDescriptionProvider_t5736798A27F452CE006C4DBBCC78EC7928B79675_StaticFields::get_offset_of__intrinsicReferenceKey_6(),
	ReflectTypeDescriptionProvider_t5736798A27F452CE006C4DBBCC78EC7928B79675_StaticFields::get_offset_of__intrinsicNullableKey_7(),
	ReflectTypeDescriptionProvider_t5736798A27F452CE006C4DBBCC78EC7928B79675_StaticFields::get_offset_of__dictionaryKey_8(),
	ReflectTypeDescriptionProvider_t5736798A27F452CE006C4DBBCC78EC7928B79675_StaticFields::get_offset_of__propertyCache_9(),
	ReflectTypeDescriptionProvider_t5736798A27F452CE006C4DBBCC78EC7928B79675_StaticFields::get_offset_of__eventCache_10(),
	ReflectTypeDescriptionProvider_t5736798A27F452CE006C4DBBCC78EC7928B79675_StaticFields::get_offset_of__attributeCache_11(),
	ReflectTypeDescriptionProvider_t5736798A27F452CE006C4DBBCC78EC7928B79675_StaticFields::get_offset_of__extendedPropertyCache_12(),
	ReflectTypeDescriptionProvider_t5736798A27F452CE006C4DBBCC78EC7928B79675_StaticFields::get_offset_of__extenderProviderKey_13(),
	ReflectTypeDescriptionProvider_t5736798A27F452CE006C4DBBCC78EC7928B79675_StaticFields::get_offset_of__extenderPropertiesKey_14(),
	ReflectTypeDescriptionProvider_t5736798A27F452CE006C4DBBCC78EC7928B79675_StaticFields::get_offset_of__extenderProviderPropertiesKey_15(),
	ReflectTypeDescriptionProvider_t5736798A27F452CE006C4DBBCC78EC7928B79675_StaticFields::get_offset_of__skipInterfaceAttributeList_16(),
	ReflectTypeDescriptionProvider_t5736798A27F452CE006C4DBBCC78EC7928B79675_StaticFields::get_offset_of__internalSyncObject_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2707 = { sizeof (ReflectedTypeData_t71CC6CDF94E72051C2A9A5CA4052C16DF8EE0A96), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2707[8] = 
{
	ReflectedTypeData_t71CC6CDF94E72051C2A9A5CA4052C16DF8EE0A96::get_offset_of__type_0(),
	ReflectedTypeData_t71CC6CDF94E72051C2A9A5CA4052C16DF8EE0A96::get_offset_of__attributes_1(),
	ReflectedTypeData_t71CC6CDF94E72051C2A9A5CA4052C16DF8EE0A96::get_offset_of__events_2(),
	ReflectedTypeData_t71CC6CDF94E72051C2A9A5CA4052C16DF8EE0A96::get_offset_of__properties_3(),
	ReflectedTypeData_t71CC6CDF94E72051C2A9A5CA4052C16DF8EE0A96::get_offset_of__converter_4(),
	ReflectedTypeData_t71CC6CDF94E72051C2A9A5CA4052C16DF8EE0A96::get_offset_of__editors_5(),
	ReflectedTypeData_t71CC6CDF94E72051C2A9A5CA4052C16DF8EE0A96::get_offset_of__editorTypes_6(),
	ReflectedTypeData_t71CC6CDF94E72051C2A9A5CA4052C16DF8EE0A96::get_offset_of__editorCount_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2708 = { sizeof (RefreshEventArgs_tAC29F8AEEF1CB6B4ADD8BC4DD0A66366A4700650), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2708[2] = 
{
	RefreshEventArgs_tAC29F8AEEF1CB6B4ADD8BC4DD0A66366A4700650::get_offset_of_componentChanged_1(),
	RefreshEventArgs_tAC29F8AEEF1CB6B4ADD8BC4DD0A66366A4700650::get_offset_of_typeChanged_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2709 = { sizeof (RefreshEventHandler_tE1B96D1B5C9580F60558220F9DEBA92333849F27), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2710 = { sizeof (RunInstallerAttribute_t40A160B1D7133CC1797AD183B581E785D6F99CB3), -1, sizeof(RunInstallerAttribute_t40A160B1D7133CC1797AD183B581E785D6F99CB3_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2710[4] = 
{
	RunInstallerAttribute_t40A160B1D7133CC1797AD183B581E785D6F99CB3::get_offset_of_runInstaller_0(),
	RunInstallerAttribute_t40A160B1D7133CC1797AD183B581E785D6F99CB3_StaticFields::get_offset_of_Yes_1(),
	RunInstallerAttribute_t40A160B1D7133CC1797AD183B581E785D6F99CB3_StaticFields::get_offset_of_No_2(),
	RunInstallerAttribute_t40A160B1D7133CC1797AD183B581E785D6F99CB3_StaticFields::get_offset_of_Default_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2711 = { sizeof (RunWorkerCompletedEventArgs_tFAFA9B2ED339BE1F33AF8845D46A251EB5C6950D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2711[1] = 
{
	RunWorkerCompletedEventArgs_tFAFA9B2ED339BE1F33AF8845D46A251EB5C6950D::get_offset_of_result_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2712 = { sizeof (RunWorkerCompletedEventHandler_t8E766853412F70C3B8826718D59377F7F464BCAB), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2713 = { sizeof (SByteConverter_t99F8032C93CF0F34D8C8BB8AA744D39E794AA4B7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2714 = { sizeof (SettingsBindableAttribute_t85FF311AAFAED1C12B3773CB10D8D29DAB791664), -1, sizeof(SettingsBindableAttribute_t85FF311AAFAED1C12B3773CB10D8D29DAB791664_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2714[3] = 
{
	SettingsBindableAttribute_t85FF311AAFAED1C12B3773CB10D8D29DAB791664_StaticFields::get_offset_of_Yes_0(),
	SettingsBindableAttribute_t85FF311AAFAED1C12B3773CB10D8D29DAB791664_StaticFields::get_offset_of_No_1(),
	SettingsBindableAttribute_t85FF311AAFAED1C12B3773CB10D8D29DAB791664::get_offset_of__bindable_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2715 = { sizeof (SingleConverter_t86A24FBD46D753B99344470E9566584F15538902), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2716 = { sizeof (StringConverter_t054FA0796F4C8E951208AFA052D99BCB8E68BED7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2717 = { sizeof (SyntaxCheck_tDC1D921DE89C089B7EF818689C16587F234ABD79), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2718 = { sizeof (TimeSpanConverter_t4025A0861C52420BC73D837729E5EFA6ECDE07C7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2719 = { sizeof (ToolboxItemFilterAttribute_tB15560039BF68E3DC316F8718947E3E7A54C6910), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2719[3] = 
{
	ToolboxItemFilterAttribute_tB15560039BF68E3DC316F8718947E3E7A54C6910::get_offset_of_filterType_0(),
	ToolboxItemFilterAttribute_tB15560039BF68E3DC316F8718947E3E7A54C6910::get_offset_of_filterString_1(),
	ToolboxItemFilterAttribute_tB15560039BF68E3DC316F8718947E3E7A54C6910::get_offset_of_typeId_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2720 = { sizeof (ToolboxItemFilterType_t55CD1A1F306F891450DFC2290FE851E94CC3B8C5)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2720[5] = 
{
	ToolboxItemFilterType_t55CD1A1F306F891450DFC2290FE851E94CC3B8C5::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2721 = { sizeof (TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB), -1, sizeof(TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2721[2] = 
{
	0,
	TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB_StaticFields::get_offset_of_useCompatibleTypeConversion_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2722 = { sizeof (SimplePropertyDescriptor_tCF596E6470E66A92F1E91E28AB918097701E7CEA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2722[2] = 
{
	SimplePropertyDescriptor_tCF596E6470E66A92F1E91E28AB918097701E7CEA::get_offset_of_componentType_17(),
	SimplePropertyDescriptor_tCF596E6470E66A92F1E91E28AB918097701E7CEA::get_offset_of_propertyType_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2723 = { sizeof (StandardValuesCollection_t929677712574EF02F5C4CF4C38E070841C20BDA3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2723[2] = 
{
	StandardValuesCollection_t929677712574EF02F5C4CF4C38E070841C20BDA3::get_offset_of_values_0(),
	StandardValuesCollection_t929677712574EF02F5C4CF4C38E070841C20BDA3::get_offset_of_valueArray_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2724 = { sizeof (TypeConverterAttribute_tA0B22E1BE9471741D2CD2A078A2C9A5FF882C2F8), -1, sizeof(TypeConverterAttribute_tA0B22E1BE9471741D2CD2A078A2C9A5FF882C2F8_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2724[2] = 
{
	TypeConverterAttribute_tA0B22E1BE9471741D2CD2A078A2C9A5FF882C2F8::get_offset_of_typeName_0(),
	TypeConverterAttribute_tA0B22E1BE9471741D2CD2A078A2C9A5FF882C2F8_StaticFields::get_offset_of_Default_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2725 = { sizeof (TypeDescriptionProvider_tE390829A953C44525366CA2A733E92642B97B591), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2725[2] = 
{
	TypeDescriptionProvider_tE390829A953C44525366CA2A733E92642B97B591::get_offset_of__parent_0(),
	TypeDescriptionProvider_tE390829A953C44525366CA2A733E92642B97B591::get_offset_of__emptyDescriptor_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2726 = { sizeof (EmptyCustomTypeDescriptor_tDDCC5072613D7D0405BCCF3D49F3EF7B4E1E7DB8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2727 = { sizeof (TypeDescriptionProviderAttribute_t387A574F4C3912AEA8435C46CC377BDA93F29034), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2727[1] = 
{
	TypeDescriptionProviderAttribute_t387A574F4C3912AEA8435C46CC377BDA93F29034::get_offset_of__typeName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2728 = { sizeof (TypeDescriptor_tEA9B846104F636343B5CBF281E8E7BE6349843AF), -1, sizeof(TypeDescriptor_tEA9B846104F636343B5CBF281E8E7BE6349843AF_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2728[16] = 
{
	TypeDescriptor_tEA9B846104F636343B5CBF281E8E7BE6349843AF_StaticFields::get_offset_of__providerTable_0(),
	TypeDescriptor_tEA9B846104F636343B5CBF281E8E7BE6349843AF_StaticFields::get_offset_of__providerTypeTable_1(),
	TypeDescriptor_tEA9B846104F636343B5CBF281E8E7BE6349843AF_StaticFields::get_offset_of__defaultProviders_2(),
	TypeDescriptor_tEA9B846104F636343B5CBF281E8E7BE6349843AF_StaticFields::get_offset_of__associationTable_3(),
	TypeDescriptor_tEA9B846104F636343B5CBF281E8E7BE6349843AF_StaticFields::get_offset_of__metadataVersion_4(),
	TypeDescriptor_tEA9B846104F636343B5CBF281E8E7BE6349843AF_StaticFields::get_offset_of__collisionIndex_5(),
	TypeDescriptor_tEA9B846104F636343B5CBF281E8E7BE6349843AF_StaticFields::get_offset_of_TraceDescriptor_6(),
	0,
	0,
	0,
	TypeDescriptor_tEA9B846104F636343B5CBF281E8E7BE6349843AF_StaticFields::get_offset_of__pipelineInitializeKeys_10(),
	TypeDescriptor_tEA9B846104F636343B5CBF281E8E7BE6349843AF_StaticFields::get_offset_of__pipelineMergeKeys_11(),
	TypeDescriptor_tEA9B846104F636343B5CBF281E8E7BE6349843AF_StaticFields::get_offset_of__pipelineFilterKeys_12(),
	TypeDescriptor_tEA9B846104F636343B5CBF281E8E7BE6349843AF_StaticFields::get_offset_of__pipelineAttributeFilterKeys_13(),
	TypeDescriptor_tEA9B846104F636343B5CBF281E8E7BE6349843AF_StaticFields::get_offset_of__internalSyncObject_14(),
	TypeDescriptor_tEA9B846104F636343B5CBF281E8E7BE6349843AF_StaticFields::get_offset_of_Refreshed_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2729 = { sizeof (AttributeProvider_t67BCE87062C434FBC0E369F34CCB196661DABFDF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2729[1] = 
{
	AttributeProvider_t67BCE87062C434FBC0E369F34CCB196661DABFDF::get_offset_of__attrs_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2730 = { sizeof (AttributeTypeDescriptor_t4D6F2E9913F8B001ADF799DA77361BAEF574A6DE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2730[1] = 
{
	AttributeTypeDescriptor_t4D6F2E9913F8B001ADF799DA77361BAEF574A6DE::get_offset_of__attributeArray_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2731 = { sizeof (ComNativeDescriptionProvider_tC63084A6A9080BAA8FB16E20E2847C18E5E3907D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2731[1] = 
{
	ComNativeDescriptionProvider_tC63084A6A9080BAA8FB16E20E2847C18E5E3907D::get_offset_of__handler_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2732 = { sizeof (ComNativeTypeDescriptor_t44C4B23471F1F0C1174FD5E9EB670AD77A974A9D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2732[2] = 
{
	ComNativeTypeDescriptor_t44C4B23471F1F0C1174FD5E9EB670AD77A974A9D::get_offset_of__handler_0(),
	ComNativeTypeDescriptor_t44C4B23471F1F0C1174FD5E9EB670AD77A974A9D::get_offset_of__instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2733 = { sizeof (AttributeFilterCacheItem_t8E137227F1585B34CFC9BEC30C7D155203FA0D01), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2733[2] = 
{
	AttributeFilterCacheItem_t8E137227F1585B34CFC9BEC30C7D155203FA0D01::get_offset_of__filter_0(),
	AttributeFilterCacheItem_t8E137227F1585B34CFC9BEC30C7D155203FA0D01::get_offset_of_FilteredMembers_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2734 = { sizeof (FilterCacheItem_tDA029760025F0503AD352DE639152D69CA138DA4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2734[2] = 
{
	FilterCacheItem_tDA029760025F0503AD352DE639152D69CA138DA4::get_offset_of__filterService_0(),
	FilterCacheItem_tDA029760025F0503AD352DE639152D69CA138DA4::get_offset_of_FilteredMembers_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2735 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2736 = { sizeof (MemberDescriptorComparer_t08208596E93DE2FB4472AB31B0316F98B288506D), -1, sizeof(MemberDescriptorComparer_t08208596E93DE2FB4472AB31B0316F98B288506D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2736[1] = 
{
	MemberDescriptorComparer_t08208596E93DE2FB4472AB31B0316F98B288506D_StaticFields::get_offset_of_Instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2737 = { sizeof (MergedTypeDescriptor_t9395D9B4F23C94B7997A9ED698CBEF7995EB162B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2737[2] = 
{
	MergedTypeDescriptor_t9395D9B4F23C94B7997A9ED698CBEF7995EB162B::get_offset_of__primary_0(),
	MergedTypeDescriptor_t9395D9B4F23C94B7997A9ED698CBEF7995EB162B::get_offset_of__secondary_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2738 = { sizeof (TypeDescriptionNode_t8C5D3A02DB612FFE1DE12251E49EC265DE6B81E1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2738[2] = 
{
	TypeDescriptionNode_t8C5D3A02DB612FFE1DE12251E49EC265DE6B81E1::get_offset_of_Next_2(),
	TypeDescriptionNode_t8C5D3A02DB612FFE1DE12251E49EC265DE6B81E1::get_offset_of_Provider_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2739 = { sizeof (DefaultExtendedTypeDescriptor_t89890252F6A685D141ECBB0C2878C6E913883ECE)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2739[2] = 
{
	DefaultExtendedTypeDescriptor_t89890252F6A685D141ECBB0C2878C6E913883ECE::get_offset_of__node_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DefaultExtendedTypeDescriptor_t89890252F6A685D141ECBB0C2878C6E913883ECE::get_offset_of__instance_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2740 = { sizeof (DefaultTypeDescriptor_t45C7CF272F02817B0F1C69470B4E786E746996F4)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2740[3] = 
{
	DefaultTypeDescriptor_t45C7CF272F02817B0F1C69470B4E786E746996F4::get_offset_of__node_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DefaultTypeDescriptor_t45C7CF272F02817B0F1C69470B4E786E746996F4::get_offset_of__objectType_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DefaultTypeDescriptor_t45C7CF272F02817B0F1C69470B4E786E746996F4::get_offset_of__instance_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2741 = { sizeof (TypeDescriptorComObject_t531EE8D60EE83A09B6539D079E837E68DC289B76), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2742 = { sizeof (TypeDescriptorInterface_t872F528EF86C66103396D4D796A932A0FF88C4D7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2743 = { sizeof (TypeListConverter_tD02D4A8E7829B15240C8A2C2F54D5672513B05C0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2743[2] = 
{
	TypeListConverter_tD02D4A8E7829B15240C8A2C2F54D5672513B05C0::get_offset_of_types_2(),
	TypeListConverter_tD02D4A8E7829B15240C8A2C2F54D5672513B05C0::get_offset_of_values_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2744 = { sizeof (UInt16Converter_t4BC0EAF1F93E418E020D9F6636F89276782C6803), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2745 = { sizeof (UInt32Converter_t13F2DD92CDD0CD6A0EFD9D0F45AF88B3893DCA65), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2746 = { sizeof (UInt64Converter_t0A91AD27C69DB74D2C09B17006EFE2F873566F6E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2747 = { sizeof (WarningException_t8CDDEA53879B798257932E94B4880AC9B9AA8CD9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2747[2] = 
{
	WarningException_t8CDDEA53879B798257932E94B4880AC9B9AA8CD9::get_offset_of_helpUrl_17(),
	WarningException_t8CDDEA53879B798257932E94B4880AC9B9AA8CD9::get_offset_of_helpTopic_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2748 = { sizeof (Win32Exception_tB05BE97AB4CADD54DF96C0109689F0ECA7517668), -1, sizeof(Win32Exception_tB05BE97AB4CADD54DF96C0109689F0ECA7517668_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2748[3] = 
{
	Win32Exception_tB05BE97AB4CADD54DF96C0109689F0ECA7517668::get_offset_of_nativeErrorCode_17(),
	Win32Exception_tB05BE97AB4CADD54DF96C0109689F0ECA7517668_StaticFields::get_offset_of_s_ErrorMessagesInitialized_18(),
	Win32Exception_tB05BE97AB4CADD54DF96C0109689F0ECA7517668_StaticFields::get_offset_of_s_ErrorMessage_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2749 = { sizeof (BaseNumberConverter_t6AF36A2503E7BABF7FB9A8EC05DF8B828491AC63), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2750 = { sizeof (InheritanceAttribute_tBB0E10C86B9ECC652035969169FC556F1C6C8AC4), -1, sizeof(InheritanceAttribute_tBB0E10C86B9ECC652035969169FC556F1C6C8AC4_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2750[5] = 
{
	InheritanceAttribute_tBB0E10C86B9ECC652035969169FC556F1C6C8AC4::get_offset_of_inheritanceLevel_0(),
	InheritanceAttribute_tBB0E10C86B9ECC652035969169FC556F1C6C8AC4_StaticFields::get_offset_of_Inherited_1(),
	InheritanceAttribute_tBB0E10C86B9ECC652035969169FC556F1C6C8AC4_StaticFields::get_offset_of_InheritedReadOnly_2(),
	InheritanceAttribute_tBB0E10C86B9ECC652035969169FC556F1C6C8AC4_StaticFields::get_offset_of_NotInherited_3(),
	InheritanceAttribute_tBB0E10C86B9ECC652035969169FC556F1C6C8AC4_StaticFields::get_offset_of_Default_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2751 = { sizeof (InheritanceLevel_t7A3CA26A46C1759F0D0D9BD78A7B298490C1DAD6)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2751[4] = 
{
	InheritanceLevel_t7A3CA26A46C1759F0D0D9BD78A7B298490C1DAD6::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2752 = { sizeof (NotifyParentPropertyAttribute_tCC257721622A29BC31EF1BFDC645E020EC7F4D68), -1, sizeof(NotifyParentPropertyAttribute_tCC257721622A29BC31EF1BFDC645E020EC7F4D68_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2752[4] = 
{
	NotifyParentPropertyAttribute_tCC257721622A29BC31EF1BFDC645E020EC7F4D68_StaticFields::get_offset_of_Yes_0(),
	NotifyParentPropertyAttribute_tCC257721622A29BC31EF1BFDC645E020EC7F4D68_StaticFields::get_offset_of_No_1(),
	NotifyParentPropertyAttribute_tCC257721622A29BC31EF1BFDC645E020EC7F4D68_StaticFields::get_offset_of_Default_2(),
	NotifyParentPropertyAttribute_tCC257721622A29BC31EF1BFDC645E020EC7F4D68::get_offset_of_notifyParent_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2753 = { sizeof (ParenthesizePropertyNameAttribute_t8C7EB4B9459246DAF7283E2D585D1D59F9886114), -1, sizeof(ParenthesizePropertyNameAttribute_t8C7EB4B9459246DAF7283E2D585D1D59F9886114_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2753[2] = 
{
	ParenthesizePropertyNameAttribute_t8C7EB4B9459246DAF7283E2D585D1D59F9886114_StaticFields::get_offset_of_Default_0(),
	ParenthesizePropertyNameAttribute_t8C7EB4B9459246DAF7283E2D585D1D59F9886114::get_offset_of_needParenthesis_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2754 = { sizeof (PropertyTabAttribute_tCC9C7766C8A49D5FEDB5C51D2391A9301F3CCBE8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2754[3] = 
{
	PropertyTabAttribute_tCC9C7766C8A49D5FEDB5C51D2391A9301F3CCBE8::get_offset_of_tabScopes_0(),
	PropertyTabAttribute_tCC9C7766C8A49D5FEDB5C51D2391A9301F3CCBE8::get_offset_of_tabClasses_1(),
	PropertyTabAttribute_tCC9C7766C8A49D5FEDB5C51D2391A9301F3CCBE8::get_offset_of_tabClassNames_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2755 = { sizeof (PropertyTabScope_t7A2ECEC4F4367AC32D472E9B17F35F5F7C0D1836)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2755[5] = 
{
	PropertyTabScope_t7A2ECEC4F4367AC32D472E9B17F35F5F7C0D1836::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2756 = { sizeof (RefreshProperties_t1DCC9EF1AA18F3609AA7B68B33D19F22A8473788)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2756[4] = 
{
	RefreshProperties_t1DCC9EF1AA18F3609AA7B68B33D19F22A8473788::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2757 = { sizeof (RefreshPropertiesAttribute_tC6E685EE61C7BFE437F9ABAE7AB9B5B83FEB3DC1), -1, sizeof(RefreshPropertiesAttribute_tC6E685EE61C7BFE437F9ABAE7AB9B5B83FEB3DC1_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2757[4] = 
{
	RefreshPropertiesAttribute_tC6E685EE61C7BFE437F9ABAE7AB9B5B83FEB3DC1_StaticFields::get_offset_of_All_0(),
	RefreshPropertiesAttribute_tC6E685EE61C7BFE437F9ABAE7AB9B5B83FEB3DC1_StaticFields::get_offset_of_Repaint_1(),
	RefreshPropertiesAttribute_tC6E685EE61C7BFE437F9ABAE7AB9B5B83FEB3DC1_StaticFields::get_offset_of_Default_2(),
	RefreshPropertiesAttribute_tC6E685EE61C7BFE437F9ABAE7AB9B5B83FEB3DC1::get_offset_of_refresh_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2758 = { sizeof (ToolboxItemAttribute_t06EC671268D222B3B4F3BE1DF5874E9B3166269C), -1, sizeof(ToolboxItemAttribute_t06EC671268D222B3B4F3BE1DF5874E9B3166269C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2758[4] = 
{
	ToolboxItemAttribute_t06EC671268D222B3B4F3BE1DF5874E9B3166269C::get_offset_of_toolboxItemType_0(),
	ToolboxItemAttribute_t06EC671268D222B3B4F3BE1DF5874E9B3166269C::get_offset_of_toolboxItemTypeName_1(),
	ToolboxItemAttribute_t06EC671268D222B3B4F3BE1DF5874E9B3166269C_StaticFields::get_offset_of_Default_2(),
	ToolboxItemAttribute_t06EC671268D222B3B4F3BE1DF5874E9B3166269C_StaticFields::get_offset_of_None_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2759 = { sizeof (WeakHashtable_t230AE0A1A5D5EE8C97A2280405572D83D757B2CF), -1, sizeof(WeakHashtable_t230AE0A1A5D5EE8C97A2280405572D83D757B2CF_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2759[3] = 
{
	WeakHashtable_t230AE0A1A5D5EE8C97A2280405572D83D757B2CF_StaticFields::get_offset_of__comparer_21(),
	WeakHashtable_t230AE0A1A5D5EE8C97A2280405572D83D757B2CF::get_offset_of__lastGlobalMem_22(),
	WeakHashtable_t230AE0A1A5D5EE8C97A2280405572D83D757B2CF::get_offset_of__lastHashCount_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2760 = { sizeof (WeakKeyComparer_tFF7B45BE010723C01B3F3205FEBCC915543996B3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2761 = { sizeof (EqualityWeakReference_t1A132A1151E232922D8016D93B3C6484D7792B54), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2761[1] = 
{
	EqualityWeakReference_t1A132A1151E232922D8016D93B3C6484D7792B54::get_offset_of__hashCode_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2762 = { sizeof (SRCategoryAttribute_t1C136A51E836D96E811610B3BB0ADFE784A7CF65), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2763 = { sizeof (CheckoutException_tF657C421DAEECE88F94372CEA043DE1D9CCAC9C5), -1, sizeof(CheckoutException_tF657C421DAEECE88F94372CEA043DE1D9CCAC9C5_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2763[1] = 
{
	CheckoutException_tF657C421DAEECE88F94372CEA043DE1D9CCAC9C5_StaticFields::get_offset_of_Canceled_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2764 = { sizeof (DesigntimeLicenseContext_tFBEF4E72A1E0E7F03A01D35ADFEE86A23F848AA1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2764[1] = 
{
	DesigntimeLicenseContext_tFBEF4E72A1E0E7F03A01D35ADFEE86A23F848AA1::get_offset_of_savedLicenseKeys_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2765 = { sizeof (RuntimeLicenseContext_t1367392EED5E28C90B5128BF01CA0B846120C641), -1, sizeof(RuntimeLicenseContext_t1367392EED5E28C90B5128BF01CA0B846120C641_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2765[2] = 
{
	RuntimeLicenseContext_t1367392EED5E28C90B5128BF01CA0B846120C641_StaticFields::get_offset_of_RuntimeLicenseContextSwitch_0(),
	RuntimeLicenseContext_t1367392EED5E28C90B5128BF01CA0B846120C641::get_offset_of_savedLicenseKeys_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2766 = { sizeof (DesigntimeLicenseContextSerializer_t613DD72FEBA8711EB6154D704B01A70E11201B5D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2767 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2768 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2769 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2770 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2771 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2772 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2773 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2774 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2775 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2776 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2777 = { sizeof (DesignerSerializerAttribute_t2EA1CB464E794E9A45402A813B5702E3ACE609DE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2777[3] = 
{
	DesignerSerializerAttribute_t2EA1CB464E794E9A45402A813B5702E3ACE609DE::get_offset_of_serializerTypeName_0(),
	DesignerSerializerAttribute_t2EA1CB464E794E9A45402A813B5702E3ACE609DE::get_offset_of_serializerBaseTypeName_1(),
	DesignerSerializerAttribute_t2EA1CB464E794E9A45402A813B5702E3ACE609DE::get_offset_of_typeId_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2778 = { sizeof (InstanceDescriptor_t964F3DAD1E093CF941D315A157F6DE4FF6E52EF6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2778[3] = 
{
	InstanceDescriptor_t964F3DAD1E093CF941D315A157F6DE4FF6E52EF6::get_offset_of_member_0(),
	InstanceDescriptor_t964F3DAD1E093CF941D315A157F6DE4FF6E52EF6::get_offset_of_arguments_1(),
	InstanceDescriptor_t964F3DAD1E093CF941D315A157F6DE4FF6E52EF6::get_offset_of_isComplete_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2779 = { sizeof (RootDesignerSerializerAttribute_tD5A87C7E5CB002D859780E1BEF96D7E1214CC0AA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2779[4] = 
{
	RootDesignerSerializerAttribute_tD5A87C7E5CB002D859780E1BEF96D7E1214CC0AA::get_offset_of_reloadable_0(),
	RootDesignerSerializerAttribute_tD5A87C7E5CB002D859780E1BEF96D7E1214CC0AA::get_offset_of_serializerTypeName_1(),
	RootDesignerSerializerAttribute_tD5A87C7E5CB002D859780E1BEF96D7E1214CC0AA::get_offset_of_serializerBaseTypeName_2(),
	RootDesignerSerializerAttribute_tD5A87C7E5CB002D859780E1BEF96D7E1214CC0AA::get_offset_of_typeId_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2780 = { sizeof (DefaultParameterValueAttribute_t289E7B39A1085788DCB9930E09487C28F5F9B040), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2780[1] = 
{
	DefaultParameterValueAttribute_t289E7B39A1085788DCB9930E09487C28F5F9B040::get_offset_of_value_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2781 = { sizeof (HandleCollector_tF2D4643449777DCA95FF738A225CC0BEB284D232), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2781[8] = 
{
	0,
	HandleCollector_tF2D4643449777DCA95FF738A225CC0BEB284D232::get_offset_of_name_1(),
	HandleCollector_tF2D4643449777DCA95FF738A225CC0BEB284D232::get_offset_of_initialThreshold_2(),
	HandleCollector_tF2D4643449777DCA95FF738A225CC0BEB284D232::get_offset_of_maximumThreshold_3(),
	HandleCollector_tF2D4643449777DCA95FF738A225CC0BEB284D232::get_offset_of_threshold_4(),
	HandleCollector_tF2D4643449777DCA95FF738A225CC0BEB284D232::get_offset_of_handleCount_5(),
	HandleCollector_tF2D4643449777DCA95FF738A225CC0BEB284D232::get_offset_of_gc_counts_6(),
	HandleCollector_tF2D4643449777DCA95FF738A225CC0BEB284D232::get_offset_of_gc_gen_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2782 = { sizeof (FriendAccessAllowedAttribute_t9EADAA524AB7E0A3138B0AE43226C19809A90CA3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2783 = { sizeof (AuthenticationException_tE24BF2E2AD351F0C9586A59191F01918659A7516), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2784 = { sizeof (SslProtocols_tDD37F8F06AD19BDAF27AEA484EC06820FE3107AE)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2784[8] = 
{
	SslProtocols_tDD37F8F06AD19BDAF27AEA484EC06820FE3107AE::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2785 = { sizeof (ServiceNameCollection_t3C60F59BA95A4A706BB70439AE03C07D09796C2C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2786 = { sizeof (TokenBinding_tC9B02488C1CAC225092E43AD9D09F7213588313B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2787 = { sizeof (ChannelBinding_t008B670203CDC7E4926CE95D32EB19E30AFE5E2C), sizeof(void*), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2788 = { sizeof (ChannelBindingKind_t4BB80CCD48C24F639DA5A09B8DA799EA101ACA54)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2788[4] = 
{
	ChannelBindingKind_t4BB80CCD48C24F639DA5A09B8DA799EA101ACA54::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2789 = { sizeof (ExtendedProtectionPolicy_t5DB5E76D5F3E2BF3CAE465DF85198A91DF30A8BC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2790 = { sizeof (ExtendedProtectionPolicyTypeConverter_t8B9718D9F32564C56F139A2E86F268BFEF7A9804), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2791 = { sizeof (PolicyEnforcement_t8E75D3581050E49BD8C764B18CE4937D5B024E19)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2791[4] = 
{
	PolicyEnforcement_t8E75D3581050E49BD8C764B18CE4937D5B024E19::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2792 = { sizeof (OidGroup_t9A99D3013C1B94CB060656F30C39E893E75FAD6B)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2792[12] = 
{
	OidGroup_t9A99D3013C1B94CB060656F30C39E893E75FAD6B::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2793 = { sizeof (Oid_tC00A10270EAF16BBF0F2619B9AEC883E0CFE6137), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2793[3] = 
{
	Oid_tC00A10270EAF16BBF0F2619B9AEC883E0CFE6137::get_offset_of_m_value_0(),
	Oid_tC00A10270EAF16BBF0F2619B9AEC883E0CFE6137::get_offset_of_m_friendlyName_1(),
	Oid_tC00A10270EAF16BBF0F2619B9AEC883E0CFE6137::get_offset_of_m_group_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2794 = { sizeof (OidCollection_tEB423F1150E53DCF513BF5A699F911586A31B94E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2794[1] = 
{
	OidCollection_tEB423F1150E53DCF513BF5A699F911586A31B94E::get_offset_of_m_list_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2795 = { sizeof (OidEnumerator_tC2DB288576C575B69F7934274DDD8A5868CEF97C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2795[2] = 
{
	OidEnumerator_tC2DB288576C575B69F7934274DDD8A5868CEF97C::get_offset_of_m_oids_0(),
	OidEnumerator_tC2DB288576C575B69F7934274DDD8A5868CEF97C::get_offset_of_m_current_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2796 = { sizeof (CAPI_tEA68010AC3470FFEBC91FC9D3C13E7D7064C3267), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2797 = { sizeof (AsnDecodeStatus_t83139F58FFE08CE7DBCB990C9F30D2F2CA5BC0BB)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2797[7] = 
{
	AsnDecodeStatus_t83139F58FFE08CE7DBCB990C9F30D2F2CA5BC0BB::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2798 = { sizeof (AsnEncodedData_t7D5EF5337DCAF507CAD7D750552C943F037A9D65), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2798[2] = 
{
	AsnEncodedData_t7D5EF5337DCAF507CAD7D750552C943F037A9D65::get_offset_of__oid_0(),
	AsnEncodedData_t7D5EF5337DCAF507CAD7D750552C943F037A9D65::get_offset_of__raw_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2799 = { sizeof (OpenFlags_t498C8F28F409EF03967CA8451E8F7D191EE84596)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2799[6] = 
{
	OpenFlags_t498C8F28F409EF03967CA8451E8F7D191EE84596::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
