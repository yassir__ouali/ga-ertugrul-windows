﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// GameSparks.Core.GSData
struct GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937;
// GameSparks.Core.GSInstance
struct GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774;
// GameSparks.Core.GSObject
struct GSObject_t05160F6677D08913C6CD9AB7C7DD9454D3B12E88;
// GameSparks.Core.GSRequest
struct GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7;
// GameSparks.Core.IGSPlatform
struct IGSPlatform_tE0AFAF9FEBCD3E24A01BD5210D69F4AC6DAE11D1;
// GameSparks.IGameSparksTimer
struct IGameSparksTimer_t2CB6A982FBBD5301B3B2AB329F0E356FD23E17C3;
// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.Action`1<Facebook.Unity.Settings.FacebookSettings/OnChangeCallback>
struct Action_1_t605040A52441734E3EA5810595C95A0D0568C238;
// System.Action`1<GameSparks.Api.Messages.AchievementEarnedMessage>
struct Action_1_tB665E708BEF44CD901EEA6128DCCD2BE761E3DD2;
// System.Action`1<GameSparks.Api.Messages.ChallengeAcceptedMessage>
struct Action_1_t56A5FB45DDBD6FC649B2F2EA894463595DA6F019;
// System.Action`1<GameSparks.Api.Messages.ChallengeChangedMessage>
struct Action_1_tC6C5B047DA5A06F8E60472987F317DC620A48C0F;
// System.Action`1<GameSparks.Api.Messages.ChallengeChatMessage>
struct Action_1_t3750D516B51391094A17D892AE18EB7DFCC8965F;
// System.Action`1<GameSparks.Api.Messages.ChallengeDeclinedMessage>
struct Action_1_tBA5878305F9CAA3DEE372DF8D6D748A0BD358E32;
// System.Action`1<GameSparks.Api.Messages.ChallengeDrawnMessage>
struct Action_1_tD683FB0D9C0B32681C7C27B1A17D934BC8ECDA5C;
// System.Action`1<GameSparks.Api.Messages.ChallengeExpiredMessage>
struct Action_1_tDCB3227E2ADD4C5A60112332532BFF3AF7C556D3;
// System.Action`1<GameSparks.Api.Messages.ChallengeIssuedMessage>
struct Action_1_tD80DD77C0554659B314930203B7EAFFD278A65F8;
// System.Action`1<GameSparks.Api.Messages.ChallengeJoinedMessage>
struct Action_1_tF7EDF47FDF943B6491A3DA14627439B0A7AD07CD;
// System.Action`1<GameSparks.Api.Messages.ChallengeLapsedMessage>
struct Action_1_t483CD7B702823AA15494D015C38BE0117BD10494;
// System.Action`1<GameSparks.Api.Messages.ChallengeLostMessage>
struct Action_1_tBD6D88A3CE9301306CAE4CEEBD0A1B8D24E34A7A;
// System.Action`1<GameSparks.Api.Messages.ChallengeStartedMessage>
struct Action_1_t8F55990D81FEC641846AC4B851F4C4C1C2049A3E;
// System.Action`1<GameSparks.Api.Messages.ChallengeTurnTakenMessage>
struct Action_1_t77C3D2E822F040B37396DBBFC2A4A2C806DBF0F2;
// System.Action`1<GameSparks.Api.Messages.ChallengeWaitingMessage>
struct Action_1_tA4FD32A8EB2243FB0524AF94B8CCED076FBD77DE;
// System.Action`1<GameSparks.Api.Messages.ChallengeWithdrawnMessage>
struct Action_1_tD793A6FAB97A5522B1A0810C13170E1A66F0B7CB;
// System.Action`1<GameSparks.Api.Messages.ChallengeWonMessage>
struct Action_1_t1E12E5F732106F4C3E98A0579E69F98EB6E4050E;
// System.Action`1<GameSparks.Api.Messages.FriendMessage>
struct Action_1_tF139EC3697AA428A3EEA452E530111DA3009ED33;
// System.Action`1<GameSparks.Api.Messages.GlobalRankChangedMessage>
struct Action_1_t57EAA59039391994FB7EFBF0FD6860F3FCB51B50;
// System.Action`1<GameSparks.Api.Messages.MatchFoundMessage>
struct Action_1_t5BB31FF5B06095D4D0398A948F68BAEF3A7B9EDB;
// System.Action`1<GameSparks.Api.Messages.MatchNotFoundMessage>
struct Action_1_t7BE0FF66FF22E957D97608C62F11B2FB1D4DC6A9;
// System.Action`1<GameSparks.Api.Messages.MatchUpdatedMessage>
struct Action_1_t3408E89713E197ED474CC1377B5AC04C53186A66;
// System.Action`1<GameSparks.Api.Messages.NewHighScoreMessage>
struct Action_1_t4E1CE38FE1E13AF4D183226F72AC6E789D638941;
// System.Action`1<GameSparks.Api.Messages.NewTeamScoreMessage>
struct Action_1_tF0D4E4EB50479AC02BE6F69D9CF12EC482CEA95E;
// System.Action`1<GameSparks.Api.Messages.ScriptMessage>
struct Action_1_tA07939E8D990074A80E1CA26894E930FA9D08CEE;
// System.Action`1<GameSparks.Api.Messages.SessionTerminatedMessage>
struct Action_1_t6C56C09D50C5F187196A7FB4D0413C3E7BFE01D4;
// System.Action`1<GameSparks.Api.Messages.SocialRankChangedMessage>
struct Action_1_t022AE2F55D98E649680E55D233D4A1626C22D986;
// System.Action`1<GameSparks.Api.Messages.TeamChatMessage>
struct Action_1_tE9BEA6F59DC29212B2E7E712EA80E8180C4966DD;
// System.Action`1<GameSparks.Api.Messages.TeamRankChangedMessage>
struct Action_1_tD5531F2B1B9DE563F0C630AEE876964F6C7260B9;
// System.Action`1<GameSparks.Api.Messages.UploadCompleteMessage>
struct Action_1_t58AAB7430B02EABBBF4BDBC069725D1E18DFCAE3;
// System.Action`1<System.Boolean>
struct Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD;
// System.Action`1<System.String>
struct Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.String,GameSparks.Core.GSInstance>
struct Dictionary_2_t67B78321AEFED4FABBEA49DA993A79846D2855A9;
// System.Collections.Generic.IDictionary`2<System.String,System.Func`2<GameSparks.Core.GSData,GameSparks.Api.Messages.GSMessage>>
struct IDictionary_2_tC2BFD55A6AAF5127D427D09CDC473E36B0DAE16D;
// System.Collections.Generic.IDictionary`2<System.String,System.Object>
struct IDictionary_2_t1E5681AFFD86E8189077B1EE929272E2AF245A91;
// System.Collections.Generic.LinkedList`1<GameSparks.Core.GSRequest>
struct LinkedList_1_t78335B40DBFA8F7BEB0B3258FAF2A30F78219F9C;
// System.Collections.Generic.LinkedList`1<System.Action>
struct LinkedList_1_tE7F103DCAF02C8979414D806B0F680082082D253;
// System.Collections.Generic.List`1<Facebook.Unity.Settings.FacebookSettings/OnChangeCallback>
struct List_1_t4761B02A8CB42D5714C127D74C8BD8BCBD280D33;
// System.Collections.Generic.List`1<Facebook.Unity.Settings.FacebookSettings/UrlSchemes>
struct List_1_tEBAA293BF803F2D17E20E8B6FE34EF3792F6FA6B;
// System.Collections.Generic.List`1<GameSparks.Core.GSConnection>
struct List_1_t96ABFF3320DF81A409731B576A781AC5D5C2E721;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226;
// System.Collections.Generic.List`1<System.String>
struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3;
// System.Collections.Generic.List`1<UnityEngine.Color32>
struct List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5;
// System.Collections.Generic.List`1<UnityEngine.RectTransform>
struct List_1_t0CD9761E1DF9817484CF4FB4253C6A626DC2311C;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5;
// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Exception
struct Exception_t;
// System.Func`2<System.String,System.String>
struct Func_2_tD9EBF5FAD46B4F561FE5ABE814DC60E6253B8B96;
// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single>
struct Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.Predicate`1<UnityEngine.Component>
struct Predicate_1_t1A9CE8ADB6E9328794CC409FD5BEAACA86D7D769;
// System.Random
struct Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Version
struct Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.Events.UnityAction`1<UnityEngine.Component>
struct UnityAction_1_tD07424D822F8527D240A320A1866C70C835C7C99;
// UnityEngine.RaycastHit2D[]
struct RaycastHit2DU5BU5D_t5F37B944987342C401FA9A231A75AD2991A66165;
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57;
// UnityEngine.RectOffset
struct RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A;
// UnityEngine.RectTransform
struct RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20;
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t431F4FBD9C59AE097FE33C4354CC6251B01B527D;
// UnityEngine.UI.Graphic
struct Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8;
// UnityEngine.UI.ObjectPool`1<UnityEngine.UI.LayoutRebuilder>
struct ObjectPool_1_tFA4F33849836CDB27432AE22249BB79D68619541;
// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllCallback
struct GetRayIntersectionAllCallback_t68C2581CCF05E868297EBD3F3361274954845095;
// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllNonAllocCallback
struct GetRayIntersectionAllNonAllocCallback_tAD7508D45DB6679B6394983579AD18D967CC2AD4;
// UnityEngine.UI.ReflectionMethodsCache/GetRaycastNonAllocCallback
struct GetRaycastNonAllocCallback_tC13D9767CFF00EAB26E9FCC4BDD505F0721A2B4D;
// UnityEngine.UI.ReflectionMethodsCache/Raycast2DCallback
struct Raycast2DCallback_tE99ABF9ABC3A380677949E8C05A3E477889B82BE;
// UnityEngine.UI.ReflectionMethodsCache/Raycast3DCallback
struct Raycast3DCallback_t83483916473C9710AEDB316A65CBE62C58935C5F;
// UnityEngine.UI.ReflectionMethodsCache/RaycastAllCallback
struct RaycastAllCallback_t751407A44270E02FAA43D0846A58EE6A8C4AE1CE;

struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;



#ifndef U3CMODULEU3E_TD0CA5CB8F838362C11DAED8A9A3159C658C1998B_H
#define U3CMODULEU3E_TD0CA5CB8F838362C11DAED8A9A3159C658C1998B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_tD0CA5CB8F838362C11DAED8A9A3159C658C1998B 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_TD0CA5CB8F838362C11DAED8A9A3159C658C1998B_H
#ifndef U3CMODULEU3E_T48807405FE1B7A134A75505377E34131ABC2EBA2_H
#define U3CMODULEU3E_T48807405FE1B7A134A75505377E34131ABC2EBA2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t48807405FE1B7A134A75505377E34131ABC2EBA2 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T48807405FE1B7A134A75505377E34131ABC2EBA2_H
#ifndef U3CMODULEU3E_T7D11BB4BD4C1ADC1F64CC310633FC1BEE61632FE_H
#define U3CMODULEU3E_T7D11BB4BD4C1ADC1F64CC310633FC1BEE61632FE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t7D11BB4BD4C1ADC1F64CC310633FC1BEE61632FE 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T7D11BB4BD4C1ADC1F64CC310633FC1BEE61632FE_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CU3EC_T40659AD935CCBCC35589D12303DF7D93D8B4ABA8_H
#define U3CU3EC_T40659AD935CCBCC35589D12303DF7D93D8B4ABA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Settings.FacebookSettings_<>c
struct  U3CU3Ec_t40659AD935CCBCC35589D12303DF7D93D8B4ABA8  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t40659AD935CCBCC35589D12303DF7D93D8B4ABA8_StaticFields
{
public:
	// Facebook.Unity.Settings.FacebookSettings_<>c Facebook.Unity.Settings.FacebookSettings_<>c::<>9
	U3CU3Ec_t40659AD935CCBCC35589D12303DF7D93D8B4ABA8 * ___U3CU3E9_0;
	// System.Action`1<Facebook.Unity.Settings.FacebookSettings_OnChangeCallback> Facebook.Unity.Settings.FacebookSettings_<>c::<>9__76_0
	Action_1_t605040A52441734E3EA5810595C95A0D0568C238 * ___U3CU3E9__76_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t40659AD935CCBCC35589D12303DF7D93D8B4ABA8_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t40659AD935CCBCC35589D12303DF7D93D8B4ABA8 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t40659AD935CCBCC35589D12303DF7D93D8B4ABA8 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t40659AD935CCBCC35589D12303DF7D93D8B4ABA8 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__76_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t40659AD935CCBCC35589D12303DF7D93D8B4ABA8_StaticFields, ___U3CU3E9__76_0_1)); }
	inline Action_1_t605040A52441734E3EA5810595C95A0D0568C238 * get_U3CU3E9__76_0_1() const { return ___U3CU3E9__76_0_1; }
	inline Action_1_t605040A52441734E3EA5810595C95A0D0568C238 ** get_address_of_U3CU3E9__76_0_1() { return &___U3CU3E9__76_0_1; }
	inline void set_U3CU3E9__76_0_1(Action_1_t605040A52441734E3EA5810595C95A0D0568C238 * value)
	{
		___U3CU3E9__76_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__76_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T40659AD935CCBCC35589D12303DF7D93D8B4ABA8_H
#ifndef URLSCHEMES_T7B8D8455C2FE4B28F5F677FFAE86E14077D94D8C_H
#define URLSCHEMES_T7B8D8455C2FE4B28F5F677FFAE86E14077D94D8C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Settings.FacebookSettings_UrlSchemes
struct  UrlSchemes_t7B8D8455C2FE4B28F5F677FFAE86E14077D94D8C  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.String> Facebook.Unity.Settings.FacebookSettings_UrlSchemes::list
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___list_0;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(UrlSchemes_t7B8D8455C2FE4B28F5F677FFAE86E14077D94D8C, ___list_0)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_list_0() const { return ___list_0; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URLSCHEMES_T7B8D8455C2FE4B28F5F677FFAE86E14077D94D8C_H
#ifndef GSALLMESSAGETYPES_T04640013901A87C78C237ABAAD07C092912D5FAA_H
#define GSALLMESSAGETYPES_T04640013901A87C78C237ABAAD07C092912D5FAA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Messages.GSAllMessageTypes
struct  GSAllMessageTypes_t04640013901A87C78C237ABAAD07C092912D5FAA  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GSALLMESSAGETYPES_T04640013901A87C78C237ABAAD07C092912D5FAA_H
#ifndef GSDATA_T122BC20340935FE4E4E6F79E9A4E2F7C48844937_H
#define GSDATA_T122BC20340935FE4E4E6F79E9A4E2F7C48844937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Core.GSData
struct  GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937  : public RuntimeObject
{
public:
	// System.Collections.Generic.IDictionary`2<System.String,System.Object> GameSparks.Core.GSData::_data
	RuntimeObject* ____data_0;

public:
	inline static int32_t get_offset_of__data_0() { return static_cast<int32_t>(offsetof(GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937, ____data_0)); }
	inline RuntimeObject* get__data_0() const { return ____data_0; }
	inline RuntimeObject** get_address_of__data_0() { return &____data_0; }
	inline void set__data_0(RuntimeObject* value)
	{
		____data_0 = value;
		Il2CppCodeGenWriteBarrier((&____data_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GSDATA_T122BC20340935FE4E4E6F79E9A4E2F7C48844937_H
#ifndef GSINSTANCE_T595DBFD43D1F55EA5ABE397A222D1AE9AD307774_H
#define GSINSTANCE_T595DBFD43D1F55EA5ABE397A222D1AE9AD307774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Core.GSInstance
struct  GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774  : public RuntimeObject
{
public:
	// System.Boolean GameSparks.Core.GSInstance::_paused
	bool ____paused_0;
	// System.String GameSparks.Core.GSInstance::_sessionId
	String_t* ____sessionId_1;
	// System.Collections.Generic.LinkedList`1<GameSparks.Core.GSRequest> GameSparks.Core.GSInstance::_sendQueue
	LinkedList_1_t78335B40DBFA8F7BEB0B3258FAF2A30F78219F9C * ____sendQueue_2;
	// System.Collections.Generic.LinkedList`1<GameSparks.Core.GSRequest> GameSparks.Core.GSInstance::_persistantQueue
	LinkedList_1_t78335B40DBFA8F7BEB0B3258FAF2A30F78219F9C * ____persistantQueue_3;
	// System.Collections.Generic.LinkedList`1<System.Action> GameSparks.Core.GSInstance::_requestedActions
	LinkedList_1_tE7F103DCAF02C8979414D806B0F680082082D253 * ____requestedActions_4;
	// System.String GameSparks.Core.GSInstance::_persistantQueueUserId
	String_t* ____persistantQueueUserId_5;
	// System.Collections.Generic.List`1<GameSparks.Core.GSConnection> GameSparks.Core.GSInstance::_connections
	List_1_t96ABFF3320DF81A409731B576A781AC5D5C2E721 * ____connections_6;
	// GameSparks.IGameSparksTimer GameSparks.Core.GSInstance::_mainLoopTimer
	RuntimeObject* ____mainLoopTimer_7;
	// GameSparks.IGameSparksTimer GameSparks.Core.GSInstance::_durableWriteTimer
	RuntimeObject* ____durableWriteTimer_8;
	// System.Boolean GameSparks.Core.GSInstance::_durableQueueDirty
	bool ____durableQueueDirty_9;
	// System.Int64 GameSparks.Core.GSInstance::_requestCounter
	int64_t ____requestCounter_10;
	// System.String GameSparks.Core.GSInstance::_currentSocketUrl
	String_t* ____currentSocketUrl_11;
	// System.Boolean GameSparks.Core.GSInstance::_ready
	bool ____ready_12;
	// System.Boolean GameSparks.Core.GSInstance::_durableQueuePaused
	bool ____durableQueuePaused_13;
	// System.Int64 GameSparks.Core.GSInstance::_durableDrainTimer
	int64_t ____durableDrainTimer_14;
	// System.Int32 GameSparks.Core.GSInstance::_currAttemptConnection
	int32_t ____currAttemptConnection_15;
	// System.Int64 GameSparks.Core.GSInstance::_nextReconnect
	int64_t ____nextReconnect_16;
	// System.Int32 GameSparks.Core.GSInstance::_retryBase
	int32_t ____retryBase_18;
	// System.Int32 GameSparks.Core.GSInstance::_retryMax
	int32_t ____retryMax_19;
	// System.Int32 GameSparks.Core.GSInstance::_requestTimeout
	int32_t ____requestTimeout_20;
	// System.Int32 GameSparks.Core.GSInstance::_durableConcurrentRequests
	int32_t ____durableConcurrentRequests_21;
	// System.Int32 GameSparks.Core.GSInstance::_durableDrainInterval
	int32_t ____durableDrainInterval_22;
	// System.Int32 GameSparks.Core.GSInstance::_handshakeOffset
	int32_t ____handshakeOffset_23;
	// GameSparks.Core.IGSPlatform GameSparks.Core.GSInstance::<GSPlatform>k__BackingField
	RuntimeObject* ___U3CGSPlatformU3Ek__BackingField_26;
	// System.String GameSparks.Core.GSInstance::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_27;
	// System.Boolean GameSparks.Core.GSInstance::<TraceMessages>k__BackingField
	bool ___U3CTraceMessagesU3Ek__BackingField_28;
	// System.Action GameSparks.Core.GSInstance::<DurableQueueLoaded>k__BackingField
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___U3CDurableQueueLoadedU3Ek__BackingField_29;
	// System.Func`2<System.String,System.String> GameSparks.Core.GSInstance::<OnGameSparksNonce>k__BackingField
	Func_2_tD9EBF5FAD46B4F561FE5ABE814DC60E6253B8B96 * ___U3COnGameSparksNonceU3Ek__BackingField_30;
	// System.Boolean GameSparks.Core.GSInstance::<DurableQueueRunning>k__BackingField
	bool ___U3CDurableQueueRunningU3Ek__BackingField_31;
	// System.Action`1<System.Boolean> GameSparks.Core.GSInstance::<GameSparksAvailable>k__BackingField
	Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * ___U3CGameSparksAvailableU3Ek__BackingField_32;
	// System.Action`1<System.String> GameSparks.Core.GSInstance::<GameSparksAuthenticated>k__BackingField
	Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * ___U3CGameSparksAuthenticatedU3Ek__BackingField_33;

public:
	inline static int32_t get_offset_of__paused_0() { return static_cast<int32_t>(offsetof(GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774, ____paused_0)); }
	inline bool get__paused_0() const { return ____paused_0; }
	inline bool* get_address_of__paused_0() { return &____paused_0; }
	inline void set__paused_0(bool value)
	{
		____paused_0 = value;
	}

	inline static int32_t get_offset_of__sessionId_1() { return static_cast<int32_t>(offsetof(GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774, ____sessionId_1)); }
	inline String_t* get__sessionId_1() const { return ____sessionId_1; }
	inline String_t** get_address_of__sessionId_1() { return &____sessionId_1; }
	inline void set__sessionId_1(String_t* value)
	{
		____sessionId_1 = value;
		Il2CppCodeGenWriteBarrier((&____sessionId_1), value);
	}

	inline static int32_t get_offset_of__sendQueue_2() { return static_cast<int32_t>(offsetof(GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774, ____sendQueue_2)); }
	inline LinkedList_1_t78335B40DBFA8F7BEB0B3258FAF2A30F78219F9C * get__sendQueue_2() const { return ____sendQueue_2; }
	inline LinkedList_1_t78335B40DBFA8F7BEB0B3258FAF2A30F78219F9C ** get_address_of__sendQueue_2() { return &____sendQueue_2; }
	inline void set__sendQueue_2(LinkedList_1_t78335B40DBFA8F7BEB0B3258FAF2A30F78219F9C * value)
	{
		____sendQueue_2 = value;
		Il2CppCodeGenWriteBarrier((&____sendQueue_2), value);
	}

	inline static int32_t get_offset_of__persistantQueue_3() { return static_cast<int32_t>(offsetof(GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774, ____persistantQueue_3)); }
	inline LinkedList_1_t78335B40DBFA8F7BEB0B3258FAF2A30F78219F9C * get__persistantQueue_3() const { return ____persistantQueue_3; }
	inline LinkedList_1_t78335B40DBFA8F7BEB0B3258FAF2A30F78219F9C ** get_address_of__persistantQueue_3() { return &____persistantQueue_3; }
	inline void set__persistantQueue_3(LinkedList_1_t78335B40DBFA8F7BEB0B3258FAF2A30F78219F9C * value)
	{
		____persistantQueue_3 = value;
		Il2CppCodeGenWriteBarrier((&____persistantQueue_3), value);
	}

	inline static int32_t get_offset_of__requestedActions_4() { return static_cast<int32_t>(offsetof(GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774, ____requestedActions_4)); }
	inline LinkedList_1_tE7F103DCAF02C8979414D806B0F680082082D253 * get__requestedActions_4() const { return ____requestedActions_4; }
	inline LinkedList_1_tE7F103DCAF02C8979414D806B0F680082082D253 ** get_address_of__requestedActions_4() { return &____requestedActions_4; }
	inline void set__requestedActions_4(LinkedList_1_tE7F103DCAF02C8979414D806B0F680082082D253 * value)
	{
		____requestedActions_4 = value;
		Il2CppCodeGenWriteBarrier((&____requestedActions_4), value);
	}

	inline static int32_t get_offset_of__persistantQueueUserId_5() { return static_cast<int32_t>(offsetof(GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774, ____persistantQueueUserId_5)); }
	inline String_t* get__persistantQueueUserId_5() const { return ____persistantQueueUserId_5; }
	inline String_t** get_address_of__persistantQueueUserId_5() { return &____persistantQueueUserId_5; }
	inline void set__persistantQueueUserId_5(String_t* value)
	{
		____persistantQueueUserId_5 = value;
		Il2CppCodeGenWriteBarrier((&____persistantQueueUserId_5), value);
	}

	inline static int32_t get_offset_of__connections_6() { return static_cast<int32_t>(offsetof(GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774, ____connections_6)); }
	inline List_1_t96ABFF3320DF81A409731B576A781AC5D5C2E721 * get__connections_6() const { return ____connections_6; }
	inline List_1_t96ABFF3320DF81A409731B576A781AC5D5C2E721 ** get_address_of__connections_6() { return &____connections_6; }
	inline void set__connections_6(List_1_t96ABFF3320DF81A409731B576A781AC5D5C2E721 * value)
	{
		____connections_6 = value;
		Il2CppCodeGenWriteBarrier((&____connections_6), value);
	}

	inline static int32_t get_offset_of__mainLoopTimer_7() { return static_cast<int32_t>(offsetof(GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774, ____mainLoopTimer_7)); }
	inline RuntimeObject* get__mainLoopTimer_7() const { return ____mainLoopTimer_7; }
	inline RuntimeObject** get_address_of__mainLoopTimer_7() { return &____mainLoopTimer_7; }
	inline void set__mainLoopTimer_7(RuntimeObject* value)
	{
		____mainLoopTimer_7 = value;
		Il2CppCodeGenWriteBarrier((&____mainLoopTimer_7), value);
	}

	inline static int32_t get_offset_of__durableWriteTimer_8() { return static_cast<int32_t>(offsetof(GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774, ____durableWriteTimer_8)); }
	inline RuntimeObject* get__durableWriteTimer_8() const { return ____durableWriteTimer_8; }
	inline RuntimeObject** get_address_of__durableWriteTimer_8() { return &____durableWriteTimer_8; }
	inline void set__durableWriteTimer_8(RuntimeObject* value)
	{
		____durableWriteTimer_8 = value;
		Il2CppCodeGenWriteBarrier((&____durableWriteTimer_8), value);
	}

	inline static int32_t get_offset_of__durableQueueDirty_9() { return static_cast<int32_t>(offsetof(GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774, ____durableQueueDirty_9)); }
	inline bool get__durableQueueDirty_9() const { return ____durableQueueDirty_9; }
	inline bool* get_address_of__durableQueueDirty_9() { return &____durableQueueDirty_9; }
	inline void set__durableQueueDirty_9(bool value)
	{
		____durableQueueDirty_9 = value;
	}

	inline static int32_t get_offset_of__requestCounter_10() { return static_cast<int32_t>(offsetof(GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774, ____requestCounter_10)); }
	inline int64_t get__requestCounter_10() const { return ____requestCounter_10; }
	inline int64_t* get_address_of__requestCounter_10() { return &____requestCounter_10; }
	inline void set__requestCounter_10(int64_t value)
	{
		____requestCounter_10 = value;
	}

	inline static int32_t get_offset_of__currentSocketUrl_11() { return static_cast<int32_t>(offsetof(GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774, ____currentSocketUrl_11)); }
	inline String_t* get__currentSocketUrl_11() const { return ____currentSocketUrl_11; }
	inline String_t** get_address_of__currentSocketUrl_11() { return &____currentSocketUrl_11; }
	inline void set__currentSocketUrl_11(String_t* value)
	{
		____currentSocketUrl_11 = value;
		Il2CppCodeGenWriteBarrier((&____currentSocketUrl_11), value);
	}

	inline static int32_t get_offset_of__ready_12() { return static_cast<int32_t>(offsetof(GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774, ____ready_12)); }
	inline bool get__ready_12() const { return ____ready_12; }
	inline bool* get_address_of__ready_12() { return &____ready_12; }
	inline void set__ready_12(bool value)
	{
		____ready_12 = value;
	}

	inline static int32_t get_offset_of__durableQueuePaused_13() { return static_cast<int32_t>(offsetof(GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774, ____durableQueuePaused_13)); }
	inline bool get__durableQueuePaused_13() const { return ____durableQueuePaused_13; }
	inline bool* get_address_of__durableQueuePaused_13() { return &____durableQueuePaused_13; }
	inline void set__durableQueuePaused_13(bool value)
	{
		____durableQueuePaused_13 = value;
	}

	inline static int32_t get_offset_of__durableDrainTimer_14() { return static_cast<int32_t>(offsetof(GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774, ____durableDrainTimer_14)); }
	inline int64_t get__durableDrainTimer_14() const { return ____durableDrainTimer_14; }
	inline int64_t* get_address_of__durableDrainTimer_14() { return &____durableDrainTimer_14; }
	inline void set__durableDrainTimer_14(int64_t value)
	{
		____durableDrainTimer_14 = value;
	}

	inline static int32_t get_offset_of__currAttemptConnection_15() { return static_cast<int32_t>(offsetof(GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774, ____currAttemptConnection_15)); }
	inline int32_t get__currAttemptConnection_15() const { return ____currAttemptConnection_15; }
	inline int32_t* get_address_of__currAttemptConnection_15() { return &____currAttemptConnection_15; }
	inline void set__currAttemptConnection_15(int32_t value)
	{
		____currAttemptConnection_15 = value;
	}

	inline static int32_t get_offset_of__nextReconnect_16() { return static_cast<int32_t>(offsetof(GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774, ____nextReconnect_16)); }
	inline int64_t get__nextReconnect_16() const { return ____nextReconnect_16; }
	inline int64_t* get_address_of__nextReconnect_16() { return &____nextReconnect_16; }
	inline void set__nextReconnect_16(int64_t value)
	{
		____nextReconnect_16 = value;
	}

	inline static int32_t get_offset_of__retryBase_18() { return static_cast<int32_t>(offsetof(GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774, ____retryBase_18)); }
	inline int32_t get__retryBase_18() const { return ____retryBase_18; }
	inline int32_t* get_address_of__retryBase_18() { return &____retryBase_18; }
	inline void set__retryBase_18(int32_t value)
	{
		____retryBase_18 = value;
	}

	inline static int32_t get_offset_of__retryMax_19() { return static_cast<int32_t>(offsetof(GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774, ____retryMax_19)); }
	inline int32_t get__retryMax_19() const { return ____retryMax_19; }
	inline int32_t* get_address_of__retryMax_19() { return &____retryMax_19; }
	inline void set__retryMax_19(int32_t value)
	{
		____retryMax_19 = value;
	}

	inline static int32_t get_offset_of__requestTimeout_20() { return static_cast<int32_t>(offsetof(GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774, ____requestTimeout_20)); }
	inline int32_t get__requestTimeout_20() const { return ____requestTimeout_20; }
	inline int32_t* get_address_of__requestTimeout_20() { return &____requestTimeout_20; }
	inline void set__requestTimeout_20(int32_t value)
	{
		____requestTimeout_20 = value;
	}

	inline static int32_t get_offset_of__durableConcurrentRequests_21() { return static_cast<int32_t>(offsetof(GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774, ____durableConcurrentRequests_21)); }
	inline int32_t get__durableConcurrentRequests_21() const { return ____durableConcurrentRequests_21; }
	inline int32_t* get_address_of__durableConcurrentRequests_21() { return &____durableConcurrentRequests_21; }
	inline void set__durableConcurrentRequests_21(int32_t value)
	{
		____durableConcurrentRequests_21 = value;
	}

	inline static int32_t get_offset_of__durableDrainInterval_22() { return static_cast<int32_t>(offsetof(GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774, ____durableDrainInterval_22)); }
	inline int32_t get__durableDrainInterval_22() const { return ____durableDrainInterval_22; }
	inline int32_t* get_address_of__durableDrainInterval_22() { return &____durableDrainInterval_22; }
	inline void set__durableDrainInterval_22(int32_t value)
	{
		____durableDrainInterval_22 = value;
	}

	inline static int32_t get_offset_of__handshakeOffset_23() { return static_cast<int32_t>(offsetof(GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774, ____handshakeOffset_23)); }
	inline int32_t get__handshakeOffset_23() const { return ____handshakeOffset_23; }
	inline int32_t* get_address_of__handshakeOffset_23() { return &____handshakeOffset_23; }
	inline void set__handshakeOffset_23(int32_t value)
	{
		____handshakeOffset_23 = value;
	}

	inline static int32_t get_offset_of_U3CGSPlatformU3Ek__BackingField_26() { return static_cast<int32_t>(offsetof(GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774, ___U3CGSPlatformU3Ek__BackingField_26)); }
	inline RuntimeObject* get_U3CGSPlatformU3Ek__BackingField_26() const { return ___U3CGSPlatformU3Ek__BackingField_26; }
	inline RuntimeObject** get_address_of_U3CGSPlatformU3Ek__BackingField_26() { return &___U3CGSPlatformU3Ek__BackingField_26; }
	inline void set_U3CGSPlatformU3Ek__BackingField_26(RuntimeObject* value)
	{
		___U3CGSPlatformU3Ek__BackingField_26 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGSPlatformU3Ek__BackingField_26), value);
	}

	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774, ___U3CNameU3Ek__BackingField_27)); }
	inline String_t* get_U3CNameU3Ek__BackingField_27() const { return ___U3CNameU3Ek__BackingField_27; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_27() { return &___U3CNameU3Ek__BackingField_27; }
	inline void set_U3CNameU3Ek__BackingField_27(String_t* value)
	{
		___U3CNameU3Ek__BackingField_27 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_27), value);
	}

	inline static int32_t get_offset_of_U3CTraceMessagesU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774, ___U3CTraceMessagesU3Ek__BackingField_28)); }
	inline bool get_U3CTraceMessagesU3Ek__BackingField_28() const { return ___U3CTraceMessagesU3Ek__BackingField_28; }
	inline bool* get_address_of_U3CTraceMessagesU3Ek__BackingField_28() { return &___U3CTraceMessagesU3Ek__BackingField_28; }
	inline void set_U3CTraceMessagesU3Ek__BackingField_28(bool value)
	{
		___U3CTraceMessagesU3Ek__BackingField_28 = value;
	}

	inline static int32_t get_offset_of_U3CDurableQueueLoadedU3Ek__BackingField_29() { return static_cast<int32_t>(offsetof(GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774, ___U3CDurableQueueLoadedU3Ek__BackingField_29)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_U3CDurableQueueLoadedU3Ek__BackingField_29() const { return ___U3CDurableQueueLoadedU3Ek__BackingField_29; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_U3CDurableQueueLoadedU3Ek__BackingField_29() { return &___U3CDurableQueueLoadedU3Ek__BackingField_29; }
	inline void set_U3CDurableQueueLoadedU3Ek__BackingField_29(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___U3CDurableQueueLoadedU3Ek__BackingField_29 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDurableQueueLoadedU3Ek__BackingField_29), value);
	}

	inline static int32_t get_offset_of_U3COnGameSparksNonceU3Ek__BackingField_30() { return static_cast<int32_t>(offsetof(GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774, ___U3COnGameSparksNonceU3Ek__BackingField_30)); }
	inline Func_2_tD9EBF5FAD46B4F561FE5ABE814DC60E6253B8B96 * get_U3COnGameSparksNonceU3Ek__BackingField_30() const { return ___U3COnGameSparksNonceU3Ek__BackingField_30; }
	inline Func_2_tD9EBF5FAD46B4F561FE5ABE814DC60E6253B8B96 ** get_address_of_U3COnGameSparksNonceU3Ek__BackingField_30() { return &___U3COnGameSparksNonceU3Ek__BackingField_30; }
	inline void set_U3COnGameSparksNonceU3Ek__BackingField_30(Func_2_tD9EBF5FAD46B4F561FE5ABE814DC60E6253B8B96 * value)
	{
		___U3COnGameSparksNonceU3Ek__BackingField_30 = value;
		Il2CppCodeGenWriteBarrier((&___U3COnGameSparksNonceU3Ek__BackingField_30), value);
	}

	inline static int32_t get_offset_of_U3CDurableQueueRunningU3Ek__BackingField_31() { return static_cast<int32_t>(offsetof(GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774, ___U3CDurableQueueRunningU3Ek__BackingField_31)); }
	inline bool get_U3CDurableQueueRunningU3Ek__BackingField_31() const { return ___U3CDurableQueueRunningU3Ek__BackingField_31; }
	inline bool* get_address_of_U3CDurableQueueRunningU3Ek__BackingField_31() { return &___U3CDurableQueueRunningU3Ek__BackingField_31; }
	inline void set_U3CDurableQueueRunningU3Ek__BackingField_31(bool value)
	{
		___U3CDurableQueueRunningU3Ek__BackingField_31 = value;
	}

	inline static int32_t get_offset_of_U3CGameSparksAvailableU3Ek__BackingField_32() { return static_cast<int32_t>(offsetof(GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774, ___U3CGameSparksAvailableU3Ek__BackingField_32)); }
	inline Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * get_U3CGameSparksAvailableU3Ek__BackingField_32() const { return ___U3CGameSparksAvailableU3Ek__BackingField_32; }
	inline Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD ** get_address_of_U3CGameSparksAvailableU3Ek__BackingField_32() { return &___U3CGameSparksAvailableU3Ek__BackingField_32; }
	inline void set_U3CGameSparksAvailableU3Ek__BackingField_32(Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * value)
	{
		___U3CGameSparksAvailableU3Ek__BackingField_32 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGameSparksAvailableU3Ek__BackingField_32), value);
	}

	inline static int32_t get_offset_of_U3CGameSparksAuthenticatedU3Ek__BackingField_33() { return static_cast<int32_t>(offsetof(GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774, ___U3CGameSparksAuthenticatedU3Ek__BackingField_33)); }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * get_U3CGameSparksAuthenticatedU3Ek__BackingField_33() const { return ___U3CGameSparksAuthenticatedU3Ek__BackingField_33; }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 ** get_address_of_U3CGameSparksAuthenticatedU3Ek__BackingField_33() { return &___U3CGameSparksAuthenticatedU3Ek__BackingField_33; }
	inline void set_U3CGameSparksAuthenticatedU3Ek__BackingField_33(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * value)
	{
		___U3CGameSparksAuthenticatedU3Ek__BackingField_33 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGameSparksAuthenticatedU3Ek__BackingField_33), value);
	}
};

struct GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774_StaticFields
{
public:
	// System.Random GameSparks.Core.GSInstance::_random
	Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F * ____random_17;
	// System.Collections.Generic.Dictionary`2<System.String,GameSparks.Core.GSInstance> GameSparks.Core.GSInstance::Instances
	Dictionary_2_t67B78321AEFED4FABBEA49DA993A79846D2855A9 * ___Instances_24;
	// System.String GameSparks.Core.GSInstance::serviceUrlBase
	String_t* ___serviceUrlBase_25;

public:
	inline static int32_t get_offset_of__random_17() { return static_cast<int32_t>(offsetof(GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774_StaticFields, ____random_17)); }
	inline Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F * get__random_17() const { return ____random_17; }
	inline Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F ** get_address_of__random_17() { return &____random_17; }
	inline void set__random_17(Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F * value)
	{
		____random_17 = value;
		Il2CppCodeGenWriteBarrier((&____random_17), value);
	}

	inline static int32_t get_offset_of_Instances_24() { return static_cast<int32_t>(offsetof(GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774_StaticFields, ___Instances_24)); }
	inline Dictionary_2_t67B78321AEFED4FABBEA49DA993A79846D2855A9 * get_Instances_24() const { return ___Instances_24; }
	inline Dictionary_2_t67B78321AEFED4FABBEA49DA993A79846D2855A9 ** get_address_of_Instances_24() { return &___Instances_24; }
	inline void set_Instances_24(Dictionary_2_t67B78321AEFED4FABBEA49DA993A79846D2855A9 * value)
	{
		___Instances_24 = value;
		Il2CppCodeGenWriteBarrier((&___Instances_24), value);
	}

	inline static int32_t get_offset_of_serviceUrlBase_25() { return static_cast<int32_t>(offsetof(GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774_StaticFields, ___serviceUrlBase_25)); }
	inline String_t* get_serviceUrlBase_25() const { return ___serviceUrlBase_25; }
	inline String_t** get_address_of_serviceUrlBase_25() { return &___serviceUrlBase_25; }
	inline void set_serviceUrlBase_25(String_t* value)
	{
		___serviceUrlBase_25 = value;
		Il2CppCodeGenWriteBarrier((&___serviceUrlBase_25), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GSINSTANCE_T595DBFD43D1F55EA5ABE397A222D1AE9AD307774_H
#ifndef U3CU3EC__DISPLAYCLASS1_T35DB9761FA638ED6DE7BCF9CA2C379832C1F00B9_H
#define U3CU3EC__DISPLAYCLASS1_T35DB9761FA638ED6DE7BCF9CA2C379832C1F00B9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Core.GSInstance_<>c__DisplayClass1
struct  U3CU3Ec__DisplayClass1_t35DB9761FA638ED6DE7BCF9CA2C379832C1F00B9  : public RuntimeObject
{
public:
	// GameSparks.Core.GSInstance GameSparks.Core.GSInstance_<>c__DisplayClass1::<>4__this
	GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774 * ___U3CU3E4__this_0;
	// System.Action GameSparks.Core.GSInstance_<>c__DisplayClass1::onDone
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___onDone_1;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass1_t35DB9761FA638ED6DE7BCF9CA2C379832C1F00B9, ___U3CU3E4__this_0)); }
	inline GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_onDone_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass1_t35DB9761FA638ED6DE7BCF9CA2C379832C1F00B9, ___onDone_1)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_onDone_1() const { return ___onDone_1; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_onDone_1() { return &___onDone_1; }
	inline void set_onDone_1(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___onDone_1 = value;
		Il2CppCodeGenWriteBarrier((&___onDone_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS1_T35DB9761FA638ED6DE7BCF9CA2C379832C1F00B9_H
#ifndef U3CU3EC__DISPLAYCLASS12_T814181B763214C49099CD4172575AA692A09DA5A_H
#define U3CU3EC__DISPLAYCLASS12_T814181B763214C49099CD4172575AA692A09DA5A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Core.GSInstance_<>c__DisplayClass12
struct  U3CU3Ec__DisplayClass12_t814181B763214C49099CD4172575AA692A09DA5A  : public RuntimeObject
{
public:
	// GameSparks.Core.GSInstance GameSparks.Core.GSInstance_<>c__DisplayClass12::<>4__this
	GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774 * ___U3CU3E4__this_0;
	// GameSparks.Core.GSRequest GameSparks.Core.GSInstance_<>c__DisplayClass12::request
	GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 * ___request_1;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass12_t814181B763214C49099CD4172575AA692A09DA5A, ___U3CU3E4__this_0)); }
	inline GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_request_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass12_t814181B763214C49099CD4172575AA692A09DA5A, ___request_1)); }
	inline GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 * get_request_1() const { return ___request_1; }
	inline GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 ** get_address_of_request_1() { return &___request_1; }
	inline void set_request_1(GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 * value)
	{
		___request_1 = value;
		Il2CppCodeGenWriteBarrier((&___request_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS12_T814181B763214C49099CD4172575AA692A09DA5A_H
#ifndef U3CU3EC__DISPLAYCLASS16_T5E2D53B935ECEFF315042EB3490BFE1049902397_H
#define U3CU3EC__DISPLAYCLASS16_T5E2D53B935ECEFF315042EB3490BFE1049902397_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Core.GSInstance_<>c__DisplayClass16
struct  U3CU3Ec__DisplayClass16_t5E2D53B935ECEFF315042EB3490BFE1049902397  : public RuntimeObject
{
public:
	// GameSparks.Core.GSInstance GameSparks.Core.GSInstance_<>c__DisplayClass16::<>4__this
	GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774 * ___U3CU3E4__this_0;
	// System.Boolean GameSparks.Core.GSInstance_<>c__DisplayClass16::avail
	bool ___avail_1;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass16_t5E2D53B935ECEFF315042EB3490BFE1049902397, ___U3CU3E4__this_0)); }
	inline GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_avail_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass16_t5E2D53B935ECEFF315042EB3490BFE1049902397, ___avail_1)); }
	inline bool get_avail_1() const { return ___avail_1; }
	inline bool* get_address_of_avail_1() { return &___avail_1; }
	inline void set_avail_1(bool value)
	{
		___avail_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS16_T5E2D53B935ECEFF315042EB3490BFE1049902397_H
#ifndef U3CU3EC__DISPLAYCLASS19_T1DD3343E8C4DFEA6288E457C5B5688599E527554_H
#define U3CU3EC__DISPLAYCLASS19_T1DD3343E8C4DFEA6288E457C5B5688599E527554_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Core.GSInstance_<>c__DisplayClass19
struct  U3CU3Ec__DisplayClass19_t1DD3343E8C4DFEA6288E457C5B5688599E527554  : public RuntimeObject
{
public:
	// System.Exception GameSparks.Core.GSInstance_<>c__DisplayClass19::e
	Exception_t * ___e_0;
	// GameSparks.Core.GSInstance GameSparks.Core.GSInstance_<>c__DisplayClass19::<>4__this
	GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_e_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass19_t1DD3343E8C4DFEA6288E457C5B5688599E527554, ___e_0)); }
	inline Exception_t * get_e_0() const { return ___e_0; }
	inline Exception_t ** get_address_of_e_0() { return &___e_0; }
	inline void set_e_0(Exception_t * value)
	{
		___e_0 = value;
		Il2CppCodeGenWriteBarrier((&___e_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass19_t1DD3343E8C4DFEA6288E457C5B5688599E527554, ___U3CU3E4__this_1)); }
	inline GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS19_T1DD3343E8C4DFEA6288E457C5B5688599E527554_H
#ifndef U3CU3EC__DISPLAYCLASS1C_TD18D627568F837BD8A71FE22C6BE8D0B1661118D_H
#define U3CU3EC__DISPLAYCLASS1C_TD18D627568F837BD8A71FE22C6BE8D0B1661118D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Core.GSInstance_<>c__DisplayClass1c
struct  U3CU3Ec__DisplayClass1c_tD18D627568F837BD8A71FE22C6BE8D0B1661118D  : public RuntimeObject
{
public:
	// GameSparks.Core.GSObject GameSparks.Core.GSInstance_<>c__DisplayClass1c::error
	GSObject_t05160F6677D08913C6CD9AB7C7DD9454D3B12E88 * ___error_0;
	// GameSparks.Core.GSInstance GameSparks.Core.GSInstance_<>c__DisplayClass1c::<>4__this
	GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774 * ___U3CU3E4__this_1;
	// GameSparks.Core.GSRequest GameSparks.Core.GSInstance_<>c__DisplayClass1c::request
	GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 * ___request_2;

public:
	inline static int32_t get_offset_of_error_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass1c_tD18D627568F837BD8A71FE22C6BE8D0B1661118D, ___error_0)); }
	inline GSObject_t05160F6677D08913C6CD9AB7C7DD9454D3B12E88 * get_error_0() const { return ___error_0; }
	inline GSObject_t05160F6677D08913C6CD9AB7C7DD9454D3B12E88 ** get_address_of_error_0() { return &___error_0; }
	inline void set_error_0(GSObject_t05160F6677D08913C6CD9AB7C7DD9454D3B12E88 * value)
	{
		___error_0 = value;
		Il2CppCodeGenWriteBarrier((&___error_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass1c_tD18D627568F837BD8A71FE22C6BE8D0B1661118D, ___U3CU3E4__this_1)); }
	inline GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}

	inline static int32_t get_offset_of_request_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass1c_tD18D627568F837BD8A71FE22C6BE8D0B1661118D, ___request_2)); }
	inline GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 * get_request_2() const { return ___request_2; }
	inline GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 ** get_address_of_request_2() { return &___request_2; }
	inline void set_request_2(GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 * value)
	{
		___request_2 = value;
		Il2CppCodeGenWriteBarrier((&___request_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS1C_TD18D627568F837BD8A71FE22C6BE8D0B1661118D_H
#ifndef U3CU3EC__DISPLAYCLASS20_T9E2EE9F0CFD8AFD6355464F612E393BEB7600EDE_H
#define U3CU3EC__DISPLAYCLASS20_T9E2EE9F0CFD8AFD6355464F612E393BEB7600EDE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Core.GSInstance_<>c__DisplayClass20
struct  U3CU3Ec__DisplayClass20_t9E2EE9F0CFD8AFD6355464F612E393BEB7600EDE  : public RuntimeObject
{
public:
	// GameSparks.Core.GSInstance GameSparks.Core.GSInstance_<>c__DisplayClass20::<>4__this
	GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774 * ___U3CU3E4__this_0;
	// GameSparks.Core.GSObject GameSparks.Core.GSInstance_<>c__DisplayClass20::response
	GSObject_t05160F6677D08913C6CD9AB7C7DD9454D3B12E88 * ___response_1;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass20_t9E2EE9F0CFD8AFD6355464F612E393BEB7600EDE, ___U3CU3E4__this_0)); }
	inline GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_response_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass20_t9E2EE9F0CFD8AFD6355464F612E393BEB7600EDE, ___response_1)); }
	inline GSObject_t05160F6677D08913C6CD9AB7C7DD9454D3B12E88 * get_response_1() const { return ___response_1; }
	inline GSObject_t05160F6677D08913C6CD9AB7C7DD9454D3B12E88 ** get_address_of_response_1() { return &___response_1; }
	inline void set_response_1(GSObject_t05160F6677D08913C6CD9AB7C7DD9454D3B12E88 * value)
	{
		___response_1 = value;
		Il2CppCodeGenWriteBarrier((&___response_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS20_T9E2EE9F0CFD8AFD6355464F612E393BEB7600EDE_H
#ifndef U3CU3EC__DISPLAYCLASS23_TDCBE81CF506E0DE1C2C2327F0252BAF59DB5A963_H
#define U3CU3EC__DISPLAYCLASS23_TDCBE81CF506E0DE1C2C2327F0252BAF59DB5A963_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Core.GSInstance_<>c__DisplayClass23
struct  U3CU3Ec__DisplayClass23_tDCBE81CF506E0DE1C2C2327F0252BAF59DB5A963  : public RuntimeObject
{
public:
	// System.Boolean GameSparks.Core.GSInstance_<>c__DisplayClass23::previous_durableQueuePaused
	bool ___previous_durableQueuePaused_0;
	// GameSparks.Core.GSInstance GameSparks.Core.GSInstance_<>c__DisplayClass23::<>4__this
	GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774 * ___U3CU3E4__this_1;
	// System.String GameSparks.Core.GSInstance_<>c__DisplayClass23::userId
	String_t* ___userId_2;

public:
	inline static int32_t get_offset_of_previous_durableQueuePaused_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass23_tDCBE81CF506E0DE1C2C2327F0252BAF59DB5A963, ___previous_durableQueuePaused_0)); }
	inline bool get_previous_durableQueuePaused_0() const { return ___previous_durableQueuePaused_0; }
	inline bool* get_address_of_previous_durableQueuePaused_0() { return &___previous_durableQueuePaused_0; }
	inline void set_previous_durableQueuePaused_0(bool value)
	{
		___previous_durableQueuePaused_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass23_tDCBE81CF506E0DE1C2C2327F0252BAF59DB5A963, ___U3CU3E4__this_1)); }
	inline GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}

	inline static int32_t get_offset_of_userId_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass23_tDCBE81CF506E0DE1C2C2327F0252BAF59DB5A963, ___userId_2)); }
	inline String_t* get_userId_2() const { return ___userId_2; }
	inline String_t** get_address_of_userId_2() { return &___userId_2; }
	inline void set_userId_2(String_t* value)
	{
		___userId_2 = value;
		Il2CppCodeGenWriteBarrier((&___userId_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS23_TDCBE81CF506E0DE1C2C2327F0252BAF59DB5A963_H
#ifndef U3CU3EC__DISPLAYCLASS9_T36DC1EED2B2386EA20D51512BC4B47C97E598E94_H
#define U3CU3EC__DISPLAYCLASS9_T36DC1EED2B2386EA20D51512BC4B47C97E598E94_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Core.GSInstance_<>c__DisplayClass9
struct  U3CU3Ec__DisplayClass9_t36DC1EED2B2386EA20D51512BC4B47C97E598E94  : public RuntimeObject
{
public:
	// GameSparks.Core.GSObject GameSparks.Core.GSInstance_<>c__DisplayClass9::response
	GSObject_t05160F6677D08913C6CD9AB7C7DD9454D3B12E88 * ___response_0;
	// GameSparks.Core.GSInstance GameSparks.Core.GSInstance_<>c__DisplayClass9::<>4__this
	GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_response_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass9_t36DC1EED2B2386EA20D51512BC4B47C97E598E94, ___response_0)); }
	inline GSObject_t05160F6677D08913C6CD9AB7C7DD9454D3B12E88 * get_response_0() const { return ___response_0; }
	inline GSObject_t05160F6677D08913C6CD9AB7C7DD9454D3B12E88 ** get_address_of_response_0() { return &___response_0; }
	inline void set_response_0(GSObject_t05160F6677D08913C6CD9AB7C7DD9454D3B12E88 * value)
	{
		___response_0 = value;
		Il2CppCodeGenWriteBarrier((&___response_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass9_t36DC1EED2B2386EA20D51512BC4B47C97E598E94, ___U3CU3E4__this_1)); }
	inline GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS9_T36DC1EED2B2386EA20D51512BC4B47C97E598E94_H
#ifndef U3CU3EC__DISPLAYCLASSF_T76A25F8FA80AD871F1C384D17CF5F1E17D4B1066_H
#define U3CU3EC__DISPLAYCLASSF_T76A25F8FA80AD871F1C384D17CF5F1E17D4B1066_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Core.GSInstance_<>c__DisplayClassf
struct  U3CU3Ec__DisplayClassf_t76A25F8FA80AD871F1C384D17CF5F1E17D4B1066  : public RuntimeObject
{
public:
	// GameSparks.Core.GSInstance GameSparks.Core.GSInstance_<>c__DisplayClassf::<>4__this
	GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774 * ___U3CU3E4__this_0;
	// GameSparks.Core.GSObject GameSparks.Core.GSInstance_<>c__DisplayClassf::response
	GSObject_t05160F6677D08913C6CD9AB7C7DD9454D3B12E88 * ___response_1;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClassf_t76A25F8FA80AD871F1C384D17CF5F1E17D4B1066, ___U3CU3E4__this_0)); }
	inline GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_response_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClassf_t76A25F8FA80AD871F1C384D17CF5F1E17D4B1066, ___response_1)); }
	inline GSObject_t05160F6677D08913C6CD9AB7C7DD9454D3B12E88 * get_response_1() const { return ___response_1; }
	inline GSObject_t05160F6677D08913C6CD9AB7C7DD9454D3B12E88 ** get_address_of_response_1() { return &___response_1; }
	inline void set_response_1(GSObject_t05160F6677D08913C6CD9AB7C7DD9454D3B12E88 * value)
	{
		___response_1 = value;
		Il2CppCodeGenWriteBarrier((&___response_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASSF_T76A25F8FA80AD871F1C384D17CF5F1E17D4B1066_H
#ifndef GSTYPEDREQUEST_2_TDADFF2FF1C9F068DA0F1FC1B8F0FFEFEE3FA1263_H
#define GSTYPEDREQUEST_2_TDADFF2FF1C9F068DA0F1FC1B8F0FFEFEE3FA1263_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Core.GSTypedRequest`2<GameSparks.Api.Requests.AccountDetailsRequest,GameSparks.Api.Responses.AccountDetailsResponse>
struct  GSTypedRequest_2_tDADFF2FF1C9F068DA0F1FC1B8F0FFEFEE3FA1263  : public RuntimeObject
{
public:
	// GameSparks.Core.GSRequest GameSparks.Core.GSTypedRequest`2::request
	GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 * ___request_0;

public:
	inline static int32_t get_offset_of_request_0() { return static_cast<int32_t>(offsetof(GSTypedRequest_2_tDADFF2FF1C9F068DA0F1FC1B8F0FFEFEE3FA1263, ___request_0)); }
	inline GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 * get_request_0() const { return ___request_0; }
	inline GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 ** get_address_of_request_0() { return &___request_0; }
	inline void set_request_0(GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 * value)
	{
		___request_0 = value;
		Il2CppCodeGenWriteBarrier((&___request_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GSTYPEDREQUEST_2_TDADFF2FF1C9F068DA0F1FC1B8F0FFEFEE3FA1263_H
#ifndef GSTYPEDREQUEST_2_T76721E61325821018639EDAC1DACD8DC530E12CF_H
#define GSTYPEDREQUEST_2_T76721E61325821018639EDAC1DACD8DC530E12CF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Core.GSTypedRequest`2<GameSparks.Api.Requests.AuthenticationRequest,GameSparks.Api.Responses.AuthenticationResponse>
struct  GSTypedRequest_2_t76721E61325821018639EDAC1DACD8DC530E12CF  : public RuntimeObject
{
public:
	// GameSparks.Core.GSRequest GameSparks.Core.GSTypedRequest`2::request
	GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 * ___request_0;

public:
	inline static int32_t get_offset_of_request_0() { return static_cast<int32_t>(offsetof(GSTypedRequest_2_t76721E61325821018639EDAC1DACD8DC530E12CF, ___request_0)); }
	inline GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 * get_request_0() const { return ___request_0; }
	inline GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 ** get_address_of_request_0() { return &___request_0; }
	inline void set_request_0(GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 * value)
	{
		___request_0 = value;
		Il2CppCodeGenWriteBarrier((&___request_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GSTYPEDREQUEST_2_T76721E61325821018639EDAC1DACD8DC530E12CF_H
#ifndef GSTYPEDREQUEST_2_T3BED7C9A709AE13322602D3AAF1CD3A66C43D55A_H
#define GSTYPEDREQUEST_2_T3BED7C9A709AE13322602D3AAF1CD3A66C43D55A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Core.GSTypedRequest`2<GameSparks.Api.Requests.ChangeUserDetailsRequest,GameSparks.Api.Responses.ChangeUserDetailsResponse>
struct  GSTypedRequest_2_t3BED7C9A709AE13322602D3AAF1CD3A66C43D55A  : public RuntimeObject
{
public:
	// GameSparks.Core.GSRequest GameSparks.Core.GSTypedRequest`2::request
	GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 * ___request_0;

public:
	inline static int32_t get_offset_of_request_0() { return static_cast<int32_t>(offsetof(GSTypedRequest_2_t3BED7C9A709AE13322602D3AAF1CD3A66C43D55A, ___request_0)); }
	inline GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 * get_request_0() const { return ___request_0; }
	inline GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 ** get_address_of_request_0() { return &___request_0; }
	inline void set_request_0(GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 * value)
	{
		___request_0 = value;
		Il2CppCodeGenWriteBarrier((&___request_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GSTYPEDREQUEST_2_T3BED7C9A709AE13322602D3AAF1CD3A66C43D55A_H
#ifndef GSTYPEDREQUEST_2_T49831775853B29B1576F89F5A7FBA08BF38C0BA7_H
#define GSTYPEDREQUEST_2_T49831775853B29B1576F89F5A7FBA08BF38C0BA7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Core.GSTypedRequest`2<GameSparks.Api.Requests.CreateTeamRequest,GameSparks.Api.Responses.CreateTeamResponse>
struct  GSTypedRequest_2_t49831775853B29B1576F89F5A7FBA08BF38C0BA7  : public RuntimeObject
{
public:
	// GameSparks.Core.GSRequest GameSparks.Core.GSTypedRequest`2::request
	GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 * ___request_0;

public:
	inline static int32_t get_offset_of_request_0() { return static_cast<int32_t>(offsetof(GSTypedRequest_2_t49831775853B29B1576F89F5A7FBA08BF38C0BA7, ___request_0)); }
	inline GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 * get_request_0() const { return ___request_0; }
	inline GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 ** get_address_of_request_0() { return &___request_0; }
	inline void set_request_0(GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 * value)
	{
		___request_0 = value;
		Il2CppCodeGenWriteBarrier((&___request_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GSTYPEDREQUEST_2_T49831775853B29B1576F89F5A7FBA08BF38C0BA7_H
#ifndef GSTYPEDREQUEST_2_T1B25DC09E3CA5D0AAFF45DD4841096451165378A_H
#define GSTYPEDREQUEST_2_T1B25DC09E3CA5D0AAFF45DD4841096451165378A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Core.GSTypedRequest`2<GameSparks.Api.Requests.DeviceAuthenticationRequest,GameSparks.Api.Responses.AuthenticationResponse>
struct  GSTypedRequest_2_t1B25DC09E3CA5D0AAFF45DD4841096451165378A  : public RuntimeObject
{
public:
	// GameSparks.Core.GSRequest GameSparks.Core.GSTypedRequest`2::request
	GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 * ___request_0;

public:
	inline static int32_t get_offset_of_request_0() { return static_cast<int32_t>(offsetof(GSTypedRequest_2_t1B25DC09E3CA5D0AAFF45DD4841096451165378A, ___request_0)); }
	inline GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 * get_request_0() const { return ___request_0; }
	inline GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 ** get_address_of_request_0() { return &___request_0; }
	inline void set_request_0(GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 * value)
	{
		___request_0 = value;
		Il2CppCodeGenWriteBarrier((&___request_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GSTYPEDREQUEST_2_T1B25DC09E3CA5D0AAFF45DD4841096451165378A_H
#ifndef GSTYPEDREQUEST_2_TF8647385A9FC5EF9611B236D20CF02B2CC6BB0E6_H
#define GSTYPEDREQUEST_2_TF8647385A9FC5EF9611B236D20CF02B2CC6BB0E6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Core.GSTypedRequest`2<GameSparks.Api.Requests.DismissMessageRequest,GameSparks.Api.Responses.DismissMessageResponse>
struct  GSTypedRequest_2_tF8647385A9FC5EF9611B236D20CF02B2CC6BB0E6  : public RuntimeObject
{
public:
	// GameSparks.Core.GSRequest GameSparks.Core.GSTypedRequest`2::request
	GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 * ___request_0;

public:
	inline static int32_t get_offset_of_request_0() { return static_cast<int32_t>(offsetof(GSTypedRequest_2_tF8647385A9FC5EF9611B236D20CF02B2CC6BB0E6, ___request_0)); }
	inline GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 * get_request_0() const { return ___request_0; }
	inline GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 ** get_address_of_request_0() { return &___request_0; }
	inline void set_request_0(GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 * value)
	{
		___request_0 = value;
		Il2CppCodeGenWriteBarrier((&___request_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GSTYPEDREQUEST_2_TF8647385A9FC5EF9611B236D20CF02B2CC6BB0E6_H
#ifndef GSTYPEDREQUEST_2_T9067AA6F28E83F9D4A59664D55DEC168C866931F_H
#define GSTYPEDREQUEST_2_T9067AA6F28E83F9D4A59664D55DEC168C866931F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Core.GSTypedRequest`2<GameSparks.Api.Requests.DropTeamRequest,GameSparks.Api.Responses.DropTeamResponse>
struct  GSTypedRequest_2_t9067AA6F28E83F9D4A59664D55DEC168C866931F  : public RuntimeObject
{
public:
	// GameSparks.Core.GSRequest GameSparks.Core.GSTypedRequest`2::request
	GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 * ___request_0;

public:
	inline static int32_t get_offset_of_request_0() { return static_cast<int32_t>(offsetof(GSTypedRequest_2_t9067AA6F28E83F9D4A59664D55DEC168C866931F, ___request_0)); }
	inline GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 * get_request_0() const { return ___request_0; }
	inline GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 ** get_address_of_request_0() { return &___request_0; }
	inline void set_request_0(GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 * value)
	{
		___request_0 = value;
		Il2CppCodeGenWriteBarrier((&___request_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GSTYPEDREQUEST_2_T9067AA6F28E83F9D4A59664D55DEC168C866931F_H
#ifndef GSTYPEDREQUEST_2_T058E09CE124F42A60802066807E6C98B649FB875_H
#define GSTYPEDREQUEST_2_T058E09CE124F42A60802066807E6C98B649FB875_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Core.GSTypedRequest`2<GameSparks.Api.Requests.EndSessionRequest,GameSparks.Api.Responses.EndSessionResponse>
struct  GSTypedRequest_2_t058E09CE124F42A60802066807E6C98B649FB875  : public RuntimeObject
{
public:
	// GameSparks.Core.GSRequest GameSparks.Core.GSTypedRequest`2::request
	GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 * ___request_0;

public:
	inline static int32_t get_offset_of_request_0() { return static_cast<int32_t>(offsetof(GSTypedRequest_2_t058E09CE124F42A60802066807E6C98B649FB875, ___request_0)); }
	inline GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 * get_request_0() const { return ___request_0; }
	inline GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 ** get_address_of_request_0() { return &___request_0; }
	inline void set_request_0(GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 * value)
	{
		___request_0 = value;
		Il2CppCodeGenWriteBarrier((&___request_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GSTYPEDREQUEST_2_T058E09CE124F42A60802066807E6C98B649FB875_H
#ifndef GSTYPEDREQUEST_2_T37F815E0380199B15F8EDDB80C40D087CC2F4214_H
#define GSTYPEDREQUEST_2_T37F815E0380199B15F8EDDB80C40D087CC2F4214_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Core.GSTypedRequest`2<GameSparks.Api.Requests.FacebookConnectRequest,GameSparks.Api.Responses.AuthenticationResponse>
struct  GSTypedRequest_2_t37F815E0380199B15F8EDDB80C40D087CC2F4214  : public RuntimeObject
{
public:
	// GameSparks.Core.GSRequest GameSparks.Core.GSTypedRequest`2::request
	GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 * ___request_0;

public:
	inline static int32_t get_offset_of_request_0() { return static_cast<int32_t>(offsetof(GSTypedRequest_2_t37F815E0380199B15F8EDDB80C40D087CC2F4214, ___request_0)); }
	inline GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 * get_request_0() const { return ___request_0; }
	inline GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 ** get_address_of_request_0() { return &___request_0; }
	inline void set_request_0(GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 * value)
	{
		___request_0 = value;
		Il2CppCodeGenWriteBarrier((&___request_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GSTYPEDREQUEST_2_T37F815E0380199B15F8EDDB80C40D087CC2F4214_H
#ifndef GSTYPEDREQUEST_2_T3F4032748C1F9CF67A28C023CA00C9B3F3E0090B_H
#define GSTYPEDREQUEST_2_T3F4032748C1F9CF67A28C023CA00C9B3F3E0090B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Core.GSTypedRequest`2<GameSparks.Api.Requests.GetDownloadableRequest,GameSparks.Api.Responses.GetDownloadableResponse>
struct  GSTypedRequest_2_t3F4032748C1F9CF67A28C023CA00C9B3F3E0090B  : public RuntimeObject
{
public:
	// GameSparks.Core.GSRequest GameSparks.Core.GSTypedRequest`2::request
	GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 * ___request_0;

public:
	inline static int32_t get_offset_of_request_0() { return static_cast<int32_t>(offsetof(GSTypedRequest_2_t3F4032748C1F9CF67A28C023CA00C9B3F3E0090B, ___request_0)); }
	inline GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 * get_request_0() const { return ___request_0; }
	inline GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 ** get_address_of_request_0() { return &___request_0; }
	inline void set_request_0(GSRequest_t12ABAE67A9A983F16B9D82B7050265C35911B1B7 * value)
	{
		___request_0 = value;
		Il2CppCodeGenWriteBarrier((&___request_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GSTYPEDREQUEST_2_T3F4032748C1F9CF67A28C023CA00C9B3F3E0090B_H
#ifndef GSTYPEDRESPONSE_T8D7A8C3784E1A1542ED006C5A97AF8C6D9E849DA_H
#define GSTYPEDRESPONSE_T8D7A8C3784E1A1542ED006C5A97AF8C6D9E849DA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Core.GSTypedResponse
struct  GSTypedResponse_t8D7A8C3784E1A1542ED006C5A97AF8C6D9E849DA  : public RuntimeObject
{
public:
	// GameSparks.Core.GSData GameSparks.Core.GSTypedResponse::response
	GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 * ___response_0;

public:
	inline static int32_t get_offset_of_response_0() { return static_cast<int32_t>(offsetof(GSTypedResponse_t8D7A8C3784E1A1542ED006C5A97AF8C6D9E849DA, ___response_0)); }
	inline GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 * get_response_0() const { return ___response_0; }
	inline GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 ** get_address_of_response_0() { return &___response_0; }
	inline void set_response_0(GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 * value)
	{
		___response_0 = value;
		Il2CppCodeGenWriteBarrier((&___response_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GSTYPEDRESPONSE_T8D7A8C3784E1A1542ED006C5A97AF8C6D9E849DA_H
#ifndef VERSION_T32E31145F06ABDA55EC856314710B188369A8132_H
#define VERSION_T32E31145F06ABDA55EC856314710B188369A8132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Core.Version
struct  Version_t32E31145F06ABDA55EC856314710B188369A8132  : public RuntimeObject
{
public:

public:
};

struct Version_t32E31145F06ABDA55EC856314710B188369A8132_StaticFields
{
public:
	// System.Version GameSparks.Core.Version::currentVersion
	Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD * ___currentVersion_0;
	// System.String GameSparks.Core.Version::buildType
	String_t* ___buildType_1;
	// System.String GameSparks.Core.Version::buildId
	String_t* ___buildId_2;

public:
	inline static int32_t get_offset_of_currentVersion_0() { return static_cast<int32_t>(offsetof(Version_t32E31145F06ABDA55EC856314710B188369A8132_StaticFields, ___currentVersion_0)); }
	inline Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD * get_currentVersion_0() const { return ___currentVersion_0; }
	inline Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD ** get_address_of_currentVersion_0() { return &___currentVersion_0; }
	inline void set_currentVersion_0(Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD * value)
	{
		___currentVersion_0 = value;
		Il2CppCodeGenWriteBarrier((&___currentVersion_0), value);
	}

	inline static int32_t get_offset_of_buildType_1() { return static_cast<int32_t>(offsetof(Version_t32E31145F06ABDA55EC856314710B188369A8132_StaticFields, ___buildType_1)); }
	inline String_t* get_buildType_1() const { return ___buildType_1; }
	inline String_t** get_address_of_buildType_1() { return &___buildType_1; }
	inline void set_buildType_1(String_t* value)
	{
		___buildType_1 = value;
		Il2CppCodeGenWriteBarrier((&___buildType_1), value);
	}

	inline static int32_t get_offset_of_buildId_2() { return static_cast<int32_t>(offsetof(Version_t32E31145F06ABDA55EC856314710B188369A8132_StaticFields, ___buildId_2)); }
	inline String_t* get_buildId_2() const { return ___buildId_2; }
	inline String_t** get_address_of_buildId_2() { return &___buildId_2; }
	inline void set_buildId_2(String_t* value)
	{
		___buildId_2 = value;
		Il2CppCodeGenWriteBarrier((&___buildId_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERSION_T32E31145F06ABDA55EC856314710B188369A8132_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef VERTEXHELPEREXTENSION_T593AF80A941B7208D2CC88AE671873CE6E2AA8E3_H
#define VERTEXHELPEREXTENSION_T593AF80A941B7208D2CC88AE671873CE6E2AA8E3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.UI.VertexHelperExtension
struct  VertexHelperExtension_t593AF80A941B7208D2CC88AE671873CE6E2AA8E3  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXHELPEREXTENSION_T593AF80A941B7208D2CC88AE671873CE6E2AA8E3_H
#ifndef BASEVERTEXEFFECT_T1EF95AB1FC33A027710E7DC86D19F700156C4F6A_H
#define BASEVERTEXEFFECT_T1EF95AB1FC33A027710E7DC86D19F700156C4F6A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.BaseVertexEffect
struct  BaseVertexEffect_t1EF95AB1FC33A027710E7DC86D19F700156C4F6A  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEVERTEXEFFECT_T1EF95AB1FC33A027710E7DC86D19F700156C4F6A_H
#ifndef U3CDELAYEDSETDIRTYU3EC__ITERATOR0_TB8BB61C2C033D95240B4E2704971663E4DC08073_H
#define U3CDELAYEDSETDIRTYU3EC__ITERATOR0_TB8BB61C2C033D95240B4E2704971663E4DC08073_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.LayoutGroup_<DelayedSetDirty>c__Iterator0
struct  U3CDelayedSetDirtyU3Ec__Iterator0_tB8BB61C2C033D95240B4E2704971663E4DC08073  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.LayoutGroup_<DelayedSetDirty>c__Iterator0::rectTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___rectTransform_0;
	// System.Object UnityEngine.UI.LayoutGroup_<DelayedSetDirty>c__Iterator0::U24current
	RuntimeObject * ___U24current_1;
	// System.Boolean UnityEngine.UI.LayoutGroup_<DelayedSetDirty>c__Iterator0::U24disposing
	bool ___U24disposing_2;
	// System.Int32 UnityEngine.UI.LayoutGroup_<DelayedSetDirty>c__Iterator0::U24PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_rectTransform_0() { return static_cast<int32_t>(offsetof(U3CDelayedSetDirtyU3Ec__Iterator0_tB8BB61C2C033D95240B4E2704971663E4DC08073, ___rectTransform_0)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_rectTransform_0() const { return ___rectTransform_0; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_rectTransform_0() { return &___rectTransform_0; }
	inline void set_rectTransform_0(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___rectTransform_0 = value;
		Il2CppCodeGenWriteBarrier((&___rectTransform_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CDelayedSetDirtyU3Ec__Iterator0_tB8BB61C2C033D95240B4E2704971663E4DC08073, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CDelayedSetDirtyU3Ec__Iterator0_tB8BB61C2C033D95240B4E2704971663E4DC08073, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CDelayedSetDirtyU3Ec__Iterator0_tB8BB61C2C033D95240B4E2704971663E4DC08073, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDELAYEDSETDIRTYU3EC__ITERATOR0_TB8BB61C2C033D95240B4E2704971663E4DC08073_H
#ifndef LAYOUTREBUILDER_T8D3501B43B1DE666140E2931FFA732B5B09EA5BD_H
#define LAYOUTREBUILDER_T8D3501B43B1DE666140E2931FFA732B5B09EA5BD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.LayoutRebuilder
struct  LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.LayoutRebuilder::m_ToRebuild
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_ToRebuild_0;
	// System.Int32 UnityEngine.UI.LayoutRebuilder::m_CachedHashFromTransform
	int32_t ___m_CachedHashFromTransform_1;

public:
	inline static int32_t get_offset_of_m_ToRebuild_0() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD, ___m_ToRebuild_0)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_ToRebuild_0() const { return ___m_ToRebuild_0; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_ToRebuild_0() { return &___m_ToRebuild_0; }
	inline void set_m_ToRebuild_0(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_ToRebuild_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ToRebuild_0), value);
	}

	inline static int32_t get_offset_of_m_CachedHashFromTransform_1() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD, ___m_CachedHashFromTransform_1)); }
	inline int32_t get_m_CachedHashFromTransform_1() const { return ___m_CachedHashFromTransform_1; }
	inline int32_t* get_address_of_m_CachedHashFromTransform_1() { return &___m_CachedHashFromTransform_1; }
	inline void set_m_CachedHashFromTransform_1(int32_t value)
	{
		___m_CachedHashFromTransform_1 = value;
	}
};

struct LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD_StaticFields
{
public:
	// UnityEngine.UI.ObjectPool`1<UnityEngine.UI.LayoutRebuilder> UnityEngine.UI.LayoutRebuilder::s_Rebuilders
	ObjectPool_1_tFA4F33849836CDB27432AE22249BB79D68619541 * ___s_Rebuilders_2;
	// UnityEngine.RectTransform_ReapplyDrivenProperties UnityEngine.UI.LayoutRebuilder::<>f__mgU24cache0
	ReapplyDrivenProperties_t431F4FBD9C59AE097FE33C4354CC6251B01B527D * ___U3CU3Ef__mgU24cache0_3;
	// System.Predicate`1<UnityEngine.Component> UnityEngine.UI.LayoutRebuilder::<>f__amU24cache0
	Predicate_1_t1A9CE8ADB6E9328794CC409FD5BEAACA86D7D769 * ___U3CU3Ef__amU24cache0_4;
	// UnityEngine.Events.UnityAction`1<UnityEngine.Component> UnityEngine.UI.LayoutRebuilder::<>f__amU24cache1
	UnityAction_1_tD07424D822F8527D240A320A1866C70C835C7C99 * ___U3CU3Ef__amU24cache1_5;
	// UnityEngine.Events.UnityAction`1<UnityEngine.Component> UnityEngine.UI.LayoutRebuilder::<>f__amU24cache2
	UnityAction_1_tD07424D822F8527D240A320A1866C70C835C7C99 * ___U3CU3Ef__amU24cache2_6;
	// UnityEngine.Events.UnityAction`1<UnityEngine.Component> UnityEngine.UI.LayoutRebuilder::<>f__amU24cache3
	UnityAction_1_tD07424D822F8527D240A320A1866C70C835C7C99 * ___U3CU3Ef__amU24cache3_7;
	// UnityEngine.Events.UnityAction`1<UnityEngine.Component> UnityEngine.UI.LayoutRebuilder::<>f__amU24cache4
	UnityAction_1_tD07424D822F8527D240A320A1866C70C835C7C99 * ___U3CU3Ef__amU24cache4_8;

public:
	inline static int32_t get_offset_of_s_Rebuilders_2() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD_StaticFields, ___s_Rebuilders_2)); }
	inline ObjectPool_1_tFA4F33849836CDB27432AE22249BB79D68619541 * get_s_Rebuilders_2() const { return ___s_Rebuilders_2; }
	inline ObjectPool_1_tFA4F33849836CDB27432AE22249BB79D68619541 ** get_address_of_s_Rebuilders_2() { return &___s_Rebuilders_2; }
	inline void set_s_Rebuilders_2(ObjectPool_1_tFA4F33849836CDB27432AE22249BB79D68619541 * value)
	{
		___s_Rebuilders_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Rebuilders_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_3() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD_StaticFields, ___U3CU3Ef__mgU24cache0_3)); }
	inline ReapplyDrivenProperties_t431F4FBD9C59AE097FE33C4354CC6251B01B527D * get_U3CU3Ef__mgU24cache0_3() const { return ___U3CU3Ef__mgU24cache0_3; }
	inline ReapplyDrivenProperties_t431F4FBD9C59AE097FE33C4354CC6251B01B527D ** get_address_of_U3CU3Ef__mgU24cache0_3() { return &___U3CU3Ef__mgU24cache0_3; }
	inline void set_U3CU3Ef__mgU24cache0_3(ReapplyDrivenProperties_t431F4FBD9C59AE097FE33C4354CC6251B01B527D * value)
	{
		___U3CU3Ef__mgU24cache0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_3), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_4() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD_StaticFields, ___U3CU3Ef__amU24cache0_4)); }
	inline Predicate_1_t1A9CE8ADB6E9328794CC409FD5BEAACA86D7D769 * get_U3CU3Ef__amU24cache0_4() const { return ___U3CU3Ef__amU24cache0_4; }
	inline Predicate_1_t1A9CE8ADB6E9328794CC409FD5BEAACA86D7D769 ** get_address_of_U3CU3Ef__amU24cache0_4() { return &___U3CU3Ef__amU24cache0_4; }
	inline void set_U3CU3Ef__amU24cache0_4(Predicate_1_t1A9CE8ADB6E9328794CC409FD5BEAACA86D7D769 * value)
	{
		___U3CU3Ef__amU24cache0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_5() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD_StaticFields, ___U3CU3Ef__amU24cache1_5)); }
	inline UnityAction_1_tD07424D822F8527D240A320A1866C70C835C7C99 * get_U3CU3Ef__amU24cache1_5() const { return ___U3CU3Ef__amU24cache1_5; }
	inline UnityAction_1_tD07424D822F8527D240A320A1866C70C835C7C99 ** get_address_of_U3CU3Ef__amU24cache1_5() { return &___U3CU3Ef__amU24cache1_5; }
	inline void set_U3CU3Ef__amU24cache1_5(UnityAction_1_tD07424D822F8527D240A320A1866C70C835C7C99 * value)
	{
		___U3CU3Ef__amU24cache1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_5), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_6() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD_StaticFields, ___U3CU3Ef__amU24cache2_6)); }
	inline UnityAction_1_tD07424D822F8527D240A320A1866C70C835C7C99 * get_U3CU3Ef__amU24cache2_6() const { return ___U3CU3Ef__amU24cache2_6; }
	inline UnityAction_1_tD07424D822F8527D240A320A1866C70C835C7C99 ** get_address_of_U3CU3Ef__amU24cache2_6() { return &___U3CU3Ef__amU24cache2_6; }
	inline void set_U3CU3Ef__amU24cache2_6(UnityAction_1_tD07424D822F8527D240A320A1866C70C835C7C99 * value)
	{
		___U3CU3Ef__amU24cache2_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_6), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_7() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD_StaticFields, ___U3CU3Ef__amU24cache3_7)); }
	inline UnityAction_1_tD07424D822F8527D240A320A1866C70C835C7C99 * get_U3CU3Ef__amU24cache3_7() const { return ___U3CU3Ef__amU24cache3_7; }
	inline UnityAction_1_tD07424D822F8527D240A320A1866C70C835C7C99 ** get_address_of_U3CU3Ef__amU24cache3_7() { return &___U3CU3Ef__amU24cache3_7; }
	inline void set_U3CU3Ef__amU24cache3_7(UnityAction_1_tD07424D822F8527D240A320A1866C70C835C7C99 * value)
	{
		___U3CU3Ef__amU24cache3_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache3_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_8() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD_StaticFields, ___U3CU3Ef__amU24cache4_8)); }
	inline UnityAction_1_tD07424D822F8527D240A320A1866C70C835C7C99 * get_U3CU3Ef__amU24cache4_8() const { return ___U3CU3Ef__amU24cache4_8; }
	inline UnityAction_1_tD07424D822F8527D240A320A1866C70C835C7C99 ** get_address_of_U3CU3Ef__amU24cache4_8() { return &___U3CU3Ef__amU24cache4_8; }
	inline void set_U3CU3Ef__amU24cache4_8(UnityAction_1_tD07424D822F8527D240A320A1866C70C835C7C99 * value)
	{
		___U3CU3Ef__amU24cache4_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache4_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYOUTREBUILDER_T8D3501B43B1DE666140E2931FFA732B5B09EA5BD_H
#ifndef LAYOUTUTILITY_T3B5074E34900DA384884FC50809EA395CB69E7D5_H
#define LAYOUTUTILITY_T3B5074E34900DA384884FC50809EA395CB69E7D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.LayoutUtility
struct  LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5  : public RuntimeObject
{
public:

public:
};

struct LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5_StaticFields
{
public:
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__amU24cache0
	Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * ___U3CU3Ef__amU24cache0_0;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__amU24cache1
	Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * ___U3CU3Ef__amU24cache1_1;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__amU24cache2
	Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * ___U3CU3Ef__amU24cache2_2;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__amU24cache3
	Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * ___U3CU3Ef__amU24cache3_3;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__amU24cache4
	Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * ___U3CU3Ef__amU24cache4_4;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__amU24cache5
	Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * ___U3CU3Ef__amU24cache5_5;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__amU24cache6
	Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * ___U3CU3Ef__amU24cache6_6;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__amU24cache7
	Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * ___U3CU3Ef__amU24cache7_7;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_0() { return static_cast<int32_t>(offsetof(LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5_StaticFields, ___U3CU3Ef__amU24cache0_0)); }
	inline Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * get_U3CU3Ef__amU24cache0_0() const { return ___U3CU3Ef__amU24cache0_0; }
	inline Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 ** get_address_of_U3CU3Ef__amU24cache0_0() { return &___U3CU3Ef__amU24cache0_0; }
	inline void set_U3CU3Ef__amU24cache0_0(Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * value)
	{
		___U3CU3Ef__amU24cache0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_1() { return static_cast<int32_t>(offsetof(LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5_StaticFields, ___U3CU3Ef__amU24cache1_1)); }
	inline Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * get_U3CU3Ef__amU24cache1_1() const { return ___U3CU3Ef__amU24cache1_1; }
	inline Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 ** get_address_of_U3CU3Ef__amU24cache1_1() { return &___U3CU3Ef__amU24cache1_1; }
	inline void set_U3CU3Ef__amU24cache1_1(Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * value)
	{
		___U3CU3Ef__amU24cache1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_1), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_2() { return static_cast<int32_t>(offsetof(LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5_StaticFields, ___U3CU3Ef__amU24cache2_2)); }
	inline Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * get_U3CU3Ef__amU24cache2_2() const { return ___U3CU3Ef__amU24cache2_2; }
	inline Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 ** get_address_of_U3CU3Ef__amU24cache2_2() { return &___U3CU3Ef__amU24cache2_2; }
	inline void set_U3CU3Ef__amU24cache2_2(Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * value)
	{
		___U3CU3Ef__amU24cache2_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_3() { return static_cast<int32_t>(offsetof(LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5_StaticFields, ___U3CU3Ef__amU24cache3_3)); }
	inline Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * get_U3CU3Ef__amU24cache3_3() const { return ___U3CU3Ef__amU24cache3_3; }
	inline Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 ** get_address_of_U3CU3Ef__amU24cache3_3() { return &___U3CU3Ef__amU24cache3_3; }
	inline void set_U3CU3Ef__amU24cache3_3(Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * value)
	{
		___U3CU3Ef__amU24cache3_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache3_3), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_4() { return static_cast<int32_t>(offsetof(LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5_StaticFields, ___U3CU3Ef__amU24cache4_4)); }
	inline Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * get_U3CU3Ef__amU24cache4_4() const { return ___U3CU3Ef__amU24cache4_4; }
	inline Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 ** get_address_of_U3CU3Ef__amU24cache4_4() { return &___U3CU3Ef__amU24cache4_4; }
	inline void set_U3CU3Ef__amU24cache4_4(Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * value)
	{
		___U3CU3Ef__amU24cache4_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache4_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5_5() { return static_cast<int32_t>(offsetof(LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5_StaticFields, ___U3CU3Ef__amU24cache5_5)); }
	inline Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * get_U3CU3Ef__amU24cache5_5() const { return ___U3CU3Ef__amU24cache5_5; }
	inline Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 ** get_address_of_U3CU3Ef__amU24cache5_5() { return &___U3CU3Ef__amU24cache5_5; }
	inline void set_U3CU3Ef__amU24cache5_5(Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * value)
	{
		___U3CU3Ef__amU24cache5_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache5_5), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache6_6() { return static_cast<int32_t>(offsetof(LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5_StaticFields, ___U3CU3Ef__amU24cache6_6)); }
	inline Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * get_U3CU3Ef__amU24cache6_6() const { return ___U3CU3Ef__amU24cache6_6; }
	inline Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 ** get_address_of_U3CU3Ef__amU24cache6_6() { return &___U3CU3Ef__amU24cache6_6; }
	inline void set_U3CU3Ef__amU24cache6_6(Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * value)
	{
		___U3CU3Ef__amU24cache6_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache6_6), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache7_7() { return static_cast<int32_t>(offsetof(LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5_StaticFields, ___U3CU3Ef__amU24cache7_7)); }
	inline Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * get_U3CU3Ef__amU24cache7_7() const { return ___U3CU3Ef__amU24cache7_7; }
	inline Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 ** get_address_of_U3CU3Ef__amU24cache7_7() { return &___U3CU3Ef__amU24cache7_7; }
	inline void set_U3CU3Ef__amU24cache7_7(Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * value)
	{
		___U3CU3Ef__amU24cache7_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache7_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYOUTUTILITY_T3B5074E34900DA384884FC50809EA395CB69E7D5_H
#ifndef REFLECTIONMETHODSCACHE_TBDADDC80D50C5F10BD00965217980B9A8D24BE8A_H
#define REFLECTIONMETHODSCACHE_TBDADDC80D50C5F10BD00965217980B9A8D24BE8A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache
struct  ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A  : public RuntimeObject
{
public:
	// UnityEngine.UI.ReflectionMethodsCache_Raycast3DCallback UnityEngine.UI.ReflectionMethodsCache::raycast3D
	Raycast3DCallback_t83483916473C9710AEDB316A65CBE62C58935C5F * ___raycast3D_0;
	// UnityEngine.UI.ReflectionMethodsCache_RaycastAllCallback UnityEngine.UI.ReflectionMethodsCache::raycast3DAll
	RaycastAllCallback_t751407A44270E02FAA43D0846A58EE6A8C4AE1CE * ___raycast3DAll_1;
	// UnityEngine.UI.ReflectionMethodsCache_Raycast2DCallback UnityEngine.UI.ReflectionMethodsCache::raycast2D
	Raycast2DCallback_tE99ABF9ABC3A380677949E8C05A3E477889B82BE * ___raycast2D_2;
	// UnityEngine.UI.ReflectionMethodsCache_GetRayIntersectionAllCallback UnityEngine.UI.ReflectionMethodsCache::getRayIntersectionAll
	GetRayIntersectionAllCallback_t68C2581CCF05E868297EBD3F3361274954845095 * ___getRayIntersectionAll_3;
	// UnityEngine.UI.ReflectionMethodsCache_GetRayIntersectionAllNonAllocCallback UnityEngine.UI.ReflectionMethodsCache::getRayIntersectionAllNonAlloc
	GetRayIntersectionAllNonAllocCallback_tAD7508D45DB6679B6394983579AD18D967CC2AD4 * ___getRayIntersectionAllNonAlloc_4;
	// UnityEngine.UI.ReflectionMethodsCache_GetRaycastNonAllocCallback UnityEngine.UI.ReflectionMethodsCache::getRaycastNonAlloc
	GetRaycastNonAllocCallback_tC13D9767CFF00EAB26E9FCC4BDD505F0721A2B4D * ___getRaycastNonAlloc_5;

public:
	inline static int32_t get_offset_of_raycast3D_0() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A, ___raycast3D_0)); }
	inline Raycast3DCallback_t83483916473C9710AEDB316A65CBE62C58935C5F * get_raycast3D_0() const { return ___raycast3D_0; }
	inline Raycast3DCallback_t83483916473C9710AEDB316A65CBE62C58935C5F ** get_address_of_raycast3D_0() { return &___raycast3D_0; }
	inline void set_raycast3D_0(Raycast3DCallback_t83483916473C9710AEDB316A65CBE62C58935C5F * value)
	{
		___raycast3D_0 = value;
		Il2CppCodeGenWriteBarrier((&___raycast3D_0), value);
	}

	inline static int32_t get_offset_of_raycast3DAll_1() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A, ___raycast3DAll_1)); }
	inline RaycastAllCallback_t751407A44270E02FAA43D0846A58EE6A8C4AE1CE * get_raycast3DAll_1() const { return ___raycast3DAll_1; }
	inline RaycastAllCallback_t751407A44270E02FAA43D0846A58EE6A8C4AE1CE ** get_address_of_raycast3DAll_1() { return &___raycast3DAll_1; }
	inline void set_raycast3DAll_1(RaycastAllCallback_t751407A44270E02FAA43D0846A58EE6A8C4AE1CE * value)
	{
		___raycast3DAll_1 = value;
		Il2CppCodeGenWriteBarrier((&___raycast3DAll_1), value);
	}

	inline static int32_t get_offset_of_raycast2D_2() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A, ___raycast2D_2)); }
	inline Raycast2DCallback_tE99ABF9ABC3A380677949E8C05A3E477889B82BE * get_raycast2D_2() const { return ___raycast2D_2; }
	inline Raycast2DCallback_tE99ABF9ABC3A380677949E8C05A3E477889B82BE ** get_address_of_raycast2D_2() { return &___raycast2D_2; }
	inline void set_raycast2D_2(Raycast2DCallback_tE99ABF9ABC3A380677949E8C05A3E477889B82BE * value)
	{
		___raycast2D_2 = value;
		Il2CppCodeGenWriteBarrier((&___raycast2D_2), value);
	}

	inline static int32_t get_offset_of_getRayIntersectionAll_3() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A, ___getRayIntersectionAll_3)); }
	inline GetRayIntersectionAllCallback_t68C2581CCF05E868297EBD3F3361274954845095 * get_getRayIntersectionAll_3() const { return ___getRayIntersectionAll_3; }
	inline GetRayIntersectionAllCallback_t68C2581CCF05E868297EBD3F3361274954845095 ** get_address_of_getRayIntersectionAll_3() { return &___getRayIntersectionAll_3; }
	inline void set_getRayIntersectionAll_3(GetRayIntersectionAllCallback_t68C2581CCF05E868297EBD3F3361274954845095 * value)
	{
		___getRayIntersectionAll_3 = value;
		Il2CppCodeGenWriteBarrier((&___getRayIntersectionAll_3), value);
	}

	inline static int32_t get_offset_of_getRayIntersectionAllNonAlloc_4() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A, ___getRayIntersectionAllNonAlloc_4)); }
	inline GetRayIntersectionAllNonAllocCallback_tAD7508D45DB6679B6394983579AD18D967CC2AD4 * get_getRayIntersectionAllNonAlloc_4() const { return ___getRayIntersectionAllNonAlloc_4; }
	inline GetRayIntersectionAllNonAllocCallback_tAD7508D45DB6679B6394983579AD18D967CC2AD4 ** get_address_of_getRayIntersectionAllNonAlloc_4() { return &___getRayIntersectionAllNonAlloc_4; }
	inline void set_getRayIntersectionAllNonAlloc_4(GetRayIntersectionAllNonAllocCallback_tAD7508D45DB6679B6394983579AD18D967CC2AD4 * value)
	{
		___getRayIntersectionAllNonAlloc_4 = value;
		Il2CppCodeGenWriteBarrier((&___getRayIntersectionAllNonAlloc_4), value);
	}

	inline static int32_t get_offset_of_getRaycastNonAlloc_5() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A, ___getRaycastNonAlloc_5)); }
	inline GetRaycastNonAllocCallback_tC13D9767CFF00EAB26E9FCC4BDD505F0721A2B4D * get_getRaycastNonAlloc_5() const { return ___getRaycastNonAlloc_5; }
	inline GetRaycastNonAllocCallback_tC13D9767CFF00EAB26E9FCC4BDD505F0721A2B4D ** get_address_of_getRaycastNonAlloc_5() { return &___getRaycastNonAlloc_5; }
	inline void set_getRaycastNonAlloc_5(GetRaycastNonAllocCallback_tC13D9767CFF00EAB26E9FCC4BDD505F0721A2B4D * value)
	{
		___getRaycastNonAlloc_5 = value;
		Il2CppCodeGenWriteBarrier((&___getRaycastNonAlloc_5), value);
	}
};

struct ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A_StaticFields
{
public:
	// UnityEngine.UI.ReflectionMethodsCache UnityEngine.UI.ReflectionMethodsCache::s_ReflectionMethodsCache
	ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A * ___s_ReflectionMethodsCache_6;

public:
	inline static int32_t get_offset_of_s_ReflectionMethodsCache_6() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A_StaticFields, ___s_ReflectionMethodsCache_6)); }
	inline ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A * get_s_ReflectionMethodsCache_6() const { return ___s_ReflectionMethodsCache_6; }
	inline ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A ** get_address_of_s_ReflectionMethodsCache_6() { return &___s_ReflectionMethodsCache_6; }
	inline void set_s_ReflectionMethodsCache_6(ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A * value)
	{
		___s_ReflectionMethodsCache_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_ReflectionMethodsCache_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFLECTIONMETHODSCACHE_TBDADDC80D50C5F10BD00965217980B9A8D24BE8A_H
#ifndef U24ARRAYTYPEU3D12_T25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199_H
#define U24ARRAYTYPEU3D12_T25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>_U24ArrayTypeU3D12
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D12_t25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D12_t25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199__padding[12];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D12_T25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199_H
#ifndef GSMESSAGE_T7604A625444D075A08A76A8029C3AE5534733F7E_H
#define GSMESSAGE_T7604A625444D075A08A76A8029C3AE5534733F7E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Messages.GSMessage
struct  GSMessage_t7604A625444D075A08A76A8029C3AE5534733F7E  : public GSTypedResponse_t8D7A8C3784E1A1542ED006C5A97AF8C6D9E849DA
{
public:
	// GameSparks.Core.GSInstance GameSparks.Api.Messages.GSMessage::<gsInstance>k__BackingField
	GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774 * ___U3CgsInstanceU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CgsInstanceU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(GSMessage_t7604A625444D075A08A76A8029C3AE5534733F7E, ___U3CgsInstanceU3Ek__BackingField_2)); }
	inline GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774 * get_U3CgsInstanceU3Ek__BackingField_2() const { return ___U3CgsInstanceU3Ek__BackingField_2; }
	inline GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774 ** get_address_of_U3CgsInstanceU3Ek__BackingField_2() { return &___U3CgsInstanceU3Ek__BackingField_2; }
	inline void set_U3CgsInstanceU3Ek__BackingField_2(GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774 * value)
	{
		___U3CgsInstanceU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CgsInstanceU3Ek__BackingField_2), value);
	}
};

struct GSMessage_t7604A625444D075A08A76A8029C3AE5534733F7E_StaticFields
{
public:
	// System.Collections.Generic.IDictionary`2<System.String,System.Func`2<GameSparks.Core.GSData,GameSparks.Api.Messages.GSMessage>> GameSparks.Api.Messages.GSMessage::handlers
	RuntimeObject* ___handlers_1;

public:
	inline static int32_t get_offset_of_handlers_1() { return static_cast<int32_t>(offsetof(GSMessage_t7604A625444D075A08A76A8029C3AE5534733F7E_StaticFields, ___handlers_1)); }
	inline RuntimeObject* get_handlers_1() const { return ___handlers_1; }
	inline RuntimeObject** get_address_of_handlers_1() { return &___handlers_1; }
	inline void set_handlers_1(RuntimeObject* value)
	{
		___handlers_1 = value;
		Il2CppCodeGenWriteBarrier((&___handlers_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GSMESSAGE_T7604A625444D075A08A76A8029C3AE5534733F7E_H
#ifndef ACCOUNTDETAILSREQUEST_TBA824708CEB3983B458DD780CA9345B77461D6BA_H
#define ACCOUNTDETAILSREQUEST_TBA824708CEB3983B458DD780CA9345B77461D6BA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Requests.AccountDetailsRequest
struct  AccountDetailsRequest_tBA824708CEB3983B458DD780CA9345B77461D6BA  : public GSTypedRequest_2_tDADFF2FF1C9F068DA0F1FC1B8F0FFEFEE3FA1263
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACCOUNTDETAILSREQUEST_TBA824708CEB3983B458DD780CA9345B77461D6BA_H
#ifndef AUTHENTICATIONREQUEST_T48096CAA649171B606B7E660EB131CF67C0FDFC5_H
#define AUTHENTICATIONREQUEST_T48096CAA649171B606B7E660EB131CF67C0FDFC5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Requests.AuthenticationRequest
struct  AuthenticationRequest_t48096CAA649171B606B7E660EB131CF67C0FDFC5  : public GSTypedRequest_2_t76721E61325821018639EDAC1DACD8DC530E12CF
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTHENTICATIONREQUEST_T48096CAA649171B606B7E660EB131CF67C0FDFC5_H
#ifndef CHANGEUSERDETAILSREQUEST_TBA8A275D4D08B06D3CBB5E88BF65EAA2D7E8057B_H
#define CHANGEUSERDETAILSREQUEST_TBA8A275D4D08B06D3CBB5E88BF65EAA2D7E8057B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Requests.ChangeUserDetailsRequest
struct  ChangeUserDetailsRequest_tBA8A275D4D08B06D3CBB5E88BF65EAA2D7E8057B  : public GSTypedRequest_2_t3BED7C9A709AE13322602D3AAF1CD3A66C43D55A
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHANGEUSERDETAILSREQUEST_TBA8A275D4D08B06D3CBB5E88BF65EAA2D7E8057B_H
#ifndef CREATETEAMREQUEST_T753F9189B1005E3D3C06FB1E3F39AD95B2A2E213_H
#define CREATETEAMREQUEST_T753F9189B1005E3D3C06FB1E3F39AD95B2A2E213_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Requests.CreateTeamRequest
struct  CreateTeamRequest_t753F9189B1005E3D3C06FB1E3F39AD95B2A2E213  : public GSTypedRequest_2_t49831775853B29B1576F89F5A7FBA08BF38C0BA7
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CREATETEAMREQUEST_T753F9189B1005E3D3C06FB1E3F39AD95B2A2E213_H
#ifndef DEVICEAUTHENTICATIONREQUEST_TE82A29B79E3FAEE538296B9C6A4D0BFDF1590631_H
#define DEVICEAUTHENTICATIONREQUEST_TE82A29B79E3FAEE538296B9C6A4D0BFDF1590631_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Requests.DeviceAuthenticationRequest
struct  DeviceAuthenticationRequest_tE82A29B79E3FAEE538296B9C6A4D0BFDF1590631  : public GSTypedRequest_2_t1B25DC09E3CA5D0AAFF45DD4841096451165378A
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEVICEAUTHENTICATIONREQUEST_TE82A29B79E3FAEE538296B9C6A4D0BFDF1590631_H
#ifndef DISMISSMESSAGEREQUEST_T3BA959D76856A74D62362A269C0857774D37D748_H
#define DISMISSMESSAGEREQUEST_T3BA959D76856A74D62362A269C0857774D37D748_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Requests.DismissMessageRequest
struct  DismissMessageRequest_t3BA959D76856A74D62362A269C0857774D37D748  : public GSTypedRequest_2_tF8647385A9FC5EF9611B236D20CF02B2CC6BB0E6
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISMISSMESSAGEREQUEST_T3BA959D76856A74D62362A269C0857774D37D748_H
#ifndef DROPTEAMREQUEST_T271A4FCDF247EF7AD01D42403A8BF41281533F0B_H
#define DROPTEAMREQUEST_T271A4FCDF247EF7AD01D42403A8BF41281533F0B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Requests.DropTeamRequest
struct  DropTeamRequest_t271A4FCDF247EF7AD01D42403A8BF41281533F0B  : public GSTypedRequest_2_t9067AA6F28E83F9D4A59664D55DEC168C866931F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DROPTEAMREQUEST_T271A4FCDF247EF7AD01D42403A8BF41281533F0B_H
#ifndef ENDSESSIONREQUEST_T806E19284145380C0157A3089F19D5829C829FC9_H
#define ENDSESSIONREQUEST_T806E19284145380C0157A3089F19D5829C829FC9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Requests.EndSessionRequest
struct  EndSessionRequest_t806E19284145380C0157A3089F19D5829C829FC9  : public GSTypedRequest_2_t058E09CE124F42A60802066807E6C98B649FB875
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENDSESSIONREQUEST_T806E19284145380C0157A3089F19D5829C829FC9_H
#ifndef FACEBOOKCONNECTREQUEST_T0CA6D1284233B489069B6D180E1E141107D7BEE9_H
#define FACEBOOKCONNECTREQUEST_T0CA6D1284233B489069B6D180E1E141107D7BEE9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Requests.FacebookConnectRequest
struct  FacebookConnectRequest_t0CA6D1284233B489069B6D180E1E141107D7BEE9  : public GSTypedRequest_2_t37F815E0380199B15F8EDDB80C40D087CC2F4214
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACEBOOKCONNECTREQUEST_T0CA6D1284233B489069B6D180E1E141107D7BEE9_H
#ifndef GETDOWNLOADABLEREQUEST_T6B67859C4DC532380A0A4C8A3C5C7AA30600F751_H
#define GETDOWNLOADABLEREQUEST_T6B67859C4DC532380A0A4C8A3C5C7AA30600F751_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Requests.GetDownloadableRequest
struct  GetDownloadableRequest_t6B67859C4DC532380A0A4C8A3C5C7AA30600F751  : public GSTypedRequest_2_t3F4032748C1F9CF67A28C023CA00C9B3F3E0090B
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETDOWNLOADABLEREQUEST_T6B67859C4DC532380A0A4C8A3C5C7AA30600F751_H
#ifndef GSREQUESTDATA_T84713ABAAB257F300A1DB26F09C91AB0519109F8_H
#define GSREQUESTDATA_T84713ABAAB257F300A1DB26F09C91AB0519109F8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Core.GSRequestData
struct  GSRequestData_t84713ABAAB257F300A1DB26F09C91AB0519109F8  : public GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GSREQUESTDATA_T84713ABAAB257F300A1DB26F09C91AB0519109F8_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#define INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#define SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#define COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifndef DRIVENRECTTRANSFORMTRACKER_TB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03_H
#define DRIVENRECTTRANSFORMTRACKER_TB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.DrivenRectTransformTracker
struct  DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03 
{
public:
	union
	{
		struct
		{
		};
		uint8_t DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRIVENRECTTRANSFORMTRACKER_TB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef VECTOR4_TD148D6428C3F8FF6CD998F82090113C2B490B76E_H
#define VECTOR4_TD148D6428C3F8FF6CD998F82090113C2B490B76E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___zeroVector_5)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___oneVector_6)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_TD148D6428C3F8FF6CD998F82090113C2B490B76E_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_TC8332394FBFEEB4B73459A35E182942340DA3537_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_TC8332394FBFEEB4B73459A35E182942340DA3537_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_tC8332394FBFEEB4B73459A35E182942340DA3537  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_tC8332394FBFEEB4B73459A35E182942340DA3537_StaticFields
{
public:
	// <PrivateImplementationDetails>_U24ArrayTypeU3D12 <PrivateImplementationDetails>::U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46
	U24ArrayTypeU3D12_t25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199  ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0;

public:
	inline static int32_t get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC8332394FBFEEB4B73459A35E182942340DA3537_StaticFields, ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0)); }
	inline U24ArrayTypeU3D12_t25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199  get_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() const { return ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline U24ArrayTypeU3D12_t25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199 * get_address_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return &___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline void set_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(U24ArrayTypeU3D12_t25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199  value)
	{
		___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_TC8332394FBFEEB4B73459A35E182942340DA3537_H
#ifndef ACHIEVEMENTEARNEDMESSAGE_T04D1FB4BB1949FC0701210628065D11CBA6A3B44_H
#define ACHIEVEMENTEARNEDMESSAGE_T04D1FB4BB1949FC0701210628065D11CBA6A3B44_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Messages.AchievementEarnedMessage
struct  AchievementEarnedMessage_t04D1FB4BB1949FC0701210628065D11CBA6A3B44  : public GSMessage_t7604A625444D075A08A76A8029C3AE5534733F7E
{
public:

public:
};

struct AchievementEarnedMessage_t04D1FB4BB1949FC0701210628065D11CBA6A3B44_StaticFields
{
public:
	// System.Action`1<GameSparks.Api.Messages.AchievementEarnedMessage> GameSparks.Api.Messages.AchievementEarnedMessage::Listener
	Action_1_tB665E708BEF44CD901EEA6128DCCD2BE761E3DD2 * ___Listener_3;

public:
	inline static int32_t get_offset_of_Listener_3() { return static_cast<int32_t>(offsetof(AchievementEarnedMessage_t04D1FB4BB1949FC0701210628065D11CBA6A3B44_StaticFields, ___Listener_3)); }
	inline Action_1_tB665E708BEF44CD901EEA6128DCCD2BE761E3DD2 * get_Listener_3() const { return ___Listener_3; }
	inline Action_1_tB665E708BEF44CD901EEA6128DCCD2BE761E3DD2 ** get_address_of_Listener_3() { return &___Listener_3; }
	inline void set_Listener_3(Action_1_tB665E708BEF44CD901EEA6128DCCD2BE761E3DD2 * value)
	{
		___Listener_3 = value;
		Il2CppCodeGenWriteBarrier((&___Listener_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACHIEVEMENTEARNEDMESSAGE_T04D1FB4BB1949FC0701210628065D11CBA6A3B44_H
#ifndef CHALLENGEACCEPTEDMESSAGE_T1C5FABDC00005A13C574E2D3B493001CE59B8121_H
#define CHALLENGEACCEPTEDMESSAGE_T1C5FABDC00005A13C574E2D3B493001CE59B8121_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Messages.ChallengeAcceptedMessage
struct  ChallengeAcceptedMessage_t1C5FABDC00005A13C574E2D3B493001CE59B8121  : public GSMessage_t7604A625444D075A08A76A8029C3AE5534733F7E
{
public:

public:
};

struct ChallengeAcceptedMessage_t1C5FABDC00005A13C574E2D3B493001CE59B8121_StaticFields
{
public:
	// System.Action`1<GameSparks.Api.Messages.ChallengeAcceptedMessage> GameSparks.Api.Messages.ChallengeAcceptedMessage::Listener
	Action_1_t56A5FB45DDBD6FC649B2F2EA894463595DA6F019 * ___Listener_3;

public:
	inline static int32_t get_offset_of_Listener_3() { return static_cast<int32_t>(offsetof(ChallengeAcceptedMessage_t1C5FABDC00005A13C574E2D3B493001CE59B8121_StaticFields, ___Listener_3)); }
	inline Action_1_t56A5FB45DDBD6FC649B2F2EA894463595DA6F019 * get_Listener_3() const { return ___Listener_3; }
	inline Action_1_t56A5FB45DDBD6FC649B2F2EA894463595DA6F019 ** get_address_of_Listener_3() { return &___Listener_3; }
	inline void set_Listener_3(Action_1_t56A5FB45DDBD6FC649B2F2EA894463595DA6F019 * value)
	{
		___Listener_3 = value;
		Il2CppCodeGenWriteBarrier((&___Listener_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHALLENGEACCEPTEDMESSAGE_T1C5FABDC00005A13C574E2D3B493001CE59B8121_H
#ifndef CHALLENGECHANGEDMESSAGE_T60942905C25ECF06313DA7327B35F2AE96028A79_H
#define CHALLENGECHANGEDMESSAGE_T60942905C25ECF06313DA7327B35F2AE96028A79_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Messages.ChallengeChangedMessage
struct  ChallengeChangedMessage_t60942905C25ECF06313DA7327B35F2AE96028A79  : public GSMessage_t7604A625444D075A08A76A8029C3AE5534733F7E
{
public:

public:
};

struct ChallengeChangedMessage_t60942905C25ECF06313DA7327B35F2AE96028A79_StaticFields
{
public:
	// System.Action`1<GameSparks.Api.Messages.ChallengeChangedMessage> GameSparks.Api.Messages.ChallengeChangedMessage::Listener
	Action_1_tC6C5B047DA5A06F8E60472987F317DC620A48C0F * ___Listener_3;

public:
	inline static int32_t get_offset_of_Listener_3() { return static_cast<int32_t>(offsetof(ChallengeChangedMessage_t60942905C25ECF06313DA7327B35F2AE96028A79_StaticFields, ___Listener_3)); }
	inline Action_1_tC6C5B047DA5A06F8E60472987F317DC620A48C0F * get_Listener_3() const { return ___Listener_3; }
	inline Action_1_tC6C5B047DA5A06F8E60472987F317DC620A48C0F ** get_address_of_Listener_3() { return &___Listener_3; }
	inline void set_Listener_3(Action_1_tC6C5B047DA5A06F8E60472987F317DC620A48C0F * value)
	{
		___Listener_3 = value;
		Il2CppCodeGenWriteBarrier((&___Listener_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHALLENGECHANGEDMESSAGE_T60942905C25ECF06313DA7327B35F2AE96028A79_H
#ifndef CHALLENGECHATMESSAGE_TBA97BA8BA44ABD87265FE6BF4C169CF25DB1B466_H
#define CHALLENGECHATMESSAGE_TBA97BA8BA44ABD87265FE6BF4C169CF25DB1B466_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Messages.ChallengeChatMessage
struct  ChallengeChatMessage_tBA97BA8BA44ABD87265FE6BF4C169CF25DB1B466  : public GSMessage_t7604A625444D075A08A76A8029C3AE5534733F7E
{
public:

public:
};

struct ChallengeChatMessage_tBA97BA8BA44ABD87265FE6BF4C169CF25DB1B466_StaticFields
{
public:
	// System.Action`1<GameSparks.Api.Messages.ChallengeChatMessage> GameSparks.Api.Messages.ChallengeChatMessage::Listener
	Action_1_t3750D516B51391094A17D892AE18EB7DFCC8965F * ___Listener_3;

public:
	inline static int32_t get_offset_of_Listener_3() { return static_cast<int32_t>(offsetof(ChallengeChatMessage_tBA97BA8BA44ABD87265FE6BF4C169CF25DB1B466_StaticFields, ___Listener_3)); }
	inline Action_1_t3750D516B51391094A17D892AE18EB7DFCC8965F * get_Listener_3() const { return ___Listener_3; }
	inline Action_1_t3750D516B51391094A17D892AE18EB7DFCC8965F ** get_address_of_Listener_3() { return &___Listener_3; }
	inline void set_Listener_3(Action_1_t3750D516B51391094A17D892AE18EB7DFCC8965F * value)
	{
		___Listener_3 = value;
		Il2CppCodeGenWriteBarrier((&___Listener_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHALLENGECHATMESSAGE_TBA97BA8BA44ABD87265FE6BF4C169CF25DB1B466_H
#ifndef CHALLENGEDECLINEDMESSAGE_T8E31D2629D3F244241E2179407245F3E10DA4D94_H
#define CHALLENGEDECLINEDMESSAGE_T8E31D2629D3F244241E2179407245F3E10DA4D94_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Messages.ChallengeDeclinedMessage
struct  ChallengeDeclinedMessage_t8E31D2629D3F244241E2179407245F3E10DA4D94  : public GSMessage_t7604A625444D075A08A76A8029C3AE5534733F7E
{
public:

public:
};

struct ChallengeDeclinedMessage_t8E31D2629D3F244241E2179407245F3E10DA4D94_StaticFields
{
public:
	// System.Action`1<GameSparks.Api.Messages.ChallengeDeclinedMessage> GameSparks.Api.Messages.ChallengeDeclinedMessage::Listener
	Action_1_tBA5878305F9CAA3DEE372DF8D6D748A0BD358E32 * ___Listener_3;

public:
	inline static int32_t get_offset_of_Listener_3() { return static_cast<int32_t>(offsetof(ChallengeDeclinedMessage_t8E31D2629D3F244241E2179407245F3E10DA4D94_StaticFields, ___Listener_3)); }
	inline Action_1_tBA5878305F9CAA3DEE372DF8D6D748A0BD358E32 * get_Listener_3() const { return ___Listener_3; }
	inline Action_1_tBA5878305F9CAA3DEE372DF8D6D748A0BD358E32 ** get_address_of_Listener_3() { return &___Listener_3; }
	inline void set_Listener_3(Action_1_tBA5878305F9CAA3DEE372DF8D6D748A0BD358E32 * value)
	{
		___Listener_3 = value;
		Il2CppCodeGenWriteBarrier((&___Listener_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHALLENGEDECLINEDMESSAGE_T8E31D2629D3F244241E2179407245F3E10DA4D94_H
#ifndef CHALLENGEDRAWNMESSAGE_T7997954B69A0D09E76C2B1E120F1D88795F05E0C_H
#define CHALLENGEDRAWNMESSAGE_T7997954B69A0D09E76C2B1E120F1D88795F05E0C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Messages.ChallengeDrawnMessage
struct  ChallengeDrawnMessage_t7997954B69A0D09E76C2B1E120F1D88795F05E0C  : public GSMessage_t7604A625444D075A08A76A8029C3AE5534733F7E
{
public:

public:
};

struct ChallengeDrawnMessage_t7997954B69A0D09E76C2B1E120F1D88795F05E0C_StaticFields
{
public:
	// System.Action`1<GameSparks.Api.Messages.ChallengeDrawnMessage> GameSparks.Api.Messages.ChallengeDrawnMessage::Listener
	Action_1_tD683FB0D9C0B32681C7C27B1A17D934BC8ECDA5C * ___Listener_3;

public:
	inline static int32_t get_offset_of_Listener_3() { return static_cast<int32_t>(offsetof(ChallengeDrawnMessage_t7997954B69A0D09E76C2B1E120F1D88795F05E0C_StaticFields, ___Listener_3)); }
	inline Action_1_tD683FB0D9C0B32681C7C27B1A17D934BC8ECDA5C * get_Listener_3() const { return ___Listener_3; }
	inline Action_1_tD683FB0D9C0B32681C7C27B1A17D934BC8ECDA5C ** get_address_of_Listener_3() { return &___Listener_3; }
	inline void set_Listener_3(Action_1_tD683FB0D9C0B32681C7C27B1A17D934BC8ECDA5C * value)
	{
		___Listener_3 = value;
		Il2CppCodeGenWriteBarrier((&___Listener_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHALLENGEDRAWNMESSAGE_T7997954B69A0D09E76C2B1E120F1D88795F05E0C_H
#ifndef CHALLENGEEXPIREDMESSAGE_TD4CCAD5DBF84E0C66C7AFB32AFE92CB71482AAFB_H
#define CHALLENGEEXPIREDMESSAGE_TD4CCAD5DBF84E0C66C7AFB32AFE92CB71482AAFB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Messages.ChallengeExpiredMessage
struct  ChallengeExpiredMessage_tD4CCAD5DBF84E0C66C7AFB32AFE92CB71482AAFB  : public GSMessage_t7604A625444D075A08A76A8029C3AE5534733F7E
{
public:

public:
};

struct ChallengeExpiredMessage_tD4CCAD5DBF84E0C66C7AFB32AFE92CB71482AAFB_StaticFields
{
public:
	// System.Action`1<GameSparks.Api.Messages.ChallengeExpiredMessage> GameSparks.Api.Messages.ChallengeExpiredMessage::Listener
	Action_1_tDCB3227E2ADD4C5A60112332532BFF3AF7C556D3 * ___Listener_3;

public:
	inline static int32_t get_offset_of_Listener_3() { return static_cast<int32_t>(offsetof(ChallengeExpiredMessage_tD4CCAD5DBF84E0C66C7AFB32AFE92CB71482AAFB_StaticFields, ___Listener_3)); }
	inline Action_1_tDCB3227E2ADD4C5A60112332532BFF3AF7C556D3 * get_Listener_3() const { return ___Listener_3; }
	inline Action_1_tDCB3227E2ADD4C5A60112332532BFF3AF7C556D3 ** get_address_of_Listener_3() { return &___Listener_3; }
	inline void set_Listener_3(Action_1_tDCB3227E2ADD4C5A60112332532BFF3AF7C556D3 * value)
	{
		___Listener_3 = value;
		Il2CppCodeGenWriteBarrier((&___Listener_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHALLENGEEXPIREDMESSAGE_TD4CCAD5DBF84E0C66C7AFB32AFE92CB71482AAFB_H
#ifndef CHALLENGEISSUEDMESSAGE_T692A7D05140D0AE3F3F260C43C9B9F320D018062_H
#define CHALLENGEISSUEDMESSAGE_T692A7D05140D0AE3F3F260C43C9B9F320D018062_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Messages.ChallengeIssuedMessage
struct  ChallengeIssuedMessage_t692A7D05140D0AE3F3F260C43C9B9F320D018062  : public GSMessage_t7604A625444D075A08A76A8029C3AE5534733F7E
{
public:

public:
};

struct ChallengeIssuedMessage_t692A7D05140D0AE3F3F260C43C9B9F320D018062_StaticFields
{
public:
	// System.Action`1<GameSparks.Api.Messages.ChallengeIssuedMessage> GameSparks.Api.Messages.ChallengeIssuedMessage::Listener
	Action_1_tD80DD77C0554659B314930203B7EAFFD278A65F8 * ___Listener_3;

public:
	inline static int32_t get_offset_of_Listener_3() { return static_cast<int32_t>(offsetof(ChallengeIssuedMessage_t692A7D05140D0AE3F3F260C43C9B9F320D018062_StaticFields, ___Listener_3)); }
	inline Action_1_tD80DD77C0554659B314930203B7EAFFD278A65F8 * get_Listener_3() const { return ___Listener_3; }
	inline Action_1_tD80DD77C0554659B314930203B7EAFFD278A65F8 ** get_address_of_Listener_3() { return &___Listener_3; }
	inline void set_Listener_3(Action_1_tD80DD77C0554659B314930203B7EAFFD278A65F8 * value)
	{
		___Listener_3 = value;
		Il2CppCodeGenWriteBarrier((&___Listener_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHALLENGEISSUEDMESSAGE_T692A7D05140D0AE3F3F260C43C9B9F320D018062_H
#ifndef CHALLENGEJOINEDMESSAGE_T7752AC7D49A555FCF12141B06097EF11F0E40178_H
#define CHALLENGEJOINEDMESSAGE_T7752AC7D49A555FCF12141B06097EF11F0E40178_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Messages.ChallengeJoinedMessage
struct  ChallengeJoinedMessage_t7752AC7D49A555FCF12141B06097EF11F0E40178  : public GSMessage_t7604A625444D075A08A76A8029C3AE5534733F7E
{
public:

public:
};

struct ChallengeJoinedMessage_t7752AC7D49A555FCF12141B06097EF11F0E40178_StaticFields
{
public:
	// System.Action`1<GameSparks.Api.Messages.ChallengeJoinedMessage> GameSparks.Api.Messages.ChallengeJoinedMessage::Listener
	Action_1_tF7EDF47FDF943B6491A3DA14627439B0A7AD07CD * ___Listener_3;

public:
	inline static int32_t get_offset_of_Listener_3() { return static_cast<int32_t>(offsetof(ChallengeJoinedMessage_t7752AC7D49A555FCF12141B06097EF11F0E40178_StaticFields, ___Listener_3)); }
	inline Action_1_tF7EDF47FDF943B6491A3DA14627439B0A7AD07CD * get_Listener_3() const { return ___Listener_3; }
	inline Action_1_tF7EDF47FDF943B6491A3DA14627439B0A7AD07CD ** get_address_of_Listener_3() { return &___Listener_3; }
	inline void set_Listener_3(Action_1_tF7EDF47FDF943B6491A3DA14627439B0A7AD07CD * value)
	{
		___Listener_3 = value;
		Il2CppCodeGenWriteBarrier((&___Listener_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHALLENGEJOINEDMESSAGE_T7752AC7D49A555FCF12141B06097EF11F0E40178_H
#ifndef CHALLENGELAPSEDMESSAGE_T955D2E09895E49F556CE70D497DE299F1DCC4075_H
#define CHALLENGELAPSEDMESSAGE_T955D2E09895E49F556CE70D497DE299F1DCC4075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Messages.ChallengeLapsedMessage
struct  ChallengeLapsedMessage_t955D2E09895E49F556CE70D497DE299F1DCC4075  : public GSMessage_t7604A625444D075A08A76A8029C3AE5534733F7E
{
public:

public:
};

struct ChallengeLapsedMessage_t955D2E09895E49F556CE70D497DE299F1DCC4075_StaticFields
{
public:
	// System.Action`1<GameSparks.Api.Messages.ChallengeLapsedMessage> GameSparks.Api.Messages.ChallengeLapsedMessage::Listener
	Action_1_t483CD7B702823AA15494D015C38BE0117BD10494 * ___Listener_3;

public:
	inline static int32_t get_offset_of_Listener_3() { return static_cast<int32_t>(offsetof(ChallengeLapsedMessage_t955D2E09895E49F556CE70D497DE299F1DCC4075_StaticFields, ___Listener_3)); }
	inline Action_1_t483CD7B702823AA15494D015C38BE0117BD10494 * get_Listener_3() const { return ___Listener_3; }
	inline Action_1_t483CD7B702823AA15494D015C38BE0117BD10494 ** get_address_of_Listener_3() { return &___Listener_3; }
	inline void set_Listener_3(Action_1_t483CD7B702823AA15494D015C38BE0117BD10494 * value)
	{
		___Listener_3 = value;
		Il2CppCodeGenWriteBarrier((&___Listener_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHALLENGELAPSEDMESSAGE_T955D2E09895E49F556CE70D497DE299F1DCC4075_H
#ifndef CHALLENGELOSTMESSAGE_TB2EC2E4D16CE555E52065D691EC467F2EA753A27_H
#define CHALLENGELOSTMESSAGE_TB2EC2E4D16CE555E52065D691EC467F2EA753A27_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Messages.ChallengeLostMessage
struct  ChallengeLostMessage_tB2EC2E4D16CE555E52065D691EC467F2EA753A27  : public GSMessage_t7604A625444D075A08A76A8029C3AE5534733F7E
{
public:

public:
};

struct ChallengeLostMessage_tB2EC2E4D16CE555E52065D691EC467F2EA753A27_StaticFields
{
public:
	// System.Action`1<GameSparks.Api.Messages.ChallengeLostMessage> GameSparks.Api.Messages.ChallengeLostMessage::Listener
	Action_1_tBD6D88A3CE9301306CAE4CEEBD0A1B8D24E34A7A * ___Listener_3;

public:
	inline static int32_t get_offset_of_Listener_3() { return static_cast<int32_t>(offsetof(ChallengeLostMessage_tB2EC2E4D16CE555E52065D691EC467F2EA753A27_StaticFields, ___Listener_3)); }
	inline Action_1_tBD6D88A3CE9301306CAE4CEEBD0A1B8D24E34A7A * get_Listener_3() const { return ___Listener_3; }
	inline Action_1_tBD6D88A3CE9301306CAE4CEEBD0A1B8D24E34A7A ** get_address_of_Listener_3() { return &___Listener_3; }
	inline void set_Listener_3(Action_1_tBD6D88A3CE9301306CAE4CEEBD0A1B8D24E34A7A * value)
	{
		___Listener_3 = value;
		Il2CppCodeGenWriteBarrier((&___Listener_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHALLENGELOSTMESSAGE_TB2EC2E4D16CE555E52065D691EC467F2EA753A27_H
#ifndef CHALLENGESTARTEDMESSAGE_T63C6EF31A5A328B29573BE452F6643EA7871EF1F_H
#define CHALLENGESTARTEDMESSAGE_T63C6EF31A5A328B29573BE452F6643EA7871EF1F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Messages.ChallengeStartedMessage
struct  ChallengeStartedMessage_t63C6EF31A5A328B29573BE452F6643EA7871EF1F  : public GSMessage_t7604A625444D075A08A76A8029C3AE5534733F7E
{
public:

public:
};

struct ChallengeStartedMessage_t63C6EF31A5A328B29573BE452F6643EA7871EF1F_StaticFields
{
public:
	// System.Action`1<GameSparks.Api.Messages.ChallengeStartedMessage> GameSparks.Api.Messages.ChallengeStartedMessage::Listener
	Action_1_t8F55990D81FEC641846AC4B851F4C4C1C2049A3E * ___Listener_3;

public:
	inline static int32_t get_offset_of_Listener_3() { return static_cast<int32_t>(offsetof(ChallengeStartedMessage_t63C6EF31A5A328B29573BE452F6643EA7871EF1F_StaticFields, ___Listener_3)); }
	inline Action_1_t8F55990D81FEC641846AC4B851F4C4C1C2049A3E * get_Listener_3() const { return ___Listener_3; }
	inline Action_1_t8F55990D81FEC641846AC4B851F4C4C1C2049A3E ** get_address_of_Listener_3() { return &___Listener_3; }
	inline void set_Listener_3(Action_1_t8F55990D81FEC641846AC4B851F4C4C1C2049A3E * value)
	{
		___Listener_3 = value;
		Il2CppCodeGenWriteBarrier((&___Listener_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHALLENGESTARTEDMESSAGE_T63C6EF31A5A328B29573BE452F6643EA7871EF1F_H
#ifndef CHALLENGETURNTAKENMESSAGE_T86FBCC75AD0C2A7A058711DA510859B54A796DC0_H
#define CHALLENGETURNTAKENMESSAGE_T86FBCC75AD0C2A7A058711DA510859B54A796DC0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Messages.ChallengeTurnTakenMessage
struct  ChallengeTurnTakenMessage_t86FBCC75AD0C2A7A058711DA510859B54A796DC0  : public GSMessage_t7604A625444D075A08A76A8029C3AE5534733F7E
{
public:

public:
};

struct ChallengeTurnTakenMessage_t86FBCC75AD0C2A7A058711DA510859B54A796DC0_StaticFields
{
public:
	// System.Action`1<GameSparks.Api.Messages.ChallengeTurnTakenMessage> GameSparks.Api.Messages.ChallengeTurnTakenMessage::Listener
	Action_1_t77C3D2E822F040B37396DBBFC2A4A2C806DBF0F2 * ___Listener_3;

public:
	inline static int32_t get_offset_of_Listener_3() { return static_cast<int32_t>(offsetof(ChallengeTurnTakenMessage_t86FBCC75AD0C2A7A058711DA510859B54A796DC0_StaticFields, ___Listener_3)); }
	inline Action_1_t77C3D2E822F040B37396DBBFC2A4A2C806DBF0F2 * get_Listener_3() const { return ___Listener_3; }
	inline Action_1_t77C3D2E822F040B37396DBBFC2A4A2C806DBF0F2 ** get_address_of_Listener_3() { return &___Listener_3; }
	inline void set_Listener_3(Action_1_t77C3D2E822F040B37396DBBFC2A4A2C806DBF0F2 * value)
	{
		___Listener_3 = value;
		Il2CppCodeGenWriteBarrier((&___Listener_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHALLENGETURNTAKENMESSAGE_T86FBCC75AD0C2A7A058711DA510859B54A796DC0_H
#ifndef CHALLENGEWAITINGMESSAGE_T109E5AC30730E48822A39E97E761753541F9524B_H
#define CHALLENGEWAITINGMESSAGE_T109E5AC30730E48822A39E97E761753541F9524B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Messages.ChallengeWaitingMessage
struct  ChallengeWaitingMessage_t109E5AC30730E48822A39E97E761753541F9524B  : public GSMessage_t7604A625444D075A08A76A8029C3AE5534733F7E
{
public:

public:
};

struct ChallengeWaitingMessage_t109E5AC30730E48822A39E97E761753541F9524B_StaticFields
{
public:
	// System.Action`1<GameSparks.Api.Messages.ChallengeWaitingMessage> GameSparks.Api.Messages.ChallengeWaitingMessage::Listener
	Action_1_tA4FD32A8EB2243FB0524AF94B8CCED076FBD77DE * ___Listener_3;

public:
	inline static int32_t get_offset_of_Listener_3() { return static_cast<int32_t>(offsetof(ChallengeWaitingMessage_t109E5AC30730E48822A39E97E761753541F9524B_StaticFields, ___Listener_3)); }
	inline Action_1_tA4FD32A8EB2243FB0524AF94B8CCED076FBD77DE * get_Listener_3() const { return ___Listener_3; }
	inline Action_1_tA4FD32A8EB2243FB0524AF94B8CCED076FBD77DE ** get_address_of_Listener_3() { return &___Listener_3; }
	inline void set_Listener_3(Action_1_tA4FD32A8EB2243FB0524AF94B8CCED076FBD77DE * value)
	{
		___Listener_3 = value;
		Il2CppCodeGenWriteBarrier((&___Listener_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHALLENGEWAITINGMESSAGE_T109E5AC30730E48822A39E97E761753541F9524B_H
#ifndef CHALLENGEWITHDRAWNMESSAGE_T38E16E33339FD988BCA5A411D5A0F6555752BA65_H
#define CHALLENGEWITHDRAWNMESSAGE_T38E16E33339FD988BCA5A411D5A0F6555752BA65_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Messages.ChallengeWithdrawnMessage
struct  ChallengeWithdrawnMessage_t38E16E33339FD988BCA5A411D5A0F6555752BA65  : public GSMessage_t7604A625444D075A08A76A8029C3AE5534733F7E
{
public:

public:
};

struct ChallengeWithdrawnMessage_t38E16E33339FD988BCA5A411D5A0F6555752BA65_StaticFields
{
public:
	// System.Action`1<GameSparks.Api.Messages.ChallengeWithdrawnMessage> GameSparks.Api.Messages.ChallengeWithdrawnMessage::Listener
	Action_1_tD793A6FAB97A5522B1A0810C13170E1A66F0B7CB * ___Listener_3;

public:
	inline static int32_t get_offset_of_Listener_3() { return static_cast<int32_t>(offsetof(ChallengeWithdrawnMessage_t38E16E33339FD988BCA5A411D5A0F6555752BA65_StaticFields, ___Listener_3)); }
	inline Action_1_tD793A6FAB97A5522B1A0810C13170E1A66F0B7CB * get_Listener_3() const { return ___Listener_3; }
	inline Action_1_tD793A6FAB97A5522B1A0810C13170E1A66F0B7CB ** get_address_of_Listener_3() { return &___Listener_3; }
	inline void set_Listener_3(Action_1_tD793A6FAB97A5522B1A0810C13170E1A66F0B7CB * value)
	{
		___Listener_3 = value;
		Il2CppCodeGenWriteBarrier((&___Listener_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHALLENGEWITHDRAWNMESSAGE_T38E16E33339FD988BCA5A411D5A0F6555752BA65_H
#ifndef CHALLENGEWONMESSAGE_TBD726ED8E8AEF2B7444866673B4B28AD2EC34E20_H
#define CHALLENGEWONMESSAGE_TBD726ED8E8AEF2B7444866673B4B28AD2EC34E20_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Messages.ChallengeWonMessage
struct  ChallengeWonMessage_tBD726ED8E8AEF2B7444866673B4B28AD2EC34E20  : public GSMessage_t7604A625444D075A08A76A8029C3AE5534733F7E
{
public:

public:
};

struct ChallengeWonMessage_tBD726ED8E8AEF2B7444866673B4B28AD2EC34E20_StaticFields
{
public:
	// System.Action`1<GameSparks.Api.Messages.ChallengeWonMessage> GameSparks.Api.Messages.ChallengeWonMessage::Listener
	Action_1_t1E12E5F732106F4C3E98A0579E69F98EB6E4050E * ___Listener_3;

public:
	inline static int32_t get_offset_of_Listener_3() { return static_cast<int32_t>(offsetof(ChallengeWonMessage_tBD726ED8E8AEF2B7444866673B4B28AD2EC34E20_StaticFields, ___Listener_3)); }
	inline Action_1_t1E12E5F732106F4C3E98A0579E69F98EB6E4050E * get_Listener_3() const { return ___Listener_3; }
	inline Action_1_t1E12E5F732106F4C3E98A0579E69F98EB6E4050E ** get_address_of_Listener_3() { return &___Listener_3; }
	inline void set_Listener_3(Action_1_t1E12E5F732106F4C3E98A0579E69F98EB6E4050E * value)
	{
		___Listener_3 = value;
		Il2CppCodeGenWriteBarrier((&___Listener_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHALLENGEWONMESSAGE_TBD726ED8E8AEF2B7444866673B4B28AD2EC34E20_H
#ifndef FRIENDMESSAGE_TFF3C4B6D8A422595373A76D204E3F374DD8F9AFF_H
#define FRIENDMESSAGE_TFF3C4B6D8A422595373A76D204E3F374DD8F9AFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Messages.FriendMessage
struct  FriendMessage_tFF3C4B6D8A422595373A76D204E3F374DD8F9AFF  : public GSMessage_t7604A625444D075A08A76A8029C3AE5534733F7E
{
public:

public:
};

struct FriendMessage_tFF3C4B6D8A422595373A76D204E3F374DD8F9AFF_StaticFields
{
public:
	// System.Action`1<GameSparks.Api.Messages.FriendMessage> GameSparks.Api.Messages.FriendMessage::Listener
	Action_1_tF139EC3697AA428A3EEA452E530111DA3009ED33 * ___Listener_3;

public:
	inline static int32_t get_offset_of_Listener_3() { return static_cast<int32_t>(offsetof(FriendMessage_tFF3C4B6D8A422595373A76D204E3F374DD8F9AFF_StaticFields, ___Listener_3)); }
	inline Action_1_tF139EC3697AA428A3EEA452E530111DA3009ED33 * get_Listener_3() const { return ___Listener_3; }
	inline Action_1_tF139EC3697AA428A3EEA452E530111DA3009ED33 ** get_address_of_Listener_3() { return &___Listener_3; }
	inline void set_Listener_3(Action_1_tF139EC3697AA428A3EEA452E530111DA3009ED33 * value)
	{
		___Listener_3 = value;
		Il2CppCodeGenWriteBarrier((&___Listener_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FRIENDMESSAGE_TFF3C4B6D8A422595373A76D204E3F374DD8F9AFF_H
#ifndef GLOBALRANKCHANGEDMESSAGE_TD39110C476CDA7239C4BE780F73A74A799A675FB_H
#define GLOBALRANKCHANGEDMESSAGE_TD39110C476CDA7239C4BE780F73A74A799A675FB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Messages.GlobalRankChangedMessage
struct  GlobalRankChangedMessage_tD39110C476CDA7239C4BE780F73A74A799A675FB  : public GSMessage_t7604A625444D075A08A76A8029C3AE5534733F7E
{
public:

public:
};

struct GlobalRankChangedMessage_tD39110C476CDA7239C4BE780F73A74A799A675FB_StaticFields
{
public:
	// System.Action`1<GameSparks.Api.Messages.GlobalRankChangedMessage> GameSparks.Api.Messages.GlobalRankChangedMessage::Listener
	Action_1_t57EAA59039391994FB7EFBF0FD6860F3FCB51B50 * ___Listener_3;

public:
	inline static int32_t get_offset_of_Listener_3() { return static_cast<int32_t>(offsetof(GlobalRankChangedMessage_tD39110C476CDA7239C4BE780F73A74A799A675FB_StaticFields, ___Listener_3)); }
	inline Action_1_t57EAA59039391994FB7EFBF0FD6860F3FCB51B50 * get_Listener_3() const { return ___Listener_3; }
	inline Action_1_t57EAA59039391994FB7EFBF0FD6860F3FCB51B50 ** get_address_of_Listener_3() { return &___Listener_3; }
	inline void set_Listener_3(Action_1_t57EAA59039391994FB7EFBF0FD6860F3FCB51B50 * value)
	{
		___Listener_3 = value;
		Il2CppCodeGenWriteBarrier((&___Listener_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLOBALRANKCHANGEDMESSAGE_TD39110C476CDA7239C4BE780F73A74A799A675FB_H
#ifndef MATCHFOUNDMESSAGE_T9D3942DC180FCB9271AEFB4191E45257D982405D_H
#define MATCHFOUNDMESSAGE_T9D3942DC180FCB9271AEFB4191E45257D982405D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Messages.MatchFoundMessage
struct  MatchFoundMessage_t9D3942DC180FCB9271AEFB4191E45257D982405D  : public GSMessage_t7604A625444D075A08A76A8029C3AE5534733F7E
{
public:

public:
};

struct MatchFoundMessage_t9D3942DC180FCB9271AEFB4191E45257D982405D_StaticFields
{
public:
	// System.Action`1<GameSparks.Api.Messages.MatchFoundMessage> GameSparks.Api.Messages.MatchFoundMessage::Listener
	Action_1_t5BB31FF5B06095D4D0398A948F68BAEF3A7B9EDB * ___Listener_3;

public:
	inline static int32_t get_offset_of_Listener_3() { return static_cast<int32_t>(offsetof(MatchFoundMessage_t9D3942DC180FCB9271AEFB4191E45257D982405D_StaticFields, ___Listener_3)); }
	inline Action_1_t5BB31FF5B06095D4D0398A948F68BAEF3A7B9EDB * get_Listener_3() const { return ___Listener_3; }
	inline Action_1_t5BB31FF5B06095D4D0398A948F68BAEF3A7B9EDB ** get_address_of_Listener_3() { return &___Listener_3; }
	inline void set_Listener_3(Action_1_t5BB31FF5B06095D4D0398A948F68BAEF3A7B9EDB * value)
	{
		___Listener_3 = value;
		Il2CppCodeGenWriteBarrier((&___Listener_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATCHFOUNDMESSAGE_T9D3942DC180FCB9271AEFB4191E45257D982405D_H
#ifndef MATCHNOTFOUNDMESSAGE_TEF611FC19D5DF772316F6A57A4033F76CBA2127B_H
#define MATCHNOTFOUNDMESSAGE_TEF611FC19D5DF772316F6A57A4033F76CBA2127B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Messages.MatchNotFoundMessage
struct  MatchNotFoundMessage_tEF611FC19D5DF772316F6A57A4033F76CBA2127B  : public GSMessage_t7604A625444D075A08A76A8029C3AE5534733F7E
{
public:

public:
};

struct MatchNotFoundMessage_tEF611FC19D5DF772316F6A57A4033F76CBA2127B_StaticFields
{
public:
	// System.Action`1<GameSparks.Api.Messages.MatchNotFoundMessage> GameSparks.Api.Messages.MatchNotFoundMessage::Listener
	Action_1_t7BE0FF66FF22E957D97608C62F11B2FB1D4DC6A9 * ___Listener_3;

public:
	inline static int32_t get_offset_of_Listener_3() { return static_cast<int32_t>(offsetof(MatchNotFoundMessage_tEF611FC19D5DF772316F6A57A4033F76CBA2127B_StaticFields, ___Listener_3)); }
	inline Action_1_t7BE0FF66FF22E957D97608C62F11B2FB1D4DC6A9 * get_Listener_3() const { return ___Listener_3; }
	inline Action_1_t7BE0FF66FF22E957D97608C62F11B2FB1D4DC6A9 ** get_address_of_Listener_3() { return &___Listener_3; }
	inline void set_Listener_3(Action_1_t7BE0FF66FF22E957D97608C62F11B2FB1D4DC6A9 * value)
	{
		___Listener_3 = value;
		Il2CppCodeGenWriteBarrier((&___Listener_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATCHNOTFOUNDMESSAGE_TEF611FC19D5DF772316F6A57A4033F76CBA2127B_H
#ifndef MATCHUPDATEDMESSAGE_T71F1AFADF43071DDDFAB6C1FCDEF60F3D778141B_H
#define MATCHUPDATEDMESSAGE_T71F1AFADF43071DDDFAB6C1FCDEF60F3D778141B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Messages.MatchUpdatedMessage
struct  MatchUpdatedMessage_t71F1AFADF43071DDDFAB6C1FCDEF60F3D778141B  : public GSMessage_t7604A625444D075A08A76A8029C3AE5534733F7E
{
public:

public:
};

struct MatchUpdatedMessage_t71F1AFADF43071DDDFAB6C1FCDEF60F3D778141B_StaticFields
{
public:
	// System.Action`1<GameSparks.Api.Messages.MatchUpdatedMessage> GameSparks.Api.Messages.MatchUpdatedMessage::Listener
	Action_1_t3408E89713E197ED474CC1377B5AC04C53186A66 * ___Listener_3;

public:
	inline static int32_t get_offset_of_Listener_3() { return static_cast<int32_t>(offsetof(MatchUpdatedMessage_t71F1AFADF43071DDDFAB6C1FCDEF60F3D778141B_StaticFields, ___Listener_3)); }
	inline Action_1_t3408E89713E197ED474CC1377B5AC04C53186A66 * get_Listener_3() const { return ___Listener_3; }
	inline Action_1_t3408E89713E197ED474CC1377B5AC04C53186A66 ** get_address_of_Listener_3() { return &___Listener_3; }
	inline void set_Listener_3(Action_1_t3408E89713E197ED474CC1377B5AC04C53186A66 * value)
	{
		___Listener_3 = value;
		Il2CppCodeGenWriteBarrier((&___Listener_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATCHUPDATEDMESSAGE_T71F1AFADF43071DDDFAB6C1FCDEF60F3D778141B_H
#ifndef NEWHIGHSCOREMESSAGE_T4004DB3957E0A8FCB2095C1090D874744DC74EAA_H
#define NEWHIGHSCOREMESSAGE_T4004DB3957E0A8FCB2095C1090D874744DC74EAA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Messages.NewHighScoreMessage
struct  NewHighScoreMessage_t4004DB3957E0A8FCB2095C1090D874744DC74EAA  : public GSMessage_t7604A625444D075A08A76A8029C3AE5534733F7E
{
public:

public:
};

struct NewHighScoreMessage_t4004DB3957E0A8FCB2095C1090D874744DC74EAA_StaticFields
{
public:
	// System.Action`1<GameSparks.Api.Messages.NewHighScoreMessage> GameSparks.Api.Messages.NewHighScoreMessage::Listener
	Action_1_t4E1CE38FE1E13AF4D183226F72AC6E789D638941 * ___Listener_3;

public:
	inline static int32_t get_offset_of_Listener_3() { return static_cast<int32_t>(offsetof(NewHighScoreMessage_t4004DB3957E0A8FCB2095C1090D874744DC74EAA_StaticFields, ___Listener_3)); }
	inline Action_1_t4E1CE38FE1E13AF4D183226F72AC6E789D638941 * get_Listener_3() const { return ___Listener_3; }
	inline Action_1_t4E1CE38FE1E13AF4D183226F72AC6E789D638941 ** get_address_of_Listener_3() { return &___Listener_3; }
	inline void set_Listener_3(Action_1_t4E1CE38FE1E13AF4D183226F72AC6E789D638941 * value)
	{
		___Listener_3 = value;
		Il2CppCodeGenWriteBarrier((&___Listener_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NEWHIGHSCOREMESSAGE_T4004DB3957E0A8FCB2095C1090D874744DC74EAA_H
#ifndef NEWTEAMSCOREMESSAGE_T14485C7162863C58D1DF0297CDE450EA3FC01F80_H
#define NEWTEAMSCOREMESSAGE_T14485C7162863C58D1DF0297CDE450EA3FC01F80_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Messages.NewTeamScoreMessage
struct  NewTeamScoreMessage_t14485C7162863C58D1DF0297CDE450EA3FC01F80  : public GSMessage_t7604A625444D075A08A76A8029C3AE5534733F7E
{
public:

public:
};

struct NewTeamScoreMessage_t14485C7162863C58D1DF0297CDE450EA3FC01F80_StaticFields
{
public:
	// System.Action`1<GameSparks.Api.Messages.NewTeamScoreMessage> GameSparks.Api.Messages.NewTeamScoreMessage::Listener
	Action_1_tF0D4E4EB50479AC02BE6F69D9CF12EC482CEA95E * ___Listener_3;

public:
	inline static int32_t get_offset_of_Listener_3() { return static_cast<int32_t>(offsetof(NewTeamScoreMessage_t14485C7162863C58D1DF0297CDE450EA3FC01F80_StaticFields, ___Listener_3)); }
	inline Action_1_tF0D4E4EB50479AC02BE6F69D9CF12EC482CEA95E * get_Listener_3() const { return ___Listener_3; }
	inline Action_1_tF0D4E4EB50479AC02BE6F69D9CF12EC482CEA95E ** get_address_of_Listener_3() { return &___Listener_3; }
	inline void set_Listener_3(Action_1_tF0D4E4EB50479AC02BE6F69D9CF12EC482CEA95E * value)
	{
		___Listener_3 = value;
		Il2CppCodeGenWriteBarrier((&___Listener_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NEWTEAMSCOREMESSAGE_T14485C7162863C58D1DF0297CDE450EA3FC01F80_H
#ifndef SCRIPTMESSAGE_T8ACBE46585B60212B7DF53DF8D2B688A7805D183_H
#define SCRIPTMESSAGE_T8ACBE46585B60212B7DF53DF8D2B688A7805D183_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Messages.ScriptMessage
struct  ScriptMessage_t8ACBE46585B60212B7DF53DF8D2B688A7805D183  : public GSMessage_t7604A625444D075A08A76A8029C3AE5534733F7E
{
public:

public:
};

struct ScriptMessage_t8ACBE46585B60212B7DF53DF8D2B688A7805D183_StaticFields
{
public:
	// System.Action`1<GameSparks.Api.Messages.ScriptMessage> GameSparks.Api.Messages.ScriptMessage::Listener
	Action_1_tA07939E8D990074A80E1CA26894E930FA9D08CEE * ___Listener_3;

public:
	inline static int32_t get_offset_of_Listener_3() { return static_cast<int32_t>(offsetof(ScriptMessage_t8ACBE46585B60212B7DF53DF8D2B688A7805D183_StaticFields, ___Listener_3)); }
	inline Action_1_tA07939E8D990074A80E1CA26894E930FA9D08CEE * get_Listener_3() const { return ___Listener_3; }
	inline Action_1_tA07939E8D990074A80E1CA26894E930FA9D08CEE ** get_address_of_Listener_3() { return &___Listener_3; }
	inline void set_Listener_3(Action_1_tA07939E8D990074A80E1CA26894E930FA9D08CEE * value)
	{
		___Listener_3 = value;
		Il2CppCodeGenWriteBarrier((&___Listener_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRIPTMESSAGE_T8ACBE46585B60212B7DF53DF8D2B688A7805D183_H
#ifndef SESSIONTERMINATEDMESSAGE_TE8C584B4A421AFD2A9C149EF8E1B5886DFC3A6AE_H
#define SESSIONTERMINATEDMESSAGE_TE8C584B4A421AFD2A9C149EF8E1B5886DFC3A6AE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Messages.SessionTerminatedMessage
struct  SessionTerminatedMessage_tE8C584B4A421AFD2A9C149EF8E1B5886DFC3A6AE  : public GSMessage_t7604A625444D075A08A76A8029C3AE5534733F7E
{
public:

public:
};

struct SessionTerminatedMessage_tE8C584B4A421AFD2A9C149EF8E1B5886DFC3A6AE_StaticFields
{
public:
	// System.Action`1<GameSparks.Api.Messages.SessionTerminatedMessage> GameSparks.Api.Messages.SessionTerminatedMessage::Listener
	Action_1_t6C56C09D50C5F187196A7FB4D0413C3E7BFE01D4 * ___Listener_3;

public:
	inline static int32_t get_offset_of_Listener_3() { return static_cast<int32_t>(offsetof(SessionTerminatedMessage_tE8C584B4A421AFD2A9C149EF8E1B5886DFC3A6AE_StaticFields, ___Listener_3)); }
	inline Action_1_t6C56C09D50C5F187196A7FB4D0413C3E7BFE01D4 * get_Listener_3() const { return ___Listener_3; }
	inline Action_1_t6C56C09D50C5F187196A7FB4D0413C3E7BFE01D4 ** get_address_of_Listener_3() { return &___Listener_3; }
	inline void set_Listener_3(Action_1_t6C56C09D50C5F187196A7FB4D0413C3E7BFE01D4 * value)
	{
		___Listener_3 = value;
		Il2CppCodeGenWriteBarrier((&___Listener_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SESSIONTERMINATEDMESSAGE_TE8C584B4A421AFD2A9C149EF8E1B5886DFC3A6AE_H
#ifndef SOCIALRANKCHANGEDMESSAGE_TEA41D07C3E504261D8474AA07A8D2647BD272F6F_H
#define SOCIALRANKCHANGEDMESSAGE_TEA41D07C3E504261D8474AA07A8D2647BD272F6F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Messages.SocialRankChangedMessage
struct  SocialRankChangedMessage_tEA41D07C3E504261D8474AA07A8D2647BD272F6F  : public GSMessage_t7604A625444D075A08A76A8029C3AE5534733F7E
{
public:

public:
};

struct SocialRankChangedMessage_tEA41D07C3E504261D8474AA07A8D2647BD272F6F_StaticFields
{
public:
	// System.Action`1<GameSparks.Api.Messages.SocialRankChangedMessage> GameSparks.Api.Messages.SocialRankChangedMessage::Listener
	Action_1_t022AE2F55D98E649680E55D233D4A1626C22D986 * ___Listener_3;

public:
	inline static int32_t get_offset_of_Listener_3() { return static_cast<int32_t>(offsetof(SocialRankChangedMessage_tEA41D07C3E504261D8474AA07A8D2647BD272F6F_StaticFields, ___Listener_3)); }
	inline Action_1_t022AE2F55D98E649680E55D233D4A1626C22D986 * get_Listener_3() const { return ___Listener_3; }
	inline Action_1_t022AE2F55D98E649680E55D233D4A1626C22D986 ** get_address_of_Listener_3() { return &___Listener_3; }
	inline void set_Listener_3(Action_1_t022AE2F55D98E649680E55D233D4A1626C22D986 * value)
	{
		___Listener_3 = value;
		Il2CppCodeGenWriteBarrier((&___Listener_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOCIALRANKCHANGEDMESSAGE_TEA41D07C3E504261D8474AA07A8D2647BD272F6F_H
#ifndef TEAMCHATMESSAGE_T59612BFD71E8471EDFADFB67FD67E8CF2992DA16_H
#define TEAMCHATMESSAGE_T59612BFD71E8471EDFADFB67FD67E8CF2992DA16_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Messages.TeamChatMessage
struct  TeamChatMessage_t59612BFD71E8471EDFADFB67FD67E8CF2992DA16  : public GSMessage_t7604A625444D075A08A76A8029C3AE5534733F7E
{
public:

public:
};

struct TeamChatMessage_t59612BFD71E8471EDFADFB67FD67E8CF2992DA16_StaticFields
{
public:
	// System.Action`1<GameSparks.Api.Messages.TeamChatMessage> GameSparks.Api.Messages.TeamChatMessage::Listener
	Action_1_tE9BEA6F59DC29212B2E7E712EA80E8180C4966DD * ___Listener_3;

public:
	inline static int32_t get_offset_of_Listener_3() { return static_cast<int32_t>(offsetof(TeamChatMessage_t59612BFD71E8471EDFADFB67FD67E8CF2992DA16_StaticFields, ___Listener_3)); }
	inline Action_1_tE9BEA6F59DC29212B2E7E712EA80E8180C4966DD * get_Listener_3() const { return ___Listener_3; }
	inline Action_1_tE9BEA6F59DC29212B2E7E712EA80E8180C4966DD ** get_address_of_Listener_3() { return &___Listener_3; }
	inline void set_Listener_3(Action_1_tE9BEA6F59DC29212B2E7E712EA80E8180C4966DD * value)
	{
		___Listener_3 = value;
		Il2CppCodeGenWriteBarrier((&___Listener_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEAMCHATMESSAGE_T59612BFD71E8471EDFADFB67FD67E8CF2992DA16_H
#ifndef TEAMRANKCHANGEDMESSAGE_TEB2EE77C6F7F85132CD3FF4AFB22F43534D5D571_H
#define TEAMRANKCHANGEDMESSAGE_TEB2EE77C6F7F85132CD3FF4AFB22F43534D5D571_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Messages.TeamRankChangedMessage
struct  TeamRankChangedMessage_tEB2EE77C6F7F85132CD3FF4AFB22F43534D5D571  : public GSMessage_t7604A625444D075A08A76A8029C3AE5534733F7E
{
public:

public:
};

struct TeamRankChangedMessage_tEB2EE77C6F7F85132CD3FF4AFB22F43534D5D571_StaticFields
{
public:
	// System.Action`1<GameSparks.Api.Messages.TeamRankChangedMessage> GameSparks.Api.Messages.TeamRankChangedMessage::Listener
	Action_1_tD5531F2B1B9DE563F0C630AEE876964F6C7260B9 * ___Listener_3;

public:
	inline static int32_t get_offset_of_Listener_3() { return static_cast<int32_t>(offsetof(TeamRankChangedMessage_tEB2EE77C6F7F85132CD3FF4AFB22F43534D5D571_StaticFields, ___Listener_3)); }
	inline Action_1_tD5531F2B1B9DE563F0C630AEE876964F6C7260B9 * get_Listener_3() const { return ___Listener_3; }
	inline Action_1_tD5531F2B1B9DE563F0C630AEE876964F6C7260B9 ** get_address_of_Listener_3() { return &___Listener_3; }
	inline void set_Listener_3(Action_1_tD5531F2B1B9DE563F0C630AEE876964F6C7260B9 * value)
	{
		___Listener_3 = value;
		Il2CppCodeGenWriteBarrier((&___Listener_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEAMRANKCHANGEDMESSAGE_TEB2EE77C6F7F85132CD3FF4AFB22F43534D5D571_H
#ifndef UPLOADCOMPLETEMESSAGE_T2F357234A77684DB822A6969FBB34B5E0D3FB7FF_H
#define UPLOADCOMPLETEMESSAGE_T2F357234A77684DB822A6969FBB34B5E0D3FB7FF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Api.Messages.UploadCompleteMessage
struct  UploadCompleteMessage_t2F357234A77684DB822A6969FBB34B5E0D3FB7FF  : public GSMessage_t7604A625444D075A08A76A8029C3AE5534733F7E
{
public:

public:
};

struct UploadCompleteMessage_t2F357234A77684DB822A6969FBB34B5E0D3FB7FF_StaticFields
{
public:
	// System.Action`1<GameSparks.Api.Messages.UploadCompleteMessage> GameSparks.Api.Messages.UploadCompleteMessage::Listener
	Action_1_t58AAB7430B02EABBBF4BDBC069725D1E18DFCAE3 * ___Listener_3;

public:
	inline static int32_t get_offset_of_Listener_3() { return static_cast<int32_t>(offsetof(UploadCompleteMessage_t2F357234A77684DB822A6969FBB34B5E0D3FB7FF_StaticFields, ___Listener_3)); }
	inline Action_1_t58AAB7430B02EABBBF4BDBC069725D1E18DFCAE3 * get_Listener_3() const { return ___Listener_3; }
	inline Action_1_t58AAB7430B02EABBBF4BDBC069725D1E18DFCAE3 ** get_address_of_Listener_3() { return &___Listener_3; }
	inline void set_Listener_3(Action_1_t58AAB7430B02EABBBF4BDBC069725D1E18DFCAE3 * value)
	{
		___Listener_3 = value;
		Il2CppCodeGenWriteBarrier((&___Listener_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPLOADCOMPLETEMESSAGE_T2F357234A77684DB822A6969FBB34B5E0D3FB7FF_H
#ifndef GSOBJECT_T05160F6677D08913C6CD9AB7C7DD9454D3B12E88_H
#define GSOBJECT_T05160F6677D08913C6CD9AB7C7DD9454D3B12E88_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Core.GSObject
struct  GSObject_t05160F6677D08913C6CD9AB7C7DD9454D3B12E88  : public GSRequestData_t84713ABAAB257F300A1DB26F09C91AB0519109F8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GSOBJECT_T05160F6677D08913C6CD9AB7C7DD9454D3B12E88_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef RAY_TE2163D4CB3E6B267E29F8ABE41684490E4A614B2_H
#define RAY_TE2163D4CB3E6B267E29F8ABE41684490E4A614B2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Ray
struct  Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 
{
public:
	// UnityEngine.Vector3 UnityEngine.Ray::m_Origin
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Origin_0;
	// UnityEngine.Vector3 UnityEngine.Ray::m_Direction
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Direction_1;

public:
	inline static int32_t get_offset_of_m_Origin_0() { return static_cast<int32_t>(offsetof(Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2, ___m_Origin_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Origin_0() const { return ___m_Origin_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Origin_0() { return &___m_Origin_0; }
	inline void set_m_Origin_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Origin_0 = value;
	}

	inline static int32_t get_offset_of_m_Direction_1() { return static_cast<int32_t>(offsetof(Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2, ___m_Direction_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Direction_1() const { return ___m_Direction_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Direction_1() { return &___m_Direction_1; }
	inline void set_m_Direction_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Direction_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAY_TE2163D4CB3E6B267E29F8ABE41684490E4A614B2_H
#ifndef RAYCASTHIT_T19695F18F9265FE5425062BBA6A4D330480538C3_H
#define RAYCASTHIT_T19695F18F9265FE5425062BBA6A4D330480538C3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit
struct  RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 
{
public:
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Point
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Normal
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Normal_1;
	// System.UInt32 UnityEngine.RaycastHit::m_FaceID
	uint32_t ___m_FaceID_2;
	// System.Single UnityEngine.RaycastHit::m_Distance
	float ___m_Distance_3;
	// UnityEngine.Vector2 UnityEngine.RaycastHit::m_UV
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_UV_4;
	// System.Int32 UnityEngine.RaycastHit::m_Collider
	int32_t ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_Point_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_Normal_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_FaceID_2() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_FaceID_2)); }
	inline uint32_t get_m_FaceID_2() const { return ___m_FaceID_2; }
	inline uint32_t* get_address_of_m_FaceID_2() { return &___m_FaceID_2; }
	inline void set_m_FaceID_2(uint32_t value)
	{
		___m_FaceID_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_UV_4() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_UV_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_UV_4() const { return ___m_UV_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_UV_4() { return &___m_UV_4; }
	inline void set_m_UV_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_UV_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_Collider_5)); }
	inline int32_t get_m_Collider_5() const { return ___m_Collider_5; }
	inline int32_t* get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(int32_t value)
	{
		___m_Collider_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTHIT_T19695F18F9265FE5425062BBA6A4D330480538C3_H
#ifndef RAYCASTHIT2D_T5E8A7F96317BAF2033362FC780F4D72DC72764BE_H
#define RAYCASTHIT2D_T5E8A7F96317BAF2033362FC780F4D72DC72764BE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit2D
struct  RaycastHit2D_t5E8A7F96317BAF2033362FC780F4D72DC72764BE 
{
public:
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Centroid
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_Centroid_0;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Point
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_Point_1;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Normal
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_Normal_2;
	// System.Single UnityEngine.RaycastHit2D::m_Distance
	float ___m_Distance_3;
	// System.Single UnityEngine.RaycastHit2D::m_Fraction
	float ___m_Fraction_4;
	// System.Int32 UnityEngine.RaycastHit2D::m_Collider
	int32_t ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Centroid_0() { return static_cast<int32_t>(offsetof(RaycastHit2D_t5E8A7F96317BAF2033362FC780F4D72DC72764BE, ___m_Centroid_0)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_Centroid_0() const { return ___m_Centroid_0; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_Centroid_0() { return &___m_Centroid_0; }
	inline void set_m_Centroid_0(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_Centroid_0 = value;
	}

	inline static int32_t get_offset_of_m_Point_1() { return static_cast<int32_t>(offsetof(RaycastHit2D_t5E8A7F96317BAF2033362FC780F4D72DC72764BE, ___m_Point_1)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_Point_1() const { return ___m_Point_1; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_Point_1() { return &___m_Point_1; }
	inline void set_m_Point_1(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_Point_1 = value;
	}

	inline static int32_t get_offset_of_m_Normal_2() { return static_cast<int32_t>(offsetof(RaycastHit2D_t5E8A7F96317BAF2033362FC780F4D72DC72764BE, ___m_Normal_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_Normal_2() const { return ___m_Normal_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_Normal_2() { return &___m_Normal_2; }
	inline void set_m_Normal_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_Normal_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit2D_t5E8A7F96317BAF2033362FC780F4D72DC72764BE, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_Fraction_4() { return static_cast<int32_t>(offsetof(RaycastHit2D_t5E8A7F96317BAF2033362FC780F4D72DC72764BE, ___m_Fraction_4)); }
	inline float get_m_Fraction_4() const { return ___m_Fraction_4; }
	inline float* get_address_of_m_Fraction_4() { return &___m_Fraction_4; }
	inline void set_m_Fraction_4(float value)
	{
		___m_Fraction_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit2D_t5E8A7F96317BAF2033362FC780F4D72DC72764BE, ___m_Collider_5)); }
	inline int32_t get_m_Collider_5() const { return ___m_Collider_5; }
	inline int32_t* get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(int32_t value)
	{
		___m_Collider_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTHIT2D_T5E8A7F96317BAF2033362FC780F4D72DC72764BE_H
#ifndef TEXTANCHOR_TEC19034D476659A5E05366C63564F34DD30E7C57_H
#define TEXTANCHOR_TEC19034D476659A5E05366C63564F34DD30E7C57_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextAnchor
struct  TextAnchor_tEC19034D476659A5E05366C63564F34DD30E7C57 
{
public:
	// System.Int32 UnityEngine.TextAnchor::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextAnchor_tEC19034D476659A5E05366C63564F34DD30E7C57, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTANCHOR_TEC19034D476659A5E05366C63564F34DD30E7C57_H
#ifndef VERTEXHELPER_T27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F_H
#define VERTEXHELPER_T27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.VertexHelper
struct  VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UnityEngine.UI.VertexHelper::m_Positions
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___m_Positions_0;
	// System.Collections.Generic.List`1<UnityEngine.Color32> UnityEngine.UI.VertexHelper::m_Colors
	List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 * ___m_Colors_1;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv0S
	List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * ___m_Uv0S_2;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv1S
	List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * ___m_Uv1S_3;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv2S
	List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * ___m_Uv2S_4;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv3S
	List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * ___m_Uv3S_5;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UnityEngine.UI.VertexHelper::m_Normals
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___m_Normals_6;
	// System.Collections.Generic.List`1<UnityEngine.Vector4> UnityEngine.UI.VertexHelper::m_Tangents
	List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 * ___m_Tangents_7;
	// System.Collections.Generic.List`1<System.Int32> UnityEngine.UI.VertexHelper::m_Indices
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ___m_Indices_8;
	// System.Boolean UnityEngine.UI.VertexHelper::m_ListsInitalized
	bool ___m_ListsInitalized_11;

public:
	inline static int32_t get_offset_of_m_Positions_0() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Positions_0)); }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * get_m_Positions_0() const { return ___m_Positions_0; }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** get_address_of_m_Positions_0() { return &___m_Positions_0; }
	inline void set_m_Positions_0(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * value)
	{
		___m_Positions_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Positions_0), value);
	}

	inline static int32_t get_offset_of_m_Colors_1() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Colors_1)); }
	inline List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 * get_m_Colors_1() const { return ___m_Colors_1; }
	inline List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 ** get_address_of_m_Colors_1() { return &___m_Colors_1; }
	inline void set_m_Colors_1(List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 * value)
	{
		___m_Colors_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Colors_1), value);
	}

	inline static int32_t get_offset_of_m_Uv0S_2() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Uv0S_2)); }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * get_m_Uv0S_2() const { return ___m_Uv0S_2; }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB ** get_address_of_m_Uv0S_2() { return &___m_Uv0S_2; }
	inline void set_m_Uv0S_2(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * value)
	{
		___m_Uv0S_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv0S_2), value);
	}

	inline static int32_t get_offset_of_m_Uv1S_3() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Uv1S_3)); }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * get_m_Uv1S_3() const { return ___m_Uv1S_3; }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB ** get_address_of_m_Uv1S_3() { return &___m_Uv1S_3; }
	inline void set_m_Uv1S_3(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * value)
	{
		___m_Uv1S_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv1S_3), value);
	}

	inline static int32_t get_offset_of_m_Uv2S_4() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Uv2S_4)); }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * get_m_Uv2S_4() const { return ___m_Uv2S_4; }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB ** get_address_of_m_Uv2S_4() { return &___m_Uv2S_4; }
	inline void set_m_Uv2S_4(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * value)
	{
		___m_Uv2S_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv2S_4), value);
	}

	inline static int32_t get_offset_of_m_Uv3S_5() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Uv3S_5)); }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * get_m_Uv3S_5() const { return ___m_Uv3S_5; }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB ** get_address_of_m_Uv3S_5() { return &___m_Uv3S_5; }
	inline void set_m_Uv3S_5(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * value)
	{
		___m_Uv3S_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv3S_5), value);
	}

	inline static int32_t get_offset_of_m_Normals_6() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Normals_6)); }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * get_m_Normals_6() const { return ___m_Normals_6; }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** get_address_of_m_Normals_6() { return &___m_Normals_6; }
	inline void set_m_Normals_6(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * value)
	{
		___m_Normals_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Normals_6), value);
	}

	inline static int32_t get_offset_of_m_Tangents_7() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Tangents_7)); }
	inline List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 * get_m_Tangents_7() const { return ___m_Tangents_7; }
	inline List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 ** get_address_of_m_Tangents_7() { return &___m_Tangents_7; }
	inline void set_m_Tangents_7(List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 * value)
	{
		___m_Tangents_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Tangents_7), value);
	}

	inline static int32_t get_offset_of_m_Indices_8() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Indices_8)); }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * get_m_Indices_8() const { return ___m_Indices_8; }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 ** get_address_of_m_Indices_8() { return &___m_Indices_8; }
	inline void set_m_Indices_8(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * value)
	{
		___m_Indices_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_Indices_8), value);
	}

	inline static int32_t get_offset_of_m_ListsInitalized_11() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_ListsInitalized_11)); }
	inline bool get_m_ListsInitalized_11() const { return ___m_ListsInitalized_11; }
	inline bool* get_address_of_m_ListsInitalized_11() { return &___m_ListsInitalized_11; }
	inline void set_m_ListsInitalized_11(bool value)
	{
		___m_ListsInitalized_11 = value;
	}
};

struct VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.UI.VertexHelper::s_DefaultTangent
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___s_DefaultTangent_9;
	// UnityEngine.Vector3 UnityEngine.UI.VertexHelper::s_DefaultNormal
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___s_DefaultNormal_10;

public:
	inline static int32_t get_offset_of_s_DefaultTangent_9() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F_StaticFields, ___s_DefaultTangent_9)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_s_DefaultTangent_9() const { return ___s_DefaultTangent_9; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_s_DefaultTangent_9() { return &___s_DefaultTangent_9; }
	inline void set_s_DefaultTangent_9(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___s_DefaultTangent_9 = value;
	}

	inline static int32_t get_offset_of_s_DefaultNormal_10() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F_StaticFields, ___s_DefaultNormal_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_s_DefaultNormal_10() const { return ___s_DefaultNormal_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_s_DefaultNormal_10() { return &___s_DefaultNormal_10; }
	inline void set_s_DefaultNormal_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___s_DefaultNormal_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXHELPER_T27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#define SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_pinvoke : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_com : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifndef FACEBOOKSETTINGS_T1A2119548288F7AD79D590C587740A8EED14D74D_H
#define FACEBOOKSETTINGS_T1A2119548288F7AD79D590C587740A8EED14D74D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Settings.FacebookSettings
struct  FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// System.Int32 Facebook.Unity.Settings.FacebookSettings::selectedAppIndex
	int32_t ___selectedAppIndex_9;
	// System.Collections.Generic.List`1<System.String> Facebook.Unity.Settings.FacebookSettings::clientTokens
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___clientTokens_10;
	// System.Collections.Generic.List`1<System.String> Facebook.Unity.Settings.FacebookSettings::appIds
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___appIds_11;
	// System.Collections.Generic.List`1<System.String> Facebook.Unity.Settings.FacebookSettings::appLabels
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___appLabels_12;
	// System.Boolean Facebook.Unity.Settings.FacebookSettings::cookie
	bool ___cookie_13;
	// System.Boolean Facebook.Unity.Settings.FacebookSettings::logging
	bool ___logging_14;
	// System.Boolean Facebook.Unity.Settings.FacebookSettings::status
	bool ___status_15;
	// System.Boolean Facebook.Unity.Settings.FacebookSettings::xfbml
	bool ___xfbml_16;
	// System.Boolean Facebook.Unity.Settings.FacebookSettings::frictionlessRequests
	bool ___frictionlessRequests_17;
	// System.String Facebook.Unity.Settings.FacebookSettings::iosURLSuffix
	String_t* ___iosURLSuffix_18;
	// System.Collections.Generic.List`1<Facebook.Unity.Settings.FacebookSettings_UrlSchemes> Facebook.Unity.Settings.FacebookSettings::appLinkSchemes
	List_1_tEBAA293BF803F2D17E20E8B6FE34EF3792F6FA6B * ___appLinkSchemes_19;
	// System.String Facebook.Unity.Settings.FacebookSettings::uploadAccessToken
	String_t* ___uploadAccessToken_20;
	// System.Boolean Facebook.Unity.Settings.FacebookSettings::autoLogAppEventsEnabled
	bool ___autoLogAppEventsEnabled_21;
	// System.Boolean Facebook.Unity.Settings.FacebookSettings::advertiserIDCollectionEnabled
	bool ___advertiserIDCollectionEnabled_22;

public:
	inline static int32_t get_offset_of_selectedAppIndex_9() { return static_cast<int32_t>(offsetof(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D, ___selectedAppIndex_9)); }
	inline int32_t get_selectedAppIndex_9() const { return ___selectedAppIndex_9; }
	inline int32_t* get_address_of_selectedAppIndex_9() { return &___selectedAppIndex_9; }
	inline void set_selectedAppIndex_9(int32_t value)
	{
		___selectedAppIndex_9 = value;
	}

	inline static int32_t get_offset_of_clientTokens_10() { return static_cast<int32_t>(offsetof(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D, ___clientTokens_10)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_clientTokens_10() const { return ___clientTokens_10; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_clientTokens_10() { return &___clientTokens_10; }
	inline void set_clientTokens_10(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___clientTokens_10 = value;
		Il2CppCodeGenWriteBarrier((&___clientTokens_10), value);
	}

	inline static int32_t get_offset_of_appIds_11() { return static_cast<int32_t>(offsetof(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D, ___appIds_11)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_appIds_11() const { return ___appIds_11; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_appIds_11() { return &___appIds_11; }
	inline void set_appIds_11(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___appIds_11 = value;
		Il2CppCodeGenWriteBarrier((&___appIds_11), value);
	}

	inline static int32_t get_offset_of_appLabels_12() { return static_cast<int32_t>(offsetof(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D, ___appLabels_12)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_appLabels_12() const { return ___appLabels_12; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_appLabels_12() { return &___appLabels_12; }
	inline void set_appLabels_12(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___appLabels_12 = value;
		Il2CppCodeGenWriteBarrier((&___appLabels_12), value);
	}

	inline static int32_t get_offset_of_cookie_13() { return static_cast<int32_t>(offsetof(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D, ___cookie_13)); }
	inline bool get_cookie_13() const { return ___cookie_13; }
	inline bool* get_address_of_cookie_13() { return &___cookie_13; }
	inline void set_cookie_13(bool value)
	{
		___cookie_13 = value;
	}

	inline static int32_t get_offset_of_logging_14() { return static_cast<int32_t>(offsetof(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D, ___logging_14)); }
	inline bool get_logging_14() const { return ___logging_14; }
	inline bool* get_address_of_logging_14() { return &___logging_14; }
	inline void set_logging_14(bool value)
	{
		___logging_14 = value;
	}

	inline static int32_t get_offset_of_status_15() { return static_cast<int32_t>(offsetof(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D, ___status_15)); }
	inline bool get_status_15() const { return ___status_15; }
	inline bool* get_address_of_status_15() { return &___status_15; }
	inline void set_status_15(bool value)
	{
		___status_15 = value;
	}

	inline static int32_t get_offset_of_xfbml_16() { return static_cast<int32_t>(offsetof(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D, ___xfbml_16)); }
	inline bool get_xfbml_16() const { return ___xfbml_16; }
	inline bool* get_address_of_xfbml_16() { return &___xfbml_16; }
	inline void set_xfbml_16(bool value)
	{
		___xfbml_16 = value;
	}

	inline static int32_t get_offset_of_frictionlessRequests_17() { return static_cast<int32_t>(offsetof(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D, ___frictionlessRequests_17)); }
	inline bool get_frictionlessRequests_17() const { return ___frictionlessRequests_17; }
	inline bool* get_address_of_frictionlessRequests_17() { return &___frictionlessRequests_17; }
	inline void set_frictionlessRequests_17(bool value)
	{
		___frictionlessRequests_17 = value;
	}

	inline static int32_t get_offset_of_iosURLSuffix_18() { return static_cast<int32_t>(offsetof(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D, ___iosURLSuffix_18)); }
	inline String_t* get_iosURLSuffix_18() const { return ___iosURLSuffix_18; }
	inline String_t** get_address_of_iosURLSuffix_18() { return &___iosURLSuffix_18; }
	inline void set_iosURLSuffix_18(String_t* value)
	{
		___iosURLSuffix_18 = value;
		Il2CppCodeGenWriteBarrier((&___iosURLSuffix_18), value);
	}

	inline static int32_t get_offset_of_appLinkSchemes_19() { return static_cast<int32_t>(offsetof(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D, ___appLinkSchemes_19)); }
	inline List_1_tEBAA293BF803F2D17E20E8B6FE34EF3792F6FA6B * get_appLinkSchemes_19() const { return ___appLinkSchemes_19; }
	inline List_1_tEBAA293BF803F2D17E20E8B6FE34EF3792F6FA6B ** get_address_of_appLinkSchemes_19() { return &___appLinkSchemes_19; }
	inline void set_appLinkSchemes_19(List_1_tEBAA293BF803F2D17E20E8B6FE34EF3792F6FA6B * value)
	{
		___appLinkSchemes_19 = value;
		Il2CppCodeGenWriteBarrier((&___appLinkSchemes_19), value);
	}

	inline static int32_t get_offset_of_uploadAccessToken_20() { return static_cast<int32_t>(offsetof(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D, ___uploadAccessToken_20)); }
	inline String_t* get_uploadAccessToken_20() const { return ___uploadAccessToken_20; }
	inline String_t** get_address_of_uploadAccessToken_20() { return &___uploadAccessToken_20; }
	inline void set_uploadAccessToken_20(String_t* value)
	{
		___uploadAccessToken_20 = value;
		Il2CppCodeGenWriteBarrier((&___uploadAccessToken_20), value);
	}

	inline static int32_t get_offset_of_autoLogAppEventsEnabled_21() { return static_cast<int32_t>(offsetof(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D, ___autoLogAppEventsEnabled_21)); }
	inline bool get_autoLogAppEventsEnabled_21() const { return ___autoLogAppEventsEnabled_21; }
	inline bool* get_address_of_autoLogAppEventsEnabled_21() { return &___autoLogAppEventsEnabled_21; }
	inline void set_autoLogAppEventsEnabled_21(bool value)
	{
		___autoLogAppEventsEnabled_21 = value;
	}

	inline static int32_t get_offset_of_advertiserIDCollectionEnabled_22() { return static_cast<int32_t>(offsetof(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D, ___advertiserIDCollectionEnabled_22)); }
	inline bool get_advertiserIDCollectionEnabled_22() const { return ___advertiserIDCollectionEnabled_22; }
	inline bool* get_address_of_advertiserIDCollectionEnabled_22() { return &___advertiserIDCollectionEnabled_22; }
	inline void set_advertiserIDCollectionEnabled_22(bool value)
	{
		___advertiserIDCollectionEnabled_22 = value;
	}
};

struct FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_StaticFields
{
public:
	// System.Collections.Generic.List`1<Facebook.Unity.Settings.FacebookSettings_OnChangeCallback> Facebook.Unity.Settings.FacebookSettings::onChangeCallbacks
	List_1_t4761B02A8CB42D5714C127D74C8BD8BCBD280D33 * ___onChangeCallbacks_7;
	// Facebook.Unity.Settings.FacebookSettings Facebook.Unity.Settings.FacebookSettings::instance
	FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D * ___instance_8;

public:
	inline static int32_t get_offset_of_onChangeCallbacks_7() { return static_cast<int32_t>(offsetof(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_StaticFields, ___onChangeCallbacks_7)); }
	inline List_1_t4761B02A8CB42D5714C127D74C8BD8BCBD280D33 * get_onChangeCallbacks_7() const { return ___onChangeCallbacks_7; }
	inline List_1_t4761B02A8CB42D5714C127D74C8BD8BCBD280D33 ** get_address_of_onChangeCallbacks_7() { return &___onChangeCallbacks_7; }
	inline void set_onChangeCallbacks_7(List_1_t4761B02A8CB42D5714C127D74C8BD8BCBD280D33 * value)
	{
		___onChangeCallbacks_7 = value;
		Il2CppCodeGenWriteBarrier((&___onChangeCallbacks_7), value);
	}

	inline static int32_t get_offset_of_instance_8() { return static_cast<int32_t>(offsetof(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_StaticFields, ___instance_8)); }
	inline FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D * get_instance_8() const { return ___instance_8; }
	inline FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D ** get_address_of_instance_8() { return &___instance_8; }
	inline void set_instance_8(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D * value)
	{
		___instance_8 = value;
		Il2CppCodeGenWriteBarrier((&___instance_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACEBOOKSETTINGS_T1A2119548288F7AD79D590C587740A8EED14D74D_H
#ifndef ONCHANGECALLBACK_T490B1A020446401D3A4B7030FA7A0D9AF521AFBB_H
#define ONCHANGECALLBACK_T490B1A020446401D3A4B7030FA7A0D9AF521AFBB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Settings.FacebookSettings_OnChangeCallback
struct  OnChangeCallback_t490B1A020446401D3A4B7030FA7A0D9AF521AFBB  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONCHANGECALLBACK_T490B1A020446401D3A4B7030FA7A0D9AF521AFBB_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef GETRAYINTERSECTIONALLCALLBACK_T68C2581CCF05E868297EBD3F3361274954845095_H
#define GETRAYINTERSECTIONALLCALLBACK_T68C2581CCF05E868297EBD3F3361274954845095_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache_GetRayIntersectionAllCallback
struct  GetRayIntersectionAllCallback_t68C2581CCF05E868297EBD3F3361274954845095  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETRAYINTERSECTIONALLCALLBACK_T68C2581CCF05E868297EBD3F3361274954845095_H
#ifndef GETRAYINTERSECTIONALLNONALLOCCALLBACK_TAD7508D45DB6679B6394983579AD18D967CC2AD4_H
#define GETRAYINTERSECTIONALLNONALLOCCALLBACK_TAD7508D45DB6679B6394983579AD18D967CC2AD4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache_GetRayIntersectionAllNonAllocCallback
struct  GetRayIntersectionAllNonAllocCallback_tAD7508D45DB6679B6394983579AD18D967CC2AD4  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETRAYINTERSECTIONALLNONALLOCCALLBACK_TAD7508D45DB6679B6394983579AD18D967CC2AD4_H
#ifndef GETRAYCASTNONALLOCCALLBACK_TC13D9767CFF00EAB26E9FCC4BDD505F0721A2B4D_H
#define GETRAYCASTNONALLOCCALLBACK_TC13D9767CFF00EAB26E9FCC4BDD505F0721A2B4D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache_GetRaycastNonAllocCallback
struct  GetRaycastNonAllocCallback_tC13D9767CFF00EAB26E9FCC4BDD505F0721A2B4D  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETRAYCASTNONALLOCCALLBACK_TC13D9767CFF00EAB26E9FCC4BDD505F0721A2B4D_H
#ifndef RAYCAST2DCALLBACK_TE99ABF9ABC3A380677949E8C05A3E477889B82BE_H
#define RAYCAST2DCALLBACK_TE99ABF9ABC3A380677949E8C05A3E477889B82BE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache_Raycast2DCallback
struct  Raycast2DCallback_tE99ABF9ABC3A380677949E8C05A3E477889B82BE  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCAST2DCALLBACK_TE99ABF9ABC3A380677949E8C05A3E477889B82BE_H
#ifndef RAYCAST3DCALLBACK_T83483916473C9710AEDB316A65CBE62C58935C5F_H
#define RAYCAST3DCALLBACK_T83483916473C9710AEDB316A65CBE62C58935C5F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache_Raycast3DCallback
struct  Raycast3DCallback_t83483916473C9710AEDB316A65CBE62C58935C5F  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCAST3DCALLBACK_T83483916473C9710AEDB316A65CBE62C58935C5F_H
#ifndef RAYCASTALLCALLBACK_T751407A44270E02FAA43D0846A58EE6A8C4AE1CE_H
#define RAYCASTALLCALLBACK_T751407A44270E02FAA43D0846A58EE6A8C4AE1CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache_RaycastAllCallback
struct  RaycastAllCallback_t751407A44270E02FAA43D0846A58EE6A8C4AE1CE  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTALLCALLBACK_T751407A44270E02FAA43D0846A58EE6A8C4AE1CE_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#define UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#ifndef BASEMESHEFFECT_T72759F31F9D204D7EFB6B45097873809D4524BA5_H
#define BASEMESHEFFECT_T72759F31F9D204D7EFB6B45097873809D4524BA5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.BaseMeshEffect
struct  BaseMeshEffect_t72759F31F9D204D7EFB6B45097873809D4524BA5  : public UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70
{
public:
	// UnityEngine.UI.Graphic UnityEngine.UI.BaseMeshEffect::m_Graphic
	Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * ___m_Graphic_4;

public:
	inline static int32_t get_offset_of_m_Graphic_4() { return static_cast<int32_t>(offsetof(BaseMeshEffect_t72759F31F9D204D7EFB6B45097873809D4524BA5, ___m_Graphic_4)); }
	inline Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * get_m_Graphic_4() const { return ___m_Graphic_4; }
	inline Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 ** get_address_of_m_Graphic_4() { return &___m_Graphic_4; }
	inline void set_m_Graphic_4(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * value)
	{
		___m_Graphic_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Graphic_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEMESHEFFECT_T72759F31F9D204D7EFB6B45097873809D4524BA5_H
#ifndef LAYOUTELEMENT_TD503826DB41B6EA85AC689292F8B2661B3C1048B_H
#define LAYOUTELEMENT_TD503826DB41B6EA85AC689292F8B2661B3C1048B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.LayoutElement
struct  LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B  : public UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70
{
public:
	// System.Boolean UnityEngine.UI.LayoutElement::m_IgnoreLayout
	bool ___m_IgnoreLayout_4;
	// System.Single UnityEngine.UI.LayoutElement::m_MinWidth
	float ___m_MinWidth_5;
	// System.Single UnityEngine.UI.LayoutElement::m_MinHeight
	float ___m_MinHeight_6;
	// System.Single UnityEngine.UI.LayoutElement::m_PreferredWidth
	float ___m_PreferredWidth_7;
	// System.Single UnityEngine.UI.LayoutElement::m_PreferredHeight
	float ___m_PreferredHeight_8;
	// System.Single UnityEngine.UI.LayoutElement::m_FlexibleWidth
	float ___m_FlexibleWidth_9;
	// System.Single UnityEngine.UI.LayoutElement::m_FlexibleHeight
	float ___m_FlexibleHeight_10;
	// System.Int32 UnityEngine.UI.LayoutElement::m_LayoutPriority
	int32_t ___m_LayoutPriority_11;

public:
	inline static int32_t get_offset_of_m_IgnoreLayout_4() { return static_cast<int32_t>(offsetof(LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B, ___m_IgnoreLayout_4)); }
	inline bool get_m_IgnoreLayout_4() const { return ___m_IgnoreLayout_4; }
	inline bool* get_address_of_m_IgnoreLayout_4() { return &___m_IgnoreLayout_4; }
	inline void set_m_IgnoreLayout_4(bool value)
	{
		___m_IgnoreLayout_4 = value;
	}

	inline static int32_t get_offset_of_m_MinWidth_5() { return static_cast<int32_t>(offsetof(LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B, ___m_MinWidth_5)); }
	inline float get_m_MinWidth_5() const { return ___m_MinWidth_5; }
	inline float* get_address_of_m_MinWidth_5() { return &___m_MinWidth_5; }
	inline void set_m_MinWidth_5(float value)
	{
		___m_MinWidth_5 = value;
	}

	inline static int32_t get_offset_of_m_MinHeight_6() { return static_cast<int32_t>(offsetof(LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B, ___m_MinHeight_6)); }
	inline float get_m_MinHeight_6() const { return ___m_MinHeight_6; }
	inline float* get_address_of_m_MinHeight_6() { return &___m_MinHeight_6; }
	inline void set_m_MinHeight_6(float value)
	{
		___m_MinHeight_6 = value;
	}

	inline static int32_t get_offset_of_m_PreferredWidth_7() { return static_cast<int32_t>(offsetof(LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B, ___m_PreferredWidth_7)); }
	inline float get_m_PreferredWidth_7() const { return ___m_PreferredWidth_7; }
	inline float* get_address_of_m_PreferredWidth_7() { return &___m_PreferredWidth_7; }
	inline void set_m_PreferredWidth_7(float value)
	{
		___m_PreferredWidth_7 = value;
	}

	inline static int32_t get_offset_of_m_PreferredHeight_8() { return static_cast<int32_t>(offsetof(LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B, ___m_PreferredHeight_8)); }
	inline float get_m_PreferredHeight_8() const { return ___m_PreferredHeight_8; }
	inline float* get_address_of_m_PreferredHeight_8() { return &___m_PreferredHeight_8; }
	inline void set_m_PreferredHeight_8(float value)
	{
		___m_PreferredHeight_8 = value;
	}

	inline static int32_t get_offset_of_m_FlexibleWidth_9() { return static_cast<int32_t>(offsetof(LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B, ___m_FlexibleWidth_9)); }
	inline float get_m_FlexibleWidth_9() const { return ___m_FlexibleWidth_9; }
	inline float* get_address_of_m_FlexibleWidth_9() { return &___m_FlexibleWidth_9; }
	inline void set_m_FlexibleWidth_9(float value)
	{
		___m_FlexibleWidth_9 = value;
	}

	inline static int32_t get_offset_of_m_FlexibleHeight_10() { return static_cast<int32_t>(offsetof(LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B, ___m_FlexibleHeight_10)); }
	inline float get_m_FlexibleHeight_10() const { return ___m_FlexibleHeight_10; }
	inline float* get_address_of_m_FlexibleHeight_10() { return &___m_FlexibleHeight_10; }
	inline void set_m_FlexibleHeight_10(float value)
	{
		___m_FlexibleHeight_10 = value;
	}

	inline static int32_t get_offset_of_m_LayoutPriority_11() { return static_cast<int32_t>(offsetof(LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B, ___m_LayoutPriority_11)); }
	inline int32_t get_m_LayoutPriority_11() const { return ___m_LayoutPriority_11; }
	inline int32_t* get_address_of_m_LayoutPriority_11() { return &___m_LayoutPriority_11; }
	inline void set_m_LayoutPriority_11(int32_t value)
	{
		___m_LayoutPriority_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYOUTELEMENT_TD503826DB41B6EA85AC689292F8B2661B3C1048B_H
#ifndef LAYOUTGROUP_T9E072B95DA6476C487C0B07A815291249025C0E4_H
#define LAYOUTGROUP_T9E072B95DA6476C487C0B07A815291249025C0E4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.LayoutGroup
struct  LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4  : public UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70
{
public:
	// UnityEngine.RectOffset UnityEngine.UI.LayoutGroup::m_Padding
	RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A * ___m_Padding_4;
	// UnityEngine.TextAnchor UnityEngine.UI.LayoutGroup::m_ChildAlignment
	int32_t ___m_ChildAlignment_5;
	// UnityEngine.RectTransform UnityEngine.UI.LayoutGroup::m_Rect
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_Rect_6;
	// UnityEngine.DrivenRectTransformTracker UnityEngine.UI.LayoutGroup::m_Tracker
	DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03  ___m_Tracker_7;
	// UnityEngine.Vector2 UnityEngine.UI.LayoutGroup::m_TotalMinSize
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_TotalMinSize_8;
	// UnityEngine.Vector2 UnityEngine.UI.LayoutGroup::m_TotalPreferredSize
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_TotalPreferredSize_9;
	// UnityEngine.Vector2 UnityEngine.UI.LayoutGroup::m_TotalFlexibleSize
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_TotalFlexibleSize_10;
	// System.Collections.Generic.List`1<UnityEngine.RectTransform> UnityEngine.UI.LayoutGroup::m_RectChildren
	List_1_t0CD9761E1DF9817484CF4FB4253C6A626DC2311C * ___m_RectChildren_11;

public:
	inline static int32_t get_offset_of_m_Padding_4() { return static_cast<int32_t>(offsetof(LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4, ___m_Padding_4)); }
	inline RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A * get_m_Padding_4() const { return ___m_Padding_4; }
	inline RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A ** get_address_of_m_Padding_4() { return &___m_Padding_4; }
	inline void set_m_Padding_4(RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A * value)
	{
		___m_Padding_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Padding_4), value);
	}

	inline static int32_t get_offset_of_m_ChildAlignment_5() { return static_cast<int32_t>(offsetof(LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4, ___m_ChildAlignment_5)); }
	inline int32_t get_m_ChildAlignment_5() const { return ___m_ChildAlignment_5; }
	inline int32_t* get_address_of_m_ChildAlignment_5() { return &___m_ChildAlignment_5; }
	inline void set_m_ChildAlignment_5(int32_t value)
	{
		___m_ChildAlignment_5 = value;
	}

	inline static int32_t get_offset_of_m_Rect_6() { return static_cast<int32_t>(offsetof(LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4, ___m_Rect_6)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_Rect_6() const { return ___m_Rect_6; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_Rect_6() { return &___m_Rect_6; }
	inline void set_m_Rect_6(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_Rect_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rect_6), value);
	}

	inline static int32_t get_offset_of_m_Tracker_7() { return static_cast<int32_t>(offsetof(LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4, ___m_Tracker_7)); }
	inline DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03  get_m_Tracker_7() const { return ___m_Tracker_7; }
	inline DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03 * get_address_of_m_Tracker_7() { return &___m_Tracker_7; }
	inline void set_m_Tracker_7(DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03  value)
	{
		___m_Tracker_7 = value;
	}

	inline static int32_t get_offset_of_m_TotalMinSize_8() { return static_cast<int32_t>(offsetof(LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4, ___m_TotalMinSize_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_TotalMinSize_8() const { return ___m_TotalMinSize_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_TotalMinSize_8() { return &___m_TotalMinSize_8; }
	inline void set_m_TotalMinSize_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_TotalMinSize_8 = value;
	}

	inline static int32_t get_offset_of_m_TotalPreferredSize_9() { return static_cast<int32_t>(offsetof(LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4, ___m_TotalPreferredSize_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_TotalPreferredSize_9() const { return ___m_TotalPreferredSize_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_TotalPreferredSize_9() { return &___m_TotalPreferredSize_9; }
	inline void set_m_TotalPreferredSize_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_TotalPreferredSize_9 = value;
	}

	inline static int32_t get_offset_of_m_TotalFlexibleSize_10() { return static_cast<int32_t>(offsetof(LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4, ___m_TotalFlexibleSize_10)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_TotalFlexibleSize_10() const { return ___m_TotalFlexibleSize_10; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_TotalFlexibleSize_10() { return &___m_TotalFlexibleSize_10; }
	inline void set_m_TotalFlexibleSize_10(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_TotalFlexibleSize_10 = value;
	}

	inline static int32_t get_offset_of_m_RectChildren_11() { return static_cast<int32_t>(offsetof(LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4, ___m_RectChildren_11)); }
	inline List_1_t0CD9761E1DF9817484CF4FB4253C6A626DC2311C * get_m_RectChildren_11() const { return ___m_RectChildren_11; }
	inline List_1_t0CD9761E1DF9817484CF4FB4253C6A626DC2311C ** get_address_of_m_RectChildren_11() { return &___m_RectChildren_11; }
	inline void set_m_RectChildren_11(List_1_t0CD9761E1DF9817484CF4FB4253C6A626DC2311C * value)
	{
		___m_RectChildren_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectChildren_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYOUTGROUP_T9E072B95DA6476C487C0B07A815291249025C0E4_H
#ifndef HORIZONTALORVERTICALLAYOUTGROUP_TFE5C3DB19C2CC4906B3E5D5F4E1966CB585174AB_H
#define HORIZONTALORVERTICALLAYOUTGROUP_TFE5C3DB19C2CC4906B3E5D5F4E1966CB585174AB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.HorizontalOrVerticalLayoutGroup
struct  HorizontalOrVerticalLayoutGroup_tFE5C3DB19C2CC4906B3E5D5F4E1966CB585174AB  : public LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4
{
public:
	// System.Single UnityEngine.UI.HorizontalOrVerticalLayoutGroup::m_Spacing
	float ___m_Spacing_12;
	// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::m_ChildForceExpandWidth
	bool ___m_ChildForceExpandWidth_13;
	// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::m_ChildForceExpandHeight
	bool ___m_ChildForceExpandHeight_14;
	// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::m_ChildControlWidth
	bool ___m_ChildControlWidth_15;
	// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::m_ChildControlHeight
	bool ___m_ChildControlHeight_16;

public:
	inline static int32_t get_offset_of_m_Spacing_12() { return static_cast<int32_t>(offsetof(HorizontalOrVerticalLayoutGroup_tFE5C3DB19C2CC4906B3E5D5F4E1966CB585174AB, ___m_Spacing_12)); }
	inline float get_m_Spacing_12() const { return ___m_Spacing_12; }
	inline float* get_address_of_m_Spacing_12() { return &___m_Spacing_12; }
	inline void set_m_Spacing_12(float value)
	{
		___m_Spacing_12 = value;
	}

	inline static int32_t get_offset_of_m_ChildForceExpandWidth_13() { return static_cast<int32_t>(offsetof(HorizontalOrVerticalLayoutGroup_tFE5C3DB19C2CC4906B3E5D5F4E1966CB585174AB, ___m_ChildForceExpandWidth_13)); }
	inline bool get_m_ChildForceExpandWidth_13() const { return ___m_ChildForceExpandWidth_13; }
	inline bool* get_address_of_m_ChildForceExpandWidth_13() { return &___m_ChildForceExpandWidth_13; }
	inline void set_m_ChildForceExpandWidth_13(bool value)
	{
		___m_ChildForceExpandWidth_13 = value;
	}

	inline static int32_t get_offset_of_m_ChildForceExpandHeight_14() { return static_cast<int32_t>(offsetof(HorizontalOrVerticalLayoutGroup_tFE5C3DB19C2CC4906B3E5D5F4E1966CB585174AB, ___m_ChildForceExpandHeight_14)); }
	inline bool get_m_ChildForceExpandHeight_14() const { return ___m_ChildForceExpandHeight_14; }
	inline bool* get_address_of_m_ChildForceExpandHeight_14() { return &___m_ChildForceExpandHeight_14; }
	inline void set_m_ChildForceExpandHeight_14(bool value)
	{
		___m_ChildForceExpandHeight_14 = value;
	}

	inline static int32_t get_offset_of_m_ChildControlWidth_15() { return static_cast<int32_t>(offsetof(HorizontalOrVerticalLayoutGroup_tFE5C3DB19C2CC4906B3E5D5F4E1966CB585174AB, ___m_ChildControlWidth_15)); }
	inline bool get_m_ChildControlWidth_15() const { return ___m_ChildControlWidth_15; }
	inline bool* get_address_of_m_ChildControlWidth_15() { return &___m_ChildControlWidth_15; }
	inline void set_m_ChildControlWidth_15(bool value)
	{
		___m_ChildControlWidth_15 = value;
	}

	inline static int32_t get_offset_of_m_ChildControlHeight_16() { return static_cast<int32_t>(offsetof(HorizontalOrVerticalLayoutGroup_tFE5C3DB19C2CC4906B3E5D5F4E1966CB585174AB, ___m_ChildControlHeight_16)); }
	inline bool get_m_ChildControlHeight_16() const { return ___m_ChildControlHeight_16; }
	inline bool* get_address_of_m_ChildControlHeight_16() { return &___m_ChildControlHeight_16; }
	inline void set_m_ChildControlHeight_16(bool value)
	{
		___m_ChildControlHeight_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HORIZONTALORVERTICALLAYOUTGROUP_TFE5C3DB19C2CC4906B3E5D5F4E1966CB585174AB_H
#ifndef POSITIONASUV1_T26F06E879E7B8DD2F93B8B3643053534D82F684A_H
#define POSITIONASUV1_T26F06E879E7B8DD2F93B8B3643053534D82F684A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.PositionAsUV1
struct  PositionAsUV1_t26F06E879E7B8DD2F93B8B3643053534D82F684A  : public BaseMeshEffect_t72759F31F9D204D7EFB6B45097873809D4524BA5
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSITIONASUV1_T26F06E879E7B8DD2F93B8B3643053534D82F684A_H
#ifndef SHADOW_TA03D2493843CDF8E64569F985AEB3FEEEEB412E1_H
#define SHADOW_TA03D2493843CDF8E64569F985AEB3FEEEEB412E1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Shadow
struct  Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1  : public BaseMeshEffect_t72759F31F9D204D7EFB6B45097873809D4524BA5
{
public:
	// UnityEngine.Color UnityEngine.UI.Shadow::m_EffectColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_EffectColor_5;
	// UnityEngine.Vector2 UnityEngine.UI.Shadow::m_EffectDistance
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_EffectDistance_6;
	// System.Boolean UnityEngine.UI.Shadow::m_UseGraphicAlpha
	bool ___m_UseGraphicAlpha_7;

public:
	inline static int32_t get_offset_of_m_EffectColor_5() { return static_cast<int32_t>(offsetof(Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1, ___m_EffectColor_5)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_EffectColor_5() const { return ___m_EffectColor_5; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_EffectColor_5() { return &___m_EffectColor_5; }
	inline void set_m_EffectColor_5(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_EffectColor_5 = value;
	}

	inline static int32_t get_offset_of_m_EffectDistance_6() { return static_cast<int32_t>(offsetof(Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1, ___m_EffectDistance_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_EffectDistance_6() const { return ___m_EffectDistance_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_EffectDistance_6() { return &___m_EffectDistance_6; }
	inline void set_m_EffectDistance_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_EffectDistance_6 = value;
	}

	inline static int32_t get_offset_of_m_UseGraphicAlpha_7() { return static_cast<int32_t>(offsetof(Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1, ___m_UseGraphicAlpha_7)); }
	inline bool get_m_UseGraphicAlpha_7() const { return ___m_UseGraphicAlpha_7; }
	inline bool* get_address_of_m_UseGraphicAlpha_7() { return &___m_UseGraphicAlpha_7; }
	inline void set_m_UseGraphicAlpha_7(bool value)
	{
		___m_UseGraphicAlpha_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADOW_TA03D2493843CDF8E64569F985AEB3FEEEEB412E1_H
#ifndef OUTLINE_TB750E496976B072E79142D51C0A991AC20183095_H
#define OUTLINE_TB750E496976B072E79142D51C0A991AC20183095_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Outline
struct  Outline_tB750E496976B072E79142D51C0A991AC20183095  : public Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OUTLINE_TB750E496976B072E79142D51C0A991AC20183095_H
#ifndef VERTICALLAYOUTGROUP_TAAEE0BAA82E9A110591DEC9A3FFC25A01C2FFA11_H
#define VERTICALLAYOUTGROUP_TAAEE0BAA82E9A110591DEC9A3FFC25A01C2FFA11_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.VerticalLayoutGroup
struct  VerticalLayoutGroup_tAAEE0BAA82E9A110591DEC9A3FFC25A01C2FFA11  : public HorizontalOrVerticalLayoutGroup_tFE5C3DB19C2CC4906B3E5D5F4E1966CB585174AB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTICALLAYOUTGROUP_TAAEE0BAA82E9A110591DEC9A3FFC25A01C2FFA11_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5800 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5801 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5802 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5803 = { sizeof (LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5803[8] = 
{
	LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B::get_offset_of_m_IgnoreLayout_4(),
	LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B::get_offset_of_m_MinWidth_5(),
	LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B::get_offset_of_m_MinHeight_6(),
	LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B::get_offset_of_m_PreferredWidth_7(),
	LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B::get_offset_of_m_PreferredHeight_8(),
	LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B::get_offset_of_m_FlexibleWidth_9(),
	LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B::get_offset_of_m_FlexibleHeight_10(),
	LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B::get_offset_of_m_LayoutPriority_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5804 = { sizeof (LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5804[8] = 
{
	LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4::get_offset_of_m_Padding_4(),
	LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4::get_offset_of_m_ChildAlignment_5(),
	LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4::get_offset_of_m_Rect_6(),
	LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4::get_offset_of_m_Tracker_7(),
	LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4::get_offset_of_m_TotalMinSize_8(),
	LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4::get_offset_of_m_TotalPreferredSize_9(),
	LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4::get_offset_of_m_TotalFlexibleSize_10(),
	LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4::get_offset_of_m_RectChildren_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5805 = { sizeof (U3CDelayedSetDirtyU3Ec__Iterator0_tB8BB61C2C033D95240B4E2704971663E4DC08073), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5805[4] = 
{
	U3CDelayedSetDirtyU3Ec__Iterator0_tB8BB61C2C033D95240B4E2704971663E4DC08073::get_offset_of_rectTransform_0(),
	U3CDelayedSetDirtyU3Ec__Iterator0_tB8BB61C2C033D95240B4E2704971663E4DC08073::get_offset_of_U24current_1(),
	U3CDelayedSetDirtyU3Ec__Iterator0_tB8BB61C2C033D95240B4E2704971663E4DC08073::get_offset_of_U24disposing_2(),
	U3CDelayedSetDirtyU3Ec__Iterator0_tB8BB61C2C033D95240B4E2704971663E4DC08073::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5806 = { sizeof (LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD), -1, sizeof(LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5806[9] = 
{
	LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD::get_offset_of_m_ToRebuild_0(),
	LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD::get_offset_of_m_CachedHashFromTransform_1(),
	LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD_StaticFields::get_offset_of_s_Rebuilders_2(),
	LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_3(),
	LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_4(),
	LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_5(),
	LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_6(),
	LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_7(),
	LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5807 = { sizeof (LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5), -1, sizeof(LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5807[8] = 
{
	LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_0(),
	LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_1(),
	LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_2(),
	LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_3(),
	LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_4(),
	LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_5(),
	LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_6(),
	LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5808 = { sizeof (VerticalLayoutGroup_tAAEE0BAA82E9A110591DEC9A3FFC25A01C2FFA11), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5809 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5810 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5810[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5811 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5811[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5812 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5812[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5813 = { sizeof (ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A), -1, sizeof(ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5813[7] = 
{
	ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A::get_offset_of_raycast3D_0(),
	ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A::get_offset_of_raycast3DAll_1(),
	ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A::get_offset_of_raycast2D_2(),
	ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A::get_offset_of_getRayIntersectionAll_3(),
	ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A::get_offset_of_getRayIntersectionAllNonAlloc_4(),
	ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A::get_offset_of_getRaycastNonAlloc_5(),
	ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A_StaticFields::get_offset_of_s_ReflectionMethodsCache_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5814 = { sizeof (Raycast3DCallback_t83483916473C9710AEDB316A65CBE62C58935C5F), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5815 = { sizeof (Raycast2DCallback_tE99ABF9ABC3A380677949E8C05A3E477889B82BE), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5816 = { sizeof (RaycastAllCallback_t751407A44270E02FAA43D0846A58EE6A8C4AE1CE), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5817 = { sizeof (GetRayIntersectionAllCallback_t68C2581CCF05E868297EBD3F3361274954845095), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5818 = { sizeof (GetRayIntersectionAllNonAllocCallback_tAD7508D45DB6679B6394983579AD18D967CC2AD4), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5819 = { sizeof (GetRaycastNonAllocCallback_tC13D9767CFF00EAB26E9FCC4BDD505F0721A2B4D), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5820 = { sizeof (VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F), -1, sizeof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5820[12] = 
{
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_Positions_0(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_Colors_1(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_Uv0S_2(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_Uv1S_3(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_Uv2S_4(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_Uv3S_5(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_Normals_6(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_Tangents_7(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_Indices_8(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F_StaticFields::get_offset_of_s_DefaultTangent_9(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F_StaticFields::get_offset_of_s_DefaultNormal_10(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_ListsInitalized_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5821 = { sizeof (VertexHelperExtension_t593AF80A941B7208D2CC88AE671873CE6E2AA8E3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5822 = { sizeof (BaseVertexEffect_t1EF95AB1FC33A027710E7DC86D19F700156C4F6A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5823 = { sizeof (BaseMeshEffect_t72759F31F9D204D7EFB6B45097873809D4524BA5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5823[1] = 
{
	BaseMeshEffect_t72759F31F9D204D7EFB6B45097873809D4524BA5::get_offset_of_m_Graphic_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5824 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5825 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5826 = { sizeof (Outline_tB750E496976B072E79142D51C0A991AC20183095), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5827 = { sizeof (PositionAsUV1_t26F06E879E7B8DD2F93B8B3643053534D82F684A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5828 = { sizeof (Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5828[4] = 
{
	Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1::get_offset_of_m_EffectColor_5(),
	Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1::get_offset_of_m_EffectDistance_6(),
	Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1::get_offset_of_m_UseGraphicAlpha_7(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5829 = { sizeof (U3CPrivateImplementationDetailsU3E_tC8332394FBFEEB4B73459A35E182942340DA3537), -1, sizeof(U3CPrivateImplementationDetailsU3E_tC8332394FBFEEB4B73459A35E182942340DA3537_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5829[1] = 
{
	U3CPrivateImplementationDetailsU3E_tC8332394FBFEEB4B73459A35E182942340DA3537_StaticFields::get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5830 = { sizeof (U24ArrayTypeU3D12_t25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D12_t25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5831 = { sizeof (U3CModuleU3E_t48807405FE1B7A134A75505377E34131ABC2EBA2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5832 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5832[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5833 = { sizeof (U3CModuleU3E_t7D11BB4BD4C1ADC1F64CC310633FC1BEE61632FE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5834 = { sizeof (FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D), -1, sizeof(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5834[19] = 
{
	0,
	0,
	0,
	FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_StaticFields::get_offset_of_onChangeCallbacks_7(),
	FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_StaticFields::get_offset_of_instance_8(),
	FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D::get_offset_of_selectedAppIndex_9(),
	FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D::get_offset_of_clientTokens_10(),
	FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D::get_offset_of_appIds_11(),
	FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D::get_offset_of_appLabels_12(),
	FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D::get_offset_of_cookie_13(),
	FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D::get_offset_of_logging_14(),
	FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D::get_offset_of_status_15(),
	FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D::get_offset_of_xfbml_16(),
	FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D::get_offset_of_frictionlessRequests_17(),
	FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D::get_offset_of_iosURLSuffix_18(),
	FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D::get_offset_of_appLinkSchemes_19(),
	FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D::get_offset_of_uploadAccessToken_20(),
	FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D::get_offset_of_autoLogAppEventsEnabled_21(),
	FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D::get_offset_of_advertiserIDCollectionEnabled_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5835 = { sizeof (OnChangeCallback_t490B1A020446401D3A4B7030FA7A0D9AF521AFBB), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5836 = { sizeof (UrlSchemes_t7B8D8455C2FE4B28F5F677FFAE86E14077D94D8C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5836[1] = 
{
	UrlSchemes_t7B8D8455C2FE4B28F5F677FFAE86E14077D94D8C::get_offset_of_list_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5837 = { sizeof (U3CU3Ec_t40659AD935CCBCC35589D12303DF7D93D8B4ABA8), -1, sizeof(U3CU3Ec_t40659AD935CCBCC35589D12303DF7D93D8B4ABA8_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5837[2] = 
{
	U3CU3Ec_t40659AD935CCBCC35589D12303DF7D93D8B4ABA8_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t40659AD935CCBCC35589D12303DF7D93D8B4ABA8_StaticFields::get_offset_of_U3CU3E9__76_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5838 = { sizeof (U3CModuleU3E_tD0CA5CB8F838362C11DAED8A9A3159C658C1998B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5839 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5839[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5840 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5840[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5841 = { sizeof (GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774), -1, sizeof(GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5841[34] = 
{
	GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774::get_offset_of__paused_0(),
	GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774::get_offset_of__sessionId_1(),
	GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774::get_offset_of__sendQueue_2(),
	GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774::get_offset_of__persistantQueue_3(),
	GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774::get_offset_of__requestedActions_4(),
	GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774::get_offset_of__persistantQueueUserId_5(),
	GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774::get_offset_of__connections_6(),
	GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774::get_offset_of__mainLoopTimer_7(),
	GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774::get_offset_of__durableWriteTimer_8(),
	GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774::get_offset_of__durableQueueDirty_9(),
	GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774::get_offset_of__requestCounter_10(),
	GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774::get_offset_of__currentSocketUrl_11(),
	GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774::get_offset_of__ready_12(),
	GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774::get_offset_of__durableQueuePaused_13(),
	GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774::get_offset_of__durableDrainTimer_14(),
	GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774::get_offset_of__currAttemptConnection_15(),
	GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774::get_offset_of__nextReconnect_16(),
	GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774_StaticFields::get_offset_of__random_17(),
	GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774::get_offset_of__retryBase_18(),
	GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774::get_offset_of__retryMax_19(),
	GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774::get_offset_of__requestTimeout_20(),
	GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774::get_offset_of__durableConcurrentRequests_21(),
	GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774::get_offset_of__durableDrainInterval_22(),
	GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774::get_offset_of__handshakeOffset_23(),
	GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774_StaticFields::get_offset_of_Instances_24(),
	GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774_StaticFields::get_offset_of_serviceUrlBase_25(),
	GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774::get_offset_of_U3CGSPlatformU3Ek__BackingField_26(),
	GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774::get_offset_of_U3CNameU3Ek__BackingField_27(),
	GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774::get_offset_of_U3CTraceMessagesU3Ek__BackingField_28(),
	GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774::get_offset_of_U3CDurableQueueLoadedU3Ek__BackingField_29(),
	GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774::get_offset_of_U3COnGameSparksNonceU3Ek__BackingField_30(),
	GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774::get_offset_of_U3CDurableQueueRunningU3Ek__BackingField_31(),
	GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774::get_offset_of_U3CGameSparksAvailableU3Ek__BackingField_32(),
	GSInstance_t595DBFD43D1F55EA5ABE397A222D1AE9AD307774::get_offset_of_U3CGameSparksAuthenticatedU3Ek__BackingField_33(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5842 = { sizeof (U3CU3Ec__DisplayClass1_t35DB9761FA638ED6DE7BCF9CA2C379832C1F00B9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5842[2] = 
{
	U3CU3Ec__DisplayClass1_t35DB9761FA638ED6DE7BCF9CA2C379832C1F00B9::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass1_t35DB9761FA638ED6DE7BCF9CA2C379832C1F00B9::get_offset_of_onDone_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5843 = { sizeof (U3CU3Ec__DisplayClass9_t36DC1EED2B2386EA20D51512BC4B47C97E598E94), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5843[2] = 
{
	U3CU3Ec__DisplayClass9_t36DC1EED2B2386EA20D51512BC4B47C97E598E94::get_offset_of_response_0(),
	U3CU3Ec__DisplayClass9_t36DC1EED2B2386EA20D51512BC4B47C97E598E94::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5844 = { sizeof (U3CU3Ec__DisplayClassf_t76A25F8FA80AD871F1C384D17CF5F1E17D4B1066), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5844[2] = 
{
	U3CU3Ec__DisplayClassf_t76A25F8FA80AD871F1C384D17CF5F1E17D4B1066::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClassf_t76A25F8FA80AD871F1C384D17CF5F1E17D4B1066::get_offset_of_response_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5845 = { sizeof (U3CU3Ec__DisplayClass12_t814181B763214C49099CD4172575AA692A09DA5A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5845[2] = 
{
	U3CU3Ec__DisplayClass12_t814181B763214C49099CD4172575AA692A09DA5A::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass12_t814181B763214C49099CD4172575AA692A09DA5A::get_offset_of_request_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5846 = { sizeof (U3CU3Ec__DisplayClass16_t5E2D53B935ECEFF315042EB3490BFE1049902397), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5846[2] = 
{
	U3CU3Ec__DisplayClass16_t5E2D53B935ECEFF315042EB3490BFE1049902397::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass16_t5E2D53B935ECEFF315042EB3490BFE1049902397::get_offset_of_avail_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5847 = { sizeof (U3CU3Ec__DisplayClass19_t1DD3343E8C4DFEA6288E457C5B5688599E527554), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5847[2] = 
{
	U3CU3Ec__DisplayClass19_t1DD3343E8C4DFEA6288E457C5B5688599E527554::get_offset_of_e_0(),
	U3CU3Ec__DisplayClass19_t1DD3343E8C4DFEA6288E457C5B5688599E527554::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5848 = { sizeof (U3CU3Ec__DisplayClass1c_tD18D627568F837BD8A71FE22C6BE8D0B1661118D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5848[3] = 
{
	U3CU3Ec__DisplayClass1c_tD18D627568F837BD8A71FE22C6BE8D0B1661118D::get_offset_of_error_0(),
	U3CU3Ec__DisplayClass1c_tD18D627568F837BD8A71FE22C6BE8D0B1661118D::get_offset_of_U3CU3E4__this_1(),
	U3CU3Ec__DisplayClass1c_tD18D627568F837BD8A71FE22C6BE8D0B1661118D::get_offset_of_request_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5849 = { sizeof (U3CU3Ec__DisplayClass20_t9E2EE9F0CFD8AFD6355464F612E393BEB7600EDE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5849[2] = 
{
	U3CU3Ec__DisplayClass20_t9E2EE9F0CFD8AFD6355464F612E393BEB7600EDE::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass20_t9E2EE9F0CFD8AFD6355464F612E393BEB7600EDE::get_offset_of_response_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5850 = { sizeof (U3CU3Ec__DisplayClass23_tDCBE81CF506E0DE1C2C2327F0252BAF59DB5A963), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5850[3] = 
{
	U3CU3Ec__DisplayClass23_tDCBE81CF506E0DE1C2C2327F0252BAF59DB5A963::get_offset_of_previous_durableQueuePaused_0(),
	U3CU3Ec__DisplayClass23_tDCBE81CF506E0DE1C2C2327F0252BAF59DB5A963::get_offset_of_U3CU3E4__this_1(),
	U3CU3Ec__DisplayClass23_tDCBE81CF506E0DE1C2C2327F0252BAF59DB5A963::get_offset_of_userId_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5851 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5852 = { sizeof (GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5852[1] = 
{
	GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937::get_offset_of__data_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5853 = { sizeof (GSRequestData_t84713ABAAB257F300A1DB26F09C91AB0519109F8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5854 = { sizeof (GSObject_t05160F6677D08913C6CD9AB7C7DD9454D3B12E88), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5855 = { sizeof (Version_t32E31145F06ABDA55EC856314710B188369A8132), -1, sizeof(Version_t32E31145F06ABDA55EC856314710B188369A8132_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5855[3] = 
{
	Version_t32E31145F06ABDA55EC856314710B188369A8132_StaticFields::get_offset_of_currentVersion_0(),
	Version_t32E31145F06ABDA55EC856314710B188369A8132_StaticFields::get_offset_of_buildType_1(),
	Version_t32E31145F06ABDA55EC856314710B188369A8132_StaticFields::get_offset_of_buildId_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5856 = { sizeof (GSTypedResponse_t8D7A8C3784E1A1542ED006C5A97AF8C6D9E849DA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5856[1] = 
{
	GSTypedResponse_t8D7A8C3784E1A1542ED006C5A97AF8C6D9E849DA::get_offset_of_response_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5857 = { sizeof (GSMessage_t7604A625444D075A08A76A8029C3AE5534733F7E), -1, sizeof(GSMessage_t7604A625444D075A08A76A8029C3AE5534733F7E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5857[2] = 
{
	GSMessage_t7604A625444D075A08A76A8029C3AE5534733F7E_StaticFields::get_offset_of_handlers_1(),
	GSMessage_t7604A625444D075A08A76A8029C3AE5534733F7E::get_offset_of_U3CgsInstanceU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5858 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5858[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5859 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5859[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5860 = { sizeof (GSAllMessageTypes_t04640013901A87C78C237ABAAD07C092912D5FAA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5861 = { sizeof (AchievementEarnedMessage_t04D1FB4BB1949FC0701210628065D11CBA6A3B44), -1, sizeof(AchievementEarnedMessage_t04D1FB4BB1949FC0701210628065D11CBA6A3B44_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5861[1] = 
{
	AchievementEarnedMessage_t04D1FB4BB1949FC0701210628065D11CBA6A3B44_StaticFields::get_offset_of_Listener_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5862 = { sizeof (ChallengeAcceptedMessage_t1C5FABDC00005A13C574E2D3B493001CE59B8121), -1, sizeof(ChallengeAcceptedMessage_t1C5FABDC00005A13C574E2D3B493001CE59B8121_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5862[1] = 
{
	ChallengeAcceptedMessage_t1C5FABDC00005A13C574E2D3B493001CE59B8121_StaticFields::get_offset_of_Listener_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5863 = { sizeof (ChallengeChangedMessage_t60942905C25ECF06313DA7327B35F2AE96028A79), -1, sizeof(ChallengeChangedMessage_t60942905C25ECF06313DA7327B35F2AE96028A79_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5863[1] = 
{
	ChallengeChangedMessage_t60942905C25ECF06313DA7327B35F2AE96028A79_StaticFields::get_offset_of_Listener_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5864 = { sizeof (ChallengeChatMessage_tBA97BA8BA44ABD87265FE6BF4C169CF25DB1B466), -1, sizeof(ChallengeChatMessage_tBA97BA8BA44ABD87265FE6BF4C169CF25DB1B466_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5864[1] = 
{
	ChallengeChatMessage_tBA97BA8BA44ABD87265FE6BF4C169CF25DB1B466_StaticFields::get_offset_of_Listener_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5865 = { sizeof (ChallengeDeclinedMessage_t8E31D2629D3F244241E2179407245F3E10DA4D94), -1, sizeof(ChallengeDeclinedMessage_t8E31D2629D3F244241E2179407245F3E10DA4D94_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5865[1] = 
{
	ChallengeDeclinedMessage_t8E31D2629D3F244241E2179407245F3E10DA4D94_StaticFields::get_offset_of_Listener_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5866 = { sizeof (ChallengeDrawnMessage_t7997954B69A0D09E76C2B1E120F1D88795F05E0C), -1, sizeof(ChallengeDrawnMessage_t7997954B69A0D09E76C2B1E120F1D88795F05E0C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5866[1] = 
{
	ChallengeDrawnMessage_t7997954B69A0D09E76C2B1E120F1D88795F05E0C_StaticFields::get_offset_of_Listener_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5867 = { sizeof (ChallengeExpiredMessage_tD4CCAD5DBF84E0C66C7AFB32AFE92CB71482AAFB), -1, sizeof(ChallengeExpiredMessage_tD4CCAD5DBF84E0C66C7AFB32AFE92CB71482AAFB_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5867[1] = 
{
	ChallengeExpiredMessage_tD4CCAD5DBF84E0C66C7AFB32AFE92CB71482AAFB_StaticFields::get_offset_of_Listener_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5868 = { sizeof (ChallengeIssuedMessage_t692A7D05140D0AE3F3F260C43C9B9F320D018062), -1, sizeof(ChallengeIssuedMessage_t692A7D05140D0AE3F3F260C43C9B9F320D018062_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5868[1] = 
{
	ChallengeIssuedMessage_t692A7D05140D0AE3F3F260C43C9B9F320D018062_StaticFields::get_offset_of_Listener_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5869 = { sizeof (ChallengeJoinedMessage_t7752AC7D49A555FCF12141B06097EF11F0E40178), -1, sizeof(ChallengeJoinedMessage_t7752AC7D49A555FCF12141B06097EF11F0E40178_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5869[1] = 
{
	ChallengeJoinedMessage_t7752AC7D49A555FCF12141B06097EF11F0E40178_StaticFields::get_offset_of_Listener_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5870 = { sizeof (ChallengeLapsedMessage_t955D2E09895E49F556CE70D497DE299F1DCC4075), -1, sizeof(ChallengeLapsedMessage_t955D2E09895E49F556CE70D497DE299F1DCC4075_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5870[1] = 
{
	ChallengeLapsedMessage_t955D2E09895E49F556CE70D497DE299F1DCC4075_StaticFields::get_offset_of_Listener_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5871 = { sizeof (ChallengeLostMessage_tB2EC2E4D16CE555E52065D691EC467F2EA753A27), -1, sizeof(ChallengeLostMessage_tB2EC2E4D16CE555E52065D691EC467F2EA753A27_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5871[1] = 
{
	ChallengeLostMessage_tB2EC2E4D16CE555E52065D691EC467F2EA753A27_StaticFields::get_offset_of_Listener_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5872 = { sizeof (ChallengeStartedMessage_t63C6EF31A5A328B29573BE452F6643EA7871EF1F), -1, sizeof(ChallengeStartedMessage_t63C6EF31A5A328B29573BE452F6643EA7871EF1F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5872[1] = 
{
	ChallengeStartedMessage_t63C6EF31A5A328B29573BE452F6643EA7871EF1F_StaticFields::get_offset_of_Listener_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5873 = { sizeof (ChallengeTurnTakenMessage_t86FBCC75AD0C2A7A058711DA510859B54A796DC0), -1, sizeof(ChallengeTurnTakenMessage_t86FBCC75AD0C2A7A058711DA510859B54A796DC0_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5873[1] = 
{
	ChallengeTurnTakenMessage_t86FBCC75AD0C2A7A058711DA510859B54A796DC0_StaticFields::get_offset_of_Listener_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5874 = { sizeof (ChallengeWaitingMessage_t109E5AC30730E48822A39E97E761753541F9524B), -1, sizeof(ChallengeWaitingMessage_t109E5AC30730E48822A39E97E761753541F9524B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5874[1] = 
{
	ChallengeWaitingMessage_t109E5AC30730E48822A39E97E761753541F9524B_StaticFields::get_offset_of_Listener_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5875 = { sizeof (ChallengeWithdrawnMessage_t38E16E33339FD988BCA5A411D5A0F6555752BA65), -1, sizeof(ChallengeWithdrawnMessage_t38E16E33339FD988BCA5A411D5A0F6555752BA65_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5875[1] = 
{
	ChallengeWithdrawnMessage_t38E16E33339FD988BCA5A411D5A0F6555752BA65_StaticFields::get_offset_of_Listener_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5876 = { sizeof (ChallengeWonMessage_tBD726ED8E8AEF2B7444866673B4B28AD2EC34E20), -1, sizeof(ChallengeWonMessage_tBD726ED8E8AEF2B7444866673B4B28AD2EC34E20_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5876[1] = 
{
	ChallengeWonMessage_tBD726ED8E8AEF2B7444866673B4B28AD2EC34E20_StaticFields::get_offset_of_Listener_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5877 = { sizeof (FriendMessage_tFF3C4B6D8A422595373A76D204E3F374DD8F9AFF), -1, sizeof(FriendMessage_tFF3C4B6D8A422595373A76D204E3F374DD8F9AFF_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5877[1] = 
{
	FriendMessage_tFF3C4B6D8A422595373A76D204E3F374DD8F9AFF_StaticFields::get_offset_of_Listener_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5878 = { sizeof (GlobalRankChangedMessage_tD39110C476CDA7239C4BE780F73A74A799A675FB), -1, sizeof(GlobalRankChangedMessage_tD39110C476CDA7239C4BE780F73A74A799A675FB_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5878[1] = 
{
	GlobalRankChangedMessage_tD39110C476CDA7239C4BE780F73A74A799A675FB_StaticFields::get_offset_of_Listener_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5879 = { sizeof (MatchFoundMessage_t9D3942DC180FCB9271AEFB4191E45257D982405D), -1, sizeof(MatchFoundMessage_t9D3942DC180FCB9271AEFB4191E45257D982405D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5879[1] = 
{
	MatchFoundMessage_t9D3942DC180FCB9271AEFB4191E45257D982405D_StaticFields::get_offset_of_Listener_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5880 = { sizeof (MatchNotFoundMessage_tEF611FC19D5DF772316F6A57A4033F76CBA2127B), -1, sizeof(MatchNotFoundMessage_tEF611FC19D5DF772316F6A57A4033F76CBA2127B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5880[1] = 
{
	MatchNotFoundMessage_tEF611FC19D5DF772316F6A57A4033F76CBA2127B_StaticFields::get_offset_of_Listener_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5881 = { sizeof (MatchUpdatedMessage_t71F1AFADF43071DDDFAB6C1FCDEF60F3D778141B), -1, sizeof(MatchUpdatedMessage_t71F1AFADF43071DDDFAB6C1FCDEF60F3D778141B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5881[1] = 
{
	MatchUpdatedMessage_t71F1AFADF43071DDDFAB6C1FCDEF60F3D778141B_StaticFields::get_offset_of_Listener_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5882 = { sizeof (NewHighScoreMessage_t4004DB3957E0A8FCB2095C1090D874744DC74EAA), -1, sizeof(NewHighScoreMessage_t4004DB3957E0A8FCB2095C1090D874744DC74EAA_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5882[1] = 
{
	NewHighScoreMessage_t4004DB3957E0A8FCB2095C1090D874744DC74EAA_StaticFields::get_offset_of_Listener_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5883 = { sizeof (NewTeamScoreMessage_t14485C7162863C58D1DF0297CDE450EA3FC01F80), -1, sizeof(NewTeamScoreMessage_t14485C7162863C58D1DF0297CDE450EA3FC01F80_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5883[1] = 
{
	NewTeamScoreMessage_t14485C7162863C58D1DF0297CDE450EA3FC01F80_StaticFields::get_offset_of_Listener_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5884 = { sizeof (ScriptMessage_t8ACBE46585B60212B7DF53DF8D2B688A7805D183), -1, sizeof(ScriptMessage_t8ACBE46585B60212B7DF53DF8D2B688A7805D183_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5884[1] = 
{
	ScriptMessage_t8ACBE46585B60212B7DF53DF8D2B688A7805D183_StaticFields::get_offset_of_Listener_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5885 = { sizeof (SessionTerminatedMessage_tE8C584B4A421AFD2A9C149EF8E1B5886DFC3A6AE), -1, sizeof(SessionTerminatedMessage_tE8C584B4A421AFD2A9C149EF8E1B5886DFC3A6AE_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5885[1] = 
{
	SessionTerminatedMessage_tE8C584B4A421AFD2A9C149EF8E1B5886DFC3A6AE_StaticFields::get_offset_of_Listener_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5886 = { sizeof (SocialRankChangedMessage_tEA41D07C3E504261D8474AA07A8D2647BD272F6F), -1, sizeof(SocialRankChangedMessage_tEA41D07C3E504261D8474AA07A8D2647BD272F6F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5886[1] = 
{
	SocialRankChangedMessage_tEA41D07C3E504261D8474AA07A8D2647BD272F6F_StaticFields::get_offset_of_Listener_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5887 = { sizeof (TeamChatMessage_t59612BFD71E8471EDFADFB67FD67E8CF2992DA16), -1, sizeof(TeamChatMessage_t59612BFD71E8471EDFADFB67FD67E8CF2992DA16_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5887[1] = 
{
	TeamChatMessage_t59612BFD71E8471EDFADFB67FD67E8CF2992DA16_StaticFields::get_offset_of_Listener_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5888 = { sizeof (TeamRankChangedMessage_tEB2EE77C6F7F85132CD3FF4AFB22F43534D5D571), -1, sizeof(TeamRankChangedMessage_tEB2EE77C6F7F85132CD3FF4AFB22F43534D5D571_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5888[1] = 
{
	TeamRankChangedMessage_tEB2EE77C6F7F85132CD3FF4AFB22F43534D5D571_StaticFields::get_offset_of_Listener_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5889 = { sizeof (UploadCompleteMessage_t2F357234A77684DB822A6969FBB34B5E0D3FB7FF), -1, sizeof(UploadCompleteMessage_t2F357234A77684DB822A6969FBB34B5E0D3FB7FF_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5889[1] = 
{
	UploadCompleteMessage_t2F357234A77684DB822A6969FBB34B5E0D3FB7FF_StaticFields::get_offset_of_Listener_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5890 = { sizeof (AccountDetailsRequest_tBA824708CEB3983B458DD780CA9345B77461D6BA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5891 = { sizeof (AuthenticationRequest_t48096CAA649171B606B7E660EB131CF67C0FDFC5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5892 = { sizeof (ChangeUserDetailsRequest_tBA8A275D4D08B06D3CBB5E88BF65EAA2D7E8057B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5893 = { sizeof (CreateTeamRequest_t753F9189B1005E3D3C06FB1E3F39AD95B2A2E213), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5894 = { sizeof (DeviceAuthenticationRequest_tE82A29B79E3FAEE538296B9C6A4D0BFDF1590631), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5895 = { sizeof (DismissMessageRequest_t3BA959D76856A74D62362A269C0857774D37D748), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5896 = { sizeof (DropTeamRequest_t271A4FCDF247EF7AD01D42403A8BF41281533F0B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5897 = { sizeof (EndSessionRequest_t806E19284145380C0157A3089F19D5829C829FC9), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5898 = { sizeof (FacebookConnectRequest_t0CA6D1284233B489069B6D180E1E141107D7BEE9), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5899 = { sizeof (GetDownloadableRequest_t6B67859C4DC532380A0A4C8A3C5C7AA30600F751), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
