﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Action`1<Zenject.DiContainer>
struct Action_1_t47FB75EB7E275E7EC20C1ADC32AC936BD0BD4E84;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<Zenject.PrefabResourceSingletonProviderCreator/PrefabId,Zenject.IPrefabInstantiator>
struct Dictionary_2_t48AB9775573DB40DF1D2BEFC217E09E2408C74BE;
// System.Collections.Generic.Dictionary`2<Zenject.PrefabSingletonProviderCreator/PrefabId,Zenject.IPrefabInstantiator>
struct Dictionary_2_t07904604D2D712478E3857B28799D6807B09E5B5;
// System.Collections.Generic.Dictionary`2<Zenject.SingletonId,Zenject.SingletonTypes>
struct Dictionary_2_tD5B7725A3F47CDF6E967A1CE3130E08DA9FF7334;
// System.Collections.Generic.Dictionary`2<Zenject.SingletonId,Zenject.StandardSingletonProviderCreator/ProviderInfo>
struct Dictionary_2_t778BDA760E0ACC7135173A9EE434CF4AB4028D26;
// System.Collections.Generic.Dictionary`2<Zenject.SubContainerSingletonProviderCreatorByInstaller/InstallerSingletonId,Zenject.ISubContainerCreator>
struct Dictionary_2_t860FD753E81C06B1E2604A10CB9891602288AD03;
// System.Collections.Generic.Dictionary`2<Zenject.SubContainerSingletonProviderCreatorByMethod/MethodSingletonId,Zenject.ISubContainerCreator>
struct Dictionary_2_t386DE62E5812D8AA2D41D0DEF4FC9E0B522953F0;
// System.Collections.Generic.Dictionary`2<Zenject.SubContainerSingletonProviderCreatorByNewPrefab/CustomSingletonId,Zenject.SubContainerSingletonProviderCreatorByNewPrefab/CreatorInfo>
struct Dictionary_2_t2F7E5CA7D4924BAD997E7E2A40BAA2361AD2E515;
// System.Collections.Generic.Dictionary`2<Zenject.SubContainerSingletonProviderCreatorByNewPrefabResource/CustomSingletonId,Zenject.SubContainerSingletonProviderCreatorByNewPrefabResource/CreatorInfo>
struct Dictionary_2_t36D1EF1D06922EA2951BDF1B3A6E0B8818A2063D;
// System.Collections.Generic.IEnumerator`1<UnityEngine.GameObject>
struct IEnumerator_1_t17C77FE93B057D368EC24057B5FD86F1543E35F4;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D;
// System.Collections.Generic.List`1<Zenject.IAnimatorIkHandler>
struct List_1_t8987DED585BFB6EA83AEBA1F954508BF6E883E51;
// System.Collections.Generic.List`1<Zenject.IAnimatorMoveHandler>
struct List_1_t3155E39EF921C0D429E9287F26693366D648F153;
// System.Collections.Generic.List`1<Zenject.TypeValuePair>
struct List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69;
// System.Delegate
struct Delegate_t;
// System.Func`2<System.Reflection.MethodInfo,System.Boolean>
struct Func_2_t25BDD008BABA5EB0719A205177AD1FB36DC6A438;
// System.Func`2<System.Type,System.Boolean>
struct Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9;
// System.Func`2<UnityEngine.Object,UnityEngine.Object>
struct Func_2_t7F9671FD00F31AFFC0C1BEB0761BA9E68AB18E89;
// System.Func`2<Zenject.InjectContext,System.Object>
struct Func_2_tE24EB23C7D8F4AD0A3A34C5DB2C9F329886F48F2;
// System.Func`2<Zenject.TypeValuePair,System.Object>
struct Func_2_t1DCE2C3D6A169B92813A57259D83F0C5FEC5B5F1;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.Animator
struct Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0;
// Zenject.CachedProvider
struct CachedProvider_t0DD0B25EA381638476E1E4EC2FC32C8CDA59BA36;
// Zenject.DiContainer
struct DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5;
// Zenject.GameObjectCreationParameters
struct GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A;
// Zenject.IPrefabInstantiator
struct IPrefabInstantiator_t7DDD0F9644A546CC27417C93BB82202E1A425F8C;
// Zenject.IPrefabProvider
struct IPrefabProvider_tA943DD6A7D0339FFE4D8581406E4A675F2E01975;
// Zenject.ISubContainerCreator
struct ISubContainerCreator_t51D3BD7DE31FCD1DC85EEDEF44461B7C6FCBB45A;
// Zenject.InjectArgs
struct InjectArgs_tD1EC7FDE37E60A7D38EF224E5FE6C8667639387C;
// Zenject.InjectContext
struct InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF;
// Zenject.InstanceProvider
struct InstanceProvider_t7063C82B8A97AAB00B4296990A3FFD011493A236;
// Zenject.MethodProviderUntyped
struct MethodProviderUntyped_tF78632D60E22154A0EE863440E1F1705DA1BD4F4;
// Zenject.PrefabInstantiator
struct PrefabInstantiator_t9A8FF9385741C61D38487118C3E94341F2C7CB21;
// Zenject.PrefabInstantiatorCached
struct PrefabInstantiatorCached_tB23BE6E42DED15DF8DD349E6E8C80C45CBDA95D5;
// Zenject.PrefabResourceSingletonProviderCreator
struct PrefabResourceSingletonProviderCreator_t9A996ED86F32A28313FCA6B25175E470AFCD8A41;
// Zenject.PrefabSingletonProviderCreator
struct PrefabSingletonProviderCreator_t2C3CC78B15E2FB38FE6DD8F2D281F6E4F0533075;
// Zenject.ResolveProvider
struct ResolveProvider_t98295C4A37915579D7B978AC7D271448C3822CB8;
// Zenject.ResourceProvider
struct ResourceProvider_t19DA8EB0F876F7D6CB06485EE48DE101A9C8934E;
// Zenject.ScriptableObjectResourceProvider
struct ScriptableObjectResourceProvider_t4A007E9097B9C258E9A4A7FE39338577B3961F44;
// Zenject.SingletonId
struct SingletonId_tF74E433D582ABB3E7532764D59325896A7A8D64C;
// Zenject.SingletonMarkRegistry
struct SingletonMarkRegistry_tB0ADDC9314F22EFEF3C51F08F71E6E1CB236EC7F;
// Zenject.StandardSingletonProviderCreator
struct StandardSingletonProviderCreator_t591FCA8A24159723185A79698D93BD3CE3647E80;
// Zenject.SubContainerCreatorByNewPrefabInstaller
struct SubContainerCreatorByNewPrefabInstaller_t8C6712ED0C9D7CECCB02AB69C9FC69AB41F933B9;
// Zenject.SubContainerDependencyProvider
struct SubContainerDependencyProvider_t6C320EB6D6F4FBBA052FEAC5771F9985BF1E314D;
// Zenject.SubContainerSingletonProviderCreatorByInstaller
struct SubContainerSingletonProviderCreatorByInstaller_t0612A1F030297B7EEDB675CC61E90C60AAED986E;
// Zenject.SubContainerSingletonProviderCreatorByMethod
struct SubContainerSingletonProviderCreatorByMethod_t875112116B40E0B708D4EE4601E758BACF9F1DED;
// Zenject.SubContainerSingletonProviderCreatorByNewPrefab
struct SubContainerSingletonProviderCreatorByNewPrefab_tD1BD0BC2BC3FB62EE8DDD51AE521B287C673A19B;
// Zenject.SubContainerSingletonProviderCreatorByNewPrefabResource
struct SubContainerSingletonProviderCreatorByNewPrefabResource_t0F56AB2C3837D0E2117B52265FCBD5E21A8295AB;
// Zenject.TransientProvider
struct TransientProvider_tAB891005B80A7655BC6103608F2471B9CF7EDA43;
// Zenject.TypeValuePair
struct TypeValuePair_tD309CDC658476FDDF08345C92D3F4D7A0A1F9792;
// Zenject.UntypedFactoryProvider
struct UntypedFactoryProvider_tF14A4C3D101D65F49AA291EB4F166F6636C9363C;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef IPROVIDEREXTENSIONS_T582E729B22D6D9DF6D784F8EF8123A3F006820F5_H
#define IPROVIDEREXTENSIONS_T582E729B22D6D9DF6D784F8EF8123A3F006820F5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.IProviderExtensions
struct  IProviderExtensions_t582E729B22D6D9DF6D784F8EF8123A3F006820F5  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IPROVIDEREXTENSIONS_T582E729B22D6D9DF6D784F8EF8123A3F006820F5_H
#ifndef INSTALLERBASE_T4F464ACE8FEB8CEEE7B4352A271F09894C28AD79_H
#define INSTALLERBASE_T4F464ACE8FEB8CEEE7B4352A271F09894C28AD79_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.InstallerBase
struct  InstallerBase_t4F464ACE8FEB8CEEE7B4352A271F09894C28AD79  : public RuntimeObject
{
public:
	// Zenject.DiContainer Zenject.InstallerBase::_container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____container_0;

public:
	inline static int32_t get_offset_of__container_0() { return static_cast<int32_t>(offsetof(InstallerBase_t4F464ACE8FEB8CEEE7B4352A271F09894C28AD79, ____container_0)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__container_0() const { return ____container_0; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__container_0() { return &____container_0; }
	inline void set__container_0(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____container_0 = value;
		Il2CppCodeGenWriteBarrier((&____container_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTALLERBASE_T4F464ACE8FEB8CEEE7B4352A271F09894C28AD79_H
#ifndef U3CGETALLINSTANCESWITHINJECTSPLITU3ED__5_T0243B4CEE2AE774AF4E8797A4E0BDB012E9C821B_H
#define U3CGETALLINSTANCESWITHINJECTSPLITU3ED__5_T0243B4CEE2AE774AF4E8797A4E0BDB012E9C821B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.InstanceProvider_<GetAllInstancesWithInjectSplit>d__5
struct  U3CGetAllInstancesWithInjectSplitU3Ed__5_t0243B4CEE2AE774AF4E8797A4E0BDB012E9C821B  : public RuntimeObject
{
public:
	// System.Int32 Zenject.InstanceProvider_<GetAllInstancesWithInjectSplit>d__5::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Collections.Generic.List`1<System.Object> Zenject.InstanceProvider_<GetAllInstancesWithInjectSplit>d__5::<>2__current
	List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * ___U3CU3E2__current_1;
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.InstanceProvider_<GetAllInstancesWithInjectSplit>d__5::args
	List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * ___args_2;
	// Zenject.InjectContext Zenject.InstanceProvider_<GetAllInstancesWithInjectSplit>d__5::context
	InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * ___context_3;
	// Zenject.InstanceProvider Zenject.InstanceProvider_<GetAllInstancesWithInjectSplit>d__5::<>4__this
	InstanceProvider_t7063C82B8A97AAB00B4296990A3FFD011493A236 * ___U3CU3E4__this_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__5_t0243B4CEE2AE774AF4E8797A4E0BDB012E9C821B, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__5_t0243B4CEE2AE774AF4E8797A4E0BDB012E9C821B, ___U3CU3E2__current_1)); }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_args_2() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__5_t0243B4CEE2AE774AF4E8797A4E0BDB012E9C821B, ___args_2)); }
	inline List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * get_args_2() const { return ___args_2; }
	inline List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 ** get_address_of_args_2() { return &___args_2; }
	inline void set_args_2(List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * value)
	{
		___args_2 = value;
		Il2CppCodeGenWriteBarrier((&___args_2), value);
	}

	inline static int32_t get_offset_of_context_3() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__5_t0243B4CEE2AE774AF4E8797A4E0BDB012E9C821B, ___context_3)); }
	inline InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * get_context_3() const { return ___context_3; }
	inline InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF ** get_address_of_context_3() { return &___context_3; }
	inline void set_context_3(InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * value)
	{
		___context_3 = value;
		Il2CppCodeGenWriteBarrier((&___context_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_4() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__5_t0243B4CEE2AE774AF4E8797A4E0BDB012E9C821B, ___U3CU3E4__this_4)); }
	inline InstanceProvider_t7063C82B8A97AAB00B4296990A3FFD011493A236 * get_U3CU3E4__this_4() const { return ___U3CU3E4__this_4; }
	inline InstanceProvider_t7063C82B8A97AAB00B4296990A3FFD011493A236 ** get_address_of_U3CU3E4__this_4() { return &___U3CU3E4__this_4; }
	inline void set_U3CU3E4__this_4(InstanceProvider_t7063C82B8A97AAB00B4296990A3FFD011493A236 * value)
	{
		___U3CU3E4__this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETALLINSTANCESWITHINJECTSPLITU3ED__5_T0243B4CEE2AE774AF4E8797A4E0BDB012E9C821B_H
#ifndef METHODPROVIDERUNTYPED_TF78632D60E22154A0EE863440E1F1705DA1BD4F4_H
#define METHODPROVIDERUNTYPED_TF78632D60E22154A0EE863440E1F1705DA1BD4F4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.MethodProviderUntyped
struct  MethodProviderUntyped_tF78632D60E22154A0EE863440E1F1705DA1BD4F4  : public RuntimeObject
{
public:
	// Zenject.DiContainer Zenject.MethodProviderUntyped::_container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____container_0;
	// System.Func`2<Zenject.InjectContext,System.Object> Zenject.MethodProviderUntyped::_method
	Func_2_tE24EB23C7D8F4AD0A3A34C5DB2C9F329886F48F2 * ____method_1;

public:
	inline static int32_t get_offset_of__container_0() { return static_cast<int32_t>(offsetof(MethodProviderUntyped_tF78632D60E22154A0EE863440E1F1705DA1BD4F4, ____container_0)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__container_0() const { return ____container_0; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__container_0() { return &____container_0; }
	inline void set__container_0(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____container_0 = value;
		Il2CppCodeGenWriteBarrier((&____container_0), value);
	}

	inline static int32_t get_offset_of__method_1() { return static_cast<int32_t>(offsetof(MethodProviderUntyped_tF78632D60E22154A0EE863440E1F1705DA1BD4F4, ____method_1)); }
	inline Func_2_tE24EB23C7D8F4AD0A3A34C5DB2C9F329886F48F2 * get__method_1() const { return ____method_1; }
	inline Func_2_tE24EB23C7D8F4AD0A3A34C5DB2C9F329886F48F2 ** get_address_of__method_1() { return &____method_1; }
	inline void set__method_1(Func_2_tE24EB23C7D8F4AD0A3A34C5DB2C9F329886F48F2 * value)
	{
		____method_1 = value;
		Il2CppCodeGenWriteBarrier((&____method_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METHODPROVIDERUNTYPED_TF78632D60E22154A0EE863440E1F1705DA1BD4F4_H
#ifndef U3CGETALLINSTANCESWITHINJECTSPLITU3ED__4_T0A88BEED476B7F82C985AE259DEC8DCE846FCCC3_H
#define U3CGETALLINSTANCESWITHINJECTSPLITU3ED__4_T0A88BEED476B7F82C985AE259DEC8DCE846FCCC3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.MethodProviderUntyped_<GetAllInstancesWithInjectSplit>d__4
struct  U3CGetAllInstancesWithInjectSplitU3Ed__4_t0A88BEED476B7F82C985AE259DEC8DCE846FCCC3  : public RuntimeObject
{
public:
	// System.Int32 Zenject.MethodProviderUntyped_<GetAllInstancesWithInjectSplit>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Collections.Generic.List`1<System.Object> Zenject.MethodProviderUntyped_<GetAllInstancesWithInjectSplit>d__4::<>2__current
	List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * ___U3CU3E2__current_1;
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.MethodProviderUntyped_<GetAllInstancesWithInjectSplit>d__4::args
	List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * ___args_2;
	// Zenject.InjectContext Zenject.MethodProviderUntyped_<GetAllInstancesWithInjectSplit>d__4::context
	InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * ___context_3;
	// Zenject.MethodProviderUntyped Zenject.MethodProviderUntyped_<GetAllInstancesWithInjectSplit>d__4::<>4__this
	MethodProviderUntyped_tF78632D60E22154A0EE863440E1F1705DA1BD4F4 * ___U3CU3E4__this_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__4_t0A88BEED476B7F82C985AE259DEC8DCE846FCCC3, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__4_t0A88BEED476B7F82C985AE259DEC8DCE846FCCC3, ___U3CU3E2__current_1)); }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_args_2() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__4_t0A88BEED476B7F82C985AE259DEC8DCE846FCCC3, ___args_2)); }
	inline List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * get_args_2() const { return ___args_2; }
	inline List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 ** get_address_of_args_2() { return &___args_2; }
	inline void set_args_2(List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * value)
	{
		___args_2 = value;
		Il2CppCodeGenWriteBarrier((&___args_2), value);
	}

	inline static int32_t get_offset_of_context_3() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__4_t0A88BEED476B7F82C985AE259DEC8DCE846FCCC3, ___context_3)); }
	inline InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * get_context_3() const { return ___context_3; }
	inline InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF ** get_address_of_context_3() { return &___context_3; }
	inline void set_context_3(InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * value)
	{
		___context_3 = value;
		Il2CppCodeGenWriteBarrier((&___context_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_4() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__4_t0A88BEED476B7F82C985AE259DEC8DCE846FCCC3, ___U3CU3E4__this_4)); }
	inline MethodProviderUntyped_tF78632D60E22154A0EE863440E1F1705DA1BD4F4 * get_U3CU3E4__this_4() const { return ___U3CU3E4__this_4; }
	inline MethodProviderUntyped_tF78632D60E22154A0EE863440E1F1705DA1BD4F4 ** get_address_of_U3CU3E4__this_4() { return &___U3CU3E4__this_4; }
	inline void set_U3CU3E4__this_4(MethodProviderUntyped_tF78632D60E22154A0EE863440E1F1705DA1BD4F4 * value)
	{
		___U3CU3E4__this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETALLINSTANCESWITHINJECTSPLITU3ED__4_T0A88BEED476B7F82C985AE259DEC8DCE846FCCC3_H
#ifndef PREFABINSTANTIATOR_T9A8FF9385741C61D38487118C3E94341F2C7CB21_H
#define PREFABINSTANTIATOR_T9A8FF9385741C61D38487118C3E94341F2C7CB21_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.PrefabInstantiator
struct  PrefabInstantiator_t9A8FF9385741C61D38487118C3E94341F2C7CB21  : public RuntimeObject
{
public:
	// Zenject.IPrefabProvider Zenject.PrefabInstantiator::_prefabProvider
	RuntimeObject* ____prefabProvider_0;
	// Zenject.DiContainer Zenject.PrefabInstantiator::_container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____container_1;
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.PrefabInstantiator::_extraArguments
	List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * ____extraArguments_2;
	// Zenject.GameObjectCreationParameters Zenject.PrefabInstantiator::_gameObjectBindInfo
	GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A * ____gameObjectBindInfo_3;
	// System.Type Zenject.PrefabInstantiator::_argumentTarget
	Type_t * ____argumentTarget_4;

public:
	inline static int32_t get_offset_of__prefabProvider_0() { return static_cast<int32_t>(offsetof(PrefabInstantiator_t9A8FF9385741C61D38487118C3E94341F2C7CB21, ____prefabProvider_0)); }
	inline RuntimeObject* get__prefabProvider_0() const { return ____prefabProvider_0; }
	inline RuntimeObject** get_address_of__prefabProvider_0() { return &____prefabProvider_0; }
	inline void set__prefabProvider_0(RuntimeObject* value)
	{
		____prefabProvider_0 = value;
		Il2CppCodeGenWriteBarrier((&____prefabProvider_0), value);
	}

	inline static int32_t get_offset_of__container_1() { return static_cast<int32_t>(offsetof(PrefabInstantiator_t9A8FF9385741C61D38487118C3E94341F2C7CB21, ____container_1)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__container_1() const { return ____container_1; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__container_1() { return &____container_1; }
	inline void set__container_1(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____container_1 = value;
		Il2CppCodeGenWriteBarrier((&____container_1), value);
	}

	inline static int32_t get_offset_of__extraArguments_2() { return static_cast<int32_t>(offsetof(PrefabInstantiator_t9A8FF9385741C61D38487118C3E94341F2C7CB21, ____extraArguments_2)); }
	inline List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * get__extraArguments_2() const { return ____extraArguments_2; }
	inline List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 ** get_address_of__extraArguments_2() { return &____extraArguments_2; }
	inline void set__extraArguments_2(List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * value)
	{
		____extraArguments_2 = value;
		Il2CppCodeGenWriteBarrier((&____extraArguments_2), value);
	}

	inline static int32_t get_offset_of__gameObjectBindInfo_3() { return static_cast<int32_t>(offsetof(PrefabInstantiator_t9A8FF9385741C61D38487118C3E94341F2C7CB21, ____gameObjectBindInfo_3)); }
	inline GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A * get__gameObjectBindInfo_3() const { return ____gameObjectBindInfo_3; }
	inline GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A ** get_address_of__gameObjectBindInfo_3() { return &____gameObjectBindInfo_3; }
	inline void set__gameObjectBindInfo_3(GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A * value)
	{
		____gameObjectBindInfo_3 = value;
		Il2CppCodeGenWriteBarrier((&____gameObjectBindInfo_3), value);
	}

	inline static int32_t get_offset_of__argumentTarget_4() { return static_cast<int32_t>(offsetof(PrefabInstantiator_t9A8FF9385741C61D38487118C3E94341F2C7CB21, ____argumentTarget_4)); }
	inline Type_t * get__argumentTarget_4() const { return ____argumentTarget_4; }
	inline Type_t ** get_address_of__argumentTarget_4() { return &____argumentTarget_4; }
	inline void set__argumentTarget_4(Type_t * value)
	{
		____argumentTarget_4 = value;
		Il2CppCodeGenWriteBarrier((&____argumentTarget_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREFABINSTANTIATOR_T9A8FF9385741C61D38487118C3E94341F2C7CB21_H
#ifndef U3CINSTANTIATEU3ED__13_TC7BC92BC8BFD11A194B9CA38C6486689D1B81397_H
#define U3CINSTANTIATEU3ED__13_TC7BC92BC8BFD11A194B9CA38C6486689D1B81397_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.PrefabInstantiator_<Instantiate>d__13
struct  U3CInstantiateU3Ed__13_tC7BC92BC8BFD11A194B9CA38C6486689D1B81397  : public RuntimeObject
{
public:
	// System.Int32 Zenject.PrefabInstantiator_<Instantiate>d__13::<>1__state
	int32_t ___U3CU3E1__state_0;
	// UnityEngine.GameObject Zenject.PrefabInstantiator_<Instantiate>d__13::<>2__current
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___U3CU3E2__current_1;
	// Zenject.PrefabInstantiator Zenject.PrefabInstantiator_<Instantiate>d__13::<>4__this
	PrefabInstantiator_t9A8FF9385741C61D38487118C3E94341F2C7CB21 * ___U3CU3E4__this_2;
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.PrefabInstantiator_<Instantiate>d__13::args
	List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * ___args_3;
	// Zenject.InjectContext Zenject.PrefabInstantiator_<Instantiate>d__13::<context>5__2
	InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * ___U3CcontextU3E5__2_4;
	// System.Boolean Zenject.PrefabInstantiator_<Instantiate>d__13::<shouldMakeActive>5__3
	bool ___U3CshouldMakeActiveU3E5__3_5;
	// UnityEngine.GameObject Zenject.PrefabInstantiator_<Instantiate>d__13::<gameObject>5__4
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___U3CgameObjectU3E5__4_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CInstantiateU3Ed__13_tC7BC92BC8BFD11A194B9CA38C6486689D1B81397, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CInstantiateU3Ed__13_tC7BC92BC8BFD11A194B9CA38C6486689D1B81397, ___U3CU3E2__current_1)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CInstantiateU3Ed__13_tC7BC92BC8BFD11A194B9CA38C6486689D1B81397, ___U3CU3E4__this_2)); }
	inline PrefabInstantiator_t9A8FF9385741C61D38487118C3E94341F2C7CB21 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline PrefabInstantiator_t9A8FF9385741C61D38487118C3E94341F2C7CB21 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(PrefabInstantiator_t9A8FF9385741C61D38487118C3E94341F2C7CB21 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_args_3() { return static_cast<int32_t>(offsetof(U3CInstantiateU3Ed__13_tC7BC92BC8BFD11A194B9CA38C6486689D1B81397, ___args_3)); }
	inline List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * get_args_3() const { return ___args_3; }
	inline List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 ** get_address_of_args_3() { return &___args_3; }
	inline void set_args_3(List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * value)
	{
		___args_3 = value;
		Il2CppCodeGenWriteBarrier((&___args_3), value);
	}

	inline static int32_t get_offset_of_U3CcontextU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CInstantiateU3Ed__13_tC7BC92BC8BFD11A194B9CA38C6486689D1B81397, ___U3CcontextU3E5__2_4)); }
	inline InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * get_U3CcontextU3E5__2_4() const { return ___U3CcontextU3E5__2_4; }
	inline InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF ** get_address_of_U3CcontextU3E5__2_4() { return &___U3CcontextU3E5__2_4; }
	inline void set_U3CcontextU3E5__2_4(InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * value)
	{
		___U3CcontextU3E5__2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcontextU3E5__2_4), value);
	}

	inline static int32_t get_offset_of_U3CshouldMakeActiveU3E5__3_5() { return static_cast<int32_t>(offsetof(U3CInstantiateU3Ed__13_tC7BC92BC8BFD11A194B9CA38C6486689D1B81397, ___U3CshouldMakeActiveU3E5__3_5)); }
	inline bool get_U3CshouldMakeActiveU3E5__3_5() const { return ___U3CshouldMakeActiveU3E5__3_5; }
	inline bool* get_address_of_U3CshouldMakeActiveU3E5__3_5() { return &___U3CshouldMakeActiveU3E5__3_5; }
	inline void set_U3CshouldMakeActiveU3E5__3_5(bool value)
	{
		___U3CshouldMakeActiveU3E5__3_5 = value;
	}

	inline static int32_t get_offset_of_U3CgameObjectU3E5__4_6() { return static_cast<int32_t>(offsetof(U3CInstantiateU3Ed__13_tC7BC92BC8BFD11A194B9CA38C6486689D1B81397, ___U3CgameObjectU3E5__4_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_U3CgameObjectU3E5__4_6() const { return ___U3CgameObjectU3E5__4_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_U3CgameObjectU3E5__4_6() { return &___U3CgameObjectU3E5__4_6; }
	inline void set_U3CgameObjectU3E5__4_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___U3CgameObjectU3E5__4_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CgameObjectU3E5__4_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINSTANTIATEU3ED__13_TC7BC92BC8BFD11A194B9CA38C6486689D1B81397_H
#ifndef PREFABINSTANTIATORCACHED_TB23BE6E42DED15DF8DD349E6E8C80C45CBDA95D5_H
#define PREFABINSTANTIATORCACHED_TB23BE6E42DED15DF8DD349E6E8C80C45CBDA95D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.PrefabInstantiatorCached
struct  PrefabInstantiatorCached_tB23BE6E42DED15DF8DD349E6E8C80C45CBDA95D5  : public RuntimeObject
{
public:
	// Zenject.IPrefabInstantiator Zenject.PrefabInstantiatorCached::_subInstantiator
	RuntimeObject* ____subInstantiator_0;
	// UnityEngine.GameObject Zenject.PrefabInstantiatorCached::_gameObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____gameObject_1;

public:
	inline static int32_t get_offset_of__subInstantiator_0() { return static_cast<int32_t>(offsetof(PrefabInstantiatorCached_tB23BE6E42DED15DF8DD349E6E8C80C45CBDA95D5, ____subInstantiator_0)); }
	inline RuntimeObject* get__subInstantiator_0() const { return ____subInstantiator_0; }
	inline RuntimeObject** get_address_of__subInstantiator_0() { return &____subInstantiator_0; }
	inline void set__subInstantiator_0(RuntimeObject* value)
	{
		____subInstantiator_0 = value;
		Il2CppCodeGenWriteBarrier((&____subInstantiator_0), value);
	}

	inline static int32_t get_offset_of__gameObject_1() { return static_cast<int32_t>(offsetof(PrefabInstantiatorCached_tB23BE6E42DED15DF8DD349E6E8C80C45CBDA95D5, ____gameObject_1)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__gameObject_1() const { return ____gameObject_1; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__gameObject_1() { return &____gameObject_1; }
	inline void set__gameObject_1(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____gameObject_1 = value;
		Il2CppCodeGenWriteBarrier((&____gameObject_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREFABINSTANTIATORCACHED_TB23BE6E42DED15DF8DD349E6E8C80C45CBDA95D5_H
#ifndef U3CINSTANTIATEU3ED__10_T6E229FE4C6F2F530C7431340B69D35641DCE8B67_H
#define U3CINSTANTIATEU3ED__10_T6E229FE4C6F2F530C7431340B69D35641DCE8B67_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.PrefabInstantiatorCached_<Instantiate>d__10
struct  U3CInstantiateU3Ed__10_t6E229FE4C6F2F530C7431340B69D35641DCE8B67  : public RuntimeObject
{
public:
	// System.Int32 Zenject.PrefabInstantiatorCached_<Instantiate>d__10::<>1__state
	int32_t ___U3CU3E1__state_0;
	// UnityEngine.GameObject Zenject.PrefabInstantiatorCached_<Instantiate>d__10::<>2__current
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___U3CU3E2__current_1;
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.PrefabInstantiatorCached_<Instantiate>d__10::args
	List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * ___args_2;
	// Zenject.PrefabInstantiatorCached Zenject.PrefabInstantiatorCached_<Instantiate>d__10::<>4__this
	PrefabInstantiatorCached_tB23BE6E42DED15DF8DD349E6E8C80C45CBDA95D5 * ___U3CU3E4__this_3;
	// System.Collections.Generic.IEnumerator`1<UnityEngine.GameObject> Zenject.PrefabInstantiatorCached_<Instantiate>d__10::<runner>5__2
	RuntimeObject* ___U3CrunnerU3E5__2_4;
	// System.Boolean Zenject.PrefabInstantiatorCached_<Instantiate>d__10::<hasMore>5__3
	bool ___U3ChasMoreU3E5__3_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CInstantiateU3Ed__10_t6E229FE4C6F2F530C7431340B69D35641DCE8B67, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CInstantiateU3Ed__10_t6E229FE4C6F2F530C7431340B69D35641DCE8B67, ___U3CU3E2__current_1)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_args_2() { return static_cast<int32_t>(offsetof(U3CInstantiateU3Ed__10_t6E229FE4C6F2F530C7431340B69D35641DCE8B67, ___args_2)); }
	inline List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * get_args_2() const { return ___args_2; }
	inline List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 ** get_address_of_args_2() { return &___args_2; }
	inline void set_args_2(List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * value)
	{
		___args_2 = value;
		Il2CppCodeGenWriteBarrier((&___args_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CInstantiateU3Ed__10_t6E229FE4C6F2F530C7431340B69D35641DCE8B67, ___U3CU3E4__this_3)); }
	inline PrefabInstantiatorCached_tB23BE6E42DED15DF8DD349E6E8C80C45CBDA95D5 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline PrefabInstantiatorCached_tB23BE6E42DED15DF8DD349E6E8C80C45CBDA95D5 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(PrefabInstantiatorCached_tB23BE6E42DED15DF8DD349E6E8C80C45CBDA95D5 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_U3CrunnerU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CInstantiateU3Ed__10_t6E229FE4C6F2F530C7431340B69D35641DCE8B67, ___U3CrunnerU3E5__2_4)); }
	inline RuntimeObject* get_U3CrunnerU3E5__2_4() const { return ___U3CrunnerU3E5__2_4; }
	inline RuntimeObject** get_address_of_U3CrunnerU3E5__2_4() { return &___U3CrunnerU3E5__2_4; }
	inline void set_U3CrunnerU3E5__2_4(RuntimeObject* value)
	{
		___U3CrunnerU3E5__2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrunnerU3E5__2_4), value);
	}

	inline static int32_t get_offset_of_U3ChasMoreU3E5__3_5() { return static_cast<int32_t>(offsetof(U3CInstantiateU3Ed__10_t6E229FE4C6F2F530C7431340B69D35641DCE8B67, ___U3ChasMoreU3E5__3_5)); }
	inline bool get_U3ChasMoreU3E5__3_5() const { return ___U3ChasMoreU3E5__3_5; }
	inline bool* get_address_of_U3ChasMoreU3E5__3_5() { return &___U3ChasMoreU3E5__3_5; }
	inline void set_U3ChasMoreU3E5__3_5(bool value)
	{
		___U3ChasMoreU3E5__3_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINSTANTIATEU3ED__10_T6E229FE4C6F2F530C7431340B69D35641DCE8B67_H
#ifndef PREFABPROVIDER_TC323C17BEECAB2A24F0D17DBEAC13395B6797034_H
#define PREFABPROVIDER_TC323C17BEECAB2A24F0D17DBEAC13395B6797034_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.PrefabProvider
struct  PrefabProvider_tC323C17BEECAB2A24F0D17DBEAC13395B6797034  : public RuntimeObject
{
public:
	// UnityEngine.Object Zenject.PrefabProvider::_prefab
	Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ____prefab_0;

public:
	inline static int32_t get_offset_of__prefab_0() { return static_cast<int32_t>(offsetof(PrefabProvider_tC323C17BEECAB2A24F0D17DBEAC13395B6797034, ____prefab_0)); }
	inline Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * get__prefab_0() const { return ____prefab_0; }
	inline Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 ** get_address_of__prefab_0() { return &____prefab_0; }
	inline void set__prefab_0(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * value)
	{
		____prefab_0 = value;
		Il2CppCodeGenWriteBarrier((&____prefab_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREFABPROVIDER_TC323C17BEECAB2A24F0D17DBEAC13395B6797034_H
#ifndef PREFABPROVIDERRESOURCE_TA4BDAA46C8051D3CAEFF7C4DC8E37189A1D76D87_H
#define PREFABPROVIDERRESOURCE_TA4BDAA46C8051D3CAEFF7C4DC8E37189A1D76D87_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.PrefabProviderResource
struct  PrefabProviderResource_tA4BDAA46C8051D3CAEFF7C4DC8E37189A1D76D87  : public RuntimeObject
{
public:
	// System.String Zenject.PrefabProviderResource::_resourcePath
	String_t* ____resourcePath_0;

public:
	inline static int32_t get_offset_of__resourcePath_0() { return static_cast<int32_t>(offsetof(PrefabProviderResource_tA4BDAA46C8051D3CAEFF7C4DC8E37189A1D76D87, ____resourcePath_0)); }
	inline String_t* get__resourcePath_0() const { return ____resourcePath_0; }
	inline String_t** get_address_of__resourcePath_0() { return &____resourcePath_0; }
	inline void set__resourcePath_0(String_t* value)
	{
		____resourcePath_0 = value;
		Il2CppCodeGenWriteBarrier((&____resourcePath_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREFABPROVIDERRESOURCE_TA4BDAA46C8051D3CAEFF7C4DC8E37189A1D76D87_H
#ifndef PREFABRESOURCESINGLETONPROVIDERCREATOR_T9A996ED86F32A28313FCA6B25175E470AFCD8A41_H
#define PREFABRESOURCESINGLETONPROVIDERCREATOR_T9A996ED86F32A28313FCA6B25175E470AFCD8A41_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.PrefabResourceSingletonProviderCreator
struct  PrefabResourceSingletonProviderCreator_t9A996ED86F32A28313FCA6B25175E470AFCD8A41  : public RuntimeObject
{
public:
	// Zenject.SingletonMarkRegistry Zenject.PrefabResourceSingletonProviderCreator::_markRegistry
	SingletonMarkRegistry_tB0ADDC9314F22EFEF3C51F08F71E6E1CB236EC7F * ____markRegistry_0;
	// Zenject.DiContainer Zenject.PrefabResourceSingletonProviderCreator::_container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____container_1;
	// System.Collections.Generic.Dictionary`2<Zenject.PrefabResourceSingletonProviderCreator_PrefabId,Zenject.IPrefabInstantiator> Zenject.PrefabResourceSingletonProviderCreator::_prefabCreators
	Dictionary_2_t48AB9775573DB40DF1D2BEFC217E09E2408C74BE * ____prefabCreators_2;

public:
	inline static int32_t get_offset_of__markRegistry_0() { return static_cast<int32_t>(offsetof(PrefabResourceSingletonProviderCreator_t9A996ED86F32A28313FCA6B25175E470AFCD8A41, ____markRegistry_0)); }
	inline SingletonMarkRegistry_tB0ADDC9314F22EFEF3C51F08F71E6E1CB236EC7F * get__markRegistry_0() const { return ____markRegistry_0; }
	inline SingletonMarkRegistry_tB0ADDC9314F22EFEF3C51F08F71E6E1CB236EC7F ** get_address_of__markRegistry_0() { return &____markRegistry_0; }
	inline void set__markRegistry_0(SingletonMarkRegistry_tB0ADDC9314F22EFEF3C51F08F71E6E1CB236EC7F * value)
	{
		____markRegistry_0 = value;
		Il2CppCodeGenWriteBarrier((&____markRegistry_0), value);
	}

	inline static int32_t get_offset_of__container_1() { return static_cast<int32_t>(offsetof(PrefabResourceSingletonProviderCreator_t9A996ED86F32A28313FCA6B25175E470AFCD8A41, ____container_1)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__container_1() const { return ____container_1; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__container_1() { return &____container_1; }
	inline void set__container_1(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____container_1 = value;
		Il2CppCodeGenWriteBarrier((&____container_1), value);
	}

	inline static int32_t get_offset_of__prefabCreators_2() { return static_cast<int32_t>(offsetof(PrefabResourceSingletonProviderCreator_t9A996ED86F32A28313FCA6B25175E470AFCD8A41, ____prefabCreators_2)); }
	inline Dictionary_2_t48AB9775573DB40DF1D2BEFC217E09E2408C74BE * get__prefabCreators_2() const { return ____prefabCreators_2; }
	inline Dictionary_2_t48AB9775573DB40DF1D2BEFC217E09E2408C74BE ** get_address_of__prefabCreators_2() { return &____prefabCreators_2; }
	inline void set__prefabCreators_2(Dictionary_2_t48AB9775573DB40DF1D2BEFC217E09E2408C74BE * value)
	{
		____prefabCreators_2 = value;
		Il2CppCodeGenWriteBarrier((&____prefabCreators_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREFABRESOURCESINGLETONPROVIDERCREATOR_T9A996ED86F32A28313FCA6B25175E470AFCD8A41_H
#ifndef PREFABID_T9C8A02397FC9DE7476535AC2C9182F87EEBEC004_H
#define PREFABID_T9C8A02397FC9DE7476535AC2C9182F87EEBEC004_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.PrefabResourceSingletonProviderCreator_PrefabId
struct  PrefabId_t9C8A02397FC9DE7476535AC2C9182F87EEBEC004  : public RuntimeObject
{
public:
	// System.Object Zenject.PrefabResourceSingletonProviderCreator_PrefabId::ConcreteIdentifier
	RuntimeObject * ___ConcreteIdentifier_0;
	// System.String Zenject.PrefabResourceSingletonProviderCreator_PrefabId::ResourcePath
	String_t* ___ResourcePath_1;

public:
	inline static int32_t get_offset_of_ConcreteIdentifier_0() { return static_cast<int32_t>(offsetof(PrefabId_t9C8A02397FC9DE7476535AC2C9182F87EEBEC004, ___ConcreteIdentifier_0)); }
	inline RuntimeObject * get_ConcreteIdentifier_0() const { return ___ConcreteIdentifier_0; }
	inline RuntimeObject ** get_address_of_ConcreteIdentifier_0() { return &___ConcreteIdentifier_0; }
	inline void set_ConcreteIdentifier_0(RuntimeObject * value)
	{
		___ConcreteIdentifier_0 = value;
		Il2CppCodeGenWriteBarrier((&___ConcreteIdentifier_0), value);
	}

	inline static int32_t get_offset_of_ResourcePath_1() { return static_cast<int32_t>(offsetof(PrefabId_t9C8A02397FC9DE7476535AC2C9182F87EEBEC004, ___ResourcePath_1)); }
	inline String_t* get_ResourcePath_1() const { return ___ResourcePath_1; }
	inline String_t** get_address_of_ResourcePath_1() { return &___ResourcePath_1; }
	inline void set_ResourcePath_1(String_t* value)
	{
		___ResourcePath_1 = value;
		Il2CppCodeGenWriteBarrier((&___ResourcePath_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREFABID_T9C8A02397FC9DE7476535AC2C9182F87EEBEC004_H
#ifndef PREFABSINGLETONPROVIDERCREATOR_T2C3CC78B15E2FB38FE6DD8F2D281F6E4F0533075_H
#define PREFABSINGLETONPROVIDERCREATOR_T2C3CC78B15E2FB38FE6DD8F2D281F6E4F0533075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.PrefabSingletonProviderCreator
struct  PrefabSingletonProviderCreator_t2C3CC78B15E2FB38FE6DD8F2D281F6E4F0533075  : public RuntimeObject
{
public:
	// Zenject.SingletonMarkRegistry Zenject.PrefabSingletonProviderCreator::_markRegistry
	SingletonMarkRegistry_tB0ADDC9314F22EFEF3C51F08F71E6E1CB236EC7F * ____markRegistry_0;
	// Zenject.DiContainer Zenject.PrefabSingletonProviderCreator::_container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____container_1;
	// System.Collections.Generic.Dictionary`2<Zenject.PrefabSingletonProviderCreator_PrefabId,Zenject.IPrefabInstantiator> Zenject.PrefabSingletonProviderCreator::_prefabCreators
	Dictionary_2_t07904604D2D712478E3857B28799D6807B09E5B5 * ____prefabCreators_2;

public:
	inline static int32_t get_offset_of__markRegistry_0() { return static_cast<int32_t>(offsetof(PrefabSingletonProviderCreator_t2C3CC78B15E2FB38FE6DD8F2D281F6E4F0533075, ____markRegistry_0)); }
	inline SingletonMarkRegistry_tB0ADDC9314F22EFEF3C51F08F71E6E1CB236EC7F * get__markRegistry_0() const { return ____markRegistry_0; }
	inline SingletonMarkRegistry_tB0ADDC9314F22EFEF3C51F08F71E6E1CB236EC7F ** get_address_of__markRegistry_0() { return &____markRegistry_0; }
	inline void set__markRegistry_0(SingletonMarkRegistry_tB0ADDC9314F22EFEF3C51F08F71E6E1CB236EC7F * value)
	{
		____markRegistry_0 = value;
		Il2CppCodeGenWriteBarrier((&____markRegistry_0), value);
	}

	inline static int32_t get_offset_of__container_1() { return static_cast<int32_t>(offsetof(PrefabSingletonProviderCreator_t2C3CC78B15E2FB38FE6DD8F2D281F6E4F0533075, ____container_1)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__container_1() const { return ____container_1; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__container_1() { return &____container_1; }
	inline void set__container_1(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____container_1 = value;
		Il2CppCodeGenWriteBarrier((&____container_1), value);
	}

	inline static int32_t get_offset_of__prefabCreators_2() { return static_cast<int32_t>(offsetof(PrefabSingletonProviderCreator_t2C3CC78B15E2FB38FE6DD8F2D281F6E4F0533075, ____prefabCreators_2)); }
	inline Dictionary_2_t07904604D2D712478E3857B28799D6807B09E5B5 * get__prefabCreators_2() const { return ____prefabCreators_2; }
	inline Dictionary_2_t07904604D2D712478E3857B28799D6807B09E5B5 ** get_address_of__prefabCreators_2() { return &____prefabCreators_2; }
	inline void set__prefabCreators_2(Dictionary_2_t07904604D2D712478E3857B28799D6807B09E5B5 * value)
	{
		____prefabCreators_2 = value;
		Il2CppCodeGenWriteBarrier((&____prefabCreators_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREFABSINGLETONPROVIDERCREATOR_T2C3CC78B15E2FB38FE6DD8F2D281F6E4F0533075_H
#ifndef PREFABID_T8F1CE05CB1F0C46FB5397BC97495BFEBE57E5059_H
#define PREFABID_T8F1CE05CB1F0C46FB5397BC97495BFEBE57E5059_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.PrefabSingletonProviderCreator_PrefabId
struct  PrefabId_t8F1CE05CB1F0C46FB5397BC97495BFEBE57E5059  : public RuntimeObject
{
public:
	// System.Object Zenject.PrefabSingletonProviderCreator_PrefabId::ConcreteIdentifier
	RuntimeObject * ___ConcreteIdentifier_0;
	// UnityEngine.Object Zenject.PrefabSingletonProviderCreator_PrefabId::Prefab
	Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___Prefab_1;

public:
	inline static int32_t get_offset_of_ConcreteIdentifier_0() { return static_cast<int32_t>(offsetof(PrefabId_t8F1CE05CB1F0C46FB5397BC97495BFEBE57E5059, ___ConcreteIdentifier_0)); }
	inline RuntimeObject * get_ConcreteIdentifier_0() const { return ___ConcreteIdentifier_0; }
	inline RuntimeObject ** get_address_of_ConcreteIdentifier_0() { return &___ConcreteIdentifier_0; }
	inline void set_ConcreteIdentifier_0(RuntimeObject * value)
	{
		___ConcreteIdentifier_0 = value;
		Il2CppCodeGenWriteBarrier((&___ConcreteIdentifier_0), value);
	}

	inline static int32_t get_offset_of_Prefab_1() { return static_cast<int32_t>(offsetof(PrefabId_t8F1CE05CB1F0C46FB5397BC97495BFEBE57E5059, ___Prefab_1)); }
	inline Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * get_Prefab_1() const { return ___Prefab_1; }
	inline Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 ** get_address_of_Prefab_1() { return &___Prefab_1; }
	inline void set_Prefab_1(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * value)
	{
		___Prefab_1 = value;
		Il2CppCodeGenWriteBarrier((&___Prefab_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREFABID_T8F1CE05CB1F0C46FB5397BC97495BFEBE57E5059_H
#ifndef PROVIDERUTIL_T187ABAB1AB3B6AF5D15DAF503BC3B3FA32D3A0C4_H
#define PROVIDERUTIL_T187ABAB1AB3B6AF5D15DAF503BC3B3FA32D3A0C4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ProviderUtil
struct  ProviderUtil_t187ABAB1AB3B6AF5D15DAF503BC3B3FA32D3A0C4  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROVIDERUTIL_T187ABAB1AB3B6AF5D15DAF503BC3B3FA32D3A0C4_H
#ifndef U3CGETALLINSTANCESWITHINJECTSPLITU3ED__7_TE755A1A34FBF05512892ED8C1391C70B0A3599D3_H
#define U3CGETALLINSTANCESWITHINJECTSPLITU3ED__7_TE755A1A34FBF05512892ED8C1391C70B0A3599D3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ResolveProvider_<GetAllInstancesWithInjectSplit>d__7
struct  U3CGetAllInstancesWithInjectSplitU3Ed__7_tE755A1A34FBF05512892ED8C1391C70B0A3599D3  : public RuntimeObject
{
public:
	// System.Int32 Zenject.ResolveProvider_<GetAllInstancesWithInjectSplit>d__7::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Collections.Generic.List`1<System.Object> Zenject.ResolveProvider_<GetAllInstancesWithInjectSplit>d__7::<>2__current
	List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * ___U3CU3E2__current_1;
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.ResolveProvider_<GetAllInstancesWithInjectSplit>d__7::args
	List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * ___args_2;
	// Zenject.InjectContext Zenject.ResolveProvider_<GetAllInstancesWithInjectSplit>d__7::context
	InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * ___context_3;
	// Zenject.ResolveProvider Zenject.ResolveProvider_<GetAllInstancesWithInjectSplit>d__7::<>4__this
	ResolveProvider_t98295C4A37915579D7B978AC7D271448C3822CB8 * ___U3CU3E4__this_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__7_tE755A1A34FBF05512892ED8C1391C70B0A3599D3, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__7_tE755A1A34FBF05512892ED8C1391C70B0A3599D3, ___U3CU3E2__current_1)); }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_args_2() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__7_tE755A1A34FBF05512892ED8C1391C70B0A3599D3, ___args_2)); }
	inline List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * get_args_2() const { return ___args_2; }
	inline List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 ** get_address_of_args_2() { return &___args_2; }
	inline void set_args_2(List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * value)
	{
		___args_2 = value;
		Il2CppCodeGenWriteBarrier((&___args_2), value);
	}

	inline static int32_t get_offset_of_context_3() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__7_tE755A1A34FBF05512892ED8C1391C70B0A3599D3, ___context_3)); }
	inline InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * get_context_3() const { return ___context_3; }
	inline InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF ** get_address_of_context_3() { return &___context_3; }
	inline void set_context_3(InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * value)
	{
		___context_3 = value;
		Il2CppCodeGenWriteBarrier((&___context_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_4() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__7_tE755A1A34FBF05512892ED8C1391C70B0A3599D3, ___U3CU3E4__this_4)); }
	inline ResolveProvider_t98295C4A37915579D7B978AC7D271448C3822CB8 * get_U3CU3E4__this_4() const { return ___U3CU3E4__this_4; }
	inline ResolveProvider_t98295C4A37915579D7B978AC7D271448C3822CB8 ** get_address_of_U3CU3E4__this_4() { return &___U3CU3E4__this_4; }
	inline void set_U3CU3E4__this_4(ResolveProvider_t98295C4A37915579D7B978AC7D271448C3822CB8 * value)
	{
		___U3CU3E4__this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETALLINSTANCESWITHINJECTSPLITU3ED__7_TE755A1A34FBF05512892ED8C1391C70B0A3599D3_H
#ifndef RESOURCEPROVIDER_T19DA8EB0F876F7D6CB06485EE48DE101A9C8934E_H
#define RESOURCEPROVIDER_T19DA8EB0F876F7D6CB06485EE48DE101A9C8934E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ResourceProvider
struct  ResourceProvider_t19DA8EB0F876F7D6CB06485EE48DE101A9C8934E  : public RuntimeObject
{
public:
	// System.Type Zenject.ResourceProvider::_resourceType
	Type_t * ____resourceType_0;
	// System.String Zenject.ResourceProvider::_resourcePath
	String_t* ____resourcePath_1;

public:
	inline static int32_t get_offset_of__resourceType_0() { return static_cast<int32_t>(offsetof(ResourceProvider_t19DA8EB0F876F7D6CB06485EE48DE101A9C8934E, ____resourceType_0)); }
	inline Type_t * get__resourceType_0() const { return ____resourceType_0; }
	inline Type_t ** get_address_of__resourceType_0() { return &____resourceType_0; }
	inline void set__resourceType_0(Type_t * value)
	{
		____resourceType_0 = value;
		Il2CppCodeGenWriteBarrier((&____resourceType_0), value);
	}

	inline static int32_t get_offset_of__resourcePath_1() { return static_cast<int32_t>(offsetof(ResourceProvider_t19DA8EB0F876F7D6CB06485EE48DE101A9C8934E, ____resourcePath_1)); }
	inline String_t* get__resourcePath_1() const { return ____resourcePath_1; }
	inline String_t** get_address_of__resourcePath_1() { return &____resourcePath_1; }
	inline void set__resourcePath_1(String_t* value)
	{
		____resourcePath_1 = value;
		Il2CppCodeGenWriteBarrier((&____resourcePath_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESOURCEPROVIDER_T19DA8EB0F876F7D6CB06485EE48DE101A9C8934E_H
#ifndef U3CGETALLINSTANCESWITHINJECTSPLITU3ED__4_T3FE8EDDF05D90AB5E52CBAF0BAEC0F0C3EC85E55_H
#define U3CGETALLINSTANCESWITHINJECTSPLITU3ED__4_T3FE8EDDF05D90AB5E52CBAF0BAEC0F0C3EC85E55_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ResourceProvider_<GetAllInstancesWithInjectSplit>d__4
struct  U3CGetAllInstancesWithInjectSplitU3Ed__4_t3FE8EDDF05D90AB5E52CBAF0BAEC0F0C3EC85E55  : public RuntimeObject
{
public:
	// System.Int32 Zenject.ResourceProvider_<GetAllInstancesWithInjectSplit>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Collections.Generic.List`1<System.Object> Zenject.ResourceProvider_<GetAllInstancesWithInjectSplit>d__4::<>2__current
	List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * ___U3CU3E2__current_1;
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.ResourceProvider_<GetAllInstancesWithInjectSplit>d__4::args
	List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * ___args_2;
	// Zenject.InjectContext Zenject.ResourceProvider_<GetAllInstancesWithInjectSplit>d__4::context
	InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * ___context_3;
	// Zenject.ResourceProvider Zenject.ResourceProvider_<GetAllInstancesWithInjectSplit>d__4::<>4__this
	ResourceProvider_t19DA8EB0F876F7D6CB06485EE48DE101A9C8934E * ___U3CU3E4__this_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__4_t3FE8EDDF05D90AB5E52CBAF0BAEC0F0C3EC85E55, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__4_t3FE8EDDF05D90AB5E52CBAF0BAEC0F0C3EC85E55, ___U3CU3E2__current_1)); }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_args_2() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__4_t3FE8EDDF05D90AB5E52CBAF0BAEC0F0C3EC85E55, ___args_2)); }
	inline List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * get_args_2() const { return ___args_2; }
	inline List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 ** get_address_of_args_2() { return &___args_2; }
	inline void set_args_2(List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * value)
	{
		___args_2 = value;
		Il2CppCodeGenWriteBarrier((&___args_2), value);
	}

	inline static int32_t get_offset_of_context_3() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__4_t3FE8EDDF05D90AB5E52CBAF0BAEC0F0C3EC85E55, ___context_3)); }
	inline InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * get_context_3() const { return ___context_3; }
	inline InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF ** get_address_of_context_3() { return &___context_3; }
	inline void set_context_3(InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * value)
	{
		___context_3 = value;
		Il2CppCodeGenWriteBarrier((&___context_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_4() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__4_t3FE8EDDF05D90AB5E52CBAF0BAEC0F0C3EC85E55, ___U3CU3E4__this_4)); }
	inline ResourceProvider_t19DA8EB0F876F7D6CB06485EE48DE101A9C8934E * get_U3CU3E4__this_4() const { return ___U3CU3E4__this_4; }
	inline ResourceProvider_t19DA8EB0F876F7D6CB06485EE48DE101A9C8934E ** get_address_of_U3CU3E4__this_4() { return &___U3CU3E4__this_4; }
	inline void set_U3CU3E4__this_4(ResourceProvider_t19DA8EB0F876F7D6CB06485EE48DE101A9C8934E * value)
	{
		___U3CU3E4__this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETALLINSTANCESWITHINJECTSPLITU3ED__4_T3FE8EDDF05D90AB5E52CBAF0BAEC0F0C3EC85E55_H
#ifndef SCRIPTABLEOBJECTRESOURCEPROVIDER_T4A007E9097B9C258E9A4A7FE39338577B3961F44_H
#define SCRIPTABLEOBJECTRESOURCEPROVIDER_T4A007E9097B9C258E9A4A7FE39338577B3961F44_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ScriptableObjectResourceProvider
struct  ScriptableObjectResourceProvider_t4A007E9097B9C258E9A4A7FE39338577B3961F44  : public RuntimeObject
{
public:
	// Zenject.DiContainer Zenject.ScriptableObjectResourceProvider::_container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____container_0;
	// System.Type Zenject.ScriptableObjectResourceProvider::_resourceType
	Type_t * ____resourceType_1;
	// System.String Zenject.ScriptableObjectResourceProvider::_resourcePath
	String_t* ____resourcePath_2;
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.ScriptableObjectResourceProvider::_extraArguments
	List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * ____extraArguments_3;
	// System.Object Zenject.ScriptableObjectResourceProvider::_concreteIdentifier
	RuntimeObject * ____concreteIdentifier_4;
	// System.Boolean Zenject.ScriptableObjectResourceProvider::_createNew
	bool ____createNew_5;

public:
	inline static int32_t get_offset_of__container_0() { return static_cast<int32_t>(offsetof(ScriptableObjectResourceProvider_t4A007E9097B9C258E9A4A7FE39338577B3961F44, ____container_0)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__container_0() const { return ____container_0; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__container_0() { return &____container_0; }
	inline void set__container_0(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____container_0 = value;
		Il2CppCodeGenWriteBarrier((&____container_0), value);
	}

	inline static int32_t get_offset_of__resourceType_1() { return static_cast<int32_t>(offsetof(ScriptableObjectResourceProvider_t4A007E9097B9C258E9A4A7FE39338577B3961F44, ____resourceType_1)); }
	inline Type_t * get__resourceType_1() const { return ____resourceType_1; }
	inline Type_t ** get_address_of__resourceType_1() { return &____resourceType_1; }
	inline void set__resourceType_1(Type_t * value)
	{
		____resourceType_1 = value;
		Il2CppCodeGenWriteBarrier((&____resourceType_1), value);
	}

	inline static int32_t get_offset_of__resourcePath_2() { return static_cast<int32_t>(offsetof(ScriptableObjectResourceProvider_t4A007E9097B9C258E9A4A7FE39338577B3961F44, ____resourcePath_2)); }
	inline String_t* get__resourcePath_2() const { return ____resourcePath_2; }
	inline String_t** get_address_of__resourcePath_2() { return &____resourcePath_2; }
	inline void set__resourcePath_2(String_t* value)
	{
		____resourcePath_2 = value;
		Il2CppCodeGenWriteBarrier((&____resourcePath_2), value);
	}

	inline static int32_t get_offset_of__extraArguments_3() { return static_cast<int32_t>(offsetof(ScriptableObjectResourceProvider_t4A007E9097B9C258E9A4A7FE39338577B3961F44, ____extraArguments_3)); }
	inline List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * get__extraArguments_3() const { return ____extraArguments_3; }
	inline List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 ** get_address_of__extraArguments_3() { return &____extraArguments_3; }
	inline void set__extraArguments_3(List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * value)
	{
		____extraArguments_3 = value;
		Il2CppCodeGenWriteBarrier((&____extraArguments_3), value);
	}

	inline static int32_t get_offset_of__concreteIdentifier_4() { return static_cast<int32_t>(offsetof(ScriptableObjectResourceProvider_t4A007E9097B9C258E9A4A7FE39338577B3961F44, ____concreteIdentifier_4)); }
	inline RuntimeObject * get__concreteIdentifier_4() const { return ____concreteIdentifier_4; }
	inline RuntimeObject ** get_address_of__concreteIdentifier_4() { return &____concreteIdentifier_4; }
	inline void set__concreteIdentifier_4(RuntimeObject * value)
	{
		____concreteIdentifier_4 = value;
		Il2CppCodeGenWriteBarrier((&____concreteIdentifier_4), value);
	}

	inline static int32_t get_offset_of__createNew_5() { return static_cast<int32_t>(offsetof(ScriptableObjectResourceProvider_t4A007E9097B9C258E9A4A7FE39338577B3961F44, ____createNew_5)); }
	inline bool get__createNew_5() const { return ____createNew_5; }
	inline bool* get_address_of__createNew_5() { return &____createNew_5; }
	inline void set__createNew_5(bool value)
	{
		____createNew_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRIPTABLEOBJECTRESOURCEPROVIDER_T4A007E9097B9C258E9A4A7FE39338577B3961F44_H
#ifndef U3CU3EC_TD6959097A591E9916075DF4F1795A247D91E512F_H
#define U3CU3EC_TD6959097A591E9916075DF4F1795A247D91E512F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ScriptableObjectResourceProvider_<>c
struct  U3CU3Ec_tD6959097A591E9916075DF4F1795A247D91E512F  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tD6959097A591E9916075DF4F1795A247D91E512F_StaticFields
{
public:
	// Zenject.ScriptableObjectResourceProvider_<>c Zenject.ScriptableObjectResourceProvider_<>c::<>9
	U3CU3Ec_tD6959097A591E9916075DF4F1795A247D91E512F * ___U3CU3E9_0;
	// System.Func`2<UnityEngine.Object,UnityEngine.Object> Zenject.ScriptableObjectResourceProvider_<>c::<>9__8_0
	Func_2_t7F9671FD00F31AFFC0C1BEB0761BA9E68AB18E89 * ___U3CU3E9__8_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tD6959097A591E9916075DF4F1795A247D91E512F_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tD6959097A591E9916075DF4F1795A247D91E512F * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tD6959097A591E9916075DF4F1795A247D91E512F ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tD6959097A591E9916075DF4F1795A247D91E512F * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__8_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tD6959097A591E9916075DF4F1795A247D91E512F_StaticFields, ___U3CU3E9__8_0_1)); }
	inline Func_2_t7F9671FD00F31AFFC0C1BEB0761BA9E68AB18E89 * get_U3CU3E9__8_0_1() const { return ___U3CU3E9__8_0_1; }
	inline Func_2_t7F9671FD00F31AFFC0C1BEB0761BA9E68AB18E89 ** get_address_of_U3CU3E9__8_0_1() { return &___U3CU3E9__8_0_1; }
	inline void set_U3CU3E9__8_0_1(Func_2_t7F9671FD00F31AFFC0C1BEB0761BA9E68AB18E89 * value)
	{
		___U3CU3E9__8_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__8_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TD6959097A591E9916075DF4F1795A247D91E512F_H
#ifndef U3CGETALLINSTANCESWITHINJECTSPLITU3ED__8_TF20F96867E0CBF13D5D21959656AA4055165D670_H
#define U3CGETALLINSTANCESWITHINJECTSPLITU3ED__8_TF20F96867E0CBF13D5D21959656AA4055165D670_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ScriptableObjectResourceProvider_<GetAllInstancesWithInjectSplit>d__8
struct  U3CGetAllInstancesWithInjectSplitU3Ed__8_tF20F96867E0CBF13D5D21959656AA4055165D670  : public RuntimeObject
{
public:
	// System.Int32 Zenject.ScriptableObjectResourceProvider_<GetAllInstancesWithInjectSplit>d__8::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Collections.Generic.List`1<System.Object> Zenject.ScriptableObjectResourceProvider_<GetAllInstancesWithInjectSplit>d__8::<>2__current
	List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * ___U3CU3E2__current_1;
	// Zenject.InjectContext Zenject.ScriptableObjectResourceProvider_<GetAllInstancesWithInjectSplit>d__8::context
	InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * ___context_2;
	// Zenject.ScriptableObjectResourceProvider Zenject.ScriptableObjectResourceProvider_<GetAllInstancesWithInjectSplit>d__8::<>4__this
	ScriptableObjectResourceProvider_t4A007E9097B9C258E9A4A7FE39338577B3961F44 * ___U3CU3E4__this_3;
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.ScriptableObjectResourceProvider_<GetAllInstancesWithInjectSplit>d__8::args
	List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * ___args_4;
	// System.Collections.Generic.List`1<System.Object> Zenject.ScriptableObjectResourceProvider_<GetAllInstancesWithInjectSplit>d__8::<objects>5__2
	List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * ___U3CobjectsU3E5__2_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__8_tF20F96867E0CBF13D5D21959656AA4055165D670, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__8_tF20F96867E0CBF13D5D21959656AA4055165D670, ___U3CU3E2__current_1)); }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_context_2() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__8_tF20F96867E0CBF13D5D21959656AA4055165D670, ___context_2)); }
	inline InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * get_context_2() const { return ___context_2; }
	inline InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF ** get_address_of_context_2() { return &___context_2; }
	inline void set_context_2(InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * value)
	{
		___context_2 = value;
		Il2CppCodeGenWriteBarrier((&___context_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__8_tF20F96867E0CBF13D5D21959656AA4055165D670, ___U3CU3E4__this_3)); }
	inline ScriptableObjectResourceProvider_t4A007E9097B9C258E9A4A7FE39338577B3961F44 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline ScriptableObjectResourceProvider_t4A007E9097B9C258E9A4A7FE39338577B3961F44 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(ScriptableObjectResourceProvider_t4A007E9097B9C258E9A4A7FE39338577B3961F44 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_args_4() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__8_tF20F96867E0CBF13D5D21959656AA4055165D670, ___args_4)); }
	inline List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * get_args_4() const { return ___args_4; }
	inline List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 ** get_address_of_args_4() { return &___args_4; }
	inline void set_args_4(List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * value)
	{
		___args_4 = value;
		Il2CppCodeGenWriteBarrier((&___args_4), value);
	}

	inline static int32_t get_offset_of_U3CobjectsU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__8_tF20F96867E0CBF13D5D21959656AA4055165D670, ___U3CobjectsU3E5__2_5)); }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * get_U3CobjectsU3E5__2_5() const { return ___U3CobjectsU3E5__2_5; }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D ** get_address_of_U3CobjectsU3E5__2_5() { return &___U3CobjectsU3E5__2_5; }
	inline void set_U3CobjectsU3E5__2_5(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * value)
	{
		___U3CobjectsU3E5__2_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CobjectsU3E5__2_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETALLINSTANCESWITHINJECTSPLITU3ED__8_TF20F96867E0CBF13D5D21959656AA4055165D670_H
#ifndef SINGLETONID_TF74E433D582ABB3E7532764D59325896A7A8D64C_H
#define SINGLETONID_TF74E433D582ABB3E7532764D59325896A7A8D64C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SingletonId
struct  SingletonId_tF74E433D582ABB3E7532764D59325896A7A8D64C  : public RuntimeObject
{
public:
	// System.Type Zenject.SingletonId::ConcreteType
	Type_t * ___ConcreteType_0;
	// System.Object Zenject.SingletonId::ConcreteIdentifier
	RuntimeObject * ___ConcreteIdentifier_1;

public:
	inline static int32_t get_offset_of_ConcreteType_0() { return static_cast<int32_t>(offsetof(SingletonId_tF74E433D582ABB3E7532764D59325896A7A8D64C, ___ConcreteType_0)); }
	inline Type_t * get_ConcreteType_0() const { return ___ConcreteType_0; }
	inline Type_t ** get_address_of_ConcreteType_0() { return &___ConcreteType_0; }
	inline void set_ConcreteType_0(Type_t * value)
	{
		___ConcreteType_0 = value;
		Il2CppCodeGenWriteBarrier((&___ConcreteType_0), value);
	}

	inline static int32_t get_offset_of_ConcreteIdentifier_1() { return static_cast<int32_t>(offsetof(SingletonId_tF74E433D582ABB3E7532764D59325896A7A8D64C, ___ConcreteIdentifier_1)); }
	inline RuntimeObject * get_ConcreteIdentifier_1() const { return ___ConcreteIdentifier_1; }
	inline RuntimeObject ** get_address_of_ConcreteIdentifier_1() { return &___ConcreteIdentifier_1; }
	inline void set_ConcreteIdentifier_1(RuntimeObject * value)
	{
		___ConcreteIdentifier_1 = value;
		Il2CppCodeGenWriteBarrier((&___ConcreteIdentifier_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETONID_TF74E433D582ABB3E7532764D59325896A7A8D64C_H
#ifndef SINGLETONIMPLIDS_T55CF17EFC231DA900608C4D728265A2BEDC2B593_H
#define SINGLETONIMPLIDS_T55CF17EFC231DA900608C4D728265A2BEDC2B593_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SingletonImplIds
struct  SingletonImplIds_t55CF17EFC231DA900608C4D728265A2BEDC2B593  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETONIMPLIDS_T55CF17EFC231DA900608C4D728265A2BEDC2B593_H
#ifndef TOGETTER_T269C4EED29F0AC2A1C9FB4061EA228DC2DDADED4_H
#define TOGETTER_T269C4EED29F0AC2A1C9FB4061EA228DC2DDADED4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SingletonImplIds_ToGetter
struct  ToGetter_t269C4EED29F0AC2A1C9FB4061EA228DC2DDADED4  : public RuntimeObject
{
public:
	// System.Delegate Zenject.SingletonImplIds_ToGetter::_method
	Delegate_t * ____method_0;
	// System.Object Zenject.SingletonImplIds_ToGetter::_identifier
	RuntimeObject * ____identifier_1;

public:
	inline static int32_t get_offset_of__method_0() { return static_cast<int32_t>(offsetof(ToGetter_t269C4EED29F0AC2A1C9FB4061EA228DC2DDADED4, ____method_0)); }
	inline Delegate_t * get__method_0() const { return ____method_0; }
	inline Delegate_t ** get_address_of__method_0() { return &____method_0; }
	inline void set__method_0(Delegate_t * value)
	{
		____method_0 = value;
		Il2CppCodeGenWriteBarrier((&____method_0), value);
	}

	inline static int32_t get_offset_of__identifier_1() { return static_cast<int32_t>(offsetof(ToGetter_t269C4EED29F0AC2A1C9FB4061EA228DC2DDADED4, ____identifier_1)); }
	inline RuntimeObject * get__identifier_1() const { return ____identifier_1; }
	inline RuntimeObject ** get_address_of__identifier_1() { return &____identifier_1; }
	inline void set__identifier_1(RuntimeObject * value)
	{
		____identifier_1 = value;
		Il2CppCodeGenWriteBarrier((&____identifier_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOGETTER_T269C4EED29F0AC2A1C9FB4061EA228DC2DDADED4_H
#ifndef TOMETHOD_T12F5E0A4D9C370BB507C42523A4401D55EBE2836_H
#define TOMETHOD_T12F5E0A4D9C370BB507C42523A4401D55EBE2836_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SingletonImplIds_ToMethod
struct  ToMethod_t12F5E0A4D9C370BB507C42523A4401D55EBE2836  : public RuntimeObject
{
public:
	// System.Delegate Zenject.SingletonImplIds_ToMethod::_method
	Delegate_t * ____method_0;

public:
	inline static int32_t get_offset_of__method_0() { return static_cast<int32_t>(offsetof(ToMethod_t12F5E0A4D9C370BB507C42523A4401D55EBE2836, ____method_0)); }
	inline Delegate_t * get__method_0() const { return ____method_0; }
	inline Delegate_t ** get_address_of__method_0() { return &____method_0; }
	inline void set__method_0(Delegate_t * value)
	{
		____method_0 = value;
		Il2CppCodeGenWriteBarrier((&____method_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOMETHOD_T12F5E0A4D9C370BB507C42523A4401D55EBE2836_H
#ifndef SINGLETONMARKREGISTRY_TB0ADDC9314F22EFEF3C51F08F71E6E1CB236EC7F_H
#define SINGLETONMARKREGISTRY_TB0ADDC9314F22EFEF3C51F08F71E6E1CB236EC7F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SingletonMarkRegistry
struct  SingletonMarkRegistry_tB0ADDC9314F22EFEF3C51F08F71E6E1CB236EC7F  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<Zenject.SingletonId,Zenject.SingletonTypes> Zenject.SingletonMarkRegistry::_singletonTypes
	Dictionary_2_tD5B7725A3F47CDF6E967A1CE3130E08DA9FF7334 * ____singletonTypes_0;

public:
	inline static int32_t get_offset_of__singletonTypes_0() { return static_cast<int32_t>(offsetof(SingletonMarkRegistry_tB0ADDC9314F22EFEF3C51F08F71E6E1CB236EC7F, ____singletonTypes_0)); }
	inline Dictionary_2_tD5B7725A3F47CDF6E967A1CE3130E08DA9FF7334 * get__singletonTypes_0() const { return ____singletonTypes_0; }
	inline Dictionary_2_tD5B7725A3F47CDF6E967A1CE3130E08DA9FF7334 ** get_address_of__singletonTypes_0() { return &____singletonTypes_0; }
	inline void set__singletonTypes_0(Dictionary_2_tD5B7725A3F47CDF6E967A1CE3130E08DA9FF7334 * value)
	{
		____singletonTypes_0 = value;
		Il2CppCodeGenWriteBarrier((&____singletonTypes_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETONMARKREGISTRY_TB0ADDC9314F22EFEF3C51F08F71E6E1CB236EC7F_H
#ifndef SINGLETONPROVIDERCREATOR_T07218AEFEEBFF6A8B65FB17100F1AB292A94F5AD_H
#define SINGLETONPROVIDERCREATOR_T07218AEFEEBFF6A8B65FB17100F1AB292A94F5AD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SingletonProviderCreator
struct  SingletonProviderCreator_t07218AEFEEBFF6A8B65FB17100F1AB292A94F5AD  : public RuntimeObject
{
public:
	// Zenject.StandardSingletonProviderCreator Zenject.SingletonProviderCreator::_standardProviderCreator
	StandardSingletonProviderCreator_t591FCA8A24159723185A79698D93BD3CE3647E80 * ____standardProviderCreator_0;
	// Zenject.SubContainerSingletonProviderCreatorByMethod Zenject.SingletonProviderCreator::_subContainerMethodProviderCreator
	SubContainerSingletonProviderCreatorByMethod_t875112116B40E0B708D4EE4601E758BACF9F1DED * ____subContainerMethodProviderCreator_1;
	// Zenject.SubContainerSingletonProviderCreatorByInstaller Zenject.SingletonProviderCreator::_subContainerInstallerProviderCreator
	SubContainerSingletonProviderCreatorByInstaller_t0612A1F030297B7EEDB675CC61E90C60AAED986E * ____subContainerInstallerProviderCreator_2;
	// Zenject.SubContainerSingletonProviderCreatorByNewPrefab Zenject.SingletonProviderCreator::_subContainerPrefabProviderCreator
	SubContainerSingletonProviderCreatorByNewPrefab_tD1BD0BC2BC3FB62EE8DDD51AE521B287C673A19B * ____subContainerPrefabProviderCreator_3;
	// Zenject.SubContainerSingletonProviderCreatorByNewPrefabResource Zenject.SingletonProviderCreator::_subContainerPrefabResourceProviderCreator
	SubContainerSingletonProviderCreatorByNewPrefabResource_t0F56AB2C3837D0E2117B52265FCBD5E21A8295AB * ____subContainerPrefabResourceProviderCreator_4;
	// Zenject.PrefabSingletonProviderCreator Zenject.SingletonProviderCreator::_prefabProviderCreator
	PrefabSingletonProviderCreator_t2C3CC78B15E2FB38FE6DD8F2D281F6E4F0533075 * ____prefabProviderCreator_5;
	// Zenject.PrefabResourceSingletonProviderCreator Zenject.SingletonProviderCreator::_prefabResourceProviderCreator
	PrefabResourceSingletonProviderCreator_t9A996ED86F32A28313FCA6B25175E470AFCD8A41 * ____prefabResourceProviderCreator_6;

public:
	inline static int32_t get_offset_of__standardProviderCreator_0() { return static_cast<int32_t>(offsetof(SingletonProviderCreator_t07218AEFEEBFF6A8B65FB17100F1AB292A94F5AD, ____standardProviderCreator_0)); }
	inline StandardSingletonProviderCreator_t591FCA8A24159723185A79698D93BD3CE3647E80 * get__standardProviderCreator_0() const { return ____standardProviderCreator_0; }
	inline StandardSingletonProviderCreator_t591FCA8A24159723185A79698D93BD3CE3647E80 ** get_address_of__standardProviderCreator_0() { return &____standardProviderCreator_0; }
	inline void set__standardProviderCreator_0(StandardSingletonProviderCreator_t591FCA8A24159723185A79698D93BD3CE3647E80 * value)
	{
		____standardProviderCreator_0 = value;
		Il2CppCodeGenWriteBarrier((&____standardProviderCreator_0), value);
	}

	inline static int32_t get_offset_of__subContainerMethodProviderCreator_1() { return static_cast<int32_t>(offsetof(SingletonProviderCreator_t07218AEFEEBFF6A8B65FB17100F1AB292A94F5AD, ____subContainerMethodProviderCreator_1)); }
	inline SubContainerSingletonProviderCreatorByMethod_t875112116B40E0B708D4EE4601E758BACF9F1DED * get__subContainerMethodProviderCreator_1() const { return ____subContainerMethodProviderCreator_1; }
	inline SubContainerSingletonProviderCreatorByMethod_t875112116B40E0B708D4EE4601E758BACF9F1DED ** get_address_of__subContainerMethodProviderCreator_1() { return &____subContainerMethodProviderCreator_1; }
	inline void set__subContainerMethodProviderCreator_1(SubContainerSingletonProviderCreatorByMethod_t875112116B40E0B708D4EE4601E758BACF9F1DED * value)
	{
		____subContainerMethodProviderCreator_1 = value;
		Il2CppCodeGenWriteBarrier((&____subContainerMethodProviderCreator_1), value);
	}

	inline static int32_t get_offset_of__subContainerInstallerProviderCreator_2() { return static_cast<int32_t>(offsetof(SingletonProviderCreator_t07218AEFEEBFF6A8B65FB17100F1AB292A94F5AD, ____subContainerInstallerProviderCreator_2)); }
	inline SubContainerSingletonProviderCreatorByInstaller_t0612A1F030297B7EEDB675CC61E90C60AAED986E * get__subContainerInstallerProviderCreator_2() const { return ____subContainerInstallerProviderCreator_2; }
	inline SubContainerSingletonProviderCreatorByInstaller_t0612A1F030297B7EEDB675CC61E90C60AAED986E ** get_address_of__subContainerInstallerProviderCreator_2() { return &____subContainerInstallerProviderCreator_2; }
	inline void set__subContainerInstallerProviderCreator_2(SubContainerSingletonProviderCreatorByInstaller_t0612A1F030297B7EEDB675CC61E90C60AAED986E * value)
	{
		____subContainerInstallerProviderCreator_2 = value;
		Il2CppCodeGenWriteBarrier((&____subContainerInstallerProviderCreator_2), value);
	}

	inline static int32_t get_offset_of__subContainerPrefabProviderCreator_3() { return static_cast<int32_t>(offsetof(SingletonProviderCreator_t07218AEFEEBFF6A8B65FB17100F1AB292A94F5AD, ____subContainerPrefabProviderCreator_3)); }
	inline SubContainerSingletonProviderCreatorByNewPrefab_tD1BD0BC2BC3FB62EE8DDD51AE521B287C673A19B * get__subContainerPrefabProviderCreator_3() const { return ____subContainerPrefabProviderCreator_3; }
	inline SubContainerSingletonProviderCreatorByNewPrefab_tD1BD0BC2BC3FB62EE8DDD51AE521B287C673A19B ** get_address_of__subContainerPrefabProviderCreator_3() { return &____subContainerPrefabProviderCreator_3; }
	inline void set__subContainerPrefabProviderCreator_3(SubContainerSingletonProviderCreatorByNewPrefab_tD1BD0BC2BC3FB62EE8DDD51AE521B287C673A19B * value)
	{
		____subContainerPrefabProviderCreator_3 = value;
		Il2CppCodeGenWriteBarrier((&____subContainerPrefabProviderCreator_3), value);
	}

	inline static int32_t get_offset_of__subContainerPrefabResourceProviderCreator_4() { return static_cast<int32_t>(offsetof(SingletonProviderCreator_t07218AEFEEBFF6A8B65FB17100F1AB292A94F5AD, ____subContainerPrefabResourceProviderCreator_4)); }
	inline SubContainerSingletonProviderCreatorByNewPrefabResource_t0F56AB2C3837D0E2117B52265FCBD5E21A8295AB * get__subContainerPrefabResourceProviderCreator_4() const { return ____subContainerPrefabResourceProviderCreator_4; }
	inline SubContainerSingletonProviderCreatorByNewPrefabResource_t0F56AB2C3837D0E2117B52265FCBD5E21A8295AB ** get_address_of__subContainerPrefabResourceProviderCreator_4() { return &____subContainerPrefabResourceProviderCreator_4; }
	inline void set__subContainerPrefabResourceProviderCreator_4(SubContainerSingletonProviderCreatorByNewPrefabResource_t0F56AB2C3837D0E2117B52265FCBD5E21A8295AB * value)
	{
		____subContainerPrefabResourceProviderCreator_4 = value;
		Il2CppCodeGenWriteBarrier((&____subContainerPrefabResourceProviderCreator_4), value);
	}

	inline static int32_t get_offset_of__prefabProviderCreator_5() { return static_cast<int32_t>(offsetof(SingletonProviderCreator_t07218AEFEEBFF6A8B65FB17100F1AB292A94F5AD, ____prefabProviderCreator_5)); }
	inline PrefabSingletonProviderCreator_t2C3CC78B15E2FB38FE6DD8F2D281F6E4F0533075 * get__prefabProviderCreator_5() const { return ____prefabProviderCreator_5; }
	inline PrefabSingletonProviderCreator_t2C3CC78B15E2FB38FE6DD8F2D281F6E4F0533075 ** get_address_of__prefabProviderCreator_5() { return &____prefabProviderCreator_5; }
	inline void set__prefabProviderCreator_5(PrefabSingletonProviderCreator_t2C3CC78B15E2FB38FE6DD8F2D281F6E4F0533075 * value)
	{
		____prefabProviderCreator_5 = value;
		Il2CppCodeGenWriteBarrier((&____prefabProviderCreator_5), value);
	}

	inline static int32_t get_offset_of__prefabResourceProviderCreator_6() { return static_cast<int32_t>(offsetof(SingletonProviderCreator_t07218AEFEEBFF6A8B65FB17100F1AB292A94F5AD, ____prefabResourceProviderCreator_6)); }
	inline PrefabResourceSingletonProviderCreator_t9A996ED86F32A28313FCA6B25175E470AFCD8A41 * get__prefabResourceProviderCreator_6() const { return ____prefabResourceProviderCreator_6; }
	inline PrefabResourceSingletonProviderCreator_t9A996ED86F32A28313FCA6B25175E470AFCD8A41 ** get_address_of__prefabResourceProviderCreator_6() { return &____prefabResourceProviderCreator_6; }
	inline void set__prefabResourceProviderCreator_6(PrefabResourceSingletonProviderCreator_t9A996ED86F32A28313FCA6B25175E470AFCD8A41 * value)
	{
		____prefabResourceProviderCreator_6 = value;
		Il2CppCodeGenWriteBarrier((&____prefabResourceProviderCreator_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETONPROVIDERCREATOR_T07218AEFEEBFF6A8B65FB17100F1AB292A94F5AD_H
#ifndef STANDARDSINGLETONPROVIDERCREATOR_T591FCA8A24159723185A79698D93BD3CE3647E80_H
#define STANDARDSINGLETONPROVIDERCREATOR_T591FCA8A24159723185A79698D93BD3CE3647E80_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.StandardSingletonProviderCreator
struct  StandardSingletonProviderCreator_t591FCA8A24159723185A79698D93BD3CE3647E80  : public RuntimeObject
{
public:
	// Zenject.SingletonMarkRegistry Zenject.StandardSingletonProviderCreator::_markRegistry
	SingletonMarkRegistry_tB0ADDC9314F22EFEF3C51F08F71E6E1CB236EC7F * ____markRegistry_0;
	// System.Collections.Generic.Dictionary`2<Zenject.SingletonId,Zenject.StandardSingletonProviderCreator_ProviderInfo> Zenject.StandardSingletonProviderCreator::_providerMap
	Dictionary_2_t778BDA760E0ACC7135173A9EE434CF4AB4028D26 * ____providerMap_1;
	// Zenject.DiContainer Zenject.StandardSingletonProviderCreator::_container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____container_2;

public:
	inline static int32_t get_offset_of__markRegistry_0() { return static_cast<int32_t>(offsetof(StandardSingletonProviderCreator_t591FCA8A24159723185A79698D93BD3CE3647E80, ____markRegistry_0)); }
	inline SingletonMarkRegistry_tB0ADDC9314F22EFEF3C51F08F71E6E1CB236EC7F * get__markRegistry_0() const { return ____markRegistry_0; }
	inline SingletonMarkRegistry_tB0ADDC9314F22EFEF3C51F08F71E6E1CB236EC7F ** get_address_of__markRegistry_0() { return &____markRegistry_0; }
	inline void set__markRegistry_0(SingletonMarkRegistry_tB0ADDC9314F22EFEF3C51F08F71E6E1CB236EC7F * value)
	{
		____markRegistry_0 = value;
		Il2CppCodeGenWriteBarrier((&____markRegistry_0), value);
	}

	inline static int32_t get_offset_of__providerMap_1() { return static_cast<int32_t>(offsetof(StandardSingletonProviderCreator_t591FCA8A24159723185A79698D93BD3CE3647E80, ____providerMap_1)); }
	inline Dictionary_2_t778BDA760E0ACC7135173A9EE434CF4AB4028D26 * get__providerMap_1() const { return ____providerMap_1; }
	inline Dictionary_2_t778BDA760E0ACC7135173A9EE434CF4AB4028D26 ** get_address_of__providerMap_1() { return &____providerMap_1; }
	inline void set__providerMap_1(Dictionary_2_t778BDA760E0ACC7135173A9EE434CF4AB4028D26 * value)
	{
		____providerMap_1 = value;
		Il2CppCodeGenWriteBarrier((&____providerMap_1), value);
	}

	inline static int32_t get_offset_of__container_2() { return static_cast<int32_t>(offsetof(StandardSingletonProviderCreator_t591FCA8A24159723185A79698D93BD3CE3647E80, ____container_2)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__container_2() const { return ____container_2; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__container_2() { return &____container_2; }
	inline void set__container_2(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____container_2 = value;
		Il2CppCodeGenWriteBarrier((&____container_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STANDARDSINGLETONPROVIDERCREATOR_T591FCA8A24159723185A79698D93BD3CE3647E80_H
#ifndef SUBCONTAINERCREATORBYINSTALLER_TB27CBFE878C3922D781B595813F0A35493A9E1C5_H
#define SUBCONTAINERCREATORBYINSTALLER_TB27CBFE878C3922D781B595813F0A35493A9E1C5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerCreatorByInstaller
struct  SubContainerCreatorByInstaller_tB27CBFE878C3922D781B595813F0A35493A9E1C5  : public RuntimeObject
{
public:
	// System.Type Zenject.SubContainerCreatorByInstaller::_installerType
	Type_t * ____installerType_0;
	// Zenject.DiContainer Zenject.SubContainerCreatorByInstaller::_container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____container_1;
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.SubContainerCreatorByInstaller::_extraArgs
	List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * ____extraArgs_2;

public:
	inline static int32_t get_offset_of__installerType_0() { return static_cast<int32_t>(offsetof(SubContainerCreatorByInstaller_tB27CBFE878C3922D781B595813F0A35493A9E1C5, ____installerType_0)); }
	inline Type_t * get__installerType_0() const { return ____installerType_0; }
	inline Type_t ** get_address_of__installerType_0() { return &____installerType_0; }
	inline void set__installerType_0(Type_t * value)
	{
		____installerType_0 = value;
		Il2CppCodeGenWriteBarrier((&____installerType_0), value);
	}

	inline static int32_t get_offset_of__container_1() { return static_cast<int32_t>(offsetof(SubContainerCreatorByInstaller_tB27CBFE878C3922D781B595813F0A35493A9E1C5, ____container_1)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__container_1() const { return ____container_1; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__container_1() { return &____container_1; }
	inline void set__container_1(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____container_1 = value;
		Il2CppCodeGenWriteBarrier((&____container_1), value);
	}

	inline static int32_t get_offset_of__extraArgs_2() { return static_cast<int32_t>(offsetof(SubContainerCreatorByInstaller_tB27CBFE878C3922D781B595813F0A35493A9E1C5, ____extraArgs_2)); }
	inline List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * get__extraArgs_2() const { return ____extraArgs_2; }
	inline List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 ** get_address_of__extraArgs_2() { return &____extraArgs_2; }
	inline void set__extraArgs_2(List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * value)
	{
		____extraArgs_2 = value;
		Il2CppCodeGenWriteBarrier((&____extraArgs_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBCONTAINERCREATORBYINSTALLER_TB27CBFE878C3922D781B595813F0A35493A9E1C5_H
#ifndef SUBCONTAINERCREATORBYMETHOD_T563F1560E9AD8C4FDE2A434DBB21F7DBBD88E5F2_H
#define SUBCONTAINERCREATORBYMETHOD_T563F1560E9AD8C4FDE2A434DBB21F7DBBD88E5F2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerCreatorByMethod
struct  SubContainerCreatorByMethod_t563F1560E9AD8C4FDE2A434DBB21F7DBBD88E5F2  : public RuntimeObject
{
public:
	// System.Action`1<Zenject.DiContainer> Zenject.SubContainerCreatorByMethod::_installMethod
	Action_1_t47FB75EB7E275E7EC20C1ADC32AC936BD0BD4E84 * ____installMethod_0;
	// Zenject.DiContainer Zenject.SubContainerCreatorByMethod::_container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____container_1;

public:
	inline static int32_t get_offset_of__installMethod_0() { return static_cast<int32_t>(offsetof(SubContainerCreatorByMethod_t563F1560E9AD8C4FDE2A434DBB21F7DBBD88E5F2, ____installMethod_0)); }
	inline Action_1_t47FB75EB7E275E7EC20C1ADC32AC936BD0BD4E84 * get__installMethod_0() const { return ____installMethod_0; }
	inline Action_1_t47FB75EB7E275E7EC20C1ADC32AC936BD0BD4E84 ** get_address_of__installMethod_0() { return &____installMethod_0; }
	inline void set__installMethod_0(Action_1_t47FB75EB7E275E7EC20C1ADC32AC936BD0BD4E84 * value)
	{
		____installMethod_0 = value;
		Il2CppCodeGenWriteBarrier((&____installMethod_0), value);
	}

	inline static int32_t get_offset_of__container_1() { return static_cast<int32_t>(offsetof(SubContainerCreatorByMethod_t563F1560E9AD8C4FDE2A434DBB21F7DBBD88E5F2, ____container_1)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__container_1() const { return ____container_1; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__container_1() { return &____container_1; }
	inline void set__container_1(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____container_1 = value;
		Il2CppCodeGenWriteBarrier((&____container_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBCONTAINERCREATORBYMETHOD_T563F1560E9AD8C4FDE2A434DBB21F7DBBD88E5F2_H
#ifndef SUBCONTAINERCREATORBYNEWPREFAB_T5F6159D02A80C3C750D631719F6E5794F1B431E1_H
#define SUBCONTAINERCREATORBYNEWPREFAB_T5F6159D02A80C3C750D631719F6E5794F1B431E1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerCreatorByNewPrefab
struct  SubContainerCreatorByNewPrefab_t5F6159D02A80C3C750D631719F6E5794F1B431E1  : public RuntimeObject
{
public:
	// Zenject.GameObjectCreationParameters Zenject.SubContainerCreatorByNewPrefab::_gameObjectBindInfo
	GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A * ____gameObjectBindInfo_0;
	// Zenject.IPrefabProvider Zenject.SubContainerCreatorByNewPrefab::_prefabProvider
	RuntimeObject* ____prefabProvider_1;
	// Zenject.DiContainer Zenject.SubContainerCreatorByNewPrefab::_container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____container_2;

public:
	inline static int32_t get_offset_of__gameObjectBindInfo_0() { return static_cast<int32_t>(offsetof(SubContainerCreatorByNewPrefab_t5F6159D02A80C3C750D631719F6E5794F1B431E1, ____gameObjectBindInfo_0)); }
	inline GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A * get__gameObjectBindInfo_0() const { return ____gameObjectBindInfo_0; }
	inline GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A ** get_address_of__gameObjectBindInfo_0() { return &____gameObjectBindInfo_0; }
	inline void set__gameObjectBindInfo_0(GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A * value)
	{
		____gameObjectBindInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&____gameObjectBindInfo_0), value);
	}

	inline static int32_t get_offset_of__prefabProvider_1() { return static_cast<int32_t>(offsetof(SubContainerCreatorByNewPrefab_t5F6159D02A80C3C750D631719F6E5794F1B431E1, ____prefabProvider_1)); }
	inline RuntimeObject* get__prefabProvider_1() const { return ____prefabProvider_1; }
	inline RuntimeObject** get_address_of__prefabProvider_1() { return &____prefabProvider_1; }
	inline void set__prefabProvider_1(RuntimeObject* value)
	{
		____prefabProvider_1 = value;
		Il2CppCodeGenWriteBarrier((&____prefabProvider_1), value);
	}

	inline static int32_t get_offset_of__container_2() { return static_cast<int32_t>(offsetof(SubContainerCreatorByNewPrefab_t5F6159D02A80C3C750D631719F6E5794F1B431E1, ____container_2)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__container_2() const { return ____container_2; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__container_2() { return &____container_2; }
	inline void set__container_2(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____container_2 = value;
		Il2CppCodeGenWriteBarrier((&____container_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBCONTAINERCREATORBYNEWPREFAB_T5F6159D02A80C3C750D631719F6E5794F1B431E1_H
#ifndef SUBCONTAINERCREATORBYNEWPREFABDYNAMICCONTEXT_T0FF9DB5FAA6CE972FFABBC76BA89560C17FFEC94_H
#define SUBCONTAINERCREATORBYNEWPREFABDYNAMICCONTEXT_T0FF9DB5FAA6CE972FFABBC76BA89560C17FFEC94_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerCreatorByNewPrefabDynamicContext
struct  SubContainerCreatorByNewPrefabDynamicContext_t0FF9DB5FAA6CE972FFABBC76BA89560C17FFEC94  : public RuntimeObject
{
public:
	// Zenject.GameObjectCreationParameters Zenject.SubContainerCreatorByNewPrefabDynamicContext::_gameObjectBindInfo
	GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A * ____gameObjectBindInfo_0;
	// Zenject.IPrefabProvider Zenject.SubContainerCreatorByNewPrefabDynamicContext::_prefabProvider
	RuntimeObject* ____prefabProvider_1;
	// Zenject.DiContainer Zenject.SubContainerCreatorByNewPrefabDynamicContext::_container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____container_2;

public:
	inline static int32_t get_offset_of__gameObjectBindInfo_0() { return static_cast<int32_t>(offsetof(SubContainerCreatorByNewPrefabDynamicContext_t0FF9DB5FAA6CE972FFABBC76BA89560C17FFEC94, ____gameObjectBindInfo_0)); }
	inline GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A * get__gameObjectBindInfo_0() const { return ____gameObjectBindInfo_0; }
	inline GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A ** get_address_of__gameObjectBindInfo_0() { return &____gameObjectBindInfo_0; }
	inline void set__gameObjectBindInfo_0(GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A * value)
	{
		____gameObjectBindInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&____gameObjectBindInfo_0), value);
	}

	inline static int32_t get_offset_of__prefabProvider_1() { return static_cast<int32_t>(offsetof(SubContainerCreatorByNewPrefabDynamicContext_t0FF9DB5FAA6CE972FFABBC76BA89560C17FFEC94, ____prefabProvider_1)); }
	inline RuntimeObject* get__prefabProvider_1() const { return ____prefabProvider_1; }
	inline RuntimeObject** get_address_of__prefabProvider_1() { return &____prefabProvider_1; }
	inline void set__prefabProvider_1(RuntimeObject* value)
	{
		____prefabProvider_1 = value;
		Il2CppCodeGenWriteBarrier((&____prefabProvider_1), value);
	}

	inline static int32_t get_offset_of__container_2() { return static_cast<int32_t>(offsetof(SubContainerCreatorByNewPrefabDynamicContext_t0FF9DB5FAA6CE972FFABBC76BA89560C17FFEC94, ____container_2)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__container_2() const { return ____container_2; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__container_2() { return &____container_2; }
	inline void set__container_2(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____container_2 = value;
		Il2CppCodeGenWriteBarrier((&____container_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBCONTAINERCREATORBYNEWPREFABDYNAMICCONTEXT_T0FF9DB5FAA6CE972FFABBC76BA89560C17FFEC94_H
#ifndef U3CU3EC__DISPLAYCLASS3_0_TD4D46C61D919699AFCE201D76AD43BE6E0D01F69_H
#define U3CU3EC__DISPLAYCLASS3_0_TD4D46C61D919699AFCE201D76AD43BE6E0D01F69_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerCreatorByNewPrefabInstaller_<>c__DisplayClass3_0
struct  U3CU3Ec__DisplayClass3_0_tD4D46C61D919699AFCE201D76AD43BE6E0D01F69  : public RuntimeObject
{
public:
	// Zenject.SubContainerCreatorByNewPrefabInstaller Zenject.SubContainerCreatorByNewPrefabInstaller_<>c__DisplayClass3_0::<>4__this
	SubContainerCreatorByNewPrefabInstaller_t8C6712ED0C9D7CECCB02AB69C9FC69AB41F933B9 * ___U3CU3E4__this_0;
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.SubContainerCreatorByNewPrefabInstaller_<>c__DisplayClass3_0::args
	List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * ___args_1;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_tD4D46C61D919699AFCE201D76AD43BE6E0D01F69, ___U3CU3E4__this_0)); }
	inline SubContainerCreatorByNewPrefabInstaller_t8C6712ED0C9D7CECCB02AB69C9FC69AB41F933B9 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline SubContainerCreatorByNewPrefabInstaller_t8C6712ED0C9D7CECCB02AB69C9FC69AB41F933B9 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(SubContainerCreatorByNewPrefabInstaller_t8C6712ED0C9D7CECCB02AB69C9FC69AB41F933B9 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_args_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_tD4D46C61D919699AFCE201D76AD43BE6E0D01F69, ___args_1)); }
	inline List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * get_args_1() const { return ___args_1; }
	inline List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 ** get_address_of_args_1() { return &___args_1; }
	inline void set_args_1(List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * value)
	{
		___args_1 = value;
		Il2CppCodeGenWriteBarrier((&___args_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS3_0_TD4D46C61D919699AFCE201D76AD43BE6E0D01F69_H
#ifndef SUBCONTAINERCREATORBYNEWPREFABWITHPARAMS_T7FAF9FE7D9537562FAC1474B20783DFE33D4F8AE_H
#define SUBCONTAINERCREATORBYNEWPREFABWITHPARAMS_T7FAF9FE7D9537562FAC1474B20783DFE33D4F8AE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerCreatorByNewPrefabWithParams
struct  SubContainerCreatorByNewPrefabWithParams_t7FAF9FE7D9537562FAC1474B20783DFE33D4F8AE  : public RuntimeObject
{
public:
	// Zenject.DiContainer Zenject.SubContainerCreatorByNewPrefabWithParams::_container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____container_0;
	// Zenject.IPrefabProvider Zenject.SubContainerCreatorByNewPrefabWithParams::_prefabProvider
	RuntimeObject* ____prefabProvider_1;
	// System.Type Zenject.SubContainerCreatorByNewPrefabWithParams::_installerType
	Type_t * ____installerType_2;
	// Zenject.GameObjectCreationParameters Zenject.SubContainerCreatorByNewPrefabWithParams::_gameObjectBindInfo
	GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A * ____gameObjectBindInfo_3;

public:
	inline static int32_t get_offset_of__container_0() { return static_cast<int32_t>(offsetof(SubContainerCreatorByNewPrefabWithParams_t7FAF9FE7D9537562FAC1474B20783DFE33D4F8AE, ____container_0)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__container_0() const { return ____container_0; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__container_0() { return &____container_0; }
	inline void set__container_0(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____container_0 = value;
		Il2CppCodeGenWriteBarrier((&____container_0), value);
	}

	inline static int32_t get_offset_of__prefabProvider_1() { return static_cast<int32_t>(offsetof(SubContainerCreatorByNewPrefabWithParams_t7FAF9FE7D9537562FAC1474B20783DFE33D4F8AE, ____prefabProvider_1)); }
	inline RuntimeObject* get__prefabProvider_1() const { return ____prefabProvider_1; }
	inline RuntimeObject** get_address_of__prefabProvider_1() { return &____prefabProvider_1; }
	inline void set__prefabProvider_1(RuntimeObject* value)
	{
		____prefabProvider_1 = value;
		Il2CppCodeGenWriteBarrier((&____prefabProvider_1), value);
	}

	inline static int32_t get_offset_of__installerType_2() { return static_cast<int32_t>(offsetof(SubContainerCreatorByNewPrefabWithParams_t7FAF9FE7D9537562FAC1474B20783DFE33D4F8AE, ____installerType_2)); }
	inline Type_t * get__installerType_2() const { return ____installerType_2; }
	inline Type_t ** get_address_of__installerType_2() { return &____installerType_2; }
	inline void set__installerType_2(Type_t * value)
	{
		____installerType_2 = value;
		Il2CppCodeGenWriteBarrier((&____installerType_2), value);
	}

	inline static int32_t get_offset_of__gameObjectBindInfo_3() { return static_cast<int32_t>(offsetof(SubContainerCreatorByNewPrefabWithParams_t7FAF9FE7D9537562FAC1474B20783DFE33D4F8AE, ____gameObjectBindInfo_3)); }
	inline GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A * get__gameObjectBindInfo_3() const { return ____gameObjectBindInfo_3; }
	inline GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A ** get_address_of__gameObjectBindInfo_3() { return &____gameObjectBindInfo_3; }
	inline void set__gameObjectBindInfo_3(GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A * value)
	{
		____gameObjectBindInfo_3 = value;
		Il2CppCodeGenWriteBarrier((&____gameObjectBindInfo_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBCONTAINERCREATORBYNEWPREFABWITHPARAMS_T7FAF9FE7D9537562FAC1474B20783DFE33D4F8AE_H
#ifndef U3CU3EC__DISPLAYCLASS7_0_T386BF28AE8520D25ADE3F43C3AA954DBE62E0C82_H
#define U3CU3EC__DISPLAYCLASS7_0_T386BF28AE8520D25ADE3F43C3AA954DBE62E0C82_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerCreatorByNewPrefabWithParams_<>c__DisplayClass7_0
struct  U3CU3Ec__DisplayClass7_0_t386BF28AE8520D25ADE3F43C3AA954DBE62E0C82  : public RuntimeObject
{
public:
	// Zenject.TypeValuePair Zenject.SubContainerCreatorByNewPrefabWithParams_<>c__DisplayClass7_0::argPair
	TypeValuePair_tD309CDC658476FDDF08345C92D3F4D7A0A1F9792 * ___argPair_0;

public:
	inline static int32_t get_offset_of_argPair_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass7_0_t386BF28AE8520D25ADE3F43C3AA954DBE62E0C82, ___argPair_0)); }
	inline TypeValuePair_tD309CDC658476FDDF08345C92D3F4D7A0A1F9792 * get_argPair_0() const { return ___argPair_0; }
	inline TypeValuePair_tD309CDC658476FDDF08345C92D3F4D7A0A1F9792 ** get_address_of_argPair_0() { return &___argPair_0; }
	inline void set_argPair_0(TypeValuePair_tD309CDC658476FDDF08345C92D3F4D7A0A1F9792 * value)
	{
		___argPair_0 = value;
		Il2CppCodeGenWriteBarrier((&___argPair_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS7_0_T386BF28AE8520D25ADE3F43C3AA954DBE62E0C82_H
#ifndef SUBCONTAINERCREATORCACHED_T8C3674F84167B2FA6DC79C8B318D605610F10EA4_H
#define SUBCONTAINERCREATORCACHED_T8C3674F84167B2FA6DC79C8B318D605610F10EA4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerCreatorCached
struct  SubContainerCreatorCached_t8C3674F84167B2FA6DC79C8B318D605610F10EA4  : public RuntimeObject
{
public:
	// Zenject.ISubContainerCreator Zenject.SubContainerCreatorCached::_subCreator
	RuntimeObject* ____subCreator_0;
	// System.Boolean Zenject.SubContainerCreatorCached::_isLookingUp
	bool ____isLookingUp_1;
	// Zenject.DiContainer Zenject.SubContainerCreatorCached::_subContainer
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____subContainer_2;

public:
	inline static int32_t get_offset_of__subCreator_0() { return static_cast<int32_t>(offsetof(SubContainerCreatorCached_t8C3674F84167B2FA6DC79C8B318D605610F10EA4, ____subCreator_0)); }
	inline RuntimeObject* get__subCreator_0() const { return ____subCreator_0; }
	inline RuntimeObject** get_address_of__subCreator_0() { return &____subCreator_0; }
	inline void set__subCreator_0(RuntimeObject* value)
	{
		____subCreator_0 = value;
		Il2CppCodeGenWriteBarrier((&____subCreator_0), value);
	}

	inline static int32_t get_offset_of__isLookingUp_1() { return static_cast<int32_t>(offsetof(SubContainerCreatorCached_t8C3674F84167B2FA6DC79C8B318D605610F10EA4, ____isLookingUp_1)); }
	inline bool get__isLookingUp_1() const { return ____isLookingUp_1; }
	inline bool* get_address_of__isLookingUp_1() { return &____isLookingUp_1; }
	inline void set__isLookingUp_1(bool value)
	{
		____isLookingUp_1 = value;
	}

	inline static int32_t get_offset_of__subContainer_2() { return static_cast<int32_t>(offsetof(SubContainerCreatorCached_t8C3674F84167B2FA6DC79C8B318D605610F10EA4, ____subContainer_2)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__subContainer_2() const { return ____subContainer_2; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__subContainer_2() { return &____subContainer_2; }
	inline void set__subContainer_2(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____subContainer_2 = value;
		Il2CppCodeGenWriteBarrier((&____subContainer_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBCONTAINERCREATORCACHED_T8C3674F84167B2FA6DC79C8B318D605610F10EA4_H
#ifndef SUBCONTAINERDEPENDENCYPROVIDER_T6C320EB6D6F4FBBA052FEAC5771F9985BF1E314D_H
#define SUBCONTAINERDEPENDENCYPROVIDER_T6C320EB6D6F4FBBA052FEAC5771F9985BF1E314D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerDependencyProvider
struct  SubContainerDependencyProvider_t6C320EB6D6F4FBBA052FEAC5771F9985BF1E314D  : public RuntimeObject
{
public:
	// Zenject.ISubContainerCreator Zenject.SubContainerDependencyProvider::_subContainerCreator
	RuntimeObject* ____subContainerCreator_0;
	// System.Type Zenject.SubContainerDependencyProvider::_dependencyType
	Type_t * ____dependencyType_1;
	// System.Object Zenject.SubContainerDependencyProvider::_identifier
	RuntimeObject * ____identifier_2;

public:
	inline static int32_t get_offset_of__subContainerCreator_0() { return static_cast<int32_t>(offsetof(SubContainerDependencyProvider_t6C320EB6D6F4FBBA052FEAC5771F9985BF1E314D, ____subContainerCreator_0)); }
	inline RuntimeObject* get__subContainerCreator_0() const { return ____subContainerCreator_0; }
	inline RuntimeObject** get_address_of__subContainerCreator_0() { return &____subContainerCreator_0; }
	inline void set__subContainerCreator_0(RuntimeObject* value)
	{
		____subContainerCreator_0 = value;
		Il2CppCodeGenWriteBarrier((&____subContainerCreator_0), value);
	}

	inline static int32_t get_offset_of__dependencyType_1() { return static_cast<int32_t>(offsetof(SubContainerDependencyProvider_t6C320EB6D6F4FBBA052FEAC5771F9985BF1E314D, ____dependencyType_1)); }
	inline Type_t * get__dependencyType_1() const { return ____dependencyType_1; }
	inline Type_t ** get_address_of__dependencyType_1() { return &____dependencyType_1; }
	inline void set__dependencyType_1(Type_t * value)
	{
		____dependencyType_1 = value;
		Il2CppCodeGenWriteBarrier((&____dependencyType_1), value);
	}

	inline static int32_t get_offset_of__identifier_2() { return static_cast<int32_t>(offsetof(SubContainerDependencyProvider_t6C320EB6D6F4FBBA052FEAC5771F9985BF1E314D, ____identifier_2)); }
	inline RuntimeObject * get__identifier_2() const { return ____identifier_2; }
	inline RuntimeObject ** get_address_of__identifier_2() { return &____identifier_2; }
	inline void set__identifier_2(RuntimeObject * value)
	{
		____identifier_2 = value;
		Il2CppCodeGenWriteBarrier((&____identifier_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBCONTAINERDEPENDENCYPROVIDER_T6C320EB6D6F4FBBA052FEAC5771F9985BF1E314D_H
#ifndef U3CGETALLINSTANCESWITHINJECTSPLITU3ED__6_T583E658E9E0C6B896656C9D6FFA54AA575D22D3F_H
#define U3CGETALLINSTANCESWITHINJECTSPLITU3ED__6_T583E658E9E0C6B896656C9D6FFA54AA575D22D3F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerDependencyProvider_<GetAllInstancesWithInjectSplit>d__6
struct  U3CGetAllInstancesWithInjectSplitU3Ed__6_t583E658E9E0C6B896656C9D6FFA54AA575D22D3F  : public RuntimeObject
{
public:
	// System.Int32 Zenject.SubContainerDependencyProvider_<GetAllInstancesWithInjectSplit>d__6::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Collections.Generic.List`1<System.Object> Zenject.SubContainerDependencyProvider_<GetAllInstancesWithInjectSplit>d__6::<>2__current
	List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * ___U3CU3E2__current_1;
	// Zenject.InjectContext Zenject.SubContainerDependencyProvider_<GetAllInstancesWithInjectSplit>d__6::context
	InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * ___context_2;
	// Zenject.SubContainerDependencyProvider Zenject.SubContainerDependencyProvider_<GetAllInstancesWithInjectSplit>d__6::<>4__this
	SubContainerDependencyProvider_t6C320EB6D6F4FBBA052FEAC5771F9985BF1E314D * ___U3CU3E4__this_3;
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.SubContainerDependencyProvider_<GetAllInstancesWithInjectSplit>d__6::args
	List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * ___args_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__6_t583E658E9E0C6B896656C9D6FFA54AA575D22D3F, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__6_t583E658E9E0C6B896656C9D6FFA54AA575D22D3F, ___U3CU3E2__current_1)); }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_context_2() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__6_t583E658E9E0C6B896656C9D6FFA54AA575D22D3F, ___context_2)); }
	inline InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * get_context_2() const { return ___context_2; }
	inline InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF ** get_address_of_context_2() { return &___context_2; }
	inline void set_context_2(InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * value)
	{
		___context_2 = value;
		Il2CppCodeGenWriteBarrier((&___context_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__6_t583E658E9E0C6B896656C9D6FFA54AA575D22D3F, ___U3CU3E4__this_3)); }
	inline SubContainerDependencyProvider_t6C320EB6D6F4FBBA052FEAC5771F9985BF1E314D * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline SubContainerDependencyProvider_t6C320EB6D6F4FBBA052FEAC5771F9985BF1E314D ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(SubContainerDependencyProvider_t6C320EB6D6F4FBBA052FEAC5771F9985BF1E314D * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_args_4() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__6_t583E658E9E0C6B896656C9D6FFA54AA575D22D3F, ___args_4)); }
	inline List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * get_args_4() const { return ___args_4; }
	inline List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 ** get_address_of_args_4() { return &___args_4; }
	inline void set_args_4(List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * value)
	{
		___args_4 = value;
		Il2CppCodeGenWriteBarrier((&___args_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETALLINSTANCESWITHINJECTSPLITU3ED__6_T583E658E9E0C6B896656C9D6FFA54AA575D22D3F_H
#ifndef SUBCONTAINERSINGLETONPROVIDERCREATORBYINSTALLER_T0612A1F030297B7EEDB675CC61E90C60AAED986E_H
#define SUBCONTAINERSINGLETONPROVIDERCREATORBYINSTALLER_T0612A1F030297B7EEDB675CC61E90C60AAED986E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerSingletonProviderCreatorByInstaller
struct  SubContainerSingletonProviderCreatorByInstaller_t0612A1F030297B7EEDB675CC61E90C60AAED986E  : public RuntimeObject
{
public:
	// Zenject.SingletonMarkRegistry Zenject.SubContainerSingletonProviderCreatorByInstaller::_markRegistry
	SingletonMarkRegistry_tB0ADDC9314F22EFEF3C51F08F71E6E1CB236EC7F * ____markRegistry_0;
	// Zenject.DiContainer Zenject.SubContainerSingletonProviderCreatorByInstaller::_container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____container_1;
	// System.Collections.Generic.Dictionary`2<Zenject.SubContainerSingletonProviderCreatorByInstaller_InstallerSingletonId,Zenject.ISubContainerCreator> Zenject.SubContainerSingletonProviderCreatorByInstaller::_subContainerCreators
	Dictionary_2_t860FD753E81C06B1E2604A10CB9891602288AD03 * ____subContainerCreators_2;

public:
	inline static int32_t get_offset_of__markRegistry_0() { return static_cast<int32_t>(offsetof(SubContainerSingletonProviderCreatorByInstaller_t0612A1F030297B7EEDB675CC61E90C60AAED986E, ____markRegistry_0)); }
	inline SingletonMarkRegistry_tB0ADDC9314F22EFEF3C51F08F71E6E1CB236EC7F * get__markRegistry_0() const { return ____markRegistry_0; }
	inline SingletonMarkRegistry_tB0ADDC9314F22EFEF3C51F08F71E6E1CB236EC7F ** get_address_of__markRegistry_0() { return &____markRegistry_0; }
	inline void set__markRegistry_0(SingletonMarkRegistry_tB0ADDC9314F22EFEF3C51F08F71E6E1CB236EC7F * value)
	{
		____markRegistry_0 = value;
		Il2CppCodeGenWriteBarrier((&____markRegistry_0), value);
	}

	inline static int32_t get_offset_of__container_1() { return static_cast<int32_t>(offsetof(SubContainerSingletonProviderCreatorByInstaller_t0612A1F030297B7EEDB675CC61E90C60AAED986E, ____container_1)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__container_1() const { return ____container_1; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__container_1() { return &____container_1; }
	inline void set__container_1(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____container_1 = value;
		Il2CppCodeGenWriteBarrier((&____container_1), value);
	}

	inline static int32_t get_offset_of__subContainerCreators_2() { return static_cast<int32_t>(offsetof(SubContainerSingletonProviderCreatorByInstaller_t0612A1F030297B7EEDB675CC61E90C60AAED986E, ____subContainerCreators_2)); }
	inline Dictionary_2_t860FD753E81C06B1E2604A10CB9891602288AD03 * get__subContainerCreators_2() const { return ____subContainerCreators_2; }
	inline Dictionary_2_t860FD753E81C06B1E2604A10CB9891602288AD03 ** get_address_of__subContainerCreators_2() { return &____subContainerCreators_2; }
	inline void set__subContainerCreators_2(Dictionary_2_t860FD753E81C06B1E2604A10CB9891602288AD03 * value)
	{
		____subContainerCreators_2 = value;
		Il2CppCodeGenWriteBarrier((&____subContainerCreators_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBCONTAINERSINGLETONPROVIDERCREATORBYINSTALLER_T0612A1F030297B7EEDB675CC61E90C60AAED986E_H
#ifndef INSTALLERSINGLETONID_TE9D05E360C566D1720455F90A0DA3356B7552C96_H
#define INSTALLERSINGLETONID_TE9D05E360C566D1720455F90A0DA3356B7552C96_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerSingletonProviderCreatorByInstaller_InstallerSingletonId
struct  InstallerSingletonId_tE9D05E360C566D1720455F90A0DA3356B7552C96  : public RuntimeObject
{
public:
	// System.Object Zenject.SubContainerSingletonProviderCreatorByInstaller_InstallerSingletonId::ConcreteIdentifier
	RuntimeObject * ___ConcreteIdentifier_0;
	// System.Type Zenject.SubContainerSingletonProviderCreatorByInstaller_InstallerSingletonId::InstallerType
	Type_t * ___InstallerType_1;

public:
	inline static int32_t get_offset_of_ConcreteIdentifier_0() { return static_cast<int32_t>(offsetof(InstallerSingletonId_tE9D05E360C566D1720455F90A0DA3356B7552C96, ___ConcreteIdentifier_0)); }
	inline RuntimeObject * get_ConcreteIdentifier_0() const { return ___ConcreteIdentifier_0; }
	inline RuntimeObject ** get_address_of_ConcreteIdentifier_0() { return &___ConcreteIdentifier_0; }
	inline void set_ConcreteIdentifier_0(RuntimeObject * value)
	{
		___ConcreteIdentifier_0 = value;
		Il2CppCodeGenWriteBarrier((&___ConcreteIdentifier_0), value);
	}

	inline static int32_t get_offset_of_InstallerType_1() { return static_cast<int32_t>(offsetof(InstallerSingletonId_tE9D05E360C566D1720455F90A0DA3356B7552C96, ___InstallerType_1)); }
	inline Type_t * get_InstallerType_1() const { return ___InstallerType_1; }
	inline Type_t ** get_address_of_InstallerType_1() { return &___InstallerType_1; }
	inline void set_InstallerType_1(Type_t * value)
	{
		___InstallerType_1 = value;
		Il2CppCodeGenWriteBarrier((&___InstallerType_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTALLERSINGLETONID_TE9D05E360C566D1720455F90A0DA3356B7552C96_H
#ifndef SUBCONTAINERSINGLETONPROVIDERCREATORBYMETHOD_T875112116B40E0B708D4EE4601E758BACF9F1DED_H
#define SUBCONTAINERSINGLETONPROVIDERCREATORBYMETHOD_T875112116B40E0B708D4EE4601E758BACF9F1DED_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerSingletonProviderCreatorByMethod
struct  SubContainerSingletonProviderCreatorByMethod_t875112116B40E0B708D4EE4601E758BACF9F1DED  : public RuntimeObject
{
public:
	// Zenject.SingletonMarkRegistry Zenject.SubContainerSingletonProviderCreatorByMethod::_markRegistry
	SingletonMarkRegistry_tB0ADDC9314F22EFEF3C51F08F71E6E1CB236EC7F * ____markRegistry_0;
	// Zenject.DiContainer Zenject.SubContainerSingletonProviderCreatorByMethod::_container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____container_1;
	// System.Collections.Generic.Dictionary`2<Zenject.SubContainerSingletonProviderCreatorByMethod_MethodSingletonId,Zenject.ISubContainerCreator> Zenject.SubContainerSingletonProviderCreatorByMethod::_subContainerCreators
	Dictionary_2_t386DE62E5812D8AA2D41D0DEF4FC9E0B522953F0 * ____subContainerCreators_2;

public:
	inline static int32_t get_offset_of__markRegistry_0() { return static_cast<int32_t>(offsetof(SubContainerSingletonProviderCreatorByMethod_t875112116B40E0B708D4EE4601E758BACF9F1DED, ____markRegistry_0)); }
	inline SingletonMarkRegistry_tB0ADDC9314F22EFEF3C51F08F71E6E1CB236EC7F * get__markRegistry_0() const { return ____markRegistry_0; }
	inline SingletonMarkRegistry_tB0ADDC9314F22EFEF3C51F08F71E6E1CB236EC7F ** get_address_of__markRegistry_0() { return &____markRegistry_0; }
	inline void set__markRegistry_0(SingletonMarkRegistry_tB0ADDC9314F22EFEF3C51F08F71E6E1CB236EC7F * value)
	{
		____markRegistry_0 = value;
		Il2CppCodeGenWriteBarrier((&____markRegistry_0), value);
	}

	inline static int32_t get_offset_of__container_1() { return static_cast<int32_t>(offsetof(SubContainerSingletonProviderCreatorByMethod_t875112116B40E0B708D4EE4601E758BACF9F1DED, ____container_1)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__container_1() const { return ____container_1; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__container_1() { return &____container_1; }
	inline void set__container_1(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____container_1 = value;
		Il2CppCodeGenWriteBarrier((&____container_1), value);
	}

	inline static int32_t get_offset_of__subContainerCreators_2() { return static_cast<int32_t>(offsetof(SubContainerSingletonProviderCreatorByMethod_t875112116B40E0B708D4EE4601E758BACF9F1DED, ____subContainerCreators_2)); }
	inline Dictionary_2_t386DE62E5812D8AA2D41D0DEF4FC9E0B522953F0 * get__subContainerCreators_2() const { return ____subContainerCreators_2; }
	inline Dictionary_2_t386DE62E5812D8AA2D41D0DEF4FC9E0B522953F0 ** get_address_of__subContainerCreators_2() { return &____subContainerCreators_2; }
	inline void set__subContainerCreators_2(Dictionary_2_t386DE62E5812D8AA2D41D0DEF4FC9E0B522953F0 * value)
	{
		____subContainerCreators_2 = value;
		Il2CppCodeGenWriteBarrier((&____subContainerCreators_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBCONTAINERSINGLETONPROVIDERCREATORBYMETHOD_T875112116B40E0B708D4EE4601E758BACF9F1DED_H
#ifndef METHODSINGLETONID_T621D33781519595AAAD21649AC048D9BE29E7648_H
#define METHODSINGLETONID_T621D33781519595AAAD21649AC048D9BE29E7648_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerSingletonProviderCreatorByMethod_MethodSingletonId
struct  MethodSingletonId_t621D33781519595AAAD21649AC048D9BE29E7648  : public RuntimeObject
{
public:
	// System.Object Zenject.SubContainerSingletonProviderCreatorByMethod_MethodSingletonId::ConcreteIdentifier
	RuntimeObject * ___ConcreteIdentifier_0;
	// System.Delegate Zenject.SubContainerSingletonProviderCreatorByMethod_MethodSingletonId::InstallerDelegate
	Delegate_t * ___InstallerDelegate_1;

public:
	inline static int32_t get_offset_of_ConcreteIdentifier_0() { return static_cast<int32_t>(offsetof(MethodSingletonId_t621D33781519595AAAD21649AC048D9BE29E7648, ___ConcreteIdentifier_0)); }
	inline RuntimeObject * get_ConcreteIdentifier_0() const { return ___ConcreteIdentifier_0; }
	inline RuntimeObject ** get_address_of_ConcreteIdentifier_0() { return &___ConcreteIdentifier_0; }
	inline void set_ConcreteIdentifier_0(RuntimeObject * value)
	{
		___ConcreteIdentifier_0 = value;
		Il2CppCodeGenWriteBarrier((&___ConcreteIdentifier_0), value);
	}

	inline static int32_t get_offset_of_InstallerDelegate_1() { return static_cast<int32_t>(offsetof(MethodSingletonId_t621D33781519595AAAD21649AC048D9BE29E7648, ___InstallerDelegate_1)); }
	inline Delegate_t * get_InstallerDelegate_1() const { return ___InstallerDelegate_1; }
	inline Delegate_t ** get_address_of_InstallerDelegate_1() { return &___InstallerDelegate_1; }
	inline void set_InstallerDelegate_1(Delegate_t * value)
	{
		___InstallerDelegate_1 = value;
		Il2CppCodeGenWriteBarrier((&___InstallerDelegate_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METHODSINGLETONID_T621D33781519595AAAD21649AC048D9BE29E7648_H
#ifndef SUBCONTAINERSINGLETONPROVIDERCREATORBYNEWPREFAB_TD1BD0BC2BC3FB62EE8DDD51AE521B287C673A19B_H
#define SUBCONTAINERSINGLETONPROVIDERCREATORBYNEWPREFAB_TD1BD0BC2BC3FB62EE8DDD51AE521B287C673A19B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerSingletonProviderCreatorByNewPrefab
struct  SubContainerSingletonProviderCreatorByNewPrefab_tD1BD0BC2BC3FB62EE8DDD51AE521B287C673A19B  : public RuntimeObject
{
public:
	// Zenject.SingletonMarkRegistry Zenject.SubContainerSingletonProviderCreatorByNewPrefab::_markRegistry
	SingletonMarkRegistry_tB0ADDC9314F22EFEF3C51F08F71E6E1CB236EC7F * ____markRegistry_0;
	// Zenject.DiContainer Zenject.SubContainerSingletonProviderCreatorByNewPrefab::_container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____container_1;
	// System.Collections.Generic.Dictionary`2<Zenject.SubContainerSingletonProviderCreatorByNewPrefab_CustomSingletonId,Zenject.SubContainerSingletonProviderCreatorByNewPrefab_CreatorInfo> Zenject.SubContainerSingletonProviderCreatorByNewPrefab::_subContainerCreators
	Dictionary_2_t2F7E5CA7D4924BAD997E7E2A40BAA2361AD2E515 * ____subContainerCreators_2;

public:
	inline static int32_t get_offset_of__markRegistry_0() { return static_cast<int32_t>(offsetof(SubContainerSingletonProviderCreatorByNewPrefab_tD1BD0BC2BC3FB62EE8DDD51AE521B287C673A19B, ____markRegistry_0)); }
	inline SingletonMarkRegistry_tB0ADDC9314F22EFEF3C51F08F71E6E1CB236EC7F * get__markRegistry_0() const { return ____markRegistry_0; }
	inline SingletonMarkRegistry_tB0ADDC9314F22EFEF3C51F08F71E6E1CB236EC7F ** get_address_of__markRegistry_0() { return &____markRegistry_0; }
	inline void set__markRegistry_0(SingletonMarkRegistry_tB0ADDC9314F22EFEF3C51F08F71E6E1CB236EC7F * value)
	{
		____markRegistry_0 = value;
		Il2CppCodeGenWriteBarrier((&____markRegistry_0), value);
	}

	inline static int32_t get_offset_of__container_1() { return static_cast<int32_t>(offsetof(SubContainerSingletonProviderCreatorByNewPrefab_tD1BD0BC2BC3FB62EE8DDD51AE521B287C673A19B, ____container_1)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__container_1() const { return ____container_1; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__container_1() { return &____container_1; }
	inline void set__container_1(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____container_1 = value;
		Il2CppCodeGenWriteBarrier((&____container_1), value);
	}

	inline static int32_t get_offset_of__subContainerCreators_2() { return static_cast<int32_t>(offsetof(SubContainerSingletonProviderCreatorByNewPrefab_tD1BD0BC2BC3FB62EE8DDD51AE521B287C673A19B, ____subContainerCreators_2)); }
	inline Dictionary_2_t2F7E5CA7D4924BAD997E7E2A40BAA2361AD2E515 * get__subContainerCreators_2() const { return ____subContainerCreators_2; }
	inline Dictionary_2_t2F7E5CA7D4924BAD997E7E2A40BAA2361AD2E515 ** get_address_of__subContainerCreators_2() { return &____subContainerCreators_2; }
	inline void set__subContainerCreators_2(Dictionary_2_t2F7E5CA7D4924BAD997E7E2A40BAA2361AD2E515 * value)
	{
		____subContainerCreators_2 = value;
		Il2CppCodeGenWriteBarrier((&____subContainerCreators_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBCONTAINERSINGLETONPROVIDERCREATORBYNEWPREFAB_TD1BD0BC2BC3FB62EE8DDD51AE521B287C673A19B_H
#ifndef CREATORINFO_TC5365DBAF4D185C993E78A2E646FE796EF865386_H
#define CREATORINFO_TC5365DBAF4D185C993E78A2E646FE796EF865386_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerSingletonProviderCreatorByNewPrefab_CreatorInfo
struct  CreatorInfo_tC5365DBAF4D185C993E78A2E646FE796EF865386  : public RuntimeObject
{
public:
	// Zenject.GameObjectCreationParameters Zenject.SubContainerSingletonProviderCreatorByNewPrefab_CreatorInfo::<GameObjectCreationParameters>k__BackingField
	GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A * ___U3CGameObjectCreationParametersU3Ek__BackingField_0;
	// Zenject.ISubContainerCreator Zenject.SubContainerSingletonProviderCreatorByNewPrefab_CreatorInfo::<Creator>k__BackingField
	RuntimeObject* ___U3CCreatorU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CGameObjectCreationParametersU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CreatorInfo_tC5365DBAF4D185C993E78A2E646FE796EF865386, ___U3CGameObjectCreationParametersU3Ek__BackingField_0)); }
	inline GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A * get_U3CGameObjectCreationParametersU3Ek__BackingField_0() const { return ___U3CGameObjectCreationParametersU3Ek__BackingField_0; }
	inline GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A ** get_address_of_U3CGameObjectCreationParametersU3Ek__BackingField_0() { return &___U3CGameObjectCreationParametersU3Ek__BackingField_0; }
	inline void set_U3CGameObjectCreationParametersU3Ek__BackingField_0(GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A * value)
	{
		___U3CGameObjectCreationParametersU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGameObjectCreationParametersU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CCreatorU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CreatorInfo_tC5365DBAF4D185C993E78A2E646FE796EF865386, ___U3CCreatorU3Ek__BackingField_1)); }
	inline RuntimeObject* get_U3CCreatorU3Ek__BackingField_1() const { return ___U3CCreatorU3Ek__BackingField_1; }
	inline RuntimeObject** get_address_of_U3CCreatorU3Ek__BackingField_1() { return &___U3CCreatorU3Ek__BackingField_1; }
	inline void set_U3CCreatorU3Ek__BackingField_1(RuntimeObject* value)
	{
		___U3CCreatorU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCreatorU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CREATORINFO_TC5365DBAF4D185C993E78A2E646FE796EF865386_H
#ifndef CUSTOMSINGLETONID_TF78EF913E97A6C14F827A97D7CD42641E421AB85_H
#define CUSTOMSINGLETONID_TF78EF913E97A6C14F827A97D7CD42641E421AB85_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerSingletonProviderCreatorByNewPrefab_CustomSingletonId
struct  CustomSingletonId_tF78EF913E97A6C14F827A97D7CD42641E421AB85  : public RuntimeObject
{
public:
	// System.Object Zenject.SubContainerSingletonProviderCreatorByNewPrefab_CustomSingletonId::ConcreteIdentifier
	RuntimeObject * ___ConcreteIdentifier_0;
	// UnityEngine.Object Zenject.SubContainerSingletonProviderCreatorByNewPrefab_CustomSingletonId::Prefab
	Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___Prefab_1;

public:
	inline static int32_t get_offset_of_ConcreteIdentifier_0() { return static_cast<int32_t>(offsetof(CustomSingletonId_tF78EF913E97A6C14F827A97D7CD42641E421AB85, ___ConcreteIdentifier_0)); }
	inline RuntimeObject * get_ConcreteIdentifier_0() const { return ___ConcreteIdentifier_0; }
	inline RuntimeObject ** get_address_of_ConcreteIdentifier_0() { return &___ConcreteIdentifier_0; }
	inline void set_ConcreteIdentifier_0(RuntimeObject * value)
	{
		___ConcreteIdentifier_0 = value;
		Il2CppCodeGenWriteBarrier((&___ConcreteIdentifier_0), value);
	}

	inline static int32_t get_offset_of_Prefab_1() { return static_cast<int32_t>(offsetof(CustomSingletonId_tF78EF913E97A6C14F827A97D7CD42641E421AB85, ___Prefab_1)); }
	inline Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * get_Prefab_1() const { return ___Prefab_1; }
	inline Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 ** get_address_of_Prefab_1() { return &___Prefab_1; }
	inline void set_Prefab_1(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * value)
	{
		___Prefab_1 = value;
		Il2CppCodeGenWriteBarrier((&___Prefab_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMSINGLETONID_TF78EF913E97A6C14F827A97D7CD42641E421AB85_H
#ifndef SUBCONTAINERSINGLETONPROVIDERCREATORBYNEWPREFABRESOURCE_T0F56AB2C3837D0E2117B52265FCBD5E21A8295AB_H
#define SUBCONTAINERSINGLETONPROVIDERCREATORBYNEWPREFABRESOURCE_T0F56AB2C3837D0E2117B52265FCBD5E21A8295AB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerSingletonProviderCreatorByNewPrefabResource
struct  SubContainerSingletonProviderCreatorByNewPrefabResource_t0F56AB2C3837D0E2117B52265FCBD5E21A8295AB  : public RuntimeObject
{
public:
	// Zenject.SingletonMarkRegistry Zenject.SubContainerSingletonProviderCreatorByNewPrefabResource::_markRegistry
	SingletonMarkRegistry_tB0ADDC9314F22EFEF3C51F08F71E6E1CB236EC7F * ____markRegistry_0;
	// Zenject.DiContainer Zenject.SubContainerSingletonProviderCreatorByNewPrefabResource::_container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____container_1;
	// System.Collections.Generic.Dictionary`2<Zenject.SubContainerSingletonProviderCreatorByNewPrefabResource_CustomSingletonId,Zenject.SubContainerSingletonProviderCreatorByNewPrefabResource_CreatorInfo> Zenject.SubContainerSingletonProviderCreatorByNewPrefabResource::_subContainerCreators
	Dictionary_2_t36D1EF1D06922EA2951BDF1B3A6E0B8818A2063D * ____subContainerCreators_2;

public:
	inline static int32_t get_offset_of__markRegistry_0() { return static_cast<int32_t>(offsetof(SubContainerSingletonProviderCreatorByNewPrefabResource_t0F56AB2C3837D0E2117B52265FCBD5E21A8295AB, ____markRegistry_0)); }
	inline SingletonMarkRegistry_tB0ADDC9314F22EFEF3C51F08F71E6E1CB236EC7F * get__markRegistry_0() const { return ____markRegistry_0; }
	inline SingletonMarkRegistry_tB0ADDC9314F22EFEF3C51F08F71E6E1CB236EC7F ** get_address_of__markRegistry_0() { return &____markRegistry_0; }
	inline void set__markRegistry_0(SingletonMarkRegistry_tB0ADDC9314F22EFEF3C51F08F71E6E1CB236EC7F * value)
	{
		____markRegistry_0 = value;
		Il2CppCodeGenWriteBarrier((&____markRegistry_0), value);
	}

	inline static int32_t get_offset_of__container_1() { return static_cast<int32_t>(offsetof(SubContainerSingletonProviderCreatorByNewPrefabResource_t0F56AB2C3837D0E2117B52265FCBD5E21A8295AB, ____container_1)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__container_1() const { return ____container_1; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__container_1() { return &____container_1; }
	inline void set__container_1(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____container_1 = value;
		Il2CppCodeGenWriteBarrier((&____container_1), value);
	}

	inline static int32_t get_offset_of__subContainerCreators_2() { return static_cast<int32_t>(offsetof(SubContainerSingletonProviderCreatorByNewPrefabResource_t0F56AB2C3837D0E2117B52265FCBD5E21A8295AB, ____subContainerCreators_2)); }
	inline Dictionary_2_t36D1EF1D06922EA2951BDF1B3A6E0B8818A2063D * get__subContainerCreators_2() const { return ____subContainerCreators_2; }
	inline Dictionary_2_t36D1EF1D06922EA2951BDF1B3A6E0B8818A2063D ** get_address_of__subContainerCreators_2() { return &____subContainerCreators_2; }
	inline void set__subContainerCreators_2(Dictionary_2_t36D1EF1D06922EA2951BDF1B3A6E0B8818A2063D * value)
	{
		____subContainerCreators_2 = value;
		Il2CppCodeGenWriteBarrier((&____subContainerCreators_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBCONTAINERSINGLETONPROVIDERCREATORBYNEWPREFABRESOURCE_T0F56AB2C3837D0E2117B52265FCBD5E21A8295AB_H
#ifndef CREATORINFO_TEFC9008F54602EF6D4D1694BEFFAAB67FC409674_H
#define CREATORINFO_TEFC9008F54602EF6D4D1694BEFFAAB67FC409674_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerSingletonProviderCreatorByNewPrefabResource_CreatorInfo
struct  CreatorInfo_tEFC9008F54602EF6D4D1694BEFFAAB67FC409674  : public RuntimeObject
{
public:
	// Zenject.GameObjectCreationParameters Zenject.SubContainerSingletonProviderCreatorByNewPrefabResource_CreatorInfo::<GameObjectCreationParameters>k__BackingField
	GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A * ___U3CGameObjectCreationParametersU3Ek__BackingField_0;
	// Zenject.ISubContainerCreator Zenject.SubContainerSingletonProviderCreatorByNewPrefabResource_CreatorInfo::<Creator>k__BackingField
	RuntimeObject* ___U3CCreatorU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CGameObjectCreationParametersU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CreatorInfo_tEFC9008F54602EF6D4D1694BEFFAAB67FC409674, ___U3CGameObjectCreationParametersU3Ek__BackingField_0)); }
	inline GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A * get_U3CGameObjectCreationParametersU3Ek__BackingField_0() const { return ___U3CGameObjectCreationParametersU3Ek__BackingField_0; }
	inline GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A ** get_address_of_U3CGameObjectCreationParametersU3Ek__BackingField_0() { return &___U3CGameObjectCreationParametersU3Ek__BackingField_0; }
	inline void set_U3CGameObjectCreationParametersU3Ek__BackingField_0(GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A * value)
	{
		___U3CGameObjectCreationParametersU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGameObjectCreationParametersU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CCreatorU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CreatorInfo_tEFC9008F54602EF6D4D1694BEFFAAB67FC409674, ___U3CCreatorU3Ek__BackingField_1)); }
	inline RuntimeObject* get_U3CCreatorU3Ek__BackingField_1() const { return ___U3CCreatorU3Ek__BackingField_1; }
	inline RuntimeObject** get_address_of_U3CCreatorU3Ek__BackingField_1() { return &___U3CCreatorU3Ek__BackingField_1; }
	inline void set_U3CCreatorU3Ek__BackingField_1(RuntimeObject* value)
	{
		___U3CCreatorU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCreatorU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CREATORINFO_TEFC9008F54602EF6D4D1694BEFFAAB67FC409674_H
#ifndef CUSTOMSINGLETONID_T96A0A45F602A050226A478E74A2EDFE0E7C834DA_H
#define CUSTOMSINGLETONID_T96A0A45F602A050226A478E74A2EDFE0E7C834DA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerSingletonProviderCreatorByNewPrefabResource_CustomSingletonId
struct  CustomSingletonId_t96A0A45F602A050226A478E74A2EDFE0E7C834DA  : public RuntimeObject
{
public:
	// System.Object Zenject.SubContainerSingletonProviderCreatorByNewPrefabResource_CustomSingletonId::ConcreteIdentifier
	RuntimeObject * ___ConcreteIdentifier_0;
	// System.String Zenject.SubContainerSingletonProviderCreatorByNewPrefabResource_CustomSingletonId::ResourcePath
	String_t* ___ResourcePath_1;

public:
	inline static int32_t get_offset_of_ConcreteIdentifier_0() { return static_cast<int32_t>(offsetof(CustomSingletonId_t96A0A45F602A050226A478E74A2EDFE0E7C834DA, ___ConcreteIdentifier_0)); }
	inline RuntimeObject * get_ConcreteIdentifier_0() const { return ___ConcreteIdentifier_0; }
	inline RuntimeObject ** get_address_of_ConcreteIdentifier_0() { return &___ConcreteIdentifier_0; }
	inline void set_ConcreteIdentifier_0(RuntimeObject * value)
	{
		___ConcreteIdentifier_0 = value;
		Il2CppCodeGenWriteBarrier((&___ConcreteIdentifier_0), value);
	}

	inline static int32_t get_offset_of_ResourcePath_1() { return static_cast<int32_t>(offsetof(CustomSingletonId_t96A0A45F602A050226A478E74A2EDFE0E7C834DA, ___ResourcePath_1)); }
	inline String_t* get_ResourcePath_1() const { return ___ResourcePath_1; }
	inline String_t** get_address_of_ResourcePath_1() { return &___ResourcePath_1; }
	inline void set_ResourcePath_1(String_t* value)
	{
		___ResourcePath_1 = value;
		Il2CppCodeGenWriteBarrier((&___ResourcePath_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMSINGLETONID_T96A0A45F602A050226A478E74A2EDFE0E7C834DA_H
#ifndef TRANSIENTPROVIDER_TAB891005B80A7655BC6103608F2471B9CF7EDA43_H
#define TRANSIENTPROVIDER_TAB891005B80A7655BC6103608F2471B9CF7EDA43_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.TransientProvider
struct  TransientProvider_tAB891005B80A7655BC6103608F2471B9CF7EDA43  : public RuntimeObject
{
public:
	// Zenject.DiContainer Zenject.TransientProvider::_container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____container_0;
	// System.Type Zenject.TransientProvider::_concreteType
	Type_t * ____concreteType_1;
	// System.Object Zenject.TransientProvider::_concreteIdentifier
	RuntimeObject * ____concreteIdentifier_2;
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.TransientProvider::_extraArguments
	List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * ____extraArguments_3;

public:
	inline static int32_t get_offset_of__container_0() { return static_cast<int32_t>(offsetof(TransientProvider_tAB891005B80A7655BC6103608F2471B9CF7EDA43, ____container_0)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__container_0() const { return ____container_0; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__container_0() { return &____container_0; }
	inline void set__container_0(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____container_0 = value;
		Il2CppCodeGenWriteBarrier((&____container_0), value);
	}

	inline static int32_t get_offset_of__concreteType_1() { return static_cast<int32_t>(offsetof(TransientProvider_tAB891005B80A7655BC6103608F2471B9CF7EDA43, ____concreteType_1)); }
	inline Type_t * get__concreteType_1() const { return ____concreteType_1; }
	inline Type_t ** get_address_of__concreteType_1() { return &____concreteType_1; }
	inline void set__concreteType_1(Type_t * value)
	{
		____concreteType_1 = value;
		Il2CppCodeGenWriteBarrier((&____concreteType_1), value);
	}

	inline static int32_t get_offset_of__concreteIdentifier_2() { return static_cast<int32_t>(offsetof(TransientProvider_tAB891005B80A7655BC6103608F2471B9CF7EDA43, ____concreteIdentifier_2)); }
	inline RuntimeObject * get__concreteIdentifier_2() const { return ____concreteIdentifier_2; }
	inline RuntimeObject ** get_address_of__concreteIdentifier_2() { return &____concreteIdentifier_2; }
	inline void set__concreteIdentifier_2(RuntimeObject * value)
	{
		____concreteIdentifier_2 = value;
		Il2CppCodeGenWriteBarrier((&____concreteIdentifier_2), value);
	}

	inline static int32_t get_offset_of__extraArguments_3() { return static_cast<int32_t>(offsetof(TransientProvider_tAB891005B80A7655BC6103608F2471B9CF7EDA43, ____extraArguments_3)); }
	inline List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * get__extraArguments_3() const { return ____extraArguments_3; }
	inline List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 ** get_address_of__extraArguments_3() { return &____extraArguments_3; }
	inline void set__extraArguments_3(List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * value)
	{
		____extraArguments_3 = value;
		Il2CppCodeGenWriteBarrier((&____extraArguments_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSIENTPROVIDER_TAB891005B80A7655BC6103608F2471B9CF7EDA43_H
#ifndef U3CGETALLINSTANCESWITHINJECTSPLITU3ED__8_TCD537E23ED94D135E58FB2796D0A50E9F8A31F91_H
#define U3CGETALLINSTANCESWITHINJECTSPLITU3ED__8_TCD537E23ED94D135E58FB2796D0A50E9F8A31F91_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.TransientProvider_<GetAllInstancesWithInjectSplit>d__8
struct  U3CGetAllInstancesWithInjectSplitU3Ed__8_tCD537E23ED94D135E58FB2796D0A50E9F8A31F91  : public RuntimeObject
{
public:
	// System.Int32 Zenject.TransientProvider_<GetAllInstancesWithInjectSplit>d__8::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Collections.Generic.List`1<System.Object> Zenject.TransientProvider_<GetAllInstancesWithInjectSplit>d__8::<>2__current
	List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * ___U3CU3E2__current_1;
	// Zenject.InjectContext Zenject.TransientProvider_<GetAllInstancesWithInjectSplit>d__8::context
	InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * ___context_2;
	// Zenject.TransientProvider Zenject.TransientProvider_<GetAllInstancesWithInjectSplit>d__8::<>4__this
	TransientProvider_tAB891005B80A7655BC6103608F2471B9CF7EDA43 * ___U3CU3E4__this_3;
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.TransientProvider_<GetAllInstancesWithInjectSplit>d__8::args
	List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * ___args_4;
	// System.Type Zenject.TransientProvider_<GetAllInstancesWithInjectSplit>d__8::<instanceType>5__2
	Type_t * ___U3CinstanceTypeU3E5__2_5;
	// Zenject.InjectArgs Zenject.TransientProvider_<GetAllInstancesWithInjectSplit>d__8::<injectArgs>5__3
	InjectArgs_tD1EC7FDE37E60A7D38EF224E5FE6C8667639387C * ___U3CinjectArgsU3E5__3_6;
	// System.Object Zenject.TransientProvider_<GetAllInstancesWithInjectSplit>d__8::<instance>5__4
	RuntimeObject * ___U3CinstanceU3E5__4_7;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__8_tCD537E23ED94D135E58FB2796D0A50E9F8A31F91, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__8_tCD537E23ED94D135E58FB2796D0A50E9F8A31F91, ___U3CU3E2__current_1)); }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_context_2() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__8_tCD537E23ED94D135E58FB2796D0A50E9F8A31F91, ___context_2)); }
	inline InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * get_context_2() const { return ___context_2; }
	inline InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF ** get_address_of_context_2() { return &___context_2; }
	inline void set_context_2(InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * value)
	{
		___context_2 = value;
		Il2CppCodeGenWriteBarrier((&___context_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__8_tCD537E23ED94D135E58FB2796D0A50E9F8A31F91, ___U3CU3E4__this_3)); }
	inline TransientProvider_tAB891005B80A7655BC6103608F2471B9CF7EDA43 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline TransientProvider_tAB891005B80A7655BC6103608F2471B9CF7EDA43 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(TransientProvider_tAB891005B80A7655BC6103608F2471B9CF7EDA43 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_args_4() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__8_tCD537E23ED94D135E58FB2796D0A50E9F8A31F91, ___args_4)); }
	inline List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * get_args_4() const { return ___args_4; }
	inline List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 ** get_address_of_args_4() { return &___args_4; }
	inline void set_args_4(List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * value)
	{
		___args_4 = value;
		Il2CppCodeGenWriteBarrier((&___args_4), value);
	}

	inline static int32_t get_offset_of_U3CinstanceTypeU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__8_tCD537E23ED94D135E58FB2796D0A50E9F8A31F91, ___U3CinstanceTypeU3E5__2_5)); }
	inline Type_t * get_U3CinstanceTypeU3E5__2_5() const { return ___U3CinstanceTypeU3E5__2_5; }
	inline Type_t ** get_address_of_U3CinstanceTypeU3E5__2_5() { return &___U3CinstanceTypeU3E5__2_5; }
	inline void set_U3CinstanceTypeU3E5__2_5(Type_t * value)
	{
		___U3CinstanceTypeU3E5__2_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CinstanceTypeU3E5__2_5), value);
	}

	inline static int32_t get_offset_of_U3CinjectArgsU3E5__3_6() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__8_tCD537E23ED94D135E58FB2796D0A50E9F8A31F91, ___U3CinjectArgsU3E5__3_6)); }
	inline InjectArgs_tD1EC7FDE37E60A7D38EF224E5FE6C8667639387C * get_U3CinjectArgsU3E5__3_6() const { return ___U3CinjectArgsU3E5__3_6; }
	inline InjectArgs_tD1EC7FDE37E60A7D38EF224E5FE6C8667639387C ** get_address_of_U3CinjectArgsU3E5__3_6() { return &___U3CinjectArgsU3E5__3_6; }
	inline void set_U3CinjectArgsU3E5__3_6(InjectArgs_tD1EC7FDE37E60A7D38EF224E5FE6C8667639387C * value)
	{
		___U3CinjectArgsU3E5__3_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CinjectArgsU3E5__3_6), value);
	}

	inline static int32_t get_offset_of_U3CinstanceU3E5__4_7() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__8_tCD537E23ED94D135E58FB2796D0A50E9F8A31F91, ___U3CinstanceU3E5__4_7)); }
	inline RuntimeObject * get_U3CinstanceU3E5__4_7() const { return ___U3CinstanceU3E5__4_7; }
	inline RuntimeObject ** get_address_of_U3CinstanceU3E5__4_7() { return &___U3CinstanceU3E5__4_7; }
	inline void set_U3CinstanceU3E5__4_7(RuntimeObject * value)
	{
		___U3CinstanceU3E5__4_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CinstanceU3E5__4_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETALLINSTANCESWITHINJECTSPLITU3ED__8_TCD537E23ED94D135E58FB2796D0A50E9F8A31F91_H
#ifndef UNTYPEDFACTORYPROVIDER_TF14A4C3D101D65F49AA291EB4F166F6636C9363C_H
#define UNTYPEDFACTORYPROVIDER_TF14A4C3D101D65F49AA291EB4F166F6636C9363C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.UntypedFactoryProvider
struct  UntypedFactoryProvider_tF14A4C3D101D65F49AA291EB4F166F6636C9363C  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.UntypedFactoryProvider::_factoryArgs
	List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * ____factoryArgs_0;
	// Zenject.DiContainer Zenject.UntypedFactoryProvider::_container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____container_1;
	// System.Type Zenject.UntypedFactoryProvider::_factoryType
	Type_t * ____factoryType_2;
	// System.Type Zenject.UntypedFactoryProvider::_concreteType
	Type_t * ____concreteType_3;
	// System.Reflection.MethodInfo Zenject.UntypedFactoryProvider::_createMethod
	MethodInfo_t * ____createMethod_4;

public:
	inline static int32_t get_offset_of__factoryArgs_0() { return static_cast<int32_t>(offsetof(UntypedFactoryProvider_tF14A4C3D101D65F49AA291EB4F166F6636C9363C, ____factoryArgs_0)); }
	inline List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * get__factoryArgs_0() const { return ____factoryArgs_0; }
	inline List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 ** get_address_of__factoryArgs_0() { return &____factoryArgs_0; }
	inline void set__factoryArgs_0(List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * value)
	{
		____factoryArgs_0 = value;
		Il2CppCodeGenWriteBarrier((&____factoryArgs_0), value);
	}

	inline static int32_t get_offset_of__container_1() { return static_cast<int32_t>(offsetof(UntypedFactoryProvider_tF14A4C3D101D65F49AA291EB4F166F6636C9363C, ____container_1)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__container_1() const { return ____container_1; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__container_1() { return &____container_1; }
	inline void set__container_1(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____container_1 = value;
		Il2CppCodeGenWriteBarrier((&____container_1), value);
	}

	inline static int32_t get_offset_of__factoryType_2() { return static_cast<int32_t>(offsetof(UntypedFactoryProvider_tF14A4C3D101D65F49AA291EB4F166F6636C9363C, ____factoryType_2)); }
	inline Type_t * get__factoryType_2() const { return ____factoryType_2; }
	inline Type_t ** get_address_of__factoryType_2() { return &____factoryType_2; }
	inline void set__factoryType_2(Type_t * value)
	{
		____factoryType_2 = value;
		Il2CppCodeGenWriteBarrier((&____factoryType_2), value);
	}

	inline static int32_t get_offset_of__concreteType_3() { return static_cast<int32_t>(offsetof(UntypedFactoryProvider_tF14A4C3D101D65F49AA291EB4F166F6636C9363C, ____concreteType_3)); }
	inline Type_t * get__concreteType_3() const { return ____concreteType_3; }
	inline Type_t ** get_address_of__concreteType_3() { return &____concreteType_3; }
	inline void set__concreteType_3(Type_t * value)
	{
		____concreteType_3 = value;
		Il2CppCodeGenWriteBarrier((&____concreteType_3), value);
	}

	inline static int32_t get_offset_of__createMethod_4() { return static_cast<int32_t>(offsetof(UntypedFactoryProvider_tF14A4C3D101D65F49AA291EB4F166F6636C9363C, ____createMethod_4)); }
	inline MethodInfo_t * get__createMethod_4() const { return ____createMethod_4; }
	inline MethodInfo_t ** get_address_of__createMethod_4() { return &____createMethod_4; }
	inline void set__createMethod_4(MethodInfo_t * value)
	{
		____createMethod_4 = value;
		Il2CppCodeGenWriteBarrier((&____createMethod_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNTYPEDFACTORYPROVIDER_TF14A4C3D101D65F49AA291EB4F166F6636C9363C_H
#ifndef U3CU3EC_T51DAA85C4705F1A1D411DAA3F7C8DE03D398AC0A_H
#define U3CU3EC_T51DAA85C4705F1A1D411DAA3F7C8DE03D398AC0A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.UntypedFactoryProvider_<>c
struct  U3CU3Ec_t51DAA85C4705F1A1D411DAA3F7C8DE03D398AC0A  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t51DAA85C4705F1A1D411DAA3F7C8DE03D398AC0A_StaticFields
{
public:
	// Zenject.UntypedFactoryProvider_<>c Zenject.UntypedFactoryProvider_<>c::<>9
	U3CU3Ec_t51DAA85C4705F1A1D411DAA3F7C8DE03D398AC0A * ___U3CU3E9_0;
	// System.Func`2<System.Reflection.MethodInfo,System.Boolean> Zenject.UntypedFactoryProvider_<>c::<>9__5_0
	Func_2_t25BDD008BABA5EB0719A205177AD1FB36DC6A438 * ___U3CU3E9__5_0_1;
	// System.Func`2<System.Type,System.Boolean> Zenject.UntypedFactoryProvider_<>c::<>9__6_0
	Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 * ___U3CU3E9__6_0_2;
	// System.Func`2<Zenject.TypeValuePair,System.Object> Zenject.UntypedFactoryProvider_<>c::<>9__8_0
	Func_2_t1DCE2C3D6A169B92813A57259D83F0C5FEC5B5F1 * ___U3CU3E9__8_0_3;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t51DAA85C4705F1A1D411DAA3F7C8DE03D398AC0A_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t51DAA85C4705F1A1D411DAA3F7C8DE03D398AC0A * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t51DAA85C4705F1A1D411DAA3F7C8DE03D398AC0A ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t51DAA85C4705F1A1D411DAA3F7C8DE03D398AC0A * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__5_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t51DAA85C4705F1A1D411DAA3F7C8DE03D398AC0A_StaticFields, ___U3CU3E9__5_0_1)); }
	inline Func_2_t25BDD008BABA5EB0719A205177AD1FB36DC6A438 * get_U3CU3E9__5_0_1() const { return ___U3CU3E9__5_0_1; }
	inline Func_2_t25BDD008BABA5EB0719A205177AD1FB36DC6A438 ** get_address_of_U3CU3E9__5_0_1() { return &___U3CU3E9__5_0_1; }
	inline void set_U3CU3E9__5_0_1(Func_2_t25BDD008BABA5EB0719A205177AD1FB36DC6A438 * value)
	{
		___U3CU3E9__5_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__5_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__6_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t51DAA85C4705F1A1D411DAA3F7C8DE03D398AC0A_StaticFields, ___U3CU3E9__6_0_2)); }
	inline Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 * get_U3CU3E9__6_0_2() const { return ___U3CU3E9__6_0_2; }
	inline Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 ** get_address_of_U3CU3E9__6_0_2() { return &___U3CU3E9__6_0_2; }
	inline void set_U3CU3E9__6_0_2(Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 * value)
	{
		___U3CU3E9__6_0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__6_0_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__8_0_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t51DAA85C4705F1A1D411DAA3F7C8DE03D398AC0A_StaticFields, ___U3CU3E9__8_0_3)); }
	inline Func_2_t1DCE2C3D6A169B92813A57259D83F0C5FEC5B5F1 * get_U3CU3E9__8_0_3() const { return ___U3CU3E9__8_0_3; }
	inline Func_2_t1DCE2C3D6A169B92813A57259D83F0C5FEC5B5F1 ** get_address_of_U3CU3E9__8_0_3() { return &___U3CU3E9__8_0_3; }
	inline void set_U3CU3E9__8_0_3(Func_2_t1DCE2C3D6A169B92813A57259D83F0C5FEC5B5F1 * value)
	{
		___U3CU3E9__8_0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__8_0_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T51DAA85C4705F1A1D411DAA3F7C8DE03D398AC0A_H
#ifndef U3CGETALLINSTANCESWITHINJECTSPLITU3ED__8_TFCEF3B3C6C9DA17B3EBCC085F1C4E81DB82DEBE9_H
#define U3CGETALLINSTANCESWITHINJECTSPLITU3ED__8_TFCEF3B3C6C9DA17B3EBCC085F1C4E81DB82DEBE9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.UntypedFactoryProvider_<GetAllInstancesWithInjectSplit>d__8
struct  U3CGetAllInstancesWithInjectSplitU3Ed__8_tFCEF3B3C6C9DA17B3EBCC085F1C4E81DB82DEBE9  : public RuntimeObject
{
public:
	// System.Int32 Zenject.UntypedFactoryProvider_<GetAllInstancesWithInjectSplit>d__8::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Collections.Generic.List`1<System.Object> Zenject.UntypedFactoryProvider_<GetAllInstancesWithInjectSplit>d__8::<>2__current
	List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * ___U3CU3E2__current_1;
	// Zenject.UntypedFactoryProvider Zenject.UntypedFactoryProvider_<GetAllInstancesWithInjectSplit>d__8::<>4__this
	UntypedFactoryProvider_tF14A4C3D101D65F49AA291EB4F166F6636C9363C * ___U3CU3E4__this_2;
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.UntypedFactoryProvider_<GetAllInstancesWithInjectSplit>d__8::args
	List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * ___args_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__8_tFCEF3B3C6C9DA17B3EBCC085F1C4E81DB82DEBE9, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__8_tFCEF3B3C6C9DA17B3EBCC085F1C4E81DB82DEBE9, ___U3CU3E2__current_1)); }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__8_tFCEF3B3C6C9DA17B3EBCC085F1C4E81DB82DEBE9, ___U3CU3E4__this_2)); }
	inline UntypedFactoryProvider_tF14A4C3D101D65F49AA291EB4F166F6636C9363C * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline UntypedFactoryProvider_tF14A4C3D101D65F49AA291EB4F166F6636C9363C ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(UntypedFactoryProvider_tF14A4C3D101D65F49AA291EB4F166F6636C9363C * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_args_3() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__8_tFCEF3B3C6C9DA17B3EBCC085F1C4E81DB82DEBE9, ___args_3)); }
	inline List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * get_args_3() const { return ___args_3; }
	inline List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 ** get_address_of_args_3() { return &___args_3; }
	inline void set_args_3(List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * value)
	{
		___args_3 = value;
		Il2CppCodeGenWriteBarrier((&___args_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETALLINSTANCESWITHINJECTSPLITU3ED__8_TFCEF3B3C6C9DA17B3EBCC085F1C4E81DB82DEBE9_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef INSTALLER_2_TF6D845F3F63B23826F20664C750AAA79301B8DBB_H
#define INSTALLER_2_TF6D845F3F63B23826F20664C750AAA79301B8DBB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.Installer`2<UnityEngine.Animator,Zenject.AnimatorInstaller>
struct  Installer_2_tF6D845F3F63B23826F20664C750AAA79301B8DBB  : public InstallerBase_t4F464ACE8FEB8CEEE7B4352A271F09894C28AD79
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTALLER_2_TF6D845F3F63B23826F20664C750AAA79301B8DBB_H
#ifndef SUBCONTAINERCREATORBYNEWPREFABINSTALLER_T8C6712ED0C9D7CECCB02AB69C9FC69AB41F933B9_H
#define SUBCONTAINERCREATORBYNEWPREFABINSTALLER_T8C6712ED0C9D7CECCB02AB69C9FC69AB41F933B9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerCreatorByNewPrefabInstaller
struct  SubContainerCreatorByNewPrefabInstaller_t8C6712ED0C9D7CECCB02AB69C9FC69AB41F933B9  : public SubContainerCreatorByNewPrefabDynamicContext_t0FF9DB5FAA6CE972FFABBC76BA89560C17FFEC94
{
public:
	// System.Type Zenject.SubContainerCreatorByNewPrefabInstaller::_installerType
	Type_t * ____installerType_3;
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.SubContainerCreatorByNewPrefabInstaller::_extraArgs
	List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * ____extraArgs_4;

public:
	inline static int32_t get_offset_of__installerType_3() { return static_cast<int32_t>(offsetof(SubContainerCreatorByNewPrefabInstaller_t8C6712ED0C9D7CECCB02AB69C9FC69AB41F933B9, ____installerType_3)); }
	inline Type_t * get__installerType_3() const { return ____installerType_3; }
	inline Type_t ** get_address_of__installerType_3() { return &____installerType_3; }
	inline void set__installerType_3(Type_t * value)
	{
		____installerType_3 = value;
		Il2CppCodeGenWriteBarrier((&____installerType_3), value);
	}

	inline static int32_t get_offset_of__extraArgs_4() { return static_cast<int32_t>(offsetof(SubContainerCreatorByNewPrefabInstaller_t8C6712ED0C9D7CECCB02AB69C9FC69AB41F933B9, ____extraArgs_4)); }
	inline List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * get__extraArgs_4() const { return ____extraArgs_4; }
	inline List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 ** get_address_of__extraArgs_4() { return &____extraArgs_4; }
	inline void set__extraArgs_4(List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * value)
	{
		____extraArgs_4 = value;
		Il2CppCodeGenWriteBarrier((&____extraArgs_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBCONTAINERCREATORBYNEWPREFABINSTALLER_T8C6712ED0C9D7CECCB02AB69C9FC69AB41F933B9_H
#ifndef SUBCONTAINERCREATORBYNEWPREFABMETHOD_T960B02558F24CCDF254D05DE0321B047E10A78FE_H
#define SUBCONTAINERCREATORBYNEWPREFABMETHOD_T960B02558F24CCDF254D05DE0321B047E10A78FE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerCreatorByNewPrefabMethod
struct  SubContainerCreatorByNewPrefabMethod_t960B02558F24CCDF254D05DE0321B047E10A78FE  : public SubContainerCreatorByNewPrefabDynamicContext_t0FF9DB5FAA6CE972FFABBC76BA89560C17FFEC94
{
public:
	// System.Action`1<Zenject.DiContainer> Zenject.SubContainerCreatorByNewPrefabMethod::_installerMethod
	Action_1_t47FB75EB7E275E7EC20C1ADC32AC936BD0BD4E84 * ____installerMethod_3;

public:
	inline static int32_t get_offset_of__installerMethod_3() { return static_cast<int32_t>(offsetof(SubContainerCreatorByNewPrefabMethod_t960B02558F24CCDF254D05DE0321B047E10A78FE, ____installerMethod_3)); }
	inline Action_1_t47FB75EB7E275E7EC20C1ADC32AC936BD0BD4E84 * get__installerMethod_3() const { return ____installerMethod_3; }
	inline Action_1_t47FB75EB7E275E7EC20C1ADC32AC936BD0BD4E84 ** get_address_of__installerMethod_3() { return &____installerMethod_3; }
	inline void set__installerMethod_3(Action_1_t47FB75EB7E275E7EC20C1ADC32AC936BD0BD4E84 * value)
	{
		____installerMethod_3 = value;
		Il2CppCodeGenWriteBarrier((&____installerMethod_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBCONTAINERCREATORBYNEWPREFABMETHOD_T960B02558F24CCDF254D05DE0321B047E10A78FE_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef ANIMATORINSTALLER_T34F3246CE01EC918798E6F5CFAAF904759ACBDFE_H
#define ANIMATORINSTALLER_T34F3246CE01EC918798E6F5CFAAF904759ACBDFE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.AnimatorInstaller
struct  AnimatorInstaller_t34F3246CE01EC918798E6F5CFAAF904759ACBDFE  : public Installer_2_tF6D845F3F63B23826F20664C750AAA79301B8DBB
{
public:
	// UnityEngine.Animator Zenject.AnimatorInstaller::_animator
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ____animator_1;

public:
	inline static int32_t get_offset_of__animator_1() { return static_cast<int32_t>(offsetof(AnimatorInstaller_t34F3246CE01EC918798E6F5CFAAF904759ACBDFE, ____animator_1)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get__animator_1() const { return ____animator_1; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of__animator_1() { return &____animator_1; }
	inline void set__animator_1(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		____animator_1 = value;
		Il2CppCodeGenWriteBarrier((&____animator_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATORINSTALLER_T34F3246CE01EC918798E6F5CFAAF904759ACBDFE_H
#ifndef INJECTSOURCES_T643ECF8A27899CEE7D40760762395F6C3E359049_H
#define INJECTSOURCES_T643ECF8A27899CEE7D40760762395F6C3E359049_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.InjectSources
struct  InjectSources_t643ECF8A27899CEE7D40760762395F6C3E359049 
{
public:
	// System.Int32 Zenject.InjectSources::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InjectSources_t643ECF8A27899CEE7D40760762395F6C3E359049, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INJECTSOURCES_T643ECF8A27899CEE7D40760762395F6C3E359049_H
#ifndef SINGLETONTYPES_T95744D53A57B5D7318E65315B811CD90544AEF62_H
#define SINGLETONTYPES_T95744D53A57B5D7318E65315B811CD90544AEF62_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SingletonTypes
struct  SingletonTypes_t95744D53A57B5D7318E65315B811CD90544AEF62 
{
public:
	// System.Int32 Zenject.SingletonTypes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SingletonTypes_t95744D53A57B5D7318E65315B811CD90544AEF62, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETONTYPES_T95744D53A57B5D7318E65315B811CD90544AEF62_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef RESOLVEPROVIDER_T98295C4A37915579D7B978AC7D271448C3822CB8_H
#define RESOLVEPROVIDER_T98295C4A37915579D7B978AC7D271448C3822CB8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ResolveProvider
struct  ResolveProvider_t98295C4A37915579D7B978AC7D271448C3822CB8  : public RuntimeObject
{
public:
	// System.Object Zenject.ResolveProvider::_identifier
	RuntimeObject * ____identifier_0;
	// Zenject.DiContainer Zenject.ResolveProvider::_container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____container_1;
	// System.Type Zenject.ResolveProvider::_contractType
	Type_t * ____contractType_2;
	// System.Boolean Zenject.ResolveProvider::_isOptional
	bool ____isOptional_3;
	// Zenject.InjectSources Zenject.ResolveProvider::_source
	int32_t ____source_4;

public:
	inline static int32_t get_offset_of__identifier_0() { return static_cast<int32_t>(offsetof(ResolveProvider_t98295C4A37915579D7B978AC7D271448C3822CB8, ____identifier_0)); }
	inline RuntimeObject * get__identifier_0() const { return ____identifier_0; }
	inline RuntimeObject ** get_address_of__identifier_0() { return &____identifier_0; }
	inline void set__identifier_0(RuntimeObject * value)
	{
		____identifier_0 = value;
		Il2CppCodeGenWriteBarrier((&____identifier_0), value);
	}

	inline static int32_t get_offset_of__container_1() { return static_cast<int32_t>(offsetof(ResolveProvider_t98295C4A37915579D7B978AC7D271448C3822CB8, ____container_1)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__container_1() const { return ____container_1; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__container_1() { return &____container_1; }
	inline void set__container_1(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____container_1 = value;
		Il2CppCodeGenWriteBarrier((&____container_1), value);
	}

	inline static int32_t get_offset_of__contractType_2() { return static_cast<int32_t>(offsetof(ResolveProvider_t98295C4A37915579D7B978AC7D271448C3822CB8, ____contractType_2)); }
	inline Type_t * get__contractType_2() const { return ____contractType_2; }
	inline Type_t ** get_address_of__contractType_2() { return &____contractType_2; }
	inline void set__contractType_2(Type_t * value)
	{
		____contractType_2 = value;
		Il2CppCodeGenWriteBarrier((&____contractType_2), value);
	}

	inline static int32_t get_offset_of__isOptional_3() { return static_cast<int32_t>(offsetof(ResolveProvider_t98295C4A37915579D7B978AC7D271448C3822CB8, ____isOptional_3)); }
	inline bool get__isOptional_3() const { return ____isOptional_3; }
	inline bool* get_address_of__isOptional_3() { return &____isOptional_3; }
	inline void set__isOptional_3(bool value)
	{
		____isOptional_3 = value;
	}

	inline static int32_t get_offset_of__source_4() { return static_cast<int32_t>(offsetof(ResolveProvider_t98295C4A37915579D7B978AC7D271448C3822CB8, ____source_4)); }
	inline int32_t get__source_4() const { return ____source_4; }
	inline int32_t* get_address_of__source_4() { return &____source_4; }
	inline void set__source_4(int32_t value)
	{
		____source_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESOLVEPROVIDER_T98295C4A37915579D7B978AC7D271448C3822CB8_H
#ifndef STANDARDSINGLETONDECLARATION_TB24801ED65CAF3CC0145574AF670A9FFC4F22101_H
#define STANDARDSINGLETONDECLARATION_TB24801ED65CAF3CC0145574AF670A9FFC4F22101_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.StandardSingletonDeclaration
struct  StandardSingletonDeclaration_tB24801ED65CAF3CC0145574AF670A9FFC4F22101  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.StandardSingletonDeclaration::<Arguments>k__BackingField
	List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * ___U3CArgumentsU3Ek__BackingField_0;
	// Zenject.SingletonId Zenject.StandardSingletonDeclaration::<Id>k__BackingField
	SingletonId_tF74E433D582ABB3E7532764D59325896A7A8D64C * ___U3CIdU3Ek__BackingField_1;
	// Zenject.SingletonTypes Zenject.StandardSingletonDeclaration::<Type>k__BackingField
	int32_t ___U3CTypeU3Ek__BackingField_2;
	// System.Object Zenject.StandardSingletonDeclaration::<SpecificId>k__BackingField
	RuntimeObject * ___U3CSpecificIdU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CArgumentsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StandardSingletonDeclaration_tB24801ED65CAF3CC0145574AF670A9FFC4F22101, ___U3CArgumentsU3Ek__BackingField_0)); }
	inline List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * get_U3CArgumentsU3Ek__BackingField_0() const { return ___U3CArgumentsU3Ek__BackingField_0; }
	inline List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 ** get_address_of_U3CArgumentsU3Ek__BackingField_0() { return &___U3CArgumentsU3Ek__BackingField_0; }
	inline void set_U3CArgumentsU3Ek__BackingField_0(List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * value)
	{
		___U3CArgumentsU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CArgumentsU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CIdU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(StandardSingletonDeclaration_tB24801ED65CAF3CC0145574AF670A9FFC4F22101, ___U3CIdU3Ek__BackingField_1)); }
	inline SingletonId_tF74E433D582ABB3E7532764D59325896A7A8D64C * get_U3CIdU3Ek__BackingField_1() const { return ___U3CIdU3Ek__BackingField_1; }
	inline SingletonId_tF74E433D582ABB3E7532764D59325896A7A8D64C ** get_address_of_U3CIdU3Ek__BackingField_1() { return &___U3CIdU3Ek__BackingField_1; }
	inline void set_U3CIdU3Ek__BackingField_1(SingletonId_tF74E433D582ABB3E7532764D59325896A7A8D64C * value)
	{
		___U3CIdU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CIdU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CTypeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(StandardSingletonDeclaration_tB24801ED65CAF3CC0145574AF670A9FFC4F22101, ___U3CTypeU3Ek__BackingField_2)); }
	inline int32_t get_U3CTypeU3Ek__BackingField_2() const { return ___U3CTypeU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CTypeU3Ek__BackingField_2() { return &___U3CTypeU3Ek__BackingField_2; }
	inline void set_U3CTypeU3Ek__BackingField_2(int32_t value)
	{
		___U3CTypeU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CSpecificIdU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(StandardSingletonDeclaration_tB24801ED65CAF3CC0145574AF670A9FFC4F22101, ___U3CSpecificIdU3Ek__BackingField_3)); }
	inline RuntimeObject * get_U3CSpecificIdU3Ek__BackingField_3() const { return ___U3CSpecificIdU3Ek__BackingField_3; }
	inline RuntimeObject ** get_address_of_U3CSpecificIdU3Ek__BackingField_3() { return &___U3CSpecificIdU3Ek__BackingField_3; }
	inline void set_U3CSpecificIdU3Ek__BackingField_3(RuntimeObject * value)
	{
		___U3CSpecificIdU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSpecificIdU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STANDARDSINGLETONDECLARATION_TB24801ED65CAF3CC0145574AF670A9FFC4F22101_H
#ifndef PROVIDERINFO_T1DBFCE455BFF700F8C47B7CE12BB93ADA704B674_H
#define PROVIDERINFO_T1DBFCE455BFF700F8C47B7CE12BB93ADA704B674_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.StandardSingletonProviderCreator_ProviderInfo
struct  ProviderInfo_t1DBFCE455BFF700F8C47B7CE12BB93ADA704B674  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.StandardSingletonProviderCreator_ProviderInfo::<Arguments>k__BackingField
	List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * ___U3CArgumentsU3Ek__BackingField_0;
	// System.Object Zenject.StandardSingletonProviderCreator_ProviderInfo::<SingletonSpecificId>k__BackingField
	RuntimeObject * ___U3CSingletonSpecificIdU3Ek__BackingField_1;
	// Zenject.SingletonTypes Zenject.StandardSingletonProviderCreator_ProviderInfo::<Type>k__BackingField
	int32_t ___U3CTypeU3Ek__BackingField_2;
	// Zenject.CachedProvider Zenject.StandardSingletonProviderCreator_ProviderInfo::<Provider>k__BackingField
	CachedProvider_t0DD0B25EA381638476E1E4EC2FC32C8CDA59BA36 * ___U3CProviderU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CArgumentsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ProviderInfo_t1DBFCE455BFF700F8C47B7CE12BB93ADA704B674, ___U3CArgumentsU3Ek__BackingField_0)); }
	inline List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * get_U3CArgumentsU3Ek__BackingField_0() const { return ___U3CArgumentsU3Ek__BackingField_0; }
	inline List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 ** get_address_of_U3CArgumentsU3Ek__BackingField_0() { return &___U3CArgumentsU3Ek__BackingField_0; }
	inline void set_U3CArgumentsU3Ek__BackingField_0(List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * value)
	{
		___U3CArgumentsU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CArgumentsU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CSingletonSpecificIdU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ProviderInfo_t1DBFCE455BFF700F8C47B7CE12BB93ADA704B674, ___U3CSingletonSpecificIdU3Ek__BackingField_1)); }
	inline RuntimeObject * get_U3CSingletonSpecificIdU3Ek__BackingField_1() const { return ___U3CSingletonSpecificIdU3Ek__BackingField_1; }
	inline RuntimeObject ** get_address_of_U3CSingletonSpecificIdU3Ek__BackingField_1() { return &___U3CSingletonSpecificIdU3Ek__BackingField_1; }
	inline void set_U3CSingletonSpecificIdU3Ek__BackingField_1(RuntimeObject * value)
	{
		___U3CSingletonSpecificIdU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSingletonSpecificIdU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CTypeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ProviderInfo_t1DBFCE455BFF700F8C47B7CE12BB93ADA704B674, ___U3CTypeU3Ek__BackingField_2)); }
	inline int32_t get_U3CTypeU3Ek__BackingField_2() const { return ___U3CTypeU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CTypeU3Ek__BackingField_2() { return &___U3CTypeU3Ek__BackingField_2; }
	inline void set_U3CTypeU3Ek__BackingField_2(int32_t value)
	{
		___U3CTypeU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CProviderU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ProviderInfo_t1DBFCE455BFF700F8C47B7CE12BB93ADA704B674, ___U3CProviderU3Ek__BackingField_3)); }
	inline CachedProvider_t0DD0B25EA381638476E1E4EC2FC32C8CDA59BA36 * get_U3CProviderU3Ek__BackingField_3() const { return ___U3CProviderU3Ek__BackingField_3; }
	inline CachedProvider_t0DD0B25EA381638476E1E4EC2FC32C8CDA59BA36 ** get_address_of_U3CProviderU3Ek__BackingField_3() { return &___U3CProviderU3Ek__BackingField_3; }
	inline void set_U3CProviderU3Ek__BackingField_3(CachedProvider_t0DD0B25EA381638476E1E4EC2FC32C8CDA59BA36 * value)
	{
		___U3CProviderU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CProviderU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROVIDERINFO_T1DBFCE455BFF700F8C47B7CE12BB93ADA704B674_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef ANIMATORIKHANDLERMANAGER_T1B475CD68C26B5EB1018D9D2471BEB7FB2E17EEA_H
#define ANIMATORIKHANDLERMANAGER_T1B475CD68C26B5EB1018D9D2471BEB7FB2E17EEA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.AnimatorIkHandlerManager
struct  AnimatorIkHandlerManager_t1B475CD68C26B5EB1018D9D2471BEB7FB2E17EEA  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Collections.Generic.List`1<Zenject.IAnimatorIkHandler> Zenject.AnimatorIkHandlerManager::_handlers
	List_1_t8987DED585BFB6EA83AEBA1F954508BF6E883E51 * ____handlers_4;

public:
	inline static int32_t get_offset_of__handlers_4() { return static_cast<int32_t>(offsetof(AnimatorIkHandlerManager_t1B475CD68C26B5EB1018D9D2471BEB7FB2E17EEA, ____handlers_4)); }
	inline List_1_t8987DED585BFB6EA83AEBA1F954508BF6E883E51 * get__handlers_4() const { return ____handlers_4; }
	inline List_1_t8987DED585BFB6EA83AEBA1F954508BF6E883E51 ** get_address_of__handlers_4() { return &____handlers_4; }
	inline void set__handlers_4(List_1_t8987DED585BFB6EA83AEBA1F954508BF6E883E51 * value)
	{
		____handlers_4 = value;
		Il2CppCodeGenWriteBarrier((&____handlers_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATORIKHANDLERMANAGER_T1B475CD68C26B5EB1018D9D2471BEB7FB2E17EEA_H
#ifndef ANIMATORMOVEHANDLERMANAGER_TE0FAF65368A36AC536D43E81F5292110EDBC1AD3_H
#define ANIMATORMOVEHANDLERMANAGER_TE0FAF65368A36AC536D43E81F5292110EDBC1AD3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.AnimatorMoveHandlerManager
struct  AnimatorMoveHandlerManager_tE0FAF65368A36AC536D43E81F5292110EDBC1AD3  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Collections.Generic.List`1<Zenject.IAnimatorMoveHandler> Zenject.AnimatorMoveHandlerManager::_handlers
	List_1_t3155E39EF921C0D429E9287F26693366D648F153 * ____handlers_4;

public:
	inline static int32_t get_offset_of__handlers_4() { return static_cast<int32_t>(offsetof(AnimatorMoveHandlerManager_tE0FAF65368A36AC536D43E81F5292110EDBC1AD3, ____handlers_4)); }
	inline List_1_t3155E39EF921C0D429E9287F26693366D648F153 * get__handlers_4() const { return ____handlers_4; }
	inline List_1_t3155E39EF921C0D429E9287F26693366D648F153 ** get_address_of__handlers_4() { return &____handlers_4; }
	inline void set__handlers_4(List_1_t3155E39EF921C0D429E9287F26693366D648F153 * value)
	{
		____handlers_4 = value;
		Il2CppCodeGenWriteBarrier((&____handlers_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATORMOVEHANDLERMANAGER_TE0FAF65368A36AC536D43E81F5292110EDBC1AD3_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7400 = { sizeof (U3CGetAllInstancesWithInjectSplitU3Ed__5_t0243B4CEE2AE774AF4E8797A4E0BDB012E9C821B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7400[5] = 
{
	U3CGetAllInstancesWithInjectSplitU3Ed__5_t0243B4CEE2AE774AF4E8797A4E0BDB012E9C821B::get_offset_of_U3CU3E1__state_0(),
	U3CGetAllInstancesWithInjectSplitU3Ed__5_t0243B4CEE2AE774AF4E8797A4E0BDB012E9C821B::get_offset_of_U3CU3E2__current_1(),
	U3CGetAllInstancesWithInjectSplitU3Ed__5_t0243B4CEE2AE774AF4E8797A4E0BDB012E9C821B::get_offset_of_args_2(),
	U3CGetAllInstancesWithInjectSplitU3Ed__5_t0243B4CEE2AE774AF4E8797A4E0BDB012E9C821B::get_offset_of_context_3(),
	U3CGetAllInstancesWithInjectSplitU3Ed__5_t0243B4CEE2AE774AF4E8797A4E0BDB012E9C821B::get_offset_of_U3CU3E4__this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7401 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7402 = { sizeof (IProviderExtensions_t582E729B22D6D9DF6D784F8EF8123A3F006820F5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7403 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7403[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7404 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7404[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7405 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7405[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7406 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7406[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7407 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7407[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7408 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7408[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7409 = { sizeof (MethodProviderUntyped_tF78632D60E22154A0EE863440E1F1705DA1BD4F4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7409[2] = 
{
	MethodProviderUntyped_tF78632D60E22154A0EE863440E1F1705DA1BD4F4::get_offset_of__container_0(),
	MethodProviderUntyped_tF78632D60E22154A0EE863440E1F1705DA1BD4F4::get_offset_of__method_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7410 = { sizeof (U3CGetAllInstancesWithInjectSplitU3Ed__4_t0A88BEED476B7F82C985AE259DEC8DCE846FCCC3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7410[5] = 
{
	U3CGetAllInstancesWithInjectSplitU3Ed__4_t0A88BEED476B7F82C985AE259DEC8DCE846FCCC3::get_offset_of_U3CU3E1__state_0(),
	U3CGetAllInstancesWithInjectSplitU3Ed__4_t0A88BEED476B7F82C985AE259DEC8DCE846FCCC3::get_offset_of_U3CU3E2__current_1(),
	U3CGetAllInstancesWithInjectSplitU3Ed__4_t0A88BEED476B7F82C985AE259DEC8DCE846FCCC3::get_offset_of_args_2(),
	U3CGetAllInstancesWithInjectSplitU3Ed__4_t0A88BEED476B7F82C985AE259DEC8DCE846FCCC3::get_offset_of_context_3(),
	U3CGetAllInstancesWithInjectSplitU3Ed__4_t0A88BEED476B7F82C985AE259DEC8DCE846FCCC3::get_offset_of_U3CU3E4__this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7411 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7411[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7412 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7412[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7413 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7413[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7414 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7414[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7415 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7415[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7416 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7416[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7417 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7417[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7418 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7418[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7419 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7419[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7420 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7420[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7421 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7421[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7422 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7422[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7423 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7424 = { sizeof (PrefabInstantiator_t9A8FF9385741C61D38487118C3E94341F2C7CB21), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7424[5] = 
{
	PrefabInstantiator_t9A8FF9385741C61D38487118C3E94341F2C7CB21::get_offset_of__prefabProvider_0(),
	PrefabInstantiator_t9A8FF9385741C61D38487118C3E94341F2C7CB21::get_offset_of__container_1(),
	PrefabInstantiator_t9A8FF9385741C61D38487118C3E94341F2C7CB21::get_offset_of__extraArguments_2(),
	PrefabInstantiator_t9A8FF9385741C61D38487118C3E94341F2C7CB21::get_offset_of__gameObjectBindInfo_3(),
	PrefabInstantiator_t9A8FF9385741C61D38487118C3E94341F2C7CB21::get_offset_of__argumentTarget_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7425 = { sizeof (U3CInstantiateU3Ed__13_tC7BC92BC8BFD11A194B9CA38C6486689D1B81397), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7425[7] = 
{
	U3CInstantiateU3Ed__13_tC7BC92BC8BFD11A194B9CA38C6486689D1B81397::get_offset_of_U3CU3E1__state_0(),
	U3CInstantiateU3Ed__13_tC7BC92BC8BFD11A194B9CA38C6486689D1B81397::get_offset_of_U3CU3E2__current_1(),
	U3CInstantiateU3Ed__13_tC7BC92BC8BFD11A194B9CA38C6486689D1B81397::get_offset_of_U3CU3E4__this_2(),
	U3CInstantiateU3Ed__13_tC7BC92BC8BFD11A194B9CA38C6486689D1B81397::get_offset_of_args_3(),
	U3CInstantiateU3Ed__13_tC7BC92BC8BFD11A194B9CA38C6486689D1B81397::get_offset_of_U3CcontextU3E5__2_4(),
	U3CInstantiateU3Ed__13_tC7BC92BC8BFD11A194B9CA38C6486689D1B81397::get_offset_of_U3CshouldMakeActiveU3E5__3_5(),
	U3CInstantiateU3Ed__13_tC7BC92BC8BFD11A194B9CA38C6486689D1B81397::get_offset_of_U3CgameObjectU3E5__4_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7426 = { sizeof (PrefabInstantiatorCached_tB23BE6E42DED15DF8DD349E6E8C80C45CBDA95D5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7426[2] = 
{
	PrefabInstantiatorCached_tB23BE6E42DED15DF8DD349E6E8C80C45CBDA95D5::get_offset_of__subInstantiator_0(),
	PrefabInstantiatorCached_tB23BE6E42DED15DF8DD349E6E8C80C45CBDA95D5::get_offset_of__gameObject_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7427 = { sizeof (U3CInstantiateU3Ed__10_t6E229FE4C6F2F530C7431340B69D35641DCE8B67), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7427[6] = 
{
	U3CInstantiateU3Ed__10_t6E229FE4C6F2F530C7431340B69D35641DCE8B67::get_offset_of_U3CU3E1__state_0(),
	U3CInstantiateU3Ed__10_t6E229FE4C6F2F530C7431340B69D35641DCE8B67::get_offset_of_U3CU3E2__current_1(),
	U3CInstantiateU3Ed__10_t6E229FE4C6F2F530C7431340B69D35641DCE8B67::get_offset_of_args_2(),
	U3CInstantiateU3Ed__10_t6E229FE4C6F2F530C7431340B69D35641DCE8B67::get_offset_of_U3CU3E4__this_3(),
	U3CInstantiateU3Ed__10_t6E229FE4C6F2F530C7431340B69D35641DCE8B67::get_offset_of_U3CrunnerU3E5__2_4(),
	U3CInstantiateU3Ed__10_t6E229FE4C6F2F530C7431340B69D35641DCE8B67::get_offset_of_U3ChasMoreU3E5__3_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7428 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7429 = { sizeof (PrefabProvider_tC323C17BEECAB2A24F0D17DBEAC13395B6797034), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7429[1] = 
{
	PrefabProvider_tC323C17BEECAB2A24F0D17DBEAC13395B6797034::get_offset_of__prefab_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7430 = { sizeof (PrefabProviderResource_tA4BDAA46C8051D3CAEFF7C4DC8E37189A1D76D87), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7430[1] = 
{
	PrefabProviderResource_tA4BDAA46C8051D3CAEFF7C4DC8E37189A1D76D87::get_offset_of__resourcePath_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7431 = { sizeof (ProviderUtil_t187ABAB1AB3B6AF5D15DAF503BC3B3FA32D3A0C4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7432 = { sizeof (ResolveProvider_t98295C4A37915579D7B978AC7D271448C3822CB8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7432[5] = 
{
	ResolveProvider_t98295C4A37915579D7B978AC7D271448C3822CB8::get_offset_of__identifier_0(),
	ResolveProvider_t98295C4A37915579D7B978AC7D271448C3822CB8::get_offset_of__container_1(),
	ResolveProvider_t98295C4A37915579D7B978AC7D271448C3822CB8::get_offset_of__contractType_2(),
	ResolveProvider_t98295C4A37915579D7B978AC7D271448C3822CB8::get_offset_of__isOptional_3(),
	ResolveProvider_t98295C4A37915579D7B978AC7D271448C3822CB8::get_offset_of__source_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7433 = { sizeof (U3CGetAllInstancesWithInjectSplitU3Ed__7_tE755A1A34FBF05512892ED8C1391C70B0A3599D3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7433[5] = 
{
	U3CGetAllInstancesWithInjectSplitU3Ed__7_tE755A1A34FBF05512892ED8C1391C70B0A3599D3::get_offset_of_U3CU3E1__state_0(),
	U3CGetAllInstancesWithInjectSplitU3Ed__7_tE755A1A34FBF05512892ED8C1391C70B0A3599D3::get_offset_of_U3CU3E2__current_1(),
	U3CGetAllInstancesWithInjectSplitU3Ed__7_tE755A1A34FBF05512892ED8C1391C70B0A3599D3::get_offset_of_args_2(),
	U3CGetAllInstancesWithInjectSplitU3Ed__7_tE755A1A34FBF05512892ED8C1391C70B0A3599D3::get_offset_of_context_3(),
	U3CGetAllInstancesWithInjectSplitU3Ed__7_tE755A1A34FBF05512892ED8C1391C70B0A3599D3::get_offset_of_U3CU3E4__this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7434 = { sizeof (ResourceProvider_t19DA8EB0F876F7D6CB06485EE48DE101A9C8934E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7434[2] = 
{
	ResourceProvider_t19DA8EB0F876F7D6CB06485EE48DE101A9C8934E::get_offset_of__resourceType_0(),
	ResourceProvider_t19DA8EB0F876F7D6CB06485EE48DE101A9C8934E::get_offset_of__resourcePath_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7435 = { sizeof (U3CGetAllInstancesWithInjectSplitU3Ed__4_t3FE8EDDF05D90AB5E52CBAF0BAEC0F0C3EC85E55), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7435[5] = 
{
	U3CGetAllInstancesWithInjectSplitU3Ed__4_t3FE8EDDF05D90AB5E52CBAF0BAEC0F0C3EC85E55::get_offset_of_U3CU3E1__state_0(),
	U3CGetAllInstancesWithInjectSplitU3Ed__4_t3FE8EDDF05D90AB5E52CBAF0BAEC0F0C3EC85E55::get_offset_of_U3CU3E2__current_1(),
	U3CGetAllInstancesWithInjectSplitU3Ed__4_t3FE8EDDF05D90AB5E52CBAF0BAEC0F0C3EC85E55::get_offset_of_args_2(),
	U3CGetAllInstancesWithInjectSplitU3Ed__4_t3FE8EDDF05D90AB5E52CBAF0BAEC0F0C3EC85E55::get_offset_of_context_3(),
	U3CGetAllInstancesWithInjectSplitU3Ed__4_t3FE8EDDF05D90AB5E52CBAF0BAEC0F0C3EC85E55::get_offset_of_U3CU3E4__this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7436 = { sizeof (ScriptableObjectResourceProvider_t4A007E9097B9C258E9A4A7FE39338577B3961F44), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7436[6] = 
{
	ScriptableObjectResourceProvider_t4A007E9097B9C258E9A4A7FE39338577B3961F44::get_offset_of__container_0(),
	ScriptableObjectResourceProvider_t4A007E9097B9C258E9A4A7FE39338577B3961F44::get_offset_of__resourceType_1(),
	ScriptableObjectResourceProvider_t4A007E9097B9C258E9A4A7FE39338577B3961F44::get_offset_of__resourcePath_2(),
	ScriptableObjectResourceProvider_t4A007E9097B9C258E9A4A7FE39338577B3961F44::get_offset_of__extraArguments_3(),
	ScriptableObjectResourceProvider_t4A007E9097B9C258E9A4A7FE39338577B3961F44::get_offset_of__concreteIdentifier_4(),
	ScriptableObjectResourceProvider_t4A007E9097B9C258E9A4A7FE39338577B3961F44::get_offset_of__createNew_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7437 = { sizeof (U3CU3Ec_tD6959097A591E9916075DF4F1795A247D91E512F), -1, sizeof(U3CU3Ec_tD6959097A591E9916075DF4F1795A247D91E512F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7437[2] = 
{
	U3CU3Ec_tD6959097A591E9916075DF4F1795A247D91E512F_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tD6959097A591E9916075DF4F1795A247D91E512F_StaticFields::get_offset_of_U3CU3E9__8_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7438 = { sizeof (U3CGetAllInstancesWithInjectSplitU3Ed__8_tF20F96867E0CBF13D5D21959656AA4055165D670), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7438[6] = 
{
	U3CGetAllInstancesWithInjectSplitU3Ed__8_tF20F96867E0CBF13D5D21959656AA4055165D670::get_offset_of_U3CU3E1__state_0(),
	U3CGetAllInstancesWithInjectSplitU3Ed__8_tF20F96867E0CBF13D5D21959656AA4055165D670::get_offset_of_U3CU3E2__current_1(),
	U3CGetAllInstancesWithInjectSplitU3Ed__8_tF20F96867E0CBF13D5D21959656AA4055165D670::get_offset_of_context_2(),
	U3CGetAllInstancesWithInjectSplitU3Ed__8_tF20F96867E0CBF13D5D21959656AA4055165D670::get_offset_of_U3CU3E4__this_3(),
	U3CGetAllInstancesWithInjectSplitU3Ed__8_tF20F96867E0CBF13D5D21959656AA4055165D670::get_offset_of_args_4(),
	U3CGetAllInstancesWithInjectSplitU3Ed__8_tF20F96867E0CBF13D5D21959656AA4055165D670::get_offset_of_U3CobjectsU3E5__2_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7439 = { sizeof (PrefabResourceSingletonProviderCreator_t9A996ED86F32A28313FCA6B25175E470AFCD8A41), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7439[3] = 
{
	PrefabResourceSingletonProviderCreator_t9A996ED86F32A28313FCA6B25175E470AFCD8A41::get_offset_of__markRegistry_0(),
	PrefabResourceSingletonProviderCreator_t9A996ED86F32A28313FCA6B25175E470AFCD8A41::get_offset_of__container_1(),
	PrefabResourceSingletonProviderCreator_t9A996ED86F32A28313FCA6B25175E470AFCD8A41::get_offset_of__prefabCreators_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7440 = { sizeof (PrefabId_t9C8A02397FC9DE7476535AC2C9182F87EEBEC004), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7440[2] = 
{
	PrefabId_t9C8A02397FC9DE7476535AC2C9182F87EEBEC004::get_offset_of_ConcreteIdentifier_0(),
	PrefabId_t9C8A02397FC9DE7476535AC2C9182F87EEBEC004::get_offset_of_ResourcePath_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7441 = { sizeof (PrefabSingletonProviderCreator_t2C3CC78B15E2FB38FE6DD8F2D281F6E4F0533075), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7441[3] = 
{
	PrefabSingletonProviderCreator_t2C3CC78B15E2FB38FE6DD8F2D281F6E4F0533075::get_offset_of__markRegistry_0(),
	PrefabSingletonProviderCreator_t2C3CC78B15E2FB38FE6DD8F2D281F6E4F0533075::get_offset_of__container_1(),
	PrefabSingletonProviderCreator_t2C3CC78B15E2FB38FE6DD8F2D281F6E4F0533075::get_offset_of__prefabCreators_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7442 = { sizeof (PrefabId_t8F1CE05CB1F0C46FB5397BC97495BFEBE57E5059), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7442[2] = 
{
	PrefabId_t8F1CE05CB1F0C46FB5397BC97495BFEBE57E5059::get_offset_of_ConcreteIdentifier_0(),
	PrefabId_t8F1CE05CB1F0C46FB5397BC97495BFEBE57E5059::get_offset_of_Prefab_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7443 = { sizeof (SingletonId_tF74E433D582ABB3E7532764D59325896A7A8D64C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7443[2] = 
{
	SingletonId_tF74E433D582ABB3E7532764D59325896A7A8D64C::get_offset_of_ConcreteType_0(),
	SingletonId_tF74E433D582ABB3E7532764D59325896A7A8D64C::get_offset_of_ConcreteIdentifier_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7444 = { sizeof (SingletonImplIds_t55CF17EFC231DA900608C4D728265A2BEDC2B593), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7445 = { sizeof (ToMethod_t12F5E0A4D9C370BB507C42523A4401D55EBE2836), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7445[1] = 
{
	ToMethod_t12F5E0A4D9C370BB507C42523A4401D55EBE2836::get_offset_of__method_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7446 = { sizeof (ToGetter_t269C4EED29F0AC2A1C9FB4061EA228DC2DDADED4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7446[2] = 
{
	ToGetter_t269C4EED29F0AC2A1C9FB4061EA228DC2DDADED4::get_offset_of__method_0(),
	ToGetter_t269C4EED29F0AC2A1C9FB4061EA228DC2DDADED4::get_offset_of__identifier_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7447 = { sizeof (SingletonMarkRegistry_tB0ADDC9314F22EFEF3C51F08F71E6E1CB236EC7F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7447[1] = 
{
	SingletonMarkRegistry_tB0ADDC9314F22EFEF3C51F08F71E6E1CB236EC7F::get_offset_of__singletonTypes_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7448 = { sizeof (SingletonProviderCreator_t07218AEFEEBFF6A8B65FB17100F1AB292A94F5AD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7448[7] = 
{
	SingletonProviderCreator_t07218AEFEEBFF6A8B65FB17100F1AB292A94F5AD::get_offset_of__standardProviderCreator_0(),
	SingletonProviderCreator_t07218AEFEEBFF6A8B65FB17100F1AB292A94F5AD::get_offset_of__subContainerMethodProviderCreator_1(),
	SingletonProviderCreator_t07218AEFEEBFF6A8B65FB17100F1AB292A94F5AD::get_offset_of__subContainerInstallerProviderCreator_2(),
	SingletonProviderCreator_t07218AEFEEBFF6A8B65FB17100F1AB292A94F5AD::get_offset_of__subContainerPrefabProviderCreator_3(),
	SingletonProviderCreator_t07218AEFEEBFF6A8B65FB17100F1AB292A94F5AD::get_offset_of__subContainerPrefabResourceProviderCreator_4(),
	SingletonProviderCreator_t07218AEFEEBFF6A8B65FB17100F1AB292A94F5AD::get_offset_of__prefabProviderCreator_5(),
	SingletonProviderCreator_t07218AEFEEBFF6A8B65FB17100F1AB292A94F5AD::get_offset_of__prefabResourceProviderCreator_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7449 = { sizeof (SingletonTypes_t95744D53A57B5D7318E65315B811CD90544AEF62)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7449[19] = 
{
	SingletonTypes_t95744D53A57B5D7318E65315B811CD90544AEF62::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7450 = { sizeof (StandardSingletonDeclaration_tB24801ED65CAF3CC0145574AF670A9FFC4F22101), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7450[4] = 
{
	StandardSingletonDeclaration_tB24801ED65CAF3CC0145574AF670A9FFC4F22101::get_offset_of_U3CArgumentsU3Ek__BackingField_0(),
	StandardSingletonDeclaration_tB24801ED65CAF3CC0145574AF670A9FFC4F22101::get_offset_of_U3CIdU3Ek__BackingField_1(),
	StandardSingletonDeclaration_tB24801ED65CAF3CC0145574AF670A9FFC4F22101::get_offset_of_U3CTypeU3Ek__BackingField_2(),
	StandardSingletonDeclaration_tB24801ED65CAF3CC0145574AF670A9FFC4F22101::get_offset_of_U3CSpecificIdU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7451 = { sizeof (StandardSingletonProviderCreator_t591FCA8A24159723185A79698D93BD3CE3647E80), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7451[3] = 
{
	StandardSingletonProviderCreator_t591FCA8A24159723185A79698D93BD3CE3647E80::get_offset_of__markRegistry_0(),
	StandardSingletonProviderCreator_t591FCA8A24159723185A79698D93BD3CE3647E80::get_offset_of__providerMap_1(),
	StandardSingletonProviderCreator_t591FCA8A24159723185A79698D93BD3CE3647E80::get_offset_of__container_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7452 = { sizeof (ProviderInfo_t1DBFCE455BFF700F8C47B7CE12BB93ADA704B674), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7452[4] = 
{
	ProviderInfo_t1DBFCE455BFF700F8C47B7CE12BB93ADA704B674::get_offset_of_U3CArgumentsU3Ek__BackingField_0(),
	ProviderInfo_t1DBFCE455BFF700F8C47B7CE12BB93ADA704B674::get_offset_of_U3CSingletonSpecificIdU3Ek__BackingField_1(),
	ProviderInfo_t1DBFCE455BFF700F8C47B7CE12BB93ADA704B674::get_offset_of_U3CTypeU3Ek__BackingField_2(),
	ProviderInfo_t1DBFCE455BFF700F8C47B7CE12BB93ADA704B674::get_offset_of_U3CProviderU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7453 = { sizeof (SubContainerSingletonProviderCreatorByInstaller_t0612A1F030297B7EEDB675CC61E90C60AAED986E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7453[3] = 
{
	SubContainerSingletonProviderCreatorByInstaller_t0612A1F030297B7EEDB675CC61E90C60AAED986E::get_offset_of__markRegistry_0(),
	SubContainerSingletonProviderCreatorByInstaller_t0612A1F030297B7EEDB675CC61E90C60AAED986E::get_offset_of__container_1(),
	SubContainerSingletonProviderCreatorByInstaller_t0612A1F030297B7EEDB675CC61E90C60AAED986E::get_offset_of__subContainerCreators_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7454 = { sizeof (InstallerSingletonId_tE9D05E360C566D1720455F90A0DA3356B7552C96), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7454[2] = 
{
	InstallerSingletonId_tE9D05E360C566D1720455F90A0DA3356B7552C96::get_offset_of_ConcreteIdentifier_0(),
	InstallerSingletonId_tE9D05E360C566D1720455F90A0DA3356B7552C96::get_offset_of_InstallerType_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7455 = { sizeof (SubContainerSingletonProviderCreatorByMethod_t875112116B40E0B708D4EE4601E758BACF9F1DED), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7455[3] = 
{
	SubContainerSingletonProviderCreatorByMethod_t875112116B40E0B708D4EE4601E758BACF9F1DED::get_offset_of__markRegistry_0(),
	SubContainerSingletonProviderCreatorByMethod_t875112116B40E0B708D4EE4601E758BACF9F1DED::get_offset_of__container_1(),
	SubContainerSingletonProviderCreatorByMethod_t875112116B40E0B708D4EE4601E758BACF9F1DED::get_offset_of__subContainerCreators_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7456 = { sizeof (MethodSingletonId_t621D33781519595AAAD21649AC048D9BE29E7648), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7456[2] = 
{
	MethodSingletonId_t621D33781519595AAAD21649AC048D9BE29E7648::get_offset_of_ConcreteIdentifier_0(),
	MethodSingletonId_t621D33781519595AAAD21649AC048D9BE29E7648::get_offset_of_InstallerDelegate_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7457 = { sizeof (SubContainerSingletonProviderCreatorByNewPrefab_tD1BD0BC2BC3FB62EE8DDD51AE521B287C673A19B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7457[3] = 
{
	SubContainerSingletonProviderCreatorByNewPrefab_tD1BD0BC2BC3FB62EE8DDD51AE521B287C673A19B::get_offset_of__markRegistry_0(),
	SubContainerSingletonProviderCreatorByNewPrefab_tD1BD0BC2BC3FB62EE8DDD51AE521B287C673A19B::get_offset_of__container_1(),
	SubContainerSingletonProviderCreatorByNewPrefab_tD1BD0BC2BC3FB62EE8DDD51AE521B287C673A19B::get_offset_of__subContainerCreators_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7458 = { sizeof (CustomSingletonId_tF78EF913E97A6C14F827A97D7CD42641E421AB85), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7458[2] = 
{
	CustomSingletonId_tF78EF913E97A6C14F827A97D7CD42641E421AB85::get_offset_of_ConcreteIdentifier_0(),
	CustomSingletonId_tF78EF913E97A6C14F827A97D7CD42641E421AB85::get_offset_of_Prefab_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7459 = { sizeof (CreatorInfo_tC5365DBAF4D185C993E78A2E646FE796EF865386), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7459[2] = 
{
	CreatorInfo_tC5365DBAF4D185C993E78A2E646FE796EF865386::get_offset_of_U3CGameObjectCreationParametersU3Ek__BackingField_0(),
	CreatorInfo_tC5365DBAF4D185C993E78A2E646FE796EF865386::get_offset_of_U3CCreatorU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7460 = { sizeof (SubContainerSingletonProviderCreatorByNewPrefabResource_t0F56AB2C3837D0E2117B52265FCBD5E21A8295AB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7460[3] = 
{
	SubContainerSingletonProviderCreatorByNewPrefabResource_t0F56AB2C3837D0E2117B52265FCBD5E21A8295AB::get_offset_of__markRegistry_0(),
	SubContainerSingletonProviderCreatorByNewPrefabResource_t0F56AB2C3837D0E2117B52265FCBD5E21A8295AB::get_offset_of__container_1(),
	SubContainerSingletonProviderCreatorByNewPrefabResource_t0F56AB2C3837D0E2117B52265FCBD5E21A8295AB::get_offset_of__subContainerCreators_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7461 = { sizeof (CustomSingletonId_t96A0A45F602A050226A478E74A2EDFE0E7C834DA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7461[2] = 
{
	CustomSingletonId_t96A0A45F602A050226A478E74A2EDFE0E7C834DA::get_offset_of_ConcreteIdentifier_0(),
	CustomSingletonId_t96A0A45F602A050226A478E74A2EDFE0E7C834DA::get_offset_of_ResourcePath_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7462 = { sizeof (CreatorInfo_tEFC9008F54602EF6D4D1694BEFFAAB67FC409674), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7462[2] = 
{
	CreatorInfo_tEFC9008F54602EF6D4D1694BEFFAAB67FC409674::get_offset_of_U3CGameObjectCreationParametersU3Ek__BackingField_0(),
	CreatorInfo_tEFC9008F54602EF6D4D1694BEFFAAB67FC409674::get_offset_of_U3CCreatorU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7463 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7464 = { sizeof (SubContainerCreatorByInstaller_tB27CBFE878C3922D781B595813F0A35493A9E1C5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7464[3] = 
{
	SubContainerCreatorByInstaller_tB27CBFE878C3922D781B595813F0A35493A9E1C5::get_offset_of__installerType_0(),
	SubContainerCreatorByInstaller_tB27CBFE878C3922D781B595813F0A35493A9E1C5::get_offset_of__container_1(),
	SubContainerCreatorByInstaller_tB27CBFE878C3922D781B595813F0A35493A9E1C5::get_offset_of__extraArgs_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7465 = { sizeof (SubContainerCreatorByMethod_t563F1560E9AD8C4FDE2A434DBB21F7DBBD88E5F2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7465[2] = 
{
	SubContainerCreatorByMethod_t563F1560E9AD8C4FDE2A434DBB21F7DBBD88E5F2::get_offset_of__installMethod_0(),
	SubContainerCreatorByMethod_t563F1560E9AD8C4FDE2A434DBB21F7DBBD88E5F2::get_offset_of__container_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7466 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7466[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7467 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7467[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7468 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7468[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7469 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7469[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7470 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7470[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7471 = { sizeof (SubContainerCreatorByNewPrefab_t5F6159D02A80C3C750D631719F6E5794F1B431E1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7471[3] = 
{
	SubContainerCreatorByNewPrefab_t5F6159D02A80C3C750D631719F6E5794F1B431E1::get_offset_of__gameObjectBindInfo_0(),
	SubContainerCreatorByNewPrefab_t5F6159D02A80C3C750D631719F6E5794F1B431E1::get_offset_of__prefabProvider_1(),
	SubContainerCreatorByNewPrefab_t5F6159D02A80C3C750D631719F6E5794F1B431E1::get_offset_of__container_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7472 = { sizeof (SubContainerCreatorByNewPrefabDynamicContext_t0FF9DB5FAA6CE972FFABBC76BA89560C17FFEC94), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7472[3] = 
{
	SubContainerCreatorByNewPrefabDynamicContext_t0FF9DB5FAA6CE972FFABBC76BA89560C17FFEC94::get_offset_of__gameObjectBindInfo_0(),
	SubContainerCreatorByNewPrefabDynamicContext_t0FF9DB5FAA6CE972FFABBC76BA89560C17FFEC94::get_offset_of__prefabProvider_1(),
	SubContainerCreatorByNewPrefabDynamicContext_t0FF9DB5FAA6CE972FFABBC76BA89560C17FFEC94::get_offset_of__container_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7473 = { sizeof (SubContainerCreatorByNewPrefabInstaller_t8C6712ED0C9D7CECCB02AB69C9FC69AB41F933B9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7473[2] = 
{
	SubContainerCreatorByNewPrefabInstaller_t8C6712ED0C9D7CECCB02AB69C9FC69AB41F933B9::get_offset_of__installerType_3(),
	SubContainerCreatorByNewPrefabInstaller_t8C6712ED0C9D7CECCB02AB69C9FC69AB41F933B9::get_offset_of__extraArgs_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7474 = { sizeof (U3CU3Ec__DisplayClass3_0_tD4D46C61D919699AFCE201D76AD43BE6E0D01F69), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7474[2] = 
{
	U3CU3Ec__DisplayClass3_0_tD4D46C61D919699AFCE201D76AD43BE6E0D01F69::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass3_0_tD4D46C61D919699AFCE201D76AD43BE6E0D01F69::get_offset_of_args_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7475 = { sizeof (SubContainerCreatorByNewPrefabMethod_t960B02558F24CCDF254D05DE0321B047E10A78FE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7475[1] = 
{
	SubContainerCreatorByNewPrefabMethod_t960B02558F24CCDF254D05DE0321B047E10A78FE::get_offset_of__installerMethod_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7476 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7476[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7477 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7477[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7478 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7478[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7479 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7479[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7480 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7480[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7481 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7481[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7482 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7482[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7483 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7483[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7484 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7484[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7485 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7485[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7486 = { sizeof (SubContainerCreatorByNewPrefabWithParams_t7FAF9FE7D9537562FAC1474B20783DFE33D4F8AE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7486[4] = 
{
	SubContainerCreatorByNewPrefabWithParams_t7FAF9FE7D9537562FAC1474B20783DFE33D4F8AE::get_offset_of__container_0(),
	SubContainerCreatorByNewPrefabWithParams_t7FAF9FE7D9537562FAC1474B20783DFE33D4F8AE::get_offset_of__prefabProvider_1(),
	SubContainerCreatorByNewPrefabWithParams_t7FAF9FE7D9537562FAC1474B20783DFE33D4F8AE::get_offset_of__installerType_2(),
	SubContainerCreatorByNewPrefabWithParams_t7FAF9FE7D9537562FAC1474B20783DFE33D4F8AE::get_offset_of__gameObjectBindInfo_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7487 = { sizeof (U3CU3Ec__DisplayClass7_0_t386BF28AE8520D25ADE3F43C3AA954DBE62E0C82), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7487[1] = 
{
	U3CU3Ec__DisplayClass7_0_t386BF28AE8520D25ADE3F43C3AA954DBE62E0C82::get_offset_of_argPair_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7488 = { sizeof (SubContainerCreatorCached_t8C3674F84167B2FA6DC79C8B318D605610F10EA4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7488[3] = 
{
	SubContainerCreatorCached_t8C3674F84167B2FA6DC79C8B318D605610F10EA4::get_offset_of__subCreator_0(),
	SubContainerCreatorCached_t8C3674F84167B2FA6DC79C8B318D605610F10EA4::get_offset_of__isLookingUp_1(),
	SubContainerCreatorCached_t8C3674F84167B2FA6DC79C8B318D605610F10EA4::get_offset_of__subContainer_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7489 = { sizeof (SubContainerDependencyProvider_t6C320EB6D6F4FBBA052FEAC5771F9985BF1E314D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7489[3] = 
{
	SubContainerDependencyProvider_t6C320EB6D6F4FBBA052FEAC5771F9985BF1E314D::get_offset_of__subContainerCreator_0(),
	SubContainerDependencyProvider_t6C320EB6D6F4FBBA052FEAC5771F9985BF1E314D::get_offset_of__dependencyType_1(),
	SubContainerDependencyProvider_t6C320EB6D6F4FBBA052FEAC5771F9985BF1E314D::get_offset_of__identifier_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7490 = { sizeof (U3CGetAllInstancesWithInjectSplitU3Ed__6_t583E658E9E0C6B896656C9D6FFA54AA575D22D3F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7490[5] = 
{
	U3CGetAllInstancesWithInjectSplitU3Ed__6_t583E658E9E0C6B896656C9D6FFA54AA575D22D3F::get_offset_of_U3CU3E1__state_0(),
	U3CGetAllInstancesWithInjectSplitU3Ed__6_t583E658E9E0C6B896656C9D6FFA54AA575D22D3F::get_offset_of_U3CU3E2__current_1(),
	U3CGetAllInstancesWithInjectSplitU3Ed__6_t583E658E9E0C6B896656C9D6FFA54AA575D22D3F::get_offset_of_context_2(),
	U3CGetAllInstancesWithInjectSplitU3Ed__6_t583E658E9E0C6B896656C9D6FFA54AA575D22D3F::get_offset_of_U3CU3E4__this_3(),
	U3CGetAllInstancesWithInjectSplitU3Ed__6_t583E658E9E0C6B896656C9D6FFA54AA575D22D3F::get_offset_of_args_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7491 = { sizeof (TransientProvider_tAB891005B80A7655BC6103608F2471B9CF7EDA43), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7491[4] = 
{
	TransientProvider_tAB891005B80A7655BC6103608F2471B9CF7EDA43::get_offset_of__container_0(),
	TransientProvider_tAB891005B80A7655BC6103608F2471B9CF7EDA43::get_offset_of__concreteType_1(),
	TransientProvider_tAB891005B80A7655BC6103608F2471B9CF7EDA43::get_offset_of__concreteIdentifier_2(),
	TransientProvider_tAB891005B80A7655BC6103608F2471B9CF7EDA43::get_offset_of__extraArguments_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7492 = { sizeof (U3CGetAllInstancesWithInjectSplitU3Ed__8_tCD537E23ED94D135E58FB2796D0A50E9F8A31F91), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7492[8] = 
{
	U3CGetAllInstancesWithInjectSplitU3Ed__8_tCD537E23ED94D135E58FB2796D0A50E9F8A31F91::get_offset_of_U3CU3E1__state_0(),
	U3CGetAllInstancesWithInjectSplitU3Ed__8_tCD537E23ED94D135E58FB2796D0A50E9F8A31F91::get_offset_of_U3CU3E2__current_1(),
	U3CGetAllInstancesWithInjectSplitU3Ed__8_tCD537E23ED94D135E58FB2796D0A50E9F8A31F91::get_offset_of_context_2(),
	U3CGetAllInstancesWithInjectSplitU3Ed__8_tCD537E23ED94D135E58FB2796D0A50E9F8A31F91::get_offset_of_U3CU3E4__this_3(),
	U3CGetAllInstancesWithInjectSplitU3Ed__8_tCD537E23ED94D135E58FB2796D0A50E9F8A31F91::get_offset_of_args_4(),
	U3CGetAllInstancesWithInjectSplitU3Ed__8_tCD537E23ED94D135E58FB2796D0A50E9F8A31F91::get_offset_of_U3CinstanceTypeU3E5__2_5(),
	U3CGetAllInstancesWithInjectSplitU3Ed__8_tCD537E23ED94D135E58FB2796D0A50E9F8A31F91::get_offset_of_U3CinjectArgsU3E5__3_6(),
	U3CGetAllInstancesWithInjectSplitU3Ed__8_tCD537E23ED94D135E58FB2796D0A50E9F8A31F91::get_offset_of_U3CinstanceU3E5__4_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7493 = { sizeof (UntypedFactoryProvider_tF14A4C3D101D65F49AA291EB4F166F6636C9363C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7493[5] = 
{
	UntypedFactoryProvider_tF14A4C3D101D65F49AA291EB4F166F6636C9363C::get_offset_of__factoryArgs_0(),
	UntypedFactoryProvider_tF14A4C3D101D65F49AA291EB4F166F6636C9363C::get_offset_of__container_1(),
	UntypedFactoryProvider_tF14A4C3D101D65F49AA291EB4F166F6636C9363C::get_offset_of__factoryType_2(),
	UntypedFactoryProvider_tF14A4C3D101D65F49AA291EB4F166F6636C9363C::get_offset_of__concreteType_3(),
	UntypedFactoryProvider_tF14A4C3D101D65F49AA291EB4F166F6636C9363C::get_offset_of__createMethod_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7494 = { sizeof (U3CU3Ec_t51DAA85C4705F1A1D411DAA3F7C8DE03D398AC0A), -1, sizeof(U3CU3Ec_t51DAA85C4705F1A1D411DAA3F7C8DE03D398AC0A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7494[4] = 
{
	U3CU3Ec_t51DAA85C4705F1A1D411DAA3F7C8DE03D398AC0A_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t51DAA85C4705F1A1D411DAA3F7C8DE03D398AC0A_StaticFields::get_offset_of_U3CU3E9__5_0_1(),
	U3CU3Ec_t51DAA85C4705F1A1D411DAA3F7C8DE03D398AC0A_StaticFields::get_offset_of_U3CU3E9__6_0_2(),
	U3CU3Ec_t51DAA85C4705F1A1D411DAA3F7C8DE03D398AC0A_StaticFields::get_offset_of_U3CU3E9__8_0_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7495 = { sizeof (U3CGetAllInstancesWithInjectSplitU3Ed__8_tFCEF3B3C6C9DA17B3EBCC085F1C4E81DB82DEBE9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7495[4] = 
{
	U3CGetAllInstancesWithInjectSplitU3Ed__8_tFCEF3B3C6C9DA17B3EBCC085F1C4E81DB82DEBE9::get_offset_of_U3CU3E1__state_0(),
	U3CGetAllInstancesWithInjectSplitU3Ed__8_tFCEF3B3C6C9DA17B3EBCC085F1C4E81DB82DEBE9::get_offset_of_U3CU3E2__current_1(),
	U3CGetAllInstancesWithInjectSplitU3Ed__8_tFCEF3B3C6C9DA17B3EBCC085F1C4E81DB82DEBE9::get_offset_of_U3CU3E4__this_2(),
	U3CGetAllInstancesWithInjectSplitU3Ed__8_tFCEF3B3C6C9DA17B3EBCC085F1C4E81DB82DEBE9::get_offset_of_args_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7496 = { sizeof (AnimatorIkHandlerManager_t1B475CD68C26B5EB1018D9D2471BEB7FB2E17EEA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7496[1] = 
{
	AnimatorIkHandlerManager_t1B475CD68C26B5EB1018D9D2471BEB7FB2E17EEA::get_offset_of__handlers_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7497 = { sizeof (AnimatorInstaller_t34F3246CE01EC918798E6F5CFAAF904759ACBDFE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7497[1] = 
{
	AnimatorInstaller_t34F3246CE01EC918798E6F5CFAAF904759ACBDFE::get_offset_of__animator_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7498 = { sizeof (AnimatorMoveHandlerManager_tE0FAF65368A36AC536D43E81F5292110EDBC1AD3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7498[1] = 
{
	AnimatorMoveHandlerManager_tE0FAF65368A36AC536D43E81F5292110EDBC1AD3::get_offset_of__handlers_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7499 = { 0, -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
