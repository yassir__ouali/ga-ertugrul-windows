﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// IngameDebugConsole.DebugLogEntry
struct DebugLogEntry_t2A901F25FD9B697BBC7B6864E19EC3E8F2902D57;
// IngameDebugConsole.DebugLogIndexList
struct DebugLogIndexList_t1A17DF0528FD925924AE4E53ABCE40AAE8699A88;
// IngameDebugConsole.DebugLogItem
struct DebugLogItem_t706D6712831B240C3EA4B655657EE6EB2F37461C;
// IngameDebugConsole.DebugLogManager
struct DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0;
// IngameDebugConsole.DebugLogPopup
struct DebugLogPopup_t9A2F2B314079CC1B8AE1CBA454189A6FB15C82A6;
// IngameDebugConsole.DebugLogRecycledListView
struct DebugLogRecycledListView_t63DA7AF9FA87FF739B5A9AC577D26D9792A809BC;
// Svelto.DataStructures.FasterList`1<Svelto.Tasks.IPausableTask>
struct FasterList_1_t2B537C5D221BBAAC4DA80F69541C173562A8478A;
// Svelto.DataStructures.FasterList`1<Svelto.WeakEvents.WeakActionBase>
struct FasterList_1_tB3C3C1935A0123A0EDD521DBC185C5105946FC3A;
// Svelto.DataStructures.FasterList`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Reflection.MethodInfo>>
struct FasterList_1_tE5B3474E20F6FF2530A614938F13E1BDB218CAAD;
// Svelto.DataStructures.ThreadSafeQueue`1<Svelto.Tasks.IPausableTask>
struct ThreadSafeQueue_1_t4B42DDB151ED238066A071C89AF1FC3DB9C1BE45;
// Svelto.DataStructures.WeakReference`1<System.Object>
struct WeakReference_1_t5DACCF0D2BA7734A55F78BBDFF0C9CB82EF5BE76;
// Svelto.Observer.Observable
struct Observable_tA31B4EE4C7F2E1CDA6FBB36ADBE6163EB0AB76FC;
// Svelto.Observer.Observer
struct Observer_tC137025318FE81433C99B6FF9523DE3DE2140EFF;
// Svelto.Tasks.IPausableTask
struct IPausableTask_t6D0A8B26571133F33371D87AC0068005C3F15100;
// Svelto.Tasks.Internal.RunnerBehaviour
struct RunnerBehaviour_t9AE6F5864AD5F25FE25A18838BC70B25D4B70DFD;
// Svelto.Tasks.Internal.UnityCoroutineRunner/<>c__DisplayClass4_0
struct U3CU3Ec__DisplayClass4_0_t8719BF1E8CBECF96E1F5A6F6279B4F159FD0E905;
// Svelto.Tasks.Internal.UnityCoroutineRunner/FlushTasksDel
struct FlushTasksDel_t5FECFC8BE91DD02043575279E76105CB8643E11D;
// Svelto.Tasks.Internal.UnityCoroutineRunner/FlushingOperation
struct FlushingOperation_tB0B9CC8E029067E8D130896A68A0E2D390E01027;
// Svelto.Tasks.Internal.UnityCoroutineRunner/RunningTasksInfo
struct RunningTasksInfo_t10813DB30B3A26BF8CF0830C17B2B3EE5DBF523C;
// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.Action`1<Svelto.Command.ICommand>
struct Action_1_t97FF407C48ACDF832088FD5EC76000FE0398C06C;
// System.Action`1<Svelto.Tasks.IPausableTask>
struct Action_1_t07F8054E833F5BFAFF0CC0B5AE1F3B8E44083838;
// System.Action`1<System.Object>
struct Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<IngameDebugConsole.DebugLogEntry,System.Int32>
struct Dictionary_2_tF20E30CEE56A9CE9AE202E95FBCC1E78C6C197F4;
// System.Collections.Generic.Dictionary`2<System.Int32,IngameDebugConsole.DebugLogItem>
struct Dictionary_2_t07C326FB9D8FDFAE1C31A75FEFEC22C9C4FAAC93;
// System.Collections.Generic.Dictionary`2<System.String,IngameDebugConsole.ConsoleMethodInfo>
struct Dictionary_2_tC1A8E4B0574EBEFD6FB25434EC7F75514E6691D8;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject[]>
struct Dictionary_2_tF92064FBE7A01E818CAD17DB69A521E9B57AF3EF;
// System.Collections.Generic.Dictionary`2<System.Type,IngameDebugConsole.DebugLogConsole/ParseFunction>
struct Dictionary_2_t9D9A08C9B375914BAD4EE6FBEFA5C575592C2247;
// System.Collections.Generic.Dictionary`2<System.Type,System.String>
struct Dictionary_2_t325762390EC6A243BD7A037A815D001885C77299;
// System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Sprite>
struct Dictionary_2_t48903AD17CC355C8DD3F85A6DC8A554A7258618D;
// System.Collections.Generic.List`1<IngameDebugConsole.DebugLogEntry>
struct List_1_t1F5E087D9E9D3F4AEFC96C24F5FC1F52B4B0D9E8;
// System.Collections.Generic.List`1<IngameDebugConsole.DebugLogItem>
struct List_1_t69EF89DD0DB7FCEDA221ACEC0DD5849BABB52566;
// System.Collections.Generic.List`1<Svelto.DataStructures.WeakReference`1<Svelto.Context.IWaitForFrameworkDestruction>>
struct List_1_t6F93A2A9508AE5C113891005CC5D94B4239026AB;
// System.Collections.Generic.List`1<Svelto.DataStructures.WeakReference`1<Svelto.Context.IWaitForFrameworkInitialization>>
struct List_1_tCFB327014C70052ABB1944AFCB0DC78D7B9A2C77;
// System.Collections.Generic.List`1<System.String>
struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3;
// System.Collections.IEnumerator
struct IEnumerator_t8789118187258CC88B77AFAC6315B5AF87D3E18A;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Exception
struct Exception_t;
// System.Func`1<System.Boolean>
struct Func_1_t4ABD6DAD480574F152452DD6B9C9A55F4F6655F1;
// System.Func`2<Svelto.WeakEvents.WeakActionBase,System.Boolean>
struct Func_2_tDEA274607343466FE9B7687D6EC0431560D455E1;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Threading.ManualResetEvent
struct ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408;
// System.Type[]
struct TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.AsyncOperation
struct AsyncOperation_t304C51ABED8AE734CC8DDDFE13013D8D5A44641D;
// UnityEngine.CanvasGroup
struct CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90;
// UnityEngine.Coroutine
struct Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129;
// UnityEngine.RectTransform
struct RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20;
// UnityEngine.Sprite
struct Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198;
// UnityEngine.UI.Image
struct Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E;
// UnityEngine.UI.InputField
struct InputField_t533609195B110760BCFF00B746C87D81969CB005;
// UnityEngine.UI.ScrollRect
struct ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51;
// UnityEngine.UI.Text
struct Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030;
// UnityEngine.WWW
struct WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664;

struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef CONSOLEMETHODINFO_TF7C43BA80D45911840FECD796D2C33EA4A097739_H
#define CONSOLEMETHODINFO_TF7C43BA80D45911840FECD796D2C33EA4A097739_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IngameDebugConsole.ConsoleMethodInfo
struct  ConsoleMethodInfo_tF7C43BA80D45911840FECD796D2C33EA4A097739  : public RuntimeObject
{
public:
	// System.Reflection.MethodInfo IngameDebugConsole.ConsoleMethodInfo::method
	MethodInfo_t * ___method_0;
	// System.Type[] IngameDebugConsole.ConsoleMethodInfo::parameterTypes
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___parameterTypes_1;
	// System.Object IngameDebugConsole.ConsoleMethodInfo::instance
	RuntimeObject * ___instance_2;
	// System.String IngameDebugConsole.ConsoleMethodInfo::signature
	String_t* ___signature_3;

public:
	inline static int32_t get_offset_of_method_0() { return static_cast<int32_t>(offsetof(ConsoleMethodInfo_tF7C43BA80D45911840FECD796D2C33EA4A097739, ___method_0)); }
	inline MethodInfo_t * get_method_0() const { return ___method_0; }
	inline MethodInfo_t ** get_address_of_method_0() { return &___method_0; }
	inline void set_method_0(MethodInfo_t * value)
	{
		___method_0 = value;
		Il2CppCodeGenWriteBarrier((&___method_0), value);
	}

	inline static int32_t get_offset_of_parameterTypes_1() { return static_cast<int32_t>(offsetof(ConsoleMethodInfo_tF7C43BA80D45911840FECD796D2C33EA4A097739, ___parameterTypes_1)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get_parameterTypes_1() const { return ___parameterTypes_1; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of_parameterTypes_1() { return &___parameterTypes_1; }
	inline void set_parameterTypes_1(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		___parameterTypes_1 = value;
		Il2CppCodeGenWriteBarrier((&___parameterTypes_1), value);
	}

	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(ConsoleMethodInfo_tF7C43BA80D45911840FECD796D2C33EA4A097739, ___instance_2)); }
	inline RuntimeObject * get_instance_2() const { return ___instance_2; }
	inline RuntimeObject ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(RuntimeObject * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}

	inline static int32_t get_offset_of_signature_3() { return static_cast<int32_t>(offsetof(ConsoleMethodInfo_tF7C43BA80D45911840FECD796D2C33EA4A097739, ___signature_3)); }
	inline String_t* get_signature_3() const { return ___signature_3; }
	inline String_t** get_address_of_signature_3() { return &___signature_3; }
	inline void set_signature_3(String_t* value)
	{
		___signature_3 = value;
		Il2CppCodeGenWriteBarrier((&___signature_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSOLEMETHODINFO_TF7C43BA80D45911840FECD796D2C33EA4A097739_H
#ifndef DEBUGLOGCONSOLE_T78D45C78CF71C30B964B789B9BE46D3F6991DAC2_H
#define DEBUGLOGCONSOLE_T78D45C78CF71C30B964B789B9BE46D3F6991DAC2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IngameDebugConsole.DebugLogConsole
struct  DebugLogConsole_t78D45C78CF71C30B964B789B9BE46D3F6991DAC2  : public RuntimeObject
{
public:

public:
};

struct DebugLogConsole_t78D45C78CF71C30B964B789B9BE46D3F6991DAC2_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,IngameDebugConsole.ConsoleMethodInfo> IngameDebugConsole.DebugLogConsole::methods
	Dictionary_2_tC1A8E4B0574EBEFD6FB25434EC7F75514E6691D8 * ___methods_0;
	// System.Collections.Generic.Dictionary`2<System.Type,IngameDebugConsole.DebugLogConsole_ParseFunction> IngameDebugConsole.DebugLogConsole::parseFunctions
	Dictionary_2_t9D9A08C9B375914BAD4EE6FBEFA5C575592C2247 * ___parseFunctions_1;
	// System.Collections.Generic.Dictionary`2<System.Type,System.String> IngameDebugConsole.DebugLogConsole::typeReadableNames
	Dictionary_2_t325762390EC6A243BD7A037A815D001885C77299 * ___typeReadableNames_2;
	// System.Collections.Generic.List`1<System.String> IngameDebugConsole.DebugLogConsole::commandArguments
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___commandArguments_3;
	// System.String[] IngameDebugConsole.DebugLogConsole::inputDelimiters
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___inputDelimiters_4;

public:
	inline static int32_t get_offset_of_methods_0() { return static_cast<int32_t>(offsetof(DebugLogConsole_t78D45C78CF71C30B964B789B9BE46D3F6991DAC2_StaticFields, ___methods_0)); }
	inline Dictionary_2_tC1A8E4B0574EBEFD6FB25434EC7F75514E6691D8 * get_methods_0() const { return ___methods_0; }
	inline Dictionary_2_tC1A8E4B0574EBEFD6FB25434EC7F75514E6691D8 ** get_address_of_methods_0() { return &___methods_0; }
	inline void set_methods_0(Dictionary_2_tC1A8E4B0574EBEFD6FB25434EC7F75514E6691D8 * value)
	{
		___methods_0 = value;
		Il2CppCodeGenWriteBarrier((&___methods_0), value);
	}

	inline static int32_t get_offset_of_parseFunctions_1() { return static_cast<int32_t>(offsetof(DebugLogConsole_t78D45C78CF71C30B964B789B9BE46D3F6991DAC2_StaticFields, ___parseFunctions_1)); }
	inline Dictionary_2_t9D9A08C9B375914BAD4EE6FBEFA5C575592C2247 * get_parseFunctions_1() const { return ___parseFunctions_1; }
	inline Dictionary_2_t9D9A08C9B375914BAD4EE6FBEFA5C575592C2247 ** get_address_of_parseFunctions_1() { return &___parseFunctions_1; }
	inline void set_parseFunctions_1(Dictionary_2_t9D9A08C9B375914BAD4EE6FBEFA5C575592C2247 * value)
	{
		___parseFunctions_1 = value;
		Il2CppCodeGenWriteBarrier((&___parseFunctions_1), value);
	}

	inline static int32_t get_offset_of_typeReadableNames_2() { return static_cast<int32_t>(offsetof(DebugLogConsole_t78D45C78CF71C30B964B789B9BE46D3F6991DAC2_StaticFields, ___typeReadableNames_2)); }
	inline Dictionary_2_t325762390EC6A243BD7A037A815D001885C77299 * get_typeReadableNames_2() const { return ___typeReadableNames_2; }
	inline Dictionary_2_t325762390EC6A243BD7A037A815D001885C77299 ** get_address_of_typeReadableNames_2() { return &___typeReadableNames_2; }
	inline void set_typeReadableNames_2(Dictionary_2_t325762390EC6A243BD7A037A815D001885C77299 * value)
	{
		___typeReadableNames_2 = value;
		Il2CppCodeGenWriteBarrier((&___typeReadableNames_2), value);
	}

	inline static int32_t get_offset_of_commandArguments_3() { return static_cast<int32_t>(offsetof(DebugLogConsole_t78D45C78CF71C30B964B789B9BE46D3F6991DAC2_StaticFields, ___commandArguments_3)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_commandArguments_3() const { return ___commandArguments_3; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_commandArguments_3() { return &___commandArguments_3; }
	inline void set_commandArguments_3(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___commandArguments_3 = value;
		Il2CppCodeGenWriteBarrier((&___commandArguments_3), value);
	}

	inline static int32_t get_offset_of_inputDelimiters_4() { return static_cast<int32_t>(offsetof(DebugLogConsole_t78D45C78CF71C30B964B789B9BE46D3F6991DAC2_StaticFields, ___inputDelimiters_4)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_inputDelimiters_4() const { return ___inputDelimiters_4; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_inputDelimiters_4() { return &___inputDelimiters_4; }
	inline void set_inputDelimiters_4(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___inputDelimiters_4 = value;
		Il2CppCodeGenWriteBarrier((&___inputDelimiters_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGLOGCONSOLE_T78D45C78CF71C30B964B789B9BE46D3F6991DAC2_H
#ifndef DEBUGLOGENTRY_T2A901F25FD9B697BBC7B6864E19EC3E8F2902D57_H
#define DEBUGLOGENTRY_T2A901F25FD9B697BBC7B6864E19EC3E8F2902D57_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IngameDebugConsole.DebugLogEntry
struct  DebugLogEntry_t2A901F25FD9B697BBC7B6864E19EC3E8F2902D57  : public RuntimeObject
{
public:
	// System.String IngameDebugConsole.DebugLogEntry::logString
	String_t* ___logString_1;
	// System.String IngameDebugConsole.DebugLogEntry::stackTrace
	String_t* ___stackTrace_2;
	// System.String IngameDebugConsole.DebugLogEntry::completeLog
	String_t* ___completeLog_3;
	// UnityEngine.Sprite IngameDebugConsole.DebugLogEntry::logTypeSpriteRepresentation
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___logTypeSpriteRepresentation_4;
	// System.Int32 IngameDebugConsole.DebugLogEntry::count
	int32_t ___count_5;
	// System.Int32 IngameDebugConsole.DebugLogEntry::hashValue
	int32_t ___hashValue_6;

public:
	inline static int32_t get_offset_of_logString_1() { return static_cast<int32_t>(offsetof(DebugLogEntry_t2A901F25FD9B697BBC7B6864E19EC3E8F2902D57, ___logString_1)); }
	inline String_t* get_logString_1() const { return ___logString_1; }
	inline String_t** get_address_of_logString_1() { return &___logString_1; }
	inline void set_logString_1(String_t* value)
	{
		___logString_1 = value;
		Il2CppCodeGenWriteBarrier((&___logString_1), value);
	}

	inline static int32_t get_offset_of_stackTrace_2() { return static_cast<int32_t>(offsetof(DebugLogEntry_t2A901F25FD9B697BBC7B6864E19EC3E8F2902D57, ___stackTrace_2)); }
	inline String_t* get_stackTrace_2() const { return ___stackTrace_2; }
	inline String_t** get_address_of_stackTrace_2() { return &___stackTrace_2; }
	inline void set_stackTrace_2(String_t* value)
	{
		___stackTrace_2 = value;
		Il2CppCodeGenWriteBarrier((&___stackTrace_2), value);
	}

	inline static int32_t get_offset_of_completeLog_3() { return static_cast<int32_t>(offsetof(DebugLogEntry_t2A901F25FD9B697BBC7B6864E19EC3E8F2902D57, ___completeLog_3)); }
	inline String_t* get_completeLog_3() const { return ___completeLog_3; }
	inline String_t** get_address_of_completeLog_3() { return &___completeLog_3; }
	inline void set_completeLog_3(String_t* value)
	{
		___completeLog_3 = value;
		Il2CppCodeGenWriteBarrier((&___completeLog_3), value);
	}

	inline static int32_t get_offset_of_logTypeSpriteRepresentation_4() { return static_cast<int32_t>(offsetof(DebugLogEntry_t2A901F25FD9B697BBC7B6864E19EC3E8F2902D57, ___logTypeSpriteRepresentation_4)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_logTypeSpriteRepresentation_4() const { return ___logTypeSpriteRepresentation_4; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_logTypeSpriteRepresentation_4() { return &___logTypeSpriteRepresentation_4; }
	inline void set_logTypeSpriteRepresentation_4(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___logTypeSpriteRepresentation_4 = value;
		Il2CppCodeGenWriteBarrier((&___logTypeSpriteRepresentation_4), value);
	}

	inline static int32_t get_offset_of_count_5() { return static_cast<int32_t>(offsetof(DebugLogEntry_t2A901F25FD9B697BBC7B6864E19EC3E8F2902D57, ___count_5)); }
	inline int32_t get_count_5() const { return ___count_5; }
	inline int32_t* get_address_of_count_5() { return &___count_5; }
	inline void set_count_5(int32_t value)
	{
		___count_5 = value;
	}

	inline static int32_t get_offset_of_hashValue_6() { return static_cast<int32_t>(offsetof(DebugLogEntry_t2A901F25FD9B697BBC7B6864E19EC3E8F2902D57, ___hashValue_6)); }
	inline int32_t get_hashValue_6() const { return ___hashValue_6; }
	inline int32_t* get_address_of_hashValue_6() { return &___hashValue_6; }
	inline void set_hashValue_6(int32_t value)
	{
		___hashValue_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGLOGENTRY_T2A901F25FD9B697BBC7B6864E19EC3E8F2902D57_H
#ifndef DEBUGLOGINDEXLIST_T1A17DF0528FD925924AE4E53ABCE40AAE8699A88_H
#define DEBUGLOGINDEXLIST_T1A17DF0528FD925924AE4E53ABCE40AAE8699A88_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IngameDebugConsole.DebugLogIndexList
struct  DebugLogIndexList_t1A17DF0528FD925924AE4E53ABCE40AAE8699A88  : public RuntimeObject
{
public:
	// System.Int32[] IngameDebugConsole.DebugLogIndexList::indices
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___indices_0;
	// System.Int32 IngameDebugConsole.DebugLogIndexList::size
	int32_t ___size_1;

public:
	inline static int32_t get_offset_of_indices_0() { return static_cast<int32_t>(offsetof(DebugLogIndexList_t1A17DF0528FD925924AE4E53ABCE40AAE8699A88, ___indices_0)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_indices_0() const { return ___indices_0; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_indices_0() { return &___indices_0; }
	inline void set_indices_0(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___indices_0 = value;
		Il2CppCodeGenWriteBarrier((&___indices_0), value);
	}

	inline static int32_t get_offset_of_size_1() { return static_cast<int32_t>(offsetof(DebugLogIndexList_t1A17DF0528FD925924AE4E53ABCE40AAE8699A88, ___size_1)); }
	inline int32_t get_size_1() const { return ___size_1; }
	inline int32_t* get_address_of_size_1() { return &___size_1; }
	inline void set_size_1(int32_t value)
	{
		___size_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGLOGINDEXLIST_T1A17DF0528FD925924AE4E53ABCE40AAE8699A88_H
#ifndef COMMANDFACTORY_TE2D2B5AB0FD6859EC22401A958B0F12DD34CB5BE_H
#define COMMANDFACTORY_TE2D2B5AB0FD6859EC22401A958B0F12DD34CB5BE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Svelto.Command.CommandFactory
struct  CommandFactory_tE2D2B5AB0FD6859EC22401A958B0F12DD34CB5BE  : public RuntimeObject
{
public:
	// System.Action`1<Svelto.Command.ICommand> Svelto.Command.CommandFactory::_onNewCommand
	Action_1_t97FF407C48ACDF832088FD5EC76000FE0398C06C * ____onNewCommand_0;

public:
	inline static int32_t get_offset_of__onNewCommand_0() { return static_cast<int32_t>(offsetof(CommandFactory_tE2D2B5AB0FD6859EC22401A958B0F12DD34CB5BE, ____onNewCommand_0)); }
	inline Action_1_t97FF407C48ACDF832088FD5EC76000FE0398C06C * get__onNewCommand_0() const { return ____onNewCommand_0; }
	inline Action_1_t97FF407C48ACDF832088FD5EC76000FE0398C06C ** get_address_of__onNewCommand_0() { return &____onNewCommand_0; }
	inline void set__onNewCommand_0(Action_1_t97FF407C48ACDF832088FD5EC76000FE0398C06C * value)
	{
		____onNewCommand_0 = value;
		Il2CppCodeGenWriteBarrier((&____onNewCommand_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMMANDFACTORY_TE2D2B5AB0FD6859EC22401A958B0F12DD34CB5BE_H
#ifndef CONTEXTNOTIFIER_T042B33F0149C6FB6A14DB1ADA08C4E865363B815_H
#define CONTEXTNOTIFIER_T042B33F0149C6FB6A14DB1ADA08C4E865363B815_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Svelto.Context.ContextNotifier
struct  ContextNotifier_t042B33F0149C6FB6A14DB1ADA08C4E865363B815  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Svelto.DataStructures.WeakReference`1<Svelto.Context.IWaitForFrameworkDestruction>> Svelto.Context.ContextNotifier::_toDeinitialize
	List_1_t6F93A2A9508AE5C113891005CC5D94B4239026AB * ____toDeinitialize_0;
	// System.Collections.Generic.List`1<Svelto.DataStructures.WeakReference`1<Svelto.Context.IWaitForFrameworkInitialization>> Svelto.Context.ContextNotifier::_toInitialize
	List_1_tCFB327014C70052ABB1944AFCB0DC78D7B9A2C77 * ____toInitialize_1;

public:
	inline static int32_t get_offset_of__toDeinitialize_0() { return static_cast<int32_t>(offsetof(ContextNotifier_t042B33F0149C6FB6A14DB1ADA08C4E865363B815, ____toDeinitialize_0)); }
	inline List_1_t6F93A2A9508AE5C113891005CC5D94B4239026AB * get__toDeinitialize_0() const { return ____toDeinitialize_0; }
	inline List_1_t6F93A2A9508AE5C113891005CC5D94B4239026AB ** get_address_of__toDeinitialize_0() { return &____toDeinitialize_0; }
	inline void set__toDeinitialize_0(List_1_t6F93A2A9508AE5C113891005CC5D94B4239026AB * value)
	{
		____toDeinitialize_0 = value;
		Il2CppCodeGenWriteBarrier((&____toDeinitialize_0), value);
	}

	inline static int32_t get_offset_of__toInitialize_1() { return static_cast<int32_t>(offsetof(ContextNotifier_t042B33F0149C6FB6A14DB1ADA08C4E865363B815, ____toInitialize_1)); }
	inline List_1_tCFB327014C70052ABB1944AFCB0DC78D7B9A2C77 * get__toInitialize_1() const { return ____toInitialize_1; }
	inline List_1_tCFB327014C70052ABB1944AFCB0DC78D7B9A2C77 ** get_address_of__toInitialize_1() { return &____toInitialize_1; }
	inline void set__toInitialize_1(List_1_tCFB327014C70052ABB1944AFCB0DC78D7B9A2C77 * value)
	{
		____toInitialize_1 = value;
		Il2CppCodeGenWriteBarrier((&____toInitialize_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTEXTNOTIFIER_T042B33F0149C6FB6A14DB1ADA08C4E865363B815_H
#ifndef GAMEOBJECTFACTORY_T0DE860347372F37B4150A4AA1509CA7E8334CD97_H
#define GAMEOBJECTFACTORY_T0DE860347372F37B4150A4AA1509CA7E8334CD97_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Svelto.Context.GameObjectFactory
struct  GameObjectFactory_t0DE860347372F37B4150A4AA1509CA7E8334CD97  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject[]> Svelto.Context.GameObjectFactory::_prefabs
	Dictionary_2_tF92064FBE7A01E818CAD17DB69A521E9B57AF3EF * ____prefabs_0;

public:
	inline static int32_t get_offset_of__prefabs_0() { return static_cast<int32_t>(offsetof(GameObjectFactory_t0DE860347372F37B4150A4AA1509CA7E8334CD97, ____prefabs_0)); }
	inline Dictionary_2_tF92064FBE7A01E818CAD17DB69A521E9B57AF3EF * get__prefabs_0() const { return ____prefabs_0; }
	inline Dictionary_2_tF92064FBE7A01E818CAD17DB69A521E9B57AF3EF ** get_address_of__prefabs_0() { return &____prefabs_0; }
	inline void set__prefabs_0(Dictionary_2_tF92064FBE7A01E818CAD17DB69A521E9B57AF3EF * value)
	{
		____prefabs_0 = value;
		Il2CppCodeGenWriteBarrier((&____prefabs_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECTFACTORY_T0DE860347372F37B4150A4AA1509CA7E8334CD97_H
#ifndef MONOBEHAVIOURFACTORY_TEFD42E36CAE52E5FA9BE40F7155339E865CBF256_H
#define MONOBEHAVIOURFACTORY_TEFD42E36CAE52E5FA9BE40F7155339E865CBF256_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Svelto.Context.MonoBehaviourFactory
struct  MonoBehaviourFactory_tEFD42E36CAE52E5FA9BE40F7155339E865CBF256  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOURFACTORY_TEFD42E36CAE52E5FA9BE40F7155339E865CBF256_H
#ifndef SYNCMETHODS_TB7BAD7FA55C26D933D6837C1F920DA0978228397_H
#define SYNCMETHODS_TB7BAD7FA55C26D933D6837C1F920DA0978228397_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Svelto.DataStructures.SyncMethods
struct  SyncMethods_tB7BAD7FA55C26D933D6837C1F920DA0978228397  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYNCMETHODS_TB7BAD7FA55C26D933D6837C1F920DA0978228397_H
#ifndef OBSERVABLE_TA31B4EE4C7F2E1CDA6FBB36ADBE6163EB0AB76FC_H
#define OBSERVABLE_TA31B4EE4C7F2E1CDA6FBB36ADBE6163EB0AB76FC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Svelto.Observer.Observable
struct  Observable_tA31B4EE4C7F2E1CDA6FBB36ADBE6163EB0AB76FC  : public RuntimeObject
{
public:
	// System.Action Svelto.Observer.Observable::Notify
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___Notify_0;

public:
	inline static int32_t get_offset_of_Notify_0() { return static_cast<int32_t>(offsetof(Observable_tA31B4EE4C7F2E1CDA6FBB36ADBE6163EB0AB76FC, ___Notify_0)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_Notify_0() const { return ___Notify_0; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_Notify_0() { return &___Notify_0; }
	inline void set_Notify_0(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___Notify_0 = value;
		Il2CppCodeGenWriteBarrier((&___Notify_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBSERVABLE_TA31B4EE4C7F2E1CDA6FBB36ADBE6163EB0AB76FC_H
#ifndef OBSERVER_TC137025318FE81433C99B6FF9523DE3DE2140EFF_H
#define OBSERVER_TC137025318FE81433C99B6FF9523DE3DE2140EFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Svelto.Observer.Observer
struct  Observer_tC137025318FE81433C99B6FF9523DE3DE2140EFF  : public RuntimeObject
{
public:
	// System.Action Svelto.Observer.Observer::_actions
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ____actions_0;
	// System.Action Svelto.Observer.Observer::_unsubscribe
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ____unsubscribe_1;

public:
	inline static int32_t get_offset_of__actions_0() { return static_cast<int32_t>(offsetof(Observer_tC137025318FE81433C99B6FF9523DE3DE2140EFF, ____actions_0)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get__actions_0() const { return ____actions_0; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of__actions_0() { return &____actions_0; }
	inline void set__actions_0(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		____actions_0 = value;
		Il2CppCodeGenWriteBarrier((&____actions_0), value);
	}

	inline static int32_t get_offset_of__unsubscribe_1() { return static_cast<int32_t>(offsetof(Observer_tC137025318FE81433C99B6FF9523DE3DE2140EFF, ____unsubscribe_1)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get__unsubscribe_1() const { return ____unsubscribe_1; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of__unsubscribe_1() { return &____unsubscribe_1; }
	inline void set__unsubscribe_1(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		____unsubscribe_1 = value;
		Il2CppCodeGenWriteBarrier((&____unsubscribe_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBSERVER_TC137025318FE81433C99B6FF9523DE3DE2140EFF_H
#ifndef U3CU3EC__DISPLAYCLASS0_0_T84601093A17D3002AB75354B1D0ECBD2875DB45C_H
#define U3CU3EC__DISPLAYCLASS0_0_T84601093A17D3002AB75354B1D0ECBD2875DB45C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Svelto.Observer.Observer_<>c__DisplayClass0_0
struct  U3CU3Ec__DisplayClass0_0_t84601093A17D3002AB75354B1D0ECBD2875DB45C  : public RuntimeObject
{
public:
	// Svelto.Observer.Observable Svelto.Observer.Observer_<>c__DisplayClass0_0::observable
	Observable_tA31B4EE4C7F2E1CDA6FBB36ADBE6163EB0AB76FC * ___observable_0;
	// Svelto.Observer.Observer Svelto.Observer.Observer_<>c__DisplayClass0_0::<>4__this
	Observer_tC137025318FE81433C99B6FF9523DE3DE2140EFF * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_observable_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass0_0_t84601093A17D3002AB75354B1D0ECBD2875DB45C, ___observable_0)); }
	inline Observable_tA31B4EE4C7F2E1CDA6FBB36ADBE6163EB0AB76FC * get_observable_0() const { return ___observable_0; }
	inline Observable_tA31B4EE4C7F2E1CDA6FBB36ADBE6163EB0AB76FC ** get_address_of_observable_0() { return &___observable_0; }
	inline void set_observable_0(Observable_tA31B4EE4C7F2E1CDA6FBB36ADBE6163EB0AB76FC * value)
	{
		___observable_0 = value;
		Il2CppCodeGenWriteBarrier((&___observable_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass0_0_t84601093A17D3002AB75354B1D0ECBD2875DB45C, ___U3CU3E4__this_1)); }
	inline Observer_tC137025318FE81433C99B6FF9523DE3DE2140EFF * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline Observer_tC137025318FE81433C99B6FF9523DE3DE2140EFF ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(Observer_tC137025318FE81433C99B6FF9523DE3DE2140EFF * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS0_0_T84601093A17D3002AB75354B1D0ECBD2875DB45C_H
#ifndef EXCEPTIONHANDLINGENUMERATOR_TD302C26A89CC82D861B04FEAE9924EC35FDBB8A7_H
#define EXCEPTIONHANDLINGENUMERATOR_TD302C26A89CC82D861B04FEAE9924EC35FDBB8A7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Svelto.Tasks.Enumerators.ExceptionHandlingEnumerator
struct  ExceptionHandlingEnumerator_tD302C26A89CC82D861B04FEAE9924EC35FDBB8A7  : public RuntimeObject
{
public:
	// System.Boolean Svelto.Tasks.Enumerators.ExceptionHandlingEnumerator::<succeeded>k__BackingField
	bool ___U3CsucceededU3Ek__BackingField_0;
	// System.Exception Svelto.Tasks.Enumerators.ExceptionHandlingEnumerator::<error>k__BackingField
	Exception_t * ___U3CerrorU3Ek__BackingField_1;
	// System.Collections.IEnumerator Svelto.Tasks.Enumerators.ExceptionHandlingEnumerator::_enumerator
	RuntimeObject* ____enumerator_2;

public:
	inline static int32_t get_offset_of_U3CsucceededU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ExceptionHandlingEnumerator_tD302C26A89CC82D861B04FEAE9924EC35FDBB8A7, ___U3CsucceededU3Ek__BackingField_0)); }
	inline bool get_U3CsucceededU3Ek__BackingField_0() const { return ___U3CsucceededU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CsucceededU3Ek__BackingField_0() { return &___U3CsucceededU3Ek__BackingField_0; }
	inline void set_U3CsucceededU3Ek__BackingField_0(bool value)
	{
		___U3CsucceededU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CerrorU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ExceptionHandlingEnumerator_tD302C26A89CC82D861B04FEAE9924EC35FDBB8A7, ___U3CerrorU3Ek__BackingField_1)); }
	inline Exception_t * get_U3CerrorU3Ek__BackingField_1() const { return ___U3CerrorU3Ek__BackingField_1; }
	inline Exception_t ** get_address_of_U3CerrorU3Ek__BackingField_1() { return &___U3CerrorU3Ek__BackingField_1; }
	inline void set_U3CerrorU3Ek__BackingField_1(Exception_t * value)
	{
		___U3CerrorU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CerrorU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of__enumerator_2() { return static_cast<int32_t>(offsetof(ExceptionHandlingEnumerator_tD302C26A89CC82D861B04FEAE9924EC35FDBB8A7, ____enumerator_2)); }
	inline RuntimeObject* get__enumerator_2() const { return ____enumerator_2; }
	inline RuntimeObject** get_address_of__enumerator_2() { return &____enumerator_2; }
	inline void set__enumerator_2(RuntimeObject* value)
	{
		____enumerator_2 = value;
		Il2CppCodeGenWriteBarrier((&____enumerator_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTIONHANDLINGENUMERATOR_TD302C26A89CC82D861B04FEAE9924EC35FDBB8A7_H
#ifndef UNITYASYNCOPERATIONENUMERATOR_T99BD541CC02B7C4704318292F5640C95376055E2_H
#define UNITYASYNCOPERATIONENUMERATOR_T99BD541CC02B7C4704318292F5640C95376055E2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Svelto.Tasks.Enumerators.UnityAsyncOperationEnumerator
struct  UnityAsyncOperationEnumerator_t99BD541CC02B7C4704318292F5640C95376055E2  : public RuntimeObject
{
public:
	// UnityEngine.AsyncOperation Svelto.Tasks.Enumerators.UnityAsyncOperationEnumerator::_asyncOp
	AsyncOperation_t304C51ABED8AE734CC8DDDFE13013D8D5A44641D * ____asyncOp_0;

public:
	inline static int32_t get_offset_of__asyncOp_0() { return static_cast<int32_t>(offsetof(UnityAsyncOperationEnumerator_t99BD541CC02B7C4704318292F5640C95376055E2, ____asyncOp_0)); }
	inline AsyncOperation_t304C51ABED8AE734CC8DDDFE13013D8D5A44641D * get__asyncOp_0() const { return ____asyncOp_0; }
	inline AsyncOperation_t304C51ABED8AE734CC8DDDFE13013D8D5A44641D ** get_address_of__asyncOp_0() { return &____asyncOp_0; }
	inline void set__asyncOp_0(AsyncOperation_t304C51ABED8AE734CC8DDDFE13013D8D5A44641D * value)
	{
		____asyncOp_0 = value;
		Il2CppCodeGenWriteBarrier((&____asyncOp_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYASYNCOPERATIONENUMERATOR_T99BD541CC02B7C4704318292F5640C95376055E2_H
#ifndef UNITYWEBREQUESTENUMERATOR_TFA04140CD01C06776B29AC3B069DE49AA20DE9B5_H
#define UNITYWEBREQUESTENUMERATOR_TFA04140CD01C06776B29AC3B069DE49AA20DE9B5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Svelto.Tasks.Enumerators.UnityWebRequestEnumerator
struct  UnityWebRequestEnumerator_tFA04140CD01C06776B29AC3B069DE49AA20DE9B5  : public RuntimeObject
{
public:
	// UnityEngine.Networking.UnityWebRequest Svelto.Tasks.Enumerators.UnityWebRequestEnumerator::_www
	UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * ____www_0;

public:
	inline static int32_t get_offset_of__www_0() { return static_cast<int32_t>(offsetof(UnityWebRequestEnumerator_tFA04140CD01C06776B29AC3B069DE49AA20DE9B5, ____www_0)); }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * get__www_0() const { return ____www_0; }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 ** get_address_of__www_0() { return &____www_0; }
	inline void set__www_0(UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * value)
	{
		____www_0 = value;
		Il2CppCodeGenWriteBarrier((&____www_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYWEBREQUESTENUMERATOR_TFA04140CD01C06776B29AC3B069DE49AA20DE9B5_H
#ifndef UNITYCOROUTINERUNNER_T53316F54C9A4F7B0A4B98DD4AC3C3A95DED8C644_H
#define UNITYCOROUTINERUNNER_T53316F54C9A4F7B0A4B98DD4AC3C3A95DED8C644_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Svelto.Tasks.Internal.UnityCoroutineRunner
struct  UnityCoroutineRunner_t53316F54C9A4F7B0A4B98DD4AC3C3A95DED8C644  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYCOROUTINERUNNER_T53316F54C9A4F7B0A4B98DD4AC3C3A95DED8C644_H
#ifndef U3CU3EC__DISPLAYCLASS4_0_T8719BF1E8CBECF96E1F5A6F6279B4F159FD0E905_H
#define U3CU3EC__DISPLAYCLASS4_0_T8719BF1E8CBECF96E1F5A6F6279B4F159FD0E905_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Svelto.Tasks.Internal.UnityCoroutineRunner_<>c__DisplayClass4_0
struct  U3CU3Ec__DisplayClass4_0_t8719BF1E8CBECF96E1F5A6F6279B4F159FD0E905  : public RuntimeObject
{
public:
	// Svelto.Tasks.Internal.RunnerBehaviour Svelto.Tasks.Internal.UnityCoroutineRunner_<>c__DisplayClass4_0::runnerBehaviourForUnityCoroutine
	RunnerBehaviour_t9AE6F5864AD5F25FE25A18838BC70B25D4B70DFD * ___runnerBehaviourForUnityCoroutine_0;

public:
	inline static int32_t get_offset_of_runnerBehaviourForUnityCoroutine_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_t8719BF1E8CBECF96E1F5A6F6279B4F159FD0E905, ___runnerBehaviourForUnityCoroutine_0)); }
	inline RunnerBehaviour_t9AE6F5864AD5F25FE25A18838BC70B25D4B70DFD * get_runnerBehaviourForUnityCoroutine_0() const { return ___runnerBehaviourForUnityCoroutine_0; }
	inline RunnerBehaviour_t9AE6F5864AD5F25FE25A18838BC70B25D4B70DFD ** get_address_of_runnerBehaviourForUnityCoroutine_0() { return &___runnerBehaviourForUnityCoroutine_0; }
	inline void set_runnerBehaviourForUnityCoroutine_0(RunnerBehaviour_t9AE6F5864AD5F25FE25A18838BC70B25D4B70DFD * value)
	{
		___runnerBehaviourForUnityCoroutine_0 = value;
		Il2CppCodeGenWriteBarrier((&___runnerBehaviourForUnityCoroutine_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS4_0_T8719BF1E8CBECF96E1F5A6F6279B4F159FD0E905_H
#ifndef U3CPROCESSU3ED__4_T0599BE77B51BF8BA0D1A4FEB19C65DF6411780BD_H
#define U3CPROCESSU3ED__4_T0599BE77B51BF8BA0D1A4FEB19C65DF6411780BD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Svelto.Tasks.Internal.UnityCoroutineRunner_<Process>d__4
struct  U3CProcessU3Ed__4_t0599BE77B51BF8BA0D1A4FEB19C65DF6411780BD  : public RuntimeObject
{
public:
	// System.Int32 Svelto.Tasks.Internal.UnityCoroutineRunner_<Process>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Svelto.Tasks.Internal.UnityCoroutineRunner_<Process>d__4::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Svelto.Tasks.Internal.RunnerBehaviour Svelto.Tasks.Internal.UnityCoroutineRunner_<Process>d__4::runnerBehaviourForUnityCoroutine
	RunnerBehaviour_t9AE6F5864AD5F25FE25A18838BC70B25D4B70DFD * ___runnerBehaviourForUnityCoroutine_2;
	// Svelto.Tasks.Internal.UnityCoroutineRunner_FlushingOperation Svelto.Tasks.Internal.UnityCoroutineRunner_<Process>d__4::flushingOperation
	FlushingOperation_tB0B9CC8E029067E8D130896A68A0E2D390E01027 * ___flushingOperation_3;
	// Svelto.Tasks.Internal.UnityCoroutineRunner_FlushTasksDel Svelto.Tasks.Internal.UnityCoroutineRunner_<Process>d__4::flushTaskDel
	FlushTasksDel_t5FECFC8BE91DD02043575279E76105CB8643E11D * ___flushTaskDel_4;
	// Svelto.DataStructures.ThreadSafeQueue`1<Svelto.Tasks.IPausableTask> Svelto.Tasks.Internal.UnityCoroutineRunner_<Process>d__4::newTaskRoutines
	ThreadSafeQueue_1_t4B42DDB151ED238066A071C89AF1FC3DB9C1BE45 * ___newTaskRoutines_5;
	// Svelto.DataStructures.FasterList`1<Svelto.Tasks.IPausableTask> Svelto.Tasks.Internal.UnityCoroutineRunner_<Process>d__4::coroutines
	FasterList_1_t2B537C5D221BBAAC4DA80F69541C173562A8478A * ___coroutines_6;
	// Svelto.Tasks.Internal.UnityCoroutineRunner_<>c__DisplayClass4_0 Svelto.Tasks.Internal.UnityCoroutineRunner_<Process>d__4::<>8__1
	U3CU3Ec__DisplayClass4_0_t8719BF1E8CBECF96E1F5A6F6279B4F159FD0E905 * ___U3CU3E8__1_7;
	// Svelto.Tasks.Internal.UnityCoroutineRunner_RunningTasksInfo Svelto.Tasks.Internal.UnityCoroutineRunner_<Process>d__4::info
	RunningTasksInfo_t10813DB30B3A26BF8CF0830C17B2B3EE5DBF523C * ___info_8;
	// System.Action`1<Svelto.Tasks.IPausableTask> Svelto.Tasks.Internal.UnityCoroutineRunner_<Process>d__4::resumeOperation
	Action_1_t07F8054E833F5BFAFF0CC0B5AE1F3B8E44083838 * ___resumeOperation_9;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CProcessU3Ed__4_t0599BE77B51BF8BA0D1A4FEB19C65DF6411780BD, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CProcessU3Ed__4_t0599BE77B51BF8BA0D1A4FEB19C65DF6411780BD, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_runnerBehaviourForUnityCoroutine_2() { return static_cast<int32_t>(offsetof(U3CProcessU3Ed__4_t0599BE77B51BF8BA0D1A4FEB19C65DF6411780BD, ___runnerBehaviourForUnityCoroutine_2)); }
	inline RunnerBehaviour_t9AE6F5864AD5F25FE25A18838BC70B25D4B70DFD * get_runnerBehaviourForUnityCoroutine_2() const { return ___runnerBehaviourForUnityCoroutine_2; }
	inline RunnerBehaviour_t9AE6F5864AD5F25FE25A18838BC70B25D4B70DFD ** get_address_of_runnerBehaviourForUnityCoroutine_2() { return &___runnerBehaviourForUnityCoroutine_2; }
	inline void set_runnerBehaviourForUnityCoroutine_2(RunnerBehaviour_t9AE6F5864AD5F25FE25A18838BC70B25D4B70DFD * value)
	{
		___runnerBehaviourForUnityCoroutine_2 = value;
		Il2CppCodeGenWriteBarrier((&___runnerBehaviourForUnityCoroutine_2), value);
	}

	inline static int32_t get_offset_of_flushingOperation_3() { return static_cast<int32_t>(offsetof(U3CProcessU3Ed__4_t0599BE77B51BF8BA0D1A4FEB19C65DF6411780BD, ___flushingOperation_3)); }
	inline FlushingOperation_tB0B9CC8E029067E8D130896A68A0E2D390E01027 * get_flushingOperation_3() const { return ___flushingOperation_3; }
	inline FlushingOperation_tB0B9CC8E029067E8D130896A68A0E2D390E01027 ** get_address_of_flushingOperation_3() { return &___flushingOperation_3; }
	inline void set_flushingOperation_3(FlushingOperation_tB0B9CC8E029067E8D130896A68A0E2D390E01027 * value)
	{
		___flushingOperation_3 = value;
		Il2CppCodeGenWriteBarrier((&___flushingOperation_3), value);
	}

	inline static int32_t get_offset_of_flushTaskDel_4() { return static_cast<int32_t>(offsetof(U3CProcessU3Ed__4_t0599BE77B51BF8BA0D1A4FEB19C65DF6411780BD, ___flushTaskDel_4)); }
	inline FlushTasksDel_t5FECFC8BE91DD02043575279E76105CB8643E11D * get_flushTaskDel_4() const { return ___flushTaskDel_4; }
	inline FlushTasksDel_t5FECFC8BE91DD02043575279E76105CB8643E11D ** get_address_of_flushTaskDel_4() { return &___flushTaskDel_4; }
	inline void set_flushTaskDel_4(FlushTasksDel_t5FECFC8BE91DD02043575279E76105CB8643E11D * value)
	{
		___flushTaskDel_4 = value;
		Il2CppCodeGenWriteBarrier((&___flushTaskDel_4), value);
	}

	inline static int32_t get_offset_of_newTaskRoutines_5() { return static_cast<int32_t>(offsetof(U3CProcessU3Ed__4_t0599BE77B51BF8BA0D1A4FEB19C65DF6411780BD, ___newTaskRoutines_5)); }
	inline ThreadSafeQueue_1_t4B42DDB151ED238066A071C89AF1FC3DB9C1BE45 * get_newTaskRoutines_5() const { return ___newTaskRoutines_5; }
	inline ThreadSafeQueue_1_t4B42DDB151ED238066A071C89AF1FC3DB9C1BE45 ** get_address_of_newTaskRoutines_5() { return &___newTaskRoutines_5; }
	inline void set_newTaskRoutines_5(ThreadSafeQueue_1_t4B42DDB151ED238066A071C89AF1FC3DB9C1BE45 * value)
	{
		___newTaskRoutines_5 = value;
		Il2CppCodeGenWriteBarrier((&___newTaskRoutines_5), value);
	}

	inline static int32_t get_offset_of_coroutines_6() { return static_cast<int32_t>(offsetof(U3CProcessU3Ed__4_t0599BE77B51BF8BA0D1A4FEB19C65DF6411780BD, ___coroutines_6)); }
	inline FasterList_1_t2B537C5D221BBAAC4DA80F69541C173562A8478A * get_coroutines_6() const { return ___coroutines_6; }
	inline FasterList_1_t2B537C5D221BBAAC4DA80F69541C173562A8478A ** get_address_of_coroutines_6() { return &___coroutines_6; }
	inline void set_coroutines_6(FasterList_1_t2B537C5D221BBAAC4DA80F69541C173562A8478A * value)
	{
		___coroutines_6 = value;
		Il2CppCodeGenWriteBarrier((&___coroutines_6), value);
	}

	inline static int32_t get_offset_of_U3CU3E8__1_7() { return static_cast<int32_t>(offsetof(U3CProcessU3Ed__4_t0599BE77B51BF8BA0D1A4FEB19C65DF6411780BD, ___U3CU3E8__1_7)); }
	inline U3CU3Ec__DisplayClass4_0_t8719BF1E8CBECF96E1F5A6F6279B4F159FD0E905 * get_U3CU3E8__1_7() const { return ___U3CU3E8__1_7; }
	inline U3CU3Ec__DisplayClass4_0_t8719BF1E8CBECF96E1F5A6F6279B4F159FD0E905 ** get_address_of_U3CU3E8__1_7() { return &___U3CU3E8__1_7; }
	inline void set_U3CU3E8__1_7(U3CU3Ec__DisplayClass4_0_t8719BF1E8CBECF96E1F5A6F6279B4F159FD0E905 * value)
	{
		___U3CU3E8__1_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E8__1_7), value);
	}

	inline static int32_t get_offset_of_info_8() { return static_cast<int32_t>(offsetof(U3CProcessU3Ed__4_t0599BE77B51BF8BA0D1A4FEB19C65DF6411780BD, ___info_8)); }
	inline RunningTasksInfo_t10813DB30B3A26BF8CF0830C17B2B3EE5DBF523C * get_info_8() const { return ___info_8; }
	inline RunningTasksInfo_t10813DB30B3A26BF8CF0830C17B2B3EE5DBF523C ** get_address_of_info_8() { return &___info_8; }
	inline void set_info_8(RunningTasksInfo_t10813DB30B3A26BF8CF0830C17B2B3EE5DBF523C * value)
	{
		___info_8 = value;
		Il2CppCodeGenWriteBarrier((&___info_8), value);
	}

	inline static int32_t get_offset_of_resumeOperation_9() { return static_cast<int32_t>(offsetof(U3CProcessU3Ed__4_t0599BE77B51BF8BA0D1A4FEB19C65DF6411780BD, ___resumeOperation_9)); }
	inline Action_1_t07F8054E833F5BFAFF0CC0B5AE1F3B8E44083838 * get_resumeOperation_9() const { return ___resumeOperation_9; }
	inline Action_1_t07F8054E833F5BFAFF0CC0B5AE1F3B8E44083838 ** get_address_of_resumeOperation_9() { return &___resumeOperation_9; }
	inline void set_resumeOperation_9(Action_1_t07F8054E833F5BFAFF0CC0B5AE1F3B8E44083838 * value)
	{
		___resumeOperation_9 = value;
		Il2CppCodeGenWriteBarrier((&___resumeOperation_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPROCESSU3ED__4_T0599BE77B51BF8BA0D1A4FEB19C65DF6411780BD_H
#ifndef FLUSHINGOPERATION_TB0B9CC8E029067E8D130896A68A0E2D390E01027_H
#define FLUSHINGOPERATION_TB0B9CC8E029067E8D130896A68A0E2D390E01027_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Svelto.Tasks.Internal.UnityCoroutineRunner_FlushingOperation
struct  FlushingOperation_tB0B9CC8E029067E8D130896A68A0E2D390E01027  : public RuntimeObject
{
public:
	// System.Boolean Svelto.Tasks.Internal.UnityCoroutineRunner_FlushingOperation::stopped
	bool ___stopped_0;

public:
	inline static int32_t get_offset_of_stopped_0() { return static_cast<int32_t>(offsetof(FlushingOperation_tB0B9CC8E029067E8D130896A68A0E2D390E01027, ___stopped_0)); }
	inline bool get_stopped_0() const { return ___stopped_0; }
	inline bool* get_address_of_stopped_0() { return &___stopped_0; }
	inline void set_stopped_0(bool value)
	{
		___stopped_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLUSHINGOPERATION_TB0B9CC8E029067E8D130896A68A0E2D390E01027_H
#ifndef RUNNINGTASKSINFO_T10813DB30B3A26BF8CF0830C17B2B3EE5DBF523C_H
#define RUNNINGTASKSINFO_T10813DB30B3A26BF8CF0830C17B2B3EE5DBF523C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Svelto.Tasks.Internal.UnityCoroutineRunner_RunningTasksInfo
struct  RunningTasksInfo_t10813DB30B3A26BF8CF0830C17B2B3EE5DBF523C  : public RuntimeObject
{
public:
	// System.Int32 Svelto.Tasks.Internal.UnityCoroutineRunner_RunningTasksInfo::count
	int32_t ___count_0;
	// System.String Svelto.Tasks.Internal.UnityCoroutineRunner_RunningTasksInfo::runnerName
	String_t* ___runnerName_1;

public:
	inline static int32_t get_offset_of_count_0() { return static_cast<int32_t>(offsetof(RunningTasksInfo_t10813DB30B3A26BF8CF0830C17B2B3EE5DBF523C, ___count_0)); }
	inline int32_t get_count_0() const { return ___count_0; }
	inline int32_t* get_address_of_count_0() { return &___count_0; }
	inline void set_count_0(int32_t value)
	{
		___count_0 = value;
	}

	inline static int32_t get_offset_of_runnerName_1() { return static_cast<int32_t>(offsetof(RunningTasksInfo_t10813DB30B3A26BF8CF0830C17B2B3EE5DBF523C, ___runnerName_1)); }
	inline String_t* get_runnerName_1() const { return ___runnerName_1; }
	inline String_t** get_address_of_runnerName_1() { return &___runnerName_1; }
	inline void set_runnerName_1(String_t* value)
	{
		___runnerName_1 = value;
		Il2CppCodeGenWriteBarrier((&___runnerName_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNNINGTASKSINFO_T10813DB30B3A26BF8CF0830C17B2B3EE5DBF523C_H
#ifndef THREADUTILITY_T1146F63BFE7B9E624F728D6E50C7FDE43E32CE20_H
#define THREADUTILITY_T1146F63BFE7B9E624F728D6E50C7FDE43E32CE20_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Svelto.Utilities.ThreadUtility
struct  ThreadUtility_t1146F63BFE7B9E624F728D6E50C7FDE43E32CE20  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THREADUTILITY_T1146F63BFE7B9E624F728D6E50C7FDE43E32CE20_H
#ifndef WEAKACTIONBASE_TAB4B827D5D83E8C1254D634C2DE7F28E65378FA8_H
#define WEAKACTIONBASE_TAB4B827D5D83E8C1254D634C2DE7F28E65378FA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Svelto.WeakEvents.WeakActionBase
struct  WeakActionBase_tAB4B827D5D83E8C1254D634C2DE7F28E65378FA8  : public RuntimeObject
{
public:
	// Svelto.DataStructures.WeakReference`1<System.Object> Svelto.WeakEvents.WeakActionBase::objectRef
	WeakReference_1_t5DACCF0D2BA7734A55F78BBDFF0C9CB82EF5BE76 * ___objectRef_0;
	// System.Reflection.MethodInfo Svelto.WeakEvents.WeakActionBase::method
	MethodInfo_t * ___method_1;

public:
	inline static int32_t get_offset_of_objectRef_0() { return static_cast<int32_t>(offsetof(WeakActionBase_tAB4B827D5D83E8C1254D634C2DE7F28E65378FA8, ___objectRef_0)); }
	inline WeakReference_1_t5DACCF0D2BA7734A55F78BBDFF0C9CB82EF5BE76 * get_objectRef_0() const { return ___objectRef_0; }
	inline WeakReference_1_t5DACCF0D2BA7734A55F78BBDFF0C9CB82EF5BE76 ** get_address_of_objectRef_0() { return &___objectRef_0; }
	inline void set_objectRef_0(WeakReference_1_t5DACCF0D2BA7734A55F78BBDFF0C9CB82EF5BE76 * value)
	{
		___objectRef_0 = value;
		Il2CppCodeGenWriteBarrier((&___objectRef_0), value);
	}

	inline static int32_t get_offset_of_method_1() { return static_cast<int32_t>(offsetof(WeakActionBase_tAB4B827D5D83E8C1254D634C2DE7F28E65378FA8, ___method_1)); }
	inline MethodInfo_t * get_method_1() const { return ___method_1; }
	inline MethodInfo_t ** get_address_of_method_1() { return &___method_1; }
	inline void set_method_1(MethodInfo_t * value)
	{
		___method_1 = value;
		Il2CppCodeGenWriteBarrier((&___method_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEAKACTIONBASE_TAB4B827D5D83E8C1254D634C2DE7F28E65378FA8_H
#ifndef WEAKEVENT_T7CDC38859BD859DB5E8C7BAB3C218393D521A08F_H
#define WEAKEVENT_T7CDC38859BD859DB5E8C7BAB3C218393D521A08F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Svelto.WeakEvents.WeakEvent
struct  WeakEvent_t7CDC38859BD859DB5E8C7BAB3C218393D521A08F  : public RuntimeObject
{
public:
	// Svelto.DataStructures.FasterList`1<Svelto.WeakEvents.WeakActionBase> Svelto.WeakEvents.WeakEvent::_subscribers
	FasterList_1_tB3C3C1935A0123A0EDD521DBC185C5105946FC3A * ____subscribers_0;
	// Svelto.DataStructures.FasterList`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Reflection.MethodInfo>> Svelto.WeakEvents.WeakEvent::_toRemove
	FasterList_1_tE5B3474E20F6FF2530A614938F13E1BDB218CAAD * ____toRemove_1;
	// System.Boolean Svelto.WeakEvents.WeakEvent::_isIterating
	bool ____isIterating_2;
	// System.Func`2<Svelto.WeakEvents.WeakActionBase,System.Boolean> Svelto.WeakEvents.WeakEvent::_invoke
	Func_2_tDEA274607343466FE9B7687D6EC0431560D455E1 * ____invoke_3;

public:
	inline static int32_t get_offset_of__subscribers_0() { return static_cast<int32_t>(offsetof(WeakEvent_t7CDC38859BD859DB5E8C7BAB3C218393D521A08F, ____subscribers_0)); }
	inline FasterList_1_tB3C3C1935A0123A0EDD521DBC185C5105946FC3A * get__subscribers_0() const { return ____subscribers_0; }
	inline FasterList_1_tB3C3C1935A0123A0EDD521DBC185C5105946FC3A ** get_address_of__subscribers_0() { return &____subscribers_0; }
	inline void set__subscribers_0(FasterList_1_tB3C3C1935A0123A0EDD521DBC185C5105946FC3A * value)
	{
		____subscribers_0 = value;
		Il2CppCodeGenWriteBarrier((&____subscribers_0), value);
	}

	inline static int32_t get_offset_of__toRemove_1() { return static_cast<int32_t>(offsetof(WeakEvent_t7CDC38859BD859DB5E8C7BAB3C218393D521A08F, ____toRemove_1)); }
	inline FasterList_1_tE5B3474E20F6FF2530A614938F13E1BDB218CAAD * get__toRemove_1() const { return ____toRemove_1; }
	inline FasterList_1_tE5B3474E20F6FF2530A614938F13E1BDB218CAAD ** get_address_of__toRemove_1() { return &____toRemove_1; }
	inline void set__toRemove_1(FasterList_1_tE5B3474E20F6FF2530A614938F13E1BDB218CAAD * value)
	{
		____toRemove_1 = value;
		Il2CppCodeGenWriteBarrier((&____toRemove_1), value);
	}

	inline static int32_t get_offset_of__isIterating_2() { return static_cast<int32_t>(offsetof(WeakEvent_t7CDC38859BD859DB5E8C7BAB3C218393D521A08F, ____isIterating_2)); }
	inline bool get__isIterating_2() const { return ____isIterating_2; }
	inline bool* get_address_of__isIterating_2() { return &____isIterating_2; }
	inline void set__isIterating_2(bool value)
	{
		____isIterating_2 = value;
	}

	inline static int32_t get_offset_of__invoke_3() { return static_cast<int32_t>(offsetof(WeakEvent_t7CDC38859BD859DB5E8C7BAB3C218393D521A08F, ____invoke_3)); }
	inline Func_2_tDEA274607343466FE9B7687D6EC0431560D455E1 * get__invoke_3() const { return ____invoke_3; }
	inline Func_2_tDEA274607343466FE9B7687D6EC0431560D455E1 ** get_address_of__invoke_3() { return &____invoke_3; }
	inline void set__invoke_3(Func_2_tDEA274607343466FE9B7687D6EC0431560D455E1 * value)
	{
		____invoke_3 = value;
		Il2CppCodeGenWriteBarrier((&____invoke_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEAKEVENT_T7CDC38859BD859DB5E8C7BAB3C218393D521A08F_H
#ifndef ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#define ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef CONSOLEMETHODATTRIBUTE_TAB9757928180DA2D91DF6D23A3AD25A9443D8E57_H
#define CONSOLEMETHODATTRIBUTE_TAB9757928180DA2D91DF6D23A3AD25A9443D8E57_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IngameDebugConsole.ConsoleMethodAttribute
struct  ConsoleMethodAttribute_tAB9757928180DA2D91DF6D23A3AD25A9443D8E57  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String IngameDebugConsole.ConsoleMethodAttribute::m_command
	String_t* ___m_command_0;
	// System.String IngameDebugConsole.ConsoleMethodAttribute::m_description
	String_t* ___m_description_1;

public:
	inline static int32_t get_offset_of_m_command_0() { return static_cast<int32_t>(offsetof(ConsoleMethodAttribute_tAB9757928180DA2D91DF6D23A3AD25A9443D8E57, ___m_command_0)); }
	inline String_t* get_m_command_0() const { return ___m_command_0; }
	inline String_t** get_address_of_m_command_0() { return &___m_command_0; }
	inline void set_m_command_0(String_t* value)
	{
		___m_command_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_command_0), value);
	}

	inline static int32_t get_offset_of_m_description_1() { return static_cast<int32_t>(offsetof(ConsoleMethodAttribute_tAB9757928180DA2D91DF6D23A3AD25A9443D8E57, ___m_description_1)); }
	inline String_t* get_m_description_1() const { return ___m_description_1; }
	inline String_t** get_address_of_m_description_1() { return &___m_description_1; }
	inline void set_m_description_1(String_t* value)
	{
		___m_description_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_description_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSOLEMETHODATTRIBUTE_TAB9757928180DA2D91DF6D23A3AD25A9443D8E57_H
#ifndef HANDITTOUNITY_T80AA186C6CA66E73DF88FE27AC23CDDC6CB7D92E_H
#define HANDITTOUNITY_T80AA186C6CA66E73DF88FE27AC23CDDC6CB7D92E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Svelto.Tasks.Internal.UnityCoroutineRunner_HandItToUnity
struct  HandItToUnity_t80AA186C6CA66E73DF88FE27AC23CDDC6CB7D92E 
{
public:
	// System.Object Svelto.Tasks.Internal.UnityCoroutineRunner_HandItToUnity::_current
	RuntimeObject * ____current_0;
	// Svelto.Tasks.IPausableTask Svelto.Tasks.Internal.UnityCoroutineRunner_HandItToUnity::_task
	RuntimeObject* ____task_1;
	// System.Action`1<Svelto.Tasks.IPausableTask> Svelto.Tasks.Internal.UnityCoroutineRunner_HandItToUnity::_resumeOperation
	Action_1_t07F8054E833F5BFAFF0CC0B5AE1F3B8E44083838 * ____resumeOperation_2;
	// System.Boolean Svelto.Tasks.Internal.UnityCoroutineRunner_HandItToUnity::_isDone
	bool ____isDone_3;
	// Svelto.Tasks.Internal.UnityCoroutineRunner_FlushingOperation Svelto.Tasks.Internal.UnityCoroutineRunner_HandItToUnity::_flushingOperation
	FlushingOperation_tB0B9CC8E029067E8D130896A68A0E2D390E01027 * ____flushingOperation_4;

public:
	inline static int32_t get_offset_of__current_0() { return static_cast<int32_t>(offsetof(HandItToUnity_t80AA186C6CA66E73DF88FE27AC23CDDC6CB7D92E, ____current_0)); }
	inline RuntimeObject * get__current_0() const { return ____current_0; }
	inline RuntimeObject ** get_address_of__current_0() { return &____current_0; }
	inline void set__current_0(RuntimeObject * value)
	{
		____current_0 = value;
		Il2CppCodeGenWriteBarrier((&____current_0), value);
	}

	inline static int32_t get_offset_of__task_1() { return static_cast<int32_t>(offsetof(HandItToUnity_t80AA186C6CA66E73DF88FE27AC23CDDC6CB7D92E, ____task_1)); }
	inline RuntimeObject* get__task_1() const { return ____task_1; }
	inline RuntimeObject** get_address_of__task_1() { return &____task_1; }
	inline void set__task_1(RuntimeObject* value)
	{
		____task_1 = value;
		Il2CppCodeGenWriteBarrier((&____task_1), value);
	}

	inline static int32_t get_offset_of__resumeOperation_2() { return static_cast<int32_t>(offsetof(HandItToUnity_t80AA186C6CA66E73DF88FE27AC23CDDC6CB7D92E, ____resumeOperation_2)); }
	inline Action_1_t07F8054E833F5BFAFF0CC0B5AE1F3B8E44083838 * get__resumeOperation_2() const { return ____resumeOperation_2; }
	inline Action_1_t07F8054E833F5BFAFF0CC0B5AE1F3B8E44083838 ** get_address_of__resumeOperation_2() { return &____resumeOperation_2; }
	inline void set__resumeOperation_2(Action_1_t07F8054E833F5BFAFF0CC0B5AE1F3B8E44083838 * value)
	{
		____resumeOperation_2 = value;
		Il2CppCodeGenWriteBarrier((&____resumeOperation_2), value);
	}

	inline static int32_t get_offset_of__isDone_3() { return static_cast<int32_t>(offsetof(HandItToUnity_t80AA186C6CA66E73DF88FE27AC23CDDC6CB7D92E, ____isDone_3)); }
	inline bool get__isDone_3() const { return ____isDone_3; }
	inline bool* get_address_of__isDone_3() { return &____isDone_3; }
	inline void set__isDone_3(bool value)
	{
		____isDone_3 = value;
	}

	inline static int32_t get_offset_of__flushingOperation_4() { return static_cast<int32_t>(offsetof(HandItToUnity_t80AA186C6CA66E73DF88FE27AC23CDDC6CB7D92E, ____flushingOperation_4)); }
	inline FlushingOperation_tB0B9CC8E029067E8D130896A68A0E2D390E01027 * get__flushingOperation_4() const { return ____flushingOperation_4; }
	inline FlushingOperation_tB0B9CC8E029067E8D130896A68A0E2D390E01027 ** get_address_of__flushingOperation_4() { return &____flushingOperation_4; }
	inline void set__flushingOperation_4(FlushingOperation_tB0B9CC8E029067E8D130896A68A0E2D390E01027 * value)
	{
		____flushingOperation_4 = value;
		Il2CppCodeGenWriteBarrier((&____flushingOperation_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Svelto.Tasks.Internal.UnityCoroutineRunner/HandItToUnity
struct HandItToUnity_t80AA186C6CA66E73DF88FE27AC23CDDC6CB7D92E_marshaled_pinvoke
{
	Il2CppIUnknown* ____current_0;
	RuntimeObject* ____task_1;
	Il2CppMethodPointer ____resumeOperation_2;
	int32_t ____isDone_3;
	FlushingOperation_tB0B9CC8E029067E8D130896A68A0E2D390E01027 * ____flushingOperation_4;
};
// Native definition for COM marshalling of Svelto.Tasks.Internal.UnityCoroutineRunner/HandItToUnity
struct HandItToUnity_t80AA186C6CA66E73DF88FE27AC23CDDC6CB7D92E_marshaled_com
{
	Il2CppIUnknown* ____current_0;
	RuntimeObject* ____task_1;
	Il2CppMethodPointer ____resumeOperation_2;
	int32_t ____isDone_3;
	FlushingOperation_tB0B9CC8E029067E8D130896A68A0E2D390E01027 * ____flushingOperation_4;
};
#endif // HANDITTOUNITY_T80AA186C6CA66E73DF88FE27AC23CDDC6CB7D92E_H
#ifndef WEAKACTION_T3A6FD94D96BFF231BD765C9373F83EAB36E42091_H
#define WEAKACTION_T3A6FD94D96BFF231BD765C9373F83EAB36E42091_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Svelto.WeakEvents.WeakAction
struct  WeakAction_t3A6FD94D96BFF231BD765C9373F83EAB36E42091  : public WeakActionBase_tAB4B827D5D83E8C1254D634C2DE7F28E65378FA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEAKACTION_T3A6FD94D96BFF231BD765C9373F83EAB36E42091_H
#ifndef BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#define BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifndef DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#define DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 
{
public:
	// System.UInt64 System.DateTime::dateData
	uint64_t ___dateData_44;

public:
	inline static int32_t get_offset_of_dateData_44() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132, ___dateData_44)); }
	inline uint64_t get_dateData_44() const { return ___dateData_44; }
	inline uint64_t* get_address_of_dateData_44() { return &___dateData_44; }
	inline void set_dateData_44(uint64_t value)
	{
		___dateData_44 = value;
	}
};

struct DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields
{
public:
	// System.Int32[] System.DateTime::DaysToMonth365
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth365_29;
	// System.Int32[] System.DateTime::DaysToMonth366
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth366_30;
	// System.DateTime System.DateTime::MinValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MinValue_31;
	// System.DateTime System.DateTime::MaxValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MaxValue_32;

public:
	inline static int32_t get_offset_of_DaysToMonth365_29() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth365_29)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth365_29() const { return ___DaysToMonth365_29; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth365_29() { return &___DaysToMonth365_29; }
	inline void set_DaysToMonth365_29(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth365_29 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth365_29), value);
	}

	inline static int32_t get_offset_of_DaysToMonth366_30() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth366_30)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth366_30() const { return ___DaysToMonth366_30; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth366_30() { return &___DaysToMonth366_30; }
	inline void set_DaysToMonth366_30(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth366_30 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth366_30), value);
	}

	inline static int32_t get_offset_of_MinValue_31() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MinValue_31)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MinValue_31() const { return ___MinValue_31; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MinValue_31() { return &___MinValue_31; }
	inline void set_MinValue_31(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MinValue_31 = value;
	}

	inline static int32_t get_offset_of_MaxValue_32() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MaxValue_32)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MaxValue_32() const { return ___MaxValue_32; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MaxValue_32() { return &___MaxValue_32; }
	inline void set_MaxValue_32(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MaxValue_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#define INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#define SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#define COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef DEBUGLOGFILTER_T1019CE9E390D9D52C93424A58A317C866D50F6E1_H
#define DEBUGLOGFILTER_T1019CE9E390D9D52C93424A58A317C866D50F6E1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IngameDebugConsole.DebugLogFilter
struct  DebugLogFilter_t1019CE9E390D9D52C93424A58A317C866D50F6E1 
{
public:
	// System.Int32 IngameDebugConsole.DebugLogFilter::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebugLogFilter_t1019CE9E390D9D52C93424A58A317C866D50F6E1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGLOGFILTER_T1019CE9E390D9D52C93424A58A317C866D50F6E1_H
#ifndef U3CMOVETOPOSANIMATIONU3ED__24_T7A94C65C8B595767AA6DCA811A0E2346B634B612_H
#define U3CMOVETOPOSANIMATIONU3ED__24_T7A94C65C8B595767AA6DCA811A0E2346B634B612_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IngameDebugConsole.DebugLogPopup_<MoveToPosAnimation>d__24
struct  U3CMoveToPosAnimationU3Ed__24_t7A94C65C8B595767AA6DCA811A0E2346B634B612  : public RuntimeObject
{
public:
	// System.Int32 IngameDebugConsole.DebugLogPopup_<MoveToPosAnimation>d__24::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object IngameDebugConsole.DebugLogPopup_<MoveToPosAnimation>d__24::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// IngameDebugConsole.DebugLogPopup IngameDebugConsole.DebugLogPopup_<MoveToPosAnimation>d__24::<>4__this
	DebugLogPopup_t9A2F2B314079CC1B8AE1CBA454189A6FB15C82A6 * ___U3CU3E4__this_2;
	// UnityEngine.Vector3 IngameDebugConsole.DebugLogPopup_<MoveToPosAnimation>d__24::targetPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___targetPos_3;
	// System.Single IngameDebugConsole.DebugLogPopup_<MoveToPosAnimation>d__24::<modifier>5__2
	float ___U3CmodifierU3E5__2_4;
	// UnityEngine.Vector3 IngameDebugConsole.DebugLogPopup_<MoveToPosAnimation>d__24::<initialPos>5__3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CinitialPosU3E5__3_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CMoveToPosAnimationU3Ed__24_t7A94C65C8B595767AA6DCA811A0E2346B634B612, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CMoveToPosAnimationU3Ed__24_t7A94C65C8B595767AA6DCA811A0E2346B634B612, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CMoveToPosAnimationU3Ed__24_t7A94C65C8B595767AA6DCA811A0E2346B634B612, ___U3CU3E4__this_2)); }
	inline DebugLogPopup_t9A2F2B314079CC1B8AE1CBA454189A6FB15C82A6 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline DebugLogPopup_t9A2F2B314079CC1B8AE1CBA454189A6FB15C82A6 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(DebugLogPopup_t9A2F2B314079CC1B8AE1CBA454189A6FB15C82A6 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_targetPos_3() { return static_cast<int32_t>(offsetof(U3CMoveToPosAnimationU3Ed__24_t7A94C65C8B595767AA6DCA811A0E2346B634B612, ___targetPos_3)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_targetPos_3() const { return ___targetPos_3; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_targetPos_3() { return &___targetPos_3; }
	inline void set_targetPos_3(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___targetPos_3 = value;
	}

	inline static int32_t get_offset_of_U3CmodifierU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CMoveToPosAnimationU3Ed__24_t7A94C65C8B595767AA6DCA811A0E2346B634B612, ___U3CmodifierU3E5__2_4)); }
	inline float get_U3CmodifierU3E5__2_4() const { return ___U3CmodifierU3E5__2_4; }
	inline float* get_address_of_U3CmodifierU3E5__2_4() { return &___U3CmodifierU3E5__2_4; }
	inline void set_U3CmodifierU3E5__2_4(float value)
	{
		___U3CmodifierU3E5__2_4 = value;
	}

	inline static int32_t get_offset_of_U3CinitialPosU3E5__3_5() { return static_cast<int32_t>(offsetof(U3CMoveToPosAnimationU3Ed__24_t7A94C65C8B595767AA6DCA811A0E2346B634B612, ___U3CinitialPosU3E5__3_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CinitialPosU3E5__3_5() const { return ___U3CinitialPosU3E5__3_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CinitialPosU3E5__3_5() { return &___U3CinitialPosU3E5__3_5; }
	inline void set_U3CinitialPosU3E5__3_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CinitialPosU3E5__3_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMOVETOPOSANIMATIONU3ED__24_T7A94C65C8B595767AA6DCA811A0E2346B634B612_H
#ifndef WWWENUMERATOR_T33D003743ADA79EF3F980A318D211DCDF8728395_H
#define WWWENUMERATOR_T33D003743ADA79EF3F980A318D211DCDF8728395_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Svelto.Tasks.Enumerators.WWWEnumerator
struct  WWWEnumerator_t33D003743ADA79EF3F980A318D211DCDF8728395  : public RuntimeObject
{
public:
	// UnityEngine.WWW Svelto.Tasks.Enumerators.WWWEnumerator::_www
	WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * ____www_0;
	// System.Single Svelto.Tasks.Enumerators.WWWEnumerator::_timeOut
	float ____timeOut_1;
	// System.DateTime Svelto.Tasks.Enumerators.WWWEnumerator::_then
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ____then_2;

public:
	inline static int32_t get_offset_of__www_0() { return static_cast<int32_t>(offsetof(WWWEnumerator_t33D003743ADA79EF3F980A318D211DCDF8728395, ____www_0)); }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * get__www_0() const { return ____www_0; }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 ** get_address_of__www_0() { return &____www_0; }
	inline void set__www_0(WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * value)
	{
		____www_0 = value;
		Il2CppCodeGenWriteBarrier((&____www_0), value);
	}

	inline static int32_t get_offset_of__timeOut_1() { return static_cast<int32_t>(offsetof(WWWEnumerator_t33D003743ADA79EF3F980A318D211DCDF8728395, ____timeOut_1)); }
	inline float get__timeOut_1() const { return ____timeOut_1; }
	inline float* get_address_of__timeOut_1() { return &____timeOut_1; }
	inline void set__timeOut_1(float value)
	{
		____timeOut_1 = value;
	}

	inline static int32_t get_offset_of__then_2() { return static_cast<int32_t>(offsetof(WWWEnumerator_t33D003743ADA79EF3F980A318D211DCDF8728395, ____then_2)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get__then_2() const { return ____then_2; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of__then_2() { return &____then_2; }
	inline void set__then_2(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		____then_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WWWENUMERATOR_T33D003743ADA79EF3F980A318D211DCDF8728395_H
#ifndef WAITFORSECONDSENUMERATOR_TFDC4A6ED9C9FA20517D0C1ECE31974BFFE450336_H
#define WAITFORSECONDSENUMERATOR_TFDC4A6ED9C9FA20517D0C1ECE31974BFFE450336_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Svelto.Tasks.Enumerators.WaitForSecondsEnumerator
struct  WaitForSecondsEnumerator_tFDC4A6ED9C9FA20517D0C1ECE31974BFFE450336  : public RuntimeObject
{
public:
	// System.DateTime Svelto.Tasks.Enumerators.WaitForSecondsEnumerator::_future
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ____future_0;
	// System.Single Svelto.Tasks.Enumerators.WaitForSecondsEnumerator::_seconds
	float ____seconds_1;

public:
	inline static int32_t get_offset_of__future_0() { return static_cast<int32_t>(offsetof(WaitForSecondsEnumerator_tFDC4A6ED9C9FA20517D0C1ECE31974BFFE450336, ____future_0)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get__future_0() const { return ____future_0; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of__future_0() { return &____future_0; }
	inline void set__future_0(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		____future_0 = value;
	}

	inline static int32_t get_offset_of__seconds_1() { return static_cast<int32_t>(offsetof(WaitForSecondsEnumerator_tFDC4A6ED9C9FA20517D0C1ECE31974BFFE450336, ____seconds_1)); }
	inline float get__seconds_1() const { return ____seconds_1; }
	inline float* get_address_of__seconds_1() { return &____seconds_1; }
	inline void set__seconds_1(float value)
	{
		____seconds_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAITFORSECONDSENUMERATOR_TFDC4A6ED9C9FA20517D0C1ECE31974BFFE450336_H
#ifndef WAITFORSIGNALENUMERATOR_TF2DC45D07FFFA1453981DFE83D13E257302A51D9_H
#define WAITFORSIGNALENUMERATOR_TF2DC45D07FFFA1453981DFE83D13E257302A51D9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Svelto.Tasks.Enumerators.WaitForSignalEnumerator
struct  WaitForSignalEnumerator_tF2DC45D07FFFA1453981DFE83D13E257302A51D9  : public RuntimeObject
{
public:
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) Svelto.Tasks.Enumerators.WaitForSignalEnumerator::_signal
	bool ____signal_0;
	// System.Object modreq(System.Runtime.CompilerServices.IsVolatile) Svelto.Tasks.Enumerators.WaitForSignalEnumerator::_return
	RuntimeObject * ____return_1;
	// System.Single modreq(System.Runtime.CompilerServices.IsVolatile) Svelto.Tasks.Enumerators.WaitForSignalEnumerator::_timeout
	float ____timeout_2;
	// System.Boolean Svelto.Tasks.Enumerators.WaitForSignalEnumerator::_autoreset
	bool ____autoreset_3;
	// System.Func`1<System.Boolean> Svelto.Tasks.Enumerators.WaitForSignalEnumerator::_extraCondition
	Func_1_t4ABD6DAD480574F152452DD6B9C9A55F4F6655F1 * ____extraCondition_4;
	// System.Single Svelto.Tasks.Enumerators.WaitForSignalEnumerator::_initialTimeOut
	float ____initialTimeOut_5;
	// System.DateTime Svelto.Tasks.Enumerators.WaitForSignalEnumerator::_then
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ____then_6;
	// System.String Svelto.Tasks.Enumerators.WaitForSignalEnumerator::_name
	String_t* ____name_7;

public:
	inline static int32_t get_offset_of__signal_0() { return static_cast<int32_t>(offsetof(WaitForSignalEnumerator_tF2DC45D07FFFA1453981DFE83D13E257302A51D9, ____signal_0)); }
	inline bool get__signal_0() const { return ____signal_0; }
	inline bool* get_address_of__signal_0() { return &____signal_0; }
	inline void set__signal_0(bool value)
	{
		____signal_0 = value;
	}

	inline static int32_t get_offset_of__return_1() { return static_cast<int32_t>(offsetof(WaitForSignalEnumerator_tF2DC45D07FFFA1453981DFE83D13E257302A51D9, ____return_1)); }
	inline RuntimeObject * get__return_1() const { return ____return_1; }
	inline RuntimeObject ** get_address_of__return_1() { return &____return_1; }
	inline void set__return_1(RuntimeObject * value)
	{
		____return_1 = value;
		Il2CppCodeGenWriteBarrier((&____return_1), value);
	}

	inline static int32_t get_offset_of__timeout_2() { return static_cast<int32_t>(offsetof(WaitForSignalEnumerator_tF2DC45D07FFFA1453981DFE83D13E257302A51D9, ____timeout_2)); }
	inline float get__timeout_2() const { return ____timeout_2; }
	inline float* get_address_of__timeout_2() { return &____timeout_2; }
	inline void set__timeout_2(float value)
	{
		____timeout_2 = value;
	}

	inline static int32_t get_offset_of__autoreset_3() { return static_cast<int32_t>(offsetof(WaitForSignalEnumerator_tF2DC45D07FFFA1453981DFE83D13E257302A51D9, ____autoreset_3)); }
	inline bool get__autoreset_3() const { return ____autoreset_3; }
	inline bool* get_address_of__autoreset_3() { return &____autoreset_3; }
	inline void set__autoreset_3(bool value)
	{
		____autoreset_3 = value;
	}

	inline static int32_t get_offset_of__extraCondition_4() { return static_cast<int32_t>(offsetof(WaitForSignalEnumerator_tF2DC45D07FFFA1453981DFE83D13E257302A51D9, ____extraCondition_4)); }
	inline Func_1_t4ABD6DAD480574F152452DD6B9C9A55F4F6655F1 * get__extraCondition_4() const { return ____extraCondition_4; }
	inline Func_1_t4ABD6DAD480574F152452DD6B9C9A55F4F6655F1 ** get_address_of__extraCondition_4() { return &____extraCondition_4; }
	inline void set__extraCondition_4(Func_1_t4ABD6DAD480574F152452DD6B9C9A55F4F6655F1 * value)
	{
		____extraCondition_4 = value;
		Il2CppCodeGenWriteBarrier((&____extraCondition_4), value);
	}

	inline static int32_t get_offset_of__initialTimeOut_5() { return static_cast<int32_t>(offsetof(WaitForSignalEnumerator_tF2DC45D07FFFA1453981DFE83D13E257302A51D9, ____initialTimeOut_5)); }
	inline float get__initialTimeOut_5() const { return ____initialTimeOut_5; }
	inline float* get_address_of__initialTimeOut_5() { return &____initialTimeOut_5; }
	inline void set__initialTimeOut_5(float value)
	{
		____initialTimeOut_5 = value;
	}

	inline static int32_t get_offset_of__then_6() { return static_cast<int32_t>(offsetof(WaitForSignalEnumerator_tF2DC45D07FFFA1453981DFE83D13E257302A51D9, ____then_6)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get__then_6() const { return ____then_6; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of__then_6() { return &____then_6; }
	inline void set__then_6(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		____then_6 = value;
	}

	inline static int32_t get_offset_of__name_7() { return static_cast<int32_t>(offsetof(WaitForSignalEnumerator_tF2DC45D07FFFA1453981DFE83D13E257302A51D9, ____name_7)); }
	inline String_t* get__name_7() const { return ____name_7; }
	inline String_t** get_address_of__name_7() { return &____name_7; }
	inline void set__name_7(String_t* value)
	{
		____name_7 = value;
		Il2CppCodeGenWriteBarrier((&____name_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAITFORSIGNALENUMERATOR_TF2DC45D07FFFA1453981DFE83D13E257302A51D9_H
#ifndef U3CU3EC__DISPLAYCLASS4_1_TF99A27291549A7B9C21A7F68F055884AD2189B92_H
#define U3CU3EC__DISPLAYCLASS4_1_TF99A27291549A7B9C21A7F68F055884AD2189B92_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Svelto.Tasks.Internal.UnityCoroutineRunner_<>c__DisplayClass4_1
struct  U3CU3Ec__DisplayClass4_1_tF99A27291549A7B9C21A7F68F055884AD2189B92  : public RuntimeObject
{
public:
	// UnityEngine.Coroutine Svelto.Tasks.Internal.UnityCoroutineRunner_<>c__DisplayClass4_1::coroutine
	Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC * ___coroutine_0;
	// Svelto.Tasks.Internal.UnityCoroutineRunner_HandItToUnity Svelto.Tasks.Internal.UnityCoroutineRunner_<>c__DisplayClass4_1::handItToUnity
	HandItToUnity_t80AA186C6CA66E73DF88FE27AC23CDDC6CB7D92E  ___handItToUnity_1;
	// Svelto.Tasks.Internal.UnityCoroutineRunner_<>c__DisplayClass4_0 Svelto.Tasks.Internal.UnityCoroutineRunner_<>c__DisplayClass4_1::CSU24<>8__locals1
	U3CU3Ec__DisplayClass4_0_t8719BF1E8CBECF96E1F5A6F6279B4F159FD0E905 * ___CSU24U3CU3E8__locals1_2;

public:
	inline static int32_t get_offset_of_coroutine_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_1_tF99A27291549A7B9C21A7F68F055884AD2189B92, ___coroutine_0)); }
	inline Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC * get_coroutine_0() const { return ___coroutine_0; }
	inline Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC ** get_address_of_coroutine_0() { return &___coroutine_0; }
	inline void set_coroutine_0(Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC * value)
	{
		___coroutine_0 = value;
		Il2CppCodeGenWriteBarrier((&___coroutine_0), value);
	}

	inline static int32_t get_offset_of_handItToUnity_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_1_tF99A27291549A7B9C21A7F68F055884AD2189B92, ___handItToUnity_1)); }
	inline HandItToUnity_t80AA186C6CA66E73DF88FE27AC23CDDC6CB7D92E  get_handItToUnity_1() const { return ___handItToUnity_1; }
	inline HandItToUnity_t80AA186C6CA66E73DF88FE27AC23CDDC6CB7D92E * get_address_of_handItToUnity_1() { return &___handItToUnity_1; }
	inline void set_handItToUnity_1(HandItToUnity_t80AA186C6CA66E73DF88FE27AC23CDDC6CB7D92E  value)
	{
		___handItToUnity_1 = value;
	}

	inline static int32_t get_offset_of_CSU24U3CU3E8__locals1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_1_tF99A27291549A7B9C21A7F68F055884AD2189B92, ___CSU24U3CU3E8__locals1_2)); }
	inline U3CU3Ec__DisplayClass4_0_t8719BF1E8CBECF96E1F5A6F6279B4F159FD0E905 * get_CSU24U3CU3E8__locals1_2() const { return ___CSU24U3CU3E8__locals1_2; }
	inline U3CU3Ec__DisplayClass4_0_t8719BF1E8CBECF96E1F5A6F6279B4F159FD0E905 ** get_address_of_CSU24U3CU3E8__locals1_2() { return &___CSU24U3CU3E8__locals1_2; }
	inline void set_CSU24U3CU3E8__locals1_2(U3CU3Ec__DisplayClass4_0_t8719BF1E8CBECF96E1F5A6F6279B4F159FD0E905 * value)
	{
		___CSU24U3CU3E8__locals1_2 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E8__locals1_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS4_1_TF99A27291549A7B9C21A7F68F055884AD2189B92_H
#ifndef U3CU3EC__DISPLAYCLASS4_2_TD4D341200EFF4FECA1B3C85655D8F8DDC535D7EF_H
#define U3CU3EC__DISPLAYCLASS4_2_TD4D341200EFF4FECA1B3C85655D8F8DDC535D7EF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Svelto.Tasks.Internal.UnityCoroutineRunner_<>c__DisplayClass4_2
struct  U3CU3Ec__DisplayClass4_2_tD4D341200EFF4FECA1B3C85655D8F8DDC535D7EF  : public RuntimeObject
{
public:
	// UnityEngine.Coroutine Svelto.Tasks.Internal.UnityCoroutineRunner_<>c__DisplayClass4_2::coroutine
	Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC * ___coroutine_0;
	// Svelto.Tasks.Internal.UnityCoroutineRunner_HandItToUnity Svelto.Tasks.Internal.UnityCoroutineRunner_<>c__DisplayClass4_2::handItToUnity
	HandItToUnity_t80AA186C6CA66E73DF88FE27AC23CDDC6CB7D92E  ___handItToUnity_1;
	// Svelto.Tasks.Internal.UnityCoroutineRunner_<>c__DisplayClass4_0 Svelto.Tasks.Internal.UnityCoroutineRunner_<>c__DisplayClass4_2::CSU24<>8__locals2
	U3CU3Ec__DisplayClass4_0_t8719BF1E8CBECF96E1F5A6F6279B4F159FD0E905 * ___CSU24U3CU3E8__locals2_2;

public:
	inline static int32_t get_offset_of_coroutine_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_2_tD4D341200EFF4FECA1B3C85655D8F8DDC535D7EF, ___coroutine_0)); }
	inline Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC * get_coroutine_0() const { return ___coroutine_0; }
	inline Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC ** get_address_of_coroutine_0() { return &___coroutine_0; }
	inline void set_coroutine_0(Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC * value)
	{
		___coroutine_0 = value;
		Il2CppCodeGenWriteBarrier((&___coroutine_0), value);
	}

	inline static int32_t get_offset_of_handItToUnity_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_2_tD4D341200EFF4FECA1B3C85655D8F8DDC535D7EF, ___handItToUnity_1)); }
	inline HandItToUnity_t80AA186C6CA66E73DF88FE27AC23CDDC6CB7D92E  get_handItToUnity_1() const { return ___handItToUnity_1; }
	inline HandItToUnity_t80AA186C6CA66E73DF88FE27AC23CDDC6CB7D92E * get_address_of_handItToUnity_1() { return &___handItToUnity_1; }
	inline void set_handItToUnity_1(HandItToUnity_t80AA186C6CA66E73DF88FE27AC23CDDC6CB7D92E  value)
	{
		___handItToUnity_1 = value;
	}

	inline static int32_t get_offset_of_CSU24U3CU3E8__locals2_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_2_tD4D341200EFF4FECA1B3C85655D8F8DDC535D7EF, ___CSU24U3CU3E8__locals2_2)); }
	inline U3CU3Ec__DisplayClass4_0_t8719BF1E8CBECF96E1F5A6F6279B4F159FD0E905 * get_CSU24U3CU3E8__locals2_2() const { return ___CSU24U3CU3E8__locals2_2; }
	inline U3CU3Ec__DisplayClass4_0_t8719BF1E8CBECF96E1F5A6F6279B4F159FD0E905 ** get_address_of_CSU24U3CU3E8__locals2_2() { return &___CSU24U3CU3E8__locals2_2; }
	inline void set_CSU24U3CU3E8__locals2_2(U3CU3Ec__DisplayClass4_0_t8719BF1E8CBECF96E1F5A6F6279B4F159FD0E905 * value)
	{
		___CSU24U3CU3E8__locals2_2 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E8__locals2_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS4_2_TD4D341200EFF4FECA1B3C85655D8F8DDC535D7EF_H
#ifndef U3CGETENUMERATORU3ED__2_TC728F2BA628DC44F8693BCD2D1A5416CAF982F6B_H
#define U3CGETENUMERATORU3ED__2_TC728F2BA628DC44F8693BCD2D1A5416CAF982F6B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Svelto.Tasks.Internal.UnityCoroutineRunner_HandItToUnity_<GetEnumerator>d__2
struct  U3CGetEnumeratorU3Ed__2_tC728F2BA628DC44F8693BCD2D1A5416CAF982F6B  : public RuntimeObject
{
public:
	// System.Int32 Svelto.Tasks.Internal.UnityCoroutineRunner_HandItToUnity_<GetEnumerator>d__2::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Svelto.Tasks.Internal.UnityCoroutineRunner_HandItToUnity_<GetEnumerator>d__2::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Svelto.Tasks.Internal.UnityCoroutineRunner_HandItToUnity Svelto.Tasks.Internal.UnityCoroutineRunner_HandItToUnity_<GetEnumerator>d__2::<>4__this
	HandItToUnity_t80AA186C6CA66E73DF88FE27AC23CDDC6CB7D92E  ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__2_tC728F2BA628DC44F8693BCD2D1A5416CAF982F6B, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__2_tC728F2BA628DC44F8693BCD2D1A5416CAF982F6B, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__2_tC728F2BA628DC44F8693BCD2D1A5416CAF982F6B, ___U3CU3E4__this_2)); }
	inline HandItToUnity_t80AA186C6CA66E73DF88FE27AC23CDDC6CB7D92E  get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline HandItToUnity_t80AA186C6CA66E73DF88FE27AC23CDDC6CB7D92E * get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(HandItToUnity_t80AA186C6CA66E73DF88FE27AC23CDDC6CB7D92E  value)
	{
		___U3CU3E4__this_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETENUMERATORU3ED__2_TC728F2BA628DC44F8693BCD2D1A5416CAF982F6B_H
#ifndef U3CWAITUNTILISDONEU3ED__4_T89C33E14EA38C81BCC3A831A776F16EC0FADB929_H
#define U3CWAITUNTILISDONEU3ED__4_T89C33E14EA38C81BCC3A831A776F16EC0FADB929_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Svelto.Tasks.Internal.UnityCoroutineRunner_HandItToUnity_<WaitUntilIsDone>d__4
struct  U3CWaitUntilIsDoneU3Ed__4_t89C33E14EA38C81BCC3A831A776F16EC0FADB929  : public RuntimeObject
{
public:
	// System.Int32 Svelto.Tasks.Internal.UnityCoroutineRunner_HandItToUnity_<WaitUntilIsDone>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Svelto.Tasks.Internal.UnityCoroutineRunner_HandItToUnity_<WaitUntilIsDone>d__4::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Svelto.Tasks.Internal.UnityCoroutineRunner_HandItToUnity Svelto.Tasks.Internal.UnityCoroutineRunner_HandItToUnity_<WaitUntilIsDone>d__4::<>4__this
	HandItToUnity_t80AA186C6CA66E73DF88FE27AC23CDDC6CB7D92E  ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitUntilIsDoneU3Ed__4_t89C33E14EA38C81BCC3A831A776F16EC0FADB929, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitUntilIsDoneU3Ed__4_t89C33E14EA38C81BCC3A831A776F16EC0FADB929, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CWaitUntilIsDoneU3Ed__4_t89C33E14EA38C81BCC3A831A776F16EC0FADB929, ___U3CU3E4__this_2)); }
	inline HandItToUnity_t80AA186C6CA66E73DF88FE27AC23CDDC6CB7D92E  get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline HandItToUnity_t80AA186C6CA66E73DF88FE27AC23CDDC6CB7D92E * get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(HandItToUnity_t80AA186C6CA66E73DF88FE27AC23CDDC6CB7D92E  value)
	{
		___U3CU3E4__this_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWAITUNTILISDONEU3ED__4_T89C33E14EA38C81BCC3A831A776F16EC0FADB929_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef MANUALRESETEVENTSLIM_T085E880B24016C42F7DE42113674D0A41B4FB445_H
#define MANUALRESETEVENTSLIM_T085E880B24016C42F7DE42113674D0A41B4FB445_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.ManualResetEventSlim
struct  ManualResetEventSlim_t085E880B24016C42F7DE42113674D0A41B4FB445  : public RuntimeObject
{
public:
	// System.Object modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.ManualResetEventSlim::m_lock
	RuntimeObject * ___m_lock_2;
	// System.Threading.ManualResetEvent modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.ManualResetEventSlim::m_eventObj
	ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * ___m_eventObj_3;
	// System.Int32 modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.ManualResetEventSlim::m_combinedState
	int32_t ___m_combinedState_4;

public:
	inline static int32_t get_offset_of_m_lock_2() { return static_cast<int32_t>(offsetof(ManualResetEventSlim_t085E880B24016C42F7DE42113674D0A41B4FB445, ___m_lock_2)); }
	inline RuntimeObject * get_m_lock_2() const { return ___m_lock_2; }
	inline RuntimeObject ** get_address_of_m_lock_2() { return &___m_lock_2; }
	inline void set_m_lock_2(RuntimeObject * value)
	{
		___m_lock_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_lock_2), value);
	}

	inline static int32_t get_offset_of_m_eventObj_3() { return static_cast<int32_t>(offsetof(ManualResetEventSlim_t085E880B24016C42F7DE42113674D0A41B4FB445, ___m_eventObj_3)); }
	inline ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * get_m_eventObj_3() const { return ___m_eventObj_3; }
	inline ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 ** get_address_of_m_eventObj_3() { return &___m_eventObj_3; }
	inline void set_m_eventObj_3(ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * value)
	{
		___m_eventObj_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_eventObj_3), value);
	}

	inline static int32_t get_offset_of_m_combinedState_4() { return static_cast<int32_t>(offsetof(ManualResetEventSlim_t085E880B24016C42F7DE42113674D0A41B4FB445, ___m_combinedState_4)); }
	inline int32_t get_m_combinedState_4() const { return ___m_combinedState_4; }
	inline int32_t* get_address_of_m_combinedState_4() { return &___m_combinedState_4; }
	inline void set_m_combinedState_4(int32_t value)
	{
		___m_combinedState_4 = value;
	}
};

struct ManualResetEventSlim_t085E880B24016C42F7DE42113674D0A41B4FB445_StaticFields
{
public:
	// System.Action`1<System.Object> System.Threading.ManualResetEventSlim::s_cancellationTokenCallback
	Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * ___s_cancellationTokenCallback_14;

public:
	inline static int32_t get_offset_of_s_cancellationTokenCallback_14() { return static_cast<int32_t>(offsetof(ManualResetEventSlim_t085E880B24016C42F7DE42113674D0A41B4FB445_StaticFields, ___s_cancellationTokenCallback_14)); }
	inline Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * get_s_cancellationTokenCallback_14() const { return ___s_cancellationTokenCallback_14; }
	inline Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 ** get_address_of_s_cancellationTokenCallback_14() { return &___s_cancellationTokenCallback_14; }
	inline void set_s_cancellationTokenCallback_14(Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * value)
	{
		___s_cancellationTokenCallback_14 = value;
		Il2CppCodeGenWriteBarrier((&___s_cancellationTokenCallback_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MANUALRESETEVENTSLIM_T085E880B24016C42F7DE42113674D0A41B4FB445_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef MANUALRESETEVENTEX_T33E2A70F227B45E36C283FE859544123EC73BB78_H
#define MANUALRESETEVENTEX_T33E2A70F227B45E36C283FE859544123EC73BB78_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Svelto.Utilities.ManualResetEventEx
struct  ManualResetEventEx_t33E2A70F227B45E36C283FE859544123EC73BB78  : public ManualResetEventSlim_t085E880B24016C42F7DE42113674D0A41B4FB445
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MANUALRESETEVENTEX_T33E2A70F227B45E36C283FE859544123EC73BB78_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef PARSEFUNCTION_TF056F17516F5F71B23D983D5663CBD92C027C91A_H
#define PARSEFUNCTION_TF056F17516F5F71B23D983D5663CBD92C027C91A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IngameDebugConsole.DebugLogConsole_ParseFunction
struct  ParseFunction_tF056F17516F5F71B23D983D5663CBD92C027C91A  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARSEFUNCTION_TF056F17516F5F71B23D983D5663CBD92C027C91A_H
#ifndef FLUSHTASKSDEL_T5FECFC8BE91DD02043575279E76105CB8643E11D_H
#define FLUSHTASKSDEL_T5FECFC8BE91DD02043575279E76105CB8643E11D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Svelto.Tasks.Internal.UnityCoroutineRunner_FlushTasksDel
struct  FlushTasksDel_t5FECFC8BE91DD02043575279E76105CB8643E11D  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLUSHTASKSDEL_T5FECFC8BE91DD02043575279E76105CB8643E11D_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef DEBUGLOGITEM_T706D6712831B240C3EA4B655657EE6EB2F37461C_H
#define DEBUGLOGITEM_T706D6712831B240C3EA4B655657EE6EB2F37461C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IngameDebugConsole.DebugLogItem
struct  DebugLogItem_t706D6712831B240C3EA4B655657EE6EB2F37461C  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.RectTransform IngameDebugConsole.DebugLogItem::transformComponent
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___transformComponent_4;
	// UnityEngine.UI.Image IngameDebugConsole.DebugLogItem::imageComponent
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___imageComponent_5;
	// UnityEngine.UI.Text IngameDebugConsole.DebugLogItem::logText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___logText_6;
	// UnityEngine.UI.Image IngameDebugConsole.DebugLogItem::logTypeImage
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___logTypeImage_7;
	// UnityEngine.GameObject IngameDebugConsole.DebugLogItem::logCountParent
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___logCountParent_8;
	// UnityEngine.UI.Text IngameDebugConsole.DebugLogItem::logCountText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___logCountText_9;
	// IngameDebugConsole.DebugLogEntry IngameDebugConsole.DebugLogItem::logEntry
	DebugLogEntry_t2A901F25FD9B697BBC7B6864E19EC3E8F2902D57 * ___logEntry_10;
	// System.Int32 IngameDebugConsole.DebugLogItem::entryIndex
	int32_t ___entryIndex_11;
	// IngameDebugConsole.DebugLogRecycledListView IngameDebugConsole.DebugLogItem::manager
	DebugLogRecycledListView_t63DA7AF9FA87FF739B5A9AC577D26D9792A809BC * ___manager_12;

public:
	inline static int32_t get_offset_of_transformComponent_4() { return static_cast<int32_t>(offsetof(DebugLogItem_t706D6712831B240C3EA4B655657EE6EB2F37461C, ___transformComponent_4)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_transformComponent_4() const { return ___transformComponent_4; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_transformComponent_4() { return &___transformComponent_4; }
	inline void set_transformComponent_4(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___transformComponent_4 = value;
		Il2CppCodeGenWriteBarrier((&___transformComponent_4), value);
	}

	inline static int32_t get_offset_of_imageComponent_5() { return static_cast<int32_t>(offsetof(DebugLogItem_t706D6712831B240C3EA4B655657EE6EB2F37461C, ___imageComponent_5)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_imageComponent_5() const { return ___imageComponent_5; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_imageComponent_5() { return &___imageComponent_5; }
	inline void set_imageComponent_5(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___imageComponent_5 = value;
		Il2CppCodeGenWriteBarrier((&___imageComponent_5), value);
	}

	inline static int32_t get_offset_of_logText_6() { return static_cast<int32_t>(offsetof(DebugLogItem_t706D6712831B240C3EA4B655657EE6EB2F37461C, ___logText_6)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_logText_6() const { return ___logText_6; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_logText_6() { return &___logText_6; }
	inline void set_logText_6(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___logText_6 = value;
		Il2CppCodeGenWriteBarrier((&___logText_6), value);
	}

	inline static int32_t get_offset_of_logTypeImage_7() { return static_cast<int32_t>(offsetof(DebugLogItem_t706D6712831B240C3EA4B655657EE6EB2F37461C, ___logTypeImage_7)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_logTypeImage_7() const { return ___logTypeImage_7; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_logTypeImage_7() { return &___logTypeImage_7; }
	inline void set_logTypeImage_7(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___logTypeImage_7 = value;
		Il2CppCodeGenWriteBarrier((&___logTypeImage_7), value);
	}

	inline static int32_t get_offset_of_logCountParent_8() { return static_cast<int32_t>(offsetof(DebugLogItem_t706D6712831B240C3EA4B655657EE6EB2F37461C, ___logCountParent_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_logCountParent_8() const { return ___logCountParent_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_logCountParent_8() { return &___logCountParent_8; }
	inline void set_logCountParent_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___logCountParent_8 = value;
		Il2CppCodeGenWriteBarrier((&___logCountParent_8), value);
	}

	inline static int32_t get_offset_of_logCountText_9() { return static_cast<int32_t>(offsetof(DebugLogItem_t706D6712831B240C3EA4B655657EE6EB2F37461C, ___logCountText_9)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_logCountText_9() const { return ___logCountText_9; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_logCountText_9() { return &___logCountText_9; }
	inline void set_logCountText_9(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___logCountText_9 = value;
		Il2CppCodeGenWriteBarrier((&___logCountText_9), value);
	}

	inline static int32_t get_offset_of_logEntry_10() { return static_cast<int32_t>(offsetof(DebugLogItem_t706D6712831B240C3EA4B655657EE6EB2F37461C, ___logEntry_10)); }
	inline DebugLogEntry_t2A901F25FD9B697BBC7B6864E19EC3E8F2902D57 * get_logEntry_10() const { return ___logEntry_10; }
	inline DebugLogEntry_t2A901F25FD9B697BBC7B6864E19EC3E8F2902D57 ** get_address_of_logEntry_10() { return &___logEntry_10; }
	inline void set_logEntry_10(DebugLogEntry_t2A901F25FD9B697BBC7B6864E19EC3E8F2902D57 * value)
	{
		___logEntry_10 = value;
		Il2CppCodeGenWriteBarrier((&___logEntry_10), value);
	}

	inline static int32_t get_offset_of_entryIndex_11() { return static_cast<int32_t>(offsetof(DebugLogItem_t706D6712831B240C3EA4B655657EE6EB2F37461C, ___entryIndex_11)); }
	inline int32_t get_entryIndex_11() const { return ___entryIndex_11; }
	inline int32_t* get_address_of_entryIndex_11() { return &___entryIndex_11; }
	inline void set_entryIndex_11(int32_t value)
	{
		___entryIndex_11 = value;
	}

	inline static int32_t get_offset_of_manager_12() { return static_cast<int32_t>(offsetof(DebugLogItem_t706D6712831B240C3EA4B655657EE6EB2F37461C, ___manager_12)); }
	inline DebugLogRecycledListView_t63DA7AF9FA87FF739B5A9AC577D26D9792A809BC * get_manager_12() const { return ___manager_12; }
	inline DebugLogRecycledListView_t63DA7AF9FA87FF739B5A9AC577D26D9792A809BC ** get_address_of_manager_12() { return &___manager_12; }
	inline void set_manager_12(DebugLogRecycledListView_t63DA7AF9FA87FF739B5A9AC577D26D9792A809BC * value)
	{
		___manager_12 = value;
		Il2CppCodeGenWriteBarrier((&___manager_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGLOGITEM_T706D6712831B240C3EA4B655657EE6EB2F37461C_H
#ifndef DEBUGLOGMANAGER_TDD37FA850790FA89B87244EEA7860F0C4A4BF2C0_H
#define DEBUGLOGMANAGER_TDD37FA850790FA89B87244EEA7860F0C4A4BF2C0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IngameDebugConsole.DebugLogManager
struct  DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean IngameDebugConsole.DebugLogManager::singleton
	bool ___singleton_5;
	// System.Single IngameDebugConsole.DebugLogManager::minimumHeight
	float ___minimumHeight_6;
	// System.Boolean IngameDebugConsole.DebugLogManager::startInPopupMode
	bool ___startInPopupMode_7;
	// System.Boolean IngameDebugConsole.DebugLogManager::clearCommandAfterExecution
	bool ___clearCommandAfterExecution_8;
	// System.Boolean IngameDebugConsole.DebugLogManager::receiveLogcatLogsInAndroid
	bool ___receiveLogcatLogsInAndroid_9;
	// System.String IngameDebugConsole.DebugLogManager::logcatArguments
	String_t* ___logcatArguments_10;
	// IngameDebugConsole.DebugLogItem IngameDebugConsole.DebugLogManager::logItemPrefab
	DebugLogItem_t706D6712831B240C3EA4B655657EE6EB2F37461C * ___logItemPrefab_11;
	// UnityEngine.Sprite IngameDebugConsole.DebugLogManager::infoLog
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___infoLog_12;
	// UnityEngine.Sprite IngameDebugConsole.DebugLogManager::warningLog
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___warningLog_13;
	// UnityEngine.Sprite IngameDebugConsole.DebugLogManager::errorLog
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___errorLog_14;
	// System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Sprite> IngameDebugConsole.DebugLogManager::logSpriteRepresentations
	Dictionary_2_t48903AD17CC355C8DD3F85A6DC8A554A7258618D * ___logSpriteRepresentations_15;
	// UnityEngine.Color IngameDebugConsole.DebugLogManager::collapseButtonNormalColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___collapseButtonNormalColor_16;
	// UnityEngine.Color IngameDebugConsole.DebugLogManager::collapseButtonSelectedColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___collapseButtonSelectedColor_17;
	// UnityEngine.Color IngameDebugConsole.DebugLogManager::filterButtonsNormalColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___filterButtonsNormalColor_18;
	// UnityEngine.Color IngameDebugConsole.DebugLogManager::filterButtonsSelectedColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___filterButtonsSelectedColor_19;
	// UnityEngine.RectTransform IngameDebugConsole.DebugLogManager::logWindowTR
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___logWindowTR_20;
	// UnityEngine.RectTransform IngameDebugConsole.DebugLogManager::canvasTR
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___canvasTR_21;
	// UnityEngine.RectTransform IngameDebugConsole.DebugLogManager::logItemsContainer
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___logItemsContainer_22;
	// UnityEngine.UI.InputField IngameDebugConsole.DebugLogManager::commandInputField
	InputField_t533609195B110760BCFF00B746C87D81969CB005 * ___commandInputField_23;
	// UnityEngine.UI.Image IngameDebugConsole.DebugLogManager::collapseButton
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___collapseButton_24;
	// UnityEngine.UI.Image IngameDebugConsole.DebugLogManager::filterInfoButton
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___filterInfoButton_25;
	// UnityEngine.UI.Image IngameDebugConsole.DebugLogManager::filterWarningButton
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___filterWarningButton_26;
	// UnityEngine.UI.Image IngameDebugConsole.DebugLogManager::filterErrorButton
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___filterErrorButton_27;
	// UnityEngine.UI.Text IngameDebugConsole.DebugLogManager::infoEntryCountText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___infoEntryCountText_28;
	// UnityEngine.UI.Text IngameDebugConsole.DebugLogManager::warningEntryCountText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___warningEntryCountText_29;
	// UnityEngine.UI.Text IngameDebugConsole.DebugLogManager::errorEntryCountText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___errorEntryCountText_30;
	// UnityEngine.GameObject IngameDebugConsole.DebugLogManager::snapToBottomButton
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___snapToBottomButton_31;
	// System.Int32 IngameDebugConsole.DebugLogManager::infoEntryCount
	int32_t ___infoEntryCount_32;
	// System.Int32 IngameDebugConsole.DebugLogManager::warningEntryCount
	int32_t ___warningEntryCount_33;
	// System.Int32 IngameDebugConsole.DebugLogManager::errorEntryCount
	int32_t ___errorEntryCount_34;
	// UnityEngine.CanvasGroup IngameDebugConsole.DebugLogManager::logWindowCanvasGroup
	CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * ___logWindowCanvasGroup_35;
	// System.Boolean IngameDebugConsole.DebugLogManager::isLogWindowVisible
	bool ___isLogWindowVisible_36;
	// System.Boolean IngameDebugConsole.DebugLogManager::screenDimensionsChanged
	bool ___screenDimensionsChanged_37;
	// IngameDebugConsole.DebugLogPopup IngameDebugConsole.DebugLogManager::popupManager
	DebugLogPopup_t9A2F2B314079CC1B8AE1CBA454189A6FB15C82A6 * ___popupManager_38;
	// UnityEngine.UI.ScrollRect IngameDebugConsole.DebugLogManager::logItemsScrollRect
	ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * ___logItemsScrollRect_39;
	// IngameDebugConsole.DebugLogRecycledListView IngameDebugConsole.DebugLogManager::recycledListView
	DebugLogRecycledListView_t63DA7AF9FA87FF739B5A9AC577D26D9792A809BC * ___recycledListView_40;
	// System.Boolean IngameDebugConsole.DebugLogManager::isCollapseOn
	bool ___isCollapseOn_41;
	// IngameDebugConsole.DebugLogFilter IngameDebugConsole.DebugLogManager::logFilter
	int32_t ___logFilter_42;
	// System.Boolean IngameDebugConsole.DebugLogManager::snapToBottom
	bool ___snapToBottom_43;
	// System.Collections.Generic.List`1<IngameDebugConsole.DebugLogEntry> IngameDebugConsole.DebugLogManager::collapsedLogEntries
	List_1_t1F5E087D9E9D3F4AEFC96C24F5FC1F52B4B0D9E8 * ___collapsedLogEntries_44;
	// System.Collections.Generic.Dictionary`2<IngameDebugConsole.DebugLogEntry,System.Int32> IngameDebugConsole.DebugLogManager::collapsedLogEntriesMap
	Dictionary_2_tF20E30CEE56A9CE9AE202E95FBCC1E78C6C197F4 * ___collapsedLogEntriesMap_45;
	// IngameDebugConsole.DebugLogIndexList IngameDebugConsole.DebugLogManager::uncollapsedLogEntriesIndices
	DebugLogIndexList_t1A17DF0528FD925924AE4E53ABCE40AAE8699A88 * ___uncollapsedLogEntriesIndices_46;
	// IngameDebugConsole.DebugLogIndexList IngameDebugConsole.DebugLogManager::indicesOfListEntriesToShow
	DebugLogIndexList_t1A17DF0528FD925924AE4E53ABCE40AAE8699A88 * ___indicesOfListEntriesToShow_47;
	// System.Collections.Generic.List`1<IngameDebugConsole.DebugLogItem> IngameDebugConsole.DebugLogManager::pooledLogItems
	List_1_t69EF89DD0DB7FCEDA221ACEC0DD5849BABB52566 * ___pooledLogItems_48;
	// UnityEngine.EventSystems.PointerEventData IngameDebugConsole.DebugLogManager::nullPointerEventData
	PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63 * ___nullPointerEventData_49;

public:
	inline static int32_t get_offset_of_singleton_5() { return static_cast<int32_t>(offsetof(DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0, ___singleton_5)); }
	inline bool get_singleton_5() const { return ___singleton_5; }
	inline bool* get_address_of_singleton_5() { return &___singleton_5; }
	inline void set_singleton_5(bool value)
	{
		___singleton_5 = value;
	}

	inline static int32_t get_offset_of_minimumHeight_6() { return static_cast<int32_t>(offsetof(DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0, ___minimumHeight_6)); }
	inline float get_minimumHeight_6() const { return ___minimumHeight_6; }
	inline float* get_address_of_minimumHeight_6() { return &___minimumHeight_6; }
	inline void set_minimumHeight_6(float value)
	{
		___minimumHeight_6 = value;
	}

	inline static int32_t get_offset_of_startInPopupMode_7() { return static_cast<int32_t>(offsetof(DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0, ___startInPopupMode_7)); }
	inline bool get_startInPopupMode_7() const { return ___startInPopupMode_7; }
	inline bool* get_address_of_startInPopupMode_7() { return &___startInPopupMode_7; }
	inline void set_startInPopupMode_7(bool value)
	{
		___startInPopupMode_7 = value;
	}

	inline static int32_t get_offset_of_clearCommandAfterExecution_8() { return static_cast<int32_t>(offsetof(DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0, ___clearCommandAfterExecution_8)); }
	inline bool get_clearCommandAfterExecution_8() const { return ___clearCommandAfterExecution_8; }
	inline bool* get_address_of_clearCommandAfterExecution_8() { return &___clearCommandAfterExecution_8; }
	inline void set_clearCommandAfterExecution_8(bool value)
	{
		___clearCommandAfterExecution_8 = value;
	}

	inline static int32_t get_offset_of_receiveLogcatLogsInAndroid_9() { return static_cast<int32_t>(offsetof(DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0, ___receiveLogcatLogsInAndroid_9)); }
	inline bool get_receiveLogcatLogsInAndroid_9() const { return ___receiveLogcatLogsInAndroid_9; }
	inline bool* get_address_of_receiveLogcatLogsInAndroid_9() { return &___receiveLogcatLogsInAndroid_9; }
	inline void set_receiveLogcatLogsInAndroid_9(bool value)
	{
		___receiveLogcatLogsInAndroid_9 = value;
	}

	inline static int32_t get_offset_of_logcatArguments_10() { return static_cast<int32_t>(offsetof(DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0, ___logcatArguments_10)); }
	inline String_t* get_logcatArguments_10() const { return ___logcatArguments_10; }
	inline String_t** get_address_of_logcatArguments_10() { return &___logcatArguments_10; }
	inline void set_logcatArguments_10(String_t* value)
	{
		___logcatArguments_10 = value;
		Il2CppCodeGenWriteBarrier((&___logcatArguments_10), value);
	}

	inline static int32_t get_offset_of_logItemPrefab_11() { return static_cast<int32_t>(offsetof(DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0, ___logItemPrefab_11)); }
	inline DebugLogItem_t706D6712831B240C3EA4B655657EE6EB2F37461C * get_logItemPrefab_11() const { return ___logItemPrefab_11; }
	inline DebugLogItem_t706D6712831B240C3EA4B655657EE6EB2F37461C ** get_address_of_logItemPrefab_11() { return &___logItemPrefab_11; }
	inline void set_logItemPrefab_11(DebugLogItem_t706D6712831B240C3EA4B655657EE6EB2F37461C * value)
	{
		___logItemPrefab_11 = value;
		Il2CppCodeGenWriteBarrier((&___logItemPrefab_11), value);
	}

	inline static int32_t get_offset_of_infoLog_12() { return static_cast<int32_t>(offsetof(DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0, ___infoLog_12)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_infoLog_12() const { return ___infoLog_12; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_infoLog_12() { return &___infoLog_12; }
	inline void set_infoLog_12(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___infoLog_12 = value;
		Il2CppCodeGenWriteBarrier((&___infoLog_12), value);
	}

	inline static int32_t get_offset_of_warningLog_13() { return static_cast<int32_t>(offsetof(DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0, ___warningLog_13)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_warningLog_13() const { return ___warningLog_13; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_warningLog_13() { return &___warningLog_13; }
	inline void set_warningLog_13(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___warningLog_13 = value;
		Il2CppCodeGenWriteBarrier((&___warningLog_13), value);
	}

	inline static int32_t get_offset_of_errorLog_14() { return static_cast<int32_t>(offsetof(DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0, ___errorLog_14)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_errorLog_14() const { return ___errorLog_14; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_errorLog_14() { return &___errorLog_14; }
	inline void set_errorLog_14(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___errorLog_14 = value;
		Il2CppCodeGenWriteBarrier((&___errorLog_14), value);
	}

	inline static int32_t get_offset_of_logSpriteRepresentations_15() { return static_cast<int32_t>(offsetof(DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0, ___logSpriteRepresentations_15)); }
	inline Dictionary_2_t48903AD17CC355C8DD3F85A6DC8A554A7258618D * get_logSpriteRepresentations_15() const { return ___logSpriteRepresentations_15; }
	inline Dictionary_2_t48903AD17CC355C8DD3F85A6DC8A554A7258618D ** get_address_of_logSpriteRepresentations_15() { return &___logSpriteRepresentations_15; }
	inline void set_logSpriteRepresentations_15(Dictionary_2_t48903AD17CC355C8DD3F85A6DC8A554A7258618D * value)
	{
		___logSpriteRepresentations_15 = value;
		Il2CppCodeGenWriteBarrier((&___logSpriteRepresentations_15), value);
	}

	inline static int32_t get_offset_of_collapseButtonNormalColor_16() { return static_cast<int32_t>(offsetof(DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0, ___collapseButtonNormalColor_16)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_collapseButtonNormalColor_16() const { return ___collapseButtonNormalColor_16; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_collapseButtonNormalColor_16() { return &___collapseButtonNormalColor_16; }
	inline void set_collapseButtonNormalColor_16(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___collapseButtonNormalColor_16 = value;
	}

	inline static int32_t get_offset_of_collapseButtonSelectedColor_17() { return static_cast<int32_t>(offsetof(DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0, ___collapseButtonSelectedColor_17)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_collapseButtonSelectedColor_17() const { return ___collapseButtonSelectedColor_17; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_collapseButtonSelectedColor_17() { return &___collapseButtonSelectedColor_17; }
	inline void set_collapseButtonSelectedColor_17(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___collapseButtonSelectedColor_17 = value;
	}

	inline static int32_t get_offset_of_filterButtonsNormalColor_18() { return static_cast<int32_t>(offsetof(DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0, ___filterButtonsNormalColor_18)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_filterButtonsNormalColor_18() const { return ___filterButtonsNormalColor_18; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_filterButtonsNormalColor_18() { return &___filterButtonsNormalColor_18; }
	inline void set_filterButtonsNormalColor_18(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___filterButtonsNormalColor_18 = value;
	}

	inline static int32_t get_offset_of_filterButtonsSelectedColor_19() { return static_cast<int32_t>(offsetof(DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0, ___filterButtonsSelectedColor_19)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_filterButtonsSelectedColor_19() const { return ___filterButtonsSelectedColor_19; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_filterButtonsSelectedColor_19() { return &___filterButtonsSelectedColor_19; }
	inline void set_filterButtonsSelectedColor_19(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___filterButtonsSelectedColor_19 = value;
	}

	inline static int32_t get_offset_of_logWindowTR_20() { return static_cast<int32_t>(offsetof(DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0, ___logWindowTR_20)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_logWindowTR_20() const { return ___logWindowTR_20; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_logWindowTR_20() { return &___logWindowTR_20; }
	inline void set_logWindowTR_20(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___logWindowTR_20 = value;
		Il2CppCodeGenWriteBarrier((&___logWindowTR_20), value);
	}

	inline static int32_t get_offset_of_canvasTR_21() { return static_cast<int32_t>(offsetof(DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0, ___canvasTR_21)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_canvasTR_21() const { return ___canvasTR_21; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_canvasTR_21() { return &___canvasTR_21; }
	inline void set_canvasTR_21(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___canvasTR_21 = value;
		Il2CppCodeGenWriteBarrier((&___canvasTR_21), value);
	}

	inline static int32_t get_offset_of_logItemsContainer_22() { return static_cast<int32_t>(offsetof(DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0, ___logItemsContainer_22)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_logItemsContainer_22() const { return ___logItemsContainer_22; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_logItemsContainer_22() { return &___logItemsContainer_22; }
	inline void set_logItemsContainer_22(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___logItemsContainer_22 = value;
		Il2CppCodeGenWriteBarrier((&___logItemsContainer_22), value);
	}

	inline static int32_t get_offset_of_commandInputField_23() { return static_cast<int32_t>(offsetof(DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0, ___commandInputField_23)); }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 * get_commandInputField_23() const { return ___commandInputField_23; }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 ** get_address_of_commandInputField_23() { return &___commandInputField_23; }
	inline void set_commandInputField_23(InputField_t533609195B110760BCFF00B746C87D81969CB005 * value)
	{
		___commandInputField_23 = value;
		Il2CppCodeGenWriteBarrier((&___commandInputField_23), value);
	}

	inline static int32_t get_offset_of_collapseButton_24() { return static_cast<int32_t>(offsetof(DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0, ___collapseButton_24)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_collapseButton_24() const { return ___collapseButton_24; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_collapseButton_24() { return &___collapseButton_24; }
	inline void set_collapseButton_24(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___collapseButton_24 = value;
		Il2CppCodeGenWriteBarrier((&___collapseButton_24), value);
	}

	inline static int32_t get_offset_of_filterInfoButton_25() { return static_cast<int32_t>(offsetof(DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0, ___filterInfoButton_25)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_filterInfoButton_25() const { return ___filterInfoButton_25; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_filterInfoButton_25() { return &___filterInfoButton_25; }
	inline void set_filterInfoButton_25(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___filterInfoButton_25 = value;
		Il2CppCodeGenWriteBarrier((&___filterInfoButton_25), value);
	}

	inline static int32_t get_offset_of_filterWarningButton_26() { return static_cast<int32_t>(offsetof(DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0, ___filterWarningButton_26)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_filterWarningButton_26() const { return ___filterWarningButton_26; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_filterWarningButton_26() { return &___filterWarningButton_26; }
	inline void set_filterWarningButton_26(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___filterWarningButton_26 = value;
		Il2CppCodeGenWriteBarrier((&___filterWarningButton_26), value);
	}

	inline static int32_t get_offset_of_filterErrorButton_27() { return static_cast<int32_t>(offsetof(DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0, ___filterErrorButton_27)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_filterErrorButton_27() const { return ___filterErrorButton_27; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_filterErrorButton_27() { return &___filterErrorButton_27; }
	inline void set_filterErrorButton_27(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___filterErrorButton_27 = value;
		Il2CppCodeGenWriteBarrier((&___filterErrorButton_27), value);
	}

	inline static int32_t get_offset_of_infoEntryCountText_28() { return static_cast<int32_t>(offsetof(DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0, ___infoEntryCountText_28)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_infoEntryCountText_28() const { return ___infoEntryCountText_28; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_infoEntryCountText_28() { return &___infoEntryCountText_28; }
	inline void set_infoEntryCountText_28(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___infoEntryCountText_28 = value;
		Il2CppCodeGenWriteBarrier((&___infoEntryCountText_28), value);
	}

	inline static int32_t get_offset_of_warningEntryCountText_29() { return static_cast<int32_t>(offsetof(DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0, ___warningEntryCountText_29)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_warningEntryCountText_29() const { return ___warningEntryCountText_29; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_warningEntryCountText_29() { return &___warningEntryCountText_29; }
	inline void set_warningEntryCountText_29(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___warningEntryCountText_29 = value;
		Il2CppCodeGenWriteBarrier((&___warningEntryCountText_29), value);
	}

	inline static int32_t get_offset_of_errorEntryCountText_30() { return static_cast<int32_t>(offsetof(DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0, ___errorEntryCountText_30)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_errorEntryCountText_30() const { return ___errorEntryCountText_30; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_errorEntryCountText_30() { return &___errorEntryCountText_30; }
	inline void set_errorEntryCountText_30(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___errorEntryCountText_30 = value;
		Il2CppCodeGenWriteBarrier((&___errorEntryCountText_30), value);
	}

	inline static int32_t get_offset_of_snapToBottomButton_31() { return static_cast<int32_t>(offsetof(DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0, ___snapToBottomButton_31)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_snapToBottomButton_31() const { return ___snapToBottomButton_31; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_snapToBottomButton_31() { return &___snapToBottomButton_31; }
	inline void set_snapToBottomButton_31(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___snapToBottomButton_31 = value;
		Il2CppCodeGenWriteBarrier((&___snapToBottomButton_31), value);
	}

	inline static int32_t get_offset_of_infoEntryCount_32() { return static_cast<int32_t>(offsetof(DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0, ___infoEntryCount_32)); }
	inline int32_t get_infoEntryCount_32() const { return ___infoEntryCount_32; }
	inline int32_t* get_address_of_infoEntryCount_32() { return &___infoEntryCount_32; }
	inline void set_infoEntryCount_32(int32_t value)
	{
		___infoEntryCount_32 = value;
	}

	inline static int32_t get_offset_of_warningEntryCount_33() { return static_cast<int32_t>(offsetof(DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0, ___warningEntryCount_33)); }
	inline int32_t get_warningEntryCount_33() const { return ___warningEntryCount_33; }
	inline int32_t* get_address_of_warningEntryCount_33() { return &___warningEntryCount_33; }
	inline void set_warningEntryCount_33(int32_t value)
	{
		___warningEntryCount_33 = value;
	}

	inline static int32_t get_offset_of_errorEntryCount_34() { return static_cast<int32_t>(offsetof(DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0, ___errorEntryCount_34)); }
	inline int32_t get_errorEntryCount_34() const { return ___errorEntryCount_34; }
	inline int32_t* get_address_of_errorEntryCount_34() { return &___errorEntryCount_34; }
	inline void set_errorEntryCount_34(int32_t value)
	{
		___errorEntryCount_34 = value;
	}

	inline static int32_t get_offset_of_logWindowCanvasGroup_35() { return static_cast<int32_t>(offsetof(DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0, ___logWindowCanvasGroup_35)); }
	inline CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * get_logWindowCanvasGroup_35() const { return ___logWindowCanvasGroup_35; }
	inline CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 ** get_address_of_logWindowCanvasGroup_35() { return &___logWindowCanvasGroup_35; }
	inline void set_logWindowCanvasGroup_35(CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * value)
	{
		___logWindowCanvasGroup_35 = value;
		Il2CppCodeGenWriteBarrier((&___logWindowCanvasGroup_35), value);
	}

	inline static int32_t get_offset_of_isLogWindowVisible_36() { return static_cast<int32_t>(offsetof(DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0, ___isLogWindowVisible_36)); }
	inline bool get_isLogWindowVisible_36() const { return ___isLogWindowVisible_36; }
	inline bool* get_address_of_isLogWindowVisible_36() { return &___isLogWindowVisible_36; }
	inline void set_isLogWindowVisible_36(bool value)
	{
		___isLogWindowVisible_36 = value;
	}

	inline static int32_t get_offset_of_screenDimensionsChanged_37() { return static_cast<int32_t>(offsetof(DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0, ___screenDimensionsChanged_37)); }
	inline bool get_screenDimensionsChanged_37() const { return ___screenDimensionsChanged_37; }
	inline bool* get_address_of_screenDimensionsChanged_37() { return &___screenDimensionsChanged_37; }
	inline void set_screenDimensionsChanged_37(bool value)
	{
		___screenDimensionsChanged_37 = value;
	}

	inline static int32_t get_offset_of_popupManager_38() { return static_cast<int32_t>(offsetof(DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0, ___popupManager_38)); }
	inline DebugLogPopup_t9A2F2B314079CC1B8AE1CBA454189A6FB15C82A6 * get_popupManager_38() const { return ___popupManager_38; }
	inline DebugLogPopup_t9A2F2B314079CC1B8AE1CBA454189A6FB15C82A6 ** get_address_of_popupManager_38() { return &___popupManager_38; }
	inline void set_popupManager_38(DebugLogPopup_t9A2F2B314079CC1B8AE1CBA454189A6FB15C82A6 * value)
	{
		___popupManager_38 = value;
		Il2CppCodeGenWriteBarrier((&___popupManager_38), value);
	}

	inline static int32_t get_offset_of_logItemsScrollRect_39() { return static_cast<int32_t>(offsetof(DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0, ___logItemsScrollRect_39)); }
	inline ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * get_logItemsScrollRect_39() const { return ___logItemsScrollRect_39; }
	inline ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 ** get_address_of_logItemsScrollRect_39() { return &___logItemsScrollRect_39; }
	inline void set_logItemsScrollRect_39(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * value)
	{
		___logItemsScrollRect_39 = value;
		Il2CppCodeGenWriteBarrier((&___logItemsScrollRect_39), value);
	}

	inline static int32_t get_offset_of_recycledListView_40() { return static_cast<int32_t>(offsetof(DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0, ___recycledListView_40)); }
	inline DebugLogRecycledListView_t63DA7AF9FA87FF739B5A9AC577D26D9792A809BC * get_recycledListView_40() const { return ___recycledListView_40; }
	inline DebugLogRecycledListView_t63DA7AF9FA87FF739B5A9AC577D26D9792A809BC ** get_address_of_recycledListView_40() { return &___recycledListView_40; }
	inline void set_recycledListView_40(DebugLogRecycledListView_t63DA7AF9FA87FF739B5A9AC577D26D9792A809BC * value)
	{
		___recycledListView_40 = value;
		Il2CppCodeGenWriteBarrier((&___recycledListView_40), value);
	}

	inline static int32_t get_offset_of_isCollapseOn_41() { return static_cast<int32_t>(offsetof(DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0, ___isCollapseOn_41)); }
	inline bool get_isCollapseOn_41() const { return ___isCollapseOn_41; }
	inline bool* get_address_of_isCollapseOn_41() { return &___isCollapseOn_41; }
	inline void set_isCollapseOn_41(bool value)
	{
		___isCollapseOn_41 = value;
	}

	inline static int32_t get_offset_of_logFilter_42() { return static_cast<int32_t>(offsetof(DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0, ___logFilter_42)); }
	inline int32_t get_logFilter_42() const { return ___logFilter_42; }
	inline int32_t* get_address_of_logFilter_42() { return &___logFilter_42; }
	inline void set_logFilter_42(int32_t value)
	{
		___logFilter_42 = value;
	}

	inline static int32_t get_offset_of_snapToBottom_43() { return static_cast<int32_t>(offsetof(DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0, ___snapToBottom_43)); }
	inline bool get_snapToBottom_43() const { return ___snapToBottom_43; }
	inline bool* get_address_of_snapToBottom_43() { return &___snapToBottom_43; }
	inline void set_snapToBottom_43(bool value)
	{
		___snapToBottom_43 = value;
	}

	inline static int32_t get_offset_of_collapsedLogEntries_44() { return static_cast<int32_t>(offsetof(DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0, ___collapsedLogEntries_44)); }
	inline List_1_t1F5E087D9E9D3F4AEFC96C24F5FC1F52B4B0D9E8 * get_collapsedLogEntries_44() const { return ___collapsedLogEntries_44; }
	inline List_1_t1F5E087D9E9D3F4AEFC96C24F5FC1F52B4B0D9E8 ** get_address_of_collapsedLogEntries_44() { return &___collapsedLogEntries_44; }
	inline void set_collapsedLogEntries_44(List_1_t1F5E087D9E9D3F4AEFC96C24F5FC1F52B4B0D9E8 * value)
	{
		___collapsedLogEntries_44 = value;
		Il2CppCodeGenWriteBarrier((&___collapsedLogEntries_44), value);
	}

	inline static int32_t get_offset_of_collapsedLogEntriesMap_45() { return static_cast<int32_t>(offsetof(DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0, ___collapsedLogEntriesMap_45)); }
	inline Dictionary_2_tF20E30CEE56A9CE9AE202E95FBCC1E78C6C197F4 * get_collapsedLogEntriesMap_45() const { return ___collapsedLogEntriesMap_45; }
	inline Dictionary_2_tF20E30CEE56A9CE9AE202E95FBCC1E78C6C197F4 ** get_address_of_collapsedLogEntriesMap_45() { return &___collapsedLogEntriesMap_45; }
	inline void set_collapsedLogEntriesMap_45(Dictionary_2_tF20E30CEE56A9CE9AE202E95FBCC1E78C6C197F4 * value)
	{
		___collapsedLogEntriesMap_45 = value;
		Il2CppCodeGenWriteBarrier((&___collapsedLogEntriesMap_45), value);
	}

	inline static int32_t get_offset_of_uncollapsedLogEntriesIndices_46() { return static_cast<int32_t>(offsetof(DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0, ___uncollapsedLogEntriesIndices_46)); }
	inline DebugLogIndexList_t1A17DF0528FD925924AE4E53ABCE40AAE8699A88 * get_uncollapsedLogEntriesIndices_46() const { return ___uncollapsedLogEntriesIndices_46; }
	inline DebugLogIndexList_t1A17DF0528FD925924AE4E53ABCE40AAE8699A88 ** get_address_of_uncollapsedLogEntriesIndices_46() { return &___uncollapsedLogEntriesIndices_46; }
	inline void set_uncollapsedLogEntriesIndices_46(DebugLogIndexList_t1A17DF0528FD925924AE4E53ABCE40AAE8699A88 * value)
	{
		___uncollapsedLogEntriesIndices_46 = value;
		Il2CppCodeGenWriteBarrier((&___uncollapsedLogEntriesIndices_46), value);
	}

	inline static int32_t get_offset_of_indicesOfListEntriesToShow_47() { return static_cast<int32_t>(offsetof(DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0, ___indicesOfListEntriesToShow_47)); }
	inline DebugLogIndexList_t1A17DF0528FD925924AE4E53ABCE40AAE8699A88 * get_indicesOfListEntriesToShow_47() const { return ___indicesOfListEntriesToShow_47; }
	inline DebugLogIndexList_t1A17DF0528FD925924AE4E53ABCE40AAE8699A88 ** get_address_of_indicesOfListEntriesToShow_47() { return &___indicesOfListEntriesToShow_47; }
	inline void set_indicesOfListEntriesToShow_47(DebugLogIndexList_t1A17DF0528FD925924AE4E53ABCE40AAE8699A88 * value)
	{
		___indicesOfListEntriesToShow_47 = value;
		Il2CppCodeGenWriteBarrier((&___indicesOfListEntriesToShow_47), value);
	}

	inline static int32_t get_offset_of_pooledLogItems_48() { return static_cast<int32_t>(offsetof(DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0, ___pooledLogItems_48)); }
	inline List_1_t69EF89DD0DB7FCEDA221ACEC0DD5849BABB52566 * get_pooledLogItems_48() const { return ___pooledLogItems_48; }
	inline List_1_t69EF89DD0DB7FCEDA221ACEC0DD5849BABB52566 ** get_address_of_pooledLogItems_48() { return &___pooledLogItems_48; }
	inline void set_pooledLogItems_48(List_1_t69EF89DD0DB7FCEDA221ACEC0DD5849BABB52566 * value)
	{
		___pooledLogItems_48 = value;
		Il2CppCodeGenWriteBarrier((&___pooledLogItems_48), value);
	}

	inline static int32_t get_offset_of_nullPointerEventData_49() { return static_cast<int32_t>(offsetof(DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0, ___nullPointerEventData_49)); }
	inline PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63 * get_nullPointerEventData_49() const { return ___nullPointerEventData_49; }
	inline PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63 ** get_address_of_nullPointerEventData_49() { return &___nullPointerEventData_49; }
	inline void set_nullPointerEventData_49(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63 * value)
	{
		___nullPointerEventData_49 = value;
		Il2CppCodeGenWriteBarrier((&___nullPointerEventData_49), value);
	}
};

struct DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0_StaticFields
{
public:
	// IngameDebugConsole.DebugLogManager IngameDebugConsole.DebugLogManager::instance
	DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0 * ___instance_4;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0_StaticFields, ___instance_4)); }
	inline DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0 * get_instance_4() const { return ___instance_4; }
	inline DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0 ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0 * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___instance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGLOGMANAGER_TDD37FA850790FA89B87244EEA7860F0C4A4BF2C0_H
#ifndef DEBUGLOGPOPUP_T9A2F2B314079CC1B8AE1CBA454189A6FB15C82A6_H
#define DEBUGLOGPOPUP_T9A2F2B314079CC1B8AE1CBA454189A6FB15C82A6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IngameDebugConsole.DebugLogPopup
struct  DebugLogPopup_t9A2F2B314079CC1B8AE1CBA454189A6FB15C82A6  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.RectTransform IngameDebugConsole.DebugLogPopup::popupTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___popupTransform_4;
	// UnityEngine.Vector2 IngameDebugConsole.DebugLogPopup::halfSize
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___halfSize_5;
	// UnityEngine.UI.Image IngameDebugConsole.DebugLogPopup::backgroundImage
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___backgroundImage_6;
	// UnityEngine.CanvasGroup IngameDebugConsole.DebugLogPopup::canvasGroup
	CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * ___canvasGroup_7;
	// IngameDebugConsole.DebugLogManager IngameDebugConsole.DebugLogPopup::debugManager
	DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0 * ___debugManager_8;
	// UnityEngine.UI.Text IngameDebugConsole.DebugLogPopup::newInfoCountText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___newInfoCountText_9;
	// UnityEngine.UI.Text IngameDebugConsole.DebugLogPopup::newWarningCountText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___newWarningCountText_10;
	// UnityEngine.UI.Text IngameDebugConsole.DebugLogPopup::newErrorCountText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___newErrorCountText_11;
	// System.Int32 IngameDebugConsole.DebugLogPopup::newInfoCount
	int32_t ___newInfoCount_12;
	// System.Int32 IngameDebugConsole.DebugLogPopup::newWarningCount
	int32_t ___newWarningCount_13;
	// System.Int32 IngameDebugConsole.DebugLogPopup::newErrorCount
	int32_t ___newErrorCount_14;
	// UnityEngine.Color IngameDebugConsole.DebugLogPopup::normalColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___normalColor_15;
	// UnityEngine.Color IngameDebugConsole.DebugLogPopup::alertColorInfo
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___alertColorInfo_16;
	// UnityEngine.Color IngameDebugConsole.DebugLogPopup::alertColorWarning
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___alertColorWarning_17;
	// UnityEngine.Color IngameDebugConsole.DebugLogPopup::alertColorError
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___alertColorError_18;
	// System.Boolean IngameDebugConsole.DebugLogPopup::isPopupBeingDragged
	bool ___isPopupBeingDragged_19;
	// System.Collections.IEnumerator IngameDebugConsole.DebugLogPopup::moveToPosCoroutine
	RuntimeObject* ___moveToPosCoroutine_20;

public:
	inline static int32_t get_offset_of_popupTransform_4() { return static_cast<int32_t>(offsetof(DebugLogPopup_t9A2F2B314079CC1B8AE1CBA454189A6FB15C82A6, ___popupTransform_4)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_popupTransform_4() const { return ___popupTransform_4; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_popupTransform_4() { return &___popupTransform_4; }
	inline void set_popupTransform_4(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___popupTransform_4 = value;
		Il2CppCodeGenWriteBarrier((&___popupTransform_4), value);
	}

	inline static int32_t get_offset_of_halfSize_5() { return static_cast<int32_t>(offsetof(DebugLogPopup_t9A2F2B314079CC1B8AE1CBA454189A6FB15C82A6, ___halfSize_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_halfSize_5() const { return ___halfSize_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_halfSize_5() { return &___halfSize_5; }
	inline void set_halfSize_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___halfSize_5 = value;
	}

	inline static int32_t get_offset_of_backgroundImage_6() { return static_cast<int32_t>(offsetof(DebugLogPopup_t9A2F2B314079CC1B8AE1CBA454189A6FB15C82A6, ___backgroundImage_6)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_backgroundImage_6() const { return ___backgroundImage_6; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_backgroundImage_6() { return &___backgroundImage_6; }
	inline void set_backgroundImage_6(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___backgroundImage_6 = value;
		Il2CppCodeGenWriteBarrier((&___backgroundImage_6), value);
	}

	inline static int32_t get_offset_of_canvasGroup_7() { return static_cast<int32_t>(offsetof(DebugLogPopup_t9A2F2B314079CC1B8AE1CBA454189A6FB15C82A6, ___canvasGroup_7)); }
	inline CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * get_canvasGroup_7() const { return ___canvasGroup_7; }
	inline CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 ** get_address_of_canvasGroup_7() { return &___canvasGroup_7; }
	inline void set_canvasGroup_7(CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * value)
	{
		___canvasGroup_7 = value;
		Il2CppCodeGenWriteBarrier((&___canvasGroup_7), value);
	}

	inline static int32_t get_offset_of_debugManager_8() { return static_cast<int32_t>(offsetof(DebugLogPopup_t9A2F2B314079CC1B8AE1CBA454189A6FB15C82A6, ___debugManager_8)); }
	inline DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0 * get_debugManager_8() const { return ___debugManager_8; }
	inline DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0 ** get_address_of_debugManager_8() { return &___debugManager_8; }
	inline void set_debugManager_8(DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0 * value)
	{
		___debugManager_8 = value;
		Il2CppCodeGenWriteBarrier((&___debugManager_8), value);
	}

	inline static int32_t get_offset_of_newInfoCountText_9() { return static_cast<int32_t>(offsetof(DebugLogPopup_t9A2F2B314079CC1B8AE1CBA454189A6FB15C82A6, ___newInfoCountText_9)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_newInfoCountText_9() const { return ___newInfoCountText_9; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_newInfoCountText_9() { return &___newInfoCountText_9; }
	inline void set_newInfoCountText_9(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___newInfoCountText_9 = value;
		Il2CppCodeGenWriteBarrier((&___newInfoCountText_9), value);
	}

	inline static int32_t get_offset_of_newWarningCountText_10() { return static_cast<int32_t>(offsetof(DebugLogPopup_t9A2F2B314079CC1B8AE1CBA454189A6FB15C82A6, ___newWarningCountText_10)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_newWarningCountText_10() const { return ___newWarningCountText_10; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_newWarningCountText_10() { return &___newWarningCountText_10; }
	inline void set_newWarningCountText_10(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___newWarningCountText_10 = value;
		Il2CppCodeGenWriteBarrier((&___newWarningCountText_10), value);
	}

	inline static int32_t get_offset_of_newErrorCountText_11() { return static_cast<int32_t>(offsetof(DebugLogPopup_t9A2F2B314079CC1B8AE1CBA454189A6FB15C82A6, ___newErrorCountText_11)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_newErrorCountText_11() const { return ___newErrorCountText_11; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_newErrorCountText_11() { return &___newErrorCountText_11; }
	inline void set_newErrorCountText_11(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___newErrorCountText_11 = value;
		Il2CppCodeGenWriteBarrier((&___newErrorCountText_11), value);
	}

	inline static int32_t get_offset_of_newInfoCount_12() { return static_cast<int32_t>(offsetof(DebugLogPopup_t9A2F2B314079CC1B8AE1CBA454189A6FB15C82A6, ___newInfoCount_12)); }
	inline int32_t get_newInfoCount_12() const { return ___newInfoCount_12; }
	inline int32_t* get_address_of_newInfoCount_12() { return &___newInfoCount_12; }
	inline void set_newInfoCount_12(int32_t value)
	{
		___newInfoCount_12 = value;
	}

	inline static int32_t get_offset_of_newWarningCount_13() { return static_cast<int32_t>(offsetof(DebugLogPopup_t9A2F2B314079CC1B8AE1CBA454189A6FB15C82A6, ___newWarningCount_13)); }
	inline int32_t get_newWarningCount_13() const { return ___newWarningCount_13; }
	inline int32_t* get_address_of_newWarningCount_13() { return &___newWarningCount_13; }
	inline void set_newWarningCount_13(int32_t value)
	{
		___newWarningCount_13 = value;
	}

	inline static int32_t get_offset_of_newErrorCount_14() { return static_cast<int32_t>(offsetof(DebugLogPopup_t9A2F2B314079CC1B8AE1CBA454189A6FB15C82A6, ___newErrorCount_14)); }
	inline int32_t get_newErrorCount_14() const { return ___newErrorCount_14; }
	inline int32_t* get_address_of_newErrorCount_14() { return &___newErrorCount_14; }
	inline void set_newErrorCount_14(int32_t value)
	{
		___newErrorCount_14 = value;
	}

	inline static int32_t get_offset_of_normalColor_15() { return static_cast<int32_t>(offsetof(DebugLogPopup_t9A2F2B314079CC1B8AE1CBA454189A6FB15C82A6, ___normalColor_15)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_normalColor_15() const { return ___normalColor_15; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_normalColor_15() { return &___normalColor_15; }
	inline void set_normalColor_15(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___normalColor_15 = value;
	}

	inline static int32_t get_offset_of_alertColorInfo_16() { return static_cast<int32_t>(offsetof(DebugLogPopup_t9A2F2B314079CC1B8AE1CBA454189A6FB15C82A6, ___alertColorInfo_16)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_alertColorInfo_16() const { return ___alertColorInfo_16; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_alertColorInfo_16() { return &___alertColorInfo_16; }
	inline void set_alertColorInfo_16(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___alertColorInfo_16 = value;
	}

	inline static int32_t get_offset_of_alertColorWarning_17() { return static_cast<int32_t>(offsetof(DebugLogPopup_t9A2F2B314079CC1B8AE1CBA454189A6FB15C82A6, ___alertColorWarning_17)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_alertColorWarning_17() const { return ___alertColorWarning_17; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_alertColorWarning_17() { return &___alertColorWarning_17; }
	inline void set_alertColorWarning_17(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___alertColorWarning_17 = value;
	}

	inline static int32_t get_offset_of_alertColorError_18() { return static_cast<int32_t>(offsetof(DebugLogPopup_t9A2F2B314079CC1B8AE1CBA454189A6FB15C82A6, ___alertColorError_18)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_alertColorError_18() const { return ___alertColorError_18; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_alertColorError_18() { return &___alertColorError_18; }
	inline void set_alertColorError_18(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___alertColorError_18 = value;
	}

	inline static int32_t get_offset_of_isPopupBeingDragged_19() { return static_cast<int32_t>(offsetof(DebugLogPopup_t9A2F2B314079CC1B8AE1CBA454189A6FB15C82A6, ___isPopupBeingDragged_19)); }
	inline bool get_isPopupBeingDragged_19() const { return ___isPopupBeingDragged_19; }
	inline bool* get_address_of_isPopupBeingDragged_19() { return &___isPopupBeingDragged_19; }
	inline void set_isPopupBeingDragged_19(bool value)
	{
		___isPopupBeingDragged_19 = value;
	}

	inline static int32_t get_offset_of_moveToPosCoroutine_20() { return static_cast<int32_t>(offsetof(DebugLogPopup_t9A2F2B314079CC1B8AE1CBA454189A6FB15C82A6, ___moveToPosCoroutine_20)); }
	inline RuntimeObject* get_moveToPosCoroutine_20() const { return ___moveToPosCoroutine_20; }
	inline RuntimeObject** get_address_of_moveToPosCoroutine_20() { return &___moveToPosCoroutine_20; }
	inline void set_moveToPosCoroutine_20(RuntimeObject* value)
	{
		___moveToPosCoroutine_20 = value;
		Il2CppCodeGenWriteBarrier((&___moveToPosCoroutine_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGLOGPOPUP_T9A2F2B314079CC1B8AE1CBA454189A6FB15C82A6_H
#ifndef DEBUGLOGRECYCLEDLISTVIEW_T63DA7AF9FA87FF739B5A9AC577D26D9792A809BC_H
#define DEBUGLOGRECYCLEDLISTVIEW_T63DA7AF9FA87FF739B5A9AC577D26D9792A809BC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IngameDebugConsole.DebugLogRecycledListView
struct  DebugLogRecycledListView_t63DA7AF9FA87FF739B5A9AC577D26D9792A809BC  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.RectTransform IngameDebugConsole.DebugLogRecycledListView::transformComponent
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___transformComponent_4;
	// UnityEngine.RectTransform IngameDebugConsole.DebugLogRecycledListView::viewportTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___viewportTransform_5;
	// IngameDebugConsole.DebugLogManager IngameDebugConsole.DebugLogRecycledListView::debugManager
	DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0 * ___debugManager_6;
	// UnityEngine.Color IngameDebugConsole.DebugLogRecycledListView::logItemNormalColor1
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___logItemNormalColor1_7;
	// UnityEngine.Color IngameDebugConsole.DebugLogRecycledListView::logItemNormalColor2
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___logItemNormalColor2_8;
	// UnityEngine.Color IngameDebugConsole.DebugLogRecycledListView::logItemSelectedColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___logItemSelectedColor_9;
	// IngameDebugConsole.DebugLogManager IngameDebugConsole.DebugLogRecycledListView::manager
	DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0 * ___manager_10;
	// System.Single IngameDebugConsole.DebugLogRecycledListView::logItemHeight
	float ___logItemHeight_11;
	// System.Single IngameDebugConsole.DebugLogRecycledListView::_1OverLogItemHeight
	float ____1OverLogItemHeight_12;
	// System.Single IngameDebugConsole.DebugLogRecycledListView::viewportHeight
	float ___viewportHeight_13;
	// System.Collections.Generic.List`1<IngameDebugConsole.DebugLogEntry> IngameDebugConsole.DebugLogRecycledListView::collapsedLogEntries
	List_1_t1F5E087D9E9D3F4AEFC96C24F5FC1F52B4B0D9E8 * ___collapsedLogEntries_14;
	// IngameDebugConsole.DebugLogIndexList IngameDebugConsole.DebugLogRecycledListView::indicesOfEntriesToShow
	DebugLogIndexList_t1A17DF0528FD925924AE4E53ABCE40AAE8699A88 * ___indicesOfEntriesToShow_15;
	// System.Int32 IngameDebugConsole.DebugLogRecycledListView::indexOfSelectedLogEntry
	int32_t ___indexOfSelectedLogEntry_16;
	// System.Single IngameDebugConsole.DebugLogRecycledListView::positionOfSelectedLogEntry
	float ___positionOfSelectedLogEntry_17;
	// System.Single IngameDebugConsole.DebugLogRecycledListView::heightOfSelectedLogEntry
	float ___heightOfSelectedLogEntry_18;
	// System.Single IngameDebugConsole.DebugLogRecycledListView::deltaHeightOfSelectedLogEntry
	float ___deltaHeightOfSelectedLogEntry_19;
	// System.Collections.Generic.Dictionary`2<System.Int32,IngameDebugConsole.DebugLogItem> IngameDebugConsole.DebugLogRecycledListView::logItemsAtIndices
	Dictionary_2_t07C326FB9D8FDFAE1C31A75FEFEC22C9C4FAAC93 * ___logItemsAtIndices_20;
	// System.Boolean IngameDebugConsole.DebugLogRecycledListView::isCollapseOn
	bool ___isCollapseOn_21;
	// System.Int32 IngameDebugConsole.DebugLogRecycledListView::currentTopIndex
	int32_t ___currentTopIndex_22;
	// System.Int32 IngameDebugConsole.DebugLogRecycledListView::currentBottomIndex
	int32_t ___currentBottomIndex_23;

public:
	inline static int32_t get_offset_of_transformComponent_4() { return static_cast<int32_t>(offsetof(DebugLogRecycledListView_t63DA7AF9FA87FF739B5A9AC577D26D9792A809BC, ___transformComponent_4)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_transformComponent_4() const { return ___transformComponent_4; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_transformComponent_4() { return &___transformComponent_4; }
	inline void set_transformComponent_4(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___transformComponent_4 = value;
		Il2CppCodeGenWriteBarrier((&___transformComponent_4), value);
	}

	inline static int32_t get_offset_of_viewportTransform_5() { return static_cast<int32_t>(offsetof(DebugLogRecycledListView_t63DA7AF9FA87FF739B5A9AC577D26D9792A809BC, ___viewportTransform_5)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_viewportTransform_5() const { return ___viewportTransform_5; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_viewportTransform_5() { return &___viewportTransform_5; }
	inline void set_viewportTransform_5(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___viewportTransform_5 = value;
		Il2CppCodeGenWriteBarrier((&___viewportTransform_5), value);
	}

	inline static int32_t get_offset_of_debugManager_6() { return static_cast<int32_t>(offsetof(DebugLogRecycledListView_t63DA7AF9FA87FF739B5A9AC577D26D9792A809BC, ___debugManager_6)); }
	inline DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0 * get_debugManager_6() const { return ___debugManager_6; }
	inline DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0 ** get_address_of_debugManager_6() { return &___debugManager_6; }
	inline void set_debugManager_6(DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0 * value)
	{
		___debugManager_6 = value;
		Il2CppCodeGenWriteBarrier((&___debugManager_6), value);
	}

	inline static int32_t get_offset_of_logItemNormalColor1_7() { return static_cast<int32_t>(offsetof(DebugLogRecycledListView_t63DA7AF9FA87FF739B5A9AC577D26D9792A809BC, ___logItemNormalColor1_7)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_logItemNormalColor1_7() const { return ___logItemNormalColor1_7; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_logItemNormalColor1_7() { return &___logItemNormalColor1_7; }
	inline void set_logItemNormalColor1_7(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___logItemNormalColor1_7 = value;
	}

	inline static int32_t get_offset_of_logItemNormalColor2_8() { return static_cast<int32_t>(offsetof(DebugLogRecycledListView_t63DA7AF9FA87FF739B5A9AC577D26D9792A809BC, ___logItemNormalColor2_8)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_logItemNormalColor2_8() const { return ___logItemNormalColor2_8; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_logItemNormalColor2_8() { return &___logItemNormalColor2_8; }
	inline void set_logItemNormalColor2_8(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___logItemNormalColor2_8 = value;
	}

	inline static int32_t get_offset_of_logItemSelectedColor_9() { return static_cast<int32_t>(offsetof(DebugLogRecycledListView_t63DA7AF9FA87FF739B5A9AC577D26D9792A809BC, ___logItemSelectedColor_9)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_logItemSelectedColor_9() const { return ___logItemSelectedColor_9; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_logItemSelectedColor_9() { return &___logItemSelectedColor_9; }
	inline void set_logItemSelectedColor_9(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___logItemSelectedColor_9 = value;
	}

	inline static int32_t get_offset_of_manager_10() { return static_cast<int32_t>(offsetof(DebugLogRecycledListView_t63DA7AF9FA87FF739B5A9AC577D26D9792A809BC, ___manager_10)); }
	inline DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0 * get_manager_10() const { return ___manager_10; }
	inline DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0 ** get_address_of_manager_10() { return &___manager_10; }
	inline void set_manager_10(DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0 * value)
	{
		___manager_10 = value;
		Il2CppCodeGenWriteBarrier((&___manager_10), value);
	}

	inline static int32_t get_offset_of_logItemHeight_11() { return static_cast<int32_t>(offsetof(DebugLogRecycledListView_t63DA7AF9FA87FF739B5A9AC577D26D9792A809BC, ___logItemHeight_11)); }
	inline float get_logItemHeight_11() const { return ___logItemHeight_11; }
	inline float* get_address_of_logItemHeight_11() { return &___logItemHeight_11; }
	inline void set_logItemHeight_11(float value)
	{
		___logItemHeight_11 = value;
	}

	inline static int32_t get_offset_of__1OverLogItemHeight_12() { return static_cast<int32_t>(offsetof(DebugLogRecycledListView_t63DA7AF9FA87FF739B5A9AC577D26D9792A809BC, ____1OverLogItemHeight_12)); }
	inline float get__1OverLogItemHeight_12() const { return ____1OverLogItemHeight_12; }
	inline float* get_address_of__1OverLogItemHeight_12() { return &____1OverLogItemHeight_12; }
	inline void set__1OverLogItemHeight_12(float value)
	{
		____1OverLogItemHeight_12 = value;
	}

	inline static int32_t get_offset_of_viewportHeight_13() { return static_cast<int32_t>(offsetof(DebugLogRecycledListView_t63DA7AF9FA87FF739B5A9AC577D26D9792A809BC, ___viewportHeight_13)); }
	inline float get_viewportHeight_13() const { return ___viewportHeight_13; }
	inline float* get_address_of_viewportHeight_13() { return &___viewportHeight_13; }
	inline void set_viewportHeight_13(float value)
	{
		___viewportHeight_13 = value;
	}

	inline static int32_t get_offset_of_collapsedLogEntries_14() { return static_cast<int32_t>(offsetof(DebugLogRecycledListView_t63DA7AF9FA87FF739B5A9AC577D26D9792A809BC, ___collapsedLogEntries_14)); }
	inline List_1_t1F5E087D9E9D3F4AEFC96C24F5FC1F52B4B0D9E8 * get_collapsedLogEntries_14() const { return ___collapsedLogEntries_14; }
	inline List_1_t1F5E087D9E9D3F4AEFC96C24F5FC1F52B4B0D9E8 ** get_address_of_collapsedLogEntries_14() { return &___collapsedLogEntries_14; }
	inline void set_collapsedLogEntries_14(List_1_t1F5E087D9E9D3F4AEFC96C24F5FC1F52B4B0D9E8 * value)
	{
		___collapsedLogEntries_14 = value;
		Il2CppCodeGenWriteBarrier((&___collapsedLogEntries_14), value);
	}

	inline static int32_t get_offset_of_indicesOfEntriesToShow_15() { return static_cast<int32_t>(offsetof(DebugLogRecycledListView_t63DA7AF9FA87FF739B5A9AC577D26D9792A809BC, ___indicesOfEntriesToShow_15)); }
	inline DebugLogIndexList_t1A17DF0528FD925924AE4E53ABCE40AAE8699A88 * get_indicesOfEntriesToShow_15() const { return ___indicesOfEntriesToShow_15; }
	inline DebugLogIndexList_t1A17DF0528FD925924AE4E53ABCE40AAE8699A88 ** get_address_of_indicesOfEntriesToShow_15() { return &___indicesOfEntriesToShow_15; }
	inline void set_indicesOfEntriesToShow_15(DebugLogIndexList_t1A17DF0528FD925924AE4E53ABCE40AAE8699A88 * value)
	{
		___indicesOfEntriesToShow_15 = value;
		Il2CppCodeGenWriteBarrier((&___indicesOfEntriesToShow_15), value);
	}

	inline static int32_t get_offset_of_indexOfSelectedLogEntry_16() { return static_cast<int32_t>(offsetof(DebugLogRecycledListView_t63DA7AF9FA87FF739B5A9AC577D26D9792A809BC, ___indexOfSelectedLogEntry_16)); }
	inline int32_t get_indexOfSelectedLogEntry_16() const { return ___indexOfSelectedLogEntry_16; }
	inline int32_t* get_address_of_indexOfSelectedLogEntry_16() { return &___indexOfSelectedLogEntry_16; }
	inline void set_indexOfSelectedLogEntry_16(int32_t value)
	{
		___indexOfSelectedLogEntry_16 = value;
	}

	inline static int32_t get_offset_of_positionOfSelectedLogEntry_17() { return static_cast<int32_t>(offsetof(DebugLogRecycledListView_t63DA7AF9FA87FF739B5A9AC577D26D9792A809BC, ___positionOfSelectedLogEntry_17)); }
	inline float get_positionOfSelectedLogEntry_17() const { return ___positionOfSelectedLogEntry_17; }
	inline float* get_address_of_positionOfSelectedLogEntry_17() { return &___positionOfSelectedLogEntry_17; }
	inline void set_positionOfSelectedLogEntry_17(float value)
	{
		___positionOfSelectedLogEntry_17 = value;
	}

	inline static int32_t get_offset_of_heightOfSelectedLogEntry_18() { return static_cast<int32_t>(offsetof(DebugLogRecycledListView_t63DA7AF9FA87FF739B5A9AC577D26D9792A809BC, ___heightOfSelectedLogEntry_18)); }
	inline float get_heightOfSelectedLogEntry_18() const { return ___heightOfSelectedLogEntry_18; }
	inline float* get_address_of_heightOfSelectedLogEntry_18() { return &___heightOfSelectedLogEntry_18; }
	inline void set_heightOfSelectedLogEntry_18(float value)
	{
		___heightOfSelectedLogEntry_18 = value;
	}

	inline static int32_t get_offset_of_deltaHeightOfSelectedLogEntry_19() { return static_cast<int32_t>(offsetof(DebugLogRecycledListView_t63DA7AF9FA87FF739B5A9AC577D26D9792A809BC, ___deltaHeightOfSelectedLogEntry_19)); }
	inline float get_deltaHeightOfSelectedLogEntry_19() const { return ___deltaHeightOfSelectedLogEntry_19; }
	inline float* get_address_of_deltaHeightOfSelectedLogEntry_19() { return &___deltaHeightOfSelectedLogEntry_19; }
	inline void set_deltaHeightOfSelectedLogEntry_19(float value)
	{
		___deltaHeightOfSelectedLogEntry_19 = value;
	}

	inline static int32_t get_offset_of_logItemsAtIndices_20() { return static_cast<int32_t>(offsetof(DebugLogRecycledListView_t63DA7AF9FA87FF739B5A9AC577D26D9792A809BC, ___logItemsAtIndices_20)); }
	inline Dictionary_2_t07C326FB9D8FDFAE1C31A75FEFEC22C9C4FAAC93 * get_logItemsAtIndices_20() const { return ___logItemsAtIndices_20; }
	inline Dictionary_2_t07C326FB9D8FDFAE1C31A75FEFEC22C9C4FAAC93 ** get_address_of_logItemsAtIndices_20() { return &___logItemsAtIndices_20; }
	inline void set_logItemsAtIndices_20(Dictionary_2_t07C326FB9D8FDFAE1C31A75FEFEC22C9C4FAAC93 * value)
	{
		___logItemsAtIndices_20 = value;
		Il2CppCodeGenWriteBarrier((&___logItemsAtIndices_20), value);
	}

	inline static int32_t get_offset_of_isCollapseOn_21() { return static_cast<int32_t>(offsetof(DebugLogRecycledListView_t63DA7AF9FA87FF739B5A9AC577D26D9792A809BC, ___isCollapseOn_21)); }
	inline bool get_isCollapseOn_21() const { return ___isCollapseOn_21; }
	inline bool* get_address_of_isCollapseOn_21() { return &___isCollapseOn_21; }
	inline void set_isCollapseOn_21(bool value)
	{
		___isCollapseOn_21 = value;
	}

	inline static int32_t get_offset_of_currentTopIndex_22() { return static_cast<int32_t>(offsetof(DebugLogRecycledListView_t63DA7AF9FA87FF739B5A9AC577D26D9792A809BC, ___currentTopIndex_22)); }
	inline int32_t get_currentTopIndex_22() const { return ___currentTopIndex_22; }
	inline int32_t* get_address_of_currentTopIndex_22() { return &___currentTopIndex_22; }
	inline void set_currentTopIndex_22(int32_t value)
	{
		___currentTopIndex_22 = value;
	}

	inline static int32_t get_offset_of_currentBottomIndex_23() { return static_cast<int32_t>(offsetof(DebugLogRecycledListView_t63DA7AF9FA87FF739B5A9AC577D26D9792A809BC, ___currentBottomIndex_23)); }
	inline int32_t get_currentBottomIndex_23() const { return ___currentBottomIndex_23; }
	inline int32_t* get_address_of_currentBottomIndex_23() { return &___currentBottomIndex_23; }
	inline void set_currentBottomIndex_23(int32_t value)
	{
		___currentBottomIndex_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGLOGRECYCLEDLISTVIEW_T63DA7AF9FA87FF739B5A9AC577D26D9792A809BC_H
#ifndef DEBUGSONSCROLLLISTENER_TB028758B6A4B1EBC34838749A6FFDF5A6C9A8B52_H
#define DEBUGSONSCROLLLISTENER_TB028758B6A4B1EBC34838749A6FFDF5A6C9A8B52_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IngameDebugConsole.DebugsOnScrollListener
struct  DebugsOnScrollListener_tB028758B6A4B1EBC34838749A6FFDF5A6C9A8B52  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.ScrollRect IngameDebugConsole.DebugsOnScrollListener::debugsScrollRect
	ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * ___debugsScrollRect_4;
	// IngameDebugConsole.DebugLogManager IngameDebugConsole.DebugsOnScrollListener::debugLogManager
	DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0 * ___debugLogManager_5;

public:
	inline static int32_t get_offset_of_debugsScrollRect_4() { return static_cast<int32_t>(offsetof(DebugsOnScrollListener_tB028758B6A4B1EBC34838749A6FFDF5A6C9A8B52, ___debugsScrollRect_4)); }
	inline ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * get_debugsScrollRect_4() const { return ___debugsScrollRect_4; }
	inline ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 ** get_address_of_debugsScrollRect_4() { return &___debugsScrollRect_4; }
	inline void set_debugsScrollRect_4(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * value)
	{
		___debugsScrollRect_4 = value;
		Il2CppCodeGenWriteBarrier((&___debugsScrollRect_4), value);
	}

	inline static int32_t get_offset_of_debugLogManager_5() { return static_cast<int32_t>(offsetof(DebugsOnScrollListener_tB028758B6A4B1EBC34838749A6FFDF5A6C9A8B52, ___debugLogManager_5)); }
	inline DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0 * get_debugLogManager_5() const { return ___debugLogManager_5; }
	inline DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0 ** get_address_of_debugLogManager_5() { return &___debugLogManager_5; }
	inline void set_debugLogManager_5(DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0 * value)
	{
		___debugLogManager_5 = value;
		Il2CppCodeGenWriteBarrier((&___debugLogManager_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGSONSCROLLLISTENER_TB028758B6A4B1EBC34838749A6FFDF5A6C9A8B52_H
#ifndef RUNNERBEHAVIOURLATE_TA8B0B5A353662B699DE3D47D303863DB2B8BCCD9_H
#define RUNNERBEHAVIOURLATE_TA8B0B5A353662B699DE3D47D303863DB2B8BCCD9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Svelto.Tasks.Internal.RunnerBehaviourLate
struct  RunnerBehaviourLate_tA8B0B5A353662B699DE3D47D303863DB2B8BCCD9  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Collections.IEnumerator Svelto.Tasks.Internal.RunnerBehaviourLate::_mainRoutine
	RuntimeObject* ____mainRoutine_4;

public:
	inline static int32_t get_offset_of__mainRoutine_4() { return static_cast<int32_t>(offsetof(RunnerBehaviourLate_tA8B0B5A353662B699DE3D47D303863DB2B8BCCD9, ____mainRoutine_4)); }
	inline RuntimeObject* get__mainRoutine_4() const { return ____mainRoutine_4; }
	inline RuntimeObject** get_address_of__mainRoutine_4() { return &____mainRoutine_4; }
	inline void set__mainRoutine_4(RuntimeObject* value)
	{
		____mainRoutine_4 = value;
		Il2CppCodeGenWriteBarrier((&____mainRoutine_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNNERBEHAVIOURLATE_TA8B0B5A353662B699DE3D47D303863DB2B8BCCD9_H
#ifndef RUNNERBEHAVIOURPHYSIC_T6BFD13FA00A21B666A35D44CF27C5D3399D7B5A7_H
#define RUNNERBEHAVIOURPHYSIC_T6BFD13FA00A21B666A35D44CF27C5D3399D7B5A7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Svelto.Tasks.Internal.RunnerBehaviourPhysic
struct  RunnerBehaviourPhysic_t6BFD13FA00A21B666A35D44CF27C5D3399D7B5A7  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Collections.IEnumerator Svelto.Tasks.Internal.RunnerBehaviourPhysic::_mainRoutine
	RuntimeObject* ____mainRoutine_4;

public:
	inline static int32_t get_offset_of__mainRoutine_4() { return static_cast<int32_t>(offsetof(RunnerBehaviourPhysic_t6BFD13FA00A21B666A35D44CF27C5D3399D7B5A7, ____mainRoutine_4)); }
	inline RuntimeObject* get__mainRoutine_4() const { return ____mainRoutine_4; }
	inline RuntimeObject** get_address_of__mainRoutine_4() { return &____mainRoutine_4; }
	inline void set__mainRoutine_4(RuntimeObject* value)
	{
		____mainRoutine_4 = value;
		Il2CppCodeGenWriteBarrier((&____mainRoutine_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNNERBEHAVIOURPHYSIC_T6BFD13FA00A21B666A35D44CF27C5D3399D7B5A7_H
#ifndef RUNNERBEHAVIOURUPDATE_TD1E513D683A3F64AED4579077A3C2EB041BE5DBE_H
#define RUNNERBEHAVIOURUPDATE_TD1E513D683A3F64AED4579077A3C2EB041BE5DBE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Svelto.Tasks.Internal.RunnerBehaviourUpdate
struct  RunnerBehaviourUpdate_tD1E513D683A3F64AED4579077A3C2EB041BE5DBE  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Collections.IEnumerator Svelto.Tasks.Internal.RunnerBehaviourUpdate::_mainRoutine
	RuntimeObject* ____mainRoutine_4;

public:
	inline static int32_t get_offset_of__mainRoutine_4() { return static_cast<int32_t>(offsetof(RunnerBehaviourUpdate_tD1E513D683A3F64AED4579077A3C2EB041BE5DBE, ____mainRoutine_4)); }
	inline RuntimeObject* get__mainRoutine_4() const { return ____mainRoutine_4; }
	inline RuntimeObject** get_address_of__mainRoutine_4() { return &____mainRoutine_4; }
	inline void set__mainRoutine_4(RuntimeObject* value)
	{
		____mainRoutine_4 = value;
		Il2CppCodeGenWriteBarrier((&____mainRoutine_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNNERBEHAVIOURUPDATE_TD1E513D683A3F64AED4579077A3C2EB041BE5DBE_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7800 = { sizeof (RunnerBehaviourLate_tA8B0B5A353662B699DE3D47D303863DB2B8BCCD9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7800[1] = 
{
	RunnerBehaviourLate_tA8B0B5A353662B699DE3D47D303863DB2B8BCCD9::get_offset_of__mainRoutine_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7801 = { sizeof (RunnerBehaviourPhysic_t6BFD13FA00A21B666A35D44CF27C5D3399D7B5A7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7801[1] = 
{
	RunnerBehaviourPhysic_t6BFD13FA00A21B666A35D44CF27C5D3399D7B5A7::get_offset_of__mainRoutine_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7802 = { sizeof (RunnerBehaviourUpdate_tD1E513D683A3F64AED4579077A3C2EB041BE5DBE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7802[1] = 
{
	RunnerBehaviourUpdate_tD1E513D683A3F64AED4579077A3C2EB041BE5DBE::get_offset_of__mainRoutine_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7803 = { sizeof (UnityCoroutineRunner_t53316F54C9A4F7B0A4B98DD4AC3C3A95DED8C644), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7803[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7804 = { sizeof (RunningTasksInfo_t10813DB30B3A26BF8CF0830C17B2B3EE5DBF523C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7804[2] = 
{
	RunningTasksInfo_t10813DB30B3A26BF8CF0830C17B2B3EE5DBF523C::get_offset_of_count_0(),
	RunningTasksInfo_t10813DB30B3A26BF8CF0830C17B2B3EE5DBF523C::get_offset_of_runnerName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7805 = { sizeof (FlushTasksDel_t5FECFC8BE91DD02043575279E76105CB8643E11D), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7806 = { sizeof (FlushingOperation_tB0B9CC8E029067E8D130896A68A0E2D390E01027), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7806[1] = 
{
	FlushingOperation_tB0B9CC8E029067E8D130896A68A0E2D390E01027::get_offset_of_stopped_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7807 = { sizeof (HandItToUnity_t80AA186C6CA66E73DF88FE27AC23CDDC6CB7D92E)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7807[5] = 
{
	HandItToUnity_t80AA186C6CA66E73DF88FE27AC23CDDC6CB7D92E::get_offset_of__current_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HandItToUnity_t80AA186C6CA66E73DF88FE27AC23CDDC6CB7D92E::get_offset_of__task_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HandItToUnity_t80AA186C6CA66E73DF88FE27AC23CDDC6CB7D92E::get_offset_of__resumeOperation_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HandItToUnity_t80AA186C6CA66E73DF88FE27AC23CDDC6CB7D92E::get_offset_of__isDone_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HandItToUnity_t80AA186C6CA66E73DF88FE27AC23CDDC6CB7D92E::get_offset_of__flushingOperation_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7808 = { sizeof (U3CGetEnumeratorU3Ed__2_tC728F2BA628DC44F8693BCD2D1A5416CAF982F6B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7808[3] = 
{
	U3CGetEnumeratorU3Ed__2_tC728F2BA628DC44F8693BCD2D1A5416CAF982F6B::get_offset_of_U3CU3E1__state_0(),
	U3CGetEnumeratorU3Ed__2_tC728F2BA628DC44F8693BCD2D1A5416CAF982F6B::get_offset_of_U3CU3E2__current_1(),
	U3CGetEnumeratorU3Ed__2_tC728F2BA628DC44F8693BCD2D1A5416CAF982F6B::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7809 = { sizeof (U3CWaitUntilIsDoneU3Ed__4_t89C33E14EA38C81BCC3A831A776F16EC0FADB929), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7809[3] = 
{
	U3CWaitUntilIsDoneU3Ed__4_t89C33E14EA38C81BCC3A831A776F16EC0FADB929::get_offset_of_U3CU3E1__state_0(),
	U3CWaitUntilIsDoneU3Ed__4_t89C33E14EA38C81BCC3A831A776F16EC0FADB929::get_offset_of_U3CU3E2__current_1(),
	U3CWaitUntilIsDoneU3Ed__4_t89C33E14EA38C81BCC3A831A776F16EC0FADB929::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7810 = { sizeof (U3CU3Ec__DisplayClass4_0_t8719BF1E8CBECF96E1F5A6F6279B4F159FD0E905), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7810[1] = 
{
	U3CU3Ec__DisplayClass4_0_t8719BF1E8CBECF96E1F5A6F6279B4F159FD0E905::get_offset_of_runnerBehaviourForUnityCoroutine_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7811 = { sizeof (U3CU3Ec__DisplayClass4_1_tF99A27291549A7B9C21A7F68F055884AD2189B92), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7811[3] = 
{
	U3CU3Ec__DisplayClass4_1_tF99A27291549A7B9C21A7F68F055884AD2189B92::get_offset_of_coroutine_0(),
	U3CU3Ec__DisplayClass4_1_tF99A27291549A7B9C21A7F68F055884AD2189B92::get_offset_of_handItToUnity_1(),
	U3CU3Ec__DisplayClass4_1_tF99A27291549A7B9C21A7F68F055884AD2189B92::get_offset_of_CSU24U3CU3E8__locals1_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7812 = { sizeof (U3CU3Ec__DisplayClass4_2_tD4D341200EFF4FECA1B3C85655D8F8DDC535D7EF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7812[3] = 
{
	U3CU3Ec__DisplayClass4_2_tD4D341200EFF4FECA1B3C85655D8F8DDC535D7EF::get_offset_of_coroutine_0(),
	U3CU3Ec__DisplayClass4_2_tD4D341200EFF4FECA1B3C85655D8F8DDC535D7EF::get_offset_of_handItToUnity_1(),
	U3CU3Ec__DisplayClass4_2_tD4D341200EFF4FECA1B3C85655D8F8DDC535D7EF::get_offset_of_CSU24U3CU3E8__locals2_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7813 = { sizeof (U3CProcessU3Ed__4_t0599BE77B51BF8BA0D1A4FEB19C65DF6411780BD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7813[10] = 
{
	U3CProcessU3Ed__4_t0599BE77B51BF8BA0D1A4FEB19C65DF6411780BD::get_offset_of_U3CU3E1__state_0(),
	U3CProcessU3Ed__4_t0599BE77B51BF8BA0D1A4FEB19C65DF6411780BD::get_offset_of_U3CU3E2__current_1(),
	U3CProcessU3Ed__4_t0599BE77B51BF8BA0D1A4FEB19C65DF6411780BD::get_offset_of_runnerBehaviourForUnityCoroutine_2(),
	U3CProcessU3Ed__4_t0599BE77B51BF8BA0D1A4FEB19C65DF6411780BD::get_offset_of_flushingOperation_3(),
	U3CProcessU3Ed__4_t0599BE77B51BF8BA0D1A4FEB19C65DF6411780BD::get_offset_of_flushTaskDel_4(),
	U3CProcessU3Ed__4_t0599BE77B51BF8BA0D1A4FEB19C65DF6411780BD::get_offset_of_newTaskRoutines_5(),
	U3CProcessU3Ed__4_t0599BE77B51BF8BA0D1A4FEB19C65DF6411780BD::get_offset_of_coroutines_6(),
	U3CProcessU3Ed__4_t0599BE77B51BF8BA0D1A4FEB19C65DF6411780BD::get_offset_of_U3CU3E8__1_7(),
	U3CProcessU3Ed__4_t0599BE77B51BF8BA0D1A4FEB19C65DF6411780BD::get_offset_of_info_8(),
	U3CProcessU3Ed__4_t0599BE77B51BF8BA0D1A4FEB19C65DF6411780BD::get_offset_of_resumeOperation_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7814 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7814[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7815 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7815[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7816 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7817 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7818 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7818[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7819 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7819[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7820 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7820[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7821 = { sizeof (ExceptionHandlingEnumerator_tD302C26A89CC82D861B04FEAE9924EC35FDBB8A7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7821[3] = 
{
	ExceptionHandlingEnumerator_tD302C26A89CC82D861B04FEAE9924EC35FDBB8A7::get_offset_of_U3CsucceededU3Ek__BackingField_0(),
	ExceptionHandlingEnumerator_tD302C26A89CC82D861B04FEAE9924EC35FDBB8A7::get_offset_of_U3CerrorU3Ek__BackingField_1(),
	ExceptionHandlingEnumerator_tD302C26A89CC82D861B04FEAE9924EC35FDBB8A7::get_offset_of__enumerator_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7822 = { sizeof (UnityAsyncOperationEnumerator_t99BD541CC02B7C4704318292F5640C95376055E2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7822[1] = 
{
	UnityAsyncOperationEnumerator_t99BD541CC02B7C4704318292F5640C95376055E2::get_offset_of__asyncOp_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7823 = { sizeof (WaitForSecondsEnumerator_tFDC4A6ED9C9FA20517D0C1ECE31974BFFE450336), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7823[2] = 
{
	WaitForSecondsEnumerator_tFDC4A6ED9C9FA20517D0C1ECE31974BFFE450336::get_offset_of__future_0(),
	WaitForSecondsEnumerator_tFDC4A6ED9C9FA20517D0C1ECE31974BFFE450336::get_offset_of__seconds_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7824 = { sizeof (WaitForSignalEnumerator_tF2DC45D07FFFA1453981DFE83D13E257302A51D9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7824[8] = 
{
	WaitForSignalEnumerator_tF2DC45D07FFFA1453981DFE83D13E257302A51D9::get_offset_of__signal_0(),
	WaitForSignalEnumerator_tF2DC45D07FFFA1453981DFE83D13E257302A51D9::get_offset_of__return_1(),
	WaitForSignalEnumerator_tF2DC45D07FFFA1453981DFE83D13E257302A51D9::get_offset_of__timeout_2(),
	WaitForSignalEnumerator_tF2DC45D07FFFA1453981DFE83D13E257302A51D9::get_offset_of__autoreset_3(),
	WaitForSignalEnumerator_tF2DC45D07FFFA1453981DFE83D13E257302A51D9::get_offset_of__extraCondition_4(),
	WaitForSignalEnumerator_tF2DC45D07FFFA1453981DFE83D13E257302A51D9::get_offset_of__initialTimeOut_5(),
	WaitForSignalEnumerator_tF2DC45D07FFFA1453981DFE83D13E257302A51D9::get_offset_of__then_6(),
	WaitForSignalEnumerator_tF2DC45D07FFFA1453981DFE83D13E257302A51D9::get_offset_of__name_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7825 = { sizeof (WWWEnumerator_t33D003743ADA79EF3F980A318D211DCDF8728395), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7825[3] = 
{
	WWWEnumerator_t33D003743ADA79EF3F980A318D211DCDF8728395::get_offset_of__www_0(),
	WWWEnumerator_t33D003743ADA79EF3F980A318D211DCDF8728395::get_offset_of__timeOut_1(),
	WWWEnumerator_t33D003743ADA79EF3F980A318D211DCDF8728395::get_offset_of__then_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7826 = { sizeof (UnityWebRequestEnumerator_tFA04140CD01C06776B29AC3B069DE49AA20DE9B5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7826[1] = 
{
	UnityWebRequestEnumerator_tFA04140CD01C06776B29AC3B069DE49AA20DE9B5::get_offset_of__www_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7827 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7827[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7828 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7828[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7829 = { sizeof (WeakAction_t3A6FD94D96BFF231BD765C9373F83EAB36E42091), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7830 = { sizeof (WeakActionBase_tAB4B827D5D83E8C1254D634C2DE7F28E65378FA8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7830[2] = 
{
	WeakActionBase_tAB4B827D5D83E8C1254D634C2DE7F28E65378FA8::get_offset_of_objectRef_0(),
	WeakActionBase_tAB4B827D5D83E8C1254D634C2DE7F28E65378FA8::get_offset_of_method_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7831 = { sizeof (WeakEvent_t7CDC38859BD859DB5E8C7BAB3C218393D521A08F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7831[4] = 
{
	WeakEvent_t7CDC38859BD859DB5E8C7BAB3C218393D521A08F::get_offset_of__subscribers_0(),
	WeakEvent_t7CDC38859BD859DB5E8C7BAB3C218393D521A08F::get_offset_of__toRemove_1(),
	WeakEvent_t7CDC38859BD859DB5E8C7BAB3C218393D521A08F::get_offset_of__isIterating_2(),
	WeakEvent_t7CDC38859BD859DB5E8C7BAB3C218393D521A08F::get_offset_of__invoke_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7832 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7832[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7833 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7833[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7834 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7835 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7836 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7836[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7837 = { sizeof (ThreadUtility_t1146F63BFE7B9E624F728D6E50C7FDE43E32CE20), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7838 = { sizeof (ManualResetEventEx_t33E2A70F227B45E36C283FE859544123EC73BB78), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7839 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7840 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7841 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7842 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7842[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7843 = { sizeof (Observable_tA31B4EE4C7F2E1CDA6FBB36ADBE6163EB0AB76FC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7843[1] = 
{
	Observable_tA31B4EE4C7F2E1CDA6FBB36ADBE6163EB0AB76FC::get_offset_of_Notify_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7844 = { sizeof (Observer_tC137025318FE81433C99B6FF9523DE3DE2140EFF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7844[2] = 
{
	Observer_tC137025318FE81433C99B6FF9523DE3DE2140EFF::get_offset_of__actions_0(),
	Observer_tC137025318FE81433C99B6FF9523DE3DE2140EFF::get_offset_of__unsubscribe_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7845 = { sizeof (U3CU3Ec__DisplayClass0_0_t84601093A17D3002AB75354B1D0ECBD2875DB45C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7845[2] = 
{
	U3CU3Ec__DisplayClass0_0_t84601093A17D3002AB75354B1D0ECBD2875DB45C::get_offset_of_observable_0(),
	U3CU3Ec__DisplayClass0_0_t84601093A17D3002AB75354B1D0ECBD2875DB45C::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7846 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7847 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7848 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7849 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7849[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7850 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7850[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7851 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7852 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7853 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7853[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7854 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7854[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7855 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7855[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7856 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7856[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7857 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7857[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7858 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7858[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7859 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7859[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7860 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7861 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7861[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7862 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7862[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7863 = { sizeof (SyncMethods_tB7BAD7FA55C26D933D6837C1F920DA0978228397), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7864 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7864[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7865 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7865[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7866 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7866[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7867 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7867[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7868 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7868[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7869 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7869[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7870 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7870[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7871 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7871[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7872 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7872[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7873 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7874 = { sizeof (ContextNotifier_t042B33F0149C6FB6A14DB1ADA08C4E865363B815), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7874[2] = 
{
	ContextNotifier_t042B33F0149C6FB6A14DB1ADA08C4E865363B815::get_offset_of__toDeinitialize_0(),
	ContextNotifier_t042B33F0149C6FB6A14DB1ADA08C4E865363B815::get_offset_of__toInitialize_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7875 = { sizeof (GameObjectFactory_t0DE860347372F37B4150A4AA1509CA7E8334CD97), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7875[1] = 
{
	GameObjectFactory_t0DE860347372F37B4150A4AA1509CA7E8334CD97::get_offset_of__prefabs_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7876 = { sizeof (MonoBehaviourFactory_tEFD42E36CAE52E5FA9BE40F7155339E865CBF256), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7877 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7878 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7879 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7880 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7881 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7882 = { sizeof (CommandFactory_tE2D2B5AB0FD6859EC22401A958B0F12DD34CB5BE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7882[1] = 
{
	CommandFactory_tE2D2B5AB0FD6859EC22401A958B0F12DD34CB5BE::get_offset_of__onNewCommand_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7883 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7884 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7885 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7886 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7887 = { sizeof (ConsoleMethodAttribute_tAB9757928180DA2D91DF6D23A3AD25A9443D8E57), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7887[2] = 
{
	ConsoleMethodAttribute_tAB9757928180DA2D91DF6D23A3AD25A9443D8E57::get_offset_of_m_command_0(),
	ConsoleMethodAttribute_tAB9757928180DA2D91DF6D23A3AD25A9443D8E57::get_offset_of_m_description_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7888 = { sizeof (ConsoleMethodInfo_tF7C43BA80D45911840FECD796D2C33EA4A097739), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7888[4] = 
{
	ConsoleMethodInfo_tF7C43BA80D45911840FECD796D2C33EA4A097739::get_offset_of_method_0(),
	ConsoleMethodInfo_tF7C43BA80D45911840FECD796D2C33EA4A097739::get_offset_of_parameterTypes_1(),
	ConsoleMethodInfo_tF7C43BA80D45911840FECD796D2C33EA4A097739::get_offset_of_instance_2(),
	ConsoleMethodInfo_tF7C43BA80D45911840FECD796D2C33EA4A097739::get_offset_of_signature_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7889 = { sizeof (DebugLogConsole_t78D45C78CF71C30B964B789B9BE46D3F6991DAC2), -1, sizeof(DebugLogConsole_t78D45C78CF71C30B964B789B9BE46D3F6991DAC2_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7889[5] = 
{
	DebugLogConsole_t78D45C78CF71C30B964B789B9BE46D3F6991DAC2_StaticFields::get_offset_of_methods_0(),
	DebugLogConsole_t78D45C78CF71C30B964B789B9BE46D3F6991DAC2_StaticFields::get_offset_of_parseFunctions_1(),
	DebugLogConsole_t78D45C78CF71C30B964B789B9BE46D3F6991DAC2_StaticFields::get_offset_of_typeReadableNames_2(),
	DebugLogConsole_t78D45C78CF71C30B964B789B9BE46D3F6991DAC2_StaticFields::get_offset_of_commandArguments_3(),
	DebugLogConsole_t78D45C78CF71C30B964B789B9BE46D3F6991DAC2_StaticFields::get_offset_of_inputDelimiters_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7890 = { sizeof (ParseFunction_tF056F17516F5F71B23D983D5663CBD92C027C91A), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7891 = { sizeof (DebugLogEntry_t2A901F25FD9B697BBC7B6864E19EC3E8F2902D57), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7891[7] = 
{
	0,
	DebugLogEntry_t2A901F25FD9B697BBC7B6864E19EC3E8F2902D57::get_offset_of_logString_1(),
	DebugLogEntry_t2A901F25FD9B697BBC7B6864E19EC3E8F2902D57::get_offset_of_stackTrace_2(),
	DebugLogEntry_t2A901F25FD9B697BBC7B6864E19EC3E8F2902D57::get_offset_of_completeLog_3(),
	DebugLogEntry_t2A901F25FD9B697BBC7B6864E19EC3E8F2902D57::get_offset_of_logTypeSpriteRepresentation_4(),
	DebugLogEntry_t2A901F25FD9B697BBC7B6864E19EC3E8F2902D57::get_offset_of_count_5(),
	DebugLogEntry_t2A901F25FD9B697BBC7B6864E19EC3E8F2902D57::get_offset_of_hashValue_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7892 = { sizeof (DebugLogIndexList_t1A17DF0528FD925924AE4E53ABCE40AAE8699A88), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7892[2] = 
{
	DebugLogIndexList_t1A17DF0528FD925924AE4E53ABCE40AAE8699A88::get_offset_of_indices_0(),
	DebugLogIndexList_t1A17DF0528FD925924AE4E53ABCE40AAE8699A88::get_offset_of_size_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7893 = { sizeof (DebugLogItem_t706D6712831B240C3EA4B655657EE6EB2F37461C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7893[9] = 
{
	DebugLogItem_t706D6712831B240C3EA4B655657EE6EB2F37461C::get_offset_of_transformComponent_4(),
	DebugLogItem_t706D6712831B240C3EA4B655657EE6EB2F37461C::get_offset_of_imageComponent_5(),
	DebugLogItem_t706D6712831B240C3EA4B655657EE6EB2F37461C::get_offset_of_logText_6(),
	DebugLogItem_t706D6712831B240C3EA4B655657EE6EB2F37461C::get_offset_of_logTypeImage_7(),
	DebugLogItem_t706D6712831B240C3EA4B655657EE6EB2F37461C::get_offset_of_logCountParent_8(),
	DebugLogItem_t706D6712831B240C3EA4B655657EE6EB2F37461C::get_offset_of_logCountText_9(),
	DebugLogItem_t706D6712831B240C3EA4B655657EE6EB2F37461C::get_offset_of_logEntry_10(),
	DebugLogItem_t706D6712831B240C3EA4B655657EE6EB2F37461C::get_offset_of_entryIndex_11(),
	DebugLogItem_t706D6712831B240C3EA4B655657EE6EB2F37461C::get_offset_of_manager_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7894 = { sizeof (DebugLogFilter_t1019CE9E390D9D52C93424A58A317C866D50F6E1)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7894[6] = 
{
	DebugLogFilter_t1019CE9E390D9D52C93424A58A317C866D50F6E1::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7895 = { sizeof (DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0), -1, sizeof(DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7895[46] = 
{
	DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0_StaticFields::get_offset_of_instance_4(),
	DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0::get_offset_of_singleton_5(),
	DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0::get_offset_of_minimumHeight_6(),
	DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0::get_offset_of_startInPopupMode_7(),
	DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0::get_offset_of_clearCommandAfterExecution_8(),
	DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0::get_offset_of_receiveLogcatLogsInAndroid_9(),
	DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0::get_offset_of_logcatArguments_10(),
	DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0::get_offset_of_logItemPrefab_11(),
	DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0::get_offset_of_infoLog_12(),
	DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0::get_offset_of_warningLog_13(),
	DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0::get_offset_of_errorLog_14(),
	DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0::get_offset_of_logSpriteRepresentations_15(),
	DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0::get_offset_of_collapseButtonNormalColor_16(),
	DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0::get_offset_of_collapseButtonSelectedColor_17(),
	DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0::get_offset_of_filterButtonsNormalColor_18(),
	DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0::get_offset_of_filterButtonsSelectedColor_19(),
	DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0::get_offset_of_logWindowTR_20(),
	DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0::get_offset_of_canvasTR_21(),
	DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0::get_offset_of_logItemsContainer_22(),
	DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0::get_offset_of_commandInputField_23(),
	DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0::get_offset_of_collapseButton_24(),
	DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0::get_offset_of_filterInfoButton_25(),
	DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0::get_offset_of_filterWarningButton_26(),
	DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0::get_offset_of_filterErrorButton_27(),
	DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0::get_offset_of_infoEntryCountText_28(),
	DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0::get_offset_of_warningEntryCountText_29(),
	DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0::get_offset_of_errorEntryCountText_30(),
	DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0::get_offset_of_snapToBottomButton_31(),
	DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0::get_offset_of_infoEntryCount_32(),
	DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0::get_offset_of_warningEntryCount_33(),
	DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0::get_offset_of_errorEntryCount_34(),
	DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0::get_offset_of_logWindowCanvasGroup_35(),
	DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0::get_offset_of_isLogWindowVisible_36(),
	DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0::get_offset_of_screenDimensionsChanged_37(),
	DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0::get_offset_of_popupManager_38(),
	DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0::get_offset_of_logItemsScrollRect_39(),
	DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0::get_offset_of_recycledListView_40(),
	DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0::get_offset_of_isCollapseOn_41(),
	DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0::get_offset_of_logFilter_42(),
	DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0::get_offset_of_snapToBottom_43(),
	DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0::get_offset_of_collapsedLogEntries_44(),
	DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0::get_offset_of_collapsedLogEntriesMap_45(),
	DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0::get_offset_of_uncollapsedLogEntriesIndices_46(),
	DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0::get_offset_of_indicesOfListEntriesToShow_47(),
	DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0::get_offset_of_pooledLogItems_48(),
	DebugLogManager_tDD37FA850790FA89B87244EEA7860F0C4A4BF2C0::get_offset_of_nullPointerEventData_49(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7896 = { sizeof (DebugLogPopup_t9A2F2B314079CC1B8AE1CBA454189A6FB15C82A6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7896[17] = 
{
	DebugLogPopup_t9A2F2B314079CC1B8AE1CBA454189A6FB15C82A6::get_offset_of_popupTransform_4(),
	DebugLogPopup_t9A2F2B314079CC1B8AE1CBA454189A6FB15C82A6::get_offset_of_halfSize_5(),
	DebugLogPopup_t9A2F2B314079CC1B8AE1CBA454189A6FB15C82A6::get_offset_of_backgroundImage_6(),
	DebugLogPopup_t9A2F2B314079CC1B8AE1CBA454189A6FB15C82A6::get_offset_of_canvasGroup_7(),
	DebugLogPopup_t9A2F2B314079CC1B8AE1CBA454189A6FB15C82A6::get_offset_of_debugManager_8(),
	DebugLogPopup_t9A2F2B314079CC1B8AE1CBA454189A6FB15C82A6::get_offset_of_newInfoCountText_9(),
	DebugLogPopup_t9A2F2B314079CC1B8AE1CBA454189A6FB15C82A6::get_offset_of_newWarningCountText_10(),
	DebugLogPopup_t9A2F2B314079CC1B8AE1CBA454189A6FB15C82A6::get_offset_of_newErrorCountText_11(),
	DebugLogPopup_t9A2F2B314079CC1B8AE1CBA454189A6FB15C82A6::get_offset_of_newInfoCount_12(),
	DebugLogPopup_t9A2F2B314079CC1B8AE1CBA454189A6FB15C82A6::get_offset_of_newWarningCount_13(),
	DebugLogPopup_t9A2F2B314079CC1B8AE1CBA454189A6FB15C82A6::get_offset_of_newErrorCount_14(),
	DebugLogPopup_t9A2F2B314079CC1B8AE1CBA454189A6FB15C82A6::get_offset_of_normalColor_15(),
	DebugLogPopup_t9A2F2B314079CC1B8AE1CBA454189A6FB15C82A6::get_offset_of_alertColorInfo_16(),
	DebugLogPopup_t9A2F2B314079CC1B8AE1CBA454189A6FB15C82A6::get_offset_of_alertColorWarning_17(),
	DebugLogPopup_t9A2F2B314079CC1B8AE1CBA454189A6FB15C82A6::get_offset_of_alertColorError_18(),
	DebugLogPopup_t9A2F2B314079CC1B8AE1CBA454189A6FB15C82A6::get_offset_of_isPopupBeingDragged_19(),
	DebugLogPopup_t9A2F2B314079CC1B8AE1CBA454189A6FB15C82A6::get_offset_of_moveToPosCoroutine_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7897 = { sizeof (U3CMoveToPosAnimationU3Ed__24_t7A94C65C8B595767AA6DCA811A0E2346B634B612), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7897[6] = 
{
	U3CMoveToPosAnimationU3Ed__24_t7A94C65C8B595767AA6DCA811A0E2346B634B612::get_offset_of_U3CU3E1__state_0(),
	U3CMoveToPosAnimationU3Ed__24_t7A94C65C8B595767AA6DCA811A0E2346B634B612::get_offset_of_U3CU3E2__current_1(),
	U3CMoveToPosAnimationU3Ed__24_t7A94C65C8B595767AA6DCA811A0E2346B634B612::get_offset_of_U3CU3E4__this_2(),
	U3CMoveToPosAnimationU3Ed__24_t7A94C65C8B595767AA6DCA811A0E2346B634B612::get_offset_of_targetPos_3(),
	U3CMoveToPosAnimationU3Ed__24_t7A94C65C8B595767AA6DCA811A0E2346B634B612::get_offset_of_U3CmodifierU3E5__2_4(),
	U3CMoveToPosAnimationU3Ed__24_t7A94C65C8B595767AA6DCA811A0E2346B634B612::get_offset_of_U3CinitialPosU3E5__3_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7898 = { sizeof (DebugLogRecycledListView_t63DA7AF9FA87FF739B5A9AC577D26D9792A809BC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7898[20] = 
{
	DebugLogRecycledListView_t63DA7AF9FA87FF739B5A9AC577D26D9792A809BC::get_offset_of_transformComponent_4(),
	DebugLogRecycledListView_t63DA7AF9FA87FF739B5A9AC577D26D9792A809BC::get_offset_of_viewportTransform_5(),
	DebugLogRecycledListView_t63DA7AF9FA87FF739B5A9AC577D26D9792A809BC::get_offset_of_debugManager_6(),
	DebugLogRecycledListView_t63DA7AF9FA87FF739B5A9AC577D26D9792A809BC::get_offset_of_logItemNormalColor1_7(),
	DebugLogRecycledListView_t63DA7AF9FA87FF739B5A9AC577D26D9792A809BC::get_offset_of_logItemNormalColor2_8(),
	DebugLogRecycledListView_t63DA7AF9FA87FF739B5A9AC577D26D9792A809BC::get_offset_of_logItemSelectedColor_9(),
	DebugLogRecycledListView_t63DA7AF9FA87FF739B5A9AC577D26D9792A809BC::get_offset_of_manager_10(),
	DebugLogRecycledListView_t63DA7AF9FA87FF739B5A9AC577D26D9792A809BC::get_offset_of_logItemHeight_11(),
	DebugLogRecycledListView_t63DA7AF9FA87FF739B5A9AC577D26D9792A809BC::get_offset_of__1OverLogItemHeight_12(),
	DebugLogRecycledListView_t63DA7AF9FA87FF739B5A9AC577D26D9792A809BC::get_offset_of_viewportHeight_13(),
	DebugLogRecycledListView_t63DA7AF9FA87FF739B5A9AC577D26D9792A809BC::get_offset_of_collapsedLogEntries_14(),
	DebugLogRecycledListView_t63DA7AF9FA87FF739B5A9AC577D26D9792A809BC::get_offset_of_indicesOfEntriesToShow_15(),
	DebugLogRecycledListView_t63DA7AF9FA87FF739B5A9AC577D26D9792A809BC::get_offset_of_indexOfSelectedLogEntry_16(),
	DebugLogRecycledListView_t63DA7AF9FA87FF739B5A9AC577D26D9792A809BC::get_offset_of_positionOfSelectedLogEntry_17(),
	DebugLogRecycledListView_t63DA7AF9FA87FF739B5A9AC577D26D9792A809BC::get_offset_of_heightOfSelectedLogEntry_18(),
	DebugLogRecycledListView_t63DA7AF9FA87FF739B5A9AC577D26D9792A809BC::get_offset_of_deltaHeightOfSelectedLogEntry_19(),
	DebugLogRecycledListView_t63DA7AF9FA87FF739B5A9AC577D26D9792A809BC::get_offset_of_logItemsAtIndices_20(),
	DebugLogRecycledListView_t63DA7AF9FA87FF739B5A9AC577D26D9792A809BC::get_offset_of_isCollapseOn_21(),
	DebugLogRecycledListView_t63DA7AF9FA87FF739B5A9AC577D26D9792A809BC::get_offset_of_currentTopIndex_22(),
	DebugLogRecycledListView_t63DA7AF9FA87FF739B5A9AC577D26D9792A809BC::get_offset_of_currentBottomIndex_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7899 = { sizeof (DebugsOnScrollListener_tB028758B6A4B1EBC34838749A6FFDF5A6C9A8B52), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7899[2] = 
{
	DebugsOnScrollListener_tB028758B6A4B1EBC34838749A6FFDF5A6C9A8B52::get_offset_of_debugsScrollRect_4(),
	DebugsOnScrollListener_tB028758B6A4B1EBC34838749A6FFDF5A6C9A8B52::get_offset_of_debugLogManager_5(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
