﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Mono.Security.Cryptography.RSAManaged
struct RSAManaged_t386184E253177BF2DD9589A7E9C171DA8E718745;
// Mono.Security.Interface.Alert
struct Alert_tABF269545F2C583CCA47FF574E612DDAF232944E;
// Mono.Security.Interface.CipherSuiteCode[]
struct CipherSuiteCodeU5BU5D_t0EC37AD4A25BB94BA9AB4A9C0C4802BD79A07CC4;
// Mono.Security.Interface.ICertificateValidator
struct ICertificateValidator_t0C1A54E00D408ADCBA27E600BFAA216E7E7D31A3;
// Mono.Security.Interface.MonoLocalCertificateSelectionCallback
struct MonoLocalCertificateSelectionCallback_t657381EF916D4EDC456FA5A6AC948EFD7A481F0A;
// Mono.Security.Interface.MonoRemoteCertificateValidationCallback
struct MonoRemoteCertificateValidationCallback_t7A8DAD12B70CE3BB19BAAD04F587D5ED02385CC6;
// Mono.Security.Interface.ValidationResult
struct ValidationResult_tBBAD776F36C835C8DDB515B1747DEF3A45C058D7;
// Mono.Security.Protocol.Ntlm.Type2Message
struct Type2Message_t990283F48D41AECF1FFBDAA3A194CDE9C9078398;
// Mono.Security.Protocol.Tls.Alert
struct Alert_t2723053A22EA47B2855ABAEFC24AD57F16D79054;
// Mono.Security.Protocol.Tls.CertificateSelectionCallback
struct CertificateSelectionCallback_t2D981EF7A74CC271A9F086BB0F483FD6BCEDF5BE;
// Mono.Security.Protocol.Tls.CertificateValidationCallback
struct CertificateValidationCallback_t4FDC57D3B9528BD8D0564B4B3B2CF135E9CD72D3;
// Mono.Security.Protocol.Tls.CertificateValidationCallback2
struct CertificateValidationCallback2_t80B8CBB56C7605C91427868F9DE72B29837CCD61;
// Mono.Security.Protocol.Tls.CipherSuite
struct CipherSuite_t203D2B4A797805B775BEFB3B623962E7B407B6F7;
// Mono.Security.Protocol.Tls.CipherSuiteCollection
struct CipherSuiteCollection_tBC06EE1AF9F0FF543D1FF7740D09685ACAAFC41E;
// Mono.Security.Protocol.Tls.Context
struct Context_t17219BB52552BCACB15DAF9B6618835642F7F56F;
// Mono.Security.Protocol.Tls.Handshake.ClientCertificateType[]
struct ClientCertificateTypeU5BU5D_t06B6854DC307CF9A4DC750B89CEC349214A2147B;
// Mono.Security.Protocol.Tls.Handshake.HandshakeMessage
struct HandshakeMessage_t9B97FBAE0BAFCEA052F2B5EA880817CA1345298C;
// Mono.Security.Protocol.Tls.Handshake.Server.TlsClientCertificate
struct TlsClientCertificate_tDB7DEB656354449A78DCA4EC0EAD85EC4CC9712B;
// Mono.Security.Protocol.Tls.PrivateKeySelectionCallback
struct PrivateKeySelectionCallback_t403FB7EE7A49085BEDF77A79C77652CB5C610F7C;
// Mono.Security.Protocol.Tls.RecordProtocol
struct RecordProtocol_tFA651C8422AECEEE03AABEDB029EC83EB8833CFA;
// Mono.Security.Protocol.Tls.SecurityParameters
struct SecurityParameters_tC07B6E750AFC2037A7EBC754BD4DC1D37F41585E;
// Mono.Security.Protocol.Tls.SslServerStream
struct SslServerStream_t424CB4AC3D421FAF8A73A20520E3E4A4934E2092;
// Mono.Security.Protocol.Tls.SslStreamBase/InternalAsyncResult
struct InternalAsyncResult_tD29ADBE4ACE8353C13107D2E648FFA1F04DCC4C0;
// Mono.Security.Protocol.Tls.TlsClientSettings
struct TlsClientSettings_t7521F1D5285E4B38591EC5E724E198CED35B39E0;
// Mono.Security.Protocol.Tls.TlsServerSettings
struct TlsServerSettings_tD19093EA2CFCCD92F9B0FE63A9B2FAA9B4B45788;
// Mono.Security.Protocol.Tls.TlsStream
struct TlsStream_tB739E33AAABEC563984AD1EA75D7F4CFB4BF27E5;
// Mono.Security.X509.X509CertificateCollection
struct X509CertificateCollection_t423BA1B9FAA983BA745023994C648C6DAC3E5A1A;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.ArrayList
struct ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.Exception
struct Exception_t;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.IO.MemoryStream
struct MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C;
// System.IO.Stream
struct Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7;
// System.IO.Stream/ReadWriteTask
struct ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80;
// System.Int16[]
struct Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.Net.HttpWebRequest
struct HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.Security.Cryptography.AsymmetricAlgorithm
struct AsymmetricAlgorithm_t9F811260245370BD8786A849DBF9F8054F97F4CB;
// System.Security.Cryptography.HashAlgorithm
struct HashAlgorithm_t65659695B16C0BBF05707BF45191A97DC156D6BA;
// System.Security.Cryptography.ICryptoTransform
struct ICryptoTransform_t43C29A7F3A8C2DDAC9F3BF9BF739B03E4D5DE9A9;
// System.Security.Cryptography.KeySizes[]
struct KeySizesU5BU5D_t934CCA482596402177BAF86727F169872D74934E;
// System.Security.Cryptography.KeyedHashAlgorithm
struct KeyedHashAlgorithm_t83CFA2CA5A4F0F39B747E61D013CB5EB919D218B;
// System.Security.Cryptography.RSA
struct RSA_tB6C4B434B2AC02E3F8981DB2908C2018E251D145;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2;
// System.Security.Cryptography.SymmetricAlgorithm
struct SymmetricAlgorithm_t0A2EC7E7AD8B8976832B4F0AC432B691F862E789;
// System.Security.Cryptography.X509Certificates.X509Certificate
struct X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2;
// System.Security.Cryptography.X509Certificates.X509CertificateCollection
struct X509CertificateCollection_t824A6C58D0D1B4A7CAE30F26CE8EE4B23A8A1833;
// System.Security.Cryptography.X509Certificates.X509Chain
struct X509Chain_t4A28E9A30CBB331C9B68AE4AFCB30625C6C8B538;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Threading.ManualResetEvent
struct ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408;
// System.Threading.SemaphoreSlim
struct SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048;
// System.UInt32[]
struct UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;

struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef CRYPTOCONVERT_TF1F175C2F2C9E65FE7D5FBF0D434B964E4CAFF76_H
#define CRYPTOCONVERT_TF1F175C2F2C9E65FE7D5FBF0D434B964E4CAFF76_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Cryptography.CryptoConvert
struct  CryptoConvert_tF1F175C2F2C9E65FE7D5FBF0D434B964E4CAFF76  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRYPTOCONVERT_TF1F175C2F2C9E65FE7D5FBF0D434B964E4CAFF76_H
#ifndef KEYBUILDER_T7D70B1A038D0911A8BC47E8BABEDF8163FA22EA7_H
#define KEYBUILDER_T7D70B1A038D0911A8BC47E8BABEDF8163FA22EA7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Cryptography.KeyBuilder
struct  KeyBuilder_t7D70B1A038D0911A8BC47E8BABEDF8163FA22EA7  : public RuntimeObject
{
public:

public:
};

struct KeyBuilder_t7D70B1A038D0911A8BC47E8BABEDF8163FA22EA7_StaticFields
{
public:
	// System.Security.Cryptography.RandomNumberGenerator Mono.Security.Cryptography.KeyBuilder::rng
	RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * ___rng_0;

public:
	inline static int32_t get_offset_of_rng_0() { return static_cast<int32_t>(offsetof(KeyBuilder_t7D70B1A038D0911A8BC47E8BABEDF8163FA22EA7_StaticFields, ___rng_0)); }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * get_rng_0() const { return ___rng_0; }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 ** get_address_of_rng_0() { return &___rng_0; }
	inline void set_rng_0(RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * value)
	{
		___rng_0 = value;
		Il2CppCodeGenWriteBarrier((&___rng_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYBUILDER_T7D70B1A038D0911A8BC47E8BABEDF8163FA22EA7_H
#ifndef PKCS1_T5ABACACDDA0FE1D9A3D2ECD6BEB8815C8431B222_H
#define PKCS1_T5ABACACDDA0FE1D9A3D2ECD6BEB8815C8431B222_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Cryptography.PKCS1
struct  PKCS1_t5ABACACDDA0FE1D9A3D2ECD6BEB8815C8431B222  : public RuntimeObject
{
public:

public:
};

struct PKCS1_t5ABACACDDA0FE1D9A3D2ECD6BEB8815C8431B222_StaticFields
{
public:
	// System.Byte[] Mono.Security.Cryptography.PKCS1::emptySHA1
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___emptySHA1_0;
	// System.Byte[] Mono.Security.Cryptography.PKCS1::emptySHA256
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___emptySHA256_1;
	// System.Byte[] Mono.Security.Cryptography.PKCS1::emptySHA384
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___emptySHA384_2;
	// System.Byte[] Mono.Security.Cryptography.PKCS1::emptySHA512
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___emptySHA512_3;

public:
	inline static int32_t get_offset_of_emptySHA1_0() { return static_cast<int32_t>(offsetof(PKCS1_t5ABACACDDA0FE1D9A3D2ECD6BEB8815C8431B222_StaticFields, ___emptySHA1_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_emptySHA1_0() const { return ___emptySHA1_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_emptySHA1_0() { return &___emptySHA1_0; }
	inline void set_emptySHA1_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___emptySHA1_0 = value;
		Il2CppCodeGenWriteBarrier((&___emptySHA1_0), value);
	}

	inline static int32_t get_offset_of_emptySHA256_1() { return static_cast<int32_t>(offsetof(PKCS1_t5ABACACDDA0FE1D9A3D2ECD6BEB8815C8431B222_StaticFields, ___emptySHA256_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_emptySHA256_1() const { return ___emptySHA256_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_emptySHA256_1() { return &___emptySHA256_1; }
	inline void set_emptySHA256_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___emptySHA256_1 = value;
		Il2CppCodeGenWriteBarrier((&___emptySHA256_1), value);
	}

	inline static int32_t get_offset_of_emptySHA384_2() { return static_cast<int32_t>(offsetof(PKCS1_t5ABACACDDA0FE1D9A3D2ECD6BEB8815C8431B222_StaticFields, ___emptySHA384_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_emptySHA384_2() const { return ___emptySHA384_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_emptySHA384_2() { return &___emptySHA384_2; }
	inline void set_emptySHA384_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___emptySHA384_2 = value;
		Il2CppCodeGenWriteBarrier((&___emptySHA384_2), value);
	}

	inline static int32_t get_offset_of_emptySHA512_3() { return static_cast<int32_t>(offsetof(PKCS1_t5ABACACDDA0FE1D9A3D2ECD6BEB8815C8431B222_StaticFields, ___emptySHA512_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_emptySHA512_3() const { return ___emptySHA512_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_emptySHA512_3() { return &___emptySHA512_3; }
	inline void set_emptySHA512_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___emptySHA512_3 = value;
		Il2CppCodeGenWriteBarrier((&___emptySHA512_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKCS1_T5ABACACDDA0FE1D9A3D2ECD6BEB8815C8431B222_H
#ifndef PKCS8_TD24BB8D5DD62F9EF2DCDB5E3B03DB4AB19415656_H
#define PKCS8_TD24BB8D5DD62F9EF2DCDB5E3B03DB4AB19415656_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Cryptography.PKCS8
struct  PKCS8_tD24BB8D5DD62F9EF2DCDB5E3B03DB4AB19415656  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKCS8_TD24BB8D5DD62F9EF2DCDB5E3B03DB4AB19415656_H
#ifndef ENCRYPTEDPRIVATEKEYINFO_TF582FAE09721F4F2BB353D5C2E28682FFB9C303D_H
#define ENCRYPTEDPRIVATEKEYINFO_TF582FAE09721F4F2BB353D5C2E28682FFB9C303D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Cryptography.PKCS8_EncryptedPrivateKeyInfo
struct  EncryptedPrivateKeyInfo_tF582FAE09721F4F2BB353D5C2E28682FFB9C303D  : public RuntimeObject
{
public:
	// System.String Mono.Security.Cryptography.PKCS8_EncryptedPrivateKeyInfo::_algorithm
	String_t* ____algorithm_0;
	// System.Byte[] Mono.Security.Cryptography.PKCS8_EncryptedPrivateKeyInfo::_salt
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____salt_1;
	// System.Int32 Mono.Security.Cryptography.PKCS8_EncryptedPrivateKeyInfo::_iterations
	int32_t ____iterations_2;
	// System.Byte[] Mono.Security.Cryptography.PKCS8_EncryptedPrivateKeyInfo::_data
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____data_3;

public:
	inline static int32_t get_offset_of__algorithm_0() { return static_cast<int32_t>(offsetof(EncryptedPrivateKeyInfo_tF582FAE09721F4F2BB353D5C2E28682FFB9C303D, ____algorithm_0)); }
	inline String_t* get__algorithm_0() const { return ____algorithm_0; }
	inline String_t** get_address_of__algorithm_0() { return &____algorithm_0; }
	inline void set__algorithm_0(String_t* value)
	{
		____algorithm_0 = value;
		Il2CppCodeGenWriteBarrier((&____algorithm_0), value);
	}

	inline static int32_t get_offset_of__salt_1() { return static_cast<int32_t>(offsetof(EncryptedPrivateKeyInfo_tF582FAE09721F4F2BB353D5C2E28682FFB9C303D, ____salt_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__salt_1() const { return ____salt_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__salt_1() { return &____salt_1; }
	inline void set__salt_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____salt_1 = value;
		Il2CppCodeGenWriteBarrier((&____salt_1), value);
	}

	inline static int32_t get_offset_of__iterations_2() { return static_cast<int32_t>(offsetof(EncryptedPrivateKeyInfo_tF582FAE09721F4F2BB353D5C2E28682FFB9C303D, ____iterations_2)); }
	inline int32_t get__iterations_2() const { return ____iterations_2; }
	inline int32_t* get_address_of__iterations_2() { return &____iterations_2; }
	inline void set__iterations_2(int32_t value)
	{
		____iterations_2 = value;
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(EncryptedPrivateKeyInfo_tF582FAE09721F4F2BB353D5C2E28682FFB9C303D, ____data_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__data_3() const { return ____data_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCRYPTEDPRIVATEKEYINFO_TF582FAE09721F4F2BB353D5C2E28682FFB9C303D_H
#ifndef PRIVATEKEYINFO_T053A73C524EC661748073EC908D290E2856DB5B9_H
#define PRIVATEKEYINFO_T053A73C524EC661748073EC908D290E2856DB5B9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Cryptography.PKCS8_PrivateKeyInfo
struct  PrivateKeyInfo_t053A73C524EC661748073EC908D290E2856DB5B9  : public RuntimeObject
{
public:
	// System.Int32 Mono.Security.Cryptography.PKCS8_PrivateKeyInfo::_version
	int32_t ____version_0;
	// System.String Mono.Security.Cryptography.PKCS8_PrivateKeyInfo::_algorithm
	String_t* ____algorithm_1;
	// System.Byte[] Mono.Security.Cryptography.PKCS8_PrivateKeyInfo::_key
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____key_2;
	// System.Collections.ArrayList Mono.Security.Cryptography.PKCS8_PrivateKeyInfo::_list
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ____list_3;

public:
	inline static int32_t get_offset_of__version_0() { return static_cast<int32_t>(offsetof(PrivateKeyInfo_t053A73C524EC661748073EC908D290E2856DB5B9, ____version_0)); }
	inline int32_t get__version_0() const { return ____version_0; }
	inline int32_t* get_address_of__version_0() { return &____version_0; }
	inline void set__version_0(int32_t value)
	{
		____version_0 = value;
	}

	inline static int32_t get_offset_of__algorithm_1() { return static_cast<int32_t>(offsetof(PrivateKeyInfo_t053A73C524EC661748073EC908D290E2856DB5B9, ____algorithm_1)); }
	inline String_t* get__algorithm_1() const { return ____algorithm_1; }
	inline String_t** get_address_of__algorithm_1() { return &____algorithm_1; }
	inline void set__algorithm_1(String_t* value)
	{
		____algorithm_1 = value;
		Il2CppCodeGenWriteBarrier((&____algorithm_1), value);
	}

	inline static int32_t get_offset_of__key_2() { return static_cast<int32_t>(offsetof(PrivateKeyInfo_t053A73C524EC661748073EC908D290E2856DB5B9, ____key_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__key_2() const { return ____key_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__key_2() { return &____key_2; }
	inline void set__key_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____key_2 = value;
		Il2CppCodeGenWriteBarrier((&____key_2), value);
	}

	inline static int32_t get_offset_of__list_3() { return static_cast<int32_t>(offsetof(PrivateKeyInfo_t053A73C524EC661748073EC908D290E2856DB5B9, ____list_3)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get__list_3() const { return ____list_3; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of__list_3() { return &____list_3; }
	inline void set__list_3(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		____list_3 = value;
		Il2CppCodeGenWriteBarrier((&____list_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRIVATEKEYINFO_T053A73C524EC661748073EC908D290E2856DB5B9_H
#ifndef CERTIFICATEVALIDATIONHELPER_T700A78AF16D5DA1698501C49E6894B1B67963F86_H
#define CERTIFICATEVALIDATIONHELPER_T700A78AF16D5DA1698501C49E6894B1B67963F86_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Interface.CertificateValidationHelper
struct  CertificateValidationHelper_t700A78AF16D5DA1698501C49E6894B1B67963F86  : public RuntimeObject
{
public:

public:
};

struct CertificateValidationHelper_t700A78AF16D5DA1698501C49E6894B1B67963F86_StaticFields
{
public:
	// System.Boolean Mono.Security.Interface.CertificateValidationHelper::noX509Chain
	bool ___noX509Chain_0;
	// System.Boolean Mono.Security.Interface.CertificateValidationHelper::supportsTrustAnchors
	bool ___supportsTrustAnchors_1;

public:
	inline static int32_t get_offset_of_noX509Chain_0() { return static_cast<int32_t>(offsetof(CertificateValidationHelper_t700A78AF16D5DA1698501C49E6894B1B67963F86_StaticFields, ___noX509Chain_0)); }
	inline bool get_noX509Chain_0() const { return ___noX509Chain_0; }
	inline bool* get_address_of_noX509Chain_0() { return &___noX509Chain_0; }
	inline void set_noX509Chain_0(bool value)
	{
		___noX509Chain_0 = value;
	}

	inline static int32_t get_offset_of_supportsTrustAnchors_1() { return static_cast<int32_t>(offsetof(CertificateValidationHelper_t700A78AF16D5DA1698501C49E6894B1B67963F86_StaticFields, ___supportsTrustAnchors_1)); }
	inline bool get_supportsTrustAnchors_1() const { return ___supportsTrustAnchors_1; }
	inline bool* get_address_of_supportsTrustAnchors_1() { return &___supportsTrustAnchors_1; }
	inline void set_supportsTrustAnchors_1(bool value)
	{
		___supportsTrustAnchors_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CERTIFICATEVALIDATIONHELPER_T700A78AF16D5DA1698501C49E6894B1B67963F86_H
#ifndef MONOTLSPROVIDER_TDCD056C5BBBE59ED6BAF63F25952B406C1143C27_H
#define MONOTLSPROVIDER_TDCD056C5BBBE59ED6BAF63F25952B406C1143C27_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Interface.MonoTlsProvider
struct  MonoTlsProvider_tDCD056C5BBBE59ED6BAF63F25952B406C1143C27  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOTLSPROVIDER_TDCD056C5BBBE59ED6BAF63F25952B406C1143C27_H
#ifndef MONOTLSPROVIDERFACTORY_T7BC164DB50C3AD4B37141048F27F7E74863146BB_H
#define MONOTLSPROVIDERFACTORY_T7BC164DB50C3AD4B37141048F27F7E74863146BB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Interface.MonoTlsProviderFactory
struct  MonoTlsProviderFactory_t7BC164DB50C3AD4B37141048F27F7E74863146BB  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOTLSPROVIDERFACTORY_T7BC164DB50C3AD4B37141048F27F7E74863146BB_H
#ifndef CHALLENGERESPONSE_T2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B_H
#define CHALLENGERESPONSE_T2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Ntlm.ChallengeResponse
struct  ChallengeResponse_t2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B  : public RuntimeObject
{
public:
	// System.Boolean Mono.Security.Protocol.Ntlm.ChallengeResponse::_disposed
	bool ____disposed_2;
	// System.Byte[] Mono.Security.Protocol.Ntlm.ChallengeResponse::_challenge
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____challenge_3;
	// System.Byte[] Mono.Security.Protocol.Ntlm.ChallengeResponse::_lmpwd
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____lmpwd_4;
	// System.Byte[] Mono.Security.Protocol.Ntlm.ChallengeResponse::_ntpwd
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____ntpwd_5;

public:
	inline static int32_t get_offset_of__disposed_2() { return static_cast<int32_t>(offsetof(ChallengeResponse_t2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B, ____disposed_2)); }
	inline bool get__disposed_2() const { return ____disposed_2; }
	inline bool* get_address_of__disposed_2() { return &____disposed_2; }
	inline void set__disposed_2(bool value)
	{
		____disposed_2 = value;
	}

	inline static int32_t get_offset_of__challenge_3() { return static_cast<int32_t>(offsetof(ChallengeResponse_t2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B, ____challenge_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__challenge_3() const { return ____challenge_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__challenge_3() { return &____challenge_3; }
	inline void set__challenge_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____challenge_3 = value;
		Il2CppCodeGenWriteBarrier((&____challenge_3), value);
	}

	inline static int32_t get_offset_of__lmpwd_4() { return static_cast<int32_t>(offsetof(ChallengeResponse_t2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B, ____lmpwd_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__lmpwd_4() const { return ____lmpwd_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__lmpwd_4() { return &____lmpwd_4; }
	inline void set__lmpwd_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____lmpwd_4 = value;
		Il2CppCodeGenWriteBarrier((&____lmpwd_4), value);
	}

	inline static int32_t get_offset_of__ntpwd_5() { return static_cast<int32_t>(offsetof(ChallengeResponse_t2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B, ____ntpwd_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__ntpwd_5() const { return ____ntpwd_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__ntpwd_5() { return &____ntpwd_5; }
	inline void set__ntpwd_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____ntpwd_5 = value;
		Il2CppCodeGenWriteBarrier((&____ntpwd_5), value);
	}
};

struct ChallengeResponse_t2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B_StaticFields
{
public:
	// System.Byte[] Mono.Security.Protocol.Ntlm.ChallengeResponse::magic
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___magic_0;
	// System.Byte[] Mono.Security.Protocol.Ntlm.ChallengeResponse::nullEncMagic
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___nullEncMagic_1;

public:
	inline static int32_t get_offset_of_magic_0() { return static_cast<int32_t>(offsetof(ChallengeResponse_t2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B_StaticFields, ___magic_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_magic_0() const { return ___magic_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_magic_0() { return &___magic_0; }
	inline void set_magic_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___magic_0 = value;
		Il2CppCodeGenWriteBarrier((&___magic_0), value);
	}

	inline static int32_t get_offset_of_nullEncMagic_1() { return static_cast<int32_t>(offsetof(ChallengeResponse_t2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B_StaticFields, ___nullEncMagic_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_nullEncMagic_1() const { return ___nullEncMagic_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_nullEncMagic_1() { return &___nullEncMagic_1; }
	inline void set_nullEncMagic_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___nullEncMagic_1 = value;
		Il2CppCodeGenWriteBarrier((&___nullEncMagic_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHALLENGERESPONSE_T2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B_H
#ifndef CHALLENGERESPONSE2_T2F5817D7717011A7F4C0DE0015D144A2292CAD77_H
#define CHALLENGERESPONSE2_T2F5817D7717011A7F4C0DE0015D144A2292CAD77_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Ntlm.ChallengeResponse2
struct  ChallengeResponse2_t2F5817D7717011A7F4C0DE0015D144A2292CAD77  : public RuntimeObject
{
public:

public:
};

struct ChallengeResponse2_t2F5817D7717011A7F4C0DE0015D144A2292CAD77_StaticFields
{
public:
	// System.Byte[] Mono.Security.Protocol.Ntlm.ChallengeResponse2::magic
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___magic_0;
	// System.Byte[] Mono.Security.Protocol.Ntlm.ChallengeResponse2::nullEncMagic
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___nullEncMagic_1;

public:
	inline static int32_t get_offset_of_magic_0() { return static_cast<int32_t>(offsetof(ChallengeResponse2_t2F5817D7717011A7F4C0DE0015D144A2292CAD77_StaticFields, ___magic_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_magic_0() const { return ___magic_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_magic_0() { return &___magic_0; }
	inline void set_magic_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___magic_0 = value;
		Il2CppCodeGenWriteBarrier((&___magic_0), value);
	}

	inline static int32_t get_offset_of_nullEncMagic_1() { return static_cast<int32_t>(offsetof(ChallengeResponse2_t2F5817D7717011A7F4C0DE0015D144A2292CAD77_StaticFields, ___nullEncMagic_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_nullEncMagic_1() const { return ___nullEncMagic_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_nullEncMagic_1() { return &___nullEncMagic_1; }
	inline void set_nullEncMagic_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___nullEncMagic_1 = value;
		Il2CppCodeGenWriteBarrier((&___nullEncMagic_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHALLENGERESPONSE2_T2F5817D7717011A7F4C0DE0015D144A2292CAD77_H
#ifndef DEBUGHELPER_TC0F42268BBF77BF578E27BD7458075BC06639521_H
#define DEBUGHELPER_TC0F42268BBF77BF578E27BD7458075BC06639521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.DebugHelper
struct  DebugHelper_tC0F42268BBF77BF578E27BD7458075BC06639521  : public RuntimeObject
{
public:

public:
};

struct DebugHelper_tC0F42268BBF77BF578E27BD7458075BC06639521_StaticFields
{
public:
	// System.Boolean Mono.Security.Protocol.Tls.DebugHelper::isInitialized
	bool ___isInitialized_0;

public:
	inline static int32_t get_offset_of_isInitialized_0() { return static_cast<int32_t>(offsetof(DebugHelper_tC0F42268BBF77BF578E27BD7458075BC06639521_StaticFields, ___isInitialized_0)); }
	inline bool get_isInitialized_0() const { return ___isInitialized_0; }
	inline bool* get_address_of_isInitialized_0() { return &___isInitialized_0; }
	inline void set_isInitialized_0(bool value)
	{
		___isInitialized_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGHELPER_TC0F42268BBF77BF578E27BD7458075BC06639521_H
#ifndef U3CU3EC_T5B194E5166B48CF5C8F9F038AABA64D57F961749_H
#define U3CU3EC_T5B194E5166B48CF5C8F9F038AABA64D57F961749_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.HttpsClientStream_<>c
struct  U3CU3Ec_t5B194E5166B48CF5C8F9F038AABA64D57F961749  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t5B194E5166B48CF5C8F9F038AABA64D57F961749_StaticFields
{
public:
	// Mono.Security.Protocol.Tls.HttpsClientStream_<>c Mono.Security.Protocol.Tls.HttpsClientStream_<>c::<>9
	U3CU3Ec_t5B194E5166B48CF5C8F9F038AABA64D57F961749 * ___U3CU3E9_0;
	// Mono.Security.Protocol.Tls.CertificateSelectionCallback Mono.Security.Protocol.Tls.HttpsClientStream_<>c::<>9__2_0
	CertificateSelectionCallback_t2D981EF7A74CC271A9F086BB0F483FD6BCEDF5BE * ___U3CU3E9__2_0_1;
	// Mono.Security.Protocol.Tls.PrivateKeySelectionCallback Mono.Security.Protocol.Tls.HttpsClientStream_<>c::<>9__2_1
	PrivateKeySelectionCallback_t403FB7EE7A49085BEDF77A79C77652CB5C610F7C * ___U3CU3E9__2_1_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t5B194E5166B48CF5C8F9F038AABA64D57F961749_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t5B194E5166B48CF5C8F9F038AABA64D57F961749 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t5B194E5166B48CF5C8F9F038AABA64D57F961749 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t5B194E5166B48CF5C8F9F038AABA64D57F961749 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__2_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t5B194E5166B48CF5C8F9F038AABA64D57F961749_StaticFields, ___U3CU3E9__2_0_1)); }
	inline CertificateSelectionCallback_t2D981EF7A74CC271A9F086BB0F483FD6BCEDF5BE * get_U3CU3E9__2_0_1() const { return ___U3CU3E9__2_0_1; }
	inline CertificateSelectionCallback_t2D981EF7A74CC271A9F086BB0F483FD6BCEDF5BE ** get_address_of_U3CU3E9__2_0_1() { return &___U3CU3E9__2_0_1; }
	inline void set_U3CU3E9__2_0_1(CertificateSelectionCallback_t2D981EF7A74CC271A9F086BB0F483FD6BCEDF5BE * value)
	{
		___U3CU3E9__2_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__2_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__2_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t5B194E5166B48CF5C8F9F038AABA64D57F961749_StaticFields, ___U3CU3E9__2_1_2)); }
	inline PrivateKeySelectionCallback_t403FB7EE7A49085BEDF77A79C77652CB5C610F7C * get_U3CU3E9__2_1_2() const { return ___U3CU3E9__2_1_2; }
	inline PrivateKeySelectionCallback_t403FB7EE7A49085BEDF77A79C77652CB5C610F7C ** get_address_of_U3CU3E9__2_1_2() { return &___U3CU3E9__2_1_2; }
	inline void set_U3CU3E9__2_1_2(PrivateKeySelectionCallback_t403FB7EE7A49085BEDF77A79C77652CB5C610F7C * value)
	{
		___U3CU3E9__2_1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__2_1_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T5B194E5166B48CF5C8F9F038AABA64D57F961749_H
#ifndef RECORDPROTOCOL_TFA651C8422AECEEE03AABEDB029EC83EB8833CFA_H
#define RECORDPROTOCOL_TFA651C8422AECEEE03AABEDB029EC83EB8833CFA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.RecordProtocol
struct  RecordProtocol_tFA651C8422AECEEE03AABEDB029EC83EB8833CFA  : public RuntimeObject
{
public:
	// System.IO.Stream Mono.Security.Protocol.Tls.RecordProtocol::innerStream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___innerStream_1;
	// Mono.Security.Protocol.Tls.Context Mono.Security.Protocol.Tls.RecordProtocol::context
	Context_t17219BB52552BCACB15DAF9B6618835642F7F56F * ___context_2;

public:
	inline static int32_t get_offset_of_innerStream_1() { return static_cast<int32_t>(offsetof(RecordProtocol_tFA651C8422AECEEE03AABEDB029EC83EB8833CFA, ___innerStream_1)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_innerStream_1() const { return ___innerStream_1; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_innerStream_1() { return &___innerStream_1; }
	inline void set_innerStream_1(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___innerStream_1 = value;
		Il2CppCodeGenWriteBarrier((&___innerStream_1), value);
	}

	inline static int32_t get_offset_of_context_2() { return static_cast<int32_t>(offsetof(RecordProtocol_tFA651C8422AECEEE03AABEDB029EC83EB8833CFA, ___context_2)); }
	inline Context_t17219BB52552BCACB15DAF9B6618835642F7F56F * get_context_2() const { return ___context_2; }
	inline Context_t17219BB52552BCACB15DAF9B6618835642F7F56F ** get_address_of_context_2() { return &___context_2; }
	inline void set_context_2(Context_t17219BB52552BCACB15DAF9B6618835642F7F56F * value)
	{
		___context_2 = value;
		Il2CppCodeGenWriteBarrier((&___context_2), value);
	}
};

struct RecordProtocol_tFA651C8422AECEEE03AABEDB029EC83EB8833CFA_StaticFields
{
public:
	// System.Threading.ManualResetEvent Mono.Security.Protocol.Tls.RecordProtocol::record_processing
	ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * ___record_processing_0;

public:
	inline static int32_t get_offset_of_record_processing_0() { return static_cast<int32_t>(offsetof(RecordProtocol_tFA651C8422AECEEE03AABEDB029EC83EB8833CFA_StaticFields, ___record_processing_0)); }
	inline ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * get_record_processing_0() const { return ___record_processing_0; }
	inline ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 ** get_address_of_record_processing_0() { return &___record_processing_0; }
	inline void set_record_processing_0(ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * value)
	{
		___record_processing_0 = value;
		Il2CppCodeGenWriteBarrier((&___record_processing_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECORDPROTOCOL_TFA651C8422AECEEE03AABEDB029EC83EB8833CFA_H
#ifndef RECEIVERECORDASYNCRESULT_TF2E4546C7F2475C74BE9023741AFFA069FC68752_H
#define RECEIVERECORDASYNCRESULT_TF2E4546C7F2475C74BE9023741AFFA069FC68752_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.RecordProtocol_ReceiveRecordAsyncResult
struct  ReceiveRecordAsyncResult_tF2E4546C7F2475C74BE9023741AFFA069FC68752  : public RuntimeObject
{
public:
	// System.Object Mono.Security.Protocol.Tls.RecordProtocol_ReceiveRecordAsyncResult::locker
	RuntimeObject * ___locker_0;
	// System.AsyncCallback Mono.Security.Protocol.Tls.RecordProtocol_ReceiveRecordAsyncResult::_userCallback
	AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ____userCallback_1;
	// System.Object Mono.Security.Protocol.Tls.RecordProtocol_ReceiveRecordAsyncResult::_userState
	RuntimeObject * ____userState_2;
	// System.Exception Mono.Security.Protocol.Tls.RecordProtocol_ReceiveRecordAsyncResult::_asyncException
	Exception_t * ____asyncException_3;
	// System.Threading.ManualResetEvent Mono.Security.Protocol.Tls.RecordProtocol_ReceiveRecordAsyncResult::handle
	ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * ___handle_4;
	// System.Byte[] Mono.Security.Protocol.Tls.RecordProtocol_ReceiveRecordAsyncResult::_resultingBuffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____resultingBuffer_5;
	// System.IO.Stream Mono.Security.Protocol.Tls.RecordProtocol_ReceiveRecordAsyncResult::_record
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ____record_6;
	// System.Boolean Mono.Security.Protocol.Tls.RecordProtocol_ReceiveRecordAsyncResult::completed
	bool ___completed_7;
	// System.Byte[] Mono.Security.Protocol.Tls.RecordProtocol_ReceiveRecordAsyncResult::_initialBuffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____initialBuffer_8;

public:
	inline static int32_t get_offset_of_locker_0() { return static_cast<int32_t>(offsetof(ReceiveRecordAsyncResult_tF2E4546C7F2475C74BE9023741AFFA069FC68752, ___locker_0)); }
	inline RuntimeObject * get_locker_0() const { return ___locker_0; }
	inline RuntimeObject ** get_address_of_locker_0() { return &___locker_0; }
	inline void set_locker_0(RuntimeObject * value)
	{
		___locker_0 = value;
		Il2CppCodeGenWriteBarrier((&___locker_0), value);
	}

	inline static int32_t get_offset_of__userCallback_1() { return static_cast<int32_t>(offsetof(ReceiveRecordAsyncResult_tF2E4546C7F2475C74BE9023741AFFA069FC68752, ____userCallback_1)); }
	inline AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * get__userCallback_1() const { return ____userCallback_1; }
	inline AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 ** get_address_of__userCallback_1() { return &____userCallback_1; }
	inline void set__userCallback_1(AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * value)
	{
		____userCallback_1 = value;
		Il2CppCodeGenWriteBarrier((&____userCallback_1), value);
	}

	inline static int32_t get_offset_of__userState_2() { return static_cast<int32_t>(offsetof(ReceiveRecordAsyncResult_tF2E4546C7F2475C74BE9023741AFFA069FC68752, ____userState_2)); }
	inline RuntimeObject * get__userState_2() const { return ____userState_2; }
	inline RuntimeObject ** get_address_of__userState_2() { return &____userState_2; }
	inline void set__userState_2(RuntimeObject * value)
	{
		____userState_2 = value;
		Il2CppCodeGenWriteBarrier((&____userState_2), value);
	}

	inline static int32_t get_offset_of__asyncException_3() { return static_cast<int32_t>(offsetof(ReceiveRecordAsyncResult_tF2E4546C7F2475C74BE9023741AFFA069FC68752, ____asyncException_3)); }
	inline Exception_t * get__asyncException_3() const { return ____asyncException_3; }
	inline Exception_t ** get_address_of__asyncException_3() { return &____asyncException_3; }
	inline void set__asyncException_3(Exception_t * value)
	{
		____asyncException_3 = value;
		Il2CppCodeGenWriteBarrier((&____asyncException_3), value);
	}

	inline static int32_t get_offset_of_handle_4() { return static_cast<int32_t>(offsetof(ReceiveRecordAsyncResult_tF2E4546C7F2475C74BE9023741AFFA069FC68752, ___handle_4)); }
	inline ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * get_handle_4() const { return ___handle_4; }
	inline ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 ** get_address_of_handle_4() { return &___handle_4; }
	inline void set_handle_4(ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * value)
	{
		___handle_4 = value;
		Il2CppCodeGenWriteBarrier((&___handle_4), value);
	}

	inline static int32_t get_offset_of__resultingBuffer_5() { return static_cast<int32_t>(offsetof(ReceiveRecordAsyncResult_tF2E4546C7F2475C74BE9023741AFFA069FC68752, ____resultingBuffer_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__resultingBuffer_5() const { return ____resultingBuffer_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__resultingBuffer_5() { return &____resultingBuffer_5; }
	inline void set__resultingBuffer_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____resultingBuffer_5 = value;
		Il2CppCodeGenWriteBarrier((&____resultingBuffer_5), value);
	}

	inline static int32_t get_offset_of__record_6() { return static_cast<int32_t>(offsetof(ReceiveRecordAsyncResult_tF2E4546C7F2475C74BE9023741AFFA069FC68752, ____record_6)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get__record_6() const { return ____record_6; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of__record_6() { return &____record_6; }
	inline void set__record_6(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		____record_6 = value;
		Il2CppCodeGenWriteBarrier((&____record_6), value);
	}

	inline static int32_t get_offset_of_completed_7() { return static_cast<int32_t>(offsetof(ReceiveRecordAsyncResult_tF2E4546C7F2475C74BE9023741AFFA069FC68752, ___completed_7)); }
	inline bool get_completed_7() const { return ___completed_7; }
	inline bool* get_address_of_completed_7() { return &___completed_7; }
	inline void set_completed_7(bool value)
	{
		___completed_7 = value;
	}

	inline static int32_t get_offset_of__initialBuffer_8() { return static_cast<int32_t>(offsetof(ReceiveRecordAsyncResult_tF2E4546C7F2475C74BE9023741AFFA069FC68752, ____initialBuffer_8)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__initialBuffer_8() const { return ____initialBuffer_8; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__initialBuffer_8() { return &____initialBuffer_8; }
	inline void set__initialBuffer_8(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____initialBuffer_8 = value;
		Il2CppCodeGenWriteBarrier((&____initialBuffer_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECEIVERECORDASYNCRESULT_TF2E4546C7F2475C74BE9023741AFFA069FC68752_H
#ifndef SENDRECORDASYNCRESULT_T43EF2777DAD8ED4C954CAF42151559D362357DC9_H
#define SENDRECORDASYNCRESULT_T43EF2777DAD8ED4C954CAF42151559D362357DC9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.RecordProtocol_SendRecordAsyncResult
struct  SendRecordAsyncResult_t43EF2777DAD8ED4C954CAF42151559D362357DC9  : public RuntimeObject
{
public:
	// System.Object Mono.Security.Protocol.Tls.RecordProtocol_SendRecordAsyncResult::locker
	RuntimeObject * ___locker_0;
	// System.AsyncCallback Mono.Security.Protocol.Tls.RecordProtocol_SendRecordAsyncResult::_userCallback
	AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ____userCallback_1;
	// System.Object Mono.Security.Protocol.Tls.RecordProtocol_SendRecordAsyncResult::_userState
	RuntimeObject * ____userState_2;
	// System.Exception Mono.Security.Protocol.Tls.RecordProtocol_SendRecordAsyncResult::_asyncException
	Exception_t * ____asyncException_3;
	// System.Threading.ManualResetEvent Mono.Security.Protocol.Tls.RecordProtocol_SendRecordAsyncResult::handle
	ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * ___handle_4;
	// Mono.Security.Protocol.Tls.Handshake.HandshakeMessage Mono.Security.Protocol.Tls.RecordProtocol_SendRecordAsyncResult::_message
	HandshakeMessage_t9B97FBAE0BAFCEA052F2B5EA880817CA1345298C * ____message_5;
	// System.Boolean Mono.Security.Protocol.Tls.RecordProtocol_SendRecordAsyncResult::completed
	bool ___completed_6;

public:
	inline static int32_t get_offset_of_locker_0() { return static_cast<int32_t>(offsetof(SendRecordAsyncResult_t43EF2777DAD8ED4C954CAF42151559D362357DC9, ___locker_0)); }
	inline RuntimeObject * get_locker_0() const { return ___locker_0; }
	inline RuntimeObject ** get_address_of_locker_0() { return &___locker_0; }
	inline void set_locker_0(RuntimeObject * value)
	{
		___locker_0 = value;
		Il2CppCodeGenWriteBarrier((&___locker_0), value);
	}

	inline static int32_t get_offset_of__userCallback_1() { return static_cast<int32_t>(offsetof(SendRecordAsyncResult_t43EF2777DAD8ED4C954CAF42151559D362357DC9, ____userCallback_1)); }
	inline AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * get__userCallback_1() const { return ____userCallback_1; }
	inline AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 ** get_address_of__userCallback_1() { return &____userCallback_1; }
	inline void set__userCallback_1(AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * value)
	{
		____userCallback_1 = value;
		Il2CppCodeGenWriteBarrier((&____userCallback_1), value);
	}

	inline static int32_t get_offset_of__userState_2() { return static_cast<int32_t>(offsetof(SendRecordAsyncResult_t43EF2777DAD8ED4C954CAF42151559D362357DC9, ____userState_2)); }
	inline RuntimeObject * get__userState_2() const { return ____userState_2; }
	inline RuntimeObject ** get_address_of__userState_2() { return &____userState_2; }
	inline void set__userState_2(RuntimeObject * value)
	{
		____userState_2 = value;
		Il2CppCodeGenWriteBarrier((&____userState_2), value);
	}

	inline static int32_t get_offset_of__asyncException_3() { return static_cast<int32_t>(offsetof(SendRecordAsyncResult_t43EF2777DAD8ED4C954CAF42151559D362357DC9, ____asyncException_3)); }
	inline Exception_t * get__asyncException_3() const { return ____asyncException_3; }
	inline Exception_t ** get_address_of__asyncException_3() { return &____asyncException_3; }
	inline void set__asyncException_3(Exception_t * value)
	{
		____asyncException_3 = value;
		Il2CppCodeGenWriteBarrier((&____asyncException_3), value);
	}

	inline static int32_t get_offset_of_handle_4() { return static_cast<int32_t>(offsetof(SendRecordAsyncResult_t43EF2777DAD8ED4C954CAF42151559D362357DC9, ___handle_4)); }
	inline ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * get_handle_4() const { return ___handle_4; }
	inline ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 ** get_address_of_handle_4() { return &___handle_4; }
	inline void set_handle_4(ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * value)
	{
		___handle_4 = value;
		Il2CppCodeGenWriteBarrier((&___handle_4), value);
	}

	inline static int32_t get_offset_of__message_5() { return static_cast<int32_t>(offsetof(SendRecordAsyncResult_t43EF2777DAD8ED4C954CAF42151559D362357DC9, ____message_5)); }
	inline HandshakeMessage_t9B97FBAE0BAFCEA052F2B5EA880817CA1345298C * get__message_5() const { return ____message_5; }
	inline HandshakeMessage_t9B97FBAE0BAFCEA052F2B5EA880817CA1345298C ** get_address_of__message_5() { return &____message_5; }
	inline void set__message_5(HandshakeMessage_t9B97FBAE0BAFCEA052F2B5EA880817CA1345298C * value)
	{
		____message_5 = value;
		Il2CppCodeGenWriteBarrier((&____message_5), value);
	}

	inline static int32_t get_offset_of_completed_6() { return static_cast<int32_t>(offsetof(SendRecordAsyncResult_t43EF2777DAD8ED4C954CAF42151559D362357DC9, ___completed_6)); }
	inline bool get_completed_6() const { return ___completed_6; }
	inline bool* get_address_of_completed_6() { return &___completed_6; }
	inline void set_completed_6(bool value)
	{
		___completed_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SENDRECORDASYNCRESULT_T43EF2777DAD8ED4C954CAF42151559D362357DC9_H
#ifndef SECURITYPARAMETERS_TC07B6E750AFC2037A7EBC754BD4DC1D37F41585E_H
#define SECURITYPARAMETERS_TC07B6E750AFC2037A7EBC754BD4DC1D37F41585E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.SecurityParameters
struct  SecurityParameters_tC07B6E750AFC2037A7EBC754BD4DC1D37F41585E  : public RuntimeObject
{
public:
	// Mono.Security.Protocol.Tls.CipherSuite Mono.Security.Protocol.Tls.SecurityParameters::cipher
	CipherSuite_t203D2B4A797805B775BEFB3B623962E7B407B6F7 * ___cipher_0;
	// System.Byte[] Mono.Security.Protocol.Tls.SecurityParameters::clientWriteMAC
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___clientWriteMAC_1;
	// System.Byte[] Mono.Security.Protocol.Tls.SecurityParameters::serverWriteMAC
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___serverWriteMAC_2;

public:
	inline static int32_t get_offset_of_cipher_0() { return static_cast<int32_t>(offsetof(SecurityParameters_tC07B6E750AFC2037A7EBC754BD4DC1D37F41585E, ___cipher_0)); }
	inline CipherSuite_t203D2B4A797805B775BEFB3B623962E7B407B6F7 * get_cipher_0() const { return ___cipher_0; }
	inline CipherSuite_t203D2B4A797805B775BEFB3B623962E7B407B6F7 ** get_address_of_cipher_0() { return &___cipher_0; }
	inline void set_cipher_0(CipherSuite_t203D2B4A797805B775BEFB3B623962E7B407B6F7 * value)
	{
		___cipher_0 = value;
		Il2CppCodeGenWriteBarrier((&___cipher_0), value);
	}

	inline static int32_t get_offset_of_clientWriteMAC_1() { return static_cast<int32_t>(offsetof(SecurityParameters_tC07B6E750AFC2037A7EBC754BD4DC1D37F41585E, ___clientWriteMAC_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_clientWriteMAC_1() const { return ___clientWriteMAC_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_clientWriteMAC_1() { return &___clientWriteMAC_1; }
	inline void set_clientWriteMAC_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___clientWriteMAC_1 = value;
		Il2CppCodeGenWriteBarrier((&___clientWriteMAC_1), value);
	}

	inline static int32_t get_offset_of_serverWriteMAC_2() { return static_cast<int32_t>(offsetof(SecurityParameters_tC07B6E750AFC2037A7EBC754BD4DC1D37F41585E, ___serverWriteMAC_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_serverWriteMAC_2() const { return ___serverWriteMAC_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_serverWriteMAC_2() { return &___serverWriteMAC_2; }
	inline void set_serverWriteMAC_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___serverWriteMAC_2 = value;
		Il2CppCodeGenWriteBarrier((&___serverWriteMAC_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECURITYPARAMETERS_TC07B6E750AFC2037A7EBC754BD4DC1D37F41585E_H
#ifndef INTERNALASYNCRESULT_TD29ADBE4ACE8353C13107D2E648FFA1F04DCC4C0_H
#define INTERNALASYNCRESULT_TD29ADBE4ACE8353C13107D2E648FFA1F04DCC4C0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.SslStreamBase_InternalAsyncResult
struct  InternalAsyncResult_tD29ADBE4ACE8353C13107D2E648FFA1F04DCC4C0  : public RuntimeObject
{
public:
	// System.Object Mono.Security.Protocol.Tls.SslStreamBase_InternalAsyncResult::locker
	RuntimeObject * ___locker_0;
	// System.AsyncCallback Mono.Security.Protocol.Tls.SslStreamBase_InternalAsyncResult::_userCallback
	AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ____userCallback_1;
	// System.Object Mono.Security.Protocol.Tls.SslStreamBase_InternalAsyncResult::_userState
	RuntimeObject * ____userState_2;
	// System.Exception Mono.Security.Protocol.Tls.SslStreamBase_InternalAsyncResult::_asyncException
	Exception_t * ____asyncException_3;
	// System.Threading.ManualResetEvent Mono.Security.Protocol.Tls.SslStreamBase_InternalAsyncResult::handle
	ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * ___handle_4;
	// System.Boolean Mono.Security.Protocol.Tls.SslStreamBase_InternalAsyncResult::completed
	bool ___completed_5;
	// System.Int32 Mono.Security.Protocol.Tls.SslStreamBase_InternalAsyncResult::_bytesRead
	int32_t ____bytesRead_6;
	// System.Boolean Mono.Security.Protocol.Tls.SslStreamBase_InternalAsyncResult::_fromWrite
	bool ____fromWrite_7;
	// System.Boolean Mono.Security.Protocol.Tls.SslStreamBase_InternalAsyncResult::_proceedAfterHandshake
	bool ____proceedAfterHandshake_8;
	// System.Byte[] Mono.Security.Protocol.Tls.SslStreamBase_InternalAsyncResult::_buffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____buffer_9;
	// System.Int32 Mono.Security.Protocol.Tls.SslStreamBase_InternalAsyncResult::_offset
	int32_t ____offset_10;
	// System.Int32 Mono.Security.Protocol.Tls.SslStreamBase_InternalAsyncResult::_count
	int32_t ____count_11;

public:
	inline static int32_t get_offset_of_locker_0() { return static_cast<int32_t>(offsetof(InternalAsyncResult_tD29ADBE4ACE8353C13107D2E648FFA1F04DCC4C0, ___locker_0)); }
	inline RuntimeObject * get_locker_0() const { return ___locker_0; }
	inline RuntimeObject ** get_address_of_locker_0() { return &___locker_0; }
	inline void set_locker_0(RuntimeObject * value)
	{
		___locker_0 = value;
		Il2CppCodeGenWriteBarrier((&___locker_0), value);
	}

	inline static int32_t get_offset_of__userCallback_1() { return static_cast<int32_t>(offsetof(InternalAsyncResult_tD29ADBE4ACE8353C13107D2E648FFA1F04DCC4C0, ____userCallback_1)); }
	inline AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * get__userCallback_1() const { return ____userCallback_1; }
	inline AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 ** get_address_of__userCallback_1() { return &____userCallback_1; }
	inline void set__userCallback_1(AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * value)
	{
		____userCallback_1 = value;
		Il2CppCodeGenWriteBarrier((&____userCallback_1), value);
	}

	inline static int32_t get_offset_of__userState_2() { return static_cast<int32_t>(offsetof(InternalAsyncResult_tD29ADBE4ACE8353C13107D2E648FFA1F04DCC4C0, ____userState_2)); }
	inline RuntimeObject * get__userState_2() const { return ____userState_2; }
	inline RuntimeObject ** get_address_of__userState_2() { return &____userState_2; }
	inline void set__userState_2(RuntimeObject * value)
	{
		____userState_2 = value;
		Il2CppCodeGenWriteBarrier((&____userState_2), value);
	}

	inline static int32_t get_offset_of__asyncException_3() { return static_cast<int32_t>(offsetof(InternalAsyncResult_tD29ADBE4ACE8353C13107D2E648FFA1F04DCC4C0, ____asyncException_3)); }
	inline Exception_t * get__asyncException_3() const { return ____asyncException_3; }
	inline Exception_t ** get_address_of__asyncException_3() { return &____asyncException_3; }
	inline void set__asyncException_3(Exception_t * value)
	{
		____asyncException_3 = value;
		Il2CppCodeGenWriteBarrier((&____asyncException_3), value);
	}

	inline static int32_t get_offset_of_handle_4() { return static_cast<int32_t>(offsetof(InternalAsyncResult_tD29ADBE4ACE8353C13107D2E648FFA1F04DCC4C0, ___handle_4)); }
	inline ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * get_handle_4() const { return ___handle_4; }
	inline ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 ** get_address_of_handle_4() { return &___handle_4; }
	inline void set_handle_4(ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * value)
	{
		___handle_4 = value;
		Il2CppCodeGenWriteBarrier((&___handle_4), value);
	}

	inline static int32_t get_offset_of_completed_5() { return static_cast<int32_t>(offsetof(InternalAsyncResult_tD29ADBE4ACE8353C13107D2E648FFA1F04DCC4C0, ___completed_5)); }
	inline bool get_completed_5() const { return ___completed_5; }
	inline bool* get_address_of_completed_5() { return &___completed_5; }
	inline void set_completed_5(bool value)
	{
		___completed_5 = value;
	}

	inline static int32_t get_offset_of__bytesRead_6() { return static_cast<int32_t>(offsetof(InternalAsyncResult_tD29ADBE4ACE8353C13107D2E648FFA1F04DCC4C0, ____bytesRead_6)); }
	inline int32_t get__bytesRead_6() const { return ____bytesRead_6; }
	inline int32_t* get_address_of__bytesRead_6() { return &____bytesRead_6; }
	inline void set__bytesRead_6(int32_t value)
	{
		____bytesRead_6 = value;
	}

	inline static int32_t get_offset_of__fromWrite_7() { return static_cast<int32_t>(offsetof(InternalAsyncResult_tD29ADBE4ACE8353C13107D2E648FFA1F04DCC4C0, ____fromWrite_7)); }
	inline bool get__fromWrite_7() const { return ____fromWrite_7; }
	inline bool* get_address_of__fromWrite_7() { return &____fromWrite_7; }
	inline void set__fromWrite_7(bool value)
	{
		____fromWrite_7 = value;
	}

	inline static int32_t get_offset_of__proceedAfterHandshake_8() { return static_cast<int32_t>(offsetof(InternalAsyncResult_tD29ADBE4ACE8353C13107D2E648FFA1F04DCC4C0, ____proceedAfterHandshake_8)); }
	inline bool get__proceedAfterHandshake_8() const { return ____proceedAfterHandshake_8; }
	inline bool* get_address_of__proceedAfterHandshake_8() { return &____proceedAfterHandshake_8; }
	inline void set__proceedAfterHandshake_8(bool value)
	{
		____proceedAfterHandshake_8 = value;
	}

	inline static int32_t get_offset_of__buffer_9() { return static_cast<int32_t>(offsetof(InternalAsyncResult_tD29ADBE4ACE8353C13107D2E648FFA1F04DCC4C0, ____buffer_9)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__buffer_9() const { return ____buffer_9; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__buffer_9() { return &____buffer_9; }
	inline void set__buffer_9(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____buffer_9 = value;
		Il2CppCodeGenWriteBarrier((&____buffer_9), value);
	}

	inline static int32_t get_offset_of__offset_10() { return static_cast<int32_t>(offsetof(InternalAsyncResult_tD29ADBE4ACE8353C13107D2E648FFA1F04DCC4C0, ____offset_10)); }
	inline int32_t get__offset_10() const { return ____offset_10; }
	inline int32_t* get_address_of__offset_10() { return &____offset_10; }
	inline void set__offset_10(int32_t value)
	{
		____offset_10 = value;
	}

	inline static int32_t get_offset_of__count_11() { return static_cast<int32_t>(offsetof(InternalAsyncResult_tD29ADBE4ACE8353C13107D2E648FFA1F04DCC4C0, ____count_11)); }
	inline int32_t get__count_11() const { return ____count_11; }
	inline int32_t* get_address_of__count_11() { return &____count_11; }
	inline void set__count_11(int32_t value)
	{
		____count_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNALASYNCRESULT_TD29ADBE4ACE8353C13107D2E648FFA1F04DCC4C0_H
#ifndef TLSCLIENTSETTINGS_T7521F1D5285E4B38591EC5E724E198CED35B39E0_H
#define TLSCLIENTSETTINGS_T7521F1D5285E4B38591EC5E724E198CED35B39E0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.TlsClientSettings
struct  TlsClientSettings_t7521F1D5285E4B38591EC5E724E198CED35B39E0  : public RuntimeObject
{
public:
	// System.String Mono.Security.Protocol.Tls.TlsClientSettings::targetHost
	String_t* ___targetHost_0;
	// System.Security.Cryptography.X509Certificates.X509CertificateCollection Mono.Security.Protocol.Tls.TlsClientSettings::certificates
	X509CertificateCollection_t824A6C58D0D1B4A7CAE30F26CE8EE4B23A8A1833 * ___certificates_1;
	// System.Security.Cryptography.X509Certificates.X509Certificate Mono.Security.Protocol.Tls.TlsClientSettings::clientCertificate
	X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 * ___clientCertificate_2;
	// Mono.Security.Cryptography.RSAManaged Mono.Security.Protocol.Tls.TlsClientSettings::certificateRSA
	RSAManaged_t386184E253177BF2DD9589A7E9C171DA8E718745 * ___certificateRSA_3;

public:
	inline static int32_t get_offset_of_targetHost_0() { return static_cast<int32_t>(offsetof(TlsClientSettings_t7521F1D5285E4B38591EC5E724E198CED35B39E0, ___targetHost_0)); }
	inline String_t* get_targetHost_0() const { return ___targetHost_0; }
	inline String_t** get_address_of_targetHost_0() { return &___targetHost_0; }
	inline void set_targetHost_0(String_t* value)
	{
		___targetHost_0 = value;
		Il2CppCodeGenWriteBarrier((&___targetHost_0), value);
	}

	inline static int32_t get_offset_of_certificates_1() { return static_cast<int32_t>(offsetof(TlsClientSettings_t7521F1D5285E4B38591EC5E724E198CED35B39E0, ___certificates_1)); }
	inline X509CertificateCollection_t824A6C58D0D1B4A7CAE30F26CE8EE4B23A8A1833 * get_certificates_1() const { return ___certificates_1; }
	inline X509CertificateCollection_t824A6C58D0D1B4A7CAE30F26CE8EE4B23A8A1833 ** get_address_of_certificates_1() { return &___certificates_1; }
	inline void set_certificates_1(X509CertificateCollection_t824A6C58D0D1B4A7CAE30F26CE8EE4B23A8A1833 * value)
	{
		___certificates_1 = value;
		Il2CppCodeGenWriteBarrier((&___certificates_1), value);
	}

	inline static int32_t get_offset_of_clientCertificate_2() { return static_cast<int32_t>(offsetof(TlsClientSettings_t7521F1D5285E4B38591EC5E724E198CED35B39E0, ___clientCertificate_2)); }
	inline X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 * get_clientCertificate_2() const { return ___clientCertificate_2; }
	inline X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 ** get_address_of_clientCertificate_2() { return &___clientCertificate_2; }
	inline void set_clientCertificate_2(X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 * value)
	{
		___clientCertificate_2 = value;
		Il2CppCodeGenWriteBarrier((&___clientCertificate_2), value);
	}

	inline static int32_t get_offset_of_certificateRSA_3() { return static_cast<int32_t>(offsetof(TlsClientSettings_t7521F1D5285E4B38591EC5E724E198CED35B39E0, ___certificateRSA_3)); }
	inline RSAManaged_t386184E253177BF2DD9589A7E9C171DA8E718745 * get_certificateRSA_3() const { return ___certificateRSA_3; }
	inline RSAManaged_t386184E253177BF2DD9589A7E9C171DA8E718745 ** get_address_of_certificateRSA_3() { return &___certificateRSA_3; }
	inline void set_certificateRSA_3(RSAManaged_t386184E253177BF2DD9589A7E9C171DA8E718745 * value)
	{
		___certificateRSA_3 = value;
		Il2CppCodeGenWriteBarrier((&___certificateRSA_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSCLIENTSETTINGS_T7521F1D5285E4B38591EC5E724E198CED35B39E0_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#define MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MarshalByRefObject
struct  MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF  : public RuntimeObject
{
public:
	// System.Object System.MarshalByRefObject::_identity
	RuntimeObject * ____identity_0;

public:
	inline static int32_t get_offset_of__identity_0() { return static_cast<int32_t>(offsetof(MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF, ____identity_0)); }
	inline RuntimeObject * get__identity_0() const { return ____identity_0; }
	inline RuntimeObject ** get_address_of__identity_0() { return &____identity_0; }
	inline void set__identity_0(RuntimeObject * value)
	{
		____identity_0 = value;
		Il2CppCodeGenWriteBarrier((&____identity_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF_marshaled_pinvoke
{
	Il2CppIUnknown* ____identity_0;
};
// Native definition for COM marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF_marshaled_com
{
	Il2CppIUnknown* ____identity_0;
};
#endif // MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#ifndef ASYMMETRICSIGNATUREDEFORMATTER_T2A943481A45060B8762C73CD0A4D624077C6AB39_H
#define ASYMMETRICSIGNATUREDEFORMATTER_T2A943481A45060B8762C73CD0A4D624077C6AB39_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.AsymmetricSignatureDeformatter
struct  AsymmetricSignatureDeformatter_t2A943481A45060B8762C73CD0A4D624077C6AB39  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYMMETRICSIGNATUREDEFORMATTER_T2A943481A45060B8762C73CD0A4D624077C6AB39_H
#ifndef ASYMMETRICSIGNATUREFORMATTER_T9480AB35AE4F9D132E9C22104181655C6533AB2E_H
#define ASYMMETRICSIGNATUREFORMATTER_T9480AB35AE4F9D132E9C22104181655C6533AB2E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.AsymmetricSignatureFormatter
struct  AsymmetricSignatureFormatter_t9480AB35AE4F9D132E9C22104181655C6533AB2E  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYMMETRICSIGNATUREFORMATTER_T9480AB35AE4F9D132E9C22104181655C6533AB2E_H
#ifndef HASHALGORITHM_T65659695B16C0BBF05707BF45191A97DC156D6BA_H
#define HASHALGORITHM_T65659695B16C0BBF05707BF45191A97DC156D6BA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.HashAlgorithm
struct  HashAlgorithm_t65659695B16C0BBF05707BF45191A97DC156D6BA  : public RuntimeObject
{
public:
	// System.Int32 System.Security.Cryptography.HashAlgorithm::HashSizeValue
	int32_t ___HashSizeValue_0;
	// System.Byte[] System.Security.Cryptography.HashAlgorithm::HashValue
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___HashValue_1;
	// System.Int32 System.Security.Cryptography.HashAlgorithm::State
	int32_t ___State_2;
	// System.Boolean System.Security.Cryptography.HashAlgorithm::m_bDisposed
	bool ___m_bDisposed_3;

public:
	inline static int32_t get_offset_of_HashSizeValue_0() { return static_cast<int32_t>(offsetof(HashAlgorithm_t65659695B16C0BBF05707BF45191A97DC156D6BA, ___HashSizeValue_0)); }
	inline int32_t get_HashSizeValue_0() const { return ___HashSizeValue_0; }
	inline int32_t* get_address_of_HashSizeValue_0() { return &___HashSizeValue_0; }
	inline void set_HashSizeValue_0(int32_t value)
	{
		___HashSizeValue_0 = value;
	}

	inline static int32_t get_offset_of_HashValue_1() { return static_cast<int32_t>(offsetof(HashAlgorithm_t65659695B16C0BBF05707BF45191A97DC156D6BA, ___HashValue_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_HashValue_1() const { return ___HashValue_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_HashValue_1() { return &___HashValue_1; }
	inline void set_HashValue_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___HashValue_1 = value;
		Il2CppCodeGenWriteBarrier((&___HashValue_1), value);
	}

	inline static int32_t get_offset_of_State_2() { return static_cast<int32_t>(offsetof(HashAlgorithm_t65659695B16C0BBF05707BF45191A97DC156D6BA, ___State_2)); }
	inline int32_t get_State_2() const { return ___State_2; }
	inline int32_t* get_address_of_State_2() { return &___State_2; }
	inline void set_State_2(int32_t value)
	{
		___State_2 = value;
	}

	inline static int32_t get_offset_of_m_bDisposed_3() { return static_cast<int32_t>(offsetof(HashAlgorithm_t65659695B16C0BBF05707BF45191A97DC156D6BA, ___m_bDisposed_3)); }
	inline bool get_m_bDisposed_3() const { return ___m_bDisposed_3; }
	inline bool* get_address_of_m_bDisposed_3() { return &___m_bDisposed_3; }
	inline void set_m_bDisposed_3(bool value)
	{
		___m_bDisposed_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HASHALGORITHM_T65659695B16C0BBF05707BF45191A97DC156D6BA_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef MD2_TCAAEC1A28A3D0B9E8810B27E4840BEA399619442_H
#define MD2_TCAAEC1A28A3D0B9E8810B27E4840BEA399619442_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Cryptography.MD2
struct  MD2_tCAAEC1A28A3D0B9E8810B27E4840BEA399619442  : public HashAlgorithm_t65659695B16C0BBF05707BF45191A97DC156D6BA
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MD2_TCAAEC1A28A3D0B9E8810B27E4840BEA399619442_H
#ifndef MD4_T932C1DEA44D4B8650873251E88AA4096164BB380_H
#define MD4_T932C1DEA44D4B8650873251E88AA4096164BB380_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Cryptography.MD4
struct  MD4_t932C1DEA44D4B8650873251E88AA4096164BB380  : public HashAlgorithm_t65659695B16C0BBF05707BF45191A97DC156D6BA
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MD4_T932C1DEA44D4B8650873251E88AA4096164BB380_H
#ifndef MD5SHA1_TEB00523D30161BCED494CC7264D4A3F7CB28D86A_H
#define MD5SHA1_TEB00523D30161BCED494CC7264D4A3F7CB28D86A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Cryptography.MD5SHA1
struct  MD5SHA1_tEB00523D30161BCED494CC7264D4A3F7CB28D86A  : public HashAlgorithm_t65659695B16C0BBF05707BF45191A97DC156D6BA
{
public:
	// System.Security.Cryptography.HashAlgorithm Mono.Security.Cryptography.MD5SHA1::md5
	HashAlgorithm_t65659695B16C0BBF05707BF45191A97DC156D6BA * ___md5_4;
	// System.Security.Cryptography.HashAlgorithm Mono.Security.Cryptography.MD5SHA1::sha
	HashAlgorithm_t65659695B16C0BBF05707BF45191A97DC156D6BA * ___sha_5;
	// System.Boolean Mono.Security.Cryptography.MD5SHA1::hashing
	bool ___hashing_6;

public:
	inline static int32_t get_offset_of_md5_4() { return static_cast<int32_t>(offsetof(MD5SHA1_tEB00523D30161BCED494CC7264D4A3F7CB28D86A, ___md5_4)); }
	inline HashAlgorithm_t65659695B16C0BBF05707BF45191A97DC156D6BA * get_md5_4() const { return ___md5_4; }
	inline HashAlgorithm_t65659695B16C0BBF05707BF45191A97DC156D6BA ** get_address_of_md5_4() { return &___md5_4; }
	inline void set_md5_4(HashAlgorithm_t65659695B16C0BBF05707BF45191A97DC156D6BA * value)
	{
		___md5_4 = value;
		Il2CppCodeGenWriteBarrier((&___md5_4), value);
	}

	inline static int32_t get_offset_of_sha_5() { return static_cast<int32_t>(offsetof(MD5SHA1_tEB00523D30161BCED494CC7264D4A3F7CB28D86A, ___sha_5)); }
	inline HashAlgorithm_t65659695B16C0BBF05707BF45191A97DC156D6BA * get_sha_5() const { return ___sha_5; }
	inline HashAlgorithm_t65659695B16C0BBF05707BF45191A97DC156D6BA ** get_address_of_sha_5() { return &___sha_5; }
	inline void set_sha_5(HashAlgorithm_t65659695B16C0BBF05707BF45191A97DC156D6BA * value)
	{
		___sha_5 = value;
		Il2CppCodeGenWriteBarrier((&___sha_5), value);
	}

	inline static int32_t get_offset_of_hashing_6() { return static_cast<int32_t>(offsetof(MD5SHA1_tEB00523D30161BCED494CC7264D4A3F7CB28D86A, ___hashing_6)); }
	inline bool get_hashing_6() const { return ___hashing_6; }
	inline bool* get_address_of_hashing_6() { return &___hashing_6; }
	inline void set_hashing_6(bool value)
	{
		___hashing_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MD5SHA1_TEB00523D30161BCED494CC7264D4A3F7CB28D86A_H
#ifndef TLSEXCEPTION_T774465EA64E3ADAAE3DB21835DD9AB8C40247F91_H
#define TLSEXCEPTION_T774465EA64E3ADAAE3DB21835DD9AB8C40247F91_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Interface.TlsException
struct  TlsException_t774465EA64E3ADAAE3DB21835DD9AB8C40247F91  : public Exception_t
{
public:
	// Mono.Security.Interface.Alert Mono.Security.Interface.TlsException::alert
	Alert_tABF269545F2C583CCA47FF574E612DDAF232944E * ___alert_17;

public:
	inline static int32_t get_offset_of_alert_17() { return static_cast<int32_t>(offsetof(TlsException_t774465EA64E3ADAAE3DB21835DD9AB8C40247F91, ___alert_17)); }
	inline Alert_tABF269545F2C583CCA47FF574E612DDAF232944E * get_alert_17() const { return ___alert_17; }
	inline Alert_tABF269545F2C583CCA47FF574E612DDAF232944E ** get_address_of_alert_17() { return &___alert_17; }
	inline void set_alert_17(Alert_tABF269545F2C583CCA47FF574E612DDAF232944E * value)
	{
		___alert_17 = value;
		Il2CppCodeGenWriteBarrier((&___alert_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSEXCEPTION_T774465EA64E3ADAAE3DB21835DD9AB8C40247F91_H
#ifndef RSASSLSIGNATUREDEFORMATTER_TE9EE489E26042B81563771B31853E66BDC5859DA_H
#define RSASSLSIGNATUREDEFORMATTER_TE9EE489E26042B81563771B31853E66BDC5859DA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.RSASslSignatureDeformatter
struct  RSASslSignatureDeformatter_tE9EE489E26042B81563771B31853E66BDC5859DA  : public AsymmetricSignatureDeformatter_t2A943481A45060B8762C73CD0A4D624077C6AB39
{
public:
	// System.Security.Cryptography.RSA Mono.Security.Protocol.Tls.RSASslSignatureDeformatter::key
	RSA_tB6C4B434B2AC02E3F8981DB2908C2018E251D145 * ___key_0;
	// System.Security.Cryptography.HashAlgorithm Mono.Security.Protocol.Tls.RSASslSignatureDeformatter::hash
	HashAlgorithm_t65659695B16C0BBF05707BF45191A97DC156D6BA * ___hash_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(RSASslSignatureDeformatter_tE9EE489E26042B81563771B31853E66BDC5859DA, ___key_0)); }
	inline RSA_tB6C4B434B2AC02E3F8981DB2908C2018E251D145 * get_key_0() const { return ___key_0; }
	inline RSA_tB6C4B434B2AC02E3F8981DB2908C2018E251D145 ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(RSA_tB6C4B434B2AC02E3F8981DB2908C2018E251D145 * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_hash_1() { return static_cast<int32_t>(offsetof(RSASslSignatureDeformatter_tE9EE489E26042B81563771B31853E66BDC5859DA, ___hash_1)); }
	inline HashAlgorithm_t65659695B16C0BBF05707BF45191A97DC156D6BA * get_hash_1() const { return ___hash_1; }
	inline HashAlgorithm_t65659695B16C0BBF05707BF45191A97DC156D6BA ** get_address_of_hash_1() { return &___hash_1; }
	inline void set_hash_1(HashAlgorithm_t65659695B16C0BBF05707BF45191A97DC156D6BA * value)
	{
		___hash_1 = value;
		Il2CppCodeGenWriteBarrier((&___hash_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RSASSLSIGNATUREDEFORMATTER_TE9EE489E26042B81563771B31853E66BDC5859DA_H
#ifndef RSASSLSIGNATUREFORMATTER_T21ABEFCB144E30953E30A458612D1F4E021470E2_H
#define RSASSLSIGNATUREFORMATTER_T21ABEFCB144E30953E30A458612D1F4E021470E2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.RSASslSignatureFormatter
struct  RSASslSignatureFormatter_t21ABEFCB144E30953E30A458612D1F4E021470E2  : public AsymmetricSignatureFormatter_t9480AB35AE4F9D132E9C22104181655C6533AB2E
{
public:
	// System.Security.Cryptography.RSA Mono.Security.Protocol.Tls.RSASslSignatureFormatter::key
	RSA_tB6C4B434B2AC02E3F8981DB2908C2018E251D145 * ___key_0;
	// System.Security.Cryptography.HashAlgorithm Mono.Security.Protocol.Tls.RSASslSignatureFormatter::hash
	HashAlgorithm_t65659695B16C0BBF05707BF45191A97DC156D6BA * ___hash_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(RSASslSignatureFormatter_t21ABEFCB144E30953E30A458612D1F4E021470E2, ___key_0)); }
	inline RSA_tB6C4B434B2AC02E3F8981DB2908C2018E251D145 * get_key_0() const { return ___key_0; }
	inline RSA_tB6C4B434B2AC02E3F8981DB2908C2018E251D145 ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(RSA_tB6C4B434B2AC02E3F8981DB2908C2018E251D145 * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_hash_1() { return static_cast<int32_t>(offsetof(RSASslSignatureFormatter_t21ABEFCB144E30953E30A458612D1F4E021470E2, ___hash_1)); }
	inline HashAlgorithm_t65659695B16C0BBF05707BF45191A97DC156D6BA * get_hash_1() const { return ___hash_1; }
	inline HashAlgorithm_t65659695B16C0BBF05707BF45191A97DC156D6BA ** get_address_of_hash_1() { return &___hash_1; }
	inline void set_hash_1(HashAlgorithm_t65659695B16C0BBF05707BF45191A97DC156D6BA * value)
	{
		___hash_1 = value;
		Il2CppCodeGenWriteBarrier((&___hash_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RSASSLSIGNATUREFORMATTER_T21ABEFCB144E30953E30A458612D1F4E021470E2_H
#ifndef SERVERRECORDPROTOCOL_T4B4908008D4580AAAC5A433442A7334A921323CA_H
#define SERVERRECORDPROTOCOL_T4B4908008D4580AAAC5A433442A7334A921323CA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.ServerRecordProtocol
struct  ServerRecordProtocol_t4B4908008D4580AAAC5A433442A7334A921323CA  : public RecordProtocol_tFA651C8422AECEEE03AABEDB029EC83EB8833CFA
{
public:
	// Mono.Security.Protocol.Tls.Handshake.Server.TlsClientCertificate Mono.Security.Protocol.Tls.ServerRecordProtocol::cert
	TlsClientCertificate_tDB7DEB656354449A78DCA4EC0EAD85EC4CC9712B * ___cert_3;

public:
	inline static int32_t get_offset_of_cert_3() { return static_cast<int32_t>(offsetof(ServerRecordProtocol_t4B4908008D4580AAAC5A433442A7334A921323CA, ___cert_3)); }
	inline TlsClientCertificate_tDB7DEB656354449A78DCA4EC0EAD85EC4CC9712B * get_cert_3() const { return ___cert_3; }
	inline TlsClientCertificate_tDB7DEB656354449A78DCA4EC0EAD85EC4CC9712B ** get_address_of_cert_3() { return &___cert_3; }
	inline void set_cert_3(TlsClientCertificate_tDB7DEB656354449A78DCA4EC0EAD85EC4CC9712B * value)
	{
		___cert_3 = value;
		Il2CppCodeGenWriteBarrier((&___cert_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERVERRECORDPROTOCOL_T4B4908008D4580AAAC5A433442A7334A921323CA_H
#ifndef SSLHANDSHAKEHASH_T06FA22E13E67503A71F1F7F87E9E2DC8F839B39C_H
#define SSLHANDSHAKEHASH_T06FA22E13E67503A71F1F7F87E9E2DC8F839B39C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.SslHandshakeHash
struct  SslHandshakeHash_t06FA22E13E67503A71F1F7F87E9E2DC8F839B39C  : public HashAlgorithm_t65659695B16C0BBF05707BF45191A97DC156D6BA
{
public:
	// System.Security.Cryptography.HashAlgorithm Mono.Security.Protocol.Tls.SslHandshakeHash::md5
	HashAlgorithm_t65659695B16C0BBF05707BF45191A97DC156D6BA * ___md5_4;
	// System.Security.Cryptography.HashAlgorithm Mono.Security.Protocol.Tls.SslHandshakeHash::sha
	HashAlgorithm_t65659695B16C0BBF05707BF45191A97DC156D6BA * ___sha_5;
	// System.Boolean Mono.Security.Protocol.Tls.SslHandshakeHash::hashing
	bool ___hashing_6;
	// System.Byte[] Mono.Security.Protocol.Tls.SslHandshakeHash::secret
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___secret_7;
	// System.Byte[] Mono.Security.Protocol.Tls.SslHandshakeHash::innerPadMD5
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___innerPadMD5_8;
	// System.Byte[] Mono.Security.Protocol.Tls.SslHandshakeHash::outerPadMD5
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___outerPadMD5_9;
	// System.Byte[] Mono.Security.Protocol.Tls.SslHandshakeHash::innerPadSHA
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___innerPadSHA_10;
	// System.Byte[] Mono.Security.Protocol.Tls.SslHandshakeHash::outerPadSHA
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___outerPadSHA_11;

public:
	inline static int32_t get_offset_of_md5_4() { return static_cast<int32_t>(offsetof(SslHandshakeHash_t06FA22E13E67503A71F1F7F87E9E2DC8F839B39C, ___md5_4)); }
	inline HashAlgorithm_t65659695B16C0BBF05707BF45191A97DC156D6BA * get_md5_4() const { return ___md5_4; }
	inline HashAlgorithm_t65659695B16C0BBF05707BF45191A97DC156D6BA ** get_address_of_md5_4() { return &___md5_4; }
	inline void set_md5_4(HashAlgorithm_t65659695B16C0BBF05707BF45191A97DC156D6BA * value)
	{
		___md5_4 = value;
		Il2CppCodeGenWriteBarrier((&___md5_4), value);
	}

	inline static int32_t get_offset_of_sha_5() { return static_cast<int32_t>(offsetof(SslHandshakeHash_t06FA22E13E67503A71F1F7F87E9E2DC8F839B39C, ___sha_5)); }
	inline HashAlgorithm_t65659695B16C0BBF05707BF45191A97DC156D6BA * get_sha_5() const { return ___sha_5; }
	inline HashAlgorithm_t65659695B16C0BBF05707BF45191A97DC156D6BA ** get_address_of_sha_5() { return &___sha_5; }
	inline void set_sha_5(HashAlgorithm_t65659695B16C0BBF05707BF45191A97DC156D6BA * value)
	{
		___sha_5 = value;
		Il2CppCodeGenWriteBarrier((&___sha_5), value);
	}

	inline static int32_t get_offset_of_hashing_6() { return static_cast<int32_t>(offsetof(SslHandshakeHash_t06FA22E13E67503A71F1F7F87E9E2DC8F839B39C, ___hashing_6)); }
	inline bool get_hashing_6() const { return ___hashing_6; }
	inline bool* get_address_of_hashing_6() { return &___hashing_6; }
	inline void set_hashing_6(bool value)
	{
		___hashing_6 = value;
	}

	inline static int32_t get_offset_of_secret_7() { return static_cast<int32_t>(offsetof(SslHandshakeHash_t06FA22E13E67503A71F1F7F87E9E2DC8F839B39C, ___secret_7)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_secret_7() const { return ___secret_7; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_secret_7() { return &___secret_7; }
	inline void set_secret_7(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___secret_7 = value;
		Il2CppCodeGenWriteBarrier((&___secret_7), value);
	}

	inline static int32_t get_offset_of_innerPadMD5_8() { return static_cast<int32_t>(offsetof(SslHandshakeHash_t06FA22E13E67503A71F1F7F87E9E2DC8F839B39C, ___innerPadMD5_8)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_innerPadMD5_8() const { return ___innerPadMD5_8; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_innerPadMD5_8() { return &___innerPadMD5_8; }
	inline void set_innerPadMD5_8(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___innerPadMD5_8 = value;
		Il2CppCodeGenWriteBarrier((&___innerPadMD5_8), value);
	}

	inline static int32_t get_offset_of_outerPadMD5_9() { return static_cast<int32_t>(offsetof(SslHandshakeHash_t06FA22E13E67503A71F1F7F87E9E2DC8F839B39C, ___outerPadMD5_9)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_outerPadMD5_9() const { return ___outerPadMD5_9; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_outerPadMD5_9() { return &___outerPadMD5_9; }
	inline void set_outerPadMD5_9(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___outerPadMD5_9 = value;
		Il2CppCodeGenWriteBarrier((&___outerPadMD5_9), value);
	}

	inline static int32_t get_offset_of_innerPadSHA_10() { return static_cast<int32_t>(offsetof(SslHandshakeHash_t06FA22E13E67503A71F1F7F87E9E2DC8F839B39C, ___innerPadSHA_10)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_innerPadSHA_10() const { return ___innerPadSHA_10; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_innerPadSHA_10() { return &___innerPadSHA_10; }
	inline void set_innerPadSHA_10(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___innerPadSHA_10 = value;
		Il2CppCodeGenWriteBarrier((&___innerPadSHA_10), value);
	}

	inline static int32_t get_offset_of_outerPadSHA_11() { return static_cast<int32_t>(offsetof(SslHandshakeHash_t06FA22E13E67503A71F1F7F87E9E2DC8F839B39C, ___outerPadSHA_11)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_outerPadSHA_11() const { return ___outerPadSHA_11; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_outerPadSHA_11() { return &___outerPadSHA_11; }
	inline void set_outerPadSHA_11(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___outerPadSHA_11 = value;
		Il2CppCodeGenWriteBarrier((&___outerPadSHA_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SSLHANDSHAKEHASH_T06FA22E13E67503A71F1F7F87E9E2DC8F839B39C_H
#ifndef TLSEXCEPTION_T07E60982E4F11EE25F5AE7E91A4BA40EB1E72CD2_H
#define TLSEXCEPTION_T07E60982E4F11EE25F5AE7E91A4BA40EB1E72CD2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.TlsException
struct  TlsException_t07E60982E4F11EE25F5AE7E91A4BA40EB1E72CD2  : public Exception_t
{
public:
	// Mono.Security.Protocol.Tls.Alert Mono.Security.Protocol.Tls.TlsException::alert
	Alert_t2723053A22EA47B2855ABAEFC24AD57F16D79054 * ___alert_17;

public:
	inline static int32_t get_offset_of_alert_17() { return static_cast<int32_t>(offsetof(TlsException_t07E60982E4F11EE25F5AE7E91A4BA40EB1E72CD2, ___alert_17)); }
	inline Alert_t2723053A22EA47B2855ABAEFC24AD57F16D79054 * get_alert_17() const { return ___alert_17; }
	inline Alert_t2723053A22EA47B2855ABAEFC24AD57F16D79054 ** get_address_of_alert_17() { return &___alert_17; }
	inline void set_alert_17(Alert_t2723053A22EA47B2855ABAEFC24AD57F16D79054 * value)
	{
		___alert_17 = value;
		Il2CppCodeGenWriteBarrier((&___alert_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSEXCEPTION_T07E60982E4F11EE25F5AE7E91A4BA40EB1E72CD2_H
#ifndef BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#define BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifndef DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#define DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 
{
public:
	// System.UInt64 System.DateTime::dateData
	uint64_t ___dateData_44;

public:
	inline static int32_t get_offset_of_dateData_44() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132, ___dateData_44)); }
	inline uint64_t get_dateData_44() const { return ___dateData_44; }
	inline uint64_t* get_address_of_dateData_44() { return &___dateData_44; }
	inline void set_dateData_44(uint64_t value)
	{
		___dateData_44 = value;
	}
};

struct DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields
{
public:
	// System.Int32[] System.DateTime::DaysToMonth365
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth365_29;
	// System.Int32[] System.DateTime::DaysToMonth366
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth366_30;
	// System.DateTime System.DateTime::MinValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MinValue_31;
	// System.DateTime System.DateTime::MaxValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MaxValue_32;

public:
	inline static int32_t get_offset_of_DaysToMonth365_29() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth365_29)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth365_29() const { return ___DaysToMonth365_29; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth365_29() { return &___DaysToMonth365_29; }
	inline void set_DaysToMonth365_29(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth365_29 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth365_29), value);
	}

	inline static int32_t get_offset_of_DaysToMonth366_30() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth366_30)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth366_30() const { return ___DaysToMonth366_30; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth366_30() { return &___DaysToMonth366_30; }
	inline void set_DaysToMonth366_30(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth366_30 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth366_30), value);
	}

	inline static int32_t get_offset_of_MinValue_31() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MinValue_31)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MinValue_31() const { return ___MinValue_31; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MinValue_31() { return &___MinValue_31; }
	inline void set_MinValue_31(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MinValue_31 = value;
	}

	inline static int32_t get_offset_of_MaxValue_32() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MaxValue_32)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MaxValue_32() const { return ___MaxValue_32; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MaxValue_32() { return &___MaxValue_32; }
	inline void set_MaxValue_32(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MaxValue_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#define STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Stream
struct  Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7  : public MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF
{
public:
	// System.IO.Stream_ReadWriteTask System.IO.Stream::_activeReadWriteTask
	ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * ____activeReadWriteTask_3;
	// System.Threading.SemaphoreSlim System.IO.Stream::_asyncActiveSemaphore
	SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * ____asyncActiveSemaphore_4;

public:
	inline static int32_t get_offset_of__activeReadWriteTask_3() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7, ____activeReadWriteTask_3)); }
	inline ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * get__activeReadWriteTask_3() const { return ____activeReadWriteTask_3; }
	inline ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 ** get_address_of__activeReadWriteTask_3() { return &____activeReadWriteTask_3; }
	inline void set__activeReadWriteTask_3(ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * value)
	{
		____activeReadWriteTask_3 = value;
		Il2CppCodeGenWriteBarrier((&____activeReadWriteTask_3), value);
	}

	inline static int32_t get_offset_of__asyncActiveSemaphore_4() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7, ____asyncActiveSemaphore_4)); }
	inline SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * get__asyncActiveSemaphore_4() const { return ____asyncActiveSemaphore_4; }
	inline SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 ** get_address_of__asyncActiveSemaphore_4() { return &____asyncActiveSemaphore_4; }
	inline void set__asyncActiveSemaphore_4(SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * value)
	{
		____asyncActiveSemaphore_4 = value;
		Il2CppCodeGenWriteBarrier((&____asyncActiveSemaphore_4), value);
	}
};

struct Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7_StaticFields
{
public:
	// System.IO.Stream System.IO.Stream::Null
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___Null_1;

public:
	inline static int32_t get_offset_of_Null_1() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7_StaticFields, ___Null_1)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_Null_1() const { return ___Null_1; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_Null_1() { return &___Null_1; }
	inline void set_Null_1(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___Null_1 = value;
		Il2CppCodeGenWriteBarrier((&___Null_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef NULLABLE_1_T9E6A67BECE376F0623B5C857F5674A0311C41793_H
#define NULLABLE_1_T9E6A67BECE376F0623B5C857F5674A0311C41793_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Boolean>
struct  Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 
{
public:
	// T System.Nullable`1::value
	bool ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793, ___value_0)); }
	inline bool get_value_0() const { return ___value_0; }
	inline bool* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(bool value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T9E6A67BECE376F0623B5C857F5674A0311C41793_H
#ifndef RSAPARAMETERS_T6A568C1275FA8F8C02615666D998134DCFFB9717_H
#define RSAPARAMETERS_T6A568C1275FA8F8C02615666D998134DCFFB9717_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.RSAParameters
struct  RSAParameters_t6A568C1275FA8F8C02615666D998134DCFFB9717 
{
public:
	// System.Byte[] System.Security.Cryptography.RSAParameters::Exponent
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___Exponent_0;
	// System.Byte[] System.Security.Cryptography.RSAParameters::Modulus
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___Modulus_1;
	// System.Byte[] System.Security.Cryptography.RSAParameters::P
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___P_2;
	// System.Byte[] System.Security.Cryptography.RSAParameters::Q
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___Q_3;
	// System.Byte[] System.Security.Cryptography.RSAParameters::DP
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___DP_4;
	// System.Byte[] System.Security.Cryptography.RSAParameters::DQ
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___DQ_5;
	// System.Byte[] System.Security.Cryptography.RSAParameters::InverseQ
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___InverseQ_6;
	// System.Byte[] System.Security.Cryptography.RSAParameters::D
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___D_7;

public:
	inline static int32_t get_offset_of_Exponent_0() { return static_cast<int32_t>(offsetof(RSAParameters_t6A568C1275FA8F8C02615666D998134DCFFB9717, ___Exponent_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_Exponent_0() const { return ___Exponent_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_Exponent_0() { return &___Exponent_0; }
	inline void set_Exponent_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___Exponent_0 = value;
		Il2CppCodeGenWriteBarrier((&___Exponent_0), value);
	}

	inline static int32_t get_offset_of_Modulus_1() { return static_cast<int32_t>(offsetof(RSAParameters_t6A568C1275FA8F8C02615666D998134DCFFB9717, ___Modulus_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_Modulus_1() const { return ___Modulus_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_Modulus_1() { return &___Modulus_1; }
	inline void set_Modulus_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___Modulus_1 = value;
		Il2CppCodeGenWriteBarrier((&___Modulus_1), value);
	}

	inline static int32_t get_offset_of_P_2() { return static_cast<int32_t>(offsetof(RSAParameters_t6A568C1275FA8F8C02615666D998134DCFFB9717, ___P_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_P_2() const { return ___P_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_P_2() { return &___P_2; }
	inline void set_P_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___P_2 = value;
		Il2CppCodeGenWriteBarrier((&___P_2), value);
	}

	inline static int32_t get_offset_of_Q_3() { return static_cast<int32_t>(offsetof(RSAParameters_t6A568C1275FA8F8C02615666D998134DCFFB9717, ___Q_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_Q_3() const { return ___Q_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_Q_3() { return &___Q_3; }
	inline void set_Q_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___Q_3 = value;
		Il2CppCodeGenWriteBarrier((&___Q_3), value);
	}

	inline static int32_t get_offset_of_DP_4() { return static_cast<int32_t>(offsetof(RSAParameters_t6A568C1275FA8F8C02615666D998134DCFFB9717, ___DP_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_DP_4() const { return ___DP_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_DP_4() { return &___DP_4; }
	inline void set_DP_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___DP_4 = value;
		Il2CppCodeGenWriteBarrier((&___DP_4), value);
	}

	inline static int32_t get_offset_of_DQ_5() { return static_cast<int32_t>(offsetof(RSAParameters_t6A568C1275FA8F8C02615666D998134DCFFB9717, ___DQ_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_DQ_5() const { return ___DQ_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_DQ_5() { return &___DQ_5; }
	inline void set_DQ_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___DQ_5 = value;
		Il2CppCodeGenWriteBarrier((&___DQ_5), value);
	}

	inline static int32_t get_offset_of_InverseQ_6() { return static_cast<int32_t>(offsetof(RSAParameters_t6A568C1275FA8F8C02615666D998134DCFFB9717, ___InverseQ_6)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_InverseQ_6() const { return ___InverseQ_6; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_InverseQ_6() { return &___InverseQ_6; }
	inline void set_InverseQ_6(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___InverseQ_6 = value;
		Il2CppCodeGenWriteBarrier((&___InverseQ_6), value);
	}

	inline static int32_t get_offset_of_D_7() { return static_cast<int32_t>(offsetof(RSAParameters_t6A568C1275FA8F8C02615666D998134DCFFB9717, ___D_7)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_D_7() const { return ___D_7; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_D_7() { return &___D_7; }
	inline void set_D_7(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___D_7 = value;
		Il2CppCodeGenWriteBarrier((&___D_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RSAPARAMETERS_T6A568C1275FA8F8C02615666D998134DCFFB9717_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef MD2MANAGED_T6867DBAD1BDCC846BE478BABC2D5DBAFFC7495EC_H
#define MD2MANAGED_T6867DBAD1BDCC846BE478BABC2D5DBAFFC7495EC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Cryptography.MD2Managed
struct  MD2Managed_t6867DBAD1BDCC846BE478BABC2D5DBAFFC7495EC  : public MD2_tCAAEC1A28A3D0B9E8810B27E4840BEA399619442
{
public:
	// System.Byte[] Mono.Security.Cryptography.MD2Managed::state
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___state_4;
	// System.Byte[] Mono.Security.Cryptography.MD2Managed::checksum
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___checksum_5;
	// System.Byte[] Mono.Security.Cryptography.MD2Managed::buffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___buffer_6;
	// System.Int32 Mono.Security.Cryptography.MD2Managed::count
	int32_t ___count_7;
	// System.Byte[] Mono.Security.Cryptography.MD2Managed::x
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___x_8;

public:
	inline static int32_t get_offset_of_state_4() { return static_cast<int32_t>(offsetof(MD2Managed_t6867DBAD1BDCC846BE478BABC2D5DBAFFC7495EC, ___state_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_state_4() const { return ___state_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_state_4() { return &___state_4; }
	inline void set_state_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___state_4 = value;
		Il2CppCodeGenWriteBarrier((&___state_4), value);
	}

	inline static int32_t get_offset_of_checksum_5() { return static_cast<int32_t>(offsetof(MD2Managed_t6867DBAD1BDCC846BE478BABC2D5DBAFFC7495EC, ___checksum_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_checksum_5() const { return ___checksum_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_checksum_5() { return &___checksum_5; }
	inline void set_checksum_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___checksum_5 = value;
		Il2CppCodeGenWriteBarrier((&___checksum_5), value);
	}

	inline static int32_t get_offset_of_buffer_6() { return static_cast<int32_t>(offsetof(MD2Managed_t6867DBAD1BDCC846BE478BABC2D5DBAFFC7495EC, ___buffer_6)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_buffer_6() const { return ___buffer_6; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_buffer_6() { return &___buffer_6; }
	inline void set_buffer_6(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___buffer_6 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_6), value);
	}

	inline static int32_t get_offset_of_count_7() { return static_cast<int32_t>(offsetof(MD2Managed_t6867DBAD1BDCC846BE478BABC2D5DBAFFC7495EC, ___count_7)); }
	inline int32_t get_count_7() const { return ___count_7; }
	inline int32_t* get_address_of_count_7() { return &___count_7; }
	inline void set_count_7(int32_t value)
	{
		___count_7 = value;
	}

	inline static int32_t get_offset_of_x_8() { return static_cast<int32_t>(offsetof(MD2Managed_t6867DBAD1BDCC846BE478BABC2D5DBAFFC7495EC, ___x_8)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_x_8() const { return ___x_8; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_x_8() { return &___x_8; }
	inline void set_x_8(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___x_8 = value;
		Il2CppCodeGenWriteBarrier((&___x_8), value);
	}
};

struct MD2Managed_t6867DBAD1BDCC846BE478BABC2D5DBAFFC7495EC_StaticFields
{
public:
	// System.Byte[] Mono.Security.Cryptography.MD2Managed::PI_SUBST
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___PI_SUBST_9;

public:
	inline static int32_t get_offset_of_PI_SUBST_9() { return static_cast<int32_t>(offsetof(MD2Managed_t6867DBAD1BDCC846BE478BABC2D5DBAFFC7495EC_StaticFields, ___PI_SUBST_9)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_PI_SUBST_9() const { return ___PI_SUBST_9; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_PI_SUBST_9() { return &___PI_SUBST_9; }
	inline void set_PI_SUBST_9(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___PI_SUBST_9 = value;
		Il2CppCodeGenWriteBarrier((&___PI_SUBST_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MD2MANAGED_T6867DBAD1BDCC846BE478BABC2D5DBAFFC7495EC_H
#ifndef MD4MANAGED_T1FC9A6CDB0A89A71416414689B3A0FF0283759D1_H
#define MD4MANAGED_T1FC9A6CDB0A89A71416414689B3A0FF0283759D1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Cryptography.MD4Managed
struct  MD4Managed_t1FC9A6CDB0A89A71416414689B3A0FF0283759D1  : public MD4_t932C1DEA44D4B8650873251E88AA4096164BB380
{
public:
	// System.UInt32[] Mono.Security.Cryptography.MD4Managed::state
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___state_4;
	// System.Byte[] Mono.Security.Cryptography.MD4Managed::buffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___buffer_5;
	// System.UInt32[] Mono.Security.Cryptography.MD4Managed::count
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___count_6;
	// System.UInt32[] Mono.Security.Cryptography.MD4Managed::x
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___x_7;
	// System.Byte[] Mono.Security.Cryptography.MD4Managed::digest
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___digest_8;

public:
	inline static int32_t get_offset_of_state_4() { return static_cast<int32_t>(offsetof(MD4Managed_t1FC9A6CDB0A89A71416414689B3A0FF0283759D1, ___state_4)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_state_4() const { return ___state_4; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_state_4() { return &___state_4; }
	inline void set_state_4(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___state_4 = value;
		Il2CppCodeGenWriteBarrier((&___state_4), value);
	}

	inline static int32_t get_offset_of_buffer_5() { return static_cast<int32_t>(offsetof(MD4Managed_t1FC9A6CDB0A89A71416414689B3A0FF0283759D1, ___buffer_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_buffer_5() const { return ___buffer_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_buffer_5() { return &___buffer_5; }
	inline void set_buffer_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___buffer_5 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_5), value);
	}

	inline static int32_t get_offset_of_count_6() { return static_cast<int32_t>(offsetof(MD4Managed_t1FC9A6CDB0A89A71416414689B3A0FF0283759D1, ___count_6)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_count_6() const { return ___count_6; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_count_6() { return &___count_6; }
	inline void set_count_6(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___count_6 = value;
		Il2CppCodeGenWriteBarrier((&___count_6), value);
	}

	inline static int32_t get_offset_of_x_7() { return static_cast<int32_t>(offsetof(MD4Managed_t1FC9A6CDB0A89A71416414689B3A0FF0283759D1, ___x_7)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_x_7() const { return ___x_7; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_x_7() { return &___x_7; }
	inline void set_x_7(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___x_7 = value;
		Il2CppCodeGenWriteBarrier((&___x_7), value);
	}

	inline static int32_t get_offset_of_digest_8() { return static_cast<int32_t>(offsetof(MD4Managed_t1FC9A6CDB0A89A71416414689B3A0FF0283759D1, ___digest_8)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_digest_8() const { return ___digest_8; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_digest_8() { return &___digest_8; }
	inline void set_digest_8(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___digest_8 = value;
		Il2CppCodeGenWriteBarrier((&___digest_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MD4MANAGED_T1FC9A6CDB0A89A71416414689B3A0FF0283759D1_H
#ifndef ALERTDESCRIPTION_T8D4DE3060801044928816134B2292AFB933D40D6_H
#define ALERTDESCRIPTION_T8D4DE3060801044928816134B2292AFB933D40D6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Interface.AlertDescription
struct  AlertDescription_t8D4DE3060801044928816134B2292AFB933D40D6 
{
public:
	// System.Byte Mono.Security.Interface.AlertDescription::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AlertDescription_t8D4DE3060801044928816134B2292AFB933D40D6, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALERTDESCRIPTION_T8D4DE3060801044928816134B2292AFB933D40D6_H
#ifndef ALERTLEVEL_T300CD4F0586BC84361B20C4B26C89EC1ECB3FC34_H
#define ALERTLEVEL_T300CD4F0586BC84361B20C4B26C89EC1ECB3FC34_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Interface.AlertLevel
struct  AlertLevel_t300CD4F0586BC84361B20C4B26C89EC1ECB3FC34 
{
public:
	// System.Byte Mono.Security.Interface.AlertLevel::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AlertLevel_t300CD4F0586BC84361B20C4B26C89EC1ECB3FC34, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALERTLEVEL_T300CD4F0586BC84361B20C4B26C89EC1ECB3FC34_H
#ifndef CIPHERSUITECODE_T32674B07A5C552605FA138AEACFFA20474A255F1_H
#define CIPHERSUITECODE_T32674B07A5C552605FA138AEACFFA20474A255F1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Interface.CipherSuiteCode
struct  CipherSuiteCode_t32674B07A5C552605FA138AEACFFA20474A255F1 
{
public:
	// System.UInt16 Mono.Security.Interface.CipherSuiteCode::value__
	uint16_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CipherSuiteCode_t32674B07A5C552605FA138AEACFFA20474A255F1, ___value___2)); }
	inline uint16_t get_value___2() const { return ___value___2; }
	inline uint16_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint16_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CIPHERSUITECODE_T32674B07A5C552605FA138AEACFFA20474A255F1_H
#ifndef MONOSSLPOLICYERRORS_T5F32A4E793EAB8B8A8128A6A3E7690D2E1F666C7_H
#define MONOSSLPOLICYERRORS_T5F32A4E793EAB8B8A8128A6A3E7690D2E1F666C7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Interface.MonoSslPolicyErrors
struct  MonoSslPolicyErrors_t5F32A4E793EAB8B8A8128A6A3E7690D2E1F666C7 
{
public:
	// System.Int32 Mono.Security.Interface.MonoSslPolicyErrors::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MonoSslPolicyErrors_t5F32A4E793EAB8B8A8128A6A3E7690D2E1F666C7, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOSSLPOLICYERRORS_T5F32A4E793EAB8B8A8128A6A3E7690D2E1F666C7_H
#ifndef TLSPROTOCOLS_T25D1B0EFE5CC77B30D19258E7AC462AB4D828163_H
#define TLSPROTOCOLS_T25D1B0EFE5CC77B30D19258E7AC462AB4D828163_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Interface.TlsProtocols
struct  TlsProtocols_t25D1B0EFE5CC77B30D19258E7AC462AB4D828163 
{
public:
	// System.Int32 Mono.Security.Interface.TlsProtocols::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TlsProtocols_t25D1B0EFE5CC77B30D19258E7AC462AB4D828163, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSPROTOCOLS_T25D1B0EFE5CC77B30D19258E7AC462AB4D828163_H
#ifndef NTLMAUTHLEVEL_TF1354DE8BF43C36E20D475A077E035BB11936015_H
#define NTLMAUTHLEVEL_TF1354DE8BF43C36E20D475A077E035BB11936015_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Ntlm.NtlmAuthLevel
struct  NtlmAuthLevel_tF1354DE8BF43C36E20D475A077E035BB11936015 
{
public:
	// System.Int32 Mono.Security.Protocol.Ntlm.NtlmAuthLevel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NtlmAuthLevel_tF1354DE8BF43C36E20D475A077E035BB11936015, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NTLMAUTHLEVEL_TF1354DE8BF43C36E20D475A077E035BB11936015_H
#ifndef NTLMFLAGS_T9AC7D2604BC2E16EDEF1C9D2066E6BEC0D8A1578_H
#define NTLMFLAGS_T9AC7D2604BC2E16EDEF1C9D2066E6BEC0D8A1578_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Ntlm.NtlmFlags
struct  NtlmFlags_t9AC7D2604BC2E16EDEF1C9D2066E6BEC0D8A1578 
{
public:
	// System.Int32 Mono.Security.Protocol.Ntlm.NtlmFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NtlmFlags_t9AC7D2604BC2E16EDEF1C9D2066E6BEC0D8A1578, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NTLMFLAGS_T9AC7D2604BC2E16EDEF1C9D2066E6BEC0D8A1578_H
#ifndef CIPHERALGORITHMTYPE_TCC441699BE9EF7C9C52BF188AABDB09DB76F79AC_H
#define CIPHERALGORITHMTYPE_TCC441699BE9EF7C9C52BF188AABDB09DB76F79AC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.CipherAlgorithmType
struct  CipherAlgorithmType_tCC441699BE9EF7C9C52BF188AABDB09DB76F79AC 
{
public:
	// System.Int32 Mono.Security.Protocol.Tls.CipherAlgorithmType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CipherAlgorithmType_tCC441699BE9EF7C9C52BF188AABDB09DB76F79AC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CIPHERALGORITHMTYPE_TCC441699BE9EF7C9C52BF188AABDB09DB76F79AC_H
#ifndef CONTENTTYPE_T27CB29E77D75A61C8BD752E08D401276B3E3BA21_H
#define CONTENTTYPE_T27CB29E77D75A61C8BD752E08D401276B3E3BA21_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.ContentType
struct  ContentType_t27CB29E77D75A61C8BD752E08D401276B3E3BA21 
{
public:
	// System.Byte Mono.Security.Protocol.Tls.ContentType::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ContentType_t27CB29E77D75A61C8BD752E08D401276B3E3BA21, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTENTTYPE_T27CB29E77D75A61C8BD752E08D401276B3E3BA21_H
#ifndef EXCHANGEALGORITHMTYPE_TB2361714C59E334FFB4A81129D72E241702E281B_H
#define EXCHANGEALGORITHMTYPE_TB2361714C59E334FFB4A81129D72E241702E281B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.ExchangeAlgorithmType
struct  ExchangeAlgorithmType_tB2361714C59E334FFB4A81129D72E241702E281B 
{
public:
	// System.Int32 Mono.Security.Protocol.Tls.ExchangeAlgorithmType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ExchangeAlgorithmType_tB2361714C59E334FFB4A81129D72E241702E281B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCHANGEALGORITHMTYPE_TB2361714C59E334FFB4A81129D72E241702E281B_H
#ifndef CLIENTCERTIFICATETYPE_T251B7570F285455294C40356733287B19A9452EA_H
#define CLIENTCERTIFICATETYPE_T251B7570F285455294C40356733287B19A9452EA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.Handshake.ClientCertificateType
struct  ClientCertificateType_t251B7570F285455294C40356733287B19A9452EA 
{
public:
	// System.Int32 Mono.Security.Protocol.Tls.Handshake.ClientCertificateType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ClientCertificateType_t251B7570F285455294C40356733287B19A9452EA, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIENTCERTIFICATETYPE_T251B7570F285455294C40356733287B19A9452EA_H
#ifndef HANDSHAKETYPE_T398528545675E7ECA525E188AABAEDDCFD162EA2_H
#define HANDSHAKETYPE_T398528545675E7ECA525E188AABAEDDCFD162EA2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.Handshake.HandshakeType
struct  HandshakeType_t398528545675E7ECA525E188AABAEDDCFD162EA2 
{
public:
	// System.Byte Mono.Security.Protocol.Tls.Handshake.HandshakeType::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HandshakeType_t398528545675E7ECA525E188AABAEDDCFD162EA2, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HANDSHAKETYPE_T398528545675E7ECA525E188AABAEDDCFD162EA2_H
#ifndef HANDSHAKESTATE_TCABEB477DC355D6A3332D7864BB8FF6D0F8F03C3_H
#define HANDSHAKESTATE_TCABEB477DC355D6A3332D7864BB8FF6D0F8F03C3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.HandshakeState
struct  HandshakeState_tCABEB477DC355D6A3332D7864BB8FF6D0F8F03C3 
{
public:
	// System.Int32 Mono.Security.Protocol.Tls.HandshakeState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HandshakeState_tCABEB477DC355D6A3332D7864BB8FF6D0F8F03C3, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HANDSHAKESTATE_TCABEB477DC355D6A3332D7864BB8FF6D0F8F03C3_H
#ifndef HASHALGORITHMTYPE_T59C53DD6764DE8CE761AAB0C0A2A12C0C651C79D_H
#define HASHALGORITHMTYPE_T59C53DD6764DE8CE761AAB0C0A2A12C0C651C79D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.HashAlgorithmType
struct  HashAlgorithmType_t59C53DD6764DE8CE761AAB0C0A2A12C0C651C79D 
{
public:
	// System.Int32 Mono.Security.Protocol.Tls.HashAlgorithmType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HashAlgorithmType_t59C53DD6764DE8CE761AAB0C0A2A12C0C651C79D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HASHALGORITHMTYPE_T59C53DD6764DE8CE761AAB0C0A2A12C0C651C79D_H
#ifndef SECURITYCOMPRESSIONTYPE_T2D26DB7EEE0BA687EE1DD3C515EF405BB44A9650_H
#define SECURITYCOMPRESSIONTYPE_T2D26DB7EEE0BA687EE1DD3C515EF405BB44A9650_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.SecurityCompressionType
struct  SecurityCompressionType_t2D26DB7EEE0BA687EE1DD3C515EF405BB44A9650 
{
public:
	// System.Int32 Mono.Security.Protocol.Tls.SecurityCompressionType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SecurityCompressionType_t2D26DB7EEE0BA687EE1DD3C515EF405BB44A9650, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECURITYCOMPRESSIONTYPE_T2D26DB7EEE0BA687EE1DD3C515EF405BB44A9650_H
#ifndef SECURITYPROTOCOLTYPE_TF81DFFC385530B08E8E214306D65F2C08537DF4C_H
#define SECURITYPROTOCOLTYPE_TF81DFFC385530B08E8E214306D65F2C08537DF4C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.SecurityProtocolType
struct  SecurityProtocolType_tF81DFFC385530B08E8E214306D65F2C08537DF4C 
{
public:
	// System.Int32 Mono.Security.Protocol.Tls.SecurityProtocolType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SecurityProtocolType_tF81DFFC385530B08E8E214306D65F2C08537DF4C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECURITYPROTOCOLTYPE_TF81DFFC385530B08E8E214306D65F2C08537DF4C_H
#ifndef NEGOTIATESTATE_T44FFDA234900CC8B794FA72862A6D800C1AEFCCB_H
#define NEGOTIATESTATE_T44FFDA234900CC8B794FA72862A6D800C1AEFCCB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.SslClientStream_NegotiateState
struct  NegotiateState_t44FFDA234900CC8B794FA72862A6D800C1AEFCCB 
{
public:
	// System.Int32 Mono.Security.Protocol.Tls.SslClientStream_NegotiateState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NegotiateState_t44FFDA234900CC8B794FA72862A6D800C1AEFCCB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NEGOTIATESTATE_T44FFDA234900CC8B794FA72862A6D800C1AEFCCB_H
#ifndef SSLSTREAMBASE_TE9E16CABCE59BAA56CFFBD9B84C28335730E9197_H
#define SSLSTREAMBASE_TE9E16CABCE59BAA56CFFBD9B84C28335730E9197_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.SslStreamBase
struct  SslStreamBase_tE9E16CABCE59BAA56CFFBD9B84C28335730E9197  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// System.IO.Stream Mono.Security.Protocol.Tls.SslStreamBase::innerStream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___innerStream_6;
	// System.IO.MemoryStream Mono.Security.Protocol.Tls.SslStreamBase::inputBuffer
	MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * ___inputBuffer_7;
	// Mono.Security.Protocol.Tls.Context Mono.Security.Protocol.Tls.SslStreamBase::context
	Context_t17219BB52552BCACB15DAF9B6618835642F7F56F * ___context_8;
	// Mono.Security.Protocol.Tls.RecordProtocol Mono.Security.Protocol.Tls.SslStreamBase::protocol
	RecordProtocol_tFA651C8422AECEEE03AABEDB029EC83EB8833CFA * ___protocol_9;
	// System.Boolean Mono.Security.Protocol.Tls.SslStreamBase::ownsStream
	bool ___ownsStream_10;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) Mono.Security.Protocol.Tls.SslStreamBase::disposed
	bool ___disposed_11;
	// System.Boolean Mono.Security.Protocol.Tls.SslStreamBase::checkCertRevocationStatus
	bool ___checkCertRevocationStatus_12;
	// System.Object Mono.Security.Protocol.Tls.SslStreamBase::negotiate
	RuntimeObject * ___negotiate_13;
	// System.Object Mono.Security.Protocol.Tls.SslStreamBase::read
	RuntimeObject * ___read_14;
	// System.Object Mono.Security.Protocol.Tls.SslStreamBase::write
	RuntimeObject * ___write_15;
	// System.Threading.ManualResetEvent Mono.Security.Protocol.Tls.SslStreamBase::negotiationComplete
	ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * ___negotiationComplete_16;
	// System.Byte[] Mono.Security.Protocol.Tls.SslStreamBase::recbuf
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___recbuf_17;
	// System.IO.MemoryStream Mono.Security.Protocol.Tls.SslStreamBase::recordStream
	MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * ___recordStream_18;

public:
	inline static int32_t get_offset_of_innerStream_6() { return static_cast<int32_t>(offsetof(SslStreamBase_tE9E16CABCE59BAA56CFFBD9B84C28335730E9197, ___innerStream_6)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_innerStream_6() const { return ___innerStream_6; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_innerStream_6() { return &___innerStream_6; }
	inline void set_innerStream_6(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___innerStream_6 = value;
		Il2CppCodeGenWriteBarrier((&___innerStream_6), value);
	}

	inline static int32_t get_offset_of_inputBuffer_7() { return static_cast<int32_t>(offsetof(SslStreamBase_tE9E16CABCE59BAA56CFFBD9B84C28335730E9197, ___inputBuffer_7)); }
	inline MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * get_inputBuffer_7() const { return ___inputBuffer_7; }
	inline MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C ** get_address_of_inputBuffer_7() { return &___inputBuffer_7; }
	inline void set_inputBuffer_7(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * value)
	{
		___inputBuffer_7 = value;
		Il2CppCodeGenWriteBarrier((&___inputBuffer_7), value);
	}

	inline static int32_t get_offset_of_context_8() { return static_cast<int32_t>(offsetof(SslStreamBase_tE9E16CABCE59BAA56CFFBD9B84C28335730E9197, ___context_8)); }
	inline Context_t17219BB52552BCACB15DAF9B6618835642F7F56F * get_context_8() const { return ___context_8; }
	inline Context_t17219BB52552BCACB15DAF9B6618835642F7F56F ** get_address_of_context_8() { return &___context_8; }
	inline void set_context_8(Context_t17219BB52552BCACB15DAF9B6618835642F7F56F * value)
	{
		___context_8 = value;
		Il2CppCodeGenWriteBarrier((&___context_8), value);
	}

	inline static int32_t get_offset_of_protocol_9() { return static_cast<int32_t>(offsetof(SslStreamBase_tE9E16CABCE59BAA56CFFBD9B84C28335730E9197, ___protocol_9)); }
	inline RecordProtocol_tFA651C8422AECEEE03AABEDB029EC83EB8833CFA * get_protocol_9() const { return ___protocol_9; }
	inline RecordProtocol_tFA651C8422AECEEE03AABEDB029EC83EB8833CFA ** get_address_of_protocol_9() { return &___protocol_9; }
	inline void set_protocol_9(RecordProtocol_tFA651C8422AECEEE03AABEDB029EC83EB8833CFA * value)
	{
		___protocol_9 = value;
		Il2CppCodeGenWriteBarrier((&___protocol_9), value);
	}

	inline static int32_t get_offset_of_ownsStream_10() { return static_cast<int32_t>(offsetof(SslStreamBase_tE9E16CABCE59BAA56CFFBD9B84C28335730E9197, ___ownsStream_10)); }
	inline bool get_ownsStream_10() const { return ___ownsStream_10; }
	inline bool* get_address_of_ownsStream_10() { return &___ownsStream_10; }
	inline void set_ownsStream_10(bool value)
	{
		___ownsStream_10 = value;
	}

	inline static int32_t get_offset_of_disposed_11() { return static_cast<int32_t>(offsetof(SslStreamBase_tE9E16CABCE59BAA56CFFBD9B84C28335730E9197, ___disposed_11)); }
	inline bool get_disposed_11() const { return ___disposed_11; }
	inline bool* get_address_of_disposed_11() { return &___disposed_11; }
	inline void set_disposed_11(bool value)
	{
		___disposed_11 = value;
	}

	inline static int32_t get_offset_of_checkCertRevocationStatus_12() { return static_cast<int32_t>(offsetof(SslStreamBase_tE9E16CABCE59BAA56CFFBD9B84C28335730E9197, ___checkCertRevocationStatus_12)); }
	inline bool get_checkCertRevocationStatus_12() const { return ___checkCertRevocationStatus_12; }
	inline bool* get_address_of_checkCertRevocationStatus_12() { return &___checkCertRevocationStatus_12; }
	inline void set_checkCertRevocationStatus_12(bool value)
	{
		___checkCertRevocationStatus_12 = value;
	}

	inline static int32_t get_offset_of_negotiate_13() { return static_cast<int32_t>(offsetof(SslStreamBase_tE9E16CABCE59BAA56CFFBD9B84C28335730E9197, ___negotiate_13)); }
	inline RuntimeObject * get_negotiate_13() const { return ___negotiate_13; }
	inline RuntimeObject ** get_address_of_negotiate_13() { return &___negotiate_13; }
	inline void set_negotiate_13(RuntimeObject * value)
	{
		___negotiate_13 = value;
		Il2CppCodeGenWriteBarrier((&___negotiate_13), value);
	}

	inline static int32_t get_offset_of_read_14() { return static_cast<int32_t>(offsetof(SslStreamBase_tE9E16CABCE59BAA56CFFBD9B84C28335730E9197, ___read_14)); }
	inline RuntimeObject * get_read_14() const { return ___read_14; }
	inline RuntimeObject ** get_address_of_read_14() { return &___read_14; }
	inline void set_read_14(RuntimeObject * value)
	{
		___read_14 = value;
		Il2CppCodeGenWriteBarrier((&___read_14), value);
	}

	inline static int32_t get_offset_of_write_15() { return static_cast<int32_t>(offsetof(SslStreamBase_tE9E16CABCE59BAA56CFFBD9B84C28335730E9197, ___write_15)); }
	inline RuntimeObject * get_write_15() const { return ___write_15; }
	inline RuntimeObject ** get_address_of_write_15() { return &___write_15; }
	inline void set_write_15(RuntimeObject * value)
	{
		___write_15 = value;
		Il2CppCodeGenWriteBarrier((&___write_15), value);
	}

	inline static int32_t get_offset_of_negotiationComplete_16() { return static_cast<int32_t>(offsetof(SslStreamBase_tE9E16CABCE59BAA56CFFBD9B84C28335730E9197, ___negotiationComplete_16)); }
	inline ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * get_negotiationComplete_16() const { return ___negotiationComplete_16; }
	inline ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 ** get_address_of_negotiationComplete_16() { return &___negotiationComplete_16; }
	inline void set_negotiationComplete_16(ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * value)
	{
		___negotiationComplete_16 = value;
		Il2CppCodeGenWriteBarrier((&___negotiationComplete_16), value);
	}

	inline static int32_t get_offset_of_recbuf_17() { return static_cast<int32_t>(offsetof(SslStreamBase_tE9E16CABCE59BAA56CFFBD9B84C28335730E9197, ___recbuf_17)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_recbuf_17() const { return ___recbuf_17; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_recbuf_17() { return &___recbuf_17; }
	inline void set_recbuf_17(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___recbuf_17 = value;
		Il2CppCodeGenWriteBarrier((&___recbuf_17), value);
	}

	inline static int32_t get_offset_of_recordStream_18() { return static_cast<int32_t>(offsetof(SslStreamBase_tE9E16CABCE59BAA56CFFBD9B84C28335730E9197, ___recordStream_18)); }
	inline MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * get_recordStream_18() const { return ___recordStream_18; }
	inline MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C ** get_address_of_recordStream_18() { return &___recordStream_18; }
	inline void set_recordStream_18(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * value)
	{
		___recordStream_18 = value;
		Il2CppCodeGenWriteBarrier((&___recordStream_18), value);
	}
};

struct SslStreamBase_tE9E16CABCE59BAA56CFFBD9B84C28335730E9197_StaticFields
{
public:
	// System.Threading.ManualResetEvent Mono.Security.Protocol.Tls.SslStreamBase::record_processing
	ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * ___record_processing_5;

public:
	inline static int32_t get_offset_of_record_processing_5() { return static_cast<int32_t>(offsetof(SslStreamBase_tE9E16CABCE59BAA56CFFBD9B84C28335730E9197_StaticFields, ___record_processing_5)); }
	inline ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * get_record_processing_5() const { return ___record_processing_5; }
	inline ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 ** get_address_of_record_processing_5() { return &___record_processing_5; }
	inline void set_record_processing_5(ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * value)
	{
		___record_processing_5 = value;
		Il2CppCodeGenWriteBarrier((&___record_processing_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SSLSTREAMBASE_TE9E16CABCE59BAA56CFFBD9B84C28335730E9197_H
#ifndef TLSSERVERSETTINGS_TD19093EA2CFCCD92F9B0FE63A9B2FAA9B4B45788_H
#define TLSSERVERSETTINGS_TD19093EA2CFCCD92F9B0FE63A9B2FAA9B4B45788_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.TlsServerSettings
struct  TlsServerSettings_tD19093EA2CFCCD92F9B0FE63A9B2FAA9B4B45788  : public RuntimeObject
{
public:
	// Mono.Security.X509.X509CertificateCollection Mono.Security.Protocol.Tls.TlsServerSettings::certificates
	X509CertificateCollection_t423BA1B9FAA983BA745023994C648C6DAC3E5A1A * ___certificates_0;
	// System.Security.Cryptography.RSA Mono.Security.Protocol.Tls.TlsServerSettings::certificateRSA
	RSA_tB6C4B434B2AC02E3F8981DB2908C2018E251D145 * ___certificateRSA_1;
	// System.Security.Cryptography.RSAParameters Mono.Security.Protocol.Tls.TlsServerSettings::rsaParameters
	RSAParameters_t6A568C1275FA8F8C02615666D998134DCFFB9717  ___rsaParameters_2;
	// System.Byte[] Mono.Security.Protocol.Tls.TlsServerSettings::signedParams
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___signedParams_3;
	// System.String[] Mono.Security.Protocol.Tls.TlsServerSettings::distinguisedNames
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___distinguisedNames_4;
	// System.Boolean Mono.Security.Protocol.Tls.TlsServerSettings::serverKeyExchange
	bool ___serverKeyExchange_5;
	// System.Boolean Mono.Security.Protocol.Tls.TlsServerSettings::certificateRequest
	bool ___certificateRequest_6;
	// Mono.Security.Protocol.Tls.Handshake.ClientCertificateType[] Mono.Security.Protocol.Tls.TlsServerSettings::certificateTypes
	ClientCertificateTypeU5BU5D_t06B6854DC307CF9A4DC750B89CEC349214A2147B* ___certificateTypes_7;

public:
	inline static int32_t get_offset_of_certificates_0() { return static_cast<int32_t>(offsetof(TlsServerSettings_tD19093EA2CFCCD92F9B0FE63A9B2FAA9B4B45788, ___certificates_0)); }
	inline X509CertificateCollection_t423BA1B9FAA983BA745023994C648C6DAC3E5A1A * get_certificates_0() const { return ___certificates_0; }
	inline X509CertificateCollection_t423BA1B9FAA983BA745023994C648C6DAC3E5A1A ** get_address_of_certificates_0() { return &___certificates_0; }
	inline void set_certificates_0(X509CertificateCollection_t423BA1B9FAA983BA745023994C648C6DAC3E5A1A * value)
	{
		___certificates_0 = value;
		Il2CppCodeGenWriteBarrier((&___certificates_0), value);
	}

	inline static int32_t get_offset_of_certificateRSA_1() { return static_cast<int32_t>(offsetof(TlsServerSettings_tD19093EA2CFCCD92F9B0FE63A9B2FAA9B4B45788, ___certificateRSA_1)); }
	inline RSA_tB6C4B434B2AC02E3F8981DB2908C2018E251D145 * get_certificateRSA_1() const { return ___certificateRSA_1; }
	inline RSA_tB6C4B434B2AC02E3F8981DB2908C2018E251D145 ** get_address_of_certificateRSA_1() { return &___certificateRSA_1; }
	inline void set_certificateRSA_1(RSA_tB6C4B434B2AC02E3F8981DB2908C2018E251D145 * value)
	{
		___certificateRSA_1 = value;
		Il2CppCodeGenWriteBarrier((&___certificateRSA_1), value);
	}

	inline static int32_t get_offset_of_rsaParameters_2() { return static_cast<int32_t>(offsetof(TlsServerSettings_tD19093EA2CFCCD92F9B0FE63A9B2FAA9B4B45788, ___rsaParameters_2)); }
	inline RSAParameters_t6A568C1275FA8F8C02615666D998134DCFFB9717  get_rsaParameters_2() const { return ___rsaParameters_2; }
	inline RSAParameters_t6A568C1275FA8F8C02615666D998134DCFFB9717 * get_address_of_rsaParameters_2() { return &___rsaParameters_2; }
	inline void set_rsaParameters_2(RSAParameters_t6A568C1275FA8F8C02615666D998134DCFFB9717  value)
	{
		___rsaParameters_2 = value;
	}

	inline static int32_t get_offset_of_signedParams_3() { return static_cast<int32_t>(offsetof(TlsServerSettings_tD19093EA2CFCCD92F9B0FE63A9B2FAA9B4B45788, ___signedParams_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_signedParams_3() const { return ___signedParams_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_signedParams_3() { return &___signedParams_3; }
	inline void set_signedParams_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___signedParams_3 = value;
		Il2CppCodeGenWriteBarrier((&___signedParams_3), value);
	}

	inline static int32_t get_offset_of_distinguisedNames_4() { return static_cast<int32_t>(offsetof(TlsServerSettings_tD19093EA2CFCCD92F9B0FE63A9B2FAA9B4B45788, ___distinguisedNames_4)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_distinguisedNames_4() const { return ___distinguisedNames_4; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_distinguisedNames_4() { return &___distinguisedNames_4; }
	inline void set_distinguisedNames_4(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___distinguisedNames_4 = value;
		Il2CppCodeGenWriteBarrier((&___distinguisedNames_4), value);
	}

	inline static int32_t get_offset_of_serverKeyExchange_5() { return static_cast<int32_t>(offsetof(TlsServerSettings_tD19093EA2CFCCD92F9B0FE63A9B2FAA9B4B45788, ___serverKeyExchange_5)); }
	inline bool get_serverKeyExchange_5() const { return ___serverKeyExchange_5; }
	inline bool* get_address_of_serverKeyExchange_5() { return &___serverKeyExchange_5; }
	inline void set_serverKeyExchange_5(bool value)
	{
		___serverKeyExchange_5 = value;
	}

	inline static int32_t get_offset_of_certificateRequest_6() { return static_cast<int32_t>(offsetof(TlsServerSettings_tD19093EA2CFCCD92F9B0FE63A9B2FAA9B4B45788, ___certificateRequest_6)); }
	inline bool get_certificateRequest_6() const { return ___certificateRequest_6; }
	inline bool* get_address_of_certificateRequest_6() { return &___certificateRequest_6; }
	inline void set_certificateRequest_6(bool value)
	{
		___certificateRequest_6 = value;
	}

	inline static int32_t get_offset_of_certificateTypes_7() { return static_cast<int32_t>(offsetof(TlsServerSettings_tD19093EA2CFCCD92F9B0FE63A9B2FAA9B4B45788, ___certificateTypes_7)); }
	inline ClientCertificateTypeU5BU5D_t06B6854DC307CF9A4DC750B89CEC349214A2147B* get_certificateTypes_7() const { return ___certificateTypes_7; }
	inline ClientCertificateTypeU5BU5D_t06B6854DC307CF9A4DC750B89CEC349214A2147B** get_address_of_certificateTypes_7() { return &___certificateTypes_7; }
	inline void set_certificateTypes_7(ClientCertificateTypeU5BU5D_t06B6854DC307CF9A4DC750B89CEC349214A2147B* value)
	{
		___certificateTypes_7 = value;
		Il2CppCodeGenWriteBarrier((&___certificateTypes_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSSERVERSETTINGS_TD19093EA2CFCCD92F9B0FE63A9B2FAA9B4B45788_H
#ifndef TLSSTREAM_TB739E33AAABEC563984AD1EA75D7F4CFB4BF27E5_H
#define TLSSTREAM_TB739E33AAABEC563984AD1EA75D7F4CFB4BF27E5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.TlsStream
struct  TlsStream_tB739E33AAABEC563984AD1EA75D7F4CFB4BF27E5  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// System.Boolean Mono.Security.Protocol.Tls.TlsStream::canRead
	bool ___canRead_5;
	// System.Boolean Mono.Security.Protocol.Tls.TlsStream::canWrite
	bool ___canWrite_6;
	// System.IO.MemoryStream Mono.Security.Protocol.Tls.TlsStream::buffer
	MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * ___buffer_7;
	// System.Byte[] Mono.Security.Protocol.Tls.TlsStream::temp
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___temp_8;

public:
	inline static int32_t get_offset_of_canRead_5() { return static_cast<int32_t>(offsetof(TlsStream_tB739E33AAABEC563984AD1EA75D7F4CFB4BF27E5, ___canRead_5)); }
	inline bool get_canRead_5() const { return ___canRead_5; }
	inline bool* get_address_of_canRead_5() { return &___canRead_5; }
	inline void set_canRead_5(bool value)
	{
		___canRead_5 = value;
	}

	inline static int32_t get_offset_of_canWrite_6() { return static_cast<int32_t>(offsetof(TlsStream_tB739E33AAABEC563984AD1EA75D7F4CFB4BF27E5, ___canWrite_6)); }
	inline bool get_canWrite_6() const { return ___canWrite_6; }
	inline bool* get_address_of_canWrite_6() { return &___canWrite_6; }
	inline void set_canWrite_6(bool value)
	{
		___canWrite_6 = value;
	}

	inline static int32_t get_offset_of_buffer_7() { return static_cast<int32_t>(offsetof(TlsStream_tB739E33AAABEC563984AD1EA75D7F4CFB4BF27E5, ___buffer_7)); }
	inline MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * get_buffer_7() const { return ___buffer_7; }
	inline MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C ** get_address_of_buffer_7() { return &___buffer_7; }
	inline void set_buffer_7(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * value)
	{
		___buffer_7 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_7), value);
	}

	inline static int32_t get_offset_of_temp_8() { return static_cast<int32_t>(offsetof(TlsStream_tB739E33AAABEC563984AD1EA75D7F4CFB4BF27E5, ___temp_8)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_temp_8() const { return ___temp_8; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_temp_8() { return &___temp_8; }
	inline void set_temp_8(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___temp_8 = value;
		Il2CppCodeGenWriteBarrier((&___temp_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSSTREAM_TB739E33AAABEC563984AD1EA75D7F4CFB4BF27E5_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef NULLABLE_1_T3290384E361396B3724B88B498CBF637D7E87B78_H
#define NULLABLE_1_T3290384E361396B3724B88B498CBF637D7E87B78_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.DateTime>
struct  Nullable_1_t3290384E361396B3724B88B498CBF637D7E87B78 
{
public:
	// T System.Nullable`1::value
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t3290384E361396B3724B88B498CBF637D7E87B78, ___value_0)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_value_0() const { return ___value_0; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t3290384E361396B3724B88B498CBF637D7E87B78, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T3290384E361396B3724B88B498CBF637D7E87B78_H
#ifndef CIPHERMODE_T1DC3069D617AC3D17A2608F5BB36C0F115D229DF_H
#define CIPHERMODE_T1DC3069D617AC3D17A2608F5BB36C0F115D229DF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.CipherMode
struct  CipherMode_t1DC3069D617AC3D17A2608F5BB36C0F115D229DF 
{
public:
	// System.Int32 System.Security.Cryptography.CipherMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CipherMode_t1DC3069D617AC3D17A2608F5BB36C0F115D229DF, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CIPHERMODE_T1DC3069D617AC3D17A2608F5BB36C0F115D229DF_H
#ifndef PADDINGMODE_TA6F228B2795D29C9554F2D6824DB9FF67519A0E0_H
#define PADDINGMODE_TA6F228B2795D29C9554F2D6824DB9FF67519A0E0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.PaddingMode
struct  PaddingMode_tA6F228B2795D29C9554F2D6824DB9FF67519A0E0 
{
public:
	// System.Int32 System.Security.Cryptography.PaddingMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PaddingMode_tA6F228B2795D29C9554F2D6824DB9FF67519A0E0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PADDINGMODE_TA6F228B2795D29C9554F2D6824DB9FF67519A0E0_H
#ifndef ALERT_TABF269545F2C583CCA47FF574E612DDAF232944E_H
#define ALERT_TABF269545F2C583CCA47FF574E612DDAF232944E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Interface.Alert
struct  Alert_tABF269545F2C583CCA47FF574E612DDAF232944E  : public RuntimeObject
{
public:
	// Mono.Security.Interface.AlertLevel Mono.Security.Interface.Alert::level
	uint8_t ___level_0;
	// Mono.Security.Interface.AlertDescription Mono.Security.Interface.Alert::description
	uint8_t ___description_1;

public:
	inline static int32_t get_offset_of_level_0() { return static_cast<int32_t>(offsetof(Alert_tABF269545F2C583CCA47FF574E612DDAF232944E, ___level_0)); }
	inline uint8_t get_level_0() const { return ___level_0; }
	inline uint8_t* get_address_of_level_0() { return &___level_0; }
	inline void set_level_0(uint8_t value)
	{
		___level_0 = value;
	}

	inline static int32_t get_offset_of_description_1() { return static_cast<int32_t>(offsetof(Alert_tABF269545F2C583CCA47FF574E612DDAF232944E, ___description_1)); }
	inline uint8_t get_description_1() const { return ___description_1; }
	inline uint8_t* get_address_of_description_1() { return &___description_1; }
	inline void set_description_1(uint8_t value)
	{
		___description_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALERT_TABF269545F2C583CCA47FF574E612DDAF232944E_H
#ifndef MONOTLSCONNECTIONINFO_TE32F709ECF061DD150F45384869CE8431BD7A74D_H
#define MONOTLSCONNECTIONINFO_TE32F709ECF061DD150F45384869CE8431BD7A74D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Interface.MonoTlsConnectionInfo
struct  MonoTlsConnectionInfo_tE32F709ECF061DD150F45384869CE8431BD7A74D  : public RuntimeObject
{
public:
	// Mono.Security.Interface.CipherSuiteCode Mono.Security.Interface.MonoTlsConnectionInfo::<CipherSuiteCode>k__BackingField
	uint16_t ___U3CCipherSuiteCodeU3Ek__BackingField_0;
	// Mono.Security.Interface.TlsProtocols Mono.Security.Interface.MonoTlsConnectionInfo::<ProtocolVersion>k__BackingField
	int32_t ___U3CProtocolVersionU3Ek__BackingField_1;
	// System.String Mono.Security.Interface.MonoTlsConnectionInfo::<PeerDomainName>k__BackingField
	String_t* ___U3CPeerDomainNameU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CCipherSuiteCodeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MonoTlsConnectionInfo_tE32F709ECF061DD150F45384869CE8431BD7A74D, ___U3CCipherSuiteCodeU3Ek__BackingField_0)); }
	inline uint16_t get_U3CCipherSuiteCodeU3Ek__BackingField_0() const { return ___U3CCipherSuiteCodeU3Ek__BackingField_0; }
	inline uint16_t* get_address_of_U3CCipherSuiteCodeU3Ek__BackingField_0() { return &___U3CCipherSuiteCodeU3Ek__BackingField_0; }
	inline void set_U3CCipherSuiteCodeU3Ek__BackingField_0(uint16_t value)
	{
		___U3CCipherSuiteCodeU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CProtocolVersionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(MonoTlsConnectionInfo_tE32F709ECF061DD150F45384869CE8431BD7A74D, ___U3CProtocolVersionU3Ek__BackingField_1)); }
	inline int32_t get_U3CProtocolVersionU3Ek__BackingField_1() const { return ___U3CProtocolVersionU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CProtocolVersionU3Ek__BackingField_1() { return &___U3CProtocolVersionU3Ek__BackingField_1; }
	inline void set_U3CProtocolVersionU3Ek__BackingField_1(int32_t value)
	{
		___U3CProtocolVersionU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CPeerDomainNameU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(MonoTlsConnectionInfo_tE32F709ECF061DD150F45384869CE8431BD7A74D, ___U3CPeerDomainNameU3Ek__BackingField_2)); }
	inline String_t* get_U3CPeerDomainNameU3Ek__BackingField_2() const { return ___U3CPeerDomainNameU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CPeerDomainNameU3Ek__BackingField_2() { return &___U3CPeerDomainNameU3Ek__BackingField_2; }
	inline void set_U3CPeerDomainNameU3Ek__BackingField_2(String_t* value)
	{
		___U3CPeerDomainNameU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPeerDomainNameU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOTLSCONNECTIONINFO_TE32F709ECF061DD150F45384869CE8431BD7A74D_H
#ifndef MESSAGEBASE_T504D166CC4021DEB56DED308D5E82C67F47F26C0_H
#define MESSAGEBASE_T504D166CC4021DEB56DED308D5E82C67F47F26C0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Ntlm.MessageBase
struct  MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0  : public RuntimeObject
{
public:
	// System.Int32 Mono.Security.Protocol.Ntlm.MessageBase::_type
	int32_t ____type_1;
	// Mono.Security.Protocol.Ntlm.NtlmFlags Mono.Security.Protocol.Ntlm.MessageBase::_flags
	int32_t ____flags_2;

public:
	inline static int32_t get_offset_of__type_1() { return static_cast<int32_t>(offsetof(MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0, ____type_1)); }
	inline int32_t get__type_1() const { return ____type_1; }
	inline int32_t* get_address_of__type_1() { return &____type_1; }
	inline void set__type_1(int32_t value)
	{
		____type_1 = value;
	}

	inline static int32_t get_offset_of__flags_2() { return static_cast<int32_t>(offsetof(MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0, ____flags_2)); }
	inline int32_t get__flags_2() const { return ____flags_2; }
	inline int32_t* get_address_of__flags_2() { return &____flags_2; }
	inline void set__flags_2(int32_t value)
	{
		____flags_2 = value;
	}
};

struct MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0_StaticFields
{
public:
	// System.Byte[] Mono.Security.Protocol.Ntlm.MessageBase::header
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___header_0;

public:
	inline static int32_t get_offset_of_header_0() { return static_cast<int32_t>(offsetof(MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0_StaticFields, ___header_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_header_0() const { return ___header_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_header_0() { return &___header_0; }
	inline void set_header_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___header_0 = value;
		Il2CppCodeGenWriteBarrier((&___header_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGEBASE_T504D166CC4021DEB56DED308D5E82C67F47F26C0_H
#ifndef NTLMSETTINGS_TC673E811873A17EA73FCA0EFD6D33839B5036009_H
#define NTLMSETTINGS_TC673E811873A17EA73FCA0EFD6D33839B5036009_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Ntlm.NtlmSettings
struct  NtlmSettings_tC673E811873A17EA73FCA0EFD6D33839B5036009  : public RuntimeObject
{
public:

public:
};

struct NtlmSettings_tC673E811873A17EA73FCA0EFD6D33839B5036009_StaticFields
{
public:
	// Mono.Security.Protocol.Ntlm.NtlmAuthLevel Mono.Security.Protocol.Ntlm.NtlmSettings::defaultAuthLevel
	int32_t ___defaultAuthLevel_0;

public:
	inline static int32_t get_offset_of_defaultAuthLevel_0() { return static_cast<int32_t>(offsetof(NtlmSettings_tC673E811873A17EA73FCA0EFD6D33839B5036009_StaticFields, ___defaultAuthLevel_0)); }
	inline int32_t get_defaultAuthLevel_0() const { return ___defaultAuthLevel_0; }
	inline int32_t* get_address_of_defaultAuthLevel_0() { return &___defaultAuthLevel_0; }
	inline void set_defaultAuthLevel_0(int32_t value)
	{
		___defaultAuthLevel_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NTLMSETTINGS_TC673E811873A17EA73FCA0EFD6D33839B5036009_H
#ifndef CIPHERSUITE_T203D2B4A797805B775BEFB3B623962E7B407B6F7_H
#define CIPHERSUITE_T203D2B4A797805B775BEFB3B623962E7B407B6F7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.CipherSuite
struct  CipherSuite_t203D2B4A797805B775BEFB3B623962E7B407B6F7  : public RuntimeObject
{
public:
	// System.Int16 Mono.Security.Protocol.Tls.CipherSuite::code
	int16_t ___code_1;
	// System.String Mono.Security.Protocol.Tls.CipherSuite::name
	String_t* ___name_2;
	// Mono.Security.Protocol.Tls.CipherAlgorithmType Mono.Security.Protocol.Tls.CipherSuite::cipherAlgorithmType
	int32_t ___cipherAlgorithmType_3;
	// Mono.Security.Protocol.Tls.HashAlgorithmType Mono.Security.Protocol.Tls.CipherSuite::hashAlgorithmType
	int32_t ___hashAlgorithmType_4;
	// Mono.Security.Protocol.Tls.ExchangeAlgorithmType Mono.Security.Protocol.Tls.CipherSuite::exchangeAlgorithmType
	int32_t ___exchangeAlgorithmType_5;
	// System.Boolean Mono.Security.Protocol.Tls.CipherSuite::isExportable
	bool ___isExportable_6;
	// System.Security.Cryptography.CipherMode Mono.Security.Protocol.Tls.CipherSuite::cipherMode
	int32_t ___cipherMode_7;
	// System.Byte Mono.Security.Protocol.Tls.CipherSuite::keyMaterialSize
	uint8_t ___keyMaterialSize_8;
	// System.Int32 Mono.Security.Protocol.Tls.CipherSuite::keyBlockSize
	int32_t ___keyBlockSize_9;
	// System.Byte Mono.Security.Protocol.Tls.CipherSuite::expandedKeyMaterialSize
	uint8_t ___expandedKeyMaterialSize_10;
	// System.Int16 Mono.Security.Protocol.Tls.CipherSuite::effectiveKeyBits
	int16_t ___effectiveKeyBits_11;
	// System.Byte Mono.Security.Protocol.Tls.CipherSuite::ivSize
	uint8_t ___ivSize_12;
	// System.Byte Mono.Security.Protocol.Tls.CipherSuite::blockSize
	uint8_t ___blockSize_13;
	// Mono.Security.Protocol.Tls.Context Mono.Security.Protocol.Tls.CipherSuite::context
	Context_t17219BB52552BCACB15DAF9B6618835642F7F56F * ___context_14;
	// System.Security.Cryptography.SymmetricAlgorithm Mono.Security.Protocol.Tls.CipherSuite::encryptionAlgorithm
	SymmetricAlgorithm_t0A2EC7E7AD8B8976832B4F0AC432B691F862E789 * ___encryptionAlgorithm_15;
	// System.Security.Cryptography.ICryptoTransform Mono.Security.Protocol.Tls.CipherSuite::encryptionCipher
	RuntimeObject* ___encryptionCipher_16;
	// System.Security.Cryptography.SymmetricAlgorithm Mono.Security.Protocol.Tls.CipherSuite::decryptionAlgorithm
	SymmetricAlgorithm_t0A2EC7E7AD8B8976832B4F0AC432B691F862E789 * ___decryptionAlgorithm_17;
	// System.Security.Cryptography.ICryptoTransform Mono.Security.Protocol.Tls.CipherSuite::decryptionCipher
	RuntimeObject* ___decryptionCipher_18;
	// System.Security.Cryptography.KeyedHashAlgorithm Mono.Security.Protocol.Tls.CipherSuite::clientHMAC
	KeyedHashAlgorithm_t83CFA2CA5A4F0F39B747E61D013CB5EB919D218B * ___clientHMAC_19;
	// System.Security.Cryptography.KeyedHashAlgorithm Mono.Security.Protocol.Tls.CipherSuite::serverHMAC
	KeyedHashAlgorithm_t83CFA2CA5A4F0F39B747E61D013CB5EB919D218B * ___serverHMAC_20;

public:
	inline static int32_t get_offset_of_code_1() { return static_cast<int32_t>(offsetof(CipherSuite_t203D2B4A797805B775BEFB3B623962E7B407B6F7, ___code_1)); }
	inline int16_t get_code_1() const { return ___code_1; }
	inline int16_t* get_address_of_code_1() { return &___code_1; }
	inline void set_code_1(int16_t value)
	{
		___code_1 = value;
	}

	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(CipherSuite_t203D2B4A797805B775BEFB3B623962E7B407B6F7, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier((&___name_2), value);
	}

	inline static int32_t get_offset_of_cipherAlgorithmType_3() { return static_cast<int32_t>(offsetof(CipherSuite_t203D2B4A797805B775BEFB3B623962E7B407B6F7, ___cipherAlgorithmType_3)); }
	inline int32_t get_cipherAlgorithmType_3() const { return ___cipherAlgorithmType_3; }
	inline int32_t* get_address_of_cipherAlgorithmType_3() { return &___cipherAlgorithmType_3; }
	inline void set_cipherAlgorithmType_3(int32_t value)
	{
		___cipherAlgorithmType_3 = value;
	}

	inline static int32_t get_offset_of_hashAlgorithmType_4() { return static_cast<int32_t>(offsetof(CipherSuite_t203D2B4A797805B775BEFB3B623962E7B407B6F7, ___hashAlgorithmType_4)); }
	inline int32_t get_hashAlgorithmType_4() const { return ___hashAlgorithmType_4; }
	inline int32_t* get_address_of_hashAlgorithmType_4() { return &___hashAlgorithmType_4; }
	inline void set_hashAlgorithmType_4(int32_t value)
	{
		___hashAlgorithmType_4 = value;
	}

	inline static int32_t get_offset_of_exchangeAlgorithmType_5() { return static_cast<int32_t>(offsetof(CipherSuite_t203D2B4A797805B775BEFB3B623962E7B407B6F7, ___exchangeAlgorithmType_5)); }
	inline int32_t get_exchangeAlgorithmType_5() const { return ___exchangeAlgorithmType_5; }
	inline int32_t* get_address_of_exchangeAlgorithmType_5() { return &___exchangeAlgorithmType_5; }
	inline void set_exchangeAlgorithmType_5(int32_t value)
	{
		___exchangeAlgorithmType_5 = value;
	}

	inline static int32_t get_offset_of_isExportable_6() { return static_cast<int32_t>(offsetof(CipherSuite_t203D2B4A797805B775BEFB3B623962E7B407B6F7, ___isExportable_6)); }
	inline bool get_isExportable_6() const { return ___isExportable_6; }
	inline bool* get_address_of_isExportable_6() { return &___isExportable_6; }
	inline void set_isExportable_6(bool value)
	{
		___isExportable_6 = value;
	}

	inline static int32_t get_offset_of_cipherMode_7() { return static_cast<int32_t>(offsetof(CipherSuite_t203D2B4A797805B775BEFB3B623962E7B407B6F7, ___cipherMode_7)); }
	inline int32_t get_cipherMode_7() const { return ___cipherMode_7; }
	inline int32_t* get_address_of_cipherMode_7() { return &___cipherMode_7; }
	inline void set_cipherMode_7(int32_t value)
	{
		___cipherMode_7 = value;
	}

	inline static int32_t get_offset_of_keyMaterialSize_8() { return static_cast<int32_t>(offsetof(CipherSuite_t203D2B4A797805B775BEFB3B623962E7B407B6F7, ___keyMaterialSize_8)); }
	inline uint8_t get_keyMaterialSize_8() const { return ___keyMaterialSize_8; }
	inline uint8_t* get_address_of_keyMaterialSize_8() { return &___keyMaterialSize_8; }
	inline void set_keyMaterialSize_8(uint8_t value)
	{
		___keyMaterialSize_8 = value;
	}

	inline static int32_t get_offset_of_keyBlockSize_9() { return static_cast<int32_t>(offsetof(CipherSuite_t203D2B4A797805B775BEFB3B623962E7B407B6F7, ___keyBlockSize_9)); }
	inline int32_t get_keyBlockSize_9() const { return ___keyBlockSize_9; }
	inline int32_t* get_address_of_keyBlockSize_9() { return &___keyBlockSize_9; }
	inline void set_keyBlockSize_9(int32_t value)
	{
		___keyBlockSize_9 = value;
	}

	inline static int32_t get_offset_of_expandedKeyMaterialSize_10() { return static_cast<int32_t>(offsetof(CipherSuite_t203D2B4A797805B775BEFB3B623962E7B407B6F7, ___expandedKeyMaterialSize_10)); }
	inline uint8_t get_expandedKeyMaterialSize_10() const { return ___expandedKeyMaterialSize_10; }
	inline uint8_t* get_address_of_expandedKeyMaterialSize_10() { return &___expandedKeyMaterialSize_10; }
	inline void set_expandedKeyMaterialSize_10(uint8_t value)
	{
		___expandedKeyMaterialSize_10 = value;
	}

	inline static int32_t get_offset_of_effectiveKeyBits_11() { return static_cast<int32_t>(offsetof(CipherSuite_t203D2B4A797805B775BEFB3B623962E7B407B6F7, ___effectiveKeyBits_11)); }
	inline int16_t get_effectiveKeyBits_11() const { return ___effectiveKeyBits_11; }
	inline int16_t* get_address_of_effectiveKeyBits_11() { return &___effectiveKeyBits_11; }
	inline void set_effectiveKeyBits_11(int16_t value)
	{
		___effectiveKeyBits_11 = value;
	}

	inline static int32_t get_offset_of_ivSize_12() { return static_cast<int32_t>(offsetof(CipherSuite_t203D2B4A797805B775BEFB3B623962E7B407B6F7, ___ivSize_12)); }
	inline uint8_t get_ivSize_12() const { return ___ivSize_12; }
	inline uint8_t* get_address_of_ivSize_12() { return &___ivSize_12; }
	inline void set_ivSize_12(uint8_t value)
	{
		___ivSize_12 = value;
	}

	inline static int32_t get_offset_of_blockSize_13() { return static_cast<int32_t>(offsetof(CipherSuite_t203D2B4A797805B775BEFB3B623962E7B407B6F7, ___blockSize_13)); }
	inline uint8_t get_blockSize_13() const { return ___blockSize_13; }
	inline uint8_t* get_address_of_blockSize_13() { return &___blockSize_13; }
	inline void set_blockSize_13(uint8_t value)
	{
		___blockSize_13 = value;
	}

	inline static int32_t get_offset_of_context_14() { return static_cast<int32_t>(offsetof(CipherSuite_t203D2B4A797805B775BEFB3B623962E7B407B6F7, ___context_14)); }
	inline Context_t17219BB52552BCACB15DAF9B6618835642F7F56F * get_context_14() const { return ___context_14; }
	inline Context_t17219BB52552BCACB15DAF9B6618835642F7F56F ** get_address_of_context_14() { return &___context_14; }
	inline void set_context_14(Context_t17219BB52552BCACB15DAF9B6618835642F7F56F * value)
	{
		___context_14 = value;
		Il2CppCodeGenWriteBarrier((&___context_14), value);
	}

	inline static int32_t get_offset_of_encryptionAlgorithm_15() { return static_cast<int32_t>(offsetof(CipherSuite_t203D2B4A797805B775BEFB3B623962E7B407B6F7, ___encryptionAlgorithm_15)); }
	inline SymmetricAlgorithm_t0A2EC7E7AD8B8976832B4F0AC432B691F862E789 * get_encryptionAlgorithm_15() const { return ___encryptionAlgorithm_15; }
	inline SymmetricAlgorithm_t0A2EC7E7AD8B8976832B4F0AC432B691F862E789 ** get_address_of_encryptionAlgorithm_15() { return &___encryptionAlgorithm_15; }
	inline void set_encryptionAlgorithm_15(SymmetricAlgorithm_t0A2EC7E7AD8B8976832B4F0AC432B691F862E789 * value)
	{
		___encryptionAlgorithm_15 = value;
		Il2CppCodeGenWriteBarrier((&___encryptionAlgorithm_15), value);
	}

	inline static int32_t get_offset_of_encryptionCipher_16() { return static_cast<int32_t>(offsetof(CipherSuite_t203D2B4A797805B775BEFB3B623962E7B407B6F7, ___encryptionCipher_16)); }
	inline RuntimeObject* get_encryptionCipher_16() const { return ___encryptionCipher_16; }
	inline RuntimeObject** get_address_of_encryptionCipher_16() { return &___encryptionCipher_16; }
	inline void set_encryptionCipher_16(RuntimeObject* value)
	{
		___encryptionCipher_16 = value;
		Il2CppCodeGenWriteBarrier((&___encryptionCipher_16), value);
	}

	inline static int32_t get_offset_of_decryptionAlgorithm_17() { return static_cast<int32_t>(offsetof(CipherSuite_t203D2B4A797805B775BEFB3B623962E7B407B6F7, ___decryptionAlgorithm_17)); }
	inline SymmetricAlgorithm_t0A2EC7E7AD8B8976832B4F0AC432B691F862E789 * get_decryptionAlgorithm_17() const { return ___decryptionAlgorithm_17; }
	inline SymmetricAlgorithm_t0A2EC7E7AD8B8976832B4F0AC432B691F862E789 ** get_address_of_decryptionAlgorithm_17() { return &___decryptionAlgorithm_17; }
	inline void set_decryptionAlgorithm_17(SymmetricAlgorithm_t0A2EC7E7AD8B8976832B4F0AC432B691F862E789 * value)
	{
		___decryptionAlgorithm_17 = value;
		Il2CppCodeGenWriteBarrier((&___decryptionAlgorithm_17), value);
	}

	inline static int32_t get_offset_of_decryptionCipher_18() { return static_cast<int32_t>(offsetof(CipherSuite_t203D2B4A797805B775BEFB3B623962E7B407B6F7, ___decryptionCipher_18)); }
	inline RuntimeObject* get_decryptionCipher_18() const { return ___decryptionCipher_18; }
	inline RuntimeObject** get_address_of_decryptionCipher_18() { return &___decryptionCipher_18; }
	inline void set_decryptionCipher_18(RuntimeObject* value)
	{
		___decryptionCipher_18 = value;
		Il2CppCodeGenWriteBarrier((&___decryptionCipher_18), value);
	}

	inline static int32_t get_offset_of_clientHMAC_19() { return static_cast<int32_t>(offsetof(CipherSuite_t203D2B4A797805B775BEFB3B623962E7B407B6F7, ___clientHMAC_19)); }
	inline KeyedHashAlgorithm_t83CFA2CA5A4F0F39B747E61D013CB5EB919D218B * get_clientHMAC_19() const { return ___clientHMAC_19; }
	inline KeyedHashAlgorithm_t83CFA2CA5A4F0F39B747E61D013CB5EB919D218B ** get_address_of_clientHMAC_19() { return &___clientHMAC_19; }
	inline void set_clientHMAC_19(KeyedHashAlgorithm_t83CFA2CA5A4F0F39B747E61D013CB5EB919D218B * value)
	{
		___clientHMAC_19 = value;
		Il2CppCodeGenWriteBarrier((&___clientHMAC_19), value);
	}

	inline static int32_t get_offset_of_serverHMAC_20() { return static_cast<int32_t>(offsetof(CipherSuite_t203D2B4A797805B775BEFB3B623962E7B407B6F7, ___serverHMAC_20)); }
	inline KeyedHashAlgorithm_t83CFA2CA5A4F0F39B747E61D013CB5EB919D218B * get_serverHMAC_20() const { return ___serverHMAC_20; }
	inline KeyedHashAlgorithm_t83CFA2CA5A4F0F39B747E61D013CB5EB919D218B ** get_address_of_serverHMAC_20() { return &___serverHMAC_20; }
	inline void set_serverHMAC_20(KeyedHashAlgorithm_t83CFA2CA5A4F0F39B747E61D013CB5EB919D218B * value)
	{
		___serverHMAC_20 = value;
		Il2CppCodeGenWriteBarrier((&___serverHMAC_20), value);
	}
};

struct CipherSuite_t203D2B4A797805B775BEFB3B623962E7B407B6F7_StaticFields
{
public:
	// System.Byte[] Mono.Security.Protocol.Tls.CipherSuite::EmptyArray
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___EmptyArray_0;

public:
	inline static int32_t get_offset_of_EmptyArray_0() { return static_cast<int32_t>(offsetof(CipherSuite_t203D2B4A797805B775BEFB3B623962E7B407B6F7_StaticFields, ___EmptyArray_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_EmptyArray_0() const { return ___EmptyArray_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_EmptyArray_0() { return &___EmptyArray_0; }
	inline void set_EmptyArray_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___EmptyArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CIPHERSUITE_T203D2B4A797805B775BEFB3B623962E7B407B6F7_H
#ifndef CONTEXT_T17219BB52552BCACB15DAF9B6618835642F7F56F_H
#define CONTEXT_T17219BB52552BCACB15DAF9B6618835642F7F56F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.Context
struct  Context_t17219BB52552BCACB15DAF9B6618835642F7F56F  : public RuntimeObject
{
public:
	// Mono.Security.Protocol.Tls.SecurityProtocolType Mono.Security.Protocol.Tls.Context::securityProtocol
	int32_t ___securityProtocol_4;
	// System.Byte[] Mono.Security.Protocol.Tls.Context::sessionId
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___sessionId_5;
	// Mono.Security.Protocol.Tls.SecurityCompressionType Mono.Security.Protocol.Tls.Context::compressionMethod
	int32_t ___compressionMethod_6;
	// Mono.Security.Protocol.Tls.TlsServerSettings Mono.Security.Protocol.Tls.Context::serverSettings
	TlsServerSettings_tD19093EA2CFCCD92F9B0FE63A9B2FAA9B4B45788 * ___serverSettings_7;
	// Mono.Security.Protocol.Tls.TlsClientSettings Mono.Security.Protocol.Tls.Context::clientSettings
	TlsClientSettings_t7521F1D5285E4B38591EC5E724E198CED35B39E0 * ___clientSettings_8;
	// Mono.Security.Protocol.Tls.SecurityParameters Mono.Security.Protocol.Tls.Context::current
	SecurityParameters_tC07B6E750AFC2037A7EBC754BD4DC1D37F41585E * ___current_9;
	// Mono.Security.Protocol.Tls.SecurityParameters Mono.Security.Protocol.Tls.Context::negotiating
	SecurityParameters_tC07B6E750AFC2037A7EBC754BD4DC1D37F41585E * ___negotiating_10;
	// Mono.Security.Protocol.Tls.SecurityParameters Mono.Security.Protocol.Tls.Context::read
	SecurityParameters_tC07B6E750AFC2037A7EBC754BD4DC1D37F41585E * ___read_11;
	// Mono.Security.Protocol.Tls.SecurityParameters Mono.Security.Protocol.Tls.Context::write
	SecurityParameters_tC07B6E750AFC2037A7EBC754BD4DC1D37F41585E * ___write_12;
	// Mono.Security.Protocol.Tls.CipherSuiteCollection Mono.Security.Protocol.Tls.Context::supportedCiphers
	CipherSuiteCollection_tBC06EE1AF9F0FF543D1FF7740D09685ACAAFC41E * ___supportedCiphers_13;
	// Mono.Security.Protocol.Tls.Handshake.HandshakeType Mono.Security.Protocol.Tls.Context::lastHandshakeMsg
	uint8_t ___lastHandshakeMsg_14;
	// Mono.Security.Protocol.Tls.HandshakeState Mono.Security.Protocol.Tls.Context::handshakeState
	int32_t ___handshakeState_15;
	// System.Boolean Mono.Security.Protocol.Tls.Context::abbreviatedHandshake
	bool ___abbreviatedHandshake_16;
	// System.Boolean Mono.Security.Protocol.Tls.Context::receivedConnectionEnd
	bool ___receivedConnectionEnd_17;
	// System.Boolean Mono.Security.Protocol.Tls.Context::sentConnectionEnd
	bool ___sentConnectionEnd_18;
	// System.Boolean Mono.Security.Protocol.Tls.Context::protocolNegotiated
	bool ___protocolNegotiated_19;
	// System.UInt64 Mono.Security.Protocol.Tls.Context::writeSequenceNumber
	uint64_t ___writeSequenceNumber_20;
	// System.UInt64 Mono.Security.Protocol.Tls.Context::readSequenceNumber
	uint64_t ___readSequenceNumber_21;
	// System.Byte[] Mono.Security.Protocol.Tls.Context::clientRandom
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___clientRandom_22;
	// System.Byte[] Mono.Security.Protocol.Tls.Context::serverRandom
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___serverRandom_23;
	// System.Byte[] Mono.Security.Protocol.Tls.Context::randomCS
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___randomCS_24;
	// System.Byte[] Mono.Security.Protocol.Tls.Context::randomSC
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___randomSC_25;
	// System.Byte[] Mono.Security.Protocol.Tls.Context::masterSecret
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___masterSecret_26;
	// System.Byte[] Mono.Security.Protocol.Tls.Context::clientWriteKey
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___clientWriteKey_27;
	// System.Byte[] Mono.Security.Protocol.Tls.Context::serverWriteKey
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___serverWriteKey_28;
	// System.Byte[] Mono.Security.Protocol.Tls.Context::clientWriteIV
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___clientWriteIV_29;
	// System.Byte[] Mono.Security.Protocol.Tls.Context::serverWriteIV
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___serverWriteIV_30;
	// Mono.Security.Protocol.Tls.TlsStream Mono.Security.Protocol.Tls.Context::handshakeMessages
	TlsStream_tB739E33AAABEC563984AD1EA75D7F4CFB4BF27E5 * ___handshakeMessages_31;
	// System.Security.Cryptography.RandomNumberGenerator Mono.Security.Protocol.Tls.Context::random
	RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * ___random_32;
	// Mono.Security.Protocol.Tls.RecordProtocol Mono.Security.Protocol.Tls.Context::recordProtocol
	RecordProtocol_tFA651C8422AECEEE03AABEDB029EC83EB8833CFA * ___recordProtocol_33;
	// System.Boolean Mono.Security.Protocol.Tls.Context::<ChangeCipherSpecDone>k__BackingField
	bool ___U3CChangeCipherSpecDoneU3Ek__BackingField_34;

public:
	inline static int32_t get_offset_of_securityProtocol_4() { return static_cast<int32_t>(offsetof(Context_t17219BB52552BCACB15DAF9B6618835642F7F56F, ___securityProtocol_4)); }
	inline int32_t get_securityProtocol_4() const { return ___securityProtocol_4; }
	inline int32_t* get_address_of_securityProtocol_4() { return &___securityProtocol_4; }
	inline void set_securityProtocol_4(int32_t value)
	{
		___securityProtocol_4 = value;
	}

	inline static int32_t get_offset_of_sessionId_5() { return static_cast<int32_t>(offsetof(Context_t17219BB52552BCACB15DAF9B6618835642F7F56F, ___sessionId_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_sessionId_5() const { return ___sessionId_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_sessionId_5() { return &___sessionId_5; }
	inline void set_sessionId_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___sessionId_5 = value;
		Il2CppCodeGenWriteBarrier((&___sessionId_5), value);
	}

	inline static int32_t get_offset_of_compressionMethod_6() { return static_cast<int32_t>(offsetof(Context_t17219BB52552BCACB15DAF9B6618835642F7F56F, ___compressionMethod_6)); }
	inline int32_t get_compressionMethod_6() const { return ___compressionMethod_6; }
	inline int32_t* get_address_of_compressionMethod_6() { return &___compressionMethod_6; }
	inline void set_compressionMethod_6(int32_t value)
	{
		___compressionMethod_6 = value;
	}

	inline static int32_t get_offset_of_serverSettings_7() { return static_cast<int32_t>(offsetof(Context_t17219BB52552BCACB15DAF9B6618835642F7F56F, ___serverSettings_7)); }
	inline TlsServerSettings_tD19093EA2CFCCD92F9B0FE63A9B2FAA9B4B45788 * get_serverSettings_7() const { return ___serverSettings_7; }
	inline TlsServerSettings_tD19093EA2CFCCD92F9B0FE63A9B2FAA9B4B45788 ** get_address_of_serverSettings_7() { return &___serverSettings_7; }
	inline void set_serverSettings_7(TlsServerSettings_tD19093EA2CFCCD92F9B0FE63A9B2FAA9B4B45788 * value)
	{
		___serverSettings_7 = value;
		Il2CppCodeGenWriteBarrier((&___serverSettings_7), value);
	}

	inline static int32_t get_offset_of_clientSettings_8() { return static_cast<int32_t>(offsetof(Context_t17219BB52552BCACB15DAF9B6618835642F7F56F, ___clientSettings_8)); }
	inline TlsClientSettings_t7521F1D5285E4B38591EC5E724E198CED35B39E0 * get_clientSettings_8() const { return ___clientSettings_8; }
	inline TlsClientSettings_t7521F1D5285E4B38591EC5E724E198CED35B39E0 ** get_address_of_clientSettings_8() { return &___clientSettings_8; }
	inline void set_clientSettings_8(TlsClientSettings_t7521F1D5285E4B38591EC5E724E198CED35B39E0 * value)
	{
		___clientSettings_8 = value;
		Il2CppCodeGenWriteBarrier((&___clientSettings_8), value);
	}

	inline static int32_t get_offset_of_current_9() { return static_cast<int32_t>(offsetof(Context_t17219BB52552BCACB15DAF9B6618835642F7F56F, ___current_9)); }
	inline SecurityParameters_tC07B6E750AFC2037A7EBC754BD4DC1D37F41585E * get_current_9() const { return ___current_9; }
	inline SecurityParameters_tC07B6E750AFC2037A7EBC754BD4DC1D37F41585E ** get_address_of_current_9() { return &___current_9; }
	inline void set_current_9(SecurityParameters_tC07B6E750AFC2037A7EBC754BD4DC1D37F41585E * value)
	{
		___current_9 = value;
		Il2CppCodeGenWriteBarrier((&___current_9), value);
	}

	inline static int32_t get_offset_of_negotiating_10() { return static_cast<int32_t>(offsetof(Context_t17219BB52552BCACB15DAF9B6618835642F7F56F, ___negotiating_10)); }
	inline SecurityParameters_tC07B6E750AFC2037A7EBC754BD4DC1D37F41585E * get_negotiating_10() const { return ___negotiating_10; }
	inline SecurityParameters_tC07B6E750AFC2037A7EBC754BD4DC1D37F41585E ** get_address_of_negotiating_10() { return &___negotiating_10; }
	inline void set_negotiating_10(SecurityParameters_tC07B6E750AFC2037A7EBC754BD4DC1D37F41585E * value)
	{
		___negotiating_10 = value;
		Il2CppCodeGenWriteBarrier((&___negotiating_10), value);
	}

	inline static int32_t get_offset_of_read_11() { return static_cast<int32_t>(offsetof(Context_t17219BB52552BCACB15DAF9B6618835642F7F56F, ___read_11)); }
	inline SecurityParameters_tC07B6E750AFC2037A7EBC754BD4DC1D37F41585E * get_read_11() const { return ___read_11; }
	inline SecurityParameters_tC07B6E750AFC2037A7EBC754BD4DC1D37F41585E ** get_address_of_read_11() { return &___read_11; }
	inline void set_read_11(SecurityParameters_tC07B6E750AFC2037A7EBC754BD4DC1D37F41585E * value)
	{
		___read_11 = value;
		Il2CppCodeGenWriteBarrier((&___read_11), value);
	}

	inline static int32_t get_offset_of_write_12() { return static_cast<int32_t>(offsetof(Context_t17219BB52552BCACB15DAF9B6618835642F7F56F, ___write_12)); }
	inline SecurityParameters_tC07B6E750AFC2037A7EBC754BD4DC1D37F41585E * get_write_12() const { return ___write_12; }
	inline SecurityParameters_tC07B6E750AFC2037A7EBC754BD4DC1D37F41585E ** get_address_of_write_12() { return &___write_12; }
	inline void set_write_12(SecurityParameters_tC07B6E750AFC2037A7EBC754BD4DC1D37F41585E * value)
	{
		___write_12 = value;
		Il2CppCodeGenWriteBarrier((&___write_12), value);
	}

	inline static int32_t get_offset_of_supportedCiphers_13() { return static_cast<int32_t>(offsetof(Context_t17219BB52552BCACB15DAF9B6618835642F7F56F, ___supportedCiphers_13)); }
	inline CipherSuiteCollection_tBC06EE1AF9F0FF543D1FF7740D09685ACAAFC41E * get_supportedCiphers_13() const { return ___supportedCiphers_13; }
	inline CipherSuiteCollection_tBC06EE1AF9F0FF543D1FF7740D09685ACAAFC41E ** get_address_of_supportedCiphers_13() { return &___supportedCiphers_13; }
	inline void set_supportedCiphers_13(CipherSuiteCollection_tBC06EE1AF9F0FF543D1FF7740D09685ACAAFC41E * value)
	{
		___supportedCiphers_13 = value;
		Il2CppCodeGenWriteBarrier((&___supportedCiphers_13), value);
	}

	inline static int32_t get_offset_of_lastHandshakeMsg_14() { return static_cast<int32_t>(offsetof(Context_t17219BB52552BCACB15DAF9B6618835642F7F56F, ___lastHandshakeMsg_14)); }
	inline uint8_t get_lastHandshakeMsg_14() const { return ___lastHandshakeMsg_14; }
	inline uint8_t* get_address_of_lastHandshakeMsg_14() { return &___lastHandshakeMsg_14; }
	inline void set_lastHandshakeMsg_14(uint8_t value)
	{
		___lastHandshakeMsg_14 = value;
	}

	inline static int32_t get_offset_of_handshakeState_15() { return static_cast<int32_t>(offsetof(Context_t17219BB52552BCACB15DAF9B6618835642F7F56F, ___handshakeState_15)); }
	inline int32_t get_handshakeState_15() const { return ___handshakeState_15; }
	inline int32_t* get_address_of_handshakeState_15() { return &___handshakeState_15; }
	inline void set_handshakeState_15(int32_t value)
	{
		___handshakeState_15 = value;
	}

	inline static int32_t get_offset_of_abbreviatedHandshake_16() { return static_cast<int32_t>(offsetof(Context_t17219BB52552BCACB15DAF9B6618835642F7F56F, ___abbreviatedHandshake_16)); }
	inline bool get_abbreviatedHandshake_16() const { return ___abbreviatedHandshake_16; }
	inline bool* get_address_of_abbreviatedHandshake_16() { return &___abbreviatedHandshake_16; }
	inline void set_abbreviatedHandshake_16(bool value)
	{
		___abbreviatedHandshake_16 = value;
	}

	inline static int32_t get_offset_of_receivedConnectionEnd_17() { return static_cast<int32_t>(offsetof(Context_t17219BB52552BCACB15DAF9B6618835642F7F56F, ___receivedConnectionEnd_17)); }
	inline bool get_receivedConnectionEnd_17() const { return ___receivedConnectionEnd_17; }
	inline bool* get_address_of_receivedConnectionEnd_17() { return &___receivedConnectionEnd_17; }
	inline void set_receivedConnectionEnd_17(bool value)
	{
		___receivedConnectionEnd_17 = value;
	}

	inline static int32_t get_offset_of_sentConnectionEnd_18() { return static_cast<int32_t>(offsetof(Context_t17219BB52552BCACB15DAF9B6618835642F7F56F, ___sentConnectionEnd_18)); }
	inline bool get_sentConnectionEnd_18() const { return ___sentConnectionEnd_18; }
	inline bool* get_address_of_sentConnectionEnd_18() { return &___sentConnectionEnd_18; }
	inline void set_sentConnectionEnd_18(bool value)
	{
		___sentConnectionEnd_18 = value;
	}

	inline static int32_t get_offset_of_protocolNegotiated_19() { return static_cast<int32_t>(offsetof(Context_t17219BB52552BCACB15DAF9B6618835642F7F56F, ___protocolNegotiated_19)); }
	inline bool get_protocolNegotiated_19() const { return ___protocolNegotiated_19; }
	inline bool* get_address_of_protocolNegotiated_19() { return &___protocolNegotiated_19; }
	inline void set_protocolNegotiated_19(bool value)
	{
		___protocolNegotiated_19 = value;
	}

	inline static int32_t get_offset_of_writeSequenceNumber_20() { return static_cast<int32_t>(offsetof(Context_t17219BB52552BCACB15DAF9B6618835642F7F56F, ___writeSequenceNumber_20)); }
	inline uint64_t get_writeSequenceNumber_20() const { return ___writeSequenceNumber_20; }
	inline uint64_t* get_address_of_writeSequenceNumber_20() { return &___writeSequenceNumber_20; }
	inline void set_writeSequenceNumber_20(uint64_t value)
	{
		___writeSequenceNumber_20 = value;
	}

	inline static int32_t get_offset_of_readSequenceNumber_21() { return static_cast<int32_t>(offsetof(Context_t17219BB52552BCACB15DAF9B6618835642F7F56F, ___readSequenceNumber_21)); }
	inline uint64_t get_readSequenceNumber_21() const { return ___readSequenceNumber_21; }
	inline uint64_t* get_address_of_readSequenceNumber_21() { return &___readSequenceNumber_21; }
	inline void set_readSequenceNumber_21(uint64_t value)
	{
		___readSequenceNumber_21 = value;
	}

	inline static int32_t get_offset_of_clientRandom_22() { return static_cast<int32_t>(offsetof(Context_t17219BB52552BCACB15DAF9B6618835642F7F56F, ___clientRandom_22)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_clientRandom_22() const { return ___clientRandom_22; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_clientRandom_22() { return &___clientRandom_22; }
	inline void set_clientRandom_22(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___clientRandom_22 = value;
		Il2CppCodeGenWriteBarrier((&___clientRandom_22), value);
	}

	inline static int32_t get_offset_of_serverRandom_23() { return static_cast<int32_t>(offsetof(Context_t17219BB52552BCACB15DAF9B6618835642F7F56F, ___serverRandom_23)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_serverRandom_23() const { return ___serverRandom_23; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_serverRandom_23() { return &___serverRandom_23; }
	inline void set_serverRandom_23(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___serverRandom_23 = value;
		Il2CppCodeGenWriteBarrier((&___serverRandom_23), value);
	}

	inline static int32_t get_offset_of_randomCS_24() { return static_cast<int32_t>(offsetof(Context_t17219BB52552BCACB15DAF9B6618835642F7F56F, ___randomCS_24)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_randomCS_24() const { return ___randomCS_24; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_randomCS_24() { return &___randomCS_24; }
	inline void set_randomCS_24(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___randomCS_24 = value;
		Il2CppCodeGenWriteBarrier((&___randomCS_24), value);
	}

	inline static int32_t get_offset_of_randomSC_25() { return static_cast<int32_t>(offsetof(Context_t17219BB52552BCACB15DAF9B6618835642F7F56F, ___randomSC_25)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_randomSC_25() const { return ___randomSC_25; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_randomSC_25() { return &___randomSC_25; }
	inline void set_randomSC_25(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___randomSC_25 = value;
		Il2CppCodeGenWriteBarrier((&___randomSC_25), value);
	}

	inline static int32_t get_offset_of_masterSecret_26() { return static_cast<int32_t>(offsetof(Context_t17219BB52552BCACB15DAF9B6618835642F7F56F, ___masterSecret_26)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_masterSecret_26() const { return ___masterSecret_26; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_masterSecret_26() { return &___masterSecret_26; }
	inline void set_masterSecret_26(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___masterSecret_26 = value;
		Il2CppCodeGenWriteBarrier((&___masterSecret_26), value);
	}

	inline static int32_t get_offset_of_clientWriteKey_27() { return static_cast<int32_t>(offsetof(Context_t17219BB52552BCACB15DAF9B6618835642F7F56F, ___clientWriteKey_27)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_clientWriteKey_27() const { return ___clientWriteKey_27; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_clientWriteKey_27() { return &___clientWriteKey_27; }
	inline void set_clientWriteKey_27(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___clientWriteKey_27 = value;
		Il2CppCodeGenWriteBarrier((&___clientWriteKey_27), value);
	}

	inline static int32_t get_offset_of_serverWriteKey_28() { return static_cast<int32_t>(offsetof(Context_t17219BB52552BCACB15DAF9B6618835642F7F56F, ___serverWriteKey_28)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_serverWriteKey_28() const { return ___serverWriteKey_28; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_serverWriteKey_28() { return &___serverWriteKey_28; }
	inline void set_serverWriteKey_28(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___serverWriteKey_28 = value;
		Il2CppCodeGenWriteBarrier((&___serverWriteKey_28), value);
	}

	inline static int32_t get_offset_of_clientWriteIV_29() { return static_cast<int32_t>(offsetof(Context_t17219BB52552BCACB15DAF9B6618835642F7F56F, ___clientWriteIV_29)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_clientWriteIV_29() const { return ___clientWriteIV_29; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_clientWriteIV_29() { return &___clientWriteIV_29; }
	inline void set_clientWriteIV_29(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___clientWriteIV_29 = value;
		Il2CppCodeGenWriteBarrier((&___clientWriteIV_29), value);
	}

	inline static int32_t get_offset_of_serverWriteIV_30() { return static_cast<int32_t>(offsetof(Context_t17219BB52552BCACB15DAF9B6618835642F7F56F, ___serverWriteIV_30)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_serverWriteIV_30() const { return ___serverWriteIV_30; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_serverWriteIV_30() { return &___serverWriteIV_30; }
	inline void set_serverWriteIV_30(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___serverWriteIV_30 = value;
		Il2CppCodeGenWriteBarrier((&___serverWriteIV_30), value);
	}

	inline static int32_t get_offset_of_handshakeMessages_31() { return static_cast<int32_t>(offsetof(Context_t17219BB52552BCACB15DAF9B6618835642F7F56F, ___handshakeMessages_31)); }
	inline TlsStream_tB739E33AAABEC563984AD1EA75D7F4CFB4BF27E5 * get_handshakeMessages_31() const { return ___handshakeMessages_31; }
	inline TlsStream_tB739E33AAABEC563984AD1EA75D7F4CFB4BF27E5 ** get_address_of_handshakeMessages_31() { return &___handshakeMessages_31; }
	inline void set_handshakeMessages_31(TlsStream_tB739E33AAABEC563984AD1EA75D7F4CFB4BF27E5 * value)
	{
		___handshakeMessages_31 = value;
		Il2CppCodeGenWriteBarrier((&___handshakeMessages_31), value);
	}

	inline static int32_t get_offset_of_random_32() { return static_cast<int32_t>(offsetof(Context_t17219BB52552BCACB15DAF9B6618835642F7F56F, ___random_32)); }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * get_random_32() const { return ___random_32; }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 ** get_address_of_random_32() { return &___random_32; }
	inline void set_random_32(RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * value)
	{
		___random_32 = value;
		Il2CppCodeGenWriteBarrier((&___random_32), value);
	}

	inline static int32_t get_offset_of_recordProtocol_33() { return static_cast<int32_t>(offsetof(Context_t17219BB52552BCACB15DAF9B6618835642F7F56F, ___recordProtocol_33)); }
	inline RecordProtocol_tFA651C8422AECEEE03AABEDB029EC83EB8833CFA * get_recordProtocol_33() const { return ___recordProtocol_33; }
	inline RecordProtocol_tFA651C8422AECEEE03AABEDB029EC83EB8833CFA ** get_address_of_recordProtocol_33() { return &___recordProtocol_33; }
	inline void set_recordProtocol_33(RecordProtocol_tFA651C8422AECEEE03AABEDB029EC83EB8833CFA * value)
	{
		___recordProtocol_33 = value;
		Il2CppCodeGenWriteBarrier((&___recordProtocol_33), value);
	}

	inline static int32_t get_offset_of_U3CChangeCipherSpecDoneU3Ek__BackingField_34() { return static_cast<int32_t>(offsetof(Context_t17219BB52552BCACB15DAF9B6618835642F7F56F, ___U3CChangeCipherSpecDoneU3Ek__BackingField_34)); }
	inline bool get_U3CChangeCipherSpecDoneU3Ek__BackingField_34() const { return ___U3CChangeCipherSpecDoneU3Ek__BackingField_34; }
	inline bool* get_address_of_U3CChangeCipherSpecDoneU3Ek__BackingField_34() { return &___U3CChangeCipherSpecDoneU3Ek__BackingField_34; }
	inline void set_U3CChangeCipherSpecDoneU3Ek__BackingField_34(bool value)
	{
		___U3CChangeCipherSpecDoneU3Ek__BackingField_34 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTEXT_T17219BB52552BCACB15DAF9B6618835642F7F56F_H
#ifndef HANDSHAKEMESSAGE_T9B97FBAE0BAFCEA052F2B5EA880817CA1345298C_H
#define HANDSHAKEMESSAGE_T9B97FBAE0BAFCEA052F2B5EA880817CA1345298C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.Handshake.HandshakeMessage
struct  HandshakeMessage_t9B97FBAE0BAFCEA052F2B5EA880817CA1345298C  : public TlsStream_tB739E33AAABEC563984AD1EA75D7F4CFB4BF27E5
{
public:
	// Mono.Security.Protocol.Tls.Context Mono.Security.Protocol.Tls.Handshake.HandshakeMessage::context
	Context_t17219BB52552BCACB15DAF9B6618835642F7F56F * ___context_10;
	// Mono.Security.Protocol.Tls.Handshake.HandshakeType Mono.Security.Protocol.Tls.Handshake.HandshakeMessage::handshakeType
	uint8_t ___handshakeType_11;
	// Mono.Security.Protocol.Tls.ContentType Mono.Security.Protocol.Tls.Handshake.HandshakeMessage::contentType
	uint8_t ___contentType_12;
	// System.Byte[] Mono.Security.Protocol.Tls.Handshake.HandshakeMessage::cache
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___cache_13;

public:
	inline static int32_t get_offset_of_context_10() { return static_cast<int32_t>(offsetof(HandshakeMessage_t9B97FBAE0BAFCEA052F2B5EA880817CA1345298C, ___context_10)); }
	inline Context_t17219BB52552BCACB15DAF9B6618835642F7F56F * get_context_10() const { return ___context_10; }
	inline Context_t17219BB52552BCACB15DAF9B6618835642F7F56F ** get_address_of_context_10() { return &___context_10; }
	inline void set_context_10(Context_t17219BB52552BCACB15DAF9B6618835642F7F56F * value)
	{
		___context_10 = value;
		Il2CppCodeGenWriteBarrier((&___context_10), value);
	}

	inline static int32_t get_offset_of_handshakeType_11() { return static_cast<int32_t>(offsetof(HandshakeMessage_t9B97FBAE0BAFCEA052F2B5EA880817CA1345298C, ___handshakeType_11)); }
	inline uint8_t get_handshakeType_11() const { return ___handshakeType_11; }
	inline uint8_t* get_address_of_handshakeType_11() { return &___handshakeType_11; }
	inline void set_handshakeType_11(uint8_t value)
	{
		___handshakeType_11 = value;
	}

	inline static int32_t get_offset_of_contentType_12() { return static_cast<int32_t>(offsetof(HandshakeMessage_t9B97FBAE0BAFCEA052F2B5EA880817CA1345298C, ___contentType_12)); }
	inline uint8_t get_contentType_12() const { return ___contentType_12; }
	inline uint8_t* get_address_of_contentType_12() { return &___contentType_12; }
	inline void set_contentType_12(uint8_t value)
	{
		___contentType_12 = value;
	}

	inline static int32_t get_offset_of_cache_13() { return static_cast<int32_t>(offsetof(HandshakeMessage_t9B97FBAE0BAFCEA052F2B5EA880817CA1345298C, ___cache_13)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_cache_13() const { return ___cache_13; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_cache_13() { return &___cache_13; }
	inline void set_cache_13(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___cache_13 = value;
		Il2CppCodeGenWriteBarrier((&___cache_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HANDSHAKEMESSAGE_T9B97FBAE0BAFCEA052F2B5EA880817CA1345298C_H
#ifndef SSLCLIENTSTREAM_T58595BA5B794C492855BB93B01C5046E1EE7263B_H
#define SSLCLIENTSTREAM_T58595BA5B794C492855BB93B01C5046E1EE7263B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.SslClientStream
struct  SslClientStream_t58595BA5B794C492855BB93B01C5046E1EE7263B  : public SslStreamBase_tE9E16CABCE59BAA56CFFBD9B84C28335730E9197
{
public:
	// Mono.Security.Protocol.Tls.CertificateValidationCallback Mono.Security.Protocol.Tls.SslClientStream::ServerCertValidation
	CertificateValidationCallback_t4FDC57D3B9528BD8D0564B4B3B2CF135E9CD72D3 * ___ServerCertValidation_19;
	// Mono.Security.Protocol.Tls.CertificateSelectionCallback Mono.Security.Protocol.Tls.SslClientStream::ClientCertSelection
	CertificateSelectionCallback_t2D981EF7A74CC271A9F086BB0F483FD6BCEDF5BE * ___ClientCertSelection_20;
	// Mono.Security.Protocol.Tls.PrivateKeySelectionCallback Mono.Security.Protocol.Tls.SslClientStream::PrivateKeySelection
	PrivateKeySelectionCallback_t403FB7EE7A49085BEDF77A79C77652CB5C610F7C * ___PrivateKeySelection_21;
	// Mono.Security.Protocol.Tls.CertificateValidationCallback2 Mono.Security.Protocol.Tls.SslClientStream::ServerCertValidation2
	CertificateValidationCallback2_t80B8CBB56C7605C91427868F9DE72B29837CCD61 * ___ServerCertValidation2_22;

public:
	inline static int32_t get_offset_of_ServerCertValidation_19() { return static_cast<int32_t>(offsetof(SslClientStream_t58595BA5B794C492855BB93B01C5046E1EE7263B, ___ServerCertValidation_19)); }
	inline CertificateValidationCallback_t4FDC57D3B9528BD8D0564B4B3B2CF135E9CD72D3 * get_ServerCertValidation_19() const { return ___ServerCertValidation_19; }
	inline CertificateValidationCallback_t4FDC57D3B9528BD8D0564B4B3B2CF135E9CD72D3 ** get_address_of_ServerCertValidation_19() { return &___ServerCertValidation_19; }
	inline void set_ServerCertValidation_19(CertificateValidationCallback_t4FDC57D3B9528BD8D0564B4B3B2CF135E9CD72D3 * value)
	{
		___ServerCertValidation_19 = value;
		Il2CppCodeGenWriteBarrier((&___ServerCertValidation_19), value);
	}

	inline static int32_t get_offset_of_ClientCertSelection_20() { return static_cast<int32_t>(offsetof(SslClientStream_t58595BA5B794C492855BB93B01C5046E1EE7263B, ___ClientCertSelection_20)); }
	inline CertificateSelectionCallback_t2D981EF7A74CC271A9F086BB0F483FD6BCEDF5BE * get_ClientCertSelection_20() const { return ___ClientCertSelection_20; }
	inline CertificateSelectionCallback_t2D981EF7A74CC271A9F086BB0F483FD6BCEDF5BE ** get_address_of_ClientCertSelection_20() { return &___ClientCertSelection_20; }
	inline void set_ClientCertSelection_20(CertificateSelectionCallback_t2D981EF7A74CC271A9F086BB0F483FD6BCEDF5BE * value)
	{
		___ClientCertSelection_20 = value;
		Il2CppCodeGenWriteBarrier((&___ClientCertSelection_20), value);
	}

	inline static int32_t get_offset_of_PrivateKeySelection_21() { return static_cast<int32_t>(offsetof(SslClientStream_t58595BA5B794C492855BB93B01C5046E1EE7263B, ___PrivateKeySelection_21)); }
	inline PrivateKeySelectionCallback_t403FB7EE7A49085BEDF77A79C77652CB5C610F7C * get_PrivateKeySelection_21() const { return ___PrivateKeySelection_21; }
	inline PrivateKeySelectionCallback_t403FB7EE7A49085BEDF77A79C77652CB5C610F7C ** get_address_of_PrivateKeySelection_21() { return &___PrivateKeySelection_21; }
	inline void set_PrivateKeySelection_21(PrivateKeySelectionCallback_t403FB7EE7A49085BEDF77A79C77652CB5C610F7C * value)
	{
		___PrivateKeySelection_21 = value;
		Il2CppCodeGenWriteBarrier((&___PrivateKeySelection_21), value);
	}

	inline static int32_t get_offset_of_ServerCertValidation2_22() { return static_cast<int32_t>(offsetof(SslClientStream_t58595BA5B794C492855BB93B01C5046E1EE7263B, ___ServerCertValidation2_22)); }
	inline CertificateValidationCallback2_t80B8CBB56C7605C91427868F9DE72B29837CCD61 * get_ServerCertValidation2_22() const { return ___ServerCertValidation2_22; }
	inline CertificateValidationCallback2_t80B8CBB56C7605C91427868F9DE72B29837CCD61 ** get_address_of_ServerCertValidation2_22() { return &___ServerCertValidation2_22; }
	inline void set_ServerCertValidation2_22(CertificateValidationCallback2_t80B8CBB56C7605C91427868F9DE72B29837CCD61 * value)
	{
		___ServerCertValidation2_22 = value;
		Il2CppCodeGenWriteBarrier((&___ServerCertValidation2_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SSLCLIENTSTREAM_T58595BA5B794C492855BB93B01C5046E1EE7263B_H
#ifndef NEGOTIATEASYNCRESULT_T7BACE93E41827A619157BB53BDC7596AA554F65E_H
#define NEGOTIATEASYNCRESULT_T7BACE93E41827A619157BB53BDC7596AA554F65E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.SslClientStream_NegotiateAsyncResult
struct  NegotiateAsyncResult_t7BACE93E41827A619157BB53BDC7596AA554F65E  : public RuntimeObject
{
public:
	// System.Object Mono.Security.Protocol.Tls.SslClientStream_NegotiateAsyncResult::locker
	RuntimeObject * ___locker_0;
	// System.AsyncCallback Mono.Security.Protocol.Tls.SslClientStream_NegotiateAsyncResult::_userCallback
	AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ____userCallback_1;
	// System.Object Mono.Security.Protocol.Tls.SslClientStream_NegotiateAsyncResult::_userState
	RuntimeObject * ____userState_2;
	// System.Exception Mono.Security.Protocol.Tls.SslClientStream_NegotiateAsyncResult::_asyncException
	Exception_t * ____asyncException_3;
	// System.Threading.ManualResetEvent Mono.Security.Protocol.Tls.SslClientStream_NegotiateAsyncResult::handle
	ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * ___handle_4;
	// Mono.Security.Protocol.Tls.SslClientStream_NegotiateState Mono.Security.Protocol.Tls.SslClientStream_NegotiateAsyncResult::_state
	int32_t ____state_5;
	// System.Boolean Mono.Security.Protocol.Tls.SslClientStream_NegotiateAsyncResult::completed
	bool ___completed_6;

public:
	inline static int32_t get_offset_of_locker_0() { return static_cast<int32_t>(offsetof(NegotiateAsyncResult_t7BACE93E41827A619157BB53BDC7596AA554F65E, ___locker_0)); }
	inline RuntimeObject * get_locker_0() const { return ___locker_0; }
	inline RuntimeObject ** get_address_of_locker_0() { return &___locker_0; }
	inline void set_locker_0(RuntimeObject * value)
	{
		___locker_0 = value;
		Il2CppCodeGenWriteBarrier((&___locker_0), value);
	}

	inline static int32_t get_offset_of__userCallback_1() { return static_cast<int32_t>(offsetof(NegotiateAsyncResult_t7BACE93E41827A619157BB53BDC7596AA554F65E, ____userCallback_1)); }
	inline AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * get__userCallback_1() const { return ____userCallback_1; }
	inline AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 ** get_address_of__userCallback_1() { return &____userCallback_1; }
	inline void set__userCallback_1(AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * value)
	{
		____userCallback_1 = value;
		Il2CppCodeGenWriteBarrier((&____userCallback_1), value);
	}

	inline static int32_t get_offset_of__userState_2() { return static_cast<int32_t>(offsetof(NegotiateAsyncResult_t7BACE93E41827A619157BB53BDC7596AA554F65E, ____userState_2)); }
	inline RuntimeObject * get__userState_2() const { return ____userState_2; }
	inline RuntimeObject ** get_address_of__userState_2() { return &____userState_2; }
	inline void set__userState_2(RuntimeObject * value)
	{
		____userState_2 = value;
		Il2CppCodeGenWriteBarrier((&____userState_2), value);
	}

	inline static int32_t get_offset_of__asyncException_3() { return static_cast<int32_t>(offsetof(NegotiateAsyncResult_t7BACE93E41827A619157BB53BDC7596AA554F65E, ____asyncException_3)); }
	inline Exception_t * get__asyncException_3() const { return ____asyncException_3; }
	inline Exception_t ** get_address_of__asyncException_3() { return &____asyncException_3; }
	inline void set__asyncException_3(Exception_t * value)
	{
		____asyncException_3 = value;
		Il2CppCodeGenWriteBarrier((&____asyncException_3), value);
	}

	inline static int32_t get_offset_of_handle_4() { return static_cast<int32_t>(offsetof(NegotiateAsyncResult_t7BACE93E41827A619157BB53BDC7596AA554F65E, ___handle_4)); }
	inline ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * get_handle_4() const { return ___handle_4; }
	inline ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 ** get_address_of_handle_4() { return &___handle_4; }
	inline void set_handle_4(ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * value)
	{
		___handle_4 = value;
		Il2CppCodeGenWriteBarrier((&___handle_4), value);
	}

	inline static int32_t get_offset_of__state_5() { return static_cast<int32_t>(offsetof(NegotiateAsyncResult_t7BACE93E41827A619157BB53BDC7596AA554F65E, ____state_5)); }
	inline int32_t get__state_5() const { return ____state_5; }
	inline int32_t* get_address_of__state_5() { return &____state_5; }
	inline void set__state_5(int32_t value)
	{
		____state_5 = value;
	}

	inline static int32_t get_offset_of_completed_6() { return static_cast<int32_t>(offsetof(NegotiateAsyncResult_t7BACE93E41827A619157BB53BDC7596AA554F65E, ___completed_6)); }
	inline bool get_completed_6() const { return ___completed_6; }
	inline bool* get_address_of_completed_6() { return &___completed_6; }
	inline void set_completed_6(bool value)
	{
		___completed_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NEGOTIATEASYNCRESULT_T7BACE93E41827A619157BB53BDC7596AA554F65E_H
#ifndef SSLSERVERSTREAM_T424CB4AC3D421FAF8A73A20520E3E4A4934E2092_H
#define SSLSERVERSTREAM_T424CB4AC3D421FAF8A73A20520E3E4A4934E2092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.SslServerStream
struct  SslServerStream_t424CB4AC3D421FAF8A73A20520E3E4A4934E2092  : public SslStreamBase_tE9E16CABCE59BAA56CFFBD9B84C28335730E9197
{
public:
	// Mono.Security.Protocol.Tls.CertificateValidationCallback Mono.Security.Protocol.Tls.SslServerStream::ClientCertValidation
	CertificateValidationCallback_t4FDC57D3B9528BD8D0564B4B3B2CF135E9CD72D3 * ___ClientCertValidation_19;
	// Mono.Security.Protocol.Tls.PrivateKeySelectionCallback Mono.Security.Protocol.Tls.SslServerStream::PrivateKeySelection
	PrivateKeySelectionCallback_t403FB7EE7A49085BEDF77A79C77652CB5C610F7C * ___PrivateKeySelection_20;
	// Mono.Security.Protocol.Tls.CertificateValidationCallback2 Mono.Security.Protocol.Tls.SslServerStream::ClientCertValidation2
	CertificateValidationCallback2_t80B8CBB56C7605C91427868F9DE72B29837CCD61 * ___ClientCertValidation2_21;

public:
	inline static int32_t get_offset_of_ClientCertValidation_19() { return static_cast<int32_t>(offsetof(SslServerStream_t424CB4AC3D421FAF8A73A20520E3E4A4934E2092, ___ClientCertValidation_19)); }
	inline CertificateValidationCallback_t4FDC57D3B9528BD8D0564B4B3B2CF135E9CD72D3 * get_ClientCertValidation_19() const { return ___ClientCertValidation_19; }
	inline CertificateValidationCallback_t4FDC57D3B9528BD8D0564B4B3B2CF135E9CD72D3 ** get_address_of_ClientCertValidation_19() { return &___ClientCertValidation_19; }
	inline void set_ClientCertValidation_19(CertificateValidationCallback_t4FDC57D3B9528BD8D0564B4B3B2CF135E9CD72D3 * value)
	{
		___ClientCertValidation_19 = value;
		Il2CppCodeGenWriteBarrier((&___ClientCertValidation_19), value);
	}

	inline static int32_t get_offset_of_PrivateKeySelection_20() { return static_cast<int32_t>(offsetof(SslServerStream_t424CB4AC3D421FAF8A73A20520E3E4A4934E2092, ___PrivateKeySelection_20)); }
	inline PrivateKeySelectionCallback_t403FB7EE7A49085BEDF77A79C77652CB5C610F7C * get_PrivateKeySelection_20() const { return ___PrivateKeySelection_20; }
	inline PrivateKeySelectionCallback_t403FB7EE7A49085BEDF77A79C77652CB5C610F7C ** get_address_of_PrivateKeySelection_20() { return &___PrivateKeySelection_20; }
	inline void set_PrivateKeySelection_20(PrivateKeySelectionCallback_t403FB7EE7A49085BEDF77A79C77652CB5C610F7C * value)
	{
		___PrivateKeySelection_20 = value;
		Il2CppCodeGenWriteBarrier((&___PrivateKeySelection_20), value);
	}

	inline static int32_t get_offset_of_ClientCertValidation2_21() { return static_cast<int32_t>(offsetof(SslServerStream_t424CB4AC3D421FAF8A73A20520E3E4A4934E2092, ___ClientCertValidation2_21)); }
	inline CertificateValidationCallback2_t80B8CBB56C7605C91427868F9DE72B29837CCD61 * get_ClientCertValidation2_21() const { return ___ClientCertValidation2_21; }
	inline CertificateValidationCallback2_t80B8CBB56C7605C91427868F9DE72B29837CCD61 ** get_address_of_ClientCertValidation2_21() { return &___ClientCertValidation2_21; }
	inline void set_ClientCertValidation2_21(CertificateValidationCallback2_t80B8CBB56C7605C91427868F9DE72B29837CCD61 * value)
	{
		___ClientCertValidation2_21 = value;
		Il2CppCodeGenWriteBarrier((&___ClientCertValidation2_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SSLSERVERSTREAM_T424CB4AC3D421FAF8A73A20520E3E4A4934E2092_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef NULLABLE_1_T87E3837BC1E3397FF9051586CE8E2328CEA1F1D2_H
#define NULLABLE_1_T87E3837BC1E3397FF9051586CE8E2328CEA1F1D2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Mono.Security.Interface.MonoSslPolicyErrors>
struct  Nullable_1_t87E3837BC1E3397FF9051586CE8E2328CEA1F1D2 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t87E3837BC1E3397FF9051586CE8E2328CEA1F1D2, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t87E3837BC1E3397FF9051586CE8E2328CEA1F1D2, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T87E3837BC1E3397FF9051586CE8E2328CEA1F1D2_H
#ifndef NULLABLE_1_T601798BE10C3F3F37B6755E475BB1B3760DCBB10_H
#define NULLABLE_1_T601798BE10C3F3F37B6755E475BB1B3760DCBB10_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Mono.Security.Interface.TlsProtocols>
struct  Nullable_1_t601798BE10C3F3F37B6755E475BB1B3760DCBB10 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t601798BE10C3F3F37B6755E475BB1B3760DCBB10, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t601798BE10C3F3F37B6755E475BB1B3760DCBB10, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T601798BE10C3F3F37B6755E475BB1B3760DCBB10_H
#ifndef SYMMETRICALGORITHM_T0A2EC7E7AD8B8976832B4F0AC432B691F862E789_H
#define SYMMETRICALGORITHM_T0A2EC7E7AD8B8976832B4F0AC432B691F862E789_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.SymmetricAlgorithm
struct  SymmetricAlgorithm_t0A2EC7E7AD8B8976832B4F0AC432B691F862E789  : public RuntimeObject
{
public:
	// System.Int32 System.Security.Cryptography.SymmetricAlgorithm::BlockSizeValue
	int32_t ___BlockSizeValue_0;
	// System.Int32 System.Security.Cryptography.SymmetricAlgorithm::FeedbackSizeValue
	int32_t ___FeedbackSizeValue_1;
	// System.Byte[] System.Security.Cryptography.SymmetricAlgorithm::IVValue
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___IVValue_2;
	// System.Byte[] System.Security.Cryptography.SymmetricAlgorithm::KeyValue
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___KeyValue_3;
	// System.Security.Cryptography.KeySizes[] System.Security.Cryptography.SymmetricAlgorithm::LegalBlockSizesValue
	KeySizesU5BU5D_t934CCA482596402177BAF86727F169872D74934E* ___LegalBlockSizesValue_4;
	// System.Security.Cryptography.KeySizes[] System.Security.Cryptography.SymmetricAlgorithm::LegalKeySizesValue
	KeySizesU5BU5D_t934CCA482596402177BAF86727F169872D74934E* ___LegalKeySizesValue_5;
	// System.Int32 System.Security.Cryptography.SymmetricAlgorithm::KeySizeValue
	int32_t ___KeySizeValue_6;
	// System.Security.Cryptography.CipherMode System.Security.Cryptography.SymmetricAlgorithm::ModeValue
	int32_t ___ModeValue_7;
	// System.Security.Cryptography.PaddingMode System.Security.Cryptography.SymmetricAlgorithm::PaddingValue
	int32_t ___PaddingValue_8;

public:
	inline static int32_t get_offset_of_BlockSizeValue_0() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t0A2EC7E7AD8B8976832B4F0AC432B691F862E789, ___BlockSizeValue_0)); }
	inline int32_t get_BlockSizeValue_0() const { return ___BlockSizeValue_0; }
	inline int32_t* get_address_of_BlockSizeValue_0() { return &___BlockSizeValue_0; }
	inline void set_BlockSizeValue_0(int32_t value)
	{
		___BlockSizeValue_0 = value;
	}

	inline static int32_t get_offset_of_FeedbackSizeValue_1() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t0A2EC7E7AD8B8976832B4F0AC432B691F862E789, ___FeedbackSizeValue_1)); }
	inline int32_t get_FeedbackSizeValue_1() const { return ___FeedbackSizeValue_1; }
	inline int32_t* get_address_of_FeedbackSizeValue_1() { return &___FeedbackSizeValue_1; }
	inline void set_FeedbackSizeValue_1(int32_t value)
	{
		___FeedbackSizeValue_1 = value;
	}

	inline static int32_t get_offset_of_IVValue_2() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t0A2EC7E7AD8B8976832B4F0AC432B691F862E789, ___IVValue_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_IVValue_2() const { return ___IVValue_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_IVValue_2() { return &___IVValue_2; }
	inline void set_IVValue_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___IVValue_2 = value;
		Il2CppCodeGenWriteBarrier((&___IVValue_2), value);
	}

	inline static int32_t get_offset_of_KeyValue_3() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t0A2EC7E7AD8B8976832B4F0AC432B691F862E789, ___KeyValue_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_KeyValue_3() const { return ___KeyValue_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_KeyValue_3() { return &___KeyValue_3; }
	inline void set_KeyValue_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___KeyValue_3 = value;
		Il2CppCodeGenWriteBarrier((&___KeyValue_3), value);
	}

	inline static int32_t get_offset_of_LegalBlockSizesValue_4() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t0A2EC7E7AD8B8976832B4F0AC432B691F862E789, ___LegalBlockSizesValue_4)); }
	inline KeySizesU5BU5D_t934CCA482596402177BAF86727F169872D74934E* get_LegalBlockSizesValue_4() const { return ___LegalBlockSizesValue_4; }
	inline KeySizesU5BU5D_t934CCA482596402177BAF86727F169872D74934E** get_address_of_LegalBlockSizesValue_4() { return &___LegalBlockSizesValue_4; }
	inline void set_LegalBlockSizesValue_4(KeySizesU5BU5D_t934CCA482596402177BAF86727F169872D74934E* value)
	{
		___LegalBlockSizesValue_4 = value;
		Il2CppCodeGenWriteBarrier((&___LegalBlockSizesValue_4), value);
	}

	inline static int32_t get_offset_of_LegalKeySizesValue_5() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t0A2EC7E7AD8B8976832B4F0AC432B691F862E789, ___LegalKeySizesValue_5)); }
	inline KeySizesU5BU5D_t934CCA482596402177BAF86727F169872D74934E* get_LegalKeySizesValue_5() const { return ___LegalKeySizesValue_5; }
	inline KeySizesU5BU5D_t934CCA482596402177BAF86727F169872D74934E** get_address_of_LegalKeySizesValue_5() { return &___LegalKeySizesValue_5; }
	inline void set_LegalKeySizesValue_5(KeySizesU5BU5D_t934CCA482596402177BAF86727F169872D74934E* value)
	{
		___LegalKeySizesValue_5 = value;
		Il2CppCodeGenWriteBarrier((&___LegalKeySizesValue_5), value);
	}

	inline static int32_t get_offset_of_KeySizeValue_6() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t0A2EC7E7AD8B8976832B4F0AC432B691F862E789, ___KeySizeValue_6)); }
	inline int32_t get_KeySizeValue_6() const { return ___KeySizeValue_6; }
	inline int32_t* get_address_of_KeySizeValue_6() { return &___KeySizeValue_6; }
	inline void set_KeySizeValue_6(int32_t value)
	{
		___KeySizeValue_6 = value;
	}

	inline static int32_t get_offset_of_ModeValue_7() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t0A2EC7E7AD8B8976832B4F0AC432B691F862E789, ___ModeValue_7)); }
	inline int32_t get_ModeValue_7() const { return ___ModeValue_7; }
	inline int32_t* get_address_of_ModeValue_7() { return &___ModeValue_7; }
	inline void set_ModeValue_7(int32_t value)
	{
		___ModeValue_7 = value;
	}

	inline static int32_t get_offset_of_PaddingValue_8() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t0A2EC7E7AD8B8976832B4F0AC432B691F862E789, ___PaddingValue_8)); }
	inline int32_t get_PaddingValue_8() const { return ___PaddingValue_8; }
	inline int32_t* get_address_of_PaddingValue_8() { return &___PaddingValue_8; }
	inline void set_PaddingValue_8(int32_t value)
	{
		___PaddingValue_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYMMETRICALGORITHM_T0A2EC7E7AD8B8976832B4F0AC432B691F862E789_H
#ifndef RC4_TC03B8F8797476E53DD785D576E7DE1951667A54B_H
#define RC4_TC03B8F8797476E53DD785D576E7DE1951667A54B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Cryptography.RC4
struct  RC4_tC03B8F8797476E53DD785D576E7DE1951667A54B  : public SymmetricAlgorithm_t0A2EC7E7AD8B8976832B4F0AC432B691F862E789
{
public:

public:
};

struct RC4_tC03B8F8797476E53DD785D576E7DE1951667A54B_StaticFields
{
public:
	// System.Security.Cryptography.KeySizes[] Mono.Security.Cryptography.RC4::s_legalBlockSizes
	KeySizesU5BU5D_t934CCA482596402177BAF86727F169872D74934E* ___s_legalBlockSizes_9;
	// System.Security.Cryptography.KeySizes[] Mono.Security.Cryptography.RC4::s_legalKeySizes
	KeySizesU5BU5D_t934CCA482596402177BAF86727F169872D74934E* ___s_legalKeySizes_10;

public:
	inline static int32_t get_offset_of_s_legalBlockSizes_9() { return static_cast<int32_t>(offsetof(RC4_tC03B8F8797476E53DD785D576E7DE1951667A54B_StaticFields, ___s_legalBlockSizes_9)); }
	inline KeySizesU5BU5D_t934CCA482596402177BAF86727F169872D74934E* get_s_legalBlockSizes_9() const { return ___s_legalBlockSizes_9; }
	inline KeySizesU5BU5D_t934CCA482596402177BAF86727F169872D74934E** get_address_of_s_legalBlockSizes_9() { return &___s_legalBlockSizes_9; }
	inline void set_s_legalBlockSizes_9(KeySizesU5BU5D_t934CCA482596402177BAF86727F169872D74934E* value)
	{
		___s_legalBlockSizes_9 = value;
		Il2CppCodeGenWriteBarrier((&___s_legalBlockSizes_9), value);
	}

	inline static int32_t get_offset_of_s_legalKeySizes_10() { return static_cast<int32_t>(offsetof(RC4_tC03B8F8797476E53DD785D576E7DE1951667A54B_StaticFields, ___s_legalKeySizes_10)); }
	inline KeySizesU5BU5D_t934CCA482596402177BAF86727F169872D74934E* get_s_legalKeySizes_10() const { return ___s_legalKeySizes_10; }
	inline KeySizesU5BU5D_t934CCA482596402177BAF86727F169872D74934E** get_address_of_s_legalKeySizes_10() { return &___s_legalKeySizes_10; }
	inline void set_s_legalKeySizes_10(KeySizesU5BU5D_t934CCA482596402177BAF86727F169872D74934E* value)
	{
		___s_legalKeySizes_10 = value;
		Il2CppCodeGenWriteBarrier((&___s_legalKeySizes_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RC4_TC03B8F8797476E53DD785D576E7DE1951667A54B_H
#ifndef MONOLOCALCERTIFICATESELECTIONCALLBACK_T657381EF916D4EDC456FA5A6AC948EFD7A481F0A_H
#define MONOLOCALCERTIFICATESELECTIONCALLBACK_T657381EF916D4EDC456FA5A6AC948EFD7A481F0A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Interface.MonoLocalCertificateSelectionCallback
struct  MonoLocalCertificateSelectionCallback_t657381EF916D4EDC456FA5A6AC948EFD7A481F0A  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOLOCALCERTIFICATESELECTIONCALLBACK_T657381EF916D4EDC456FA5A6AC948EFD7A481F0A_H
#ifndef MONOREMOTECERTIFICATEVALIDATIONCALLBACK_T7A8DAD12B70CE3BB19BAAD04F587D5ED02385CC6_H
#define MONOREMOTECERTIFICATEVALIDATIONCALLBACK_T7A8DAD12B70CE3BB19BAAD04F587D5ED02385CC6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Interface.MonoRemoteCertificateValidationCallback
struct  MonoRemoteCertificateValidationCallback_t7A8DAD12B70CE3BB19BAAD04F587D5ED02385CC6  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOREMOTECERTIFICATEVALIDATIONCALLBACK_T7A8DAD12B70CE3BB19BAAD04F587D5ED02385CC6_H
#ifndef MONOTLSSETTINGS_T5905C7532C92A87F88C8F3440165DF8AA49A1BBF_H
#define MONOTLSSETTINGS_T5905C7532C92A87F88C8F3440165DF8AA49A1BBF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Interface.MonoTlsSettings
struct  MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF  : public RuntimeObject
{
public:
	// Mono.Security.Interface.MonoRemoteCertificateValidationCallback Mono.Security.Interface.MonoTlsSettings::<RemoteCertificateValidationCallback>k__BackingField
	MonoRemoteCertificateValidationCallback_t7A8DAD12B70CE3BB19BAAD04F587D5ED02385CC6 * ___U3CRemoteCertificateValidationCallbackU3Ek__BackingField_0;
	// Mono.Security.Interface.MonoLocalCertificateSelectionCallback Mono.Security.Interface.MonoTlsSettings::<ClientCertificateSelectionCallback>k__BackingField
	MonoLocalCertificateSelectionCallback_t657381EF916D4EDC456FA5A6AC948EFD7A481F0A * ___U3CClientCertificateSelectionCallbackU3Ek__BackingField_1;
	// System.Nullable`1<System.DateTime> Mono.Security.Interface.MonoTlsSettings::<CertificateValidationTime>k__BackingField
	Nullable_1_t3290384E361396B3724B88B498CBF637D7E87B78  ___U3CCertificateValidationTimeU3Ek__BackingField_2;
	// System.Security.Cryptography.X509Certificates.X509CertificateCollection Mono.Security.Interface.MonoTlsSettings::<TrustAnchors>k__BackingField
	X509CertificateCollection_t824A6C58D0D1B4A7CAE30F26CE8EE4B23A8A1833 * ___U3CTrustAnchorsU3Ek__BackingField_3;
	// System.Object Mono.Security.Interface.MonoTlsSettings::<UserSettings>k__BackingField
	RuntimeObject * ___U3CUserSettingsU3Ek__BackingField_4;
	// System.String[] Mono.Security.Interface.MonoTlsSettings::<CertificateSearchPaths>k__BackingField
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___U3CCertificateSearchPathsU3Ek__BackingField_5;
	// System.Boolean Mono.Security.Interface.MonoTlsSettings::<SendCloseNotify>k__BackingField
	bool ___U3CSendCloseNotifyU3Ek__BackingField_6;
	// System.Nullable`1<Mono.Security.Interface.TlsProtocols> Mono.Security.Interface.MonoTlsSettings::<EnabledProtocols>k__BackingField
	Nullable_1_t601798BE10C3F3F37B6755E475BB1B3760DCBB10  ___U3CEnabledProtocolsU3Ek__BackingField_7;
	// Mono.Security.Interface.CipherSuiteCode[] Mono.Security.Interface.MonoTlsSettings::<EnabledCiphers>k__BackingField
	CipherSuiteCodeU5BU5D_t0EC37AD4A25BB94BA9AB4A9C0C4802BD79A07CC4* ___U3CEnabledCiphersU3Ek__BackingField_8;
	// System.Boolean Mono.Security.Interface.MonoTlsSettings::cloned
	bool ___cloned_9;
	// System.Boolean Mono.Security.Interface.MonoTlsSettings::checkCertName
	bool ___checkCertName_10;
	// System.Boolean Mono.Security.Interface.MonoTlsSettings::checkCertRevocationStatus
	bool ___checkCertRevocationStatus_11;
	// System.Nullable`1<System.Boolean> Mono.Security.Interface.MonoTlsSettings::useServicePointManagerCallback
	Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  ___useServicePointManagerCallback_12;
	// System.Boolean Mono.Security.Interface.MonoTlsSettings::skipSystemValidators
	bool ___skipSystemValidators_13;
	// System.Boolean Mono.Security.Interface.MonoTlsSettings::callbackNeedsChain
	bool ___callbackNeedsChain_14;
	// Mono.Security.Interface.ICertificateValidator Mono.Security.Interface.MonoTlsSettings::certificateValidator
	RuntimeObject* ___certificateValidator_15;

public:
	inline static int32_t get_offset_of_U3CRemoteCertificateValidationCallbackU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF, ___U3CRemoteCertificateValidationCallbackU3Ek__BackingField_0)); }
	inline MonoRemoteCertificateValidationCallback_t7A8DAD12B70CE3BB19BAAD04F587D5ED02385CC6 * get_U3CRemoteCertificateValidationCallbackU3Ek__BackingField_0() const { return ___U3CRemoteCertificateValidationCallbackU3Ek__BackingField_0; }
	inline MonoRemoteCertificateValidationCallback_t7A8DAD12B70CE3BB19BAAD04F587D5ED02385CC6 ** get_address_of_U3CRemoteCertificateValidationCallbackU3Ek__BackingField_0() { return &___U3CRemoteCertificateValidationCallbackU3Ek__BackingField_0; }
	inline void set_U3CRemoteCertificateValidationCallbackU3Ek__BackingField_0(MonoRemoteCertificateValidationCallback_t7A8DAD12B70CE3BB19BAAD04F587D5ED02385CC6 * value)
	{
		___U3CRemoteCertificateValidationCallbackU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRemoteCertificateValidationCallbackU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CClientCertificateSelectionCallbackU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF, ___U3CClientCertificateSelectionCallbackU3Ek__BackingField_1)); }
	inline MonoLocalCertificateSelectionCallback_t657381EF916D4EDC456FA5A6AC948EFD7A481F0A * get_U3CClientCertificateSelectionCallbackU3Ek__BackingField_1() const { return ___U3CClientCertificateSelectionCallbackU3Ek__BackingField_1; }
	inline MonoLocalCertificateSelectionCallback_t657381EF916D4EDC456FA5A6AC948EFD7A481F0A ** get_address_of_U3CClientCertificateSelectionCallbackU3Ek__BackingField_1() { return &___U3CClientCertificateSelectionCallbackU3Ek__BackingField_1; }
	inline void set_U3CClientCertificateSelectionCallbackU3Ek__BackingField_1(MonoLocalCertificateSelectionCallback_t657381EF916D4EDC456FA5A6AC948EFD7A481F0A * value)
	{
		___U3CClientCertificateSelectionCallbackU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CClientCertificateSelectionCallbackU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CCertificateValidationTimeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF, ___U3CCertificateValidationTimeU3Ek__BackingField_2)); }
	inline Nullable_1_t3290384E361396B3724B88B498CBF637D7E87B78  get_U3CCertificateValidationTimeU3Ek__BackingField_2() const { return ___U3CCertificateValidationTimeU3Ek__BackingField_2; }
	inline Nullable_1_t3290384E361396B3724B88B498CBF637D7E87B78 * get_address_of_U3CCertificateValidationTimeU3Ek__BackingField_2() { return &___U3CCertificateValidationTimeU3Ek__BackingField_2; }
	inline void set_U3CCertificateValidationTimeU3Ek__BackingField_2(Nullable_1_t3290384E361396B3724B88B498CBF637D7E87B78  value)
	{
		___U3CCertificateValidationTimeU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CTrustAnchorsU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF, ___U3CTrustAnchorsU3Ek__BackingField_3)); }
	inline X509CertificateCollection_t824A6C58D0D1B4A7CAE30F26CE8EE4B23A8A1833 * get_U3CTrustAnchorsU3Ek__BackingField_3() const { return ___U3CTrustAnchorsU3Ek__BackingField_3; }
	inline X509CertificateCollection_t824A6C58D0D1B4A7CAE30F26CE8EE4B23A8A1833 ** get_address_of_U3CTrustAnchorsU3Ek__BackingField_3() { return &___U3CTrustAnchorsU3Ek__BackingField_3; }
	inline void set_U3CTrustAnchorsU3Ek__BackingField_3(X509CertificateCollection_t824A6C58D0D1B4A7CAE30F26CE8EE4B23A8A1833 * value)
	{
		___U3CTrustAnchorsU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTrustAnchorsU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CUserSettingsU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF, ___U3CUserSettingsU3Ek__BackingField_4)); }
	inline RuntimeObject * get_U3CUserSettingsU3Ek__BackingField_4() const { return ___U3CUserSettingsU3Ek__BackingField_4; }
	inline RuntimeObject ** get_address_of_U3CUserSettingsU3Ek__BackingField_4() { return &___U3CUserSettingsU3Ek__BackingField_4; }
	inline void set_U3CUserSettingsU3Ek__BackingField_4(RuntimeObject * value)
	{
		___U3CUserSettingsU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUserSettingsU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CCertificateSearchPathsU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF, ___U3CCertificateSearchPathsU3Ek__BackingField_5)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_U3CCertificateSearchPathsU3Ek__BackingField_5() const { return ___U3CCertificateSearchPathsU3Ek__BackingField_5; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_U3CCertificateSearchPathsU3Ek__BackingField_5() { return &___U3CCertificateSearchPathsU3Ek__BackingField_5; }
	inline void set_U3CCertificateSearchPathsU3Ek__BackingField_5(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___U3CCertificateSearchPathsU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCertificateSearchPathsU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CSendCloseNotifyU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF, ___U3CSendCloseNotifyU3Ek__BackingField_6)); }
	inline bool get_U3CSendCloseNotifyU3Ek__BackingField_6() const { return ___U3CSendCloseNotifyU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CSendCloseNotifyU3Ek__BackingField_6() { return &___U3CSendCloseNotifyU3Ek__BackingField_6; }
	inline void set_U3CSendCloseNotifyU3Ek__BackingField_6(bool value)
	{
		___U3CSendCloseNotifyU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CEnabledProtocolsU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF, ___U3CEnabledProtocolsU3Ek__BackingField_7)); }
	inline Nullable_1_t601798BE10C3F3F37B6755E475BB1B3760DCBB10  get_U3CEnabledProtocolsU3Ek__BackingField_7() const { return ___U3CEnabledProtocolsU3Ek__BackingField_7; }
	inline Nullable_1_t601798BE10C3F3F37B6755E475BB1B3760DCBB10 * get_address_of_U3CEnabledProtocolsU3Ek__BackingField_7() { return &___U3CEnabledProtocolsU3Ek__BackingField_7; }
	inline void set_U3CEnabledProtocolsU3Ek__BackingField_7(Nullable_1_t601798BE10C3F3F37B6755E475BB1B3760DCBB10  value)
	{
		___U3CEnabledProtocolsU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CEnabledCiphersU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF, ___U3CEnabledCiphersU3Ek__BackingField_8)); }
	inline CipherSuiteCodeU5BU5D_t0EC37AD4A25BB94BA9AB4A9C0C4802BD79A07CC4* get_U3CEnabledCiphersU3Ek__BackingField_8() const { return ___U3CEnabledCiphersU3Ek__BackingField_8; }
	inline CipherSuiteCodeU5BU5D_t0EC37AD4A25BB94BA9AB4A9C0C4802BD79A07CC4** get_address_of_U3CEnabledCiphersU3Ek__BackingField_8() { return &___U3CEnabledCiphersU3Ek__BackingField_8; }
	inline void set_U3CEnabledCiphersU3Ek__BackingField_8(CipherSuiteCodeU5BU5D_t0EC37AD4A25BB94BA9AB4A9C0C4802BD79A07CC4* value)
	{
		___U3CEnabledCiphersU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEnabledCiphersU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_cloned_9() { return static_cast<int32_t>(offsetof(MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF, ___cloned_9)); }
	inline bool get_cloned_9() const { return ___cloned_9; }
	inline bool* get_address_of_cloned_9() { return &___cloned_9; }
	inline void set_cloned_9(bool value)
	{
		___cloned_9 = value;
	}

	inline static int32_t get_offset_of_checkCertName_10() { return static_cast<int32_t>(offsetof(MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF, ___checkCertName_10)); }
	inline bool get_checkCertName_10() const { return ___checkCertName_10; }
	inline bool* get_address_of_checkCertName_10() { return &___checkCertName_10; }
	inline void set_checkCertName_10(bool value)
	{
		___checkCertName_10 = value;
	}

	inline static int32_t get_offset_of_checkCertRevocationStatus_11() { return static_cast<int32_t>(offsetof(MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF, ___checkCertRevocationStatus_11)); }
	inline bool get_checkCertRevocationStatus_11() const { return ___checkCertRevocationStatus_11; }
	inline bool* get_address_of_checkCertRevocationStatus_11() { return &___checkCertRevocationStatus_11; }
	inline void set_checkCertRevocationStatus_11(bool value)
	{
		___checkCertRevocationStatus_11 = value;
	}

	inline static int32_t get_offset_of_useServicePointManagerCallback_12() { return static_cast<int32_t>(offsetof(MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF, ___useServicePointManagerCallback_12)); }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  get_useServicePointManagerCallback_12() const { return ___useServicePointManagerCallback_12; }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 * get_address_of_useServicePointManagerCallback_12() { return &___useServicePointManagerCallback_12; }
	inline void set_useServicePointManagerCallback_12(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  value)
	{
		___useServicePointManagerCallback_12 = value;
	}

	inline static int32_t get_offset_of_skipSystemValidators_13() { return static_cast<int32_t>(offsetof(MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF, ___skipSystemValidators_13)); }
	inline bool get_skipSystemValidators_13() const { return ___skipSystemValidators_13; }
	inline bool* get_address_of_skipSystemValidators_13() { return &___skipSystemValidators_13; }
	inline void set_skipSystemValidators_13(bool value)
	{
		___skipSystemValidators_13 = value;
	}

	inline static int32_t get_offset_of_callbackNeedsChain_14() { return static_cast<int32_t>(offsetof(MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF, ___callbackNeedsChain_14)); }
	inline bool get_callbackNeedsChain_14() const { return ___callbackNeedsChain_14; }
	inline bool* get_address_of_callbackNeedsChain_14() { return &___callbackNeedsChain_14; }
	inline void set_callbackNeedsChain_14(bool value)
	{
		___callbackNeedsChain_14 = value;
	}

	inline static int32_t get_offset_of_certificateValidator_15() { return static_cast<int32_t>(offsetof(MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF, ___certificateValidator_15)); }
	inline RuntimeObject* get_certificateValidator_15() const { return ___certificateValidator_15; }
	inline RuntimeObject** get_address_of_certificateValidator_15() { return &___certificateValidator_15; }
	inline void set_certificateValidator_15(RuntimeObject* value)
	{
		___certificateValidator_15 = value;
		Il2CppCodeGenWriteBarrier((&___certificateValidator_15), value);
	}
};

struct MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF_StaticFields
{
public:
	// Mono.Security.Interface.MonoTlsSettings Mono.Security.Interface.MonoTlsSettings::defaultSettings
	MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF * ___defaultSettings_16;

public:
	inline static int32_t get_offset_of_defaultSettings_16() { return static_cast<int32_t>(offsetof(MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF_StaticFields, ___defaultSettings_16)); }
	inline MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF * get_defaultSettings_16() const { return ___defaultSettings_16; }
	inline MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF ** get_address_of_defaultSettings_16() { return &___defaultSettings_16; }
	inline void set_defaultSettings_16(MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF * value)
	{
		___defaultSettings_16 = value;
		Il2CppCodeGenWriteBarrier((&___defaultSettings_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOTLSSETTINGS_T5905C7532C92A87F88C8F3440165DF8AA49A1BBF_H
#ifndef VALIDATIONRESULT_TBBAD776F36C835C8DDB515B1747DEF3A45C058D7_H
#define VALIDATIONRESULT_TBBAD776F36C835C8DDB515B1747DEF3A45C058D7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Interface.ValidationResult
struct  ValidationResult_tBBAD776F36C835C8DDB515B1747DEF3A45C058D7  : public RuntimeObject
{
public:
	// System.Boolean Mono.Security.Interface.ValidationResult::trusted
	bool ___trusted_0;
	// System.Boolean Mono.Security.Interface.ValidationResult::user_denied
	bool ___user_denied_1;
	// System.Int32 Mono.Security.Interface.ValidationResult::error_code
	int32_t ___error_code_2;
	// System.Nullable`1<Mono.Security.Interface.MonoSslPolicyErrors> Mono.Security.Interface.ValidationResult::policy_errors
	Nullable_1_t87E3837BC1E3397FF9051586CE8E2328CEA1F1D2  ___policy_errors_3;

public:
	inline static int32_t get_offset_of_trusted_0() { return static_cast<int32_t>(offsetof(ValidationResult_tBBAD776F36C835C8DDB515B1747DEF3A45C058D7, ___trusted_0)); }
	inline bool get_trusted_0() const { return ___trusted_0; }
	inline bool* get_address_of_trusted_0() { return &___trusted_0; }
	inline void set_trusted_0(bool value)
	{
		___trusted_0 = value;
	}

	inline static int32_t get_offset_of_user_denied_1() { return static_cast<int32_t>(offsetof(ValidationResult_tBBAD776F36C835C8DDB515B1747DEF3A45C058D7, ___user_denied_1)); }
	inline bool get_user_denied_1() const { return ___user_denied_1; }
	inline bool* get_address_of_user_denied_1() { return &___user_denied_1; }
	inline void set_user_denied_1(bool value)
	{
		___user_denied_1 = value;
	}

	inline static int32_t get_offset_of_error_code_2() { return static_cast<int32_t>(offsetof(ValidationResult_tBBAD776F36C835C8DDB515B1747DEF3A45C058D7, ___error_code_2)); }
	inline int32_t get_error_code_2() const { return ___error_code_2; }
	inline int32_t* get_address_of_error_code_2() { return &___error_code_2; }
	inline void set_error_code_2(int32_t value)
	{
		___error_code_2 = value;
	}

	inline static int32_t get_offset_of_policy_errors_3() { return static_cast<int32_t>(offsetof(ValidationResult_tBBAD776F36C835C8DDB515B1747DEF3A45C058D7, ___policy_errors_3)); }
	inline Nullable_1_t87E3837BC1E3397FF9051586CE8E2328CEA1F1D2  get_policy_errors_3() const { return ___policy_errors_3; }
	inline Nullable_1_t87E3837BC1E3397FF9051586CE8E2328CEA1F1D2 * get_address_of_policy_errors_3() { return &___policy_errors_3; }
	inline void set_policy_errors_3(Nullable_1_t87E3837BC1E3397FF9051586CE8E2328CEA1F1D2  value)
	{
		___policy_errors_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALIDATIONRESULT_TBBAD776F36C835C8DDB515B1747DEF3A45C058D7_H
#ifndef TYPE1MESSAGE_TF2DA0014BB300ABA864D84752FFA278EC6E6519C_H
#define TYPE1MESSAGE_TF2DA0014BB300ABA864D84752FFA278EC6E6519C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Ntlm.Type1Message
struct  Type1Message_tF2DA0014BB300ABA864D84752FFA278EC6E6519C  : public MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0
{
public:
	// System.String Mono.Security.Protocol.Ntlm.Type1Message::_host
	String_t* ____host_3;
	// System.String Mono.Security.Protocol.Ntlm.Type1Message::_domain
	String_t* ____domain_4;

public:
	inline static int32_t get_offset_of__host_3() { return static_cast<int32_t>(offsetof(Type1Message_tF2DA0014BB300ABA864D84752FFA278EC6E6519C, ____host_3)); }
	inline String_t* get__host_3() const { return ____host_3; }
	inline String_t** get_address_of__host_3() { return &____host_3; }
	inline void set__host_3(String_t* value)
	{
		____host_3 = value;
		Il2CppCodeGenWriteBarrier((&____host_3), value);
	}

	inline static int32_t get_offset_of__domain_4() { return static_cast<int32_t>(offsetof(Type1Message_tF2DA0014BB300ABA864D84752FFA278EC6E6519C, ____domain_4)); }
	inline String_t* get__domain_4() const { return ____domain_4; }
	inline String_t** get_address_of__domain_4() { return &____domain_4; }
	inline void set__domain_4(String_t* value)
	{
		____domain_4 = value;
		Il2CppCodeGenWriteBarrier((&____domain_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE1MESSAGE_TF2DA0014BB300ABA864D84752FFA278EC6E6519C_H
#ifndef TYPE2MESSAGE_T990283F48D41AECF1FFBDAA3A194CDE9C9078398_H
#define TYPE2MESSAGE_T990283F48D41AECF1FFBDAA3A194CDE9C9078398_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Ntlm.Type2Message
struct  Type2Message_t990283F48D41AECF1FFBDAA3A194CDE9C9078398  : public MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0
{
public:
	// System.Byte[] Mono.Security.Protocol.Ntlm.Type2Message::_nonce
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____nonce_3;
	// System.String Mono.Security.Protocol.Ntlm.Type2Message::_targetName
	String_t* ____targetName_4;
	// System.Byte[] Mono.Security.Protocol.Ntlm.Type2Message::_targetInfo
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____targetInfo_5;

public:
	inline static int32_t get_offset_of__nonce_3() { return static_cast<int32_t>(offsetof(Type2Message_t990283F48D41AECF1FFBDAA3A194CDE9C9078398, ____nonce_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__nonce_3() const { return ____nonce_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__nonce_3() { return &____nonce_3; }
	inline void set__nonce_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____nonce_3 = value;
		Il2CppCodeGenWriteBarrier((&____nonce_3), value);
	}

	inline static int32_t get_offset_of__targetName_4() { return static_cast<int32_t>(offsetof(Type2Message_t990283F48D41AECF1FFBDAA3A194CDE9C9078398, ____targetName_4)); }
	inline String_t* get__targetName_4() const { return ____targetName_4; }
	inline String_t** get_address_of__targetName_4() { return &____targetName_4; }
	inline void set__targetName_4(String_t* value)
	{
		____targetName_4 = value;
		Il2CppCodeGenWriteBarrier((&____targetName_4), value);
	}

	inline static int32_t get_offset_of__targetInfo_5() { return static_cast<int32_t>(offsetof(Type2Message_t990283F48D41AECF1FFBDAA3A194CDE9C9078398, ____targetInfo_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__targetInfo_5() const { return ____targetInfo_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__targetInfo_5() { return &____targetInfo_5; }
	inline void set__targetInfo_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____targetInfo_5 = value;
		Il2CppCodeGenWriteBarrier((&____targetInfo_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE2MESSAGE_T990283F48D41AECF1FFBDAA3A194CDE9C9078398_H
#ifndef TYPE3MESSAGE_T6D21CF9E3D56192F8D9B6E2B29474773E838846C_H
#define TYPE3MESSAGE_T6D21CF9E3D56192F8D9B6E2B29474773E838846C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Ntlm.Type3Message
struct  Type3Message_t6D21CF9E3D56192F8D9B6E2B29474773E838846C  : public MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0
{
public:
	// Mono.Security.Protocol.Ntlm.NtlmAuthLevel Mono.Security.Protocol.Ntlm.Type3Message::_level
	int32_t ____level_3;
	// System.Byte[] Mono.Security.Protocol.Ntlm.Type3Message::_challenge
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____challenge_4;
	// System.String Mono.Security.Protocol.Ntlm.Type3Message::_host
	String_t* ____host_5;
	// System.String Mono.Security.Protocol.Ntlm.Type3Message::_domain
	String_t* ____domain_6;
	// System.String Mono.Security.Protocol.Ntlm.Type3Message::_username
	String_t* ____username_7;
	// System.String Mono.Security.Protocol.Ntlm.Type3Message::_password
	String_t* ____password_8;
	// Mono.Security.Protocol.Ntlm.Type2Message Mono.Security.Protocol.Ntlm.Type3Message::_type2
	Type2Message_t990283F48D41AECF1FFBDAA3A194CDE9C9078398 * ____type2_9;
	// System.Byte[] Mono.Security.Protocol.Ntlm.Type3Message::_lm
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____lm_10;
	// System.Byte[] Mono.Security.Protocol.Ntlm.Type3Message::_nt
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____nt_11;

public:
	inline static int32_t get_offset_of__level_3() { return static_cast<int32_t>(offsetof(Type3Message_t6D21CF9E3D56192F8D9B6E2B29474773E838846C, ____level_3)); }
	inline int32_t get__level_3() const { return ____level_3; }
	inline int32_t* get_address_of__level_3() { return &____level_3; }
	inline void set__level_3(int32_t value)
	{
		____level_3 = value;
	}

	inline static int32_t get_offset_of__challenge_4() { return static_cast<int32_t>(offsetof(Type3Message_t6D21CF9E3D56192F8D9B6E2B29474773E838846C, ____challenge_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__challenge_4() const { return ____challenge_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__challenge_4() { return &____challenge_4; }
	inline void set__challenge_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____challenge_4 = value;
		Il2CppCodeGenWriteBarrier((&____challenge_4), value);
	}

	inline static int32_t get_offset_of__host_5() { return static_cast<int32_t>(offsetof(Type3Message_t6D21CF9E3D56192F8D9B6E2B29474773E838846C, ____host_5)); }
	inline String_t* get__host_5() const { return ____host_5; }
	inline String_t** get_address_of__host_5() { return &____host_5; }
	inline void set__host_5(String_t* value)
	{
		____host_5 = value;
		Il2CppCodeGenWriteBarrier((&____host_5), value);
	}

	inline static int32_t get_offset_of__domain_6() { return static_cast<int32_t>(offsetof(Type3Message_t6D21CF9E3D56192F8D9B6E2B29474773E838846C, ____domain_6)); }
	inline String_t* get__domain_6() const { return ____domain_6; }
	inline String_t** get_address_of__domain_6() { return &____domain_6; }
	inline void set__domain_6(String_t* value)
	{
		____domain_6 = value;
		Il2CppCodeGenWriteBarrier((&____domain_6), value);
	}

	inline static int32_t get_offset_of__username_7() { return static_cast<int32_t>(offsetof(Type3Message_t6D21CF9E3D56192F8D9B6E2B29474773E838846C, ____username_7)); }
	inline String_t* get__username_7() const { return ____username_7; }
	inline String_t** get_address_of__username_7() { return &____username_7; }
	inline void set__username_7(String_t* value)
	{
		____username_7 = value;
		Il2CppCodeGenWriteBarrier((&____username_7), value);
	}

	inline static int32_t get_offset_of__password_8() { return static_cast<int32_t>(offsetof(Type3Message_t6D21CF9E3D56192F8D9B6E2B29474773E838846C, ____password_8)); }
	inline String_t* get__password_8() const { return ____password_8; }
	inline String_t** get_address_of__password_8() { return &____password_8; }
	inline void set__password_8(String_t* value)
	{
		____password_8 = value;
		Il2CppCodeGenWriteBarrier((&____password_8), value);
	}

	inline static int32_t get_offset_of__type2_9() { return static_cast<int32_t>(offsetof(Type3Message_t6D21CF9E3D56192F8D9B6E2B29474773E838846C, ____type2_9)); }
	inline Type2Message_t990283F48D41AECF1FFBDAA3A194CDE9C9078398 * get__type2_9() const { return ____type2_9; }
	inline Type2Message_t990283F48D41AECF1FFBDAA3A194CDE9C9078398 ** get_address_of__type2_9() { return &____type2_9; }
	inline void set__type2_9(Type2Message_t990283F48D41AECF1FFBDAA3A194CDE9C9078398 * value)
	{
		____type2_9 = value;
		Il2CppCodeGenWriteBarrier((&____type2_9), value);
	}

	inline static int32_t get_offset_of__lm_10() { return static_cast<int32_t>(offsetof(Type3Message_t6D21CF9E3D56192F8D9B6E2B29474773E838846C, ____lm_10)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__lm_10() const { return ____lm_10; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__lm_10() { return &____lm_10; }
	inline void set__lm_10(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____lm_10 = value;
		Il2CppCodeGenWriteBarrier((&____lm_10), value);
	}

	inline static int32_t get_offset_of__nt_11() { return static_cast<int32_t>(offsetof(Type3Message_t6D21CF9E3D56192F8D9B6E2B29474773E838846C, ____nt_11)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__nt_11() const { return ____nt_11; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__nt_11() { return &____nt_11; }
	inline void set__nt_11(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____nt_11 = value;
		Il2CppCodeGenWriteBarrier((&____nt_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE3MESSAGE_T6D21CF9E3D56192F8D9B6E2B29474773E838846C_H
#ifndef CERTIFICATESELECTIONCALLBACK_T2D981EF7A74CC271A9F086BB0F483FD6BCEDF5BE_H
#define CERTIFICATESELECTIONCALLBACK_T2D981EF7A74CC271A9F086BB0F483FD6BCEDF5BE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.CertificateSelectionCallback
struct  CertificateSelectionCallback_t2D981EF7A74CC271A9F086BB0F483FD6BCEDF5BE  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CERTIFICATESELECTIONCALLBACK_T2D981EF7A74CC271A9F086BB0F483FD6BCEDF5BE_H
#ifndef CERTIFICATEVALIDATIONCALLBACK_T4FDC57D3B9528BD8D0564B4B3B2CF135E9CD72D3_H
#define CERTIFICATEVALIDATIONCALLBACK_T4FDC57D3B9528BD8D0564B4B3B2CF135E9CD72D3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.CertificateValidationCallback
struct  CertificateValidationCallback_t4FDC57D3B9528BD8D0564B4B3B2CF135E9CD72D3  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CERTIFICATEVALIDATIONCALLBACK_T4FDC57D3B9528BD8D0564B4B3B2CF135E9CD72D3_H
#ifndef CERTIFICATEVALIDATIONCALLBACK2_T80B8CBB56C7605C91427868F9DE72B29837CCD61_H
#define CERTIFICATEVALIDATIONCALLBACK2_T80B8CBB56C7605C91427868F9DE72B29837CCD61_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.CertificateValidationCallback2
struct  CertificateValidationCallback2_t80B8CBB56C7605C91427868F9DE72B29837CCD61  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CERTIFICATEVALIDATIONCALLBACK2_T80B8CBB56C7605C91427868F9DE72B29837CCD61_H
#ifndef TLSCLIENTCERTIFICATE_T03B550B55DF5FC35774F3B5077E576367DFB16C2_H
#define TLSCLIENTCERTIFICATE_T03B550B55DF5FC35774F3B5077E576367DFB16C2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.Handshake.Client.TlsClientCertificate
struct  TlsClientCertificate_t03B550B55DF5FC35774F3B5077E576367DFB16C2  : public HandshakeMessage_t9B97FBAE0BAFCEA052F2B5EA880817CA1345298C
{
public:
	// System.Boolean Mono.Security.Protocol.Tls.Handshake.Client.TlsClientCertificate::clientCertSelected
	bool ___clientCertSelected_14;
	// System.Security.Cryptography.X509Certificates.X509Certificate Mono.Security.Protocol.Tls.Handshake.Client.TlsClientCertificate::clientCert
	X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 * ___clientCert_15;

public:
	inline static int32_t get_offset_of_clientCertSelected_14() { return static_cast<int32_t>(offsetof(TlsClientCertificate_t03B550B55DF5FC35774F3B5077E576367DFB16C2, ___clientCertSelected_14)); }
	inline bool get_clientCertSelected_14() const { return ___clientCertSelected_14; }
	inline bool* get_address_of_clientCertSelected_14() { return &___clientCertSelected_14; }
	inline void set_clientCertSelected_14(bool value)
	{
		___clientCertSelected_14 = value;
	}

	inline static int32_t get_offset_of_clientCert_15() { return static_cast<int32_t>(offsetof(TlsClientCertificate_t03B550B55DF5FC35774F3B5077E576367DFB16C2, ___clientCert_15)); }
	inline X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 * get_clientCert_15() const { return ___clientCert_15; }
	inline X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 ** get_address_of_clientCert_15() { return &___clientCert_15; }
	inline void set_clientCert_15(X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 * value)
	{
		___clientCert_15 = value;
		Il2CppCodeGenWriteBarrier((&___clientCert_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSCLIENTCERTIFICATE_T03B550B55DF5FC35774F3B5077E576367DFB16C2_H
#ifndef TLSCLIENTCERTIFICATEVERIFY_T7403133EF8E01827A1B2D4F0C987C7927FABAEC5_H
#define TLSCLIENTCERTIFICATEVERIFY_T7403133EF8E01827A1B2D4F0C987C7927FABAEC5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.Handshake.Client.TlsClientCertificateVerify
struct  TlsClientCertificateVerify_t7403133EF8E01827A1B2D4F0C987C7927FABAEC5  : public HandshakeMessage_t9B97FBAE0BAFCEA052F2B5EA880817CA1345298C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSCLIENTCERTIFICATEVERIFY_T7403133EF8E01827A1B2D4F0C987C7927FABAEC5_H
#ifndef TLSCLIENTFINISHED_TDE6377D436615E994372DEC7CD1FF034EA610A90_H
#define TLSCLIENTFINISHED_TDE6377D436615E994372DEC7CD1FF034EA610A90_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.Handshake.Client.TlsClientFinished
struct  TlsClientFinished_tDE6377D436615E994372DEC7CD1FF034EA610A90  : public HandshakeMessage_t9B97FBAE0BAFCEA052F2B5EA880817CA1345298C
{
public:

public:
};

struct TlsClientFinished_tDE6377D436615E994372DEC7CD1FF034EA610A90_StaticFields
{
public:
	// System.Byte[] Mono.Security.Protocol.Tls.Handshake.Client.TlsClientFinished::Ssl3Marker
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___Ssl3Marker_14;

public:
	inline static int32_t get_offset_of_Ssl3Marker_14() { return static_cast<int32_t>(offsetof(TlsClientFinished_tDE6377D436615E994372DEC7CD1FF034EA610A90_StaticFields, ___Ssl3Marker_14)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_Ssl3Marker_14() const { return ___Ssl3Marker_14; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_Ssl3Marker_14() { return &___Ssl3Marker_14; }
	inline void set_Ssl3Marker_14(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___Ssl3Marker_14 = value;
		Il2CppCodeGenWriteBarrier((&___Ssl3Marker_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSCLIENTFINISHED_TDE6377D436615E994372DEC7CD1FF034EA610A90_H
#ifndef TLSCLIENTHELLO_T6F6C4D0AD370DAF5DAD157F6EE6E2F4C60864013_H
#define TLSCLIENTHELLO_T6F6C4D0AD370DAF5DAD157F6EE6E2F4C60864013_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.Handshake.Client.TlsClientHello
struct  TlsClientHello_t6F6C4D0AD370DAF5DAD157F6EE6E2F4C60864013  : public HandshakeMessage_t9B97FBAE0BAFCEA052F2B5EA880817CA1345298C
{
public:
	// System.Byte[] Mono.Security.Protocol.Tls.Handshake.Client.TlsClientHello::random
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___random_14;

public:
	inline static int32_t get_offset_of_random_14() { return static_cast<int32_t>(offsetof(TlsClientHello_t6F6C4D0AD370DAF5DAD157F6EE6E2F4C60864013, ___random_14)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_random_14() const { return ___random_14; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_random_14() { return &___random_14; }
	inline void set_random_14(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___random_14 = value;
		Il2CppCodeGenWriteBarrier((&___random_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSCLIENTHELLO_T6F6C4D0AD370DAF5DAD157F6EE6E2F4C60864013_H
#ifndef TLSCLIENTKEYEXCHANGE_TE244A6ABFC942308A37FFE5C0C12B4C67CC28460_H
#define TLSCLIENTKEYEXCHANGE_TE244A6ABFC942308A37FFE5C0C12B4C67CC28460_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.Handshake.Client.TlsClientKeyExchange
struct  TlsClientKeyExchange_tE244A6ABFC942308A37FFE5C0C12B4C67CC28460  : public HandshakeMessage_t9B97FBAE0BAFCEA052F2B5EA880817CA1345298C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSCLIENTKEYEXCHANGE_TE244A6ABFC942308A37FFE5C0C12B4C67CC28460_H
#ifndef TLSSERVERCERTIFICATE_TBA4685202875F457FA1FFD3F0B7E50C621A7DDEC_H
#define TLSSERVERCERTIFICATE_TBA4685202875F457FA1FFD3F0B7E50C621A7DDEC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificate
struct  TlsServerCertificate_tBA4685202875F457FA1FFD3F0B7E50C621A7DDEC  : public HandshakeMessage_t9B97FBAE0BAFCEA052F2B5EA880817CA1345298C
{
public:
	// Mono.Security.X509.X509CertificateCollection Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificate::certificates
	X509CertificateCollection_t423BA1B9FAA983BA745023994C648C6DAC3E5A1A * ___certificates_14;

public:
	inline static int32_t get_offset_of_certificates_14() { return static_cast<int32_t>(offsetof(TlsServerCertificate_tBA4685202875F457FA1FFD3F0B7E50C621A7DDEC, ___certificates_14)); }
	inline X509CertificateCollection_t423BA1B9FAA983BA745023994C648C6DAC3E5A1A * get_certificates_14() const { return ___certificates_14; }
	inline X509CertificateCollection_t423BA1B9FAA983BA745023994C648C6DAC3E5A1A ** get_address_of_certificates_14() { return &___certificates_14; }
	inline void set_certificates_14(X509CertificateCollection_t423BA1B9FAA983BA745023994C648C6DAC3E5A1A * value)
	{
		___certificates_14 = value;
		Il2CppCodeGenWriteBarrier((&___certificates_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSSERVERCERTIFICATE_TBA4685202875F457FA1FFD3F0B7E50C621A7DDEC_H
#ifndef TLSSERVERCERTIFICATEREQUEST_T005A4A579268F30770A0EE3FA9FA772B204E6900_H
#define TLSSERVERCERTIFICATEREQUEST_T005A4A579268F30770A0EE3FA9FA772B204E6900_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificateRequest
struct  TlsServerCertificateRequest_t005A4A579268F30770A0EE3FA9FA772B204E6900  : public HandshakeMessage_t9B97FBAE0BAFCEA052F2B5EA880817CA1345298C
{
public:
	// Mono.Security.Protocol.Tls.Handshake.ClientCertificateType[] Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificateRequest::certificateTypes
	ClientCertificateTypeU5BU5D_t06B6854DC307CF9A4DC750B89CEC349214A2147B* ___certificateTypes_14;
	// System.String[] Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificateRequest::distinguisedNames
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___distinguisedNames_15;

public:
	inline static int32_t get_offset_of_certificateTypes_14() { return static_cast<int32_t>(offsetof(TlsServerCertificateRequest_t005A4A579268F30770A0EE3FA9FA772B204E6900, ___certificateTypes_14)); }
	inline ClientCertificateTypeU5BU5D_t06B6854DC307CF9A4DC750B89CEC349214A2147B* get_certificateTypes_14() const { return ___certificateTypes_14; }
	inline ClientCertificateTypeU5BU5D_t06B6854DC307CF9A4DC750B89CEC349214A2147B** get_address_of_certificateTypes_14() { return &___certificateTypes_14; }
	inline void set_certificateTypes_14(ClientCertificateTypeU5BU5D_t06B6854DC307CF9A4DC750B89CEC349214A2147B* value)
	{
		___certificateTypes_14 = value;
		Il2CppCodeGenWriteBarrier((&___certificateTypes_14), value);
	}

	inline static int32_t get_offset_of_distinguisedNames_15() { return static_cast<int32_t>(offsetof(TlsServerCertificateRequest_t005A4A579268F30770A0EE3FA9FA772B204E6900, ___distinguisedNames_15)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_distinguisedNames_15() const { return ___distinguisedNames_15; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_distinguisedNames_15() { return &___distinguisedNames_15; }
	inline void set_distinguisedNames_15(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___distinguisedNames_15 = value;
		Il2CppCodeGenWriteBarrier((&___distinguisedNames_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSSERVERCERTIFICATEREQUEST_T005A4A579268F30770A0EE3FA9FA772B204E6900_H
#ifndef TLSSERVERFINISHED_TFCC471675890AC0C3EE7899CF10AAE3D55EBDDE7_H
#define TLSSERVERFINISHED_TFCC471675890AC0C3EE7899CF10AAE3D55EBDDE7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerFinished
struct  TlsServerFinished_tFCC471675890AC0C3EE7899CF10AAE3D55EBDDE7  : public HandshakeMessage_t9B97FBAE0BAFCEA052F2B5EA880817CA1345298C
{
public:

public:
};

struct TlsServerFinished_tFCC471675890AC0C3EE7899CF10AAE3D55EBDDE7_StaticFields
{
public:
	// System.Byte[] Mono.Security.Protocol.Tls.Handshake.Client.TlsServerFinished::Ssl3Marker
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___Ssl3Marker_14;

public:
	inline static int32_t get_offset_of_Ssl3Marker_14() { return static_cast<int32_t>(offsetof(TlsServerFinished_tFCC471675890AC0C3EE7899CF10AAE3D55EBDDE7_StaticFields, ___Ssl3Marker_14)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_Ssl3Marker_14() const { return ___Ssl3Marker_14; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_Ssl3Marker_14() { return &___Ssl3Marker_14; }
	inline void set_Ssl3Marker_14(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___Ssl3Marker_14 = value;
		Il2CppCodeGenWriteBarrier((&___Ssl3Marker_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSSERVERFINISHED_TFCC471675890AC0C3EE7899CF10AAE3D55EBDDE7_H
#ifndef TLSSERVERHELLO_TE57721F1222D4A82CC98058BCDA5511B0AA4F294_H
#define TLSSERVERHELLO_TE57721F1222D4A82CC98058BCDA5511B0AA4F294_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHello
struct  TlsServerHello_tE57721F1222D4A82CC98058BCDA5511B0AA4F294  : public HandshakeMessage_t9B97FBAE0BAFCEA052F2B5EA880817CA1345298C
{
public:
	// Mono.Security.Protocol.Tls.SecurityCompressionType Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHello::compressionMethod
	int32_t ___compressionMethod_14;
	// System.Byte[] Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHello::random
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___random_15;
	// System.Byte[] Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHello::sessionId
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___sessionId_16;
	// Mono.Security.Protocol.Tls.CipherSuite Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHello::cipherSuite
	CipherSuite_t203D2B4A797805B775BEFB3B623962E7B407B6F7 * ___cipherSuite_17;

public:
	inline static int32_t get_offset_of_compressionMethod_14() { return static_cast<int32_t>(offsetof(TlsServerHello_tE57721F1222D4A82CC98058BCDA5511B0AA4F294, ___compressionMethod_14)); }
	inline int32_t get_compressionMethod_14() const { return ___compressionMethod_14; }
	inline int32_t* get_address_of_compressionMethod_14() { return &___compressionMethod_14; }
	inline void set_compressionMethod_14(int32_t value)
	{
		___compressionMethod_14 = value;
	}

	inline static int32_t get_offset_of_random_15() { return static_cast<int32_t>(offsetof(TlsServerHello_tE57721F1222D4A82CC98058BCDA5511B0AA4F294, ___random_15)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_random_15() const { return ___random_15; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_random_15() { return &___random_15; }
	inline void set_random_15(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___random_15 = value;
		Il2CppCodeGenWriteBarrier((&___random_15), value);
	}

	inline static int32_t get_offset_of_sessionId_16() { return static_cast<int32_t>(offsetof(TlsServerHello_tE57721F1222D4A82CC98058BCDA5511B0AA4F294, ___sessionId_16)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_sessionId_16() const { return ___sessionId_16; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_sessionId_16() { return &___sessionId_16; }
	inline void set_sessionId_16(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___sessionId_16 = value;
		Il2CppCodeGenWriteBarrier((&___sessionId_16), value);
	}

	inline static int32_t get_offset_of_cipherSuite_17() { return static_cast<int32_t>(offsetof(TlsServerHello_tE57721F1222D4A82CC98058BCDA5511B0AA4F294, ___cipherSuite_17)); }
	inline CipherSuite_t203D2B4A797805B775BEFB3B623962E7B407B6F7 * get_cipherSuite_17() const { return ___cipherSuite_17; }
	inline CipherSuite_t203D2B4A797805B775BEFB3B623962E7B407B6F7 ** get_address_of_cipherSuite_17() { return &___cipherSuite_17; }
	inline void set_cipherSuite_17(CipherSuite_t203D2B4A797805B775BEFB3B623962E7B407B6F7 * value)
	{
		___cipherSuite_17 = value;
		Il2CppCodeGenWriteBarrier((&___cipherSuite_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSSERVERHELLO_TE57721F1222D4A82CC98058BCDA5511B0AA4F294_H
#ifndef TLSSERVERHELLODONE_TE26C2109DAF7607E99910210AD1B78142BD37CC9_H
#define TLSSERVERHELLODONE_TE26C2109DAF7607E99910210AD1B78142BD37CC9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHelloDone
struct  TlsServerHelloDone_tE26C2109DAF7607E99910210AD1B78142BD37CC9  : public HandshakeMessage_t9B97FBAE0BAFCEA052F2B5EA880817CA1345298C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSSERVERHELLODONE_TE26C2109DAF7607E99910210AD1B78142BD37CC9_H
#ifndef TLSCLIENTCERTIFICATE_TDB7DEB656354449A78DCA4EC0EAD85EC4CC9712B_H
#define TLSCLIENTCERTIFICATE_TDB7DEB656354449A78DCA4EC0EAD85EC4CC9712B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.Handshake.Server.TlsClientCertificate
struct  TlsClientCertificate_tDB7DEB656354449A78DCA4EC0EAD85EC4CC9712B  : public HandshakeMessage_t9B97FBAE0BAFCEA052F2B5EA880817CA1345298C
{
public:
	// Mono.Security.X509.X509CertificateCollection Mono.Security.Protocol.Tls.Handshake.Server.TlsClientCertificate::clientCertificates
	X509CertificateCollection_t423BA1B9FAA983BA745023994C648C6DAC3E5A1A * ___clientCertificates_14;

public:
	inline static int32_t get_offset_of_clientCertificates_14() { return static_cast<int32_t>(offsetof(TlsClientCertificate_tDB7DEB656354449A78DCA4EC0EAD85EC4CC9712B, ___clientCertificates_14)); }
	inline X509CertificateCollection_t423BA1B9FAA983BA745023994C648C6DAC3E5A1A * get_clientCertificates_14() const { return ___clientCertificates_14; }
	inline X509CertificateCollection_t423BA1B9FAA983BA745023994C648C6DAC3E5A1A ** get_address_of_clientCertificates_14() { return &___clientCertificates_14; }
	inline void set_clientCertificates_14(X509CertificateCollection_t423BA1B9FAA983BA745023994C648C6DAC3E5A1A * value)
	{
		___clientCertificates_14 = value;
		Il2CppCodeGenWriteBarrier((&___clientCertificates_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSCLIENTCERTIFICATE_TDB7DEB656354449A78DCA4EC0EAD85EC4CC9712B_H
#ifndef TLSCLIENTCERTIFICATEVERIFY_T1FA0AB8DF4EAF73FDD4473C5B04AF978CC3B0854_H
#define TLSCLIENTCERTIFICATEVERIFY_T1FA0AB8DF4EAF73FDD4473C5B04AF978CC3B0854_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.Handshake.Server.TlsClientCertificateVerify
struct  TlsClientCertificateVerify_t1FA0AB8DF4EAF73FDD4473C5B04AF978CC3B0854  : public HandshakeMessage_t9B97FBAE0BAFCEA052F2B5EA880817CA1345298C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSCLIENTCERTIFICATEVERIFY_T1FA0AB8DF4EAF73FDD4473C5B04AF978CC3B0854_H
#ifndef TLSCLIENTFINISHED_T6709F9BFEE17B5E01FC80A408C4BA0DED6238936_H
#define TLSCLIENTFINISHED_T6709F9BFEE17B5E01FC80A408C4BA0DED6238936_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.Handshake.Server.TlsClientFinished
struct  TlsClientFinished_t6709F9BFEE17B5E01FC80A408C4BA0DED6238936  : public HandshakeMessage_t9B97FBAE0BAFCEA052F2B5EA880817CA1345298C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSCLIENTFINISHED_T6709F9BFEE17B5E01FC80A408C4BA0DED6238936_H
#ifndef TLSCLIENTHELLO_T1C3D09C861EE366065E07E94DD179D8A59656A72_H
#define TLSCLIENTHELLO_T1C3D09C861EE366065E07E94DD179D8A59656A72_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.Handshake.Server.TlsClientHello
struct  TlsClientHello_t1C3D09C861EE366065E07E94DD179D8A59656A72  : public HandshakeMessage_t9B97FBAE0BAFCEA052F2B5EA880817CA1345298C
{
public:
	// System.Byte[] Mono.Security.Protocol.Tls.Handshake.Server.TlsClientHello::random
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___random_14;
	// System.Byte[] Mono.Security.Protocol.Tls.Handshake.Server.TlsClientHello::sessionId
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___sessionId_15;
	// System.Int16[] Mono.Security.Protocol.Tls.Handshake.Server.TlsClientHello::cipherSuites
	Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* ___cipherSuites_16;
	// System.Byte[] Mono.Security.Protocol.Tls.Handshake.Server.TlsClientHello::compressionMethods
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___compressionMethods_17;

public:
	inline static int32_t get_offset_of_random_14() { return static_cast<int32_t>(offsetof(TlsClientHello_t1C3D09C861EE366065E07E94DD179D8A59656A72, ___random_14)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_random_14() const { return ___random_14; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_random_14() { return &___random_14; }
	inline void set_random_14(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___random_14 = value;
		Il2CppCodeGenWriteBarrier((&___random_14), value);
	}

	inline static int32_t get_offset_of_sessionId_15() { return static_cast<int32_t>(offsetof(TlsClientHello_t1C3D09C861EE366065E07E94DD179D8A59656A72, ___sessionId_15)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_sessionId_15() const { return ___sessionId_15; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_sessionId_15() { return &___sessionId_15; }
	inline void set_sessionId_15(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___sessionId_15 = value;
		Il2CppCodeGenWriteBarrier((&___sessionId_15), value);
	}

	inline static int32_t get_offset_of_cipherSuites_16() { return static_cast<int32_t>(offsetof(TlsClientHello_t1C3D09C861EE366065E07E94DD179D8A59656A72, ___cipherSuites_16)); }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* get_cipherSuites_16() const { return ___cipherSuites_16; }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28** get_address_of_cipherSuites_16() { return &___cipherSuites_16; }
	inline void set_cipherSuites_16(Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* value)
	{
		___cipherSuites_16 = value;
		Il2CppCodeGenWriteBarrier((&___cipherSuites_16), value);
	}

	inline static int32_t get_offset_of_compressionMethods_17() { return static_cast<int32_t>(offsetof(TlsClientHello_t1C3D09C861EE366065E07E94DD179D8A59656A72, ___compressionMethods_17)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_compressionMethods_17() const { return ___compressionMethods_17; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_compressionMethods_17() { return &___compressionMethods_17; }
	inline void set_compressionMethods_17(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___compressionMethods_17 = value;
		Il2CppCodeGenWriteBarrier((&___compressionMethods_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSCLIENTHELLO_T1C3D09C861EE366065E07E94DD179D8A59656A72_H
#ifndef TLSCLIENTKEYEXCHANGE_T62C1953E2EB30E724026247CF1D5FC4F542780A1_H
#define TLSCLIENTKEYEXCHANGE_T62C1953E2EB30E724026247CF1D5FC4F542780A1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.Handshake.Server.TlsClientKeyExchange
struct  TlsClientKeyExchange_t62C1953E2EB30E724026247CF1D5FC4F542780A1  : public HandshakeMessage_t9B97FBAE0BAFCEA052F2B5EA880817CA1345298C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSCLIENTKEYEXCHANGE_T62C1953E2EB30E724026247CF1D5FC4F542780A1_H
#ifndef TLSSERVERCERTIFICATE_T3ABDCD8FE26DB00EF291B8A06DC9A239EB617C9F_H
#define TLSSERVERCERTIFICATE_T3ABDCD8FE26DB00EF291B8A06DC9A239EB617C9F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.Handshake.Server.TlsServerCertificate
struct  TlsServerCertificate_t3ABDCD8FE26DB00EF291B8A06DC9A239EB617C9F  : public HandshakeMessage_t9B97FBAE0BAFCEA052F2B5EA880817CA1345298C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSSERVERCERTIFICATE_T3ABDCD8FE26DB00EF291B8A06DC9A239EB617C9F_H
#ifndef TLSSERVERCERTIFICATEREQUEST_T597768CD7EEC450288F74EFABFB52085EDB6EABE_H
#define TLSSERVERCERTIFICATEREQUEST_T597768CD7EEC450288F74EFABFB52085EDB6EABE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.Handshake.Server.TlsServerCertificateRequest
struct  TlsServerCertificateRequest_t597768CD7EEC450288F74EFABFB52085EDB6EABE  : public HandshakeMessage_t9B97FBAE0BAFCEA052F2B5EA880817CA1345298C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSSERVERCERTIFICATEREQUEST_T597768CD7EEC450288F74EFABFB52085EDB6EABE_H
#ifndef TLSSERVERFINISHED_T25C5A1F96AA9CEFB6023F82AE758931628B3354B_H
#define TLSSERVERFINISHED_T25C5A1F96AA9CEFB6023F82AE758931628B3354B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.Handshake.Server.TlsServerFinished
struct  TlsServerFinished_t25C5A1F96AA9CEFB6023F82AE758931628B3354B  : public HandshakeMessage_t9B97FBAE0BAFCEA052F2B5EA880817CA1345298C
{
public:

public:
};

struct TlsServerFinished_t25C5A1F96AA9CEFB6023F82AE758931628B3354B_StaticFields
{
public:
	// System.Byte[] Mono.Security.Protocol.Tls.Handshake.Server.TlsServerFinished::Ssl3Marker
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___Ssl3Marker_14;

public:
	inline static int32_t get_offset_of_Ssl3Marker_14() { return static_cast<int32_t>(offsetof(TlsServerFinished_t25C5A1F96AA9CEFB6023F82AE758931628B3354B_StaticFields, ___Ssl3Marker_14)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_Ssl3Marker_14() const { return ___Ssl3Marker_14; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_Ssl3Marker_14() { return &___Ssl3Marker_14; }
	inline void set_Ssl3Marker_14(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___Ssl3Marker_14 = value;
		Il2CppCodeGenWriteBarrier((&___Ssl3Marker_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSSERVERFINISHED_T25C5A1F96AA9CEFB6023F82AE758931628B3354B_H
#ifndef TLSSERVERHELLO_T6D7DD6CD1FE495775D62CAE27A6ED8D9547E1DE6_H
#define TLSSERVERHELLO_T6D7DD6CD1FE495775D62CAE27A6ED8D9547E1DE6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.Handshake.Server.TlsServerHello
struct  TlsServerHello_t6D7DD6CD1FE495775D62CAE27A6ED8D9547E1DE6  : public HandshakeMessage_t9B97FBAE0BAFCEA052F2B5EA880817CA1345298C
{
public:
	// System.Int32 Mono.Security.Protocol.Tls.Handshake.Server.TlsServerHello::unixTime
	int32_t ___unixTime_14;
	// System.Byte[] Mono.Security.Protocol.Tls.Handshake.Server.TlsServerHello::random
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___random_15;

public:
	inline static int32_t get_offset_of_unixTime_14() { return static_cast<int32_t>(offsetof(TlsServerHello_t6D7DD6CD1FE495775D62CAE27A6ED8D9547E1DE6, ___unixTime_14)); }
	inline int32_t get_unixTime_14() const { return ___unixTime_14; }
	inline int32_t* get_address_of_unixTime_14() { return &___unixTime_14; }
	inline void set_unixTime_14(int32_t value)
	{
		___unixTime_14 = value;
	}

	inline static int32_t get_offset_of_random_15() { return static_cast<int32_t>(offsetof(TlsServerHello_t6D7DD6CD1FE495775D62CAE27A6ED8D9547E1DE6, ___random_15)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_random_15() const { return ___random_15; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_random_15() { return &___random_15; }
	inline void set_random_15(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___random_15 = value;
		Il2CppCodeGenWriteBarrier((&___random_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSSERVERHELLO_T6D7DD6CD1FE495775D62CAE27A6ED8D9547E1DE6_H
#ifndef TLSSERVERHELLODONE_TFD098E1C9CF27D120FDF856DA56D1FA0272D2C56_H
#define TLSSERVERHELLODONE_TFD098E1C9CF27D120FDF856DA56D1FA0272D2C56_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.Handshake.Server.TlsServerHelloDone
struct  TlsServerHelloDone_tFD098E1C9CF27D120FDF856DA56D1FA0272D2C56  : public HandshakeMessage_t9B97FBAE0BAFCEA052F2B5EA880817CA1345298C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSSERVERHELLODONE_TFD098E1C9CF27D120FDF856DA56D1FA0272D2C56_H
#ifndef TLSSERVERKEYEXCHANGE_T0BE2433D8F78113295F138B4883140B95D95D63D_H
#define TLSSERVERKEYEXCHANGE_T0BE2433D8F78113295F138B4883140B95D95D63D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.Handshake.Server.TlsServerKeyExchange
struct  TlsServerKeyExchange_t0BE2433D8F78113295F138B4883140B95D95D63D  : public HandshakeMessage_t9B97FBAE0BAFCEA052F2B5EA880817CA1345298C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSSERVERKEYEXCHANGE_T0BE2433D8F78113295F138B4883140B95D95D63D_H
#ifndef HTTPSCLIENTSTREAM_TBF1258ECA9726BE12C94DD2B9A7F33C6004F65E3_H
#define HTTPSCLIENTSTREAM_TBF1258ECA9726BE12C94DD2B9A7F33C6004F65E3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.HttpsClientStream
struct  HttpsClientStream_tBF1258ECA9726BE12C94DD2B9A7F33C6004F65E3  : public SslClientStream_t58595BA5B794C492855BB93B01C5046E1EE7263B
{
public:
	// System.Net.HttpWebRequest Mono.Security.Protocol.Tls.HttpsClientStream::_request
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0 * ____request_23;
	// System.Int32 Mono.Security.Protocol.Tls.HttpsClientStream::_status
	int32_t ____status_24;

public:
	inline static int32_t get_offset_of__request_23() { return static_cast<int32_t>(offsetof(HttpsClientStream_tBF1258ECA9726BE12C94DD2B9A7F33C6004F65E3, ____request_23)); }
	inline HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0 * get__request_23() const { return ____request_23; }
	inline HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0 ** get_address_of__request_23() { return &____request_23; }
	inline void set__request_23(HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0 * value)
	{
		____request_23 = value;
		Il2CppCodeGenWriteBarrier((&____request_23), value);
	}

	inline static int32_t get_offset_of__status_24() { return static_cast<int32_t>(offsetof(HttpsClientStream_tBF1258ECA9726BE12C94DD2B9A7F33C6004F65E3, ____status_24)); }
	inline int32_t get__status_24() const { return ____status_24; }
	inline int32_t* get_address_of__status_24() { return &____status_24; }
	inline void set__status_24(int32_t value)
	{
		____status_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPSCLIENTSTREAM_TBF1258ECA9726BE12C94DD2B9A7F33C6004F65E3_H
#ifndef PRIVATEKEYSELECTIONCALLBACK_T403FB7EE7A49085BEDF77A79C77652CB5C610F7C_H
#define PRIVATEKEYSELECTIONCALLBACK_T403FB7EE7A49085BEDF77A79C77652CB5C610F7C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.PrivateKeySelectionCallback
struct  PrivateKeySelectionCallback_t403FB7EE7A49085BEDF77A79C77652CB5C610F7C  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRIVATEKEYSELECTIONCALLBACK_T403FB7EE7A49085BEDF77A79C77652CB5C610F7C_H
#ifndef SERVERCONTEXT_TD3EDDFF1E3B38E0C470E949B122B87AF47E681DD_H
#define SERVERCONTEXT_TD3EDDFF1E3B38E0C470E949B122B87AF47E681DD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.ServerContext
struct  ServerContext_tD3EDDFF1E3B38E0C470E949B122B87AF47E681DD  : public Context_t17219BB52552BCACB15DAF9B6618835642F7F56F
{
public:
	// Mono.Security.Protocol.Tls.SslServerStream Mono.Security.Protocol.Tls.ServerContext::sslStream
	SslServerStream_t424CB4AC3D421FAF8A73A20520E3E4A4934E2092 * ___sslStream_35;
	// System.Boolean Mono.Security.Protocol.Tls.ServerContext::request_client_certificate
	bool ___request_client_certificate_36;
	// System.Boolean Mono.Security.Protocol.Tls.ServerContext::clientCertificateRequired
	bool ___clientCertificateRequired_37;

public:
	inline static int32_t get_offset_of_sslStream_35() { return static_cast<int32_t>(offsetof(ServerContext_tD3EDDFF1E3B38E0C470E949B122B87AF47E681DD, ___sslStream_35)); }
	inline SslServerStream_t424CB4AC3D421FAF8A73A20520E3E4A4934E2092 * get_sslStream_35() const { return ___sslStream_35; }
	inline SslServerStream_t424CB4AC3D421FAF8A73A20520E3E4A4934E2092 ** get_address_of_sslStream_35() { return &___sslStream_35; }
	inline void set_sslStream_35(SslServerStream_t424CB4AC3D421FAF8A73A20520E3E4A4934E2092 * value)
	{
		___sslStream_35 = value;
		Il2CppCodeGenWriteBarrier((&___sslStream_35), value);
	}

	inline static int32_t get_offset_of_request_client_certificate_36() { return static_cast<int32_t>(offsetof(ServerContext_tD3EDDFF1E3B38E0C470E949B122B87AF47E681DD, ___request_client_certificate_36)); }
	inline bool get_request_client_certificate_36() const { return ___request_client_certificate_36; }
	inline bool* get_address_of_request_client_certificate_36() { return &___request_client_certificate_36; }
	inline void set_request_client_certificate_36(bool value)
	{
		___request_client_certificate_36 = value;
	}

	inline static int32_t get_offset_of_clientCertificateRequired_37() { return static_cast<int32_t>(offsetof(ServerContext_tD3EDDFF1E3B38E0C470E949B122B87AF47E681DD, ___clientCertificateRequired_37)); }
	inline bool get_clientCertificateRequired_37() const { return ___clientCertificateRequired_37; }
	inline bool* get_address_of_clientCertificateRequired_37() { return &___clientCertificateRequired_37; }
	inline void set_clientCertificateRequired_37(bool value)
	{
		___clientCertificateRequired_37 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERVERCONTEXT_TD3EDDFF1E3B38E0C470E949B122B87AF47E681DD_H
#ifndef SSLCIPHERSUITE_TD2E888FCB7837E83012F988D2E3FED646735B224_H
#define SSLCIPHERSUITE_TD2E888FCB7837E83012F988D2E3FED646735B224_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.SslCipherSuite
struct  SslCipherSuite_tD2E888FCB7837E83012F988D2E3FED646735B224  : public CipherSuite_t203D2B4A797805B775BEFB3B623962E7B407B6F7
{
public:
	// System.Byte[] Mono.Security.Protocol.Tls.SslCipherSuite::pad1
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___pad1_21;
	// System.Byte[] Mono.Security.Protocol.Tls.SslCipherSuite::pad2
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___pad2_22;
	// System.Byte[] Mono.Security.Protocol.Tls.SslCipherSuite::header
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___header_24;

public:
	inline static int32_t get_offset_of_pad1_21() { return static_cast<int32_t>(offsetof(SslCipherSuite_tD2E888FCB7837E83012F988D2E3FED646735B224, ___pad1_21)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_pad1_21() const { return ___pad1_21; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_pad1_21() { return &___pad1_21; }
	inline void set_pad1_21(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___pad1_21 = value;
		Il2CppCodeGenWriteBarrier((&___pad1_21), value);
	}

	inline static int32_t get_offset_of_pad2_22() { return static_cast<int32_t>(offsetof(SslCipherSuite_tD2E888FCB7837E83012F988D2E3FED646735B224, ___pad2_22)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_pad2_22() const { return ___pad2_22; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_pad2_22() { return &___pad2_22; }
	inline void set_pad2_22(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___pad2_22 = value;
		Il2CppCodeGenWriteBarrier((&___pad2_22), value);
	}

	inline static int32_t get_offset_of_header_24() { return static_cast<int32_t>(offsetof(SslCipherSuite_tD2E888FCB7837E83012F988D2E3FED646735B224, ___header_24)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_header_24() const { return ___header_24; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_header_24() { return &___header_24; }
	inline void set_header_24(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___header_24 = value;
		Il2CppCodeGenWriteBarrier((&___header_24), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SSLCIPHERSUITE_TD2E888FCB7837E83012F988D2E3FED646735B224_H
#ifndef ASYNCHANDSHAKEDELEGATE_T4944C99180A0E6C1BCB2D911C1AD0003C561F4D5_H
#define ASYNCHANDSHAKEDELEGATE_T4944C99180A0E6C1BCB2D911C1AD0003C561F4D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.SslStreamBase_AsyncHandshakeDelegate
struct  AsyncHandshakeDelegate_t4944C99180A0E6C1BCB2D911C1AD0003C561F4D5  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCHANDSHAKEDELEGATE_T4944C99180A0E6C1BCB2D911C1AD0003C561F4D5_H
#ifndef TLSCIPHERSUITE_T3A10CE7884F410B9D16F81E8E89FE5CCC33E9283_H
#define TLSCIPHERSUITE_T3A10CE7884F410B9D16F81E8E89FE5CCC33E9283_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.TlsCipherSuite
struct  TlsCipherSuite_t3A10CE7884F410B9D16F81E8E89FE5CCC33E9283  : public CipherSuite_t203D2B4A797805B775BEFB3B623962E7B407B6F7
{
public:
	// System.Byte[] Mono.Security.Protocol.Tls.TlsCipherSuite::header
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___header_22;
	// System.Object Mono.Security.Protocol.Tls.TlsCipherSuite::headerLock
	RuntimeObject * ___headerLock_23;

public:
	inline static int32_t get_offset_of_header_22() { return static_cast<int32_t>(offsetof(TlsCipherSuite_t3A10CE7884F410B9D16F81E8E89FE5CCC33E9283, ___header_22)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_header_22() const { return ___header_22; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_header_22() { return &___header_22; }
	inline void set_header_22(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___header_22 = value;
		Il2CppCodeGenWriteBarrier((&___header_22), value);
	}

	inline static int32_t get_offset_of_headerLock_23() { return static_cast<int32_t>(offsetof(TlsCipherSuite_t3A10CE7884F410B9D16F81E8E89FE5CCC33E9283, ___headerLock_23)); }
	inline RuntimeObject * get_headerLock_23() const { return ___headerLock_23; }
	inline RuntimeObject ** get_address_of_headerLock_23() { return &___headerLock_23; }
	inline void set_headerLock_23(RuntimeObject * value)
	{
		___headerLock_23 = value;
		Il2CppCodeGenWriteBarrier((&___headerLock_23), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSCIPHERSUITE_T3A10CE7884F410B9D16F81E8E89FE5CCC33E9283_H
#ifndef ARC4MANAGED_T03DC2C93774F0A3652F03EFA78A0431AB664F397_H
#define ARC4MANAGED_T03DC2C93774F0A3652F03EFA78A0431AB664F397_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Cryptography.ARC4Managed
struct  ARC4Managed_t03DC2C93774F0A3652F03EFA78A0431AB664F397  : public RC4_tC03B8F8797476E53DD785D576E7DE1951667A54B
{
public:
	// System.Byte[] Mono.Security.Cryptography.ARC4Managed::key
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___key_11;
	// System.Byte[] Mono.Security.Cryptography.ARC4Managed::state
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___state_12;
	// System.Byte Mono.Security.Cryptography.ARC4Managed::x
	uint8_t ___x_13;
	// System.Byte Mono.Security.Cryptography.ARC4Managed::y
	uint8_t ___y_14;
	// System.Boolean Mono.Security.Cryptography.ARC4Managed::m_disposed
	bool ___m_disposed_15;

public:
	inline static int32_t get_offset_of_key_11() { return static_cast<int32_t>(offsetof(ARC4Managed_t03DC2C93774F0A3652F03EFA78A0431AB664F397, ___key_11)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_key_11() const { return ___key_11; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_key_11() { return &___key_11; }
	inline void set_key_11(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___key_11 = value;
		Il2CppCodeGenWriteBarrier((&___key_11), value);
	}

	inline static int32_t get_offset_of_state_12() { return static_cast<int32_t>(offsetof(ARC4Managed_t03DC2C93774F0A3652F03EFA78A0431AB664F397, ___state_12)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_state_12() const { return ___state_12; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_state_12() { return &___state_12; }
	inline void set_state_12(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___state_12 = value;
		Il2CppCodeGenWriteBarrier((&___state_12), value);
	}

	inline static int32_t get_offset_of_x_13() { return static_cast<int32_t>(offsetof(ARC4Managed_t03DC2C93774F0A3652F03EFA78A0431AB664F397, ___x_13)); }
	inline uint8_t get_x_13() const { return ___x_13; }
	inline uint8_t* get_address_of_x_13() { return &___x_13; }
	inline void set_x_13(uint8_t value)
	{
		___x_13 = value;
	}

	inline static int32_t get_offset_of_y_14() { return static_cast<int32_t>(offsetof(ARC4Managed_t03DC2C93774F0A3652F03EFA78A0431AB664F397, ___y_14)); }
	inline uint8_t get_y_14() const { return ___y_14; }
	inline uint8_t* get_address_of_y_14() { return &___y_14; }
	inline void set_y_14(uint8_t value)
	{
		___y_14 = value;
	}

	inline static int32_t get_offset_of_m_disposed_15() { return static_cast<int32_t>(offsetof(ARC4Managed_t03DC2C93774F0A3652F03EFA78A0431AB664F397, ___m_disposed_15)); }
	inline bool get_m_disposed_15() const { return ___m_disposed_15; }
	inline bool* get_address_of_m_disposed_15() { return &___m_disposed_15; }
	inline void set_m_disposed_15(bool value)
	{
		___m_disposed_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARC4MANAGED_T03DC2C93774F0A3652F03EFA78A0431AB664F397_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2200 = { sizeof (ContentType_t27CB29E77D75A61C8BD752E08D401276B3E3BA21)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2200[5] = 
{
	ContentType_t27CB29E77D75A61C8BD752E08D401276B3E3BA21::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2201 = { sizeof (Context_t17219BB52552BCACB15DAF9B6618835642F7F56F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2201[35] = 
{
	0,
	0,
	0,
	0,
	Context_t17219BB52552BCACB15DAF9B6618835642F7F56F::get_offset_of_securityProtocol_4(),
	Context_t17219BB52552BCACB15DAF9B6618835642F7F56F::get_offset_of_sessionId_5(),
	Context_t17219BB52552BCACB15DAF9B6618835642F7F56F::get_offset_of_compressionMethod_6(),
	Context_t17219BB52552BCACB15DAF9B6618835642F7F56F::get_offset_of_serverSettings_7(),
	Context_t17219BB52552BCACB15DAF9B6618835642F7F56F::get_offset_of_clientSettings_8(),
	Context_t17219BB52552BCACB15DAF9B6618835642F7F56F::get_offset_of_current_9(),
	Context_t17219BB52552BCACB15DAF9B6618835642F7F56F::get_offset_of_negotiating_10(),
	Context_t17219BB52552BCACB15DAF9B6618835642F7F56F::get_offset_of_read_11(),
	Context_t17219BB52552BCACB15DAF9B6618835642F7F56F::get_offset_of_write_12(),
	Context_t17219BB52552BCACB15DAF9B6618835642F7F56F::get_offset_of_supportedCiphers_13(),
	Context_t17219BB52552BCACB15DAF9B6618835642F7F56F::get_offset_of_lastHandshakeMsg_14(),
	Context_t17219BB52552BCACB15DAF9B6618835642F7F56F::get_offset_of_handshakeState_15(),
	Context_t17219BB52552BCACB15DAF9B6618835642F7F56F::get_offset_of_abbreviatedHandshake_16(),
	Context_t17219BB52552BCACB15DAF9B6618835642F7F56F::get_offset_of_receivedConnectionEnd_17(),
	Context_t17219BB52552BCACB15DAF9B6618835642F7F56F::get_offset_of_sentConnectionEnd_18(),
	Context_t17219BB52552BCACB15DAF9B6618835642F7F56F::get_offset_of_protocolNegotiated_19(),
	Context_t17219BB52552BCACB15DAF9B6618835642F7F56F::get_offset_of_writeSequenceNumber_20(),
	Context_t17219BB52552BCACB15DAF9B6618835642F7F56F::get_offset_of_readSequenceNumber_21(),
	Context_t17219BB52552BCACB15DAF9B6618835642F7F56F::get_offset_of_clientRandom_22(),
	Context_t17219BB52552BCACB15DAF9B6618835642F7F56F::get_offset_of_serverRandom_23(),
	Context_t17219BB52552BCACB15DAF9B6618835642F7F56F::get_offset_of_randomCS_24(),
	Context_t17219BB52552BCACB15DAF9B6618835642F7F56F::get_offset_of_randomSC_25(),
	Context_t17219BB52552BCACB15DAF9B6618835642F7F56F::get_offset_of_masterSecret_26(),
	Context_t17219BB52552BCACB15DAF9B6618835642F7F56F::get_offset_of_clientWriteKey_27(),
	Context_t17219BB52552BCACB15DAF9B6618835642F7F56F::get_offset_of_serverWriteKey_28(),
	Context_t17219BB52552BCACB15DAF9B6618835642F7F56F::get_offset_of_clientWriteIV_29(),
	Context_t17219BB52552BCACB15DAF9B6618835642F7F56F::get_offset_of_serverWriteIV_30(),
	Context_t17219BB52552BCACB15DAF9B6618835642F7F56F::get_offset_of_handshakeMessages_31(),
	Context_t17219BB52552BCACB15DAF9B6618835642F7F56F::get_offset_of_random_32(),
	Context_t17219BB52552BCACB15DAF9B6618835642F7F56F::get_offset_of_recordProtocol_33(),
	Context_t17219BB52552BCACB15DAF9B6618835642F7F56F::get_offset_of_U3CChangeCipherSpecDoneU3Ek__BackingField_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2202 = { sizeof (DebugHelper_tC0F42268BBF77BF578E27BD7458075BC06639521), -1, sizeof(DebugHelper_tC0F42268BBF77BF578E27BD7458075BC06639521_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2202[1] = 
{
	DebugHelper_tC0F42268BBF77BF578E27BD7458075BC06639521_StaticFields::get_offset_of_isInitialized_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2203 = { sizeof (ExchangeAlgorithmType_tB2361714C59E334FFB4A81129D72E241702E281B)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2203[6] = 
{
	ExchangeAlgorithmType_tB2361714C59E334FFB4A81129D72E241702E281B::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2204 = { sizeof (HandshakeState_tCABEB477DC355D6A3332D7864BB8FF6D0F8F03C3)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2204[4] = 
{
	HandshakeState_tCABEB477DC355D6A3332D7864BB8FF6D0F8F03C3::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2205 = { sizeof (HashAlgorithmType_t59C53DD6764DE8CE761AAB0C0A2A12C0C651C79D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2205[4] = 
{
	HashAlgorithmType_t59C53DD6764DE8CE761AAB0C0A2A12C0C651C79D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2206 = { sizeof (HttpsClientStream_tBF1258ECA9726BE12C94DD2B9A7F33C6004F65E3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2206[2] = 
{
	HttpsClientStream_tBF1258ECA9726BE12C94DD2B9A7F33C6004F65E3::get_offset_of__request_23(),
	HttpsClientStream_tBF1258ECA9726BE12C94DD2B9A7F33C6004F65E3::get_offset_of__status_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2207 = { sizeof (U3CU3Ec_t5B194E5166B48CF5C8F9F038AABA64D57F961749), -1, sizeof(U3CU3Ec_t5B194E5166B48CF5C8F9F038AABA64D57F961749_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2207[3] = 
{
	U3CU3Ec_t5B194E5166B48CF5C8F9F038AABA64D57F961749_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t5B194E5166B48CF5C8F9F038AABA64D57F961749_StaticFields::get_offset_of_U3CU3E9__2_0_1(),
	U3CU3Ec_t5B194E5166B48CF5C8F9F038AABA64D57F961749_StaticFields::get_offset_of_U3CU3E9__2_1_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2208 = { sizeof (RSASslSignatureDeformatter_tE9EE489E26042B81563771B31853E66BDC5859DA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2208[2] = 
{
	RSASslSignatureDeformatter_tE9EE489E26042B81563771B31853E66BDC5859DA::get_offset_of_key_0(),
	RSASslSignatureDeformatter_tE9EE489E26042B81563771B31853E66BDC5859DA::get_offset_of_hash_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2209 = { sizeof (RSASslSignatureFormatter_t21ABEFCB144E30953E30A458612D1F4E021470E2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2209[2] = 
{
	RSASslSignatureFormatter_t21ABEFCB144E30953E30A458612D1F4E021470E2::get_offset_of_key_0(),
	RSASslSignatureFormatter_t21ABEFCB144E30953E30A458612D1F4E021470E2::get_offset_of_hash_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2210 = { sizeof (RecordProtocol_tFA651C8422AECEEE03AABEDB029EC83EB8833CFA), -1, sizeof(RecordProtocol_tFA651C8422AECEEE03AABEDB029EC83EB8833CFA_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2210[3] = 
{
	RecordProtocol_tFA651C8422AECEEE03AABEDB029EC83EB8833CFA_StaticFields::get_offset_of_record_processing_0(),
	RecordProtocol_tFA651C8422AECEEE03AABEDB029EC83EB8833CFA::get_offset_of_innerStream_1(),
	RecordProtocol_tFA651C8422AECEEE03AABEDB029EC83EB8833CFA::get_offset_of_context_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2211 = { sizeof (ReceiveRecordAsyncResult_tF2E4546C7F2475C74BE9023741AFFA069FC68752), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2211[9] = 
{
	ReceiveRecordAsyncResult_tF2E4546C7F2475C74BE9023741AFFA069FC68752::get_offset_of_locker_0(),
	ReceiveRecordAsyncResult_tF2E4546C7F2475C74BE9023741AFFA069FC68752::get_offset_of__userCallback_1(),
	ReceiveRecordAsyncResult_tF2E4546C7F2475C74BE9023741AFFA069FC68752::get_offset_of__userState_2(),
	ReceiveRecordAsyncResult_tF2E4546C7F2475C74BE9023741AFFA069FC68752::get_offset_of__asyncException_3(),
	ReceiveRecordAsyncResult_tF2E4546C7F2475C74BE9023741AFFA069FC68752::get_offset_of_handle_4(),
	ReceiveRecordAsyncResult_tF2E4546C7F2475C74BE9023741AFFA069FC68752::get_offset_of__resultingBuffer_5(),
	ReceiveRecordAsyncResult_tF2E4546C7F2475C74BE9023741AFFA069FC68752::get_offset_of__record_6(),
	ReceiveRecordAsyncResult_tF2E4546C7F2475C74BE9023741AFFA069FC68752::get_offset_of_completed_7(),
	ReceiveRecordAsyncResult_tF2E4546C7F2475C74BE9023741AFFA069FC68752::get_offset_of__initialBuffer_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2212 = { sizeof (SendRecordAsyncResult_t43EF2777DAD8ED4C954CAF42151559D362357DC9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2212[7] = 
{
	SendRecordAsyncResult_t43EF2777DAD8ED4C954CAF42151559D362357DC9::get_offset_of_locker_0(),
	SendRecordAsyncResult_t43EF2777DAD8ED4C954CAF42151559D362357DC9::get_offset_of__userCallback_1(),
	SendRecordAsyncResult_t43EF2777DAD8ED4C954CAF42151559D362357DC9::get_offset_of__userState_2(),
	SendRecordAsyncResult_t43EF2777DAD8ED4C954CAF42151559D362357DC9::get_offset_of__asyncException_3(),
	SendRecordAsyncResult_t43EF2777DAD8ED4C954CAF42151559D362357DC9::get_offset_of_handle_4(),
	SendRecordAsyncResult_t43EF2777DAD8ED4C954CAF42151559D362357DC9::get_offset_of__message_5(),
	SendRecordAsyncResult_t43EF2777DAD8ED4C954CAF42151559D362357DC9::get_offset_of_completed_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2213 = { sizeof (SecurityCompressionType_t2D26DB7EEE0BA687EE1DD3C515EF405BB44A9650)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2213[3] = 
{
	SecurityCompressionType_t2D26DB7EEE0BA687EE1DD3C515EF405BB44A9650::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2214 = { sizeof (SecurityParameters_tC07B6E750AFC2037A7EBC754BD4DC1D37F41585E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2214[3] = 
{
	SecurityParameters_tC07B6E750AFC2037A7EBC754BD4DC1D37F41585E::get_offset_of_cipher_0(),
	SecurityParameters_tC07B6E750AFC2037A7EBC754BD4DC1D37F41585E::get_offset_of_clientWriteMAC_1(),
	SecurityParameters_tC07B6E750AFC2037A7EBC754BD4DC1D37F41585E::get_offset_of_serverWriteMAC_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2215 = { sizeof (SecurityProtocolType_tF81DFFC385530B08E8E214306D65F2C08537DF4C)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2215[7] = 
{
	SecurityProtocolType_tF81DFFC385530B08E8E214306D65F2C08537DF4C::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2216 = { sizeof (ServerContext_tD3EDDFF1E3B38E0C470E949B122B87AF47E681DD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2216[3] = 
{
	ServerContext_tD3EDDFF1E3B38E0C470E949B122B87AF47E681DD::get_offset_of_sslStream_35(),
	ServerContext_tD3EDDFF1E3B38E0C470E949B122B87AF47E681DD::get_offset_of_request_client_certificate_36(),
	ServerContext_tD3EDDFF1E3B38E0C470E949B122B87AF47E681DD::get_offset_of_clientCertificateRequired_37(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2217 = { sizeof (ServerRecordProtocol_t4B4908008D4580AAAC5A433442A7334A921323CA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2217[1] = 
{
	ServerRecordProtocol_t4B4908008D4580AAAC5A433442A7334A921323CA::get_offset_of_cert_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2218 = { sizeof (SslCipherSuite_tD2E888FCB7837E83012F988D2E3FED646735B224), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2218[4] = 
{
	SslCipherSuite_tD2E888FCB7837E83012F988D2E3FED646735B224::get_offset_of_pad1_21(),
	SslCipherSuite_tD2E888FCB7837E83012F988D2E3FED646735B224::get_offset_of_pad2_22(),
	0,
	SslCipherSuite_tD2E888FCB7837E83012F988D2E3FED646735B224::get_offset_of_header_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2219 = { sizeof (CertificateValidationCallback_t4FDC57D3B9528BD8D0564B4B3B2CF135E9CD72D3), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2220 = { sizeof (CertificateValidationCallback2_t80B8CBB56C7605C91427868F9DE72B29837CCD61), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2221 = { sizeof (CertificateSelectionCallback_t2D981EF7A74CC271A9F086BB0F483FD6BCEDF5BE), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2222 = { sizeof (PrivateKeySelectionCallback_t403FB7EE7A49085BEDF77A79C77652CB5C610F7C), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2223 = { sizeof (SslClientStream_t58595BA5B794C492855BB93B01C5046E1EE7263B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2223[4] = 
{
	SslClientStream_t58595BA5B794C492855BB93B01C5046E1EE7263B::get_offset_of_ServerCertValidation_19(),
	SslClientStream_t58595BA5B794C492855BB93B01C5046E1EE7263B::get_offset_of_ClientCertSelection_20(),
	SslClientStream_t58595BA5B794C492855BB93B01C5046E1EE7263B::get_offset_of_PrivateKeySelection_21(),
	SslClientStream_t58595BA5B794C492855BB93B01C5046E1EE7263B::get_offset_of_ServerCertValidation2_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2224 = { sizeof (NegotiateState_t44FFDA234900CC8B794FA72862A6D800C1AEFCCB)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2224[8] = 
{
	NegotiateState_t44FFDA234900CC8B794FA72862A6D800C1AEFCCB::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2225 = { sizeof (NegotiateAsyncResult_t7BACE93E41827A619157BB53BDC7596AA554F65E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2225[7] = 
{
	NegotiateAsyncResult_t7BACE93E41827A619157BB53BDC7596AA554F65E::get_offset_of_locker_0(),
	NegotiateAsyncResult_t7BACE93E41827A619157BB53BDC7596AA554F65E::get_offset_of__userCallback_1(),
	NegotiateAsyncResult_t7BACE93E41827A619157BB53BDC7596AA554F65E::get_offset_of__userState_2(),
	NegotiateAsyncResult_t7BACE93E41827A619157BB53BDC7596AA554F65E::get_offset_of__asyncException_3(),
	NegotiateAsyncResult_t7BACE93E41827A619157BB53BDC7596AA554F65E::get_offset_of_handle_4(),
	NegotiateAsyncResult_t7BACE93E41827A619157BB53BDC7596AA554F65E::get_offset_of__state_5(),
	NegotiateAsyncResult_t7BACE93E41827A619157BB53BDC7596AA554F65E::get_offset_of_completed_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2226 = { sizeof (SslHandshakeHash_t06FA22E13E67503A71F1F7F87E9E2DC8F839B39C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2226[8] = 
{
	SslHandshakeHash_t06FA22E13E67503A71F1F7F87E9E2DC8F839B39C::get_offset_of_md5_4(),
	SslHandshakeHash_t06FA22E13E67503A71F1F7F87E9E2DC8F839B39C::get_offset_of_sha_5(),
	SslHandshakeHash_t06FA22E13E67503A71F1F7F87E9E2DC8F839B39C::get_offset_of_hashing_6(),
	SslHandshakeHash_t06FA22E13E67503A71F1F7F87E9E2DC8F839B39C::get_offset_of_secret_7(),
	SslHandshakeHash_t06FA22E13E67503A71F1F7F87E9E2DC8F839B39C::get_offset_of_innerPadMD5_8(),
	SslHandshakeHash_t06FA22E13E67503A71F1F7F87E9E2DC8F839B39C::get_offset_of_outerPadMD5_9(),
	SslHandshakeHash_t06FA22E13E67503A71F1F7F87E9E2DC8F839B39C::get_offset_of_innerPadSHA_10(),
	SslHandshakeHash_t06FA22E13E67503A71F1F7F87E9E2DC8F839B39C::get_offset_of_outerPadSHA_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2227 = { sizeof (SslServerStream_t424CB4AC3D421FAF8A73A20520E3E4A4934E2092), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2227[3] = 
{
	SslServerStream_t424CB4AC3D421FAF8A73A20520E3E4A4934E2092::get_offset_of_ClientCertValidation_19(),
	SslServerStream_t424CB4AC3D421FAF8A73A20520E3E4A4934E2092::get_offset_of_PrivateKeySelection_20(),
	SslServerStream_t424CB4AC3D421FAF8A73A20520E3E4A4934E2092::get_offset_of_ClientCertValidation2_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2228 = { sizeof (SslStreamBase_tE9E16CABCE59BAA56CFFBD9B84C28335730E9197), -1, sizeof(SslStreamBase_tE9E16CABCE59BAA56CFFBD9B84C28335730E9197_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2228[14] = 
{
	SslStreamBase_tE9E16CABCE59BAA56CFFBD9B84C28335730E9197_StaticFields::get_offset_of_record_processing_5(),
	SslStreamBase_tE9E16CABCE59BAA56CFFBD9B84C28335730E9197::get_offset_of_innerStream_6(),
	SslStreamBase_tE9E16CABCE59BAA56CFFBD9B84C28335730E9197::get_offset_of_inputBuffer_7(),
	SslStreamBase_tE9E16CABCE59BAA56CFFBD9B84C28335730E9197::get_offset_of_context_8(),
	SslStreamBase_tE9E16CABCE59BAA56CFFBD9B84C28335730E9197::get_offset_of_protocol_9(),
	SslStreamBase_tE9E16CABCE59BAA56CFFBD9B84C28335730E9197::get_offset_of_ownsStream_10(),
	SslStreamBase_tE9E16CABCE59BAA56CFFBD9B84C28335730E9197::get_offset_of_disposed_11(),
	SslStreamBase_tE9E16CABCE59BAA56CFFBD9B84C28335730E9197::get_offset_of_checkCertRevocationStatus_12(),
	SslStreamBase_tE9E16CABCE59BAA56CFFBD9B84C28335730E9197::get_offset_of_negotiate_13(),
	SslStreamBase_tE9E16CABCE59BAA56CFFBD9B84C28335730E9197::get_offset_of_read_14(),
	SslStreamBase_tE9E16CABCE59BAA56CFFBD9B84C28335730E9197::get_offset_of_write_15(),
	SslStreamBase_tE9E16CABCE59BAA56CFFBD9B84C28335730E9197::get_offset_of_negotiationComplete_16(),
	SslStreamBase_tE9E16CABCE59BAA56CFFBD9B84C28335730E9197::get_offset_of_recbuf_17(),
	SslStreamBase_tE9E16CABCE59BAA56CFFBD9B84C28335730E9197::get_offset_of_recordStream_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2229 = { sizeof (AsyncHandshakeDelegate_t4944C99180A0E6C1BCB2D911C1AD0003C561F4D5), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2230 = { sizeof (InternalAsyncResult_tD29ADBE4ACE8353C13107D2E648FFA1F04DCC4C0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2230[12] = 
{
	InternalAsyncResult_tD29ADBE4ACE8353C13107D2E648FFA1F04DCC4C0::get_offset_of_locker_0(),
	InternalAsyncResult_tD29ADBE4ACE8353C13107D2E648FFA1F04DCC4C0::get_offset_of__userCallback_1(),
	InternalAsyncResult_tD29ADBE4ACE8353C13107D2E648FFA1F04DCC4C0::get_offset_of__userState_2(),
	InternalAsyncResult_tD29ADBE4ACE8353C13107D2E648FFA1F04DCC4C0::get_offset_of__asyncException_3(),
	InternalAsyncResult_tD29ADBE4ACE8353C13107D2E648FFA1F04DCC4C0::get_offset_of_handle_4(),
	InternalAsyncResult_tD29ADBE4ACE8353C13107D2E648FFA1F04DCC4C0::get_offset_of_completed_5(),
	InternalAsyncResult_tD29ADBE4ACE8353C13107D2E648FFA1F04DCC4C0::get_offset_of__bytesRead_6(),
	InternalAsyncResult_tD29ADBE4ACE8353C13107D2E648FFA1F04DCC4C0::get_offset_of__fromWrite_7(),
	InternalAsyncResult_tD29ADBE4ACE8353C13107D2E648FFA1F04DCC4C0::get_offset_of__proceedAfterHandshake_8(),
	InternalAsyncResult_tD29ADBE4ACE8353C13107D2E648FFA1F04DCC4C0::get_offset_of__buffer_9(),
	InternalAsyncResult_tD29ADBE4ACE8353C13107D2E648FFA1F04DCC4C0::get_offset_of__offset_10(),
	InternalAsyncResult_tD29ADBE4ACE8353C13107D2E648FFA1F04DCC4C0::get_offset_of__count_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2231 = { sizeof (TlsCipherSuite_t3A10CE7884F410B9D16F81E8E89FE5CCC33E9283), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2231[3] = 
{
	0,
	TlsCipherSuite_t3A10CE7884F410B9D16F81E8E89FE5CCC33E9283::get_offset_of_header_22(),
	TlsCipherSuite_t3A10CE7884F410B9D16F81E8E89FE5CCC33E9283::get_offset_of_headerLock_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2232 = { sizeof (TlsClientSettings_t7521F1D5285E4B38591EC5E724E198CED35B39E0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2232[4] = 
{
	TlsClientSettings_t7521F1D5285E4B38591EC5E724E198CED35B39E0::get_offset_of_targetHost_0(),
	TlsClientSettings_t7521F1D5285E4B38591EC5E724E198CED35B39E0::get_offset_of_certificates_1(),
	TlsClientSettings_t7521F1D5285E4B38591EC5E724E198CED35B39E0::get_offset_of_clientCertificate_2(),
	TlsClientSettings_t7521F1D5285E4B38591EC5E724E198CED35B39E0::get_offset_of_certificateRSA_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2233 = { sizeof (TlsException_t07E60982E4F11EE25F5AE7E91A4BA40EB1E72CD2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2233[1] = 
{
	TlsException_t07E60982E4F11EE25F5AE7E91A4BA40EB1E72CD2::get_offset_of_alert_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2234 = { sizeof (TlsServerSettings_tD19093EA2CFCCD92F9B0FE63A9B2FAA9B4B45788), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2234[8] = 
{
	TlsServerSettings_tD19093EA2CFCCD92F9B0FE63A9B2FAA9B4B45788::get_offset_of_certificates_0(),
	TlsServerSettings_tD19093EA2CFCCD92F9B0FE63A9B2FAA9B4B45788::get_offset_of_certificateRSA_1(),
	TlsServerSettings_tD19093EA2CFCCD92F9B0FE63A9B2FAA9B4B45788::get_offset_of_rsaParameters_2(),
	TlsServerSettings_tD19093EA2CFCCD92F9B0FE63A9B2FAA9B4B45788::get_offset_of_signedParams_3(),
	TlsServerSettings_tD19093EA2CFCCD92F9B0FE63A9B2FAA9B4B45788::get_offset_of_distinguisedNames_4(),
	TlsServerSettings_tD19093EA2CFCCD92F9B0FE63A9B2FAA9B4B45788::get_offset_of_serverKeyExchange_5(),
	TlsServerSettings_tD19093EA2CFCCD92F9B0FE63A9B2FAA9B4B45788::get_offset_of_certificateRequest_6(),
	TlsServerSettings_tD19093EA2CFCCD92F9B0FE63A9B2FAA9B4B45788::get_offset_of_certificateTypes_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2235 = { sizeof (TlsStream_tB739E33AAABEC563984AD1EA75D7F4CFB4BF27E5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2235[5] = 
{
	TlsStream_tB739E33AAABEC563984AD1EA75D7F4CFB4BF27E5::get_offset_of_canRead_5(),
	TlsStream_tB739E33AAABEC563984AD1EA75D7F4CFB4BF27E5::get_offset_of_canWrite_6(),
	TlsStream_tB739E33AAABEC563984AD1EA75D7F4CFB4BF27E5::get_offset_of_buffer_7(),
	TlsStream_tB739E33AAABEC563984AD1EA75D7F4CFB4BF27E5::get_offset_of_temp_8(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2236 = { sizeof (ClientCertificateType_t251B7570F285455294C40356733287B19A9452EA)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2236[6] = 
{
	ClientCertificateType_t251B7570F285455294C40356733287B19A9452EA::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2237 = { sizeof (HandshakeMessage_t9B97FBAE0BAFCEA052F2B5EA880817CA1345298C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2237[4] = 
{
	HandshakeMessage_t9B97FBAE0BAFCEA052F2B5EA880817CA1345298C::get_offset_of_context_10(),
	HandshakeMessage_t9B97FBAE0BAFCEA052F2B5EA880817CA1345298C::get_offset_of_handshakeType_11(),
	HandshakeMessage_t9B97FBAE0BAFCEA052F2B5EA880817CA1345298C::get_offset_of_contentType_12(),
	HandshakeMessage_t9B97FBAE0BAFCEA052F2B5EA880817CA1345298C::get_offset_of_cache_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2238 = { sizeof (HandshakeType_t398528545675E7ECA525E188AABAEDDCFD162EA2)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2238[12] = 
{
	HandshakeType_t398528545675E7ECA525E188AABAEDDCFD162EA2::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2239 = { sizeof (TlsClientCertificate_tDB7DEB656354449A78DCA4EC0EAD85EC4CC9712B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2239[1] = 
{
	TlsClientCertificate_tDB7DEB656354449A78DCA4EC0EAD85EC4CC9712B::get_offset_of_clientCertificates_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2240 = { sizeof (TlsClientCertificateVerify_t1FA0AB8DF4EAF73FDD4473C5B04AF978CC3B0854), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2241 = { sizeof (TlsClientFinished_t6709F9BFEE17B5E01FC80A408C4BA0DED6238936), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2242 = { sizeof (TlsClientHello_t1C3D09C861EE366065E07E94DD179D8A59656A72), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2242[4] = 
{
	TlsClientHello_t1C3D09C861EE366065E07E94DD179D8A59656A72::get_offset_of_random_14(),
	TlsClientHello_t1C3D09C861EE366065E07E94DD179D8A59656A72::get_offset_of_sessionId_15(),
	TlsClientHello_t1C3D09C861EE366065E07E94DD179D8A59656A72::get_offset_of_cipherSuites_16(),
	TlsClientHello_t1C3D09C861EE366065E07E94DD179D8A59656A72::get_offset_of_compressionMethods_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2243 = { sizeof (TlsClientKeyExchange_t62C1953E2EB30E724026247CF1D5FC4F542780A1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2244 = { sizeof (TlsServerCertificate_t3ABDCD8FE26DB00EF291B8A06DC9A239EB617C9F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2245 = { sizeof (TlsServerCertificateRequest_t597768CD7EEC450288F74EFABFB52085EDB6EABE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2246 = { sizeof (TlsServerFinished_t25C5A1F96AA9CEFB6023F82AE758931628B3354B), -1, sizeof(TlsServerFinished_t25C5A1F96AA9CEFB6023F82AE758931628B3354B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2246[1] = 
{
	TlsServerFinished_t25C5A1F96AA9CEFB6023F82AE758931628B3354B_StaticFields::get_offset_of_Ssl3Marker_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2247 = { sizeof (TlsServerHello_t6D7DD6CD1FE495775D62CAE27A6ED8D9547E1DE6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2247[2] = 
{
	TlsServerHello_t6D7DD6CD1FE495775D62CAE27A6ED8D9547E1DE6::get_offset_of_unixTime_14(),
	TlsServerHello_t6D7DD6CD1FE495775D62CAE27A6ED8D9547E1DE6::get_offset_of_random_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2248 = { sizeof (TlsServerHelloDone_tFD098E1C9CF27D120FDF856DA56D1FA0272D2C56), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2249 = { sizeof (TlsServerKeyExchange_t0BE2433D8F78113295F138B4883140B95D95D63D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2250 = { sizeof (TlsClientCertificate_t03B550B55DF5FC35774F3B5077E576367DFB16C2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2250[2] = 
{
	TlsClientCertificate_t03B550B55DF5FC35774F3B5077E576367DFB16C2::get_offset_of_clientCertSelected_14(),
	TlsClientCertificate_t03B550B55DF5FC35774F3B5077E576367DFB16C2::get_offset_of_clientCert_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2251 = { sizeof (TlsClientCertificateVerify_t7403133EF8E01827A1B2D4F0C987C7927FABAEC5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2252 = { sizeof (TlsClientFinished_tDE6377D436615E994372DEC7CD1FF034EA610A90), -1, sizeof(TlsClientFinished_tDE6377D436615E994372DEC7CD1FF034EA610A90_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2252[1] = 
{
	TlsClientFinished_tDE6377D436615E994372DEC7CD1FF034EA610A90_StaticFields::get_offset_of_Ssl3Marker_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2253 = { sizeof (TlsClientHello_t6F6C4D0AD370DAF5DAD157F6EE6E2F4C60864013), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2253[1] = 
{
	TlsClientHello_t6F6C4D0AD370DAF5DAD157F6EE6E2F4C60864013::get_offset_of_random_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2254 = { sizeof (TlsClientKeyExchange_tE244A6ABFC942308A37FFE5C0C12B4C67CC28460), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2255 = { sizeof (TlsServerCertificate_tBA4685202875F457FA1FFD3F0B7E50C621A7DDEC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2255[1] = 
{
	TlsServerCertificate_tBA4685202875F457FA1FFD3F0B7E50C621A7DDEC::get_offset_of_certificates_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2256 = { sizeof (TlsServerCertificateRequest_t005A4A579268F30770A0EE3FA9FA772B204E6900), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2256[2] = 
{
	TlsServerCertificateRequest_t005A4A579268F30770A0EE3FA9FA772B204E6900::get_offset_of_certificateTypes_14(),
	TlsServerCertificateRequest_t005A4A579268F30770A0EE3FA9FA772B204E6900::get_offset_of_distinguisedNames_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2257 = { sizeof (TlsServerFinished_tFCC471675890AC0C3EE7899CF10AAE3D55EBDDE7), -1, sizeof(TlsServerFinished_tFCC471675890AC0C3EE7899CF10AAE3D55EBDDE7_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2257[1] = 
{
	TlsServerFinished_tFCC471675890AC0C3EE7899CF10AAE3D55EBDDE7_StaticFields::get_offset_of_Ssl3Marker_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2258 = { sizeof (TlsServerHello_tE57721F1222D4A82CC98058BCDA5511B0AA4F294), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2258[4] = 
{
	TlsServerHello_tE57721F1222D4A82CC98058BCDA5511B0AA4F294::get_offset_of_compressionMethod_14(),
	TlsServerHello_tE57721F1222D4A82CC98058BCDA5511B0AA4F294::get_offset_of_random_15(),
	TlsServerHello_tE57721F1222D4A82CC98058BCDA5511B0AA4F294::get_offset_of_sessionId_16(),
	TlsServerHello_tE57721F1222D4A82CC98058BCDA5511B0AA4F294::get_offset_of_cipherSuite_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2259 = { sizeof (TlsServerHelloDone_tE26C2109DAF7607E99910210AD1B78142BD37CC9), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2260 = { sizeof (ChallengeResponse_t2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B), -1, sizeof(ChallengeResponse_t2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2260[6] = 
{
	ChallengeResponse_t2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B_StaticFields::get_offset_of_magic_0(),
	ChallengeResponse_t2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B_StaticFields::get_offset_of_nullEncMagic_1(),
	ChallengeResponse_t2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B::get_offset_of__disposed_2(),
	ChallengeResponse_t2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B::get_offset_of__challenge_3(),
	ChallengeResponse_t2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B::get_offset_of__lmpwd_4(),
	ChallengeResponse_t2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B::get_offset_of__ntpwd_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2261 = { sizeof (ChallengeResponse2_t2F5817D7717011A7F4C0DE0015D144A2292CAD77), -1, sizeof(ChallengeResponse2_t2F5817D7717011A7F4C0DE0015D144A2292CAD77_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2261[2] = 
{
	ChallengeResponse2_t2F5817D7717011A7F4C0DE0015D144A2292CAD77_StaticFields::get_offset_of_magic_0(),
	ChallengeResponse2_t2F5817D7717011A7F4C0DE0015D144A2292CAD77_StaticFields::get_offset_of_nullEncMagic_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2262 = { sizeof (MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0), -1, sizeof(MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2262[3] = 
{
	MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0_StaticFields::get_offset_of_header_0(),
	MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0::get_offset_of__type_1(),
	MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0::get_offset_of__flags_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2263 = { sizeof (NtlmAuthLevel_tF1354DE8BF43C36E20D475A077E035BB11936015)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2263[5] = 
{
	NtlmAuthLevel_tF1354DE8BF43C36E20D475A077E035BB11936015::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2264 = { sizeof (NtlmFlags_t9AC7D2604BC2E16EDEF1C9D2066E6BEC0D8A1578)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2264[11] = 
{
	NtlmFlags_t9AC7D2604BC2E16EDEF1C9D2066E6BEC0D8A1578::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2265 = { sizeof (NtlmSettings_tC673E811873A17EA73FCA0EFD6D33839B5036009), -1, sizeof(NtlmSettings_tC673E811873A17EA73FCA0EFD6D33839B5036009_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2265[1] = 
{
	NtlmSettings_tC673E811873A17EA73FCA0EFD6D33839B5036009_StaticFields::get_offset_of_defaultAuthLevel_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2266 = { sizeof (Type1Message_tF2DA0014BB300ABA864D84752FFA278EC6E6519C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2266[2] = 
{
	Type1Message_tF2DA0014BB300ABA864D84752FFA278EC6E6519C::get_offset_of__host_3(),
	Type1Message_tF2DA0014BB300ABA864D84752FFA278EC6E6519C::get_offset_of__domain_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2267 = { sizeof (Type2Message_t990283F48D41AECF1FFBDAA3A194CDE9C9078398), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2267[3] = 
{
	Type2Message_t990283F48D41AECF1FFBDAA3A194CDE9C9078398::get_offset_of__nonce_3(),
	Type2Message_t990283F48D41AECF1FFBDAA3A194CDE9C9078398::get_offset_of__targetName_4(),
	Type2Message_t990283F48D41AECF1FFBDAA3A194CDE9C9078398::get_offset_of__targetInfo_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2268 = { sizeof (Type3Message_t6D21CF9E3D56192F8D9B6E2B29474773E838846C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2268[9] = 
{
	Type3Message_t6D21CF9E3D56192F8D9B6E2B29474773E838846C::get_offset_of__level_3(),
	Type3Message_t6D21CF9E3D56192F8D9B6E2B29474773E838846C::get_offset_of__challenge_4(),
	Type3Message_t6D21CF9E3D56192F8D9B6E2B29474773E838846C::get_offset_of__host_5(),
	Type3Message_t6D21CF9E3D56192F8D9B6E2B29474773E838846C::get_offset_of__domain_6(),
	Type3Message_t6D21CF9E3D56192F8D9B6E2B29474773E838846C::get_offset_of__username_7(),
	Type3Message_t6D21CF9E3D56192F8D9B6E2B29474773E838846C::get_offset_of__password_8(),
	Type3Message_t6D21CF9E3D56192F8D9B6E2B29474773E838846C::get_offset_of__type2_9(),
	Type3Message_t6D21CF9E3D56192F8D9B6E2B29474773E838846C::get_offset_of__lm_10(),
	Type3Message_t6D21CF9E3D56192F8D9B6E2B29474773E838846C::get_offset_of__nt_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2269 = { sizeof (AlertLevel_t300CD4F0586BC84361B20C4B26C89EC1ECB3FC34)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2269[3] = 
{
	AlertLevel_t300CD4F0586BC84361B20C4B26C89EC1ECB3FC34::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2270 = { sizeof (AlertDescription_t8D4DE3060801044928816134B2292AFB933D40D6)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2270[26] = 
{
	AlertDescription_t8D4DE3060801044928816134B2292AFB933D40D6::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2271 = { sizeof (Alert_tABF269545F2C583CCA47FF574E612DDAF232944E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2271[2] = 
{
	Alert_tABF269545F2C583CCA47FF574E612DDAF232944E::get_offset_of_level_0(),
	Alert_tABF269545F2C583CCA47FF574E612DDAF232944E::get_offset_of_description_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2272 = { sizeof (ValidationResult_tBBAD776F36C835C8DDB515B1747DEF3A45C058D7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2272[4] = 
{
	ValidationResult_tBBAD776F36C835C8DDB515B1747DEF3A45C058D7::get_offset_of_trusted_0(),
	ValidationResult_tBBAD776F36C835C8DDB515B1747DEF3A45C058D7::get_offset_of_user_denied_1(),
	ValidationResult_tBBAD776F36C835C8DDB515B1747DEF3A45C058D7::get_offset_of_error_code_2(),
	ValidationResult_tBBAD776F36C835C8DDB515B1747DEF3A45C058D7::get_offset_of_policy_errors_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2273 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2274 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2275 = { sizeof (CertificateValidationHelper_t700A78AF16D5DA1698501C49E6894B1B67963F86), -1, sizeof(CertificateValidationHelper_t700A78AF16D5DA1698501C49E6894B1B67963F86_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2275[2] = 
{
	CertificateValidationHelper_t700A78AF16D5DA1698501C49E6894B1B67963F86_StaticFields::get_offset_of_noX509Chain_0(),
	CertificateValidationHelper_t700A78AF16D5DA1698501C49E6894B1B67963F86_StaticFields::get_offset_of_supportsTrustAnchors_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2276 = { sizeof (CipherSuiteCode_t32674B07A5C552605FA138AEACFFA20474A255F1)+ sizeof (RuntimeObject), sizeof(uint16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2276[267] = 
{
	CipherSuiteCode_t32674B07A5C552605FA138AEACFFA20474A255F1::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2277 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2278 = { sizeof (MonoTlsConnectionInfo_tE32F709ECF061DD150F45384869CE8431BD7A74D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2278[3] = 
{
	MonoTlsConnectionInfo_tE32F709ECF061DD150F45384869CE8431BD7A74D::get_offset_of_U3CCipherSuiteCodeU3Ek__BackingField_0(),
	MonoTlsConnectionInfo_tE32F709ECF061DD150F45384869CE8431BD7A74D::get_offset_of_U3CProtocolVersionU3Ek__BackingField_1(),
	MonoTlsConnectionInfo_tE32F709ECF061DD150F45384869CE8431BD7A74D::get_offset_of_U3CPeerDomainNameU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2279 = { sizeof (MonoSslPolicyErrors_t5F32A4E793EAB8B8A8128A6A3E7690D2E1F666C7)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2279[5] = 
{
	MonoSslPolicyErrors_t5F32A4E793EAB8B8A8128A6A3E7690D2E1F666C7::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2280 = { sizeof (MonoRemoteCertificateValidationCallback_t7A8DAD12B70CE3BB19BAAD04F587D5ED02385CC6), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2281 = { sizeof (MonoLocalCertificateSelectionCallback_t657381EF916D4EDC456FA5A6AC948EFD7A481F0A), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2282 = { sizeof (MonoTlsProvider_tDCD056C5BBBE59ED6BAF63F25952B406C1143C27), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2283 = { sizeof (MonoTlsProviderFactory_t7BC164DB50C3AD4B37141048F27F7E74863146BB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2284 = { sizeof (MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF), -1, sizeof(MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2284[17] = 
{
	MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF::get_offset_of_U3CRemoteCertificateValidationCallbackU3Ek__BackingField_0(),
	MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF::get_offset_of_U3CClientCertificateSelectionCallbackU3Ek__BackingField_1(),
	MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF::get_offset_of_U3CCertificateValidationTimeU3Ek__BackingField_2(),
	MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF::get_offset_of_U3CTrustAnchorsU3Ek__BackingField_3(),
	MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF::get_offset_of_U3CUserSettingsU3Ek__BackingField_4(),
	MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF::get_offset_of_U3CCertificateSearchPathsU3Ek__BackingField_5(),
	MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF::get_offset_of_U3CSendCloseNotifyU3Ek__BackingField_6(),
	MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF::get_offset_of_U3CEnabledProtocolsU3Ek__BackingField_7(),
	MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF::get_offset_of_U3CEnabledCiphersU3Ek__BackingField_8(),
	MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF::get_offset_of_cloned_9(),
	MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF::get_offset_of_checkCertName_10(),
	MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF::get_offset_of_checkCertRevocationStatus_11(),
	MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF::get_offset_of_useServicePointManagerCallback_12(),
	MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF::get_offset_of_skipSystemValidators_13(),
	MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF::get_offset_of_callbackNeedsChain_14(),
	MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF::get_offset_of_certificateValidator_15(),
	MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF_StaticFields::get_offset_of_defaultSettings_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2285 = { sizeof (TlsException_t774465EA64E3ADAAE3DB21835DD9AB8C40247F91), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2285[1] = 
{
	TlsException_t774465EA64E3ADAAE3DB21835DD9AB8C40247F91::get_offset_of_alert_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2286 = { sizeof (TlsProtocols_t25D1B0EFE5CC77B30D19258E7AC462AB4D828163)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2286[13] = 
{
	TlsProtocols_t25D1B0EFE5CC77B30D19258E7AC462AB4D828163::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2287 = { sizeof (ARC4Managed_t03DC2C93774F0A3652F03EFA78A0431AB664F397), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2287[5] = 
{
	ARC4Managed_t03DC2C93774F0A3652F03EFA78A0431AB664F397::get_offset_of_key_11(),
	ARC4Managed_t03DC2C93774F0A3652F03EFA78A0431AB664F397::get_offset_of_state_12(),
	ARC4Managed_t03DC2C93774F0A3652F03EFA78A0431AB664F397::get_offset_of_x_13(),
	ARC4Managed_t03DC2C93774F0A3652F03EFA78A0431AB664F397::get_offset_of_y_14(),
	ARC4Managed_t03DC2C93774F0A3652F03EFA78A0431AB664F397::get_offset_of_m_disposed_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2288 = { sizeof (CryptoConvert_tF1F175C2F2C9E65FE7D5FBF0D434B964E4CAFF76), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2289 = { sizeof (KeyBuilder_t7D70B1A038D0911A8BC47E8BABEDF8163FA22EA7), -1, sizeof(KeyBuilder_t7D70B1A038D0911A8BC47E8BABEDF8163FA22EA7_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2289[1] = 
{
	KeyBuilder_t7D70B1A038D0911A8BC47E8BABEDF8163FA22EA7_StaticFields::get_offset_of_rng_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2290 = { sizeof (MD2_tCAAEC1A28A3D0B9E8810B27E4840BEA399619442), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2291 = { sizeof (MD2Managed_t6867DBAD1BDCC846BE478BABC2D5DBAFFC7495EC), -1, sizeof(MD2Managed_t6867DBAD1BDCC846BE478BABC2D5DBAFFC7495EC_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2291[6] = 
{
	MD2Managed_t6867DBAD1BDCC846BE478BABC2D5DBAFFC7495EC::get_offset_of_state_4(),
	MD2Managed_t6867DBAD1BDCC846BE478BABC2D5DBAFFC7495EC::get_offset_of_checksum_5(),
	MD2Managed_t6867DBAD1BDCC846BE478BABC2D5DBAFFC7495EC::get_offset_of_buffer_6(),
	MD2Managed_t6867DBAD1BDCC846BE478BABC2D5DBAFFC7495EC::get_offset_of_count_7(),
	MD2Managed_t6867DBAD1BDCC846BE478BABC2D5DBAFFC7495EC::get_offset_of_x_8(),
	MD2Managed_t6867DBAD1BDCC846BE478BABC2D5DBAFFC7495EC_StaticFields::get_offset_of_PI_SUBST_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2292 = { sizeof (MD4_t932C1DEA44D4B8650873251E88AA4096164BB380), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2293 = { sizeof (MD4Managed_t1FC9A6CDB0A89A71416414689B3A0FF0283759D1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2293[5] = 
{
	MD4Managed_t1FC9A6CDB0A89A71416414689B3A0FF0283759D1::get_offset_of_state_4(),
	MD4Managed_t1FC9A6CDB0A89A71416414689B3A0FF0283759D1::get_offset_of_buffer_5(),
	MD4Managed_t1FC9A6CDB0A89A71416414689B3A0FF0283759D1::get_offset_of_count_6(),
	MD4Managed_t1FC9A6CDB0A89A71416414689B3A0FF0283759D1::get_offset_of_x_7(),
	MD4Managed_t1FC9A6CDB0A89A71416414689B3A0FF0283759D1::get_offset_of_digest_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2294 = { sizeof (MD5SHA1_tEB00523D30161BCED494CC7264D4A3F7CB28D86A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2294[3] = 
{
	MD5SHA1_tEB00523D30161BCED494CC7264D4A3F7CB28D86A::get_offset_of_md5_4(),
	MD5SHA1_tEB00523D30161BCED494CC7264D4A3F7CB28D86A::get_offset_of_sha_5(),
	MD5SHA1_tEB00523D30161BCED494CC7264D4A3F7CB28D86A::get_offset_of_hashing_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2295 = { sizeof (PKCS1_t5ABACACDDA0FE1D9A3D2ECD6BEB8815C8431B222), -1, sizeof(PKCS1_t5ABACACDDA0FE1D9A3D2ECD6BEB8815C8431B222_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2295[4] = 
{
	PKCS1_t5ABACACDDA0FE1D9A3D2ECD6BEB8815C8431B222_StaticFields::get_offset_of_emptySHA1_0(),
	PKCS1_t5ABACACDDA0FE1D9A3D2ECD6BEB8815C8431B222_StaticFields::get_offset_of_emptySHA256_1(),
	PKCS1_t5ABACACDDA0FE1D9A3D2ECD6BEB8815C8431B222_StaticFields::get_offset_of_emptySHA384_2(),
	PKCS1_t5ABACACDDA0FE1D9A3D2ECD6BEB8815C8431B222_StaticFields::get_offset_of_emptySHA512_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2296 = { sizeof (PKCS8_tD24BB8D5DD62F9EF2DCDB5E3B03DB4AB19415656), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2297 = { sizeof (PrivateKeyInfo_t053A73C524EC661748073EC908D290E2856DB5B9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2297[4] = 
{
	PrivateKeyInfo_t053A73C524EC661748073EC908D290E2856DB5B9::get_offset_of__version_0(),
	PrivateKeyInfo_t053A73C524EC661748073EC908D290E2856DB5B9::get_offset_of__algorithm_1(),
	PrivateKeyInfo_t053A73C524EC661748073EC908D290E2856DB5B9::get_offset_of__key_2(),
	PrivateKeyInfo_t053A73C524EC661748073EC908D290E2856DB5B9::get_offset_of__list_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2298 = { sizeof (EncryptedPrivateKeyInfo_tF582FAE09721F4F2BB353D5C2E28682FFB9C303D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2298[4] = 
{
	EncryptedPrivateKeyInfo_tF582FAE09721F4F2BB353D5C2E28682FFB9C303D::get_offset_of__algorithm_0(),
	EncryptedPrivateKeyInfo_tF582FAE09721F4F2BB353D5C2E28682FFB9C303D::get_offset_of__salt_1(),
	EncryptedPrivateKeyInfo_tF582FAE09721F4F2BB353D5C2E28682FFB9C303D::get_offset_of__iterations_2(),
	EncryptedPrivateKeyInfo_tF582FAE09721F4F2BB353D5C2E28682FFB9C303D::get_offset_of__data_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2299 = { sizeof (RC4_tC03B8F8797476E53DD785D576E7DE1951667A54B), -1, sizeof(RC4_tC03B8F8797476E53DD785D576E7DE1951667A54B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2299[2] = 
{
	RC4_tC03B8F8797476E53DD785D576E7DE1951667A54B_StaticFields::get_offset_of_s_legalBlockSizes_9(),
	RC4_tC03B8F8797476E53DD785D576E7DE1951667A54B_StaticFields::get_offset_of_s_legalKeySizes_10(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
