﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// BattleIntroFlow
struct BattleIntroFlow_t6B627D5733BE5B127F2EBBABF6747828F5F35D36;
// BattleInventoryFlow
struct BattleInventoryFlow_t7F82684619055127E49D19EEF227C08632E08E84;
// ChatListItemData
struct ChatListItemData_tB039C181C297B802B7B35327AA170A2106CDD53B;
// CollectGoalFlow
struct CollectGoalFlow_tB876F66620038DC89241A21E6C080DA1B60BA94C;
// CollectGoalTutorialNode
struct CollectGoalTutorialNode_t1743E320CB9D993CF09AC77A20CED76149C74637;
// Contexts
struct Contexts_t313FEE68C5FB0568E30785C33C0F6209F5F7B87D;
// CustomMessageData
struct CustomMessageData_t2BA087DE2A16E4C6D102083A45431AA2E422A6DD;
// CustomTutorialModel
struct CustomTutorialModel_t7F1E6EF5B6AF58AEA9FB2573811B4562E587E635;
// DestroyFiveInventoryTutorialNode
struct DestroyFiveInventoryTutorialNode_tE58CD63865141981AFF5F1A2E9A0A836D1576FF7;
// DestroyGoalTutorialNode
struct DestroyGoalTutorialNode_tA62D39E749050BBCB967A3B7FA6CE41997DDC58D;
// DestroyTowerGoalNode
struct DestroyTowerGoalNode_t75EDEEB6BF4F7A62DBA3DFCB647F82385DBFE311;
// Entitas.IGroup`1<GameEntity>
struct IGroup_1_tF4940889845236B5907C50AD6A40AD6CE69EC680;
// FirstSkillFlow
struct FirstSkillFlow_t178220FD212277F8C8E0CF2D00031184B612455F;
// FirstSkillTutorialNode
struct FirstSkillTutorialNode_t6A610C4EE426DE5AE3E6677C4574CDAE89B904C8;
// FourthSkillFlow
struct FourthSkillFlow_t30FF5419C188236877763F85BAD84CE9339BBA91;
// FourthSkillTutorialNode
struct FourthSkillTutorialNode_t54BD10C3CA58AB022A554FFCA2723F0D0FB20057;
// GameEntity
struct GameEntity_t995A5498A539905D7F1F8284D59BD48F077D78FF;
// GameSettingsSO
struct GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C;
// GameSparks.Api.Responses.AuthenticationResponse
struct AuthenticationResponse_tB52C005AD2DB9DDC3021B03A35E01367A2A9EFB9;
// GameSparks.Api.Responses.LeaderboardDataResponse
struct LeaderboardDataResponse_t58F860D186FBA6DA0B2EE7B029B213A2C99F8D01;
// GameSparks.Core.GSData
struct GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937;
// GoalsTutorialNode
struct GoalsTutorialNode_tF301448520894286687F236F80234B8A56CBE7B1;
// KickTooltipData
struct KickTooltipData_t7912892AE01A47560991479A19D49CB87E1F4448;
// KickerManager
struct KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA;
// MetadataTask
struct MetadataTask_t50EC30F550F961ABB90D0D5290708083A8D3B04A;
// MovementTutorialNode
struct MovementTutorialNode_t612D67CD2F5CEA136C089C042E8C326FECB3018A;
// NotificationSamples.GameNotificationsManager
struct GameNotificationsManager_tA91A2FAF80490C54B58C02C8B5652AFA290D6B9F;
// OnQuestEndedEvent
struct OnQuestEndedEvent_tC0EC99C69DD18EF07F2A9877323E1CE9E7825D6F;
// OnQuestStartedEvent
struct OnQuestStartedEvent_t8E404B059FB2011AF0DC8299EBBF6A2DB9657948;
// OnQuestUpdatedEvent
struct OnQuestUpdatedEvent_t8B85F5C13C8C6D94E08B49B1C3AFA8A6118F8CAB;
// Quest
struct Quest_tBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021;
// QuestsVO
struct QuestsVO_t585A9D6E6540A29D33D1DE147EAFF40417B85429;
// ReportTooltipData
struct ReportTooltipData_tF8EDAC5D364823AA3EA0B424A1C09029CF485A93;
// SecondSkillFlow
struct SecondSkillFlow_t56C78A79BCE637FB296A312521CD8D3C5FE89165;
// SecondSkillTutorialNode
struct SecondSkillTutorialNode_t1E6ECA8797A902ABA81F995DC3640D0FCC725A7F;
// SpellUIInitializer
struct SpellUIInitializer_t66B06A358BA3B762376CD13F820EFC620ADFDAD8;
// StarTournamentListItemData
struct StarTournamentListItemData_t6FE18476EB88BBD97CE1527A1C5EE42E3A8936AD;
// StarTournamentRankVariable
struct StarTournamentRankVariable_tC180F6992EF9E03FF630834652B1B578945581BD;
// StarTournamentSystem
struct StarTournamentSystem_tBBE982F84537BACA13B9A61826F04C1F5F726C88;
// StarTournamentVariable
struct StarTournamentVariable_tAEC9A6E89954C3E8E99380747371340583301A90;
// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.String,Quest>
struct Dictionary_2_t3AA935A331F98DF15B2004585C1645B647B60793;
// System.Collections.Generic.Dictionary`2<System.String,TournamentModel>
struct Dictionary_2_t0D44CD823085042D11C357BC35A9D64FFB54556A;
// System.Collections.Generic.List`1<GameSparks.Core.GSData>
struct List_1_t0F7C46C2525274B25F1AE138EE936A6F09DAA716;
// System.Collections.Generic.List`1<StarTournamentListItemData>
struct List_1_t386DA0CB20C9114E16EF88E20B87F7B391EAA070;
// System.Collections.Generic.List`1<Tayr.Trigger>
struct List_1_tD769F165CB73AC301E135982F0DB33265BCF7EB0;
// System.Collections.Generic.List`1<TribeTournamentContributionListItemData>
struct List_1_t3CFA56CDB27665BE76F1EEA57249AE608A43237D;
// System.Collections.Generic.List`1<TribeTournamentListItemData>
struct List_1_t6367895834A22C833A94F350602CA85277483EE4;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.CompilerServices.IAsyncStateMachine
struct IAsyncStateMachine_tEFDFBE18E061A6065AB2FF735F1425FB59F919BC;
// System.String
struct String_t;
// System.Threading.Tasks.Task
struct Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2;
// System.Threading.Tasks.Task`1<System.Threading.Tasks.VoidTaskResult>
struct Task_1_t1359D75350E9D976BFA28AD96E417450DE277673;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// TMPro.TMP_InputField
struct TMP_InputField_tC3C57E697A57232E8A855D39600CF06CFDA8F6CB;
// TMPro.TextMeshProUGUI
struct TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438;
// Tayr.GameSparksPlatform
struct GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348;
// Tayr.ILibrary
struct ILibrary_tCBD3DE1F66AD92DAB1112B8018EBA4A4BD23767C;
// Tayr.INode
struct INode_tD44544594CD9341F4835D560B81E4A956C1D1DC7;
// Tayr.INodeAnimationHandler
struct INodeAnimationHandler_t7A50F84EDDDD8CA150788E8B2A8D0F06E5ABF200;
// Tayr.NodeAnimator
struct NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F;
// Tayr.TDynamicList
struct TDynamicList_tF4B70F991EE13A3CB137902A92EC1A729C06DB19;
// Tayr.TList
struct TList_tB67CEA10085B72B7C69ACE7FCF01FCC38E6EF292;
// Tayr.TSoundSystem
struct TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511;
// Tayr.TSystemsManager
struct TSystemsManager_t22A7C9150EDDB2B8046FF0832CBAEDA8FAEB8462;
// Tayr.Trigger
struct Trigger_tFA4DAF5E2FB996087C1F5DD53203E250D4770C09;
// Tayr.VOSaver
struct VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F;
// Tayr.VariableChangedEvent
struct VariableChangedEvent_t553579619C432FFE7C7636199CE57457FA699BA4;
// ThirdSkillFlow
struct ThirdSkillFlow_t99B9A8894864D1DBED1774C522267C4A60B76652;
// ThirdSkillTutorialNode
struct ThirdSkillTutorialNode_tE7A98B1237FF73D72F138C0C69D59D0E36C3BA1D;
// TimeSystem
struct TimeSystem_t3EA79F95B78D7FF6BC7D3A29AADB4E9CFB0F2CED;
// TournamentModel
struct TournamentModel_tB7076328E3D85DBCC9B2B22F1BD7B3EEC967DB3D;
// TournamentsVO
struct TournamentsVO_tCBA9A235AE4069263CE332B1E9150CDABD0E8016;
// TribeTournamentContributionListItemData
struct TribeTournamentContributionListItemData_tF9C29726809FAC898576A1F8CC49BDA2D376924F;
// TribeTournamentListItemData
struct TribeTournamentListItemData_t972BE1E8BC71C51099D379602320348346EBEE14;
// TribeTournamentSystem
struct TribeTournamentSystem_t7F0DE0115218A7B9AA81C8EF2B923A4450F36424;
// TribeTournamentVariable
struct TribeTournamentVariable_t8978F616B72F4D039DC3E75EBB14E6F4A7D15300;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F;
// UnityEngine.Events.UnityAction
struct UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.TouchScreenKeyboard
struct TouchScreenKeyboard_t2A69F85698E9780470181532D3F2BC903623FD90;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.UI.Button
struct Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B;
// UnityEngine.UI.Image
struct Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E;
// UserEvents
struct UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7;
// UserVO
struct UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94;
// Zenject.DiContainer
struct DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5;
// Zenject.DisposableManager
struct DisposableManager_tCB394E2EA1FE174EB07A1082C5F1760FFE6F9ADC;

struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef BASICFLOW_TED6559FAA2A08AD1986ABE4002984C2C018A1A56_H
#define BASICFLOW_TED6559FAA2A08AD1986ABE4002984C2C018A1A56_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BasicFlow
struct  BasicFlow_tED6559FAA2A08AD1986ABE4002984C2C018A1A56  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICFLOW_TED6559FAA2A08AD1986ABE4002984C2C018A1A56_H
#ifndef BASICTUTORIALFLOW_TBA017E4C87F10BC8DD5BA4E75B90C9AFD0A3730B_H
#define BASICTUTORIALFLOW_TBA017E4C87F10BC8DD5BA4E75B90C9AFD0A3730B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BasicTutorialFlow
struct  BasicTutorialFlow_tBA017E4C87F10BC8DD5BA4E75B90C9AFD0A3730B  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICTUTORIALFLOW_TBA017E4C87F10BC8DD5BA4E75B90C9AFD0A3730B_H
#ifndef CUSTOMMESSAGEDATA_T2BA087DE2A16E4C6D102083A45431AA2E422A6DD_H
#define CUSTOMMESSAGEDATA_T2BA087DE2A16E4C6D102083A45431AA2E422A6DD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CustomMessageData
struct  CustomMessageData_t2BA087DE2A16E4C6D102083A45431AA2E422A6DD  : public RuntimeObject
{
public:
	// System.String CustomMessageData::Action
	String_t* ___Action_0;
	// System.String CustomMessageData::TargetId
	String_t* ___TargetId_1;

public:
	inline static int32_t get_offset_of_Action_0() { return static_cast<int32_t>(offsetof(CustomMessageData_t2BA087DE2A16E4C6D102083A45431AA2E422A6DD, ___Action_0)); }
	inline String_t* get_Action_0() const { return ___Action_0; }
	inline String_t** get_address_of_Action_0() { return &___Action_0; }
	inline void set_Action_0(String_t* value)
	{
		___Action_0 = value;
		Il2CppCodeGenWriteBarrier((&___Action_0), value);
	}

	inline static int32_t get_offset_of_TargetId_1() { return static_cast<int32_t>(offsetof(CustomMessageData_t2BA087DE2A16E4C6D102083A45431AA2E422A6DD, ___TargetId_1)); }
	inline String_t* get_TargetId_1() const { return ___TargetId_1; }
	inline String_t** get_address_of_TargetId_1() { return &___TargetId_1; }
	inline void set_TargetId_1(String_t* value)
	{
		___TargetId_1 = value;
		Il2CppCodeGenWriteBarrier((&___TargetId_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMMESSAGEDATA_T2BA087DE2A16E4C6D102083A45431AA2E422A6DD_H
#ifndef MESSAGESUTILS_T8378B9BBF4CB32A0949C62B26744A1E851769C58_H
#define MESSAGESUTILS_T8378B9BBF4CB32A0949C62B26744A1E851769C58_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Game.MessagesUtils
struct  MessagesUtils_t8378B9BBF4CB32A0949C62B26744A1E851769C58  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGESUTILS_T8378B9BBF4CB32A0949C62B26744A1E851769C58_H
#ifndef KICKTOOLTIPDATA_T7912892AE01A47560991479A19D49CB87E1F4448_H
#define KICKTOOLTIPDATA_T7912892AE01A47560991479A19D49CB87E1F4448_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// KickTooltipData
struct  KickTooltipData_t7912892AE01A47560991479A19D49CB87E1F4448  : public RuntimeObject
{
public:
	// GameSparks.Core.GSData KickTooltipData::PlayerData
	GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 * ___PlayerData_0;
	// UnityEngine.Transform KickTooltipData::ListItem
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___ListItem_1;

public:
	inline static int32_t get_offset_of_PlayerData_0() { return static_cast<int32_t>(offsetof(KickTooltipData_t7912892AE01A47560991479A19D49CB87E1F4448, ___PlayerData_0)); }
	inline GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 * get_PlayerData_0() const { return ___PlayerData_0; }
	inline GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 ** get_address_of_PlayerData_0() { return &___PlayerData_0; }
	inline void set_PlayerData_0(GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 * value)
	{
		___PlayerData_0 = value;
		Il2CppCodeGenWriteBarrier((&___PlayerData_0), value);
	}

	inline static int32_t get_offset_of_ListItem_1() { return static_cast<int32_t>(offsetof(KickTooltipData_t7912892AE01A47560991479A19D49CB87E1F4448, ___ListItem_1)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_ListItem_1() const { return ___ListItem_1; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_ListItem_1() { return &___ListItem_1; }
	inline void set_ListItem_1(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___ListItem_1 = value;
		Il2CppCodeGenWriteBarrier((&___ListItem_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KICKTOOLTIPDATA_T7912892AE01A47560991479A19D49CB87E1F4448_H
#ifndef U3CTASKU3ED__2_TEE94232046EAF9F0668852CE5B71A652DA1B0444_H
#define U3CTASKU3ED__2_TEE94232046EAF9F0668852CE5B71A652DA1B0444_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MetadataTask_<Task>d__2
struct  U3CTaskU3Ed__2_tEE94232046EAF9F0668852CE5B71A652DA1B0444  : public RuntimeObject
{
public:
	// System.Int32 MetadataTask_<Task>d__2::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object MetadataTask_<Task>d__2::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// MetadataTask MetadataTask_<Task>d__2::<>4__this
	MetadataTask_t50EC30F550F961ABB90D0D5290708083A8D3B04A * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__2_tEE94232046EAF9F0668852CE5B71A652DA1B0444, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__2_tEE94232046EAF9F0668852CE5B71A652DA1B0444, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__2_tEE94232046EAF9F0668852CE5B71A652DA1B0444, ___U3CU3E4__this_2)); }
	inline MetadataTask_t50EC30F550F961ABB90D0D5290708083A8D3B04A * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline MetadataTask_t50EC30F550F961ABB90D0D5290708083A8D3B04A ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(MetadataTask_t50EC30F550F961ABB90D0D5290708083A8D3B04A * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTASKU3ED__2_TEE94232046EAF9F0668852CE5B71A652DA1B0444_H
#ifndef NOTIFICATIONSUTILS_T5C24829892F954D60BC0B145F54F03700C46D50F_H
#define NOTIFICATIONSUTILS_T5C24829892F954D60BC0B145F54F03700C46D50F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NotificationsUtils
struct  NotificationsUtils_t5C24829892F954D60BC0B145F54F03700C46D50F  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTIFICATIONSUTILS_T5C24829892F954D60BC0B145F54F03700C46D50F_H
#ifndef QUEST_TBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021_H
#define QUEST_TBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Quest
struct  Quest_tBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021  : public RuntimeObject
{
public:
	// OnQuestStartedEvent Quest::_onQuestStarted
	OnQuestStartedEvent_t8E404B059FB2011AF0DC8299EBBF6A2DB9657948 * ____onQuestStarted_0;
	// OnQuestUpdatedEvent Quest::_onQuestUpdated
	OnQuestUpdatedEvent_t8B85F5C13C8C6D94E08B49B1C3AFA8A6118F8CAB * ____onQuestUpdated_1;
	// OnQuestEndedEvent Quest::_onQuestEnded
	OnQuestEndedEvent_tC0EC99C69DD18EF07F2A9877323E1CE9E7825D6F * ____onQuestEnded_2;
	// System.Collections.Generic.List`1<Tayr.Trigger> Quest::_startTriggers
	List_1_tD769F165CB73AC301E135982F0DB33265BCF7EB0 * ____startTriggers_3;
	// System.Collections.Generic.List`1<Tayr.Trigger> Quest::_endTriggers
	List_1_tD769F165CB73AC301E135982F0DB33265BCF7EB0 * ____endTriggers_4;
	// System.Boolean Quest::_isStarted
	bool ____isStarted_5;
	// System.Boolean Quest::_isUpdated
	bool ____isUpdated_6;
	// System.Boolean Quest::_isEnded
	bool ____isEnded_7;

public:
	inline static int32_t get_offset_of__onQuestStarted_0() { return static_cast<int32_t>(offsetof(Quest_tBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021, ____onQuestStarted_0)); }
	inline OnQuestStartedEvent_t8E404B059FB2011AF0DC8299EBBF6A2DB9657948 * get__onQuestStarted_0() const { return ____onQuestStarted_0; }
	inline OnQuestStartedEvent_t8E404B059FB2011AF0DC8299EBBF6A2DB9657948 ** get_address_of__onQuestStarted_0() { return &____onQuestStarted_0; }
	inline void set__onQuestStarted_0(OnQuestStartedEvent_t8E404B059FB2011AF0DC8299EBBF6A2DB9657948 * value)
	{
		____onQuestStarted_0 = value;
		Il2CppCodeGenWriteBarrier((&____onQuestStarted_0), value);
	}

	inline static int32_t get_offset_of__onQuestUpdated_1() { return static_cast<int32_t>(offsetof(Quest_tBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021, ____onQuestUpdated_1)); }
	inline OnQuestUpdatedEvent_t8B85F5C13C8C6D94E08B49B1C3AFA8A6118F8CAB * get__onQuestUpdated_1() const { return ____onQuestUpdated_1; }
	inline OnQuestUpdatedEvent_t8B85F5C13C8C6D94E08B49B1C3AFA8A6118F8CAB ** get_address_of__onQuestUpdated_1() { return &____onQuestUpdated_1; }
	inline void set__onQuestUpdated_1(OnQuestUpdatedEvent_t8B85F5C13C8C6D94E08B49B1C3AFA8A6118F8CAB * value)
	{
		____onQuestUpdated_1 = value;
		Il2CppCodeGenWriteBarrier((&____onQuestUpdated_1), value);
	}

	inline static int32_t get_offset_of__onQuestEnded_2() { return static_cast<int32_t>(offsetof(Quest_tBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021, ____onQuestEnded_2)); }
	inline OnQuestEndedEvent_tC0EC99C69DD18EF07F2A9877323E1CE9E7825D6F * get__onQuestEnded_2() const { return ____onQuestEnded_2; }
	inline OnQuestEndedEvent_tC0EC99C69DD18EF07F2A9877323E1CE9E7825D6F ** get_address_of__onQuestEnded_2() { return &____onQuestEnded_2; }
	inline void set__onQuestEnded_2(OnQuestEndedEvent_tC0EC99C69DD18EF07F2A9877323E1CE9E7825D6F * value)
	{
		____onQuestEnded_2 = value;
		Il2CppCodeGenWriteBarrier((&____onQuestEnded_2), value);
	}

	inline static int32_t get_offset_of__startTriggers_3() { return static_cast<int32_t>(offsetof(Quest_tBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021, ____startTriggers_3)); }
	inline List_1_tD769F165CB73AC301E135982F0DB33265BCF7EB0 * get__startTriggers_3() const { return ____startTriggers_3; }
	inline List_1_tD769F165CB73AC301E135982F0DB33265BCF7EB0 ** get_address_of__startTriggers_3() { return &____startTriggers_3; }
	inline void set__startTriggers_3(List_1_tD769F165CB73AC301E135982F0DB33265BCF7EB0 * value)
	{
		____startTriggers_3 = value;
		Il2CppCodeGenWriteBarrier((&____startTriggers_3), value);
	}

	inline static int32_t get_offset_of__endTriggers_4() { return static_cast<int32_t>(offsetof(Quest_tBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021, ____endTriggers_4)); }
	inline List_1_tD769F165CB73AC301E135982F0DB33265BCF7EB0 * get__endTriggers_4() const { return ____endTriggers_4; }
	inline List_1_tD769F165CB73AC301E135982F0DB33265BCF7EB0 ** get_address_of__endTriggers_4() { return &____endTriggers_4; }
	inline void set__endTriggers_4(List_1_tD769F165CB73AC301E135982F0DB33265BCF7EB0 * value)
	{
		____endTriggers_4 = value;
		Il2CppCodeGenWriteBarrier((&____endTriggers_4), value);
	}

	inline static int32_t get_offset_of__isStarted_5() { return static_cast<int32_t>(offsetof(Quest_tBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021, ____isStarted_5)); }
	inline bool get__isStarted_5() const { return ____isStarted_5; }
	inline bool* get_address_of__isStarted_5() { return &____isStarted_5; }
	inline void set__isStarted_5(bool value)
	{
		____isStarted_5 = value;
	}

	inline static int32_t get_offset_of__isUpdated_6() { return static_cast<int32_t>(offsetof(Quest_tBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021, ____isUpdated_6)); }
	inline bool get__isUpdated_6() const { return ____isUpdated_6; }
	inline bool* get_address_of__isUpdated_6() { return &____isUpdated_6; }
	inline void set__isUpdated_6(bool value)
	{
		____isUpdated_6 = value;
	}

	inline static int32_t get_offset_of__isEnded_7() { return static_cast<int32_t>(offsetof(Quest_tBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021, ____isEnded_7)); }
	inline bool get__isEnded_7() const { return ____isEnded_7; }
	inline bool* get_address_of__isEnded_7() { return &____isEnded_7; }
	inline void set__isEnded_7(bool value)
	{
		____isEnded_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUEST_TBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021_H
#ifndef U3CU3EC__DISPLAYCLASS11_0_T54616E3EFBF3ACD267B52391C2028B56E60D9328_H
#define U3CU3EC__DISPLAYCLASS11_0_T54616E3EFBF3ACD267B52391C2028B56E60D9328_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Quest_<>c__DisplayClass11_0
struct  U3CU3Ec__DisplayClass11_0_t54616E3EFBF3ACD267B52391C2028B56E60D9328  : public RuntimeObject
{
public:
	// Tayr.Trigger Quest_<>c__DisplayClass11_0::t
	Trigger_tFA4DAF5E2FB996087C1F5DD53203E250D4770C09 * ___t_0;
	// Quest Quest_<>c__DisplayClass11_0::<>4__this
	Quest_tBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_t_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass11_0_t54616E3EFBF3ACD267B52391C2028B56E60D9328, ___t_0)); }
	inline Trigger_tFA4DAF5E2FB996087C1F5DD53203E250D4770C09 * get_t_0() const { return ___t_0; }
	inline Trigger_tFA4DAF5E2FB996087C1F5DD53203E250D4770C09 ** get_address_of_t_0() { return &___t_0; }
	inline void set_t_0(Trigger_tFA4DAF5E2FB996087C1F5DD53203E250D4770C09 * value)
	{
		___t_0 = value;
		Il2CppCodeGenWriteBarrier((&___t_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass11_0_t54616E3EFBF3ACD267B52391C2028B56E60D9328, ___U3CU3E4__this_1)); }
	inline Quest_tBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline Quest_tBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(Quest_tBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS11_0_T54616E3EFBF3ACD267B52391C2028B56E60D9328_H
#ifndef U3CU3EC__DISPLAYCLASS12_0_T7EED697364D448A257B04EB82E68193380EE4802_H
#define U3CU3EC__DISPLAYCLASS12_0_T7EED697364D448A257B04EB82E68193380EE4802_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Quest_<>c__DisplayClass12_0
struct  U3CU3Ec__DisplayClass12_0_t7EED697364D448A257B04EB82E68193380EE4802  : public RuntimeObject
{
public:
	// Quest Quest_<>c__DisplayClass12_0::<>4__this
	Quest_tBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021 * ___U3CU3E4__this_0;
	// Tayr.Trigger Quest_<>c__DisplayClass12_0::t
	Trigger_tFA4DAF5E2FB996087C1F5DD53203E250D4770C09 * ___t_1;
	// UnityEngine.Events.UnityAction Quest_<>c__DisplayClass12_0::<>9__0
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___U3CU3E9__0_2;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass12_0_t7EED697364D448A257B04EB82E68193380EE4802, ___U3CU3E4__this_0)); }
	inline Quest_tBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline Quest_tBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(Quest_tBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_t_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass12_0_t7EED697364D448A257B04EB82E68193380EE4802, ___t_1)); }
	inline Trigger_tFA4DAF5E2FB996087C1F5DD53203E250D4770C09 * get_t_1() const { return ___t_1; }
	inline Trigger_tFA4DAF5E2FB996087C1F5DD53203E250D4770C09 ** get_address_of_t_1() { return &___t_1; }
	inline void set_t_1(Trigger_tFA4DAF5E2FB996087C1F5DD53203E250D4770C09 * value)
	{
		___t_1 = value;
		Il2CppCodeGenWriteBarrier((&___t_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass12_0_t7EED697364D448A257B04EB82E68193380EE4802, ___U3CU3E9__0_2)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_U3CU3E9__0_2() const { return ___U3CU3E9__0_2; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_U3CU3E9__0_2() { return &___U3CU3E9__0_2; }
	inline void set_U3CU3E9__0_2(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___U3CU3E9__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__0_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS12_0_T7EED697364D448A257B04EB82E68193380EE4802_H
#ifndef U3CU3EC__DISPLAYCLASS8_0_T273D4CBDC29F9DB87ACF1356FE964106CF9E21D9_H
#define U3CU3EC__DISPLAYCLASS8_0_T273D4CBDC29F9DB87ACF1356FE964106CF9E21D9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Quest_<>c__DisplayClass8_0
struct  U3CU3Ec__DisplayClass8_0_t273D4CBDC29F9DB87ACF1356FE964106CF9E21D9  : public RuntimeObject
{
public:
	// Tayr.Trigger Quest_<>c__DisplayClass8_0::t
	Trigger_tFA4DAF5E2FB996087C1F5DD53203E250D4770C09 * ___t_0;
	// Quest Quest_<>c__DisplayClass8_0::<>4__this
	Quest_tBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_t_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_0_t273D4CBDC29F9DB87ACF1356FE964106CF9E21D9, ___t_0)); }
	inline Trigger_tFA4DAF5E2FB996087C1F5DD53203E250D4770C09 * get_t_0() const { return ___t_0; }
	inline Trigger_tFA4DAF5E2FB996087C1F5DD53203E250D4770C09 ** get_address_of_t_0() { return &___t_0; }
	inline void set_t_0(Trigger_tFA4DAF5E2FB996087C1F5DD53203E250D4770C09 * value)
	{
		___t_0 = value;
		Il2CppCodeGenWriteBarrier((&___t_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_0_t273D4CBDC29F9DB87ACF1356FE964106CF9E21D9, ___U3CU3E4__this_1)); }
	inline Quest_tBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline Quest_tBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(Quest_tBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS8_0_T273D4CBDC29F9DB87ACF1356FE964106CF9E21D9_H
#ifndef QUESTSYSTEM_TDFB5A64D6154EACAD855C582FF3E40C7A14C83D1_H
#define QUESTSYSTEM_TDFB5A64D6154EACAD855C582FF3E40C7A14C83D1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// QuestSystem
struct  QuestSystem_tDFB5A64D6154EACAD855C582FF3E40C7A14C83D1  : public RuntimeObject
{
public:
	// QuestsVO QuestSystem::_questsVO
	QuestsVO_t585A9D6E6540A29D33D1DE147EAFF40417B85429 * ____questsVO_0;
	// System.Collections.Generic.Dictionary`2<System.String,Quest> QuestSystem::_quests
	Dictionary_2_t3AA935A331F98DF15B2004585C1645B647B60793 * ____quests_1;

public:
	inline static int32_t get_offset_of__questsVO_0() { return static_cast<int32_t>(offsetof(QuestSystem_tDFB5A64D6154EACAD855C582FF3E40C7A14C83D1, ____questsVO_0)); }
	inline QuestsVO_t585A9D6E6540A29D33D1DE147EAFF40417B85429 * get__questsVO_0() const { return ____questsVO_0; }
	inline QuestsVO_t585A9D6E6540A29D33D1DE147EAFF40417B85429 ** get_address_of__questsVO_0() { return &____questsVO_0; }
	inline void set__questsVO_0(QuestsVO_t585A9D6E6540A29D33D1DE147EAFF40417B85429 * value)
	{
		____questsVO_0 = value;
		Il2CppCodeGenWriteBarrier((&____questsVO_0), value);
	}

	inline static int32_t get_offset_of__quests_1() { return static_cast<int32_t>(offsetof(QuestSystem_tDFB5A64D6154EACAD855C582FF3E40C7A14C83D1, ____quests_1)); }
	inline Dictionary_2_t3AA935A331F98DF15B2004585C1645B647B60793 * get__quests_1() const { return ____quests_1; }
	inline Dictionary_2_t3AA935A331F98DF15B2004585C1645B647B60793 ** get_address_of__quests_1() { return &____quests_1; }
	inline void set__quests_1(Dictionary_2_t3AA935A331F98DF15B2004585C1645B647B60793 * value)
	{
		____quests_1 = value;
		Il2CppCodeGenWriteBarrier((&____quests_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUESTSYSTEM_TDFB5A64D6154EACAD855C582FF3E40C7A14C83D1_H
#ifndef SOUNDUTILS_T6727D4BB1D79FF716E16DB7DF6045389EF1978ED_H
#define SOUNDUTILS_T6727D4BB1D79FF716E16DB7DF6045389EF1978ED_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SoundUtils
struct  SoundUtils_t6727D4BB1D79FF716E16DB7DF6045389EF1978ED  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOUNDUTILS_T6727D4BB1D79FF716E16DB7DF6045389EF1978ED_H
#ifndef STARTOURNAMENTLISTITEMDATA_T6FE18476EB88BBD97CE1527A1C5EE42E3A8936AD_H
#define STARTOURNAMENTLISTITEMDATA_T6FE18476EB88BBD97CE1527A1C5EE42E3A8936AD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StarTournamentListItemData
struct  StarTournamentListItemData_t6FE18476EB88BBD97CE1527A1C5EE42E3A8936AD  : public RuntimeObject
{
public:
	// System.Int32 StarTournamentListItemData::Icon
	int32_t ___Icon_0;
	// System.String StarTournamentListItemData::UserName
	String_t* ___UserName_1;
	// System.String StarTournamentListItemData::UserId
	String_t* ___UserId_2;
	// System.String StarTournamentListItemData::TeamId
	String_t* ___TeamId_3;
	// System.Int32 StarTournamentListItemData::Stars
	int32_t ___Stars_4;
	// System.String StarTournamentListItemData::TeamName
	String_t* ___TeamName_5;
	// System.Int32 StarTournamentListItemData::Rank
	int32_t ___Rank_6;

public:
	inline static int32_t get_offset_of_Icon_0() { return static_cast<int32_t>(offsetof(StarTournamentListItemData_t6FE18476EB88BBD97CE1527A1C5EE42E3A8936AD, ___Icon_0)); }
	inline int32_t get_Icon_0() const { return ___Icon_0; }
	inline int32_t* get_address_of_Icon_0() { return &___Icon_0; }
	inline void set_Icon_0(int32_t value)
	{
		___Icon_0 = value;
	}

	inline static int32_t get_offset_of_UserName_1() { return static_cast<int32_t>(offsetof(StarTournamentListItemData_t6FE18476EB88BBD97CE1527A1C5EE42E3A8936AD, ___UserName_1)); }
	inline String_t* get_UserName_1() const { return ___UserName_1; }
	inline String_t** get_address_of_UserName_1() { return &___UserName_1; }
	inline void set_UserName_1(String_t* value)
	{
		___UserName_1 = value;
		Il2CppCodeGenWriteBarrier((&___UserName_1), value);
	}

	inline static int32_t get_offset_of_UserId_2() { return static_cast<int32_t>(offsetof(StarTournamentListItemData_t6FE18476EB88BBD97CE1527A1C5EE42E3A8936AD, ___UserId_2)); }
	inline String_t* get_UserId_2() const { return ___UserId_2; }
	inline String_t** get_address_of_UserId_2() { return &___UserId_2; }
	inline void set_UserId_2(String_t* value)
	{
		___UserId_2 = value;
		Il2CppCodeGenWriteBarrier((&___UserId_2), value);
	}

	inline static int32_t get_offset_of_TeamId_3() { return static_cast<int32_t>(offsetof(StarTournamentListItemData_t6FE18476EB88BBD97CE1527A1C5EE42E3A8936AD, ___TeamId_3)); }
	inline String_t* get_TeamId_3() const { return ___TeamId_3; }
	inline String_t** get_address_of_TeamId_3() { return &___TeamId_3; }
	inline void set_TeamId_3(String_t* value)
	{
		___TeamId_3 = value;
		Il2CppCodeGenWriteBarrier((&___TeamId_3), value);
	}

	inline static int32_t get_offset_of_Stars_4() { return static_cast<int32_t>(offsetof(StarTournamentListItemData_t6FE18476EB88BBD97CE1527A1C5EE42E3A8936AD, ___Stars_4)); }
	inline int32_t get_Stars_4() const { return ___Stars_4; }
	inline int32_t* get_address_of_Stars_4() { return &___Stars_4; }
	inline void set_Stars_4(int32_t value)
	{
		___Stars_4 = value;
	}

	inline static int32_t get_offset_of_TeamName_5() { return static_cast<int32_t>(offsetof(StarTournamentListItemData_t6FE18476EB88BBD97CE1527A1C5EE42E3A8936AD, ___TeamName_5)); }
	inline String_t* get_TeamName_5() const { return ___TeamName_5; }
	inline String_t** get_address_of_TeamName_5() { return &___TeamName_5; }
	inline void set_TeamName_5(String_t* value)
	{
		___TeamName_5 = value;
		Il2CppCodeGenWriteBarrier((&___TeamName_5), value);
	}

	inline static int32_t get_offset_of_Rank_6() { return static_cast<int32_t>(offsetof(StarTournamentListItemData_t6FE18476EB88BBD97CE1527A1C5EE42E3A8936AD, ___Rank_6)); }
	inline int32_t get_Rank_6() const { return ___Rank_6; }
	inline int32_t* get_address_of_Rank_6() { return &___Rank_6; }
	inline void set_Rank_6(int32_t value)
	{
		___Rank_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STARTOURNAMENTLISTITEMDATA_T6FE18476EB88BBD97CE1527A1C5EE42E3A8936AD_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef BASICTSYSTEM_TD6508F31E78D89E2227A8E4AD3D70DFDEBCB07F8_H
#define BASICTSYSTEM_TD6508F31E78D89E2227A8E4AD3D70DFDEBCB07F8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.BasicTSystem
struct  BasicTSystem_tD6508F31E78D89E2227A8E4AD3D70DFDEBCB07F8  : public RuntimeObject
{
public:
	// Zenject.DisposableManager Tayr.BasicTSystem::_disposableManager
	DisposableManager_tCB394E2EA1FE174EB07A1082C5F1760FFE6F9ADC * ____disposableManager_0;
	// Tayr.TSystemsManager Tayr.BasicTSystem::_systemsManager
	TSystemsManager_t22A7C9150EDDB2B8046FF0832CBAEDA8FAEB8462 * ____systemsManager_1;
	// System.Boolean Tayr.BasicTSystem::<IsInitialized>k__BackingField
	bool ___U3CIsInitializedU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of__disposableManager_0() { return static_cast<int32_t>(offsetof(BasicTSystem_tD6508F31E78D89E2227A8E4AD3D70DFDEBCB07F8, ____disposableManager_0)); }
	inline DisposableManager_tCB394E2EA1FE174EB07A1082C5F1760FFE6F9ADC * get__disposableManager_0() const { return ____disposableManager_0; }
	inline DisposableManager_tCB394E2EA1FE174EB07A1082C5F1760FFE6F9ADC ** get_address_of__disposableManager_0() { return &____disposableManager_0; }
	inline void set__disposableManager_0(DisposableManager_tCB394E2EA1FE174EB07A1082C5F1760FFE6F9ADC * value)
	{
		____disposableManager_0 = value;
		Il2CppCodeGenWriteBarrier((&____disposableManager_0), value);
	}

	inline static int32_t get_offset_of__systemsManager_1() { return static_cast<int32_t>(offsetof(BasicTSystem_tD6508F31E78D89E2227A8E4AD3D70DFDEBCB07F8, ____systemsManager_1)); }
	inline TSystemsManager_t22A7C9150EDDB2B8046FF0832CBAEDA8FAEB8462 * get__systemsManager_1() const { return ____systemsManager_1; }
	inline TSystemsManager_t22A7C9150EDDB2B8046FF0832CBAEDA8FAEB8462 ** get_address_of__systemsManager_1() { return &____systemsManager_1; }
	inline void set__systemsManager_1(TSystemsManager_t22A7C9150EDDB2B8046FF0832CBAEDA8FAEB8462 * value)
	{
		____systemsManager_1 = value;
		Il2CppCodeGenWriteBarrier((&____systemsManager_1), value);
	}

	inline static int32_t get_offset_of_U3CIsInitializedU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(BasicTSystem_tD6508F31E78D89E2227A8E4AD3D70DFDEBCB07F8, ___U3CIsInitializedU3Ek__BackingField_2)); }
	inline bool get_U3CIsInitializedU3Ek__BackingField_2() const { return ___U3CIsInitializedU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CIsInitializedU3Ek__BackingField_2() { return &___U3CIsInitializedU3Ek__BackingField_2; }
	inline void set_U3CIsInitializedU3Ek__BackingField_2(bool value)
	{
		___U3CIsInitializedU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICTSYSTEM_TD6508F31E78D89E2227A8E4AD3D70DFDEBCB07F8_H
#ifndef TDYNAMICLISTDATA_T634C7C07588B155397EB9DB5A3825AC657E12369_H
#define TDYNAMICLISTDATA_T634C7C07588B155397EB9DB5A3825AC657E12369_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.TDynamicListData
struct  TDynamicListData_t634C7C07588B155397EB9DB5A3825AC657E12369  : public RuntimeObject
{
public:
	// UnityEngine.GameObject Tayr.TDynamicListData::Prefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___Prefab_0;

public:
	inline static int32_t get_offset_of_Prefab_0() { return static_cast<int32_t>(offsetof(TDynamicListData_t634C7C07588B155397EB9DB5A3825AC657E12369, ___Prefab_0)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_Prefab_0() const { return ___Prefab_0; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_Prefab_0() { return &___Prefab_0; }
	inline void set_Prefab_0(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___Prefab_0 = value;
		Il2CppCodeGenWriteBarrier((&___Prefab_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TDYNAMICLISTDATA_T634C7C07588B155397EB9DB5A3825AC657E12369_H
#ifndef TOURNAMENTSMODEL_T51DD2964A1114E748D15DE84191A0CC3F8C9A55B_H
#define TOURNAMENTSMODEL_T51DD2964A1114E748D15DE84191A0CC3F8C9A55B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TournamentsModel
struct  TournamentsModel_t51DD2964A1114E748D15DE84191A0CC3F8C9A55B  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,TournamentModel> TournamentsModel::tournaments
	Dictionary_2_t0D44CD823085042D11C357BC35A9D64FFB54556A * ___tournaments_0;

public:
	inline static int32_t get_offset_of_tournaments_0() { return static_cast<int32_t>(offsetof(TournamentsModel_t51DD2964A1114E748D15DE84191A0CC3F8C9A55B, ___tournaments_0)); }
	inline Dictionary_2_t0D44CD823085042D11C357BC35A9D64FFB54556A * get_tournaments_0() const { return ___tournaments_0; }
	inline Dictionary_2_t0D44CD823085042D11C357BC35A9D64FFB54556A ** get_address_of_tournaments_0() { return &___tournaments_0; }
	inline void set_tournaments_0(Dictionary_2_t0D44CD823085042D11C357BC35A9D64FFB54556A * value)
	{
		___tournaments_0 = value;
		Il2CppCodeGenWriteBarrier((&___tournaments_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOURNAMENTSMODEL_T51DD2964A1114E748D15DE84191A0CC3F8C9A55B_H
#ifndef TRIBETOURNAMENTCONTRIBUTIONLISTITEMDATA_TF9C29726809FAC898576A1F8CC49BDA2D376924F_H
#define TRIBETOURNAMENTCONTRIBUTIONLISTITEMDATA_TF9C29726809FAC898576A1F8CC49BDA2D376924F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TribeTournamentContributionListItemData
struct  TribeTournamentContributionListItemData_tF9C29726809FAC898576A1F8CC49BDA2D376924F  : public RuntimeObject
{
public:
	// System.String TribeTournamentContributionListItemData::PlayerId
	String_t* ___PlayerId_0;
	// System.String TribeTournamentContributionListItemData::PlayerName
	String_t* ___PlayerName_1;
	// System.Int32 TribeTournamentContributionListItemData::Stars
	int32_t ___Stars_2;
	// System.Int32 TribeTournamentContributionListItemData::Rank
	int32_t ___Rank_3;
	// System.Boolean TribeTournamentContributionListItemData::IsParticipant
	bool ___IsParticipant_4;

public:
	inline static int32_t get_offset_of_PlayerId_0() { return static_cast<int32_t>(offsetof(TribeTournamentContributionListItemData_tF9C29726809FAC898576A1F8CC49BDA2D376924F, ___PlayerId_0)); }
	inline String_t* get_PlayerId_0() const { return ___PlayerId_0; }
	inline String_t** get_address_of_PlayerId_0() { return &___PlayerId_0; }
	inline void set_PlayerId_0(String_t* value)
	{
		___PlayerId_0 = value;
		Il2CppCodeGenWriteBarrier((&___PlayerId_0), value);
	}

	inline static int32_t get_offset_of_PlayerName_1() { return static_cast<int32_t>(offsetof(TribeTournamentContributionListItemData_tF9C29726809FAC898576A1F8CC49BDA2D376924F, ___PlayerName_1)); }
	inline String_t* get_PlayerName_1() const { return ___PlayerName_1; }
	inline String_t** get_address_of_PlayerName_1() { return &___PlayerName_1; }
	inline void set_PlayerName_1(String_t* value)
	{
		___PlayerName_1 = value;
		Il2CppCodeGenWriteBarrier((&___PlayerName_1), value);
	}

	inline static int32_t get_offset_of_Stars_2() { return static_cast<int32_t>(offsetof(TribeTournamentContributionListItemData_tF9C29726809FAC898576A1F8CC49BDA2D376924F, ___Stars_2)); }
	inline int32_t get_Stars_2() const { return ___Stars_2; }
	inline int32_t* get_address_of_Stars_2() { return &___Stars_2; }
	inline void set_Stars_2(int32_t value)
	{
		___Stars_2 = value;
	}

	inline static int32_t get_offset_of_Rank_3() { return static_cast<int32_t>(offsetof(TribeTournamentContributionListItemData_tF9C29726809FAC898576A1F8CC49BDA2D376924F, ___Rank_3)); }
	inline int32_t get_Rank_3() const { return ___Rank_3; }
	inline int32_t* get_address_of_Rank_3() { return &___Rank_3; }
	inline void set_Rank_3(int32_t value)
	{
		___Rank_3 = value;
	}

	inline static int32_t get_offset_of_IsParticipant_4() { return static_cast<int32_t>(offsetof(TribeTournamentContributionListItemData_tF9C29726809FAC898576A1F8CC49BDA2D376924F, ___IsParticipant_4)); }
	inline bool get_IsParticipant_4() const { return ___IsParticipant_4; }
	inline bool* get_address_of_IsParticipant_4() { return &___IsParticipant_4; }
	inline void set_IsParticipant_4(bool value)
	{
		___IsParticipant_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIBETOURNAMENTCONTRIBUTIONLISTITEMDATA_TF9C29726809FAC898576A1F8CC49BDA2D376924F_H
#ifndef TRIBETOURNAMENTLISTITEMDATA_T972BE1E8BC71C51099D379602320348346EBEE14_H
#define TRIBETOURNAMENTLISTITEMDATA_T972BE1E8BC71C51099D379602320348346EBEE14_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TribeTournamentListItemData
struct  TribeTournamentListItemData_t972BE1E8BC71C51099D379602320348346EBEE14  : public RuntimeObject
{
public:
	// System.Int32 TribeTournamentListItemData::Icon
	int32_t ___Icon_0;
	// System.String TribeTournamentListItemData::TeamName
	String_t* ___TeamName_1;
	// System.String TribeTournamentListItemData::TeamId
	String_t* ___TeamId_2;
	// System.Int32 TribeTournamentListItemData::Stars
	int32_t ___Stars_3;
	// System.Int32 TribeTournamentListItemData::Rank
	int32_t ___Rank_4;

public:
	inline static int32_t get_offset_of_Icon_0() { return static_cast<int32_t>(offsetof(TribeTournamentListItemData_t972BE1E8BC71C51099D379602320348346EBEE14, ___Icon_0)); }
	inline int32_t get_Icon_0() const { return ___Icon_0; }
	inline int32_t* get_address_of_Icon_0() { return &___Icon_0; }
	inline void set_Icon_0(int32_t value)
	{
		___Icon_0 = value;
	}

	inline static int32_t get_offset_of_TeamName_1() { return static_cast<int32_t>(offsetof(TribeTournamentListItemData_t972BE1E8BC71C51099D379602320348346EBEE14, ___TeamName_1)); }
	inline String_t* get_TeamName_1() const { return ___TeamName_1; }
	inline String_t** get_address_of_TeamName_1() { return &___TeamName_1; }
	inline void set_TeamName_1(String_t* value)
	{
		___TeamName_1 = value;
		Il2CppCodeGenWriteBarrier((&___TeamName_1), value);
	}

	inline static int32_t get_offset_of_TeamId_2() { return static_cast<int32_t>(offsetof(TribeTournamentListItemData_t972BE1E8BC71C51099D379602320348346EBEE14, ___TeamId_2)); }
	inline String_t* get_TeamId_2() const { return ___TeamId_2; }
	inline String_t** get_address_of_TeamId_2() { return &___TeamId_2; }
	inline void set_TeamId_2(String_t* value)
	{
		___TeamId_2 = value;
		Il2CppCodeGenWriteBarrier((&___TeamId_2), value);
	}

	inline static int32_t get_offset_of_Stars_3() { return static_cast<int32_t>(offsetof(TribeTournamentListItemData_t972BE1E8BC71C51099D379602320348346EBEE14, ___Stars_3)); }
	inline int32_t get_Stars_3() const { return ___Stars_3; }
	inline int32_t* get_address_of_Stars_3() { return &___Stars_3; }
	inline void set_Stars_3(int32_t value)
	{
		___Stars_3 = value;
	}

	inline static int32_t get_offset_of_Rank_4() { return static_cast<int32_t>(offsetof(TribeTournamentListItemData_t972BE1E8BC71C51099D379602320348346EBEE14, ___Rank_4)); }
	inline int32_t get_Rank_4() const { return ___Rank_4; }
	inline int32_t* get_address_of_Rank_4() { return &___Rank_4; }
	inline void set_Rank_4(int32_t value)
	{
		___Rank_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIBETOURNAMENTLISTITEMDATA_T972BE1E8BC71C51099D379602320348346EBEE14_H
#ifndef TUTORIALUTILS_T7425E0BFABD0CF5606ECFC8C2BA50802577072F5_H
#define TUTORIALUTILS_T7425E0BFABD0CF5606ECFC8C2BA50802577072F5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TutorialUtils
struct  TutorialUtils_t7425E0BFABD0CF5606ECFC8C2BA50802577072F5  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUTORIALUTILS_T7425E0BFABD0CF5606ECFC8C2BA50802577072F5_H
#ifndef UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#define UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * ___m_PersistentCalls_1;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_2;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_Calls_0)); }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_CallsDirty_2)); }
	inline bool get_m_CallsDirty_2() const { return ___m_CallsDirty_2; }
	inline bool* get_address_of_m_CallsDirty_2() { return &___m_CallsDirty_2; }
	inline void set_m_CallsDirty_2(bool value)
	{
		___m_CallsDirty_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#ifndef BATTLEINTROFLOW_T6B627D5733BE5B127F2EBBABF6747828F5F35D36_H
#define BATTLEINTROFLOW_T6B627D5733BE5B127F2EBBABF6747828F5F35D36_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BattleIntroFlow
struct  BattleIntroFlow_t6B627D5733BE5B127F2EBBABF6747828F5F35D36  : public BasicTutorialFlow_tBA017E4C87F10BC8DD5BA4E75B90C9AFD0A3730B
{
public:
	// Tayr.ILibrary BattleIntroFlow::_uiMain
	RuntimeObject* ____uiMain_0;
	// Contexts BattleIntroFlow::_contexts
	Contexts_t313FEE68C5FB0568E30785C33C0F6209F5F7B87D * ____contexts_1;

public:
	inline static int32_t get_offset_of__uiMain_0() { return static_cast<int32_t>(offsetof(BattleIntroFlow_t6B627D5733BE5B127F2EBBABF6747828F5F35D36, ____uiMain_0)); }
	inline RuntimeObject* get__uiMain_0() const { return ____uiMain_0; }
	inline RuntimeObject** get_address_of__uiMain_0() { return &____uiMain_0; }
	inline void set__uiMain_0(RuntimeObject* value)
	{
		____uiMain_0 = value;
		Il2CppCodeGenWriteBarrier((&____uiMain_0), value);
	}

	inline static int32_t get_offset_of__contexts_1() { return static_cast<int32_t>(offsetof(BattleIntroFlow_t6B627D5733BE5B127F2EBBABF6747828F5F35D36, ____contexts_1)); }
	inline Contexts_t313FEE68C5FB0568E30785C33C0F6209F5F7B87D * get__contexts_1() const { return ____contexts_1; }
	inline Contexts_t313FEE68C5FB0568E30785C33C0F6209F5F7B87D ** get_address_of__contexts_1() { return &____contexts_1; }
	inline void set__contexts_1(Contexts_t313FEE68C5FB0568E30785C33C0F6209F5F7B87D * value)
	{
		____contexts_1 = value;
		Il2CppCodeGenWriteBarrier((&____contexts_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BATTLEINTROFLOW_T6B627D5733BE5B127F2EBBABF6747828F5F35D36_H
#ifndef BATTLEINVENTORYFLOW_T7F82684619055127E49D19EEF227C08632E08E84_H
#define BATTLEINVENTORYFLOW_T7F82684619055127E49D19EEF227C08632E08E84_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BattleInventoryFlow
struct  BattleInventoryFlow_t7F82684619055127E49D19EEF227C08632E08E84  : public BasicTutorialFlow_tBA017E4C87F10BC8DD5BA4E75B90C9AFD0A3730B
{
public:
	// Tayr.ILibrary BattleInventoryFlow::_uiMain
	RuntimeObject* ____uiMain_0;
	// Contexts BattleInventoryFlow::_contexts
	Contexts_t313FEE68C5FB0568E30785C33C0F6209F5F7B87D * ____contexts_1;

public:
	inline static int32_t get_offset_of__uiMain_0() { return static_cast<int32_t>(offsetof(BattleInventoryFlow_t7F82684619055127E49D19EEF227C08632E08E84, ____uiMain_0)); }
	inline RuntimeObject* get__uiMain_0() const { return ____uiMain_0; }
	inline RuntimeObject** get_address_of__uiMain_0() { return &____uiMain_0; }
	inline void set__uiMain_0(RuntimeObject* value)
	{
		____uiMain_0 = value;
		Il2CppCodeGenWriteBarrier((&____uiMain_0), value);
	}

	inline static int32_t get_offset_of__contexts_1() { return static_cast<int32_t>(offsetof(BattleInventoryFlow_t7F82684619055127E49D19EEF227C08632E08E84, ____contexts_1)); }
	inline Contexts_t313FEE68C5FB0568E30785C33C0F6209F5F7B87D * get__contexts_1() const { return ____contexts_1; }
	inline Contexts_t313FEE68C5FB0568E30785C33C0F6209F5F7B87D ** get_address_of__contexts_1() { return &____contexts_1; }
	inline void set__contexts_1(Contexts_t313FEE68C5FB0568E30785C33C0F6209F5F7B87D * value)
	{
		____contexts_1 = value;
		Il2CppCodeGenWriteBarrier((&____contexts_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BATTLEINVENTORYFLOW_T7F82684619055127E49D19EEF227C08632E08E84_H
#ifndef CHATLISTITEMDATA_TB039C181C297B802B7B35327AA170A2106CDD53B_H
#define CHATLISTITEMDATA_TB039C181C297B802B7B35327AA170A2106CDD53B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChatListItemData
struct  ChatListItemData_tB039C181C297B802B7B35327AA170A2106CDD53B  : public TDynamicListData_t634C7C07588B155397EB9DB5A3825AC657E12369
{
public:
	// System.String ChatListItemData::MessageId
	String_t* ___MessageId_1;
	// System.String ChatListItemData::SenderId
	String_t* ___SenderId_2;
	// System.String ChatListItemData::SenderDisplayName
	String_t* ___SenderDisplayName_3;
	// System.String ChatListItemData::Message
	String_t* ___Message_4;
	// CustomMessageData ChatListItemData::CustomMessageData
	CustomMessageData_t2BA087DE2A16E4C6D102083A45431AA2E422A6DD * ___CustomMessageData_5;
	// System.Int64 ChatListItemData::EpochTime
	int64_t ___EpochTime_6;
	// Tayr.TDynamicList ChatListItemData::List
	TDynamicList_tF4B70F991EE13A3CB137902A92EC1A729C06DB19 * ___List_7;

public:
	inline static int32_t get_offset_of_MessageId_1() { return static_cast<int32_t>(offsetof(ChatListItemData_tB039C181C297B802B7B35327AA170A2106CDD53B, ___MessageId_1)); }
	inline String_t* get_MessageId_1() const { return ___MessageId_1; }
	inline String_t** get_address_of_MessageId_1() { return &___MessageId_1; }
	inline void set_MessageId_1(String_t* value)
	{
		___MessageId_1 = value;
		Il2CppCodeGenWriteBarrier((&___MessageId_1), value);
	}

	inline static int32_t get_offset_of_SenderId_2() { return static_cast<int32_t>(offsetof(ChatListItemData_tB039C181C297B802B7B35327AA170A2106CDD53B, ___SenderId_2)); }
	inline String_t* get_SenderId_2() const { return ___SenderId_2; }
	inline String_t** get_address_of_SenderId_2() { return &___SenderId_2; }
	inline void set_SenderId_2(String_t* value)
	{
		___SenderId_2 = value;
		Il2CppCodeGenWriteBarrier((&___SenderId_2), value);
	}

	inline static int32_t get_offset_of_SenderDisplayName_3() { return static_cast<int32_t>(offsetof(ChatListItemData_tB039C181C297B802B7B35327AA170A2106CDD53B, ___SenderDisplayName_3)); }
	inline String_t* get_SenderDisplayName_3() const { return ___SenderDisplayName_3; }
	inline String_t** get_address_of_SenderDisplayName_3() { return &___SenderDisplayName_3; }
	inline void set_SenderDisplayName_3(String_t* value)
	{
		___SenderDisplayName_3 = value;
		Il2CppCodeGenWriteBarrier((&___SenderDisplayName_3), value);
	}

	inline static int32_t get_offset_of_Message_4() { return static_cast<int32_t>(offsetof(ChatListItemData_tB039C181C297B802B7B35327AA170A2106CDD53B, ___Message_4)); }
	inline String_t* get_Message_4() const { return ___Message_4; }
	inline String_t** get_address_of_Message_4() { return &___Message_4; }
	inline void set_Message_4(String_t* value)
	{
		___Message_4 = value;
		Il2CppCodeGenWriteBarrier((&___Message_4), value);
	}

	inline static int32_t get_offset_of_CustomMessageData_5() { return static_cast<int32_t>(offsetof(ChatListItemData_tB039C181C297B802B7B35327AA170A2106CDD53B, ___CustomMessageData_5)); }
	inline CustomMessageData_t2BA087DE2A16E4C6D102083A45431AA2E422A6DD * get_CustomMessageData_5() const { return ___CustomMessageData_5; }
	inline CustomMessageData_t2BA087DE2A16E4C6D102083A45431AA2E422A6DD ** get_address_of_CustomMessageData_5() { return &___CustomMessageData_5; }
	inline void set_CustomMessageData_5(CustomMessageData_t2BA087DE2A16E4C6D102083A45431AA2E422A6DD * value)
	{
		___CustomMessageData_5 = value;
		Il2CppCodeGenWriteBarrier((&___CustomMessageData_5), value);
	}

	inline static int32_t get_offset_of_EpochTime_6() { return static_cast<int32_t>(offsetof(ChatListItemData_tB039C181C297B802B7B35327AA170A2106CDD53B, ___EpochTime_6)); }
	inline int64_t get_EpochTime_6() const { return ___EpochTime_6; }
	inline int64_t* get_address_of_EpochTime_6() { return &___EpochTime_6; }
	inline void set_EpochTime_6(int64_t value)
	{
		___EpochTime_6 = value;
	}

	inline static int32_t get_offset_of_List_7() { return static_cast<int32_t>(offsetof(ChatListItemData_tB039C181C297B802B7B35327AA170A2106CDD53B, ___List_7)); }
	inline TDynamicList_tF4B70F991EE13A3CB137902A92EC1A729C06DB19 * get_List_7() const { return ___List_7; }
	inline TDynamicList_tF4B70F991EE13A3CB137902A92EC1A729C06DB19 ** get_address_of_List_7() { return &___List_7; }
	inline void set_List_7(TDynamicList_tF4B70F991EE13A3CB137902A92EC1A729C06DB19 * value)
	{
		___List_7 = value;
		Il2CppCodeGenWriteBarrier((&___List_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHATLISTITEMDATA_TB039C181C297B802B7B35327AA170A2106CDD53B_H
#ifndef COLLECTGOALFLOW_TB876F66620038DC89241A21E6C080DA1B60BA94C_H
#define COLLECTGOALFLOW_TB876F66620038DC89241A21E6C080DA1B60BA94C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CollectGoalFlow
struct  CollectGoalFlow_tB876F66620038DC89241A21E6C080DA1B60BA94C  : public BasicTutorialFlow_tBA017E4C87F10BC8DD5BA4E75B90C9AFD0A3730B
{
public:
	// Tayr.ILibrary CollectGoalFlow::_uiMain
	RuntimeObject* ____uiMain_0;
	// Contexts CollectGoalFlow::_contexts
	Contexts_t313FEE68C5FB0568E30785C33C0F6209F5F7B87D * ____contexts_1;
	// Zenject.DiContainer CollectGoalFlow::_container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____container_2;

public:
	inline static int32_t get_offset_of__uiMain_0() { return static_cast<int32_t>(offsetof(CollectGoalFlow_tB876F66620038DC89241A21E6C080DA1B60BA94C, ____uiMain_0)); }
	inline RuntimeObject* get__uiMain_0() const { return ____uiMain_0; }
	inline RuntimeObject** get_address_of__uiMain_0() { return &____uiMain_0; }
	inline void set__uiMain_0(RuntimeObject* value)
	{
		____uiMain_0 = value;
		Il2CppCodeGenWriteBarrier((&____uiMain_0), value);
	}

	inline static int32_t get_offset_of__contexts_1() { return static_cast<int32_t>(offsetof(CollectGoalFlow_tB876F66620038DC89241A21E6C080DA1B60BA94C, ____contexts_1)); }
	inline Contexts_t313FEE68C5FB0568E30785C33C0F6209F5F7B87D * get__contexts_1() const { return ____contexts_1; }
	inline Contexts_t313FEE68C5FB0568E30785C33C0F6209F5F7B87D ** get_address_of__contexts_1() { return &____contexts_1; }
	inline void set__contexts_1(Contexts_t313FEE68C5FB0568E30785C33C0F6209F5F7B87D * value)
	{
		____contexts_1 = value;
		Il2CppCodeGenWriteBarrier((&____contexts_1), value);
	}

	inline static int32_t get_offset_of__container_2() { return static_cast<int32_t>(offsetof(CollectGoalFlow_tB876F66620038DC89241A21E6C080DA1B60BA94C, ____container_2)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__container_2() const { return ____container_2; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__container_2() { return &____container_2; }
	inline void set__container_2(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____container_2 = value;
		Il2CppCodeGenWriteBarrier((&____container_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTGOALFLOW_TB876F66620038DC89241A21E6C080DA1B60BA94C_H
#ifndef FIRSTSKILLFLOW_T178220FD212277F8C8E0CF2D00031184B612455F_H
#define FIRSTSKILLFLOW_T178220FD212277F8C8E0CF2D00031184B612455F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FirstSkillFlow
struct  FirstSkillFlow_t178220FD212277F8C8E0CF2D00031184B612455F  : public BasicTutorialFlow_tBA017E4C87F10BC8DD5BA4E75B90C9AFD0A3730B
{
public:
	// Tayr.ILibrary FirstSkillFlow::_uiMain
	RuntimeObject* ____uiMain_0;
	// SpellUIInitializer FirstSkillFlow::_spellInitilizer
	SpellUIInitializer_t66B06A358BA3B762376CD13F820EFC620ADFDAD8 * ____spellInitilizer_1;
	// Contexts FirstSkillFlow::_contexts
	Contexts_t313FEE68C5FB0568E30785C33C0F6209F5F7B87D * ____contexts_2;
	// Zenject.DiContainer FirstSkillFlow::_container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____container_3;

public:
	inline static int32_t get_offset_of__uiMain_0() { return static_cast<int32_t>(offsetof(FirstSkillFlow_t178220FD212277F8C8E0CF2D00031184B612455F, ____uiMain_0)); }
	inline RuntimeObject* get__uiMain_0() const { return ____uiMain_0; }
	inline RuntimeObject** get_address_of__uiMain_0() { return &____uiMain_0; }
	inline void set__uiMain_0(RuntimeObject* value)
	{
		____uiMain_0 = value;
		Il2CppCodeGenWriteBarrier((&____uiMain_0), value);
	}

	inline static int32_t get_offset_of__spellInitilizer_1() { return static_cast<int32_t>(offsetof(FirstSkillFlow_t178220FD212277F8C8E0CF2D00031184B612455F, ____spellInitilizer_1)); }
	inline SpellUIInitializer_t66B06A358BA3B762376CD13F820EFC620ADFDAD8 * get__spellInitilizer_1() const { return ____spellInitilizer_1; }
	inline SpellUIInitializer_t66B06A358BA3B762376CD13F820EFC620ADFDAD8 ** get_address_of__spellInitilizer_1() { return &____spellInitilizer_1; }
	inline void set__spellInitilizer_1(SpellUIInitializer_t66B06A358BA3B762376CD13F820EFC620ADFDAD8 * value)
	{
		____spellInitilizer_1 = value;
		Il2CppCodeGenWriteBarrier((&____spellInitilizer_1), value);
	}

	inline static int32_t get_offset_of__contexts_2() { return static_cast<int32_t>(offsetof(FirstSkillFlow_t178220FD212277F8C8E0CF2D00031184B612455F, ____contexts_2)); }
	inline Contexts_t313FEE68C5FB0568E30785C33C0F6209F5F7B87D * get__contexts_2() const { return ____contexts_2; }
	inline Contexts_t313FEE68C5FB0568E30785C33C0F6209F5F7B87D ** get_address_of__contexts_2() { return &____contexts_2; }
	inline void set__contexts_2(Contexts_t313FEE68C5FB0568E30785C33C0F6209F5F7B87D * value)
	{
		____contexts_2 = value;
		Il2CppCodeGenWriteBarrier((&____contexts_2), value);
	}

	inline static int32_t get_offset_of__container_3() { return static_cast<int32_t>(offsetof(FirstSkillFlow_t178220FD212277F8C8E0CF2D00031184B612455F, ____container_3)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__container_3() const { return ____container_3; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__container_3() { return &____container_3; }
	inline void set__container_3(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____container_3 = value;
		Il2CppCodeGenWriteBarrier((&____container_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIRSTSKILLFLOW_T178220FD212277F8C8E0CF2D00031184B612455F_H
#ifndef FOURTHSKILLFLOW_T30FF5419C188236877763F85BAD84CE9339BBA91_H
#define FOURTHSKILLFLOW_T30FF5419C188236877763F85BAD84CE9339BBA91_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FourthSkillFlow
struct  FourthSkillFlow_t30FF5419C188236877763F85BAD84CE9339BBA91  : public BasicTutorialFlow_tBA017E4C87F10BC8DD5BA4E75B90C9AFD0A3730B
{
public:
	// Tayr.ILibrary FourthSkillFlow::_uiMain
	RuntimeObject* ____uiMain_0;
	// SpellUIInitializer FourthSkillFlow::_spellInitilizer
	SpellUIInitializer_t66B06A358BA3B762376CD13F820EFC620ADFDAD8 * ____spellInitilizer_1;
	// Contexts FourthSkillFlow::_contexts
	Contexts_t313FEE68C5FB0568E30785C33C0F6209F5F7B87D * ____contexts_2;

public:
	inline static int32_t get_offset_of__uiMain_0() { return static_cast<int32_t>(offsetof(FourthSkillFlow_t30FF5419C188236877763F85BAD84CE9339BBA91, ____uiMain_0)); }
	inline RuntimeObject* get__uiMain_0() const { return ____uiMain_0; }
	inline RuntimeObject** get_address_of__uiMain_0() { return &____uiMain_0; }
	inline void set__uiMain_0(RuntimeObject* value)
	{
		____uiMain_0 = value;
		Il2CppCodeGenWriteBarrier((&____uiMain_0), value);
	}

	inline static int32_t get_offset_of__spellInitilizer_1() { return static_cast<int32_t>(offsetof(FourthSkillFlow_t30FF5419C188236877763F85BAD84CE9339BBA91, ____spellInitilizer_1)); }
	inline SpellUIInitializer_t66B06A358BA3B762376CD13F820EFC620ADFDAD8 * get__spellInitilizer_1() const { return ____spellInitilizer_1; }
	inline SpellUIInitializer_t66B06A358BA3B762376CD13F820EFC620ADFDAD8 ** get_address_of__spellInitilizer_1() { return &____spellInitilizer_1; }
	inline void set__spellInitilizer_1(SpellUIInitializer_t66B06A358BA3B762376CD13F820EFC620ADFDAD8 * value)
	{
		____spellInitilizer_1 = value;
		Il2CppCodeGenWriteBarrier((&____spellInitilizer_1), value);
	}

	inline static int32_t get_offset_of__contexts_2() { return static_cast<int32_t>(offsetof(FourthSkillFlow_t30FF5419C188236877763F85BAD84CE9339BBA91, ____contexts_2)); }
	inline Contexts_t313FEE68C5FB0568E30785C33C0F6209F5F7B87D * get__contexts_2() const { return ____contexts_2; }
	inline Contexts_t313FEE68C5FB0568E30785C33C0F6209F5F7B87D ** get_address_of__contexts_2() { return &____contexts_2; }
	inline void set__contexts_2(Contexts_t313FEE68C5FB0568E30785C33C0F6209F5F7B87D * value)
	{
		____contexts_2 = value;
		Il2CppCodeGenWriteBarrier((&____contexts_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FOURTHSKILLFLOW_T30FF5419C188236877763F85BAD84CE9339BBA91_H
#ifndef NOTIFICATIONSSYSTEM_T112A3E3163906777DF795E66F73BBA012B942BEC_H
#define NOTIFICATIONSSYSTEM_T112A3E3163906777DF795E66F73BBA012B942BEC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NotificationsSystem
struct  NotificationsSystem_t112A3E3163906777DF795E66F73BBA012B942BEC  : public BasicTSystem_tD6508F31E78D89E2227A8E4AD3D70DFDEBCB07F8
{
public:
	// NotificationSamples.GameNotificationsManager NotificationsSystem::_notificationsManager
	GameNotificationsManager_tA91A2FAF80490C54B58C02C8B5652AFA290D6B9F * ____notificationsManager_3;

public:
	inline static int32_t get_offset_of__notificationsManager_3() { return static_cast<int32_t>(offsetof(NotificationsSystem_t112A3E3163906777DF795E66F73BBA012B942BEC, ____notificationsManager_3)); }
	inline GameNotificationsManager_tA91A2FAF80490C54B58C02C8B5652AFA290D6B9F * get__notificationsManager_3() const { return ____notificationsManager_3; }
	inline GameNotificationsManager_tA91A2FAF80490C54B58C02C8B5652AFA290D6B9F ** get_address_of__notificationsManager_3() { return &____notificationsManager_3; }
	inline void set__notificationsManager_3(GameNotificationsManager_tA91A2FAF80490C54B58C02C8B5652AFA290D6B9F * value)
	{
		____notificationsManager_3 = value;
		Il2CppCodeGenWriteBarrier((&____notificationsManager_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTIFICATIONSSYSTEM_T112A3E3163906777DF795E66F73BBA012B942BEC_H
#ifndef SECONDSKILLFLOW_T56C78A79BCE637FB296A312521CD8D3C5FE89165_H
#define SECONDSKILLFLOW_T56C78A79BCE637FB296A312521CD8D3C5FE89165_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SecondSkillFlow
struct  SecondSkillFlow_t56C78A79BCE637FB296A312521CD8D3C5FE89165  : public BasicTutorialFlow_tBA017E4C87F10BC8DD5BA4E75B90C9AFD0A3730B
{
public:
	// Tayr.ILibrary SecondSkillFlow::_uiMain
	RuntimeObject* ____uiMain_0;
	// SpellUIInitializer SecondSkillFlow::_spellInitilizer
	SpellUIInitializer_t66B06A358BA3B762376CD13F820EFC620ADFDAD8 * ____spellInitilizer_1;

public:
	inline static int32_t get_offset_of__uiMain_0() { return static_cast<int32_t>(offsetof(SecondSkillFlow_t56C78A79BCE637FB296A312521CD8D3C5FE89165, ____uiMain_0)); }
	inline RuntimeObject* get__uiMain_0() const { return ____uiMain_0; }
	inline RuntimeObject** get_address_of__uiMain_0() { return &____uiMain_0; }
	inline void set__uiMain_0(RuntimeObject* value)
	{
		____uiMain_0 = value;
		Il2CppCodeGenWriteBarrier((&____uiMain_0), value);
	}

	inline static int32_t get_offset_of__spellInitilizer_1() { return static_cast<int32_t>(offsetof(SecondSkillFlow_t56C78A79BCE637FB296A312521CD8D3C5FE89165, ____spellInitilizer_1)); }
	inline SpellUIInitializer_t66B06A358BA3B762376CD13F820EFC620ADFDAD8 * get__spellInitilizer_1() const { return ____spellInitilizer_1; }
	inline SpellUIInitializer_t66B06A358BA3B762376CD13F820EFC620ADFDAD8 ** get_address_of__spellInitilizer_1() { return &____spellInitilizer_1; }
	inline void set__spellInitilizer_1(SpellUIInitializer_t66B06A358BA3B762376CD13F820EFC620ADFDAD8 * value)
	{
		____spellInitilizer_1 = value;
		Il2CppCodeGenWriteBarrier((&____spellInitilizer_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECONDSKILLFLOW_T56C78A79BCE637FB296A312521CD8D3C5FE89165_H
#ifndef SOUNDSYSTEM_TAD8B0EBAA6C3E35776FEC9AC64E85EE19E81694D_H
#define SOUNDSYSTEM_TAD8B0EBAA6C3E35776FEC9AC64E85EE19E81694D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SoundSystem
struct  SoundSystem_tAD8B0EBAA6C3E35776FEC9AC64E85EE19E81694D  : public BasicTSystem_tD6508F31E78D89E2227A8E4AD3D70DFDEBCB07F8
{
public:
	// UserVO SoundSystem::_userVO
	UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * ____userVO_3;
	// Tayr.TSoundSystem SoundSystem::_soundSystem
	TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511 * ____soundSystem_4;

public:
	inline static int32_t get_offset_of__userVO_3() { return static_cast<int32_t>(offsetof(SoundSystem_tAD8B0EBAA6C3E35776FEC9AC64E85EE19E81694D, ____userVO_3)); }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * get__userVO_3() const { return ____userVO_3; }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 ** get_address_of__userVO_3() { return &____userVO_3; }
	inline void set__userVO_3(UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * value)
	{
		____userVO_3 = value;
		Il2CppCodeGenWriteBarrier((&____userVO_3), value);
	}

	inline static int32_t get_offset_of__soundSystem_4() { return static_cast<int32_t>(offsetof(SoundSystem_tAD8B0EBAA6C3E35776FEC9AC64E85EE19E81694D, ____soundSystem_4)); }
	inline TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511 * get__soundSystem_4() const { return ____soundSystem_4; }
	inline TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511 ** get_address_of__soundSystem_4() { return &____soundSystem_4; }
	inline void set__soundSystem_4(TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511 * value)
	{
		____soundSystem_4 = value;
		Il2CppCodeGenWriteBarrier((&____soundSystem_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOUNDSYSTEM_TAD8B0EBAA6C3E35776FEC9AC64E85EE19E81694D_H
#ifndef STARTOURNAMENTSYSTEM_TBBE982F84537BACA13B9A61826F04C1F5F726C88_H
#define STARTOURNAMENTSYSTEM_TBBE982F84537BACA13B9A61826F04C1F5F726C88_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StarTournamentSystem
struct  StarTournamentSystem_tBBE982F84537BACA13B9A61826F04C1F5F726C88  : public BasicTSystem_tD6508F31E78D89E2227A8E4AD3D70DFDEBCB07F8
{
public:
	// Zenject.DiContainer StarTournamentSystem::_diContainer
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____diContainer_3;
	// TournamentsVO StarTournamentSystem::_tournamentsVO
	TournamentsVO_tCBA9A235AE4069263CE332B1E9150CDABD0E8016 * ____tournamentsVO_4;
	// Tayr.Trigger StarTournamentSystem::_startTrigger
	Trigger_tFA4DAF5E2FB996087C1F5DD53203E250D4770C09 * ____startTrigger_5;
	// Tayr.Trigger StarTournamentSystem::_endTrigger
	Trigger_tFA4DAF5E2FB996087C1F5DD53203E250D4770C09 * ____endTrigger_6;
	// Quest StarTournamentSystem::_starTournamentQuest
	Quest_tBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021 * ____starTournamentQuest_7;
	// StarTournamentVariable StarTournamentSystem::_startVariable
	StarTournamentVariable_tAEC9A6E89954C3E8E99380747371340583301A90 * ____startVariable_8;
	// TournamentModel StarTournamentSystem::_currentTournament
	TournamentModel_tB7076328E3D85DBCC9B2B22F1BD7B3EEC967DB3D * ____currentTournament_9;
	// System.Boolean StarTournamentSystem::_started
	bool ____started_10;
	// UnityEngine.Events.UnityEvent StarTournamentSystem::OnTournamentStart
	UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * ___OnTournamentStart_11;

public:
	inline static int32_t get_offset_of__diContainer_3() { return static_cast<int32_t>(offsetof(StarTournamentSystem_tBBE982F84537BACA13B9A61826F04C1F5F726C88, ____diContainer_3)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__diContainer_3() const { return ____diContainer_3; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__diContainer_3() { return &____diContainer_3; }
	inline void set__diContainer_3(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____diContainer_3 = value;
		Il2CppCodeGenWriteBarrier((&____diContainer_3), value);
	}

	inline static int32_t get_offset_of__tournamentsVO_4() { return static_cast<int32_t>(offsetof(StarTournamentSystem_tBBE982F84537BACA13B9A61826F04C1F5F726C88, ____tournamentsVO_4)); }
	inline TournamentsVO_tCBA9A235AE4069263CE332B1E9150CDABD0E8016 * get__tournamentsVO_4() const { return ____tournamentsVO_4; }
	inline TournamentsVO_tCBA9A235AE4069263CE332B1E9150CDABD0E8016 ** get_address_of__tournamentsVO_4() { return &____tournamentsVO_4; }
	inline void set__tournamentsVO_4(TournamentsVO_tCBA9A235AE4069263CE332B1E9150CDABD0E8016 * value)
	{
		____tournamentsVO_4 = value;
		Il2CppCodeGenWriteBarrier((&____tournamentsVO_4), value);
	}

	inline static int32_t get_offset_of__startTrigger_5() { return static_cast<int32_t>(offsetof(StarTournamentSystem_tBBE982F84537BACA13B9A61826F04C1F5F726C88, ____startTrigger_5)); }
	inline Trigger_tFA4DAF5E2FB996087C1F5DD53203E250D4770C09 * get__startTrigger_5() const { return ____startTrigger_5; }
	inline Trigger_tFA4DAF5E2FB996087C1F5DD53203E250D4770C09 ** get_address_of__startTrigger_5() { return &____startTrigger_5; }
	inline void set__startTrigger_5(Trigger_tFA4DAF5E2FB996087C1F5DD53203E250D4770C09 * value)
	{
		____startTrigger_5 = value;
		Il2CppCodeGenWriteBarrier((&____startTrigger_5), value);
	}

	inline static int32_t get_offset_of__endTrigger_6() { return static_cast<int32_t>(offsetof(StarTournamentSystem_tBBE982F84537BACA13B9A61826F04C1F5F726C88, ____endTrigger_6)); }
	inline Trigger_tFA4DAF5E2FB996087C1F5DD53203E250D4770C09 * get__endTrigger_6() const { return ____endTrigger_6; }
	inline Trigger_tFA4DAF5E2FB996087C1F5DD53203E250D4770C09 ** get_address_of__endTrigger_6() { return &____endTrigger_6; }
	inline void set__endTrigger_6(Trigger_tFA4DAF5E2FB996087C1F5DD53203E250D4770C09 * value)
	{
		____endTrigger_6 = value;
		Il2CppCodeGenWriteBarrier((&____endTrigger_6), value);
	}

	inline static int32_t get_offset_of__starTournamentQuest_7() { return static_cast<int32_t>(offsetof(StarTournamentSystem_tBBE982F84537BACA13B9A61826F04C1F5F726C88, ____starTournamentQuest_7)); }
	inline Quest_tBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021 * get__starTournamentQuest_7() const { return ____starTournamentQuest_7; }
	inline Quest_tBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021 ** get_address_of__starTournamentQuest_7() { return &____starTournamentQuest_7; }
	inline void set__starTournamentQuest_7(Quest_tBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021 * value)
	{
		____starTournamentQuest_7 = value;
		Il2CppCodeGenWriteBarrier((&____starTournamentQuest_7), value);
	}

	inline static int32_t get_offset_of__startVariable_8() { return static_cast<int32_t>(offsetof(StarTournamentSystem_tBBE982F84537BACA13B9A61826F04C1F5F726C88, ____startVariable_8)); }
	inline StarTournamentVariable_tAEC9A6E89954C3E8E99380747371340583301A90 * get__startVariable_8() const { return ____startVariable_8; }
	inline StarTournamentVariable_tAEC9A6E89954C3E8E99380747371340583301A90 ** get_address_of__startVariable_8() { return &____startVariable_8; }
	inline void set__startVariable_8(StarTournamentVariable_tAEC9A6E89954C3E8E99380747371340583301A90 * value)
	{
		____startVariable_8 = value;
		Il2CppCodeGenWriteBarrier((&____startVariable_8), value);
	}

	inline static int32_t get_offset_of__currentTournament_9() { return static_cast<int32_t>(offsetof(StarTournamentSystem_tBBE982F84537BACA13B9A61826F04C1F5F726C88, ____currentTournament_9)); }
	inline TournamentModel_tB7076328E3D85DBCC9B2B22F1BD7B3EEC967DB3D * get__currentTournament_9() const { return ____currentTournament_9; }
	inline TournamentModel_tB7076328E3D85DBCC9B2B22F1BD7B3EEC967DB3D ** get_address_of__currentTournament_9() { return &____currentTournament_9; }
	inline void set__currentTournament_9(TournamentModel_tB7076328E3D85DBCC9B2B22F1BD7B3EEC967DB3D * value)
	{
		____currentTournament_9 = value;
		Il2CppCodeGenWriteBarrier((&____currentTournament_9), value);
	}

	inline static int32_t get_offset_of__started_10() { return static_cast<int32_t>(offsetof(StarTournamentSystem_tBBE982F84537BACA13B9A61826F04C1F5F726C88, ____started_10)); }
	inline bool get__started_10() const { return ____started_10; }
	inline bool* get_address_of__started_10() { return &____started_10; }
	inline void set__started_10(bool value)
	{
		____started_10 = value;
	}

	inline static int32_t get_offset_of_OnTournamentStart_11() { return static_cast<int32_t>(offsetof(StarTournamentSystem_tBBE982F84537BACA13B9A61826F04C1F5F726C88, ___OnTournamentStart_11)); }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * get_OnTournamentStart_11() const { return ___OnTournamentStart_11; }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F ** get_address_of_OnTournamentStart_11() { return &___OnTournamentStart_11; }
	inline void set_OnTournamentStart_11(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * value)
	{
		___OnTournamentStart_11 = value;
		Il2CppCodeGenWriteBarrier((&___OnTournamentStart_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STARTOURNAMENTSYSTEM_TBBE982F84537BACA13B9A61826F04C1F5F726C88_H
#ifndef BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#define BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifndef DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#define DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 
{
public:
	// System.UInt64 System.DateTime::dateData
	uint64_t ___dateData_44;

public:
	inline static int32_t get_offset_of_dateData_44() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132, ___dateData_44)); }
	inline uint64_t get_dateData_44() const { return ___dateData_44; }
	inline uint64_t* get_address_of_dateData_44() { return &___dateData_44; }
	inline void set_dateData_44(uint64_t value)
	{
		___dateData_44 = value;
	}
};

struct DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields
{
public:
	// System.Int32[] System.DateTime::DaysToMonth365
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth365_29;
	// System.Int32[] System.DateTime::DaysToMonth366
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth366_30;
	// System.DateTime System.DateTime::MinValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MinValue_31;
	// System.DateTime System.DateTime::MaxValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MaxValue_32;

public:
	inline static int32_t get_offset_of_DaysToMonth365_29() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth365_29)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth365_29() const { return ___DaysToMonth365_29; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth365_29() { return &___DaysToMonth365_29; }
	inline void set_DaysToMonth365_29(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth365_29 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth365_29), value);
	}

	inline static int32_t get_offset_of_DaysToMonth366_30() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth366_30)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth366_30() const { return ___DaysToMonth366_30; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth366_30() { return &___DaysToMonth366_30; }
	inline void set_DaysToMonth366_30(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth366_30 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth366_30), value);
	}

	inline static int32_t get_offset_of_MinValue_31() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MinValue_31)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MinValue_31() const { return ___MinValue_31; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MinValue_31() { return &___MinValue_31; }
	inline void set_MinValue_31(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MinValue_31 = value;
	}

	inline static int32_t get_offset_of_MaxValue_32() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MaxValue_32)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MaxValue_32() const { return ___MaxValue_32; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MaxValue_32() { return &___MaxValue_32; }
	inline void set_MaxValue_32(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MaxValue_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef ASYNCMETHODBUILDERCORE_T4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01_H
#define ASYNCMETHODBUILDERCORE_T4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.AsyncMethodBuilderCore
struct  AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01 
{
public:
	// System.Runtime.CompilerServices.IAsyncStateMachine System.Runtime.CompilerServices.AsyncMethodBuilderCore::m_stateMachine
	RuntimeObject* ___m_stateMachine_0;
	// System.Action System.Runtime.CompilerServices.AsyncMethodBuilderCore::m_defaultContextAction
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___m_defaultContextAction_1;

public:
	inline static int32_t get_offset_of_m_stateMachine_0() { return static_cast<int32_t>(offsetof(AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01, ___m_stateMachine_0)); }
	inline RuntimeObject* get_m_stateMachine_0() const { return ___m_stateMachine_0; }
	inline RuntimeObject** get_address_of_m_stateMachine_0() { return &___m_stateMachine_0; }
	inline void set_m_stateMachine_0(RuntimeObject* value)
	{
		___m_stateMachine_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_stateMachine_0), value);
	}

	inline static int32_t get_offset_of_m_defaultContextAction_1() { return static_cast<int32_t>(offsetof(AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01, ___m_defaultContextAction_1)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_m_defaultContextAction_1() const { return ___m_defaultContextAction_1; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_m_defaultContextAction_1() { return &___m_defaultContextAction_1; }
	inline void set_m_defaultContextAction_1(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___m_defaultContextAction_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_defaultContextAction_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.CompilerServices.AsyncMethodBuilderCore
struct AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01_marshaled_pinvoke
{
	RuntimeObject* ___m_stateMachine_0;
	Il2CppMethodPointer ___m_defaultContextAction_1;
};
// Native definition for COM marshalling of System.Runtime.CompilerServices.AsyncMethodBuilderCore
struct AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01_marshaled_com
{
	RuntimeObject* ___m_stateMachine_0;
	Il2CppMethodPointer ___m_defaultContextAction_1;
};
#endif // ASYNCMETHODBUILDERCORE_T4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01_H
#ifndef TASKAWAITER_T0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F_H
#define TASKAWAITER_T0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.TaskAwaiter
struct  TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F 
{
public:
	// System.Threading.Tasks.Task System.Runtime.CompilerServices.TaskAwaiter::m_task
	Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * ___m_task_0;

public:
	inline static int32_t get_offset_of_m_task_0() { return static_cast<int32_t>(offsetof(TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F, ___m_task_0)); }
	inline Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * get_m_task_0() const { return ___m_task_0; }
	inline Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 ** get_address_of_m_task_0() { return &___m_task_0; }
	inline void set_m_task_0(Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * value)
	{
		___m_task_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_task_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.CompilerServices.TaskAwaiter
struct TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F_marshaled_pinvoke
{
	Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * ___m_task_0;
};
// Native definition for COM marshalling of System.Runtime.CompilerServices.TaskAwaiter
struct TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F_marshaled_com
{
	Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * ___m_task_0;
};
#endif // TASKAWAITER_T0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef TEAMSYSTEM_T0238BD7E12677CA842EEE8222E0FB9116A52D036_H
#define TEAMSYSTEM_T0238BD7E12677CA842EEE8222E0FB9116A52D036_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TeamSystem
struct  TeamSystem_t0238BD7E12677CA842EEE8222E0FB9116A52D036  : public BasicTSystem_tD6508F31E78D89E2227A8E4AD3D70DFDEBCB07F8
{
public:
	// Tayr.GameSparksPlatform TeamSystem::_gameSparks
	GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * ____gameSparks_3;
	// UserVO TeamSystem::_userVO
	UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * ____userVO_4;
	// UserEvents TeamSystem::_userEvents
	UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * ____userEvents_5;
	// Tayr.VOSaver TeamSystem::_voSaver
	VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * ____voSaver_6;

public:
	inline static int32_t get_offset_of__gameSparks_3() { return static_cast<int32_t>(offsetof(TeamSystem_t0238BD7E12677CA842EEE8222E0FB9116A52D036, ____gameSparks_3)); }
	inline GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * get__gameSparks_3() const { return ____gameSparks_3; }
	inline GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 ** get_address_of__gameSparks_3() { return &____gameSparks_3; }
	inline void set__gameSparks_3(GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * value)
	{
		____gameSparks_3 = value;
		Il2CppCodeGenWriteBarrier((&____gameSparks_3), value);
	}

	inline static int32_t get_offset_of__userVO_4() { return static_cast<int32_t>(offsetof(TeamSystem_t0238BD7E12677CA842EEE8222E0FB9116A52D036, ____userVO_4)); }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * get__userVO_4() const { return ____userVO_4; }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 ** get_address_of__userVO_4() { return &____userVO_4; }
	inline void set__userVO_4(UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * value)
	{
		____userVO_4 = value;
		Il2CppCodeGenWriteBarrier((&____userVO_4), value);
	}

	inline static int32_t get_offset_of__userEvents_5() { return static_cast<int32_t>(offsetof(TeamSystem_t0238BD7E12677CA842EEE8222E0FB9116A52D036, ____userEvents_5)); }
	inline UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * get__userEvents_5() const { return ____userEvents_5; }
	inline UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 ** get_address_of__userEvents_5() { return &____userEvents_5; }
	inline void set__userEvents_5(UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * value)
	{
		____userEvents_5 = value;
		Il2CppCodeGenWriteBarrier((&____userEvents_5), value);
	}

	inline static int32_t get_offset_of__voSaver_6() { return static_cast<int32_t>(offsetof(TeamSystem_t0238BD7E12677CA842EEE8222E0FB9116A52D036, ____voSaver_6)); }
	inline VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * get__voSaver_6() const { return ____voSaver_6; }
	inline VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F ** get_address_of__voSaver_6() { return &____voSaver_6; }
	inline void set__voSaver_6(VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * value)
	{
		____voSaver_6 = value;
		Il2CppCodeGenWriteBarrier((&____voSaver_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEAMSYSTEM_T0238BD7E12677CA842EEE8222E0FB9116A52D036_H
#ifndef THIRDSKILLFLOW_T99B9A8894864D1DBED1774C522267C4A60B76652_H
#define THIRDSKILLFLOW_T99B9A8894864D1DBED1774C522267C4A60B76652_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdSkillFlow
struct  ThirdSkillFlow_t99B9A8894864D1DBED1774C522267C4A60B76652  : public BasicTutorialFlow_tBA017E4C87F10BC8DD5BA4E75B90C9AFD0A3730B
{
public:
	// Tayr.ILibrary ThirdSkillFlow::_uiMain
	RuntimeObject* ____uiMain_0;
	// SpellUIInitializer ThirdSkillFlow::_spellInitilizer
	SpellUIInitializer_t66B06A358BA3B762376CD13F820EFC620ADFDAD8 * ____spellInitilizer_1;

public:
	inline static int32_t get_offset_of__uiMain_0() { return static_cast<int32_t>(offsetof(ThirdSkillFlow_t99B9A8894864D1DBED1774C522267C4A60B76652, ____uiMain_0)); }
	inline RuntimeObject* get__uiMain_0() const { return ____uiMain_0; }
	inline RuntimeObject** get_address_of__uiMain_0() { return &____uiMain_0; }
	inline void set__uiMain_0(RuntimeObject* value)
	{
		____uiMain_0 = value;
		Il2CppCodeGenWriteBarrier((&____uiMain_0), value);
	}

	inline static int32_t get_offset_of__spellInitilizer_1() { return static_cast<int32_t>(offsetof(ThirdSkillFlow_t99B9A8894864D1DBED1774C522267C4A60B76652, ____spellInitilizer_1)); }
	inline SpellUIInitializer_t66B06A358BA3B762376CD13F820EFC620ADFDAD8 * get__spellInitilizer_1() const { return ____spellInitilizer_1; }
	inline SpellUIInitializer_t66B06A358BA3B762376CD13F820EFC620ADFDAD8 ** get_address_of__spellInitilizer_1() { return &____spellInitilizer_1; }
	inline void set__spellInitilizer_1(SpellUIInitializer_t66B06A358BA3B762376CD13F820EFC620ADFDAD8 * value)
	{
		____spellInitilizer_1 = value;
		Il2CppCodeGenWriteBarrier((&____spellInitilizer_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THIRDSKILLFLOW_T99B9A8894864D1DBED1774C522267C4A60B76652_H
#ifndef TRIBETOURNAMENTSYSTEM_T7F0DE0115218A7B9AA81C8EF2B923A4450F36424_H
#define TRIBETOURNAMENTSYSTEM_T7F0DE0115218A7B9AA81C8EF2B923A4450F36424_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TribeTournamentSystem
struct  TribeTournamentSystem_t7F0DE0115218A7B9AA81C8EF2B923A4450F36424  : public BasicTSystem_tD6508F31E78D89E2227A8E4AD3D70DFDEBCB07F8
{
public:
	// Zenject.DiContainer TribeTournamentSystem::_diContainer
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____diContainer_3;
	// TournamentsVO TribeTournamentSystem::_tournamentsVO
	TournamentsVO_tCBA9A235AE4069263CE332B1E9150CDABD0E8016 * ____tournamentsVO_4;
	// Tayr.Trigger TribeTournamentSystem::_startTrigger
	Trigger_tFA4DAF5E2FB996087C1F5DD53203E250D4770C09 * ____startTrigger_5;
	// Tayr.Trigger TribeTournamentSystem::_endTrigger
	Trigger_tFA4DAF5E2FB996087C1F5DD53203E250D4770C09 * ____endTrigger_6;
	// Quest TribeTournamentSystem::_starTournamentQuest
	Quest_tBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021 * ____starTournamentQuest_7;
	// TribeTournamentVariable TribeTournamentSystem::_tournamentVariable
	TribeTournamentVariable_t8978F616B72F4D039DC3E75EBB14E6F4A7D15300 * ____tournamentVariable_8;
	// TournamentModel TribeTournamentSystem::_currentTournament
	TournamentModel_tB7076328E3D85DBCC9B2B22F1BD7B3EEC967DB3D * ____currentTournament_9;
	// System.Boolean TribeTournamentSystem::_started
	bool ____started_10;
	// UnityEngine.Events.UnityEvent TribeTournamentSystem::OnTournamentStart
	UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * ___OnTournamentStart_11;

public:
	inline static int32_t get_offset_of__diContainer_3() { return static_cast<int32_t>(offsetof(TribeTournamentSystem_t7F0DE0115218A7B9AA81C8EF2B923A4450F36424, ____diContainer_3)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__diContainer_3() const { return ____diContainer_3; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__diContainer_3() { return &____diContainer_3; }
	inline void set__diContainer_3(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____diContainer_3 = value;
		Il2CppCodeGenWriteBarrier((&____diContainer_3), value);
	}

	inline static int32_t get_offset_of__tournamentsVO_4() { return static_cast<int32_t>(offsetof(TribeTournamentSystem_t7F0DE0115218A7B9AA81C8EF2B923A4450F36424, ____tournamentsVO_4)); }
	inline TournamentsVO_tCBA9A235AE4069263CE332B1E9150CDABD0E8016 * get__tournamentsVO_4() const { return ____tournamentsVO_4; }
	inline TournamentsVO_tCBA9A235AE4069263CE332B1E9150CDABD0E8016 ** get_address_of__tournamentsVO_4() { return &____tournamentsVO_4; }
	inline void set__tournamentsVO_4(TournamentsVO_tCBA9A235AE4069263CE332B1E9150CDABD0E8016 * value)
	{
		____tournamentsVO_4 = value;
		Il2CppCodeGenWriteBarrier((&____tournamentsVO_4), value);
	}

	inline static int32_t get_offset_of__startTrigger_5() { return static_cast<int32_t>(offsetof(TribeTournamentSystem_t7F0DE0115218A7B9AA81C8EF2B923A4450F36424, ____startTrigger_5)); }
	inline Trigger_tFA4DAF5E2FB996087C1F5DD53203E250D4770C09 * get__startTrigger_5() const { return ____startTrigger_5; }
	inline Trigger_tFA4DAF5E2FB996087C1F5DD53203E250D4770C09 ** get_address_of__startTrigger_5() { return &____startTrigger_5; }
	inline void set__startTrigger_5(Trigger_tFA4DAF5E2FB996087C1F5DD53203E250D4770C09 * value)
	{
		____startTrigger_5 = value;
		Il2CppCodeGenWriteBarrier((&____startTrigger_5), value);
	}

	inline static int32_t get_offset_of__endTrigger_6() { return static_cast<int32_t>(offsetof(TribeTournamentSystem_t7F0DE0115218A7B9AA81C8EF2B923A4450F36424, ____endTrigger_6)); }
	inline Trigger_tFA4DAF5E2FB996087C1F5DD53203E250D4770C09 * get__endTrigger_6() const { return ____endTrigger_6; }
	inline Trigger_tFA4DAF5E2FB996087C1F5DD53203E250D4770C09 ** get_address_of__endTrigger_6() { return &____endTrigger_6; }
	inline void set__endTrigger_6(Trigger_tFA4DAF5E2FB996087C1F5DD53203E250D4770C09 * value)
	{
		____endTrigger_6 = value;
		Il2CppCodeGenWriteBarrier((&____endTrigger_6), value);
	}

	inline static int32_t get_offset_of__starTournamentQuest_7() { return static_cast<int32_t>(offsetof(TribeTournamentSystem_t7F0DE0115218A7B9AA81C8EF2B923A4450F36424, ____starTournamentQuest_7)); }
	inline Quest_tBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021 * get__starTournamentQuest_7() const { return ____starTournamentQuest_7; }
	inline Quest_tBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021 ** get_address_of__starTournamentQuest_7() { return &____starTournamentQuest_7; }
	inline void set__starTournamentQuest_7(Quest_tBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021 * value)
	{
		____starTournamentQuest_7 = value;
		Il2CppCodeGenWriteBarrier((&____starTournamentQuest_7), value);
	}

	inline static int32_t get_offset_of__tournamentVariable_8() { return static_cast<int32_t>(offsetof(TribeTournamentSystem_t7F0DE0115218A7B9AA81C8EF2B923A4450F36424, ____tournamentVariable_8)); }
	inline TribeTournamentVariable_t8978F616B72F4D039DC3E75EBB14E6F4A7D15300 * get__tournamentVariable_8() const { return ____tournamentVariable_8; }
	inline TribeTournamentVariable_t8978F616B72F4D039DC3E75EBB14E6F4A7D15300 ** get_address_of__tournamentVariable_8() { return &____tournamentVariable_8; }
	inline void set__tournamentVariable_8(TribeTournamentVariable_t8978F616B72F4D039DC3E75EBB14E6F4A7D15300 * value)
	{
		____tournamentVariable_8 = value;
		Il2CppCodeGenWriteBarrier((&____tournamentVariable_8), value);
	}

	inline static int32_t get_offset_of__currentTournament_9() { return static_cast<int32_t>(offsetof(TribeTournamentSystem_t7F0DE0115218A7B9AA81C8EF2B923A4450F36424, ____currentTournament_9)); }
	inline TournamentModel_tB7076328E3D85DBCC9B2B22F1BD7B3EEC967DB3D * get__currentTournament_9() const { return ____currentTournament_9; }
	inline TournamentModel_tB7076328E3D85DBCC9B2B22F1BD7B3EEC967DB3D ** get_address_of__currentTournament_9() { return &____currentTournament_9; }
	inline void set__currentTournament_9(TournamentModel_tB7076328E3D85DBCC9B2B22F1BD7B3EEC967DB3D * value)
	{
		____currentTournament_9 = value;
		Il2CppCodeGenWriteBarrier((&____currentTournament_9), value);
	}

	inline static int32_t get_offset_of__started_10() { return static_cast<int32_t>(offsetof(TribeTournamentSystem_t7F0DE0115218A7B9AA81C8EF2B923A4450F36424, ____started_10)); }
	inline bool get__started_10() const { return ____started_10; }
	inline bool* get_address_of__started_10() { return &____started_10; }
	inline void set__started_10(bool value)
	{
		____started_10 = value;
	}

	inline static int32_t get_offset_of_OnTournamentStart_11() { return static_cast<int32_t>(offsetof(TribeTournamentSystem_t7F0DE0115218A7B9AA81C8EF2B923A4450F36424, ___OnTournamentStart_11)); }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * get_OnTournamentStart_11() const { return ___OnTournamentStart_11; }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F ** get_address_of_OnTournamentStart_11() { return &___OnTournamentStart_11; }
	inline void set_OnTournamentStart_11(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * value)
	{
		___OnTournamentStart_11 = value;
		Il2CppCodeGenWriteBarrier((&___OnTournamentStart_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIBETOURNAMENTSYSTEM_T7F0DE0115218A7B9AA81C8EF2B923A4450F36424_H
#ifndef UNITYEVENT_T5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F_H
#define UNITYEVENT_T5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent
struct  UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_T5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef MESSAGETYPE_T07EE7B9773D6279718FDDBAE71930B611365858F_H
#define MESSAGETYPE_T07EE7B9773D6279718FDDBAE71930B611365858F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Game.MessageType
struct  MessageType_t07EE7B9773D6279718FDDBAE71930B611365858F 
{
public:
	// System.Int32 Game.MessageType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MessageType_t07EE7B9773D6279718FDDBAE71930B611365858F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGETYPE_T07EE7B9773D6279718FDDBAE71930B611365858F_H
#ifndef ONQUESTENDEDEVENT_TC0EC99C69DD18EF07F2A9877323E1CE9E7825D6F_H
#define ONQUESTENDEDEVENT_TC0EC99C69DD18EF07F2A9877323E1CE9E7825D6F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OnQuestEndedEvent
struct  OnQuestEndedEvent_tC0EC99C69DD18EF07F2A9877323E1CE9E7825D6F  : public UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONQUESTENDEDEVENT_TC0EC99C69DD18EF07F2A9877323E1CE9E7825D6F_H
#ifndef ONQUESTSTARTEDEVENT_T8E404B059FB2011AF0DC8299EBBF6A2DB9657948_H
#define ONQUESTSTARTEDEVENT_T8E404B059FB2011AF0DC8299EBBF6A2DB9657948_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OnQuestStartedEvent
struct  OnQuestStartedEvent_t8E404B059FB2011AF0DC8299EBBF6A2DB9657948  : public UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONQUESTSTARTEDEVENT_T8E404B059FB2011AF0DC8299EBBF6A2DB9657948_H
#ifndef ONQUESTUPDATEDEVENT_T8B85F5C13C8C6D94E08B49B1C3AFA8A6118F8CAB_H
#define ONQUESTUPDATEDEVENT_T8B85F5C13C8C6D94E08B49B1C3AFA8A6118F8CAB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OnQuestUpdatedEvent
struct  OnQuestUpdatedEvent_t8B85F5C13C8C6D94E08B49B1C3AFA8A6118F8CAB  : public UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONQUESTUPDATEDEVENT_T8B85F5C13C8C6D94E08B49B1C3AFA8A6118F8CAB_H
#ifndef REPORTTOOLTIPDATA_TF8EDAC5D364823AA3EA0B424A1C09029CF485A93_H
#define REPORTTOOLTIPDATA_TF8EDAC5D364823AA3EA0B424A1C09029CF485A93_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReportTooltipData
struct  ReportTooltipData_tF8EDAC5D364823AA3EA0B424A1C09029CF485A93  : public RuntimeObject
{
public:
	// System.String ReportTooltipData::MessageId
	String_t* ___MessageId_0;
	// UnityEngine.Vector3 ReportTooltipData::Position
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___Position_1;

public:
	inline static int32_t get_offset_of_MessageId_0() { return static_cast<int32_t>(offsetof(ReportTooltipData_tF8EDAC5D364823AA3EA0B424A1C09029CF485A93, ___MessageId_0)); }
	inline String_t* get_MessageId_0() const { return ___MessageId_0; }
	inline String_t** get_address_of_MessageId_0() { return &___MessageId_0; }
	inline void set_MessageId_0(String_t* value)
	{
		___MessageId_0 = value;
		Il2CppCodeGenWriteBarrier((&___MessageId_0), value);
	}

	inline static int32_t get_offset_of_Position_1() { return static_cast<int32_t>(offsetof(ReportTooltipData_tF8EDAC5D364823AA3EA0B424A1C09029CF485A93, ___Position_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_Position_1() const { return ___Position_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_Position_1() { return &___Position_1; }
	inline void set_Position_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___Position_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REPORTTOOLTIPDATA_TF8EDAC5D364823AA3EA0B424A1C09029CF485A93_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef ASYNCTASKMETHODBUILDER_1_T66ED1808B26B8081A2804D6A750D13386E360BD9_H
#define ASYNCTASKMETHODBUILDER_1_T66ED1808B26B8081A2804D6A750D13386E360BD9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Threading.Tasks.VoidTaskResult>
struct  AsyncTaskMethodBuilder_1_t66ED1808B26B8081A2804D6A750D13386E360BD9 
{
public:
	// System.Runtime.CompilerServices.AsyncMethodBuilderCore System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::m_coreState
	AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01  ___m_coreState_1;
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::m_task
	Task_1_t1359D75350E9D976BFA28AD96E417450DE277673 * ___m_task_2;

public:
	inline static int32_t get_offset_of_m_coreState_1() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_t66ED1808B26B8081A2804D6A750D13386E360BD9, ___m_coreState_1)); }
	inline AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01  get_m_coreState_1() const { return ___m_coreState_1; }
	inline AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01 * get_address_of_m_coreState_1() { return &___m_coreState_1; }
	inline void set_m_coreState_1(AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01  value)
	{
		___m_coreState_1 = value;
	}

	inline static int32_t get_offset_of_m_task_2() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_t66ED1808B26B8081A2804D6A750D13386E360BD9, ___m_task_2)); }
	inline Task_1_t1359D75350E9D976BFA28AD96E417450DE277673 * get_m_task_2() const { return ___m_task_2; }
	inline Task_1_t1359D75350E9D976BFA28AD96E417450DE277673 ** get_address_of_m_task_2() { return &___m_task_2; }
	inline void set_m_task_2(Task_1_t1359D75350E9D976BFA28AD96E417450DE277673 * value)
	{
		___m_task_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_task_2), value);
	}
};

struct AsyncTaskMethodBuilder_1_t66ED1808B26B8081A2804D6A750D13386E360BD9_StaticFields
{
public:
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::s_defaultResultTask
	Task_1_t1359D75350E9D976BFA28AD96E417450DE277673 * ___s_defaultResultTask_0;

public:
	inline static int32_t get_offset_of_s_defaultResultTask_0() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_t66ED1808B26B8081A2804D6A750D13386E360BD9_StaticFields, ___s_defaultResultTask_0)); }
	inline Task_1_t1359D75350E9D976BFA28AD96E417450DE277673 * get_s_defaultResultTask_0() const { return ___s_defaultResultTask_0; }
	inline Task_1_t1359D75350E9D976BFA28AD96E417450DE277673 ** get_address_of_s_defaultResultTask_0() { return &___s_defaultResultTask_0; }
	inline void set_s_defaultResultTask_0(Task_1_t1359D75350E9D976BFA28AD96E417450DE277673 * value)
	{
		___s_defaultResultTask_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_defaultResultTask_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCTASKMETHODBUILDER_1_T66ED1808B26B8081A2804D6A750D13386E360BD9_H
#ifndef TIMESPAN_TA8069278ACE8A74D6DF7D514A9CD4432433F64C4_H
#define TIMESPAN_TA8069278ACE8A74D6DF7D514A9CD4432433F64C4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_22;

public:
	inline static int32_t get_offset_of__ticks_22() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4, ____ticks_22)); }
	inline int64_t get__ticks_22() const { return ____ticks_22; }
	inline int64_t* get_address_of__ticks_22() { return &____ticks_22; }
	inline void set__ticks_22(int64_t value)
	{
		____ticks_22 = value;
	}
};

struct TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___Zero_19;
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___MaxValue_20;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___MinValue_21;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.TimeSpan::_legacyConfigChecked
	bool ____legacyConfigChecked_23;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.TimeSpan::_legacyMode
	bool ____legacyMode_24;

public:
	inline static int32_t get_offset_of_Zero_19() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ___Zero_19)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_Zero_19() const { return ___Zero_19; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_Zero_19() { return &___Zero_19; }
	inline void set_Zero_19(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___Zero_19 = value;
	}

	inline static int32_t get_offset_of_MaxValue_20() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ___MaxValue_20)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_MaxValue_20() const { return ___MaxValue_20; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_MaxValue_20() { return &___MaxValue_20; }
	inline void set_MaxValue_20(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___MaxValue_20 = value;
	}

	inline static int32_t get_offset_of_MinValue_21() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ___MinValue_21)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_MinValue_21() const { return ___MinValue_21; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_MinValue_21() { return &___MinValue_21; }
	inline void set_MinValue_21(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___MinValue_21 = value;
	}

	inline static int32_t get_offset_of__legacyConfigChecked_23() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ____legacyConfigChecked_23)); }
	inline bool get__legacyConfigChecked_23() const { return ____legacyConfigChecked_23; }
	inline bool* get_address_of__legacyConfigChecked_23() { return &____legacyConfigChecked_23; }
	inline void set__legacyConfigChecked_23(bool value)
	{
		____legacyConfigChecked_23 = value;
	}

	inline static int32_t get_offset_of__legacyMode_24() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ____legacyMode_24)); }
	inline bool get__legacyMode_24() const { return ____legacyMode_24; }
	inline bool* get_address_of__legacyMode_24() { return &____legacyMode_24; }
	inline void set__legacyMode_24(bool value)
	{
		____legacyMode_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_TA8069278ACE8A74D6DF7D514A9CD4432433F64C4_H
#ifndef TEAM_TYPE_T3C178DBCDEB1B9A42045B6C497BEC33A8557E170_H
#define TEAM_TYPE_T3C178DBCDEB1B9A42045B6C497BEC33A8557E170_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TEAM_TYPE
struct  TEAM_TYPE_t3C178DBCDEB1B9A42045B6C497BEC33A8557E170 
{
public:
	// System.Int32 TEAM_TYPE::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TEAM_TYPE_t3C178DBCDEB1B9A42045B6C497BEC33A8557E170, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEAM_TYPE_T3C178DBCDEB1B9A42045B6C497BEC33A8557E170_H
#ifndef REFREAHTYPE_TD4E50A740AAB4EA48BFF2B426369C53D543441DE_H
#define REFREAHTYPE_TD4E50A740AAB4EA48BFF2B426369C53D543441DE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.RefreahType
struct  RefreahType_tD4E50A740AAB4EA48BFF2B426369C53D543441DE 
{
public:
	// System.Int32 Tayr.RefreahType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RefreahType_tD4E50A740AAB4EA48BFF2B426369C53D543441DE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFREAHTYPE_TD4E50A740AAB4EA48BFF2B426369C53D543441DE_H
#ifndef TOURNAMENTSTATUS_T7079FE4722A4990FBFA69A120E5A7E6A1148FFEF_H
#define TOURNAMENTSTATUS_T7079FE4722A4990FBFA69A120E5A7E6A1148FFEF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TournamentStatus
struct  TournamentStatus_t7079FE4722A4990FBFA69A120E5A7E6A1148FFEF 
{
public:
	// System.Int32 TournamentStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TournamentStatus_t7079FE4722A4990FBFA69A120E5A7E6A1148FFEF, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOURNAMENTSTATUS_T7079FE4722A4990FBFA69A120E5A7E6A1148FFEF_H
#ifndef TOURNAMENTTYPE_TDDF357A7FD7E7B9E1A0C976C193A82A0CF9C6EF2_H
#define TOURNAMENTTYPE_TDDF357A7FD7E7B9E1A0C976C193A82A0CF9C6EF2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TournamentType
struct  TournamentType_tDDF357A7FD7E7B9E1A0C976C193A82A0CF9C6EF2 
{
public:
	// System.Int32 TournamentType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TournamentType_tDDF357A7FD7E7B9E1A0C976C193A82A0CF9C6EF2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOURNAMENTTYPE_TDDF357A7FD7E7B9E1A0C976C193A82A0CF9C6EF2_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef ASYNCTASKMETHODBUILDER_T0CD1893D670405BED201BE8CA6F2E811F2C0F487_H
#define ASYNCTASKMETHODBUILDER_T0CD1893D670405BED201BE8CA6F2E811F2C0F487_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.AsyncTaskMethodBuilder
struct  AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487 
{
public:
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Threading.Tasks.VoidTaskResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder::m_builder
	AsyncTaskMethodBuilder_1_t66ED1808B26B8081A2804D6A750D13386E360BD9  ___m_builder_1;

public:
	inline static int32_t get_offset_of_m_builder_1() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487, ___m_builder_1)); }
	inline AsyncTaskMethodBuilder_1_t66ED1808B26B8081A2804D6A750D13386E360BD9  get_m_builder_1() const { return ___m_builder_1; }
	inline AsyncTaskMethodBuilder_1_t66ED1808B26B8081A2804D6A750D13386E360BD9 * get_address_of_m_builder_1() { return &___m_builder_1; }
	inline void set_m_builder_1(AsyncTaskMethodBuilder_1_t66ED1808B26B8081A2804D6A750D13386E360BD9  value)
	{
		___m_builder_1 = value;
	}
};

struct AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487_StaticFields
{
public:
	// System.Threading.Tasks.Task`1<System.Threading.Tasks.VoidTaskResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder::s_cachedCompleted
	Task_1_t1359D75350E9D976BFA28AD96E417450DE277673 * ___s_cachedCompleted_0;

public:
	inline static int32_t get_offset_of_s_cachedCompleted_0() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487_StaticFields, ___s_cachedCompleted_0)); }
	inline Task_1_t1359D75350E9D976BFA28AD96E417450DE277673 * get_s_cachedCompleted_0() const { return ___s_cachedCompleted_0; }
	inline Task_1_t1359D75350E9D976BFA28AD96E417450DE277673 ** get_address_of_s_cachedCompleted_0() { return &___s_cachedCompleted_0; }
	inline void set_s_cachedCompleted_0(Task_1_t1359D75350E9D976BFA28AD96E417450DE277673 * value)
	{
		___s_cachedCompleted_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_cachedCompleted_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.CompilerServices.AsyncTaskMethodBuilder
struct AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487_marshaled_pinvoke
{
	AsyncTaskMethodBuilder_1_t66ED1808B26B8081A2804D6A750D13386E360BD9  ___m_builder_1;
};
// Native definition for COM marshalling of System.Runtime.CompilerServices.AsyncTaskMethodBuilder
struct AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487_marshaled_com
{
	AsyncTaskMethodBuilder_1_t66ED1808B26B8081A2804D6A750D13386E360BD9  ___m_builder_1;
};
#endif // ASYNCTASKMETHODBUILDER_T0CD1893D670405BED201BE8CA6F2E811F2C0F487_H
#ifndef VARIABLE_T5D003723AEAA82A1E36B619BD98E937C05793BFD_H
#define VARIABLE_T5D003723AEAA82A1E36B619BD98E937C05793BFD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.Variable
struct  Variable_t5D003723AEAA82A1E36B619BD98E937C05793BFD  : public RuntimeObject
{
public:
	// Tayr.VariableChangedEvent Tayr.Variable::_onVariableChanged
	VariableChangedEvent_t553579619C432FFE7C7636199CE57457FA699BA4 * ____onVariableChanged_0;
	// System.Int32 Tayr.Variable::_value
	int32_t ____value_1;
	// Tayr.RefreahType Tayr.Variable::_refreshType
	int32_t ____refreshType_2;
	// System.Boolean Tayr.Variable::_isInitialized
	bool ____isInitialized_3;

public:
	inline static int32_t get_offset_of__onVariableChanged_0() { return static_cast<int32_t>(offsetof(Variable_t5D003723AEAA82A1E36B619BD98E937C05793BFD, ____onVariableChanged_0)); }
	inline VariableChangedEvent_t553579619C432FFE7C7636199CE57457FA699BA4 * get__onVariableChanged_0() const { return ____onVariableChanged_0; }
	inline VariableChangedEvent_t553579619C432FFE7C7636199CE57457FA699BA4 ** get_address_of__onVariableChanged_0() { return &____onVariableChanged_0; }
	inline void set__onVariableChanged_0(VariableChangedEvent_t553579619C432FFE7C7636199CE57457FA699BA4 * value)
	{
		____onVariableChanged_0 = value;
		Il2CppCodeGenWriteBarrier((&____onVariableChanged_0), value);
	}

	inline static int32_t get_offset_of__value_1() { return static_cast<int32_t>(offsetof(Variable_t5D003723AEAA82A1E36B619BD98E937C05793BFD, ____value_1)); }
	inline int32_t get__value_1() const { return ____value_1; }
	inline int32_t* get_address_of__value_1() { return &____value_1; }
	inline void set__value_1(int32_t value)
	{
		____value_1 = value;
	}

	inline static int32_t get_offset_of__refreshType_2() { return static_cast<int32_t>(offsetof(Variable_t5D003723AEAA82A1E36B619BD98E937C05793BFD, ____refreshType_2)); }
	inline int32_t get__refreshType_2() const { return ____refreshType_2; }
	inline int32_t* get_address_of__refreshType_2() { return &____refreshType_2; }
	inline void set__refreshType_2(int32_t value)
	{
		____refreshType_2 = value;
	}

	inline static int32_t get_offset_of__isInitialized_3() { return static_cast<int32_t>(offsetof(Variable_t5D003723AEAA82A1E36B619BD98E937C05793BFD, ____isInitialized_3)); }
	inline bool get__isInitialized_3() const { return ____isInitialized_3; }
	inline bool* get_address_of__isInitialized_3() { return &____isInitialized_3; }
	inline void set__isInitialized_3(bool value)
	{
		____isInitialized_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VARIABLE_T5D003723AEAA82A1E36B619BD98E937C05793BFD_H
#ifndef TIMESYSTEM_T3EA79F95B78D7FF6BC7D3A29AADB4E9CFB0F2CED_H
#define TIMESYSTEM_T3EA79F95B78D7FF6BC7D3A29AADB4E9CFB0F2CED_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TimeSystem
struct  TimeSystem_t3EA79F95B78D7FF6BC7D3A29AADB4E9CFB0F2CED  : public BasicTSystem_tD6508F31E78D89E2227A8E4AD3D70DFDEBCB07F8
{
public:
	// Tayr.GameSparksPlatform TimeSystem::_gameSparks
	GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * ____gameSparks_3;
	// UserVO TimeSystem::_userVO
	UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * ____userVO_4;
	// System.DateTime TimeSystem::_lastServerTime
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ____lastServerTime_5;
	// System.DateTime TimeSystem::_lastClientTime
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ____lastClientTime_6;
	// System.TimeSpan TimeSystem::_clientServerOffset
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ____clientServerOffset_7;
	// System.Boolean TimeSystem::_timeFound
	bool ____timeFound_8;
	// System.Int64 TimeSystem::<ServerTime>k__BackingField
	int64_t ___U3CServerTimeU3Ek__BackingField_9;

public:
	inline static int32_t get_offset_of__gameSparks_3() { return static_cast<int32_t>(offsetof(TimeSystem_t3EA79F95B78D7FF6BC7D3A29AADB4E9CFB0F2CED, ____gameSparks_3)); }
	inline GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * get__gameSparks_3() const { return ____gameSparks_3; }
	inline GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 ** get_address_of__gameSparks_3() { return &____gameSparks_3; }
	inline void set__gameSparks_3(GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * value)
	{
		____gameSparks_3 = value;
		Il2CppCodeGenWriteBarrier((&____gameSparks_3), value);
	}

	inline static int32_t get_offset_of__userVO_4() { return static_cast<int32_t>(offsetof(TimeSystem_t3EA79F95B78D7FF6BC7D3A29AADB4E9CFB0F2CED, ____userVO_4)); }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * get__userVO_4() const { return ____userVO_4; }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 ** get_address_of__userVO_4() { return &____userVO_4; }
	inline void set__userVO_4(UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * value)
	{
		____userVO_4 = value;
		Il2CppCodeGenWriteBarrier((&____userVO_4), value);
	}

	inline static int32_t get_offset_of__lastServerTime_5() { return static_cast<int32_t>(offsetof(TimeSystem_t3EA79F95B78D7FF6BC7D3A29AADB4E9CFB0F2CED, ____lastServerTime_5)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get__lastServerTime_5() const { return ____lastServerTime_5; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of__lastServerTime_5() { return &____lastServerTime_5; }
	inline void set__lastServerTime_5(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		____lastServerTime_5 = value;
	}

	inline static int32_t get_offset_of__lastClientTime_6() { return static_cast<int32_t>(offsetof(TimeSystem_t3EA79F95B78D7FF6BC7D3A29AADB4E9CFB0F2CED, ____lastClientTime_6)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get__lastClientTime_6() const { return ____lastClientTime_6; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of__lastClientTime_6() { return &____lastClientTime_6; }
	inline void set__lastClientTime_6(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		____lastClientTime_6 = value;
	}

	inline static int32_t get_offset_of__clientServerOffset_7() { return static_cast<int32_t>(offsetof(TimeSystem_t3EA79F95B78D7FF6BC7D3A29AADB4E9CFB0F2CED, ____clientServerOffset_7)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get__clientServerOffset_7() const { return ____clientServerOffset_7; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of__clientServerOffset_7() { return &____clientServerOffset_7; }
	inline void set__clientServerOffset_7(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		____clientServerOffset_7 = value;
	}

	inline static int32_t get_offset_of__timeFound_8() { return static_cast<int32_t>(offsetof(TimeSystem_t3EA79F95B78D7FF6BC7D3A29AADB4E9CFB0F2CED, ____timeFound_8)); }
	inline bool get__timeFound_8() const { return ____timeFound_8; }
	inline bool* get_address_of__timeFound_8() { return &____timeFound_8; }
	inline void set__timeFound_8(bool value)
	{
		____timeFound_8 = value;
	}

	inline static int32_t get_offset_of_U3CServerTimeU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(TimeSystem_t3EA79F95B78D7FF6BC7D3A29AADB4E9CFB0F2CED, ___U3CServerTimeU3Ek__BackingField_9)); }
	inline int64_t get_U3CServerTimeU3Ek__BackingField_9() const { return ___U3CServerTimeU3Ek__BackingField_9; }
	inline int64_t* get_address_of_U3CServerTimeU3Ek__BackingField_9() { return &___U3CServerTimeU3Ek__BackingField_9; }
	inline void set_U3CServerTimeU3Ek__BackingField_9(int64_t value)
	{
		___U3CServerTimeU3Ek__BackingField_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESYSTEM_T3EA79F95B78D7FF6BC7D3A29AADB4E9CFB0F2CED_H
#ifndef TOURNAMENTMODEL_TB7076328E3D85DBCC9B2B22F1BD7B3EEC967DB3D_H
#define TOURNAMENTMODEL_TB7076328E3D85DBCC9B2B22F1BD7B3EEC967DB3D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TournamentModel
struct  TournamentModel_tB7076328E3D85DBCC9B2B22F1BD7B3EEC967DB3D  : public RuntimeObject
{
public:
	// System.String TournamentModel::id
	String_t* ___id_0;
	// TournamentType TournamentModel::type
	int32_t ___type_1;
	// System.Int64 TournamentModel::startTime
	int64_t ___startTime_2;
	// System.Int64 TournamentModel::endTime
	int64_t ___endTime_3;
	// TournamentStatus TournamentModel::status
	int32_t ___status_4;
	// System.Int32 TournamentModel::rank
	int32_t ___rank_5;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(TournamentModel_tB7076328E3D85DBCC9B2B22F1BD7B3EEC967DB3D, ___id_0)); }
	inline String_t* get_id_0() const { return ___id_0; }
	inline String_t** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(String_t* value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier((&___id_0), value);
	}

	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(TournamentModel_tB7076328E3D85DBCC9B2B22F1BD7B3EEC967DB3D, ___type_1)); }
	inline int32_t get_type_1() const { return ___type_1; }
	inline int32_t* get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(int32_t value)
	{
		___type_1 = value;
	}

	inline static int32_t get_offset_of_startTime_2() { return static_cast<int32_t>(offsetof(TournamentModel_tB7076328E3D85DBCC9B2B22F1BD7B3EEC967DB3D, ___startTime_2)); }
	inline int64_t get_startTime_2() const { return ___startTime_2; }
	inline int64_t* get_address_of_startTime_2() { return &___startTime_2; }
	inline void set_startTime_2(int64_t value)
	{
		___startTime_2 = value;
	}

	inline static int32_t get_offset_of_endTime_3() { return static_cast<int32_t>(offsetof(TournamentModel_tB7076328E3D85DBCC9B2B22F1BD7B3EEC967DB3D, ___endTime_3)); }
	inline int64_t get_endTime_3() const { return ___endTime_3; }
	inline int64_t* get_address_of_endTime_3() { return &___endTime_3; }
	inline void set_endTime_3(int64_t value)
	{
		___endTime_3 = value;
	}

	inline static int32_t get_offset_of_status_4() { return static_cast<int32_t>(offsetof(TournamentModel_tB7076328E3D85DBCC9B2B22F1BD7B3EEC967DB3D, ___status_4)); }
	inline int32_t get_status_4() const { return ___status_4; }
	inline int32_t* get_address_of_status_4() { return &___status_4; }
	inline void set_status_4(int32_t value)
	{
		___status_4 = value;
	}

	inline static int32_t get_offset_of_rank_5() { return static_cast<int32_t>(offsetof(TournamentModel_tB7076328E3D85DBCC9B2B22F1BD7B3EEC967DB3D, ___rank_5)); }
	inline int32_t get_rank_5() const { return ___rank_5; }
	inline int32_t* get_address_of_rank_5() { return &___rank_5; }
	inline void set_rank_5(int32_t value)
	{
		___rank_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOURNAMENTMODEL_TB7076328E3D85DBCC9B2B22F1BD7B3EEC967DB3D_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef U3CSHOWU3ED__0_TAA8320161EEB766929406A1BFEBCFE688EA702B4_H
#define U3CSHOWU3ED__0_TAA8320161EEB766929406A1BFEBCFE688EA702B4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BasicFlow_<Show>d__0
struct  U3CShowU3Ed__0_tAA8320161EEB766929406A1BFEBCFE688EA702B4 
{
public:
	// System.Int32 BasicFlow_<Show>d__0::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder BasicFlow_<Show>d__0::<>t__builder
	AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  ___U3CU3Et__builder_1;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CShowU3Ed__0_tAA8320161EEB766929406A1BFEBCFE688EA702B4, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3CShowU3Ed__0_tAA8320161EEB766929406A1BFEBCFE688EA702B4, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487 * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  value)
	{
		___U3CU3Et__builder_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSHOWU3ED__0_TAA8320161EEB766929406A1BFEBCFE688EA702B4_H
#ifndef U3CSHOWU3ED__0_T0D1F0512B4F7EE67BE1F282E746AB38E196BE4D2_H
#define U3CSHOWU3ED__0_T0D1F0512B4F7EE67BE1F282E746AB38E196BE4D2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BasicTutorialFlow_<Show>d__0
struct  U3CShowU3Ed__0_t0D1F0512B4F7EE67BE1F282E746AB38E196BE4D2 
{
public:
	// System.Int32 BasicTutorialFlow_<Show>d__0::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder BasicTutorialFlow_<Show>d__0::<>t__builder
	AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  ___U3CU3Et__builder_1;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CShowU3Ed__0_t0D1F0512B4F7EE67BE1F282E746AB38E196BE4D2, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3CShowU3Ed__0_t0D1F0512B4F7EE67BE1F282E746AB38E196BE4D2, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487 * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  value)
	{
		___U3CU3Et__builder_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSHOWU3ED__0_T0D1F0512B4F7EE67BE1F282E746AB38E196BE4D2_H
#ifndef U3CONDESTROYU3ED__4_T01BC33A675417028768FAC2621EDEA9F416AB467_H
#define U3CONDESTROYU3ED__4_T01BC33A675417028768FAC2621EDEA9F416AB467_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BattleIntroFlow_<OnDestroy>d__4
struct  U3COnDestroyU3Ed__4_t01BC33A675417028768FAC2621EDEA9F416AB467 
{
public:
	// System.Int32 BattleIntroFlow_<OnDestroy>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder BattleIntroFlow_<OnDestroy>d__4::<>t__builder
	AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  ___U3CU3Et__builder_1;
	// BattleIntroFlow BattleIntroFlow_<OnDestroy>d__4::<>4__this
	BattleIntroFlow_t6B627D5733BE5B127F2EBBABF6747828F5F35D36 * ___U3CU3E4__this_2;
	// Entitas.IGroup`1<GameEntity> BattleIntroFlow_<OnDestroy>d__4::<entities>5__2
	RuntimeObject* ___U3CentitiesU3E5__2_3;
	// System.Int32 BattleIntroFlow_<OnDestroy>d__4::<initObjectives>5__3
	int32_t ___U3CinitObjectivesU3E5__3_4;
	// System.Int32 BattleIntroFlow_<OnDestroy>d__4::<currentObjectives>5__4
	int32_t ___U3CcurrentObjectivesU3E5__4_5;
	// System.Runtime.CompilerServices.TaskAwaiter BattleIntroFlow_<OnDestroy>d__4::<>u__1
	TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  ___U3CU3Eu__1_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3COnDestroyU3Ed__4_t01BC33A675417028768FAC2621EDEA9F416AB467, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3COnDestroyU3Ed__4_t01BC33A675417028768FAC2621EDEA9F416AB467, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487 * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  value)
	{
		___U3CU3Et__builder_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3COnDestroyU3Ed__4_t01BC33A675417028768FAC2621EDEA9F416AB467, ___U3CU3E4__this_2)); }
	inline BattleIntroFlow_t6B627D5733BE5B127F2EBBABF6747828F5F35D36 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline BattleIntroFlow_t6B627D5733BE5B127F2EBBABF6747828F5F35D36 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(BattleIntroFlow_t6B627D5733BE5B127F2EBBABF6747828F5F35D36 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CentitiesU3E5__2_3() { return static_cast<int32_t>(offsetof(U3COnDestroyU3Ed__4_t01BC33A675417028768FAC2621EDEA9F416AB467, ___U3CentitiesU3E5__2_3)); }
	inline RuntimeObject* get_U3CentitiesU3E5__2_3() const { return ___U3CentitiesU3E5__2_3; }
	inline RuntimeObject** get_address_of_U3CentitiesU3E5__2_3() { return &___U3CentitiesU3E5__2_3; }
	inline void set_U3CentitiesU3E5__2_3(RuntimeObject* value)
	{
		___U3CentitiesU3E5__2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CentitiesU3E5__2_3), value);
	}

	inline static int32_t get_offset_of_U3CinitObjectivesU3E5__3_4() { return static_cast<int32_t>(offsetof(U3COnDestroyU3Ed__4_t01BC33A675417028768FAC2621EDEA9F416AB467, ___U3CinitObjectivesU3E5__3_4)); }
	inline int32_t get_U3CinitObjectivesU3E5__3_4() const { return ___U3CinitObjectivesU3E5__3_4; }
	inline int32_t* get_address_of_U3CinitObjectivesU3E5__3_4() { return &___U3CinitObjectivesU3E5__3_4; }
	inline void set_U3CinitObjectivesU3E5__3_4(int32_t value)
	{
		___U3CinitObjectivesU3E5__3_4 = value;
	}

	inline static int32_t get_offset_of_U3CcurrentObjectivesU3E5__4_5() { return static_cast<int32_t>(offsetof(U3COnDestroyU3Ed__4_t01BC33A675417028768FAC2621EDEA9F416AB467, ___U3CcurrentObjectivesU3E5__4_5)); }
	inline int32_t get_U3CcurrentObjectivesU3E5__4_5() const { return ___U3CcurrentObjectivesU3E5__4_5; }
	inline int32_t* get_address_of_U3CcurrentObjectivesU3E5__4_5() { return &___U3CcurrentObjectivesU3E5__4_5; }
	inline void set_U3CcurrentObjectivesU3E5__4_5(int32_t value)
	{
		___U3CcurrentObjectivesU3E5__4_5 = value;
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_6() { return static_cast<int32_t>(offsetof(U3COnDestroyU3Ed__4_t01BC33A675417028768FAC2621EDEA9F416AB467, ___U3CU3Eu__1_6)); }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  get_U3CU3Eu__1_6() const { return ___U3CU3Eu__1_6; }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F * get_address_of_U3CU3Eu__1_6() { return &___U3CU3Eu__1_6; }
	inline void set_U3CU3Eu__1_6(TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  value)
	{
		___U3CU3Eu__1_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONDESTROYU3ED__4_T01BC33A675417028768FAC2621EDEA9F416AB467_H
#ifndef U3CONHEROMOVESU3ED__3_T4DAB8171E36E366B35731ED6BDF22878B8E407B5_H
#define U3CONHEROMOVESU3ED__3_T4DAB8171E36E366B35731ED6BDF22878B8E407B5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BattleIntroFlow_<OnHeroMoves>d__3
struct  U3COnHeroMovesU3Ed__3_t4DAB8171E36E366B35731ED6BDF22878B8E407B5 
{
public:
	// System.Int32 BattleIntroFlow_<OnHeroMoves>d__3::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder BattleIntroFlow_<OnHeroMoves>d__3::<>t__builder
	AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  ___U3CU3Et__builder_1;
	// UnityEngine.Vector3 BattleIntroFlow_<OnHeroMoves>d__3::initPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___initPosition_2;
	// GameEntity BattleIntroFlow_<OnHeroMoves>d__3::hero
	GameEntity_t995A5498A539905D7F1F8284D59BD48F077D78FF * ___hero_3;
	// System.Int32 BattleIntroFlow_<OnHeroMoves>d__3::distance
	int32_t ___distance_4;
	// System.Runtime.CompilerServices.TaskAwaiter BattleIntroFlow_<OnHeroMoves>d__3::<>u__1
	TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  ___U3CU3Eu__1_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3COnHeroMovesU3Ed__3_t4DAB8171E36E366B35731ED6BDF22878B8E407B5, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3COnHeroMovesU3Ed__3_t4DAB8171E36E366B35731ED6BDF22878B8E407B5, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487 * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  value)
	{
		___U3CU3Et__builder_1 = value;
	}

	inline static int32_t get_offset_of_initPosition_2() { return static_cast<int32_t>(offsetof(U3COnHeroMovesU3Ed__3_t4DAB8171E36E366B35731ED6BDF22878B8E407B5, ___initPosition_2)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_initPosition_2() const { return ___initPosition_2; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_initPosition_2() { return &___initPosition_2; }
	inline void set_initPosition_2(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___initPosition_2 = value;
	}

	inline static int32_t get_offset_of_hero_3() { return static_cast<int32_t>(offsetof(U3COnHeroMovesU3Ed__3_t4DAB8171E36E366B35731ED6BDF22878B8E407B5, ___hero_3)); }
	inline GameEntity_t995A5498A539905D7F1F8284D59BD48F077D78FF * get_hero_3() const { return ___hero_3; }
	inline GameEntity_t995A5498A539905D7F1F8284D59BD48F077D78FF ** get_address_of_hero_3() { return &___hero_3; }
	inline void set_hero_3(GameEntity_t995A5498A539905D7F1F8284D59BD48F077D78FF * value)
	{
		___hero_3 = value;
		Il2CppCodeGenWriteBarrier((&___hero_3), value);
	}

	inline static int32_t get_offset_of_distance_4() { return static_cast<int32_t>(offsetof(U3COnHeroMovesU3Ed__3_t4DAB8171E36E366B35731ED6BDF22878B8E407B5, ___distance_4)); }
	inline int32_t get_distance_4() const { return ___distance_4; }
	inline int32_t* get_address_of_distance_4() { return &___distance_4; }
	inline void set_distance_4(int32_t value)
	{
		___distance_4 = value;
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_5() { return static_cast<int32_t>(offsetof(U3COnHeroMovesU3Ed__3_t4DAB8171E36E366B35731ED6BDF22878B8E407B5, ___U3CU3Eu__1_5)); }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  get_U3CU3Eu__1_5() const { return ___U3CU3Eu__1_5; }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F * get_address_of_U3CU3Eu__1_5() { return &___U3CU3Eu__1_5; }
	inline void set_U3CU3Eu__1_5(TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  value)
	{
		___U3CU3Eu__1_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONHEROMOVESU3ED__3_T4DAB8171E36E366B35731ED6BDF22878B8E407B5_H
#ifndef U3CSHOWU3ED__2_TE012D437EE48C917521209D67E5D70B984CF68E6_H
#define U3CSHOWU3ED__2_TE012D437EE48C917521209D67E5D70B984CF68E6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BattleIntroFlow_<Show>d__2
struct  U3CShowU3Ed__2_tE012D437EE48C917521209D67E5D70B984CF68E6 
{
public:
	// System.Int32 BattleIntroFlow_<Show>d__2::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder BattleIntroFlow_<Show>d__2::<>t__builder
	AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  ___U3CU3Et__builder_1;
	// BattleIntroFlow BattleIntroFlow_<Show>d__2::<>4__this
	BattleIntroFlow_t6B627D5733BE5B127F2EBBABF6747828F5F35D36 * ___U3CU3E4__this_2;
	// CustomTutorialModel BattleIntroFlow_<Show>d__2::model
	CustomTutorialModel_t7F1E6EF5B6AF58AEA9FB2573811B4562E587E635 * ___model_3;
	// MovementTutorialNode BattleIntroFlow_<Show>d__2::<movementNode>5__2
	MovementTutorialNode_t612D67CD2F5CEA136C089C042E8C326FECB3018A * ___U3CmovementNodeU3E5__2_4;
	// DestroyGoalTutorialNode BattleIntroFlow_<Show>d__2::<destroyNode>5__3
	DestroyGoalTutorialNode_tA62D39E749050BBCB967A3B7FA6CE41997DDC58D * ___U3CdestroyNodeU3E5__3_5;
	// GoalsTutorialNode BattleIntroFlow_<Show>d__2::<goalsNode>5__4
	GoalsTutorialNode_tF301448520894286687F236F80234B8A56CBE7B1 * ___U3CgoalsNodeU3E5__4_6;
	// System.Runtime.CompilerServices.TaskAwaiter BattleIntroFlow_<Show>d__2::<>u__1
	TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  ___U3CU3Eu__1_7;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CShowU3Ed__2_tE012D437EE48C917521209D67E5D70B984CF68E6, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3CShowU3Ed__2_tE012D437EE48C917521209D67E5D70B984CF68E6, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487 * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  value)
	{
		___U3CU3Et__builder_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CShowU3Ed__2_tE012D437EE48C917521209D67E5D70B984CF68E6, ___U3CU3E4__this_2)); }
	inline BattleIntroFlow_t6B627D5733BE5B127F2EBBABF6747828F5F35D36 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline BattleIntroFlow_t6B627D5733BE5B127F2EBBABF6747828F5F35D36 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(BattleIntroFlow_t6B627D5733BE5B127F2EBBABF6747828F5F35D36 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_model_3() { return static_cast<int32_t>(offsetof(U3CShowU3Ed__2_tE012D437EE48C917521209D67E5D70B984CF68E6, ___model_3)); }
	inline CustomTutorialModel_t7F1E6EF5B6AF58AEA9FB2573811B4562E587E635 * get_model_3() const { return ___model_3; }
	inline CustomTutorialModel_t7F1E6EF5B6AF58AEA9FB2573811B4562E587E635 ** get_address_of_model_3() { return &___model_3; }
	inline void set_model_3(CustomTutorialModel_t7F1E6EF5B6AF58AEA9FB2573811B4562E587E635 * value)
	{
		___model_3 = value;
		Il2CppCodeGenWriteBarrier((&___model_3), value);
	}

	inline static int32_t get_offset_of_U3CmovementNodeU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CShowU3Ed__2_tE012D437EE48C917521209D67E5D70B984CF68E6, ___U3CmovementNodeU3E5__2_4)); }
	inline MovementTutorialNode_t612D67CD2F5CEA136C089C042E8C326FECB3018A * get_U3CmovementNodeU3E5__2_4() const { return ___U3CmovementNodeU3E5__2_4; }
	inline MovementTutorialNode_t612D67CD2F5CEA136C089C042E8C326FECB3018A ** get_address_of_U3CmovementNodeU3E5__2_4() { return &___U3CmovementNodeU3E5__2_4; }
	inline void set_U3CmovementNodeU3E5__2_4(MovementTutorialNode_t612D67CD2F5CEA136C089C042E8C326FECB3018A * value)
	{
		___U3CmovementNodeU3E5__2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmovementNodeU3E5__2_4), value);
	}

	inline static int32_t get_offset_of_U3CdestroyNodeU3E5__3_5() { return static_cast<int32_t>(offsetof(U3CShowU3Ed__2_tE012D437EE48C917521209D67E5D70B984CF68E6, ___U3CdestroyNodeU3E5__3_5)); }
	inline DestroyGoalTutorialNode_tA62D39E749050BBCB967A3B7FA6CE41997DDC58D * get_U3CdestroyNodeU3E5__3_5() const { return ___U3CdestroyNodeU3E5__3_5; }
	inline DestroyGoalTutorialNode_tA62D39E749050BBCB967A3B7FA6CE41997DDC58D ** get_address_of_U3CdestroyNodeU3E5__3_5() { return &___U3CdestroyNodeU3E5__3_5; }
	inline void set_U3CdestroyNodeU3E5__3_5(DestroyGoalTutorialNode_tA62D39E749050BBCB967A3B7FA6CE41997DDC58D * value)
	{
		___U3CdestroyNodeU3E5__3_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdestroyNodeU3E5__3_5), value);
	}

	inline static int32_t get_offset_of_U3CgoalsNodeU3E5__4_6() { return static_cast<int32_t>(offsetof(U3CShowU3Ed__2_tE012D437EE48C917521209D67E5D70B984CF68E6, ___U3CgoalsNodeU3E5__4_6)); }
	inline GoalsTutorialNode_tF301448520894286687F236F80234B8A56CBE7B1 * get_U3CgoalsNodeU3E5__4_6() const { return ___U3CgoalsNodeU3E5__4_6; }
	inline GoalsTutorialNode_tF301448520894286687F236F80234B8A56CBE7B1 ** get_address_of_U3CgoalsNodeU3E5__4_6() { return &___U3CgoalsNodeU3E5__4_6; }
	inline void set_U3CgoalsNodeU3E5__4_6(GoalsTutorialNode_tF301448520894286687F236F80234B8A56CBE7B1 * value)
	{
		___U3CgoalsNodeU3E5__4_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CgoalsNodeU3E5__4_6), value);
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_7() { return static_cast<int32_t>(offsetof(U3CShowU3Ed__2_tE012D437EE48C917521209D67E5D70B984CF68E6, ___U3CU3Eu__1_7)); }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  get_U3CU3Eu__1_7() const { return ___U3CU3Eu__1_7; }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F * get_address_of_U3CU3Eu__1_7() { return &___U3CU3Eu__1_7; }
	inline void set_U3CU3Eu__1_7(TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  value)
	{
		___U3CU3Eu__1_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSHOWU3ED__2_TE012D437EE48C917521209D67E5D70B984CF68E6_H
#ifndef U3CONDESTROYU3ED__3_TE631C5A8B008EC66D79AB4E1E53B90AB2BAEC5A7_H
#define U3CONDESTROYU3ED__3_TE631C5A8B008EC66D79AB4E1E53B90AB2BAEC5A7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BattleInventoryFlow_<OnDestroy>d__3
struct  U3COnDestroyU3Ed__3_tE631C5A8B008EC66D79AB4E1E53B90AB2BAEC5A7 
{
public:
	// System.Int32 BattleInventoryFlow_<OnDestroy>d__3::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder BattleInventoryFlow_<OnDestroy>d__3::<>t__builder
	AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  ___U3CU3Et__builder_1;
	// BattleInventoryFlow BattleInventoryFlow_<OnDestroy>d__3::<>4__this
	BattleInventoryFlow_t7F82684619055127E49D19EEF227C08632E08E84 * ___U3CU3E4__this_2;
	// Entitas.IGroup`1<GameEntity> BattleInventoryFlow_<OnDestroy>d__3::<entities>5__2
	RuntimeObject* ___U3CentitiesU3E5__2_3;
	// System.Int32 BattleInventoryFlow_<OnDestroy>d__3::<initObjectives>5__3
	int32_t ___U3CinitObjectivesU3E5__3_4;
	// System.Int32 BattleInventoryFlow_<OnDestroy>d__3::<currentObjectives>5__4
	int32_t ___U3CcurrentObjectivesU3E5__4_5;
	// System.Runtime.CompilerServices.TaskAwaiter BattleInventoryFlow_<OnDestroy>d__3::<>u__1
	TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  ___U3CU3Eu__1_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3COnDestroyU3Ed__3_tE631C5A8B008EC66D79AB4E1E53B90AB2BAEC5A7, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3COnDestroyU3Ed__3_tE631C5A8B008EC66D79AB4E1E53B90AB2BAEC5A7, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487 * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  value)
	{
		___U3CU3Et__builder_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3COnDestroyU3Ed__3_tE631C5A8B008EC66D79AB4E1E53B90AB2BAEC5A7, ___U3CU3E4__this_2)); }
	inline BattleInventoryFlow_t7F82684619055127E49D19EEF227C08632E08E84 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline BattleInventoryFlow_t7F82684619055127E49D19EEF227C08632E08E84 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(BattleInventoryFlow_t7F82684619055127E49D19EEF227C08632E08E84 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CentitiesU3E5__2_3() { return static_cast<int32_t>(offsetof(U3COnDestroyU3Ed__3_tE631C5A8B008EC66D79AB4E1E53B90AB2BAEC5A7, ___U3CentitiesU3E5__2_3)); }
	inline RuntimeObject* get_U3CentitiesU3E5__2_3() const { return ___U3CentitiesU3E5__2_3; }
	inline RuntimeObject** get_address_of_U3CentitiesU3E5__2_3() { return &___U3CentitiesU3E5__2_3; }
	inline void set_U3CentitiesU3E5__2_3(RuntimeObject* value)
	{
		___U3CentitiesU3E5__2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CentitiesU3E5__2_3), value);
	}

	inline static int32_t get_offset_of_U3CinitObjectivesU3E5__3_4() { return static_cast<int32_t>(offsetof(U3COnDestroyU3Ed__3_tE631C5A8B008EC66D79AB4E1E53B90AB2BAEC5A7, ___U3CinitObjectivesU3E5__3_4)); }
	inline int32_t get_U3CinitObjectivesU3E5__3_4() const { return ___U3CinitObjectivesU3E5__3_4; }
	inline int32_t* get_address_of_U3CinitObjectivesU3E5__3_4() { return &___U3CinitObjectivesU3E5__3_4; }
	inline void set_U3CinitObjectivesU3E5__3_4(int32_t value)
	{
		___U3CinitObjectivesU3E5__3_4 = value;
	}

	inline static int32_t get_offset_of_U3CcurrentObjectivesU3E5__4_5() { return static_cast<int32_t>(offsetof(U3COnDestroyU3Ed__3_tE631C5A8B008EC66D79AB4E1E53B90AB2BAEC5A7, ___U3CcurrentObjectivesU3E5__4_5)); }
	inline int32_t get_U3CcurrentObjectivesU3E5__4_5() const { return ___U3CcurrentObjectivesU3E5__4_5; }
	inline int32_t* get_address_of_U3CcurrentObjectivesU3E5__4_5() { return &___U3CcurrentObjectivesU3E5__4_5; }
	inline void set_U3CcurrentObjectivesU3E5__4_5(int32_t value)
	{
		___U3CcurrentObjectivesU3E5__4_5 = value;
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_6() { return static_cast<int32_t>(offsetof(U3COnDestroyU3Ed__3_tE631C5A8B008EC66D79AB4E1E53B90AB2BAEC5A7, ___U3CU3Eu__1_6)); }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  get_U3CU3Eu__1_6() const { return ___U3CU3Eu__1_6; }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F * get_address_of_U3CU3Eu__1_6() { return &___U3CU3Eu__1_6; }
	inline void set_U3CU3Eu__1_6(TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  value)
	{
		___U3CU3Eu__1_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONDESTROYU3ED__3_TE631C5A8B008EC66D79AB4E1E53B90AB2BAEC5A7_H
#ifndef U3CONFIVEDESTROEDU3ED__4_T9B770EABE669404D730174009CE9ABAF5CB67D45_H
#define U3CONFIVEDESTROEDU3ED__4_T9B770EABE669404D730174009CE9ABAF5CB67D45_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BattleInventoryFlow_<OnFiveDestroed>d__4
struct  U3COnFiveDestroedU3Ed__4_t9B770EABE669404D730174009CE9ABAF5CB67D45 
{
public:
	// System.Int32 BattleInventoryFlow_<OnFiveDestroed>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder BattleInventoryFlow_<OnFiveDestroed>d__4::<>t__builder
	AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  ___U3CU3Et__builder_1;
	// BattleInventoryFlow BattleInventoryFlow_<OnFiveDestroed>d__4::<>4__this
	BattleInventoryFlow_t7F82684619055127E49D19EEF227C08632E08E84 * ___U3CU3E4__this_2;
	// GameEntity BattleInventoryFlow_<OnFiveDestroed>d__4::<battleInventory>5__2
	GameEntity_t995A5498A539905D7F1F8284D59BD48F077D78FF * ___U3CbattleInventoryU3E5__2_3;
	// System.Runtime.CompilerServices.TaskAwaiter BattleInventoryFlow_<OnFiveDestroed>d__4::<>u__1
	TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  ___U3CU3Eu__1_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3COnFiveDestroedU3Ed__4_t9B770EABE669404D730174009CE9ABAF5CB67D45, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3COnFiveDestroedU3Ed__4_t9B770EABE669404D730174009CE9ABAF5CB67D45, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487 * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  value)
	{
		___U3CU3Et__builder_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3COnFiveDestroedU3Ed__4_t9B770EABE669404D730174009CE9ABAF5CB67D45, ___U3CU3E4__this_2)); }
	inline BattleInventoryFlow_t7F82684619055127E49D19EEF227C08632E08E84 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline BattleInventoryFlow_t7F82684619055127E49D19EEF227C08632E08E84 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(BattleInventoryFlow_t7F82684619055127E49D19EEF227C08632E08E84 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CbattleInventoryU3E5__2_3() { return static_cast<int32_t>(offsetof(U3COnFiveDestroedU3Ed__4_t9B770EABE669404D730174009CE9ABAF5CB67D45, ___U3CbattleInventoryU3E5__2_3)); }
	inline GameEntity_t995A5498A539905D7F1F8284D59BD48F077D78FF * get_U3CbattleInventoryU3E5__2_3() const { return ___U3CbattleInventoryU3E5__2_3; }
	inline GameEntity_t995A5498A539905D7F1F8284D59BD48F077D78FF ** get_address_of_U3CbattleInventoryU3E5__2_3() { return &___U3CbattleInventoryU3E5__2_3; }
	inline void set_U3CbattleInventoryU3E5__2_3(GameEntity_t995A5498A539905D7F1F8284D59BD48F077D78FF * value)
	{
		___U3CbattleInventoryU3E5__2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CbattleInventoryU3E5__2_3), value);
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_4() { return static_cast<int32_t>(offsetof(U3COnFiveDestroedU3Ed__4_t9B770EABE669404D730174009CE9ABAF5CB67D45, ___U3CU3Eu__1_4)); }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  get_U3CU3Eu__1_4() const { return ___U3CU3Eu__1_4; }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F * get_address_of_U3CU3Eu__1_4() { return &___U3CU3Eu__1_4; }
	inline void set_U3CU3Eu__1_4(TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  value)
	{
		___U3CU3Eu__1_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONFIVEDESTROEDU3ED__4_T9B770EABE669404D730174009CE9ABAF5CB67D45_H
#ifndef U3CSHOWU3ED__2_T65BDDF5FF6C6738F8F950BE6ABD6F758861C044F_H
#define U3CSHOWU3ED__2_T65BDDF5FF6C6738F8F950BE6ABD6F758861C044F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BattleInventoryFlow_<Show>d__2
struct  U3CShowU3Ed__2_t65BDDF5FF6C6738F8F950BE6ABD6F758861C044F 
{
public:
	// System.Int32 BattleInventoryFlow_<Show>d__2::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder BattleInventoryFlow_<Show>d__2::<>t__builder
	AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  ___U3CU3Et__builder_1;
	// BattleInventoryFlow BattleInventoryFlow_<Show>d__2::<>4__this
	BattleInventoryFlow_t7F82684619055127E49D19EEF227C08632E08E84 * ___U3CU3E4__this_2;
	// CustomTutorialModel BattleInventoryFlow_<Show>d__2::model
	CustomTutorialModel_t7F1E6EF5B6AF58AEA9FB2573811B4562E587E635 * ___model_3;
	// DestroyFiveInventoryTutorialNode BattleInventoryFlow_<Show>d__2::<destroyFiveNode>5__2
	DestroyFiveInventoryTutorialNode_tE58CD63865141981AFF5F1A2E9A0A836D1576FF7 * ___U3CdestroyFiveNodeU3E5__2_4;
	// DestroyTowerGoalNode BattleInventoryFlow_<Show>d__2::<matchFiveNode>5__3
	DestroyTowerGoalNode_t75EDEEB6BF4F7A62DBA3DFCB647F82385DBFE311 * ___U3CmatchFiveNodeU3E5__3_5;
	// System.Runtime.CompilerServices.TaskAwaiter BattleInventoryFlow_<Show>d__2::<>u__1
	TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  ___U3CU3Eu__1_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CShowU3Ed__2_t65BDDF5FF6C6738F8F950BE6ABD6F758861C044F, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3CShowU3Ed__2_t65BDDF5FF6C6738F8F950BE6ABD6F758861C044F, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487 * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  value)
	{
		___U3CU3Et__builder_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CShowU3Ed__2_t65BDDF5FF6C6738F8F950BE6ABD6F758861C044F, ___U3CU3E4__this_2)); }
	inline BattleInventoryFlow_t7F82684619055127E49D19EEF227C08632E08E84 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline BattleInventoryFlow_t7F82684619055127E49D19EEF227C08632E08E84 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(BattleInventoryFlow_t7F82684619055127E49D19EEF227C08632E08E84 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_model_3() { return static_cast<int32_t>(offsetof(U3CShowU3Ed__2_t65BDDF5FF6C6738F8F950BE6ABD6F758861C044F, ___model_3)); }
	inline CustomTutorialModel_t7F1E6EF5B6AF58AEA9FB2573811B4562E587E635 * get_model_3() const { return ___model_3; }
	inline CustomTutorialModel_t7F1E6EF5B6AF58AEA9FB2573811B4562E587E635 ** get_address_of_model_3() { return &___model_3; }
	inline void set_model_3(CustomTutorialModel_t7F1E6EF5B6AF58AEA9FB2573811B4562E587E635 * value)
	{
		___model_3 = value;
		Il2CppCodeGenWriteBarrier((&___model_3), value);
	}

	inline static int32_t get_offset_of_U3CdestroyFiveNodeU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CShowU3Ed__2_t65BDDF5FF6C6738F8F950BE6ABD6F758861C044F, ___U3CdestroyFiveNodeU3E5__2_4)); }
	inline DestroyFiveInventoryTutorialNode_tE58CD63865141981AFF5F1A2E9A0A836D1576FF7 * get_U3CdestroyFiveNodeU3E5__2_4() const { return ___U3CdestroyFiveNodeU3E5__2_4; }
	inline DestroyFiveInventoryTutorialNode_tE58CD63865141981AFF5F1A2E9A0A836D1576FF7 ** get_address_of_U3CdestroyFiveNodeU3E5__2_4() { return &___U3CdestroyFiveNodeU3E5__2_4; }
	inline void set_U3CdestroyFiveNodeU3E5__2_4(DestroyFiveInventoryTutorialNode_tE58CD63865141981AFF5F1A2E9A0A836D1576FF7 * value)
	{
		___U3CdestroyFiveNodeU3E5__2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdestroyFiveNodeU3E5__2_4), value);
	}

	inline static int32_t get_offset_of_U3CmatchFiveNodeU3E5__3_5() { return static_cast<int32_t>(offsetof(U3CShowU3Ed__2_t65BDDF5FF6C6738F8F950BE6ABD6F758861C044F, ___U3CmatchFiveNodeU3E5__3_5)); }
	inline DestroyTowerGoalNode_t75EDEEB6BF4F7A62DBA3DFCB647F82385DBFE311 * get_U3CmatchFiveNodeU3E5__3_5() const { return ___U3CmatchFiveNodeU3E5__3_5; }
	inline DestroyTowerGoalNode_t75EDEEB6BF4F7A62DBA3DFCB647F82385DBFE311 ** get_address_of_U3CmatchFiveNodeU3E5__3_5() { return &___U3CmatchFiveNodeU3E5__3_5; }
	inline void set_U3CmatchFiveNodeU3E5__3_5(DestroyTowerGoalNode_t75EDEEB6BF4F7A62DBA3DFCB647F82385DBFE311 * value)
	{
		___U3CmatchFiveNodeU3E5__3_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmatchFiveNodeU3E5__3_5), value);
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_6() { return static_cast<int32_t>(offsetof(U3CShowU3Ed__2_t65BDDF5FF6C6738F8F950BE6ABD6F758861C044F, ___U3CU3Eu__1_6)); }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  get_U3CU3Eu__1_6() const { return ___U3CU3Eu__1_6; }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F * get_address_of_U3CU3Eu__1_6() { return &___U3CU3Eu__1_6; }
	inline void set_U3CU3Eu__1_6(TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  value)
	{
		___U3CU3Eu__1_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSHOWU3ED__2_T65BDDF5FF6C6738F8F950BE6ABD6F758861C044F_H
#ifndef U3CONCOLLECTU3ED__4_T33C2C978285BB931AE4273B5FECE627316463282_H
#define U3CONCOLLECTU3ED__4_T33C2C978285BB931AE4273B5FECE627316463282_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CollectGoalFlow_<OnCollect>d__4
struct  U3COnCollectU3Ed__4_t33C2C978285BB931AE4273B5FECE627316463282 
{
public:
	// System.Int32 CollectGoalFlow_<OnCollect>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder CollectGoalFlow_<OnCollect>d__4::<>t__builder
	AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  ___U3CU3Et__builder_1;
	// CollectGoalFlow CollectGoalFlow_<OnCollect>d__4::<>4__this
	CollectGoalFlow_tB876F66620038DC89241A21E6C080DA1B60BA94C * ___U3CU3E4__this_2;
	// GameEntity CollectGoalFlow_<OnCollect>d__4::<hero>5__2
	GameEntity_t995A5498A539905D7F1F8284D59BD48F077D78FF * ___U3CheroU3E5__2_3;
	// System.Runtime.CompilerServices.TaskAwaiter CollectGoalFlow_<OnCollect>d__4::<>u__1
	TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  ___U3CU3Eu__1_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3COnCollectU3Ed__4_t33C2C978285BB931AE4273B5FECE627316463282, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3COnCollectU3Ed__4_t33C2C978285BB931AE4273B5FECE627316463282, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487 * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  value)
	{
		___U3CU3Et__builder_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3COnCollectU3Ed__4_t33C2C978285BB931AE4273B5FECE627316463282, ___U3CU3E4__this_2)); }
	inline CollectGoalFlow_tB876F66620038DC89241A21E6C080DA1B60BA94C * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline CollectGoalFlow_tB876F66620038DC89241A21E6C080DA1B60BA94C ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(CollectGoalFlow_tB876F66620038DC89241A21E6C080DA1B60BA94C * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CheroU3E5__2_3() { return static_cast<int32_t>(offsetof(U3COnCollectU3Ed__4_t33C2C978285BB931AE4273B5FECE627316463282, ___U3CheroU3E5__2_3)); }
	inline GameEntity_t995A5498A539905D7F1F8284D59BD48F077D78FF * get_U3CheroU3E5__2_3() const { return ___U3CheroU3E5__2_3; }
	inline GameEntity_t995A5498A539905D7F1F8284D59BD48F077D78FF ** get_address_of_U3CheroU3E5__2_3() { return &___U3CheroU3E5__2_3; }
	inline void set_U3CheroU3E5__2_3(GameEntity_t995A5498A539905D7F1F8284D59BD48F077D78FF * value)
	{
		___U3CheroU3E5__2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CheroU3E5__2_3), value);
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_4() { return static_cast<int32_t>(offsetof(U3COnCollectU3Ed__4_t33C2C978285BB931AE4273B5FECE627316463282, ___U3CU3Eu__1_4)); }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  get_U3CU3Eu__1_4() const { return ___U3CU3Eu__1_4; }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F * get_address_of_U3CU3Eu__1_4() { return &___U3CU3Eu__1_4; }
	inline void set_U3CU3Eu__1_4(TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  value)
	{
		___U3CU3Eu__1_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONCOLLECTU3ED__4_T33C2C978285BB931AE4273B5FECE627316463282_H
#ifndef U3CONDELIVERU3ED__5_T270B044AF011A6B8129137B56EEA267F14883397_H
#define U3CONDELIVERU3ED__5_T270B044AF011A6B8129137B56EEA267F14883397_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CollectGoalFlow_<OnDeliver>d__5
struct  U3COnDeliverU3Ed__5_t270B044AF011A6B8129137B56EEA267F14883397 
{
public:
	// System.Int32 CollectGoalFlow_<OnDeliver>d__5::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder CollectGoalFlow_<OnDeliver>d__5::<>t__builder
	AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  ___U3CU3Et__builder_1;
	// CollectGoalFlow CollectGoalFlow_<OnDeliver>d__5::<>4__this
	CollectGoalFlow_tB876F66620038DC89241A21E6C080DA1B60BA94C * ___U3CU3E4__this_2;
	// GameEntity CollectGoalFlow_<OnDeliver>d__5::<hero>5__2
	GameEntity_t995A5498A539905D7F1F8284D59BD48F077D78FF * ___U3CheroU3E5__2_3;
	// System.Runtime.CompilerServices.TaskAwaiter CollectGoalFlow_<OnDeliver>d__5::<>u__1
	TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  ___U3CU3Eu__1_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3COnDeliverU3Ed__5_t270B044AF011A6B8129137B56EEA267F14883397, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3COnDeliverU3Ed__5_t270B044AF011A6B8129137B56EEA267F14883397, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487 * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  value)
	{
		___U3CU3Et__builder_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3COnDeliverU3Ed__5_t270B044AF011A6B8129137B56EEA267F14883397, ___U3CU3E4__this_2)); }
	inline CollectGoalFlow_tB876F66620038DC89241A21E6C080DA1B60BA94C * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline CollectGoalFlow_tB876F66620038DC89241A21E6C080DA1B60BA94C ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(CollectGoalFlow_tB876F66620038DC89241A21E6C080DA1B60BA94C * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CheroU3E5__2_3() { return static_cast<int32_t>(offsetof(U3COnDeliverU3Ed__5_t270B044AF011A6B8129137B56EEA267F14883397, ___U3CheroU3E5__2_3)); }
	inline GameEntity_t995A5498A539905D7F1F8284D59BD48F077D78FF * get_U3CheroU3E5__2_3() const { return ___U3CheroU3E5__2_3; }
	inline GameEntity_t995A5498A539905D7F1F8284D59BD48F077D78FF ** get_address_of_U3CheroU3E5__2_3() { return &___U3CheroU3E5__2_3; }
	inline void set_U3CheroU3E5__2_3(GameEntity_t995A5498A539905D7F1F8284D59BD48F077D78FF * value)
	{
		___U3CheroU3E5__2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CheroU3E5__2_3), value);
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_4() { return static_cast<int32_t>(offsetof(U3COnDeliverU3Ed__5_t270B044AF011A6B8129137B56EEA267F14883397, ___U3CU3Eu__1_4)); }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  get_U3CU3Eu__1_4() const { return ___U3CU3Eu__1_4; }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F * get_address_of_U3CU3Eu__1_4() { return &___U3CU3Eu__1_4; }
	inline void set_U3CU3Eu__1_4(TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  value)
	{
		___U3CU3Eu__1_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONDELIVERU3ED__5_T270B044AF011A6B8129137B56EEA267F14883397_H
#ifndef U3CSHOWU3ED__3_TD08670400D4E3FE215A266237C100F325F1C497B_H
#define U3CSHOWU3ED__3_TD08670400D4E3FE215A266237C100F325F1C497B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CollectGoalFlow_<Show>d__3
struct  U3CShowU3Ed__3_tD08670400D4E3FE215A266237C100F325F1C497B 
{
public:
	// System.Int32 CollectGoalFlow_<Show>d__3::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder CollectGoalFlow_<Show>d__3::<>t__builder
	AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  ___U3CU3Et__builder_1;
	// CollectGoalFlow CollectGoalFlow_<Show>d__3::<>4__this
	CollectGoalFlow_tB876F66620038DC89241A21E6C080DA1B60BA94C * ___U3CU3E4__this_2;
	// CustomTutorialModel CollectGoalFlow_<Show>d__3::model
	CustomTutorialModel_t7F1E6EF5B6AF58AEA9FB2573811B4562E587E635 * ___model_3;
	// CollectGoalTutorialNode CollectGoalFlow_<Show>d__3::<node>5__2
	CollectGoalTutorialNode_t1743E320CB9D993CF09AC77A20CED76149C74637 * ___U3CnodeU3E5__2_4;
	// System.Runtime.CompilerServices.TaskAwaiter CollectGoalFlow_<Show>d__3::<>u__1
	TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  ___U3CU3Eu__1_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CShowU3Ed__3_tD08670400D4E3FE215A266237C100F325F1C497B, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3CShowU3Ed__3_tD08670400D4E3FE215A266237C100F325F1C497B, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487 * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  value)
	{
		___U3CU3Et__builder_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CShowU3Ed__3_tD08670400D4E3FE215A266237C100F325F1C497B, ___U3CU3E4__this_2)); }
	inline CollectGoalFlow_tB876F66620038DC89241A21E6C080DA1B60BA94C * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline CollectGoalFlow_tB876F66620038DC89241A21E6C080DA1B60BA94C ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(CollectGoalFlow_tB876F66620038DC89241A21E6C080DA1B60BA94C * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_model_3() { return static_cast<int32_t>(offsetof(U3CShowU3Ed__3_tD08670400D4E3FE215A266237C100F325F1C497B, ___model_3)); }
	inline CustomTutorialModel_t7F1E6EF5B6AF58AEA9FB2573811B4562E587E635 * get_model_3() const { return ___model_3; }
	inline CustomTutorialModel_t7F1E6EF5B6AF58AEA9FB2573811B4562E587E635 ** get_address_of_model_3() { return &___model_3; }
	inline void set_model_3(CustomTutorialModel_t7F1E6EF5B6AF58AEA9FB2573811B4562E587E635 * value)
	{
		___model_3 = value;
		Il2CppCodeGenWriteBarrier((&___model_3), value);
	}

	inline static int32_t get_offset_of_U3CnodeU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CShowU3Ed__3_tD08670400D4E3FE215A266237C100F325F1C497B, ___U3CnodeU3E5__2_4)); }
	inline CollectGoalTutorialNode_t1743E320CB9D993CF09AC77A20CED76149C74637 * get_U3CnodeU3E5__2_4() const { return ___U3CnodeU3E5__2_4; }
	inline CollectGoalTutorialNode_t1743E320CB9D993CF09AC77A20CED76149C74637 ** get_address_of_U3CnodeU3E5__2_4() { return &___U3CnodeU3E5__2_4; }
	inline void set_U3CnodeU3E5__2_4(CollectGoalTutorialNode_t1743E320CB9D993CF09AC77A20CED76149C74637 * value)
	{
		___U3CnodeU3E5__2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnodeU3E5__2_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_5() { return static_cast<int32_t>(offsetof(U3CShowU3Ed__3_tD08670400D4E3FE215A266237C100F325F1C497B, ___U3CU3Eu__1_5)); }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  get_U3CU3Eu__1_5() const { return ___U3CU3Eu__1_5; }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F * get_address_of_U3CU3Eu__1_5() { return &___U3CU3Eu__1_5; }
	inline void set_U3CU3Eu__1_5(TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  value)
	{
		___U3CU3Eu__1_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSHOWU3ED__3_TD08670400D4E3FE215A266237C100F325F1C497B_H
#ifndef U3CSHOWU3ED__4_TB8228F66E8CFD8B544DBE8B10AA4C07D3EE41E4C_H
#define U3CSHOWU3ED__4_TB8228F66E8CFD8B544DBE8B10AA4C07D3EE41E4C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FirstSkillFlow_<Show>d__4
struct  U3CShowU3Ed__4_tB8228F66E8CFD8B544DBE8B10AA4C07D3EE41E4C 
{
public:
	// System.Int32 FirstSkillFlow_<Show>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder FirstSkillFlow_<Show>d__4::<>t__builder
	AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  ___U3CU3Et__builder_1;
	// FirstSkillFlow FirstSkillFlow_<Show>d__4::<>4__this
	FirstSkillFlow_t178220FD212277F8C8E0CF2D00031184B612455F * ___U3CU3E4__this_2;
	// CustomTutorialModel FirstSkillFlow_<Show>d__4::model
	CustomTutorialModel_t7F1E6EF5B6AF58AEA9FB2573811B4562E587E635 * ___model_3;
	// FirstSkillTutorialNode FirstSkillFlow_<Show>d__4::<node>5__2
	FirstSkillTutorialNode_t6A610C4EE426DE5AE3E6677C4574CDAE89B904C8 * ___U3CnodeU3E5__2_4;
	// System.Runtime.CompilerServices.TaskAwaiter FirstSkillFlow_<Show>d__4::<>u__1
	TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  ___U3CU3Eu__1_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CShowU3Ed__4_tB8228F66E8CFD8B544DBE8B10AA4C07D3EE41E4C, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3CShowU3Ed__4_tB8228F66E8CFD8B544DBE8B10AA4C07D3EE41E4C, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487 * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  value)
	{
		___U3CU3Et__builder_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CShowU3Ed__4_tB8228F66E8CFD8B544DBE8B10AA4C07D3EE41E4C, ___U3CU3E4__this_2)); }
	inline FirstSkillFlow_t178220FD212277F8C8E0CF2D00031184B612455F * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline FirstSkillFlow_t178220FD212277F8C8E0CF2D00031184B612455F ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(FirstSkillFlow_t178220FD212277F8C8E0CF2D00031184B612455F * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_model_3() { return static_cast<int32_t>(offsetof(U3CShowU3Ed__4_tB8228F66E8CFD8B544DBE8B10AA4C07D3EE41E4C, ___model_3)); }
	inline CustomTutorialModel_t7F1E6EF5B6AF58AEA9FB2573811B4562E587E635 * get_model_3() const { return ___model_3; }
	inline CustomTutorialModel_t7F1E6EF5B6AF58AEA9FB2573811B4562E587E635 ** get_address_of_model_3() { return &___model_3; }
	inline void set_model_3(CustomTutorialModel_t7F1E6EF5B6AF58AEA9FB2573811B4562E587E635 * value)
	{
		___model_3 = value;
		Il2CppCodeGenWriteBarrier((&___model_3), value);
	}

	inline static int32_t get_offset_of_U3CnodeU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CShowU3Ed__4_tB8228F66E8CFD8B544DBE8B10AA4C07D3EE41E4C, ___U3CnodeU3E5__2_4)); }
	inline FirstSkillTutorialNode_t6A610C4EE426DE5AE3E6677C4574CDAE89B904C8 * get_U3CnodeU3E5__2_4() const { return ___U3CnodeU3E5__2_4; }
	inline FirstSkillTutorialNode_t6A610C4EE426DE5AE3E6677C4574CDAE89B904C8 ** get_address_of_U3CnodeU3E5__2_4() { return &___U3CnodeU3E5__2_4; }
	inline void set_U3CnodeU3E5__2_4(FirstSkillTutorialNode_t6A610C4EE426DE5AE3E6677C4574CDAE89B904C8 * value)
	{
		___U3CnodeU3E5__2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnodeU3E5__2_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_5() { return static_cast<int32_t>(offsetof(U3CShowU3Ed__4_tB8228F66E8CFD8B544DBE8B10AA4C07D3EE41E4C, ___U3CU3Eu__1_5)); }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  get_U3CU3Eu__1_5() const { return ___U3CU3Eu__1_5; }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F * get_address_of_U3CU3Eu__1_5() { return &___U3CU3Eu__1_5; }
	inline void set_U3CU3Eu__1_5(TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  value)
	{
		___U3CU3Eu__1_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSHOWU3ED__4_TB8228F66E8CFD8B544DBE8B10AA4C07D3EE41E4C_H
#ifndef U3CONMOUSEUPU3ED__5_TF0C36A445CE9B38F7EEC3B196771FCE07470201E_H
#define U3CONMOUSEUPU3ED__5_TF0C36A445CE9B38F7EEC3B196771FCE07470201E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FourthSkillFlow_<OnMouseUp>d__5
struct  U3COnMouseUpU3Ed__5_tF0C36A445CE9B38F7EEC3B196771FCE07470201E 
{
public:
	// System.Int32 FourthSkillFlow_<OnMouseUp>d__5::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder FourthSkillFlow_<OnMouseUp>d__5::<>t__builder
	AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  ___U3CU3Et__builder_1;
	// System.Runtime.CompilerServices.TaskAwaiter FourthSkillFlow_<OnMouseUp>d__5::<>u__1
	TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  ___U3CU3Eu__1_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3COnMouseUpU3Ed__5_tF0C36A445CE9B38F7EEC3B196771FCE07470201E, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3COnMouseUpU3Ed__5_tF0C36A445CE9B38F7EEC3B196771FCE07470201E, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487 * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  value)
	{
		___U3CU3Et__builder_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_2() { return static_cast<int32_t>(offsetof(U3COnMouseUpU3Ed__5_tF0C36A445CE9B38F7EEC3B196771FCE07470201E, ___U3CU3Eu__1_2)); }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  get_U3CU3Eu__1_2() const { return ___U3CU3Eu__1_2; }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F * get_address_of_U3CU3Eu__1_2() { return &___U3CU3Eu__1_2; }
	inline void set_U3CU3Eu__1_2(TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  value)
	{
		___U3CU3Eu__1_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONMOUSEUPU3ED__5_TF0C36A445CE9B38F7EEC3B196771FCE07470201E_H
#ifndef U3CONSCREENTAPU3ED__4_TE5A32BFDE6907918F920EEA58721B270250453B7_H
#define U3CONSCREENTAPU3ED__4_TE5A32BFDE6907918F920EEA58721B270250453B7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FourthSkillFlow_<OnScreenTap>d__4
struct  U3COnScreenTapU3Ed__4_tE5A32BFDE6907918F920EEA58721B270250453B7 
{
public:
	// System.Int32 FourthSkillFlow_<OnScreenTap>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder FourthSkillFlow_<OnScreenTap>d__4::<>t__builder
	AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  ___U3CU3Et__builder_1;
	// FourthSkillFlow FourthSkillFlow_<OnScreenTap>d__4::<>4__this
	FourthSkillFlow_t30FF5419C188236877763F85BAD84CE9339BBA91 * ___U3CU3E4__this_2;
	// System.Runtime.CompilerServices.TaskAwaiter FourthSkillFlow_<OnScreenTap>d__4::<>u__1
	TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  ___U3CU3Eu__1_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3COnScreenTapU3Ed__4_tE5A32BFDE6907918F920EEA58721B270250453B7, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3COnScreenTapU3Ed__4_tE5A32BFDE6907918F920EEA58721B270250453B7, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487 * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  value)
	{
		___U3CU3Et__builder_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3COnScreenTapU3Ed__4_tE5A32BFDE6907918F920EEA58721B270250453B7, ___U3CU3E4__this_2)); }
	inline FourthSkillFlow_t30FF5419C188236877763F85BAD84CE9339BBA91 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline FourthSkillFlow_t30FF5419C188236877763F85BAD84CE9339BBA91 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(FourthSkillFlow_t30FF5419C188236877763F85BAD84CE9339BBA91 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_3() { return static_cast<int32_t>(offsetof(U3COnScreenTapU3Ed__4_tE5A32BFDE6907918F920EEA58721B270250453B7, ___U3CU3Eu__1_3)); }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  get_U3CU3Eu__1_3() const { return ___U3CU3Eu__1_3; }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F * get_address_of_U3CU3Eu__1_3() { return &___U3CU3Eu__1_3; }
	inline void set_U3CU3Eu__1_3(TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  value)
	{
		___U3CU3Eu__1_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONSCREENTAPU3ED__4_TE5A32BFDE6907918F920EEA58721B270250453B7_H
#ifndef U3CSHOWU3ED__3_TF195BCE7909E69C3A101B835AC7766233FF757B9_H
#define U3CSHOWU3ED__3_TF195BCE7909E69C3A101B835AC7766233FF757B9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FourthSkillFlow_<Show>d__3
struct  U3CShowU3Ed__3_tF195BCE7909E69C3A101B835AC7766233FF757B9 
{
public:
	// System.Int32 FourthSkillFlow_<Show>d__3::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder FourthSkillFlow_<Show>d__3::<>t__builder
	AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  ___U3CU3Et__builder_1;
	// FourthSkillFlow FourthSkillFlow_<Show>d__3::<>4__this
	FourthSkillFlow_t30FF5419C188236877763F85BAD84CE9339BBA91 * ___U3CU3E4__this_2;
	// CustomTutorialModel FourthSkillFlow_<Show>d__3::model
	CustomTutorialModel_t7F1E6EF5B6AF58AEA9FB2573811B4562E587E635 * ___model_3;
	// FourthSkillTutorialNode FourthSkillFlow_<Show>d__3::<node>5__2
	FourthSkillTutorialNode_t54BD10C3CA58AB022A554FFCA2723F0D0FB20057 * ___U3CnodeU3E5__2_4;
	// System.Runtime.CompilerServices.TaskAwaiter FourthSkillFlow_<Show>d__3::<>u__1
	TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  ___U3CU3Eu__1_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CShowU3Ed__3_tF195BCE7909E69C3A101B835AC7766233FF757B9, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3CShowU3Ed__3_tF195BCE7909E69C3A101B835AC7766233FF757B9, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487 * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  value)
	{
		___U3CU3Et__builder_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CShowU3Ed__3_tF195BCE7909E69C3A101B835AC7766233FF757B9, ___U3CU3E4__this_2)); }
	inline FourthSkillFlow_t30FF5419C188236877763F85BAD84CE9339BBA91 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline FourthSkillFlow_t30FF5419C188236877763F85BAD84CE9339BBA91 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(FourthSkillFlow_t30FF5419C188236877763F85BAD84CE9339BBA91 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_model_3() { return static_cast<int32_t>(offsetof(U3CShowU3Ed__3_tF195BCE7909E69C3A101B835AC7766233FF757B9, ___model_3)); }
	inline CustomTutorialModel_t7F1E6EF5B6AF58AEA9FB2573811B4562E587E635 * get_model_3() const { return ___model_3; }
	inline CustomTutorialModel_t7F1E6EF5B6AF58AEA9FB2573811B4562E587E635 ** get_address_of_model_3() { return &___model_3; }
	inline void set_model_3(CustomTutorialModel_t7F1E6EF5B6AF58AEA9FB2573811B4562E587E635 * value)
	{
		___model_3 = value;
		Il2CppCodeGenWriteBarrier((&___model_3), value);
	}

	inline static int32_t get_offset_of_U3CnodeU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CShowU3Ed__3_tF195BCE7909E69C3A101B835AC7766233FF757B9, ___U3CnodeU3E5__2_4)); }
	inline FourthSkillTutorialNode_t54BD10C3CA58AB022A554FFCA2723F0D0FB20057 * get_U3CnodeU3E5__2_4() const { return ___U3CnodeU3E5__2_4; }
	inline FourthSkillTutorialNode_t54BD10C3CA58AB022A554FFCA2723F0D0FB20057 ** get_address_of_U3CnodeU3E5__2_4() { return &___U3CnodeU3E5__2_4; }
	inline void set_U3CnodeU3E5__2_4(FourthSkillTutorialNode_t54BD10C3CA58AB022A554FFCA2723F0D0FB20057 * value)
	{
		___U3CnodeU3E5__2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnodeU3E5__2_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_5() { return static_cast<int32_t>(offsetof(U3CShowU3Ed__3_tF195BCE7909E69C3A101B835AC7766233FF757B9, ___U3CU3Eu__1_5)); }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  get_U3CU3Eu__1_5() const { return ___U3CU3Eu__1_5; }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F * get_address_of_U3CU3Eu__1_5() { return &___U3CU3Eu__1_5; }
	inline void set_U3CU3Eu__1_5(TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  value)
	{
		___U3CU3Eu__1_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSHOWU3ED__3_TF195BCE7909E69C3A101B835AC7766233FF757B9_H
#ifndef U3CSHOWU3ED__2_TEF5B69CA7AF0F99DB5776D7FAA552FF57CC9BEBF_H
#define U3CSHOWU3ED__2_TEF5B69CA7AF0F99DB5776D7FAA552FF57CC9BEBF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SecondSkillFlow_<Show>d__2
struct  U3CShowU3Ed__2_tEF5B69CA7AF0F99DB5776D7FAA552FF57CC9BEBF 
{
public:
	// System.Int32 SecondSkillFlow_<Show>d__2::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder SecondSkillFlow_<Show>d__2::<>t__builder
	AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  ___U3CU3Et__builder_1;
	// SecondSkillFlow SecondSkillFlow_<Show>d__2::<>4__this
	SecondSkillFlow_t56C78A79BCE637FB296A312521CD8D3C5FE89165 * ___U3CU3E4__this_2;
	// CustomTutorialModel SecondSkillFlow_<Show>d__2::model
	CustomTutorialModel_t7F1E6EF5B6AF58AEA9FB2573811B4562E587E635 * ___model_3;
	// SecondSkillTutorialNode SecondSkillFlow_<Show>d__2::<node>5__2
	SecondSkillTutorialNode_t1E6ECA8797A902ABA81F995DC3640D0FCC725A7F * ___U3CnodeU3E5__2_4;
	// System.Runtime.CompilerServices.TaskAwaiter SecondSkillFlow_<Show>d__2::<>u__1
	TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  ___U3CU3Eu__1_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CShowU3Ed__2_tEF5B69CA7AF0F99DB5776D7FAA552FF57CC9BEBF, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3CShowU3Ed__2_tEF5B69CA7AF0F99DB5776D7FAA552FF57CC9BEBF, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487 * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  value)
	{
		___U3CU3Et__builder_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CShowU3Ed__2_tEF5B69CA7AF0F99DB5776D7FAA552FF57CC9BEBF, ___U3CU3E4__this_2)); }
	inline SecondSkillFlow_t56C78A79BCE637FB296A312521CD8D3C5FE89165 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline SecondSkillFlow_t56C78A79BCE637FB296A312521CD8D3C5FE89165 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(SecondSkillFlow_t56C78A79BCE637FB296A312521CD8D3C5FE89165 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_model_3() { return static_cast<int32_t>(offsetof(U3CShowU3Ed__2_tEF5B69CA7AF0F99DB5776D7FAA552FF57CC9BEBF, ___model_3)); }
	inline CustomTutorialModel_t7F1E6EF5B6AF58AEA9FB2573811B4562E587E635 * get_model_3() const { return ___model_3; }
	inline CustomTutorialModel_t7F1E6EF5B6AF58AEA9FB2573811B4562E587E635 ** get_address_of_model_3() { return &___model_3; }
	inline void set_model_3(CustomTutorialModel_t7F1E6EF5B6AF58AEA9FB2573811B4562E587E635 * value)
	{
		___model_3 = value;
		Il2CppCodeGenWriteBarrier((&___model_3), value);
	}

	inline static int32_t get_offset_of_U3CnodeU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CShowU3Ed__2_tEF5B69CA7AF0F99DB5776D7FAA552FF57CC9BEBF, ___U3CnodeU3E5__2_4)); }
	inline SecondSkillTutorialNode_t1E6ECA8797A902ABA81F995DC3640D0FCC725A7F * get_U3CnodeU3E5__2_4() const { return ___U3CnodeU3E5__2_4; }
	inline SecondSkillTutorialNode_t1E6ECA8797A902ABA81F995DC3640D0FCC725A7F ** get_address_of_U3CnodeU3E5__2_4() { return &___U3CnodeU3E5__2_4; }
	inline void set_U3CnodeU3E5__2_4(SecondSkillTutorialNode_t1E6ECA8797A902ABA81F995DC3640D0FCC725A7F * value)
	{
		___U3CnodeU3E5__2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnodeU3E5__2_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_5() { return static_cast<int32_t>(offsetof(U3CShowU3Ed__2_tEF5B69CA7AF0F99DB5776D7FAA552FF57CC9BEBF, ___U3CU3Eu__1_5)); }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  get_U3CU3Eu__1_5() const { return ___U3CU3Eu__1_5; }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F * get_address_of_U3CU3Eu__1_5() { return &___U3CU3Eu__1_5; }
	inline void set_U3CU3Eu__1_5(TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  value)
	{
		___U3CU3Eu__1_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSHOWU3ED__2_TEF5B69CA7AF0F99DB5776D7FAA552FF57CC9BEBF_H
#ifndef STARTOURNAMENTRANKVARIABLE_TC180F6992EF9E03FF630834652B1B578945581BD_H
#define STARTOURNAMENTRANKVARIABLE_TC180F6992EF9E03FF630834652B1B578945581BD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StarTournamentRankVariable
struct  StarTournamentRankVariable_tC180F6992EF9E03FF630834652B1B578945581BD  : public Variable_t5D003723AEAA82A1E36B619BD98E937C05793BFD
{
public:
	// TournamentsVO StarTournamentRankVariable::_tournamentVO
	TournamentsVO_tCBA9A235AE4069263CE332B1E9150CDABD0E8016 * ____tournamentVO_4;

public:
	inline static int32_t get_offset_of__tournamentVO_4() { return static_cast<int32_t>(offsetof(StarTournamentRankVariable_tC180F6992EF9E03FF630834652B1B578945581BD, ____tournamentVO_4)); }
	inline TournamentsVO_tCBA9A235AE4069263CE332B1E9150CDABD0E8016 * get__tournamentVO_4() const { return ____tournamentVO_4; }
	inline TournamentsVO_tCBA9A235AE4069263CE332B1E9150CDABD0E8016 ** get_address_of__tournamentVO_4() { return &____tournamentVO_4; }
	inline void set__tournamentVO_4(TournamentsVO_tCBA9A235AE4069263CE332B1E9150CDABD0E8016 * value)
	{
		____tournamentVO_4 = value;
		Il2CppCodeGenWriteBarrier((&____tournamentVO_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STARTOURNAMENTRANKVARIABLE_TC180F6992EF9E03FF630834652B1B578945581BD_H
#ifndef STARTOURNAMENTVARIABLE_TAEC9A6E89954C3E8E99380747371340583301A90_H
#define STARTOURNAMENTVARIABLE_TAEC9A6E89954C3E8E99380747371340583301A90_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StarTournamentVariable
struct  StarTournamentVariable_tAEC9A6E89954C3E8E99380747371340583301A90  : public Variable_t5D003723AEAA82A1E36B619BD98E937C05793BFD
{
public:
	// TournamentsVO StarTournamentVariable::_tournamentVO
	TournamentsVO_tCBA9A235AE4069263CE332B1E9150CDABD0E8016 * ____tournamentVO_4;

public:
	inline static int32_t get_offset_of__tournamentVO_4() { return static_cast<int32_t>(offsetof(StarTournamentVariable_tAEC9A6E89954C3E8E99380747371340583301A90, ____tournamentVO_4)); }
	inline TournamentsVO_tCBA9A235AE4069263CE332B1E9150CDABD0E8016 * get__tournamentVO_4() const { return ____tournamentVO_4; }
	inline TournamentsVO_tCBA9A235AE4069263CE332B1E9150CDABD0E8016 ** get_address_of__tournamentVO_4() { return &____tournamentVO_4; }
	inline void set__tournamentVO_4(TournamentsVO_tCBA9A235AE4069263CE332B1E9150CDABD0E8016 * value)
	{
		____tournamentVO_4 = value;
		Il2CppCodeGenWriteBarrier((&____tournamentVO_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STARTOURNAMENTVARIABLE_TAEC9A6E89954C3E8E99380747371340583301A90_H
#ifndef DISPLAYNAMECALLBACK_T31292D7DED26E8CC661875D2DC7808003C4A0C4D_H
#define DISPLAYNAMECALLBACK_T31292D7DED26E8CC661875D2DC7808003C4A0C4D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.DisplayNameCallback
struct  DisplayNameCallback_t31292D7DED26E8CC661875D2DC7808003C4A0C4D  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISPLAYNAMECALLBACK_T31292D7DED26E8CC661875D2DC7808003C4A0C4D_H
#ifndef LEADERBOARDCALLBACK_T4F0DB136BE2987B04C4987B870819756C02516B0_H
#define LEADERBOARDCALLBACK_T4F0DB136BE2987B04C4987B870819756C02516B0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.LeaderboardCallback
struct  LeaderboardCallback_t4F0DB136BE2987B04C4987B870819756C02516B0  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEADERBOARDCALLBACK_T4F0DB136BE2987B04C4987B870819756C02516B0_H
#ifndef LOGINCALLBACK_T59A2D9666572CF04C083B1D3DCBF89AE9F916856_H
#define LOGINCALLBACK_T59A2D9666572CF04C083B1D3DCBF89AE9F916856_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.LoginCallback
struct  LoginCallback_t59A2D9666572CF04C083B1D3DCBF89AE9F916856  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGINCALLBACK_T59A2D9666572CF04C083B1D3DCBF89AE9F916856_H
#ifndef U3CSHOWU3ED__2_T125B8D889373F6DBC4587B03503162C8D4A6F3F3_H
#define U3CSHOWU3ED__2_T125B8D889373F6DBC4587B03503162C8D4A6F3F3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdSkillFlow_<Show>d__2
struct  U3CShowU3Ed__2_t125B8D889373F6DBC4587B03503162C8D4A6F3F3 
{
public:
	// System.Int32 ThirdSkillFlow_<Show>d__2::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder ThirdSkillFlow_<Show>d__2::<>t__builder
	AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  ___U3CU3Et__builder_1;
	// ThirdSkillFlow ThirdSkillFlow_<Show>d__2::<>4__this
	ThirdSkillFlow_t99B9A8894864D1DBED1774C522267C4A60B76652 * ___U3CU3E4__this_2;
	// CustomTutorialModel ThirdSkillFlow_<Show>d__2::model
	CustomTutorialModel_t7F1E6EF5B6AF58AEA9FB2573811B4562E587E635 * ___model_3;
	// ThirdSkillTutorialNode ThirdSkillFlow_<Show>d__2::<node>5__2
	ThirdSkillTutorialNode_tE7A98B1237FF73D72F138C0C69D59D0E36C3BA1D * ___U3CnodeU3E5__2_4;
	// System.Runtime.CompilerServices.TaskAwaiter ThirdSkillFlow_<Show>d__2::<>u__1
	TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  ___U3CU3Eu__1_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CShowU3Ed__2_t125B8D889373F6DBC4587B03503162C8D4A6F3F3, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3CShowU3Ed__2_t125B8D889373F6DBC4587B03503162C8D4A6F3F3, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487 * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  value)
	{
		___U3CU3Et__builder_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CShowU3Ed__2_t125B8D889373F6DBC4587B03503162C8D4A6F3F3, ___U3CU3E4__this_2)); }
	inline ThirdSkillFlow_t99B9A8894864D1DBED1774C522267C4A60B76652 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ThirdSkillFlow_t99B9A8894864D1DBED1774C522267C4A60B76652 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ThirdSkillFlow_t99B9A8894864D1DBED1774C522267C4A60B76652 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_model_3() { return static_cast<int32_t>(offsetof(U3CShowU3Ed__2_t125B8D889373F6DBC4587B03503162C8D4A6F3F3, ___model_3)); }
	inline CustomTutorialModel_t7F1E6EF5B6AF58AEA9FB2573811B4562E587E635 * get_model_3() const { return ___model_3; }
	inline CustomTutorialModel_t7F1E6EF5B6AF58AEA9FB2573811B4562E587E635 ** get_address_of_model_3() { return &___model_3; }
	inline void set_model_3(CustomTutorialModel_t7F1E6EF5B6AF58AEA9FB2573811B4562E587E635 * value)
	{
		___model_3 = value;
		Il2CppCodeGenWriteBarrier((&___model_3), value);
	}

	inline static int32_t get_offset_of_U3CnodeU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CShowU3Ed__2_t125B8D889373F6DBC4587B03503162C8D4A6F3F3, ___U3CnodeU3E5__2_4)); }
	inline ThirdSkillTutorialNode_tE7A98B1237FF73D72F138C0C69D59D0E36C3BA1D * get_U3CnodeU3E5__2_4() const { return ___U3CnodeU3E5__2_4; }
	inline ThirdSkillTutorialNode_tE7A98B1237FF73D72F138C0C69D59D0E36C3BA1D ** get_address_of_U3CnodeU3E5__2_4() { return &___U3CnodeU3E5__2_4; }
	inline void set_U3CnodeU3E5__2_4(ThirdSkillTutorialNode_tE7A98B1237FF73D72F138C0C69D59D0E36C3BA1D * value)
	{
		___U3CnodeU3E5__2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnodeU3E5__2_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_5() { return static_cast<int32_t>(offsetof(U3CShowU3Ed__2_t125B8D889373F6DBC4587B03503162C8D4A6F3F3, ___U3CU3Eu__1_5)); }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  get_U3CU3Eu__1_5() const { return ___U3CU3Eu__1_5; }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F * get_address_of_U3CU3Eu__1_5() { return &___U3CU3Eu__1_5; }
	inline void set_U3CU3Eu__1_5(TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  value)
	{
		___U3CU3Eu__1_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSHOWU3ED__2_T125B8D889373F6DBC4587B03503162C8D4A6F3F3_H
#ifndef TRIBETOURNAMENTVARIABLE_T8978F616B72F4D039DC3E75EBB14E6F4A7D15300_H
#define TRIBETOURNAMENTVARIABLE_T8978F616B72F4D039DC3E75EBB14E6F4A7D15300_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TribeTournamentVariable
struct  TribeTournamentVariable_t8978F616B72F4D039DC3E75EBB14E6F4A7D15300  : public Variable_t5D003723AEAA82A1E36B619BD98E937C05793BFD
{
public:
	// TournamentsVO TribeTournamentVariable::_tournamentVO
	TournamentsVO_tCBA9A235AE4069263CE332B1E9150CDABD0E8016 * ____tournamentVO_4;

public:
	inline static int32_t get_offset_of__tournamentVO_4() { return static_cast<int32_t>(offsetof(TribeTournamentVariable_t8978F616B72F4D039DC3E75EBB14E6F4A7D15300, ____tournamentVO_4)); }
	inline TournamentsVO_tCBA9A235AE4069263CE332B1E9150CDABD0E8016 * get__tournamentVO_4() const { return ____tournamentVO_4; }
	inline TournamentsVO_tCBA9A235AE4069263CE332B1E9150CDABD0E8016 ** get_address_of__tournamentVO_4() { return &____tournamentVO_4; }
	inline void set__tournamentVO_4(TournamentsVO_tCBA9A235AE4069263CE332B1E9150CDABD0E8016 * value)
	{
		____tournamentVO_4 = value;
		Il2CppCodeGenWriteBarrier((&____tournamentVO_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIBETOURNAMENTVARIABLE_T8978F616B72F4D039DC3E75EBB14E6F4A7D15300_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef TMONOBEHAVIOUR_TC5EA4E5C9CF2F8B1360110F3DCB3CA86F7D68FF3_H
#define TMONOBEHAVIOUR_TC5EA4E5C9CF2F8B1360110F3DCB3CA86F7D68FF3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.TMonoBehaviour
struct  TMonoBehaviour_tC5EA4E5C9CF2F8B1360110F3DCB3CA86F7D68FF3  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMONOBEHAVIOUR_TC5EA4E5C9CF2F8B1360110F3DCB3CA86F7D68FF3_H
#ifndef STARTOURNAMENTCOMPONENT_T7DD941F8F2E84CDD23E915D456046E5646A6AB0B_H
#define STARTOURNAMENTCOMPONENT_T7DD941F8F2E84CDD23E915D456046E5646A6AB0B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StarTournamentComponent
struct  StarTournamentComponent_t7DD941F8F2E84CDD23E915D456046E5646A6AB0B  : public TMonoBehaviour_tC5EA4E5C9CF2F8B1360110F3DCB3CA86F7D68FF3
{
public:
	// UnityEngine.Transform StarTournamentComponent::_parent
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____parent_4;
	// UnityEngine.UI.Button StarTournamentComponent::_button
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____button_5;
	// TMPro.TextMeshProUGUI StarTournamentComponent::_timeCount
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____timeCount_6;
	// TMPro.TextMeshProUGUI StarTournamentComponent::_rank
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____rank_7;
	// Tayr.ILibrary StarTournamentComponent::_uiMain
	RuntimeObject* ____uiMain_8;
	// TimeSystem StarTournamentComponent::_timeSystem
	TimeSystem_t3EA79F95B78D7FF6BC7D3A29AADB4E9CFB0F2CED * ____timeSystem_9;
	// StarTournamentSystem StarTournamentComponent::_tournamentSystem
	StarTournamentSystem_tBBE982F84537BACA13B9A61826F04C1F5F726C88 * ____tournamentSystem_10;
	// KickerManager StarTournamentComponent::_kickerManager
	KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA * ____kickerManager_11;
	// Zenject.DiContainer StarTournamentComponent::_diContainer
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____diContainer_12;
	// System.Boolean StarTournamentComponent::_active
	bool ____active_13;
	// System.DateTime StarTournamentComponent::_endTime
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ____endTime_14;
	// StarTournamentRankVariable StarTournamentComponent::_rankVariable
	StarTournamentRankVariable_tC180F6992EF9E03FF630834652B1B578945581BD * ____rankVariable_15;

public:
	inline static int32_t get_offset_of__parent_4() { return static_cast<int32_t>(offsetof(StarTournamentComponent_t7DD941F8F2E84CDD23E915D456046E5646A6AB0B, ____parent_4)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__parent_4() const { return ____parent_4; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__parent_4() { return &____parent_4; }
	inline void set__parent_4(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____parent_4 = value;
		Il2CppCodeGenWriteBarrier((&____parent_4), value);
	}

	inline static int32_t get_offset_of__button_5() { return static_cast<int32_t>(offsetof(StarTournamentComponent_t7DD941F8F2E84CDD23E915D456046E5646A6AB0B, ____button_5)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__button_5() const { return ____button_5; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__button_5() { return &____button_5; }
	inline void set__button_5(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____button_5 = value;
		Il2CppCodeGenWriteBarrier((&____button_5), value);
	}

	inline static int32_t get_offset_of__timeCount_6() { return static_cast<int32_t>(offsetof(StarTournamentComponent_t7DD941F8F2E84CDD23E915D456046E5646A6AB0B, ____timeCount_6)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__timeCount_6() const { return ____timeCount_6; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__timeCount_6() { return &____timeCount_6; }
	inline void set__timeCount_6(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____timeCount_6 = value;
		Il2CppCodeGenWriteBarrier((&____timeCount_6), value);
	}

	inline static int32_t get_offset_of__rank_7() { return static_cast<int32_t>(offsetof(StarTournamentComponent_t7DD941F8F2E84CDD23E915D456046E5646A6AB0B, ____rank_7)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__rank_7() const { return ____rank_7; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__rank_7() { return &____rank_7; }
	inline void set__rank_7(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____rank_7 = value;
		Il2CppCodeGenWriteBarrier((&____rank_7), value);
	}

	inline static int32_t get_offset_of__uiMain_8() { return static_cast<int32_t>(offsetof(StarTournamentComponent_t7DD941F8F2E84CDD23E915D456046E5646A6AB0B, ____uiMain_8)); }
	inline RuntimeObject* get__uiMain_8() const { return ____uiMain_8; }
	inline RuntimeObject** get_address_of__uiMain_8() { return &____uiMain_8; }
	inline void set__uiMain_8(RuntimeObject* value)
	{
		____uiMain_8 = value;
		Il2CppCodeGenWriteBarrier((&____uiMain_8), value);
	}

	inline static int32_t get_offset_of__timeSystem_9() { return static_cast<int32_t>(offsetof(StarTournamentComponent_t7DD941F8F2E84CDD23E915D456046E5646A6AB0B, ____timeSystem_9)); }
	inline TimeSystem_t3EA79F95B78D7FF6BC7D3A29AADB4E9CFB0F2CED * get__timeSystem_9() const { return ____timeSystem_9; }
	inline TimeSystem_t3EA79F95B78D7FF6BC7D3A29AADB4E9CFB0F2CED ** get_address_of__timeSystem_9() { return &____timeSystem_9; }
	inline void set__timeSystem_9(TimeSystem_t3EA79F95B78D7FF6BC7D3A29AADB4E9CFB0F2CED * value)
	{
		____timeSystem_9 = value;
		Il2CppCodeGenWriteBarrier((&____timeSystem_9), value);
	}

	inline static int32_t get_offset_of__tournamentSystem_10() { return static_cast<int32_t>(offsetof(StarTournamentComponent_t7DD941F8F2E84CDD23E915D456046E5646A6AB0B, ____tournamentSystem_10)); }
	inline StarTournamentSystem_tBBE982F84537BACA13B9A61826F04C1F5F726C88 * get__tournamentSystem_10() const { return ____tournamentSystem_10; }
	inline StarTournamentSystem_tBBE982F84537BACA13B9A61826F04C1F5F726C88 ** get_address_of__tournamentSystem_10() { return &____tournamentSystem_10; }
	inline void set__tournamentSystem_10(StarTournamentSystem_tBBE982F84537BACA13B9A61826F04C1F5F726C88 * value)
	{
		____tournamentSystem_10 = value;
		Il2CppCodeGenWriteBarrier((&____tournamentSystem_10), value);
	}

	inline static int32_t get_offset_of__kickerManager_11() { return static_cast<int32_t>(offsetof(StarTournamentComponent_t7DD941F8F2E84CDD23E915D456046E5646A6AB0B, ____kickerManager_11)); }
	inline KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA * get__kickerManager_11() const { return ____kickerManager_11; }
	inline KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA ** get_address_of__kickerManager_11() { return &____kickerManager_11; }
	inline void set__kickerManager_11(KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA * value)
	{
		____kickerManager_11 = value;
		Il2CppCodeGenWriteBarrier((&____kickerManager_11), value);
	}

	inline static int32_t get_offset_of__diContainer_12() { return static_cast<int32_t>(offsetof(StarTournamentComponent_t7DD941F8F2E84CDD23E915D456046E5646A6AB0B, ____diContainer_12)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__diContainer_12() const { return ____diContainer_12; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__diContainer_12() { return &____diContainer_12; }
	inline void set__diContainer_12(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____diContainer_12 = value;
		Il2CppCodeGenWriteBarrier((&____diContainer_12), value);
	}

	inline static int32_t get_offset_of__active_13() { return static_cast<int32_t>(offsetof(StarTournamentComponent_t7DD941F8F2E84CDD23E915D456046E5646A6AB0B, ____active_13)); }
	inline bool get__active_13() const { return ____active_13; }
	inline bool* get_address_of__active_13() { return &____active_13; }
	inline void set__active_13(bool value)
	{
		____active_13 = value;
	}

	inline static int32_t get_offset_of__endTime_14() { return static_cast<int32_t>(offsetof(StarTournamentComponent_t7DD941F8F2E84CDD23E915D456046E5646A6AB0B, ____endTime_14)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get__endTime_14() const { return ____endTime_14; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of__endTime_14() { return &____endTime_14; }
	inline void set__endTime_14(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		____endTime_14 = value;
	}

	inline static int32_t get_offset_of__rankVariable_15() { return static_cast<int32_t>(offsetof(StarTournamentComponent_t7DD941F8F2E84CDD23E915D456046E5646A6AB0B, ____rankVariable_15)); }
	inline StarTournamentRankVariable_tC180F6992EF9E03FF630834652B1B578945581BD * get__rankVariable_15() const { return ____rankVariable_15; }
	inline StarTournamentRankVariable_tC180F6992EF9E03FF630834652B1B578945581BD ** get_address_of__rankVariable_15() { return &____rankVariable_15; }
	inline void set__rankVariable_15(StarTournamentRankVariable_tC180F6992EF9E03FF630834652B1B578945581BD * value)
	{
		____rankVariable_15 = value;
		Il2CppCodeGenWriteBarrier((&____rankVariable_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STARTOURNAMENTCOMPONENT_T7DD941F8F2E84CDD23E915D456046E5646A6AB0B_H
#ifndef BASICNODE_1_T0CD6D84C6B86DCAC0B15FA6B29832B5A10179CED_H
#define BASICNODE_1_T0CD6D84C6B86DCAC0B15FA6B29832B5A10179CED_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.BasicNode`1<GameSparks.Core.GSData>
struct  BasicNode_1_t0CD6D84C6B86DCAC0B15FA6B29832B5A10179CED  : public TMonoBehaviour_tC5EA4E5C9CF2F8B1360110F3DCB3CA86F7D68FF3
{
public:
	// T Tayr.BasicNode`1::Data
	GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 * ___Data_4;
	// Tayr.INodeAnimationHandler Tayr.BasicNode`1::_nodeAnimationHandler
	RuntimeObject* ____nodeAnimationHandler_5;
	// Tayr.NodeAnimator Tayr.BasicNode`1::_nodeAnimator
	NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F * ____nodeAnimator_6;

public:
	inline static int32_t get_offset_of_Data_4() { return static_cast<int32_t>(offsetof(BasicNode_1_t0CD6D84C6B86DCAC0B15FA6B29832B5A10179CED, ___Data_4)); }
	inline GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 * get_Data_4() const { return ___Data_4; }
	inline GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 ** get_address_of_Data_4() { return &___Data_4; }
	inline void set_Data_4(GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 * value)
	{
		___Data_4 = value;
		Il2CppCodeGenWriteBarrier((&___Data_4), value);
	}

	inline static int32_t get_offset_of__nodeAnimationHandler_5() { return static_cast<int32_t>(offsetof(BasicNode_1_t0CD6D84C6B86DCAC0B15FA6B29832B5A10179CED, ____nodeAnimationHandler_5)); }
	inline RuntimeObject* get__nodeAnimationHandler_5() const { return ____nodeAnimationHandler_5; }
	inline RuntimeObject** get_address_of__nodeAnimationHandler_5() { return &____nodeAnimationHandler_5; }
	inline void set__nodeAnimationHandler_5(RuntimeObject* value)
	{
		____nodeAnimationHandler_5 = value;
		Il2CppCodeGenWriteBarrier((&____nodeAnimationHandler_5), value);
	}

	inline static int32_t get_offset_of__nodeAnimator_6() { return static_cast<int32_t>(offsetof(BasicNode_1_t0CD6D84C6B86DCAC0B15FA6B29832B5A10179CED, ____nodeAnimator_6)); }
	inline NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F * get__nodeAnimator_6() const { return ____nodeAnimator_6; }
	inline NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F ** get_address_of__nodeAnimator_6() { return &____nodeAnimator_6; }
	inline void set__nodeAnimator_6(NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F * value)
	{
		____nodeAnimator_6 = value;
		Il2CppCodeGenWriteBarrier((&____nodeAnimator_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICNODE_1_T0CD6D84C6B86DCAC0B15FA6B29832B5A10179CED_H
#ifndef BASICNODE_1_T5A702E46D2514E5417728C1C36AA87C2918A2070_H
#define BASICNODE_1_T5A702E46D2514E5417728C1C36AA87C2918A2070_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.BasicNode`1<KickTooltipData>
struct  BasicNode_1_t5A702E46D2514E5417728C1C36AA87C2918A2070  : public TMonoBehaviour_tC5EA4E5C9CF2F8B1360110F3DCB3CA86F7D68FF3
{
public:
	// T Tayr.BasicNode`1::Data
	KickTooltipData_t7912892AE01A47560991479A19D49CB87E1F4448 * ___Data_4;
	// Tayr.INodeAnimationHandler Tayr.BasicNode`1::_nodeAnimationHandler
	RuntimeObject* ____nodeAnimationHandler_5;
	// Tayr.NodeAnimator Tayr.BasicNode`1::_nodeAnimator
	NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F * ____nodeAnimator_6;

public:
	inline static int32_t get_offset_of_Data_4() { return static_cast<int32_t>(offsetof(BasicNode_1_t5A702E46D2514E5417728C1C36AA87C2918A2070, ___Data_4)); }
	inline KickTooltipData_t7912892AE01A47560991479A19D49CB87E1F4448 * get_Data_4() const { return ___Data_4; }
	inline KickTooltipData_t7912892AE01A47560991479A19D49CB87E1F4448 ** get_address_of_Data_4() { return &___Data_4; }
	inline void set_Data_4(KickTooltipData_t7912892AE01A47560991479A19D49CB87E1F4448 * value)
	{
		___Data_4 = value;
		Il2CppCodeGenWriteBarrier((&___Data_4), value);
	}

	inline static int32_t get_offset_of__nodeAnimationHandler_5() { return static_cast<int32_t>(offsetof(BasicNode_1_t5A702E46D2514E5417728C1C36AA87C2918A2070, ____nodeAnimationHandler_5)); }
	inline RuntimeObject* get__nodeAnimationHandler_5() const { return ____nodeAnimationHandler_5; }
	inline RuntimeObject** get_address_of__nodeAnimationHandler_5() { return &____nodeAnimationHandler_5; }
	inline void set__nodeAnimationHandler_5(RuntimeObject* value)
	{
		____nodeAnimationHandler_5 = value;
		Il2CppCodeGenWriteBarrier((&____nodeAnimationHandler_5), value);
	}

	inline static int32_t get_offset_of__nodeAnimator_6() { return static_cast<int32_t>(offsetof(BasicNode_1_t5A702E46D2514E5417728C1C36AA87C2918A2070, ____nodeAnimator_6)); }
	inline NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F * get__nodeAnimator_6() const { return ____nodeAnimator_6; }
	inline NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F ** get_address_of__nodeAnimator_6() { return &____nodeAnimator_6; }
	inline void set__nodeAnimator_6(NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F * value)
	{
		____nodeAnimator_6 = value;
		Il2CppCodeGenWriteBarrier((&____nodeAnimator_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICNODE_1_T5A702E46D2514E5417728C1C36AA87C2918A2070_H
#ifndef BASICNODE_1_T1BE16E2D7C883E375ADF1FDE2D0B141788813C9D_H
#define BASICNODE_1_T1BE16E2D7C883E375ADF1FDE2D0B141788813C9D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.BasicNode`1<ReportTooltipData>
struct  BasicNode_1_t1BE16E2D7C883E375ADF1FDE2D0B141788813C9D  : public TMonoBehaviour_tC5EA4E5C9CF2F8B1360110F3DCB3CA86F7D68FF3
{
public:
	// T Tayr.BasicNode`1::Data
	ReportTooltipData_tF8EDAC5D364823AA3EA0B424A1C09029CF485A93 * ___Data_4;
	// Tayr.INodeAnimationHandler Tayr.BasicNode`1::_nodeAnimationHandler
	RuntimeObject* ____nodeAnimationHandler_5;
	// Tayr.NodeAnimator Tayr.BasicNode`1::_nodeAnimator
	NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F * ____nodeAnimator_6;

public:
	inline static int32_t get_offset_of_Data_4() { return static_cast<int32_t>(offsetof(BasicNode_1_t1BE16E2D7C883E375ADF1FDE2D0B141788813C9D, ___Data_4)); }
	inline ReportTooltipData_tF8EDAC5D364823AA3EA0B424A1C09029CF485A93 * get_Data_4() const { return ___Data_4; }
	inline ReportTooltipData_tF8EDAC5D364823AA3EA0B424A1C09029CF485A93 ** get_address_of_Data_4() { return &___Data_4; }
	inline void set_Data_4(ReportTooltipData_tF8EDAC5D364823AA3EA0B424A1C09029CF485A93 * value)
	{
		___Data_4 = value;
		Il2CppCodeGenWriteBarrier((&___Data_4), value);
	}

	inline static int32_t get_offset_of__nodeAnimationHandler_5() { return static_cast<int32_t>(offsetof(BasicNode_1_t1BE16E2D7C883E375ADF1FDE2D0B141788813C9D, ____nodeAnimationHandler_5)); }
	inline RuntimeObject* get__nodeAnimationHandler_5() const { return ____nodeAnimationHandler_5; }
	inline RuntimeObject** get_address_of__nodeAnimationHandler_5() { return &____nodeAnimationHandler_5; }
	inline void set__nodeAnimationHandler_5(RuntimeObject* value)
	{
		____nodeAnimationHandler_5 = value;
		Il2CppCodeGenWriteBarrier((&____nodeAnimationHandler_5), value);
	}

	inline static int32_t get_offset_of__nodeAnimator_6() { return static_cast<int32_t>(offsetof(BasicNode_1_t1BE16E2D7C883E375ADF1FDE2D0B141788813C9D, ____nodeAnimator_6)); }
	inline NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F * get__nodeAnimator_6() const { return ____nodeAnimator_6; }
	inline NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F ** get_address_of__nodeAnimator_6() { return &____nodeAnimator_6; }
	inline void set__nodeAnimator_6(NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F * value)
	{
		____nodeAnimator_6 = value;
		Il2CppCodeGenWriteBarrier((&____nodeAnimator_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICNODE_1_T1BE16E2D7C883E375ADF1FDE2D0B141788813C9D_H
#ifndef BASICNODE_1_T80C898A8840F148EAC8A5FE47B08C3ABA79CE64D_H
#define BASICNODE_1_T80C898A8840F148EAC8A5FE47B08C3ABA79CE64D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.BasicNode`1<System.Int32>
struct  BasicNode_1_t80C898A8840F148EAC8A5FE47B08C3ABA79CE64D  : public TMonoBehaviour_tC5EA4E5C9CF2F8B1360110F3DCB3CA86F7D68FF3
{
public:
	// T Tayr.BasicNode`1::Data
	int32_t ___Data_4;
	// Tayr.INodeAnimationHandler Tayr.BasicNode`1::_nodeAnimationHandler
	RuntimeObject* ____nodeAnimationHandler_5;
	// Tayr.NodeAnimator Tayr.BasicNode`1::_nodeAnimator
	NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F * ____nodeAnimator_6;

public:
	inline static int32_t get_offset_of_Data_4() { return static_cast<int32_t>(offsetof(BasicNode_1_t80C898A8840F148EAC8A5FE47B08C3ABA79CE64D, ___Data_4)); }
	inline int32_t get_Data_4() const { return ___Data_4; }
	inline int32_t* get_address_of_Data_4() { return &___Data_4; }
	inline void set_Data_4(int32_t value)
	{
		___Data_4 = value;
	}

	inline static int32_t get_offset_of__nodeAnimationHandler_5() { return static_cast<int32_t>(offsetof(BasicNode_1_t80C898A8840F148EAC8A5FE47B08C3ABA79CE64D, ____nodeAnimationHandler_5)); }
	inline RuntimeObject* get__nodeAnimationHandler_5() const { return ____nodeAnimationHandler_5; }
	inline RuntimeObject** get_address_of__nodeAnimationHandler_5() { return &____nodeAnimationHandler_5; }
	inline void set__nodeAnimationHandler_5(RuntimeObject* value)
	{
		____nodeAnimationHandler_5 = value;
		Il2CppCodeGenWriteBarrier((&____nodeAnimationHandler_5), value);
	}

	inline static int32_t get_offset_of__nodeAnimator_6() { return static_cast<int32_t>(offsetof(BasicNode_1_t80C898A8840F148EAC8A5FE47B08C3ABA79CE64D, ____nodeAnimator_6)); }
	inline NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F * get__nodeAnimator_6() const { return ____nodeAnimator_6; }
	inline NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F ** get_address_of__nodeAnimator_6() { return &____nodeAnimator_6; }
	inline void set__nodeAnimator_6(NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F * value)
	{
		____nodeAnimator_6 = value;
		Il2CppCodeGenWriteBarrier((&____nodeAnimator_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICNODE_1_T80C898A8840F148EAC8A5FE47B08C3ABA79CE64D_H
#ifndef BASICNODE_1_T31F5A99F9379D18DE55F22D14196E346D7C1FADB_H
#define BASICNODE_1_T31F5A99F9379D18DE55F22D14196E346D7C1FADB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.BasicNode`1<System.String>
struct  BasicNode_1_t31F5A99F9379D18DE55F22D14196E346D7C1FADB  : public TMonoBehaviour_tC5EA4E5C9CF2F8B1360110F3DCB3CA86F7D68FF3
{
public:
	// T Tayr.BasicNode`1::Data
	String_t* ___Data_4;
	// Tayr.INodeAnimationHandler Tayr.BasicNode`1::_nodeAnimationHandler
	RuntimeObject* ____nodeAnimationHandler_5;
	// Tayr.NodeAnimator Tayr.BasicNode`1::_nodeAnimator
	NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F * ____nodeAnimator_6;

public:
	inline static int32_t get_offset_of_Data_4() { return static_cast<int32_t>(offsetof(BasicNode_1_t31F5A99F9379D18DE55F22D14196E346D7C1FADB, ___Data_4)); }
	inline String_t* get_Data_4() const { return ___Data_4; }
	inline String_t** get_address_of_Data_4() { return &___Data_4; }
	inline void set_Data_4(String_t* value)
	{
		___Data_4 = value;
		Il2CppCodeGenWriteBarrier((&___Data_4), value);
	}

	inline static int32_t get_offset_of__nodeAnimationHandler_5() { return static_cast<int32_t>(offsetof(BasicNode_1_t31F5A99F9379D18DE55F22D14196E346D7C1FADB, ____nodeAnimationHandler_5)); }
	inline RuntimeObject* get__nodeAnimationHandler_5() const { return ____nodeAnimationHandler_5; }
	inline RuntimeObject** get_address_of__nodeAnimationHandler_5() { return &____nodeAnimationHandler_5; }
	inline void set__nodeAnimationHandler_5(RuntimeObject* value)
	{
		____nodeAnimationHandler_5 = value;
		Il2CppCodeGenWriteBarrier((&____nodeAnimationHandler_5), value);
	}

	inline static int32_t get_offset_of__nodeAnimator_6() { return static_cast<int32_t>(offsetof(BasicNode_1_t31F5A99F9379D18DE55F22D14196E346D7C1FADB, ____nodeAnimator_6)); }
	inline NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F * get__nodeAnimator_6() const { return ____nodeAnimator_6; }
	inline NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F ** get_address_of__nodeAnimator_6() { return &____nodeAnimator_6; }
	inline void set__nodeAnimator_6(NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F * value)
	{
		____nodeAnimator_6 = value;
		Il2CppCodeGenWriteBarrier((&____nodeAnimator_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICNODE_1_T31F5A99F9379D18DE55F22D14196E346D7C1FADB_H
#ifndef TLISTITEM_T3835B3A6EDB64421C763D6938505136E6CD7628C_H
#define TLISTITEM_T3835B3A6EDB64421C763D6938505136E6CD7628C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.TListItem
struct  TListItem_t3835B3A6EDB64421C763D6938505136E6CD7628C  : public TMonoBehaviour_tC5EA4E5C9CF2F8B1360110F3DCB3CA86F7D68FF3
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLISTITEM_T3835B3A6EDB64421C763D6938505136E6CD7628C_H
#ifndef TRIBETOURNAMENTCOMPONENT_T3FD8E187603AA1F1817918ABF0BFC7A176430C1E_H
#define TRIBETOURNAMENTCOMPONENT_T3FD8E187603AA1F1817918ABF0BFC7A176430C1E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TribeTournamentComponent
struct  TribeTournamentComponent_t3FD8E187603AA1F1817918ABF0BFC7A176430C1E  : public TMonoBehaviour_tC5EA4E5C9CF2F8B1360110F3DCB3CA86F7D68FF3
{
public:
	// UnityEngine.Transform TribeTournamentComponent::_parent
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____parent_4;
	// UnityEngine.UI.Button TribeTournamentComponent::_button
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____button_5;
	// TMPro.TextMeshProUGUI TribeTournamentComponent::_timeCount
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____timeCount_6;
	// Tayr.ILibrary TribeTournamentComponent::_uiMain
	RuntimeObject* ____uiMain_7;
	// TimeSystem TribeTournamentComponent::_timeSystem
	TimeSystem_t3EA79F95B78D7FF6BC7D3A29AADB4E9CFB0F2CED * ____timeSystem_8;
	// TribeTournamentSystem TribeTournamentComponent::_tournamentSystem
	TribeTournamentSystem_t7F0DE0115218A7B9AA81C8EF2B923A4450F36424 * ____tournamentSystem_9;
	// KickerManager TribeTournamentComponent::_kickerManager
	KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA * ____kickerManager_10;
	// System.Boolean TribeTournamentComponent::_active
	bool ____active_11;
	// System.DateTime TribeTournamentComponent::_endTime
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ____endTime_12;

public:
	inline static int32_t get_offset_of__parent_4() { return static_cast<int32_t>(offsetof(TribeTournamentComponent_t3FD8E187603AA1F1817918ABF0BFC7A176430C1E, ____parent_4)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__parent_4() const { return ____parent_4; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__parent_4() { return &____parent_4; }
	inline void set__parent_4(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____parent_4 = value;
		Il2CppCodeGenWriteBarrier((&____parent_4), value);
	}

	inline static int32_t get_offset_of__button_5() { return static_cast<int32_t>(offsetof(TribeTournamentComponent_t3FD8E187603AA1F1817918ABF0BFC7A176430C1E, ____button_5)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__button_5() const { return ____button_5; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__button_5() { return &____button_5; }
	inline void set__button_5(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____button_5 = value;
		Il2CppCodeGenWriteBarrier((&____button_5), value);
	}

	inline static int32_t get_offset_of__timeCount_6() { return static_cast<int32_t>(offsetof(TribeTournamentComponent_t3FD8E187603AA1F1817918ABF0BFC7A176430C1E, ____timeCount_6)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__timeCount_6() const { return ____timeCount_6; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__timeCount_6() { return &____timeCount_6; }
	inline void set__timeCount_6(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____timeCount_6 = value;
		Il2CppCodeGenWriteBarrier((&____timeCount_6), value);
	}

	inline static int32_t get_offset_of__uiMain_7() { return static_cast<int32_t>(offsetof(TribeTournamentComponent_t3FD8E187603AA1F1817918ABF0BFC7A176430C1E, ____uiMain_7)); }
	inline RuntimeObject* get__uiMain_7() const { return ____uiMain_7; }
	inline RuntimeObject** get_address_of__uiMain_7() { return &____uiMain_7; }
	inline void set__uiMain_7(RuntimeObject* value)
	{
		____uiMain_7 = value;
		Il2CppCodeGenWriteBarrier((&____uiMain_7), value);
	}

	inline static int32_t get_offset_of__timeSystem_8() { return static_cast<int32_t>(offsetof(TribeTournamentComponent_t3FD8E187603AA1F1817918ABF0BFC7A176430C1E, ____timeSystem_8)); }
	inline TimeSystem_t3EA79F95B78D7FF6BC7D3A29AADB4E9CFB0F2CED * get__timeSystem_8() const { return ____timeSystem_8; }
	inline TimeSystem_t3EA79F95B78D7FF6BC7D3A29AADB4E9CFB0F2CED ** get_address_of__timeSystem_8() { return &____timeSystem_8; }
	inline void set__timeSystem_8(TimeSystem_t3EA79F95B78D7FF6BC7D3A29AADB4E9CFB0F2CED * value)
	{
		____timeSystem_8 = value;
		Il2CppCodeGenWriteBarrier((&____timeSystem_8), value);
	}

	inline static int32_t get_offset_of__tournamentSystem_9() { return static_cast<int32_t>(offsetof(TribeTournamentComponent_t3FD8E187603AA1F1817918ABF0BFC7A176430C1E, ____tournamentSystem_9)); }
	inline TribeTournamentSystem_t7F0DE0115218A7B9AA81C8EF2B923A4450F36424 * get__tournamentSystem_9() const { return ____tournamentSystem_9; }
	inline TribeTournamentSystem_t7F0DE0115218A7B9AA81C8EF2B923A4450F36424 ** get_address_of__tournamentSystem_9() { return &____tournamentSystem_9; }
	inline void set__tournamentSystem_9(TribeTournamentSystem_t7F0DE0115218A7B9AA81C8EF2B923A4450F36424 * value)
	{
		____tournamentSystem_9 = value;
		Il2CppCodeGenWriteBarrier((&____tournamentSystem_9), value);
	}

	inline static int32_t get_offset_of__kickerManager_10() { return static_cast<int32_t>(offsetof(TribeTournamentComponent_t3FD8E187603AA1F1817918ABF0BFC7A176430C1E, ____kickerManager_10)); }
	inline KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA * get__kickerManager_10() const { return ____kickerManager_10; }
	inline KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA ** get_address_of__kickerManager_10() { return &____kickerManager_10; }
	inline void set__kickerManager_10(KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA * value)
	{
		____kickerManager_10 = value;
		Il2CppCodeGenWriteBarrier((&____kickerManager_10), value);
	}

	inline static int32_t get_offset_of__active_11() { return static_cast<int32_t>(offsetof(TribeTournamentComponent_t3FD8E187603AA1F1817918ABF0BFC7A176430C1E, ____active_11)); }
	inline bool get__active_11() const { return ____active_11; }
	inline bool* get_address_of__active_11() { return &____active_11; }
	inline void set__active_11(bool value)
	{
		____active_11 = value;
	}

	inline static int32_t get_offset_of__endTime_12() { return static_cast<int32_t>(offsetof(TribeTournamentComponent_t3FD8E187603AA1F1817918ABF0BFC7A176430C1E, ____endTime_12)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get__endTime_12() const { return ____endTime_12; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of__endTime_12() { return &____endTime_12; }
	inline void set__endTime_12(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		____endTime_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIBETOURNAMENTCOMPONENT_T3FD8E187603AA1F1817918ABF0BFC7A176430C1E_H
#ifndef CHATHEARTLISTITEM_TBDBC364CD7AE74FAC5A8B29FA33290CBEFF79D29_H
#define CHATHEARTLISTITEM_TBDBC364CD7AE74FAC5A8B29FA33290CBEFF79D29_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChatHeartListItem
struct  ChatHeartListItem_tBDBC364CD7AE74FAC5A8B29FA33290CBEFF79D29  : public TListItem_t3835B3A6EDB64421C763D6938505136E6CD7628C
{
public:
	// TMPro.TextMeshProUGUI ChatHeartListItem::_name
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____name_4;
	// UnityEngine.UI.Button ChatHeartListItem::_helpBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____helpBtn_5;
	// TMPro.TextMeshProUGUI ChatHeartListItem::_lifeReceived
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____lifeReceived_6;
	// UserVO ChatHeartListItem::_userVO
	UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * ____userVO_7;
	// UserEvents ChatHeartListItem::_userEvents
	UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * ____userEvents_8;
	// Tayr.GameSparksPlatform ChatHeartListItem::_gameSparks
	GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * ____gameSparks_9;
	// ChatListItemData ChatHeartListItem::_item
	ChatListItemData_tB039C181C297B802B7B35327AA170A2106CDD53B * ____item_10;
	// System.Int32 ChatHeartListItem::_lifeReceivedCount
	int32_t ____lifeReceivedCount_11;
	// System.Int32 ChatHeartListItem::_index
	int32_t ____index_12;

public:
	inline static int32_t get_offset_of__name_4() { return static_cast<int32_t>(offsetof(ChatHeartListItem_tBDBC364CD7AE74FAC5A8B29FA33290CBEFF79D29, ____name_4)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__name_4() const { return ____name_4; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__name_4() { return &____name_4; }
	inline void set__name_4(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____name_4 = value;
		Il2CppCodeGenWriteBarrier((&____name_4), value);
	}

	inline static int32_t get_offset_of__helpBtn_5() { return static_cast<int32_t>(offsetof(ChatHeartListItem_tBDBC364CD7AE74FAC5A8B29FA33290CBEFF79D29, ____helpBtn_5)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__helpBtn_5() const { return ____helpBtn_5; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__helpBtn_5() { return &____helpBtn_5; }
	inline void set__helpBtn_5(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____helpBtn_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpBtn_5), value);
	}

	inline static int32_t get_offset_of__lifeReceived_6() { return static_cast<int32_t>(offsetof(ChatHeartListItem_tBDBC364CD7AE74FAC5A8B29FA33290CBEFF79D29, ____lifeReceived_6)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__lifeReceived_6() const { return ____lifeReceived_6; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__lifeReceived_6() { return &____lifeReceived_6; }
	inline void set__lifeReceived_6(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____lifeReceived_6 = value;
		Il2CppCodeGenWriteBarrier((&____lifeReceived_6), value);
	}

	inline static int32_t get_offset_of__userVO_7() { return static_cast<int32_t>(offsetof(ChatHeartListItem_tBDBC364CD7AE74FAC5A8B29FA33290CBEFF79D29, ____userVO_7)); }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * get__userVO_7() const { return ____userVO_7; }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 ** get_address_of__userVO_7() { return &____userVO_7; }
	inline void set__userVO_7(UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * value)
	{
		____userVO_7 = value;
		Il2CppCodeGenWriteBarrier((&____userVO_7), value);
	}

	inline static int32_t get_offset_of__userEvents_8() { return static_cast<int32_t>(offsetof(ChatHeartListItem_tBDBC364CD7AE74FAC5A8B29FA33290CBEFF79D29, ____userEvents_8)); }
	inline UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * get__userEvents_8() const { return ____userEvents_8; }
	inline UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 ** get_address_of__userEvents_8() { return &____userEvents_8; }
	inline void set__userEvents_8(UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * value)
	{
		____userEvents_8 = value;
		Il2CppCodeGenWriteBarrier((&____userEvents_8), value);
	}

	inline static int32_t get_offset_of__gameSparks_9() { return static_cast<int32_t>(offsetof(ChatHeartListItem_tBDBC364CD7AE74FAC5A8B29FA33290CBEFF79D29, ____gameSparks_9)); }
	inline GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * get__gameSparks_9() const { return ____gameSparks_9; }
	inline GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 ** get_address_of__gameSparks_9() { return &____gameSparks_9; }
	inline void set__gameSparks_9(GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * value)
	{
		____gameSparks_9 = value;
		Il2CppCodeGenWriteBarrier((&____gameSparks_9), value);
	}

	inline static int32_t get_offset_of__item_10() { return static_cast<int32_t>(offsetof(ChatHeartListItem_tBDBC364CD7AE74FAC5A8B29FA33290CBEFF79D29, ____item_10)); }
	inline ChatListItemData_tB039C181C297B802B7B35327AA170A2106CDD53B * get__item_10() const { return ____item_10; }
	inline ChatListItemData_tB039C181C297B802B7B35327AA170A2106CDD53B ** get_address_of__item_10() { return &____item_10; }
	inline void set__item_10(ChatListItemData_tB039C181C297B802B7B35327AA170A2106CDD53B * value)
	{
		____item_10 = value;
		Il2CppCodeGenWriteBarrier((&____item_10), value);
	}

	inline static int32_t get_offset_of__lifeReceivedCount_11() { return static_cast<int32_t>(offsetof(ChatHeartListItem_tBDBC364CD7AE74FAC5A8B29FA33290CBEFF79D29, ____lifeReceivedCount_11)); }
	inline int32_t get__lifeReceivedCount_11() const { return ____lifeReceivedCount_11; }
	inline int32_t* get_address_of__lifeReceivedCount_11() { return &____lifeReceivedCount_11; }
	inline void set__lifeReceivedCount_11(int32_t value)
	{
		____lifeReceivedCount_11 = value;
	}

	inline static int32_t get_offset_of__index_12() { return static_cast<int32_t>(offsetof(ChatHeartListItem_tBDBC364CD7AE74FAC5A8B29FA33290CBEFF79D29, ____index_12)); }
	inline int32_t get__index_12() const { return ____index_12; }
	inline int32_t* get_address_of__index_12() { return &____index_12; }
	inline void set__index_12(int32_t value)
	{
		____index_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHATHEARTLISTITEM_TBDBC364CD7AE74FAC5A8B29FA33290CBEFF79D29_H
#ifndef CHATJOINREQUESTITEM_T8DA07962248D92C46A048DD7AAA046D52226CDAC_H
#define CHATJOINREQUESTITEM_T8DA07962248D92C46A048DD7AAA046D52226CDAC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChatJoinRequestItem
struct  ChatJoinRequestItem_t8DA07962248D92C46A048DD7AAA046D52226CDAC  : public TListItem_t3835B3A6EDB64421C763D6938505136E6CD7628C
{
public:
	// TMPro.TextMeshProUGUI ChatJoinRequestItem::_name
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____name_4;
	// UnityEngine.UI.Button ChatJoinRequestItem::_yesBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____yesBtn_5;
	// UnityEngine.UI.Button ChatJoinRequestItem::_noBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____noBtn_6;
	// Tayr.GameSparksPlatform ChatJoinRequestItem::_gameSparks
	GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * ____gameSparks_7;
	// KickerManager ChatJoinRequestItem::_kickerManager
	KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA * ____kickerManager_8;
	// ChatListItemData ChatJoinRequestItem::_data
	ChatListItemData_tB039C181C297B802B7B35327AA170A2106CDD53B * ____data_9;
	// System.Int32 ChatJoinRequestItem::_index
	int32_t ____index_10;

public:
	inline static int32_t get_offset_of__name_4() { return static_cast<int32_t>(offsetof(ChatJoinRequestItem_t8DA07962248D92C46A048DD7AAA046D52226CDAC, ____name_4)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__name_4() const { return ____name_4; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__name_4() { return &____name_4; }
	inline void set__name_4(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____name_4 = value;
		Il2CppCodeGenWriteBarrier((&____name_4), value);
	}

	inline static int32_t get_offset_of__yesBtn_5() { return static_cast<int32_t>(offsetof(ChatJoinRequestItem_t8DA07962248D92C46A048DD7AAA046D52226CDAC, ____yesBtn_5)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__yesBtn_5() const { return ____yesBtn_5; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__yesBtn_5() { return &____yesBtn_5; }
	inline void set__yesBtn_5(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____yesBtn_5 = value;
		Il2CppCodeGenWriteBarrier((&____yesBtn_5), value);
	}

	inline static int32_t get_offset_of__noBtn_6() { return static_cast<int32_t>(offsetof(ChatJoinRequestItem_t8DA07962248D92C46A048DD7AAA046D52226CDAC, ____noBtn_6)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__noBtn_6() const { return ____noBtn_6; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__noBtn_6() { return &____noBtn_6; }
	inline void set__noBtn_6(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____noBtn_6 = value;
		Il2CppCodeGenWriteBarrier((&____noBtn_6), value);
	}

	inline static int32_t get_offset_of__gameSparks_7() { return static_cast<int32_t>(offsetof(ChatJoinRequestItem_t8DA07962248D92C46A048DD7AAA046D52226CDAC, ____gameSparks_7)); }
	inline GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * get__gameSparks_7() const { return ____gameSparks_7; }
	inline GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 ** get_address_of__gameSparks_7() { return &____gameSparks_7; }
	inline void set__gameSparks_7(GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * value)
	{
		____gameSparks_7 = value;
		Il2CppCodeGenWriteBarrier((&____gameSparks_7), value);
	}

	inline static int32_t get_offset_of__kickerManager_8() { return static_cast<int32_t>(offsetof(ChatJoinRequestItem_t8DA07962248D92C46A048DD7AAA046D52226CDAC, ____kickerManager_8)); }
	inline KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA * get__kickerManager_8() const { return ____kickerManager_8; }
	inline KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA ** get_address_of__kickerManager_8() { return &____kickerManager_8; }
	inline void set__kickerManager_8(KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA * value)
	{
		____kickerManager_8 = value;
		Il2CppCodeGenWriteBarrier((&____kickerManager_8), value);
	}

	inline static int32_t get_offset_of__data_9() { return static_cast<int32_t>(offsetof(ChatJoinRequestItem_t8DA07962248D92C46A048DD7AAA046D52226CDAC, ____data_9)); }
	inline ChatListItemData_tB039C181C297B802B7B35327AA170A2106CDD53B * get__data_9() const { return ____data_9; }
	inline ChatListItemData_tB039C181C297B802B7B35327AA170A2106CDD53B ** get_address_of__data_9() { return &____data_9; }
	inline void set__data_9(ChatListItemData_tB039C181C297B802B7B35327AA170A2106CDD53B * value)
	{
		____data_9 = value;
		Il2CppCodeGenWriteBarrier((&____data_9), value);
	}

	inline static int32_t get_offset_of__index_10() { return static_cast<int32_t>(offsetof(ChatJoinRequestItem_t8DA07962248D92C46A048DD7AAA046D52226CDAC, ____index_10)); }
	inline int32_t get__index_10() const { return ____index_10; }
	inline int32_t* get_address_of__index_10() { return &____index_10; }
	inline void set__index_10(int32_t value)
	{
		____index_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHATJOINREQUESTITEM_T8DA07962248D92C46A048DD7AAA046D52226CDAC_H
#ifndef CHATNODE_T7A741E0D933BAFC733CCBF17E9D553DA370552A0_H
#define CHATNODE_T7A741E0D933BAFC733CCBF17E9D553DA370552A0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChatNode
struct  ChatNode_t7A741E0D933BAFC733CCBF17E9D553DA370552A0  : public BasicNode_1_t31F5A99F9379D18DE55F22D14196E346D7C1FADB
{
public:
	// Tayr.TDynamicList ChatNode::_chatList
	TDynamicList_tF4B70F991EE13A3CB137902A92EC1A729C06DB19 * ____chatList_7;
	// UnityEngine.UI.Button ChatNode::_chatBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____chatBtn_8;
	// UnityEngine.UI.Button ChatNode::_heartBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____heartBtn_9;
	// Tayr.GameSparksPlatform ChatNode::_gameSparks
	GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * ____gameSparks_10;
	// Tayr.ILibrary ChatNode::_uiMain
	RuntimeObject* ____uiMain_11;
	// UserVO ChatNode::_userVo
	UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * ____userVo_12;
	// UserEvents ChatNode::_userEvents
	UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * ____userEvents_13;
	// UnityEngine.TouchScreenKeyboard ChatNode::_keyboard
	TouchScreenKeyboard_t2A69F85698E9780470181532D3F2BC903623FD90 * ____keyboard_14;

public:
	inline static int32_t get_offset_of__chatList_7() { return static_cast<int32_t>(offsetof(ChatNode_t7A741E0D933BAFC733CCBF17E9D553DA370552A0, ____chatList_7)); }
	inline TDynamicList_tF4B70F991EE13A3CB137902A92EC1A729C06DB19 * get__chatList_7() const { return ____chatList_7; }
	inline TDynamicList_tF4B70F991EE13A3CB137902A92EC1A729C06DB19 ** get_address_of__chatList_7() { return &____chatList_7; }
	inline void set__chatList_7(TDynamicList_tF4B70F991EE13A3CB137902A92EC1A729C06DB19 * value)
	{
		____chatList_7 = value;
		Il2CppCodeGenWriteBarrier((&____chatList_7), value);
	}

	inline static int32_t get_offset_of__chatBtn_8() { return static_cast<int32_t>(offsetof(ChatNode_t7A741E0D933BAFC733CCBF17E9D553DA370552A0, ____chatBtn_8)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__chatBtn_8() const { return ____chatBtn_8; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__chatBtn_8() { return &____chatBtn_8; }
	inline void set__chatBtn_8(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____chatBtn_8 = value;
		Il2CppCodeGenWriteBarrier((&____chatBtn_8), value);
	}

	inline static int32_t get_offset_of__heartBtn_9() { return static_cast<int32_t>(offsetof(ChatNode_t7A741E0D933BAFC733CCBF17E9D553DA370552A0, ____heartBtn_9)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__heartBtn_9() const { return ____heartBtn_9; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__heartBtn_9() { return &____heartBtn_9; }
	inline void set__heartBtn_9(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____heartBtn_9 = value;
		Il2CppCodeGenWriteBarrier((&____heartBtn_9), value);
	}

	inline static int32_t get_offset_of__gameSparks_10() { return static_cast<int32_t>(offsetof(ChatNode_t7A741E0D933BAFC733CCBF17E9D553DA370552A0, ____gameSparks_10)); }
	inline GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * get__gameSparks_10() const { return ____gameSparks_10; }
	inline GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 ** get_address_of__gameSparks_10() { return &____gameSparks_10; }
	inline void set__gameSparks_10(GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * value)
	{
		____gameSparks_10 = value;
		Il2CppCodeGenWriteBarrier((&____gameSparks_10), value);
	}

	inline static int32_t get_offset_of__uiMain_11() { return static_cast<int32_t>(offsetof(ChatNode_t7A741E0D933BAFC733CCBF17E9D553DA370552A0, ____uiMain_11)); }
	inline RuntimeObject* get__uiMain_11() const { return ____uiMain_11; }
	inline RuntimeObject** get_address_of__uiMain_11() { return &____uiMain_11; }
	inline void set__uiMain_11(RuntimeObject* value)
	{
		____uiMain_11 = value;
		Il2CppCodeGenWriteBarrier((&____uiMain_11), value);
	}

	inline static int32_t get_offset_of__userVo_12() { return static_cast<int32_t>(offsetof(ChatNode_t7A741E0D933BAFC733CCBF17E9D553DA370552A0, ____userVo_12)); }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * get__userVo_12() const { return ____userVo_12; }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 ** get_address_of__userVo_12() { return &____userVo_12; }
	inline void set__userVo_12(UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * value)
	{
		____userVo_12 = value;
		Il2CppCodeGenWriteBarrier((&____userVo_12), value);
	}

	inline static int32_t get_offset_of__userEvents_13() { return static_cast<int32_t>(offsetof(ChatNode_t7A741E0D933BAFC733CCBF17E9D553DA370552A0, ____userEvents_13)); }
	inline UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * get__userEvents_13() const { return ____userEvents_13; }
	inline UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 ** get_address_of__userEvents_13() { return &____userEvents_13; }
	inline void set__userEvents_13(UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * value)
	{
		____userEvents_13 = value;
		Il2CppCodeGenWriteBarrier((&____userEvents_13), value);
	}

	inline static int32_t get_offset_of__keyboard_14() { return static_cast<int32_t>(offsetof(ChatNode_t7A741E0D933BAFC733CCBF17E9D553DA370552A0, ____keyboard_14)); }
	inline TouchScreenKeyboard_t2A69F85698E9780470181532D3F2BC903623FD90 * get__keyboard_14() const { return ____keyboard_14; }
	inline TouchScreenKeyboard_t2A69F85698E9780470181532D3F2BC903623FD90 ** get_address_of__keyboard_14() { return &____keyboard_14; }
	inline void set__keyboard_14(TouchScreenKeyboard_t2A69F85698E9780470181532D3F2BC903623FD90 * value)
	{
		____keyboard_14 = value;
		Il2CppCodeGenWriteBarrier((&____keyboard_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHATNODE_T7A741E0D933BAFC733CCBF17E9D553DA370552A0_H
#ifndef CHATRECEIVEMSGLISTITEM_TD9C9B67291E9A1C2E30707832237FA6CC0082670_H
#define CHATRECEIVEMSGLISTITEM_TD9C9B67291E9A1C2E30707832237FA6CC0082670_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChatReceiveMsgListItem
struct  ChatReceiveMsgListItem_tD9C9B67291E9A1C2E30707832237FA6CC0082670  : public TListItem_t3835B3A6EDB64421C763D6938505136E6CD7628C
{
public:
	// UnityEngine.UI.Button ChatReceiveMsgListItem::_btn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____btn_4;
	// TMPro.TextMeshProUGUI ChatReceiveMsgListItem::_name
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____name_5;
	// TMPro.TextMeshProUGUI ChatReceiveMsgListItem::_message
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____message_6;
	// TMPro.TextMeshProUGUI ChatReceiveMsgListItem::_time
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____time_7;
	// Tayr.ILibrary ChatReceiveMsgListItem::_uiMain
	RuntimeObject* ____uiMain_8;
	// ChatListItemData ChatReceiveMsgListItem::_item
	ChatListItemData_tB039C181C297B802B7B35327AA170A2106CDD53B * ____item_9;

public:
	inline static int32_t get_offset_of__btn_4() { return static_cast<int32_t>(offsetof(ChatReceiveMsgListItem_tD9C9B67291E9A1C2E30707832237FA6CC0082670, ____btn_4)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__btn_4() const { return ____btn_4; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__btn_4() { return &____btn_4; }
	inline void set__btn_4(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____btn_4 = value;
		Il2CppCodeGenWriteBarrier((&____btn_4), value);
	}

	inline static int32_t get_offset_of__name_5() { return static_cast<int32_t>(offsetof(ChatReceiveMsgListItem_tD9C9B67291E9A1C2E30707832237FA6CC0082670, ____name_5)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__name_5() const { return ____name_5; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__name_5() { return &____name_5; }
	inline void set__name_5(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____name_5 = value;
		Il2CppCodeGenWriteBarrier((&____name_5), value);
	}

	inline static int32_t get_offset_of__message_6() { return static_cast<int32_t>(offsetof(ChatReceiveMsgListItem_tD9C9B67291E9A1C2E30707832237FA6CC0082670, ____message_6)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__message_6() const { return ____message_6; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__message_6() { return &____message_6; }
	inline void set__message_6(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____message_6 = value;
		Il2CppCodeGenWriteBarrier((&____message_6), value);
	}

	inline static int32_t get_offset_of__time_7() { return static_cast<int32_t>(offsetof(ChatReceiveMsgListItem_tD9C9B67291E9A1C2E30707832237FA6CC0082670, ____time_7)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__time_7() const { return ____time_7; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__time_7() { return &____time_7; }
	inline void set__time_7(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____time_7 = value;
		Il2CppCodeGenWriteBarrier((&____time_7), value);
	}

	inline static int32_t get_offset_of__uiMain_8() { return static_cast<int32_t>(offsetof(ChatReceiveMsgListItem_tD9C9B67291E9A1C2E30707832237FA6CC0082670, ____uiMain_8)); }
	inline RuntimeObject* get__uiMain_8() const { return ____uiMain_8; }
	inline RuntimeObject** get_address_of__uiMain_8() { return &____uiMain_8; }
	inline void set__uiMain_8(RuntimeObject* value)
	{
		____uiMain_8 = value;
		Il2CppCodeGenWriteBarrier((&____uiMain_8), value);
	}

	inline static int32_t get_offset_of__item_9() { return static_cast<int32_t>(offsetof(ChatReceiveMsgListItem_tD9C9B67291E9A1C2E30707832237FA6CC0082670, ____item_9)); }
	inline ChatListItemData_tB039C181C297B802B7B35327AA170A2106CDD53B * get__item_9() const { return ____item_9; }
	inline ChatListItemData_tB039C181C297B802B7B35327AA170A2106CDD53B ** get_address_of__item_9() { return &____item_9; }
	inline void set__item_9(ChatListItemData_tB039C181C297B802B7B35327AA170A2106CDD53B * value)
	{
		____item_9 = value;
		Il2CppCodeGenWriteBarrier((&____item_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHATRECEIVEMSGLISTITEM_TD9C9B67291E9A1C2E30707832237FA6CC0082670_H
#ifndef CHATSENDMSGLISTITEM_TC821821D0C7D3E952A9848B38025C10F436A2884_H
#define CHATSENDMSGLISTITEM_TC821821D0C7D3E952A9848B38025C10F436A2884_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChatSendMsgListItem
struct  ChatSendMsgListItem_tC821821D0C7D3E952A9848B38025C10F436A2884  : public TListItem_t3835B3A6EDB64421C763D6938505136E6CD7628C
{
public:
	// TMPro.TextMeshProUGUI ChatSendMsgListItem::_name
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____name_4;
	// TMPro.TextMeshProUGUI ChatSendMsgListItem::_message
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____message_5;
	// TMPro.TextMeshProUGUI ChatSendMsgListItem::_time
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____time_6;

public:
	inline static int32_t get_offset_of__name_4() { return static_cast<int32_t>(offsetof(ChatSendMsgListItem_tC821821D0C7D3E952A9848B38025C10F436A2884, ____name_4)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__name_4() const { return ____name_4; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__name_4() { return &____name_4; }
	inline void set__name_4(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____name_4 = value;
		Il2CppCodeGenWriteBarrier((&____name_4), value);
	}

	inline static int32_t get_offset_of__message_5() { return static_cast<int32_t>(offsetof(ChatSendMsgListItem_tC821821D0C7D3E952A9848B38025C10F436A2884, ____message_5)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__message_5() const { return ____message_5; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__message_5() { return &____message_5; }
	inline void set__message_5(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____message_5 = value;
		Il2CppCodeGenWriteBarrier((&____message_5), value);
	}

	inline static int32_t get_offset_of__time_6() { return static_cast<int32_t>(offsetof(ChatSendMsgListItem_tC821821D0C7D3E952A9848B38025C10F436A2884, ____time_6)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__time_6() const { return ____time_6; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__time_6() { return &____time_6; }
	inline void set__time_6(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____time_6 = value;
		Il2CppCodeGenWriteBarrier((&____time_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHATSENDMSGLISTITEM_TC821821D0C7D3E952A9848B38025C10F436A2884_H
#ifndef CREATETEAMNODE_T83E09D1D20B2ADF00639F2549FD9E6935F405689_H
#define CREATETEAMNODE_T83E09D1D20B2ADF00639F2549FD9E6935F405689_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CreateTeamNode
struct  CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689  : public BasicNode_1_t80C898A8840F148EAC8A5FE47B08C3ABA79CE64D
{
public:
	// TMPro.TMP_InputField CreateTeamNode::_nameField
	TMP_InputField_tC3C57E697A57232E8A855D39600CF06CFDA8F6CB * ____nameField_7;
	// UnityEngine.UI.Button CreateTeamNode::_browseBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____browseBtn_8;
	// UnityEngine.UI.Image CreateTeamNode::_teamIcon
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____teamIcon_9;
	// TMPro.TMP_InputField CreateTeamNode::_descriptionField
	TMP_InputField_tC3C57E697A57232E8A855D39600CF06CFDA8F6CB * ____descriptionField_10;
	// UnityEngine.UI.Button CreateTeamNode::_typeLeftBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____typeLeftBtn_11;
	// UnityEngine.UI.Button CreateTeamNode::_typeRightBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____typeRightBtn_12;
	// TMPro.TextMeshProUGUI CreateTeamNode::_type
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____type_13;
	// UnityEngine.UI.Button CreateTeamNode::_requiredLeftBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____requiredLeftBtn_14;
	// UnityEngine.UI.Button CreateTeamNode::_requiredRightBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____requiredRightBtn_15;
	// TMPro.TextMeshProUGUI CreateTeamNode::_required
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____required_16;
	// UnityEngine.UI.Button CreateTeamNode::_createBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____createBtn_17;
	// Tayr.ILibrary CreateTeamNode::_iconLibrary
	RuntimeObject* ____iconLibrary_18;
	// Tayr.ILibrary CreateTeamNode::_uiMain
	RuntimeObject* ____uiMain_19;
	// Tayr.GameSparksPlatform CreateTeamNode::_gameSpark
	GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * ____gameSpark_20;
	// UserEvents CreateTeamNode::_userEvents
	UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * ____userEvents_21;
	// UserVO CreateTeamNode::_userVo
	UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * ____userVo_22;
	// KickerManager CreateTeamNode::_kickerManager
	KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA * ____kickerManager_23;
	// Tayr.VOSaver CreateTeamNode::_voSaver
	VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * ____voSaver_24;
	// TEAM_TYPE CreateTeamNode::_currentType
	int32_t ____currentType_25;
	// System.Int32 CreateTeamNode::_currentRequirement
	int32_t ____currentRequirement_26;
	// System.Int32 CreateTeamNode::_currentIcon
	int32_t ____currentIcon_27;
	// Tayr.INode CreateTeamNode::_badgesNode
	RuntimeObject* ____badgesNode_28;

public:
	inline static int32_t get_offset_of__nameField_7() { return static_cast<int32_t>(offsetof(CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689, ____nameField_7)); }
	inline TMP_InputField_tC3C57E697A57232E8A855D39600CF06CFDA8F6CB * get__nameField_7() const { return ____nameField_7; }
	inline TMP_InputField_tC3C57E697A57232E8A855D39600CF06CFDA8F6CB ** get_address_of__nameField_7() { return &____nameField_7; }
	inline void set__nameField_7(TMP_InputField_tC3C57E697A57232E8A855D39600CF06CFDA8F6CB * value)
	{
		____nameField_7 = value;
		Il2CppCodeGenWriteBarrier((&____nameField_7), value);
	}

	inline static int32_t get_offset_of__browseBtn_8() { return static_cast<int32_t>(offsetof(CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689, ____browseBtn_8)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__browseBtn_8() const { return ____browseBtn_8; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__browseBtn_8() { return &____browseBtn_8; }
	inline void set__browseBtn_8(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____browseBtn_8 = value;
		Il2CppCodeGenWriteBarrier((&____browseBtn_8), value);
	}

	inline static int32_t get_offset_of__teamIcon_9() { return static_cast<int32_t>(offsetof(CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689, ____teamIcon_9)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__teamIcon_9() const { return ____teamIcon_9; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__teamIcon_9() { return &____teamIcon_9; }
	inline void set__teamIcon_9(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____teamIcon_9 = value;
		Il2CppCodeGenWriteBarrier((&____teamIcon_9), value);
	}

	inline static int32_t get_offset_of__descriptionField_10() { return static_cast<int32_t>(offsetof(CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689, ____descriptionField_10)); }
	inline TMP_InputField_tC3C57E697A57232E8A855D39600CF06CFDA8F6CB * get__descriptionField_10() const { return ____descriptionField_10; }
	inline TMP_InputField_tC3C57E697A57232E8A855D39600CF06CFDA8F6CB ** get_address_of__descriptionField_10() { return &____descriptionField_10; }
	inline void set__descriptionField_10(TMP_InputField_tC3C57E697A57232E8A855D39600CF06CFDA8F6CB * value)
	{
		____descriptionField_10 = value;
		Il2CppCodeGenWriteBarrier((&____descriptionField_10), value);
	}

	inline static int32_t get_offset_of__typeLeftBtn_11() { return static_cast<int32_t>(offsetof(CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689, ____typeLeftBtn_11)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__typeLeftBtn_11() const { return ____typeLeftBtn_11; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__typeLeftBtn_11() { return &____typeLeftBtn_11; }
	inline void set__typeLeftBtn_11(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____typeLeftBtn_11 = value;
		Il2CppCodeGenWriteBarrier((&____typeLeftBtn_11), value);
	}

	inline static int32_t get_offset_of__typeRightBtn_12() { return static_cast<int32_t>(offsetof(CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689, ____typeRightBtn_12)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__typeRightBtn_12() const { return ____typeRightBtn_12; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__typeRightBtn_12() { return &____typeRightBtn_12; }
	inline void set__typeRightBtn_12(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____typeRightBtn_12 = value;
		Il2CppCodeGenWriteBarrier((&____typeRightBtn_12), value);
	}

	inline static int32_t get_offset_of__type_13() { return static_cast<int32_t>(offsetof(CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689, ____type_13)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__type_13() const { return ____type_13; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__type_13() { return &____type_13; }
	inline void set__type_13(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____type_13 = value;
		Il2CppCodeGenWriteBarrier((&____type_13), value);
	}

	inline static int32_t get_offset_of__requiredLeftBtn_14() { return static_cast<int32_t>(offsetof(CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689, ____requiredLeftBtn_14)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__requiredLeftBtn_14() const { return ____requiredLeftBtn_14; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__requiredLeftBtn_14() { return &____requiredLeftBtn_14; }
	inline void set__requiredLeftBtn_14(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____requiredLeftBtn_14 = value;
		Il2CppCodeGenWriteBarrier((&____requiredLeftBtn_14), value);
	}

	inline static int32_t get_offset_of__requiredRightBtn_15() { return static_cast<int32_t>(offsetof(CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689, ____requiredRightBtn_15)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__requiredRightBtn_15() const { return ____requiredRightBtn_15; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__requiredRightBtn_15() { return &____requiredRightBtn_15; }
	inline void set__requiredRightBtn_15(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____requiredRightBtn_15 = value;
		Il2CppCodeGenWriteBarrier((&____requiredRightBtn_15), value);
	}

	inline static int32_t get_offset_of__required_16() { return static_cast<int32_t>(offsetof(CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689, ____required_16)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__required_16() const { return ____required_16; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__required_16() { return &____required_16; }
	inline void set__required_16(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____required_16 = value;
		Il2CppCodeGenWriteBarrier((&____required_16), value);
	}

	inline static int32_t get_offset_of__createBtn_17() { return static_cast<int32_t>(offsetof(CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689, ____createBtn_17)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__createBtn_17() const { return ____createBtn_17; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__createBtn_17() { return &____createBtn_17; }
	inline void set__createBtn_17(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____createBtn_17 = value;
		Il2CppCodeGenWriteBarrier((&____createBtn_17), value);
	}

	inline static int32_t get_offset_of__iconLibrary_18() { return static_cast<int32_t>(offsetof(CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689, ____iconLibrary_18)); }
	inline RuntimeObject* get__iconLibrary_18() const { return ____iconLibrary_18; }
	inline RuntimeObject** get_address_of__iconLibrary_18() { return &____iconLibrary_18; }
	inline void set__iconLibrary_18(RuntimeObject* value)
	{
		____iconLibrary_18 = value;
		Il2CppCodeGenWriteBarrier((&____iconLibrary_18), value);
	}

	inline static int32_t get_offset_of__uiMain_19() { return static_cast<int32_t>(offsetof(CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689, ____uiMain_19)); }
	inline RuntimeObject* get__uiMain_19() const { return ____uiMain_19; }
	inline RuntimeObject** get_address_of__uiMain_19() { return &____uiMain_19; }
	inline void set__uiMain_19(RuntimeObject* value)
	{
		____uiMain_19 = value;
		Il2CppCodeGenWriteBarrier((&____uiMain_19), value);
	}

	inline static int32_t get_offset_of__gameSpark_20() { return static_cast<int32_t>(offsetof(CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689, ____gameSpark_20)); }
	inline GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * get__gameSpark_20() const { return ____gameSpark_20; }
	inline GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 ** get_address_of__gameSpark_20() { return &____gameSpark_20; }
	inline void set__gameSpark_20(GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * value)
	{
		____gameSpark_20 = value;
		Il2CppCodeGenWriteBarrier((&____gameSpark_20), value);
	}

	inline static int32_t get_offset_of__userEvents_21() { return static_cast<int32_t>(offsetof(CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689, ____userEvents_21)); }
	inline UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * get__userEvents_21() const { return ____userEvents_21; }
	inline UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 ** get_address_of__userEvents_21() { return &____userEvents_21; }
	inline void set__userEvents_21(UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * value)
	{
		____userEvents_21 = value;
		Il2CppCodeGenWriteBarrier((&____userEvents_21), value);
	}

	inline static int32_t get_offset_of__userVo_22() { return static_cast<int32_t>(offsetof(CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689, ____userVo_22)); }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * get__userVo_22() const { return ____userVo_22; }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 ** get_address_of__userVo_22() { return &____userVo_22; }
	inline void set__userVo_22(UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * value)
	{
		____userVo_22 = value;
		Il2CppCodeGenWriteBarrier((&____userVo_22), value);
	}

	inline static int32_t get_offset_of__kickerManager_23() { return static_cast<int32_t>(offsetof(CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689, ____kickerManager_23)); }
	inline KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA * get__kickerManager_23() const { return ____kickerManager_23; }
	inline KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA ** get_address_of__kickerManager_23() { return &____kickerManager_23; }
	inline void set__kickerManager_23(KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA * value)
	{
		____kickerManager_23 = value;
		Il2CppCodeGenWriteBarrier((&____kickerManager_23), value);
	}

	inline static int32_t get_offset_of__voSaver_24() { return static_cast<int32_t>(offsetof(CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689, ____voSaver_24)); }
	inline VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * get__voSaver_24() const { return ____voSaver_24; }
	inline VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F ** get_address_of__voSaver_24() { return &____voSaver_24; }
	inline void set__voSaver_24(VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * value)
	{
		____voSaver_24 = value;
		Il2CppCodeGenWriteBarrier((&____voSaver_24), value);
	}

	inline static int32_t get_offset_of__currentType_25() { return static_cast<int32_t>(offsetof(CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689, ____currentType_25)); }
	inline int32_t get__currentType_25() const { return ____currentType_25; }
	inline int32_t* get_address_of__currentType_25() { return &____currentType_25; }
	inline void set__currentType_25(int32_t value)
	{
		____currentType_25 = value;
	}

	inline static int32_t get_offset_of__currentRequirement_26() { return static_cast<int32_t>(offsetof(CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689, ____currentRequirement_26)); }
	inline int32_t get__currentRequirement_26() const { return ____currentRequirement_26; }
	inline int32_t* get_address_of__currentRequirement_26() { return &____currentRequirement_26; }
	inline void set__currentRequirement_26(int32_t value)
	{
		____currentRequirement_26 = value;
	}

	inline static int32_t get_offset_of__currentIcon_27() { return static_cast<int32_t>(offsetof(CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689, ____currentIcon_27)); }
	inline int32_t get__currentIcon_27() const { return ____currentIcon_27; }
	inline int32_t* get_address_of__currentIcon_27() { return &____currentIcon_27; }
	inline void set__currentIcon_27(int32_t value)
	{
		____currentIcon_27 = value;
	}

	inline static int32_t get_offset_of__badgesNode_28() { return static_cast<int32_t>(offsetof(CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689, ____badgesNode_28)); }
	inline RuntimeObject* get__badgesNode_28() const { return ____badgesNode_28; }
	inline RuntimeObject** get_address_of__badgesNode_28() { return &____badgesNode_28; }
	inline void set__badgesNode_28(RuntimeObject* value)
	{
		____badgesNode_28 = value;
		Il2CppCodeGenWriteBarrier((&____badgesNode_28), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CREATETEAMNODE_T83E09D1D20B2ADF00639F2549FD9E6935F405689_H
#ifndef HEARTSRECEIVEDNODE_TFA9035693063BE76AAF99DFBE0BB801E47860B26_H
#define HEARTSRECEIVEDNODE_TFA9035693063BE76AAF99DFBE0BB801E47860B26_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HeartsReceivedNode
struct  HeartsReceivedNode_tFA9035693063BE76AAF99DFBE0BB801E47860B26  : public BasicNode_1_t31F5A99F9379D18DE55F22D14196E346D7C1FADB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEARTSRECEIVEDNODE_TFA9035693063BE76AAF99DFBE0BB801E47860B26_H
#ifndef ICONSLISTITEM_TFCE5D92F5E1D082DB67513DB5FE32E4D863A40A7_H
#define ICONSLISTITEM_TFCE5D92F5E1D082DB67513DB5FE32E4D863A40A7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IconsListItem
struct  IconsListItem_tFCE5D92F5E1D082DB67513DB5FE32E4D863A40A7  : public TListItem_t3835B3A6EDB64421C763D6938505136E6CD7628C
{
public:
	// UnityEngine.UI.Image IconsListItem::_icon
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____icon_4;
	// UnityEngine.UI.Button IconsListItem::_selectIcon
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____selectIcon_5;
	// Tayr.ILibrary IconsListItem::_iconLibrary
	RuntimeObject* ____iconLibrary_6;
	// UserEvents IconsListItem::_userEvents
	UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * ____userEvents_7;
	// System.Int32 IconsListItem::_index
	int32_t ____index_8;

public:
	inline static int32_t get_offset_of__icon_4() { return static_cast<int32_t>(offsetof(IconsListItem_tFCE5D92F5E1D082DB67513DB5FE32E4D863A40A7, ____icon_4)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__icon_4() const { return ____icon_4; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__icon_4() { return &____icon_4; }
	inline void set__icon_4(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____icon_4 = value;
		Il2CppCodeGenWriteBarrier((&____icon_4), value);
	}

	inline static int32_t get_offset_of__selectIcon_5() { return static_cast<int32_t>(offsetof(IconsListItem_tFCE5D92F5E1D082DB67513DB5FE32E4D863A40A7, ____selectIcon_5)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__selectIcon_5() const { return ____selectIcon_5; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__selectIcon_5() { return &____selectIcon_5; }
	inline void set__selectIcon_5(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____selectIcon_5 = value;
		Il2CppCodeGenWriteBarrier((&____selectIcon_5), value);
	}

	inline static int32_t get_offset_of__iconLibrary_6() { return static_cast<int32_t>(offsetof(IconsListItem_tFCE5D92F5E1D082DB67513DB5FE32E4D863A40A7, ____iconLibrary_6)); }
	inline RuntimeObject* get__iconLibrary_6() const { return ____iconLibrary_6; }
	inline RuntimeObject** get_address_of__iconLibrary_6() { return &____iconLibrary_6; }
	inline void set__iconLibrary_6(RuntimeObject* value)
	{
		____iconLibrary_6 = value;
		Il2CppCodeGenWriteBarrier((&____iconLibrary_6), value);
	}

	inline static int32_t get_offset_of__userEvents_7() { return static_cast<int32_t>(offsetof(IconsListItem_tFCE5D92F5E1D082DB67513DB5FE32E4D863A40A7, ____userEvents_7)); }
	inline UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * get__userEvents_7() const { return ____userEvents_7; }
	inline UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 ** get_address_of__userEvents_7() { return &____userEvents_7; }
	inline void set__userEvents_7(UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * value)
	{
		____userEvents_7 = value;
		Il2CppCodeGenWriteBarrier((&____userEvents_7), value);
	}

	inline static int32_t get_offset_of__index_8() { return static_cast<int32_t>(offsetof(IconsListItem_tFCE5D92F5E1D082DB67513DB5FE32E4D863A40A7, ____index_8)); }
	inline int32_t get__index_8() const { return ____index_8; }
	inline int32_t* get_address_of__index_8() { return &____index_8; }
	inline void set__index_8(int32_t value)
	{
		____index_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ICONSLISTITEM_TFCE5D92F5E1D082DB67513DB5FE32E4D863A40A7_H
#ifndef JOINTEAMLISTITEM_T44B45CC0825AB01016E454D9EC234EF33B392A6B_H
#define JOINTEAMLISTITEM_T44B45CC0825AB01016E454D9EC234EF33B392A6B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JoinTeamListItem
struct  JoinTeamListItem_t44B45CC0825AB01016E454D9EC234EF33B392A6B  : public TListItem_t3835B3A6EDB64421C763D6938505136E6CD7628C
{
public:
	// TMPro.TextMeshProUGUI JoinTeamListItem::_teamName
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____teamName_4;
	// UnityEngine.UI.Image JoinTeamListItem::_teamIcon
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____teamIcon_5;
	// TMPro.TextMeshProUGUI JoinTeamListItem::_membersCount
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____membersCount_6;
	// UnityEngine.UI.Button JoinTeamListItem::_joinBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____joinBtn_7;
	// TMPro.TextMeshProUGUI JoinTeamListItem::_joinBtnText
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____joinBtnText_8;
	// Tayr.ILibrary JoinTeamListItem::_uiMain
	RuntimeObject* ____uiMain_9;
	// Tayr.ILibrary JoinTeamListItem::_iconLibrary
	RuntimeObject* ____iconLibrary_10;
	// Tayr.GameSparksPlatform JoinTeamListItem::_gameSpark
	GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * ____gameSpark_11;
	// UserEvents JoinTeamListItem::_userEvents
	UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * ____userEvents_12;
	// UserVO JoinTeamListItem::_userVO
	UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * ____userVO_13;
	// GameSparks.Core.GSData JoinTeamListItem::_item
	GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 * ____item_14;

public:
	inline static int32_t get_offset_of__teamName_4() { return static_cast<int32_t>(offsetof(JoinTeamListItem_t44B45CC0825AB01016E454D9EC234EF33B392A6B, ____teamName_4)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__teamName_4() const { return ____teamName_4; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__teamName_4() { return &____teamName_4; }
	inline void set__teamName_4(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____teamName_4 = value;
		Il2CppCodeGenWriteBarrier((&____teamName_4), value);
	}

	inline static int32_t get_offset_of__teamIcon_5() { return static_cast<int32_t>(offsetof(JoinTeamListItem_t44B45CC0825AB01016E454D9EC234EF33B392A6B, ____teamIcon_5)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__teamIcon_5() const { return ____teamIcon_5; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__teamIcon_5() { return &____teamIcon_5; }
	inline void set__teamIcon_5(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____teamIcon_5 = value;
		Il2CppCodeGenWriteBarrier((&____teamIcon_5), value);
	}

	inline static int32_t get_offset_of__membersCount_6() { return static_cast<int32_t>(offsetof(JoinTeamListItem_t44B45CC0825AB01016E454D9EC234EF33B392A6B, ____membersCount_6)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__membersCount_6() const { return ____membersCount_6; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__membersCount_6() { return &____membersCount_6; }
	inline void set__membersCount_6(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____membersCount_6 = value;
		Il2CppCodeGenWriteBarrier((&____membersCount_6), value);
	}

	inline static int32_t get_offset_of__joinBtn_7() { return static_cast<int32_t>(offsetof(JoinTeamListItem_t44B45CC0825AB01016E454D9EC234EF33B392A6B, ____joinBtn_7)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__joinBtn_7() const { return ____joinBtn_7; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__joinBtn_7() { return &____joinBtn_7; }
	inline void set__joinBtn_7(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____joinBtn_7 = value;
		Il2CppCodeGenWriteBarrier((&____joinBtn_7), value);
	}

	inline static int32_t get_offset_of__joinBtnText_8() { return static_cast<int32_t>(offsetof(JoinTeamListItem_t44B45CC0825AB01016E454D9EC234EF33B392A6B, ____joinBtnText_8)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__joinBtnText_8() const { return ____joinBtnText_8; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__joinBtnText_8() { return &____joinBtnText_8; }
	inline void set__joinBtnText_8(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____joinBtnText_8 = value;
		Il2CppCodeGenWriteBarrier((&____joinBtnText_8), value);
	}

	inline static int32_t get_offset_of__uiMain_9() { return static_cast<int32_t>(offsetof(JoinTeamListItem_t44B45CC0825AB01016E454D9EC234EF33B392A6B, ____uiMain_9)); }
	inline RuntimeObject* get__uiMain_9() const { return ____uiMain_9; }
	inline RuntimeObject** get_address_of__uiMain_9() { return &____uiMain_9; }
	inline void set__uiMain_9(RuntimeObject* value)
	{
		____uiMain_9 = value;
		Il2CppCodeGenWriteBarrier((&____uiMain_9), value);
	}

	inline static int32_t get_offset_of__iconLibrary_10() { return static_cast<int32_t>(offsetof(JoinTeamListItem_t44B45CC0825AB01016E454D9EC234EF33B392A6B, ____iconLibrary_10)); }
	inline RuntimeObject* get__iconLibrary_10() const { return ____iconLibrary_10; }
	inline RuntimeObject** get_address_of__iconLibrary_10() { return &____iconLibrary_10; }
	inline void set__iconLibrary_10(RuntimeObject* value)
	{
		____iconLibrary_10 = value;
		Il2CppCodeGenWriteBarrier((&____iconLibrary_10), value);
	}

	inline static int32_t get_offset_of__gameSpark_11() { return static_cast<int32_t>(offsetof(JoinTeamListItem_t44B45CC0825AB01016E454D9EC234EF33B392A6B, ____gameSpark_11)); }
	inline GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * get__gameSpark_11() const { return ____gameSpark_11; }
	inline GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 ** get_address_of__gameSpark_11() { return &____gameSpark_11; }
	inline void set__gameSpark_11(GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * value)
	{
		____gameSpark_11 = value;
		Il2CppCodeGenWriteBarrier((&____gameSpark_11), value);
	}

	inline static int32_t get_offset_of__userEvents_12() { return static_cast<int32_t>(offsetof(JoinTeamListItem_t44B45CC0825AB01016E454D9EC234EF33B392A6B, ____userEvents_12)); }
	inline UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * get__userEvents_12() const { return ____userEvents_12; }
	inline UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 ** get_address_of__userEvents_12() { return &____userEvents_12; }
	inline void set__userEvents_12(UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * value)
	{
		____userEvents_12 = value;
		Il2CppCodeGenWriteBarrier((&____userEvents_12), value);
	}

	inline static int32_t get_offset_of__userVO_13() { return static_cast<int32_t>(offsetof(JoinTeamListItem_t44B45CC0825AB01016E454D9EC234EF33B392A6B, ____userVO_13)); }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * get__userVO_13() const { return ____userVO_13; }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 ** get_address_of__userVO_13() { return &____userVO_13; }
	inline void set__userVO_13(UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * value)
	{
		____userVO_13 = value;
		Il2CppCodeGenWriteBarrier((&____userVO_13), value);
	}

	inline static int32_t get_offset_of__item_14() { return static_cast<int32_t>(offsetof(JoinTeamListItem_t44B45CC0825AB01016E454D9EC234EF33B392A6B, ____item_14)); }
	inline GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 * get__item_14() const { return ____item_14; }
	inline GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 ** get_address_of__item_14() { return &____item_14; }
	inline void set__item_14(GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 * value)
	{
		____item_14 = value;
		Il2CppCodeGenWriteBarrier((&____item_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOINTEAMLISTITEM_T44B45CC0825AB01016E454D9EC234EF33B392A6B_H
#ifndef JOINTEAMNODE_TC9D51921B004C5820A625822BB14B6F90F7D1579_H
#define JOINTEAMNODE_TC9D51921B004C5820A625822BB14B6F90F7D1579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JoinTeamNode
struct  JoinTeamNode_tC9D51921B004C5820A625822BB14B6F90F7D1579  : public BasicNode_1_t80C898A8840F148EAC8A5FE47B08C3ABA79CE64D
{
public:
	// Tayr.TList JoinTeamNode::_teamsList
	TList_tB67CEA10085B72B7C69ACE7FCF01FCC38E6EF292 * ____teamsList_7;
	// Tayr.GameSparksPlatform JoinTeamNode::_gameSpark
	GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * ____gameSpark_8;
	// UserEvents JoinTeamNode::_userEvents
	UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * ____userEvents_9;
	// System.Collections.Generic.List`1<GameSparks.Core.GSData> JoinTeamNode::_teams
	List_1_t0F7C46C2525274B25F1AE138EE936A6F09DAA716 * ____teams_10;

public:
	inline static int32_t get_offset_of__teamsList_7() { return static_cast<int32_t>(offsetof(JoinTeamNode_tC9D51921B004C5820A625822BB14B6F90F7D1579, ____teamsList_7)); }
	inline TList_tB67CEA10085B72B7C69ACE7FCF01FCC38E6EF292 * get__teamsList_7() const { return ____teamsList_7; }
	inline TList_tB67CEA10085B72B7C69ACE7FCF01FCC38E6EF292 ** get_address_of__teamsList_7() { return &____teamsList_7; }
	inline void set__teamsList_7(TList_tB67CEA10085B72B7C69ACE7FCF01FCC38E6EF292 * value)
	{
		____teamsList_7 = value;
		Il2CppCodeGenWriteBarrier((&____teamsList_7), value);
	}

	inline static int32_t get_offset_of__gameSpark_8() { return static_cast<int32_t>(offsetof(JoinTeamNode_tC9D51921B004C5820A625822BB14B6F90F7D1579, ____gameSpark_8)); }
	inline GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * get__gameSpark_8() const { return ____gameSpark_8; }
	inline GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 ** get_address_of__gameSpark_8() { return &____gameSpark_8; }
	inline void set__gameSpark_8(GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * value)
	{
		____gameSpark_8 = value;
		Il2CppCodeGenWriteBarrier((&____gameSpark_8), value);
	}

	inline static int32_t get_offset_of__userEvents_9() { return static_cast<int32_t>(offsetof(JoinTeamNode_tC9D51921B004C5820A625822BB14B6F90F7D1579, ____userEvents_9)); }
	inline UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * get__userEvents_9() const { return ____userEvents_9; }
	inline UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 ** get_address_of__userEvents_9() { return &____userEvents_9; }
	inline void set__userEvents_9(UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * value)
	{
		____userEvents_9 = value;
		Il2CppCodeGenWriteBarrier((&____userEvents_9), value);
	}

	inline static int32_t get_offset_of__teams_10() { return static_cast<int32_t>(offsetof(JoinTeamNode_tC9D51921B004C5820A625822BB14B6F90F7D1579, ____teams_10)); }
	inline List_1_t0F7C46C2525274B25F1AE138EE936A6F09DAA716 * get__teams_10() const { return ____teams_10; }
	inline List_1_t0F7C46C2525274B25F1AE138EE936A6F09DAA716 ** get_address_of__teams_10() { return &____teams_10; }
	inline void set__teams_10(List_1_t0F7C46C2525274B25F1AE138EE936A6F09DAA716 * value)
	{
		____teams_10 = value;
		Il2CppCodeGenWriteBarrier((&____teams_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOINTEAMNODE_TC9D51921B004C5820A625822BB14B6F90F7D1579_H
#ifndef KICKTOOLTIPNODE_T5BA646F823658E7CDA37D282243868064D40F9D2_H
#define KICKTOOLTIPNODE_T5BA646F823658E7CDA37D282243868064D40F9D2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// KickTooltipNode
struct  KickTooltipNode_t5BA646F823658E7CDA37D282243868064D40F9D2  : public BasicNode_1_t5A702E46D2514E5417728C1C36AA87C2918A2070
{
public:
	// UnityEngine.UI.Button KickTooltipNode::_kickBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____kickBtn_7;
	// Tayr.ILibrary KickTooltipNode::_uiMain
	RuntimeObject* ____uiMain_8;

public:
	inline static int32_t get_offset_of__kickBtn_7() { return static_cast<int32_t>(offsetof(KickTooltipNode_t5BA646F823658E7CDA37D282243868064D40F9D2, ____kickBtn_7)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__kickBtn_7() const { return ____kickBtn_7; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__kickBtn_7() { return &____kickBtn_7; }
	inline void set__kickBtn_7(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____kickBtn_7 = value;
		Il2CppCodeGenWriteBarrier((&____kickBtn_7), value);
	}

	inline static int32_t get_offset_of__uiMain_8() { return static_cast<int32_t>(offsetof(KickTooltipNode_t5BA646F823658E7CDA37D282243868064D40F9D2, ____uiMain_8)); }
	inline RuntimeObject* get__uiMain_8() const { return ____uiMain_8; }
	inline RuntimeObject** get_address_of__uiMain_8() { return &____uiMain_8; }
	inline void set__uiMain_8(RuntimeObject* value)
	{
		____uiMain_8 = value;
		Il2CppCodeGenWriteBarrier((&____uiMain_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KICKTOOLTIPNODE_T5BA646F823658E7CDA37D282243868064D40F9D2_H
#ifndef MYTEAMLISTITEM_TB264E6A70F6FB1A5968128CBF86FF0987023FFBC_H
#define MYTEAMLISTITEM_TB264E6A70F6FB1A5968128CBF86FF0987023FFBC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MyTeamListItem
struct  MyTeamListItem_tB264E6A70F6FB1A5968128CBF86FF0987023FFBC  : public TListItem_t3835B3A6EDB64421C763D6938505136E6CD7628C
{
public:
	// UnityEngine.UI.Image MyTeamListItem::_bgHighlight
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____bgHighlight_4;
	// UnityEngine.UI.Image MyTeamListItem::_bgNormal
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____bgNormal_5;
	// TMPro.TextMeshProUGUI MyTeamListItem::_rankText
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____rankText_6;
	// UnityEngine.UI.Image MyTeamListItem::_rank1
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____rank1_7;
	// UnityEngine.UI.Image MyTeamListItem::_rank2
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____rank2_8;
	// UnityEngine.UI.Image MyTeamListItem::_rank3
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____rank3_9;
	// TMPro.TextMeshProUGUI MyTeamListItem::_normaleName
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____normaleName_10;
	// UnityEngine.Transform MyTeamListItem::_leaderParent
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____leaderParent_11;
	// TMPro.TextMeshProUGUI MyTeamListItem::_leaderName
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____leaderName_12;
	// TMPro.TextMeshProUGUI MyTeamListItem::_help
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____help_13;
	// TMPro.TextMeshProUGUI MyTeamListItem::_level
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____level_14;
	// UnityEngine.UI.Button MyTeamListItem::_kick
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____kick_15;
	// UserVO MyTeamListItem::_userVO
	UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * ____userVO_16;
	// Tayr.ILibrary MyTeamListItem::_uiMain
	RuntimeObject* ____uiMain_17;
	// GameSparks.Core.GSData MyTeamListItem::_data
	GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 * ____data_18;

public:
	inline static int32_t get_offset_of__bgHighlight_4() { return static_cast<int32_t>(offsetof(MyTeamListItem_tB264E6A70F6FB1A5968128CBF86FF0987023FFBC, ____bgHighlight_4)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__bgHighlight_4() const { return ____bgHighlight_4; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__bgHighlight_4() { return &____bgHighlight_4; }
	inline void set__bgHighlight_4(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____bgHighlight_4 = value;
		Il2CppCodeGenWriteBarrier((&____bgHighlight_4), value);
	}

	inline static int32_t get_offset_of__bgNormal_5() { return static_cast<int32_t>(offsetof(MyTeamListItem_tB264E6A70F6FB1A5968128CBF86FF0987023FFBC, ____bgNormal_5)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__bgNormal_5() const { return ____bgNormal_5; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__bgNormal_5() { return &____bgNormal_5; }
	inline void set__bgNormal_5(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____bgNormal_5 = value;
		Il2CppCodeGenWriteBarrier((&____bgNormal_5), value);
	}

	inline static int32_t get_offset_of__rankText_6() { return static_cast<int32_t>(offsetof(MyTeamListItem_tB264E6A70F6FB1A5968128CBF86FF0987023FFBC, ____rankText_6)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__rankText_6() const { return ____rankText_6; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__rankText_6() { return &____rankText_6; }
	inline void set__rankText_6(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____rankText_6 = value;
		Il2CppCodeGenWriteBarrier((&____rankText_6), value);
	}

	inline static int32_t get_offset_of__rank1_7() { return static_cast<int32_t>(offsetof(MyTeamListItem_tB264E6A70F6FB1A5968128CBF86FF0987023FFBC, ____rank1_7)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__rank1_7() const { return ____rank1_7; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__rank1_7() { return &____rank1_7; }
	inline void set__rank1_7(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____rank1_7 = value;
		Il2CppCodeGenWriteBarrier((&____rank1_7), value);
	}

	inline static int32_t get_offset_of__rank2_8() { return static_cast<int32_t>(offsetof(MyTeamListItem_tB264E6A70F6FB1A5968128CBF86FF0987023FFBC, ____rank2_8)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__rank2_8() const { return ____rank2_8; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__rank2_8() { return &____rank2_8; }
	inline void set__rank2_8(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____rank2_8 = value;
		Il2CppCodeGenWriteBarrier((&____rank2_8), value);
	}

	inline static int32_t get_offset_of__rank3_9() { return static_cast<int32_t>(offsetof(MyTeamListItem_tB264E6A70F6FB1A5968128CBF86FF0987023FFBC, ____rank3_9)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__rank3_9() const { return ____rank3_9; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__rank3_9() { return &____rank3_9; }
	inline void set__rank3_9(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____rank3_9 = value;
		Il2CppCodeGenWriteBarrier((&____rank3_9), value);
	}

	inline static int32_t get_offset_of__normaleName_10() { return static_cast<int32_t>(offsetof(MyTeamListItem_tB264E6A70F6FB1A5968128CBF86FF0987023FFBC, ____normaleName_10)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__normaleName_10() const { return ____normaleName_10; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__normaleName_10() { return &____normaleName_10; }
	inline void set__normaleName_10(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____normaleName_10 = value;
		Il2CppCodeGenWriteBarrier((&____normaleName_10), value);
	}

	inline static int32_t get_offset_of__leaderParent_11() { return static_cast<int32_t>(offsetof(MyTeamListItem_tB264E6A70F6FB1A5968128CBF86FF0987023FFBC, ____leaderParent_11)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__leaderParent_11() const { return ____leaderParent_11; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__leaderParent_11() { return &____leaderParent_11; }
	inline void set__leaderParent_11(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____leaderParent_11 = value;
		Il2CppCodeGenWriteBarrier((&____leaderParent_11), value);
	}

	inline static int32_t get_offset_of__leaderName_12() { return static_cast<int32_t>(offsetof(MyTeamListItem_tB264E6A70F6FB1A5968128CBF86FF0987023FFBC, ____leaderName_12)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__leaderName_12() const { return ____leaderName_12; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__leaderName_12() { return &____leaderName_12; }
	inline void set__leaderName_12(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____leaderName_12 = value;
		Il2CppCodeGenWriteBarrier((&____leaderName_12), value);
	}

	inline static int32_t get_offset_of__help_13() { return static_cast<int32_t>(offsetof(MyTeamListItem_tB264E6A70F6FB1A5968128CBF86FF0987023FFBC, ____help_13)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__help_13() const { return ____help_13; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__help_13() { return &____help_13; }
	inline void set__help_13(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____help_13 = value;
		Il2CppCodeGenWriteBarrier((&____help_13), value);
	}

	inline static int32_t get_offset_of__level_14() { return static_cast<int32_t>(offsetof(MyTeamListItem_tB264E6A70F6FB1A5968128CBF86FF0987023FFBC, ____level_14)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__level_14() const { return ____level_14; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__level_14() { return &____level_14; }
	inline void set__level_14(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____level_14 = value;
		Il2CppCodeGenWriteBarrier((&____level_14), value);
	}

	inline static int32_t get_offset_of__kick_15() { return static_cast<int32_t>(offsetof(MyTeamListItem_tB264E6A70F6FB1A5968128CBF86FF0987023FFBC, ____kick_15)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__kick_15() const { return ____kick_15; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__kick_15() { return &____kick_15; }
	inline void set__kick_15(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____kick_15 = value;
		Il2CppCodeGenWriteBarrier((&____kick_15), value);
	}

	inline static int32_t get_offset_of__userVO_16() { return static_cast<int32_t>(offsetof(MyTeamListItem_tB264E6A70F6FB1A5968128CBF86FF0987023FFBC, ____userVO_16)); }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * get__userVO_16() const { return ____userVO_16; }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 ** get_address_of__userVO_16() { return &____userVO_16; }
	inline void set__userVO_16(UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * value)
	{
		____userVO_16 = value;
		Il2CppCodeGenWriteBarrier((&____userVO_16), value);
	}

	inline static int32_t get_offset_of__uiMain_17() { return static_cast<int32_t>(offsetof(MyTeamListItem_tB264E6A70F6FB1A5968128CBF86FF0987023FFBC, ____uiMain_17)); }
	inline RuntimeObject* get__uiMain_17() const { return ____uiMain_17; }
	inline RuntimeObject** get_address_of__uiMain_17() { return &____uiMain_17; }
	inline void set__uiMain_17(RuntimeObject* value)
	{
		____uiMain_17 = value;
		Il2CppCodeGenWriteBarrier((&____uiMain_17), value);
	}

	inline static int32_t get_offset_of__data_18() { return static_cast<int32_t>(offsetof(MyTeamListItem_tB264E6A70F6FB1A5968128CBF86FF0987023FFBC, ____data_18)); }
	inline GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 * get__data_18() const { return ____data_18; }
	inline GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 ** get_address_of__data_18() { return &____data_18; }
	inline void set__data_18(GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 * value)
	{
		____data_18 = value;
		Il2CppCodeGenWriteBarrier((&____data_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MYTEAMLISTITEM_TB264E6A70F6FB1A5968128CBF86FF0987023FFBC_H
#ifndef MYTEAMNODE_TA1981263902DFFE7AAB041823582BEFD1C6815E5_H
#define MYTEAMNODE_TA1981263902DFFE7AAB041823582BEFD1C6815E5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MyTeamNode
struct  MyTeamNode_tA1981263902DFFE7AAB041823582BEFD1C6815E5  : public BasicNode_1_t31F5A99F9379D18DE55F22D14196E346D7C1FADB
{
public:
	// TMPro.TextMeshProUGUI MyTeamNode::_name
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____name_7;
	// TMPro.TextMeshProUGUI MyTeamNode::_members
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____members_8;
	// Tayr.TList MyTeamNode::_list
	TList_tB67CEA10085B72B7C69ACE7FCF01FCC38E6EF292 * ____list_9;
	// UnityEngine.UI.Image MyTeamNode::_icon
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____icon_10;
	// UnityEngine.UI.Button MyTeamNode::_infoBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____infoBtn_11;
	// Tayr.ILibrary MyTeamNode::_iconLibrary
	RuntimeObject* ____iconLibrary_12;
	// Tayr.ILibrary MyTeamNode::_uiMain
	RuntimeObject* ____uiMain_13;
	// Tayr.GameSparksPlatform MyTeamNode::_gameSpark
	GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * ____gameSpark_14;
	// UserVO MyTeamNode::_userVO
	UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * ____userVO_15;
	// UserEvents MyTeamNode::_userEvents
	UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * ____userEvents_16;
	// GameSparks.Core.GSData MyTeamNode::_teamDetails
	GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 * ____teamDetails_17;

public:
	inline static int32_t get_offset_of__name_7() { return static_cast<int32_t>(offsetof(MyTeamNode_tA1981263902DFFE7AAB041823582BEFD1C6815E5, ____name_7)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__name_7() const { return ____name_7; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__name_7() { return &____name_7; }
	inline void set__name_7(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____name_7 = value;
		Il2CppCodeGenWriteBarrier((&____name_7), value);
	}

	inline static int32_t get_offset_of__members_8() { return static_cast<int32_t>(offsetof(MyTeamNode_tA1981263902DFFE7AAB041823582BEFD1C6815E5, ____members_8)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__members_8() const { return ____members_8; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__members_8() { return &____members_8; }
	inline void set__members_8(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____members_8 = value;
		Il2CppCodeGenWriteBarrier((&____members_8), value);
	}

	inline static int32_t get_offset_of__list_9() { return static_cast<int32_t>(offsetof(MyTeamNode_tA1981263902DFFE7AAB041823582BEFD1C6815E5, ____list_9)); }
	inline TList_tB67CEA10085B72B7C69ACE7FCF01FCC38E6EF292 * get__list_9() const { return ____list_9; }
	inline TList_tB67CEA10085B72B7C69ACE7FCF01FCC38E6EF292 ** get_address_of__list_9() { return &____list_9; }
	inline void set__list_9(TList_tB67CEA10085B72B7C69ACE7FCF01FCC38E6EF292 * value)
	{
		____list_9 = value;
		Il2CppCodeGenWriteBarrier((&____list_9), value);
	}

	inline static int32_t get_offset_of__icon_10() { return static_cast<int32_t>(offsetof(MyTeamNode_tA1981263902DFFE7AAB041823582BEFD1C6815E5, ____icon_10)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__icon_10() const { return ____icon_10; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__icon_10() { return &____icon_10; }
	inline void set__icon_10(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____icon_10 = value;
		Il2CppCodeGenWriteBarrier((&____icon_10), value);
	}

	inline static int32_t get_offset_of__infoBtn_11() { return static_cast<int32_t>(offsetof(MyTeamNode_tA1981263902DFFE7AAB041823582BEFD1C6815E5, ____infoBtn_11)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__infoBtn_11() const { return ____infoBtn_11; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__infoBtn_11() { return &____infoBtn_11; }
	inline void set__infoBtn_11(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____infoBtn_11 = value;
		Il2CppCodeGenWriteBarrier((&____infoBtn_11), value);
	}

	inline static int32_t get_offset_of__iconLibrary_12() { return static_cast<int32_t>(offsetof(MyTeamNode_tA1981263902DFFE7AAB041823582BEFD1C6815E5, ____iconLibrary_12)); }
	inline RuntimeObject* get__iconLibrary_12() const { return ____iconLibrary_12; }
	inline RuntimeObject** get_address_of__iconLibrary_12() { return &____iconLibrary_12; }
	inline void set__iconLibrary_12(RuntimeObject* value)
	{
		____iconLibrary_12 = value;
		Il2CppCodeGenWriteBarrier((&____iconLibrary_12), value);
	}

	inline static int32_t get_offset_of__uiMain_13() { return static_cast<int32_t>(offsetof(MyTeamNode_tA1981263902DFFE7AAB041823582BEFD1C6815E5, ____uiMain_13)); }
	inline RuntimeObject* get__uiMain_13() const { return ____uiMain_13; }
	inline RuntimeObject** get_address_of__uiMain_13() { return &____uiMain_13; }
	inline void set__uiMain_13(RuntimeObject* value)
	{
		____uiMain_13 = value;
		Il2CppCodeGenWriteBarrier((&____uiMain_13), value);
	}

	inline static int32_t get_offset_of__gameSpark_14() { return static_cast<int32_t>(offsetof(MyTeamNode_tA1981263902DFFE7AAB041823582BEFD1C6815E5, ____gameSpark_14)); }
	inline GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * get__gameSpark_14() const { return ____gameSpark_14; }
	inline GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 ** get_address_of__gameSpark_14() { return &____gameSpark_14; }
	inline void set__gameSpark_14(GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * value)
	{
		____gameSpark_14 = value;
		Il2CppCodeGenWriteBarrier((&____gameSpark_14), value);
	}

	inline static int32_t get_offset_of__userVO_15() { return static_cast<int32_t>(offsetof(MyTeamNode_tA1981263902DFFE7AAB041823582BEFD1C6815E5, ____userVO_15)); }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * get__userVO_15() const { return ____userVO_15; }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 ** get_address_of__userVO_15() { return &____userVO_15; }
	inline void set__userVO_15(UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * value)
	{
		____userVO_15 = value;
		Il2CppCodeGenWriteBarrier((&____userVO_15), value);
	}

	inline static int32_t get_offset_of__userEvents_16() { return static_cast<int32_t>(offsetof(MyTeamNode_tA1981263902DFFE7AAB041823582BEFD1C6815E5, ____userEvents_16)); }
	inline UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * get__userEvents_16() const { return ____userEvents_16; }
	inline UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 ** get_address_of__userEvents_16() { return &____userEvents_16; }
	inline void set__userEvents_16(UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * value)
	{
		____userEvents_16 = value;
		Il2CppCodeGenWriteBarrier((&____userEvents_16), value);
	}

	inline static int32_t get_offset_of__teamDetails_17() { return static_cast<int32_t>(offsetof(MyTeamNode_tA1981263902DFFE7AAB041823582BEFD1C6815E5, ____teamDetails_17)); }
	inline GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 * get__teamDetails_17() const { return ____teamDetails_17; }
	inline GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 ** get_address_of__teamDetails_17() { return &____teamDetails_17; }
	inline void set__teamDetails_17(GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 * value)
	{
		____teamDetails_17 = value;
		Il2CppCodeGenWriteBarrier((&____teamDetails_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MYTEAMNODE_TA1981263902DFFE7AAB041823582BEFD1C6815E5_H
#ifndef REPORTTOOLTIPNODE_T2E2DC6BA9C018B53769C88CD542C8B3418C2B1EC_H
#define REPORTTOOLTIPNODE_T2E2DC6BA9C018B53769C88CD542C8B3418C2B1EC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReportTooltipNode
struct  ReportTooltipNode_t2E2DC6BA9C018B53769C88CD542C8B3418C2B1EC  : public BasicNode_1_t1BE16E2D7C883E375ADF1FDE2D0B141788813C9D
{
public:
	// UnityEngine.UI.Button ReportTooltipNode::_btn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____btn_7;
	// Tayr.ILibrary ReportTooltipNode::_uiMain
	RuntimeObject* ____uiMain_8;

public:
	inline static int32_t get_offset_of__btn_7() { return static_cast<int32_t>(offsetof(ReportTooltipNode_t2E2DC6BA9C018B53769C88CD542C8B3418C2B1EC, ____btn_7)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__btn_7() const { return ____btn_7; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__btn_7() { return &____btn_7; }
	inline void set__btn_7(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____btn_7 = value;
		Il2CppCodeGenWriteBarrier((&____btn_7), value);
	}

	inline static int32_t get_offset_of__uiMain_8() { return static_cast<int32_t>(offsetof(ReportTooltipNode_t2E2DC6BA9C018B53769C88CD542C8B3418C2B1EC, ____uiMain_8)); }
	inline RuntimeObject* get__uiMain_8() const { return ____uiMain_8; }
	inline RuntimeObject** get_address_of__uiMain_8() { return &____uiMain_8; }
	inline void set__uiMain_8(RuntimeObject* value)
	{
		____uiMain_8 = value;
		Il2CppCodeGenWriteBarrier((&____uiMain_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REPORTTOOLTIPNODE_T2E2DC6BA9C018B53769C88CD542C8B3418C2B1EC_H
#ifndef SEARCHTEAMNODE_T97541D5D86D00D231BF9205475733C65BB3AA956_H
#define SEARCHTEAMNODE_T97541D5D86D00D231BF9205475733C65BB3AA956_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SearchTeamNode
struct  SearchTeamNode_t97541D5D86D00D231BF9205475733C65BB3AA956  : public BasicNode_1_t31F5A99F9379D18DE55F22D14196E346D7C1FADB
{
public:
	// TMPro.TMP_InputField SearchTeamNode::_inputField
	TMP_InputField_tC3C57E697A57232E8A855D39600CF06CFDA8F6CB * ____inputField_7;
	// UnityEngine.UI.Button SearchTeamNode::_searchButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____searchButton_8;
	// Tayr.TList SearchTeamNode::_teamsList
	TList_tB67CEA10085B72B7C69ACE7FCF01FCC38E6EF292 * ____teamsList_9;
	// Tayr.GameSparksPlatform SearchTeamNode::_gameSparks
	GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * ____gameSparks_10;
	// UserEvents SearchTeamNode::_userEvents
	UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * ____userEvents_11;

public:
	inline static int32_t get_offset_of__inputField_7() { return static_cast<int32_t>(offsetof(SearchTeamNode_t97541D5D86D00D231BF9205475733C65BB3AA956, ____inputField_7)); }
	inline TMP_InputField_tC3C57E697A57232E8A855D39600CF06CFDA8F6CB * get__inputField_7() const { return ____inputField_7; }
	inline TMP_InputField_tC3C57E697A57232E8A855D39600CF06CFDA8F6CB ** get_address_of__inputField_7() { return &____inputField_7; }
	inline void set__inputField_7(TMP_InputField_tC3C57E697A57232E8A855D39600CF06CFDA8F6CB * value)
	{
		____inputField_7 = value;
		Il2CppCodeGenWriteBarrier((&____inputField_7), value);
	}

	inline static int32_t get_offset_of__searchButton_8() { return static_cast<int32_t>(offsetof(SearchTeamNode_t97541D5D86D00D231BF9205475733C65BB3AA956, ____searchButton_8)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__searchButton_8() const { return ____searchButton_8; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__searchButton_8() { return &____searchButton_8; }
	inline void set__searchButton_8(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____searchButton_8 = value;
		Il2CppCodeGenWriteBarrier((&____searchButton_8), value);
	}

	inline static int32_t get_offset_of__teamsList_9() { return static_cast<int32_t>(offsetof(SearchTeamNode_t97541D5D86D00D231BF9205475733C65BB3AA956, ____teamsList_9)); }
	inline TList_tB67CEA10085B72B7C69ACE7FCF01FCC38E6EF292 * get__teamsList_9() const { return ____teamsList_9; }
	inline TList_tB67CEA10085B72B7C69ACE7FCF01FCC38E6EF292 ** get_address_of__teamsList_9() { return &____teamsList_9; }
	inline void set__teamsList_9(TList_tB67CEA10085B72B7C69ACE7FCF01FCC38E6EF292 * value)
	{
		____teamsList_9 = value;
		Il2CppCodeGenWriteBarrier((&____teamsList_9), value);
	}

	inline static int32_t get_offset_of__gameSparks_10() { return static_cast<int32_t>(offsetof(SearchTeamNode_t97541D5D86D00D231BF9205475733C65BB3AA956, ____gameSparks_10)); }
	inline GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * get__gameSparks_10() const { return ____gameSparks_10; }
	inline GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 ** get_address_of__gameSparks_10() { return &____gameSparks_10; }
	inline void set__gameSparks_10(GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * value)
	{
		____gameSparks_10 = value;
		Il2CppCodeGenWriteBarrier((&____gameSparks_10), value);
	}

	inline static int32_t get_offset_of__userEvents_11() { return static_cast<int32_t>(offsetof(SearchTeamNode_t97541D5D86D00D231BF9205475733C65BB3AA956, ____userEvents_11)); }
	inline UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * get__userEvents_11() const { return ____userEvents_11; }
	inline UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 ** get_address_of__userEvents_11() { return &____userEvents_11; }
	inline void set__userEvents_11(UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * value)
	{
		____userEvents_11 = value;
		Il2CppCodeGenWriteBarrier((&____userEvents_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEARCHTEAMNODE_T97541D5D86D00D231BF9205475733C65BB3AA956_H
#ifndef STARTOURNAMENTLEADERBOARDLISTITEM_TA79391D3CE83B88B641ADF858BE52EBAAB1750B7_H
#define STARTOURNAMENTLEADERBOARDLISTITEM_TA79391D3CE83B88B641ADF858BE52EBAAB1750B7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StarTournamentLeaderboardListItem
struct  StarTournamentLeaderboardListItem_tA79391D3CE83B88B641ADF858BE52EBAAB1750B7  : public TListItem_t3835B3A6EDB64421C763D6938505136E6CD7628C
{
public:
	// UnityEngine.UI.Image StarTournamentLeaderboardListItem::_bgHighlight
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____bgHighlight_4;
	// UnityEngine.UI.Image StarTournamentLeaderboardListItem::_bgNormal
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____bgNormal_5;
	// UnityEngine.UI.Image StarTournamentLeaderboardListItem::_firstRank
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____firstRank_6;
	// UnityEngine.UI.Image StarTournamentLeaderboardListItem::_secondRank
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____secondRank_7;
	// UnityEngine.UI.Image StarTournamentLeaderboardListItem::_thirdRank
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____thirdRank_8;
	// TMPro.TextMeshProUGUI StarTournamentLeaderboardListItem::_rankText
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____rankText_9;
	// UnityEngine.UI.Image StarTournamentLeaderboardListItem::_teamIcon
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____teamIcon_10;
	// TMPro.TextMeshProUGUI StarTournamentLeaderboardListItem::_playerName
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____playerName_11;
	// TMPro.TextMeshProUGUI StarTournamentLeaderboardListItem::_teamName
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____teamName_12;
	// TMPro.TextMeshProUGUI StarTournamentLeaderboardListItem::_starsScore
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____starsScore_13;
	// TMPro.TextMeshProUGUI StarTournamentLeaderboardListItem::_reward
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____reward_14;
	// UnityEngine.UI.Button StarTournamentLeaderboardListItem::_viewTeamBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____viewTeamBtn_15;
	// Tayr.ILibrary StarTournamentLeaderboardListItem::_iconLibrary
	RuntimeObject* ____iconLibrary_16;
	// UserVO StarTournamentLeaderboardListItem::_userVO
	UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * ____userVO_17;
	// StarTournamentListItemData StarTournamentLeaderboardListItem::_data
	StarTournamentListItemData_t6FE18476EB88BBD97CE1527A1C5EE42E3A8936AD * ____data_18;

public:
	inline static int32_t get_offset_of__bgHighlight_4() { return static_cast<int32_t>(offsetof(StarTournamentLeaderboardListItem_tA79391D3CE83B88B641ADF858BE52EBAAB1750B7, ____bgHighlight_4)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__bgHighlight_4() const { return ____bgHighlight_4; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__bgHighlight_4() { return &____bgHighlight_4; }
	inline void set__bgHighlight_4(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____bgHighlight_4 = value;
		Il2CppCodeGenWriteBarrier((&____bgHighlight_4), value);
	}

	inline static int32_t get_offset_of__bgNormal_5() { return static_cast<int32_t>(offsetof(StarTournamentLeaderboardListItem_tA79391D3CE83B88B641ADF858BE52EBAAB1750B7, ____bgNormal_5)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__bgNormal_5() const { return ____bgNormal_5; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__bgNormal_5() { return &____bgNormal_5; }
	inline void set__bgNormal_5(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____bgNormal_5 = value;
		Il2CppCodeGenWriteBarrier((&____bgNormal_5), value);
	}

	inline static int32_t get_offset_of__firstRank_6() { return static_cast<int32_t>(offsetof(StarTournamentLeaderboardListItem_tA79391D3CE83B88B641ADF858BE52EBAAB1750B7, ____firstRank_6)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__firstRank_6() const { return ____firstRank_6; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__firstRank_6() { return &____firstRank_6; }
	inline void set__firstRank_6(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____firstRank_6 = value;
		Il2CppCodeGenWriteBarrier((&____firstRank_6), value);
	}

	inline static int32_t get_offset_of__secondRank_7() { return static_cast<int32_t>(offsetof(StarTournamentLeaderboardListItem_tA79391D3CE83B88B641ADF858BE52EBAAB1750B7, ____secondRank_7)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__secondRank_7() const { return ____secondRank_7; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__secondRank_7() { return &____secondRank_7; }
	inline void set__secondRank_7(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____secondRank_7 = value;
		Il2CppCodeGenWriteBarrier((&____secondRank_7), value);
	}

	inline static int32_t get_offset_of__thirdRank_8() { return static_cast<int32_t>(offsetof(StarTournamentLeaderboardListItem_tA79391D3CE83B88B641ADF858BE52EBAAB1750B7, ____thirdRank_8)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__thirdRank_8() const { return ____thirdRank_8; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__thirdRank_8() { return &____thirdRank_8; }
	inline void set__thirdRank_8(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____thirdRank_8 = value;
		Il2CppCodeGenWriteBarrier((&____thirdRank_8), value);
	}

	inline static int32_t get_offset_of__rankText_9() { return static_cast<int32_t>(offsetof(StarTournamentLeaderboardListItem_tA79391D3CE83B88B641ADF858BE52EBAAB1750B7, ____rankText_9)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__rankText_9() const { return ____rankText_9; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__rankText_9() { return &____rankText_9; }
	inline void set__rankText_9(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____rankText_9 = value;
		Il2CppCodeGenWriteBarrier((&____rankText_9), value);
	}

	inline static int32_t get_offset_of__teamIcon_10() { return static_cast<int32_t>(offsetof(StarTournamentLeaderboardListItem_tA79391D3CE83B88B641ADF858BE52EBAAB1750B7, ____teamIcon_10)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__teamIcon_10() const { return ____teamIcon_10; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__teamIcon_10() { return &____teamIcon_10; }
	inline void set__teamIcon_10(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____teamIcon_10 = value;
		Il2CppCodeGenWriteBarrier((&____teamIcon_10), value);
	}

	inline static int32_t get_offset_of__playerName_11() { return static_cast<int32_t>(offsetof(StarTournamentLeaderboardListItem_tA79391D3CE83B88B641ADF858BE52EBAAB1750B7, ____playerName_11)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__playerName_11() const { return ____playerName_11; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__playerName_11() { return &____playerName_11; }
	inline void set__playerName_11(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____playerName_11 = value;
		Il2CppCodeGenWriteBarrier((&____playerName_11), value);
	}

	inline static int32_t get_offset_of__teamName_12() { return static_cast<int32_t>(offsetof(StarTournamentLeaderboardListItem_tA79391D3CE83B88B641ADF858BE52EBAAB1750B7, ____teamName_12)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__teamName_12() const { return ____teamName_12; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__teamName_12() { return &____teamName_12; }
	inline void set__teamName_12(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____teamName_12 = value;
		Il2CppCodeGenWriteBarrier((&____teamName_12), value);
	}

	inline static int32_t get_offset_of__starsScore_13() { return static_cast<int32_t>(offsetof(StarTournamentLeaderboardListItem_tA79391D3CE83B88B641ADF858BE52EBAAB1750B7, ____starsScore_13)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__starsScore_13() const { return ____starsScore_13; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__starsScore_13() { return &____starsScore_13; }
	inline void set__starsScore_13(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____starsScore_13 = value;
		Il2CppCodeGenWriteBarrier((&____starsScore_13), value);
	}

	inline static int32_t get_offset_of__reward_14() { return static_cast<int32_t>(offsetof(StarTournamentLeaderboardListItem_tA79391D3CE83B88B641ADF858BE52EBAAB1750B7, ____reward_14)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__reward_14() const { return ____reward_14; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__reward_14() { return &____reward_14; }
	inline void set__reward_14(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____reward_14 = value;
		Il2CppCodeGenWriteBarrier((&____reward_14), value);
	}

	inline static int32_t get_offset_of__viewTeamBtn_15() { return static_cast<int32_t>(offsetof(StarTournamentLeaderboardListItem_tA79391D3CE83B88B641ADF858BE52EBAAB1750B7, ____viewTeamBtn_15)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__viewTeamBtn_15() const { return ____viewTeamBtn_15; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__viewTeamBtn_15() { return &____viewTeamBtn_15; }
	inline void set__viewTeamBtn_15(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____viewTeamBtn_15 = value;
		Il2CppCodeGenWriteBarrier((&____viewTeamBtn_15), value);
	}

	inline static int32_t get_offset_of__iconLibrary_16() { return static_cast<int32_t>(offsetof(StarTournamentLeaderboardListItem_tA79391D3CE83B88B641ADF858BE52EBAAB1750B7, ____iconLibrary_16)); }
	inline RuntimeObject* get__iconLibrary_16() const { return ____iconLibrary_16; }
	inline RuntimeObject** get_address_of__iconLibrary_16() { return &____iconLibrary_16; }
	inline void set__iconLibrary_16(RuntimeObject* value)
	{
		____iconLibrary_16 = value;
		Il2CppCodeGenWriteBarrier((&____iconLibrary_16), value);
	}

	inline static int32_t get_offset_of__userVO_17() { return static_cast<int32_t>(offsetof(StarTournamentLeaderboardListItem_tA79391D3CE83B88B641ADF858BE52EBAAB1750B7, ____userVO_17)); }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * get__userVO_17() const { return ____userVO_17; }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 ** get_address_of__userVO_17() { return &____userVO_17; }
	inline void set__userVO_17(UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * value)
	{
		____userVO_17 = value;
		Il2CppCodeGenWriteBarrier((&____userVO_17), value);
	}

	inline static int32_t get_offset_of__data_18() { return static_cast<int32_t>(offsetof(StarTournamentLeaderboardListItem_tA79391D3CE83B88B641ADF858BE52EBAAB1750B7, ____data_18)); }
	inline StarTournamentListItemData_t6FE18476EB88BBD97CE1527A1C5EE42E3A8936AD * get__data_18() const { return ____data_18; }
	inline StarTournamentListItemData_t6FE18476EB88BBD97CE1527A1C5EE42E3A8936AD ** get_address_of__data_18() { return &____data_18; }
	inline void set__data_18(StarTournamentListItemData_t6FE18476EB88BBD97CE1527A1C5EE42E3A8936AD * value)
	{
		____data_18 = value;
		Il2CppCodeGenWriteBarrier((&____data_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STARTOURNAMENTLEADERBOARDLISTITEM_TA79391D3CE83B88B641ADF858BE52EBAAB1750B7_H
#ifndef POPUPNODE_1_T0CE6B02DBF7AA10094AC095DCCDFAB5C11B256D4_H
#define POPUPNODE_1_T0CE6B02DBF7AA10094AC095DCCDFAB5C11B256D4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.PopupNode`1<GameSparks.Core.GSData>
struct  PopupNode_1_t0CE6B02DBF7AA10094AC095DCCDFAB5C11B256D4  : public BasicNode_1_t0CD6D84C6B86DCAC0B15FA6B29832B5A10179CED
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POPUPNODE_1_T0CE6B02DBF7AA10094AC095DCCDFAB5C11B256D4_H
#ifndef POPUPNODE_1_T8943E6F3C0A3C9A8E7B4BD8A0BE5D9CFAAA59EF4_H
#define POPUPNODE_1_T8943E6F3C0A3C9A8E7B4BD8A0BE5D9CFAAA59EF4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.PopupNode`1<System.Int32>
struct  PopupNode_1_t8943E6F3C0A3C9A8E7B4BD8A0BE5D9CFAAA59EF4  : public BasicNode_1_t80C898A8840F148EAC8A5FE47B08C3ABA79CE64D
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POPUPNODE_1_T8943E6F3C0A3C9A8E7B4BD8A0BE5D9CFAAA59EF4_H
#ifndef POPUPNODE_1_TC4D74D0E442AE2B83A0453484F4005FC1E81B3EB_H
#define POPUPNODE_1_TC4D74D0E442AE2B83A0453484F4005FC1E81B3EB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.PopupNode`1<System.String>
struct  PopupNode_1_tC4D74D0E442AE2B83A0453484F4005FC1E81B3EB  : public BasicNode_1_t31F5A99F9379D18DE55F22D14196E346D7C1FADB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POPUPNODE_1_TC4D74D0E442AE2B83A0453484F4005FC1E81B3EB_H
#ifndef TABSNODE_1_T49E3604644CDBB4D36A232C96C46E05ECF9B7E18_H
#define TABSNODE_1_T49E3604644CDBB4D36A232C96C46E05ECF9B7E18_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.TabsNode`1<System.Int32>
struct  TabsNode_1_t49E3604644CDBB4D36A232C96C46E05ECF9B7E18  : public BasicNode_1_t80C898A8840F148EAC8A5FE47B08C3ABA79CE64D
{
public:
	// Tayr.INode Tayr.TabsNode`1::_currentTab
	RuntimeObject* ____currentTab_7;

public:
	inline static int32_t get_offset_of__currentTab_7() { return static_cast<int32_t>(offsetof(TabsNode_1_t49E3604644CDBB4D36A232C96C46E05ECF9B7E18, ____currentTab_7)); }
	inline RuntimeObject* get__currentTab_7() const { return ____currentTab_7; }
	inline RuntimeObject** get_address_of__currentTab_7() { return &____currentTab_7; }
	inline void set__currentTab_7(RuntimeObject* value)
	{
		____currentTab_7 = value;
		Il2CppCodeGenWriteBarrier((&____currentTab_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TABSNODE_1_T49E3604644CDBB4D36A232C96C46E05ECF9B7E18_H
#ifndef TRIBETOURNAMENTCONTRIBUTIONLISTITEM_T9B763EB84E7C72F6EB1DE62F77CE1E1A6CB93E0A_H
#define TRIBETOURNAMENTCONTRIBUTIONLISTITEM_T9B763EB84E7C72F6EB1DE62F77CE1E1A6CB93E0A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TribeTournamentContributionListItem
struct  TribeTournamentContributionListItem_t9B763EB84E7C72F6EB1DE62F77CE1E1A6CB93E0A  : public TListItem_t3835B3A6EDB64421C763D6938505136E6CD7628C
{
public:
	// UnityEngine.UI.Image TribeTournamentContributionListItem::_bgHighlight
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____bgHighlight_4;
	// UnityEngine.UI.Image TribeTournamentContributionListItem::_bgNormal
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____bgNormal_5;
	// UnityEngine.UI.Image TribeTournamentContributionListItem::_firstRank
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____firstRank_6;
	// UnityEngine.UI.Image TribeTournamentContributionListItem::_secondRank
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____secondRank_7;
	// UnityEngine.UI.Image TribeTournamentContributionListItem::_thirdRank
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____thirdRank_8;
	// TMPro.TextMeshProUGUI TribeTournamentContributionListItem::_rankText
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____rankText_9;
	// TMPro.TextMeshProUGUI TribeTournamentContributionListItem::_starsScore
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____starsScore_10;
	// TMPro.TextMeshProUGUI TribeTournamentContributionListItem::_playerName
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____playerName_11;
	// UserVO TribeTournamentContributionListItem::_userVO
	UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * ____userVO_12;
	// TribeTournamentContributionListItemData TribeTournamentContributionListItem::_data
	TribeTournamentContributionListItemData_tF9C29726809FAC898576A1F8CC49BDA2D376924F * ____data_13;

public:
	inline static int32_t get_offset_of__bgHighlight_4() { return static_cast<int32_t>(offsetof(TribeTournamentContributionListItem_t9B763EB84E7C72F6EB1DE62F77CE1E1A6CB93E0A, ____bgHighlight_4)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__bgHighlight_4() const { return ____bgHighlight_4; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__bgHighlight_4() { return &____bgHighlight_4; }
	inline void set__bgHighlight_4(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____bgHighlight_4 = value;
		Il2CppCodeGenWriteBarrier((&____bgHighlight_4), value);
	}

	inline static int32_t get_offset_of__bgNormal_5() { return static_cast<int32_t>(offsetof(TribeTournamentContributionListItem_t9B763EB84E7C72F6EB1DE62F77CE1E1A6CB93E0A, ____bgNormal_5)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__bgNormal_5() const { return ____bgNormal_5; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__bgNormal_5() { return &____bgNormal_5; }
	inline void set__bgNormal_5(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____bgNormal_5 = value;
		Il2CppCodeGenWriteBarrier((&____bgNormal_5), value);
	}

	inline static int32_t get_offset_of__firstRank_6() { return static_cast<int32_t>(offsetof(TribeTournamentContributionListItem_t9B763EB84E7C72F6EB1DE62F77CE1E1A6CB93E0A, ____firstRank_6)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__firstRank_6() const { return ____firstRank_6; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__firstRank_6() { return &____firstRank_6; }
	inline void set__firstRank_6(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____firstRank_6 = value;
		Il2CppCodeGenWriteBarrier((&____firstRank_6), value);
	}

	inline static int32_t get_offset_of__secondRank_7() { return static_cast<int32_t>(offsetof(TribeTournamentContributionListItem_t9B763EB84E7C72F6EB1DE62F77CE1E1A6CB93E0A, ____secondRank_7)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__secondRank_7() const { return ____secondRank_7; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__secondRank_7() { return &____secondRank_7; }
	inline void set__secondRank_7(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____secondRank_7 = value;
		Il2CppCodeGenWriteBarrier((&____secondRank_7), value);
	}

	inline static int32_t get_offset_of__thirdRank_8() { return static_cast<int32_t>(offsetof(TribeTournamentContributionListItem_t9B763EB84E7C72F6EB1DE62F77CE1E1A6CB93E0A, ____thirdRank_8)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__thirdRank_8() const { return ____thirdRank_8; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__thirdRank_8() { return &____thirdRank_8; }
	inline void set__thirdRank_8(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____thirdRank_8 = value;
		Il2CppCodeGenWriteBarrier((&____thirdRank_8), value);
	}

	inline static int32_t get_offset_of__rankText_9() { return static_cast<int32_t>(offsetof(TribeTournamentContributionListItem_t9B763EB84E7C72F6EB1DE62F77CE1E1A6CB93E0A, ____rankText_9)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__rankText_9() const { return ____rankText_9; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__rankText_9() { return &____rankText_9; }
	inline void set__rankText_9(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____rankText_9 = value;
		Il2CppCodeGenWriteBarrier((&____rankText_9), value);
	}

	inline static int32_t get_offset_of__starsScore_10() { return static_cast<int32_t>(offsetof(TribeTournamentContributionListItem_t9B763EB84E7C72F6EB1DE62F77CE1E1A6CB93E0A, ____starsScore_10)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__starsScore_10() const { return ____starsScore_10; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__starsScore_10() { return &____starsScore_10; }
	inline void set__starsScore_10(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____starsScore_10 = value;
		Il2CppCodeGenWriteBarrier((&____starsScore_10), value);
	}

	inline static int32_t get_offset_of__playerName_11() { return static_cast<int32_t>(offsetof(TribeTournamentContributionListItem_t9B763EB84E7C72F6EB1DE62F77CE1E1A6CB93E0A, ____playerName_11)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__playerName_11() const { return ____playerName_11; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__playerName_11() { return &____playerName_11; }
	inline void set__playerName_11(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____playerName_11 = value;
		Il2CppCodeGenWriteBarrier((&____playerName_11), value);
	}

	inline static int32_t get_offset_of__userVO_12() { return static_cast<int32_t>(offsetof(TribeTournamentContributionListItem_t9B763EB84E7C72F6EB1DE62F77CE1E1A6CB93E0A, ____userVO_12)); }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * get__userVO_12() const { return ____userVO_12; }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 ** get_address_of__userVO_12() { return &____userVO_12; }
	inline void set__userVO_12(UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * value)
	{
		____userVO_12 = value;
		Il2CppCodeGenWriteBarrier((&____userVO_12), value);
	}

	inline static int32_t get_offset_of__data_13() { return static_cast<int32_t>(offsetof(TribeTournamentContributionListItem_t9B763EB84E7C72F6EB1DE62F77CE1E1A6CB93E0A, ____data_13)); }
	inline TribeTournamentContributionListItemData_tF9C29726809FAC898576A1F8CC49BDA2D376924F * get__data_13() const { return ____data_13; }
	inline TribeTournamentContributionListItemData_tF9C29726809FAC898576A1F8CC49BDA2D376924F ** get_address_of__data_13() { return &____data_13; }
	inline void set__data_13(TribeTournamentContributionListItemData_tF9C29726809FAC898576A1F8CC49BDA2D376924F * value)
	{
		____data_13 = value;
		Il2CppCodeGenWriteBarrier((&____data_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIBETOURNAMENTCONTRIBUTIONLISTITEM_T9B763EB84E7C72F6EB1DE62F77CE1E1A6CB93E0A_H
#ifndef TRIBETOURNAMENTLEADERBOARDLISTITEM_TD1D05159EFE7AD9AFB45643D5C957ED11F5AC0FF_H
#define TRIBETOURNAMENTLEADERBOARDLISTITEM_TD1D05159EFE7AD9AFB45643D5C957ED11F5AC0FF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TribeTournamentLeaderboardListItem
struct  TribeTournamentLeaderboardListItem_tD1D05159EFE7AD9AFB45643D5C957ED11F5AC0FF  : public TListItem_t3835B3A6EDB64421C763D6938505136E6CD7628C
{
public:
	// UnityEngine.UI.Image TribeTournamentLeaderboardListItem::_bgHighlight
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____bgHighlight_4;
	// UnityEngine.UI.Image TribeTournamentLeaderboardListItem::_bgNormal
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____bgNormal_5;
	// UnityEngine.UI.Image TribeTournamentLeaderboardListItem::_firstRank
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____firstRank_6;
	// UnityEngine.UI.Image TribeTournamentLeaderboardListItem::_secondRank
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____secondRank_7;
	// UnityEngine.UI.Image TribeTournamentLeaderboardListItem::_thirdRank
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____thirdRank_8;
	// TMPro.TextMeshProUGUI TribeTournamentLeaderboardListItem::_rankText
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____rankText_9;
	// UnityEngine.UI.Image TribeTournamentLeaderboardListItem::_teamIcon
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____teamIcon_10;
	// TMPro.TextMeshProUGUI TribeTournamentLeaderboardListItem::_teamName
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____teamName_11;
	// TMPro.TextMeshProUGUI TribeTournamentLeaderboardListItem::_starsScore
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____starsScore_12;
	// TMPro.TextMeshProUGUI TribeTournamentLeaderboardListItem::_reward
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____reward_13;
	// UnityEngine.UI.Button TribeTournamentLeaderboardListItem::_viewTeamBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____viewTeamBtn_14;
	// Tayr.ILibrary TribeTournamentLeaderboardListItem::_iconLibrary
	RuntimeObject* ____iconLibrary_15;
	// UserVO TribeTournamentLeaderboardListItem::_userVO
	UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * ____userVO_16;
	// TribeTournamentListItemData TribeTournamentLeaderboardListItem::_data
	TribeTournamentListItemData_t972BE1E8BC71C51099D379602320348346EBEE14 * ____data_17;

public:
	inline static int32_t get_offset_of__bgHighlight_4() { return static_cast<int32_t>(offsetof(TribeTournamentLeaderboardListItem_tD1D05159EFE7AD9AFB45643D5C957ED11F5AC0FF, ____bgHighlight_4)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__bgHighlight_4() const { return ____bgHighlight_4; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__bgHighlight_4() { return &____bgHighlight_4; }
	inline void set__bgHighlight_4(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____bgHighlight_4 = value;
		Il2CppCodeGenWriteBarrier((&____bgHighlight_4), value);
	}

	inline static int32_t get_offset_of__bgNormal_5() { return static_cast<int32_t>(offsetof(TribeTournamentLeaderboardListItem_tD1D05159EFE7AD9AFB45643D5C957ED11F5AC0FF, ____bgNormal_5)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__bgNormal_5() const { return ____bgNormal_5; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__bgNormal_5() { return &____bgNormal_5; }
	inline void set__bgNormal_5(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____bgNormal_5 = value;
		Il2CppCodeGenWriteBarrier((&____bgNormal_5), value);
	}

	inline static int32_t get_offset_of__firstRank_6() { return static_cast<int32_t>(offsetof(TribeTournamentLeaderboardListItem_tD1D05159EFE7AD9AFB45643D5C957ED11F5AC0FF, ____firstRank_6)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__firstRank_6() const { return ____firstRank_6; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__firstRank_6() { return &____firstRank_6; }
	inline void set__firstRank_6(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____firstRank_6 = value;
		Il2CppCodeGenWriteBarrier((&____firstRank_6), value);
	}

	inline static int32_t get_offset_of__secondRank_7() { return static_cast<int32_t>(offsetof(TribeTournamentLeaderboardListItem_tD1D05159EFE7AD9AFB45643D5C957ED11F5AC0FF, ____secondRank_7)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__secondRank_7() const { return ____secondRank_7; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__secondRank_7() { return &____secondRank_7; }
	inline void set__secondRank_7(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____secondRank_7 = value;
		Il2CppCodeGenWriteBarrier((&____secondRank_7), value);
	}

	inline static int32_t get_offset_of__thirdRank_8() { return static_cast<int32_t>(offsetof(TribeTournamentLeaderboardListItem_tD1D05159EFE7AD9AFB45643D5C957ED11F5AC0FF, ____thirdRank_8)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__thirdRank_8() const { return ____thirdRank_8; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__thirdRank_8() { return &____thirdRank_8; }
	inline void set__thirdRank_8(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____thirdRank_8 = value;
		Il2CppCodeGenWriteBarrier((&____thirdRank_8), value);
	}

	inline static int32_t get_offset_of__rankText_9() { return static_cast<int32_t>(offsetof(TribeTournamentLeaderboardListItem_tD1D05159EFE7AD9AFB45643D5C957ED11F5AC0FF, ____rankText_9)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__rankText_9() const { return ____rankText_9; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__rankText_9() { return &____rankText_9; }
	inline void set__rankText_9(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____rankText_9 = value;
		Il2CppCodeGenWriteBarrier((&____rankText_9), value);
	}

	inline static int32_t get_offset_of__teamIcon_10() { return static_cast<int32_t>(offsetof(TribeTournamentLeaderboardListItem_tD1D05159EFE7AD9AFB45643D5C957ED11F5AC0FF, ____teamIcon_10)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__teamIcon_10() const { return ____teamIcon_10; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__teamIcon_10() { return &____teamIcon_10; }
	inline void set__teamIcon_10(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____teamIcon_10 = value;
		Il2CppCodeGenWriteBarrier((&____teamIcon_10), value);
	}

	inline static int32_t get_offset_of__teamName_11() { return static_cast<int32_t>(offsetof(TribeTournamentLeaderboardListItem_tD1D05159EFE7AD9AFB45643D5C957ED11F5AC0FF, ____teamName_11)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__teamName_11() const { return ____teamName_11; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__teamName_11() { return &____teamName_11; }
	inline void set__teamName_11(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____teamName_11 = value;
		Il2CppCodeGenWriteBarrier((&____teamName_11), value);
	}

	inline static int32_t get_offset_of__starsScore_12() { return static_cast<int32_t>(offsetof(TribeTournamentLeaderboardListItem_tD1D05159EFE7AD9AFB45643D5C957ED11F5AC0FF, ____starsScore_12)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__starsScore_12() const { return ____starsScore_12; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__starsScore_12() { return &____starsScore_12; }
	inline void set__starsScore_12(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____starsScore_12 = value;
		Il2CppCodeGenWriteBarrier((&____starsScore_12), value);
	}

	inline static int32_t get_offset_of__reward_13() { return static_cast<int32_t>(offsetof(TribeTournamentLeaderboardListItem_tD1D05159EFE7AD9AFB45643D5C957ED11F5AC0FF, ____reward_13)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__reward_13() const { return ____reward_13; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__reward_13() { return &____reward_13; }
	inline void set__reward_13(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____reward_13 = value;
		Il2CppCodeGenWriteBarrier((&____reward_13), value);
	}

	inline static int32_t get_offset_of__viewTeamBtn_14() { return static_cast<int32_t>(offsetof(TribeTournamentLeaderboardListItem_tD1D05159EFE7AD9AFB45643D5C957ED11F5AC0FF, ____viewTeamBtn_14)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__viewTeamBtn_14() const { return ____viewTeamBtn_14; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__viewTeamBtn_14() { return &____viewTeamBtn_14; }
	inline void set__viewTeamBtn_14(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____viewTeamBtn_14 = value;
		Il2CppCodeGenWriteBarrier((&____viewTeamBtn_14), value);
	}

	inline static int32_t get_offset_of__iconLibrary_15() { return static_cast<int32_t>(offsetof(TribeTournamentLeaderboardListItem_tD1D05159EFE7AD9AFB45643D5C957ED11F5AC0FF, ____iconLibrary_15)); }
	inline RuntimeObject* get__iconLibrary_15() const { return ____iconLibrary_15; }
	inline RuntimeObject** get_address_of__iconLibrary_15() { return &____iconLibrary_15; }
	inline void set__iconLibrary_15(RuntimeObject* value)
	{
		____iconLibrary_15 = value;
		Il2CppCodeGenWriteBarrier((&____iconLibrary_15), value);
	}

	inline static int32_t get_offset_of__userVO_16() { return static_cast<int32_t>(offsetof(TribeTournamentLeaderboardListItem_tD1D05159EFE7AD9AFB45643D5C957ED11F5AC0FF, ____userVO_16)); }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * get__userVO_16() const { return ____userVO_16; }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 ** get_address_of__userVO_16() { return &____userVO_16; }
	inline void set__userVO_16(UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * value)
	{
		____userVO_16 = value;
		Il2CppCodeGenWriteBarrier((&____userVO_16), value);
	}

	inline static int32_t get_offset_of__data_17() { return static_cast<int32_t>(offsetof(TribeTournamentLeaderboardListItem_tD1D05159EFE7AD9AFB45643D5C957ED11F5AC0FF, ____data_17)); }
	inline TribeTournamentListItemData_t972BE1E8BC71C51099D379602320348346EBEE14 * get__data_17() const { return ____data_17; }
	inline TribeTournamentListItemData_t972BE1E8BC71C51099D379602320348346EBEE14 ** get_address_of__data_17() { return &____data_17; }
	inline void set__data_17(TribeTournamentListItemData_t972BE1E8BC71C51099D379602320348346EBEE14 * value)
	{
		____data_17 = value;
		Il2CppCodeGenWriteBarrier((&____data_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIBETOURNAMENTLEADERBOARDLISTITEM_TD1D05159EFE7AD9AFB45643D5C957ED11F5AC0FF_H
#ifndef BADGESPOPUPNODE_TB7B0E6B874317064FEC0DBDBFF0B63E1CBA25D9A_H
#define BADGESPOPUPNODE_TB7B0E6B874317064FEC0DBDBFF0B63E1CBA25D9A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BadgesPopupNode
struct  BadgesPopupNode_tB7B0E6B874317064FEC0DBDBFF0B63E1CBA25D9A  : public PopupNode_1_t8943E6F3C0A3C9A8E7B4BD8A0BE5D9CFAAA59EF4
{
public:
	// Tayr.TList BadgesPopupNode::_list
	TList_tB67CEA10085B72B7C69ACE7FCF01FCC38E6EF292 * ____list_7;
	// GameSettingsSO BadgesPopupNode::_globalSO
	GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C * ____globalSO_8;

public:
	inline static int32_t get_offset_of__list_7() { return static_cast<int32_t>(offsetof(BadgesPopupNode_tB7B0E6B874317064FEC0DBDBFF0B63E1CBA25D9A, ____list_7)); }
	inline TList_tB67CEA10085B72B7C69ACE7FCF01FCC38E6EF292 * get__list_7() const { return ____list_7; }
	inline TList_tB67CEA10085B72B7C69ACE7FCF01FCC38E6EF292 ** get_address_of__list_7() { return &____list_7; }
	inline void set__list_7(TList_tB67CEA10085B72B7C69ACE7FCF01FCC38E6EF292 * value)
	{
		____list_7 = value;
		Il2CppCodeGenWriteBarrier((&____list_7), value);
	}

	inline static int32_t get_offset_of__globalSO_8() { return static_cast<int32_t>(offsetof(BadgesPopupNode_tB7B0E6B874317064FEC0DBDBFF0B63E1CBA25D9A, ____globalSO_8)); }
	inline GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C * get__globalSO_8() const { return ____globalSO_8; }
	inline GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C ** get_address_of__globalSO_8() { return &____globalSO_8; }
	inline void set__globalSO_8(GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C * value)
	{
		____globalSO_8 = value;
		Il2CppCodeGenWriteBarrier((&____globalSO_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BADGESPOPUPNODE_TB7B0E6B874317064FEC0DBDBFF0B63E1CBA25D9A_H
#ifndef EDITTEAMPOPUPNODE_TCC67D85F6C59BDB715909ED1A0E53D9A660491D5_H
#define EDITTEAMPOPUPNODE_TCC67D85F6C59BDB715909ED1A0E53D9A660491D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EditTeamPopupNode
struct  EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5  : public PopupNode_1_t0CE6B02DBF7AA10094AC095DCCDFAB5C11B256D4
{
public:
	// TMPro.TextMeshProUGUI EditTeamPopupNode::_name
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____name_7;
	// UnityEngine.UI.Image EditTeamPopupNode::_teamIcon
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____teamIcon_8;
	// TMPro.TMP_InputField EditTeamPopupNode::_descriptionField
	TMP_InputField_tC3C57E697A57232E8A855D39600CF06CFDA8F6CB * ____descriptionField_9;
	// UnityEngine.UI.Button EditTeamPopupNode::_typeLeftBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____typeLeftBtn_10;
	// UnityEngine.UI.Button EditTeamPopupNode::_typeRightBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____typeRightBtn_11;
	// TMPro.TextMeshProUGUI EditTeamPopupNode::_type
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____type_12;
	// UnityEngine.UI.Button EditTeamPopupNode::_requiredLeftBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____requiredLeftBtn_13;
	// UnityEngine.UI.Button EditTeamPopupNode::_requiredRightBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____requiredRightBtn_14;
	// TMPro.TextMeshProUGUI EditTeamPopupNode::_required
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____required_15;
	// UnityEngine.UI.Button EditTeamPopupNode::_saveBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____saveBtn_16;
	// Tayr.ILibrary EditTeamPopupNode::_iconLibrary
	RuntimeObject* ____iconLibrary_17;
	// Tayr.ILibrary EditTeamPopupNode::_uiMain
	RuntimeObject* ____uiMain_18;
	// Tayr.GameSparksPlatform EditTeamPopupNode::_gameSpark
	GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * ____gameSpark_19;
	// UserEvents EditTeamPopupNode::_userEvents
	UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * ____userEvents_20;
	// UserVO EditTeamPopupNode::_userVo
	UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * ____userVo_21;
	// KickerManager EditTeamPopupNode::_kickerManager
	KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA * ____kickerManager_22;
	// System.Int32 EditTeamPopupNode::_currentRequirement
	int32_t ____currentRequirement_23;
	// TEAM_TYPE EditTeamPopupNode::_currentType
	int32_t ____currentType_24;

public:
	inline static int32_t get_offset_of__name_7() { return static_cast<int32_t>(offsetof(EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5, ____name_7)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__name_7() const { return ____name_7; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__name_7() { return &____name_7; }
	inline void set__name_7(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____name_7 = value;
		Il2CppCodeGenWriteBarrier((&____name_7), value);
	}

	inline static int32_t get_offset_of__teamIcon_8() { return static_cast<int32_t>(offsetof(EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5, ____teamIcon_8)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__teamIcon_8() const { return ____teamIcon_8; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__teamIcon_8() { return &____teamIcon_8; }
	inline void set__teamIcon_8(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____teamIcon_8 = value;
		Il2CppCodeGenWriteBarrier((&____teamIcon_8), value);
	}

	inline static int32_t get_offset_of__descriptionField_9() { return static_cast<int32_t>(offsetof(EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5, ____descriptionField_9)); }
	inline TMP_InputField_tC3C57E697A57232E8A855D39600CF06CFDA8F6CB * get__descriptionField_9() const { return ____descriptionField_9; }
	inline TMP_InputField_tC3C57E697A57232E8A855D39600CF06CFDA8F6CB ** get_address_of__descriptionField_9() { return &____descriptionField_9; }
	inline void set__descriptionField_9(TMP_InputField_tC3C57E697A57232E8A855D39600CF06CFDA8F6CB * value)
	{
		____descriptionField_9 = value;
		Il2CppCodeGenWriteBarrier((&____descriptionField_9), value);
	}

	inline static int32_t get_offset_of__typeLeftBtn_10() { return static_cast<int32_t>(offsetof(EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5, ____typeLeftBtn_10)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__typeLeftBtn_10() const { return ____typeLeftBtn_10; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__typeLeftBtn_10() { return &____typeLeftBtn_10; }
	inline void set__typeLeftBtn_10(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____typeLeftBtn_10 = value;
		Il2CppCodeGenWriteBarrier((&____typeLeftBtn_10), value);
	}

	inline static int32_t get_offset_of__typeRightBtn_11() { return static_cast<int32_t>(offsetof(EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5, ____typeRightBtn_11)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__typeRightBtn_11() const { return ____typeRightBtn_11; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__typeRightBtn_11() { return &____typeRightBtn_11; }
	inline void set__typeRightBtn_11(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____typeRightBtn_11 = value;
		Il2CppCodeGenWriteBarrier((&____typeRightBtn_11), value);
	}

	inline static int32_t get_offset_of__type_12() { return static_cast<int32_t>(offsetof(EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5, ____type_12)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__type_12() const { return ____type_12; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__type_12() { return &____type_12; }
	inline void set__type_12(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____type_12 = value;
		Il2CppCodeGenWriteBarrier((&____type_12), value);
	}

	inline static int32_t get_offset_of__requiredLeftBtn_13() { return static_cast<int32_t>(offsetof(EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5, ____requiredLeftBtn_13)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__requiredLeftBtn_13() const { return ____requiredLeftBtn_13; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__requiredLeftBtn_13() { return &____requiredLeftBtn_13; }
	inline void set__requiredLeftBtn_13(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____requiredLeftBtn_13 = value;
		Il2CppCodeGenWriteBarrier((&____requiredLeftBtn_13), value);
	}

	inline static int32_t get_offset_of__requiredRightBtn_14() { return static_cast<int32_t>(offsetof(EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5, ____requiredRightBtn_14)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__requiredRightBtn_14() const { return ____requiredRightBtn_14; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__requiredRightBtn_14() { return &____requiredRightBtn_14; }
	inline void set__requiredRightBtn_14(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____requiredRightBtn_14 = value;
		Il2CppCodeGenWriteBarrier((&____requiredRightBtn_14), value);
	}

	inline static int32_t get_offset_of__required_15() { return static_cast<int32_t>(offsetof(EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5, ____required_15)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__required_15() const { return ____required_15; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__required_15() { return &____required_15; }
	inline void set__required_15(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____required_15 = value;
		Il2CppCodeGenWriteBarrier((&____required_15), value);
	}

	inline static int32_t get_offset_of__saveBtn_16() { return static_cast<int32_t>(offsetof(EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5, ____saveBtn_16)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__saveBtn_16() const { return ____saveBtn_16; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__saveBtn_16() { return &____saveBtn_16; }
	inline void set__saveBtn_16(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____saveBtn_16 = value;
		Il2CppCodeGenWriteBarrier((&____saveBtn_16), value);
	}

	inline static int32_t get_offset_of__iconLibrary_17() { return static_cast<int32_t>(offsetof(EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5, ____iconLibrary_17)); }
	inline RuntimeObject* get__iconLibrary_17() const { return ____iconLibrary_17; }
	inline RuntimeObject** get_address_of__iconLibrary_17() { return &____iconLibrary_17; }
	inline void set__iconLibrary_17(RuntimeObject* value)
	{
		____iconLibrary_17 = value;
		Il2CppCodeGenWriteBarrier((&____iconLibrary_17), value);
	}

	inline static int32_t get_offset_of__uiMain_18() { return static_cast<int32_t>(offsetof(EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5, ____uiMain_18)); }
	inline RuntimeObject* get__uiMain_18() const { return ____uiMain_18; }
	inline RuntimeObject** get_address_of__uiMain_18() { return &____uiMain_18; }
	inline void set__uiMain_18(RuntimeObject* value)
	{
		____uiMain_18 = value;
		Il2CppCodeGenWriteBarrier((&____uiMain_18), value);
	}

	inline static int32_t get_offset_of__gameSpark_19() { return static_cast<int32_t>(offsetof(EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5, ____gameSpark_19)); }
	inline GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * get__gameSpark_19() const { return ____gameSpark_19; }
	inline GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 ** get_address_of__gameSpark_19() { return &____gameSpark_19; }
	inline void set__gameSpark_19(GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * value)
	{
		____gameSpark_19 = value;
		Il2CppCodeGenWriteBarrier((&____gameSpark_19), value);
	}

	inline static int32_t get_offset_of__userEvents_20() { return static_cast<int32_t>(offsetof(EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5, ____userEvents_20)); }
	inline UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * get__userEvents_20() const { return ____userEvents_20; }
	inline UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 ** get_address_of__userEvents_20() { return &____userEvents_20; }
	inline void set__userEvents_20(UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * value)
	{
		____userEvents_20 = value;
		Il2CppCodeGenWriteBarrier((&____userEvents_20), value);
	}

	inline static int32_t get_offset_of__userVo_21() { return static_cast<int32_t>(offsetof(EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5, ____userVo_21)); }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * get__userVo_21() const { return ____userVo_21; }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 ** get_address_of__userVo_21() { return &____userVo_21; }
	inline void set__userVo_21(UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * value)
	{
		____userVo_21 = value;
		Il2CppCodeGenWriteBarrier((&____userVo_21), value);
	}

	inline static int32_t get_offset_of__kickerManager_22() { return static_cast<int32_t>(offsetof(EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5, ____kickerManager_22)); }
	inline KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA * get__kickerManager_22() const { return ____kickerManager_22; }
	inline KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA ** get_address_of__kickerManager_22() { return &____kickerManager_22; }
	inline void set__kickerManager_22(KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA * value)
	{
		____kickerManager_22 = value;
		Il2CppCodeGenWriteBarrier((&____kickerManager_22), value);
	}

	inline static int32_t get_offset_of__currentRequirement_23() { return static_cast<int32_t>(offsetof(EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5, ____currentRequirement_23)); }
	inline int32_t get__currentRequirement_23() const { return ____currentRequirement_23; }
	inline int32_t* get_address_of__currentRequirement_23() { return &____currentRequirement_23; }
	inline void set__currentRequirement_23(int32_t value)
	{
		____currentRequirement_23 = value;
	}

	inline static int32_t get_offset_of__currentType_24() { return static_cast<int32_t>(offsetof(EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5, ____currentType_24)); }
	inline int32_t get__currentType_24() const { return ____currentType_24; }
	inline int32_t* get_address_of__currentType_24() { return &____currentType_24; }
	inline void set__currentType_24(int32_t value)
	{
		____currentType_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDITTEAMPOPUPNODE_TCC67D85F6C59BDB715909ED1A0E53D9A660491D5_H
#ifndef KICKPOPUPNODE_T5FBB58C498CBB2F341DF97DF71672143D20BC3F3_H
#define KICKPOPUPNODE_T5FBB58C498CBB2F341DF97DF71672143D20BC3F3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// KickPopupNode
struct  KickPopupNode_t5FBB58C498CBB2F341DF97DF71672143D20BC3F3  : public PopupNode_1_t0CE6B02DBF7AA10094AC095DCCDFAB5C11B256D4
{
public:
	// TMPro.TextMeshProUGUI KickPopupNode::_text
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____text_7;
	// UnityEngine.UI.Button KickPopupNode::_kick
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____kick_8;
	// UnityEngine.UI.Button KickPopupNode::_cancel
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____cancel_9;
	// Tayr.GameSparksPlatform KickPopupNode::_gameSparks
	GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * ____gameSparks_10;
	// UserEvents KickPopupNode::_userEvents
	UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * ____userEvents_11;
	// System.String KickPopupNode::_initialText
	String_t* ____initialText_12;

public:
	inline static int32_t get_offset_of__text_7() { return static_cast<int32_t>(offsetof(KickPopupNode_t5FBB58C498CBB2F341DF97DF71672143D20BC3F3, ____text_7)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__text_7() const { return ____text_7; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__text_7() { return &____text_7; }
	inline void set__text_7(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____text_7 = value;
		Il2CppCodeGenWriteBarrier((&____text_7), value);
	}

	inline static int32_t get_offset_of__kick_8() { return static_cast<int32_t>(offsetof(KickPopupNode_t5FBB58C498CBB2F341DF97DF71672143D20BC3F3, ____kick_8)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__kick_8() const { return ____kick_8; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__kick_8() { return &____kick_8; }
	inline void set__kick_8(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____kick_8 = value;
		Il2CppCodeGenWriteBarrier((&____kick_8), value);
	}

	inline static int32_t get_offset_of__cancel_9() { return static_cast<int32_t>(offsetof(KickPopupNode_t5FBB58C498CBB2F341DF97DF71672143D20BC3F3, ____cancel_9)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__cancel_9() const { return ____cancel_9; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__cancel_9() { return &____cancel_9; }
	inline void set__cancel_9(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____cancel_9 = value;
		Il2CppCodeGenWriteBarrier((&____cancel_9), value);
	}

	inline static int32_t get_offset_of__gameSparks_10() { return static_cast<int32_t>(offsetof(KickPopupNode_t5FBB58C498CBB2F341DF97DF71672143D20BC3F3, ____gameSparks_10)); }
	inline GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * get__gameSparks_10() const { return ____gameSparks_10; }
	inline GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 ** get_address_of__gameSparks_10() { return &____gameSparks_10; }
	inline void set__gameSparks_10(GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * value)
	{
		____gameSparks_10 = value;
		Il2CppCodeGenWriteBarrier((&____gameSparks_10), value);
	}

	inline static int32_t get_offset_of__userEvents_11() { return static_cast<int32_t>(offsetof(KickPopupNode_t5FBB58C498CBB2F341DF97DF71672143D20BC3F3, ____userEvents_11)); }
	inline UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * get__userEvents_11() const { return ____userEvents_11; }
	inline UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 ** get_address_of__userEvents_11() { return &____userEvents_11; }
	inline void set__userEvents_11(UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * value)
	{
		____userEvents_11 = value;
		Il2CppCodeGenWriteBarrier((&____userEvents_11), value);
	}

	inline static int32_t get_offset_of__initialText_12() { return static_cast<int32_t>(offsetof(KickPopupNode_t5FBB58C498CBB2F341DF97DF71672143D20BC3F3, ____initialText_12)); }
	inline String_t* get__initialText_12() const { return ____initialText_12; }
	inline String_t** get_address_of__initialText_12() { return &____initialText_12; }
	inline void set__initialText_12(String_t* value)
	{
		____initialText_12 = value;
		Il2CppCodeGenWriteBarrier((&____initialText_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KICKPOPUPNODE_T5FBB58C498CBB2F341DF97DF71672143D20BC3F3_H
#ifndef LEAVETEAMPOPUPNODE_T9B98647F1040635703171B59BB692ADC5BB4DE21_H
#define LEAVETEAMPOPUPNODE_T9B98647F1040635703171B59BB692ADC5BB4DE21_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LeaveTeamPopupNode
struct  LeaveTeamPopupNode_t9B98647F1040635703171B59BB692ADC5BB4DE21  : public PopupNode_1_tC4D74D0E442AE2B83A0453484F4005FC1E81B3EB
{
public:
	// UnityEngine.UI.Button LeaveTeamPopupNode::_leaveBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____leaveBtn_7;
	// UnityEngine.UI.Button LeaveTeamPopupNode::_cancleBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____cancleBtn_8;
	// Tayr.GameSparksPlatform LeaveTeamPopupNode::_gameSpark
	GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * ____gameSpark_9;
	// UserEvents LeaveTeamPopupNode::_userEvents
	UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * ____userEvents_10;
	// UserVO LeaveTeamPopupNode::_userVO
	UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * ____userVO_11;
	// Tayr.VOSaver LeaveTeamPopupNode::_voSaver
	VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * ____voSaver_12;

public:
	inline static int32_t get_offset_of__leaveBtn_7() { return static_cast<int32_t>(offsetof(LeaveTeamPopupNode_t9B98647F1040635703171B59BB692ADC5BB4DE21, ____leaveBtn_7)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__leaveBtn_7() const { return ____leaveBtn_7; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__leaveBtn_7() { return &____leaveBtn_7; }
	inline void set__leaveBtn_7(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____leaveBtn_7 = value;
		Il2CppCodeGenWriteBarrier((&____leaveBtn_7), value);
	}

	inline static int32_t get_offset_of__cancleBtn_8() { return static_cast<int32_t>(offsetof(LeaveTeamPopupNode_t9B98647F1040635703171B59BB692ADC5BB4DE21, ____cancleBtn_8)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__cancleBtn_8() const { return ____cancleBtn_8; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__cancleBtn_8() { return &____cancleBtn_8; }
	inline void set__cancleBtn_8(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____cancleBtn_8 = value;
		Il2CppCodeGenWriteBarrier((&____cancleBtn_8), value);
	}

	inline static int32_t get_offset_of__gameSpark_9() { return static_cast<int32_t>(offsetof(LeaveTeamPopupNode_t9B98647F1040635703171B59BB692ADC5BB4DE21, ____gameSpark_9)); }
	inline GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * get__gameSpark_9() const { return ____gameSpark_9; }
	inline GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 ** get_address_of__gameSpark_9() { return &____gameSpark_9; }
	inline void set__gameSpark_9(GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * value)
	{
		____gameSpark_9 = value;
		Il2CppCodeGenWriteBarrier((&____gameSpark_9), value);
	}

	inline static int32_t get_offset_of__userEvents_10() { return static_cast<int32_t>(offsetof(LeaveTeamPopupNode_t9B98647F1040635703171B59BB692ADC5BB4DE21, ____userEvents_10)); }
	inline UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * get__userEvents_10() const { return ____userEvents_10; }
	inline UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 ** get_address_of__userEvents_10() { return &____userEvents_10; }
	inline void set__userEvents_10(UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * value)
	{
		____userEvents_10 = value;
		Il2CppCodeGenWriteBarrier((&____userEvents_10), value);
	}

	inline static int32_t get_offset_of__userVO_11() { return static_cast<int32_t>(offsetof(LeaveTeamPopupNode_t9B98647F1040635703171B59BB692ADC5BB4DE21, ____userVO_11)); }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * get__userVO_11() const { return ____userVO_11; }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 ** get_address_of__userVO_11() { return &____userVO_11; }
	inline void set__userVO_11(UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * value)
	{
		____userVO_11 = value;
		Il2CppCodeGenWriteBarrier((&____userVO_11), value);
	}

	inline static int32_t get_offset_of__voSaver_12() { return static_cast<int32_t>(offsetof(LeaveTeamPopupNode_t9B98647F1040635703171B59BB692ADC5BB4DE21, ____voSaver_12)); }
	inline VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * get__voSaver_12() const { return ____voSaver_12; }
	inline VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F ** get_address_of__voSaver_12() { return &____voSaver_12; }
	inline void set__voSaver_12(VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * value)
	{
		____voSaver_12 = value;
		Il2CppCodeGenWriteBarrier((&____voSaver_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEAVETEAMPOPUPNODE_T9B98647F1040635703171B59BB692ADC5BB4DE21_H
#ifndef MYTEAMINFONODE_TF5C2189CDF910E7227D0E34ADE7BBB8024740DD0_H
#define MYTEAMINFONODE_TF5C2189CDF910E7227D0E34ADE7BBB8024740DD0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MyTeamInfoNode
struct  MyTeamInfoNode_tF5C2189CDF910E7227D0E34ADE7BBB8024740DD0  : public PopupNode_1_t0CE6B02DBF7AA10094AC095DCCDFAB5C11B256D4
{
public:
	// UnityEngine.UI.Image MyTeamInfoNode::_icon
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____icon_7;
	// TMPro.TextMeshProUGUI MyTeamInfoNode::_name
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____name_8;
	// TMPro.TextMeshProUGUI MyTeamInfoNode::_description
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____description_9;
	// TMPro.TextMeshProUGUI MyTeamInfoNode::_members
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____members_10;
	// UnityEngine.UI.Button MyTeamInfoNode::_leaveBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____leaveBtn_11;
	// UnityEngine.UI.Button MyTeamInfoNode::_editBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____editBtn_12;
	// TMPro.TextMeshProUGUI MyTeamInfoNode::_teamScore
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____teamScore_13;
	// TMPro.TextMeshProUGUI MyTeamInfoNode::_weeklyHelp
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____weeklyHelp_14;
	// TMPro.TextMeshProUGUI MyTeamInfoNode::_requiredLevel
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____requiredLevel_15;
	// TMPro.TextMeshProUGUI MyTeamInfoNode::_teamType
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____teamType_16;
	// Tayr.ILibrary MyTeamInfoNode::_iconLibrary
	RuntimeObject* ____iconLibrary_17;
	// Tayr.ILibrary MyTeamInfoNode::_uiMain
	RuntimeObject* ____uiMain_18;
	// UserEvents MyTeamInfoNode::_userEvents
	UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * ____userEvents_19;
	// UserVO MyTeamInfoNode::_userVo
	UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * ____userVo_20;

public:
	inline static int32_t get_offset_of__icon_7() { return static_cast<int32_t>(offsetof(MyTeamInfoNode_tF5C2189CDF910E7227D0E34ADE7BBB8024740DD0, ____icon_7)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__icon_7() const { return ____icon_7; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__icon_7() { return &____icon_7; }
	inline void set__icon_7(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____icon_7 = value;
		Il2CppCodeGenWriteBarrier((&____icon_7), value);
	}

	inline static int32_t get_offset_of__name_8() { return static_cast<int32_t>(offsetof(MyTeamInfoNode_tF5C2189CDF910E7227D0E34ADE7BBB8024740DD0, ____name_8)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__name_8() const { return ____name_8; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__name_8() { return &____name_8; }
	inline void set__name_8(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____name_8 = value;
		Il2CppCodeGenWriteBarrier((&____name_8), value);
	}

	inline static int32_t get_offset_of__description_9() { return static_cast<int32_t>(offsetof(MyTeamInfoNode_tF5C2189CDF910E7227D0E34ADE7BBB8024740DD0, ____description_9)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__description_9() const { return ____description_9; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__description_9() { return &____description_9; }
	inline void set__description_9(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____description_9 = value;
		Il2CppCodeGenWriteBarrier((&____description_9), value);
	}

	inline static int32_t get_offset_of__members_10() { return static_cast<int32_t>(offsetof(MyTeamInfoNode_tF5C2189CDF910E7227D0E34ADE7BBB8024740DD0, ____members_10)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__members_10() const { return ____members_10; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__members_10() { return &____members_10; }
	inline void set__members_10(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____members_10 = value;
		Il2CppCodeGenWriteBarrier((&____members_10), value);
	}

	inline static int32_t get_offset_of__leaveBtn_11() { return static_cast<int32_t>(offsetof(MyTeamInfoNode_tF5C2189CDF910E7227D0E34ADE7BBB8024740DD0, ____leaveBtn_11)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__leaveBtn_11() const { return ____leaveBtn_11; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__leaveBtn_11() { return &____leaveBtn_11; }
	inline void set__leaveBtn_11(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____leaveBtn_11 = value;
		Il2CppCodeGenWriteBarrier((&____leaveBtn_11), value);
	}

	inline static int32_t get_offset_of__editBtn_12() { return static_cast<int32_t>(offsetof(MyTeamInfoNode_tF5C2189CDF910E7227D0E34ADE7BBB8024740DD0, ____editBtn_12)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__editBtn_12() const { return ____editBtn_12; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__editBtn_12() { return &____editBtn_12; }
	inline void set__editBtn_12(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____editBtn_12 = value;
		Il2CppCodeGenWriteBarrier((&____editBtn_12), value);
	}

	inline static int32_t get_offset_of__teamScore_13() { return static_cast<int32_t>(offsetof(MyTeamInfoNode_tF5C2189CDF910E7227D0E34ADE7BBB8024740DD0, ____teamScore_13)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__teamScore_13() const { return ____teamScore_13; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__teamScore_13() { return &____teamScore_13; }
	inline void set__teamScore_13(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____teamScore_13 = value;
		Il2CppCodeGenWriteBarrier((&____teamScore_13), value);
	}

	inline static int32_t get_offset_of__weeklyHelp_14() { return static_cast<int32_t>(offsetof(MyTeamInfoNode_tF5C2189CDF910E7227D0E34ADE7BBB8024740DD0, ____weeklyHelp_14)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__weeklyHelp_14() const { return ____weeklyHelp_14; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__weeklyHelp_14() { return &____weeklyHelp_14; }
	inline void set__weeklyHelp_14(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____weeklyHelp_14 = value;
		Il2CppCodeGenWriteBarrier((&____weeklyHelp_14), value);
	}

	inline static int32_t get_offset_of__requiredLevel_15() { return static_cast<int32_t>(offsetof(MyTeamInfoNode_tF5C2189CDF910E7227D0E34ADE7BBB8024740DD0, ____requiredLevel_15)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__requiredLevel_15() const { return ____requiredLevel_15; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__requiredLevel_15() { return &____requiredLevel_15; }
	inline void set__requiredLevel_15(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____requiredLevel_15 = value;
		Il2CppCodeGenWriteBarrier((&____requiredLevel_15), value);
	}

	inline static int32_t get_offset_of__teamType_16() { return static_cast<int32_t>(offsetof(MyTeamInfoNode_tF5C2189CDF910E7227D0E34ADE7BBB8024740DD0, ____teamType_16)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__teamType_16() const { return ____teamType_16; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__teamType_16() { return &____teamType_16; }
	inline void set__teamType_16(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____teamType_16 = value;
		Il2CppCodeGenWriteBarrier((&____teamType_16), value);
	}

	inline static int32_t get_offset_of__iconLibrary_17() { return static_cast<int32_t>(offsetof(MyTeamInfoNode_tF5C2189CDF910E7227D0E34ADE7BBB8024740DD0, ____iconLibrary_17)); }
	inline RuntimeObject* get__iconLibrary_17() const { return ____iconLibrary_17; }
	inline RuntimeObject** get_address_of__iconLibrary_17() { return &____iconLibrary_17; }
	inline void set__iconLibrary_17(RuntimeObject* value)
	{
		____iconLibrary_17 = value;
		Il2CppCodeGenWriteBarrier((&____iconLibrary_17), value);
	}

	inline static int32_t get_offset_of__uiMain_18() { return static_cast<int32_t>(offsetof(MyTeamInfoNode_tF5C2189CDF910E7227D0E34ADE7BBB8024740DD0, ____uiMain_18)); }
	inline RuntimeObject* get__uiMain_18() const { return ____uiMain_18; }
	inline RuntimeObject** get_address_of__uiMain_18() { return &____uiMain_18; }
	inline void set__uiMain_18(RuntimeObject* value)
	{
		____uiMain_18 = value;
		Il2CppCodeGenWriteBarrier((&____uiMain_18), value);
	}

	inline static int32_t get_offset_of__userEvents_19() { return static_cast<int32_t>(offsetof(MyTeamInfoNode_tF5C2189CDF910E7227D0E34ADE7BBB8024740DD0, ____userEvents_19)); }
	inline UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * get__userEvents_19() const { return ____userEvents_19; }
	inline UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 ** get_address_of__userEvents_19() { return &____userEvents_19; }
	inline void set__userEvents_19(UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * value)
	{
		____userEvents_19 = value;
		Il2CppCodeGenWriteBarrier((&____userEvents_19), value);
	}

	inline static int32_t get_offset_of__userVo_20() { return static_cast<int32_t>(offsetof(MyTeamInfoNode_tF5C2189CDF910E7227D0E34ADE7BBB8024740DD0, ____userVo_20)); }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * get__userVo_20() const { return ____userVo_20; }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 ** get_address_of__userVo_20() { return &____userVo_20; }
	inline void set__userVo_20(UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * value)
	{
		____userVo_20 = value;
		Il2CppCodeGenWriteBarrier((&____userVo_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MYTEAMINFONODE_TF5C2189CDF910E7227D0E34ADE7BBB8024740DD0_H
#ifndef REPORTCHATPOPUPNODE_T794F02E6ED048EDC040CFE0794ABD0F7CDAA5BD9_H
#define REPORTCHATPOPUPNODE_T794F02E6ED048EDC040CFE0794ABD0F7CDAA5BD9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReportChatPopupNode
struct  ReportChatPopupNode_t794F02E6ED048EDC040CFE0794ABD0F7CDAA5BD9  : public PopupNode_1_tC4D74D0E442AE2B83A0453484F4005FC1E81B3EB
{
public:
	// UnityEngine.UI.Button ReportChatPopupNode::_sendBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____sendBtn_7;
	// TMPro.TMP_InputField ReportChatPopupNode::_emailField
	TMP_InputField_tC3C57E697A57232E8A855D39600CF06CFDA8F6CB * ____emailField_8;
	// TMPro.TMP_InputField ReportChatPopupNode::_descriptionField
	TMP_InputField_tC3C57E697A57232E8A855D39600CF06CFDA8F6CB * ____descriptionField_9;
	// KickerManager ReportChatPopupNode::_kicker
	KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA * ____kicker_10;
	// Tayr.GameSparksPlatform ReportChatPopupNode::_gameSparks
	GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * ____gameSparks_11;

public:
	inline static int32_t get_offset_of__sendBtn_7() { return static_cast<int32_t>(offsetof(ReportChatPopupNode_t794F02E6ED048EDC040CFE0794ABD0F7CDAA5BD9, ____sendBtn_7)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__sendBtn_7() const { return ____sendBtn_7; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__sendBtn_7() { return &____sendBtn_7; }
	inline void set__sendBtn_7(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____sendBtn_7 = value;
		Il2CppCodeGenWriteBarrier((&____sendBtn_7), value);
	}

	inline static int32_t get_offset_of__emailField_8() { return static_cast<int32_t>(offsetof(ReportChatPopupNode_t794F02E6ED048EDC040CFE0794ABD0F7CDAA5BD9, ____emailField_8)); }
	inline TMP_InputField_tC3C57E697A57232E8A855D39600CF06CFDA8F6CB * get__emailField_8() const { return ____emailField_8; }
	inline TMP_InputField_tC3C57E697A57232E8A855D39600CF06CFDA8F6CB ** get_address_of__emailField_8() { return &____emailField_8; }
	inline void set__emailField_8(TMP_InputField_tC3C57E697A57232E8A855D39600CF06CFDA8F6CB * value)
	{
		____emailField_8 = value;
		Il2CppCodeGenWriteBarrier((&____emailField_8), value);
	}

	inline static int32_t get_offset_of__descriptionField_9() { return static_cast<int32_t>(offsetof(ReportChatPopupNode_t794F02E6ED048EDC040CFE0794ABD0F7CDAA5BD9, ____descriptionField_9)); }
	inline TMP_InputField_tC3C57E697A57232E8A855D39600CF06CFDA8F6CB * get__descriptionField_9() const { return ____descriptionField_9; }
	inline TMP_InputField_tC3C57E697A57232E8A855D39600CF06CFDA8F6CB ** get_address_of__descriptionField_9() { return &____descriptionField_9; }
	inline void set__descriptionField_9(TMP_InputField_tC3C57E697A57232E8A855D39600CF06CFDA8F6CB * value)
	{
		____descriptionField_9 = value;
		Il2CppCodeGenWriteBarrier((&____descriptionField_9), value);
	}

	inline static int32_t get_offset_of__kicker_10() { return static_cast<int32_t>(offsetof(ReportChatPopupNode_t794F02E6ED048EDC040CFE0794ABD0F7CDAA5BD9, ____kicker_10)); }
	inline KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA * get__kicker_10() const { return ____kicker_10; }
	inline KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA ** get_address_of__kicker_10() { return &____kicker_10; }
	inline void set__kicker_10(KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA * value)
	{
		____kicker_10 = value;
		Il2CppCodeGenWriteBarrier((&____kicker_10), value);
	}

	inline static int32_t get_offset_of__gameSparks_11() { return static_cast<int32_t>(offsetof(ReportChatPopupNode_t794F02E6ED048EDC040CFE0794ABD0F7CDAA5BD9, ____gameSparks_11)); }
	inline GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * get__gameSparks_11() const { return ____gameSparks_11; }
	inline GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 ** get_address_of__gameSparks_11() { return &____gameSparks_11; }
	inline void set__gameSparks_11(GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * value)
	{
		____gameSparks_11 = value;
		Il2CppCodeGenWriteBarrier((&____gameSparks_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REPORTCHATPOPUPNODE_T794F02E6ED048EDC040CFE0794ABD0F7CDAA5BD9_H
#ifndef REPORTCONFIRMPOPUP_T2AF52E64BA94BE60A5C008C27E88AB8962BDF8BE_H
#define REPORTCONFIRMPOPUP_T2AF52E64BA94BE60A5C008C27E88AB8962BDF8BE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReportConfirmPopup
struct  ReportConfirmPopup_t2AF52E64BA94BE60A5C008C27E88AB8962BDF8BE  : public PopupNode_1_tC4D74D0E442AE2B83A0453484F4005FC1E81B3EB
{
public:
	// UnityEngine.UI.Button ReportConfirmPopup::_yesBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____yesBtn_7;
	// UnityEngine.UI.Button ReportConfirmPopup::_noBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____noBtn_8;
	// Tayr.ILibrary ReportConfirmPopup::_uiMain
	RuntimeObject* ____uiMain_9;

public:
	inline static int32_t get_offset_of__yesBtn_7() { return static_cast<int32_t>(offsetof(ReportConfirmPopup_t2AF52E64BA94BE60A5C008C27E88AB8962BDF8BE, ____yesBtn_7)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__yesBtn_7() const { return ____yesBtn_7; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__yesBtn_7() { return &____yesBtn_7; }
	inline void set__yesBtn_7(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____yesBtn_7 = value;
		Il2CppCodeGenWriteBarrier((&____yesBtn_7), value);
	}

	inline static int32_t get_offset_of__noBtn_8() { return static_cast<int32_t>(offsetof(ReportConfirmPopup_t2AF52E64BA94BE60A5C008C27E88AB8962BDF8BE, ____noBtn_8)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__noBtn_8() const { return ____noBtn_8; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__noBtn_8() { return &____noBtn_8; }
	inline void set__noBtn_8(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____noBtn_8 = value;
		Il2CppCodeGenWriteBarrier((&____noBtn_8), value);
	}

	inline static int32_t get_offset_of__uiMain_9() { return static_cast<int32_t>(offsetof(ReportConfirmPopup_t2AF52E64BA94BE60A5C008C27E88AB8962BDF8BE, ____uiMain_9)); }
	inline RuntimeObject* get__uiMain_9() const { return ____uiMain_9; }
	inline RuntimeObject** get_address_of__uiMain_9() { return &____uiMain_9; }
	inline void set__uiMain_9(RuntimeObject* value)
	{
		____uiMain_9 = value;
		Il2CppCodeGenWriteBarrier((&____uiMain_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REPORTCONFIRMPOPUP_T2AF52E64BA94BE60A5C008C27E88AB8962BDF8BE_H
#ifndef STARTOURNAMENTENDEDPOPUPNODE_T08EC46F53416DF39D760B38B64472076A9670516_H
#define STARTOURNAMENTENDEDPOPUPNODE_T08EC46F53416DF39D760B38B64472076A9670516_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StarTournamentEndedPopupNode
struct  StarTournamentEndedPopupNode_t08EC46F53416DF39D760B38B64472076A9670516  : public PopupNode_1_tC4D74D0E442AE2B83A0453484F4005FC1E81B3EB
{
public:
	// Tayr.TList StarTournamentEndedPopupNode::_list
	TList_tB67CEA10085B72B7C69ACE7FCF01FCC38E6EF292 * ____list_7;
	// UnityEngine.UI.Button StarTournamentEndedPopupNode::_claimBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____claimBtn_8;
	// UnityEngine.UI.Button StarTournamentEndedPopupNode::_closeBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____closeBtn_9;
	// TMPro.TextMeshProUGUI StarTournamentEndedPopupNode::_rankText
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____rankText_10;
	// Tayr.GameSparksPlatform StarTournamentEndedPopupNode::_gameSpark
	GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * ____gameSpark_11;
	// UserVO StarTournamentEndedPopupNode::_userVO
	UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * ____userVO_12;
	// KickerManager StarTournamentEndedPopupNode::_kickerManager
	KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA * ____kickerManager_13;

public:
	inline static int32_t get_offset_of__list_7() { return static_cast<int32_t>(offsetof(StarTournamentEndedPopupNode_t08EC46F53416DF39D760B38B64472076A9670516, ____list_7)); }
	inline TList_tB67CEA10085B72B7C69ACE7FCF01FCC38E6EF292 * get__list_7() const { return ____list_7; }
	inline TList_tB67CEA10085B72B7C69ACE7FCF01FCC38E6EF292 ** get_address_of__list_7() { return &____list_7; }
	inline void set__list_7(TList_tB67CEA10085B72B7C69ACE7FCF01FCC38E6EF292 * value)
	{
		____list_7 = value;
		Il2CppCodeGenWriteBarrier((&____list_7), value);
	}

	inline static int32_t get_offset_of__claimBtn_8() { return static_cast<int32_t>(offsetof(StarTournamentEndedPopupNode_t08EC46F53416DF39D760B38B64472076A9670516, ____claimBtn_8)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__claimBtn_8() const { return ____claimBtn_8; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__claimBtn_8() { return &____claimBtn_8; }
	inline void set__claimBtn_8(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____claimBtn_8 = value;
		Il2CppCodeGenWriteBarrier((&____claimBtn_8), value);
	}

	inline static int32_t get_offset_of__closeBtn_9() { return static_cast<int32_t>(offsetof(StarTournamentEndedPopupNode_t08EC46F53416DF39D760B38B64472076A9670516, ____closeBtn_9)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__closeBtn_9() const { return ____closeBtn_9; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__closeBtn_9() { return &____closeBtn_9; }
	inline void set__closeBtn_9(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____closeBtn_9 = value;
		Il2CppCodeGenWriteBarrier((&____closeBtn_9), value);
	}

	inline static int32_t get_offset_of__rankText_10() { return static_cast<int32_t>(offsetof(StarTournamentEndedPopupNode_t08EC46F53416DF39D760B38B64472076A9670516, ____rankText_10)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__rankText_10() const { return ____rankText_10; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__rankText_10() { return &____rankText_10; }
	inline void set__rankText_10(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____rankText_10 = value;
		Il2CppCodeGenWriteBarrier((&____rankText_10), value);
	}

	inline static int32_t get_offset_of__gameSpark_11() { return static_cast<int32_t>(offsetof(StarTournamentEndedPopupNode_t08EC46F53416DF39D760B38B64472076A9670516, ____gameSpark_11)); }
	inline GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * get__gameSpark_11() const { return ____gameSpark_11; }
	inline GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 ** get_address_of__gameSpark_11() { return &____gameSpark_11; }
	inline void set__gameSpark_11(GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * value)
	{
		____gameSpark_11 = value;
		Il2CppCodeGenWriteBarrier((&____gameSpark_11), value);
	}

	inline static int32_t get_offset_of__userVO_12() { return static_cast<int32_t>(offsetof(StarTournamentEndedPopupNode_t08EC46F53416DF39D760B38B64472076A9670516, ____userVO_12)); }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * get__userVO_12() const { return ____userVO_12; }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 ** get_address_of__userVO_12() { return &____userVO_12; }
	inline void set__userVO_12(UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * value)
	{
		____userVO_12 = value;
		Il2CppCodeGenWriteBarrier((&____userVO_12), value);
	}

	inline static int32_t get_offset_of__kickerManager_13() { return static_cast<int32_t>(offsetof(StarTournamentEndedPopupNode_t08EC46F53416DF39D760B38B64472076A9670516, ____kickerManager_13)); }
	inline KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA * get__kickerManager_13() const { return ____kickerManager_13; }
	inline KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA ** get_address_of__kickerManager_13() { return &____kickerManager_13; }
	inline void set__kickerManager_13(KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA * value)
	{
		____kickerManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____kickerManager_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STARTOURNAMENTENDEDPOPUPNODE_T08EC46F53416DF39D760B38B64472076A9670516_H
#ifndef STARTOURNAMENTJOINEDPOPUPNODE_T4BCAE122FEC6FDE5C033DCBBA2F83498444A39C8_H
#define STARTOURNAMENTJOINEDPOPUPNODE_T4BCAE122FEC6FDE5C033DCBBA2F83498444A39C8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StarTournamentJoinedPopupNode
struct  StarTournamentJoinedPopupNode_t4BCAE122FEC6FDE5C033DCBBA2F83498444A39C8  : public PopupNode_1_tC4D74D0E442AE2B83A0453484F4005FC1E81B3EB
{
public:
	// Tayr.TList StarTournamentJoinedPopupNode::_list
	TList_tB67CEA10085B72B7C69ACE7FCF01FCC38E6EF292 * ____list_7;
	// UnityEngine.UI.Button StarTournamentJoinedPopupNode::_closeBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____closeBtn_8;
	// Tayr.GameSparksPlatform StarTournamentJoinedPopupNode::_gameSpark
	GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * ____gameSpark_9;
	// System.Single StarTournamentJoinedPopupNode::_lastTimeRefresh
	float ____lastTimeRefresh_10;
	// System.Collections.Generic.List`1<StarTournamentListItemData> StarTournamentJoinedPopupNode::_items
	List_1_t386DA0CB20C9114E16EF88E20B87F7B391EAA070 * ____items_11;

public:
	inline static int32_t get_offset_of__list_7() { return static_cast<int32_t>(offsetof(StarTournamentJoinedPopupNode_t4BCAE122FEC6FDE5C033DCBBA2F83498444A39C8, ____list_7)); }
	inline TList_tB67CEA10085B72B7C69ACE7FCF01FCC38E6EF292 * get__list_7() const { return ____list_7; }
	inline TList_tB67CEA10085B72B7C69ACE7FCF01FCC38E6EF292 ** get_address_of__list_7() { return &____list_7; }
	inline void set__list_7(TList_tB67CEA10085B72B7C69ACE7FCF01FCC38E6EF292 * value)
	{
		____list_7 = value;
		Il2CppCodeGenWriteBarrier((&____list_7), value);
	}

	inline static int32_t get_offset_of__closeBtn_8() { return static_cast<int32_t>(offsetof(StarTournamentJoinedPopupNode_t4BCAE122FEC6FDE5C033DCBBA2F83498444A39C8, ____closeBtn_8)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__closeBtn_8() const { return ____closeBtn_8; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__closeBtn_8() { return &____closeBtn_8; }
	inline void set__closeBtn_8(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____closeBtn_8 = value;
		Il2CppCodeGenWriteBarrier((&____closeBtn_8), value);
	}

	inline static int32_t get_offset_of__gameSpark_9() { return static_cast<int32_t>(offsetof(StarTournamentJoinedPopupNode_t4BCAE122FEC6FDE5C033DCBBA2F83498444A39C8, ____gameSpark_9)); }
	inline GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * get__gameSpark_9() const { return ____gameSpark_9; }
	inline GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 ** get_address_of__gameSpark_9() { return &____gameSpark_9; }
	inline void set__gameSpark_9(GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * value)
	{
		____gameSpark_9 = value;
		Il2CppCodeGenWriteBarrier((&____gameSpark_9), value);
	}

	inline static int32_t get_offset_of__lastTimeRefresh_10() { return static_cast<int32_t>(offsetof(StarTournamentJoinedPopupNode_t4BCAE122FEC6FDE5C033DCBBA2F83498444A39C8, ____lastTimeRefresh_10)); }
	inline float get__lastTimeRefresh_10() const { return ____lastTimeRefresh_10; }
	inline float* get_address_of__lastTimeRefresh_10() { return &____lastTimeRefresh_10; }
	inline void set__lastTimeRefresh_10(float value)
	{
		____lastTimeRefresh_10 = value;
	}

	inline static int32_t get_offset_of__items_11() { return static_cast<int32_t>(offsetof(StarTournamentJoinedPopupNode_t4BCAE122FEC6FDE5C033DCBBA2F83498444A39C8, ____items_11)); }
	inline List_1_t386DA0CB20C9114E16EF88E20B87F7B391EAA070 * get__items_11() const { return ____items_11; }
	inline List_1_t386DA0CB20C9114E16EF88E20B87F7B391EAA070 ** get_address_of__items_11() { return &____items_11; }
	inline void set__items_11(List_1_t386DA0CB20C9114E16EF88E20B87F7B391EAA070 * value)
	{
		____items_11 = value;
		Il2CppCodeGenWriteBarrier((&____items_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STARTOURNAMENTJOINEDPOPUPNODE_T4BCAE122FEC6FDE5C033DCBBA2F83498444A39C8_H
#ifndef STARTOURNAMENTSTARTEDPOPUPNODE_T678998B84DDCA79F2FA54A02895AB025E05C21C4_H
#define STARTOURNAMENTSTARTEDPOPUPNODE_T678998B84DDCA79F2FA54A02895AB025E05C21C4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StarTournamentStartedPopupNode
struct  StarTournamentStartedPopupNode_t678998B84DDCA79F2FA54A02895AB025E05C21C4  : public PopupNode_1_tC4D74D0E442AE2B83A0453484F4005FC1E81B3EB
{
public:
	// UnityEngine.UI.Button StarTournamentStartedPopupNode::_continueBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____continueBtn_7;

public:
	inline static int32_t get_offset_of__continueBtn_7() { return static_cast<int32_t>(offsetof(StarTournamentStartedPopupNode_t678998B84DDCA79F2FA54A02895AB025E05C21C4, ____continueBtn_7)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__continueBtn_7() const { return ____continueBtn_7; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__continueBtn_7() { return &____continueBtn_7; }
	inline void set__continueBtn_7(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____continueBtn_7 = value;
		Il2CppCodeGenWriteBarrier((&____continueBtn_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STARTOURNAMENTSTARTEDPOPUPNODE_T678998B84DDCA79F2FA54A02895AB025E05C21C4_H
#ifndef TEAMDETAILSPOPUPNODE_T94CE3993039832F823E9CAB0C650365208416D8C_H
#define TEAMDETAILSPOPUPNODE_T94CE3993039832F823E9CAB0C650365208416D8C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TeamDetailsPopupNode
struct  TeamDetailsPopupNode_t94CE3993039832F823E9CAB0C650365208416D8C  : public PopupNode_1_tC4D74D0E442AE2B83A0453484F4005FC1E81B3EB
{
public:
	// UnityEngine.UI.Image TeamDetailsPopupNode::_teamIcon
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____teamIcon_7;
	// TMPro.TextMeshProUGUI TeamDetailsPopupNode::_teamName
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____teamName_8;
	// TMPro.TextMeshProUGUI TeamDetailsPopupNode::_teamDescription
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____teamDescription_9;
	// TMPro.TextMeshProUGUI TeamDetailsPopupNode::_teamMembers
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____teamMembers_10;
	// TMPro.TextMeshProUGUI TeamDetailsPopupNode::_teamScore
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____teamScore_11;
	// TMPro.TextMeshProUGUI TeamDetailsPopupNode::_weeklyHelp
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____weeklyHelp_12;
	// TMPro.TextMeshProUGUI TeamDetailsPopupNode::_requiredLevel
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____requiredLevel_13;
	// TMPro.TextMeshProUGUI TeamDetailsPopupNode::_teamType
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____teamType_14;
	// UnityEngine.UI.Button TeamDetailsPopupNode::_joinBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____joinBtn_15;
	// Tayr.ILibrary TeamDetailsPopupNode::_iconLibrary
	RuntimeObject* ____iconLibrary_16;
	// UserEvents TeamDetailsPopupNode::_userEvents
	UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * ____userEvents_17;
	// Tayr.GameSparksPlatform TeamDetailsPopupNode::_gameSparks
	GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * ____gameSparks_18;
	// UserVO TeamDetailsPopupNode::_userVO
	UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * ____userVO_19;
	// KickerManager TeamDetailsPopupNode::_kickerManager
	KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA * ____kickerManager_20;
	// Tayr.VOSaver TeamDetailsPopupNode::_voSaver
	VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * ____voSaver_21;
	// GameSparks.Core.GSData TeamDetailsPopupNode::_teamData
	GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 * ____teamData_22;

public:
	inline static int32_t get_offset_of__teamIcon_7() { return static_cast<int32_t>(offsetof(TeamDetailsPopupNode_t94CE3993039832F823E9CAB0C650365208416D8C, ____teamIcon_7)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__teamIcon_7() const { return ____teamIcon_7; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__teamIcon_7() { return &____teamIcon_7; }
	inline void set__teamIcon_7(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____teamIcon_7 = value;
		Il2CppCodeGenWriteBarrier((&____teamIcon_7), value);
	}

	inline static int32_t get_offset_of__teamName_8() { return static_cast<int32_t>(offsetof(TeamDetailsPopupNode_t94CE3993039832F823E9CAB0C650365208416D8C, ____teamName_8)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__teamName_8() const { return ____teamName_8; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__teamName_8() { return &____teamName_8; }
	inline void set__teamName_8(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____teamName_8 = value;
		Il2CppCodeGenWriteBarrier((&____teamName_8), value);
	}

	inline static int32_t get_offset_of__teamDescription_9() { return static_cast<int32_t>(offsetof(TeamDetailsPopupNode_t94CE3993039832F823E9CAB0C650365208416D8C, ____teamDescription_9)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__teamDescription_9() const { return ____teamDescription_9; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__teamDescription_9() { return &____teamDescription_9; }
	inline void set__teamDescription_9(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____teamDescription_9 = value;
		Il2CppCodeGenWriteBarrier((&____teamDescription_9), value);
	}

	inline static int32_t get_offset_of__teamMembers_10() { return static_cast<int32_t>(offsetof(TeamDetailsPopupNode_t94CE3993039832F823E9CAB0C650365208416D8C, ____teamMembers_10)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__teamMembers_10() const { return ____teamMembers_10; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__teamMembers_10() { return &____teamMembers_10; }
	inline void set__teamMembers_10(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____teamMembers_10 = value;
		Il2CppCodeGenWriteBarrier((&____teamMembers_10), value);
	}

	inline static int32_t get_offset_of__teamScore_11() { return static_cast<int32_t>(offsetof(TeamDetailsPopupNode_t94CE3993039832F823E9CAB0C650365208416D8C, ____teamScore_11)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__teamScore_11() const { return ____teamScore_11; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__teamScore_11() { return &____teamScore_11; }
	inline void set__teamScore_11(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____teamScore_11 = value;
		Il2CppCodeGenWriteBarrier((&____teamScore_11), value);
	}

	inline static int32_t get_offset_of__weeklyHelp_12() { return static_cast<int32_t>(offsetof(TeamDetailsPopupNode_t94CE3993039832F823E9CAB0C650365208416D8C, ____weeklyHelp_12)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__weeklyHelp_12() const { return ____weeklyHelp_12; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__weeklyHelp_12() { return &____weeklyHelp_12; }
	inline void set__weeklyHelp_12(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____weeklyHelp_12 = value;
		Il2CppCodeGenWriteBarrier((&____weeklyHelp_12), value);
	}

	inline static int32_t get_offset_of__requiredLevel_13() { return static_cast<int32_t>(offsetof(TeamDetailsPopupNode_t94CE3993039832F823E9CAB0C650365208416D8C, ____requiredLevel_13)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__requiredLevel_13() const { return ____requiredLevel_13; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__requiredLevel_13() { return &____requiredLevel_13; }
	inline void set__requiredLevel_13(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____requiredLevel_13 = value;
		Il2CppCodeGenWriteBarrier((&____requiredLevel_13), value);
	}

	inline static int32_t get_offset_of__teamType_14() { return static_cast<int32_t>(offsetof(TeamDetailsPopupNode_t94CE3993039832F823E9CAB0C650365208416D8C, ____teamType_14)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__teamType_14() const { return ____teamType_14; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__teamType_14() { return &____teamType_14; }
	inline void set__teamType_14(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____teamType_14 = value;
		Il2CppCodeGenWriteBarrier((&____teamType_14), value);
	}

	inline static int32_t get_offset_of__joinBtn_15() { return static_cast<int32_t>(offsetof(TeamDetailsPopupNode_t94CE3993039832F823E9CAB0C650365208416D8C, ____joinBtn_15)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__joinBtn_15() const { return ____joinBtn_15; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__joinBtn_15() { return &____joinBtn_15; }
	inline void set__joinBtn_15(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____joinBtn_15 = value;
		Il2CppCodeGenWriteBarrier((&____joinBtn_15), value);
	}

	inline static int32_t get_offset_of__iconLibrary_16() { return static_cast<int32_t>(offsetof(TeamDetailsPopupNode_t94CE3993039832F823E9CAB0C650365208416D8C, ____iconLibrary_16)); }
	inline RuntimeObject* get__iconLibrary_16() const { return ____iconLibrary_16; }
	inline RuntimeObject** get_address_of__iconLibrary_16() { return &____iconLibrary_16; }
	inline void set__iconLibrary_16(RuntimeObject* value)
	{
		____iconLibrary_16 = value;
		Il2CppCodeGenWriteBarrier((&____iconLibrary_16), value);
	}

	inline static int32_t get_offset_of__userEvents_17() { return static_cast<int32_t>(offsetof(TeamDetailsPopupNode_t94CE3993039832F823E9CAB0C650365208416D8C, ____userEvents_17)); }
	inline UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * get__userEvents_17() const { return ____userEvents_17; }
	inline UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 ** get_address_of__userEvents_17() { return &____userEvents_17; }
	inline void set__userEvents_17(UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * value)
	{
		____userEvents_17 = value;
		Il2CppCodeGenWriteBarrier((&____userEvents_17), value);
	}

	inline static int32_t get_offset_of__gameSparks_18() { return static_cast<int32_t>(offsetof(TeamDetailsPopupNode_t94CE3993039832F823E9CAB0C650365208416D8C, ____gameSparks_18)); }
	inline GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * get__gameSparks_18() const { return ____gameSparks_18; }
	inline GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 ** get_address_of__gameSparks_18() { return &____gameSparks_18; }
	inline void set__gameSparks_18(GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * value)
	{
		____gameSparks_18 = value;
		Il2CppCodeGenWriteBarrier((&____gameSparks_18), value);
	}

	inline static int32_t get_offset_of__userVO_19() { return static_cast<int32_t>(offsetof(TeamDetailsPopupNode_t94CE3993039832F823E9CAB0C650365208416D8C, ____userVO_19)); }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * get__userVO_19() const { return ____userVO_19; }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 ** get_address_of__userVO_19() { return &____userVO_19; }
	inline void set__userVO_19(UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * value)
	{
		____userVO_19 = value;
		Il2CppCodeGenWriteBarrier((&____userVO_19), value);
	}

	inline static int32_t get_offset_of__kickerManager_20() { return static_cast<int32_t>(offsetof(TeamDetailsPopupNode_t94CE3993039832F823E9CAB0C650365208416D8C, ____kickerManager_20)); }
	inline KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA * get__kickerManager_20() const { return ____kickerManager_20; }
	inline KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA ** get_address_of__kickerManager_20() { return &____kickerManager_20; }
	inline void set__kickerManager_20(KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA * value)
	{
		____kickerManager_20 = value;
		Il2CppCodeGenWriteBarrier((&____kickerManager_20), value);
	}

	inline static int32_t get_offset_of__voSaver_21() { return static_cast<int32_t>(offsetof(TeamDetailsPopupNode_t94CE3993039832F823E9CAB0C650365208416D8C, ____voSaver_21)); }
	inline VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * get__voSaver_21() const { return ____voSaver_21; }
	inline VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F ** get_address_of__voSaver_21() { return &____voSaver_21; }
	inline void set__voSaver_21(VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * value)
	{
		____voSaver_21 = value;
		Il2CppCodeGenWriteBarrier((&____voSaver_21), value);
	}

	inline static int32_t get_offset_of__teamData_22() { return static_cast<int32_t>(offsetof(TeamDetailsPopupNode_t94CE3993039832F823E9CAB0C650365208416D8C, ____teamData_22)); }
	inline GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 * get__teamData_22() const { return ____teamData_22; }
	inline GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 ** get_address_of__teamData_22() { return &____teamData_22; }
	inline void set__teamData_22(GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 * value)
	{
		____teamData_22 = value;
		Il2CppCodeGenWriteBarrier((&____teamData_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEAMDETAILSPOPUPNODE_T94CE3993039832F823E9CAB0C650365208416D8C_H
#ifndef TEAMVIEWCOMPONENT_TC26F5EF388A1F94F841B0FFD54B1C6204453020D_H
#define TEAMVIEWCOMPONENT_TC26F5EF388A1F94F841B0FFD54B1C6204453020D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TeamViewComponent
struct  TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D  : public TabsNode_1_t49E3604644CDBB4D36A232C96C46E05ECF9B7E18
{
public:
	// UnityEngine.Transform TeamViewComponent::_notConnected
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____notConnected_8;
	// UnityEngine.Transform TeamViewComponent::_joinTabs
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____joinTabs_9;
	// UnityEngine.Transform TeamViewComponent::_joinedTabs
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____joinedTabs_10;
	// UnityEngine.UI.Button TeamViewComponent::_teamsTabBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____teamsTabBtn_11;
	// UnityEngine.Transform TeamViewComponent::_teamsTabActive
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____teamsTabActive_12;
	// UnityEngine.Transform TeamViewComponent::_teamsTabInactive
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____teamsTabInactive_13;
	// UnityEngine.UI.Button TeamViewComponent::_createTabBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____createTabBtn_14;
	// UnityEngine.Transform TeamViewComponent::_createTabActive
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____createTabActive_15;
	// UnityEngine.Transform TeamViewComponent::_createTabInactive
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____createTabInactive_16;
	// UnityEngine.UI.Button TeamViewComponent::_chatTabBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____chatTabBtn_17;
	// UnityEngine.Transform TeamViewComponent::_chatTabActive
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____chatTabActive_18;
	// UnityEngine.Transform TeamViewComponent::_chatTabInactive
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____chatTabInactive_19;
	// UnityEngine.UI.Button TeamViewComponent::_myteamTabBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____myteamTabBtn_20;
	// UnityEngine.Transform TeamViewComponent::_myteamTabActive
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____myteamTabActive_21;
	// UnityEngine.Transform TeamViewComponent::_myteamTabInactive
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____myteamTabInactive_22;
	// UnityEngine.UI.Button TeamViewComponent::_searchTabBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____searchTabBtn_23;
	// UnityEngine.Transform TeamViewComponent::_searchTabActive
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____searchTabActive_24;
	// UnityEngine.Transform TeamViewComponent::_searchTabInactive
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____searchTabInactive_25;
	// TMPro.TextMeshProUGUI TeamViewComponent::_requirementText
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____requirementText_26;
	// Tayr.ILibrary TeamViewComponent::_uiMain
	RuntimeObject* ____uiMain_27;
	// UserEvents TeamViewComponent::_userEvents
	UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * ____userEvents_28;
	// UserVO TeamViewComponent::_userVO
	UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * ____userVO_29;
	// GameSettingsSO TeamViewComponent::_gameSettingsSO
	GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C * ____gameSettingsSO_30;

public:
	inline static int32_t get_offset_of__notConnected_8() { return static_cast<int32_t>(offsetof(TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D, ____notConnected_8)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__notConnected_8() const { return ____notConnected_8; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__notConnected_8() { return &____notConnected_8; }
	inline void set__notConnected_8(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____notConnected_8 = value;
		Il2CppCodeGenWriteBarrier((&____notConnected_8), value);
	}

	inline static int32_t get_offset_of__joinTabs_9() { return static_cast<int32_t>(offsetof(TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D, ____joinTabs_9)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__joinTabs_9() const { return ____joinTabs_9; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__joinTabs_9() { return &____joinTabs_9; }
	inline void set__joinTabs_9(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____joinTabs_9 = value;
		Il2CppCodeGenWriteBarrier((&____joinTabs_9), value);
	}

	inline static int32_t get_offset_of__joinedTabs_10() { return static_cast<int32_t>(offsetof(TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D, ____joinedTabs_10)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__joinedTabs_10() const { return ____joinedTabs_10; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__joinedTabs_10() { return &____joinedTabs_10; }
	inline void set__joinedTabs_10(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____joinedTabs_10 = value;
		Il2CppCodeGenWriteBarrier((&____joinedTabs_10), value);
	}

	inline static int32_t get_offset_of__teamsTabBtn_11() { return static_cast<int32_t>(offsetof(TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D, ____teamsTabBtn_11)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__teamsTabBtn_11() const { return ____teamsTabBtn_11; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__teamsTabBtn_11() { return &____teamsTabBtn_11; }
	inline void set__teamsTabBtn_11(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____teamsTabBtn_11 = value;
		Il2CppCodeGenWriteBarrier((&____teamsTabBtn_11), value);
	}

	inline static int32_t get_offset_of__teamsTabActive_12() { return static_cast<int32_t>(offsetof(TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D, ____teamsTabActive_12)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__teamsTabActive_12() const { return ____teamsTabActive_12; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__teamsTabActive_12() { return &____teamsTabActive_12; }
	inline void set__teamsTabActive_12(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____teamsTabActive_12 = value;
		Il2CppCodeGenWriteBarrier((&____teamsTabActive_12), value);
	}

	inline static int32_t get_offset_of__teamsTabInactive_13() { return static_cast<int32_t>(offsetof(TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D, ____teamsTabInactive_13)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__teamsTabInactive_13() const { return ____teamsTabInactive_13; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__teamsTabInactive_13() { return &____teamsTabInactive_13; }
	inline void set__teamsTabInactive_13(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____teamsTabInactive_13 = value;
		Il2CppCodeGenWriteBarrier((&____teamsTabInactive_13), value);
	}

	inline static int32_t get_offset_of__createTabBtn_14() { return static_cast<int32_t>(offsetof(TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D, ____createTabBtn_14)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__createTabBtn_14() const { return ____createTabBtn_14; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__createTabBtn_14() { return &____createTabBtn_14; }
	inline void set__createTabBtn_14(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____createTabBtn_14 = value;
		Il2CppCodeGenWriteBarrier((&____createTabBtn_14), value);
	}

	inline static int32_t get_offset_of__createTabActive_15() { return static_cast<int32_t>(offsetof(TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D, ____createTabActive_15)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__createTabActive_15() const { return ____createTabActive_15; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__createTabActive_15() { return &____createTabActive_15; }
	inline void set__createTabActive_15(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____createTabActive_15 = value;
		Il2CppCodeGenWriteBarrier((&____createTabActive_15), value);
	}

	inline static int32_t get_offset_of__createTabInactive_16() { return static_cast<int32_t>(offsetof(TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D, ____createTabInactive_16)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__createTabInactive_16() const { return ____createTabInactive_16; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__createTabInactive_16() { return &____createTabInactive_16; }
	inline void set__createTabInactive_16(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____createTabInactive_16 = value;
		Il2CppCodeGenWriteBarrier((&____createTabInactive_16), value);
	}

	inline static int32_t get_offset_of__chatTabBtn_17() { return static_cast<int32_t>(offsetof(TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D, ____chatTabBtn_17)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__chatTabBtn_17() const { return ____chatTabBtn_17; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__chatTabBtn_17() { return &____chatTabBtn_17; }
	inline void set__chatTabBtn_17(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____chatTabBtn_17 = value;
		Il2CppCodeGenWriteBarrier((&____chatTabBtn_17), value);
	}

	inline static int32_t get_offset_of__chatTabActive_18() { return static_cast<int32_t>(offsetof(TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D, ____chatTabActive_18)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__chatTabActive_18() const { return ____chatTabActive_18; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__chatTabActive_18() { return &____chatTabActive_18; }
	inline void set__chatTabActive_18(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____chatTabActive_18 = value;
		Il2CppCodeGenWriteBarrier((&____chatTabActive_18), value);
	}

	inline static int32_t get_offset_of__chatTabInactive_19() { return static_cast<int32_t>(offsetof(TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D, ____chatTabInactive_19)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__chatTabInactive_19() const { return ____chatTabInactive_19; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__chatTabInactive_19() { return &____chatTabInactive_19; }
	inline void set__chatTabInactive_19(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____chatTabInactive_19 = value;
		Il2CppCodeGenWriteBarrier((&____chatTabInactive_19), value);
	}

	inline static int32_t get_offset_of__myteamTabBtn_20() { return static_cast<int32_t>(offsetof(TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D, ____myteamTabBtn_20)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__myteamTabBtn_20() const { return ____myteamTabBtn_20; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__myteamTabBtn_20() { return &____myteamTabBtn_20; }
	inline void set__myteamTabBtn_20(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____myteamTabBtn_20 = value;
		Il2CppCodeGenWriteBarrier((&____myteamTabBtn_20), value);
	}

	inline static int32_t get_offset_of__myteamTabActive_21() { return static_cast<int32_t>(offsetof(TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D, ____myteamTabActive_21)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__myteamTabActive_21() const { return ____myteamTabActive_21; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__myteamTabActive_21() { return &____myteamTabActive_21; }
	inline void set__myteamTabActive_21(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____myteamTabActive_21 = value;
		Il2CppCodeGenWriteBarrier((&____myteamTabActive_21), value);
	}

	inline static int32_t get_offset_of__myteamTabInactive_22() { return static_cast<int32_t>(offsetof(TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D, ____myteamTabInactive_22)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__myteamTabInactive_22() const { return ____myteamTabInactive_22; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__myteamTabInactive_22() { return &____myteamTabInactive_22; }
	inline void set__myteamTabInactive_22(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____myteamTabInactive_22 = value;
		Il2CppCodeGenWriteBarrier((&____myteamTabInactive_22), value);
	}

	inline static int32_t get_offset_of__searchTabBtn_23() { return static_cast<int32_t>(offsetof(TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D, ____searchTabBtn_23)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__searchTabBtn_23() const { return ____searchTabBtn_23; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__searchTabBtn_23() { return &____searchTabBtn_23; }
	inline void set__searchTabBtn_23(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____searchTabBtn_23 = value;
		Il2CppCodeGenWriteBarrier((&____searchTabBtn_23), value);
	}

	inline static int32_t get_offset_of__searchTabActive_24() { return static_cast<int32_t>(offsetof(TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D, ____searchTabActive_24)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__searchTabActive_24() const { return ____searchTabActive_24; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__searchTabActive_24() { return &____searchTabActive_24; }
	inline void set__searchTabActive_24(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____searchTabActive_24 = value;
		Il2CppCodeGenWriteBarrier((&____searchTabActive_24), value);
	}

	inline static int32_t get_offset_of__searchTabInactive_25() { return static_cast<int32_t>(offsetof(TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D, ____searchTabInactive_25)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__searchTabInactive_25() const { return ____searchTabInactive_25; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__searchTabInactive_25() { return &____searchTabInactive_25; }
	inline void set__searchTabInactive_25(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____searchTabInactive_25 = value;
		Il2CppCodeGenWriteBarrier((&____searchTabInactive_25), value);
	}

	inline static int32_t get_offset_of__requirementText_26() { return static_cast<int32_t>(offsetof(TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D, ____requirementText_26)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__requirementText_26() const { return ____requirementText_26; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__requirementText_26() { return &____requirementText_26; }
	inline void set__requirementText_26(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____requirementText_26 = value;
		Il2CppCodeGenWriteBarrier((&____requirementText_26), value);
	}

	inline static int32_t get_offset_of__uiMain_27() { return static_cast<int32_t>(offsetof(TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D, ____uiMain_27)); }
	inline RuntimeObject* get__uiMain_27() const { return ____uiMain_27; }
	inline RuntimeObject** get_address_of__uiMain_27() { return &____uiMain_27; }
	inline void set__uiMain_27(RuntimeObject* value)
	{
		____uiMain_27 = value;
		Il2CppCodeGenWriteBarrier((&____uiMain_27), value);
	}

	inline static int32_t get_offset_of__userEvents_28() { return static_cast<int32_t>(offsetof(TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D, ____userEvents_28)); }
	inline UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * get__userEvents_28() const { return ____userEvents_28; }
	inline UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 ** get_address_of__userEvents_28() { return &____userEvents_28; }
	inline void set__userEvents_28(UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * value)
	{
		____userEvents_28 = value;
		Il2CppCodeGenWriteBarrier((&____userEvents_28), value);
	}

	inline static int32_t get_offset_of__userVO_29() { return static_cast<int32_t>(offsetof(TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D, ____userVO_29)); }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * get__userVO_29() const { return ____userVO_29; }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 ** get_address_of__userVO_29() { return &____userVO_29; }
	inline void set__userVO_29(UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * value)
	{
		____userVO_29 = value;
		Il2CppCodeGenWriteBarrier((&____userVO_29), value);
	}

	inline static int32_t get_offset_of__gameSettingsSO_30() { return static_cast<int32_t>(offsetof(TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D, ____gameSettingsSO_30)); }
	inline GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C * get__gameSettingsSO_30() const { return ____gameSettingsSO_30; }
	inline GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C ** get_address_of__gameSettingsSO_30() { return &____gameSettingsSO_30; }
	inline void set__gameSettingsSO_30(GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C * value)
	{
		____gameSettingsSO_30 = value;
		Il2CppCodeGenWriteBarrier((&____gameSettingsSO_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEAMVIEWCOMPONENT_TC26F5EF388A1F94F841B0FFD54B1C6204453020D_H
#ifndef TRIBETOURNAMENTENDEDPOPUPNODE_TD46D0CFDC9BAA5806C2FAF5B38D9542A0057DA8E_H
#define TRIBETOURNAMENTENDEDPOPUPNODE_TD46D0CFDC9BAA5806C2FAF5B38D9542A0057DA8E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TribeTournamentEndedPopupNode
struct  TribeTournamentEndedPopupNode_tD46D0CFDC9BAA5806C2FAF5B38D9542A0057DA8E  : public PopupNode_1_tC4D74D0E442AE2B83A0453484F4005FC1E81B3EB
{
public:
	// Tayr.TList TribeTournamentEndedPopupNode::_leaderboardList
	TList_tB67CEA10085B72B7C69ACE7FCF01FCC38E6EF292 * ____leaderboardList_7;
	// Tayr.TList TribeTournamentEndedPopupNode::_contributionList
	TList_tB67CEA10085B72B7C69ACE7FCF01FCC38E6EF292 * ____contributionList_8;
	// UnityEngine.UI.Button TribeTournamentEndedPopupNode::_closeBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____closeBtn_9;
	// UnityEngine.UI.Button TribeTournamentEndedPopupNode::_leaderboardBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____leaderboardBtn_10;
	// UnityEngine.UI.Button TribeTournamentEndedPopupNode::_contributionBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____contributionBtn_11;
	// UnityEngine.UI.Button TribeTournamentEndedPopupNode::_claimBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____claimBtn_12;
	// UnityEngine.Transform TribeTournamentEndedPopupNode::_leaderboardOn
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____leaderboardOn_13;
	// UnityEngine.Transform TribeTournamentEndedPopupNode::_contributionOn
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____contributionOn_14;
	// Tayr.ILibrary TribeTournamentEndedPopupNode::_uiMain
	RuntimeObject* ____uiMain_15;
	// Tayr.GameSparksPlatform TribeTournamentEndedPopupNode::_gameSpark
	GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * ____gameSpark_16;
	// KickerManager TribeTournamentEndedPopupNode::_kickerManager
	KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA * ____kickerManager_17;
	// UserVO TribeTournamentEndedPopupNode::_userVO
	UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * ____userVO_18;
	// System.Collections.Generic.List`1<TribeTournamentListItemData> TribeTournamentEndedPopupNode::_leaderboardItems
	List_1_t6367895834A22C833A94F350602CA85277483EE4 * ____leaderboardItems_19;
	// System.Collections.Generic.List`1<TribeTournamentContributionListItemData> TribeTournamentEndedPopupNode::_contributionItems
	List_1_t3CFA56CDB27665BE76F1EEA57249AE608A43237D * ____contributionItems_20;
	// UnityEngine.GameObject TribeTournamentEndedPopupNode::_seperatorPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____seperatorPrefab_21;

public:
	inline static int32_t get_offset_of__leaderboardList_7() { return static_cast<int32_t>(offsetof(TribeTournamentEndedPopupNode_tD46D0CFDC9BAA5806C2FAF5B38D9542A0057DA8E, ____leaderboardList_7)); }
	inline TList_tB67CEA10085B72B7C69ACE7FCF01FCC38E6EF292 * get__leaderboardList_7() const { return ____leaderboardList_7; }
	inline TList_tB67CEA10085B72B7C69ACE7FCF01FCC38E6EF292 ** get_address_of__leaderboardList_7() { return &____leaderboardList_7; }
	inline void set__leaderboardList_7(TList_tB67CEA10085B72B7C69ACE7FCF01FCC38E6EF292 * value)
	{
		____leaderboardList_7 = value;
		Il2CppCodeGenWriteBarrier((&____leaderboardList_7), value);
	}

	inline static int32_t get_offset_of__contributionList_8() { return static_cast<int32_t>(offsetof(TribeTournamentEndedPopupNode_tD46D0CFDC9BAA5806C2FAF5B38D9542A0057DA8E, ____contributionList_8)); }
	inline TList_tB67CEA10085B72B7C69ACE7FCF01FCC38E6EF292 * get__contributionList_8() const { return ____contributionList_8; }
	inline TList_tB67CEA10085B72B7C69ACE7FCF01FCC38E6EF292 ** get_address_of__contributionList_8() { return &____contributionList_8; }
	inline void set__contributionList_8(TList_tB67CEA10085B72B7C69ACE7FCF01FCC38E6EF292 * value)
	{
		____contributionList_8 = value;
		Il2CppCodeGenWriteBarrier((&____contributionList_8), value);
	}

	inline static int32_t get_offset_of__closeBtn_9() { return static_cast<int32_t>(offsetof(TribeTournamentEndedPopupNode_tD46D0CFDC9BAA5806C2FAF5B38D9542A0057DA8E, ____closeBtn_9)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__closeBtn_9() const { return ____closeBtn_9; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__closeBtn_9() { return &____closeBtn_9; }
	inline void set__closeBtn_9(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____closeBtn_9 = value;
		Il2CppCodeGenWriteBarrier((&____closeBtn_9), value);
	}

	inline static int32_t get_offset_of__leaderboardBtn_10() { return static_cast<int32_t>(offsetof(TribeTournamentEndedPopupNode_tD46D0CFDC9BAA5806C2FAF5B38D9542A0057DA8E, ____leaderboardBtn_10)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__leaderboardBtn_10() const { return ____leaderboardBtn_10; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__leaderboardBtn_10() { return &____leaderboardBtn_10; }
	inline void set__leaderboardBtn_10(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____leaderboardBtn_10 = value;
		Il2CppCodeGenWriteBarrier((&____leaderboardBtn_10), value);
	}

	inline static int32_t get_offset_of__contributionBtn_11() { return static_cast<int32_t>(offsetof(TribeTournamentEndedPopupNode_tD46D0CFDC9BAA5806C2FAF5B38D9542A0057DA8E, ____contributionBtn_11)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__contributionBtn_11() const { return ____contributionBtn_11; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__contributionBtn_11() { return &____contributionBtn_11; }
	inline void set__contributionBtn_11(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____contributionBtn_11 = value;
		Il2CppCodeGenWriteBarrier((&____contributionBtn_11), value);
	}

	inline static int32_t get_offset_of__claimBtn_12() { return static_cast<int32_t>(offsetof(TribeTournamentEndedPopupNode_tD46D0CFDC9BAA5806C2FAF5B38D9542A0057DA8E, ____claimBtn_12)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__claimBtn_12() const { return ____claimBtn_12; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__claimBtn_12() { return &____claimBtn_12; }
	inline void set__claimBtn_12(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____claimBtn_12 = value;
		Il2CppCodeGenWriteBarrier((&____claimBtn_12), value);
	}

	inline static int32_t get_offset_of__leaderboardOn_13() { return static_cast<int32_t>(offsetof(TribeTournamentEndedPopupNode_tD46D0CFDC9BAA5806C2FAF5B38D9542A0057DA8E, ____leaderboardOn_13)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__leaderboardOn_13() const { return ____leaderboardOn_13; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__leaderboardOn_13() { return &____leaderboardOn_13; }
	inline void set__leaderboardOn_13(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____leaderboardOn_13 = value;
		Il2CppCodeGenWriteBarrier((&____leaderboardOn_13), value);
	}

	inline static int32_t get_offset_of__contributionOn_14() { return static_cast<int32_t>(offsetof(TribeTournamentEndedPopupNode_tD46D0CFDC9BAA5806C2FAF5B38D9542A0057DA8E, ____contributionOn_14)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__contributionOn_14() const { return ____contributionOn_14; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__contributionOn_14() { return &____contributionOn_14; }
	inline void set__contributionOn_14(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____contributionOn_14 = value;
		Il2CppCodeGenWriteBarrier((&____contributionOn_14), value);
	}

	inline static int32_t get_offset_of__uiMain_15() { return static_cast<int32_t>(offsetof(TribeTournamentEndedPopupNode_tD46D0CFDC9BAA5806C2FAF5B38D9542A0057DA8E, ____uiMain_15)); }
	inline RuntimeObject* get__uiMain_15() const { return ____uiMain_15; }
	inline RuntimeObject** get_address_of__uiMain_15() { return &____uiMain_15; }
	inline void set__uiMain_15(RuntimeObject* value)
	{
		____uiMain_15 = value;
		Il2CppCodeGenWriteBarrier((&____uiMain_15), value);
	}

	inline static int32_t get_offset_of__gameSpark_16() { return static_cast<int32_t>(offsetof(TribeTournamentEndedPopupNode_tD46D0CFDC9BAA5806C2FAF5B38D9542A0057DA8E, ____gameSpark_16)); }
	inline GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * get__gameSpark_16() const { return ____gameSpark_16; }
	inline GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 ** get_address_of__gameSpark_16() { return &____gameSpark_16; }
	inline void set__gameSpark_16(GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * value)
	{
		____gameSpark_16 = value;
		Il2CppCodeGenWriteBarrier((&____gameSpark_16), value);
	}

	inline static int32_t get_offset_of__kickerManager_17() { return static_cast<int32_t>(offsetof(TribeTournamentEndedPopupNode_tD46D0CFDC9BAA5806C2FAF5B38D9542A0057DA8E, ____kickerManager_17)); }
	inline KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA * get__kickerManager_17() const { return ____kickerManager_17; }
	inline KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA ** get_address_of__kickerManager_17() { return &____kickerManager_17; }
	inline void set__kickerManager_17(KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA * value)
	{
		____kickerManager_17 = value;
		Il2CppCodeGenWriteBarrier((&____kickerManager_17), value);
	}

	inline static int32_t get_offset_of__userVO_18() { return static_cast<int32_t>(offsetof(TribeTournamentEndedPopupNode_tD46D0CFDC9BAA5806C2FAF5B38D9542A0057DA8E, ____userVO_18)); }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * get__userVO_18() const { return ____userVO_18; }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 ** get_address_of__userVO_18() { return &____userVO_18; }
	inline void set__userVO_18(UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * value)
	{
		____userVO_18 = value;
		Il2CppCodeGenWriteBarrier((&____userVO_18), value);
	}

	inline static int32_t get_offset_of__leaderboardItems_19() { return static_cast<int32_t>(offsetof(TribeTournamentEndedPopupNode_tD46D0CFDC9BAA5806C2FAF5B38D9542A0057DA8E, ____leaderboardItems_19)); }
	inline List_1_t6367895834A22C833A94F350602CA85277483EE4 * get__leaderboardItems_19() const { return ____leaderboardItems_19; }
	inline List_1_t6367895834A22C833A94F350602CA85277483EE4 ** get_address_of__leaderboardItems_19() { return &____leaderboardItems_19; }
	inline void set__leaderboardItems_19(List_1_t6367895834A22C833A94F350602CA85277483EE4 * value)
	{
		____leaderboardItems_19 = value;
		Il2CppCodeGenWriteBarrier((&____leaderboardItems_19), value);
	}

	inline static int32_t get_offset_of__contributionItems_20() { return static_cast<int32_t>(offsetof(TribeTournamentEndedPopupNode_tD46D0CFDC9BAA5806C2FAF5B38D9542A0057DA8E, ____contributionItems_20)); }
	inline List_1_t3CFA56CDB27665BE76F1EEA57249AE608A43237D * get__contributionItems_20() const { return ____contributionItems_20; }
	inline List_1_t3CFA56CDB27665BE76F1EEA57249AE608A43237D ** get_address_of__contributionItems_20() { return &____contributionItems_20; }
	inline void set__contributionItems_20(List_1_t3CFA56CDB27665BE76F1EEA57249AE608A43237D * value)
	{
		____contributionItems_20 = value;
		Il2CppCodeGenWriteBarrier((&____contributionItems_20), value);
	}

	inline static int32_t get_offset_of__seperatorPrefab_21() { return static_cast<int32_t>(offsetof(TribeTournamentEndedPopupNode_tD46D0CFDC9BAA5806C2FAF5B38D9542A0057DA8E, ____seperatorPrefab_21)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__seperatorPrefab_21() const { return ____seperatorPrefab_21; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__seperatorPrefab_21() { return &____seperatorPrefab_21; }
	inline void set__seperatorPrefab_21(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____seperatorPrefab_21 = value;
		Il2CppCodeGenWriteBarrier((&____seperatorPrefab_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIBETOURNAMENTENDEDPOPUPNODE_TD46D0CFDC9BAA5806C2FAF5B38D9542A0057DA8E_H
#ifndef TRIBETOURNAMENTINFOPOPUPNODE_T231D4A176DBC62E5A28FC935FD71BEF6D86DC719_H
#define TRIBETOURNAMENTINFOPOPUPNODE_T231D4A176DBC62E5A28FC935FD71BEF6D86DC719_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TribeTournamentInfoPopupNode
struct  TribeTournamentInfoPopupNode_t231D4A176DBC62E5A28FC935FD71BEF6D86DC719  : public PopupNode_1_tC4D74D0E442AE2B83A0453484F4005FC1E81B3EB
{
public:
	// TMPro.TextMeshProUGUI TribeTournamentInfoPopupNode::_infoText
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____infoText_7;
	// UnityEngine.UI.Button TribeTournamentInfoPopupNode::_continueBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____continueBtn_8;

public:
	inline static int32_t get_offset_of__infoText_7() { return static_cast<int32_t>(offsetof(TribeTournamentInfoPopupNode_t231D4A176DBC62E5A28FC935FD71BEF6D86DC719, ____infoText_7)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__infoText_7() const { return ____infoText_7; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__infoText_7() { return &____infoText_7; }
	inline void set__infoText_7(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____infoText_7 = value;
		Il2CppCodeGenWriteBarrier((&____infoText_7), value);
	}

	inline static int32_t get_offset_of__continueBtn_8() { return static_cast<int32_t>(offsetof(TribeTournamentInfoPopupNode_t231D4A176DBC62E5A28FC935FD71BEF6D86DC719, ____continueBtn_8)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__continueBtn_8() const { return ____continueBtn_8; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__continueBtn_8() { return &____continueBtn_8; }
	inline void set__continueBtn_8(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____continueBtn_8 = value;
		Il2CppCodeGenWriteBarrier((&____continueBtn_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIBETOURNAMENTINFOPOPUPNODE_T231D4A176DBC62E5A28FC935FD71BEF6D86DC719_H
#ifndef TRIBETOURNAMENTJOINEDPOPUPNODE_T4725067D973F39C6A3B2BD23D6C1A0BDF6F0B7F4_H
#define TRIBETOURNAMENTJOINEDPOPUPNODE_T4725067D973F39C6A3B2BD23D6C1A0BDF6F0B7F4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TribeTournamentJoinedPopupNode
struct  TribeTournamentJoinedPopupNode_t4725067D973F39C6A3B2BD23D6C1A0BDF6F0B7F4  : public PopupNode_1_tC4D74D0E442AE2B83A0453484F4005FC1E81B3EB
{
public:
	// Tayr.TList TribeTournamentJoinedPopupNode::_leaderboardList
	TList_tB67CEA10085B72B7C69ACE7FCF01FCC38E6EF292 * ____leaderboardList_7;
	// Tayr.TList TribeTournamentJoinedPopupNode::_contributionList
	TList_tB67CEA10085B72B7C69ACE7FCF01FCC38E6EF292 * ____contributionList_8;
	// UnityEngine.UI.Button TribeTournamentJoinedPopupNode::_closeBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____closeBtn_9;
	// UnityEngine.UI.Button TribeTournamentJoinedPopupNode::_leaderboardBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____leaderboardBtn_10;
	// UnityEngine.UI.Button TribeTournamentJoinedPopupNode::_contributionBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____contributionBtn_11;
	// UnityEngine.Transform TribeTournamentJoinedPopupNode::_leaderboardOn
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____leaderboardOn_12;
	// UnityEngine.Transform TribeTournamentJoinedPopupNode::_contributionOn
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____contributionOn_13;
	// Tayr.ILibrary TribeTournamentJoinedPopupNode::_uiMain
	RuntimeObject* ____uiMain_14;
	// Tayr.GameSparksPlatform TribeTournamentJoinedPopupNode::_gameSpark
	GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * ____gameSpark_15;
	// System.Single TribeTournamentJoinedPopupNode::_lastTimeRefresh
	float ____lastTimeRefresh_16;
	// System.Collections.Generic.List`1<TribeTournamentListItemData> TribeTournamentJoinedPopupNode::_leaderboardItems
	List_1_t6367895834A22C833A94F350602CA85277483EE4 * ____leaderboardItems_17;
	// System.Collections.Generic.List`1<TribeTournamentContributionListItemData> TribeTournamentJoinedPopupNode::_contributionItems
	List_1_t3CFA56CDB27665BE76F1EEA57249AE608A43237D * ____contributionItems_18;
	// UnityEngine.GameObject TribeTournamentJoinedPopupNode::_seperatorPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____seperatorPrefab_20;

public:
	inline static int32_t get_offset_of__leaderboardList_7() { return static_cast<int32_t>(offsetof(TribeTournamentJoinedPopupNode_t4725067D973F39C6A3B2BD23D6C1A0BDF6F0B7F4, ____leaderboardList_7)); }
	inline TList_tB67CEA10085B72B7C69ACE7FCF01FCC38E6EF292 * get__leaderboardList_7() const { return ____leaderboardList_7; }
	inline TList_tB67CEA10085B72B7C69ACE7FCF01FCC38E6EF292 ** get_address_of__leaderboardList_7() { return &____leaderboardList_7; }
	inline void set__leaderboardList_7(TList_tB67CEA10085B72B7C69ACE7FCF01FCC38E6EF292 * value)
	{
		____leaderboardList_7 = value;
		Il2CppCodeGenWriteBarrier((&____leaderboardList_7), value);
	}

	inline static int32_t get_offset_of__contributionList_8() { return static_cast<int32_t>(offsetof(TribeTournamentJoinedPopupNode_t4725067D973F39C6A3B2BD23D6C1A0BDF6F0B7F4, ____contributionList_8)); }
	inline TList_tB67CEA10085B72B7C69ACE7FCF01FCC38E6EF292 * get__contributionList_8() const { return ____contributionList_8; }
	inline TList_tB67CEA10085B72B7C69ACE7FCF01FCC38E6EF292 ** get_address_of__contributionList_8() { return &____contributionList_8; }
	inline void set__contributionList_8(TList_tB67CEA10085B72B7C69ACE7FCF01FCC38E6EF292 * value)
	{
		____contributionList_8 = value;
		Il2CppCodeGenWriteBarrier((&____contributionList_8), value);
	}

	inline static int32_t get_offset_of__closeBtn_9() { return static_cast<int32_t>(offsetof(TribeTournamentJoinedPopupNode_t4725067D973F39C6A3B2BD23D6C1A0BDF6F0B7F4, ____closeBtn_9)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__closeBtn_9() const { return ____closeBtn_9; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__closeBtn_9() { return &____closeBtn_9; }
	inline void set__closeBtn_9(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____closeBtn_9 = value;
		Il2CppCodeGenWriteBarrier((&____closeBtn_9), value);
	}

	inline static int32_t get_offset_of__leaderboardBtn_10() { return static_cast<int32_t>(offsetof(TribeTournamentJoinedPopupNode_t4725067D973F39C6A3B2BD23D6C1A0BDF6F0B7F4, ____leaderboardBtn_10)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__leaderboardBtn_10() const { return ____leaderboardBtn_10; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__leaderboardBtn_10() { return &____leaderboardBtn_10; }
	inline void set__leaderboardBtn_10(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____leaderboardBtn_10 = value;
		Il2CppCodeGenWriteBarrier((&____leaderboardBtn_10), value);
	}

	inline static int32_t get_offset_of__contributionBtn_11() { return static_cast<int32_t>(offsetof(TribeTournamentJoinedPopupNode_t4725067D973F39C6A3B2BD23D6C1A0BDF6F0B7F4, ____contributionBtn_11)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__contributionBtn_11() const { return ____contributionBtn_11; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__contributionBtn_11() { return &____contributionBtn_11; }
	inline void set__contributionBtn_11(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____contributionBtn_11 = value;
		Il2CppCodeGenWriteBarrier((&____contributionBtn_11), value);
	}

	inline static int32_t get_offset_of__leaderboardOn_12() { return static_cast<int32_t>(offsetof(TribeTournamentJoinedPopupNode_t4725067D973F39C6A3B2BD23D6C1A0BDF6F0B7F4, ____leaderboardOn_12)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__leaderboardOn_12() const { return ____leaderboardOn_12; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__leaderboardOn_12() { return &____leaderboardOn_12; }
	inline void set__leaderboardOn_12(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____leaderboardOn_12 = value;
		Il2CppCodeGenWriteBarrier((&____leaderboardOn_12), value);
	}

	inline static int32_t get_offset_of__contributionOn_13() { return static_cast<int32_t>(offsetof(TribeTournamentJoinedPopupNode_t4725067D973F39C6A3B2BD23D6C1A0BDF6F0B7F4, ____contributionOn_13)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__contributionOn_13() const { return ____contributionOn_13; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__contributionOn_13() { return &____contributionOn_13; }
	inline void set__contributionOn_13(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____contributionOn_13 = value;
		Il2CppCodeGenWriteBarrier((&____contributionOn_13), value);
	}

	inline static int32_t get_offset_of__uiMain_14() { return static_cast<int32_t>(offsetof(TribeTournamentJoinedPopupNode_t4725067D973F39C6A3B2BD23D6C1A0BDF6F0B7F4, ____uiMain_14)); }
	inline RuntimeObject* get__uiMain_14() const { return ____uiMain_14; }
	inline RuntimeObject** get_address_of__uiMain_14() { return &____uiMain_14; }
	inline void set__uiMain_14(RuntimeObject* value)
	{
		____uiMain_14 = value;
		Il2CppCodeGenWriteBarrier((&____uiMain_14), value);
	}

	inline static int32_t get_offset_of__gameSpark_15() { return static_cast<int32_t>(offsetof(TribeTournamentJoinedPopupNode_t4725067D973F39C6A3B2BD23D6C1A0BDF6F0B7F4, ____gameSpark_15)); }
	inline GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * get__gameSpark_15() const { return ____gameSpark_15; }
	inline GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 ** get_address_of__gameSpark_15() { return &____gameSpark_15; }
	inline void set__gameSpark_15(GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * value)
	{
		____gameSpark_15 = value;
		Il2CppCodeGenWriteBarrier((&____gameSpark_15), value);
	}

	inline static int32_t get_offset_of__lastTimeRefresh_16() { return static_cast<int32_t>(offsetof(TribeTournamentJoinedPopupNode_t4725067D973F39C6A3B2BD23D6C1A0BDF6F0B7F4, ____lastTimeRefresh_16)); }
	inline float get__lastTimeRefresh_16() const { return ____lastTimeRefresh_16; }
	inline float* get_address_of__lastTimeRefresh_16() { return &____lastTimeRefresh_16; }
	inline void set__lastTimeRefresh_16(float value)
	{
		____lastTimeRefresh_16 = value;
	}

	inline static int32_t get_offset_of__leaderboardItems_17() { return static_cast<int32_t>(offsetof(TribeTournamentJoinedPopupNode_t4725067D973F39C6A3B2BD23D6C1A0BDF6F0B7F4, ____leaderboardItems_17)); }
	inline List_1_t6367895834A22C833A94F350602CA85277483EE4 * get__leaderboardItems_17() const { return ____leaderboardItems_17; }
	inline List_1_t6367895834A22C833A94F350602CA85277483EE4 ** get_address_of__leaderboardItems_17() { return &____leaderboardItems_17; }
	inline void set__leaderboardItems_17(List_1_t6367895834A22C833A94F350602CA85277483EE4 * value)
	{
		____leaderboardItems_17 = value;
		Il2CppCodeGenWriteBarrier((&____leaderboardItems_17), value);
	}

	inline static int32_t get_offset_of__contributionItems_18() { return static_cast<int32_t>(offsetof(TribeTournamentJoinedPopupNode_t4725067D973F39C6A3B2BD23D6C1A0BDF6F0B7F4, ____contributionItems_18)); }
	inline List_1_t3CFA56CDB27665BE76F1EEA57249AE608A43237D * get__contributionItems_18() const { return ____contributionItems_18; }
	inline List_1_t3CFA56CDB27665BE76F1EEA57249AE608A43237D ** get_address_of__contributionItems_18() { return &____contributionItems_18; }
	inline void set__contributionItems_18(List_1_t3CFA56CDB27665BE76F1EEA57249AE608A43237D * value)
	{
		____contributionItems_18 = value;
		Il2CppCodeGenWriteBarrier((&____contributionItems_18), value);
	}

	inline static int32_t get_offset_of__seperatorPrefab_20() { return static_cast<int32_t>(offsetof(TribeTournamentJoinedPopupNode_t4725067D973F39C6A3B2BD23D6C1A0BDF6F0B7F4, ____seperatorPrefab_20)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__seperatorPrefab_20() const { return ____seperatorPrefab_20; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__seperatorPrefab_20() { return &____seperatorPrefab_20; }
	inline void set__seperatorPrefab_20(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____seperatorPrefab_20 = value;
		Il2CppCodeGenWriteBarrier((&____seperatorPrefab_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIBETOURNAMENTJOINEDPOPUPNODE_T4725067D973F39C6A3B2BD23D6C1A0BDF6F0B7F4_H
#ifndef TRIBETOURNAMENTSTARTEDPOPUPNODE_TA95B9DDB1ADAEEF5F7A876B2FB452474B44226B6_H
#define TRIBETOURNAMENTSTARTEDPOPUPNODE_TA95B9DDB1ADAEEF5F7A876B2FB452474B44226B6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TribeTournamentStartedPopupNode
struct  TribeTournamentStartedPopupNode_tA95B9DDB1ADAEEF5F7A876B2FB452474B44226B6  : public PopupNode_1_tC4D74D0E442AE2B83A0453484F4005FC1E81B3EB
{
public:
	// UnityEngine.UI.Button TribeTournamentStartedPopupNode::_continueBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____continueBtn_7;

public:
	inline static int32_t get_offset_of__continueBtn_7() { return static_cast<int32_t>(offsetof(TribeTournamentStartedPopupNode_tA95B9DDB1ADAEEF5F7A876B2FB452474B44226B6, ____continueBtn_7)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__continueBtn_7() const { return ____continueBtn_7; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__continueBtn_7() { return &____continueBtn_7; }
	inline void set__continueBtn_7(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____continueBtn_7 = value;
		Il2CppCodeGenWriteBarrier((&____continueBtn_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIBETOURNAMENTSTARTEDPOPUPNODE_TA95B9DDB1ADAEEF5F7A876B2FB452474B44226B6_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8600 = { sizeof (U3CTaskU3Ed__2_tEE94232046EAF9F0668852CE5B71A652DA1B0444), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8600[3] = 
{
	U3CTaskU3Ed__2_tEE94232046EAF9F0668852CE5B71A652DA1B0444::get_offset_of_U3CU3E1__state_0(),
	U3CTaskU3Ed__2_tEE94232046EAF9F0668852CE5B71A652DA1B0444::get_offset_of_U3CU3E2__current_1(),
	U3CTaskU3Ed__2_tEE94232046EAF9F0668852CE5B71A652DA1B0444::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8601 = { sizeof (NotificationsSystem_t112A3E3163906777DF795E66F73BBA012B942BEC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8601[1] = 
{
	NotificationsSystem_t112A3E3163906777DF795E66F73BBA012B942BEC::get_offset_of__notificationsManager_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8602 = { sizeof (NotificationsUtils_t5C24829892F954D60BC0B145F54F03700C46D50F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8603 = { sizeof (Quest_tBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8603[8] = 
{
	Quest_tBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021::get_offset_of__onQuestStarted_0(),
	Quest_tBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021::get_offset_of__onQuestUpdated_1(),
	Quest_tBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021::get_offset_of__onQuestEnded_2(),
	Quest_tBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021::get_offset_of__startTriggers_3(),
	Quest_tBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021::get_offset_of__endTriggers_4(),
	Quest_tBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021::get_offset_of__isStarted_5(),
	Quest_tBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021::get_offset_of__isUpdated_6(),
	Quest_tBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021::get_offset_of__isEnded_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8604 = { sizeof (U3CU3Ec__DisplayClass8_0_t273D4CBDC29F9DB87ACF1356FE964106CF9E21D9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8604[2] = 
{
	U3CU3Ec__DisplayClass8_0_t273D4CBDC29F9DB87ACF1356FE964106CF9E21D9::get_offset_of_t_0(),
	U3CU3Ec__DisplayClass8_0_t273D4CBDC29F9DB87ACF1356FE964106CF9E21D9::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8605 = { sizeof (U3CU3Ec__DisplayClass11_0_t54616E3EFBF3ACD267B52391C2028B56E60D9328), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8605[2] = 
{
	U3CU3Ec__DisplayClass11_0_t54616E3EFBF3ACD267B52391C2028B56E60D9328::get_offset_of_t_0(),
	U3CU3Ec__DisplayClass11_0_t54616E3EFBF3ACD267B52391C2028B56E60D9328::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8606 = { sizeof (U3CU3Ec__DisplayClass12_0_t7EED697364D448A257B04EB82E68193380EE4802), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8606[3] = 
{
	U3CU3Ec__DisplayClass12_0_t7EED697364D448A257B04EB82E68193380EE4802::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass12_0_t7EED697364D448A257B04EB82E68193380EE4802::get_offset_of_t_1(),
	U3CU3Ec__DisplayClass12_0_t7EED697364D448A257B04EB82E68193380EE4802::get_offset_of_U3CU3E9__0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8607 = { sizeof (OnQuestUpdatedEvent_t8B85F5C13C8C6D94E08B49B1C3AFA8A6118F8CAB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8608 = { sizeof (OnQuestStartedEvent_t8E404B059FB2011AF0DC8299EBBF6A2DB9657948), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8609 = { sizeof (OnQuestEndedEvent_tC0EC99C69DD18EF07F2A9877323E1CE9E7825D6F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8610 = { sizeof (QuestSystem_tDFB5A64D6154EACAD855C582FF3E40C7A14C83D1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8610[2] = 
{
	QuestSystem_tDFB5A64D6154EACAD855C582FF3E40C7A14C83D1::get_offset_of__questsVO_0(),
	QuestSystem_tDFB5A64D6154EACAD855C582FF3E40C7A14C83D1::get_offset_of__quests_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8611 = { sizeof (SoundSystem_tAD8B0EBAA6C3E35776FEC9AC64E85EE19E81694D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8611[2] = 
{
	SoundSystem_tAD8B0EBAA6C3E35776FEC9AC64E85EE19E81694D::get_offset_of__userVO_3(),
	SoundSystem_tAD8B0EBAA6C3E35776FEC9AC64E85EE19E81694D::get_offset_of__soundSystem_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8612 = { sizeof (SoundUtils_t6727D4BB1D79FF716E16DB7DF6045389EF1978ED), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8613 = { sizeof (TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8613[23] = 
{
	TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D::get_offset_of__notConnected_8(),
	TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D::get_offset_of__joinTabs_9(),
	TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D::get_offset_of__joinedTabs_10(),
	TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D::get_offset_of__teamsTabBtn_11(),
	TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D::get_offset_of__teamsTabActive_12(),
	TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D::get_offset_of__teamsTabInactive_13(),
	TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D::get_offset_of__createTabBtn_14(),
	TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D::get_offset_of__createTabActive_15(),
	TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D::get_offset_of__createTabInactive_16(),
	TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D::get_offset_of__chatTabBtn_17(),
	TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D::get_offset_of__chatTabActive_18(),
	TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D::get_offset_of__chatTabInactive_19(),
	TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D::get_offset_of__myteamTabBtn_20(),
	TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D::get_offset_of__myteamTabActive_21(),
	TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D::get_offset_of__myteamTabInactive_22(),
	TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D::get_offset_of__searchTabBtn_23(),
	TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D::get_offset_of__searchTabActive_24(),
	TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D::get_offset_of__searchTabInactive_25(),
	TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D::get_offset_of__requirementText_26(),
	TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D::get_offset_of__uiMain_27(),
	TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D::get_offset_of__userEvents_28(),
	TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D::get_offset_of__userVO_29(),
	TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D::get_offset_of__gameSettingsSO_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8614 = { sizeof (ChatHeartListItem_tBDBC364CD7AE74FAC5A8B29FA33290CBEFF79D29), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8614[9] = 
{
	ChatHeartListItem_tBDBC364CD7AE74FAC5A8B29FA33290CBEFF79D29::get_offset_of__name_4(),
	ChatHeartListItem_tBDBC364CD7AE74FAC5A8B29FA33290CBEFF79D29::get_offset_of__helpBtn_5(),
	ChatHeartListItem_tBDBC364CD7AE74FAC5A8B29FA33290CBEFF79D29::get_offset_of__lifeReceived_6(),
	ChatHeartListItem_tBDBC364CD7AE74FAC5A8B29FA33290CBEFF79D29::get_offset_of__userVO_7(),
	ChatHeartListItem_tBDBC364CD7AE74FAC5A8B29FA33290CBEFF79D29::get_offset_of__userEvents_8(),
	ChatHeartListItem_tBDBC364CD7AE74FAC5A8B29FA33290CBEFF79D29::get_offset_of__gameSparks_9(),
	ChatHeartListItem_tBDBC364CD7AE74FAC5A8B29FA33290CBEFF79D29::get_offset_of__item_10(),
	ChatHeartListItem_tBDBC364CD7AE74FAC5A8B29FA33290CBEFF79D29::get_offset_of__lifeReceivedCount_11(),
	ChatHeartListItem_tBDBC364CD7AE74FAC5A8B29FA33290CBEFF79D29::get_offset_of__index_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8615 = { sizeof (ChatJoinRequestItem_t8DA07962248D92C46A048DD7AAA046D52226CDAC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8615[7] = 
{
	ChatJoinRequestItem_t8DA07962248D92C46A048DD7AAA046D52226CDAC::get_offset_of__name_4(),
	ChatJoinRequestItem_t8DA07962248D92C46A048DD7AAA046D52226CDAC::get_offset_of__yesBtn_5(),
	ChatJoinRequestItem_t8DA07962248D92C46A048DD7AAA046D52226CDAC::get_offset_of__noBtn_6(),
	ChatJoinRequestItem_t8DA07962248D92C46A048DD7AAA046D52226CDAC::get_offset_of__gameSparks_7(),
	ChatJoinRequestItem_t8DA07962248D92C46A048DD7AAA046D52226CDAC::get_offset_of__kickerManager_8(),
	ChatJoinRequestItem_t8DA07962248D92C46A048DD7AAA046D52226CDAC::get_offset_of__data_9(),
	ChatJoinRequestItem_t8DA07962248D92C46A048DD7AAA046D52226CDAC::get_offset_of__index_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8616 = { sizeof (ChatReceiveMsgListItem_tD9C9B67291E9A1C2E30707832237FA6CC0082670), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8616[6] = 
{
	ChatReceiveMsgListItem_tD9C9B67291E9A1C2E30707832237FA6CC0082670::get_offset_of__btn_4(),
	ChatReceiveMsgListItem_tD9C9B67291E9A1C2E30707832237FA6CC0082670::get_offset_of__name_5(),
	ChatReceiveMsgListItem_tD9C9B67291E9A1C2E30707832237FA6CC0082670::get_offset_of__message_6(),
	ChatReceiveMsgListItem_tD9C9B67291E9A1C2E30707832237FA6CC0082670::get_offset_of__time_7(),
	ChatReceiveMsgListItem_tD9C9B67291E9A1C2E30707832237FA6CC0082670::get_offset_of__uiMain_8(),
	ChatReceiveMsgListItem_tD9C9B67291E9A1C2E30707832237FA6CC0082670::get_offset_of__item_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8617 = { sizeof (ChatSendMsgListItem_tC821821D0C7D3E952A9848B38025C10F436A2884), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8617[3] = 
{
	ChatSendMsgListItem_tC821821D0C7D3E952A9848B38025C10F436A2884::get_offset_of__name_4(),
	ChatSendMsgListItem_tC821821D0C7D3E952A9848B38025C10F436A2884::get_offset_of__message_5(),
	ChatSendMsgListItem_tC821821D0C7D3E952A9848B38025C10F436A2884::get_offset_of__time_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8618 = { sizeof (IconsListItem_tFCE5D92F5E1D082DB67513DB5FE32E4D863A40A7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8618[5] = 
{
	IconsListItem_tFCE5D92F5E1D082DB67513DB5FE32E4D863A40A7::get_offset_of__icon_4(),
	IconsListItem_tFCE5D92F5E1D082DB67513DB5FE32E4D863A40A7::get_offset_of__selectIcon_5(),
	IconsListItem_tFCE5D92F5E1D082DB67513DB5FE32E4D863A40A7::get_offset_of__iconLibrary_6(),
	IconsListItem_tFCE5D92F5E1D082DB67513DB5FE32E4D863A40A7::get_offset_of__userEvents_7(),
	IconsListItem_tFCE5D92F5E1D082DB67513DB5FE32E4D863A40A7::get_offset_of__index_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8619 = { sizeof (JoinTeamListItem_t44B45CC0825AB01016E454D9EC234EF33B392A6B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8619[11] = 
{
	JoinTeamListItem_t44B45CC0825AB01016E454D9EC234EF33B392A6B::get_offset_of__teamName_4(),
	JoinTeamListItem_t44B45CC0825AB01016E454D9EC234EF33B392A6B::get_offset_of__teamIcon_5(),
	JoinTeamListItem_t44B45CC0825AB01016E454D9EC234EF33B392A6B::get_offset_of__membersCount_6(),
	JoinTeamListItem_t44B45CC0825AB01016E454D9EC234EF33B392A6B::get_offset_of__joinBtn_7(),
	JoinTeamListItem_t44B45CC0825AB01016E454D9EC234EF33B392A6B::get_offset_of__joinBtnText_8(),
	JoinTeamListItem_t44B45CC0825AB01016E454D9EC234EF33B392A6B::get_offset_of__uiMain_9(),
	JoinTeamListItem_t44B45CC0825AB01016E454D9EC234EF33B392A6B::get_offset_of__iconLibrary_10(),
	JoinTeamListItem_t44B45CC0825AB01016E454D9EC234EF33B392A6B::get_offset_of__gameSpark_11(),
	JoinTeamListItem_t44B45CC0825AB01016E454D9EC234EF33B392A6B::get_offset_of__userEvents_12(),
	JoinTeamListItem_t44B45CC0825AB01016E454D9EC234EF33B392A6B::get_offset_of__userVO_13(),
	JoinTeamListItem_t44B45CC0825AB01016E454D9EC234EF33B392A6B::get_offset_of__item_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8620 = { sizeof (MyTeamListItem_tB264E6A70F6FB1A5968128CBF86FF0987023FFBC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8620[15] = 
{
	MyTeamListItem_tB264E6A70F6FB1A5968128CBF86FF0987023FFBC::get_offset_of__bgHighlight_4(),
	MyTeamListItem_tB264E6A70F6FB1A5968128CBF86FF0987023FFBC::get_offset_of__bgNormal_5(),
	MyTeamListItem_tB264E6A70F6FB1A5968128CBF86FF0987023FFBC::get_offset_of__rankText_6(),
	MyTeamListItem_tB264E6A70F6FB1A5968128CBF86FF0987023FFBC::get_offset_of__rank1_7(),
	MyTeamListItem_tB264E6A70F6FB1A5968128CBF86FF0987023FFBC::get_offset_of__rank2_8(),
	MyTeamListItem_tB264E6A70F6FB1A5968128CBF86FF0987023FFBC::get_offset_of__rank3_9(),
	MyTeamListItem_tB264E6A70F6FB1A5968128CBF86FF0987023FFBC::get_offset_of__normaleName_10(),
	MyTeamListItem_tB264E6A70F6FB1A5968128CBF86FF0987023FFBC::get_offset_of__leaderParent_11(),
	MyTeamListItem_tB264E6A70F6FB1A5968128CBF86FF0987023FFBC::get_offset_of__leaderName_12(),
	MyTeamListItem_tB264E6A70F6FB1A5968128CBF86FF0987023FFBC::get_offset_of__help_13(),
	MyTeamListItem_tB264E6A70F6FB1A5968128CBF86FF0987023FFBC::get_offset_of__level_14(),
	MyTeamListItem_tB264E6A70F6FB1A5968128CBF86FF0987023FFBC::get_offset_of__kick_15(),
	MyTeamListItem_tB264E6A70F6FB1A5968128CBF86FF0987023FFBC::get_offset_of__userVO_16(),
	MyTeamListItem_tB264E6A70F6FB1A5968128CBF86FF0987023FFBC::get_offset_of__uiMain_17(),
	MyTeamListItem_tB264E6A70F6FB1A5968128CBF86FF0987023FFBC::get_offset_of__data_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8621 = { sizeof (BadgesPopupNode_tB7B0E6B874317064FEC0DBDBFF0B63E1CBA25D9A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8621[2] = 
{
	BadgesPopupNode_tB7B0E6B874317064FEC0DBDBFF0B63E1CBA25D9A::get_offset_of__list_7(),
	BadgesPopupNode_tB7B0E6B874317064FEC0DBDBFF0B63E1CBA25D9A::get_offset_of__globalSO_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8622 = { sizeof (ChatNode_t7A741E0D933BAFC733CCBF17E9D553DA370552A0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8622[8] = 
{
	ChatNode_t7A741E0D933BAFC733CCBF17E9D553DA370552A0::get_offset_of__chatList_7(),
	ChatNode_t7A741E0D933BAFC733CCBF17E9D553DA370552A0::get_offset_of__chatBtn_8(),
	ChatNode_t7A741E0D933BAFC733CCBF17E9D553DA370552A0::get_offset_of__heartBtn_9(),
	ChatNode_t7A741E0D933BAFC733CCBF17E9D553DA370552A0::get_offset_of__gameSparks_10(),
	ChatNode_t7A741E0D933BAFC733CCBF17E9D553DA370552A0::get_offset_of__uiMain_11(),
	ChatNode_t7A741E0D933BAFC733CCBF17E9D553DA370552A0::get_offset_of__userVo_12(),
	ChatNode_t7A741E0D933BAFC733CCBF17E9D553DA370552A0::get_offset_of__userEvents_13(),
	ChatNode_t7A741E0D933BAFC733CCBF17E9D553DA370552A0::get_offset_of__keyboard_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8623 = { sizeof (ChatListItemData_tB039C181C297B802B7B35327AA170A2106CDD53B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8623[7] = 
{
	ChatListItemData_tB039C181C297B802B7B35327AA170A2106CDD53B::get_offset_of_MessageId_1(),
	ChatListItemData_tB039C181C297B802B7B35327AA170A2106CDD53B::get_offset_of_SenderId_2(),
	ChatListItemData_tB039C181C297B802B7B35327AA170A2106CDD53B::get_offset_of_SenderDisplayName_3(),
	ChatListItemData_tB039C181C297B802B7B35327AA170A2106CDD53B::get_offset_of_Message_4(),
	ChatListItemData_tB039C181C297B802B7B35327AA170A2106CDD53B::get_offset_of_CustomMessageData_5(),
	ChatListItemData_tB039C181C297B802B7B35327AA170A2106CDD53B::get_offset_of_EpochTime_6(),
	ChatListItemData_tB039C181C297B802B7B35327AA170A2106CDD53B::get_offset_of_List_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8624 = { sizeof (CustomMessageData_t2BA087DE2A16E4C6D102083A45431AA2E422A6DD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8624[2] = 
{
	CustomMessageData_t2BA087DE2A16E4C6D102083A45431AA2E422A6DD::get_offset_of_Action_0(),
	CustomMessageData_t2BA087DE2A16E4C6D102083A45431AA2E422A6DD::get_offset_of_TargetId_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8625 = { sizeof (CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8625[22] = 
{
	CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689::get_offset_of__nameField_7(),
	CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689::get_offset_of__browseBtn_8(),
	CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689::get_offset_of__teamIcon_9(),
	CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689::get_offset_of__descriptionField_10(),
	CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689::get_offset_of__typeLeftBtn_11(),
	CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689::get_offset_of__typeRightBtn_12(),
	CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689::get_offset_of__type_13(),
	CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689::get_offset_of__requiredLeftBtn_14(),
	CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689::get_offset_of__requiredRightBtn_15(),
	CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689::get_offset_of__required_16(),
	CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689::get_offset_of__createBtn_17(),
	CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689::get_offset_of__iconLibrary_18(),
	CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689::get_offset_of__uiMain_19(),
	CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689::get_offset_of__gameSpark_20(),
	CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689::get_offset_of__userEvents_21(),
	CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689::get_offset_of__userVo_22(),
	CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689::get_offset_of__kickerManager_23(),
	CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689::get_offset_of__voSaver_24(),
	CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689::get_offset_of__currentType_25(),
	CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689::get_offset_of__currentRequirement_26(),
	CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689::get_offset_of__currentIcon_27(),
	CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689::get_offset_of__badgesNode_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8626 = { sizeof (TEAM_TYPE_t3C178DBCDEB1B9A42045B6C497BEC33A8557E170)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable8626[3] = 
{
	TEAM_TYPE_t3C178DBCDEB1B9A42045B6C497BEC33A8557E170::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8627 = { sizeof (EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8627[18] = 
{
	EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5::get_offset_of__name_7(),
	EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5::get_offset_of__teamIcon_8(),
	EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5::get_offset_of__descriptionField_9(),
	EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5::get_offset_of__typeLeftBtn_10(),
	EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5::get_offset_of__typeRightBtn_11(),
	EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5::get_offset_of__type_12(),
	EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5::get_offset_of__requiredLeftBtn_13(),
	EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5::get_offset_of__requiredRightBtn_14(),
	EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5::get_offset_of__required_15(),
	EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5::get_offset_of__saveBtn_16(),
	EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5::get_offset_of__iconLibrary_17(),
	EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5::get_offset_of__uiMain_18(),
	EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5::get_offset_of__gameSpark_19(),
	EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5::get_offset_of__userEvents_20(),
	EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5::get_offset_of__userVo_21(),
	EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5::get_offset_of__kickerManager_22(),
	EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5::get_offset_of__currentRequirement_23(),
	EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5::get_offset_of__currentType_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8628 = { sizeof (HeartsReceivedNode_tFA9035693063BE76AAF99DFBE0BB801E47860B26), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8629 = { sizeof (JoinTeamNode_tC9D51921B004C5820A625822BB14B6F90F7D1579), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8629[4] = 
{
	JoinTeamNode_tC9D51921B004C5820A625822BB14B6F90F7D1579::get_offset_of__teamsList_7(),
	JoinTeamNode_tC9D51921B004C5820A625822BB14B6F90F7D1579::get_offset_of__gameSpark_8(),
	JoinTeamNode_tC9D51921B004C5820A625822BB14B6F90F7D1579::get_offset_of__userEvents_9(),
	JoinTeamNode_tC9D51921B004C5820A625822BB14B6F90F7D1579::get_offset_of__teams_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8630 = { sizeof (KickPopupNode_t5FBB58C498CBB2F341DF97DF71672143D20BC3F3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8630[6] = 
{
	KickPopupNode_t5FBB58C498CBB2F341DF97DF71672143D20BC3F3::get_offset_of__text_7(),
	KickPopupNode_t5FBB58C498CBB2F341DF97DF71672143D20BC3F3::get_offset_of__kick_8(),
	KickPopupNode_t5FBB58C498CBB2F341DF97DF71672143D20BC3F3::get_offset_of__cancel_9(),
	KickPopupNode_t5FBB58C498CBB2F341DF97DF71672143D20BC3F3::get_offset_of__gameSparks_10(),
	KickPopupNode_t5FBB58C498CBB2F341DF97DF71672143D20BC3F3::get_offset_of__userEvents_11(),
	KickPopupNode_t5FBB58C498CBB2F341DF97DF71672143D20BC3F3::get_offset_of__initialText_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8631 = { sizeof (KickTooltipNode_t5BA646F823658E7CDA37D282243868064D40F9D2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8631[2] = 
{
	KickTooltipNode_t5BA646F823658E7CDA37D282243868064D40F9D2::get_offset_of__kickBtn_7(),
	KickTooltipNode_t5BA646F823658E7CDA37D282243868064D40F9D2::get_offset_of__uiMain_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8632 = { sizeof (KickTooltipData_t7912892AE01A47560991479A19D49CB87E1F4448), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8632[2] = 
{
	KickTooltipData_t7912892AE01A47560991479A19D49CB87E1F4448::get_offset_of_PlayerData_0(),
	KickTooltipData_t7912892AE01A47560991479A19D49CB87E1F4448::get_offset_of_ListItem_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8633 = { sizeof (LeaveTeamPopupNode_t9B98647F1040635703171B59BB692ADC5BB4DE21), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8633[6] = 
{
	LeaveTeamPopupNode_t9B98647F1040635703171B59BB692ADC5BB4DE21::get_offset_of__leaveBtn_7(),
	LeaveTeamPopupNode_t9B98647F1040635703171B59BB692ADC5BB4DE21::get_offset_of__cancleBtn_8(),
	LeaveTeamPopupNode_t9B98647F1040635703171B59BB692ADC5BB4DE21::get_offset_of__gameSpark_9(),
	LeaveTeamPopupNode_t9B98647F1040635703171B59BB692ADC5BB4DE21::get_offset_of__userEvents_10(),
	LeaveTeamPopupNode_t9B98647F1040635703171B59BB692ADC5BB4DE21::get_offset_of__userVO_11(),
	LeaveTeamPopupNode_t9B98647F1040635703171B59BB692ADC5BB4DE21::get_offset_of__voSaver_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8634 = { sizeof (MyTeamInfoNode_tF5C2189CDF910E7227D0E34ADE7BBB8024740DD0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8634[14] = 
{
	MyTeamInfoNode_tF5C2189CDF910E7227D0E34ADE7BBB8024740DD0::get_offset_of__icon_7(),
	MyTeamInfoNode_tF5C2189CDF910E7227D0E34ADE7BBB8024740DD0::get_offset_of__name_8(),
	MyTeamInfoNode_tF5C2189CDF910E7227D0E34ADE7BBB8024740DD0::get_offset_of__description_9(),
	MyTeamInfoNode_tF5C2189CDF910E7227D0E34ADE7BBB8024740DD0::get_offset_of__members_10(),
	MyTeamInfoNode_tF5C2189CDF910E7227D0E34ADE7BBB8024740DD0::get_offset_of__leaveBtn_11(),
	MyTeamInfoNode_tF5C2189CDF910E7227D0E34ADE7BBB8024740DD0::get_offset_of__editBtn_12(),
	MyTeamInfoNode_tF5C2189CDF910E7227D0E34ADE7BBB8024740DD0::get_offset_of__teamScore_13(),
	MyTeamInfoNode_tF5C2189CDF910E7227D0E34ADE7BBB8024740DD0::get_offset_of__weeklyHelp_14(),
	MyTeamInfoNode_tF5C2189CDF910E7227D0E34ADE7BBB8024740DD0::get_offset_of__requiredLevel_15(),
	MyTeamInfoNode_tF5C2189CDF910E7227D0E34ADE7BBB8024740DD0::get_offset_of__teamType_16(),
	MyTeamInfoNode_tF5C2189CDF910E7227D0E34ADE7BBB8024740DD0::get_offset_of__iconLibrary_17(),
	MyTeamInfoNode_tF5C2189CDF910E7227D0E34ADE7BBB8024740DD0::get_offset_of__uiMain_18(),
	MyTeamInfoNode_tF5C2189CDF910E7227D0E34ADE7BBB8024740DD0::get_offset_of__userEvents_19(),
	MyTeamInfoNode_tF5C2189CDF910E7227D0E34ADE7BBB8024740DD0::get_offset_of__userVo_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8635 = { sizeof (MyTeamNode_tA1981263902DFFE7AAB041823582BEFD1C6815E5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8635[11] = 
{
	MyTeamNode_tA1981263902DFFE7AAB041823582BEFD1C6815E5::get_offset_of__name_7(),
	MyTeamNode_tA1981263902DFFE7AAB041823582BEFD1C6815E5::get_offset_of__members_8(),
	MyTeamNode_tA1981263902DFFE7AAB041823582BEFD1C6815E5::get_offset_of__list_9(),
	MyTeamNode_tA1981263902DFFE7AAB041823582BEFD1C6815E5::get_offset_of__icon_10(),
	MyTeamNode_tA1981263902DFFE7AAB041823582BEFD1C6815E5::get_offset_of__infoBtn_11(),
	MyTeamNode_tA1981263902DFFE7AAB041823582BEFD1C6815E5::get_offset_of__iconLibrary_12(),
	MyTeamNode_tA1981263902DFFE7AAB041823582BEFD1C6815E5::get_offset_of__uiMain_13(),
	MyTeamNode_tA1981263902DFFE7AAB041823582BEFD1C6815E5::get_offset_of__gameSpark_14(),
	MyTeamNode_tA1981263902DFFE7AAB041823582BEFD1C6815E5::get_offset_of__userVO_15(),
	MyTeamNode_tA1981263902DFFE7AAB041823582BEFD1C6815E5::get_offset_of__userEvents_16(),
	MyTeamNode_tA1981263902DFFE7AAB041823582BEFD1C6815E5::get_offset_of__teamDetails_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8636 = { sizeof (ReportChatPopupNode_t794F02E6ED048EDC040CFE0794ABD0F7CDAA5BD9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8636[5] = 
{
	ReportChatPopupNode_t794F02E6ED048EDC040CFE0794ABD0F7CDAA5BD9::get_offset_of__sendBtn_7(),
	ReportChatPopupNode_t794F02E6ED048EDC040CFE0794ABD0F7CDAA5BD9::get_offset_of__emailField_8(),
	ReportChatPopupNode_t794F02E6ED048EDC040CFE0794ABD0F7CDAA5BD9::get_offset_of__descriptionField_9(),
	ReportChatPopupNode_t794F02E6ED048EDC040CFE0794ABD0F7CDAA5BD9::get_offset_of__kicker_10(),
	ReportChatPopupNode_t794F02E6ED048EDC040CFE0794ABD0F7CDAA5BD9::get_offset_of__gameSparks_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8637 = { sizeof (ReportConfirmPopup_t2AF52E64BA94BE60A5C008C27E88AB8962BDF8BE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8637[3] = 
{
	ReportConfirmPopup_t2AF52E64BA94BE60A5C008C27E88AB8962BDF8BE::get_offset_of__yesBtn_7(),
	ReportConfirmPopup_t2AF52E64BA94BE60A5C008C27E88AB8962BDF8BE::get_offset_of__noBtn_8(),
	ReportConfirmPopup_t2AF52E64BA94BE60A5C008C27E88AB8962BDF8BE::get_offset_of__uiMain_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8638 = { sizeof (ReportTooltipNode_t2E2DC6BA9C018B53769C88CD542C8B3418C2B1EC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8638[2] = 
{
	ReportTooltipNode_t2E2DC6BA9C018B53769C88CD542C8B3418C2B1EC::get_offset_of__btn_7(),
	ReportTooltipNode_t2E2DC6BA9C018B53769C88CD542C8B3418C2B1EC::get_offset_of__uiMain_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8639 = { sizeof (ReportTooltipData_tF8EDAC5D364823AA3EA0B424A1C09029CF485A93), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8639[2] = 
{
	ReportTooltipData_tF8EDAC5D364823AA3EA0B424A1C09029CF485A93::get_offset_of_MessageId_0(),
	ReportTooltipData_tF8EDAC5D364823AA3EA0B424A1C09029CF485A93::get_offset_of_Position_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8640 = { sizeof (SearchTeamNode_t97541D5D86D00D231BF9205475733C65BB3AA956), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8640[5] = 
{
	SearchTeamNode_t97541D5D86D00D231BF9205475733C65BB3AA956::get_offset_of__inputField_7(),
	SearchTeamNode_t97541D5D86D00D231BF9205475733C65BB3AA956::get_offset_of__searchButton_8(),
	SearchTeamNode_t97541D5D86D00D231BF9205475733C65BB3AA956::get_offset_of__teamsList_9(),
	SearchTeamNode_t97541D5D86D00D231BF9205475733C65BB3AA956::get_offset_of__gameSparks_10(),
	SearchTeamNode_t97541D5D86D00D231BF9205475733C65BB3AA956::get_offset_of__userEvents_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8641 = { sizeof (TeamDetailsPopupNode_t94CE3993039832F823E9CAB0C650365208416D8C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8641[16] = 
{
	TeamDetailsPopupNode_t94CE3993039832F823E9CAB0C650365208416D8C::get_offset_of__teamIcon_7(),
	TeamDetailsPopupNode_t94CE3993039832F823E9CAB0C650365208416D8C::get_offset_of__teamName_8(),
	TeamDetailsPopupNode_t94CE3993039832F823E9CAB0C650365208416D8C::get_offset_of__teamDescription_9(),
	TeamDetailsPopupNode_t94CE3993039832F823E9CAB0C650365208416D8C::get_offset_of__teamMembers_10(),
	TeamDetailsPopupNode_t94CE3993039832F823E9CAB0C650365208416D8C::get_offset_of__teamScore_11(),
	TeamDetailsPopupNode_t94CE3993039832F823E9CAB0C650365208416D8C::get_offset_of__weeklyHelp_12(),
	TeamDetailsPopupNode_t94CE3993039832F823E9CAB0C650365208416D8C::get_offset_of__requiredLevel_13(),
	TeamDetailsPopupNode_t94CE3993039832F823E9CAB0C650365208416D8C::get_offset_of__teamType_14(),
	TeamDetailsPopupNode_t94CE3993039832F823E9CAB0C650365208416D8C::get_offset_of__joinBtn_15(),
	TeamDetailsPopupNode_t94CE3993039832F823E9CAB0C650365208416D8C::get_offset_of__iconLibrary_16(),
	TeamDetailsPopupNode_t94CE3993039832F823E9CAB0C650365208416D8C::get_offset_of__userEvents_17(),
	TeamDetailsPopupNode_t94CE3993039832F823E9CAB0C650365208416D8C::get_offset_of__gameSparks_18(),
	TeamDetailsPopupNode_t94CE3993039832F823E9CAB0C650365208416D8C::get_offset_of__userVO_19(),
	TeamDetailsPopupNode_t94CE3993039832F823E9CAB0C650365208416D8C::get_offset_of__kickerManager_20(),
	TeamDetailsPopupNode_t94CE3993039832F823E9CAB0C650365208416D8C::get_offset_of__voSaver_21(),
	TeamDetailsPopupNode_t94CE3993039832F823E9CAB0C650365208416D8C::get_offset_of__teamData_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8642 = { sizeof (TeamSystem_t0238BD7E12677CA842EEE8222E0FB9116A52D036), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8642[4] = 
{
	TeamSystem_t0238BD7E12677CA842EEE8222E0FB9116A52D036::get_offset_of__gameSparks_3(),
	TeamSystem_t0238BD7E12677CA842EEE8222E0FB9116A52D036::get_offset_of__userVO_4(),
	TeamSystem_t0238BD7E12677CA842EEE8222E0FB9116A52D036::get_offset_of__userEvents_5(),
	TeamSystem_t0238BD7E12677CA842EEE8222E0FB9116A52D036::get_offset_of__voSaver_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8643 = { sizeof (TimeSystem_t3EA79F95B78D7FF6BC7D3A29AADB4E9CFB0F2CED), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8643[7] = 
{
	TimeSystem_t3EA79F95B78D7FF6BC7D3A29AADB4E9CFB0F2CED::get_offset_of__gameSparks_3(),
	TimeSystem_t3EA79F95B78D7FF6BC7D3A29AADB4E9CFB0F2CED::get_offset_of__userVO_4(),
	TimeSystem_t3EA79F95B78D7FF6BC7D3A29AADB4E9CFB0F2CED::get_offset_of__lastServerTime_5(),
	TimeSystem_t3EA79F95B78D7FF6BC7D3A29AADB4E9CFB0F2CED::get_offset_of__lastClientTime_6(),
	TimeSystem_t3EA79F95B78D7FF6BC7D3A29AADB4E9CFB0F2CED::get_offset_of__clientServerOffset_7(),
	TimeSystem_t3EA79F95B78D7FF6BC7D3A29AADB4E9CFB0F2CED::get_offset_of__timeFound_8(),
	TimeSystem_t3EA79F95B78D7FF6BC7D3A29AADB4E9CFB0F2CED::get_offset_of_U3CServerTimeU3Ek__BackingField_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8644 = { sizeof (TournamentsModel_t51DD2964A1114E748D15DE84191A0CC3F8C9A55B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8644[1] = 
{
	TournamentsModel_t51DD2964A1114E748D15DE84191A0CC3F8C9A55B::get_offset_of_tournaments_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8645 = { sizeof (TournamentModel_tB7076328E3D85DBCC9B2B22F1BD7B3EEC967DB3D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8645[6] = 
{
	TournamentModel_tB7076328E3D85DBCC9B2B22F1BD7B3EEC967DB3D::get_offset_of_id_0(),
	TournamentModel_tB7076328E3D85DBCC9B2B22F1BD7B3EEC967DB3D::get_offset_of_type_1(),
	TournamentModel_tB7076328E3D85DBCC9B2B22F1BD7B3EEC967DB3D::get_offset_of_startTime_2(),
	TournamentModel_tB7076328E3D85DBCC9B2B22F1BD7B3EEC967DB3D::get_offset_of_endTime_3(),
	TournamentModel_tB7076328E3D85DBCC9B2B22F1BD7B3EEC967DB3D::get_offset_of_status_4(),
	TournamentModel_tB7076328E3D85DBCC9B2B22F1BD7B3EEC967DB3D::get_offset_of_rank_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8646 = { sizeof (TournamentType_tDDF357A7FD7E7B9E1A0C976C193A82A0CF9C6EF2)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable8646[3] = 
{
	TournamentType_tDDF357A7FD7E7B9E1A0C976C193A82A0CF9C6EF2::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8647 = { sizeof (TournamentStatus_t7079FE4722A4990FBFA69A120E5A7E6A1148FFEF)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable8647[5] = 
{
	TournamentStatus_t7079FE4722A4990FBFA69A120E5A7E6A1148FFEF::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8648 = { sizeof (StarTournamentComponent_t7DD941F8F2E84CDD23E915D456046E5646A6AB0B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8648[12] = 
{
	StarTournamentComponent_t7DD941F8F2E84CDD23E915D456046E5646A6AB0B::get_offset_of__parent_4(),
	StarTournamentComponent_t7DD941F8F2E84CDD23E915D456046E5646A6AB0B::get_offset_of__button_5(),
	StarTournamentComponent_t7DD941F8F2E84CDD23E915D456046E5646A6AB0B::get_offset_of__timeCount_6(),
	StarTournamentComponent_t7DD941F8F2E84CDD23E915D456046E5646A6AB0B::get_offset_of__rank_7(),
	StarTournamentComponent_t7DD941F8F2E84CDD23E915D456046E5646A6AB0B::get_offset_of__uiMain_8(),
	StarTournamentComponent_t7DD941F8F2E84CDD23E915D456046E5646A6AB0B::get_offset_of__timeSystem_9(),
	StarTournamentComponent_t7DD941F8F2E84CDD23E915D456046E5646A6AB0B::get_offset_of__tournamentSystem_10(),
	StarTournamentComponent_t7DD941F8F2E84CDD23E915D456046E5646A6AB0B::get_offset_of__kickerManager_11(),
	StarTournamentComponent_t7DD941F8F2E84CDD23E915D456046E5646A6AB0B::get_offset_of__diContainer_12(),
	StarTournamentComponent_t7DD941F8F2E84CDD23E915D456046E5646A6AB0B::get_offset_of__active_13(),
	StarTournamentComponent_t7DD941F8F2E84CDD23E915D456046E5646A6AB0B::get_offset_of__endTime_14(),
	StarTournamentComponent_t7DD941F8F2E84CDD23E915D456046E5646A6AB0B::get_offset_of__rankVariable_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8649 = { sizeof (StarTournamentLeaderboardListItem_tA79391D3CE83B88B641ADF858BE52EBAAB1750B7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8649[15] = 
{
	StarTournamentLeaderboardListItem_tA79391D3CE83B88B641ADF858BE52EBAAB1750B7::get_offset_of__bgHighlight_4(),
	StarTournamentLeaderboardListItem_tA79391D3CE83B88B641ADF858BE52EBAAB1750B7::get_offset_of__bgNormal_5(),
	StarTournamentLeaderboardListItem_tA79391D3CE83B88B641ADF858BE52EBAAB1750B7::get_offset_of__firstRank_6(),
	StarTournamentLeaderboardListItem_tA79391D3CE83B88B641ADF858BE52EBAAB1750B7::get_offset_of__secondRank_7(),
	StarTournamentLeaderboardListItem_tA79391D3CE83B88B641ADF858BE52EBAAB1750B7::get_offset_of__thirdRank_8(),
	StarTournamentLeaderboardListItem_tA79391D3CE83B88B641ADF858BE52EBAAB1750B7::get_offset_of__rankText_9(),
	StarTournamentLeaderboardListItem_tA79391D3CE83B88B641ADF858BE52EBAAB1750B7::get_offset_of__teamIcon_10(),
	StarTournamentLeaderboardListItem_tA79391D3CE83B88B641ADF858BE52EBAAB1750B7::get_offset_of__playerName_11(),
	StarTournamentLeaderboardListItem_tA79391D3CE83B88B641ADF858BE52EBAAB1750B7::get_offset_of__teamName_12(),
	StarTournamentLeaderboardListItem_tA79391D3CE83B88B641ADF858BE52EBAAB1750B7::get_offset_of__starsScore_13(),
	StarTournamentLeaderboardListItem_tA79391D3CE83B88B641ADF858BE52EBAAB1750B7::get_offset_of__reward_14(),
	StarTournamentLeaderboardListItem_tA79391D3CE83B88B641ADF858BE52EBAAB1750B7::get_offset_of__viewTeamBtn_15(),
	StarTournamentLeaderboardListItem_tA79391D3CE83B88B641ADF858BE52EBAAB1750B7::get_offset_of__iconLibrary_16(),
	StarTournamentLeaderboardListItem_tA79391D3CE83B88B641ADF858BE52EBAAB1750B7::get_offset_of__userVO_17(),
	StarTournamentLeaderboardListItem_tA79391D3CE83B88B641ADF858BE52EBAAB1750B7::get_offset_of__data_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8650 = { sizeof (StarTournamentListItemData_t6FE18476EB88BBD97CE1527A1C5EE42E3A8936AD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8650[7] = 
{
	StarTournamentListItemData_t6FE18476EB88BBD97CE1527A1C5EE42E3A8936AD::get_offset_of_Icon_0(),
	StarTournamentListItemData_t6FE18476EB88BBD97CE1527A1C5EE42E3A8936AD::get_offset_of_UserName_1(),
	StarTournamentListItemData_t6FE18476EB88BBD97CE1527A1C5EE42E3A8936AD::get_offset_of_UserId_2(),
	StarTournamentListItemData_t6FE18476EB88BBD97CE1527A1C5EE42E3A8936AD::get_offset_of_TeamId_3(),
	StarTournamentListItemData_t6FE18476EB88BBD97CE1527A1C5EE42E3A8936AD::get_offset_of_Stars_4(),
	StarTournamentListItemData_t6FE18476EB88BBD97CE1527A1C5EE42E3A8936AD::get_offset_of_TeamName_5(),
	StarTournamentListItemData_t6FE18476EB88BBD97CE1527A1C5EE42E3A8936AD::get_offset_of_Rank_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8651 = { sizeof (StarTournamentEndedPopupNode_t08EC46F53416DF39D760B38B64472076A9670516), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8651[7] = 
{
	StarTournamentEndedPopupNode_t08EC46F53416DF39D760B38B64472076A9670516::get_offset_of__list_7(),
	StarTournamentEndedPopupNode_t08EC46F53416DF39D760B38B64472076A9670516::get_offset_of__claimBtn_8(),
	StarTournamentEndedPopupNode_t08EC46F53416DF39D760B38B64472076A9670516::get_offset_of__closeBtn_9(),
	StarTournamentEndedPopupNode_t08EC46F53416DF39D760B38B64472076A9670516::get_offset_of__rankText_10(),
	StarTournamentEndedPopupNode_t08EC46F53416DF39D760B38B64472076A9670516::get_offset_of__gameSpark_11(),
	StarTournamentEndedPopupNode_t08EC46F53416DF39D760B38B64472076A9670516::get_offset_of__userVO_12(),
	StarTournamentEndedPopupNode_t08EC46F53416DF39D760B38B64472076A9670516::get_offset_of__kickerManager_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8652 = { sizeof (StarTournamentJoinedPopupNode_t4BCAE122FEC6FDE5C033DCBBA2F83498444A39C8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8652[6] = 
{
	StarTournamentJoinedPopupNode_t4BCAE122FEC6FDE5C033DCBBA2F83498444A39C8::get_offset_of__list_7(),
	StarTournamentJoinedPopupNode_t4BCAE122FEC6FDE5C033DCBBA2F83498444A39C8::get_offset_of__closeBtn_8(),
	StarTournamentJoinedPopupNode_t4BCAE122FEC6FDE5C033DCBBA2F83498444A39C8::get_offset_of__gameSpark_9(),
	StarTournamentJoinedPopupNode_t4BCAE122FEC6FDE5C033DCBBA2F83498444A39C8::get_offset_of__lastTimeRefresh_10(),
	StarTournamentJoinedPopupNode_t4BCAE122FEC6FDE5C033DCBBA2F83498444A39C8::get_offset_of__items_11(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8653 = { sizeof (StarTournamentStartedPopupNode_t678998B84DDCA79F2FA54A02895AB025E05C21C4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8653[1] = 
{
	StarTournamentStartedPopupNode_t678998B84DDCA79F2FA54A02895AB025E05C21C4::get_offset_of__continueBtn_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8654 = { sizeof (StarTournamentSystem_tBBE982F84537BACA13B9A61826F04C1F5F726C88), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8654[9] = 
{
	StarTournamentSystem_tBBE982F84537BACA13B9A61826F04C1F5F726C88::get_offset_of__diContainer_3(),
	StarTournamentSystem_tBBE982F84537BACA13B9A61826F04C1F5F726C88::get_offset_of__tournamentsVO_4(),
	StarTournamentSystem_tBBE982F84537BACA13B9A61826F04C1F5F726C88::get_offset_of__startTrigger_5(),
	StarTournamentSystem_tBBE982F84537BACA13B9A61826F04C1F5F726C88::get_offset_of__endTrigger_6(),
	StarTournamentSystem_tBBE982F84537BACA13B9A61826F04C1F5F726C88::get_offset_of__starTournamentQuest_7(),
	StarTournamentSystem_tBBE982F84537BACA13B9A61826F04C1F5F726C88::get_offset_of__startVariable_8(),
	StarTournamentSystem_tBBE982F84537BACA13B9A61826F04C1F5F726C88::get_offset_of__currentTournament_9(),
	StarTournamentSystem_tBBE982F84537BACA13B9A61826F04C1F5F726C88::get_offset_of__started_10(),
	StarTournamentSystem_tBBE982F84537BACA13B9A61826F04C1F5F726C88::get_offset_of_OnTournamentStart_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8655 = { sizeof (StarTournamentRankVariable_tC180F6992EF9E03FF630834652B1B578945581BD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8655[1] = 
{
	StarTournamentRankVariable_tC180F6992EF9E03FF630834652B1B578945581BD::get_offset_of__tournamentVO_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8656 = { sizeof (StarTournamentVariable_tAEC9A6E89954C3E8E99380747371340583301A90), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8656[1] = 
{
	StarTournamentVariable_tAEC9A6E89954C3E8E99380747371340583301A90::get_offset_of__tournamentVO_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8657 = { sizeof (TribeTournamentComponent_t3FD8E187603AA1F1817918ABF0BFC7A176430C1E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8657[9] = 
{
	TribeTournamentComponent_t3FD8E187603AA1F1817918ABF0BFC7A176430C1E::get_offset_of__parent_4(),
	TribeTournamentComponent_t3FD8E187603AA1F1817918ABF0BFC7A176430C1E::get_offset_of__button_5(),
	TribeTournamentComponent_t3FD8E187603AA1F1817918ABF0BFC7A176430C1E::get_offset_of__timeCount_6(),
	TribeTournamentComponent_t3FD8E187603AA1F1817918ABF0BFC7A176430C1E::get_offset_of__uiMain_7(),
	TribeTournamentComponent_t3FD8E187603AA1F1817918ABF0BFC7A176430C1E::get_offset_of__timeSystem_8(),
	TribeTournamentComponent_t3FD8E187603AA1F1817918ABF0BFC7A176430C1E::get_offset_of__tournamentSystem_9(),
	TribeTournamentComponent_t3FD8E187603AA1F1817918ABF0BFC7A176430C1E::get_offset_of__kickerManager_10(),
	TribeTournamentComponent_t3FD8E187603AA1F1817918ABF0BFC7A176430C1E::get_offset_of__active_11(),
	TribeTournamentComponent_t3FD8E187603AA1F1817918ABF0BFC7A176430C1E::get_offset_of__endTime_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8658 = { sizeof (TribeTournamentContributionListItem_t9B763EB84E7C72F6EB1DE62F77CE1E1A6CB93E0A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8658[10] = 
{
	TribeTournamentContributionListItem_t9B763EB84E7C72F6EB1DE62F77CE1E1A6CB93E0A::get_offset_of__bgHighlight_4(),
	TribeTournamentContributionListItem_t9B763EB84E7C72F6EB1DE62F77CE1E1A6CB93E0A::get_offset_of__bgNormal_5(),
	TribeTournamentContributionListItem_t9B763EB84E7C72F6EB1DE62F77CE1E1A6CB93E0A::get_offset_of__firstRank_6(),
	TribeTournamentContributionListItem_t9B763EB84E7C72F6EB1DE62F77CE1E1A6CB93E0A::get_offset_of__secondRank_7(),
	TribeTournamentContributionListItem_t9B763EB84E7C72F6EB1DE62F77CE1E1A6CB93E0A::get_offset_of__thirdRank_8(),
	TribeTournamentContributionListItem_t9B763EB84E7C72F6EB1DE62F77CE1E1A6CB93E0A::get_offset_of__rankText_9(),
	TribeTournamentContributionListItem_t9B763EB84E7C72F6EB1DE62F77CE1E1A6CB93E0A::get_offset_of__starsScore_10(),
	TribeTournamentContributionListItem_t9B763EB84E7C72F6EB1DE62F77CE1E1A6CB93E0A::get_offset_of__playerName_11(),
	TribeTournamentContributionListItem_t9B763EB84E7C72F6EB1DE62F77CE1E1A6CB93E0A::get_offset_of__userVO_12(),
	TribeTournamentContributionListItem_t9B763EB84E7C72F6EB1DE62F77CE1E1A6CB93E0A::get_offset_of__data_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8659 = { sizeof (TribeTournamentContributionListItemData_tF9C29726809FAC898576A1F8CC49BDA2D376924F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8659[5] = 
{
	TribeTournamentContributionListItemData_tF9C29726809FAC898576A1F8CC49BDA2D376924F::get_offset_of_PlayerId_0(),
	TribeTournamentContributionListItemData_tF9C29726809FAC898576A1F8CC49BDA2D376924F::get_offset_of_PlayerName_1(),
	TribeTournamentContributionListItemData_tF9C29726809FAC898576A1F8CC49BDA2D376924F::get_offset_of_Stars_2(),
	TribeTournamentContributionListItemData_tF9C29726809FAC898576A1F8CC49BDA2D376924F::get_offset_of_Rank_3(),
	TribeTournamentContributionListItemData_tF9C29726809FAC898576A1F8CC49BDA2D376924F::get_offset_of_IsParticipant_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8660 = { sizeof (TribeTournamentLeaderboardListItem_tD1D05159EFE7AD9AFB45643D5C957ED11F5AC0FF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8660[14] = 
{
	TribeTournamentLeaderboardListItem_tD1D05159EFE7AD9AFB45643D5C957ED11F5AC0FF::get_offset_of__bgHighlight_4(),
	TribeTournamentLeaderboardListItem_tD1D05159EFE7AD9AFB45643D5C957ED11F5AC0FF::get_offset_of__bgNormal_5(),
	TribeTournamentLeaderboardListItem_tD1D05159EFE7AD9AFB45643D5C957ED11F5AC0FF::get_offset_of__firstRank_6(),
	TribeTournamentLeaderboardListItem_tD1D05159EFE7AD9AFB45643D5C957ED11F5AC0FF::get_offset_of__secondRank_7(),
	TribeTournamentLeaderboardListItem_tD1D05159EFE7AD9AFB45643D5C957ED11F5AC0FF::get_offset_of__thirdRank_8(),
	TribeTournamentLeaderboardListItem_tD1D05159EFE7AD9AFB45643D5C957ED11F5AC0FF::get_offset_of__rankText_9(),
	TribeTournamentLeaderboardListItem_tD1D05159EFE7AD9AFB45643D5C957ED11F5AC0FF::get_offset_of__teamIcon_10(),
	TribeTournamentLeaderboardListItem_tD1D05159EFE7AD9AFB45643D5C957ED11F5AC0FF::get_offset_of__teamName_11(),
	TribeTournamentLeaderboardListItem_tD1D05159EFE7AD9AFB45643D5C957ED11F5AC0FF::get_offset_of__starsScore_12(),
	TribeTournamentLeaderboardListItem_tD1D05159EFE7AD9AFB45643D5C957ED11F5AC0FF::get_offset_of__reward_13(),
	TribeTournamentLeaderboardListItem_tD1D05159EFE7AD9AFB45643D5C957ED11F5AC0FF::get_offset_of__viewTeamBtn_14(),
	TribeTournamentLeaderboardListItem_tD1D05159EFE7AD9AFB45643D5C957ED11F5AC0FF::get_offset_of__iconLibrary_15(),
	TribeTournamentLeaderboardListItem_tD1D05159EFE7AD9AFB45643D5C957ED11F5AC0FF::get_offset_of__userVO_16(),
	TribeTournamentLeaderboardListItem_tD1D05159EFE7AD9AFB45643D5C957ED11F5AC0FF::get_offset_of__data_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8661 = { sizeof (TribeTournamentListItemData_t972BE1E8BC71C51099D379602320348346EBEE14), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8661[5] = 
{
	TribeTournamentListItemData_t972BE1E8BC71C51099D379602320348346EBEE14::get_offset_of_Icon_0(),
	TribeTournamentListItemData_t972BE1E8BC71C51099D379602320348346EBEE14::get_offset_of_TeamName_1(),
	TribeTournamentListItemData_t972BE1E8BC71C51099D379602320348346EBEE14::get_offset_of_TeamId_2(),
	TribeTournamentListItemData_t972BE1E8BC71C51099D379602320348346EBEE14::get_offset_of_Stars_3(),
	TribeTournamentListItemData_t972BE1E8BC71C51099D379602320348346EBEE14::get_offset_of_Rank_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8662 = { sizeof (TribeTournamentEndedPopupNode_tD46D0CFDC9BAA5806C2FAF5B38D9542A0057DA8E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8662[15] = 
{
	TribeTournamentEndedPopupNode_tD46D0CFDC9BAA5806C2FAF5B38D9542A0057DA8E::get_offset_of__leaderboardList_7(),
	TribeTournamentEndedPopupNode_tD46D0CFDC9BAA5806C2FAF5B38D9542A0057DA8E::get_offset_of__contributionList_8(),
	TribeTournamentEndedPopupNode_tD46D0CFDC9BAA5806C2FAF5B38D9542A0057DA8E::get_offset_of__closeBtn_9(),
	TribeTournamentEndedPopupNode_tD46D0CFDC9BAA5806C2FAF5B38D9542A0057DA8E::get_offset_of__leaderboardBtn_10(),
	TribeTournamentEndedPopupNode_tD46D0CFDC9BAA5806C2FAF5B38D9542A0057DA8E::get_offset_of__contributionBtn_11(),
	TribeTournamentEndedPopupNode_tD46D0CFDC9BAA5806C2FAF5B38D9542A0057DA8E::get_offset_of__claimBtn_12(),
	TribeTournamentEndedPopupNode_tD46D0CFDC9BAA5806C2FAF5B38D9542A0057DA8E::get_offset_of__leaderboardOn_13(),
	TribeTournamentEndedPopupNode_tD46D0CFDC9BAA5806C2FAF5B38D9542A0057DA8E::get_offset_of__contributionOn_14(),
	TribeTournamentEndedPopupNode_tD46D0CFDC9BAA5806C2FAF5B38D9542A0057DA8E::get_offset_of__uiMain_15(),
	TribeTournamentEndedPopupNode_tD46D0CFDC9BAA5806C2FAF5B38D9542A0057DA8E::get_offset_of__gameSpark_16(),
	TribeTournamentEndedPopupNode_tD46D0CFDC9BAA5806C2FAF5B38D9542A0057DA8E::get_offset_of__kickerManager_17(),
	TribeTournamentEndedPopupNode_tD46D0CFDC9BAA5806C2FAF5B38D9542A0057DA8E::get_offset_of__userVO_18(),
	TribeTournamentEndedPopupNode_tD46D0CFDC9BAA5806C2FAF5B38D9542A0057DA8E::get_offset_of__leaderboardItems_19(),
	TribeTournamentEndedPopupNode_tD46D0CFDC9BAA5806C2FAF5B38D9542A0057DA8E::get_offset_of__contributionItems_20(),
	TribeTournamentEndedPopupNode_tD46D0CFDC9BAA5806C2FAF5B38D9542A0057DA8E::get_offset_of__seperatorPrefab_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8663 = { sizeof (TribeTournamentInfoPopupNode_t231D4A176DBC62E5A28FC935FD71BEF6D86DC719), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8663[2] = 
{
	TribeTournamentInfoPopupNode_t231D4A176DBC62E5A28FC935FD71BEF6D86DC719::get_offset_of__infoText_7(),
	TribeTournamentInfoPopupNode_t231D4A176DBC62E5A28FC935FD71BEF6D86DC719::get_offset_of__continueBtn_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8664 = { sizeof (TribeTournamentJoinedPopupNode_t4725067D973F39C6A3B2BD23D6C1A0BDF6F0B7F4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8664[14] = 
{
	TribeTournamentJoinedPopupNode_t4725067D973F39C6A3B2BD23D6C1A0BDF6F0B7F4::get_offset_of__leaderboardList_7(),
	TribeTournamentJoinedPopupNode_t4725067D973F39C6A3B2BD23D6C1A0BDF6F0B7F4::get_offset_of__contributionList_8(),
	TribeTournamentJoinedPopupNode_t4725067D973F39C6A3B2BD23D6C1A0BDF6F0B7F4::get_offset_of__closeBtn_9(),
	TribeTournamentJoinedPopupNode_t4725067D973F39C6A3B2BD23D6C1A0BDF6F0B7F4::get_offset_of__leaderboardBtn_10(),
	TribeTournamentJoinedPopupNode_t4725067D973F39C6A3B2BD23D6C1A0BDF6F0B7F4::get_offset_of__contributionBtn_11(),
	TribeTournamentJoinedPopupNode_t4725067D973F39C6A3B2BD23D6C1A0BDF6F0B7F4::get_offset_of__leaderboardOn_12(),
	TribeTournamentJoinedPopupNode_t4725067D973F39C6A3B2BD23D6C1A0BDF6F0B7F4::get_offset_of__contributionOn_13(),
	TribeTournamentJoinedPopupNode_t4725067D973F39C6A3B2BD23D6C1A0BDF6F0B7F4::get_offset_of__uiMain_14(),
	TribeTournamentJoinedPopupNode_t4725067D973F39C6A3B2BD23D6C1A0BDF6F0B7F4::get_offset_of__gameSpark_15(),
	TribeTournamentJoinedPopupNode_t4725067D973F39C6A3B2BD23D6C1A0BDF6F0B7F4::get_offset_of__lastTimeRefresh_16(),
	TribeTournamentJoinedPopupNode_t4725067D973F39C6A3B2BD23D6C1A0BDF6F0B7F4::get_offset_of__leaderboardItems_17(),
	TribeTournamentJoinedPopupNode_t4725067D973F39C6A3B2BD23D6C1A0BDF6F0B7F4::get_offset_of__contributionItems_18(),
	0,
	TribeTournamentJoinedPopupNode_t4725067D973F39C6A3B2BD23D6C1A0BDF6F0B7F4::get_offset_of__seperatorPrefab_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8665 = { sizeof (TribeTournamentStartedPopupNode_tA95B9DDB1ADAEEF5F7A876B2FB452474B44226B6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8665[1] = 
{
	TribeTournamentStartedPopupNode_tA95B9DDB1ADAEEF5F7A876B2FB452474B44226B6::get_offset_of__continueBtn_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8666 = { sizeof (TribeTournamentSystem_t7F0DE0115218A7B9AA81C8EF2B923A4450F36424), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8666[9] = 
{
	TribeTournamentSystem_t7F0DE0115218A7B9AA81C8EF2B923A4450F36424::get_offset_of__diContainer_3(),
	TribeTournamentSystem_t7F0DE0115218A7B9AA81C8EF2B923A4450F36424::get_offset_of__tournamentsVO_4(),
	TribeTournamentSystem_t7F0DE0115218A7B9AA81C8EF2B923A4450F36424::get_offset_of__startTrigger_5(),
	TribeTournamentSystem_t7F0DE0115218A7B9AA81C8EF2B923A4450F36424::get_offset_of__endTrigger_6(),
	TribeTournamentSystem_t7F0DE0115218A7B9AA81C8EF2B923A4450F36424::get_offset_of__starTournamentQuest_7(),
	TribeTournamentSystem_t7F0DE0115218A7B9AA81C8EF2B923A4450F36424::get_offset_of__tournamentVariable_8(),
	TribeTournamentSystem_t7F0DE0115218A7B9AA81C8EF2B923A4450F36424::get_offset_of__currentTournament_9(),
	TribeTournamentSystem_t7F0DE0115218A7B9AA81C8EF2B923A4450F36424::get_offset_of__started_10(),
	TribeTournamentSystem_t7F0DE0115218A7B9AA81C8EF2B923A4450F36424::get_offset_of_OnTournamentStart_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8667 = { sizeof (TribeTournamentVariable_t8978F616B72F4D039DC3E75EBB14E6F4A7D15300), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8667[1] = 
{
	TribeTournamentVariable_t8978F616B72F4D039DC3E75EBB14E6F4A7D15300::get_offset_of__tournamentVO_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8668 = { sizeof (BasicFlow_tED6559FAA2A08AD1986ABE4002984C2C018A1A56), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8669 = { sizeof (U3CShowU3Ed__0_tAA8320161EEB766929406A1BFEBCFE688EA702B4)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8669[2] = 
{
	U3CShowU3Ed__0_tAA8320161EEB766929406A1BFEBCFE688EA702B4::get_offset_of_U3CU3E1__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CShowU3Ed__0_tAA8320161EEB766929406A1BFEBCFE688EA702B4::get_offset_of_U3CU3Et__builder_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8670 = { sizeof (BasicTutorialFlow_tBA017E4C87F10BC8DD5BA4E75B90C9AFD0A3730B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8671 = { sizeof (U3CShowU3Ed__0_t0D1F0512B4F7EE67BE1F282E746AB38E196BE4D2)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8671[2] = 
{
	U3CShowU3Ed__0_t0D1F0512B4F7EE67BE1F282E746AB38E196BE4D2::get_offset_of_U3CU3E1__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CShowU3Ed__0_t0D1F0512B4F7EE67BE1F282E746AB38E196BE4D2::get_offset_of_U3CU3Et__builder_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8672 = { sizeof (BattleIntroFlow_t6B627D5733BE5B127F2EBBABF6747828F5F35D36), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8672[2] = 
{
	BattleIntroFlow_t6B627D5733BE5B127F2EBBABF6747828F5F35D36::get_offset_of__uiMain_0(),
	BattleIntroFlow_t6B627D5733BE5B127F2EBBABF6747828F5F35D36::get_offset_of__contexts_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8673 = { sizeof (U3CShowU3Ed__2_tE012D437EE48C917521209D67E5D70B984CF68E6)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8673[8] = 
{
	U3CShowU3Ed__2_tE012D437EE48C917521209D67E5D70B984CF68E6::get_offset_of_U3CU3E1__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CShowU3Ed__2_tE012D437EE48C917521209D67E5D70B984CF68E6::get_offset_of_U3CU3Et__builder_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CShowU3Ed__2_tE012D437EE48C917521209D67E5D70B984CF68E6::get_offset_of_U3CU3E4__this_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CShowU3Ed__2_tE012D437EE48C917521209D67E5D70B984CF68E6::get_offset_of_model_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CShowU3Ed__2_tE012D437EE48C917521209D67E5D70B984CF68E6::get_offset_of_U3CmovementNodeU3E5__2_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CShowU3Ed__2_tE012D437EE48C917521209D67E5D70B984CF68E6::get_offset_of_U3CdestroyNodeU3E5__3_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CShowU3Ed__2_tE012D437EE48C917521209D67E5D70B984CF68E6::get_offset_of_U3CgoalsNodeU3E5__4_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CShowU3Ed__2_tE012D437EE48C917521209D67E5D70B984CF68E6::get_offset_of_U3CU3Eu__1_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8674 = { sizeof (U3COnHeroMovesU3Ed__3_t4DAB8171E36E366B35731ED6BDF22878B8E407B5)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8674[6] = 
{
	U3COnHeroMovesU3Ed__3_t4DAB8171E36E366B35731ED6BDF22878B8E407B5::get_offset_of_U3CU3E1__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3COnHeroMovesU3Ed__3_t4DAB8171E36E366B35731ED6BDF22878B8E407B5::get_offset_of_U3CU3Et__builder_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3COnHeroMovesU3Ed__3_t4DAB8171E36E366B35731ED6BDF22878B8E407B5::get_offset_of_initPosition_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3COnHeroMovesU3Ed__3_t4DAB8171E36E366B35731ED6BDF22878B8E407B5::get_offset_of_hero_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3COnHeroMovesU3Ed__3_t4DAB8171E36E366B35731ED6BDF22878B8E407B5::get_offset_of_distance_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3COnHeroMovesU3Ed__3_t4DAB8171E36E366B35731ED6BDF22878B8E407B5::get_offset_of_U3CU3Eu__1_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8675 = { sizeof (U3COnDestroyU3Ed__4_t01BC33A675417028768FAC2621EDEA9F416AB467)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8675[7] = 
{
	U3COnDestroyU3Ed__4_t01BC33A675417028768FAC2621EDEA9F416AB467::get_offset_of_U3CU3E1__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3COnDestroyU3Ed__4_t01BC33A675417028768FAC2621EDEA9F416AB467::get_offset_of_U3CU3Et__builder_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3COnDestroyU3Ed__4_t01BC33A675417028768FAC2621EDEA9F416AB467::get_offset_of_U3CU3E4__this_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3COnDestroyU3Ed__4_t01BC33A675417028768FAC2621EDEA9F416AB467::get_offset_of_U3CentitiesU3E5__2_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3COnDestroyU3Ed__4_t01BC33A675417028768FAC2621EDEA9F416AB467::get_offset_of_U3CinitObjectivesU3E5__3_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3COnDestroyU3Ed__4_t01BC33A675417028768FAC2621EDEA9F416AB467::get_offset_of_U3CcurrentObjectivesU3E5__4_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3COnDestroyU3Ed__4_t01BC33A675417028768FAC2621EDEA9F416AB467::get_offset_of_U3CU3Eu__1_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8676 = { sizeof (BattleInventoryFlow_t7F82684619055127E49D19EEF227C08632E08E84), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8676[2] = 
{
	BattleInventoryFlow_t7F82684619055127E49D19EEF227C08632E08E84::get_offset_of__uiMain_0(),
	BattleInventoryFlow_t7F82684619055127E49D19EEF227C08632E08E84::get_offset_of__contexts_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8677 = { sizeof (U3CShowU3Ed__2_t65BDDF5FF6C6738F8F950BE6ABD6F758861C044F)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8677[7] = 
{
	U3CShowU3Ed__2_t65BDDF5FF6C6738F8F950BE6ABD6F758861C044F::get_offset_of_U3CU3E1__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CShowU3Ed__2_t65BDDF5FF6C6738F8F950BE6ABD6F758861C044F::get_offset_of_U3CU3Et__builder_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CShowU3Ed__2_t65BDDF5FF6C6738F8F950BE6ABD6F758861C044F::get_offset_of_U3CU3E4__this_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CShowU3Ed__2_t65BDDF5FF6C6738F8F950BE6ABD6F758861C044F::get_offset_of_model_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CShowU3Ed__2_t65BDDF5FF6C6738F8F950BE6ABD6F758861C044F::get_offset_of_U3CdestroyFiveNodeU3E5__2_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CShowU3Ed__2_t65BDDF5FF6C6738F8F950BE6ABD6F758861C044F::get_offset_of_U3CmatchFiveNodeU3E5__3_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CShowU3Ed__2_t65BDDF5FF6C6738F8F950BE6ABD6F758861C044F::get_offset_of_U3CU3Eu__1_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8678 = { sizeof (U3COnDestroyU3Ed__3_tE631C5A8B008EC66D79AB4E1E53B90AB2BAEC5A7)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8678[7] = 
{
	U3COnDestroyU3Ed__3_tE631C5A8B008EC66D79AB4E1E53B90AB2BAEC5A7::get_offset_of_U3CU3E1__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3COnDestroyU3Ed__3_tE631C5A8B008EC66D79AB4E1E53B90AB2BAEC5A7::get_offset_of_U3CU3Et__builder_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3COnDestroyU3Ed__3_tE631C5A8B008EC66D79AB4E1E53B90AB2BAEC5A7::get_offset_of_U3CU3E4__this_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3COnDestroyU3Ed__3_tE631C5A8B008EC66D79AB4E1E53B90AB2BAEC5A7::get_offset_of_U3CentitiesU3E5__2_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3COnDestroyU3Ed__3_tE631C5A8B008EC66D79AB4E1E53B90AB2BAEC5A7::get_offset_of_U3CinitObjectivesU3E5__3_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3COnDestroyU3Ed__3_tE631C5A8B008EC66D79AB4E1E53B90AB2BAEC5A7::get_offset_of_U3CcurrentObjectivesU3E5__4_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3COnDestroyU3Ed__3_tE631C5A8B008EC66D79AB4E1E53B90AB2BAEC5A7::get_offset_of_U3CU3Eu__1_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8679 = { sizeof (U3COnFiveDestroedU3Ed__4_t9B770EABE669404D730174009CE9ABAF5CB67D45)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8679[5] = 
{
	U3COnFiveDestroedU3Ed__4_t9B770EABE669404D730174009CE9ABAF5CB67D45::get_offset_of_U3CU3E1__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3COnFiveDestroedU3Ed__4_t9B770EABE669404D730174009CE9ABAF5CB67D45::get_offset_of_U3CU3Et__builder_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3COnFiveDestroedU3Ed__4_t9B770EABE669404D730174009CE9ABAF5CB67D45::get_offset_of_U3CU3E4__this_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3COnFiveDestroedU3Ed__4_t9B770EABE669404D730174009CE9ABAF5CB67D45::get_offset_of_U3CbattleInventoryU3E5__2_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3COnFiveDestroedU3Ed__4_t9B770EABE669404D730174009CE9ABAF5CB67D45::get_offset_of_U3CU3Eu__1_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8680 = { sizeof (CollectGoalFlow_tB876F66620038DC89241A21E6C080DA1B60BA94C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8680[3] = 
{
	CollectGoalFlow_tB876F66620038DC89241A21E6C080DA1B60BA94C::get_offset_of__uiMain_0(),
	CollectGoalFlow_tB876F66620038DC89241A21E6C080DA1B60BA94C::get_offset_of__contexts_1(),
	CollectGoalFlow_tB876F66620038DC89241A21E6C080DA1B60BA94C::get_offset_of__container_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8681 = { sizeof (U3CShowU3Ed__3_tD08670400D4E3FE215A266237C100F325F1C497B)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8681[6] = 
{
	U3CShowU3Ed__3_tD08670400D4E3FE215A266237C100F325F1C497B::get_offset_of_U3CU3E1__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CShowU3Ed__3_tD08670400D4E3FE215A266237C100F325F1C497B::get_offset_of_U3CU3Et__builder_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CShowU3Ed__3_tD08670400D4E3FE215A266237C100F325F1C497B::get_offset_of_U3CU3E4__this_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CShowU3Ed__3_tD08670400D4E3FE215A266237C100F325F1C497B::get_offset_of_model_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CShowU3Ed__3_tD08670400D4E3FE215A266237C100F325F1C497B::get_offset_of_U3CnodeU3E5__2_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CShowU3Ed__3_tD08670400D4E3FE215A266237C100F325F1C497B::get_offset_of_U3CU3Eu__1_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8682 = { sizeof (U3COnCollectU3Ed__4_t33C2C978285BB931AE4273B5FECE627316463282)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8682[5] = 
{
	U3COnCollectU3Ed__4_t33C2C978285BB931AE4273B5FECE627316463282::get_offset_of_U3CU3E1__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3COnCollectU3Ed__4_t33C2C978285BB931AE4273B5FECE627316463282::get_offset_of_U3CU3Et__builder_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3COnCollectU3Ed__4_t33C2C978285BB931AE4273B5FECE627316463282::get_offset_of_U3CU3E4__this_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3COnCollectU3Ed__4_t33C2C978285BB931AE4273B5FECE627316463282::get_offset_of_U3CheroU3E5__2_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3COnCollectU3Ed__4_t33C2C978285BB931AE4273B5FECE627316463282::get_offset_of_U3CU3Eu__1_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8683 = { sizeof (U3COnDeliverU3Ed__5_t270B044AF011A6B8129137B56EEA267F14883397)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8683[5] = 
{
	U3COnDeliverU3Ed__5_t270B044AF011A6B8129137B56EEA267F14883397::get_offset_of_U3CU3E1__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3COnDeliverU3Ed__5_t270B044AF011A6B8129137B56EEA267F14883397::get_offset_of_U3CU3Et__builder_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3COnDeliverU3Ed__5_t270B044AF011A6B8129137B56EEA267F14883397::get_offset_of_U3CU3E4__this_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3COnDeliverU3Ed__5_t270B044AF011A6B8129137B56EEA267F14883397::get_offset_of_U3CheroU3E5__2_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3COnDeliverU3Ed__5_t270B044AF011A6B8129137B56EEA267F14883397::get_offset_of_U3CU3Eu__1_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8684 = { sizeof (FirstSkillFlow_t178220FD212277F8C8E0CF2D00031184B612455F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8684[4] = 
{
	FirstSkillFlow_t178220FD212277F8C8E0CF2D00031184B612455F::get_offset_of__uiMain_0(),
	FirstSkillFlow_t178220FD212277F8C8E0CF2D00031184B612455F::get_offset_of__spellInitilizer_1(),
	FirstSkillFlow_t178220FD212277F8C8E0CF2D00031184B612455F::get_offset_of__contexts_2(),
	FirstSkillFlow_t178220FD212277F8C8E0CF2D00031184B612455F::get_offset_of__container_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8685 = { sizeof (U3CShowU3Ed__4_tB8228F66E8CFD8B544DBE8B10AA4C07D3EE41E4C)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8685[6] = 
{
	U3CShowU3Ed__4_tB8228F66E8CFD8B544DBE8B10AA4C07D3EE41E4C::get_offset_of_U3CU3E1__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CShowU3Ed__4_tB8228F66E8CFD8B544DBE8B10AA4C07D3EE41E4C::get_offset_of_U3CU3Et__builder_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CShowU3Ed__4_tB8228F66E8CFD8B544DBE8B10AA4C07D3EE41E4C::get_offset_of_U3CU3E4__this_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CShowU3Ed__4_tB8228F66E8CFD8B544DBE8B10AA4C07D3EE41E4C::get_offset_of_model_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CShowU3Ed__4_tB8228F66E8CFD8B544DBE8B10AA4C07D3EE41E4C::get_offset_of_U3CnodeU3E5__2_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CShowU3Ed__4_tB8228F66E8CFD8B544DBE8B10AA4C07D3EE41E4C::get_offset_of_U3CU3Eu__1_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8686 = { sizeof (FourthSkillFlow_t30FF5419C188236877763F85BAD84CE9339BBA91), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8686[3] = 
{
	FourthSkillFlow_t30FF5419C188236877763F85BAD84CE9339BBA91::get_offset_of__uiMain_0(),
	FourthSkillFlow_t30FF5419C188236877763F85BAD84CE9339BBA91::get_offset_of__spellInitilizer_1(),
	FourthSkillFlow_t30FF5419C188236877763F85BAD84CE9339BBA91::get_offset_of__contexts_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8687 = { sizeof (U3CShowU3Ed__3_tF195BCE7909E69C3A101B835AC7766233FF757B9)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8687[6] = 
{
	U3CShowU3Ed__3_tF195BCE7909E69C3A101B835AC7766233FF757B9::get_offset_of_U3CU3E1__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CShowU3Ed__3_tF195BCE7909E69C3A101B835AC7766233FF757B9::get_offset_of_U3CU3Et__builder_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CShowU3Ed__3_tF195BCE7909E69C3A101B835AC7766233FF757B9::get_offset_of_U3CU3E4__this_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CShowU3Ed__3_tF195BCE7909E69C3A101B835AC7766233FF757B9::get_offset_of_model_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CShowU3Ed__3_tF195BCE7909E69C3A101B835AC7766233FF757B9::get_offset_of_U3CnodeU3E5__2_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CShowU3Ed__3_tF195BCE7909E69C3A101B835AC7766233FF757B9::get_offset_of_U3CU3Eu__1_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8688 = { sizeof (U3COnScreenTapU3Ed__4_tE5A32BFDE6907918F920EEA58721B270250453B7)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8688[4] = 
{
	U3COnScreenTapU3Ed__4_tE5A32BFDE6907918F920EEA58721B270250453B7::get_offset_of_U3CU3E1__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3COnScreenTapU3Ed__4_tE5A32BFDE6907918F920EEA58721B270250453B7::get_offset_of_U3CU3Et__builder_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3COnScreenTapU3Ed__4_tE5A32BFDE6907918F920EEA58721B270250453B7::get_offset_of_U3CU3E4__this_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3COnScreenTapU3Ed__4_tE5A32BFDE6907918F920EEA58721B270250453B7::get_offset_of_U3CU3Eu__1_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8689 = { sizeof (U3COnMouseUpU3Ed__5_tF0C36A445CE9B38F7EEC3B196771FCE07470201E)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8689[3] = 
{
	U3COnMouseUpU3Ed__5_tF0C36A445CE9B38F7EEC3B196771FCE07470201E::get_offset_of_U3CU3E1__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3COnMouseUpU3Ed__5_tF0C36A445CE9B38F7EEC3B196771FCE07470201E::get_offset_of_U3CU3Et__builder_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3COnMouseUpU3Ed__5_tF0C36A445CE9B38F7EEC3B196771FCE07470201E::get_offset_of_U3CU3Eu__1_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8690 = { sizeof (SecondSkillFlow_t56C78A79BCE637FB296A312521CD8D3C5FE89165), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8690[2] = 
{
	SecondSkillFlow_t56C78A79BCE637FB296A312521CD8D3C5FE89165::get_offset_of__uiMain_0(),
	SecondSkillFlow_t56C78A79BCE637FB296A312521CD8D3C5FE89165::get_offset_of__spellInitilizer_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8691 = { sizeof (U3CShowU3Ed__2_tEF5B69CA7AF0F99DB5776D7FAA552FF57CC9BEBF)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8691[6] = 
{
	U3CShowU3Ed__2_tEF5B69CA7AF0F99DB5776D7FAA552FF57CC9BEBF::get_offset_of_U3CU3E1__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CShowU3Ed__2_tEF5B69CA7AF0F99DB5776D7FAA552FF57CC9BEBF::get_offset_of_U3CU3Et__builder_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CShowU3Ed__2_tEF5B69CA7AF0F99DB5776D7FAA552FF57CC9BEBF::get_offset_of_U3CU3E4__this_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CShowU3Ed__2_tEF5B69CA7AF0F99DB5776D7FAA552FF57CC9BEBF::get_offset_of_model_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CShowU3Ed__2_tEF5B69CA7AF0F99DB5776D7FAA552FF57CC9BEBF::get_offset_of_U3CnodeU3E5__2_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CShowU3Ed__2_tEF5B69CA7AF0F99DB5776D7FAA552FF57CC9BEBF::get_offset_of_U3CU3Eu__1_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8692 = { sizeof (ThirdSkillFlow_t99B9A8894864D1DBED1774C522267C4A60B76652), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8692[2] = 
{
	ThirdSkillFlow_t99B9A8894864D1DBED1774C522267C4A60B76652::get_offset_of__uiMain_0(),
	ThirdSkillFlow_t99B9A8894864D1DBED1774C522267C4A60B76652::get_offset_of__spellInitilizer_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8693 = { sizeof (U3CShowU3Ed__2_t125B8D889373F6DBC4587B03503162C8D4A6F3F3)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8693[6] = 
{
	U3CShowU3Ed__2_t125B8D889373F6DBC4587B03503162C8D4A6F3F3::get_offset_of_U3CU3E1__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CShowU3Ed__2_t125B8D889373F6DBC4587B03503162C8D4A6F3F3::get_offset_of_U3CU3Et__builder_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CShowU3Ed__2_t125B8D889373F6DBC4587B03503162C8D4A6F3F3::get_offset_of_U3CU3E4__this_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CShowU3Ed__2_t125B8D889373F6DBC4587B03503162C8D4A6F3F3::get_offset_of_model_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CShowU3Ed__2_t125B8D889373F6DBC4587B03503162C8D4A6F3F3::get_offset_of_U3CnodeU3E5__2_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CShowU3Ed__2_t125B8D889373F6DBC4587B03503162C8D4A6F3F3::get_offset_of_U3CU3Eu__1_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8694 = { sizeof (TutorialUtils_t7425E0BFABD0CF5606ECFC8C2BA50802577072F5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8695 = { sizeof (MessagesUtils_t8378B9BBF4CB32A0949C62B26744A1E851769C58), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8696 = { sizeof (MessageType_t07EE7B9773D6279718FDDBAE71930B611365858F)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable8696[5] = 
{
	MessageType_t07EE7B9773D6279718FDDBAE71930B611365858F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8697 = { sizeof (LoginCallback_t59A2D9666572CF04C083B1D3DCBF89AE9F916856), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8698 = { sizeof (DisplayNameCallback_t31292D7DED26E8CC661875D2DC7808003C4A0C4D), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8699 = { sizeof (LeaderboardCallback_t4F0DB136BE2987B04C4987B870819756C02516B0), sizeof(Il2CppMethodPointer), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
