﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.DiContainer/ProviderInfo>>
struct Dictionary_2_t7C592452CF60789D3B03C452382E134D587ABADC;
// System.Collections.Generic.HashSet`1<System.Object>
struct HashSet_1_t297CD7F944846107B388993164FCD9E317A338A3;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_t31EF1520A3A805598500BB6033C14ABDA7116D5E;
// System.Collections.Generic.IEnumerable`1<Zenject.DiContainer>
struct IEnumerable_1_tF49BC792153BEBAC6DB7A25673D94710BE5F0056;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.List`1<System.Object>>
struct IEnumerator_1_t883C7A6A7B96519C2EFA6500ED941131CCF16129;
// System.Collections.Generic.IEnumerator`1<UnityEngine.GameObject>
struct IEnumerator_1_t17C77FE93B057D368EC24057B5FD86F1543E35F4;
// System.Collections.Generic.IEnumerator`1<Zenject.InjectableInfo>
struct IEnumerator_1_t356D0A3704259DF7911E6967DB6B4CC8E2849DA7;
// System.Collections.Generic.List`1<<>f__AnonymousType0`2<Zenject.DiContainer/ProviderPair,System.Int32>>
struct List_1_t96D85E616B8F1D63BF7EC3A7828B462B9A182456;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D;
// System.Collections.Generic.List`1<System.Type>
struct List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0;
// System.Collections.Generic.List`1<UnityEngine.MonoBehaviour>
struct List_1_tA826461A2C9E99A4825BFEC4BE4DBF2F1A142DA7;
// System.Collections.Generic.List`1<Zenject.DiContainer>
struct List_1_tC0C0C06645444D1ED985242DB086B9AFC1D8E1FA;
// System.Collections.Generic.List`1<Zenject.IBindingFinalizer>
struct List_1_t260A5990EE904C05AE0C7D28243F87BFA487442A;
// System.Collections.Generic.List`1<Zenject.ILazy>
struct List_1_t4B97E348520D143EA8C892D9014012A434C2A126;
// System.Collections.Generic.List`1<Zenject.InstallerBase>
struct List_1_t0579372F26330F2A4603BEBDDF54A2BF6791CF8B;
// System.Collections.Generic.List`1<Zenject.MonoInstaller>
struct List_1_tCF773BC8FB84889055B7C7EDDE5C5D83FA2D4F08;
// System.Collections.Generic.List`1<Zenject.ScriptableObjectInstaller>
struct List_1_t8B49C95109DF1CE321982E768E438471621D0733;
// System.Collections.Generic.List`1<Zenject.TypeValuePair>
struct List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69;
// System.Collections.Generic.Queue`1<Zenject.IBindingFinalizer>
struct Queue_1_tB4CDAB25704B2A2FE785C4404A82F5B755C20BD9;
// System.Collections.Generic.Stack`1<Zenject.DiContainer/LookupId>
struct Stack_1_t30B30BF8B50F476BF18BDC1667E81F8EE0045E48;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Func`2<<>f__AnonymousType0`2<Zenject.DiContainer/ProviderPair,System.Int32>,System.Boolean>
struct Func_2_t54C395B226509C9A26E591AACA3A9C9BB85CD7B1;
// System.Func`2<<>f__AnonymousType0`2<Zenject.DiContainer/ProviderPair,System.Int32>,System.Int32>
struct Func_2_tA59CD5991CE1D5EFC471FB54024E70E6DC7710ED;
// System.Func`2<<>f__AnonymousType0`2<Zenject.DiContainer/ProviderPair,System.Int32>,Zenject.DiContainer/ProviderPair>
struct Func_2_tC09774CCB9423FDD7EFDC04BDFBAAD3396C85E9B;
// System.Func`2<System.String,System.Boolean>
struct Func_2_t3AD4B0F443BFD399C4AC2D6EE99FFE3BC0970017;
// System.Func`2<System.Type,System.Boolean>
struct Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9;
// System.Func`2<UnityEngine.GameObject,System.Collections.Generic.IEnumerable`1<Zenject.SceneContext>>
struct Func_2_t6D143D5B850346EA82D99E2A69981B807B171CF6;
// System.Func`2<UnityEngine.GameObject,System.Collections.Generic.IEnumerable`1<Zenject.SceneDecoratorContext>>
struct Func_2_t9999430700062AC839370FE209CB4F33E8A3A67B;
// System.Func`2<UnityEngine.SceneManagement.Scene,System.Collections.Generic.IEnumerable`1<UnityEngine.GameObject>>
struct Func_2_tFAFA393FCFA2B792ED7E14FB5E6990E11CBBAF46;
// System.Func`2<Zenject.DiContainer,System.Collections.Generic.IEnumerable`1<Zenject.IBindingFinalizer>>
struct Func_2_t54CC118F1E2AA7F5B40EDF23AE63CACDDE532C98;
// System.Func`2<Zenject.DiContainer/ProviderPair,Zenject.IProvider>
struct Func_2_t21BBB65AA54D3E1A113E578E8D62667B7ADD006F;
// System.Func`2<Zenject.InjectContext,UnityEngine.GameObject>
struct Func_2_tD5D4A9F4A7BCFBB67D83DD4F52BFE19A8153D2D1;
// System.Func`2<Zenject.SceneContext,Zenject.DiContainer>
struct Func_2_tD6FA67F8E051BBD2664B65E112F6CA7FB16B04E9;
// System.Func`2<Zenject.TypeValuePair,System.String>
struct Func_2_t1C48021AB2F773B7BE03E0871A032086737F7698;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.Component[]
struct ComponentU5BU5D_t7BE50AFB6301C06D990819B3D8F35CA326CDD155;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// Zenject.AddToCurrentGameObjectComponentProvider
struct AddToCurrentGameObjectComponentProvider_t9CCD8774C1BA2207D55CE64479BD916A57DF069E;
// Zenject.AddToGameObjectComponentProviderBase
struct AddToGameObjectComponentProviderBase_tACB741545D819318E619C706D60A87C16EAC5434;
// Zenject.BindingCondition
struct BindingCondition_t3BC89ABD74DA160C3396036CE0360DE414B4ADD8;
// Zenject.BindingId
struct BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA;
// Zenject.CachedProvider
struct CachedProvider_t0DD0B25EA381638476E1E4EC2FC32C8CDA59BA36;
// Zenject.Context
struct Context_t04459FF3A5B7E8AF25B965F5717F535AE8FA6DBA;
// Zenject.DiContainer
struct DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5;
// Zenject.DiContainer/ProviderInfo
struct ProviderInfo_t038F6812DFCFCD306479198F3E14CCB57B026C86;
// Zenject.EmptyGameObjectProvider
struct EmptyGameObjectProvider_t99FDC7D0A141A3CB7F442969996294D2F3B749FC;
// Zenject.GameObjectCreationParameters
struct GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A;
// Zenject.GetFromPrefabComponentProvider
struct GetFromPrefabComponentProvider_t42F7CECCDF7930D6C1C6F1397452ABE68BADBDF6;
// Zenject.IPrefabInstantiator
struct IPrefabInstantiator_t7DDD0F9644A546CC27417C93BB82202E1A425F8C;
// Zenject.IProvider
struct IProvider_t26B24AA2351CC93A1621F3C57B5BE5CFC4624394;
// Zenject.InjectContext
struct InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF;
// Zenject.InstantiateOnPrefabComponentProvider
struct InstantiateOnPrefabComponentProvider_t1445E68D6F2B17287C0FDE478F5E751A3FBBDF78;
// Zenject.LazyInstanceInjector
struct LazyInstanceInjector_t0120534A9142C2F3AC0A2A87E5CAC778A6F831A6;
// Zenject.PrefabGameObjectProvider
struct PrefabGameObjectProvider_t194304A9DD65D5EF02263FE908B286D92CE4D8C3;
// Zenject.SingletonMarkRegistry
struct SingletonMarkRegistry_tB0ADDC9314F22EFEF3C51F08F71E6E1CB236EC7F;
// Zenject.SingletonProviderCreator
struct SingletonProviderCreator_t07218AEFEEBFF6A8B65FB17100F1AB292A94F5AD;

struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef ADDTOCURRENTGAMEOBJECTCOMPONENTPROVIDER_T9CCD8774C1BA2207D55CE64479BD916A57DF069E_H
#define ADDTOCURRENTGAMEOBJECTCOMPONENTPROVIDER_T9CCD8774C1BA2207D55CE64479BD916A57DF069E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.AddToCurrentGameObjectComponentProvider
struct  AddToCurrentGameObjectComponentProvider_t9CCD8774C1BA2207D55CE64479BD916A57DF069E  : public RuntimeObject
{
public:
	// System.Object Zenject.AddToCurrentGameObjectComponentProvider::_concreteIdentifier
	RuntimeObject * ____concreteIdentifier_0;
	// System.Type Zenject.AddToCurrentGameObjectComponentProvider::_componentType
	Type_t * ____componentType_1;
	// Zenject.DiContainer Zenject.AddToCurrentGameObjectComponentProvider::_container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____container_2;
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.AddToCurrentGameObjectComponentProvider::_extraArguments
	List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * ____extraArguments_3;

public:
	inline static int32_t get_offset_of__concreteIdentifier_0() { return static_cast<int32_t>(offsetof(AddToCurrentGameObjectComponentProvider_t9CCD8774C1BA2207D55CE64479BD916A57DF069E, ____concreteIdentifier_0)); }
	inline RuntimeObject * get__concreteIdentifier_0() const { return ____concreteIdentifier_0; }
	inline RuntimeObject ** get_address_of__concreteIdentifier_0() { return &____concreteIdentifier_0; }
	inline void set__concreteIdentifier_0(RuntimeObject * value)
	{
		____concreteIdentifier_0 = value;
		Il2CppCodeGenWriteBarrier((&____concreteIdentifier_0), value);
	}

	inline static int32_t get_offset_of__componentType_1() { return static_cast<int32_t>(offsetof(AddToCurrentGameObjectComponentProvider_t9CCD8774C1BA2207D55CE64479BD916A57DF069E, ____componentType_1)); }
	inline Type_t * get__componentType_1() const { return ____componentType_1; }
	inline Type_t ** get_address_of__componentType_1() { return &____componentType_1; }
	inline void set__componentType_1(Type_t * value)
	{
		____componentType_1 = value;
		Il2CppCodeGenWriteBarrier((&____componentType_1), value);
	}

	inline static int32_t get_offset_of__container_2() { return static_cast<int32_t>(offsetof(AddToCurrentGameObjectComponentProvider_t9CCD8774C1BA2207D55CE64479BD916A57DF069E, ____container_2)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__container_2() const { return ____container_2; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__container_2() { return &____container_2; }
	inline void set__container_2(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____container_2 = value;
		Il2CppCodeGenWriteBarrier((&____container_2), value);
	}

	inline static int32_t get_offset_of__extraArguments_3() { return static_cast<int32_t>(offsetof(AddToCurrentGameObjectComponentProvider_t9CCD8774C1BA2207D55CE64479BD916A57DF069E, ____extraArguments_3)); }
	inline List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * get__extraArguments_3() const { return ____extraArguments_3; }
	inline List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 ** get_address_of__extraArguments_3() { return &____extraArguments_3; }
	inline void set__extraArguments_3(List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * value)
	{
		____extraArguments_3 = value;
		Il2CppCodeGenWriteBarrier((&____extraArguments_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADDTOCURRENTGAMEOBJECTCOMPONENTPROVIDER_T9CCD8774C1BA2207D55CE64479BD916A57DF069E_H
#ifndef U3CGETALLINSTANCESWITHINJECTSPLITU3ED__12_T407DF40E51448E8E78815FC3FA70E960A41941FC_H
#define U3CGETALLINSTANCESWITHINJECTSPLITU3ED__12_T407DF40E51448E8E78815FC3FA70E960A41941FC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.AddToCurrentGameObjectComponentProvider_<GetAllInstancesWithInjectSplit>d__12
struct  U3CGetAllInstancesWithInjectSplitU3Ed__12_t407DF40E51448E8E78815FC3FA70E960A41941FC  : public RuntimeObject
{
public:
	// System.Int32 Zenject.AddToCurrentGameObjectComponentProvider_<GetAllInstancesWithInjectSplit>d__12::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Collections.Generic.List`1<System.Object> Zenject.AddToCurrentGameObjectComponentProvider_<GetAllInstancesWithInjectSplit>d__12::<>2__current
	List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * ___U3CU3E2__current_1;
	// Zenject.InjectContext Zenject.AddToCurrentGameObjectComponentProvider_<GetAllInstancesWithInjectSplit>d__12::context
	InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * ___context_2;
	// Zenject.AddToCurrentGameObjectComponentProvider Zenject.AddToCurrentGameObjectComponentProvider_<GetAllInstancesWithInjectSplit>d__12::<>4__this
	AddToCurrentGameObjectComponentProvider_t9CCD8774C1BA2207D55CE64479BD916A57DF069E * ___U3CU3E4__this_3;
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.AddToCurrentGameObjectComponentProvider_<GetAllInstancesWithInjectSplit>d__12::args
	List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * ___args_4;
	// System.Object Zenject.AddToCurrentGameObjectComponentProvider_<GetAllInstancesWithInjectSplit>d__12::<instance>5__2
	RuntimeObject * ___U3CinstanceU3E5__2_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__12_t407DF40E51448E8E78815FC3FA70E960A41941FC, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__12_t407DF40E51448E8E78815FC3FA70E960A41941FC, ___U3CU3E2__current_1)); }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_context_2() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__12_t407DF40E51448E8E78815FC3FA70E960A41941FC, ___context_2)); }
	inline InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * get_context_2() const { return ___context_2; }
	inline InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF ** get_address_of_context_2() { return &___context_2; }
	inline void set_context_2(InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * value)
	{
		___context_2 = value;
		Il2CppCodeGenWriteBarrier((&___context_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__12_t407DF40E51448E8E78815FC3FA70E960A41941FC, ___U3CU3E4__this_3)); }
	inline AddToCurrentGameObjectComponentProvider_t9CCD8774C1BA2207D55CE64479BD916A57DF069E * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline AddToCurrentGameObjectComponentProvider_t9CCD8774C1BA2207D55CE64479BD916A57DF069E ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(AddToCurrentGameObjectComponentProvider_t9CCD8774C1BA2207D55CE64479BD916A57DF069E * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_args_4() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__12_t407DF40E51448E8E78815FC3FA70E960A41941FC, ___args_4)); }
	inline List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * get_args_4() const { return ___args_4; }
	inline List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 ** get_address_of_args_4() { return &___args_4; }
	inline void set_args_4(List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * value)
	{
		___args_4 = value;
		Il2CppCodeGenWriteBarrier((&___args_4), value);
	}

	inline static int32_t get_offset_of_U3CinstanceU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__12_t407DF40E51448E8E78815FC3FA70E960A41941FC, ___U3CinstanceU3E5__2_5)); }
	inline RuntimeObject * get_U3CinstanceU3E5__2_5() const { return ___U3CinstanceU3E5__2_5; }
	inline RuntimeObject ** get_address_of_U3CinstanceU3E5__2_5() { return &___U3CinstanceU3E5__2_5; }
	inline void set_U3CinstanceU3E5__2_5(RuntimeObject * value)
	{
		___U3CinstanceU3E5__2_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CinstanceU3E5__2_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETALLINSTANCESWITHINJECTSPLITU3ED__12_T407DF40E51448E8E78815FC3FA70E960A41941FC_H
#ifndef ADDTOGAMEOBJECTCOMPONENTPROVIDERBASE_TACB741545D819318E619C706D60A87C16EAC5434_H
#define ADDTOGAMEOBJECTCOMPONENTPROVIDERBASE_TACB741545D819318E619C706D60A87C16EAC5434_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.AddToGameObjectComponentProviderBase
struct  AddToGameObjectComponentProviderBase_tACB741545D819318E619C706D60A87C16EAC5434  : public RuntimeObject
{
public:
	// System.Object Zenject.AddToGameObjectComponentProviderBase::_concreteIdentifier
	RuntimeObject * ____concreteIdentifier_0;
	// System.Type Zenject.AddToGameObjectComponentProviderBase::_componentType
	Type_t * ____componentType_1;
	// Zenject.DiContainer Zenject.AddToGameObjectComponentProviderBase::_container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____container_2;
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.AddToGameObjectComponentProviderBase::_extraArguments
	List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * ____extraArguments_3;

public:
	inline static int32_t get_offset_of__concreteIdentifier_0() { return static_cast<int32_t>(offsetof(AddToGameObjectComponentProviderBase_tACB741545D819318E619C706D60A87C16EAC5434, ____concreteIdentifier_0)); }
	inline RuntimeObject * get__concreteIdentifier_0() const { return ____concreteIdentifier_0; }
	inline RuntimeObject ** get_address_of__concreteIdentifier_0() { return &____concreteIdentifier_0; }
	inline void set__concreteIdentifier_0(RuntimeObject * value)
	{
		____concreteIdentifier_0 = value;
		Il2CppCodeGenWriteBarrier((&____concreteIdentifier_0), value);
	}

	inline static int32_t get_offset_of__componentType_1() { return static_cast<int32_t>(offsetof(AddToGameObjectComponentProviderBase_tACB741545D819318E619C706D60A87C16EAC5434, ____componentType_1)); }
	inline Type_t * get__componentType_1() const { return ____componentType_1; }
	inline Type_t ** get_address_of__componentType_1() { return &____componentType_1; }
	inline void set__componentType_1(Type_t * value)
	{
		____componentType_1 = value;
		Il2CppCodeGenWriteBarrier((&____componentType_1), value);
	}

	inline static int32_t get_offset_of__container_2() { return static_cast<int32_t>(offsetof(AddToGameObjectComponentProviderBase_tACB741545D819318E619C706D60A87C16EAC5434, ____container_2)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__container_2() const { return ____container_2; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__container_2() { return &____container_2; }
	inline void set__container_2(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____container_2 = value;
		Il2CppCodeGenWriteBarrier((&____container_2), value);
	}

	inline static int32_t get_offset_of__extraArguments_3() { return static_cast<int32_t>(offsetof(AddToGameObjectComponentProviderBase_tACB741545D819318E619C706D60A87C16EAC5434, ____extraArguments_3)); }
	inline List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * get__extraArguments_3() const { return ____extraArguments_3; }
	inline List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 ** get_address_of__extraArguments_3() { return &____extraArguments_3; }
	inline void set__extraArguments_3(List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * value)
	{
		____extraArguments_3 = value;
		Il2CppCodeGenWriteBarrier((&____extraArguments_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADDTOGAMEOBJECTCOMPONENTPROVIDERBASE_TACB741545D819318E619C706D60A87C16EAC5434_H
#ifndef U3CGETALLINSTANCESWITHINJECTSPLITU3ED__14_TEF5DBD5A6E175503235B0119D5E5360A5020BF8A_H
#define U3CGETALLINSTANCESWITHINJECTSPLITU3ED__14_TEF5DBD5A6E175503235B0119D5E5360A5020BF8A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.AddToGameObjectComponentProviderBase_<GetAllInstancesWithInjectSplit>d__14
struct  U3CGetAllInstancesWithInjectSplitU3Ed__14_tEF5DBD5A6E175503235B0119D5E5360A5020BF8A  : public RuntimeObject
{
public:
	// System.Int32 Zenject.AddToGameObjectComponentProviderBase_<GetAllInstancesWithInjectSplit>d__14::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Collections.Generic.List`1<System.Object> Zenject.AddToGameObjectComponentProviderBase_<GetAllInstancesWithInjectSplit>d__14::<>2__current
	List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * ___U3CU3E2__current_1;
	// Zenject.InjectContext Zenject.AddToGameObjectComponentProviderBase_<GetAllInstancesWithInjectSplit>d__14::context
	InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * ___context_2;
	// Zenject.AddToGameObjectComponentProviderBase Zenject.AddToGameObjectComponentProviderBase_<GetAllInstancesWithInjectSplit>d__14::<>4__this
	AddToGameObjectComponentProviderBase_tACB741545D819318E619C706D60A87C16EAC5434 * ___U3CU3E4__this_3;
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.AddToGameObjectComponentProviderBase_<GetAllInstancesWithInjectSplit>d__14::args
	List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * ___args_4;
	// System.Object Zenject.AddToGameObjectComponentProviderBase_<GetAllInstancesWithInjectSplit>d__14::<instance>5__2
	RuntimeObject * ___U3CinstanceU3E5__2_5;
	// UnityEngine.GameObject Zenject.AddToGameObjectComponentProviderBase_<GetAllInstancesWithInjectSplit>d__14::<gameObj>5__3
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___U3CgameObjU3E5__3_6;
	// System.Boolean Zenject.AddToGameObjectComponentProviderBase_<GetAllInstancesWithInjectSplit>d__14::<wasActive>5__4
	bool ___U3CwasActiveU3E5__4_7;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__14_tEF5DBD5A6E175503235B0119D5E5360A5020BF8A, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__14_tEF5DBD5A6E175503235B0119D5E5360A5020BF8A, ___U3CU3E2__current_1)); }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_context_2() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__14_tEF5DBD5A6E175503235B0119D5E5360A5020BF8A, ___context_2)); }
	inline InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * get_context_2() const { return ___context_2; }
	inline InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF ** get_address_of_context_2() { return &___context_2; }
	inline void set_context_2(InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * value)
	{
		___context_2 = value;
		Il2CppCodeGenWriteBarrier((&___context_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__14_tEF5DBD5A6E175503235B0119D5E5360A5020BF8A, ___U3CU3E4__this_3)); }
	inline AddToGameObjectComponentProviderBase_tACB741545D819318E619C706D60A87C16EAC5434 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline AddToGameObjectComponentProviderBase_tACB741545D819318E619C706D60A87C16EAC5434 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(AddToGameObjectComponentProviderBase_tACB741545D819318E619C706D60A87C16EAC5434 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_args_4() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__14_tEF5DBD5A6E175503235B0119D5E5360A5020BF8A, ___args_4)); }
	inline List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * get_args_4() const { return ___args_4; }
	inline List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 ** get_address_of_args_4() { return &___args_4; }
	inline void set_args_4(List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * value)
	{
		___args_4 = value;
		Il2CppCodeGenWriteBarrier((&___args_4), value);
	}

	inline static int32_t get_offset_of_U3CinstanceU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__14_tEF5DBD5A6E175503235B0119D5E5360A5020BF8A, ___U3CinstanceU3E5__2_5)); }
	inline RuntimeObject * get_U3CinstanceU3E5__2_5() const { return ___U3CinstanceU3E5__2_5; }
	inline RuntimeObject ** get_address_of_U3CinstanceU3E5__2_5() { return &___U3CinstanceU3E5__2_5; }
	inline void set_U3CinstanceU3E5__2_5(RuntimeObject * value)
	{
		___U3CinstanceU3E5__2_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CinstanceU3E5__2_5), value);
	}

	inline static int32_t get_offset_of_U3CgameObjU3E5__3_6() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__14_tEF5DBD5A6E175503235B0119D5E5360A5020BF8A, ___U3CgameObjU3E5__3_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_U3CgameObjU3E5__3_6() const { return ___U3CgameObjU3E5__3_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_U3CgameObjU3E5__3_6() { return &___U3CgameObjU3E5__3_6; }
	inline void set_U3CgameObjU3E5__3_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___U3CgameObjU3E5__3_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CgameObjU3E5__3_6), value);
	}

	inline static int32_t get_offset_of_U3CwasActiveU3E5__4_7() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__14_tEF5DBD5A6E175503235B0119D5E5360A5020BF8A, ___U3CwasActiveU3E5__4_7)); }
	inline bool get_U3CwasActiveU3E5__4_7() const { return ___U3CwasActiveU3E5__4_7; }
	inline bool* get_address_of_U3CwasActiveU3E5__4_7() { return &___U3CwasActiveU3E5__4_7; }
	inline void set_U3CwasActiveU3E5__4_7(bool value)
	{
		___U3CwasActiveU3E5__4_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETALLINSTANCESWITHINJECTSPLITU3ED__14_TEF5DBD5A6E175503235B0119D5E5360A5020BF8A_H
#ifndef BINDINGID_TDE4E18829D06364806DC2F3F04F0574D68DF92BA_H
#define BINDINGID_TDE4E18829D06364806DC2F3F04F0574D68DF92BA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.BindingId
struct  BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA  : public RuntimeObject
{
public:
	// System.Type Zenject.BindingId::Type
	Type_t * ___Type_0;
	// System.Object Zenject.BindingId::Identifier
	RuntimeObject * ___Identifier_1;

public:
	inline static int32_t get_offset_of_Type_0() { return static_cast<int32_t>(offsetof(BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA, ___Type_0)); }
	inline Type_t * get_Type_0() const { return ___Type_0; }
	inline Type_t ** get_address_of_Type_0() { return &___Type_0; }
	inline void set_Type_0(Type_t * value)
	{
		___Type_0 = value;
		Il2CppCodeGenWriteBarrier((&___Type_0), value);
	}

	inline static int32_t get_offset_of_Identifier_1() { return static_cast<int32_t>(offsetof(BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA, ___Identifier_1)); }
	inline RuntimeObject * get_Identifier_1() const { return ___Identifier_1; }
	inline RuntimeObject ** get_address_of_Identifier_1() { return &___Identifier_1; }
	inline void set_Identifier_1(RuntimeObject * value)
	{
		___Identifier_1 = value;
		Il2CppCodeGenWriteBarrier((&___Identifier_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGID_TDE4E18829D06364806DC2F3F04F0574D68DF92BA_H
#ifndef CACHEDPROVIDER_T0DD0B25EA381638476E1E4EC2FC32C8CDA59BA36_H
#define CACHEDPROVIDER_T0DD0B25EA381638476E1E4EC2FC32C8CDA59BA36_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.CachedProvider
struct  CachedProvider_t0DD0B25EA381638476E1E4EC2FC32C8CDA59BA36  : public RuntimeObject
{
public:
	// Zenject.IProvider Zenject.CachedProvider::_creator
	RuntimeObject* ____creator_0;
	// System.Collections.Generic.List`1<System.Object> Zenject.CachedProvider::_instances
	List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * ____instances_1;
	// System.Boolean Zenject.CachedProvider::_isCreatingInstance
	bool ____isCreatingInstance_2;

public:
	inline static int32_t get_offset_of__creator_0() { return static_cast<int32_t>(offsetof(CachedProvider_t0DD0B25EA381638476E1E4EC2FC32C8CDA59BA36, ____creator_0)); }
	inline RuntimeObject* get__creator_0() const { return ____creator_0; }
	inline RuntimeObject** get_address_of__creator_0() { return &____creator_0; }
	inline void set__creator_0(RuntimeObject* value)
	{
		____creator_0 = value;
		Il2CppCodeGenWriteBarrier((&____creator_0), value);
	}

	inline static int32_t get_offset_of__instances_1() { return static_cast<int32_t>(offsetof(CachedProvider_t0DD0B25EA381638476E1E4EC2FC32C8CDA59BA36, ____instances_1)); }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * get__instances_1() const { return ____instances_1; }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D ** get_address_of__instances_1() { return &____instances_1; }
	inline void set__instances_1(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * value)
	{
		____instances_1 = value;
		Il2CppCodeGenWriteBarrier((&____instances_1), value);
	}

	inline static int32_t get_offset_of__isCreatingInstance_2() { return static_cast<int32_t>(offsetof(CachedProvider_t0DD0B25EA381638476E1E4EC2FC32C8CDA59BA36, ____isCreatingInstance_2)); }
	inline bool get__isCreatingInstance_2() const { return ____isCreatingInstance_2; }
	inline bool* get_address_of__isCreatingInstance_2() { return &____isCreatingInstance_2; }
	inline void set__isCreatingInstance_2(bool value)
	{
		____isCreatingInstance_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CACHEDPROVIDER_T0DD0B25EA381638476E1E4EC2FC32C8CDA59BA36_H
#ifndef U3CGETALLINSTANCESWITHINJECTSPLITU3ED__5_T36659B04E0790E6DFBBF55651E001AE8AEF04EBD_H
#define U3CGETALLINSTANCESWITHINJECTSPLITU3ED__5_T36659B04E0790E6DFBBF55651E001AE8AEF04EBD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.CachedProvider_<GetAllInstancesWithInjectSplit>d__5
struct  U3CGetAllInstancesWithInjectSplitU3Ed__5_t36659B04E0790E6DFBBF55651E001AE8AEF04EBD  : public RuntimeObject
{
public:
	// System.Int32 Zenject.CachedProvider_<GetAllInstancesWithInjectSplit>d__5::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Collections.Generic.List`1<System.Object> Zenject.CachedProvider_<GetAllInstancesWithInjectSplit>d__5::<>2__current
	List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * ___U3CU3E2__current_1;
	// Zenject.InjectContext Zenject.CachedProvider_<GetAllInstancesWithInjectSplit>d__5::context
	InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * ___context_2;
	// Zenject.CachedProvider Zenject.CachedProvider_<GetAllInstancesWithInjectSplit>d__5::<>4__this
	CachedProvider_t0DD0B25EA381638476E1E4EC2FC32C8CDA59BA36 * ___U3CU3E4__this_3;
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.CachedProvider_<GetAllInstancesWithInjectSplit>d__5::args
	List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * ___args_4;
	// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.List`1<System.Object>> Zenject.CachedProvider_<GetAllInstancesWithInjectSplit>d__5::<runner>5__2
	RuntimeObject* ___U3CrunnerU3E5__2_5;
	// System.Boolean Zenject.CachedProvider_<GetAllInstancesWithInjectSplit>d__5::<hasMore>5__3
	bool ___U3ChasMoreU3E5__3_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__5_t36659B04E0790E6DFBBF55651E001AE8AEF04EBD, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__5_t36659B04E0790E6DFBBF55651E001AE8AEF04EBD, ___U3CU3E2__current_1)); }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_context_2() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__5_t36659B04E0790E6DFBBF55651E001AE8AEF04EBD, ___context_2)); }
	inline InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * get_context_2() const { return ___context_2; }
	inline InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF ** get_address_of_context_2() { return &___context_2; }
	inline void set_context_2(InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * value)
	{
		___context_2 = value;
		Il2CppCodeGenWriteBarrier((&___context_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__5_t36659B04E0790E6DFBBF55651E001AE8AEF04EBD, ___U3CU3E4__this_3)); }
	inline CachedProvider_t0DD0B25EA381638476E1E4EC2FC32C8CDA59BA36 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline CachedProvider_t0DD0B25EA381638476E1E4EC2FC32C8CDA59BA36 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(CachedProvider_t0DD0B25EA381638476E1E4EC2FC32C8CDA59BA36 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_args_4() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__5_t36659B04E0790E6DFBBF55651E001AE8AEF04EBD, ___args_4)); }
	inline List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * get_args_4() const { return ___args_4; }
	inline List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 ** get_address_of_args_4() { return &___args_4; }
	inline void set_args_4(List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * value)
	{
		___args_4 = value;
		Il2CppCodeGenWriteBarrier((&___args_4), value);
	}

	inline static int32_t get_offset_of_U3CrunnerU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__5_t36659B04E0790E6DFBBF55651E001AE8AEF04EBD, ___U3CrunnerU3E5__2_5)); }
	inline RuntimeObject* get_U3CrunnerU3E5__2_5() const { return ___U3CrunnerU3E5__2_5; }
	inline RuntimeObject** get_address_of_U3CrunnerU3E5__2_5() { return &___U3CrunnerU3E5__2_5; }
	inline void set_U3CrunnerU3E5__2_5(RuntimeObject* value)
	{
		___U3CrunnerU3E5__2_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrunnerU3E5__2_5), value);
	}

	inline static int32_t get_offset_of_U3ChasMoreU3E5__3_6() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__5_t36659B04E0790E6DFBBF55651E001AE8AEF04EBD, ___U3ChasMoreU3E5__3_6)); }
	inline bool get_U3ChasMoreU3E5__3_6() const { return ___U3ChasMoreU3E5__3_6; }
	inline bool* get_address_of_U3ChasMoreU3E5__3_6() { return &___U3ChasMoreU3E5__3_6; }
	inline void set_U3ChasMoreU3E5__3_6(bool value)
	{
		___U3ChasMoreU3E5__3_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETALLINSTANCESWITHINJECTSPLITU3ED__5_T36659B04E0790E6DFBBF55651E001AE8AEF04EBD_H
#ifndef DICONTAINER_T7619E999A5CE72FEE4D2419403214E62D95FFFD5_H
#define DICONTAINER_T7619E999A5CE72FEE4D2419403214E62D95FFFD5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.DiContainer
struct  DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.DiContainer_ProviderInfo>> Zenject.DiContainer::_providers
	Dictionary_2_t7C592452CF60789D3B03C452382E134D587ABADC * ____providers_0;
	// System.Collections.Generic.List`1<Zenject.DiContainer> Zenject.DiContainer::_parentContainers
	List_1_tC0C0C06645444D1ED985242DB086B9AFC1D8E1FA * ____parentContainers_1;
	// System.Collections.Generic.List`1<Zenject.DiContainer> Zenject.DiContainer::_ancestorContainers
	List_1_tC0C0C06645444D1ED985242DB086B9AFC1D8E1FA * ____ancestorContainers_2;
	// System.Collections.Generic.Stack`1<Zenject.DiContainer_LookupId> Zenject.DiContainer::_resolvesInProgress
	Stack_1_t30B30BF8B50F476BF18BDC1667E81F8EE0045E48 * ____resolvesInProgress_3;
	// Zenject.SingletonProviderCreator Zenject.DiContainer::_singletonProviderCreator
	SingletonProviderCreator_t07218AEFEEBFF6A8B65FB17100F1AB292A94F5AD * ____singletonProviderCreator_4;
	// Zenject.SingletonMarkRegistry Zenject.DiContainer::_singletonMarkRegistry
	SingletonMarkRegistry_tB0ADDC9314F22EFEF3C51F08F71E6E1CB236EC7F * ____singletonMarkRegistry_5;
	// Zenject.LazyInstanceInjector Zenject.DiContainer::_lazyInjector
	LazyInstanceInjector_t0120534A9142C2F3AC0A2A87E5CAC778A6F831A6 * ____lazyInjector_6;
	// System.Collections.Generic.Queue`1<Zenject.IBindingFinalizer> Zenject.DiContainer::_currentBindings
	Queue_1_tB4CDAB25704B2A2FE785C4404A82F5B755C20BD9 * ____currentBindings_7;
	// System.Collections.Generic.List`1<Zenject.IBindingFinalizer> Zenject.DiContainer::_childBindings
	List_1_t260A5990EE904C05AE0C7D28243F87BFA487442A * ____childBindings_8;
	// System.Collections.Generic.List`1<Zenject.ILazy> Zenject.DiContainer::_lateBindingsToValidate
	List_1_t4B97E348520D143EA8C892D9014012A434C2A126 * ____lateBindingsToValidate_9;
	// Zenject.Context Zenject.DiContainer::_context
	Context_t04459FF3A5B7E8AF25B965F5717F535AE8FA6DBA * ____context_10;
	// System.Boolean Zenject.DiContainer::_isFinalizingBinding
	bool ____isFinalizingBinding_11;
	// System.Boolean Zenject.DiContainer::_isValidating
	bool ____isValidating_12;
	// System.Boolean Zenject.DiContainer::_isInstalling
	bool ____isInstalling_13;
	// System.Boolean Zenject.DiContainer::_hasDisplayedInstallWarning
	bool ____hasDisplayedInstallWarning_14;
	// System.Boolean Zenject.DiContainer::<ShouldCheckForInstallWarning>k__BackingField
	bool ___U3CShouldCheckForInstallWarningU3Ek__BackingField_15;
	// System.Boolean Zenject.DiContainer::<AssertOnNewGameObjects>k__BackingField
	bool ___U3CAssertOnNewGameObjectsU3Ek__BackingField_16;
	// UnityEngine.Transform Zenject.DiContainer::<DefaultParent>k__BackingField
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___U3CDefaultParentU3Ek__BackingField_17;

public:
	inline static int32_t get_offset_of__providers_0() { return static_cast<int32_t>(offsetof(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5, ____providers_0)); }
	inline Dictionary_2_t7C592452CF60789D3B03C452382E134D587ABADC * get__providers_0() const { return ____providers_0; }
	inline Dictionary_2_t7C592452CF60789D3B03C452382E134D587ABADC ** get_address_of__providers_0() { return &____providers_0; }
	inline void set__providers_0(Dictionary_2_t7C592452CF60789D3B03C452382E134D587ABADC * value)
	{
		____providers_0 = value;
		Il2CppCodeGenWriteBarrier((&____providers_0), value);
	}

	inline static int32_t get_offset_of__parentContainers_1() { return static_cast<int32_t>(offsetof(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5, ____parentContainers_1)); }
	inline List_1_tC0C0C06645444D1ED985242DB086B9AFC1D8E1FA * get__parentContainers_1() const { return ____parentContainers_1; }
	inline List_1_tC0C0C06645444D1ED985242DB086B9AFC1D8E1FA ** get_address_of__parentContainers_1() { return &____parentContainers_1; }
	inline void set__parentContainers_1(List_1_tC0C0C06645444D1ED985242DB086B9AFC1D8E1FA * value)
	{
		____parentContainers_1 = value;
		Il2CppCodeGenWriteBarrier((&____parentContainers_1), value);
	}

	inline static int32_t get_offset_of__ancestorContainers_2() { return static_cast<int32_t>(offsetof(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5, ____ancestorContainers_2)); }
	inline List_1_tC0C0C06645444D1ED985242DB086B9AFC1D8E1FA * get__ancestorContainers_2() const { return ____ancestorContainers_2; }
	inline List_1_tC0C0C06645444D1ED985242DB086B9AFC1D8E1FA ** get_address_of__ancestorContainers_2() { return &____ancestorContainers_2; }
	inline void set__ancestorContainers_2(List_1_tC0C0C06645444D1ED985242DB086B9AFC1D8E1FA * value)
	{
		____ancestorContainers_2 = value;
		Il2CppCodeGenWriteBarrier((&____ancestorContainers_2), value);
	}

	inline static int32_t get_offset_of__resolvesInProgress_3() { return static_cast<int32_t>(offsetof(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5, ____resolvesInProgress_3)); }
	inline Stack_1_t30B30BF8B50F476BF18BDC1667E81F8EE0045E48 * get__resolvesInProgress_3() const { return ____resolvesInProgress_3; }
	inline Stack_1_t30B30BF8B50F476BF18BDC1667E81F8EE0045E48 ** get_address_of__resolvesInProgress_3() { return &____resolvesInProgress_3; }
	inline void set__resolvesInProgress_3(Stack_1_t30B30BF8B50F476BF18BDC1667E81F8EE0045E48 * value)
	{
		____resolvesInProgress_3 = value;
		Il2CppCodeGenWriteBarrier((&____resolvesInProgress_3), value);
	}

	inline static int32_t get_offset_of__singletonProviderCreator_4() { return static_cast<int32_t>(offsetof(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5, ____singletonProviderCreator_4)); }
	inline SingletonProviderCreator_t07218AEFEEBFF6A8B65FB17100F1AB292A94F5AD * get__singletonProviderCreator_4() const { return ____singletonProviderCreator_4; }
	inline SingletonProviderCreator_t07218AEFEEBFF6A8B65FB17100F1AB292A94F5AD ** get_address_of__singletonProviderCreator_4() { return &____singletonProviderCreator_4; }
	inline void set__singletonProviderCreator_4(SingletonProviderCreator_t07218AEFEEBFF6A8B65FB17100F1AB292A94F5AD * value)
	{
		____singletonProviderCreator_4 = value;
		Il2CppCodeGenWriteBarrier((&____singletonProviderCreator_4), value);
	}

	inline static int32_t get_offset_of__singletonMarkRegistry_5() { return static_cast<int32_t>(offsetof(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5, ____singletonMarkRegistry_5)); }
	inline SingletonMarkRegistry_tB0ADDC9314F22EFEF3C51F08F71E6E1CB236EC7F * get__singletonMarkRegistry_5() const { return ____singletonMarkRegistry_5; }
	inline SingletonMarkRegistry_tB0ADDC9314F22EFEF3C51F08F71E6E1CB236EC7F ** get_address_of__singletonMarkRegistry_5() { return &____singletonMarkRegistry_5; }
	inline void set__singletonMarkRegistry_5(SingletonMarkRegistry_tB0ADDC9314F22EFEF3C51F08F71E6E1CB236EC7F * value)
	{
		____singletonMarkRegistry_5 = value;
		Il2CppCodeGenWriteBarrier((&____singletonMarkRegistry_5), value);
	}

	inline static int32_t get_offset_of__lazyInjector_6() { return static_cast<int32_t>(offsetof(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5, ____lazyInjector_6)); }
	inline LazyInstanceInjector_t0120534A9142C2F3AC0A2A87E5CAC778A6F831A6 * get__lazyInjector_6() const { return ____lazyInjector_6; }
	inline LazyInstanceInjector_t0120534A9142C2F3AC0A2A87E5CAC778A6F831A6 ** get_address_of__lazyInjector_6() { return &____lazyInjector_6; }
	inline void set__lazyInjector_6(LazyInstanceInjector_t0120534A9142C2F3AC0A2A87E5CAC778A6F831A6 * value)
	{
		____lazyInjector_6 = value;
		Il2CppCodeGenWriteBarrier((&____lazyInjector_6), value);
	}

	inline static int32_t get_offset_of__currentBindings_7() { return static_cast<int32_t>(offsetof(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5, ____currentBindings_7)); }
	inline Queue_1_tB4CDAB25704B2A2FE785C4404A82F5B755C20BD9 * get__currentBindings_7() const { return ____currentBindings_7; }
	inline Queue_1_tB4CDAB25704B2A2FE785C4404A82F5B755C20BD9 ** get_address_of__currentBindings_7() { return &____currentBindings_7; }
	inline void set__currentBindings_7(Queue_1_tB4CDAB25704B2A2FE785C4404A82F5B755C20BD9 * value)
	{
		____currentBindings_7 = value;
		Il2CppCodeGenWriteBarrier((&____currentBindings_7), value);
	}

	inline static int32_t get_offset_of__childBindings_8() { return static_cast<int32_t>(offsetof(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5, ____childBindings_8)); }
	inline List_1_t260A5990EE904C05AE0C7D28243F87BFA487442A * get__childBindings_8() const { return ____childBindings_8; }
	inline List_1_t260A5990EE904C05AE0C7D28243F87BFA487442A ** get_address_of__childBindings_8() { return &____childBindings_8; }
	inline void set__childBindings_8(List_1_t260A5990EE904C05AE0C7D28243F87BFA487442A * value)
	{
		____childBindings_8 = value;
		Il2CppCodeGenWriteBarrier((&____childBindings_8), value);
	}

	inline static int32_t get_offset_of__lateBindingsToValidate_9() { return static_cast<int32_t>(offsetof(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5, ____lateBindingsToValidate_9)); }
	inline List_1_t4B97E348520D143EA8C892D9014012A434C2A126 * get__lateBindingsToValidate_9() const { return ____lateBindingsToValidate_9; }
	inline List_1_t4B97E348520D143EA8C892D9014012A434C2A126 ** get_address_of__lateBindingsToValidate_9() { return &____lateBindingsToValidate_9; }
	inline void set__lateBindingsToValidate_9(List_1_t4B97E348520D143EA8C892D9014012A434C2A126 * value)
	{
		____lateBindingsToValidate_9 = value;
		Il2CppCodeGenWriteBarrier((&____lateBindingsToValidate_9), value);
	}

	inline static int32_t get_offset_of__context_10() { return static_cast<int32_t>(offsetof(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5, ____context_10)); }
	inline Context_t04459FF3A5B7E8AF25B965F5717F535AE8FA6DBA * get__context_10() const { return ____context_10; }
	inline Context_t04459FF3A5B7E8AF25B965F5717F535AE8FA6DBA ** get_address_of__context_10() { return &____context_10; }
	inline void set__context_10(Context_t04459FF3A5B7E8AF25B965F5717F535AE8FA6DBA * value)
	{
		____context_10 = value;
		Il2CppCodeGenWriteBarrier((&____context_10), value);
	}

	inline static int32_t get_offset_of__isFinalizingBinding_11() { return static_cast<int32_t>(offsetof(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5, ____isFinalizingBinding_11)); }
	inline bool get__isFinalizingBinding_11() const { return ____isFinalizingBinding_11; }
	inline bool* get_address_of__isFinalizingBinding_11() { return &____isFinalizingBinding_11; }
	inline void set__isFinalizingBinding_11(bool value)
	{
		____isFinalizingBinding_11 = value;
	}

	inline static int32_t get_offset_of__isValidating_12() { return static_cast<int32_t>(offsetof(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5, ____isValidating_12)); }
	inline bool get__isValidating_12() const { return ____isValidating_12; }
	inline bool* get_address_of__isValidating_12() { return &____isValidating_12; }
	inline void set__isValidating_12(bool value)
	{
		____isValidating_12 = value;
	}

	inline static int32_t get_offset_of__isInstalling_13() { return static_cast<int32_t>(offsetof(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5, ____isInstalling_13)); }
	inline bool get__isInstalling_13() const { return ____isInstalling_13; }
	inline bool* get_address_of__isInstalling_13() { return &____isInstalling_13; }
	inline void set__isInstalling_13(bool value)
	{
		____isInstalling_13 = value;
	}

	inline static int32_t get_offset_of__hasDisplayedInstallWarning_14() { return static_cast<int32_t>(offsetof(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5, ____hasDisplayedInstallWarning_14)); }
	inline bool get__hasDisplayedInstallWarning_14() const { return ____hasDisplayedInstallWarning_14; }
	inline bool* get_address_of__hasDisplayedInstallWarning_14() { return &____hasDisplayedInstallWarning_14; }
	inline void set__hasDisplayedInstallWarning_14(bool value)
	{
		____hasDisplayedInstallWarning_14 = value;
	}

	inline static int32_t get_offset_of_U3CShouldCheckForInstallWarningU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5, ___U3CShouldCheckForInstallWarningU3Ek__BackingField_15)); }
	inline bool get_U3CShouldCheckForInstallWarningU3Ek__BackingField_15() const { return ___U3CShouldCheckForInstallWarningU3Ek__BackingField_15; }
	inline bool* get_address_of_U3CShouldCheckForInstallWarningU3Ek__BackingField_15() { return &___U3CShouldCheckForInstallWarningU3Ek__BackingField_15; }
	inline void set_U3CShouldCheckForInstallWarningU3Ek__BackingField_15(bool value)
	{
		___U3CShouldCheckForInstallWarningU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3CAssertOnNewGameObjectsU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5, ___U3CAssertOnNewGameObjectsU3Ek__BackingField_16)); }
	inline bool get_U3CAssertOnNewGameObjectsU3Ek__BackingField_16() const { return ___U3CAssertOnNewGameObjectsU3Ek__BackingField_16; }
	inline bool* get_address_of_U3CAssertOnNewGameObjectsU3Ek__BackingField_16() { return &___U3CAssertOnNewGameObjectsU3Ek__BackingField_16; }
	inline void set_U3CAssertOnNewGameObjectsU3Ek__BackingField_16(bool value)
	{
		___U3CAssertOnNewGameObjectsU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CDefaultParentU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5, ___U3CDefaultParentU3Ek__BackingField_17)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_U3CDefaultParentU3Ek__BackingField_17() const { return ___U3CDefaultParentU3Ek__BackingField_17; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_U3CDefaultParentU3Ek__BackingField_17() { return &___U3CDefaultParentU3Ek__BackingField_17; }
	inline void set_U3CDefaultParentU3Ek__BackingField_17(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___U3CDefaultParentU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDefaultParentU3Ek__BackingField_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICONTAINER_T7619E999A5CE72FEE4D2419403214E62D95FFFD5_H
#ifndef U3CU3EC_T796C2A8263700A6383229AE9711EDF5DD258D101_H
#define U3CU3EC_T796C2A8263700A6383229AE9711EDF5DD258D101_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.DiContainer_<>c
struct  U3CU3Ec_t796C2A8263700A6383229AE9711EDF5DD258D101  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t796C2A8263700A6383229AE9711EDF5DD258D101_StaticFields
{
public:
	// Zenject.DiContainer_<>c Zenject.DiContainer_<>c::<>9
	U3CU3Ec_t796C2A8263700A6383229AE9711EDF5DD258D101 * ___U3CU3E9_0;
	// System.Func`2<Zenject.DiContainer,System.Collections.Generic.IEnumerable`1<Zenject.IBindingFinalizer>> Zenject.DiContainer_<>c::<>9__19_0
	Func_2_t54CC118F1E2AA7F5B40EDF23AE63CACDDE532C98 * ___U3CU3E9__19_0_1;
	// System.Func`2<Zenject.DiContainer_ProviderPair,Zenject.IProvider> Zenject.DiContainer_<>c::<>9__58_0
	Func_2_t21BBB65AA54D3E1A113E578E8D62667B7ADD006F * ___U3CU3E9__58_0_2;
	// System.Func`2<System.Type,System.Boolean> Zenject.DiContainer_<>c::<>9__74_1
	Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 * ___U3CU3E9__74_1_3;
	// System.Func`2<<>f__AnonymousType0`2<Zenject.DiContainer_ProviderPair,System.Int32>,System.Int32> Zenject.DiContainer_<>c::<>9__75_1
	Func_2_tA59CD5991CE1D5EFC471FB54024E70E6DC7710ED * ___U3CU3E9__75_1_4;
	// System.Func`2<<>f__AnonymousType0`2<Zenject.DiContainer_ProviderPair,System.Int32>,System.Boolean> Zenject.DiContainer_<>c::<>9__75_3
	Func_2_t54C395B226509C9A26E591AACA3A9C9BB85CD7B1 * ___U3CU3E9__75_3_5;
	// System.Func`2<<>f__AnonymousType0`2<Zenject.DiContainer_ProviderPair,System.Int32>,Zenject.DiContainer_ProviderPair> Zenject.DiContainer_<>c::<>9__75_4
	Func_2_tC09774CCB9423FDD7EFDC04BDFBAAD3396C85E9B * ___U3CU3E9__75_4_6;
	// System.Func`2<Zenject.TypeValuePair,System.String> Zenject.DiContainer_<>c::<>9__83_0
	Func_2_t1C48021AB2F773B7BE03E0871A032086737F7698 * ___U3CU3E9__83_0_7;
	// System.Func`2<Zenject.TypeValuePair,System.String> Zenject.DiContainer_<>c::<>9__86_0
	Func_2_t1C48021AB2F773B7BE03E0871A032086737F7698 * ___U3CU3E9__86_0_8;
	// System.Func`2<System.Type,System.Boolean> Zenject.DiContainer_<>c::<>9__175_0
	Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 * ___U3CU3E9__175_0_9;
	// System.Func`2<System.Type,System.Boolean> Zenject.DiContainer_<>c::<>9__176_0
	Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 * ___U3CU3E9__176_0_10;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t796C2A8263700A6383229AE9711EDF5DD258D101_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t796C2A8263700A6383229AE9711EDF5DD258D101 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t796C2A8263700A6383229AE9711EDF5DD258D101 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t796C2A8263700A6383229AE9711EDF5DD258D101 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__19_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t796C2A8263700A6383229AE9711EDF5DD258D101_StaticFields, ___U3CU3E9__19_0_1)); }
	inline Func_2_t54CC118F1E2AA7F5B40EDF23AE63CACDDE532C98 * get_U3CU3E9__19_0_1() const { return ___U3CU3E9__19_0_1; }
	inline Func_2_t54CC118F1E2AA7F5B40EDF23AE63CACDDE532C98 ** get_address_of_U3CU3E9__19_0_1() { return &___U3CU3E9__19_0_1; }
	inline void set_U3CU3E9__19_0_1(Func_2_t54CC118F1E2AA7F5B40EDF23AE63CACDDE532C98 * value)
	{
		___U3CU3E9__19_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__19_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__58_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t796C2A8263700A6383229AE9711EDF5DD258D101_StaticFields, ___U3CU3E9__58_0_2)); }
	inline Func_2_t21BBB65AA54D3E1A113E578E8D62667B7ADD006F * get_U3CU3E9__58_0_2() const { return ___U3CU3E9__58_0_2; }
	inline Func_2_t21BBB65AA54D3E1A113E578E8D62667B7ADD006F ** get_address_of_U3CU3E9__58_0_2() { return &___U3CU3E9__58_0_2; }
	inline void set_U3CU3E9__58_0_2(Func_2_t21BBB65AA54D3E1A113E578E8D62667B7ADD006F * value)
	{
		___U3CU3E9__58_0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__58_0_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__74_1_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t796C2A8263700A6383229AE9711EDF5DD258D101_StaticFields, ___U3CU3E9__74_1_3)); }
	inline Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 * get_U3CU3E9__74_1_3() const { return ___U3CU3E9__74_1_3; }
	inline Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 ** get_address_of_U3CU3E9__74_1_3() { return &___U3CU3E9__74_1_3; }
	inline void set_U3CU3E9__74_1_3(Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 * value)
	{
		___U3CU3E9__74_1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__74_1_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__75_1_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_t796C2A8263700A6383229AE9711EDF5DD258D101_StaticFields, ___U3CU3E9__75_1_4)); }
	inline Func_2_tA59CD5991CE1D5EFC471FB54024E70E6DC7710ED * get_U3CU3E9__75_1_4() const { return ___U3CU3E9__75_1_4; }
	inline Func_2_tA59CD5991CE1D5EFC471FB54024E70E6DC7710ED ** get_address_of_U3CU3E9__75_1_4() { return &___U3CU3E9__75_1_4; }
	inline void set_U3CU3E9__75_1_4(Func_2_tA59CD5991CE1D5EFC471FB54024E70E6DC7710ED * value)
	{
		___U3CU3E9__75_1_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__75_1_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__75_3_5() { return static_cast<int32_t>(offsetof(U3CU3Ec_t796C2A8263700A6383229AE9711EDF5DD258D101_StaticFields, ___U3CU3E9__75_3_5)); }
	inline Func_2_t54C395B226509C9A26E591AACA3A9C9BB85CD7B1 * get_U3CU3E9__75_3_5() const { return ___U3CU3E9__75_3_5; }
	inline Func_2_t54C395B226509C9A26E591AACA3A9C9BB85CD7B1 ** get_address_of_U3CU3E9__75_3_5() { return &___U3CU3E9__75_3_5; }
	inline void set_U3CU3E9__75_3_5(Func_2_t54C395B226509C9A26E591AACA3A9C9BB85CD7B1 * value)
	{
		___U3CU3E9__75_3_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__75_3_5), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__75_4_6() { return static_cast<int32_t>(offsetof(U3CU3Ec_t796C2A8263700A6383229AE9711EDF5DD258D101_StaticFields, ___U3CU3E9__75_4_6)); }
	inline Func_2_tC09774CCB9423FDD7EFDC04BDFBAAD3396C85E9B * get_U3CU3E9__75_4_6() const { return ___U3CU3E9__75_4_6; }
	inline Func_2_tC09774CCB9423FDD7EFDC04BDFBAAD3396C85E9B ** get_address_of_U3CU3E9__75_4_6() { return &___U3CU3E9__75_4_6; }
	inline void set_U3CU3E9__75_4_6(Func_2_tC09774CCB9423FDD7EFDC04BDFBAAD3396C85E9B * value)
	{
		___U3CU3E9__75_4_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__75_4_6), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__83_0_7() { return static_cast<int32_t>(offsetof(U3CU3Ec_t796C2A8263700A6383229AE9711EDF5DD258D101_StaticFields, ___U3CU3E9__83_0_7)); }
	inline Func_2_t1C48021AB2F773B7BE03E0871A032086737F7698 * get_U3CU3E9__83_0_7() const { return ___U3CU3E9__83_0_7; }
	inline Func_2_t1C48021AB2F773B7BE03E0871A032086737F7698 ** get_address_of_U3CU3E9__83_0_7() { return &___U3CU3E9__83_0_7; }
	inline void set_U3CU3E9__83_0_7(Func_2_t1C48021AB2F773B7BE03E0871A032086737F7698 * value)
	{
		___U3CU3E9__83_0_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__83_0_7), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__86_0_8() { return static_cast<int32_t>(offsetof(U3CU3Ec_t796C2A8263700A6383229AE9711EDF5DD258D101_StaticFields, ___U3CU3E9__86_0_8)); }
	inline Func_2_t1C48021AB2F773B7BE03E0871A032086737F7698 * get_U3CU3E9__86_0_8() const { return ___U3CU3E9__86_0_8; }
	inline Func_2_t1C48021AB2F773B7BE03E0871A032086737F7698 ** get_address_of_U3CU3E9__86_0_8() { return &___U3CU3E9__86_0_8; }
	inline void set_U3CU3E9__86_0_8(Func_2_t1C48021AB2F773B7BE03E0871A032086737F7698 * value)
	{
		___U3CU3E9__86_0_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__86_0_8), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__175_0_9() { return static_cast<int32_t>(offsetof(U3CU3Ec_t796C2A8263700A6383229AE9711EDF5DD258D101_StaticFields, ___U3CU3E9__175_0_9)); }
	inline Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 * get_U3CU3E9__175_0_9() const { return ___U3CU3E9__175_0_9; }
	inline Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 ** get_address_of_U3CU3E9__175_0_9() { return &___U3CU3E9__175_0_9; }
	inline void set_U3CU3E9__175_0_9(Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 * value)
	{
		___U3CU3E9__175_0_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__175_0_9), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__176_0_10() { return static_cast<int32_t>(offsetof(U3CU3Ec_t796C2A8263700A6383229AE9711EDF5DD258D101_StaticFields, ___U3CU3E9__176_0_10)); }
	inline Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 * get_U3CU3E9__176_0_10() const { return ___U3CU3E9__176_0_10; }
	inline Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 ** get_address_of_U3CU3E9__176_0_10() { return &___U3CU3E9__176_0_10; }
	inline void set_U3CU3E9__176_0_10(Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 * value)
	{
		___U3CU3E9__176_0_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__176_0_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T796C2A8263700A6383229AE9711EDF5DD258D101_H
#ifndef U3CU3EC__DISPLAYCLASS158_0_T7419DA0257C3E5FDBCED2089793EA859DA008041_H
#define U3CU3EC__DISPLAYCLASS158_0_T7419DA0257C3E5FDBCED2089793EA859DA008041_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.DiContainer_<>c__DisplayClass158_0
struct  U3CU3Ec__DisplayClass158_0_t7419DA0257C3E5FDBCED2089793EA859DA008041  : public RuntimeObject
{
public:
	// Zenject.DiContainer Zenject.DiContainer_<>c__DisplayClass158_0::<>4__this
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ___U3CU3E4__this_0;
	// System.Type Zenject.DiContainer_<>c__DisplayClass158_0::contractType
	Type_t * ___contractType_1;
	// System.Object Zenject.DiContainer_<>c__DisplayClass158_0::identifier
	RuntimeObject * ___identifier_2;
	// System.Type Zenject.DiContainer_<>c__DisplayClass158_0::concreteType
	Type_t * ___concreteType_3;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass158_0_t7419DA0257C3E5FDBCED2089793EA859DA008041, ___U3CU3E4__this_0)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_contractType_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass158_0_t7419DA0257C3E5FDBCED2089793EA859DA008041, ___contractType_1)); }
	inline Type_t * get_contractType_1() const { return ___contractType_1; }
	inline Type_t ** get_address_of_contractType_1() { return &___contractType_1; }
	inline void set_contractType_1(Type_t * value)
	{
		___contractType_1 = value;
		Il2CppCodeGenWriteBarrier((&___contractType_1), value);
	}

	inline static int32_t get_offset_of_identifier_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass158_0_t7419DA0257C3E5FDBCED2089793EA859DA008041, ___identifier_2)); }
	inline RuntimeObject * get_identifier_2() const { return ___identifier_2; }
	inline RuntimeObject ** get_address_of_identifier_2() { return &___identifier_2; }
	inline void set_identifier_2(RuntimeObject * value)
	{
		___identifier_2 = value;
		Il2CppCodeGenWriteBarrier((&___identifier_2), value);
	}

	inline static int32_t get_offset_of_concreteType_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass158_0_t7419DA0257C3E5FDBCED2089793EA859DA008041, ___concreteType_3)); }
	inline Type_t * get_concreteType_3() const { return ___concreteType_3; }
	inline Type_t ** get_address_of_concreteType_3() { return &___concreteType_3; }
	inline void set_concreteType_3(Type_t * value)
	{
		___concreteType_3 = value;
		Il2CppCodeGenWriteBarrier((&___concreteType_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS158_0_T7419DA0257C3E5FDBCED2089793EA859DA008041_H
#ifndef U3CU3EC__DISPLAYCLASS51_0_TCCB988B4E31257DD9D490195B8525F4B9B18B347_H
#define U3CU3EC__DISPLAYCLASS51_0_TCCB988B4E31257DD9D490195B8525F4B9B18B347_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.DiContainer_<>c__DisplayClass51_0
struct  U3CU3Ec__DisplayClass51_0_tCCB988B4E31257DD9D490195B8525F4B9B18B347  : public RuntimeObject
{
public:
	// Zenject.InjectContext Zenject.DiContainer_<>c__DisplayClass51_0::injectContext
	InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * ___injectContext_0;

public:
	inline static int32_t get_offset_of_injectContext_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass51_0_tCCB988B4E31257DD9D490195B8525F4B9B18B347, ___injectContext_0)); }
	inline InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * get_injectContext_0() const { return ___injectContext_0; }
	inline InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF ** get_address_of_injectContext_0() { return &___injectContext_0; }
	inline void set_injectContext_0(InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * value)
	{
		___injectContext_0 = value;
		Il2CppCodeGenWriteBarrier((&___injectContext_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS51_0_TCCB988B4E31257DD9D490195B8525F4B9B18B347_H
#ifndef U3CU3EC__DISPLAYCLASS59_0_T2EA11530640333862D7CBC76D824CAE0F1E864A8_H
#define U3CU3EC__DISPLAYCLASS59_0_T2EA11530640333862D7CBC76D824CAE0F1E864A8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.DiContainer_<>c__DisplayClass59_0
struct  U3CU3Ec__DisplayClass59_0_t2EA11530640333862D7CBC76D824CAE0F1E864A8  : public RuntimeObject
{
public:
	// Zenject.InjectContext Zenject.DiContainer_<>c__DisplayClass59_0::context
	InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * ___context_0;

public:
	inline static int32_t get_offset_of_context_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass59_0_t2EA11530640333862D7CBC76D824CAE0F1E864A8, ___context_0)); }
	inline InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * get_context_0() const { return ___context_0; }
	inline InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF ** get_address_of_context_0() { return &___context_0; }
	inline void set_context_0(InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * value)
	{
		___context_0 = value;
		Il2CppCodeGenWriteBarrier((&___context_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS59_0_T2EA11530640333862D7CBC76D824CAE0F1E864A8_H
#ifndef U3CU3EC__DISPLAYCLASS63_0_T3AC546137B60AD36628C4C7651EC0BC243310E2B_H
#define U3CU3EC__DISPLAYCLASS63_0_T3AC546137B60AD36628C4C7651EC0BC243310E2B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.DiContainer_<>c__DisplayClass63_0
struct  U3CU3Ec__DisplayClass63_0_t3AC546137B60AD36628C4C7651EC0BC243310E2B  : public RuntimeObject
{
public:
	// Zenject.BindingId Zenject.DiContainer_<>c__DisplayClass63_0::bindingId
	BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA * ___bindingId_0;

public:
	inline static int32_t get_offset_of_bindingId_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass63_0_t3AC546137B60AD36628C4C7651EC0BC243310E2B, ___bindingId_0)); }
	inline BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA * get_bindingId_0() const { return ___bindingId_0; }
	inline BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA ** get_address_of_bindingId_0() { return &___bindingId_0; }
	inline void set_bindingId_0(BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA * value)
	{
		___bindingId_0 = value;
		Il2CppCodeGenWriteBarrier((&___bindingId_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS63_0_T3AC546137B60AD36628C4C7651EC0BC243310E2B_H
#ifndef U3CU3EC__DISPLAYCLASS67_0_T03EBDB1872CBC7078F09C9831BF45860C8F02AFA_H
#define U3CU3EC__DISPLAYCLASS67_0_T03EBDB1872CBC7078F09C9831BF45860C8F02AFA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.DiContainer_<>c__DisplayClass67_0
struct  U3CU3Ec__DisplayClass67_0_t03EBDB1872CBC7078F09C9831BF45860C8F02AFA  : public RuntimeObject
{
public:
	// Zenject.DiContainer Zenject.DiContainer_<>c__DisplayClass67_0::<>4__this
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ___U3CU3E4__this_0;
	// Zenject.InjectContext Zenject.DiContainer_<>c__DisplayClass67_0::context
	InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * ___context_1;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass67_0_t03EBDB1872CBC7078F09C9831BF45860C8F02AFA, ___U3CU3E4__this_0)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_context_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass67_0_t03EBDB1872CBC7078F09C9831BF45860C8F02AFA, ___context_1)); }
	inline InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * get_context_1() const { return ___context_1; }
	inline InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF ** get_address_of_context_1() { return &___context_1; }
	inline void set_context_1(InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * value)
	{
		___context_1 = value;
		Il2CppCodeGenWriteBarrier((&___context_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS67_0_T03EBDB1872CBC7078F09C9831BF45860C8F02AFA_H
#ifndef U3CU3EC__DISPLAYCLASS74_0_TA09B55C26E67F636E9D97B0D9259D9604E391487_H
#define U3CU3EC__DISPLAYCLASS74_0_TA09B55C26E67F636E9D97B0D9259D9604E391487_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.DiContainer_<>c__DisplayClass74_0
struct  U3CU3Ec__DisplayClass74_0_tA09B55C26E67F636E9D97B0D9259D9604E391487  : public RuntimeObject
{
public:
	// Zenject.InjectContext Zenject.DiContainer_<>c__DisplayClass74_0::context
	InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * ___context_0;

public:
	inline static int32_t get_offset_of_context_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass74_0_tA09B55C26E67F636E9D97B0D9259D9604E391487, ___context_0)); }
	inline InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * get_context_0() const { return ___context_0; }
	inline InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF ** get_address_of_context_0() { return &___context_0; }
	inline void set_context_0(InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * value)
	{
		___context_0 = value;
		Il2CppCodeGenWriteBarrier((&___context_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS74_0_TA09B55C26E67F636E9D97B0D9259D9604E391487_H
#ifndef U3CU3EC__DISPLAYCLASS75_0_T831AD3800DCE5B0F2F54BF79204F0A5C9CCDA831_H
#define U3CU3EC__DISPLAYCLASS75_0_T831AD3800DCE5B0F2F54BF79204F0A5C9CCDA831_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.DiContainer_<>c__DisplayClass75_0
struct  U3CU3Ec__DisplayClass75_0_t831AD3800DCE5B0F2F54BF79204F0A5C9CCDA831  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<<>f__AnonymousType0`2<Zenject.DiContainer_ProviderPair,System.Int32>> Zenject.DiContainer_<>c__DisplayClass75_0::sortedProviders
	List_1_t96D85E616B8F1D63BF7EC3A7828B462B9A182456 * ___sortedProviders_0;

public:
	inline static int32_t get_offset_of_sortedProviders_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass75_0_t831AD3800DCE5B0F2F54BF79204F0A5C9CCDA831, ___sortedProviders_0)); }
	inline List_1_t96D85E616B8F1D63BF7EC3A7828B462B9A182456 * get_sortedProviders_0() const { return ___sortedProviders_0; }
	inline List_1_t96D85E616B8F1D63BF7EC3A7828B462B9A182456 ** get_address_of_sortedProviders_0() { return &___sortedProviders_0; }
	inline void set_sortedProviders_0(List_1_t96D85E616B8F1D63BF7EC3A7828B462B9A182456 * value)
	{
		___sortedProviders_0 = value;
		Il2CppCodeGenWriteBarrier((&___sortedProviders_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS75_0_T831AD3800DCE5B0F2F54BF79204F0A5C9CCDA831_H
#ifndef U3CGETDEPENDENCYCONTRACTSU3ED__81_TD502D02FDEE43AF8701F3F53937B5D497A656B24_H
#define U3CGETDEPENDENCYCONTRACTSU3ED__81_TD502D02FDEE43AF8701F3F53937B5D497A656B24_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.DiContainer_<GetDependencyContracts>d__81
struct  U3CGetDependencyContractsU3Ed__81_tD502D02FDEE43AF8701F3F53937B5D497A656B24  : public RuntimeObject
{
public:
	// System.Int32 Zenject.DiContainer_<GetDependencyContracts>d__81::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Type Zenject.DiContainer_<GetDependencyContracts>d__81::<>2__current
	Type_t * ___U3CU3E2__current_1;
	// System.Int32 Zenject.DiContainer_<GetDependencyContracts>d__81::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// Zenject.DiContainer Zenject.DiContainer_<GetDependencyContracts>d__81::<>4__this
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ___U3CU3E4__this_3;
	// System.Type Zenject.DiContainer_<GetDependencyContracts>d__81::contract
	Type_t * ___contract_4;
	// System.Type Zenject.DiContainer_<GetDependencyContracts>d__81::<>3__contract
	Type_t * ___U3CU3E3__contract_5;
	// System.Collections.Generic.IEnumerator`1<Zenject.InjectableInfo> Zenject.DiContainer_<GetDependencyContracts>d__81::<>7__wrap1
	RuntimeObject* ___U3CU3E7__wrap1_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetDependencyContractsU3Ed__81_tD502D02FDEE43AF8701F3F53937B5D497A656B24, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetDependencyContractsU3Ed__81_tD502D02FDEE43AF8701F3F53937B5D497A656B24, ___U3CU3E2__current_1)); }
	inline Type_t * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline Type_t ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(Type_t * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CGetDependencyContractsU3Ed__81_tD502D02FDEE43AF8701F3F53937B5D497A656B24, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CGetDependencyContractsU3Ed__81_tD502D02FDEE43AF8701F3F53937B5D497A656B24, ___U3CU3E4__this_3)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_contract_4() { return static_cast<int32_t>(offsetof(U3CGetDependencyContractsU3Ed__81_tD502D02FDEE43AF8701F3F53937B5D497A656B24, ___contract_4)); }
	inline Type_t * get_contract_4() const { return ___contract_4; }
	inline Type_t ** get_address_of_contract_4() { return &___contract_4; }
	inline void set_contract_4(Type_t * value)
	{
		___contract_4 = value;
		Il2CppCodeGenWriteBarrier((&___contract_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__contract_5() { return static_cast<int32_t>(offsetof(U3CGetDependencyContractsU3Ed__81_tD502D02FDEE43AF8701F3F53937B5D497A656B24, ___U3CU3E3__contract_5)); }
	inline Type_t * get_U3CU3E3__contract_5() const { return ___U3CU3E3__contract_5; }
	inline Type_t ** get_address_of_U3CU3E3__contract_5() { return &___U3CU3E3__contract_5; }
	inline void set_U3CU3E3__contract_5(Type_t * value)
	{
		___U3CU3E3__contract_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__contract_5), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_6() { return static_cast<int32_t>(offsetof(U3CGetDependencyContractsU3Ed__81_tD502D02FDEE43AF8701F3F53937B5D497A656B24, ___U3CU3E7__wrap1_6)); }
	inline RuntimeObject* get_U3CU3E7__wrap1_6() const { return ___U3CU3E7__wrap1_6; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap1_6() { return &___U3CU3E7__wrap1_6; }
	inline void set_U3CU3E7__wrap1_6(RuntimeObject* value)
	{
		___U3CU3E7__wrap1_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap1_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETDEPENDENCYCONTRACTSU3ED__81_TD502D02FDEE43AF8701F3F53937B5D497A656B24_H
#ifndef PROVIDERINFO_T038F6812DFCFCD306479198F3E14CCB57B026C86_H
#define PROVIDERINFO_T038F6812DFCFCD306479198F3E14CCB57B026C86_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.DiContainer_ProviderInfo
struct  ProviderInfo_t038F6812DFCFCD306479198F3E14CCB57B026C86  : public RuntimeObject
{
public:
	// System.Boolean Zenject.DiContainer_ProviderInfo::<NonLazy>k__BackingField
	bool ___U3CNonLazyU3Ek__BackingField_0;
	// Zenject.IProvider Zenject.DiContainer_ProviderInfo::<Provider>k__BackingField
	RuntimeObject* ___U3CProviderU3Ek__BackingField_1;
	// Zenject.BindingCondition Zenject.DiContainer_ProviderInfo::<Condition>k__BackingField
	BindingCondition_t3BC89ABD74DA160C3396036CE0360DE414B4ADD8 * ___U3CConditionU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CNonLazyU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ProviderInfo_t038F6812DFCFCD306479198F3E14CCB57B026C86, ___U3CNonLazyU3Ek__BackingField_0)); }
	inline bool get_U3CNonLazyU3Ek__BackingField_0() const { return ___U3CNonLazyU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CNonLazyU3Ek__BackingField_0() { return &___U3CNonLazyU3Ek__BackingField_0; }
	inline void set_U3CNonLazyU3Ek__BackingField_0(bool value)
	{
		___U3CNonLazyU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CProviderU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ProviderInfo_t038F6812DFCFCD306479198F3E14CCB57B026C86, ___U3CProviderU3Ek__BackingField_1)); }
	inline RuntimeObject* get_U3CProviderU3Ek__BackingField_1() const { return ___U3CProviderU3Ek__BackingField_1; }
	inline RuntimeObject** get_address_of_U3CProviderU3Ek__BackingField_1() { return &___U3CProviderU3Ek__BackingField_1; }
	inline void set_U3CProviderU3Ek__BackingField_1(RuntimeObject* value)
	{
		___U3CProviderU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CProviderU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CConditionU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ProviderInfo_t038F6812DFCFCD306479198F3E14CCB57B026C86, ___U3CConditionU3Ek__BackingField_2)); }
	inline BindingCondition_t3BC89ABD74DA160C3396036CE0360DE414B4ADD8 * get_U3CConditionU3Ek__BackingField_2() const { return ___U3CConditionU3Ek__BackingField_2; }
	inline BindingCondition_t3BC89ABD74DA160C3396036CE0360DE414B4ADD8 ** get_address_of_U3CConditionU3Ek__BackingField_2() { return &___U3CConditionU3Ek__BackingField_2; }
	inline void set_U3CConditionU3Ek__BackingField_2(BindingCondition_t3BC89ABD74DA160C3396036CE0360DE414B4ADD8 * value)
	{
		___U3CConditionU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CConditionU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROVIDERINFO_T038F6812DFCFCD306479198F3E14CCB57B026C86_H
#ifndef PROVIDERPAIR_T94BF5C0920EE77086B5FFF74A411E29EAB78A504_H
#define PROVIDERPAIR_T94BF5C0920EE77086B5FFF74A411E29EAB78A504_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.DiContainer_ProviderPair
struct  ProviderPair_t94BF5C0920EE77086B5FFF74A411E29EAB78A504  : public RuntimeObject
{
public:
	// Zenject.DiContainer_ProviderInfo Zenject.DiContainer_ProviderPair::<ProviderInfo>k__BackingField
	ProviderInfo_t038F6812DFCFCD306479198F3E14CCB57B026C86 * ___U3CProviderInfoU3Ek__BackingField_0;
	// Zenject.DiContainer Zenject.DiContainer_ProviderPair::<Container>k__BackingField
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ___U3CContainerU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CProviderInfoU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ProviderPair_t94BF5C0920EE77086B5FFF74A411E29EAB78A504, ___U3CProviderInfoU3Ek__BackingField_0)); }
	inline ProviderInfo_t038F6812DFCFCD306479198F3E14CCB57B026C86 * get_U3CProviderInfoU3Ek__BackingField_0() const { return ___U3CProviderInfoU3Ek__BackingField_0; }
	inline ProviderInfo_t038F6812DFCFCD306479198F3E14CCB57B026C86 ** get_address_of_U3CProviderInfoU3Ek__BackingField_0() { return &___U3CProviderInfoU3Ek__BackingField_0; }
	inline void set_U3CProviderInfoU3Ek__BackingField_0(ProviderInfo_t038F6812DFCFCD306479198F3E14CCB57B026C86 * value)
	{
		___U3CProviderInfoU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CProviderInfoU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CContainerU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ProviderPair_t94BF5C0920EE77086B5FFF74A411E29EAB78A504, ___U3CContainerU3Ek__BackingField_1)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get_U3CContainerU3Ek__BackingField_1() const { return ___U3CContainerU3Ek__BackingField_1; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of_U3CContainerU3Ek__BackingField_1() { return &___U3CContainerU3Ek__BackingField_1; }
	inline void set_U3CContainerU3Ek__BackingField_1(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		___U3CContainerU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CContainerU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROVIDERPAIR_T94BF5C0920EE77086B5FFF74A411E29EAB78A504_H
#ifndef EMPTYGAMEOBJECTPROVIDER_T99FDC7D0A141A3CB7F442969996294D2F3B749FC_H
#define EMPTYGAMEOBJECTPROVIDER_T99FDC7D0A141A3CB7F442969996294D2F3B749FC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.EmptyGameObjectProvider
struct  EmptyGameObjectProvider_t99FDC7D0A141A3CB7F442969996294D2F3B749FC  : public RuntimeObject
{
public:
	// Zenject.DiContainer Zenject.EmptyGameObjectProvider::_container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____container_0;
	// Zenject.GameObjectCreationParameters Zenject.EmptyGameObjectProvider::_gameObjectBindInfo
	GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A * ____gameObjectBindInfo_1;

public:
	inline static int32_t get_offset_of__container_0() { return static_cast<int32_t>(offsetof(EmptyGameObjectProvider_t99FDC7D0A141A3CB7F442969996294D2F3B749FC, ____container_0)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__container_0() const { return ____container_0; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__container_0() { return &____container_0; }
	inline void set__container_0(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____container_0 = value;
		Il2CppCodeGenWriteBarrier((&____container_0), value);
	}

	inline static int32_t get_offset_of__gameObjectBindInfo_1() { return static_cast<int32_t>(offsetof(EmptyGameObjectProvider_t99FDC7D0A141A3CB7F442969996294D2F3B749FC, ____gameObjectBindInfo_1)); }
	inline GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A * get__gameObjectBindInfo_1() const { return ____gameObjectBindInfo_1; }
	inline GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A ** get_address_of__gameObjectBindInfo_1() { return &____gameObjectBindInfo_1; }
	inline void set__gameObjectBindInfo_1(GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A * value)
	{
		____gameObjectBindInfo_1 = value;
		Il2CppCodeGenWriteBarrier((&____gameObjectBindInfo_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMPTYGAMEOBJECTPROVIDER_T99FDC7D0A141A3CB7F442969996294D2F3B749FC_H
#ifndef U3CGETALLINSTANCESWITHINJECTSPLITU3ED__4_T4FAC9337E075E17CA14A0882BA6A03AAA5470C6C_H
#define U3CGETALLINSTANCESWITHINJECTSPLITU3ED__4_T4FAC9337E075E17CA14A0882BA6A03AAA5470C6C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.EmptyGameObjectProvider_<GetAllInstancesWithInjectSplit>d__4
struct  U3CGetAllInstancesWithInjectSplitU3Ed__4_t4FAC9337E075E17CA14A0882BA6A03AAA5470C6C  : public RuntimeObject
{
public:
	// System.Int32 Zenject.EmptyGameObjectProvider_<GetAllInstancesWithInjectSplit>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Collections.Generic.List`1<System.Object> Zenject.EmptyGameObjectProvider_<GetAllInstancesWithInjectSplit>d__4::<>2__current
	List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * ___U3CU3E2__current_1;
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.EmptyGameObjectProvider_<GetAllInstancesWithInjectSplit>d__4::args
	List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * ___args_2;
	// Zenject.EmptyGameObjectProvider Zenject.EmptyGameObjectProvider_<GetAllInstancesWithInjectSplit>d__4::<>4__this
	EmptyGameObjectProvider_t99FDC7D0A141A3CB7F442969996294D2F3B749FC * ___U3CU3E4__this_3;
	// Zenject.InjectContext Zenject.EmptyGameObjectProvider_<GetAllInstancesWithInjectSplit>d__4::context
	InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * ___context_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__4_t4FAC9337E075E17CA14A0882BA6A03AAA5470C6C, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__4_t4FAC9337E075E17CA14A0882BA6A03AAA5470C6C, ___U3CU3E2__current_1)); }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_args_2() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__4_t4FAC9337E075E17CA14A0882BA6A03AAA5470C6C, ___args_2)); }
	inline List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * get_args_2() const { return ___args_2; }
	inline List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 ** get_address_of_args_2() { return &___args_2; }
	inline void set_args_2(List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * value)
	{
		___args_2 = value;
		Il2CppCodeGenWriteBarrier((&___args_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__4_t4FAC9337E075E17CA14A0882BA6A03AAA5470C6C, ___U3CU3E4__this_3)); }
	inline EmptyGameObjectProvider_t99FDC7D0A141A3CB7F442969996294D2F3B749FC * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline EmptyGameObjectProvider_t99FDC7D0A141A3CB7F442969996294D2F3B749FC ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(EmptyGameObjectProvider_t99FDC7D0A141A3CB7F442969996294D2F3B749FC * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_context_4() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__4_t4FAC9337E075E17CA14A0882BA6A03AAA5470C6C, ___context_4)); }
	inline InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * get_context_4() const { return ___context_4; }
	inline InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF ** get_address_of_context_4() { return &___context_4; }
	inline void set_context_4(InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * value)
	{
		___context_4 = value;
		Il2CppCodeGenWriteBarrier((&___context_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETALLINSTANCESWITHINJECTSPLITU3ED__4_T4FAC9337E075E17CA14A0882BA6A03AAA5470C6C_H
#ifndef GETFROMPREFABCOMPONENTPROVIDER_T42F7CECCDF7930D6C1C6F1397452ABE68BADBDF6_H
#define GETFROMPREFABCOMPONENTPROVIDER_T42F7CECCDF7930D6C1C6F1397452ABE68BADBDF6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.GetFromPrefabComponentProvider
struct  GetFromPrefabComponentProvider_t42F7CECCDF7930D6C1C6F1397452ABE68BADBDF6  : public RuntimeObject
{
public:
	// Zenject.IPrefabInstantiator Zenject.GetFromPrefabComponentProvider::_prefabInstantiator
	RuntimeObject* ____prefabInstantiator_0;
	// System.Type Zenject.GetFromPrefabComponentProvider::_componentType
	Type_t * ____componentType_1;

public:
	inline static int32_t get_offset_of__prefabInstantiator_0() { return static_cast<int32_t>(offsetof(GetFromPrefabComponentProvider_t42F7CECCDF7930D6C1C6F1397452ABE68BADBDF6, ____prefabInstantiator_0)); }
	inline RuntimeObject* get__prefabInstantiator_0() const { return ____prefabInstantiator_0; }
	inline RuntimeObject** get_address_of__prefabInstantiator_0() { return &____prefabInstantiator_0; }
	inline void set__prefabInstantiator_0(RuntimeObject* value)
	{
		____prefabInstantiator_0 = value;
		Il2CppCodeGenWriteBarrier((&____prefabInstantiator_0), value);
	}

	inline static int32_t get_offset_of__componentType_1() { return static_cast<int32_t>(offsetof(GetFromPrefabComponentProvider_t42F7CECCDF7930D6C1C6F1397452ABE68BADBDF6, ____componentType_1)); }
	inline Type_t * get__componentType_1() const { return ____componentType_1; }
	inline Type_t ** get_address_of__componentType_1() { return &____componentType_1; }
	inline void set__componentType_1(Type_t * value)
	{
		____componentType_1 = value;
		Il2CppCodeGenWriteBarrier((&____componentType_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETFROMPREFABCOMPONENTPROVIDER_T42F7CECCDF7930D6C1C6F1397452ABE68BADBDF6_H
#ifndef U3CGETALLINSTANCESWITHINJECTSPLITU3ED__4_TDAB2793AFCFBAD3222C1423FF34E9B27280F33BE_H
#define U3CGETALLINSTANCESWITHINJECTSPLITU3ED__4_TDAB2793AFCFBAD3222C1423FF34E9B27280F33BE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.GetFromPrefabComponentProvider_<GetAllInstancesWithInjectSplit>d__4
struct  U3CGetAllInstancesWithInjectSplitU3Ed__4_tDAB2793AFCFBAD3222C1423FF34E9B27280F33BE  : public RuntimeObject
{
public:
	// System.Int32 Zenject.GetFromPrefabComponentProvider_<GetAllInstancesWithInjectSplit>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Collections.Generic.List`1<System.Object> Zenject.GetFromPrefabComponentProvider_<GetAllInstancesWithInjectSplit>d__4::<>2__current
	List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * ___U3CU3E2__current_1;
	// Zenject.InjectContext Zenject.GetFromPrefabComponentProvider_<GetAllInstancesWithInjectSplit>d__4::context
	InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * ___context_2;
	// Zenject.GetFromPrefabComponentProvider Zenject.GetFromPrefabComponentProvider_<GetAllInstancesWithInjectSplit>d__4::<>4__this
	GetFromPrefabComponentProvider_t42F7CECCDF7930D6C1C6F1397452ABE68BADBDF6 * ___U3CU3E4__this_3;
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.GetFromPrefabComponentProvider_<GetAllInstancesWithInjectSplit>d__4::args
	List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * ___args_4;
	// System.Collections.Generic.IEnumerator`1<UnityEngine.GameObject> Zenject.GetFromPrefabComponentProvider_<GetAllInstancesWithInjectSplit>d__4::<gameObjectRunner>5__2
	RuntimeObject* ___U3CgameObjectRunnerU3E5__2_5;
	// System.Boolean Zenject.GetFromPrefabComponentProvider_<GetAllInstancesWithInjectSplit>d__4::<hasMore>5__3
	bool ___U3ChasMoreU3E5__3_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__4_tDAB2793AFCFBAD3222C1423FF34E9B27280F33BE, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__4_tDAB2793AFCFBAD3222C1423FF34E9B27280F33BE, ___U3CU3E2__current_1)); }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_context_2() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__4_tDAB2793AFCFBAD3222C1423FF34E9B27280F33BE, ___context_2)); }
	inline InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * get_context_2() const { return ___context_2; }
	inline InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF ** get_address_of_context_2() { return &___context_2; }
	inline void set_context_2(InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * value)
	{
		___context_2 = value;
		Il2CppCodeGenWriteBarrier((&___context_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__4_tDAB2793AFCFBAD3222C1423FF34E9B27280F33BE, ___U3CU3E4__this_3)); }
	inline GetFromPrefabComponentProvider_t42F7CECCDF7930D6C1C6F1397452ABE68BADBDF6 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline GetFromPrefabComponentProvider_t42F7CECCDF7930D6C1C6F1397452ABE68BADBDF6 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(GetFromPrefabComponentProvider_t42F7CECCDF7930D6C1C6F1397452ABE68BADBDF6 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_args_4() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__4_tDAB2793AFCFBAD3222C1423FF34E9B27280F33BE, ___args_4)); }
	inline List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * get_args_4() const { return ___args_4; }
	inline List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 ** get_address_of_args_4() { return &___args_4; }
	inline void set_args_4(List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * value)
	{
		___args_4 = value;
		Il2CppCodeGenWriteBarrier((&___args_4), value);
	}

	inline static int32_t get_offset_of_U3CgameObjectRunnerU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__4_tDAB2793AFCFBAD3222C1423FF34E9B27280F33BE, ___U3CgameObjectRunnerU3E5__2_5)); }
	inline RuntimeObject* get_U3CgameObjectRunnerU3E5__2_5() const { return ___U3CgameObjectRunnerU3E5__2_5; }
	inline RuntimeObject** get_address_of_U3CgameObjectRunnerU3E5__2_5() { return &___U3CgameObjectRunnerU3E5__2_5; }
	inline void set_U3CgameObjectRunnerU3E5__2_5(RuntimeObject* value)
	{
		___U3CgameObjectRunnerU3E5__2_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CgameObjectRunnerU3E5__2_5), value);
	}

	inline static int32_t get_offset_of_U3ChasMoreU3E5__3_6() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__4_tDAB2793AFCFBAD3222C1423FF34E9B27280F33BE, ___U3ChasMoreU3E5__3_6)); }
	inline bool get_U3ChasMoreU3E5__3_6() const { return ___U3ChasMoreU3E5__3_6; }
	inline bool* get_address_of_U3ChasMoreU3E5__3_6() { return &___U3ChasMoreU3E5__3_6; }
	inline void set_U3ChasMoreU3E5__3_6(bool value)
	{
		___U3ChasMoreU3E5__3_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETALLINSTANCESWITHINJECTSPLITU3ED__4_TDAB2793AFCFBAD3222C1423FF34E9B27280F33BE_H
#ifndef INJECTARGS_TD1EC7FDE37E60A7D38EF224E5FE6C8667639387C_H
#define INJECTARGS_TD1EC7FDE37E60A7D38EF224E5FE6C8667639387C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.InjectArgs
struct  InjectArgs_tD1EC7FDE37E60A7D38EF224E5FE6C8667639387C  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.InjectArgs::ExtraArgs
	List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * ___ExtraArgs_0;
	// Zenject.InjectContext Zenject.InjectArgs::Context
	InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * ___Context_1;
	// System.Object Zenject.InjectArgs::ConcreteIdentifier
	RuntimeObject * ___ConcreteIdentifier_2;

public:
	inline static int32_t get_offset_of_ExtraArgs_0() { return static_cast<int32_t>(offsetof(InjectArgs_tD1EC7FDE37E60A7D38EF224E5FE6C8667639387C, ___ExtraArgs_0)); }
	inline List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * get_ExtraArgs_0() const { return ___ExtraArgs_0; }
	inline List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 ** get_address_of_ExtraArgs_0() { return &___ExtraArgs_0; }
	inline void set_ExtraArgs_0(List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * value)
	{
		___ExtraArgs_0 = value;
		Il2CppCodeGenWriteBarrier((&___ExtraArgs_0), value);
	}

	inline static int32_t get_offset_of_Context_1() { return static_cast<int32_t>(offsetof(InjectArgs_tD1EC7FDE37E60A7D38EF224E5FE6C8667639387C, ___Context_1)); }
	inline InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * get_Context_1() const { return ___Context_1; }
	inline InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF ** get_address_of_Context_1() { return &___Context_1; }
	inline void set_Context_1(InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * value)
	{
		___Context_1 = value;
		Il2CppCodeGenWriteBarrier((&___Context_1), value);
	}

	inline static int32_t get_offset_of_ConcreteIdentifier_2() { return static_cast<int32_t>(offsetof(InjectArgs_tD1EC7FDE37E60A7D38EF224E5FE6C8667639387C, ___ConcreteIdentifier_2)); }
	inline RuntimeObject * get_ConcreteIdentifier_2() const { return ___ConcreteIdentifier_2; }
	inline RuntimeObject ** get_address_of_ConcreteIdentifier_2() { return &___ConcreteIdentifier_2; }
	inline void set_ConcreteIdentifier_2(RuntimeObject * value)
	{
		___ConcreteIdentifier_2 = value;
		Il2CppCodeGenWriteBarrier((&___ConcreteIdentifier_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INJECTARGS_TD1EC7FDE37E60A7D38EF224E5FE6C8667639387C_H
#ifndef INSTALLERBASE_T4F464ACE8FEB8CEEE7B4352A271F09894C28AD79_H
#define INSTALLERBASE_T4F464ACE8FEB8CEEE7B4352A271F09894C28AD79_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.InstallerBase
struct  InstallerBase_t4F464ACE8FEB8CEEE7B4352A271F09894C28AD79  : public RuntimeObject
{
public:
	// Zenject.DiContainer Zenject.InstallerBase::_container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____container_0;

public:
	inline static int32_t get_offset_of__container_0() { return static_cast<int32_t>(offsetof(InstallerBase_t4F464ACE8FEB8CEEE7B4352A271F09894C28AD79, ____container_0)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__container_0() const { return ____container_0; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__container_0() { return &____container_0; }
	inline void set__container_0(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____container_0 = value;
		Il2CppCodeGenWriteBarrier((&____container_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTALLERBASE_T4F464ACE8FEB8CEEE7B4352A271F09894C28AD79_H
#ifndef INSTANCEPROVIDER_T7063C82B8A97AAB00B4296990A3FFD011493A236_H
#define INSTANCEPROVIDER_T7063C82B8A97AAB00B4296990A3FFD011493A236_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.InstanceProvider
struct  InstanceProvider_t7063C82B8A97AAB00B4296990A3FFD011493A236  : public RuntimeObject
{
public:
	// System.Object Zenject.InstanceProvider::_instance
	RuntimeObject * ____instance_0;
	// System.Type Zenject.InstanceProvider::_instanceType
	Type_t * ____instanceType_1;
	// Zenject.DiContainer Zenject.InstanceProvider::_container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____container_2;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(InstanceProvider_t7063C82B8A97AAB00B4296990A3FFD011493A236, ____instance_0)); }
	inline RuntimeObject * get__instance_0() const { return ____instance_0; }
	inline RuntimeObject ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(RuntimeObject * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier((&____instance_0), value);
	}

	inline static int32_t get_offset_of__instanceType_1() { return static_cast<int32_t>(offsetof(InstanceProvider_t7063C82B8A97AAB00B4296990A3FFD011493A236, ____instanceType_1)); }
	inline Type_t * get__instanceType_1() const { return ____instanceType_1; }
	inline Type_t ** get_address_of__instanceType_1() { return &____instanceType_1; }
	inline void set__instanceType_1(Type_t * value)
	{
		____instanceType_1 = value;
		Il2CppCodeGenWriteBarrier((&____instanceType_1), value);
	}

	inline static int32_t get_offset_of__container_2() { return static_cast<int32_t>(offsetof(InstanceProvider_t7063C82B8A97AAB00B4296990A3FFD011493A236, ____container_2)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__container_2() const { return ____container_2; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__container_2() { return &____container_2; }
	inline void set__container_2(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____container_2 = value;
		Il2CppCodeGenWriteBarrier((&____container_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTANCEPROVIDER_T7063C82B8A97AAB00B4296990A3FFD011493A236_H
#ifndef INSTANTIATEONPREFABCOMPONENTPROVIDER_T1445E68D6F2B17287C0FDE478F5E751A3FBBDF78_H
#define INSTANTIATEONPREFABCOMPONENTPROVIDER_T1445E68D6F2B17287C0FDE478F5E751A3FBBDF78_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.InstantiateOnPrefabComponentProvider
struct  InstantiateOnPrefabComponentProvider_t1445E68D6F2B17287C0FDE478F5E751A3FBBDF78  : public RuntimeObject
{
public:
	// Zenject.IPrefabInstantiator Zenject.InstantiateOnPrefabComponentProvider::_prefabInstantiator
	RuntimeObject* ____prefabInstantiator_0;
	// System.Type Zenject.InstantiateOnPrefabComponentProvider::_componentType
	Type_t * ____componentType_1;

public:
	inline static int32_t get_offset_of__prefabInstantiator_0() { return static_cast<int32_t>(offsetof(InstantiateOnPrefabComponentProvider_t1445E68D6F2B17287C0FDE478F5E751A3FBBDF78, ____prefabInstantiator_0)); }
	inline RuntimeObject* get__prefabInstantiator_0() const { return ____prefabInstantiator_0; }
	inline RuntimeObject** get_address_of__prefabInstantiator_0() { return &____prefabInstantiator_0; }
	inline void set__prefabInstantiator_0(RuntimeObject* value)
	{
		____prefabInstantiator_0 = value;
		Il2CppCodeGenWriteBarrier((&____prefabInstantiator_0), value);
	}

	inline static int32_t get_offset_of__componentType_1() { return static_cast<int32_t>(offsetof(InstantiateOnPrefabComponentProvider_t1445E68D6F2B17287C0FDE478F5E751A3FBBDF78, ____componentType_1)); }
	inline Type_t * get__componentType_1() const { return ____componentType_1; }
	inline Type_t ** get_address_of__componentType_1() { return &____componentType_1; }
	inline void set__componentType_1(Type_t * value)
	{
		____componentType_1 = value;
		Il2CppCodeGenWriteBarrier((&____componentType_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTANTIATEONPREFABCOMPONENTPROVIDER_T1445E68D6F2B17287C0FDE478F5E751A3FBBDF78_H
#ifndef U3CGETALLINSTANCESWITHINJECTSPLITU3ED__4_T263A67D351300C40A7B53B4E2C68D77CE96DB9BC_H
#define U3CGETALLINSTANCESWITHINJECTSPLITU3ED__4_T263A67D351300C40A7B53B4E2C68D77CE96DB9BC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.InstantiateOnPrefabComponentProvider_<GetAllInstancesWithInjectSplit>d__4
struct  U3CGetAllInstancesWithInjectSplitU3Ed__4_t263A67D351300C40A7B53B4E2C68D77CE96DB9BC  : public RuntimeObject
{
public:
	// System.Int32 Zenject.InstantiateOnPrefabComponentProvider_<GetAllInstancesWithInjectSplit>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Collections.Generic.List`1<System.Object> Zenject.InstantiateOnPrefabComponentProvider_<GetAllInstancesWithInjectSplit>d__4::<>2__current
	List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * ___U3CU3E2__current_1;
	// Zenject.InjectContext Zenject.InstantiateOnPrefabComponentProvider_<GetAllInstancesWithInjectSplit>d__4::context
	InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * ___context_2;
	// Zenject.InstantiateOnPrefabComponentProvider Zenject.InstantiateOnPrefabComponentProvider_<GetAllInstancesWithInjectSplit>d__4::<>4__this
	InstantiateOnPrefabComponentProvider_t1445E68D6F2B17287C0FDE478F5E751A3FBBDF78 * ___U3CU3E4__this_3;
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.InstantiateOnPrefabComponentProvider_<GetAllInstancesWithInjectSplit>d__4::args
	List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * ___args_4;
	// System.Collections.Generic.IEnumerator`1<UnityEngine.GameObject> Zenject.InstantiateOnPrefabComponentProvider_<GetAllInstancesWithInjectSplit>d__4::<gameObjectRunner>5__2
	RuntimeObject* ___U3CgameObjectRunnerU3E5__2_5;
	// System.Boolean Zenject.InstantiateOnPrefabComponentProvider_<GetAllInstancesWithInjectSplit>d__4::<hasMore>5__3
	bool ___U3ChasMoreU3E5__3_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__4_t263A67D351300C40A7B53B4E2C68D77CE96DB9BC, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__4_t263A67D351300C40A7B53B4E2C68D77CE96DB9BC, ___U3CU3E2__current_1)); }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_context_2() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__4_t263A67D351300C40A7B53B4E2C68D77CE96DB9BC, ___context_2)); }
	inline InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * get_context_2() const { return ___context_2; }
	inline InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF ** get_address_of_context_2() { return &___context_2; }
	inline void set_context_2(InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * value)
	{
		___context_2 = value;
		Il2CppCodeGenWriteBarrier((&___context_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__4_t263A67D351300C40A7B53B4E2C68D77CE96DB9BC, ___U3CU3E4__this_3)); }
	inline InstantiateOnPrefabComponentProvider_t1445E68D6F2B17287C0FDE478F5E751A3FBBDF78 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline InstantiateOnPrefabComponentProvider_t1445E68D6F2B17287C0FDE478F5E751A3FBBDF78 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(InstantiateOnPrefabComponentProvider_t1445E68D6F2B17287C0FDE478F5E751A3FBBDF78 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_args_4() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__4_t263A67D351300C40A7B53B4E2C68D77CE96DB9BC, ___args_4)); }
	inline List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * get_args_4() const { return ___args_4; }
	inline List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 ** get_address_of_args_4() { return &___args_4; }
	inline void set_args_4(List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * value)
	{
		___args_4 = value;
		Il2CppCodeGenWriteBarrier((&___args_4), value);
	}

	inline static int32_t get_offset_of_U3CgameObjectRunnerU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__4_t263A67D351300C40A7B53B4E2C68D77CE96DB9BC, ___U3CgameObjectRunnerU3E5__2_5)); }
	inline RuntimeObject* get_U3CgameObjectRunnerU3E5__2_5() const { return ___U3CgameObjectRunnerU3E5__2_5; }
	inline RuntimeObject** get_address_of_U3CgameObjectRunnerU3E5__2_5() { return &___U3CgameObjectRunnerU3E5__2_5; }
	inline void set_U3CgameObjectRunnerU3E5__2_5(RuntimeObject* value)
	{
		___U3CgameObjectRunnerU3E5__2_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CgameObjectRunnerU3E5__2_5), value);
	}

	inline static int32_t get_offset_of_U3ChasMoreU3E5__3_6() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__4_t263A67D351300C40A7B53B4E2C68D77CE96DB9BC, ___U3ChasMoreU3E5__3_6)); }
	inline bool get_U3ChasMoreU3E5__3_6() const { return ___U3ChasMoreU3E5__3_6; }
	inline bool* get_address_of_U3ChasMoreU3E5__3_6() { return &___U3ChasMoreU3E5__3_6; }
	inline void set_U3ChasMoreU3E5__3_6(bool value)
	{
		___U3ChasMoreU3E5__3_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETALLINSTANCESWITHINJECTSPLITU3ED__4_T263A67D351300C40A7B53B4E2C68D77CE96DB9BC_H
#ifndef LAZYINSTANCEINJECTOR_T0120534A9142C2F3AC0A2A87E5CAC778A6F831A6_H
#define LAZYINSTANCEINJECTOR_T0120534A9142C2F3AC0A2A87E5CAC778A6F831A6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.LazyInstanceInjector
struct  LazyInstanceInjector_t0120534A9142C2F3AC0A2A87E5CAC778A6F831A6  : public RuntimeObject
{
public:
	// Zenject.DiContainer Zenject.LazyInstanceInjector::_container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____container_0;
	// System.Collections.Generic.HashSet`1<System.Object> Zenject.LazyInstanceInjector::_instancesToInject
	HashSet_1_t297CD7F944846107B388993164FCD9E317A338A3 * ____instancesToInject_1;

public:
	inline static int32_t get_offset_of__container_0() { return static_cast<int32_t>(offsetof(LazyInstanceInjector_t0120534A9142C2F3AC0A2A87E5CAC778A6F831A6, ____container_0)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__container_0() const { return ____container_0; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__container_0() { return &____container_0; }
	inline void set__container_0(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____container_0 = value;
		Il2CppCodeGenWriteBarrier((&____container_0), value);
	}

	inline static int32_t get_offset_of__instancesToInject_1() { return static_cast<int32_t>(offsetof(LazyInstanceInjector_t0120534A9142C2F3AC0A2A87E5CAC778A6F831A6, ____instancesToInject_1)); }
	inline HashSet_1_t297CD7F944846107B388993164FCD9E317A338A3 * get__instancesToInject_1() const { return ____instancesToInject_1; }
	inline HashSet_1_t297CD7F944846107B388993164FCD9E317A338A3 ** get_address_of__instancesToInject_1() { return &____instancesToInject_1; }
	inline void set__instancesToInject_1(HashSet_1_t297CD7F944846107B388993164FCD9E317A338A3 * value)
	{
		____instancesToInject_1 = value;
		Il2CppCodeGenWriteBarrier((&____instancesToInject_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAZYINSTANCEINJECTOR_T0120534A9142C2F3AC0A2A87E5CAC778A6F831A6_H
#ifndef MONOINSTALLERUTIL_T27880052EDA631B23E6D4C4BE34A2C06F4B1AA50_H
#define MONOINSTALLERUTIL_T27880052EDA631B23E6D4C4BE34A2C06F4B1AA50_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.MonoInstallerUtil
struct  MonoInstallerUtil_t27880052EDA631B23E6D4C4BE34A2C06F4B1AA50  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOINSTALLERUTIL_T27880052EDA631B23E6D4C4BE34A2C06F4B1AA50_H
#ifndef PREFABGAMEOBJECTPROVIDER_T194304A9DD65D5EF02263FE908B286D92CE4D8C3_H
#define PREFABGAMEOBJECTPROVIDER_T194304A9DD65D5EF02263FE908B286D92CE4D8C3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.PrefabGameObjectProvider
struct  PrefabGameObjectProvider_t194304A9DD65D5EF02263FE908B286D92CE4D8C3  : public RuntimeObject
{
public:
	// Zenject.IPrefabInstantiator Zenject.PrefabGameObjectProvider::_prefabCreator
	RuntimeObject* ____prefabCreator_0;

public:
	inline static int32_t get_offset_of__prefabCreator_0() { return static_cast<int32_t>(offsetof(PrefabGameObjectProvider_t194304A9DD65D5EF02263FE908B286D92CE4D8C3, ____prefabCreator_0)); }
	inline RuntimeObject* get__prefabCreator_0() const { return ____prefabCreator_0; }
	inline RuntimeObject** get_address_of__prefabCreator_0() { return &____prefabCreator_0; }
	inline void set__prefabCreator_0(RuntimeObject* value)
	{
		____prefabCreator_0 = value;
		Il2CppCodeGenWriteBarrier((&____prefabCreator_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREFABGAMEOBJECTPROVIDER_T194304A9DD65D5EF02263FE908B286D92CE4D8C3_H
#ifndef U3CGETALLINSTANCESWITHINJECTSPLITU3ED__3_TDF6F2C9DF8DA0E4BEC31D405E85E2E2E899AB08C_H
#define U3CGETALLINSTANCESWITHINJECTSPLITU3ED__3_TDF6F2C9DF8DA0E4BEC31D405E85E2E2E899AB08C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.PrefabGameObjectProvider_<GetAllInstancesWithInjectSplit>d__3
struct  U3CGetAllInstancesWithInjectSplitU3Ed__3_tDF6F2C9DF8DA0E4BEC31D405E85E2E2E899AB08C  : public RuntimeObject
{
public:
	// System.Int32 Zenject.PrefabGameObjectProvider_<GetAllInstancesWithInjectSplit>d__3::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Collections.Generic.List`1<System.Object> Zenject.PrefabGameObjectProvider_<GetAllInstancesWithInjectSplit>d__3::<>2__current
	List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * ___U3CU3E2__current_1;
	// Zenject.PrefabGameObjectProvider Zenject.PrefabGameObjectProvider_<GetAllInstancesWithInjectSplit>d__3::<>4__this
	PrefabGameObjectProvider_t194304A9DD65D5EF02263FE908B286D92CE4D8C3 * ___U3CU3E4__this_2;
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.PrefabGameObjectProvider_<GetAllInstancesWithInjectSplit>d__3::args
	List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * ___args_3;
	// System.Collections.Generic.IEnumerator`1<UnityEngine.GameObject> Zenject.PrefabGameObjectProvider_<GetAllInstancesWithInjectSplit>d__3::<runner>5__2
	RuntimeObject* ___U3CrunnerU3E5__2_4;
	// System.Boolean Zenject.PrefabGameObjectProvider_<GetAllInstancesWithInjectSplit>d__3::<hasMore>5__3
	bool ___U3ChasMoreU3E5__3_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__3_tDF6F2C9DF8DA0E4BEC31D405E85E2E2E899AB08C, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__3_tDF6F2C9DF8DA0E4BEC31D405E85E2E2E899AB08C, ___U3CU3E2__current_1)); }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__3_tDF6F2C9DF8DA0E4BEC31D405E85E2E2E899AB08C, ___U3CU3E4__this_2)); }
	inline PrefabGameObjectProvider_t194304A9DD65D5EF02263FE908B286D92CE4D8C3 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline PrefabGameObjectProvider_t194304A9DD65D5EF02263FE908B286D92CE4D8C3 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(PrefabGameObjectProvider_t194304A9DD65D5EF02263FE908B286D92CE4D8C3 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_args_3() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__3_tDF6F2C9DF8DA0E4BEC31D405E85E2E2E899AB08C, ___args_3)); }
	inline List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * get_args_3() const { return ___args_3; }
	inline List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 ** get_address_of_args_3() { return &___args_3; }
	inline void set_args_3(List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * value)
	{
		___args_3 = value;
		Il2CppCodeGenWriteBarrier((&___args_3), value);
	}

	inline static int32_t get_offset_of_U3CrunnerU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__3_tDF6F2C9DF8DA0E4BEC31D405E85E2E2E899AB08C, ___U3CrunnerU3E5__2_4)); }
	inline RuntimeObject* get_U3CrunnerU3E5__2_4() const { return ___U3CrunnerU3E5__2_4; }
	inline RuntimeObject** get_address_of_U3CrunnerU3E5__2_4() { return &___U3CrunnerU3E5__2_4; }
	inline void set_U3CrunnerU3E5__2_4(RuntimeObject* value)
	{
		___U3CrunnerU3E5__2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrunnerU3E5__2_4), value);
	}

	inline static int32_t get_offset_of_U3ChasMoreU3E5__3_5() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__3_tDF6F2C9DF8DA0E4BEC31D405E85E2E2E899AB08C, ___U3ChasMoreU3E5__3_5)); }
	inline bool get_U3ChasMoreU3E5__3_5() const { return ___U3ChasMoreU3E5__3_5; }
	inline bool* get_address_of_U3ChasMoreU3E5__3_5() { return &___U3ChasMoreU3E5__3_5; }
	inline void set_U3ChasMoreU3E5__3_5(bool value)
	{
		___U3ChasMoreU3E5__3_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETALLINSTANCESWITHINJECTSPLITU3ED__3_TDF6F2C9DF8DA0E4BEC31D405E85E2E2E899AB08C_H
#ifndef U3CU3EC_T7EB23F8FFAF0F64E9848F096FF193DADE635199B_H
#define U3CU3EC_T7EB23F8FFAF0F64E9848F096FF193DADE635199B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SceneContext_<>c
struct  U3CU3Ec_t7EB23F8FFAF0F64E9848F096FF193DADE635199B  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t7EB23F8FFAF0F64E9848F096FF193DADE635199B_StaticFields
{
public:
	// Zenject.SceneContext_<>c Zenject.SceneContext_<>c::<>9
	U3CU3Ec_t7EB23F8FFAF0F64E9848F096FF193DADE635199B * ___U3CU3E9_0;
	// System.Func`2<UnityEngine.SceneManagement.Scene,System.Collections.Generic.IEnumerable`1<UnityEngine.GameObject>> Zenject.SceneContext_<>c::<>9__28_0
	Func_2_tFAFA393FCFA2B792ED7E14FB5E6990E11CBBAF46 * ___U3CU3E9__28_0_1;
	// System.Func`2<UnityEngine.GameObject,System.Collections.Generic.IEnumerable`1<Zenject.SceneContext>> Zenject.SceneContext_<>c::<>9__28_1
	Func_2_t6D143D5B850346EA82D99E2A69981B807B171CF6 * ___U3CU3E9__28_1_2;
	// System.Func`2<Zenject.SceneContext,Zenject.DiContainer> Zenject.SceneContext_<>c::<>9__28_3
	Func_2_tD6FA67F8E051BBD2664B65E112F6CA7FB16B04E9 * ___U3CU3E9__28_3_3;
	// System.Func`2<UnityEngine.SceneManagement.Scene,System.Collections.Generic.IEnumerable`1<UnityEngine.GameObject>> Zenject.SceneContext_<>c::<>9__29_0
	Func_2_tFAFA393FCFA2B792ED7E14FB5E6990E11CBBAF46 * ___U3CU3E9__29_0_4;
	// System.Func`2<UnityEngine.GameObject,System.Collections.Generic.IEnumerable`1<Zenject.SceneDecoratorContext>> Zenject.SceneContext_<>c::<>9__29_1
	Func_2_t9999430700062AC839370FE209CB4F33E8A3A67B * ___U3CU3E9__29_1_5;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t7EB23F8FFAF0F64E9848F096FF193DADE635199B_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t7EB23F8FFAF0F64E9848F096FF193DADE635199B * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t7EB23F8FFAF0F64E9848F096FF193DADE635199B ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t7EB23F8FFAF0F64E9848F096FF193DADE635199B * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__28_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t7EB23F8FFAF0F64E9848F096FF193DADE635199B_StaticFields, ___U3CU3E9__28_0_1)); }
	inline Func_2_tFAFA393FCFA2B792ED7E14FB5E6990E11CBBAF46 * get_U3CU3E9__28_0_1() const { return ___U3CU3E9__28_0_1; }
	inline Func_2_tFAFA393FCFA2B792ED7E14FB5E6990E11CBBAF46 ** get_address_of_U3CU3E9__28_0_1() { return &___U3CU3E9__28_0_1; }
	inline void set_U3CU3E9__28_0_1(Func_2_tFAFA393FCFA2B792ED7E14FB5E6990E11CBBAF46 * value)
	{
		___U3CU3E9__28_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__28_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__28_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t7EB23F8FFAF0F64E9848F096FF193DADE635199B_StaticFields, ___U3CU3E9__28_1_2)); }
	inline Func_2_t6D143D5B850346EA82D99E2A69981B807B171CF6 * get_U3CU3E9__28_1_2() const { return ___U3CU3E9__28_1_2; }
	inline Func_2_t6D143D5B850346EA82D99E2A69981B807B171CF6 ** get_address_of_U3CU3E9__28_1_2() { return &___U3CU3E9__28_1_2; }
	inline void set_U3CU3E9__28_1_2(Func_2_t6D143D5B850346EA82D99E2A69981B807B171CF6 * value)
	{
		___U3CU3E9__28_1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__28_1_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__28_3_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t7EB23F8FFAF0F64E9848F096FF193DADE635199B_StaticFields, ___U3CU3E9__28_3_3)); }
	inline Func_2_tD6FA67F8E051BBD2664B65E112F6CA7FB16B04E9 * get_U3CU3E9__28_3_3() const { return ___U3CU3E9__28_3_3; }
	inline Func_2_tD6FA67F8E051BBD2664B65E112F6CA7FB16B04E9 ** get_address_of_U3CU3E9__28_3_3() { return &___U3CU3E9__28_3_3; }
	inline void set_U3CU3E9__28_3_3(Func_2_tD6FA67F8E051BBD2664B65E112F6CA7FB16B04E9 * value)
	{
		___U3CU3E9__28_3_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__28_3_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__29_0_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_t7EB23F8FFAF0F64E9848F096FF193DADE635199B_StaticFields, ___U3CU3E9__29_0_4)); }
	inline Func_2_tFAFA393FCFA2B792ED7E14FB5E6990E11CBBAF46 * get_U3CU3E9__29_0_4() const { return ___U3CU3E9__29_0_4; }
	inline Func_2_tFAFA393FCFA2B792ED7E14FB5E6990E11CBBAF46 ** get_address_of_U3CU3E9__29_0_4() { return &___U3CU3E9__29_0_4; }
	inline void set_U3CU3E9__29_0_4(Func_2_tFAFA393FCFA2B792ED7E14FB5E6990E11CBBAF46 * value)
	{
		___U3CU3E9__29_0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__29_0_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__29_1_5() { return static_cast<int32_t>(offsetof(U3CU3Ec_t7EB23F8FFAF0F64E9848F096FF193DADE635199B_StaticFields, ___U3CU3E9__29_1_5)); }
	inline Func_2_t9999430700062AC839370FE209CB4F33E8A3A67B * get_U3CU3E9__29_1_5() const { return ___U3CU3E9__29_1_5; }
	inline Func_2_t9999430700062AC839370FE209CB4F33E8A3A67B ** get_address_of_U3CU3E9__29_1_5() { return &___U3CU3E9__29_1_5; }
	inline void set_U3CU3E9__29_1_5(Func_2_t9999430700062AC839370FE209CB4F33E8A3A67B * value)
	{
		___U3CU3E9__29_1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__29_1_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T7EB23F8FFAF0F64E9848F096FF193DADE635199B_H
#ifndef U3CU3EC__DISPLAYCLASS28_0_TFF8D082EF9DA6AC69721ED1899C89CAA9DB8CCBC_H
#define U3CU3EC__DISPLAYCLASS28_0_TFF8D082EF9DA6AC69721ED1899C89CAA9DB8CCBC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SceneContext_<>c__DisplayClass28_0
struct  U3CU3Ec__DisplayClass28_0_tFF8D082EF9DA6AC69721ED1899C89CAA9DB8CCBC  : public RuntimeObject
{
public:
	// System.Collections.Generic.IEnumerable`1<System.String> Zenject.SceneContext_<>c__DisplayClass28_0::parentContractNames
	RuntimeObject* ___parentContractNames_0;
	// System.Func`2<System.String,System.Boolean> Zenject.SceneContext_<>c__DisplayClass28_0::<>9__4
	Func_2_t3AD4B0F443BFD399C4AC2D6EE99FFE3BC0970017 * ___U3CU3E9__4_1;

public:
	inline static int32_t get_offset_of_parentContractNames_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass28_0_tFF8D082EF9DA6AC69721ED1899C89CAA9DB8CCBC, ___parentContractNames_0)); }
	inline RuntimeObject* get_parentContractNames_0() const { return ___parentContractNames_0; }
	inline RuntimeObject** get_address_of_parentContractNames_0() { return &___parentContractNames_0; }
	inline void set_parentContractNames_0(RuntimeObject* value)
	{
		___parentContractNames_0 = value;
		Il2CppCodeGenWriteBarrier((&___parentContractNames_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__4_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass28_0_tFF8D082EF9DA6AC69721ED1899C89CAA9DB8CCBC, ___U3CU3E9__4_1)); }
	inline Func_2_t3AD4B0F443BFD399C4AC2D6EE99FFE3BC0970017 * get_U3CU3E9__4_1() const { return ___U3CU3E9__4_1; }
	inline Func_2_t3AD4B0F443BFD399C4AC2D6EE99FFE3BC0970017 ** get_address_of_U3CU3E9__4_1() { return &___U3CU3E9__4_1; }
	inline void set_U3CU3E9__4_1(Func_2_t3AD4B0F443BFD399C4AC2D6EE99FFE3BC0970017 * value)
	{
		___U3CU3E9__4_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__4_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS28_0_TFF8D082EF9DA6AC69721ED1899C89CAA9DB8CCBC_H
#ifndef U3CU3EC__DISPLAYCLASS30_0_T05114B7B3F36618C82FE6D9E90DEE11F4BBA658A_H
#define U3CU3EC__DISPLAYCLASS30_0_T05114B7B3F36618C82FE6D9E90DEE11F4BBA658A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SceneContext_<>c__DisplayClass30_0
struct  U3CU3Ec__DisplayClass30_0_t05114B7B3F36618C82FE6D9E90DEE11F4BBA658A  : public RuntimeObject
{
public:
	// System.Collections.Generic.IEnumerable`1<Zenject.DiContainer> Zenject.SceneContext_<>c__DisplayClass30_0::parents
	RuntimeObject* ___parents_0;

public:
	inline static int32_t get_offset_of_parents_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass30_0_t05114B7B3F36618C82FE6D9E90DEE11F4BBA658A, ___parents_0)); }
	inline RuntimeObject* get_parents_0() const { return ___parents_0; }
	inline RuntimeObject** get_address_of_parents_0() { return &___parents_0; }
	inline void set_parents_0(RuntimeObject* value)
	{
		___parents_0 = value;
		Il2CppCodeGenWriteBarrier((&___parents_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS30_0_T05114B7B3F36618C82FE6D9E90DEE11F4BBA658A_H
#ifndef SCRIPTABLEOBJECTINSTALLERUTIL_TF39DAB5971870E69DFFF738578E1FF1351AD4818_H
#define SCRIPTABLEOBJECTINSTALLERUTIL_TF39DAB5971870E69DFFF738578E1FF1351AD4818_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ScriptableObjectInstallerUtil
struct  ScriptableObjectInstallerUtil_tF39DAB5971870E69DFFF738578E1FF1351AD4818  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRIPTABLEOBJECTINSTALLERUTIL_TF39DAB5971870E69DFFF738578E1FF1351AD4818_H
#ifndef STATICCONTEXT_TAC293A700E899C8140D66B0A4C0FB5883EE5069F_H
#define STATICCONTEXT_TAC293A700E899C8140D66B0A4C0FB5883EE5069F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.StaticContext
struct  StaticContext_tAC293A700E899C8140D66B0A4C0FB5883EE5069F  : public RuntimeObject
{
public:

public:
};

struct StaticContext_tAC293A700E899C8140D66B0A4C0FB5883EE5069F_StaticFields
{
public:
	// Zenject.DiContainer Zenject.StaticContext::_container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____container_0;

public:
	inline static int32_t get_offset_of__container_0() { return static_cast<int32_t>(offsetof(StaticContext_tAC293A700E899C8140D66B0A4C0FB5883EE5069F_StaticFields, ____container_0)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__container_0() const { return ____container_0; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__container_0() { return &____container_0; }
	inline void set__container_0(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____container_0 = value;
		Il2CppCodeGenWriteBarrier((&____container_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATICCONTEXT_TAC293A700E899C8140D66B0A4C0FB5883EE5069F_H
#ifndef ENUMERATOR_T3D4AC3127853FAB2F510C6C9DE72C63A09E0BF91_H
#define ENUMERATOR_T3D4AC3127853FAB2F510C6C9DE72C63A09E0BF91_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1_Enumerator<Zenject.DiContainer>
struct  Enumerator_t3D4AC3127853FAB2F510C6C9DE72C63A09E0BF91 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1_Enumerator::list
	List_1_tC0C0C06645444D1ED985242DB086B9AFC1D8E1FA * ___list_0;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1_Enumerator::current
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_t3D4AC3127853FAB2F510C6C9DE72C63A09E0BF91, ___list_0)); }
	inline List_1_tC0C0C06645444D1ED985242DB086B9AFC1D8E1FA * get_list_0() const { return ___list_0; }
	inline List_1_tC0C0C06645444D1ED985242DB086B9AFC1D8E1FA ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_tC0C0C06645444D1ED985242DB086B9AFC1D8E1FA * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_t3D4AC3127853FAB2F510C6C9DE72C63A09E0BF91, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_t3D4AC3127853FAB2F510C6C9DE72C63A09E0BF91, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t3D4AC3127853FAB2F510C6C9DE72C63A09E0BF91, ___current_3)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get_current_3() const { return ___current_3; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T3D4AC3127853FAB2F510C6C9DE72C63A09E0BF91_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef ADDTOEXISTINGGAMEOBJECTCOMPONENTPROVIDER_T9B0432997ED1107ED7D91EF5252E6929A485AAAA_H
#define ADDTOEXISTINGGAMEOBJECTCOMPONENTPROVIDER_T9B0432997ED1107ED7D91EF5252E6929A485AAAA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.AddToExistingGameObjectComponentProvider
struct  AddToExistingGameObjectComponentProvider_t9B0432997ED1107ED7D91EF5252E6929A485AAAA  : public AddToGameObjectComponentProviderBase_tACB741545D819318E619C706D60A87C16EAC5434
{
public:
	// UnityEngine.GameObject Zenject.AddToExistingGameObjectComponentProvider::_gameObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____gameObject_4;

public:
	inline static int32_t get_offset_of__gameObject_4() { return static_cast<int32_t>(offsetof(AddToExistingGameObjectComponentProvider_t9B0432997ED1107ED7D91EF5252E6929A485AAAA, ____gameObject_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__gameObject_4() const { return ____gameObject_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__gameObject_4() { return &____gameObject_4; }
	inline void set__gameObject_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____gameObject_4 = value;
		Il2CppCodeGenWriteBarrier((&____gameObject_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADDTOEXISTINGGAMEOBJECTCOMPONENTPROVIDER_T9B0432997ED1107ED7D91EF5252E6929A485AAAA_H
#ifndef ADDTOEXISTINGGAMEOBJECTCOMPONENTPROVIDERGETTER_T90117CDD7BC87E54FA50EAE29E6EAFE3DEBF61D2_H
#define ADDTOEXISTINGGAMEOBJECTCOMPONENTPROVIDERGETTER_T90117CDD7BC87E54FA50EAE29E6EAFE3DEBF61D2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.AddToExistingGameObjectComponentProviderGetter
struct  AddToExistingGameObjectComponentProviderGetter_t90117CDD7BC87E54FA50EAE29E6EAFE3DEBF61D2  : public AddToGameObjectComponentProviderBase_tACB741545D819318E619C706D60A87C16EAC5434
{
public:
	// System.Func`2<Zenject.InjectContext,UnityEngine.GameObject> Zenject.AddToExistingGameObjectComponentProviderGetter::_gameObjectGetter
	Func_2_tD5D4A9F4A7BCFBB67D83DD4F52BFE19A8153D2D1 * ____gameObjectGetter_4;

public:
	inline static int32_t get_offset_of__gameObjectGetter_4() { return static_cast<int32_t>(offsetof(AddToExistingGameObjectComponentProviderGetter_t90117CDD7BC87E54FA50EAE29E6EAFE3DEBF61D2, ____gameObjectGetter_4)); }
	inline Func_2_tD5D4A9F4A7BCFBB67D83DD4F52BFE19A8153D2D1 * get__gameObjectGetter_4() const { return ____gameObjectGetter_4; }
	inline Func_2_tD5D4A9F4A7BCFBB67D83DD4F52BFE19A8153D2D1 ** get_address_of__gameObjectGetter_4() { return &____gameObjectGetter_4; }
	inline void set__gameObjectGetter_4(Func_2_tD5D4A9F4A7BCFBB67D83DD4F52BFE19A8153D2D1 * value)
	{
		____gameObjectGetter_4 = value;
		Il2CppCodeGenWriteBarrier((&____gameObjectGetter_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADDTOEXISTINGGAMEOBJECTCOMPONENTPROVIDERGETTER_T90117CDD7BC87E54FA50EAE29E6EAFE3DEBF61D2_H
#ifndef ADDTONEWGAMEOBJECTCOMPONENTPROVIDER_T56FB68E5B31DF7DA3848423315F5544109E54063_H
#define ADDTONEWGAMEOBJECTCOMPONENTPROVIDER_T56FB68E5B31DF7DA3848423315F5544109E54063_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.AddToNewGameObjectComponentProvider
struct  AddToNewGameObjectComponentProvider_t56FB68E5B31DF7DA3848423315F5544109E54063  : public AddToGameObjectComponentProviderBase_tACB741545D819318E619C706D60A87C16EAC5434
{
public:
	// Zenject.GameObjectCreationParameters Zenject.AddToNewGameObjectComponentProvider::_gameObjectBindInfo
	GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A * ____gameObjectBindInfo_4;

public:
	inline static int32_t get_offset_of__gameObjectBindInfo_4() { return static_cast<int32_t>(offsetof(AddToNewGameObjectComponentProvider_t56FB68E5B31DF7DA3848423315F5544109E54063, ____gameObjectBindInfo_4)); }
	inline GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A * get__gameObjectBindInfo_4() const { return ____gameObjectBindInfo_4; }
	inline GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A ** get_address_of__gameObjectBindInfo_4() { return &____gameObjectBindInfo_4; }
	inline void set__gameObjectBindInfo_4(GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A * value)
	{
		____gameObjectBindInfo_4 = value;
		Il2CppCodeGenWriteBarrier((&____gameObjectBindInfo_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADDTONEWGAMEOBJECTCOMPONENTPROVIDER_T56FB68E5B31DF7DA3848423315F5544109E54063_H
#ifndef LOOKUPID_T3D88DB078E23694C787C1892C681AB1F34416808_H
#define LOOKUPID_T3D88DB078E23694C787C1892C681AB1F34416808_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.DiContainer_LookupId
struct  LookupId_t3D88DB078E23694C787C1892C681AB1F34416808 
{
public:
	// Zenject.IProvider Zenject.DiContainer_LookupId::Provider
	RuntimeObject* ___Provider_0;
	// Zenject.BindingId Zenject.DiContainer_LookupId::BindingId
	BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA * ___BindingId_1;

public:
	inline static int32_t get_offset_of_Provider_0() { return static_cast<int32_t>(offsetof(LookupId_t3D88DB078E23694C787C1892C681AB1F34416808, ___Provider_0)); }
	inline RuntimeObject* get_Provider_0() const { return ___Provider_0; }
	inline RuntimeObject** get_address_of_Provider_0() { return &___Provider_0; }
	inline void set_Provider_0(RuntimeObject* value)
	{
		___Provider_0 = value;
		Il2CppCodeGenWriteBarrier((&___Provider_0), value);
	}

	inline static int32_t get_offset_of_BindingId_1() { return static_cast<int32_t>(offsetof(LookupId_t3D88DB078E23694C787C1892C681AB1F34416808, ___BindingId_1)); }
	inline BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA * get_BindingId_1() const { return ___BindingId_1; }
	inline BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA ** get_address_of_BindingId_1() { return &___BindingId_1; }
	inline void set_BindingId_1(BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA * value)
	{
		___BindingId_1 = value;
		Il2CppCodeGenWriteBarrier((&___BindingId_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Zenject.DiContainer/LookupId
struct LookupId_t3D88DB078E23694C787C1892C681AB1F34416808_marshaled_pinvoke
{
	RuntimeObject* ___Provider_0;
	BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA * ___BindingId_1;
};
// Native definition for COM marshalling of Zenject.DiContainer/LookupId
struct LookupId_t3D88DB078E23694C787C1892C681AB1F34416808_marshaled_com
{
	RuntimeObject* ___Provider_0;
	BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA * ___BindingId_1;
};
#endif // LOOKUPID_T3D88DB078E23694C787C1892C681AB1F34416808_H
#ifndef INSTALLER_TF99426A2815876B038EA481AC3EBD83EBDF3F93E_H
#define INSTALLER_TF99426A2815876B038EA481AC3EBD83EBDF3F93E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.Installer
struct  Installer_tF99426A2815876B038EA481AC3EBD83EBDF3F93E  : public InstallerBase_t4F464ACE8FEB8CEEE7B4352A271F09894C28AD79
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTALLER_TF99426A2815876B038EA481AC3EBD83EBDF3F93E_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef U3CU3EC__DISPLAYCLASS77_0_T4586DEB2F4F4851C00E8140E1C6FC745B0F58C5C_H
#define U3CU3EC__DISPLAYCLASS77_0_T4586DEB2F4F4851C00E8140E1C6FC745B0F58C5C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.DiContainer_<>c__DisplayClass77_0
struct  U3CU3Ec__DisplayClass77_0_t4586DEB2F4F4851C00E8140E1C6FC745B0F58C5C  : public RuntimeObject
{
public:
	// Zenject.DiContainer_LookupId Zenject.DiContainer_<>c__DisplayClass77_0::lookupId
	LookupId_t3D88DB078E23694C787C1892C681AB1F34416808  ___lookupId_0;

public:
	inline static int32_t get_offset_of_lookupId_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass77_0_t4586DEB2F4F4851C00E8140E1C6FC745B0F58C5C, ___lookupId_0)); }
	inline LookupId_t3D88DB078E23694C787C1892C681AB1F34416808  get_lookupId_0() const { return ___lookupId_0; }
	inline LookupId_t3D88DB078E23694C787C1892C681AB1F34416808 * get_address_of_lookupId_0() { return &___lookupId_0; }
	inline void set_lookupId_0(LookupId_t3D88DB078E23694C787C1892C681AB1F34416808  value)
	{
		___lookupId_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS77_0_T4586DEB2F4F4851C00E8140E1C6FC745B0F58C5C_H
#ifndef PROVIDERLOOKUPRESULT_T56C89C687DB92A4040F88136EA54AE530A5D9057_H
#define PROVIDERLOOKUPRESULT_T56C89C687DB92A4040F88136EA54AE530A5D9057_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.DiContainer_ProviderLookupResult
struct  ProviderLookupResult_t56C89C687DB92A4040F88136EA54AE530A5D9057 
{
public:
	// System.Int32 Zenject.DiContainer_ProviderLookupResult::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ProviderLookupResult_t56C89C687DB92A4040F88136EA54AE530A5D9057, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROVIDERLOOKUPRESULT_T56C89C687DB92A4040F88136EA54AE530A5D9057_H
#ifndef INJECTSOURCES_T643ECF8A27899CEE7D40760762395F6C3E359049_H
#define INJECTSOURCES_T643ECF8A27899CEE7D40760762395F6C3E359049_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.InjectSources
struct  InjectSources_t643ECF8A27899CEE7D40760762395F6C3E359049 
{
public:
	// System.Int32 Zenject.InjectSources::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InjectSources_t643ECF8A27899CEE7D40760762395F6C3E359049, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INJECTSOURCES_T643ECF8A27899CEE7D40760762395F6C3E359049_H
#ifndef BINDTYPES_TD67195A715D24E425ACDF1B8AF0F75EA1BE4186F_H
#define BINDTYPES_TD67195A715D24E425ACDF1B8AF0F75EA1BE4186F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ZenjectBinding_BindTypes
struct  BindTypes_tD67195A715D24E425ACDF1B8AF0F75EA1BE4186F 
{
public:
	// System.Int32 Zenject.ZenjectBinding_BindTypes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindTypes_tD67195A715D24E425ACDF1B8AF0F75EA1BE4186F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDTYPES_TD67195A715D24E425ACDF1B8AF0F75EA1BE4186F_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#define SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_pinvoke : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_com : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifndef U3CGETALLCONTAINERSTOLOOKUPU3ED__60_T3FFC62EEAB90A93F877D284F85591E25366C6A2D_H
#define U3CGETALLCONTAINERSTOLOOKUPU3ED__60_T3FFC62EEAB90A93F877D284F85591E25366C6A2D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.DiContainer_<GetAllContainersToLookup>d__60
struct  U3CGetAllContainersToLookupU3Ed__60_t3FFC62EEAB90A93F877D284F85591E25366C6A2D  : public RuntimeObject
{
public:
	// System.Int32 Zenject.DiContainer_<GetAllContainersToLookup>d__60::<>1__state
	int32_t ___U3CU3E1__state_0;
	// Zenject.DiContainer Zenject.DiContainer_<GetAllContainersToLookup>d__60::<>2__current
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ___U3CU3E2__current_1;
	// System.Int32 Zenject.DiContainer_<GetAllContainersToLookup>d__60::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// Zenject.InjectSources Zenject.DiContainer_<GetAllContainersToLookup>d__60::sourceType
	int32_t ___sourceType_3;
	// Zenject.InjectSources Zenject.DiContainer_<GetAllContainersToLookup>d__60::<>3__sourceType
	int32_t ___U3CU3E3__sourceType_4;
	// Zenject.DiContainer Zenject.DiContainer_<GetAllContainersToLookup>d__60::<>4__this
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ___U3CU3E4__this_5;
	// System.Collections.Generic.List`1_Enumerator<Zenject.DiContainer> Zenject.DiContainer_<GetAllContainersToLookup>d__60::<>7__wrap1
	Enumerator_t3D4AC3127853FAB2F510C6C9DE72C63A09E0BF91  ___U3CU3E7__wrap1_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetAllContainersToLookupU3Ed__60_t3FFC62EEAB90A93F877D284F85591E25366C6A2D, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetAllContainersToLookupU3Ed__60_t3FFC62EEAB90A93F877D284F85591E25366C6A2D, ___U3CU3E2__current_1)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CGetAllContainersToLookupU3Ed__60_t3FFC62EEAB90A93F877D284F85591E25366C6A2D, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_sourceType_3() { return static_cast<int32_t>(offsetof(U3CGetAllContainersToLookupU3Ed__60_t3FFC62EEAB90A93F877D284F85591E25366C6A2D, ___sourceType_3)); }
	inline int32_t get_sourceType_3() const { return ___sourceType_3; }
	inline int32_t* get_address_of_sourceType_3() { return &___sourceType_3; }
	inline void set_sourceType_3(int32_t value)
	{
		___sourceType_3 = value;
	}

	inline static int32_t get_offset_of_U3CU3E3__sourceType_4() { return static_cast<int32_t>(offsetof(U3CGetAllContainersToLookupU3Ed__60_t3FFC62EEAB90A93F877D284F85591E25366C6A2D, ___U3CU3E3__sourceType_4)); }
	inline int32_t get_U3CU3E3__sourceType_4() const { return ___U3CU3E3__sourceType_4; }
	inline int32_t* get_address_of_U3CU3E3__sourceType_4() { return &___U3CU3E3__sourceType_4; }
	inline void set_U3CU3E3__sourceType_4(int32_t value)
	{
		___U3CU3E3__sourceType_4 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_5() { return static_cast<int32_t>(offsetof(U3CGetAllContainersToLookupU3Ed__60_t3FFC62EEAB90A93F877D284F85591E25366C6A2D, ___U3CU3E4__this_5)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get_U3CU3E4__this_5() const { return ___U3CU3E4__this_5; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of_U3CU3E4__this_5() { return &___U3CU3E4__this_5; }
	inline void set_U3CU3E4__this_5(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		___U3CU3E4__this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_5), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_6() { return static_cast<int32_t>(offsetof(U3CGetAllContainersToLookupU3Ed__60_t3FFC62EEAB90A93F877D284F85591E25366C6A2D, ___U3CU3E7__wrap1_6)); }
	inline Enumerator_t3D4AC3127853FAB2F510C6C9DE72C63A09E0BF91  get_U3CU3E7__wrap1_6() const { return ___U3CU3E7__wrap1_6; }
	inline Enumerator_t3D4AC3127853FAB2F510C6C9DE72C63A09E0BF91 * get_address_of_U3CU3E7__wrap1_6() { return &___U3CU3E7__wrap1_6; }
	inline void set_U3CU3E7__wrap1_6(Enumerator_t3D4AC3127853FAB2F510C6C9DE72C63A09E0BF91  value)
	{
		___U3CU3E7__wrap1_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETALLCONTAINERSTOLOOKUPU3ED__60_T3FFC62EEAB90A93F877D284F85591E25366C6A2D_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef BINDINGCONDITION_T3BC89ABD74DA160C3396036CE0360DE414B4ADD8_H
#define BINDINGCONDITION_T3BC89ABD74DA160C3396036CE0360DE414B4ADD8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.BindingCondition
struct  BindingCondition_t3BC89ABD74DA160C3396036CE0360DE414B4ADD8  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGCONDITION_T3BC89ABD74DA160C3396036CE0360DE414B4ADD8_H
#ifndef SCRIPTABLEOBJECTINSTALLERBASE_T2B6C9A132878FE8B711E2E733FB10E227A48708A_H
#define SCRIPTABLEOBJECTINSTALLERBASE_T2B6C9A132878FE8B711E2E733FB10E227A48708A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ScriptableObjectInstallerBase
struct  ScriptableObjectInstallerBase_t2B6C9A132878FE8B711E2E733FB10E227A48708A  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// Zenject.DiContainer Zenject.ScriptableObjectInstallerBase::_container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____container_4;

public:
	inline static int32_t get_offset_of__container_4() { return static_cast<int32_t>(offsetof(ScriptableObjectInstallerBase_t2B6C9A132878FE8B711E2E733FB10E227A48708A, ____container_4)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__container_4() const { return ____container_4; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__container_4() { return &____container_4; }
	inline void set__container_4(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____container_4 = value;
		Il2CppCodeGenWriteBarrier((&____container_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRIPTABLEOBJECTINSTALLERBASE_T2B6C9A132878FE8B711E2E733FB10E227A48708A_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef SCRIPTABLEOBJECTINSTALLER_TAC791A8E2F09B0B1D8D61690E70CEDB55F1AD8D6_H
#define SCRIPTABLEOBJECTINSTALLER_TAC791A8E2F09B0B1D8D61690E70CEDB55F1AD8D6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ScriptableObjectInstaller
struct  ScriptableObjectInstaller_tAC791A8E2F09B0B1D8D61690E70CEDB55F1AD8D6  : public ScriptableObjectInstallerBase_t2B6C9A132878FE8B711E2E733FB10E227A48708A
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRIPTABLEOBJECTINSTALLER_TAC791A8E2F09B0B1D8D61690E70CEDB55F1AD8D6_H
#ifndef CONTEXT_T04459FF3A5B7E8AF25B965F5717F535AE8FA6DBA_H
#define CONTEXT_T04459FF3A5B7E8AF25B965F5717F535AE8FA6DBA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.Context
struct  Context_t04459FF3A5B7E8AF25B965F5717F535AE8FA6DBA  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Collections.Generic.List`1<Zenject.MonoInstaller> Zenject.Context::_installers
	List_1_tCF773BC8FB84889055B7C7EDDE5C5D83FA2D4F08 * ____installers_4;
	// System.Collections.Generic.List`1<Zenject.MonoInstaller> Zenject.Context::_installerPrefabs
	List_1_tCF773BC8FB84889055B7C7EDDE5C5D83FA2D4F08 * ____installerPrefabs_5;
	// System.Collections.Generic.List`1<Zenject.ScriptableObjectInstaller> Zenject.Context::_scriptableObjectInstallers
	List_1_t8B49C95109DF1CE321982E768E438471621D0733 * ____scriptableObjectInstallers_6;
	// System.Collections.Generic.List`1<Zenject.InstallerBase> Zenject.Context::_normalInstallers
	List_1_t0579372F26330F2A4603BEBDDF54A2BF6791CF8B * ____normalInstallers_7;
	// System.Collections.Generic.List`1<System.Type> Zenject.Context::_normalInstallerTypes
	List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0 * ____normalInstallerTypes_8;

public:
	inline static int32_t get_offset_of__installers_4() { return static_cast<int32_t>(offsetof(Context_t04459FF3A5B7E8AF25B965F5717F535AE8FA6DBA, ____installers_4)); }
	inline List_1_tCF773BC8FB84889055B7C7EDDE5C5D83FA2D4F08 * get__installers_4() const { return ____installers_4; }
	inline List_1_tCF773BC8FB84889055B7C7EDDE5C5D83FA2D4F08 ** get_address_of__installers_4() { return &____installers_4; }
	inline void set__installers_4(List_1_tCF773BC8FB84889055B7C7EDDE5C5D83FA2D4F08 * value)
	{
		____installers_4 = value;
		Il2CppCodeGenWriteBarrier((&____installers_4), value);
	}

	inline static int32_t get_offset_of__installerPrefabs_5() { return static_cast<int32_t>(offsetof(Context_t04459FF3A5B7E8AF25B965F5717F535AE8FA6DBA, ____installerPrefabs_5)); }
	inline List_1_tCF773BC8FB84889055B7C7EDDE5C5D83FA2D4F08 * get__installerPrefabs_5() const { return ____installerPrefabs_5; }
	inline List_1_tCF773BC8FB84889055B7C7EDDE5C5D83FA2D4F08 ** get_address_of__installerPrefabs_5() { return &____installerPrefabs_5; }
	inline void set__installerPrefabs_5(List_1_tCF773BC8FB84889055B7C7EDDE5C5D83FA2D4F08 * value)
	{
		____installerPrefabs_5 = value;
		Il2CppCodeGenWriteBarrier((&____installerPrefabs_5), value);
	}

	inline static int32_t get_offset_of__scriptableObjectInstallers_6() { return static_cast<int32_t>(offsetof(Context_t04459FF3A5B7E8AF25B965F5717F535AE8FA6DBA, ____scriptableObjectInstallers_6)); }
	inline List_1_t8B49C95109DF1CE321982E768E438471621D0733 * get__scriptableObjectInstallers_6() const { return ____scriptableObjectInstallers_6; }
	inline List_1_t8B49C95109DF1CE321982E768E438471621D0733 ** get_address_of__scriptableObjectInstallers_6() { return &____scriptableObjectInstallers_6; }
	inline void set__scriptableObjectInstallers_6(List_1_t8B49C95109DF1CE321982E768E438471621D0733 * value)
	{
		____scriptableObjectInstallers_6 = value;
		Il2CppCodeGenWriteBarrier((&____scriptableObjectInstallers_6), value);
	}

	inline static int32_t get_offset_of__normalInstallers_7() { return static_cast<int32_t>(offsetof(Context_t04459FF3A5B7E8AF25B965F5717F535AE8FA6DBA, ____normalInstallers_7)); }
	inline List_1_t0579372F26330F2A4603BEBDDF54A2BF6791CF8B * get__normalInstallers_7() const { return ____normalInstallers_7; }
	inline List_1_t0579372F26330F2A4603BEBDDF54A2BF6791CF8B ** get_address_of__normalInstallers_7() { return &____normalInstallers_7; }
	inline void set__normalInstallers_7(List_1_t0579372F26330F2A4603BEBDDF54A2BF6791CF8B * value)
	{
		____normalInstallers_7 = value;
		Il2CppCodeGenWriteBarrier((&____normalInstallers_7), value);
	}

	inline static int32_t get_offset_of__normalInstallerTypes_8() { return static_cast<int32_t>(offsetof(Context_t04459FF3A5B7E8AF25B965F5717F535AE8FA6DBA, ____normalInstallerTypes_8)); }
	inline List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0 * get__normalInstallerTypes_8() const { return ____normalInstallerTypes_8; }
	inline List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0 ** get_address_of__normalInstallerTypes_8() { return &____normalInstallerTypes_8; }
	inline void set__normalInstallerTypes_8(List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0 * value)
	{
		____normalInstallerTypes_8 = value;
		Il2CppCodeGenWriteBarrier((&____normalInstallerTypes_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTEXT_T04459FF3A5B7E8AF25B965F5717F535AE8FA6DBA_H
#ifndef MONOINSTALLERBASE_T91D926958396F3B2A0F3F126540C446F01105597_H
#define MONOINSTALLERBASE_T91D926958396F3B2A0F3F126540C446F01105597_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.MonoInstallerBase
struct  MonoInstallerBase_t91D926958396F3B2A0F3F126540C446F01105597  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Zenject.DiContainer Zenject.MonoInstallerBase::_container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____container_4;

public:
	inline static int32_t get_offset_of__container_4() { return static_cast<int32_t>(offsetof(MonoInstallerBase_t91D926958396F3B2A0F3F126540C446F01105597, ____container_4)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__container_4() const { return ____container_4; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__container_4() { return &____container_4; }
	inline void set__container_4(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____container_4 = value;
		Il2CppCodeGenWriteBarrier((&____container_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOINSTALLERBASE_T91D926958396F3B2A0F3F126540C446F01105597_H
#ifndef ZENJECTBINDING_T17AAB9EBAE67FB2179BAD31E5EE9022852986D20_H
#define ZENJECTBINDING_T17AAB9EBAE67FB2179BAD31E5EE9022852986D20_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ZenjectBinding
struct  ZenjectBinding_t17AAB9EBAE67FB2179BAD31E5EE9022852986D20  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Component[] Zenject.ZenjectBinding::_components
	ComponentU5BU5D_t7BE50AFB6301C06D990819B3D8F35CA326CDD155* ____components_4;
	// System.String Zenject.ZenjectBinding::_identifier
	String_t* ____identifier_5;
	// Zenject.Context Zenject.ZenjectBinding::_context
	Context_t04459FF3A5B7E8AF25B965F5717F535AE8FA6DBA * ____context_6;
	// Zenject.ZenjectBinding_BindTypes Zenject.ZenjectBinding::_bindType
	int32_t ____bindType_7;

public:
	inline static int32_t get_offset_of__components_4() { return static_cast<int32_t>(offsetof(ZenjectBinding_t17AAB9EBAE67FB2179BAD31E5EE9022852986D20, ____components_4)); }
	inline ComponentU5BU5D_t7BE50AFB6301C06D990819B3D8F35CA326CDD155* get__components_4() const { return ____components_4; }
	inline ComponentU5BU5D_t7BE50AFB6301C06D990819B3D8F35CA326CDD155** get_address_of__components_4() { return &____components_4; }
	inline void set__components_4(ComponentU5BU5D_t7BE50AFB6301C06D990819B3D8F35CA326CDD155* value)
	{
		____components_4 = value;
		Il2CppCodeGenWriteBarrier((&____components_4), value);
	}

	inline static int32_t get_offset_of__identifier_5() { return static_cast<int32_t>(offsetof(ZenjectBinding_t17AAB9EBAE67FB2179BAD31E5EE9022852986D20, ____identifier_5)); }
	inline String_t* get__identifier_5() const { return ____identifier_5; }
	inline String_t** get_address_of__identifier_5() { return &____identifier_5; }
	inline void set__identifier_5(String_t* value)
	{
		____identifier_5 = value;
		Il2CppCodeGenWriteBarrier((&____identifier_5), value);
	}

	inline static int32_t get_offset_of__context_6() { return static_cast<int32_t>(offsetof(ZenjectBinding_t17AAB9EBAE67FB2179BAD31E5EE9022852986D20, ____context_6)); }
	inline Context_t04459FF3A5B7E8AF25B965F5717F535AE8FA6DBA * get__context_6() const { return ____context_6; }
	inline Context_t04459FF3A5B7E8AF25B965F5717F535AE8FA6DBA ** get_address_of__context_6() { return &____context_6; }
	inline void set__context_6(Context_t04459FF3A5B7E8AF25B965F5717F535AE8FA6DBA * value)
	{
		____context_6 = value;
		Il2CppCodeGenWriteBarrier((&____context_6), value);
	}

	inline static int32_t get_offset_of__bindType_7() { return static_cast<int32_t>(offsetof(ZenjectBinding_t17AAB9EBAE67FB2179BAD31E5EE9022852986D20, ____bindType_7)); }
	inline int32_t get__bindType_7() const { return ____bindType_7; }
	inline int32_t* get_address_of__bindType_7() { return &____bindType_7; }
	inline void set__bindType_7(int32_t value)
	{
		____bindType_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZENJECTBINDING_T17AAB9EBAE67FB2179BAD31E5EE9022852986D20_H
#ifndef MONOINSTALLER_T9D57364413F95D7A946A3B7901B67365D562A343_H
#define MONOINSTALLER_T9D57364413F95D7A946A3B7901B67365D562A343_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.MonoInstaller
struct  MonoInstaller_t9D57364413F95D7A946A3B7901B67365D562A343  : public MonoInstallerBase_t91D926958396F3B2A0F3F126540C446F01105597
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOINSTALLER_T9D57364413F95D7A946A3B7901B67365D562A343_H
#ifndef SCENEDECORATORCONTEXT_T06D110A4DD08DDFACE5B4D34FAB24512713EFC14_H
#define SCENEDECORATORCONTEXT_T06D110A4DD08DDFACE5B4D34FAB24512713EFC14_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SceneDecoratorContext
struct  SceneDecoratorContext_t06D110A4DD08DDFACE5B4D34FAB24512713EFC14  : public Context_t04459FF3A5B7E8AF25B965F5717F535AE8FA6DBA
{
public:
	// System.Collections.Generic.List`1<Zenject.MonoInstaller> Zenject.SceneDecoratorContext::_lateInstallers
	List_1_tCF773BC8FB84889055B7C7EDDE5C5D83FA2D4F08 * ____lateInstallers_9;
	// System.Collections.Generic.List`1<Zenject.MonoInstaller> Zenject.SceneDecoratorContext::_lateInstallerPrefabs
	List_1_tCF773BC8FB84889055B7C7EDDE5C5D83FA2D4F08 * ____lateInstallerPrefabs_10;
	// System.Collections.Generic.List`1<Zenject.ScriptableObjectInstaller> Zenject.SceneDecoratorContext::_lateScriptableObjectInstallers
	List_1_t8B49C95109DF1CE321982E768E438471621D0733 * ____lateScriptableObjectInstallers_11;
	// System.String Zenject.SceneDecoratorContext::_decoratedContractName
	String_t* ____decoratedContractName_12;
	// Zenject.DiContainer Zenject.SceneDecoratorContext::_container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____container_13;
	// System.Collections.Generic.List`1<UnityEngine.MonoBehaviour> Zenject.SceneDecoratorContext::_injectableMonoBehaviours
	List_1_tA826461A2C9E99A4825BFEC4BE4DBF2F1A142DA7 * ____injectableMonoBehaviours_14;

public:
	inline static int32_t get_offset_of__lateInstallers_9() { return static_cast<int32_t>(offsetof(SceneDecoratorContext_t06D110A4DD08DDFACE5B4D34FAB24512713EFC14, ____lateInstallers_9)); }
	inline List_1_tCF773BC8FB84889055B7C7EDDE5C5D83FA2D4F08 * get__lateInstallers_9() const { return ____lateInstallers_9; }
	inline List_1_tCF773BC8FB84889055B7C7EDDE5C5D83FA2D4F08 ** get_address_of__lateInstallers_9() { return &____lateInstallers_9; }
	inline void set__lateInstallers_9(List_1_tCF773BC8FB84889055B7C7EDDE5C5D83FA2D4F08 * value)
	{
		____lateInstallers_9 = value;
		Il2CppCodeGenWriteBarrier((&____lateInstallers_9), value);
	}

	inline static int32_t get_offset_of__lateInstallerPrefabs_10() { return static_cast<int32_t>(offsetof(SceneDecoratorContext_t06D110A4DD08DDFACE5B4D34FAB24512713EFC14, ____lateInstallerPrefabs_10)); }
	inline List_1_tCF773BC8FB84889055B7C7EDDE5C5D83FA2D4F08 * get__lateInstallerPrefabs_10() const { return ____lateInstallerPrefabs_10; }
	inline List_1_tCF773BC8FB84889055B7C7EDDE5C5D83FA2D4F08 ** get_address_of__lateInstallerPrefabs_10() { return &____lateInstallerPrefabs_10; }
	inline void set__lateInstallerPrefabs_10(List_1_tCF773BC8FB84889055B7C7EDDE5C5D83FA2D4F08 * value)
	{
		____lateInstallerPrefabs_10 = value;
		Il2CppCodeGenWriteBarrier((&____lateInstallerPrefabs_10), value);
	}

	inline static int32_t get_offset_of__lateScriptableObjectInstallers_11() { return static_cast<int32_t>(offsetof(SceneDecoratorContext_t06D110A4DD08DDFACE5B4D34FAB24512713EFC14, ____lateScriptableObjectInstallers_11)); }
	inline List_1_t8B49C95109DF1CE321982E768E438471621D0733 * get__lateScriptableObjectInstallers_11() const { return ____lateScriptableObjectInstallers_11; }
	inline List_1_t8B49C95109DF1CE321982E768E438471621D0733 ** get_address_of__lateScriptableObjectInstallers_11() { return &____lateScriptableObjectInstallers_11; }
	inline void set__lateScriptableObjectInstallers_11(List_1_t8B49C95109DF1CE321982E768E438471621D0733 * value)
	{
		____lateScriptableObjectInstallers_11 = value;
		Il2CppCodeGenWriteBarrier((&____lateScriptableObjectInstallers_11), value);
	}

	inline static int32_t get_offset_of__decoratedContractName_12() { return static_cast<int32_t>(offsetof(SceneDecoratorContext_t06D110A4DD08DDFACE5B4D34FAB24512713EFC14, ____decoratedContractName_12)); }
	inline String_t* get__decoratedContractName_12() const { return ____decoratedContractName_12; }
	inline String_t** get_address_of__decoratedContractName_12() { return &____decoratedContractName_12; }
	inline void set__decoratedContractName_12(String_t* value)
	{
		____decoratedContractName_12 = value;
		Il2CppCodeGenWriteBarrier((&____decoratedContractName_12), value);
	}

	inline static int32_t get_offset_of__container_13() { return static_cast<int32_t>(offsetof(SceneDecoratorContext_t06D110A4DD08DDFACE5B4D34FAB24512713EFC14, ____container_13)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__container_13() const { return ____container_13; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__container_13() { return &____container_13; }
	inline void set__container_13(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____container_13 = value;
		Il2CppCodeGenWriteBarrier((&____container_13), value);
	}

	inline static int32_t get_offset_of__injectableMonoBehaviours_14() { return static_cast<int32_t>(offsetof(SceneDecoratorContext_t06D110A4DD08DDFACE5B4D34FAB24512713EFC14, ____injectableMonoBehaviours_14)); }
	inline List_1_tA826461A2C9E99A4825BFEC4BE4DBF2F1A142DA7 * get__injectableMonoBehaviours_14() const { return ____injectableMonoBehaviours_14; }
	inline List_1_tA826461A2C9E99A4825BFEC4BE4DBF2F1A142DA7 ** get_address_of__injectableMonoBehaviours_14() { return &____injectableMonoBehaviours_14; }
	inline void set__injectableMonoBehaviours_14(List_1_tA826461A2C9E99A4825BFEC4BE4DBF2F1A142DA7 * value)
	{
		____injectableMonoBehaviours_14 = value;
		Il2CppCodeGenWriteBarrier((&____injectableMonoBehaviours_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENEDECORATORCONTEXT_T06D110A4DD08DDFACE5B4D34FAB24512713EFC14_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7300 = { sizeof (U3CU3Ec__DisplayClass28_0_tFF8D082EF9DA6AC69721ED1899C89CAA9DB8CCBC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7300[2] = 
{
	U3CU3Ec__DisplayClass28_0_tFF8D082EF9DA6AC69721ED1899C89CAA9DB8CCBC::get_offset_of_parentContractNames_0(),
	U3CU3Ec__DisplayClass28_0_tFF8D082EF9DA6AC69721ED1899C89CAA9DB8CCBC::get_offset_of_U3CU3E9__4_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7301 = { sizeof (U3CU3Ec_t7EB23F8FFAF0F64E9848F096FF193DADE635199B), -1, sizeof(U3CU3Ec_t7EB23F8FFAF0F64E9848F096FF193DADE635199B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7301[6] = 
{
	U3CU3Ec_t7EB23F8FFAF0F64E9848F096FF193DADE635199B_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t7EB23F8FFAF0F64E9848F096FF193DADE635199B_StaticFields::get_offset_of_U3CU3E9__28_0_1(),
	U3CU3Ec_t7EB23F8FFAF0F64E9848F096FF193DADE635199B_StaticFields::get_offset_of_U3CU3E9__28_1_2(),
	U3CU3Ec_t7EB23F8FFAF0F64E9848F096FF193DADE635199B_StaticFields::get_offset_of_U3CU3E9__28_3_3(),
	U3CU3Ec_t7EB23F8FFAF0F64E9848F096FF193DADE635199B_StaticFields::get_offset_of_U3CU3E9__29_0_4(),
	U3CU3Ec_t7EB23F8FFAF0F64E9848F096FF193DADE635199B_StaticFields::get_offset_of_U3CU3E9__29_1_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7302 = { sizeof (U3CU3Ec__DisplayClass30_0_t05114B7B3F36618C82FE6D9E90DEE11F4BBA658A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7302[1] = 
{
	U3CU3Ec__DisplayClass30_0_t05114B7B3F36618C82FE6D9E90DEE11F4BBA658A::get_offset_of_parents_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7303 = { sizeof (SceneDecoratorContext_t06D110A4DD08DDFACE5B4D34FAB24512713EFC14), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7303[6] = 
{
	SceneDecoratorContext_t06D110A4DD08DDFACE5B4D34FAB24512713EFC14::get_offset_of__lateInstallers_9(),
	SceneDecoratorContext_t06D110A4DD08DDFACE5B4D34FAB24512713EFC14::get_offset_of__lateInstallerPrefabs_10(),
	SceneDecoratorContext_t06D110A4DD08DDFACE5B4D34FAB24512713EFC14::get_offset_of__lateScriptableObjectInstallers_11(),
	SceneDecoratorContext_t06D110A4DD08DDFACE5B4D34FAB24512713EFC14::get_offset_of__decoratedContractName_12(),
	SceneDecoratorContext_t06D110A4DD08DDFACE5B4D34FAB24512713EFC14::get_offset_of__container_13(),
	SceneDecoratorContext_t06D110A4DD08DDFACE5B4D34FAB24512713EFC14::get_offset_of__injectableMonoBehaviours_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7304 = { sizeof (StaticContext_tAC293A700E899C8140D66B0A4C0FB5883EE5069F), -1, sizeof(StaticContext_tAC293A700E899C8140D66B0A4C0FB5883EE5069F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7304[1] = 
{
	StaticContext_tAC293A700E899C8140D66B0A4C0FB5883EE5069F_StaticFields::get_offset_of__container_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7305 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7306 = { sizeof (Installer_tF99426A2815876B038EA481AC3EBD83EBDF3F93E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7307 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7308 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7309 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7310 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7311 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7312 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7313 = { sizeof (InstallerBase_t4F464ACE8FEB8CEEE7B4352A271F09894C28AD79), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7313[1] = 
{
	InstallerBase_t4F464ACE8FEB8CEEE7B4352A271F09894C28AD79::get_offset_of__container_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7314 = { sizeof (MonoInstaller_t9D57364413F95D7A946A3B7901B67365D562A343), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7315 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7316 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7317 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7318 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7319 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7320 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7321 = { sizeof (MonoInstallerUtil_t27880052EDA631B23E6D4C4BE34A2C06F4B1AA50), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7322 = { sizeof (MonoInstallerBase_t91D926958396F3B2A0F3F126540C446F01105597), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7322[1] = 
{
	MonoInstallerBase_t91D926958396F3B2A0F3F126540C446F01105597::get_offset_of__container_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7323 = { sizeof (ScriptableObjectInstaller_tAC791A8E2F09B0B1D8D61690E70CEDB55F1AD8D6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7324 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7325 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7326 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7327 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7328 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7329 = { sizeof (ScriptableObjectInstallerUtil_tF39DAB5971870E69DFFF738578E1FF1351AD4818), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7330 = { sizeof (ScriptableObjectInstallerBase_t2B6C9A132878FE8B711E2E733FB10E227A48708A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7330[1] = 
{
	ScriptableObjectInstallerBase_t2B6C9A132878FE8B711E2E733FB10E227A48708A::get_offset_of__container_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7331 = { sizeof (ZenjectBinding_t17AAB9EBAE67FB2179BAD31E5EE9022852986D20), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7331[4] = 
{
	ZenjectBinding_t17AAB9EBAE67FB2179BAD31E5EE9022852986D20::get_offset_of__components_4(),
	ZenjectBinding_t17AAB9EBAE67FB2179BAD31E5EE9022852986D20::get_offset_of__identifier_5(),
	ZenjectBinding_t17AAB9EBAE67FB2179BAD31E5EE9022852986D20::get_offset_of__context_6(),
	ZenjectBinding_t17AAB9EBAE67FB2179BAD31E5EE9022852986D20::get_offset_of__bindType_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7332 = { sizeof (BindTypes_tD67195A715D24E425ACDF1B8AF0F75EA1BE4186F)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7332[5] = 
{
	BindTypes_tD67195A715D24E425ACDF1B8AF0F75EA1BE4186F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7333 = { sizeof (BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7333[2] = 
{
	BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA::get_offset_of_Type_0(),
	BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA::get_offset_of_Identifier_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7334 = { sizeof (BindingCondition_t3BC89ABD74DA160C3396036CE0360DE414B4ADD8), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7335 = { sizeof (InjectArgs_tD1EC7FDE37E60A7D38EF224E5FE6C8667639387C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7335[3] = 
{
	InjectArgs_tD1EC7FDE37E60A7D38EF224E5FE6C8667639387C::get_offset_of_ExtraArgs_0(),
	InjectArgs_tD1EC7FDE37E60A7D38EF224E5FE6C8667639387C::get_offset_of_Context_1(),
	InjectArgs_tD1EC7FDE37E60A7D38EF224E5FE6C8667639387C::get_offset_of_ConcreteIdentifier_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7336 = { sizeof (DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7336[18] = 
{
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5::get_offset_of__providers_0(),
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5::get_offset_of__parentContainers_1(),
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5::get_offset_of__ancestorContainers_2(),
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5::get_offset_of__resolvesInProgress_3(),
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5::get_offset_of__singletonProviderCreator_4(),
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5::get_offset_of__singletonMarkRegistry_5(),
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5::get_offset_of__lazyInjector_6(),
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5::get_offset_of__currentBindings_7(),
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5::get_offset_of__childBindings_8(),
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5::get_offset_of__lateBindingsToValidate_9(),
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5::get_offset_of__context_10(),
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5::get_offset_of__isFinalizingBinding_11(),
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5::get_offset_of__isValidating_12(),
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5::get_offset_of__isInstalling_13(),
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5::get_offset_of__hasDisplayedInstallWarning_14(),
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5::get_offset_of_U3CShouldCheckForInstallWarningU3Ek__BackingField_15(),
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5::get_offset_of_U3CAssertOnNewGameObjectsU3Ek__BackingField_16(),
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5::get_offset_of_U3CDefaultParentU3Ek__BackingField_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7337 = { sizeof (ProviderPair_t94BF5C0920EE77086B5FFF74A411E29EAB78A504), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7337[2] = 
{
	ProviderPair_t94BF5C0920EE77086B5FFF74A411E29EAB78A504::get_offset_of_U3CProviderInfoU3Ek__BackingField_0(),
	ProviderPair_t94BF5C0920EE77086B5FFF74A411E29EAB78A504::get_offset_of_U3CContainerU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7338 = { sizeof (ProviderLookupResult_t56C89C687DB92A4040F88136EA54AE530A5D9057)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7338[4] = 
{
	ProviderLookupResult_t56C89C687DB92A4040F88136EA54AE530A5D9057::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7339 = { sizeof (LookupId_t3D88DB078E23694C787C1892C681AB1F34416808)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7339[2] = 
{
	LookupId_t3D88DB078E23694C787C1892C681AB1F34416808::get_offset_of_Provider_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LookupId_t3D88DB078E23694C787C1892C681AB1F34416808::get_offset_of_BindingId_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7340 = { sizeof (ProviderInfo_t038F6812DFCFCD306479198F3E14CCB57B026C86), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7340[3] = 
{
	ProviderInfo_t038F6812DFCFCD306479198F3E14CCB57B026C86::get_offset_of_U3CNonLazyU3Ek__BackingField_0(),
	ProviderInfo_t038F6812DFCFCD306479198F3E14CCB57B026C86::get_offset_of_U3CProviderU3Ek__BackingField_1(),
	ProviderInfo_t038F6812DFCFCD306479198F3E14CCB57B026C86::get_offset_of_U3CConditionU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7341 = { sizeof (U3CU3Ec_t796C2A8263700A6383229AE9711EDF5DD258D101), -1, sizeof(U3CU3Ec_t796C2A8263700A6383229AE9711EDF5DD258D101_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7341[11] = 
{
	U3CU3Ec_t796C2A8263700A6383229AE9711EDF5DD258D101_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t796C2A8263700A6383229AE9711EDF5DD258D101_StaticFields::get_offset_of_U3CU3E9__19_0_1(),
	U3CU3Ec_t796C2A8263700A6383229AE9711EDF5DD258D101_StaticFields::get_offset_of_U3CU3E9__58_0_2(),
	U3CU3Ec_t796C2A8263700A6383229AE9711EDF5DD258D101_StaticFields::get_offset_of_U3CU3E9__74_1_3(),
	U3CU3Ec_t796C2A8263700A6383229AE9711EDF5DD258D101_StaticFields::get_offset_of_U3CU3E9__75_1_4(),
	U3CU3Ec_t796C2A8263700A6383229AE9711EDF5DD258D101_StaticFields::get_offset_of_U3CU3E9__75_3_5(),
	U3CU3Ec_t796C2A8263700A6383229AE9711EDF5DD258D101_StaticFields::get_offset_of_U3CU3E9__75_4_6(),
	U3CU3Ec_t796C2A8263700A6383229AE9711EDF5DD258D101_StaticFields::get_offset_of_U3CU3E9__83_0_7(),
	U3CU3Ec_t796C2A8263700A6383229AE9711EDF5DD258D101_StaticFields::get_offset_of_U3CU3E9__86_0_8(),
	U3CU3Ec_t796C2A8263700A6383229AE9711EDF5DD258D101_StaticFields::get_offset_of_U3CU3E9__175_0_9(),
	U3CU3Ec_t796C2A8263700A6383229AE9711EDF5DD258D101_StaticFields::get_offset_of_U3CU3E9__176_0_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7342 = { sizeof (U3CU3Ec__DisplayClass51_0_tCCB988B4E31257DD9D490195B8525F4B9B18B347), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7342[1] = 
{
	U3CU3Ec__DisplayClass51_0_tCCB988B4E31257DD9D490195B8525F4B9B18B347::get_offset_of_injectContext_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7343 = { sizeof (U3CU3Ec__DisplayClass59_0_t2EA11530640333862D7CBC76D824CAE0F1E864A8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7343[1] = 
{
	U3CU3Ec__DisplayClass59_0_t2EA11530640333862D7CBC76D824CAE0F1E864A8::get_offset_of_context_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7344 = { sizeof (U3CGetAllContainersToLookupU3Ed__60_t3FFC62EEAB90A93F877D284F85591E25366C6A2D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7344[7] = 
{
	U3CGetAllContainersToLookupU3Ed__60_t3FFC62EEAB90A93F877D284F85591E25366C6A2D::get_offset_of_U3CU3E1__state_0(),
	U3CGetAllContainersToLookupU3Ed__60_t3FFC62EEAB90A93F877D284F85591E25366C6A2D::get_offset_of_U3CU3E2__current_1(),
	U3CGetAllContainersToLookupU3Ed__60_t3FFC62EEAB90A93F877D284F85591E25366C6A2D::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CGetAllContainersToLookupU3Ed__60_t3FFC62EEAB90A93F877D284F85591E25366C6A2D::get_offset_of_sourceType_3(),
	U3CGetAllContainersToLookupU3Ed__60_t3FFC62EEAB90A93F877D284F85591E25366C6A2D::get_offset_of_U3CU3E3__sourceType_4(),
	U3CGetAllContainersToLookupU3Ed__60_t3FFC62EEAB90A93F877D284F85591E25366C6A2D::get_offset_of_U3CU3E4__this_5(),
	U3CGetAllContainersToLookupU3Ed__60_t3FFC62EEAB90A93F877D284F85591E25366C6A2D::get_offset_of_U3CU3E7__wrap1_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7345 = { sizeof (U3CU3Ec__DisplayClass63_0_t3AC546137B60AD36628C4C7651EC0BC243310E2B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7345[1] = 
{
	U3CU3Ec__DisplayClass63_0_t3AC546137B60AD36628C4C7651EC0BC243310E2B::get_offset_of_bindingId_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7346 = { sizeof (U3CU3Ec__DisplayClass67_0_t03EBDB1872CBC7078F09C9831BF45860C8F02AFA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7346[2] = 
{
	U3CU3Ec__DisplayClass67_0_t03EBDB1872CBC7078F09C9831BF45860C8F02AFA::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass67_0_t03EBDB1872CBC7078F09C9831BF45860C8F02AFA::get_offset_of_context_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7347 = { sizeof (U3CU3Ec__DisplayClass74_0_tA09B55C26E67F636E9D97B0D9259D9604E391487), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7347[1] = 
{
	U3CU3Ec__DisplayClass74_0_tA09B55C26E67F636E9D97B0D9259D9604E391487::get_offset_of_context_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7348 = { sizeof (U3CU3Ec__DisplayClass75_0_t831AD3800DCE5B0F2F54BF79204F0A5C9CCDA831), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7348[1] = 
{
	U3CU3Ec__DisplayClass75_0_t831AD3800DCE5B0F2F54BF79204F0A5C9CCDA831::get_offset_of_sortedProviders_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7349 = { sizeof (U3CU3Ec__DisplayClass77_0_t4586DEB2F4F4851C00E8140E1C6FC745B0F58C5C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7349[1] = 
{
	U3CU3Ec__DisplayClass77_0_t4586DEB2F4F4851C00E8140E1C6FC745B0F58C5C::get_offset_of_lookupId_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7350 = { sizeof (U3CGetDependencyContractsU3Ed__81_tD502D02FDEE43AF8701F3F53937B5D497A656B24), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7350[7] = 
{
	U3CGetDependencyContractsU3Ed__81_tD502D02FDEE43AF8701F3F53937B5D497A656B24::get_offset_of_U3CU3E1__state_0(),
	U3CGetDependencyContractsU3Ed__81_tD502D02FDEE43AF8701F3F53937B5D497A656B24::get_offset_of_U3CU3E2__current_1(),
	U3CGetDependencyContractsU3Ed__81_tD502D02FDEE43AF8701F3F53937B5D497A656B24::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CGetDependencyContractsU3Ed__81_tD502D02FDEE43AF8701F3F53937B5D497A656B24::get_offset_of_U3CU3E4__this_3(),
	U3CGetDependencyContractsU3Ed__81_tD502D02FDEE43AF8701F3F53937B5D497A656B24::get_offset_of_contract_4(),
	U3CGetDependencyContractsU3Ed__81_tD502D02FDEE43AF8701F3F53937B5D497A656B24::get_offset_of_U3CU3E3__contract_5(),
	U3CGetDependencyContractsU3Ed__81_tD502D02FDEE43AF8701F3F53937B5D497A656B24::get_offset_of_U3CU3E7__wrap1_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7351 = { sizeof (U3CU3Ec__DisplayClass158_0_t7419DA0257C3E5FDBCED2089793EA859DA008041), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7351[4] = 
{
	U3CU3Ec__DisplayClass158_0_t7419DA0257C3E5FDBCED2089793EA859DA008041::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass158_0_t7419DA0257C3E5FDBCED2089793EA859DA008041::get_offset_of_contractType_1(),
	U3CU3Ec__DisplayClass158_0_t7419DA0257C3E5FDBCED2089793EA859DA008041::get_offset_of_identifier_2(),
	U3CU3Ec__DisplayClass158_0_t7419DA0257C3E5FDBCED2089793EA859DA008041::get_offset_of_concreteType_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7352 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7352[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7353 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7354 = { sizeof (LazyInstanceInjector_t0120534A9142C2F3AC0A2A87E5CAC778A6F831A6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7354[2] = 
{
	LazyInstanceInjector_t0120534A9142C2F3AC0A2A87E5CAC778A6F831A6::get_offset_of__container_0(),
	LazyInstanceInjector_t0120534A9142C2F3AC0A2A87E5CAC778A6F831A6::get_offset_of__instancesToInject_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7355 = { sizeof (CachedProvider_t0DD0B25EA381638476E1E4EC2FC32C8CDA59BA36), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7355[3] = 
{
	CachedProvider_t0DD0B25EA381638476E1E4EC2FC32C8CDA59BA36::get_offset_of__creator_0(),
	CachedProvider_t0DD0B25EA381638476E1E4EC2FC32C8CDA59BA36::get_offset_of__instances_1(),
	CachedProvider_t0DD0B25EA381638476E1E4EC2FC32C8CDA59BA36::get_offset_of__isCreatingInstance_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7356 = { sizeof (U3CGetAllInstancesWithInjectSplitU3Ed__5_t36659B04E0790E6DFBBF55651E001AE8AEF04EBD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7356[7] = 
{
	U3CGetAllInstancesWithInjectSplitU3Ed__5_t36659B04E0790E6DFBBF55651E001AE8AEF04EBD::get_offset_of_U3CU3E1__state_0(),
	U3CGetAllInstancesWithInjectSplitU3Ed__5_t36659B04E0790E6DFBBF55651E001AE8AEF04EBD::get_offset_of_U3CU3E2__current_1(),
	U3CGetAllInstancesWithInjectSplitU3Ed__5_t36659B04E0790E6DFBBF55651E001AE8AEF04EBD::get_offset_of_context_2(),
	U3CGetAllInstancesWithInjectSplitU3Ed__5_t36659B04E0790E6DFBBF55651E001AE8AEF04EBD::get_offset_of_U3CU3E4__this_3(),
	U3CGetAllInstancesWithInjectSplitU3Ed__5_t36659B04E0790E6DFBBF55651E001AE8AEF04EBD::get_offset_of_args_4(),
	U3CGetAllInstancesWithInjectSplitU3Ed__5_t36659B04E0790E6DFBBF55651E001AE8AEF04EBD::get_offset_of_U3CrunnerU3E5__2_5(),
	U3CGetAllInstancesWithInjectSplitU3Ed__5_t36659B04E0790E6DFBBF55651E001AE8AEF04EBD::get_offset_of_U3ChasMoreU3E5__3_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7357 = { sizeof (AddToCurrentGameObjectComponentProvider_t9CCD8774C1BA2207D55CE64479BD916A57DF069E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7357[4] = 
{
	AddToCurrentGameObjectComponentProvider_t9CCD8774C1BA2207D55CE64479BD916A57DF069E::get_offset_of__concreteIdentifier_0(),
	AddToCurrentGameObjectComponentProvider_t9CCD8774C1BA2207D55CE64479BD916A57DF069E::get_offset_of__componentType_1(),
	AddToCurrentGameObjectComponentProvider_t9CCD8774C1BA2207D55CE64479BD916A57DF069E::get_offset_of__container_2(),
	AddToCurrentGameObjectComponentProvider_t9CCD8774C1BA2207D55CE64479BD916A57DF069E::get_offset_of__extraArguments_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7358 = { sizeof (U3CGetAllInstancesWithInjectSplitU3Ed__12_t407DF40E51448E8E78815FC3FA70E960A41941FC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7358[6] = 
{
	U3CGetAllInstancesWithInjectSplitU3Ed__12_t407DF40E51448E8E78815FC3FA70E960A41941FC::get_offset_of_U3CU3E1__state_0(),
	U3CGetAllInstancesWithInjectSplitU3Ed__12_t407DF40E51448E8E78815FC3FA70E960A41941FC::get_offset_of_U3CU3E2__current_1(),
	U3CGetAllInstancesWithInjectSplitU3Ed__12_t407DF40E51448E8E78815FC3FA70E960A41941FC::get_offset_of_context_2(),
	U3CGetAllInstancesWithInjectSplitU3Ed__12_t407DF40E51448E8E78815FC3FA70E960A41941FC::get_offset_of_U3CU3E4__this_3(),
	U3CGetAllInstancesWithInjectSplitU3Ed__12_t407DF40E51448E8E78815FC3FA70E960A41941FC::get_offset_of_args_4(),
	U3CGetAllInstancesWithInjectSplitU3Ed__12_t407DF40E51448E8E78815FC3FA70E960A41941FC::get_offset_of_U3CinstanceU3E5__2_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7359 = { sizeof (AddToExistingGameObjectComponentProvider_t9B0432997ED1107ED7D91EF5252E6929A485AAAA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7359[1] = 
{
	AddToExistingGameObjectComponentProvider_t9B0432997ED1107ED7D91EF5252E6929A485AAAA::get_offset_of__gameObject_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7360 = { sizeof (AddToExistingGameObjectComponentProviderGetter_t90117CDD7BC87E54FA50EAE29E6EAFE3DEBF61D2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7360[1] = 
{
	AddToExistingGameObjectComponentProviderGetter_t90117CDD7BC87E54FA50EAE29E6EAFE3DEBF61D2::get_offset_of__gameObjectGetter_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7361 = { sizeof (AddToGameObjectComponentProviderBase_tACB741545D819318E619C706D60A87C16EAC5434), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7361[4] = 
{
	AddToGameObjectComponentProviderBase_tACB741545D819318E619C706D60A87C16EAC5434::get_offset_of__concreteIdentifier_0(),
	AddToGameObjectComponentProviderBase_tACB741545D819318E619C706D60A87C16EAC5434::get_offset_of__componentType_1(),
	AddToGameObjectComponentProviderBase_tACB741545D819318E619C706D60A87C16EAC5434::get_offset_of__container_2(),
	AddToGameObjectComponentProviderBase_tACB741545D819318E619C706D60A87C16EAC5434::get_offset_of__extraArguments_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7362 = { sizeof (U3CGetAllInstancesWithInjectSplitU3Ed__14_tEF5DBD5A6E175503235B0119D5E5360A5020BF8A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7362[8] = 
{
	U3CGetAllInstancesWithInjectSplitU3Ed__14_tEF5DBD5A6E175503235B0119D5E5360A5020BF8A::get_offset_of_U3CU3E1__state_0(),
	U3CGetAllInstancesWithInjectSplitU3Ed__14_tEF5DBD5A6E175503235B0119D5E5360A5020BF8A::get_offset_of_U3CU3E2__current_1(),
	U3CGetAllInstancesWithInjectSplitU3Ed__14_tEF5DBD5A6E175503235B0119D5E5360A5020BF8A::get_offset_of_context_2(),
	U3CGetAllInstancesWithInjectSplitU3Ed__14_tEF5DBD5A6E175503235B0119D5E5360A5020BF8A::get_offset_of_U3CU3E4__this_3(),
	U3CGetAllInstancesWithInjectSplitU3Ed__14_tEF5DBD5A6E175503235B0119D5E5360A5020BF8A::get_offset_of_args_4(),
	U3CGetAllInstancesWithInjectSplitU3Ed__14_tEF5DBD5A6E175503235B0119D5E5360A5020BF8A::get_offset_of_U3CinstanceU3E5__2_5(),
	U3CGetAllInstancesWithInjectSplitU3Ed__14_tEF5DBD5A6E175503235B0119D5E5360A5020BF8A::get_offset_of_U3CgameObjU3E5__3_6(),
	U3CGetAllInstancesWithInjectSplitU3Ed__14_tEF5DBD5A6E175503235B0119D5E5360A5020BF8A::get_offset_of_U3CwasActiveU3E5__4_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7363 = { sizeof (AddToNewGameObjectComponentProvider_t56FB68E5B31DF7DA3848423315F5544109E54063), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7363[1] = 
{
	AddToNewGameObjectComponentProvider_t56FB68E5B31DF7DA3848423315F5544109E54063::get_offset_of__gameObjectBindInfo_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7364 = { sizeof (GetFromPrefabComponentProvider_t42F7CECCDF7930D6C1C6F1397452ABE68BADBDF6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7364[2] = 
{
	GetFromPrefabComponentProvider_t42F7CECCDF7930D6C1C6F1397452ABE68BADBDF6::get_offset_of__prefabInstantiator_0(),
	GetFromPrefabComponentProvider_t42F7CECCDF7930D6C1C6F1397452ABE68BADBDF6::get_offset_of__componentType_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7365 = { sizeof (U3CGetAllInstancesWithInjectSplitU3Ed__4_tDAB2793AFCFBAD3222C1423FF34E9B27280F33BE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7365[7] = 
{
	U3CGetAllInstancesWithInjectSplitU3Ed__4_tDAB2793AFCFBAD3222C1423FF34E9B27280F33BE::get_offset_of_U3CU3E1__state_0(),
	U3CGetAllInstancesWithInjectSplitU3Ed__4_tDAB2793AFCFBAD3222C1423FF34E9B27280F33BE::get_offset_of_U3CU3E2__current_1(),
	U3CGetAllInstancesWithInjectSplitU3Ed__4_tDAB2793AFCFBAD3222C1423FF34E9B27280F33BE::get_offset_of_context_2(),
	U3CGetAllInstancesWithInjectSplitU3Ed__4_tDAB2793AFCFBAD3222C1423FF34E9B27280F33BE::get_offset_of_U3CU3E4__this_3(),
	U3CGetAllInstancesWithInjectSplitU3Ed__4_tDAB2793AFCFBAD3222C1423FF34E9B27280F33BE::get_offset_of_args_4(),
	U3CGetAllInstancesWithInjectSplitU3Ed__4_tDAB2793AFCFBAD3222C1423FF34E9B27280F33BE::get_offset_of_U3CgameObjectRunnerU3E5__2_5(),
	U3CGetAllInstancesWithInjectSplitU3Ed__4_tDAB2793AFCFBAD3222C1423FF34E9B27280F33BE::get_offset_of_U3ChasMoreU3E5__3_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7366 = { sizeof (InstantiateOnPrefabComponentProvider_t1445E68D6F2B17287C0FDE478F5E751A3FBBDF78), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7366[2] = 
{
	InstantiateOnPrefabComponentProvider_t1445E68D6F2B17287C0FDE478F5E751A3FBBDF78::get_offset_of__prefabInstantiator_0(),
	InstantiateOnPrefabComponentProvider_t1445E68D6F2B17287C0FDE478F5E751A3FBBDF78::get_offset_of__componentType_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7367 = { sizeof (U3CGetAllInstancesWithInjectSplitU3Ed__4_t263A67D351300C40A7B53B4E2C68D77CE96DB9BC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7367[7] = 
{
	U3CGetAllInstancesWithInjectSplitU3Ed__4_t263A67D351300C40A7B53B4E2C68D77CE96DB9BC::get_offset_of_U3CU3E1__state_0(),
	U3CGetAllInstancesWithInjectSplitU3Ed__4_t263A67D351300C40A7B53B4E2C68D77CE96DB9BC::get_offset_of_U3CU3E2__current_1(),
	U3CGetAllInstancesWithInjectSplitU3Ed__4_t263A67D351300C40A7B53B4E2C68D77CE96DB9BC::get_offset_of_context_2(),
	U3CGetAllInstancesWithInjectSplitU3Ed__4_t263A67D351300C40A7B53B4E2C68D77CE96DB9BC::get_offset_of_U3CU3E4__this_3(),
	U3CGetAllInstancesWithInjectSplitU3Ed__4_t263A67D351300C40A7B53B4E2C68D77CE96DB9BC::get_offset_of_args_4(),
	U3CGetAllInstancesWithInjectSplitU3Ed__4_t263A67D351300C40A7B53B4E2C68D77CE96DB9BC::get_offset_of_U3CgameObjectRunnerU3E5__2_5(),
	U3CGetAllInstancesWithInjectSplitU3Ed__4_t263A67D351300C40A7B53B4E2C68D77CE96DB9BC::get_offset_of_U3ChasMoreU3E5__3_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7368 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7368[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7369 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7370 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7370[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7371 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7372 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7372[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7373 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7374 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7374[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7375 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7376 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7376[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7377 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7378 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7378[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7379 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7380 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7380[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7381 = { sizeof (EmptyGameObjectProvider_t99FDC7D0A141A3CB7F442969996294D2F3B749FC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7381[2] = 
{
	EmptyGameObjectProvider_t99FDC7D0A141A3CB7F442969996294D2F3B749FC::get_offset_of__container_0(),
	EmptyGameObjectProvider_t99FDC7D0A141A3CB7F442969996294D2F3B749FC::get_offset_of__gameObjectBindInfo_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7382 = { sizeof (U3CGetAllInstancesWithInjectSplitU3Ed__4_t4FAC9337E075E17CA14A0882BA6A03AAA5470C6C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7382[5] = 
{
	U3CGetAllInstancesWithInjectSplitU3Ed__4_t4FAC9337E075E17CA14A0882BA6A03AAA5470C6C::get_offset_of_U3CU3E1__state_0(),
	U3CGetAllInstancesWithInjectSplitU3Ed__4_t4FAC9337E075E17CA14A0882BA6A03AAA5470C6C::get_offset_of_U3CU3E2__current_1(),
	U3CGetAllInstancesWithInjectSplitU3Ed__4_t4FAC9337E075E17CA14A0882BA6A03AAA5470C6C::get_offset_of_args_2(),
	U3CGetAllInstancesWithInjectSplitU3Ed__4_t4FAC9337E075E17CA14A0882BA6A03AAA5470C6C::get_offset_of_U3CU3E4__this_3(),
	U3CGetAllInstancesWithInjectSplitU3Ed__4_t4FAC9337E075E17CA14A0882BA6A03AAA5470C6C::get_offset_of_context_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7383 = { sizeof (PrefabGameObjectProvider_t194304A9DD65D5EF02263FE908B286D92CE4D8C3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7383[1] = 
{
	PrefabGameObjectProvider_t194304A9DD65D5EF02263FE908B286D92CE4D8C3::get_offset_of__prefabCreator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7384 = { sizeof (U3CGetAllInstancesWithInjectSplitU3Ed__3_tDF6F2C9DF8DA0E4BEC31D405E85E2E2E899AB08C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7384[6] = 
{
	U3CGetAllInstancesWithInjectSplitU3Ed__3_tDF6F2C9DF8DA0E4BEC31D405E85E2E2E899AB08C::get_offset_of_U3CU3E1__state_0(),
	U3CGetAllInstancesWithInjectSplitU3Ed__3_tDF6F2C9DF8DA0E4BEC31D405E85E2E2E899AB08C::get_offset_of_U3CU3E2__current_1(),
	U3CGetAllInstancesWithInjectSplitU3Ed__3_tDF6F2C9DF8DA0E4BEC31D405E85E2E2E899AB08C::get_offset_of_U3CU3E4__this_2(),
	U3CGetAllInstancesWithInjectSplitU3Ed__3_tDF6F2C9DF8DA0E4BEC31D405E85E2E2E899AB08C::get_offset_of_args_3(),
	U3CGetAllInstancesWithInjectSplitU3Ed__3_tDF6F2C9DF8DA0E4BEC31D405E85E2E2E899AB08C::get_offset_of_U3CrunnerU3E5__2_4(),
	U3CGetAllInstancesWithInjectSplitU3Ed__3_tDF6F2C9DF8DA0E4BEC31D405E85E2E2E899AB08C::get_offset_of_U3ChasMoreU3E5__3_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7385 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7385[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7386 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7386[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7387 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7387[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7388 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7388[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7389 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7389[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7390 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7390[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7391 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7391[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7392 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7392[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7393 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7393[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7394 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7394[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7395 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7395[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7396 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7396[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7397 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7397[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7398 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7398[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7399 = { sizeof (InstanceProvider_t7063C82B8A97AAB00B4296990A3FFD011493A236), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7399[3] = 
{
	InstanceProvider_t7063C82B8A97AAB00B4296990A3FFD011493A236::get_offset_of__instance_0(),
	InstanceProvider_t7063C82B8A97AAB00B4296990A3FFD011493A236::get_offset_of__instanceType_1(),
	InstanceProvider_t7063C82B8A97AAB00B4296990A3FFD011493A236::get_offset_of__container_2(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
