﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// DG.Tweening.Tween
struct Tween_t119487E0AB84EF563521F1043116BDBAE68AC437;
// DG.Tweening.Tween[]
struct TweenU5BU5D_t59E878639D2690DEB50DB29D6D3871EB68312DD5;
// Entitas.ContextInfo
struct ContextInfo_tC2E42F30007CB6FEAE0F1E8B6EE4240270D59CED;
// Entitas.EntityComponentChanged
struct EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A;
// Entitas.EntityComponentReplaced
struct EntityComponentReplaced_tECE10545D16DE6AECA3B03B47FA2582FE7A11B57;
// Entitas.EntityEvent
struct EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6;
// Entitas.IAERC
struct IAERC_t5B01A73F4F13C01268E7BFAEA2D6397AAA38DDA0;
// Entitas.IComponent
struct IComponent_tE1E43FF3BDF6A6960F83258783FE4CEADD8B85DC;
// Entitas.IComponent[]
struct IComponentU5BU5D_tFF2E27ADA201A9FEE088EAD3BA6A3B92F5FF9B0B;
// Entitas.IContext
struct IContext_t620F28E017D438765172A4D4551A2D82B0448C50;
// Entitas.IEntity
struct IEntity_tB710DED3F76286AAA2C85314F575D57C65F1F379;
// Entitas.IGroup
struct IGroup_t94F870F72969813321E23B41ECE258F1EBE226F2;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.HashSet`1<System.Object>
struct HashSet_1_t297CD7F944846107B388993164FCD9E317A338A3;
// System.Collections.Generic.List`1<DG.Tweening.Tween>
struct List_1_t57BB69F1AC3759152D9E750F6120000328D367B8;
// System.Collections.Generic.List`1<Entitas.ICleanupSystem>
struct List_1_t3E0F132FC1070967FBCD19B4BDF44B4F85CA03AC;
// System.Collections.Generic.List`1<Entitas.IComponent>
struct List_1_tD91F3E0C71F5B0B17D56C4CD48174E1BC2FFB9AE;
// System.Collections.Generic.List`1<Entitas.IExecuteSystem>
struct List_1_tE232269578E5E48549D25DD0C9823B612D968293;
// System.Collections.Generic.List`1<Entitas.IInitializeSystem>
struct List_1_t1E70CB97F04FCE20A9F01BCEBF8EB1C867FC348C;
// System.Collections.Generic.List`1<Entitas.ITearDownSystem>
struct List_1_t78F50ABA4688AD6A1255F66AB89EE8BAFD069913;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226;
// System.Collections.Generic.Stack`1<DG.Tweening.Tween>
struct Stack_1_t6CA58A0BB4B4756170E9E83B26CD1DDE07AFA198;
// System.Collections.Generic.Stack`1<Entitas.IComponent>[]
struct Stack_1U5BU5D_t959DEE82B18D275AF34FB16A9E6547FDD3ACF272;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.Func`2<Entitas.IEntity,System.String>
struct Func_2_t1FF357CE5BDF87DFFA00204A6E5BE7F5FA1251EE;
// System.Func`2<System.Object,System.String>
struct Func_2_t44B347E67E515867D995E8BD5EFD67FA88CE53CF;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;

struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;



#ifndef U3CMODULEU3E_T742F26D51A4DF97E0C52DE993659E98281A71377_H
#define U3CMODULEU3E_T742F26D51A4DF97E0C52DE993659E98281A71377_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t742F26D51A4DF97E0C52DE993659E98281A71377 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T742F26D51A4DF97E0C52DE993659E98281A71377_H
#ifndef U3CMODULEU3E_T45C880959CDBDE1C6E60B255EF64A1F2A2EE6A82_H
#define U3CMODULEU3E_T45C880959CDBDE1C6E60B255EF64A1F2A2EE6A82_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t45C880959CDBDE1C6E60B255EF64A1F2A2EE6A82 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T45C880959CDBDE1C6E60B255EF64A1F2A2EE6A82_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CWAITFORCOMPLETIONU3ED__14_TABA622D9721D6AE328B313499A5377995ABF8780_H
#define U3CWAITFORCOMPLETIONU3ED__14_TABA622D9721D6AE328B313499A5377995ABF8780_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.DOTweenComponent_<WaitForCompletion>d__14
struct  U3CWaitForCompletionU3Ed__14_tABA622D9721D6AE328B313499A5377995ABF8780  : public RuntimeObject
{
public:
	// System.Int32 DG.Tweening.Core.DOTweenComponent_<WaitForCompletion>d__14::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object DG.Tweening.Core.DOTweenComponent_<WaitForCompletion>d__14::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// DG.Tweening.Tween DG.Tweening.Core.DOTweenComponent_<WaitForCompletion>d__14::t
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * ___t_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitForCompletionU3Ed__14_tABA622D9721D6AE328B313499A5377995ABF8780, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitForCompletionU3Ed__14_tABA622D9721D6AE328B313499A5377995ABF8780, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_t_2() { return static_cast<int32_t>(offsetof(U3CWaitForCompletionU3Ed__14_tABA622D9721D6AE328B313499A5377995ABF8780, ___t_2)); }
	inline Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * get_t_2() const { return ___t_2; }
	inline Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 ** get_address_of_t_2() { return &___t_2; }
	inline void set_t_2(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * value)
	{
		___t_2 = value;
		Il2CppCodeGenWriteBarrier((&___t_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWAITFORCOMPLETIONU3ED__14_TABA622D9721D6AE328B313499A5377995ABF8780_H
#ifndef U3CWAITFORELAPSEDLOOPSU3ED__17_TC386E18F77FE29C85AC9E63C8AAEB94002585A93_H
#define U3CWAITFORELAPSEDLOOPSU3ED__17_TC386E18F77FE29C85AC9E63C8AAEB94002585A93_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.DOTweenComponent_<WaitForElapsedLoops>d__17
struct  U3CWaitForElapsedLoopsU3Ed__17_tC386E18F77FE29C85AC9E63C8AAEB94002585A93  : public RuntimeObject
{
public:
	// System.Int32 DG.Tweening.Core.DOTweenComponent_<WaitForElapsedLoops>d__17::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object DG.Tweening.Core.DOTweenComponent_<WaitForElapsedLoops>d__17::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// DG.Tweening.Tween DG.Tweening.Core.DOTweenComponent_<WaitForElapsedLoops>d__17::t
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * ___t_2;
	// System.Int32 DG.Tweening.Core.DOTweenComponent_<WaitForElapsedLoops>d__17::elapsedLoops
	int32_t ___elapsedLoops_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitForElapsedLoopsU3Ed__17_tC386E18F77FE29C85AC9E63C8AAEB94002585A93, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitForElapsedLoopsU3Ed__17_tC386E18F77FE29C85AC9E63C8AAEB94002585A93, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_t_2() { return static_cast<int32_t>(offsetof(U3CWaitForElapsedLoopsU3Ed__17_tC386E18F77FE29C85AC9E63C8AAEB94002585A93, ___t_2)); }
	inline Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * get_t_2() const { return ___t_2; }
	inline Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 ** get_address_of_t_2() { return &___t_2; }
	inline void set_t_2(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * value)
	{
		___t_2 = value;
		Il2CppCodeGenWriteBarrier((&___t_2), value);
	}

	inline static int32_t get_offset_of_elapsedLoops_3() { return static_cast<int32_t>(offsetof(U3CWaitForElapsedLoopsU3Ed__17_tC386E18F77FE29C85AC9E63C8AAEB94002585A93, ___elapsedLoops_3)); }
	inline int32_t get_elapsedLoops_3() const { return ___elapsedLoops_3; }
	inline int32_t* get_address_of_elapsedLoops_3() { return &___elapsedLoops_3; }
	inline void set_elapsedLoops_3(int32_t value)
	{
		___elapsedLoops_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWAITFORELAPSEDLOOPSU3ED__17_TC386E18F77FE29C85AC9E63C8AAEB94002585A93_H
#ifndef U3CWAITFORKILLU3ED__16_TDEA92D4CB64EDF28F23753CDC00A8497D2EA8E29_H
#define U3CWAITFORKILLU3ED__16_TDEA92D4CB64EDF28F23753CDC00A8497D2EA8E29_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.DOTweenComponent_<WaitForKill>d__16
struct  U3CWaitForKillU3Ed__16_tDEA92D4CB64EDF28F23753CDC00A8497D2EA8E29  : public RuntimeObject
{
public:
	// System.Int32 DG.Tweening.Core.DOTweenComponent_<WaitForKill>d__16::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object DG.Tweening.Core.DOTweenComponent_<WaitForKill>d__16::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// DG.Tweening.Tween DG.Tweening.Core.DOTweenComponent_<WaitForKill>d__16::t
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * ___t_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitForKillU3Ed__16_tDEA92D4CB64EDF28F23753CDC00A8497D2EA8E29, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitForKillU3Ed__16_tDEA92D4CB64EDF28F23753CDC00A8497D2EA8E29, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_t_2() { return static_cast<int32_t>(offsetof(U3CWaitForKillU3Ed__16_tDEA92D4CB64EDF28F23753CDC00A8497D2EA8E29, ___t_2)); }
	inline Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * get_t_2() const { return ___t_2; }
	inline Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 ** get_address_of_t_2() { return &___t_2; }
	inline void set_t_2(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * value)
	{
		___t_2 = value;
		Il2CppCodeGenWriteBarrier((&___t_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWAITFORKILLU3ED__16_TDEA92D4CB64EDF28F23753CDC00A8497D2EA8E29_H
#ifndef U3CWAITFORPOSITIONU3ED__18_TF105A8769B617DC60C65C66A388F207B66047748_H
#define U3CWAITFORPOSITIONU3ED__18_TF105A8769B617DC60C65C66A388F207B66047748_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.DOTweenComponent_<WaitForPosition>d__18
struct  U3CWaitForPositionU3Ed__18_tF105A8769B617DC60C65C66A388F207B66047748  : public RuntimeObject
{
public:
	// System.Int32 DG.Tweening.Core.DOTweenComponent_<WaitForPosition>d__18::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object DG.Tweening.Core.DOTweenComponent_<WaitForPosition>d__18::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// DG.Tweening.Tween DG.Tweening.Core.DOTweenComponent_<WaitForPosition>d__18::t
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * ___t_2;
	// System.Single DG.Tweening.Core.DOTweenComponent_<WaitForPosition>d__18::position
	float ___position_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitForPositionU3Ed__18_tF105A8769B617DC60C65C66A388F207B66047748, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitForPositionU3Ed__18_tF105A8769B617DC60C65C66A388F207B66047748, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_t_2() { return static_cast<int32_t>(offsetof(U3CWaitForPositionU3Ed__18_tF105A8769B617DC60C65C66A388F207B66047748, ___t_2)); }
	inline Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * get_t_2() const { return ___t_2; }
	inline Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 ** get_address_of_t_2() { return &___t_2; }
	inline void set_t_2(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * value)
	{
		___t_2 = value;
		Il2CppCodeGenWriteBarrier((&___t_2), value);
	}

	inline static int32_t get_offset_of_position_3() { return static_cast<int32_t>(offsetof(U3CWaitForPositionU3Ed__18_tF105A8769B617DC60C65C66A388F207B66047748, ___position_3)); }
	inline float get_position_3() const { return ___position_3; }
	inline float* get_address_of_position_3() { return &___position_3; }
	inline void set_position_3(float value)
	{
		___position_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWAITFORPOSITIONU3ED__18_TF105A8769B617DC60C65C66A388F207B66047748_H
#ifndef U3CWAITFORREWINDU3ED__15_TB0B3B4DBE9292613741BC793EBE22C9465E31E91_H
#define U3CWAITFORREWINDU3ED__15_TB0B3B4DBE9292613741BC793EBE22C9465E31E91_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.DOTweenComponent_<WaitForRewind>d__15
struct  U3CWaitForRewindU3Ed__15_tB0B3B4DBE9292613741BC793EBE22C9465E31E91  : public RuntimeObject
{
public:
	// System.Int32 DG.Tweening.Core.DOTweenComponent_<WaitForRewind>d__15::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object DG.Tweening.Core.DOTweenComponent_<WaitForRewind>d__15::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// DG.Tweening.Tween DG.Tweening.Core.DOTweenComponent_<WaitForRewind>d__15::t
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * ___t_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitForRewindU3Ed__15_tB0B3B4DBE9292613741BC793EBE22C9465E31E91, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitForRewindU3Ed__15_tB0B3B4DBE9292613741BC793EBE22C9465E31E91, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_t_2() { return static_cast<int32_t>(offsetof(U3CWaitForRewindU3Ed__15_tB0B3B4DBE9292613741BC793EBE22C9465E31E91, ___t_2)); }
	inline Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * get_t_2() const { return ___t_2; }
	inline Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 ** get_address_of_t_2() { return &___t_2; }
	inline void set_t_2(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * value)
	{
		___t_2 = value;
		Il2CppCodeGenWriteBarrier((&___t_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWAITFORREWINDU3ED__15_TB0B3B4DBE9292613741BC793EBE22C9465E31E91_H
#ifndef U3CWAITFORSTARTU3ED__19_T56B7F72F2BA96960DE945427A547739C42D33FCA_H
#define U3CWAITFORSTARTU3ED__19_T56B7F72F2BA96960DE945427A547739C42D33FCA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.DOTweenComponent_<WaitForStart>d__19
struct  U3CWaitForStartU3Ed__19_t56B7F72F2BA96960DE945427A547739C42D33FCA  : public RuntimeObject
{
public:
	// System.Int32 DG.Tweening.Core.DOTweenComponent_<WaitForStart>d__19::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object DG.Tweening.Core.DOTweenComponent_<WaitForStart>d__19::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// DG.Tweening.Tween DG.Tweening.Core.DOTweenComponent_<WaitForStart>d__19::t
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * ___t_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitForStartU3Ed__19_t56B7F72F2BA96960DE945427A547739C42D33FCA, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitForStartU3Ed__19_t56B7F72F2BA96960DE945427A547739C42D33FCA, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_t_2() { return static_cast<int32_t>(offsetof(U3CWaitForStartU3Ed__19_t56B7F72F2BA96960DE945427A547739C42D33FCA, ___t_2)); }
	inline Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * get_t_2() const { return ___t_2; }
	inline Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 ** get_address_of_t_2() { return &___t_2; }
	inline void set_t_2(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * value)
	{
		___t_2 = value;
		Il2CppCodeGenWriteBarrier((&___t_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWAITFORSTARTU3ED__19_T56B7F72F2BA96960DE945427A547739C42D33FCA_H
#ifndef DEBUGGER_TD9D47C252FB20009C8276590D54394E430619D16_H
#define DEBUGGER_TD9D47C252FB20009C8276590D54394E430619D16_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.Debugger
struct  Debugger_tD9D47C252FB20009C8276590D54394E430619D16  : public RuntimeObject
{
public:

public:
};

struct Debugger_tD9D47C252FB20009C8276590D54394E430619D16_StaticFields
{
public:
	// System.Int32 DG.Tweening.Core.Debugger::logPriority
	int32_t ___logPriority_0;

public:
	inline static int32_t get_offset_of_logPriority_0() { return static_cast<int32_t>(offsetof(Debugger_tD9D47C252FB20009C8276590D54394E430619D16_StaticFields, ___logPriority_0)); }
	inline int32_t get_logPriority_0() const { return ___logPriority_0; }
	inline int32_t* get_address_of_logPriority_0() { return &___logPriority_0; }
	inline void set_logPriority_0(int32_t value)
	{
		___logPriority_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGGER_TD9D47C252FB20009C8276590D54394E430619D16_H
#ifndef BOUNCE_TF73282443E3B5769C935FFD5431CB1845F59AE83_H
#define BOUNCE_TF73282443E3B5769C935FFD5431CB1845F59AE83_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.Easing.Bounce
struct  Bounce_tF73282443E3B5769C935FFD5431CB1845F59AE83  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNCE_TF73282443E3B5769C935FFD5431CB1845F59AE83_H
#ifndef EASEMANAGER_T3F8C15CCC71E6E2388920BCF5147DB78AFCE54CA_H
#define EASEMANAGER_T3F8C15CCC71E6E2388920BCF5147DB78AFCE54CA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.Easing.EaseManager
struct  EaseManager_t3F8C15CCC71E6E2388920BCF5147DB78AFCE54CA  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EASEMANAGER_T3F8C15CCC71E6E2388920BCF5147DB78AFCE54CA_H
#ifndef FLASH_TE3F9477809C448F0F16BF2F64A707E48F8C91B3E_H
#define FLASH_TE3F9477809C448F0F16BF2F64A707E48F8C91B3E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.Easing.Flash
struct  Flash_tE3F9477809C448F0F16BF2F64A707E48F8C91B3E  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLASH_TE3F9477809C448F0F16BF2F64A707E48F8C91B3E_H
#ifndef EXTENSIONS_T0EE019A0F679EAB3ACB55E1C9F31F7A1FAE8B475_H
#define EXTENSIONS_T0EE019A0F679EAB3ACB55E1C9F31F7A1FAE8B475_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.Extensions
struct  Extensions_t0EE019A0F679EAB3ACB55E1C9F31F7A1FAE8B475  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENSIONS_T0EE019A0F679EAB3ACB55E1C9F31F7A1FAE8B475_H
#ifndef TWEENMANAGER_T60E1FACD2C008A79361FCD1037D92408C1DAF386_H
#define TWEENMANAGER_T60E1FACD2C008A79361FCD1037D92408C1DAF386_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.TweenManager
struct  TweenManager_t60E1FACD2C008A79361FCD1037D92408C1DAF386  : public RuntimeObject
{
public:

public:
};

struct TweenManager_t60E1FACD2C008A79361FCD1037D92408C1DAF386_StaticFields
{
public:
	// System.Int32 DG.Tweening.Core.TweenManager::maxActive
	int32_t ___maxActive_0;
	// System.Int32 DG.Tweening.Core.TweenManager::maxTweeners
	int32_t ___maxTweeners_1;
	// System.Int32 DG.Tweening.Core.TweenManager::maxSequences
	int32_t ___maxSequences_2;
	// System.Boolean DG.Tweening.Core.TweenManager::hasActiveTweens
	bool ___hasActiveTweens_3;
	// System.Boolean DG.Tweening.Core.TweenManager::hasActiveDefaultTweens
	bool ___hasActiveDefaultTweens_4;
	// System.Boolean DG.Tweening.Core.TweenManager::hasActiveLateTweens
	bool ___hasActiveLateTweens_5;
	// System.Boolean DG.Tweening.Core.TweenManager::hasActiveFixedTweens
	bool ___hasActiveFixedTweens_6;
	// System.Boolean DG.Tweening.Core.TweenManager::hasActiveManualTweens
	bool ___hasActiveManualTweens_7;
	// System.Int32 DG.Tweening.Core.TweenManager::totActiveTweens
	int32_t ___totActiveTweens_8;
	// System.Int32 DG.Tweening.Core.TweenManager::totActiveDefaultTweens
	int32_t ___totActiveDefaultTweens_9;
	// System.Int32 DG.Tweening.Core.TweenManager::totActiveLateTweens
	int32_t ___totActiveLateTweens_10;
	// System.Int32 DG.Tweening.Core.TweenManager::totActiveFixedTweens
	int32_t ___totActiveFixedTweens_11;
	// System.Int32 DG.Tweening.Core.TweenManager::totActiveManualTweens
	int32_t ___totActiveManualTweens_12;
	// System.Int32 DG.Tweening.Core.TweenManager::totActiveTweeners
	int32_t ___totActiveTweeners_13;
	// System.Int32 DG.Tweening.Core.TweenManager::totActiveSequences
	int32_t ___totActiveSequences_14;
	// System.Int32 DG.Tweening.Core.TweenManager::totPooledTweeners
	int32_t ___totPooledTweeners_15;
	// System.Int32 DG.Tweening.Core.TweenManager::totPooledSequences
	int32_t ___totPooledSequences_16;
	// System.Int32 DG.Tweening.Core.TweenManager::totTweeners
	int32_t ___totTweeners_17;
	// System.Int32 DG.Tweening.Core.TweenManager::totSequences
	int32_t ___totSequences_18;
	// System.Boolean DG.Tweening.Core.TweenManager::isUpdateLoop
	bool ___isUpdateLoop_19;
	// DG.Tweening.Tween[] DG.Tweening.Core.TweenManager::_activeTweens
	TweenU5BU5D_t59E878639D2690DEB50DB29D6D3871EB68312DD5* ____activeTweens_20;
	// DG.Tweening.Tween[] DG.Tweening.Core.TweenManager::_pooledTweeners
	TweenU5BU5D_t59E878639D2690DEB50DB29D6D3871EB68312DD5* ____pooledTweeners_21;
	// System.Collections.Generic.Stack`1<DG.Tweening.Tween> DG.Tweening.Core.TweenManager::_PooledSequences
	Stack_1_t6CA58A0BB4B4756170E9E83B26CD1DDE07AFA198 * ____PooledSequences_22;
	// System.Collections.Generic.List`1<DG.Tweening.Tween> DG.Tweening.Core.TweenManager::_KillList
	List_1_t57BB69F1AC3759152D9E750F6120000328D367B8 * ____KillList_23;
	// System.Int32 DG.Tweening.Core.TweenManager::_maxActiveLookupId
	int32_t ____maxActiveLookupId_24;
	// System.Boolean DG.Tweening.Core.TweenManager::_requiresActiveReorganization
	bool ____requiresActiveReorganization_25;
	// System.Int32 DG.Tweening.Core.TweenManager::_reorganizeFromId
	int32_t ____reorganizeFromId_26;
	// System.Int32 DG.Tweening.Core.TweenManager::_minPooledTweenerId
	int32_t ____minPooledTweenerId_27;
	// System.Int32 DG.Tweening.Core.TweenManager::_maxPooledTweenerId
	int32_t ____maxPooledTweenerId_28;
	// System.Boolean DG.Tweening.Core.TweenManager::_despawnAllCalledFromUpdateLoopCallback
	bool ____despawnAllCalledFromUpdateLoopCallback_29;

public:
	inline static int32_t get_offset_of_maxActive_0() { return static_cast<int32_t>(offsetof(TweenManager_t60E1FACD2C008A79361FCD1037D92408C1DAF386_StaticFields, ___maxActive_0)); }
	inline int32_t get_maxActive_0() const { return ___maxActive_0; }
	inline int32_t* get_address_of_maxActive_0() { return &___maxActive_0; }
	inline void set_maxActive_0(int32_t value)
	{
		___maxActive_0 = value;
	}

	inline static int32_t get_offset_of_maxTweeners_1() { return static_cast<int32_t>(offsetof(TweenManager_t60E1FACD2C008A79361FCD1037D92408C1DAF386_StaticFields, ___maxTweeners_1)); }
	inline int32_t get_maxTweeners_1() const { return ___maxTweeners_1; }
	inline int32_t* get_address_of_maxTweeners_1() { return &___maxTweeners_1; }
	inline void set_maxTweeners_1(int32_t value)
	{
		___maxTweeners_1 = value;
	}

	inline static int32_t get_offset_of_maxSequences_2() { return static_cast<int32_t>(offsetof(TweenManager_t60E1FACD2C008A79361FCD1037D92408C1DAF386_StaticFields, ___maxSequences_2)); }
	inline int32_t get_maxSequences_2() const { return ___maxSequences_2; }
	inline int32_t* get_address_of_maxSequences_2() { return &___maxSequences_2; }
	inline void set_maxSequences_2(int32_t value)
	{
		___maxSequences_2 = value;
	}

	inline static int32_t get_offset_of_hasActiveTweens_3() { return static_cast<int32_t>(offsetof(TweenManager_t60E1FACD2C008A79361FCD1037D92408C1DAF386_StaticFields, ___hasActiveTweens_3)); }
	inline bool get_hasActiveTweens_3() const { return ___hasActiveTweens_3; }
	inline bool* get_address_of_hasActiveTweens_3() { return &___hasActiveTweens_3; }
	inline void set_hasActiveTweens_3(bool value)
	{
		___hasActiveTweens_3 = value;
	}

	inline static int32_t get_offset_of_hasActiveDefaultTweens_4() { return static_cast<int32_t>(offsetof(TweenManager_t60E1FACD2C008A79361FCD1037D92408C1DAF386_StaticFields, ___hasActiveDefaultTweens_4)); }
	inline bool get_hasActiveDefaultTweens_4() const { return ___hasActiveDefaultTweens_4; }
	inline bool* get_address_of_hasActiveDefaultTweens_4() { return &___hasActiveDefaultTweens_4; }
	inline void set_hasActiveDefaultTweens_4(bool value)
	{
		___hasActiveDefaultTweens_4 = value;
	}

	inline static int32_t get_offset_of_hasActiveLateTweens_5() { return static_cast<int32_t>(offsetof(TweenManager_t60E1FACD2C008A79361FCD1037D92408C1DAF386_StaticFields, ___hasActiveLateTweens_5)); }
	inline bool get_hasActiveLateTweens_5() const { return ___hasActiveLateTweens_5; }
	inline bool* get_address_of_hasActiveLateTweens_5() { return &___hasActiveLateTweens_5; }
	inline void set_hasActiveLateTweens_5(bool value)
	{
		___hasActiveLateTweens_5 = value;
	}

	inline static int32_t get_offset_of_hasActiveFixedTweens_6() { return static_cast<int32_t>(offsetof(TweenManager_t60E1FACD2C008A79361FCD1037D92408C1DAF386_StaticFields, ___hasActiveFixedTweens_6)); }
	inline bool get_hasActiveFixedTweens_6() const { return ___hasActiveFixedTweens_6; }
	inline bool* get_address_of_hasActiveFixedTweens_6() { return &___hasActiveFixedTweens_6; }
	inline void set_hasActiveFixedTweens_6(bool value)
	{
		___hasActiveFixedTweens_6 = value;
	}

	inline static int32_t get_offset_of_hasActiveManualTweens_7() { return static_cast<int32_t>(offsetof(TweenManager_t60E1FACD2C008A79361FCD1037D92408C1DAF386_StaticFields, ___hasActiveManualTweens_7)); }
	inline bool get_hasActiveManualTweens_7() const { return ___hasActiveManualTweens_7; }
	inline bool* get_address_of_hasActiveManualTweens_7() { return &___hasActiveManualTweens_7; }
	inline void set_hasActiveManualTweens_7(bool value)
	{
		___hasActiveManualTweens_7 = value;
	}

	inline static int32_t get_offset_of_totActiveTweens_8() { return static_cast<int32_t>(offsetof(TweenManager_t60E1FACD2C008A79361FCD1037D92408C1DAF386_StaticFields, ___totActiveTweens_8)); }
	inline int32_t get_totActiveTweens_8() const { return ___totActiveTweens_8; }
	inline int32_t* get_address_of_totActiveTweens_8() { return &___totActiveTweens_8; }
	inline void set_totActiveTweens_8(int32_t value)
	{
		___totActiveTweens_8 = value;
	}

	inline static int32_t get_offset_of_totActiveDefaultTweens_9() { return static_cast<int32_t>(offsetof(TweenManager_t60E1FACD2C008A79361FCD1037D92408C1DAF386_StaticFields, ___totActiveDefaultTweens_9)); }
	inline int32_t get_totActiveDefaultTweens_9() const { return ___totActiveDefaultTweens_9; }
	inline int32_t* get_address_of_totActiveDefaultTweens_9() { return &___totActiveDefaultTweens_9; }
	inline void set_totActiveDefaultTweens_9(int32_t value)
	{
		___totActiveDefaultTweens_9 = value;
	}

	inline static int32_t get_offset_of_totActiveLateTweens_10() { return static_cast<int32_t>(offsetof(TweenManager_t60E1FACD2C008A79361FCD1037D92408C1DAF386_StaticFields, ___totActiveLateTweens_10)); }
	inline int32_t get_totActiveLateTweens_10() const { return ___totActiveLateTweens_10; }
	inline int32_t* get_address_of_totActiveLateTweens_10() { return &___totActiveLateTweens_10; }
	inline void set_totActiveLateTweens_10(int32_t value)
	{
		___totActiveLateTweens_10 = value;
	}

	inline static int32_t get_offset_of_totActiveFixedTweens_11() { return static_cast<int32_t>(offsetof(TweenManager_t60E1FACD2C008A79361FCD1037D92408C1DAF386_StaticFields, ___totActiveFixedTweens_11)); }
	inline int32_t get_totActiveFixedTweens_11() const { return ___totActiveFixedTweens_11; }
	inline int32_t* get_address_of_totActiveFixedTweens_11() { return &___totActiveFixedTweens_11; }
	inline void set_totActiveFixedTweens_11(int32_t value)
	{
		___totActiveFixedTweens_11 = value;
	}

	inline static int32_t get_offset_of_totActiveManualTweens_12() { return static_cast<int32_t>(offsetof(TweenManager_t60E1FACD2C008A79361FCD1037D92408C1DAF386_StaticFields, ___totActiveManualTweens_12)); }
	inline int32_t get_totActiveManualTweens_12() const { return ___totActiveManualTweens_12; }
	inline int32_t* get_address_of_totActiveManualTweens_12() { return &___totActiveManualTweens_12; }
	inline void set_totActiveManualTweens_12(int32_t value)
	{
		___totActiveManualTweens_12 = value;
	}

	inline static int32_t get_offset_of_totActiveTweeners_13() { return static_cast<int32_t>(offsetof(TweenManager_t60E1FACD2C008A79361FCD1037D92408C1DAF386_StaticFields, ___totActiveTweeners_13)); }
	inline int32_t get_totActiveTweeners_13() const { return ___totActiveTweeners_13; }
	inline int32_t* get_address_of_totActiveTweeners_13() { return &___totActiveTweeners_13; }
	inline void set_totActiveTweeners_13(int32_t value)
	{
		___totActiveTweeners_13 = value;
	}

	inline static int32_t get_offset_of_totActiveSequences_14() { return static_cast<int32_t>(offsetof(TweenManager_t60E1FACD2C008A79361FCD1037D92408C1DAF386_StaticFields, ___totActiveSequences_14)); }
	inline int32_t get_totActiveSequences_14() const { return ___totActiveSequences_14; }
	inline int32_t* get_address_of_totActiveSequences_14() { return &___totActiveSequences_14; }
	inline void set_totActiveSequences_14(int32_t value)
	{
		___totActiveSequences_14 = value;
	}

	inline static int32_t get_offset_of_totPooledTweeners_15() { return static_cast<int32_t>(offsetof(TweenManager_t60E1FACD2C008A79361FCD1037D92408C1DAF386_StaticFields, ___totPooledTweeners_15)); }
	inline int32_t get_totPooledTweeners_15() const { return ___totPooledTweeners_15; }
	inline int32_t* get_address_of_totPooledTweeners_15() { return &___totPooledTweeners_15; }
	inline void set_totPooledTweeners_15(int32_t value)
	{
		___totPooledTweeners_15 = value;
	}

	inline static int32_t get_offset_of_totPooledSequences_16() { return static_cast<int32_t>(offsetof(TweenManager_t60E1FACD2C008A79361FCD1037D92408C1DAF386_StaticFields, ___totPooledSequences_16)); }
	inline int32_t get_totPooledSequences_16() const { return ___totPooledSequences_16; }
	inline int32_t* get_address_of_totPooledSequences_16() { return &___totPooledSequences_16; }
	inline void set_totPooledSequences_16(int32_t value)
	{
		___totPooledSequences_16 = value;
	}

	inline static int32_t get_offset_of_totTweeners_17() { return static_cast<int32_t>(offsetof(TweenManager_t60E1FACD2C008A79361FCD1037D92408C1DAF386_StaticFields, ___totTweeners_17)); }
	inline int32_t get_totTweeners_17() const { return ___totTweeners_17; }
	inline int32_t* get_address_of_totTweeners_17() { return &___totTweeners_17; }
	inline void set_totTweeners_17(int32_t value)
	{
		___totTweeners_17 = value;
	}

	inline static int32_t get_offset_of_totSequences_18() { return static_cast<int32_t>(offsetof(TweenManager_t60E1FACD2C008A79361FCD1037D92408C1DAF386_StaticFields, ___totSequences_18)); }
	inline int32_t get_totSequences_18() const { return ___totSequences_18; }
	inline int32_t* get_address_of_totSequences_18() { return &___totSequences_18; }
	inline void set_totSequences_18(int32_t value)
	{
		___totSequences_18 = value;
	}

	inline static int32_t get_offset_of_isUpdateLoop_19() { return static_cast<int32_t>(offsetof(TweenManager_t60E1FACD2C008A79361FCD1037D92408C1DAF386_StaticFields, ___isUpdateLoop_19)); }
	inline bool get_isUpdateLoop_19() const { return ___isUpdateLoop_19; }
	inline bool* get_address_of_isUpdateLoop_19() { return &___isUpdateLoop_19; }
	inline void set_isUpdateLoop_19(bool value)
	{
		___isUpdateLoop_19 = value;
	}

	inline static int32_t get_offset_of__activeTweens_20() { return static_cast<int32_t>(offsetof(TweenManager_t60E1FACD2C008A79361FCD1037D92408C1DAF386_StaticFields, ____activeTweens_20)); }
	inline TweenU5BU5D_t59E878639D2690DEB50DB29D6D3871EB68312DD5* get__activeTweens_20() const { return ____activeTweens_20; }
	inline TweenU5BU5D_t59E878639D2690DEB50DB29D6D3871EB68312DD5** get_address_of__activeTweens_20() { return &____activeTweens_20; }
	inline void set__activeTweens_20(TweenU5BU5D_t59E878639D2690DEB50DB29D6D3871EB68312DD5* value)
	{
		____activeTweens_20 = value;
		Il2CppCodeGenWriteBarrier((&____activeTweens_20), value);
	}

	inline static int32_t get_offset_of__pooledTweeners_21() { return static_cast<int32_t>(offsetof(TweenManager_t60E1FACD2C008A79361FCD1037D92408C1DAF386_StaticFields, ____pooledTweeners_21)); }
	inline TweenU5BU5D_t59E878639D2690DEB50DB29D6D3871EB68312DD5* get__pooledTweeners_21() const { return ____pooledTweeners_21; }
	inline TweenU5BU5D_t59E878639D2690DEB50DB29D6D3871EB68312DD5** get_address_of__pooledTweeners_21() { return &____pooledTweeners_21; }
	inline void set__pooledTweeners_21(TweenU5BU5D_t59E878639D2690DEB50DB29D6D3871EB68312DD5* value)
	{
		____pooledTweeners_21 = value;
		Il2CppCodeGenWriteBarrier((&____pooledTweeners_21), value);
	}

	inline static int32_t get_offset_of__PooledSequences_22() { return static_cast<int32_t>(offsetof(TweenManager_t60E1FACD2C008A79361FCD1037D92408C1DAF386_StaticFields, ____PooledSequences_22)); }
	inline Stack_1_t6CA58A0BB4B4756170E9E83B26CD1DDE07AFA198 * get__PooledSequences_22() const { return ____PooledSequences_22; }
	inline Stack_1_t6CA58A0BB4B4756170E9E83B26CD1DDE07AFA198 ** get_address_of__PooledSequences_22() { return &____PooledSequences_22; }
	inline void set__PooledSequences_22(Stack_1_t6CA58A0BB4B4756170E9E83B26CD1DDE07AFA198 * value)
	{
		____PooledSequences_22 = value;
		Il2CppCodeGenWriteBarrier((&____PooledSequences_22), value);
	}

	inline static int32_t get_offset_of__KillList_23() { return static_cast<int32_t>(offsetof(TweenManager_t60E1FACD2C008A79361FCD1037D92408C1DAF386_StaticFields, ____KillList_23)); }
	inline List_1_t57BB69F1AC3759152D9E750F6120000328D367B8 * get__KillList_23() const { return ____KillList_23; }
	inline List_1_t57BB69F1AC3759152D9E750F6120000328D367B8 ** get_address_of__KillList_23() { return &____KillList_23; }
	inline void set__KillList_23(List_1_t57BB69F1AC3759152D9E750F6120000328D367B8 * value)
	{
		____KillList_23 = value;
		Il2CppCodeGenWriteBarrier((&____KillList_23), value);
	}

	inline static int32_t get_offset_of__maxActiveLookupId_24() { return static_cast<int32_t>(offsetof(TweenManager_t60E1FACD2C008A79361FCD1037D92408C1DAF386_StaticFields, ____maxActiveLookupId_24)); }
	inline int32_t get__maxActiveLookupId_24() const { return ____maxActiveLookupId_24; }
	inline int32_t* get_address_of__maxActiveLookupId_24() { return &____maxActiveLookupId_24; }
	inline void set__maxActiveLookupId_24(int32_t value)
	{
		____maxActiveLookupId_24 = value;
	}

	inline static int32_t get_offset_of__requiresActiveReorganization_25() { return static_cast<int32_t>(offsetof(TweenManager_t60E1FACD2C008A79361FCD1037D92408C1DAF386_StaticFields, ____requiresActiveReorganization_25)); }
	inline bool get__requiresActiveReorganization_25() const { return ____requiresActiveReorganization_25; }
	inline bool* get_address_of__requiresActiveReorganization_25() { return &____requiresActiveReorganization_25; }
	inline void set__requiresActiveReorganization_25(bool value)
	{
		____requiresActiveReorganization_25 = value;
	}

	inline static int32_t get_offset_of__reorganizeFromId_26() { return static_cast<int32_t>(offsetof(TweenManager_t60E1FACD2C008A79361FCD1037D92408C1DAF386_StaticFields, ____reorganizeFromId_26)); }
	inline int32_t get__reorganizeFromId_26() const { return ____reorganizeFromId_26; }
	inline int32_t* get_address_of__reorganizeFromId_26() { return &____reorganizeFromId_26; }
	inline void set__reorganizeFromId_26(int32_t value)
	{
		____reorganizeFromId_26 = value;
	}

	inline static int32_t get_offset_of__minPooledTweenerId_27() { return static_cast<int32_t>(offsetof(TweenManager_t60E1FACD2C008A79361FCD1037D92408C1DAF386_StaticFields, ____minPooledTweenerId_27)); }
	inline int32_t get__minPooledTweenerId_27() const { return ____minPooledTweenerId_27; }
	inline int32_t* get_address_of__minPooledTweenerId_27() { return &____minPooledTweenerId_27; }
	inline void set__minPooledTweenerId_27(int32_t value)
	{
		____minPooledTweenerId_27 = value;
	}

	inline static int32_t get_offset_of__maxPooledTweenerId_28() { return static_cast<int32_t>(offsetof(TweenManager_t60E1FACD2C008A79361FCD1037D92408C1DAF386_StaticFields, ____maxPooledTweenerId_28)); }
	inline int32_t get__maxPooledTweenerId_28() const { return ____maxPooledTweenerId_28; }
	inline int32_t* get_address_of__maxPooledTweenerId_28() { return &____maxPooledTweenerId_28; }
	inline void set__maxPooledTweenerId_28(int32_t value)
	{
		____maxPooledTweenerId_28 = value;
	}

	inline static int32_t get_offset_of__despawnAllCalledFromUpdateLoopCallback_29() { return static_cast<int32_t>(offsetof(TweenManager_t60E1FACD2C008A79361FCD1037D92408C1DAF386_StaticFields, ____despawnAllCalledFromUpdateLoopCallback_29)); }
	inline bool get__despawnAllCalledFromUpdateLoopCallback_29() const { return ____despawnAllCalledFromUpdateLoopCallback_29; }
	inline bool* get_address_of__despawnAllCalledFromUpdateLoopCallback_29() { return &____despawnAllCalledFromUpdateLoopCallback_29; }
	inline void set__despawnAllCalledFromUpdateLoopCallback_29(bool value)
	{
		____despawnAllCalledFromUpdateLoopCallback_29 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENMANAGER_T60E1FACD2C008A79361FCD1037D92408C1DAF386_H
#ifndef COLLECTORCONTEXTEXTENSION_TAE2C92F0329285A4FA7360BB31C30E6FF8BB32F4_H
#define COLLECTORCONTEXTEXTENSION_TAE2C92F0329285A4FA7360BB31C30E6FF8BB32F4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.CollectorContextExtension
struct  CollectorContextExtension_tAE2C92F0329285A4FA7360BB31C30E6FF8BB32F4  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTORCONTEXTEXTENSION_TAE2C92F0329285A4FA7360BB31C30E6FF8BB32F4_H
#ifndef CONTEXTINFO_TC2E42F30007CB6FEAE0F1E8B6EE4240270D59CED_H
#define CONTEXTINFO_TC2E42F30007CB6FEAE0F1E8B6EE4240270D59CED_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.ContextInfo
struct  ContextInfo_tC2E42F30007CB6FEAE0F1E8B6EE4240270D59CED  : public RuntimeObject
{
public:
	// System.String Entitas.ContextInfo::name
	String_t* ___name_0;
	// System.String[] Entitas.ContextInfo::componentNames
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___componentNames_1;
	// System.Type[] Entitas.ContextInfo::componentTypes
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___componentTypes_2;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(ContextInfo_tC2E42F30007CB6FEAE0F1E8B6EE4240270D59CED, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_componentNames_1() { return static_cast<int32_t>(offsetof(ContextInfo_tC2E42F30007CB6FEAE0F1E8B6EE4240270D59CED, ___componentNames_1)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_componentNames_1() const { return ___componentNames_1; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_componentNames_1() { return &___componentNames_1; }
	inline void set_componentNames_1(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___componentNames_1 = value;
		Il2CppCodeGenWriteBarrier((&___componentNames_1), value);
	}

	inline static int32_t get_offset_of_componentTypes_2() { return static_cast<int32_t>(offsetof(ContextInfo_tC2E42F30007CB6FEAE0F1E8B6EE4240270D59CED, ___componentTypes_2)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get_componentTypes_2() const { return ___componentTypes_2; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of_componentTypes_2() { return &___componentTypes_2; }
	inline void set_componentTypes_2(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		___componentTypes_2 = value;
		Il2CppCodeGenWriteBarrier((&___componentTypes_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTEXTINFO_TC2E42F30007CB6FEAE0F1E8B6EE4240270D59CED_H
#ifndef U3CU3EC_TBB75825E443987A79032BCE92971925042E081B4_H
#define U3CU3EC_TBB75825E443987A79032BCE92971925042E081B4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.ContextStillHasRetainedEntitiesException_<>c
struct  U3CU3Ec_tBB75825E443987A79032BCE92971925042E081B4  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tBB75825E443987A79032BCE92971925042E081B4_StaticFields
{
public:
	// Entitas.ContextStillHasRetainedEntitiesException_<>c Entitas.ContextStillHasRetainedEntitiesException_<>c::<>9
	U3CU3Ec_tBB75825E443987A79032BCE92971925042E081B4 * ___U3CU3E9_0;
	// System.Func`2<System.Object,System.String> Entitas.ContextStillHasRetainedEntitiesException_<>c::<>9__0_1
	Func_2_t44B347E67E515867D995E8BD5EFD67FA88CE53CF * ___U3CU3E9__0_1_1;
	// System.Func`2<Entitas.IEntity,System.String> Entitas.ContextStillHasRetainedEntitiesException_<>c::<>9__0_0
	Func_2_t1FF357CE5BDF87DFFA00204A6E5BE7F5FA1251EE * ___U3CU3E9__0_0_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tBB75825E443987A79032BCE92971925042E081B4_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tBB75825E443987A79032BCE92971925042E081B4 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tBB75825E443987A79032BCE92971925042E081B4 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tBB75825E443987A79032BCE92971925042E081B4 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__0_1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tBB75825E443987A79032BCE92971925042E081B4_StaticFields, ___U3CU3E9__0_1_1)); }
	inline Func_2_t44B347E67E515867D995E8BD5EFD67FA88CE53CF * get_U3CU3E9__0_1_1() const { return ___U3CU3E9__0_1_1; }
	inline Func_2_t44B347E67E515867D995E8BD5EFD67FA88CE53CF ** get_address_of_U3CU3E9__0_1_1() { return &___U3CU3E9__0_1_1; }
	inline void set_U3CU3E9__0_1_1(Func_2_t44B347E67E515867D995E8BD5EFD67FA88CE53CF * value)
	{
		___U3CU3E9__0_1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__0_1_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__0_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_tBB75825E443987A79032BCE92971925042E081B4_StaticFields, ___U3CU3E9__0_0_2)); }
	inline Func_2_t1FF357CE5BDF87DFFA00204A6E5BE7F5FA1251EE * get_U3CU3E9__0_0_2() const { return ___U3CU3E9__0_0_2; }
	inline Func_2_t1FF357CE5BDF87DFFA00204A6E5BE7F5FA1251EE ** get_address_of_U3CU3E9__0_0_2() { return &___U3CU3E9__0_0_2; }
	inline void set_U3CU3E9__0_0_2(Func_2_t1FF357CE5BDF87DFFA00204A6E5BE7F5FA1251EE * value)
	{
		___U3CU3E9__0_0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__0_0_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TBB75825E443987A79032BCE92971925042E081B4_H
#ifndef ENTITY_TB86FED06A87B5FEA836FF73B89D5168789557783_H
#define ENTITY_TB86FED06A87B5FEA836FF73B89D5168789557783_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.Entity
struct  Entity_tB86FED06A87B5FEA836FF73B89D5168789557783  : public RuntimeObject
{
public:
	// Entitas.EntityComponentChanged Entitas.Entity::OnComponentAdded
	EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * ___OnComponentAdded_0;
	// Entitas.EntityComponentChanged Entitas.Entity::OnComponentRemoved
	EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * ___OnComponentRemoved_1;
	// Entitas.EntityComponentReplaced Entitas.Entity::OnComponentReplaced
	EntityComponentReplaced_tECE10545D16DE6AECA3B03B47FA2582FE7A11B57 * ___OnComponentReplaced_2;
	// Entitas.EntityEvent Entitas.Entity::OnEntityReleased
	EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * ___OnEntityReleased_3;
	// Entitas.EntityEvent Entitas.Entity::OnDestroyEntity
	EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * ___OnDestroyEntity_4;
	// System.Collections.Generic.List`1<Entitas.IComponent> Entitas.Entity::_componentBuffer
	List_1_tD91F3E0C71F5B0B17D56C4CD48174E1BC2FFB9AE * ____componentBuffer_5;
	// System.Collections.Generic.List`1<System.Int32> Entitas.Entity::_indexBuffer
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ____indexBuffer_6;
	// System.Int32 Entitas.Entity::_creationIndex
	int32_t ____creationIndex_7;
	// System.Boolean Entitas.Entity::_isEnabled
	bool ____isEnabled_8;
	// System.Int32 Entitas.Entity::_totalComponents
	int32_t ____totalComponents_9;
	// Entitas.IComponent[] Entitas.Entity::_components
	IComponentU5BU5D_tFF2E27ADA201A9FEE088EAD3BA6A3B92F5FF9B0B* ____components_10;
	// System.Collections.Generic.Stack`1<Entitas.IComponent>[] Entitas.Entity::_componentPools
	Stack_1U5BU5D_t959DEE82B18D275AF34FB16A9E6547FDD3ACF272* ____componentPools_11;
	// Entitas.ContextInfo Entitas.Entity::_contextInfo
	ContextInfo_tC2E42F30007CB6FEAE0F1E8B6EE4240270D59CED * ____contextInfo_12;
	// Entitas.IAERC Entitas.Entity::_aerc
	RuntimeObject* ____aerc_13;
	// Entitas.IComponent[] Entitas.Entity::_componentsCache
	IComponentU5BU5D_tFF2E27ADA201A9FEE088EAD3BA6A3B92F5FF9B0B* ____componentsCache_14;
	// System.Int32[] Entitas.Entity::_componentIndicesCache
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ____componentIndicesCache_15;
	// System.String Entitas.Entity::_toStringCache
	String_t* ____toStringCache_16;
	// System.Text.StringBuilder Entitas.Entity::_toStringBuilder
	StringBuilder_t * ____toStringBuilder_17;

public:
	inline static int32_t get_offset_of_OnComponentAdded_0() { return static_cast<int32_t>(offsetof(Entity_tB86FED06A87B5FEA836FF73B89D5168789557783, ___OnComponentAdded_0)); }
	inline EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * get_OnComponentAdded_0() const { return ___OnComponentAdded_0; }
	inline EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A ** get_address_of_OnComponentAdded_0() { return &___OnComponentAdded_0; }
	inline void set_OnComponentAdded_0(EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * value)
	{
		___OnComponentAdded_0 = value;
		Il2CppCodeGenWriteBarrier((&___OnComponentAdded_0), value);
	}

	inline static int32_t get_offset_of_OnComponentRemoved_1() { return static_cast<int32_t>(offsetof(Entity_tB86FED06A87B5FEA836FF73B89D5168789557783, ___OnComponentRemoved_1)); }
	inline EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * get_OnComponentRemoved_1() const { return ___OnComponentRemoved_1; }
	inline EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A ** get_address_of_OnComponentRemoved_1() { return &___OnComponentRemoved_1; }
	inline void set_OnComponentRemoved_1(EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * value)
	{
		___OnComponentRemoved_1 = value;
		Il2CppCodeGenWriteBarrier((&___OnComponentRemoved_1), value);
	}

	inline static int32_t get_offset_of_OnComponentReplaced_2() { return static_cast<int32_t>(offsetof(Entity_tB86FED06A87B5FEA836FF73B89D5168789557783, ___OnComponentReplaced_2)); }
	inline EntityComponentReplaced_tECE10545D16DE6AECA3B03B47FA2582FE7A11B57 * get_OnComponentReplaced_2() const { return ___OnComponentReplaced_2; }
	inline EntityComponentReplaced_tECE10545D16DE6AECA3B03B47FA2582FE7A11B57 ** get_address_of_OnComponentReplaced_2() { return &___OnComponentReplaced_2; }
	inline void set_OnComponentReplaced_2(EntityComponentReplaced_tECE10545D16DE6AECA3B03B47FA2582FE7A11B57 * value)
	{
		___OnComponentReplaced_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnComponentReplaced_2), value);
	}

	inline static int32_t get_offset_of_OnEntityReleased_3() { return static_cast<int32_t>(offsetof(Entity_tB86FED06A87B5FEA836FF73B89D5168789557783, ___OnEntityReleased_3)); }
	inline EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * get_OnEntityReleased_3() const { return ___OnEntityReleased_3; }
	inline EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 ** get_address_of_OnEntityReleased_3() { return &___OnEntityReleased_3; }
	inline void set_OnEntityReleased_3(EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * value)
	{
		___OnEntityReleased_3 = value;
		Il2CppCodeGenWriteBarrier((&___OnEntityReleased_3), value);
	}

	inline static int32_t get_offset_of_OnDestroyEntity_4() { return static_cast<int32_t>(offsetof(Entity_tB86FED06A87B5FEA836FF73B89D5168789557783, ___OnDestroyEntity_4)); }
	inline EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * get_OnDestroyEntity_4() const { return ___OnDestroyEntity_4; }
	inline EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 ** get_address_of_OnDestroyEntity_4() { return &___OnDestroyEntity_4; }
	inline void set_OnDestroyEntity_4(EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * value)
	{
		___OnDestroyEntity_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnDestroyEntity_4), value);
	}

	inline static int32_t get_offset_of__componentBuffer_5() { return static_cast<int32_t>(offsetof(Entity_tB86FED06A87B5FEA836FF73B89D5168789557783, ____componentBuffer_5)); }
	inline List_1_tD91F3E0C71F5B0B17D56C4CD48174E1BC2FFB9AE * get__componentBuffer_5() const { return ____componentBuffer_5; }
	inline List_1_tD91F3E0C71F5B0B17D56C4CD48174E1BC2FFB9AE ** get_address_of__componentBuffer_5() { return &____componentBuffer_5; }
	inline void set__componentBuffer_5(List_1_tD91F3E0C71F5B0B17D56C4CD48174E1BC2FFB9AE * value)
	{
		____componentBuffer_5 = value;
		Il2CppCodeGenWriteBarrier((&____componentBuffer_5), value);
	}

	inline static int32_t get_offset_of__indexBuffer_6() { return static_cast<int32_t>(offsetof(Entity_tB86FED06A87B5FEA836FF73B89D5168789557783, ____indexBuffer_6)); }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * get__indexBuffer_6() const { return ____indexBuffer_6; }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 ** get_address_of__indexBuffer_6() { return &____indexBuffer_6; }
	inline void set__indexBuffer_6(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * value)
	{
		____indexBuffer_6 = value;
		Il2CppCodeGenWriteBarrier((&____indexBuffer_6), value);
	}

	inline static int32_t get_offset_of__creationIndex_7() { return static_cast<int32_t>(offsetof(Entity_tB86FED06A87B5FEA836FF73B89D5168789557783, ____creationIndex_7)); }
	inline int32_t get__creationIndex_7() const { return ____creationIndex_7; }
	inline int32_t* get_address_of__creationIndex_7() { return &____creationIndex_7; }
	inline void set__creationIndex_7(int32_t value)
	{
		____creationIndex_7 = value;
	}

	inline static int32_t get_offset_of__isEnabled_8() { return static_cast<int32_t>(offsetof(Entity_tB86FED06A87B5FEA836FF73B89D5168789557783, ____isEnabled_8)); }
	inline bool get__isEnabled_8() const { return ____isEnabled_8; }
	inline bool* get_address_of__isEnabled_8() { return &____isEnabled_8; }
	inline void set__isEnabled_8(bool value)
	{
		____isEnabled_8 = value;
	}

	inline static int32_t get_offset_of__totalComponents_9() { return static_cast<int32_t>(offsetof(Entity_tB86FED06A87B5FEA836FF73B89D5168789557783, ____totalComponents_9)); }
	inline int32_t get__totalComponents_9() const { return ____totalComponents_9; }
	inline int32_t* get_address_of__totalComponents_9() { return &____totalComponents_9; }
	inline void set__totalComponents_9(int32_t value)
	{
		____totalComponents_9 = value;
	}

	inline static int32_t get_offset_of__components_10() { return static_cast<int32_t>(offsetof(Entity_tB86FED06A87B5FEA836FF73B89D5168789557783, ____components_10)); }
	inline IComponentU5BU5D_tFF2E27ADA201A9FEE088EAD3BA6A3B92F5FF9B0B* get__components_10() const { return ____components_10; }
	inline IComponentU5BU5D_tFF2E27ADA201A9FEE088EAD3BA6A3B92F5FF9B0B** get_address_of__components_10() { return &____components_10; }
	inline void set__components_10(IComponentU5BU5D_tFF2E27ADA201A9FEE088EAD3BA6A3B92F5FF9B0B* value)
	{
		____components_10 = value;
		Il2CppCodeGenWriteBarrier((&____components_10), value);
	}

	inline static int32_t get_offset_of__componentPools_11() { return static_cast<int32_t>(offsetof(Entity_tB86FED06A87B5FEA836FF73B89D5168789557783, ____componentPools_11)); }
	inline Stack_1U5BU5D_t959DEE82B18D275AF34FB16A9E6547FDD3ACF272* get__componentPools_11() const { return ____componentPools_11; }
	inline Stack_1U5BU5D_t959DEE82B18D275AF34FB16A9E6547FDD3ACF272** get_address_of__componentPools_11() { return &____componentPools_11; }
	inline void set__componentPools_11(Stack_1U5BU5D_t959DEE82B18D275AF34FB16A9E6547FDD3ACF272* value)
	{
		____componentPools_11 = value;
		Il2CppCodeGenWriteBarrier((&____componentPools_11), value);
	}

	inline static int32_t get_offset_of__contextInfo_12() { return static_cast<int32_t>(offsetof(Entity_tB86FED06A87B5FEA836FF73B89D5168789557783, ____contextInfo_12)); }
	inline ContextInfo_tC2E42F30007CB6FEAE0F1E8B6EE4240270D59CED * get__contextInfo_12() const { return ____contextInfo_12; }
	inline ContextInfo_tC2E42F30007CB6FEAE0F1E8B6EE4240270D59CED ** get_address_of__contextInfo_12() { return &____contextInfo_12; }
	inline void set__contextInfo_12(ContextInfo_tC2E42F30007CB6FEAE0F1E8B6EE4240270D59CED * value)
	{
		____contextInfo_12 = value;
		Il2CppCodeGenWriteBarrier((&____contextInfo_12), value);
	}

	inline static int32_t get_offset_of__aerc_13() { return static_cast<int32_t>(offsetof(Entity_tB86FED06A87B5FEA836FF73B89D5168789557783, ____aerc_13)); }
	inline RuntimeObject* get__aerc_13() const { return ____aerc_13; }
	inline RuntimeObject** get_address_of__aerc_13() { return &____aerc_13; }
	inline void set__aerc_13(RuntimeObject* value)
	{
		____aerc_13 = value;
		Il2CppCodeGenWriteBarrier((&____aerc_13), value);
	}

	inline static int32_t get_offset_of__componentsCache_14() { return static_cast<int32_t>(offsetof(Entity_tB86FED06A87B5FEA836FF73B89D5168789557783, ____componentsCache_14)); }
	inline IComponentU5BU5D_tFF2E27ADA201A9FEE088EAD3BA6A3B92F5FF9B0B* get__componentsCache_14() const { return ____componentsCache_14; }
	inline IComponentU5BU5D_tFF2E27ADA201A9FEE088EAD3BA6A3B92F5FF9B0B** get_address_of__componentsCache_14() { return &____componentsCache_14; }
	inline void set__componentsCache_14(IComponentU5BU5D_tFF2E27ADA201A9FEE088EAD3BA6A3B92F5FF9B0B* value)
	{
		____componentsCache_14 = value;
		Il2CppCodeGenWriteBarrier((&____componentsCache_14), value);
	}

	inline static int32_t get_offset_of__componentIndicesCache_15() { return static_cast<int32_t>(offsetof(Entity_tB86FED06A87B5FEA836FF73B89D5168789557783, ____componentIndicesCache_15)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get__componentIndicesCache_15() const { return ____componentIndicesCache_15; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of__componentIndicesCache_15() { return &____componentIndicesCache_15; }
	inline void set__componentIndicesCache_15(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		____componentIndicesCache_15 = value;
		Il2CppCodeGenWriteBarrier((&____componentIndicesCache_15), value);
	}

	inline static int32_t get_offset_of__toStringCache_16() { return static_cast<int32_t>(offsetof(Entity_tB86FED06A87B5FEA836FF73B89D5168789557783, ____toStringCache_16)); }
	inline String_t* get__toStringCache_16() const { return ____toStringCache_16; }
	inline String_t** get_address_of__toStringCache_16() { return &____toStringCache_16; }
	inline void set__toStringCache_16(String_t* value)
	{
		____toStringCache_16 = value;
		Il2CppCodeGenWriteBarrier((&____toStringCache_16), value);
	}

	inline static int32_t get_offset_of__toStringBuilder_17() { return static_cast<int32_t>(offsetof(Entity_tB86FED06A87B5FEA836FF73B89D5168789557783, ____toStringBuilder_17)); }
	inline StringBuilder_t * get__toStringBuilder_17() const { return ____toStringBuilder_17; }
	inline StringBuilder_t ** get_address_of__toStringBuilder_17() { return &____toStringBuilder_17; }
	inline void set__toStringBuilder_17(StringBuilder_t * value)
	{
		____toStringBuilder_17 = value;
		Il2CppCodeGenWriteBarrier((&____toStringBuilder_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTITY_TB86FED06A87B5FEA836FF73B89D5168789557783_H
#ifndef SAFEAERC_TC3A4274F22895D6896C084185BA5E145EF097A30_H
#define SAFEAERC_TC3A4274F22895D6896C084185BA5E145EF097A30_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.SafeAERC
struct  SafeAERC_tC3A4274F22895D6896C084185BA5E145EF097A30  : public RuntimeObject
{
public:
	// Entitas.IEntity Entitas.SafeAERC::_entity
	RuntimeObject* ____entity_0;
	// System.Collections.Generic.HashSet`1<System.Object> Entitas.SafeAERC::_owners
	HashSet_1_t297CD7F944846107B388993164FCD9E317A338A3 * ____owners_1;

public:
	inline static int32_t get_offset_of__entity_0() { return static_cast<int32_t>(offsetof(SafeAERC_tC3A4274F22895D6896C084185BA5E145EF097A30, ____entity_0)); }
	inline RuntimeObject* get__entity_0() const { return ____entity_0; }
	inline RuntimeObject** get_address_of__entity_0() { return &____entity_0; }
	inline void set__entity_0(RuntimeObject* value)
	{
		____entity_0 = value;
		Il2CppCodeGenWriteBarrier((&____entity_0), value);
	}

	inline static int32_t get_offset_of__owners_1() { return static_cast<int32_t>(offsetof(SafeAERC_tC3A4274F22895D6896C084185BA5E145EF097A30, ____owners_1)); }
	inline HashSet_1_t297CD7F944846107B388993164FCD9E317A338A3 * get__owners_1() const { return ____owners_1; }
	inline HashSet_1_t297CD7F944846107B388993164FCD9E317A338A3 ** get_address_of__owners_1() { return &____owners_1; }
	inline void set__owners_1(HashSet_1_t297CD7F944846107B388993164FCD9E317A338A3 * value)
	{
		____owners_1 = value;
		Il2CppCodeGenWriteBarrier((&____owners_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAFEAERC_TC3A4274F22895D6896C084185BA5E145EF097A30_H
#ifndef SYSTEMS_T4C86DA84B6D6F50FF23B497DD599ED594274DEA9_H
#define SYSTEMS_T4C86DA84B6D6F50FF23B497DD599ED594274DEA9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.Systems
struct  Systems_t4C86DA84B6D6F50FF23B497DD599ED594274DEA9  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Entitas.IInitializeSystem> Entitas.Systems::_initializeSystems
	List_1_t1E70CB97F04FCE20A9F01BCEBF8EB1C867FC348C * ____initializeSystems_0;
	// System.Collections.Generic.List`1<Entitas.IExecuteSystem> Entitas.Systems::_executeSystems
	List_1_tE232269578E5E48549D25DD0C9823B612D968293 * ____executeSystems_1;
	// System.Collections.Generic.List`1<Entitas.ICleanupSystem> Entitas.Systems::_cleanupSystems
	List_1_t3E0F132FC1070967FBCD19B4BDF44B4F85CA03AC * ____cleanupSystems_2;
	// System.Collections.Generic.List`1<Entitas.ITearDownSystem> Entitas.Systems::_tearDownSystems
	List_1_t78F50ABA4688AD6A1255F66AB89EE8BAFD069913 * ____tearDownSystems_3;

public:
	inline static int32_t get_offset_of__initializeSystems_0() { return static_cast<int32_t>(offsetof(Systems_t4C86DA84B6D6F50FF23B497DD599ED594274DEA9, ____initializeSystems_0)); }
	inline List_1_t1E70CB97F04FCE20A9F01BCEBF8EB1C867FC348C * get__initializeSystems_0() const { return ____initializeSystems_0; }
	inline List_1_t1E70CB97F04FCE20A9F01BCEBF8EB1C867FC348C ** get_address_of__initializeSystems_0() { return &____initializeSystems_0; }
	inline void set__initializeSystems_0(List_1_t1E70CB97F04FCE20A9F01BCEBF8EB1C867FC348C * value)
	{
		____initializeSystems_0 = value;
		Il2CppCodeGenWriteBarrier((&____initializeSystems_0), value);
	}

	inline static int32_t get_offset_of__executeSystems_1() { return static_cast<int32_t>(offsetof(Systems_t4C86DA84B6D6F50FF23B497DD599ED594274DEA9, ____executeSystems_1)); }
	inline List_1_tE232269578E5E48549D25DD0C9823B612D968293 * get__executeSystems_1() const { return ____executeSystems_1; }
	inline List_1_tE232269578E5E48549D25DD0C9823B612D968293 ** get_address_of__executeSystems_1() { return &____executeSystems_1; }
	inline void set__executeSystems_1(List_1_tE232269578E5E48549D25DD0C9823B612D968293 * value)
	{
		____executeSystems_1 = value;
		Il2CppCodeGenWriteBarrier((&____executeSystems_1), value);
	}

	inline static int32_t get_offset_of__cleanupSystems_2() { return static_cast<int32_t>(offsetof(Systems_t4C86DA84B6D6F50FF23B497DD599ED594274DEA9, ____cleanupSystems_2)); }
	inline List_1_t3E0F132FC1070967FBCD19B4BDF44B4F85CA03AC * get__cleanupSystems_2() const { return ____cleanupSystems_2; }
	inline List_1_t3E0F132FC1070967FBCD19B4BDF44B4F85CA03AC ** get_address_of__cleanupSystems_2() { return &____cleanupSystems_2; }
	inline void set__cleanupSystems_2(List_1_t3E0F132FC1070967FBCD19B4BDF44B4F85CA03AC * value)
	{
		____cleanupSystems_2 = value;
		Il2CppCodeGenWriteBarrier((&____cleanupSystems_2), value);
	}

	inline static int32_t get_offset_of__tearDownSystems_3() { return static_cast<int32_t>(offsetof(Systems_t4C86DA84B6D6F50FF23B497DD599ED594274DEA9, ____tearDownSystems_3)); }
	inline List_1_t78F50ABA4688AD6A1255F66AB89EE8BAFD069913 * get__tearDownSystems_3() const { return ____tearDownSystems_3; }
	inline List_1_t78F50ABA4688AD6A1255F66AB89EE8BAFD069913 ** get_address_of__tearDownSystems_3() { return &____tearDownSystems_3; }
	inline void set__tearDownSystems_3(List_1_t78F50ABA4688AD6A1255F66AB89EE8BAFD069913 * value)
	{
		____tearDownSystems_3 = value;
		Il2CppCodeGenWriteBarrier((&____tearDownSystems_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMS_T4C86DA84B6D6F50FF23B497DD599ED594274DEA9_H
#ifndef TRIGGERONEVENTMATCHEREXTENSION_TEF975E8E9F0D4FEE53123912F9A8837705E5FF25_H
#define TRIGGERONEVENTMATCHEREXTENSION_TEF975E8E9F0D4FEE53123912F9A8837705E5FF25_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.TriggerOnEventMatcherExtension
struct  TriggerOnEventMatcherExtension_tEF975E8E9F0D4FEE53123912F9A8837705E5FF25  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERONEVENTMATCHEREXTENSION_TEF975E8E9F0D4FEE53123912F9A8837705E5FF25_H
#ifndef ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#define ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef __STATICARRAYINITTYPESIZEU3D120_TEB325D5130B7CFF9CF1EB5B7763C067DA86B461B_H
#define __STATICARRAYINITTYPESIZEU3D120_TEB325D5130B7CFF9CF1EB5B7763C067DA86B461B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D120
struct  __StaticArrayInitTypeSizeU3D120_tEB325D5130B7CFF9CF1EB5B7763C067DA86B461B 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D120_tEB325D5130B7CFF9CF1EB5B7763C067DA86B461B__padding[120];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D120_TEB325D5130B7CFF9CF1EB5B7763C067DA86B461B_H
#ifndef __STATICARRAYINITTYPESIZEU3D20_T3ECF3082F4AA0AD5E18BDD8B1803286AF95A8329_H
#define __STATICARRAYINITTYPESIZEU3D20_T3ECF3082F4AA0AD5E18BDD8B1803286AF95A8329_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D20
struct  __StaticArrayInitTypeSizeU3D20_t3ECF3082F4AA0AD5E18BDD8B1803286AF95A8329 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D20_t3ECF3082F4AA0AD5E18BDD8B1803286AF95A8329__padding[20];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D20_T3ECF3082F4AA0AD5E18BDD8B1803286AF95A8329_H
#ifndef __STATICARRAYINITTYPESIZEU3D50_T29DB5F5A1875DCB081464683E3C76FFC9FCFCA55_H
#define __STATICARRAYINITTYPESIZEU3D50_T29DB5F5A1875DCB081464683E3C76FFC9FCFCA55_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D50
struct  __StaticArrayInitTypeSizeU3D50_t29DB5F5A1875DCB081464683E3C76FFC9FCFCA55 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D50_t29DB5F5A1875DCB081464683E3C76FFC9FCFCA55__padding[50];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D50_T29DB5F5A1875DCB081464683E3C76FFC9FCFCA55_H
#ifndef CONTEXTATTRIBUTE_TCC46F2BA273A4F8FE981674FA22BA7D229AF6817_H
#define CONTEXTATTRIBUTE_TCC46F2BA273A4F8FE981674FA22BA7D229AF6817_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.CodeGeneration.Attributes.ContextAttribute
struct  ContextAttribute_tCC46F2BA273A4F8FE981674FA22BA7D229AF6817  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String Entitas.CodeGeneration.Attributes.ContextAttribute::contextName
	String_t* ___contextName_0;

public:
	inline static int32_t get_offset_of_contextName_0() { return static_cast<int32_t>(offsetof(ContextAttribute_tCC46F2BA273A4F8FE981674FA22BA7D229AF6817, ___contextName_0)); }
	inline String_t* get_contextName_0() const { return ___contextName_0; }
	inline String_t** get_address_of_contextName_0() { return &___contextName_0; }
	inline void set_contextName_0(String_t* value)
	{
		___contextName_0 = value;
		Il2CppCodeGenWriteBarrier((&___contextName_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTEXTATTRIBUTE_TCC46F2BA273A4F8FE981674FA22BA7D229AF6817_H
#ifndef CUSTOMENTITYINDEXATTRIBUTE_T514404ABF8B11B77892F7B3454E656EA019697FD_H
#define CUSTOMENTITYINDEXATTRIBUTE_T514404ABF8B11B77892F7B3454E656EA019697FD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.CodeGeneration.Attributes.CustomEntityIndexAttribute
struct  CustomEntityIndexAttribute_t514404ABF8B11B77892F7B3454E656EA019697FD  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Type Entitas.CodeGeneration.Attributes.CustomEntityIndexAttribute::contextType
	Type_t * ___contextType_0;

public:
	inline static int32_t get_offset_of_contextType_0() { return static_cast<int32_t>(offsetof(CustomEntityIndexAttribute_t514404ABF8B11B77892F7B3454E656EA019697FD, ___contextType_0)); }
	inline Type_t * get_contextType_0() const { return ___contextType_0; }
	inline Type_t ** get_address_of_contextType_0() { return &___contextType_0; }
	inline void set_contextType_0(Type_t * value)
	{
		___contextType_0 = value;
		Il2CppCodeGenWriteBarrier((&___contextType_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMENTITYINDEXATTRIBUTE_T514404ABF8B11B77892F7B3454E656EA019697FD_H
#ifndef DONTGENERATEATTRIBUTE_TFBFC6C730BAC7F0C3B13EFEC3D91BAC4DC04C857_H
#define DONTGENERATEATTRIBUTE_TFBFC6C730BAC7F0C3B13EFEC3D91BAC4DC04C857_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.CodeGeneration.Attributes.DontGenerateAttribute
struct  DontGenerateAttribute_tFBFC6C730BAC7F0C3B13EFEC3D91BAC4DC04C857  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Boolean Entitas.CodeGeneration.Attributes.DontGenerateAttribute::generateIndex
	bool ___generateIndex_0;

public:
	inline static int32_t get_offset_of_generateIndex_0() { return static_cast<int32_t>(offsetof(DontGenerateAttribute_tFBFC6C730BAC7F0C3B13EFEC3D91BAC4DC04C857, ___generateIndex_0)); }
	inline bool get_generateIndex_0() const { return ___generateIndex_0; }
	inline bool* get_address_of_generateIndex_0() { return &___generateIndex_0; }
	inline void set_generateIndex_0(bool value)
	{
		___generateIndex_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DONTGENERATEATTRIBUTE_TFBFC6C730BAC7F0C3B13EFEC3D91BAC4DC04C857_H
#ifndef ENTITYINDEXGETMETHODATTRIBUTE_T948AD15E1610A40D1DBA5CB984A4C5260A79ADD6_H
#define ENTITYINDEXGETMETHODATTRIBUTE_T948AD15E1610A40D1DBA5CB984A4C5260A79ADD6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.CodeGeneration.Attributes.EntityIndexGetMethodAttribute
struct  EntityIndexGetMethodAttribute_t948AD15E1610A40D1DBA5CB984A4C5260A79ADD6  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTITYINDEXGETMETHODATTRIBUTE_T948AD15E1610A40D1DBA5CB984A4C5260A79ADD6_H
#ifndef ENTITASEXCEPTION_T71786D33342FC2A2F61B3AAC7FD34318ABC7CD33_H
#define ENTITASEXCEPTION_T71786D33342FC2A2F61B3AAC7FD34318ABC7CD33_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.EntitasException
struct  EntitasException_t71786D33342FC2A2F61B3AAC7FD34318ABC7CD33  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTITASEXCEPTION_T71786D33342FC2A2F61B3AAC7FD34318ABC7CD33_H
#ifndef MATCHEREXCEPTION_T753A013446CEF9B87BE52DDE80A5536442489653_H
#define MATCHEREXCEPTION_T753A013446CEF9B87BE52DDE80A5536442489653_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.MatcherException
struct  MatcherException_t753A013446CEF9B87BE52DDE80A5536442489653  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATCHEREXCEPTION_T753A013446CEF9B87BE52DDE80A5536442489653_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#define INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_TA611FCC2D7ACFEACBF22A4ED660B10D861531BEB_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_TA611FCC2D7ACFEACBF22A4ED660B10D861531BEB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_tA611FCC2D7ACFEACBF22A4ED660B10D861531BEB  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_tA611FCC2D7ACFEACBF22A4ED660B10D861531BEB_StaticFields
{
public:
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D50 <PrivateImplementationDetails>::6F98278EFCD257898AD01BE39D1D0AEFB78FC551
	__StaticArrayInitTypeSizeU3D50_t29DB5F5A1875DCB081464683E3C76FFC9FCFCA55  ___6F98278EFCD257898AD01BE39D1D0AEFB78FC551_0;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D20 <PrivateImplementationDetails>::8C4C6C4E493EC2BEEF5F6F6A9C4472C13BED42E8
	__StaticArrayInitTypeSizeU3D20_t3ECF3082F4AA0AD5E18BDD8B1803286AF95A8329  ___8C4C6C4E493EC2BEEF5F6F6A9C4472C13BED42E8_1;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D120 <PrivateImplementationDetails>::9DC5F4D0A1418B4EC71B22D21E93D134922FA735
	__StaticArrayInitTypeSizeU3D120_tEB325D5130B7CFF9CF1EB5B7763C067DA86B461B  ___9DC5F4D0A1418B4EC71B22D21E93D134922FA735_2;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D50 <PrivateImplementationDetails>::FD0BD55CDDDFD0B323012A45F83437763AF58952
	__StaticArrayInitTypeSizeU3D50_t29DB5F5A1875DCB081464683E3C76FFC9FCFCA55  ___FD0BD55CDDDFD0B323012A45F83437763AF58952_3;

public:
	inline static int32_t get_offset_of_U36F98278EFCD257898AD01BE39D1D0AEFB78FC551_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tA611FCC2D7ACFEACBF22A4ED660B10D861531BEB_StaticFields, ___6F98278EFCD257898AD01BE39D1D0AEFB78FC551_0)); }
	inline __StaticArrayInitTypeSizeU3D50_t29DB5F5A1875DCB081464683E3C76FFC9FCFCA55  get_U36F98278EFCD257898AD01BE39D1D0AEFB78FC551_0() const { return ___6F98278EFCD257898AD01BE39D1D0AEFB78FC551_0; }
	inline __StaticArrayInitTypeSizeU3D50_t29DB5F5A1875DCB081464683E3C76FFC9FCFCA55 * get_address_of_U36F98278EFCD257898AD01BE39D1D0AEFB78FC551_0() { return &___6F98278EFCD257898AD01BE39D1D0AEFB78FC551_0; }
	inline void set_U36F98278EFCD257898AD01BE39D1D0AEFB78FC551_0(__StaticArrayInitTypeSizeU3D50_t29DB5F5A1875DCB081464683E3C76FFC9FCFCA55  value)
	{
		___6F98278EFCD257898AD01BE39D1D0AEFB78FC551_0 = value;
	}

	inline static int32_t get_offset_of_U38C4C6C4E493EC2BEEF5F6F6A9C4472C13BED42E8_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tA611FCC2D7ACFEACBF22A4ED660B10D861531BEB_StaticFields, ___8C4C6C4E493EC2BEEF5F6F6A9C4472C13BED42E8_1)); }
	inline __StaticArrayInitTypeSizeU3D20_t3ECF3082F4AA0AD5E18BDD8B1803286AF95A8329  get_U38C4C6C4E493EC2BEEF5F6F6A9C4472C13BED42E8_1() const { return ___8C4C6C4E493EC2BEEF5F6F6A9C4472C13BED42E8_1; }
	inline __StaticArrayInitTypeSizeU3D20_t3ECF3082F4AA0AD5E18BDD8B1803286AF95A8329 * get_address_of_U38C4C6C4E493EC2BEEF5F6F6A9C4472C13BED42E8_1() { return &___8C4C6C4E493EC2BEEF5F6F6A9C4472C13BED42E8_1; }
	inline void set_U38C4C6C4E493EC2BEEF5F6F6A9C4472C13BED42E8_1(__StaticArrayInitTypeSizeU3D20_t3ECF3082F4AA0AD5E18BDD8B1803286AF95A8329  value)
	{
		___8C4C6C4E493EC2BEEF5F6F6A9C4472C13BED42E8_1 = value;
	}

	inline static int32_t get_offset_of_U39DC5F4D0A1418B4EC71B22D21E93D134922FA735_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tA611FCC2D7ACFEACBF22A4ED660B10D861531BEB_StaticFields, ___9DC5F4D0A1418B4EC71B22D21E93D134922FA735_2)); }
	inline __StaticArrayInitTypeSizeU3D120_tEB325D5130B7CFF9CF1EB5B7763C067DA86B461B  get_U39DC5F4D0A1418B4EC71B22D21E93D134922FA735_2() const { return ___9DC5F4D0A1418B4EC71B22D21E93D134922FA735_2; }
	inline __StaticArrayInitTypeSizeU3D120_tEB325D5130B7CFF9CF1EB5B7763C067DA86B461B * get_address_of_U39DC5F4D0A1418B4EC71B22D21E93D134922FA735_2() { return &___9DC5F4D0A1418B4EC71B22D21E93D134922FA735_2; }
	inline void set_U39DC5F4D0A1418B4EC71B22D21E93D134922FA735_2(__StaticArrayInitTypeSizeU3D120_tEB325D5130B7CFF9CF1EB5B7763C067DA86B461B  value)
	{
		___9DC5F4D0A1418B4EC71B22D21E93D134922FA735_2 = value;
	}

	inline static int32_t get_offset_of_FD0BD55CDDDFD0B323012A45F83437763AF58952_3() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tA611FCC2D7ACFEACBF22A4ED660B10D861531BEB_StaticFields, ___FD0BD55CDDDFD0B323012A45F83437763AF58952_3)); }
	inline __StaticArrayInitTypeSizeU3D50_t29DB5F5A1875DCB081464683E3C76FFC9FCFCA55  get_FD0BD55CDDDFD0B323012A45F83437763AF58952_3() const { return ___FD0BD55CDDDFD0B323012A45F83437763AF58952_3; }
	inline __StaticArrayInitTypeSizeU3D50_t29DB5F5A1875DCB081464683E3C76FFC9FCFCA55 * get_address_of_FD0BD55CDDDFD0B323012A45F83437763AF58952_3() { return &___FD0BD55CDDDFD0B323012A45F83437763AF58952_3; }
	inline void set_FD0BD55CDDDFD0B323012A45F83437763AF58952_3(__StaticArrayInitTypeSizeU3D50_t29DB5F5A1875DCB081464683E3C76FFC9FCFCA55  value)
	{
		___FD0BD55CDDDFD0B323012A45F83437763AF58952_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_TA611FCC2D7ACFEACBF22A4ED660B10D861531BEB_H
#ifndef AUTOPLAY_T17E2BFFF87FCE2B8BAD2FD23B2B65379B515EC12_H
#define AUTOPLAY_T17E2BFFF87FCE2B8BAD2FD23B2B65379B515EC12_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.AutoPlay
struct  AutoPlay_t17E2BFFF87FCE2B8BAD2FD23B2B65379B515EC12 
{
public:
	// System.Int32 DG.Tweening.AutoPlay::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AutoPlay_t17E2BFFF87FCE2B8BAD2FD23B2B65379B515EC12, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTOPLAY_T17E2BFFF87FCE2B8BAD2FD23B2B65379B515EC12_H
#ifndef SETTINGSLOCATION_T6D5FD003DE9DA2EB1AC2EBD0B8CA88E9176ED452_H
#define SETTINGSLOCATION_T6D5FD003DE9DA2EB1AC2EBD0B8CA88E9176ED452_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.DOTweenSettings_SettingsLocation
struct  SettingsLocation_t6D5FD003DE9DA2EB1AC2EBD0B8CA88E9176ED452 
{
public:
	// System.Int32 DG.Tweening.Core.DOTweenSettings_SettingsLocation::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SettingsLocation_t6D5FD003DE9DA2EB1AC2EBD0B8CA88E9176ED452, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETTINGSLOCATION_T6D5FD003DE9DA2EB1AC2EBD0B8CA88E9176ED452_H
#ifndef SPECIALSTARTUPMODE_TE272D2FEC46F4A5FC68E9A99E0093494AA4E0E49_H
#define SPECIALSTARTUPMODE_TE272D2FEC46F4A5FC68E9A99E0093494AA4E0E49_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.Enums.SpecialStartupMode
struct  SpecialStartupMode_tE272D2FEC46F4A5FC68E9A99E0093494AA4E0E49 
{
public:
	// System.Int32 DG.Tweening.Core.Enums.SpecialStartupMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SpecialStartupMode_tE272D2FEC46F4A5FC68E9A99E0093494AA4E0E49, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPECIALSTARTUPMODE_TE272D2FEC46F4A5FC68E9A99E0093494AA4E0E49_H
#ifndef UPDATEMODE_TF271804A5FF87FE0CBFBD3FF6011D9499D5C6662_H
#define UPDATEMODE_TF271804A5FF87FE0CBFBD3FF6011D9499D5C6662_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.Enums.UpdateMode
struct  UpdateMode_tF271804A5FF87FE0CBFBD3FF6011D9499D5C6662 
{
public:
	// System.Int32 DG.Tweening.Core.Enums.UpdateMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UpdateMode_tF271804A5FF87FE0CBFBD3FF6011D9499D5C6662, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATEMODE_TF271804A5FF87FE0CBFBD3FF6011D9499D5C6662_H
#ifndef UPDATENOTICE_T03AF68BDDDBD2D8575BA411C06D96FC886C74D39_H
#define UPDATENOTICE_T03AF68BDDDBD2D8575BA411C06D96FC886C74D39_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.Enums.UpdateNotice
struct  UpdateNotice_t03AF68BDDDBD2D8575BA411C06D96FC886C74D39 
{
public:
	// System.Int32 DG.Tweening.Core.Enums.UpdateNotice::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UpdateNotice_t03AF68BDDDBD2D8575BA411C06D96FC886C74D39, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATENOTICE_T03AF68BDDDBD2D8575BA411C06D96FC886C74D39_H
#ifndef CAPACITYINCREASEMODE_T36881005D472A2413565F67F5E73C94DED8A777D_H
#define CAPACITYINCREASEMODE_T36881005D472A2413565F67F5E73C94DED8A777D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.TweenManager_CapacityIncreaseMode
struct  CapacityIncreaseMode_t36881005D472A2413565F67F5E73C94DED8A777D 
{
public:
	// System.Int32 DG.Tweening.Core.TweenManager_CapacityIncreaseMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CapacityIncreaseMode_t36881005D472A2413565F67F5E73C94DED8A777D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAPACITYINCREASEMODE_T36881005D472A2413565F67F5E73C94DED8A777D_H
#ifndef EASE_TC8B7BB21FCD1947DD4A1DD8025D4E9C6CC01DA0E_H
#define EASE_TC8B7BB21FCD1947DD4A1DD8025D4E9C6CC01DA0E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Ease
struct  Ease_tC8B7BB21FCD1947DD4A1DD8025D4E9C6CC01DA0E 
{
public:
	// System.Int32 DG.Tweening.Ease::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Ease_tC8B7BB21FCD1947DD4A1DD8025D4E9C6CC01DA0E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EASE_TC8B7BB21FCD1947DD4A1DD8025D4E9C6CC01DA0E_H
#ifndef LOGBEHAVIOUR_TD1A4AACD65E9C63844373CF59B8E9A576814F813_H
#define LOGBEHAVIOUR_TD1A4AACD65E9C63844373CF59B8E9A576814F813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.LogBehaviour
struct  LogBehaviour_tD1A4AACD65E9C63844373CF59B8E9A576814F813 
{
public:
	// System.Int32 DG.Tweening.LogBehaviour::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LogBehaviour_tD1A4AACD65E9C63844373CF59B8E9A576814F813, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGBEHAVIOUR_TD1A4AACD65E9C63844373CF59B8E9A576814F813_H
#ifndef LOOPTYPE_T1F864F717700724AC772E79CCF167C3C44D6EE1A_H
#define LOOPTYPE_T1F864F717700724AC772E79CCF167C3C44D6EE1A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.LoopType
struct  LoopType_t1F864F717700724AC772E79CCF167C3C44D6EE1A 
{
public:
	// System.Int32 DG.Tweening.LoopType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LoopType_t1F864F717700724AC772E79CCF167C3C44D6EE1A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOOPTYPE_T1F864F717700724AC772E79CCF167C3C44D6EE1A_H
#ifndef UPDATETYPE_T0EBAB258B7D72D4959A3835E554B7958CAFDAF63_H
#define UPDATETYPE_T0EBAB258B7D72D4959A3835E554B7958CAFDAF63_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.UpdateType
struct  UpdateType_t0EBAB258B7D72D4959A3835E554B7958CAFDAF63 
{
public:
	// System.Int32 DG.Tweening.UpdateType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UpdateType_t0EBAB258B7D72D4959A3835E554B7958CAFDAF63, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATETYPE_T0EBAB258B7D72D4959A3835E554B7958CAFDAF63_H
#ifndef ENTITYINDEXTYPE_T46D4684C925867D0CCA486B402E57664F71E6F8E_H
#define ENTITYINDEXTYPE_T46D4684C925867D0CCA486B402E57664F71E6F8E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.CodeGeneration.Attributes.EntityIndexType
struct  EntityIndexType_t46D4684C925867D0CCA486B402E57664F71E6F8E 
{
public:
	// System.Int32 Entitas.CodeGeneration.Attributes.EntityIndexType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EntityIndexType_t46D4684C925867D0CCA486B402E57664F71E6F8E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTITYINDEXTYPE_T46D4684C925867D0CCA486B402E57664F71E6F8E_H
#ifndef EVENTTARGET_T116BD7DA9A939B54477A9F429106EC67EB49A6C2_H
#define EVENTTARGET_T116BD7DA9A939B54477A9F429106EC67EB49A6C2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.CodeGeneration.Attributes.EventTarget
struct  EventTarget_t116BD7DA9A939B54477A9F429106EC67EB49A6C2 
{
public:
	// System.Int32 Entitas.CodeGeneration.Attributes.EventTarget::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EventTarget_t116BD7DA9A939B54477A9F429106EC67EB49A6C2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTTARGET_T116BD7DA9A939B54477A9F429106EC67EB49A6C2_H
#ifndef EVENTTYPE_T40A4EAA6A23F7288106E89EDE2610C86746758A3_H
#define EVENTTYPE_T40A4EAA6A23F7288106E89EDE2610C86746758A3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.CodeGeneration.Attributes.EventType
struct  EventType_t40A4EAA6A23F7288106E89EDE2610C86746758A3 
{
public:
	// System.Int32 Entitas.CodeGeneration.Attributes.EventType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EventType_t40A4EAA6A23F7288106E89EDE2610C86746758A3, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTTYPE_T40A4EAA6A23F7288106E89EDE2610C86746758A3_H
#ifndef COLLECTOREXCEPTION_TC99C67E8041C8145EFA76944442A2E52DD916E84_H
#define COLLECTOREXCEPTION_TC99C67E8041C8145EFA76944442A2E52DD916E84_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.CollectorException
struct  CollectorException_tC99C67E8041C8145EFA76944442A2E52DD916E84  : public EntitasException_t71786D33342FC2A2F61B3AAC7FD34318ABC7CD33
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTOREXCEPTION_TC99C67E8041C8145EFA76944442A2E52DD916E84_H
#ifndef CONTEXTDOESNOTCONTAINENTITYEXCEPTION_T7080D5711853883454B78F851D81DFE846DE5B95_H
#define CONTEXTDOESNOTCONTAINENTITYEXCEPTION_T7080D5711853883454B78F851D81DFE846DE5B95_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.ContextDoesNotContainEntityException
struct  ContextDoesNotContainEntityException_t7080D5711853883454B78F851D81DFE846DE5B95  : public EntitasException_t71786D33342FC2A2F61B3AAC7FD34318ABC7CD33
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTEXTDOESNOTCONTAINENTITYEXCEPTION_T7080D5711853883454B78F851D81DFE846DE5B95_H
#ifndef CONTEXTENTITYINDEXDOESALREADYEXISTEXCEPTION_TCB4C94A55764B9852F030A20334B50BE8299E6BC_H
#define CONTEXTENTITYINDEXDOESALREADYEXISTEXCEPTION_TCB4C94A55764B9852F030A20334B50BE8299E6BC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.ContextEntityIndexDoesAlreadyExistException
struct  ContextEntityIndexDoesAlreadyExistException_tCB4C94A55764B9852F030A20334B50BE8299E6BC  : public EntitasException_t71786D33342FC2A2F61B3AAC7FD34318ABC7CD33
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTEXTENTITYINDEXDOESALREADYEXISTEXCEPTION_TCB4C94A55764B9852F030A20334B50BE8299E6BC_H
#ifndef CONTEXTENTITYINDEXDOESNOTEXISTEXCEPTION_T2191FE494E5293B1BA0242B7D67CCA9C05602672_H
#define CONTEXTENTITYINDEXDOESNOTEXISTEXCEPTION_T2191FE494E5293B1BA0242B7D67CCA9C05602672_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.ContextEntityIndexDoesNotExistException
struct  ContextEntityIndexDoesNotExistException_t2191FE494E5293B1BA0242B7D67CCA9C05602672  : public EntitasException_t71786D33342FC2A2F61B3AAC7FD34318ABC7CD33
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTEXTENTITYINDEXDOESNOTEXISTEXCEPTION_T2191FE494E5293B1BA0242B7D67CCA9C05602672_H
#ifndef CONTEXTINFOEXCEPTION_T5E142D0B2B6C87CF5CC9A70D38CF5D40FB1EA381_H
#define CONTEXTINFOEXCEPTION_T5E142D0B2B6C87CF5CC9A70D38CF5D40FB1EA381_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.ContextInfoException
struct  ContextInfoException_t5E142D0B2B6C87CF5CC9A70D38CF5D40FB1EA381  : public EntitasException_t71786D33342FC2A2F61B3AAC7FD34318ABC7CD33
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTEXTINFOEXCEPTION_T5E142D0B2B6C87CF5CC9A70D38CF5D40FB1EA381_H
#ifndef CONTEXTSTILLHASRETAINEDENTITIESEXCEPTION_TFDD6AAA582B14187B08C2B3A07787308DE518C17_H
#define CONTEXTSTILLHASRETAINEDENTITIESEXCEPTION_TFDD6AAA582B14187B08C2B3A07787308DE518C17_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.ContextStillHasRetainedEntitiesException
struct  ContextStillHasRetainedEntitiesException_tFDD6AAA582B14187B08C2B3A07787308DE518C17  : public EntitasException_t71786D33342FC2A2F61B3AAC7FD34318ABC7CD33
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTEXTSTILLHASRETAINEDENTITIESEXCEPTION_TFDD6AAA582B14187B08C2B3A07787308DE518C17_H
#ifndef ENTITYALREADYHASCOMPONENTEXCEPTION_T30825CB8B4D13727D4ABF3EBB518E8BC590D4BFB_H
#define ENTITYALREADYHASCOMPONENTEXCEPTION_T30825CB8B4D13727D4ABF3EBB518E8BC590D4BFB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.EntityAlreadyHasComponentException
struct  EntityAlreadyHasComponentException_t30825CB8B4D13727D4ABF3EBB518E8BC590D4BFB  : public EntitasException_t71786D33342FC2A2F61B3AAC7FD34318ABC7CD33
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTITYALREADYHASCOMPONENTEXCEPTION_T30825CB8B4D13727D4ABF3EBB518E8BC590D4BFB_H
#ifndef ENTITYDOESNOTHAVECOMPONENTEXCEPTION_TE224D88B637576CC426C2510B406781E58AE209C_H
#define ENTITYDOESNOTHAVECOMPONENTEXCEPTION_TE224D88B637576CC426C2510B406781E58AE209C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.EntityDoesNotHaveComponentException
struct  EntityDoesNotHaveComponentException_tE224D88B637576CC426C2510B406781E58AE209C  : public EntitasException_t71786D33342FC2A2F61B3AAC7FD34318ABC7CD33
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTITYDOESNOTHAVECOMPONENTEXCEPTION_TE224D88B637576CC426C2510B406781E58AE209C_H
#ifndef ENTITYINDEXEXCEPTION_T6834F599460536EA81AFA69CA928AA2022334EF4_H
#define ENTITYINDEXEXCEPTION_T6834F599460536EA81AFA69CA928AA2022334EF4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.EntityIndexException
struct  EntityIndexException_t6834F599460536EA81AFA69CA928AA2022334EF4  : public EntitasException_t71786D33342FC2A2F61B3AAC7FD34318ABC7CD33
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTITYINDEXEXCEPTION_T6834F599460536EA81AFA69CA928AA2022334EF4_H
#ifndef ENTITYISALREADYRETAINEDBYOWNEREXCEPTION_T48896BC8B21BDFFFD7F85D819233996F6E8D2783_H
#define ENTITYISALREADYRETAINEDBYOWNEREXCEPTION_T48896BC8B21BDFFFD7F85D819233996F6E8D2783_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.EntityIsAlreadyRetainedByOwnerException
struct  EntityIsAlreadyRetainedByOwnerException_t48896BC8B21BDFFFD7F85D819233996F6E8D2783  : public EntitasException_t71786D33342FC2A2F61B3AAC7FD34318ABC7CD33
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTITYISALREADYRETAINEDBYOWNEREXCEPTION_T48896BC8B21BDFFFD7F85D819233996F6E8D2783_H
#ifndef ENTITYISNOTDESTROYEDEXCEPTION_TE5F9909534769331523B7F21128EF075CE107364_H
#define ENTITYISNOTDESTROYEDEXCEPTION_TE5F9909534769331523B7F21128EF075CE107364_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.EntityIsNotDestroyedException
struct  EntityIsNotDestroyedException_tE5F9909534769331523B7F21128EF075CE107364  : public EntitasException_t71786D33342FC2A2F61B3AAC7FD34318ABC7CD33
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTITYISNOTDESTROYEDEXCEPTION_TE5F9909534769331523B7F21128EF075CE107364_H
#ifndef ENTITYISNOTENABLEDEXCEPTION_T7AE4AB9E6B2414C4063824D87F5C4674046B2C11_H
#define ENTITYISNOTENABLEDEXCEPTION_T7AE4AB9E6B2414C4063824D87F5C4674046B2C11_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.EntityIsNotEnabledException
struct  EntityIsNotEnabledException_t7AE4AB9E6B2414C4063824D87F5C4674046B2C11  : public EntitasException_t71786D33342FC2A2F61B3AAC7FD34318ABC7CD33
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTITYISNOTENABLEDEXCEPTION_T7AE4AB9E6B2414C4063824D87F5C4674046B2C11_H
#ifndef ENTITYISNOTRETAINEDBYOWNEREXCEPTION_T3553575E99A69F8AD64A5BAC2DC898E6954DC225_H
#define ENTITYISNOTRETAINEDBYOWNEREXCEPTION_T3553575E99A69F8AD64A5BAC2DC898E6954DC225_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.EntityIsNotRetainedByOwnerException
struct  EntityIsNotRetainedByOwnerException_t3553575E99A69F8AD64A5BAC2DC898E6954DC225  : public EntitasException_t71786D33342FC2A2F61B3AAC7FD34318ABC7CD33
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTITYISNOTRETAINEDBYOWNEREXCEPTION_T3553575E99A69F8AD64A5BAC2DC898E6954DC225_H
#ifndef GROUPEVENT_T5D8EAED9A0E5DF01072E622340ED850672F54891_H
#define GROUPEVENT_T5D8EAED9A0E5DF01072E622340ED850672F54891_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.GroupEvent
struct  GroupEvent_t5D8EAED9A0E5DF01072E622340ED850672F54891 
{
public:
	// System.Byte Entitas.GroupEvent::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GroupEvent_t5D8EAED9A0E5DF01072E622340ED850672F54891, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GROUPEVENT_T5D8EAED9A0E5DF01072E622340ED850672F54891_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef ABSTRACTENTITYINDEXATTRIBUTE_TC53F438F525D86D77BBFE5B0C9613E00E303F2EB_H
#define ABSTRACTENTITYINDEXATTRIBUTE_TC53F438F525D86D77BBFE5B0C9613E00E303F2EB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.CodeGeneration.Attributes.AbstractEntityIndexAttribute
struct  AbstractEntityIndexAttribute_tC53F438F525D86D77BBFE5B0C9613E00E303F2EB  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// Entitas.CodeGeneration.Attributes.EntityIndexType Entitas.CodeGeneration.Attributes.AbstractEntityIndexAttribute::entityIndexType
	int32_t ___entityIndexType_0;

public:
	inline static int32_t get_offset_of_entityIndexType_0() { return static_cast<int32_t>(offsetof(AbstractEntityIndexAttribute_tC53F438F525D86D77BBFE5B0C9613E00E303F2EB, ___entityIndexType_0)); }
	inline int32_t get_entityIndexType_0() const { return ___entityIndexType_0; }
	inline int32_t* get_address_of_entityIndexType_0() { return &___entityIndexType_0; }
	inline void set_entityIndexType_0(int32_t value)
	{
		___entityIndexType_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTENTITYINDEXATTRIBUTE_TC53F438F525D86D77BBFE5B0C9613E00E303F2EB_H
#ifndef EVENTATTRIBUTE_T34E326AC98C5C26372A605464C75C3E6F7864C29_H
#define EVENTATTRIBUTE_T34E326AC98C5C26372A605464C75C3E6F7864C29_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.CodeGeneration.Attributes.EventAttribute
struct  EventAttribute_t34E326AC98C5C26372A605464C75C3E6F7864C29  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// Entitas.CodeGeneration.Attributes.EventTarget Entitas.CodeGeneration.Attributes.EventAttribute::eventTarget
	int32_t ___eventTarget_0;
	// Entitas.CodeGeneration.Attributes.EventType Entitas.CodeGeneration.Attributes.EventAttribute::eventType
	int32_t ___eventType_1;
	// System.Int32 Entitas.CodeGeneration.Attributes.EventAttribute::priority
	int32_t ___priority_2;

public:
	inline static int32_t get_offset_of_eventTarget_0() { return static_cast<int32_t>(offsetof(EventAttribute_t34E326AC98C5C26372A605464C75C3E6F7864C29, ___eventTarget_0)); }
	inline int32_t get_eventTarget_0() const { return ___eventTarget_0; }
	inline int32_t* get_address_of_eventTarget_0() { return &___eventTarget_0; }
	inline void set_eventTarget_0(int32_t value)
	{
		___eventTarget_0 = value;
	}

	inline static int32_t get_offset_of_eventType_1() { return static_cast<int32_t>(offsetof(EventAttribute_t34E326AC98C5C26372A605464C75C3E6F7864C29, ___eventType_1)); }
	inline int32_t get_eventType_1() const { return ___eventType_1; }
	inline int32_t* get_address_of_eventType_1() { return &___eventType_1; }
	inline void set_eventType_1(int32_t value)
	{
		___eventType_1 = value;
	}

	inline static int32_t get_offset_of_priority_2() { return static_cast<int32_t>(offsetof(EventAttribute_t34E326AC98C5C26372A605464C75C3E6F7864C29, ___priority_2)); }
	inline int32_t get_priority_2() const { return ___priority_2; }
	inline int32_t* get_address_of_priority_2() { return &___priority_2; }
	inline void set_priority_2(int32_t value)
	{
		___priority_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTATTRIBUTE_T34E326AC98C5C26372A605464C75C3E6F7864C29_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#define SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_pinvoke : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_com : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifndef DOTWEENSETTINGS_TB3147E333BEA0ECA98C236973B61A17B8D3A33A1_H
#define DOTWEENSETTINGS_TB3147E333BEA0ECA98C236973B61A17B8D3A33A1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.DOTweenSettings
struct  DOTweenSettings_tB3147E333BEA0ECA98C236973B61A17B8D3A33A1  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// System.Boolean DG.Tweening.Core.DOTweenSettings::useSafeMode
	bool ___useSafeMode_5;
	// System.Single DG.Tweening.Core.DOTweenSettings::timeScale
	float ___timeScale_6;
	// System.Boolean DG.Tweening.Core.DOTweenSettings::useSmoothDeltaTime
	bool ___useSmoothDeltaTime_7;
	// System.Single DG.Tweening.Core.DOTweenSettings::maxSmoothUnscaledTime
	float ___maxSmoothUnscaledTime_8;
	// System.Boolean DG.Tweening.Core.DOTweenSettings::showUnityEditorReport
	bool ___showUnityEditorReport_9;
	// DG.Tweening.LogBehaviour DG.Tweening.Core.DOTweenSettings::logBehaviour
	int32_t ___logBehaviour_10;
	// System.Boolean DG.Tweening.Core.DOTweenSettings::drawGizmos
	bool ___drawGizmos_11;
	// System.Boolean DG.Tweening.Core.DOTweenSettings::defaultRecyclable
	bool ___defaultRecyclable_12;
	// DG.Tweening.AutoPlay DG.Tweening.Core.DOTweenSettings::defaultAutoPlay
	int32_t ___defaultAutoPlay_13;
	// DG.Tweening.UpdateType DG.Tweening.Core.DOTweenSettings::defaultUpdateType
	int32_t ___defaultUpdateType_14;
	// System.Boolean DG.Tweening.Core.DOTweenSettings::defaultTimeScaleIndependent
	bool ___defaultTimeScaleIndependent_15;
	// DG.Tweening.Ease DG.Tweening.Core.DOTweenSettings::defaultEaseType
	int32_t ___defaultEaseType_16;
	// System.Single DG.Tweening.Core.DOTweenSettings::defaultEaseOvershootOrAmplitude
	float ___defaultEaseOvershootOrAmplitude_17;
	// System.Single DG.Tweening.Core.DOTweenSettings::defaultEasePeriod
	float ___defaultEasePeriod_18;
	// System.Boolean DG.Tweening.Core.DOTweenSettings::defaultAutoKill
	bool ___defaultAutoKill_19;
	// DG.Tweening.LoopType DG.Tweening.Core.DOTweenSettings::defaultLoopType
	int32_t ___defaultLoopType_20;
	// DG.Tweening.Core.DOTweenSettings_SettingsLocation DG.Tweening.Core.DOTweenSettings::storeSettingsLocation
	int32_t ___storeSettingsLocation_21;

public:
	inline static int32_t get_offset_of_useSafeMode_5() { return static_cast<int32_t>(offsetof(DOTweenSettings_tB3147E333BEA0ECA98C236973B61A17B8D3A33A1, ___useSafeMode_5)); }
	inline bool get_useSafeMode_5() const { return ___useSafeMode_5; }
	inline bool* get_address_of_useSafeMode_5() { return &___useSafeMode_5; }
	inline void set_useSafeMode_5(bool value)
	{
		___useSafeMode_5 = value;
	}

	inline static int32_t get_offset_of_timeScale_6() { return static_cast<int32_t>(offsetof(DOTweenSettings_tB3147E333BEA0ECA98C236973B61A17B8D3A33A1, ___timeScale_6)); }
	inline float get_timeScale_6() const { return ___timeScale_6; }
	inline float* get_address_of_timeScale_6() { return &___timeScale_6; }
	inline void set_timeScale_6(float value)
	{
		___timeScale_6 = value;
	}

	inline static int32_t get_offset_of_useSmoothDeltaTime_7() { return static_cast<int32_t>(offsetof(DOTweenSettings_tB3147E333BEA0ECA98C236973B61A17B8D3A33A1, ___useSmoothDeltaTime_7)); }
	inline bool get_useSmoothDeltaTime_7() const { return ___useSmoothDeltaTime_7; }
	inline bool* get_address_of_useSmoothDeltaTime_7() { return &___useSmoothDeltaTime_7; }
	inline void set_useSmoothDeltaTime_7(bool value)
	{
		___useSmoothDeltaTime_7 = value;
	}

	inline static int32_t get_offset_of_maxSmoothUnscaledTime_8() { return static_cast<int32_t>(offsetof(DOTweenSettings_tB3147E333BEA0ECA98C236973B61A17B8D3A33A1, ___maxSmoothUnscaledTime_8)); }
	inline float get_maxSmoothUnscaledTime_8() const { return ___maxSmoothUnscaledTime_8; }
	inline float* get_address_of_maxSmoothUnscaledTime_8() { return &___maxSmoothUnscaledTime_8; }
	inline void set_maxSmoothUnscaledTime_8(float value)
	{
		___maxSmoothUnscaledTime_8 = value;
	}

	inline static int32_t get_offset_of_showUnityEditorReport_9() { return static_cast<int32_t>(offsetof(DOTweenSettings_tB3147E333BEA0ECA98C236973B61A17B8D3A33A1, ___showUnityEditorReport_9)); }
	inline bool get_showUnityEditorReport_9() const { return ___showUnityEditorReport_9; }
	inline bool* get_address_of_showUnityEditorReport_9() { return &___showUnityEditorReport_9; }
	inline void set_showUnityEditorReport_9(bool value)
	{
		___showUnityEditorReport_9 = value;
	}

	inline static int32_t get_offset_of_logBehaviour_10() { return static_cast<int32_t>(offsetof(DOTweenSettings_tB3147E333BEA0ECA98C236973B61A17B8D3A33A1, ___logBehaviour_10)); }
	inline int32_t get_logBehaviour_10() const { return ___logBehaviour_10; }
	inline int32_t* get_address_of_logBehaviour_10() { return &___logBehaviour_10; }
	inline void set_logBehaviour_10(int32_t value)
	{
		___logBehaviour_10 = value;
	}

	inline static int32_t get_offset_of_drawGizmos_11() { return static_cast<int32_t>(offsetof(DOTweenSettings_tB3147E333BEA0ECA98C236973B61A17B8D3A33A1, ___drawGizmos_11)); }
	inline bool get_drawGizmos_11() const { return ___drawGizmos_11; }
	inline bool* get_address_of_drawGizmos_11() { return &___drawGizmos_11; }
	inline void set_drawGizmos_11(bool value)
	{
		___drawGizmos_11 = value;
	}

	inline static int32_t get_offset_of_defaultRecyclable_12() { return static_cast<int32_t>(offsetof(DOTweenSettings_tB3147E333BEA0ECA98C236973B61A17B8D3A33A1, ___defaultRecyclable_12)); }
	inline bool get_defaultRecyclable_12() const { return ___defaultRecyclable_12; }
	inline bool* get_address_of_defaultRecyclable_12() { return &___defaultRecyclable_12; }
	inline void set_defaultRecyclable_12(bool value)
	{
		___defaultRecyclable_12 = value;
	}

	inline static int32_t get_offset_of_defaultAutoPlay_13() { return static_cast<int32_t>(offsetof(DOTweenSettings_tB3147E333BEA0ECA98C236973B61A17B8D3A33A1, ___defaultAutoPlay_13)); }
	inline int32_t get_defaultAutoPlay_13() const { return ___defaultAutoPlay_13; }
	inline int32_t* get_address_of_defaultAutoPlay_13() { return &___defaultAutoPlay_13; }
	inline void set_defaultAutoPlay_13(int32_t value)
	{
		___defaultAutoPlay_13 = value;
	}

	inline static int32_t get_offset_of_defaultUpdateType_14() { return static_cast<int32_t>(offsetof(DOTweenSettings_tB3147E333BEA0ECA98C236973B61A17B8D3A33A1, ___defaultUpdateType_14)); }
	inline int32_t get_defaultUpdateType_14() const { return ___defaultUpdateType_14; }
	inline int32_t* get_address_of_defaultUpdateType_14() { return &___defaultUpdateType_14; }
	inline void set_defaultUpdateType_14(int32_t value)
	{
		___defaultUpdateType_14 = value;
	}

	inline static int32_t get_offset_of_defaultTimeScaleIndependent_15() { return static_cast<int32_t>(offsetof(DOTweenSettings_tB3147E333BEA0ECA98C236973B61A17B8D3A33A1, ___defaultTimeScaleIndependent_15)); }
	inline bool get_defaultTimeScaleIndependent_15() const { return ___defaultTimeScaleIndependent_15; }
	inline bool* get_address_of_defaultTimeScaleIndependent_15() { return &___defaultTimeScaleIndependent_15; }
	inline void set_defaultTimeScaleIndependent_15(bool value)
	{
		___defaultTimeScaleIndependent_15 = value;
	}

	inline static int32_t get_offset_of_defaultEaseType_16() { return static_cast<int32_t>(offsetof(DOTweenSettings_tB3147E333BEA0ECA98C236973B61A17B8D3A33A1, ___defaultEaseType_16)); }
	inline int32_t get_defaultEaseType_16() const { return ___defaultEaseType_16; }
	inline int32_t* get_address_of_defaultEaseType_16() { return &___defaultEaseType_16; }
	inline void set_defaultEaseType_16(int32_t value)
	{
		___defaultEaseType_16 = value;
	}

	inline static int32_t get_offset_of_defaultEaseOvershootOrAmplitude_17() { return static_cast<int32_t>(offsetof(DOTweenSettings_tB3147E333BEA0ECA98C236973B61A17B8D3A33A1, ___defaultEaseOvershootOrAmplitude_17)); }
	inline float get_defaultEaseOvershootOrAmplitude_17() const { return ___defaultEaseOvershootOrAmplitude_17; }
	inline float* get_address_of_defaultEaseOvershootOrAmplitude_17() { return &___defaultEaseOvershootOrAmplitude_17; }
	inline void set_defaultEaseOvershootOrAmplitude_17(float value)
	{
		___defaultEaseOvershootOrAmplitude_17 = value;
	}

	inline static int32_t get_offset_of_defaultEasePeriod_18() { return static_cast<int32_t>(offsetof(DOTweenSettings_tB3147E333BEA0ECA98C236973B61A17B8D3A33A1, ___defaultEasePeriod_18)); }
	inline float get_defaultEasePeriod_18() const { return ___defaultEasePeriod_18; }
	inline float* get_address_of_defaultEasePeriod_18() { return &___defaultEasePeriod_18; }
	inline void set_defaultEasePeriod_18(float value)
	{
		___defaultEasePeriod_18 = value;
	}

	inline static int32_t get_offset_of_defaultAutoKill_19() { return static_cast<int32_t>(offsetof(DOTweenSettings_tB3147E333BEA0ECA98C236973B61A17B8D3A33A1, ___defaultAutoKill_19)); }
	inline bool get_defaultAutoKill_19() const { return ___defaultAutoKill_19; }
	inline bool* get_address_of_defaultAutoKill_19() { return &___defaultAutoKill_19; }
	inline void set_defaultAutoKill_19(bool value)
	{
		___defaultAutoKill_19 = value;
	}

	inline static int32_t get_offset_of_defaultLoopType_20() { return static_cast<int32_t>(offsetof(DOTweenSettings_tB3147E333BEA0ECA98C236973B61A17B8D3A33A1, ___defaultLoopType_20)); }
	inline int32_t get_defaultLoopType_20() const { return ___defaultLoopType_20; }
	inline int32_t* get_address_of_defaultLoopType_20() { return &___defaultLoopType_20; }
	inline void set_defaultLoopType_20(int32_t value)
	{
		___defaultLoopType_20 = value;
	}

	inline static int32_t get_offset_of_storeSettingsLocation_21() { return static_cast<int32_t>(offsetof(DOTweenSettings_tB3147E333BEA0ECA98C236973B61A17B8D3A33A1, ___storeSettingsLocation_21)); }
	inline int32_t get_storeSettingsLocation_21() const { return ___storeSettingsLocation_21; }
	inline int32_t* get_address_of_storeSettingsLocation_21() { return &___storeSettingsLocation_21; }
	inline void set_storeSettingsLocation_21(int32_t value)
	{
		___storeSettingsLocation_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOTWEENSETTINGS_TB3147E333BEA0ECA98C236973B61A17B8D3A33A1_H
#ifndef ENTITYINDEXATTRIBUTE_T157B398E0AB74C4E344254DD6D1174D59286C75E_H
#define ENTITYINDEXATTRIBUTE_T157B398E0AB74C4E344254DD6D1174D59286C75E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.CodeGeneration.Attributes.EntityIndexAttribute
struct  EntityIndexAttribute_t157B398E0AB74C4E344254DD6D1174D59286C75E  : public AbstractEntityIndexAttribute_tC53F438F525D86D77BBFE5B0C9613E00E303F2EB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTITYINDEXATTRIBUTE_T157B398E0AB74C4E344254DD6D1174D59286C75E_H
#ifndef PRIMARYENTITYINDEXATTRIBUTE_T650C506BD36D143AF4AC97DEAE82BFFB4933A12C_H
#define PRIMARYENTITYINDEXATTRIBUTE_T650C506BD36D143AF4AC97DEAE82BFFB4933A12C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.CodeGeneration.Attributes.PrimaryEntityIndexAttribute
struct  PrimaryEntityIndexAttribute_t650C506BD36D143AF4AC97DEAE82BFFB4933A12C  : public AbstractEntityIndexAttribute_tC53F438F525D86D77BBFE5B0C9613E00E303F2EB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRIMARYENTITYINDEXATTRIBUTE_T650C506BD36D143AF4AC97DEAE82BFFB4933A12C_H
#ifndef CONTEXTENTITYCHANGED_T1C8C691CEE56C008F944E90383D7D7400E111EC1_H
#define CONTEXTENTITYCHANGED_T1C8C691CEE56C008F944E90383D7D7400E111EC1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.ContextEntityChanged
struct  ContextEntityChanged_t1C8C691CEE56C008F944E90383D7D7400E111EC1  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTEXTENTITYCHANGED_T1C8C691CEE56C008F944E90383D7D7400E111EC1_H
#ifndef CONTEXTGROUPCHANGED_TDF404FDADBE283C90826E021920179C9C5986728_H
#define CONTEXTGROUPCHANGED_TDF404FDADBE283C90826E021920179C9C5986728_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.ContextGroupChanged
struct  ContextGroupChanged_tDF404FDADBE283C90826E021920179C9C5986728  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTEXTGROUPCHANGED_TDF404FDADBE283C90826E021920179C9C5986728_H
#ifndef ENTITYCOMPONENTCHANGED_T326B845F3277940DACBBC8836618A83196065E1A_H
#define ENTITYCOMPONENTCHANGED_T326B845F3277940DACBBC8836618A83196065E1A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.EntityComponentChanged
struct  EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTITYCOMPONENTCHANGED_T326B845F3277940DACBBC8836618A83196065E1A_H
#ifndef ENTITYCOMPONENTREPLACED_TECE10545D16DE6AECA3B03B47FA2582FE7A11B57_H
#define ENTITYCOMPONENTREPLACED_TECE10545D16DE6AECA3B03B47FA2582FE7A11B57_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.EntityComponentReplaced
struct  EntityComponentReplaced_tECE10545D16DE6AECA3B03B47FA2582FE7A11B57  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTITYCOMPONENTREPLACED_TECE10545D16DE6AECA3B03B47FA2582FE7A11B57_H
#ifndef ENTITYEVENT_TE7578C70965872DD79E91A748EBBCD674AEAE1D6_H
#define ENTITYEVENT_TE7578C70965872DD79E91A748EBBCD674AEAE1D6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.EntityEvent
struct  EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTITYEVENT_TE7578C70965872DD79E91A748EBBCD674AEAE1D6_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef DOTWEENCOMPONENT_T48118927332C9BFAE792209EEE42AF786370B05F_H
#define DOTWEENCOMPONENT_T48118927332C9BFAE792209EEE42AF786370B05F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.DOTweenComponent
struct  DOTweenComponent_t48118927332C9BFAE792209EEE42AF786370B05F  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 DG.Tweening.Core.DOTweenComponent::inspectorUpdater
	int32_t ___inspectorUpdater_4;
	// System.Single DG.Tweening.Core.DOTweenComponent::_unscaledTime
	float ____unscaledTime_5;
	// System.Single DG.Tweening.Core.DOTweenComponent::_unscaledDeltaTime
	float ____unscaledDeltaTime_6;
	// System.Boolean DG.Tweening.Core.DOTweenComponent::_duplicateToDestroy
	bool ____duplicateToDestroy_7;

public:
	inline static int32_t get_offset_of_inspectorUpdater_4() { return static_cast<int32_t>(offsetof(DOTweenComponent_t48118927332C9BFAE792209EEE42AF786370B05F, ___inspectorUpdater_4)); }
	inline int32_t get_inspectorUpdater_4() const { return ___inspectorUpdater_4; }
	inline int32_t* get_address_of_inspectorUpdater_4() { return &___inspectorUpdater_4; }
	inline void set_inspectorUpdater_4(int32_t value)
	{
		___inspectorUpdater_4 = value;
	}

	inline static int32_t get_offset_of__unscaledTime_5() { return static_cast<int32_t>(offsetof(DOTweenComponent_t48118927332C9BFAE792209EEE42AF786370B05F, ____unscaledTime_5)); }
	inline float get__unscaledTime_5() const { return ____unscaledTime_5; }
	inline float* get_address_of__unscaledTime_5() { return &____unscaledTime_5; }
	inline void set__unscaledTime_5(float value)
	{
		____unscaledTime_5 = value;
	}

	inline static int32_t get_offset_of__unscaledDeltaTime_6() { return static_cast<int32_t>(offsetof(DOTweenComponent_t48118927332C9BFAE792209EEE42AF786370B05F, ____unscaledDeltaTime_6)); }
	inline float get__unscaledDeltaTime_6() const { return ____unscaledDeltaTime_6; }
	inline float* get_address_of__unscaledDeltaTime_6() { return &____unscaledDeltaTime_6; }
	inline void set__unscaledDeltaTime_6(float value)
	{
		____unscaledDeltaTime_6 = value;
	}

	inline static int32_t get_offset_of__duplicateToDestroy_7() { return static_cast<int32_t>(offsetof(DOTweenComponent_t48118927332C9BFAE792209EEE42AF786370B05F, ____duplicateToDestroy_7)); }
	inline bool get__duplicateToDestroy_7() const { return ____duplicateToDestroy_7; }
	inline bool* get_address_of__duplicateToDestroy_7() { return &____duplicateToDestroy_7; }
	inline void set__duplicateToDestroy_7(bool value)
	{
		____duplicateToDestroy_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOTWEENCOMPONENT_T48118927332C9BFAE792209EEE42AF786370B05F_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6600 = { sizeof (Debugger_tD9D47C252FB20009C8276590D54394E430619D16), -1, sizeof(Debugger_tD9D47C252FB20009C8276590D54394E430619D16_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6600[1] = 
{
	Debugger_tD9D47C252FB20009C8276590D54394E430619D16_StaticFields::get_offset_of_logPriority_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6601 = { sizeof (DOTweenComponent_t48118927332C9BFAE792209EEE42AF786370B05F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6601[4] = 
{
	DOTweenComponent_t48118927332C9BFAE792209EEE42AF786370B05F::get_offset_of_inspectorUpdater_4(),
	DOTweenComponent_t48118927332C9BFAE792209EEE42AF786370B05F::get_offset_of__unscaledTime_5(),
	DOTweenComponent_t48118927332C9BFAE792209EEE42AF786370B05F::get_offset_of__unscaledDeltaTime_6(),
	DOTweenComponent_t48118927332C9BFAE792209EEE42AF786370B05F::get_offset_of__duplicateToDestroy_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6602 = { sizeof (U3CWaitForCompletionU3Ed__14_tABA622D9721D6AE328B313499A5377995ABF8780), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6602[3] = 
{
	U3CWaitForCompletionU3Ed__14_tABA622D9721D6AE328B313499A5377995ABF8780::get_offset_of_U3CU3E1__state_0(),
	U3CWaitForCompletionU3Ed__14_tABA622D9721D6AE328B313499A5377995ABF8780::get_offset_of_U3CU3E2__current_1(),
	U3CWaitForCompletionU3Ed__14_tABA622D9721D6AE328B313499A5377995ABF8780::get_offset_of_t_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6603 = { sizeof (U3CWaitForRewindU3Ed__15_tB0B3B4DBE9292613741BC793EBE22C9465E31E91), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6603[3] = 
{
	U3CWaitForRewindU3Ed__15_tB0B3B4DBE9292613741BC793EBE22C9465E31E91::get_offset_of_U3CU3E1__state_0(),
	U3CWaitForRewindU3Ed__15_tB0B3B4DBE9292613741BC793EBE22C9465E31E91::get_offset_of_U3CU3E2__current_1(),
	U3CWaitForRewindU3Ed__15_tB0B3B4DBE9292613741BC793EBE22C9465E31E91::get_offset_of_t_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6604 = { sizeof (U3CWaitForKillU3Ed__16_tDEA92D4CB64EDF28F23753CDC00A8497D2EA8E29), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6604[3] = 
{
	U3CWaitForKillU3Ed__16_tDEA92D4CB64EDF28F23753CDC00A8497D2EA8E29::get_offset_of_U3CU3E1__state_0(),
	U3CWaitForKillU3Ed__16_tDEA92D4CB64EDF28F23753CDC00A8497D2EA8E29::get_offset_of_U3CU3E2__current_1(),
	U3CWaitForKillU3Ed__16_tDEA92D4CB64EDF28F23753CDC00A8497D2EA8E29::get_offset_of_t_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6605 = { sizeof (U3CWaitForElapsedLoopsU3Ed__17_tC386E18F77FE29C85AC9E63C8AAEB94002585A93), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6605[4] = 
{
	U3CWaitForElapsedLoopsU3Ed__17_tC386E18F77FE29C85AC9E63C8AAEB94002585A93::get_offset_of_U3CU3E1__state_0(),
	U3CWaitForElapsedLoopsU3Ed__17_tC386E18F77FE29C85AC9E63C8AAEB94002585A93::get_offset_of_U3CU3E2__current_1(),
	U3CWaitForElapsedLoopsU3Ed__17_tC386E18F77FE29C85AC9E63C8AAEB94002585A93::get_offset_of_t_2(),
	U3CWaitForElapsedLoopsU3Ed__17_tC386E18F77FE29C85AC9E63C8AAEB94002585A93::get_offset_of_elapsedLoops_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6606 = { sizeof (U3CWaitForPositionU3Ed__18_tF105A8769B617DC60C65C66A388F207B66047748), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6606[4] = 
{
	U3CWaitForPositionU3Ed__18_tF105A8769B617DC60C65C66A388F207B66047748::get_offset_of_U3CU3E1__state_0(),
	U3CWaitForPositionU3Ed__18_tF105A8769B617DC60C65C66A388F207B66047748::get_offset_of_U3CU3E2__current_1(),
	U3CWaitForPositionU3Ed__18_tF105A8769B617DC60C65C66A388F207B66047748::get_offset_of_t_2(),
	U3CWaitForPositionU3Ed__18_tF105A8769B617DC60C65C66A388F207B66047748::get_offset_of_position_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6607 = { sizeof (U3CWaitForStartU3Ed__19_t56B7F72F2BA96960DE945427A547739C42D33FCA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6607[3] = 
{
	U3CWaitForStartU3Ed__19_t56B7F72F2BA96960DE945427A547739C42D33FCA::get_offset_of_U3CU3E1__state_0(),
	U3CWaitForStartU3Ed__19_t56B7F72F2BA96960DE945427A547739C42D33FCA::get_offset_of_U3CU3E2__current_1(),
	U3CWaitForStartU3Ed__19_t56B7F72F2BA96960DE945427A547739C42D33FCA::get_offset_of_t_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6608 = { sizeof (DOTweenSettings_tB3147E333BEA0ECA98C236973B61A17B8D3A33A1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6608[18] = 
{
	0,
	DOTweenSettings_tB3147E333BEA0ECA98C236973B61A17B8D3A33A1::get_offset_of_useSafeMode_5(),
	DOTweenSettings_tB3147E333BEA0ECA98C236973B61A17B8D3A33A1::get_offset_of_timeScale_6(),
	DOTweenSettings_tB3147E333BEA0ECA98C236973B61A17B8D3A33A1::get_offset_of_useSmoothDeltaTime_7(),
	DOTweenSettings_tB3147E333BEA0ECA98C236973B61A17B8D3A33A1::get_offset_of_maxSmoothUnscaledTime_8(),
	DOTweenSettings_tB3147E333BEA0ECA98C236973B61A17B8D3A33A1::get_offset_of_showUnityEditorReport_9(),
	DOTweenSettings_tB3147E333BEA0ECA98C236973B61A17B8D3A33A1::get_offset_of_logBehaviour_10(),
	DOTweenSettings_tB3147E333BEA0ECA98C236973B61A17B8D3A33A1::get_offset_of_drawGizmos_11(),
	DOTweenSettings_tB3147E333BEA0ECA98C236973B61A17B8D3A33A1::get_offset_of_defaultRecyclable_12(),
	DOTweenSettings_tB3147E333BEA0ECA98C236973B61A17B8D3A33A1::get_offset_of_defaultAutoPlay_13(),
	DOTweenSettings_tB3147E333BEA0ECA98C236973B61A17B8D3A33A1::get_offset_of_defaultUpdateType_14(),
	DOTweenSettings_tB3147E333BEA0ECA98C236973B61A17B8D3A33A1::get_offset_of_defaultTimeScaleIndependent_15(),
	DOTweenSettings_tB3147E333BEA0ECA98C236973B61A17B8D3A33A1::get_offset_of_defaultEaseType_16(),
	DOTweenSettings_tB3147E333BEA0ECA98C236973B61A17B8D3A33A1::get_offset_of_defaultEaseOvershootOrAmplitude_17(),
	DOTweenSettings_tB3147E333BEA0ECA98C236973B61A17B8D3A33A1::get_offset_of_defaultEasePeriod_18(),
	DOTweenSettings_tB3147E333BEA0ECA98C236973B61A17B8D3A33A1::get_offset_of_defaultAutoKill_19(),
	DOTweenSettings_tB3147E333BEA0ECA98C236973B61A17B8D3A33A1::get_offset_of_defaultLoopType_20(),
	DOTweenSettings_tB3147E333BEA0ECA98C236973B61A17B8D3A33A1::get_offset_of_storeSettingsLocation_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6609 = { sizeof (SettingsLocation_t6D5FD003DE9DA2EB1AC2EBD0B8CA88E9176ED452)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6609[4] = 
{
	SettingsLocation_t6D5FD003DE9DA2EB1AC2EBD0B8CA88E9176ED452::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6610 = { sizeof (Extensions_t0EE019A0F679EAB3ACB55E1C9F31F7A1FAE8B475), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6611 = { sizeof (TweenManager_t60E1FACD2C008A79361FCD1037D92408C1DAF386), -1, sizeof(TweenManager_t60E1FACD2C008A79361FCD1037D92408C1DAF386_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6611[30] = 
{
	TweenManager_t60E1FACD2C008A79361FCD1037D92408C1DAF386_StaticFields::get_offset_of_maxActive_0(),
	TweenManager_t60E1FACD2C008A79361FCD1037D92408C1DAF386_StaticFields::get_offset_of_maxTweeners_1(),
	TweenManager_t60E1FACD2C008A79361FCD1037D92408C1DAF386_StaticFields::get_offset_of_maxSequences_2(),
	TweenManager_t60E1FACD2C008A79361FCD1037D92408C1DAF386_StaticFields::get_offset_of_hasActiveTweens_3(),
	TweenManager_t60E1FACD2C008A79361FCD1037D92408C1DAF386_StaticFields::get_offset_of_hasActiveDefaultTweens_4(),
	TweenManager_t60E1FACD2C008A79361FCD1037D92408C1DAF386_StaticFields::get_offset_of_hasActiveLateTweens_5(),
	TweenManager_t60E1FACD2C008A79361FCD1037D92408C1DAF386_StaticFields::get_offset_of_hasActiveFixedTweens_6(),
	TweenManager_t60E1FACD2C008A79361FCD1037D92408C1DAF386_StaticFields::get_offset_of_hasActiveManualTweens_7(),
	TweenManager_t60E1FACD2C008A79361FCD1037D92408C1DAF386_StaticFields::get_offset_of_totActiveTweens_8(),
	TweenManager_t60E1FACD2C008A79361FCD1037D92408C1DAF386_StaticFields::get_offset_of_totActiveDefaultTweens_9(),
	TweenManager_t60E1FACD2C008A79361FCD1037D92408C1DAF386_StaticFields::get_offset_of_totActiveLateTweens_10(),
	TweenManager_t60E1FACD2C008A79361FCD1037D92408C1DAF386_StaticFields::get_offset_of_totActiveFixedTweens_11(),
	TweenManager_t60E1FACD2C008A79361FCD1037D92408C1DAF386_StaticFields::get_offset_of_totActiveManualTweens_12(),
	TweenManager_t60E1FACD2C008A79361FCD1037D92408C1DAF386_StaticFields::get_offset_of_totActiveTweeners_13(),
	TweenManager_t60E1FACD2C008A79361FCD1037D92408C1DAF386_StaticFields::get_offset_of_totActiveSequences_14(),
	TweenManager_t60E1FACD2C008A79361FCD1037D92408C1DAF386_StaticFields::get_offset_of_totPooledTweeners_15(),
	TweenManager_t60E1FACD2C008A79361FCD1037D92408C1DAF386_StaticFields::get_offset_of_totPooledSequences_16(),
	TweenManager_t60E1FACD2C008A79361FCD1037D92408C1DAF386_StaticFields::get_offset_of_totTweeners_17(),
	TweenManager_t60E1FACD2C008A79361FCD1037D92408C1DAF386_StaticFields::get_offset_of_totSequences_18(),
	TweenManager_t60E1FACD2C008A79361FCD1037D92408C1DAF386_StaticFields::get_offset_of_isUpdateLoop_19(),
	TweenManager_t60E1FACD2C008A79361FCD1037D92408C1DAF386_StaticFields::get_offset_of__activeTweens_20(),
	TweenManager_t60E1FACD2C008A79361FCD1037D92408C1DAF386_StaticFields::get_offset_of__pooledTweeners_21(),
	TweenManager_t60E1FACD2C008A79361FCD1037D92408C1DAF386_StaticFields::get_offset_of__PooledSequences_22(),
	TweenManager_t60E1FACD2C008A79361FCD1037D92408C1DAF386_StaticFields::get_offset_of__KillList_23(),
	TweenManager_t60E1FACD2C008A79361FCD1037D92408C1DAF386_StaticFields::get_offset_of__maxActiveLookupId_24(),
	TweenManager_t60E1FACD2C008A79361FCD1037D92408C1DAF386_StaticFields::get_offset_of__requiresActiveReorganization_25(),
	TweenManager_t60E1FACD2C008A79361FCD1037D92408C1DAF386_StaticFields::get_offset_of__reorganizeFromId_26(),
	TweenManager_t60E1FACD2C008A79361FCD1037D92408C1DAF386_StaticFields::get_offset_of__minPooledTweenerId_27(),
	TweenManager_t60E1FACD2C008A79361FCD1037D92408C1DAF386_StaticFields::get_offset_of__maxPooledTweenerId_28(),
	TweenManager_t60E1FACD2C008A79361FCD1037D92408C1DAF386_StaticFields::get_offset_of__despawnAllCalledFromUpdateLoopCallback_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6612 = { sizeof (CapacityIncreaseMode_t36881005D472A2413565F67F5E73C94DED8A777D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6612[4] = 
{
	CapacityIncreaseMode_t36881005D472A2413565F67F5E73C94DED8A777D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6613 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6613[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6614 = { sizeof (SpecialStartupMode_tE272D2FEC46F4A5FC68E9A99E0093494AA4E0E49)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6614[6] = 
{
	SpecialStartupMode_tE272D2FEC46F4A5FC68E9A99E0093494AA4E0E49::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6615 = { sizeof (UpdateNotice_t03AF68BDDDBD2D8575BA411C06D96FC886C74D39)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6615[3] = 
{
	UpdateNotice_t03AF68BDDDBD2D8575BA411C06D96FC886C74D39::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6616 = { sizeof (UpdateMode_tF271804A5FF87FE0CBFBD3FF6011D9499D5C6662)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6616[4] = 
{
	UpdateMode_tF271804A5FF87FE0CBFBD3FF6011D9499D5C6662::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6617 = { sizeof (Bounce_tF73282443E3B5769C935FFD5431CB1845F59AE83), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6618 = { sizeof (EaseManager_t3F8C15CCC71E6E2388920BCF5147DB78AFCE54CA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6619 = { sizeof (Flash_tE3F9477809C448F0F16BF2F64A707E48F8C91B3E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6620 = { sizeof (U3CPrivateImplementationDetailsU3E_tA611FCC2D7ACFEACBF22A4ED660B10D861531BEB), -1, sizeof(U3CPrivateImplementationDetailsU3E_tA611FCC2D7ACFEACBF22A4ED660B10D861531BEB_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6620[4] = 
{
	U3CPrivateImplementationDetailsU3E_tA611FCC2D7ACFEACBF22A4ED660B10D861531BEB_StaticFields::get_offset_of_U36F98278EFCD257898AD01BE39D1D0AEFB78FC551_0(),
	U3CPrivateImplementationDetailsU3E_tA611FCC2D7ACFEACBF22A4ED660B10D861531BEB_StaticFields::get_offset_of_U38C4C6C4E493EC2BEEF5F6F6A9C4472C13BED42E8_1(),
	U3CPrivateImplementationDetailsU3E_tA611FCC2D7ACFEACBF22A4ED660B10D861531BEB_StaticFields::get_offset_of_U39DC5F4D0A1418B4EC71B22D21E93D134922FA735_2(),
	U3CPrivateImplementationDetailsU3E_tA611FCC2D7ACFEACBF22A4ED660B10D861531BEB_StaticFields::get_offset_of_FD0BD55CDDDFD0B323012A45F83437763AF58952_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6621 = { sizeof (__StaticArrayInitTypeSizeU3D20_t3ECF3082F4AA0AD5E18BDD8B1803286AF95A8329)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D20_t3ECF3082F4AA0AD5E18BDD8B1803286AF95A8329 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6622 = { sizeof (__StaticArrayInitTypeSizeU3D50_t29DB5F5A1875DCB081464683E3C76FFC9FCFCA55)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D50_t29DB5F5A1875DCB081464683E3C76FFC9FCFCA55 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6623 = { sizeof (__StaticArrayInitTypeSizeU3D120_tEB325D5130B7CFF9CF1EB5B7763C067DA86B461B)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D120_tEB325D5130B7CFF9CF1EB5B7763C067DA86B461B ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6624 = { sizeof (U3CModuleU3E_t742F26D51A4DF97E0C52DE993659E98281A71377), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6625 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6625[11] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6626 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6626[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6627 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6627[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6628 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6628[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6629 = { sizeof (Entity_tB86FED06A87B5FEA836FF73B89D5168789557783), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6629[18] = 
{
	Entity_tB86FED06A87B5FEA836FF73B89D5168789557783::get_offset_of_OnComponentAdded_0(),
	Entity_tB86FED06A87B5FEA836FF73B89D5168789557783::get_offset_of_OnComponentRemoved_1(),
	Entity_tB86FED06A87B5FEA836FF73B89D5168789557783::get_offset_of_OnComponentReplaced_2(),
	Entity_tB86FED06A87B5FEA836FF73B89D5168789557783::get_offset_of_OnEntityReleased_3(),
	Entity_tB86FED06A87B5FEA836FF73B89D5168789557783::get_offset_of_OnDestroyEntity_4(),
	Entity_tB86FED06A87B5FEA836FF73B89D5168789557783::get_offset_of__componentBuffer_5(),
	Entity_tB86FED06A87B5FEA836FF73B89D5168789557783::get_offset_of__indexBuffer_6(),
	Entity_tB86FED06A87B5FEA836FF73B89D5168789557783::get_offset_of__creationIndex_7(),
	Entity_tB86FED06A87B5FEA836FF73B89D5168789557783::get_offset_of__isEnabled_8(),
	Entity_tB86FED06A87B5FEA836FF73B89D5168789557783::get_offset_of__totalComponents_9(),
	Entity_tB86FED06A87B5FEA836FF73B89D5168789557783::get_offset_of__components_10(),
	Entity_tB86FED06A87B5FEA836FF73B89D5168789557783::get_offset_of__componentPools_11(),
	Entity_tB86FED06A87B5FEA836FF73B89D5168789557783::get_offset_of__contextInfo_12(),
	Entity_tB86FED06A87B5FEA836FF73B89D5168789557783::get_offset_of__aerc_13(),
	Entity_tB86FED06A87B5FEA836FF73B89D5168789557783::get_offset_of__componentsCache_14(),
	Entity_tB86FED06A87B5FEA836FF73B89D5168789557783::get_offset_of__componentIndicesCache_15(),
	Entity_tB86FED06A87B5FEA836FF73B89D5168789557783::get_offset_of__toStringCache_16(),
	Entity_tB86FED06A87B5FEA836FF73B89D5168789557783::get_offset_of__toStringBuilder_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6630 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6630[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6631 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6631[22] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6632 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6632[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6633 = { sizeof (Systems_t4C86DA84B6D6F50FF23B497DD599ED594274DEA9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6633[4] = 
{
	Systems_t4C86DA84B6D6F50FF23B497DD599ED594274DEA9::get_offset_of__initializeSystems_0(),
	Systems_t4C86DA84B6D6F50FF23B497DD599ED594274DEA9::get_offset_of__executeSystems_1(),
	Systems_t4C86DA84B6D6F50FF23B497DD599ED594274DEA9::get_offset_of__cleanupSystems_2(),
	Systems_t4C86DA84B6D6F50FF23B497DD599ED594274DEA9::get_offset_of__tearDownSystems_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6634 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6634[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6635 = { sizeof (GroupEvent_t5D8EAED9A0E5DF01072E622340ED850672F54891)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6635[4] = 
{
	GroupEvent_t5D8EAED9A0E5DF01072E622340ED850672F54891::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6636 = { sizeof (ContextDoesNotContainEntityException_t7080D5711853883454B78F851D81DFE846DE5B95), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6637 = { sizeof (ContextEntityIndexDoesAlreadyExistException_tCB4C94A55764B9852F030A20334B50BE8299E6BC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6638 = { sizeof (ContextEntityIndexDoesNotExistException_t2191FE494E5293B1BA0242B7D67CCA9C05602672), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6639 = { sizeof (ContextStillHasRetainedEntitiesException_tFDD6AAA582B14187B08C2B3A07787308DE518C17), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6640 = { sizeof (U3CU3Ec_tBB75825E443987A79032BCE92971925042E081B4), -1, sizeof(U3CU3Ec_tBB75825E443987A79032BCE92971925042E081B4_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6640[3] = 
{
	U3CU3Ec_tBB75825E443987A79032BCE92971925042E081B4_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tBB75825E443987A79032BCE92971925042E081B4_StaticFields::get_offset_of_U3CU3E9__0_1_1(),
	U3CU3Ec_tBB75825E443987A79032BCE92971925042E081B4_StaticFields::get_offset_of_U3CU3E9__0_0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6641 = { sizeof (EntityIsNotDestroyedException_tE5F9909534769331523B7F21128EF075CE107364), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6642 = { sizeof (ContextInfoException_t5E142D0B2B6C87CF5CC9A70D38CF5D40FB1EA381), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6643 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6643[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6644 = { sizeof (EntityAlreadyHasComponentException_t30825CB8B4D13727D4ABF3EBB518E8BC590D4BFB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6645 = { sizeof (EntityDoesNotHaveComponentException_tE224D88B637576CC426C2510B406781E58AE209C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6646 = { sizeof (EntityIsAlreadyRetainedByOwnerException_t48896BC8B21BDFFFD7F85D819233996F6E8D2783), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6647 = { sizeof (EntityIsNotEnabledException_t7AE4AB9E6B2414C4063824D87F5C4674046B2C11), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6648 = { sizeof (EntityIsNotRetainedByOwnerException_t3553575E99A69F8AD64A5BAC2DC898E6954DC225), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6649 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6650 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6650[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6651 = { sizeof (EntityIndexException_t6834F599460536EA81AFA69CA928AA2022334EF4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6652 = { sizeof (MatcherException_t753A013446CEF9B87BE52DDE80A5536442489653), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6653 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6653[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6654 = { sizeof (CollectorException_tC99C67E8041C8145EFA76944442A2E52DD916E84), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6655 = { sizeof (SafeAERC_tC3A4274F22895D6896C084185BA5E145EF097A30), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6655[2] = 
{
	SafeAERC_tC3A4274F22895D6896C084185BA5E145EF097A30::get_offset_of__entity_0(),
	SafeAERC_tC3A4274F22895D6896C084185BA5E145EF097A30::get_offset_of__owners_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6656 = { sizeof (ContextInfo_tC2E42F30007CB6FEAE0F1E8B6EE4240270D59CED), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6656[3] = 
{
	ContextInfo_tC2E42F30007CB6FEAE0F1E8B6EE4240270D59CED::get_offset_of_name_0(),
	ContextInfo_tC2E42F30007CB6FEAE0F1E8B6EE4240270D59CED::get_offset_of_componentNames_1(),
	ContextInfo_tC2E42F30007CB6FEAE0F1E8B6EE4240270D59CED::get_offset_of_componentTypes_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6657 = { sizeof (EntitasException_t71786D33342FC2A2F61B3AAC7FD34318ABC7CD33), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6658 = { sizeof (ContextEntityChanged_t1C8C691CEE56C008F944E90383D7D7400E111EC1), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6659 = { sizeof (ContextGroupChanged_tDF404FDADBE283C90826E021920179C9C5986728), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6660 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6661 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6662 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6663 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6664 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6665 = { sizeof (EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6666 = { sizeof (EntityComponentReplaced_tECE10545D16DE6AECA3B03B47FA2582FE7A11B57), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6667 = { sizeof (EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6668 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6669 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6670 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6671 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6672 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6673 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6674 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6675 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6676 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6677 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6678 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6679 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6680 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6681 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6682 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6683 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6684 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6685 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6685[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6686 = { sizeof (CollectorContextExtension_tAE2C92F0329285A4FA7360BB31C30E6FF8BB32F4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6687 = { sizeof (TriggerOnEventMatcherExtension_tEF975E8E9F0D4FEE53123912F9A8837705E5FF25), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6688 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6689 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6690 = { sizeof (U3CModuleU3E_t45C880959CDBDE1C6E60B255EF64A1F2A2EE6A82), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6691 = { sizeof (ContextAttribute_tCC46F2BA273A4F8FE981674FA22BA7D229AF6817), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6691[1] = 
{
	ContextAttribute_tCC46F2BA273A4F8FE981674FA22BA7D229AF6817::get_offset_of_contextName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6692 = { sizeof (CustomEntityIndexAttribute_t514404ABF8B11B77892F7B3454E656EA019697FD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6692[1] = 
{
	CustomEntityIndexAttribute_t514404ABF8B11B77892F7B3454E656EA019697FD::get_offset_of_contextType_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6693 = { sizeof (DontGenerateAttribute_tFBFC6C730BAC7F0C3B13EFEC3D91BAC4DC04C857), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6693[1] = 
{
	DontGenerateAttribute_tFBFC6C730BAC7F0C3B13EFEC3D91BAC4DC04C857::get_offset_of_generateIndex_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6694 = { sizeof (AbstractEntityIndexAttribute_tC53F438F525D86D77BBFE5B0C9613E00E303F2EB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6694[1] = 
{
	AbstractEntityIndexAttribute_tC53F438F525D86D77BBFE5B0C9613E00E303F2EB::get_offset_of_entityIndexType_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6695 = { sizeof (EntityIndexAttribute_t157B398E0AB74C4E344254DD6D1174D59286C75E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6696 = { sizeof (EntityIndexGetMethodAttribute_t948AD15E1610A40D1DBA5CB984A4C5260A79ADD6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6697 = { sizeof (EntityIndexType_t46D4684C925867D0CCA486B402E57664F71E6F8E)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6697[3] = 
{
	EntityIndexType_t46D4684C925867D0CCA486B402E57664F71E6F8E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6698 = { sizeof (PrimaryEntityIndexAttribute_t650C506BD36D143AF4AC97DEAE82BFFB4933A12C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6699 = { sizeof (EventAttribute_t34E326AC98C5C26372A605464C75C3E6F7864C29), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6699[3] = 
{
	EventAttribute_t34E326AC98C5C26372A605464C75C3E6F7864C29::get_offset_of_eventTarget_0(),
	EventAttribute_t34E326AC98C5C26372A605464C75C3E6F7864C29::get_offset_of_eventType_1(),
	EventAttribute_t34E326AC98C5C26372A605464C75C3E6F7864C29::get_offset_of_priority_2(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
