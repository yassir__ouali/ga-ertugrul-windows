﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// ModestTree.Util.ReflectionUtil/IMemberInfo
struct IMemberInfo_tD1731B772E9EB49881FD11F95BAD87F81E98E3AF;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.Reflection.Assembly,System.Type[]>
struct Dictionary_2_tC371B9A7E8B4A801BC5A4A04E678C970C09DA4FB;
// System.Collections.Generic.IEnumerable`1<System.Reflection.Assembly>
struct IEnumerable_1_tB8810CAC403B495501CB1EFFE19817AB440D0CF2;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_t31EF1520A3A805598500BB6033C14ABDA7116D5E;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Transform>
struct IEnumerator_1_t087F3F887ADBAA0E204C4398571F003BED673D66;
// System.Collections.Generic.List`1<System.Func`2<System.Reflection.Assembly,System.Boolean>>
struct List_1_t808B4CBCA4E9C3FB196A98237AFA7B11A2C4FE6B;
// System.Collections.Generic.List`1<System.Func`2<System.Type,System.Boolean>>
struct List_1_t949487C81A6A48214F1F028DEDA38942444C592F;
// System.Collections.IEnumerator
struct IEnumerator_t8789118187258CC88B77AFAC6315B5AF87D3E18A;
// System.Func`2<System.Type,System.Boolean>
struct Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9;
// System.Func`2<System.Type,System.Reflection.Assembly>
struct Func_2_t751A0CA37094CFBF9643B9A1A5381D7F7706AD84;
// System.Func`2<UnityEngine.Component,System.Int32>
struct Func_2_t25B2DEA762E03D29CC73F2F2953058953F171F47;
// System.Func`2<UnityEngine.GameObject,System.Boolean>
struct Func_2_t9A23FB973C3AFF530C708F9A738041D32958B273;
// System.Func`2<UnityEngine.SceneManagement.Scene,System.Boolean>
struct Func_2_t62BEF835FEEE197E60620EC309AE8C51F47FCE54;
// System.Func`2<UnityEngine.Transform,UnityEngine.GameObject>
struct Func_2_t8BA966684AA1DB941EB0687302DAE3BA51D85BEF;
// System.Reflection.Assembly
struct Assembly_t;
// System.Reflection.FieldInfo
struct FieldInfo_t;
// System.Reflection.FieldInfo[]
struct FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE;
// System.Reflection.PropertyInfo
struct PropertyInfo_t;
// System.Reflection.PropertyInfo[]
struct PropertyInfoU5BU5D_tAD8E99B12FF99CA4F2EA37B612DE68E112B4CF7E;
// System.String
struct String_t;
// System.Text.RegularExpressions.Regex
struct Regex_tFD46E63A462E852189FD6AB4E2B0B67C4D8FDBDF;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// Zenject.BindFinalizerWrapper
struct BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8;
// Zenject.BindInfo
struct BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991;
// Zenject.ConventionBindInfo
struct ConventionBindInfo_tB930FD847C98C78F434391890C5A459C3CAE0777;
// Zenject.InjectContext
struct InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef REFLECTIONUTIL_T2FE9A4B708E1BDBE93AC2DA76D843CF9C58C1166_H
#define REFLECTIONUTIL_T2FE9A4B708E1BDBE93AC2DA76D843CF9C58C1166_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModestTree.Util.ReflectionUtil
struct  ReflectionUtil_t2FE9A4B708E1BDBE93AC2DA76D843CF9C58C1166  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFLECTIONUTIL_T2FE9A4B708E1BDBE93AC2DA76D843CF9C58C1166_H
#ifndef FIELDMEMBERINFO_T9BB5FB0164B17591B55E0092C1A0707596AC9D9E_H
#define FIELDMEMBERINFO_T9BB5FB0164B17591B55E0092C1A0707596AC9D9E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModestTree.Util.ReflectionUtil_FieldMemberInfo
struct  FieldMemberInfo_t9BB5FB0164B17591B55E0092C1A0707596AC9D9E  : public RuntimeObject
{
public:
	// System.Reflection.FieldInfo ModestTree.Util.ReflectionUtil_FieldMemberInfo::_fieldInfo
	FieldInfo_t * ____fieldInfo_0;

public:
	inline static int32_t get_offset_of__fieldInfo_0() { return static_cast<int32_t>(offsetof(FieldMemberInfo_t9BB5FB0164B17591B55E0092C1A0707596AC9D9E, ____fieldInfo_0)); }
	inline FieldInfo_t * get__fieldInfo_0() const { return ____fieldInfo_0; }
	inline FieldInfo_t ** get_address_of__fieldInfo_0() { return &____fieldInfo_0; }
	inline void set__fieldInfo_0(FieldInfo_t * value)
	{
		____fieldInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&____fieldInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIELDMEMBERINFO_T9BB5FB0164B17591B55E0092C1A0707596AC9D9E_H
#ifndef PROPERTYMEMBERINFO_T81056D2609AD476BD03C40AA7F982B20BC30AC3A_H
#define PROPERTYMEMBERINFO_T81056D2609AD476BD03C40AA7F982B20BC30AC3A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModestTree.Util.ReflectionUtil_PropertyMemberInfo
struct  PropertyMemberInfo_t81056D2609AD476BD03C40AA7F982B20BC30AC3A  : public RuntimeObject
{
public:
	// System.Reflection.PropertyInfo ModestTree.Util.ReflectionUtil_PropertyMemberInfo::_propInfo
	PropertyInfo_t * ____propInfo_0;

public:
	inline static int32_t get_offset_of__propInfo_0() { return static_cast<int32_t>(offsetof(PropertyMemberInfo_t81056D2609AD476BD03C40AA7F982B20BC30AC3A, ____propInfo_0)); }
	inline PropertyInfo_t * get__propInfo_0() const { return ____propInfo_0; }
	inline PropertyInfo_t ** get_address_of__propInfo_0() { return &____propInfo_0; }
	inline void set__propInfo_0(PropertyInfo_t * value)
	{
		____propInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&____propInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYMEMBERINFO_T81056D2609AD476BD03C40AA7F982B20BC30AC3A_H
#ifndef UNITYUTIL_T1A34BE5619C44240C462EF9BE7C2E5C66F9CFDE1_H
#define UNITYUTIL_T1A34BE5619C44240C462EF9BE7C2E5C66F9CFDE1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModestTree.Util.UnityUtil
struct  UnityUtil_t1A34BE5619C44240C462EF9BE7C2E5C66F9CFDE1  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYUTIL_T1A34BE5619C44240C462EF9BE7C2E5C66F9CFDE1_H
#ifndef U3CU3EC_TA602105FB398DF622DF985E5CA08C87834A51F93_H
#define U3CU3EC_TA602105FB398DF622DF985E5CA08C87834A51F93_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModestTree.Util.UnityUtil_<>c
struct  U3CU3Ec_tA602105FB398DF622DF985E5CA08C87834A51F93  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tA602105FB398DF622DF985E5CA08C87834A51F93_StaticFields
{
public:
	// ModestTree.Util.UnityUtil_<>c ModestTree.Util.UnityUtil_<>c::<>9
	U3CU3Ec_tA602105FB398DF622DF985E5CA08C87834A51F93 * ___U3CU3E9_0;
	// System.Func`2<UnityEngine.SceneManagement.Scene,System.Boolean> ModestTree.Util.UnityUtil_<>c::<>9__3_0
	Func_2_t62BEF835FEEE197E60620EC309AE8C51F47FCE54 * ___U3CU3E9__3_0_1;
	// System.Func`2<UnityEngine.Transform,UnityEngine.GameObject> ModestTree.Util.UnityUtil_<>c::<>9__15_0
	Func_2_t8BA966684AA1DB941EB0687302DAE3BA51D85BEF * ___U3CU3E9__15_0_2;
	// System.Func`2<UnityEngine.Component,System.Int32> ModestTree.Util.UnityUtil_<>c::<>9__18_0
	Func_2_t25B2DEA762E03D29CC73F2F2953058953F171F47 * ___U3CU3E9__18_0_3;
	// System.Func`2<UnityEngine.Component,System.Int32> ModestTree.Util.UnityUtil_<>c::<>9__19_0
	Func_2_t25B2DEA762E03D29CC73F2F2953058953F171F47 * ___U3CU3E9__19_0_4;
	// System.Func`2<UnityEngine.Transform,UnityEngine.GameObject> ModestTree.Util.UnityUtil_<>c::<>9__22_0
	Func_2_t8BA966684AA1DB941EB0687302DAE3BA51D85BEF * ___U3CU3E9__22_0_5;
	// System.Func`2<UnityEngine.GameObject,System.Boolean> ModestTree.Util.UnityUtil_<>c::<>9__23_0
	Func_2_t9A23FB973C3AFF530C708F9A738041D32958B273 * ___U3CU3E9__23_0_6;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tA602105FB398DF622DF985E5CA08C87834A51F93_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tA602105FB398DF622DF985E5CA08C87834A51F93 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tA602105FB398DF622DF985E5CA08C87834A51F93 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tA602105FB398DF622DF985E5CA08C87834A51F93 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__3_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tA602105FB398DF622DF985E5CA08C87834A51F93_StaticFields, ___U3CU3E9__3_0_1)); }
	inline Func_2_t62BEF835FEEE197E60620EC309AE8C51F47FCE54 * get_U3CU3E9__3_0_1() const { return ___U3CU3E9__3_0_1; }
	inline Func_2_t62BEF835FEEE197E60620EC309AE8C51F47FCE54 ** get_address_of_U3CU3E9__3_0_1() { return &___U3CU3E9__3_0_1; }
	inline void set_U3CU3E9__3_0_1(Func_2_t62BEF835FEEE197E60620EC309AE8C51F47FCE54 * value)
	{
		___U3CU3E9__3_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__3_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__15_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_tA602105FB398DF622DF985E5CA08C87834A51F93_StaticFields, ___U3CU3E9__15_0_2)); }
	inline Func_2_t8BA966684AA1DB941EB0687302DAE3BA51D85BEF * get_U3CU3E9__15_0_2() const { return ___U3CU3E9__15_0_2; }
	inline Func_2_t8BA966684AA1DB941EB0687302DAE3BA51D85BEF ** get_address_of_U3CU3E9__15_0_2() { return &___U3CU3E9__15_0_2; }
	inline void set_U3CU3E9__15_0_2(Func_2_t8BA966684AA1DB941EB0687302DAE3BA51D85BEF * value)
	{
		___U3CU3E9__15_0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__15_0_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__18_0_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_tA602105FB398DF622DF985E5CA08C87834A51F93_StaticFields, ___U3CU3E9__18_0_3)); }
	inline Func_2_t25B2DEA762E03D29CC73F2F2953058953F171F47 * get_U3CU3E9__18_0_3() const { return ___U3CU3E9__18_0_3; }
	inline Func_2_t25B2DEA762E03D29CC73F2F2953058953F171F47 ** get_address_of_U3CU3E9__18_0_3() { return &___U3CU3E9__18_0_3; }
	inline void set_U3CU3E9__18_0_3(Func_2_t25B2DEA762E03D29CC73F2F2953058953F171F47 * value)
	{
		___U3CU3E9__18_0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__18_0_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__19_0_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_tA602105FB398DF622DF985E5CA08C87834A51F93_StaticFields, ___U3CU3E9__19_0_4)); }
	inline Func_2_t25B2DEA762E03D29CC73F2F2953058953F171F47 * get_U3CU3E9__19_0_4() const { return ___U3CU3E9__19_0_4; }
	inline Func_2_t25B2DEA762E03D29CC73F2F2953058953F171F47 ** get_address_of_U3CU3E9__19_0_4() { return &___U3CU3E9__19_0_4; }
	inline void set_U3CU3E9__19_0_4(Func_2_t25B2DEA762E03D29CC73F2F2953058953F171F47 * value)
	{
		___U3CU3E9__19_0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__19_0_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__22_0_5() { return static_cast<int32_t>(offsetof(U3CU3Ec_tA602105FB398DF622DF985E5CA08C87834A51F93_StaticFields, ___U3CU3E9__22_0_5)); }
	inline Func_2_t8BA966684AA1DB941EB0687302DAE3BA51D85BEF * get_U3CU3E9__22_0_5() const { return ___U3CU3E9__22_0_5; }
	inline Func_2_t8BA966684AA1DB941EB0687302DAE3BA51D85BEF ** get_address_of_U3CU3E9__22_0_5() { return &___U3CU3E9__22_0_5; }
	inline void set_U3CU3E9__22_0_5(Func_2_t8BA966684AA1DB941EB0687302DAE3BA51D85BEF * value)
	{
		___U3CU3E9__22_0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__22_0_5), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__23_0_6() { return static_cast<int32_t>(offsetof(U3CU3Ec_tA602105FB398DF622DF985E5CA08C87834A51F93_StaticFields, ___U3CU3E9__23_0_6)); }
	inline Func_2_t9A23FB973C3AFF530C708F9A738041D32958B273 * get_U3CU3E9__23_0_6() const { return ___U3CU3E9__23_0_6; }
	inline Func_2_t9A23FB973C3AFF530C708F9A738041D32958B273 ** get_address_of_U3CU3E9__23_0_6() { return &___U3CU3E9__23_0_6; }
	inline void set_U3CU3E9__23_0_6(Func_2_t9A23FB973C3AFF530C708F9A738041D32958B273 * value)
	{
		___U3CU3E9__23_0_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__23_0_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TA602105FB398DF622DF985E5CA08C87834A51F93_H
#ifndef U3CGETDIRECTCHILDRENU3ED__21_T7B65D076C8C1E0E995E2DF95F5B0818C8FB28128_H
#define U3CGETDIRECTCHILDRENU3ED__21_T7B65D076C8C1E0E995E2DF95F5B0818C8FB28128_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModestTree.Util.UnityUtil_<GetDirectChildren>d__21
struct  U3CGetDirectChildrenU3Ed__21_t7B65D076C8C1E0E995E2DF95F5B0818C8FB28128  : public RuntimeObject
{
public:
	// System.Int32 ModestTree.Util.UnityUtil_<GetDirectChildren>d__21::<>1__state
	int32_t ___U3CU3E1__state_0;
	// UnityEngine.GameObject ModestTree.Util.UnityUtil_<GetDirectChildren>d__21::<>2__current
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___U3CU3E2__current_1;
	// System.Int32 ModestTree.Util.UnityUtil_<GetDirectChildren>d__21::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// UnityEngine.GameObject ModestTree.Util.UnityUtil_<GetDirectChildren>d__21::obj
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_3;
	// UnityEngine.GameObject ModestTree.Util.UnityUtil_<GetDirectChildren>d__21::<>3__obj
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___U3CU3E3__obj_4;
	// System.Collections.IEnumerator ModestTree.Util.UnityUtil_<GetDirectChildren>d__21::<>7__wrap1
	RuntimeObject* ___U3CU3E7__wrap1_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetDirectChildrenU3Ed__21_t7B65D076C8C1E0E995E2DF95F5B0818C8FB28128, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetDirectChildrenU3Ed__21_t7B65D076C8C1E0E995E2DF95F5B0818C8FB28128, ___U3CU3E2__current_1)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CGetDirectChildrenU3Ed__21_t7B65D076C8C1E0E995E2DF95F5B0818C8FB28128, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_obj_3() { return static_cast<int32_t>(offsetof(U3CGetDirectChildrenU3Ed__21_t7B65D076C8C1E0E995E2DF95F5B0818C8FB28128, ___obj_3)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_3() const { return ___obj_3; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_3() { return &___obj_3; }
	inline void set_obj_3(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_3 = value;
		Il2CppCodeGenWriteBarrier((&___obj_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__obj_4() { return static_cast<int32_t>(offsetof(U3CGetDirectChildrenU3Ed__21_t7B65D076C8C1E0E995E2DF95F5B0818C8FB28128, ___U3CU3E3__obj_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_U3CU3E3__obj_4() const { return ___U3CU3E3__obj_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_U3CU3E3__obj_4() { return &___U3CU3E3__obj_4; }
	inline void set_U3CU3E3__obj_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___U3CU3E3__obj_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__obj_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_5() { return static_cast<int32_t>(offsetof(U3CGetDirectChildrenU3Ed__21_t7B65D076C8C1E0E995E2DF95F5B0818C8FB28128, ___U3CU3E7__wrap1_5)); }
	inline RuntimeObject* get_U3CU3E7__wrap1_5() const { return ___U3CU3E7__wrap1_5; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap1_5() { return &___U3CU3E7__wrap1_5; }
	inline void set_U3CU3E7__wrap1_5(RuntimeObject* value)
	{
		___U3CU3E7__wrap1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap1_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETDIRECTCHILDRENU3ED__21_T7B65D076C8C1E0E995E2DF95F5B0818C8FB28128_H
#ifndef U3CGETDIRECTCHILDRENANDSELFU3ED__20_TC2B12E2A37622D65A4EAB0D3CDA05670D2882E58_H
#define U3CGETDIRECTCHILDRENANDSELFU3ED__20_TC2B12E2A37622D65A4EAB0D3CDA05670D2882E58_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModestTree.Util.UnityUtil_<GetDirectChildrenAndSelf>d__20
struct  U3CGetDirectChildrenAndSelfU3Ed__20_tC2B12E2A37622D65A4EAB0D3CDA05670D2882E58  : public RuntimeObject
{
public:
	// System.Int32 ModestTree.Util.UnityUtil_<GetDirectChildrenAndSelf>d__20::<>1__state
	int32_t ___U3CU3E1__state_0;
	// UnityEngine.GameObject ModestTree.Util.UnityUtil_<GetDirectChildrenAndSelf>d__20::<>2__current
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___U3CU3E2__current_1;
	// System.Int32 ModestTree.Util.UnityUtil_<GetDirectChildrenAndSelf>d__20::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// UnityEngine.GameObject ModestTree.Util.UnityUtil_<GetDirectChildrenAndSelf>d__20::obj
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_3;
	// UnityEngine.GameObject ModestTree.Util.UnityUtil_<GetDirectChildrenAndSelf>d__20::<>3__obj
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___U3CU3E3__obj_4;
	// System.Collections.IEnumerator ModestTree.Util.UnityUtil_<GetDirectChildrenAndSelf>d__20::<>7__wrap1
	RuntimeObject* ___U3CU3E7__wrap1_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetDirectChildrenAndSelfU3Ed__20_tC2B12E2A37622D65A4EAB0D3CDA05670D2882E58, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetDirectChildrenAndSelfU3Ed__20_tC2B12E2A37622D65A4EAB0D3CDA05670D2882E58, ___U3CU3E2__current_1)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CGetDirectChildrenAndSelfU3Ed__20_tC2B12E2A37622D65A4EAB0D3CDA05670D2882E58, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_obj_3() { return static_cast<int32_t>(offsetof(U3CGetDirectChildrenAndSelfU3Ed__20_tC2B12E2A37622D65A4EAB0D3CDA05670D2882E58, ___obj_3)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_3() const { return ___obj_3; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_3() { return &___obj_3; }
	inline void set_obj_3(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_3 = value;
		Il2CppCodeGenWriteBarrier((&___obj_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__obj_4() { return static_cast<int32_t>(offsetof(U3CGetDirectChildrenAndSelfU3Ed__20_tC2B12E2A37622D65A4EAB0D3CDA05670D2882E58, ___U3CU3E3__obj_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_U3CU3E3__obj_4() const { return ___U3CU3E3__obj_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_U3CU3E3__obj_4() { return &___U3CU3E3__obj_4; }
	inline void set_U3CU3E3__obj_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___U3CU3E3__obj_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__obj_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_5() { return static_cast<int32_t>(offsetof(U3CGetDirectChildrenAndSelfU3Ed__20_tC2B12E2A37622D65A4EAB0D3CDA05670D2882E58, ___U3CU3E7__wrap1_5)); }
	inline RuntimeObject* get_U3CU3E7__wrap1_5() const { return ___U3CU3E7__wrap1_5; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap1_5() { return &___U3CU3E7__wrap1_5; }
	inline void set_U3CU3E7__wrap1_5(RuntimeObject* value)
	{
		___U3CU3E7__wrap1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap1_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETDIRECTCHILDRENANDSELFU3ED__20_TC2B12E2A37622D65A4EAB0D3CDA05670D2882E58_H
#ifndef U3CGETPARENTSU3ED__16_T29E1A9C6BB27DF28E99854F849217242A281C128_H
#define U3CGETPARENTSU3ED__16_T29E1A9C6BB27DF28E99854F849217242A281C128_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModestTree.Util.UnityUtil_<GetParents>d__16
struct  U3CGetParentsU3Ed__16_t29E1A9C6BB27DF28E99854F849217242A281C128  : public RuntimeObject
{
public:
	// System.Int32 ModestTree.Util.UnityUtil_<GetParents>d__16::<>1__state
	int32_t ___U3CU3E1__state_0;
	// UnityEngine.Transform ModestTree.Util.UnityUtil_<GetParents>d__16::<>2__current
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___U3CU3E2__current_1;
	// System.Int32 ModestTree.Util.UnityUtil_<GetParents>d__16::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// UnityEngine.Transform ModestTree.Util.UnityUtil_<GetParents>d__16::transform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___transform_3;
	// UnityEngine.Transform ModestTree.Util.UnityUtil_<GetParents>d__16::<>3__transform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___U3CU3E3__transform_4;
	// System.Collections.Generic.IEnumerator`1<UnityEngine.Transform> ModestTree.Util.UnityUtil_<GetParents>d__16::<>7__wrap1
	RuntimeObject* ___U3CU3E7__wrap1_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetParentsU3Ed__16_t29E1A9C6BB27DF28E99854F849217242A281C128, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetParentsU3Ed__16_t29E1A9C6BB27DF28E99854F849217242A281C128, ___U3CU3E2__current_1)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CGetParentsU3Ed__16_t29E1A9C6BB27DF28E99854F849217242A281C128, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_transform_3() { return static_cast<int32_t>(offsetof(U3CGetParentsU3Ed__16_t29E1A9C6BB27DF28E99854F849217242A281C128, ___transform_3)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_transform_3() const { return ___transform_3; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_transform_3() { return &___transform_3; }
	inline void set_transform_3(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___transform_3 = value;
		Il2CppCodeGenWriteBarrier((&___transform_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__transform_4() { return static_cast<int32_t>(offsetof(U3CGetParentsU3Ed__16_t29E1A9C6BB27DF28E99854F849217242A281C128, ___U3CU3E3__transform_4)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_U3CU3E3__transform_4() const { return ___U3CU3E3__transform_4; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_U3CU3E3__transform_4() { return &___U3CU3E3__transform_4; }
	inline void set_U3CU3E3__transform_4(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___U3CU3E3__transform_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__transform_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_5() { return static_cast<int32_t>(offsetof(U3CGetParentsU3Ed__16_t29E1A9C6BB27DF28E99854F849217242A281C128, ___U3CU3E7__wrap1_5)); }
	inline RuntimeObject* get_U3CU3E7__wrap1_5() const { return ___U3CU3E7__wrap1_5; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap1_5() { return &___U3CU3E7__wrap1_5; }
	inline void set_U3CU3E7__wrap1_5(RuntimeObject* value)
	{
		___U3CU3E7__wrap1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap1_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETPARENTSU3ED__16_T29E1A9C6BB27DF28E99854F849217242A281C128_H
#ifndef U3CGETPARENTSANDSELFU3ED__17_T5430D6B6D87F95A3450AA4E4FE4FB3D518E98FE8_H
#define U3CGETPARENTSANDSELFU3ED__17_T5430D6B6D87F95A3450AA4E4FE4FB3D518E98FE8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModestTree.Util.UnityUtil_<GetParentsAndSelf>d__17
struct  U3CGetParentsAndSelfU3Ed__17_t5430D6B6D87F95A3450AA4E4FE4FB3D518E98FE8  : public RuntimeObject
{
public:
	// System.Int32 ModestTree.Util.UnityUtil_<GetParentsAndSelf>d__17::<>1__state
	int32_t ___U3CU3E1__state_0;
	// UnityEngine.Transform ModestTree.Util.UnityUtil_<GetParentsAndSelf>d__17::<>2__current
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___U3CU3E2__current_1;
	// System.Int32 ModestTree.Util.UnityUtil_<GetParentsAndSelf>d__17::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// UnityEngine.Transform ModestTree.Util.UnityUtil_<GetParentsAndSelf>d__17::transform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___transform_3;
	// UnityEngine.Transform ModestTree.Util.UnityUtil_<GetParentsAndSelf>d__17::<>3__transform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___U3CU3E3__transform_4;
	// System.Collections.Generic.IEnumerator`1<UnityEngine.Transform> ModestTree.Util.UnityUtil_<GetParentsAndSelf>d__17::<>7__wrap1
	RuntimeObject* ___U3CU3E7__wrap1_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetParentsAndSelfU3Ed__17_t5430D6B6D87F95A3450AA4E4FE4FB3D518E98FE8, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetParentsAndSelfU3Ed__17_t5430D6B6D87F95A3450AA4E4FE4FB3D518E98FE8, ___U3CU3E2__current_1)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CGetParentsAndSelfU3Ed__17_t5430D6B6D87F95A3450AA4E4FE4FB3D518E98FE8, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_transform_3() { return static_cast<int32_t>(offsetof(U3CGetParentsAndSelfU3Ed__17_t5430D6B6D87F95A3450AA4E4FE4FB3D518E98FE8, ___transform_3)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_transform_3() const { return ___transform_3; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_transform_3() { return &___transform_3; }
	inline void set_transform_3(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___transform_3 = value;
		Il2CppCodeGenWriteBarrier((&___transform_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__transform_4() { return static_cast<int32_t>(offsetof(U3CGetParentsAndSelfU3Ed__17_t5430D6B6D87F95A3450AA4E4FE4FB3D518E98FE8, ___U3CU3E3__transform_4)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_U3CU3E3__transform_4() const { return ___U3CU3E3__transform_4; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_U3CU3E3__transform_4() { return &___U3CU3E3__transform_4; }
	inline void set_U3CU3E3__transform_4(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___U3CU3E3__transform_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__transform_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_5() { return static_cast<int32_t>(offsetof(U3CGetParentsAndSelfU3Ed__17_t5430D6B6D87F95A3450AA4E4FE4FB3D518E98FE8, ___U3CU3E7__wrap1_5)); }
	inline RuntimeObject* get_U3CU3E7__wrap1_5() const { return ___U3CU3E7__wrap1_5; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap1_5() { return &___U3CU3E7__wrap1_5; }
	inline void set_U3CU3E7__wrap1_5(RuntimeObject* value)
	{
		___U3CU3E7__wrap1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap1_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETPARENTSANDSELFU3ED__17_T5430D6B6D87F95A3450AA4E4FE4FB3D518E98FE8_H
#ifndef VALUEPAIR_TACB737B99879F9153E961C487549045DE5FD7A30_H
#define VALUEPAIR_TACB737B99879F9153E961C487549045DE5FD7A30_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModestTree.Util.ValuePair
struct  ValuePair_tACB737B99879F9153E961C487549045DE5FD7A30  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALUEPAIR_TACB737B99879F9153E961C487549045DE5FD7A30_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef U3CU3EC__DISPLAYCLASS2_0_TAB80E35D5F91F578CF50CD65CC6E73D10AF1F861_H
#define U3CU3EC__DISPLAYCLASS2_0_TAB80E35D5F91F578CF50CD65CC6E73D10AF1F861_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ConditionCopyNonLazyBinder_<>c__DisplayClass2_0
struct  U3CU3Ec__DisplayClass2_0_tAB80E35D5F91F578CF50CD65CC6E73D10AF1F861  : public RuntimeObject
{
public:
	// System.Object Zenject.ConditionCopyNonLazyBinder_<>c__DisplayClass2_0::instance
	RuntimeObject * ___instance_0;

public:
	inline static int32_t get_offset_of_instance_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_tAB80E35D5F91F578CF50CD65CC6E73D10AF1F861, ___instance_0)); }
	inline RuntimeObject * get_instance_0() const { return ___instance_0; }
	inline RuntimeObject ** get_address_of_instance_0() { return &___instance_0; }
	inline void set_instance_0(RuntimeObject * value)
	{
		___instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS2_0_TAB80E35D5F91F578CF50CD65CC6E73D10AF1F861_H
#ifndef U3CU3EC__DISPLAYCLASS3_0_T1FB4741E71D3EAEABB5A2E0EA6F4D51856A1E276_H
#define U3CU3EC__DISPLAYCLASS3_0_T1FB4741E71D3EAEABB5A2E0EA6F4D51856A1E276_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ConditionCopyNonLazyBinder_<>c__DisplayClass3_0
struct  U3CU3Ec__DisplayClass3_0_t1FB4741E71D3EAEABB5A2E0EA6F4D51856A1E276  : public RuntimeObject
{
public:
	// System.Type[] Zenject.ConditionCopyNonLazyBinder_<>c__DisplayClass3_0::targets
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___targets_0;

public:
	inline static int32_t get_offset_of_targets_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_t1FB4741E71D3EAEABB5A2E0EA6F4D51856A1E276, ___targets_0)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get_targets_0() const { return ___targets_0; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of_targets_0() { return &___targets_0; }
	inline void set_targets_0(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		___targets_0 = value;
		Il2CppCodeGenWriteBarrier((&___targets_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS3_0_T1FB4741E71D3EAEABB5A2E0EA6F4D51856A1E276_H
#ifndef U3CU3EC__DISPLAYCLASS3_1_TA3BA7DCCB202AAD723101244B757FEFB8DFAF3D2_H
#define U3CU3EC__DISPLAYCLASS3_1_TA3BA7DCCB202AAD723101244B757FEFB8DFAF3D2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ConditionCopyNonLazyBinder_<>c__DisplayClass3_1
struct  U3CU3Ec__DisplayClass3_1_tA3BA7DCCB202AAD723101244B757FEFB8DFAF3D2  : public RuntimeObject
{
public:
	// Zenject.InjectContext Zenject.ConditionCopyNonLazyBinder_<>c__DisplayClass3_1::r
	InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * ___r_0;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_1_tA3BA7DCCB202AAD723101244B757FEFB8DFAF3D2, ___r_0)); }
	inline InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * get_r_0() const { return ___r_0; }
	inline InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF ** get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(InjectContext_tD1BDE94C49785956950BCFD5E1693537A7008FFF * value)
	{
		___r_0 = value;
		Il2CppCodeGenWriteBarrier((&___r_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS3_1_TA3BA7DCCB202AAD723101244B757FEFB8DFAF3D2_H
#ifndef CONVENTIONASSEMBLYSELECTIONBINDER_TDA8EA9964B1C084D8EAC0CE7947F00CC9EB0C226_H
#define CONVENTIONASSEMBLYSELECTIONBINDER_TDA8EA9964B1C084D8EAC0CE7947F00CC9EB0C226_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ConventionAssemblySelectionBinder
struct  ConventionAssemblySelectionBinder_tDA8EA9964B1C084D8EAC0CE7947F00CC9EB0C226  : public RuntimeObject
{
public:
	// Zenject.ConventionBindInfo Zenject.ConventionAssemblySelectionBinder::<BindInfo>k__BackingField
	ConventionBindInfo_tB930FD847C98C78F434391890C5A459C3CAE0777 * ___U3CBindInfoU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CBindInfoU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ConventionAssemblySelectionBinder_tDA8EA9964B1C084D8EAC0CE7947F00CC9EB0C226, ___U3CBindInfoU3Ek__BackingField_0)); }
	inline ConventionBindInfo_tB930FD847C98C78F434391890C5A459C3CAE0777 * get_U3CBindInfoU3Ek__BackingField_0() const { return ___U3CBindInfoU3Ek__BackingField_0; }
	inline ConventionBindInfo_tB930FD847C98C78F434391890C5A459C3CAE0777 ** get_address_of_U3CBindInfoU3Ek__BackingField_0() { return &___U3CBindInfoU3Ek__BackingField_0; }
	inline void set_U3CBindInfoU3Ek__BackingField_0(ConventionBindInfo_tB930FD847C98C78F434391890C5A459C3CAE0777 * value)
	{
		___U3CBindInfoU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBindInfoU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONVENTIONASSEMBLYSELECTIONBINDER_TDA8EA9964B1C084D8EAC0CE7947F00CC9EB0C226_H
#ifndef U3CU3EC_T0C530B2B83E46B5751D50A81ABC71FF75E4E87EA_H
#define U3CU3EC_T0C530B2B83E46B5751D50A81ABC71FF75E4E87EA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ConventionAssemblySelectionBinder_<>c
struct  U3CU3Ec_t0C530B2B83E46B5751D50A81ABC71FF75E4E87EA  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t0C530B2B83E46B5751D50A81ABC71FF75E4E87EA_StaticFields
{
public:
	// Zenject.ConventionAssemblySelectionBinder_<>c Zenject.ConventionAssemblySelectionBinder_<>c::<>9
	U3CU3Ec_t0C530B2B83E46B5751D50A81ABC71FF75E4E87EA * ___U3CU3E9_0;
	// System.Func`2<System.Type,System.Reflection.Assembly> Zenject.ConventionAssemblySelectionBinder_<>c::<>9__8_0
	Func_2_t751A0CA37094CFBF9643B9A1A5381D7F7706AD84 * ___U3CU3E9__8_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t0C530B2B83E46B5751D50A81ABC71FF75E4E87EA_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t0C530B2B83E46B5751D50A81ABC71FF75E4E87EA * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t0C530B2B83E46B5751D50A81ABC71FF75E4E87EA ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t0C530B2B83E46B5751D50A81ABC71FF75E4E87EA * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__8_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t0C530B2B83E46B5751D50A81ABC71FF75E4E87EA_StaticFields, ___U3CU3E9__8_0_1)); }
	inline Func_2_t751A0CA37094CFBF9643B9A1A5381D7F7706AD84 * get_U3CU3E9__8_0_1() const { return ___U3CU3E9__8_0_1; }
	inline Func_2_t751A0CA37094CFBF9643B9A1A5381D7F7706AD84 ** get_address_of_U3CU3E9__8_0_1() { return &___U3CU3E9__8_0_1; }
	inline void set_U3CU3E9__8_0_1(Func_2_t751A0CA37094CFBF9643B9A1A5381D7F7706AD84 * value)
	{
		___U3CU3E9__8_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__8_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T0C530B2B83E46B5751D50A81ABC71FF75E4E87EA_H
#ifndef U3CU3EC__DISPLAYCLASS12_0_TA8CCD26865251C02BE02D5E52854B92F2D5A263B_H
#define U3CU3EC__DISPLAYCLASS12_0_TA8CCD26865251C02BE02D5E52854B92F2D5A263B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ConventionAssemblySelectionBinder_<>c__DisplayClass12_0
struct  U3CU3Ec__DisplayClass12_0_tA8CCD26865251C02BE02D5E52854B92F2D5A263B  : public RuntimeObject
{
public:
	// System.Collections.Generic.IEnumerable`1<System.Reflection.Assembly> Zenject.ConventionAssemblySelectionBinder_<>c__DisplayClass12_0::assemblies
	RuntimeObject* ___assemblies_0;

public:
	inline static int32_t get_offset_of_assemblies_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass12_0_tA8CCD26865251C02BE02D5E52854B92F2D5A263B, ___assemblies_0)); }
	inline RuntimeObject* get_assemblies_0() const { return ___assemblies_0; }
	inline RuntimeObject** get_address_of_assemblies_0() { return &___assemblies_0; }
	inline void set_assemblies_0(RuntimeObject* value)
	{
		___assemblies_0 = value;
		Il2CppCodeGenWriteBarrier((&___assemblies_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS12_0_TA8CCD26865251C02BE02D5E52854B92F2D5A263B_H
#ifndef CONVENTIONBINDINFO_TB930FD847C98C78F434391890C5A459C3CAE0777_H
#define CONVENTIONBINDINFO_TB930FD847C98C78F434391890C5A459C3CAE0777_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ConventionBindInfo
struct  ConventionBindInfo_tB930FD847C98C78F434391890C5A459C3CAE0777  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.Func`2<System.Type,System.Boolean>> Zenject.ConventionBindInfo::_typeFilters
	List_1_t949487C81A6A48214F1F028DEDA38942444C592F * ____typeFilters_0;
	// System.Collections.Generic.List`1<System.Func`2<System.Reflection.Assembly,System.Boolean>> Zenject.ConventionBindInfo::_assemblyFilters
	List_1_t808B4CBCA4E9C3FB196A98237AFA7B11A2C4FE6B * ____assemblyFilters_1;

public:
	inline static int32_t get_offset_of__typeFilters_0() { return static_cast<int32_t>(offsetof(ConventionBindInfo_tB930FD847C98C78F434391890C5A459C3CAE0777, ____typeFilters_0)); }
	inline List_1_t949487C81A6A48214F1F028DEDA38942444C592F * get__typeFilters_0() const { return ____typeFilters_0; }
	inline List_1_t949487C81A6A48214F1F028DEDA38942444C592F ** get_address_of__typeFilters_0() { return &____typeFilters_0; }
	inline void set__typeFilters_0(List_1_t949487C81A6A48214F1F028DEDA38942444C592F * value)
	{
		____typeFilters_0 = value;
		Il2CppCodeGenWriteBarrier((&____typeFilters_0), value);
	}

	inline static int32_t get_offset_of__assemblyFilters_1() { return static_cast<int32_t>(offsetof(ConventionBindInfo_tB930FD847C98C78F434391890C5A459C3CAE0777, ____assemblyFilters_1)); }
	inline List_1_t808B4CBCA4E9C3FB196A98237AFA7B11A2C4FE6B * get__assemblyFilters_1() const { return ____assemblyFilters_1; }
	inline List_1_t808B4CBCA4E9C3FB196A98237AFA7B11A2C4FE6B ** get_address_of__assemblyFilters_1() { return &____assemblyFilters_1; }
	inline void set__assemblyFilters_1(List_1_t808B4CBCA4E9C3FB196A98237AFA7B11A2C4FE6B * value)
	{
		____assemblyFilters_1 = value;
		Il2CppCodeGenWriteBarrier((&____assemblyFilters_1), value);
	}
};

struct ConventionBindInfo_tB930FD847C98C78F434391890C5A459C3CAE0777_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Reflection.Assembly,System.Type[]> Zenject.ConventionBindInfo::_assemblyTypeCache
	Dictionary_2_tC371B9A7E8B4A801BC5A4A04E678C970C09DA4FB * ____assemblyTypeCache_2;

public:
	inline static int32_t get_offset_of__assemblyTypeCache_2() { return static_cast<int32_t>(offsetof(ConventionBindInfo_tB930FD847C98C78F434391890C5A459C3CAE0777_StaticFields, ____assemblyTypeCache_2)); }
	inline Dictionary_2_tC371B9A7E8B4A801BC5A4A04E678C970C09DA4FB * get__assemblyTypeCache_2() const { return ____assemblyTypeCache_2; }
	inline Dictionary_2_tC371B9A7E8B4A801BC5A4A04E678C970C09DA4FB ** get_address_of__assemblyTypeCache_2() { return &____assemblyTypeCache_2; }
	inline void set__assemblyTypeCache_2(Dictionary_2_tC371B9A7E8B4A801BC5A4A04E678C970C09DA4FB * value)
	{
		____assemblyTypeCache_2 = value;
		Il2CppCodeGenWriteBarrier((&____assemblyTypeCache_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONVENTIONBINDINFO_TB930FD847C98C78F434391890C5A459C3CAE0777_H
#ifndef U3CU3EC__DISPLAYCLASS6_0_TACD1A6778E1903D631607D539E248847ED355E74_H
#define U3CU3EC__DISPLAYCLASS6_0_TACD1A6778E1903D631607D539E248847ED355E74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ConventionBindInfo_<>c__DisplayClass6_0
struct  U3CU3Ec__DisplayClass6_0_tACD1A6778E1903D631607D539E248847ED355E74  : public RuntimeObject
{
public:
	// System.Reflection.Assembly Zenject.ConventionBindInfo_<>c__DisplayClass6_0::assembly
	Assembly_t * ___assembly_0;

public:
	inline static int32_t get_offset_of_assembly_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_tACD1A6778E1903D631607D539E248847ED355E74, ___assembly_0)); }
	inline Assembly_t * get_assembly_0() const { return ___assembly_0; }
	inline Assembly_t ** get_address_of_assembly_0() { return &___assembly_0; }
	inline void set_assembly_0(Assembly_t * value)
	{
		___assembly_0 = value;
		Il2CppCodeGenWriteBarrier((&___assembly_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS6_0_TACD1A6778E1903D631607D539E248847ED355E74_H
#ifndef U3CU3EC__DISPLAYCLASS7_0_T8F147406B3D60BB663C6081BA38E5B4C487C8F9D_H
#define U3CU3EC__DISPLAYCLASS7_0_T8F147406B3D60BB663C6081BA38E5B4C487C8F9D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ConventionBindInfo_<>c__DisplayClass7_0
struct  U3CU3Ec__DisplayClass7_0_t8F147406B3D60BB663C6081BA38E5B4C487C8F9D  : public RuntimeObject
{
public:
	// System.Type Zenject.ConventionBindInfo_<>c__DisplayClass7_0::type
	Type_t * ___type_0;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass7_0_t8F147406B3D60BB663C6081BA38E5B4C487C8F9D, ___type_0)); }
	inline Type_t * get_type_0() const { return ___type_0; }
	inline Type_t ** get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(Type_t * value)
	{
		___type_0 = value;
		Il2CppCodeGenWriteBarrier((&___type_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS7_0_T8F147406B3D60BB663C6081BA38E5B4C487C8F9D_H
#ifndef U3CU3EC__DISPLAYCLASS13_0_T8D723DAE7CCB78D84AAF14D60AE462752CBE6A3D_H
#define U3CU3EC__DISPLAYCLASS13_0_T8D723DAE7CCB78D84AAF14D60AE462752CBE6A3D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ConventionFilterTypesBinder_<>c__DisplayClass13_0
struct  U3CU3Ec__DisplayClass13_0_t8D723DAE7CCB78D84AAF14D60AE462752CBE6A3D  : public RuntimeObject
{
public:
	// System.Collections.Generic.IEnumerable`1<System.String> Zenject.ConventionFilterTypesBinder_<>c__DisplayClass13_0::namespaces
	RuntimeObject* ___namespaces_0;

public:
	inline static int32_t get_offset_of_namespaces_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass13_0_t8D723DAE7CCB78D84AAF14D60AE462752CBE6A3D, ___namespaces_0)); }
	inline RuntimeObject* get_namespaces_0() const { return ___namespaces_0; }
	inline RuntimeObject** get_address_of_namespaces_0() { return &___namespaces_0; }
	inline void set_namespaces_0(RuntimeObject* value)
	{
		___namespaces_0 = value;
		Il2CppCodeGenWriteBarrier((&___namespaces_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS13_0_T8D723DAE7CCB78D84AAF14D60AE462752CBE6A3D_H
#ifndef U3CU3EC__DISPLAYCLASS13_1_T72FD05D36BA3552A1ED0188D1B6F5CB7C6868BBA_H
#define U3CU3EC__DISPLAYCLASS13_1_T72FD05D36BA3552A1ED0188D1B6F5CB7C6868BBA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ConventionFilterTypesBinder_<>c__DisplayClass13_1
struct  U3CU3Ec__DisplayClass13_1_t72FD05D36BA3552A1ED0188D1B6F5CB7C6868BBA  : public RuntimeObject
{
public:
	// System.Type Zenject.ConventionFilterTypesBinder_<>c__DisplayClass13_1::t
	Type_t * ___t_0;

public:
	inline static int32_t get_offset_of_t_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass13_1_t72FD05D36BA3552A1ED0188D1B6F5CB7C6868BBA, ___t_0)); }
	inline Type_t * get_t_0() const { return ___t_0; }
	inline Type_t ** get_address_of_t_0() { return &___t_0; }
	inline void set_t_0(Type_t * value)
	{
		___t_0 = value;
		Il2CppCodeGenWriteBarrier((&___t_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS13_1_T72FD05D36BA3552A1ED0188D1B6F5CB7C6868BBA_H
#ifndef U3CU3EC__DISPLAYCLASS14_0_T634D795B9D2CBC7A8A1B6BD5DAC61AAAF43830C3_H
#define U3CU3EC__DISPLAYCLASS14_0_T634D795B9D2CBC7A8A1B6BD5DAC61AAAF43830C3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ConventionFilterTypesBinder_<>c__DisplayClass14_0
struct  U3CU3Ec__DisplayClass14_0_t634D795B9D2CBC7A8A1B6BD5DAC61AAAF43830C3  : public RuntimeObject
{
public:
	// System.String Zenject.ConventionFilterTypesBinder_<>c__DisplayClass14_0::suffix
	String_t* ___suffix_0;

public:
	inline static int32_t get_offset_of_suffix_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass14_0_t634D795B9D2CBC7A8A1B6BD5DAC61AAAF43830C3, ___suffix_0)); }
	inline String_t* get_suffix_0() const { return ___suffix_0; }
	inline String_t** get_address_of_suffix_0() { return &___suffix_0; }
	inline void set_suffix_0(String_t* value)
	{
		___suffix_0 = value;
		Il2CppCodeGenWriteBarrier((&___suffix_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS14_0_T634D795B9D2CBC7A8A1B6BD5DAC61AAAF43830C3_H
#ifndef U3CU3EC__DISPLAYCLASS15_0_TB8C83D8F822902EBD9303E8AB114EF6D237B656E_H
#define U3CU3EC__DISPLAYCLASS15_0_TB8C83D8F822902EBD9303E8AB114EF6D237B656E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ConventionFilterTypesBinder_<>c__DisplayClass15_0
struct  U3CU3Ec__DisplayClass15_0_tB8C83D8F822902EBD9303E8AB114EF6D237B656E  : public RuntimeObject
{
public:
	// System.String Zenject.ConventionFilterTypesBinder_<>c__DisplayClass15_0::prefix
	String_t* ___prefix_0;

public:
	inline static int32_t get_offset_of_prefix_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass15_0_tB8C83D8F822902EBD9303E8AB114EF6D237B656E, ___prefix_0)); }
	inline String_t* get_prefix_0() const { return ___prefix_0; }
	inline String_t** get_address_of_prefix_0() { return &___prefix_0; }
	inline void set_prefix_0(String_t* value)
	{
		___prefix_0 = value;
		Il2CppCodeGenWriteBarrier((&___prefix_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS15_0_TB8C83D8F822902EBD9303E8AB114EF6D237B656E_H
#ifndef U3CU3EC__DISPLAYCLASS18_0_TA7CD36F7ED372110BA5826B587228063E9F736F9_H
#define U3CU3EC__DISPLAYCLASS18_0_TA7CD36F7ED372110BA5826B587228063E9F736F9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ConventionFilterTypesBinder_<>c__DisplayClass18_0
struct  U3CU3Ec__DisplayClass18_0_tA7CD36F7ED372110BA5826B587228063E9F736F9  : public RuntimeObject
{
public:
	// System.Text.RegularExpressions.Regex Zenject.ConventionFilterTypesBinder_<>c__DisplayClass18_0::regex
	Regex_tFD46E63A462E852189FD6AB4E2B0B67C4D8FDBDF * ___regex_0;

public:
	inline static int32_t get_offset_of_regex_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass18_0_tA7CD36F7ED372110BA5826B587228063E9F736F9, ___regex_0)); }
	inline Regex_tFD46E63A462E852189FD6AB4E2B0B67C4D8FDBDF * get_regex_0() const { return ___regex_0; }
	inline Regex_tFD46E63A462E852189FD6AB4E2B0B67C4D8FDBDF ** get_address_of_regex_0() { return &___regex_0; }
	inline void set_regex_0(Regex_tFD46E63A462E852189FD6AB4E2B0B67C4D8FDBDF * value)
	{
		___regex_0 = value;
		Il2CppCodeGenWriteBarrier((&___regex_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS18_0_TA7CD36F7ED372110BA5826B587228063E9F736F9_H
#ifndef U3CU3EC__DISPLAYCLASS2_0_T83063F9EC62F0795B3F07FE3812F03B76CF4EB99_H
#define U3CU3EC__DISPLAYCLASS2_0_T83063F9EC62F0795B3F07FE3812F03B76CF4EB99_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ConventionFilterTypesBinder_<>c__DisplayClass2_0
struct  U3CU3Ec__DisplayClass2_0_t83063F9EC62F0795B3F07FE3812F03B76CF4EB99  : public RuntimeObject
{
public:
	// System.Type Zenject.ConventionFilterTypesBinder_<>c__DisplayClass2_0::parentType
	Type_t * ___parentType_0;

public:
	inline static int32_t get_offset_of_parentType_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t83063F9EC62F0795B3F07FE3812F03B76CF4EB99, ___parentType_0)); }
	inline Type_t * get_parentType_0() const { return ___parentType_0; }
	inline Type_t ** get_address_of_parentType_0() { return &___parentType_0; }
	inline void set_parentType_0(Type_t * value)
	{
		___parentType_0 = value;
		Il2CppCodeGenWriteBarrier((&___parentType_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS2_0_T83063F9EC62F0795B3F07FE3812F03B76CF4EB99_H
#ifndef U3CU3EC__DISPLAYCLASS4_0_TBBD4CEEE72A1A406A3008C53298DC577C1E55817_H
#define U3CU3EC__DISPLAYCLASS4_0_TBBD4CEEE72A1A406A3008C53298DC577C1E55817_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ConventionFilterTypesBinder_<>c__DisplayClass4_0
struct  U3CU3Ec__DisplayClass4_0_tBBD4CEEE72A1A406A3008C53298DC577C1E55817  : public RuntimeObject
{
public:
	// System.Type Zenject.ConventionFilterTypesBinder_<>c__DisplayClass4_0::parentType
	Type_t * ___parentType_0;

public:
	inline static int32_t get_offset_of_parentType_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_tBBD4CEEE72A1A406A3008C53298DC577C1E55817, ___parentType_0)); }
	inline Type_t * get_parentType_0() const { return ___parentType_0; }
	inline Type_t ** get_address_of_parentType_0() { return &___parentType_0; }
	inline void set_parentType_0(Type_t * value)
	{
		___parentType_0 = value;
		Il2CppCodeGenWriteBarrier((&___parentType_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS4_0_TBBD4CEEE72A1A406A3008C53298DC577C1E55817_H
#ifndef U3CU3EC__DISPLAYCLASS6_0_T14A356B2EE0A007356D8F713458A2AB1C997239A_H
#define U3CU3EC__DISPLAYCLASS6_0_T14A356B2EE0A007356D8F713458A2AB1C997239A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ConventionFilterTypesBinder_<>c__DisplayClass6_0
struct  U3CU3Ec__DisplayClass6_0_t14A356B2EE0A007356D8F713458A2AB1C997239A  : public RuntimeObject
{
public:
	// System.Type Zenject.ConventionFilterTypesBinder_<>c__DisplayClass6_0::attribute
	Type_t * ___attribute_0;

public:
	inline static int32_t get_offset_of_attribute_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_t14A356B2EE0A007356D8F713458A2AB1C997239A, ___attribute_0)); }
	inline Type_t * get_attribute_0() const { return ___attribute_0; }
	inline Type_t ** get_address_of_attribute_0() { return &___attribute_0; }
	inline void set_attribute_0(Type_t * value)
	{
		___attribute_0 = value;
		Il2CppCodeGenWriteBarrier((&___attribute_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS6_0_T14A356B2EE0A007356D8F713458A2AB1C997239A_H
#ifndef U3CU3EC__DISPLAYCLASS8_0_TC7E2D8FCD63FDBF3ECFC29C97EB0719B4004DA19_H
#define U3CU3EC__DISPLAYCLASS8_0_TC7E2D8FCD63FDBF3ECFC29C97EB0719B4004DA19_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ConventionFilterTypesBinder_<>c__DisplayClass8_0
struct  U3CU3Ec__DisplayClass8_0_tC7E2D8FCD63FDBF3ECFC29C97EB0719B4004DA19  : public RuntimeObject
{
public:
	// System.Type Zenject.ConventionFilterTypesBinder_<>c__DisplayClass8_0::attribute
	Type_t * ___attribute_0;

public:
	inline static int32_t get_offset_of_attribute_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_0_tC7E2D8FCD63FDBF3ECFC29C97EB0719B4004DA19, ___attribute_0)); }
	inline Type_t * get_attribute_0() const { return ___attribute_0; }
	inline Type_t ** get_address_of_attribute_0() { return &___attribute_0; }
	inline void set_attribute_0(Type_t * value)
	{
		___attribute_0 = value;
		Il2CppCodeGenWriteBarrier((&___attribute_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS8_0_TC7E2D8FCD63FDBF3ECFC29C97EB0719B4004DA19_H
#ifndef CONVENTIONSELECTTYPESBINDER_T990A6A643BFBFFFF47AA4D6ADCE4F943002B4451_H
#define CONVENTIONSELECTTYPESBINDER_T990A6A643BFBFFFF47AA4D6ADCE4F943002B4451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ConventionSelectTypesBinder
struct  ConventionSelectTypesBinder_t990A6A643BFBFFFF47AA4D6ADCE4F943002B4451  : public RuntimeObject
{
public:
	// Zenject.ConventionBindInfo Zenject.ConventionSelectTypesBinder::_bindInfo
	ConventionBindInfo_tB930FD847C98C78F434391890C5A459C3CAE0777 * ____bindInfo_0;

public:
	inline static int32_t get_offset_of__bindInfo_0() { return static_cast<int32_t>(offsetof(ConventionSelectTypesBinder_t990A6A643BFBFFFF47AA4D6ADCE4F943002B4451, ____bindInfo_0)); }
	inline ConventionBindInfo_tB930FD847C98C78F434391890C5A459C3CAE0777 * get__bindInfo_0() const { return ____bindInfo_0; }
	inline ConventionBindInfo_tB930FD847C98C78F434391890C5A459C3CAE0777 ** get_address_of__bindInfo_0() { return &____bindInfo_0; }
	inline void set__bindInfo_0(ConventionBindInfo_tB930FD847C98C78F434391890C5A459C3CAE0777 * value)
	{
		____bindInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&____bindInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONVENTIONSELECTTYPESBINDER_T990A6A643BFBFFFF47AA4D6ADCE4F943002B4451_H
#ifndef U3CU3EC_T4D8FB18A17EB67B14C8322B4D41E34DC1909D779_H
#define U3CU3EC_T4D8FB18A17EB67B14C8322B4D41E34DC1909D779_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ConventionSelectTypesBinder_<>c
struct  U3CU3Ec_t4D8FB18A17EB67B14C8322B4D41E34DC1909D779  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t4D8FB18A17EB67B14C8322B4D41E34DC1909D779_StaticFields
{
public:
	// Zenject.ConventionSelectTypesBinder_<>c Zenject.ConventionSelectTypesBinder_<>c::<>9
	U3CU3Ec_t4D8FB18A17EB67B14C8322B4D41E34DC1909D779 * ___U3CU3E9_0;
	// System.Func`2<System.Type,System.Boolean> Zenject.ConventionSelectTypesBinder_<>c::<>9__4_0
	Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 * ___U3CU3E9__4_0_1;
	// System.Func`2<System.Type,System.Boolean> Zenject.ConventionSelectTypesBinder_<>c::<>9__5_0
	Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 * ___U3CU3E9__5_0_2;
	// System.Func`2<System.Type,System.Boolean> Zenject.ConventionSelectTypesBinder_<>c::<>9__6_0
	Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 * ___U3CU3E9__6_0_3;
	// System.Func`2<System.Type,System.Boolean> Zenject.ConventionSelectTypesBinder_<>c::<>9__7_0
	Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 * ___U3CU3E9__7_0_4;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4D8FB18A17EB67B14C8322B4D41E34DC1909D779_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t4D8FB18A17EB67B14C8322B4D41E34DC1909D779 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t4D8FB18A17EB67B14C8322B4D41E34DC1909D779 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t4D8FB18A17EB67B14C8322B4D41E34DC1909D779 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__4_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4D8FB18A17EB67B14C8322B4D41E34DC1909D779_StaticFields, ___U3CU3E9__4_0_1)); }
	inline Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 * get_U3CU3E9__4_0_1() const { return ___U3CU3E9__4_0_1; }
	inline Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 ** get_address_of_U3CU3E9__4_0_1() { return &___U3CU3E9__4_0_1; }
	inline void set_U3CU3E9__4_0_1(Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 * value)
	{
		___U3CU3E9__4_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__4_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__5_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4D8FB18A17EB67B14C8322B4D41E34DC1909D779_StaticFields, ___U3CU3E9__5_0_2)); }
	inline Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 * get_U3CU3E9__5_0_2() const { return ___U3CU3E9__5_0_2; }
	inline Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 ** get_address_of_U3CU3E9__5_0_2() { return &___U3CU3E9__5_0_2; }
	inline void set_U3CU3E9__5_0_2(Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 * value)
	{
		___U3CU3E9__5_0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__5_0_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__6_0_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4D8FB18A17EB67B14C8322B4D41E34DC1909D779_StaticFields, ___U3CU3E9__6_0_3)); }
	inline Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 * get_U3CU3E9__6_0_3() const { return ___U3CU3E9__6_0_3; }
	inline Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 ** get_address_of_U3CU3E9__6_0_3() { return &___U3CU3E9__6_0_3; }
	inline void set_U3CU3E9__6_0_3(Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 * value)
	{
		___U3CU3E9__6_0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__6_0_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__7_0_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4D8FB18A17EB67B14C8322B4D41E34DC1909D779_StaticFields, ___U3CU3E9__7_0_4)); }
	inline Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 * get_U3CU3E9__7_0_4() const { return ___U3CU3E9__7_0_4; }
	inline Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 ** get_address_of_U3CU3E9__7_0_4() { return &___U3CU3E9__7_0_4; }
	inline void set_U3CU3E9__7_0_4(Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 * value)
	{
		___U3CU3E9__7_0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__7_0_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T4D8FB18A17EB67B14C8322B4D41E34DC1909D779_H
#ifndef NONLAZYBINDER_TA9FB0768A2F39559FBDDCAFBF940AAD240B7C43C_H
#define NONLAZYBINDER_TA9FB0768A2F39559FBDDCAFBF940AAD240B7C43C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.NonLazyBinder
struct  NonLazyBinder_tA9FB0768A2F39559FBDDCAFBF940AAD240B7C43C  : public RuntimeObject
{
public:
	// Zenject.BindInfo Zenject.NonLazyBinder::<BindInfo>k__BackingField
	BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * ___U3CBindInfoU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CBindInfoU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(NonLazyBinder_tA9FB0768A2F39559FBDDCAFBF940AAD240B7C43C, ___U3CBindInfoU3Ek__BackingField_0)); }
	inline BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * get_U3CBindInfoU3Ek__BackingField_0() const { return ___U3CBindInfoU3Ek__BackingField_0; }
	inline BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 ** get_address_of_U3CBindInfoU3Ek__BackingField_0() { return &___U3CBindInfoU3Ek__BackingField_0; }
	inline void set_U3CBindInfoU3Ek__BackingField_0(BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * value)
	{
		___U3CBindInfoU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBindInfoU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NONLAZYBINDER_TA9FB0768A2F39559FBDDCAFBF940AAD240B7C43C_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef SCENE_T942E023788C2BC9FBB7EC8356B4FB0088B2CFED2_H
#define SCENE_T942E023788C2BC9FBB7EC8356B4FB0088B2CFED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SceneManagement.Scene
struct  Scene_t942E023788C2BC9FBB7EC8356B4FB0088B2CFED2 
{
public:
	// System.Int32 UnityEngine.SceneManagement.Scene::m_Handle
	int32_t ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(Scene_t942E023788C2BC9FBB7EC8356B4FB0088B2CFED2, ___m_Handle_0)); }
	inline int32_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline int32_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(int32_t value)
	{
		___m_Handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENE_T942E023788C2BC9FBB7EC8356B4FB0088B2CFED2_H
#ifndef ARGNONLAZYBINDER_T56DEDF75B9E224623A2E117702BC79D8C9E84383_H
#define ARGNONLAZYBINDER_T56DEDF75B9E224623A2E117702BC79D8C9E84383_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ArgNonLazyBinder
struct  ArgNonLazyBinder_t56DEDF75B9E224623A2E117702BC79D8C9E84383  : public NonLazyBinder_tA9FB0768A2F39559FBDDCAFBF940AAD240B7C43C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGNONLAZYBINDER_T56DEDF75B9E224623A2E117702BC79D8C9E84383_H
#ifndef CONVENTIONFILTERTYPESBINDER_T2C483ED9EB8E219E361BFF24550EA3A5DB74B393_H
#define CONVENTIONFILTERTYPESBINDER_T2C483ED9EB8E219E361BFF24550EA3A5DB74B393_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ConventionFilterTypesBinder
struct  ConventionFilterTypesBinder_t2C483ED9EB8E219E361BFF24550EA3A5DB74B393  : public ConventionAssemblySelectionBinder_tDA8EA9964B1C084D8EAC0CE7947F00CC9EB0C226
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONVENTIONFILTERTYPESBINDER_T2C483ED9EB8E219E361BFF24550EA3A5DB74B393_H
#ifndef COPYNONLAZYBINDER_TC55396C09D2CF16C40164CC4E24D58AC45632D11_H
#define COPYNONLAZYBINDER_TC55396C09D2CF16C40164CC4E24D58AC45632D11_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.CopyNonLazyBinder
struct  CopyNonLazyBinder_tC55396C09D2CF16C40164CC4E24D58AC45632D11  : public NonLazyBinder_tA9FB0768A2F39559FBDDCAFBF940AAD240B7C43C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COPYNONLAZYBINDER_TC55396C09D2CF16C40164CC4E24D58AC45632D11_H
#ifndef U3CGET_ALLSCENESU3ED__1_TB23B48524B7BE2222993914120F76002C556EB15_H
#define U3CGET_ALLSCENESU3ED__1_TB23B48524B7BE2222993914120F76002C556EB15_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModestTree.Util.UnityUtil_<get_AllScenes>d__1
struct  U3Cget_AllScenesU3Ed__1_tB23B48524B7BE2222993914120F76002C556EB15  : public RuntimeObject
{
public:
	// System.Int32 ModestTree.Util.UnityUtil_<get_AllScenes>d__1::<>1__state
	int32_t ___U3CU3E1__state_0;
	// UnityEngine.SceneManagement.Scene ModestTree.Util.UnityUtil_<get_AllScenes>d__1::<>2__current
	Scene_t942E023788C2BC9FBB7EC8356B4FB0088B2CFED2  ___U3CU3E2__current_1;
	// System.Int32 ModestTree.Util.UnityUtil_<get_AllScenes>d__1::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// System.Int32 ModestTree.Util.UnityUtil_<get_AllScenes>d__1::<i>5__2
	int32_t ___U3CiU3E5__2_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3Cget_AllScenesU3Ed__1_tB23B48524B7BE2222993914120F76002C556EB15, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3Cget_AllScenesU3Ed__1_tB23B48524B7BE2222993914120F76002C556EB15, ___U3CU3E2__current_1)); }
	inline Scene_t942E023788C2BC9FBB7EC8356B4FB0088B2CFED2  get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline Scene_t942E023788C2BC9FBB7EC8356B4FB0088B2CFED2 * get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(Scene_t942E023788C2BC9FBB7EC8356B4FB0088B2CFED2  value)
	{
		___U3CU3E2__current_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3Cget_AllScenesU3Ed__1_tB23B48524B7BE2222993914120F76002C556EB15, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E5__2_3() { return static_cast<int32_t>(offsetof(U3Cget_AllScenesU3Ed__1_tB23B48524B7BE2222993914120F76002C556EB15, ___U3CiU3E5__2_3)); }
	inline int32_t get_U3CiU3E5__2_3() const { return ___U3CiU3E5__2_3; }
	inline int32_t* get_address_of_U3CiU3E5__2_3() { return &___U3CiU3E5__2_3; }
	inline void set_U3CiU3E5__2_3(int32_t value)
	{
		___U3CiU3E5__2_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGET_ALLSCENESU3ED__1_TB23B48524B7BE2222993914120F76002C556EB15_H
#ifndef BINDINGFLAGS_TE35C91D046E63A1B92BB9AB909FCF9DA84379ED0_H
#define BINDINGFLAGS_TE35C91D046E63A1B92BB9AB909FCF9DA84379ED0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_tE35C91D046E63A1B92BB9AB909FCF9DA84379ED0 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tE35C91D046E63A1B92BB9AB909FCF9DA84379ED0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_TE35C91D046E63A1B92BB9AB909FCF9DA84379ED0_H
#ifndef CONDITIONCOPYNONLAZYBINDER_TF7F37C5F1508AAB283151819E412C853A8D94B9B_H
#define CONDITIONCOPYNONLAZYBINDER_TF7F37C5F1508AAB283151819E412C853A8D94B9B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ConditionCopyNonLazyBinder
struct  ConditionCopyNonLazyBinder_tF7F37C5F1508AAB283151819E412C853A8D94B9B  : public CopyNonLazyBinder_tC55396C09D2CF16C40164CC4E24D58AC45632D11
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONDITIONCOPYNONLAZYBINDER_TF7F37C5F1508AAB283151819E412C853A8D94B9B_H
#ifndef U3CGETFIELDSANDPROPERTIESU3ED__6_T4D3FD6BF24CA3BECD8E1B8E852C58D8272FDE598_H
#define U3CGETFIELDSANDPROPERTIESU3ED__6_T4D3FD6BF24CA3BECD8E1B8E852C58D8272FDE598_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModestTree.Util.ReflectionUtil_<GetFieldsAndProperties>d__6
struct  U3CGetFieldsAndPropertiesU3Ed__6_t4D3FD6BF24CA3BECD8E1B8E852C58D8272FDE598  : public RuntimeObject
{
public:
	// System.Int32 ModestTree.Util.ReflectionUtil_<GetFieldsAndProperties>d__6::<>1__state
	int32_t ___U3CU3E1__state_0;
	// ModestTree.Util.ReflectionUtil_IMemberInfo ModestTree.Util.ReflectionUtil_<GetFieldsAndProperties>d__6::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// System.Int32 ModestTree.Util.ReflectionUtil_<GetFieldsAndProperties>d__6::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// System.Type ModestTree.Util.ReflectionUtil_<GetFieldsAndProperties>d__6::type
	Type_t * ___type_3;
	// System.Type ModestTree.Util.ReflectionUtil_<GetFieldsAndProperties>d__6::<>3__type
	Type_t * ___U3CU3E3__type_4;
	// System.Reflection.BindingFlags ModestTree.Util.ReflectionUtil_<GetFieldsAndProperties>d__6::flags
	int32_t ___flags_5;
	// System.Reflection.BindingFlags ModestTree.Util.ReflectionUtil_<GetFieldsAndProperties>d__6::<>3__flags
	int32_t ___U3CU3E3__flags_6;
	// System.Reflection.PropertyInfo[] ModestTree.Util.ReflectionUtil_<GetFieldsAndProperties>d__6::<>7__wrap1
	PropertyInfoU5BU5D_tAD8E99B12FF99CA4F2EA37B612DE68E112B4CF7E* ___U3CU3E7__wrap1_7;
	// System.Int32 ModestTree.Util.ReflectionUtil_<GetFieldsAndProperties>d__6::<>7__wrap2
	int32_t ___U3CU3E7__wrap2_8;
	// System.Reflection.FieldInfo[] ModestTree.Util.ReflectionUtil_<GetFieldsAndProperties>d__6::<>7__wrap3
	FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE* ___U3CU3E7__wrap3_9;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetFieldsAndPropertiesU3Ed__6_t4D3FD6BF24CA3BECD8E1B8E852C58D8272FDE598, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetFieldsAndPropertiesU3Ed__6_t4D3FD6BF24CA3BECD8E1B8E852C58D8272FDE598, ___U3CU3E2__current_1)); }
	inline RuntimeObject* get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject* value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CGetFieldsAndPropertiesU3Ed__6_t4D3FD6BF24CA3BECD8E1B8E852C58D8272FDE598, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(U3CGetFieldsAndPropertiesU3Ed__6_t4D3FD6BF24CA3BECD8E1B8E852C58D8272FDE598, ___type_3)); }
	inline Type_t * get_type_3() const { return ___type_3; }
	inline Type_t ** get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(Type_t * value)
	{
		___type_3 = value;
		Il2CppCodeGenWriteBarrier((&___type_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__type_4() { return static_cast<int32_t>(offsetof(U3CGetFieldsAndPropertiesU3Ed__6_t4D3FD6BF24CA3BECD8E1B8E852C58D8272FDE598, ___U3CU3E3__type_4)); }
	inline Type_t * get_U3CU3E3__type_4() const { return ___U3CU3E3__type_4; }
	inline Type_t ** get_address_of_U3CU3E3__type_4() { return &___U3CU3E3__type_4; }
	inline void set_U3CU3E3__type_4(Type_t * value)
	{
		___U3CU3E3__type_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__type_4), value);
	}

	inline static int32_t get_offset_of_flags_5() { return static_cast<int32_t>(offsetof(U3CGetFieldsAndPropertiesU3Ed__6_t4D3FD6BF24CA3BECD8E1B8E852C58D8272FDE598, ___flags_5)); }
	inline int32_t get_flags_5() const { return ___flags_5; }
	inline int32_t* get_address_of_flags_5() { return &___flags_5; }
	inline void set_flags_5(int32_t value)
	{
		___flags_5 = value;
	}

	inline static int32_t get_offset_of_U3CU3E3__flags_6() { return static_cast<int32_t>(offsetof(U3CGetFieldsAndPropertiesU3Ed__6_t4D3FD6BF24CA3BECD8E1B8E852C58D8272FDE598, ___U3CU3E3__flags_6)); }
	inline int32_t get_U3CU3E3__flags_6() const { return ___U3CU3E3__flags_6; }
	inline int32_t* get_address_of_U3CU3E3__flags_6() { return &___U3CU3E3__flags_6; }
	inline void set_U3CU3E3__flags_6(int32_t value)
	{
		___U3CU3E3__flags_6 = value;
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_7() { return static_cast<int32_t>(offsetof(U3CGetFieldsAndPropertiesU3Ed__6_t4D3FD6BF24CA3BECD8E1B8E852C58D8272FDE598, ___U3CU3E7__wrap1_7)); }
	inline PropertyInfoU5BU5D_tAD8E99B12FF99CA4F2EA37B612DE68E112B4CF7E* get_U3CU3E7__wrap1_7() const { return ___U3CU3E7__wrap1_7; }
	inline PropertyInfoU5BU5D_tAD8E99B12FF99CA4F2EA37B612DE68E112B4CF7E** get_address_of_U3CU3E7__wrap1_7() { return &___U3CU3E7__wrap1_7; }
	inline void set_U3CU3E7__wrap1_7(PropertyInfoU5BU5D_tAD8E99B12FF99CA4F2EA37B612DE68E112B4CF7E* value)
	{
		___U3CU3E7__wrap1_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap1_7), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap2_8() { return static_cast<int32_t>(offsetof(U3CGetFieldsAndPropertiesU3Ed__6_t4D3FD6BF24CA3BECD8E1B8E852C58D8272FDE598, ___U3CU3E7__wrap2_8)); }
	inline int32_t get_U3CU3E7__wrap2_8() const { return ___U3CU3E7__wrap2_8; }
	inline int32_t* get_address_of_U3CU3E7__wrap2_8() { return &___U3CU3E7__wrap2_8; }
	inline void set_U3CU3E7__wrap2_8(int32_t value)
	{
		___U3CU3E7__wrap2_8 = value;
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap3_9() { return static_cast<int32_t>(offsetof(U3CGetFieldsAndPropertiesU3Ed__6_t4D3FD6BF24CA3BECD8E1B8E852C58D8272FDE598, ___U3CU3E7__wrap3_9)); }
	inline FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE* get_U3CU3E7__wrap3_9() const { return ___U3CU3E7__wrap3_9; }
	inline FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE** get_address_of_U3CU3E7__wrap3_9() { return &___U3CU3E7__wrap3_9; }
	inline void set_U3CU3E7__wrap3_9(FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE* value)
	{
		___U3CU3E7__wrap3_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap3_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETFIELDSANDPROPERTIESU3ED__6_T4D3FD6BF24CA3BECD8E1B8E852C58D8272FDE598_H
#ifndef ARGCONDITIONCOPYNONLAZYBINDER_T5F1B4361E0805E20DDBEDEBC61B90CFFBB68B21B_H
#define ARGCONDITIONCOPYNONLAZYBINDER_T5F1B4361E0805E20DDBEDEBC61B90CFFBB68B21B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ArgConditionCopyNonLazyBinder
struct  ArgConditionCopyNonLazyBinder_t5F1B4361E0805E20DDBEDEBC61B90CFFBB68B21B  : public ConditionCopyNonLazyBinder_tF7F37C5F1508AAB283151819E412C853A8D94B9B
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGCONDITIONCOPYNONLAZYBINDER_T5F1B4361E0805E20DDBEDEBC61B90CFFBB68B21B_H
#ifndef SCOPEARGCONDITIONCOPYNONLAZYBINDER_T218A003451367EEC6A439D63A0F38B964EF07B33_H
#define SCOPEARGCONDITIONCOPYNONLAZYBINDER_T218A003451367EEC6A439D63A0F38B964EF07B33_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ScopeArgConditionCopyNonLazyBinder
struct  ScopeArgConditionCopyNonLazyBinder_t218A003451367EEC6A439D63A0F38B964EF07B33  : public ArgConditionCopyNonLazyBinder_t5F1B4361E0805E20DDBEDEBC61B90CFFBB68B21B
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCOPEARGCONDITIONCOPYNONLAZYBINDER_T218A003451367EEC6A439D63A0F38B964EF07B33_H
#ifndef FROMBINDER_T69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75_H
#define FROMBINDER_T69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.FromBinder
struct  FromBinder_t69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75  : public ScopeArgConditionCopyNonLazyBinder_t218A003451367EEC6A439D63A0F38B964EF07B33
{
public:
	// Zenject.BindFinalizerWrapper Zenject.FromBinder::<FinalizerWrapper>k__BackingField
	BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 * ___U3CFinalizerWrapperU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CFinalizerWrapperU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(FromBinder_t69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75, ___U3CFinalizerWrapperU3Ek__BackingField_1)); }
	inline BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 * get_U3CFinalizerWrapperU3Ek__BackingField_1() const { return ___U3CFinalizerWrapperU3Ek__BackingField_1; }
	inline BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 ** get_address_of_U3CFinalizerWrapperU3Ek__BackingField_1() { return &___U3CFinalizerWrapperU3Ek__BackingField_1; }
	inline void set_U3CFinalizerWrapperU3Ek__BackingField_1(BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 * value)
	{
		___U3CFinalizerWrapperU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFinalizerWrapperU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FROMBINDER_T69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75_H
#ifndef FROMBINDERNONGENERIC_T677B667DE367A055191AC7B64A622B8495C6CF89_H
#define FROMBINDERNONGENERIC_T677B667DE367A055191AC7B64A622B8495C6CF89_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.FromBinderNonGeneric
struct  FromBinderNonGeneric_t677B667DE367A055191AC7B64A622B8495C6CF89  : public FromBinder_t69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FROMBINDERNONGENERIC_T677B667DE367A055191AC7B64A622B8495C6CF89_H
#ifndef CONCRETEBINDERNONGENERIC_T607DFF5BDEE16A8EF207F5D6148835CD108F72F9_H
#define CONCRETEBINDERNONGENERIC_T607DFF5BDEE16A8EF207F5D6148835CD108F72F9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ConcreteBinderNonGeneric
struct  ConcreteBinderNonGeneric_t607DFF5BDEE16A8EF207F5D6148835CD108F72F9  : public FromBinderNonGeneric_t677B667DE367A055191AC7B64A622B8495C6CF89
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONCRETEBINDERNONGENERIC_T607DFF5BDEE16A8EF207F5D6148835CD108F72F9_H
#ifndef CONCRETEIDBINDERNONGENERIC_T372B43CB3CC50C44EF038F152C0F6345DFA22E24_H
#define CONCRETEIDBINDERNONGENERIC_T372B43CB3CC50C44EF038F152C0F6345DFA22E24_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ConcreteIdBinderNonGeneric
struct  ConcreteIdBinderNonGeneric_t372B43CB3CC50C44EF038F152C0F6345DFA22E24  : public ConcreteBinderNonGeneric_t607DFF5BDEE16A8EF207F5D6148835CD108F72F9
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONCRETEIDBINDERNONGENERIC_T372B43CB3CC50C44EF038F152C0F6345DFA22E24_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7000 = { sizeof (ReflectionUtil_t2FE9A4B708E1BDBE93AC2DA76D843CF9C58C1166), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7001 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7002 = { sizeof (PropertyMemberInfo_t81056D2609AD476BD03C40AA7F982B20BC30AC3A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7002[1] = 
{
	PropertyMemberInfo_t81056D2609AD476BD03C40AA7F982B20BC30AC3A::get_offset_of__propInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7003 = { sizeof (FieldMemberInfo_t9BB5FB0164B17591B55E0092C1A0707596AC9D9E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7003[1] = 
{
	FieldMemberInfo_t9BB5FB0164B17591B55E0092C1A0707596AC9D9E::get_offset_of__fieldInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7004 = { sizeof (U3CGetFieldsAndPropertiesU3Ed__6_t4D3FD6BF24CA3BECD8E1B8E852C58D8272FDE598), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7004[10] = 
{
	U3CGetFieldsAndPropertiesU3Ed__6_t4D3FD6BF24CA3BECD8E1B8E852C58D8272FDE598::get_offset_of_U3CU3E1__state_0(),
	U3CGetFieldsAndPropertiesU3Ed__6_t4D3FD6BF24CA3BECD8E1B8E852C58D8272FDE598::get_offset_of_U3CU3E2__current_1(),
	U3CGetFieldsAndPropertiesU3Ed__6_t4D3FD6BF24CA3BECD8E1B8E852C58D8272FDE598::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CGetFieldsAndPropertiesU3Ed__6_t4D3FD6BF24CA3BECD8E1B8E852C58D8272FDE598::get_offset_of_type_3(),
	U3CGetFieldsAndPropertiesU3Ed__6_t4D3FD6BF24CA3BECD8E1B8E852C58D8272FDE598::get_offset_of_U3CU3E3__type_4(),
	U3CGetFieldsAndPropertiesU3Ed__6_t4D3FD6BF24CA3BECD8E1B8E852C58D8272FDE598::get_offset_of_flags_5(),
	U3CGetFieldsAndPropertiesU3Ed__6_t4D3FD6BF24CA3BECD8E1B8E852C58D8272FDE598::get_offset_of_U3CU3E3__flags_6(),
	U3CGetFieldsAndPropertiesU3Ed__6_t4D3FD6BF24CA3BECD8E1B8E852C58D8272FDE598::get_offset_of_U3CU3E7__wrap1_7(),
	U3CGetFieldsAndPropertiesU3Ed__6_t4D3FD6BF24CA3BECD8E1B8E852C58D8272FDE598::get_offset_of_U3CU3E7__wrap2_8(),
	U3CGetFieldsAndPropertiesU3Ed__6_t4D3FD6BF24CA3BECD8E1B8E852C58D8272FDE598::get_offset_of_U3CU3E7__wrap3_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7005 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7005[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7006 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7006[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7007 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7007[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7008 = { sizeof (ValuePair_tACB737B99879F9153E961C487549045DE5FD7A30), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7009 = { sizeof (UnityUtil_t1A34BE5619C44240C462EF9BE7C2E5C66F9CFDE1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7010 = { sizeof (U3Cget_AllScenesU3Ed__1_tB23B48524B7BE2222993914120F76002C556EB15), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7010[4] = 
{
	U3Cget_AllScenesU3Ed__1_tB23B48524B7BE2222993914120F76002C556EB15::get_offset_of_U3CU3E1__state_0(),
	U3Cget_AllScenesU3Ed__1_tB23B48524B7BE2222993914120F76002C556EB15::get_offset_of_U3CU3E2__current_1(),
	U3Cget_AllScenesU3Ed__1_tB23B48524B7BE2222993914120F76002C556EB15::get_offset_of_U3CU3El__initialThreadId_2(),
	U3Cget_AllScenesU3Ed__1_tB23B48524B7BE2222993914120F76002C556EB15::get_offset_of_U3CiU3E5__2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7011 = { sizeof (U3CU3Ec_tA602105FB398DF622DF985E5CA08C87834A51F93), -1, sizeof(U3CU3Ec_tA602105FB398DF622DF985E5CA08C87834A51F93_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7011[7] = 
{
	U3CU3Ec_tA602105FB398DF622DF985E5CA08C87834A51F93_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tA602105FB398DF622DF985E5CA08C87834A51F93_StaticFields::get_offset_of_U3CU3E9__3_0_1(),
	U3CU3Ec_tA602105FB398DF622DF985E5CA08C87834A51F93_StaticFields::get_offset_of_U3CU3E9__15_0_2(),
	U3CU3Ec_tA602105FB398DF622DF985E5CA08C87834A51F93_StaticFields::get_offset_of_U3CU3E9__18_0_3(),
	U3CU3Ec_tA602105FB398DF622DF985E5CA08C87834A51F93_StaticFields::get_offset_of_U3CU3E9__19_0_4(),
	U3CU3Ec_tA602105FB398DF622DF985E5CA08C87834A51F93_StaticFields::get_offset_of_U3CU3E9__22_0_5(),
	U3CU3Ec_tA602105FB398DF622DF985E5CA08C87834A51F93_StaticFields::get_offset_of_U3CU3E9__23_0_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7012 = { sizeof (U3CGetParentsU3Ed__16_t29E1A9C6BB27DF28E99854F849217242A281C128), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7012[6] = 
{
	U3CGetParentsU3Ed__16_t29E1A9C6BB27DF28E99854F849217242A281C128::get_offset_of_U3CU3E1__state_0(),
	U3CGetParentsU3Ed__16_t29E1A9C6BB27DF28E99854F849217242A281C128::get_offset_of_U3CU3E2__current_1(),
	U3CGetParentsU3Ed__16_t29E1A9C6BB27DF28E99854F849217242A281C128::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CGetParentsU3Ed__16_t29E1A9C6BB27DF28E99854F849217242A281C128::get_offset_of_transform_3(),
	U3CGetParentsU3Ed__16_t29E1A9C6BB27DF28E99854F849217242A281C128::get_offset_of_U3CU3E3__transform_4(),
	U3CGetParentsU3Ed__16_t29E1A9C6BB27DF28E99854F849217242A281C128::get_offset_of_U3CU3E7__wrap1_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7013 = { sizeof (U3CGetParentsAndSelfU3Ed__17_t5430D6B6D87F95A3450AA4E4FE4FB3D518E98FE8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7013[6] = 
{
	U3CGetParentsAndSelfU3Ed__17_t5430D6B6D87F95A3450AA4E4FE4FB3D518E98FE8::get_offset_of_U3CU3E1__state_0(),
	U3CGetParentsAndSelfU3Ed__17_t5430D6B6D87F95A3450AA4E4FE4FB3D518E98FE8::get_offset_of_U3CU3E2__current_1(),
	U3CGetParentsAndSelfU3Ed__17_t5430D6B6D87F95A3450AA4E4FE4FB3D518E98FE8::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CGetParentsAndSelfU3Ed__17_t5430D6B6D87F95A3450AA4E4FE4FB3D518E98FE8::get_offset_of_transform_3(),
	U3CGetParentsAndSelfU3Ed__17_t5430D6B6D87F95A3450AA4E4FE4FB3D518E98FE8::get_offset_of_U3CU3E3__transform_4(),
	U3CGetParentsAndSelfU3Ed__17_t5430D6B6D87F95A3450AA4E4FE4FB3D518E98FE8::get_offset_of_U3CU3E7__wrap1_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7014 = { sizeof (U3CGetDirectChildrenAndSelfU3Ed__20_tC2B12E2A37622D65A4EAB0D3CDA05670D2882E58), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7014[6] = 
{
	U3CGetDirectChildrenAndSelfU3Ed__20_tC2B12E2A37622D65A4EAB0D3CDA05670D2882E58::get_offset_of_U3CU3E1__state_0(),
	U3CGetDirectChildrenAndSelfU3Ed__20_tC2B12E2A37622D65A4EAB0D3CDA05670D2882E58::get_offset_of_U3CU3E2__current_1(),
	U3CGetDirectChildrenAndSelfU3Ed__20_tC2B12E2A37622D65A4EAB0D3CDA05670D2882E58::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CGetDirectChildrenAndSelfU3Ed__20_tC2B12E2A37622D65A4EAB0D3CDA05670D2882E58::get_offset_of_obj_3(),
	U3CGetDirectChildrenAndSelfU3Ed__20_tC2B12E2A37622D65A4EAB0D3CDA05670D2882E58::get_offset_of_U3CU3E3__obj_4(),
	U3CGetDirectChildrenAndSelfU3Ed__20_tC2B12E2A37622D65A4EAB0D3CDA05670D2882E58::get_offset_of_U3CU3E7__wrap1_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7015 = { sizeof (U3CGetDirectChildrenU3Ed__21_t7B65D076C8C1E0E995E2DF95F5B0818C8FB28128), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7015[6] = 
{
	U3CGetDirectChildrenU3Ed__21_t7B65D076C8C1E0E995E2DF95F5B0818C8FB28128::get_offset_of_U3CU3E1__state_0(),
	U3CGetDirectChildrenU3Ed__21_t7B65D076C8C1E0E995E2DF95F5B0818C8FB28128::get_offset_of_U3CU3E2__current_1(),
	U3CGetDirectChildrenU3Ed__21_t7B65D076C8C1E0E995E2DF95F5B0818C8FB28128::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CGetDirectChildrenU3Ed__21_t7B65D076C8C1E0E995E2DF95F5B0818C8FB28128::get_offset_of_obj_3(),
	U3CGetDirectChildrenU3Ed__21_t7B65D076C8C1E0E995E2DF95F5B0818C8FB28128::get_offset_of_U3CU3E3__obj_4(),
	U3CGetDirectChildrenU3Ed__21_t7B65D076C8C1E0E995E2DF95F5B0818C8FB28128::get_offset_of_U3CU3E7__wrap1_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7016 = { sizeof (ArgConditionCopyNonLazyBinder_t5F1B4361E0805E20DDBEDEBC61B90CFFBB68B21B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7017 = { sizeof (ArgNonLazyBinder_t56DEDF75B9E224623A2E117702BC79D8C9E84383), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7018 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7019 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7019[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7020 = { sizeof (ConcreteBinderNonGeneric_t607DFF5BDEE16A8EF207F5D6148835CD108F72F9), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7021 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7022 = { sizeof (ConcreteIdBinderNonGeneric_t372B43CB3CC50C44EF038F152C0F6345DFA22E24), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7023 = { sizeof (ConditionCopyNonLazyBinder_tF7F37C5F1508AAB283151819E412C853A8D94B9B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7024 = { sizeof (U3CU3Ec__DisplayClass2_0_tAB80E35D5F91F578CF50CD65CC6E73D10AF1F861), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7024[1] = 
{
	U3CU3Ec__DisplayClass2_0_tAB80E35D5F91F578CF50CD65CC6E73D10AF1F861::get_offset_of_instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7025 = { sizeof (U3CU3Ec__DisplayClass3_0_t1FB4741E71D3EAEABB5A2E0EA6F4D51856A1E276), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7025[1] = 
{
	U3CU3Ec__DisplayClass3_0_t1FB4741E71D3EAEABB5A2E0EA6F4D51856A1E276::get_offset_of_targets_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7026 = { sizeof (U3CU3Ec__DisplayClass3_1_tA3BA7DCCB202AAD723101244B757FEFB8DFAF3D2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7026[1] = 
{
	U3CU3Ec__DisplayClass3_1_tA3BA7DCCB202AAD723101244B757FEFB8DFAF3D2::get_offset_of_r_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7027 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7027[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7028 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7028[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7029 = { sizeof (ConventionAssemblySelectionBinder_tDA8EA9964B1C084D8EAC0CE7947F00CC9EB0C226), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7029[1] = 
{
	ConventionAssemblySelectionBinder_tDA8EA9964B1C084D8EAC0CE7947F00CC9EB0C226::get_offset_of_U3CBindInfoU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7030 = { sizeof (U3CU3Ec_t0C530B2B83E46B5751D50A81ABC71FF75E4E87EA), -1, sizeof(U3CU3Ec_t0C530B2B83E46B5751D50A81ABC71FF75E4E87EA_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7030[2] = 
{
	U3CU3Ec_t0C530B2B83E46B5751D50A81ABC71FF75E4E87EA_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t0C530B2B83E46B5751D50A81ABC71FF75E4E87EA_StaticFields::get_offset_of_U3CU3E9__8_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7031 = { sizeof (U3CU3Ec__DisplayClass12_0_tA8CCD26865251C02BE02D5E52854B92F2D5A263B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7031[1] = 
{
	U3CU3Ec__DisplayClass12_0_tA8CCD26865251C02BE02D5E52854B92F2D5A263B::get_offset_of_assemblies_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7032 = { sizeof (ConventionBindInfo_tB930FD847C98C78F434391890C5A459C3CAE0777), -1, sizeof(ConventionBindInfo_tB930FD847C98C78F434391890C5A459C3CAE0777_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7032[3] = 
{
	ConventionBindInfo_tB930FD847C98C78F434391890C5A459C3CAE0777::get_offset_of__typeFilters_0(),
	ConventionBindInfo_tB930FD847C98C78F434391890C5A459C3CAE0777::get_offset_of__assemblyFilters_1(),
	ConventionBindInfo_tB930FD847C98C78F434391890C5A459C3CAE0777_StaticFields::get_offset_of__assemblyTypeCache_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7033 = { sizeof (U3CU3Ec__DisplayClass6_0_tACD1A6778E1903D631607D539E248847ED355E74), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7033[1] = 
{
	U3CU3Ec__DisplayClass6_0_tACD1A6778E1903D631607D539E248847ED355E74::get_offset_of_assembly_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7034 = { sizeof (U3CU3Ec__DisplayClass7_0_t8F147406B3D60BB663C6081BA38E5B4C487C8F9D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7034[1] = 
{
	U3CU3Ec__DisplayClass7_0_t8F147406B3D60BB663C6081BA38E5B4C487C8F9D::get_offset_of_type_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7035 = { sizeof (ConventionFilterTypesBinder_t2C483ED9EB8E219E361BFF24550EA3A5DB74B393), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7036 = { sizeof (U3CU3Ec__DisplayClass2_0_t83063F9EC62F0795B3F07FE3812F03B76CF4EB99), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7036[1] = 
{
	U3CU3Ec__DisplayClass2_0_t83063F9EC62F0795B3F07FE3812F03B76CF4EB99::get_offset_of_parentType_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7037 = { sizeof (U3CU3Ec__DisplayClass4_0_tBBD4CEEE72A1A406A3008C53298DC577C1E55817), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7037[1] = 
{
	U3CU3Ec__DisplayClass4_0_tBBD4CEEE72A1A406A3008C53298DC577C1E55817::get_offset_of_parentType_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7038 = { sizeof (U3CU3Ec__DisplayClass6_0_t14A356B2EE0A007356D8F713458A2AB1C997239A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7038[1] = 
{
	U3CU3Ec__DisplayClass6_0_t14A356B2EE0A007356D8F713458A2AB1C997239A::get_offset_of_attribute_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7039 = { sizeof (U3CU3Ec__DisplayClass8_0_tC7E2D8FCD63FDBF3ECFC29C97EB0719B4004DA19), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7039[1] = 
{
	U3CU3Ec__DisplayClass8_0_tC7E2D8FCD63FDBF3ECFC29C97EB0719B4004DA19::get_offset_of_attribute_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7040 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7040[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7041 = { sizeof (U3CU3Ec__DisplayClass13_0_t8D723DAE7CCB78D84AAF14D60AE462752CBE6A3D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7041[1] = 
{
	U3CU3Ec__DisplayClass13_0_t8D723DAE7CCB78D84AAF14D60AE462752CBE6A3D::get_offset_of_namespaces_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7042 = { sizeof (U3CU3Ec__DisplayClass13_1_t72FD05D36BA3552A1ED0188D1B6F5CB7C6868BBA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7042[1] = 
{
	U3CU3Ec__DisplayClass13_1_t72FD05D36BA3552A1ED0188D1B6F5CB7C6868BBA::get_offset_of_t_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7043 = { sizeof (U3CU3Ec__DisplayClass14_0_t634D795B9D2CBC7A8A1B6BD5DAC61AAAF43830C3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7043[1] = 
{
	U3CU3Ec__DisplayClass14_0_t634D795B9D2CBC7A8A1B6BD5DAC61AAAF43830C3::get_offset_of_suffix_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7044 = { sizeof (U3CU3Ec__DisplayClass15_0_tB8C83D8F822902EBD9303E8AB114EF6D237B656E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7044[1] = 
{
	U3CU3Ec__DisplayClass15_0_tB8C83D8F822902EBD9303E8AB114EF6D237B656E::get_offset_of_prefix_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7045 = { sizeof (U3CU3Ec__DisplayClass18_0_tA7CD36F7ED372110BA5826B587228063E9F736F9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7045[1] = 
{
	U3CU3Ec__DisplayClass18_0_tA7CD36F7ED372110BA5826B587228063E9F736F9::get_offset_of_regex_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7046 = { sizeof (ConventionSelectTypesBinder_t990A6A643BFBFFFF47AA4D6ADCE4F943002B4451), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7046[1] = 
{
	ConventionSelectTypesBinder_t990A6A643BFBFFFF47AA4D6ADCE4F943002B4451::get_offset_of__bindInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7047 = { sizeof (U3CU3Ec_t4D8FB18A17EB67B14C8322B4D41E34DC1909D779), -1, sizeof(U3CU3Ec_t4D8FB18A17EB67B14C8322B4D41E34DC1909D779_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7047[5] = 
{
	U3CU3Ec_t4D8FB18A17EB67B14C8322B4D41E34DC1909D779_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t4D8FB18A17EB67B14C8322B4D41E34DC1909D779_StaticFields::get_offset_of_U3CU3E9__4_0_1(),
	U3CU3Ec_t4D8FB18A17EB67B14C8322B4D41E34DC1909D779_StaticFields::get_offset_of_U3CU3E9__5_0_2(),
	U3CU3Ec_t4D8FB18A17EB67B14C8322B4D41E34DC1909D779_StaticFields::get_offset_of_U3CU3E9__6_0_3(),
	U3CU3Ec_t4D8FB18A17EB67B14C8322B4D41E34DC1909D779_StaticFields::get_offset_of_U3CU3E9__7_0_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7048 = { sizeof (CopyNonLazyBinder_tC55396C09D2CF16C40164CC4E24D58AC45632D11), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7049 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7050 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7050[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7051 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7051[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7052 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7052[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7053 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7053[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7054 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7054[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7055 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7055[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7056 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7057 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7057[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7058 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7058[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7059 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7059[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7060 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7061 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7061[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7062 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7062[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7063 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7063[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7064 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7065 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7065[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7066 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7066[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7067 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7067[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7068 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7069 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7069[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7070 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7070[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7071 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7071[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7072 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7073 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7073[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7074 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7074[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7075 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7075[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7076 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7077 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7077[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7078 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7078[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7079 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7079[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7080 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7080[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7081 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7081[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7082 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7083 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7083[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7084 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7084[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7085 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7085[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7086 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7087 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7087[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7088 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7088[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7089 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7089[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7090 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7091 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7091[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7092 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7092[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7093 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7093[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7094 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7095 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7095[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7096 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7096[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7097 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7097[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7098 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7099 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7099[2] = 
{
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
