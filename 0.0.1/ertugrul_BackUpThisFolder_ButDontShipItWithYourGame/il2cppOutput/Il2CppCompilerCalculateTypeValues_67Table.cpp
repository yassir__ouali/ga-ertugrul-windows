﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Facebook.Unity.AccessToken
struct AccessToken_t70C8F33A230D47D1D766DD0EEEE7F22E0D89DA6E;
// Facebook.Unity.AsyncRequestString
struct AsyncRequestString_t33F30E800180F52474E4C86249DF12C85BFA9AFB;
// Facebook.Unity.CallbackManager
struct CallbackManager_t3ED952CB7A3597AAC84A6A973ADAE06A3E832BF8;
// Facebook.Unity.Editor.Dialogs.MockLoginDialog
struct MockLoginDialog_t4B515228F0AE861F1C4067E1BC5AC26EE210F5B8;
// Facebook.Unity.Editor.IEditorWrapper
struct IEditorWrapper_tC274D44D390EBDF2179E325E17FD6E4EAB8BDD87;
// Facebook.Unity.FB/OnDLLLoaded
struct OnDLLLoaded_tB74B93A2FF5819C30C822CB7B88B674DCF83DCD3;
// Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGraphResult>
struct FacebookDelegate_1_tF029640B8495BD2CE458F81E993CBE125C757EB5;
// Facebook.Unity.Gameroom.GameroomFacebook/OnComplete
struct OnComplete_t6617755663754FE5E8E8F946F5639627C5589513;
// Facebook.Unity.Gameroom.GameroomFacebookGameObject
struct GameroomFacebookGameObject_t88ED0BA9A30FDBD5312F126A32F8147B95191A0E;
// Facebook.Unity.Gameroom.IGameroomWrapper
struct IGameroomWrapper_t5F225D45B2110557830DE40DE4D98BCC0E58D06A;
// Facebook.Unity.HideUnityDelegate
struct HideUnityDelegate_tFFB6D624086048B6FFFB85D9BC1946920684385B;
// Facebook.Unity.IAsyncRequestStringWrapper
struct IAsyncRequestStringWrapper_tF63688F0A04A43F92DF4B4DBA355963EC80CD167;
// Facebook.Unity.IFacebook
struct IFacebook_tD90C8D6ABB9A39E15C63BD589D8461E0D967E805;
// Facebook.Unity.IFacebookCallbackHandler
struct IFacebookCallbackHandler_t1E1E1431319F01401B6249913E3F8B17C02A1067;
// Facebook.Unity.IFacebookImplementation
struct IFacebookImplementation_tCFB270E1A589730204AC2530B5A8524F5ABD905A;
// Facebook.Unity.IFacebookLogger
struct IFacebookLogger_t1DC4993395D6D31BD6DE87593FA55D470F093EBA;
// Facebook.Unity.InitDelegate
struct InitDelegate_tD9FF06C3949FD06E97E275F72D8F43AF4F496F47;
// Facebook.Unity.ResultContainer
struct ResultContainer_t19BFBF119CCF533782EEA08E523DCFB899453602;
// Facebook.Unity.Utilities/Callback`1<Facebook.Unity.ResultContainer>
struct Callback_1_t9200E2A35184E330F144E9195F3882E126DA2887;
// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.IDictionary`2<System.String,System.Object>
struct IDictionary_2_t1E5681AFFD86E8189077B1EE929272E2AF245A91;
// System.Collections.Generic.IDictionary`2<System.String,System.String>
struct IDictionary_2_t8D4B47914EFD2300DFBC7D9626F3D538CFA7CA53;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_t31EF1520A3A805598500BB6033C14ABDA7116D5E;
// System.Collections.Generic.IList`1<System.Object>
struct IList_1_tE09735A322C3B17000EF4E4BC8026FEDEB7B0D9B;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Func`2<System.Object,System.String>
struct Func_2_t44B347E67E515867D995E8BD5EFD67FA88CE53CF;
// System.Func`2<System.String,System.Boolean>
struct Func_2_t3AD4B0F443BFD399C4AC2D6EE99FFE3BC0970017;
// System.Globalization.NumberFormatInfo
struct NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.IO.StringReader
struct StringReader_t3095DEB3D26F40D1A7F9B76835D80AFE70E47E12;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.Uri
struct Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.GUIStyle
struct GUIStyle_t671F175A201A19166385EE3392292A5F50070572;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C;
// UnityEngine.WWW
struct WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664;
// UnityEngine.WWWForm
struct WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24;

struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;



#ifndef U3CMODULEU3E_T85E142A41EE1971B3F6082EB28F5293289597ACA_H
#define U3CMODULEU3E_T85E142A41EE1971B3F6082EB28F5293289597ACA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t85E142A41EE1971B3F6082EB28F5293289597ACA 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T85E142A41EE1971B3F6082EB28F5293289597ACA_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef JSON_T3B64DF1F5718A3803F0A8B3F78D47C0E4CA66173_H
#define JSON_T3B64DF1F5718A3803F0A8B3F78D47C0E4CA66173_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.MiniJSON.Json
struct  Json_t3B64DF1F5718A3803F0A8B3F78D47C0E4CA66173  : public RuntimeObject
{
public:

public:
};

struct Json_t3B64DF1F5718A3803F0A8B3F78D47C0E4CA66173_StaticFields
{
public:
	// System.Globalization.NumberFormatInfo Facebook.MiniJSON.Json::numberFormat
	NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8 * ___numberFormat_0;

public:
	inline static int32_t get_offset_of_numberFormat_0() { return static_cast<int32_t>(offsetof(Json_t3B64DF1F5718A3803F0A8B3F78D47C0E4CA66173_StaticFields, ___numberFormat_0)); }
	inline NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8 * get_numberFormat_0() const { return ___numberFormat_0; }
	inline NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8 ** get_address_of_numberFormat_0() { return &___numberFormat_0; }
	inline void set_numberFormat_0(NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8 * value)
	{
		___numberFormat_0 = value;
		Il2CppCodeGenWriteBarrier((&___numberFormat_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSON_T3B64DF1F5718A3803F0A8B3F78D47C0E4CA66173_H
#ifndef PARSER_T3F8B8E817A8EC72214A36DFD774A63FBBAD022A7_H
#define PARSER_T3F8B8E817A8EC72214A36DFD774A63FBBAD022A7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.MiniJSON.Json_Parser
struct  Parser_t3F8B8E817A8EC72214A36DFD774A63FBBAD022A7  : public RuntimeObject
{
public:
	// System.IO.StringReader Facebook.MiniJSON.Json_Parser::json
	StringReader_t3095DEB3D26F40D1A7F9B76835D80AFE70E47E12 * ___json_0;

public:
	inline static int32_t get_offset_of_json_0() { return static_cast<int32_t>(offsetof(Parser_t3F8B8E817A8EC72214A36DFD774A63FBBAD022A7, ___json_0)); }
	inline StringReader_t3095DEB3D26F40D1A7F9B76835D80AFE70E47E12 * get_json_0() const { return ___json_0; }
	inline StringReader_t3095DEB3D26F40D1A7F9B76835D80AFE70E47E12 ** get_address_of_json_0() { return &___json_0; }
	inline void set_json_0(StringReader_t3095DEB3D26F40D1A7F9B76835D80AFE70E47E12 * value)
	{
		___json_0 = value;
		Il2CppCodeGenWriteBarrier((&___json_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARSER_T3F8B8E817A8EC72214A36DFD774A63FBBAD022A7_H
#ifndef SERIALIZER_T9A570CD7E2FB71A1074B7231DD16C94BE5A7BBAB_H
#define SERIALIZER_T9A570CD7E2FB71A1074B7231DD16C94BE5A7BBAB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.MiniJSON.Json_Serializer
struct  Serializer_t9A570CD7E2FB71A1074B7231DD16C94BE5A7BBAB  : public RuntimeObject
{
public:
	// System.Text.StringBuilder Facebook.MiniJSON.Json_Serializer::builder
	StringBuilder_t * ___builder_0;

public:
	inline static int32_t get_offset_of_builder_0() { return static_cast<int32_t>(offsetof(Serializer_t9A570CD7E2FB71A1074B7231DD16C94BE5A7BBAB, ___builder_0)); }
	inline StringBuilder_t * get_builder_0() const { return ___builder_0; }
	inline StringBuilder_t ** get_address_of_builder_0() { return &___builder_0; }
	inline void set_builder_0(StringBuilder_t * value)
	{
		___builder_0 = value;
		Il2CppCodeGenWriteBarrier((&___builder_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZER_T9A570CD7E2FB71A1074B7231DD16C94BE5A7BBAB_H
#ifndef U3CSTARTU3ED__9_T11FBA837E020FE9F38CE8B3AC18532C39AFB1E47_H
#define U3CSTARTU3ED__9_T11FBA837E020FE9F38CE8B3AC18532C39AFB1E47_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.AsyncRequestString_<Start>d__9
struct  U3CStartU3Ed__9_t11FBA837E020FE9F38CE8B3AC18532C39AFB1E47  : public RuntimeObject
{
public:
	// System.Int32 Facebook.Unity.AsyncRequestString_<Start>d__9::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Facebook.Unity.AsyncRequestString_<Start>d__9::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Facebook.Unity.AsyncRequestString Facebook.Unity.AsyncRequestString_<Start>d__9::<>4__this
	AsyncRequestString_t33F30E800180F52474E4C86249DF12C85BFA9AFB * ___U3CU3E4__this_2;
	// UnityEngine.WWW Facebook.Unity.AsyncRequestString_<Start>d__9::<www>5__2
	WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * ___U3CwwwU3E5__2_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__9_t11FBA837E020FE9F38CE8B3AC18532C39AFB1E47, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__9_t11FBA837E020FE9F38CE8B3AC18532C39AFB1E47, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__9_t11FBA837E020FE9F38CE8B3AC18532C39AFB1E47, ___U3CU3E4__this_2)); }
	inline AsyncRequestString_t33F30E800180F52474E4C86249DF12C85BFA9AFB * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline AsyncRequestString_t33F30E800180F52474E4C86249DF12C85BFA9AFB ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(AsyncRequestString_t33F30E800180F52474E4C86249DF12C85BFA9AFB * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__9_t11FBA837E020FE9F38CE8B3AC18532C39AFB1E47, ___U3CwwwU3E5__2_3)); }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * get_U3CwwwU3E5__2_3() const { return ___U3CwwwU3E5__2_3; }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 ** get_address_of_U3CwwwU3E5__2_3() { return &___U3CwwwU3E5__2_3; }
	inline void set_U3CwwwU3E5__2_3(WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * value)
	{
		___U3CwwwU3E5__2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E5__2_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3ED__9_T11FBA837E020FE9F38CE8B3AC18532C39AFB1E47_H
#ifndef ASYNCREQUESTSTRINGWRAPPER_TC819E49764AFCD1846BD890E46A836429B314072_H
#define ASYNCREQUESTSTRINGWRAPPER_TC819E49764AFCD1846BD890E46A836429B314072_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.AsyncRequestStringWrapper
struct  AsyncRequestStringWrapper_tC819E49764AFCD1846BD890E46A836429B314072  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCREQUESTSTRINGWRAPPER_TC819E49764AFCD1846BD890E46A836429B314072_H
#ifndef CALLBACKMANAGER_T3ED952CB7A3597AAC84A6A973ADAE06A3E832BF8_H
#define CALLBACKMANAGER_T3ED952CB7A3597AAC84A6A973ADAE06A3E832BF8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.CallbackManager
struct  CallbackManager_t3ED952CB7A3597AAC84A6A973ADAE06A3E832BF8  : public RuntimeObject
{
public:
	// System.Collections.Generic.IDictionary`2<System.String,System.Object> Facebook.Unity.CallbackManager::facebookDelegates
	RuntimeObject* ___facebookDelegates_0;
	// System.Int32 Facebook.Unity.CallbackManager::nextAsyncId
	int32_t ___nextAsyncId_1;

public:
	inline static int32_t get_offset_of_facebookDelegates_0() { return static_cast<int32_t>(offsetof(CallbackManager_t3ED952CB7A3597AAC84A6A973ADAE06A3E832BF8, ___facebookDelegates_0)); }
	inline RuntimeObject* get_facebookDelegates_0() const { return ___facebookDelegates_0; }
	inline RuntimeObject** get_address_of_facebookDelegates_0() { return &___facebookDelegates_0; }
	inline void set_facebookDelegates_0(RuntimeObject* value)
	{
		___facebookDelegates_0 = value;
		Il2CppCodeGenWriteBarrier((&___facebookDelegates_0), value);
	}

	inline static int32_t get_offset_of_nextAsyncId_1() { return static_cast<int32_t>(offsetof(CallbackManager_t3ED952CB7A3597AAC84A6A973ADAE06A3E832BF8, ___nextAsyncId_1)); }
	inline int32_t get_nextAsyncId_1() const { return ___nextAsyncId_1; }
	inline int32_t* get_address_of_nextAsyncId_1() { return &___nextAsyncId_1; }
	inline void set_nextAsyncId_1(int32_t value)
	{
		___nextAsyncId_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CALLBACKMANAGER_T3ED952CB7A3597AAC84A6A973ADAE06A3E832BF8_H
#ifndef CODELESSIAPAUTOLOG_T504E37E47A83CE3165C2B715EBE243A7CAC84ED1_H
#define CODELESSIAPAUTOLOG_T504E37E47A83CE3165C2B715EBE243A7CAC84ED1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.CodelessIAPAutoLog
struct  CodelessIAPAutoLog_t504E37E47A83CE3165C2B715EBE243A7CAC84ED1  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CODELESSIAPAUTOLOG_T504E37E47A83CE3165C2B715EBE243A7CAC84ED1_H
#ifndef COMPONENTFACTORY_T6F32B91728195B8DCC317A76993B99AD9301CAD3_H
#define COMPONENTFACTORY_T6F32B91728195B8DCC317A76993B99AD9301CAD3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.ComponentFactory
struct  ComponentFactory_t6F32B91728195B8DCC317A76993B99AD9301CAD3  : public RuntimeObject
{
public:

public:
};

struct ComponentFactory_t6F32B91728195B8DCC317A76993B99AD9301CAD3_StaticFields
{
public:
	// UnityEngine.GameObject Facebook.Unity.ComponentFactory::facebookGameObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___facebookGameObject_0;

public:
	inline static int32_t get_offset_of_facebookGameObject_0() { return static_cast<int32_t>(offsetof(ComponentFactory_t6F32B91728195B8DCC317A76993B99AD9301CAD3_StaticFields, ___facebookGameObject_0)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_facebookGameObject_0() const { return ___facebookGameObject_0; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_facebookGameObject_0() { return &___facebookGameObject_0; }
	inline void set_facebookGameObject_0(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___facebookGameObject_0 = value;
		Il2CppCodeGenWriteBarrier((&___facebookGameObject_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENTFACTORY_T6F32B91728195B8DCC317A76993B99AD9301CAD3_H
#ifndef U3CU3EC__DISPLAYCLASS4_0_T89BFEA810AF32D3E8BE3B52E1060F30E3883E618_H
#define U3CU3EC__DISPLAYCLASS4_0_T89BFEA810AF32D3E8BE3B52E1060F30E3883E618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Editor.Dialogs.MockLoginDialog_<>c__DisplayClass4_0
struct  U3CU3Ec__DisplayClass4_0_t89BFEA810AF32D3E8BE3B52E1060F30E3883E618  : public RuntimeObject
{
public:
	// System.String Facebook.Unity.Editor.Dialogs.MockLoginDialog_<>c__DisplayClass4_0::facebookID
	String_t* ___facebookID_0;
	// Facebook.Unity.Editor.Dialogs.MockLoginDialog Facebook.Unity.Editor.Dialogs.MockLoginDialog_<>c__DisplayClass4_0::<>4__this
	MockLoginDialog_t4B515228F0AE861F1C4067E1BC5AC26EE210F5B8 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_facebookID_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_t89BFEA810AF32D3E8BE3B52E1060F30E3883E618, ___facebookID_0)); }
	inline String_t* get_facebookID_0() const { return ___facebookID_0; }
	inline String_t** get_address_of_facebookID_0() { return &___facebookID_0; }
	inline void set_facebookID_0(String_t* value)
	{
		___facebookID_0 = value;
		Il2CppCodeGenWriteBarrier((&___facebookID_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_t89BFEA810AF32D3E8BE3B52E1060F30E3883E618, ___U3CU3E4__this_1)); }
	inline MockLoginDialog_t4B515228F0AE861F1C4067E1BC5AC26EE210F5B8 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline MockLoginDialog_t4B515228F0AE861F1C4067E1BC5AC26EE210F5B8 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(MockLoginDialog_t4B515228F0AE861F1C4067E1BC5AC26EE210F5B8 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS4_0_T89BFEA810AF32D3E8BE3B52E1060F30E3883E618_H
#ifndef EDITORWRAPPER_TF0E63F8963D378246332CA72407B583137FD4CDC_H
#define EDITORWRAPPER_TF0E63F8963D378246332CA72407B583137FD4CDC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Editor.EditorWrapper
struct  EditorWrapper_tF0E63F8963D378246332CA72407B583137FD4CDC  : public RuntimeObject
{
public:
	// Facebook.Unity.IFacebookCallbackHandler Facebook.Unity.Editor.EditorWrapper::callbackHandler
	RuntimeObject* ___callbackHandler_0;

public:
	inline static int32_t get_offset_of_callbackHandler_0() { return static_cast<int32_t>(offsetof(EditorWrapper_tF0E63F8963D378246332CA72407B583137FD4CDC, ___callbackHandler_0)); }
	inline RuntimeObject* get_callbackHandler_0() const { return ___callbackHandler_0; }
	inline RuntimeObject** get_address_of_callbackHandler_0() { return &___callbackHandler_0; }
	inline void set_callbackHandler_0(RuntimeObject* value)
	{
		___callbackHandler_0 = value;
		Il2CppCodeGenWriteBarrier((&___callbackHandler_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDITORWRAPPER_TF0E63F8963D378246332CA72407B583137FD4CDC_H
#ifndef U3CU3EC__DISPLAYCLASS35_0_T7E602B315C674DD369ABB7D1622626680D7D6442_H
#define U3CU3EC__DISPLAYCLASS35_0_T7E602B315C674DD369ABB7D1622626680D7D6442_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.FB_<>c__DisplayClass35_0
struct  U3CU3Ec__DisplayClass35_0_t7E602B315C674DD369ABB7D1622626680D7D6442  : public RuntimeObject
{
public:
	// Facebook.Unity.InitDelegate Facebook.Unity.FB_<>c__DisplayClass35_0::onInitComplete
	InitDelegate_tD9FF06C3949FD06E97E275F72D8F43AF4F496F47 * ___onInitComplete_0;
	// System.String Facebook.Unity.FB_<>c__DisplayClass35_0::appId
	String_t* ___appId_1;
	// System.Boolean Facebook.Unity.FB_<>c__DisplayClass35_0::cookie
	bool ___cookie_2;
	// System.Boolean Facebook.Unity.FB_<>c__DisplayClass35_0::logging
	bool ___logging_3;
	// System.Boolean Facebook.Unity.FB_<>c__DisplayClass35_0::status
	bool ___status_4;
	// System.Boolean Facebook.Unity.FB_<>c__DisplayClass35_0::xfbml
	bool ___xfbml_5;
	// System.String Facebook.Unity.FB_<>c__DisplayClass35_0::authResponse
	String_t* ___authResponse_6;
	// System.Boolean Facebook.Unity.FB_<>c__DisplayClass35_0::frictionlessRequests
	bool ___frictionlessRequests_7;
	// System.String Facebook.Unity.FB_<>c__DisplayClass35_0::javascriptSDKLocale
	String_t* ___javascriptSDKLocale_8;
	// Facebook.Unity.HideUnityDelegate Facebook.Unity.FB_<>c__DisplayClass35_0::onHideUnity
	HideUnityDelegate_tFFB6D624086048B6FFFB85D9BC1946920684385B * ___onHideUnity_9;

public:
	inline static int32_t get_offset_of_onInitComplete_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass35_0_t7E602B315C674DD369ABB7D1622626680D7D6442, ___onInitComplete_0)); }
	inline InitDelegate_tD9FF06C3949FD06E97E275F72D8F43AF4F496F47 * get_onInitComplete_0() const { return ___onInitComplete_0; }
	inline InitDelegate_tD9FF06C3949FD06E97E275F72D8F43AF4F496F47 ** get_address_of_onInitComplete_0() { return &___onInitComplete_0; }
	inline void set_onInitComplete_0(InitDelegate_tD9FF06C3949FD06E97E275F72D8F43AF4F496F47 * value)
	{
		___onInitComplete_0 = value;
		Il2CppCodeGenWriteBarrier((&___onInitComplete_0), value);
	}

	inline static int32_t get_offset_of_appId_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass35_0_t7E602B315C674DD369ABB7D1622626680D7D6442, ___appId_1)); }
	inline String_t* get_appId_1() const { return ___appId_1; }
	inline String_t** get_address_of_appId_1() { return &___appId_1; }
	inline void set_appId_1(String_t* value)
	{
		___appId_1 = value;
		Il2CppCodeGenWriteBarrier((&___appId_1), value);
	}

	inline static int32_t get_offset_of_cookie_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass35_0_t7E602B315C674DD369ABB7D1622626680D7D6442, ___cookie_2)); }
	inline bool get_cookie_2() const { return ___cookie_2; }
	inline bool* get_address_of_cookie_2() { return &___cookie_2; }
	inline void set_cookie_2(bool value)
	{
		___cookie_2 = value;
	}

	inline static int32_t get_offset_of_logging_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass35_0_t7E602B315C674DD369ABB7D1622626680D7D6442, ___logging_3)); }
	inline bool get_logging_3() const { return ___logging_3; }
	inline bool* get_address_of_logging_3() { return &___logging_3; }
	inline void set_logging_3(bool value)
	{
		___logging_3 = value;
	}

	inline static int32_t get_offset_of_status_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass35_0_t7E602B315C674DD369ABB7D1622626680D7D6442, ___status_4)); }
	inline bool get_status_4() const { return ___status_4; }
	inline bool* get_address_of_status_4() { return &___status_4; }
	inline void set_status_4(bool value)
	{
		___status_4 = value;
	}

	inline static int32_t get_offset_of_xfbml_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass35_0_t7E602B315C674DD369ABB7D1622626680D7D6442, ___xfbml_5)); }
	inline bool get_xfbml_5() const { return ___xfbml_5; }
	inline bool* get_address_of_xfbml_5() { return &___xfbml_5; }
	inline void set_xfbml_5(bool value)
	{
		___xfbml_5 = value;
	}

	inline static int32_t get_offset_of_authResponse_6() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass35_0_t7E602B315C674DD369ABB7D1622626680D7D6442, ___authResponse_6)); }
	inline String_t* get_authResponse_6() const { return ___authResponse_6; }
	inline String_t** get_address_of_authResponse_6() { return &___authResponse_6; }
	inline void set_authResponse_6(String_t* value)
	{
		___authResponse_6 = value;
		Il2CppCodeGenWriteBarrier((&___authResponse_6), value);
	}

	inline static int32_t get_offset_of_frictionlessRequests_7() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass35_0_t7E602B315C674DD369ABB7D1622626680D7D6442, ___frictionlessRequests_7)); }
	inline bool get_frictionlessRequests_7() const { return ___frictionlessRequests_7; }
	inline bool* get_address_of_frictionlessRequests_7() { return &___frictionlessRequests_7; }
	inline void set_frictionlessRequests_7(bool value)
	{
		___frictionlessRequests_7 = value;
	}

	inline static int32_t get_offset_of_javascriptSDKLocale_8() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass35_0_t7E602B315C674DD369ABB7D1622626680D7D6442, ___javascriptSDKLocale_8)); }
	inline String_t* get_javascriptSDKLocale_8() const { return ___javascriptSDKLocale_8; }
	inline String_t** get_address_of_javascriptSDKLocale_8() { return &___javascriptSDKLocale_8; }
	inline void set_javascriptSDKLocale_8(String_t* value)
	{
		___javascriptSDKLocale_8 = value;
		Il2CppCodeGenWriteBarrier((&___javascriptSDKLocale_8), value);
	}

	inline static int32_t get_offset_of_onHideUnity_9() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass35_0_t7E602B315C674DD369ABB7D1622626680D7D6442, ___onHideUnity_9)); }
	inline HideUnityDelegate_tFFB6D624086048B6FFFB85D9BC1946920684385B * get_onHideUnity_9() const { return ___onHideUnity_9; }
	inline HideUnityDelegate_tFFB6D624086048B6FFFB85D9BC1946920684385B ** get_address_of_onHideUnity_9() { return &___onHideUnity_9; }
	inline void set_onHideUnity_9(HideUnityDelegate_tFFB6D624086048B6FFFB85D9BC1946920684385B * value)
	{
		___onHideUnity_9 = value;
		Il2CppCodeGenWriteBarrier((&___onHideUnity_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS35_0_T7E602B315C674DD369ABB7D1622626680D7D6442_H
#ifndef CANVAS_T72D35BCBD47DD5CBD1E3EA17576E7E67B464A3B8_H
#define CANVAS_T72D35BCBD47DD5CBD1E3EA17576E7E67B464A3B8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.FB_Canvas
struct  Canvas_t72D35BCBD47DD5CBD1E3EA17576E7E67B464A3B8  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANVAS_T72D35BCBD47DD5CBD1E3EA17576E7E67B464A3B8_H
#ifndef MOBILE_TF2F83CA4B7DB9B42FF98DD3D89AF275EF68F24BC_H
#define MOBILE_TF2F83CA4B7DB9B42FF98DD3D89AF275EF68F24BC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.FB_Mobile
struct  Mobile_tF2F83CA4B7DB9B42FF98DD3D89AF275EF68F24BC  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOBILE_TF2F83CA4B7DB9B42FF98DD3D89AF275EF68F24BC_H
#ifndef FBUNITYUTILITY_TDE1EAB5E50BF646A5D2ED896C45CA031C976DDE1_H
#define FBUNITYUTILITY_TDE1EAB5E50BF646A5D2ED896C45CA031C976DDE1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.FBUnityUtility
struct  FBUnityUtility_tDE1EAB5E50BF646A5D2ED896C45CA031C976DDE1  : public RuntimeObject
{
public:

public:
};

struct FBUnityUtility_tDE1EAB5E50BF646A5D2ED896C45CA031C976DDE1_StaticFields
{
public:
	// Facebook.Unity.IAsyncRequestStringWrapper Facebook.Unity.FBUnityUtility::asyncRequestStringWrapper
	RuntimeObject* ___asyncRequestStringWrapper_0;

public:
	inline static int32_t get_offset_of_asyncRequestStringWrapper_0() { return static_cast<int32_t>(offsetof(FBUnityUtility_tDE1EAB5E50BF646A5D2ED896C45CA031C976DDE1_StaticFields, ___asyncRequestStringWrapper_0)); }
	inline RuntimeObject* get_asyncRequestStringWrapper_0() const { return ___asyncRequestStringWrapper_0; }
	inline RuntimeObject** get_address_of_asyncRequestStringWrapper_0() { return &___asyncRequestStringWrapper_0; }
	inline void set_asyncRequestStringWrapper_0(RuntimeObject* value)
	{
		___asyncRequestStringWrapper_0 = value;
		Il2CppCodeGenWriteBarrier((&___asyncRequestStringWrapper_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FBUNITYUTILITY_TDE1EAB5E50BF646A5D2ED896C45CA031C976DDE1_H
#ifndef FACEBOOKBASE_T89F93EF12353405D2CEFAC7B59E860123FB472C7_H
#define FACEBOOKBASE_T89F93EF12353405D2CEFAC7B59E860123FB472C7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.FacebookBase
struct  FacebookBase_t89F93EF12353405D2CEFAC7B59E860123FB472C7  : public RuntimeObject
{
public:
	// Facebook.Unity.InitDelegate Facebook.Unity.FacebookBase::onInitCompleteDelegate
	InitDelegate_tD9FF06C3949FD06E97E275F72D8F43AF4F496F47 * ___onInitCompleteDelegate_0;
	// System.Boolean Facebook.Unity.FacebookBase::<Initialized>k__BackingField
	bool ___U3CInitializedU3Ek__BackingField_1;
	// Facebook.Unity.CallbackManager Facebook.Unity.FacebookBase::<CallbackManager>k__BackingField
	CallbackManager_t3ED952CB7A3597AAC84A6A973ADAE06A3E832BF8 * ___U3CCallbackManagerU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_onInitCompleteDelegate_0() { return static_cast<int32_t>(offsetof(FacebookBase_t89F93EF12353405D2CEFAC7B59E860123FB472C7, ___onInitCompleteDelegate_0)); }
	inline InitDelegate_tD9FF06C3949FD06E97E275F72D8F43AF4F496F47 * get_onInitCompleteDelegate_0() const { return ___onInitCompleteDelegate_0; }
	inline InitDelegate_tD9FF06C3949FD06E97E275F72D8F43AF4F496F47 ** get_address_of_onInitCompleteDelegate_0() { return &___onInitCompleteDelegate_0; }
	inline void set_onInitCompleteDelegate_0(InitDelegate_tD9FF06C3949FD06E97E275F72D8F43AF4F496F47 * value)
	{
		___onInitCompleteDelegate_0 = value;
		Il2CppCodeGenWriteBarrier((&___onInitCompleteDelegate_0), value);
	}

	inline static int32_t get_offset_of_U3CInitializedU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(FacebookBase_t89F93EF12353405D2CEFAC7B59E860123FB472C7, ___U3CInitializedU3Ek__BackingField_1)); }
	inline bool get_U3CInitializedU3Ek__BackingField_1() const { return ___U3CInitializedU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CInitializedU3Ek__BackingField_1() { return &___U3CInitializedU3Ek__BackingField_1; }
	inline void set_U3CInitializedU3Ek__BackingField_1(bool value)
	{
		___U3CInitializedU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CCallbackManagerU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(FacebookBase_t89F93EF12353405D2CEFAC7B59E860123FB472C7, ___U3CCallbackManagerU3Ek__BackingField_2)); }
	inline CallbackManager_t3ED952CB7A3597AAC84A6A973ADAE06A3E832BF8 * get_U3CCallbackManagerU3Ek__BackingField_2() const { return ___U3CCallbackManagerU3Ek__BackingField_2; }
	inline CallbackManager_t3ED952CB7A3597AAC84A6A973ADAE06A3E832BF8 ** get_address_of_U3CCallbackManagerU3Ek__BackingField_2() { return &___U3CCallbackManagerU3Ek__BackingField_2; }
	inline void set_U3CCallbackManagerU3Ek__BackingField_2(CallbackManager_t3ED952CB7A3597AAC84A6A973ADAE06A3E832BF8 * value)
	{
		___U3CCallbackManagerU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackManagerU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACEBOOKBASE_T89F93EF12353405D2CEFAC7B59E860123FB472C7_H
#ifndef U3CU3EC_T4490DE473F81406EFB0DF8DC550047B9E7D58824_H
#define U3CU3EC_T4490DE473F81406EFB0DF8DC550047B9E7D58824_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.FacebookBase_<>c
struct  U3CU3Ec_t4490DE473F81406EFB0DF8DC550047B9E7D58824  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t4490DE473F81406EFB0DF8DC550047B9E7D58824_StaticFields
{
public:
	// Facebook.Unity.FacebookBase_<>c Facebook.Unity.FacebookBase_<>c::<>9
	U3CU3Ec_t4490DE473F81406EFB0DF8DC550047B9E7D58824 * ___U3CU3E9_0;
	// System.Func`2<System.String,System.Boolean> Facebook.Unity.FacebookBase_<>c::<>9__41_0
	Func_2_t3AD4B0F443BFD399C4AC2D6EE99FFE3BC0970017 * ___U3CU3E9__41_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4490DE473F81406EFB0DF8DC550047B9E7D58824_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t4490DE473F81406EFB0DF8DC550047B9E7D58824 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t4490DE473F81406EFB0DF8DC550047B9E7D58824 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t4490DE473F81406EFB0DF8DC550047B9E7D58824 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__41_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4490DE473F81406EFB0DF8DC550047B9E7D58824_StaticFields, ___U3CU3E9__41_0_1)); }
	inline Func_2_t3AD4B0F443BFD399C4AC2D6EE99FFE3BC0970017 * get_U3CU3E9__41_0_1() const { return ___U3CU3E9__41_0_1; }
	inline Func_2_t3AD4B0F443BFD399C4AC2D6EE99FFE3BC0970017 ** get_address_of_U3CU3E9__41_0_1() { return &___U3CU3E9__41_0_1; }
	inline void set_U3CU3E9__41_0_1(Func_2_t3AD4B0F443BFD399C4AC2D6EE99FFE3BC0970017 * value)
	{
		___U3CU3E9__41_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__41_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T4490DE473F81406EFB0DF8DC550047B9E7D58824_H
#ifndef FACEBOOKLOGGER_T6A0582DEF6D939F2BE21517040545DF936A41BC4_H
#define FACEBOOKLOGGER_T6A0582DEF6D939F2BE21517040545DF936A41BC4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.FacebookLogger
struct  FacebookLogger_t6A0582DEF6D939F2BE21517040545DF936A41BC4  : public RuntimeObject
{
public:

public:
};

struct FacebookLogger_t6A0582DEF6D939F2BE21517040545DF936A41BC4_StaticFields
{
public:
	// Facebook.Unity.IFacebookLogger Facebook.Unity.FacebookLogger::<Instance>k__BackingField
	RuntimeObject* ___U3CInstanceU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(FacebookLogger_t6A0582DEF6D939F2BE21517040545DF936A41BC4_StaticFields, ___U3CInstanceU3Ek__BackingField_0)); }
	inline RuntimeObject* get_U3CInstanceU3Ek__BackingField_0() const { return ___U3CInstanceU3Ek__BackingField_0; }
	inline RuntimeObject** get_address_of_U3CInstanceU3Ek__BackingField_0() { return &___U3CInstanceU3Ek__BackingField_0; }
	inline void set_U3CInstanceU3Ek__BackingField_0(RuntimeObject* value)
	{
		___U3CInstanceU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACEBOOKLOGGER_T6A0582DEF6D939F2BE21517040545DF936A41BC4_H
#ifndef DEBUGLOGGER_TA488557789DB3901B501A9F673800D999F6681F6_H
#define DEBUGLOGGER_TA488557789DB3901B501A9F673800D999F6681F6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.FacebookLogger_DebugLogger
struct  DebugLogger_tA488557789DB3901B501A9F673800D999F6681F6  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGLOGGER_TA488557789DB3901B501A9F673800D999F6681F6_H
#ifndef U3CDELAYEVENTU3ED__1_TCD85F1EE7CE5D7F214560EF1955ECE205BAA4C1D_H
#define U3CDELAYEVENTU3ED__1_TCD85F1EE7CE5D7F214560EF1955ECE205BAA4C1D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.FacebookScheduler_<DelayEvent>d__1
struct  U3CDelayEventU3Ed__1_tCD85F1EE7CE5D7F214560EF1955ECE205BAA4C1D  : public RuntimeObject
{
public:
	// System.Int32 Facebook.Unity.FacebookScheduler_<DelayEvent>d__1::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Facebook.Unity.FacebookScheduler_<DelayEvent>d__1::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Int64 Facebook.Unity.FacebookScheduler_<DelayEvent>d__1::delay
	int64_t ___delay_2;
	// System.Action Facebook.Unity.FacebookScheduler_<DelayEvent>d__1::action
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___action_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CDelayEventU3Ed__1_tCD85F1EE7CE5D7F214560EF1955ECE205BAA4C1D, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CDelayEventU3Ed__1_tCD85F1EE7CE5D7F214560EF1955ECE205BAA4C1D, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_delay_2() { return static_cast<int32_t>(offsetof(U3CDelayEventU3Ed__1_tCD85F1EE7CE5D7F214560EF1955ECE205BAA4C1D, ___delay_2)); }
	inline int64_t get_delay_2() const { return ___delay_2; }
	inline int64_t* get_address_of_delay_2() { return &___delay_2; }
	inline void set_delay_2(int64_t value)
	{
		___delay_2 = value;
	}

	inline static int32_t get_offset_of_action_3() { return static_cast<int32_t>(offsetof(U3CDelayEventU3Ed__1_tCD85F1EE7CE5D7F214560EF1955ECE205BAA4C1D, ___action_3)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_action_3() const { return ___action_3; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_action_3() { return &___action_3; }
	inline void set_action_3(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___action_3 = value;
		Il2CppCodeGenWriteBarrier((&___action_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDELAYEVENTU3ED__1_TCD85F1EE7CE5D7F214560EF1955ECE205BAA4C1D_H
#ifndef FACEBOOKSDKVERSION_T346709522EE2B2B782EAEC96031871CE25FE3FA1_H
#define FACEBOOKSDKVERSION_T346709522EE2B2B782EAEC96031871CE25FE3FA1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.FacebookSdkVersion
struct  FacebookSdkVersion_t346709522EE2B2B782EAEC96031871CE25FE3FA1  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACEBOOKSDKVERSION_T346709522EE2B2B782EAEC96031871CE25FE3FA1_H
#ifndef U3CWAITFORPIPERESPONSEU3ED__4_T45F560A5F0488A30AD17E48606523BDB3E7854FC_H
#define U3CWAITFORPIPERESPONSEU3ED__4_T45F560A5F0488A30AD17E48606523BDB3E7854FC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Gameroom.GameroomFacebookGameObject_<WaitForPipeResponse>d__4
struct  U3CWaitForPipeResponseU3Ed__4_t45F560A5F0488A30AD17E48606523BDB3E7854FC  : public RuntimeObject
{
public:
	// System.Int32 Facebook.Unity.Gameroom.GameroomFacebookGameObject_<WaitForPipeResponse>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Facebook.Unity.Gameroom.GameroomFacebookGameObject_<WaitForPipeResponse>d__4::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Facebook.Unity.Gameroom.GameroomFacebookGameObject Facebook.Unity.Gameroom.GameroomFacebookGameObject_<WaitForPipeResponse>d__4::<>4__this
	GameroomFacebookGameObject_t88ED0BA9A30FDBD5312F126A32F8147B95191A0E * ___U3CU3E4__this_2;
	// Facebook.Unity.Gameroom.GameroomFacebook_OnComplete Facebook.Unity.Gameroom.GameroomFacebookGameObject_<WaitForPipeResponse>d__4::onCompleteDelegate
	OnComplete_t6617755663754FE5E8E8F946F5639627C5589513 * ___onCompleteDelegate_3;
	// System.String Facebook.Unity.Gameroom.GameroomFacebookGameObject_<WaitForPipeResponse>d__4::callbackId
	String_t* ___callbackId_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitForPipeResponseU3Ed__4_t45F560A5F0488A30AD17E48606523BDB3E7854FC, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitForPipeResponseU3Ed__4_t45F560A5F0488A30AD17E48606523BDB3E7854FC, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CWaitForPipeResponseU3Ed__4_t45F560A5F0488A30AD17E48606523BDB3E7854FC, ___U3CU3E4__this_2)); }
	inline GameroomFacebookGameObject_t88ED0BA9A30FDBD5312F126A32F8147B95191A0E * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline GameroomFacebookGameObject_t88ED0BA9A30FDBD5312F126A32F8147B95191A0E ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(GameroomFacebookGameObject_t88ED0BA9A30FDBD5312F126A32F8147B95191A0E * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_onCompleteDelegate_3() { return static_cast<int32_t>(offsetof(U3CWaitForPipeResponseU3Ed__4_t45F560A5F0488A30AD17E48606523BDB3E7854FC, ___onCompleteDelegate_3)); }
	inline OnComplete_t6617755663754FE5E8E8F946F5639627C5589513 * get_onCompleteDelegate_3() const { return ___onCompleteDelegate_3; }
	inline OnComplete_t6617755663754FE5E8E8F946F5639627C5589513 ** get_address_of_onCompleteDelegate_3() { return &___onCompleteDelegate_3; }
	inline void set_onCompleteDelegate_3(OnComplete_t6617755663754FE5E8E8F946F5639627C5589513 * value)
	{
		___onCompleteDelegate_3 = value;
		Il2CppCodeGenWriteBarrier((&___onCompleteDelegate_3), value);
	}

	inline static int32_t get_offset_of_callbackId_4() { return static_cast<int32_t>(offsetof(U3CWaitForPipeResponseU3Ed__4_t45F560A5F0488A30AD17E48606523BDB3E7854FC, ___callbackId_4)); }
	inline String_t* get_callbackId_4() const { return ___callbackId_4; }
	inline String_t** get_address_of_callbackId_4() { return &___callbackId_4; }
	inline void set_callbackId_4(String_t* value)
	{
		___callbackId_4 = value;
		Il2CppCodeGenWriteBarrier((&___callbackId_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWAITFORPIPERESPONSEU3ED__4_T45F560A5F0488A30AD17E48606523BDB3E7854FC_H
#ifndef METHODARGUMENTS_T5BC48FC1AB90B7EF0A09C7CBF20F9F028C18FDFB_H
#define METHODARGUMENTS_T5BC48FC1AB90B7EF0A09C7CBF20F9F028C18FDFB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.MethodArguments
struct  MethodArguments_t5BC48FC1AB90B7EF0A09C7CBF20F9F028C18FDFB  : public RuntimeObject
{
public:
	// System.Collections.Generic.IDictionary`2<System.String,System.Object> Facebook.Unity.MethodArguments::arguments
	RuntimeObject* ___arguments_0;

public:
	inline static int32_t get_offset_of_arguments_0() { return static_cast<int32_t>(offsetof(MethodArguments_t5BC48FC1AB90B7EF0A09C7CBF20F9F028C18FDFB, ___arguments_0)); }
	inline RuntimeObject* get_arguments_0() const { return ___arguments_0; }
	inline RuntimeObject** get_address_of_arguments_0() { return &___arguments_0; }
	inline void set_arguments_0(RuntimeObject* value)
	{
		___arguments_0 = value;
		Il2CppCodeGenWriteBarrier((&___arguments_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METHODARGUMENTS_T5BC48FC1AB90B7EF0A09C7CBF20F9F028C18FDFB_H
#ifndef RESULTCONTAINER_T19BFBF119CCF533782EEA08E523DCFB899453602_H
#define RESULTCONTAINER_T19BFBF119CCF533782EEA08E523DCFB899453602_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.ResultContainer
struct  ResultContainer_t19BFBF119CCF533782EEA08E523DCFB899453602  : public RuntimeObject
{
public:
	// System.String Facebook.Unity.ResultContainer::<RawResult>k__BackingField
	String_t* ___U3CRawResultU3Ek__BackingField_1;
	// System.Collections.Generic.IDictionary`2<System.String,System.Object> Facebook.Unity.ResultContainer::<ResultDictionary>k__BackingField
	RuntimeObject* ___U3CResultDictionaryU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CRawResultU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ResultContainer_t19BFBF119CCF533782EEA08E523DCFB899453602, ___U3CRawResultU3Ek__BackingField_1)); }
	inline String_t* get_U3CRawResultU3Ek__BackingField_1() const { return ___U3CRawResultU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CRawResultU3Ek__BackingField_1() { return &___U3CRawResultU3Ek__BackingField_1; }
	inline void set_U3CRawResultU3Ek__BackingField_1(String_t* value)
	{
		___U3CRawResultU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRawResultU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CResultDictionaryU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ResultContainer_t19BFBF119CCF533782EEA08E523DCFB899453602, ___U3CResultDictionaryU3Ek__BackingField_2)); }
	inline RuntimeObject* get_U3CResultDictionaryU3Ek__BackingField_2() const { return ___U3CResultDictionaryU3Ek__BackingField_2; }
	inline RuntimeObject** get_address_of_U3CResultDictionaryU3Ek__BackingField_2() { return &___U3CResultDictionaryU3Ek__BackingField_2; }
	inline void set_U3CResultDictionaryU3Ek__BackingField_2(RuntimeObject* value)
	{
		___U3CResultDictionaryU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CResultDictionaryU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESULTCONTAINER_T19BFBF119CCF533782EEA08E523DCFB899453602_H
#ifndef UTILITIES_T38892FCEB1858136314024DA4782D94B8806D7BD_H
#define UTILITIES_T38892FCEB1858136314024DA4782D94B8806D7BD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Utilities
struct  Utilities_t38892FCEB1858136314024DA4782D94B8806D7BD  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UTILITIES_T38892FCEB1858136314024DA4782D94B8806D7BD_H
#ifndef U3CU3EC_T02687171AEE0F550EB7319FF63E2F93FF89B7CE2_H
#define U3CU3EC_T02687171AEE0F550EB7319FF63E2F93FF89B7CE2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Utilities_<>c
struct  U3CU3Ec_t02687171AEE0F550EB7319FF63E2F93FF89B7CE2  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t02687171AEE0F550EB7319FF63E2F93FF89B7CE2_StaticFields
{
public:
	// Facebook.Unity.Utilities_<>c Facebook.Unity.Utilities_<>c::<>9
	U3CU3Ec_t02687171AEE0F550EB7319FF63E2F93FF89B7CE2 * ___U3CU3E9_0;
	// System.Func`2<System.Object,System.String> Facebook.Unity.Utilities_<>c::<>9__18_0
	Func_2_t44B347E67E515867D995E8BD5EFD67FA88CE53CF * ___U3CU3E9__18_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t02687171AEE0F550EB7319FF63E2F93FF89B7CE2_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t02687171AEE0F550EB7319FF63E2F93FF89B7CE2 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t02687171AEE0F550EB7319FF63E2F93FF89B7CE2 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t02687171AEE0F550EB7319FF63E2F93FF89B7CE2 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__18_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t02687171AEE0F550EB7319FF63E2F93FF89B7CE2_StaticFields, ___U3CU3E9__18_0_1)); }
	inline Func_2_t44B347E67E515867D995E8BD5EFD67FA88CE53CF * get_U3CU3E9__18_0_1() const { return ___U3CU3E9__18_0_1; }
	inline Func_2_t44B347E67E515867D995E8BD5EFD67FA88CE53CF ** get_address_of_U3CU3E9__18_0_1() { return &___U3CU3E9__18_0_1; }
	inline void set_U3CU3E9__18_0_1(Func_2_t44B347E67E515867D995E8BD5EFD67FA88CE53CF * value)
	{
		___U3CU3E9__18_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__18_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T02687171AEE0F550EB7319FF63E2F93FF89B7CE2_H
#ifndef ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#define ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef POSTCONSTRUCTORATTRIBUTE_T4CEF1EC49C810198695BAA93D198FFE623EF447D_H
#define POSTCONSTRUCTORATTRIBUTE_T4CEF1EC49C810198695BAA93D198FFE623EF447D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.CodeGeneration.Attributes.PostConstructorAttribute
struct  PostConstructorAttribute_t4CEF1EC49C810198695BAA93D198FFE623EF447D  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTCONSTRUCTORATTRIBUTE_T4CEF1EC49C810198695BAA93D198FFE623EF447D_H
#ifndef UNIQUEATTRIBUTE_TA13A1C0849F610545E56A1E177C294CCCDDE8631_H
#define UNIQUEATTRIBUTE_TA13A1C0849F610545E56A1E177C294CCCDDE8631_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.CodeGeneration.Attributes.UniqueAttribute
struct  UniqueAttribute_tA13A1C0849F610545E56A1E177C294CCCDDE8631  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIQUEATTRIBUTE_TA13A1C0849F610545E56A1E177C294CCCDDE8631_H
#ifndef GAMEROOMFACEBOOK_T7C9C84A6698D3491CEC1C6AED8752AD52C85233A_H
#define GAMEROOMFACEBOOK_T7C9C84A6698D3491CEC1C6AED8752AD52C85233A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Gameroom.GameroomFacebook
struct  GameroomFacebook_t7C9C84A6698D3491CEC1C6AED8752AD52C85233A  : public FacebookBase_t89F93EF12353405D2CEFAC7B59E860123FB472C7
{
public:
	// System.String Facebook.Unity.Gameroom.GameroomFacebook::appId
	String_t* ___appId_3;
	// Facebook.Unity.Gameroom.IGameroomWrapper Facebook.Unity.Gameroom.GameroomFacebook::gameroomWrapper
	RuntimeObject* ___gameroomWrapper_4;
	// System.Boolean Facebook.Unity.Gameroom.GameroomFacebook::<LimitEventUsage>k__BackingField
	bool ___U3CLimitEventUsageU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_appId_3() { return static_cast<int32_t>(offsetof(GameroomFacebook_t7C9C84A6698D3491CEC1C6AED8752AD52C85233A, ___appId_3)); }
	inline String_t* get_appId_3() const { return ___appId_3; }
	inline String_t** get_address_of_appId_3() { return &___appId_3; }
	inline void set_appId_3(String_t* value)
	{
		___appId_3 = value;
		Il2CppCodeGenWriteBarrier((&___appId_3), value);
	}

	inline static int32_t get_offset_of_gameroomWrapper_4() { return static_cast<int32_t>(offsetof(GameroomFacebook_t7C9C84A6698D3491CEC1C6AED8752AD52C85233A, ___gameroomWrapper_4)); }
	inline RuntimeObject* get_gameroomWrapper_4() const { return ___gameroomWrapper_4; }
	inline RuntimeObject** get_address_of_gameroomWrapper_4() { return &___gameroomWrapper_4; }
	inline void set_gameroomWrapper_4(RuntimeObject* value)
	{
		___gameroomWrapper_4 = value;
		Il2CppCodeGenWriteBarrier((&___gameroomWrapper_4), value);
	}

	inline static int32_t get_offset_of_U3CLimitEventUsageU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(GameroomFacebook_t7C9C84A6698D3491CEC1C6AED8752AD52C85233A, ___U3CLimitEventUsageU3Ek__BackingField_5)); }
	inline bool get_U3CLimitEventUsageU3Ek__BackingField_5() const { return ___U3CLimitEventUsageU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CLimitEventUsageU3Ek__BackingField_5() { return &___U3CLimitEventUsageU3Ek__BackingField_5; }
	inline void set_U3CLimitEventUsageU3Ek__BackingField_5(bool value)
	{
		___U3CLimitEventUsageU3Ek__BackingField_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEROOMFACEBOOK_T7C9C84A6698D3491CEC1C6AED8752AD52C85233A_H
#ifndef BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#define BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifndef DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#define DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 
{
public:
	// System.UInt64 System.DateTime::dateData
	uint64_t ___dateData_44;

public:
	inline static int32_t get_offset_of_dateData_44() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132, ___dateData_44)); }
	inline uint64_t get_dateData_44() const { return ___dateData_44; }
	inline uint64_t* get_address_of_dateData_44() { return &___dateData_44; }
	inline void set_dateData_44(uint64_t value)
	{
		___dateData_44 = value;
	}
};

struct DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields
{
public:
	// System.Int32[] System.DateTime::DaysToMonth365
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth365_29;
	// System.Int32[] System.DateTime::DaysToMonth366
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth366_30;
	// System.DateTime System.DateTime::MinValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MinValue_31;
	// System.DateTime System.DateTime::MaxValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MaxValue_32;

public:
	inline static int32_t get_offset_of_DaysToMonth365_29() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth365_29)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth365_29() const { return ___DaysToMonth365_29; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth365_29() { return &___DaysToMonth365_29; }
	inline void set_DaysToMonth365_29(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth365_29 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth365_29), value);
	}

	inline static int32_t get_offset_of_DaysToMonth366_30() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth366_30)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth366_30() const { return ___DaysToMonth366_30; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth366_30() { return &___DaysToMonth366_30; }
	inline void set_DaysToMonth366_30(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth366_30 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth366_30), value);
	}

	inline static int32_t get_offset_of_MinValue_31() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MinValue_31)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MinValue_31() const { return ___MinValue_31; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MinValue_31() { return &___MinValue_31; }
	inline void set_MinValue_31(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MinValue_31 = value;
	}

	inline static int32_t get_offset_of_MaxValue_32() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MaxValue_32)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MaxValue_32() const { return ___MaxValue_32; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MaxValue_32() { return &___MaxValue_32; }
	inline void set_MaxValue_32(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MaxValue_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef NULLABLE_1_T8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5_H
#define NULLABLE_1_T8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Int64>
struct  Nullable_1_t8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5 
{
public:
	// T System.Nullable`1::value
	int64_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5, ___value_0)); }
	inline int64_t get_value_0() const { return ___value_0; }
	inline int64_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int64_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#define RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t35B976DE901B5423C11705E156938EA27AB402CE 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#ifndef EVENTTARGET_T116BD7DA9A939B54477A9F429106EC67EB49A6C2_H
#define EVENTTARGET_T116BD7DA9A939B54477A9F429106EC67EB49A6C2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.CodeGeneration.Attributes.EventTarget
struct  EventTarget_t116BD7DA9A939B54477A9F429106EC67EB49A6C2 
{
public:
	// System.Int32 Entitas.CodeGeneration.Attributes.EventTarget::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EventTarget_t116BD7DA9A939B54477A9F429106EC67EB49A6C2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTTARGET_T116BD7DA9A939B54477A9F429106EC67EB49A6C2_H
#ifndef EVENTTYPE_T40A4EAA6A23F7288106E89EDE2610C86746758A3_H
#define EVENTTYPE_T40A4EAA6A23F7288106E89EDE2610C86746758A3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.CodeGeneration.Attributes.EventType
struct  EventType_t40A4EAA6A23F7288106E89EDE2610C86746758A3 
{
public:
	// System.Int32 Entitas.CodeGeneration.Attributes.EventType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EventType_t40A4EAA6A23F7288106E89EDE2610C86746758A3, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTTYPE_T40A4EAA6A23F7288106E89EDE2610C86746758A3_H
#ifndef TOKEN_T04B7126B22EA517C99B68C5E0F65504A5E9C68A7_H
#define TOKEN_T04B7126B22EA517C99B68C5E0F65504A5E9C68A7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.MiniJSON.Json_Parser_TOKEN
struct  TOKEN_t04B7126B22EA517C99B68C5E0F65504A5E9C68A7 
{
public:
	// System.Int32 Facebook.MiniJSON.Json_Parser_TOKEN::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TOKEN_t04B7126B22EA517C99B68C5E0F65504A5E9C68A7, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOKEN_T04B7126B22EA517C99B68C5E0F65504A5E9C68A7_H
#ifndef IFNOTEXIST_T1CEE0E9DC0A1ECA7EA496C9E14AA293C02203F38_H
#define IFNOTEXIST_T1CEE0E9DC0A1ECA7EA496C9E14AA293C02203F38_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.ComponentFactory_IfNotExist
struct  IfNotExist_t1CEE0E9DC0A1ECA7EA496C9E14AA293C02203F38 
{
public:
	// System.Int32 Facebook.Unity.ComponentFactory_IfNotExist::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(IfNotExist_t1CEE0E9DC0A1ECA7EA496C9E14AA293C02203F38, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IFNOTEXIST_T1CEE0E9DC0A1ECA7EA496C9E14AA293C02203F38_H
#ifndef FACEBOOKUNITYPLATFORM_T245EC6C7AD9DA9CDC2A40B725918837E19CEE49B_H
#define FACEBOOKUNITYPLATFORM_T245EC6C7AD9DA9CDC2A40B725918837E19CEE49B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.FacebookUnityPlatform
struct  FacebookUnityPlatform_t245EC6C7AD9DA9CDC2A40B725918837E19CEE49B 
{
public:
	// System.Int32 Facebook.Unity.FacebookUnityPlatform::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FacebookUnityPlatform_t245EC6C7AD9DA9CDC2A40B725918837E19CEE49B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACEBOOKUNITYPLATFORM_T245EC6C7AD9DA9CDC2A40B725918837E19CEE49B_H
#ifndef HTTPMETHOD_T2F212F7FFB35F0534ED41BFA3A8999EC664AC71B_H
#define HTTPMETHOD_T2F212F7FFB35F0534ED41BFA3A8999EC664AC71B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.HttpMethod
struct  HttpMethod_t2F212F7FFB35F0534ED41BFA3A8999EC664AC71B 
{
public:
	// System.Int32 Facebook.Unity.HttpMethod::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HttpMethod_t2F212F7FFB35F0534ED41BFA3A8999EC664AC71B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPMETHOD_T2F212F7FFB35F0534ED41BFA3A8999EC664AC71B_H
#ifndef OGACTIONTYPE_T244E4C9DC93B07294B27F0165CB60E0A84C59D47_H
#define OGACTIONTYPE_T244E4C9DC93B07294B27F0165CB60E0A84C59D47_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.OGActionType
struct  OGActionType_t244E4C9DC93B07294B27F0165CB60E0A84C59D47 
{
public:
	// System.Int32 Facebook.Unity.OGActionType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(OGActionType_t244E4C9DC93B07294B27F0165CB60E0A84C59D47, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OGACTIONTYPE_T244E4C9DC93B07294B27F0165CB60E0A84C59D47_H
#ifndef RESULTBASE_TB17E9A196664F60E8B9B839509E7614CCD921EAB_H
#define RESULTBASE_TB17E9A196664F60E8B9B839509E7614CCD921EAB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.ResultBase
struct  ResultBase_tB17E9A196664F60E8B9B839509E7614CCD921EAB  : public RuntimeObject
{
public:
	// System.String Facebook.Unity.ResultBase::<Error>k__BackingField
	String_t* ___U3CErrorU3Ek__BackingField_0;
	// System.Collections.Generic.IDictionary`2<System.String,System.Object> Facebook.Unity.ResultBase::<ResultDictionary>k__BackingField
	RuntimeObject* ___U3CResultDictionaryU3Ek__BackingField_1;
	// System.String Facebook.Unity.ResultBase::<RawResult>k__BackingField
	String_t* ___U3CRawResultU3Ek__BackingField_2;
	// System.Boolean Facebook.Unity.ResultBase::<Cancelled>k__BackingField
	bool ___U3CCancelledU3Ek__BackingField_3;
	// System.String Facebook.Unity.ResultBase::<CallbackId>k__BackingField
	String_t* ___U3CCallbackIdU3Ek__BackingField_4;
	// System.Nullable`1<System.Int64> Facebook.Unity.ResultBase::<CanvasErrorCode>k__BackingField
	Nullable_1_t8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5  ___U3CCanvasErrorCodeU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CErrorU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ResultBase_tB17E9A196664F60E8B9B839509E7614CCD921EAB, ___U3CErrorU3Ek__BackingField_0)); }
	inline String_t* get_U3CErrorU3Ek__BackingField_0() const { return ___U3CErrorU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CErrorU3Ek__BackingField_0() { return &___U3CErrorU3Ek__BackingField_0; }
	inline void set_U3CErrorU3Ek__BackingField_0(String_t* value)
	{
		___U3CErrorU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CErrorU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CResultDictionaryU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ResultBase_tB17E9A196664F60E8B9B839509E7614CCD921EAB, ___U3CResultDictionaryU3Ek__BackingField_1)); }
	inline RuntimeObject* get_U3CResultDictionaryU3Ek__BackingField_1() const { return ___U3CResultDictionaryU3Ek__BackingField_1; }
	inline RuntimeObject** get_address_of_U3CResultDictionaryU3Ek__BackingField_1() { return &___U3CResultDictionaryU3Ek__BackingField_1; }
	inline void set_U3CResultDictionaryU3Ek__BackingField_1(RuntimeObject* value)
	{
		___U3CResultDictionaryU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CResultDictionaryU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CRawResultU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ResultBase_tB17E9A196664F60E8B9B839509E7614CCD921EAB, ___U3CRawResultU3Ek__BackingField_2)); }
	inline String_t* get_U3CRawResultU3Ek__BackingField_2() const { return ___U3CRawResultU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CRawResultU3Ek__BackingField_2() { return &___U3CRawResultU3Ek__BackingField_2; }
	inline void set_U3CRawResultU3Ek__BackingField_2(String_t* value)
	{
		___U3CRawResultU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRawResultU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CCancelledU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ResultBase_tB17E9A196664F60E8B9B839509E7614CCD921EAB, ___U3CCancelledU3Ek__BackingField_3)); }
	inline bool get_U3CCancelledU3Ek__BackingField_3() const { return ___U3CCancelledU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CCancelledU3Ek__BackingField_3() { return &___U3CCancelledU3Ek__BackingField_3; }
	inline void set_U3CCancelledU3Ek__BackingField_3(bool value)
	{
		___U3CCancelledU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CCallbackIdU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(ResultBase_tB17E9A196664F60E8B9B839509E7614CCD921EAB, ___U3CCallbackIdU3Ek__BackingField_4)); }
	inline String_t* get_U3CCallbackIdU3Ek__BackingField_4() const { return ___U3CCallbackIdU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CCallbackIdU3Ek__BackingField_4() { return &___U3CCallbackIdU3Ek__BackingField_4; }
	inline void set_U3CCallbackIdU3Ek__BackingField_4(String_t* value)
	{
		___U3CCallbackIdU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackIdU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CCanvasErrorCodeU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(ResultBase_tB17E9A196664F60E8B9B839509E7614CCD921EAB, ___U3CCanvasErrorCodeU3Ek__BackingField_5)); }
	inline Nullable_1_t8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5  get_U3CCanvasErrorCodeU3Ek__BackingField_5() const { return ___U3CCanvasErrorCodeU3Ek__BackingField_5; }
	inline Nullable_1_t8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5 * get_address_of_U3CCanvasErrorCodeU3Ek__BackingField_5() { return &___U3CCanvasErrorCodeU3Ek__BackingField_5; }
	inline void set_U3CCanvasErrorCodeU3Ek__BackingField_5(Nullable_1_t8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5  value)
	{
		___U3CCanvasErrorCodeU3Ek__BackingField_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESULTBASE_TB17E9A196664F60E8B9B839509E7614CCD921EAB_H
#ifndef SHAREDIALOGMODE_T40A2D9F39025821336E766D7D4FEA16006545BEB_H
#define SHAREDIALOGMODE_T40A2D9F39025821336E766D7D4FEA16006545BEB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.ShareDialogMode
struct  ShareDialogMode_t40A2D9F39025821336E766D7D4FEA16006545BEB 
{
public:
	// System.Int32 Facebook.Unity.ShareDialogMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ShareDialogMode_t40A2D9F39025821336E766D7D4FEA16006545BEB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHAREDIALOGMODE_T40A2D9F39025821336E766D7D4FEA16006545BEB_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef NULLABLE_1_T3290384E361396B3724B88B498CBF637D7E87B78_H
#define NULLABLE_1_T3290384E361396B3724B88B498CBF637D7E87B78_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.DateTime>
struct  Nullable_1_t3290384E361396B3724B88B498CBF637D7E87B78 
{
public:
	// T System.Nullable`1::value
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t3290384E361396B3724B88B498CBF637D7E87B78, ___value_0)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_value_0() const { return ___value_0; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t3290384E361396B3724B88B498CBF637D7E87B78, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T3290384E361396B3724B88B498CBF637D7E87B78_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef ACCESSTOKEN_T70C8F33A230D47D1D766DD0EEEE7F22E0D89DA6E_H
#define ACCESSTOKEN_T70C8F33A230D47D1D766DD0EEEE7F22E0D89DA6E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.AccessToken
struct  AccessToken_t70C8F33A230D47D1D766DD0EEEE7F22E0D89DA6E  : public RuntimeObject
{
public:
	// System.String Facebook.Unity.AccessToken::<TokenString>k__BackingField
	String_t* ___U3CTokenStringU3Ek__BackingField_1;
	// System.DateTime Facebook.Unity.AccessToken::<ExpirationTime>k__BackingField
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___U3CExpirationTimeU3Ek__BackingField_2;
	// System.Collections.Generic.IEnumerable`1<System.String> Facebook.Unity.AccessToken::<Permissions>k__BackingField
	RuntimeObject* ___U3CPermissionsU3Ek__BackingField_3;
	// System.String Facebook.Unity.AccessToken::<UserId>k__BackingField
	String_t* ___U3CUserIdU3Ek__BackingField_4;
	// System.Nullable`1<System.DateTime> Facebook.Unity.AccessToken::<LastRefresh>k__BackingField
	Nullable_1_t3290384E361396B3724B88B498CBF637D7E87B78  ___U3CLastRefreshU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CTokenStringU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AccessToken_t70C8F33A230D47D1D766DD0EEEE7F22E0D89DA6E, ___U3CTokenStringU3Ek__BackingField_1)); }
	inline String_t* get_U3CTokenStringU3Ek__BackingField_1() const { return ___U3CTokenStringU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CTokenStringU3Ek__BackingField_1() { return &___U3CTokenStringU3Ek__BackingField_1; }
	inline void set_U3CTokenStringU3Ek__BackingField_1(String_t* value)
	{
		___U3CTokenStringU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTokenStringU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CExpirationTimeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AccessToken_t70C8F33A230D47D1D766DD0EEEE7F22E0D89DA6E, ___U3CExpirationTimeU3Ek__BackingField_2)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_U3CExpirationTimeU3Ek__BackingField_2() const { return ___U3CExpirationTimeU3Ek__BackingField_2; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_U3CExpirationTimeU3Ek__BackingField_2() { return &___U3CExpirationTimeU3Ek__BackingField_2; }
	inline void set_U3CExpirationTimeU3Ek__BackingField_2(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___U3CExpirationTimeU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CPermissionsU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AccessToken_t70C8F33A230D47D1D766DD0EEEE7F22E0D89DA6E, ___U3CPermissionsU3Ek__BackingField_3)); }
	inline RuntimeObject* get_U3CPermissionsU3Ek__BackingField_3() const { return ___U3CPermissionsU3Ek__BackingField_3; }
	inline RuntimeObject** get_address_of_U3CPermissionsU3Ek__BackingField_3() { return &___U3CPermissionsU3Ek__BackingField_3; }
	inline void set_U3CPermissionsU3Ek__BackingField_3(RuntimeObject* value)
	{
		___U3CPermissionsU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPermissionsU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CUserIdU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(AccessToken_t70C8F33A230D47D1D766DD0EEEE7F22E0D89DA6E, ___U3CUserIdU3Ek__BackingField_4)); }
	inline String_t* get_U3CUserIdU3Ek__BackingField_4() const { return ___U3CUserIdU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CUserIdU3Ek__BackingField_4() { return &___U3CUserIdU3Ek__BackingField_4; }
	inline void set_U3CUserIdU3Ek__BackingField_4(String_t* value)
	{
		___U3CUserIdU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUserIdU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CLastRefreshU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(AccessToken_t70C8F33A230D47D1D766DD0EEEE7F22E0D89DA6E, ___U3CLastRefreshU3Ek__BackingField_5)); }
	inline Nullable_1_t3290384E361396B3724B88B498CBF637D7E87B78  get_U3CLastRefreshU3Ek__BackingField_5() const { return ___U3CLastRefreshU3Ek__BackingField_5; }
	inline Nullable_1_t3290384E361396B3724B88B498CBF637D7E87B78 * get_address_of_U3CLastRefreshU3Ek__BackingField_5() { return &___U3CLastRefreshU3Ek__BackingField_5; }
	inline void set_U3CLastRefreshU3Ek__BackingField_5(Nullable_1_t3290384E361396B3724B88B498CBF637D7E87B78  value)
	{
		___U3CLastRefreshU3Ek__BackingField_5 = value;
	}
};

struct AccessToken_t70C8F33A230D47D1D766DD0EEEE7F22E0D89DA6E_StaticFields
{
public:
	// Facebook.Unity.AccessToken Facebook.Unity.AccessToken::<CurrentAccessToken>k__BackingField
	AccessToken_t70C8F33A230D47D1D766DD0EEEE7F22E0D89DA6E * ___U3CCurrentAccessTokenU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CCurrentAccessTokenU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AccessToken_t70C8F33A230D47D1D766DD0EEEE7F22E0D89DA6E_StaticFields, ___U3CCurrentAccessTokenU3Ek__BackingField_0)); }
	inline AccessToken_t70C8F33A230D47D1D766DD0EEEE7F22E0D89DA6E * get_U3CCurrentAccessTokenU3Ek__BackingField_0() const { return ___U3CCurrentAccessTokenU3Ek__BackingField_0; }
	inline AccessToken_t70C8F33A230D47D1D766DD0EEEE7F22E0D89DA6E ** get_address_of_U3CCurrentAccessTokenU3Ek__BackingField_0() { return &___U3CCurrentAccessTokenU3Ek__BackingField_0; }
	inline void set_U3CCurrentAccessTokenU3Ek__BackingField_0(AccessToken_t70C8F33A230D47D1D766DD0EEEE7F22E0D89DA6E * value)
	{
		___U3CCurrentAccessTokenU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCurrentAccessTokenU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACCESSTOKEN_T70C8F33A230D47D1D766DD0EEEE7F22E0D89DA6E_H
#ifndef ACCESSTOKENREFRESHRESULT_T9B2C219A86308A6F812DB3B7900BD65BB9D2974F_H
#define ACCESSTOKENREFRESHRESULT_T9B2C219A86308A6F812DB3B7900BD65BB9D2974F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.AccessTokenRefreshResult
struct  AccessTokenRefreshResult_t9B2C219A86308A6F812DB3B7900BD65BB9D2974F  : public ResultBase_tB17E9A196664F60E8B9B839509E7614CCD921EAB
{
public:
	// Facebook.Unity.AccessToken Facebook.Unity.AccessTokenRefreshResult::<AccessToken>k__BackingField
	AccessToken_t70C8F33A230D47D1D766DD0EEEE7F22E0D89DA6E * ___U3CAccessTokenU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CAccessTokenU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(AccessTokenRefreshResult_t9B2C219A86308A6F812DB3B7900BD65BB9D2974F, ___U3CAccessTokenU3Ek__BackingField_6)); }
	inline AccessToken_t70C8F33A230D47D1D766DD0EEEE7F22E0D89DA6E * get_U3CAccessTokenU3Ek__BackingField_6() const { return ___U3CAccessTokenU3Ek__BackingField_6; }
	inline AccessToken_t70C8F33A230D47D1D766DD0EEEE7F22E0D89DA6E ** get_address_of_U3CAccessTokenU3Ek__BackingField_6() { return &___U3CAccessTokenU3Ek__BackingField_6; }
	inline void set_U3CAccessTokenU3Ek__BackingField_6(AccessToken_t70C8F33A230D47D1D766DD0EEEE7F22E0D89DA6E * value)
	{
		___U3CAccessTokenU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAccessTokenU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACCESSTOKENREFRESHRESULT_T9B2C219A86308A6F812DB3B7900BD65BB9D2974F_H
#ifndef APPINVITERESULT_T855BB8DDD7557CB470052D0961D1EEADC1FA5AC5_H
#define APPINVITERESULT_T855BB8DDD7557CB470052D0961D1EEADC1FA5AC5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.AppInviteResult
struct  AppInviteResult_t855BB8DDD7557CB470052D0961D1EEADC1FA5AC5  : public ResultBase_tB17E9A196664F60E8B9B839509E7614CCD921EAB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPINVITERESULT_T855BB8DDD7557CB470052D0961D1EEADC1FA5AC5_H
#ifndef APPLINKRESULT_TBBF0456C6F7898ACE53B383E3F6B23700171BC73_H
#define APPLINKRESULT_TBBF0456C6F7898ACE53B383E3F6B23700171BC73_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.AppLinkResult
struct  AppLinkResult_tBBF0456C6F7898ACE53B383E3F6B23700171BC73  : public ResultBase_tB17E9A196664F60E8B9B839509E7614CCD921EAB
{
public:
	// System.String Facebook.Unity.AppLinkResult::<Url>k__BackingField
	String_t* ___U3CUrlU3Ek__BackingField_6;
	// System.String Facebook.Unity.AppLinkResult::<TargetUrl>k__BackingField
	String_t* ___U3CTargetUrlU3Ek__BackingField_7;
	// System.String Facebook.Unity.AppLinkResult::<Ref>k__BackingField
	String_t* ___U3CRefU3Ek__BackingField_8;
	// System.Collections.Generic.IDictionary`2<System.String,System.Object> Facebook.Unity.AppLinkResult::<Extras>k__BackingField
	RuntimeObject* ___U3CExtrasU3Ek__BackingField_9;

public:
	inline static int32_t get_offset_of_U3CUrlU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(AppLinkResult_tBBF0456C6F7898ACE53B383E3F6B23700171BC73, ___U3CUrlU3Ek__BackingField_6)); }
	inline String_t* get_U3CUrlU3Ek__BackingField_6() const { return ___U3CUrlU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CUrlU3Ek__BackingField_6() { return &___U3CUrlU3Ek__BackingField_6; }
	inline void set_U3CUrlU3Ek__BackingField_6(String_t* value)
	{
		___U3CUrlU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUrlU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CTargetUrlU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(AppLinkResult_tBBF0456C6F7898ACE53B383E3F6B23700171BC73, ___U3CTargetUrlU3Ek__BackingField_7)); }
	inline String_t* get_U3CTargetUrlU3Ek__BackingField_7() const { return ___U3CTargetUrlU3Ek__BackingField_7; }
	inline String_t** get_address_of_U3CTargetUrlU3Ek__BackingField_7() { return &___U3CTargetUrlU3Ek__BackingField_7; }
	inline void set_U3CTargetUrlU3Ek__BackingField_7(String_t* value)
	{
		___U3CTargetUrlU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTargetUrlU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CRefU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(AppLinkResult_tBBF0456C6F7898ACE53B383E3F6B23700171BC73, ___U3CRefU3Ek__BackingField_8)); }
	inline String_t* get_U3CRefU3Ek__BackingField_8() const { return ___U3CRefU3Ek__BackingField_8; }
	inline String_t** get_address_of_U3CRefU3Ek__BackingField_8() { return &___U3CRefU3Ek__BackingField_8; }
	inline void set_U3CRefU3Ek__BackingField_8(String_t* value)
	{
		___U3CRefU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRefU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3CExtrasU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(AppLinkResult_tBBF0456C6F7898ACE53B383E3F6B23700171BC73, ___U3CExtrasU3Ek__BackingField_9)); }
	inline RuntimeObject* get_U3CExtrasU3Ek__BackingField_9() const { return ___U3CExtrasU3Ek__BackingField_9; }
	inline RuntimeObject** get_address_of_U3CExtrasU3Ek__BackingField_9() { return &___U3CExtrasU3Ek__BackingField_9; }
	inline void set_U3CExtrasU3Ek__BackingField_9(RuntimeObject* value)
	{
		___U3CExtrasU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CExtrasU3Ek__BackingField_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPLINKRESULT_TBBF0456C6F7898ACE53B383E3F6B23700171BC73_H
#ifndef APPREQUESTRESULT_T838349EBDF4E864EEE2AC0E501E1C1AE2EF2CC01_H
#define APPREQUESTRESULT_T838349EBDF4E864EEE2AC0E501E1C1AE2EF2CC01_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.AppRequestResult
struct  AppRequestResult_t838349EBDF4E864EEE2AC0E501E1C1AE2EF2CC01  : public ResultBase_tB17E9A196664F60E8B9B839509E7614CCD921EAB
{
public:
	// System.String Facebook.Unity.AppRequestResult::<RequestID>k__BackingField
	String_t* ___U3CRequestIDU3Ek__BackingField_6;
	// System.Collections.Generic.IEnumerable`1<System.String> Facebook.Unity.AppRequestResult::<To>k__BackingField
	RuntimeObject* ___U3CToU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_U3CRequestIDU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(AppRequestResult_t838349EBDF4E864EEE2AC0E501E1C1AE2EF2CC01, ___U3CRequestIDU3Ek__BackingField_6)); }
	inline String_t* get_U3CRequestIDU3Ek__BackingField_6() const { return ___U3CRequestIDU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CRequestIDU3Ek__BackingField_6() { return &___U3CRequestIDU3Ek__BackingField_6; }
	inline void set_U3CRequestIDU3Ek__BackingField_6(String_t* value)
	{
		___U3CRequestIDU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRequestIDU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CToU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(AppRequestResult_t838349EBDF4E864EEE2AC0E501E1C1AE2EF2CC01, ___U3CToU3Ek__BackingField_7)); }
	inline RuntimeObject* get_U3CToU3Ek__BackingField_7() const { return ___U3CToU3Ek__BackingField_7; }
	inline RuntimeObject** get_address_of_U3CToU3Ek__BackingField_7() { return &___U3CToU3Ek__BackingField_7; }
	inline void set_U3CToU3Ek__BackingField_7(RuntimeObject* value)
	{
		___U3CToU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CToU3Ek__BackingField_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPREQUESTRESULT_T838349EBDF4E864EEE2AC0E501E1C1AE2EF2CC01_H
#ifndef EDITORFACEBOOK_TAB2305F929DE16DBA0CB25C5DC0D0D690E72CC97_H
#define EDITORFACEBOOK_TAB2305F929DE16DBA0CB25C5DC0D0D690E72CC97_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Editor.EditorFacebook
struct  EditorFacebook_tAB2305F929DE16DBA0CB25C5DC0D0D690E72CC97  : public FacebookBase_t89F93EF12353405D2CEFAC7B59E860123FB472C7
{
public:
	// Facebook.Unity.Editor.IEditorWrapper Facebook.Unity.Editor.EditorFacebook::editorWrapper
	RuntimeObject* ___editorWrapper_3;
	// System.Boolean Facebook.Unity.Editor.EditorFacebook::<LimitEventUsage>k__BackingField
	bool ___U3CLimitEventUsageU3Ek__BackingField_4;
	// Facebook.Unity.ShareDialogMode Facebook.Unity.Editor.EditorFacebook::<ShareDialogMode>k__BackingField
	int32_t ___U3CShareDialogModeU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_editorWrapper_3() { return static_cast<int32_t>(offsetof(EditorFacebook_tAB2305F929DE16DBA0CB25C5DC0D0D690E72CC97, ___editorWrapper_3)); }
	inline RuntimeObject* get_editorWrapper_3() const { return ___editorWrapper_3; }
	inline RuntimeObject** get_address_of_editorWrapper_3() { return &___editorWrapper_3; }
	inline void set_editorWrapper_3(RuntimeObject* value)
	{
		___editorWrapper_3 = value;
		Il2CppCodeGenWriteBarrier((&___editorWrapper_3), value);
	}

	inline static int32_t get_offset_of_U3CLimitEventUsageU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(EditorFacebook_tAB2305F929DE16DBA0CB25C5DC0D0D690E72CC97, ___U3CLimitEventUsageU3Ek__BackingField_4)); }
	inline bool get_U3CLimitEventUsageU3Ek__BackingField_4() const { return ___U3CLimitEventUsageU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CLimitEventUsageU3Ek__BackingField_4() { return &___U3CLimitEventUsageU3Ek__BackingField_4; }
	inline void set_U3CLimitEventUsageU3Ek__BackingField_4(bool value)
	{
		___U3CLimitEventUsageU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CShareDialogModeU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(EditorFacebook_tAB2305F929DE16DBA0CB25C5DC0D0D690E72CC97, ___U3CShareDialogModeU3Ek__BackingField_5)); }
	inline int32_t get_U3CShareDialogModeU3Ek__BackingField_5() const { return ___U3CShareDialogModeU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CShareDialogModeU3Ek__BackingField_5() { return &___U3CShareDialogModeU3Ek__BackingField_5; }
	inline void set_U3CShareDialogModeU3Ek__BackingField_5(int32_t value)
	{
		___U3CShareDialogModeU3Ek__BackingField_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDITORFACEBOOK_TAB2305F929DE16DBA0CB25C5DC0D0D690E72CC97_H
#ifndef GRAPHRESULT_T3AF5558F4BE466D1C58FE83A3E54916F07E51F8A_H
#define GRAPHRESULT_T3AF5558F4BE466D1C58FE83A3E54916F07E51F8A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.GraphResult
struct  GraphResult_t3AF5558F4BE466D1C58FE83A3E54916F07E51F8A  : public ResultBase_tB17E9A196664F60E8B9B839509E7614CCD921EAB
{
public:
	// System.Collections.Generic.IList`1<System.Object> Facebook.Unity.GraphResult::<ResultList>k__BackingField
	RuntimeObject* ___U3CResultListU3Ek__BackingField_6;
	// UnityEngine.Texture2D Facebook.Unity.GraphResult::<Texture>k__BackingField
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___U3CTextureU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_U3CResultListU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(GraphResult_t3AF5558F4BE466D1C58FE83A3E54916F07E51F8A, ___U3CResultListU3Ek__BackingField_6)); }
	inline RuntimeObject* get_U3CResultListU3Ek__BackingField_6() const { return ___U3CResultListU3Ek__BackingField_6; }
	inline RuntimeObject** get_address_of_U3CResultListU3Ek__BackingField_6() { return &___U3CResultListU3Ek__BackingField_6; }
	inline void set_U3CResultListU3Ek__BackingField_6(RuntimeObject* value)
	{
		___U3CResultListU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CResultListU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CTextureU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(GraphResult_t3AF5558F4BE466D1C58FE83A3E54916F07E51F8A, ___U3CTextureU3Ek__BackingField_7)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_U3CTextureU3Ek__BackingField_7() const { return ___U3CTextureU3Ek__BackingField_7; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_U3CTextureU3Ek__BackingField_7() { return &___U3CTextureU3Ek__BackingField_7; }
	inline void set_U3CTextureU3Ek__BackingField_7(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___U3CTextureU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTextureU3Ek__BackingField_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHRESULT_T3AF5558F4BE466D1C58FE83A3E54916F07E51F8A_H
#ifndef LOGINRESULT_T7A3012297BB48C9E049AF971434529DE5912A912_H
#define LOGINRESULT_T7A3012297BB48C9E049AF971434529DE5912A912_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.LoginResult
struct  LoginResult_t7A3012297BB48C9E049AF971434529DE5912A912  : public ResultBase_tB17E9A196664F60E8B9B839509E7614CCD921EAB
{
public:
	// Facebook.Unity.AccessToken Facebook.Unity.LoginResult::<AccessToken>k__BackingField
	AccessToken_t70C8F33A230D47D1D766DD0EEEE7F22E0D89DA6E * ___U3CAccessTokenU3Ek__BackingField_10;

public:
	inline static int32_t get_offset_of_U3CAccessTokenU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(LoginResult_t7A3012297BB48C9E049AF971434529DE5912A912, ___U3CAccessTokenU3Ek__BackingField_10)); }
	inline AccessToken_t70C8F33A230D47D1D766DD0EEEE7F22E0D89DA6E * get_U3CAccessTokenU3Ek__BackingField_10() const { return ___U3CAccessTokenU3Ek__BackingField_10; }
	inline AccessToken_t70C8F33A230D47D1D766DD0EEEE7F22E0D89DA6E ** get_address_of_U3CAccessTokenU3Ek__BackingField_10() { return &___U3CAccessTokenU3Ek__BackingField_10; }
	inline void set_U3CAccessTokenU3Ek__BackingField_10(AccessToken_t70C8F33A230D47D1D766DD0EEEE7F22E0D89DA6E * value)
	{
		___U3CAccessTokenU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAccessTokenU3Ek__BackingField_10), value);
	}
};

struct LoginResult_t7A3012297BB48C9E049AF971434529DE5912A912_StaticFields
{
public:
	// System.String Facebook.Unity.LoginResult::UserIdKey
	String_t* ___UserIdKey_6;
	// System.String Facebook.Unity.LoginResult::ExpirationTimestampKey
	String_t* ___ExpirationTimestampKey_7;
	// System.String Facebook.Unity.LoginResult::PermissionsKey
	String_t* ___PermissionsKey_8;
	// System.String Facebook.Unity.LoginResult::AccessTokenKey
	String_t* ___AccessTokenKey_9;

public:
	inline static int32_t get_offset_of_UserIdKey_6() { return static_cast<int32_t>(offsetof(LoginResult_t7A3012297BB48C9E049AF971434529DE5912A912_StaticFields, ___UserIdKey_6)); }
	inline String_t* get_UserIdKey_6() const { return ___UserIdKey_6; }
	inline String_t** get_address_of_UserIdKey_6() { return &___UserIdKey_6; }
	inline void set_UserIdKey_6(String_t* value)
	{
		___UserIdKey_6 = value;
		Il2CppCodeGenWriteBarrier((&___UserIdKey_6), value);
	}

	inline static int32_t get_offset_of_ExpirationTimestampKey_7() { return static_cast<int32_t>(offsetof(LoginResult_t7A3012297BB48C9E049AF971434529DE5912A912_StaticFields, ___ExpirationTimestampKey_7)); }
	inline String_t* get_ExpirationTimestampKey_7() const { return ___ExpirationTimestampKey_7; }
	inline String_t** get_address_of_ExpirationTimestampKey_7() { return &___ExpirationTimestampKey_7; }
	inline void set_ExpirationTimestampKey_7(String_t* value)
	{
		___ExpirationTimestampKey_7 = value;
		Il2CppCodeGenWriteBarrier((&___ExpirationTimestampKey_7), value);
	}

	inline static int32_t get_offset_of_PermissionsKey_8() { return static_cast<int32_t>(offsetof(LoginResult_t7A3012297BB48C9E049AF971434529DE5912A912_StaticFields, ___PermissionsKey_8)); }
	inline String_t* get_PermissionsKey_8() const { return ___PermissionsKey_8; }
	inline String_t** get_address_of_PermissionsKey_8() { return &___PermissionsKey_8; }
	inline void set_PermissionsKey_8(String_t* value)
	{
		___PermissionsKey_8 = value;
		Il2CppCodeGenWriteBarrier((&___PermissionsKey_8), value);
	}

	inline static int32_t get_offset_of_AccessTokenKey_9() { return static_cast<int32_t>(offsetof(LoginResult_t7A3012297BB48C9E049AF971434529DE5912A912_StaticFields, ___AccessTokenKey_9)); }
	inline String_t* get_AccessTokenKey_9() const { return ___AccessTokenKey_9; }
	inline String_t** get_address_of_AccessTokenKey_9() { return &___AccessTokenKey_9; }
	inline void set_AccessTokenKey_9(String_t* value)
	{
		___AccessTokenKey_9 = value;
		Il2CppCodeGenWriteBarrier((&___AccessTokenKey_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGINRESULT_T7A3012297BB48C9E049AF971434529DE5912A912_H
#ifndef MOBILEFACEBOOK_T27A7B4F062FC7F3935525F591E59A411462EF919_H
#define MOBILEFACEBOOK_T27A7B4F062FC7F3935525F591E59A411462EF919_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Mobile.MobileFacebook
struct  MobileFacebook_t27A7B4F062FC7F3935525F591E59A411462EF919  : public FacebookBase_t89F93EF12353405D2CEFAC7B59E860123FB472C7
{
public:
	// Facebook.Unity.ShareDialogMode Facebook.Unity.Mobile.MobileFacebook::shareDialogMode
	int32_t ___shareDialogMode_3;

public:
	inline static int32_t get_offset_of_shareDialogMode_3() { return static_cast<int32_t>(offsetof(MobileFacebook_t27A7B4F062FC7F3935525F591E59A411462EF919, ___shareDialogMode_3)); }
	inline int32_t get_shareDialogMode_3() const { return ___shareDialogMode_3; }
	inline int32_t* get_address_of_shareDialogMode_3() { return &___shareDialogMode_3; }
	inline void set_shareDialogMode_3(int32_t value)
	{
		___shareDialogMode_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOBILEFACEBOOK_T27A7B4F062FC7F3935525F591E59A411462EF919_H
#ifndef PAYRESULT_T5E5D8B2964B98530AA763B440FCFB137D01EB47E_H
#define PAYRESULT_T5E5D8B2964B98530AA763B440FCFB137D01EB47E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.PayResult
struct  PayResult_t5E5D8B2964B98530AA763B440FCFB137D01EB47E  : public ResultBase_tB17E9A196664F60E8B9B839509E7614CCD921EAB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAYRESULT_T5E5D8B2964B98530AA763B440FCFB137D01EB47E_H
#ifndef SHARERESULT_T2C8058416F1EBBE353840B0276F3E438E4E76B2C_H
#define SHARERESULT_T2C8058416F1EBBE353840B0276F3E438E4E76B2C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.ShareResult
struct  ShareResult_t2C8058416F1EBBE353840B0276F3E438E4E76B2C  : public ResultBase_tB17E9A196664F60E8B9B839509E7614CCD921EAB
{
public:
	// System.String Facebook.Unity.ShareResult::<PostId>k__BackingField
	String_t* ___U3CPostIdU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CPostIdU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(ShareResult_t2C8058416F1EBBE353840B0276F3E438E4E76B2C, ___U3CPostIdU3Ek__BackingField_6)); }
	inline String_t* get_U3CPostIdU3Ek__BackingField_6() const { return ___U3CPostIdU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CPostIdU3Ek__BackingField_6() { return &___U3CPostIdU3Ek__BackingField_6; }
	inline void set_U3CPostIdU3Ek__BackingField_6(String_t* value)
	{
		___U3CPostIdU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPostIdU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHARERESULT_T2C8058416F1EBBE353840B0276F3E438E4E76B2C_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef NULLABLE_1_TE0C782573E021F2C72FEE9B4EB80C97956F5337A_H
#define NULLABLE_1_TE0C782573E021F2C72FEE9B4EB80C97956F5337A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Facebook.Unity.FacebookUnityPlatform>
struct  Nullable_1_tE0C782573E021F2C72FEE9B4EB80C97956F5337A 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_tE0C782573E021F2C72FEE9B4EB80C97956F5337A, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_tE0C782573E021F2C72FEE9B4EB80C97956F5337A, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_TE0C782573E021F2C72FEE9B4EB80C97956F5337A_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#define SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_pinvoke : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_com : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifndef CONSTANTS_T5370C0E7D2006DEAB21DCACA0E2C27261BC2F2B0_H
#define CONSTANTS_T5370C0E7D2006DEAB21DCACA0E2C27261BC2F2B0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Constants
struct  Constants_t5370C0E7D2006DEAB21DCACA0E2C27261BC2F2B0  : public RuntimeObject
{
public:

public:
};

struct Constants_t5370C0E7D2006DEAB21DCACA0E2C27261BC2F2B0_StaticFields
{
public:
	// System.Nullable`1<Facebook.Unity.FacebookUnityPlatform> Facebook.Unity.Constants::currentPlatform
	Nullable_1_tE0C782573E021F2C72FEE9B4EB80C97956F5337A  ___currentPlatform_0;

public:
	inline static int32_t get_offset_of_currentPlatform_0() { return static_cast<int32_t>(offsetof(Constants_t5370C0E7D2006DEAB21DCACA0E2C27261BC2F2B0_StaticFields, ___currentPlatform_0)); }
	inline Nullable_1_tE0C782573E021F2C72FEE9B4EB80C97956F5337A  get_currentPlatform_0() const { return ___currentPlatform_0; }
	inline Nullable_1_tE0C782573E021F2C72FEE9B4EB80C97956F5337A * get_address_of_currentPlatform_0() { return &___currentPlatform_0; }
	inline void set_currentPlatform_0(Nullable_1_tE0C782573E021F2C72FEE9B4EB80C97956F5337A  value)
	{
		___currentPlatform_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTANTS_T5370C0E7D2006DEAB21DCACA0E2C27261BC2F2B0_H
#ifndef FB_TE8952E47A8F480E242B6FA8D42045B63222B4442_H
#define FB_TE8952E47A8F480E242B6FA8D42045B63222B4442_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.FB
struct  FB_tE8952E47A8F480E242B6FA8D42045B63222B4442  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:

public:
};

struct FB_tE8952E47A8F480E242B6FA8D42045B63222B4442_StaticFields
{
public:
	// Facebook.Unity.IFacebook Facebook.Unity.FB::facebook
	RuntimeObject* ___facebook_5;
	// System.Boolean Facebook.Unity.FB::isInitCalled
	bool ___isInitCalled_6;
	// System.String Facebook.Unity.FB::facebookDomain
	String_t* ___facebookDomain_7;
	// System.String Facebook.Unity.FB::graphApiVersion
	String_t* ___graphApiVersion_8;
	// System.String Facebook.Unity.FB::<AppId>k__BackingField
	String_t* ___U3CAppIdU3Ek__BackingField_9;
	// System.String Facebook.Unity.FB::<ClientToken>k__BackingField
	String_t* ___U3CClientTokenU3Ek__BackingField_10;
	// Facebook.Unity.FB_OnDLLLoaded Facebook.Unity.FB::<OnDLLLoadedDelegate>k__BackingField
	OnDLLLoaded_tB74B93A2FF5819C30C822CB7B88B674DCF83DCD3 * ___U3COnDLLLoadedDelegateU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of_facebook_5() { return static_cast<int32_t>(offsetof(FB_tE8952E47A8F480E242B6FA8D42045B63222B4442_StaticFields, ___facebook_5)); }
	inline RuntimeObject* get_facebook_5() const { return ___facebook_5; }
	inline RuntimeObject** get_address_of_facebook_5() { return &___facebook_5; }
	inline void set_facebook_5(RuntimeObject* value)
	{
		___facebook_5 = value;
		Il2CppCodeGenWriteBarrier((&___facebook_5), value);
	}

	inline static int32_t get_offset_of_isInitCalled_6() { return static_cast<int32_t>(offsetof(FB_tE8952E47A8F480E242B6FA8D42045B63222B4442_StaticFields, ___isInitCalled_6)); }
	inline bool get_isInitCalled_6() const { return ___isInitCalled_6; }
	inline bool* get_address_of_isInitCalled_6() { return &___isInitCalled_6; }
	inline void set_isInitCalled_6(bool value)
	{
		___isInitCalled_6 = value;
	}

	inline static int32_t get_offset_of_facebookDomain_7() { return static_cast<int32_t>(offsetof(FB_tE8952E47A8F480E242B6FA8D42045B63222B4442_StaticFields, ___facebookDomain_7)); }
	inline String_t* get_facebookDomain_7() const { return ___facebookDomain_7; }
	inline String_t** get_address_of_facebookDomain_7() { return &___facebookDomain_7; }
	inline void set_facebookDomain_7(String_t* value)
	{
		___facebookDomain_7 = value;
		Il2CppCodeGenWriteBarrier((&___facebookDomain_7), value);
	}

	inline static int32_t get_offset_of_graphApiVersion_8() { return static_cast<int32_t>(offsetof(FB_tE8952E47A8F480E242B6FA8D42045B63222B4442_StaticFields, ___graphApiVersion_8)); }
	inline String_t* get_graphApiVersion_8() const { return ___graphApiVersion_8; }
	inline String_t** get_address_of_graphApiVersion_8() { return &___graphApiVersion_8; }
	inline void set_graphApiVersion_8(String_t* value)
	{
		___graphApiVersion_8 = value;
		Il2CppCodeGenWriteBarrier((&___graphApiVersion_8), value);
	}

	inline static int32_t get_offset_of_U3CAppIdU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(FB_tE8952E47A8F480E242B6FA8D42045B63222B4442_StaticFields, ___U3CAppIdU3Ek__BackingField_9)); }
	inline String_t* get_U3CAppIdU3Ek__BackingField_9() const { return ___U3CAppIdU3Ek__BackingField_9; }
	inline String_t** get_address_of_U3CAppIdU3Ek__BackingField_9() { return &___U3CAppIdU3Ek__BackingField_9; }
	inline void set_U3CAppIdU3Ek__BackingField_9(String_t* value)
	{
		___U3CAppIdU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAppIdU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3CClientTokenU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(FB_tE8952E47A8F480E242B6FA8D42045B63222B4442_StaticFields, ___U3CClientTokenU3Ek__BackingField_10)); }
	inline String_t* get_U3CClientTokenU3Ek__BackingField_10() const { return ___U3CClientTokenU3Ek__BackingField_10; }
	inline String_t** get_address_of_U3CClientTokenU3Ek__BackingField_10() { return &___U3CClientTokenU3Ek__BackingField_10; }
	inline void set_U3CClientTokenU3Ek__BackingField_10(String_t* value)
	{
		___U3CClientTokenU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CClientTokenU3Ek__BackingField_10), value);
	}

	inline static int32_t get_offset_of_U3COnDLLLoadedDelegateU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(FB_tE8952E47A8F480E242B6FA8D42045B63222B4442_StaticFields, ___U3COnDLLLoadedDelegateU3Ek__BackingField_11)); }
	inline OnDLLLoaded_tB74B93A2FF5819C30C822CB7B88B674DCF83DCD3 * get_U3COnDLLLoadedDelegateU3Ek__BackingField_11() const { return ___U3COnDLLLoadedDelegateU3Ek__BackingField_11; }
	inline OnDLLLoaded_tB74B93A2FF5819C30C822CB7B88B674DCF83DCD3 ** get_address_of_U3COnDLLLoadedDelegateU3Ek__BackingField_11() { return &___U3COnDLLLoadedDelegateU3Ek__BackingField_11; }
	inline void set_U3COnDLLLoadedDelegateU3Ek__BackingField_11(OnDLLLoaded_tB74B93A2FF5819C30C822CB7B88B674DCF83DCD3 * value)
	{
		___U3COnDLLLoadedDelegateU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3COnDLLLoadedDelegateU3Ek__BackingField_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FB_TE8952E47A8F480E242B6FA8D42045B63222B4442_H
#ifndef ONDLLLOADED_TB74B93A2FF5819C30C822CB7B88B674DCF83DCD3_H
#define ONDLLLOADED_TB74B93A2FF5819C30C822CB7B88B674DCF83DCD3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.FB_OnDLLLoaded
struct  OnDLLLoaded_tB74B93A2FF5819C30C822CB7B88B674DCF83DCD3  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONDLLLOADED_TB74B93A2FF5819C30C822CB7B88B674DCF83DCD3_H
#ifndef ONCOMPLETE_T6617755663754FE5E8E8F946F5639627C5589513_H
#define ONCOMPLETE_T6617755663754FE5E8E8F946F5639627C5589513_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Gameroom.GameroomFacebook_OnComplete
struct  OnComplete_t6617755663754FE5E8E8F946F5639627C5589513  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONCOMPLETE_T6617755663754FE5E8E8F946F5639627C5589513_H
#ifndef HIDEUNITYDELEGATE_TFFB6D624086048B6FFFB85D9BC1946920684385B_H
#define HIDEUNITYDELEGATE_TFFB6D624086048B6FFFB85D9BC1946920684385B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.HideUnityDelegate
struct  HideUnityDelegate_tFFB6D624086048B6FFFB85D9BC1946920684385B  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDEUNITYDELEGATE_TFFB6D624086048B6FFFB85D9BC1946920684385B_H
#ifndef INITDELEGATE_TD9FF06C3949FD06E97E275F72D8F43AF4F496F47_H
#define INITDELEGATE_TD9FF06C3949FD06E97E275F72D8F43AF4F496F47_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.InitDelegate
struct  InitDelegate_tD9FF06C3949FD06E97E275F72D8F43AF4F496F47  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INITDELEGATE_TD9FF06C3949FD06E97E275F72D8F43AF4F496F47_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef ASYNCREQUESTSTRING_T33F30E800180F52474E4C86249DF12C85BFA9AFB_H
#define ASYNCREQUESTSTRING_T33F30E800180F52474E4C86249DF12C85BFA9AFB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.AsyncRequestString
struct  AsyncRequestString_t33F30E800180F52474E4C86249DF12C85BFA9AFB  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Uri Facebook.Unity.AsyncRequestString::url
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___url_4;
	// Facebook.Unity.HttpMethod Facebook.Unity.AsyncRequestString::method
	int32_t ___method_5;
	// System.Collections.Generic.IDictionary`2<System.String,System.String> Facebook.Unity.AsyncRequestString::formData
	RuntimeObject* ___formData_6;
	// UnityEngine.WWWForm Facebook.Unity.AsyncRequestString::query
	WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24 * ___query_7;
	// Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGraphResult> Facebook.Unity.AsyncRequestString::callback
	FacebookDelegate_1_tF029640B8495BD2CE458F81E993CBE125C757EB5 * ___callback_8;

public:
	inline static int32_t get_offset_of_url_4() { return static_cast<int32_t>(offsetof(AsyncRequestString_t33F30E800180F52474E4C86249DF12C85BFA9AFB, ___url_4)); }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * get_url_4() const { return ___url_4; }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E ** get_address_of_url_4() { return &___url_4; }
	inline void set_url_4(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * value)
	{
		___url_4 = value;
		Il2CppCodeGenWriteBarrier((&___url_4), value);
	}

	inline static int32_t get_offset_of_method_5() { return static_cast<int32_t>(offsetof(AsyncRequestString_t33F30E800180F52474E4C86249DF12C85BFA9AFB, ___method_5)); }
	inline int32_t get_method_5() const { return ___method_5; }
	inline int32_t* get_address_of_method_5() { return &___method_5; }
	inline void set_method_5(int32_t value)
	{
		___method_5 = value;
	}

	inline static int32_t get_offset_of_formData_6() { return static_cast<int32_t>(offsetof(AsyncRequestString_t33F30E800180F52474E4C86249DF12C85BFA9AFB, ___formData_6)); }
	inline RuntimeObject* get_formData_6() const { return ___formData_6; }
	inline RuntimeObject** get_address_of_formData_6() { return &___formData_6; }
	inline void set_formData_6(RuntimeObject* value)
	{
		___formData_6 = value;
		Il2CppCodeGenWriteBarrier((&___formData_6), value);
	}

	inline static int32_t get_offset_of_query_7() { return static_cast<int32_t>(offsetof(AsyncRequestString_t33F30E800180F52474E4C86249DF12C85BFA9AFB, ___query_7)); }
	inline WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24 * get_query_7() const { return ___query_7; }
	inline WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24 ** get_address_of_query_7() { return &___query_7; }
	inline void set_query_7(WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24 * value)
	{
		___query_7 = value;
		Il2CppCodeGenWriteBarrier((&___query_7), value);
	}

	inline static int32_t get_offset_of_callback_8() { return static_cast<int32_t>(offsetof(AsyncRequestString_t33F30E800180F52474E4C86249DF12C85BFA9AFB, ___callback_8)); }
	inline FacebookDelegate_1_tF029640B8495BD2CE458F81E993CBE125C757EB5 * get_callback_8() const { return ___callback_8; }
	inline FacebookDelegate_1_tF029640B8495BD2CE458F81E993CBE125C757EB5 ** get_address_of_callback_8() { return &___callback_8; }
	inline void set_callback_8(FacebookDelegate_1_tF029640B8495BD2CE458F81E993CBE125C757EB5 * value)
	{
		___callback_8 = value;
		Il2CppCodeGenWriteBarrier((&___callback_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCREQUESTSTRING_T33F30E800180F52474E4C86249DF12C85BFA9AFB_H
#ifndef EDITORFACEBOOKMOCKDIALOG_T43C7DA74401C1D0F653CAB314D17E2A60E212A3F_H
#define EDITORFACEBOOKMOCKDIALOG_T43C7DA74401C1D0F653CAB314D17E2A60E212A3F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Editor.EditorFacebookMockDialog
struct  EditorFacebookMockDialog_t43C7DA74401C1D0F653CAB314D17E2A60E212A3F  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Rect Facebook.Unity.Editor.EditorFacebookMockDialog::modalRect
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___modalRect_4;
	// UnityEngine.GUIStyle Facebook.Unity.Editor.EditorFacebookMockDialog::modalStyle
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___modalStyle_5;
	// Facebook.Unity.Utilities_Callback`1<Facebook.Unity.ResultContainer> Facebook.Unity.Editor.EditorFacebookMockDialog::<Callback>k__BackingField
	Callback_1_t9200E2A35184E330F144E9195F3882E126DA2887 * ___U3CCallbackU3Ek__BackingField_6;
	// System.String Facebook.Unity.Editor.EditorFacebookMockDialog::<CallbackID>k__BackingField
	String_t* ___U3CCallbackIDU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_modalRect_4() { return static_cast<int32_t>(offsetof(EditorFacebookMockDialog_t43C7DA74401C1D0F653CAB314D17E2A60E212A3F, ___modalRect_4)); }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE  get_modalRect_4() const { return ___modalRect_4; }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE * get_address_of_modalRect_4() { return &___modalRect_4; }
	inline void set_modalRect_4(Rect_t35B976DE901B5423C11705E156938EA27AB402CE  value)
	{
		___modalRect_4 = value;
	}

	inline static int32_t get_offset_of_modalStyle_5() { return static_cast<int32_t>(offsetof(EditorFacebookMockDialog_t43C7DA74401C1D0F653CAB314D17E2A60E212A3F, ___modalStyle_5)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_modalStyle_5() const { return ___modalStyle_5; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_modalStyle_5() { return &___modalStyle_5; }
	inline void set_modalStyle_5(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___modalStyle_5 = value;
		Il2CppCodeGenWriteBarrier((&___modalStyle_5), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(EditorFacebookMockDialog_t43C7DA74401C1D0F653CAB314D17E2A60E212A3F, ___U3CCallbackU3Ek__BackingField_6)); }
	inline Callback_1_t9200E2A35184E330F144E9195F3882E126DA2887 * get_U3CCallbackU3Ek__BackingField_6() const { return ___U3CCallbackU3Ek__BackingField_6; }
	inline Callback_1_t9200E2A35184E330F144E9195F3882E126DA2887 ** get_address_of_U3CCallbackU3Ek__BackingField_6() { return &___U3CCallbackU3Ek__BackingField_6; }
	inline void set_U3CCallbackU3Ek__BackingField_6(Callback_1_t9200E2A35184E330F144E9195F3882E126DA2887 * value)
	{
		___U3CCallbackU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CCallbackIDU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(EditorFacebookMockDialog_t43C7DA74401C1D0F653CAB314D17E2A60E212A3F, ___U3CCallbackIDU3Ek__BackingField_7)); }
	inline String_t* get_U3CCallbackIDU3Ek__BackingField_7() const { return ___U3CCallbackIDU3Ek__BackingField_7; }
	inline String_t** get_address_of_U3CCallbackIDU3Ek__BackingField_7() { return &___U3CCallbackIDU3Ek__BackingField_7; }
	inline void set_U3CCallbackIDU3Ek__BackingField_7(String_t* value)
	{
		___U3CCallbackIDU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackIDU3Ek__BackingField_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDITORFACEBOOKMOCKDIALOG_T43C7DA74401C1D0F653CAB314D17E2A60E212A3F_H
#ifndef COMPILEDFACEBOOKLOADER_T00BC9903E387CA1A268154CB0B8132188627FA4F_H
#define COMPILEDFACEBOOKLOADER_T00BC9903E387CA1A268154CB0B8132188627FA4F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.FB_CompiledFacebookLoader
struct  CompiledFacebookLoader_t00BC9903E387CA1A268154CB0B8132188627FA4F  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPILEDFACEBOOKLOADER_T00BC9903E387CA1A268154CB0B8132188627FA4F_H
#ifndef FACEBOOKGAMEOBJECT_T0EB6A31799B33368D3236B7D7E3A66E4ED3FF355_H
#define FACEBOOKGAMEOBJECT_T0EB6A31799B33368D3236B7D7E3A66E4ED3FF355_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.FacebookGameObject
struct  FacebookGameObject_t0EB6A31799B33368D3236B7D7E3A66E4ED3FF355  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Facebook.Unity.IFacebookImplementation Facebook.Unity.FacebookGameObject::<Facebook>k__BackingField
	RuntimeObject* ___U3CFacebookU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CFacebookU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(FacebookGameObject_t0EB6A31799B33368D3236B7D7E3A66E4ED3FF355, ___U3CFacebookU3Ek__BackingField_4)); }
	inline RuntimeObject* get_U3CFacebookU3Ek__BackingField_4() const { return ___U3CFacebookU3Ek__BackingField_4; }
	inline RuntimeObject** get_address_of_U3CFacebookU3Ek__BackingField_4() { return &___U3CFacebookU3Ek__BackingField_4; }
	inline void set_U3CFacebookU3Ek__BackingField_4(RuntimeObject* value)
	{
		___U3CFacebookU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFacebookU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACEBOOKGAMEOBJECT_T0EB6A31799B33368D3236B7D7E3A66E4ED3FF355_H
#ifndef FACEBOOKSCHEDULER_TE073D0E8755AB0B15F52B553095716ED9465DBD1_H
#define FACEBOOKSCHEDULER_TE073D0E8755AB0B15F52B553095716ED9465DBD1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.FacebookScheduler
struct  FacebookScheduler_tE073D0E8755AB0B15F52B553095716ED9465DBD1  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACEBOOKSCHEDULER_TE073D0E8755AB0B15F52B553095716ED9465DBD1_H
#ifndef EMPTYMOCKDIALOG_T65530C5D13B47C894CE2A9A4656580EB515547E4_H
#define EMPTYMOCKDIALOG_T65530C5D13B47C894CE2A9A4656580EB515547E4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Editor.Dialogs.EmptyMockDialog
struct  EmptyMockDialog_t65530C5D13B47C894CE2A9A4656580EB515547E4  : public EditorFacebookMockDialog_t43C7DA74401C1D0F653CAB314D17E2A60E212A3F
{
public:
	// System.String Facebook.Unity.Editor.Dialogs.EmptyMockDialog::<EmptyDialogTitle>k__BackingField
	String_t* ___U3CEmptyDialogTitleU3Ek__BackingField_8;

public:
	inline static int32_t get_offset_of_U3CEmptyDialogTitleU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(EmptyMockDialog_t65530C5D13B47C894CE2A9A4656580EB515547E4, ___U3CEmptyDialogTitleU3Ek__BackingField_8)); }
	inline String_t* get_U3CEmptyDialogTitleU3Ek__BackingField_8() const { return ___U3CEmptyDialogTitleU3Ek__BackingField_8; }
	inline String_t** get_address_of_U3CEmptyDialogTitleU3Ek__BackingField_8() { return &___U3CEmptyDialogTitleU3Ek__BackingField_8; }
	inline void set_U3CEmptyDialogTitleU3Ek__BackingField_8(String_t* value)
	{
		___U3CEmptyDialogTitleU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEmptyDialogTitleU3Ek__BackingField_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMPTYMOCKDIALOG_T65530C5D13B47C894CE2A9A4656580EB515547E4_H
#ifndef MOCKLOGINDIALOG_T4B515228F0AE861F1C4067E1BC5AC26EE210F5B8_H
#define MOCKLOGINDIALOG_T4B515228F0AE861F1C4067E1BC5AC26EE210F5B8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Editor.Dialogs.MockLoginDialog
struct  MockLoginDialog_t4B515228F0AE861F1C4067E1BC5AC26EE210F5B8  : public EditorFacebookMockDialog_t43C7DA74401C1D0F653CAB314D17E2A60E212A3F
{
public:
	// System.String Facebook.Unity.Editor.Dialogs.MockLoginDialog::accessToken
	String_t* ___accessToken_8;

public:
	inline static int32_t get_offset_of_accessToken_8() { return static_cast<int32_t>(offsetof(MockLoginDialog_t4B515228F0AE861F1C4067E1BC5AC26EE210F5B8, ___accessToken_8)); }
	inline String_t* get_accessToken_8() const { return ___accessToken_8; }
	inline String_t** get_address_of_accessToken_8() { return &___accessToken_8; }
	inline void set_accessToken_8(String_t* value)
	{
		___accessToken_8 = value;
		Il2CppCodeGenWriteBarrier((&___accessToken_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOCKLOGINDIALOG_T4B515228F0AE861F1C4067E1BC5AC26EE210F5B8_H
#ifndef MOCKSHAREDIALOG_TC2C1394FC8DAFC55285B3D66BF8A2C0586700E7B_H
#define MOCKSHAREDIALOG_TC2C1394FC8DAFC55285B3D66BF8A2C0586700E7B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Editor.Dialogs.MockShareDialog
struct  MockShareDialog_tC2C1394FC8DAFC55285B3D66BF8A2C0586700E7B  : public EditorFacebookMockDialog_t43C7DA74401C1D0F653CAB314D17E2A60E212A3F
{
public:
	// System.String Facebook.Unity.Editor.Dialogs.MockShareDialog::<SubTitle>k__BackingField
	String_t* ___U3CSubTitleU3Ek__BackingField_8;

public:
	inline static int32_t get_offset_of_U3CSubTitleU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(MockShareDialog_tC2C1394FC8DAFC55285B3D66BF8A2C0586700E7B, ___U3CSubTitleU3Ek__BackingField_8)); }
	inline String_t* get_U3CSubTitleU3Ek__BackingField_8() const { return ___U3CSubTitleU3Ek__BackingField_8; }
	inline String_t** get_address_of_U3CSubTitleU3Ek__BackingField_8() { return &___U3CSubTitleU3Ek__BackingField_8; }
	inline void set_U3CSubTitleU3Ek__BackingField_8(String_t* value)
	{
		___U3CSubTitleU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSubTitleU3Ek__BackingField_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOCKSHAREDIALOG_TC2C1394FC8DAFC55285B3D66BF8A2C0586700E7B_H
#ifndef EDITORFACEBOOKGAMEOBJECT_TDD2E0D58DB5C8DA3C9125564F44A5D1F684F1FA1_H
#define EDITORFACEBOOKGAMEOBJECT_TDD2E0D58DB5C8DA3C9125564F44A5D1F684F1FA1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Editor.EditorFacebookGameObject
struct  EditorFacebookGameObject_tDD2E0D58DB5C8DA3C9125564F44A5D1F684F1FA1  : public FacebookGameObject_t0EB6A31799B33368D3236B7D7E3A66E4ED3FF355
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDITORFACEBOOKGAMEOBJECT_TDD2E0D58DB5C8DA3C9125564F44A5D1F684F1FA1_H
#ifndef EDITORFACEBOOKLOADER_TA47B89F4EA01778D9DD0C71EEB23A17E3CFE99A9_H
#define EDITORFACEBOOKLOADER_TA47B89F4EA01778D9DD0C71EEB23A17E3CFE99A9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Editor.EditorFacebookLoader
struct  EditorFacebookLoader_tA47B89F4EA01778D9DD0C71EEB23A17E3CFE99A9  : public CompiledFacebookLoader_t00BC9903E387CA1A268154CB0B8132188627FA4F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDITORFACEBOOKLOADER_TA47B89F4EA01778D9DD0C71EEB23A17E3CFE99A9_H
#ifndef GAMEROOMFACEBOOKGAMEOBJECT_T88ED0BA9A30FDBD5312F126A32F8147B95191A0E_H
#define GAMEROOMFACEBOOKGAMEOBJECT_T88ED0BA9A30FDBD5312F126A32F8147B95191A0E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Gameroom.GameroomFacebookGameObject
struct  GameroomFacebookGameObject_t88ED0BA9A30FDBD5312F126A32F8147B95191A0E  : public FacebookGameObject_t0EB6A31799B33368D3236B7D7E3A66E4ED3FF355
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEROOMFACEBOOKGAMEOBJECT_T88ED0BA9A30FDBD5312F126A32F8147B95191A0E_H
#ifndef GAMEROOMFACEBOOKLOADER_TD33DC4194829118EB3EE64ECEA36BCF489666E91_H
#define GAMEROOMFACEBOOKLOADER_TD33DC4194829118EB3EE64ECEA36BCF489666E91_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Gameroom.GameroomFacebookLoader
struct  GameroomFacebookLoader_tD33DC4194829118EB3EE64ECEA36BCF489666E91  : public CompiledFacebookLoader_t00BC9903E387CA1A268154CB0B8132188627FA4F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEROOMFACEBOOKLOADER_TD33DC4194829118EB3EE64ECEA36BCF489666E91_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6700 = { sizeof (EventTarget_t116BD7DA9A939B54477A9F429106EC67EB49A6C2)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6700[3] = 
{
	EventTarget_t116BD7DA9A939B54477A9F429106EC67EB49A6C2::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6701 = { sizeof (EventType_t40A4EAA6A23F7288106E89EDE2610C86746758A3)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6701[3] = 
{
	EventType_t40A4EAA6A23F7288106E89EDE2610C86746758A3::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6702 = { sizeof (PostConstructorAttribute_t4CEF1EC49C810198695BAA93D198FFE623EF447D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6703 = { sizeof (UniqueAttribute_tA13A1C0849F610545E56A1E177C294CCCDDE8631), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6704 = { sizeof (U3CModuleU3E_t85E142A41EE1971B3F6082EB28F5293289597ACA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6705 = { sizeof (Json_t3B64DF1F5718A3803F0A8B3F78D47C0E4CA66173), -1, sizeof(Json_t3B64DF1F5718A3803F0A8B3F78D47C0E4CA66173_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6705[1] = 
{
	Json_t3B64DF1F5718A3803F0A8B3F78D47C0E4CA66173_StaticFields::get_offset_of_numberFormat_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6706 = { sizeof (Parser_t3F8B8E817A8EC72214A36DFD774A63FBBAD022A7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6706[1] = 
{
	Parser_t3F8B8E817A8EC72214A36DFD774A63FBBAD022A7::get_offset_of_json_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6707 = { sizeof (TOKEN_t04B7126B22EA517C99B68C5E0F65504A5E9C68A7)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6707[13] = 
{
	TOKEN_t04B7126B22EA517C99B68C5E0F65504A5E9C68A7::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6708 = { sizeof (Serializer_t9A570CD7E2FB71A1074B7231DD16C94BE5A7BBAB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6708[1] = 
{
	Serializer_t9A570CD7E2FB71A1074B7231DD16C94BE5A7BBAB::get_offset_of_builder_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6709 = { sizeof (AccessToken_t70C8F33A230D47D1D766DD0EEEE7F22E0D89DA6E), -1, sizeof(AccessToken_t70C8F33A230D47D1D766DD0EEEE7F22E0D89DA6E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6709[6] = 
{
	AccessToken_t70C8F33A230D47D1D766DD0EEEE7F22E0D89DA6E_StaticFields::get_offset_of_U3CCurrentAccessTokenU3Ek__BackingField_0(),
	AccessToken_t70C8F33A230D47D1D766DD0EEEE7F22E0D89DA6E::get_offset_of_U3CTokenStringU3Ek__BackingField_1(),
	AccessToken_t70C8F33A230D47D1D766DD0EEEE7F22E0D89DA6E::get_offset_of_U3CExpirationTimeU3Ek__BackingField_2(),
	AccessToken_t70C8F33A230D47D1D766DD0EEEE7F22E0D89DA6E::get_offset_of_U3CPermissionsU3Ek__BackingField_3(),
	AccessToken_t70C8F33A230D47D1D766DD0EEEE7F22E0D89DA6E::get_offset_of_U3CUserIdU3Ek__BackingField_4(),
	AccessToken_t70C8F33A230D47D1D766DD0EEEE7F22E0D89DA6E::get_offset_of_U3CLastRefreshU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6710 = { sizeof (CallbackManager_t3ED952CB7A3597AAC84A6A973ADAE06A3E832BF8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6710[2] = 
{
	CallbackManager_t3ED952CB7A3597AAC84A6A973ADAE06A3E832BF8::get_offset_of_facebookDelegates_0(),
	CallbackManager_t3ED952CB7A3597AAC84A6A973ADAE06A3E832BF8::get_offset_of_nextAsyncId_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6711 = { sizeof (ComponentFactory_t6F32B91728195B8DCC317A76993B99AD9301CAD3), -1, sizeof(ComponentFactory_t6F32B91728195B8DCC317A76993B99AD9301CAD3_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6711[1] = 
{
	ComponentFactory_t6F32B91728195B8DCC317A76993B99AD9301CAD3_StaticFields::get_offset_of_facebookGameObject_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6712 = { sizeof (IfNotExist_t1CEE0E9DC0A1ECA7EA496C9E14AA293C02203F38)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6712[3] = 
{
	IfNotExist_t1CEE0E9DC0A1ECA7EA496C9E14AA293C02203F38::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6713 = { sizeof (Constants_t5370C0E7D2006DEAB21DCACA0E2C27261BC2F2B0), -1, sizeof(Constants_t5370C0E7D2006DEAB21DCACA0E2C27261BC2F2B0_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6713[1] = 
{
	Constants_t5370C0E7D2006DEAB21DCACA0E2C27261BC2F2B0_StaticFields::get_offset_of_currentPlatform_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6714 = { sizeof (FB_tE8952E47A8F480E242B6FA8D42045B63222B4442), -1, sizeof(FB_tE8952E47A8F480E242B6FA8D42045B63222B4442_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6714[8] = 
{
	0,
	FB_tE8952E47A8F480E242B6FA8D42045B63222B4442_StaticFields::get_offset_of_facebook_5(),
	FB_tE8952E47A8F480E242B6FA8D42045B63222B4442_StaticFields::get_offset_of_isInitCalled_6(),
	FB_tE8952E47A8F480E242B6FA8D42045B63222B4442_StaticFields::get_offset_of_facebookDomain_7(),
	FB_tE8952E47A8F480E242B6FA8D42045B63222B4442_StaticFields::get_offset_of_graphApiVersion_8(),
	FB_tE8952E47A8F480E242B6FA8D42045B63222B4442_StaticFields::get_offset_of_U3CAppIdU3Ek__BackingField_9(),
	FB_tE8952E47A8F480E242B6FA8D42045B63222B4442_StaticFields::get_offset_of_U3CClientTokenU3Ek__BackingField_10(),
	FB_tE8952E47A8F480E242B6FA8D42045B63222B4442_StaticFields::get_offset_of_U3COnDLLLoadedDelegateU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6715 = { sizeof (OnDLLLoaded_tB74B93A2FF5819C30C822CB7B88B674DCF83DCD3), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6716 = { sizeof (Canvas_t72D35BCBD47DD5CBD1E3EA17576E7E67B464A3B8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6717 = { sizeof (Mobile_tF2F83CA4B7DB9B42FF98DD3D89AF275EF68F24BC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6718 = { sizeof (CompiledFacebookLoader_t00BC9903E387CA1A268154CB0B8132188627FA4F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6719 = { sizeof (U3CU3Ec__DisplayClass35_0_t7E602B315C674DD369ABB7D1622626680D7D6442), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6719[10] = 
{
	U3CU3Ec__DisplayClass35_0_t7E602B315C674DD369ABB7D1622626680D7D6442::get_offset_of_onInitComplete_0(),
	U3CU3Ec__DisplayClass35_0_t7E602B315C674DD369ABB7D1622626680D7D6442::get_offset_of_appId_1(),
	U3CU3Ec__DisplayClass35_0_t7E602B315C674DD369ABB7D1622626680D7D6442::get_offset_of_cookie_2(),
	U3CU3Ec__DisplayClass35_0_t7E602B315C674DD369ABB7D1622626680D7D6442::get_offset_of_logging_3(),
	U3CU3Ec__DisplayClass35_0_t7E602B315C674DD369ABB7D1622626680D7D6442::get_offset_of_status_4(),
	U3CU3Ec__DisplayClass35_0_t7E602B315C674DD369ABB7D1622626680D7D6442::get_offset_of_xfbml_5(),
	U3CU3Ec__DisplayClass35_0_t7E602B315C674DD369ABB7D1622626680D7D6442::get_offset_of_authResponse_6(),
	U3CU3Ec__DisplayClass35_0_t7E602B315C674DD369ABB7D1622626680D7D6442::get_offset_of_frictionlessRequests_7(),
	U3CU3Ec__DisplayClass35_0_t7E602B315C674DD369ABB7D1622626680D7D6442::get_offset_of_javascriptSDKLocale_8(),
	U3CU3Ec__DisplayClass35_0_t7E602B315C674DD369ABB7D1622626680D7D6442::get_offset_of_onHideUnity_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6720 = { sizeof (FacebookBase_t89F93EF12353405D2CEFAC7B59E860123FB472C7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6720[3] = 
{
	FacebookBase_t89F93EF12353405D2CEFAC7B59E860123FB472C7::get_offset_of_onInitCompleteDelegate_0(),
	FacebookBase_t89F93EF12353405D2CEFAC7B59E860123FB472C7::get_offset_of_U3CInitializedU3Ek__BackingField_1(),
	FacebookBase_t89F93EF12353405D2CEFAC7B59E860123FB472C7::get_offset_of_U3CCallbackManagerU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6721 = { sizeof (U3CU3Ec_t4490DE473F81406EFB0DF8DC550047B9E7D58824), -1, sizeof(U3CU3Ec_t4490DE473F81406EFB0DF8DC550047B9E7D58824_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6721[2] = 
{
	U3CU3Ec_t4490DE473F81406EFB0DF8DC550047B9E7D58824_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t4490DE473F81406EFB0DF8DC550047B9E7D58824_StaticFields::get_offset_of_U3CU3E9__41_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6722 = { sizeof (InitDelegate_tD9FF06C3949FD06E97E275F72D8F43AF4F496F47), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6723 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6724 = { sizeof (HideUnityDelegate_tFFB6D624086048B6FFFB85D9BC1946920684385B), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6725 = { sizeof (FacebookGameObject_t0EB6A31799B33368D3236B7D7E3A66E4ED3FF355), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6725[1] = 
{
	FacebookGameObject_t0EB6A31799B33368D3236B7D7E3A66E4ED3FF355::get_offset_of_U3CFacebookU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6726 = { sizeof (FacebookSdkVersion_t346709522EE2B2B782EAEC96031871CE25FE3FA1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6727 = { sizeof (FacebookUnityPlatform_t245EC6C7AD9DA9CDC2A40B725918837E19CEE49B)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6727[6] = 
{
	FacebookUnityPlatform_t245EC6C7AD9DA9CDC2A40B725918837E19CEE49B::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6728 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6729 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6730 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6731 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6732 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6733 = { sizeof (MethodArguments_t5BC48FC1AB90B7EF0A09C7CBF20F9F028C18FDFB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6733[1] = 
{
	MethodArguments_t5BC48FC1AB90B7EF0A09C7CBF20F9F028C18FDFB::get_offset_of_arguments_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6734 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6734[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6735 = { sizeof (ShareDialogMode_t40A2D9F39025821336E766D7D4FEA16006545BEB)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6735[5] = 
{
	ShareDialogMode_t40A2D9F39025821336E766D7D4FEA16006545BEB::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6736 = { sizeof (OGActionType_t244E4C9DC93B07294B27F0165CB60E0A84C59D47)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6736[4] = 
{
	OGActionType_t244E4C9DC93B07294B27F0165CB60E0A84C59D47::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6737 = { sizeof (AccessTokenRefreshResult_t9B2C219A86308A6F812DB3B7900BD65BB9D2974F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6737[1] = 
{
	AccessTokenRefreshResult_t9B2C219A86308A6F812DB3B7900BD65BB9D2974F::get_offset_of_U3CAccessTokenU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6738 = { sizeof (AppInviteResult_t855BB8DDD7557CB470052D0961D1EEADC1FA5AC5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6739 = { sizeof (AppLinkResult_tBBF0456C6F7898ACE53B383E3F6B23700171BC73), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6739[4] = 
{
	AppLinkResult_tBBF0456C6F7898ACE53B383E3F6B23700171BC73::get_offset_of_U3CUrlU3Ek__BackingField_6(),
	AppLinkResult_tBBF0456C6F7898ACE53B383E3F6B23700171BC73::get_offset_of_U3CTargetUrlU3Ek__BackingField_7(),
	AppLinkResult_tBBF0456C6F7898ACE53B383E3F6B23700171BC73::get_offset_of_U3CRefU3Ek__BackingField_8(),
	AppLinkResult_tBBF0456C6F7898ACE53B383E3F6B23700171BC73::get_offset_of_U3CExtrasU3Ek__BackingField_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6740 = { sizeof (AppRequestResult_t838349EBDF4E864EEE2AC0E501E1C1AE2EF2CC01), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6740[2] = 
{
	AppRequestResult_t838349EBDF4E864EEE2AC0E501E1C1AE2EF2CC01::get_offset_of_U3CRequestIDU3Ek__BackingField_6(),
	AppRequestResult_t838349EBDF4E864EEE2AC0E501E1C1AE2EF2CC01::get_offset_of_U3CToU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6741 = { sizeof (GraphResult_t3AF5558F4BE466D1C58FE83A3E54916F07E51F8A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6741[2] = 
{
	GraphResult_t3AF5558F4BE466D1C58FE83A3E54916F07E51F8A::get_offset_of_U3CResultListU3Ek__BackingField_6(),
	GraphResult_t3AF5558F4BE466D1C58FE83A3E54916F07E51F8A::get_offset_of_U3CTextureU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6742 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6743 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6744 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6745 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6746 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6747 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6748 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6749 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6750 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6751 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6752 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6753 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6754 = { sizeof (LoginResult_t7A3012297BB48C9E049AF971434529DE5912A912), -1, sizeof(LoginResult_t7A3012297BB48C9E049AF971434529DE5912A912_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6754[5] = 
{
	LoginResult_t7A3012297BB48C9E049AF971434529DE5912A912_StaticFields::get_offset_of_UserIdKey_6(),
	LoginResult_t7A3012297BB48C9E049AF971434529DE5912A912_StaticFields::get_offset_of_ExpirationTimestampKey_7(),
	LoginResult_t7A3012297BB48C9E049AF971434529DE5912A912_StaticFields::get_offset_of_PermissionsKey_8(),
	LoginResult_t7A3012297BB48C9E049AF971434529DE5912A912_StaticFields::get_offset_of_AccessTokenKey_9(),
	LoginResult_t7A3012297BB48C9E049AF971434529DE5912A912::get_offset_of_U3CAccessTokenU3Ek__BackingField_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6755 = { sizeof (PayResult_t5E5D8B2964B98530AA763B440FCFB137D01EB47E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6756 = { sizeof (ResultBase_tB17E9A196664F60E8B9B839509E7614CCD921EAB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6756[6] = 
{
	ResultBase_tB17E9A196664F60E8B9B839509E7614CCD921EAB::get_offset_of_U3CErrorU3Ek__BackingField_0(),
	ResultBase_tB17E9A196664F60E8B9B839509E7614CCD921EAB::get_offset_of_U3CResultDictionaryU3Ek__BackingField_1(),
	ResultBase_tB17E9A196664F60E8B9B839509E7614CCD921EAB::get_offset_of_U3CRawResultU3Ek__BackingField_2(),
	ResultBase_tB17E9A196664F60E8B9B839509E7614CCD921EAB::get_offset_of_U3CCancelledU3Ek__BackingField_3(),
	ResultBase_tB17E9A196664F60E8B9B839509E7614CCD921EAB::get_offset_of_U3CCallbackIdU3Ek__BackingField_4(),
	ResultBase_tB17E9A196664F60E8B9B839509E7614CCD921EAB::get_offset_of_U3CCanvasErrorCodeU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6757 = { sizeof (ResultContainer_t19BFBF119CCF533782EEA08E523DCFB899453602), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6757[3] = 
{
	0,
	ResultContainer_t19BFBF119CCF533782EEA08E523DCFB899453602::get_offset_of_U3CRawResultU3Ek__BackingField_1(),
	ResultContainer_t19BFBF119CCF533782EEA08E523DCFB899453602::get_offset_of_U3CResultDictionaryU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6758 = { sizeof (ShareResult_t2C8058416F1EBBE353840B0276F3E438E4E76B2C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6758[1] = 
{
	ShareResult_t2C8058416F1EBBE353840B0276F3E438E4E76B2C::get_offset_of_U3CPostIdU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6759 = { sizeof (AsyncRequestString_t33F30E800180F52474E4C86249DF12C85BFA9AFB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6759[5] = 
{
	AsyncRequestString_t33F30E800180F52474E4C86249DF12C85BFA9AFB::get_offset_of_url_4(),
	AsyncRequestString_t33F30E800180F52474E4C86249DF12C85BFA9AFB::get_offset_of_method_5(),
	AsyncRequestString_t33F30E800180F52474E4C86249DF12C85BFA9AFB::get_offset_of_formData_6(),
	AsyncRequestString_t33F30E800180F52474E4C86249DF12C85BFA9AFB::get_offset_of_query_7(),
	AsyncRequestString_t33F30E800180F52474E4C86249DF12C85BFA9AFB::get_offset_of_callback_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6760 = { sizeof (U3CStartU3Ed__9_t11FBA837E020FE9F38CE8B3AC18532C39AFB1E47), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6760[4] = 
{
	U3CStartU3Ed__9_t11FBA837E020FE9F38CE8B3AC18532C39AFB1E47::get_offset_of_U3CU3E1__state_0(),
	U3CStartU3Ed__9_t11FBA837E020FE9F38CE8B3AC18532C39AFB1E47::get_offset_of_U3CU3E2__current_1(),
	U3CStartU3Ed__9_t11FBA837E020FE9F38CE8B3AC18532C39AFB1E47::get_offset_of_U3CU3E4__this_2(),
	U3CStartU3Ed__9_t11FBA837E020FE9F38CE8B3AC18532C39AFB1E47::get_offset_of_U3CwwwU3E5__2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6761 = { sizeof (FacebookLogger_t6A0582DEF6D939F2BE21517040545DF936A41BC4), -1, sizeof(FacebookLogger_t6A0582DEF6D939F2BE21517040545DF936A41BC4_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6761[1] = 
{
	FacebookLogger_t6A0582DEF6D939F2BE21517040545DF936A41BC4_StaticFields::get_offset_of_U3CInstanceU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6762 = { sizeof (DebugLogger_tA488557789DB3901B501A9F673800D999F6681F6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6763 = { sizeof (HttpMethod_t2F212F7FFB35F0534ED41BFA3A8999EC664AC71B)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6763[4] = 
{
	HttpMethod_t2F212F7FFB35F0534ED41BFA3A8999EC664AC71B::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6764 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6765 = { sizeof (Utilities_t38892FCEB1858136314024DA4782D94B8806D7BD), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6766 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6767 = { sizeof (U3CU3Ec_t02687171AEE0F550EB7319FF63E2F93FF89B7CE2), -1, sizeof(U3CU3Ec_t02687171AEE0F550EB7319FF63E2F93FF89B7CE2_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6767[2] = 
{
	U3CU3Ec_t02687171AEE0F550EB7319FF63E2F93FF89B7CE2_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t02687171AEE0F550EB7319FF63E2F93FF89B7CE2_StaticFields::get_offset_of_U3CU3E9__18_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6768 = { sizeof (FBUnityUtility_tDE1EAB5E50BF646A5D2ED896C45CA031C976DDE1), -1, sizeof(FBUnityUtility_tDE1EAB5E50BF646A5D2ED896C45CA031C976DDE1_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6768[1] = 
{
	FBUnityUtility_tDE1EAB5E50BF646A5D2ED896C45CA031C976DDE1_StaticFields::get_offset_of_asyncRequestStringWrapper_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6769 = { sizeof (AsyncRequestStringWrapper_tC819E49764AFCD1846BD890E46A836429B314072), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6770 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6771 = { sizeof (FacebookScheduler_tE073D0E8755AB0B15F52B553095716ED9465DBD1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6772 = { sizeof (U3CDelayEventU3Ed__1_tCD85F1EE7CE5D7F214560EF1955ECE205BAA4C1D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6772[4] = 
{
	U3CDelayEventU3Ed__1_tCD85F1EE7CE5D7F214560EF1955ECE205BAA4C1D::get_offset_of_U3CU3E1__state_0(),
	U3CDelayEventU3Ed__1_tCD85F1EE7CE5D7F214560EF1955ECE205BAA4C1D::get_offset_of_U3CU3E2__current_1(),
	U3CDelayEventU3Ed__1_tCD85F1EE7CE5D7F214560EF1955ECE205BAA4C1D::get_offset_of_delay_2(),
	U3CDelayEventU3Ed__1_tCD85F1EE7CE5D7F214560EF1955ECE205BAA4C1D::get_offset_of_action_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6773 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6774 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6775 = { sizeof (CodelessIAPAutoLog_t504E37E47A83CE3165C2B715EBE243A7CAC84ED1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6776 = { sizeof (GameroomFacebook_t7C9C84A6698D3491CEC1C6AED8752AD52C85233A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6776[3] = 
{
	GameroomFacebook_t7C9C84A6698D3491CEC1C6AED8752AD52C85233A::get_offset_of_appId_3(),
	GameroomFacebook_t7C9C84A6698D3491CEC1C6AED8752AD52C85233A::get_offset_of_gameroomWrapper_4(),
	GameroomFacebook_t7C9C84A6698D3491CEC1C6AED8752AD52C85233A::get_offset_of_U3CLimitEventUsageU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6777 = { sizeof (OnComplete_t6617755663754FE5E8E8F946F5639627C5589513), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6778 = { sizeof (GameroomFacebookGameObject_t88ED0BA9A30FDBD5312F126A32F8147B95191A0E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6779 = { sizeof (U3CWaitForPipeResponseU3Ed__4_t45F560A5F0488A30AD17E48606523BDB3E7854FC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6779[5] = 
{
	U3CWaitForPipeResponseU3Ed__4_t45F560A5F0488A30AD17E48606523BDB3E7854FC::get_offset_of_U3CU3E1__state_0(),
	U3CWaitForPipeResponseU3Ed__4_t45F560A5F0488A30AD17E48606523BDB3E7854FC::get_offset_of_U3CU3E2__current_1(),
	U3CWaitForPipeResponseU3Ed__4_t45F560A5F0488A30AD17E48606523BDB3E7854FC::get_offset_of_U3CU3E4__this_2(),
	U3CWaitForPipeResponseU3Ed__4_t45F560A5F0488A30AD17E48606523BDB3E7854FC::get_offset_of_onCompleteDelegate_3(),
	U3CWaitForPipeResponseU3Ed__4_t45F560A5F0488A30AD17E48606523BDB3E7854FC::get_offset_of_callbackId_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6780 = { sizeof (GameroomFacebookLoader_tD33DC4194829118EB3EE64ECEA36BCF489666E91), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6781 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6782 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6783 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6784 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6785 = { sizeof (EditorFacebook_tAB2305F929DE16DBA0CB25C5DC0D0D690E72CC97), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6785[3] = 
{
	EditorFacebook_tAB2305F929DE16DBA0CB25C5DC0D0D690E72CC97::get_offset_of_editorWrapper_3(),
	EditorFacebook_tAB2305F929DE16DBA0CB25C5DC0D0D690E72CC97::get_offset_of_U3CLimitEventUsageU3Ek__BackingField_4(),
	EditorFacebook_tAB2305F929DE16DBA0CB25C5DC0D0D690E72CC97::get_offset_of_U3CShareDialogModeU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6786 = { sizeof (EditorFacebookGameObject_tDD2E0D58DB5C8DA3C9125564F44A5D1F684F1FA1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6787 = { sizeof (EditorFacebookLoader_tA47B89F4EA01778D9DD0C71EEB23A17E3CFE99A9), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6788 = { sizeof (EditorFacebookMockDialog_t43C7DA74401C1D0F653CAB314D17E2A60E212A3F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6788[4] = 
{
	EditorFacebookMockDialog_t43C7DA74401C1D0F653CAB314D17E2A60E212A3F::get_offset_of_modalRect_4(),
	EditorFacebookMockDialog_t43C7DA74401C1D0F653CAB314D17E2A60E212A3F::get_offset_of_modalStyle_5(),
	EditorFacebookMockDialog_t43C7DA74401C1D0F653CAB314D17E2A60E212A3F::get_offset_of_U3CCallbackU3Ek__BackingField_6(),
	EditorFacebookMockDialog_t43C7DA74401C1D0F653CAB314D17E2A60E212A3F::get_offset_of_U3CCallbackIDU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6789 = { sizeof (EditorWrapper_tF0E63F8963D378246332CA72407B583137FD4CDC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6789[1] = 
{
	EditorWrapper_tF0E63F8963D378246332CA72407B583137FD4CDC::get_offset_of_callbackHandler_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6790 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6791 = { sizeof (EmptyMockDialog_t65530C5D13B47C894CE2A9A4656580EB515547E4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6791[1] = 
{
	EmptyMockDialog_t65530C5D13B47C894CE2A9A4656580EB515547E4::get_offset_of_U3CEmptyDialogTitleU3Ek__BackingField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6792 = { sizeof (MockLoginDialog_t4B515228F0AE861F1C4067E1BC5AC26EE210F5B8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6792[1] = 
{
	MockLoginDialog_t4B515228F0AE861F1C4067E1BC5AC26EE210F5B8::get_offset_of_accessToken_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6793 = { sizeof (U3CU3Ec__DisplayClass4_0_t89BFEA810AF32D3E8BE3B52E1060F30E3883E618), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6793[2] = 
{
	U3CU3Ec__DisplayClass4_0_t89BFEA810AF32D3E8BE3B52E1060F30E3883E618::get_offset_of_facebookID_0(),
	U3CU3Ec__DisplayClass4_0_t89BFEA810AF32D3E8BE3B52E1060F30E3883E618::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6794 = { sizeof (MockShareDialog_tC2C1394FC8DAFC55285B3D66BF8A2C0586700E7B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6794[1] = 
{
	MockShareDialog_tC2C1394FC8DAFC55285B3D66BF8A2C0586700E7B::get_offset_of_U3CSubTitleU3Ek__BackingField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6795 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6796 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6797 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6798 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6799 = { sizeof (MobileFacebook_t27A7B4F062FC7F3935525F591E59A411462EF919), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6799[1] = 
{
	MobileFacebook_t27A7B4F062FC7F3935525F591E59A411462EF919::get_offset_of_shareDialogMode_3(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
