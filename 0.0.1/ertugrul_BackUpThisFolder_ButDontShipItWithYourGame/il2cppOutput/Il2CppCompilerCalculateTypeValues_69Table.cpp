﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// GameSparks.Core.GSData
struct GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937;
// GameSparks.Platforms.PlatformBase
struct PlatformBase_tCBFDE4E2C964A9DC16353A32B6F7974D5107187B;
// GameSparks.Platforms.TimerController
struct TimerController_t0671C34BEC0EC688FD4BF412F0683EFCC40F007D;
// MoenenGames.CyanLevelEditor.CyanActionSetting
struct CyanActionSetting_t52F579D228577FDDE87DC71F2C2DAB5B6CB7F46E;
// MoenenGames.CyanLevelEditor.CyanCameraSetting
struct CyanCameraSetting_t206CA210D2F54015079229DBC666790D10EB7237;
// MoenenGames.CyanLevelEditor.CyanComponent
struct CyanComponent_tC7880F9103B559C4E6EB97745CE3A56781CDB3BC;
// MoenenGames.CyanLevelEditor.CyanCursorSetting
struct CyanCursorSetting_t80DBA731373075E7ACA488DBB3733A88089199D3;
// MoenenGames.CyanLevelEditor.CyanGridSetting
struct CyanGridSetting_tF2D64053897D26131BDCF7A3F2675BD2161BE8CF;
// MoenenGames.CyanLevelEditor.CyanLevelEditor
struct CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6;
// MoenenGames.CyanLevelEditor.CyanMessage
struct CyanMessage_t5A284AED26BF63EDC0379206EBE0289EDF8B4D26;
// MoenenGames.CyanLevelEditor.CyanMessage/CyanIntIntRvent
struct CyanIntIntRvent_t3050562EFC3FABA385FF4019E611B224C7F39B2F;
// MoenenGames.CyanLevelEditor.CyanMessage/CyanIntRvent
struct CyanIntRvent_t36B9E29A72B590D0D9FEDE9A938B67733AB53FC0;
// MoenenGames.CyanLevelEditor.CyanMessage/CyanTransformEvent
struct CyanTransformEvent_t9A71D6662200E71DA050209321EBE077E3FD117C;
// MoenenGames.CyanLevelEditor.CyanObjectPool/Prefab[]
struct PrefabU5BU5D_t6275A85F607D793C12F6AE6F46A8EB81F4F4A0BA;
// MoenenGames.CyanLevelEditor.CyanObjectPool[]
struct CyanObjectPoolU5BU5D_tB4F9ED39CA989B62B12CCC787FA3FEFCE3D0C3F8;
// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.Action`1<System.Exception>
struct Action_1_t114FA5D778AE43CC5A12F16A8CCD137F67ED6962;
// System.Attribute
struct Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.IEnumerator`1<System.Reflection.FieldInfo>
struct IEnumerator_1_tFF29F758EE2F45C6B96A41F6379516A23E129A8C;
// System.Collections.Generic.IEnumerator`1<System.Reflection.MethodInfo>
struct IEnumerator_1_t7A8759013AA69D1FE49168C24F3A6826B9FB3D3F;
// System.Collections.Generic.IEnumerator`1<System.Reflection.PropertyInfo>
struct IEnumerator_1_t537D797E2644AF9046B1E4CAAC405D8CE9D01534;
// System.Collections.Generic.IEnumerator`1<System.Type>
struct IEnumerator_1_t9F60FD6E97DB04D8FC2044CDD1C2F74C8FC3678E;
// System.Collections.Generic.List`1<ArabicMapping>
struct List_1_t022F5E210841DAA46FF81D72C234B97CDAE42DA7;
// System.Collections.Generic.List`1<GameSparks.Platforms.IControlledTimer>
struct List_1_t0AE33103621FBB0BEED00AC15A04D3C4EE6A4720;
// System.Collections.Generic.List`1<GameSparks.Platforms.IControlledWebSocket>
struct List_1_tA6F692DEC0215E08FB5821E35DE9B5D5C2B35EDC;
// System.Collections.Generic.List`1<MoenenGames.CyanLevelEditor.ActionInfo>
struct List_1_tC9F26360E86F3EED335ADB7E6838E9C069B6039F;
// System.Collections.Generic.List`1<MoenenGames.CyanLevelEditor.ItemRawData>
struct List_1_t295A774ECA33315CFD19005983613AC9E19B4992;
// System.Collections.Generic.List`1<MoenenGames.CyanLevelEditor.MapData/MapItem>
struct List_1_t4A3A177747B5B9DEA2C8FE1D93FAB0830EBCA7D5;
// System.Collections.Generic.List`1<System.Action>
struct List_1_t9E7F53A878191B703A76E69CE36AA90A372800E7;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Reflection.FieldInfo
struct FieldInfo_t;
// System.Reflection.FieldInfo[]
struct FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Reflection.MethodInfo[]
struct MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B;
// System.Reflection.PropertyInfo
struct PropertyInfo_t;
// System.Reflection.PropertyInfo[]
struct PropertyInfoU5BU5D_tAD8E99B12FF99CA4F2EA37B612DE68E112B4CF7E;
// System.String
struct String_t;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.Camera
struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34;
// UnityEngine.Collider
struct Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520;
// UnityEngine.RectTransform
struct RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20;
// UnityEngine.Sprite
struct Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.UI.Toggle
struct Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106;




#ifndef U3CMODULEU3E_T2420DD831EDFA31B0432655ED654E5827F2A336B_H
#define U3CMODULEU3E_T2420DD831EDFA31B0432655ED654E5827F2A336B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t2420DD831EDFA31B0432655ED654E5827F2A336B 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T2420DD831EDFA31B0432655ED654E5827F2A336B_H
#ifndef U3CMODULEU3E_TD1EFFA6201164FCC6E8DD2EDD5EE15545901839B_H
#define U3CMODULEU3E_TD1EFFA6201164FCC6E8DD2EDD5EE15545901839B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_tD1EFFA6201164FCC6E8DD2EDD5EE15545901839B 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_TD1EFFA6201164FCC6E8DD2EDD5EE15545901839B_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef ARABICFIXERTOOL_TF486A04CCCDA6C1029950FA33B72987A695104BE_H
#define ARABICFIXERTOOL_TF486A04CCCDA6C1029950FA33B72987A695104BE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ArabicFixerTool
struct  ArabicFixerTool_tF486A04CCCDA6C1029950FA33B72987A695104BE  : public RuntimeObject
{
public:

public:
};

struct ArabicFixerTool_tF486A04CCCDA6C1029950FA33B72987A695104BE_StaticFields
{
public:
	// System.Boolean ArabicFixerTool::showTashkeel
	bool ___showTashkeel_0;
	// System.Boolean ArabicFixerTool::combineTashkeel
	bool ___combineTashkeel_1;
	// System.Boolean ArabicFixerTool::useHinduNumbers
	bool ___useHinduNumbers_2;

public:
	inline static int32_t get_offset_of_showTashkeel_0() { return static_cast<int32_t>(offsetof(ArabicFixerTool_tF486A04CCCDA6C1029950FA33B72987A695104BE_StaticFields, ___showTashkeel_0)); }
	inline bool get_showTashkeel_0() const { return ___showTashkeel_0; }
	inline bool* get_address_of_showTashkeel_0() { return &___showTashkeel_0; }
	inline void set_showTashkeel_0(bool value)
	{
		___showTashkeel_0 = value;
	}

	inline static int32_t get_offset_of_combineTashkeel_1() { return static_cast<int32_t>(offsetof(ArabicFixerTool_tF486A04CCCDA6C1029950FA33B72987A695104BE_StaticFields, ___combineTashkeel_1)); }
	inline bool get_combineTashkeel_1() const { return ___combineTashkeel_1; }
	inline bool* get_address_of_combineTashkeel_1() { return &___combineTashkeel_1; }
	inline void set_combineTashkeel_1(bool value)
	{
		___combineTashkeel_1 = value;
	}

	inline static int32_t get_offset_of_useHinduNumbers_2() { return static_cast<int32_t>(offsetof(ArabicFixerTool_tF486A04CCCDA6C1029950FA33B72987A695104BE_StaticFields, ___useHinduNumbers_2)); }
	inline bool get_useHinduNumbers_2() const { return ___useHinduNumbers_2; }
	inline bool* get_address_of_useHinduNumbers_2() { return &___useHinduNumbers_2; }
	inline void set_useHinduNumbers_2(bool value)
	{
		___useHinduNumbers_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARABICFIXERTOOL_TF486A04CCCDA6C1029950FA33B72987A695104BE_H
#ifndef ARABICMAPPING_T70BF7202D9CD13A070774F647DA888EA258B7EA2_H
#define ARABICMAPPING_T70BF7202D9CD13A070774F647DA888EA258B7EA2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ArabicMapping
struct  ArabicMapping_t70BF7202D9CD13A070774F647DA888EA258B7EA2  : public RuntimeObject
{
public:
	// System.Int32 ArabicMapping::from
	int32_t ___from_0;
	// System.Int32 ArabicMapping::to
	int32_t ___to_1;

public:
	inline static int32_t get_offset_of_from_0() { return static_cast<int32_t>(offsetof(ArabicMapping_t70BF7202D9CD13A070774F647DA888EA258B7EA2, ___from_0)); }
	inline int32_t get_from_0() const { return ___from_0; }
	inline int32_t* get_address_of_from_0() { return &___from_0; }
	inline void set_from_0(int32_t value)
	{
		___from_0 = value;
	}

	inline static int32_t get_offset_of_to_1() { return static_cast<int32_t>(offsetof(ArabicMapping_t70BF7202D9CD13A070774F647DA888EA258B7EA2, ___to_1)); }
	inline int32_t get_to_1() const { return ___to_1; }
	inline int32_t* get_address_of_to_1() { return &___to_1; }
	inline void set_to_1(int32_t value)
	{
		___to_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARABICMAPPING_T70BF7202D9CD13A070774F647DA888EA258B7EA2_H
#ifndef ARABICTABLE_TABF52F1916CF2138C65717244746EEC11E620DB1_H
#define ARABICTABLE_TABF52F1916CF2138C65717244746EEC11E620DB1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ArabicTable
struct  ArabicTable_tABF52F1916CF2138C65717244746EEC11E620DB1  : public RuntimeObject
{
public:

public:
};

struct ArabicTable_tABF52F1916CF2138C65717244746EEC11E620DB1_StaticFields
{
public:
	// System.Collections.Generic.List`1<ArabicMapping> ArabicTable::mapList
	List_1_t022F5E210841DAA46FF81D72C234B97CDAE42DA7 * ___mapList_0;
	// ArabicTable ArabicTable::arabicMapper
	ArabicTable_tABF52F1916CF2138C65717244746EEC11E620DB1 * ___arabicMapper_1;

public:
	inline static int32_t get_offset_of_mapList_0() { return static_cast<int32_t>(offsetof(ArabicTable_tABF52F1916CF2138C65717244746EEC11E620DB1_StaticFields, ___mapList_0)); }
	inline List_1_t022F5E210841DAA46FF81D72C234B97CDAE42DA7 * get_mapList_0() const { return ___mapList_0; }
	inline List_1_t022F5E210841DAA46FF81D72C234B97CDAE42DA7 ** get_address_of_mapList_0() { return &___mapList_0; }
	inline void set_mapList_0(List_1_t022F5E210841DAA46FF81D72C234B97CDAE42DA7 * value)
	{
		___mapList_0 = value;
		Il2CppCodeGenWriteBarrier((&___mapList_0), value);
	}

	inline static int32_t get_offset_of_arabicMapper_1() { return static_cast<int32_t>(offsetof(ArabicTable_tABF52F1916CF2138C65717244746EEC11E620DB1_StaticFields, ___arabicMapper_1)); }
	inline ArabicTable_tABF52F1916CF2138C65717244746EEC11E620DB1 * get_arabicMapper_1() const { return ___arabicMapper_1; }
	inline ArabicTable_tABF52F1916CF2138C65717244746EEC11E620DB1 ** get_address_of_arabicMapper_1() { return &___arabicMapper_1; }
	inline void set_arabicMapper_1(ArabicTable_tABF52F1916CF2138C65717244746EEC11E620DB1 * value)
	{
		___arabicMapper_1 = value;
		Il2CppCodeGenWriteBarrier((&___arabicMapper_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARABICTABLE_TABF52F1916CF2138C65717244746EEC11E620DB1_H
#ifndef COMMANDLINEUTILS_TC12399B1E5E0D200EEA1299E720BE5AD11DB1873_H
#define COMMANDLINEUTILS_TC12399B1E5E0D200EEA1299E720BE5AD11DB1873_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CommandLineUtils
struct  CommandLineUtils_tC12399B1E5E0D200EEA1299E720BE5AD11DB1873  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMMANDLINEUTILS_TC12399B1E5E0D200EEA1299E720BE5AD11DB1873_H
#ifndef COMPILE_T1D82CE8E8A091D34E18CAC282E75C98B81B85B89_H
#define COMPILE_T1D82CE8E8A091D34E18CAC282E75C98B81B85B89_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DesperateDevs.CodeGeneration.CodeGenerator.Unity.Editor.Compile
struct  Compile_t1D82CE8E8A091D34E18CAC282E75C98B81B85B89  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPILE_T1D82CE8E8A091D34E18CAC282E75C98B81B85B89_H
#ifndef FASTCONCATUTILITY_T043C8ABBE1B55F767D84E6AB4FB3781438179706_H
#define FASTCONCATUTILITY_T043C8ABBE1B55F767D84E6AB4FB3781438179706_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FastConcatUtility
struct  FastConcatUtility_t043C8ABBE1B55F767D84E6AB4FB3781438179706  : public RuntimeObject
{
public:

public:
};

struct FastConcatUtility_t043C8ABBE1B55F767D84E6AB4FB3781438179706_StaticFields
{
public:
	// System.Text.StringBuilder FastConcatUtility::_stringBuilder
	StringBuilder_t * ____stringBuilder_0;

public:
	inline static int32_t get_offset_of__stringBuilder_0() { return static_cast<int32_t>(offsetof(FastConcatUtility_t043C8ABBE1B55F767D84E6AB4FB3781438179706_StaticFields, ____stringBuilder_0)); }
	inline StringBuilder_t * get__stringBuilder_0() const { return ____stringBuilder_0; }
	inline StringBuilder_t ** get_address_of__stringBuilder_0() { return &____stringBuilder_0; }
	inline void set__stringBuilder_0(StringBuilder_t * value)
	{
		____stringBuilder_0 = value;
		Il2CppCodeGenWriteBarrier((&____stringBuilder_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FASTCONCATUTILITY_T043C8ABBE1B55F767D84E6AB4FB3781438179706_H
#ifndef U3CU3EC__DISPLAYCLASS52_0_T41E4EEB3622B04A2519695B52E97DA725AC6AA4D_H
#define U3CU3EC__DISPLAYCLASS52_0_T41E4EEB3622B04A2519695B52E97DA725AC6AA4D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Platforms.PlatformBase_<>c__DisplayClass52_0
struct  U3CU3Ec__DisplayClass52_0_t41E4EEB3622B04A2519695B52E97DA725AC6AA4D  : public RuntimeObject
{
public:
	// System.String GameSparks.Platforms.PlatformBase_<>c__DisplayClass52_0::message
	String_t* ___message_0;

public:
	inline static int32_t get_offset_of_message_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass52_0_t41E4EEB3622B04A2519695B52E97DA725AC6AA4D, ___message_0)); }
	inline String_t* get_message_0() const { return ___message_0; }
	inline String_t** get_address_of_message_0() { return &___message_0; }
	inline void set_message_0(String_t* value)
	{
		___message_0 = value;
		Il2CppCodeGenWriteBarrier((&___message_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS52_0_T41E4EEB3622B04A2519695B52E97DA725AC6AA4D_H
#ifndef U3CDELAYEDQUITU3ED__11_T52EAB0DA42199004026ED4F53BF31E73B223B79B_H
#define U3CDELAYEDQUITU3ED__11_T52EAB0DA42199004026ED4F53BF31E73B223B79B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Platforms.PlatformBase_<DelayedQuit>d__11
struct  U3CDelayedQuitU3Ed__11_t52EAB0DA42199004026ED4F53BF31E73B223B79B  : public RuntimeObject
{
public:
	// System.Int32 GameSparks.Platforms.PlatformBase_<DelayedQuit>d__11::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object GameSparks.Platforms.PlatformBase_<DelayedQuit>d__11::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// GameSparks.Platforms.PlatformBase GameSparks.Platforms.PlatformBase_<DelayedQuit>d__11::<>4__this
	PlatformBase_tCBFDE4E2C964A9DC16353A32B6F7974D5107187B * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CDelayedQuitU3Ed__11_t52EAB0DA42199004026ED4F53BF31E73B223B79B, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CDelayedQuitU3Ed__11_t52EAB0DA42199004026ED4F53BF31E73B223B79B, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CDelayedQuitU3Ed__11_t52EAB0DA42199004026ED4F53BF31E73B223B79B, ___U3CU3E4__this_2)); }
	inline PlatformBase_tCBFDE4E2C964A9DC16353A32B6F7974D5107187B * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline PlatformBase_tCBFDE4E2C964A9DC16353A32B6F7974D5107187B ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(PlatformBase_tCBFDE4E2C964A9DC16353A32B6F7974D5107187B * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDELAYEDQUITU3ED__11_T52EAB0DA42199004026ED4F53BF31E73B223B79B_H
#ifndef TIMERCONTROLLER_T0671C34BEC0EC688FD4BF412F0683EFCC40F007D_H
#define TIMERCONTROLLER_T0671C34BEC0EC688FD4BF412F0683EFCC40F007D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Platforms.TimerController
struct  TimerController_t0671C34BEC0EC688FD4BF412F0683EFCC40F007D  : public RuntimeObject
{
public:
	// System.Int64 GameSparks.Platforms.TimerController::timeOfLastUpdate
	int64_t ___timeOfLastUpdate_0;
	// System.Collections.Generic.List`1<GameSparks.Platforms.IControlledTimer> GameSparks.Platforms.TimerController::timers
	List_1_t0AE33103621FBB0BEED00AC15A04D3C4EE6A4720 * ___timers_1;

public:
	inline static int32_t get_offset_of_timeOfLastUpdate_0() { return static_cast<int32_t>(offsetof(TimerController_t0671C34BEC0EC688FD4BF412F0683EFCC40F007D, ___timeOfLastUpdate_0)); }
	inline int64_t get_timeOfLastUpdate_0() const { return ___timeOfLastUpdate_0; }
	inline int64_t* get_address_of_timeOfLastUpdate_0() { return &___timeOfLastUpdate_0; }
	inline void set_timeOfLastUpdate_0(int64_t value)
	{
		___timeOfLastUpdate_0 = value;
	}

	inline static int32_t get_offset_of_timers_1() { return static_cast<int32_t>(offsetof(TimerController_t0671C34BEC0EC688FD4BF412F0683EFCC40F007D, ___timers_1)); }
	inline List_1_t0AE33103621FBB0BEED00AC15A04D3C4EE6A4720 * get_timers_1() const { return ___timers_1; }
	inline List_1_t0AE33103621FBB0BEED00AC15A04D3C4EE6A4720 ** get_address_of_timers_1() { return &___timers_1; }
	inline void set_timers_1(List_1_t0AE33103621FBB0BEED00AC15A04D3C4EE6A4720 * value)
	{
		___timers_1 = value;
		Il2CppCodeGenWriteBarrier((&___timers_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMERCONTROLLER_T0671C34BEC0EC688FD4BF412F0683EFCC40F007D_H
#ifndef UNITYTIMER_T5A0BA21B61DF888D9D7E26A10716DB28FB9D90C5_H
#define UNITYTIMER_T5A0BA21B61DF888D9D7E26A10716DB28FB9D90C5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Platforms.UnityTimer
struct  UnityTimer_t5A0BA21B61DF888D9D7E26A10716DB28FB9D90C5  : public RuntimeObject
{
public:
	// System.Action GameSparks.Platforms.UnityTimer::callback
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___callback_0;
	// System.Int32 GameSparks.Platforms.UnityTimer::interval
	int32_t ___interval_1;
	// System.Int64 GameSparks.Platforms.UnityTimer::elapsedTicks
	int64_t ___elapsedTicks_2;
	// System.Boolean GameSparks.Platforms.UnityTimer::running
	bool ___running_3;
	// GameSparks.Platforms.TimerController GameSparks.Platforms.UnityTimer::controller
	TimerController_t0671C34BEC0EC688FD4BF412F0683EFCC40F007D * ___controller_4;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(UnityTimer_t5A0BA21B61DF888D9D7E26A10716DB28FB9D90C5, ___callback_0)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_callback_0() const { return ___callback_0; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}

	inline static int32_t get_offset_of_interval_1() { return static_cast<int32_t>(offsetof(UnityTimer_t5A0BA21B61DF888D9D7E26A10716DB28FB9D90C5, ___interval_1)); }
	inline int32_t get_interval_1() const { return ___interval_1; }
	inline int32_t* get_address_of_interval_1() { return &___interval_1; }
	inline void set_interval_1(int32_t value)
	{
		___interval_1 = value;
	}

	inline static int32_t get_offset_of_elapsedTicks_2() { return static_cast<int32_t>(offsetof(UnityTimer_t5A0BA21B61DF888D9D7E26A10716DB28FB9D90C5, ___elapsedTicks_2)); }
	inline int64_t get_elapsedTicks_2() const { return ___elapsedTicks_2; }
	inline int64_t* get_address_of_elapsedTicks_2() { return &___elapsedTicks_2; }
	inline void set_elapsedTicks_2(int64_t value)
	{
		___elapsedTicks_2 = value;
	}

	inline static int32_t get_offset_of_running_3() { return static_cast<int32_t>(offsetof(UnityTimer_t5A0BA21B61DF888D9D7E26A10716DB28FB9D90C5, ___running_3)); }
	inline bool get_running_3() const { return ___running_3; }
	inline bool* get_address_of_running_3() { return &___running_3; }
	inline void set_running_3(bool value)
	{
		___running_3 = value;
	}

	inline static int32_t get_offset_of_controller_4() { return static_cast<int32_t>(offsetof(UnityTimer_t5A0BA21B61DF888D9D7E26A10716DB28FB9D90C5, ___controller_4)); }
	inline TimerController_t0671C34BEC0EC688FD4BF412F0683EFCC40F007D * get_controller_4() const { return ___controller_4; }
	inline TimerController_t0671C34BEC0EC688FD4BF412F0683EFCC40F007D ** get_address_of_controller_4() { return &___controller_4; }
	inline void set_controller_4(TimerController_t0671C34BEC0EC688FD4BF412F0683EFCC40F007D * value)
	{
		___controller_4 = value;
		Il2CppCodeGenWriteBarrier((&___controller_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYTIMER_T5A0BA21B61DF888D9D7E26A10716DB28FB9D90C5_H
#ifndef LISTEXTENSION_TF877099FF04236ED7637DB297435B8DB2185F540_H
#define LISTEXTENSION_TF877099FF04236ED7637DB297435B8DB2185F540_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ListExtension
struct  ListExtension_tF877099FF04236ED7637DB297435B8DB2185F540  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LISTEXTENSION_TF877099FF04236ED7637DB297435B8DB2185F540_H
#ifndef ASSERT_T32438BFB9A55883E4D28CFDA4E439285219D71B6_H
#define ASSERT_T32438BFB9A55883E4D28CFDA4E439285219D71B6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModestTree.Assert
struct  Assert_t32438BFB9A55883E4D28CFDA4E439285219D71B6  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSERT_T32438BFB9A55883E4D28CFDA4E439285219D71B6_H
#ifndef LINQEXTENSIONS_TBA515E336DD5AAA0EBEA00C99E72CDEF1687D3EA_H
#define LINQEXTENSIONS_TBA515E336DD5AAA0EBEA00C99E72CDEF1687D3EA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModestTree.LinqExtensions
struct  LinqExtensions_tBA515E336DD5AAA0EBEA00C99E72CDEF1687D3EA  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINQEXTENSIONS_TBA515E336DD5AAA0EBEA00C99E72CDEF1687D3EA_H
#ifndef LOG_TC116D6C973E7531C2AA4AA0C12A6AE7D0A691B40_H
#define LOG_TC116D6C973E7531C2AA4AA0C12A6AE7D0A691B40_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModestTree.Log
struct  Log_tC116D6C973E7531C2AA4AA0C12A6AE7D0A691B40  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOG_TC116D6C973E7531C2AA4AA0C12A6AE7D0A691B40_H
#ifndef MISCEXTENSIONS_TF3AAB0F6CDB9245A5F8ACC2831221532F6B641FF_H
#define MISCEXTENSIONS_TF3AAB0F6CDB9245A5F8ACC2831221532F6B641FF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModestTree.MiscExtensions
struct  MiscExtensions_tF3AAB0F6CDB9245A5F8ACC2831221532F6B641FF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MISCEXTENSIONS_TF3AAB0F6CDB9245A5F8ACC2831221532F6B641FF_H
#ifndef TYPEEXTENSIONS_TED5DBFEEF4E4BE76D253B3393C6D36E7552C0196_H
#define TYPEEXTENSIONS_TED5DBFEEF4E4BE76D253B3393C6D36E7552C0196_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModestTree.TypeExtensions
struct  TypeExtensions_tED5DBFEEF4E4BE76D253B3393C6D36E7552C0196  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEEXTENSIONS_TED5DBFEEF4E4BE76D253B3393C6D36E7552C0196_H
#ifndef U3CU3EC__DISPLAYCLASS35_0_T708B31425175507F80B7B782BD80203EC7AA6441_H
#define U3CU3EC__DISPLAYCLASS35_0_T708B31425175507F80B7B782BD80203EC7AA6441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModestTree.TypeExtensions_<>c__DisplayClass35_0
struct  U3CU3Ec__DisplayClass35_0_t708B31425175507F80B7B782BD80203EC7AA6441  : public RuntimeObject
{
public:
	// System.Type[] ModestTree.TypeExtensions_<>c__DisplayClass35_0::attributeTypes
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___attributeTypes_0;

public:
	inline static int32_t get_offset_of_attributeTypes_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass35_0_t708B31425175507F80B7B782BD80203EC7AA6441, ___attributeTypes_0)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get_attributeTypes_0() const { return ___attributeTypes_0; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of_attributeTypes_0() { return &___attributeTypes_0; }
	inline void set_attributeTypes_0(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		___attributeTypes_0 = value;
		Il2CppCodeGenWriteBarrier((&___attributeTypes_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS35_0_T708B31425175507F80B7B782BD80203EC7AA6441_H
#ifndef U3CU3EC__DISPLAYCLASS35_1_TE3F17278B9F72ABDD93289E988A9BD816B77406A_H
#define U3CU3EC__DISPLAYCLASS35_1_TE3F17278B9F72ABDD93289E988A9BD816B77406A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModestTree.TypeExtensions_<>c__DisplayClass35_1
struct  U3CU3Ec__DisplayClass35_1_tE3F17278B9F72ABDD93289E988A9BD816B77406A  : public RuntimeObject
{
public:
	// System.Attribute ModestTree.TypeExtensions_<>c__DisplayClass35_1::a
	Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74 * ___a_0;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass35_1_tE3F17278B9F72ABDD93289E988A9BD816B77406A, ___a_0)); }
	inline Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74 * get_a_0() const { return ___a_0; }
	inline Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74 ** get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74 * value)
	{
		___a_0 = value;
		Il2CppCodeGenWriteBarrier((&___a_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS35_1_TE3F17278B9F72ABDD93289E988A9BD816B77406A_H
#ifndef U3CU3EC__DISPLAYCLASS39_0_T9526CECAB44E4C658FD5B8B1CFF0FCEF9C1EB4F1_H
#define U3CU3EC__DISPLAYCLASS39_0_T9526CECAB44E4C658FD5B8B1CFF0FCEF9C1EB4F1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModestTree.TypeExtensions_<>c__DisplayClass39_0
struct  U3CU3Ec__DisplayClass39_0_t9526CECAB44E4C658FD5B8B1CFF0FCEF9C1EB4F1  : public RuntimeObject
{
public:
	// System.Type[] ModestTree.TypeExtensions_<>c__DisplayClass39_0::attributeTypes
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___attributeTypes_0;

public:
	inline static int32_t get_offset_of_attributeTypes_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass39_0_t9526CECAB44E4C658FD5B8B1CFF0FCEF9C1EB4F1, ___attributeTypes_0)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get_attributeTypes_0() const { return ___attributeTypes_0; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of_attributeTypes_0() { return &___attributeTypes_0; }
	inline void set_attributeTypes_0(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		___attributeTypes_0 = value;
		Il2CppCodeGenWriteBarrier((&___attributeTypes_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS39_0_T9526CECAB44E4C658FD5B8B1CFF0FCEF9C1EB4F1_H
#ifndef U3CU3EC__DISPLAYCLASS39_1_T146B962CF3457AB8ECF100B1E71100370B3172B2_H
#define U3CU3EC__DISPLAYCLASS39_1_T146B962CF3457AB8ECF100B1E71100370B3172B2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModestTree.TypeExtensions_<>c__DisplayClass39_1
struct  U3CU3Ec__DisplayClass39_1_t146B962CF3457AB8ECF100B1E71100370B3172B2  : public RuntimeObject
{
public:
	// System.Attribute ModestTree.TypeExtensions_<>c__DisplayClass39_1::a
	Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74 * ___a_0;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass39_1_t146B962CF3457AB8ECF100B1E71100370B3172B2, ___a_0)); }
	inline Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74 * get_a_0() const { return ___a_0; }
	inline Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74 ** get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74 * value)
	{
		___a_0 = value;
		Il2CppCodeGenWriteBarrier((&___a_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS39_1_T146B962CF3457AB8ECF100B1E71100370B3172B2_H
#ifndef U3CGETALLINSTANCEFIELDSU3ED__25_TD7F6236F6A4B81B14F6226370E24A70D5B63D905_H
#define U3CGETALLINSTANCEFIELDSU3ED__25_TD7F6236F6A4B81B14F6226370E24A70D5B63D905_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModestTree.TypeExtensions_<GetAllInstanceFields>d__25
struct  U3CGetAllInstanceFieldsU3Ed__25_tD7F6236F6A4B81B14F6226370E24A70D5B63D905  : public RuntimeObject
{
public:
	// System.Int32 ModestTree.TypeExtensions_<GetAllInstanceFields>d__25::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Reflection.FieldInfo ModestTree.TypeExtensions_<GetAllInstanceFields>d__25::<>2__current
	FieldInfo_t * ___U3CU3E2__current_1;
	// System.Int32 ModestTree.TypeExtensions_<GetAllInstanceFields>d__25::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// System.Type ModestTree.TypeExtensions_<GetAllInstanceFields>d__25::type
	Type_t * ___type_3;
	// System.Type ModestTree.TypeExtensions_<GetAllInstanceFields>d__25::<>3__type
	Type_t * ___U3CU3E3__type_4;
	// System.Reflection.FieldInfo[] ModestTree.TypeExtensions_<GetAllInstanceFields>d__25::<>7__wrap1
	FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE* ___U3CU3E7__wrap1_5;
	// System.Int32 ModestTree.TypeExtensions_<GetAllInstanceFields>d__25::<>7__wrap2
	int32_t ___U3CU3E7__wrap2_6;
	// System.Collections.Generic.IEnumerator`1<System.Reflection.FieldInfo> ModestTree.TypeExtensions_<GetAllInstanceFields>d__25::<>7__wrap3
	RuntimeObject* ___U3CU3E7__wrap3_7;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetAllInstanceFieldsU3Ed__25_tD7F6236F6A4B81B14F6226370E24A70D5B63D905, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetAllInstanceFieldsU3Ed__25_tD7F6236F6A4B81B14F6226370E24A70D5B63D905, ___U3CU3E2__current_1)); }
	inline FieldInfo_t * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline FieldInfo_t ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(FieldInfo_t * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CGetAllInstanceFieldsU3Ed__25_tD7F6236F6A4B81B14F6226370E24A70D5B63D905, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(U3CGetAllInstanceFieldsU3Ed__25_tD7F6236F6A4B81B14F6226370E24A70D5B63D905, ___type_3)); }
	inline Type_t * get_type_3() const { return ___type_3; }
	inline Type_t ** get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(Type_t * value)
	{
		___type_3 = value;
		Il2CppCodeGenWriteBarrier((&___type_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__type_4() { return static_cast<int32_t>(offsetof(U3CGetAllInstanceFieldsU3Ed__25_tD7F6236F6A4B81B14F6226370E24A70D5B63D905, ___U3CU3E3__type_4)); }
	inline Type_t * get_U3CU3E3__type_4() const { return ___U3CU3E3__type_4; }
	inline Type_t ** get_address_of_U3CU3E3__type_4() { return &___U3CU3E3__type_4; }
	inline void set_U3CU3E3__type_4(Type_t * value)
	{
		___U3CU3E3__type_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__type_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_5() { return static_cast<int32_t>(offsetof(U3CGetAllInstanceFieldsU3Ed__25_tD7F6236F6A4B81B14F6226370E24A70D5B63D905, ___U3CU3E7__wrap1_5)); }
	inline FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE* get_U3CU3E7__wrap1_5() const { return ___U3CU3E7__wrap1_5; }
	inline FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE** get_address_of_U3CU3E7__wrap1_5() { return &___U3CU3E7__wrap1_5; }
	inline void set_U3CU3E7__wrap1_5(FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE* value)
	{
		___U3CU3E7__wrap1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap1_5), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap2_6() { return static_cast<int32_t>(offsetof(U3CGetAllInstanceFieldsU3Ed__25_tD7F6236F6A4B81B14F6226370E24A70D5B63D905, ___U3CU3E7__wrap2_6)); }
	inline int32_t get_U3CU3E7__wrap2_6() const { return ___U3CU3E7__wrap2_6; }
	inline int32_t* get_address_of_U3CU3E7__wrap2_6() { return &___U3CU3E7__wrap2_6; }
	inline void set_U3CU3E7__wrap2_6(int32_t value)
	{
		___U3CU3E7__wrap2_6 = value;
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap3_7() { return static_cast<int32_t>(offsetof(U3CGetAllInstanceFieldsU3Ed__25_tD7F6236F6A4B81B14F6226370E24A70D5B63D905, ___U3CU3E7__wrap3_7)); }
	inline RuntimeObject* get_U3CU3E7__wrap3_7() const { return ___U3CU3E7__wrap3_7; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap3_7() { return &___U3CU3E7__wrap3_7; }
	inline void set_U3CU3E7__wrap3_7(RuntimeObject* value)
	{
		___U3CU3E7__wrap3_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap3_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETALLINSTANCEFIELDSU3ED__25_TD7F6236F6A4B81B14F6226370E24A70D5B63D905_H
#ifndef U3CGETALLINSTANCEMETHODSU3ED__27_TAE22CA9FF15C83847047E3977AF78DAF2771F9AE_H
#define U3CGETALLINSTANCEMETHODSU3ED__27_TAE22CA9FF15C83847047E3977AF78DAF2771F9AE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModestTree.TypeExtensions_<GetAllInstanceMethods>d__27
struct  U3CGetAllInstanceMethodsU3Ed__27_tAE22CA9FF15C83847047E3977AF78DAF2771F9AE  : public RuntimeObject
{
public:
	// System.Int32 ModestTree.TypeExtensions_<GetAllInstanceMethods>d__27::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Reflection.MethodInfo ModestTree.TypeExtensions_<GetAllInstanceMethods>d__27::<>2__current
	MethodInfo_t * ___U3CU3E2__current_1;
	// System.Int32 ModestTree.TypeExtensions_<GetAllInstanceMethods>d__27::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// System.Type ModestTree.TypeExtensions_<GetAllInstanceMethods>d__27::type
	Type_t * ___type_3;
	// System.Type ModestTree.TypeExtensions_<GetAllInstanceMethods>d__27::<>3__type
	Type_t * ___U3CU3E3__type_4;
	// System.Reflection.MethodInfo[] ModestTree.TypeExtensions_<GetAllInstanceMethods>d__27::<>7__wrap1
	MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B* ___U3CU3E7__wrap1_5;
	// System.Int32 ModestTree.TypeExtensions_<GetAllInstanceMethods>d__27::<>7__wrap2
	int32_t ___U3CU3E7__wrap2_6;
	// System.Collections.Generic.IEnumerator`1<System.Reflection.MethodInfo> ModestTree.TypeExtensions_<GetAllInstanceMethods>d__27::<>7__wrap3
	RuntimeObject* ___U3CU3E7__wrap3_7;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetAllInstanceMethodsU3Ed__27_tAE22CA9FF15C83847047E3977AF78DAF2771F9AE, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetAllInstanceMethodsU3Ed__27_tAE22CA9FF15C83847047E3977AF78DAF2771F9AE, ___U3CU3E2__current_1)); }
	inline MethodInfo_t * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline MethodInfo_t ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(MethodInfo_t * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CGetAllInstanceMethodsU3Ed__27_tAE22CA9FF15C83847047E3977AF78DAF2771F9AE, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(U3CGetAllInstanceMethodsU3Ed__27_tAE22CA9FF15C83847047E3977AF78DAF2771F9AE, ___type_3)); }
	inline Type_t * get_type_3() const { return ___type_3; }
	inline Type_t ** get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(Type_t * value)
	{
		___type_3 = value;
		Il2CppCodeGenWriteBarrier((&___type_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__type_4() { return static_cast<int32_t>(offsetof(U3CGetAllInstanceMethodsU3Ed__27_tAE22CA9FF15C83847047E3977AF78DAF2771F9AE, ___U3CU3E3__type_4)); }
	inline Type_t * get_U3CU3E3__type_4() const { return ___U3CU3E3__type_4; }
	inline Type_t ** get_address_of_U3CU3E3__type_4() { return &___U3CU3E3__type_4; }
	inline void set_U3CU3E3__type_4(Type_t * value)
	{
		___U3CU3E3__type_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__type_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_5() { return static_cast<int32_t>(offsetof(U3CGetAllInstanceMethodsU3Ed__27_tAE22CA9FF15C83847047E3977AF78DAF2771F9AE, ___U3CU3E7__wrap1_5)); }
	inline MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B* get_U3CU3E7__wrap1_5() const { return ___U3CU3E7__wrap1_5; }
	inline MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B** get_address_of_U3CU3E7__wrap1_5() { return &___U3CU3E7__wrap1_5; }
	inline void set_U3CU3E7__wrap1_5(MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B* value)
	{
		___U3CU3E7__wrap1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap1_5), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap2_6() { return static_cast<int32_t>(offsetof(U3CGetAllInstanceMethodsU3Ed__27_tAE22CA9FF15C83847047E3977AF78DAF2771F9AE, ___U3CU3E7__wrap2_6)); }
	inline int32_t get_U3CU3E7__wrap2_6() const { return ___U3CU3E7__wrap2_6; }
	inline int32_t* get_address_of_U3CU3E7__wrap2_6() { return &___U3CU3E7__wrap2_6; }
	inline void set_U3CU3E7__wrap2_6(int32_t value)
	{
		___U3CU3E7__wrap2_6 = value;
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap3_7() { return static_cast<int32_t>(offsetof(U3CGetAllInstanceMethodsU3Ed__27_tAE22CA9FF15C83847047E3977AF78DAF2771F9AE, ___U3CU3E7__wrap3_7)); }
	inline RuntimeObject* get_U3CU3E7__wrap3_7() const { return ___U3CU3E7__wrap3_7; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap3_7() { return &___U3CU3E7__wrap3_7; }
	inline void set_U3CU3E7__wrap3_7(RuntimeObject* value)
	{
		___U3CU3E7__wrap3_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap3_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETALLINSTANCEMETHODSU3ED__27_TAE22CA9FF15C83847047E3977AF78DAF2771F9AE_H
#ifndef U3CGETALLINSTANCEPROPERTIESU3ED__26_TABD7A33B589EF2C3764831583FF7B6D0E832BA64_H
#define U3CGETALLINSTANCEPROPERTIESU3ED__26_TABD7A33B589EF2C3764831583FF7B6D0E832BA64_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModestTree.TypeExtensions_<GetAllInstanceProperties>d__26
struct  U3CGetAllInstancePropertiesU3Ed__26_tABD7A33B589EF2C3764831583FF7B6D0E832BA64  : public RuntimeObject
{
public:
	// System.Int32 ModestTree.TypeExtensions_<GetAllInstanceProperties>d__26::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Reflection.PropertyInfo ModestTree.TypeExtensions_<GetAllInstanceProperties>d__26::<>2__current
	PropertyInfo_t * ___U3CU3E2__current_1;
	// System.Int32 ModestTree.TypeExtensions_<GetAllInstanceProperties>d__26::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// System.Type ModestTree.TypeExtensions_<GetAllInstanceProperties>d__26::type
	Type_t * ___type_3;
	// System.Type ModestTree.TypeExtensions_<GetAllInstanceProperties>d__26::<>3__type
	Type_t * ___U3CU3E3__type_4;
	// System.Reflection.PropertyInfo[] ModestTree.TypeExtensions_<GetAllInstanceProperties>d__26::<>7__wrap1
	PropertyInfoU5BU5D_tAD8E99B12FF99CA4F2EA37B612DE68E112B4CF7E* ___U3CU3E7__wrap1_5;
	// System.Int32 ModestTree.TypeExtensions_<GetAllInstanceProperties>d__26::<>7__wrap2
	int32_t ___U3CU3E7__wrap2_6;
	// System.Collections.Generic.IEnumerator`1<System.Reflection.PropertyInfo> ModestTree.TypeExtensions_<GetAllInstanceProperties>d__26::<>7__wrap3
	RuntimeObject* ___U3CU3E7__wrap3_7;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetAllInstancePropertiesU3Ed__26_tABD7A33B589EF2C3764831583FF7B6D0E832BA64, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetAllInstancePropertiesU3Ed__26_tABD7A33B589EF2C3764831583FF7B6D0E832BA64, ___U3CU3E2__current_1)); }
	inline PropertyInfo_t * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline PropertyInfo_t ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(PropertyInfo_t * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CGetAllInstancePropertiesU3Ed__26_tABD7A33B589EF2C3764831583FF7B6D0E832BA64, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(U3CGetAllInstancePropertiesU3Ed__26_tABD7A33B589EF2C3764831583FF7B6D0E832BA64, ___type_3)); }
	inline Type_t * get_type_3() const { return ___type_3; }
	inline Type_t ** get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(Type_t * value)
	{
		___type_3 = value;
		Il2CppCodeGenWriteBarrier((&___type_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__type_4() { return static_cast<int32_t>(offsetof(U3CGetAllInstancePropertiesU3Ed__26_tABD7A33B589EF2C3764831583FF7B6D0E832BA64, ___U3CU3E3__type_4)); }
	inline Type_t * get_U3CU3E3__type_4() const { return ___U3CU3E3__type_4; }
	inline Type_t ** get_address_of_U3CU3E3__type_4() { return &___U3CU3E3__type_4; }
	inline void set_U3CU3E3__type_4(Type_t * value)
	{
		___U3CU3E3__type_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__type_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_5() { return static_cast<int32_t>(offsetof(U3CGetAllInstancePropertiesU3Ed__26_tABD7A33B589EF2C3764831583FF7B6D0E832BA64, ___U3CU3E7__wrap1_5)); }
	inline PropertyInfoU5BU5D_tAD8E99B12FF99CA4F2EA37B612DE68E112B4CF7E* get_U3CU3E7__wrap1_5() const { return ___U3CU3E7__wrap1_5; }
	inline PropertyInfoU5BU5D_tAD8E99B12FF99CA4F2EA37B612DE68E112B4CF7E** get_address_of_U3CU3E7__wrap1_5() { return &___U3CU3E7__wrap1_5; }
	inline void set_U3CU3E7__wrap1_5(PropertyInfoU5BU5D_tAD8E99B12FF99CA4F2EA37B612DE68E112B4CF7E* value)
	{
		___U3CU3E7__wrap1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap1_5), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap2_6() { return static_cast<int32_t>(offsetof(U3CGetAllInstancePropertiesU3Ed__26_tABD7A33B589EF2C3764831583FF7B6D0E832BA64, ___U3CU3E7__wrap2_6)); }
	inline int32_t get_U3CU3E7__wrap2_6() const { return ___U3CU3E7__wrap2_6; }
	inline int32_t* get_address_of_U3CU3E7__wrap2_6() { return &___U3CU3E7__wrap2_6; }
	inline void set_U3CU3E7__wrap2_6(int32_t value)
	{
		___U3CU3E7__wrap2_6 = value;
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap3_7() { return static_cast<int32_t>(offsetof(U3CGetAllInstancePropertiesU3Ed__26_tABD7A33B589EF2C3764831583FF7B6D0E832BA64, ___U3CU3E7__wrap3_7)); }
	inline RuntimeObject* get_U3CU3E7__wrap3_7() const { return ___U3CU3E7__wrap3_7; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap3_7() { return &___U3CU3E7__wrap3_7; }
	inline void set_U3CU3E7__wrap3_7(RuntimeObject* value)
	{
		___U3CU3E7__wrap3_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap3_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETALLINSTANCEPROPERTIESU3ED__26_TABD7A33B589EF2C3764831583FF7B6D0E832BA64_H
#ifndef U3CGETPARENTTYPESU3ED__22_T09FE9B3783AE41E4985C473FF68DCC15529BAA58_H
#define U3CGETPARENTTYPESU3ED__22_T09FE9B3783AE41E4985C473FF68DCC15529BAA58_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModestTree.TypeExtensions_<GetParentTypes>d__22
struct  U3CGetParentTypesU3Ed__22_t09FE9B3783AE41E4985C473FF68DCC15529BAA58  : public RuntimeObject
{
public:
	// System.Int32 ModestTree.TypeExtensions_<GetParentTypes>d__22::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Type ModestTree.TypeExtensions_<GetParentTypes>d__22::<>2__current
	Type_t * ___U3CU3E2__current_1;
	// System.Int32 ModestTree.TypeExtensions_<GetParentTypes>d__22::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// System.Type ModestTree.TypeExtensions_<GetParentTypes>d__22::type
	Type_t * ___type_3;
	// System.Type ModestTree.TypeExtensions_<GetParentTypes>d__22::<>3__type
	Type_t * ___U3CU3E3__type_4;
	// System.Collections.Generic.IEnumerator`1<System.Type> ModestTree.TypeExtensions_<GetParentTypes>d__22::<>7__wrap1
	RuntimeObject* ___U3CU3E7__wrap1_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetParentTypesU3Ed__22_t09FE9B3783AE41E4985C473FF68DCC15529BAA58, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetParentTypesU3Ed__22_t09FE9B3783AE41E4985C473FF68DCC15529BAA58, ___U3CU3E2__current_1)); }
	inline Type_t * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline Type_t ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(Type_t * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CGetParentTypesU3Ed__22_t09FE9B3783AE41E4985C473FF68DCC15529BAA58, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(U3CGetParentTypesU3Ed__22_t09FE9B3783AE41E4985C473FF68DCC15529BAA58, ___type_3)); }
	inline Type_t * get_type_3() const { return ___type_3; }
	inline Type_t ** get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(Type_t * value)
	{
		___type_3 = value;
		Il2CppCodeGenWriteBarrier((&___type_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__type_4() { return static_cast<int32_t>(offsetof(U3CGetParentTypesU3Ed__22_t09FE9B3783AE41E4985C473FF68DCC15529BAA58, ___U3CU3E3__type_4)); }
	inline Type_t * get_U3CU3E3__type_4() const { return ___U3CU3E3__type_4; }
	inline Type_t ** get_address_of_U3CU3E3__type_4() { return &___U3CU3E3__type_4; }
	inline void set_U3CU3E3__type_4(Type_t * value)
	{
		___U3CU3E3__type_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__type_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_5() { return static_cast<int32_t>(offsetof(U3CGetParentTypesU3Ed__22_t09FE9B3783AE41E4985C473FF68DCC15529BAA58, ___U3CU3E7__wrap1_5)); }
	inline RuntimeObject* get_U3CU3E7__wrap1_5() const { return ___U3CU3E7__wrap1_5; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap1_5() { return &___U3CU3E7__wrap1_5; }
	inline void set_U3CU3E7__wrap1_5(RuntimeObject* value)
	{
		___U3CU3E7__wrap1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap1_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETPARENTTYPESU3ED__22_T09FE9B3783AE41E4985C473FF68DCC15529BAA58_H
#ifndef CYANCOMPONENT_TC7880F9103B559C4E6EB97745CE3A56781CDB3BC_H
#define CYANCOMPONENT_TC7880F9103B559C4E6EB97745CE3A56781CDB3BC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoenenGames.CyanLevelEditor.CyanComponent
struct  CyanComponent_tC7880F9103B559C4E6EB97745CE3A56781CDB3BC  : public RuntimeObject
{
public:
	// UnityEngine.Camera MoenenGames.CyanLevelEditor.CyanComponent::MainCamera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___MainCamera_0;
	// UnityEngine.Collider MoenenGames.CyanLevelEditor.CyanComponent::DefaultGround
	Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * ___DefaultGround_1;
	// UnityEngine.Transform MoenenGames.CyanLevelEditor.CyanComponent::Cursor
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___Cursor_2;
	// UnityEngine.Transform MoenenGames.CyanLevelEditor.CyanComponent::HoveringHighLight
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___HoveringHighLight_3;
	// UnityEngine.Transform MoenenGames.CyanLevelEditor.CyanComponent::SelectingHighLight
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___SelectingHighLight_4;
	// UnityEngine.Transform MoenenGames.CyanLevelEditor.CyanComponent::ObjectContainer
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___ObjectContainer_5;
	// UnityEngine.RectTransform MoenenGames.CyanLevelEditor.CyanComponent::PoolUIContainer
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___PoolUIContainer_6;
	// UnityEngine.RectTransform MoenenGames.CyanLevelEditor.CyanComponent::PrefabUIContainer
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___PrefabUIContainer_7;
	// UnityEngine.UI.Toggle MoenenGames.CyanLevelEditor.CyanComponent::PoolItemTemplate
	Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * ___PoolItemTemplate_8;
	// UnityEngine.UI.Toggle MoenenGames.CyanLevelEditor.CyanComponent::PrefabItemTemplate
	Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * ___PrefabItemTemplate_9;

public:
	inline static int32_t get_offset_of_MainCamera_0() { return static_cast<int32_t>(offsetof(CyanComponent_tC7880F9103B559C4E6EB97745CE3A56781CDB3BC, ___MainCamera_0)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_MainCamera_0() const { return ___MainCamera_0; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_MainCamera_0() { return &___MainCamera_0; }
	inline void set_MainCamera_0(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___MainCamera_0 = value;
		Il2CppCodeGenWriteBarrier((&___MainCamera_0), value);
	}

	inline static int32_t get_offset_of_DefaultGround_1() { return static_cast<int32_t>(offsetof(CyanComponent_tC7880F9103B559C4E6EB97745CE3A56781CDB3BC, ___DefaultGround_1)); }
	inline Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * get_DefaultGround_1() const { return ___DefaultGround_1; }
	inline Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF ** get_address_of_DefaultGround_1() { return &___DefaultGround_1; }
	inline void set_DefaultGround_1(Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * value)
	{
		___DefaultGround_1 = value;
		Il2CppCodeGenWriteBarrier((&___DefaultGround_1), value);
	}

	inline static int32_t get_offset_of_Cursor_2() { return static_cast<int32_t>(offsetof(CyanComponent_tC7880F9103B559C4E6EB97745CE3A56781CDB3BC, ___Cursor_2)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_Cursor_2() const { return ___Cursor_2; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_Cursor_2() { return &___Cursor_2; }
	inline void set_Cursor_2(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___Cursor_2 = value;
		Il2CppCodeGenWriteBarrier((&___Cursor_2), value);
	}

	inline static int32_t get_offset_of_HoveringHighLight_3() { return static_cast<int32_t>(offsetof(CyanComponent_tC7880F9103B559C4E6EB97745CE3A56781CDB3BC, ___HoveringHighLight_3)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_HoveringHighLight_3() const { return ___HoveringHighLight_3; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_HoveringHighLight_3() { return &___HoveringHighLight_3; }
	inline void set_HoveringHighLight_3(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___HoveringHighLight_3 = value;
		Il2CppCodeGenWriteBarrier((&___HoveringHighLight_3), value);
	}

	inline static int32_t get_offset_of_SelectingHighLight_4() { return static_cast<int32_t>(offsetof(CyanComponent_tC7880F9103B559C4E6EB97745CE3A56781CDB3BC, ___SelectingHighLight_4)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_SelectingHighLight_4() const { return ___SelectingHighLight_4; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_SelectingHighLight_4() { return &___SelectingHighLight_4; }
	inline void set_SelectingHighLight_4(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___SelectingHighLight_4 = value;
		Il2CppCodeGenWriteBarrier((&___SelectingHighLight_4), value);
	}

	inline static int32_t get_offset_of_ObjectContainer_5() { return static_cast<int32_t>(offsetof(CyanComponent_tC7880F9103B559C4E6EB97745CE3A56781CDB3BC, ___ObjectContainer_5)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_ObjectContainer_5() const { return ___ObjectContainer_5; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_ObjectContainer_5() { return &___ObjectContainer_5; }
	inline void set_ObjectContainer_5(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___ObjectContainer_5 = value;
		Il2CppCodeGenWriteBarrier((&___ObjectContainer_5), value);
	}

	inline static int32_t get_offset_of_PoolUIContainer_6() { return static_cast<int32_t>(offsetof(CyanComponent_tC7880F9103B559C4E6EB97745CE3A56781CDB3BC, ___PoolUIContainer_6)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_PoolUIContainer_6() const { return ___PoolUIContainer_6; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_PoolUIContainer_6() { return &___PoolUIContainer_6; }
	inline void set_PoolUIContainer_6(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___PoolUIContainer_6 = value;
		Il2CppCodeGenWriteBarrier((&___PoolUIContainer_6), value);
	}

	inline static int32_t get_offset_of_PrefabUIContainer_7() { return static_cast<int32_t>(offsetof(CyanComponent_tC7880F9103B559C4E6EB97745CE3A56781CDB3BC, ___PrefabUIContainer_7)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_PrefabUIContainer_7() const { return ___PrefabUIContainer_7; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_PrefabUIContainer_7() { return &___PrefabUIContainer_7; }
	inline void set_PrefabUIContainer_7(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___PrefabUIContainer_7 = value;
		Il2CppCodeGenWriteBarrier((&___PrefabUIContainer_7), value);
	}

	inline static int32_t get_offset_of_PoolItemTemplate_8() { return static_cast<int32_t>(offsetof(CyanComponent_tC7880F9103B559C4E6EB97745CE3A56781CDB3BC, ___PoolItemTemplate_8)); }
	inline Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * get_PoolItemTemplate_8() const { return ___PoolItemTemplate_8; }
	inline Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 ** get_address_of_PoolItemTemplate_8() { return &___PoolItemTemplate_8; }
	inline void set_PoolItemTemplate_8(Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * value)
	{
		___PoolItemTemplate_8 = value;
		Il2CppCodeGenWriteBarrier((&___PoolItemTemplate_8), value);
	}

	inline static int32_t get_offset_of_PrefabItemTemplate_9() { return static_cast<int32_t>(offsetof(CyanComponent_tC7880F9103B559C4E6EB97745CE3A56781CDB3BC, ___PrefabItemTemplate_9)); }
	inline Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * get_PrefabItemTemplate_9() const { return ___PrefabItemTemplate_9; }
	inline Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 ** get_address_of_PrefabItemTemplate_9() { return &___PrefabItemTemplate_9; }
	inline void set_PrefabItemTemplate_9(Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * value)
	{
		___PrefabItemTemplate_9 = value;
		Il2CppCodeGenWriteBarrier((&___PrefabItemTemplate_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CYANCOMPONENT_TC7880F9103B559C4E6EB97745CE3A56781CDB3BC_H
#ifndef CYANGRIDSETTING_TF2D64053897D26131BDCF7A3F2675BD2161BE8CF_H
#define CYANGRIDSETTING_TF2D64053897D26131BDCF7A3F2675BD2161BE8CF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoenenGames.CyanLevelEditor.CyanGridSetting
struct  CyanGridSetting_tF2D64053897D26131BDCF7A3F2675BD2161BE8CF  : public RuntimeObject
{
public:
	// System.Boolean MoenenGames.CyanLevelEditor.CyanGridSetting::PrefabSnapToGrid
	bool ___PrefabSnapToGrid_0;
	// System.Boolean MoenenGames.CyanLevelEditor.CyanGridSetting::CursorSnapToGrid
	bool ___CursorSnapToGrid_1;
	// System.Single MoenenGames.CyanLevelEditor.CyanGridSetting::GridSize
	float ___GridSize_2;

public:
	inline static int32_t get_offset_of_PrefabSnapToGrid_0() { return static_cast<int32_t>(offsetof(CyanGridSetting_tF2D64053897D26131BDCF7A3F2675BD2161BE8CF, ___PrefabSnapToGrid_0)); }
	inline bool get_PrefabSnapToGrid_0() const { return ___PrefabSnapToGrid_0; }
	inline bool* get_address_of_PrefabSnapToGrid_0() { return &___PrefabSnapToGrid_0; }
	inline void set_PrefabSnapToGrid_0(bool value)
	{
		___PrefabSnapToGrid_0 = value;
	}

	inline static int32_t get_offset_of_CursorSnapToGrid_1() { return static_cast<int32_t>(offsetof(CyanGridSetting_tF2D64053897D26131BDCF7A3F2675BD2161BE8CF, ___CursorSnapToGrid_1)); }
	inline bool get_CursorSnapToGrid_1() const { return ___CursorSnapToGrid_1; }
	inline bool* get_address_of_CursorSnapToGrid_1() { return &___CursorSnapToGrid_1; }
	inline void set_CursorSnapToGrid_1(bool value)
	{
		___CursorSnapToGrid_1 = value;
	}

	inline static int32_t get_offset_of_GridSize_2() { return static_cast<int32_t>(offsetof(CyanGridSetting_tF2D64053897D26131BDCF7A3F2675BD2161BE8CF, ___GridSize_2)); }
	inline float get_GridSize_2() const { return ___GridSize_2; }
	inline float* get_address_of_GridSize_2() { return &___GridSize_2; }
	inline void set_GridSize_2(float value)
	{
		___GridSize_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CYANGRIDSETTING_TF2D64053897D26131BDCF7A3F2675BD2161BE8CF_H
#ifndef U3CU3EC__DISPLAYCLASS85_0_T0F545B2D92119D6E0F01D781FDE36A27347E7144_H
#define U3CU3EC__DISPLAYCLASS85_0_T0F545B2D92119D6E0F01D781FDE36A27347E7144_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoenenGames.CyanLevelEditor.CyanLevelEditor_<>c__DisplayClass85_0
struct  U3CU3Ec__DisplayClass85_0_t0F545B2D92119D6E0F01D781FDE36A27347E7144  : public RuntimeObject
{
public:
	// MoenenGames.CyanLevelEditor.CyanLevelEditor MoenenGames.CyanLevelEditor.CyanLevelEditor_<>c__DisplayClass85_0::<>4__this
	CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6 * ___U3CU3E4__this_0;
	// System.Int32 MoenenGames.CyanLevelEditor.CyanLevelEditor_<>c__DisplayClass85_0::poolIndex
	int32_t ___poolIndex_1;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass85_0_t0F545B2D92119D6E0F01D781FDE36A27347E7144, ___U3CU3E4__this_0)); }
	inline CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_poolIndex_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass85_0_t0F545B2D92119D6E0F01D781FDE36A27347E7144, ___poolIndex_1)); }
	inline int32_t get_poolIndex_1() const { return ___poolIndex_1; }
	inline int32_t* get_address_of_poolIndex_1() { return &___poolIndex_1; }
	inline void set_poolIndex_1(int32_t value)
	{
		___poolIndex_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS85_0_T0F545B2D92119D6E0F01D781FDE36A27347E7144_H
#ifndef U3CU3EC__DISPLAYCLASS86_0_T68FAED02A98BE4BDEB410ED9C5626971AB316922_H
#define U3CU3EC__DISPLAYCLASS86_0_T68FAED02A98BE4BDEB410ED9C5626971AB316922_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoenenGames.CyanLevelEditor.CyanLevelEditor_<>c__DisplayClass86_0
struct  U3CU3Ec__DisplayClass86_0_t68FAED02A98BE4BDEB410ED9C5626971AB316922  : public RuntimeObject
{
public:
	// MoenenGames.CyanLevelEditor.CyanLevelEditor MoenenGames.CyanLevelEditor.CyanLevelEditor_<>c__DisplayClass86_0::<>4__this
	CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6 * ___U3CU3E4__this_0;
	// System.Int32 MoenenGames.CyanLevelEditor.CyanLevelEditor_<>c__DisplayClass86_0::poolIndex
	int32_t ___poolIndex_1;
	// System.Int32 MoenenGames.CyanLevelEditor.CyanLevelEditor_<>c__DisplayClass86_0::prefabIndex
	int32_t ___prefabIndex_2;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass86_0_t68FAED02A98BE4BDEB410ED9C5626971AB316922, ___U3CU3E4__this_0)); }
	inline CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_poolIndex_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass86_0_t68FAED02A98BE4BDEB410ED9C5626971AB316922, ___poolIndex_1)); }
	inline int32_t get_poolIndex_1() const { return ___poolIndex_1; }
	inline int32_t* get_address_of_poolIndex_1() { return &___poolIndex_1; }
	inline void set_poolIndex_1(int32_t value)
	{
		___poolIndex_1 = value;
	}

	inline static int32_t get_offset_of_prefabIndex_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass86_0_t68FAED02A98BE4BDEB410ED9C5626971AB316922, ___prefabIndex_2)); }
	inline int32_t get_prefabIndex_2() const { return ___prefabIndex_2; }
	inline int32_t* get_address_of_prefabIndex_2() { return &___prefabIndex_2; }
	inline void set_prefabIndex_2(int32_t value)
	{
		___prefabIndex_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS86_0_T68FAED02A98BE4BDEB410ED9C5626971AB316922_H
#ifndef CYANMESSAGE_T5A284AED26BF63EDC0379206EBE0289EDF8B4D26_H
#define CYANMESSAGE_T5A284AED26BF63EDC0379206EBE0289EDF8B4D26_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoenenGames.CyanLevelEditor.CyanMessage
struct  CyanMessage_t5A284AED26BF63EDC0379206EBE0289EDF8B4D26  : public RuntimeObject
{
public:
	// MoenenGames.CyanLevelEditor.CyanMessage_CyanTransformEvent MoenenGames.CyanLevelEditor.CyanMessage::OnObjectPaint
	CyanTransformEvent_t9A71D6662200E71DA050209321EBE077E3FD117C * ___OnObjectPaint_0;
	// MoenenGames.CyanLevelEditor.CyanMessage_CyanTransformEvent MoenenGames.CyanLevelEditor.CyanMessage::BeforeObjectDelete
	CyanTransformEvent_t9A71D6662200E71DA050209321EBE077E3FD117C * ___BeforeObjectDelete_1;
	// MoenenGames.CyanLevelEditor.CyanMessage_CyanTransformEvent MoenenGames.CyanLevelEditor.CyanMessage::OnHoverChange
	CyanTransformEvent_t9A71D6662200E71DA050209321EBE077E3FD117C * ___OnHoverChange_2;
	// MoenenGames.CyanLevelEditor.CyanMessage_CyanTransformEvent MoenenGames.CyanLevelEditor.CyanMessage::OnSelectionChange
	CyanTransformEvent_t9A71D6662200E71DA050209321EBE077E3FD117C * ___OnSelectionChange_3;
	// MoenenGames.CyanLevelEditor.CyanMessage_CyanIntRvent MoenenGames.CyanLevelEditor.CyanMessage::OnPoolSwitch
	CyanIntRvent_t36B9E29A72B590D0D9FEDE9A938B67733AB53FC0 * ___OnPoolSwitch_4;
	// MoenenGames.CyanLevelEditor.CyanMessage_CyanIntIntRvent MoenenGames.CyanLevelEditor.CyanMessage::OnPrefabSwitch
	CyanIntIntRvent_t3050562EFC3FABA385FF4019E611B224C7F39B2F * ___OnPrefabSwitch_5;

public:
	inline static int32_t get_offset_of_OnObjectPaint_0() { return static_cast<int32_t>(offsetof(CyanMessage_t5A284AED26BF63EDC0379206EBE0289EDF8B4D26, ___OnObjectPaint_0)); }
	inline CyanTransformEvent_t9A71D6662200E71DA050209321EBE077E3FD117C * get_OnObjectPaint_0() const { return ___OnObjectPaint_0; }
	inline CyanTransformEvent_t9A71D6662200E71DA050209321EBE077E3FD117C ** get_address_of_OnObjectPaint_0() { return &___OnObjectPaint_0; }
	inline void set_OnObjectPaint_0(CyanTransformEvent_t9A71D6662200E71DA050209321EBE077E3FD117C * value)
	{
		___OnObjectPaint_0 = value;
		Il2CppCodeGenWriteBarrier((&___OnObjectPaint_0), value);
	}

	inline static int32_t get_offset_of_BeforeObjectDelete_1() { return static_cast<int32_t>(offsetof(CyanMessage_t5A284AED26BF63EDC0379206EBE0289EDF8B4D26, ___BeforeObjectDelete_1)); }
	inline CyanTransformEvent_t9A71D6662200E71DA050209321EBE077E3FD117C * get_BeforeObjectDelete_1() const { return ___BeforeObjectDelete_1; }
	inline CyanTransformEvent_t9A71D6662200E71DA050209321EBE077E3FD117C ** get_address_of_BeforeObjectDelete_1() { return &___BeforeObjectDelete_1; }
	inline void set_BeforeObjectDelete_1(CyanTransformEvent_t9A71D6662200E71DA050209321EBE077E3FD117C * value)
	{
		___BeforeObjectDelete_1 = value;
		Il2CppCodeGenWriteBarrier((&___BeforeObjectDelete_1), value);
	}

	inline static int32_t get_offset_of_OnHoverChange_2() { return static_cast<int32_t>(offsetof(CyanMessage_t5A284AED26BF63EDC0379206EBE0289EDF8B4D26, ___OnHoverChange_2)); }
	inline CyanTransformEvent_t9A71D6662200E71DA050209321EBE077E3FD117C * get_OnHoverChange_2() const { return ___OnHoverChange_2; }
	inline CyanTransformEvent_t9A71D6662200E71DA050209321EBE077E3FD117C ** get_address_of_OnHoverChange_2() { return &___OnHoverChange_2; }
	inline void set_OnHoverChange_2(CyanTransformEvent_t9A71D6662200E71DA050209321EBE077E3FD117C * value)
	{
		___OnHoverChange_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnHoverChange_2), value);
	}

	inline static int32_t get_offset_of_OnSelectionChange_3() { return static_cast<int32_t>(offsetof(CyanMessage_t5A284AED26BF63EDC0379206EBE0289EDF8B4D26, ___OnSelectionChange_3)); }
	inline CyanTransformEvent_t9A71D6662200E71DA050209321EBE077E3FD117C * get_OnSelectionChange_3() const { return ___OnSelectionChange_3; }
	inline CyanTransformEvent_t9A71D6662200E71DA050209321EBE077E3FD117C ** get_address_of_OnSelectionChange_3() { return &___OnSelectionChange_3; }
	inline void set_OnSelectionChange_3(CyanTransformEvent_t9A71D6662200E71DA050209321EBE077E3FD117C * value)
	{
		___OnSelectionChange_3 = value;
		Il2CppCodeGenWriteBarrier((&___OnSelectionChange_3), value);
	}

	inline static int32_t get_offset_of_OnPoolSwitch_4() { return static_cast<int32_t>(offsetof(CyanMessage_t5A284AED26BF63EDC0379206EBE0289EDF8B4D26, ___OnPoolSwitch_4)); }
	inline CyanIntRvent_t36B9E29A72B590D0D9FEDE9A938B67733AB53FC0 * get_OnPoolSwitch_4() const { return ___OnPoolSwitch_4; }
	inline CyanIntRvent_t36B9E29A72B590D0D9FEDE9A938B67733AB53FC0 ** get_address_of_OnPoolSwitch_4() { return &___OnPoolSwitch_4; }
	inline void set_OnPoolSwitch_4(CyanIntRvent_t36B9E29A72B590D0D9FEDE9A938B67733AB53FC0 * value)
	{
		___OnPoolSwitch_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnPoolSwitch_4), value);
	}

	inline static int32_t get_offset_of_OnPrefabSwitch_5() { return static_cast<int32_t>(offsetof(CyanMessage_t5A284AED26BF63EDC0379206EBE0289EDF8B4D26, ___OnPrefabSwitch_5)); }
	inline CyanIntIntRvent_t3050562EFC3FABA385FF4019E611B224C7F39B2F * get_OnPrefabSwitch_5() const { return ___OnPrefabSwitch_5; }
	inline CyanIntIntRvent_t3050562EFC3FABA385FF4019E611B224C7F39B2F ** get_address_of_OnPrefabSwitch_5() { return &___OnPrefabSwitch_5; }
	inline void set_OnPrefabSwitch_5(CyanIntIntRvent_t3050562EFC3FABA385FF4019E611B224C7F39B2F * value)
	{
		___OnPrefabSwitch_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnPrefabSwitch_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CYANMESSAGE_T5A284AED26BF63EDC0379206EBE0289EDF8B4D26_H
#ifndef CYANOBJECTPOOL_TEBC761771A66414B6FC6ACD1722E361442B9E20B_H
#define CYANOBJECTPOOL_TEBC761771A66414B6FC6ACD1722E361442B9E20B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoenenGames.CyanLevelEditor.CyanObjectPool
struct  CyanObjectPool_tEBC761771A66414B6FC6ACD1722E361442B9E20B  : public RuntimeObject
{
public:
	// System.String MoenenGames.CyanLevelEditor.CyanObjectPool::Name
	String_t* ___Name_0;
	// MoenenGames.CyanLevelEditor.CyanObjectPool_Prefab[] MoenenGames.CyanLevelEditor.CyanObjectPool::Prefabs
	PrefabU5BU5D_t6275A85F607D793C12F6AE6F46A8EB81F4F4A0BA* ___Prefabs_1;
	// UnityEngine.Sprite MoenenGames.CyanLevelEditor.CyanObjectPool::PoolIcon
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___PoolIcon_2;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(CyanObjectPool_tEBC761771A66414B6FC6ACD1722E361442B9E20B, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_Prefabs_1() { return static_cast<int32_t>(offsetof(CyanObjectPool_tEBC761771A66414B6FC6ACD1722E361442B9E20B, ___Prefabs_1)); }
	inline PrefabU5BU5D_t6275A85F607D793C12F6AE6F46A8EB81F4F4A0BA* get_Prefabs_1() const { return ___Prefabs_1; }
	inline PrefabU5BU5D_t6275A85F607D793C12F6AE6F46A8EB81F4F4A0BA** get_address_of_Prefabs_1() { return &___Prefabs_1; }
	inline void set_Prefabs_1(PrefabU5BU5D_t6275A85F607D793C12F6AE6F46A8EB81F4F4A0BA* value)
	{
		___Prefabs_1 = value;
		Il2CppCodeGenWriteBarrier((&___Prefabs_1), value);
	}

	inline static int32_t get_offset_of_PoolIcon_2() { return static_cast<int32_t>(offsetof(CyanObjectPool_tEBC761771A66414B6FC6ACD1722E361442B9E20B, ___PoolIcon_2)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_PoolIcon_2() const { return ___PoolIcon_2; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_PoolIcon_2() { return &___PoolIcon_2; }
	inline void set_PoolIcon_2(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___PoolIcon_2 = value;
		Il2CppCodeGenWriteBarrier((&___PoolIcon_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CYANOBJECTPOOL_TEBC761771A66414B6FC6ACD1722E361442B9E20B_H
#ifndef PREFAB_T9A8682B18F1324D8885AB33089A02BF5C742AA1B_H
#define PREFAB_T9A8682B18F1324D8885AB33089A02BF5C742AA1B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoenenGames.CyanLevelEditor.CyanObjectPool_Prefab
struct  Prefab_t9A8682B18F1324D8885AB33089A02BF5C742AA1B  : public RuntimeObject
{
public:
	// UnityEngine.Transform MoenenGames.CyanLevelEditor.CyanObjectPool_Prefab::transform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___transform_0;
	// UnityEngine.Sprite MoenenGames.CyanLevelEditor.CyanObjectPool_Prefab::Icon
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___Icon_1;
	// System.String MoenenGames.CyanLevelEditor.CyanObjectPool_Prefab::Name
	String_t* ___Name_2;

public:
	inline static int32_t get_offset_of_transform_0() { return static_cast<int32_t>(offsetof(Prefab_t9A8682B18F1324D8885AB33089A02BF5C742AA1B, ___transform_0)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_transform_0() const { return ___transform_0; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_transform_0() { return &___transform_0; }
	inline void set_transform_0(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___transform_0 = value;
		Il2CppCodeGenWriteBarrier((&___transform_0), value);
	}

	inline static int32_t get_offset_of_Icon_1() { return static_cast<int32_t>(offsetof(Prefab_t9A8682B18F1324D8885AB33089A02BF5C742AA1B, ___Icon_1)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_Icon_1() const { return ___Icon_1; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_Icon_1() { return &___Icon_1; }
	inline void set_Icon_1(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___Icon_1 = value;
		Il2CppCodeGenWriteBarrier((&___Icon_1), value);
	}

	inline static int32_t get_offset_of_Name_2() { return static_cast<int32_t>(offsetof(Prefab_t9A8682B18F1324D8885AB33089A02BF5C742AA1B, ___Name_2)); }
	inline String_t* get_Name_2() const { return ___Name_2; }
	inline String_t** get_address_of_Name_2() { return &___Name_2; }
	inline void set_Name_2(String_t* value)
	{
		___Name_2 = value;
		Il2CppCodeGenWriteBarrier((&___Name_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREFAB_T9A8682B18F1324D8885AB33089A02BF5C742AA1B_H
#ifndef MAPDATA_TA7A939C073DE4163D917C3AD9BA8111C169E71DF_H
#define MAPDATA_TA7A939C073DE4163D917C3AD9BA8111C169E71DF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoenenGames.CyanLevelEditor.MapData
struct  MapData_tA7A939C073DE4163D917C3AD9BA8111C169E71DF  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<MoenenGames.CyanLevelEditor.MapData_MapItem> MoenenGames.CyanLevelEditor.MapData::Items
	List_1_t4A3A177747B5B9DEA2C8FE1D93FAB0830EBCA7D5 * ___Items_0;

public:
	inline static int32_t get_offset_of_Items_0() { return static_cast<int32_t>(offsetof(MapData_tA7A939C073DE4163D917C3AD9BA8111C169E71DF, ___Items_0)); }
	inline List_1_t4A3A177747B5B9DEA2C8FE1D93FAB0830EBCA7D5 * get_Items_0() const { return ___Items_0; }
	inline List_1_t4A3A177747B5B9DEA2C8FE1D93FAB0830EBCA7D5 ** get_address_of_Items_0() { return &___Items_0; }
	inline void set_Items_0(List_1_t4A3A177747B5B9DEA2C8FE1D93FAB0830EBCA7D5 * value)
	{
		___Items_0 = value;
		Il2CppCodeGenWriteBarrier((&___Items_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAPDATA_TA7A939C073DE4163D917C3AD9BA8111C169E71DF_H
#ifndef MAPITEMS_T58F3A8F754693DC80EDDEB936D6023AC8FECCF9D_H
#define MAPITEMS_T58F3A8F754693DC80EDDEB936D6023AC8FECCF9D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoenenGames.CyanLevelEditor.MapItems
struct  MapItems_t58F3A8F754693DC80EDDEB936D6023AC8FECCF9D  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<MoenenGames.CyanLevelEditor.ItemRawData> MoenenGames.CyanLevelEditor.MapItems::Items
	List_1_t295A774ECA33315CFD19005983613AC9E19B4992 * ___Items_0;

public:
	inline static int32_t get_offset_of_Items_0() { return static_cast<int32_t>(offsetof(MapItems_t58F3A8F754693DC80EDDEB936D6023AC8FECCF9D, ___Items_0)); }
	inline List_1_t295A774ECA33315CFD19005983613AC9E19B4992 * get_Items_0() const { return ___Items_0; }
	inline List_1_t295A774ECA33315CFD19005983613AC9E19B4992 ** get_address_of_Items_0() { return &___Items_0; }
	inline void set_Items_0(List_1_t295A774ECA33315CFD19005983613AC9E19B4992 * value)
	{
		___Items_0 = value;
		Il2CppCodeGenWriteBarrier((&___Items_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAPITEMS_T58F3A8F754693DC80EDDEB936D6023AC8FECCF9D_H
#ifndef MAPRAWDATA_TF8450C3240F82CA0E23B1D062D7FC6554079475B_H
#define MAPRAWDATA_TF8450C3240F82CA0E23B1D062D7FC6554079475B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoenenGames.CyanLevelEditor.MapRawData
struct  MapRawData_tF8450C3240F82CA0E23B1D062D7FC6554079475B  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.Int32> MoenenGames.CyanLevelEditor.MapRawData::Items
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ___Items_0;

public:
	inline static int32_t get_offset_of_Items_0() { return static_cast<int32_t>(offsetof(MapRawData_tF8450C3240F82CA0E23B1D062D7FC6554079475B, ___Items_0)); }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * get_Items_0() const { return ___Items_0; }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 ** get_address_of_Items_0() { return &___Items_0; }
	inline void set_Items_0(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * value)
	{
		___Items_0 = value;
		Il2CppCodeGenWriteBarrier((&___Items_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAPRAWDATA_TF8450C3240F82CA0E23B1D062D7FC6554079475B_H
#ifndef MURMUR3_T9B5DE6D260DF16828C4D99596E8DE8EF28CA0D63_H
#define MURMUR3_T9B5DE6D260DF16828C4D99596E8DE8EF28CA0D63_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Murmur3
struct  Murmur3_t9B5DE6D260DF16828C4D99596E8DE8EF28CA0D63  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MURMUR3_T9B5DE6D260DF16828C4D99596E8DE8EF28CA0D63_H
#ifndef NATIVESHARE_TD828B308303066D1C6A16FC5BAAFFF2FC50BC905_H
#define NATIVESHARE_TD828B308303066D1C6A16FC5BAAFFF2FC50BC905_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NativeShare
struct  NativeShare_tD828B308303066D1C6A16FC5BAAFFF2FC50BC905  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATIVESHARE_TD828B308303066D1C6A16FC5BAAFFF2FC50BC905_H
#ifndef NETFXCOREWRAPPERS_TD2C7C0C4D3695246DB1CC58C809224C3B1D968BC_H
#define NETFXCOREWRAPPERS_TD2C7C0C4D3695246DB1CC58C809224C3B1D968BC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NetFXCoreWrappers
struct  NetFXCoreWrappers_tD2C7C0C4D3695246DB1CC58C809224C3B1D968BC  : public RuntimeObject
{
public:

public:
};

struct NetFXCoreWrappers_tD2C7C0C4D3695246DB1CC58C809224C3B1D968BC_StaticFields
{
public:
	// System.Type NetFXCoreWrappers::_compilerType
	Type_t * ____compilerType_0;

public:
	inline static int32_t get_offset_of__compilerType_0() { return static_cast<int32_t>(offsetof(NetFXCoreWrappers_tD2C7C0C4D3695246DB1CC58C809224C3B1D968BC_StaticFields, ____compilerType_0)); }
	inline Type_t * get__compilerType_0() const { return ____compilerType_0; }
	inline Type_t ** get_address_of__compilerType_0() { return &____compilerType_0; }
	inline void set__compilerType_0(Type_t * value)
	{
		____compilerType_0 = value;
		Il2CppCodeGenWriteBarrier((&____compilerType_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETFXCOREWRAPPERS_TD2C7C0C4D3695246DB1CC58C809224C3B1D968BC_H
#ifndef PIXWRAPPER_TB8CD50D0874CDEE9CF018F647FEE035B52003458_H
#define PIXWRAPPER_TB8CD50D0874CDEE9CF018F647FEE035B52003458_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PixWrapper
struct  PixWrapper_tB8CD50D0874CDEE9CF018F647FEE035B52003458  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PIXWRAPPER_TB8CD50D0874CDEE9CF018F647FEE035B52003458_H
#ifndef SAFEEVENT_T476B080AD6C587A6CAC0C20F14D54871A8CD34C4_H
#define SAFEEVENT_T476B080AD6C587A6CAC0C20F14D54871A8CD34C4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SafeEvent
struct  SafeEvent_t476B080AD6C587A6CAC0C20F14D54871A8CD34C4  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAFEEVENT_T476B080AD6C587A6CAC0C20F14D54871A8CD34C4_H
#ifndef ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#define ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef TASHKEELLOCATION_T02FFF4619086A02B2BCDF8F083015A9861F16C61_H
#define TASHKEELLOCATION_T02FFF4619086A02B2BCDF8F083015A9861F16C61_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TashkeelLocation
struct  TashkeelLocation_t02FFF4619086A02B2BCDF8F083015A9861F16C61  : public RuntimeObject
{
public:
	// System.Char TashkeelLocation::tashkeel
	Il2CppChar ___tashkeel_0;
	// System.Int32 TashkeelLocation::position
	int32_t ___position_1;

public:
	inline static int32_t get_offset_of_tashkeel_0() { return static_cast<int32_t>(offsetof(TashkeelLocation_t02FFF4619086A02B2BCDF8F083015A9861F16C61, ___tashkeel_0)); }
	inline Il2CppChar get_tashkeel_0() const { return ___tashkeel_0; }
	inline Il2CppChar* get_address_of_tashkeel_0() { return &___tashkeel_0; }
	inline void set_tashkeel_0(Il2CppChar value)
	{
		___tashkeel_0 = value;
	}

	inline static int32_t get_offset_of_position_1() { return static_cast<int32_t>(offsetof(TashkeelLocation_t02FFF4619086A02B2BCDF8F083015A9861F16C61, ___position_1)); }
	inline int32_t get_position_1() const { return ___position_1; }
	inline int32_t* get_address_of_position_1() { return &___position_1; }
	inline void set_position_1(int32_t value)
	{
		___position_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TASHKEELLOCATION_T02FFF4619086A02B2BCDF8F083015A9861F16C61_H
#ifndef TASKRUNNEREXTENSIONS_TB5716337975FB8CFB597423EE2C15E46BEF9995D_H
#define TASKRUNNEREXTENSIONS_TB5716337975FB8CFB597423EE2C15E46BEF9995D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TaskRunnerExtensions
struct  TaskRunnerExtensions_tB5716337975FB8CFB597423EE2C15E46BEF9995D  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TASKRUNNEREXTENSIONS_TB5716337975FB8CFB597423EE2C15E46BEF9995D_H
#ifndef UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#define UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * ___m_PersistentCalls_1;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_2;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_Calls_0)); }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_CallsDirty_2)); }
	inline bool get_m_CallsDirty_2() const { return ___m_CallsDirty_2; }
	inline bool* get_address_of_m_CallsDirty_2() { return &___m_CallsDirty_2; }
	inline void set_m_CallsDirty_2(bool value)
	{
		___m_CallsDirty_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#ifndef __STATICARRAYINITTYPESIZEU3D14_T12646BFA3861F306D3C2EA3328A781189C87D291_H
#define __STATICARRAYINITTYPESIZEU3D14_T12646BFA3861F306D3C2EA3328A781189C87D291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D14
struct  __StaticArrayInitTypeSizeU3D14_t12646BFA3861F306D3C2EA3328A781189C87D291 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D14_t12646BFA3861F306D3C2EA3328A781189C87D291__padding[14];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D14_T12646BFA3861F306D3C2EA3328A781189C87D291_H
#ifndef __STATICARRAYINITTYPESIZEU3D16_TE4C9301FB9E45F250EBDF09B231F154B1B53DEA5_H
#define __STATICARRAYINITTYPESIZEU3D16_TE4C9301FB9E45F250EBDF09B231F154B1B53DEA5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D16
struct  __StaticArrayInitTypeSizeU3D16_tE4C9301FB9E45F250EBDF09B231F154B1B53DEA5 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D16_tE4C9301FB9E45F250EBDF09B231F154B1B53DEA5__padding[16];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D16_TE4C9301FB9E45F250EBDF09B231F154B1B53DEA5_H
#ifndef PRESERVEATTRIBUTE_TF43F26FBD37217D5D84FA64A21C86AB4DCE29CC6_H
#define PRESERVEATTRIBUTE_TF43F26FBD37217D5D84FA64A21C86AB4DCE29CC6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModestTree.Util.PreserveAttribute
struct  PreserveAttribute_tF43F26FBD37217D5D84FA64A21C86AB4DCE29CC6  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRESERVEATTRIBUTE_TF43F26FBD37217D5D84FA64A21C86AB4DCE29CC6_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef UNITYEVENT_1_T6DD758393B13FC2A58BE44E647D9EBEA4F27D914_H
#define UNITYEVENT_1_T6DD758393B13FC2A58BE44E647D9EBEA4F27D914_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<System.Int32>
struct  UnityEvent_1_t6DD758393B13FC2A58BE44E647D9EBEA4F27D914  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_1_t6DD758393B13FC2A58BE44E647D9EBEA4F27D914, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T6DD758393B13FC2A58BE44E647D9EBEA4F27D914_H
#ifndef UNITYEVENT_1_T0194CB708072293BCFF0F7A26737132AC2BA7C62_H
#define UNITYEVENT_1_T0194CB708072293BCFF0F7A26737132AC2BA7C62_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<UnityEngine.Transform>
struct  UnityEvent_1_t0194CB708072293BCFF0F7A26737132AC2BA7C62  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_1_t0194CB708072293BCFF0F7A26737132AC2BA7C62, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T0194CB708072293BCFF0F7A26737132AC2BA7C62_H
#ifndef UNITYEVENT_2_T72075D01C70D2F1F80A5D4E03FECFAFA7BAAAFA8_H
#define UNITYEVENT_2_T72075D01C70D2F1F80A5D4E03FECFAFA7BAAAFA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`2<System.Int32,System.Int32>
struct  UnityEvent_2_t72075D01C70D2F1F80A5D4E03FECFAFA7BAAAFA8  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`2::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_2_t72075D01C70D2F1F80A5D4E03FECFAFA7BAAAFA8, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_2_T72075D01C70D2F1F80A5D4E03FECFAFA7BAAAFA8_H
#ifndef QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#define QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T80C24D421F26EA34451B875C2488070F0D2730FC_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T80C24D421F26EA34451B875C2488070F0D2730FC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t80C24D421F26EA34451B875C2488070F0D2730FC  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t80C24D421F26EA34451B875C2488070F0D2730FC_StaticFields
{
public:
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D14 <PrivateImplementationDetails>::73A7CA944158467943BFEB320F79099327A00212
	__StaticArrayInitTypeSizeU3D14_t12646BFA3861F306D3C2EA3328A781189C87D291  ___73A7CA944158467943BFEB320F79099327A00212_0;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D16 <PrivateImplementationDetails>::CE5AFBB0D2B25FA36AC018803534E8B5A9C5BBCA
	__StaticArrayInitTypeSizeU3D16_tE4C9301FB9E45F250EBDF09B231F154B1B53DEA5  ___CE5AFBB0D2B25FA36AC018803534E8B5A9C5BBCA_1;

public:
	inline static int32_t get_offset_of_U373A7CA944158467943BFEB320F79099327A00212_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t80C24D421F26EA34451B875C2488070F0D2730FC_StaticFields, ___73A7CA944158467943BFEB320F79099327A00212_0)); }
	inline __StaticArrayInitTypeSizeU3D14_t12646BFA3861F306D3C2EA3328A781189C87D291  get_U373A7CA944158467943BFEB320F79099327A00212_0() const { return ___73A7CA944158467943BFEB320F79099327A00212_0; }
	inline __StaticArrayInitTypeSizeU3D14_t12646BFA3861F306D3C2EA3328A781189C87D291 * get_address_of_U373A7CA944158467943BFEB320F79099327A00212_0() { return &___73A7CA944158467943BFEB320F79099327A00212_0; }
	inline void set_U373A7CA944158467943BFEB320F79099327A00212_0(__StaticArrayInitTypeSizeU3D14_t12646BFA3861F306D3C2EA3328A781189C87D291  value)
	{
		___73A7CA944158467943BFEB320F79099327A00212_0 = value;
	}

	inline static int32_t get_offset_of_CE5AFBB0D2B25FA36AC018803534E8B5A9C5BBCA_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t80C24D421F26EA34451B875C2488070F0D2730FC_StaticFields, ___CE5AFBB0D2B25FA36AC018803534E8B5A9C5BBCA_1)); }
	inline __StaticArrayInitTypeSizeU3D16_tE4C9301FB9E45F250EBDF09B231F154B1B53DEA5  get_CE5AFBB0D2B25FA36AC018803534E8B5A9C5BBCA_1() const { return ___CE5AFBB0D2B25FA36AC018803534E8B5A9C5BBCA_1; }
	inline __StaticArrayInitTypeSizeU3D16_tE4C9301FB9E45F250EBDF09B231F154B1B53DEA5 * get_address_of_CE5AFBB0D2B25FA36AC018803534E8B5A9C5BBCA_1() { return &___CE5AFBB0D2B25FA36AC018803534E8B5A9C5BBCA_1; }
	inline void set_CE5AFBB0D2B25FA36AC018803534E8B5A9C5BBCA_1(__StaticArrayInitTypeSizeU3D16_tE4C9301FB9E45F250EBDF09B231F154B1B53DEA5  value)
	{
		___CE5AFBB0D2B25FA36AC018803534E8B5A9C5BBCA_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T80C24D421F26EA34451B875C2488070F0D2730FC_H
#ifndef GENERALARABICLETTERS_T3A7ADF16DE49842C1265D032B8372869B07B9EBE_H
#define GENERALARABICLETTERS_T3A7ADF16DE49842C1265D032B8372869B07B9EBE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GeneralArabicLetters
struct  GeneralArabicLetters_t3A7ADF16DE49842C1265D032B8372869B07B9EBE 
{
public:
	// System.Int32 GeneralArabicLetters::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GeneralArabicLetters_t3A7ADF16DE49842C1265D032B8372869B07B9EBE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERALARABICLETTERS_T3A7ADF16DE49842C1265D032B8372869B07B9EBE_H
#ifndef ISOLATEDARABICLETTERS_TF5305790426783BA845F4BE47ECA15A8C837E9E9_H
#define ISOLATEDARABICLETTERS_TF5305790426783BA845F4BE47ECA15A8C837E9E9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IsolatedArabicLetters
struct  IsolatedArabicLetters_tF5305790426783BA845F4BE47ECA15A8C837E9E9 
{
public:
	// System.Int32 IsolatedArabicLetters::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(IsolatedArabicLetters_tF5305790426783BA845F4BE47ECA15A8C837E9E9, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ISOLATEDARABICLETTERS_TF5305790426783BA845F4BE47ECA15A8C837E9E9_H
#ifndef ACTIONINFO_TB61C754D893D43A27CA0ADCA6DDC1C26D2EB4A2D_H
#define ACTIONINFO_TB61C754D893D43A27CA0ADCA6DDC1C26D2EB4A2D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoenenGames.CyanLevelEditor.ActionInfo
struct  ActionInfo_tB61C754D893D43A27CA0ADCA6DDC1C26D2EB4A2D 
{
public:
	// System.Boolean MoenenGames.CyanLevelEditor.ActionInfo::IsCreate
	bool ___IsCreate_0;
	// System.Int32 MoenenGames.CyanLevelEditor.ActionInfo::PoolID
	int32_t ___PoolID_1;
	// System.Int32 MoenenGames.CyanLevelEditor.ActionInfo::PrefabID
	int32_t ___PrefabID_2;
	// System.Int32 MoenenGames.CyanLevelEditor.ActionInfo::TransformID
	int32_t ___TransformID_3;
	// UnityEngine.Vector3 MoenenGames.CyanLevelEditor.ActionInfo::Position
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___Position_4;
	// UnityEngine.Quaternion MoenenGames.CyanLevelEditor.ActionInfo::Rotation
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___Rotation_5;

public:
	inline static int32_t get_offset_of_IsCreate_0() { return static_cast<int32_t>(offsetof(ActionInfo_tB61C754D893D43A27CA0ADCA6DDC1C26D2EB4A2D, ___IsCreate_0)); }
	inline bool get_IsCreate_0() const { return ___IsCreate_0; }
	inline bool* get_address_of_IsCreate_0() { return &___IsCreate_0; }
	inline void set_IsCreate_0(bool value)
	{
		___IsCreate_0 = value;
	}

	inline static int32_t get_offset_of_PoolID_1() { return static_cast<int32_t>(offsetof(ActionInfo_tB61C754D893D43A27CA0ADCA6DDC1C26D2EB4A2D, ___PoolID_1)); }
	inline int32_t get_PoolID_1() const { return ___PoolID_1; }
	inline int32_t* get_address_of_PoolID_1() { return &___PoolID_1; }
	inline void set_PoolID_1(int32_t value)
	{
		___PoolID_1 = value;
	}

	inline static int32_t get_offset_of_PrefabID_2() { return static_cast<int32_t>(offsetof(ActionInfo_tB61C754D893D43A27CA0ADCA6DDC1C26D2EB4A2D, ___PrefabID_2)); }
	inline int32_t get_PrefabID_2() const { return ___PrefabID_2; }
	inline int32_t* get_address_of_PrefabID_2() { return &___PrefabID_2; }
	inline void set_PrefabID_2(int32_t value)
	{
		___PrefabID_2 = value;
	}

	inline static int32_t get_offset_of_TransformID_3() { return static_cast<int32_t>(offsetof(ActionInfo_tB61C754D893D43A27CA0ADCA6DDC1C26D2EB4A2D, ___TransformID_3)); }
	inline int32_t get_TransformID_3() const { return ___TransformID_3; }
	inline int32_t* get_address_of_TransformID_3() { return &___TransformID_3; }
	inline void set_TransformID_3(int32_t value)
	{
		___TransformID_3 = value;
	}

	inline static int32_t get_offset_of_Position_4() { return static_cast<int32_t>(offsetof(ActionInfo_tB61C754D893D43A27CA0ADCA6DDC1C26D2EB4A2D, ___Position_4)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_Position_4() const { return ___Position_4; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_Position_4() { return &___Position_4; }
	inline void set_Position_4(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___Position_4 = value;
	}

	inline static int32_t get_offset_of_Rotation_5() { return static_cast<int32_t>(offsetof(ActionInfo_tB61C754D893D43A27CA0ADCA6DDC1C26D2EB4A2D, ___Rotation_5)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_Rotation_5() const { return ___Rotation_5; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_Rotation_5() { return &___Rotation_5; }
	inline void set_Rotation_5(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___Rotation_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of MoenenGames.CyanLevelEditor.ActionInfo
struct ActionInfo_tB61C754D893D43A27CA0ADCA6DDC1C26D2EB4A2D_marshaled_pinvoke
{
	int32_t ___IsCreate_0;
	int32_t ___PoolID_1;
	int32_t ___PrefabID_2;
	int32_t ___TransformID_3;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___Position_4;
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___Rotation_5;
};
// Native definition for COM marshalling of MoenenGames.CyanLevelEditor.ActionInfo
struct ActionInfo_tB61C754D893D43A27CA0ADCA6DDC1C26D2EB4A2D_marshaled_com
{
	int32_t ___IsCreate_0;
	int32_t ___PoolID_1;
	int32_t ___PrefabID_2;
	int32_t ___TransformID_3;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___Position_4;
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___Rotation_5;
};
#endif // ACTIONINFO_TB61C754D893D43A27CA0ADCA6DDC1C26D2EB4A2D_H
#ifndef CYANDELETEMODE_T7C98F1C271A4F57C4A41C04CF787622FA2B49496_H
#define CYANDELETEMODE_T7C98F1C271A4F57C4A41C04CF787622FA2B49496_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoenenGames.CyanLevelEditor.CyanActionSetting_CyanDeleteMode
struct  CyanDeleteMode_t7C98F1C271A4F57C4A41C04CF787622FA2B49496 
{
public:
	// System.Int32 MoenenGames.CyanLevelEditor.CyanActionSetting_CyanDeleteMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CyanDeleteMode_t7C98F1C271A4F57C4A41C04CF787622FA2B49496, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CYANDELETEMODE_T7C98F1C271A4F57C4A41C04CF787622FA2B49496_H
#ifndef MOUSEBUTTON_TA407D242A3943936B7253771B2FB28D7E708AE74_H
#define MOUSEBUTTON_TA407D242A3943936B7253771B2FB28D7E708AE74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoenenGames.CyanLevelEditor.CyanActionSetting_MouseButton
struct  MouseButton_tA407D242A3943936B7253771B2FB28D7E708AE74 
{
public:
	// System.Int32 MoenenGames.CyanLevelEditor.CyanActionSetting_MouseButton::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MouseButton_tA407D242A3943936B7253771B2FB28D7E708AE74, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSEBUTTON_TA407D242A3943936B7253771B2FB28D7E708AE74_H
#ifndef CAMERAACTIONMODE_T17BCC1E677B2395A4CB8F310C078216D9559CAFB_H
#define CAMERAACTIONMODE_T17BCC1E677B2395A4CB8F310C078216D9559CAFB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoenenGames.CyanLevelEditor.CyanCameraSetting_CameraActionMode
struct  CameraActionMode_t17BCC1E677B2395A4CB8F310C078216D9559CAFB 
{
public:
	// System.Int32 MoenenGames.CyanLevelEditor.CyanCameraSetting_CameraActionMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CameraActionMode_t17BCC1E677B2395A4CB8F310C078216D9559CAFB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAACTIONMODE_T17BCC1E677B2395A4CB8F310C078216D9559CAFB_H
#ifndef CURSORSIZECHANGING_T9F1BD81C7E026F63F20F2954C6B177ADE60FB59B_H
#define CURSORSIZECHANGING_T9F1BD81C7E026F63F20F2954C6B177ADE60FB59B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoenenGames.CyanLevelEditor.CyanCursorSetting_CursorSizeChanging
struct  CursorSizeChanging_t9F1BD81C7E026F63F20F2954C6B177ADE60FB59B 
{
public:
	// System.Int32 MoenenGames.CyanLevelEditor.CyanCursorSetting_CursorSizeChanging::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CursorSizeChanging_t9F1BD81C7E026F63F20F2954C6B177ADE60FB59B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURSORSIZECHANGING_T9F1BD81C7E026F63F20F2954C6B177ADE60FB59B_H
#ifndef CYANINTINTRVENT_T3050562EFC3FABA385FF4019E611B224C7F39B2F_H
#define CYANINTINTRVENT_T3050562EFC3FABA385FF4019E611B224C7F39B2F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoenenGames.CyanLevelEditor.CyanMessage_CyanIntIntRvent
struct  CyanIntIntRvent_t3050562EFC3FABA385FF4019E611B224C7F39B2F  : public UnityEvent_2_t72075D01C70D2F1F80A5D4E03FECFAFA7BAAAFA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CYANINTINTRVENT_T3050562EFC3FABA385FF4019E611B224C7F39B2F_H
#ifndef CYANINTRVENT_T36B9E29A72B590D0D9FEDE9A938B67733AB53FC0_H
#define CYANINTRVENT_T36B9E29A72B590D0D9FEDE9A938B67733AB53FC0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoenenGames.CyanLevelEditor.CyanMessage_CyanIntRvent
struct  CyanIntRvent_t36B9E29A72B590D0D9FEDE9A938B67733AB53FC0  : public UnityEvent_1_t6DD758393B13FC2A58BE44E647D9EBEA4F27D914
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CYANINTRVENT_T36B9E29A72B590D0D9FEDE9A938B67733AB53FC0_H
#ifndef CYANTRANSFORMEVENT_T9A71D6662200E71DA050209321EBE077E3FD117C_H
#define CYANTRANSFORMEVENT_T9A71D6662200E71DA050209321EBE077E3FD117C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoenenGames.CyanLevelEditor.CyanMessage_CyanTransformEvent
struct  CyanTransformEvent_t9A71D6662200E71DA050209321EBE077E3FD117C  : public UnityEvent_1_t0194CB708072293BCFF0F7A26737132AC2BA7C62
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CYANTRANSFORMEVENT_T9A71D6662200E71DA050209321EBE077E3FD117C_H
#ifndef ITEMRAWDATA_T35493A15D30782C24AACFE62D54AF2D24AE80DC2_H
#define ITEMRAWDATA_T35493A15D30782C24AACFE62D54AF2D24AE80DC2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoenenGames.CyanLevelEditor.ItemRawData
struct  ItemRawData_t35493A15D30782C24AACFE62D54AF2D24AE80DC2  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 MoenenGames.CyanLevelEditor.ItemRawData::Position
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___Position_0;
	// UnityEngine.Vector3 MoenenGames.CyanLevelEditor.ItemRawData::Rotation
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___Rotation_1;
	// System.String MoenenGames.CyanLevelEditor.ItemRawData::Prefab
	String_t* ___Prefab_2;

public:
	inline static int32_t get_offset_of_Position_0() { return static_cast<int32_t>(offsetof(ItemRawData_t35493A15D30782C24AACFE62D54AF2D24AE80DC2, ___Position_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_Position_0() const { return ___Position_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_Position_0() { return &___Position_0; }
	inline void set_Position_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___Position_0 = value;
	}

	inline static int32_t get_offset_of_Rotation_1() { return static_cast<int32_t>(offsetof(ItemRawData_t35493A15D30782C24AACFE62D54AF2D24AE80DC2, ___Rotation_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_Rotation_1() const { return ___Rotation_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_Rotation_1() { return &___Rotation_1; }
	inline void set_Rotation_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___Rotation_1 = value;
	}

	inline static int32_t get_offset_of_Prefab_2() { return static_cast<int32_t>(offsetof(ItemRawData_t35493A15D30782C24AACFE62D54AF2D24AE80DC2, ___Prefab_2)); }
	inline String_t* get_Prefab_2() const { return ___Prefab_2; }
	inline String_t** get_address_of_Prefab_2() { return &___Prefab_2; }
	inline void set_Prefab_2(String_t* value)
	{
		___Prefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___Prefab_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ITEMRAWDATA_T35493A15D30782C24AACFE62D54AF2D24AE80DC2_H
#ifndef MAPITEM_T3958670AC2FB278DB1978A82F511B45DB681D6B5_H
#define MAPITEM_T3958670AC2FB278DB1978A82F511B45DB681D6B5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoenenGames.CyanLevelEditor.MapData_MapItem
struct  MapItem_t3958670AC2FB278DB1978A82F511B45DB681D6B5  : public RuntimeObject
{
public:
	// System.Int32 MoenenGames.CyanLevelEditor.MapData_MapItem::i
	int32_t ___i_0;
	// System.Int32 MoenenGames.CyanLevelEditor.MapData_MapItem::j
	int32_t ___j_1;
	// UnityEngine.Vector3 MoenenGames.CyanLevelEditor.MapData_MapItem::p
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___p_2;
	// UnityEngine.Vector3 MoenenGames.CyanLevelEditor.MapData_MapItem::q
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___q_3;

public:
	inline static int32_t get_offset_of_i_0() { return static_cast<int32_t>(offsetof(MapItem_t3958670AC2FB278DB1978A82F511B45DB681D6B5, ___i_0)); }
	inline int32_t get_i_0() const { return ___i_0; }
	inline int32_t* get_address_of_i_0() { return &___i_0; }
	inline void set_i_0(int32_t value)
	{
		___i_0 = value;
	}

	inline static int32_t get_offset_of_j_1() { return static_cast<int32_t>(offsetof(MapItem_t3958670AC2FB278DB1978A82F511B45DB681D6B5, ___j_1)); }
	inline int32_t get_j_1() const { return ___j_1; }
	inline int32_t* get_address_of_j_1() { return &___j_1; }
	inline void set_j_1(int32_t value)
	{
		___j_1 = value;
	}

	inline static int32_t get_offset_of_p_2() { return static_cast<int32_t>(offsetof(MapItem_t3958670AC2FB278DB1978A82F511B45DB681D6B5, ___p_2)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_p_2() const { return ___p_2; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_p_2() { return &___p_2; }
	inline void set_p_2(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___p_2 = value;
	}

	inline static int32_t get_offset_of_q_3() { return static_cast<int32_t>(offsetof(MapItem_t3958670AC2FB278DB1978A82F511B45DB681D6B5, ___q_3)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_q_3() const { return ___q_3; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_q_3() { return &___q_3; }
	inline void set_q_3(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___q_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAPITEM_T3958670AC2FB278DB1978A82F511B45DB681D6B5_H
#ifndef NULLABLE_1_T75BFB9848B0C0C6930A6973335B59621D36E6203_H
#define NULLABLE_1_T75BFB9848B0C0C6930A6973335B59621D36E6203_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<UnityEngine.Vector3>
struct  Nullable_1_t75BFB9848B0C0C6930A6973335B59621D36E6203 
{
public:
	// T System.Nullable`1::value
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t75BFB9848B0C0C6930A6973335B59621D36E6203, ___value_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_value_0() const { return ___value_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t75BFB9848B0C0C6930A6973335B59621D36E6203, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T75BFB9848B0C0C6930A6973335B59621D36E6203_H
#ifndef KEYCODE_TC93EA87C5A6901160B583ADFCD3EF6726570DC3C_H
#define KEYCODE_TC93EA87C5A6901160B583ADFCD3EF6726570DC3C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.KeyCode
struct  KeyCode_tC93EA87C5A6901160B583ADFCD3EF6726570DC3C 
{
public:
	// System.Int32 UnityEngine.KeyCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(KeyCode_tC93EA87C5A6901160B583ADFCD3EF6726570DC3C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYCODE_TC93EA87C5A6901160B583ADFCD3EF6726570DC3C_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef CYANACTIONSETTING_T52F579D228577FDDE87DC71F2C2DAB5B6CB7F46E_H
#define CYANACTIONSETTING_T52F579D228577FDDE87DC71F2C2DAB5B6CB7F46E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoenenGames.CyanLevelEditor.CyanActionSetting
struct  CyanActionSetting_t52F579D228577FDDE87DC71F2C2DAB5B6CB7F46E  : public RuntimeObject
{
public:
	// System.Int32 MoenenGames.CyanLevelEditor.CyanActionSetting::MaxUndoNum
	int32_t ___MaxUndoNum_0;
	// System.Single MoenenGames.CyanLevelEditor.CyanActionSetting::MouseHoldTime
	float ___MouseHoldTime_1;
	// System.Single MoenenGames.CyanLevelEditor.CyanActionSetting::MotionDely
	float ___MotionDely_2;
	// MoenenGames.CyanLevelEditor.CyanActionSetting_CyanDeleteMode MoenenGames.CyanLevelEditor.CyanActionSetting::DeleteMode
	int32_t ___DeleteMode_3;
	// System.Boolean MoenenGames.CyanLevelEditor.CyanActionSetting::PaintOnWall
	bool ___PaintOnWall_4;
	// System.Boolean MoenenGames.CyanLevelEditor.CyanActionSetting::PaintOnBottom
	bool ___PaintOnBottom_5;
	// System.Boolean MoenenGames.CyanLevelEditor.CyanActionSetting::PaintOutsideGround
	bool ___PaintOutsideGround_6;
	// System.Boolean MoenenGames.CyanLevelEditor.CyanActionSetting::PaintOnObject
	bool ___PaintOnObject_7;
	// MoenenGames.CyanLevelEditor.CyanActionSetting_MouseButton MoenenGames.CyanLevelEditor.CyanActionSetting::PaintObjectMouseButton
	int32_t ___PaintObjectMouseButton_8;
	// MoenenGames.CyanLevelEditor.CyanActionSetting_MouseButton MoenenGames.CyanLevelEditor.CyanActionSetting::SelectObjectMouseButton
	int32_t ___SelectObjectMouseButton_9;
	// MoenenGames.CyanLevelEditor.CyanActionSetting_MouseButton MoenenGames.CyanLevelEditor.CyanActionSetting::RotateObjectMouseButton
	int32_t ___RotateObjectMouseButton_10;
	// MoenenGames.CyanLevelEditor.CyanActionSetting_MouseButton MoenenGames.CyanLevelEditor.CyanActionSetting::DeleteObjectMouseButton
	int32_t ___DeleteObjectMouseButton_11;
	// MoenenGames.CyanLevelEditor.CyanActionSetting_MouseButton MoenenGames.CyanLevelEditor.CyanActionSetting::CancelPaintingMouseButton
	int32_t ___CancelPaintingMouseButton_12;
	// UnityEngine.KeyCode MoenenGames.CyanLevelEditor.CyanActionSetting::PaintObjectKey
	int32_t ___PaintObjectKey_13;
	// UnityEngine.KeyCode MoenenGames.CyanLevelEditor.CyanActionSetting::SelectObjectKey
	int32_t ___SelectObjectKey_14;
	// UnityEngine.KeyCode MoenenGames.CyanLevelEditor.CyanActionSetting::RotateObjectKey
	int32_t ___RotateObjectKey_15;
	// UnityEngine.KeyCode MoenenGames.CyanLevelEditor.CyanActionSetting::DeleteObjectKey
	int32_t ___DeleteObjectKey_16;
	// UnityEngine.KeyCode MoenenGames.CyanLevelEditor.CyanActionSetting::CancelPaintingKey
	int32_t ___CancelPaintingKey_17;

public:
	inline static int32_t get_offset_of_MaxUndoNum_0() { return static_cast<int32_t>(offsetof(CyanActionSetting_t52F579D228577FDDE87DC71F2C2DAB5B6CB7F46E, ___MaxUndoNum_0)); }
	inline int32_t get_MaxUndoNum_0() const { return ___MaxUndoNum_0; }
	inline int32_t* get_address_of_MaxUndoNum_0() { return &___MaxUndoNum_0; }
	inline void set_MaxUndoNum_0(int32_t value)
	{
		___MaxUndoNum_0 = value;
	}

	inline static int32_t get_offset_of_MouseHoldTime_1() { return static_cast<int32_t>(offsetof(CyanActionSetting_t52F579D228577FDDE87DC71F2C2DAB5B6CB7F46E, ___MouseHoldTime_1)); }
	inline float get_MouseHoldTime_1() const { return ___MouseHoldTime_1; }
	inline float* get_address_of_MouseHoldTime_1() { return &___MouseHoldTime_1; }
	inline void set_MouseHoldTime_1(float value)
	{
		___MouseHoldTime_1 = value;
	}

	inline static int32_t get_offset_of_MotionDely_2() { return static_cast<int32_t>(offsetof(CyanActionSetting_t52F579D228577FDDE87DC71F2C2DAB5B6CB7F46E, ___MotionDely_2)); }
	inline float get_MotionDely_2() const { return ___MotionDely_2; }
	inline float* get_address_of_MotionDely_2() { return &___MotionDely_2; }
	inline void set_MotionDely_2(float value)
	{
		___MotionDely_2 = value;
	}

	inline static int32_t get_offset_of_DeleteMode_3() { return static_cast<int32_t>(offsetof(CyanActionSetting_t52F579D228577FDDE87DC71F2C2DAB5B6CB7F46E, ___DeleteMode_3)); }
	inline int32_t get_DeleteMode_3() const { return ___DeleteMode_3; }
	inline int32_t* get_address_of_DeleteMode_3() { return &___DeleteMode_3; }
	inline void set_DeleteMode_3(int32_t value)
	{
		___DeleteMode_3 = value;
	}

	inline static int32_t get_offset_of_PaintOnWall_4() { return static_cast<int32_t>(offsetof(CyanActionSetting_t52F579D228577FDDE87DC71F2C2DAB5B6CB7F46E, ___PaintOnWall_4)); }
	inline bool get_PaintOnWall_4() const { return ___PaintOnWall_4; }
	inline bool* get_address_of_PaintOnWall_4() { return &___PaintOnWall_4; }
	inline void set_PaintOnWall_4(bool value)
	{
		___PaintOnWall_4 = value;
	}

	inline static int32_t get_offset_of_PaintOnBottom_5() { return static_cast<int32_t>(offsetof(CyanActionSetting_t52F579D228577FDDE87DC71F2C2DAB5B6CB7F46E, ___PaintOnBottom_5)); }
	inline bool get_PaintOnBottom_5() const { return ___PaintOnBottom_5; }
	inline bool* get_address_of_PaintOnBottom_5() { return &___PaintOnBottom_5; }
	inline void set_PaintOnBottom_5(bool value)
	{
		___PaintOnBottom_5 = value;
	}

	inline static int32_t get_offset_of_PaintOutsideGround_6() { return static_cast<int32_t>(offsetof(CyanActionSetting_t52F579D228577FDDE87DC71F2C2DAB5B6CB7F46E, ___PaintOutsideGround_6)); }
	inline bool get_PaintOutsideGround_6() const { return ___PaintOutsideGround_6; }
	inline bool* get_address_of_PaintOutsideGround_6() { return &___PaintOutsideGround_6; }
	inline void set_PaintOutsideGround_6(bool value)
	{
		___PaintOutsideGround_6 = value;
	}

	inline static int32_t get_offset_of_PaintOnObject_7() { return static_cast<int32_t>(offsetof(CyanActionSetting_t52F579D228577FDDE87DC71F2C2DAB5B6CB7F46E, ___PaintOnObject_7)); }
	inline bool get_PaintOnObject_7() const { return ___PaintOnObject_7; }
	inline bool* get_address_of_PaintOnObject_7() { return &___PaintOnObject_7; }
	inline void set_PaintOnObject_7(bool value)
	{
		___PaintOnObject_7 = value;
	}

	inline static int32_t get_offset_of_PaintObjectMouseButton_8() { return static_cast<int32_t>(offsetof(CyanActionSetting_t52F579D228577FDDE87DC71F2C2DAB5B6CB7F46E, ___PaintObjectMouseButton_8)); }
	inline int32_t get_PaintObjectMouseButton_8() const { return ___PaintObjectMouseButton_8; }
	inline int32_t* get_address_of_PaintObjectMouseButton_8() { return &___PaintObjectMouseButton_8; }
	inline void set_PaintObjectMouseButton_8(int32_t value)
	{
		___PaintObjectMouseButton_8 = value;
	}

	inline static int32_t get_offset_of_SelectObjectMouseButton_9() { return static_cast<int32_t>(offsetof(CyanActionSetting_t52F579D228577FDDE87DC71F2C2DAB5B6CB7F46E, ___SelectObjectMouseButton_9)); }
	inline int32_t get_SelectObjectMouseButton_9() const { return ___SelectObjectMouseButton_9; }
	inline int32_t* get_address_of_SelectObjectMouseButton_9() { return &___SelectObjectMouseButton_9; }
	inline void set_SelectObjectMouseButton_9(int32_t value)
	{
		___SelectObjectMouseButton_9 = value;
	}

	inline static int32_t get_offset_of_RotateObjectMouseButton_10() { return static_cast<int32_t>(offsetof(CyanActionSetting_t52F579D228577FDDE87DC71F2C2DAB5B6CB7F46E, ___RotateObjectMouseButton_10)); }
	inline int32_t get_RotateObjectMouseButton_10() const { return ___RotateObjectMouseButton_10; }
	inline int32_t* get_address_of_RotateObjectMouseButton_10() { return &___RotateObjectMouseButton_10; }
	inline void set_RotateObjectMouseButton_10(int32_t value)
	{
		___RotateObjectMouseButton_10 = value;
	}

	inline static int32_t get_offset_of_DeleteObjectMouseButton_11() { return static_cast<int32_t>(offsetof(CyanActionSetting_t52F579D228577FDDE87DC71F2C2DAB5B6CB7F46E, ___DeleteObjectMouseButton_11)); }
	inline int32_t get_DeleteObjectMouseButton_11() const { return ___DeleteObjectMouseButton_11; }
	inline int32_t* get_address_of_DeleteObjectMouseButton_11() { return &___DeleteObjectMouseButton_11; }
	inline void set_DeleteObjectMouseButton_11(int32_t value)
	{
		___DeleteObjectMouseButton_11 = value;
	}

	inline static int32_t get_offset_of_CancelPaintingMouseButton_12() { return static_cast<int32_t>(offsetof(CyanActionSetting_t52F579D228577FDDE87DC71F2C2DAB5B6CB7F46E, ___CancelPaintingMouseButton_12)); }
	inline int32_t get_CancelPaintingMouseButton_12() const { return ___CancelPaintingMouseButton_12; }
	inline int32_t* get_address_of_CancelPaintingMouseButton_12() { return &___CancelPaintingMouseButton_12; }
	inline void set_CancelPaintingMouseButton_12(int32_t value)
	{
		___CancelPaintingMouseButton_12 = value;
	}

	inline static int32_t get_offset_of_PaintObjectKey_13() { return static_cast<int32_t>(offsetof(CyanActionSetting_t52F579D228577FDDE87DC71F2C2DAB5B6CB7F46E, ___PaintObjectKey_13)); }
	inline int32_t get_PaintObjectKey_13() const { return ___PaintObjectKey_13; }
	inline int32_t* get_address_of_PaintObjectKey_13() { return &___PaintObjectKey_13; }
	inline void set_PaintObjectKey_13(int32_t value)
	{
		___PaintObjectKey_13 = value;
	}

	inline static int32_t get_offset_of_SelectObjectKey_14() { return static_cast<int32_t>(offsetof(CyanActionSetting_t52F579D228577FDDE87DC71F2C2DAB5B6CB7F46E, ___SelectObjectKey_14)); }
	inline int32_t get_SelectObjectKey_14() const { return ___SelectObjectKey_14; }
	inline int32_t* get_address_of_SelectObjectKey_14() { return &___SelectObjectKey_14; }
	inline void set_SelectObjectKey_14(int32_t value)
	{
		___SelectObjectKey_14 = value;
	}

	inline static int32_t get_offset_of_RotateObjectKey_15() { return static_cast<int32_t>(offsetof(CyanActionSetting_t52F579D228577FDDE87DC71F2C2DAB5B6CB7F46E, ___RotateObjectKey_15)); }
	inline int32_t get_RotateObjectKey_15() const { return ___RotateObjectKey_15; }
	inline int32_t* get_address_of_RotateObjectKey_15() { return &___RotateObjectKey_15; }
	inline void set_RotateObjectKey_15(int32_t value)
	{
		___RotateObjectKey_15 = value;
	}

	inline static int32_t get_offset_of_DeleteObjectKey_16() { return static_cast<int32_t>(offsetof(CyanActionSetting_t52F579D228577FDDE87DC71F2C2DAB5B6CB7F46E, ___DeleteObjectKey_16)); }
	inline int32_t get_DeleteObjectKey_16() const { return ___DeleteObjectKey_16; }
	inline int32_t* get_address_of_DeleteObjectKey_16() { return &___DeleteObjectKey_16; }
	inline void set_DeleteObjectKey_16(int32_t value)
	{
		___DeleteObjectKey_16 = value;
	}

	inline static int32_t get_offset_of_CancelPaintingKey_17() { return static_cast<int32_t>(offsetof(CyanActionSetting_t52F579D228577FDDE87DC71F2C2DAB5B6CB7F46E, ___CancelPaintingKey_17)); }
	inline int32_t get_CancelPaintingKey_17() const { return ___CancelPaintingKey_17; }
	inline int32_t* get_address_of_CancelPaintingKey_17() { return &___CancelPaintingKey_17; }
	inline void set_CancelPaintingKey_17(int32_t value)
	{
		___CancelPaintingKey_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CYANACTIONSETTING_T52F579D228577FDDE87DC71F2C2DAB5B6CB7F46E_H
#ifndef CYANCAMERASETTING_T206CA210D2F54015079229DBC666790D10EB7237_H
#define CYANCAMERASETTING_T206CA210D2F54015079229DBC666790D10EB7237_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoenenGames.CyanLevelEditor.CyanCameraSetting
struct  CyanCameraSetting_t206CA210D2F54015079229DBC666790D10EB7237  : public RuntimeObject
{
public:
	// System.Int32 MoenenGames.CyanLevelEditor.CyanCameraSetting::DragThreshold
	int32_t ___DragThreshold_0;
	// System.Single MoenenGames.CyanLevelEditor.CyanCameraSetting::MoveSpeed
	float ___MoveSpeed_1;
	// System.Boolean MoenenGames.CyanLevelEditor.CyanCameraSetting::ClampXZInsideGround
	bool ___ClampXZInsideGround_2;
	// System.Boolean MoenenGames.CyanLevelEditor.CyanCameraSetting::ClampYInsideGround
	bool ___ClampYInsideGround_3;
	// System.Single MoenenGames.CyanLevelEditor.CyanCameraSetting::RotateSpeed
	float ___RotateSpeed_4;
	// System.Single MoenenGames.CyanLevelEditor.CyanCameraSetting::AngelYMin
	float ___AngelYMin_5;
	// System.Single MoenenGames.CyanLevelEditor.CyanCameraSetting::AngelYMax
	float ___AngelYMax_6;
	// System.Single MoenenGames.CyanLevelEditor.CyanCameraSetting::AngelXMin
	float ___AngelXMin_7;
	// System.Single MoenenGames.CyanLevelEditor.CyanCameraSetting::AngelXMax
	float ___AngelXMax_8;
	// System.Single MoenenGames.CyanLevelEditor.CyanCameraSetting::DragZoomSpeed
	float ___DragZoomSpeed_9;
	// System.Single MoenenGames.CyanLevelEditor.CyanCameraSetting::WheelZoomSpeed
	float ___WheelZoomSpeed_10;
	// System.Single MoenenGames.CyanLevelEditor.CyanCameraSetting::MinZoomDistance
	float ___MinZoomDistance_11;
	// System.Single MoenenGames.CyanLevelEditor.CyanCameraSetting::MaxZoomDistance
	float ___MaxZoomDistance_12;
	// MoenenGames.CyanLevelEditor.CyanCameraSetting_CameraActionMode MoenenGames.CyanLevelEditor.CyanCameraSetting::MoveMouseA
	int32_t ___MoveMouseA_13;
	// MoenenGames.CyanLevelEditor.CyanCameraSetting_CameraActionMode MoenenGames.CyanLevelEditor.CyanCameraSetting::RotateMouseA
	int32_t ___RotateMouseA_14;
	// MoenenGames.CyanLevelEditor.CyanCameraSetting_CameraActionMode MoenenGames.CyanLevelEditor.CyanCameraSetting::ZoomMouseA
	int32_t ___ZoomMouseA_15;
	// UnityEngine.KeyCode MoenenGames.CyanLevelEditor.CyanCameraSetting::MoveKeyA
	int32_t ___MoveKeyA_16;
	// UnityEngine.KeyCode MoenenGames.CyanLevelEditor.CyanCameraSetting::RotateKeyA
	int32_t ___RotateKeyA_17;
	// UnityEngine.KeyCode MoenenGames.CyanLevelEditor.CyanCameraSetting::ZoomKeyA
	int32_t ___ZoomKeyA_18;
	// MoenenGames.CyanLevelEditor.CyanCameraSetting_CameraActionMode MoenenGames.CyanLevelEditor.CyanCameraSetting::MoveMouseB
	int32_t ___MoveMouseB_19;
	// MoenenGames.CyanLevelEditor.CyanCameraSetting_CameraActionMode MoenenGames.CyanLevelEditor.CyanCameraSetting::RotateMouseB
	int32_t ___RotateMouseB_20;
	// MoenenGames.CyanLevelEditor.CyanCameraSetting_CameraActionMode MoenenGames.CyanLevelEditor.CyanCameraSetting::ZoomMouseB
	int32_t ___ZoomMouseB_21;
	// UnityEngine.KeyCode MoenenGames.CyanLevelEditor.CyanCameraSetting::MoveKeyB
	int32_t ___MoveKeyB_22;
	// UnityEngine.KeyCode MoenenGames.CyanLevelEditor.CyanCameraSetting::RotateKeyB
	int32_t ___RotateKeyB_23;
	// UnityEngine.KeyCode MoenenGames.CyanLevelEditor.CyanCameraSetting::ZoomKeyB
	int32_t ___ZoomKeyB_24;

public:
	inline static int32_t get_offset_of_DragThreshold_0() { return static_cast<int32_t>(offsetof(CyanCameraSetting_t206CA210D2F54015079229DBC666790D10EB7237, ___DragThreshold_0)); }
	inline int32_t get_DragThreshold_0() const { return ___DragThreshold_0; }
	inline int32_t* get_address_of_DragThreshold_0() { return &___DragThreshold_0; }
	inline void set_DragThreshold_0(int32_t value)
	{
		___DragThreshold_0 = value;
	}

	inline static int32_t get_offset_of_MoveSpeed_1() { return static_cast<int32_t>(offsetof(CyanCameraSetting_t206CA210D2F54015079229DBC666790D10EB7237, ___MoveSpeed_1)); }
	inline float get_MoveSpeed_1() const { return ___MoveSpeed_1; }
	inline float* get_address_of_MoveSpeed_1() { return &___MoveSpeed_1; }
	inline void set_MoveSpeed_1(float value)
	{
		___MoveSpeed_1 = value;
	}

	inline static int32_t get_offset_of_ClampXZInsideGround_2() { return static_cast<int32_t>(offsetof(CyanCameraSetting_t206CA210D2F54015079229DBC666790D10EB7237, ___ClampXZInsideGround_2)); }
	inline bool get_ClampXZInsideGround_2() const { return ___ClampXZInsideGround_2; }
	inline bool* get_address_of_ClampXZInsideGround_2() { return &___ClampXZInsideGround_2; }
	inline void set_ClampXZInsideGround_2(bool value)
	{
		___ClampXZInsideGround_2 = value;
	}

	inline static int32_t get_offset_of_ClampYInsideGround_3() { return static_cast<int32_t>(offsetof(CyanCameraSetting_t206CA210D2F54015079229DBC666790D10EB7237, ___ClampYInsideGround_3)); }
	inline bool get_ClampYInsideGround_3() const { return ___ClampYInsideGround_3; }
	inline bool* get_address_of_ClampYInsideGround_3() { return &___ClampYInsideGround_3; }
	inline void set_ClampYInsideGround_3(bool value)
	{
		___ClampYInsideGround_3 = value;
	}

	inline static int32_t get_offset_of_RotateSpeed_4() { return static_cast<int32_t>(offsetof(CyanCameraSetting_t206CA210D2F54015079229DBC666790D10EB7237, ___RotateSpeed_4)); }
	inline float get_RotateSpeed_4() const { return ___RotateSpeed_4; }
	inline float* get_address_of_RotateSpeed_4() { return &___RotateSpeed_4; }
	inline void set_RotateSpeed_4(float value)
	{
		___RotateSpeed_4 = value;
	}

	inline static int32_t get_offset_of_AngelYMin_5() { return static_cast<int32_t>(offsetof(CyanCameraSetting_t206CA210D2F54015079229DBC666790D10EB7237, ___AngelYMin_5)); }
	inline float get_AngelYMin_5() const { return ___AngelYMin_5; }
	inline float* get_address_of_AngelYMin_5() { return &___AngelYMin_5; }
	inline void set_AngelYMin_5(float value)
	{
		___AngelYMin_5 = value;
	}

	inline static int32_t get_offset_of_AngelYMax_6() { return static_cast<int32_t>(offsetof(CyanCameraSetting_t206CA210D2F54015079229DBC666790D10EB7237, ___AngelYMax_6)); }
	inline float get_AngelYMax_6() const { return ___AngelYMax_6; }
	inline float* get_address_of_AngelYMax_6() { return &___AngelYMax_6; }
	inline void set_AngelYMax_6(float value)
	{
		___AngelYMax_6 = value;
	}

	inline static int32_t get_offset_of_AngelXMin_7() { return static_cast<int32_t>(offsetof(CyanCameraSetting_t206CA210D2F54015079229DBC666790D10EB7237, ___AngelXMin_7)); }
	inline float get_AngelXMin_7() const { return ___AngelXMin_7; }
	inline float* get_address_of_AngelXMin_7() { return &___AngelXMin_7; }
	inline void set_AngelXMin_7(float value)
	{
		___AngelXMin_7 = value;
	}

	inline static int32_t get_offset_of_AngelXMax_8() { return static_cast<int32_t>(offsetof(CyanCameraSetting_t206CA210D2F54015079229DBC666790D10EB7237, ___AngelXMax_8)); }
	inline float get_AngelXMax_8() const { return ___AngelXMax_8; }
	inline float* get_address_of_AngelXMax_8() { return &___AngelXMax_8; }
	inline void set_AngelXMax_8(float value)
	{
		___AngelXMax_8 = value;
	}

	inline static int32_t get_offset_of_DragZoomSpeed_9() { return static_cast<int32_t>(offsetof(CyanCameraSetting_t206CA210D2F54015079229DBC666790D10EB7237, ___DragZoomSpeed_9)); }
	inline float get_DragZoomSpeed_9() const { return ___DragZoomSpeed_9; }
	inline float* get_address_of_DragZoomSpeed_9() { return &___DragZoomSpeed_9; }
	inline void set_DragZoomSpeed_9(float value)
	{
		___DragZoomSpeed_9 = value;
	}

	inline static int32_t get_offset_of_WheelZoomSpeed_10() { return static_cast<int32_t>(offsetof(CyanCameraSetting_t206CA210D2F54015079229DBC666790D10EB7237, ___WheelZoomSpeed_10)); }
	inline float get_WheelZoomSpeed_10() const { return ___WheelZoomSpeed_10; }
	inline float* get_address_of_WheelZoomSpeed_10() { return &___WheelZoomSpeed_10; }
	inline void set_WheelZoomSpeed_10(float value)
	{
		___WheelZoomSpeed_10 = value;
	}

	inline static int32_t get_offset_of_MinZoomDistance_11() { return static_cast<int32_t>(offsetof(CyanCameraSetting_t206CA210D2F54015079229DBC666790D10EB7237, ___MinZoomDistance_11)); }
	inline float get_MinZoomDistance_11() const { return ___MinZoomDistance_11; }
	inline float* get_address_of_MinZoomDistance_11() { return &___MinZoomDistance_11; }
	inline void set_MinZoomDistance_11(float value)
	{
		___MinZoomDistance_11 = value;
	}

	inline static int32_t get_offset_of_MaxZoomDistance_12() { return static_cast<int32_t>(offsetof(CyanCameraSetting_t206CA210D2F54015079229DBC666790D10EB7237, ___MaxZoomDistance_12)); }
	inline float get_MaxZoomDistance_12() const { return ___MaxZoomDistance_12; }
	inline float* get_address_of_MaxZoomDistance_12() { return &___MaxZoomDistance_12; }
	inline void set_MaxZoomDistance_12(float value)
	{
		___MaxZoomDistance_12 = value;
	}

	inline static int32_t get_offset_of_MoveMouseA_13() { return static_cast<int32_t>(offsetof(CyanCameraSetting_t206CA210D2F54015079229DBC666790D10EB7237, ___MoveMouseA_13)); }
	inline int32_t get_MoveMouseA_13() const { return ___MoveMouseA_13; }
	inline int32_t* get_address_of_MoveMouseA_13() { return &___MoveMouseA_13; }
	inline void set_MoveMouseA_13(int32_t value)
	{
		___MoveMouseA_13 = value;
	}

	inline static int32_t get_offset_of_RotateMouseA_14() { return static_cast<int32_t>(offsetof(CyanCameraSetting_t206CA210D2F54015079229DBC666790D10EB7237, ___RotateMouseA_14)); }
	inline int32_t get_RotateMouseA_14() const { return ___RotateMouseA_14; }
	inline int32_t* get_address_of_RotateMouseA_14() { return &___RotateMouseA_14; }
	inline void set_RotateMouseA_14(int32_t value)
	{
		___RotateMouseA_14 = value;
	}

	inline static int32_t get_offset_of_ZoomMouseA_15() { return static_cast<int32_t>(offsetof(CyanCameraSetting_t206CA210D2F54015079229DBC666790D10EB7237, ___ZoomMouseA_15)); }
	inline int32_t get_ZoomMouseA_15() const { return ___ZoomMouseA_15; }
	inline int32_t* get_address_of_ZoomMouseA_15() { return &___ZoomMouseA_15; }
	inline void set_ZoomMouseA_15(int32_t value)
	{
		___ZoomMouseA_15 = value;
	}

	inline static int32_t get_offset_of_MoveKeyA_16() { return static_cast<int32_t>(offsetof(CyanCameraSetting_t206CA210D2F54015079229DBC666790D10EB7237, ___MoveKeyA_16)); }
	inline int32_t get_MoveKeyA_16() const { return ___MoveKeyA_16; }
	inline int32_t* get_address_of_MoveKeyA_16() { return &___MoveKeyA_16; }
	inline void set_MoveKeyA_16(int32_t value)
	{
		___MoveKeyA_16 = value;
	}

	inline static int32_t get_offset_of_RotateKeyA_17() { return static_cast<int32_t>(offsetof(CyanCameraSetting_t206CA210D2F54015079229DBC666790D10EB7237, ___RotateKeyA_17)); }
	inline int32_t get_RotateKeyA_17() const { return ___RotateKeyA_17; }
	inline int32_t* get_address_of_RotateKeyA_17() { return &___RotateKeyA_17; }
	inline void set_RotateKeyA_17(int32_t value)
	{
		___RotateKeyA_17 = value;
	}

	inline static int32_t get_offset_of_ZoomKeyA_18() { return static_cast<int32_t>(offsetof(CyanCameraSetting_t206CA210D2F54015079229DBC666790D10EB7237, ___ZoomKeyA_18)); }
	inline int32_t get_ZoomKeyA_18() const { return ___ZoomKeyA_18; }
	inline int32_t* get_address_of_ZoomKeyA_18() { return &___ZoomKeyA_18; }
	inline void set_ZoomKeyA_18(int32_t value)
	{
		___ZoomKeyA_18 = value;
	}

	inline static int32_t get_offset_of_MoveMouseB_19() { return static_cast<int32_t>(offsetof(CyanCameraSetting_t206CA210D2F54015079229DBC666790D10EB7237, ___MoveMouseB_19)); }
	inline int32_t get_MoveMouseB_19() const { return ___MoveMouseB_19; }
	inline int32_t* get_address_of_MoveMouseB_19() { return &___MoveMouseB_19; }
	inline void set_MoveMouseB_19(int32_t value)
	{
		___MoveMouseB_19 = value;
	}

	inline static int32_t get_offset_of_RotateMouseB_20() { return static_cast<int32_t>(offsetof(CyanCameraSetting_t206CA210D2F54015079229DBC666790D10EB7237, ___RotateMouseB_20)); }
	inline int32_t get_RotateMouseB_20() const { return ___RotateMouseB_20; }
	inline int32_t* get_address_of_RotateMouseB_20() { return &___RotateMouseB_20; }
	inline void set_RotateMouseB_20(int32_t value)
	{
		___RotateMouseB_20 = value;
	}

	inline static int32_t get_offset_of_ZoomMouseB_21() { return static_cast<int32_t>(offsetof(CyanCameraSetting_t206CA210D2F54015079229DBC666790D10EB7237, ___ZoomMouseB_21)); }
	inline int32_t get_ZoomMouseB_21() const { return ___ZoomMouseB_21; }
	inline int32_t* get_address_of_ZoomMouseB_21() { return &___ZoomMouseB_21; }
	inline void set_ZoomMouseB_21(int32_t value)
	{
		___ZoomMouseB_21 = value;
	}

	inline static int32_t get_offset_of_MoveKeyB_22() { return static_cast<int32_t>(offsetof(CyanCameraSetting_t206CA210D2F54015079229DBC666790D10EB7237, ___MoveKeyB_22)); }
	inline int32_t get_MoveKeyB_22() const { return ___MoveKeyB_22; }
	inline int32_t* get_address_of_MoveKeyB_22() { return &___MoveKeyB_22; }
	inline void set_MoveKeyB_22(int32_t value)
	{
		___MoveKeyB_22 = value;
	}

	inline static int32_t get_offset_of_RotateKeyB_23() { return static_cast<int32_t>(offsetof(CyanCameraSetting_t206CA210D2F54015079229DBC666790D10EB7237, ___RotateKeyB_23)); }
	inline int32_t get_RotateKeyB_23() const { return ___RotateKeyB_23; }
	inline int32_t* get_address_of_RotateKeyB_23() { return &___RotateKeyB_23; }
	inline void set_RotateKeyB_23(int32_t value)
	{
		___RotateKeyB_23 = value;
	}

	inline static int32_t get_offset_of_ZoomKeyB_24() { return static_cast<int32_t>(offsetof(CyanCameraSetting_t206CA210D2F54015079229DBC666790D10EB7237, ___ZoomKeyB_24)); }
	inline int32_t get_ZoomKeyB_24() const { return ___ZoomKeyB_24; }
	inline int32_t* get_address_of_ZoomKeyB_24() { return &___ZoomKeyB_24; }
	inline void set_ZoomKeyB_24(int32_t value)
	{
		___ZoomKeyB_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CYANCAMERASETTING_T206CA210D2F54015079229DBC666790D10EB7237_H
#ifndef CYANCURSORSETTING_T80DBA731373075E7ACA488DBB3733A88089199D3_H
#define CYANCURSORSETTING_T80DBA731373075E7ACA488DBB3733A88089199D3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoenenGames.CyanLevelEditor.CyanCursorSetting
struct  CyanCursorSetting_t80DBA731373075E7ACA488DBB3733A88089199D3  : public RuntimeObject
{
public:
	// System.Boolean MoenenGames.CyanLevelEditor.CyanCursorSetting::ShowCursor
	bool ___ShowCursor_0;
	// System.Boolean MoenenGames.CyanLevelEditor.CyanCursorSetting::ShowPaitingPrefab
	bool ___ShowPaitingPrefab_1;
	// System.Boolean MoenenGames.CyanLevelEditor.CyanCursorSetting::HighLightHoveringObject
	bool ___HighLightHoveringObject_2;
	// System.Boolean MoenenGames.CyanLevelEditor.CyanCursorSetting::HighLightSelectingObject
	bool ___HighLightSelectingObject_3;
	// System.Boolean MoenenGames.CyanLevelEditor.CyanCursorSetting::CrosswiseBySurface
	bool ___CrosswiseBySurface_4;
	// System.Single MoenenGames.CyanLevelEditor.CyanCursorSetting::MotionLerpRate
	float ___MotionLerpRate_5;
	// MoenenGames.CyanLevelEditor.CyanCursorSetting_CursorSizeChanging MoenenGames.CyanLevelEditor.CyanCursorSetting::CursorSizeChangeBy
	int32_t ___CursorSizeChangeBy_6;

public:
	inline static int32_t get_offset_of_ShowCursor_0() { return static_cast<int32_t>(offsetof(CyanCursorSetting_t80DBA731373075E7ACA488DBB3733A88089199D3, ___ShowCursor_0)); }
	inline bool get_ShowCursor_0() const { return ___ShowCursor_0; }
	inline bool* get_address_of_ShowCursor_0() { return &___ShowCursor_0; }
	inline void set_ShowCursor_0(bool value)
	{
		___ShowCursor_0 = value;
	}

	inline static int32_t get_offset_of_ShowPaitingPrefab_1() { return static_cast<int32_t>(offsetof(CyanCursorSetting_t80DBA731373075E7ACA488DBB3733A88089199D3, ___ShowPaitingPrefab_1)); }
	inline bool get_ShowPaitingPrefab_1() const { return ___ShowPaitingPrefab_1; }
	inline bool* get_address_of_ShowPaitingPrefab_1() { return &___ShowPaitingPrefab_1; }
	inline void set_ShowPaitingPrefab_1(bool value)
	{
		___ShowPaitingPrefab_1 = value;
	}

	inline static int32_t get_offset_of_HighLightHoveringObject_2() { return static_cast<int32_t>(offsetof(CyanCursorSetting_t80DBA731373075E7ACA488DBB3733A88089199D3, ___HighLightHoveringObject_2)); }
	inline bool get_HighLightHoveringObject_2() const { return ___HighLightHoveringObject_2; }
	inline bool* get_address_of_HighLightHoveringObject_2() { return &___HighLightHoveringObject_2; }
	inline void set_HighLightHoveringObject_2(bool value)
	{
		___HighLightHoveringObject_2 = value;
	}

	inline static int32_t get_offset_of_HighLightSelectingObject_3() { return static_cast<int32_t>(offsetof(CyanCursorSetting_t80DBA731373075E7ACA488DBB3733A88089199D3, ___HighLightSelectingObject_3)); }
	inline bool get_HighLightSelectingObject_3() const { return ___HighLightSelectingObject_3; }
	inline bool* get_address_of_HighLightSelectingObject_3() { return &___HighLightSelectingObject_3; }
	inline void set_HighLightSelectingObject_3(bool value)
	{
		___HighLightSelectingObject_3 = value;
	}

	inline static int32_t get_offset_of_CrosswiseBySurface_4() { return static_cast<int32_t>(offsetof(CyanCursorSetting_t80DBA731373075E7ACA488DBB3733A88089199D3, ___CrosswiseBySurface_4)); }
	inline bool get_CrosswiseBySurface_4() const { return ___CrosswiseBySurface_4; }
	inline bool* get_address_of_CrosswiseBySurface_4() { return &___CrosswiseBySurface_4; }
	inline void set_CrosswiseBySurface_4(bool value)
	{
		___CrosswiseBySurface_4 = value;
	}

	inline static int32_t get_offset_of_MotionLerpRate_5() { return static_cast<int32_t>(offsetof(CyanCursorSetting_t80DBA731373075E7ACA488DBB3733A88089199D3, ___MotionLerpRate_5)); }
	inline float get_MotionLerpRate_5() const { return ___MotionLerpRate_5; }
	inline float* get_address_of_MotionLerpRate_5() { return &___MotionLerpRate_5; }
	inline void set_MotionLerpRate_5(float value)
	{
		___MotionLerpRate_5 = value;
	}

	inline static int32_t get_offset_of_CursorSizeChangeBy_6() { return static_cast<int32_t>(offsetof(CyanCursorSetting_t80DBA731373075E7ACA488DBB3733A88089199D3, ___CursorSizeChangeBy_6)); }
	inline int32_t get_CursorSizeChangeBy_6() const { return ___CursorSizeChangeBy_6; }
	inline int32_t* get_address_of_CursorSizeChangeBy_6() { return &___CursorSizeChangeBy_6; }
	inline void set_CursorSizeChangeBy_6(int32_t value)
	{
		___CursorSizeChangeBy_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CYANCURSORSETTING_T80DBA731373075E7ACA488DBB3733A88089199D3_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef FIX3DTEXTCS_T9927D5BD5BF86B691C134D9D53DCC21C262941D3_H
#define FIX3DTEXTCS_T9927D5BD5BF86B691C134D9D53DCC21C262941D3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Fix3dTextCS
struct  Fix3dTextCS_t9927D5BD5BF86B691C134D9D53DCC21C262941D3  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String Fix3dTextCS::text
	String_t* ___text_4;
	// System.Boolean Fix3dTextCS::tashkeel
	bool ___tashkeel_5;
	// System.Boolean Fix3dTextCS::hinduNumbers
	bool ___hinduNumbers_6;

public:
	inline static int32_t get_offset_of_text_4() { return static_cast<int32_t>(offsetof(Fix3dTextCS_t9927D5BD5BF86B691C134D9D53DCC21C262941D3, ___text_4)); }
	inline String_t* get_text_4() const { return ___text_4; }
	inline String_t** get_address_of_text_4() { return &___text_4; }
	inline void set_text_4(String_t* value)
	{
		___text_4 = value;
		Il2CppCodeGenWriteBarrier((&___text_4), value);
	}

	inline static int32_t get_offset_of_tashkeel_5() { return static_cast<int32_t>(offsetof(Fix3dTextCS_t9927D5BD5BF86B691C134D9D53DCC21C262941D3, ___tashkeel_5)); }
	inline bool get_tashkeel_5() const { return ___tashkeel_5; }
	inline bool* get_address_of_tashkeel_5() { return &___tashkeel_5; }
	inline void set_tashkeel_5(bool value)
	{
		___tashkeel_5 = value;
	}

	inline static int32_t get_offset_of_hinduNumbers_6() { return static_cast<int32_t>(offsetof(Fix3dTextCS_t9927D5BD5BF86B691C134D9D53DCC21C262941D3, ___hinduNumbers_6)); }
	inline bool get_hinduNumbers_6() const { return ___hinduNumbers_6; }
	inline bool* get_address_of_hinduNumbers_6() { return &___hinduNumbers_6; }
	inline void set_hinduNumbers_6(bool value)
	{
		___hinduNumbers_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIX3DTEXTCS_T9927D5BD5BF86B691C134D9D53DCC21C262941D3_H
#ifndef FIXGUITEXTCS_T0E5CB7AABD8A55325DE47FF00F2C9A154272FC2B_H
#define FIXGUITEXTCS_T0E5CB7AABD8A55325DE47FF00F2C9A154272FC2B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FixGUITextCS
struct  FixGUITextCS_t0E5CB7AABD8A55325DE47FF00F2C9A154272FC2B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String FixGUITextCS::text
	String_t* ___text_4;
	// System.Boolean FixGUITextCS::tashkeel
	bool ___tashkeel_5;
	// System.Boolean FixGUITextCS::hinduNumbers
	bool ___hinduNumbers_6;

public:
	inline static int32_t get_offset_of_text_4() { return static_cast<int32_t>(offsetof(FixGUITextCS_t0E5CB7AABD8A55325DE47FF00F2C9A154272FC2B, ___text_4)); }
	inline String_t* get_text_4() const { return ___text_4; }
	inline String_t** get_address_of_text_4() { return &___text_4; }
	inline void set_text_4(String_t* value)
	{
		___text_4 = value;
		Il2CppCodeGenWriteBarrier((&___text_4), value);
	}

	inline static int32_t get_offset_of_tashkeel_5() { return static_cast<int32_t>(offsetof(FixGUITextCS_t0E5CB7AABD8A55325DE47FF00F2C9A154272FC2B, ___tashkeel_5)); }
	inline bool get_tashkeel_5() const { return ___tashkeel_5; }
	inline bool* get_address_of_tashkeel_5() { return &___tashkeel_5; }
	inline void set_tashkeel_5(bool value)
	{
		___tashkeel_5 = value;
	}

	inline static int32_t get_offset_of_hinduNumbers_6() { return static_cast<int32_t>(offsetof(FixGUITextCS_t0E5CB7AABD8A55325DE47FF00F2C9A154272FC2B, ___hinduNumbers_6)); }
	inline bool get_hinduNumbers_6() const { return ___hinduNumbers_6; }
	inline bool* get_address_of_hinduNumbers_6() { return &___hinduNumbers_6; }
	inline void set_hinduNumbers_6(bool value)
	{
		___hinduNumbers_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIXGUITEXTCS_T0E5CB7AABD8A55325DE47FF00F2C9A154272FC2B_H
#ifndef FIXTEXTMESHPROUGUI_T67EB4FC50DAFB4D252C6F4BF294DA593CA013D49_H
#define FIXTEXTMESHPROUGUI_T67EB4FC50DAFB4D252C6F4BF294DA593CA013D49_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FixTextMeshProUGUI
struct  FixTextMeshProUGUI_t67EB4FC50DAFB4D252C6F4BF294DA593CA013D49  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String FixTextMeshProUGUI::text
	String_t* ___text_4;
	// System.Boolean FixTextMeshProUGUI::tashkeel
	bool ___tashkeel_5;
	// System.Boolean FixTextMeshProUGUI::hinduNumbers
	bool ___hinduNumbers_6;

public:
	inline static int32_t get_offset_of_text_4() { return static_cast<int32_t>(offsetof(FixTextMeshProUGUI_t67EB4FC50DAFB4D252C6F4BF294DA593CA013D49, ___text_4)); }
	inline String_t* get_text_4() const { return ___text_4; }
	inline String_t** get_address_of_text_4() { return &___text_4; }
	inline void set_text_4(String_t* value)
	{
		___text_4 = value;
		Il2CppCodeGenWriteBarrier((&___text_4), value);
	}

	inline static int32_t get_offset_of_tashkeel_5() { return static_cast<int32_t>(offsetof(FixTextMeshProUGUI_t67EB4FC50DAFB4D252C6F4BF294DA593CA013D49, ___tashkeel_5)); }
	inline bool get_tashkeel_5() const { return ___tashkeel_5; }
	inline bool* get_address_of_tashkeel_5() { return &___tashkeel_5; }
	inline void set_tashkeel_5(bool value)
	{
		___tashkeel_5 = value;
	}

	inline static int32_t get_offset_of_hinduNumbers_6() { return static_cast<int32_t>(offsetof(FixTextMeshProUGUI_t67EB4FC50DAFB4D252C6F4BF294DA593CA013D49, ___hinduNumbers_6)); }
	inline bool get_hinduNumbers_6() const { return ___hinduNumbers_6; }
	inline bool* get_address_of_hinduNumbers_6() { return &___hinduNumbers_6; }
	inline void set_hinduNumbers_6(bool value)
	{
		___hinduNumbers_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIXTEXTMESHPROUGUI_T67EB4FC50DAFB4D252C6F4BF294DA593CA013D49_H
#ifndef PLATFORMBASE_TCBFDE4E2C964A9DC16353A32B6F7974D5107187B_H
#define PLATFORMBASE_TCBFDE4E2C964A9DC16353A32B6F7974D5107187B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Platforms.PlatformBase
struct  PlatformBase_tCBFDE4E2C964A9DC16353A32B6F7974D5107187B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Collections.Generic.List`1<System.Action> GameSparks.Platforms.PlatformBase::_actions
	List_1_t9E7F53A878191B703A76E69CE36AA90A372800E7 * ____actions_7;
	// System.Collections.Generic.List`1<System.Action> GameSparks.Platforms.PlatformBase::_currentActions
	List_1_t9E7F53A878191B703A76E69CE36AA90A372800E7 * ____currentActions_8;
	// System.Boolean GameSparks.Platforms.PlatformBase::_allowQuitting
	bool ____allowQuitting_9;
	// System.String GameSparks.Platforms.PlatformBase::<DeviceName>k__BackingField
	String_t* ___U3CDeviceNameU3Ek__BackingField_10;
	// System.String GameSparks.Platforms.PlatformBase::<DeviceType>k__BackingField
	String_t* ___U3CDeviceTypeU3Ek__BackingField_11;
	// GameSparks.Core.GSData GameSparks.Platforms.PlatformBase::<DeviceStats>k__BackingField
	GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 * ___U3CDeviceStatsU3Ek__BackingField_12;
	// System.String GameSparks.Platforms.PlatformBase::<DeviceId>k__BackingField
	String_t* ___U3CDeviceIdU3Ek__BackingField_13;
	// System.String GameSparks.Platforms.PlatformBase::<Platform>k__BackingField
	String_t* ___U3CPlatformU3Ek__BackingField_14;
	// System.Boolean GameSparks.Platforms.PlatformBase::<ExtraDebug>k__BackingField
	bool ___U3CExtraDebugU3Ek__BackingField_15;
	// System.String GameSparks.Platforms.PlatformBase::<PersistentDataPath>k__BackingField
	String_t* ___U3CPersistentDataPathU3Ek__BackingField_16;
	// System.String GameSparks.Platforms.PlatformBase::m_authToken
	String_t* ___m_authToken_17;
	// System.String GameSparks.Platforms.PlatformBase::m_userId
	String_t* ___m_userId_18;
	// System.Action`1<System.Exception> GameSparks.Platforms.PlatformBase::<ExceptionReporter>k__BackingField
	Action_1_t114FA5D778AE43CC5A12F16A8CCD137F67ED6962 * ___U3CExceptionReporterU3Ek__BackingField_19;

public:
	inline static int32_t get_offset_of__actions_7() { return static_cast<int32_t>(offsetof(PlatformBase_tCBFDE4E2C964A9DC16353A32B6F7974D5107187B, ____actions_7)); }
	inline List_1_t9E7F53A878191B703A76E69CE36AA90A372800E7 * get__actions_7() const { return ____actions_7; }
	inline List_1_t9E7F53A878191B703A76E69CE36AA90A372800E7 ** get_address_of__actions_7() { return &____actions_7; }
	inline void set__actions_7(List_1_t9E7F53A878191B703A76E69CE36AA90A372800E7 * value)
	{
		____actions_7 = value;
		Il2CppCodeGenWriteBarrier((&____actions_7), value);
	}

	inline static int32_t get_offset_of__currentActions_8() { return static_cast<int32_t>(offsetof(PlatformBase_tCBFDE4E2C964A9DC16353A32B6F7974D5107187B, ____currentActions_8)); }
	inline List_1_t9E7F53A878191B703A76E69CE36AA90A372800E7 * get__currentActions_8() const { return ____currentActions_8; }
	inline List_1_t9E7F53A878191B703A76E69CE36AA90A372800E7 ** get_address_of__currentActions_8() { return &____currentActions_8; }
	inline void set__currentActions_8(List_1_t9E7F53A878191B703A76E69CE36AA90A372800E7 * value)
	{
		____currentActions_8 = value;
		Il2CppCodeGenWriteBarrier((&____currentActions_8), value);
	}

	inline static int32_t get_offset_of__allowQuitting_9() { return static_cast<int32_t>(offsetof(PlatformBase_tCBFDE4E2C964A9DC16353A32B6F7974D5107187B, ____allowQuitting_9)); }
	inline bool get__allowQuitting_9() const { return ____allowQuitting_9; }
	inline bool* get_address_of__allowQuitting_9() { return &____allowQuitting_9; }
	inline void set__allowQuitting_9(bool value)
	{
		____allowQuitting_9 = value;
	}

	inline static int32_t get_offset_of_U3CDeviceNameU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(PlatformBase_tCBFDE4E2C964A9DC16353A32B6F7974D5107187B, ___U3CDeviceNameU3Ek__BackingField_10)); }
	inline String_t* get_U3CDeviceNameU3Ek__BackingField_10() const { return ___U3CDeviceNameU3Ek__BackingField_10; }
	inline String_t** get_address_of_U3CDeviceNameU3Ek__BackingField_10() { return &___U3CDeviceNameU3Ek__BackingField_10; }
	inline void set_U3CDeviceNameU3Ek__BackingField_10(String_t* value)
	{
		___U3CDeviceNameU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDeviceNameU3Ek__BackingField_10), value);
	}

	inline static int32_t get_offset_of_U3CDeviceTypeU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(PlatformBase_tCBFDE4E2C964A9DC16353A32B6F7974D5107187B, ___U3CDeviceTypeU3Ek__BackingField_11)); }
	inline String_t* get_U3CDeviceTypeU3Ek__BackingField_11() const { return ___U3CDeviceTypeU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CDeviceTypeU3Ek__BackingField_11() { return &___U3CDeviceTypeU3Ek__BackingField_11; }
	inline void set_U3CDeviceTypeU3Ek__BackingField_11(String_t* value)
	{
		___U3CDeviceTypeU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDeviceTypeU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CDeviceStatsU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(PlatformBase_tCBFDE4E2C964A9DC16353A32B6F7974D5107187B, ___U3CDeviceStatsU3Ek__BackingField_12)); }
	inline GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 * get_U3CDeviceStatsU3Ek__BackingField_12() const { return ___U3CDeviceStatsU3Ek__BackingField_12; }
	inline GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 ** get_address_of_U3CDeviceStatsU3Ek__BackingField_12() { return &___U3CDeviceStatsU3Ek__BackingField_12; }
	inline void set_U3CDeviceStatsU3Ek__BackingField_12(GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 * value)
	{
		___U3CDeviceStatsU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDeviceStatsU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CDeviceIdU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(PlatformBase_tCBFDE4E2C964A9DC16353A32B6F7974D5107187B, ___U3CDeviceIdU3Ek__BackingField_13)); }
	inline String_t* get_U3CDeviceIdU3Ek__BackingField_13() const { return ___U3CDeviceIdU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CDeviceIdU3Ek__BackingField_13() { return &___U3CDeviceIdU3Ek__BackingField_13; }
	inline void set_U3CDeviceIdU3Ek__BackingField_13(String_t* value)
	{
		___U3CDeviceIdU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDeviceIdU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_U3CPlatformU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(PlatformBase_tCBFDE4E2C964A9DC16353A32B6F7974D5107187B, ___U3CPlatformU3Ek__BackingField_14)); }
	inline String_t* get_U3CPlatformU3Ek__BackingField_14() const { return ___U3CPlatformU3Ek__BackingField_14; }
	inline String_t** get_address_of_U3CPlatformU3Ek__BackingField_14() { return &___U3CPlatformU3Ek__BackingField_14; }
	inline void set_U3CPlatformU3Ek__BackingField_14(String_t* value)
	{
		___U3CPlatformU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPlatformU3Ek__BackingField_14), value);
	}

	inline static int32_t get_offset_of_U3CExtraDebugU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(PlatformBase_tCBFDE4E2C964A9DC16353A32B6F7974D5107187B, ___U3CExtraDebugU3Ek__BackingField_15)); }
	inline bool get_U3CExtraDebugU3Ek__BackingField_15() const { return ___U3CExtraDebugU3Ek__BackingField_15; }
	inline bool* get_address_of_U3CExtraDebugU3Ek__BackingField_15() { return &___U3CExtraDebugU3Ek__BackingField_15; }
	inline void set_U3CExtraDebugU3Ek__BackingField_15(bool value)
	{
		___U3CExtraDebugU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3CPersistentDataPathU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(PlatformBase_tCBFDE4E2C964A9DC16353A32B6F7974D5107187B, ___U3CPersistentDataPathU3Ek__BackingField_16)); }
	inline String_t* get_U3CPersistentDataPathU3Ek__BackingField_16() const { return ___U3CPersistentDataPathU3Ek__BackingField_16; }
	inline String_t** get_address_of_U3CPersistentDataPathU3Ek__BackingField_16() { return &___U3CPersistentDataPathU3Ek__BackingField_16; }
	inline void set_U3CPersistentDataPathU3Ek__BackingField_16(String_t* value)
	{
		___U3CPersistentDataPathU3Ek__BackingField_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPersistentDataPathU3Ek__BackingField_16), value);
	}

	inline static int32_t get_offset_of_m_authToken_17() { return static_cast<int32_t>(offsetof(PlatformBase_tCBFDE4E2C964A9DC16353A32B6F7974D5107187B, ___m_authToken_17)); }
	inline String_t* get_m_authToken_17() const { return ___m_authToken_17; }
	inline String_t** get_address_of_m_authToken_17() { return &___m_authToken_17; }
	inline void set_m_authToken_17(String_t* value)
	{
		___m_authToken_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_authToken_17), value);
	}

	inline static int32_t get_offset_of_m_userId_18() { return static_cast<int32_t>(offsetof(PlatformBase_tCBFDE4E2C964A9DC16353A32B6F7974D5107187B, ___m_userId_18)); }
	inline String_t* get_m_userId_18() const { return ___m_userId_18; }
	inline String_t** get_address_of_m_userId_18() { return &___m_userId_18; }
	inline void set_m_userId_18(String_t* value)
	{
		___m_userId_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_userId_18), value);
	}

	inline static int32_t get_offset_of_U3CExceptionReporterU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(PlatformBase_tCBFDE4E2C964A9DC16353A32B6F7974D5107187B, ___U3CExceptionReporterU3Ek__BackingField_19)); }
	inline Action_1_t114FA5D778AE43CC5A12F16A8CCD137F67ED6962 * get_U3CExceptionReporterU3Ek__BackingField_19() const { return ___U3CExceptionReporterU3Ek__BackingField_19; }
	inline Action_1_t114FA5D778AE43CC5A12F16A8CCD137F67ED6962 ** get_address_of_U3CExceptionReporterU3Ek__BackingField_19() { return &___U3CExceptionReporterU3Ek__BackingField_19; }
	inline void set_U3CExceptionReporterU3Ek__BackingField_19(Action_1_t114FA5D778AE43CC5A12F16A8CCD137F67ED6962 * value)
	{
		___U3CExceptionReporterU3Ek__BackingField_19 = value;
		Il2CppCodeGenWriteBarrier((&___U3CExceptionReporterU3Ek__BackingField_19), value);
	}
};

struct PlatformBase_tCBFDE4E2C964A9DC16353A32B6F7974D5107187B_StaticFields
{
public:
	// System.String GameSparks.Platforms.PlatformBase::PLAYER_PREF_AUTHTOKEN_KEY
	String_t* ___PLAYER_PREF_AUTHTOKEN_KEY_4;
	// System.String GameSparks.Platforms.PlatformBase::PLAYER_PREF_USERID_KEY
	String_t* ___PLAYER_PREF_USERID_KEY_5;
	// System.String GameSparks.Platforms.PlatformBase::PLAYER_PREF_DEVICEID_KEY
	String_t* ___PLAYER_PREF_DEVICEID_KEY_6;

public:
	inline static int32_t get_offset_of_PLAYER_PREF_AUTHTOKEN_KEY_4() { return static_cast<int32_t>(offsetof(PlatformBase_tCBFDE4E2C964A9DC16353A32B6F7974D5107187B_StaticFields, ___PLAYER_PREF_AUTHTOKEN_KEY_4)); }
	inline String_t* get_PLAYER_PREF_AUTHTOKEN_KEY_4() const { return ___PLAYER_PREF_AUTHTOKEN_KEY_4; }
	inline String_t** get_address_of_PLAYER_PREF_AUTHTOKEN_KEY_4() { return &___PLAYER_PREF_AUTHTOKEN_KEY_4; }
	inline void set_PLAYER_PREF_AUTHTOKEN_KEY_4(String_t* value)
	{
		___PLAYER_PREF_AUTHTOKEN_KEY_4 = value;
		Il2CppCodeGenWriteBarrier((&___PLAYER_PREF_AUTHTOKEN_KEY_4), value);
	}

	inline static int32_t get_offset_of_PLAYER_PREF_USERID_KEY_5() { return static_cast<int32_t>(offsetof(PlatformBase_tCBFDE4E2C964A9DC16353A32B6F7974D5107187B_StaticFields, ___PLAYER_PREF_USERID_KEY_5)); }
	inline String_t* get_PLAYER_PREF_USERID_KEY_5() const { return ___PLAYER_PREF_USERID_KEY_5; }
	inline String_t** get_address_of_PLAYER_PREF_USERID_KEY_5() { return &___PLAYER_PREF_USERID_KEY_5; }
	inline void set_PLAYER_PREF_USERID_KEY_5(String_t* value)
	{
		___PLAYER_PREF_USERID_KEY_5 = value;
		Il2CppCodeGenWriteBarrier((&___PLAYER_PREF_USERID_KEY_5), value);
	}

	inline static int32_t get_offset_of_PLAYER_PREF_DEVICEID_KEY_6() { return static_cast<int32_t>(offsetof(PlatformBase_tCBFDE4E2C964A9DC16353A32B6F7974D5107187B_StaticFields, ___PLAYER_PREF_DEVICEID_KEY_6)); }
	inline String_t* get_PLAYER_PREF_DEVICEID_KEY_6() const { return ___PLAYER_PREF_DEVICEID_KEY_6; }
	inline String_t** get_address_of_PLAYER_PREF_DEVICEID_KEY_6() { return &___PLAYER_PREF_DEVICEID_KEY_6; }
	inline void set_PLAYER_PREF_DEVICEID_KEY_6(String_t* value)
	{
		___PLAYER_PREF_DEVICEID_KEY_6 = value;
		Il2CppCodeGenWriteBarrier((&___PLAYER_PREF_DEVICEID_KEY_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORMBASE_TCBFDE4E2C964A9DC16353A32B6F7974D5107187B_H
#ifndef WEBSOCKETCONTROLLER_T058EEAFBC2A602648C67D685CB9310E3DE4DCB9E_H
#define WEBSOCKETCONTROLLER_T058EEAFBC2A602648C67D685CB9310E3DE4DCB9E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Platforms.WebSocketController
struct  WebSocketController_t058EEAFBC2A602648C67D685CB9310E3DE4DCB9E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String GameSparks.Platforms.WebSocketController::<GSName>k__BackingField
	String_t* ___U3CGSNameU3Ek__BackingField_4;
	// System.Collections.Generic.List`1<GameSparks.Platforms.IControlledWebSocket> GameSparks.Platforms.WebSocketController::webSockets
	List_1_tA6F692DEC0215E08FB5821E35DE9B5D5C2B35EDC * ___webSockets_5;
	// System.Boolean GameSparks.Platforms.WebSocketController::websocketCollectionModified
	bool ___websocketCollectionModified_6;

public:
	inline static int32_t get_offset_of_U3CGSNameU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(WebSocketController_t058EEAFBC2A602648C67D685CB9310E3DE4DCB9E, ___U3CGSNameU3Ek__BackingField_4)); }
	inline String_t* get_U3CGSNameU3Ek__BackingField_4() const { return ___U3CGSNameU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CGSNameU3Ek__BackingField_4() { return &___U3CGSNameU3Ek__BackingField_4; }
	inline void set_U3CGSNameU3Ek__BackingField_4(String_t* value)
	{
		___U3CGSNameU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGSNameU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_webSockets_5() { return static_cast<int32_t>(offsetof(WebSocketController_t058EEAFBC2A602648C67D685CB9310E3DE4DCB9E, ___webSockets_5)); }
	inline List_1_tA6F692DEC0215E08FB5821E35DE9B5D5C2B35EDC * get_webSockets_5() const { return ___webSockets_5; }
	inline List_1_tA6F692DEC0215E08FB5821E35DE9B5D5C2B35EDC ** get_address_of_webSockets_5() { return &___webSockets_5; }
	inline void set_webSockets_5(List_1_tA6F692DEC0215E08FB5821E35DE9B5D5C2B35EDC * value)
	{
		___webSockets_5 = value;
		Il2CppCodeGenWriteBarrier((&___webSockets_5), value);
	}

	inline static int32_t get_offset_of_websocketCollectionModified_6() { return static_cast<int32_t>(offsetof(WebSocketController_t058EEAFBC2A602648C67D685CB9310E3DE4DCB9E, ___websocketCollectionModified_6)); }
	inline bool get_websocketCollectionModified_6() const { return ___websocketCollectionModified_6; }
	inline bool* get_address_of_websocketCollectionModified_6() { return &___websocketCollectionModified_6; }
	inline void set_websocketCollectionModified_6(bool value)
	{
		___websocketCollectionModified_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBSOCKETCONTROLLER_T058EEAFBC2A602648C67D685CB9310E3DE4DCB9E_H
#ifndef CYANLEVELEDITOR_T22E46946CA890622F3B9069EF816C703F7C664B6_H
#define CYANLEVELEDITOR_T22E46946CA890622F3B9069EF816C703F7C664B6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoenenGames.CyanLevelEditor.CyanLevelEditor
struct  CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// MoenenGames.CyanLevelEditor.CyanActionSetting MoenenGames.CyanLevelEditor.CyanLevelEditor::ActionSetting
	CyanActionSetting_t52F579D228577FDDE87DC71F2C2DAB5B6CB7F46E * ___ActionSetting_4;
	// MoenenGames.CyanLevelEditor.CyanCursorSetting MoenenGames.CyanLevelEditor.CyanLevelEditor::CursorSetting
	CyanCursorSetting_t80DBA731373075E7ACA488DBB3733A88089199D3 * ___CursorSetting_5;
	// MoenenGames.CyanLevelEditor.CyanCameraSetting MoenenGames.CyanLevelEditor.CyanLevelEditor::CameraSetting
	CyanCameraSetting_t206CA210D2F54015079229DBC666790D10EB7237 * ___CameraSetting_6;
	// MoenenGames.CyanLevelEditor.CyanGridSetting MoenenGames.CyanLevelEditor.CyanLevelEditor::GridSetting
	CyanGridSetting_tF2D64053897D26131BDCF7A3F2675BD2161BE8CF * ___GridSetting_7;
	// MoenenGames.CyanLevelEditor.CyanComponent MoenenGames.CyanLevelEditor.CyanLevelEditor::Components
	CyanComponent_tC7880F9103B559C4E6EB97745CE3A56781CDB3BC * ___Components_8;
	// MoenenGames.CyanLevelEditor.CyanMessage MoenenGames.CyanLevelEditor.CyanLevelEditor::Messages
	CyanMessage_t5A284AED26BF63EDC0379206EBE0289EDF8B4D26 * ___Messages_9;
	// MoenenGames.CyanLevelEditor.CyanObjectPool[] MoenenGames.CyanLevelEditor.CyanLevelEditor::PrefabPools
	CyanObjectPoolU5BU5D_tB4F9ED39CA989B62B12CCC787FA3FEFCE3D0C3F8* ___PrefabPools_10;
	// System.Int32 MoenenGames.CyanLevelEditor.CyanLevelEditor::ShowingPoolIndex
	int32_t ___ShowingPoolIndex_13;
	// System.Int32 MoenenGames.CyanLevelEditor.CyanLevelEditor::ShowingPrefabIndex
	int32_t ___ShowingPrefabIndex_14;
	// System.Int32 MoenenGames.CyanLevelEditor.CyanLevelEditor::ShowingPrefabSelfPoolIndex
	int32_t ___ShowingPrefabSelfPoolIndex_15;
	// UnityEngine.Transform MoenenGames.CyanLevelEditor.CyanLevelEditor::PaintingPrefabTF
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___PaintingPrefabTF_16;
	// UnityEngine.Collider MoenenGames.CyanLevelEditor.CyanLevelEditor::PaintingPrefabCollider
	Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * ___PaintingPrefabCollider_17;
	// UnityEngine.Collider MoenenGames.CyanLevelEditor.CyanLevelEditor::HoveringObjectCollider
	Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * ___HoveringObjectCollider_18;
	// UnityEngine.Collider MoenenGames.CyanLevelEditor.CyanLevelEditor::SelectingObjectCollider
	Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * ___SelectingObjectCollider_19;
	// System.Boolean MoenenGames.CyanLevelEditor.CyanLevelEditor::MouseChanging
	bool ___MouseChanging_20;
	// System.Single MoenenGames.CyanLevelEditor.CyanLevelEditor::CameraZoomDistance
	float ___CameraZoomDistance_21;
	// UnityEngine.Vector3 MoenenGames.CyanLevelEditor.CyanLevelEditor::CameraAimPivot
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___CameraAimPivot_22;
	// System.Nullable`1<UnityEngine.Vector3> MoenenGames.CyanLevelEditor.CyanLevelEditor::CameraStartChangeMousePosition
	Nullable_1_t75BFB9848B0C0C6930A6973335B59621D36E6203  ___CameraStartChangeMousePosition_23;
	// System.Boolean MoenenGames.CyanLevelEditor.CyanLevelEditor::CanPaint
	bool ___CanPaint_24;
	// System.Boolean MoenenGames.CyanLevelEditor.CyanLevelEditor::InsideGround
	bool ___InsideGround_25;
	// System.Boolean MoenenGames.CyanLevelEditor.CyanLevelEditor::OnObject
	bool ___OnObject_26;
	// System.Single MoenenGames.CyanLevelEditor.CyanLevelEditor::LastMouseMoveTime
	float ___LastMouseMoveTime_27;
	// System.Single MoenenGames.CyanLevelEditor.CyanLevelEditor::LeftHoldTime
	float ___LeftHoldTime_28;
	// System.Single MoenenGames.CyanLevelEditor.CyanLevelEditor::LeftHoldTimeLoop
	float ___LeftHoldTimeLoop_29;
	// System.Single MoenenGames.CyanLevelEditor.CyanLevelEditor::RightHoldTime
	float ___RightHoldTime_30;
	// System.Single MoenenGames.CyanLevelEditor.CyanLevelEditor::RightHoldTimeLoop
	float ___RightHoldTimeLoop_31;
	// System.Single MoenenGames.CyanLevelEditor.CyanLevelEditor::MotionDelyTime
	float ___MotionDelyTime_32;
	// UnityEngine.Vector3 MoenenGames.CyanLevelEditor.CyanLevelEditor::RayCastPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___RayCastPos_33;
	// UnityEngine.Vector3 MoenenGames.CyanLevelEditor.CyanLevelEditor::RayCastNormal
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___RayCastNormal_34;
	// UnityEngine.Vector3 MoenenGames.CyanLevelEditor.CyanLevelEditor::AimCursorPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___AimCursorPos_35;
	// UnityEngine.Vector3 MoenenGames.CyanLevelEditor.CyanLevelEditor::AimPrefabPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___AimPrefabPos_36;
	// UnityEngine.Vector3 MoenenGames.CyanLevelEditor.CyanLevelEditor::HoverHighLightOffset
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___HoverHighLightOffset_37;
	// UnityEngine.Vector3 MoenenGames.CyanLevelEditor.CyanLevelEditor::SelectHighLightOffset
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___SelectHighLightOffset_38;
	// UnityEngine.Quaternion MoenenGames.CyanLevelEditor.CyanLevelEditor::PaintingRotation
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___PaintingRotation_39;
	// UnityEngine.Quaternion MoenenGames.CyanLevelEditor.CyanLevelEditor::AimPrefabRot
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___AimPrefabRot_40;
	// UnityEngine.Quaternion MoenenGames.CyanLevelEditor.CyanLevelEditor::AimCursorRot
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___AimCursorRot_41;
	// System.Collections.Generic.List`1<MoenenGames.CyanLevelEditor.ActionInfo> MoenenGames.CyanLevelEditor.CyanLevelEditor::UndoList
	List_1_tC9F26360E86F3EED335ADB7E6838E9C069B6039F * ___UndoList_42;
	// System.Collections.Generic.List`1<MoenenGames.CyanLevelEditor.ActionInfo> MoenenGames.CyanLevelEditor.CyanLevelEditor::RedoList
	List_1_tC9F26360E86F3EED335ADB7E6838E9C069B6039F * ___RedoList_43;

public:
	inline static int32_t get_offset_of_ActionSetting_4() { return static_cast<int32_t>(offsetof(CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6, ___ActionSetting_4)); }
	inline CyanActionSetting_t52F579D228577FDDE87DC71F2C2DAB5B6CB7F46E * get_ActionSetting_4() const { return ___ActionSetting_4; }
	inline CyanActionSetting_t52F579D228577FDDE87DC71F2C2DAB5B6CB7F46E ** get_address_of_ActionSetting_4() { return &___ActionSetting_4; }
	inline void set_ActionSetting_4(CyanActionSetting_t52F579D228577FDDE87DC71F2C2DAB5B6CB7F46E * value)
	{
		___ActionSetting_4 = value;
		Il2CppCodeGenWriteBarrier((&___ActionSetting_4), value);
	}

	inline static int32_t get_offset_of_CursorSetting_5() { return static_cast<int32_t>(offsetof(CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6, ___CursorSetting_5)); }
	inline CyanCursorSetting_t80DBA731373075E7ACA488DBB3733A88089199D3 * get_CursorSetting_5() const { return ___CursorSetting_5; }
	inline CyanCursorSetting_t80DBA731373075E7ACA488DBB3733A88089199D3 ** get_address_of_CursorSetting_5() { return &___CursorSetting_5; }
	inline void set_CursorSetting_5(CyanCursorSetting_t80DBA731373075E7ACA488DBB3733A88089199D3 * value)
	{
		___CursorSetting_5 = value;
		Il2CppCodeGenWriteBarrier((&___CursorSetting_5), value);
	}

	inline static int32_t get_offset_of_CameraSetting_6() { return static_cast<int32_t>(offsetof(CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6, ___CameraSetting_6)); }
	inline CyanCameraSetting_t206CA210D2F54015079229DBC666790D10EB7237 * get_CameraSetting_6() const { return ___CameraSetting_6; }
	inline CyanCameraSetting_t206CA210D2F54015079229DBC666790D10EB7237 ** get_address_of_CameraSetting_6() { return &___CameraSetting_6; }
	inline void set_CameraSetting_6(CyanCameraSetting_t206CA210D2F54015079229DBC666790D10EB7237 * value)
	{
		___CameraSetting_6 = value;
		Il2CppCodeGenWriteBarrier((&___CameraSetting_6), value);
	}

	inline static int32_t get_offset_of_GridSetting_7() { return static_cast<int32_t>(offsetof(CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6, ___GridSetting_7)); }
	inline CyanGridSetting_tF2D64053897D26131BDCF7A3F2675BD2161BE8CF * get_GridSetting_7() const { return ___GridSetting_7; }
	inline CyanGridSetting_tF2D64053897D26131BDCF7A3F2675BD2161BE8CF ** get_address_of_GridSetting_7() { return &___GridSetting_7; }
	inline void set_GridSetting_7(CyanGridSetting_tF2D64053897D26131BDCF7A3F2675BD2161BE8CF * value)
	{
		___GridSetting_7 = value;
		Il2CppCodeGenWriteBarrier((&___GridSetting_7), value);
	}

	inline static int32_t get_offset_of_Components_8() { return static_cast<int32_t>(offsetof(CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6, ___Components_8)); }
	inline CyanComponent_tC7880F9103B559C4E6EB97745CE3A56781CDB3BC * get_Components_8() const { return ___Components_8; }
	inline CyanComponent_tC7880F9103B559C4E6EB97745CE3A56781CDB3BC ** get_address_of_Components_8() { return &___Components_8; }
	inline void set_Components_8(CyanComponent_tC7880F9103B559C4E6EB97745CE3A56781CDB3BC * value)
	{
		___Components_8 = value;
		Il2CppCodeGenWriteBarrier((&___Components_8), value);
	}

	inline static int32_t get_offset_of_Messages_9() { return static_cast<int32_t>(offsetof(CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6, ___Messages_9)); }
	inline CyanMessage_t5A284AED26BF63EDC0379206EBE0289EDF8B4D26 * get_Messages_9() const { return ___Messages_9; }
	inline CyanMessage_t5A284AED26BF63EDC0379206EBE0289EDF8B4D26 ** get_address_of_Messages_9() { return &___Messages_9; }
	inline void set_Messages_9(CyanMessage_t5A284AED26BF63EDC0379206EBE0289EDF8B4D26 * value)
	{
		___Messages_9 = value;
		Il2CppCodeGenWriteBarrier((&___Messages_9), value);
	}

	inline static int32_t get_offset_of_PrefabPools_10() { return static_cast<int32_t>(offsetof(CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6, ___PrefabPools_10)); }
	inline CyanObjectPoolU5BU5D_tB4F9ED39CA989B62B12CCC787FA3FEFCE3D0C3F8* get_PrefabPools_10() const { return ___PrefabPools_10; }
	inline CyanObjectPoolU5BU5D_tB4F9ED39CA989B62B12CCC787FA3FEFCE3D0C3F8** get_address_of_PrefabPools_10() { return &___PrefabPools_10; }
	inline void set_PrefabPools_10(CyanObjectPoolU5BU5D_tB4F9ED39CA989B62B12CCC787FA3FEFCE3D0C3F8* value)
	{
		___PrefabPools_10 = value;
		Il2CppCodeGenWriteBarrier((&___PrefabPools_10), value);
	}

	inline static int32_t get_offset_of_ShowingPoolIndex_13() { return static_cast<int32_t>(offsetof(CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6, ___ShowingPoolIndex_13)); }
	inline int32_t get_ShowingPoolIndex_13() const { return ___ShowingPoolIndex_13; }
	inline int32_t* get_address_of_ShowingPoolIndex_13() { return &___ShowingPoolIndex_13; }
	inline void set_ShowingPoolIndex_13(int32_t value)
	{
		___ShowingPoolIndex_13 = value;
	}

	inline static int32_t get_offset_of_ShowingPrefabIndex_14() { return static_cast<int32_t>(offsetof(CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6, ___ShowingPrefabIndex_14)); }
	inline int32_t get_ShowingPrefabIndex_14() const { return ___ShowingPrefabIndex_14; }
	inline int32_t* get_address_of_ShowingPrefabIndex_14() { return &___ShowingPrefabIndex_14; }
	inline void set_ShowingPrefabIndex_14(int32_t value)
	{
		___ShowingPrefabIndex_14 = value;
	}

	inline static int32_t get_offset_of_ShowingPrefabSelfPoolIndex_15() { return static_cast<int32_t>(offsetof(CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6, ___ShowingPrefabSelfPoolIndex_15)); }
	inline int32_t get_ShowingPrefabSelfPoolIndex_15() const { return ___ShowingPrefabSelfPoolIndex_15; }
	inline int32_t* get_address_of_ShowingPrefabSelfPoolIndex_15() { return &___ShowingPrefabSelfPoolIndex_15; }
	inline void set_ShowingPrefabSelfPoolIndex_15(int32_t value)
	{
		___ShowingPrefabSelfPoolIndex_15 = value;
	}

	inline static int32_t get_offset_of_PaintingPrefabTF_16() { return static_cast<int32_t>(offsetof(CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6, ___PaintingPrefabTF_16)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_PaintingPrefabTF_16() const { return ___PaintingPrefabTF_16; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_PaintingPrefabTF_16() { return &___PaintingPrefabTF_16; }
	inline void set_PaintingPrefabTF_16(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___PaintingPrefabTF_16 = value;
		Il2CppCodeGenWriteBarrier((&___PaintingPrefabTF_16), value);
	}

	inline static int32_t get_offset_of_PaintingPrefabCollider_17() { return static_cast<int32_t>(offsetof(CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6, ___PaintingPrefabCollider_17)); }
	inline Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * get_PaintingPrefabCollider_17() const { return ___PaintingPrefabCollider_17; }
	inline Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF ** get_address_of_PaintingPrefabCollider_17() { return &___PaintingPrefabCollider_17; }
	inline void set_PaintingPrefabCollider_17(Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * value)
	{
		___PaintingPrefabCollider_17 = value;
		Il2CppCodeGenWriteBarrier((&___PaintingPrefabCollider_17), value);
	}

	inline static int32_t get_offset_of_HoveringObjectCollider_18() { return static_cast<int32_t>(offsetof(CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6, ___HoveringObjectCollider_18)); }
	inline Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * get_HoveringObjectCollider_18() const { return ___HoveringObjectCollider_18; }
	inline Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF ** get_address_of_HoveringObjectCollider_18() { return &___HoveringObjectCollider_18; }
	inline void set_HoveringObjectCollider_18(Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * value)
	{
		___HoveringObjectCollider_18 = value;
		Il2CppCodeGenWriteBarrier((&___HoveringObjectCollider_18), value);
	}

	inline static int32_t get_offset_of_SelectingObjectCollider_19() { return static_cast<int32_t>(offsetof(CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6, ___SelectingObjectCollider_19)); }
	inline Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * get_SelectingObjectCollider_19() const { return ___SelectingObjectCollider_19; }
	inline Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF ** get_address_of_SelectingObjectCollider_19() { return &___SelectingObjectCollider_19; }
	inline void set_SelectingObjectCollider_19(Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * value)
	{
		___SelectingObjectCollider_19 = value;
		Il2CppCodeGenWriteBarrier((&___SelectingObjectCollider_19), value);
	}

	inline static int32_t get_offset_of_MouseChanging_20() { return static_cast<int32_t>(offsetof(CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6, ___MouseChanging_20)); }
	inline bool get_MouseChanging_20() const { return ___MouseChanging_20; }
	inline bool* get_address_of_MouseChanging_20() { return &___MouseChanging_20; }
	inline void set_MouseChanging_20(bool value)
	{
		___MouseChanging_20 = value;
	}

	inline static int32_t get_offset_of_CameraZoomDistance_21() { return static_cast<int32_t>(offsetof(CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6, ___CameraZoomDistance_21)); }
	inline float get_CameraZoomDistance_21() const { return ___CameraZoomDistance_21; }
	inline float* get_address_of_CameraZoomDistance_21() { return &___CameraZoomDistance_21; }
	inline void set_CameraZoomDistance_21(float value)
	{
		___CameraZoomDistance_21 = value;
	}

	inline static int32_t get_offset_of_CameraAimPivot_22() { return static_cast<int32_t>(offsetof(CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6, ___CameraAimPivot_22)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_CameraAimPivot_22() const { return ___CameraAimPivot_22; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_CameraAimPivot_22() { return &___CameraAimPivot_22; }
	inline void set_CameraAimPivot_22(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___CameraAimPivot_22 = value;
	}

	inline static int32_t get_offset_of_CameraStartChangeMousePosition_23() { return static_cast<int32_t>(offsetof(CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6, ___CameraStartChangeMousePosition_23)); }
	inline Nullable_1_t75BFB9848B0C0C6930A6973335B59621D36E6203  get_CameraStartChangeMousePosition_23() const { return ___CameraStartChangeMousePosition_23; }
	inline Nullable_1_t75BFB9848B0C0C6930A6973335B59621D36E6203 * get_address_of_CameraStartChangeMousePosition_23() { return &___CameraStartChangeMousePosition_23; }
	inline void set_CameraStartChangeMousePosition_23(Nullable_1_t75BFB9848B0C0C6930A6973335B59621D36E6203  value)
	{
		___CameraStartChangeMousePosition_23 = value;
	}

	inline static int32_t get_offset_of_CanPaint_24() { return static_cast<int32_t>(offsetof(CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6, ___CanPaint_24)); }
	inline bool get_CanPaint_24() const { return ___CanPaint_24; }
	inline bool* get_address_of_CanPaint_24() { return &___CanPaint_24; }
	inline void set_CanPaint_24(bool value)
	{
		___CanPaint_24 = value;
	}

	inline static int32_t get_offset_of_InsideGround_25() { return static_cast<int32_t>(offsetof(CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6, ___InsideGround_25)); }
	inline bool get_InsideGround_25() const { return ___InsideGround_25; }
	inline bool* get_address_of_InsideGround_25() { return &___InsideGround_25; }
	inline void set_InsideGround_25(bool value)
	{
		___InsideGround_25 = value;
	}

	inline static int32_t get_offset_of_OnObject_26() { return static_cast<int32_t>(offsetof(CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6, ___OnObject_26)); }
	inline bool get_OnObject_26() const { return ___OnObject_26; }
	inline bool* get_address_of_OnObject_26() { return &___OnObject_26; }
	inline void set_OnObject_26(bool value)
	{
		___OnObject_26 = value;
	}

	inline static int32_t get_offset_of_LastMouseMoveTime_27() { return static_cast<int32_t>(offsetof(CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6, ___LastMouseMoveTime_27)); }
	inline float get_LastMouseMoveTime_27() const { return ___LastMouseMoveTime_27; }
	inline float* get_address_of_LastMouseMoveTime_27() { return &___LastMouseMoveTime_27; }
	inline void set_LastMouseMoveTime_27(float value)
	{
		___LastMouseMoveTime_27 = value;
	}

	inline static int32_t get_offset_of_LeftHoldTime_28() { return static_cast<int32_t>(offsetof(CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6, ___LeftHoldTime_28)); }
	inline float get_LeftHoldTime_28() const { return ___LeftHoldTime_28; }
	inline float* get_address_of_LeftHoldTime_28() { return &___LeftHoldTime_28; }
	inline void set_LeftHoldTime_28(float value)
	{
		___LeftHoldTime_28 = value;
	}

	inline static int32_t get_offset_of_LeftHoldTimeLoop_29() { return static_cast<int32_t>(offsetof(CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6, ___LeftHoldTimeLoop_29)); }
	inline float get_LeftHoldTimeLoop_29() const { return ___LeftHoldTimeLoop_29; }
	inline float* get_address_of_LeftHoldTimeLoop_29() { return &___LeftHoldTimeLoop_29; }
	inline void set_LeftHoldTimeLoop_29(float value)
	{
		___LeftHoldTimeLoop_29 = value;
	}

	inline static int32_t get_offset_of_RightHoldTime_30() { return static_cast<int32_t>(offsetof(CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6, ___RightHoldTime_30)); }
	inline float get_RightHoldTime_30() const { return ___RightHoldTime_30; }
	inline float* get_address_of_RightHoldTime_30() { return &___RightHoldTime_30; }
	inline void set_RightHoldTime_30(float value)
	{
		___RightHoldTime_30 = value;
	}

	inline static int32_t get_offset_of_RightHoldTimeLoop_31() { return static_cast<int32_t>(offsetof(CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6, ___RightHoldTimeLoop_31)); }
	inline float get_RightHoldTimeLoop_31() const { return ___RightHoldTimeLoop_31; }
	inline float* get_address_of_RightHoldTimeLoop_31() { return &___RightHoldTimeLoop_31; }
	inline void set_RightHoldTimeLoop_31(float value)
	{
		___RightHoldTimeLoop_31 = value;
	}

	inline static int32_t get_offset_of_MotionDelyTime_32() { return static_cast<int32_t>(offsetof(CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6, ___MotionDelyTime_32)); }
	inline float get_MotionDelyTime_32() const { return ___MotionDelyTime_32; }
	inline float* get_address_of_MotionDelyTime_32() { return &___MotionDelyTime_32; }
	inline void set_MotionDelyTime_32(float value)
	{
		___MotionDelyTime_32 = value;
	}

	inline static int32_t get_offset_of_RayCastPos_33() { return static_cast<int32_t>(offsetof(CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6, ___RayCastPos_33)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_RayCastPos_33() const { return ___RayCastPos_33; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_RayCastPos_33() { return &___RayCastPos_33; }
	inline void set_RayCastPos_33(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___RayCastPos_33 = value;
	}

	inline static int32_t get_offset_of_RayCastNormal_34() { return static_cast<int32_t>(offsetof(CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6, ___RayCastNormal_34)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_RayCastNormal_34() const { return ___RayCastNormal_34; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_RayCastNormal_34() { return &___RayCastNormal_34; }
	inline void set_RayCastNormal_34(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___RayCastNormal_34 = value;
	}

	inline static int32_t get_offset_of_AimCursorPos_35() { return static_cast<int32_t>(offsetof(CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6, ___AimCursorPos_35)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_AimCursorPos_35() const { return ___AimCursorPos_35; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_AimCursorPos_35() { return &___AimCursorPos_35; }
	inline void set_AimCursorPos_35(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___AimCursorPos_35 = value;
	}

	inline static int32_t get_offset_of_AimPrefabPos_36() { return static_cast<int32_t>(offsetof(CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6, ___AimPrefabPos_36)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_AimPrefabPos_36() const { return ___AimPrefabPos_36; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_AimPrefabPos_36() { return &___AimPrefabPos_36; }
	inline void set_AimPrefabPos_36(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___AimPrefabPos_36 = value;
	}

	inline static int32_t get_offset_of_HoverHighLightOffset_37() { return static_cast<int32_t>(offsetof(CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6, ___HoverHighLightOffset_37)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_HoverHighLightOffset_37() const { return ___HoverHighLightOffset_37; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_HoverHighLightOffset_37() { return &___HoverHighLightOffset_37; }
	inline void set_HoverHighLightOffset_37(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___HoverHighLightOffset_37 = value;
	}

	inline static int32_t get_offset_of_SelectHighLightOffset_38() { return static_cast<int32_t>(offsetof(CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6, ___SelectHighLightOffset_38)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_SelectHighLightOffset_38() const { return ___SelectHighLightOffset_38; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_SelectHighLightOffset_38() { return &___SelectHighLightOffset_38; }
	inline void set_SelectHighLightOffset_38(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___SelectHighLightOffset_38 = value;
	}

	inline static int32_t get_offset_of_PaintingRotation_39() { return static_cast<int32_t>(offsetof(CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6, ___PaintingRotation_39)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_PaintingRotation_39() const { return ___PaintingRotation_39; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_PaintingRotation_39() { return &___PaintingRotation_39; }
	inline void set_PaintingRotation_39(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___PaintingRotation_39 = value;
	}

	inline static int32_t get_offset_of_AimPrefabRot_40() { return static_cast<int32_t>(offsetof(CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6, ___AimPrefabRot_40)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_AimPrefabRot_40() const { return ___AimPrefabRot_40; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_AimPrefabRot_40() { return &___AimPrefabRot_40; }
	inline void set_AimPrefabRot_40(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___AimPrefabRot_40 = value;
	}

	inline static int32_t get_offset_of_AimCursorRot_41() { return static_cast<int32_t>(offsetof(CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6, ___AimCursorRot_41)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_AimCursorRot_41() const { return ___AimCursorRot_41; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_AimCursorRot_41() { return &___AimCursorRot_41; }
	inline void set_AimCursorRot_41(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___AimCursorRot_41 = value;
	}

	inline static int32_t get_offset_of_UndoList_42() { return static_cast<int32_t>(offsetof(CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6, ___UndoList_42)); }
	inline List_1_tC9F26360E86F3EED335ADB7E6838E9C069B6039F * get_UndoList_42() const { return ___UndoList_42; }
	inline List_1_tC9F26360E86F3EED335ADB7E6838E9C069B6039F ** get_address_of_UndoList_42() { return &___UndoList_42; }
	inline void set_UndoList_42(List_1_tC9F26360E86F3EED335ADB7E6838E9C069B6039F * value)
	{
		___UndoList_42 = value;
		Il2CppCodeGenWriteBarrier((&___UndoList_42), value);
	}

	inline static int32_t get_offset_of_RedoList_43() { return static_cast<int32_t>(offsetof(CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6, ___RedoList_43)); }
	inline List_1_tC9F26360E86F3EED335ADB7E6838E9C069B6039F * get_RedoList_43() const { return ___RedoList_43; }
	inline List_1_tC9F26360E86F3EED335ADB7E6838E9C069B6039F ** get_address_of_RedoList_43() { return &___RedoList_43; }
	inline void set_RedoList_43(List_1_tC9F26360E86F3EED335ADB7E6838E9C069B6039F * value)
	{
		___RedoList_43 = value;
		Il2CppCodeGenWriteBarrier((&___RedoList_43), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CYANLEVELEDITOR_T22E46946CA890622F3B9069EF816C703F7C664B6_H
#ifndef SETARABICTEXTEXAMPLE_T44C12A1B401FF63F8693928EA65F21CDE39F71B6_H
#define SETARABICTEXTEXAMPLE_T44C12A1B401FF63F8693928EA65F21CDE39F71B6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SetArabicTextExample
struct  SetArabicTextExample_t44C12A1B401FF63F8693928EA65F21CDE39F71B6  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String SetArabicTextExample::text
	String_t* ___text_4;

public:
	inline static int32_t get_offset_of_text_4() { return static_cast<int32_t>(offsetof(SetArabicTextExample_t44C12A1B401FF63F8693928EA65F21CDE39F71B6, ___text_4)); }
	inline String_t* get_text_4() const { return ___text_4; }
	inline String_t** get_address_of_text_4() { return &___text_4; }
	inline void set_text_4(String_t* value)
	{
		___text_4 = value;
		Il2CppCodeGenWriteBarrier((&___text_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETARABICTEXTEXAMPLE_T44C12A1B401FF63F8693928EA65F21CDE39F71B6_H
#ifndef STARFXCONTROLLER_T43DD9475A7C574AD503BCEB33A4C3CBD985AD075_H
#define STARFXCONTROLLER_T43DD9475A7C574AD503BCEB33A4C3CBD985AD075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StarFxController
struct  StarFxController_t43DD9475A7C574AD503BCEB33A4C3CBD985AD075  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject[] StarFxController::starFX
	GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* ___starFX_4;
	// System.Int32 StarFxController::ea
	int32_t ___ea_5;
	// System.Int32 StarFxController::currentEa
	int32_t ___currentEa_6;
	// System.Single StarFxController::delay
	float ___delay_7;
	// System.Single StarFxController::currentDelay
	float ___currentDelay_8;
	// System.Boolean StarFxController::isEnd
	bool ___isEnd_9;
	// System.Int32 StarFxController::idStar
	int32_t ___idStar_10;

public:
	inline static int32_t get_offset_of_starFX_4() { return static_cast<int32_t>(offsetof(StarFxController_t43DD9475A7C574AD503BCEB33A4C3CBD985AD075, ___starFX_4)); }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* get_starFX_4() const { return ___starFX_4; }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520** get_address_of_starFX_4() { return &___starFX_4; }
	inline void set_starFX_4(GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* value)
	{
		___starFX_4 = value;
		Il2CppCodeGenWriteBarrier((&___starFX_4), value);
	}

	inline static int32_t get_offset_of_ea_5() { return static_cast<int32_t>(offsetof(StarFxController_t43DD9475A7C574AD503BCEB33A4C3CBD985AD075, ___ea_5)); }
	inline int32_t get_ea_5() const { return ___ea_5; }
	inline int32_t* get_address_of_ea_5() { return &___ea_5; }
	inline void set_ea_5(int32_t value)
	{
		___ea_5 = value;
	}

	inline static int32_t get_offset_of_currentEa_6() { return static_cast<int32_t>(offsetof(StarFxController_t43DD9475A7C574AD503BCEB33A4C3CBD985AD075, ___currentEa_6)); }
	inline int32_t get_currentEa_6() const { return ___currentEa_6; }
	inline int32_t* get_address_of_currentEa_6() { return &___currentEa_6; }
	inline void set_currentEa_6(int32_t value)
	{
		___currentEa_6 = value;
	}

	inline static int32_t get_offset_of_delay_7() { return static_cast<int32_t>(offsetof(StarFxController_t43DD9475A7C574AD503BCEB33A4C3CBD985AD075, ___delay_7)); }
	inline float get_delay_7() const { return ___delay_7; }
	inline float* get_address_of_delay_7() { return &___delay_7; }
	inline void set_delay_7(float value)
	{
		___delay_7 = value;
	}

	inline static int32_t get_offset_of_currentDelay_8() { return static_cast<int32_t>(offsetof(StarFxController_t43DD9475A7C574AD503BCEB33A4C3CBD985AD075, ___currentDelay_8)); }
	inline float get_currentDelay_8() const { return ___currentDelay_8; }
	inline float* get_address_of_currentDelay_8() { return &___currentDelay_8; }
	inline void set_currentDelay_8(float value)
	{
		___currentDelay_8 = value;
	}

	inline static int32_t get_offset_of_isEnd_9() { return static_cast<int32_t>(offsetof(StarFxController_t43DD9475A7C574AD503BCEB33A4C3CBD985AD075, ___isEnd_9)); }
	inline bool get_isEnd_9() const { return ___isEnd_9; }
	inline bool* get_address_of_isEnd_9() { return &___isEnd_9; }
	inline void set_isEnd_9(bool value)
	{
		___isEnd_9 = value;
	}

	inline static int32_t get_offset_of_idStar_10() { return static_cast<int32_t>(offsetof(StarFxController_t43DD9475A7C574AD503BCEB33A4C3CBD985AD075, ___idStar_10)); }
	inline int32_t get_idStar_10() const { return ___idStar_10; }
	inline int32_t* get_address_of_idStar_10() { return &___idStar_10; }
	inline void set_idStar_10(int32_t value)
	{
		___idStar_10 = value;
	}
};

struct StarFxController_t43DD9475A7C574AD503BCEB33A4C3CBD985AD075_StaticFields
{
public:
	// StarFxController StarFxController::myStarFxController
	StarFxController_t43DD9475A7C574AD503BCEB33A4C3CBD985AD075 * ___myStarFxController_11;

public:
	inline static int32_t get_offset_of_myStarFxController_11() { return static_cast<int32_t>(offsetof(StarFxController_t43DD9475A7C574AD503BCEB33A4C3CBD985AD075_StaticFields, ___myStarFxController_11)); }
	inline StarFxController_t43DD9475A7C574AD503BCEB33A4C3CBD985AD075 * get_myStarFxController_11() const { return ___myStarFxController_11; }
	inline StarFxController_t43DD9475A7C574AD503BCEB33A4C3CBD985AD075 ** get_address_of_myStarFxController_11() { return &___myStarFxController_11; }
	inline void set_myStarFxController_11(StarFxController_t43DD9475A7C574AD503BCEB33A4C3CBD985AD075 * value)
	{
		___myStarFxController_11 = value;
		Il2CppCodeGenWriteBarrier((&___myStarFxController_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STARFXCONTROLLER_T43DD9475A7C574AD503BCEB33A4C3CBD985AD075_H
#ifndef UNITYCONTEXT_T0D8D140543A4C26F9405C7E7E3CE23A0D43B0B6A_H
#define UNITYCONTEXT_T0D8D140543A4C26F9405C7E7E3CE23A0D43B0B6A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityContext
struct  UnityContext_t0D8D140543A4C26F9405C7E7E3CE23A0D43B0B6A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYCONTEXT_T0D8D140543A4C26F9405C7E7E3CE23A0D43B0B6A_H
#ifndef VFXCONTROLLER_TBDD97B953055545A8817FEE3FF2C394707EE7547_H
#define VFXCONTROLLER_TBDD97B953055545A8817FEE3FF2C394707EE7547_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// vfxController
struct  vfxController_tBDD97B953055545A8817FEE3FF2C394707EE7547  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject[] vfxController::starFx01Prefabs
	GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* ___starFx01Prefabs_4;
	// UnityEngine.GameObject[] vfxController::starFx02Prefabs
	GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* ___starFx02Prefabs_5;
	// UnityEngine.GameObject[] vfxController::starFx03Prefabs
	GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* ___starFx03Prefabs_6;
	// UnityEngine.GameObject[] vfxController::starFx04Prefabs
	GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* ___starFx04Prefabs_7;
	// UnityEngine.GameObject[] vfxController::starFx05Prefabs
	GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* ___starFx05Prefabs_8;
	// UnityEngine.GameObject[] vfxController::DesStarFxObjs
	GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* ___DesStarFxObjs_9;
	// UnityEngine.GameObject[] vfxController::bgFxPrefabs
	GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* ___bgFxPrefabs_10;
	// System.Int32 vfxController::currentStarImage
	int32_t ___currentStarImage_11;
	// System.Int32 vfxController::currentStarFx
	int32_t ___currentStarFx_12;
	// System.Int32 vfxController::currentLevel
	int32_t ___currentLevel_13;
	// System.Int32 vfxController::currentBgFx
	int32_t ___currentBgFx_14;

public:
	inline static int32_t get_offset_of_starFx01Prefabs_4() { return static_cast<int32_t>(offsetof(vfxController_tBDD97B953055545A8817FEE3FF2C394707EE7547, ___starFx01Prefabs_4)); }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* get_starFx01Prefabs_4() const { return ___starFx01Prefabs_4; }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520** get_address_of_starFx01Prefabs_4() { return &___starFx01Prefabs_4; }
	inline void set_starFx01Prefabs_4(GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* value)
	{
		___starFx01Prefabs_4 = value;
		Il2CppCodeGenWriteBarrier((&___starFx01Prefabs_4), value);
	}

	inline static int32_t get_offset_of_starFx02Prefabs_5() { return static_cast<int32_t>(offsetof(vfxController_tBDD97B953055545A8817FEE3FF2C394707EE7547, ___starFx02Prefabs_5)); }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* get_starFx02Prefabs_5() const { return ___starFx02Prefabs_5; }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520** get_address_of_starFx02Prefabs_5() { return &___starFx02Prefabs_5; }
	inline void set_starFx02Prefabs_5(GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* value)
	{
		___starFx02Prefabs_5 = value;
		Il2CppCodeGenWriteBarrier((&___starFx02Prefabs_5), value);
	}

	inline static int32_t get_offset_of_starFx03Prefabs_6() { return static_cast<int32_t>(offsetof(vfxController_tBDD97B953055545A8817FEE3FF2C394707EE7547, ___starFx03Prefabs_6)); }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* get_starFx03Prefabs_6() const { return ___starFx03Prefabs_6; }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520** get_address_of_starFx03Prefabs_6() { return &___starFx03Prefabs_6; }
	inline void set_starFx03Prefabs_6(GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* value)
	{
		___starFx03Prefabs_6 = value;
		Il2CppCodeGenWriteBarrier((&___starFx03Prefabs_6), value);
	}

	inline static int32_t get_offset_of_starFx04Prefabs_7() { return static_cast<int32_t>(offsetof(vfxController_tBDD97B953055545A8817FEE3FF2C394707EE7547, ___starFx04Prefabs_7)); }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* get_starFx04Prefabs_7() const { return ___starFx04Prefabs_7; }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520** get_address_of_starFx04Prefabs_7() { return &___starFx04Prefabs_7; }
	inline void set_starFx04Prefabs_7(GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* value)
	{
		___starFx04Prefabs_7 = value;
		Il2CppCodeGenWriteBarrier((&___starFx04Prefabs_7), value);
	}

	inline static int32_t get_offset_of_starFx05Prefabs_8() { return static_cast<int32_t>(offsetof(vfxController_tBDD97B953055545A8817FEE3FF2C394707EE7547, ___starFx05Prefabs_8)); }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* get_starFx05Prefabs_8() const { return ___starFx05Prefabs_8; }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520** get_address_of_starFx05Prefabs_8() { return &___starFx05Prefabs_8; }
	inline void set_starFx05Prefabs_8(GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* value)
	{
		___starFx05Prefabs_8 = value;
		Il2CppCodeGenWriteBarrier((&___starFx05Prefabs_8), value);
	}

	inline static int32_t get_offset_of_DesStarFxObjs_9() { return static_cast<int32_t>(offsetof(vfxController_tBDD97B953055545A8817FEE3FF2C394707EE7547, ___DesStarFxObjs_9)); }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* get_DesStarFxObjs_9() const { return ___DesStarFxObjs_9; }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520** get_address_of_DesStarFxObjs_9() { return &___DesStarFxObjs_9; }
	inline void set_DesStarFxObjs_9(GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* value)
	{
		___DesStarFxObjs_9 = value;
		Il2CppCodeGenWriteBarrier((&___DesStarFxObjs_9), value);
	}

	inline static int32_t get_offset_of_bgFxPrefabs_10() { return static_cast<int32_t>(offsetof(vfxController_tBDD97B953055545A8817FEE3FF2C394707EE7547, ___bgFxPrefabs_10)); }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* get_bgFxPrefabs_10() const { return ___bgFxPrefabs_10; }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520** get_address_of_bgFxPrefabs_10() { return &___bgFxPrefabs_10; }
	inline void set_bgFxPrefabs_10(GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* value)
	{
		___bgFxPrefabs_10 = value;
		Il2CppCodeGenWriteBarrier((&___bgFxPrefabs_10), value);
	}

	inline static int32_t get_offset_of_currentStarImage_11() { return static_cast<int32_t>(offsetof(vfxController_tBDD97B953055545A8817FEE3FF2C394707EE7547, ___currentStarImage_11)); }
	inline int32_t get_currentStarImage_11() const { return ___currentStarImage_11; }
	inline int32_t* get_address_of_currentStarImage_11() { return &___currentStarImage_11; }
	inline void set_currentStarImage_11(int32_t value)
	{
		___currentStarImage_11 = value;
	}

	inline static int32_t get_offset_of_currentStarFx_12() { return static_cast<int32_t>(offsetof(vfxController_tBDD97B953055545A8817FEE3FF2C394707EE7547, ___currentStarFx_12)); }
	inline int32_t get_currentStarFx_12() const { return ___currentStarFx_12; }
	inline int32_t* get_address_of_currentStarFx_12() { return &___currentStarFx_12; }
	inline void set_currentStarFx_12(int32_t value)
	{
		___currentStarFx_12 = value;
	}

	inline static int32_t get_offset_of_currentLevel_13() { return static_cast<int32_t>(offsetof(vfxController_tBDD97B953055545A8817FEE3FF2C394707EE7547, ___currentLevel_13)); }
	inline int32_t get_currentLevel_13() const { return ___currentLevel_13; }
	inline int32_t* get_address_of_currentLevel_13() { return &___currentLevel_13; }
	inline void set_currentLevel_13(int32_t value)
	{
		___currentLevel_13 = value;
	}

	inline static int32_t get_offset_of_currentBgFx_14() { return static_cast<int32_t>(offsetof(vfxController_tBDD97B953055545A8817FEE3FF2C394707EE7547, ___currentBgFx_14)); }
	inline int32_t get_currentBgFx_14() const { return ___currentBgFx_14; }
	inline int32_t* get_address_of_currentBgFx_14() { return &___currentBgFx_14; }
	inline void set_currentBgFx_14(int32_t value)
	{
		___currentBgFx_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VFXCONTROLLER_TBDD97B953055545A8817FEE3FF2C394707EE7547_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6900 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6901 = { sizeof (PlatformBase_tCBFDE4E2C964A9DC16353A32B6F7974D5107187B), -1, sizeof(PlatformBase_tCBFDE4E2C964A9DC16353A32B6F7974D5107187B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6901[16] = 
{
	PlatformBase_tCBFDE4E2C964A9DC16353A32B6F7974D5107187B_StaticFields::get_offset_of_PLAYER_PREF_AUTHTOKEN_KEY_4(),
	PlatformBase_tCBFDE4E2C964A9DC16353A32B6F7974D5107187B_StaticFields::get_offset_of_PLAYER_PREF_USERID_KEY_5(),
	PlatformBase_tCBFDE4E2C964A9DC16353A32B6F7974D5107187B_StaticFields::get_offset_of_PLAYER_PREF_DEVICEID_KEY_6(),
	PlatformBase_tCBFDE4E2C964A9DC16353A32B6F7974D5107187B::get_offset_of__actions_7(),
	PlatformBase_tCBFDE4E2C964A9DC16353A32B6F7974D5107187B::get_offset_of__currentActions_8(),
	PlatformBase_tCBFDE4E2C964A9DC16353A32B6F7974D5107187B::get_offset_of__allowQuitting_9(),
	PlatformBase_tCBFDE4E2C964A9DC16353A32B6F7974D5107187B::get_offset_of_U3CDeviceNameU3Ek__BackingField_10(),
	PlatformBase_tCBFDE4E2C964A9DC16353A32B6F7974D5107187B::get_offset_of_U3CDeviceTypeU3Ek__BackingField_11(),
	PlatformBase_tCBFDE4E2C964A9DC16353A32B6F7974D5107187B::get_offset_of_U3CDeviceStatsU3Ek__BackingField_12(),
	PlatformBase_tCBFDE4E2C964A9DC16353A32B6F7974D5107187B::get_offset_of_U3CDeviceIdU3Ek__BackingField_13(),
	PlatformBase_tCBFDE4E2C964A9DC16353A32B6F7974D5107187B::get_offset_of_U3CPlatformU3Ek__BackingField_14(),
	PlatformBase_tCBFDE4E2C964A9DC16353A32B6F7974D5107187B::get_offset_of_U3CExtraDebugU3Ek__BackingField_15(),
	PlatformBase_tCBFDE4E2C964A9DC16353A32B6F7974D5107187B::get_offset_of_U3CPersistentDataPathU3Ek__BackingField_16(),
	PlatformBase_tCBFDE4E2C964A9DC16353A32B6F7974D5107187B::get_offset_of_m_authToken_17(),
	PlatformBase_tCBFDE4E2C964A9DC16353A32B6F7974D5107187B::get_offset_of_m_userId_18(),
	PlatformBase_tCBFDE4E2C964A9DC16353A32B6F7974D5107187B::get_offset_of_U3CExceptionReporterU3Ek__BackingField_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6902 = { sizeof (U3CDelayedQuitU3Ed__11_t52EAB0DA42199004026ED4F53BF31E73B223B79B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6902[3] = 
{
	U3CDelayedQuitU3Ed__11_t52EAB0DA42199004026ED4F53BF31E73B223B79B::get_offset_of_U3CU3E1__state_0(),
	U3CDelayedQuitU3Ed__11_t52EAB0DA42199004026ED4F53BF31E73B223B79B::get_offset_of_U3CU3E2__current_1(),
	U3CDelayedQuitU3Ed__11_t52EAB0DA42199004026ED4F53BF31E73B223B79B::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6903 = { sizeof (U3CU3Ec__DisplayClass52_0_t41E4EEB3622B04A2519695B52E97DA725AC6AA4D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6903[1] = 
{
	U3CU3Ec__DisplayClass52_0_t41E4EEB3622B04A2519695B52E97DA725AC6AA4D::get_offset_of_message_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6904 = { sizeof (TimerController_t0671C34BEC0EC688FD4BF412F0683EFCC40F007D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6904[2] = 
{
	TimerController_t0671C34BEC0EC688FD4BF412F0683EFCC40F007D::get_offset_of_timeOfLastUpdate_0(),
	TimerController_t0671C34BEC0EC688FD4BF412F0683EFCC40F007D::get_offset_of_timers_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6905 = { sizeof (UnityTimer_t5A0BA21B61DF888D9D7E26A10716DB28FB9D90C5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6905[5] = 
{
	UnityTimer_t5A0BA21B61DF888D9D7E26A10716DB28FB9D90C5::get_offset_of_callback_0(),
	UnityTimer_t5A0BA21B61DF888D9D7E26A10716DB28FB9D90C5::get_offset_of_interval_1(),
	UnityTimer_t5A0BA21B61DF888D9D7E26A10716DB28FB9D90C5::get_offset_of_elapsedTicks_2(),
	UnityTimer_t5A0BA21B61DF888D9D7E26A10716DB28FB9D90C5::get_offset_of_running_3(),
	UnityTimer_t5A0BA21B61DF888D9D7E26A10716DB28FB9D90C5::get_offset_of_controller_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6906 = { sizeof (WebSocketController_t058EEAFBC2A602648C67D685CB9310E3DE4DCB9E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6906[3] = 
{
	WebSocketController_t058EEAFBC2A602648C67D685CB9310E3DE4DCB9E::get_offset_of_U3CGSNameU3Ek__BackingField_4(),
	WebSocketController_t058EEAFBC2A602648C67D685CB9310E3DE4DCB9E::get_offset_of_webSockets_5(),
	WebSocketController_t058EEAFBC2A602648C67D685CB9310E3DE4DCB9E::get_offset_of_websocketCollectionModified_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6907 = { sizeof (Compile_t1D82CE8E8A091D34E18CAC282E75C98B81B85B89), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6908 = { sizeof (MapData_tA7A939C073DE4163D917C3AD9BA8111C169E71DF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6908[1] = 
{
	MapData_tA7A939C073DE4163D917C3AD9BA8111C169E71DF::get_offset_of_Items_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6909 = { sizeof (MapItem_t3958670AC2FB278DB1978A82F511B45DB681D6B5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6909[4] = 
{
	MapItem_t3958670AC2FB278DB1978A82F511B45DB681D6B5::get_offset_of_i_0(),
	MapItem_t3958670AC2FB278DB1978A82F511B45DB681D6B5::get_offset_of_j_1(),
	MapItem_t3958670AC2FB278DB1978A82F511B45DB681D6B5::get_offset_of_p_2(),
	MapItem_t3958670AC2FB278DB1978A82F511B45DB681D6B5::get_offset_of_q_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6910 = { sizeof (CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6910[40] = 
{
	CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6::get_offset_of_ActionSetting_4(),
	CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6::get_offset_of_CursorSetting_5(),
	CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6::get_offset_of_CameraSetting_6(),
	CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6::get_offset_of_GridSetting_7(),
	CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6::get_offset_of_Components_8(),
	CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6::get_offset_of_Messages_9(),
	CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6::get_offset_of_PrefabPools_10(),
	0,
	0,
	CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6::get_offset_of_ShowingPoolIndex_13(),
	CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6::get_offset_of_ShowingPrefabIndex_14(),
	CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6::get_offset_of_ShowingPrefabSelfPoolIndex_15(),
	CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6::get_offset_of_PaintingPrefabTF_16(),
	CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6::get_offset_of_PaintingPrefabCollider_17(),
	CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6::get_offset_of_HoveringObjectCollider_18(),
	CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6::get_offset_of_SelectingObjectCollider_19(),
	CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6::get_offset_of_MouseChanging_20(),
	CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6::get_offset_of_CameraZoomDistance_21(),
	CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6::get_offset_of_CameraAimPivot_22(),
	CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6::get_offset_of_CameraStartChangeMousePosition_23(),
	CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6::get_offset_of_CanPaint_24(),
	CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6::get_offset_of_InsideGround_25(),
	CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6::get_offset_of_OnObject_26(),
	CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6::get_offset_of_LastMouseMoveTime_27(),
	CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6::get_offset_of_LeftHoldTime_28(),
	CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6::get_offset_of_LeftHoldTimeLoop_29(),
	CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6::get_offset_of_RightHoldTime_30(),
	CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6::get_offset_of_RightHoldTimeLoop_31(),
	CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6::get_offset_of_MotionDelyTime_32(),
	CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6::get_offset_of_RayCastPos_33(),
	CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6::get_offset_of_RayCastNormal_34(),
	CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6::get_offset_of_AimCursorPos_35(),
	CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6::get_offset_of_AimPrefabPos_36(),
	CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6::get_offset_of_HoverHighLightOffset_37(),
	CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6::get_offset_of_SelectHighLightOffset_38(),
	CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6::get_offset_of_PaintingRotation_39(),
	CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6::get_offset_of_AimPrefabRot_40(),
	CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6::get_offset_of_AimCursorRot_41(),
	CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6::get_offset_of_UndoList_42(),
	CyanLevelEditor_t22E46946CA890622F3B9069EF816C703F7C664B6::get_offset_of_RedoList_43(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6911 = { sizeof (U3CU3Ec__DisplayClass85_0_t0F545B2D92119D6E0F01D781FDE36A27347E7144), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6911[2] = 
{
	U3CU3Ec__DisplayClass85_0_t0F545B2D92119D6E0F01D781FDE36A27347E7144::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass85_0_t0F545B2D92119D6E0F01D781FDE36A27347E7144::get_offset_of_poolIndex_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6912 = { sizeof (U3CU3Ec__DisplayClass86_0_t68FAED02A98BE4BDEB410ED9C5626971AB316922), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6912[3] = 
{
	U3CU3Ec__DisplayClass86_0_t68FAED02A98BE4BDEB410ED9C5626971AB316922::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass86_0_t68FAED02A98BE4BDEB410ED9C5626971AB316922::get_offset_of_poolIndex_1(),
	U3CU3Ec__DisplayClass86_0_t68FAED02A98BE4BDEB410ED9C5626971AB316922::get_offset_of_prefabIndex_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6913 = { sizeof (CyanActionSetting_t52F579D228577FDDE87DC71F2C2DAB5B6CB7F46E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6913[18] = 
{
	CyanActionSetting_t52F579D228577FDDE87DC71F2C2DAB5B6CB7F46E::get_offset_of_MaxUndoNum_0(),
	CyanActionSetting_t52F579D228577FDDE87DC71F2C2DAB5B6CB7F46E::get_offset_of_MouseHoldTime_1(),
	CyanActionSetting_t52F579D228577FDDE87DC71F2C2DAB5B6CB7F46E::get_offset_of_MotionDely_2(),
	CyanActionSetting_t52F579D228577FDDE87DC71F2C2DAB5B6CB7F46E::get_offset_of_DeleteMode_3(),
	CyanActionSetting_t52F579D228577FDDE87DC71F2C2DAB5B6CB7F46E::get_offset_of_PaintOnWall_4(),
	CyanActionSetting_t52F579D228577FDDE87DC71F2C2DAB5B6CB7F46E::get_offset_of_PaintOnBottom_5(),
	CyanActionSetting_t52F579D228577FDDE87DC71F2C2DAB5B6CB7F46E::get_offset_of_PaintOutsideGround_6(),
	CyanActionSetting_t52F579D228577FDDE87DC71F2C2DAB5B6CB7F46E::get_offset_of_PaintOnObject_7(),
	CyanActionSetting_t52F579D228577FDDE87DC71F2C2DAB5B6CB7F46E::get_offset_of_PaintObjectMouseButton_8(),
	CyanActionSetting_t52F579D228577FDDE87DC71F2C2DAB5B6CB7F46E::get_offset_of_SelectObjectMouseButton_9(),
	CyanActionSetting_t52F579D228577FDDE87DC71F2C2DAB5B6CB7F46E::get_offset_of_RotateObjectMouseButton_10(),
	CyanActionSetting_t52F579D228577FDDE87DC71F2C2DAB5B6CB7F46E::get_offset_of_DeleteObjectMouseButton_11(),
	CyanActionSetting_t52F579D228577FDDE87DC71F2C2DAB5B6CB7F46E::get_offset_of_CancelPaintingMouseButton_12(),
	CyanActionSetting_t52F579D228577FDDE87DC71F2C2DAB5B6CB7F46E::get_offset_of_PaintObjectKey_13(),
	CyanActionSetting_t52F579D228577FDDE87DC71F2C2DAB5B6CB7F46E::get_offset_of_SelectObjectKey_14(),
	CyanActionSetting_t52F579D228577FDDE87DC71F2C2DAB5B6CB7F46E::get_offset_of_RotateObjectKey_15(),
	CyanActionSetting_t52F579D228577FDDE87DC71F2C2DAB5B6CB7F46E::get_offset_of_DeleteObjectKey_16(),
	CyanActionSetting_t52F579D228577FDDE87DC71F2C2DAB5B6CB7F46E::get_offset_of_CancelPaintingKey_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6914 = { sizeof (MouseButton_tA407D242A3943936B7253771B2FB28D7E708AE74)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6914[6] = 
{
	MouseButton_tA407D242A3943936B7253771B2FB28D7E708AE74::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6915 = { sizeof (CyanDeleteMode_t7C98F1C271A4F57C4A41C04CF787622FA2B49496)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6915[4] = 
{
	CyanDeleteMode_t7C98F1C271A4F57C4A41C04CF787622FA2B49496::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6916 = { sizeof (CyanCursorSetting_t80DBA731373075E7ACA488DBB3733A88089199D3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6916[7] = 
{
	CyanCursorSetting_t80DBA731373075E7ACA488DBB3733A88089199D3::get_offset_of_ShowCursor_0(),
	CyanCursorSetting_t80DBA731373075E7ACA488DBB3733A88089199D3::get_offset_of_ShowPaitingPrefab_1(),
	CyanCursorSetting_t80DBA731373075E7ACA488DBB3733A88089199D3::get_offset_of_HighLightHoveringObject_2(),
	CyanCursorSetting_t80DBA731373075E7ACA488DBB3733A88089199D3::get_offset_of_HighLightSelectingObject_3(),
	CyanCursorSetting_t80DBA731373075E7ACA488DBB3733A88089199D3::get_offset_of_CrosswiseBySurface_4(),
	CyanCursorSetting_t80DBA731373075E7ACA488DBB3733A88089199D3::get_offset_of_MotionLerpRate_5(),
	CyanCursorSetting_t80DBA731373075E7ACA488DBB3733A88089199D3::get_offset_of_CursorSizeChangeBy_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6917 = { sizeof (CursorSizeChanging_t9F1BD81C7E026F63F20F2954C6B177ADE60FB59B)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6917[4] = 
{
	CursorSizeChanging_t9F1BD81C7E026F63F20F2954C6B177ADE60FB59B::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6918 = { sizeof (CyanCameraSetting_t206CA210D2F54015079229DBC666790D10EB7237), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6918[25] = 
{
	CyanCameraSetting_t206CA210D2F54015079229DBC666790D10EB7237::get_offset_of_DragThreshold_0(),
	CyanCameraSetting_t206CA210D2F54015079229DBC666790D10EB7237::get_offset_of_MoveSpeed_1(),
	CyanCameraSetting_t206CA210D2F54015079229DBC666790D10EB7237::get_offset_of_ClampXZInsideGround_2(),
	CyanCameraSetting_t206CA210D2F54015079229DBC666790D10EB7237::get_offset_of_ClampYInsideGround_3(),
	CyanCameraSetting_t206CA210D2F54015079229DBC666790D10EB7237::get_offset_of_RotateSpeed_4(),
	CyanCameraSetting_t206CA210D2F54015079229DBC666790D10EB7237::get_offset_of_AngelYMin_5(),
	CyanCameraSetting_t206CA210D2F54015079229DBC666790D10EB7237::get_offset_of_AngelYMax_6(),
	CyanCameraSetting_t206CA210D2F54015079229DBC666790D10EB7237::get_offset_of_AngelXMin_7(),
	CyanCameraSetting_t206CA210D2F54015079229DBC666790D10EB7237::get_offset_of_AngelXMax_8(),
	CyanCameraSetting_t206CA210D2F54015079229DBC666790D10EB7237::get_offset_of_DragZoomSpeed_9(),
	CyanCameraSetting_t206CA210D2F54015079229DBC666790D10EB7237::get_offset_of_WheelZoomSpeed_10(),
	CyanCameraSetting_t206CA210D2F54015079229DBC666790D10EB7237::get_offset_of_MinZoomDistance_11(),
	CyanCameraSetting_t206CA210D2F54015079229DBC666790D10EB7237::get_offset_of_MaxZoomDistance_12(),
	CyanCameraSetting_t206CA210D2F54015079229DBC666790D10EB7237::get_offset_of_MoveMouseA_13(),
	CyanCameraSetting_t206CA210D2F54015079229DBC666790D10EB7237::get_offset_of_RotateMouseA_14(),
	CyanCameraSetting_t206CA210D2F54015079229DBC666790D10EB7237::get_offset_of_ZoomMouseA_15(),
	CyanCameraSetting_t206CA210D2F54015079229DBC666790D10EB7237::get_offset_of_MoveKeyA_16(),
	CyanCameraSetting_t206CA210D2F54015079229DBC666790D10EB7237::get_offset_of_RotateKeyA_17(),
	CyanCameraSetting_t206CA210D2F54015079229DBC666790D10EB7237::get_offset_of_ZoomKeyA_18(),
	CyanCameraSetting_t206CA210D2F54015079229DBC666790D10EB7237::get_offset_of_MoveMouseB_19(),
	CyanCameraSetting_t206CA210D2F54015079229DBC666790D10EB7237::get_offset_of_RotateMouseB_20(),
	CyanCameraSetting_t206CA210D2F54015079229DBC666790D10EB7237::get_offset_of_ZoomMouseB_21(),
	CyanCameraSetting_t206CA210D2F54015079229DBC666790D10EB7237::get_offset_of_MoveKeyB_22(),
	CyanCameraSetting_t206CA210D2F54015079229DBC666790D10EB7237::get_offset_of_RotateKeyB_23(),
	CyanCameraSetting_t206CA210D2F54015079229DBC666790D10EB7237::get_offset_of_ZoomKeyB_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6919 = { sizeof (CameraActionMode_t17BCC1E677B2395A4CB8F310C078216D9559CAFB)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6919[5] = 
{
	CameraActionMode_t17BCC1E677B2395A4CB8F310C078216D9559CAFB::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6920 = { sizeof (CyanGridSetting_tF2D64053897D26131BDCF7A3F2675BD2161BE8CF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6920[3] = 
{
	CyanGridSetting_tF2D64053897D26131BDCF7A3F2675BD2161BE8CF::get_offset_of_PrefabSnapToGrid_0(),
	CyanGridSetting_tF2D64053897D26131BDCF7A3F2675BD2161BE8CF::get_offset_of_CursorSnapToGrid_1(),
	CyanGridSetting_tF2D64053897D26131BDCF7A3F2675BD2161BE8CF::get_offset_of_GridSize_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6921 = { sizeof (CyanComponent_tC7880F9103B559C4E6EB97745CE3A56781CDB3BC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6921[10] = 
{
	CyanComponent_tC7880F9103B559C4E6EB97745CE3A56781CDB3BC::get_offset_of_MainCamera_0(),
	CyanComponent_tC7880F9103B559C4E6EB97745CE3A56781CDB3BC::get_offset_of_DefaultGround_1(),
	CyanComponent_tC7880F9103B559C4E6EB97745CE3A56781CDB3BC::get_offset_of_Cursor_2(),
	CyanComponent_tC7880F9103B559C4E6EB97745CE3A56781CDB3BC::get_offset_of_HoveringHighLight_3(),
	CyanComponent_tC7880F9103B559C4E6EB97745CE3A56781CDB3BC::get_offset_of_SelectingHighLight_4(),
	CyanComponent_tC7880F9103B559C4E6EB97745CE3A56781CDB3BC::get_offset_of_ObjectContainer_5(),
	CyanComponent_tC7880F9103B559C4E6EB97745CE3A56781CDB3BC::get_offset_of_PoolUIContainer_6(),
	CyanComponent_tC7880F9103B559C4E6EB97745CE3A56781CDB3BC::get_offset_of_PrefabUIContainer_7(),
	CyanComponent_tC7880F9103B559C4E6EB97745CE3A56781CDB3BC::get_offset_of_PoolItemTemplate_8(),
	CyanComponent_tC7880F9103B559C4E6EB97745CE3A56781CDB3BC::get_offset_of_PrefabItemTemplate_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6922 = { sizeof (CyanObjectPool_tEBC761771A66414B6FC6ACD1722E361442B9E20B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6922[3] = 
{
	CyanObjectPool_tEBC761771A66414B6FC6ACD1722E361442B9E20B::get_offset_of_Name_0(),
	CyanObjectPool_tEBC761771A66414B6FC6ACD1722E361442B9E20B::get_offset_of_Prefabs_1(),
	CyanObjectPool_tEBC761771A66414B6FC6ACD1722E361442B9E20B::get_offset_of_PoolIcon_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6923 = { sizeof (Prefab_t9A8682B18F1324D8885AB33089A02BF5C742AA1B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6923[3] = 
{
	Prefab_t9A8682B18F1324D8885AB33089A02BF5C742AA1B::get_offset_of_transform_0(),
	Prefab_t9A8682B18F1324D8885AB33089A02BF5C742AA1B::get_offset_of_Icon_1(),
	Prefab_t9A8682B18F1324D8885AB33089A02BF5C742AA1B::get_offset_of_Name_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6924 = { sizeof (MapRawData_tF8450C3240F82CA0E23B1D062D7FC6554079475B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6924[1] = 
{
	MapRawData_tF8450C3240F82CA0E23B1D062D7FC6554079475B::get_offset_of_Items_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6925 = { sizeof (MapItems_t58F3A8F754693DC80EDDEB936D6023AC8FECCF9D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6925[1] = 
{
	MapItems_t58F3A8F754693DC80EDDEB936D6023AC8FECCF9D::get_offset_of_Items_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6926 = { sizeof (ItemRawData_t35493A15D30782C24AACFE62D54AF2D24AE80DC2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6926[3] = 
{
	ItemRawData_t35493A15D30782C24AACFE62D54AF2D24AE80DC2::get_offset_of_Position_0(),
	ItemRawData_t35493A15D30782C24AACFE62D54AF2D24AE80DC2::get_offset_of_Rotation_1(),
	ItemRawData_t35493A15D30782C24AACFE62D54AF2D24AE80DC2::get_offset_of_Prefab_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6927 = { sizeof (CyanMessage_t5A284AED26BF63EDC0379206EBE0289EDF8B4D26), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6927[6] = 
{
	CyanMessage_t5A284AED26BF63EDC0379206EBE0289EDF8B4D26::get_offset_of_OnObjectPaint_0(),
	CyanMessage_t5A284AED26BF63EDC0379206EBE0289EDF8B4D26::get_offset_of_BeforeObjectDelete_1(),
	CyanMessage_t5A284AED26BF63EDC0379206EBE0289EDF8B4D26::get_offset_of_OnHoverChange_2(),
	CyanMessage_t5A284AED26BF63EDC0379206EBE0289EDF8B4D26::get_offset_of_OnSelectionChange_3(),
	CyanMessage_t5A284AED26BF63EDC0379206EBE0289EDF8B4D26::get_offset_of_OnPoolSwitch_4(),
	CyanMessage_t5A284AED26BF63EDC0379206EBE0289EDF8B4D26::get_offset_of_OnPrefabSwitch_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6928 = { sizeof (CyanTransformEvent_t9A71D6662200E71DA050209321EBE077E3FD117C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6929 = { sizeof (CyanIntRvent_t36B9E29A72B590D0D9FEDE9A938B67733AB53FC0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6930 = { sizeof (CyanIntIntRvent_t3050562EFC3FABA385FF4019E611B224C7F39B2F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6931 = { sizeof (ActionInfo_tB61C754D893D43A27CA0ADCA6DDC1C26D2EB4A2D)+ sizeof (RuntimeObject), sizeof(ActionInfo_tB61C754D893D43A27CA0ADCA6DDC1C26D2EB4A2D_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable6931[6] = 
{
	ActionInfo_tB61C754D893D43A27CA0ADCA6DDC1C26D2EB4A2D::get_offset_of_IsCreate_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ActionInfo_tB61C754D893D43A27CA0ADCA6DDC1C26D2EB4A2D::get_offset_of_PoolID_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ActionInfo_tB61C754D893D43A27CA0ADCA6DDC1C26D2EB4A2D::get_offset_of_PrefabID_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ActionInfo_tB61C754D893D43A27CA0ADCA6DDC1C26D2EB4A2D::get_offset_of_TransformID_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ActionInfo_tB61C754D893D43A27CA0ADCA6DDC1C26D2EB4A2D::get_offset_of_Position_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ActionInfo_tB61C754D893D43A27CA0ADCA6DDC1C26D2EB4A2D::get_offset_of_Rotation_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6932 = { sizeof (U3CPrivateImplementationDetailsU3E_t80C24D421F26EA34451B875C2488070F0D2730FC), -1, sizeof(U3CPrivateImplementationDetailsU3E_t80C24D421F26EA34451B875C2488070F0D2730FC_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6932[2] = 
{
	U3CPrivateImplementationDetailsU3E_t80C24D421F26EA34451B875C2488070F0D2730FC_StaticFields::get_offset_of_U373A7CA944158467943BFEB320F79099327A00212_0(),
	U3CPrivateImplementationDetailsU3E_t80C24D421F26EA34451B875C2488070F0D2730FC_StaticFields::get_offset_of_CE5AFBB0D2B25FA36AC018803534E8B5A9C5BBCA_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6933 = { sizeof (__StaticArrayInitTypeSizeU3D14_t12646BFA3861F306D3C2EA3328A781189C87D291)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D14_t12646BFA3861F306D3C2EA3328A781189C87D291 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6934 = { sizeof (__StaticArrayInitTypeSizeU3D16_tE4C9301FB9E45F250EBDF09B231F154B1B53DEA5)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D16_tE4C9301FB9E45F250EBDF09B231F154B1B53DEA5 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6935 = { sizeof (U3CModuleU3E_tD1EFFA6201164FCC6E8DD2EDD5EE15545901839B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6936 = { sizeof (StarFxController_t43DD9475A7C574AD503BCEB33A4C3CBD985AD075), -1, sizeof(StarFxController_t43DD9475A7C574AD503BCEB33A4C3CBD985AD075_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6936[8] = 
{
	StarFxController_t43DD9475A7C574AD503BCEB33A4C3CBD985AD075::get_offset_of_starFX_4(),
	StarFxController_t43DD9475A7C574AD503BCEB33A4C3CBD985AD075::get_offset_of_ea_5(),
	StarFxController_t43DD9475A7C574AD503BCEB33A4C3CBD985AD075::get_offset_of_currentEa_6(),
	StarFxController_t43DD9475A7C574AD503BCEB33A4C3CBD985AD075::get_offset_of_delay_7(),
	StarFxController_t43DD9475A7C574AD503BCEB33A4C3CBD985AD075::get_offset_of_currentDelay_8(),
	StarFxController_t43DD9475A7C574AD503BCEB33A4C3CBD985AD075::get_offset_of_isEnd_9(),
	StarFxController_t43DD9475A7C574AD503BCEB33A4C3CBD985AD075::get_offset_of_idStar_10(),
	StarFxController_t43DD9475A7C574AD503BCEB33A4C3CBD985AD075_StaticFields::get_offset_of_myStarFxController_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6937 = { sizeof (vfxController_tBDD97B953055545A8817FEE3FF2C394707EE7547), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6937[11] = 
{
	vfxController_tBDD97B953055545A8817FEE3FF2C394707EE7547::get_offset_of_starFx01Prefabs_4(),
	vfxController_tBDD97B953055545A8817FEE3FF2C394707EE7547::get_offset_of_starFx02Prefabs_5(),
	vfxController_tBDD97B953055545A8817FEE3FF2C394707EE7547::get_offset_of_starFx03Prefabs_6(),
	vfxController_tBDD97B953055545A8817FEE3FF2C394707EE7547::get_offset_of_starFx04Prefabs_7(),
	vfxController_tBDD97B953055545A8817FEE3FF2C394707EE7547::get_offset_of_starFx05Prefabs_8(),
	vfxController_tBDD97B953055545A8817FEE3FF2C394707EE7547::get_offset_of_DesStarFxObjs_9(),
	vfxController_tBDD97B953055545A8817FEE3FF2C394707EE7547::get_offset_of_bgFxPrefabs_10(),
	vfxController_tBDD97B953055545A8817FEE3FF2C394707EE7547::get_offset_of_currentStarImage_11(),
	vfxController_tBDD97B953055545A8817FEE3FF2C394707EE7547::get_offset_of_currentStarFx_12(),
	vfxController_tBDD97B953055545A8817FEE3FF2C394707EE7547::get_offset_of_currentLevel_13(),
	vfxController_tBDD97B953055545A8817FEE3FF2C394707EE7547::get_offset_of_currentBgFx_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6938 = { sizeof (U3CModuleU3E_t2420DD831EDFA31B0432655ED654E5827F2A336B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6939 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6939[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6940 = { sizeof (IsolatedArabicLetters_tF5305790426783BA845F4BE47ECA15A8C837E9E9)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6940[42] = 
{
	IsolatedArabicLetters_tF5305790426783BA845F4BE47ECA15A8C837E9E9::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6941 = { sizeof (GeneralArabicLetters_t3A7ADF16DE49842C1265D032B8372869B07B9EBE)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6941[42] = 
{
	GeneralArabicLetters_t3A7ADF16DE49842C1265D032B8372869B07B9EBE::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6942 = { sizeof (ArabicMapping_t70BF7202D9CD13A070774F647DA888EA258B7EA2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6942[2] = 
{
	ArabicMapping_t70BF7202D9CD13A070774F647DA888EA258B7EA2::get_offset_of_from_0(),
	ArabicMapping_t70BF7202D9CD13A070774F647DA888EA258B7EA2::get_offset_of_to_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6943 = { sizeof (ArabicTable_tABF52F1916CF2138C65717244746EEC11E620DB1), -1, sizeof(ArabicTable_tABF52F1916CF2138C65717244746EEC11E620DB1_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6943[2] = 
{
	ArabicTable_tABF52F1916CF2138C65717244746EEC11E620DB1_StaticFields::get_offset_of_mapList_0(),
	ArabicTable_tABF52F1916CF2138C65717244746EEC11E620DB1_StaticFields::get_offset_of_arabicMapper_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6944 = { sizeof (TashkeelLocation_t02FFF4619086A02B2BCDF8F083015A9861F16C61), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6944[2] = 
{
	TashkeelLocation_t02FFF4619086A02B2BCDF8F083015A9861F16C61::get_offset_of_tashkeel_0(),
	TashkeelLocation_t02FFF4619086A02B2BCDF8F083015A9861F16C61::get_offset_of_position_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6945 = { sizeof (ArabicFixerTool_tF486A04CCCDA6C1029950FA33B72987A695104BE), -1, sizeof(ArabicFixerTool_tF486A04CCCDA6C1029950FA33B72987A695104BE_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6945[3] = 
{
	ArabicFixerTool_tF486A04CCCDA6C1029950FA33B72987A695104BE_StaticFields::get_offset_of_showTashkeel_0(),
	ArabicFixerTool_tF486A04CCCDA6C1029950FA33B72987A695104BE_StaticFields::get_offset_of_combineTashkeel_1(),
	ArabicFixerTool_tF486A04CCCDA6C1029950FA33B72987A695104BE_StaticFields::get_offset_of_useHinduNumbers_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6946 = { sizeof (Fix3dTextCS_t9927D5BD5BF86B691C134D9D53DCC21C262941D3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6946[3] = 
{
	Fix3dTextCS_t9927D5BD5BF86B691C134D9D53DCC21C262941D3::get_offset_of_text_4(),
	Fix3dTextCS_t9927D5BD5BF86B691C134D9D53DCC21C262941D3::get_offset_of_tashkeel_5(),
	Fix3dTextCS_t9927D5BD5BF86B691C134D9D53DCC21C262941D3::get_offset_of_hinduNumbers_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6947 = { sizeof (FixGUITextCS_t0E5CB7AABD8A55325DE47FF00F2C9A154272FC2B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6947[3] = 
{
	FixGUITextCS_t0E5CB7AABD8A55325DE47FF00F2C9A154272FC2B::get_offset_of_text_4(),
	FixGUITextCS_t0E5CB7AABD8A55325DE47FF00F2C9A154272FC2B::get_offset_of_tashkeel_5(),
	FixGUITextCS_t0E5CB7AABD8A55325DE47FF00F2C9A154272FC2B::get_offset_of_hinduNumbers_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6948 = { sizeof (FixTextMeshProUGUI_t67EB4FC50DAFB4D252C6F4BF294DA593CA013D49), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6948[3] = 
{
	FixTextMeshProUGUI_t67EB4FC50DAFB4D252C6F4BF294DA593CA013D49::get_offset_of_text_4(),
	FixTextMeshProUGUI_t67EB4FC50DAFB4D252C6F4BF294DA593CA013D49::get_offset_of_tashkeel_5(),
	FixTextMeshProUGUI_t67EB4FC50DAFB4D252C6F4BF294DA593CA013D49::get_offset_of_hinduNumbers_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6949 = { sizeof (SetArabicTextExample_t44C12A1B401FF63F8693928EA65F21CDE39F71B6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6949[1] = 
{
	SetArabicTextExample_t44C12A1B401FF63F8693928EA65F21CDE39F71B6::get_offset_of_text_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6950 = { sizeof (ListExtension_tF877099FF04236ED7637DB297435B8DB2185F540), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6951 = { sizeof (NativeShare_tD828B308303066D1C6A16FC5BAAFFF2FC50BC905), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6952 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6952[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6953 = { sizeof (CommandLineUtils_tC12399B1E5E0D200EEA1299E720BE5AD11DB1873), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6954 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6954[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6955 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6955[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6956 = { sizeof (UnityContext_t0D8D140543A4C26F9405C7E7E3CE23A0D43B0B6A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6957 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6957[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6958 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6958[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6959 = { sizeof (FastConcatUtility_t043C8ABBE1B55F767D84E6AB4FB3781438179706), -1, sizeof(FastConcatUtility_t043C8ABBE1B55F767D84E6AB4FB3781438179706_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6959[1] = 
{
	FastConcatUtility_t043C8ABBE1B55F767D84E6AB4FB3781438179706_StaticFields::get_offset_of__stringBuilder_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6960 = { sizeof (Murmur3_t9B5DE6D260DF16828C4D99596E8DE8EF28CA0D63), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6961 = { sizeof (NetFXCoreWrappers_tD2C7C0C4D3695246DB1CC58C809224C3B1D968BC), -1, sizeof(NetFXCoreWrappers_tD2C7C0C4D3695246DB1CC58C809224C3B1D968BC_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6961[1] = 
{
	NetFXCoreWrappers_tD2C7C0C4D3695246DB1CC58C809224C3B1D968BC_StaticFields::get_offset_of__compilerType_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6962 = { sizeof (SafeEvent_t476B080AD6C587A6CAC0C20F14D54871A8CD34C4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6963 = { sizeof (PixWrapper_tB8CD50D0874CDEE9CF018F647FEE035B52003458), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6964 = { sizeof (TaskRunnerExtensions_tB5716337975FB8CFB597423EE2C15E46BEF9995D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6965 = { sizeof (Assert_t32438BFB9A55883E4D28CFDA4E439285219D71B6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6966 = { sizeof (LinqExtensions_tBA515E336DD5AAA0EBEA00C99E72CDEF1687D3EA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6967 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6967[9] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6968 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6968[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6969 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6969[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6970 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6970[11] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6971 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6971[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6972 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6972[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6973 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6973[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6974 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6974[11] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6975 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6975[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6976 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6976[11] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6977 = { sizeof (Log_tC116D6C973E7531C2AA4AA0C12A6AE7D0A691B40), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6978 = { sizeof (MiscExtensions_tF3AAB0F6CDB9245A5F8ACC2831221532F6B641FF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6979 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6979[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6980 = { sizeof (TypeExtensions_tED5DBFEEF4E4BE76D253B3393C6D36E7552C0196), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6981 = { sizeof (U3CGetParentTypesU3Ed__22_t09FE9B3783AE41E4985C473FF68DCC15529BAA58), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6981[6] = 
{
	U3CGetParentTypesU3Ed__22_t09FE9B3783AE41E4985C473FF68DCC15529BAA58::get_offset_of_U3CU3E1__state_0(),
	U3CGetParentTypesU3Ed__22_t09FE9B3783AE41E4985C473FF68DCC15529BAA58::get_offset_of_U3CU3E2__current_1(),
	U3CGetParentTypesU3Ed__22_t09FE9B3783AE41E4985C473FF68DCC15529BAA58::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CGetParentTypesU3Ed__22_t09FE9B3783AE41E4985C473FF68DCC15529BAA58::get_offset_of_type_3(),
	U3CGetParentTypesU3Ed__22_t09FE9B3783AE41E4985C473FF68DCC15529BAA58::get_offset_of_U3CU3E3__type_4(),
	U3CGetParentTypesU3Ed__22_t09FE9B3783AE41E4985C473FF68DCC15529BAA58::get_offset_of_U3CU3E7__wrap1_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6982 = { sizeof (U3CGetAllInstanceFieldsU3Ed__25_tD7F6236F6A4B81B14F6226370E24A70D5B63D905), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6982[8] = 
{
	U3CGetAllInstanceFieldsU3Ed__25_tD7F6236F6A4B81B14F6226370E24A70D5B63D905::get_offset_of_U3CU3E1__state_0(),
	U3CGetAllInstanceFieldsU3Ed__25_tD7F6236F6A4B81B14F6226370E24A70D5B63D905::get_offset_of_U3CU3E2__current_1(),
	U3CGetAllInstanceFieldsU3Ed__25_tD7F6236F6A4B81B14F6226370E24A70D5B63D905::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CGetAllInstanceFieldsU3Ed__25_tD7F6236F6A4B81B14F6226370E24A70D5B63D905::get_offset_of_type_3(),
	U3CGetAllInstanceFieldsU3Ed__25_tD7F6236F6A4B81B14F6226370E24A70D5B63D905::get_offset_of_U3CU3E3__type_4(),
	U3CGetAllInstanceFieldsU3Ed__25_tD7F6236F6A4B81B14F6226370E24A70D5B63D905::get_offset_of_U3CU3E7__wrap1_5(),
	U3CGetAllInstanceFieldsU3Ed__25_tD7F6236F6A4B81B14F6226370E24A70D5B63D905::get_offset_of_U3CU3E7__wrap2_6(),
	U3CGetAllInstanceFieldsU3Ed__25_tD7F6236F6A4B81B14F6226370E24A70D5B63D905::get_offset_of_U3CU3E7__wrap3_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6983 = { sizeof (U3CGetAllInstancePropertiesU3Ed__26_tABD7A33B589EF2C3764831583FF7B6D0E832BA64), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6983[8] = 
{
	U3CGetAllInstancePropertiesU3Ed__26_tABD7A33B589EF2C3764831583FF7B6D0E832BA64::get_offset_of_U3CU3E1__state_0(),
	U3CGetAllInstancePropertiesU3Ed__26_tABD7A33B589EF2C3764831583FF7B6D0E832BA64::get_offset_of_U3CU3E2__current_1(),
	U3CGetAllInstancePropertiesU3Ed__26_tABD7A33B589EF2C3764831583FF7B6D0E832BA64::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CGetAllInstancePropertiesU3Ed__26_tABD7A33B589EF2C3764831583FF7B6D0E832BA64::get_offset_of_type_3(),
	U3CGetAllInstancePropertiesU3Ed__26_tABD7A33B589EF2C3764831583FF7B6D0E832BA64::get_offset_of_U3CU3E3__type_4(),
	U3CGetAllInstancePropertiesU3Ed__26_tABD7A33B589EF2C3764831583FF7B6D0E832BA64::get_offset_of_U3CU3E7__wrap1_5(),
	U3CGetAllInstancePropertiesU3Ed__26_tABD7A33B589EF2C3764831583FF7B6D0E832BA64::get_offset_of_U3CU3E7__wrap2_6(),
	U3CGetAllInstancePropertiesU3Ed__26_tABD7A33B589EF2C3764831583FF7B6D0E832BA64::get_offset_of_U3CU3E7__wrap3_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6984 = { sizeof (U3CGetAllInstanceMethodsU3Ed__27_tAE22CA9FF15C83847047E3977AF78DAF2771F9AE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6984[8] = 
{
	U3CGetAllInstanceMethodsU3Ed__27_tAE22CA9FF15C83847047E3977AF78DAF2771F9AE::get_offset_of_U3CU3E1__state_0(),
	U3CGetAllInstanceMethodsU3Ed__27_tAE22CA9FF15C83847047E3977AF78DAF2771F9AE::get_offset_of_U3CU3E2__current_1(),
	U3CGetAllInstanceMethodsU3Ed__27_tAE22CA9FF15C83847047E3977AF78DAF2771F9AE::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CGetAllInstanceMethodsU3Ed__27_tAE22CA9FF15C83847047E3977AF78DAF2771F9AE::get_offset_of_type_3(),
	U3CGetAllInstanceMethodsU3Ed__27_tAE22CA9FF15C83847047E3977AF78DAF2771F9AE::get_offset_of_U3CU3E3__type_4(),
	U3CGetAllInstanceMethodsU3Ed__27_tAE22CA9FF15C83847047E3977AF78DAF2771F9AE::get_offset_of_U3CU3E7__wrap1_5(),
	U3CGetAllInstanceMethodsU3Ed__27_tAE22CA9FF15C83847047E3977AF78DAF2771F9AE::get_offset_of_U3CU3E7__wrap2_6(),
	U3CGetAllInstanceMethodsU3Ed__27_tAE22CA9FF15C83847047E3977AF78DAF2771F9AE::get_offset_of_U3CU3E7__wrap3_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6985 = { sizeof (U3CU3Ec__DisplayClass35_0_t708B31425175507F80B7B782BD80203EC7AA6441), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6985[1] = 
{
	U3CU3Ec__DisplayClass35_0_t708B31425175507F80B7B782BD80203EC7AA6441::get_offset_of_attributeTypes_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6986 = { sizeof (U3CU3Ec__DisplayClass35_1_tE3F17278B9F72ABDD93289E988A9BD816B77406A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6986[1] = 
{
	U3CU3Ec__DisplayClass35_1_tE3F17278B9F72ABDD93289E988A9BD816B77406A::get_offset_of_a_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6987 = { sizeof (U3CU3Ec__DisplayClass39_0_t9526CECAB44E4C658FD5B8B1CFF0FCEF9C1EB4F1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6987[1] = 
{
	U3CU3Ec__DisplayClass39_0_t9526CECAB44E4C658FD5B8B1CFF0FCEF9C1EB4F1::get_offset_of_attributeTypes_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6988 = { sizeof (U3CU3Ec__DisplayClass39_1_t146B962CF3457AB8ECF100B1E71100370B3172B2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6988[1] = 
{
	U3CU3Ec__DisplayClass39_1_t146B962CF3457AB8ECF100B1E71100370B3172B2::get_offset_of_a_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6989 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6990 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6991 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6992 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6993 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6994 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6995 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6996 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6997 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6998 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6999 = { sizeof (PreserveAttribute_tF43F26FBD37217D5D84FA64A21C86AB4DCE29CC6), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
