﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// BattleEcsInitializer
struct BattleEcsInitializer_tA76C29D8A9881FDF3955010F51F9DA4FA7134C67;
// BattleSettingsSO
struct BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE;
// CameraShake
struct CameraShake_t5EBAAD00C215BE104B246C1E17C22CDDEA0F3F7E;
// Contexts
struct Contexts_t313FEE68C5FB0568E30785C33C0F6209F5F7B87D;
// CustomTutorialModel
struct CustomTutorialModel_t7F1E6EF5B6AF58AEA9FB2573811B4562E587E635;
// CustomTutorialSO
struct CustomTutorialSO_t45FC033F62BB26BC5595EAA9FEF1DF50BFA2A41D;
// DG.Tweening.Tweener
struct Tweener_t9B2A5E94EE6D11F7607E58AE4E37186FF63587C8;
// EffectVisual
struct EffectVisual_tE885D61366B46DC715156887B773663B79D6777A;
// EffectVisualSO
struct EffectVisualSO_t1C5856B5E821FD66A4C1BD41DB5194A5610E6FFC;
// Entitas.ICollector`1<GameEntity>
struct ICollector_1_tDA9721194F1438993CA7EC5D39C69B898FBBB26A;
// Entitas.IGroup`1<GameEntity>
struct IGroup_1_tF4940889845236B5907C50AD6A40AD6CE69EC680;
// FeaturesSwitchConfig
struct FeaturesSwitchConfig_tBF1848651D45C05C78F36D68CB647F5CB2F802D3;
// GameContext
struct GameContext_t7DA531FCF38581666B3642E3E737E5B057494978;
// GameSettingsSO
struct GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C;
// GoalsTutorialNode
struct GoalsTutorialNode_tF301448520894286687F236F80234B8A56CBE7B1;
// InventorySystem
struct InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2;
// InventoryVO
struct InventoryVO_tC7F0305FC930782BD2E4A47E3BD0504F0DCC6AD9;
// ItemsSO
struct ItemsSO_tD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A;
// KickerManager
struct KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA;
// LevelModel
struct LevelModel_t5549E807391907F7844D01A4FB4004330AF5E05C;
// LoseSummaryNodeData
struct LoseSummaryNodeData_t9807706F230D328167B227F082922DBD1C0BAA36;
// PerLevelTutorialModel
struct PerLevelTutorialModel_t617000CC336C47983E77A6E6171F0DE08379A61C;
// PerLevelTutorialSO
struct PerLevelTutorialSO_tB55F6372D80FCA75DD834538D5137169A7A7E66B;
// SoundSO
struct SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73;
// SpellUIInitializer
struct SpellUIInitializer_t66B06A358BA3B762376CD13F820EFC620ADFDAD8;
// SpellsSO
struct SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439;
// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.String,GameEntity>
struct Dictionary_2_t70CAAE41AA21DBE49EE58820FA11623347BDFA5D;
// System.Collections.Generic.Dictionary`2<System.String,UIObjective>
struct Dictionary_2_tA079ADCDB82918F4B62B96F8654CEBEBF463A062;
// System.Collections.Generic.List`1<Entitas.ICleanupSystem>
struct List_1_t3E0F132FC1070967FBCD19B4BDF44B4F85CA03AC;
// System.Collections.Generic.List`1<Entitas.IExecuteSystem>
struct List_1_tE232269578E5E48549D25DD0C9823B612D968293;
// System.Collections.Generic.List`1<Entitas.IInitializeSystem>
struct List_1_t1E70CB97F04FCE20A9F01BCEBF8EB1C867FC348C;
// System.Collections.Generic.List`1<Entitas.ITearDownSystem>
struct List_1_t78F50ABA4688AD6A1255F66AB89EE8BAFD069913;
// System.Collections.Generic.List`1<GameEntity>
struct List_1_t37F4C7CF3D1202E4756C2E10D18F5A665A14FA29;
// System.Func`3<GameEntity,Entitas.IComponent,System.String>
struct Func_3_tC499949CBF645C3D2C44549A0A930BF6F9485FA1;
// System.Func`3<GameEntity,Entitas.IComponent,System.String[]>
struct Func_3_tC179A8DB3DC1F41CFC7E52AB5999F0F276F0AF05;
// System.Runtime.CompilerServices.IAsyncStateMachine
struct IAsyncStateMachine_tEFDFBE18E061A6065AB2FF735F1425FB59F919BC;
// System.String
struct String_t;
// System.Threading.Tasks.Task
struct Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2;
// System.Threading.Tasks.Task`1<System.Threading.Tasks.VoidTaskResult>
struct Task_1_t1359D75350E9D976BFA28AD96E417450DE277673;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// TMPro.TextMeshProUGUI
struct TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438;
// Tayr.ILibrary
struct ILibrary_tCBD3DE1F66AD92DAB1112B8018EBA4A4BD23767C;
// Tayr.INodeAnimationHandler
struct INodeAnimationHandler_t7A50F84EDDDD8CA150788E8B2A8D0F06E5ABF200;
// Tayr.NodeAnimator
struct NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F;
// Tayr.TList
struct TList_tB67CEA10085B72B7C69ACE7FCF01FCC38E6EF292;
// Tayr.TLocalizerTextMeshProUGUI
struct TLocalizerTextMeshProUGUI_tC187D21D45AC70D0B204800BB874B0BEDAAFD557;
// Tayr.TSoundSystem
struct TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511;
// TowersSO
struct TowersSO_t3B7DE821E5F8017950776EEFC42E680B361BCDF0;
// TrapsSO
struct TrapsSO_tFF060852B3652DD05F33E1B19B8EC9166468D9C2;
// UnitsSO
struct UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423;
// UnityEngine.Animator
struct Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.ParticleSystem
struct ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.UI.Button
struct Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B;
// UnityEngine.UI.Image
struct Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E;
// UserVO
struct UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94;
// WinSummaryNodeData
struct WinSummaryNodeData_tD1C3F590D3B3A7D085A783296AD766C9DDA98DEE;
// Zenject.DiContainer
struct DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef ADDGAMEOBJECTEFFECTRENDERCOMPONENT_T837ECF100B0B3C3D326C867C4185C585ADB19DE1_H
#define ADDGAMEOBJECTEFFECTRENDERCOMPONENT_T837ECF100B0B3C3D326C867C4185C585ADB19DE1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AddGameObjectEffectRenderComponent
struct  AddGameObjectEffectRenderComponent_t837ECF100B0B3C3D326C867C4185C585ADB19DE1  : public RuntimeObject
{
public:
	// System.String AddGameObjectEffectRenderComponent::TargetId
	String_t* ___TargetId_0;
	// EffectVisual AddGameObjectEffectRenderComponent::EffectVisual
	EffectVisual_tE885D61366B46DC715156887B773663B79D6777A * ___EffectVisual_1;

public:
	inline static int32_t get_offset_of_TargetId_0() { return static_cast<int32_t>(offsetof(AddGameObjectEffectRenderComponent_t837ECF100B0B3C3D326C867C4185C585ADB19DE1, ___TargetId_0)); }
	inline String_t* get_TargetId_0() const { return ___TargetId_0; }
	inline String_t** get_address_of_TargetId_0() { return &___TargetId_0; }
	inline void set_TargetId_0(String_t* value)
	{
		___TargetId_0 = value;
		Il2CppCodeGenWriteBarrier((&___TargetId_0), value);
	}

	inline static int32_t get_offset_of_EffectVisual_1() { return static_cast<int32_t>(offsetof(AddGameObjectEffectRenderComponent_t837ECF100B0B3C3D326C867C4185C585ADB19DE1, ___EffectVisual_1)); }
	inline EffectVisual_tE885D61366B46DC715156887B773663B79D6777A * get_EffectVisual_1() const { return ___EffectVisual_1; }
	inline EffectVisual_tE885D61366B46DC715156887B773663B79D6777A ** get_address_of_EffectVisual_1() { return &___EffectVisual_1; }
	inline void set_EffectVisual_1(EffectVisual_tE885D61366B46DC715156887B773663B79D6777A * value)
	{
		___EffectVisual_1 = value;
		Il2CppCodeGenWriteBarrier((&___EffectVisual_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADDGAMEOBJECTEFFECTRENDERCOMPONENT_T837ECF100B0B3C3D326C867C4185C585ADB19DE1_H
#ifndef ANIMATORCOMPONENT_T6440D282B095E4FC14E536BAF9D000A85EA6A7E2_H
#define ANIMATORCOMPONENT_T6440D282B095E4FC14E536BAF9D000A85EA6A7E2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AnimatorComponent
struct  AnimatorComponent_t6440D282B095E4FC14E536BAF9D000A85EA6A7E2  : public RuntimeObject
{
public:
	// UnityEngine.Animator AnimatorComponent::Animator
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ___Animator_0;

public:
	inline static int32_t get_offset_of_Animator_0() { return static_cast<int32_t>(offsetof(AnimatorComponent_t6440D282B095E4FC14E536BAF9D000A85EA6A7E2, ___Animator_0)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get_Animator_0() const { return ___Animator_0; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of_Animator_0() { return &___Animator_0; }
	inline void set_Animator_0(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		___Animator_0 = value;
		Il2CppCodeGenWriteBarrier((&___Animator_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATORCOMPONENT_T6440D282B095E4FC14E536BAF9D000A85EA6A7E2_H
#ifndef BOOSTERTAPCOMPONENT_T4873A307B15974377549C253DB2C79D93C041517_H
#define BOOSTERTAPCOMPONENT_T4873A307B15974377549C253DB2C79D93C041517_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BoosterTapComponent
struct  BoosterTapComponent_t4873A307B15974377549C253DB2C79D93C041517  : public RuntimeObject
{
public:
	// System.String BoosterTapComponent::SpellId
	String_t* ___SpellId_0;

public:
	inline static int32_t get_offset_of_SpellId_0() { return static_cast<int32_t>(offsetof(BoosterTapComponent_t4873A307B15974377549C253DB2C79D93C041517, ___SpellId_0)); }
	inline String_t* get_SpellId_0() const { return ___SpellId_0; }
	inline String_t** get_address_of_SpellId_0() { return &___SpellId_0; }
	inline void set_SpellId_0(String_t* value)
	{
		___SpellId_0 = value;
		Il2CppCodeGenWriteBarrier((&___SpellId_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOSTERTAPCOMPONENT_T4873A307B15974377549C253DB2C79D93C041517_H
#ifndef BUTTONTAPCOMPONENT_T1064F9A8B3F34BBD1333866AF91575596A6E015F_H
#define BUTTONTAPCOMPONENT_T1064F9A8B3F34BBD1333866AF91575596A6E015F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ButtonTapComponent
struct  ButtonTapComponent_t1064F9A8B3F34BBD1333866AF91575596A6E015F  : public RuntimeObject
{
public:
	// System.String ButtonTapComponent::SpellId
	String_t* ___SpellId_0;

public:
	inline static int32_t get_offset_of_SpellId_0() { return static_cast<int32_t>(offsetof(ButtonTapComponent_t1064F9A8B3F34BBD1333866AF91575596A6E015F, ___SpellId_0)); }
	inline String_t* get_SpellId_0() const { return ___SpellId_0; }
	inline String_t** get_address_of_SpellId_0() { return &___SpellId_0; }
	inline void set_SpellId_0(String_t* value)
	{
		___SpellId_0 = value;
		Il2CppCodeGenWriteBarrier((&___SpellId_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONTAPCOMPONENT_T1064F9A8B3F34BBD1333866AF91575596A6E015F_H
#ifndef CALLOUTSCOMPONENT_T94CD42511A2D8486E40304BD74431F08BC8C663C_H
#define CALLOUTSCOMPONENT_T94CD42511A2D8486E40304BD74431F08BC8C663C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CalloutsComponent
struct  CalloutsComponent_t94CD42511A2D8486E40304BD74431F08BC8C663C  : public RuntimeObject
{
public:
	// System.String CalloutsComponent::CalloutId
	String_t* ___CalloutId_0;

public:
	inline static int32_t get_offset_of_CalloutId_0() { return static_cast<int32_t>(offsetof(CalloutsComponent_t94CD42511A2D8486E40304BD74431F08BC8C663C, ___CalloutId_0)); }
	inline String_t* get_CalloutId_0() const { return ___CalloutId_0; }
	inline String_t** get_address_of_CalloutId_0() { return &___CalloutId_0; }
	inline void set_CalloutId_0(String_t* value)
	{
		___CalloutId_0 = value;
		Il2CppCodeGenWriteBarrier((&___CalloutId_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CALLOUTSCOMPONENT_T94CD42511A2D8486E40304BD74431F08BC8C663C_H
#ifndef U3CSHAKEU3ED__12_TB0AF5B6AE8BF2353E9DFDB29307648337003BF25_H
#define U3CSHAKEU3ED__12_TB0AF5B6AE8BF2353E9DFDB29307648337003BF25_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraShake_<Shake>d__12
struct  U3CShakeU3Ed__12_tB0AF5B6AE8BF2353E9DFDB29307648337003BF25  : public RuntimeObject
{
public:
	// System.Int32 CameraShake_<Shake>d__12::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object CameraShake_<Shake>d__12::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// CameraShake CameraShake_<Shake>d__12::<>4__this
	CameraShake_t5EBAAD00C215BE104B246C1E17C22CDDEA0F3F7E * ___U3CU3E4__this_2;
	// System.Single CameraShake_<Shake>d__12::delay
	float ___delay_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CShakeU3Ed__12_tB0AF5B6AE8BF2353E9DFDB29307648337003BF25, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CShakeU3Ed__12_tB0AF5B6AE8BF2353E9DFDB29307648337003BF25, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CShakeU3Ed__12_tB0AF5B6AE8BF2353E9DFDB29307648337003BF25, ___U3CU3E4__this_2)); }
	inline CameraShake_t5EBAAD00C215BE104B246C1E17C22CDDEA0F3F7E * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline CameraShake_t5EBAAD00C215BE104B246C1E17C22CDDEA0F3F7E ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(CameraShake_t5EBAAD00C215BE104B246C1E17C22CDDEA0F3F7E * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_delay_3() { return static_cast<int32_t>(offsetof(U3CShakeU3Ed__12_tB0AF5B6AE8BF2353E9DFDB29307648337003BF25, ___delay_3)); }
	inline float get_delay_3() const { return ___delay_3; }
	inline float* get_address_of_delay_3() { return &___delay_3; }
	inline void set_delay_3(float value)
	{
		___delay_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSHAKEU3ED__12_TB0AF5B6AE8BF2353E9DFDB29307648337003BF25_H
#ifndef CAMERASHAKECOMPONENT_TA29F67821BA0742287EBFC5C4CC853E4C8F533EC_H
#define CAMERASHAKECOMPONENT_TA29F67821BA0742287EBFC5C4CC853E4C8F533EC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraShakeComponent
struct  CameraShakeComponent_tA29F67821BA0742287EBFC5C4CC853E4C8F533EC  : public RuntimeObject
{
public:
	// System.Single CameraShakeComponent::ShakeAmount
	float ___ShakeAmount_0;
	// System.Single CameraShakeComponent::ShakeDuration
	float ___ShakeDuration_1;
	// System.Single CameraShakeComponent::Delay
	float ___Delay_2;

public:
	inline static int32_t get_offset_of_ShakeAmount_0() { return static_cast<int32_t>(offsetof(CameraShakeComponent_tA29F67821BA0742287EBFC5C4CC853E4C8F533EC, ___ShakeAmount_0)); }
	inline float get_ShakeAmount_0() const { return ___ShakeAmount_0; }
	inline float* get_address_of_ShakeAmount_0() { return &___ShakeAmount_0; }
	inline void set_ShakeAmount_0(float value)
	{
		___ShakeAmount_0 = value;
	}

	inline static int32_t get_offset_of_ShakeDuration_1() { return static_cast<int32_t>(offsetof(CameraShakeComponent_tA29F67821BA0742287EBFC5C4CC853E4C8F533EC, ___ShakeDuration_1)); }
	inline float get_ShakeDuration_1() const { return ___ShakeDuration_1; }
	inline float* get_address_of_ShakeDuration_1() { return &___ShakeDuration_1; }
	inline void set_ShakeDuration_1(float value)
	{
		___ShakeDuration_1 = value;
	}

	inline static int32_t get_offset_of_Delay_2() { return static_cast<int32_t>(offsetof(CameraShakeComponent_tA29F67821BA0742287EBFC5C4CC853E4C8F533EC, ___Delay_2)); }
	inline float get_Delay_2() const { return ___Delay_2; }
	inline float* get_address_of_Delay_2() { return &___Delay_2; }
	inline void set_Delay_2(float value)
	{
		___Delay_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERASHAKECOMPONENT_TA29F67821BA0742287EBFC5C4CC853E4C8F533EC_H
#ifndef U3CU3EC_TC4E3FD3778FF243A0B879DA96D098918C6DB772C_H
#define U3CU3EC_TC4E3FD3778FF243A0B879DA96D098918C6DB772C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EffectRenderEntityIndex_<>c
struct  U3CU3Ec_tC4E3FD3778FF243A0B879DA96D098918C6DB772C  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tC4E3FD3778FF243A0B879DA96D098918C6DB772C_StaticFields
{
public:
	// EffectRenderEntityIndex_<>c EffectRenderEntityIndex_<>c::<>9
	U3CU3Ec_tC4E3FD3778FF243A0B879DA96D098918C6DB772C * ___U3CU3E9_0;
	// System.Func`3<GameEntity,Entitas.IComponent,System.String> EffectRenderEntityIndex_<>c::<>9__0_0
	Func_3_tC499949CBF645C3D2C44549A0A930BF6F9485FA1 * ___U3CU3E9__0_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tC4E3FD3778FF243A0B879DA96D098918C6DB772C_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tC4E3FD3778FF243A0B879DA96D098918C6DB772C * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tC4E3FD3778FF243A0B879DA96D098918C6DB772C ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tC4E3FD3778FF243A0B879DA96D098918C6DB772C * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__0_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tC4E3FD3778FF243A0B879DA96D098918C6DB772C_StaticFields, ___U3CU3E9__0_0_1)); }
	inline Func_3_tC499949CBF645C3D2C44549A0A930BF6F9485FA1 * get_U3CU3E9__0_0_1() const { return ___U3CU3E9__0_0_1; }
	inline Func_3_tC499949CBF645C3D2C44549A0A930BF6F9485FA1 ** get_address_of_U3CU3E9__0_0_1() { return &___U3CU3E9__0_0_1; }
	inline void set_U3CU3E9__0_0_1(Func_3_tC499949CBF645C3D2C44549A0A930BF6F9485FA1 * value)
	{
		___U3CU3E9__0_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__0_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TC4E3FD3778FF243A0B879DA96D098918C6DB772C_H
#ifndef ABSTRACTENTITYINDEX_2_T21CBBEC4E85C6B3DF1A910297A15C295BA77FBEA_H
#define ABSTRACTENTITYINDEX_2_T21CBBEC4E85C6B3DF1A910297A15C295BA77FBEA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.AbstractEntityIndex`2<GameEntity,System.String>
struct  AbstractEntityIndex_2_t21CBBEC4E85C6B3DF1A910297A15C295BA77FBEA  : public RuntimeObject
{
public:
	// System.String Entitas.AbstractEntityIndex`2::_name
	String_t* ____name_0;
	// Entitas.IGroup`1<TEntity> Entitas.AbstractEntityIndex`2::_group
	RuntimeObject* ____group_1;
	// System.Func`3<TEntity,Entitas.IComponent,TKey> Entitas.AbstractEntityIndex`2::_getKey
	Func_3_tC499949CBF645C3D2C44549A0A930BF6F9485FA1 * ____getKey_2;
	// System.Func`3<TEntity,Entitas.IComponent,TKey[]> Entitas.AbstractEntityIndex`2::_getKeys
	Func_3_tC179A8DB3DC1F41CFC7E52AB5999F0F276F0AF05 * ____getKeys_3;
	// System.Boolean Entitas.AbstractEntityIndex`2::_isSingleKey
	bool ____isSingleKey_4;

public:
	inline static int32_t get_offset_of__name_0() { return static_cast<int32_t>(offsetof(AbstractEntityIndex_2_t21CBBEC4E85C6B3DF1A910297A15C295BA77FBEA, ____name_0)); }
	inline String_t* get__name_0() const { return ____name_0; }
	inline String_t** get_address_of__name_0() { return &____name_0; }
	inline void set__name_0(String_t* value)
	{
		____name_0 = value;
		Il2CppCodeGenWriteBarrier((&____name_0), value);
	}

	inline static int32_t get_offset_of__group_1() { return static_cast<int32_t>(offsetof(AbstractEntityIndex_2_t21CBBEC4E85C6B3DF1A910297A15C295BA77FBEA, ____group_1)); }
	inline RuntimeObject* get__group_1() const { return ____group_1; }
	inline RuntimeObject** get_address_of__group_1() { return &____group_1; }
	inline void set__group_1(RuntimeObject* value)
	{
		____group_1 = value;
		Il2CppCodeGenWriteBarrier((&____group_1), value);
	}

	inline static int32_t get_offset_of__getKey_2() { return static_cast<int32_t>(offsetof(AbstractEntityIndex_2_t21CBBEC4E85C6B3DF1A910297A15C295BA77FBEA, ____getKey_2)); }
	inline Func_3_tC499949CBF645C3D2C44549A0A930BF6F9485FA1 * get__getKey_2() const { return ____getKey_2; }
	inline Func_3_tC499949CBF645C3D2C44549A0A930BF6F9485FA1 ** get_address_of__getKey_2() { return &____getKey_2; }
	inline void set__getKey_2(Func_3_tC499949CBF645C3D2C44549A0A930BF6F9485FA1 * value)
	{
		____getKey_2 = value;
		Il2CppCodeGenWriteBarrier((&____getKey_2), value);
	}

	inline static int32_t get_offset_of__getKeys_3() { return static_cast<int32_t>(offsetof(AbstractEntityIndex_2_t21CBBEC4E85C6B3DF1A910297A15C295BA77FBEA, ____getKeys_3)); }
	inline Func_3_tC179A8DB3DC1F41CFC7E52AB5999F0F276F0AF05 * get__getKeys_3() const { return ____getKeys_3; }
	inline Func_3_tC179A8DB3DC1F41CFC7E52AB5999F0F276F0AF05 ** get_address_of__getKeys_3() { return &____getKeys_3; }
	inline void set__getKeys_3(Func_3_tC179A8DB3DC1F41CFC7E52AB5999F0F276F0AF05 * value)
	{
		____getKeys_3 = value;
		Il2CppCodeGenWriteBarrier((&____getKeys_3), value);
	}

	inline static int32_t get_offset_of__isSingleKey_4() { return static_cast<int32_t>(offsetof(AbstractEntityIndex_2_t21CBBEC4E85C6B3DF1A910297A15C295BA77FBEA, ____isSingleKey_4)); }
	inline bool get__isSingleKey_4() const { return ____isSingleKey_4; }
	inline bool* get_address_of__isSingleKey_4() { return &____isSingleKey_4; }
	inline void set__isSingleKey_4(bool value)
	{
		____isSingleKey_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTENTITYINDEX_2_T21CBBEC4E85C6B3DF1A910297A15C295BA77FBEA_H
#ifndef REACTIVESYSTEM_1_TD4874D09441436F0E3DB74FACC6C25A1F78AE0F9_H
#define REACTIVESYSTEM_1_TD4874D09441436F0E3DB74FACC6C25A1F78AE0F9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.ReactiveSystem`1<GameEntity>
struct  ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9  : public RuntimeObject
{
public:
	// Entitas.ICollector`1<TEntity> Entitas.ReactiveSystem`1::_collector
	RuntimeObject* ____collector_0;
	// System.Collections.Generic.List`1<TEntity> Entitas.ReactiveSystem`1::_buffer
	List_1_t37F4C7CF3D1202E4756C2E10D18F5A665A14FA29 * ____buffer_1;
	// System.String Entitas.ReactiveSystem`1::_toStringCache
	String_t* ____toStringCache_2;

public:
	inline static int32_t get_offset_of__collector_0() { return static_cast<int32_t>(offsetof(ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9, ____collector_0)); }
	inline RuntimeObject* get__collector_0() const { return ____collector_0; }
	inline RuntimeObject** get_address_of__collector_0() { return &____collector_0; }
	inline void set__collector_0(RuntimeObject* value)
	{
		____collector_0 = value;
		Il2CppCodeGenWriteBarrier((&____collector_0), value);
	}

	inline static int32_t get_offset_of__buffer_1() { return static_cast<int32_t>(offsetof(ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9, ____buffer_1)); }
	inline List_1_t37F4C7CF3D1202E4756C2E10D18F5A665A14FA29 * get__buffer_1() const { return ____buffer_1; }
	inline List_1_t37F4C7CF3D1202E4756C2E10D18F5A665A14FA29 ** get_address_of__buffer_1() { return &____buffer_1; }
	inline void set__buffer_1(List_1_t37F4C7CF3D1202E4756C2E10D18F5A665A14FA29 * value)
	{
		____buffer_1 = value;
		Il2CppCodeGenWriteBarrier((&____buffer_1), value);
	}

	inline static int32_t get_offset_of__toStringCache_2() { return static_cast<int32_t>(offsetof(ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9, ____toStringCache_2)); }
	inline String_t* get__toStringCache_2() const { return ____toStringCache_2; }
	inline String_t** get_address_of__toStringCache_2() { return &____toStringCache_2; }
	inline void set__toStringCache_2(String_t* value)
	{
		____toStringCache_2 = value;
		Il2CppCodeGenWriteBarrier((&____toStringCache_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REACTIVESYSTEM_1_TD4874D09441436F0E3DB74FACC6C25A1F78AE0F9_H
#ifndef SYSTEMS_T4C86DA84B6D6F50FF23B497DD599ED594274DEA9_H
#define SYSTEMS_T4C86DA84B6D6F50FF23B497DD599ED594274DEA9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.Systems
struct  Systems_t4C86DA84B6D6F50FF23B497DD599ED594274DEA9  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Entitas.IInitializeSystem> Entitas.Systems::_initializeSystems
	List_1_t1E70CB97F04FCE20A9F01BCEBF8EB1C867FC348C * ____initializeSystems_0;
	// System.Collections.Generic.List`1<Entitas.IExecuteSystem> Entitas.Systems::_executeSystems
	List_1_tE232269578E5E48549D25DD0C9823B612D968293 * ____executeSystems_1;
	// System.Collections.Generic.List`1<Entitas.ICleanupSystem> Entitas.Systems::_cleanupSystems
	List_1_t3E0F132FC1070967FBCD19B4BDF44B4F85CA03AC * ____cleanupSystems_2;
	// System.Collections.Generic.List`1<Entitas.ITearDownSystem> Entitas.Systems::_tearDownSystems
	List_1_t78F50ABA4688AD6A1255F66AB89EE8BAFD069913 * ____tearDownSystems_3;

public:
	inline static int32_t get_offset_of__initializeSystems_0() { return static_cast<int32_t>(offsetof(Systems_t4C86DA84B6D6F50FF23B497DD599ED594274DEA9, ____initializeSystems_0)); }
	inline List_1_t1E70CB97F04FCE20A9F01BCEBF8EB1C867FC348C * get__initializeSystems_0() const { return ____initializeSystems_0; }
	inline List_1_t1E70CB97F04FCE20A9F01BCEBF8EB1C867FC348C ** get_address_of__initializeSystems_0() { return &____initializeSystems_0; }
	inline void set__initializeSystems_0(List_1_t1E70CB97F04FCE20A9F01BCEBF8EB1C867FC348C * value)
	{
		____initializeSystems_0 = value;
		Il2CppCodeGenWriteBarrier((&____initializeSystems_0), value);
	}

	inline static int32_t get_offset_of__executeSystems_1() { return static_cast<int32_t>(offsetof(Systems_t4C86DA84B6D6F50FF23B497DD599ED594274DEA9, ____executeSystems_1)); }
	inline List_1_tE232269578E5E48549D25DD0C9823B612D968293 * get__executeSystems_1() const { return ____executeSystems_1; }
	inline List_1_tE232269578E5E48549D25DD0C9823B612D968293 ** get_address_of__executeSystems_1() { return &____executeSystems_1; }
	inline void set__executeSystems_1(List_1_tE232269578E5E48549D25DD0C9823B612D968293 * value)
	{
		____executeSystems_1 = value;
		Il2CppCodeGenWriteBarrier((&____executeSystems_1), value);
	}

	inline static int32_t get_offset_of__cleanupSystems_2() { return static_cast<int32_t>(offsetof(Systems_t4C86DA84B6D6F50FF23B497DD599ED594274DEA9, ____cleanupSystems_2)); }
	inline List_1_t3E0F132FC1070967FBCD19B4BDF44B4F85CA03AC * get__cleanupSystems_2() const { return ____cleanupSystems_2; }
	inline List_1_t3E0F132FC1070967FBCD19B4BDF44B4F85CA03AC ** get_address_of__cleanupSystems_2() { return &____cleanupSystems_2; }
	inline void set__cleanupSystems_2(List_1_t3E0F132FC1070967FBCD19B4BDF44B4F85CA03AC * value)
	{
		____cleanupSystems_2 = value;
		Il2CppCodeGenWriteBarrier((&____cleanupSystems_2), value);
	}

	inline static int32_t get_offset_of__tearDownSystems_3() { return static_cast<int32_t>(offsetof(Systems_t4C86DA84B6D6F50FF23B497DD599ED594274DEA9, ____tearDownSystems_3)); }
	inline List_1_t78F50ABA4688AD6A1255F66AB89EE8BAFD069913 * get__tearDownSystems_3() const { return ____tearDownSystems_3; }
	inline List_1_t78F50ABA4688AD6A1255F66AB89EE8BAFD069913 ** get_address_of__tearDownSystems_3() { return &____tearDownSystems_3; }
	inline void set__tearDownSystems_3(List_1_t78F50ABA4688AD6A1255F66AB89EE8BAFD069913 * value)
	{
		____tearDownSystems_3 = value;
		Il2CppCodeGenWriteBarrier((&____tearDownSystems_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMS_T4C86DA84B6D6F50FF23B497DD599ED594274DEA9_H
#ifndef ENVIRONMENTRENDERSYSTEM_TD87EA9C079CC43C3E28B6A225C0794A7CA1F72C8_H
#define ENVIRONMENTRENDERSYSTEM_TD87EA9C079CC43C3E28B6A225C0794A7CA1F72C8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnvironmentRenderSystem
struct  EnvironmentRenderSystem_tD87EA9C079CC43C3E28B6A225C0794A7CA1F72C8  : public RuntimeObject
{
public:
	// Zenject.DiContainer EnvironmentRenderSystem::_container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____container_0;
	// GameContext EnvironmentRenderSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_1;
	// UnityEngine.GameObject EnvironmentRenderSystem::_environment
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____environment_2;

public:
	inline static int32_t get_offset_of__container_0() { return static_cast<int32_t>(offsetof(EnvironmentRenderSystem_tD87EA9C079CC43C3E28B6A225C0794A7CA1F72C8, ____container_0)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__container_0() const { return ____container_0; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__container_0() { return &____container_0; }
	inline void set__container_0(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____container_0 = value;
		Il2CppCodeGenWriteBarrier((&____container_0), value);
	}

	inline static int32_t get_offset_of__context_1() { return static_cast<int32_t>(offsetof(EnvironmentRenderSystem_tD87EA9C079CC43C3E28B6A225C0794A7CA1F72C8, ____context_1)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_1() const { return ____context_1; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_1() { return &____context_1; }
	inline void set__context_1(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_1 = value;
		Il2CppCodeGenWriteBarrier((&____context_1), value);
	}

	inline static int32_t get_offset_of__environment_2() { return static_cast<int32_t>(offsetof(EnvironmentRenderSystem_tD87EA9C079CC43C3E28B6A225C0794A7CA1F72C8, ____environment_2)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__environment_2() const { return ____environment_2; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__environment_2() { return &____environment_2; }
	inline void set__environment_2(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____environment_2 = value;
		Il2CppCodeGenWriteBarrier((&____environment_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENVIRONMENTRENDERSYSTEM_TD87EA9C079CC43C3E28B6A225C0794A7CA1F72C8_H
#ifndef FXSOUNDCOMPONENT_T67B072AC87CD6EDDE23219727D01789C036D6600_H
#define FXSOUNDCOMPONENT_T67B072AC87CD6EDDE23219727D01789C036D6600_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FXSoundComponent
struct  FXSoundComponent_t67B072AC87CD6EDDE23219727D01789C036D6600  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FXSOUNDCOMPONENT_T67B072AC87CD6EDDE23219727D01789C036D6600_H
#ifndef ILIBRARYCOMPONENT_T7F3699C9BF31B88877F93D66CFB44371CD296735_H
#define ILIBRARYCOMPONENT_T7F3699C9BF31B88877F93D66CFB44371CD296735_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ILibraryComponent
struct  ILibraryComponent_t7F3699C9BF31B88877F93D66CFB44371CD296735  : public RuntimeObject
{
public:
	// Tayr.ILibrary ILibraryComponent::Library
	RuntimeObject* ___Library_0;

public:
	inline static int32_t get_offset_of_Library_0() { return static_cast<int32_t>(offsetof(ILibraryComponent_t7F3699C9BF31B88877F93D66CFB44371CD296735, ___Library_0)); }
	inline RuntimeObject* get_Library_0() const { return ___Library_0; }
	inline RuntimeObject** get_address_of_Library_0() { return &___Library_0; }
	inline void set_Library_0(RuntimeObject* value)
	{
		___Library_0 = value;
		Il2CppCodeGenWriteBarrier((&___Library_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ILIBRARYCOMPONENT_T7F3699C9BF31B88877F93D66CFB44371CD296735_H
#ifndef INVENTORYRENDERSYSTEM_TC845B5274539DEBA6E8834FCA4E8D03E6A79060C_H
#define INVENTORYRENDERSYSTEM_TC845B5274539DEBA6E8834FCA4E8D03E6A79060C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InventoryRenderSystem
struct  InventoryRenderSystem_tC845B5274539DEBA6E8834FCA4E8D03E6A79060C  : public RuntimeObject
{
public:
	// GameSettingsSO InventoryRenderSystem::_gameSettings
	GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C * ____gameSettings_0;
	// Entitas.IGroup`1<GameEntity> InventoryRenderSystem::_entities
	RuntimeObject* ____entities_1;
	// GameContext InventoryRenderSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_2;

public:
	inline static int32_t get_offset_of__gameSettings_0() { return static_cast<int32_t>(offsetof(InventoryRenderSystem_tC845B5274539DEBA6E8834FCA4E8D03E6A79060C, ____gameSettings_0)); }
	inline GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C * get__gameSettings_0() const { return ____gameSettings_0; }
	inline GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C ** get_address_of__gameSettings_0() { return &____gameSettings_0; }
	inline void set__gameSettings_0(GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C * value)
	{
		____gameSettings_0 = value;
		Il2CppCodeGenWriteBarrier((&____gameSettings_0), value);
	}

	inline static int32_t get_offset_of__entities_1() { return static_cast<int32_t>(offsetof(InventoryRenderSystem_tC845B5274539DEBA6E8834FCA4E8D03E6A79060C, ____entities_1)); }
	inline RuntimeObject* get__entities_1() const { return ____entities_1; }
	inline RuntimeObject** get_address_of__entities_1() { return &____entities_1; }
	inline void set__entities_1(RuntimeObject* value)
	{
		____entities_1 = value;
		Il2CppCodeGenWriteBarrier((&____entities_1), value);
	}

	inline static int32_t get_offset_of__context_2() { return static_cast<int32_t>(offsetof(InventoryRenderSystem_tC845B5274539DEBA6E8834FCA4E8D03E6A79060C, ____context_2)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_2() const { return ____context_2; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_2() { return &____context_2; }
	inline void set__context_2(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_2 = value;
		Il2CppCodeGenWriteBarrier((&____context_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVENTORYRENDERSYSTEM_TC845B5274539DEBA6E8834FCA4E8D03E6A79060C_H
#ifndef LEVELCOMPONENT_T682483027DD67BDF5D6912C2BB9D1F0469A2C644_H
#define LEVELCOMPONENT_T682483027DD67BDF5D6912C2BB9D1F0469A2C644_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LevelComponent
struct  LevelComponent_t682483027DD67BDF5D6912C2BB9D1F0469A2C644  : public RuntimeObject
{
public:
	// LevelModel LevelComponent::LevelModel
	LevelModel_t5549E807391907F7844D01A4FB4004330AF5E05C * ___LevelModel_0;

public:
	inline static int32_t get_offset_of_LevelModel_0() { return static_cast<int32_t>(offsetof(LevelComponent_t682483027DD67BDF5D6912C2BB9D1F0469A2C644, ___LevelModel_0)); }
	inline LevelModel_t5549E807391907F7844D01A4FB4004330AF5E05C * get_LevelModel_0() const { return ___LevelModel_0; }
	inline LevelModel_t5549E807391907F7844D01A4FB4004330AF5E05C ** get_address_of_LevelModel_0() { return &___LevelModel_0; }
	inline void set_LevelModel_0(LevelModel_t5549E807391907F7844D01A4FB4004330AF5E05C * value)
	{
		___LevelModel_0 = value;
		Il2CppCodeGenWriteBarrier((&___LevelModel_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELCOMPONENT_T682483027DD67BDF5D6912C2BB9D1F0469A2C644_H
#ifndef LOSESUMMARYNODEDATA_T9807706F230D328167B227F082922DBD1C0BAA36_H
#define LOSESUMMARYNODEDATA_T9807706F230D328167B227F082922DBD1C0BAA36_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoseSummaryNodeData
struct  LoseSummaryNodeData_t9807706F230D328167B227F082922DBD1C0BAA36  : public RuntimeObject
{
public:
	// System.Int32 LoseSummaryNodeData::Level
	int32_t ___Level_0;

public:
	inline static int32_t get_offset_of_Level_0() { return static_cast<int32_t>(offsetof(LoseSummaryNodeData_t9807706F230D328167B227F082922DBD1C0BAA36, ___Level_0)); }
	inline int32_t get_Level_0() const { return ___Level_0; }
	inline int32_t* get_address_of_Level_0() { return &___Level_0; }
	inline void set_Level_0(int32_t value)
	{
		___Level_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOSESUMMARYNODEDATA_T9807706F230D328167B227F082922DBD1C0BAA36_H
#ifndef MUSICSOUNDCOMPONENT_TB975365C7BD5C382B6D1685AA770668ED79096C4_H
#define MUSICSOUNDCOMPONENT_TB975365C7BD5C382B6D1685AA770668ED79096C4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MusicSoundComponent
struct  MusicSoundComponent_tB975365C7BD5C382B6D1685AA770668ED79096C4  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MUSICSOUNDCOMPONENT_TB975365C7BD5C382B6D1685AA770668ED79096C4_H
#ifndef REMOVEVIEWONDISPOSECOMPONENT_TE54A9A8E3DB4158A7D2E649412FEACD20965E449_H
#define REMOVEVIEWONDISPOSECOMPONENT_TE54A9A8E3DB4158A7D2E649412FEACD20965E449_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RemoveViewOnDisposeComponent
struct  RemoveViewOnDisposeComponent_tE54A9A8E3DB4158A7D2E649412FEACD20965E449  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOVEVIEWONDISPOSECOMPONENT_TE54A9A8E3DB4158A7D2E649412FEACD20965E449_H
#ifndef SCREENTAPCOMPONENT_T3A70AEB8C394143D61F292E42709115ED4CA70EE_H
#define SCREENTAPCOMPONENT_T3A70AEB8C394143D61F292E42709115ED4CA70EE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScreenTapComponent
struct  ScreenTapComponent_t3A70AEB8C394143D61F292E42709115ED4CA70EE  : public RuntimeObject
{
public:
	// System.String ScreenTapComponent::SpellId
	String_t* ___SpellId_0;

public:
	inline static int32_t get_offset_of_SpellId_0() { return static_cast<int32_t>(offsetof(ScreenTapComponent_t3A70AEB8C394143D61F292E42709115ED4CA70EE, ___SpellId_0)); }
	inline String_t* get_SpellId_0() const { return ___SpellId_0; }
	inline String_t** get_address_of_SpellId_0() { return &___SpellId_0; }
	inline void set_SpellId_0(String_t* value)
	{
		___SpellId_0 = value;
		Il2CppCodeGenWriteBarrier((&___SpellId_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENTAPCOMPONENT_T3A70AEB8C394143D61F292E42709115ED4CA70EE_H
#ifndef SOUNDCOMPONENT_T19B56A885393FE7EC0BFF4A9AEF609C240983D7C_H
#define SOUNDCOMPONENT_T19B56A885393FE7EC0BFF4A9AEF609C240983D7C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SoundComponent
struct  SoundComponent_t19B56A885393FE7EC0BFF4A9AEF609C240983D7C  : public RuntimeObject
{
public:
	// System.String SoundComponent::ClipName
	String_t* ___ClipName_0;

public:
	inline static int32_t get_offset_of_ClipName_0() { return static_cast<int32_t>(offsetof(SoundComponent_t19B56A885393FE7EC0BFF4A9AEF609C240983D7C, ___ClipName_0)); }
	inline String_t* get_ClipName_0() const { return ___ClipName_0; }
	inline String_t** get_address_of_ClipName_0() { return &___ClipName_0; }
	inline void set_ClipName_0(String_t* value)
	{
		___ClipName_0 = value;
		Il2CppCodeGenWriteBarrier((&___ClipName_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOUNDCOMPONENT_T19B56A885393FE7EC0BFF4A9AEF609C240983D7C_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef TIMEPRESSURERENDERSYSTEM_T3B24E02FD278B6407D9DA3F5FD176151B80979F5_H
#define TIMEPRESSURERENDERSYSTEM_T3B24E02FD278B6407D9DA3F5FD176151B80979F5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TimePressureRenderSystem
struct  TimePressureRenderSystem_t3B24E02FD278B6407D9DA3F5FD176151B80979F5  : public RuntimeObject
{
public:
	// GameContext TimePressureRenderSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_0;

public:
	inline static int32_t get_offset_of__context_0() { return static_cast<int32_t>(offsetof(TimePressureRenderSystem_t3B24E02FD278B6407D9DA3F5FD176151B80979F5, ____context_0)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_0() const { return ____context_0; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_0() { return &____context_0; }
	inline void set__context_0(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_0 = value;
		Il2CppCodeGenWriteBarrier((&____context_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMEPRESSURERENDERSYSTEM_T3B24E02FD278B6407D9DA3F5FD176151B80979F5_H
#ifndef TOWERHPBARCOMPONENT_T40985B862AAE62ABB3F2C686D354A7BD548552E1_H
#define TOWERHPBARCOMPONENT_T40985B862AAE62ABB3F2C686D354A7BD548552E1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TowerHPBarComponent
struct  TowerHPBarComponent_t40985B862AAE62ABB3F2C686D354A7BD548552E1  : public RuntimeObject
{
public:
	// UnityEngine.GameObject TowerHPBarComponent::HPBar
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___HPBar_0;
	// UnityEngine.UI.Image TowerHPBarComponent::ProgressBar
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___ProgressBar_1;

public:
	inline static int32_t get_offset_of_HPBar_0() { return static_cast<int32_t>(offsetof(TowerHPBarComponent_t40985B862AAE62ABB3F2C686D354A7BD548552E1, ___HPBar_0)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_HPBar_0() const { return ___HPBar_0; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_HPBar_0() { return &___HPBar_0; }
	inline void set_HPBar_0(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___HPBar_0 = value;
		Il2CppCodeGenWriteBarrier((&___HPBar_0), value);
	}

	inline static int32_t get_offset_of_ProgressBar_1() { return static_cast<int32_t>(offsetof(TowerHPBarComponent_t40985B862AAE62ABB3F2C686D354A7BD548552E1, ___ProgressBar_1)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_ProgressBar_1() const { return ___ProgressBar_1; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_ProgressBar_1() { return &___ProgressBar_1; }
	inline void set_ProgressBar_1(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___ProgressBar_1 = value;
		Il2CppCodeGenWriteBarrier((&___ProgressBar_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOWERHPBARCOMPONENT_T40985B862AAE62ABB3F2C686D354A7BD548552E1_H
#ifndef TOWERHEALINGRENDERSYSTEM_T55266A2DC95393BF6E1E9E867D7E1E9A64CC23E4_H
#define TOWERHEALINGRENDERSYSTEM_T55266A2DC95393BF6E1E9E867D7E1E9A64CC23E4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TowerHealingRenderSystem
struct  TowerHealingRenderSystem_t55266A2DC95393BF6E1E9E867D7E1E9A64CC23E4  : public RuntimeObject
{
public:
	// GameContext TowerHealingRenderSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_0;
	// Entitas.IGroup`1<GameEntity> TowerHealingRenderSystem::_entities
	RuntimeObject* ____entities_1;

public:
	inline static int32_t get_offset_of__context_0() { return static_cast<int32_t>(offsetof(TowerHealingRenderSystem_t55266A2DC95393BF6E1E9E867D7E1E9A64CC23E4, ____context_0)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_0() const { return ____context_0; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_0() { return &____context_0; }
	inline void set__context_0(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_0 = value;
		Il2CppCodeGenWriteBarrier((&____context_0), value);
	}

	inline static int32_t get_offset_of__entities_1() { return static_cast<int32_t>(offsetof(TowerHealingRenderSystem_t55266A2DC95393BF6E1E9E867D7E1E9A64CC23E4, ____entities_1)); }
	inline RuntimeObject* get__entities_1() const { return ____entities_1; }
	inline RuntimeObject** get_address_of__entities_1() { return &____entities_1; }
	inline void set__entities_1(RuntimeObject* value)
	{
		____entities_1 = value;
		Il2CppCodeGenWriteBarrier((&____entities_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOWERHEALINGRENDERSYSTEM_T55266A2DC95393BF6E1E9E867D7E1E9A64CC23E4_H
#ifndef TOWERRENDERCOMPONENT_T86F5EE9B65D02400E2F4532577BD5416D86FC19E_H
#define TOWERRENDERCOMPONENT_T86F5EE9B65D02400E2F4532577BD5416D86FC19E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TowerRenderComponent
struct  TowerRenderComponent_t86F5EE9B65D02400E2F4532577BD5416D86FC19E  : public RuntimeObject
{
public:
	// UnityEngine.GameObject TowerRenderComponent::Idle
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___Idle_0;
	// UnityEngine.GameObject TowerRenderComponent::Destroyed
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___Destroyed_1;

public:
	inline static int32_t get_offset_of_Idle_0() { return static_cast<int32_t>(offsetof(TowerRenderComponent_t86F5EE9B65D02400E2F4532577BD5416D86FC19E, ___Idle_0)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_Idle_0() const { return ___Idle_0; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_Idle_0() { return &___Idle_0; }
	inline void set_Idle_0(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___Idle_0 = value;
		Il2CppCodeGenWriteBarrier((&___Idle_0), value);
	}

	inline static int32_t get_offset_of_Destroyed_1() { return static_cast<int32_t>(offsetof(TowerRenderComponent_t86F5EE9B65D02400E2F4532577BD5416D86FC19E, ___Destroyed_1)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_Destroyed_1() const { return ___Destroyed_1; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_Destroyed_1() { return &___Destroyed_1; }
	inline void set_Destroyed_1(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___Destroyed_1 = value;
		Il2CppCodeGenWriteBarrier((&___Destroyed_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOWERRENDERCOMPONENT_T86F5EE9B65D02400E2F4532577BD5416D86FC19E_H
#ifndef TOWERROTATIONSYSTEM_T4A572C980B80BBB1BF97336C685C7A5213C27102_H
#define TOWERROTATIONSYSTEM_T4A572C980B80BBB1BF97336C685C7A5213C27102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TowerRotationSystem
struct  TowerRotationSystem_t4A572C980B80BBB1BF97336C685C7A5213C27102  : public RuntimeObject
{
public:
	// GameContext TowerRotationSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_0;
	// Entitas.IGroup`1<GameEntity> TowerRotationSystem::_entities
	RuntimeObject* ____entities_1;

public:
	inline static int32_t get_offset_of__context_0() { return static_cast<int32_t>(offsetof(TowerRotationSystem_t4A572C980B80BBB1BF97336C685C7A5213C27102, ____context_0)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_0() const { return ____context_0; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_0() { return &____context_0; }
	inline void set__context_0(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_0 = value;
		Il2CppCodeGenWriteBarrier((&____context_0), value);
	}

	inline static int32_t get_offset_of__entities_1() { return static_cast<int32_t>(offsetof(TowerRotationSystem_t4A572C980B80BBB1BF97336C685C7A5213C27102, ____entities_1)); }
	inline RuntimeObject* get__entities_1() const { return ____entities_1; }
	inline RuntimeObject** get_address_of__entities_1() { return &____entities_1; }
	inline void set__entities_1(RuntimeObject* value)
	{
		____entities_1 = value;
		Il2CppCodeGenWriteBarrier((&____entities_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOWERROTATIONSYSTEM_T4A572C980B80BBB1BF97336C685C7A5213C27102_H
#ifndef TOWERTAPCOMPONENT_TCE28812FC562A2930967569187D82DD8D4DE70D6_H
#define TOWERTAPCOMPONENT_TCE28812FC562A2930967569187D82DD8D4DE70D6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TowerTapComponent
struct  TowerTapComponent_tCE28812FC562A2930967569187D82DD8D4DE70D6  : public RuntimeObject
{
public:
	// System.String TowerTapComponent::SpellId
	String_t* ___SpellId_0;
	// System.String TowerTapComponent::TowerId
	String_t* ___TowerId_1;

public:
	inline static int32_t get_offset_of_SpellId_0() { return static_cast<int32_t>(offsetof(TowerTapComponent_tCE28812FC562A2930967569187D82DD8D4DE70D6, ___SpellId_0)); }
	inline String_t* get_SpellId_0() const { return ___SpellId_0; }
	inline String_t** get_address_of_SpellId_0() { return &___SpellId_0; }
	inline void set_SpellId_0(String_t* value)
	{
		___SpellId_0 = value;
		Il2CppCodeGenWriteBarrier((&___SpellId_0), value);
	}

	inline static int32_t get_offset_of_TowerId_1() { return static_cast<int32_t>(offsetof(TowerTapComponent_tCE28812FC562A2930967569187D82DD8D4DE70D6, ___TowerId_1)); }
	inline String_t* get_TowerId_1() const { return ___TowerId_1; }
	inline String_t** get_address_of_TowerId_1() { return &___TowerId_1; }
	inline void set_TowerId_1(String_t* value)
	{
		___TowerId_1 = value;
		Il2CppCodeGenWriteBarrier((&___TowerId_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOWERTAPCOMPONENT_TCE28812FC562A2930967569187D82DD8D4DE70D6_H
#ifndef TOWERTOPCOMPONENT_TF55EF7BFBA8765EE56500584F133C6ACC3119A82_H
#define TOWERTOPCOMPONENT_TF55EF7BFBA8765EE56500584F133C6ACC3119A82_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TowerTopComponent
struct  TowerTopComponent_tF55EF7BFBA8765EE56500584F133C6ACC3119A82  : public RuntimeObject
{
public:
	// UnityEngine.GameObject TowerTopComponent::TowerTop
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___TowerTop_0;

public:
	inline static int32_t get_offset_of_TowerTop_0() { return static_cast<int32_t>(offsetof(TowerTopComponent_tF55EF7BFBA8765EE56500584F133C6ACC3119A82, ___TowerTop_0)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_TowerTop_0() const { return ___TowerTop_0; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_TowerTop_0() { return &___TowerTop_0; }
	inline void set_TowerTop_0(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___TowerTop_0 = value;
		Il2CppCodeGenWriteBarrier((&___TowerTop_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOWERTOPCOMPONENT_TF55EF7BFBA8765EE56500584F133C6ACC3119A82_H
#ifndef UIHOLDERCOMPONENT_T77C1FEDC4FF8369CC3E81B337440D5561A64857C_H
#define UIHOLDERCOMPONENT_T77C1FEDC4FF8369CC3E81B337440D5561A64857C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIHolderComponent
struct  UIHolderComponent_t77C1FEDC4FF8369CC3E81B337440D5561A64857C  : public RuntimeObject
{
public:
	// UnityEngine.GameObject UIHolderComponent::GameObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___GameObject_0;

public:
	inline static int32_t get_offset_of_GameObject_0() { return static_cast<int32_t>(offsetof(UIHolderComponent_t77C1FEDC4FF8369CC3E81B337440D5561A64857C, ___GameObject_0)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_GameObject_0() const { return ___GameObject_0; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_GameObject_0() { return &___GameObject_0; }
	inline void set_GameObject_0(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___GameObject_0 = value;
		Il2CppCodeGenWriteBarrier((&___GameObject_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIHOLDERCOMPONENT_T77C1FEDC4FF8369CC3E81B337440D5561A64857C_H
#ifndef VIEWCOMPONENT_T89247AC078C9D4A67D4EE820ED67BBF71F84904A_H
#define VIEWCOMPONENT_T89247AC078C9D4A67D4EE820ED67BBF71F84904A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ViewComponent
struct  ViewComponent_t89247AC078C9D4A67D4EE820ED67BBF71F84904A  : public RuntimeObject
{
public:
	// UnityEngine.GameObject ViewComponent::GameObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___GameObject_0;

public:
	inline static int32_t get_offset_of_GameObject_0() { return static_cast<int32_t>(offsetof(ViewComponent_t89247AC078C9D4A67D4EE820ED67BBF71F84904A, ___GameObject_0)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_GameObject_0() const { return ___GameObject_0; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_GameObject_0() { return &___GameObject_0; }
	inline void set_GameObject_0(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___GameObject_0 = value;
		Il2CppCodeGenWriteBarrier((&___GameObject_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIEWCOMPONENT_T89247AC078C9D4A67D4EE820ED67BBF71F84904A_H
#ifndef WINSUMMARYNODEDATA_TD1C3F590D3B3A7D085A783296AD766C9DDA98DEE_H
#define WINSUMMARYNODEDATA_TD1C3F590D3B3A7D085A783296AD766C9DDA98DEE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WinSummaryNodeData
struct  WinSummaryNodeData_tD1C3F590D3B3A7D085A783296AD766C9DDA98DEE  : public RuntimeObject
{
public:
	// System.Int32 WinSummaryNodeData::Stars
	int32_t ___Stars_0;
	// System.Int32 WinSummaryNodeData::Score
	int32_t ___Score_1;
	// System.Int32 WinSummaryNodeData::Level
	int32_t ___Level_2;

public:
	inline static int32_t get_offset_of_Stars_0() { return static_cast<int32_t>(offsetof(WinSummaryNodeData_tD1C3F590D3B3A7D085A783296AD766C9DDA98DEE, ___Stars_0)); }
	inline int32_t get_Stars_0() const { return ___Stars_0; }
	inline int32_t* get_address_of_Stars_0() { return &___Stars_0; }
	inline void set_Stars_0(int32_t value)
	{
		___Stars_0 = value;
	}

	inline static int32_t get_offset_of_Score_1() { return static_cast<int32_t>(offsetof(WinSummaryNodeData_tD1C3F590D3B3A7D085A783296AD766C9DDA98DEE, ___Score_1)); }
	inline int32_t get_Score_1() const { return ___Score_1; }
	inline int32_t* get_address_of_Score_1() { return &___Score_1; }
	inline void set_Score_1(int32_t value)
	{
		___Score_1 = value;
	}

	inline static int32_t get_offset_of_Level_2() { return static_cast<int32_t>(offsetof(WinSummaryNodeData_tD1C3F590D3B3A7D085A783296AD766C9DDA98DEE, ___Level_2)); }
	inline int32_t get_Level_2() const { return ___Level_2; }
	inline int32_t* get_address_of_Level_2() { return &___Level_2; }
	inline void set_Level_2(int32_t value)
	{
		___Level_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WINSUMMARYNODEDATA_TD1C3F590D3B3A7D085A783296AD766C9DDA98DEE_H
#ifndef ADDGAMEOBJECTEFFECTRENDERSYSTEM_TBEC7DF7EC3C4CF0D8E90A1DC9884ECA5745A0139_H
#define ADDGAMEOBJECTEFFECTRENDERSYSTEM_TBEC7DF7EC3C4CF0D8E90A1DC9884ECA5745A0139_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AddGameObjectEffectRenderSystem
struct  AddGameObjectEffectRenderSystem_tBEC7DF7EC3C4CF0D8E90A1DC9884ECA5745A0139  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// UnitsSO AddGameObjectEffectRenderSystem::_unitsSO
	UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 * ____unitsSO_3;
	// EffectVisualSO AddGameObjectEffectRenderSystem::_effectVisualSO
	EffectVisualSO_t1C5856B5E821FD66A4C1BD41DB5194A5610E6FFC * ____effectVisualSO_4;
	// Zenject.DiContainer AddGameObjectEffectRenderSystem::_diContainer
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____diContainer_5;
	// GameContext AddGameObjectEffectRenderSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_6;

public:
	inline static int32_t get_offset_of__unitsSO_3() { return static_cast<int32_t>(offsetof(AddGameObjectEffectRenderSystem_tBEC7DF7EC3C4CF0D8E90A1DC9884ECA5745A0139, ____unitsSO_3)); }
	inline UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 * get__unitsSO_3() const { return ____unitsSO_3; }
	inline UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 ** get_address_of__unitsSO_3() { return &____unitsSO_3; }
	inline void set__unitsSO_3(UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 * value)
	{
		____unitsSO_3 = value;
		Il2CppCodeGenWriteBarrier((&____unitsSO_3), value);
	}

	inline static int32_t get_offset_of__effectVisualSO_4() { return static_cast<int32_t>(offsetof(AddGameObjectEffectRenderSystem_tBEC7DF7EC3C4CF0D8E90A1DC9884ECA5745A0139, ____effectVisualSO_4)); }
	inline EffectVisualSO_t1C5856B5E821FD66A4C1BD41DB5194A5610E6FFC * get__effectVisualSO_4() const { return ____effectVisualSO_4; }
	inline EffectVisualSO_t1C5856B5E821FD66A4C1BD41DB5194A5610E6FFC ** get_address_of__effectVisualSO_4() { return &____effectVisualSO_4; }
	inline void set__effectVisualSO_4(EffectVisualSO_t1C5856B5E821FD66A4C1BD41DB5194A5610E6FFC * value)
	{
		____effectVisualSO_4 = value;
		Il2CppCodeGenWriteBarrier((&____effectVisualSO_4), value);
	}

	inline static int32_t get_offset_of__diContainer_5() { return static_cast<int32_t>(offsetof(AddGameObjectEffectRenderSystem_tBEC7DF7EC3C4CF0D8E90A1DC9884ECA5745A0139, ____diContainer_5)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__diContainer_5() const { return ____diContainer_5; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__diContainer_5() { return &____diContainer_5; }
	inline void set__diContainer_5(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____diContainer_5 = value;
		Il2CppCodeGenWriteBarrier((&____diContainer_5), value);
	}

	inline static int32_t get_offset_of__context_6() { return static_cast<int32_t>(offsetof(AddGameObjectEffectRenderSystem_tBEC7DF7EC3C4CF0D8E90A1DC9884ECA5745A0139, ____context_6)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_6() const { return ____context_6; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_6() { return &____context_6; }
	inline void set__context_6(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_6 = value;
		Il2CppCodeGenWriteBarrier((&____context_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADDGAMEOBJECTEFFECTRENDERSYSTEM_TBEC7DF7EC3C4CF0D8E90A1DC9884ECA5745A0139_H
#ifndef ANIMATORTRIGGERSPELLRENDRERSYSTEM_T8FFF012295B09B58F09DDF13BEB99791FF234160_H
#define ANIMATORTRIGGERSPELLRENDRERSYSTEM_T8FFF012295B09B58F09DDF13BEB99791FF234160_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AnimatorTriggerSpellRendrerSystem
struct  AnimatorTriggerSpellRendrerSystem_t8FFF012295B09B58F09DDF13BEB99791FF234160  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// GameContext AnimatorTriggerSpellRendrerSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_3;

public:
	inline static int32_t get_offset_of__context_3() { return static_cast<int32_t>(offsetof(AnimatorTriggerSpellRendrerSystem_t8FFF012295B09B58F09DDF13BEB99791FF234160, ____context_3)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_3() const { return ____context_3; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_3() { return &____context_3; }
	inline void set__context_3(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_3 = value;
		Il2CppCodeGenWriteBarrier((&____context_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATORTRIGGERSPELLRENDRERSYSTEM_T8FFF012295B09B58F09DDF13BEB99791FF234160_H
#ifndef AOERENDERINITSYSTEM_T6F35F1B56581411162C3BAD09E52C51F356EC70C_H
#define AOERENDERINITSYSTEM_T6F35F1B56581411162C3BAD09E52C51F356EC70C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AoeRenderInitSystem
struct  AoeRenderInitSystem_t6F35F1B56581411162C3BAD09E52C51F356EC70C  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// Zenject.DiContainer AoeRenderInitSystem::_container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____container_3;
	// GameContext AoeRenderInitSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_4;

public:
	inline static int32_t get_offset_of__container_3() { return static_cast<int32_t>(offsetof(AoeRenderInitSystem_t6F35F1B56581411162C3BAD09E52C51F356EC70C, ____container_3)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__container_3() const { return ____container_3; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__container_3() { return &____container_3; }
	inline void set__container_3(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____container_3 = value;
		Il2CppCodeGenWriteBarrier((&____container_3), value);
	}

	inline static int32_t get_offset_of__context_4() { return static_cast<int32_t>(offsetof(AoeRenderInitSystem_t6F35F1B56581411162C3BAD09E52C51F356EC70C, ____context_4)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_4() const { return ____context_4; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_4() { return &____context_4; }
	inline void set__context_4(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_4 = value;
		Il2CppCodeGenWriteBarrier((&____context_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AOERENDERINITSYSTEM_T6F35F1B56581411162C3BAD09E52C51F356EC70C_H
#ifndef BATTLENODESTUTORIALSYSTEM_T739310FF08D20FAC4C461216CF6CB25B7F7960AF_H
#define BATTLENODESTUTORIALSYSTEM_T739310FF08D20FAC4C461216CF6CB25B7F7960AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BattleNodesTutorialSystem
struct  BattleNodesTutorialSystem_t739310FF08D20FAC4C461216CF6CB25B7F7960AF  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// Tayr.ILibrary BattleNodesTutorialSystem::_uiMain
	RuntimeObject* ____uiMain_3;
	// CustomTutorialSO BattleNodesTutorialSystem::_tutorialSO
	CustomTutorialSO_t45FC033F62BB26BC5595EAA9FEF1DF50BFA2A41D * ____tutorialSO_4;
	// Zenject.DiContainer BattleNodesTutorialSystem::_container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____container_5;
	// GameContext BattleNodesTutorialSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_6;

public:
	inline static int32_t get_offset_of__uiMain_3() { return static_cast<int32_t>(offsetof(BattleNodesTutorialSystem_t739310FF08D20FAC4C461216CF6CB25B7F7960AF, ____uiMain_3)); }
	inline RuntimeObject* get__uiMain_3() const { return ____uiMain_3; }
	inline RuntimeObject** get_address_of__uiMain_3() { return &____uiMain_3; }
	inline void set__uiMain_3(RuntimeObject* value)
	{
		____uiMain_3 = value;
		Il2CppCodeGenWriteBarrier((&____uiMain_3), value);
	}

	inline static int32_t get_offset_of__tutorialSO_4() { return static_cast<int32_t>(offsetof(BattleNodesTutorialSystem_t739310FF08D20FAC4C461216CF6CB25B7F7960AF, ____tutorialSO_4)); }
	inline CustomTutorialSO_t45FC033F62BB26BC5595EAA9FEF1DF50BFA2A41D * get__tutorialSO_4() const { return ____tutorialSO_4; }
	inline CustomTutorialSO_t45FC033F62BB26BC5595EAA9FEF1DF50BFA2A41D ** get_address_of__tutorialSO_4() { return &____tutorialSO_4; }
	inline void set__tutorialSO_4(CustomTutorialSO_t45FC033F62BB26BC5595EAA9FEF1DF50BFA2A41D * value)
	{
		____tutorialSO_4 = value;
		Il2CppCodeGenWriteBarrier((&____tutorialSO_4), value);
	}

	inline static int32_t get_offset_of__container_5() { return static_cast<int32_t>(offsetof(BattleNodesTutorialSystem_t739310FF08D20FAC4C461216CF6CB25B7F7960AF, ____container_5)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__container_5() const { return ____container_5; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__container_5() { return &____container_5; }
	inline void set__container_5(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____container_5 = value;
		Il2CppCodeGenWriteBarrier((&____container_5), value);
	}

	inline static int32_t get_offset_of__context_6() { return static_cast<int32_t>(offsetof(BattleNodesTutorialSystem_t739310FF08D20FAC4C461216CF6CB25B7F7960AF, ____context_6)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_6() const { return ____context_6; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_6() { return &____context_6; }
	inline void set__context_6(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_6 = value;
		Il2CppCodeGenWriteBarrier((&____context_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BATTLENODESTUTORIALSYSTEM_T739310FF08D20FAC4C461216CF6CB25B7F7960AF_H
#ifndef BATTLESOUNDSYSTEM_TE8CE33A9D5C192A05CE7218DEB71EA1D04647083_H
#define BATTLESOUNDSYSTEM_TE8CE33A9D5C192A05CE7218DEB71EA1D04647083_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BattleSoundSystem
struct  BattleSoundSystem_tE8CE33A9D5C192A05CE7218DEB71EA1D04647083  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// Tayr.ILibrary BattleSoundSystem::_uiMain
	RuntimeObject* ____uiMain_3;
	// Zenject.DiContainer BattleSoundSystem::_diContainer
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____diContainer_4;
	// TowersSO BattleSoundSystem::_towersSO
	TowersSO_t3B7DE821E5F8017950776EEFC42E680B361BCDF0 * ____towersSO_5;
	// Tayr.TSoundSystem BattleSoundSystem::_soundSystem
	TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511 * ____soundSystem_6;
	// GameContext BattleSoundSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_7;

public:
	inline static int32_t get_offset_of__uiMain_3() { return static_cast<int32_t>(offsetof(BattleSoundSystem_tE8CE33A9D5C192A05CE7218DEB71EA1D04647083, ____uiMain_3)); }
	inline RuntimeObject* get__uiMain_3() const { return ____uiMain_3; }
	inline RuntimeObject** get_address_of__uiMain_3() { return &____uiMain_3; }
	inline void set__uiMain_3(RuntimeObject* value)
	{
		____uiMain_3 = value;
		Il2CppCodeGenWriteBarrier((&____uiMain_3), value);
	}

	inline static int32_t get_offset_of__diContainer_4() { return static_cast<int32_t>(offsetof(BattleSoundSystem_tE8CE33A9D5C192A05CE7218DEB71EA1D04647083, ____diContainer_4)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__diContainer_4() const { return ____diContainer_4; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__diContainer_4() { return &____diContainer_4; }
	inline void set__diContainer_4(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____diContainer_4 = value;
		Il2CppCodeGenWriteBarrier((&____diContainer_4), value);
	}

	inline static int32_t get_offset_of__towersSO_5() { return static_cast<int32_t>(offsetof(BattleSoundSystem_tE8CE33A9D5C192A05CE7218DEB71EA1D04647083, ____towersSO_5)); }
	inline TowersSO_t3B7DE821E5F8017950776EEFC42E680B361BCDF0 * get__towersSO_5() const { return ____towersSO_5; }
	inline TowersSO_t3B7DE821E5F8017950776EEFC42E680B361BCDF0 ** get_address_of__towersSO_5() { return &____towersSO_5; }
	inline void set__towersSO_5(TowersSO_t3B7DE821E5F8017950776EEFC42E680B361BCDF0 * value)
	{
		____towersSO_5 = value;
		Il2CppCodeGenWriteBarrier((&____towersSO_5), value);
	}

	inline static int32_t get_offset_of__soundSystem_6() { return static_cast<int32_t>(offsetof(BattleSoundSystem_tE8CE33A9D5C192A05CE7218DEB71EA1D04647083, ____soundSystem_6)); }
	inline TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511 * get__soundSystem_6() const { return ____soundSystem_6; }
	inline TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511 ** get_address_of__soundSystem_6() { return &____soundSystem_6; }
	inline void set__soundSystem_6(TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511 * value)
	{
		____soundSystem_6 = value;
		Il2CppCodeGenWriteBarrier((&____soundSystem_6), value);
	}

	inline static int32_t get_offset_of__context_7() { return static_cast<int32_t>(offsetof(BattleSoundSystem_tE8CE33A9D5C192A05CE7218DEB71EA1D04647083, ____context_7)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_7() const { return ____context_7; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_7() { return &____context_7; }
	inline void set__context_7(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_7 = value;
		Il2CppCodeGenWriteBarrier((&____context_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BATTLESOUNDSYSTEM_TE8CE33A9D5C192A05CE7218DEB71EA1D04647083_H
#ifndef CAMERASHAKESYSTEM_TE06433F5F18F1EB0DD7FA6624804DCA50C0DFFF4_H
#define CAMERASHAKESYSTEM_TE06433F5F18F1EB0DD7FA6624804DCA50C0DFFF4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraShakeSystem
struct  CameraShakeSystem_tE06433F5F18F1EB0DD7FA6624804DCA50C0DFFF4  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// GameContext CameraShakeSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_3;

public:
	inline static int32_t get_offset_of__context_3() { return static_cast<int32_t>(offsetof(CameraShakeSystem_tE06433F5F18F1EB0DD7FA6624804DCA50C0DFFF4, ____context_3)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_3() const { return ____context_3; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_3() { return &____context_3; }
	inline void set__context_3(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_3 = value;
		Il2CppCodeGenWriteBarrier((&____context_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERASHAKESYSTEM_TE06433F5F18F1EB0DD7FA6624804DCA50C0DFFF4_H
#ifndef CARTRENDERSYSTEM_T7FE03BBE43DB4D1F93882FF37747FB540E310F90_H
#define CARTRENDERSYSTEM_T7FE03BBE43DB4D1F93882FF37747FB540E310F90_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CartRenderSystem
struct  CartRenderSystem_t7FE03BBE43DB4D1F93882FF37747FB540E310F90  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// Zenject.DiContainer CartRenderSystem::_diContainer
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____diContainer_3;
	// UnitsSO CartRenderSystem::_unitsSO
	UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 * ____unitsSO_4;
	// GameContext CartRenderSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_5;

public:
	inline static int32_t get_offset_of__diContainer_3() { return static_cast<int32_t>(offsetof(CartRenderSystem_t7FE03BBE43DB4D1F93882FF37747FB540E310F90, ____diContainer_3)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__diContainer_3() const { return ____diContainer_3; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__diContainer_3() { return &____diContainer_3; }
	inline void set__diContainer_3(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____diContainer_3 = value;
		Il2CppCodeGenWriteBarrier((&____diContainer_3), value);
	}

	inline static int32_t get_offset_of__unitsSO_4() { return static_cast<int32_t>(offsetof(CartRenderSystem_t7FE03BBE43DB4D1F93882FF37747FB540E310F90, ____unitsSO_4)); }
	inline UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 * get__unitsSO_4() const { return ____unitsSO_4; }
	inline UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 ** get_address_of__unitsSO_4() { return &____unitsSO_4; }
	inline void set__unitsSO_4(UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 * value)
	{
		____unitsSO_4 = value;
		Il2CppCodeGenWriteBarrier((&____unitsSO_4), value);
	}

	inline static int32_t get_offset_of__context_5() { return static_cast<int32_t>(offsetof(CartRenderSystem_t7FE03BBE43DB4D1F93882FF37747FB540E310F90, ____context_5)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_5() const { return ____context_5; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_5() { return &____context_5; }
	inline void set__context_5(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_5 = value;
		Il2CppCodeGenWriteBarrier((&____context_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CARTRENDERSYSTEM_T7FE03BBE43DB4D1F93882FF37747FB540E310F90_H
#ifndef COLLECTABLEKICKERWITHICONSYSTEM_TF454A93E6EE83153ABA0A04C9B9F0B8D5CAFF70C_H
#define COLLECTABLEKICKERWITHICONSYSTEM_TF454A93E6EE83153ABA0A04C9B9F0B8D5CAFF70C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CollectableKickerWithIconSystem
struct  CollectableKickerWithIconSystem_tF454A93E6EE83153ABA0A04C9B9F0B8D5CAFF70C  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// Tayr.ILibrary CollectableKickerWithIconSystem::_uiMain
	RuntimeObject* ____uiMain_3;
	// ItemsSO CollectableKickerWithIconSystem::_itemsSO
	ItemsSO_tD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A * ____itemsSO_4;
	// GameContext CollectableKickerWithIconSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_5;

public:
	inline static int32_t get_offset_of__uiMain_3() { return static_cast<int32_t>(offsetof(CollectableKickerWithIconSystem_tF454A93E6EE83153ABA0A04C9B9F0B8D5CAFF70C, ____uiMain_3)); }
	inline RuntimeObject* get__uiMain_3() const { return ____uiMain_3; }
	inline RuntimeObject** get_address_of__uiMain_3() { return &____uiMain_3; }
	inline void set__uiMain_3(RuntimeObject* value)
	{
		____uiMain_3 = value;
		Il2CppCodeGenWriteBarrier((&____uiMain_3), value);
	}

	inline static int32_t get_offset_of__itemsSO_4() { return static_cast<int32_t>(offsetof(CollectableKickerWithIconSystem_tF454A93E6EE83153ABA0A04C9B9F0B8D5CAFF70C, ____itemsSO_4)); }
	inline ItemsSO_tD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A * get__itemsSO_4() const { return ____itemsSO_4; }
	inline ItemsSO_tD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A ** get_address_of__itemsSO_4() { return &____itemsSO_4; }
	inline void set__itemsSO_4(ItemsSO_tD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A * value)
	{
		____itemsSO_4 = value;
		Il2CppCodeGenWriteBarrier((&____itemsSO_4), value);
	}

	inline static int32_t get_offset_of__context_5() { return static_cast<int32_t>(offsetof(CollectableKickerWithIconSystem_tF454A93E6EE83153ABA0A04C9B9F0B8D5CAFF70C, ____context_5)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_5() const { return ____context_5; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_5() { return &____context_5; }
	inline void set__context_5(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_5 = value;
		Il2CppCodeGenWriteBarrier((&____context_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTABLEKICKERWITHICONSYSTEM_TF454A93E6EE83153ABA0A04C9B9F0B8D5CAFF70C_H
#ifndef COLLECTABLERENDERSYSTEM_T0AF772D5F4376446D0485B0990A7D88D61A021C3_H
#define COLLECTABLERENDERSYSTEM_T0AF772D5F4376446D0485B0990A7D88D61A021C3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CollectableRenderSystem
struct  CollectableRenderSystem_t0AF772D5F4376446D0485B0990A7D88D61A021C3  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// Zenject.DiContainer CollectableRenderSystem::_diContainer
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____diContainer_3;
	// ItemsSO CollectableRenderSystem::_itemsSO
	ItemsSO_tD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A * ____itemsSO_4;
	// GameContext CollectableRenderSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_5;

public:
	inline static int32_t get_offset_of__diContainer_3() { return static_cast<int32_t>(offsetof(CollectableRenderSystem_t0AF772D5F4376446D0485B0990A7D88D61A021C3, ____diContainer_3)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__diContainer_3() const { return ____diContainer_3; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__diContainer_3() { return &____diContainer_3; }
	inline void set__diContainer_3(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____diContainer_3 = value;
		Il2CppCodeGenWriteBarrier((&____diContainer_3), value);
	}

	inline static int32_t get_offset_of__itemsSO_4() { return static_cast<int32_t>(offsetof(CollectableRenderSystem_t0AF772D5F4376446D0485B0990A7D88D61A021C3, ____itemsSO_4)); }
	inline ItemsSO_tD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A * get__itemsSO_4() const { return ____itemsSO_4; }
	inline ItemsSO_tD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A ** get_address_of__itemsSO_4() { return &____itemsSO_4; }
	inline void set__itemsSO_4(ItemsSO_tD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A * value)
	{
		____itemsSO_4 = value;
		Il2CppCodeGenWriteBarrier((&____itemsSO_4), value);
	}

	inline static int32_t get_offset_of__context_5() { return static_cast<int32_t>(offsetof(CollectableRenderSystem_t0AF772D5F4376446D0485B0990A7D88D61A021C3, ____context_5)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_5() const { return ____context_5; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_5() { return &____context_5; }
	inline void set__context_5(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_5 = value;
		Il2CppCodeGenWriteBarrier((&____context_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTABLERENDERSYSTEM_T0AF772D5F4376446D0485B0990A7D88D61A021C3_H
#ifndef DECOYRENDERSYSTEM_T5D143A72EC07CF0F462EB56C928617A3EB725C58_H
#define DECOYRENDERSYSTEM_T5D143A72EC07CF0F462EB56C928617A3EB725C58_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DecoyRenderSystem
struct  DecoyRenderSystem_t5D143A72EC07CF0F462EB56C928617A3EB725C58  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// Zenject.DiContainer DecoyRenderSystem::_diContainer
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____diContainer_3;
	// UnitsSO DecoyRenderSystem::_unitsSO
	UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 * ____unitsSO_4;
	// GameContext DecoyRenderSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_5;

public:
	inline static int32_t get_offset_of__diContainer_3() { return static_cast<int32_t>(offsetof(DecoyRenderSystem_t5D143A72EC07CF0F462EB56C928617A3EB725C58, ____diContainer_3)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__diContainer_3() const { return ____diContainer_3; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__diContainer_3() { return &____diContainer_3; }
	inline void set__diContainer_3(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____diContainer_3 = value;
		Il2CppCodeGenWriteBarrier((&____diContainer_3), value);
	}

	inline static int32_t get_offset_of__unitsSO_4() { return static_cast<int32_t>(offsetof(DecoyRenderSystem_t5D143A72EC07CF0F462EB56C928617A3EB725C58, ____unitsSO_4)); }
	inline UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 * get__unitsSO_4() const { return ____unitsSO_4; }
	inline UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 ** get_address_of__unitsSO_4() { return &____unitsSO_4; }
	inline void set__unitsSO_4(UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 * value)
	{
		____unitsSO_4 = value;
		Il2CppCodeGenWriteBarrier((&____unitsSO_4), value);
	}

	inline static int32_t get_offset_of__context_5() { return static_cast<int32_t>(offsetof(DecoyRenderSystem_t5D143A72EC07CF0F462EB56C928617A3EB725C58, ____context_5)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_5() const { return ____context_5; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_5() { return &____context_5; }
	inline void set__context_5(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_5 = value;
		Il2CppCodeGenWriteBarrier((&____context_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECOYRENDERSYSTEM_T5D143A72EC07CF0F462EB56C928617A3EB725C58_H
#ifndef DESTROYABLECALLOUTSSYSTEM_T466F9C65CEE33AD8D86C8F5EB1ACC16DD7D04145_H
#define DESTROYABLECALLOUTSSYSTEM_T466F9C65CEE33AD8D86C8F5EB1ACC16DD7D04145_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DestroyableCalloutsSystem
struct  DestroyableCalloutsSystem_t466F9C65CEE33AD8D86C8F5EB1ACC16DD7D04145  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// Tayr.ILibrary DestroyableCalloutsSystem::_uiMain
	RuntimeObject* ____uiMain_3;
	// CustomTutorialSO DestroyableCalloutsSystem::_tutorialSO
	CustomTutorialSO_t45FC033F62BB26BC5595EAA9FEF1DF50BFA2A41D * ____tutorialSO_4;
	// GameContext DestroyableCalloutsSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_5;
	// System.Int32 DestroyableCalloutsSystem::_maxCallouts
	int32_t ____maxCallouts_6;
	// CustomTutorialModel DestroyableCalloutsSystem::_tutorialModel
	CustomTutorialModel_t7F1E6EF5B6AF58AEA9FB2573811B4562E587E635 * ____tutorialModel_7;
	// System.Boolean DestroyableCalloutsSystem::_isTriggered
	bool ____isTriggered_8;

public:
	inline static int32_t get_offset_of__uiMain_3() { return static_cast<int32_t>(offsetof(DestroyableCalloutsSystem_t466F9C65CEE33AD8D86C8F5EB1ACC16DD7D04145, ____uiMain_3)); }
	inline RuntimeObject* get__uiMain_3() const { return ____uiMain_3; }
	inline RuntimeObject** get_address_of__uiMain_3() { return &____uiMain_3; }
	inline void set__uiMain_3(RuntimeObject* value)
	{
		____uiMain_3 = value;
		Il2CppCodeGenWriteBarrier((&____uiMain_3), value);
	}

	inline static int32_t get_offset_of__tutorialSO_4() { return static_cast<int32_t>(offsetof(DestroyableCalloutsSystem_t466F9C65CEE33AD8D86C8F5EB1ACC16DD7D04145, ____tutorialSO_4)); }
	inline CustomTutorialSO_t45FC033F62BB26BC5595EAA9FEF1DF50BFA2A41D * get__tutorialSO_4() const { return ____tutorialSO_4; }
	inline CustomTutorialSO_t45FC033F62BB26BC5595EAA9FEF1DF50BFA2A41D ** get_address_of__tutorialSO_4() { return &____tutorialSO_4; }
	inline void set__tutorialSO_4(CustomTutorialSO_t45FC033F62BB26BC5595EAA9FEF1DF50BFA2A41D * value)
	{
		____tutorialSO_4 = value;
		Il2CppCodeGenWriteBarrier((&____tutorialSO_4), value);
	}

	inline static int32_t get_offset_of__context_5() { return static_cast<int32_t>(offsetof(DestroyableCalloutsSystem_t466F9C65CEE33AD8D86C8F5EB1ACC16DD7D04145, ____context_5)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_5() const { return ____context_5; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_5() { return &____context_5; }
	inline void set__context_5(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_5 = value;
		Il2CppCodeGenWriteBarrier((&____context_5), value);
	}

	inline static int32_t get_offset_of__maxCallouts_6() { return static_cast<int32_t>(offsetof(DestroyableCalloutsSystem_t466F9C65CEE33AD8D86C8F5EB1ACC16DD7D04145, ____maxCallouts_6)); }
	inline int32_t get__maxCallouts_6() const { return ____maxCallouts_6; }
	inline int32_t* get_address_of__maxCallouts_6() { return &____maxCallouts_6; }
	inline void set__maxCallouts_6(int32_t value)
	{
		____maxCallouts_6 = value;
	}

	inline static int32_t get_offset_of__tutorialModel_7() { return static_cast<int32_t>(offsetof(DestroyableCalloutsSystem_t466F9C65CEE33AD8D86C8F5EB1ACC16DD7D04145, ____tutorialModel_7)); }
	inline CustomTutorialModel_t7F1E6EF5B6AF58AEA9FB2573811B4562E587E635 * get__tutorialModel_7() const { return ____tutorialModel_7; }
	inline CustomTutorialModel_t7F1E6EF5B6AF58AEA9FB2573811B4562E587E635 ** get_address_of__tutorialModel_7() { return &____tutorialModel_7; }
	inline void set__tutorialModel_7(CustomTutorialModel_t7F1E6EF5B6AF58AEA9FB2573811B4562E587E635 * value)
	{
		____tutorialModel_7 = value;
		Il2CppCodeGenWriteBarrier((&____tutorialModel_7), value);
	}

	inline static int32_t get_offset_of__isTriggered_8() { return static_cast<int32_t>(offsetof(DestroyableCalloutsSystem_t466F9C65CEE33AD8D86C8F5EB1ACC16DD7D04145, ____isTriggered_8)); }
	inline bool get__isTriggered_8() const { return ____isTriggered_8; }
	inline bool* get_address_of__isTriggered_8() { return &____isTriggered_8; }
	inline void set__isTriggered_8(bool value)
	{
		____isTriggered_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESTROYABLECALLOUTSSYSTEM_T466F9C65CEE33AD8D86C8F5EB1ACC16DD7D04145_H
#ifndef DESTROYABLEKICKERWITHICONSYSTEM_T801EFA2C6EB88A29A1678565BB8E03BB23A8370B_H
#define DESTROYABLEKICKERWITHICONSYSTEM_T801EFA2C6EB88A29A1678565BB8E03BB23A8370B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DestroyableKickerWithIconSystem
struct  DestroyableKickerWithIconSystem_t801EFA2C6EB88A29A1678565BB8E03BB23A8370B  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// Tayr.ILibrary DestroyableKickerWithIconSystem::_uiMain
	RuntimeObject* ____uiMain_3;
	// BattleSettingsSO DestroyableKickerWithIconSystem::_battleSettingsSO
	BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE * ____battleSettingsSO_4;
	// ItemsSO DestroyableKickerWithIconSystem::_itemsSO
	ItemsSO_tD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A * ____itemsSO_5;
	// GameContext DestroyableKickerWithIconSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_6;

public:
	inline static int32_t get_offset_of__uiMain_3() { return static_cast<int32_t>(offsetof(DestroyableKickerWithIconSystem_t801EFA2C6EB88A29A1678565BB8E03BB23A8370B, ____uiMain_3)); }
	inline RuntimeObject* get__uiMain_3() const { return ____uiMain_3; }
	inline RuntimeObject** get_address_of__uiMain_3() { return &____uiMain_3; }
	inline void set__uiMain_3(RuntimeObject* value)
	{
		____uiMain_3 = value;
		Il2CppCodeGenWriteBarrier((&____uiMain_3), value);
	}

	inline static int32_t get_offset_of__battleSettingsSO_4() { return static_cast<int32_t>(offsetof(DestroyableKickerWithIconSystem_t801EFA2C6EB88A29A1678565BB8E03BB23A8370B, ____battleSettingsSO_4)); }
	inline BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE * get__battleSettingsSO_4() const { return ____battleSettingsSO_4; }
	inline BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE ** get_address_of__battleSettingsSO_4() { return &____battleSettingsSO_4; }
	inline void set__battleSettingsSO_4(BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE * value)
	{
		____battleSettingsSO_4 = value;
		Il2CppCodeGenWriteBarrier((&____battleSettingsSO_4), value);
	}

	inline static int32_t get_offset_of__itemsSO_5() { return static_cast<int32_t>(offsetof(DestroyableKickerWithIconSystem_t801EFA2C6EB88A29A1678565BB8E03BB23A8370B, ____itemsSO_5)); }
	inline ItemsSO_tD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A * get__itemsSO_5() const { return ____itemsSO_5; }
	inline ItemsSO_tD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A ** get_address_of__itemsSO_5() { return &____itemsSO_5; }
	inline void set__itemsSO_5(ItemsSO_tD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A * value)
	{
		____itemsSO_5 = value;
		Il2CppCodeGenWriteBarrier((&____itemsSO_5), value);
	}

	inline static int32_t get_offset_of__context_6() { return static_cast<int32_t>(offsetof(DestroyableKickerWithIconSystem_t801EFA2C6EB88A29A1678565BB8E03BB23A8370B, ____context_6)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_6() const { return ____context_6; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_6() { return &____context_6; }
	inline void set__context_6(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_6 = value;
		Il2CppCodeGenWriteBarrier((&____context_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESTROYABLEKICKERWITHICONSYSTEM_T801EFA2C6EB88A29A1678565BB8E03BB23A8370B_H
#ifndef EFFECTMAINRENDRERSYSTEM_TB3D6F88868897C742DF7EC21A730E9117FE70FBE_H
#define EFFECTMAINRENDRERSYSTEM_TB3D6F88868897C742DF7EC21A730E9117FE70FBE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EffectMainRendrerSystem
struct  EffectMainRendrerSystem_tB3D6F88868897C742DF7EC21A730E9117FE70FBE  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// UnitsSO EffectMainRendrerSystem::_unitsSO
	UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 * ____unitsSO_3;
	// EffectVisualSO EffectMainRendrerSystem::_effectVisualSO
	EffectVisualSO_t1C5856B5E821FD66A4C1BD41DB5194A5610E6FFC * ____effectVisualSO_4;
	// GameContext EffectMainRendrerSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_5;

public:
	inline static int32_t get_offset_of__unitsSO_3() { return static_cast<int32_t>(offsetof(EffectMainRendrerSystem_tB3D6F88868897C742DF7EC21A730E9117FE70FBE, ____unitsSO_3)); }
	inline UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 * get__unitsSO_3() const { return ____unitsSO_3; }
	inline UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 ** get_address_of__unitsSO_3() { return &____unitsSO_3; }
	inline void set__unitsSO_3(UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 * value)
	{
		____unitsSO_3 = value;
		Il2CppCodeGenWriteBarrier((&____unitsSO_3), value);
	}

	inline static int32_t get_offset_of__effectVisualSO_4() { return static_cast<int32_t>(offsetof(EffectMainRendrerSystem_tB3D6F88868897C742DF7EC21A730E9117FE70FBE, ____effectVisualSO_4)); }
	inline EffectVisualSO_t1C5856B5E821FD66A4C1BD41DB5194A5610E6FFC * get__effectVisualSO_4() const { return ____effectVisualSO_4; }
	inline EffectVisualSO_t1C5856B5E821FD66A4C1BD41DB5194A5610E6FFC ** get_address_of__effectVisualSO_4() { return &____effectVisualSO_4; }
	inline void set__effectVisualSO_4(EffectVisualSO_t1C5856B5E821FD66A4C1BD41DB5194A5610E6FFC * value)
	{
		____effectVisualSO_4 = value;
		Il2CppCodeGenWriteBarrier((&____effectVisualSO_4), value);
	}

	inline static int32_t get_offset_of__context_5() { return static_cast<int32_t>(offsetof(EffectMainRendrerSystem_tB3D6F88868897C742DF7EC21A730E9117FE70FBE, ____context_5)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_5() const { return ____context_5; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_5() { return &____context_5; }
	inline void set__context_5(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_5 = value;
		Il2CppCodeGenWriteBarrier((&____context_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EFFECTMAINRENDRERSYSTEM_TB3D6F88868897C742DF7EC21A730E9117FE70FBE_H
#ifndef EFFECTRENDERDISPOSESYSTEM_T1DBF43E154539554F2E37A3D36798D627D0BBD4D_H
#define EFFECTRENDERDISPOSESYSTEM_T1DBF43E154539554F2E37A3D36798D627D0BBD4D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EffectRenderDisposeSystem
struct  EffectRenderDisposeSystem_t1DBF43E154539554F2E37A3D36798D627D0BBD4D  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// UnitsSO EffectRenderDisposeSystem::_unitsSO
	UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 * ____unitsSO_3;
	// EffectVisualSO EffectRenderDisposeSystem::_effectVisualSO
	EffectVisualSO_t1C5856B5E821FD66A4C1BD41DB5194A5610E6FFC * ____effectVisualSO_4;
	// Zenject.DiContainer EffectRenderDisposeSystem::_diContainer
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____diContainer_5;
	// GameContext EffectRenderDisposeSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_6;

public:
	inline static int32_t get_offset_of__unitsSO_3() { return static_cast<int32_t>(offsetof(EffectRenderDisposeSystem_t1DBF43E154539554F2E37A3D36798D627D0BBD4D, ____unitsSO_3)); }
	inline UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 * get__unitsSO_3() const { return ____unitsSO_3; }
	inline UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 ** get_address_of__unitsSO_3() { return &____unitsSO_3; }
	inline void set__unitsSO_3(UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 * value)
	{
		____unitsSO_3 = value;
		Il2CppCodeGenWriteBarrier((&____unitsSO_3), value);
	}

	inline static int32_t get_offset_of__effectVisualSO_4() { return static_cast<int32_t>(offsetof(EffectRenderDisposeSystem_t1DBF43E154539554F2E37A3D36798D627D0BBD4D, ____effectVisualSO_4)); }
	inline EffectVisualSO_t1C5856B5E821FD66A4C1BD41DB5194A5610E6FFC * get__effectVisualSO_4() const { return ____effectVisualSO_4; }
	inline EffectVisualSO_t1C5856B5E821FD66A4C1BD41DB5194A5610E6FFC ** get_address_of__effectVisualSO_4() { return &____effectVisualSO_4; }
	inline void set__effectVisualSO_4(EffectVisualSO_t1C5856B5E821FD66A4C1BD41DB5194A5610E6FFC * value)
	{
		____effectVisualSO_4 = value;
		Il2CppCodeGenWriteBarrier((&____effectVisualSO_4), value);
	}

	inline static int32_t get_offset_of__diContainer_5() { return static_cast<int32_t>(offsetof(EffectRenderDisposeSystem_t1DBF43E154539554F2E37A3D36798D627D0BBD4D, ____diContainer_5)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__diContainer_5() const { return ____diContainer_5; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__diContainer_5() { return &____diContainer_5; }
	inline void set__diContainer_5(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____diContainer_5 = value;
		Il2CppCodeGenWriteBarrier((&____diContainer_5), value);
	}

	inline static int32_t get_offset_of__context_6() { return static_cast<int32_t>(offsetof(EffectRenderDisposeSystem_t1DBF43E154539554F2E37A3D36798D627D0BBD4D, ____context_6)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_6() const { return ____context_6; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_6() { return &____context_6; }
	inline void set__context_6(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_6 = value;
		Il2CppCodeGenWriteBarrier((&____context_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EFFECTRENDERDISPOSESYSTEM_T1DBF43E154539554F2E37A3D36798D627D0BBD4D_H
#ifndef PRIMARYENTITYINDEX_2_T17175B118C000A97322D66364138713F2177167A_H
#define PRIMARYENTITYINDEX_2_T17175B118C000A97322D66364138713F2177167A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.PrimaryEntityIndex`2<GameEntity,System.String>
struct  PrimaryEntityIndex_2_t17175B118C000A97322D66364138713F2177167A  : public AbstractEntityIndex_2_t21CBBEC4E85C6B3DF1A910297A15C295BA77FBEA
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TEntity> Entitas.PrimaryEntityIndex`2::_index
	Dictionary_2_t70CAAE41AA21DBE49EE58820FA11623347BDFA5D * ____index_5;

public:
	inline static int32_t get_offset_of__index_5() { return static_cast<int32_t>(offsetof(PrimaryEntityIndex_2_t17175B118C000A97322D66364138713F2177167A, ____index_5)); }
	inline Dictionary_2_t70CAAE41AA21DBE49EE58820FA11623347BDFA5D * get__index_5() const { return ____index_5; }
	inline Dictionary_2_t70CAAE41AA21DBE49EE58820FA11623347BDFA5D ** get_address_of__index_5() { return &____index_5; }
	inline void set__index_5(Dictionary_2_t70CAAE41AA21DBE49EE58820FA11623347BDFA5D * value)
	{
		____index_5 = value;
		Il2CppCodeGenWriteBarrier((&____index_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRIMARYENTITYINDEX_2_T17175B118C000A97322D66364138713F2177167A_H
#ifndef FEATURE_T3D2CE4A6E0A21FB131F20D2F64B7A6C289F0CF0F_H
#define FEATURE_T3D2CE4A6E0A21FB131F20D2F64B7A6C289F0CF0F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Feature
struct  Feature_t3D2CE4A6E0A21FB131F20D2F64B7A6C289F0CF0F  : public Systems_t4C86DA84B6D6F50FF23B497DD599ED594274DEA9
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FEATURE_T3D2CE4A6E0A21FB131F20D2F64B7A6C289F0CF0F_H
#ifndef HEROANIMATIONSSYSTEM_T230B10CB4AE99718FFE8A2E896D800ADDF833422_H
#define HEROANIMATIONSSYSTEM_T230B10CB4AE99718FFE8A2E896D800ADDF833422_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HeroAnimationsSystem
struct  HeroAnimationsSystem_t230B10CB4AE99718FFE8A2E896D800ADDF833422  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// GameContext HeroAnimationsSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_3;

public:
	inline static int32_t get_offset_of__context_3() { return static_cast<int32_t>(offsetof(HeroAnimationsSystem_t230B10CB4AE99718FFE8A2E896D800ADDF833422, ____context_3)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_3() const { return ____context_3; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_3() { return &____context_3; }
	inline void set__context_3(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_3 = value;
		Il2CppCodeGenWriteBarrier((&____context_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEROANIMATIONSSYSTEM_T230B10CB4AE99718FFE8A2E896D800ADDF833422_H
#ifndef HEROCOLLECTEDRENDERSYSTEM_TB14E94CBCCB48058CF44396C6E363D162B804C71_H
#define HEROCOLLECTEDRENDERSYSTEM_TB14E94CBCCB48058CF44396C6E363D162B804C71_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HeroCollectedRenderSystem
struct  HeroCollectedRenderSystem_tB14E94CBCCB48058CF44396C6E363D162B804C71  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// Zenject.DiContainer HeroCollectedRenderSystem::_diContainer
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____diContainer_3;
	// ItemsSO HeroCollectedRenderSystem::_itemsSO
	ItemsSO_tD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A * ____itemsSO_4;
	// GameContext HeroCollectedRenderSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_5;

public:
	inline static int32_t get_offset_of__diContainer_3() { return static_cast<int32_t>(offsetof(HeroCollectedRenderSystem_tB14E94CBCCB48058CF44396C6E363D162B804C71, ____diContainer_3)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__diContainer_3() const { return ____diContainer_3; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__diContainer_3() { return &____diContainer_3; }
	inline void set__diContainer_3(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____diContainer_3 = value;
		Il2CppCodeGenWriteBarrier((&____diContainer_3), value);
	}

	inline static int32_t get_offset_of__itemsSO_4() { return static_cast<int32_t>(offsetof(HeroCollectedRenderSystem_tB14E94CBCCB48058CF44396C6E363D162B804C71, ____itemsSO_4)); }
	inline ItemsSO_tD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A * get__itemsSO_4() const { return ____itemsSO_4; }
	inline ItemsSO_tD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A ** get_address_of__itemsSO_4() { return &____itemsSO_4; }
	inline void set__itemsSO_4(ItemsSO_tD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A * value)
	{
		____itemsSO_4 = value;
		Il2CppCodeGenWriteBarrier((&____itemsSO_4), value);
	}

	inline static int32_t get_offset_of__context_5() { return static_cast<int32_t>(offsetof(HeroCollectedRenderSystem_tB14E94CBCCB48058CF44396C6E363D162B804C71, ____context_5)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_5() const { return ____context_5; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_5() { return &____context_5; }
	inline void set__context_5(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_5 = value;
		Il2CppCodeGenWriteBarrier((&____context_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEROCOLLECTEDRENDERSYSTEM_TB14E94CBCCB48058CF44396C6E363D162B804C71_H
#ifndef HEROHEALTHRENDERSYSTEM_T75C28A2A739FC9E3EC68C36C6C80C91A8241F960_H
#define HEROHEALTHRENDERSYSTEM_T75C28A2A739FC9E3EC68C36C6C80C91A8241F960_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HeroHealthRenderSystem
struct  HeroHealthRenderSystem_t75C28A2A739FC9E3EC68C36C6C80C91A8241F960  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// Tayr.ILibrary HeroHealthRenderSystem::_uiMain
	RuntimeObject* ____uiMain_3;
	// SoundSO HeroHealthRenderSystem::_soundSO
	SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73 * ____soundSO_4;
	// System.Single HeroHealthRenderSystem::_lastTimeSound
	float ____lastTimeSound_5;
	// GameContext HeroHealthRenderSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_6;

public:
	inline static int32_t get_offset_of__uiMain_3() { return static_cast<int32_t>(offsetof(HeroHealthRenderSystem_t75C28A2A739FC9E3EC68C36C6C80C91A8241F960, ____uiMain_3)); }
	inline RuntimeObject* get__uiMain_3() const { return ____uiMain_3; }
	inline RuntimeObject** get_address_of__uiMain_3() { return &____uiMain_3; }
	inline void set__uiMain_3(RuntimeObject* value)
	{
		____uiMain_3 = value;
		Il2CppCodeGenWriteBarrier((&____uiMain_3), value);
	}

	inline static int32_t get_offset_of__soundSO_4() { return static_cast<int32_t>(offsetof(HeroHealthRenderSystem_t75C28A2A739FC9E3EC68C36C6C80C91A8241F960, ____soundSO_4)); }
	inline SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73 * get__soundSO_4() const { return ____soundSO_4; }
	inline SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73 ** get_address_of__soundSO_4() { return &____soundSO_4; }
	inline void set__soundSO_4(SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73 * value)
	{
		____soundSO_4 = value;
		Il2CppCodeGenWriteBarrier((&____soundSO_4), value);
	}

	inline static int32_t get_offset_of__lastTimeSound_5() { return static_cast<int32_t>(offsetof(HeroHealthRenderSystem_t75C28A2A739FC9E3EC68C36C6C80C91A8241F960, ____lastTimeSound_5)); }
	inline float get__lastTimeSound_5() const { return ____lastTimeSound_5; }
	inline float* get_address_of__lastTimeSound_5() { return &____lastTimeSound_5; }
	inline void set__lastTimeSound_5(float value)
	{
		____lastTimeSound_5 = value;
	}

	inline static int32_t get_offset_of__context_6() { return static_cast<int32_t>(offsetof(HeroHealthRenderSystem_t75C28A2A739FC9E3EC68C36C6C80C91A8241F960, ____context_6)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_6() const { return ____context_6; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_6() { return &____context_6; }
	inline void set__context_6(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_6 = value;
		Il2CppCodeGenWriteBarrier((&____context_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEROHEALTHRENDERSYSTEM_T75C28A2A739FC9E3EC68C36C6C80C91A8241F960_H
#ifndef HEROINITRENDERSYSTEM_T2635936F42F6041B5A244AE4A7E0C9FBC8165AB3_H
#define HEROINITRENDERSYSTEM_T2635936F42F6041B5A244AE4A7E0C9FBC8165AB3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HeroInitRenderSystem
struct  HeroInitRenderSystem_t2635936F42F6041B5A244AE4A7E0C9FBC8165AB3  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// Zenject.DiContainer HeroInitRenderSystem::_diContainer
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____diContainer_3;
	// UnitsSO HeroInitRenderSystem::_unitsConf
	UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 * ____unitsConf_4;
	// GameContext HeroInitRenderSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_5;

public:
	inline static int32_t get_offset_of__diContainer_3() { return static_cast<int32_t>(offsetof(HeroInitRenderSystem_t2635936F42F6041B5A244AE4A7E0C9FBC8165AB3, ____diContainer_3)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__diContainer_3() const { return ____diContainer_3; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__diContainer_3() { return &____diContainer_3; }
	inline void set__diContainer_3(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____diContainer_3 = value;
		Il2CppCodeGenWriteBarrier((&____diContainer_3), value);
	}

	inline static int32_t get_offset_of__unitsConf_4() { return static_cast<int32_t>(offsetof(HeroInitRenderSystem_t2635936F42F6041B5A244AE4A7E0C9FBC8165AB3, ____unitsConf_4)); }
	inline UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 * get__unitsConf_4() const { return ____unitsConf_4; }
	inline UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 ** get_address_of__unitsConf_4() { return &____unitsConf_4; }
	inline void set__unitsConf_4(UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 * value)
	{
		____unitsConf_4 = value;
		Il2CppCodeGenWriteBarrier((&____unitsConf_4), value);
	}

	inline static int32_t get_offset_of__context_5() { return static_cast<int32_t>(offsetof(HeroInitRenderSystem_t2635936F42F6041B5A244AE4A7E0C9FBC8165AB3, ____context_5)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_5() const { return ____context_5; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_5() { return &____context_5; }
	inline void set__context_5(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_5 = value;
		Il2CppCodeGenWriteBarrier((&____context_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEROINITRENDERSYSTEM_T2635936F42F6041B5A244AE4A7E0C9FBC8165AB3_H
#ifndef HEROMOVEMENTRENDERSYSTEM_TD6481D723FF68517C1FB9AE3A13CABA583766BA6_H
#define HEROMOVEMENTRENDERSYSTEM_TD6481D723FF68517C1FB9AE3A13CABA583766BA6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HeroMovementRenderSystem
struct  HeroMovementRenderSystem_tD6481D723FF68517C1FB9AE3A13CABA583766BA6  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// System.Single HeroMovementRenderSystem::EPSILON
	float ___EPSILON_3;

public:
	inline static int32_t get_offset_of_EPSILON_3() { return static_cast<int32_t>(offsetof(HeroMovementRenderSystem_tD6481D723FF68517C1FB9AE3A13CABA583766BA6, ___EPSILON_3)); }
	inline float get_EPSILON_3() const { return ___EPSILON_3; }
	inline float* get_address_of_EPSILON_3() { return &___EPSILON_3; }
	inline void set_EPSILON_3(float value)
	{
		___EPSILON_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEROMOVEMENTRENDERSYSTEM_TD6481D723FF68517C1FB9AE3A13CABA583766BA6_H
#ifndef INVENTORYFREERENDERSYSTEM_TAF4F619609C6ED69141732FF5E377F0201132129_H
#define INVENTORYFREERENDERSYSTEM_TAF4F619609C6ED69141732FF5E377F0201132129_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InventoryFreeRenderSystem
struct  InventoryFreeRenderSystem_tAF4F619609C6ED69141732FF5E377F0201132129  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// GameContext InventoryFreeRenderSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_3;

public:
	inline static int32_t get_offset_of__context_3() { return static_cast<int32_t>(offsetof(InventoryFreeRenderSystem_tAF4F619609C6ED69141732FF5E377F0201132129, ____context_3)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_3() const { return ____context_3; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_3() { return &____context_3; }
	inline void set__context_3(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_3 = value;
		Il2CppCodeGenWriteBarrier((&____context_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVENTORYFREERENDERSYSTEM_TAF4F619609C6ED69141732FF5E377F0201132129_H
#ifndef INVENTORYRENDERINITSYSTEM_T55EBC35BDB6E9B4EFFB774E72CF32A31026E9438_H
#define INVENTORYRENDERINITSYSTEM_T55EBC35BDB6E9B4EFFB774E72CF32A31026E9438_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InventoryRenderInitSystem
struct  InventoryRenderInitSystem_t55EBC35BDB6E9B4EFFB774E72CF32A31026E9438  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// GameContext InventoryRenderInitSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_3;

public:
	inline static int32_t get_offset_of__context_3() { return static_cast<int32_t>(offsetof(InventoryRenderInitSystem_t55EBC35BDB6E9B4EFFB774E72CF32A31026E9438, ____context_3)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_3() const { return ____context_3; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_3() { return &____context_3; }
	inline void set__context_3(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_3 = value;
		Il2CppCodeGenWriteBarrier((&____context_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVENTORYRENDERINITSYSTEM_T55EBC35BDB6E9B4EFFB774E72CF32A31026E9438_H
#ifndef LEFTHANDUIINITSYSTEM_T87D41FBCE6AE359217AD3C75DE1EFF735B0EDC26_H
#define LEFTHANDUIINITSYSTEM_T87D41FBCE6AE359217AD3C75DE1EFF735B0EDC26_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LeftHandUIInitSystem
struct  LeftHandUIInitSystem_t87D41FBCE6AE359217AD3C75DE1EFF735B0EDC26  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// GameContext LeftHandUIInitSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_3;

public:
	inline static int32_t get_offset_of__context_3() { return static_cast<int32_t>(offsetof(LeftHandUIInitSystem_t87D41FBCE6AE359217AD3C75DE1EFF735B0EDC26, ____context_3)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_3() const { return ____context_3; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_3() { return &____context_3; }
	inline void set__context_3(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_3 = value;
		Il2CppCodeGenWriteBarrier((&____context_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEFTHANDUIINITSYSTEM_T87D41FBCE6AE359217AD3C75DE1EFF735B0EDC26_H
#ifndef MEGABOOSTERSTATESYSTEM_T492C6A7E5409E4D8A82A30D117626EFE4D14B572_H
#define MEGABOOSTERSTATESYSTEM_T492C6A7E5409E4D8A82A30D117626EFE4D14B572_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MegaBoosterStateSystem
struct  MegaBoosterStateSystem_t492C6A7E5409E4D8A82A30D117626EFE4D14B572  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// GameContext MegaBoosterStateSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_3;

public:
	inline static int32_t get_offset_of__context_3() { return static_cast<int32_t>(offsetof(MegaBoosterStateSystem_t492C6A7E5409E4D8A82A30D117626EFE4D14B572, ____context_3)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_3() const { return ____context_3; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_3() { return &____context_3; }
	inline void set__context_3(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_3 = value;
		Il2CppCodeGenWriteBarrier((&____context_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEGABOOSTERSTATESYSTEM_T492C6A7E5409E4D8A82A30D117626EFE4D14B572_H
#ifndef NORMALEBOOSTERSTATESYSTEM_TA39F70EDC43638B5B1CC88CF21907414D39B7125_H
#define NORMALEBOOSTERSTATESYSTEM_TA39F70EDC43638B5B1CC88CF21907414D39B7125_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NormaleBoosterStateSystem
struct  NormaleBoosterStateSystem_tA39F70EDC43638B5B1CC88CF21907414D39B7125  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// GameContext NormaleBoosterStateSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_3;

public:
	inline static int32_t get_offset_of__context_3() { return static_cast<int32_t>(offsetof(NormaleBoosterStateSystem_tA39F70EDC43638B5B1CC88CF21907414D39B7125, ____context_3)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_3() const { return ____context_3; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_3() { return &____context_3; }
	inline void set__context_3(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_3 = value;
		Il2CppCodeGenWriteBarrier((&____context_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NORMALEBOOSTERSTATESYSTEM_TA39F70EDC43638B5B1CC88CF21907414D39B7125_H
#ifndef OBJECTIVECOUNTRENDERSYSTEM_T198210908B26FB1A8EDC8A9DB88852CF45D99DFB_H
#define OBJECTIVECOUNTRENDERSYSTEM_T198210908B26FB1A8EDC8A9DB88852CF45D99DFB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ObjectiveCountRenderSystem
struct  ObjectiveCountRenderSystem_t198210908B26FB1A8EDC8A9DB88852CF45D99DFB  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// GameContext ObjectiveCountRenderSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_3;

public:
	inline static int32_t get_offset_of__context_3() { return static_cast<int32_t>(offsetof(ObjectiveCountRenderSystem_t198210908B26FB1A8EDC8A9DB88852CF45D99DFB, ____context_3)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_3() const { return ____context_3; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_3() { return &____context_3; }
	inline void set__context_3(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_3 = value;
		Il2CppCodeGenWriteBarrier((&____context_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTIVECOUNTRENDERSYSTEM_T198210908B26FB1A8EDC8A9DB88852CF45D99DFB_H
#ifndef OBJECTIVESRENDERINITSYSTEM_T27E23B7A0DE0B91D340E261A6988849FCC35191A_H
#define OBJECTIVESRENDERINITSYSTEM_T27E23B7A0DE0B91D340E261A6988849FCC35191A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ObjectivesRenderInitSystem
struct  ObjectivesRenderInitSystem_t27E23B7A0DE0B91D340E261A6988849FCC35191A  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// Tayr.ILibrary ObjectivesRenderInitSystem::_uiMain
	RuntimeObject* ____uiMain_3;
	// UnitsSO ObjectivesRenderInitSystem::_unitsConf
	UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 * ____unitsConf_4;
	// GameContext ObjectivesRenderInitSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_5;

public:
	inline static int32_t get_offset_of__uiMain_3() { return static_cast<int32_t>(offsetof(ObjectivesRenderInitSystem_t27E23B7A0DE0B91D340E261A6988849FCC35191A, ____uiMain_3)); }
	inline RuntimeObject* get__uiMain_3() const { return ____uiMain_3; }
	inline RuntimeObject** get_address_of__uiMain_3() { return &____uiMain_3; }
	inline void set__uiMain_3(RuntimeObject* value)
	{
		____uiMain_3 = value;
		Il2CppCodeGenWriteBarrier((&____uiMain_3), value);
	}

	inline static int32_t get_offset_of__unitsConf_4() { return static_cast<int32_t>(offsetof(ObjectivesRenderInitSystem_t27E23B7A0DE0B91D340E261A6988849FCC35191A, ____unitsConf_4)); }
	inline UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 * get__unitsConf_4() const { return ____unitsConf_4; }
	inline UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 ** get_address_of__unitsConf_4() { return &____unitsConf_4; }
	inline void set__unitsConf_4(UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 * value)
	{
		____unitsConf_4 = value;
		Il2CppCodeGenWriteBarrier((&____unitsConf_4), value);
	}

	inline static int32_t get_offset_of__context_5() { return static_cast<int32_t>(offsetof(ObjectivesRenderInitSystem_t27E23B7A0DE0B91D340E261A6988849FCC35191A, ____context_5)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_5() const { return ____context_5; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_5() { return &____context_5; }
	inline void set__context_5(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_5 = value;
		Il2CppCodeGenWriteBarrier((&____context_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTIVESRENDERINITSYSTEM_T27E23B7A0DE0B91D340E261A6988849FCC35191A_H
#ifndef PROJECTILERENDERINITSYSTEM_T9630DFE1C78BF96242B55D595071AD21EF3A4F8D_H
#define PROJECTILERENDERINITSYSTEM_T9630DFE1C78BF96242B55D595071AD21EF3A4F8D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ProjectileRenderInitSystem
struct  ProjectileRenderInitSystem_t9630DFE1C78BF96242B55D595071AD21EF3A4F8D  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// Zenject.DiContainer ProjectileRenderInitSystem::_container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____container_3;
	// GameContext ProjectileRenderInitSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_4;

public:
	inline static int32_t get_offset_of__container_3() { return static_cast<int32_t>(offsetof(ProjectileRenderInitSystem_t9630DFE1C78BF96242B55D595071AD21EF3A4F8D, ____container_3)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__container_3() const { return ____container_3; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__container_3() { return &____container_3; }
	inline void set__container_3(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____container_3 = value;
		Il2CppCodeGenWriteBarrier((&____container_3), value);
	}

	inline static int32_t get_offset_of__context_4() { return static_cast<int32_t>(offsetof(ProjectileRenderInitSystem_t9630DFE1C78BF96242B55D595071AD21EF3A4F8D, ____context_4)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_4() const { return ____context_4; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_4() { return &____context_4; }
	inline void set__context_4(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_4 = value;
		Il2CppCodeGenWriteBarrier((&____context_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROJECTILERENDERINITSYSTEM_T9630DFE1C78BF96242B55D595071AD21EF3A4F8D_H
#ifndef PROJECTILERENDERMOVEMENTSYSTEM_TFF1FDD22E235DF6570503388DD67D1A77E6600E9_H
#define PROJECTILERENDERMOVEMENTSYSTEM_TFF1FDD22E235DF6570503388DD67D1A77E6600E9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ProjectileRenderMovementSystem
struct  ProjectileRenderMovementSystem_tFF1FDD22E235DF6570503388DD67D1A77E6600E9  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROJECTILERENDERMOVEMENTSYSTEM_TFF1FDD22E235DF6570503388DD67D1A77E6600E9_H
#ifndef SPELLUISTATUSSYSTEM_T6BB6D73D1782996C162FBDD5FBD8FC82E551A7AA_H
#define SPELLUISTATUSSYSTEM_T6BB6D73D1782996C162FBDD5FBD8FC82E551A7AA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpellUIStatusSystem
struct  SpellUIStatusSystem_t6BB6D73D1782996C162FBDD5FBD8FC82E551A7AA  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// SpellsSO SpellUIStatusSystem::_spellsSO
	SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * ____spellsSO_3;
	// UnitsSO SpellUIStatusSystem::_unitsSO
	UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 * ____unitsSO_4;
	// InventorySystem SpellUIStatusSystem::_inventorySystem
	InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 * ____inventorySystem_5;
	// Entitas.IGroup`1<GameEntity> SpellUIStatusSystem::_entities
	RuntimeObject* ____entities_6;
	// GameContext SpellUIStatusSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_7;

public:
	inline static int32_t get_offset_of__spellsSO_3() { return static_cast<int32_t>(offsetof(SpellUIStatusSystem_t6BB6D73D1782996C162FBDD5FBD8FC82E551A7AA, ____spellsSO_3)); }
	inline SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * get__spellsSO_3() const { return ____spellsSO_3; }
	inline SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 ** get_address_of__spellsSO_3() { return &____spellsSO_3; }
	inline void set__spellsSO_3(SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * value)
	{
		____spellsSO_3 = value;
		Il2CppCodeGenWriteBarrier((&____spellsSO_3), value);
	}

	inline static int32_t get_offset_of__unitsSO_4() { return static_cast<int32_t>(offsetof(SpellUIStatusSystem_t6BB6D73D1782996C162FBDD5FBD8FC82E551A7AA, ____unitsSO_4)); }
	inline UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 * get__unitsSO_4() const { return ____unitsSO_4; }
	inline UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 ** get_address_of__unitsSO_4() { return &____unitsSO_4; }
	inline void set__unitsSO_4(UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 * value)
	{
		____unitsSO_4 = value;
		Il2CppCodeGenWriteBarrier((&____unitsSO_4), value);
	}

	inline static int32_t get_offset_of__inventorySystem_5() { return static_cast<int32_t>(offsetof(SpellUIStatusSystem_t6BB6D73D1782996C162FBDD5FBD8FC82E551A7AA, ____inventorySystem_5)); }
	inline InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 * get__inventorySystem_5() const { return ____inventorySystem_5; }
	inline InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 ** get_address_of__inventorySystem_5() { return &____inventorySystem_5; }
	inline void set__inventorySystem_5(InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 * value)
	{
		____inventorySystem_5 = value;
		Il2CppCodeGenWriteBarrier((&____inventorySystem_5), value);
	}

	inline static int32_t get_offset_of__entities_6() { return static_cast<int32_t>(offsetof(SpellUIStatusSystem_t6BB6D73D1782996C162FBDD5FBD8FC82E551A7AA, ____entities_6)); }
	inline RuntimeObject* get__entities_6() const { return ____entities_6; }
	inline RuntimeObject** get_address_of__entities_6() { return &____entities_6; }
	inline void set__entities_6(RuntimeObject* value)
	{
		____entities_6 = value;
		Il2CppCodeGenWriteBarrier((&____entities_6), value);
	}

	inline static int32_t get_offset_of__context_7() { return static_cast<int32_t>(offsetof(SpellUIStatusSystem_t6BB6D73D1782996C162FBDD5FBD8FC82E551A7AA, ____context_7)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_7() const { return ____context_7; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_7() { return &____context_7; }
	inline void set__context_7(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_7 = value;
		Il2CppCodeGenWriteBarrier((&____context_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPELLUISTATUSSYSTEM_T6BB6D73D1782996C162FBDD5FBD8FC82E551A7AA_H
#ifndef SPELLUIUNLOCKSYSTEM_T9F96E0D5E6B58576448EA8C6EB4E6950AC7B746D_H
#define SPELLUIUNLOCKSYSTEM_T9F96E0D5E6B58576448EA8C6EB4E6950AC7B746D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpellUIUnlockSystem
struct  SpellUIUnlockSystem_t9F96E0D5E6B58576448EA8C6EB4E6950AC7B746D  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// Tayr.ILibrary SpellUIUnlockSystem::_uiMain
	RuntimeObject* ____uiMain_3;
	// UnitsSO SpellUIUnlockSystem::_unitsConf
	UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 * ____unitsConf_4;
	// GameContext SpellUIUnlockSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_5;

public:
	inline static int32_t get_offset_of__uiMain_3() { return static_cast<int32_t>(offsetof(SpellUIUnlockSystem_t9F96E0D5E6B58576448EA8C6EB4E6950AC7B746D, ____uiMain_3)); }
	inline RuntimeObject* get__uiMain_3() const { return ____uiMain_3; }
	inline RuntimeObject** get_address_of__uiMain_3() { return &____uiMain_3; }
	inline void set__uiMain_3(RuntimeObject* value)
	{
		____uiMain_3 = value;
		Il2CppCodeGenWriteBarrier((&____uiMain_3), value);
	}

	inline static int32_t get_offset_of__unitsConf_4() { return static_cast<int32_t>(offsetof(SpellUIUnlockSystem_t9F96E0D5E6B58576448EA8C6EB4E6950AC7B746D, ____unitsConf_4)); }
	inline UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 * get__unitsConf_4() const { return ____unitsConf_4; }
	inline UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 ** get_address_of__unitsConf_4() { return &____unitsConf_4; }
	inline void set__unitsConf_4(UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 * value)
	{
		____unitsConf_4 = value;
		Il2CppCodeGenWriteBarrier((&____unitsConf_4), value);
	}

	inline static int32_t get_offset_of__context_5() { return static_cast<int32_t>(offsetof(SpellUIUnlockSystem_t9F96E0D5E6B58576448EA8C6EB4E6950AC7B746D, ____context_5)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_5() const { return ____context_5; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_5() { return &____context_5; }
	inline void set__context_5(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_5 = value;
		Il2CppCodeGenWriteBarrier((&____context_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPELLUIUNLOCKSYSTEM_T9F96E0D5E6B58576448EA8C6EB4E6950AC7B746D_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef ASYNCMETHODBUILDERCORE_T4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01_H
#define ASYNCMETHODBUILDERCORE_T4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.AsyncMethodBuilderCore
struct  AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01 
{
public:
	// System.Runtime.CompilerServices.IAsyncStateMachine System.Runtime.CompilerServices.AsyncMethodBuilderCore::m_stateMachine
	RuntimeObject* ___m_stateMachine_0;
	// System.Action System.Runtime.CompilerServices.AsyncMethodBuilderCore::m_defaultContextAction
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___m_defaultContextAction_1;

public:
	inline static int32_t get_offset_of_m_stateMachine_0() { return static_cast<int32_t>(offsetof(AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01, ___m_stateMachine_0)); }
	inline RuntimeObject* get_m_stateMachine_0() const { return ___m_stateMachine_0; }
	inline RuntimeObject** get_address_of_m_stateMachine_0() { return &___m_stateMachine_0; }
	inline void set_m_stateMachine_0(RuntimeObject* value)
	{
		___m_stateMachine_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_stateMachine_0), value);
	}

	inline static int32_t get_offset_of_m_defaultContextAction_1() { return static_cast<int32_t>(offsetof(AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01, ___m_defaultContextAction_1)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_m_defaultContextAction_1() const { return ___m_defaultContextAction_1; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_m_defaultContextAction_1() { return &___m_defaultContextAction_1; }
	inline void set_m_defaultContextAction_1(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___m_defaultContextAction_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_defaultContextAction_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.CompilerServices.AsyncMethodBuilderCore
struct AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01_marshaled_pinvoke
{
	RuntimeObject* ___m_stateMachine_0;
	Il2CppMethodPointer ___m_defaultContextAction_1;
};
// Native definition for COM marshalling of System.Runtime.CompilerServices.AsyncMethodBuilderCore
struct AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01_marshaled_com
{
	RuntimeObject* ___m_stateMachine_0;
	Il2CppMethodPointer ___m_defaultContextAction_1;
};
#endif // ASYNCMETHODBUILDERCORE_T4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01_H
#ifndef TASKAWAITER_T0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F_H
#define TASKAWAITER_T0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.TaskAwaiter
struct  TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F 
{
public:
	// System.Threading.Tasks.Task System.Runtime.CompilerServices.TaskAwaiter::m_task
	Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * ___m_task_0;

public:
	inline static int32_t get_offset_of_m_task_0() { return static_cast<int32_t>(offsetof(TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F, ___m_task_0)); }
	inline Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * get_m_task_0() const { return ___m_task_0; }
	inline Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 ** get_address_of_m_task_0() { return &___m_task_0; }
	inline void set_m_task_0(Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * value)
	{
		___m_task_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_task_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.CompilerServices.TaskAwaiter
struct TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F_marshaled_pinvoke
{
	Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * ___m_task_0;
};
// Native definition for COM marshalling of System.Runtime.CompilerServices.TaskAwaiter
struct TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F_marshaled_com
{
	Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * ___m_task_0;
};
#endif // TASKAWAITER_T0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F_H
#ifndef TOWERCALLOUTSYSTEM_T7881CCFC9FFF807D37301CB232D05C1096E999E9_H
#define TOWERCALLOUTSYSTEM_T7881CCFC9FFF807D37301CB232D05C1096E999E9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TowerCalloutSystem
struct  TowerCalloutSystem_t7881CCFC9FFF807D37301CB232D05C1096E999E9  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// Tayr.ILibrary TowerCalloutSystem::_uiMain
	RuntimeObject* ____uiMain_3;
	// CustomTutorialSO TowerCalloutSystem::_tutorialSO
	CustomTutorialSO_t45FC033F62BB26BC5595EAA9FEF1DF50BFA2A41D * ____tutorialSO_4;
	// GameContext TowerCalloutSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_5;
	// System.Int32 TowerCalloutSystem::_maxCallouts
	int32_t ____maxCallouts_6;
	// CustomTutorialModel TowerCalloutSystem::_tutorialModel
	CustomTutorialModel_t7F1E6EF5B6AF58AEA9FB2573811B4562E587E635 * ____tutorialModel_7;
	// System.Boolean TowerCalloutSystem::_isTriggered
	bool ____isTriggered_8;

public:
	inline static int32_t get_offset_of__uiMain_3() { return static_cast<int32_t>(offsetof(TowerCalloutSystem_t7881CCFC9FFF807D37301CB232D05C1096E999E9, ____uiMain_3)); }
	inline RuntimeObject* get__uiMain_3() const { return ____uiMain_3; }
	inline RuntimeObject** get_address_of__uiMain_3() { return &____uiMain_3; }
	inline void set__uiMain_3(RuntimeObject* value)
	{
		____uiMain_3 = value;
		Il2CppCodeGenWriteBarrier((&____uiMain_3), value);
	}

	inline static int32_t get_offset_of__tutorialSO_4() { return static_cast<int32_t>(offsetof(TowerCalloutSystem_t7881CCFC9FFF807D37301CB232D05C1096E999E9, ____tutorialSO_4)); }
	inline CustomTutorialSO_t45FC033F62BB26BC5595EAA9FEF1DF50BFA2A41D * get__tutorialSO_4() const { return ____tutorialSO_4; }
	inline CustomTutorialSO_t45FC033F62BB26BC5595EAA9FEF1DF50BFA2A41D ** get_address_of__tutorialSO_4() { return &____tutorialSO_4; }
	inline void set__tutorialSO_4(CustomTutorialSO_t45FC033F62BB26BC5595EAA9FEF1DF50BFA2A41D * value)
	{
		____tutorialSO_4 = value;
		Il2CppCodeGenWriteBarrier((&____tutorialSO_4), value);
	}

	inline static int32_t get_offset_of__context_5() { return static_cast<int32_t>(offsetof(TowerCalloutSystem_t7881CCFC9FFF807D37301CB232D05C1096E999E9, ____context_5)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_5() const { return ____context_5; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_5() { return &____context_5; }
	inline void set__context_5(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_5 = value;
		Il2CppCodeGenWriteBarrier((&____context_5), value);
	}

	inline static int32_t get_offset_of__maxCallouts_6() { return static_cast<int32_t>(offsetof(TowerCalloutSystem_t7881CCFC9FFF807D37301CB232D05C1096E999E9, ____maxCallouts_6)); }
	inline int32_t get__maxCallouts_6() const { return ____maxCallouts_6; }
	inline int32_t* get_address_of__maxCallouts_6() { return &____maxCallouts_6; }
	inline void set__maxCallouts_6(int32_t value)
	{
		____maxCallouts_6 = value;
	}

	inline static int32_t get_offset_of__tutorialModel_7() { return static_cast<int32_t>(offsetof(TowerCalloutSystem_t7881CCFC9FFF807D37301CB232D05C1096E999E9, ____tutorialModel_7)); }
	inline CustomTutorialModel_t7F1E6EF5B6AF58AEA9FB2573811B4562E587E635 * get__tutorialModel_7() const { return ____tutorialModel_7; }
	inline CustomTutorialModel_t7F1E6EF5B6AF58AEA9FB2573811B4562E587E635 ** get_address_of__tutorialModel_7() { return &____tutorialModel_7; }
	inline void set__tutorialModel_7(CustomTutorialModel_t7F1E6EF5B6AF58AEA9FB2573811B4562E587E635 * value)
	{
		____tutorialModel_7 = value;
		Il2CppCodeGenWriteBarrier((&____tutorialModel_7), value);
	}

	inline static int32_t get_offset_of__isTriggered_8() { return static_cast<int32_t>(offsetof(TowerCalloutSystem_t7881CCFC9FFF807D37301CB232D05C1096E999E9, ____isTriggered_8)); }
	inline bool get__isTriggered_8() const { return ____isTriggered_8; }
	inline bool* get_address_of__isTriggered_8() { return &____isTriggered_8; }
	inline void set__isTriggered_8(bool value)
	{
		____isTriggered_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOWERCALLOUTSYSTEM_T7881CCFC9FFF807D37301CB232D05C1096E999E9_H
#ifndef TOWERKICKERWITHICONSYSTEM_T617FA57E1A9226152864A1AD4ED6CF1C059D8A09_H
#define TOWERKICKERWITHICONSYSTEM_T617FA57E1A9226152864A1AD4ED6CF1C059D8A09_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TowerKickerWithIconSystem
struct  TowerKickerWithIconSystem_t617FA57E1A9226152864A1AD4ED6CF1C059D8A09  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// TowersSO TowerKickerWithIconSystem::_towersSO
	TowersSO_t3B7DE821E5F8017950776EEFC42E680B361BCDF0 * ____towersSO_3;
	// Tayr.ILibrary TowerKickerWithIconSystem::_uiMain
	RuntimeObject* ____uiMain_4;
	// GameContext TowerKickerWithIconSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_5;

public:
	inline static int32_t get_offset_of__towersSO_3() { return static_cast<int32_t>(offsetof(TowerKickerWithIconSystem_t617FA57E1A9226152864A1AD4ED6CF1C059D8A09, ____towersSO_3)); }
	inline TowersSO_t3B7DE821E5F8017950776EEFC42E680B361BCDF0 * get__towersSO_3() const { return ____towersSO_3; }
	inline TowersSO_t3B7DE821E5F8017950776EEFC42E680B361BCDF0 ** get_address_of__towersSO_3() { return &____towersSO_3; }
	inline void set__towersSO_3(TowersSO_t3B7DE821E5F8017950776EEFC42E680B361BCDF0 * value)
	{
		____towersSO_3 = value;
		Il2CppCodeGenWriteBarrier((&____towersSO_3), value);
	}

	inline static int32_t get_offset_of__uiMain_4() { return static_cast<int32_t>(offsetof(TowerKickerWithIconSystem_t617FA57E1A9226152864A1AD4ED6CF1C059D8A09, ____uiMain_4)); }
	inline RuntimeObject* get__uiMain_4() const { return ____uiMain_4; }
	inline RuntimeObject** get_address_of__uiMain_4() { return &____uiMain_4; }
	inline void set__uiMain_4(RuntimeObject* value)
	{
		____uiMain_4 = value;
		Il2CppCodeGenWriteBarrier((&____uiMain_4), value);
	}

	inline static int32_t get_offset_of__context_5() { return static_cast<int32_t>(offsetof(TowerKickerWithIconSystem_t617FA57E1A9226152864A1AD4ED6CF1C059D8A09, ____context_5)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_5() const { return ____context_5; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_5() { return &____context_5; }
	inline void set__context_5(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_5 = value;
		Il2CppCodeGenWriteBarrier((&____context_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOWERKICKERWITHICONSYSTEM_T617FA57E1A9226152864A1AD4ED6CF1C059D8A09_H
#ifndef TOWERRENDERSYSTEM_T2323ECB7EB8589584194DE05BCACF223FB864FD4_H
#define TOWERRENDERSYSTEM_T2323ECB7EB8589584194DE05BCACF223FB864FD4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TowerRenderSystem
struct  TowerRenderSystem_t2323ECB7EB8589584194DE05BCACF223FB864FD4  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// Tayr.ILibrary TowerRenderSystem::_uiMain
	RuntimeObject* ____uiMain_3;
	// Zenject.DiContainer TowerRenderSystem::_diContainer
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____diContainer_4;
	// TowersSO TowerRenderSystem::_towersSO
	TowersSO_t3B7DE821E5F8017950776EEFC42E680B361BCDF0 * ____towersSO_5;
	// GameContext TowerRenderSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_6;

public:
	inline static int32_t get_offset_of__uiMain_3() { return static_cast<int32_t>(offsetof(TowerRenderSystem_t2323ECB7EB8589584194DE05BCACF223FB864FD4, ____uiMain_3)); }
	inline RuntimeObject* get__uiMain_3() const { return ____uiMain_3; }
	inline RuntimeObject** get_address_of__uiMain_3() { return &____uiMain_3; }
	inline void set__uiMain_3(RuntimeObject* value)
	{
		____uiMain_3 = value;
		Il2CppCodeGenWriteBarrier((&____uiMain_3), value);
	}

	inline static int32_t get_offset_of__diContainer_4() { return static_cast<int32_t>(offsetof(TowerRenderSystem_t2323ECB7EB8589584194DE05BCACF223FB864FD4, ____diContainer_4)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__diContainer_4() const { return ____diContainer_4; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__diContainer_4() { return &____diContainer_4; }
	inline void set__diContainer_4(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____diContainer_4 = value;
		Il2CppCodeGenWriteBarrier((&____diContainer_4), value);
	}

	inline static int32_t get_offset_of__towersSO_5() { return static_cast<int32_t>(offsetof(TowerRenderSystem_t2323ECB7EB8589584194DE05BCACF223FB864FD4, ____towersSO_5)); }
	inline TowersSO_t3B7DE821E5F8017950776EEFC42E680B361BCDF0 * get__towersSO_5() const { return ____towersSO_5; }
	inline TowersSO_t3B7DE821E5F8017950776EEFC42E680B361BCDF0 ** get_address_of__towersSO_5() { return &____towersSO_5; }
	inline void set__towersSO_5(TowersSO_t3B7DE821E5F8017950776EEFC42E680B361BCDF0 * value)
	{
		____towersSO_5 = value;
		Il2CppCodeGenWriteBarrier((&____towersSO_5), value);
	}

	inline static int32_t get_offset_of__context_6() { return static_cast<int32_t>(offsetof(TowerRenderSystem_t2323ECB7EB8589584194DE05BCACF223FB864FD4, ____context_6)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_6() const { return ____context_6; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_6() { return &____context_6; }
	inline void set__context_6(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_6 = value;
		Il2CppCodeGenWriteBarrier((&____context_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOWERRENDERSYSTEM_T2323ECB7EB8589584194DE05BCACF223FB864FD4_H
#ifndef TOWERSTATERENDERSYSTEM_TB2EFC75A920AE3308B098F8A0FB0D0FDD6DDCC15_H
#define TOWERSTATERENDERSYSTEM_TB2EFC75A920AE3308B098F8A0FB0D0FDD6DDCC15_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TowerStateRenderSystem
struct  TowerStateRenderSystem_tB2EFC75A920AE3308B098F8A0FB0D0FDD6DDCC15  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// TowersSO TowerStateRenderSystem::_towersSO
	TowersSO_t3B7DE821E5F8017950776EEFC42E680B361BCDF0 * ____towersSO_3;
	// Tayr.ILibrary TowerStateRenderSystem::_sfxLibrary
	RuntimeObject* ____sfxLibrary_4;
	// GameContext TowerStateRenderSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_5;

public:
	inline static int32_t get_offset_of__towersSO_3() { return static_cast<int32_t>(offsetof(TowerStateRenderSystem_tB2EFC75A920AE3308B098F8A0FB0D0FDD6DDCC15, ____towersSO_3)); }
	inline TowersSO_t3B7DE821E5F8017950776EEFC42E680B361BCDF0 * get__towersSO_3() const { return ____towersSO_3; }
	inline TowersSO_t3B7DE821E5F8017950776EEFC42E680B361BCDF0 ** get_address_of__towersSO_3() { return &____towersSO_3; }
	inline void set__towersSO_3(TowersSO_t3B7DE821E5F8017950776EEFC42E680B361BCDF0 * value)
	{
		____towersSO_3 = value;
		Il2CppCodeGenWriteBarrier((&____towersSO_3), value);
	}

	inline static int32_t get_offset_of__sfxLibrary_4() { return static_cast<int32_t>(offsetof(TowerStateRenderSystem_tB2EFC75A920AE3308B098F8A0FB0D0FDD6DDCC15, ____sfxLibrary_4)); }
	inline RuntimeObject* get__sfxLibrary_4() const { return ____sfxLibrary_4; }
	inline RuntimeObject** get_address_of__sfxLibrary_4() { return &____sfxLibrary_4; }
	inline void set__sfxLibrary_4(RuntimeObject* value)
	{
		____sfxLibrary_4 = value;
		Il2CppCodeGenWriteBarrier((&____sfxLibrary_4), value);
	}

	inline static int32_t get_offset_of__context_5() { return static_cast<int32_t>(offsetof(TowerStateRenderSystem_tB2EFC75A920AE3308B098F8A0FB0D0FDD6DDCC15, ____context_5)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_5() const { return ____context_5; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_5() { return &____context_5; }
	inline void set__context_5(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_5 = value;
		Il2CppCodeGenWriteBarrier((&____context_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOWERSTATERENDERSYSTEM_TB2EFC75A920AE3308B098F8A0FB0D0FDD6DDCC15_H
#ifndef TRAPCALLOUTSYSTEM_TE42E39E38FE8532829F1E8409C5E0ED7717076BF_H
#define TRAPCALLOUTSYSTEM_TE42E39E38FE8532829F1E8409C5E0ED7717076BF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TrapCalloutSystem
struct  TrapCalloutSystem_tE42E39E38FE8532829F1E8409C5E0ED7717076BF  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// Tayr.ILibrary TrapCalloutSystem::_uiMain
	RuntimeObject* ____uiMain_3;
	// CustomTutorialSO TrapCalloutSystem::_tutorialSO
	CustomTutorialSO_t45FC033F62BB26BC5595EAA9FEF1DF50BFA2A41D * ____tutorialSO_4;
	// GameContext TrapCalloutSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_5;
	// System.Int32 TrapCalloutSystem::_maxCallouts
	int32_t ____maxCallouts_6;
	// CustomTutorialModel TrapCalloutSystem::_tutorialModel
	CustomTutorialModel_t7F1E6EF5B6AF58AEA9FB2573811B4562E587E635 * ____tutorialModel_7;
	// System.Boolean TrapCalloutSystem::_isTriggered
	bool ____isTriggered_8;

public:
	inline static int32_t get_offset_of__uiMain_3() { return static_cast<int32_t>(offsetof(TrapCalloutSystem_tE42E39E38FE8532829F1E8409C5E0ED7717076BF, ____uiMain_3)); }
	inline RuntimeObject* get__uiMain_3() const { return ____uiMain_3; }
	inline RuntimeObject** get_address_of__uiMain_3() { return &____uiMain_3; }
	inline void set__uiMain_3(RuntimeObject* value)
	{
		____uiMain_3 = value;
		Il2CppCodeGenWriteBarrier((&____uiMain_3), value);
	}

	inline static int32_t get_offset_of__tutorialSO_4() { return static_cast<int32_t>(offsetof(TrapCalloutSystem_tE42E39E38FE8532829F1E8409C5E0ED7717076BF, ____tutorialSO_4)); }
	inline CustomTutorialSO_t45FC033F62BB26BC5595EAA9FEF1DF50BFA2A41D * get__tutorialSO_4() const { return ____tutorialSO_4; }
	inline CustomTutorialSO_t45FC033F62BB26BC5595EAA9FEF1DF50BFA2A41D ** get_address_of__tutorialSO_4() { return &____tutorialSO_4; }
	inline void set__tutorialSO_4(CustomTutorialSO_t45FC033F62BB26BC5595EAA9FEF1DF50BFA2A41D * value)
	{
		____tutorialSO_4 = value;
		Il2CppCodeGenWriteBarrier((&____tutorialSO_4), value);
	}

	inline static int32_t get_offset_of__context_5() { return static_cast<int32_t>(offsetof(TrapCalloutSystem_tE42E39E38FE8532829F1E8409C5E0ED7717076BF, ____context_5)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_5() const { return ____context_5; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_5() { return &____context_5; }
	inline void set__context_5(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_5 = value;
		Il2CppCodeGenWriteBarrier((&____context_5), value);
	}

	inline static int32_t get_offset_of__maxCallouts_6() { return static_cast<int32_t>(offsetof(TrapCalloutSystem_tE42E39E38FE8532829F1E8409C5E0ED7717076BF, ____maxCallouts_6)); }
	inline int32_t get__maxCallouts_6() const { return ____maxCallouts_6; }
	inline int32_t* get_address_of__maxCallouts_6() { return &____maxCallouts_6; }
	inline void set__maxCallouts_6(int32_t value)
	{
		____maxCallouts_6 = value;
	}

	inline static int32_t get_offset_of__tutorialModel_7() { return static_cast<int32_t>(offsetof(TrapCalloutSystem_tE42E39E38FE8532829F1E8409C5E0ED7717076BF, ____tutorialModel_7)); }
	inline CustomTutorialModel_t7F1E6EF5B6AF58AEA9FB2573811B4562E587E635 * get__tutorialModel_7() const { return ____tutorialModel_7; }
	inline CustomTutorialModel_t7F1E6EF5B6AF58AEA9FB2573811B4562E587E635 ** get_address_of__tutorialModel_7() { return &____tutorialModel_7; }
	inline void set__tutorialModel_7(CustomTutorialModel_t7F1E6EF5B6AF58AEA9FB2573811B4562E587E635 * value)
	{
		____tutorialModel_7 = value;
		Il2CppCodeGenWriteBarrier((&____tutorialModel_7), value);
	}

	inline static int32_t get_offset_of__isTriggered_8() { return static_cast<int32_t>(offsetof(TrapCalloutSystem_tE42E39E38FE8532829F1E8409C5E0ED7717076BF, ____isTriggered_8)); }
	inline bool get__isTriggered_8() const { return ____isTriggered_8; }
	inline bool* get_address_of__isTriggered_8() { return &____isTriggered_8; }
	inline void set__isTriggered_8(bool value)
	{
		____isTriggered_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRAPCALLOUTSYSTEM_TE42E39E38FE8532829F1E8409C5E0ED7717076BF_H
#ifndef TRAPRENDERSYSTEM_TADC9898D19EB0C4753108E84CC071C378D4866AD_H
#define TRAPRENDERSYSTEM_TADC9898D19EB0C4753108E84CC071C378D4866AD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TrapRenderSystem
struct  TrapRenderSystem_tADC9898D19EB0C4753108E84CC071C378D4866AD  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// Zenject.DiContainer TrapRenderSystem::_diContainer
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____diContainer_3;
	// TrapsSO TrapRenderSystem::_trapsSO
	TrapsSO_tFF060852B3652DD05F33E1B19B8EC9166468D9C2 * ____trapsSO_4;
	// GameContext TrapRenderSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_5;

public:
	inline static int32_t get_offset_of__diContainer_3() { return static_cast<int32_t>(offsetof(TrapRenderSystem_tADC9898D19EB0C4753108E84CC071C378D4866AD, ____diContainer_3)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__diContainer_3() const { return ____diContainer_3; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__diContainer_3() { return &____diContainer_3; }
	inline void set__diContainer_3(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____diContainer_3 = value;
		Il2CppCodeGenWriteBarrier((&____diContainer_3), value);
	}

	inline static int32_t get_offset_of__trapsSO_4() { return static_cast<int32_t>(offsetof(TrapRenderSystem_tADC9898D19EB0C4753108E84CC071C378D4866AD, ____trapsSO_4)); }
	inline TrapsSO_tFF060852B3652DD05F33E1B19B8EC9166468D9C2 * get__trapsSO_4() const { return ____trapsSO_4; }
	inline TrapsSO_tFF060852B3652DD05F33E1B19B8EC9166468D9C2 ** get_address_of__trapsSO_4() { return &____trapsSO_4; }
	inline void set__trapsSO_4(TrapsSO_tFF060852B3652DD05F33E1B19B8EC9166468D9C2 * value)
	{
		____trapsSO_4 = value;
		Il2CppCodeGenWriteBarrier((&____trapsSO_4), value);
	}

	inline static int32_t get_offset_of__context_5() { return static_cast<int32_t>(offsetof(TrapRenderSystem_tADC9898D19EB0C4753108E84CC071C378D4866AD, ____context_5)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_5() const { return ____context_5; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_5() { return &____context_5; }
	inline void set__context_5(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_5 = value;
		Il2CppCodeGenWriteBarrier((&____context_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRAPRENDERSYSTEM_TADC9898D19EB0C4753108E84CC071C378D4866AD_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef BATTLEKICKERWITHICONCOMPONENT_T9744346E9F295A455BBD6E49E1041E3EF7F28107_H
#define BATTLEKICKERWITHICONCOMPONENT_T9744346E9F295A455BBD6E49E1041E3EF7F28107_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BattleKickerWithIconComponent
struct  BattleKickerWithIconComponent_t9744346E9F295A455BBD6E49E1041E3EF7F28107  : public RuntimeObject
{
public:
	// System.String BattleKickerWithIconComponent::ItemId
	String_t* ___ItemId_0;
	// UnityEngine.Vector3 BattleKickerWithIconComponent::Position
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___Position_1;

public:
	inline static int32_t get_offset_of_ItemId_0() { return static_cast<int32_t>(offsetof(BattleKickerWithIconComponent_t9744346E9F295A455BBD6E49E1041E3EF7F28107, ___ItemId_0)); }
	inline String_t* get_ItemId_0() const { return ___ItemId_0; }
	inline String_t** get_address_of_ItemId_0() { return &___ItemId_0; }
	inline void set_ItemId_0(String_t* value)
	{
		___ItemId_0 = value;
		Il2CppCodeGenWriteBarrier((&___ItemId_0), value);
	}

	inline static int32_t get_offset_of_Position_1() { return static_cast<int32_t>(offsetof(BattleKickerWithIconComponent_t9744346E9F295A455BBD6E49E1041E3EF7F28107, ___Position_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_Position_1() const { return ___Position_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_Position_1() { return &___Position_1; }
	inline void set_Position_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___Position_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BATTLEKICKERWITHICONCOMPONENT_T9744346E9F295A455BBD6E49E1041E3EF7F28107_H
#ifndef EFFECTRENDERENTITYINDEX_TF5D57B2C69C74DA04F3510E5A0C2B6D9AF5211F6_H
#define EFFECTRENDERENTITYINDEX_TF5D57B2C69C74DA04F3510E5A0C2B6D9AF5211F6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EffectRenderEntityIndex
struct  EffectRenderEntityIndex_tF5D57B2C69C74DA04F3510E5A0C2B6D9AF5211F6  : public PrimaryEntityIndex_2_t17175B118C000A97322D66364138713F2177167A
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EFFECTRENDERENTITYINDEX_TF5D57B2C69C74DA04F3510E5A0C2B6D9AF5211F6_H
#ifndef EFFECTVISUALTYPE_T026EC7D3087FCA80C407ED9F3797CE097903D15A_H
#define EFFECTVISUALTYPE_T026EC7D3087FCA80C407ED9F3797CE097903D15A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EffectVisualType
struct  EffectVisualType_t026EC7D3087FCA80C407ED9F3797CE097903D15A 
{
public:
	// System.Int32 EffectVisualType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EffectVisualType_t026EC7D3087FCA80C407ED9F3797CE097903D15A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EFFECTVISUALTYPE_T026EC7D3087FCA80C407ED9F3797CE097903D15A_H
#ifndef ERTUGRULFEATURE_TC4183E2BE6E3B2EDB7F366467E3D8E03B5A00484_H
#define ERTUGRULFEATURE_TC4183E2BE6E3B2EDB7F366467E3D8E03B5A00484_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ErtugrulFeature
struct  ErtugrulFeature_tC4183E2BE6E3B2EDB7F366467E3D8E03B5A00484  : public Feature_t3D2CE4A6E0A21FB131F20D2F64B7A6C289F0CF0F
{
public:
	// Zenject.DiContainer ErtugrulFeature::_diContainer
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____diContainer_4;
	// Contexts ErtugrulFeature::_contexts
	Contexts_t313FEE68C5FB0568E30785C33C0F6209F5F7B87D * ____contexts_5;

public:
	inline static int32_t get_offset_of__diContainer_4() { return static_cast<int32_t>(offsetof(ErtugrulFeature_tC4183E2BE6E3B2EDB7F366467E3D8E03B5A00484, ____diContainer_4)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__diContainer_4() const { return ____diContainer_4; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__diContainer_4() { return &____diContainer_4; }
	inline void set__diContainer_4(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____diContainer_4 = value;
		Il2CppCodeGenWriteBarrier((&____diContainer_4), value);
	}

	inline static int32_t get_offset_of__contexts_5() { return static_cast<int32_t>(offsetof(ErtugrulFeature_tC4183E2BE6E3B2EDB7F366467E3D8E03B5A00484, ____contexts_5)); }
	inline Contexts_t313FEE68C5FB0568E30785C33C0F6209F5F7B87D * get__contexts_5() const { return ____contexts_5; }
	inline Contexts_t313FEE68C5FB0568E30785C33C0F6209F5F7B87D ** get_address_of__contexts_5() { return &____contexts_5; }
	inline void set__contexts_5(Contexts_t313FEE68C5FB0568E30785C33C0F6209F5F7B87D * value)
	{
		____contexts_5 = value;
		Il2CppCodeGenWriteBarrier((&____contexts_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ERTUGRULFEATURE_TC4183E2BE6E3B2EDB7F366467E3D8E03B5A00484_H
#ifndef ASYNCTASKMETHODBUILDER_1_T66ED1808B26B8081A2804D6A750D13386E360BD9_H
#define ASYNCTASKMETHODBUILDER_1_T66ED1808B26B8081A2804D6A750D13386E360BD9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Threading.Tasks.VoidTaskResult>
struct  AsyncTaskMethodBuilder_1_t66ED1808B26B8081A2804D6A750D13386E360BD9 
{
public:
	// System.Runtime.CompilerServices.AsyncMethodBuilderCore System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::m_coreState
	AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01  ___m_coreState_1;
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::m_task
	Task_1_t1359D75350E9D976BFA28AD96E417450DE277673 * ___m_task_2;

public:
	inline static int32_t get_offset_of_m_coreState_1() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_t66ED1808B26B8081A2804D6A750D13386E360BD9, ___m_coreState_1)); }
	inline AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01  get_m_coreState_1() const { return ___m_coreState_1; }
	inline AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01 * get_address_of_m_coreState_1() { return &___m_coreState_1; }
	inline void set_m_coreState_1(AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01  value)
	{
		___m_coreState_1 = value;
	}

	inline static int32_t get_offset_of_m_task_2() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_t66ED1808B26B8081A2804D6A750D13386E360BD9, ___m_task_2)); }
	inline Task_1_t1359D75350E9D976BFA28AD96E417450DE277673 * get_m_task_2() const { return ___m_task_2; }
	inline Task_1_t1359D75350E9D976BFA28AD96E417450DE277673 ** get_address_of_m_task_2() { return &___m_task_2; }
	inline void set_m_task_2(Task_1_t1359D75350E9D976BFA28AD96E417450DE277673 * value)
	{
		___m_task_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_task_2), value);
	}
};

struct AsyncTaskMethodBuilder_1_t66ED1808B26B8081A2804D6A750D13386E360BD9_StaticFields
{
public:
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::s_defaultResultTask
	Task_1_t1359D75350E9D976BFA28AD96E417450DE277673 * ___s_defaultResultTask_0;

public:
	inline static int32_t get_offset_of_s_defaultResultTask_0() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_t66ED1808B26B8081A2804D6A750D13386E360BD9_StaticFields, ___s_defaultResultTask_0)); }
	inline Task_1_t1359D75350E9D976BFA28AD96E417450DE277673 * get_s_defaultResultTask_0() const { return ___s_defaultResultTask_0; }
	inline Task_1_t1359D75350E9D976BFA28AD96E417450DE277673 ** get_address_of_s_defaultResultTask_0() { return &___s_defaultResultTask_0; }
	inline void set_s_defaultResultTask_0(Task_1_t1359D75350E9D976BFA28AD96E417450DE277673 * value)
	{
		___s_defaultResultTask_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_defaultResultTask_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCTASKMETHODBUILDER_1_T66ED1808B26B8081A2804D6A750D13386E360BD9_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef EFFECTRENDRERCOMPONENT_T4BCB77D9E1E31E2B6534FD49EC7FAF8AB1ADC28F_H
#define EFFECTRENDRERCOMPONENT_T4BCB77D9E1E31E2B6534FD49EC7FAF8AB1ADC28F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EffectRendrerComponent
struct  EffectRendrerComponent_t4BCB77D9E1E31E2B6534FD49EC7FAF8AB1ADC28F  : public RuntimeObject
{
public:
	// System.String EffectRendrerComponent::VisualId
	String_t* ___VisualId_0;
	// System.String EffectRendrerComponent::TargetId
	String_t* ___TargetId_1;
	// EffectVisualType EffectRendrerComponent::EffectVisualType
	int32_t ___EffectVisualType_2;

public:
	inline static int32_t get_offset_of_VisualId_0() { return static_cast<int32_t>(offsetof(EffectRendrerComponent_t4BCB77D9E1E31E2B6534FD49EC7FAF8AB1ADC28F, ___VisualId_0)); }
	inline String_t* get_VisualId_0() const { return ___VisualId_0; }
	inline String_t** get_address_of_VisualId_0() { return &___VisualId_0; }
	inline void set_VisualId_0(String_t* value)
	{
		___VisualId_0 = value;
		Il2CppCodeGenWriteBarrier((&___VisualId_0), value);
	}

	inline static int32_t get_offset_of_TargetId_1() { return static_cast<int32_t>(offsetof(EffectRendrerComponent_t4BCB77D9E1E31E2B6534FD49EC7FAF8AB1ADC28F, ___TargetId_1)); }
	inline String_t* get_TargetId_1() const { return ___TargetId_1; }
	inline String_t** get_address_of_TargetId_1() { return &___TargetId_1; }
	inline void set_TargetId_1(String_t* value)
	{
		___TargetId_1 = value;
		Il2CppCodeGenWriteBarrier((&___TargetId_1), value);
	}

	inline static int32_t get_offset_of_EffectVisualType_2() { return static_cast<int32_t>(offsetof(EffectRendrerComponent_t4BCB77D9E1E31E2B6534FD49EC7FAF8AB1ADC28F, ___EffectVisualType_2)); }
	inline int32_t get_EffectVisualType_2() const { return ___EffectVisualType_2; }
	inline int32_t* get_address_of_EffectVisualType_2() { return &___EffectVisualType_2; }
	inline void set_EffectVisualType_2(int32_t value)
	{
		___EffectVisualType_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EFFECTRENDRERCOMPONENT_T4BCB77D9E1E31E2B6534FD49EC7FAF8AB1ADC28F_H
#ifndef RENDERFEATURE_TD8D02B6D1FC897C71185766227AB45DC1E8B7D12_H
#define RENDERFEATURE_TD8D02B6D1FC897C71185766227AB45DC1E8B7D12_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RenderFeature
struct  RenderFeature_tD8D02B6D1FC897C71185766227AB45DC1E8B7D12  : public ErtugrulFeature_tC4183E2BE6E3B2EDB7F366467E3D8E03B5A00484
{
public:
	// FeaturesSwitchConfig RenderFeature::_featuresSwitchConfig
	FeaturesSwitchConfig_tBF1848651D45C05C78F36D68CB647F5CB2F802D3 * ____featuresSwitchConfig_6;

public:
	inline static int32_t get_offset_of__featuresSwitchConfig_6() { return static_cast<int32_t>(offsetof(RenderFeature_tD8D02B6D1FC897C71185766227AB45DC1E8B7D12, ____featuresSwitchConfig_6)); }
	inline FeaturesSwitchConfig_tBF1848651D45C05C78F36D68CB647F5CB2F802D3 * get__featuresSwitchConfig_6() const { return ____featuresSwitchConfig_6; }
	inline FeaturesSwitchConfig_tBF1848651D45C05C78F36D68CB647F5CB2F802D3 ** get_address_of__featuresSwitchConfig_6() { return &____featuresSwitchConfig_6; }
	inline void set__featuresSwitchConfig_6(FeaturesSwitchConfig_tBF1848651D45C05C78F36D68CB647F5CB2F802D3 * value)
	{
		____featuresSwitchConfig_6 = value;
		Il2CppCodeGenWriteBarrier((&____featuresSwitchConfig_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERFEATURE_TD8D02B6D1FC897C71185766227AB45DC1E8B7D12_H
#ifndef ASYNCTASKMETHODBUILDER_T0CD1893D670405BED201BE8CA6F2E811F2C0F487_H
#define ASYNCTASKMETHODBUILDER_T0CD1893D670405BED201BE8CA6F2E811F2C0F487_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.AsyncTaskMethodBuilder
struct  AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487 
{
public:
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Threading.Tasks.VoidTaskResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder::m_builder
	AsyncTaskMethodBuilder_1_t66ED1808B26B8081A2804D6A750D13386E360BD9  ___m_builder_1;

public:
	inline static int32_t get_offset_of_m_builder_1() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487, ___m_builder_1)); }
	inline AsyncTaskMethodBuilder_1_t66ED1808B26B8081A2804D6A750D13386E360BD9  get_m_builder_1() const { return ___m_builder_1; }
	inline AsyncTaskMethodBuilder_1_t66ED1808B26B8081A2804D6A750D13386E360BD9 * get_address_of_m_builder_1() { return &___m_builder_1; }
	inline void set_m_builder_1(AsyncTaskMethodBuilder_1_t66ED1808B26B8081A2804D6A750D13386E360BD9  value)
	{
		___m_builder_1 = value;
	}
};

struct AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487_StaticFields
{
public:
	// System.Threading.Tasks.Task`1<System.Threading.Tasks.VoidTaskResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder::s_cachedCompleted
	Task_1_t1359D75350E9D976BFA28AD96E417450DE277673 * ___s_cachedCompleted_0;

public:
	inline static int32_t get_offset_of_s_cachedCompleted_0() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487_StaticFields, ___s_cachedCompleted_0)); }
	inline Task_1_t1359D75350E9D976BFA28AD96E417450DE277673 * get_s_cachedCompleted_0() const { return ___s_cachedCompleted_0; }
	inline Task_1_t1359D75350E9D976BFA28AD96E417450DE277673 ** get_address_of_s_cachedCompleted_0() { return &___s_cachedCompleted_0; }
	inline void set_s_cachedCompleted_0(Task_1_t1359D75350E9D976BFA28AD96E417450DE277673 * value)
	{
		___s_cachedCompleted_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_cachedCompleted_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.CompilerServices.AsyncTaskMethodBuilder
struct AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487_marshaled_pinvoke
{
	AsyncTaskMethodBuilder_1_t66ED1808B26B8081A2804D6A750D13386E360BD9  ___m_builder_1;
};
// Native definition for COM marshalling of System.Runtime.CompilerServices.AsyncTaskMethodBuilder
struct AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487_marshaled_com
{
	AsyncTaskMethodBuilder_1_t66ED1808B26B8081A2804D6A750D13386E360BD9  ___m_builder_1;
};
#endif // ASYNCTASKMETHODBUILDER_T0CD1893D670405BED201BE8CA6F2E811F2C0F487_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef U3CONCONTINUEBTNU3ED__10_TB742710BD67850A20CB762A74781F5ECAE28B6FB_H
#define U3CONCONTINUEBTNU3ED__10_TB742710BD67850A20CB762A74781F5ECAE28B6FB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoalsTutorialNode_<OnContinueBtn>d__10
struct  U3COnContinueBtnU3Ed__10_tB742710BD67850A20CB762A74781F5ECAE28B6FB 
{
public:
	// System.Int32 GoalsTutorialNode_<OnContinueBtn>d__10::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder GoalsTutorialNode_<OnContinueBtn>d__10::<>t__builder
	AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  ___U3CU3Et__builder_1;
	// GoalsTutorialNode GoalsTutorialNode_<OnContinueBtn>d__10::<>4__this
	GoalsTutorialNode_tF301448520894286687F236F80234B8A56CBE7B1 * ___U3CU3E4__this_2;
	// System.Runtime.CompilerServices.TaskAwaiter GoalsTutorialNode_<OnContinueBtn>d__10::<>u__1
	TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  ___U3CU3Eu__1_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3COnContinueBtnU3Ed__10_tB742710BD67850A20CB762A74781F5ECAE28B6FB, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3COnContinueBtnU3Ed__10_tB742710BD67850A20CB762A74781F5ECAE28B6FB, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487 * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  value)
	{
		___U3CU3Et__builder_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3COnContinueBtnU3Ed__10_tB742710BD67850A20CB762A74781F5ECAE28B6FB, ___U3CU3E4__this_2)); }
	inline GoalsTutorialNode_tF301448520894286687F236F80234B8A56CBE7B1 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline GoalsTutorialNode_tF301448520894286687F236F80234B8A56CBE7B1 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(GoalsTutorialNode_tF301448520894286687F236F80234B8A56CBE7B1 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_3() { return static_cast<int32_t>(offsetof(U3COnContinueBtnU3Ed__10_tB742710BD67850A20CB762A74781F5ECAE28B6FB, ___U3CU3Eu__1_3)); }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  get_U3CU3Eu__1_3() const { return ___U3CU3Eu__1_3; }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F * get_address_of_U3CU3Eu__1_3() { return &___U3CU3Eu__1_3; }
	inline void set_U3CU3Eu__1_3(TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  value)
	{
		___U3CU3Eu__1_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONCONTINUEBTNU3ED__10_TB742710BD67850A20CB762A74781F5ECAE28B6FB_H
#ifndef U3CONBUTTONTAPU3ED__31_T10681CEEA7391B8166E0EF699E793CDFEF35F325_H
#define U3CONBUTTONTAPU3ED__31_T10681CEEA7391B8166E0EF699E793CDFEF35F325_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpellUIInitializer_<OnButtonTap>d__31
struct  U3COnButtonTapU3Ed__31_t10681CEEA7391B8166E0EF699E793CDFEF35F325 
{
public:
	// System.Int32 SpellUIInitializer_<OnButtonTap>d__31::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder SpellUIInitializer_<OnButtonTap>d__31::<>t__builder
	AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  ___U3CU3Et__builder_1;
	// System.Int32 SpellUIInitializer_<OnButtonTap>d__31::index
	int32_t ___index_2;
	// SpellUIInitializer SpellUIInitializer_<OnButtonTap>d__31::<>4__this
	SpellUIInitializer_t66B06A358BA3B762376CD13F820EFC620ADFDAD8 * ___U3CU3E4__this_3;
	// System.Runtime.CompilerServices.TaskAwaiter SpellUIInitializer_<OnButtonTap>d__31::<>u__1
	TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  ___U3CU3Eu__1_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3COnButtonTapU3Ed__31_t10681CEEA7391B8166E0EF699E793CDFEF35F325, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3COnButtonTapU3Ed__31_t10681CEEA7391B8166E0EF699E793CDFEF35F325, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487 * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  value)
	{
		___U3CU3Et__builder_1 = value;
	}

	inline static int32_t get_offset_of_index_2() { return static_cast<int32_t>(offsetof(U3COnButtonTapU3Ed__31_t10681CEEA7391B8166E0EF699E793CDFEF35F325, ___index_2)); }
	inline int32_t get_index_2() const { return ___index_2; }
	inline int32_t* get_address_of_index_2() { return &___index_2; }
	inline void set_index_2(int32_t value)
	{
		___index_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3COnButtonTapU3Ed__31_t10681CEEA7391B8166E0EF699E793CDFEF35F325, ___U3CU3E4__this_3)); }
	inline SpellUIInitializer_t66B06A358BA3B762376CD13F820EFC620ADFDAD8 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline SpellUIInitializer_t66B06A358BA3B762376CD13F820EFC620ADFDAD8 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(SpellUIInitializer_t66B06A358BA3B762376CD13F820EFC620ADFDAD8 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_4() { return static_cast<int32_t>(offsetof(U3COnButtonTapU3Ed__31_t10681CEEA7391B8166E0EF699E793CDFEF35F325, ___U3CU3Eu__1_4)); }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  get_U3CU3Eu__1_4() const { return ___U3CU3Eu__1_4; }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F * get_address_of_U3CU3Eu__1_4() { return &___U3CU3Eu__1_4; }
	inline void set_U3CU3Eu__1_4(TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  value)
	{
		___U3CU3Eu__1_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONBUTTONTAPU3ED__31_T10681CEEA7391B8166E0EF699E793CDFEF35F325_H
#ifndef U3CONTOWERTAPU3ED__32_T4CCF45D4B4489264E83E2C07CCABBC5485DC5910_H
#define U3CONTOWERTAPU3ED__32_T4CCF45D4B4489264E83E2C07CCABBC5485DC5910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpellUIInitializer_<OnTowerTap>d__32
struct  U3COnTowerTapU3Ed__32_t4CCF45D4B4489264E83E2C07CCABBC5485DC5910 
{
public:
	// System.Int32 SpellUIInitializer_<OnTowerTap>d__32::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder SpellUIInitializer_<OnTowerTap>d__32::<>t__builder
	AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  ___U3CU3Et__builder_1;
	// SpellUIInitializer SpellUIInitializer_<OnTowerTap>d__32::<>4__this
	SpellUIInitializer_t66B06A358BA3B762376CD13F820EFC620ADFDAD8 * ___U3CU3E4__this_2;
	// System.Runtime.CompilerServices.TaskAwaiter SpellUIInitializer_<OnTowerTap>d__32::<>u__1
	TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  ___U3CU3Eu__1_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3COnTowerTapU3Ed__32_t4CCF45D4B4489264E83E2C07CCABBC5485DC5910, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3COnTowerTapU3Ed__32_t4CCF45D4B4489264E83E2C07CCABBC5485DC5910, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487 * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  value)
	{
		___U3CU3Et__builder_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3COnTowerTapU3Ed__32_t4CCF45D4B4489264E83E2C07CCABBC5485DC5910, ___U3CU3E4__this_2)); }
	inline SpellUIInitializer_t66B06A358BA3B762376CD13F820EFC620ADFDAD8 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline SpellUIInitializer_t66B06A358BA3B762376CD13F820EFC620ADFDAD8 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(SpellUIInitializer_t66B06A358BA3B762376CD13F820EFC620ADFDAD8 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_3() { return static_cast<int32_t>(offsetof(U3COnTowerTapU3Ed__32_t4CCF45D4B4489264E83E2C07CCABBC5485DC5910, ___U3CU3Eu__1_3)); }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  get_U3CU3Eu__1_3() const { return ___U3CU3Eu__1_3; }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F * get_address_of_U3CU3Eu__1_3() { return &___U3CU3Eu__1_3; }
	inline void set_U3CU3Eu__1_3(TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  value)
	{
		___U3CU3Eu__1_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONTOWERTAPU3ED__32_T4CCF45D4B4489264E83E2C07CCABBC5485DC5910_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef CAMERASHAKE_T5EBAAD00C215BE104B246C1E17C22CDDEA0F3F7E_H
#define CAMERASHAKE_T5EBAAD00C215BE104B246C1E17C22CDDEA0F3F7E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraShake
struct  CameraShake_t5EBAAD00C215BE104B246C1E17C22CDDEA0F3F7E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean CameraShake::debugMode
	bool ___debugMode_4;
	// System.Single CameraShake::shakeAmount
	float ___shakeAmount_5;
	// System.Single CameraShake::shakeDuration
	float ___shakeDuration_6;
	// System.Single CameraShake::shakePercentage
	float ___shakePercentage_7;
	// System.Single CameraShake::startAmount
	float ___startAmount_8;
	// System.Single CameraShake::startDuration
	float ___startDuration_9;
	// System.Boolean CameraShake::isRunning
	bool ___isRunning_10;
	// System.Boolean CameraShake::smooth
	bool ___smooth_11;
	// System.Single CameraShake::smoothAmount
	float ___smoothAmount_12;

public:
	inline static int32_t get_offset_of_debugMode_4() { return static_cast<int32_t>(offsetof(CameraShake_t5EBAAD00C215BE104B246C1E17C22CDDEA0F3F7E, ___debugMode_4)); }
	inline bool get_debugMode_4() const { return ___debugMode_4; }
	inline bool* get_address_of_debugMode_4() { return &___debugMode_4; }
	inline void set_debugMode_4(bool value)
	{
		___debugMode_4 = value;
	}

	inline static int32_t get_offset_of_shakeAmount_5() { return static_cast<int32_t>(offsetof(CameraShake_t5EBAAD00C215BE104B246C1E17C22CDDEA0F3F7E, ___shakeAmount_5)); }
	inline float get_shakeAmount_5() const { return ___shakeAmount_5; }
	inline float* get_address_of_shakeAmount_5() { return &___shakeAmount_5; }
	inline void set_shakeAmount_5(float value)
	{
		___shakeAmount_5 = value;
	}

	inline static int32_t get_offset_of_shakeDuration_6() { return static_cast<int32_t>(offsetof(CameraShake_t5EBAAD00C215BE104B246C1E17C22CDDEA0F3F7E, ___shakeDuration_6)); }
	inline float get_shakeDuration_6() const { return ___shakeDuration_6; }
	inline float* get_address_of_shakeDuration_6() { return &___shakeDuration_6; }
	inline void set_shakeDuration_6(float value)
	{
		___shakeDuration_6 = value;
	}

	inline static int32_t get_offset_of_shakePercentage_7() { return static_cast<int32_t>(offsetof(CameraShake_t5EBAAD00C215BE104B246C1E17C22CDDEA0F3F7E, ___shakePercentage_7)); }
	inline float get_shakePercentage_7() const { return ___shakePercentage_7; }
	inline float* get_address_of_shakePercentage_7() { return &___shakePercentage_7; }
	inline void set_shakePercentage_7(float value)
	{
		___shakePercentage_7 = value;
	}

	inline static int32_t get_offset_of_startAmount_8() { return static_cast<int32_t>(offsetof(CameraShake_t5EBAAD00C215BE104B246C1E17C22CDDEA0F3F7E, ___startAmount_8)); }
	inline float get_startAmount_8() const { return ___startAmount_8; }
	inline float* get_address_of_startAmount_8() { return &___startAmount_8; }
	inline void set_startAmount_8(float value)
	{
		___startAmount_8 = value;
	}

	inline static int32_t get_offset_of_startDuration_9() { return static_cast<int32_t>(offsetof(CameraShake_t5EBAAD00C215BE104B246C1E17C22CDDEA0F3F7E, ___startDuration_9)); }
	inline float get_startDuration_9() const { return ___startDuration_9; }
	inline float* get_address_of_startDuration_9() { return &___startDuration_9; }
	inline void set_startDuration_9(float value)
	{
		___startDuration_9 = value;
	}

	inline static int32_t get_offset_of_isRunning_10() { return static_cast<int32_t>(offsetof(CameraShake_t5EBAAD00C215BE104B246C1E17C22CDDEA0F3F7E, ___isRunning_10)); }
	inline bool get_isRunning_10() const { return ___isRunning_10; }
	inline bool* get_address_of_isRunning_10() { return &___isRunning_10; }
	inline void set_isRunning_10(bool value)
	{
		___isRunning_10 = value;
	}

	inline static int32_t get_offset_of_smooth_11() { return static_cast<int32_t>(offsetof(CameraShake_t5EBAAD00C215BE104B246C1E17C22CDDEA0F3F7E, ___smooth_11)); }
	inline bool get_smooth_11() const { return ___smooth_11; }
	inline bool* get_address_of_smooth_11() { return &___smooth_11; }
	inline void set_smooth_11(bool value)
	{
		___smooth_11 = value;
	}

	inline static int32_t get_offset_of_smoothAmount_12() { return static_cast<int32_t>(offsetof(CameraShake_t5EBAAD00C215BE104B246C1E17C22CDDEA0F3F7E, ___smoothAmount_12)); }
	inline float get_smoothAmount_12() const { return ___smoothAmount_12; }
	inline float* get_address_of_smoothAmount_12() { return &___smoothAmount_12; }
	inline void set_smoothAmount_12(float value)
	{
		___smoothAmount_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERASHAKE_T5EBAAD00C215BE104B246C1E17C22CDDEA0F3F7E_H
#ifndef HERODROPRENDERSYSTEM_T5F1458141D724DEB03AE50BD90C8E8C4AE8A55BF_H
#define HERODROPRENDERSYSTEM_T5F1458141D724DEB03AE50BD90C8E8C4AE8A55BF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HeroDropRenderSystem
struct  HeroDropRenderSystem_t5F1458141D724DEB03AE50BD90C8E8C4AE8A55BF  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HERODROPRENDERSYSTEM_T5F1458141D724DEB03AE50BD90C8E8C4AE8A55BF_H
#ifndef SMOOTHFOLLOW_T838D162CB27CCC06F9329D5E50959FE5C9EFA5E9_H
#define SMOOTHFOLLOW_T838D162CB27CCC06F9329D5E50959FE5C9EFA5E9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SmoothFollow
struct  SmoothFollow_t838D162CB27CCC06F9329D5E50959FE5C9EFA5E9  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Transform SmoothFollow::target
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___target_4;
	// System.Single SmoothFollow::distance
	float ___distance_5;
	// System.Single SmoothFollow::height
	float ___height_6;
	// System.Single SmoothFollow::heightDamping
	float ___heightDamping_7;
	// System.Single SmoothFollow::rotationDamping
	float ___rotationDamping_8;

public:
	inline static int32_t get_offset_of_target_4() { return static_cast<int32_t>(offsetof(SmoothFollow_t838D162CB27CCC06F9329D5E50959FE5C9EFA5E9, ___target_4)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_target_4() const { return ___target_4; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_target_4() { return &___target_4; }
	inline void set_target_4(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___target_4 = value;
		Il2CppCodeGenWriteBarrier((&___target_4), value);
	}

	inline static int32_t get_offset_of_distance_5() { return static_cast<int32_t>(offsetof(SmoothFollow_t838D162CB27CCC06F9329D5E50959FE5C9EFA5E9, ___distance_5)); }
	inline float get_distance_5() const { return ___distance_5; }
	inline float* get_address_of_distance_5() { return &___distance_5; }
	inline void set_distance_5(float value)
	{
		___distance_5 = value;
	}

	inline static int32_t get_offset_of_height_6() { return static_cast<int32_t>(offsetof(SmoothFollow_t838D162CB27CCC06F9329D5E50959FE5C9EFA5E9, ___height_6)); }
	inline float get_height_6() const { return ___height_6; }
	inline float* get_address_of_height_6() { return &___height_6; }
	inline void set_height_6(float value)
	{
		___height_6 = value;
	}

	inline static int32_t get_offset_of_heightDamping_7() { return static_cast<int32_t>(offsetof(SmoothFollow_t838D162CB27CCC06F9329D5E50959FE5C9EFA5E9, ___heightDamping_7)); }
	inline float get_heightDamping_7() const { return ___heightDamping_7; }
	inline float* get_address_of_heightDamping_7() { return &___heightDamping_7; }
	inline void set_heightDamping_7(float value)
	{
		___heightDamping_7 = value;
	}

	inline static int32_t get_offset_of_rotationDamping_8() { return static_cast<int32_t>(offsetof(SmoothFollow_t838D162CB27CCC06F9329D5E50959FE5C9EFA5E9, ___rotationDamping_8)); }
	inline float get_rotationDamping_8() const { return ___rotationDamping_8; }
	inline float* get_address_of_rotationDamping_8() { return &___rotationDamping_8; }
	inline void set_rotationDamping_8(float value)
	{
		___rotationDamping_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SMOOTHFOLLOW_T838D162CB27CCC06F9329D5E50959FE5C9EFA5E9_H
#ifndef TMONOBEHAVIOUR_TC5EA4E5C9CF2F8B1360110F3DCB3CA86F7D68FF3_H
#define TMONOBEHAVIOUR_TC5EA4E5C9CF2F8B1360110F3DCB3CA86F7D68FF3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.TMonoBehaviour
struct  TMonoBehaviour_tC5EA4E5C9CF2F8B1360110F3DCB3CA86F7D68FF3  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMONOBEHAVIOUR_TC5EA4E5C9CF2F8B1360110F3DCB3CA86F7D68FF3_H
#ifndef BATTLEINVENTORYCOMPONENT_TB8F3758E6C052D9F512425DEB3F4C3050833797A_H
#define BATTLEINVENTORYCOMPONENT_TB8F3758E6C052D9F512425DEB3F4C3050833797A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BattleInventoryComponent
struct  BattleInventoryComponent_tB8F3758E6C052D9F512425DEB3F4C3050833797A  : public TMonoBehaviour_tC5EA4E5C9CF2F8B1360110F3DCB3CA86F7D68FF3
{
public:
	// Tayr.TList BattleInventoryComponent::_list
	TList_tB67CEA10085B72B7C69ACE7FCF01FCC38E6EF292 * ____list_4;
	// System.Int32 BattleInventoryComponent::_inventorySize
	int32_t ____inventorySize_5;

public:
	inline static int32_t get_offset_of__list_4() { return static_cast<int32_t>(offsetof(BattleInventoryComponent_tB8F3758E6C052D9F512425DEB3F4C3050833797A, ____list_4)); }
	inline TList_tB67CEA10085B72B7C69ACE7FCF01FCC38E6EF292 * get__list_4() const { return ____list_4; }
	inline TList_tB67CEA10085B72B7C69ACE7FCF01FCC38E6EF292 ** get_address_of__list_4() { return &____list_4; }
	inline void set__list_4(TList_tB67CEA10085B72B7C69ACE7FCF01FCC38E6EF292 * value)
	{
		____list_4 = value;
		Il2CppCodeGenWriteBarrier((&____list_4), value);
	}

	inline static int32_t get_offset_of__inventorySize_5() { return static_cast<int32_t>(offsetof(BattleInventoryComponent_tB8F3758E6C052D9F512425DEB3F4C3050833797A, ____inventorySize_5)); }
	inline int32_t get__inventorySize_5() const { return ____inventorySize_5; }
	inline int32_t* get_address_of__inventorySize_5() { return &____inventorySize_5; }
	inline void set__inventorySize_5(int32_t value)
	{
		____inventorySize_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BATTLEINVENTORYCOMPONENT_TB8F3758E6C052D9F512425DEB3F4C3050833797A_H
#ifndef DAMAGEKICKER_T32D75279096E1962C07DA19EDE11BE5EFEADC5FD_H
#define DAMAGEKICKER_T32D75279096E1962C07DA19EDE11BE5EFEADC5FD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DamageKicker
struct  DamageKicker_t32D75279096E1962C07DA19EDE11BE5EFEADC5FD  : public TMonoBehaviour_tC5EA4E5C9CF2F8B1360110F3DCB3CA86F7D68FF3
{
public:
	// TMPro.TextMeshProUGUI DamageKicker::_text
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____text_4;
	// System.Single DamageKicker::_accumulateDuration
	float ____accumulateDuration_5;
	// System.Single DamageKicker::_lastAccumulation
	float ____lastAccumulation_6;
	// System.Single DamageKicker::_damageAccumulated
	float ____damageAccumulated_7;
	// UnityEngine.Vector3 DamageKicker::_punchVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ____punchVector_8;
	// DG.Tweening.Tweener DamageKicker::_tweener
	Tweener_t9B2A5E94EE6D11F7607E58AE4E37186FF63587C8 * ____tweener_9;

public:
	inline static int32_t get_offset_of__text_4() { return static_cast<int32_t>(offsetof(DamageKicker_t32D75279096E1962C07DA19EDE11BE5EFEADC5FD, ____text_4)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__text_4() const { return ____text_4; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__text_4() { return &____text_4; }
	inline void set__text_4(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____text_4 = value;
		Il2CppCodeGenWriteBarrier((&____text_4), value);
	}

	inline static int32_t get_offset_of__accumulateDuration_5() { return static_cast<int32_t>(offsetof(DamageKicker_t32D75279096E1962C07DA19EDE11BE5EFEADC5FD, ____accumulateDuration_5)); }
	inline float get__accumulateDuration_5() const { return ____accumulateDuration_5; }
	inline float* get_address_of__accumulateDuration_5() { return &____accumulateDuration_5; }
	inline void set__accumulateDuration_5(float value)
	{
		____accumulateDuration_5 = value;
	}

	inline static int32_t get_offset_of__lastAccumulation_6() { return static_cast<int32_t>(offsetof(DamageKicker_t32D75279096E1962C07DA19EDE11BE5EFEADC5FD, ____lastAccumulation_6)); }
	inline float get__lastAccumulation_6() const { return ____lastAccumulation_6; }
	inline float* get_address_of__lastAccumulation_6() { return &____lastAccumulation_6; }
	inline void set__lastAccumulation_6(float value)
	{
		____lastAccumulation_6 = value;
	}

	inline static int32_t get_offset_of__damageAccumulated_7() { return static_cast<int32_t>(offsetof(DamageKicker_t32D75279096E1962C07DA19EDE11BE5EFEADC5FD, ____damageAccumulated_7)); }
	inline float get__damageAccumulated_7() const { return ____damageAccumulated_7; }
	inline float* get_address_of__damageAccumulated_7() { return &____damageAccumulated_7; }
	inline void set__damageAccumulated_7(float value)
	{
		____damageAccumulated_7 = value;
	}

	inline static int32_t get_offset_of__punchVector_8() { return static_cast<int32_t>(offsetof(DamageKicker_t32D75279096E1962C07DA19EDE11BE5EFEADC5FD, ____punchVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get__punchVector_8() const { return ____punchVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of__punchVector_8() { return &____punchVector_8; }
	inline void set__punchVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		____punchVector_8 = value;
	}

	inline static int32_t get_offset_of__tweener_9() { return static_cast<int32_t>(offsetof(DamageKicker_t32D75279096E1962C07DA19EDE11BE5EFEADC5FD, ____tweener_9)); }
	inline Tweener_t9B2A5E94EE6D11F7607E58AE4E37186FF63587C8 * get__tweener_9() const { return ____tweener_9; }
	inline Tweener_t9B2A5E94EE6D11F7607E58AE4E37186FF63587C8 ** get_address_of__tweener_9() { return &____tweener_9; }
	inline void set__tweener_9(Tweener_t9B2A5E94EE6D11F7607E58AE4E37186FF63587C8 * value)
	{
		____tweener_9 = value;
		Il2CppCodeGenWriteBarrier((&____tweener_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DAMAGEKICKER_T32D75279096E1962C07DA19EDE11BE5EFEADC5FD_H
#ifndef HPBAR_TB262BDBB4A983FC76CE9D9AE362FB00D608A93F9_H
#define HPBAR_TB262BDBB4A983FC76CE9D9AE362FB00D608A93F9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HpBar
struct  HpBar_tB262BDBB4A983FC76CE9D9AE362FB00D608A93F9  : public TMonoBehaviour_tC5EA4E5C9CF2F8B1360110F3DCB3CA86F7D68FF3
{
public:
	// UnityEngine.UI.Image HpBar::_uiHp
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____uiHp_4;

public:
	inline static int32_t get_offset_of__uiHp_4() { return static_cast<int32_t>(offsetof(HpBar_tB262BDBB4A983FC76CE9D9AE362FB00D608A93F9, ____uiHp_4)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__uiHp_4() const { return ____uiHp_4; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__uiHp_4() { return &____uiHp_4; }
	inline void set__uiHp_4(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____uiHp_4 = value;
		Il2CppCodeGenWriteBarrier((&____uiHp_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HPBAR_TB262BDBB4A983FC76CE9D9AE362FB00D608A93F9_H
#ifndef HPUIBEHAVIOR_TAFD1C3F31638942C35946D970A36E72F44595CF7_H
#define HPUIBEHAVIOR_TAFD1C3F31638942C35946D970A36E72F44595CF7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HpUIBehavior
struct  HpUIBehavior_tAFD1C3F31638942C35946D970A36E72F44595CF7  : public TMonoBehaviour_tC5EA4E5C9CF2F8B1360110F3DCB3CA86F7D68FF3
{
public:
	// UnityEngine.UI.Image HpUIBehavior::_progress
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____progress_4;
	// TMPro.TextMeshProUGUI HpUIBehavior::_hpText
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____hpText_5;

public:
	inline static int32_t get_offset_of__progress_4() { return static_cast<int32_t>(offsetof(HpUIBehavior_tAFD1C3F31638942C35946D970A36E72F44595CF7, ____progress_4)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__progress_4() const { return ____progress_4; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__progress_4() { return &____progress_4; }
	inline void set__progress_4(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____progress_4 = value;
		Il2CppCodeGenWriteBarrier((&____progress_4), value);
	}

	inline static int32_t get_offset_of__hpText_5() { return static_cast<int32_t>(offsetof(HpUIBehavior_tAFD1C3F31638942C35946D970A36E72F44595CF7, ____hpText_5)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__hpText_5() const { return ____hpText_5; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__hpText_5() { return &____hpText_5; }
	inline void set__hpText_5(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____hpText_5 = value;
		Il2CppCodeGenWriteBarrier((&____hpText_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HPUIBEHAVIOR_TAFD1C3F31638942C35946D970A36E72F44595CF7_H
#ifndef BASICNODE_1_T56EF59E5C4BF3DD3B0683550A56A5B17B8A2520D_H
#define BASICNODE_1_T56EF59E5C4BF3DD3B0683550A56A5B17B8A2520D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.BasicNode`1<CustomTutorialModel>
struct  BasicNode_1_t56EF59E5C4BF3DD3B0683550A56A5B17B8A2520D  : public TMonoBehaviour_tC5EA4E5C9CF2F8B1360110F3DCB3CA86F7D68FF3
{
public:
	// T Tayr.BasicNode`1::Data
	CustomTutorialModel_t7F1E6EF5B6AF58AEA9FB2573811B4562E587E635 * ___Data_4;
	// Tayr.INodeAnimationHandler Tayr.BasicNode`1::_nodeAnimationHandler
	RuntimeObject* ____nodeAnimationHandler_5;
	// Tayr.NodeAnimator Tayr.BasicNode`1::_nodeAnimator
	NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F * ____nodeAnimator_6;

public:
	inline static int32_t get_offset_of_Data_4() { return static_cast<int32_t>(offsetof(BasicNode_1_t56EF59E5C4BF3DD3B0683550A56A5B17B8A2520D, ___Data_4)); }
	inline CustomTutorialModel_t7F1E6EF5B6AF58AEA9FB2573811B4562E587E635 * get_Data_4() const { return ___Data_4; }
	inline CustomTutorialModel_t7F1E6EF5B6AF58AEA9FB2573811B4562E587E635 ** get_address_of_Data_4() { return &___Data_4; }
	inline void set_Data_4(CustomTutorialModel_t7F1E6EF5B6AF58AEA9FB2573811B4562E587E635 * value)
	{
		___Data_4 = value;
		Il2CppCodeGenWriteBarrier((&___Data_4), value);
	}

	inline static int32_t get_offset_of__nodeAnimationHandler_5() { return static_cast<int32_t>(offsetof(BasicNode_1_t56EF59E5C4BF3DD3B0683550A56A5B17B8A2520D, ____nodeAnimationHandler_5)); }
	inline RuntimeObject* get__nodeAnimationHandler_5() const { return ____nodeAnimationHandler_5; }
	inline RuntimeObject** get_address_of__nodeAnimationHandler_5() { return &____nodeAnimationHandler_5; }
	inline void set__nodeAnimationHandler_5(RuntimeObject* value)
	{
		____nodeAnimationHandler_5 = value;
		Il2CppCodeGenWriteBarrier((&____nodeAnimationHandler_5), value);
	}

	inline static int32_t get_offset_of__nodeAnimator_6() { return static_cast<int32_t>(offsetof(BasicNode_1_t56EF59E5C4BF3DD3B0683550A56A5B17B8A2520D, ____nodeAnimator_6)); }
	inline NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F * get__nodeAnimator_6() const { return ____nodeAnimator_6; }
	inline NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F ** get_address_of__nodeAnimator_6() { return &____nodeAnimator_6; }
	inline void set__nodeAnimator_6(NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F * value)
	{
		____nodeAnimator_6 = value;
		Il2CppCodeGenWriteBarrier((&____nodeAnimator_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICNODE_1_T56EF59E5C4BF3DD3B0683550A56A5B17B8A2520D_H
#ifndef BASICNODE_1_TC15963AAB43917EBFB80C97B165DDBD6C98E39BB_H
#define BASICNODE_1_TC15963AAB43917EBFB80C97B165DDBD6C98E39BB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.BasicNode`1<GameContext>
struct  BasicNode_1_tC15963AAB43917EBFB80C97B165DDBD6C98E39BB  : public TMonoBehaviour_tC5EA4E5C9CF2F8B1360110F3DCB3CA86F7D68FF3
{
public:
	// T Tayr.BasicNode`1::Data
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ___Data_4;
	// Tayr.INodeAnimationHandler Tayr.BasicNode`1::_nodeAnimationHandler
	RuntimeObject* ____nodeAnimationHandler_5;
	// Tayr.NodeAnimator Tayr.BasicNode`1::_nodeAnimator
	NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F * ____nodeAnimator_6;

public:
	inline static int32_t get_offset_of_Data_4() { return static_cast<int32_t>(offsetof(BasicNode_1_tC15963AAB43917EBFB80C97B165DDBD6C98E39BB, ___Data_4)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get_Data_4() const { return ___Data_4; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of_Data_4() { return &___Data_4; }
	inline void set_Data_4(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		___Data_4 = value;
		Il2CppCodeGenWriteBarrier((&___Data_4), value);
	}

	inline static int32_t get_offset_of__nodeAnimationHandler_5() { return static_cast<int32_t>(offsetof(BasicNode_1_tC15963AAB43917EBFB80C97B165DDBD6C98E39BB, ____nodeAnimationHandler_5)); }
	inline RuntimeObject* get__nodeAnimationHandler_5() const { return ____nodeAnimationHandler_5; }
	inline RuntimeObject** get_address_of__nodeAnimationHandler_5() { return &____nodeAnimationHandler_5; }
	inline void set__nodeAnimationHandler_5(RuntimeObject* value)
	{
		____nodeAnimationHandler_5 = value;
		Il2CppCodeGenWriteBarrier((&____nodeAnimationHandler_5), value);
	}

	inline static int32_t get_offset_of__nodeAnimator_6() { return static_cast<int32_t>(offsetof(BasicNode_1_tC15963AAB43917EBFB80C97B165DDBD6C98E39BB, ____nodeAnimator_6)); }
	inline NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F * get__nodeAnimator_6() const { return ____nodeAnimator_6; }
	inline NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F ** get_address_of__nodeAnimator_6() { return &____nodeAnimator_6; }
	inline void set__nodeAnimator_6(NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F * value)
	{
		____nodeAnimator_6 = value;
		Il2CppCodeGenWriteBarrier((&____nodeAnimator_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICNODE_1_TC15963AAB43917EBFB80C97B165DDBD6C98E39BB_H
#ifndef BASICNODE_1_T69966F6A6017B9F9944A0DFB087909A1F61C2D76_H
#define BASICNODE_1_T69966F6A6017B9F9944A0DFB087909A1F61C2D76_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.BasicNode`1<LoseSummaryNodeData>
struct  BasicNode_1_t69966F6A6017B9F9944A0DFB087909A1F61C2D76  : public TMonoBehaviour_tC5EA4E5C9CF2F8B1360110F3DCB3CA86F7D68FF3
{
public:
	// T Tayr.BasicNode`1::Data
	LoseSummaryNodeData_t9807706F230D328167B227F082922DBD1C0BAA36 * ___Data_4;
	// Tayr.INodeAnimationHandler Tayr.BasicNode`1::_nodeAnimationHandler
	RuntimeObject* ____nodeAnimationHandler_5;
	// Tayr.NodeAnimator Tayr.BasicNode`1::_nodeAnimator
	NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F * ____nodeAnimator_6;

public:
	inline static int32_t get_offset_of_Data_4() { return static_cast<int32_t>(offsetof(BasicNode_1_t69966F6A6017B9F9944A0DFB087909A1F61C2D76, ___Data_4)); }
	inline LoseSummaryNodeData_t9807706F230D328167B227F082922DBD1C0BAA36 * get_Data_4() const { return ___Data_4; }
	inline LoseSummaryNodeData_t9807706F230D328167B227F082922DBD1C0BAA36 ** get_address_of_Data_4() { return &___Data_4; }
	inline void set_Data_4(LoseSummaryNodeData_t9807706F230D328167B227F082922DBD1C0BAA36 * value)
	{
		___Data_4 = value;
		Il2CppCodeGenWriteBarrier((&___Data_4), value);
	}

	inline static int32_t get_offset_of__nodeAnimationHandler_5() { return static_cast<int32_t>(offsetof(BasicNode_1_t69966F6A6017B9F9944A0DFB087909A1F61C2D76, ____nodeAnimationHandler_5)); }
	inline RuntimeObject* get__nodeAnimationHandler_5() const { return ____nodeAnimationHandler_5; }
	inline RuntimeObject** get_address_of__nodeAnimationHandler_5() { return &____nodeAnimationHandler_5; }
	inline void set__nodeAnimationHandler_5(RuntimeObject* value)
	{
		____nodeAnimationHandler_5 = value;
		Il2CppCodeGenWriteBarrier((&____nodeAnimationHandler_5), value);
	}

	inline static int32_t get_offset_of__nodeAnimator_6() { return static_cast<int32_t>(offsetof(BasicNode_1_t69966F6A6017B9F9944A0DFB087909A1F61C2D76, ____nodeAnimator_6)); }
	inline NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F * get__nodeAnimator_6() const { return ____nodeAnimator_6; }
	inline NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F ** get_address_of__nodeAnimator_6() { return &____nodeAnimator_6; }
	inline void set__nodeAnimator_6(NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F * value)
	{
		____nodeAnimator_6 = value;
		Il2CppCodeGenWriteBarrier((&____nodeAnimator_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICNODE_1_T69966F6A6017B9F9944A0DFB087909A1F61C2D76_H
#ifndef BASICNODE_1_T80C898A8840F148EAC8A5FE47B08C3ABA79CE64D_H
#define BASICNODE_1_T80C898A8840F148EAC8A5FE47B08C3ABA79CE64D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.BasicNode`1<System.Int32>
struct  BasicNode_1_t80C898A8840F148EAC8A5FE47B08C3ABA79CE64D  : public TMonoBehaviour_tC5EA4E5C9CF2F8B1360110F3DCB3CA86F7D68FF3
{
public:
	// T Tayr.BasicNode`1::Data
	int32_t ___Data_4;
	// Tayr.INodeAnimationHandler Tayr.BasicNode`1::_nodeAnimationHandler
	RuntimeObject* ____nodeAnimationHandler_5;
	// Tayr.NodeAnimator Tayr.BasicNode`1::_nodeAnimator
	NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F * ____nodeAnimator_6;

public:
	inline static int32_t get_offset_of_Data_4() { return static_cast<int32_t>(offsetof(BasicNode_1_t80C898A8840F148EAC8A5FE47B08C3ABA79CE64D, ___Data_4)); }
	inline int32_t get_Data_4() const { return ___Data_4; }
	inline int32_t* get_address_of_Data_4() { return &___Data_4; }
	inline void set_Data_4(int32_t value)
	{
		___Data_4 = value;
	}

	inline static int32_t get_offset_of__nodeAnimationHandler_5() { return static_cast<int32_t>(offsetof(BasicNode_1_t80C898A8840F148EAC8A5FE47B08C3ABA79CE64D, ____nodeAnimationHandler_5)); }
	inline RuntimeObject* get__nodeAnimationHandler_5() const { return ____nodeAnimationHandler_5; }
	inline RuntimeObject** get_address_of__nodeAnimationHandler_5() { return &____nodeAnimationHandler_5; }
	inline void set__nodeAnimationHandler_5(RuntimeObject* value)
	{
		____nodeAnimationHandler_5 = value;
		Il2CppCodeGenWriteBarrier((&____nodeAnimationHandler_5), value);
	}

	inline static int32_t get_offset_of__nodeAnimator_6() { return static_cast<int32_t>(offsetof(BasicNode_1_t80C898A8840F148EAC8A5FE47B08C3ABA79CE64D, ____nodeAnimator_6)); }
	inline NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F * get__nodeAnimator_6() const { return ____nodeAnimator_6; }
	inline NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F ** get_address_of__nodeAnimator_6() { return &____nodeAnimator_6; }
	inline void set__nodeAnimator_6(NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F * value)
	{
		____nodeAnimator_6 = value;
		Il2CppCodeGenWriteBarrier((&____nodeAnimator_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICNODE_1_T80C898A8840F148EAC8A5FE47B08C3ABA79CE64D_H
#ifndef BASICNODE_1_T31F5A99F9379D18DE55F22D14196E346D7C1FADB_H
#define BASICNODE_1_T31F5A99F9379D18DE55F22D14196E346D7C1FADB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.BasicNode`1<System.String>
struct  BasicNode_1_t31F5A99F9379D18DE55F22D14196E346D7C1FADB  : public TMonoBehaviour_tC5EA4E5C9CF2F8B1360110F3DCB3CA86F7D68FF3
{
public:
	// T Tayr.BasicNode`1::Data
	String_t* ___Data_4;
	// Tayr.INodeAnimationHandler Tayr.BasicNode`1::_nodeAnimationHandler
	RuntimeObject* ____nodeAnimationHandler_5;
	// Tayr.NodeAnimator Tayr.BasicNode`1::_nodeAnimator
	NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F * ____nodeAnimator_6;

public:
	inline static int32_t get_offset_of_Data_4() { return static_cast<int32_t>(offsetof(BasicNode_1_t31F5A99F9379D18DE55F22D14196E346D7C1FADB, ___Data_4)); }
	inline String_t* get_Data_4() const { return ___Data_4; }
	inline String_t** get_address_of_Data_4() { return &___Data_4; }
	inline void set_Data_4(String_t* value)
	{
		___Data_4 = value;
		Il2CppCodeGenWriteBarrier((&___Data_4), value);
	}

	inline static int32_t get_offset_of__nodeAnimationHandler_5() { return static_cast<int32_t>(offsetof(BasicNode_1_t31F5A99F9379D18DE55F22D14196E346D7C1FADB, ____nodeAnimationHandler_5)); }
	inline RuntimeObject* get__nodeAnimationHandler_5() const { return ____nodeAnimationHandler_5; }
	inline RuntimeObject** get_address_of__nodeAnimationHandler_5() { return &____nodeAnimationHandler_5; }
	inline void set__nodeAnimationHandler_5(RuntimeObject* value)
	{
		____nodeAnimationHandler_5 = value;
		Il2CppCodeGenWriteBarrier((&____nodeAnimationHandler_5), value);
	}

	inline static int32_t get_offset_of__nodeAnimator_6() { return static_cast<int32_t>(offsetof(BasicNode_1_t31F5A99F9379D18DE55F22D14196E346D7C1FADB, ____nodeAnimator_6)); }
	inline NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F * get__nodeAnimator_6() const { return ____nodeAnimator_6; }
	inline NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F ** get_address_of__nodeAnimator_6() { return &____nodeAnimator_6; }
	inline void set__nodeAnimator_6(NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F * value)
	{
		____nodeAnimator_6 = value;
		Il2CppCodeGenWriteBarrier((&____nodeAnimator_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICNODE_1_T31F5A99F9379D18DE55F22D14196E346D7C1FADB_H
#ifndef BASICNODE_1_T2B66230344420DCD7C3E07CA1F5ACAD424BB1EC5_H
#define BASICNODE_1_T2B66230344420DCD7C3E07CA1F5ACAD424BB1EC5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.BasicNode`1<WinSummaryNodeData>
struct  BasicNode_1_t2B66230344420DCD7C3E07CA1F5ACAD424BB1EC5  : public TMonoBehaviour_tC5EA4E5C9CF2F8B1360110F3DCB3CA86F7D68FF3
{
public:
	// T Tayr.BasicNode`1::Data
	WinSummaryNodeData_tD1C3F590D3B3A7D085A783296AD766C9DDA98DEE * ___Data_4;
	// Tayr.INodeAnimationHandler Tayr.BasicNode`1::_nodeAnimationHandler
	RuntimeObject* ____nodeAnimationHandler_5;
	// Tayr.NodeAnimator Tayr.BasicNode`1::_nodeAnimator
	NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F * ____nodeAnimator_6;

public:
	inline static int32_t get_offset_of_Data_4() { return static_cast<int32_t>(offsetof(BasicNode_1_t2B66230344420DCD7C3E07CA1F5ACAD424BB1EC5, ___Data_4)); }
	inline WinSummaryNodeData_tD1C3F590D3B3A7D085A783296AD766C9DDA98DEE * get_Data_4() const { return ___Data_4; }
	inline WinSummaryNodeData_tD1C3F590D3B3A7D085A783296AD766C9DDA98DEE ** get_address_of_Data_4() { return &___Data_4; }
	inline void set_Data_4(WinSummaryNodeData_tD1C3F590D3B3A7D085A783296AD766C9DDA98DEE * value)
	{
		___Data_4 = value;
		Il2CppCodeGenWriteBarrier((&___Data_4), value);
	}

	inline static int32_t get_offset_of__nodeAnimationHandler_5() { return static_cast<int32_t>(offsetof(BasicNode_1_t2B66230344420DCD7C3E07CA1F5ACAD424BB1EC5, ____nodeAnimationHandler_5)); }
	inline RuntimeObject* get__nodeAnimationHandler_5() const { return ____nodeAnimationHandler_5; }
	inline RuntimeObject** get_address_of__nodeAnimationHandler_5() { return &____nodeAnimationHandler_5; }
	inline void set__nodeAnimationHandler_5(RuntimeObject* value)
	{
		____nodeAnimationHandler_5 = value;
		Il2CppCodeGenWriteBarrier((&____nodeAnimationHandler_5), value);
	}

	inline static int32_t get_offset_of__nodeAnimator_6() { return static_cast<int32_t>(offsetof(BasicNode_1_t2B66230344420DCD7C3E07CA1F5ACAD424BB1EC5, ____nodeAnimator_6)); }
	inline NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F * get__nodeAnimator_6() const { return ____nodeAnimator_6; }
	inline NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F ** get_address_of__nodeAnimator_6() { return &____nodeAnimator_6; }
	inline void set__nodeAnimator_6(NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F * value)
	{
		____nodeAnimator_6 = value;
		Il2CppCodeGenWriteBarrier((&____nodeAnimator_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICNODE_1_T2B66230344420DCD7C3E07CA1F5ACAD424BB1EC5_H
#ifndef TLISTITEM_T3835B3A6EDB64421C763D6938505136E6CD7628C_H
#define TLISTITEM_T3835B3A6EDB64421C763D6938505136E6CD7628C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.TListItem
struct  TListItem_t3835B3A6EDB64421C763D6938505136E6CD7628C  : public TMonoBehaviour_tC5EA4E5C9CF2F8B1360110F3DCB3CA86F7D68FF3
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLISTITEM_T3835B3A6EDB64421C763D6938505136E6CD7628C_H
#ifndef TIMECOUNTERUICOMPONENT_T3AD995E2AC46D2BF82AF866C4F7651BF9928E245_H
#define TIMECOUNTERUICOMPONENT_T3AD995E2AC46D2BF82AF866C4F7651BF9928E245_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TimeCounterUIComponent
struct  TimeCounterUIComponent_t3AD995E2AC46D2BF82AF866C4F7651BF9928E245  : public TMonoBehaviour_tC5EA4E5C9CF2F8B1360110F3DCB3CA86F7D68FF3
{
public:
	// TMPro.TextMeshProUGUI TimeCounterUIComponent::_counter
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____counter_4;
	// UnityEngine.Animator TimeCounterUIComponent::_animator
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ____animator_5;

public:
	inline static int32_t get_offset_of__counter_4() { return static_cast<int32_t>(offsetof(TimeCounterUIComponent_t3AD995E2AC46D2BF82AF866C4F7651BF9928E245, ____counter_4)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__counter_4() const { return ____counter_4; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__counter_4() { return &____counter_4; }
	inline void set__counter_4(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____counter_4 = value;
		Il2CppCodeGenWriteBarrier((&____counter_4), value);
	}

	inline static int32_t get_offset_of__animator_5() { return static_cast<int32_t>(offsetof(TimeCounterUIComponent_t3AD995E2AC46D2BF82AF866C4F7651BF9928E245, ____animator_5)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get__animator_5() const { return ____animator_5; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of__animator_5() { return &____animator_5; }
	inline void set__animator_5(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		____animator_5 = value;
		Il2CppCodeGenWriteBarrier((&____animator_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMECOUNTERUICOMPONENT_T3AD995E2AC46D2BF82AF866C4F7651BF9928E245_H
#ifndef UICALLOUTBEHAVIOR_T565A832FB1E549570845B819FA71B4CA6CA119F4_H
#define UICALLOUTBEHAVIOR_T565A832FB1E549570845B819FA71B4CA6CA119F4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UICalloutBehavior
struct  UICalloutBehavior_t565A832FB1E549570845B819FA71B4CA6CA119F4  : public TMonoBehaviour_tC5EA4E5C9CF2F8B1360110F3DCB3CA86F7D68FF3
{
public:
	// TMPro.TextMeshProUGUI UICalloutBehavior::_description
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____description_4;

public:
	inline static int32_t get_offset_of__description_4() { return static_cast<int32_t>(offsetof(UICalloutBehavior_t565A832FB1E549570845B819FA71B4CA6CA119F4, ____description_4)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__description_4() const { return ____description_4; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__description_4() { return &____description_4; }
	inline void set__description_4(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____description_4 = value;
		Il2CppCodeGenWriteBarrier((&____description_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UICALLOUTBEHAVIOR_T565A832FB1E549570845B819FA71B4CA6CA119F4_H
#ifndef UIOBJECTIVE_T94D85D5DE412998C059AE6A45DB99258B95E8023_H
#define UIOBJECTIVE_T94D85D5DE412998C059AE6A45DB99258B95E8023_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIObjective
struct  UIObjective_t94D85D5DE412998C059AE6A45DB99258B95E8023  : public TMonoBehaviour_tC5EA4E5C9CF2F8B1360110F3DCB3CA86F7D68FF3
{
public:
	// UnityEngine.UI.Image UIObjective::_icon
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____icon_4;
	// UnityEngine.UI.Image UIObjective::_finish
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____finish_5;
	// TMPro.TextMeshProUGUI UIObjective::_countText
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____countText_6;
	// UnityEngine.ParticleSystem UIObjective::_achievementParticle
	ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * ____achievementParticle_7;
	// ItemsSO UIObjective::_itemsSO
	ItemsSO_tD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A * ____itemsSO_8;
	// TowersSO UIObjective::_towersSO
	TowersSO_t3B7DE821E5F8017950776EEFC42E680B361BCDF0 * ____towersSO_9;
	// Zenject.DiContainer UIObjective::_diContainer
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____diContainer_10;
	// System.String UIObjective::_id
	String_t* ____id_11;
	// System.Int32 UIObjective::_count
	int32_t ____count_12;

public:
	inline static int32_t get_offset_of__icon_4() { return static_cast<int32_t>(offsetof(UIObjective_t94D85D5DE412998C059AE6A45DB99258B95E8023, ____icon_4)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__icon_4() const { return ____icon_4; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__icon_4() { return &____icon_4; }
	inline void set__icon_4(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____icon_4 = value;
		Il2CppCodeGenWriteBarrier((&____icon_4), value);
	}

	inline static int32_t get_offset_of__finish_5() { return static_cast<int32_t>(offsetof(UIObjective_t94D85D5DE412998C059AE6A45DB99258B95E8023, ____finish_5)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__finish_5() const { return ____finish_5; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__finish_5() { return &____finish_5; }
	inline void set__finish_5(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____finish_5 = value;
		Il2CppCodeGenWriteBarrier((&____finish_5), value);
	}

	inline static int32_t get_offset_of__countText_6() { return static_cast<int32_t>(offsetof(UIObjective_t94D85D5DE412998C059AE6A45DB99258B95E8023, ____countText_6)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__countText_6() const { return ____countText_6; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__countText_6() { return &____countText_6; }
	inline void set__countText_6(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____countText_6 = value;
		Il2CppCodeGenWriteBarrier((&____countText_6), value);
	}

	inline static int32_t get_offset_of__achievementParticle_7() { return static_cast<int32_t>(offsetof(UIObjective_t94D85D5DE412998C059AE6A45DB99258B95E8023, ____achievementParticle_7)); }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * get__achievementParticle_7() const { return ____achievementParticle_7; }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D ** get_address_of__achievementParticle_7() { return &____achievementParticle_7; }
	inline void set__achievementParticle_7(ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * value)
	{
		____achievementParticle_7 = value;
		Il2CppCodeGenWriteBarrier((&____achievementParticle_7), value);
	}

	inline static int32_t get_offset_of__itemsSO_8() { return static_cast<int32_t>(offsetof(UIObjective_t94D85D5DE412998C059AE6A45DB99258B95E8023, ____itemsSO_8)); }
	inline ItemsSO_tD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A * get__itemsSO_8() const { return ____itemsSO_8; }
	inline ItemsSO_tD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A ** get_address_of__itemsSO_8() { return &____itemsSO_8; }
	inline void set__itemsSO_8(ItemsSO_tD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A * value)
	{
		____itemsSO_8 = value;
		Il2CppCodeGenWriteBarrier((&____itemsSO_8), value);
	}

	inline static int32_t get_offset_of__towersSO_9() { return static_cast<int32_t>(offsetof(UIObjective_t94D85D5DE412998C059AE6A45DB99258B95E8023, ____towersSO_9)); }
	inline TowersSO_t3B7DE821E5F8017950776EEFC42E680B361BCDF0 * get__towersSO_9() const { return ____towersSO_9; }
	inline TowersSO_t3B7DE821E5F8017950776EEFC42E680B361BCDF0 ** get_address_of__towersSO_9() { return &____towersSO_9; }
	inline void set__towersSO_9(TowersSO_t3B7DE821E5F8017950776EEFC42E680B361BCDF0 * value)
	{
		____towersSO_9 = value;
		Il2CppCodeGenWriteBarrier((&____towersSO_9), value);
	}

	inline static int32_t get_offset_of__diContainer_10() { return static_cast<int32_t>(offsetof(UIObjective_t94D85D5DE412998C059AE6A45DB99258B95E8023, ____diContainer_10)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__diContainer_10() const { return ____diContainer_10; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__diContainer_10() { return &____diContainer_10; }
	inline void set__diContainer_10(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____diContainer_10 = value;
		Il2CppCodeGenWriteBarrier((&____diContainer_10), value);
	}

	inline static int32_t get_offset_of__id_11() { return static_cast<int32_t>(offsetof(UIObjective_t94D85D5DE412998C059AE6A45DB99258B95E8023, ____id_11)); }
	inline String_t* get__id_11() const { return ____id_11; }
	inline String_t** get_address_of__id_11() { return &____id_11; }
	inline void set__id_11(String_t* value)
	{
		____id_11 = value;
		Il2CppCodeGenWriteBarrier((&____id_11), value);
	}

	inline static int32_t get_offset_of__count_12() { return static_cast<int32_t>(offsetof(UIObjective_t94D85D5DE412998C059AE6A45DB99258B95E8023, ____count_12)); }
	inline int32_t get__count_12() const { return ____count_12; }
	inline int32_t* get_address_of__count_12() { return &____count_12; }
	inline void set__count_12(int32_t value)
	{
		____count_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIOBJECTIVE_T94D85D5DE412998C059AE6A45DB99258B95E8023_H
#ifndef BATTLEINVENTORYLISTITEM_T1D6B5F84E1B80F45276049DEC8765BF471392876_H
#define BATTLEINVENTORYLISTITEM_T1D6B5F84E1B80F45276049DEC8765BF471392876_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BattleInventoryListItem
struct  BattleInventoryListItem_t1D6B5F84E1B80F45276049DEC8765BF471392876  : public TListItem_t3835B3A6EDB64421C763D6938505136E6CD7628C
{
public:
	// UnityEngine.UI.Image BattleInventoryListItem::_icon
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____icon_4;
	// Zenject.DiContainer BattleInventoryListItem::_container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____container_5;
	// ItemsSO BattleInventoryListItem::_itemsConf
	ItemsSO_tD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A * ____itemsConf_6;

public:
	inline static int32_t get_offset_of__icon_4() { return static_cast<int32_t>(offsetof(BattleInventoryListItem_t1D6B5F84E1B80F45276049DEC8765BF471392876, ____icon_4)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__icon_4() const { return ____icon_4; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__icon_4() { return &____icon_4; }
	inline void set__icon_4(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____icon_4 = value;
		Il2CppCodeGenWriteBarrier((&____icon_4), value);
	}

	inline static int32_t get_offset_of__container_5() { return static_cast<int32_t>(offsetof(BattleInventoryListItem_t1D6B5F84E1B80F45276049DEC8765BF471392876, ____container_5)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__container_5() const { return ____container_5; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__container_5() { return &____container_5; }
	inline void set__container_5(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____container_5 = value;
		Il2CppCodeGenWriteBarrier((&____container_5), value);
	}

	inline static int32_t get_offset_of__itemsConf_6() { return static_cast<int32_t>(offsetof(BattleInventoryListItem_t1D6B5F84E1B80F45276049DEC8765BF471392876, ____itemsConf_6)); }
	inline ItemsSO_tD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A * get__itemsConf_6() const { return ____itemsConf_6; }
	inline ItemsSO_tD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A ** get_address_of__itemsConf_6() { return &____itemsConf_6; }
	inline void set__itemsConf_6(ItemsSO_tD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A * value)
	{
		____itemsConf_6 = value;
		Il2CppCodeGenWriteBarrier((&____itemsConf_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BATTLEINVENTORYLISTITEM_T1D6B5F84E1B80F45276049DEC8765BF471392876_H
#ifndef BATTLENODE_T1D1FD268C2AB2C374DBAF83358F99FE4C7A6FF3E_H
#define BATTLENODE_T1D1FD268C2AB2C374DBAF83358F99FE4C7A6FF3E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BattleNode
struct  BattleNode_t1D1FD268C2AB2C374DBAF83358F99FE4C7A6FF3E  : public BasicNode_1_t80C898A8840F148EAC8A5FE47B08C3ABA79CE64D
{
public:
	// UnityEngine.UI.Image BattleNode::_heroHPBBar
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____heroHPBBar_7;
	// TMPro.TextMeshProUGUI BattleNode::_heroHPBText
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____heroHPBText_8;
	// UnityEngine.Transform BattleNode::_objectivesParent
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____objectivesParent_9;
	// UnityEngine.UI.Button BattleNode::_pauseBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____pauseBtn_10;
	// Tayr.ILibrary BattleNode::_uiMain
	RuntimeObject* ____uiMain_11;
	// Zenject.DiContainer BattleNode::_container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____container_12;
	// Tayr.TSoundSystem BattleNode::_soundSystem
	TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511 * ____soundSystem_13;
	// SoundSO BattleNode::_soundSO
	SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73 * ____soundSO_14;
	// UserVO BattleNode::_userVO
	UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * ____userVO_15;
	// InventoryVO BattleNode::_inventoryVO
	InventoryVO_tC7F0305FC930782BD2E4A47E3BD0504F0DCC6AD9 * ____inventoryVO_16;
	// System.Collections.Generic.Dictionary`2<System.String,UIObjective> BattleNode::_objectiveDictionary
	Dictionary_2_tA079ADCDB82918F4B62B96F8654CEBEBF463A062 * ____objectiveDictionary_17;
	// System.Int32 BattleNode::_currentHP
	int32_t ____currentHP_18;

public:
	inline static int32_t get_offset_of__heroHPBBar_7() { return static_cast<int32_t>(offsetof(BattleNode_t1D1FD268C2AB2C374DBAF83358F99FE4C7A6FF3E, ____heroHPBBar_7)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__heroHPBBar_7() const { return ____heroHPBBar_7; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__heroHPBBar_7() { return &____heroHPBBar_7; }
	inline void set__heroHPBBar_7(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____heroHPBBar_7 = value;
		Il2CppCodeGenWriteBarrier((&____heroHPBBar_7), value);
	}

	inline static int32_t get_offset_of__heroHPBText_8() { return static_cast<int32_t>(offsetof(BattleNode_t1D1FD268C2AB2C374DBAF83358F99FE4C7A6FF3E, ____heroHPBText_8)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__heroHPBText_8() const { return ____heroHPBText_8; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__heroHPBText_8() { return &____heroHPBText_8; }
	inline void set__heroHPBText_8(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____heroHPBText_8 = value;
		Il2CppCodeGenWriteBarrier((&____heroHPBText_8), value);
	}

	inline static int32_t get_offset_of__objectivesParent_9() { return static_cast<int32_t>(offsetof(BattleNode_t1D1FD268C2AB2C374DBAF83358F99FE4C7A6FF3E, ____objectivesParent_9)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__objectivesParent_9() const { return ____objectivesParent_9; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__objectivesParent_9() { return &____objectivesParent_9; }
	inline void set__objectivesParent_9(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____objectivesParent_9 = value;
		Il2CppCodeGenWriteBarrier((&____objectivesParent_9), value);
	}

	inline static int32_t get_offset_of__pauseBtn_10() { return static_cast<int32_t>(offsetof(BattleNode_t1D1FD268C2AB2C374DBAF83358F99FE4C7A6FF3E, ____pauseBtn_10)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__pauseBtn_10() const { return ____pauseBtn_10; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__pauseBtn_10() { return &____pauseBtn_10; }
	inline void set__pauseBtn_10(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____pauseBtn_10 = value;
		Il2CppCodeGenWriteBarrier((&____pauseBtn_10), value);
	}

	inline static int32_t get_offset_of__uiMain_11() { return static_cast<int32_t>(offsetof(BattleNode_t1D1FD268C2AB2C374DBAF83358F99FE4C7A6FF3E, ____uiMain_11)); }
	inline RuntimeObject* get__uiMain_11() const { return ____uiMain_11; }
	inline RuntimeObject** get_address_of__uiMain_11() { return &____uiMain_11; }
	inline void set__uiMain_11(RuntimeObject* value)
	{
		____uiMain_11 = value;
		Il2CppCodeGenWriteBarrier((&____uiMain_11), value);
	}

	inline static int32_t get_offset_of__container_12() { return static_cast<int32_t>(offsetof(BattleNode_t1D1FD268C2AB2C374DBAF83358F99FE4C7A6FF3E, ____container_12)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__container_12() const { return ____container_12; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__container_12() { return &____container_12; }
	inline void set__container_12(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____container_12 = value;
		Il2CppCodeGenWriteBarrier((&____container_12), value);
	}

	inline static int32_t get_offset_of__soundSystem_13() { return static_cast<int32_t>(offsetof(BattleNode_t1D1FD268C2AB2C374DBAF83358F99FE4C7A6FF3E, ____soundSystem_13)); }
	inline TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511 * get__soundSystem_13() const { return ____soundSystem_13; }
	inline TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511 ** get_address_of__soundSystem_13() { return &____soundSystem_13; }
	inline void set__soundSystem_13(TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511 * value)
	{
		____soundSystem_13 = value;
		Il2CppCodeGenWriteBarrier((&____soundSystem_13), value);
	}

	inline static int32_t get_offset_of__soundSO_14() { return static_cast<int32_t>(offsetof(BattleNode_t1D1FD268C2AB2C374DBAF83358F99FE4C7A6FF3E, ____soundSO_14)); }
	inline SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73 * get__soundSO_14() const { return ____soundSO_14; }
	inline SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73 ** get_address_of__soundSO_14() { return &____soundSO_14; }
	inline void set__soundSO_14(SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73 * value)
	{
		____soundSO_14 = value;
		Il2CppCodeGenWriteBarrier((&____soundSO_14), value);
	}

	inline static int32_t get_offset_of__userVO_15() { return static_cast<int32_t>(offsetof(BattleNode_t1D1FD268C2AB2C374DBAF83358F99FE4C7A6FF3E, ____userVO_15)); }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * get__userVO_15() const { return ____userVO_15; }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 ** get_address_of__userVO_15() { return &____userVO_15; }
	inline void set__userVO_15(UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * value)
	{
		____userVO_15 = value;
		Il2CppCodeGenWriteBarrier((&____userVO_15), value);
	}

	inline static int32_t get_offset_of__inventoryVO_16() { return static_cast<int32_t>(offsetof(BattleNode_t1D1FD268C2AB2C374DBAF83358F99FE4C7A6FF3E, ____inventoryVO_16)); }
	inline InventoryVO_tC7F0305FC930782BD2E4A47E3BD0504F0DCC6AD9 * get__inventoryVO_16() const { return ____inventoryVO_16; }
	inline InventoryVO_tC7F0305FC930782BD2E4A47E3BD0504F0DCC6AD9 ** get_address_of__inventoryVO_16() { return &____inventoryVO_16; }
	inline void set__inventoryVO_16(InventoryVO_tC7F0305FC930782BD2E4A47E3BD0504F0DCC6AD9 * value)
	{
		____inventoryVO_16 = value;
		Il2CppCodeGenWriteBarrier((&____inventoryVO_16), value);
	}

	inline static int32_t get_offset_of__objectiveDictionary_17() { return static_cast<int32_t>(offsetof(BattleNode_t1D1FD268C2AB2C374DBAF83358F99FE4C7A6FF3E, ____objectiveDictionary_17)); }
	inline Dictionary_2_tA079ADCDB82918F4B62B96F8654CEBEBF463A062 * get__objectiveDictionary_17() const { return ____objectiveDictionary_17; }
	inline Dictionary_2_tA079ADCDB82918F4B62B96F8654CEBEBF463A062 ** get_address_of__objectiveDictionary_17() { return &____objectiveDictionary_17; }
	inline void set__objectiveDictionary_17(Dictionary_2_tA079ADCDB82918F4B62B96F8654CEBEBF463A062 * value)
	{
		____objectiveDictionary_17 = value;
		Il2CppCodeGenWriteBarrier((&____objectiveDictionary_17), value);
	}

	inline static int32_t get_offset_of__currentHP_18() { return static_cast<int32_t>(offsetof(BattleNode_t1D1FD268C2AB2C374DBAF83358F99FE4C7A6FF3E, ____currentHP_18)); }
	inline int32_t get__currentHP_18() const { return ____currentHP_18; }
	inline int32_t* get_address_of__currentHP_18() { return &____currentHP_18; }
	inline void set__currentHP_18(int32_t value)
	{
		____currentHP_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BATTLENODE_T1D1FD268C2AB2C374DBAF83358F99FE4C7A6FF3E_H
#ifndef COLLECTGOALTUTORIALNODE_T1743E320CB9D993CF09AC77A20CED76149C74637_H
#define COLLECTGOALTUTORIALNODE_T1743E320CB9D993CF09AC77A20CED76149C74637_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CollectGoalTutorialNode
struct  CollectGoalTutorialNode_t1743E320CB9D993CF09AC77A20CED76149C74637  : public BasicNode_1_t56EF59E5C4BF3DD3B0683550A56A5B17B8A2520D
{
public:
	// UnityEngine.Transform CollectGoalTutorialNode::_collectDescription
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____collectDescription_7;
	// UnityEngine.Transform CollectGoalTutorialNode::_deliverDescription
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____deliverDescription_8;
	// UnityEngine.Transform CollectGoalTutorialNode::_arrow
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____arrow_9;

public:
	inline static int32_t get_offset_of__collectDescription_7() { return static_cast<int32_t>(offsetof(CollectGoalTutorialNode_t1743E320CB9D993CF09AC77A20CED76149C74637, ____collectDescription_7)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__collectDescription_7() const { return ____collectDescription_7; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__collectDescription_7() { return &____collectDescription_7; }
	inline void set__collectDescription_7(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____collectDescription_7 = value;
		Il2CppCodeGenWriteBarrier((&____collectDescription_7), value);
	}

	inline static int32_t get_offset_of__deliverDescription_8() { return static_cast<int32_t>(offsetof(CollectGoalTutorialNode_t1743E320CB9D993CF09AC77A20CED76149C74637, ____deliverDescription_8)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__deliverDescription_8() const { return ____deliverDescription_8; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__deliverDescription_8() { return &____deliverDescription_8; }
	inline void set__deliverDescription_8(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____deliverDescription_8 = value;
		Il2CppCodeGenWriteBarrier((&____deliverDescription_8), value);
	}

	inline static int32_t get_offset_of__arrow_9() { return static_cast<int32_t>(offsetof(CollectGoalTutorialNode_t1743E320CB9D993CF09AC77A20CED76149C74637, ____arrow_9)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__arrow_9() const { return ____arrow_9; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__arrow_9() { return &____arrow_9; }
	inline void set__arrow_9(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____arrow_9 = value;
		Il2CppCodeGenWriteBarrier((&____arrow_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTGOALTUTORIALNODE_T1743E320CB9D993CF09AC77A20CED76149C74637_H
#ifndef DESTROYFIVEINVENTORYTUTORIALNODE_TE58CD63865141981AFF5F1A2E9A0A836D1576FF7_H
#define DESTROYFIVEINVENTORYTUTORIALNODE_TE58CD63865141981AFF5F1A2E9A0A836D1576FF7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DestroyFiveInventoryTutorialNode
struct  DestroyFiveInventoryTutorialNode_tE58CD63865141981AFF5F1A2E9A0A836D1576FF7  : public BasicNode_1_t56EF59E5C4BF3DD3B0683550A56A5B17B8A2520D
{
public:
	// UnityEngine.Transform DestroyFiveInventoryTutorialNode::_description
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____description_7;

public:
	inline static int32_t get_offset_of__description_7() { return static_cast<int32_t>(offsetof(DestroyFiveInventoryTutorialNode_tE58CD63865141981AFF5F1A2E9A0A836D1576FF7, ____description_7)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__description_7() const { return ____description_7; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__description_7() { return &____description_7; }
	inline void set__description_7(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____description_7 = value;
		Il2CppCodeGenWriteBarrier((&____description_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESTROYFIVEINVENTORYTUTORIALNODE_TE58CD63865141981AFF5F1A2E9A0A836D1576FF7_H
#ifndef DESTROYGOALTUTORIALNODE_TA62D39E749050BBCB967A3B7FA6CE41997DDC58D_H
#define DESTROYGOALTUTORIALNODE_TA62D39E749050BBCB967A3B7FA6CE41997DDC58D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DestroyGoalTutorialNode
struct  DestroyGoalTutorialNode_tA62D39E749050BBCB967A3B7FA6CE41997DDC58D  : public BasicNode_1_t56EF59E5C4BF3DD3B0683550A56A5B17B8A2520D
{
public:
	// UnityEngine.Transform DestroyGoalTutorialNode::_description
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____description_7;

public:
	inline static int32_t get_offset_of__description_7() { return static_cast<int32_t>(offsetof(DestroyGoalTutorialNode_tA62D39E749050BBCB967A3B7FA6CE41997DDC58D, ____description_7)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__description_7() const { return ____description_7; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__description_7() { return &____description_7; }
	inline void set__description_7(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____description_7 = value;
		Il2CppCodeGenWriteBarrier((&____description_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESTROYGOALTUTORIALNODE_TA62D39E749050BBCB967A3B7FA6CE41997DDC58D_H
#ifndef DESTROYTOWERGOALNODE_T75EDEEB6BF4F7A62DBA3DFCB647F82385DBFE311_H
#define DESTROYTOWERGOALNODE_T75EDEEB6BF4F7A62DBA3DFCB647F82385DBFE311_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DestroyTowerGoalNode
struct  DestroyTowerGoalNode_t75EDEEB6BF4F7A62DBA3DFCB647F82385DBFE311  : public BasicNode_1_t56EF59E5C4BF3DD3B0683550A56A5B17B8A2520D
{
public:
	// UnityEngine.Transform DestroyTowerGoalNode::_description
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____description_7;

public:
	inline static int32_t get_offset_of__description_7() { return static_cast<int32_t>(offsetof(DestroyTowerGoalNode_t75EDEEB6BF4F7A62DBA3DFCB647F82385DBFE311, ____description_7)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__description_7() const { return ____description_7; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__description_7() { return &____description_7; }
	inline void set__description_7(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____description_7 = value;
		Il2CppCodeGenWriteBarrier((&____description_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESTROYTOWERGOALNODE_T75EDEEB6BF4F7A62DBA3DFCB647F82385DBFE311_H
#ifndef LOSESUMMARYNODE_T36A15CC7032A9119437AD7D99D2C93BCCAB6BAAF_H
#define LOSESUMMARYNODE_T36A15CC7032A9119437AD7D99D2C93BCCAB6BAAF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoseSummaryNode
struct  LoseSummaryNode_t36A15CC7032A9119437AD7D99D2C93BCCAB6BAAF  : public BasicNode_1_t69966F6A6017B9F9944A0DFB087909A1F61C2D76
{
public:
	// Tayr.TLocalizerTextMeshProUGUI LoseSummaryNode::_level
	TLocalizerTextMeshProUGUI_tC187D21D45AC70D0B204800BB874B0BEDAAFD557 * ____level_7;
	// UnityEngine.UI.Button LoseSummaryNode::_tryAgainBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____tryAgainBtn_8;
	// UnityEngine.UI.Button LoseSummaryNode::_closeBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____closeBtn_9;
	// Tayr.ILibrary LoseSummaryNode::_uiMain
	RuntimeObject* ____uiMain_10;
	// Zenject.DiContainer LoseSummaryNode::_container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____container_11;
	// Tayr.TSoundSystem LoseSummaryNode::_soundSystem
	TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511 * ____soundSystem_12;
	// SoundSO LoseSummaryNode::_soundSO
	SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73 * ____soundSO_13;

public:
	inline static int32_t get_offset_of__level_7() { return static_cast<int32_t>(offsetof(LoseSummaryNode_t36A15CC7032A9119437AD7D99D2C93BCCAB6BAAF, ____level_7)); }
	inline TLocalizerTextMeshProUGUI_tC187D21D45AC70D0B204800BB874B0BEDAAFD557 * get__level_7() const { return ____level_7; }
	inline TLocalizerTextMeshProUGUI_tC187D21D45AC70D0B204800BB874B0BEDAAFD557 ** get_address_of__level_7() { return &____level_7; }
	inline void set__level_7(TLocalizerTextMeshProUGUI_tC187D21D45AC70D0B204800BB874B0BEDAAFD557 * value)
	{
		____level_7 = value;
		Il2CppCodeGenWriteBarrier((&____level_7), value);
	}

	inline static int32_t get_offset_of__tryAgainBtn_8() { return static_cast<int32_t>(offsetof(LoseSummaryNode_t36A15CC7032A9119437AD7D99D2C93BCCAB6BAAF, ____tryAgainBtn_8)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__tryAgainBtn_8() const { return ____tryAgainBtn_8; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__tryAgainBtn_8() { return &____tryAgainBtn_8; }
	inline void set__tryAgainBtn_8(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____tryAgainBtn_8 = value;
		Il2CppCodeGenWriteBarrier((&____tryAgainBtn_8), value);
	}

	inline static int32_t get_offset_of__closeBtn_9() { return static_cast<int32_t>(offsetof(LoseSummaryNode_t36A15CC7032A9119437AD7D99D2C93BCCAB6BAAF, ____closeBtn_9)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__closeBtn_9() const { return ____closeBtn_9; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__closeBtn_9() { return &____closeBtn_9; }
	inline void set__closeBtn_9(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____closeBtn_9 = value;
		Il2CppCodeGenWriteBarrier((&____closeBtn_9), value);
	}

	inline static int32_t get_offset_of__uiMain_10() { return static_cast<int32_t>(offsetof(LoseSummaryNode_t36A15CC7032A9119437AD7D99D2C93BCCAB6BAAF, ____uiMain_10)); }
	inline RuntimeObject* get__uiMain_10() const { return ____uiMain_10; }
	inline RuntimeObject** get_address_of__uiMain_10() { return &____uiMain_10; }
	inline void set__uiMain_10(RuntimeObject* value)
	{
		____uiMain_10 = value;
		Il2CppCodeGenWriteBarrier((&____uiMain_10), value);
	}

	inline static int32_t get_offset_of__container_11() { return static_cast<int32_t>(offsetof(LoseSummaryNode_t36A15CC7032A9119437AD7D99D2C93BCCAB6BAAF, ____container_11)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__container_11() const { return ____container_11; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__container_11() { return &____container_11; }
	inline void set__container_11(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____container_11 = value;
		Il2CppCodeGenWriteBarrier((&____container_11), value);
	}

	inline static int32_t get_offset_of__soundSystem_12() { return static_cast<int32_t>(offsetof(LoseSummaryNode_t36A15CC7032A9119437AD7D99D2C93BCCAB6BAAF, ____soundSystem_12)); }
	inline TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511 * get__soundSystem_12() const { return ____soundSystem_12; }
	inline TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511 ** get_address_of__soundSystem_12() { return &____soundSystem_12; }
	inline void set__soundSystem_12(TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511 * value)
	{
		____soundSystem_12 = value;
		Il2CppCodeGenWriteBarrier((&____soundSystem_12), value);
	}

	inline static int32_t get_offset_of__soundSO_13() { return static_cast<int32_t>(offsetof(LoseSummaryNode_t36A15CC7032A9119437AD7D99D2C93BCCAB6BAAF, ____soundSO_13)); }
	inline SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73 * get__soundSO_13() const { return ____soundSO_13; }
	inline SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73 ** get_address_of__soundSO_13() { return &____soundSO_13; }
	inline void set__soundSO_13(SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73 * value)
	{
		____soundSO_13 = value;
		Il2CppCodeGenWriteBarrier((&____soundSO_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOSESUMMARYNODE_T36A15CC7032A9119437AD7D99D2C93BCCAB6BAAF_H
#ifndef MOVEMENTTUTORIALNODE_T612D67CD2F5CEA136C089C042E8C326FECB3018A_H
#define MOVEMENTTUTORIALNODE_T612D67CD2F5CEA136C089C042E8C326FECB3018A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MovementTutorialNode
struct  MovementTutorialNode_t612D67CD2F5CEA136C089C042E8C326FECB3018A  : public BasicNode_1_t56EF59E5C4BF3DD3B0683550A56A5B17B8A2520D
{
public:
	// TMPro.TextMeshProUGUI MovementTutorialNode::_description
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____description_7;

public:
	inline static int32_t get_offset_of__description_7() { return static_cast<int32_t>(offsetof(MovementTutorialNode_t612D67CD2F5CEA136C089C042E8C326FECB3018A, ____description_7)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__description_7() const { return ____description_7; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__description_7() { return &____description_7; }
	inline void set__description_7(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____description_7 = value;
		Il2CppCodeGenWriteBarrier((&____description_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVEMENTTUTORIALNODE_T612D67CD2F5CEA136C089C042E8C326FECB3018A_H
#ifndef PAUSENODE_TD76F910C8791B50A883EC9C3537274CD42B07531_H
#define PAUSENODE_TD76F910C8791B50A883EC9C3537274CD42B07531_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PauseNode
struct  PauseNode_tD76F910C8791B50A883EC9C3537274CD42B07531  : public BasicNode_1_t80C898A8840F148EAC8A5FE47B08C3ABA79CE64D
{
public:
	// UnityEngine.UI.Button PauseNode::_closeBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____closeBtn_7;
	// UnityEngine.UI.Button PauseNode::_fxSoundBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____fxSoundBtn_8;
	// UnityEngine.UI.Button PauseNode::_musicSoundBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____musicSoundBtn_9;
	// UnityEngine.UI.Image PauseNode::_fxSoundOn
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____fxSoundOn_10;
	// UnityEngine.UI.Image PauseNode::_fxSoundOff
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____fxSoundOff_11;
	// UnityEngine.UI.Image PauseNode::_musicSoundOn
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____musicSoundOn_12;
	// UnityEngine.UI.Image PauseNode::_musicSoundOff
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____musicSoundOff_13;
	// Zenject.DiContainer PauseNode::_container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____container_14;
	// UserVO PauseNode::_userVO
	UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * ____userVO_15;
	// Tayr.TSoundSystem PauseNode::_soundSystem
	TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511 * ____soundSystem_16;
	// SoundSO PauseNode::_soundSO
	SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73 * ____soundSO_17;
	// BattleEcsInitializer PauseNode::_battleEcsInitializer
	BattleEcsInitializer_tA76C29D8A9881FDF3955010F51F9DA4FA7134C67 * ____battleEcsInitializer_18;
	// System.Boolean PauseNode::_fx
	bool ____fx_19;
	// System.Boolean PauseNode::_music
	bool ____music_20;

public:
	inline static int32_t get_offset_of__closeBtn_7() { return static_cast<int32_t>(offsetof(PauseNode_tD76F910C8791B50A883EC9C3537274CD42B07531, ____closeBtn_7)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__closeBtn_7() const { return ____closeBtn_7; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__closeBtn_7() { return &____closeBtn_7; }
	inline void set__closeBtn_7(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____closeBtn_7 = value;
		Il2CppCodeGenWriteBarrier((&____closeBtn_7), value);
	}

	inline static int32_t get_offset_of__fxSoundBtn_8() { return static_cast<int32_t>(offsetof(PauseNode_tD76F910C8791B50A883EC9C3537274CD42B07531, ____fxSoundBtn_8)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__fxSoundBtn_8() const { return ____fxSoundBtn_8; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__fxSoundBtn_8() { return &____fxSoundBtn_8; }
	inline void set__fxSoundBtn_8(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____fxSoundBtn_8 = value;
		Il2CppCodeGenWriteBarrier((&____fxSoundBtn_8), value);
	}

	inline static int32_t get_offset_of__musicSoundBtn_9() { return static_cast<int32_t>(offsetof(PauseNode_tD76F910C8791B50A883EC9C3537274CD42B07531, ____musicSoundBtn_9)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__musicSoundBtn_9() const { return ____musicSoundBtn_9; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__musicSoundBtn_9() { return &____musicSoundBtn_9; }
	inline void set__musicSoundBtn_9(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____musicSoundBtn_9 = value;
		Il2CppCodeGenWriteBarrier((&____musicSoundBtn_9), value);
	}

	inline static int32_t get_offset_of__fxSoundOn_10() { return static_cast<int32_t>(offsetof(PauseNode_tD76F910C8791B50A883EC9C3537274CD42B07531, ____fxSoundOn_10)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__fxSoundOn_10() const { return ____fxSoundOn_10; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__fxSoundOn_10() { return &____fxSoundOn_10; }
	inline void set__fxSoundOn_10(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____fxSoundOn_10 = value;
		Il2CppCodeGenWriteBarrier((&____fxSoundOn_10), value);
	}

	inline static int32_t get_offset_of__fxSoundOff_11() { return static_cast<int32_t>(offsetof(PauseNode_tD76F910C8791B50A883EC9C3537274CD42B07531, ____fxSoundOff_11)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__fxSoundOff_11() const { return ____fxSoundOff_11; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__fxSoundOff_11() { return &____fxSoundOff_11; }
	inline void set__fxSoundOff_11(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____fxSoundOff_11 = value;
		Il2CppCodeGenWriteBarrier((&____fxSoundOff_11), value);
	}

	inline static int32_t get_offset_of__musicSoundOn_12() { return static_cast<int32_t>(offsetof(PauseNode_tD76F910C8791B50A883EC9C3537274CD42B07531, ____musicSoundOn_12)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__musicSoundOn_12() const { return ____musicSoundOn_12; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__musicSoundOn_12() { return &____musicSoundOn_12; }
	inline void set__musicSoundOn_12(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____musicSoundOn_12 = value;
		Il2CppCodeGenWriteBarrier((&____musicSoundOn_12), value);
	}

	inline static int32_t get_offset_of__musicSoundOff_13() { return static_cast<int32_t>(offsetof(PauseNode_tD76F910C8791B50A883EC9C3537274CD42B07531, ____musicSoundOff_13)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__musicSoundOff_13() const { return ____musicSoundOff_13; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__musicSoundOff_13() { return &____musicSoundOff_13; }
	inline void set__musicSoundOff_13(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____musicSoundOff_13 = value;
		Il2CppCodeGenWriteBarrier((&____musicSoundOff_13), value);
	}

	inline static int32_t get_offset_of__container_14() { return static_cast<int32_t>(offsetof(PauseNode_tD76F910C8791B50A883EC9C3537274CD42B07531, ____container_14)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__container_14() const { return ____container_14; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__container_14() { return &____container_14; }
	inline void set__container_14(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____container_14 = value;
		Il2CppCodeGenWriteBarrier((&____container_14), value);
	}

	inline static int32_t get_offset_of__userVO_15() { return static_cast<int32_t>(offsetof(PauseNode_tD76F910C8791B50A883EC9C3537274CD42B07531, ____userVO_15)); }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * get__userVO_15() const { return ____userVO_15; }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 ** get_address_of__userVO_15() { return &____userVO_15; }
	inline void set__userVO_15(UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * value)
	{
		____userVO_15 = value;
		Il2CppCodeGenWriteBarrier((&____userVO_15), value);
	}

	inline static int32_t get_offset_of__soundSystem_16() { return static_cast<int32_t>(offsetof(PauseNode_tD76F910C8791B50A883EC9C3537274CD42B07531, ____soundSystem_16)); }
	inline TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511 * get__soundSystem_16() const { return ____soundSystem_16; }
	inline TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511 ** get_address_of__soundSystem_16() { return &____soundSystem_16; }
	inline void set__soundSystem_16(TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511 * value)
	{
		____soundSystem_16 = value;
		Il2CppCodeGenWriteBarrier((&____soundSystem_16), value);
	}

	inline static int32_t get_offset_of__soundSO_17() { return static_cast<int32_t>(offsetof(PauseNode_tD76F910C8791B50A883EC9C3537274CD42B07531, ____soundSO_17)); }
	inline SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73 * get__soundSO_17() const { return ____soundSO_17; }
	inline SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73 ** get_address_of__soundSO_17() { return &____soundSO_17; }
	inline void set__soundSO_17(SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73 * value)
	{
		____soundSO_17 = value;
		Il2CppCodeGenWriteBarrier((&____soundSO_17), value);
	}

	inline static int32_t get_offset_of__battleEcsInitializer_18() { return static_cast<int32_t>(offsetof(PauseNode_tD76F910C8791B50A883EC9C3537274CD42B07531, ____battleEcsInitializer_18)); }
	inline BattleEcsInitializer_tA76C29D8A9881FDF3955010F51F9DA4FA7134C67 * get__battleEcsInitializer_18() const { return ____battleEcsInitializer_18; }
	inline BattleEcsInitializer_tA76C29D8A9881FDF3955010F51F9DA4FA7134C67 ** get_address_of__battleEcsInitializer_18() { return &____battleEcsInitializer_18; }
	inline void set__battleEcsInitializer_18(BattleEcsInitializer_tA76C29D8A9881FDF3955010F51F9DA4FA7134C67 * value)
	{
		____battleEcsInitializer_18 = value;
		Il2CppCodeGenWriteBarrier((&____battleEcsInitializer_18), value);
	}

	inline static int32_t get_offset_of__fx_19() { return static_cast<int32_t>(offsetof(PauseNode_tD76F910C8791B50A883EC9C3537274CD42B07531, ____fx_19)); }
	inline bool get__fx_19() const { return ____fx_19; }
	inline bool* get_address_of__fx_19() { return &____fx_19; }
	inline void set__fx_19(bool value)
	{
		____fx_19 = value;
	}

	inline static int32_t get_offset_of__music_20() { return static_cast<int32_t>(offsetof(PauseNode_tD76F910C8791B50A883EC9C3537274CD42B07531, ____music_20)); }
	inline bool get__music_20() const { return ____music_20; }
	inline bool* get_address_of__music_20() { return &____music_20; }
	inline void set__music_20(bool value)
	{
		____music_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAUSENODE_TD76F910C8791B50A883EC9C3537274CD42B07531_H
#ifndef REVIVENODE_T71A1E27EE0DE3CA67D44FFF4DA4D7AE8C47D2FFF_H
#define REVIVENODE_T71A1E27EE0DE3CA67D44FFF4DA4D7AE8C47D2FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReviveNode
struct  ReviveNode_t71A1E27EE0DE3CA67D44FFF4DA4D7AE8C47D2FFF  : public BasicNode_1_tC15963AAB43917EBFB80C97B165DDBD6C98E39BB
{
public:
	// TMPro.TextMeshProUGUI ReviveNode::_reviveCost
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____reviveCost_7;
	// UnityEngine.UI.Button ReviveNode::_revive
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____revive_8;
	// UnityEngine.UI.Button ReviveNode::_closeBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____closeBtn_9;
	// Zenject.DiContainer ReviveNode::_container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____container_10;
	// Tayr.ILibrary ReviveNode::_uiMain
	RuntimeObject* ____uiMain_11;
	// UserVO ReviveNode::_userVO
	UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * ____userVO_12;
	// SpellsSO ReviveNode::_spellsSO
	SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * ____spellsSO_13;
	// BattleSettingsSO ReviveNode::_battleSettingsSO
	BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE * ____battleSettingsSO_14;
	// InventorySystem ReviveNode::_inventorySystem
	InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 * ____inventorySystem_15;
	// BattleEcsInitializer ReviveNode::_battleEcsInitializer
	BattleEcsInitializer_tA76C29D8A9881FDF3955010F51F9DA4FA7134C67 * ____battleEcsInitializer_16;
	// KickerManager ReviveNode::_kickerManager
	KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA * ____kickerManager_17;
	// Tayr.TSoundSystem ReviveNode::_soundSystem
	TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511 * ____soundSystem_18;
	// SoundSO ReviveNode::_soundSO
	SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73 * ____soundSO_19;

public:
	inline static int32_t get_offset_of__reviveCost_7() { return static_cast<int32_t>(offsetof(ReviveNode_t71A1E27EE0DE3CA67D44FFF4DA4D7AE8C47D2FFF, ____reviveCost_7)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__reviveCost_7() const { return ____reviveCost_7; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__reviveCost_7() { return &____reviveCost_7; }
	inline void set__reviveCost_7(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____reviveCost_7 = value;
		Il2CppCodeGenWriteBarrier((&____reviveCost_7), value);
	}

	inline static int32_t get_offset_of__revive_8() { return static_cast<int32_t>(offsetof(ReviveNode_t71A1E27EE0DE3CA67D44FFF4DA4D7AE8C47D2FFF, ____revive_8)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__revive_8() const { return ____revive_8; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__revive_8() { return &____revive_8; }
	inline void set__revive_8(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____revive_8 = value;
		Il2CppCodeGenWriteBarrier((&____revive_8), value);
	}

	inline static int32_t get_offset_of__closeBtn_9() { return static_cast<int32_t>(offsetof(ReviveNode_t71A1E27EE0DE3CA67D44FFF4DA4D7AE8C47D2FFF, ____closeBtn_9)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__closeBtn_9() const { return ____closeBtn_9; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__closeBtn_9() { return &____closeBtn_9; }
	inline void set__closeBtn_9(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____closeBtn_9 = value;
		Il2CppCodeGenWriteBarrier((&____closeBtn_9), value);
	}

	inline static int32_t get_offset_of__container_10() { return static_cast<int32_t>(offsetof(ReviveNode_t71A1E27EE0DE3CA67D44FFF4DA4D7AE8C47D2FFF, ____container_10)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__container_10() const { return ____container_10; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__container_10() { return &____container_10; }
	inline void set__container_10(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____container_10 = value;
		Il2CppCodeGenWriteBarrier((&____container_10), value);
	}

	inline static int32_t get_offset_of__uiMain_11() { return static_cast<int32_t>(offsetof(ReviveNode_t71A1E27EE0DE3CA67D44FFF4DA4D7AE8C47D2FFF, ____uiMain_11)); }
	inline RuntimeObject* get__uiMain_11() const { return ____uiMain_11; }
	inline RuntimeObject** get_address_of__uiMain_11() { return &____uiMain_11; }
	inline void set__uiMain_11(RuntimeObject* value)
	{
		____uiMain_11 = value;
		Il2CppCodeGenWriteBarrier((&____uiMain_11), value);
	}

	inline static int32_t get_offset_of__userVO_12() { return static_cast<int32_t>(offsetof(ReviveNode_t71A1E27EE0DE3CA67D44FFF4DA4D7AE8C47D2FFF, ____userVO_12)); }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * get__userVO_12() const { return ____userVO_12; }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 ** get_address_of__userVO_12() { return &____userVO_12; }
	inline void set__userVO_12(UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * value)
	{
		____userVO_12 = value;
		Il2CppCodeGenWriteBarrier((&____userVO_12), value);
	}

	inline static int32_t get_offset_of__spellsSO_13() { return static_cast<int32_t>(offsetof(ReviveNode_t71A1E27EE0DE3CA67D44FFF4DA4D7AE8C47D2FFF, ____spellsSO_13)); }
	inline SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * get__spellsSO_13() const { return ____spellsSO_13; }
	inline SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 ** get_address_of__spellsSO_13() { return &____spellsSO_13; }
	inline void set__spellsSO_13(SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * value)
	{
		____spellsSO_13 = value;
		Il2CppCodeGenWriteBarrier((&____spellsSO_13), value);
	}

	inline static int32_t get_offset_of__battleSettingsSO_14() { return static_cast<int32_t>(offsetof(ReviveNode_t71A1E27EE0DE3CA67D44FFF4DA4D7AE8C47D2FFF, ____battleSettingsSO_14)); }
	inline BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE * get__battleSettingsSO_14() const { return ____battleSettingsSO_14; }
	inline BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE ** get_address_of__battleSettingsSO_14() { return &____battleSettingsSO_14; }
	inline void set__battleSettingsSO_14(BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE * value)
	{
		____battleSettingsSO_14 = value;
		Il2CppCodeGenWriteBarrier((&____battleSettingsSO_14), value);
	}

	inline static int32_t get_offset_of__inventorySystem_15() { return static_cast<int32_t>(offsetof(ReviveNode_t71A1E27EE0DE3CA67D44FFF4DA4D7AE8C47D2FFF, ____inventorySystem_15)); }
	inline InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 * get__inventorySystem_15() const { return ____inventorySystem_15; }
	inline InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 ** get_address_of__inventorySystem_15() { return &____inventorySystem_15; }
	inline void set__inventorySystem_15(InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 * value)
	{
		____inventorySystem_15 = value;
		Il2CppCodeGenWriteBarrier((&____inventorySystem_15), value);
	}

	inline static int32_t get_offset_of__battleEcsInitializer_16() { return static_cast<int32_t>(offsetof(ReviveNode_t71A1E27EE0DE3CA67D44FFF4DA4D7AE8C47D2FFF, ____battleEcsInitializer_16)); }
	inline BattleEcsInitializer_tA76C29D8A9881FDF3955010F51F9DA4FA7134C67 * get__battleEcsInitializer_16() const { return ____battleEcsInitializer_16; }
	inline BattleEcsInitializer_tA76C29D8A9881FDF3955010F51F9DA4FA7134C67 ** get_address_of__battleEcsInitializer_16() { return &____battleEcsInitializer_16; }
	inline void set__battleEcsInitializer_16(BattleEcsInitializer_tA76C29D8A9881FDF3955010F51F9DA4FA7134C67 * value)
	{
		____battleEcsInitializer_16 = value;
		Il2CppCodeGenWriteBarrier((&____battleEcsInitializer_16), value);
	}

	inline static int32_t get_offset_of__kickerManager_17() { return static_cast<int32_t>(offsetof(ReviveNode_t71A1E27EE0DE3CA67D44FFF4DA4D7AE8C47D2FFF, ____kickerManager_17)); }
	inline KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA * get__kickerManager_17() const { return ____kickerManager_17; }
	inline KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA ** get_address_of__kickerManager_17() { return &____kickerManager_17; }
	inline void set__kickerManager_17(KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA * value)
	{
		____kickerManager_17 = value;
		Il2CppCodeGenWriteBarrier((&____kickerManager_17), value);
	}

	inline static int32_t get_offset_of__soundSystem_18() { return static_cast<int32_t>(offsetof(ReviveNode_t71A1E27EE0DE3CA67D44FFF4DA4D7AE8C47D2FFF, ____soundSystem_18)); }
	inline TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511 * get__soundSystem_18() const { return ____soundSystem_18; }
	inline TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511 ** get_address_of__soundSystem_18() { return &____soundSystem_18; }
	inline void set__soundSystem_18(TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511 * value)
	{
		____soundSystem_18 = value;
		Il2CppCodeGenWriteBarrier((&____soundSystem_18), value);
	}

	inline static int32_t get_offset_of__soundSO_19() { return static_cast<int32_t>(offsetof(ReviveNode_t71A1E27EE0DE3CA67D44FFF4DA4D7AE8C47D2FFF, ____soundSO_19)); }
	inline SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73 * get__soundSO_19() const { return ____soundSO_19; }
	inline SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73 ** get_address_of__soundSO_19() { return &____soundSO_19; }
	inline void set__soundSO_19(SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73 * value)
	{
		____soundSO_19 = value;
		Il2CppCodeGenWriteBarrier((&____soundSO_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REVIVENODE_T71A1E27EE0DE3CA67D44FFF4DA4D7AE8C47D2FFF_H
#ifndef REVIVETIMEPRESSURENODE_T184083765D50D32D7C3EDC4180F7E3298BF82CFB_H
#define REVIVETIMEPRESSURENODE_T184083765D50D32D7C3EDC4180F7E3298BF82CFB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReviveTimePressureNode
struct  ReviveTimePressureNode_t184083765D50D32D7C3EDC4180F7E3298BF82CFB  : public BasicNode_1_tC15963AAB43917EBFB80C97B165DDBD6C98E39BB
{
public:
	// TMPro.TextMeshProUGUI ReviveTimePressureNode::_reviveCost
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____reviveCost_7;
	// UnityEngine.UI.Button ReviveTimePressureNode::_revive
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____revive_8;
	// UnityEngine.UI.Button ReviveTimePressureNode::_closeBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____closeBtn_9;
	// Zenject.DiContainer ReviveTimePressureNode::_container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____container_10;
	// Tayr.ILibrary ReviveTimePressureNode::_uiMain
	RuntimeObject* ____uiMain_11;
	// UserVO ReviveTimePressureNode::_userVO
	UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * ____userVO_12;
	// SpellsSO ReviveTimePressureNode::_spellsSO
	SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * ____spellsSO_13;
	// BattleSettingsSO ReviveTimePressureNode::_battleSettingsSO
	BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE * ____battleSettingsSO_14;
	// InventorySystem ReviveTimePressureNode::_inventorySystem
	InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 * ____inventorySystem_15;
	// BattleEcsInitializer ReviveTimePressureNode::_battleEcsInitializer
	BattleEcsInitializer_tA76C29D8A9881FDF3955010F51F9DA4FA7134C67 * ____battleEcsInitializer_16;
	// KickerManager ReviveTimePressureNode::_kickerManager
	KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA * ____kickerManager_17;
	// Tayr.TSoundSystem ReviveTimePressureNode::_soundSystem
	TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511 * ____soundSystem_18;
	// SoundSO ReviveTimePressureNode::_soundSO
	SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73 * ____soundSO_19;

public:
	inline static int32_t get_offset_of__reviveCost_7() { return static_cast<int32_t>(offsetof(ReviveTimePressureNode_t184083765D50D32D7C3EDC4180F7E3298BF82CFB, ____reviveCost_7)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__reviveCost_7() const { return ____reviveCost_7; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__reviveCost_7() { return &____reviveCost_7; }
	inline void set__reviveCost_7(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____reviveCost_7 = value;
		Il2CppCodeGenWriteBarrier((&____reviveCost_7), value);
	}

	inline static int32_t get_offset_of__revive_8() { return static_cast<int32_t>(offsetof(ReviveTimePressureNode_t184083765D50D32D7C3EDC4180F7E3298BF82CFB, ____revive_8)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__revive_8() const { return ____revive_8; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__revive_8() { return &____revive_8; }
	inline void set__revive_8(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____revive_8 = value;
		Il2CppCodeGenWriteBarrier((&____revive_8), value);
	}

	inline static int32_t get_offset_of__closeBtn_9() { return static_cast<int32_t>(offsetof(ReviveTimePressureNode_t184083765D50D32D7C3EDC4180F7E3298BF82CFB, ____closeBtn_9)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__closeBtn_9() const { return ____closeBtn_9; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__closeBtn_9() { return &____closeBtn_9; }
	inline void set__closeBtn_9(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____closeBtn_9 = value;
		Il2CppCodeGenWriteBarrier((&____closeBtn_9), value);
	}

	inline static int32_t get_offset_of__container_10() { return static_cast<int32_t>(offsetof(ReviveTimePressureNode_t184083765D50D32D7C3EDC4180F7E3298BF82CFB, ____container_10)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__container_10() const { return ____container_10; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__container_10() { return &____container_10; }
	inline void set__container_10(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____container_10 = value;
		Il2CppCodeGenWriteBarrier((&____container_10), value);
	}

	inline static int32_t get_offset_of__uiMain_11() { return static_cast<int32_t>(offsetof(ReviveTimePressureNode_t184083765D50D32D7C3EDC4180F7E3298BF82CFB, ____uiMain_11)); }
	inline RuntimeObject* get__uiMain_11() const { return ____uiMain_11; }
	inline RuntimeObject** get_address_of__uiMain_11() { return &____uiMain_11; }
	inline void set__uiMain_11(RuntimeObject* value)
	{
		____uiMain_11 = value;
		Il2CppCodeGenWriteBarrier((&____uiMain_11), value);
	}

	inline static int32_t get_offset_of__userVO_12() { return static_cast<int32_t>(offsetof(ReviveTimePressureNode_t184083765D50D32D7C3EDC4180F7E3298BF82CFB, ____userVO_12)); }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * get__userVO_12() const { return ____userVO_12; }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 ** get_address_of__userVO_12() { return &____userVO_12; }
	inline void set__userVO_12(UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * value)
	{
		____userVO_12 = value;
		Il2CppCodeGenWriteBarrier((&____userVO_12), value);
	}

	inline static int32_t get_offset_of__spellsSO_13() { return static_cast<int32_t>(offsetof(ReviveTimePressureNode_t184083765D50D32D7C3EDC4180F7E3298BF82CFB, ____spellsSO_13)); }
	inline SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * get__spellsSO_13() const { return ____spellsSO_13; }
	inline SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 ** get_address_of__spellsSO_13() { return &____spellsSO_13; }
	inline void set__spellsSO_13(SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * value)
	{
		____spellsSO_13 = value;
		Il2CppCodeGenWriteBarrier((&____spellsSO_13), value);
	}

	inline static int32_t get_offset_of__battleSettingsSO_14() { return static_cast<int32_t>(offsetof(ReviveTimePressureNode_t184083765D50D32D7C3EDC4180F7E3298BF82CFB, ____battleSettingsSO_14)); }
	inline BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE * get__battleSettingsSO_14() const { return ____battleSettingsSO_14; }
	inline BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE ** get_address_of__battleSettingsSO_14() { return &____battleSettingsSO_14; }
	inline void set__battleSettingsSO_14(BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE * value)
	{
		____battleSettingsSO_14 = value;
		Il2CppCodeGenWriteBarrier((&____battleSettingsSO_14), value);
	}

	inline static int32_t get_offset_of__inventorySystem_15() { return static_cast<int32_t>(offsetof(ReviveTimePressureNode_t184083765D50D32D7C3EDC4180F7E3298BF82CFB, ____inventorySystem_15)); }
	inline InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 * get__inventorySystem_15() const { return ____inventorySystem_15; }
	inline InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 ** get_address_of__inventorySystem_15() { return &____inventorySystem_15; }
	inline void set__inventorySystem_15(InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 * value)
	{
		____inventorySystem_15 = value;
		Il2CppCodeGenWriteBarrier((&____inventorySystem_15), value);
	}

	inline static int32_t get_offset_of__battleEcsInitializer_16() { return static_cast<int32_t>(offsetof(ReviveTimePressureNode_t184083765D50D32D7C3EDC4180F7E3298BF82CFB, ____battleEcsInitializer_16)); }
	inline BattleEcsInitializer_tA76C29D8A9881FDF3955010F51F9DA4FA7134C67 * get__battleEcsInitializer_16() const { return ____battleEcsInitializer_16; }
	inline BattleEcsInitializer_tA76C29D8A9881FDF3955010F51F9DA4FA7134C67 ** get_address_of__battleEcsInitializer_16() { return &____battleEcsInitializer_16; }
	inline void set__battleEcsInitializer_16(BattleEcsInitializer_tA76C29D8A9881FDF3955010F51F9DA4FA7134C67 * value)
	{
		____battleEcsInitializer_16 = value;
		Il2CppCodeGenWriteBarrier((&____battleEcsInitializer_16), value);
	}

	inline static int32_t get_offset_of__kickerManager_17() { return static_cast<int32_t>(offsetof(ReviveTimePressureNode_t184083765D50D32D7C3EDC4180F7E3298BF82CFB, ____kickerManager_17)); }
	inline KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA * get__kickerManager_17() const { return ____kickerManager_17; }
	inline KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA ** get_address_of__kickerManager_17() { return &____kickerManager_17; }
	inline void set__kickerManager_17(KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA * value)
	{
		____kickerManager_17 = value;
		Il2CppCodeGenWriteBarrier((&____kickerManager_17), value);
	}

	inline static int32_t get_offset_of__soundSystem_18() { return static_cast<int32_t>(offsetof(ReviveTimePressureNode_t184083765D50D32D7C3EDC4180F7E3298BF82CFB, ____soundSystem_18)); }
	inline TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511 * get__soundSystem_18() const { return ____soundSystem_18; }
	inline TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511 ** get_address_of__soundSystem_18() { return &____soundSystem_18; }
	inline void set__soundSystem_18(TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511 * value)
	{
		____soundSystem_18 = value;
		Il2CppCodeGenWriteBarrier((&____soundSystem_18), value);
	}

	inline static int32_t get_offset_of__soundSO_19() { return static_cast<int32_t>(offsetof(ReviveTimePressureNode_t184083765D50D32D7C3EDC4180F7E3298BF82CFB, ____soundSO_19)); }
	inline SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73 * get__soundSO_19() const { return ____soundSO_19; }
	inline SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73 ** get_address_of__soundSO_19() { return &____soundSO_19; }
	inline void set__soundSO_19(SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73 * value)
	{
		____soundSO_19 = value;
		Il2CppCodeGenWriteBarrier((&____soundSO_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REVIVETIMEPRESSURENODE_T184083765D50D32D7C3EDC4180F7E3298BF82CFB_H
#ifndef SIMPLETUTORIALNODE_TE4B38BD3ACE9EF2381DB70698C62521BD631156F_H
#define SIMPLETUTORIALNODE_TE4B38BD3ACE9EF2381DB70698C62521BD631156F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleTutorialNode
struct  SimpleTutorialNode_tE4B38BD3ACE9EF2381DB70698C62521BD631156F  : public BasicNode_1_t31F5A99F9379D18DE55F22D14196E346D7C1FADB
{
public:
	// TMPro.TextMeshProUGUI SimpleTutorialNode::_description
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____description_7;
	// PerLevelTutorialSO SimpleTutorialNode::_tutorialSO
	PerLevelTutorialSO_tB55F6372D80FCA75DD834538D5137169A7A7E66B * ____tutorialSO_8;
	// PerLevelTutorialModel SimpleTutorialNode::_tutorialModel
	PerLevelTutorialModel_t617000CC336C47983E77A6E6171F0DE08379A61C * ____tutorialModel_9;

public:
	inline static int32_t get_offset_of__description_7() { return static_cast<int32_t>(offsetof(SimpleTutorialNode_tE4B38BD3ACE9EF2381DB70698C62521BD631156F, ____description_7)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__description_7() const { return ____description_7; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__description_7() { return &____description_7; }
	inline void set__description_7(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____description_7 = value;
		Il2CppCodeGenWriteBarrier((&____description_7), value);
	}

	inline static int32_t get_offset_of__tutorialSO_8() { return static_cast<int32_t>(offsetof(SimpleTutorialNode_tE4B38BD3ACE9EF2381DB70698C62521BD631156F, ____tutorialSO_8)); }
	inline PerLevelTutorialSO_tB55F6372D80FCA75DD834538D5137169A7A7E66B * get__tutorialSO_8() const { return ____tutorialSO_8; }
	inline PerLevelTutorialSO_tB55F6372D80FCA75DD834538D5137169A7A7E66B ** get_address_of__tutorialSO_8() { return &____tutorialSO_8; }
	inline void set__tutorialSO_8(PerLevelTutorialSO_tB55F6372D80FCA75DD834538D5137169A7A7E66B * value)
	{
		____tutorialSO_8 = value;
		Il2CppCodeGenWriteBarrier((&____tutorialSO_8), value);
	}

	inline static int32_t get_offset_of__tutorialModel_9() { return static_cast<int32_t>(offsetof(SimpleTutorialNode_tE4B38BD3ACE9EF2381DB70698C62521BD631156F, ____tutorialModel_9)); }
	inline PerLevelTutorialModel_t617000CC336C47983E77A6E6171F0DE08379A61C * get__tutorialModel_9() const { return ____tutorialModel_9; }
	inline PerLevelTutorialModel_t617000CC336C47983E77A6E6171F0DE08379A61C ** get_address_of__tutorialModel_9() { return &____tutorialModel_9; }
	inline void set__tutorialModel_9(PerLevelTutorialModel_t617000CC336C47983E77A6E6171F0DE08379A61C * value)
	{
		____tutorialModel_9 = value;
		Il2CppCodeGenWriteBarrier((&____tutorialModel_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLETUTORIALNODE_TE4B38BD3ACE9EF2381DB70698C62521BD631156F_H
#ifndef SKILLTUTORIALNODE_T21AA20D8086E6AAEBF4CE8ECB99B16A10D2A1C91_H
#define SKILLTUTORIALNODE_T21AA20D8086E6AAEBF4CE8ECB99B16A10D2A1C91_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SkillTutorialNode
struct  SkillTutorialNode_t21AA20D8086E6AAEBF4CE8ECB99B16A10D2A1C91  : public BasicNode_1_t56EF59E5C4BF3DD3B0683550A56A5B17B8A2520D
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKILLTUTORIALNODE_T21AA20D8086E6AAEBF4CE8ECB99B16A10D2A1C91_H
#ifndef WINSUMMARYNODE_TA1C713FFA6B2CFC5DC5781097398E1A461EF911F_H
#define WINSUMMARYNODE_TA1C713FFA6B2CFC5DC5781097398E1A461EF911F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WinSummaryNode
struct  WinSummaryNode_tA1C713FFA6B2CFC5DC5781097398E1A461EF911F  : public BasicNode_1_t2B66230344420DCD7C3E07CA1F5ACAD424BB1EC5
{
public:
	// Tayr.TLocalizerTextMeshProUGUI WinSummaryNode::_level
	TLocalizerTextMeshProUGUI_tC187D21D45AC70D0B204800BB874B0BEDAAFD557 * ____level_7;
	// UnityEngine.UI.Button WinSummaryNode::_continueBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____continueBtn_8;
	// Tayr.ILibrary WinSummaryNode::_uiMain
	RuntimeObject* ____uiMain_9;
	// Zenject.DiContainer WinSummaryNode::_container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____container_10;
	// UnityEngine.GameObject WinSummaryNode::FxGameObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___FxGameObject_11;
	// Tayr.TSoundSystem WinSummaryNode::_soundSystem
	TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511 * ____soundSystem_12;
	// SoundSO WinSummaryNode::_soundSO
	SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73 * ____soundSO_13;
	// UnityEngine.GameObject WinSummaryNode::_stars
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____stars_14;

public:
	inline static int32_t get_offset_of__level_7() { return static_cast<int32_t>(offsetof(WinSummaryNode_tA1C713FFA6B2CFC5DC5781097398E1A461EF911F, ____level_7)); }
	inline TLocalizerTextMeshProUGUI_tC187D21D45AC70D0B204800BB874B0BEDAAFD557 * get__level_7() const { return ____level_7; }
	inline TLocalizerTextMeshProUGUI_tC187D21D45AC70D0B204800BB874B0BEDAAFD557 ** get_address_of__level_7() { return &____level_7; }
	inline void set__level_7(TLocalizerTextMeshProUGUI_tC187D21D45AC70D0B204800BB874B0BEDAAFD557 * value)
	{
		____level_7 = value;
		Il2CppCodeGenWriteBarrier((&____level_7), value);
	}

	inline static int32_t get_offset_of__continueBtn_8() { return static_cast<int32_t>(offsetof(WinSummaryNode_tA1C713FFA6B2CFC5DC5781097398E1A461EF911F, ____continueBtn_8)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__continueBtn_8() const { return ____continueBtn_8; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__continueBtn_8() { return &____continueBtn_8; }
	inline void set__continueBtn_8(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____continueBtn_8 = value;
		Il2CppCodeGenWriteBarrier((&____continueBtn_8), value);
	}

	inline static int32_t get_offset_of__uiMain_9() { return static_cast<int32_t>(offsetof(WinSummaryNode_tA1C713FFA6B2CFC5DC5781097398E1A461EF911F, ____uiMain_9)); }
	inline RuntimeObject* get__uiMain_9() const { return ____uiMain_9; }
	inline RuntimeObject** get_address_of__uiMain_9() { return &____uiMain_9; }
	inline void set__uiMain_9(RuntimeObject* value)
	{
		____uiMain_9 = value;
		Il2CppCodeGenWriteBarrier((&____uiMain_9), value);
	}

	inline static int32_t get_offset_of__container_10() { return static_cast<int32_t>(offsetof(WinSummaryNode_tA1C713FFA6B2CFC5DC5781097398E1A461EF911F, ____container_10)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__container_10() const { return ____container_10; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__container_10() { return &____container_10; }
	inline void set__container_10(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____container_10 = value;
		Il2CppCodeGenWriteBarrier((&____container_10), value);
	}

	inline static int32_t get_offset_of_FxGameObject_11() { return static_cast<int32_t>(offsetof(WinSummaryNode_tA1C713FFA6B2CFC5DC5781097398E1A461EF911F, ___FxGameObject_11)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_FxGameObject_11() const { return ___FxGameObject_11; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_FxGameObject_11() { return &___FxGameObject_11; }
	inline void set_FxGameObject_11(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___FxGameObject_11 = value;
		Il2CppCodeGenWriteBarrier((&___FxGameObject_11), value);
	}

	inline static int32_t get_offset_of__soundSystem_12() { return static_cast<int32_t>(offsetof(WinSummaryNode_tA1C713FFA6B2CFC5DC5781097398E1A461EF911F, ____soundSystem_12)); }
	inline TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511 * get__soundSystem_12() const { return ____soundSystem_12; }
	inline TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511 ** get_address_of__soundSystem_12() { return &____soundSystem_12; }
	inline void set__soundSystem_12(TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511 * value)
	{
		____soundSystem_12 = value;
		Il2CppCodeGenWriteBarrier((&____soundSystem_12), value);
	}

	inline static int32_t get_offset_of__soundSO_13() { return static_cast<int32_t>(offsetof(WinSummaryNode_tA1C713FFA6B2CFC5DC5781097398E1A461EF911F, ____soundSO_13)); }
	inline SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73 * get__soundSO_13() const { return ____soundSO_13; }
	inline SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73 ** get_address_of__soundSO_13() { return &____soundSO_13; }
	inline void set__soundSO_13(SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73 * value)
	{
		____soundSO_13 = value;
		Il2CppCodeGenWriteBarrier((&____soundSO_13), value);
	}

	inline static int32_t get_offset_of__stars_14() { return static_cast<int32_t>(offsetof(WinSummaryNode_tA1C713FFA6B2CFC5DC5781097398E1A461EF911F, ____stars_14)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__stars_14() const { return ____stars_14; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__stars_14() { return &____stars_14; }
	inline void set__stars_14(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____stars_14 = value;
		Il2CppCodeGenWriteBarrier((&____stars_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WINSUMMARYNODE_TA1C713FFA6B2CFC5DC5781097398E1A461EF911F_H
#ifndef FIRSTSKILLTUTORIALNODE_T6A610C4EE426DE5AE3E6677C4574CDAE89B904C8_H
#define FIRSTSKILLTUTORIALNODE_T6A610C4EE426DE5AE3E6677C4574CDAE89B904C8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FirstSkillTutorialNode
struct  FirstSkillTutorialNode_t6A610C4EE426DE5AE3E6677C4574CDAE89B904C8  : public SkillTutorialNode_t21AA20D8086E6AAEBF4CE8ECB99B16A10D2A1C91
{
public:
	// TMPro.TextMeshProUGUI FirstSkillTutorialNode::_selectDescription
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____selectDescription_7;
	// UnityEngine.Transform FirstSkillTutorialNode::_selectPopup
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____selectPopup_8;
	// TMPro.TextMeshProUGUI FirstSkillTutorialNode::_actionDescription
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____actionDescription_9;
	// UnityEngine.Transform FirstSkillTutorialNode::_actionPopup
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____actionPopup_10;
	// UnityEngine.Transform FirstSkillTutorialNode::_arrow
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____arrow_11;

public:
	inline static int32_t get_offset_of__selectDescription_7() { return static_cast<int32_t>(offsetof(FirstSkillTutorialNode_t6A610C4EE426DE5AE3E6677C4574CDAE89B904C8, ____selectDescription_7)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__selectDescription_7() const { return ____selectDescription_7; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__selectDescription_7() { return &____selectDescription_7; }
	inline void set__selectDescription_7(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____selectDescription_7 = value;
		Il2CppCodeGenWriteBarrier((&____selectDescription_7), value);
	}

	inline static int32_t get_offset_of__selectPopup_8() { return static_cast<int32_t>(offsetof(FirstSkillTutorialNode_t6A610C4EE426DE5AE3E6677C4574CDAE89B904C8, ____selectPopup_8)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__selectPopup_8() const { return ____selectPopup_8; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__selectPopup_8() { return &____selectPopup_8; }
	inline void set__selectPopup_8(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____selectPopup_8 = value;
		Il2CppCodeGenWriteBarrier((&____selectPopup_8), value);
	}

	inline static int32_t get_offset_of__actionDescription_9() { return static_cast<int32_t>(offsetof(FirstSkillTutorialNode_t6A610C4EE426DE5AE3E6677C4574CDAE89B904C8, ____actionDescription_9)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__actionDescription_9() const { return ____actionDescription_9; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__actionDescription_9() { return &____actionDescription_9; }
	inline void set__actionDescription_9(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____actionDescription_9 = value;
		Il2CppCodeGenWriteBarrier((&____actionDescription_9), value);
	}

	inline static int32_t get_offset_of__actionPopup_10() { return static_cast<int32_t>(offsetof(FirstSkillTutorialNode_t6A610C4EE426DE5AE3E6677C4574CDAE89B904C8, ____actionPopup_10)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__actionPopup_10() const { return ____actionPopup_10; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__actionPopup_10() { return &____actionPopup_10; }
	inline void set__actionPopup_10(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____actionPopup_10 = value;
		Il2CppCodeGenWriteBarrier((&____actionPopup_10), value);
	}

	inline static int32_t get_offset_of__arrow_11() { return static_cast<int32_t>(offsetof(FirstSkillTutorialNode_t6A610C4EE426DE5AE3E6677C4574CDAE89B904C8, ____arrow_11)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__arrow_11() const { return ____arrow_11; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__arrow_11() { return &____arrow_11; }
	inline void set__arrow_11(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____arrow_11 = value;
		Il2CppCodeGenWriteBarrier((&____arrow_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIRSTSKILLTUTORIALNODE_T6A610C4EE426DE5AE3E6677C4574CDAE89B904C8_H
#ifndef FOURTHSKILLTUTORIALNODE_T54BD10C3CA58AB022A554FFCA2723F0D0FB20057_H
#define FOURTHSKILLTUTORIALNODE_T54BD10C3CA58AB022A554FFCA2723F0D0FB20057_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FourthSkillTutorialNode
struct  FourthSkillTutorialNode_t54BD10C3CA58AB022A554FFCA2723F0D0FB20057  : public SkillTutorialNode_t21AA20D8086E6AAEBF4CE8ECB99B16A10D2A1C91
{
public:
	// TMPro.TextMeshProUGUI FourthSkillTutorialNode::_selectDescription
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____selectDescription_7;
	// UnityEngine.Transform FourthSkillTutorialNode::_selectPopup
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____selectPopup_8;
	// TMPro.TextMeshProUGUI FourthSkillTutorialNode::_actionDescription
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____actionDescription_9;
	// UnityEngine.Transform FourthSkillTutorialNode::_actionPopup
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____actionPopup_10;
	// UnityEngine.Transform FourthSkillTutorialNode::_arrow
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____arrow_11;
	// KickerManager FourthSkillTutorialNode::_kickerManager
	KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA * ____kickerManager_12;

public:
	inline static int32_t get_offset_of__selectDescription_7() { return static_cast<int32_t>(offsetof(FourthSkillTutorialNode_t54BD10C3CA58AB022A554FFCA2723F0D0FB20057, ____selectDescription_7)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__selectDescription_7() const { return ____selectDescription_7; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__selectDescription_7() { return &____selectDescription_7; }
	inline void set__selectDescription_7(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____selectDescription_7 = value;
		Il2CppCodeGenWriteBarrier((&____selectDescription_7), value);
	}

	inline static int32_t get_offset_of__selectPopup_8() { return static_cast<int32_t>(offsetof(FourthSkillTutorialNode_t54BD10C3CA58AB022A554FFCA2723F0D0FB20057, ____selectPopup_8)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__selectPopup_8() const { return ____selectPopup_8; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__selectPopup_8() { return &____selectPopup_8; }
	inline void set__selectPopup_8(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____selectPopup_8 = value;
		Il2CppCodeGenWriteBarrier((&____selectPopup_8), value);
	}

	inline static int32_t get_offset_of__actionDescription_9() { return static_cast<int32_t>(offsetof(FourthSkillTutorialNode_t54BD10C3CA58AB022A554FFCA2723F0D0FB20057, ____actionDescription_9)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__actionDescription_9() const { return ____actionDescription_9; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__actionDescription_9() { return &____actionDescription_9; }
	inline void set__actionDescription_9(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____actionDescription_9 = value;
		Il2CppCodeGenWriteBarrier((&____actionDescription_9), value);
	}

	inline static int32_t get_offset_of__actionPopup_10() { return static_cast<int32_t>(offsetof(FourthSkillTutorialNode_t54BD10C3CA58AB022A554FFCA2723F0D0FB20057, ____actionPopup_10)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__actionPopup_10() const { return ____actionPopup_10; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__actionPopup_10() { return &____actionPopup_10; }
	inline void set__actionPopup_10(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____actionPopup_10 = value;
		Il2CppCodeGenWriteBarrier((&____actionPopup_10), value);
	}

	inline static int32_t get_offset_of__arrow_11() { return static_cast<int32_t>(offsetof(FourthSkillTutorialNode_t54BD10C3CA58AB022A554FFCA2723F0D0FB20057, ____arrow_11)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__arrow_11() const { return ____arrow_11; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__arrow_11() { return &____arrow_11; }
	inline void set__arrow_11(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____arrow_11 = value;
		Il2CppCodeGenWriteBarrier((&____arrow_11), value);
	}

	inline static int32_t get_offset_of__kickerManager_12() { return static_cast<int32_t>(offsetof(FourthSkillTutorialNode_t54BD10C3CA58AB022A554FFCA2723F0D0FB20057, ____kickerManager_12)); }
	inline KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA * get__kickerManager_12() const { return ____kickerManager_12; }
	inline KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA ** get_address_of__kickerManager_12() { return &____kickerManager_12; }
	inline void set__kickerManager_12(KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA * value)
	{
		____kickerManager_12 = value;
		Il2CppCodeGenWriteBarrier((&____kickerManager_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FOURTHSKILLTUTORIALNODE_T54BD10C3CA58AB022A554FFCA2723F0D0FB20057_H
#ifndef GOALSTUTORIALNODE_TF301448520894286687F236F80234B8A56CBE7B1_H
#define GOALSTUTORIALNODE_TF301448520894286687F236F80234B8A56CBE7B1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoalsTutorialNode
struct  GoalsTutorialNode_tF301448520894286687F236F80234B8A56CBE7B1  : public SkillTutorialNode_t21AA20D8086E6AAEBF4CE8ECB99B16A10D2A1C91
{
public:
	// UnityEngine.UI.Button GoalsTutorialNode::_continueBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____continueBtn_7;
	// UnityEngine.Transform GoalsTutorialNode::_descriptionPopup
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____descriptionPopup_8;
	// UnityEngine.Transform GoalsTutorialNode::_arrow
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____arrow_9;
	// System.Boolean GoalsTutorialNode::_continue
	bool ____continue_10;

public:
	inline static int32_t get_offset_of__continueBtn_7() { return static_cast<int32_t>(offsetof(GoalsTutorialNode_tF301448520894286687F236F80234B8A56CBE7B1, ____continueBtn_7)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__continueBtn_7() const { return ____continueBtn_7; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__continueBtn_7() { return &____continueBtn_7; }
	inline void set__continueBtn_7(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____continueBtn_7 = value;
		Il2CppCodeGenWriteBarrier((&____continueBtn_7), value);
	}

	inline static int32_t get_offset_of__descriptionPopup_8() { return static_cast<int32_t>(offsetof(GoalsTutorialNode_tF301448520894286687F236F80234B8A56CBE7B1, ____descriptionPopup_8)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__descriptionPopup_8() const { return ____descriptionPopup_8; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__descriptionPopup_8() { return &____descriptionPopup_8; }
	inline void set__descriptionPopup_8(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____descriptionPopup_8 = value;
		Il2CppCodeGenWriteBarrier((&____descriptionPopup_8), value);
	}

	inline static int32_t get_offset_of__arrow_9() { return static_cast<int32_t>(offsetof(GoalsTutorialNode_tF301448520894286687F236F80234B8A56CBE7B1, ____arrow_9)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__arrow_9() const { return ____arrow_9; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__arrow_9() { return &____arrow_9; }
	inline void set__arrow_9(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____arrow_9 = value;
		Il2CppCodeGenWriteBarrier((&____arrow_9), value);
	}

	inline static int32_t get_offset_of__continue_10() { return static_cast<int32_t>(offsetof(GoalsTutorialNode_tF301448520894286687F236F80234B8A56CBE7B1, ____continue_10)); }
	inline bool get__continue_10() const { return ____continue_10; }
	inline bool* get_address_of__continue_10() { return &____continue_10; }
	inline void set__continue_10(bool value)
	{
		____continue_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GOALSTUTORIALNODE_TF301448520894286687F236F80234B8A56CBE7B1_H
#ifndef SECONDSKILLTUTORIALNODE_T1E6ECA8797A902ABA81F995DC3640D0FCC725A7F_H
#define SECONDSKILLTUTORIALNODE_T1E6ECA8797A902ABA81F995DC3640D0FCC725A7F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SecondSkillTutorialNode
struct  SecondSkillTutorialNode_t1E6ECA8797A902ABA81F995DC3640D0FCC725A7F  : public SkillTutorialNode_t21AA20D8086E6AAEBF4CE8ECB99B16A10D2A1C91
{
public:
	// TMPro.TextMeshProUGUI SecondSkillTutorialNode::_selectDescription
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____selectDescription_7;
	// UnityEngine.Transform SecondSkillTutorialNode::_selectPopup
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____selectPopup_8;
	// UnityEngine.Transform SecondSkillTutorialNode::_arrow
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____arrow_9;

public:
	inline static int32_t get_offset_of__selectDescription_7() { return static_cast<int32_t>(offsetof(SecondSkillTutorialNode_t1E6ECA8797A902ABA81F995DC3640D0FCC725A7F, ____selectDescription_7)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__selectDescription_7() const { return ____selectDescription_7; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__selectDescription_7() { return &____selectDescription_7; }
	inline void set__selectDescription_7(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____selectDescription_7 = value;
		Il2CppCodeGenWriteBarrier((&____selectDescription_7), value);
	}

	inline static int32_t get_offset_of__selectPopup_8() { return static_cast<int32_t>(offsetof(SecondSkillTutorialNode_t1E6ECA8797A902ABA81F995DC3640D0FCC725A7F, ____selectPopup_8)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__selectPopup_8() const { return ____selectPopup_8; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__selectPopup_8() { return &____selectPopup_8; }
	inline void set__selectPopup_8(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____selectPopup_8 = value;
		Il2CppCodeGenWriteBarrier((&____selectPopup_8), value);
	}

	inline static int32_t get_offset_of__arrow_9() { return static_cast<int32_t>(offsetof(SecondSkillTutorialNode_t1E6ECA8797A902ABA81F995DC3640D0FCC725A7F, ____arrow_9)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__arrow_9() const { return ____arrow_9; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__arrow_9() { return &____arrow_9; }
	inline void set__arrow_9(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____arrow_9 = value;
		Il2CppCodeGenWriteBarrier((&____arrow_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECONDSKILLTUTORIALNODE_T1E6ECA8797A902ABA81F995DC3640D0FCC725A7F_H
#ifndef THIRDSKILLTUTORIALNODE_TE7A98B1237FF73D72F138C0C69D59D0E36C3BA1D_H
#define THIRDSKILLTUTORIALNODE_TE7A98B1237FF73D72F138C0C69D59D0E36C3BA1D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdSkillTutorialNode
struct  ThirdSkillTutorialNode_tE7A98B1237FF73D72F138C0C69D59D0E36C3BA1D  : public SkillTutorialNode_t21AA20D8086E6AAEBF4CE8ECB99B16A10D2A1C91
{
public:
	// UnityEngine.Transform ThirdSkillTutorialNode::_selectPopup
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____selectPopup_7;
	// UnityEngine.Transform ThirdSkillTutorialNode::_arrow
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____arrow_8;

public:
	inline static int32_t get_offset_of__selectPopup_7() { return static_cast<int32_t>(offsetof(ThirdSkillTutorialNode_tE7A98B1237FF73D72F138C0C69D59D0E36C3BA1D, ____selectPopup_7)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__selectPopup_7() const { return ____selectPopup_7; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__selectPopup_7() { return &____selectPopup_7; }
	inline void set__selectPopup_7(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____selectPopup_7 = value;
		Il2CppCodeGenWriteBarrier((&____selectPopup_7), value);
	}

	inline static int32_t get_offset_of__arrow_8() { return static_cast<int32_t>(offsetof(ThirdSkillTutorialNode_tE7A98B1237FF73D72F138C0C69D59D0E36C3BA1D, ____arrow_8)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__arrow_8() const { return ____arrow_8; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__arrow_8() { return &____arrow_8; }
	inline void set__arrow_8(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____arrow_8 = value;
		Il2CppCodeGenWriteBarrier((&____arrow_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THIRDSKILLTUTORIALNODE_TE7A98B1237FF73D72F138C0C69D59D0E36C3BA1D_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8100 = { sizeof (U3COnButtonTapU3Ed__31_t10681CEEA7391B8166E0EF699E793CDFEF35F325)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8100[5] = 
{
	U3COnButtonTapU3Ed__31_t10681CEEA7391B8166E0EF699E793CDFEF35F325::get_offset_of_U3CU3E1__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3COnButtonTapU3Ed__31_t10681CEEA7391B8166E0EF699E793CDFEF35F325::get_offset_of_U3CU3Et__builder_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3COnButtonTapU3Ed__31_t10681CEEA7391B8166E0EF699E793CDFEF35F325::get_offset_of_index_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3COnButtonTapU3Ed__31_t10681CEEA7391B8166E0EF699E793CDFEF35F325::get_offset_of_U3CU3E4__this_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3COnButtonTapU3Ed__31_t10681CEEA7391B8166E0EF699E793CDFEF35F325::get_offset_of_U3CU3Eu__1_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8101 = { sizeof (U3COnTowerTapU3Ed__32_t4CCF45D4B4489264E83E2C07CCABBC5485DC5910)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8101[4] = 
{
	U3COnTowerTapU3Ed__32_t4CCF45D4B4489264E83E2C07CCABBC5485DC5910::get_offset_of_U3CU3E1__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3COnTowerTapU3Ed__32_t4CCF45D4B4489264E83E2C07CCABBC5485DC5910::get_offset_of_U3CU3Et__builder_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3COnTowerTapU3Ed__32_t4CCF45D4B4489264E83E2C07CCABBC5485DC5910::get_offset_of_U3CU3E4__this_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3COnTowerTapU3Ed__32_t4CCF45D4B4489264E83E2C07CCABBC5485DC5910::get_offset_of_U3CU3Eu__1_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8102 = { sizeof (BoosterTapComponent_t4873A307B15974377549C253DB2C79D93C041517), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8102[1] = 
{
	BoosterTapComponent_t4873A307B15974377549C253DB2C79D93C041517::get_offset_of_SpellId_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8103 = { sizeof (ButtonTapComponent_t1064F9A8B3F34BBD1333866AF91575596A6E015F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8103[1] = 
{
	ButtonTapComponent_t1064F9A8B3F34BBD1333866AF91575596A6E015F::get_offset_of_SpellId_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8104 = { sizeof (ScreenTapComponent_t3A70AEB8C394143D61F292E42709115ED4CA70EE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8104[1] = 
{
	ScreenTapComponent_t3A70AEB8C394143D61F292E42709115ED4CA70EE::get_offset_of_SpellId_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8105 = { sizeof (TowerTapComponent_tCE28812FC562A2930967569187D82DD8D4DE70D6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8105[2] = 
{
	TowerTapComponent_tCE28812FC562A2930967569187D82DD8D4DE70D6::get_offset_of_SpellId_0(),
	TowerTapComponent_tCE28812FC562A2930967569187D82DD8D4DE70D6::get_offset_of_TowerId_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8106 = { sizeof (BattleNode_t1D1FD268C2AB2C374DBAF83358F99FE4C7A6FF3E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8106[12] = 
{
	BattleNode_t1D1FD268C2AB2C374DBAF83358F99FE4C7A6FF3E::get_offset_of__heroHPBBar_7(),
	BattleNode_t1D1FD268C2AB2C374DBAF83358F99FE4C7A6FF3E::get_offset_of__heroHPBText_8(),
	BattleNode_t1D1FD268C2AB2C374DBAF83358F99FE4C7A6FF3E::get_offset_of__objectivesParent_9(),
	BattleNode_t1D1FD268C2AB2C374DBAF83358F99FE4C7A6FF3E::get_offset_of__pauseBtn_10(),
	BattleNode_t1D1FD268C2AB2C374DBAF83358F99FE4C7A6FF3E::get_offset_of__uiMain_11(),
	BattleNode_t1D1FD268C2AB2C374DBAF83358F99FE4C7A6FF3E::get_offset_of__container_12(),
	BattleNode_t1D1FD268C2AB2C374DBAF83358F99FE4C7A6FF3E::get_offset_of__soundSystem_13(),
	BattleNode_t1D1FD268C2AB2C374DBAF83358F99FE4C7A6FF3E::get_offset_of__soundSO_14(),
	BattleNode_t1D1FD268C2AB2C374DBAF83358F99FE4C7A6FF3E::get_offset_of__userVO_15(),
	BattleNode_t1D1FD268C2AB2C374DBAF83358F99FE4C7A6FF3E::get_offset_of__inventoryVO_16(),
	BattleNode_t1D1FD268C2AB2C374DBAF83358F99FE4C7A6FF3E::get_offset_of__objectiveDictionary_17(),
	BattleNode_t1D1FD268C2AB2C374DBAF83358F99FE4C7A6FF3E::get_offset_of__currentHP_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8107 = { sizeof (DamageKicker_t32D75279096E1962C07DA19EDE11BE5EFEADC5FD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8107[6] = 
{
	DamageKicker_t32D75279096E1962C07DA19EDE11BE5EFEADC5FD::get_offset_of__text_4(),
	DamageKicker_t32D75279096E1962C07DA19EDE11BE5EFEADC5FD::get_offset_of__accumulateDuration_5(),
	DamageKicker_t32D75279096E1962C07DA19EDE11BE5EFEADC5FD::get_offset_of__lastAccumulation_6(),
	DamageKicker_t32D75279096E1962C07DA19EDE11BE5EFEADC5FD::get_offset_of__damageAccumulated_7(),
	DamageKicker_t32D75279096E1962C07DA19EDE11BE5EFEADC5FD::get_offset_of__punchVector_8(),
	DamageKicker_t32D75279096E1962C07DA19EDE11BE5EFEADC5FD::get_offset_of__tweener_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8108 = { sizeof (HpBar_tB262BDBB4A983FC76CE9D9AE362FB00D608A93F9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8108[1] = 
{
	HpBar_tB262BDBB4A983FC76CE9D9AE362FB00D608A93F9::get_offset_of__uiHp_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8109 = { sizeof (TimeCounterUIComponent_t3AD995E2AC46D2BF82AF866C4F7651BF9928E245), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8109[2] = 
{
	TimeCounterUIComponent_t3AD995E2AC46D2BF82AF866C4F7651BF9928E245::get_offset_of__counter_4(),
	TimeCounterUIComponent_t3AD995E2AC46D2BF82AF866C4F7651BF9928E245::get_offset_of__animator_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8110 = { sizeof (UIObjective_t94D85D5DE412998C059AE6A45DB99258B95E8023), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8110[9] = 
{
	UIObjective_t94D85D5DE412998C059AE6A45DB99258B95E8023::get_offset_of__icon_4(),
	UIObjective_t94D85D5DE412998C059AE6A45DB99258B95E8023::get_offset_of__finish_5(),
	UIObjective_t94D85D5DE412998C059AE6A45DB99258B95E8023::get_offset_of__countText_6(),
	UIObjective_t94D85D5DE412998C059AE6A45DB99258B95E8023::get_offset_of__achievementParticle_7(),
	UIObjective_t94D85D5DE412998C059AE6A45DB99258B95E8023::get_offset_of__itemsSO_8(),
	UIObjective_t94D85D5DE412998C059AE6A45DB99258B95E8023::get_offset_of__towersSO_9(),
	UIObjective_t94D85D5DE412998C059AE6A45DB99258B95E8023::get_offset_of__diContainer_10(),
	UIObjective_t94D85D5DE412998C059AE6A45DB99258B95E8023::get_offset_of__id_11(),
	UIObjective_t94D85D5DE412998C059AE6A45DB99258B95E8023::get_offset_of__count_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8111 = { sizeof (LoseSummaryNode_t36A15CC7032A9119437AD7D99D2C93BCCAB6BAAF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8111[7] = 
{
	LoseSummaryNode_t36A15CC7032A9119437AD7D99D2C93BCCAB6BAAF::get_offset_of__level_7(),
	LoseSummaryNode_t36A15CC7032A9119437AD7D99D2C93BCCAB6BAAF::get_offset_of__tryAgainBtn_8(),
	LoseSummaryNode_t36A15CC7032A9119437AD7D99D2C93BCCAB6BAAF::get_offset_of__closeBtn_9(),
	LoseSummaryNode_t36A15CC7032A9119437AD7D99D2C93BCCAB6BAAF::get_offset_of__uiMain_10(),
	LoseSummaryNode_t36A15CC7032A9119437AD7D99D2C93BCCAB6BAAF::get_offset_of__container_11(),
	LoseSummaryNode_t36A15CC7032A9119437AD7D99D2C93BCCAB6BAAF::get_offset_of__soundSystem_12(),
	LoseSummaryNode_t36A15CC7032A9119437AD7D99D2C93BCCAB6BAAF::get_offset_of__soundSO_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8112 = { sizeof (LoseSummaryNodeData_t9807706F230D328167B227F082922DBD1C0BAA36), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8112[1] = 
{
	LoseSummaryNodeData_t9807706F230D328167B227F082922DBD1C0BAA36::get_offset_of_Level_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8113 = { sizeof (PauseNode_tD76F910C8791B50A883EC9C3537274CD42B07531), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8113[14] = 
{
	PauseNode_tD76F910C8791B50A883EC9C3537274CD42B07531::get_offset_of__closeBtn_7(),
	PauseNode_tD76F910C8791B50A883EC9C3537274CD42B07531::get_offset_of__fxSoundBtn_8(),
	PauseNode_tD76F910C8791B50A883EC9C3537274CD42B07531::get_offset_of__musicSoundBtn_9(),
	PauseNode_tD76F910C8791B50A883EC9C3537274CD42B07531::get_offset_of__fxSoundOn_10(),
	PauseNode_tD76F910C8791B50A883EC9C3537274CD42B07531::get_offset_of__fxSoundOff_11(),
	PauseNode_tD76F910C8791B50A883EC9C3537274CD42B07531::get_offset_of__musicSoundOn_12(),
	PauseNode_tD76F910C8791B50A883EC9C3537274CD42B07531::get_offset_of__musicSoundOff_13(),
	PauseNode_tD76F910C8791B50A883EC9C3537274CD42B07531::get_offset_of__container_14(),
	PauseNode_tD76F910C8791B50A883EC9C3537274CD42B07531::get_offset_of__userVO_15(),
	PauseNode_tD76F910C8791B50A883EC9C3537274CD42B07531::get_offset_of__soundSystem_16(),
	PauseNode_tD76F910C8791B50A883EC9C3537274CD42B07531::get_offset_of__soundSO_17(),
	PauseNode_tD76F910C8791B50A883EC9C3537274CD42B07531::get_offset_of__battleEcsInitializer_18(),
	PauseNode_tD76F910C8791B50A883EC9C3537274CD42B07531::get_offset_of__fx_19(),
	PauseNode_tD76F910C8791B50A883EC9C3537274CD42B07531::get_offset_of__music_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8114 = { sizeof (ReviveNode_t71A1E27EE0DE3CA67D44FFF4DA4D7AE8C47D2FFF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8114[13] = 
{
	ReviveNode_t71A1E27EE0DE3CA67D44FFF4DA4D7AE8C47D2FFF::get_offset_of__reviveCost_7(),
	ReviveNode_t71A1E27EE0DE3CA67D44FFF4DA4D7AE8C47D2FFF::get_offset_of__revive_8(),
	ReviveNode_t71A1E27EE0DE3CA67D44FFF4DA4D7AE8C47D2FFF::get_offset_of__closeBtn_9(),
	ReviveNode_t71A1E27EE0DE3CA67D44FFF4DA4D7AE8C47D2FFF::get_offset_of__container_10(),
	ReviveNode_t71A1E27EE0DE3CA67D44FFF4DA4D7AE8C47D2FFF::get_offset_of__uiMain_11(),
	ReviveNode_t71A1E27EE0DE3CA67D44FFF4DA4D7AE8C47D2FFF::get_offset_of__userVO_12(),
	ReviveNode_t71A1E27EE0DE3CA67D44FFF4DA4D7AE8C47D2FFF::get_offset_of__spellsSO_13(),
	ReviveNode_t71A1E27EE0DE3CA67D44FFF4DA4D7AE8C47D2FFF::get_offset_of__battleSettingsSO_14(),
	ReviveNode_t71A1E27EE0DE3CA67D44FFF4DA4D7AE8C47D2FFF::get_offset_of__inventorySystem_15(),
	ReviveNode_t71A1E27EE0DE3CA67D44FFF4DA4D7AE8C47D2FFF::get_offset_of__battleEcsInitializer_16(),
	ReviveNode_t71A1E27EE0DE3CA67D44FFF4DA4D7AE8C47D2FFF::get_offset_of__kickerManager_17(),
	ReviveNode_t71A1E27EE0DE3CA67D44FFF4DA4D7AE8C47D2FFF::get_offset_of__soundSystem_18(),
	ReviveNode_t71A1E27EE0DE3CA67D44FFF4DA4D7AE8C47D2FFF::get_offset_of__soundSO_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8115 = { sizeof (ReviveTimePressureNode_t184083765D50D32D7C3EDC4180F7E3298BF82CFB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8115[13] = 
{
	ReviveTimePressureNode_t184083765D50D32D7C3EDC4180F7E3298BF82CFB::get_offset_of__reviveCost_7(),
	ReviveTimePressureNode_t184083765D50D32D7C3EDC4180F7E3298BF82CFB::get_offset_of__revive_8(),
	ReviveTimePressureNode_t184083765D50D32D7C3EDC4180F7E3298BF82CFB::get_offset_of__closeBtn_9(),
	ReviveTimePressureNode_t184083765D50D32D7C3EDC4180F7E3298BF82CFB::get_offset_of__container_10(),
	ReviveTimePressureNode_t184083765D50D32D7C3EDC4180F7E3298BF82CFB::get_offset_of__uiMain_11(),
	ReviveTimePressureNode_t184083765D50D32D7C3EDC4180F7E3298BF82CFB::get_offset_of__userVO_12(),
	ReviveTimePressureNode_t184083765D50D32D7C3EDC4180F7E3298BF82CFB::get_offset_of__spellsSO_13(),
	ReviveTimePressureNode_t184083765D50D32D7C3EDC4180F7E3298BF82CFB::get_offset_of__battleSettingsSO_14(),
	ReviveTimePressureNode_t184083765D50D32D7C3EDC4180F7E3298BF82CFB::get_offset_of__inventorySystem_15(),
	ReviveTimePressureNode_t184083765D50D32D7C3EDC4180F7E3298BF82CFB::get_offset_of__battleEcsInitializer_16(),
	ReviveTimePressureNode_t184083765D50D32D7C3EDC4180F7E3298BF82CFB::get_offset_of__kickerManager_17(),
	ReviveTimePressureNode_t184083765D50D32D7C3EDC4180F7E3298BF82CFB::get_offset_of__soundSystem_18(),
	ReviveTimePressureNode_t184083765D50D32D7C3EDC4180F7E3298BF82CFB::get_offset_of__soundSO_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8116 = { sizeof (FirstSkillTutorialNode_t6A610C4EE426DE5AE3E6677C4574CDAE89B904C8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8116[5] = 
{
	FirstSkillTutorialNode_t6A610C4EE426DE5AE3E6677C4574CDAE89B904C8::get_offset_of__selectDescription_7(),
	FirstSkillTutorialNode_t6A610C4EE426DE5AE3E6677C4574CDAE89B904C8::get_offset_of__selectPopup_8(),
	FirstSkillTutorialNode_t6A610C4EE426DE5AE3E6677C4574CDAE89B904C8::get_offset_of__actionDescription_9(),
	FirstSkillTutorialNode_t6A610C4EE426DE5AE3E6677C4574CDAE89B904C8::get_offset_of__actionPopup_10(),
	FirstSkillTutorialNode_t6A610C4EE426DE5AE3E6677C4574CDAE89B904C8::get_offset_of__arrow_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8117 = { sizeof (FourthSkillTutorialNode_t54BD10C3CA58AB022A554FFCA2723F0D0FB20057), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8117[6] = 
{
	FourthSkillTutorialNode_t54BD10C3CA58AB022A554FFCA2723F0D0FB20057::get_offset_of__selectDescription_7(),
	FourthSkillTutorialNode_t54BD10C3CA58AB022A554FFCA2723F0D0FB20057::get_offset_of__selectPopup_8(),
	FourthSkillTutorialNode_t54BD10C3CA58AB022A554FFCA2723F0D0FB20057::get_offset_of__actionDescription_9(),
	FourthSkillTutorialNode_t54BD10C3CA58AB022A554FFCA2723F0D0FB20057::get_offset_of__actionPopup_10(),
	FourthSkillTutorialNode_t54BD10C3CA58AB022A554FFCA2723F0D0FB20057::get_offset_of__arrow_11(),
	FourthSkillTutorialNode_t54BD10C3CA58AB022A554FFCA2723F0D0FB20057::get_offset_of__kickerManager_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8118 = { sizeof (GoalsTutorialNode_tF301448520894286687F236F80234B8A56CBE7B1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8118[4] = 
{
	GoalsTutorialNode_tF301448520894286687F236F80234B8A56CBE7B1::get_offset_of__continueBtn_7(),
	GoalsTutorialNode_tF301448520894286687F236F80234B8A56CBE7B1::get_offset_of__descriptionPopup_8(),
	GoalsTutorialNode_tF301448520894286687F236F80234B8A56CBE7B1::get_offset_of__arrow_9(),
	GoalsTutorialNode_tF301448520894286687F236F80234B8A56CBE7B1::get_offset_of__continue_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8119 = { sizeof (U3COnContinueBtnU3Ed__10_tB742710BD67850A20CB762A74781F5ECAE28B6FB)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8119[4] = 
{
	U3COnContinueBtnU3Ed__10_tB742710BD67850A20CB762A74781F5ECAE28B6FB::get_offset_of_U3CU3E1__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3COnContinueBtnU3Ed__10_tB742710BD67850A20CB762A74781F5ECAE28B6FB::get_offset_of_U3CU3Et__builder_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3COnContinueBtnU3Ed__10_tB742710BD67850A20CB762A74781F5ECAE28B6FB::get_offset_of_U3CU3E4__this_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3COnContinueBtnU3Ed__10_tB742710BD67850A20CB762A74781F5ECAE28B6FB::get_offset_of_U3CU3Eu__1_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8120 = { sizeof (SecondSkillTutorialNode_t1E6ECA8797A902ABA81F995DC3640D0FCC725A7F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8120[3] = 
{
	SecondSkillTutorialNode_t1E6ECA8797A902ABA81F995DC3640D0FCC725A7F::get_offset_of__selectDescription_7(),
	SecondSkillTutorialNode_t1E6ECA8797A902ABA81F995DC3640D0FCC725A7F::get_offset_of__selectPopup_8(),
	SecondSkillTutorialNode_t1E6ECA8797A902ABA81F995DC3640D0FCC725A7F::get_offset_of__arrow_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8121 = { sizeof (SkillTutorialNode_t21AA20D8086E6AAEBF4CE8ECB99B16A10D2A1C91), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8122 = { sizeof (ThirdSkillTutorialNode_tE7A98B1237FF73D72F138C0C69D59D0E36C3BA1D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8122[2] = 
{
	ThirdSkillTutorialNode_tE7A98B1237FF73D72F138C0C69D59D0E36C3BA1D::get_offset_of__selectPopup_7(),
	ThirdSkillTutorialNode_tE7A98B1237FF73D72F138C0C69D59D0E36C3BA1D::get_offset_of__arrow_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8123 = { sizeof (CollectGoalTutorialNode_t1743E320CB9D993CF09AC77A20CED76149C74637), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8123[3] = 
{
	CollectGoalTutorialNode_t1743E320CB9D993CF09AC77A20CED76149C74637::get_offset_of__collectDescription_7(),
	CollectGoalTutorialNode_t1743E320CB9D993CF09AC77A20CED76149C74637::get_offset_of__deliverDescription_8(),
	CollectGoalTutorialNode_t1743E320CB9D993CF09AC77A20CED76149C74637::get_offset_of__arrow_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8124 = { sizeof (DestroyFiveInventoryTutorialNode_tE58CD63865141981AFF5F1A2E9A0A836D1576FF7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8124[1] = 
{
	DestroyFiveInventoryTutorialNode_tE58CD63865141981AFF5F1A2E9A0A836D1576FF7::get_offset_of__description_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8125 = { sizeof (DestroyGoalTutorialNode_tA62D39E749050BBCB967A3B7FA6CE41997DDC58D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8125[1] = 
{
	DestroyGoalTutorialNode_tA62D39E749050BBCB967A3B7FA6CE41997DDC58D::get_offset_of__description_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8126 = { sizeof (DestroyTowerGoalNode_t75EDEEB6BF4F7A62DBA3DFCB647F82385DBFE311), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8126[1] = 
{
	DestroyTowerGoalNode_t75EDEEB6BF4F7A62DBA3DFCB647F82385DBFE311::get_offset_of__description_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8127 = { sizeof (MovementTutorialNode_t612D67CD2F5CEA136C089C042E8C326FECB3018A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8127[1] = 
{
	MovementTutorialNode_t612D67CD2F5CEA136C089C042E8C326FECB3018A::get_offset_of__description_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8128 = { sizeof (SimpleTutorialNode_tE4B38BD3ACE9EF2381DB70698C62521BD631156F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8128[3] = 
{
	SimpleTutorialNode_tE4B38BD3ACE9EF2381DB70698C62521BD631156F::get_offset_of__description_7(),
	SimpleTutorialNode_tE4B38BD3ACE9EF2381DB70698C62521BD631156F::get_offset_of__tutorialSO_8(),
	SimpleTutorialNode_tE4B38BD3ACE9EF2381DB70698C62521BD631156F::get_offset_of__tutorialModel_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8129 = { sizeof (WinSummaryNode_tA1C713FFA6B2CFC5DC5781097398E1A461EF911F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8129[8] = 
{
	WinSummaryNode_tA1C713FFA6B2CFC5DC5781097398E1A461EF911F::get_offset_of__level_7(),
	WinSummaryNode_tA1C713FFA6B2CFC5DC5781097398E1A461EF911F::get_offset_of__continueBtn_8(),
	WinSummaryNode_tA1C713FFA6B2CFC5DC5781097398E1A461EF911F::get_offset_of__uiMain_9(),
	WinSummaryNode_tA1C713FFA6B2CFC5DC5781097398E1A461EF911F::get_offset_of__container_10(),
	WinSummaryNode_tA1C713FFA6B2CFC5DC5781097398E1A461EF911F::get_offset_of_FxGameObject_11(),
	WinSummaryNode_tA1C713FFA6B2CFC5DC5781097398E1A461EF911F::get_offset_of__soundSystem_12(),
	WinSummaryNode_tA1C713FFA6B2CFC5DC5781097398E1A461EF911F::get_offset_of__soundSO_13(),
	WinSummaryNode_tA1C713FFA6B2CFC5DC5781097398E1A461EF911F::get_offset_of__stars_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8130 = { sizeof (WinSummaryNodeData_tD1C3F590D3B3A7D085A783296AD766C9DDA98DEE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8130[3] = 
{
	WinSummaryNodeData_tD1C3F590D3B3A7D085A783296AD766C9DDA98DEE::get_offset_of_Stars_0(),
	WinSummaryNodeData_tD1C3F590D3B3A7D085A783296AD766C9DDA98DEE::get_offset_of_Score_1(),
	WinSummaryNodeData_tD1C3F590D3B3A7D085A783296AD766C9DDA98DEE::get_offset_of_Level_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8131 = { sizeof (AoeRenderInitSystem_t6F35F1B56581411162C3BAD09E52C51F356EC70C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8131[2] = 
{
	AoeRenderInitSystem_t6F35F1B56581411162C3BAD09E52C51F356EC70C::get_offset_of__container_3(),
	AoeRenderInitSystem_t6F35F1B56581411162C3BAD09E52C51F356EC70C::get_offset_of__context_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8132 = { sizeof (UICalloutBehavior_t565A832FB1E549570845B819FA71B4CA6CA119F4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8132[1] = 
{
	UICalloutBehavior_t565A832FB1E549570845B819FA71B4CA6CA119F4::get_offset_of__description_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8133 = { sizeof (CalloutsComponent_t94CD42511A2D8486E40304BD74431F08BC8C663C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8133[1] = 
{
	CalloutsComponent_t94CD42511A2D8486E40304BD74431F08BC8C663C::get_offset_of_CalloutId_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8134 = { sizeof (DestroyableCalloutsSystem_t466F9C65CEE33AD8D86C8F5EB1ACC16DD7D04145), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8134[6] = 
{
	DestroyableCalloutsSystem_t466F9C65CEE33AD8D86C8F5EB1ACC16DD7D04145::get_offset_of__uiMain_3(),
	DestroyableCalloutsSystem_t466F9C65CEE33AD8D86C8F5EB1ACC16DD7D04145::get_offset_of__tutorialSO_4(),
	DestroyableCalloutsSystem_t466F9C65CEE33AD8D86C8F5EB1ACC16DD7D04145::get_offset_of__context_5(),
	DestroyableCalloutsSystem_t466F9C65CEE33AD8D86C8F5EB1ACC16DD7D04145::get_offset_of__maxCallouts_6(),
	DestroyableCalloutsSystem_t466F9C65CEE33AD8D86C8F5EB1ACC16DD7D04145::get_offset_of__tutorialModel_7(),
	DestroyableCalloutsSystem_t466F9C65CEE33AD8D86C8F5EB1ACC16DD7D04145::get_offset_of__isTriggered_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8135 = { sizeof (TowerCalloutSystem_t7881CCFC9FFF807D37301CB232D05C1096E999E9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8135[6] = 
{
	TowerCalloutSystem_t7881CCFC9FFF807D37301CB232D05C1096E999E9::get_offset_of__uiMain_3(),
	TowerCalloutSystem_t7881CCFC9FFF807D37301CB232D05C1096E999E9::get_offset_of__tutorialSO_4(),
	TowerCalloutSystem_t7881CCFC9FFF807D37301CB232D05C1096E999E9::get_offset_of__context_5(),
	TowerCalloutSystem_t7881CCFC9FFF807D37301CB232D05C1096E999E9::get_offset_of__maxCallouts_6(),
	TowerCalloutSystem_t7881CCFC9FFF807D37301CB232D05C1096E999E9::get_offset_of__tutorialModel_7(),
	TowerCalloutSystem_t7881CCFC9FFF807D37301CB232D05C1096E999E9::get_offset_of__isTriggered_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8136 = { sizeof (TrapCalloutSystem_tE42E39E38FE8532829F1E8409C5E0ED7717076BF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8136[6] = 
{
	TrapCalloutSystem_tE42E39E38FE8532829F1E8409C5E0ED7717076BF::get_offset_of__uiMain_3(),
	TrapCalloutSystem_tE42E39E38FE8532829F1E8409C5E0ED7717076BF::get_offset_of__tutorialSO_4(),
	TrapCalloutSystem_tE42E39E38FE8532829F1E8409C5E0ED7717076BF::get_offset_of__context_5(),
	TrapCalloutSystem_tE42E39E38FE8532829F1E8409C5E0ED7717076BF::get_offset_of__maxCallouts_6(),
	TrapCalloutSystem_tE42E39E38FE8532829F1E8409C5E0ED7717076BF::get_offset_of__tutorialModel_7(),
	TrapCalloutSystem_tE42E39E38FE8532829F1E8409C5E0ED7717076BF::get_offset_of__isTriggered_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8137 = { sizeof (SmoothFollow_t838D162CB27CCC06F9329D5E50959FE5C9EFA5E9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8137[5] = 
{
	SmoothFollow_t838D162CB27CCC06F9329D5E50959FE5C9EFA5E9::get_offset_of_target_4(),
	SmoothFollow_t838D162CB27CCC06F9329D5E50959FE5C9EFA5E9::get_offset_of_distance_5(),
	SmoothFollow_t838D162CB27CCC06F9329D5E50959FE5C9EFA5E9::get_offset_of_height_6(),
	SmoothFollow_t838D162CB27CCC06F9329D5E50959FE5C9EFA5E9::get_offset_of_heightDamping_7(),
	SmoothFollow_t838D162CB27CCC06F9329D5E50959FE5C9EFA5E9::get_offset_of_rotationDamping_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8138 = { sizeof (CartRenderSystem_t7FE03BBE43DB4D1F93882FF37747FB540E310F90), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8138[3] = 
{
	CartRenderSystem_t7FE03BBE43DB4D1F93882FF37747FB540E310F90::get_offset_of__diContainer_3(),
	CartRenderSystem_t7FE03BBE43DB4D1F93882FF37747FB540E310F90::get_offset_of__unitsSO_4(),
	CartRenderSystem_t7FE03BBE43DB4D1F93882FF37747FB540E310F90::get_offset_of__context_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8139 = { sizeof (HeroCollectedRenderSystem_tB14E94CBCCB48058CF44396C6E363D162B804C71), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8139[3] = 
{
	HeroCollectedRenderSystem_tB14E94CBCCB48058CF44396C6E363D162B804C71::get_offset_of__diContainer_3(),
	HeroCollectedRenderSystem_tB14E94CBCCB48058CF44396C6E363D162B804C71::get_offset_of__itemsSO_4(),
	HeroCollectedRenderSystem_tB14E94CBCCB48058CF44396C6E363D162B804C71::get_offset_of__context_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8140 = { sizeof (HeroDropRenderSystem_t5F1458141D724DEB03AE50BD90C8E8C4AE8A55BF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8141 = { sizeof (ViewComponent_t89247AC078C9D4A67D4EE820ED67BBF71F84904A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8141[1] = 
{
	ViewComponent_t89247AC078C9D4A67D4EE820ED67BBF71F84904A::get_offset_of_GameObject_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8142 = { sizeof (AnimatorComponent_t6440D282B095E4FC14E536BAF9D000A85EA6A7E2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8142[1] = 
{
	AnimatorComponent_t6440D282B095E4FC14E536BAF9D000A85EA6A7E2::get_offset_of_Animator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8143 = { sizeof (ILibraryComponent_t7F3699C9BF31B88877F93D66CFB44371CD296735), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8143[1] = 
{
	ILibraryComponent_t7F3699C9BF31B88877F93D66CFB44371CD296735::get_offset_of_Library_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8144 = { sizeof (UIHolderComponent_t77C1FEDC4FF8369CC3E81B337440D5561A64857C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8144[1] = 
{
	UIHolderComponent_t77C1FEDC4FF8369CC3E81B337440D5561A64857C::get_offset_of_GameObject_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8145 = { sizeof (DecoyRenderSystem_t5D143A72EC07CF0F462EB56C928617A3EB725C58), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8145[3] = 
{
	DecoyRenderSystem_t5D143A72EC07CF0F462EB56C928617A3EB725C58::get_offset_of__diContainer_3(),
	DecoyRenderSystem_t5D143A72EC07CF0F462EB56C928617A3EB725C58::get_offset_of__unitsSO_4(),
	DecoyRenderSystem_t5D143A72EC07CF0F462EB56C928617A3EB725C58::get_offset_of__context_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8146 = { sizeof (RemoveViewOnDisposeComponent_tE54A9A8E3DB4158A7D2E649412FEACD20965E449), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8147 = { sizeof (AddGameObjectEffectRenderComponent_t837ECF100B0B3C3D326C867C4185C585ADB19DE1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8147[2] = 
{
	AddGameObjectEffectRenderComponent_t837ECF100B0B3C3D326C867C4185C585ADB19DE1::get_offset_of_TargetId_0(),
	AddGameObjectEffectRenderComponent_t837ECF100B0B3C3D326C867C4185C585ADB19DE1::get_offset_of_EffectVisual_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8148 = { sizeof (EffectRendrerComponent_t4BCB77D9E1E31E2B6534FD49EC7FAF8AB1ADC28F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8148[3] = 
{
	EffectRendrerComponent_t4BCB77D9E1E31E2B6534FD49EC7FAF8AB1ADC28F::get_offset_of_VisualId_0(),
	EffectRendrerComponent_t4BCB77D9E1E31E2B6534FD49EC7FAF8AB1ADC28F::get_offset_of_TargetId_1(),
	EffectRendrerComponent_t4BCB77D9E1E31E2B6534FD49EC7FAF8AB1ADC28F::get_offset_of_EffectVisualType_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8149 = { sizeof (EffectRenderEntityIndex_tF5D57B2C69C74DA04F3510E5A0C2B6D9AF5211F6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8150 = { sizeof (U3CU3Ec_tC4E3FD3778FF243A0B879DA96D098918C6DB772C), -1, sizeof(U3CU3Ec_tC4E3FD3778FF243A0B879DA96D098918C6DB772C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8150[2] = 
{
	U3CU3Ec_tC4E3FD3778FF243A0B879DA96D098918C6DB772C_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tC4E3FD3778FF243A0B879DA96D098918C6DB772C_StaticFields::get_offset_of_U3CU3E9__0_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8151 = { sizeof (EffectMainRendrerSystem_tB3D6F88868897C742DF7EC21A730E9117FE70FBE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8151[3] = 
{
	EffectMainRendrerSystem_tB3D6F88868897C742DF7EC21A730E9117FE70FBE::get_offset_of__unitsSO_3(),
	EffectMainRendrerSystem_tB3D6F88868897C742DF7EC21A730E9117FE70FBE::get_offset_of__effectVisualSO_4(),
	EffectMainRendrerSystem_tB3D6F88868897C742DF7EC21A730E9117FE70FBE::get_offset_of__context_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8152 = { sizeof (AddGameObjectEffectRenderSystem_tBEC7DF7EC3C4CF0D8E90A1DC9884ECA5745A0139), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8152[4] = 
{
	AddGameObjectEffectRenderSystem_tBEC7DF7EC3C4CF0D8E90A1DC9884ECA5745A0139::get_offset_of__unitsSO_3(),
	AddGameObjectEffectRenderSystem_tBEC7DF7EC3C4CF0D8E90A1DC9884ECA5745A0139::get_offset_of__effectVisualSO_4(),
	AddGameObjectEffectRenderSystem_tBEC7DF7EC3C4CF0D8E90A1DC9884ECA5745A0139::get_offset_of__diContainer_5(),
	AddGameObjectEffectRenderSystem_tBEC7DF7EC3C4CF0D8E90A1DC9884ECA5745A0139::get_offset_of__context_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8153 = { sizeof (EffectRenderDisposeSystem_t1DBF43E154539554F2E37A3D36798D627D0BBD4D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8153[4] = 
{
	EffectRenderDisposeSystem_t1DBF43E154539554F2E37A3D36798D627D0BBD4D::get_offset_of__unitsSO_3(),
	EffectRenderDisposeSystem_t1DBF43E154539554F2E37A3D36798D627D0BBD4D::get_offset_of__effectVisualSO_4(),
	EffectRenderDisposeSystem_t1DBF43E154539554F2E37A3D36798D627D0BBD4D::get_offset_of__diContainer_5(),
	EffectRenderDisposeSystem_t1DBF43E154539554F2E37A3D36798D627D0BBD4D::get_offset_of__context_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8154 = { sizeof (HpUIBehavior_tAFD1C3F31638942C35946D970A36E72F44595CF7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8154[2] = 
{
	HpUIBehavior_tAFD1C3F31638942C35946D970A36E72F44595CF7::get_offset_of__progress_4(),
	HpUIBehavior_tAFD1C3F31638942C35946D970A36E72F44595CF7::get_offset_of__hpText_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8155 = { sizeof (HeroHealthRenderSystem_t75C28A2A739FC9E3EC68C36C6C80C91A8241F960), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8155[4] = 
{
	HeroHealthRenderSystem_t75C28A2A739FC9E3EC68C36C6C80C91A8241F960::get_offset_of__uiMain_3(),
	HeroHealthRenderSystem_t75C28A2A739FC9E3EC68C36C6C80C91A8241F960::get_offset_of__soundSO_4(),
	HeroHealthRenderSystem_t75C28A2A739FC9E3EC68C36C6C80C91A8241F960::get_offset_of__lastTimeSound_5(),
	HeroHealthRenderSystem_t75C28A2A739FC9E3EC68C36C6C80C91A8241F960::get_offset_of__context_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8156 = { sizeof (HeroAnimationsSystem_t230B10CB4AE99718FFE8A2E896D800ADDF833422), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8156[1] = 
{
	HeroAnimationsSystem_t230B10CB4AE99718FFE8A2E896D800ADDF833422::get_offset_of__context_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8157 = { sizeof (HeroMovementRenderSystem_tD6481D723FF68517C1FB9AE3A13CABA583766BA6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8157[1] = 
{
	HeroMovementRenderSystem_tD6481D723FF68517C1FB9AE3A13CABA583766BA6::get_offset_of_EPSILON_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8158 = { sizeof (HeroInitRenderSystem_t2635936F42F6041B5A244AE4A7E0C9FBC8165AB3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8158[3] = 
{
	HeroInitRenderSystem_t2635936F42F6041B5A244AE4A7E0C9FBC8165AB3::get_offset_of__diContainer_3(),
	HeroInitRenderSystem_t2635936F42F6041B5A244AE4A7E0C9FBC8165AB3::get_offset_of__unitsConf_4(),
	HeroInitRenderSystem_t2635936F42F6041B5A244AE4A7E0C9FBC8165AB3::get_offset_of__context_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8159 = { sizeof (BattleInventoryComponent_tB8F3758E6C052D9F512425DEB3F4C3050833797A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8159[2] = 
{
	BattleInventoryComponent_tB8F3758E6C052D9F512425DEB3F4C3050833797A::get_offset_of__list_4(),
	BattleInventoryComponent_tB8F3758E6C052D9F512425DEB3F4C3050833797A::get_offset_of__inventorySize_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8160 = { sizeof (BattleInventoryListItem_t1D6B5F84E1B80F45276049DEC8765BF471392876), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8160[3] = 
{
	BattleInventoryListItem_t1D6B5F84E1B80F45276049DEC8765BF471392876::get_offset_of__icon_4(),
	BattleInventoryListItem_t1D6B5F84E1B80F45276049DEC8765BF471392876::get_offset_of__container_5(),
	BattleInventoryListItem_t1D6B5F84E1B80F45276049DEC8765BF471392876::get_offset_of__itemsConf_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8161 = { sizeof (InventoryRenderInitSystem_t55EBC35BDB6E9B4EFFB774E72CF32A31026E9438), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8161[1] = 
{
	InventoryRenderInitSystem_t55EBC35BDB6E9B4EFFB774E72CF32A31026E9438::get_offset_of__context_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8162 = { sizeof (InventoryRenderSystem_tC845B5274539DEBA6E8834FCA4E8D03E6A79060C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8162[3] = 
{
	InventoryRenderSystem_tC845B5274539DEBA6E8834FCA4E8D03E6A79060C::get_offset_of__gameSettings_0(),
	InventoryRenderSystem_tC845B5274539DEBA6E8834FCA4E8D03E6A79060C::get_offset_of__entities_1(),
	InventoryRenderSystem_tC845B5274539DEBA6E8834FCA4E8D03E6A79060C::get_offset_of__context_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8163 = { sizeof (InventoryFreeRenderSystem_tAF4F619609C6ED69141732FF5E377F0201132129), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8163[1] = 
{
	InventoryFreeRenderSystem_tAF4F619609C6ED69141732FF5E377F0201132129::get_offset_of__context_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8164 = { sizeof (BattleKickerWithIconComponent_t9744346E9F295A455BBD6E49E1041E3EF7F28107), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8164[2] = 
{
	BattleKickerWithIconComponent_t9744346E9F295A455BBD6E49E1041E3EF7F28107::get_offset_of_ItemId_0(),
	BattleKickerWithIconComponent_t9744346E9F295A455BBD6E49E1041E3EF7F28107::get_offset_of_Position_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8165 = { sizeof (CollectableKickerWithIconSystem_tF454A93E6EE83153ABA0A04C9B9F0B8D5CAFF70C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8165[3] = 
{
	CollectableKickerWithIconSystem_tF454A93E6EE83153ABA0A04C9B9F0B8D5CAFF70C::get_offset_of__uiMain_3(),
	CollectableKickerWithIconSystem_tF454A93E6EE83153ABA0A04C9B9F0B8D5CAFF70C::get_offset_of__itemsSO_4(),
	CollectableKickerWithIconSystem_tF454A93E6EE83153ABA0A04C9B9F0B8D5CAFF70C::get_offset_of__context_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8166 = { sizeof (DestroyableKickerWithIconSystem_t801EFA2C6EB88A29A1678565BB8E03BB23A8370B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8166[4] = 
{
	DestroyableKickerWithIconSystem_t801EFA2C6EB88A29A1678565BB8E03BB23A8370B::get_offset_of__uiMain_3(),
	DestroyableKickerWithIconSystem_t801EFA2C6EB88A29A1678565BB8E03BB23A8370B::get_offset_of__battleSettingsSO_4(),
	DestroyableKickerWithIconSystem_t801EFA2C6EB88A29A1678565BB8E03BB23A8370B::get_offset_of__itemsSO_5(),
	DestroyableKickerWithIconSystem_t801EFA2C6EB88A29A1678565BB8E03BB23A8370B::get_offset_of__context_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8167 = { sizeof (TowerKickerWithIconSystem_t617FA57E1A9226152864A1AD4ED6CF1C059D8A09), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8167[3] = 
{
	TowerKickerWithIconSystem_t617FA57E1A9226152864A1AD4ED6CF1C059D8A09::get_offset_of__towersSO_3(),
	TowerKickerWithIconSystem_t617FA57E1A9226152864A1AD4ED6CF1C059D8A09::get_offset_of__uiMain_4(),
	TowerKickerWithIconSystem_t617FA57E1A9226152864A1AD4ED6CF1C059D8A09::get_offset_of__context_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8168 = { sizeof (LevelComponent_t682483027DD67BDF5D6912C2BB9D1F0469A2C644), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8168[1] = 
{
	LevelComponent_t682483027DD67BDF5D6912C2BB9D1F0469A2C644::get_offset_of_LevelModel_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8169 = { sizeof (EnvironmentRenderSystem_tD87EA9C079CC43C3E28B6A225C0794A7CA1F72C8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8169[3] = 
{
	EnvironmentRenderSystem_tD87EA9C079CC43C3E28B6A225C0794A7CA1F72C8::get_offset_of__container_0(),
	EnvironmentRenderSystem_tD87EA9C079CC43C3E28B6A225C0794A7CA1F72C8::get_offset_of__context_1(),
	EnvironmentRenderSystem_tD87EA9C079CC43C3E28B6A225C0794A7CA1F72C8::get_offset_of__environment_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8170 = { sizeof (ObjectiveCountRenderSystem_t198210908B26FB1A8EDC8A9DB88852CF45D99DFB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8170[1] = 
{
	ObjectiveCountRenderSystem_t198210908B26FB1A8EDC8A9DB88852CF45D99DFB::get_offset_of__context_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8171 = { sizeof (ObjectivesRenderInitSystem_t27E23B7A0DE0B91D340E261A6988849FCC35191A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8171[3] = 
{
	ObjectivesRenderInitSystem_t27E23B7A0DE0B91D340E261A6988849FCC35191A::get_offset_of__uiMain_3(),
	ObjectivesRenderInitSystem_t27E23B7A0DE0B91D340E261A6988849FCC35191A::get_offset_of__unitsConf_4(),
	ObjectivesRenderInitSystem_t27E23B7A0DE0B91D340E261A6988849FCC35191A::get_offset_of__context_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8172 = { sizeof (ProjectileRenderInitSystem_t9630DFE1C78BF96242B55D595071AD21EF3A4F8D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8172[2] = 
{
	ProjectileRenderInitSystem_t9630DFE1C78BF96242B55D595071AD21EF3A4F8D::get_offset_of__container_3(),
	ProjectileRenderInitSystem_t9630DFE1C78BF96242B55D595071AD21EF3A4F8D::get_offset_of__context_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8173 = { sizeof (ProjectileRenderMovementSystem_tFF1FDD22E235DF6570503388DD67D1A77E6600E9), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8174 = { sizeof (RenderFeature_tD8D02B6D1FC897C71185766227AB45DC1E8B7D12), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8174[1] = 
{
	RenderFeature_tD8D02B6D1FC897C71185766227AB45DC1E8B7D12::get_offset_of__featuresSwitchConfig_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8175 = { sizeof (SoundComponent_t19B56A885393FE7EC0BFF4A9AEF609C240983D7C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8175[1] = 
{
	SoundComponent_t19B56A885393FE7EC0BFF4A9AEF609C240983D7C::get_offset_of_ClipName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8176 = { sizeof (FXSoundComponent_t67B072AC87CD6EDDE23219727D01789C036D6600), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8177 = { sizeof (MusicSoundComponent_tB975365C7BD5C382B6D1685AA770668ED79096C4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8178 = { sizeof (BattleSoundSystem_tE8CE33A9D5C192A05CE7218DEB71EA1D04647083), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8178[5] = 
{
	BattleSoundSystem_tE8CE33A9D5C192A05CE7218DEB71EA1D04647083::get_offset_of__uiMain_3(),
	BattleSoundSystem_tE8CE33A9D5C192A05CE7218DEB71EA1D04647083::get_offset_of__diContainer_4(),
	BattleSoundSystem_tE8CE33A9D5C192A05CE7218DEB71EA1D04647083::get_offset_of__towersSO_5(),
	BattleSoundSystem_tE8CE33A9D5C192A05CE7218DEB71EA1D04647083::get_offset_of__soundSystem_6(),
	BattleSoundSystem_tE8CE33A9D5C192A05CE7218DEB71EA1D04647083::get_offset_of__context_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8179 = { sizeof (AnimatorTriggerSpellRendrerSystem_t8FFF012295B09B58F09DDF13BEB99791FF234160), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8179[1] = 
{
	AnimatorTriggerSpellRendrerSystem_t8FFF012295B09B58F09DDF13BEB99791FF234160::get_offset_of__context_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8180 = { sizeof (CameraShake_t5EBAAD00C215BE104B246C1E17C22CDDEA0F3F7E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8180[9] = 
{
	CameraShake_t5EBAAD00C215BE104B246C1E17C22CDDEA0F3F7E::get_offset_of_debugMode_4(),
	CameraShake_t5EBAAD00C215BE104B246C1E17C22CDDEA0F3F7E::get_offset_of_shakeAmount_5(),
	CameraShake_t5EBAAD00C215BE104B246C1E17C22CDDEA0F3F7E::get_offset_of_shakeDuration_6(),
	CameraShake_t5EBAAD00C215BE104B246C1E17C22CDDEA0F3F7E::get_offset_of_shakePercentage_7(),
	CameraShake_t5EBAAD00C215BE104B246C1E17C22CDDEA0F3F7E::get_offset_of_startAmount_8(),
	CameraShake_t5EBAAD00C215BE104B246C1E17C22CDDEA0F3F7E::get_offset_of_startDuration_9(),
	CameraShake_t5EBAAD00C215BE104B246C1E17C22CDDEA0F3F7E::get_offset_of_isRunning_10(),
	CameraShake_t5EBAAD00C215BE104B246C1E17C22CDDEA0F3F7E::get_offset_of_smooth_11(),
	CameraShake_t5EBAAD00C215BE104B246C1E17C22CDDEA0F3F7E::get_offset_of_smoothAmount_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8181 = { sizeof (U3CShakeU3Ed__12_tB0AF5B6AE8BF2353E9DFDB29307648337003BF25), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8181[4] = 
{
	U3CShakeU3Ed__12_tB0AF5B6AE8BF2353E9DFDB29307648337003BF25::get_offset_of_U3CU3E1__state_0(),
	U3CShakeU3Ed__12_tB0AF5B6AE8BF2353E9DFDB29307648337003BF25::get_offset_of_U3CU3E2__current_1(),
	U3CShakeU3Ed__12_tB0AF5B6AE8BF2353E9DFDB29307648337003BF25::get_offset_of_U3CU3E4__this_2(),
	U3CShakeU3Ed__12_tB0AF5B6AE8BF2353E9DFDB29307648337003BF25::get_offset_of_delay_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8182 = { sizeof (CameraShakeComponent_tA29F67821BA0742287EBFC5C4CC853E4C8F533EC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8182[3] = 
{
	CameraShakeComponent_tA29F67821BA0742287EBFC5C4CC853E4C8F533EC::get_offset_of_ShakeAmount_0(),
	CameraShakeComponent_tA29F67821BA0742287EBFC5C4CC853E4C8F533EC::get_offset_of_ShakeDuration_1(),
	CameraShakeComponent_tA29F67821BA0742287EBFC5C4CC853E4C8F533EC::get_offset_of_Delay_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8183 = { sizeof (CameraShakeSystem_tE06433F5F18F1EB0DD7FA6624804DCA50C0DFFF4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8183[1] = 
{
	CameraShakeSystem_tE06433F5F18F1EB0DD7FA6624804DCA50C0DFFF4::get_offset_of__context_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8184 = { sizeof (NormaleBoosterStateSystem_tA39F70EDC43638B5B1CC88CF21907414D39B7125), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8184[1] = 
{
	NormaleBoosterStateSystem_tA39F70EDC43638B5B1CC88CF21907414D39B7125::get_offset_of__context_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8185 = { sizeof (MegaBoosterStateSystem_t492C6A7E5409E4D8A82A30D117626EFE4D14B572), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8185[1] = 
{
	MegaBoosterStateSystem_t492C6A7E5409E4D8A82A30D117626EFE4D14B572::get_offset_of__context_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8186 = { sizeof (SpellUIStatusSystem_t6BB6D73D1782996C162FBDD5FBD8FC82E551A7AA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8186[5] = 
{
	SpellUIStatusSystem_t6BB6D73D1782996C162FBDD5FBD8FC82E551A7AA::get_offset_of__spellsSO_3(),
	SpellUIStatusSystem_t6BB6D73D1782996C162FBDD5FBD8FC82E551A7AA::get_offset_of__unitsSO_4(),
	SpellUIStatusSystem_t6BB6D73D1782996C162FBDD5FBD8FC82E551A7AA::get_offset_of__inventorySystem_5(),
	SpellUIStatusSystem_t6BB6D73D1782996C162FBDD5FBD8FC82E551A7AA::get_offset_of__entities_6(),
	SpellUIStatusSystem_t6BB6D73D1782996C162FBDD5FBD8FC82E551A7AA::get_offset_of__context_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8187 = { sizeof (SpellUIUnlockSystem_t9F96E0D5E6B58576448EA8C6EB4E6950AC7B746D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8187[3] = 
{
	SpellUIUnlockSystem_t9F96E0D5E6B58576448EA8C6EB4E6950AC7B746D::get_offset_of__uiMain_3(),
	SpellUIUnlockSystem_t9F96E0D5E6B58576448EA8C6EB4E6950AC7B746D::get_offset_of__unitsConf_4(),
	SpellUIUnlockSystem_t9F96E0D5E6B58576448EA8C6EB4E6950AC7B746D::get_offset_of__context_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8188 = { sizeof (TowerRenderComponent_t86F5EE9B65D02400E2F4532577BD5416D86FC19E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8188[2] = 
{
	TowerRenderComponent_t86F5EE9B65D02400E2F4532577BD5416D86FC19E::get_offset_of_Idle_0(),
	TowerRenderComponent_t86F5EE9B65D02400E2F4532577BD5416D86FC19E::get_offset_of_Destroyed_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8189 = { sizeof (TowerHPBarComponent_t40985B862AAE62ABB3F2C686D354A7BD548552E1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8189[2] = 
{
	TowerHPBarComponent_t40985B862AAE62ABB3F2C686D354A7BD548552E1::get_offset_of_HPBar_0(),
	TowerHPBarComponent_t40985B862AAE62ABB3F2C686D354A7BD548552E1::get_offset_of_ProgressBar_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8190 = { sizeof (TowerTopComponent_tF55EF7BFBA8765EE56500584F133C6ACC3119A82), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8190[1] = 
{
	TowerTopComponent_tF55EF7BFBA8765EE56500584F133C6ACC3119A82::get_offset_of_TowerTop_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8191 = { sizeof (TowerHealingRenderSystem_t55266A2DC95393BF6E1E9E867D7E1E9A64CC23E4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8191[2] = 
{
	TowerHealingRenderSystem_t55266A2DC95393BF6E1E9E867D7E1E9A64CC23E4::get_offset_of__context_0(),
	TowerHealingRenderSystem_t55266A2DC95393BF6E1E9E867D7E1E9A64CC23E4::get_offset_of__entities_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8192 = { sizeof (TowerRenderSystem_t2323ECB7EB8589584194DE05BCACF223FB864FD4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8192[4] = 
{
	TowerRenderSystem_t2323ECB7EB8589584194DE05BCACF223FB864FD4::get_offset_of__uiMain_3(),
	TowerRenderSystem_t2323ECB7EB8589584194DE05BCACF223FB864FD4::get_offset_of__diContainer_4(),
	TowerRenderSystem_t2323ECB7EB8589584194DE05BCACF223FB864FD4::get_offset_of__towersSO_5(),
	TowerRenderSystem_t2323ECB7EB8589584194DE05BCACF223FB864FD4::get_offset_of__context_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8193 = { sizeof (TowerRotationSystem_t4A572C980B80BBB1BF97336C685C7A5213C27102), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8193[2] = 
{
	TowerRotationSystem_t4A572C980B80BBB1BF97336C685C7A5213C27102::get_offset_of__context_0(),
	TowerRotationSystem_t4A572C980B80BBB1BF97336C685C7A5213C27102::get_offset_of__entities_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8194 = { sizeof (TowerStateRenderSystem_tB2EFC75A920AE3308B098F8A0FB0D0FDD6DDCC15), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8194[3] = 
{
	TowerStateRenderSystem_tB2EFC75A920AE3308B098F8A0FB0D0FDD6DDCC15::get_offset_of__towersSO_3(),
	TowerStateRenderSystem_tB2EFC75A920AE3308B098F8A0FB0D0FDD6DDCC15::get_offset_of__sfxLibrary_4(),
	TowerStateRenderSystem_tB2EFC75A920AE3308B098F8A0FB0D0FDD6DDCC15::get_offset_of__context_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8195 = { sizeof (TrapRenderSystem_tADC9898D19EB0C4753108E84CC071C378D4866AD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8195[3] = 
{
	TrapRenderSystem_tADC9898D19EB0C4753108E84CC071C378D4866AD::get_offset_of__diContainer_3(),
	TrapRenderSystem_tADC9898D19EB0C4753108E84CC071C378D4866AD::get_offset_of__trapsSO_4(),
	TrapRenderSystem_tADC9898D19EB0C4753108E84CC071C378D4866AD::get_offset_of__context_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8196 = { sizeof (BattleNodesTutorialSystem_t739310FF08D20FAC4C461216CF6CB25B7F7960AF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8196[4] = 
{
	BattleNodesTutorialSystem_t739310FF08D20FAC4C461216CF6CB25B7F7960AF::get_offset_of__uiMain_3(),
	BattleNodesTutorialSystem_t739310FF08D20FAC4C461216CF6CB25B7F7960AF::get_offset_of__tutorialSO_4(),
	BattleNodesTutorialSystem_t739310FF08D20FAC4C461216CF6CB25B7F7960AF::get_offset_of__container_5(),
	BattleNodesTutorialSystem_t739310FF08D20FAC4C461216CF6CB25B7F7960AF::get_offset_of__context_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8197 = { sizeof (LeftHandUIInitSystem_t87D41FBCE6AE359217AD3C75DE1EFF735B0EDC26), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8197[1] = 
{
	LeftHandUIInitSystem_t87D41FBCE6AE359217AD3C75DE1EFF735B0EDC26::get_offset_of__context_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8198 = { sizeof (TimePressureRenderSystem_t3B24E02FD278B6407D9DA3F5FD176151B80979F5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8198[1] = 
{
	TimePressureRenderSystem_t3B24E02FD278B6407D9DA3F5FD176151B80979F5::get_offset_of__context_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8199 = { sizeof (CollectableRenderSystem_t0AF772D5F4376446D0485B0990A7D88D61A021C3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8199[3] = 
{
	CollectableRenderSystem_t0AF772D5F4376446D0485B0990A7D88D61A021C3::get_offset_of__diContainer_3(),
	CollectableRenderSystem_t0AF772D5F4376446D0485B0990A7D88D61A021C3::get_offset_of__itemsSO_4(),
	CollectableRenderSystem_t0AF772D5F4376446D0485B0990A7D88D61A021C3::get_offset_of__context_5(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
