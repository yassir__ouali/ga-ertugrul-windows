﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Facebook.Unity.Example.GraphRequest
struct GraphRequest_t81F9EA1312B20AB3FDA2B9BE84C03A026BBDC347;
// GoogleMobileAds.Api.CustomNativeTemplateAd
struct CustomNativeTemplateAd_tA73CD30978AE046F6E81DA0DE0D90E0260870D94;
// GoogleMobileAds.Common.IAdLoaderClient
struct IAdLoaderClient_t84589A1B80A9DBB407D5D4C4C1EB6DAEAC491DDA;
// GoogleMobileAds.Common.IBannerClient
struct IBannerClient_tCD22951E2FDD99321BCAA4A48B964518BBFEA044;
// GoogleMobileAds.Common.ICustomNativeTemplateClient
struct ICustomNativeTemplateClient_tDE3FDB31089215F5F16EEBA903FA8E0EB9072B1D;
// GoogleMobileAds.Common.IInterstitialClient
struct IInterstitialClient_tC06AD2227C7A7C2AB65FC63D956ED4B0A925D412;
// GoogleMobileAds.Common.IMobileAdsClient
struct IMobileAdsClient_t312ABFC24685432B8519D5AB4114909B65828884;
// GoogleMobileAds.Common.IRewardBasedVideoAdClient
struct IRewardBasedVideoAdClient_tE13DAC556DD16DB599ADC16C58B10889C179AB01;
// GoogleMobileAds.Common.IRewardedAdClient
struct IRewardedAdClient_t3EF74F491E5306CF8BCB4FE14CE1D0E532CCAFBD;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.String,System.Action`2<GoogleMobileAds.Api.CustomNativeTemplateAd,System.String>>
struct Dictionary_2_t8B8E8A2A647CF36A292C75063777A26CA5CB41B3;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC;
// System.Collections.Generic.HashSet`1<GoogleMobileAds.Api.NativeAdType>
struct HashSet_1_t67D33D99B1AE0A542D9C13CBD2729E6E18466F8E;
// System.Collections.Generic.HashSet`1<System.String>
struct HashSet_1_tAF31CD0D5B1F27E354D2DA33F8CF07F490E8C98A;
// System.Collections.Generic.IList`1<System.String>
struct IList_1_tFFB5515FC97391D04D5034ED7F334357FED1FAE6;
// System.Collections.Generic.List`1<GoogleMobileAds.Api.Mediation.MediationExtras>
struct List_1_t31A991A8A6D89FB1F0200E8925EADDC20ABC5811;
// System.Collections.Generic.List`1<System.Action>
struct List_1_t9E7F53A878191B703A76E69CE36AA90A372800E7;
// System.Collections.Generic.List`1<System.String>
struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3;
// System.Collections.Generic.Stack`1<System.String>
struct Stack_1_t13ADD3EDE9022020A434D32784D651F9FE027067;
// System.EventHandler`1<GoogleMobileAds.Api.AdErrorEventArgs>
struct EventHandler_1_t6770E1CD9311E89A0B3CC72E0247509E692D0984;
// System.EventHandler`1<GoogleMobileAds.Api.AdFailedToLoadEventArgs>
struct EventHandler_1_t5D4F18F6AB51C53CC8C112AFAADAEE92FB1C1A41;
// System.EventHandler`1<GoogleMobileAds.Api.CustomNativeEventArgs>
struct EventHandler_1_t511DB271A3221C99AE96D46A4B804F2417E87DE3;
// System.EventHandler`1<GoogleMobileAds.Api.Reward>
struct EventHandler_1_t0B0C84881A5FE7FACD54CC9982D03AE5DC1AA168;
// System.EventHandler`1<System.EventArgs>
struct EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.GUIStyle
struct GUIStyle_t671F175A201A19166385EE3392292A5F50070572;
// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CTAKESCREENSHOTU3ED__4_T32FCC3EB0A36F0973EE1E62E3E835E9133D1C105_H
#define U3CTAKESCREENSHOTU3ED__4_T32FCC3EB0A36F0973EE1E62E3E835E9133D1C105_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Example.GraphRequest_<TakeScreenshot>d__4
struct  U3CTakeScreenshotU3Ed__4_t32FCC3EB0A36F0973EE1E62E3E835E9133D1C105  : public RuntimeObject
{
public:
	// System.Int32 Facebook.Unity.Example.GraphRequest_<TakeScreenshot>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Facebook.Unity.Example.GraphRequest_<TakeScreenshot>d__4::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Facebook.Unity.Example.GraphRequest Facebook.Unity.Example.GraphRequest_<TakeScreenshot>d__4::<>4__this
	GraphRequest_t81F9EA1312B20AB3FDA2B9BE84C03A026BBDC347 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CTakeScreenshotU3Ed__4_t32FCC3EB0A36F0973EE1E62E3E835E9133D1C105, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CTakeScreenshotU3Ed__4_t32FCC3EB0A36F0973EE1E62E3E835E9133D1C105, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CTakeScreenshotU3Ed__4_t32FCC3EB0A36F0973EE1E62E3E835E9133D1C105, ___U3CU3E4__this_2)); }
	inline GraphRequest_t81F9EA1312B20AB3FDA2B9BE84C03A026BBDC347 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline GraphRequest_t81F9EA1312B20AB3FDA2B9BE84C03A026BBDC347 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(GraphRequest_t81F9EA1312B20AB3FDA2B9BE84C03A026BBDC347 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTAKESCREENSHOTU3ED__4_T32FCC3EB0A36F0973EE1E62E3E835E9133D1C105_H
#ifndef ADLOADER_TE5D111966C04D585B1DCE3FAA3F0583CA7A3588A_H
#define ADLOADER_TE5D111966C04D585B1DCE3FAA3F0583CA7A3588A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.Api.AdLoader
struct  AdLoader_tE5D111966C04D585B1DCE3FAA3F0583CA7A3588A  : public RuntimeObject
{
public:
	// GoogleMobileAds.Common.IAdLoaderClient GoogleMobileAds.Api.AdLoader::adLoaderClient
	RuntimeObject* ___adLoaderClient_0;
	// System.EventHandler`1<GoogleMobileAds.Api.AdFailedToLoadEventArgs> GoogleMobileAds.Api.AdLoader::OnAdFailedToLoad
	EventHandler_1_t5D4F18F6AB51C53CC8C112AFAADAEE92FB1C1A41 * ___OnAdFailedToLoad_1;
	// System.EventHandler`1<GoogleMobileAds.Api.CustomNativeEventArgs> GoogleMobileAds.Api.AdLoader::OnCustomNativeTemplateAdLoaded
	EventHandler_1_t511DB271A3221C99AE96D46A4B804F2417E87DE3 * ___OnCustomNativeTemplateAdLoaded_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.Action`2<GoogleMobileAds.Api.CustomNativeTemplateAd,System.String>> GoogleMobileAds.Api.AdLoader::<CustomNativeTemplateClickHandlers>k__BackingField
	Dictionary_2_t8B8E8A2A647CF36A292C75063777A26CA5CB41B3 * ___U3CCustomNativeTemplateClickHandlersU3Ek__BackingField_3;
	// System.String GoogleMobileAds.Api.AdLoader::<AdUnitId>k__BackingField
	String_t* ___U3CAdUnitIdU3Ek__BackingField_4;
	// System.Collections.Generic.HashSet`1<GoogleMobileAds.Api.NativeAdType> GoogleMobileAds.Api.AdLoader::<AdTypes>k__BackingField
	HashSet_1_t67D33D99B1AE0A542D9C13CBD2729E6E18466F8E * ___U3CAdTypesU3Ek__BackingField_5;
	// System.Collections.Generic.HashSet`1<System.String> GoogleMobileAds.Api.AdLoader::<TemplateIds>k__BackingField
	HashSet_1_tAF31CD0D5B1F27E354D2DA33F8CF07F490E8C98A * ___U3CTemplateIdsU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_adLoaderClient_0() { return static_cast<int32_t>(offsetof(AdLoader_tE5D111966C04D585B1DCE3FAA3F0583CA7A3588A, ___adLoaderClient_0)); }
	inline RuntimeObject* get_adLoaderClient_0() const { return ___adLoaderClient_0; }
	inline RuntimeObject** get_address_of_adLoaderClient_0() { return &___adLoaderClient_0; }
	inline void set_adLoaderClient_0(RuntimeObject* value)
	{
		___adLoaderClient_0 = value;
		Il2CppCodeGenWriteBarrier((&___adLoaderClient_0), value);
	}

	inline static int32_t get_offset_of_OnAdFailedToLoad_1() { return static_cast<int32_t>(offsetof(AdLoader_tE5D111966C04D585B1DCE3FAA3F0583CA7A3588A, ___OnAdFailedToLoad_1)); }
	inline EventHandler_1_t5D4F18F6AB51C53CC8C112AFAADAEE92FB1C1A41 * get_OnAdFailedToLoad_1() const { return ___OnAdFailedToLoad_1; }
	inline EventHandler_1_t5D4F18F6AB51C53CC8C112AFAADAEE92FB1C1A41 ** get_address_of_OnAdFailedToLoad_1() { return &___OnAdFailedToLoad_1; }
	inline void set_OnAdFailedToLoad_1(EventHandler_1_t5D4F18F6AB51C53CC8C112AFAADAEE92FB1C1A41 * value)
	{
		___OnAdFailedToLoad_1 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdFailedToLoad_1), value);
	}

	inline static int32_t get_offset_of_OnCustomNativeTemplateAdLoaded_2() { return static_cast<int32_t>(offsetof(AdLoader_tE5D111966C04D585B1DCE3FAA3F0583CA7A3588A, ___OnCustomNativeTemplateAdLoaded_2)); }
	inline EventHandler_1_t511DB271A3221C99AE96D46A4B804F2417E87DE3 * get_OnCustomNativeTemplateAdLoaded_2() const { return ___OnCustomNativeTemplateAdLoaded_2; }
	inline EventHandler_1_t511DB271A3221C99AE96D46A4B804F2417E87DE3 ** get_address_of_OnCustomNativeTemplateAdLoaded_2() { return &___OnCustomNativeTemplateAdLoaded_2; }
	inline void set_OnCustomNativeTemplateAdLoaded_2(EventHandler_1_t511DB271A3221C99AE96D46A4B804F2417E87DE3 * value)
	{
		___OnCustomNativeTemplateAdLoaded_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnCustomNativeTemplateAdLoaded_2), value);
	}

	inline static int32_t get_offset_of_U3CCustomNativeTemplateClickHandlersU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AdLoader_tE5D111966C04D585B1DCE3FAA3F0583CA7A3588A, ___U3CCustomNativeTemplateClickHandlersU3Ek__BackingField_3)); }
	inline Dictionary_2_t8B8E8A2A647CF36A292C75063777A26CA5CB41B3 * get_U3CCustomNativeTemplateClickHandlersU3Ek__BackingField_3() const { return ___U3CCustomNativeTemplateClickHandlersU3Ek__BackingField_3; }
	inline Dictionary_2_t8B8E8A2A647CF36A292C75063777A26CA5CB41B3 ** get_address_of_U3CCustomNativeTemplateClickHandlersU3Ek__BackingField_3() { return &___U3CCustomNativeTemplateClickHandlersU3Ek__BackingField_3; }
	inline void set_U3CCustomNativeTemplateClickHandlersU3Ek__BackingField_3(Dictionary_2_t8B8E8A2A647CF36A292C75063777A26CA5CB41B3 * value)
	{
		___U3CCustomNativeTemplateClickHandlersU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCustomNativeTemplateClickHandlersU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CAdUnitIdU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(AdLoader_tE5D111966C04D585B1DCE3FAA3F0583CA7A3588A, ___U3CAdUnitIdU3Ek__BackingField_4)); }
	inline String_t* get_U3CAdUnitIdU3Ek__BackingField_4() const { return ___U3CAdUnitIdU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CAdUnitIdU3Ek__BackingField_4() { return &___U3CAdUnitIdU3Ek__BackingField_4; }
	inline void set_U3CAdUnitIdU3Ek__BackingField_4(String_t* value)
	{
		___U3CAdUnitIdU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAdUnitIdU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CAdTypesU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(AdLoader_tE5D111966C04D585B1DCE3FAA3F0583CA7A3588A, ___U3CAdTypesU3Ek__BackingField_5)); }
	inline HashSet_1_t67D33D99B1AE0A542D9C13CBD2729E6E18466F8E * get_U3CAdTypesU3Ek__BackingField_5() const { return ___U3CAdTypesU3Ek__BackingField_5; }
	inline HashSet_1_t67D33D99B1AE0A542D9C13CBD2729E6E18466F8E ** get_address_of_U3CAdTypesU3Ek__BackingField_5() { return &___U3CAdTypesU3Ek__BackingField_5; }
	inline void set_U3CAdTypesU3Ek__BackingField_5(HashSet_1_t67D33D99B1AE0A542D9C13CBD2729E6E18466F8E * value)
	{
		___U3CAdTypesU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAdTypesU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CTemplateIdsU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(AdLoader_tE5D111966C04D585B1DCE3FAA3F0583CA7A3588A, ___U3CTemplateIdsU3Ek__BackingField_6)); }
	inline HashSet_1_tAF31CD0D5B1F27E354D2DA33F8CF07F490E8C98A * get_U3CTemplateIdsU3Ek__BackingField_6() const { return ___U3CTemplateIdsU3Ek__BackingField_6; }
	inline HashSet_1_tAF31CD0D5B1F27E354D2DA33F8CF07F490E8C98A ** get_address_of_U3CTemplateIdsU3Ek__BackingField_6() { return &___U3CTemplateIdsU3Ek__BackingField_6; }
	inline void set_U3CTemplateIdsU3Ek__BackingField_6(HashSet_1_tAF31CD0D5B1F27E354D2DA33F8CF07F490E8C98A * value)
	{
		___U3CTemplateIdsU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTemplateIdsU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADLOADER_TE5D111966C04D585B1DCE3FAA3F0583CA7A3588A_H
#ifndef BUILDER_T855B9AD356DAB2EC5FA6C043605024B98EB4BAA7_H
#define BUILDER_T855B9AD356DAB2EC5FA6C043605024B98EB4BAA7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.Api.AdLoader_Builder
struct  Builder_t855B9AD356DAB2EC5FA6C043605024B98EB4BAA7  : public RuntimeObject
{
public:
	// System.String GoogleMobileAds.Api.AdLoader_Builder::<AdUnitId>k__BackingField
	String_t* ___U3CAdUnitIdU3Ek__BackingField_0;
	// System.Collections.Generic.HashSet`1<GoogleMobileAds.Api.NativeAdType> GoogleMobileAds.Api.AdLoader_Builder::<AdTypes>k__BackingField
	HashSet_1_t67D33D99B1AE0A542D9C13CBD2729E6E18466F8E * ___U3CAdTypesU3Ek__BackingField_1;
	// System.Collections.Generic.HashSet`1<System.String> GoogleMobileAds.Api.AdLoader_Builder::<TemplateIds>k__BackingField
	HashSet_1_tAF31CD0D5B1F27E354D2DA33F8CF07F490E8C98A * ___U3CTemplateIdsU3Ek__BackingField_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.Action`2<GoogleMobileAds.Api.CustomNativeTemplateAd,System.String>> GoogleMobileAds.Api.AdLoader_Builder::<CustomNativeTemplateClickHandlers>k__BackingField
	Dictionary_2_t8B8E8A2A647CF36A292C75063777A26CA5CB41B3 * ___U3CCustomNativeTemplateClickHandlersU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CAdUnitIdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Builder_t855B9AD356DAB2EC5FA6C043605024B98EB4BAA7, ___U3CAdUnitIdU3Ek__BackingField_0)); }
	inline String_t* get_U3CAdUnitIdU3Ek__BackingField_0() const { return ___U3CAdUnitIdU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CAdUnitIdU3Ek__BackingField_0() { return &___U3CAdUnitIdU3Ek__BackingField_0; }
	inline void set_U3CAdUnitIdU3Ek__BackingField_0(String_t* value)
	{
		___U3CAdUnitIdU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAdUnitIdU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CAdTypesU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Builder_t855B9AD356DAB2EC5FA6C043605024B98EB4BAA7, ___U3CAdTypesU3Ek__BackingField_1)); }
	inline HashSet_1_t67D33D99B1AE0A542D9C13CBD2729E6E18466F8E * get_U3CAdTypesU3Ek__BackingField_1() const { return ___U3CAdTypesU3Ek__BackingField_1; }
	inline HashSet_1_t67D33D99B1AE0A542D9C13CBD2729E6E18466F8E ** get_address_of_U3CAdTypesU3Ek__BackingField_1() { return &___U3CAdTypesU3Ek__BackingField_1; }
	inline void set_U3CAdTypesU3Ek__BackingField_1(HashSet_1_t67D33D99B1AE0A542D9C13CBD2729E6E18466F8E * value)
	{
		___U3CAdTypesU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAdTypesU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CTemplateIdsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Builder_t855B9AD356DAB2EC5FA6C043605024B98EB4BAA7, ___U3CTemplateIdsU3Ek__BackingField_2)); }
	inline HashSet_1_tAF31CD0D5B1F27E354D2DA33F8CF07F490E8C98A * get_U3CTemplateIdsU3Ek__BackingField_2() const { return ___U3CTemplateIdsU3Ek__BackingField_2; }
	inline HashSet_1_tAF31CD0D5B1F27E354D2DA33F8CF07F490E8C98A ** get_address_of_U3CTemplateIdsU3Ek__BackingField_2() { return &___U3CTemplateIdsU3Ek__BackingField_2; }
	inline void set_U3CTemplateIdsU3Ek__BackingField_2(HashSet_1_tAF31CD0D5B1F27E354D2DA33F8CF07F490E8C98A * value)
	{
		___U3CTemplateIdsU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTemplateIdsU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CCustomNativeTemplateClickHandlersU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Builder_t855B9AD356DAB2EC5FA6C043605024B98EB4BAA7, ___U3CCustomNativeTemplateClickHandlersU3Ek__BackingField_3)); }
	inline Dictionary_2_t8B8E8A2A647CF36A292C75063777A26CA5CB41B3 * get_U3CCustomNativeTemplateClickHandlersU3Ek__BackingField_3() const { return ___U3CCustomNativeTemplateClickHandlersU3Ek__BackingField_3; }
	inline Dictionary_2_t8B8E8A2A647CF36A292C75063777A26CA5CB41B3 ** get_address_of_U3CCustomNativeTemplateClickHandlersU3Ek__BackingField_3() { return &___U3CCustomNativeTemplateClickHandlersU3Ek__BackingField_3; }
	inline void set_U3CCustomNativeTemplateClickHandlersU3Ek__BackingField_3(Dictionary_2_t8B8E8A2A647CF36A292C75063777A26CA5CB41B3 * value)
	{
		___U3CCustomNativeTemplateClickHandlersU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCustomNativeTemplateClickHandlersU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILDER_T855B9AD356DAB2EC5FA6C043605024B98EB4BAA7_H
#ifndef ADSIZE_T10976E93E4E1F370899C5D6ED9DDB683F9C53E04_H
#define ADSIZE_T10976E93E4E1F370899C5D6ED9DDB683F9C53E04_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.Api.AdSize
struct  AdSize_t10976E93E4E1F370899C5D6ED9DDB683F9C53E04  : public RuntimeObject
{
public:
	// System.Boolean GoogleMobileAds.Api.AdSize::isSmartBanner
	bool ___isSmartBanner_0;
	// System.Int32 GoogleMobileAds.Api.AdSize::width
	int32_t ___width_1;
	// System.Int32 GoogleMobileAds.Api.AdSize::height
	int32_t ___height_2;

public:
	inline static int32_t get_offset_of_isSmartBanner_0() { return static_cast<int32_t>(offsetof(AdSize_t10976E93E4E1F370899C5D6ED9DDB683F9C53E04, ___isSmartBanner_0)); }
	inline bool get_isSmartBanner_0() const { return ___isSmartBanner_0; }
	inline bool* get_address_of_isSmartBanner_0() { return &___isSmartBanner_0; }
	inline void set_isSmartBanner_0(bool value)
	{
		___isSmartBanner_0 = value;
	}

	inline static int32_t get_offset_of_width_1() { return static_cast<int32_t>(offsetof(AdSize_t10976E93E4E1F370899C5D6ED9DDB683F9C53E04, ___width_1)); }
	inline int32_t get_width_1() const { return ___width_1; }
	inline int32_t* get_address_of_width_1() { return &___width_1; }
	inline void set_width_1(int32_t value)
	{
		___width_1 = value;
	}

	inline static int32_t get_offset_of_height_2() { return static_cast<int32_t>(offsetof(AdSize_t10976E93E4E1F370899C5D6ED9DDB683F9C53E04, ___height_2)); }
	inline int32_t get_height_2() const { return ___height_2; }
	inline int32_t* get_address_of_height_2() { return &___height_2; }
	inline void set_height_2(int32_t value)
	{
		___height_2 = value;
	}
};

struct AdSize_t10976E93E4E1F370899C5D6ED9DDB683F9C53E04_StaticFields
{
public:
	// GoogleMobileAds.Api.AdSize GoogleMobileAds.Api.AdSize::Banner
	AdSize_t10976E93E4E1F370899C5D6ED9DDB683F9C53E04 * ___Banner_3;
	// GoogleMobileAds.Api.AdSize GoogleMobileAds.Api.AdSize::MediumRectangle
	AdSize_t10976E93E4E1F370899C5D6ED9DDB683F9C53E04 * ___MediumRectangle_4;
	// GoogleMobileAds.Api.AdSize GoogleMobileAds.Api.AdSize::IABBanner
	AdSize_t10976E93E4E1F370899C5D6ED9DDB683F9C53E04 * ___IABBanner_5;
	// GoogleMobileAds.Api.AdSize GoogleMobileAds.Api.AdSize::Leaderboard
	AdSize_t10976E93E4E1F370899C5D6ED9DDB683F9C53E04 * ___Leaderboard_6;
	// GoogleMobileAds.Api.AdSize GoogleMobileAds.Api.AdSize::SmartBanner
	AdSize_t10976E93E4E1F370899C5D6ED9DDB683F9C53E04 * ___SmartBanner_7;
	// System.Int32 GoogleMobileAds.Api.AdSize::FullWidth
	int32_t ___FullWidth_8;

public:
	inline static int32_t get_offset_of_Banner_3() { return static_cast<int32_t>(offsetof(AdSize_t10976E93E4E1F370899C5D6ED9DDB683F9C53E04_StaticFields, ___Banner_3)); }
	inline AdSize_t10976E93E4E1F370899C5D6ED9DDB683F9C53E04 * get_Banner_3() const { return ___Banner_3; }
	inline AdSize_t10976E93E4E1F370899C5D6ED9DDB683F9C53E04 ** get_address_of_Banner_3() { return &___Banner_3; }
	inline void set_Banner_3(AdSize_t10976E93E4E1F370899C5D6ED9DDB683F9C53E04 * value)
	{
		___Banner_3 = value;
		Il2CppCodeGenWriteBarrier((&___Banner_3), value);
	}

	inline static int32_t get_offset_of_MediumRectangle_4() { return static_cast<int32_t>(offsetof(AdSize_t10976E93E4E1F370899C5D6ED9DDB683F9C53E04_StaticFields, ___MediumRectangle_4)); }
	inline AdSize_t10976E93E4E1F370899C5D6ED9DDB683F9C53E04 * get_MediumRectangle_4() const { return ___MediumRectangle_4; }
	inline AdSize_t10976E93E4E1F370899C5D6ED9DDB683F9C53E04 ** get_address_of_MediumRectangle_4() { return &___MediumRectangle_4; }
	inline void set_MediumRectangle_4(AdSize_t10976E93E4E1F370899C5D6ED9DDB683F9C53E04 * value)
	{
		___MediumRectangle_4 = value;
		Il2CppCodeGenWriteBarrier((&___MediumRectangle_4), value);
	}

	inline static int32_t get_offset_of_IABBanner_5() { return static_cast<int32_t>(offsetof(AdSize_t10976E93E4E1F370899C5D6ED9DDB683F9C53E04_StaticFields, ___IABBanner_5)); }
	inline AdSize_t10976E93E4E1F370899C5D6ED9DDB683F9C53E04 * get_IABBanner_5() const { return ___IABBanner_5; }
	inline AdSize_t10976E93E4E1F370899C5D6ED9DDB683F9C53E04 ** get_address_of_IABBanner_5() { return &___IABBanner_5; }
	inline void set_IABBanner_5(AdSize_t10976E93E4E1F370899C5D6ED9DDB683F9C53E04 * value)
	{
		___IABBanner_5 = value;
		Il2CppCodeGenWriteBarrier((&___IABBanner_5), value);
	}

	inline static int32_t get_offset_of_Leaderboard_6() { return static_cast<int32_t>(offsetof(AdSize_t10976E93E4E1F370899C5D6ED9DDB683F9C53E04_StaticFields, ___Leaderboard_6)); }
	inline AdSize_t10976E93E4E1F370899C5D6ED9DDB683F9C53E04 * get_Leaderboard_6() const { return ___Leaderboard_6; }
	inline AdSize_t10976E93E4E1F370899C5D6ED9DDB683F9C53E04 ** get_address_of_Leaderboard_6() { return &___Leaderboard_6; }
	inline void set_Leaderboard_6(AdSize_t10976E93E4E1F370899C5D6ED9DDB683F9C53E04 * value)
	{
		___Leaderboard_6 = value;
		Il2CppCodeGenWriteBarrier((&___Leaderboard_6), value);
	}

	inline static int32_t get_offset_of_SmartBanner_7() { return static_cast<int32_t>(offsetof(AdSize_t10976E93E4E1F370899C5D6ED9DDB683F9C53E04_StaticFields, ___SmartBanner_7)); }
	inline AdSize_t10976E93E4E1F370899C5D6ED9DDB683F9C53E04 * get_SmartBanner_7() const { return ___SmartBanner_7; }
	inline AdSize_t10976E93E4E1F370899C5D6ED9DDB683F9C53E04 ** get_address_of_SmartBanner_7() { return &___SmartBanner_7; }
	inline void set_SmartBanner_7(AdSize_t10976E93E4E1F370899C5D6ED9DDB683F9C53E04 * value)
	{
		___SmartBanner_7 = value;
		Il2CppCodeGenWriteBarrier((&___SmartBanner_7), value);
	}

	inline static int32_t get_offset_of_FullWidth_8() { return static_cast<int32_t>(offsetof(AdSize_t10976E93E4E1F370899C5D6ED9DDB683F9C53E04_StaticFields, ___FullWidth_8)); }
	inline int32_t get_FullWidth_8() const { return ___FullWidth_8; }
	inline int32_t* get_address_of_FullWidth_8() { return &___FullWidth_8; }
	inline void set_FullWidth_8(int32_t value)
	{
		___FullWidth_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADSIZE_T10976E93E4E1F370899C5D6ED9DDB683F9C53E04_H
#ifndef BANNERVIEW_T93EE32590F086BE31F51E2F84714B49C4328B4B5_H
#define BANNERVIEW_T93EE32590F086BE31F51E2F84714B49C4328B4B5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.Api.BannerView
struct  BannerView_t93EE32590F086BE31F51E2F84714B49C4328B4B5  : public RuntimeObject
{
public:
	// GoogleMobileAds.Common.IBannerClient GoogleMobileAds.Api.BannerView::client
	RuntimeObject* ___client_0;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Api.BannerView::OnAdLoaded
	EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 * ___OnAdLoaded_1;
	// System.EventHandler`1<GoogleMobileAds.Api.AdFailedToLoadEventArgs> GoogleMobileAds.Api.BannerView::OnAdFailedToLoad
	EventHandler_1_t5D4F18F6AB51C53CC8C112AFAADAEE92FB1C1A41 * ___OnAdFailedToLoad_2;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Api.BannerView::OnAdOpening
	EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 * ___OnAdOpening_3;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Api.BannerView::OnAdClosed
	EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 * ___OnAdClosed_4;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Api.BannerView::OnAdLeavingApplication
	EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 * ___OnAdLeavingApplication_5;

public:
	inline static int32_t get_offset_of_client_0() { return static_cast<int32_t>(offsetof(BannerView_t93EE32590F086BE31F51E2F84714B49C4328B4B5, ___client_0)); }
	inline RuntimeObject* get_client_0() const { return ___client_0; }
	inline RuntimeObject** get_address_of_client_0() { return &___client_0; }
	inline void set_client_0(RuntimeObject* value)
	{
		___client_0 = value;
		Il2CppCodeGenWriteBarrier((&___client_0), value);
	}

	inline static int32_t get_offset_of_OnAdLoaded_1() { return static_cast<int32_t>(offsetof(BannerView_t93EE32590F086BE31F51E2F84714B49C4328B4B5, ___OnAdLoaded_1)); }
	inline EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 * get_OnAdLoaded_1() const { return ___OnAdLoaded_1; }
	inline EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 ** get_address_of_OnAdLoaded_1() { return &___OnAdLoaded_1; }
	inline void set_OnAdLoaded_1(EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 * value)
	{
		___OnAdLoaded_1 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdLoaded_1), value);
	}

	inline static int32_t get_offset_of_OnAdFailedToLoad_2() { return static_cast<int32_t>(offsetof(BannerView_t93EE32590F086BE31F51E2F84714B49C4328B4B5, ___OnAdFailedToLoad_2)); }
	inline EventHandler_1_t5D4F18F6AB51C53CC8C112AFAADAEE92FB1C1A41 * get_OnAdFailedToLoad_2() const { return ___OnAdFailedToLoad_2; }
	inline EventHandler_1_t5D4F18F6AB51C53CC8C112AFAADAEE92FB1C1A41 ** get_address_of_OnAdFailedToLoad_2() { return &___OnAdFailedToLoad_2; }
	inline void set_OnAdFailedToLoad_2(EventHandler_1_t5D4F18F6AB51C53CC8C112AFAADAEE92FB1C1A41 * value)
	{
		___OnAdFailedToLoad_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdFailedToLoad_2), value);
	}

	inline static int32_t get_offset_of_OnAdOpening_3() { return static_cast<int32_t>(offsetof(BannerView_t93EE32590F086BE31F51E2F84714B49C4328B4B5, ___OnAdOpening_3)); }
	inline EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 * get_OnAdOpening_3() const { return ___OnAdOpening_3; }
	inline EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 ** get_address_of_OnAdOpening_3() { return &___OnAdOpening_3; }
	inline void set_OnAdOpening_3(EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 * value)
	{
		___OnAdOpening_3 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdOpening_3), value);
	}

	inline static int32_t get_offset_of_OnAdClosed_4() { return static_cast<int32_t>(offsetof(BannerView_t93EE32590F086BE31F51E2F84714B49C4328B4B5, ___OnAdClosed_4)); }
	inline EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 * get_OnAdClosed_4() const { return ___OnAdClosed_4; }
	inline EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 ** get_address_of_OnAdClosed_4() { return &___OnAdClosed_4; }
	inline void set_OnAdClosed_4(EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 * value)
	{
		___OnAdClosed_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdClosed_4), value);
	}

	inline static int32_t get_offset_of_OnAdLeavingApplication_5() { return static_cast<int32_t>(offsetof(BannerView_t93EE32590F086BE31F51E2F84714B49C4328B4B5, ___OnAdLeavingApplication_5)); }
	inline EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 * get_OnAdLeavingApplication_5() const { return ___OnAdLeavingApplication_5; }
	inline EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 ** get_address_of_OnAdLeavingApplication_5() { return &___OnAdLeavingApplication_5; }
	inline void set_OnAdLeavingApplication_5(EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 * value)
	{
		___OnAdLeavingApplication_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdLeavingApplication_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BANNERVIEW_T93EE32590F086BE31F51E2F84714B49C4328B4B5_H
#ifndef CUSTOMNATIVETEMPLATEAD_TA73CD30978AE046F6E81DA0DE0D90E0260870D94_H
#define CUSTOMNATIVETEMPLATEAD_TA73CD30978AE046F6E81DA0DE0D90E0260870D94_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.Api.CustomNativeTemplateAd
struct  CustomNativeTemplateAd_tA73CD30978AE046F6E81DA0DE0D90E0260870D94  : public RuntimeObject
{
public:
	// GoogleMobileAds.Common.ICustomNativeTemplateClient GoogleMobileAds.Api.CustomNativeTemplateAd::client
	RuntimeObject* ___client_0;

public:
	inline static int32_t get_offset_of_client_0() { return static_cast<int32_t>(offsetof(CustomNativeTemplateAd_tA73CD30978AE046F6E81DA0DE0D90E0260870D94, ___client_0)); }
	inline RuntimeObject* get_client_0() const { return ___client_0; }
	inline RuntimeObject** get_address_of_client_0() { return &___client_0; }
	inline void set_client_0(RuntimeObject* value)
	{
		___client_0 = value;
		Il2CppCodeGenWriteBarrier((&___client_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMNATIVETEMPLATEAD_TA73CD30978AE046F6E81DA0DE0D90E0260870D94_H
#ifndef INTERSTITIALAD_TAA5D23EA1E8483C7D10850AEA71D0DD8D75C89DD_H
#define INTERSTITIALAD_TAA5D23EA1E8483C7D10850AEA71D0DD8D75C89DD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.Api.InterstitialAd
struct  InterstitialAd_tAA5D23EA1E8483C7D10850AEA71D0DD8D75C89DD  : public RuntimeObject
{
public:
	// GoogleMobileAds.Common.IInterstitialClient GoogleMobileAds.Api.InterstitialAd::client
	RuntimeObject* ___client_0;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Api.InterstitialAd::OnAdLoaded
	EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 * ___OnAdLoaded_1;
	// System.EventHandler`1<GoogleMobileAds.Api.AdFailedToLoadEventArgs> GoogleMobileAds.Api.InterstitialAd::OnAdFailedToLoad
	EventHandler_1_t5D4F18F6AB51C53CC8C112AFAADAEE92FB1C1A41 * ___OnAdFailedToLoad_2;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Api.InterstitialAd::OnAdOpening
	EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 * ___OnAdOpening_3;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Api.InterstitialAd::OnAdClosed
	EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 * ___OnAdClosed_4;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Api.InterstitialAd::OnAdLeavingApplication
	EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 * ___OnAdLeavingApplication_5;

public:
	inline static int32_t get_offset_of_client_0() { return static_cast<int32_t>(offsetof(InterstitialAd_tAA5D23EA1E8483C7D10850AEA71D0DD8D75C89DD, ___client_0)); }
	inline RuntimeObject* get_client_0() const { return ___client_0; }
	inline RuntimeObject** get_address_of_client_0() { return &___client_0; }
	inline void set_client_0(RuntimeObject* value)
	{
		___client_0 = value;
		Il2CppCodeGenWriteBarrier((&___client_0), value);
	}

	inline static int32_t get_offset_of_OnAdLoaded_1() { return static_cast<int32_t>(offsetof(InterstitialAd_tAA5D23EA1E8483C7D10850AEA71D0DD8D75C89DD, ___OnAdLoaded_1)); }
	inline EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 * get_OnAdLoaded_1() const { return ___OnAdLoaded_1; }
	inline EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 ** get_address_of_OnAdLoaded_1() { return &___OnAdLoaded_1; }
	inline void set_OnAdLoaded_1(EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 * value)
	{
		___OnAdLoaded_1 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdLoaded_1), value);
	}

	inline static int32_t get_offset_of_OnAdFailedToLoad_2() { return static_cast<int32_t>(offsetof(InterstitialAd_tAA5D23EA1E8483C7D10850AEA71D0DD8D75C89DD, ___OnAdFailedToLoad_2)); }
	inline EventHandler_1_t5D4F18F6AB51C53CC8C112AFAADAEE92FB1C1A41 * get_OnAdFailedToLoad_2() const { return ___OnAdFailedToLoad_2; }
	inline EventHandler_1_t5D4F18F6AB51C53CC8C112AFAADAEE92FB1C1A41 ** get_address_of_OnAdFailedToLoad_2() { return &___OnAdFailedToLoad_2; }
	inline void set_OnAdFailedToLoad_2(EventHandler_1_t5D4F18F6AB51C53CC8C112AFAADAEE92FB1C1A41 * value)
	{
		___OnAdFailedToLoad_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdFailedToLoad_2), value);
	}

	inline static int32_t get_offset_of_OnAdOpening_3() { return static_cast<int32_t>(offsetof(InterstitialAd_tAA5D23EA1E8483C7D10850AEA71D0DD8D75C89DD, ___OnAdOpening_3)); }
	inline EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 * get_OnAdOpening_3() const { return ___OnAdOpening_3; }
	inline EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 ** get_address_of_OnAdOpening_3() { return &___OnAdOpening_3; }
	inline void set_OnAdOpening_3(EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 * value)
	{
		___OnAdOpening_3 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdOpening_3), value);
	}

	inline static int32_t get_offset_of_OnAdClosed_4() { return static_cast<int32_t>(offsetof(InterstitialAd_tAA5D23EA1E8483C7D10850AEA71D0DD8D75C89DD, ___OnAdClosed_4)); }
	inline EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 * get_OnAdClosed_4() const { return ___OnAdClosed_4; }
	inline EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 ** get_address_of_OnAdClosed_4() { return &___OnAdClosed_4; }
	inline void set_OnAdClosed_4(EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 * value)
	{
		___OnAdClosed_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdClosed_4), value);
	}

	inline static int32_t get_offset_of_OnAdLeavingApplication_5() { return static_cast<int32_t>(offsetof(InterstitialAd_tAA5D23EA1E8483C7D10850AEA71D0DD8D75C89DD, ___OnAdLeavingApplication_5)); }
	inline EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 * get_OnAdLeavingApplication_5() const { return ___OnAdLeavingApplication_5; }
	inline EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 ** get_address_of_OnAdLeavingApplication_5() { return &___OnAdLeavingApplication_5; }
	inline void set_OnAdLeavingApplication_5(EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 * value)
	{
		___OnAdLeavingApplication_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdLeavingApplication_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERSTITIALAD_TAA5D23EA1E8483C7D10850AEA71D0DD8D75C89DD_H
#ifndef MEDIATIONEXTRAS_T53F0F7F90139A2D039272C9764DC880D4B3AB301_H
#define MEDIATIONEXTRAS_T53F0F7F90139A2D039272C9764DC880D4B3AB301_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.Api.Mediation.MediationExtras
struct  MediationExtras_t53F0F7F90139A2D039272C9764DC880D4B3AB301  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.String> GoogleMobileAds.Api.Mediation.MediationExtras::<Extras>k__BackingField
	Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * ___U3CExtrasU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CExtrasU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MediationExtras_t53F0F7F90139A2D039272C9764DC880D4B3AB301, ___U3CExtrasU3Ek__BackingField_0)); }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * get_U3CExtrasU3Ek__BackingField_0() const { return ___U3CExtrasU3Ek__BackingField_0; }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC ** get_address_of_U3CExtrasU3Ek__BackingField_0() { return &___U3CExtrasU3Ek__BackingField_0; }
	inline void set_U3CExtrasU3Ek__BackingField_0(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * value)
	{
		___U3CExtrasU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CExtrasU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEDIATIONEXTRAS_T53F0F7F90139A2D039272C9764DC880D4B3AB301_H
#ifndef MOBILEADS_TFA519006704C915B6A32186550B3203DFF8D242D_H
#define MOBILEADS_TFA519006704C915B6A32186550B3203DFF8D242D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.Api.MobileAds
struct  MobileAds_tFA519006704C915B6A32186550B3203DFF8D242D  : public RuntimeObject
{
public:

public:
};

struct MobileAds_tFA519006704C915B6A32186550B3203DFF8D242D_StaticFields
{
public:
	// GoogleMobileAds.Common.IMobileAdsClient GoogleMobileAds.Api.MobileAds::client
	RuntimeObject* ___client_0;

public:
	inline static int32_t get_offset_of_client_0() { return static_cast<int32_t>(offsetof(MobileAds_tFA519006704C915B6A32186550B3203DFF8D242D_StaticFields, ___client_0)); }
	inline RuntimeObject* get_client_0() const { return ___client_0; }
	inline RuntimeObject** get_address_of_client_0() { return &___client_0; }
	inline void set_client_0(RuntimeObject* value)
	{
		___client_0 = value;
		Il2CppCodeGenWriteBarrier((&___client_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOBILEADS_TFA519006704C915B6A32186550B3203DFF8D242D_H
#ifndef REWARDBASEDVIDEOAD_T9795550B217AAB71B32AEDA94163C6A929353491_H
#define REWARDBASEDVIDEOAD_T9795550B217AAB71B32AEDA94163C6A929353491_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.Api.RewardBasedVideoAd
struct  RewardBasedVideoAd_t9795550B217AAB71B32AEDA94163C6A929353491  : public RuntimeObject
{
public:
	// GoogleMobileAds.Common.IRewardBasedVideoAdClient GoogleMobileAds.Api.RewardBasedVideoAd::client
	RuntimeObject* ___client_0;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Api.RewardBasedVideoAd::OnAdLoaded
	EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 * ___OnAdLoaded_2;
	// System.EventHandler`1<GoogleMobileAds.Api.AdFailedToLoadEventArgs> GoogleMobileAds.Api.RewardBasedVideoAd::OnAdFailedToLoad
	EventHandler_1_t5D4F18F6AB51C53CC8C112AFAADAEE92FB1C1A41 * ___OnAdFailedToLoad_3;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Api.RewardBasedVideoAd::OnAdOpening
	EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 * ___OnAdOpening_4;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Api.RewardBasedVideoAd::OnAdStarted
	EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 * ___OnAdStarted_5;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Api.RewardBasedVideoAd::OnAdClosed
	EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 * ___OnAdClosed_6;
	// System.EventHandler`1<GoogleMobileAds.Api.Reward> GoogleMobileAds.Api.RewardBasedVideoAd::OnAdRewarded
	EventHandler_1_t0B0C84881A5FE7FACD54CC9982D03AE5DC1AA168 * ___OnAdRewarded_7;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Api.RewardBasedVideoAd::OnAdLeavingApplication
	EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 * ___OnAdLeavingApplication_8;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Api.RewardBasedVideoAd::OnAdCompleted
	EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 * ___OnAdCompleted_9;

public:
	inline static int32_t get_offset_of_client_0() { return static_cast<int32_t>(offsetof(RewardBasedVideoAd_t9795550B217AAB71B32AEDA94163C6A929353491, ___client_0)); }
	inline RuntimeObject* get_client_0() const { return ___client_0; }
	inline RuntimeObject** get_address_of_client_0() { return &___client_0; }
	inline void set_client_0(RuntimeObject* value)
	{
		___client_0 = value;
		Il2CppCodeGenWriteBarrier((&___client_0), value);
	}

	inline static int32_t get_offset_of_OnAdLoaded_2() { return static_cast<int32_t>(offsetof(RewardBasedVideoAd_t9795550B217AAB71B32AEDA94163C6A929353491, ___OnAdLoaded_2)); }
	inline EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 * get_OnAdLoaded_2() const { return ___OnAdLoaded_2; }
	inline EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 ** get_address_of_OnAdLoaded_2() { return &___OnAdLoaded_2; }
	inline void set_OnAdLoaded_2(EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 * value)
	{
		___OnAdLoaded_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdLoaded_2), value);
	}

	inline static int32_t get_offset_of_OnAdFailedToLoad_3() { return static_cast<int32_t>(offsetof(RewardBasedVideoAd_t9795550B217AAB71B32AEDA94163C6A929353491, ___OnAdFailedToLoad_3)); }
	inline EventHandler_1_t5D4F18F6AB51C53CC8C112AFAADAEE92FB1C1A41 * get_OnAdFailedToLoad_3() const { return ___OnAdFailedToLoad_3; }
	inline EventHandler_1_t5D4F18F6AB51C53CC8C112AFAADAEE92FB1C1A41 ** get_address_of_OnAdFailedToLoad_3() { return &___OnAdFailedToLoad_3; }
	inline void set_OnAdFailedToLoad_3(EventHandler_1_t5D4F18F6AB51C53CC8C112AFAADAEE92FB1C1A41 * value)
	{
		___OnAdFailedToLoad_3 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdFailedToLoad_3), value);
	}

	inline static int32_t get_offset_of_OnAdOpening_4() { return static_cast<int32_t>(offsetof(RewardBasedVideoAd_t9795550B217AAB71B32AEDA94163C6A929353491, ___OnAdOpening_4)); }
	inline EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 * get_OnAdOpening_4() const { return ___OnAdOpening_4; }
	inline EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 ** get_address_of_OnAdOpening_4() { return &___OnAdOpening_4; }
	inline void set_OnAdOpening_4(EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 * value)
	{
		___OnAdOpening_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdOpening_4), value);
	}

	inline static int32_t get_offset_of_OnAdStarted_5() { return static_cast<int32_t>(offsetof(RewardBasedVideoAd_t9795550B217AAB71B32AEDA94163C6A929353491, ___OnAdStarted_5)); }
	inline EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 * get_OnAdStarted_5() const { return ___OnAdStarted_5; }
	inline EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 ** get_address_of_OnAdStarted_5() { return &___OnAdStarted_5; }
	inline void set_OnAdStarted_5(EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 * value)
	{
		___OnAdStarted_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdStarted_5), value);
	}

	inline static int32_t get_offset_of_OnAdClosed_6() { return static_cast<int32_t>(offsetof(RewardBasedVideoAd_t9795550B217AAB71B32AEDA94163C6A929353491, ___OnAdClosed_6)); }
	inline EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 * get_OnAdClosed_6() const { return ___OnAdClosed_6; }
	inline EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 ** get_address_of_OnAdClosed_6() { return &___OnAdClosed_6; }
	inline void set_OnAdClosed_6(EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 * value)
	{
		___OnAdClosed_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdClosed_6), value);
	}

	inline static int32_t get_offset_of_OnAdRewarded_7() { return static_cast<int32_t>(offsetof(RewardBasedVideoAd_t9795550B217AAB71B32AEDA94163C6A929353491, ___OnAdRewarded_7)); }
	inline EventHandler_1_t0B0C84881A5FE7FACD54CC9982D03AE5DC1AA168 * get_OnAdRewarded_7() const { return ___OnAdRewarded_7; }
	inline EventHandler_1_t0B0C84881A5FE7FACD54CC9982D03AE5DC1AA168 ** get_address_of_OnAdRewarded_7() { return &___OnAdRewarded_7; }
	inline void set_OnAdRewarded_7(EventHandler_1_t0B0C84881A5FE7FACD54CC9982D03AE5DC1AA168 * value)
	{
		___OnAdRewarded_7 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdRewarded_7), value);
	}

	inline static int32_t get_offset_of_OnAdLeavingApplication_8() { return static_cast<int32_t>(offsetof(RewardBasedVideoAd_t9795550B217AAB71B32AEDA94163C6A929353491, ___OnAdLeavingApplication_8)); }
	inline EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 * get_OnAdLeavingApplication_8() const { return ___OnAdLeavingApplication_8; }
	inline EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 ** get_address_of_OnAdLeavingApplication_8() { return &___OnAdLeavingApplication_8; }
	inline void set_OnAdLeavingApplication_8(EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 * value)
	{
		___OnAdLeavingApplication_8 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdLeavingApplication_8), value);
	}

	inline static int32_t get_offset_of_OnAdCompleted_9() { return static_cast<int32_t>(offsetof(RewardBasedVideoAd_t9795550B217AAB71B32AEDA94163C6A929353491, ___OnAdCompleted_9)); }
	inline EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 * get_OnAdCompleted_9() const { return ___OnAdCompleted_9; }
	inline EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 ** get_address_of_OnAdCompleted_9() { return &___OnAdCompleted_9; }
	inline void set_OnAdCompleted_9(EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 * value)
	{
		___OnAdCompleted_9 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdCompleted_9), value);
	}
};

struct RewardBasedVideoAd_t9795550B217AAB71B32AEDA94163C6A929353491_StaticFields
{
public:
	// GoogleMobileAds.Api.RewardBasedVideoAd GoogleMobileAds.Api.RewardBasedVideoAd::instance
	RewardBasedVideoAd_t9795550B217AAB71B32AEDA94163C6A929353491 * ___instance_1;

public:
	inline static int32_t get_offset_of_instance_1() { return static_cast<int32_t>(offsetof(RewardBasedVideoAd_t9795550B217AAB71B32AEDA94163C6A929353491_StaticFields, ___instance_1)); }
	inline RewardBasedVideoAd_t9795550B217AAB71B32AEDA94163C6A929353491 * get_instance_1() const { return ___instance_1; }
	inline RewardBasedVideoAd_t9795550B217AAB71B32AEDA94163C6A929353491 ** get_address_of_instance_1() { return &___instance_1; }
	inline void set_instance_1(RewardBasedVideoAd_t9795550B217AAB71B32AEDA94163C6A929353491 * value)
	{
		___instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REWARDBASEDVIDEOAD_T9795550B217AAB71B32AEDA94163C6A929353491_H
#ifndef REWARDEDAD_TF537DADD772F3E98E9283E7CE9CB2AAE957E16D4_H
#define REWARDEDAD_TF537DADD772F3E98E9283E7CE9CB2AAE957E16D4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.Api.RewardedAd
struct  RewardedAd_tF537DADD772F3E98E9283E7CE9CB2AAE957E16D4  : public RuntimeObject
{
public:
	// GoogleMobileAds.Common.IRewardedAdClient GoogleMobileAds.Api.RewardedAd::client
	RuntimeObject* ___client_0;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Api.RewardedAd::OnAdLoaded
	EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 * ___OnAdLoaded_1;
	// System.EventHandler`1<GoogleMobileAds.Api.AdErrorEventArgs> GoogleMobileAds.Api.RewardedAd::OnAdFailedToLoad
	EventHandler_1_t6770E1CD9311E89A0B3CC72E0247509E692D0984 * ___OnAdFailedToLoad_2;
	// System.EventHandler`1<GoogleMobileAds.Api.AdErrorEventArgs> GoogleMobileAds.Api.RewardedAd::OnAdFailedToShow
	EventHandler_1_t6770E1CD9311E89A0B3CC72E0247509E692D0984 * ___OnAdFailedToShow_3;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Api.RewardedAd::OnAdOpening
	EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 * ___OnAdOpening_4;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Api.RewardedAd::OnAdClosed
	EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 * ___OnAdClosed_5;
	// System.EventHandler`1<GoogleMobileAds.Api.Reward> GoogleMobileAds.Api.RewardedAd::OnUserEarnedReward
	EventHandler_1_t0B0C84881A5FE7FACD54CC9982D03AE5DC1AA168 * ___OnUserEarnedReward_6;

public:
	inline static int32_t get_offset_of_client_0() { return static_cast<int32_t>(offsetof(RewardedAd_tF537DADD772F3E98E9283E7CE9CB2AAE957E16D4, ___client_0)); }
	inline RuntimeObject* get_client_0() const { return ___client_0; }
	inline RuntimeObject** get_address_of_client_0() { return &___client_0; }
	inline void set_client_0(RuntimeObject* value)
	{
		___client_0 = value;
		Il2CppCodeGenWriteBarrier((&___client_0), value);
	}

	inline static int32_t get_offset_of_OnAdLoaded_1() { return static_cast<int32_t>(offsetof(RewardedAd_tF537DADD772F3E98E9283E7CE9CB2AAE957E16D4, ___OnAdLoaded_1)); }
	inline EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 * get_OnAdLoaded_1() const { return ___OnAdLoaded_1; }
	inline EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 ** get_address_of_OnAdLoaded_1() { return &___OnAdLoaded_1; }
	inline void set_OnAdLoaded_1(EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 * value)
	{
		___OnAdLoaded_1 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdLoaded_1), value);
	}

	inline static int32_t get_offset_of_OnAdFailedToLoad_2() { return static_cast<int32_t>(offsetof(RewardedAd_tF537DADD772F3E98E9283E7CE9CB2AAE957E16D4, ___OnAdFailedToLoad_2)); }
	inline EventHandler_1_t6770E1CD9311E89A0B3CC72E0247509E692D0984 * get_OnAdFailedToLoad_2() const { return ___OnAdFailedToLoad_2; }
	inline EventHandler_1_t6770E1CD9311E89A0B3CC72E0247509E692D0984 ** get_address_of_OnAdFailedToLoad_2() { return &___OnAdFailedToLoad_2; }
	inline void set_OnAdFailedToLoad_2(EventHandler_1_t6770E1CD9311E89A0B3CC72E0247509E692D0984 * value)
	{
		___OnAdFailedToLoad_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdFailedToLoad_2), value);
	}

	inline static int32_t get_offset_of_OnAdFailedToShow_3() { return static_cast<int32_t>(offsetof(RewardedAd_tF537DADD772F3E98E9283E7CE9CB2AAE957E16D4, ___OnAdFailedToShow_3)); }
	inline EventHandler_1_t6770E1CD9311E89A0B3CC72E0247509E692D0984 * get_OnAdFailedToShow_3() const { return ___OnAdFailedToShow_3; }
	inline EventHandler_1_t6770E1CD9311E89A0B3CC72E0247509E692D0984 ** get_address_of_OnAdFailedToShow_3() { return &___OnAdFailedToShow_3; }
	inline void set_OnAdFailedToShow_3(EventHandler_1_t6770E1CD9311E89A0B3CC72E0247509E692D0984 * value)
	{
		___OnAdFailedToShow_3 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdFailedToShow_3), value);
	}

	inline static int32_t get_offset_of_OnAdOpening_4() { return static_cast<int32_t>(offsetof(RewardedAd_tF537DADD772F3E98E9283E7CE9CB2AAE957E16D4, ___OnAdOpening_4)); }
	inline EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 * get_OnAdOpening_4() const { return ___OnAdOpening_4; }
	inline EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 ** get_address_of_OnAdOpening_4() { return &___OnAdOpening_4; }
	inline void set_OnAdOpening_4(EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 * value)
	{
		___OnAdOpening_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdOpening_4), value);
	}

	inline static int32_t get_offset_of_OnAdClosed_5() { return static_cast<int32_t>(offsetof(RewardedAd_tF537DADD772F3E98E9283E7CE9CB2AAE957E16D4, ___OnAdClosed_5)); }
	inline EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 * get_OnAdClosed_5() const { return ___OnAdClosed_5; }
	inline EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 ** get_address_of_OnAdClosed_5() { return &___OnAdClosed_5; }
	inline void set_OnAdClosed_5(EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 * value)
	{
		___OnAdClosed_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdClosed_5), value);
	}

	inline static int32_t get_offset_of_OnUserEarnedReward_6() { return static_cast<int32_t>(offsetof(RewardedAd_tF537DADD772F3E98E9283E7CE9CB2AAE957E16D4, ___OnUserEarnedReward_6)); }
	inline EventHandler_1_t0B0C84881A5FE7FACD54CC9982D03AE5DC1AA168 * get_OnUserEarnedReward_6() const { return ___OnUserEarnedReward_6; }
	inline EventHandler_1_t0B0C84881A5FE7FACD54CC9982D03AE5DC1AA168 ** get_address_of_OnUserEarnedReward_6() { return &___OnUserEarnedReward_6; }
	inline void set_OnUserEarnedReward_6(EventHandler_1_t0B0C84881A5FE7FACD54CC9982D03AE5DC1AA168 * value)
	{
		___OnUserEarnedReward_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnUserEarnedReward_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REWARDEDAD_TF537DADD772F3E98E9283E7CE9CB2AAE957E16D4_H
#ifndef REWARDEDADDUMMYCLIENT_T85A34E08FDD3D986DFA1CAC1166A85FCD92F2BD5_H
#define REWARDEDADDUMMYCLIENT_T85A34E08FDD3D986DFA1CAC1166A85FCD92F2BD5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.Common.RewardedAdDummyClient
struct  RewardedAdDummyClient_t85A34E08FDD3D986DFA1CAC1166A85FCD92F2BD5  : public RuntimeObject
{
public:
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Common.RewardedAdDummyClient::OnAdLoaded
	EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 * ___OnAdLoaded_0;
	// System.EventHandler`1<GoogleMobileAds.Api.AdErrorEventArgs> GoogleMobileAds.Common.RewardedAdDummyClient::OnAdFailedToLoad
	EventHandler_1_t6770E1CD9311E89A0B3CC72E0247509E692D0984 * ___OnAdFailedToLoad_1;
	// System.EventHandler`1<GoogleMobileAds.Api.AdErrorEventArgs> GoogleMobileAds.Common.RewardedAdDummyClient::OnAdFailedToShow
	EventHandler_1_t6770E1CD9311E89A0B3CC72E0247509E692D0984 * ___OnAdFailedToShow_2;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Common.RewardedAdDummyClient::OnAdOpening
	EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 * ___OnAdOpening_3;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Common.RewardedAdDummyClient::OnAdClosed
	EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 * ___OnAdClosed_4;
	// System.EventHandler`1<GoogleMobileAds.Api.Reward> GoogleMobileAds.Common.RewardedAdDummyClient::OnUserEarnedReward
	EventHandler_1_t0B0C84881A5FE7FACD54CC9982D03AE5DC1AA168 * ___OnUserEarnedReward_5;

public:
	inline static int32_t get_offset_of_OnAdLoaded_0() { return static_cast<int32_t>(offsetof(RewardedAdDummyClient_t85A34E08FDD3D986DFA1CAC1166A85FCD92F2BD5, ___OnAdLoaded_0)); }
	inline EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 * get_OnAdLoaded_0() const { return ___OnAdLoaded_0; }
	inline EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 ** get_address_of_OnAdLoaded_0() { return &___OnAdLoaded_0; }
	inline void set_OnAdLoaded_0(EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 * value)
	{
		___OnAdLoaded_0 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdLoaded_0), value);
	}

	inline static int32_t get_offset_of_OnAdFailedToLoad_1() { return static_cast<int32_t>(offsetof(RewardedAdDummyClient_t85A34E08FDD3D986DFA1CAC1166A85FCD92F2BD5, ___OnAdFailedToLoad_1)); }
	inline EventHandler_1_t6770E1CD9311E89A0B3CC72E0247509E692D0984 * get_OnAdFailedToLoad_1() const { return ___OnAdFailedToLoad_1; }
	inline EventHandler_1_t6770E1CD9311E89A0B3CC72E0247509E692D0984 ** get_address_of_OnAdFailedToLoad_1() { return &___OnAdFailedToLoad_1; }
	inline void set_OnAdFailedToLoad_1(EventHandler_1_t6770E1CD9311E89A0B3CC72E0247509E692D0984 * value)
	{
		___OnAdFailedToLoad_1 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdFailedToLoad_1), value);
	}

	inline static int32_t get_offset_of_OnAdFailedToShow_2() { return static_cast<int32_t>(offsetof(RewardedAdDummyClient_t85A34E08FDD3D986DFA1CAC1166A85FCD92F2BD5, ___OnAdFailedToShow_2)); }
	inline EventHandler_1_t6770E1CD9311E89A0B3CC72E0247509E692D0984 * get_OnAdFailedToShow_2() const { return ___OnAdFailedToShow_2; }
	inline EventHandler_1_t6770E1CD9311E89A0B3CC72E0247509E692D0984 ** get_address_of_OnAdFailedToShow_2() { return &___OnAdFailedToShow_2; }
	inline void set_OnAdFailedToShow_2(EventHandler_1_t6770E1CD9311E89A0B3CC72E0247509E692D0984 * value)
	{
		___OnAdFailedToShow_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdFailedToShow_2), value);
	}

	inline static int32_t get_offset_of_OnAdOpening_3() { return static_cast<int32_t>(offsetof(RewardedAdDummyClient_t85A34E08FDD3D986DFA1CAC1166A85FCD92F2BD5, ___OnAdOpening_3)); }
	inline EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 * get_OnAdOpening_3() const { return ___OnAdOpening_3; }
	inline EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 ** get_address_of_OnAdOpening_3() { return &___OnAdOpening_3; }
	inline void set_OnAdOpening_3(EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 * value)
	{
		___OnAdOpening_3 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdOpening_3), value);
	}

	inline static int32_t get_offset_of_OnAdClosed_4() { return static_cast<int32_t>(offsetof(RewardedAdDummyClient_t85A34E08FDD3D986DFA1CAC1166A85FCD92F2BD5, ___OnAdClosed_4)); }
	inline EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 * get_OnAdClosed_4() const { return ___OnAdClosed_4; }
	inline EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 ** get_address_of_OnAdClosed_4() { return &___OnAdClosed_4; }
	inline void set_OnAdClosed_4(EventHandler_1_t0AE3AC8D80EB2E6F59F4164AD4CA43F3669A6711 * value)
	{
		___OnAdClosed_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdClosed_4), value);
	}

	inline static int32_t get_offset_of_OnUserEarnedReward_5() { return static_cast<int32_t>(offsetof(RewardedAdDummyClient_t85A34E08FDD3D986DFA1CAC1166A85FCD92F2BD5, ___OnUserEarnedReward_5)); }
	inline EventHandler_1_t0B0C84881A5FE7FACD54CC9982D03AE5DC1AA168 * get_OnUserEarnedReward_5() const { return ___OnUserEarnedReward_5; }
	inline EventHandler_1_t0B0C84881A5FE7FACD54CC9982D03AE5DC1AA168 ** get_address_of_OnUserEarnedReward_5() { return &___OnUserEarnedReward_5; }
	inline void set_OnUserEarnedReward_5(EventHandler_1_t0B0C84881A5FE7FACD54CC9982D03AE5DC1AA168 * value)
	{
		___OnUserEarnedReward_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnUserEarnedReward_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REWARDEDADDUMMYCLIENT_T85A34E08FDD3D986DFA1CAC1166A85FCD92F2BD5_H
#ifndef UTILS_TBD140E67B725142CBE4F6441B6628592C49D892B_H
#define UTILS_TBD140E67B725142CBE4F6441B6628592C49D892B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.Common.Utils
struct  Utils_tBD140E67B725142CBE4F6441B6628592C49D892B  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UTILS_TBD140E67B725142CBE4F6441B6628592C49D892B_H
#ifndef EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#define EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventArgs
struct  EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E  : public RuntimeObject
{
public:

public:
};

struct EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields, ___Empty_0)); }
	inline EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef ADERROREVENTARGS_T9C85856D82FD7BF9084CE64814B53E6D7A046A42_H
#define ADERROREVENTARGS_T9C85856D82FD7BF9084CE64814B53E6D7A046A42_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.Api.AdErrorEventArgs
struct  AdErrorEventArgs_t9C85856D82FD7BF9084CE64814B53E6D7A046A42  : public EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E
{
public:
	// System.String GoogleMobileAds.Api.AdErrorEventArgs::<Message>k__BackingField
	String_t* ___U3CMessageU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CMessageU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AdErrorEventArgs_t9C85856D82FD7BF9084CE64814B53E6D7A046A42, ___U3CMessageU3Ek__BackingField_1)); }
	inline String_t* get_U3CMessageU3Ek__BackingField_1() const { return ___U3CMessageU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CMessageU3Ek__BackingField_1() { return &___U3CMessageU3Ek__BackingField_1; }
	inline void set_U3CMessageU3Ek__BackingField_1(String_t* value)
	{
		___U3CMessageU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMessageU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADERROREVENTARGS_T9C85856D82FD7BF9084CE64814B53E6D7A046A42_H
#ifndef ADFAILEDTOLOADEVENTARGS_T7DBB865D1AC4FB5F962042A40D0DD5B413377F29_H
#define ADFAILEDTOLOADEVENTARGS_T7DBB865D1AC4FB5F962042A40D0DD5B413377F29_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.Api.AdFailedToLoadEventArgs
struct  AdFailedToLoadEventArgs_t7DBB865D1AC4FB5F962042A40D0DD5B413377F29  : public EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E
{
public:
	// System.String GoogleMobileAds.Api.AdFailedToLoadEventArgs::<Message>k__BackingField
	String_t* ___U3CMessageU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CMessageU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AdFailedToLoadEventArgs_t7DBB865D1AC4FB5F962042A40D0DD5B413377F29, ___U3CMessageU3Ek__BackingField_1)); }
	inline String_t* get_U3CMessageU3Ek__BackingField_1() const { return ___U3CMessageU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CMessageU3Ek__BackingField_1() { return &___U3CMessageU3Ek__BackingField_1; }
	inline void set_U3CMessageU3Ek__BackingField_1(String_t* value)
	{
		___U3CMessageU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMessageU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADFAILEDTOLOADEVENTARGS_T7DBB865D1AC4FB5F962042A40D0DD5B413377F29_H
#ifndef CUSTOMNATIVEEVENTARGS_TA1BC7F7FF42154E57061FCA213F6EB7166C7831C_H
#define CUSTOMNATIVEEVENTARGS_TA1BC7F7FF42154E57061FCA213F6EB7166C7831C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.Api.CustomNativeEventArgs
struct  CustomNativeEventArgs_tA1BC7F7FF42154E57061FCA213F6EB7166C7831C  : public EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E
{
public:
	// GoogleMobileAds.Api.CustomNativeTemplateAd GoogleMobileAds.Api.CustomNativeEventArgs::<nativeAd>k__BackingField
	CustomNativeTemplateAd_tA73CD30978AE046F6E81DA0DE0D90E0260870D94 * ___U3CnativeAdU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CnativeAdU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CustomNativeEventArgs_tA1BC7F7FF42154E57061FCA213F6EB7166C7831C, ___U3CnativeAdU3Ek__BackingField_1)); }
	inline CustomNativeTemplateAd_tA73CD30978AE046F6E81DA0DE0D90E0260870D94 * get_U3CnativeAdU3Ek__BackingField_1() const { return ___U3CnativeAdU3Ek__BackingField_1; }
	inline CustomNativeTemplateAd_tA73CD30978AE046F6E81DA0DE0D90E0260870D94 ** get_address_of_U3CnativeAdU3Ek__BackingField_1() { return &___U3CnativeAdU3Ek__BackingField_1; }
	inline void set_U3CnativeAdU3Ek__BackingField_1(CustomNativeTemplateAd_tA73CD30978AE046F6E81DA0DE0D90E0260870D94 * value)
	{
		___U3CnativeAdU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnativeAdU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMNATIVEEVENTARGS_TA1BC7F7FF42154E57061FCA213F6EB7166C7831C_H
#ifndef REWARD_T5656463A5A19508F4B9AF6BAB14D343F5A9EC260_H
#define REWARD_T5656463A5A19508F4B9AF6BAB14D343F5A9EC260_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.Api.Reward
struct  Reward_t5656463A5A19508F4B9AF6BAB14D343F5A9EC260  : public EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E
{
public:
	// System.String GoogleMobileAds.Api.Reward::<Type>k__BackingField
	String_t* ___U3CTypeU3Ek__BackingField_1;
	// System.Double GoogleMobileAds.Api.Reward::<Amount>k__BackingField
	double ___U3CAmountU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CTypeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Reward_t5656463A5A19508F4B9AF6BAB14D343F5A9EC260, ___U3CTypeU3Ek__BackingField_1)); }
	inline String_t* get_U3CTypeU3Ek__BackingField_1() const { return ___U3CTypeU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CTypeU3Ek__BackingField_1() { return &___U3CTypeU3Ek__BackingField_1; }
	inline void set_U3CTypeU3Ek__BackingField_1(String_t* value)
	{
		___U3CTypeU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTypeU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CAmountU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Reward_t5656463A5A19508F4B9AF6BAB14D343F5A9EC260, ___U3CAmountU3Ek__BackingField_2)); }
	inline double get_U3CAmountU3Ek__BackingField_2() const { return ___U3CAmountU3Ek__BackingField_2; }
	inline double* get_address_of_U3CAmountU3Ek__BackingField_2() { return &___U3CAmountU3Ek__BackingField_2; }
	inline void set_U3CAmountU3Ek__BackingField_2(double value)
	{
		___U3CAmountU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REWARD_T5656463A5A19508F4B9AF6BAB14D343F5A9EC260_H
#ifndef BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#define BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifndef DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#define DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 
{
public:
	// System.UInt64 System.DateTime::dateData
	uint64_t ___dateData_44;

public:
	inline static int32_t get_offset_of_dateData_44() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132, ___dateData_44)); }
	inline uint64_t get_dateData_44() const { return ___dateData_44; }
	inline uint64_t* get_address_of_dateData_44() { return &___dateData_44; }
	inline void set_dateData_44(uint64_t value)
	{
		___dateData_44 = value;
	}
};

struct DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields
{
public:
	// System.Int32[] System.DateTime::DaysToMonth365
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth365_29;
	// System.Int32[] System.DateTime::DaysToMonth366
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth366_30;
	// System.DateTime System.DateTime::MinValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MinValue_31;
	// System.DateTime System.DateTime::MaxValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MaxValue_32;

public:
	inline static int32_t get_offset_of_DaysToMonth365_29() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth365_29)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth365_29() const { return ___DaysToMonth365_29; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth365_29() { return &___DaysToMonth365_29; }
	inline void set_DaysToMonth365_29(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth365_29 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth365_29), value);
	}

	inline static int32_t get_offset_of_DaysToMonth366_30() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth366_30)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth366_30() const { return ___DaysToMonth366_30; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth366_30() { return &___DaysToMonth366_30; }
	inline void set_DaysToMonth366_30(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth366_30 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth366_30), value);
	}

	inline static int32_t get_offset_of_MinValue_31() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MinValue_31)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MinValue_31() const { return ___MinValue_31; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MinValue_31() { return &___MinValue_31; }
	inline void set_MinValue_31(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MinValue_31 = value;
	}

	inline static int32_t get_offset_of_MaxValue_32() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MaxValue_32)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MaxValue_32() const { return ___MaxValue_32; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MaxValue_32() { return &___MaxValue_32; }
	inline void set_MaxValue_32(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MaxValue_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef NULLABLE_1_T9E6A67BECE376F0623B5C857F5674A0311C41793_H
#define NULLABLE_1_T9E6A67BECE376F0623B5C857F5674A0311C41793_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Boolean>
struct  Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 
{
public:
	// T System.Nullable`1::value
	bool ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793, ___value_0)); }
	inline bool get_value_0() const { return ___value_0; }
	inline bool* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(bool value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T9E6A67BECE376F0623B5C857F5674A0311C41793_H
#ifndef NULLABLE_1_TE4EDC8D5ED2772A911F67696644E6C77FA716DC0_H
#define NULLABLE_1_TE4EDC8D5ED2772A911F67696644E6C77FA716DC0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Single>
struct  Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0 
{
public:
	// T System.Nullable`1::value
	float ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0, ___value_0)); }
	inline float get_value_0() const { return ___value_0; }
	inline float* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(float value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_TE4EDC8D5ED2772A911F67696644E6C77FA716DC0_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef SHAREDIALOGMODE_T40A2D9F39025821336E766D7D4FEA16006545BEB_H
#define SHAREDIALOGMODE_T40A2D9F39025821336E766D7D4FEA16006545BEB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.ShareDialogMode
struct  ShareDialogMode_t40A2D9F39025821336E766D7D4FEA16006545BEB 
{
public:
	// System.Int32 Facebook.Unity.ShareDialogMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ShareDialogMode_t40A2D9F39025821336E766D7D4FEA16006545BEB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHAREDIALOGMODE_T40A2D9F39025821336E766D7D4FEA16006545BEB_H
#ifndef ADPOSITION_T6A92A3E97DC2384E409302E35FB6EAEEE3ECF850_H
#define ADPOSITION_T6A92A3E97DC2384E409302E35FB6EAEEE3ECF850_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.Api.AdPosition
struct  AdPosition_t6A92A3E97DC2384E409302E35FB6EAEEE3ECF850 
{
public:
	// System.Int32 GoogleMobileAds.Api.AdPosition::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AdPosition_t6A92A3E97DC2384E409302E35FB6EAEEE3ECF850, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADPOSITION_T6A92A3E97DC2384E409302E35FB6EAEEE3ECF850_H
#ifndef ADAPTERSTATE_TE5ED3B9F9544763B73A0ABBEF88F2ED76381C5C9_H
#define ADAPTERSTATE_TE5ED3B9F9544763B73A0ABBEF88F2ED76381C5C9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.Api.AdapterState
struct  AdapterState_tE5ED3B9F9544763B73A0ABBEF88F2ED76381C5C9 
{
public:
	// System.Int32 GoogleMobileAds.Api.AdapterState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AdapterState_tE5ED3B9F9544763B73A0ABBEF88F2ED76381C5C9, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADAPTERSTATE_TE5ED3B9F9544763B73A0ABBEF88F2ED76381C5C9_H
#ifndef GENDER_TB13F4EC090657F9C9725E7EAEFCA6056E5C72660_H
#define GENDER_TB13F4EC090657F9C9725E7EAEFCA6056E5C72660_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.Api.Gender
struct  Gender_tB13F4EC090657F9C9725E7EAEFCA6056E5C72660 
{
public:
	// System.Int32 GoogleMobileAds.Api.Gender::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Gender_tB13F4EC090657F9C9725E7EAEFCA6056E5C72660, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENDER_TB13F4EC090657F9C9725E7EAEFCA6056E5C72660_H
#ifndef NATIVEADTYPE_TE23A725E4AFC0AFB3401D83417E936A42CB0C628_H
#define NATIVEADTYPE_TE23A725E4AFC0AFB3401D83417E936A42CB0C628_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.Api.NativeAdType
struct  NativeAdType_tE23A725E4AFC0AFB3401D83417E936A42CB0C628 
{
public:
	// System.Int32 GoogleMobileAds.Api.NativeAdType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NativeAdType_tE23A725E4AFC0AFB3401D83417E936A42CB0C628, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATIVEADTYPE_TE23A725E4AFC0AFB3401D83417E936A42CB0C628_H
#ifndef NULLABLE_1_T3290384E361396B3724B88B498CBF637D7E87B78_H
#define NULLABLE_1_T3290384E361396B3724B88B498CBF637D7E87B78_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.DateTime>
struct  Nullable_1_t3290384E361396B3724B88B498CBF637D7E87B78 
{
public:
	// T System.Nullable`1::value
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t3290384E361396B3724B88B498CBF637D7E87B78, ___value_0)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_value_0() const { return ___value_0; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t3290384E361396B3724B88B498CBF637D7E87B78, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T3290384E361396B3724B88B498CBF637D7E87B78_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef ADAPTERSTATUS_T56EFBE96CA15511B44B4FB40C70F1D113984933D_H
#define ADAPTERSTATUS_T56EFBE96CA15511B44B4FB40C70F1D113984933D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.Api.AdapterStatus
struct  AdapterStatus_t56EFBE96CA15511B44B4FB40C70F1D113984933D  : public RuntimeObject
{
public:
	// GoogleMobileAds.Api.AdapterState GoogleMobileAds.Api.AdapterStatus::<InitializationState>k__BackingField
	int32_t ___U3CInitializationStateU3Ek__BackingField_0;
	// System.String GoogleMobileAds.Api.AdapterStatus::<Description>k__BackingField
	String_t* ___U3CDescriptionU3Ek__BackingField_1;
	// System.Int32 GoogleMobileAds.Api.AdapterStatus::<Latency>k__BackingField
	int32_t ___U3CLatencyU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CInitializationStateU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AdapterStatus_t56EFBE96CA15511B44B4FB40C70F1D113984933D, ___U3CInitializationStateU3Ek__BackingField_0)); }
	inline int32_t get_U3CInitializationStateU3Ek__BackingField_0() const { return ___U3CInitializationStateU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CInitializationStateU3Ek__BackingField_0() { return &___U3CInitializationStateU3Ek__BackingField_0; }
	inline void set_U3CInitializationStateU3Ek__BackingField_0(int32_t value)
	{
		___U3CInitializationStateU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CDescriptionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AdapterStatus_t56EFBE96CA15511B44B4FB40C70F1D113984933D, ___U3CDescriptionU3Ek__BackingField_1)); }
	inline String_t* get_U3CDescriptionU3Ek__BackingField_1() const { return ___U3CDescriptionU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CDescriptionU3Ek__BackingField_1() { return &___U3CDescriptionU3Ek__BackingField_1; }
	inline void set_U3CDescriptionU3Ek__BackingField_1(String_t* value)
	{
		___U3CDescriptionU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDescriptionU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CLatencyU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AdapterStatus_t56EFBE96CA15511B44B4FB40C70F1D113984933D, ___U3CLatencyU3Ek__BackingField_2)); }
	inline int32_t get_U3CLatencyU3Ek__BackingField_2() const { return ___U3CLatencyU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CLatencyU3Ek__BackingField_2() { return &___U3CLatencyU3Ek__BackingField_2; }
	inline void set_U3CLatencyU3Ek__BackingField_2(int32_t value)
	{
		___U3CLatencyU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADAPTERSTATUS_T56EFBE96CA15511B44B4FB40C70F1D113984933D_H
#ifndef NULLABLE_1_T0E7AB3B742088C7CDDBE407A13177FC189584AEF_H
#define NULLABLE_1_T0E7AB3B742088C7CDDBE407A13177FC189584AEF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<GoogleMobileAds.Api.Gender>
struct  Nullable_1_t0E7AB3B742088C7CDDBE407A13177FC189584AEF 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t0E7AB3B742088C7CDDBE407A13177FC189584AEF, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t0E7AB3B742088C7CDDBE407A13177FC189584AEF, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T0E7AB3B742088C7CDDBE407A13177FC189584AEF_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef ADREQUEST_T98763832B122F67AC2952360B6381F4F5848306F_H
#define ADREQUEST_T98763832B122F67AC2952360B6381F4F5848306F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.Api.AdRequest
struct  AdRequest_t98763832B122F67AC2952360B6381F4F5848306F  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.String> GoogleMobileAds.Api.AdRequest::<TestDevices>k__BackingField
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___U3CTestDevicesU3Ek__BackingField_2;
	// System.Collections.Generic.HashSet`1<System.String> GoogleMobileAds.Api.AdRequest::<Keywords>k__BackingField
	HashSet_1_tAF31CD0D5B1F27E354D2DA33F8CF07F490E8C98A * ___U3CKeywordsU3Ek__BackingField_3;
	// System.Nullable`1<System.DateTime> GoogleMobileAds.Api.AdRequest::<Birthday>k__BackingField
	Nullable_1_t3290384E361396B3724B88B498CBF637D7E87B78  ___U3CBirthdayU3Ek__BackingField_4;
	// System.Nullable`1<GoogleMobileAds.Api.Gender> GoogleMobileAds.Api.AdRequest::<Gender>k__BackingField
	Nullable_1_t0E7AB3B742088C7CDDBE407A13177FC189584AEF  ___U3CGenderU3Ek__BackingField_5;
	// System.Nullable`1<System.Boolean> GoogleMobileAds.Api.AdRequest::<TagForChildDirectedTreatment>k__BackingField
	Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  ___U3CTagForChildDirectedTreatmentU3Ek__BackingField_6;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> GoogleMobileAds.Api.AdRequest::<Extras>k__BackingField
	Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * ___U3CExtrasU3Ek__BackingField_7;
	// System.Collections.Generic.List`1<GoogleMobileAds.Api.Mediation.MediationExtras> GoogleMobileAds.Api.AdRequest::<MediationExtras>k__BackingField
	List_1_t31A991A8A6D89FB1F0200E8925EADDC20ABC5811 * ___U3CMediationExtrasU3Ek__BackingField_8;

public:
	inline static int32_t get_offset_of_U3CTestDevicesU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AdRequest_t98763832B122F67AC2952360B6381F4F5848306F, ___U3CTestDevicesU3Ek__BackingField_2)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_U3CTestDevicesU3Ek__BackingField_2() const { return ___U3CTestDevicesU3Ek__BackingField_2; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_U3CTestDevicesU3Ek__BackingField_2() { return &___U3CTestDevicesU3Ek__BackingField_2; }
	inline void set_U3CTestDevicesU3Ek__BackingField_2(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___U3CTestDevicesU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTestDevicesU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CKeywordsU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AdRequest_t98763832B122F67AC2952360B6381F4F5848306F, ___U3CKeywordsU3Ek__BackingField_3)); }
	inline HashSet_1_tAF31CD0D5B1F27E354D2DA33F8CF07F490E8C98A * get_U3CKeywordsU3Ek__BackingField_3() const { return ___U3CKeywordsU3Ek__BackingField_3; }
	inline HashSet_1_tAF31CD0D5B1F27E354D2DA33F8CF07F490E8C98A ** get_address_of_U3CKeywordsU3Ek__BackingField_3() { return &___U3CKeywordsU3Ek__BackingField_3; }
	inline void set_U3CKeywordsU3Ek__BackingField_3(HashSet_1_tAF31CD0D5B1F27E354D2DA33F8CF07F490E8C98A * value)
	{
		___U3CKeywordsU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CKeywordsU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CBirthdayU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(AdRequest_t98763832B122F67AC2952360B6381F4F5848306F, ___U3CBirthdayU3Ek__BackingField_4)); }
	inline Nullable_1_t3290384E361396B3724B88B498CBF637D7E87B78  get_U3CBirthdayU3Ek__BackingField_4() const { return ___U3CBirthdayU3Ek__BackingField_4; }
	inline Nullable_1_t3290384E361396B3724B88B498CBF637D7E87B78 * get_address_of_U3CBirthdayU3Ek__BackingField_4() { return &___U3CBirthdayU3Ek__BackingField_4; }
	inline void set_U3CBirthdayU3Ek__BackingField_4(Nullable_1_t3290384E361396B3724B88B498CBF637D7E87B78  value)
	{
		___U3CBirthdayU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CGenderU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(AdRequest_t98763832B122F67AC2952360B6381F4F5848306F, ___U3CGenderU3Ek__BackingField_5)); }
	inline Nullable_1_t0E7AB3B742088C7CDDBE407A13177FC189584AEF  get_U3CGenderU3Ek__BackingField_5() const { return ___U3CGenderU3Ek__BackingField_5; }
	inline Nullable_1_t0E7AB3B742088C7CDDBE407A13177FC189584AEF * get_address_of_U3CGenderU3Ek__BackingField_5() { return &___U3CGenderU3Ek__BackingField_5; }
	inline void set_U3CGenderU3Ek__BackingField_5(Nullable_1_t0E7AB3B742088C7CDDBE407A13177FC189584AEF  value)
	{
		___U3CGenderU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CTagForChildDirectedTreatmentU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(AdRequest_t98763832B122F67AC2952360B6381F4F5848306F, ___U3CTagForChildDirectedTreatmentU3Ek__BackingField_6)); }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  get_U3CTagForChildDirectedTreatmentU3Ek__BackingField_6() const { return ___U3CTagForChildDirectedTreatmentU3Ek__BackingField_6; }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 * get_address_of_U3CTagForChildDirectedTreatmentU3Ek__BackingField_6() { return &___U3CTagForChildDirectedTreatmentU3Ek__BackingField_6; }
	inline void set_U3CTagForChildDirectedTreatmentU3Ek__BackingField_6(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  value)
	{
		___U3CTagForChildDirectedTreatmentU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CExtrasU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(AdRequest_t98763832B122F67AC2952360B6381F4F5848306F, ___U3CExtrasU3Ek__BackingField_7)); }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * get_U3CExtrasU3Ek__BackingField_7() const { return ___U3CExtrasU3Ek__BackingField_7; }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC ** get_address_of_U3CExtrasU3Ek__BackingField_7() { return &___U3CExtrasU3Ek__BackingField_7; }
	inline void set_U3CExtrasU3Ek__BackingField_7(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * value)
	{
		___U3CExtrasU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CExtrasU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CMediationExtrasU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(AdRequest_t98763832B122F67AC2952360B6381F4F5848306F, ___U3CMediationExtrasU3Ek__BackingField_8)); }
	inline List_1_t31A991A8A6D89FB1F0200E8925EADDC20ABC5811 * get_U3CMediationExtrasU3Ek__BackingField_8() const { return ___U3CMediationExtrasU3Ek__BackingField_8; }
	inline List_1_t31A991A8A6D89FB1F0200E8925EADDC20ABC5811 ** get_address_of_U3CMediationExtrasU3Ek__BackingField_8() { return &___U3CMediationExtrasU3Ek__BackingField_8; }
	inline void set_U3CMediationExtrasU3Ek__BackingField_8(List_1_t31A991A8A6D89FB1F0200E8925EADDC20ABC5811 * value)
	{
		___U3CMediationExtrasU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMediationExtrasU3Ek__BackingField_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADREQUEST_T98763832B122F67AC2952360B6381F4F5848306F_H
#ifndef BUILDER_T2384CCFEB139CFC834887DBD4754905ABFE8F2E9_H
#define BUILDER_T2384CCFEB139CFC834887DBD4754905ABFE8F2E9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.Api.AdRequest_Builder
struct  Builder_t2384CCFEB139CFC834887DBD4754905ABFE8F2E9  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.String> GoogleMobileAds.Api.AdRequest_Builder::<TestDevices>k__BackingField
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___U3CTestDevicesU3Ek__BackingField_0;
	// System.Collections.Generic.HashSet`1<System.String> GoogleMobileAds.Api.AdRequest_Builder::<Keywords>k__BackingField
	HashSet_1_tAF31CD0D5B1F27E354D2DA33F8CF07F490E8C98A * ___U3CKeywordsU3Ek__BackingField_1;
	// System.Nullable`1<System.DateTime> GoogleMobileAds.Api.AdRequest_Builder::<Birthday>k__BackingField
	Nullable_1_t3290384E361396B3724B88B498CBF637D7E87B78  ___U3CBirthdayU3Ek__BackingField_2;
	// System.Nullable`1<GoogleMobileAds.Api.Gender> GoogleMobileAds.Api.AdRequest_Builder::<Gender>k__BackingField
	Nullable_1_t0E7AB3B742088C7CDDBE407A13177FC189584AEF  ___U3CGenderU3Ek__BackingField_3;
	// System.Nullable`1<System.Boolean> GoogleMobileAds.Api.AdRequest_Builder::<ChildDirectedTreatmentTag>k__BackingField
	Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  ___U3CChildDirectedTreatmentTagU3Ek__BackingField_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> GoogleMobileAds.Api.AdRequest_Builder::<Extras>k__BackingField
	Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * ___U3CExtrasU3Ek__BackingField_5;
	// System.Collections.Generic.List`1<GoogleMobileAds.Api.Mediation.MediationExtras> GoogleMobileAds.Api.AdRequest_Builder::<MediationExtras>k__BackingField
	List_1_t31A991A8A6D89FB1F0200E8925EADDC20ABC5811 * ___U3CMediationExtrasU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CTestDevicesU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Builder_t2384CCFEB139CFC834887DBD4754905ABFE8F2E9, ___U3CTestDevicesU3Ek__BackingField_0)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_U3CTestDevicesU3Ek__BackingField_0() const { return ___U3CTestDevicesU3Ek__BackingField_0; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_U3CTestDevicesU3Ek__BackingField_0() { return &___U3CTestDevicesU3Ek__BackingField_0; }
	inline void set_U3CTestDevicesU3Ek__BackingField_0(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___U3CTestDevicesU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTestDevicesU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CKeywordsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Builder_t2384CCFEB139CFC834887DBD4754905ABFE8F2E9, ___U3CKeywordsU3Ek__BackingField_1)); }
	inline HashSet_1_tAF31CD0D5B1F27E354D2DA33F8CF07F490E8C98A * get_U3CKeywordsU3Ek__BackingField_1() const { return ___U3CKeywordsU3Ek__BackingField_1; }
	inline HashSet_1_tAF31CD0D5B1F27E354D2DA33F8CF07F490E8C98A ** get_address_of_U3CKeywordsU3Ek__BackingField_1() { return &___U3CKeywordsU3Ek__BackingField_1; }
	inline void set_U3CKeywordsU3Ek__BackingField_1(HashSet_1_tAF31CD0D5B1F27E354D2DA33F8CF07F490E8C98A * value)
	{
		___U3CKeywordsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CKeywordsU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CBirthdayU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Builder_t2384CCFEB139CFC834887DBD4754905ABFE8F2E9, ___U3CBirthdayU3Ek__BackingField_2)); }
	inline Nullable_1_t3290384E361396B3724B88B498CBF637D7E87B78  get_U3CBirthdayU3Ek__BackingField_2() const { return ___U3CBirthdayU3Ek__BackingField_2; }
	inline Nullable_1_t3290384E361396B3724B88B498CBF637D7E87B78 * get_address_of_U3CBirthdayU3Ek__BackingField_2() { return &___U3CBirthdayU3Ek__BackingField_2; }
	inline void set_U3CBirthdayU3Ek__BackingField_2(Nullable_1_t3290384E361396B3724B88B498CBF637D7E87B78  value)
	{
		___U3CBirthdayU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CGenderU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Builder_t2384CCFEB139CFC834887DBD4754905ABFE8F2E9, ___U3CGenderU3Ek__BackingField_3)); }
	inline Nullable_1_t0E7AB3B742088C7CDDBE407A13177FC189584AEF  get_U3CGenderU3Ek__BackingField_3() const { return ___U3CGenderU3Ek__BackingField_3; }
	inline Nullable_1_t0E7AB3B742088C7CDDBE407A13177FC189584AEF * get_address_of_U3CGenderU3Ek__BackingField_3() { return &___U3CGenderU3Ek__BackingField_3; }
	inline void set_U3CGenderU3Ek__BackingField_3(Nullable_1_t0E7AB3B742088C7CDDBE407A13177FC189584AEF  value)
	{
		___U3CGenderU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CChildDirectedTreatmentTagU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Builder_t2384CCFEB139CFC834887DBD4754905ABFE8F2E9, ___U3CChildDirectedTreatmentTagU3Ek__BackingField_4)); }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  get_U3CChildDirectedTreatmentTagU3Ek__BackingField_4() const { return ___U3CChildDirectedTreatmentTagU3Ek__BackingField_4; }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 * get_address_of_U3CChildDirectedTreatmentTagU3Ek__BackingField_4() { return &___U3CChildDirectedTreatmentTagU3Ek__BackingField_4; }
	inline void set_U3CChildDirectedTreatmentTagU3Ek__BackingField_4(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  value)
	{
		___U3CChildDirectedTreatmentTagU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CExtrasU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Builder_t2384CCFEB139CFC834887DBD4754905ABFE8F2E9, ___U3CExtrasU3Ek__BackingField_5)); }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * get_U3CExtrasU3Ek__BackingField_5() const { return ___U3CExtrasU3Ek__BackingField_5; }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC ** get_address_of_U3CExtrasU3Ek__BackingField_5() { return &___U3CExtrasU3Ek__BackingField_5; }
	inline void set_U3CExtrasU3Ek__BackingField_5(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * value)
	{
		___U3CExtrasU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CExtrasU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CMediationExtrasU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Builder_t2384CCFEB139CFC834887DBD4754905ABFE8F2E9, ___U3CMediationExtrasU3Ek__BackingField_6)); }
	inline List_1_t31A991A8A6D89FB1F0200E8925EADDC20ABC5811 * get_U3CMediationExtrasU3Ek__BackingField_6() const { return ___U3CMediationExtrasU3Ek__BackingField_6; }
	inline List_1_t31A991A8A6D89FB1F0200E8925EADDC20ABC5811 ** get_address_of_U3CMediationExtrasU3Ek__BackingField_6() { return &___U3CMediationExtrasU3Ek__BackingField_6; }
	inline void set_U3CMediationExtrasU3Ek__BackingField_6(List_1_t31A991A8A6D89FB1F0200E8925EADDC20ABC5811 * value)
	{
		___U3CMediationExtrasU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMediationExtrasU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILDER_T2384CCFEB139CFC834887DBD4754905ABFE8F2E9_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef CONSOLEBASE_T80F55AC34D95221101149083BAB6EBF9571E72B4_H
#define CONSOLEBASE_T80F55AC34D95221101149083BAB6EBF9571E72B4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Example.ConsoleBase
struct  ConsoleBase_t80F55AC34D95221101149083BAB6EBF9571E72B4  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String Facebook.Unity.Example.ConsoleBase::status
	String_t* ___status_6;
	// System.String Facebook.Unity.Example.ConsoleBase::lastResponse
	String_t* ___lastResponse_7;
	// UnityEngine.Vector2 Facebook.Unity.Example.ConsoleBase::scrollPosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___scrollPosition_8;
	// System.Nullable`1<System.Single> Facebook.Unity.Example.ConsoleBase::scaleFactor
	Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0  ___scaleFactor_9;
	// UnityEngine.GUIStyle Facebook.Unity.Example.ConsoleBase::textStyle
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___textStyle_10;
	// UnityEngine.GUIStyle Facebook.Unity.Example.ConsoleBase::buttonStyle
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___buttonStyle_11;
	// UnityEngine.GUIStyle Facebook.Unity.Example.ConsoleBase::textInputStyle
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___textInputStyle_12;
	// UnityEngine.GUIStyle Facebook.Unity.Example.ConsoleBase::labelStyle
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___labelStyle_13;
	// UnityEngine.Texture2D Facebook.Unity.Example.ConsoleBase::<LastResponseTexture>k__BackingField
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___U3CLastResponseTextureU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of_status_6() { return static_cast<int32_t>(offsetof(ConsoleBase_t80F55AC34D95221101149083BAB6EBF9571E72B4, ___status_6)); }
	inline String_t* get_status_6() const { return ___status_6; }
	inline String_t** get_address_of_status_6() { return &___status_6; }
	inline void set_status_6(String_t* value)
	{
		___status_6 = value;
		Il2CppCodeGenWriteBarrier((&___status_6), value);
	}

	inline static int32_t get_offset_of_lastResponse_7() { return static_cast<int32_t>(offsetof(ConsoleBase_t80F55AC34D95221101149083BAB6EBF9571E72B4, ___lastResponse_7)); }
	inline String_t* get_lastResponse_7() const { return ___lastResponse_7; }
	inline String_t** get_address_of_lastResponse_7() { return &___lastResponse_7; }
	inline void set_lastResponse_7(String_t* value)
	{
		___lastResponse_7 = value;
		Il2CppCodeGenWriteBarrier((&___lastResponse_7), value);
	}

	inline static int32_t get_offset_of_scrollPosition_8() { return static_cast<int32_t>(offsetof(ConsoleBase_t80F55AC34D95221101149083BAB6EBF9571E72B4, ___scrollPosition_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_scrollPosition_8() const { return ___scrollPosition_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_scrollPosition_8() { return &___scrollPosition_8; }
	inline void set_scrollPosition_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___scrollPosition_8 = value;
	}

	inline static int32_t get_offset_of_scaleFactor_9() { return static_cast<int32_t>(offsetof(ConsoleBase_t80F55AC34D95221101149083BAB6EBF9571E72B4, ___scaleFactor_9)); }
	inline Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0  get_scaleFactor_9() const { return ___scaleFactor_9; }
	inline Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0 * get_address_of_scaleFactor_9() { return &___scaleFactor_9; }
	inline void set_scaleFactor_9(Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0  value)
	{
		___scaleFactor_9 = value;
	}

	inline static int32_t get_offset_of_textStyle_10() { return static_cast<int32_t>(offsetof(ConsoleBase_t80F55AC34D95221101149083BAB6EBF9571E72B4, ___textStyle_10)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_textStyle_10() const { return ___textStyle_10; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_textStyle_10() { return &___textStyle_10; }
	inline void set_textStyle_10(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___textStyle_10 = value;
		Il2CppCodeGenWriteBarrier((&___textStyle_10), value);
	}

	inline static int32_t get_offset_of_buttonStyle_11() { return static_cast<int32_t>(offsetof(ConsoleBase_t80F55AC34D95221101149083BAB6EBF9571E72B4, ___buttonStyle_11)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_buttonStyle_11() const { return ___buttonStyle_11; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_buttonStyle_11() { return &___buttonStyle_11; }
	inline void set_buttonStyle_11(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___buttonStyle_11 = value;
		Il2CppCodeGenWriteBarrier((&___buttonStyle_11), value);
	}

	inline static int32_t get_offset_of_textInputStyle_12() { return static_cast<int32_t>(offsetof(ConsoleBase_t80F55AC34D95221101149083BAB6EBF9571E72B4, ___textInputStyle_12)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_textInputStyle_12() const { return ___textInputStyle_12; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_textInputStyle_12() { return &___textInputStyle_12; }
	inline void set_textInputStyle_12(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___textInputStyle_12 = value;
		Il2CppCodeGenWriteBarrier((&___textInputStyle_12), value);
	}

	inline static int32_t get_offset_of_labelStyle_13() { return static_cast<int32_t>(offsetof(ConsoleBase_t80F55AC34D95221101149083BAB6EBF9571E72B4, ___labelStyle_13)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_labelStyle_13() const { return ___labelStyle_13; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_labelStyle_13() { return &___labelStyle_13; }
	inline void set_labelStyle_13(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___labelStyle_13 = value;
		Il2CppCodeGenWriteBarrier((&___labelStyle_13), value);
	}

	inline static int32_t get_offset_of_U3CLastResponseTextureU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(ConsoleBase_t80F55AC34D95221101149083BAB6EBF9571E72B4, ___U3CLastResponseTextureU3Ek__BackingField_14)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_U3CLastResponseTextureU3Ek__BackingField_14() const { return ___U3CLastResponseTextureU3Ek__BackingField_14; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_U3CLastResponseTextureU3Ek__BackingField_14() { return &___U3CLastResponseTextureU3Ek__BackingField_14; }
	inline void set_U3CLastResponseTextureU3Ek__BackingField_14(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___U3CLastResponseTextureU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLastResponseTextureU3Ek__BackingField_14), value);
	}
};

struct ConsoleBase_t80F55AC34D95221101149083BAB6EBF9571E72B4_StaticFields
{
public:
	// System.Collections.Generic.Stack`1<System.String> Facebook.Unity.Example.ConsoleBase::menuStack
	Stack_1_t13ADD3EDE9022020A434D32784D651F9FE027067 * ___menuStack_5;

public:
	inline static int32_t get_offset_of_menuStack_5() { return static_cast<int32_t>(offsetof(ConsoleBase_t80F55AC34D95221101149083BAB6EBF9571E72B4_StaticFields, ___menuStack_5)); }
	inline Stack_1_t13ADD3EDE9022020A434D32784D651F9FE027067 * get_menuStack_5() const { return ___menuStack_5; }
	inline Stack_1_t13ADD3EDE9022020A434D32784D651F9FE027067 ** get_address_of_menuStack_5() { return &___menuStack_5; }
	inline void set_menuStack_5(Stack_1_t13ADD3EDE9022020A434D32784D651F9FE027067 * value)
	{
		___menuStack_5 = value;
		Il2CppCodeGenWriteBarrier((&___menuStack_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSOLEBASE_T80F55AC34D95221101149083BAB6EBF9571E72B4_H
#ifndef MOBILEADSEVENTEXECUTOR_TBB365EB01588F8B60DA48EC9A7BE0DCF0DB784AA_H
#define MOBILEADSEVENTEXECUTOR_TBB365EB01588F8B60DA48EC9A7BE0DCF0DB784AA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.Common.MobileAdsEventExecutor
struct  MobileAdsEventExecutor_tBB365EB01588F8B60DA48EC9A7BE0DCF0DB784AA  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct MobileAdsEventExecutor_tBB365EB01588F8B60DA48EC9A7BE0DCF0DB784AA_StaticFields
{
public:
	// GoogleMobileAds.Common.MobileAdsEventExecutor GoogleMobileAds.Common.MobileAdsEventExecutor::instance
	MobileAdsEventExecutor_tBB365EB01588F8B60DA48EC9A7BE0DCF0DB784AA * ___instance_4;
	// System.Collections.Generic.List`1<System.Action> GoogleMobileAds.Common.MobileAdsEventExecutor::adEventsQueue
	List_1_t9E7F53A878191B703A76E69CE36AA90A372800E7 * ___adEventsQueue_5;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) GoogleMobileAds.Common.MobileAdsEventExecutor::adEventsQueueEmpty
	bool ___adEventsQueueEmpty_6;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(MobileAdsEventExecutor_tBB365EB01588F8B60DA48EC9A7BE0DCF0DB784AA_StaticFields, ___instance_4)); }
	inline MobileAdsEventExecutor_tBB365EB01588F8B60DA48EC9A7BE0DCF0DB784AA * get_instance_4() const { return ___instance_4; }
	inline MobileAdsEventExecutor_tBB365EB01588F8B60DA48EC9A7BE0DCF0DB784AA ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(MobileAdsEventExecutor_tBB365EB01588F8B60DA48EC9A7BE0DCF0DB784AA * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___instance_4), value);
	}

	inline static int32_t get_offset_of_adEventsQueue_5() { return static_cast<int32_t>(offsetof(MobileAdsEventExecutor_tBB365EB01588F8B60DA48EC9A7BE0DCF0DB784AA_StaticFields, ___adEventsQueue_5)); }
	inline List_1_t9E7F53A878191B703A76E69CE36AA90A372800E7 * get_adEventsQueue_5() const { return ___adEventsQueue_5; }
	inline List_1_t9E7F53A878191B703A76E69CE36AA90A372800E7 ** get_address_of_adEventsQueue_5() { return &___adEventsQueue_5; }
	inline void set_adEventsQueue_5(List_1_t9E7F53A878191B703A76E69CE36AA90A372800E7 * value)
	{
		___adEventsQueue_5 = value;
		Il2CppCodeGenWriteBarrier((&___adEventsQueue_5), value);
	}

	inline static int32_t get_offset_of_adEventsQueueEmpty_6() { return static_cast<int32_t>(offsetof(MobileAdsEventExecutor_tBB365EB01588F8B60DA48EC9A7BE0DCF0DB784AA_StaticFields, ___adEventsQueueEmpty_6)); }
	inline bool get_adEventsQueueEmpty_6() const { return ___adEventsQueueEmpty_6; }
	inline bool* get_address_of_adEventsQueueEmpty_6() { return &___adEventsQueueEmpty_6; }
	inline void set_adEventsQueueEmpty_6(bool value)
	{
		___adEventsQueueEmpty_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOBILEADSEVENTEXECUTOR_TBB365EB01588F8B60DA48EC9A7BE0DCF0DB784AA_H
#ifndef LOGVIEW_T81FB093AA3DB55ED7D8B97F6BAD8C6BCEFC8DF7A_H
#define LOGVIEW_T81FB093AA3DB55ED7D8B97F6BAD8C6BCEFC8DF7A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Example.LogView
struct  LogView_t81FB093AA3DB55ED7D8B97F6BAD8C6BCEFC8DF7A  : public ConsoleBase_t80F55AC34D95221101149083BAB6EBF9571E72B4
{
public:

public:
};

struct LogView_t81FB093AA3DB55ED7D8B97F6BAD8C6BCEFC8DF7A_StaticFields
{
public:
	// System.String Facebook.Unity.Example.LogView::datePatt
	String_t* ___datePatt_15;
	// System.Collections.Generic.IList`1<System.String> Facebook.Unity.Example.LogView::events
	RuntimeObject* ___events_16;

public:
	inline static int32_t get_offset_of_datePatt_15() { return static_cast<int32_t>(offsetof(LogView_t81FB093AA3DB55ED7D8B97F6BAD8C6BCEFC8DF7A_StaticFields, ___datePatt_15)); }
	inline String_t* get_datePatt_15() const { return ___datePatt_15; }
	inline String_t** get_address_of_datePatt_15() { return &___datePatt_15; }
	inline void set_datePatt_15(String_t* value)
	{
		___datePatt_15 = value;
		Il2CppCodeGenWriteBarrier((&___datePatt_15), value);
	}

	inline static int32_t get_offset_of_events_16() { return static_cast<int32_t>(offsetof(LogView_t81FB093AA3DB55ED7D8B97F6BAD8C6BCEFC8DF7A_StaticFields, ___events_16)); }
	inline RuntimeObject* get_events_16() const { return ___events_16; }
	inline RuntimeObject** get_address_of_events_16() { return &___events_16; }
	inline void set_events_16(RuntimeObject* value)
	{
		___events_16 = value;
		Il2CppCodeGenWriteBarrier((&___events_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGVIEW_T81FB093AA3DB55ED7D8B97F6BAD8C6BCEFC8DF7A_H
#ifndef MENUBASE_TA4B32275F482BF1B58E859A78BF074D13FF3E93A_H
#define MENUBASE_TA4B32275F482BF1B58E859A78BF074D13FF3E93A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Example.MenuBase
struct  MenuBase_tA4B32275F482BF1B58E859A78BF074D13FF3E93A  : public ConsoleBase_t80F55AC34D95221101149083BAB6EBF9571E72B4
{
public:

public:
};

struct MenuBase_tA4B32275F482BF1B58E859A78BF074D13FF3E93A_StaticFields
{
public:
	// Facebook.Unity.ShareDialogMode Facebook.Unity.Example.MenuBase::shareDialogMode
	int32_t ___shareDialogMode_15;

public:
	inline static int32_t get_offset_of_shareDialogMode_15() { return static_cast<int32_t>(offsetof(MenuBase_tA4B32275F482BF1B58E859A78BF074D13FF3E93A_StaticFields, ___shareDialogMode_15)); }
	inline int32_t get_shareDialogMode_15() const { return ___shareDialogMode_15; }
	inline int32_t* get_address_of_shareDialogMode_15() { return &___shareDialogMode_15; }
	inline void set_shareDialogMode_15(int32_t value)
	{
		___shareDialogMode_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MENUBASE_TA4B32275F482BF1B58E859A78BF074D13FF3E93A_H
#ifndef ACCESSTOKENMENU_TA169D1DDBAD90D514BE1C0BA14FA7FAC3AA5542D_H
#define ACCESSTOKENMENU_TA169D1DDBAD90D514BE1C0BA14FA7FAC3AA5542D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Example.AccessTokenMenu
struct  AccessTokenMenu_tA169D1DDBAD90D514BE1C0BA14FA7FAC3AA5542D  : public MenuBase_tA4B32275F482BF1B58E859A78BF074D13FF3E93A
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACCESSTOKENMENU_TA169D1DDBAD90D514BE1C0BA14FA7FAC3AA5542D_H
#ifndef APPEVENTS_T20C76E739C9ABC6392F335DCF9902C17C87D6B70_H
#define APPEVENTS_T20C76E739C9ABC6392F335DCF9902C17C87D6B70_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Example.AppEvents
struct  AppEvents_t20C76E739C9ABC6392F335DCF9902C17C87D6B70  : public MenuBase_tA4B32275F482BF1B58E859A78BF074D13FF3E93A
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPEVENTS_T20C76E739C9ABC6392F335DCF9902C17C87D6B70_H
#ifndef APPINVITES_T20590921C9CC4B49A844AB27042A146CA6903B0C_H
#define APPINVITES_T20590921C9CC4B49A844AB27042A146CA6903B0C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Example.AppInvites
struct  AppInvites_t20590921C9CC4B49A844AB27042A146CA6903B0C  : public MenuBase_tA4B32275F482BF1B58E859A78BF074D13FF3E93A
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPINVITES_T20590921C9CC4B49A844AB27042A146CA6903B0C_H
#ifndef APPLINKS_T0D8BC945EC82E808B1CD4748D540A69D2970913B_H
#define APPLINKS_T0D8BC945EC82E808B1CD4748D540A69D2970913B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Example.AppLinks
struct  AppLinks_t0D8BC945EC82E808B1CD4748D540A69D2970913B  : public MenuBase_tA4B32275F482BF1B58E859A78BF074D13FF3E93A
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPLINKS_T0D8BC945EC82E808B1CD4748D540A69D2970913B_H
#ifndef APPREQUESTS_T22F4C53E63A0E4C23359B3D16D9CDF957A60EEA3_H
#define APPREQUESTS_T22F4C53E63A0E4C23359B3D16D9CDF957A60EEA3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Example.AppRequests
struct  AppRequests_t22F4C53E63A0E4C23359B3D16D9CDF957A60EEA3  : public MenuBase_tA4B32275F482BF1B58E859A78BF074D13FF3E93A
{
public:
	// System.String Facebook.Unity.Example.AppRequests::requestMessage
	String_t* ___requestMessage_16;
	// System.String Facebook.Unity.Example.AppRequests::requestTo
	String_t* ___requestTo_17;
	// System.String Facebook.Unity.Example.AppRequests::requestFilter
	String_t* ___requestFilter_18;
	// System.String Facebook.Unity.Example.AppRequests::requestExcludes
	String_t* ___requestExcludes_19;
	// System.String Facebook.Unity.Example.AppRequests::requestMax
	String_t* ___requestMax_20;
	// System.String Facebook.Unity.Example.AppRequests::requestData
	String_t* ___requestData_21;
	// System.String Facebook.Unity.Example.AppRequests::requestTitle
	String_t* ___requestTitle_22;
	// System.String Facebook.Unity.Example.AppRequests::requestObjectID
	String_t* ___requestObjectID_23;
	// System.Int32 Facebook.Unity.Example.AppRequests::selectedAction
	int32_t ___selectedAction_24;
	// System.String[] Facebook.Unity.Example.AppRequests::actionTypeStrings
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___actionTypeStrings_25;

public:
	inline static int32_t get_offset_of_requestMessage_16() { return static_cast<int32_t>(offsetof(AppRequests_t22F4C53E63A0E4C23359B3D16D9CDF957A60EEA3, ___requestMessage_16)); }
	inline String_t* get_requestMessage_16() const { return ___requestMessage_16; }
	inline String_t** get_address_of_requestMessage_16() { return &___requestMessage_16; }
	inline void set_requestMessage_16(String_t* value)
	{
		___requestMessage_16 = value;
		Il2CppCodeGenWriteBarrier((&___requestMessage_16), value);
	}

	inline static int32_t get_offset_of_requestTo_17() { return static_cast<int32_t>(offsetof(AppRequests_t22F4C53E63A0E4C23359B3D16D9CDF957A60EEA3, ___requestTo_17)); }
	inline String_t* get_requestTo_17() const { return ___requestTo_17; }
	inline String_t** get_address_of_requestTo_17() { return &___requestTo_17; }
	inline void set_requestTo_17(String_t* value)
	{
		___requestTo_17 = value;
		Il2CppCodeGenWriteBarrier((&___requestTo_17), value);
	}

	inline static int32_t get_offset_of_requestFilter_18() { return static_cast<int32_t>(offsetof(AppRequests_t22F4C53E63A0E4C23359B3D16D9CDF957A60EEA3, ___requestFilter_18)); }
	inline String_t* get_requestFilter_18() const { return ___requestFilter_18; }
	inline String_t** get_address_of_requestFilter_18() { return &___requestFilter_18; }
	inline void set_requestFilter_18(String_t* value)
	{
		___requestFilter_18 = value;
		Il2CppCodeGenWriteBarrier((&___requestFilter_18), value);
	}

	inline static int32_t get_offset_of_requestExcludes_19() { return static_cast<int32_t>(offsetof(AppRequests_t22F4C53E63A0E4C23359B3D16D9CDF957A60EEA3, ___requestExcludes_19)); }
	inline String_t* get_requestExcludes_19() const { return ___requestExcludes_19; }
	inline String_t** get_address_of_requestExcludes_19() { return &___requestExcludes_19; }
	inline void set_requestExcludes_19(String_t* value)
	{
		___requestExcludes_19 = value;
		Il2CppCodeGenWriteBarrier((&___requestExcludes_19), value);
	}

	inline static int32_t get_offset_of_requestMax_20() { return static_cast<int32_t>(offsetof(AppRequests_t22F4C53E63A0E4C23359B3D16D9CDF957A60EEA3, ___requestMax_20)); }
	inline String_t* get_requestMax_20() const { return ___requestMax_20; }
	inline String_t** get_address_of_requestMax_20() { return &___requestMax_20; }
	inline void set_requestMax_20(String_t* value)
	{
		___requestMax_20 = value;
		Il2CppCodeGenWriteBarrier((&___requestMax_20), value);
	}

	inline static int32_t get_offset_of_requestData_21() { return static_cast<int32_t>(offsetof(AppRequests_t22F4C53E63A0E4C23359B3D16D9CDF957A60EEA3, ___requestData_21)); }
	inline String_t* get_requestData_21() const { return ___requestData_21; }
	inline String_t** get_address_of_requestData_21() { return &___requestData_21; }
	inline void set_requestData_21(String_t* value)
	{
		___requestData_21 = value;
		Il2CppCodeGenWriteBarrier((&___requestData_21), value);
	}

	inline static int32_t get_offset_of_requestTitle_22() { return static_cast<int32_t>(offsetof(AppRequests_t22F4C53E63A0E4C23359B3D16D9CDF957A60EEA3, ___requestTitle_22)); }
	inline String_t* get_requestTitle_22() const { return ___requestTitle_22; }
	inline String_t** get_address_of_requestTitle_22() { return &___requestTitle_22; }
	inline void set_requestTitle_22(String_t* value)
	{
		___requestTitle_22 = value;
		Il2CppCodeGenWriteBarrier((&___requestTitle_22), value);
	}

	inline static int32_t get_offset_of_requestObjectID_23() { return static_cast<int32_t>(offsetof(AppRequests_t22F4C53E63A0E4C23359B3D16D9CDF957A60EEA3, ___requestObjectID_23)); }
	inline String_t* get_requestObjectID_23() const { return ___requestObjectID_23; }
	inline String_t** get_address_of_requestObjectID_23() { return &___requestObjectID_23; }
	inline void set_requestObjectID_23(String_t* value)
	{
		___requestObjectID_23 = value;
		Il2CppCodeGenWriteBarrier((&___requestObjectID_23), value);
	}

	inline static int32_t get_offset_of_selectedAction_24() { return static_cast<int32_t>(offsetof(AppRequests_t22F4C53E63A0E4C23359B3D16D9CDF957A60EEA3, ___selectedAction_24)); }
	inline int32_t get_selectedAction_24() const { return ___selectedAction_24; }
	inline int32_t* get_address_of_selectedAction_24() { return &___selectedAction_24; }
	inline void set_selectedAction_24(int32_t value)
	{
		___selectedAction_24 = value;
	}

	inline static int32_t get_offset_of_actionTypeStrings_25() { return static_cast<int32_t>(offsetof(AppRequests_t22F4C53E63A0E4C23359B3D16D9CDF957A60EEA3, ___actionTypeStrings_25)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_actionTypeStrings_25() const { return ___actionTypeStrings_25; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_actionTypeStrings_25() { return &___actionTypeStrings_25; }
	inline void set_actionTypeStrings_25(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___actionTypeStrings_25 = value;
		Il2CppCodeGenWriteBarrier((&___actionTypeStrings_25), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPREQUESTS_T22F4C53E63A0E4C23359B3D16D9CDF957A60EEA3_H
#ifndef DIALOGSHARE_T055DE5CF84315A9A01E6ABFEE8F136DFB778EBC0_H
#define DIALOGSHARE_T055DE5CF84315A9A01E6ABFEE8F136DFB778EBC0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Example.DialogShare
struct  DialogShare_t055DE5CF84315A9A01E6ABFEE8F136DFB778EBC0  : public MenuBase_tA4B32275F482BF1B58E859A78BF074D13FF3E93A
{
public:
	// System.String Facebook.Unity.Example.DialogShare::shareLink
	String_t* ___shareLink_16;
	// System.String Facebook.Unity.Example.DialogShare::shareTitle
	String_t* ___shareTitle_17;
	// System.String Facebook.Unity.Example.DialogShare::shareDescription
	String_t* ___shareDescription_18;
	// System.String Facebook.Unity.Example.DialogShare::shareImage
	String_t* ___shareImage_19;
	// System.String Facebook.Unity.Example.DialogShare::feedTo
	String_t* ___feedTo_20;
	// System.String Facebook.Unity.Example.DialogShare::feedLink
	String_t* ___feedLink_21;
	// System.String Facebook.Unity.Example.DialogShare::feedTitle
	String_t* ___feedTitle_22;
	// System.String Facebook.Unity.Example.DialogShare::feedCaption
	String_t* ___feedCaption_23;
	// System.String Facebook.Unity.Example.DialogShare::feedDescription
	String_t* ___feedDescription_24;
	// System.String Facebook.Unity.Example.DialogShare::feedImage
	String_t* ___feedImage_25;
	// System.String Facebook.Unity.Example.DialogShare::feedMediaSource
	String_t* ___feedMediaSource_26;

public:
	inline static int32_t get_offset_of_shareLink_16() { return static_cast<int32_t>(offsetof(DialogShare_t055DE5CF84315A9A01E6ABFEE8F136DFB778EBC0, ___shareLink_16)); }
	inline String_t* get_shareLink_16() const { return ___shareLink_16; }
	inline String_t** get_address_of_shareLink_16() { return &___shareLink_16; }
	inline void set_shareLink_16(String_t* value)
	{
		___shareLink_16 = value;
		Il2CppCodeGenWriteBarrier((&___shareLink_16), value);
	}

	inline static int32_t get_offset_of_shareTitle_17() { return static_cast<int32_t>(offsetof(DialogShare_t055DE5CF84315A9A01E6ABFEE8F136DFB778EBC0, ___shareTitle_17)); }
	inline String_t* get_shareTitle_17() const { return ___shareTitle_17; }
	inline String_t** get_address_of_shareTitle_17() { return &___shareTitle_17; }
	inline void set_shareTitle_17(String_t* value)
	{
		___shareTitle_17 = value;
		Il2CppCodeGenWriteBarrier((&___shareTitle_17), value);
	}

	inline static int32_t get_offset_of_shareDescription_18() { return static_cast<int32_t>(offsetof(DialogShare_t055DE5CF84315A9A01E6ABFEE8F136DFB778EBC0, ___shareDescription_18)); }
	inline String_t* get_shareDescription_18() const { return ___shareDescription_18; }
	inline String_t** get_address_of_shareDescription_18() { return &___shareDescription_18; }
	inline void set_shareDescription_18(String_t* value)
	{
		___shareDescription_18 = value;
		Il2CppCodeGenWriteBarrier((&___shareDescription_18), value);
	}

	inline static int32_t get_offset_of_shareImage_19() { return static_cast<int32_t>(offsetof(DialogShare_t055DE5CF84315A9A01E6ABFEE8F136DFB778EBC0, ___shareImage_19)); }
	inline String_t* get_shareImage_19() const { return ___shareImage_19; }
	inline String_t** get_address_of_shareImage_19() { return &___shareImage_19; }
	inline void set_shareImage_19(String_t* value)
	{
		___shareImage_19 = value;
		Il2CppCodeGenWriteBarrier((&___shareImage_19), value);
	}

	inline static int32_t get_offset_of_feedTo_20() { return static_cast<int32_t>(offsetof(DialogShare_t055DE5CF84315A9A01E6ABFEE8F136DFB778EBC0, ___feedTo_20)); }
	inline String_t* get_feedTo_20() const { return ___feedTo_20; }
	inline String_t** get_address_of_feedTo_20() { return &___feedTo_20; }
	inline void set_feedTo_20(String_t* value)
	{
		___feedTo_20 = value;
		Il2CppCodeGenWriteBarrier((&___feedTo_20), value);
	}

	inline static int32_t get_offset_of_feedLink_21() { return static_cast<int32_t>(offsetof(DialogShare_t055DE5CF84315A9A01E6ABFEE8F136DFB778EBC0, ___feedLink_21)); }
	inline String_t* get_feedLink_21() const { return ___feedLink_21; }
	inline String_t** get_address_of_feedLink_21() { return &___feedLink_21; }
	inline void set_feedLink_21(String_t* value)
	{
		___feedLink_21 = value;
		Il2CppCodeGenWriteBarrier((&___feedLink_21), value);
	}

	inline static int32_t get_offset_of_feedTitle_22() { return static_cast<int32_t>(offsetof(DialogShare_t055DE5CF84315A9A01E6ABFEE8F136DFB778EBC0, ___feedTitle_22)); }
	inline String_t* get_feedTitle_22() const { return ___feedTitle_22; }
	inline String_t** get_address_of_feedTitle_22() { return &___feedTitle_22; }
	inline void set_feedTitle_22(String_t* value)
	{
		___feedTitle_22 = value;
		Il2CppCodeGenWriteBarrier((&___feedTitle_22), value);
	}

	inline static int32_t get_offset_of_feedCaption_23() { return static_cast<int32_t>(offsetof(DialogShare_t055DE5CF84315A9A01E6ABFEE8F136DFB778EBC0, ___feedCaption_23)); }
	inline String_t* get_feedCaption_23() const { return ___feedCaption_23; }
	inline String_t** get_address_of_feedCaption_23() { return &___feedCaption_23; }
	inline void set_feedCaption_23(String_t* value)
	{
		___feedCaption_23 = value;
		Il2CppCodeGenWriteBarrier((&___feedCaption_23), value);
	}

	inline static int32_t get_offset_of_feedDescription_24() { return static_cast<int32_t>(offsetof(DialogShare_t055DE5CF84315A9A01E6ABFEE8F136DFB778EBC0, ___feedDescription_24)); }
	inline String_t* get_feedDescription_24() const { return ___feedDescription_24; }
	inline String_t** get_address_of_feedDescription_24() { return &___feedDescription_24; }
	inline void set_feedDescription_24(String_t* value)
	{
		___feedDescription_24 = value;
		Il2CppCodeGenWriteBarrier((&___feedDescription_24), value);
	}

	inline static int32_t get_offset_of_feedImage_25() { return static_cast<int32_t>(offsetof(DialogShare_t055DE5CF84315A9A01E6ABFEE8F136DFB778EBC0, ___feedImage_25)); }
	inline String_t* get_feedImage_25() const { return ___feedImage_25; }
	inline String_t** get_address_of_feedImage_25() { return &___feedImage_25; }
	inline void set_feedImage_25(String_t* value)
	{
		___feedImage_25 = value;
		Il2CppCodeGenWriteBarrier((&___feedImage_25), value);
	}

	inline static int32_t get_offset_of_feedMediaSource_26() { return static_cast<int32_t>(offsetof(DialogShare_t055DE5CF84315A9A01E6ABFEE8F136DFB778EBC0, ___feedMediaSource_26)); }
	inline String_t* get_feedMediaSource_26() const { return ___feedMediaSource_26; }
	inline String_t** get_address_of_feedMediaSource_26() { return &___feedMediaSource_26; }
	inline void set_feedMediaSource_26(String_t* value)
	{
		___feedMediaSource_26 = value;
		Il2CppCodeGenWriteBarrier((&___feedMediaSource_26), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIALOGSHARE_T055DE5CF84315A9A01E6ABFEE8F136DFB778EBC0_H
#ifndef GRAPHREQUEST_T81F9EA1312B20AB3FDA2B9BE84C03A026BBDC347_H
#define GRAPHREQUEST_T81F9EA1312B20AB3FDA2B9BE84C03A026BBDC347_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Example.GraphRequest
struct  GraphRequest_t81F9EA1312B20AB3FDA2B9BE84C03A026BBDC347  : public MenuBase_tA4B32275F482BF1B58E859A78BF074D13FF3E93A
{
public:
	// System.String Facebook.Unity.Example.GraphRequest::apiQuery
	String_t* ___apiQuery_16;
	// UnityEngine.Texture2D Facebook.Unity.Example.GraphRequest::profilePic
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___profilePic_17;

public:
	inline static int32_t get_offset_of_apiQuery_16() { return static_cast<int32_t>(offsetof(GraphRequest_t81F9EA1312B20AB3FDA2B9BE84C03A026BBDC347, ___apiQuery_16)); }
	inline String_t* get_apiQuery_16() const { return ___apiQuery_16; }
	inline String_t** get_address_of_apiQuery_16() { return &___apiQuery_16; }
	inline void set_apiQuery_16(String_t* value)
	{
		___apiQuery_16 = value;
		Il2CppCodeGenWriteBarrier((&___apiQuery_16), value);
	}

	inline static int32_t get_offset_of_profilePic_17() { return static_cast<int32_t>(offsetof(GraphRequest_t81F9EA1312B20AB3FDA2B9BE84C03A026BBDC347, ___profilePic_17)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_profilePic_17() const { return ___profilePic_17; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_profilePic_17() { return &___profilePic_17; }
	inline void set_profilePic_17(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___profilePic_17 = value;
		Il2CppCodeGenWriteBarrier((&___profilePic_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHREQUEST_T81F9EA1312B20AB3FDA2B9BE84C03A026BBDC347_H
#ifndef MAINMENU_T7BBE78D87657176F7FD619692A2AA99B7D954B9F_H
#define MAINMENU_T7BBE78D87657176F7FD619692A2AA99B7D954B9F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Example.MainMenu
struct  MainMenu_t7BBE78D87657176F7FD619692A2AA99B7D954B9F  : public MenuBase_tA4B32275F482BF1B58E859A78BF074D13FF3E93A
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAINMENU_T7BBE78D87657176F7FD619692A2AA99B7D954B9F_H
#ifndef PAY_T8F37A17103EAE0C62C542549D25A1469DD9574FC_H
#define PAY_T8F37A17103EAE0C62C542549D25A1469DD9574FC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Example.Pay
struct  Pay_t8F37A17103EAE0C62C542549D25A1469DD9574FC  : public MenuBase_tA4B32275F482BF1B58E859A78BF074D13FF3E93A
{
public:
	// System.String Facebook.Unity.Example.Pay::payProduct
	String_t* ___payProduct_16;

public:
	inline static int32_t get_offset_of_payProduct_16() { return static_cast<int32_t>(offsetof(Pay_t8F37A17103EAE0C62C542549D25A1469DD9574FC, ___payProduct_16)); }
	inline String_t* get_payProduct_16() const { return ___payProduct_16; }
	inline String_t** get_address_of_payProduct_16() { return &___payProduct_16; }
	inline void set_payProduct_16(String_t* value)
	{
		___payProduct_16 = value;
		Il2CppCodeGenWriteBarrier((&___payProduct_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAY_T8F37A17103EAE0C62C542549D25A1469DD9574FC_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8800 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8801 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8802 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8803 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8804 = { sizeof (MobileAdsEventExecutor_tBB365EB01588F8B60DA48EC9A7BE0DCF0DB784AA), -1, sizeof(MobileAdsEventExecutor_tBB365EB01588F8B60DA48EC9A7BE0DCF0DB784AA_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8804[3] = 
{
	MobileAdsEventExecutor_tBB365EB01588F8B60DA48EC9A7BE0DCF0DB784AA_StaticFields::get_offset_of_instance_4(),
	MobileAdsEventExecutor_tBB365EB01588F8B60DA48EC9A7BE0DCF0DB784AA_StaticFields::get_offset_of_adEventsQueue_5(),
	MobileAdsEventExecutor_tBB365EB01588F8B60DA48EC9A7BE0DCF0DB784AA_StaticFields::get_offset_of_adEventsQueueEmpty_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8805 = { sizeof (RewardedAdDummyClient_t85A34E08FDD3D986DFA1CAC1166A85FCD92F2BD5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8805[6] = 
{
	RewardedAdDummyClient_t85A34E08FDD3D986DFA1CAC1166A85FCD92F2BD5::get_offset_of_OnAdLoaded_0(),
	RewardedAdDummyClient_t85A34E08FDD3D986DFA1CAC1166A85FCD92F2BD5::get_offset_of_OnAdFailedToLoad_1(),
	RewardedAdDummyClient_t85A34E08FDD3D986DFA1CAC1166A85FCD92F2BD5::get_offset_of_OnAdFailedToShow_2(),
	RewardedAdDummyClient_t85A34E08FDD3D986DFA1CAC1166A85FCD92F2BD5::get_offset_of_OnAdOpening_3(),
	RewardedAdDummyClient_t85A34E08FDD3D986DFA1CAC1166A85FCD92F2BD5::get_offset_of_OnAdClosed_4(),
	RewardedAdDummyClient_t85A34E08FDD3D986DFA1CAC1166A85FCD92F2BD5::get_offset_of_OnUserEarnedReward_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8806 = { sizeof (Utils_tBD140E67B725142CBE4F6441B6628592C49D892B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8807 = { sizeof (AdapterStatus_t56EFBE96CA15511B44B4FB40C70F1D113984933D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8807[3] = 
{
	AdapterStatus_t56EFBE96CA15511B44B4FB40C70F1D113984933D::get_offset_of_U3CInitializationStateU3Ek__BackingField_0(),
	AdapterStatus_t56EFBE96CA15511B44B4FB40C70F1D113984933D::get_offset_of_U3CDescriptionU3Ek__BackingField_1(),
	AdapterStatus_t56EFBE96CA15511B44B4FB40C70F1D113984933D::get_offset_of_U3CLatencyU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8808 = { sizeof (AdapterState_tE5ED3B9F9544763B73A0ABBEF88F2ED76381C5C9)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable8808[3] = 
{
	AdapterState_tE5ED3B9F9544763B73A0ABBEF88F2ED76381C5C9::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8809 = { sizeof (AdErrorEventArgs_t9C85856D82FD7BF9084CE64814B53E6D7A046A42), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8809[1] = 
{
	AdErrorEventArgs_t9C85856D82FD7BF9084CE64814B53E6D7A046A42::get_offset_of_U3CMessageU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8810 = { sizeof (AdFailedToLoadEventArgs_t7DBB865D1AC4FB5F962042A40D0DD5B413377F29), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8810[1] = 
{
	AdFailedToLoadEventArgs_t7DBB865D1AC4FB5F962042A40D0DD5B413377F29::get_offset_of_U3CMessageU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8811 = { sizeof (NativeAdType_tE23A725E4AFC0AFB3401D83417E936A42CB0C628)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable8811[2] = 
{
	NativeAdType_tE23A725E4AFC0AFB3401D83417E936A42CB0C628::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8812 = { sizeof (AdLoader_tE5D111966C04D585B1DCE3FAA3F0583CA7A3588A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8812[7] = 
{
	AdLoader_tE5D111966C04D585B1DCE3FAA3F0583CA7A3588A::get_offset_of_adLoaderClient_0(),
	AdLoader_tE5D111966C04D585B1DCE3FAA3F0583CA7A3588A::get_offset_of_OnAdFailedToLoad_1(),
	AdLoader_tE5D111966C04D585B1DCE3FAA3F0583CA7A3588A::get_offset_of_OnCustomNativeTemplateAdLoaded_2(),
	AdLoader_tE5D111966C04D585B1DCE3FAA3F0583CA7A3588A::get_offset_of_U3CCustomNativeTemplateClickHandlersU3Ek__BackingField_3(),
	AdLoader_tE5D111966C04D585B1DCE3FAA3F0583CA7A3588A::get_offset_of_U3CAdUnitIdU3Ek__BackingField_4(),
	AdLoader_tE5D111966C04D585B1DCE3FAA3F0583CA7A3588A::get_offset_of_U3CAdTypesU3Ek__BackingField_5(),
	AdLoader_tE5D111966C04D585B1DCE3FAA3F0583CA7A3588A::get_offset_of_U3CTemplateIdsU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8813 = { sizeof (Builder_t855B9AD356DAB2EC5FA6C043605024B98EB4BAA7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8813[4] = 
{
	Builder_t855B9AD356DAB2EC5FA6C043605024B98EB4BAA7::get_offset_of_U3CAdUnitIdU3Ek__BackingField_0(),
	Builder_t855B9AD356DAB2EC5FA6C043605024B98EB4BAA7::get_offset_of_U3CAdTypesU3Ek__BackingField_1(),
	Builder_t855B9AD356DAB2EC5FA6C043605024B98EB4BAA7::get_offset_of_U3CTemplateIdsU3Ek__BackingField_2(),
	Builder_t855B9AD356DAB2EC5FA6C043605024B98EB4BAA7::get_offset_of_U3CCustomNativeTemplateClickHandlersU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8814 = { sizeof (AdPosition_t6A92A3E97DC2384E409302E35FB6EAEEE3ECF850)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable8814[8] = 
{
	AdPosition_t6A92A3E97DC2384E409302E35FB6EAEEE3ECF850::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8815 = { sizeof (AdRequest_t98763832B122F67AC2952360B6381F4F5848306F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8815[9] = 
{
	0,
	0,
	AdRequest_t98763832B122F67AC2952360B6381F4F5848306F::get_offset_of_U3CTestDevicesU3Ek__BackingField_2(),
	AdRequest_t98763832B122F67AC2952360B6381F4F5848306F::get_offset_of_U3CKeywordsU3Ek__BackingField_3(),
	AdRequest_t98763832B122F67AC2952360B6381F4F5848306F::get_offset_of_U3CBirthdayU3Ek__BackingField_4(),
	AdRequest_t98763832B122F67AC2952360B6381F4F5848306F::get_offset_of_U3CGenderU3Ek__BackingField_5(),
	AdRequest_t98763832B122F67AC2952360B6381F4F5848306F::get_offset_of_U3CTagForChildDirectedTreatmentU3Ek__BackingField_6(),
	AdRequest_t98763832B122F67AC2952360B6381F4F5848306F::get_offset_of_U3CExtrasU3Ek__BackingField_7(),
	AdRequest_t98763832B122F67AC2952360B6381F4F5848306F::get_offset_of_U3CMediationExtrasU3Ek__BackingField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8816 = { sizeof (Builder_t2384CCFEB139CFC834887DBD4754905ABFE8F2E9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8816[7] = 
{
	Builder_t2384CCFEB139CFC834887DBD4754905ABFE8F2E9::get_offset_of_U3CTestDevicesU3Ek__BackingField_0(),
	Builder_t2384CCFEB139CFC834887DBD4754905ABFE8F2E9::get_offset_of_U3CKeywordsU3Ek__BackingField_1(),
	Builder_t2384CCFEB139CFC834887DBD4754905ABFE8F2E9::get_offset_of_U3CBirthdayU3Ek__BackingField_2(),
	Builder_t2384CCFEB139CFC834887DBD4754905ABFE8F2E9::get_offset_of_U3CGenderU3Ek__BackingField_3(),
	Builder_t2384CCFEB139CFC834887DBD4754905ABFE8F2E9::get_offset_of_U3CChildDirectedTreatmentTagU3Ek__BackingField_4(),
	Builder_t2384CCFEB139CFC834887DBD4754905ABFE8F2E9::get_offset_of_U3CExtrasU3Ek__BackingField_5(),
	Builder_t2384CCFEB139CFC834887DBD4754905ABFE8F2E9::get_offset_of_U3CMediationExtrasU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8817 = { sizeof (AdSize_t10976E93E4E1F370899C5D6ED9DDB683F9C53E04), -1, sizeof(AdSize_t10976E93E4E1F370899C5D6ED9DDB683F9C53E04_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8817[9] = 
{
	AdSize_t10976E93E4E1F370899C5D6ED9DDB683F9C53E04::get_offset_of_isSmartBanner_0(),
	AdSize_t10976E93E4E1F370899C5D6ED9DDB683F9C53E04::get_offset_of_width_1(),
	AdSize_t10976E93E4E1F370899C5D6ED9DDB683F9C53E04::get_offset_of_height_2(),
	AdSize_t10976E93E4E1F370899C5D6ED9DDB683F9C53E04_StaticFields::get_offset_of_Banner_3(),
	AdSize_t10976E93E4E1F370899C5D6ED9DDB683F9C53E04_StaticFields::get_offset_of_MediumRectangle_4(),
	AdSize_t10976E93E4E1F370899C5D6ED9DDB683F9C53E04_StaticFields::get_offset_of_IABBanner_5(),
	AdSize_t10976E93E4E1F370899C5D6ED9DDB683F9C53E04_StaticFields::get_offset_of_Leaderboard_6(),
	AdSize_t10976E93E4E1F370899C5D6ED9DDB683F9C53E04_StaticFields::get_offset_of_SmartBanner_7(),
	AdSize_t10976E93E4E1F370899C5D6ED9DDB683F9C53E04_StaticFields::get_offset_of_FullWidth_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8818 = { sizeof (BannerView_t93EE32590F086BE31F51E2F84714B49C4328B4B5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8818[6] = 
{
	BannerView_t93EE32590F086BE31F51E2F84714B49C4328B4B5::get_offset_of_client_0(),
	BannerView_t93EE32590F086BE31F51E2F84714B49C4328B4B5::get_offset_of_OnAdLoaded_1(),
	BannerView_t93EE32590F086BE31F51E2F84714B49C4328B4B5::get_offset_of_OnAdFailedToLoad_2(),
	BannerView_t93EE32590F086BE31F51E2F84714B49C4328B4B5::get_offset_of_OnAdOpening_3(),
	BannerView_t93EE32590F086BE31F51E2F84714B49C4328B4B5::get_offset_of_OnAdClosed_4(),
	BannerView_t93EE32590F086BE31F51E2F84714B49C4328B4B5::get_offset_of_OnAdLeavingApplication_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8819 = { sizeof (CustomNativeEventArgs_tA1BC7F7FF42154E57061FCA213F6EB7166C7831C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8819[1] = 
{
	CustomNativeEventArgs_tA1BC7F7FF42154E57061FCA213F6EB7166C7831C::get_offset_of_U3CnativeAdU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8820 = { sizeof (CustomNativeTemplateAd_tA73CD30978AE046F6E81DA0DE0D90E0260870D94), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8820[1] = 
{
	CustomNativeTemplateAd_tA73CD30978AE046F6E81DA0DE0D90E0260870D94::get_offset_of_client_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8821 = { sizeof (Gender_tB13F4EC090657F9C9725E7EAEFCA6056E5C72660)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable8821[4] = 
{
	Gender_tB13F4EC090657F9C9725E7EAEFCA6056E5C72660::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8822 = { sizeof (InterstitialAd_tAA5D23EA1E8483C7D10850AEA71D0DD8D75C89DD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8822[6] = 
{
	InterstitialAd_tAA5D23EA1E8483C7D10850AEA71D0DD8D75C89DD::get_offset_of_client_0(),
	InterstitialAd_tAA5D23EA1E8483C7D10850AEA71D0DD8D75C89DD::get_offset_of_OnAdLoaded_1(),
	InterstitialAd_tAA5D23EA1E8483C7D10850AEA71D0DD8D75C89DD::get_offset_of_OnAdFailedToLoad_2(),
	InterstitialAd_tAA5D23EA1E8483C7D10850AEA71D0DD8D75C89DD::get_offset_of_OnAdOpening_3(),
	InterstitialAd_tAA5D23EA1E8483C7D10850AEA71D0DD8D75C89DD::get_offset_of_OnAdClosed_4(),
	InterstitialAd_tAA5D23EA1E8483C7D10850AEA71D0DD8D75C89DD::get_offset_of_OnAdLeavingApplication_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8823 = { sizeof (MobileAds_tFA519006704C915B6A32186550B3203DFF8D242D), -1, sizeof(MobileAds_tFA519006704C915B6A32186550B3203DFF8D242D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8823[1] = 
{
	MobileAds_tFA519006704C915B6A32186550B3203DFF8D242D_StaticFields::get_offset_of_client_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8824 = { sizeof (Reward_t5656463A5A19508F4B9AF6BAB14D343F5A9EC260), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8824[2] = 
{
	Reward_t5656463A5A19508F4B9AF6BAB14D343F5A9EC260::get_offset_of_U3CTypeU3Ek__BackingField_1(),
	Reward_t5656463A5A19508F4B9AF6BAB14D343F5A9EC260::get_offset_of_U3CAmountU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8825 = { sizeof (RewardBasedVideoAd_t9795550B217AAB71B32AEDA94163C6A929353491), -1, sizeof(RewardBasedVideoAd_t9795550B217AAB71B32AEDA94163C6A929353491_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8825[10] = 
{
	RewardBasedVideoAd_t9795550B217AAB71B32AEDA94163C6A929353491::get_offset_of_client_0(),
	RewardBasedVideoAd_t9795550B217AAB71B32AEDA94163C6A929353491_StaticFields::get_offset_of_instance_1(),
	RewardBasedVideoAd_t9795550B217AAB71B32AEDA94163C6A929353491::get_offset_of_OnAdLoaded_2(),
	RewardBasedVideoAd_t9795550B217AAB71B32AEDA94163C6A929353491::get_offset_of_OnAdFailedToLoad_3(),
	RewardBasedVideoAd_t9795550B217AAB71B32AEDA94163C6A929353491::get_offset_of_OnAdOpening_4(),
	RewardBasedVideoAd_t9795550B217AAB71B32AEDA94163C6A929353491::get_offset_of_OnAdStarted_5(),
	RewardBasedVideoAd_t9795550B217AAB71B32AEDA94163C6A929353491::get_offset_of_OnAdClosed_6(),
	RewardBasedVideoAd_t9795550B217AAB71B32AEDA94163C6A929353491::get_offset_of_OnAdRewarded_7(),
	RewardBasedVideoAd_t9795550B217AAB71B32AEDA94163C6A929353491::get_offset_of_OnAdLeavingApplication_8(),
	RewardBasedVideoAd_t9795550B217AAB71B32AEDA94163C6A929353491::get_offset_of_OnAdCompleted_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8826 = { sizeof (RewardedAd_tF537DADD772F3E98E9283E7CE9CB2AAE957E16D4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8826[7] = 
{
	RewardedAd_tF537DADD772F3E98E9283E7CE9CB2AAE957E16D4::get_offset_of_client_0(),
	RewardedAd_tF537DADD772F3E98E9283E7CE9CB2AAE957E16D4::get_offset_of_OnAdLoaded_1(),
	RewardedAd_tF537DADD772F3E98E9283E7CE9CB2AAE957E16D4::get_offset_of_OnAdFailedToLoad_2(),
	RewardedAd_tF537DADD772F3E98E9283E7CE9CB2AAE957E16D4::get_offset_of_OnAdFailedToShow_3(),
	RewardedAd_tF537DADD772F3E98E9283E7CE9CB2AAE957E16D4::get_offset_of_OnAdOpening_4(),
	RewardedAd_tF537DADD772F3E98E9283E7CE9CB2AAE957E16D4::get_offset_of_OnAdClosed_5(),
	RewardedAd_tF537DADD772F3E98E9283E7CE9CB2AAE957E16D4::get_offset_of_OnUserEarnedReward_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8827 = { sizeof (MediationExtras_t53F0F7F90139A2D039272C9764DC880D4B3AB301), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8827[1] = 
{
	MediationExtras_t53F0F7F90139A2D039272C9764DC880D4B3AB301::get_offset_of_U3CExtrasU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8828 = { sizeof (ConsoleBase_t80F55AC34D95221101149083BAB6EBF9571E72B4), -1, sizeof(ConsoleBase_t80F55AC34D95221101149083BAB6EBF9571E72B4_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8828[11] = 
{
	0,
	ConsoleBase_t80F55AC34D95221101149083BAB6EBF9571E72B4_StaticFields::get_offset_of_menuStack_5(),
	ConsoleBase_t80F55AC34D95221101149083BAB6EBF9571E72B4::get_offset_of_status_6(),
	ConsoleBase_t80F55AC34D95221101149083BAB6EBF9571E72B4::get_offset_of_lastResponse_7(),
	ConsoleBase_t80F55AC34D95221101149083BAB6EBF9571E72B4::get_offset_of_scrollPosition_8(),
	ConsoleBase_t80F55AC34D95221101149083BAB6EBF9571E72B4::get_offset_of_scaleFactor_9(),
	ConsoleBase_t80F55AC34D95221101149083BAB6EBF9571E72B4::get_offset_of_textStyle_10(),
	ConsoleBase_t80F55AC34D95221101149083BAB6EBF9571E72B4::get_offset_of_buttonStyle_11(),
	ConsoleBase_t80F55AC34D95221101149083BAB6EBF9571E72B4::get_offset_of_textInputStyle_12(),
	ConsoleBase_t80F55AC34D95221101149083BAB6EBF9571E72B4::get_offset_of_labelStyle_13(),
	ConsoleBase_t80F55AC34D95221101149083BAB6EBF9571E72B4::get_offset_of_U3CLastResponseTextureU3Ek__BackingField_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8829 = { sizeof (LogView_t81FB093AA3DB55ED7D8B97F6BAD8C6BCEFC8DF7A), -1, sizeof(LogView_t81FB093AA3DB55ED7D8B97F6BAD8C6BCEFC8DF7A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8829[2] = 
{
	LogView_t81FB093AA3DB55ED7D8B97F6BAD8C6BCEFC8DF7A_StaticFields::get_offset_of_datePatt_15(),
	LogView_t81FB093AA3DB55ED7D8B97F6BAD8C6BCEFC8DF7A_StaticFields::get_offset_of_events_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8830 = { sizeof (MenuBase_tA4B32275F482BF1B58E859A78BF074D13FF3E93A), -1, sizeof(MenuBase_tA4B32275F482BF1B58E859A78BF074D13FF3E93A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8830[1] = 
{
	MenuBase_tA4B32275F482BF1B58E859A78BF074D13FF3E93A_StaticFields::get_offset_of_shareDialogMode_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8831 = { sizeof (AccessTokenMenu_tA169D1DDBAD90D514BE1C0BA14FA7FAC3AA5542D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8832 = { sizeof (AppEvents_t20C76E739C9ABC6392F335DCF9902C17C87D6B70), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8833 = { sizeof (AppInvites_t20590921C9CC4B49A844AB27042A146CA6903B0C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8834 = { sizeof (AppLinks_t0D8BC945EC82E808B1CD4748D540A69D2970913B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8835 = { sizeof (AppRequests_t22F4C53E63A0E4C23359B3D16D9CDF957A60EEA3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8835[10] = 
{
	AppRequests_t22F4C53E63A0E4C23359B3D16D9CDF957A60EEA3::get_offset_of_requestMessage_16(),
	AppRequests_t22F4C53E63A0E4C23359B3D16D9CDF957A60EEA3::get_offset_of_requestTo_17(),
	AppRequests_t22F4C53E63A0E4C23359B3D16D9CDF957A60EEA3::get_offset_of_requestFilter_18(),
	AppRequests_t22F4C53E63A0E4C23359B3D16D9CDF957A60EEA3::get_offset_of_requestExcludes_19(),
	AppRequests_t22F4C53E63A0E4C23359B3D16D9CDF957A60EEA3::get_offset_of_requestMax_20(),
	AppRequests_t22F4C53E63A0E4C23359B3D16D9CDF957A60EEA3::get_offset_of_requestData_21(),
	AppRequests_t22F4C53E63A0E4C23359B3D16D9CDF957A60EEA3::get_offset_of_requestTitle_22(),
	AppRequests_t22F4C53E63A0E4C23359B3D16D9CDF957A60EEA3::get_offset_of_requestObjectID_23(),
	AppRequests_t22F4C53E63A0E4C23359B3D16D9CDF957A60EEA3::get_offset_of_selectedAction_24(),
	AppRequests_t22F4C53E63A0E4C23359B3D16D9CDF957A60EEA3::get_offset_of_actionTypeStrings_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8836 = { sizeof (DialogShare_t055DE5CF84315A9A01E6ABFEE8F136DFB778EBC0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8836[11] = 
{
	DialogShare_t055DE5CF84315A9A01E6ABFEE8F136DFB778EBC0::get_offset_of_shareLink_16(),
	DialogShare_t055DE5CF84315A9A01E6ABFEE8F136DFB778EBC0::get_offset_of_shareTitle_17(),
	DialogShare_t055DE5CF84315A9A01E6ABFEE8F136DFB778EBC0::get_offset_of_shareDescription_18(),
	DialogShare_t055DE5CF84315A9A01E6ABFEE8F136DFB778EBC0::get_offset_of_shareImage_19(),
	DialogShare_t055DE5CF84315A9A01E6ABFEE8F136DFB778EBC0::get_offset_of_feedTo_20(),
	DialogShare_t055DE5CF84315A9A01E6ABFEE8F136DFB778EBC0::get_offset_of_feedLink_21(),
	DialogShare_t055DE5CF84315A9A01E6ABFEE8F136DFB778EBC0::get_offset_of_feedTitle_22(),
	DialogShare_t055DE5CF84315A9A01E6ABFEE8F136DFB778EBC0::get_offset_of_feedCaption_23(),
	DialogShare_t055DE5CF84315A9A01E6ABFEE8F136DFB778EBC0::get_offset_of_feedDescription_24(),
	DialogShare_t055DE5CF84315A9A01E6ABFEE8F136DFB778EBC0::get_offset_of_feedImage_25(),
	DialogShare_t055DE5CF84315A9A01E6ABFEE8F136DFB778EBC0::get_offset_of_feedMediaSource_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8837 = { sizeof (GraphRequest_t81F9EA1312B20AB3FDA2B9BE84C03A026BBDC347), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8837[2] = 
{
	GraphRequest_t81F9EA1312B20AB3FDA2B9BE84C03A026BBDC347::get_offset_of_apiQuery_16(),
	GraphRequest_t81F9EA1312B20AB3FDA2B9BE84C03A026BBDC347::get_offset_of_profilePic_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8838 = { sizeof (U3CTakeScreenshotU3Ed__4_t32FCC3EB0A36F0973EE1E62E3E835E9133D1C105), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8838[3] = 
{
	U3CTakeScreenshotU3Ed__4_t32FCC3EB0A36F0973EE1E62E3E835E9133D1C105::get_offset_of_U3CU3E1__state_0(),
	U3CTakeScreenshotU3Ed__4_t32FCC3EB0A36F0973EE1E62E3E835E9133D1C105::get_offset_of_U3CU3E2__current_1(),
	U3CTakeScreenshotU3Ed__4_t32FCC3EB0A36F0973EE1E62E3E835E9133D1C105::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8839 = { sizeof (MainMenu_t7BBE78D87657176F7FD619692A2AA99B7D954B9F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8840 = { sizeof (Pay_t8F37A17103EAE0C62C542549D25A1469DD9574FC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8840[1] = 
{
	Pay_t8F37A17103EAE0C62C542549D25A1469DD9574FC::get_offset_of_payProduct_16(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
