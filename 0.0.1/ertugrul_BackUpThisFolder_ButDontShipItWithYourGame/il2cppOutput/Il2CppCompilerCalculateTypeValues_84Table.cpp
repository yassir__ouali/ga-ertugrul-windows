﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// ChapterModel[]
struct ChapterModelU5BU5D_t157A022851AA288B91C80E539D65F11D1F063317;
// ChestModelSO
struct ChestModelSO_t78F97E6AC8F488AD3C18236C4080E78CCF29B2A2;
// ClaimTournamentRewardEvent
struct ClaimTournamentRewardEvent_t31E71F6AB2C04F3BD77BC744E3F0CE51D5CB0D63;
// DataDictionary
struct DataDictionary_t9D4A0BE16CC0324BF2D47F35B50C65CC9B44E606;
// FillLivesEvent
struct FillLivesEvent_tBC3C967A3339323DC976C55B063FCD0FF64D2568;
// GameSettingsSO
struct GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C;
// GameSparks.Core.GSData
struct GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937;
// GlobalVO
struct GlobalVO_t3F00C793D791DA4C9FDF395F616EA34E82E53385;
// InventorySystem
struct InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2;
// InventoryVO
struct InventoryVO_tC7F0305FC930782BD2E4A47E3BD0504F0DCC6AD9;
// LoginCommandEvent
struct LoginCommandEvent_tE333B28F8632EC5795F9FCD22A6CC7EB36F1C493;
// MetadataVO
struct MetadataVO_t65F0D785B7DE72B5A899170B925A9E360F4BEB59;
// PatchingVO
struct PatchingVO_tF6810B5A961785E0EB990B4C511101A0A75F3CB4;
// RewardModel
struct RewardModel_tE09D75F3B47938652450ABB51F21663745C8BC44;
// RewardModel[]
struct RewardModelU5BU5D_t5B4D5EB45F969FE53860B9964AEB5E519B048C11;
// RotaryHeart.Lib.SerializableDictionary.ReorderableList
struct ReorderableList_tE9342433971E09B8B941B8BDB654FBCACD8839D0;
// RotaryHeart.Lib.SerializableDictionary.RequiredReferences
struct RequiredReferences_tD502DA241623B0039A32F954E9BB8040D038F5B0;
// StarChestEvent
struct StarChestEvent_t0797A526A463F05F371BD49E20AE31B774586F43;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.String,AssetData>
struct Dictionary_2_tE93A2B9ABDA72381551E41330AA05157E9F976C1;
// System.Collections.Generic.Dictionary`2<System.String,ConfModel>
struct Dictionary_2_tF352C3C4DFF013A39485B78BB272A67BB62D3B9D;
// System.Collections.Generic.Dictionary`2<System.String,ItemGenerationModel>
struct Dictionary_2_t3174197D3B3D5BB902D450BCB5A8DA1B57C6B057;
// System.Collections.Generic.Dictionary`2<System.String,LevelModel>
struct Dictionary_2_t9C3570F5044A5A45EA82770EF6D53748E89DD9C5;
// System.Collections.Generic.Dictionary`2<System.String,LevelObjective>
struct Dictionary_2_t27EDBB09D6B6FEBC5A2B61FA9725D313DA2BB658;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC;
// System.Collections.Generic.List`1<AssetData>
struct List_1_t4C0E5E5429A980785DA4734722012505FB30D287;
// System.Collections.Generic.List`1<ChestItemModel>
struct List_1_t82FEF585FAFA799806FF83BC03E51C393CD8004F;
// System.Collections.Generic.List`1<CustomTutorialModel>
struct List_1_tA4D8069E4BD0FB418FBF665C39E5CDE768F2CE0C;
// System.Collections.Generic.List`1<DummyLeaderboardModel>
struct List_1_t15D6C7E13BDC8A5780D3F7B823F475DBF7785142;
// System.Collections.Generic.List`1<EffectModel>
struct List_1_t659C06110FB03B1EF961FEA5BF138ABD36CE9ABB;
// System.Collections.Generic.List`1<EffectVisual>
struct List_1_tA366AAFE63D066448A3C35B822F880C014E71660;
// System.Collections.Generic.List`1<EffectVisualModel>
struct List_1_tB04C00DC79DD1B4D62E9A575C6F56E38DAD22984;
// System.Collections.Generic.List`1<ItemContentModel>
struct List_1_t2C6CF342B96E130723D45845A1FD2053CBAF0F27;
// System.Collections.Generic.List`1<ItemModel>
struct List_1_t0B47A19D77897D2BC30A0820609E3BBA2156F084;
// System.Collections.Generic.List`1<PerLevelTutorialModel>
struct List_1_tBE32162E196BAE003F483B65C07526E97560480C;
// System.Collections.Generic.List`1<RewardModel>
struct List_1_t05E5F1AEA6DCB3F4DE9EF82A15D37F0095D1F827;
// System.Collections.Generic.List`1<SpellModel>
struct List_1_t3C7DBA87FE72C14B6D9818C99E13D8ED9660373F;
// System.Collections.Generic.List`1<System.String>
struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3;
// System.Collections.Generic.List`1<TowerModel>
struct List_1_t7D23CBEBEF47512C344C65315325FF324D97FC1F;
// System.Collections.Generic.List`1<TrapModel>
struct List_1_t5E828DBB770C7425713DBEAE9366AFC3C1C4ADA7;
// System.Collections.Generic.List`1<UnitsModel>
struct List_1_tD552FACB362EB8A9DA9B6EDB71E741DA5D570834;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// Tayr.TriggerModel
struct TriggerModel_t09B711CC906A9A76F767318AF6754B20F253399E;
// Tayr.VOSaver
struct VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F;
// TournamentResultCommandEvent
struct TournamentResultCommandEvent_t63203EB3B05D0FB9B44625DF65238A1FDB642012;
// TournamentsVO
struct TournamentsVO_tCBA9A235AE4069263CE332B1E9150CDABD0E8016;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F;
// UpdateSettingsEvent
struct UpdateSettingsEvent_tBDFC5C0BE56631615A2FE1F8DF1593535637EC8F;
// UserVO
struct UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94;
// Zenject.DiContainer
struct DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef ASSETDATA_T155B7C20CE31B42FB840C26A65CBBE11D4D16D7A_H
#define ASSETDATA_T155B7C20CE31B42FB840C26A65CBBE11D4D16D7A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AssetData
struct  AssetData_t155B7C20CE31B42FB840C26A65CBBE11D4D16D7A  : public RuntimeObject
{
public:
	// System.String AssetData::Bundle
	String_t* ___Bundle_0;
	// System.String AssetData::Prefab
	String_t* ___Prefab_1;
	// System.Int32 AssetData::WarmUp
	int32_t ___WarmUp_2;

public:
	inline static int32_t get_offset_of_Bundle_0() { return static_cast<int32_t>(offsetof(AssetData_t155B7C20CE31B42FB840C26A65CBBE11D4D16D7A, ___Bundle_0)); }
	inline String_t* get_Bundle_0() const { return ___Bundle_0; }
	inline String_t** get_address_of_Bundle_0() { return &___Bundle_0; }
	inline void set_Bundle_0(String_t* value)
	{
		___Bundle_0 = value;
		Il2CppCodeGenWriteBarrier((&___Bundle_0), value);
	}

	inline static int32_t get_offset_of_Prefab_1() { return static_cast<int32_t>(offsetof(AssetData_t155B7C20CE31B42FB840C26A65CBBE11D4D16D7A, ___Prefab_1)); }
	inline String_t* get_Prefab_1() const { return ___Prefab_1; }
	inline String_t** get_address_of_Prefab_1() { return &___Prefab_1; }
	inline void set_Prefab_1(String_t* value)
	{
		___Prefab_1 = value;
		Il2CppCodeGenWriteBarrier((&___Prefab_1), value);
	}

	inline static int32_t get_offset_of_WarmUp_2() { return static_cast<int32_t>(offsetof(AssetData_t155B7C20CE31B42FB840C26A65CBBE11D4D16D7A, ___WarmUp_2)); }
	inline int32_t get_WarmUp_2() const { return ___WarmUp_2; }
	inline int32_t* get_address_of_WarmUp_2() { return &___WarmUp_2; }
	inline void set_WarmUp_2(int32_t value)
	{
		___WarmUp_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSETDATA_T155B7C20CE31B42FB840C26A65CBBE11D4D16D7A_H
#ifndef BATTLECONSTANTS_T9FB60E5A8095C645C4A9066C587967D9B457C162_H
#define BATTLECONSTANTS_T9FB60E5A8095C645C4A9066C587967D9B457C162_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BattleConstants
struct  BattleConstants_t9FB60E5A8095C645C4A9066C587967D9B457C162  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BATTLECONSTANTS_T9FB60E5A8095C645C4A9066C587967D9B457C162_H
#ifndef CHAPTERMODEL_T128F63AB2A81208183CD6AC72466B96980DA0494_H
#define CHAPTERMODEL_T128F63AB2A81208183CD6AC72466B96980DA0494_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChapterModel
struct  ChapterModel_t128F63AB2A81208183CD6AC72466B96980DA0494  : public RuntimeObject
{
public:
	// System.String ChapterModel::Bundle
	String_t* ___Bundle_0;
	// System.Int32 ChapterModel::MaxLevel
	int32_t ___MaxLevel_1;

public:
	inline static int32_t get_offset_of_Bundle_0() { return static_cast<int32_t>(offsetof(ChapterModel_t128F63AB2A81208183CD6AC72466B96980DA0494, ___Bundle_0)); }
	inline String_t* get_Bundle_0() const { return ___Bundle_0; }
	inline String_t** get_address_of_Bundle_0() { return &___Bundle_0; }
	inline void set_Bundle_0(String_t* value)
	{
		___Bundle_0 = value;
		Il2CppCodeGenWriteBarrier((&___Bundle_0), value);
	}

	inline static int32_t get_offset_of_MaxLevel_1() { return static_cast<int32_t>(offsetof(ChapterModel_t128F63AB2A81208183CD6AC72466B96980DA0494, ___MaxLevel_1)); }
	inline int32_t get_MaxLevel_1() const { return ___MaxLevel_1; }
	inline int32_t* get_address_of_MaxLevel_1() { return &___MaxLevel_1; }
	inline void set_MaxLevel_1(int32_t value)
	{
		___MaxLevel_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHAPTERMODEL_T128F63AB2A81208183CD6AC72466B96980DA0494_H
#ifndef CHAPTERSSO_T6D25AB47CA5A386B1455B167AA805B0CAB2B24C1_H
#define CHAPTERSSO_T6D25AB47CA5A386B1455B167AA805B0CAB2B24C1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChaptersSO
struct  ChaptersSO_t6D25AB47CA5A386B1455B167AA805B0CAB2B24C1  : public RuntimeObject
{
public:
	// ChapterModel[] ChaptersSO::Chapters
	ChapterModelU5BU5D_t157A022851AA288B91C80E539D65F11D1F063317* ___Chapters_0;

public:
	inline static int32_t get_offset_of_Chapters_0() { return static_cast<int32_t>(offsetof(ChaptersSO_t6D25AB47CA5A386B1455B167AA805B0CAB2B24C1, ___Chapters_0)); }
	inline ChapterModelU5BU5D_t157A022851AA288B91C80E539D65F11D1F063317* get_Chapters_0() const { return ___Chapters_0; }
	inline ChapterModelU5BU5D_t157A022851AA288B91C80E539D65F11D1F063317** get_address_of_Chapters_0() { return &___Chapters_0; }
	inline void set_Chapters_0(ChapterModelU5BU5D_t157A022851AA288B91C80E539D65F11D1F063317* value)
	{
		___Chapters_0 = value;
		Il2CppCodeGenWriteBarrier((&___Chapters_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHAPTERSSO_T6D25AB47CA5A386B1455B167AA805B0CAB2B24C1_H
#ifndef CHESTITEMMODEL_TFB97FEEB81B57C2A0C1DDD9C5662A3965197EFA8_H
#define CHESTITEMMODEL_TFB97FEEB81B57C2A0C1DDD9C5662A3965197EFA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChestItemModel
struct  ChestItemModel_tFB97FEEB81B57C2A0C1DDD9C5662A3965197EFA8  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<ItemContentModel> ChestItemModel::Content
	List_1_t2C6CF342B96E130723D45845A1FD2053CBAF0F27 * ___Content_0;
	// System.Int32 ChestItemModel::Percentage
	int32_t ___Percentage_1;
	// System.Int32 ChestItemModel::Number
	int32_t ___Number_2;
	// System.Boolean ChestItemModel::IsUnique
	bool ___IsUnique_3;

public:
	inline static int32_t get_offset_of_Content_0() { return static_cast<int32_t>(offsetof(ChestItemModel_tFB97FEEB81B57C2A0C1DDD9C5662A3965197EFA8, ___Content_0)); }
	inline List_1_t2C6CF342B96E130723D45845A1FD2053CBAF0F27 * get_Content_0() const { return ___Content_0; }
	inline List_1_t2C6CF342B96E130723D45845A1FD2053CBAF0F27 ** get_address_of_Content_0() { return &___Content_0; }
	inline void set_Content_0(List_1_t2C6CF342B96E130723D45845A1FD2053CBAF0F27 * value)
	{
		___Content_0 = value;
		Il2CppCodeGenWriteBarrier((&___Content_0), value);
	}

	inline static int32_t get_offset_of_Percentage_1() { return static_cast<int32_t>(offsetof(ChestItemModel_tFB97FEEB81B57C2A0C1DDD9C5662A3965197EFA8, ___Percentage_1)); }
	inline int32_t get_Percentage_1() const { return ___Percentage_1; }
	inline int32_t* get_address_of_Percentage_1() { return &___Percentage_1; }
	inline void set_Percentage_1(int32_t value)
	{
		___Percentage_1 = value;
	}

	inline static int32_t get_offset_of_Number_2() { return static_cast<int32_t>(offsetof(ChestItemModel_tFB97FEEB81B57C2A0C1DDD9C5662A3965197EFA8, ___Number_2)); }
	inline int32_t get_Number_2() const { return ___Number_2; }
	inline int32_t* get_address_of_Number_2() { return &___Number_2; }
	inline void set_Number_2(int32_t value)
	{
		___Number_2 = value;
	}

	inline static int32_t get_offset_of_IsUnique_3() { return static_cast<int32_t>(offsetof(ChestItemModel_tFB97FEEB81B57C2A0C1DDD9C5662A3965197EFA8, ___IsUnique_3)); }
	inline bool get_IsUnique_3() const { return ___IsUnique_3; }
	inline bool* get_address_of_IsUnique_3() { return &___IsUnique_3; }
	inline void set_IsUnique_3(bool value)
	{
		___IsUnique_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHESTITEMMODEL_TFB97FEEB81B57C2A0C1DDD9C5662A3965197EFA8_H
#ifndef CHESTMODELSO_T78F97E6AC8F488AD3C18236C4080E78CCF29B2A2_H
#define CHESTMODELSO_T78F97E6AC8F488AD3C18236C4080E78CCF29B2A2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChestModelSO
struct  ChestModelSO_t78F97E6AC8F488AD3C18236C4080E78CCF29B2A2  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<ChestItemModel> ChestModelSO::Items
	List_1_t82FEF585FAFA799806FF83BC03E51C393CD8004F * ___Items_0;

public:
	inline static int32_t get_offset_of_Items_0() { return static_cast<int32_t>(offsetof(ChestModelSO_t78F97E6AC8F488AD3C18236C4080E78CCF29B2A2, ___Items_0)); }
	inline List_1_t82FEF585FAFA799806FF83BC03E51C393CD8004F * get_Items_0() const { return ___Items_0; }
	inline List_1_t82FEF585FAFA799806FF83BC03E51C393CD8004F ** get_address_of_Items_0() { return &___Items_0; }
	inline void set_Items_0(List_1_t82FEF585FAFA799806FF83BC03E51C393CD8004F * value)
	{
		___Items_0 = value;
		Il2CppCodeGenWriteBarrier((&___Items_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHESTMODELSO_T78F97E6AC8F488AD3C18236C4080E78CCF29B2A2_H
#ifndef CLAIMTOURNAMENTREWARDCOMMANDMODEL_T4ADDCB2DC663FF9A7A7BF212E7D3764861368E1D_H
#define CLAIMTOURNAMENTREWARDCOMMANDMODEL_T4ADDCB2DC663FF9A7A7BF212E7D3764861368E1D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ClaimTournamentRewardCommandModel
struct  ClaimTournamentRewardCommandModel_t4ADDCB2DC663FF9A7A7BF212E7D3764861368E1D  : public RuntimeObject
{
public:
	// System.String ClaimTournamentRewardCommandModel::TournamentId
	String_t* ___TournamentId_0;
	// GameSparks.Core.GSData ClaimTournamentRewardCommandModel::ResponseData
	GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 * ___ResponseData_1;

public:
	inline static int32_t get_offset_of_TournamentId_0() { return static_cast<int32_t>(offsetof(ClaimTournamentRewardCommandModel_t4ADDCB2DC663FF9A7A7BF212E7D3764861368E1D, ___TournamentId_0)); }
	inline String_t* get_TournamentId_0() const { return ___TournamentId_0; }
	inline String_t** get_address_of_TournamentId_0() { return &___TournamentId_0; }
	inline void set_TournamentId_0(String_t* value)
	{
		___TournamentId_0 = value;
		Il2CppCodeGenWriteBarrier((&___TournamentId_0), value);
	}

	inline static int32_t get_offset_of_ResponseData_1() { return static_cast<int32_t>(offsetof(ClaimTournamentRewardCommandModel_t4ADDCB2DC663FF9A7A7BF212E7D3764861368E1D, ___ResponseData_1)); }
	inline GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 * get_ResponseData_1() const { return ___ResponseData_1; }
	inline GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 ** get_address_of_ResponseData_1() { return &___ResponseData_1; }
	inline void set_ResponseData_1(GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 * value)
	{
		___ResponseData_1 = value;
		Il2CppCodeGenWriteBarrier((&___ResponseData_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLAIMTOURNAMENTREWARDCOMMANDMODEL_T4ADDCB2DC663FF9A7A7BF212E7D3764861368E1D_H
#ifndef CLAIMTOURNAMENTREWARDCOMMANDRESULT_TECE3DBA1983CEB89B4011D09194CF0ABE5EFD193_H
#define CLAIMTOURNAMENTREWARDCOMMANDRESULT_TECE3DBA1983CEB89B4011D09194CF0ABE5EFD193_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ClaimTournamentRewardCommandResult
struct  ClaimTournamentRewardCommandResult_tECE3DBA1983CEB89B4011D09194CF0ABE5EFD193  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<RewardModel> ClaimTournamentRewardCommandResult::Rewards
	List_1_t05E5F1AEA6DCB3F4DE9EF82A15D37F0095D1F827 * ___Rewards_0;

public:
	inline static int32_t get_offset_of_Rewards_0() { return static_cast<int32_t>(offsetof(ClaimTournamentRewardCommandResult_tECE3DBA1983CEB89B4011D09194CF0ABE5EFD193, ___Rewards_0)); }
	inline List_1_t05E5F1AEA6DCB3F4DE9EF82A15D37F0095D1F827 * get_Rewards_0() const { return ___Rewards_0; }
	inline List_1_t05E5F1AEA6DCB3F4DE9EF82A15D37F0095D1F827 ** get_address_of_Rewards_0() { return &___Rewards_0; }
	inline void set_Rewards_0(List_1_t05E5F1AEA6DCB3F4DE9EF82A15D37F0095D1F827 * value)
	{
		___Rewards_0 = value;
		Il2CppCodeGenWriteBarrier((&___Rewards_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLAIMTOURNAMENTREWARDCOMMANDRESULT_TECE3DBA1983CEB89B4011D09194CF0ABE5EFD193_H
#ifndef CONFMODEL_T22142FE7B971DACF1840795E24BED946C0D8A77F_H
#define CONFMODEL_T22142FE7B971DACF1840795E24BED946C0D8A77F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ConfModel
struct  ConfModel_t22142FE7B971DACF1840795E24BED946C0D8A77F  : public RuntimeObject
{
public:
	// System.String ConfModel::FileName
	String_t* ___FileName_0;
	// System.Type ConfModel::Type
	Type_t * ___Type_1;

public:
	inline static int32_t get_offset_of_FileName_0() { return static_cast<int32_t>(offsetof(ConfModel_t22142FE7B971DACF1840795E24BED946C0D8A77F, ___FileName_0)); }
	inline String_t* get_FileName_0() const { return ___FileName_0; }
	inline String_t** get_address_of_FileName_0() { return &___FileName_0; }
	inline void set_FileName_0(String_t* value)
	{
		___FileName_0 = value;
		Il2CppCodeGenWriteBarrier((&___FileName_0), value);
	}

	inline static int32_t get_offset_of_Type_1() { return static_cast<int32_t>(offsetof(ConfModel_t22142FE7B971DACF1840795E24BED946C0D8A77F, ___Type_1)); }
	inline Type_t * get_Type_1() const { return ___Type_1; }
	inline Type_t ** get_address_of_Type_1() { return &___Type_1; }
	inline void set_Type_1(Type_t * value)
	{
		___Type_1 = value;
		Il2CppCodeGenWriteBarrier((&___Type_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFMODEL_T22142FE7B971DACF1840795E24BED946C0D8A77F_H
#ifndef DUMMYLEADERBOARDMODEL_T86249198E504029DAC0693F7BD6CFB50B007058F_H
#define DUMMYLEADERBOARDMODEL_T86249198E504029DAC0693F7BD6CFB50B007058F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DummyLeaderboardModel
struct  DummyLeaderboardModel_t86249198E504029DAC0693F7BD6CFB50B007058F  : public RuntimeObject
{
public:
	// System.String DummyLeaderboardModel::Name
	String_t* ___Name_0;
	// System.Int32 DummyLeaderboardModel::Score
	int32_t ___Score_1;
	// System.Int32 DummyLeaderboardModel::Icon
	int32_t ___Icon_2;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(DummyLeaderboardModel_t86249198E504029DAC0693F7BD6CFB50B007058F, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_Score_1() { return static_cast<int32_t>(offsetof(DummyLeaderboardModel_t86249198E504029DAC0693F7BD6CFB50B007058F, ___Score_1)); }
	inline int32_t get_Score_1() const { return ___Score_1; }
	inline int32_t* get_address_of_Score_1() { return &___Score_1; }
	inline void set_Score_1(int32_t value)
	{
		___Score_1 = value;
	}

	inline static int32_t get_offset_of_Icon_2() { return static_cast<int32_t>(offsetof(DummyLeaderboardModel_t86249198E504029DAC0693F7BD6CFB50B007058F, ___Icon_2)); }
	inline int32_t get_Icon_2() const { return ___Icon_2; }
	inline int32_t* get_address_of_Icon_2() { return &___Icon_2; }
	inline void set_Icon_2(int32_t value)
	{
		___Icon_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DUMMYLEADERBOARDMODEL_T86249198E504029DAC0693F7BD6CFB50B007058F_H
#ifndef DUMMYLEADERBOARDSO_T874BAA75E6FA341794950FA9B3141529607DDC08_H
#define DUMMYLEADERBOARDSO_T874BAA75E6FA341794950FA9B3141529607DDC08_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DummyLeaderboardSO
struct  DummyLeaderboardSO_t874BAA75E6FA341794950FA9B3141529607DDC08  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<DummyLeaderboardModel> DummyLeaderboardSO::Players
	List_1_t15D6C7E13BDC8A5780D3F7B823F475DBF7785142 * ___Players_0;
	// System.Collections.Generic.List`1<DummyLeaderboardModel> DummyLeaderboardSO::Teams
	List_1_t15D6C7E13BDC8A5780D3F7B823F475DBF7785142 * ___Teams_1;

public:
	inline static int32_t get_offset_of_Players_0() { return static_cast<int32_t>(offsetof(DummyLeaderboardSO_t874BAA75E6FA341794950FA9B3141529607DDC08, ___Players_0)); }
	inline List_1_t15D6C7E13BDC8A5780D3F7B823F475DBF7785142 * get_Players_0() const { return ___Players_0; }
	inline List_1_t15D6C7E13BDC8A5780D3F7B823F475DBF7785142 ** get_address_of_Players_0() { return &___Players_0; }
	inline void set_Players_0(List_1_t15D6C7E13BDC8A5780D3F7B823F475DBF7785142 * value)
	{
		___Players_0 = value;
		Il2CppCodeGenWriteBarrier((&___Players_0), value);
	}

	inline static int32_t get_offset_of_Teams_1() { return static_cast<int32_t>(offsetof(DummyLeaderboardSO_t874BAA75E6FA341794950FA9B3141529607DDC08, ___Teams_1)); }
	inline List_1_t15D6C7E13BDC8A5780D3F7B823F475DBF7785142 * get_Teams_1() const { return ___Teams_1; }
	inline List_1_t15D6C7E13BDC8A5780D3F7B823F475DBF7785142 ** get_address_of_Teams_1() { return &___Teams_1; }
	inline void set_Teams_1(List_1_t15D6C7E13BDC8A5780D3F7B823F475DBF7785142 * value)
	{
		___Teams_1 = value;
		Il2CppCodeGenWriteBarrier((&___Teams_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DUMMYLEADERBOARDSO_T874BAA75E6FA341794950FA9B3141529607DDC08_H
#ifndef EFFECTVISUALMODEL_T4E4CB43B096384FFC7DAC3E6DF1B2DF8DED965DF_H
#define EFFECTVISUALMODEL_T4E4CB43B096384FFC7DAC3E6DF1B2DF8DED965DF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EffectVisualModel
struct  EffectVisualModel_t4E4CB43B096384FFC7DAC3E6DF1B2DF8DED965DF  : public RuntimeObject
{
public:
	// System.String EffectVisualModel::Id
	String_t* ___Id_0;
	// System.Collections.Generic.List`1<EffectVisual> EffectVisualModel::Visuals
	List_1_tA366AAFE63D066448A3C35B822F880C014E71660 * ___Visuals_1;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(EffectVisualModel_t4E4CB43B096384FFC7DAC3E6DF1B2DF8DED965DF, ___Id_0)); }
	inline String_t* get_Id_0() const { return ___Id_0; }
	inline String_t** get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(String_t* value)
	{
		___Id_0 = value;
		Il2CppCodeGenWriteBarrier((&___Id_0), value);
	}

	inline static int32_t get_offset_of_Visuals_1() { return static_cast<int32_t>(offsetof(EffectVisualModel_t4E4CB43B096384FFC7DAC3E6DF1B2DF8DED965DF, ___Visuals_1)); }
	inline List_1_tA366AAFE63D066448A3C35B822F880C014E71660 * get_Visuals_1() const { return ___Visuals_1; }
	inline List_1_tA366AAFE63D066448A3C35B822F880C014E71660 ** get_address_of_Visuals_1() { return &___Visuals_1; }
	inline void set_Visuals_1(List_1_tA366AAFE63D066448A3C35B822F880C014E71660 * value)
	{
		___Visuals_1 = value;
		Il2CppCodeGenWriteBarrier((&___Visuals_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EFFECTVISUALMODEL_T4E4CB43B096384FFC7DAC3E6DF1B2DF8DED965DF_H
#ifndef EFFECTVISUALSO_T1C5856B5E821FD66A4C1BD41DB5194A5610E6FFC_H
#define EFFECTVISUALSO_T1C5856B5E821FD66A4C1BD41DB5194A5610E6FFC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EffectVisualSO
struct  EffectVisualSO_t1C5856B5E821FD66A4C1BD41DB5194A5610E6FFC  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<EffectVisualModel> EffectVisualSO::Visuals
	List_1_tB04C00DC79DD1B4D62E9A575C6F56E38DAD22984 * ___Visuals_0;

public:
	inline static int32_t get_offset_of_Visuals_0() { return static_cast<int32_t>(offsetof(EffectVisualSO_t1C5856B5E821FD66A4C1BD41DB5194A5610E6FFC, ___Visuals_0)); }
	inline List_1_tB04C00DC79DD1B4D62E9A575C6F56E38DAD22984 * get_Visuals_0() const { return ___Visuals_0; }
	inline List_1_tB04C00DC79DD1B4D62E9A575C6F56E38DAD22984 ** get_address_of_Visuals_0() { return &___Visuals_0; }
	inline void set_Visuals_0(List_1_tB04C00DC79DD1B4D62E9A575C6F56E38DAD22984 * value)
	{
		___Visuals_0 = value;
		Il2CppCodeGenWriteBarrier((&___Visuals_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EFFECTVISUALSO_T1C5856B5E821FD66A4C1BD41DB5194A5610E6FFC_H
#ifndef U3CU3EC__DISPLAYCLASS1_0_T7A7BD7F79E3AC1F6B21194D6A498110244521FC9_H
#define U3CU3EC__DISPLAYCLASS1_0_T7A7BD7F79E3AC1F6B21194D6A498110244521FC9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EffectVisualSO_<>c__DisplayClass1_0
struct  U3CU3Ec__DisplayClass1_0_t7A7BD7F79E3AC1F6B21194D6A498110244521FC9  : public RuntimeObject
{
public:
	// System.String EffectVisualSO_<>c__DisplayClass1_0::id
	String_t* ___id_0;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass1_0_t7A7BD7F79E3AC1F6B21194D6A498110244521FC9, ___id_0)); }
	inline String_t* get_id_0() const { return ___id_0; }
	inline String_t** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(String_t* value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier((&___id_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS1_0_T7A7BD7F79E3AC1F6B21194D6A498110244521FC9_H
#ifndef FEATURESSWITCHCONFIG_TBF1848651D45C05C78F36D68CB647F5CB2F802D3_H
#define FEATURESSWITCHCONFIG_TBF1848651D45C05C78F36D68CB647F5CB2F802D3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FeaturesSwitchConfig
struct  FeaturesSwitchConfig_tBF1848651D45C05C78F36D68CB647F5CB2F802D3  : public RuntimeObject
{
public:
	// System.Boolean FeaturesSwitchConfig::TimePressureSystem
	bool ___TimePressureSystem_0;

public:
	inline static int32_t get_offset_of_TimePressureSystem_0() { return static_cast<int32_t>(offsetof(FeaturesSwitchConfig_tBF1848651D45C05C78F36D68CB647F5CB2F802D3, ___TimePressureSystem_0)); }
	inline bool get_TimePressureSystem_0() const { return ___TimePressureSystem_0; }
	inline bool* get_address_of_TimePressureSystem_0() { return &___TimePressureSystem_0; }
	inline void set_TimePressureSystem_0(bool value)
	{
		___TimePressureSystem_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FEATURESSWITCHCONFIG_TBF1848651D45C05C78F36D68CB647F5CB2F802D3_H
#ifndef FILLLIVESCOMMANDMODEL_T4F764E839800C5918E0C8A7B08E464EA8C88F084_H
#define FILLLIVESCOMMANDMODEL_T4F764E839800C5918E0C8A7B08E464EA8C88F084_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FillLivesCommandModel
struct  FillLivesCommandModel_t4F764E839800C5918E0C8A7B08E464EA8C88F084  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILLLIVESCOMMANDMODEL_T4F764E839800C5918E0C8A7B08E464EA8C88F084_H
#ifndef FILLLIVESCOMMANDRESULTMODEL_T917B92ECF3B5C550EBD4B28257BB8F1D58C6D18A_H
#define FILLLIVESCOMMANDRESULTMODEL_T917B92ECF3B5C550EBD4B28257BB8F1D58C6D18A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FillLivesCommandResultModel
struct  FillLivesCommandResultModel_t917B92ECF3B5C550EBD4B28257BB8F1D58C6D18A  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILLLIVESCOMMANDRESULTMODEL_T917B92ECF3B5C550EBD4B28257BB8F1D58C6D18A_H
#ifndef GAMECONSTANTS_T1A630C2F5F2E6945735E954CED4C0833EDAE31CC_H
#define GAMECONSTANTS_T1A630C2F5F2E6945735E954CED4C0833EDAE31CC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameConstants
struct  GameConstants_t1A630C2F5F2E6945735E954CED4C0833EDAE31CC  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMECONSTANTS_T1A630C2F5F2E6945735E954CED4C0833EDAE31CC_H
#ifndef GAMEDIRESOLVER_T866ECA664BF6463B4E179E3635C89291E8FD8A33_H
#define GAMEDIRESOLVER_T866ECA664BF6463B4E179E3635C89291E8FD8A33_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameDiResolver
struct  GameDiResolver_t866ECA664BF6463B4E179E3635C89291E8FD8A33  : public RuntimeObject
{
public:

public:
};

struct GameDiResolver_t866ECA664BF6463B4E179E3635C89291E8FD8A33_StaticFields
{
public:
	// Zenject.DiContainer GameDiResolver::_gameContainer
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____gameContainer_0;
	// Zenject.DiContainer GameDiResolver::_battleContainer
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____battleContainer_1;

public:
	inline static int32_t get_offset_of__gameContainer_0() { return static_cast<int32_t>(offsetof(GameDiResolver_t866ECA664BF6463B4E179E3635C89291E8FD8A33_StaticFields, ____gameContainer_0)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__gameContainer_0() const { return ____gameContainer_0; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__gameContainer_0() { return &____gameContainer_0; }
	inline void set__gameContainer_0(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____gameContainer_0 = value;
		Il2CppCodeGenWriteBarrier((&____gameContainer_0), value);
	}

	inline static int32_t get_offset_of__battleContainer_1() { return static_cast<int32_t>(offsetof(GameDiResolver_t866ECA664BF6463B4E179E3635C89291E8FD8A33_StaticFields, ____battleContainer_1)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__battleContainer_1() const { return ____battleContainer_1; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__battleContainer_1() { return &____battleContainer_1; }
	inline void set__battleContainer_1(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____battleContainer_1 = value;
		Il2CppCodeGenWriteBarrier((&____battleContainer_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEDIRESOLVER_T866ECA664BF6463B4E179E3635C89291E8FD8A33_H
#ifndef GAMEMAINCONFIGS_T0F18F8075F6195A8B2EC2FCB69D934E925EA071A_H
#define GAMEMAINCONFIGS_T0F18F8075F6195A8B2EC2FCB69D934E925EA071A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameMainConfigs
struct  GameMainConfigs_t0F18F8075F6195A8B2EC2FCB69D934E925EA071A  : public RuntimeObject
{
public:

public:
};

struct GameMainConfigs_t0F18F8075F6195A8B2EC2FCB69D934E925EA071A_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,ConfModel> GameMainConfigs::_gameConfigs
	Dictionary_2_tF352C3C4DFF013A39485B78BB272A67BB62D3B9D * ____gameConfigs_0;

public:
	inline static int32_t get_offset_of__gameConfigs_0() { return static_cast<int32_t>(offsetof(GameMainConfigs_t0F18F8075F6195A8B2EC2FCB69D934E925EA071A_StaticFields, ____gameConfigs_0)); }
	inline Dictionary_2_tF352C3C4DFF013A39485B78BB272A67BB62D3B9D * get__gameConfigs_0() const { return ____gameConfigs_0; }
	inline Dictionary_2_tF352C3C4DFF013A39485B78BB272A67BB62D3B9D ** get_address_of__gameConfigs_0() { return &____gameConfigs_0; }
	inline void set__gameConfigs_0(Dictionary_2_tF352C3C4DFF013A39485B78BB272A67BB62D3B9D * value)
	{
		____gameConfigs_0 = value;
		Il2CppCodeGenWriteBarrier((&____gameConfigs_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEMAINCONFIGS_T0F18F8075F6195A8B2EC2FCB69D934E925EA071A_H
#ifndef GAMEPATHRESOLVER_T5EF708FE19C455A8D6940A65EB9A098F50237644_H
#define GAMEPATHRESOLVER_T5EF708FE19C455A8D6940A65EB9A098F50237644_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GamePathResolver
struct  GamePathResolver_t5EF708FE19C455A8D6940A65EB9A098F50237644  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEPATHRESOLVER_T5EF708FE19C455A8D6940A65EB9A098F50237644_H
#ifndef GAMESTATE_TABB839413827DD148BD92CA09A37DA4C24372F36_H
#define GAMESTATE_TABB839413827DD148BD92CA09A37DA4C24372F36_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameState
struct  GameState_tABB839413827DD148BD92CA09A37DA4C24372F36  : public RuntimeObject
{
public:

public:
};

struct GameState_tABB839413827DD148BD92CA09A37DA4C24372F36_StaticFields
{
public:
	// System.Int32 GameState::TeamCount
	int32_t ___TeamCount_0;

public:
	inline static int32_t get_offset_of_TeamCount_0() { return static_cast<int32_t>(offsetof(GameState_tABB839413827DD148BD92CA09A37DA4C24372F36_StaticFields, ___TeamCount_0)); }
	inline int32_t get_TeamCount_0() const { return ___TeamCount_0; }
	inline int32_t* get_address_of_TeamCount_0() { return &___TeamCount_0; }
	inline void set_TeamCount_0(int32_t value)
	{
		___TeamCount_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMESTATE_TABB839413827DD148BD92CA09A37DA4C24372F36_H
#ifndef GLOBALSO_TACFCA803E1D70E0299E50028D7799CE31F402273_H
#define GLOBALSO_TACFCA803E1D70E0299E50028D7799CE31F402273_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GlobalSO
struct  GlobalSO_tACFCA803E1D70E0299E50028D7799CE31F402273  : public RuntimeObject
{
public:
	// System.String GlobalSO::ServerUrl
	String_t* ___ServerUrl_0;
	// System.String GlobalSO::Version
	String_t* ___Version_1;

public:
	inline static int32_t get_offset_of_ServerUrl_0() { return static_cast<int32_t>(offsetof(GlobalSO_tACFCA803E1D70E0299E50028D7799CE31F402273, ___ServerUrl_0)); }
	inline String_t* get_ServerUrl_0() const { return ___ServerUrl_0; }
	inline String_t** get_address_of_ServerUrl_0() { return &___ServerUrl_0; }
	inline void set_ServerUrl_0(String_t* value)
	{
		___ServerUrl_0 = value;
		Il2CppCodeGenWriteBarrier((&___ServerUrl_0), value);
	}

	inline static int32_t get_offset_of_Version_1() { return static_cast<int32_t>(offsetof(GlobalSO_tACFCA803E1D70E0299E50028D7799CE31F402273, ___Version_1)); }
	inline String_t* get_Version_1() const { return ___Version_1; }
	inline String_t** get_address_of_Version_1() { return &___Version_1; }
	inline void set_Version_1(String_t* value)
	{
		___Version_1 = value;
		Il2CppCodeGenWriteBarrier((&___Version_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLOBALSO_TACFCA803E1D70E0299E50028D7799CE31F402273_H
#ifndef HEARTUTILS_T75148D84D8BFBC8993AF840CA04824A4D8E24808_H
#define HEARTUTILS_T75148D84D8BFBC8993AF840CA04824A4D8E24808_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HeartUtils
struct  HeartUtils_t75148D84D8BFBC8993AF840CA04824A4D8E24808  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEARTUTILS_T75148D84D8BFBC8993AF840CA04824A4D8E24808_H
#ifndef ITEMCONTENTMODEL_T7F429490208DE8F65A9F2278897FF14267F9983D_H
#define ITEMCONTENTMODEL_T7F429490208DE8F65A9F2278897FF14267F9983D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ItemContentModel
struct  ItemContentModel_t7F429490208DE8F65A9F2278897FF14267F9983D  : public RuntimeObject
{
public:
	// RewardModel ItemContentModel::Reward
	RewardModel_tE09D75F3B47938652450ABB51F21663745C8BC44 * ___Reward_0;
	// System.Int32 ItemContentModel::Weight
	int32_t ___Weight_1;
	// System.String ItemContentModel::Prefab
	String_t* ___Prefab_2;
	// System.String ItemContentModel::RequirementStr
	String_t* ___RequirementStr_3;

public:
	inline static int32_t get_offset_of_Reward_0() { return static_cast<int32_t>(offsetof(ItemContentModel_t7F429490208DE8F65A9F2278897FF14267F9983D, ___Reward_0)); }
	inline RewardModel_tE09D75F3B47938652450ABB51F21663745C8BC44 * get_Reward_0() const { return ___Reward_0; }
	inline RewardModel_tE09D75F3B47938652450ABB51F21663745C8BC44 ** get_address_of_Reward_0() { return &___Reward_0; }
	inline void set_Reward_0(RewardModel_tE09D75F3B47938652450ABB51F21663745C8BC44 * value)
	{
		___Reward_0 = value;
		Il2CppCodeGenWriteBarrier((&___Reward_0), value);
	}

	inline static int32_t get_offset_of_Weight_1() { return static_cast<int32_t>(offsetof(ItemContentModel_t7F429490208DE8F65A9F2278897FF14267F9983D, ___Weight_1)); }
	inline int32_t get_Weight_1() const { return ___Weight_1; }
	inline int32_t* get_address_of_Weight_1() { return &___Weight_1; }
	inline void set_Weight_1(int32_t value)
	{
		___Weight_1 = value;
	}

	inline static int32_t get_offset_of_Prefab_2() { return static_cast<int32_t>(offsetof(ItemContentModel_t7F429490208DE8F65A9F2278897FF14267F9983D, ___Prefab_2)); }
	inline String_t* get_Prefab_2() const { return ___Prefab_2; }
	inline String_t** get_address_of_Prefab_2() { return &___Prefab_2; }
	inline void set_Prefab_2(String_t* value)
	{
		___Prefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___Prefab_2), value);
	}

	inline static int32_t get_offset_of_RequirementStr_3() { return static_cast<int32_t>(offsetof(ItemContentModel_t7F429490208DE8F65A9F2278897FF14267F9983D, ___RequirementStr_3)); }
	inline String_t* get_RequirementStr_3() const { return ___RequirementStr_3; }
	inline String_t** get_address_of_RequirementStr_3() { return &___RequirementStr_3; }
	inline void set_RequirementStr_3(String_t* value)
	{
		___RequirementStr_3 = value;
		Il2CppCodeGenWriteBarrier((&___RequirementStr_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ITEMCONTENTMODEL_T7F429490208DE8F65A9F2278897FF14267F9983D_H
#ifndef ITEMGENERATIONMODEL_TF18CD55B9CD457553363A05EF1A715F2619E1BA6_H
#define ITEMGENERATIONMODEL_TF18CD55B9CD457553363A05EF1A715F2619E1BA6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ItemGenerationModel
struct  ItemGenerationModel_tF18CD55B9CD457553363A05EF1A715F2619E1BA6  : public RuntimeObject
{
public:
	// System.Int32 ItemGenerationModel::Weight
	int32_t ___Weight_0;
	// System.Int32 ItemGenerationModel::MinCount
	int32_t ___MinCount_1;

public:
	inline static int32_t get_offset_of_Weight_0() { return static_cast<int32_t>(offsetof(ItemGenerationModel_tF18CD55B9CD457553363A05EF1A715F2619E1BA6, ___Weight_0)); }
	inline int32_t get_Weight_0() const { return ___Weight_0; }
	inline int32_t* get_address_of_Weight_0() { return &___Weight_0; }
	inline void set_Weight_0(int32_t value)
	{
		___Weight_0 = value;
	}

	inline static int32_t get_offset_of_MinCount_1() { return static_cast<int32_t>(offsetof(ItemGenerationModel_tF18CD55B9CD457553363A05EF1A715F2619E1BA6, ___MinCount_1)); }
	inline int32_t get_MinCount_1() const { return ___MinCount_1; }
	inline int32_t* get_address_of_MinCount_1() { return &___MinCount_1; }
	inline void set_MinCount_1(int32_t value)
	{
		___MinCount_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ITEMGENERATIONMODEL_TF18CD55B9CD457553363A05EF1A715F2619E1BA6_H
#ifndef U3CU3EC__DISPLAYCLASS1_0_T1B0AB715C679927C205A4F2E17FCB64373E4C730_H
#define U3CU3EC__DISPLAYCLASS1_0_T1B0AB715C679927C205A4F2E17FCB64373E4C730_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ItemsSO_<>c__DisplayClass1_0
struct  U3CU3Ec__DisplayClass1_0_t1B0AB715C679927C205A4F2E17FCB64373E4C730  : public RuntimeObject
{
public:
	// System.String ItemsSO_<>c__DisplayClass1_0::id
	String_t* ___id_0;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass1_0_t1B0AB715C679927C205A4F2E17FCB64373E4C730, ___id_0)); }
	inline String_t* get_id_0() const { return ___id_0; }
	inline String_t** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(String_t* value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier((&___id_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS1_0_T1B0AB715C679927C205A4F2E17FCB64373E4C730_H
#ifndef LEVELMODEL_T5549E807391907F7844D01A4FB4004330AF5E05C_H
#define LEVELMODEL_T5549E807391907F7844D01A4FB4004330AF5E05C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LevelModel
struct  LevelModel_t5549E807391907F7844D01A4FB4004330AF5E05C  : public RuntimeObject
{
public:
	// System.String LevelModel::Level
	String_t* ___Level_0;
	// System.String LevelModel::LevelBundle
	String_t* ___LevelBundle_1;
	// System.String LevelModel::Environment
	String_t* ___Environment_2;
	// System.String LevelModel::EnvironmentBundle
	String_t* ___EnvironmentBundle_3;
	// System.Int32 LevelModel::ItemsCount
	int32_t ___ItemsCount_4;
	// System.Collections.Generic.Dictionary`2<System.String,ItemGenerationModel> LevelModel::ItemsToGenerate
	Dictionary_2_t3174197D3B3D5BB902D450BCB5A8DA1B57C6B057 * ___ItemsToGenerate_5;
	// System.Collections.Generic.List`1<System.String> LevelModel::InventoryItems
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___InventoryItems_6;
	// System.Collections.Generic.Dictionary`2<System.String,LevelObjective> LevelModel::Objectives
	Dictionary_2_t27EDBB09D6B6FEBC5A2B61FA9725D313DA2BB658 * ___Objectives_7;
	// System.Int32 LevelModel::ItemsGenerationDelay
	int32_t ___ItemsGenerationDelay_8;
	// System.Single LevelModel::Duration
	float ___Duration_9;

public:
	inline static int32_t get_offset_of_Level_0() { return static_cast<int32_t>(offsetof(LevelModel_t5549E807391907F7844D01A4FB4004330AF5E05C, ___Level_0)); }
	inline String_t* get_Level_0() const { return ___Level_0; }
	inline String_t** get_address_of_Level_0() { return &___Level_0; }
	inline void set_Level_0(String_t* value)
	{
		___Level_0 = value;
		Il2CppCodeGenWriteBarrier((&___Level_0), value);
	}

	inline static int32_t get_offset_of_LevelBundle_1() { return static_cast<int32_t>(offsetof(LevelModel_t5549E807391907F7844D01A4FB4004330AF5E05C, ___LevelBundle_1)); }
	inline String_t* get_LevelBundle_1() const { return ___LevelBundle_1; }
	inline String_t** get_address_of_LevelBundle_1() { return &___LevelBundle_1; }
	inline void set_LevelBundle_1(String_t* value)
	{
		___LevelBundle_1 = value;
		Il2CppCodeGenWriteBarrier((&___LevelBundle_1), value);
	}

	inline static int32_t get_offset_of_Environment_2() { return static_cast<int32_t>(offsetof(LevelModel_t5549E807391907F7844D01A4FB4004330AF5E05C, ___Environment_2)); }
	inline String_t* get_Environment_2() const { return ___Environment_2; }
	inline String_t** get_address_of_Environment_2() { return &___Environment_2; }
	inline void set_Environment_2(String_t* value)
	{
		___Environment_2 = value;
		Il2CppCodeGenWriteBarrier((&___Environment_2), value);
	}

	inline static int32_t get_offset_of_EnvironmentBundle_3() { return static_cast<int32_t>(offsetof(LevelModel_t5549E807391907F7844D01A4FB4004330AF5E05C, ___EnvironmentBundle_3)); }
	inline String_t* get_EnvironmentBundle_3() const { return ___EnvironmentBundle_3; }
	inline String_t** get_address_of_EnvironmentBundle_3() { return &___EnvironmentBundle_3; }
	inline void set_EnvironmentBundle_3(String_t* value)
	{
		___EnvironmentBundle_3 = value;
		Il2CppCodeGenWriteBarrier((&___EnvironmentBundle_3), value);
	}

	inline static int32_t get_offset_of_ItemsCount_4() { return static_cast<int32_t>(offsetof(LevelModel_t5549E807391907F7844D01A4FB4004330AF5E05C, ___ItemsCount_4)); }
	inline int32_t get_ItemsCount_4() const { return ___ItemsCount_4; }
	inline int32_t* get_address_of_ItemsCount_4() { return &___ItemsCount_4; }
	inline void set_ItemsCount_4(int32_t value)
	{
		___ItemsCount_4 = value;
	}

	inline static int32_t get_offset_of_ItemsToGenerate_5() { return static_cast<int32_t>(offsetof(LevelModel_t5549E807391907F7844D01A4FB4004330AF5E05C, ___ItemsToGenerate_5)); }
	inline Dictionary_2_t3174197D3B3D5BB902D450BCB5A8DA1B57C6B057 * get_ItemsToGenerate_5() const { return ___ItemsToGenerate_5; }
	inline Dictionary_2_t3174197D3B3D5BB902D450BCB5A8DA1B57C6B057 ** get_address_of_ItemsToGenerate_5() { return &___ItemsToGenerate_5; }
	inline void set_ItemsToGenerate_5(Dictionary_2_t3174197D3B3D5BB902D450BCB5A8DA1B57C6B057 * value)
	{
		___ItemsToGenerate_5 = value;
		Il2CppCodeGenWriteBarrier((&___ItemsToGenerate_5), value);
	}

	inline static int32_t get_offset_of_InventoryItems_6() { return static_cast<int32_t>(offsetof(LevelModel_t5549E807391907F7844D01A4FB4004330AF5E05C, ___InventoryItems_6)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_InventoryItems_6() const { return ___InventoryItems_6; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_InventoryItems_6() { return &___InventoryItems_6; }
	inline void set_InventoryItems_6(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___InventoryItems_6 = value;
		Il2CppCodeGenWriteBarrier((&___InventoryItems_6), value);
	}

	inline static int32_t get_offset_of_Objectives_7() { return static_cast<int32_t>(offsetof(LevelModel_t5549E807391907F7844D01A4FB4004330AF5E05C, ___Objectives_7)); }
	inline Dictionary_2_t27EDBB09D6B6FEBC5A2B61FA9725D313DA2BB658 * get_Objectives_7() const { return ___Objectives_7; }
	inline Dictionary_2_t27EDBB09D6B6FEBC5A2B61FA9725D313DA2BB658 ** get_address_of_Objectives_7() { return &___Objectives_7; }
	inline void set_Objectives_7(Dictionary_2_t27EDBB09D6B6FEBC5A2B61FA9725D313DA2BB658 * value)
	{
		___Objectives_7 = value;
		Il2CppCodeGenWriteBarrier((&___Objectives_7), value);
	}

	inline static int32_t get_offset_of_ItemsGenerationDelay_8() { return static_cast<int32_t>(offsetof(LevelModel_t5549E807391907F7844D01A4FB4004330AF5E05C, ___ItemsGenerationDelay_8)); }
	inline int32_t get_ItemsGenerationDelay_8() const { return ___ItemsGenerationDelay_8; }
	inline int32_t* get_address_of_ItemsGenerationDelay_8() { return &___ItemsGenerationDelay_8; }
	inline void set_ItemsGenerationDelay_8(int32_t value)
	{
		___ItemsGenerationDelay_8 = value;
	}

	inline static int32_t get_offset_of_Duration_9() { return static_cast<int32_t>(offsetof(LevelModel_t5549E807391907F7844D01A4FB4004330AF5E05C, ___Duration_9)); }
	inline float get_Duration_9() const { return ___Duration_9; }
	inline float* get_address_of_Duration_9() { return &___Duration_9; }
	inline void set_Duration_9(float value)
	{
		___Duration_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELMODEL_T5549E807391907F7844D01A4FB4004330AF5E05C_H
#ifndef LOGINCOMMANDMODEL_T24D0303EC2BAA2132036E25E25AB91AC1FFACCD0_H
#define LOGINCOMMANDMODEL_T24D0303EC2BAA2132036E25E25AB91AC1FFACCD0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoginCommandModel
struct  LoginCommandModel_t24D0303EC2BAA2132036E25E25AB91AC1FFACCD0  : public RuntimeObject
{
public:
	// GameSparks.Core.GSData LoginCommandModel::ResponseData
	GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 * ___ResponseData_0;

public:
	inline static int32_t get_offset_of_ResponseData_0() { return static_cast<int32_t>(offsetof(LoginCommandModel_t24D0303EC2BAA2132036E25E25AB91AC1FFACCD0, ___ResponseData_0)); }
	inline GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 * get_ResponseData_0() const { return ___ResponseData_0; }
	inline GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 ** get_address_of_ResponseData_0() { return &___ResponseData_0; }
	inline void set_ResponseData_0(GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 * value)
	{
		___ResponseData_0 = value;
		Il2CppCodeGenWriteBarrier((&___ResponseData_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGINCOMMANDMODEL_T24D0303EC2BAA2132036E25E25AB91AC1FFACCD0_H
#ifndef LOGINCOMMANDRESULTMODEL_T397DF3FF54EC195B423F09100D36D1A4A7D78893_H
#define LOGINCOMMANDRESULTMODEL_T397DF3FF54EC195B423F09100D36D1A4A7D78893_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoginCommandResultModel
struct  LoginCommandResultModel_t397DF3FF54EC195B423F09100D36D1A4A7D78893  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGINCOMMANDRESULTMODEL_T397DF3FF54EC195B423F09100D36D1A4A7D78893_H
#ifndef PATCHINGCOMPLETEDCOMMANDMODEL_TE4A47B81CCDC996363955B3FC4E4EF1B06E1A8F4_H
#define PATCHINGCOMPLETEDCOMMANDMODEL_TE4A47B81CCDC996363955B3FC4E4EF1B06E1A8F4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PatchingCompletedCommandModel
struct  PatchingCompletedCommandModel_tE4A47B81CCDC996363955B3FC4E4EF1B06E1A8F4  : public RuntimeObject
{
public:
	// System.String PatchingCompletedCommandModel::Hash
	String_t* ___Hash_0;
	// System.String PatchingCompletedCommandModel::Version
	String_t* ___Version_1;

public:
	inline static int32_t get_offset_of_Hash_0() { return static_cast<int32_t>(offsetof(PatchingCompletedCommandModel_tE4A47B81CCDC996363955B3FC4E4EF1B06E1A8F4, ___Hash_0)); }
	inline String_t* get_Hash_0() const { return ___Hash_0; }
	inline String_t** get_address_of_Hash_0() { return &___Hash_0; }
	inline void set_Hash_0(String_t* value)
	{
		___Hash_0 = value;
		Il2CppCodeGenWriteBarrier((&___Hash_0), value);
	}

	inline static int32_t get_offset_of_Version_1() { return static_cast<int32_t>(offsetof(PatchingCompletedCommandModel_tE4A47B81CCDC996363955B3FC4E4EF1B06E1A8F4, ___Version_1)); }
	inline String_t* get_Version_1() const { return ___Version_1; }
	inline String_t** get_address_of_Version_1() { return &___Version_1; }
	inline void set_Version_1(String_t* value)
	{
		___Version_1 = value;
		Il2CppCodeGenWriteBarrier((&___Version_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATCHINGCOMPLETEDCOMMANDMODEL_TE4A47B81CCDC996363955B3FC4E4EF1B06E1A8F4_H
#ifndef PATCHINGCOMPLETEDCOMMANDMODELRESULT_T96CB76D8E153FAA3C0D2F43327591BC428EC6CF5_H
#define PATCHINGCOMPLETEDCOMMANDMODELRESULT_T96CB76D8E153FAA3C0D2F43327591BC428EC6CF5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PatchingCompletedCommandModelResult
struct  PatchingCompletedCommandModelResult_t96CB76D8E153FAA3C0D2F43327591BC428EC6CF5  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATCHINGCOMPLETEDCOMMANDMODELRESULT_T96CB76D8E153FAA3C0D2F43327591BC428EC6CF5_H
#ifndef PATCHINGUPDATEDCOMMANDMODEL_T2445D63426E3FC82073263F556AA8C4103A4E4C1_H
#define PATCHINGUPDATEDCOMMANDMODEL_T2445D63426E3FC82073263F556AA8C4103A4E4C1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PatchingUpdatedCommandModel
struct  PatchingUpdatedCommandModel_t2445D63426E3FC82073263F556AA8C4103A4E4C1  : public RuntimeObject
{
public:
	// System.String PatchingUpdatedCommandModel::Hash
	String_t* ___Hash_0;
	// System.String PatchingUpdatedCommandModel::Version
	String_t* ___Version_1;
	// System.Collections.Generic.List`1<System.String> PatchingUpdatedCommandModel::MissingAssets
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___MissingAssets_2;

public:
	inline static int32_t get_offset_of_Hash_0() { return static_cast<int32_t>(offsetof(PatchingUpdatedCommandModel_t2445D63426E3FC82073263F556AA8C4103A4E4C1, ___Hash_0)); }
	inline String_t* get_Hash_0() const { return ___Hash_0; }
	inline String_t** get_address_of_Hash_0() { return &___Hash_0; }
	inline void set_Hash_0(String_t* value)
	{
		___Hash_0 = value;
		Il2CppCodeGenWriteBarrier((&___Hash_0), value);
	}

	inline static int32_t get_offset_of_Version_1() { return static_cast<int32_t>(offsetof(PatchingUpdatedCommandModel_t2445D63426E3FC82073263F556AA8C4103A4E4C1, ___Version_1)); }
	inline String_t* get_Version_1() const { return ___Version_1; }
	inline String_t** get_address_of_Version_1() { return &___Version_1; }
	inline void set_Version_1(String_t* value)
	{
		___Version_1 = value;
		Il2CppCodeGenWriteBarrier((&___Version_1), value);
	}

	inline static int32_t get_offset_of_MissingAssets_2() { return static_cast<int32_t>(offsetof(PatchingUpdatedCommandModel_t2445D63426E3FC82073263F556AA8C4103A4E4C1, ___MissingAssets_2)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_MissingAssets_2() const { return ___MissingAssets_2; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_MissingAssets_2() { return &___MissingAssets_2; }
	inline void set_MissingAssets_2(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___MissingAssets_2 = value;
		Il2CppCodeGenWriteBarrier((&___MissingAssets_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATCHINGUPDATEDCOMMANDMODEL_T2445D63426E3FC82073263F556AA8C4103A4E4C1_H
#ifndef PATCHINGUPDATEDCOMMANDRESULTMODEL_T2B339406188ABE674F022106438A21C1474AD480_H
#define PATCHINGUPDATEDCOMMANDRESULTMODEL_T2B339406188ABE674F022106438A21C1474AD480_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PatchingUpdatedCommandResultModel
struct  PatchingUpdatedCommandResultModel_t2B339406188ABE674F022106438A21C1474AD480  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATCHINGUPDATEDCOMMANDRESULTMODEL_T2B339406188ABE674F022106438A21C1474AD480_H
#ifndef PERLEVELTUTORIALMODEL_T617000CC336C47983E77A6E6171F0DE08379A61C_H
#define PERLEVELTUTORIALMODEL_T617000CC336C47983E77A6E6171F0DE08379A61C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PerLevelTutorialModel
struct  PerLevelTutorialModel_t617000CC336C47983E77A6E6171F0DE08379A61C  : public RuntimeObject
{
public:
	// System.String PerLevelTutorialModel::Id
	String_t* ___Id_0;
	// Tayr.TriggerModel PerLevelTutorialModel::_startTigger
	TriggerModel_t09B711CC906A9A76F767318AF6754B20F253399E * ____startTigger_1;
	// System.String PerLevelTutorialModel::StartTriggerStr
	String_t* ___StartTriggerStr_2;
	// DataDictionary PerLevelTutorialModel::Data
	DataDictionary_t9D4A0BE16CC0324BF2D47F35B50C65CC9B44E606 * ___Data_3;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(PerLevelTutorialModel_t617000CC336C47983E77A6E6171F0DE08379A61C, ___Id_0)); }
	inline String_t* get_Id_0() const { return ___Id_0; }
	inline String_t** get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(String_t* value)
	{
		___Id_0 = value;
		Il2CppCodeGenWriteBarrier((&___Id_0), value);
	}

	inline static int32_t get_offset_of__startTigger_1() { return static_cast<int32_t>(offsetof(PerLevelTutorialModel_t617000CC336C47983E77A6E6171F0DE08379A61C, ____startTigger_1)); }
	inline TriggerModel_t09B711CC906A9A76F767318AF6754B20F253399E * get__startTigger_1() const { return ____startTigger_1; }
	inline TriggerModel_t09B711CC906A9A76F767318AF6754B20F253399E ** get_address_of__startTigger_1() { return &____startTigger_1; }
	inline void set__startTigger_1(TriggerModel_t09B711CC906A9A76F767318AF6754B20F253399E * value)
	{
		____startTigger_1 = value;
		Il2CppCodeGenWriteBarrier((&____startTigger_1), value);
	}

	inline static int32_t get_offset_of_StartTriggerStr_2() { return static_cast<int32_t>(offsetof(PerLevelTutorialModel_t617000CC336C47983E77A6E6171F0DE08379A61C, ___StartTriggerStr_2)); }
	inline String_t* get_StartTriggerStr_2() const { return ___StartTriggerStr_2; }
	inline String_t** get_address_of_StartTriggerStr_2() { return &___StartTriggerStr_2; }
	inline void set_StartTriggerStr_2(String_t* value)
	{
		___StartTriggerStr_2 = value;
		Il2CppCodeGenWriteBarrier((&___StartTriggerStr_2), value);
	}

	inline static int32_t get_offset_of_Data_3() { return static_cast<int32_t>(offsetof(PerLevelTutorialModel_t617000CC336C47983E77A6E6171F0DE08379A61C, ___Data_3)); }
	inline DataDictionary_t9D4A0BE16CC0324BF2D47F35B50C65CC9B44E606 * get_Data_3() const { return ___Data_3; }
	inline DataDictionary_t9D4A0BE16CC0324BF2D47F35B50C65CC9B44E606 ** get_address_of_Data_3() { return &___Data_3; }
	inline void set_Data_3(DataDictionary_t9D4A0BE16CC0324BF2D47F35B50C65CC9B44E606 * value)
	{
		___Data_3 = value;
		Il2CppCodeGenWriteBarrier((&___Data_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PERLEVELTUTORIALMODEL_T617000CC336C47983E77A6E6171F0DE08379A61C_H
#ifndef U3CU3EC__DISPLAYCLASS1_0_T9E186A929A5B89C2CF8480323CF4CCE355ED23F8_H
#define U3CU3EC__DISPLAYCLASS1_0_T9E186A929A5B89C2CF8480323CF4CCE355ED23F8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PerLevelTutorialSO_<>c__DisplayClass1_0
struct  U3CU3Ec__DisplayClass1_0_t9E186A929A5B89C2CF8480323CF4CCE355ED23F8  : public RuntimeObject
{
public:
	// System.String PerLevelTutorialSO_<>c__DisplayClass1_0::id
	String_t* ___id_0;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass1_0_t9E186A929A5B89C2CF8480323CF4CCE355ED23F8, ___id_0)); }
	inline String_t* get_id_0() const { return ___id_0; }
	inline String_t** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(String_t* value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier((&___id_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS1_0_T9E186A929A5B89C2CF8480323CF4CCE355ED23F8_H
#ifndef DRAWABLEDICTIONARY_T5EE13D6B57AC0486747142D13BD761EE7C87F78F_H
#define DRAWABLEDICTIONARY_T5EE13D6B57AC0486747142D13BD761EE7C87F78F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotaryHeart.Lib.SerializableDictionary.DrawableDictionary
struct  DrawableDictionary_t5EE13D6B57AC0486747142D13BD761EE7C87F78F  : public RuntimeObject
{
public:
	// RotaryHeart.Lib.SerializableDictionary.ReorderableList RotaryHeart.Lib.SerializableDictionary.DrawableDictionary::reorderableList
	ReorderableList_tE9342433971E09B8B941B8BDB654FBCACD8839D0 * ___reorderableList_0;
	// RotaryHeart.Lib.SerializableDictionary.RequiredReferences RotaryHeart.Lib.SerializableDictionary.DrawableDictionary::reqReferences
	RequiredReferences_tD502DA241623B0039A32F954E9BB8040D038F5B0 * ___reqReferences_1;
	// System.Boolean RotaryHeart.Lib.SerializableDictionary.DrawableDictionary::isExpanded
	bool ___isExpanded_2;

public:
	inline static int32_t get_offset_of_reorderableList_0() { return static_cast<int32_t>(offsetof(DrawableDictionary_t5EE13D6B57AC0486747142D13BD761EE7C87F78F, ___reorderableList_0)); }
	inline ReorderableList_tE9342433971E09B8B941B8BDB654FBCACD8839D0 * get_reorderableList_0() const { return ___reorderableList_0; }
	inline ReorderableList_tE9342433971E09B8B941B8BDB654FBCACD8839D0 ** get_address_of_reorderableList_0() { return &___reorderableList_0; }
	inline void set_reorderableList_0(ReorderableList_tE9342433971E09B8B941B8BDB654FBCACD8839D0 * value)
	{
		___reorderableList_0 = value;
		Il2CppCodeGenWriteBarrier((&___reorderableList_0), value);
	}

	inline static int32_t get_offset_of_reqReferences_1() { return static_cast<int32_t>(offsetof(DrawableDictionary_t5EE13D6B57AC0486747142D13BD761EE7C87F78F, ___reqReferences_1)); }
	inline RequiredReferences_tD502DA241623B0039A32F954E9BB8040D038F5B0 * get_reqReferences_1() const { return ___reqReferences_1; }
	inline RequiredReferences_tD502DA241623B0039A32F954E9BB8040D038F5B0 ** get_address_of_reqReferences_1() { return &___reqReferences_1; }
	inline void set_reqReferences_1(RequiredReferences_tD502DA241623B0039A32F954E9BB8040D038F5B0 * value)
	{
		___reqReferences_1 = value;
		Il2CppCodeGenWriteBarrier((&___reqReferences_1), value);
	}

	inline static int32_t get_offset_of_isExpanded_2() { return static_cast<int32_t>(offsetof(DrawableDictionary_t5EE13D6B57AC0486747142D13BD761EE7C87F78F, ___isExpanded_2)); }
	inline bool get_isExpanded_2() const { return ___isExpanded_2; }
	inline bool* get_address_of_isExpanded_2() { return &___isExpanded_2; }
	inline void set_isExpanded_2(bool value)
	{
		___isExpanded_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAWABLEDICTIONARY_T5EE13D6B57AC0486747142D13BD761EE7C87F78F_H
#ifndef U3CU3EC__DISPLAYCLASS1_0_TC85EADDF907753C4C6CAF2A466928E0776A9754D_H
#define U3CU3EC__DISPLAYCLASS1_0_TC85EADDF907753C4C6CAF2A466928E0776A9754D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpellsSO_<>c__DisplayClass1_0
struct  U3CU3Ec__DisplayClass1_0_tC85EADDF907753C4C6CAF2A466928E0776A9754D  : public RuntimeObject
{
public:
	// System.String SpellsSO_<>c__DisplayClass1_0::id
	String_t* ___id_0;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass1_0_tC85EADDF907753C4C6CAF2A466928E0776A9754D, ___id_0)); }
	inline String_t* get_id_0() const { return ___id_0; }
	inline String_t** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(String_t* value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier((&___id_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS1_0_TC85EADDF907753C4C6CAF2A466928E0776A9754D_H
#ifndef STARCHESTCOMMANDMODEL_T409270279AE02F8F9215517A80707E19E7700234_H
#define STARCHESTCOMMANDMODEL_T409270279AE02F8F9215517A80707E19E7700234_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StarChestCommandModel
struct  StarChestCommandModel_t409270279AE02F8F9215517A80707E19E7700234  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STARCHESTCOMMANDMODEL_T409270279AE02F8F9215517A80707E19E7700234_H
#ifndef STARCHESTCOMMANDRESULTMODEL_T1C69C4D7DFAE2E5B169B6C02222DB48508EA2191_H
#define STARCHESTCOMMANDRESULTMODEL_T1C69C4D7DFAE2E5B169B6C02222DB48508EA2191_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StarChestCommandResultModel
struct  StarChestCommandResultModel_t1C69C4D7DFAE2E5B169B6C02222DB48508EA2191  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<ItemContentModel> StarChestCommandResultModel::Rewards
	List_1_t2C6CF342B96E130723D45845A1FD2053CBAF0F27 * ___Rewards_0;

public:
	inline static int32_t get_offset_of_Rewards_0() { return static_cast<int32_t>(offsetof(StarChestCommandResultModel_t1C69C4D7DFAE2E5B169B6C02222DB48508EA2191, ___Rewards_0)); }
	inline List_1_t2C6CF342B96E130723D45845A1FD2053CBAF0F27 * get_Rewards_0() const { return ___Rewards_0; }
	inline List_1_t2C6CF342B96E130723D45845A1FD2053CBAF0F27 ** get_address_of_Rewards_0() { return &___Rewards_0; }
	inline void set_Rewards_0(List_1_t2C6CF342B96E130723D45845A1FD2053CBAF0F27 * value)
	{
		___Rewards_0 = value;
		Il2CppCodeGenWriteBarrier((&___Rewards_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STARCHESTCOMMANDRESULTMODEL_T1C69C4D7DFAE2E5B169B6C02222DB48508EA2191_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef BASICCOMMAND_2_T7396A66A96AF85C35291CC1CAD86F570239D1790_H
#define BASICCOMMAND_2_T7396A66A96AF85C35291CC1CAD86F570239D1790_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.BasicCommand`2<ClaimTournamentRewardCommandModel,ClaimTournamentRewardCommandResult>
struct  BasicCommand_2_t7396A66A96AF85C35291CC1CAD86F570239D1790  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICCOMMAND_2_T7396A66A96AF85C35291CC1CAD86F570239D1790_H
#ifndef BASICCOMMAND_2_TF760A423B08F23BB56400BBB5A32726635FB6B89_H
#define BASICCOMMAND_2_TF760A423B08F23BB56400BBB5A32726635FB6B89_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.BasicCommand`2<FillLivesCommandModel,FillLivesCommandResultModel>
struct  BasicCommand_2_tF760A423B08F23BB56400BBB5A32726635FB6B89  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICCOMMAND_2_TF760A423B08F23BB56400BBB5A32726635FB6B89_H
#ifndef BASICCOMMAND_2_T93C8E8B4AEABF7BBFA44068A405F96B75A1C7A19_H
#define BASICCOMMAND_2_T93C8E8B4AEABF7BBFA44068A405F96B75A1C7A19_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.BasicCommand`2<LoginCommandModel,LoginCommandResultModel>
struct  BasicCommand_2_t93C8E8B4AEABF7BBFA44068A405F96B75A1C7A19  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICCOMMAND_2_T93C8E8B4AEABF7BBFA44068A405F96B75A1C7A19_H
#ifndef BASICCOMMAND_2_T40DF06C9EDDFF21881BFD80EC0C0A8ED55335254_H
#define BASICCOMMAND_2_T40DF06C9EDDFF21881BFD80EC0C0A8ED55335254_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.BasicCommand`2<PatchingCompletedCommandModel,PatchingCompletedCommandModelResult>
struct  BasicCommand_2_t40DF06C9EDDFF21881BFD80EC0C0A8ED55335254  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICCOMMAND_2_T40DF06C9EDDFF21881BFD80EC0C0A8ED55335254_H
#ifndef BASICCOMMAND_2_T45D3A9467B05F5FEA9C7C53B615AA70747D07CD0_H
#define BASICCOMMAND_2_T45D3A9467B05F5FEA9C7C53B615AA70747D07CD0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.BasicCommand`2<PatchingUpdatedCommandModel,PatchingUpdatedCommandResultModel>
struct  BasicCommand_2_t45D3A9467B05F5FEA9C7C53B615AA70747D07CD0  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICCOMMAND_2_T45D3A9467B05F5FEA9C7C53B615AA70747D07CD0_H
#ifndef BASICCOMMAND_2_TE4347BF76EF1E47266E872052FF1D524D3906CDE_H
#define BASICCOMMAND_2_TE4347BF76EF1E47266E872052FF1D524D3906CDE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.BasicCommand`2<StarChestCommandModel,StarChestCommandResultModel>
struct  BasicCommand_2_tE4347BF76EF1E47266E872052FF1D524D3906CDE  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICCOMMAND_2_TE4347BF76EF1E47266E872052FF1D524D3906CDE_H
#ifndef BASICCOMMAND_2_T5F9555B3F91A98758F458742D8E775E977CAF4A5_H
#define BASICCOMMAND_2_T5F9555B3F91A98758F458742D8E775E977CAF4A5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.BasicCommand`2<TournamentResultCommandModel,TournamentResultCommandModelResult>
struct  BasicCommand_2_t5F9555B3F91A98758F458742D8E775E977CAF4A5  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICCOMMAND_2_T5F9555B3F91A98758F458742D8E775E977CAF4A5_H
#ifndef BASICCOMMAND_2_T0921118066C3ABAE6A6B2B11AC724538F4FBAF44_H
#define BASICCOMMAND_2_T0921118066C3ABAE6A6B2B11AC724538F4FBAF44_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.BasicCommand`2<TutorialCompletedCommandModel,TutorialCompletedCommandResult>
struct  BasicCommand_2_t0921118066C3ABAE6A6B2B11AC724538F4FBAF44  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICCOMMAND_2_T0921118066C3ABAE6A6B2B11AC724538F4FBAF44_H
#ifndef BASICCOMMAND_2_T2E6968A2415C59A1010CC28B656E8DBF8D3AF542_H
#define BASICCOMMAND_2_T2E6968A2415C59A1010CC28B656E8DBF8D3AF542_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.BasicCommand`2<UpdateSettingsCommandModel,UpdateSettingsCommandResult>
struct  BasicCommand_2_t2E6968A2415C59A1010CC28B656E8DBF8D3AF542  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICCOMMAND_2_T2E6968A2415C59A1010CC28B656E8DBF8D3AF542_H
#ifndef TOURNAMENTRESULTCOMMANDMODEL_T85DE82B1305AA756766D74891EF4404FF50367B6_H
#define TOURNAMENTRESULTCOMMANDMODEL_T85DE82B1305AA756766D74891EF4404FF50367B6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TournamentResultCommandModel
struct  TournamentResultCommandModel_t85DE82B1305AA756766D74891EF4404FF50367B6  : public RuntimeObject
{
public:
	// GameSparks.Core.GSData TournamentResultCommandModel::ResponseData
	GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 * ___ResponseData_0;

public:
	inline static int32_t get_offset_of_ResponseData_0() { return static_cast<int32_t>(offsetof(TournamentResultCommandModel_t85DE82B1305AA756766D74891EF4404FF50367B6, ___ResponseData_0)); }
	inline GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 * get_ResponseData_0() const { return ___ResponseData_0; }
	inline GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 ** get_address_of_ResponseData_0() { return &___ResponseData_0; }
	inline void set_ResponseData_0(GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 * value)
	{
		___ResponseData_0 = value;
		Il2CppCodeGenWriteBarrier((&___ResponseData_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOURNAMENTRESULTCOMMANDMODEL_T85DE82B1305AA756766D74891EF4404FF50367B6_H
#ifndef TOURNAMENTRESULTCOMMANDMODELRESULT_TCCEC4575292DCA9D77CE4E9D594BA31EDE162F77_H
#define TOURNAMENTRESULTCOMMANDMODELRESULT_TCCEC4575292DCA9D77CE4E9D594BA31EDE162F77_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TournamentResultCommandModelResult
struct  TournamentResultCommandModelResult_tCCEC4575292DCA9D77CE4E9D594BA31EDE162F77  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOURNAMENTRESULTCOMMANDMODELRESULT_TCCEC4575292DCA9D77CE4E9D594BA31EDE162F77_H
#ifndef U3CU3EC__DISPLAYCLASS1_0_TAE58CA8E50F9CCB21ED71CCCCA1E1EDF56A9F5CE_H
#define U3CU3EC__DISPLAYCLASS1_0_TAE58CA8E50F9CCB21ED71CCCCA1E1EDF56A9F5CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TowersSO_<>c__DisplayClass1_0
struct  U3CU3Ec__DisplayClass1_0_tAE58CA8E50F9CCB21ED71CCCCA1E1EDF56A9F5CE  : public RuntimeObject
{
public:
	// System.String TowersSO_<>c__DisplayClass1_0::id
	String_t* ___id_0;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass1_0_tAE58CA8E50F9CCB21ED71CCCCA1E1EDF56A9F5CE, ___id_0)); }
	inline String_t* get_id_0() const { return ___id_0; }
	inline String_t** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(String_t* value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier((&___id_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS1_0_TAE58CA8E50F9CCB21ED71CCCCA1E1EDF56A9F5CE_H
#ifndef U3CU3EC__DISPLAYCLASS1_0_T04C535B8B5D127AD9A39CBB909D40690C6014560_H
#define U3CU3EC__DISPLAYCLASS1_0_T04C535B8B5D127AD9A39CBB909D40690C6014560_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TrapsSO_<>c__DisplayClass1_0
struct  U3CU3Ec__DisplayClass1_0_t04C535B8B5D127AD9A39CBB909D40690C6014560  : public RuntimeObject
{
public:
	// System.String TrapsSO_<>c__DisplayClass1_0::id
	String_t* ___id_0;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass1_0_t04C535B8B5D127AD9A39CBB909D40690C6014560, ___id_0)); }
	inline String_t* get_id_0() const { return ___id_0; }
	inline String_t** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(String_t* value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier((&___id_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS1_0_T04C535B8B5D127AD9A39CBB909D40690C6014560_H
#ifndef TUTORIALCOMPLETEDCOMMANDMODEL_T8E735DB75DAEA8DB6988603694DAE3DC5A702561_H
#define TUTORIALCOMPLETEDCOMMANDMODEL_T8E735DB75DAEA8DB6988603694DAE3DC5A702561_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TutorialCompletedCommandModel
struct  TutorialCompletedCommandModel_t8E735DB75DAEA8DB6988603694DAE3DC5A702561  : public RuntimeObject
{
public:
	// System.String TutorialCompletedCommandModel::TutorialId
	String_t* ___TutorialId_0;

public:
	inline static int32_t get_offset_of_TutorialId_0() { return static_cast<int32_t>(offsetof(TutorialCompletedCommandModel_t8E735DB75DAEA8DB6988603694DAE3DC5A702561, ___TutorialId_0)); }
	inline String_t* get_TutorialId_0() const { return ___TutorialId_0; }
	inline String_t** get_address_of_TutorialId_0() { return &___TutorialId_0; }
	inline void set_TutorialId_0(String_t* value)
	{
		___TutorialId_0 = value;
		Il2CppCodeGenWriteBarrier((&___TutorialId_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUTORIALCOMPLETEDCOMMANDMODEL_T8E735DB75DAEA8DB6988603694DAE3DC5A702561_H
#ifndef TUTORIALCOMPLETEDCOMMANDRESULT_T28E5178ED159419CFF37D5F86599383281ABF500_H
#define TUTORIALCOMPLETEDCOMMANDRESULT_T28E5178ED159419CFF37D5F86599383281ABF500_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TutorialCompletedCommandResult
struct  TutorialCompletedCommandResult_t28E5178ED159419CFF37D5F86599383281ABF500  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUTORIALCOMPLETEDCOMMANDRESULT_T28E5178ED159419CFF37D5F86599383281ABF500_H
#ifndef U3CU3EC__DISPLAYCLASS1_0_TF59194C24E02C70B72472D026137782318AB9B1B_H
#define U3CU3EC__DISPLAYCLASS1_0_TF59194C24E02C70B72472D026137782318AB9B1B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnitsSO_<>c__DisplayClass1_0
struct  U3CU3Ec__DisplayClass1_0_tF59194C24E02C70B72472D026137782318AB9B1B  : public RuntimeObject
{
public:
	// System.String UnitsSO_<>c__DisplayClass1_0::id
	String_t* ___id_0;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass1_0_tF59194C24E02C70B72472D026137782318AB9B1B, ___id_0)); }
	inline String_t* get_id_0() const { return ___id_0; }
	inline String_t** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(String_t* value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier((&___id_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS1_0_TF59194C24E02C70B72472D026137782318AB9B1B_H
#ifndef UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#define UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * ___m_PersistentCalls_1;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_2;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_Calls_0)); }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_CallsDirty_2)); }
	inline bool get_m_CallsDirty_2() const { return ___m_CallsDirty_2; }
	inline bool* get_address_of_m_CallsDirty_2() { return &___m_CallsDirty_2; }
	inline void set_m_CallsDirty_2(bool value)
	{
		___m_CallsDirty_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#ifndef UPDATESETTINGSCOMMANDMODEL_TCBDA27C0FF5752D47D9532DDDB23FA2611F9E040_H
#define UPDATESETTINGSCOMMANDMODEL_TCBDA27C0FF5752D47D9532DDDB23FA2611F9E040_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UpdateSettingsCommandModel
struct  UpdateSettingsCommandModel_tCBDA27C0FF5752D47D9532DDDB23FA2611F9E040  : public RuntimeObject
{
public:
	// System.Boolean UpdateSettingsCommandModel::Fx
	bool ___Fx_0;
	// System.Boolean UpdateSettingsCommandModel::Music
	bool ___Music_1;

public:
	inline static int32_t get_offset_of_Fx_0() { return static_cast<int32_t>(offsetof(UpdateSettingsCommandModel_tCBDA27C0FF5752D47D9532DDDB23FA2611F9E040, ___Fx_0)); }
	inline bool get_Fx_0() const { return ___Fx_0; }
	inline bool* get_address_of_Fx_0() { return &___Fx_0; }
	inline void set_Fx_0(bool value)
	{
		___Fx_0 = value;
	}

	inline static int32_t get_offset_of_Music_1() { return static_cast<int32_t>(offsetof(UpdateSettingsCommandModel_tCBDA27C0FF5752D47D9532DDDB23FA2611F9E040, ___Music_1)); }
	inline bool get_Music_1() const { return ___Music_1; }
	inline bool* get_address_of_Music_1() { return &___Music_1; }
	inline void set_Music_1(bool value)
	{
		___Music_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATESETTINGSCOMMANDMODEL_TCBDA27C0FF5752D47D9532DDDB23FA2611F9E040_H
#ifndef UPDATESETTINGSCOMMANDRESULT_T1C5BAE390EC340A9EED4A392D498941FF9EFAFFE_H
#define UPDATESETTINGSCOMMANDRESULT_T1C5BAE390EC340A9EED4A392D498941FF9EFAFFE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UpdateSettingsCommandResult
struct  UpdateSettingsCommandResult_t1C5BAE390EC340A9EED4A392D498941FF9EFAFFE  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATESETTINGSCOMMANDRESULT_T1C5BAE390EC340A9EED4A392D498941FF9EFAFFE_H
#ifndef CLAIMTOURNAMENTREWARDCOMMAND_T39383095AB8EFBB4C86B2DAE8F1A7DE4A474F6DF_H
#define CLAIMTOURNAMENTREWARDCOMMAND_T39383095AB8EFBB4C86B2DAE8F1A7DE4A474F6DF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ClaimTournamentRewardCommand
struct  ClaimTournamentRewardCommand_t39383095AB8EFBB4C86B2DAE8F1A7DE4A474F6DF  : public BasicCommand_2_t7396A66A96AF85C35291CC1CAD86F570239D1790
{
public:
	// TournamentsVO ClaimTournamentRewardCommand::_tournamentsVO
	TournamentsVO_tCBA9A235AE4069263CE332B1E9150CDABD0E8016 * ____tournamentsVO_0;
	// Tayr.VOSaver ClaimTournamentRewardCommand::_voSaver
	VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * ____voSaver_1;

public:
	inline static int32_t get_offset_of__tournamentsVO_0() { return static_cast<int32_t>(offsetof(ClaimTournamentRewardCommand_t39383095AB8EFBB4C86B2DAE8F1A7DE4A474F6DF, ____tournamentsVO_0)); }
	inline TournamentsVO_tCBA9A235AE4069263CE332B1E9150CDABD0E8016 * get__tournamentsVO_0() const { return ____tournamentsVO_0; }
	inline TournamentsVO_tCBA9A235AE4069263CE332B1E9150CDABD0E8016 ** get_address_of__tournamentsVO_0() { return &____tournamentsVO_0; }
	inline void set__tournamentsVO_0(TournamentsVO_tCBA9A235AE4069263CE332B1E9150CDABD0E8016 * value)
	{
		____tournamentsVO_0 = value;
		Il2CppCodeGenWriteBarrier((&____tournamentsVO_0), value);
	}

	inline static int32_t get_offset_of__voSaver_1() { return static_cast<int32_t>(offsetof(ClaimTournamentRewardCommand_t39383095AB8EFBB4C86B2DAE8F1A7DE4A474F6DF, ____voSaver_1)); }
	inline VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * get__voSaver_1() const { return ____voSaver_1; }
	inline VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F ** get_address_of__voSaver_1() { return &____voSaver_1; }
	inline void set__voSaver_1(VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * value)
	{
		____voSaver_1 = value;
		Il2CppCodeGenWriteBarrier((&____voSaver_1), value);
	}
};

struct ClaimTournamentRewardCommand_t39383095AB8EFBB4C86B2DAE8F1A7DE4A474F6DF_StaticFields
{
public:
	// ClaimTournamentRewardEvent ClaimTournamentRewardCommand::ClaimTournamentRewardCommandEvent
	ClaimTournamentRewardEvent_t31E71F6AB2C04F3BD77BC744E3F0CE51D5CB0D63 * ___ClaimTournamentRewardCommandEvent_2;

public:
	inline static int32_t get_offset_of_ClaimTournamentRewardCommandEvent_2() { return static_cast<int32_t>(offsetof(ClaimTournamentRewardCommand_t39383095AB8EFBB4C86B2DAE8F1A7DE4A474F6DF_StaticFields, ___ClaimTournamentRewardCommandEvent_2)); }
	inline ClaimTournamentRewardEvent_t31E71F6AB2C04F3BD77BC744E3F0CE51D5CB0D63 * get_ClaimTournamentRewardCommandEvent_2() const { return ___ClaimTournamentRewardCommandEvent_2; }
	inline ClaimTournamentRewardEvent_t31E71F6AB2C04F3BD77BC744E3F0CE51D5CB0D63 ** get_address_of_ClaimTournamentRewardCommandEvent_2() { return &___ClaimTournamentRewardCommandEvent_2; }
	inline void set_ClaimTournamentRewardCommandEvent_2(ClaimTournamentRewardEvent_t31E71F6AB2C04F3BD77BC744E3F0CE51D5CB0D63 * value)
	{
		___ClaimTournamentRewardCommandEvent_2 = value;
		Il2CppCodeGenWriteBarrier((&___ClaimTournamentRewardCommandEvent_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLAIMTOURNAMENTREWARDCOMMAND_T39383095AB8EFBB4C86B2DAE8F1A7DE4A474F6DF_H
#ifndef FILLLIVESCOMMAND_T4837FA208059DB83DC9BC312E0AC4FCD51019733_H
#define FILLLIVESCOMMAND_T4837FA208059DB83DC9BC312E0AC4FCD51019733_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FillLivesCommand
struct  FillLivesCommand_t4837FA208059DB83DC9BC312E0AC4FCD51019733  : public BasicCommand_2_tF760A423B08F23BB56400BBB5A32726635FB6B89
{
public:
	// InventorySystem FillLivesCommand::InventorySystem
	InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 * ___InventorySystem_0;
	// Tayr.VOSaver FillLivesCommand::_voSaver
	VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * ____voSaver_1;
	// InventoryVO FillLivesCommand::_inventoryVO
	InventoryVO_tC7F0305FC930782BD2E4A47E3BD0504F0DCC6AD9 * ____inventoryVO_2;
	// GameSettingsSO FillLivesCommand::_gameSettingsSO
	GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C * ____gameSettingsSO_3;

public:
	inline static int32_t get_offset_of_InventorySystem_0() { return static_cast<int32_t>(offsetof(FillLivesCommand_t4837FA208059DB83DC9BC312E0AC4FCD51019733, ___InventorySystem_0)); }
	inline InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 * get_InventorySystem_0() const { return ___InventorySystem_0; }
	inline InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 ** get_address_of_InventorySystem_0() { return &___InventorySystem_0; }
	inline void set_InventorySystem_0(InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 * value)
	{
		___InventorySystem_0 = value;
		Il2CppCodeGenWriteBarrier((&___InventorySystem_0), value);
	}

	inline static int32_t get_offset_of__voSaver_1() { return static_cast<int32_t>(offsetof(FillLivesCommand_t4837FA208059DB83DC9BC312E0AC4FCD51019733, ____voSaver_1)); }
	inline VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * get__voSaver_1() const { return ____voSaver_1; }
	inline VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F ** get_address_of__voSaver_1() { return &____voSaver_1; }
	inline void set__voSaver_1(VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * value)
	{
		____voSaver_1 = value;
		Il2CppCodeGenWriteBarrier((&____voSaver_1), value);
	}

	inline static int32_t get_offset_of__inventoryVO_2() { return static_cast<int32_t>(offsetof(FillLivesCommand_t4837FA208059DB83DC9BC312E0AC4FCD51019733, ____inventoryVO_2)); }
	inline InventoryVO_tC7F0305FC930782BD2E4A47E3BD0504F0DCC6AD9 * get__inventoryVO_2() const { return ____inventoryVO_2; }
	inline InventoryVO_tC7F0305FC930782BD2E4A47E3BD0504F0DCC6AD9 ** get_address_of__inventoryVO_2() { return &____inventoryVO_2; }
	inline void set__inventoryVO_2(InventoryVO_tC7F0305FC930782BD2E4A47E3BD0504F0DCC6AD9 * value)
	{
		____inventoryVO_2 = value;
		Il2CppCodeGenWriteBarrier((&____inventoryVO_2), value);
	}

	inline static int32_t get_offset_of__gameSettingsSO_3() { return static_cast<int32_t>(offsetof(FillLivesCommand_t4837FA208059DB83DC9BC312E0AC4FCD51019733, ____gameSettingsSO_3)); }
	inline GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C * get__gameSettingsSO_3() const { return ____gameSettingsSO_3; }
	inline GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C ** get_address_of__gameSettingsSO_3() { return &____gameSettingsSO_3; }
	inline void set__gameSettingsSO_3(GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C * value)
	{
		____gameSettingsSO_3 = value;
		Il2CppCodeGenWriteBarrier((&____gameSettingsSO_3), value);
	}
};

struct FillLivesCommand_t4837FA208059DB83DC9BC312E0AC4FCD51019733_StaticFields
{
public:
	// FillLivesEvent FillLivesCommand::FillLivesCommandEvent
	FillLivesEvent_tBC3C967A3339323DC976C55B063FCD0FF64D2568 * ___FillLivesCommandEvent_4;

public:
	inline static int32_t get_offset_of_FillLivesCommandEvent_4() { return static_cast<int32_t>(offsetof(FillLivesCommand_t4837FA208059DB83DC9BC312E0AC4FCD51019733_StaticFields, ___FillLivesCommandEvent_4)); }
	inline FillLivesEvent_tBC3C967A3339323DC976C55B063FCD0FF64D2568 * get_FillLivesCommandEvent_4() const { return ___FillLivesCommandEvent_4; }
	inline FillLivesEvent_tBC3C967A3339323DC976C55B063FCD0FF64D2568 ** get_address_of_FillLivesCommandEvent_4() { return &___FillLivesCommandEvent_4; }
	inline void set_FillLivesCommandEvent_4(FillLivesEvent_tBC3C967A3339323DC976C55B063FCD0FF64D2568 * value)
	{
		___FillLivesCommandEvent_4 = value;
		Il2CppCodeGenWriteBarrier((&___FillLivesCommandEvent_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILLLIVESCOMMAND_T4837FA208059DB83DC9BC312E0AC4FCD51019733_H
#ifndef LOGINCOMMAND_T2A7CDE454F84D34A9AABFD4605747CADE3A26427_H
#define LOGINCOMMAND_T2A7CDE454F84D34A9AABFD4605747CADE3A26427_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoginCommand
struct  LoginCommand_t2A7CDE454F84D34A9AABFD4605747CADE3A26427  : public BasicCommand_2_t93C8E8B4AEABF7BBFA44068A405F96B75A1C7A19
{
public:
	// GlobalVO LoginCommand::_globalVO
	GlobalVO_t3F00C793D791DA4C9FDF395F616EA34E82E53385 * ____globalVO_0;
	// UserVO LoginCommand::_userVO
	UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * ____userVO_1;
	// TournamentsVO LoginCommand::_tournamentsVO
	TournamentsVO_tCBA9A235AE4069263CE332B1E9150CDABD0E8016 * ____tournamentsVO_2;
	// Tayr.VOSaver LoginCommand::_voSaver
	VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * ____voSaver_3;
	// MetadataVO LoginCommand::_metadataVO
	MetadataVO_t65F0D785B7DE72B5A899170B925A9E360F4BEB59 * ____metadataVO_4;

public:
	inline static int32_t get_offset_of__globalVO_0() { return static_cast<int32_t>(offsetof(LoginCommand_t2A7CDE454F84D34A9AABFD4605747CADE3A26427, ____globalVO_0)); }
	inline GlobalVO_t3F00C793D791DA4C9FDF395F616EA34E82E53385 * get__globalVO_0() const { return ____globalVO_0; }
	inline GlobalVO_t3F00C793D791DA4C9FDF395F616EA34E82E53385 ** get_address_of__globalVO_0() { return &____globalVO_0; }
	inline void set__globalVO_0(GlobalVO_t3F00C793D791DA4C9FDF395F616EA34E82E53385 * value)
	{
		____globalVO_0 = value;
		Il2CppCodeGenWriteBarrier((&____globalVO_0), value);
	}

	inline static int32_t get_offset_of__userVO_1() { return static_cast<int32_t>(offsetof(LoginCommand_t2A7CDE454F84D34A9AABFD4605747CADE3A26427, ____userVO_1)); }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * get__userVO_1() const { return ____userVO_1; }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 ** get_address_of__userVO_1() { return &____userVO_1; }
	inline void set__userVO_1(UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * value)
	{
		____userVO_1 = value;
		Il2CppCodeGenWriteBarrier((&____userVO_1), value);
	}

	inline static int32_t get_offset_of__tournamentsVO_2() { return static_cast<int32_t>(offsetof(LoginCommand_t2A7CDE454F84D34A9AABFD4605747CADE3A26427, ____tournamentsVO_2)); }
	inline TournamentsVO_tCBA9A235AE4069263CE332B1E9150CDABD0E8016 * get__tournamentsVO_2() const { return ____tournamentsVO_2; }
	inline TournamentsVO_tCBA9A235AE4069263CE332B1E9150CDABD0E8016 ** get_address_of__tournamentsVO_2() { return &____tournamentsVO_2; }
	inline void set__tournamentsVO_2(TournamentsVO_tCBA9A235AE4069263CE332B1E9150CDABD0E8016 * value)
	{
		____tournamentsVO_2 = value;
		Il2CppCodeGenWriteBarrier((&____tournamentsVO_2), value);
	}

	inline static int32_t get_offset_of__voSaver_3() { return static_cast<int32_t>(offsetof(LoginCommand_t2A7CDE454F84D34A9AABFD4605747CADE3A26427, ____voSaver_3)); }
	inline VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * get__voSaver_3() const { return ____voSaver_3; }
	inline VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F ** get_address_of__voSaver_3() { return &____voSaver_3; }
	inline void set__voSaver_3(VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * value)
	{
		____voSaver_3 = value;
		Il2CppCodeGenWriteBarrier((&____voSaver_3), value);
	}

	inline static int32_t get_offset_of__metadataVO_4() { return static_cast<int32_t>(offsetof(LoginCommand_t2A7CDE454F84D34A9AABFD4605747CADE3A26427, ____metadataVO_4)); }
	inline MetadataVO_t65F0D785B7DE72B5A899170B925A9E360F4BEB59 * get__metadataVO_4() const { return ____metadataVO_4; }
	inline MetadataVO_t65F0D785B7DE72B5A899170B925A9E360F4BEB59 ** get_address_of__metadataVO_4() { return &____metadataVO_4; }
	inline void set__metadataVO_4(MetadataVO_t65F0D785B7DE72B5A899170B925A9E360F4BEB59 * value)
	{
		____metadataVO_4 = value;
		Il2CppCodeGenWriteBarrier((&____metadataVO_4), value);
	}
};

struct LoginCommand_t2A7CDE454F84D34A9AABFD4605747CADE3A26427_StaticFields
{
public:
	// LoginCommandEvent LoginCommand::LoginCommandEvent
	LoginCommandEvent_tE333B28F8632EC5795F9FCD22A6CC7EB36F1C493 * ___LoginCommandEvent_5;

public:
	inline static int32_t get_offset_of_LoginCommandEvent_5() { return static_cast<int32_t>(offsetof(LoginCommand_t2A7CDE454F84D34A9AABFD4605747CADE3A26427_StaticFields, ___LoginCommandEvent_5)); }
	inline LoginCommandEvent_tE333B28F8632EC5795F9FCD22A6CC7EB36F1C493 * get_LoginCommandEvent_5() const { return ___LoginCommandEvent_5; }
	inline LoginCommandEvent_tE333B28F8632EC5795F9FCD22A6CC7EB36F1C493 ** get_address_of_LoginCommandEvent_5() { return &___LoginCommandEvent_5; }
	inline void set_LoginCommandEvent_5(LoginCommandEvent_tE333B28F8632EC5795F9FCD22A6CC7EB36F1C493 * value)
	{
		___LoginCommandEvent_5 = value;
		Il2CppCodeGenWriteBarrier((&___LoginCommandEvent_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGINCOMMAND_T2A7CDE454F84D34A9AABFD4605747CADE3A26427_H
#ifndef PATCHINGCOMPLETEDCOMMAND_T005C53F233E61DE0A4F2905D5D200A52BA27C414_H
#define PATCHINGCOMPLETEDCOMMAND_T005C53F233E61DE0A4F2905D5D200A52BA27C414_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PatchingCompletedCommand
struct  PatchingCompletedCommand_t005C53F233E61DE0A4F2905D5D200A52BA27C414  : public BasicCommand_2_t40DF06C9EDDFF21881BFD80EC0C0A8ED55335254
{
public:
	// PatchingVO PatchingCompletedCommand::_patchingVO
	PatchingVO_tF6810B5A961785E0EB990B4C511101A0A75F3CB4 * ____patchingVO_0;
	// GlobalVO PatchingCompletedCommand::_globalVO
	GlobalVO_t3F00C793D791DA4C9FDF395F616EA34E82E53385 * ____globalVO_1;
	// Tayr.VOSaver PatchingCompletedCommand::_voSaver
	VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * ____voSaver_2;

public:
	inline static int32_t get_offset_of__patchingVO_0() { return static_cast<int32_t>(offsetof(PatchingCompletedCommand_t005C53F233E61DE0A4F2905D5D200A52BA27C414, ____patchingVO_0)); }
	inline PatchingVO_tF6810B5A961785E0EB990B4C511101A0A75F3CB4 * get__patchingVO_0() const { return ____patchingVO_0; }
	inline PatchingVO_tF6810B5A961785E0EB990B4C511101A0A75F3CB4 ** get_address_of__patchingVO_0() { return &____patchingVO_0; }
	inline void set__patchingVO_0(PatchingVO_tF6810B5A961785E0EB990B4C511101A0A75F3CB4 * value)
	{
		____patchingVO_0 = value;
		Il2CppCodeGenWriteBarrier((&____patchingVO_0), value);
	}

	inline static int32_t get_offset_of__globalVO_1() { return static_cast<int32_t>(offsetof(PatchingCompletedCommand_t005C53F233E61DE0A4F2905D5D200A52BA27C414, ____globalVO_1)); }
	inline GlobalVO_t3F00C793D791DA4C9FDF395F616EA34E82E53385 * get__globalVO_1() const { return ____globalVO_1; }
	inline GlobalVO_t3F00C793D791DA4C9FDF395F616EA34E82E53385 ** get_address_of__globalVO_1() { return &____globalVO_1; }
	inline void set__globalVO_1(GlobalVO_t3F00C793D791DA4C9FDF395F616EA34E82E53385 * value)
	{
		____globalVO_1 = value;
		Il2CppCodeGenWriteBarrier((&____globalVO_1), value);
	}

	inline static int32_t get_offset_of__voSaver_2() { return static_cast<int32_t>(offsetof(PatchingCompletedCommand_t005C53F233E61DE0A4F2905D5D200A52BA27C414, ____voSaver_2)); }
	inline VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * get__voSaver_2() const { return ____voSaver_2; }
	inline VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F ** get_address_of__voSaver_2() { return &____voSaver_2; }
	inline void set__voSaver_2(VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * value)
	{
		____voSaver_2 = value;
		Il2CppCodeGenWriteBarrier((&____voSaver_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATCHINGCOMPLETEDCOMMAND_T005C53F233E61DE0A4F2905D5D200A52BA27C414_H
#ifndef PATCHINGUPDATEDCOMMAND_T8183DE064E78F1689A29A91EF9B6D4C9B42C731D_H
#define PATCHINGUPDATEDCOMMAND_T8183DE064E78F1689A29A91EF9B6D4C9B42C731D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PatchingUpdatedCommand
struct  PatchingUpdatedCommand_t8183DE064E78F1689A29A91EF9B6D4C9B42C731D  : public BasicCommand_2_t45D3A9467B05F5FEA9C7C53B615AA70747D07CD0
{
public:
	// PatchingVO PatchingUpdatedCommand::_patchingVO
	PatchingVO_tF6810B5A961785E0EB990B4C511101A0A75F3CB4 * ____patchingVO_0;
	// Tayr.VOSaver PatchingUpdatedCommand::_voSaver
	VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * ____voSaver_1;

public:
	inline static int32_t get_offset_of__patchingVO_0() { return static_cast<int32_t>(offsetof(PatchingUpdatedCommand_t8183DE064E78F1689A29A91EF9B6D4C9B42C731D, ____patchingVO_0)); }
	inline PatchingVO_tF6810B5A961785E0EB990B4C511101A0A75F3CB4 * get__patchingVO_0() const { return ____patchingVO_0; }
	inline PatchingVO_tF6810B5A961785E0EB990B4C511101A0A75F3CB4 ** get_address_of__patchingVO_0() { return &____patchingVO_0; }
	inline void set__patchingVO_0(PatchingVO_tF6810B5A961785E0EB990B4C511101A0A75F3CB4 * value)
	{
		____patchingVO_0 = value;
		Il2CppCodeGenWriteBarrier((&____patchingVO_0), value);
	}

	inline static int32_t get_offset_of__voSaver_1() { return static_cast<int32_t>(offsetof(PatchingUpdatedCommand_t8183DE064E78F1689A29A91EF9B6D4C9B42C731D, ____voSaver_1)); }
	inline VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * get__voSaver_1() const { return ____voSaver_1; }
	inline VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F ** get_address_of__voSaver_1() { return &____voSaver_1; }
	inline void set__voSaver_1(VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * value)
	{
		____voSaver_1 = value;
		Il2CppCodeGenWriteBarrier((&____voSaver_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATCHINGUPDATEDCOMMAND_T8183DE064E78F1689A29A91EF9B6D4C9B42C731D_H
#ifndef SERIALIZABLEDICTIONARYBASE_2_T9AF6CD40FC44CF59E24DE40D15BFB1806852FEEB_H
#define SERIALIZABLEDICTIONARYBASE_2_T9AF6CD40FC44CF59E24DE40D15BFB1806852FEEB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2<System.String,AssetData>
struct  SerializableDictionaryBase_2_t9AF6CD40FC44CF59E24DE40D15BFB1806852FEEB  : public DrawableDictionary_t5EE13D6B57AC0486747142D13BD761EE7C87F78F
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_dict
	Dictionary_2_tE93A2B9ABDA72381551E41330AA05157E9F976C1 * ____dict_3;
	// System.Collections.Generic.List`1<TKey> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_keyValues
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ____keyValues_5;
	// System.Collections.Generic.List`1<TKey> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_keys
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ____keys_6;
	// System.Collections.Generic.List`1<TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_values
	List_1_t4C0E5E5429A980785DA4734722012505FB30D287 * ____values_7;

public:
	inline static int32_t get_offset_of__dict_3() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t9AF6CD40FC44CF59E24DE40D15BFB1806852FEEB, ____dict_3)); }
	inline Dictionary_2_tE93A2B9ABDA72381551E41330AA05157E9F976C1 * get__dict_3() const { return ____dict_3; }
	inline Dictionary_2_tE93A2B9ABDA72381551E41330AA05157E9F976C1 ** get_address_of__dict_3() { return &____dict_3; }
	inline void set__dict_3(Dictionary_2_tE93A2B9ABDA72381551E41330AA05157E9F976C1 * value)
	{
		____dict_3 = value;
		Il2CppCodeGenWriteBarrier((&____dict_3), value);
	}

	inline static int32_t get_offset_of__keyValues_5() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t9AF6CD40FC44CF59E24DE40D15BFB1806852FEEB, ____keyValues_5)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get__keyValues_5() const { return ____keyValues_5; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of__keyValues_5() { return &____keyValues_5; }
	inline void set__keyValues_5(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		____keyValues_5 = value;
		Il2CppCodeGenWriteBarrier((&____keyValues_5), value);
	}

	inline static int32_t get_offset_of__keys_6() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t9AF6CD40FC44CF59E24DE40D15BFB1806852FEEB, ____keys_6)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get__keys_6() const { return ____keys_6; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of__keys_6() { return &____keys_6; }
	inline void set__keys_6(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		____keys_6 = value;
		Il2CppCodeGenWriteBarrier((&____keys_6), value);
	}

	inline static int32_t get_offset_of__values_7() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t9AF6CD40FC44CF59E24DE40D15BFB1806852FEEB, ____values_7)); }
	inline List_1_t4C0E5E5429A980785DA4734722012505FB30D287 * get__values_7() const { return ____values_7; }
	inline List_1_t4C0E5E5429A980785DA4734722012505FB30D287 ** get_address_of__values_7() { return &____values_7; }
	inline void set__values_7(List_1_t4C0E5E5429A980785DA4734722012505FB30D287 * value)
	{
		____values_7 = value;
		Il2CppCodeGenWriteBarrier((&____values_7), value);
	}
};

struct SerializableDictionaryBase_2_t9AF6CD40FC44CF59E24DE40D15BFB1806852FEEB_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_staticEmptyDict
	Dictionary_2_tE93A2B9ABDA72381551E41330AA05157E9F976C1 * ____staticEmptyDict_4;

public:
	inline static int32_t get_offset_of__staticEmptyDict_4() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t9AF6CD40FC44CF59E24DE40D15BFB1806852FEEB_StaticFields, ____staticEmptyDict_4)); }
	inline Dictionary_2_tE93A2B9ABDA72381551E41330AA05157E9F976C1 * get__staticEmptyDict_4() const { return ____staticEmptyDict_4; }
	inline Dictionary_2_tE93A2B9ABDA72381551E41330AA05157E9F976C1 ** get_address_of__staticEmptyDict_4() { return &____staticEmptyDict_4; }
	inline void set__staticEmptyDict_4(Dictionary_2_tE93A2B9ABDA72381551E41330AA05157E9F976C1 * value)
	{
		____staticEmptyDict_4 = value;
		Il2CppCodeGenWriteBarrier((&____staticEmptyDict_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEDICTIONARYBASE_2_T9AF6CD40FC44CF59E24DE40D15BFB1806852FEEB_H
#ifndef SERIALIZABLEDICTIONARYBASE_2_T4B29E4F36BD124CEA2580573E193FD92E22FC7EE_H
#define SERIALIZABLEDICTIONARYBASE_2_T4B29E4F36BD124CEA2580573E193FD92E22FC7EE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2<System.String,System.String>
struct  SerializableDictionaryBase_2_t4B29E4F36BD124CEA2580573E193FD92E22FC7EE  : public DrawableDictionary_t5EE13D6B57AC0486747142D13BD761EE7C87F78F
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_dict
	Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * ____dict_3;
	// System.Collections.Generic.List`1<TKey> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_keyValues
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ____keyValues_5;
	// System.Collections.Generic.List`1<TKey> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_keys
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ____keys_6;
	// System.Collections.Generic.List`1<TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_values
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ____values_7;

public:
	inline static int32_t get_offset_of__dict_3() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t4B29E4F36BD124CEA2580573E193FD92E22FC7EE, ____dict_3)); }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * get__dict_3() const { return ____dict_3; }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC ** get_address_of__dict_3() { return &____dict_3; }
	inline void set__dict_3(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * value)
	{
		____dict_3 = value;
		Il2CppCodeGenWriteBarrier((&____dict_3), value);
	}

	inline static int32_t get_offset_of__keyValues_5() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t4B29E4F36BD124CEA2580573E193FD92E22FC7EE, ____keyValues_5)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get__keyValues_5() const { return ____keyValues_5; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of__keyValues_5() { return &____keyValues_5; }
	inline void set__keyValues_5(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		____keyValues_5 = value;
		Il2CppCodeGenWriteBarrier((&____keyValues_5), value);
	}

	inline static int32_t get_offset_of__keys_6() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t4B29E4F36BD124CEA2580573E193FD92E22FC7EE, ____keys_6)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get__keys_6() const { return ____keys_6; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of__keys_6() { return &____keys_6; }
	inline void set__keys_6(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		____keys_6 = value;
		Il2CppCodeGenWriteBarrier((&____keys_6), value);
	}

	inline static int32_t get_offset_of__values_7() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t4B29E4F36BD124CEA2580573E193FD92E22FC7EE, ____values_7)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get__values_7() const { return ____values_7; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of__values_7() { return &____values_7; }
	inline void set__values_7(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		____values_7 = value;
		Il2CppCodeGenWriteBarrier((&____values_7), value);
	}
};

struct SerializableDictionaryBase_2_t4B29E4F36BD124CEA2580573E193FD92E22FC7EE_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_staticEmptyDict
	Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * ____staticEmptyDict_4;

public:
	inline static int32_t get_offset_of__staticEmptyDict_4() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t4B29E4F36BD124CEA2580573E193FD92E22FC7EE_StaticFields, ____staticEmptyDict_4)); }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * get__staticEmptyDict_4() const { return ____staticEmptyDict_4; }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC ** get_address_of__staticEmptyDict_4() { return &____staticEmptyDict_4; }
	inline void set__staticEmptyDict_4(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * value)
	{
		____staticEmptyDict_4 = value;
		Il2CppCodeGenWriteBarrier((&____staticEmptyDict_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEDICTIONARYBASE_2_T4B29E4F36BD124CEA2580573E193FD92E22FC7EE_H
#ifndef STARCHESTCOMMAND_T3EFBF99A4EE9670D214FA091E39710C0B69923F8_H
#define STARCHESTCOMMAND_T3EFBF99A4EE9670D214FA091E39710C0B69923F8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StarChestCommand
struct  StarChestCommand_t3EFBF99A4EE9670D214FA091E39710C0B69923F8  : public BasicCommand_2_tE4347BF76EF1E47266E872052FF1D524D3906CDE
{
public:
	// ChestModelSO StarChestCommand::_starsChest
	ChestModelSO_t78F97E6AC8F488AD3C18236C4080E78CCF29B2A2 * ____starsChest_0;
	// TournamentsVO StarChestCommand::_tournamentsVO
	TournamentsVO_tCBA9A235AE4069263CE332B1E9150CDABD0E8016 * ____tournamentsVO_1;
	// InventorySystem StarChestCommand::_inventorySystem
	InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 * ____inventorySystem_2;
	// InventoryVO StarChestCommand::_inventoryVO
	InventoryVO_tC7F0305FC930782BD2E4A47E3BD0504F0DCC6AD9 * ____inventoryVO_3;
	// Tayr.VOSaver StarChestCommand::_voSaver
	VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * ____voSaver_4;

public:
	inline static int32_t get_offset_of__starsChest_0() { return static_cast<int32_t>(offsetof(StarChestCommand_t3EFBF99A4EE9670D214FA091E39710C0B69923F8, ____starsChest_0)); }
	inline ChestModelSO_t78F97E6AC8F488AD3C18236C4080E78CCF29B2A2 * get__starsChest_0() const { return ____starsChest_0; }
	inline ChestModelSO_t78F97E6AC8F488AD3C18236C4080E78CCF29B2A2 ** get_address_of__starsChest_0() { return &____starsChest_0; }
	inline void set__starsChest_0(ChestModelSO_t78F97E6AC8F488AD3C18236C4080E78CCF29B2A2 * value)
	{
		____starsChest_0 = value;
		Il2CppCodeGenWriteBarrier((&____starsChest_0), value);
	}

	inline static int32_t get_offset_of__tournamentsVO_1() { return static_cast<int32_t>(offsetof(StarChestCommand_t3EFBF99A4EE9670D214FA091E39710C0B69923F8, ____tournamentsVO_1)); }
	inline TournamentsVO_tCBA9A235AE4069263CE332B1E9150CDABD0E8016 * get__tournamentsVO_1() const { return ____tournamentsVO_1; }
	inline TournamentsVO_tCBA9A235AE4069263CE332B1E9150CDABD0E8016 ** get_address_of__tournamentsVO_1() { return &____tournamentsVO_1; }
	inline void set__tournamentsVO_1(TournamentsVO_tCBA9A235AE4069263CE332B1E9150CDABD0E8016 * value)
	{
		____tournamentsVO_1 = value;
		Il2CppCodeGenWriteBarrier((&____tournamentsVO_1), value);
	}

	inline static int32_t get_offset_of__inventorySystem_2() { return static_cast<int32_t>(offsetof(StarChestCommand_t3EFBF99A4EE9670D214FA091E39710C0B69923F8, ____inventorySystem_2)); }
	inline InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 * get__inventorySystem_2() const { return ____inventorySystem_2; }
	inline InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 ** get_address_of__inventorySystem_2() { return &____inventorySystem_2; }
	inline void set__inventorySystem_2(InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 * value)
	{
		____inventorySystem_2 = value;
		Il2CppCodeGenWriteBarrier((&____inventorySystem_2), value);
	}

	inline static int32_t get_offset_of__inventoryVO_3() { return static_cast<int32_t>(offsetof(StarChestCommand_t3EFBF99A4EE9670D214FA091E39710C0B69923F8, ____inventoryVO_3)); }
	inline InventoryVO_tC7F0305FC930782BD2E4A47E3BD0504F0DCC6AD9 * get__inventoryVO_3() const { return ____inventoryVO_3; }
	inline InventoryVO_tC7F0305FC930782BD2E4A47E3BD0504F0DCC6AD9 ** get_address_of__inventoryVO_3() { return &____inventoryVO_3; }
	inline void set__inventoryVO_3(InventoryVO_tC7F0305FC930782BD2E4A47E3BD0504F0DCC6AD9 * value)
	{
		____inventoryVO_3 = value;
		Il2CppCodeGenWriteBarrier((&____inventoryVO_3), value);
	}

	inline static int32_t get_offset_of__voSaver_4() { return static_cast<int32_t>(offsetof(StarChestCommand_t3EFBF99A4EE9670D214FA091E39710C0B69923F8, ____voSaver_4)); }
	inline VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * get__voSaver_4() const { return ____voSaver_4; }
	inline VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F ** get_address_of__voSaver_4() { return &____voSaver_4; }
	inline void set__voSaver_4(VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * value)
	{
		____voSaver_4 = value;
		Il2CppCodeGenWriteBarrier((&____voSaver_4), value);
	}
};

struct StarChestCommand_t3EFBF99A4EE9670D214FA091E39710C0B69923F8_StaticFields
{
public:
	// StarChestEvent StarChestCommand::StarChestCommandEvent
	StarChestEvent_t0797A526A463F05F371BD49E20AE31B774586F43 * ___StarChestCommandEvent_5;

public:
	inline static int32_t get_offset_of_StarChestCommandEvent_5() { return static_cast<int32_t>(offsetof(StarChestCommand_t3EFBF99A4EE9670D214FA091E39710C0B69923F8_StaticFields, ___StarChestCommandEvent_5)); }
	inline StarChestEvent_t0797A526A463F05F371BD49E20AE31B774586F43 * get_StarChestCommandEvent_5() const { return ___StarChestCommandEvent_5; }
	inline StarChestEvent_t0797A526A463F05F371BD49E20AE31B774586F43 ** get_address_of_StarChestCommandEvent_5() { return &___StarChestCommandEvent_5; }
	inline void set_StarChestCommandEvent_5(StarChestEvent_t0797A526A463F05F371BD49E20AE31B774586F43 * value)
	{
		___StarChestCommandEvent_5 = value;
		Il2CppCodeGenWriteBarrier((&___StarChestCommandEvent_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STARCHESTCOMMAND_T3EFBF99A4EE9670D214FA091E39710C0B69923F8_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef TOURNAMENTRESULTCOMMAND_T952310C8FDEEE1E034696B42AD609BC49FE86085_H
#define TOURNAMENTRESULTCOMMAND_T952310C8FDEEE1E034696B42AD609BC49FE86085_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TournamentResultCommand
struct  TournamentResultCommand_t952310C8FDEEE1E034696B42AD609BC49FE86085  : public BasicCommand_2_t5F9555B3F91A98758F458742D8E775E977CAF4A5
{
public:
	// TournamentsVO TournamentResultCommand::_tournamentsVO
	TournamentsVO_tCBA9A235AE4069263CE332B1E9150CDABD0E8016 * ____tournamentsVO_0;
	// Tayr.VOSaver TournamentResultCommand::_voSaver
	VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * ____voSaver_1;

public:
	inline static int32_t get_offset_of__tournamentsVO_0() { return static_cast<int32_t>(offsetof(TournamentResultCommand_t952310C8FDEEE1E034696B42AD609BC49FE86085, ____tournamentsVO_0)); }
	inline TournamentsVO_tCBA9A235AE4069263CE332B1E9150CDABD0E8016 * get__tournamentsVO_0() const { return ____tournamentsVO_0; }
	inline TournamentsVO_tCBA9A235AE4069263CE332B1E9150CDABD0E8016 ** get_address_of__tournamentsVO_0() { return &____tournamentsVO_0; }
	inline void set__tournamentsVO_0(TournamentsVO_tCBA9A235AE4069263CE332B1E9150CDABD0E8016 * value)
	{
		____tournamentsVO_0 = value;
		Il2CppCodeGenWriteBarrier((&____tournamentsVO_0), value);
	}

	inline static int32_t get_offset_of__voSaver_1() { return static_cast<int32_t>(offsetof(TournamentResultCommand_t952310C8FDEEE1E034696B42AD609BC49FE86085, ____voSaver_1)); }
	inline VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * get__voSaver_1() const { return ____voSaver_1; }
	inline VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F ** get_address_of__voSaver_1() { return &____voSaver_1; }
	inline void set__voSaver_1(VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * value)
	{
		____voSaver_1 = value;
		Il2CppCodeGenWriteBarrier((&____voSaver_1), value);
	}
};

struct TournamentResultCommand_t952310C8FDEEE1E034696B42AD609BC49FE86085_StaticFields
{
public:
	// TournamentResultCommandEvent TournamentResultCommand::TournamentResultCommandEvent
	TournamentResultCommandEvent_t63203EB3B05D0FB9B44625DF65238A1FDB642012 * ___TournamentResultCommandEvent_2;

public:
	inline static int32_t get_offset_of_TournamentResultCommandEvent_2() { return static_cast<int32_t>(offsetof(TournamentResultCommand_t952310C8FDEEE1E034696B42AD609BC49FE86085_StaticFields, ___TournamentResultCommandEvent_2)); }
	inline TournamentResultCommandEvent_t63203EB3B05D0FB9B44625DF65238A1FDB642012 * get_TournamentResultCommandEvent_2() const { return ___TournamentResultCommandEvent_2; }
	inline TournamentResultCommandEvent_t63203EB3B05D0FB9B44625DF65238A1FDB642012 ** get_address_of_TournamentResultCommandEvent_2() { return &___TournamentResultCommandEvent_2; }
	inline void set_TournamentResultCommandEvent_2(TournamentResultCommandEvent_t63203EB3B05D0FB9B44625DF65238A1FDB642012 * value)
	{
		___TournamentResultCommandEvent_2 = value;
		Il2CppCodeGenWriteBarrier((&___TournamentResultCommandEvent_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOURNAMENTRESULTCOMMAND_T952310C8FDEEE1E034696B42AD609BC49FE86085_H
#ifndef TUTORIALCOMPLETEDCOMMAND_TC0EC8D50886D81D69556E9C075A66E78C3B6A2DC_H
#define TUTORIALCOMPLETEDCOMMAND_TC0EC8D50886D81D69556E9C075A66E78C3B6A2DC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TutorialCompletedCommand
struct  TutorialCompletedCommand_tC0EC8D50886D81D69556E9C075A66E78C3B6A2DC  : public BasicCommand_2_t0921118066C3ABAE6A6B2B11AC724538F4FBAF44
{
public:
	// UserVO TutorialCompletedCommand::_userVO
	UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * ____userVO_0;
	// Tayr.VOSaver TutorialCompletedCommand::_voSaver
	VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * ____voSaver_1;

public:
	inline static int32_t get_offset_of__userVO_0() { return static_cast<int32_t>(offsetof(TutorialCompletedCommand_tC0EC8D50886D81D69556E9C075A66E78C3B6A2DC, ____userVO_0)); }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * get__userVO_0() const { return ____userVO_0; }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 ** get_address_of__userVO_0() { return &____userVO_0; }
	inline void set__userVO_0(UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * value)
	{
		____userVO_0 = value;
		Il2CppCodeGenWriteBarrier((&____userVO_0), value);
	}

	inline static int32_t get_offset_of__voSaver_1() { return static_cast<int32_t>(offsetof(TutorialCompletedCommand_tC0EC8D50886D81D69556E9C075A66E78C3B6A2DC, ____voSaver_1)); }
	inline VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * get__voSaver_1() const { return ____voSaver_1; }
	inline VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F ** get_address_of__voSaver_1() { return &____voSaver_1; }
	inline void set__voSaver_1(VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * value)
	{
		____voSaver_1 = value;
		Il2CppCodeGenWriteBarrier((&____voSaver_1), value);
	}
};

struct TutorialCompletedCommand_tC0EC8D50886D81D69556E9C075A66E78C3B6A2DC_StaticFields
{
public:
	// ClaimTournamentRewardEvent TutorialCompletedCommand::ClaimTournamentRewardCommandEvent
	ClaimTournamentRewardEvent_t31E71F6AB2C04F3BD77BC744E3F0CE51D5CB0D63 * ___ClaimTournamentRewardCommandEvent_2;

public:
	inline static int32_t get_offset_of_ClaimTournamentRewardCommandEvent_2() { return static_cast<int32_t>(offsetof(TutorialCompletedCommand_tC0EC8D50886D81D69556E9C075A66E78C3B6A2DC_StaticFields, ___ClaimTournamentRewardCommandEvent_2)); }
	inline ClaimTournamentRewardEvent_t31E71F6AB2C04F3BD77BC744E3F0CE51D5CB0D63 * get_ClaimTournamentRewardCommandEvent_2() const { return ___ClaimTournamentRewardCommandEvent_2; }
	inline ClaimTournamentRewardEvent_t31E71F6AB2C04F3BD77BC744E3F0CE51D5CB0D63 ** get_address_of_ClaimTournamentRewardCommandEvent_2() { return &___ClaimTournamentRewardCommandEvent_2; }
	inline void set_ClaimTournamentRewardCommandEvent_2(ClaimTournamentRewardEvent_t31E71F6AB2C04F3BD77BC744E3F0CE51D5CB0D63 * value)
	{
		___ClaimTournamentRewardCommandEvent_2 = value;
		Il2CppCodeGenWriteBarrier((&___ClaimTournamentRewardCommandEvent_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUTORIALCOMPLETEDCOMMAND_TC0EC8D50886D81D69556E9C075A66E78C3B6A2DC_H
#ifndef UNITYEVENT_1_TE2FEBAB111368B6CFD5C1D869109FB420ADF53E8_H
#define UNITYEVENT_1_TE2FEBAB111368B6CFD5C1D869109FB420ADF53E8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<ClaimTournamentRewardCommandResult>
struct  UnityEvent_1_tE2FEBAB111368B6CFD5C1D869109FB420ADF53E8  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_1_tE2FEBAB111368B6CFD5C1D869109FB420ADF53E8, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_TE2FEBAB111368B6CFD5C1D869109FB420ADF53E8_H
#ifndef UNITYEVENT_1_T7825C5DC82A6D3EA21911EE19F0856169B2D1618_H
#define UNITYEVENT_1_T7825C5DC82A6D3EA21911EE19F0856169B2D1618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<FillLivesCommandResultModel>
struct  UnityEvent_1_t7825C5DC82A6D3EA21911EE19F0856169B2D1618  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_1_t7825C5DC82A6D3EA21911EE19F0856169B2D1618, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T7825C5DC82A6D3EA21911EE19F0856169B2D1618_H
#ifndef UNITYEVENT_1_T478528ABA5C6114923C5E05DC9DDAB11693166CB_H
#define UNITYEVENT_1_T478528ABA5C6114923C5E05DC9DDAB11693166CB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<LevelChestCommandResultModel>
struct  UnityEvent_1_t478528ABA5C6114923C5E05DC9DDAB11693166CB  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_1_t478528ABA5C6114923C5E05DC9DDAB11693166CB, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T478528ABA5C6114923C5E05DC9DDAB11693166CB_H
#ifndef UNITYEVENT_1_T96D83BFDB4BA1BD6A02ACA3C1B662C83F79FE416_H
#define UNITYEVENT_1_T96D83BFDB4BA1BD6A02ACA3C1B662C83F79FE416_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<LoginCommandResultModel>
struct  UnityEvent_1_t96D83BFDB4BA1BD6A02ACA3C1B662C83F79FE416  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_1_t96D83BFDB4BA1BD6A02ACA3C1B662C83F79FE416, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T96D83BFDB4BA1BD6A02ACA3C1B662C83F79FE416_H
#ifndef UNITYEVENT_1_T010A955103A0869A551D985B78DB159CB3BE56A3_H
#define UNITYEVENT_1_T010A955103A0869A551D985B78DB159CB3BE56A3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<StarChestCommandResultModel>
struct  UnityEvent_1_t010A955103A0869A551D985B78DB159CB3BE56A3  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_1_t010A955103A0869A551D985B78DB159CB3BE56A3, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T010A955103A0869A551D985B78DB159CB3BE56A3_H
#ifndef UNITYEVENT_1_T74946EB8080B3ED591EA6608929AA84C3F246D2E_H
#define UNITYEVENT_1_T74946EB8080B3ED591EA6608929AA84C3F246D2E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<TournamentResultCommandModelResult>
struct  UnityEvent_1_t74946EB8080B3ED591EA6608929AA84C3F246D2E  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_1_t74946EB8080B3ED591EA6608929AA84C3F246D2E, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T74946EB8080B3ED591EA6608929AA84C3F246D2E_H
#ifndef UPDATESETTINGSCOMMAND_T67A15F1B0D6B07C873DEF9D9A76B92778DF552D0_H
#define UPDATESETTINGSCOMMAND_T67A15F1B0D6B07C873DEF9D9A76B92778DF552D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UpdateSettingsCommand
struct  UpdateSettingsCommand_t67A15F1B0D6B07C873DEF9D9A76B92778DF552D0  : public BasicCommand_2_t2E6968A2415C59A1010CC28B656E8DBF8D3AF542
{
public:
	// UserVO UpdateSettingsCommand::_userVO
	UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * ____userVO_0;
	// Tayr.VOSaver UpdateSettingsCommand::_voSaver
	VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * ____voSaver_1;

public:
	inline static int32_t get_offset_of__userVO_0() { return static_cast<int32_t>(offsetof(UpdateSettingsCommand_t67A15F1B0D6B07C873DEF9D9A76B92778DF552D0, ____userVO_0)); }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * get__userVO_0() const { return ____userVO_0; }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 ** get_address_of__userVO_0() { return &____userVO_0; }
	inline void set__userVO_0(UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * value)
	{
		____userVO_0 = value;
		Il2CppCodeGenWriteBarrier((&____userVO_0), value);
	}

	inline static int32_t get_offset_of__voSaver_1() { return static_cast<int32_t>(offsetof(UpdateSettingsCommand_t67A15F1B0D6B07C873DEF9D9A76B92778DF552D0, ____voSaver_1)); }
	inline VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * get__voSaver_1() const { return ____voSaver_1; }
	inline VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F ** get_address_of__voSaver_1() { return &____voSaver_1; }
	inline void set__voSaver_1(VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * value)
	{
		____voSaver_1 = value;
		Il2CppCodeGenWriteBarrier((&____voSaver_1), value);
	}
};

struct UpdateSettingsCommand_t67A15F1B0D6B07C873DEF9D9A76B92778DF552D0_StaticFields
{
public:
	// UpdateSettingsEvent UpdateSettingsCommand::UpdateSettingsCommandEvent
	UpdateSettingsEvent_tBDFC5C0BE56631615A2FE1F8DF1593535637EC8F * ___UpdateSettingsCommandEvent_2;

public:
	inline static int32_t get_offset_of_UpdateSettingsCommandEvent_2() { return static_cast<int32_t>(offsetof(UpdateSettingsCommand_t67A15F1B0D6B07C873DEF9D9A76B92778DF552D0_StaticFields, ___UpdateSettingsCommandEvent_2)); }
	inline UpdateSettingsEvent_tBDFC5C0BE56631615A2FE1F8DF1593535637EC8F * get_UpdateSettingsCommandEvent_2() const { return ___UpdateSettingsCommandEvent_2; }
	inline UpdateSettingsEvent_tBDFC5C0BE56631615A2FE1F8DF1593535637EC8F ** get_address_of_UpdateSettingsCommandEvent_2() { return &___UpdateSettingsCommandEvent_2; }
	inline void set_UpdateSettingsCommandEvent_2(UpdateSettingsEvent_tBDFC5C0BE56631615A2FE1F8DF1593535637EC8F * value)
	{
		___UpdateSettingsCommandEvent_2 = value;
		Il2CppCodeGenWriteBarrier((&___UpdateSettingsCommandEvent_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATESETTINGSCOMMAND_T67A15F1B0D6B07C873DEF9D9A76B92778DF552D0_H
#ifndef ASSETDICTIONARY_TC29AFDE2D533D0F9B91BA34778BBF8605137D687_H
#define ASSETDICTIONARY_TC29AFDE2D533D0F9B91BA34778BBF8605137D687_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AssetDictionary
struct  AssetDictionary_tC29AFDE2D533D0F9B91BA34778BBF8605137D687  : public SerializableDictionaryBase_2_t9AF6CD40FC44CF59E24DE40D15BFB1806852FEEB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSETDICTIONARY_TC29AFDE2D533D0F9B91BA34778BBF8605137D687_H
#ifndef CLAIMTOURNAMENTREWARDEVENT_T31E71F6AB2C04F3BD77BC744E3F0CE51D5CB0D63_H
#define CLAIMTOURNAMENTREWARDEVENT_T31E71F6AB2C04F3BD77BC744E3F0CE51D5CB0D63_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ClaimTournamentRewardEvent
struct  ClaimTournamentRewardEvent_t31E71F6AB2C04F3BD77BC744E3F0CE51D5CB0D63  : public UnityEvent_1_tE2FEBAB111368B6CFD5C1D869109FB420ADF53E8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLAIMTOURNAMENTREWARDEVENT_T31E71F6AB2C04F3BD77BC744E3F0CE51D5CB0D63_H
#ifndef CURRENCYTYPE_T22F6D5B9845CBB5FA241F40F84F92C0900D51C55_H
#define CURRENCYTYPE_T22F6D5B9845CBB5FA241F40F84F92C0900D51C55_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CurrencyType
struct  CurrencyType_t22F6D5B9845CBB5FA241F40F84F92C0900D51C55 
{
public:
	// System.Int32 CurrencyType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CurrencyType_t22F6D5B9845CBB5FA241F40F84F92C0900D51C55, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURRENCYTYPE_T22F6D5B9845CBB5FA241F40F84F92C0900D51C55_H
#ifndef CUSTOMTUTORIALTYPE_TB1BFA15E99AB96E461A01DDDBD92211896AE4968_H
#define CUSTOMTUTORIALTYPE_TB1BFA15E99AB96E461A01DDDBD92211896AE4968_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CustomTutorialType
struct  CustomTutorialType_tB1BFA15E99AB96E461A01DDDBD92211896AE4968 
{
public:
	// System.Int32 CustomTutorialType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CustomTutorialType_tB1BFA15E99AB96E461A01DDDBD92211896AE4968, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMTUTORIALTYPE_TB1BFA15E99AB96E461A01DDDBD92211896AE4968_H
#ifndef DATADICTIONARY_T9D4A0BE16CC0324BF2D47F35B50C65CC9B44E606_H
#define DATADICTIONARY_T9D4A0BE16CC0324BF2D47F35B50C65CC9B44E606_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DataDictionary
struct  DataDictionary_t9D4A0BE16CC0324BF2D47F35B50C65CC9B44E606  : public SerializableDictionaryBase_2_t4B29E4F36BD124CEA2580573E193FD92E22FC7EE
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATADICTIONARY_T9D4A0BE16CC0324BF2D47F35B50C65CC9B44E606_H
#ifndef EFFECTBRAIN_T72F5BBABEE27713003E0583D6C88E6A84FE3DDFF_H
#define EFFECTBRAIN_T72F5BBABEE27713003E0583D6C88E6A84FE3DDFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EffectBrain
struct  EffectBrain_t72F5BBABEE27713003E0583D6C88E6A84FE3DDFF 
{
public:
	// System.Int32 EffectBrain::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EffectBrain_t72F5BBABEE27713003E0583D6C88E6A84FE3DDFF, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EFFECTBRAIN_T72F5BBABEE27713003E0583D6C88E6A84FE3DDFF_H
#ifndef EFFECTTYPE_T001314E778DA097AFE0D47BF52477494AF02E0DF_H
#define EFFECTTYPE_T001314E778DA097AFE0D47BF52477494AF02E0DF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EffectType
struct  EffectType_t001314E778DA097AFE0D47BF52477494AF02E0DF 
{
public:
	// System.Int32 EffectType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EffectType_t001314E778DA097AFE0D47BF52477494AF02E0DF, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EFFECTTYPE_T001314E778DA097AFE0D47BF52477494AF02E0DF_H
#ifndef EFFECTVISUALTYPE_T026EC7D3087FCA80C407ED9F3797CE097903D15A_H
#define EFFECTVISUALTYPE_T026EC7D3087FCA80C407ED9F3797CE097903D15A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EffectVisualType
struct  EffectVisualType_t026EC7D3087FCA80C407ED9F3797CE097903D15A 
{
public:
	// System.Int32 EffectVisualType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EffectVisualType_t026EC7D3087FCA80C407ED9F3797CE097903D15A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EFFECTVISUALTYPE_T026EC7D3087FCA80C407ED9F3797CE097903D15A_H
#ifndef FILLLIVESEVENT_TBC3C967A3339323DC976C55B063FCD0FF64D2568_H
#define FILLLIVESEVENT_TBC3C967A3339323DC976C55B063FCD0FF64D2568_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FillLivesEvent
struct  FillLivesEvent_tBC3C967A3339323DC976C55B063FCD0FF64D2568  : public UnityEvent_1_t7825C5DC82A6D3EA21911EE19F0856169B2D1618
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILLLIVESEVENT_TBC3C967A3339323DC976C55B063FCD0FF64D2568_H
#ifndef ITEMTYPE_T41090A4C4D2BCA3765F224EA9264EC817755A5A3_H
#define ITEMTYPE_T41090A4C4D2BCA3765F224EA9264EC817755A5A3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ItemType
struct  ItemType_t41090A4C4D2BCA3765F224EA9264EC817755A5A3 
{
public:
	// System.Int32 ItemType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ItemType_t41090A4C4D2BCA3765F224EA9264EC817755A5A3, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ITEMTYPE_T41090A4C4D2BCA3765F224EA9264EC817755A5A3_H
#ifndef LEVELCHESTEVENT_TE4A44711C3740FE5BCA5CBEAE24289CB248D4429_H
#define LEVELCHESTEVENT_TE4A44711C3740FE5BCA5CBEAE24289CB248D4429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LevelChestEvent
struct  LevelChestEvent_tE4A44711C3740FE5BCA5CBEAE24289CB248D4429  : public UnityEvent_1_t478528ABA5C6114923C5E05DC9DDAB11693166CB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELCHESTEVENT_TE4A44711C3740FE5BCA5CBEAE24289CB248D4429_H
#ifndef LOGINCOMMANDEVENT_TE333B28F8632EC5795F9FCD22A6CC7EB36F1C493_H
#define LOGINCOMMANDEVENT_TE333B28F8632EC5795F9FCD22A6CC7EB36F1C493_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoginCommandEvent
struct  LoginCommandEvent_tE333B28F8632EC5795F9FCD22A6CC7EB36F1C493  : public UnityEvent_1_t96D83BFDB4BA1BD6A02ACA3C1B662C83F79FE416
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGINCOMMANDEVENT_TE333B28F8632EC5795F9FCD22A6CC7EB36F1C493_H
#ifndef OBJECTIVETYPE_TA6A9DA7F8810313B6E359E2D2B8B619E4E1BEE28_H
#define OBJECTIVETYPE_TA6A9DA7F8810313B6E359E2D2B8B619E4E1BEE28_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ObjectiveType
struct  ObjectiveType_tA6A9DA7F8810313B6E359E2D2B8B619E4E1BEE28 
{
public:
	// System.Int32 ObjectiveType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ObjectiveType_tA6A9DA7F8810313B6E359E2D2B8B619E4E1BEE28, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTIVETYPE_TA6A9DA7F8810313B6E359E2D2B8B619E4E1BEE28_H
#ifndef SPELLAI_T31BA0E89C8EE4E13EC803218DD905CB4693C8366_H
#define SPELLAI_T31BA0E89C8EE4E13EC803218DD905CB4693C8366_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpellAI
struct  SpellAI_t31BA0E89C8EE4E13EC803218DD905CB4693C8366 
{
public:
	// System.Int32 SpellAI::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SpellAI_t31BA0E89C8EE4E13EC803218DD905CB4693C8366, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPELLAI_T31BA0E89C8EE4E13EC803218DD905CB4693C8366_H
#ifndef SPELLCONTROLLER_TF10DAA53E6D7502133083BF8AD162F9CBEE65643_H
#define SPELLCONTROLLER_TF10DAA53E6D7502133083BF8AD162F9CBEE65643_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpellController
struct  SpellController_tF10DAA53E6D7502133083BF8AD162F9CBEE65643 
{
public:
	// System.Int32 SpellController::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SpellController_tF10DAA53E6D7502133083BF8AD162F9CBEE65643, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPELLCONTROLLER_TF10DAA53E6D7502133083BF8AD162F9CBEE65643_H
#ifndef SPELLINPUT_T7E941C18CACBD4A789B292019E2341A93AE8739F_H
#define SPELLINPUT_T7E941C18CACBD4A789B292019E2341A93AE8739F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpellInput
struct  SpellInput_t7E941C18CACBD4A789B292019E2341A93AE8739F 
{
public:
	// System.Int32 SpellInput::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SpellInput_t7E941C18CACBD4A789B292019E2341A93AE8739F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPELLINPUT_T7E941C18CACBD4A789B292019E2341A93AE8739F_H
#ifndef SPELLRUNNER_T504261A93EFC5915A80B9E13A0D004262B424C11_H
#define SPELLRUNNER_T504261A93EFC5915A80B9E13A0D004262B424C11_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpellRunner
struct  SpellRunner_t504261A93EFC5915A80B9E13A0D004262B424C11 
{
public:
	// System.Int32 SpellRunner::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SpellRunner_t504261A93EFC5915A80B9E13A0D004262B424C11, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPELLRUNNER_T504261A93EFC5915A80B9E13A0D004262B424C11_H
#ifndef STARCHESTEVENT_T0797A526A463F05F371BD49E20AE31B774586F43_H
#define STARCHESTEVENT_T0797A526A463F05F371BD49E20AE31B774586F43_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StarChestEvent
struct  StarChestEvent_t0797A526A463F05F371BD49E20AE31B774586F43  : public UnityEvent_1_t010A955103A0869A551D985B78DB159CB3BE56A3
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STARCHESTEVENT_T0797A526A463F05F371BD49E20AE31B774586F43_H
#ifndef TOURNAMENTRESULTCOMMANDEVENT_T63203EB3B05D0FB9B44625DF65238A1FDB642012_H
#define TOURNAMENTRESULTCOMMANDEVENT_T63203EB3B05D0FB9B44625DF65238A1FDB642012_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TournamentResultCommandEvent
struct  TournamentResultCommandEvent_t63203EB3B05D0FB9B44625DF65238A1FDB642012  : public UnityEvent_1_t74946EB8080B3ED591EA6608929AA84C3F246D2E
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOURNAMENTRESULTCOMMANDEVENT_T63203EB3B05D0FB9B44625DF65238A1FDB642012_H
#ifndef TUTORIALCOMPLETEDEVENT_T6DBFFBCD3D5CD8DEA1865FB0036F52F297D85EFE_H
#define TUTORIALCOMPLETEDEVENT_T6DBFFBCD3D5CD8DEA1865FB0036F52F297D85EFE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TutorialCompletedEvent
struct  TutorialCompletedEvent_t6DBFFBCD3D5CD8DEA1865FB0036F52F297D85EFE  : public UnityEvent_1_tE2FEBAB111368B6CFD5C1D869109FB420ADF53E8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUTORIALCOMPLETEDEVENT_T6DBFFBCD3D5CD8DEA1865FB0036F52F297D85EFE_H
#ifndef TUTORIALTYPE_T95A6CD09B8522505F42A6F3FFC2E01CCD692A302_H
#define TUTORIALTYPE_T95A6CD09B8522505F42A6F3FFC2E01CCD692A302_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TutorialType
struct  TutorialType_t95A6CD09B8522505F42A6F3FFC2E01CCD692A302 
{
public:
	// System.Int32 TutorialType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TutorialType_t95A6CD09B8522505F42A6F3FFC2E01CCD692A302, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUTORIALTYPE_T95A6CD09B8522505F42A6F3FFC2E01CCD692A302_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef UPDATESETTINGSEVENT_TBDFC5C0BE56631615A2FE1F8DF1593535637EC8F_H
#define UPDATESETTINGSEVENT_TBDFC5C0BE56631615A2FE1F8DF1593535637EC8F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UpdateSettingsEvent
struct  UpdateSettingsEvent_tBDFC5C0BE56631615A2FE1F8DF1593535637EC8F  : public UnityEvent_1_t7825C5DC82A6D3EA21911EE19F0856169B2D1618
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATESETTINGSEVENT_TBDFC5C0BE56631615A2FE1F8DF1593535637EC8F_H
#ifndef CUSTOMTUTORIALMODEL_T7F1E6EF5B6AF58AEA9FB2573811B4562E587E635_H
#define CUSTOMTUTORIALMODEL_T7F1E6EF5B6AF58AEA9FB2573811B4562E587E635_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CustomTutorialModel
struct  CustomTutorialModel_t7F1E6EF5B6AF58AEA9FB2573811B4562E587E635  : public RuntimeObject
{
public:
	// System.String CustomTutorialModel::Id
	String_t* ___Id_0;
	// CustomTutorialType CustomTutorialModel::Type
	int32_t ___Type_1;
	// System.String CustomTutorialModel::Trigger
	String_t* ___Trigger_2;
	// Tayr.TriggerModel CustomTutorialModel::_startTigger
	TriggerModel_t09B711CC906A9A76F767318AF6754B20F253399E * ____startTigger_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> CustomTutorialModel::Data
	Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * ___Data_4;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(CustomTutorialModel_t7F1E6EF5B6AF58AEA9FB2573811B4562E587E635, ___Id_0)); }
	inline String_t* get_Id_0() const { return ___Id_0; }
	inline String_t** get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(String_t* value)
	{
		___Id_0 = value;
		Il2CppCodeGenWriteBarrier((&___Id_0), value);
	}

	inline static int32_t get_offset_of_Type_1() { return static_cast<int32_t>(offsetof(CustomTutorialModel_t7F1E6EF5B6AF58AEA9FB2573811B4562E587E635, ___Type_1)); }
	inline int32_t get_Type_1() const { return ___Type_1; }
	inline int32_t* get_address_of_Type_1() { return &___Type_1; }
	inline void set_Type_1(int32_t value)
	{
		___Type_1 = value;
	}

	inline static int32_t get_offset_of_Trigger_2() { return static_cast<int32_t>(offsetof(CustomTutorialModel_t7F1E6EF5B6AF58AEA9FB2573811B4562E587E635, ___Trigger_2)); }
	inline String_t* get_Trigger_2() const { return ___Trigger_2; }
	inline String_t** get_address_of_Trigger_2() { return &___Trigger_2; }
	inline void set_Trigger_2(String_t* value)
	{
		___Trigger_2 = value;
		Il2CppCodeGenWriteBarrier((&___Trigger_2), value);
	}

	inline static int32_t get_offset_of__startTigger_3() { return static_cast<int32_t>(offsetof(CustomTutorialModel_t7F1E6EF5B6AF58AEA9FB2573811B4562E587E635, ____startTigger_3)); }
	inline TriggerModel_t09B711CC906A9A76F767318AF6754B20F253399E * get__startTigger_3() const { return ____startTigger_3; }
	inline TriggerModel_t09B711CC906A9A76F767318AF6754B20F253399E ** get_address_of__startTigger_3() { return &____startTigger_3; }
	inline void set__startTigger_3(TriggerModel_t09B711CC906A9A76F767318AF6754B20F253399E * value)
	{
		____startTigger_3 = value;
		Il2CppCodeGenWriteBarrier((&____startTigger_3), value);
	}

	inline static int32_t get_offset_of_Data_4() { return static_cast<int32_t>(offsetof(CustomTutorialModel_t7F1E6EF5B6AF58AEA9FB2573811B4562E587E635, ___Data_4)); }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * get_Data_4() const { return ___Data_4; }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC ** get_address_of_Data_4() { return &___Data_4; }
	inline void set_Data_4(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * value)
	{
		___Data_4 = value;
		Il2CppCodeGenWriteBarrier((&___Data_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMTUTORIALMODEL_T7F1E6EF5B6AF58AEA9FB2573811B4562E587E635_H
#ifndef U3CU3EC__DISPLAYCLASS1_0_T8A0EA66C8B605616AE4A6A5640B599178BA16E15_H
#define U3CU3EC__DISPLAYCLASS1_0_T8A0EA66C8B605616AE4A6A5640B599178BA16E15_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CustomTutorialSO_<>c__DisplayClass1_0
struct  U3CU3Ec__DisplayClass1_0_t8A0EA66C8B605616AE4A6A5640B599178BA16E15  : public RuntimeObject
{
public:
	// CustomTutorialType CustomTutorialSO_<>c__DisplayClass1_0::type
	int32_t ___type_0;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass1_0_t8A0EA66C8B605616AE4A6A5640B599178BA16E15, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS1_0_T8A0EA66C8B605616AE4A6A5640B599178BA16E15_H
#ifndef U3CU3EC__DISPLAYCLASS2_0_TA198EA948F0DC0F03FB3D83644AAE8654B69EC47_H
#define U3CU3EC__DISPLAYCLASS2_0_TA198EA948F0DC0F03FB3D83644AAE8654B69EC47_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CustomTutorialSO_<>c__DisplayClass2_0
struct  U3CU3Ec__DisplayClass2_0_tA198EA948F0DC0F03FB3D83644AAE8654B69EC47  : public RuntimeObject
{
public:
	// CustomTutorialType CustomTutorialSO_<>c__DisplayClass2_0::type
	int32_t ___type_0;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_tA198EA948F0DC0F03FB3D83644AAE8654B69EC47, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS2_0_TA198EA948F0DC0F03FB3D83644AAE8654B69EC47_H
#ifndef EFFECTMODEL_T1BF2DAB91B75E4435889D8BBCCA583888883223E_H
#define EFFECTMODEL_T1BF2DAB91B75E4435889D8BBCCA583888883223E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EffectModel
struct  EffectModel_t1BF2DAB91B75E4435889D8BBCCA583888883223E  : public RuntimeObject
{
public:
	// EffectBrain EffectModel::EffectBrain
	int32_t ___EffectBrain_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> EffectModel::EffectBrainData
	Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * ___EffectBrainData_1;
	// EffectType EffectModel::EffectType
	int32_t ___EffectType_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> EffectModel::EffectData
	Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * ___EffectData_3;
	// System.String EffectModel::EffectVisualId
	String_t* ___EffectVisualId_4;

public:
	inline static int32_t get_offset_of_EffectBrain_0() { return static_cast<int32_t>(offsetof(EffectModel_t1BF2DAB91B75E4435889D8BBCCA583888883223E, ___EffectBrain_0)); }
	inline int32_t get_EffectBrain_0() const { return ___EffectBrain_0; }
	inline int32_t* get_address_of_EffectBrain_0() { return &___EffectBrain_0; }
	inline void set_EffectBrain_0(int32_t value)
	{
		___EffectBrain_0 = value;
	}

	inline static int32_t get_offset_of_EffectBrainData_1() { return static_cast<int32_t>(offsetof(EffectModel_t1BF2DAB91B75E4435889D8BBCCA583888883223E, ___EffectBrainData_1)); }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * get_EffectBrainData_1() const { return ___EffectBrainData_1; }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC ** get_address_of_EffectBrainData_1() { return &___EffectBrainData_1; }
	inline void set_EffectBrainData_1(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * value)
	{
		___EffectBrainData_1 = value;
		Il2CppCodeGenWriteBarrier((&___EffectBrainData_1), value);
	}

	inline static int32_t get_offset_of_EffectType_2() { return static_cast<int32_t>(offsetof(EffectModel_t1BF2DAB91B75E4435889D8BBCCA583888883223E, ___EffectType_2)); }
	inline int32_t get_EffectType_2() const { return ___EffectType_2; }
	inline int32_t* get_address_of_EffectType_2() { return &___EffectType_2; }
	inline void set_EffectType_2(int32_t value)
	{
		___EffectType_2 = value;
	}

	inline static int32_t get_offset_of_EffectData_3() { return static_cast<int32_t>(offsetof(EffectModel_t1BF2DAB91B75E4435889D8BBCCA583888883223E, ___EffectData_3)); }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * get_EffectData_3() const { return ___EffectData_3; }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC ** get_address_of_EffectData_3() { return &___EffectData_3; }
	inline void set_EffectData_3(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * value)
	{
		___EffectData_3 = value;
		Il2CppCodeGenWriteBarrier((&___EffectData_3), value);
	}

	inline static int32_t get_offset_of_EffectVisualId_4() { return static_cast<int32_t>(offsetof(EffectModel_t1BF2DAB91B75E4435889D8BBCCA583888883223E, ___EffectVisualId_4)); }
	inline String_t* get_EffectVisualId_4() const { return ___EffectVisualId_4; }
	inline String_t** get_address_of_EffectVisualId_4() { return &___EffectVisualId_4; }
	inline void set_EffectVisualId_4(String_t* value)
	{
		___EffectVisualId_4 = value;
		Il2CppCodeGenWriteBarrier((&___EffectVisualId_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EFFECTMODEL_T1BF2DAB91B75E4435889D8BBCCA583888883223E_H
#ifndef EFFECTVISUAL_TE885D61366B46DC715156887B773663B79D6777A_H
#define EFFECTVISUAL_TE885D61366B46DC715156887B773663B79D6777A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EffectVisual
struct  EffectVisual_tE885D61366B46DC715156887B773663B79D6777A  : public RuntimeObject
{
public:
	// System.String EffectVisual::Prefab
	String_t* ___Prefab_0;
	// System.String EffectVisual::Bundle
	String_t* ___Bundle_1;
	// EffectVisualType EffectVisual::EffectVisualType
	int32_t ___EffectVisualType_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> EffectVisual::EffectVisualData
	Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * ___EffectVisualData_3;

public:
	inline static int32_t get_offset_of_Prefab_0() { return static_cast<int32_t>(offsetof(EffectVisual_tE885D61366B46DC715156887B773663B79D6777A, ___Prefab_0)); }
	inline String_t* get_Prefab_0() const { return ___Prefab_0; }
	inline String_t** get_address_of_Prefab_0() { return &___Prefab_0; }
	inline void set_Prefab_0(String_t* value)
	{
		___Prefab_0 = value;
		Il2CppCodeGenWriteBarrier((&___Prefab_0), value);
	}

	inline static int32_t get_offset_of_Bundle_1() { return static_cast<int32_t>(offsetof(EffectVisual_tE885D61366B46DC715156887B773663B79D6777A, ___Bundle_1)); }
	inline String_t* get_Bundle_1() const { return ___Bundle_1; }
	inline String_t** get_address_of_Bundle_1() { return &___Bundle_1; }
	inline void set_Bundle_1(String_t* value)
	{
		___Bundle_1 = value;
		Il2CppCodeGenWriteBarrier((&___Bundle_1), value);
	}

	inline static int32_t get_offset_of_EffectVisualType_2() { return static_cast<int32_t>(offsetof(EffectVisual_tE885D61366B46DC715156887B773663B79D6777A, ___EffectVisualType_2)); }
	inline int32_t get_EffectVisualType_2() const { return ___EffectVisualType_2; }
	inline int32_t* get_address_of_EffectVisualType_2() { return &___EffectVisualType_2; }
	inline void set_EffectVisualType_2(int32_t value)
	{
		___EffectVisualType_2 = value;
	}

	inline static int32_t get_offset_of_EffectVisualData_3() { return static_cast<int32_t>(offsetof(EffectVisual_tE885D61366B46DC715156887B773663B79D6777A, ___EffectVisualData_3)); }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * get_EffectVisualData_3() const { return ___EffectVisualData_3; }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC ** get_address_of_EffectVisualData_3() { return &___EffectVisualData_3; }
	inline void set_EffectVisualData_3(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * value)
	{
		___EffectVisualData_3 = value;
		Il2CppCodeGenWriteBarrier((&___EffectVisualData_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EFFECTVISUAL_TE885D61366B46DC715156887B773663B79D6777A_H
#ifndef ITEMMODEL_T620A13B4D13CD275487D308843103864063DBE1F_H
#define ITEMMODEL_T620A13B4D13CD275487D308843103864063DBE1F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ItemModel
struct  ItemModel_t620A13B4D13CD275487D308843103864063DBE1F  : public RuntimeObject
{
public:
	// System.String ItemModel::Id
	String_t* ___Id_0;
	// System.String ItemModel::Bundle
	String_t* ___Bundle_1;
	// System.String ItemModel::Prefab
	String_t* ___Prefab_2;
	// System.String ItemModel::Icon
	String_t* ___Icon_3;
	// System.String ItemModel::DeathExplosion
	String_t* ___DeathExplosion_4;
	// ItemType ItemModel::Type
	int32_t ___Type_5;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(ItemModel_t620A13B4D13CD275487D308843103864063DBE1F, ___Id_0)); }
	inline String_t* get_Id_0() const { return ___Id_0; }
	inline String_t** get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(String_t* value)
	{
		___Id_0 = value;
		Il2CppCodeGenWriteBarrier((&___Id_0), value);
	}

	inline static int32_t get_offset_of_Bundle_1() { return static_cast<int32_t>(offsetof(ItemModel_t620A13B4D13CD275487D308843103864063DBE1F, ___Bundle_1)); }
	inline String_t* get_Bundle_1() const { return ___Bundle_1; }
	inline String_t** get_address_of_Bundle_1() { return &___Bundle_1; }
	inline void set_Bundle_1(String_t* value)
	{
		___Bundle_1 = value;
		Il2CppCodeGenWriteBarrier((&___Bundle_1), value);
	}

	inline static int32_t get_offset_of_Prefab_2() { return static_cast<int32_t>(offsetof(ItemModel_t620A13B4D13CD275487D308843103864063DBE1F, ___Prefab_2)); }
	inline String_t* get_Prefab_2() const { return ___Prefab_2; }
	inline String_t** get_address_of_Prefab_2() { return &___Prefab_2; }
	inline void set_Prefab_2(String_t* value)
	{
		___Prefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___Prefab_2), value);
	}

	inline static int32_t get_offset_of_Icon_3() { return static_cast<int32_t>(offsetof(ItemModel_t620A13B4D13CD275487D308843103864063DBE1F, ___Icon_3)); }
	inline String_t* get_Icon_3() const { return ___Icon_3; }
	inline String_t** get_address_of_Icon_3() { return &___Icon_3; }
	inline void set_Icon_3(String_t* value)
	{
		___Icon_3 = value;
		Il2CppCodeGenWriteBarrier((&___Icon_3), value);
	}

	inline static int32_t get_offset_of_DeathExplosion_4() { return static_cast<int32_t>(offsetof(ItemModel_t620A13B4D13CD275487D308843103864063DBE1F, ___DeathExplosion_4)); }
	inline String_t* get_DeathExplosion_4() const { return ___DeathExplosion_4; }
	inline String_t** get_address_of_DeathExplosion_4() { return &___DeathExplosion_4; }
	inline void set_DeathExplosion_4(String_t* value)
	{
		___DeathExplosion_4 = value;
		Il2CppCodeGenWriteBarrier((&___DeathExplosion_4), value);
	}

	inline static int32_t get_offset_of_Type_5() { return static_cast<int32_t>(offsetof(ItemModel_t620A13B4D13CD275487D308843103864063DBE1F, ___Type_5)); }
	inline int32_t get_Type_5() const { return ___Type_5; }
	inline int32_t* get_address_of_Type_5() { return &___Type_5; }
	inline void set_Type_5(int32_t value)
	{
		___Type_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ITEMMODEL_T620A13B4D13CD275487D308843103864063DBE1F_H
#ifndef LEVELOBJECTIVE_T694A34150000D3DE3C9341AAC6D6B37667B796EC_H
#define LEVELOBJECTIVE_T694A34150000D3DE3C9341AAC6D6B37667B796EC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LevelObjective
struct  LevelObjective_t694A34150000D3DE3C9341AAC6D6B37667B796EC  : public RuntimeObject
{
public:
	// ObjectiveType LevelObjective::Type
	int32_t ___Type_0;
	// System.String LevelObjective::UnitId
	String_t* ___UnitId_1;
	// System.Int32 LevelObjective::Count
	int32_t ___Count_2;

public:
	inline static int32_t get_offset_of_Type_0() { return static_cast<int32_t>(offsetof(LevelObjective_t694A34150000D3DE3C9341AAC6D6B37667B796EC, ___Type_0)); }
	inline int32_t get_Type_0() const { return ___Type_0; }
	inline int32_t* get_address_of_Type_0() { return &___Type_0; }
	inline void set_Type_0(int32_t value)
	{
		___Type_0 = value;
	}

	inline static int32_t get_offset_of_UnitId_1() { return static_cast<int32_t>(offsetof(LevelObjective_t694A34150000D3DE3C9341AAC6D6B37667B796EC, ___UnitId_1)); }
	inline String_t* get_UnitId_1() const { return ___UnitId_1; }
	inline String_t** get_address_of_UnitId_1() { return &___UnitId_1; }
	inline void set_UnitId_1(String_t* value)
	{
		___UnitId_1 = value;
		Il2CppCodeGenWriteBarrier((&___UnitId_1), value);
	}

	inline static int32_t get_offset_of_Count_2() { return static_cast<int32_t>(offsetof(LevelObjective_t694A34150000D3DE3C9341AAC6D6B37667B796EC, ___Count_2)); }
	inline int32_t get_Count_2() const { return ___Count_2; }
	inline int32_t* get_address_of_Count_2() { return &___Count_2; }
	inline void set_Count_2(int32_t value)
	{
		___Count_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELOBJECTIVE_T694A34150000D3DE3C9341AAC6D6B37667B796EC_H
#ifndef REWARDMODEL_TE09D75F3B47938652450ABB51F21663745C8BC44_H
#define REWARDMODEL_TE09D75F3B47938652450ABB51F21663745C8BC44_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RewardModel
struct  RewardModel_tE09D75F3B47938652450ABB51F21663745C8BC44  : public RuntimeObject
{
public:
	// CurrencyType RewardModel::Type
	int32_t ___Type_0;
	// System.Int32 RewardModel::Value
	int32_t ___Value_1;
	// System.String RewardModel::Id
	String_t* ___Id_2;

public:
	inline static int32_t get_offset_of_Type_0() { return static_cast<int32_t>(offsetof(RewardModel_tE09D75F3B47938652450ABB51F21663745C8BC44, ___Type_0)); }
	inline int32_t get_Type_0() const { return ___Type_0; }
	inline int32_t* get_address_of_Type_0() { return &___Type_0; }
	inline void set_Type_0(int32_t value)
	{
		___Type_0 = value;
	}

	inline static int32_t get_offset_of_Value_1() { return static_cast<int32_t>(offsetof(RewardModel_tE09D75F3B47938652450ABB51F21663745C8BC44, ___Value_1)); }
	inline int32_t get_Value_1() const { return ___Value_1; }
	inline int32_t* get_address_of_Value_1() { return &___Value_1; }
	inline void set_Value_1(int32_t value)
	{
		___Value_1 = value;
	}

	inline static int32_t get_offset_of_Id_2() { return static_cast<int32_t>(offsetof(RewardModel_tE09D75F3B47938652450ABB51F21663745C8BC44, ___Id_2)); }
	inline String_t* get_Id_2() const { return ___Id_2; }
	inline String_t** get_address_of_Id_2() { return &___Id_2; }
	inline void set_Id_2(String_t* value)
	{
		___Id_2 = value;
		Il2CppCodeGenWriteBarrier((&___Id_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REWARDMODEL_TE09D75F3B47938652450ABB51F21663745C8BC44_H
#ifndef SPELLMODEL_T50B172E1768260E1736AA33F4ED72DB9CDC35235_H
#define SPELLMODEL_T50B172E1768260E1736AA33F4ED72DB9CDC35235_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpellModel
struct  SpellModel_t50B172E1768260E1736AA33F4ED72DB9CDC35235  : public RuntimeObject
{
public:
	// System.String SpellModel::Id
	String_t* ___Id_0;
	// SpellInput SpellModel::SpellInput
	int32_t ___SpellInput_1;
	// System.Collections.Generic.Dictionary`2<System.String,AssetData> SpellModel::Assets
	Dictionary_2_tE93A2B9ABDA72381551E41330AA05157E9F976C1 * ___Assets_2;
	// SpellController SpellModel::SpellController
	int32_t ___SpellController_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> SpellModel::SpellControllerData
	Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * ___SpellControllerData_4;
	// System.Collections.Generic.List`1<EffectModel> SpellModel::Effects
	List_1_t659C06110FB03B1EF961FEA5BF138ABD36CE9ABB * ___Effects_5;
	// System.Boolean SpellModel::IsBooster
	bool ___IsBooster_6;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(SpellModel_t50B172E1768260E1736AA33F4ED72DB9CDC35235, ___Id_0)); }
	inline String_t* get_Id_0() const { return ___Id_0; }
	inline String_t** get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(String_t* value)
	{
		___Id_0 = value;
		Il2CppCodeGenWriteBarrier((&___Id_0), value);
	}

	inline static int32_t get_offset_of_SpellInput_1() { return static_cast<int32_t>(offsetof(SpellModel_t50B172E1768260E1736AA33F4ED72DB9CDC35235, ___SpellInput_1)); }
	inline int32_t get_SpellInput_1() const { return ___SpellInput_1; }
	inline int32_t* get_address_of_SpellInput_1() { return &___SpellInput_1; }
	inline void set_SpellInput_1(int32_t value)
	{
		___SpellInput_1 = value;
	}

	inline static int32_t get_offset_of_Assets_2() { return static_cast<int32_t>(offsetof(SpellModel_t50B172E1768260E1736AA33F4ED72DB9CDC35235, ___Assets_2)); }
	inline Dictionary_2_tE93A2B9ABDA72381551E41330AA05157E9F976C1 * get_Assets_2() const { return ___Assets_2; }
	inline Dictionary_2_tE93A2B9ABDA72381551E41330AA05157E9F976C1 ** get_address_of_Assets_2() { return &___Assets_2; }
	inline void set_Assets_2(Dictionary_2_tE93A2B9ABDA72381551E41330AA05157E9F976C1 * value)
	{
		___Assets_2 = value;
		Il2CppCodeGenWriteBarrier((&___Assets_2), value);
	}

	inline static int32_t get_offset_of_SpellController_3() { return static_cast<int32_t>(offsetof(SpellModel_t50B172E1768260E1736AA33F4ED72DB9CDC35235, ___SpellController_3)); }
	inline int32_t get_SpellController_3() const { return ___SpellController_3; }
	inline int32_t* get_address_of_SpellController_3() { return &___SpellController_3; }
	inline void set_SpellController_3(int32_t value)
	{
		___SpellController_3 = value;
	}

	inline static int32_t get_offset_of_SpellControllerData_4() { return static_cast<int32_t>(offsetof(SpellModel_t50B172E1768260E1736AA33F4ED72DB9CDC35235, ___SpellControllerData_4)); }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * get_SpellControllerData_4() const { return ___SpellControllerData_4; }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC ** get_address_of_SpellControllerData_4() { return &___SpellControllerData_4; }
	inline void set_SpellControllerData_4(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * value)
	{
		___SpellControllerData_4 = value;
		Il2CppCodeGenWriteBarrier((&___SpellControllerData_4), value);
	}

	inline static int32_t get_offset_of_Effects_5() { return static_cast<int32_t>(offsetof(SpellModel_t50B172E1768260E1736AA33F4ED72DB9CDC35235, ___Effects_5)); }
	inline List_1_t659C06110FB03B1EF961FEA5BF138ABD36CE9ABB * get_Effects_5() const { return ___Effects_5; }
	inline List_1_t659C06110FB03B1EF961FEA5BF138ABD36CE9ABB ** get_address_of_Effects_5() { return &___Effects_5; }
	inline void set_Effects_5(List_1_t659C06110FB03B1EF961FEA5BF138ABD36CE9ABB * value)
	{
		___Effects_5 = value;
		Il2CppCodeGenWriteBarrier((&___Effects_5), value);
	}

	inline static int32_t get_offset_of_IsBooster_6() { return static_cast<int32_t>(offsetof(SpellModel_t50B172E1768260E1736AA33F4ED72DB9CDC35235, ___IsBooster_6)); }
	inline bool get_IsBooster_6() const { return ___IsBooster_6; }
	inline bool* get_address_of_IsBooster_6() { return &___IsBooster_6; }
	inline void set_IsBooster_6(bool value)
	{
		___IsBooster_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPELLMODEL_T50B172E1768260E1736AA33F4ED72DB9CDC35235_H
#ifndef TOWERMODEL_T9DD2032CC7655923CBFCAC0EF0DF7B2D11683D31_H
#define TOWERMODEL_T9DD2032CC7655923CBFCAC0EF0DF7B2D11683D31_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TowerModel
struct  TowerModel_t9DD2032CC7655923CBFCAC0EF0DF7B2D11683D31  : public RuntimeObject
{
public:
	// System.String TowerModel::Id
	String_t* ___Id_0;
	// System.String TowerModel::Bundle
	String_t* ___Bundle_1;
	// System.String TowerModel::Prefab
	String_t* ___Prefab_2;
	// System.String TowerModel::Icon
	String_t* ___Icon_3;
	// SpellAI TowerModel::SpellAI
	int32_t ___SpellAI_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> TowerModel::AIData
	Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * ___AIData_5;
	// System.Single TowerModel::Range
	float ___Range_6;
	// System.String TowerModel::Spell
	String_t* ___Spell_7;
	// System.Int32 TowerModel::HP
	int32_t ___HP_8;
	// System.String TowerModel::DeathExplosion
	String_t* ___DeathExplosion_9;
	// System.Single TowerModel::HealDuration
	float ___HealDuration_10;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(TowerModel_t9DD2032CC7655923CBFCAC0EF0DF7B2D11683D31, ___Id_0)); }
	inline String_t* get_Id_0() const { return ___Id_0; }
	inline String_t** get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(String_t* value)
	{
		___Id_0 = value;
		Il2CppCodeGenWriteBarrier((&___Id_0), value);
	}

	inline static int32_t get_offset_of_Bundle_1() { return static_cast<int32_t>(offsetof(TowerModel_t9DD2032CC7655923CBFCAC0EF0DF7B2D11683D31, ___Bundle_1)); }
	inline String_t* get_Bundle_1() const { return ___Bundle_1; }
	inline String_t** get_address_of_Bundle_1() { return &___Bundle_1; }
	inline void set_Bundle_1(String_t* value)
	{
		___Bundle_1 = value;
		Il2CppCodeGenWriteBarrier((&___Bundle_1), value);
	}

	inline static int32_t get_offset_of_Prefab_2() { return static_cast<int32_t>(offsetof(TowerModel_t9DD2032CC7655923CBFCAC0EF0DF7B2D11683D31, ___Prefab_2)); }
	inline String_t* get_Prefab_2() const { return ___Prefab_2; }
	inline String_t** get_address_of_Prefab_2() { return &___Prefab_2; }
	inline void set_Prefab_2(String_t* value)
	{
		___Prefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___Prefab_2), value);
	}

	inline static int32_t get_offset_of_Icon_3() { return static_cast<int32_t>(offsetof(TowerModel_t9DD2032CC7655923CBFCAC0EF0DF7B2D11683D31, ___Icon_3)); }
	inline String_t* get_Icon_3() const { return ___Icon_3; }
	inline String_t** get_address_of_Icon_3() { return &___Icon_3; }
	inline void set_Icon_3(String_t* value)
	{
		___Icon_3 = value;
		Il2CppCodeGenWriteBarrier((&___Icon_3), value);
	}

	inline static int32_t get_offset_of_SpellAI_4() { return static_cast<int32_t>(offsetof(TowerModel_t9DD2032CC7655923CBFCAC0EF0DF7B2D11683D31, ___SpellAI_4)); }
	inline int32_t get_SpellAI_4() const { return ___SpellAI_4; }
	inline int32_t* get_address_of_SpellAI_4() { return &___SpellAI_4; }
	inline void set_SpellAI_4(int32_t value)
	{
		___SpellAI_4 = value;
	}

	inline static int32_t get_offset_of_AIData_5() { return static_cast<int32_t>(offsetof(TowerModel_t9DD2032CC7655923CBFCAC0EF0DF7B2D11683D31, ___AIData_5)); }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * get_AIData_5() const { return ___AIData_5; }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC ** get_address_of_AIData_5() { return &___AIData_5; }
	inline void set_AIData_5(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * value)
	{
		___AIData_5 = value;
		Il2CppCodeGenWriteBarrier((&___AIData_5), value);
	}

	inline static int32_t get_offset_of_Range_6() { return static_cast<int32_t>(offsetof(TowerModel_t9DD2032CC7655923CBFCAC0EF0DF7B2D11683D31, ___Range_6)); }
	inline float get_Range_6() const { return ___Range_6; }
	inline float* get_address_of_Range_6() { return &___Range_6; }
	inline void set_Range_6(float value)
	{
		___Range_6 = value;
	}

	inline static int32_t get_offset_of_Spell_7() { return static_cast<int32_t>(offsetof(TowerModel_t9DD2032CC7655923CBFCAC0EF0DF7B2D11683D31, ___Spell_7)); }
	inline String_t* get_Spell_7() const { return ___Spell_7; }
	inline String_t** get_address_of_Spell_7() { return &___Spell_7; }
	inline void set_Spell_7(String_t* value)
	{
		___Spell_7 = value;
		Il2CppCodeGenWriteBarrier((&___Spell_7), value);
	}

	inline static int32_t get_offset_of_HP_8() { return static_cast<int32_t>(offsetof(TowerModel_t9DD2032CC7655923CBFCAC0EF0DF7B2D11683D31, ___HP_8)); }
	inline int32_t get_HP_8() const { return ___HP_8; }
	inline int32_t* get_address_of_HP_8() { return &___HP_8; }
	inline void set_HP_8(int32_t value)
	{
		___HP_8 = value;
	}

	inline static int32_t get_offset_of_DeathExplosion_9() { return static_cast<int32_t>(offsetof(TowerModel_t9DD2032CC7655923CBFCAC0EF0DF7B2D11683D31, ___DeathExplosion_9)); }
	inline String_t* get_DeathExplosion_9() const { return ___DeathExplosion_9; }
	inline String_t** get_address_of_DeathExplosion_9() { return &___DeathExplosion_9; }
	inline void set_DeathExplosion_9(String_t* value)
	{
		___DeathExplosion_9 = value;
		Il2CppCodeGenWriteBarrier((&___DeathExplosion_9), value);
	}

	inline static int32_t get_offset_of_HealDuration_10() { return static_cast<int32_t>(offsetof(TowerModel_t9DD2032CC7655923CBFCAC0EF0DF7B2D11683D31, ___HealDuration_10)); }
	inline float get_HealDuration_10() const { return ___HealDuration_10; }
	inline float* get_address_of_HealDuration_10() { return &___HealDuration_10; }
	inline void set_HealDuration_10(float value)
	{
		___HealDuration_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOWERMODEL_T9DD2032CC7655923CBFCAC0EF0DF7B2D11683D31_H
#ifndef TRAPMODEL_TC99E2852889B4321B6754DFDDD005862CDC965EC_H
#define TRAPMODEL_TC99E2852889B4321B6754DFDDD005862CDC965EC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TrapModel
struct  TrapModel_tC99E2852889B4321B6754DFDDD005862CDC965EC  : public RuntimeObject
{
public:
	// System.String TrapModel::Id
	String_t* ___Id_0;
	// System.String TrapModel::Bundle
	String_t* ___Bundle_1;
	// System.String TrapModel::Prefab
	String_t* ___Prefab_2;
	// System.String TrapModel::Icon
	String_t* ___Icon_3;
	// SpellAI TrapModel::SpellAI
	int32_t ___SpellAI_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> TrapModel::AIData
	Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * ___AIData_5;
	// System.Single TrapModel::Range
	float ___Range_6;
	// System.Boolean TrapModel::IsScalable
	bool ___IsScalable_7;
	// System.String TrapModel::Spell
	String_t* ___Spell_8;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(TrapModel_tC99E2852889B4321B6754DFDDD005862CDC965EC, ___Id_0)); }
	inline String_t* get_Id_0() const { return ___Id_0; }
	inline String_t** get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(String_t* value)
	{
		___Id_0 = value;
		Il2CppCodeGenWriteBarrier((&___Id_0), value);
	}

	inline static int32_t get_offset_of_Bundle_1() { return static_cast<int32_t>(offsetof(TrapModel_tC99E2852889B4321B6754DFDDD005862CDC965EC, ___Bundle_1)); }
	inline String_t* get_Bundle_1() const { return ___Bundle_1; }
	inline String_t** get_address_of_Bundle_1() { return &___Bundle_1; }
	inline void set_Bundle_1(String_t* value)
	{
		___Bundle_1 = value;
		Il2CppCodeGenWriteBarrier((&___Bundle_1), value);
	}

	inline static int32_t get_offset_of_Prefab_2() { return static_cast<int32_t>(offsetof(TrapModel_tC99E2852889B4321B6754DFDDD005862CDC965EC, ___Prefab_2)); }
	inline String_t* get_Prefab_2() const { return ___Prefab_2; }
	inline String_t** get_address_of_Prefab_2() { return &___Prefab_2; }
	inline void set_Prefab_2(String_t* value)
	{
		___Prefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___Prefab_2), value);
	}

	inline static int32_t get_offset_of_Icon_3() { return static_cast<int32_t>(offsetof(TrapModel_tC99E2852889B4321B6754DFDDD005862CDC965EC, ___Icon_3)); }
	inline String_t* get_Icon_3() const { return ___Icon_3; }
	inline String_t** get_address_of_Icon_3() { return &___Icon_3; }
	inline void set_Icon_3(String_t* value)
	{
		___Icon_3 = value;
		Il2CppCodeGenWriteBarrier((&___Icon_3), value);
	}

	inline static int32_t get_offset_of_SpellAI_4() { return static_cast<int32_t>(offsetof(TrapModel_tC99E2852889B4321B6754DFDDD005862CDC965EC, ___SpellAI_4)); }
	inline int32_t get_SpellAI_4() const { return ___SpellAI_4; }
	inline int32_t* get_address_of_SpellAI_4() { return &___SpellAI_4; }
	inline void set_SpellAI_4(int32_t value)
	{
		___SpellAI_4 = value;
	}

	inline static int32_t get_offset_of_AIData_5() { return static_cast<int32_t>(offsetof(TrapModel_tC99E2852889B4321B6754DFDDD005862CDC965EC, ___AIData_5)); }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * get_AIData_5() const { return ___AIData_5; }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC ** get_address_of_AIData_5() { return &___AIData_5; }
	inline void set_AIData_5(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * value)
	{
		___AIData_5 = value;
		Il2CppCodeGenWriteBarrier((&___AIData_5), value);
	}

	inline static int32_t get_offset_of_Range_6() { return static_cast<int32_t>(offsetof(TrapModel_tC99E2852889B4321B6754DFDDD005862CDC965EC, ___Range_6)); }
	inline float get_Range_6() const { return ___Range_6; }
	inline float* get_address_of_Range_6() { return &___Range_6; }
	inline void set_Range_6(float value)
	{
		___Range_6 = value;
	}

	inline static int32_t get_offset_of_IsScalable_7() { return static_cast<int32_t>(offsetof(TrapModel_tC99E2852889B4321B6754DFDDD005862CDC965EC, ___IsScalable_7)); }
	inline bool get_IsScalable_7() const { return ___IsScalable_7; }
	inline bool* get_address_of_IsScalable_7() { return &___IsScalable_7; }
	inline void set_IsScalable_7(bool value)
	{
		___IsScalable_7 = value;
	}

	inline static int32_t get_offset_of_Spell_8() { return static_cast<int32_t>(offsetof(TrapModel_tC99E2852889B4321B6754DFDDD005862CDC965EC, ___Spell_8)); }
	inline String_t* get_Spell_8() const { return ___Spell_8; }
	inline String_t** get_address_of_Spell_8() { return &___Spell_8; }
	inline void set_Spell_8(String_t* value)
	{
		___Spell_8 = value;
		Il2CppCodeGenWriteBarrier((&___Spell_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRAPMODEL_TC99E2852889B4321B6754DFDDD005862CDC965EC_H
#ifndef SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#define SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_pinvoke : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_com : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifndef BATTLESETTINGSSO_T49214DEA165282C3E2BD7F72B6223E67DF19A3FE_H
#define BATTLESETTINGSSO_T49214DEA165282C3E2BD7F72B6223E67DF19A3FE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BattleSettingsSO
struct  BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// System.String BattleSettingsSO::DefaultBoost
	String_t* ___DefaultBoost_4;
	// System.String BattleSettingsSO::SpecialBoost
	String_t* ___SpecialBoost_5;
	// System.Int32 BattleSettingsSO::BattleInventorySize
	int32_t ___BattleInventorySize_6;
	// System.Int32 BattleSettingsSO::ThreeStarsPercentage
	int32_t ___ThreeStarsPercentage_7;
	// System.Int32 BattleSettingsSO::TwoStarsPercentage
	int32_t ___TwoStarsPercentage_8;
	// System.String BattleSettingsSO::ReviveSpellId
	String_t* ___ReviveSpellId_9;
	// System.Int32 BattleSettingsSO::ReviveCost
	int32_t ___ReviveCost_10;
	// System.String BattleSettingsSO::Spell1
	String_t* ___Spell1_11;
	// System.Int32 BattleSettingsSO::Spell1Unlock
	int32_t ___Spell1Unlock_12;
	// System.Int32 BattleSettingsSO::Spell1InitialValue
	int32_t ___Spell1InitialValue_13;
	// System.String BattleSettingsSO::Spell2
	String_t* ___Spell2_14;
	// System.Int32 BattleSettingsSO::Spell2Unlock
	int32_t ___Spell2Unlock_15;
	// System.Int32 BattleSettingsSO::Spell2InitialValue
	int32_t ___Spell2InitialValue_16;
	// System.String BattleSettingsSO::Spell3
	String_t* ___Spell3_17;
	// System.Int32 BattleSettingsSO::Spell3Unlock
	int32_t ___Spell3Unlock_18;
	// System.Int32 BattleSettingsSO::Spell3InitialValue
	int32_t ___Spell3InitialValue_19;
	// System.String BattleSettingsSO::Spell4
	String_t* ___Spell4_20;
	// System.Int32 BattleSettingsSO::Spell4Unlock
	int32_t ___Spell4Unlock_21;
	// System.Int32 BattleSettingsSO::Spell4InitialValue
	int32_t ___Spell4InitialValue_22;

public:
	inline static int32_t get_offset_of_DefaultBoost_4() { return static_cast<int32_t>(offsetof(BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE, ___DefaultBoost_4)); }
	inline String_t* get_DefaultBoost_4() const { return ___DefaultBoost_4; }
	inline String_t** get_address_of_DefaultBoost_4() { return &___DefaultBoost_4; }
	inline void set_DefaultBoost_4(String_t* value)
	{
		___DefaultBoost_4 = value;
		Il2CppCodeGenWriteBarrier((&___DefaultBoost_4), value);
	}

	inline static int32_t get_offset_of_SpecialBoost_5() { return static_cast<int32_t>(offsetof(BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE, ___SpecialBoost_5)); }
	inline String_t* get_SpecialBoost_5() const { return ___SpecialBoost_5; }
	inline String_t** get_address_of_SpecialBoost_5() { return &___SpecialBoost_5; }
	inline void set_SpecialBoost_5(String_t* value)
	{
		___SpecialBoost_5 = value;
		Il2CppCodeGenWriteBarrier((&___SpecialBoost_5), value);
	}

	inline static int32_t get_offset_of_BattleInventorySize_6() { return static_cast<int32_t>(offsetof(BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE, ___BattleInventorySize_6)); }
	inline int32_t get_BattleInventorySize_6() const { return ___BattleInventorySize_6; }
	inline int32_t* get_address_of_BattleInventorySize_6() { return &___BattleInventorySize_6; }
	inline void set_BattleInventorySize_6(int32_t value)
	{
		___BattleInventorySize_6 = value;
	}

	inline static int32_t get_offset_of_ThreeStarsPercentage_7() { return static_cast<int32_t>(offsetof(BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE, ___ThreeStarsPercentage_7)); }
	inline int32_t get_ThreeStarsPercentage_7() const { return ___ThreeStarsPercentage_7; }
	inline int32_t* get_address_of_ThreeStarsPercentage_7() { return &___ThreeStarsPercentage_7; }
	inline void set_ThreeStarsPercentage_7(int32_t value)
	{
		___ThreeStarsPercentage_7 = value;
	}

	inline static int32_t get_offset_of_TwoStarsPercentage_8() { return static_cast<int32_t>(offsetof(BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE, ___TwoStarsPercentage_8)); }
	inline int32_t get_TwoStarsPercentage_8() const { return ___TwoStarsPercentage_8; }
	inline int32_t* get_address_of_TwoStarsPercentage_8() { return &___TwoStarsPercentage_8; }
	inline void set_TwoStarsPercentage_8(int32_t value)
	{
		___TwoStarsPercentage_8 = value;
	}

	inline static int32_t get_offset_of_ReviveSpellId_9() { return static_cast<int32_t>(offsetof(BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE, ___ReviveSpellId_9)); }
	inline String_t* get_ReviveSpellId_9() const { return ___ReviveSpellId_9; }
	inline String_t** get_address_of_ReviveSpellId_9() { return &___ReviveSpellId_9; }
	inline void set_ReviveSpellId_9(String_t* value)
	{
		___ReviveSpellId_9 = value;
		Il2CppCodeGenWriteBarrier((&___ReviveSpellId_9), value);
	}

	inline static int32_t get_offset_of_ReviveCost_10() { return static_cast<int32_t>(offsetof(BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE, ___ReviveCost_10)); }
	inline int32_t get_ReviveCost_10() const { return ___ReviveCost_10; }
	inline int32_t* get_address_of_ReviveCost_10() { return &___ReviveCost_10; }
	inline void set_ReviveCost_10(int32_t value)
	{
		___ReviveCost_10 = value;
	}

	inline static int32_t get_offset_of_Spell1_11() { return static_cast<int32_t>(offsetof(BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE, ___Spell1_11)); }
	inline String_t* get_Spell1_11() const { return ___Spell1_11; }
	inline String_t** get_address_of_Spell1_11() { return &___Spell1_11; }
	inline void set_Spell1_11(String_t* value)
	{
		___Spell1_11 = value;
		Il2CppCodeGenWriteBarrier((&___Spell1_11), value);
	}

	inline static int32_t get_offset_of_Spell1Unlock_12() { return static_cast<int32_t>(offsetof(BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE, ___Spell1Unlock_12)); }
	inline int32_t get_Spell1Unlock_12() const { return ___Spell1Unlock_12; }
	inline int32_t* get_address_of_Spell1Unlock_12() { return &___Spell1Unlock_12; }
	inline void set_Spell1Unlock_12(int32_t value)
	{
		___Spell1Unlock_12 = value;
	}

	inline static int32_t get_offset_of_Spell1InitialValue_13() { return static_cast<int32_t>(offsetof(BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE, ___Spell1InitialValue_13)); }
	inline int32_t get_Spell1InitialValue_13() const { return ___Spell1InitialValue_13; }
	inline int32_t* get_address_of_Spell1InitialValue_13() { return &___Spell1InitialValue_13; }
	inline void set_Spell1InitialValue_13(int32_t value)
	{
		___Spell1InitialValue_13 = value;
	}

	inline static int32_t get_offset_of_Spell2_14() { return static_cast<int32_t>(offsetof(BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE, ___Spell2_14)); }
	inline String_t* get_Spell2_14() const { return ___Spell2_14; }
	inline String_t** get_address_of_Spell2_14() { return &___Spell2_14; }
	inline void set_Spell2_14(String_t* value)
	{
		___Spell2_14 = value;
		Il2CppCodeGenWriteBarrier((&___Spell2_14), value);
	}

	inline static int32_t get_offset_of_Spell2Unlock_15() { return static_cast<int32_t>(offsetof(BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE, ___Spell2Unlock_15)); }
	inline int32_t get_Spell2Unlock_15() const { return ___Spell2Unlock_15; }
	inline int32_t* get_address_of_Spell2Unlock_15() { return &___Spell2Unlock_15; }
	inline void set_Spell2Unlock_15(int32_t value)
	{
		___Spell2Unlock_15 = value;
	}

	inline static int32_t get_offset_of_Spell2InitialValue_16() { return static_cast<int32_t>(offsetof(BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE, ___Spell2InitialValue_16)); }
	inline int32_t get_Spell2InitialValue_16() const { return ___Spell2InitialValue_16; }
	inline int32_t* get_address_of_Spell2InitialValue_16() { return &___Spell2InitialValue_16; }
	inline void set_Spell2InitialValue_16(int32_t value)
	{
		___Spell2InitialValue_16 = value;
	}

	inline static int32_t get_offset_of_Spell3_17() { return static_cast<int32_t>(offsetof(BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE, ___Spell3_17)); }
	inline String_t* get_Spell3_17() const { return ___Spell3_17; }
	inline String_t** get_address_of_Spell3_17() { return &___Spell3_17; }
	inline void set_Spell3_17(String_t* value)
	{
		___Spell3_17 = value;
		Il2CppCodeGenWriteBarrier((&___Spell3_17), value);
	}

	inline static int32_t get_offset_of_Spell3Unlock_18() { return static_cast<int32_t>(offsetof(BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE, ___Spell3Unlock_18)); }
	inline int32_t get_Spell3Unlock_18() const { return ___Spell3Unlock_18; }
	inline int32_t* get_address_of_Spell3Unlock_18() { return &___Spell3Unlock_18; }
	inline void set_Spell3Unlock_18(int32_t value)
	{
		___Spell3Unlock_18 = value;
	}

	inline static int32_t get_offset_of_Spell3InitialValue_19() { return static_cast<int32_t>(offsetof(BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE, ___Spell3InitialValue_19)); }
	inline int32_t get_Spell3InitialValue_19() const { return ___Spell3InitialValue_19; }
	inline int32_t* get_address_of_Spell3InitialValue_19() { return &___Spell3InitialValue_19; }
	inline void set_Spell3InitialValue_19(int32_t value)
	{
		___Spell3InitialValue_19 = value;
	}

	inline static int32_t get_offset_of_Spell4_20() { return static_cast<int32_t>(offsetof(BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE, ___Spell4_20)); }
	inline String_t* get_Spell4_20() const { return ___Spell4_20; }
	inline String_t** get_address_of_Spell4_20() { return &___Spell4_20; }
	inline void set_Spell4_20(String_t* value)
	{
		___Spell4_20 = value;
		Il2CppCodeGenWriteBarrier((&___Spell4_20), value);
	}

	inline static int32_t get_offset_of_Spell4Unlock_21() { return static_cast<int32_t>(offsetof(BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE, ___Spell4Unlock_21)); }
	inline int32_t get_Spell4Unlock_21() const { return ___Spell4Unlock_21; }
	inline int32_t* get_address_of_Spell4Unlock_21() { return &___Spell4Unlock_21; }
	inline void set_Spell4Unlock_21(int32_t value)
	{
		___Spell4Unlock_21 = value;
	}

	inline static int32_t get_offset_of_Spell4InitialValue_22() { return static_cast<int32_t>(offsetof(BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE, ___Spell4InitialValue_22)); }
	inline int32_t get_Spell4InitialValue_22() const { return ___Spell4InitialValue_22; }
	inline int32_t* get_address_of_Spell4InitialValue_22() { return &___Spell4InitialValue_22; }
	inline void set_Spell4InitialValue_22(int32_t value)
	{
		___Spell4InitialValue_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BATTLESETTINGSSO_T49214DEA165282C3E2BD7F72B6223E67DF19A3FE_H
#ifndef CUSTOMTUTORIALSO_T45FC033F62BB26BC5595EAA9FEF1DF50BFA2A41D_H
#define CUSTOMTUTORIALSO_T45FC033F62BB26BC5595EAA9FEF1DF50BFA2A41D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CustomTutorialSO
struct  CustomTutorialSO_t45FC033F62BB26BC5595EAA9FEF1DF50BFA2A41D  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// System.Collections.Generic.List`1<CustomTutorialModel> CustomTutorialSO::CustomTutorials
	List_1_tA4D8069E4BD0FB418FBF665C39E5CDE768F2CE0C * ___CustomTutorials_4;

public:
	inline static int32_t get_offset_of_CustomTutorials_4() { return static_cast<int32_t>(offsetof(CustomTutorialSO_t45FC033F62BB26BC5595EAA9FEF1DF50BFA2A41D, ___CustomTutorials_4)); }
	inline List_1_tA4D8069E4BD0FB418FBF665C39E5CDE768F2CE0C * get_CustomTutorials_4() const { return ___CustomTutorials_4; }
	inline List_1_tA4D8069E4BD0FB418FBF665C39E5CDE768F2CE0C ** get_address_of_CustomTutorials_4() { return &___CustomTutorials_4; }
	inline void set_CustomTutorials_4(List_1_tA4D8069E4BD0FB418FBF665C39E5CDE768F2CE0C * value)
	{
		___CustomTutorials_4 = value;
		Il2CppCodeGenWriteBarrier((&___CustomTutorials_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMTUTORIALSO_T45FC033F62BB26BC5595EAA9FEF1DF50BFA2A41D_H
#ifndef GAMESETTINGSSO_TC1B94CA47B375640E013220718E7800109B9058C_H
#define GAMESETTINGSSO_TC1B94CA47B375640E013220718E7800109B9058C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSettingsSO
struct  GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// System.Single GameSettingsSO::LeaderboardUpdateTime
	float ___LeaderboardUpdateTime_4;
	// System.Int32 GameSettingsSO::IconCount
	int32_t ___IconCount_5;
	// System.Int32 GameSettingsSO::InitCoins
	int32_t ___InitCoins_6;
	// System.Int32 GameSettingsSO::InitHearts
	int32_t ___InitHearts_7;
	// System.Int32 GameSettingsSO::MaxHearts
	int32_t ___MaxHearts_8;
	// System.Int32 GameSettingsSO::LiveRecoveryTimeInMinutes
	int32_t ___LiveRecoveryTimeInMinutes_9;
	// System.Int32 GameSettingsSO::FirstTimeJoinTeamReward
	int32_t ___FirstTimeJoinTeamReward_10;
	// System.Int32 GameSettingsSO::MinLevelToJoinTeam
	int32_t ___MinLevelToJoinTeam_11;
	// System.String GameSettingsSO::SurveyUrl
	String_t* ___SurveyUrl_12;
	// System.Single GameSettingsSO::GlobalTweenDuration
	float ___GlobalTweenDuration_13;
	// System.Int32 GameSettingsSO::MinLeaderboardCount
	int32_t ___MinLeaderboardCount_14;

public:
	inline static int32_t get_offset_of_LeaderboardUpdateTime_4() { return static_cast<int32_t>(offsetof(GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C, ___LeaderboardUpdateTime_4)); }
	inline float get_LeaderboardUpdateTime_4() const { return ___LeaderboardUpdateTime_4; }
	inline float* get_address_of_LeaderboardUpdateTime_4() { return &___LeaderboardUpdateTime_4; }
	inline void set_LeaderboardUpdateTime_4(float value)
	{
		___LeaderboardUpdateTime_4 = value;
	}

	inline static int32_t get_offset_of_IconCount_5() { return static_cast<int32_t>(offsetof(GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C, ___IconCount_5)); }
	inline int32_t get_IconCount_5() const { return ___IconCount_5; }
	inline int32_t* get_address_of_IconCount_5() { return &___IconCount_5; }
	inline void set_IconCount_5(int32_t value)
	{
		___IconCount_5 = value;
	}

	inline static int32_t get_offset_of_InitCoins_6() { return static_cast<int32_t>(offsetof(GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C, ___InitCoins_6)); }
	inline int32_t get_InitCoins_6() const { return ___InitCoins_6; }
	inline int32_t* get_address_of_InitCoins_6() { return &___InitCoins_6; }
	inline void set_InitCoins_6(int32_t value)
	{
		___InitCoins_6 = value;
	}

	inline static int32_t get_offset_of_InitHearts_7() { return static_cast<int32_t>(offsetof(GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C, ___InitHearts_7)); }
	inline int32_t get_InitHearts_7() const { return ___InitHearts_7; }
	inline int32_t* get_address_of_InitHearts_7() { return &___InitHearts_7; }
	inline void set_InitHearts_7(int32_t value)
	{
		___InitHearts_7 = value;
	}

	inline static int32_t get_offset_of_MaxHearts_8() { return static_cast<int32_t>(offsetof(GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C, ___MaxHearts_8)); }
	inline int32_t get_MaxHearts_8() const { return ___MaxHearts_8; }
	inline int32_t* get_address_of_MaxHearts_8() { return &___MaxHearts_8; }
	inline void set_MaxHearts_8(int32_t value)
	{
		___MaxHearts_8 = value;
	}

	inline static int32_t get_offset_of_LiveRecoveryTimeInMinutes_9() { return static_cast<int32_t>(offsetof(GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C, ___LiveRecoveryTimeInMinutes_9)); }
	inline int32_t get_LiveRecoveryTimeInMinutes_9() const { return ___LiveRecoveryTimeInMinutes_9; }
	inline int32_t* get_address_of_LiveRecoveryTimeInMinutes_9() { return &___LiveRecoveryTimeInMinutes_9; }
	inline void set_LiveRecoveryTimeInMinutes_9(int32_t value)
	{
		___LiveRecoveryTimeInMinutes_9 = value;
	}

	inline static int32_t get_offset_of_FirstTimeJoinTeamReward_10() { return static_cast<int32_t>(offsetof(GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C, ___FirstTimeJoinTeamReward_10)); }
	inline int32_t get_FirstTimeJoinTeamReward_10() const { return ___FirstTimeJoinTeamReward_10; }
	inline int32_t* get_address_of_FirstTimeJoinTeamReward_10() { return &___FirstTimeJoinTeamReward_10; }
	inline void set_FirstTimeJoinTeamReward_10(int32_t value)
	{
		___FirstTimeJoinTeamReward_10 = value;
	}

	inline static int32_t get_offset_of_MinLevelToJoinTeam_11() { return static_cast<int32_t>(offsetof(GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C, ___MinLevelToJoinTeam_11)); }
	inline int32_t get_MinLevelToJoinTeam_11() const { return ___MinLevelToJoinTeam_11; }
	inline int32_t* get_address_of_MinLevelToJoinTeam_11() { return &___MinLevelToJoinTeam_11; }
	inline void set_MinLevelToJoinTeam_11(int32_t value)
	{
		___MinLevelToJoinTeam_11 = value;
	}

	inline static int32_t get_offset_of_SurveyUrl_12() { return static_cast<int32_t>(offsetof(GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C, ___SurveyUrl_12)); }
	inline String_t* get_SurveyUrl_12() const { return ___SurveyUrl_12; }
	inline String_t** get_address_of_SurveyUrl_12() { return &___SurveyUrl_12; }
	inline void set_SurveyUrl_12(String_t* value)
	{
		___SurveyUrl_12 = value;
		Il2CppCodeGenWriteBarrier((&___SurveyUrl_12), value);
	}

	inline static int32_t get_offset_of_GlobalTweenDuration_13() { return static_cast<int32_t>(offsetof(GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C, ___GlobalTweenDuration_13)); }
	inline float get_GlobalTweenDuration_13() const { return ___GlobalTweenDuration_13; }
	inline float* get_address_of_GlobalTweenDuration_13() { return &___GlobalTweenDuration_13; }
	inline void set_GlobalTweenDuration_13(float value)
	{
		___GlobalTweenDuration_13 = value;
	}

	inline static int32_t get_offset_of_MinLeaderboardCount_14() { return static_cast<int32_t>(offsetof(GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C, ___MinLeaderboardCount_14)); }
	inline int32_t get_MinLeaderboardCount_14() const { return ___MinLeaderboardCount_14; }
	inline int32_t* get_address_of_MinLeaderboardCount_14() { return &___MinLeaderboardCount_14; }
	inline void set_MinLeaderboardCount_14(int32_t value)
	{
		___MinLeaderboardCount_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMESETTINGSSO_TC1B94CA47B375640E013220718E7800109B9058C_H
#ifndef ITEMSSO_TD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A_H
#define ITEMSSO_TD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ItemsSO
struct  ItemsSO_tD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// System.Collections.Generic.List`1<ItemModel> ItemsSO::Items
	List_1_t0B47A19D77897D2BC30A0820609E3BBA2156F084 * ___Items_4;

public:
	inline static int32_t get_offset_of_Items_4() { return static_cast<int32_t>(offsetof(ItemsSO_tD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A, ___Items_4)); }
	inline List_1_t0B47A19D77897D2BC30A0820609E3BBA2156F084 * get_Items_4() const { return ___Items_4; }
	inline List_1_t0B47A19D77897D2BC30A0820609E3BBA2156F084 ** get_address_of_Items_4() { return &___Items_4; }
	inline void set_Items_4(List_1_t0B47A19D77897D2BC30A0820609E3BBA2156F084 * value)
	{
		___Items_4 = value;
		Il2CppCodeGenWriteBarrier((&___Items_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ITEMSSO_TD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A_H
#ifndef LEVELSSO_T51232A976915E19D66106CE9DD068463ADBAD0EF_H
#define LEVELSSO_T51232A976915E19D66106CE9DD068463ADBAD0EF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LevelsSO
struct  LevelsSO_t51232A976915E19D66106CE9DD068463ADBAD0EF  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,LevelModel> LevelsSO::Levels
	Dictionary_2_t9C3570F5044A5A45EA82770EF6D53748E89DD9C5 * ___Levels_4;

public:
	inline static int32_t get_offset_of_Levels_4() { return static_cast<int32_t>(offsetof(LevelsSO_t51232A976915E19D66106CE9DD068463ADBAD0EF, ___Levels_4)); }
	inline Dictionary_2_t9C3570F5044A5A45EA82770EF6D53748E89DD9C5 * get_Levels_4() const { return ___Levels_4; }
	inline Dictionary_2_t9C3570F5044A5A45EA82770EF6D53748E89DD9C5 ** get_address_of_Levels_4() { return &___Levels_4; }
	inline void set_Levels_4(Dictionary_2_t9C3570F5044A5A45EA82770EF6D53748E89DD9C5 * value)
	{
		___Levels_4 = value;
		Il2CppCodeGenWriteBarrier((&___Levels_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELSSO_T51232A976915E19D66106CE9DD068463ADBAD0EF_H
#ifndef PERLEVELTUTORIALSO_TB55F6372D80FCA75DD834538D5137169A7A7E66B_H
#define PERLEVELTUTORIALSO_TB55F6372D80FCA75DD834538D5137169A7A7E66B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PerLevelTutorialSO
struct  PerLevelTutorialSO_tB55F6372D80FCA75DD834538D5137169A7A7E66B  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// System.Collections.Generic.List`1<PerLevelTutorialModel> PerLevelTutorialSO::Tutorials
	List_1_tBE32162E196BAE003F483B65C07526E97560480C * ___Tutorials_4;

public:
	inline static int32_t get_offset_of_Tutorials_4() { return static_cast<int32_t>(offsetof(PerLevelTutorialSO_tB55F6372D80FCA75DD834538D5137169A7A7E66B, ___Tutorials_4)); }
	inline List_1_tBE32162E196BAE003F483B65C07526E97560480C * get_Tutorials_4() const { return ___Tutorials_4; }
	inline List_1_tBE32162E196BAE003F483B65C07526E97560480C ** get_address_of_Tutorials_4() { return &___Tutorials_4; }
	inline void set_Tutorials_4(List_1_tBE32162E196BAE003F483B65C07526E97560480C * value)
	{
		___Tutorials_4 = value;
		Il2CppCodeGenWriteBarrier((&___Tutorials_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PERLEVELTUTORIALSO_TB55F6372D80FCA75DD834538D5137169A7A7E66B_H
#ifndef SOUNDSO_T9B0D26B0A9F986D8019428EBC54B7CC6259E2C73_H
#define SOUNDSO_T9B0D26B0A9F986D8019428EBC54B7CC6259E2C73_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SoundSO
struct  SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// System.String SoundSO::BackgroundMain
	String_t* ___BackgroundMain_4;
	// System.String SoundSO::BackgroundBattle
	String_t* ___BackgroundBattle_5;
	// System.String SoundSO::Button
	String_t* ___Button_6;
	// System.String SoundSO::SummaryWin
	String_t* ___SummaryWin_7;
	// System.String SoundSO::SummaryLose
	String_t* ___SummaryLose_8;
	// System.String SoundSO::Sword
	String_t* ___Sword_9;
	// System.String SoundSO::Damage
	String_t* ___Damage_10;
	// System.Single SoundSO::DamageDelay
	float ___DamageDelay_11;

public:
	inline static int32_t get_offset_of_BackgroundMain_4() { return static_cast<int32_t>(offsetof(SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73, ___BackgroundMain_4)); }
	inline String_t* get_BackgroundMain_4() const { return ___BackgroundMain_4; }
	inline String_t** get_address_of_BackgroundMain_4() { return &___BackgroundMain_4; }
	inline void set_BackgroundMain_4(String_t* value)
	{
		___BackgroundMain_4 = value;
		Il2CppCodeGenWriteBarrier((&___BackgroundMain_4), value);
	}

	inline static int32_t get_offset_of_BackgroundBattle_5() { return static_cast<int32_t>(offsetof(SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73, ___BackgroundBattle_5)); }
	inline String_t* get_BackgroundBattle_5() const { return ___BackgroundBattle_5; }
	inline String_t** get_address_of_BackgroundBattle_5() { return &___BackgroundBattle_5; }
	inline void set_BackgroundBattle_5(String_t* value)
	{
		___BackgroundBattle_5 = value;
		Il2CppCodeGenWriteBarrier((&___BackgroundBattle_5), value);
	}

	inline static int32_t get_offset_of_Button_6() { return static_cast<int32_t>(offsetof(SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73, ___Button_6)); }
	inline String_t* get_Button_6() const { return ___Button_6; }
	inline String_t** get_address_of_Button_6() { return &___Button_6; }
	inline void set_Button_6(String_t* value)
	{
		___Button_6 = value;
		Il2CppCodeGenWriteBarrier((&___Button_6), value);
	}

	inline static int32_t get_offset_of_SummaryWin_7() { return static_cast<int32_t>(offsetof(SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73, ___SummaryWin_7)); }
	inline String_t* get_SummaryWin_7() const { return ___SummaryWin_7; }
	inline String_t** get_address_of_SummaryWin_7() { return &___SummaryWin_7; }
	inline void set_SummaryWin_7(String_t* value)
	{
		___SummaryWin_7 = value;
		Il2CppCodeGenWriteBarrier((&___SummaryWin_7), value);
	}

	inline static int32_t get_offset_of_SummaryLose_8() { return static_cast<int32_t>(offsetof(SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73, ___SummaryLose_8)); }
	inline String_t* get_SummaryLose_8() const { return ___SummaryLose_8; }
	inline String_t** get_address_of_SummaryLose_8() { return &___SummaryLose_8; }
	inline void set_SummaryLose_8(String_t* value)
	{
		___SummaryLose_8 = value;
		Il2CppCodeGenWriteBarrier((&___SummaryLose_8), value);
	}

	inline static int32_t get_offset_of_Sword_9() { return static_cast<int32_t>(offsetof(SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73, ___Sword_9)); }
	inline String_t* get_Sword_9() const { return ___Sword_9; }
	inline String_t** get_address_of_Sword_9() { return &___Sword_9; }
	inline void set_Sword_9(String_t* value)
	{
		___Sword_9 = value;
		Il2CppCodeGenWriteBarrier((&___Sword_9), value);
	}

	inline static int32_t get_offset_of_Damage_10() { return static_cast<int32_t>(offsetof(SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73, ___Damage_10)); }
	inline String_t* get_Damage_10() const { return ___Damage_10; }
	inline String_t** get_address_of_Damage_10() { return &___Damage_10; }
	inline void set_Damage_10(String_t* value)
	{
		___Damage_10 = value;
		Il2CppCodeGenWriteBarrier((&___Damage_10), value);
	}

	inline static int32_t get_offset_of_DamageDelay_11() { return static_cast<int32_t>(offsetof(SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73, ___DamageDelay_11)); }
	inline float get_DamageDelay_11() const { return ___DamageDelay_11; }
	inline float* get_address_of_DamageDelay_11() { return &___DamageDelay_11; }
	inline void set_DamageDelay_11(float value)
	{
		___DamageDelay_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOUNDSO_T9B0D26B0A9F986D8019428EBC54B7CC6259E2C73_H
#ifndef SPELLSSO_TD87C28C149EA2BE80535490B95DC914E94705439_H
#define SPELLSSO_TD87C28C149EA2BE80535490B95DC914E94705439_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpellsSO
struct  SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// System.Collections.Generic.List`1<SpellModel> SpellsSO::Spells
	List_1_t3C7DBA87FE72C14B6D9818C99E13D8ED9660373F * ___Spells_4;

public:
	inline static int32_t get_offset_of_Spells_4() { return static_cast<int32_t>(offsetof(SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439, ___Spells_4)); }
	inline List_1_t3C7DBA87FE72C14B6D9818C99E13D8ED9660373F * get_Spells_4() const { return ___Spells_4; }
	inline List_1_t3C7DBA87FE72C14B6D9818C99E13D8ED9660373F ** get_address_of_Spells_4() { return &___Spells_4; }
	inline void set_Spells_4(List_1_t3C7DBA87FE72C14B6D9818C99E13D8ED9660373F * value)
	{
		___Spells_4 = value;
		Il2CppCodeGenWriteBarrier((&___Spells_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPELLSSO_TD87C28C149EA2BE80535490B95DC914E94705439_H
#ifndef TOURNAMENTREWARDSSO_T9121EDF677AEDF239EF74868C27C0879178523E1_H
#define TOURNAMENTREWARDSSO_T9121EDF677AEDF239EF74868C27C0879178523E1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TournamentRewardsSO
struct  TournamentRewardsSO_t9121EDF677AEDF239EF74868C27C0879178523E1  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// RewardModel[] TournamentRewardsSO::Rewards
	RewardModelU5BU5D_t5B4D5EB45F969FE53860B9964AEB5E519B048C11* ___Rewards_4;

public:
	inline static int32_t get_offset_of_Rewards_4() { return static_cast<int32_t>(offsetof(TournamentRewardsSO_t9121EDF677AEDF239EF74868C27C0879178523E1, ___Rewards_4)); }
	inline RewardModelU5BU5D_t5B4D5EB45F969FE53860B9964AEB5E519B048C11* get_Rewards_4() const { return ___Rewards_4; }
	inline RewardModelU5BU5D_t5B4D5EB45F969FE53860B9964AEB5E519B048C11** get_address_of_Rewards_4() { return &___Rewards_4; }
	inline void set_Rewards_4(RewardModelU5BU5D_t5B4D5EB45F969FE53860B9964AEB5E519B048C11* value)
	{
		___Rewards_4 = value;
		Il2CppCodeGenWriteBarrier((&___Rewards_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOURNAMENTREWARDSSO_T9121EDF677AEDF239EF74868C27C0879178523E1_H
#ifndef TOWERSSO_T3B7DE821E5F8017950776EEFC42E680B361BCDF0_H
#define TOWERSSO_T3B7DE821E5F8017950776EEFC42E680B361BCDF0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TowersSO
struct  TowersSO_t3B7DE821E5F8017950776EEFC42E680B361BCDF0  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// System.Collections.Generic.List`1<TowerModel> TowersSO::Towers
	List_1_t7D23CBEBEF47512C344C65315325FF324D97FC1F * ___Towers_4;

public:
	inline static int32_t get_offset_of_Towers_4() { return static_cast<int32_t>(offsetof(TowersSO_t3B7DE821E5F8017950776EEFC42E680B361BCDF0, ___Towers_4)); }
	inline List_1_t7D23CBEBEF47512C344C65315325FF324D97FC1F * get_Towers_4() const { return ___Towers_4; }
	inline List_1_t7D23CBEBEF47512C344C65315325FF324D97FC1F ** get_address_of_Towers_4() { return &___Towers_4; }
	inline void set_Towers_4(List_1_t7D23CBEBEF47512C344C65315325FF324D97FC1F * value)
	{
		___Towers_4 = value;
		Il2CppCodeGenWriteBarrier((&___Towers_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOWERSSO_T3B7DE821E5F8017950776EEFC42E680B361BCDF0_H
#ifndef TRAPSSO_TFF060852B3652DD05F33E1B19B8EC9166468D9C2_H
#define TRAPSSO_TFF060852B3652DD05F33E1B19B8EC9166468D9C2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TrapsSO
struct  TrapsSO_tFF060852B3652DD05F33E1B19B8EC9166468D9C2  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// System.Collections.Generic.List`1<TrapModel> TrapsSO::Traps
	List_1_t5E828DBB770C7425713DBEAE9366AFC3C1C4ADA7 * ___Traps_4;

public:
	inline static int32_t get_offset_of_Traps_4() { return static_cast<int32_t>(offsetof(TrapsSO_tFF060852B3652DD05F33E1B19B8EC9166468D9C2, ___Traps_4)); }
	inline List_1_t5E828DBB770C7425713DBEAE9366AFC3C1C4ADA7 * get_Traps_4() const { return ___Traps_4; }
	inline List_1_t5E828DBB770C7425713DBEAE9366AFC3C1C4ADA7 ** get_address_of_Traps_4() { return &___Traps_4; }
	inline void set_Traps_4(List_1_t5E828DBB770C7425713DBEAE9366AFC3C1C4ADA7 * value)
	{
		___Traps_4 = value;
		Il2CppCodeGenWriteBarrier((&___Traps_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRAPSSO_TFF060852B3652DD05F33E1B19B8EC9166468D9C2_H
#ifndef UNITSSO_T8A4174A0E155362BFFAA11BE367BD31F8D001423_H
#define UNITSSO_T8A4174A0E155362BFFAA11BE367BD31F8D001423_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnitsSO
struct  UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// System.Collections.Generic.List`1<UnitsModel> UnitsSO::Units
	List_1_tD552FACB362EB8A9DA9B6EDB71E741DA5D570834 * ___Units_4;

public:
	inline static int32_t get_offset_of_Units_4() { return static_cast<int32_t>(offsetof(UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423, ___Units_4)); }
	inline List_1_tD552FACB362EB8A9DA9B6EDB71E741DA5D570834 * get_Units_4() const { return ___Units_4; }
	inline List_1_tD552FACB362EB8A9DA9B6EDB71E741DA5D570834 ** get_address_of_Units_4() { return &___Units_4; }
	inline void set_Units_4(List_1_tD552FACB362EB8A9DA9B6EDB71E741DA5D570834 * value)
	{
		___Units_4 = value;
		Il2CppCodeGenWriteBarrier((&___Units_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITSSO_T8A4174A0E155362BFFAA11BE367BD31F8D001423_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8400 = { sizeof (LevelChestEvent_tE4A44711C3740FE5BCA5CBEAE24289CB248D4429), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8401 = { sizeof (StarChestCommand_t3EFBF99A4EE9670D214FA091E39710C0B69923F8), -1, sizeof(StarChestCommand_t3EFBF99A4EE9670D214FA091E39710C0B69923F8_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8401[6] = 
{
	StarChestCommand_t3EFBF99A4EE9670D214FA091E39710C0B69923F8::get_offset_of__starsChest_0(),
	StarChestCommand_t3EFBF99A4EE9670D214FA091E39710C0B69923F8::get_offset_of__tournamentsVO_1(),
	StarChestCommand_t3EFBF99A4EE9670D214FA091E39710C0B69923F8::get_offset_of__inventorySystem_2(),
	StarChestCommand_t3EFBF99A4EE9670D214FA091E39710C0B69923F8::get_offset_of__inventoryVO_3(),
	StarChestCommand_t3EFBF99A4EE9670D214FA091E39710C0B69923F8::get_offset_of__voSaver_4(),
	StarChestCommand_t3EFBF99A4EE9670D214FA091E39710C0B69923F8_StaticFields::get_offset_of_StarChestCommandEvent_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8402 = { sizeof (StarChestCommandModel_t409270279AE02F8F9215517A80707E19E7700234), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8403 = { sizeof (StarChestCommandResultModel_t1C69C4D7DFAE2E5B169B6C02222DB48508EA2191), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8403[1] = 
{
	StarChestCommandResultModel_t1C69C4D7DFAE2E5B169B6C02222DB48508EA2191::get_offset_of_Rewards_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8404 = { sizeof (StarChestEvent_t0797A526A463F05F371BD49E20AE31B774586F43), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8405 = { sizeof (FillLivesCommand_t4837FA208059DB83DC9BC312E0AC4FCD51019733), -1, sizeof(FillLivesCommand_t4837FA208059DB83DC9BC312E0AC4FCD51019733_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8405[5] = 
{
	FillLivesCommand_t4837FA208059DB83DC9BC312E0AC4FCD51019733::get_offset_of_InventorySystem_0(),
	FillLivesCommand_t4837FA208059DB83DC9BC312E0AC4FCD51019733::get_offset_of__voSaver_1(),
	FillLivesCommand_t4837FA208059DB83DC9BC312E0AC4FCD51019733::get_offset_of__inventoryVO_2(),
	FillLivesCommand_t4837FA208059DB83DC9BC312E0AC4FCD51019733::get_offset_of__gameSettingsSO_3(),
	FillLivesCommand_t4837FA208059DB83DC9BC312E0AC4FCD51019733_StaticFields::get_offset_of_FillLivesCommandEvent_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8406 = { sizeof (FillLivesCommandModel_t4F764E839800C5918E0C8A7B08E464EA8C88F084), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8407 = { sizeof (FillLivesCommandResultModel_t917B92ECF3B5C550EBD4B28257BB8F1D58C6D18A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8408 = { sizeof (FillLivesEvent_tBC3C967A3339323DC976C55B063FCD0FF64D2568), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8409 = { sizeof (HeartUtils_t75148D84D8BFBC8993AF840CA04824A4D8E24808), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8410 = { sizeof (LoginCommand_t2A7CDE454F84D34A9AABFD4605747CADE3A26427), -1, sizeof(LoginCommand_t2A7CDE454F84D34A9AABFD4605747CADE3A26427_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8410[6] = 
{
	LoginCommand_t2A7CDE454F84D34A9AABFD4605747CADE3A26427::get_offset_of__globalVO_0(),
	LoginCommand_t2A7CDE454F84D34A9AABFD4605747CADE3A26427::get_offset_of__userVO_1(),
	LoginCommand_t2A7CDE454F84D34A9AABFD4605747CADE3A26427::get_offset_of__tournamentsVO_2(),
	LoginCommand_t2A7CDE454F84D34A9AABFD4605747CADE3A26427::get_offset_of__voSaver_3(),
	LoginCommand_t2A7CDE454F84D34A9AABFD4605747CADE3A26427::get_offset_of__metadataVO_4(),
	LoginCommand_t2A7CDE454F84D34A9AABFD4605747CADE3A26427_StaticFields::get_offset_of_LoginCommandEvent_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8411 = { sizeof (LoginCommandModel_t24D0303EC2BAA2132036E25E25AB91AC1FFACCD0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8411[1] = 
{
	LoginCommandModel_t24D0303EC2BAA2132036E25E25AB91AC1FFACCD0::get_offset_of_ResponseData_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8412 = { sizeof (LoginCommandResultModel_t397DF3FF54EC195B423F09100D36D1A4A7D78893), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8413 = { sizeof (LoginCommandEvent_tE333B28F8632EC5795F9FCD22A6CC7EB36F1C493), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8414 = { sizeof (PatchingCompletedCommand_t005C53F233E61DE0A4F2905D5D200A52BA27C414), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8414[3] = 
{
	PatchingCompletedCommand_t005C53F233E61DE0A4F2905D5D200A52BA27C414::get_offset_of__patchingVO_0(),
	PatchingCompletedCommand_t005C53F233E61DE0A4F2905D5D200A52BA27C414::get_offset_of__globalVO_1(),
	PatchingCompletedCommand_t005C53F233E61DE0A4F2905D5D200A52BA27C414::get_offset_of__voSaver_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8415 = { sizeof (PatchingCompletedCommandModel_tE4A47B81CCDC996363955B3FC4E4EF1B06E1A8F4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8415[2] = 
{
	PatchingCompletedCommandModel_tE4A47B81CCDC996363955B3FC4E4EF1B06E1A8F4::get_offset_of_Hash_0(),
	PatchingCompletedCommandModel_tE4A47B81CCDC996363955B3FC4E4EF1B06E1A8F4::get_offset_of_Version_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8416 = { sizeof (PatchingCompletedCommandModelResult_t96CB76D8E153FAA3C0D2F43327591BC428EC6CF5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8417 = { sizeof (PatchingUpdatedCommand_t8183DE064E78F1689A29A91EF9B6D4C9B42C731D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8417[2] = 
{
	PatchingUpdatedCommand_t8183DE064E78F1689A29A91EF9B6D4C9B42C731D::get_offset_of__patchingVO_0(),
	PatchingUpdatedCommand_t8183DE064E78F1689A29A91EF9B6D4C9B42C731D::get_offset_of__voSaver_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8418 = { sizeof (PatchingUpdatedCommandModel_t2445D63426E3FC82073263F556AA8C4103A4E4C1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8418[3] = 
{
	PatchingUpdatedCommandModel_t2445D63426E3FC82073263F556AA8C4103A4E4C1::get_offset_of_Hash_0(),
	PatchingUpdatedCommandModel_t2445D63426E3FC82073263F556AA8C4103A4E4C1::get_offset_of_Version_1(),
	PatchingUpdatedCommandModel_t2445D63426E3FC82073263F556AA8C4103A4E4C1::get_offset_of_MissingAssets_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8419 = { sizeof (PatchingUpdatedCommandResultModel_t2B339406188ABE674F022106438A21C1474AD480), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8420 = { sizeof (UpdateSettingsCommand_t67A15F1B0D6B07C873DEF9D9A76B92778DF552D0), -1, sizeof(UpdateSettingsCommand_t67A15F1B0D6B07C873DEF9D9A76B92778DF552D0_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8420[3] = 
{
	UpdateSettingsCommand_t67A15F1B0D6B07C873DEF9D9A76B92778DF552D0::get_offset_of__userVO_0(),
	UpdateSettingsCommand_t67A15F1B0D6B07C873DEF9D9A76B92778DF552D0::get_offset_of__voSaver_1(),
	UpdateSettingsCommand_t67A15F1B0D6B07C873DEF9D9A76B92778DF552D0_StaticFields::get_offset_of_UpdateSettingsCommandEvent_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8421 = { sizeof (UpdateSettingsCommandModel_tCBDA27C0FF5752D47D9532DDDB23FA2611F9E040), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8421[2] = 
{
	UpdateSettingsCommandModel_tCBDA27C0FF5752D47D9532DDDB23FA2611F9E040::get_offset_of_Fx_0(),
	UpdateSettingsCommandModel_tCBDA27C0FF5752D47D9532DDDB23FA2611F9E040::get_offset_of_Music_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8422 = { sizeof (UpdateSettingsCommandResult_t1C5BAE390EC340A9EED4A392D498941FF9EFAFFE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8423 = { sizeof (UpdateSettingsEvent_tBDFC5C0BE56631615A2FE1F8DF1593535637EC8F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8424 = { sizeof (ClaimTournamentRewardCommand_t39383095AB8EFBB4C86B2DAE8F1A7DE4A474F6DF), -1, sizeof(ClaimTournamentRewardCommand_t39383095AB8EFBB4C86B2DAE8F1A7DE4A474F6DF_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8424[3] = 
{
	ClaimTournamentRewardCommand_t39383095AB8EFBB4C86B2DAE8F1A7DE4A474F6DF::get_offset_of__tournamentsVO_0(),
	ClaimTournamentRewardCommand_t39383095AB8EFBB4C86B2DAE8F1A7DE4A474F6DF::get_offset_of__voSaver_1(),
	ClaimTournamentRewardCommand_t39383095AB8EFBB4C86B2DAE8F1A7DE4A474F6DF_StaticFields::get_offset_of_ClaimTournamentRewardCommandEvent_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8425 = { sizeof (ClaimTournamentRewardCommandModel_t4ADDCB2DC663FF9A7A7BF212E7D3764861368E1D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8425[2] = 
{
	ClaimTournamentRewardCommandModel_t4ADDCB2DC663FF9A7A7BF212E7D3764861368E1D::get_offset_of_TournamentId_0(),
	ClaimTournamentRewardCommandModel_t4ADDCB2DC663FF9A7A7BF212E7D3764861368E1D::get_offset_of_ResponseData_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8426 = { sizeof (ClaimTournamentRewardCommandResult_tECE3DBA1983CEB89B4011D09194CF0ABE5EFD193), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8426[1] = 
{
	ClaimTournamentRewardCommandResult_tECE3DBA1983CEB89B4011D09194CF0ABE5EFD193::get_offset_of_Rewards_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8427 = { sizeof (ClaimTournamentRewardEvent_t31E71F6AB2C04F3BD77BC744E3F0CE51D5CB0D63), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8428 = { sizeof (TournamentResultCommand_t952310C8FDEEE1E034696B42AD609BC49FE86085), -1, sizeof(TournamentResultCommand_t952310C8FDEEE1E034696B42AD609BC49FE86085_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8428[3] = 
{
	TournamentResultCommand_t952310C8FDEEE1E034696B42AD609BC49FE86085::get_offset_of__tournamentsVO_0(),
	TournamentResultCommand_t952310C8FDEEE1E034696B42AD609BC49FE86085::get_offset_of__voSaver_1(),
	TournamentResultCommand_t952310C8FDEEE1E034696B42AD609BC49FE86085_StaticFields::get_offset_of_TournamentResultCommandEvent_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8429 = { sizeof (TournamentResultCommandModel_t85DE82B1305AA756766D74891EF4404FF50367B6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8429[1] = 
{
	TournamentResultCommandModel_t85DE82B1305AA756766D74891EF4404FF50367B6::get_offset_of_ResponseData_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8430 = { sizeof (TournamentResultCommandModelResult_tCCEC4575292DCA9D77CE4E9D594BA31EDE162F77), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8431 = { sizeof (TournamentResultCommandEvent_t63203EB3B05D0FB9B44625DF65238A1FDB642012), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8432 = { sizeof (TutorialCompletedCommand_tC0EC8D50886D81D69556E9C075A66E78C3B6A2DC), -1, sizeof(TutorialCompletedCommand_tC0EC8D50886D81D69556E9C075A66E78C3B6A2DC_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8432[3] = 
{
	TutorialCompletedCommand_tC0EC8D50886D81D69556E9C075A66E78C3B6A2DC::get_offset_of__userVO_0(),
	TutorialCompletedCommand_tC0EC8D50886D81D69556E9C075A66E78C3B6A2DC::get_offset_of__voSaver_1(),
	TutorialCompletedCommand_tC0EC8D50886D81D69556E9C075A66E78C3B6A2DC_StaticFields::get_offset_of_ClaimTournamentRewardCommandEvent_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8433 = { sizeof (TutorialCompletedCommandModel_t8E735DB75DAEA8DB6988603694DAE3DC5A702561), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8433[1] = 
{
	TutorialCompletedCommandModel_t8E735DB75DAEA8DB6988603694DAE3DC5A702561::get_offset_of_TutorialId_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8434 = { sizeof (TutorialCompletedCommandResult_t28E5178ED159419CFF37D5F86599383281ABF500), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8435 = { sizeof (TutorialCompletedEvent_t6DBFFBCD3D5CD8DEA1865FB0036F52F297D85EFE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8436 = { sizeof (BattleConstants_t9FB60E5A8095C645C4A9066C587967D9B457C162), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8436[15] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8437 = { sizeof (GameMainConfigs_t0F18F8075F6195A8B2EC2FCB69D934E925EA071A), -1, sizeof(GameMainConfigs_t0F18F8075F6195A8B2EC2FCB69D934E925EA071A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8437[1] = 
{
	GameMainConfigs_t0F18F8075F6195A8B2EC2FCB69D934E925EA071A_StaticFields::get_offset_of__gameConfigs_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8438 = { sizeof (ConfModel_t22142FE7B971DACF1840795E24BED946C0D8A77F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8438[2] = 
{
	ConfModel_t22142FE7B971DACF1840795E24BED946C0D8A77F::get_offset_of_FileName_0(),
	ConfModel_t22142FE7B971DACF1840795E24BED946C0D8A77F::get_offset_of_Type_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8439 = { sizeof (GameConstants_t1A630C2F5F2E6945735E954CED4C0833EDAE31CC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8439[72] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8440 = { sizeof (GameDiResolver_t866ECA664BF6463B4E179E3635C89291E8FD8A33), -1, sizeof(GameDiResolver_t866ECA664BF6463B4E179E3635C89291E8FD8A33_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8440[2] = 
{
	GameDiResolver_t866ECA664BF6463B4E179E3635C89291E8FD8A33_StaticFields::get_offset_of__gameContainer_0(),
	GameDiResolver_t866ECA664BF6463B4E179E3635C89291E8FD8A33_StaticFields::get_offset_of__battleContainer_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8441 = { sizeof (GamePathResolver_t5EF708FE19C455A8D6940A65EB9A098F50237644), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8442 = { sizeof (GameState_tABB839413827DD148BD92CA09A37DA4C24372F36), -1, sizeof(GameState_tABB839413827DD148BD92CA09A37DA4C24372F36_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8442[1] = 
{
	GameState_tABB839413827DD148BD92CA09A37DA4C24372F36_StaticFields::get_offset_of_TeamCount_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8443 = { sizeof (BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8443[19] = 
{
	BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE::get_offset_of_DefaultBoost_4(),
	BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE::get_offset_of_SpecialBoost_5(),
	BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE::get_offset_of_BattleInventorySize_6(),
	BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE::get_offset_of_ThreeStarsPercentage_7(),
	BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE::get_offset_of_TwoStarsPercentage_8(),
	BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE::get_offset_of_ReviveSpellId_9(),
	BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE::get_offset_of_ReviveCost_10(),
	BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE::get_offset_of_Spell1_11(),
	BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE::get_offset_of_Spell1Unlock_12(),
	BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE::get_offset_of_Spell1InitialValue_13(),
	BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE::get_offset_of_Spell2_14(),
	BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE::get_offset_of_Spell2Unlock_15(),
	BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE::get_offset_of_Spell2InitialValue_16(),
	BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE::get_offset_of_Spell3_17(),
	BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE::get_offset_of_Spell3Unlock_18(),
	BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE::get_offset_of_Spell3InitialValue_19(),
	BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE::get_offset_of_Spell4_20(),
	BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE::get_offset_of_Spell4Unlock_21(),
	BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE::get_offset_of_Spell4InitialValue_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8444 = { sizeof (ChaptersSO_t6D25AB47CA5A386B1455B167AA805B0CAB2B24C1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8444[1] = 
{
	ChaptersSO_t6D25AB47CA5A386B1455B167AA805B0CAB2B24C1::get_offset_of_Chapters_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8445 = { sizeof (ChapterModel_t128F63AB2A81208183CD6AC72466B96980DA0494), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8445[2] = 
{
	ChapterModel_t128F63AB2A81208183CD6AC72466B96980DA0494::get_offset_of_Bundle_0(),
	ChapterModel_t128F63AB2A81208183CD6AC72466B96980DA0494::get_offset_of_MaxLevel_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8446 = { sizeof (ChestModelSO_t78F97E6AC8F488AD3C18236C4080E78CCF29B2A2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8446[1] = 
{
	ChestModelSO_t78F97E6AC8F488AD3C18236C4080E78CCF29B2A2::get_offset_of_Items_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8447 = { sizeof (ChestItemModel_tFB97FEEB81B57C2A0C1DDD9C5662A3965197EFA8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8447[4] = 
{
	ChestItemModel_tFB97FEEB81B57C2A0C1DDD9C5662A3965197EFA8::get_offset_of_Content_0(),
	ChestItemModel_tFB97FEEB81B57C2A0C1DDD9C5662A3965197EFA8::get_offset_of_Percentage_1(),
	ChestItemModel_tFB97FEEB81B57C2A0C1DDD9C5662A3965197EFA8::get_offset_of_Number_2(),
	ChestItemModel_tFB97FEEB81B57C2A0C1DDD9C5662A3965197EFA8::get_offset_of_IsUnique_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8448 = { sizeof (ItemContentModel_t7F429490208DE8F65A9F2278897FF14267F9983D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8448[4] = 
{
	ItemContentModel_t7F429490208DE8F65A9F2278897FF14267F9983D::get_offset_of_Reward_0(),
	ItemContentModel_t7F429490208DE8F65A9F2278897FF14267F9983D::get_offset_of_Weight_1(),
	ItemContentModel_t7F429490208DE8F65A9F2278897FF14267F9983D::get_offset_of_Prefab_2(),
	ItemContentModel_t7F429490208DE8F65A9F2278897FF14267F9983D::get_offset_of_RequirementStr_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8449 = { sizeof (RewardModel_tE09D75F3B47938652450ABB51F21663745C8BC44), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8449[3] = 
{
	RewardModel_tE09D75F3B47938652450ABB51F21663745C8BC44::get_offset_of_Type_0(),
	RewardModel_tE09D75F3B47938652450ABB51F21663745C8BC44::get_offset_of_Value_1(),
	RewardModel_tE09D75F3B47938652450ABB51F21663745C8BC44::get_offset_of_Id_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8450 = { sizeof (CustomTutorialSO_t45FC033F62BB26BC5595EAA9FEF1DF50BFA2A41D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8450[1] = 
{
	CustomTutorialSO_t45FC033F62BB26BC5595EAA9FEF1DF50BFA2A41D::get_offset_of_CustomTutorials_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8451 = { sizeof (U3CU3Ec__DisplayClass1_0_t8A0EA66C8B605616AE4A6A5640B599178BA16E15), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8451[1] = 
{
	U3CU3Ec__DisplayClass1_0_t8A0EA66C8B605616AE4A6A5640B599178BA16E15::get_offset_of_type_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8452 = { sizeof (U3CU3Ec__DisplayClass2_0_tA198EA948F0DC0F03FB3D83644AAE8654B69EC47), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8452[1] = 
{
	U3CU3Ec__DisplayClass2_0_tA198EA948F0DC0F03FB3D83644AAE8654B69EC47::get_offset_of_type_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8453 = { sizeof (CustomTutorialModel_t7F1E6EF5B6AF58AEA9FB2573811B4562E587E635), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8453[5] = 
{
	CustomTutorialModel_t7F1E6EF5B6AF58AEA9FB2573811B4562E587E635::get_offset_of_Id_0(),
	CustomTutorialModel_t7F1E6EF5B6AF58AEA9FB2573811B4562E587E635::get_offset_of_Type_1(),
	CustomTutorialModel_t7F1E6EF5B6AF58AEA9FB2573811B4562E587E635::get_offset_of_Trigger_2(),
	CustomTutorialModel_t7F1E6EF5B6AF58AEA9FB2573811B4562E587E635::get_offset_of__startTigger_3(),
	CustomTutorialModel_t7F1E6EF5B6AF58AEA9FB2573811B4562E587E635::get_offset_of_Data_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8454 = { sizeof (CustomTutorialType_tB1BFA15E99AB96E461A01DDDBD92211896AE4968)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable8454[6] = 
{
	CustomTutorialType_tB1BFA15E99AB96E461A01DDDBD92211896AE4968::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8455 = { sizeof (DummyLeaderboardSO_t874BAA75E6FA341794950FA9B3141529607DDC08), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8455[2] = 
{
	DummyLeaderboardSO_t874BAA75E6FA341794950FA9B3141529607DDC08::get_offset_of_Players_0(),
	DummyLeaderboardSO_t874BAA75E6FA341794950FA9B3141529607DDC08::get_offset_of_Teams_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8456 = { sizeof (DummyLeaderboardModel_t86249198E504029DAC0693F7BD6CFB50B007058F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8456[3] = 
{
	DummyLeaderboardModel_t86249198E504029DAC0693F7BD6CFB50B007058F::get_offset_of_Name_0(),
	DummyLeaderboardModel_t86249198E504029DAC0693F7BD6CFB50B007058F::get_offset_of_Score_1(),
	DummyLeaderboardModel_t86249198E504029DAC0693F7BD6CFB50B007058F::get_offset_of_Icon_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8457 = { sizeof (EffectVisualSO_t1C5856B5E821FD66A4C1BD41DB5194A5610E6FFC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8457[1] = 
{
	EffectVisualSO_t1C5856B5E821FD66A4C1BD41DB5194A5610E6FFC::get_offset_of_Visuals_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8458 = { sizeof (U3CU3Ec__DisplayClass1_0_t7A7BD7F79E3AC1F6B21194D6A498110244521FC9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8458[1] = 
{
	U3CU3Ec__DisplayClass1_0_t7A7BD7F79E3AC1F6B21194D6A498110244521FC9::get_offset_of_id_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8459 = { sizeof (EffectVisualModel_t4E4CB43B096384FFC7DAC3E6DF1B2DF8DED965DF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8459[2] = 
{
	EffectVisualModel_t4E4CB43B096384FFC7DAC3E6DF1B2DF8DED965DF::get_offset_of_Id_0(),
	EffectVisualModel_t4E4CB43B096384FFC7DAC3E6DF1B2DF8DED965DF::get_offset_of_Visuals_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8460 = { sizeof (EffectVisual_tE885D61366B46DC715156887B773663B79D6777A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8460[4] = 
{
	EffectVisual_tE885D61366B46DC715156887B773663B79D6777A::get_offset_of_Prefab_0(),
	EffectVisual_tE885D61366B46DC715156887B773663B79D6777A::get_offset_of_Bundle_1(),
	EffectVisual_tE885D61366B46DC715156887B773663B79D6777A::get_offset_of_EffectVisualType_2(),
	EffectVisual_tE885D61366B46DC715156887B773663B79D6777A::get_offset_of_EffectVisualData_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8461 = { sizeof (EffectVisualType_t026EC7D3087FCA80C407ED9F3797CE097903D15A)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable8461[4] = 
{
	EffectVisualType_t026EC7D3087FCA80C407ED9F3797CE097903D15A::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8462 = { sizeof (FeaturesSwitchConfig_tBF1848651D45C05C78F36D68CB647F5CB2F802D3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8462[1] = 
{
	FeaturesSwitchConfig_tBF1848651D45C05C78F36D68CB647F5CB2F802D3::get_offset_of_TimePressureSystem_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8463 = { sizeof (GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8463[11] = 
{
	GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C::get_offset_of_LeaderboardUpdateTime_4(),
	GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C::get_offset_of_IconCount_5(),
	GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C::get_offset_of_InitCoins_6(),
	GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C::get_offset_of_InitHearts_7(),
	GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C::get_offset_of_MaxHearts_8(),
	GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C::get_offset_of_LiveRecoveryTimeInMinutes_9(),
	GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C::get_offset_of_FirstTimeJoinTeamReward_10(),
	GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C::get_offset_of_MinLevelToJoinTeam_11(),
	GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C::get_offset_of_SurveyUrl_12(),
	GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C::get_offset_of_GlobalTweenDuration_13(),
	GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C::get_offset_of_MinLeaderboardCount_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8464 = { sizeof (GlobalSO_tACFCA803E1D70E0299E50028D7799CE31F402273), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8464[2] = 
{
	GlobalSO_tACFCA803E1D70E0299E50028D7799CE31F402273::get_offset_of_ServerUrl_0(),
	GlobalSO_tACFCA803E1D70E0299E50028D7799CE31F402273::get_offset_of_Version_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8465 = { sizeof (ItemsSO_tD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8465[1] = 
{
	ItemsSO_tD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A::get_offset_of_Items_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8466 = { sizeof (U3CU3Ec__DisplayClass1_0_t1B0AB715C679927C205A4F2E17FCB64373E4C730), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8466[1] = 
{
	U3CU3Ec__DisplayClass1_0_t1B0AB715C679927C205A4F2E17FCB64373E4C730::get_offset_of_id_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8467 = { sizeof (ItemModel_t620A13B4D13CD275487D308843103864063DBE1F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8467[6] = 
{
	ItemModel_t620A13B4D13CD275487D308843103864063DBE1F::get_offset_of_Id_0(),
	ItemModel_t620A13B4D13CD275487D308843103864063DBE1F::get_offset_of_Bundle_1(),
	ItemModel_t620A13B4D13CD275487D308843103864063DBE1F::get_offset_of_Prefab_2(),
	ItemModel_t620A13B4D13CD275487D308843103864063DBE1F::get_offset_of_Icon_3(),
	ItemModel_t620A13B4D13CD275487D308843103864063DBE1F::get_offset_of_DeathExplosion_4(),
	ItemModel_t620A13B4D13CD275487D308843103864063DBE1F::get_offset_of_Type_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8468 = { sizeof (ItemType_t41090A4C4D2BCA3765F224EA9264EC817755A5A3)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable8468[4] = 
{
	ItemType_t41090A4C4D2BCA3765F224EA9264EC817755A5A3::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8469 = { sizeof (LevelsSO_t51232A976915E19D66106CE9DD068463ADBAD0EF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8469[1] = 
{
	LevelsSO_t51232A976915E19D66106CE9DD068463ADBAD0EF::get_offset_of_Levels_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8470 = { sizeof (LevelModel_t5549E807391907F7844D01A4FB4004330AF5E05C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8470[10] = 
{
	LevelModel_t5549E807391907F7844D01A4FB4004330AF5E05C::get_offset_of_Level_0(),
	LevelModel_t5549E807391907F7844D01A4FB4004330AF5E05C::get_offset_of_LevelBundle_1(),
	LevelModel_t5549E807391907F7844D01A4FB4004330AF5E05C::get_offset_of_Environment_2(),
	LevelModel_t5549E807391907F7844D01A4FB4004330AF5E05C::get_offset_of_EnvironmentBundle_3(),
	LevelModel_t5549E807391907F7844D01A4FB4004330AF5E05C::get_offset_of_ItemsCount_4(),
	LevelModel_t5549E807391907F7844D01A4FB4004330AF5E05C::get_offset_of_ItemsToGenerate_5(),
	LevelModel_t5549E807391907F7844D01A4FB4004330AF5E05C::get_offset_of_InventoryItems_6(),
	LevelModel_t5549E807391907F7844D01A4FB4004330AF5E05C::get_offset_of_Objectives_7(),
	LevelModel_t5549E807391907F7844D01A4FB4004330AF5E05C::get_offset_of_ItemsGenerationDelay_8(),
	LevelModel_t5549E807391907F7844D01A4FB4004330AF5E05C::get_offset_of_Duration_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8471 = { sizeof (LevelObjective_t694A34150000D3DE3C9341AAC6D6B37667B796EC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8471[3] = 
{
	LevelObjective_t694A34150000D3DE3C9341AAC6D6B37667B796EC::get_offset_of_Type_0(),
	LevelObjective_t694A34150000D3DE3C9341AAC6D6B37667B796EC::get_offset_of_UnitId_1(),
	LevelObjective_t694A34150000D3DE3C9341AAC6D6B37667B796EC::get_offset_of_Count_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8472 = { sizeof (ObjectiveType_tA6A9DA7F8810313B6E359E2D2B8B619E4E1BEE28)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable8472[5] = 
{
	ObjectiveType_tA6A9DA7F8810313B6E359E2D2B8B619E4E1BEE28::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8473 = { sizeof (ItemGenerationModel_tF18CD55B9CD457553363A05EF1A715F2619E1BA6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8473[2] = 
{
	ItemGenerationModel_tF18CD55B9CD457553363A05EF1A715F2619E1BA6::get_offset_of_Weight_0(),
	ItemGenerationModel_tF18CD55B9CD457553363A05EF1A715F2619E1BA6::get_offset_of_MinCount_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8474 = { sizeof (PerLevelTutorialSO_tB55F6372D80FCA75DD834538D5137169A7A7E66B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8474[1] = 
{
	PerLevelTutorialSO_tB55F6372D80FCA75DD834538D5137169A7A7E66B::get_offset_of_Tutorials_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8475 = { sizeof (U3CU3Ec__DisplayClass1_0_t9E186A929A5B89C2CF8480323CF4CCE355ED23F8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8475[1] = 
{
	U3CU3Ec__DisplayClass1_0_t9E186A929A5B89C2CF8480323CF4CCE355ED23F8::get_offset_of_id_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8476 = { sizeof (PerLevelTutorialModel_t617000CC336C47983E77A6E6171F0DE08379A61C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8476[4] = 
{
	PerLevelTutorialModel_t617000CC336C47983E77A6E6171F0DE08379A61C::get_offset_of_Id_0(),
	PerLevelTutorialModel_t617000CC336C47983E77A6E6171F0DE08379A61C::get_offset_of__startTigger_1(),
	PerLevelTutorialModel_t617000CC336C47983E77A6E6171F0DE08379A61C::get_offset_of_StartTriggerStr_2(),
	PerLevelTutorialModel_t617000CC336C47983E77A6E6171F0DE08379A61C::get_offset_of_Data_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8477 = { sizeof (TutorialType_t95A6CD09B8522505F42A6F3FFC2E01CCD692A302)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable8477[4] = 
{
	TutorialType_t95A6CD09B8522505F42A6F3FFC2E01CCD692A302::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8478 = { sizeof (SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8478[8] = 
{
	SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73::get_offset_of_BackgroundMain_4(),
	SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73::get_offset_of_BackgroundBattle_5(),
	SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73::get_offset_of_Button_6(),
	SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73::get_offset_of_SummaryWin_7(),
	SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73::get_offset_of_SummaryLose_8(),
	SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73::get_offset_of_Sword_9(),
	SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73::get_offset_of_Damage_10(),
	SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73::get_offset_of_DamageDelay_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8479 = { sizeof (SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8479[1] = 
{
	SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439::get_offset_of_Spells_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8480 = { sizeof (U3CU3Ec__DisplayClass1_0_tC85EADDF907753C4C6CAF2A466928E0776A9754D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8480[1] = 
{
	U3CU3Ec__DisplayClass1_0_tC85EADDF907753C4C6CAF2A466928E0776A9754D::get_offset_of_id_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8481 = { sizeof (SpellModel_t50B172E1768260E1736AA33F4ED72DB9CDC35235), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8481[7] = 
{
	SpellModel_t50B172E1768260E1736AA33F4ED72DB9CDC35235::get_offset_of_Id_0(),
	SpellModel_t50B172E1768260E1736AA33F4ED72DB9CDC35235::get_offset_of_SpellInput_1(),
	SpellModel_t50B172E1768260E1736AA33F4ED72DB9CDC35235::get_offset_of_Assets_2(),
	SpellModel_t50B172E1768260E1736AA33F4ED72DB9CDC35235::get_offset_of_SpellController_3(),
	SpellModel_t50B172E1768260E1736AA33F4ED72DB9CDC35235::get_offset_of_SpellControllerData_4(),
	SpellModel_t50B172E1768260E1736AA33F4ED72DB9CDC35235::get_offset_of_Effects_5(),
	SpellModel_t50B172E1768260E1736AA33F4ED72DB9CDC35235::get_offset_of_IsBooster_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8482 = { sizeof (DataDictionary_t9D4A0BE16CC0324BF2D47F35B50C65CC9B44E606), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8483 = { sizeof (AssetDictionary_tC29AFDE2D533D0F9B91BA34778BBF8605137D687), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8484 = { sizeof (EffectModel_t1BF2DAB91B75E4435889D8BBCCA583888883223E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8484[5] = 
{
	EffectModel_t1BF2DAB91B75E4435889D8BBCCA583888883223E::get_offset_of_EffectBrain_0(),
	EffectModel_t1BF2DAB91B75E4435889D8BBCCA583888883223E::get_offset_of_EffectBrainData_1(),
	EffectModel_t1BF2DAB91B75E4435889D8BBCCA583888883223E::get_offset_of_EffectType_2(),
	EffectModel_t1BF2DAB91B75E4435889D8BBCCA583888883223E::get_offset_of_EffectData_3(),
	EffectModel_t1BF2DAB91B75E4435889D8BBCCA583888883223E::get_offset_of_EffectVisualId_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8485 = { sizeof (AssetData_t155B7C20CE31B42FB840C26A65CBBE11D4D16D7A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8485[3] = 
{
	AssetData_t155B7C20CE31B42FB840C26A65CBBE11D4D16D7A::get_offset_of_Bundle_0(),
	AssetData_t155B7C20CE31B42FB840C26A65CBBE11D4D16D7A::get_offset_of_Prefab_1(),
	AssetData_t155B7C20CE31B42FB840C26A65CBBE11D4D16D7A::get_offset_of_WarmUp_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8486 = { sizeof (SpellInput_t7E941C18CACBD4A789B292019E2341A93AE8739F)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable8486[5] = 
{
	SpellInput_t7E941C18CACBD4A789B292019E2341A93AE8739F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8487 = { sizeof (SpellRunner_t504261A93EFC5915A80B9E13A0D004262B424C11)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable8487[3] = 
{
	SpellRunner_t504261A93EFC5915A80B9E13A0D004262B424C11::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8488 = { sizeof (SpellController_tF10DAA53E6D7502133083BF8AD162F9CBEE65643)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable8488[14] = 
{
	SpellController_tF10DAA53E6D7502133083BF8AD162F9CBEE65643::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8489 = { sizeof (EffectType_t001314E778DA097AFE0D47BF52477494AF02E0DF)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable8489[9] = 
{
	EffectType_t001314E778DA097AFE0D47BF52477494AF02E0DF::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8490 = { sizeof (TournamentRewardsSO_t9121EDF677AEDF239EF74868C27C0879178523E1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8490[1] = 
{
	TournamentRewardsSO_t9121EDF677AEDF239EF74868C27C0879178523E1::get_offset_of_Rewards_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8491 = { sizeof (TowersSO_t3B7DE821E5F8017950776EEFC42E680B361BCDF0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8491[1] = 
{
	TowersSO_t3B7DE821E5F8017950776EEFC42E680B361BCDF0::get_offset_of_Towers_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8492 = { sizeof (U3CU3Ec__DisplayClass1_0_tAE58CA8E50F9CCB21ED71CCCCA1E1EDF56A9F5CE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8492[1] = 
{
	U3CU3Ec__DisplayClass1_0_tAE58CA8E50F9CCB21ED71CCCCA1E1EDF56A9F5CE::get_offset_of_id_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8493 = { sizeof (TowerModel_t9DD2032CC7655923CBFCAC0EF0DF7B2D11683D31), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8493[11] = 
{
	TowerModel_t9DD2032CC7655923CBFCAC0EF0DF7B2D11683D31::get_offset_of_Id_0(),
	TowerModel_t9DD2032CC7655923CBFCAC0EF0DF7B2D11683D31::get_offset_of_Bundle_1(),
	TowerModel_t9DD2032CC7655923CBFCAC0EF0DF7B2D11683D31::get_offset_of_Prefab_2(),
	TowerModel_t9DD2032CC7655923CBFCAC0EF0DF7B2D11683D31::get_offset_of_Icon_3(),
	TowerModel_t9DD2032CC7655923CBFCAC0EF0DF7B2D11683D31::get_offset_of_SpellAI_4(),
	TowerModel_t9DD2032CC7655923CBFCAC0EF0DF7B2D11683D31::get_offset_of_AIData_5(),
	TowerModel_t9DD2032CC7655923CBFCAC0EF0DF7B2D11683D31::get_offset_of_Range_6(),
	TowerModel_t9DD2032CC7655923CBFCAC0EF0DF7B2D11683D31::get_offset_of_Spell_7(),
	TowerModel_t9DD2032CC7655923CBFCAC0EF0DF7B2D11683D31::get_offset_of_HP_8(),
	TowerModel_t9DD2032CC7655923CBFCAC0EF0DF7B2D11683D31::get_offset_of_DeathExplosion_9(),
	TowerModel_t9DD2032CC7655923CBFCAC0EF0DF7B2D11683D31::get_offset_of_HealDuration_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8494 = { sizeof (SpellAI_t31BA0E89C8EE4E13EC803218DD905CB4693C8366)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable8494[3] = 
{
	SpellAI_t31BA0E89C8EE4E13EC803218DD905CB4693C8366::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8495 = { sizeof (TrapsSO_tFF060852B3652DD05F33E1B19B8EC9166468D9C2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8495[1] = 
{
	TrapsSO_tFF060852B3652DD05F33E1B19B8EC9166468D9C2::get_offset_of_Traps_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8496 = { sizeof (U3CU3Ec__DisplayClass1_0_t04C535B8B5D127AD9A39CBB909D40690C6014560), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8496[1] = 
{
	U3CU3Ec__DisplayClass1_0_t04C535B8B5D127AD9A39CBB909D40690C6014560::get_offset_of_id_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8497 = { sizeof (TrapModel_tC99E2852889B4321B6754DFDDD005862CDC965EC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8497[9] = 
{
	TrapModel_tC99E2852889B4321B6754DFDDD005862CDC965EC::get_offset_of_Id_0(),
	TrapModel_tC99E2852889B4321B6754DFDDD005862CDC965EC::get_offset_of_Bundle_1(),
	TrapModel_tC99E2852889B4321B6754DFDDD005862CDC965EC::get_offset_of_Prefab_2(),
	TrapModel_tC99E2852889B4321B6754DFDDD005862CDC965EC::get_offset_of_Icon_3(),
	TrapModel_tC99E2852889B4321B6754DFDDD005862CDC965EC::get_offset_of_SpellAI_4(),
	TrapModel_tC99E2852889B4321B6754DFDDD005862CDC965EC::get_offset_of_AIData_5(),
	TrapModel_tC99E2852889B4321B6754DFDDD005862CDC965EC::get_offset_of_Range_6(),
	TrapModel_tC99E2852889B4321B6754DFDDD005862CDC965EC::get_offset_of_IsScalable_7(),
	TrapModel_tC99E2852889B4321B6754DFDDD005862CDC965EC::get_offset_of_Spell_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8498 = { sizeof (UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8498[1] = 
{
	UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423::get_offset_of_Units_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8499 = { sizeof (U3CU3Ec__DisplayClass1_0_tF59194C24E02C70B72472D026137782318AB9B1B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8499[1] = 
{
	U3CU3Ec__DisplayClass1_0_tF59194C24E02C70B72472D026137782318AB9B1B::get_offset_of_id_0(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
