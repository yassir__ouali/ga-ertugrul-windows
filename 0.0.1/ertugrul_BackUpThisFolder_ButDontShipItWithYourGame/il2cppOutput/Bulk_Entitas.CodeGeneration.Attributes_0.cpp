﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Entitas.CodeGeneration.Attributes.AbstractEntityIndexAttribute
struct AbstractEntityIndexAttribute_tC53F438F525D86D77BBFE5B0C9613E00E303F2EB;
// Entitas.CodeGeneration.Attributes.ContextAttribute
struct ContextAttribute_tCC46F2BA273A4F8FE981674FA22BA7D229AF6817;
// Entitas.CodeGeneration.Attributes.CustomEntityIndexAttribute
struct CustomEntityIndexAttribute_t514404ABF8B11B77892F7B3454E656EA019697FD;
// Entitas.CodeGeneration.Attributes.DontGenerateAttribute
struct DontGenerateAttribute_tFBFC6C730BAC7F0C3B13EFEC3D91BAC4DC04C857;
// Entitas.CodeGeneration.Attributes.EntityIndexAttribute
struct EntityIndexAttribute_t157B398E0AB74C4E344254DD6D1174D59286C75E;
// Entitas.CodeGeneration.Attributes.EntityIndexGetMethodAttribute
struct EntityIndexGetMethodAttribute_t948AD15E1610A40D1DBA5CB984A4C5260A79ADD6;
// Entitas.CodeGeneration.Attributes.EventAttribute
struct EventAttribute_t34E326AC98C5C26372A605464C75C3E6F7864C29;
// Entitas.CodeGeneration.Attributes.PostConstructorAttribute
struct PostConstructorAttribute_t4CEF1EC49C810198695BAA93D198FFE623EF447D;
// Entitas.CodeGeneration.Attributes.PrimaryEntityIndexAttribute
struct PrimaryEntityIndexAttribute_t650C506BD36D143AF4AC97DEAE82BFFB4933A12C;
// Entitas.CodeGeneration.Attributes.UniqueAttribute
struct UniqueAttribute_tA13A1C0849F610545E56A1E177C294CCCDDE8631;
// System.Attribute
struct Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Reflection.Binder
struct Binder_t4D5CB06963501D32847C057B57157D6DC49CA759;
// System.Reflection.MemberFilter
struct MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;




#ifndef U3CMODULEU3E_T45C880959CDBDE1C6E60B255EF64A1F2A2EE6A82_H
#define U3CMODULEU3E_T45C880959CDBDE1C6E60B255EF64A1F2A2EE6A82_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t45C880959CDBDE1C6E60B255EF64A1F2A2EE6A82 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T45C880959CDBDE1C6E60B255EF64A1F2A2EE6A82_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#define ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifndef MEMBERINFO_T_H
#define MEMBERINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERINFO_T_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef CONTEXTATTRIBUTE_TCC46F2BA273A4F8FE981674FA22BA7D229AF6817_H
#define CONTEXTATTRIBUTE_TCC46F2BA273A4F8FE981674FA22BA7D229AF6817_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.CodeGeneration.Attributes.ContextAttribute
struct  ContextAttribute_tCC46F2BA273A4F8FE981674FA22BA7D229AF6817  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String Entitas.CodeGeneration.Attributes.ContextAttribute::contextName
	String_t* ___contextName_0;

public:
	inline static int32_t get_offset_of_contextName_0() { return static_cast<int32_t>(offsetof(ContextAttribute_tCC46F2BA273A4F8FE981674FA22BA7D229AF6817, ___contextName_0)); }
	inline String_t* get_contextName_0() const { return ___contextName_0; }
	inline String_t** get_address_of_contextName_0() { return &___contextName_0; }
	inline void set_contextName_0(String_t* value)
	{
		___contextName_0 = value;
		Il2CppCodeGenWriteBarrier((&___contextName_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTEXTATTRIBUTE_TCC46F2BA273A4F8FE981674FA22BA7D229AF6817_H
#ifndef CUSTOMENTITYINDEXATTRIBUTE_T514404ABF8B11B77892F7B3454E656EA019697FD_H
#define CUSTOMENTITYINDEXATTRIBUTE_T514404ABF8B11B77892F7B3454E656EA019697FD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.CodeGeneration.Attributes.CustomEntityIndexAttribute
struct  CustomEntityIndexAttribute_t514404ABF8B11B77892F7B3454E656EA019697FD  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Type Entitas.CodeGeneration.Attributes.CustomEntityIndexAttribute::contextType
	Type_t * ___contextType_0;

public:
	inline static int32_t get_offset_of_contextType_0() { return static_cast<int32_t>(offsetof(CustomEntityIndexAttribute_t514404ABF8B11B77892F7B3454E656EA019697FD, ___contextType_0)); }
	inline Type_t * get_contextType_0() const { return ___contextType_0; }
	inline Type_t ** get_address_of_contextType_0() { return &___contextType_0; }
	inline void set_contextType_0(Type_t * value)
	{
		___contextType_0 = value;
		Il2CppCodeGenWriteBarrier((&___contextType_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMENTITYINDEXATTRIBUTE_T514404ABF8B11B77892F7B3454E656EA019697FD_H
#ifndef DONTGENERATEATTRIBUTE_TFBFC6C730BAC7F0C3B13EFEC3D91BAC4DC04C857_H
#define DONTGENERATEATTRIBUTE_TFBFC6C730BAC7F0C3B13EFEC3D91BAC4DC04C857_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.CodeGeneration.Attributes.DontGenerateAttribute
struct  DontGenerateAttribute_tFBFC6C730BAC7F0C3B13EFEC3D91BAC4DC04C857  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Boolean Entitas.CodeGeneration.Attributes.DontGenerateAttribute::generateIndex
	bool ___generateIndex_0;

public:
	inline static int32_t get_offset_of_generateIndex_0() { return static_cast<int32_t>(offsetof(DontGenerateAttribute_tFBFC6C730BAC7F0C3B13EFEC3D91BAC4DC04C857, ___generateIndex_0)); }
	inline bool get_generateIndex_0() const { return ___generateIndex_0; }
	inline bool* get_address_of_generateIndex_0() { return &___generateIndex_0; }
	inline void set_generateIndex_0(bool value)
	{
		___generateIndex_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DONTGENERATEATTRIBUTE_TFBFC6C730BAC7F0C3B13EFEC3D91BAC4DC04C857_H
#ifndef ENTITYINDEXGETMETHODATTRIBUTE_T948AD15E1610A40D1DBA5CB984A4C5260A79ADD6_H
#define ENTITYINDEXGETMETHODATTRIBUTE_T948AD15E1610A40D1DBA5CB984A4C5260A79ADD6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.CodeGeneration.Attributes.EntityIndexGetMethodAttribute
struct  EntityIndexGetMethodAttribute_t948AD15E1610A40D1DBA5CB984A4C5260A79ADD6  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTITYINDEXGETMETHODATTRIBUTE_T948AD15E1610A40D1DBA5CB984A4C5260A79ADD6_H
#ifndef POSTCONSTRUCTORATTRIBUTE_T4CEF1EC49C810198695BAA93D198FFE623EF447D_H
#define POSTCONSTRUCTORATTRIBUTE_T4CEF1EC49C810198695BAA93D198FFE623EF447D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.CodeGeneration.Attributes.PostConstructorAttribute
struct  PostConstructorAttribute_t4CEF1EC49C810198695BAA93D198FFE623EF447D  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTCONSTRUCTORATTRIBUTE_T4CEF1EC49C810198695BAA93D198FFE623EF447D_H
#ifndef UNIQUEATTRIBUTE_TA13A1C0849F610545E56A1E177C294CCCDDE8631_H
#define UNIQUEATTRIBUTE_TA13A1C0849F610545E56A1E177C294CCCDDE8631_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.CodeGeneration.Attributes.UniqueAttribute
struct  UniqueAttribute_tA13A1C0849F610545E56A1E177C294CCCDDE8631  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIQUEATTRIBUTE_TA13A1C0849F610545E56A1E177C294CCCDDE8631_H
#ifndef BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#define BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#define INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef ENTITYINDEXTYPE_T46D4684C925867D0CCA486B402E57664F71E6F8E_H
#define ENTITYINDEXTYPE_T46D4684C925867D0CCA486B402E57664F71E6F8E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.CodeGeneration.Attributes.EntityIndexType
struct  EntityIndexType_t46D4684C925867D0CCA486B402E57664F71E6F8E 
{
public:
	// System.Int32 Entitas.CodeGeneration.Attributes.EntityIndexType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EntityIndexType_t46D4684C925867D0CCA486B402E57664F71E6F8E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTITYINDEXTYPE_T46D4684C925867D0CCA486B402E57664F71E6F8E_H
#ifndef EVENTTARGET_T116BD7DA9A939B54477A9F429106EC67EB49A6C2_H
#define EVENTTARGET_T116BD7DA9A939B54477A9F429106EC67EB49A6C2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.CodeGeneration.Attributes.EventTarget
struct  EventTarget_t116BD7DA9A939B54477A9F429106EC67EB49A6C2 
{
public:
	// System.Int32 Entitas.CodeGeneration.Attributes.EventTarget::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EventTarget_t116BD7DA9A939B54477A9F429106EC67EB49A6C2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTTARGET_T116BD7DA9A939B54477A9F429106EC67EB49A6C2_H
#ifndef EVENTTYPE_T40A4EAA6A23F7288106E89EDE2610C86746758A3_H
#define EVENTTYPE_T40A4EAA6A23F7288106E89EDE2610C86746758A3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.CodeGeneration.Attributes.EventType
struct  EventType_t40A4EAA6A23F7288106E89EDE2610C86746758A3 
{
public:
	// System.Int32 Entitas.CodeGeneration.Attributes.EventType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EventType_t40A4EAA6A23F7288106E89EDE2610C86746758A3, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTTYPE_T40A4EAA6A23F7288106E89EDE2610C86746758A3_H
#ifndef BINDINGFLAGS_TE35C91D046E63A1B92BB9AB909FCF9DA84379ED0_H
#define BINDINGFLAGS_TE35C91D046E63A1B92BB9AB909FCF9DA84379ED0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_tE35C91D046E63A1B92BB9AB909FCF9DA84379ED0 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tE35C91D046E63A1B92BB9AB909FCF9DA84379ED0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_TE35C91D046E63A1B92BB9AB909FCF9DA84379ED0_H
#ifndef RUNTIMETYPEHANDLE_T7B542280A22F0EC4EAC2061C29178845847A8B2D_H
#define RUNTIMETYPEHANDLE_T7B542280A22F0EC4EAC2061C29178845847A8B2D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMETYPEHANDLE_T7B542280A22F0EC4EAC2061C29178845847A8B2D_H
#ifndef ABSTRACTENTITYINDEXATTRIBUTE_TC53F438F525D86D77BBFE5B0C9613E00E303F2EB_H
#define ABSTRACTENTITYINDEXATTRIBUTE_TC53F438F525D86D77BBFE5B0C9613E00E303F2EB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.CodeGeneration.Attributes.AbstractEntityIndexAttribute
struct  AbstractEntityIndexAttribute_tC53F438F525D86D77BBFE5B0C9613E00E303F2EB  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// Entitas.CodeGeneration.Attributes.EntityIndexType Entitas.CodeGeneration.Attributes.AbstractEntityIndexAttribute::entityIndexType
	int32_t ___entityIndexType_0;

public:
	inline static int32_t get_offset_of_entityIndexType_0() { return static_cast<int32_t>(offsetof(AbstractEntityIndexAttribute_tC53F438F525D86D77BBFE5B0C9613E00E303F2EB, ___entityIndexType_0)); }
	inline int32_t get_entityIndexType_0() const { return ___entityIndexType_0; }
	inline int32_t* get_address_of_entityIndexType_0() { return &___entityIndexType_0; }
	inline void set_entityIndexType_0(int32_t value)
	{
		___entityIndexType_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTENTITYINDEXATTRIBUTE_TC53F438F525D86D77BBFE5B0C9613E00E303F2EB_H
#ifndef EVENTATTRIBUTE_T34E326AC98C5C26372A605464C75C3E6F7864C29_H
#define EVENTATTRIBUTE_T34E326AC98C5C26372A605464C75C3E6F7864C29_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.CodeGeneration.Attributes.EventAttribute
struct  EventAttribute_t34E326AC98C5C26372A605464C75C3E6F7864C29  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// Entitas.CodeGeneration.Attributes.EventTarget Entitas.CodeGeneration.Attributes.EventAttribute::eventTarget
	int32_t ___eventTarget_0;
	// Entitas.CodeGeneration.Attributes.EventType Entitas.CodeGeneration.Attributes.EventAttribute::eventType
	int32_t ___eventType_1;
	// System.Int32 Entitas.CodeGeneration.Attributes.EventAttribute::priority
	int32_t ___priority_2;

public:
	inline static int32_t get_offset_of_eventTarget_0() { return static_cast<int32_t>(offsetof(EventAttribute_t34E326AC98C5C26372A605464C75C3E6F7864C29, ___eventTarget_0)); }
	inline int32_t get_eventTarget_0() const { return ___eventTarget_0; }
	inline int32_t* get_address_of_eventTarget_0() { return &___eventTarget_0; }
	inline void set_eventTarget_0(int32_t value)
	{
		___eventTarget_0 = value;
	}

	inline static int32_t get_offset_of_eventType_1() { return static_cast<int32_t>(offsetof(EventAttribute_t34E326AC98C5C26372A605464C75C3E6F7864C29, ___eventType_1)); }
	inline int32_t get_eventType_1() const { return ___eventType_1; }
	inline int32_t* get_address_of_eventType_1() { return &___eventType_1; }
	inline void set_eventType_1(int32_t value)
	{
		___eventType_1 = value;
	}

	inline static int32_t get_offset_of_priority_2() { return static_cast<int32_t>(offsetof(EventAttribute_t34E326AC98C5C26372A605464C75C3E6F7864C29, ___priority_2)); }
	inline int32_t get_priority_2() const { return ___priority_2; }
	inline int32_t* get_address_of_priority_2() { return &___priority_2; }
	inline void set_priority_2(int32_t value)
	{
		___priority_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTATTRIBUTE_T34E326AC98C5C26372A605464C75C3E6F7864C29_H
#ifndef TYPE_T_H
#define TYPE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((&___FilterAttribute_0), value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((&___FilterName_1), value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((&___FilterNameIgnoreCase_2), value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((&___Missing_3), value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTypes_5), value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((&___defaultBinder_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T_H
#ifndef ENTITYINDEXATTRIBUTE_T157B398E0AB74C4E344254DD6D1174D59286C75E_H
#define ENTITYINDEXATTRIBUTE_T157B398E0AB74C4E344254DD6D1174D59286C75E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.CodeGeneration.Attributes.EntityIndexAttribute
struct  EntityIndexAttribute_t157B398E0AB74C4E344254DD6D1174D59286C75E  : public AbstractEntityIndexAttribute_tC53F438F525D86D77BBFE5B0C9613E00E303F2EB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTITYINDEXATTRIBUTE_T157B398E0AB74C4E344254DD6D1174D59286C75E_H
#ifndef PRIMARYENTITYINDEXATTRIBUTE_T650C506BD36D143AF4AC97DEAE82BFFB4933A12C_H
#define PRIMARYENTITYINDEXATTRIBUTE_T650C506BD36D143AF4AC97DEAE82BFFB4933A12C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.CodeGeneration.Attributes.PrimaryEntityIndexAttribute
struct  PrimaryEntityIndexAttribute_t650C506BD36D143AF4AC97DEAE82BFFB4933A12C  : public AbstractEntityIndexAttribute_tC53F438F525D86D77BBFE5B0C9613E00E303F2EB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRIMARYENTITYINDEXATTRIBUTE_T650C506BD36D143AF4AC97DEAE82BFFB4933A12C_H



// System.Void System.Attribute::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0 (Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74 * __this, const RuntimeMethod* method);
// System.Void Entitas.CodeGeneration.Attributes.AbstractEntityIndexAttribute::.ctor(Entitas.CodeGeneration.Attributes.EntityIndexType)
extern "C" IL2CPP_METHOD_ATTR void AbstractEntityIndexAttribute__ctor_m417709634AC05E9721B18F3BC44695A3B32646D0 (AbstractEntityIndexAttribute_tC53F438F525D86D77BBFE5B0C9613E00E303F2EB * __this, int32_t ___entityIndexType0, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Entitas.CodeGeneration.Attributes.AbstractEntityIndexAttribute::.ctor(Entitas.CodeGeneration.Attributes.EntityIndexType)
extern "C" IL2CPP_METHOD_ATTR void AbstractEntityIndexAttribute__ctor_m417709634AC05E9721B18F3BC44695A3B32646D0 (AbstractEntityIndexAttribute_tC53F438F525D86D77BBFE5B0C9613E00E303F2EB * __this, int32_t ___entityIndexType0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___entityIndexType0;
		__this->set_entityIndexType_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Entitas.CodeGeneration.Attributes.ContextAttribute::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void ContextAttribute__ctor_m110F95CAB40D16F522FCA171E45971A68DC98669 (ContextAttribute_tCC46F2BA273A4F8FE981674FA22BA7D229AF6817 * __this, String_t* ___contextName0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___contextName0;
		__this->set_contextName_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Entitas.CodeGeneration.Attributes.CustomEntityIndexAttribute::.ctor(System.Type)
extern "C" IL2CPP_METHOD_ATTR void CustomEntityIndexAttribute__ctor_m8F646CE721D4B4310EEE25CBEFB5F3D3A1064F4F (CustomEntityIndexAttribute_t514404ABF8B11B77892F7B3454E656EA019697FD * __this, Type_t * ___contextType0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		Type_t * L_0 = ___contextType0;
		__this->set_contextType_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Entitas.CodeGeneration.Attributes.DontGenerateAttribute::.ctor(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void DontGenerateAttribute__ctor_mB2E2D665E4D062E6E80C5F3CF921AA3CD30FFCEF (DontGenerateAttribute_tFBFC6C730BAC7F0C3B13EFEC3D91BAC4DC04C857 * __this, bool ___generateIndex0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		bool L_0 = ___generateIndex0;
		__this->set_generateIndex_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Entitas.CodeGeneration.Attributes.EntityIndexAttribute::.ctor()
extern "C" IL2CPP_METHOD_ATTR void EntityIndexAttribute__ctor_m386E33009554D21667E6ACE881173C30EF9F6BEC (EntityIndexAttribute_t157B398E0AB74C4E344254DD6D1174D59286C75E * __this, const RuntimeMethod* method)
{
	{
		AbstractEntityIndexAttribute__ctor_m417709634AC05E9721B18F3BC44695A3B32646D0(__this, 0, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Entitas.CodeGeneration.Attributes.EntityIndexGetMethodAttribute::.ctor()
extern "C" IL2CPP_METHOD_ATTR void EntityIndexGetMethodAttribute__ctor_m9AF2976A29DFB8848C47AB6434CA5BE5B2528A09 (EntityIndexGetMethodAttribute_t948AD15E1610A40D1DBA5CB984A4C5260A79ADD6 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Entitas.CodeGeneration.Attributes.EventAttribute::.ctor(Entitas.CodeGeneration.Attributes.EventTarget,Entitas.CodeGeneration.Attributes.EventType,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void EventAttribute__ctor_mE9AFAC26DA9382A79B80EB508C16ADFD5AEE3720 (EventAttribute_t34E326AC98C5C26372A605464C75C3E6F7864C29 * __this, int32_t ___eventTarget0, int32_t ___eventType1, int32_t ___priority2, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___eventTarget0;
		__this->set_eventTarget_0(L_0);
		int32_t L_1 = ___eventType1;
		__this->set_eventType_1(L_1);
		int32_t L_2 = ___priority2;
		__this->set_priority_2(L_2);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Entitas.CodeGeneration.Attributes.PostConstructorAttribute::.ctor()
extern "C" IL2CPP_METHOD_ATTR void PostConstructorAttribute__ctor_mC4078F29C24BDBF172167250249A84E09ED6B552 (PostConstructorAttribute_t4CEF1EC49C810198695BAA93D198FFE623EF447D * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Entitas.CodeGeneration.Attributes.PrimaryEntityIndexAttribute::.ctor()
extern "C" IL2CPP_METHOD_ATTR void PrimaryEntityIndexAttribute__ctor_m236B2BE68B5A8D49BA38C111F4C64548FD3F025A (PrimaryEntityIndexAttribute_t650C506BD36D143AF4AC97DEAE82BFFB4933A12C * __this, const RuntimeMethod* method)
{
	{
		AbstractEntityIndexAttribute__ctor_m417709634AC05E9721B18F3BC44695A3B32646D0(__this, 1, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Entitas.CodeGeneration.Attributes.UniqueAttribute::.ctor()
extern "C" IL2CPP_METHOD_ATTR void UniqueAttribute__ctor_m9E0154DF527232F540A5B3FC07C1BF1EC8985CB3 (UniqueAttribute_tA13A1C0849F610545E56A1E177C294CCCDDE8631 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
