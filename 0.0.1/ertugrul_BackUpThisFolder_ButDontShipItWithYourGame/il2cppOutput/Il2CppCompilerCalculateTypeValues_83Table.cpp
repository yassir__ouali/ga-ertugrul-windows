﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// BattleSettingsSO
struct BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE;
// ChestGotchaComponent
struct ChestGotchaComponent_tA318577CE122D49B2C8F7D8D9BCD3F5E2BB0EA45;
// ChestModelSO
struct ChestModelSO_t78F97E6AC8F488AD3C18236C4080E78CCF29B2A2;
// ConsumeSpellEvent
struct ConsumeSpellEvent_t99A1EF82A5CC840F993D8568A4547414594DA516;
// Contexts
struct Contexts_t313FEE68C5FB0568E30785C33C0F6209F5F7B87D;
// Entitas.ICollector`1<GameEntity>
struct ICollector_1_tDA9721194F1438993CA7EC5D39C69B898FBBB26A;
// Entitas.IGroup`1<GameEntity>
struct IGroup_1_tF4940889845236B5907C50AD6A40AD6CE69EC680;
// GameContext
struct GameContext_t7DA531FCF38581666B3642E3E737E5B057494978;
// GameSettingsSO
struct GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C;
// GameSparks.Core.GSData
struct GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937;
// GotchaNodeData
struct GotchaNodeData_t7B74A837B128AFAF6F873873674CC2D974DB1EEB;
// InventorySystem
struct InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2;
// InventoryVO
struct InventoryVO_tC7F0305FC930782BD2E4A47E3BD0504F0DCC6AD9;
// KickerManager
struct KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA;
// LevelChestEvent
struct LevelChestEvent_tE4A44711C3740FE5BCA5CBEAE24289CB248D4429;
// LevelLostEvent
struct LevelLostEvent_t94C8D8FEA2B83A32F83B354B37B05B07139C1A48;
// LevelVariable
struct LevelVariable_t8E3E27789809A8DFA12D41DB3927AC1DC90D4279;
// LevelWonEvent
struct LevelWonEvent_t52CF9D205E167284E58802E830A1FD5897098944;
// NotificationSamples.GameNotificationsManager
struct GameNotificationsManager_tA91A2FAF80490C54B58C02C8B5652AFA290D6B9F;
// Quest
struct Quest_tBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021;
// ReviveCommandEvent
struct ReviveCommandEvent_t5CCE33B3F24E5C83F3E608F719C653E55CDE4470;
// SoundSO
struct SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73;
// SpellsSO
struct SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439;
// StarVariable
struct StarVariable_t5610FC328147BA512296EC37D396F70C4D050676;
// SubmitLevelCheat
struct SubmitLevelCheat_t77B4717B7CE62D7C292C127633F118AD5C13D22F;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.List`1<Entitas.ICleanupSystem>
struct List_1_t3E0F132FC1070967FBCD19B4BDF44B4F85CA03AC;
// System.Collections.Generic.List`1<Entitas.IExecuteSystem>
struct List_1_tE232269578E5E48549D25DD0C9823B612D968293;
// System.Collections.Generic.List`1<Entitas.IInitializeSystem>
struct List_1_t1E70CB97F04FCE20A9F01BCEBF8EB1C867FC348C;
// System.Collections.Generic.List`1<Entitas.ITearDownSystem>
struct List_1_t78F50ABA4688AD6A1255F66AB89EE8BAFD069913;
// System.Collections.Generic.List`1<GameEntity>
struct List_1_t37F4C7CF3D1202E4756C2E10D18F5A665A14FA29;
// System.Collections.Generic.List`1<ItemContentModel>
struct List_1_t2C6CF342B96E130723D45845A1FD2053CBAF0F27;
// System.Func`2<ItemContentModel,System.Int32>
struct Func_2_tC6F95DC88A67C082CA868928DE76AC3FC9906A40;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.String
struct String_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// TMPro.TextMeshProUGUI
struct TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438;
// Tayr.GameSparksPlatform
struct GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348;
// Tayr.ILibrary
struct ILibrary_tCBD3DE1F66AD92DAB1112B8018EBA4A4BD23767C;
// Tayr.INodeAnimationHandler
struct INodeAnimationHandler_t7A50F84EDDDD8CA150788E8B2A8D0F06E5ABF200;
// Tayr.NodeAnimator
struct NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F;
// Tayr.VOSaver
struct VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F;
// Tayr.VariableChangedEvent
struct VariableChangedEvent_t553579619C432FFE7C7636199CE57457FA699BA4;
// TournamentsVO
struct TournamentsVO_tCBA9A235AE4069263CE332B1E9150CDABD0E8016;
// TowersSO
struct TowersSO_t3B7DE821E5F8017950776EEFC42E680B361BCDF0;
// TrapsSO
struct TrapsSO_tFF060852B3652DD05F33E1B19B8EC9166468D9C2;
// UnitsSO
struct UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423;
// UnityEngine.Animator
struct Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.UI.Button
struct Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B;
// UnityEngine.UI.Image
struct Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E;
// UserEvents
struct UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7;
// UserVO
struct UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94;
// Zenject.DiContainer
struct DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef AIUTILS_TF258624F778C8B3EB6206D1106823C3A93CB7E3A_H
#define AIUTILS_TF258624F778C8B3EB6206D1106823C3A93CB7E3A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AIUtils
struct  AIUtils_tF258624F778C8B3EB6206D1106823C3A93CB7E3A  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AIUTILS_TF258624F778C8B3EB6206D1106823C3A93CB7E3A_H
#ifndef ADDSPELLSCHEATCOMMANDMODEL_T791A2D188E67AC49BD841E2A757671764EF2231D_H
#define ADDSPELLSCHEATCOMMANDMODEL_T791A2D188E67AC49BD841E2A757671764EF2231D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AddSpellsCheatCommandModel
struct  AddSpellsCheatCommandModel_t791A2D188E67AC49BD841E2A757671764EF2231D  : public RuntimeObject
{
public:
	// System.Int32 AddSpellsCheatCommandModel::Count
	int32_t ___Count_0;

public:
	inline static int32_t get_offset_of_Count_0() { return static_cast<int32_t>(offsetof(AddSpellsCheatCommandModel_t791A2D188E67AC49BD841E2A757671764EF2231D, ___Count_0)); }
	inline int32_t get_Count_0() const { return ___Count_0; }
	inline int32_t* get_address_of_Count_0() { return &___Count_0; }
	inline void set_Count_0(int32_t value)
	{
		___Count_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADDSPELLSCHEATCOMMANDMODEL_T791A2D188E67AC49BD841E2A757671764EF2231D_H
#ifndef ADDSPELLSCHEATCOMMANDRESULTMODEL_TBD0463F2503315AC84C891CB7060E403C5BB22B0_H
#define ADDSPELLSCHEATCOMMANDRESULTMODEL_TBD0463F2503315AC84C891CB7060E403C5BB22B0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AddSpellsCheatCommandResultModel
struct  AddSpellsCheatCommandResultModel_tBD0463F2503315AC84C891CB7060E403C5BB22B0  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADDSPELLSCHEATCOMMANDRESULTMODEL_TBD0463F2503315AC84C891CB7060E403C5BB22B0_H
#ifndef ANIMATORTRIGGERCOMPONENT_TDC969D009A6F3EA063E87BA74E26FC7DB024F149_H
#define ANIMATORTRIGGERCOMPONENT_TDC969D009A6F3EA063E87BA74E26FC7DB024F149_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AnimatorTriggerComponent
struct  AnimatorTriggerComponent_tDC969D009A6F3EA063E87BA74E26FC7DB024F149  : public RuntimeObject
{
public:
	// System.String AnimatorTriggerComponent::TargetId
	String_t* ___TargetId_0;
	// System.String AnimatorTriggerComponent::TriggerName
	String_t* ___TriggerName_1;

public:
	inline static int32_t get_offset_of_TargetId_0() { return static_cast<int32_t>(offsetof(AnimatorTriggerComponent_tDC969D009A6F3EA063E87BA74E26FC7DB024F149, ___TargetId_0)); }
	inline String_t* get_TargetId_0() const { return ___TargetId_0; }
	inline String_t** get_address_of_TargetId_0() { return &___TargetId_0; }
	inline void set_TargetId_0(String_t* value)
	{
		___TargetId_0 = value;
		Il2CppCodeGenWriteBarrier((&___TargetId_0), value);
	}

	inline static int32_t get_offset_of_TriggerName_1() { return static_cast<int32_t>(offsetof(AnimatorTriggerComponent_tDC969D009A6F3EA063E87BA74E26FC7DB024F149, ___TriggerName_1)); }
	inline String_t* get_TriggerName_1() const { return ___TriggerName_1; }
	inline String_t** get_address_of_TriggerName_1() { return &___TriggerName_1; }
	inline void set_TriggerName_1(String_t* value)
	{
		___TriggerName_1 = value;
		Il2CppCodeGenWriteBarrier((&___TriggerName_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATORTRIGGERCOMPONENT_TDC969D009A6F3EA063E87BA74E26FC7DB024F149_H
#ifndef AOECOMPONENT_TF428CE243F66E1239169CEBB7B3058DF28C85809_H
#define AOECOMPONENT_TF428CE243F66E1239169CEBB7B3058DF28C85809_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AoeComponent
struct  AoeComponent_tF428CE243F66E1239169CEBB7B3058DF28C85809  : public RuntimeObject
{
public:
	// System.Single AoeComponent::Radius
	float ___Radius_0;

public:
	inline static int32_t get_offset_of_Radius_0() { return static_cast<int32_t>(offsetof(AoeComponent_tF428CE243F66E1239169CEBB7B3058DF28C85809, ___Radius_0)); }
	inline float get_Radius_0() const { return ___Radius_0; }
	inline float* get_address_of_Radius_0() { return &___Radius_0; }
	inline void set_Radius_0(float value)
	{
		___Radius_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AOECOMPONENT_TF428CE243F66E1239169CEBB7B3058DF28C85809_H
#ifndef BATTLECOMMANDUTILS_T81B76E8E86DEDEFFDC1103E10D3C8650921E84DC_H
#define BATTLECOMMANDUTILS_T81B76E8E86DEDEFFDC1103E10D3C8650921E84DC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BattleCommandUtils
struct  BattleCommandUtils_t81B76E8E86DEDEFFDC1103E10D3C8650921E84DC  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BATTLECOMMANDUTILS_T81B76E8E86DEDEFFDC1103E10D3C8650921E84DC_H
#ifndef BATTLETIMECOMPONENT_TEDFAA2735C49A6FA764F8DBEEE56EDF39A6E11EE_H
#define BATTLETIMECOMPONENT_TEDFAA2735C49A6FA764F8DBEEE56EDF39A6E11EE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BattleTimeComponent
struct  BattleTimeComponent_tEDFAA2735C49A6FA764F8DBEEE56EDF39A6E11EE  : public RuntimeObject
{
public:
	// System.Single BattleTimeComponent::Time
	float ___Time_0;

public:
	inline static int32_t get_offset_of_Time_0() { return static_cast<int32_t>(offsetof(BattleTimeComponent_tEDFAA2735C49A6FA764F8DBEEE56EDF39A6E11EE, ___Time_0)); }
	inline float get_Time_0() const { return ___Time_0; }
	inline float* get_address_of_Time_0() { return &___Time_0; }
	inline void set_Time_0(float value)
	{
		___Time_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BATTLETIMECOMPONENT_TEDFAA2735C49A6FA764F8DBEEE56EDF39A6E11EE_H
#ifndef BATTLETIMESYSTEM_T3C3EA048C7A829A065CFC3F2EB900EA617549C3F_H
#define BATTLETIMESYSTEM_T3C3EA048C7A829A065CFC3F2EB900EA617549C3F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BattleTimeSystem
struct  BattleTimeSystem_t3C3EA048C7A829A065CFC3F2EB900EA617549C3F  : public RuntimeObject
{
public:
	// GameContext BattleTimeSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_0;

public:
	inline static int32_t get_offset_of__context_0() { return static_cast<int32_t>(offsetof(BattleTimeSystem_t3C3EA048C7A829A065CFC3F2EB900EA617549C3F, ____context_0)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_0() const { return ____context_0; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_0() { return &____context_0; }
	inline void set__context_0(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_0 = value;
		Il2CppCodeGenWriteBarrier((&____context_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BATTLETIMESYSTEM_T3C3EA048C7A829A065CFC3F2EB900EA617549C3F_H
#ifndef BLOCKINGSPELL_TEC8ACE0D193BB3B7997CFE67A30BB0F454CB6007_H
#define BLOCKINGSPELL_TEC8ACE0D193BB3B7997CFE67A30BB0F454CB6007_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BlockingSpell
struct  BlockingSpell_tEC8ACE0D193BB3B7997CFE67A30BB0F454CB6007  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLOCKINGSPELL_TEC8ACE0D193BB3B7997CFE67A30BB0F454CB6007_H
#ifndef CHAPTERSUTILS_TCB388F0B35B67640D1813E4EDD9D3910634EF833_H
#define CHAPTERSUTILS_TCB388F0B35B67640D1813E4EDD9D3910634EF833_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChaptersUtils
struct  ChaptersUtils_tCB388F0B35B67640D1813E4EDD9D3910634EF833  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHAPTERSUTILS_TCB388F0B35B67640D1813E4EDD9D3910634EF833_H
#ifndef CHEATSDICTIONARY_TAE0AD2EE44BAE0EDDF8737127DFFB63BE8F0357B_H
#define CHEATSDICTIONARY_TAE0AD2EE44BAE0EDDF8737127DFFB63BE8F0357B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CheatsDictionary
struct  CheatsDictionary_tAE0AD2EE44BAE0EDDF8737127DFFB63BE8F0357B  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHEATSDICTIONARY_TAE0AD2EE44BAE0EDDF8737127DFFB63BE8F0357B_H
#ifndef CHESTSUTILS_T025BA5FD13B87313CCA5F605FE7066399F0443C8_H
#define CHESTSUTILS_T025BA5FD13B87313CCA5F605FE7066399F0443C8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChestsUtils
struct  ChestsUtils_t025BA5FD13B87313CCA5F605FE7066399F0443C8  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHESTSUTILS_T025BA5FD13B87313CCA5F605FE7066399F0443C8_H
#ifndef U3CU3EC_TCF9FE9503A64A6801962BD22B858BEA8C612E5A7_H
#define U3CU3EC_TCF9FE9503A64A6801962BD22B858BEA8C612E5A7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChestsUtils_<>c
struct  U3CU3Ec_tCF9FE9503A64A6801962BD22B858BEA8C612E5A7  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tCF9FE9503A64A6801962BD22B858BEA8C612E5A7_StaticFields
{
public:
	// ChestsUtils_<>c ChestsUtils_<>c::<>9
	U3CU3Ec_tCF9FE9503A64A6801962BD22B858BEA8C612E5A7 * ___U3CU3E9_0;
	// System.Func`2<ItemContentModel,System.Int32> ChestsUtils_<>c::<>9__1_0
	Func_2_tC6F95DC88A67C082CA868928DE76AC3FC9906A40 * ___U3CU3E9__1_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tCF9FE9503A64A6801962BD22B858BEA8C612E5A7_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tCF9FE9503A64A6801962BD22B858BEA8C612E5A7 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tCF9FE9503A64A6801962BD22B858BEA8C612E5A7 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tCF9FE9503A64A6801962BD22B858BEA8C612E5A7 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__1_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tCF9FE9503A64A6801962BD22B858BEA8C612E5A7_StaticFields, ___U3CU3E9__1_0_1)); }
	inline Func_2_tC6F95DC88A67C082CA868928DE76AC3FC9906A40 * get_U3CU3E9__1_0_1() const { return ___U3CU3E9__1_0_1; }
	inline Func_2_tC6F95DC88A67C082CA868928DE76AC3FC9906A40 ** get_address_of_U3CU3E9__1_0_1() { return &___U3CU3E9__1_0_1; }
	inline void set_U3CU3E9__1_0_1(Func_2_tC6F95DC88A67C082CA868928DE76AC3FC9906A40 * value)
	{
		___U3CU3E9__1_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__1_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TCF9FE9503A64A6801962BD22B858BEA8C612E5A7_H
#ifndef CLEARDATACHEAT_T9A397895E8DC547509E94B030C5B57BC814FA254_H
#define CLEARDATACHEAT_T9A397895E8DC547509E94B030C5B57BC814FA254_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ClearDataCheat
struct  ClearDataCheat_t9A397895E8DC547509E94B030C5B57BC814FA254  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLEARDATACHEAT_T9A397895E8DC547509E94B030C5B57BC814FA254_H
#ifndef COMMANDCHEATS_TA39912D252DB4C3A6B45F65FF565794793DA1037_H
#define COMMANDCHEATS_TA39912D252DB4C3A6B45F65FF565794793DA1037_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CommandCheats
struct  CommandCheats_tA39912D252DB4C3A6B45F65FF565794793DA1037  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMMANDCHEATS_TA39912D252DB4C3A6B45F65FF565794793DA1037_H
#ifndef COMPANIONCOMPONENT_T25D4800A96B514082EF457F3C3CD6DC54A49CB57_H
#define COMPANIONCOMPONENT_T25D4800A96B514082EF457F3C3CD6DC54A49CB57_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CompanionComponent
struct  CompanionComponent_t25D4800A96B514082EF457F3C3CD6DC54A49CB57  : public RuntimeObject
{
public:
	// System.String CompanionComponent::CompanionId
	String_t* ___CompanionId_0;

public:
	inline static int32_t get_offset_of_CompanionId_0() { return static_cast<int32_t>(offsetof(CompanionComponent_t25D4800A96B514082EF457F3C3CD6DC54A49CB57, ___CompanionId_0)); }
	inline String_t* get_CompanionId_0() const { return ___CompanionId_0; }
	inline String_t** get_address_of_CompanionId_0() { return &___CompanionId_0; }
	inline void set_CompanionId_0(String_t* value)
	{
		___CompanionId_0 = value;
		Il2CppCodeGenWriteBarrier((&___CompanionId_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPANIONCOMPONENT_T25D4800A96B514082EF457F3C3CD6DC54A49CB57_H
#ifndef CONFCHEATS_T8778C80FBE0D5B00BDF12A4B0CB263D6D0C60D7B_H
#define CONFCHEATS_T8778C80FBE0D5B00BDF12A4B0CB263D6D0C60D7B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ConfCheats
struct  ConfCheats_t8778C80FBE0D5B00BDF12A4B0CB263D6D0C60D7B  : public RuntimeObject
{
public:
	// Zenject.DiContainer ConfCheats::_diContainer
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____diContainer_0;

public:
	inline static int32_t get_offset_of__diContainer_0() { return static_cast<int32_t>(offsetof(ConfCheats_t8778C80FBE0D5B00BDF12A4B0CB263D6D0C60D7B, ____diContainer_0)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__diContainer_0() const { return ____diContainer_0; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__diContainer_0() { return &____diContainer_0; }
	inline void set__diContainer_0(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____diContainer_0 = value;
		Il2CppCodeGenWriteBarrier((&____diContainer_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFCHEATS_T8778C80FBE0D5B00BDF12A4B0CB263D6D0C60D7B_H
#ifndef CONSUMESPELLCOMMANDMODEL_T0D5A73862A452D6C88F74A66703AE278AA372D9A_H
#define CONSUMESPELLCOMMANDMODEL_T0D5A73862A452D6C88F74A66703AE278AA372D9A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ConsumeSpellCommandModel
struct  ConsumeSpellCommandModel_t0D5A73862A452D6C88F74A66703AE278AA372D9A  : public RuntimeObject
{
public:
	// System.String ConsumeSpellCommandModel::SpellId
	String_t* ___SpellId_0;

public:
	inline static int32_t get_offset_of_SpellId_0() { return static_cast<int32_t>(offsetof(ConsumeSpellCommandModel_t0D5A73862A452D6C88F74A66703AE278AA372D9A, ___SpellId_0)); }
	inline String_t* get_SpellId_0() const { return ___SpellId_0; }
	inline String_t** get_address_of_SpellId_0() { return &___SpellId_0; }
	inline void set_SpellId_0(String_t* value)
	{
		___SpellId_0 = value;
		Il2CppCodeGenWriteBarrier((&___SpellId_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSUMESPELLCOMMANDMODEL_T0D5A73862A452D6C88F74A66703AE278AA372D9A_H
#ifndef CONSUMESPELLCOMMANDRESULTMODEL_T67C78E5BE640F4619BDECA9482ECA9E947D7E27A_H
#define CONSUMESPELLCOMMANDRESULTMODEL_T67C78E5BE640F4619BDECA9482ECA9E947D7E27A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ConsumeSpellCommandResultModel
struct  ConsumeSpellCommandResultModel_t67C78E5BE640F4619BDECA9482ECA9E947D7E27A  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSUMESPELLCOMMANDRESULTMODEL_T67C78E5BE640F4619BDECA9482ECA9E947D7E27A_H
#ifndef CREATETRAPAFTERTIMESYSTEM_TA798566C21ADB31B4D0CC794B25A3E085C93B90D_H
#define CREATETRAPAFTERTIMESYSTEM_TA798566C21ADB31B4D0CC794B25A3E085C93B90D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CreateTrapAfterTimeSystem
struct  CreateTrapAfterTimeSystem_tA798566C21ADB31B4D0CC794B25A3E085C93B90D  : public RuntimeObject
{
public:
	// GameContext CreateTrapAfterTimeSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_0;
	// Entitas.IGroup`1<GameEntity> CreateTrapAfterTimeSystem::_group
	RuntimeObject* ____group_1;

public:
	inline static int32_t get_offset_of__context_0() { return static_cast<int32_t>(offsetof(CreateTrapAfterTimeSystem_tA798566C21ADB31B4D0CC794B25A3E085C93B90D, ____context_0)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_0() const { return ____context_0; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_0() { return &____context_0; }
	inline void set__context_0(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_0 = value;
		Il2CppCodeGenWriteBarrier((&____context_0), value);
	}

	inline static int32_t get_offset_of__group_1() { return static_cast<int32_t>(offsetof(CreateTrapAfterTimeSystem_tA798566C21ADB31B4D0CC794B25A3E085C93B90D, ____group_1)); }
	inline RuntimeObject* get__group_1() const { return ____group_1; }
	inline RuntimeObject** get_address_of__group_1() { return &____group_1; }
	inline void set__group_1(RuntimeObject* value)
	{
		____group_1 = value;
		Il2CppCodeGenWriteBarrier((&____group_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CREATETRAPAFTERTIMESYSTEM_TA798566C21ADB31B4D0CC794B25A3E085C93B90D_H
#ifndef DECOYCOMPONENT_TF6B0B470DD347730957875051E3D5670D3839070_H
#define DECOYCOMPONENT_TF6B0B470DD347730957875051E3D5670D3839070_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DecoyComponent
struct  DecoyComponent_tF6B0B470DD347730957875051E3D5670D3839070  : public RuntimeObject
{
public:
	// System.String DecoyComponent::DecoyBundle
	String_t* ___DecoyBundle_0;
	// System.String DecoyComponent::DecoyPrefab
	String_t* ___DecoyPrefab_1;

public:
	inline static int32_t get_offset_of_DecoyBundle_0() { return static_cast<int32_t>(offsetof(DecoyComponent_tF6B0B470DD347730957875051E3D5670D3839070, ___DecoyBundle_0)); }
	inline String_t* get_DecoyBundle_0() const { return ___DecoyBundle_0; }
	inline String_t** get_address_of_DecoyBundle_0() { return &___DecoyBundle_0; }
	inline void set_DecoyBundle_0(String_t* value)
	{
		___DecoyBundle_0 = value;
		Il2CppCodeGenWriteBarrier((&___DecoyBundle_0), value);
	}

	inline static int32_t get_offset_of_DecoyPrefab_1() { return static_cast<int32_t>(offsetof(DecoyComponent_tF6B0B470DD347730957875051E3D5670D3839070, ___DecoyPrefab_1)); }
	inline String_t* get_DecoyPrefab_1() const { return ___DecoyPrefab_1; }
	inline String_t** get_address_of_DecoyPrefab_1() { return &___DecoyPrefab_1; }
	inline void set_DecoyPrefab_1(String_t* value)
	{
		___DecoyPrefab_1 = value;
		Il2CppCodeGenWriteBarrier((&___DecoyPrefab_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECOYCOMPONENT_TF6B0B470DD347730957875051E3D5670D3839070_H
#ifndef DEFENSEIDCOMPONENT_T273FEC2B955E7A431924573AEDD1622BFB4AB9D5_H
#define DEFENSEIDCOMPONENT_T273FEC2B955E7A431924573AEDD1622BFB4AB9D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DefenseIdComponent
struct  DefenseIdComponent_t273FEC2B955E7A431924573AEDD1622BFB4AB9D5  : public RuntimeObject
{
public:
	// System.String DefenseIdComponent::DefenseId
	String_t* ___DefenseId_0;

public:
	inline static int32_t get_offset_of_DefenseId_0() { return static_cast<int32_t>(offsetof(DefenseIdComponent_t273FEC2B955E7A431924573AEDD1622BFB4AB9D5, ___DefenseId_0)); }
	inline String_t* get_DefenseId_0() const { return ___DefenseId_0; }
	inline String_t** get_address_of_DefenseId_0() { return &___DefenseId_0; }
	inline void set_DefenseId_0(String_t* value)
	{
		___DefenseId_0 = value;
		Il2CppCodeGenWriteBarrier((&___DefenseId_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFENSEIDCOMPONENT_T273FEC2B955E7A431924573AEDD1622BFB4AB9D5_H
#ifndef DEFENSESPELLCOMPONENT_TC6D1C273AF1ACE089CFA2FFCFBBB896DDE348936_H
#define DEFENSESPELLCOMPONENT_TC6D1C273AF1ACE089CFA2FFCFBBB896DDE348936_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DefenseSpellComponent
struct  DefenseSpellComponent_tC6D1C273AF1ACE089CFA2FFCFBBB896DDE348936  : public RuntimeObject
{
public:
	// System.String DefenseSpellComponent::SpellId
	String_t* ___SpellId_0;

public:
	inline static int32_t get_offset_of_SpellId_0() { return static_cast<int32_t>(offsetof(DefenseSpellComponent_tC6D1C273AF1ACE089CFA2FFCFBBB896DDE348936, ___SpellId_0)); }
	inline String_t* get_SpellId_0() const { return ___SpellId_0; }
	inline String_t** get_address_of_SpellId_0() { return &___SpellId_0; }
	inline void set_SpellId_0(String_t* value)
	{
		___SpellId_0 = value;
		Il2CppCodeGenWriteBarrier((&___SpellId_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFENSESPELLCOMPONENT_TC6D1C273AF1ACE089CFA2FFCFBBB896DDE348936_H
#ifndef DISPLAYNAMECHEAT_TA9CF3AD1FAD29502051B6B253F45071962C38EEE_H
#define DISPLAYNAMECHEAT_TA9CF3AD1FAD29502051B6B253F45071962C38EEE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DisplayNameCheat
struct  DisplayNameCheat_tA9CF3AD1FAD29502051B6B253F45071962C38EEE  : public RuntimeObject
{
public:
	// Tayr.GameSparksPlatform DisplayNameCheat::_gameSpark
	GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * ____gameSpark_0;

public:
	inline static int32_t get_offset_of__gameSpark_0() { return static_cast<int32_t>(offsetof(DisplayNameCheat_tA9CF3AD1FAD29502051B6B253F45071962C38EEE, ____gameSpark_0)); }
	inline GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * get__gameSpark_0() const { return ____gameSpark_0; }
	inline GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 ** get_address_of__gameSpark_0() { return &____gameSpark_0; }
	inline void set__gameSpark_0(GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * value)
	{
		____gameSpark_0 = value;
		Il2CppCodeGenWriteBarrier((&____gameSpark_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISPLAYNAMECHEAT_TA9CF3AD1FAD29502051B6B253F45071962C38EEE_H
#ifndef REACTIVESYSTEM_1_TD4874D09441436F0E3DB74FACC6C25A1F78AE0F9_H
#define REACTIVESYSTEM_1_TD4874D09441436F0E3DB74FACC6C25A1F78AE0F9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.ReactiveSystem`1<GameEntity>
struct  ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9  : public RuntimeObject
{
public:
	// Entitas.ICollector`1<TEntity> Entitas.ReactiveSystem`1::_collector
	RuntimeObject* ____collector_0;
	// System.Collections.Generic.List`1<TEntity> Entitas.ReactiveSystem`1::_buffer
	List_1_t37F4C7CF3D1202E4756C2E10D18F5A665A14FA29 * ____buffer_1;
	// System.String Entitas.ReactiveSystem`1::_toStringCache
	String_t* ____toStringCache_2;

public:
	inline static int32_t get_offset_of__collector_0() { return static_cast<int32_t>(offsetof(ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9, ____collector_0)); }
	inline RuntimeObject* get__collector_0() const { return ____collector_0; }
	inline RuntimeObject** get_address_of__collector_0() { return &____collector_0; }
	inline void set__collector_0(RuntimeObject* value)
	{
		____collector_0 = value;
		Il2CppCodeGenWriteBarrier((&____collector_0), value);
	}

	inline static int32_t get_offset_of__buffer_1() { return static_cast<int32_t>(offsetof(ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9, ____buffer_1)); }
	inline List_1_t37F4C7CF3D1202E4756C2E10D18F5A665A14FA29 * get__buffer_1() const { return ____buffer_1; }
	inline List_1_t37F4C7CF3D1202E4756C2E10D18F5A665A14FA29 ** get_address_of__buffer_1() { return &____buffer_1; }
	inline void set__buffer_1(List_1_t37F4C7CF3D1202E4756C2E10D18F5A665A14FA29 * value)
	{
		____buffer_1 = value;
		Il2CppCodeGenWriteBarrier((&____buffer_1), value);
	}

	inline static int32_t get_offset_of__toStringCache_2() { return static_cast<int32_t>(offsetof(ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9, ____toStringCache_2)); }
	inline String_t* get__toStringCache_2() const { return ____toStringCache_2; }
	inline String_t** get_address_of__toStringCache_2() { return &____toStringCache_2; }
	inline void set__toStringCache_2(String_t* value)
	{
		____toStringCache_2 = value;
		Il2CppCodeGenWriteBarrier((&____toStringCache_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REACTIVESYSTEM_1_TD4874D09441436F0E3DB74FACC6C25A1F78AE0F9_H
#ifndef SYSTEMS_T4C86DA84B6D6F50FF23B497DD599ED594274DEA9_H
#define SYSTEMS_T4C86DA84B6D6F50FF23B497DD599ED594274DEA9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.Systems
struct  Systems_t4C86DA84B6D6F50FF23B497DD599ED594274DEA9  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Entitas.IInitializeSystem> Entitas.Systems::_initializeSystems
	List_1_t1E70CB97F04FCE20A9F01BCEBF8EB1C867FC348C * ____initializeSystems_0;
	// System.Collections.Generic.List`1<Entitas.IExecuteSystem> Entitas.Systems::_executeSystems
	List_1_tE232269578E5E48549D25DD0C9823B612D968293 * ____executeSystems_1;
	// System.Collections.Generic.List`1<Entitas.ICleanupSystem> Entitas.Systems::_cleanupSystems
	List_1_t3E0F132FC1070967FBCD19B4BDF44B4F85CA03AC * ____cleanupSystems_2;
	// System.Collections.Generic.List`1<Entitas.ITearDownSystem> Entitas.Systems::_tearDownSystems
	List_1_t78F50ABA4688AD6A1255F66AB89EE8BAFD069913 * ____tearDownSystems_3;

public:
	inline static int32_t get_offset_of__initializeSystems_0() { return static_cast<int32_t>(offsetof(Systems_t4C86DA84B6D6F50FF23B497DD599ED594274DEA9, ____initializeSystems_0)); }
	inline List_1_t1E70CB97F04FCE20A9F01BCEBF8EB1C867FC348C * get__initializeSystems_0() const { return ____initializeSystems_0; }
	inline List_1_t1E70CB97F04FCE20A9F01BCEBF8EB1C867FC348C ** get_address_of__initializeSystems_0() { return &____initializeSystems_0; }
	inline void set__initializeSystems_0(List_1_t1E70CB97F04FCE20A9F01BCEBF8EB1C867FC348C * value)
	{
		____initializeSystems_0 = value;
		Il2CppCodeGenWriteBarrier((&____initializeSystems_0), value);
	}

	inline static int32_t get_offset_of__executeSystems_1() { return static_cast<int32_t>(offsetof(Systems_t4C86DA84B6D6F50FF23B497DD599ED594274DEA9, ____executeSystems_1)); }
	inline List_1_tE232269578E5E48549D25DD0C9823B612D968293 * get__executeSystems_1() const { return ____executeSystems_1; }
	inline List_1_tE232269578E5E48549D25DD0C9823B612D968293 ** get_address_of__executeSystems_1() { return &____executeSystems_1; }
	inline void set__executeSystems_1(List_1_tE232269578E5E48549D25DD0C9823B612D968293 * value)
	{
		____executeSystems_1 = value;
		Il2CppCodeGenWriteBarrier((&____executeSystems_1), value);
	}

	inline static int32_t get_offset_of__cleanupSystems_2() { return static_cast<int32_t>(offsetof(Systems_t4C86DA84B6D6F50FF23B497DD599ED594274DEA9, ____cleanupSystems_2)); }
	inline List_1_t3E0F132FC1070967FBCD19B4BDF44B4F85CA03AC * get__cleanupSystems_2() const { return ____cleanupSystems_2; }
	inline List_1_t3E0F132FC1070967FBCD19B4BDF44B4F85CA03AC ** get_address_of__cleanupSystems_2() { return &____cleanupSystems_2; }
	inline void set__cleanupSystems_2(List_1_t3E0F132FC1070967FBCD19B4BDF44B4F85CA03AC * value)
	{
		____cleanupSystems_2 = value;
		Il2CppCodeGenWriteBarrier((&____cleanupSystems_2), value);
	}

	inline static int32_t get_offset_of__tearDownSystems_3() { return static_cast<int32_t>(offsetof(Systems_t4C86DA84B6D6F50FF23B497DD599ED594274DEA9, ____tearDownSystems_3)); }
	inline List_1_t78F50ABA4688AD6A1255F66AB89EE8BAFD069913 * get__tearDownSystems_3() const { return ____tearDownSystems_3; }
	inline List_1_t78F50ABA4688AD6A1255F66AB89EE8BAFD069913 ** get_address_of__tearDownSystems_3() { return &____tearDownSystems_3; }
	inline void set__tearDownSystems_3(List_1_t78F50ABA4688AD6A1255F66AB89EE8BAFD069913 * value)
	{
		____tearDownSystems_3 = value;
		Il2CppCodeGenWriteBarrier((&____tearDownSystems_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMS_T4C86DA84B6D6F50FF23B497DD599ED594274DEA9_H
#ifndef GENERALCHEATS_TA76EB336B088C807D1B53B0A573317CEF56CCF76_H
#define GENERALCHEATS_TA76EB336B088C807D1B53B0A573317CEF56CCF76_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GeneralCheats
struct  GeneralCheats_tA76EB336B088C807D1B53B0A573317CEF56CCF76  : public RuntimeObject
{
public:
	// Tayr.VOSaver GeneralCheats::_voSaver
	VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * ____voSaver_0;
	// UserVO GeneralCheats::_userVO
	UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * ____userVO_1;
	// InventorySystem GeneralCheats::_inventorySystem
	InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 * ____inventorySystem_2;
	// InventoryVO GeneralCheats::_inventoryVO
	InventoryVO_tC7F0305FC930782BD2E4A47E3BD0504F0DCC6AD9 * ____inventoryVO_3;

public:
	inline static int32_t get_offset_of__voSaver_0() { return static_cast<int32_t>(offsetof(GeneralCheats_tA76EB336B088C807D1B53B0A573317CEF56CCF76, ____voSaver_0)); }
	inline VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * get__voSaver_0() const { return ____voSaver_0; }
	inline VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F ** get_address_of__voSaver_0() { return &____voSaver_0; }
	inline void set__voSaver_0(VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * value)
	{
		____voSaver_0 = value;
		Il2CppCodeGenWriteBarrier((&____voSaver_0), value);
	}

	inline static int32_t get_offset_of__userVO_1() { return static_cast<int32_t>(offsetof(GeneralCheats_tA76EB336B088C807D1B53B0A573317CEF56CCF76, ____userVO_1)); }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * get__userVO_1() const { return ____userVO_1; }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 ** get_address_of__userVO_1() { return &____userVO_1; }
	inline void set__userVO_1(UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * value)
	{
		____userVO_1 = value;
		Il2CppCodeGenWriteBarrier((&____userVO_1), value);
	}

	inline static int32_t get_offset_of__inventorySystem_2() { return static_cast<int32_t>(offsetof(GeneralCheats_tA76EB336B088C807D1B53B0A573317CEF56CCF76, ____inventorySystem_2)); }
	inline InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 * get__inventorySystem_2() const { return ____inventorySystem_2; }
	inline InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 ** get_address_of__inventorySystem_2() { return &____inventorySystem_2; }
	inline void set__inventorySystem_2(InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 * value)
	{
		____inventorySystem_2 = value;
		Il2CppCodeGenWriteBarrier((&____inventorySystem_2), value);
	}

	inline static int32_t get_offset_of__inventoryVO_3() { return static_cast<int32_t>(offsetof(GeneralCheats_tA76EB336B088C807D1B53B0A573317CEF56CCF76, ____inventoryVO_3)); }
	inline InventoryVO_tC7F0305FC930782BD2E4A47E3BD0504F0DCC6AD9 * get__inventoryVO_3() const { return ____inventoryVO_3; }
	inline InventoryVO_tC7F0305FC930782BD2E4A47E3BD0504F0DCC6AD9 ** get_address_of__inventoryVO_3() { return &____inventoryVO_3; }
	inline void set__inventoryVO_3(InventoryVO_tC7F0305FC930782BD2E4A47E3BD0504F0DCC6AD9 * value)
	{
		____inventoryVO_3 = value;
		Il2CppCodeGenWriteBarrier((&____inventoryVO_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERALCHEATS_TA76EB336B088C807D1B53B0A573317CEF56CCF76_H
#ifndef GOTCHANODEDATA_T7B74A837B128AFAF6F873873674CC2D974DB1EEB_H
#define GOTCHANODEDATA_T7B74A837B128AFAF6F873873674CC2D974DB1EEB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GotchaNodeData
struct  GotchaNodeData_t7B74A837B128AFAF6F873873674CC2D974DB1EEB  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<ItemContentModel> GotchaNodeData::Rewards
	List_1_t2C6CF342B96E130723D45845A1FD2053CBAF0F27 * ___Rewards_0;

public:
	inline static int32_t get_offset_of_Rewards_0() { return static_cast<int32_t>(offsetof(GotchaNodeData_t7B74A837B128AFAF6F873873674CC2D974DB1EEB, ___Rewards_0)); }
	inline List_1_t2C6CF342B96E130723D45845A1FD2053CBAF0F27 * get_Rewards_0() const { return ___Rewards_0; }
	inline List_1_t2C6CF342B96E130723D45845A1FD2053CBAF0F27 ** get_address_of_Rewards_0() { return &___Rewards_0; }
	inline void set_Rewards_0(List_1_t2C6CF342B96E130723D45845A1FD2053CBAF0F27 * value)
	{
		___Rewards_0 = value;
		Il2CppCodeGenWriteBarrier((&___Rewards_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GOTCHANODEDATA_T7B74A837B128AFAF6F873873674CC2D974DB1EEB_H
#ifndef LEVELCHESTCOMMANDMODEL_T34B3A388CD2A718C1293052B731684DF1038F485_H
#define LEVELCHESTCOMMANDMODEL_T34B3A388CD2A718C1293052B731684DF1038F485_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LevelChestCommandModel
struct  LevelChestCommandModel_t34B3A388CD2A718C1293052B731684DF1038F485  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELCHESTCOMMANDMODEL_T34B3A388CD2A718C1293052B731684DF1038F485_H
#ifndef LEVELCHESTCOMMANDRESULTMODEL_TF4ECE52AC2A37B46679689F0AA89616692B936F2_H
#define LEVELCHESTCOMMANDRESULTMODEL_TF4ECE52AC2A37B46679689F0AA89616692B936F2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LevelChestCommandResultModel
struct  LevelChestCommandResultModel_tF4ECE52AC2A37B46679689F0AA89616692B936F2  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<ItemContentModel> LevelChestCommandResultModel::Rewards
	List_1_t2C6CF342B96E130723D45845A1FD2053CBAF0F27 * ___Rewards_0;

public:
	inline static int32_t get_offset_of_Rewards_0() { return static_cast<int32_t>(offsetof(LevelChestCommandResultModel_tF4ECE52AC2A37B46679689F0AA89616692B936F2, ___Rewards_0)); }
	inline List_1_t2C6CF342B96E130723D45845A1FD2053CBAF0F27 * get_Rewards_0() const { return ___Rewards_0; }
	inline List_1_t2C6CF342B96E130723D45845A1FD2053CBAF0F27 ** get_address_of_Rewards_0() { return &___Rewards_0; }
	inline void set_Rewards_0(List_1_t2C6CF342B96E130723D45845A1FD2053CBAF0F27 * value)
	{
		___Rewards_0 = value;
		Il2CppCodeGenWriteBarrier((&___Rewards_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELCHESTCOMMANDRESULTMODEL_TF4ECE52AC2A37B46679689F0AA89616692B936F2_H
#ifndef LEVELLOSTCOMMANDMODEL_T4CD66AE0DDD1133CCF8B325881903764D8F85760_H
#define LEVELLOSTCOMMANDMODEL_T4CD66AE0DDD1133CCF8B325881903764D8F85760_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LevelLostCommandModel
struct  LevelLostCommandModel_t4CD66AE0DDD1133CCF8B325881903764D8F85760  : public RuntimeObject
{
public:
	// System.Int32 LevelLostCommandModel::LevelNumber
	int32_t ___LevelNumber_0;
	// System.Int32 LevelLostCommandModel::Stars
	int32_t ___Stars_1;
	// GameSparks.Core.GSData LevelLostCommandModel::ResponseData
	GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 * ___ResponseData_2;

public:
	inline static int32_t get_offset_of_LevelNumber_0() { return static_cast<int32_t>(offsetof(LevelLostCommandModel_t4CD66AE0DDD1133CCF8B325881903764D8F85760, ___LevelNumber_0)); }
	inline int32_t get_LevelNumber_0() const { return ___LevelNumber_0; }
	inline int32_t* get_address_of_LevelNumber_0() { return &___LevelNumber_0; }
	inline void set_LevelNumber_0(int32_t value)
	{
		___LevelNumber_0 = value;
	}

	inline static int32_t get_offset_of_Stars_1() { return static_cast<int32_t>(offsetof(LevelLostCommandModel_t4CD66AE0DDD1133CCF8B325881903764D8F85760, ___Stars_1)); }
	inline int32_t get_Stars_1() const { return ___Stars_1; }
	inline int32_t* get_address_of_Stars_1() { return &___Stars_1; }
	inline void set_Stars_1(int32_t value)
	{
		___Stars_1 = value;
	}

	inline static int32_t get_offset_of_ResponseData_2() { return static_cast<int32_t>(offsetof(LevelLostCommandModel_t4CD66AE0DDD1133CCF8B325881903764D8F85760, ___ResponseData_2)); }
	inline GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 * get_ResponseData_2() const { return ___ResponseData_2; }
	inline GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 ** get_address_of_ResponseData_2() { return &___ResponseData_2; }
	inline void set_ResponseData_2(GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 * value)
	{
		___ResponseData_2 = value;
		Il2CppCodeGenWriteBarrier((&___ResponseData_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELLOSTCOMMANDMODEL_T4CD66AE0DDD1133CCF8B325881903764D8F85760_H
#ifndef LEVELLOSTCOMMANDRESULTMODEL_TCD8892EDE40EEDD2DA9FB8BF2A01A253B668B06B_H
#define LEVELLOSTCOMMANDRESULTMODEL_TCD8892EDE40EEDD2DA9FB8BF2A01A253B668B06B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LevelLostCommandResultModel
struct  LevelLostCommandResultModel_tCD8892EDE40EEDD2DA9FB8BF2A01A253B668B06B  : public RuntimeObject
{
public:
	// System.Int32 LevelLostCommandResultModel::Stars
	int32_t ___Stars_0;

public:
	inline static int32_t get_offset_of_Stars_0() { return static_cast<int32_t>(offsetof(LevelLostCommandResultModel_tCD8892EDE40EEDD2DA9FB8BF2A01A253B668B06B, ___Stars_0)); }
	inline int32_t get_Stars_0() const { return ___Stars_0; }
	inline int32_t* get_address_of_Stars_0() { return &___Stars_0; }
	inline void set_Stars_0(int32_t value)
	{
		___Stars_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELLOSTCOMMANDRESULTMODEL_TCD8892EDE40EEDD2DA9FB8BF2A01A253B668B06B_H
#ifndef LEVELWONCOMMANDMODEL_T1A8C787CFE6EA68E97E76B2C5C25238208FCF53A_H
#define LEVELWONCOMMANDMODEL_T1A8C787CFE6EA68E97E76B2C5C25238208FCF53A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LevelWonCommandModel
struct  LevelWonCommandModel_t1A8C787CFE6EA68E97E76B2C5C25238208FCF53A  : public RuntimeObject
{
public:
	// System.Int32 LevelWonCommandModel::Stars
	int32_t ___Stars_0;

public:
	inline static int32_t get_offset_of_Stars_0() { return static_cast<int32_t>(offsetof(LevelWonCommandModel_t1A8C787CFE6EA68E97E76B2C5C25238208FCF53A, ___Stars_0)); }
	inline int32_t get_Stars_0() const { return ___Stars_0; }
	inline int32_t* get_address_of_Stars_0() { return &___Stars_0; }
	inline void set_Stars_0(int32_t value)
	{
		___Stars_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELWONCOMMANDMODEL_T1A8C787CFE6EA68E97E76B2C5C25238208FCF53A_H
#ifndef LEVELWONCOMMANDRESULTMODEL_T858BFA2B17BB11DDE488DA1E1E66078C6E920091_H
#define LEVELWONCOMMANDRESULTMODEL_T858BFA2B17BB11DDE488DA1E1E66078C6E920091_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LevelWonCommandResultModel
struct  LevelWonCommandResultModel_t858BFA2B17BB11DDE488DA1E1E66078C6E920091  : public RuntimeObject
{
public:
	// System.Int32 LevelWonCommandResultModel::Stars
	int32_t ___Stars_0;

public:
	inline static int32_t get_offset_of_Stars_0() { return static_cast<int32_t>(offsetof(LevelWonCommandResultModel_t858BFA2B17BB11DDE488DA1E1E66078C6E920091, ___Stars_0)); }
	inline int32_t get_Stars_0() const { return ___Stars_0; }
	inline int32_t* get_address_of_Stars_0() { return &___Stars_0; }
	inline void set_Stars_0(int32_t value)
	{
		___Stars_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELWONCOMMANDRESULTMODEL_T858BFA2B17BB11DDE488DA1E1E66078C6E920091_H
#ifndef LOOPINGAICOMPONENT_T9AA3BF1DC9DA6157F248DD04289BEBABA54B1EBC_H
#define LOOPINGAICOMPONENT_T9AA3BF1DC9DA6157F248DD04289BEBABA54B1EBC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoopingAIComponent
struct  LoopingAIComponent_t9AA3BF1DC9DA6157F248DD04289BEBABA54B1EBC  : public RuntimeObject
{
public:
	// System.Single LoopingAIComponent::LastLoopTime
	float ___LastLoopTime_0;
	// System.Single LoopingAIComponent::Frequency
	float ___Frequency_1;

public:
	inline static int32_t get_offset_of_LastLoopTime_0() { return static_cast<int32_t>(offsetof(LoopingAIComponent_t9AA3BF1DC9DA6157F248DD04289BEBABA54B1EBC, ___LastLoopTime_0)); }
	inline float get_LastLoopTime_0() const { return ___LastLoopTime_0; }
	inline float* get_address_of_LastLoopTime_0() { return &___LastLoopTime_0; }
	inline void set_LastLoopTime_0(float value)
	{
		___LastLoopTime_0 = value;
	}

	inline static int32_t get_offset_of_Frequency_1() { return static_cast<int32_t>(offsetof(LoopingAIComponent_t9AA3BF1DC9DA6157F248DD04289BEBABA54B1EBC, ___Frequency_1)); }
	inline float get_Frequency_1() const { return ___Frequency_1; }
	inline float* get_address_of_Frequency_1() { return &___Frequency_1; }
	inline void set_Frequency_1(float value)
	{
		___Frequency_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOOPINGAICOMPONENT_T9AA3BF1DC9DA6157F248DD04289BEBABA54B1EBC_H
#ifndef LOOPINGAISYSTEM_T104C524417DE5C4BDFC9FB1AD4A1A57ED37F9B06_H
#define LOOPINGAISYSTEM_T104C524417DE5C4BDFC9FB1AD4A1A57ED37F9B06_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoopingAISystem
struct  LoopingAISystem_t104C524417DE5C4BDFC9FB1AD4A1A57ED37F9B06  : public RuntimeObject
{
public:
	// SpellsSO LoopingAISystem::_spellsSO
	SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * ____spellsSO_0;
	// Entitas.IGroup`1<GameEntity> LoopingAISystem::_entities
	RuntimeObject* ____entities_1;
	// GameContext LoopingAISystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_2;

public:
	inline static int32_t get_offset_of__spellsSO_0() { return static_cast<int32_t>(offsetof(LoopingAISystem_t104C524417DE5C4BDFC9FB1AD4A1A57ED37F9B06, ____spellsSO_0)); }
	inline SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * get__spellsSO_0() const { return ____spellsSO_0; }
	inline SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 ** get_address_of__spellsSO_0() { return &____spellsSO_0; }
	inline void set__spellsSO_0(SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * value)
	{
		____spellsSO_0 = value;
		Il2CppCodeGenWriteBarrier((&____spellsSO_0), value);
	}

	inline static int32_t get_offset_of__entities_1() { return static_cast<int32_t>(offsetof(LoopingAISystem_t104C524417DE5C4BDFC9FB1AD4A1A57ED37F9B06, ____entities_1)); }
	inline RuntimeObject* get__entities_1() const { return ____entities_1; }
	inline RuntimeObject** get_address_of__entities_1() { return &____entities_1; }
	inline void set__entities_1(RuntimeObject* value)
	{
		____entities_1 = value;
		Il2CppCodeGenWriteBarrier((&____entities_1), value);
	}

	inline static int32_t get_offset_of__context_2() { return static_cast<int32_t>(offsetof(LoopingAISystem_t104C524417DE5C4BDFC9FB1AD4A1A57ED37F9B06, ____context_2)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_2() const { return ____context_2; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_2() { return &____context_2; }
	inline void set__context_2(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_2 = value;
		Il2CppCodeGenWriteBarrier((&____context_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOOPINGAISYSTEM_T104C524417DE5C4BDFC9FB1AD4A1A57ED37F9B06_H
#ifndef NOTIFICATIONCHEAT_TCE02BBFC7C7ECB3543798505AA3969BD07D41748_H
#define NOTIFICATIONCHEAT_TCE02BBFC7C7ECB3543798505AA3969BD07D41748_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NotificationCheat
struct  NotificationCheat_tCE02BBFC7C7ECB3543798505AA3969BD07D41748  : public RuntimeObject
{
public:
	// NotificationSamples.GameNotificationsManager NotificationCheat::_notificationsManager
	GameNotificationsManager_tA91A2FAF80490C54B58C02C8B5652AFA290D6B9F * ____notificationsManager_0;

public:
	inline static int32_t get_offset_of__notificationsManager_0() { return static_cast<int32_t>(offsetof(NotificationCheat_tCE02BBFC7C7ECB3543798505AA3969BD07D41748, ____notificationsManager_0)); }
	inline GameNotificationsManager_tA91A2FAF80490C54B58C02C8B5652AFA290D6B9F * get__notificationsManager_0() const { return ____notificationsManager_0; }
	inline GameNotificationsManager_tA91A2FAF80490C54B58C02C8B5652AFA290D6B9F ** get_address_of__notificationsManager_0() { return &____notificationsManager_0; }
	inline void set__notificationsManager_0(GameNotificationsManager_tA91A2FAF80490C54B58C02C8B5652AFA290D6B9F * value)
	{
		____notificationsManager_0 = value;
		Il2CppCodeGenWriteBarrier((&____notificationsManager_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTIFICATIONCHEAT_TCE02BBFC7C7ECB3543798505AA3969BD07D41748_H
#ifndef PROJECTILEMOVEMENTSYSTEM_T9B5DFF53BDDD117EC20BFF18FD35E54B638F6E10_H
#define PROJECTILEMOVEMENTSYSTEM_T9B5DFF53BDDD117EC20BFF18FD35E54B638F6E10_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ProjectileMovementSystem
struct  ProjectileMovementSystem_t9B5DFF53BDDD117EC20BFF18FD35E54B638F6E10  : public RuntimeObject
{
public:
	// Entitas.IGroup`1<GameEntity> ProjectileMovementSystem::_entities
	RuntimeObject* ____entities_0;
	// GameContext ProjectileMovementSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_1;

public:
	inline static int32_t get_offset_of__entities_0() { return static_cast<int32_t>(offsetof(ProjectileMovementSystem_t9B5DFF53BDDD117EC20BFF18FD35E54B638F6E10, ____entities_0)); }
	inline RuntimeObject* get__entities_0() const { return ____entities_0; }
	inline RuntimeObject** get_address_of__entities_0() { return &____entities_0; }
	inline void set__entities_0(RuntimeObject* value)
	{
		____entities_0 = value;
		Il2CppCodeGenWriteBarrier((&____entities_0), value);
	}

	inline static int32_t get_offset_of__context_1() { return static_cast<int32_t>(offsetof(ProjectileMovementSystem_t9B5DFF53BDDD117EC20BFF18FD35E54B638F6E10, ____context_1)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_1() const { return ____context_1; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_1() { return &____context_1; }
	inline void set__context_1(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_1 = value;
		Il2CppCodeGenWriteBarrier((&____context_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROJECTILEMOVEMENTSYSTEM_T9B5DFF53BDDD117EC20BFF18FD35E54B638F6E10_H
#ifndef PULSEAICOMPONENT_TF2952693A05C9C863A4F15B53411CDE887287D9E_H
#define PULSEAICOMPONENT_TF2952693A05C9C863A4F15B53411CDE887287D9E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PulseAIComponent
struct  PulseAIComponent_tF2952693A05C9C863A4F15B53411CDE887287D9E  : public RuntimeObject
{
public:
	// System.Single PulseAIComponent::LastPulseTime
	float ___LastPulseTime_0;
	// System.Single PulseAIComponent::Frequency
	float ___Frequency_1;

public:
	inline static int32_t get_offset_of_LastPulseTime_0() { return static_cast<int32_t>(offsetof(PulseAIComponent_tF2952693A05C9C863A4F15B53411CDE887287D9E, ___LastPulseTime_0)); }
	inline float get_LastPulseTime_0() const { return ___LastPulseTime_0; }
	inline float* get_address_of_LastPulseTime_0() { return &___LastPulseTime_0; }
	inline void set_LastPulseTime_0(float value)
	{
		___LastPulseTime_0 = value;
	}

	inline static int32_t get_offset_of_Frequency_1() { return static_cast<int32_t>(offsetof(PulseAIComponent_tF2952693A05C9C863A4F15B53411CDE887287D9E, ___Frequency_1)); }
	inline float get_Frequency_1() const { return ___Frequency_1; }
	inline float* get_address_of_Frequency_1() { return &___Frequency_1; }
	inline void set_Frequency_1(float value)
	{
		___Frequency_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PULSEAICOMPONENT_TF2952693A05C9C863A4F15B53411CDE887287D9E_H
#ifndef PULSEAISYSTEM_TAEEA3660C9ADDF1E15251FAA417EB456E79FE402_H
#define PULSEAISYSTEM_TAEEA3660C9ADDF1E15251FAA417EB456E79FE402_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PulseAISystem
struct  PulseAISystem_tAEEA3660C9ADDF1E15251FAA417EB456E79FE402  : public RuntimeObject
{
public:
	// SpellsSO PulseAISystem::_spellsSO
	SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * ____spellsSO_0;
	// Entitas.IGroup`1<GameEntity> PulseAISystem::_entities
	RuntimeObject* ____entities_1;
	// GameContext PulseAISystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_2;

public:
	inline static int32_t get_offset_of__spellsSO_0() { return static_cast<int32_t>(offsetof(PulseAISystem_tAEEA3660C9ADDF1E15251FAA417EB456E79FE402, ____spellsSO_0)); }
	inline SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * get__spellsSO_0() const { return ____spellsSO_0; }
	inline SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 ** get_address_of__spellsSO_0() { return &____spellsSO_0; }
	inline void set__spellsSO_0(SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * value)
	{
		____spellsSO_0 = value;
		Il2CppCodeGenWriteBarrier((&____spellsSO_0), value);
	}

	inline static int32_t get_offset_of__entities_1() { return static_cast<int32_t>(offsetof(PulseAISystem_tAEEA3660C9ADDF1E15251FAA417EB456E79FE402, ____entities_1)); }
	inline RuntimeObject* get__entities_1() const { return ____entities_1; }
	inline RuntimeObject** get_address_of__entities_1() { return &____entities_1; }
	inline void set__entities_1(RuntimeObject* value)
	{
		____entities_1 = value;
		Il2CppCodeGenWriteBarrier((&____entities_1), value);
	}

	inline static int32_t get_offset_of__context_2() { return static_cast<int32_t>(offsetof(PulseAISystem_tAEEA3660C9ADDF1E15251FAA417EB456E79FE402, ____context_2)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_2() const { return ____context_2; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_2() { return &____context_2; }
	inline void set__context_2(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_2 = value;
		Il2CppCodeGenWriteBarrier((&____context_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PULSEAISYSTEM_TAEEA3660C9ADDF1E15251FAA417EB456E79FE402_H
#ifndef RAYCASTUTILS_T75B4B4DA2EC9161B7E9694AA3B787525AEC1ABCA_H
#define RAYCASTUTILS_T75B4B4DA2EC9161B7E9694AA3B787525AEC1ABCA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RaycastUtils
struct  RaycastUtils_t75B4B4DA2EC9161B7E9694AA3B787525AEC1ABCA  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTUTILS_T75B4B4DA2EC9161B7E9694AA3B787525AEC1ABCA_H
#ifndef REVIVECOMMANDMODEL_T9016F442CB25E5AED7385F73B0CE9C969ED51879_H
#define REVIVECOMMANDMODEL_T9016F442CB25E5AED7385F73B0CE9C969ED51879_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReviveCommandModel
struct  ReviveCommandModel_t9016F442CB25E5AED7385F73B0CE9C969ED51879  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REVIVECOMMANDMODEL_T9016F442CB25E5AED7385F73B0CE9C969ED51879_H
#ifndef REVIVECOMMANDRESULTMODEL_T483C14D7B2131E717207F02F0FFE04752FFCB0E1_H
#define REVIVECOMMANDRESULTMODEL_T483C14D7B2131E717207F02F0FFE04752FFCB0E1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReviveCommandResultModel
struct  ReviveCommandResultModel_t483C14D7B2131E717207F02F0FFE04752FFCB0E1  : public RuntimeObject
{
public:
	// System.Boolean ReviveCommandResultModel::Revived
	bool ___Revived_0;

public:
	inline static int32_t get_offset_of_Revived_0() { return static_cast<int32_t>(offsetof(ReviveCommandResultModel_t483C14D7B2131E717207F02F0FFE04752FFCB0E1, ___Revived_0)); }
	inline bool get_Revived_0() const { return ___Revived_0; }
	inline bool* get_address_of_Revived_0() { return &___Revived_0; }
	inline void set_Revived_0(bool value)
	{
		___Revived_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REVIVECOMMANDRESULTMODEL_T483C14D7B2131E717207F02F0FFE04752FFCB0E1_H
#ifndef SPELLCOMPONENT_T4675125CB9902D20CF5FE1D1BFE8D3C06B147A40_H
#define SPELLCOMPONENT_T4675125CB9902D20CF5FE1D1BFE8D3C06B147A40_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpellComponent
struct  SpellComponent_t4675125CB9902D20CF5FE1D1BFE8D3C06B147A40  : public RuntimeObject
{
public:
	// System.String SpellComponent::SpellId
	String_t* ___SpellId_0;

public:
	inline static int32_t get_offset_of_SpellId_0() { return static_cast<int32_t>(offsetof(SpellComponent_t4675125CB9902D20CF5FE1D1BFE8D3C06B147A40, ___SpellId_0)); }
	inline String_t* get_SpellId_0() const { return ___SpellId_0; }
	inline String_t** get_address_of_SpellId_0() { return &___SpellId_0; }
	inline void set_SpellId_0(String_t* value)
	{
		___SpellId_0 = value;
		Il2CppCodeGenWriteBarrier((&___SpellId_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPELLCOMPONENT_T4675125CB9902D20CF5FE1D1BFE8D3C06B147A40_H
#ifndef SPELLINDEXCOMPONENT_TDF0FF7388ABB141C0263B711256C2C6912F523DF_H
#define SPELLINDEXCOMPONENT_TDF0FF7388ABB141C0263B711256C2C6912F523DF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpellIndexComponent
struct  SpellIndexComponent_tDF0FF7388ABB141C0263B711256C2C6912F523DF  : public RuntimeObject
{
public:
	// System.Int32 SpellIndexComponent::Index
	int32_t ___Index_0;

public:
	inline static int32_t get_offset_of_Index_0() { return static_cast<int32_t>(offsetof(SpellIndexComponent_tDF0FF7388ABB141C0263B711256C2C6912F523DF, ___Index_0)); }
	inline int32_t get_Index_0() const { return ___Index_0; }
	inline int32_t* get_address_of_Index_0() { return &___Index_0; }
	inline void set_Index_0(int32_t value)
	{
		___Index_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPELLINDEXCOMPONENT_TDF0FF7388ABB141C0263B711256C2C6912F523DF_H
#ifndef SPELLMETACOMPONENT_TB4965B165064C7E469BFB468BFB0B2ED45329109_H
#define SPELLMETACOMPONENT_TB4965B165064C7E469BFB468BFB0B2ED45329109_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpellMetaComponent
struct  SpellMetaComponent_tB4965B165064C7E469BFB468BFB0B2ED45329109  : public RuntimeObject
{
public:
	// System.String SpellMetaComponent::SpellId
	String_t* ___SpellId_0;

public:
	inline static int32_t get_offset_of_SpellId_0() { return static_cast<int32_t>(offsetof(SpellMetaComponent_tB4965B165064C7E469BFB468BFB0B2ED45329109, ___SpellId_0)); }
	inline String_t* get_SpellId_0() const { return ___SpellId_0; }
	inline String_t** get_address_of_SpellId_0() { return &___SpellId_0; }
	inline void set_SpellId_0(String_t* value)
	{
		___SpellId_0 = value;
		Il2CppCodeGenWriteBarrier((&___SpellId_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPELLMETACOMPONENT_TB4965B165064C7E469BFB468BFB0B2ED45329109_H
#ifndef STARTLEVELCHEAT_TED489B7E94B35861FBF1812AE937C30957714A96_H
#define STARTLEVELCHEAT_TED489B7E94B35861FBF1812AE937C30957714A96_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StartLevelCheat
struct  StartLevelCheat_tED489B7E94B35861FBF1812AE937C30957714A96  : public RuntimeObject
{
public:
	// UserVO StartLevelCheat::_userVO
	UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * ____userVO_0;
	// Tayr.VOSaver StartLevelCheat::_voSaver
	VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * ____voSaver_1;

public:
	inline static int32_t get_offset_of__userVO_0() { return static_cast<int32_t>(offsetof(StartLevelCheat_tED489B7E94B35861FBF1812AE937C30957714A96, ____userVO_0)); }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * get__userVO_0() const { return ____userVO_0; }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 ** get_address_of__userVO_0() { return &____userVO_0; }
	inline void set__userVO_0(UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * value)
	{
		____userVO_0 = value;
		Il2CppCodeGenWriteBarrier((&____userVO_0), value);
	}

	inline static int32_t get_offset_of__voSaver_1() { return static_cast<int32_t>(offsetof(StartLevelCheat_tED489B7E94B35861FBF1812AE937C30957714A96, ____voSaver_1)); }
	inline VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * get__voSaver_1() const { return ____voSaver_1; }
	inline VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F ** get_address_of__voSaver_1() { return &____voSaver_1; }
	inline void set__voSaver_1(VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * value)
	{
		____voSaver_1 = value;
		Il2CppCodeGenWriteBarrier((&____voSaver_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STARTLEVELCHEAT_TED489B7E94B35861FBF1812AE937C30957714A96_H
#ifndef SUBMITLEVELCHEAT_T77B4717B7CE62D7C292C127633F118AD5C13D22F_H
#define SUBMITLEVELCHEAT_T77B4717B7CE62D7C292C127633F118AD5C13D22F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SubmitLevelCheat
struct  SubmitLevelCheat_t77B4717B7CE62D7C292C127633F118AD5C13D22F  : public RuntimeObject
{
public:
	// Tayr.GameSparksPlatform SubmitLevelCheat::_gameSpark
	GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * ____gameSpark_0;
	// UserVO SubmitLevelCheat::_userVO
	UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * ____userVO_1;

public:
	inline static int32_t get_offset_of__gameSpark_0() { return static_cast<int32_t>(offsetof(SubmitLevelCheat_t77B4717B7CE62D7C292C127633F118AD5C13D22F, ____gameSpark_0)); }
	inline GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * get__gameSpark_0() const { return ____gameSpark_0; }
	inline GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 ** get_address_of__gameSpark_0() { return &____gameSpark_0; }
	inline void set__gameSpark_0(GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * value)
	{
		____gameSpark_0 = value;
		Il2CppCodeGenWriteBarrier((&____gameSpark_0), value);
	}

	inline static int32_t get_offset_of__userVO_1() { return static_cast<int32_t>(offsetof(SubmitLevelCheat_t77B4717B7CE62D7C292C127633F118AD5C13D22F, ____userVO_1)); }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * get__userVO_1() const { return ____userVO_1; }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 ** get_address_of__userVO_1() { return &____userVO_1; }
	inline void set__userVO_1(UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * value)
	{
		____userVO_1 = value;
		Il2CppCodeGenWriteBarrier((&____userVO_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBMITLEVELCHEAT_T77B4717B7CE62D7C292C127633F118AD5C13D22F_H
#ifndef U3CU3EC__DISPLAYCLASS2_0_TFA4ECACEC58A7A5E144BF7182AF518243943300C_H
#define U3CU3EC__DISPLAYCLASS2_0_TFA4ECACEC58A7A5E144BF7182AF518243943300C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SubmitLevelCheat_<>c__DisplayClass2_0
struct  U3CU3Ec__DisplayClass2_0_tFA4ECACEC58A7A5E144BF7182AF518243943300C  : public RuntimeObject
{
public:
	// SubmitLevelCheat SubmitLevelCheat_<>c__DisplayClass2_0::<>4__this
	SubmitLevelCheat_t77B4717B7CE62D7C292C127633F118AD5C13D22F * ___U3CU3E4__this_0;
	// System.Int32 SubmitLevelCheat_<>c__DisplayClass2_0::stars
	int32_t ___stars_1;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_tFA4ECACEC58A7A5E144BF7182AF518243943300C, ___U3CU3E4__this_0)); }
	inline SubmitLevelCheat_t77B4717B7CE62D7C292C127633F118AD5C13D22F * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline SubmitLevelCheat_t77B4717B7CE62D7C292C127633F118AD5C13D22F ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(SubmitLevelCheat_t77B4717B7CE62D7C292C127633F118AD5C13D22F * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_stars_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_tFA4ECACEC58A7A5E144BF7182AF518243943300C, ___stars_1)); }
	inline int32_t get_stars_1() const { return ___stars_1; }
	inline int32_t* get_address_of_stars_1() { return &___stars_1; }
	inline void set_stars_1(int32_t value)
	{
		___stars_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS2_0_TFA4ECACEC58A7A5E144BF7182AF518243943300C_H
#ifndef SUBMITTROPHYCHEAT_T8C8A63FD5BD974542EC4BB7B1E70B6CFABC9541C_H
#define SUBMITTROPHYCHEAT_T8C8A63FD5BD974542EC4BB7B1E70B6CFABC9541C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SubmitTrophyCheat
struct  SubmitTrophyCheat_t8C8A63FD5BD974542EC4BB7B1E70B6CFABC9541C  : public RuntimeObject
{
public:
	// Tayr.GameSparksPlatform SubmitTrophyCheat::_gameSpark
	GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * ____gameSpark_0;
	// UserVO SubmitTrophyCheat::_userVO
	UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * ____userVO_1;

public:
	inline static int32_t get_offset_of__gameSpark_0() { return static_cast<int32_t>(offsetof(SubmitTrophyCheat_t8C8A63FD5BD974542EC4BB7B1E70B6CFABC9541C, ____gameSpark_0)); }
	inline GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * get__gameSpark_0() const { return ____gameSpark_0; }
	inline GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 ** get_address_of__gameSpark_0() { return &____gameSpark_0; }
	inline void set__gameSpark_0(GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * value)
	{
		____gameSpark_0 = value;
		Il2CppCodeGenWriteBarrier((&____gameSpark_0), value);
	}

	inline static int32_t get_offset_of__userVO_1() { return static_cast<int32_t>(offsetof(SubmitTrophyCheat_t8C8A63FD5BD974542EC4BB7B1E70B6CFABC9541C, ____userVO_1)); }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * get__userVO_1() const { return ____userVO_1; }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 ** get_address_of__userVO_1() { return &____userVO_1; }
	inline void set__userVO_1(UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * value)
	{
		____userVO_1 = value;
		Il2CppCodeGenWriteBarrier((&____userVO_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBMITTROPHYCHEAT_T8C8A63FD5BD974542EC4BB7B1E70B6CFABC9541C_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef TAPTAPTOWERRECOGNIZERCOMPONENT_T89827D4F7E4C2C288ACA589DD3798E442CB820E4_H
#define TAPTAPTOWERRECOGNIZERCOMPONENT_T89827D4F7E4C2C288ACA589DD3798E442CB820E4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TapTapTowerRecognizerComponent
struct  TapTapTowerRecognizerComponent_t89827D4F7E4C2C288ACA589DD3798E442CB820E4  : public RuntimeObject
{
public:
	// System.String TapTapTowerRecognizerComponent::TargetId
	String_t* ___TargetId_0;

public:
	inline static int32_t get_offset_of_TargetId_0() { return static_cast<int32_t>(offsetof(TapTapTowerRecognizerComponent_t89827D4F7E4C2C288ACA589DD3798E442CB820E4, ___TargetId_0)); }
	inline String_t* get_TargetId_0() const { return ___TargetId_0; }
	inline String_t** get_address_of_TargetId_0() { return &___TargetId_0; }
	inline void set_TargetId_0(String_t* value)
	{
		___TargetId_0 = value;
		Il2CppCodeGenWriteBarrier((&___TargetId_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAPTAPTOWERRECOGNIZERCOMPONENT_T89827D4F7E4C2C288ACA589DD3798E442CB820E4_H
#ifndef BASICCOMMAND_2_T3832919DED03BBD7125B47683E8E7345F6DACD60_H
#define BASICCOMMAND_2_T3832919DED03BBD7125B47683E8E7345F6DACD60_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.BasicCommand`2<AddSpellsCheatCommandModel,AddSpellsCheatCommandResultModel>
struct  BasicCommand_2_t3832919DED03BBD7125B47683E8E7345F6DACD60  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICCOMMAND_2_T3832919DED03BBD7125B47683E8E7345F6DACD60_H
#ifndef BASICCOMMAND_2_T17D98B836F02EB94D6FEDF45675409959DA9D577_H
#define BASICCOMMAND_2_T17D98B836F02EB94D6FEDF45675409959DA9D577_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.BasicCommand`2<ConsumeSpellCommandModel,ConsumeSpellCommandResultModel>
struct  BasicCommand_2_t17D98B836F02EB94D6FEDF45675409959DA9D577  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICCOMMAND_2_T17D98B836F02EB94D6FEDF45675409959DA9D577_H
#ifndef BASICCOMMAND_2_TEA933A19074E7EC45005BA02213FEC3BC998CEB7_H
#define BASICCOMMAND_2_TEA933A19074E7EC45005BA02213FEC3BC998CEB7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.BasicCommand`2<LevelChestCommandModel,LevelChestCommandResultModel>
struct  BasicCommand_2_tEA933A19074E7EC45005BA02213FEC3BC998CEB7  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICCOMMAND_2_TEA933A19074E7EC45005BA02213FEC3BC998CEB7_H
#ifndef BASICCOMMAND_2_TFA40288012C0693610100789FFCBCFF70F5306CA_H
#define BASICCOMMAND_2_TFA40288012C0693610100789FFCBCFF70F5306CA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.BasicCommand`2<LevelLostCommandModel,LevelLostCommandResultModel>
struct  BasicCommand_2_tFA40288012C0693610100789FFCBCFF70F5306CA  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICCOMMAND_2_TFA40288012C0693610100789FFCBCFF70F5306CA_H
#ifndef BASICCOMMAND_2_TFCE53C296FEA5AEA83DBB18C4CE2BF2CE9C66D16_H
#define BASICCOMMAND_2_TFCE53C296FEA5AEA83DBB18C4CE2BF2CE9C66D16_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.BasicCommand`2<LevelWonCommandModel,LevelWonCommandResultModel>
struct  BasicCommand_2_tFCE53C296FEA5AEA83DBB18C4CE2BF2CE9C66D16  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICCOMMAND_2_TFCE53C296FEA5AEA83DBB18C4CE2BF2CE9C66D16_H
#ifndef BASICCOMMAND_2_T8EA644DCD7371D25EC39CE9C1487E6348F50035D_H
#define BASICCOMMAND_2_T8EA644DCD7371D25EC39CE9C1487E6348F50035D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.BasicCommand`2<ReviveCommandModel,ReviveCommandResultModel>
struct  BasicCommand_2_t8EA644DCD7371D25EC39CE9C1487E6348F50035D  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICCOMMAND_2_T8EA644DCD7371D25EC39CE9C1487E6348F50035D_H
#ifndef TOWERHEALINGCOMPONENT_TBF1C6AD3469D91F868A272D1ED2BD56CDB4D93D4_H
#define TOWERHEALINGCOMPONENT_TBF1C6AD3469D91F868A272D1ED2BD56CDB4D93D4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TowerHealingComponent
struct  TowerHealingComponent_tBF1C6AD3469D91F868A272D1ED2BD56CDB4D93D4  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOWERHEALINGCOMPONENT_TBF1C6AD3469D91F868A272D1ED2BD56CDB4D93D4_H
#ifndef TOWERHEALINGDURATION_T247E3B8EA86A4D76A40D407854846D21CCABF915_H
#define TOWERHEALINGDURATION_T247E3B8EA86A4D76A40D407854846D21CCABF915_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TowerHealingDuration
struct  TowerHealingDuration_t247E3B8EA86A4D76A40D407854846D21CCABF915  : public RuntimeObject
{
public:
	// System.Single TowerHealingDuration::Duration
	float ___Duration_0;

public:
	inline static int32_t get_offset_of_Duration_0() { return static_cast<int32_t>(offsetof(TowerHealingDuration_t247E3B8EA86A4D76A40D407854846D21CCABF915, ___Duration_0)); }
	inline float get_Duration_0() const { return ___Duration_0; }
	inline float* get_address_of_Duration_0() { return &___Duration_0; }
	inline void set_Duration_0(float value)
	{
		___Duration_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOWERHEALINGDURATION_T247E3B8EA86A4D76A40D407854846D21CCABF915_H
#ifndef TOWERHEALINGSTARTTIMECOMPONENT_TD7958630EA379A94C28205095CD7F9C18C07EFAB_H
#define TOWERHEALINGSTARTTIMECOMPONENT_TD7958630EA379A94C28205095CD7F9C18C07EFAB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TowerHealingStartTimeComponent
struct  TowerHealingStartTimeComponent_tD7958630EA379A94C28205095CD7F9C18C07EFAB  : public RuntimeObject
{
public:
	// System.Single TowerHealingStartTimeComponent::StartTime
	float ___StartTime_0;

public:
	inline static int32_t get_offset_of_StartTime_0() { return static_cast<int32_t>(offsetof(TowerHealingStartTimeComponent_tD7958630EA379A94C28205095CD7F9C18C07EFAB, ___StartTime_0)); }
	inline float get_StartTime_0() const { return ___StartTime_0; }
	inline float* get_address_of_StartTime_0() { return &___StartTime_0; }
	inline void set_StartTime_0(float value)
	{
		___StartTime_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOWERHEALINGSTARTTIMECOMPONENT_TD7958630EA379A94C28205095CD7F9C18C07EFAB_H
#ifndef TOWERHEALINGSYSTEM_T35BED9065265E4593021A609BC41DFFCE7250461_H
#define TOWERHEALINGSYSTEM_T35BED9065265E4593021A609BC41DFFCE7250461_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TowerHealingSystem
struct  TowerHealingSystem_t35BED9065265E4593021A609BC41DFFCE7250461  : public RuntimeObject
{
public:
	// TowersSO TowerHealingSystem::_towersSO
	TowersSO_t3B7DE821E5F8017950776EEFC42E680B361BCDF0 * ____towersSO_0;
	// GameContext TowerHealingSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_1;
	// Entitas.IGroup`1<GameEntity> TowerHealingSystem::_towers
	RuntimeObject* ____towers_2;

public:
	inline static int32_t get_offset_of__towersSO_0() { return static_cast<int32_t>(offsetof(TowerHealingSystem_t35BED9065265E4593021A609BC41DFFCE7250461, ____towersSO_0)); }
	inline TowersSO_t3B7DE821E5F8017950776EEFC42E680B361BCDF0 * get__towersSO_0() const { return ____towersSO_0; }
	inline TowersSO_t3B7DE821E5F8017950776EEFC42E680B361BCDF0 ** get_address_of__towersSO_0() { return &____towersSO_0; }
	inline void set__towersSO_0(TowersSO_t3B7DE821E5F8017950776EEFC42E680B361BCDF0 * value)
	{
		____towersSO_0 = value;
		Il2CppCodeGenWriteBarrier((&____towersSO_0), value);
	}

	inline static int32_t get_offset_of__context_1() { return static_cast<int32_t>(offsetof(TowerHealingSystem_t35BED9065265E4593021A609BC41DFFCE7250461, ____context_1)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_1() const { return ____context_1; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_1() { return &____context_1; }
	inline void set__context_1(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_1 = value;
		Il2CppCodeGenWriteBarrier((&____context_1), value);
	}

	inline static int32_t get_offset_of__towers_2() { return static_cast<int32_t>(offsetof(TowerHealingSystem_t35BED9065265E4593021A609BC41DFFCE7250461, ____towers_2)); }
	inline RuntimeObject* get__towers_2() const { return ____towers_2; }
	inline RuntimeObject** get_address_of__towers_2() { return &____towers_2; }
	inline void set__towers_2(RuntimeObject* value)
	{
		____towers_2 = value;
		Il2CppCodeGenWriteBarrier((&____towers_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOWERHEALINGSYSTEM_T35BED9065265E4593021A609BC41DFFCE7250461_H
#ifndef UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#define UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * ___m_PersistentCalls_1;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_2;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_Calls_0)); }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_CallsDirty_2)); }
	inline bool get_m_CallsDirty_2() const { return ___m_CallsDirty_2; }
	inline bool* get_address_of_m_CallsDirty_2() { return &___m_CallsDirty_2; }
	inline void set_m_CallsDirty_2(bool value)
	{
		___m_CallsDirty_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#ifndef ADDSPELLSCHEATCOMMAND_T82FD966051B1B85FE769FC1137874BB39F67C60F_H
#define ADDSPELLSCHEATCOMMAND_T82FD966051B1B85FE769FC1137874BB39F67C60F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AddSpellsCheatCommand
struct  AddSpellsCheatCommand_t82FD966051B1B85FE769FC1137874BB39F67C60F  : public BasicCommand_2_t3832919DED03BBD7125B47683E8E7345F6DACD60
{
public:
	// InventorySystem AddSpellsCheatCommand::_inventorySystem
	InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 * ____inventorySystem_0;
	// Tayr.VOSaver AddSpellsCheatCommand::_voSaver
	VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * ____voSaver_1;
	// InventoryVO AddSpellsCheatCommand::_inventoryVO
	InventoryVO_tC7F0305FC930782BD2E4A47E3BD0504F0DCC6AD9 * ____inventoryVO_2;
	// BattleSettingsSO AddSpellsCheatCommand::_battleSettingsSO
	BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE * ____battleSettingsSO_3;

public:
	inline static int32_t get_offset_of__inventorySystem_0() { return static_cast<int32_t>(offsetof(AddSpellsCheatCommand_t82FD966051B1B85FE769FC1137874BB39F67C60F, ____inventorySystem_0)); }
	inline InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 * get__inventorySystem_0() const { return ____inventorySystem_0; }
	inline InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 ** get_address_of__inventorySystem_0() { return &____inventorySystem_0; }
	inline void set__inventorySystem_0(InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 * value)
	{
		____inventorySystem_0 = value;
		Il2CppCodeGenWriteBarrier((&____inventorySystem_0), value);
	}

	inline static int32_t get_offset_of__voSaver_1() { return static_cast<int32_t>(offsetof(AddSpellsCheatCommand_t82FD966051B1B85FE769FC1137874BB39F67C60F, ____voSaver_1)); }
	inline VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * get__voSaver_1() const { return ____voSaver_1; }
	inline VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F ** get_address_of__voSaver_1() { return &____voSaver_1; }
	inline void set__voSaver_1(VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * value)
	{
		____voSaver_1 = value;
		Il2CppCodeGenWriteBarrier((&____voSaver_1), value);
	}

	inline static int32_t get_offset_of__inventoryVO_2() { return static_cast<int32_t>(offsetof(AddSpellsCheatCommand_t82FD966051B1B85FE769FC1137874BB39F67C60F, ____inventoryVO_2)); }
	inline InventoryVO_tC7F0305FC930782BD2E4A47E3BD0504F0DCC6AD9 * get__inventoryVO_2() const { return ____inventoryVO_2; }
	inline InventoryVO_tC7F0305FC930782BD2E4A47E3BD0504F0DCC6AD9 ** get_address_of__inventoryVO_2() { return &____inventoryVO_2; }
	inline void set__inventoryVO_2(InventoryVO_tC7F0305FC930782BD2E4A47E3BD0504F0DCC6AD9 * value)
	{
		____inventoryVO_2 = value;
		Il2CppCodeGenWriteBarrier((&____inventoryVO_2), value);
	}

	inline static int32_t get_offset_of__battleSettingsSO_3() { return static_cast<int32_t>(offsetof(AddSpellsCheatCommand_t82FD966051B1B85FE769FC1137874BB39F67C60F, ____battleSettingsSO_3)); }
	inline BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE * get__battleSettingsSO_3() const { return ____battleSettingsSO_3; }
	inline BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE ** get_address_of__battleSettingsSO_3() { return &____battleSettingsSO_3; }
	inline void set__battleSettingsSO_3(BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE * value)
	{
		____battleSettingsSO_3 = value;
		Il2CppCodeGenWriteBarrier((&____battleSettingsSO_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADDSPELLSCHEATCOMMAND_T82FD966051B1B85FE769FC1137874BB39F67C60F_H
#ifndef AOEPROJECTILESPELLSYSTEM_T7F5B8C1B69D33C96BDA6433FA78CD6B8D79C3DF0_H
#define AOEPROJECTILESPELLSYSTEM_T7F5B8C1B69D33C96BDA6433FA78CD6B8D79C3DF0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AoeProjectileSpellSystem
struct  AoeProjectileSpellSystem_t7F5B8C1B69D33C96BDA6433FA78CD6B8D79C3DF0  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// SpellsSO AoeProjectileSpellSystem::_spellSO
	SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * ____spellSO_3;
	// GameContext AoeProjectileSpellSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_4;

public:
	inline static int32_t get_offset_of__spellSO_3() { return static_cast<int32_t>(offsetof(AoeProjectileSpellSystem_t7F5B8C1B69D33C96BDA6433FA78CD6B8D79C3DF0, ____spellSO_3)); }
	inline SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * get__spellSO_3() const { return ____spellSO_3; }
	inline SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 ** get_address_of__spellSO_3() { return &____spellSO_3; }
	inline void set__spellSO_3(SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * value)
	{
		____spellSO_3 = value;
		Il2CppCodeGenWriteBarrier((&____spellSO_3), value);
	}

	inline static int32_t get_offset_of__context_4() { return static_cast<int32_t>(offsetof(AoeProjectileSpellSystem_t7F5B8C1B69D33C96BDA6433FA78CD6B8D79C3DF0, ____context_4)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_4() const { return ____context_4; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_4() { return &____context_4; }
	inline void set__context_4(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_4 = value;
		Il2CppCodeGenWriteBarrier((&____context_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AOEPROJECTILESPELLSYSTEM_T7F5B8C1B69D33C96BDA6433FA78CD6B8D79C3DF0_H
#ifndef COMPANIONSPELLSYSTEM_T99E789E2AF35FEA8CCC51E566303551634100D01_H
#define COMPANIONSPELLSYSTEM_T99E789E2AF35FEA8CCC51E566303551634100D01_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CompanionSpellSystem
struct  CompanionSpellSystem_t99E789E2AF35FEA8CCC51E566303551634100D01  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// SpellsSO CompanionSpellSystem::_spellSO
	SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * ____spellSO_3;
	// UnitsSO CompanionSpellSystem::_unitsSO
	UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 * ____unitsSO_4;
	// GameContext CompanionSpellSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_5;

public:
	inline static int32_t get_offset_of__spellSO_3() { return static_cast<int32_t>(offsetof(CompanionSpellSystem_t99E789E2AF35FEA8CCC51E566303551634100D01, ____spellSO_3)); }
	inline SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * get__spellSO_3() const { return ____spellSO_3; }
	inline SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 ** get_address_of__spellSO_3() { return &____spellSO_3; }
	inline void set__spellSO_3(SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * value)
	{
		____spellSO_3 = value;
		Il2CppCodeGenWriteBarrier((&____spellSO_3), value);
	}

	inline static int32_t get_offset_of__unitsSO_4() { return static_cast<int32_t>(offsetof(CompanionSpellSystem_t99E789E2AF35FEA8CCC51E566303551634100D01, ____unitsSO_4)); }
	inline UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 * get__unitsSO_4() const { return ____unitsSO_4; }
	inline UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 ** get_address_of__unitsSO_4() { return &____unitsSO_4; }
	inline void set__unitsSO_4(UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 * value)
	{
		____unitsSO_4 = value;
		Il2CppCodeGenWriteBarrier((&____unitsSO_4), value);
	}

	inline static int32_t get_offset_of__context_5() { return static_cast<int32_t>(offsetof(CompanionSpellSystem_t99E789E2AF35FEA8CCC51E566303551634100D01, ____context_5)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_5() const { return ____context_5; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_5() { return &____context_5; }
	inline void set__context_5(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_5 = value;
		Il2CppCodeGenWriteBarrier((&____context_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPANIONSPELLSYSTEM_T99E789E2AF35FEA8CCC51E566303551634100D01_H
#ifndef CONSUMESPELLCOMMAND_TECBA7AA1F4987C7D973B7A472CF6AE49C9C17B2D_H
#define CONSUMESPELLCOMMAND_TECBA7AA1F4987C7D973B7A472CF6AE49C9C17B2D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ConsumeSpellCommand
struct  ConsumeSpellCommand_tECBA7AA1F4987C7D973B7A472CF6AE49C9C17B2D  : public BasicCommand_2_t17D98B836F02EB94D6FEDF45675409959DA9D577
{
public:
	// GameSettingsSO ConsumeSpellCommand::_gameSettingsSO
	GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C * ____gameSettingsSO_0;
	// InventorySystem ConsumeSpellCommand::_inventorySystem
	InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 * ____inventorySystem_1;
	// Tayr.VOSaver ConsumeSpellCommand::_voSaver
	VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * ____voSaver_2;
	// InventoryVO ConsumeSpellCommand::_inventoryVO
	InventoryVO_tC7F0305FC930782BD2E4A47E3BD0504F0DCC6AD9 * ____inventoryVO_3;

public:
	inline static int32_t get_offset_of__gameSettingsSO_0() { return static_cast<int32_t>(offsetof(ConsumeSpellCommand_tECBA7AA1F4987C7D973B7A472CF6AE49C9C17B2D, ____gameSettingsSO_0)); }
	inline GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C * get__gameSettingsSO_0() const { return ____gameSettingsSO_0; }
	inline GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C ** get_address_of__gameSettingsSO_0() { return &____gameSettingsSO_0; }
	inline void set__gameSettingsSO_0(GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C * value)
	{
		____gameSettingsSO_0 = value;
		Il2CppCodeGenWriteBarrier((&____gameSettingsSO_0), value);
	}

	inline static int32_t get_offset_of__inventorySystem_1() { return static_cast<int32_t>(offsetof(ConsumeSpellCommand_tECBA7AA1F4987C7D973B7A472CF6AE49C9C17B2D, ____inventorySystem_1)); }
	inline InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 * get__inventorySystem_1() const { return ____inventorySystem_1; }
	inline InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 ** get_address_of__inventorySystem_1() { return &____inventorySystem_1; }
	inline void set__inventorySystem_1(InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 * value)
	{
		____inventorySystem_1 = value;
		Il2CppCodeGenWriteBarrier((&____inventorySystem_1), value);
	}

	inline static int32_t get_offset_of__voSaver_2() { return static_cast<int32_t>(offsetof(ConsumeSpellCommand_tECBA7AA1F4987C7D973B7A472CF6AE49C9C17B2D, ____voSaver_2)); }
	inline VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * get__voSaver_2() const { return ____voSaver_2; }
	inline VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F ** get_address_of__voSaver_2() { return &____voSaver_2; }
	inline void set__voSaver_2(VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * value)
	{
		____voSaver_2 = value;
		Il2CppCodeGenWriteBarrier((&____voSaver_2), value);
	}

	inline static int32_t get_offset_of__inventoryVO_3() { return static_cast<int32_t>(offsetof(ConsumeSpellCommand_tECBA7AA1F4987C7D973B7A472CF6AE49C9C17B2D, ____inventoryVO_3)); }
	inline InventoryVO_tC7F0305FC930782BD2E4A47E3BD0504F0DCC6AD9 * get__inventoryVO_3() const { return ____inventoryVO_3; }
	inline InventoryVO_tC7F0305FC930782BD2E4A47E3BD0504F0DCC6AD9 ** get_address_of__inventoryVO_3() { return &____inventoryVO_3; }
	inline void set__inventoryVO_3(InventoryVO_tC7F0305FC930782BD2E4A47E3BD0504F0DCC6AD9 * value)
	{
		____inventoryVO_3 = value;
		Il2CppCodeGenWriteBarrier((&____inventoryVO_3), value);
	}
};

struct ConsumeSpellCommand_tECBA7AA1F4987C7D973B7A472CF6AE49C9C17B2D_StaticFields
{
public:
	// ConsumeSpellEvent ConsumeSpellCommand::ConsumeSpellCommandEvent
	ConsumeSpellEvent_t99A1EF82A5CC840F993D8568A4547414594DA516 * ___ConsumeSpellCommandEvent_4;

public:
	inline static int32_t get_offset_of_ConsumeSpellCommandEvent_4() { return static_cast<int32_t>(offsetof(ConsumeSpellCommand_tECBA7AA1F4987C7D973B7A472CF6AE49C9C17B2D_StaticFields, ___ConsumeSpellCommandEvent_4)); }
	inline ConsumeSpellEvent_t99A1EF82A5CC840F993D8568A4547414594DA516 * get_ConsumeSpellCommandEvent_4() const { return ___ConsumeSpellCommandEvent_4; }
	inline ConsumeSpellEvent_t99A1EF82A5CC840F993D8568A4547414594DA516 ** get_address_of_ConsumeSpellCommandEvent_4() { return &___ConsumeSpellCommandEvent_4; }
	inline void set_ConsumeSpellCommandEvent_4(ConsumeSpellEvent_t99A1EF82A5CC840F993D8568A4547414594DA516 * value)
	{
		___ConsumeSpellCommandEvent_4 = value;
		Il2CppCodeGenWriteBarrier((&___ConsumeSpellCommandEvent_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSUMESPELLCOMMAND_TECBA7AA1F4987C7D973B7A472CF6AE49C9C17B2D_H
#ifndef DECOYHPSYSTEM_T0AEE898AE1311AF423CE18B9025ADE3977E260AC_H
#define DECOYHPSYSTEM_T0AEE898AE1311AF423CE18B9025ADE3977E260AC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DecoyHPSystem
struct  DecoyHPSystem_t0AEE898AE1311AF423CE18B9025ADE3977E260AC  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// GameContext DecoyHPSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_3;

public:
	inline static int32_t get_offset_of__context_3() { return static_cast<int32_t>(offsetof(DecoyHPSystem_t0AEE898AE1311AF423CE18B9025ADE3977E260AC, ____context_3)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_3() const { return ____context_3; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_3() { return &____context_3; }
	inline void set__context_3(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_3 = value;
		Il2CppCodeGenWriteBarrier((&____context_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECOYHPSYSTEM_T0AEE898AE1311AF423CE18B9025ADE3977E260AC_H
#ifndef DECOYSPELLSYSTEM_T582E869AC6345726713035A6A5EACDD3E1085188_H
#define DECOYSPELLSYSTEM_T582E869AC6345726713035A6A5EACDD3E1085188_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DecoySpellSystem
struct  DecoySpellSystem_t582E869AC6345726713035A6A5EACDD3E1085188  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// SpellsSO DecoySpellSystem::_spellSO
	SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * ____spellSO_3;
	// UnitsSO DecoySpellSystem::_unitsSO
	UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 * ____unitsSO_4;
	// GameContext DecoySpellSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_5;

public:
	inline static int32_t get_offset_of__spellSO_3() { return static_cast<int32_t>(offsetof(DecoySpellSystem_t582E869AC6345726713035A6A5EACDD3E1085188, ____spellSO_3)); }
	inline SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * get__spellSO_3() const { return ____spellSO_3; }
	inline SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 ** get_address_of__spellSO_3() { return &____spellSO_3; }
	inline void set__spellSO_3(SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * value)
	{
		____spellSO_3 = value;
		Il2CppCodeGenWriteBarrier((&____spellSO_3), value);
	}

	inline static int32_t get_offset_of__unitsSO_4() { return static_cast<int32_t>(offsetof(DecoySpellSystem_t582E869AC6345726713035A6A5EACDD3E1085188, ____unitsSO_4)); }
	inline UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 * get__unitsSO_4() const { return ____unitsSO_4; }
	inline UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 ** get_address_of__unitsSO_4() { return &____unitsSO_4; }
	inline void set__unitsSO_4(UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 * value)
	{
		____unitsSO_4 = value;
		Il2CppCodeGenWriteBarrier((&____unitsSO_4), value);
	}

	inline static int32_t get_offset_of__context_5() { return static_cast<int32_t>(offsetof(DecoySpellSystem_t582E869AC6345726713035A6A5EACDD3E1085188, ____context_5)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_5() const { return ____context_5; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_5() { return &____context_5; }
	inline void set__context_5(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_5 = value;
		Il2CppCodeGenWriteBarrier((&____context_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECOYSPELLSYSTEM_T582E869AC6345726713035A6A5EACDD3E1085188_H
#ifndef FEATURE_T3D2CE4A6E0A21FB131F20D2F64B7A6C289F0CF0F_H
#define FEATURE_T3D2CE4A6E0A21FB131F20D2F64B7A6C289F0CF0F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Feature
struct  Feature_t3D2CE4A6E0A21FB131F20D2F64B7A6C289F0CF0F  : public Systems_t4C86DA84B6D6F50FF23B497DD599ED594274DEA9
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FEATURE_T3D2CE4A6E0A21FB131F20D2F64B7A6C289F0CF0F_H
#ifndef LEVELCHESTCOMMAND_TA24853FAD5DAB3B2047F39B8B9B827FF432229A6_H
#define LEVELCHESTCOMMAND_TA24853FAD5DAB3B2047F39B8B9B827FF432229A6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LevelChestCommand
struct  LevelChestCommand_tA24853FAD5DAB3B2047F39B8B9B827FF432229A6  : public BasicCommand_2_tEA933A19074E7EC45005BA02213FEC3BC998CEB7
{
public:
	// ChestModelSO LevelChestCommand::_starsChest
	ChestModelSO_t78F97E6AC8F488AD3C18236C4080E78CCF29B2A2 * ____starsChest_0;
	// TournamentsVO LevelChestCommand::_tournamentsVO
	TournamentsVO_tCBA9A235AE4069263CE332B1E9150CDABD0E8016 * ____tournamentsVO_1;
	// Tayr.VOSaver LevelChestCommand::_voSaver
	VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * ____voSaver_2;
	// InventorySystem LevelChestCommand::_inventorySystem
	InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 * ____inventorySystem_3;
	// InventoryVO LevelChestCommand::_inventoryVO
	InventoryVO_tC7F0305FC930782BD2E4A47E3BD0504F0DCC6AD9 * ____inventoryVO_4;

public:
	inline static int32_t get_offset_of__starsChest_0() { return static_cast<int32_t>(offsetof(LevelChestCommand_tA24853FAD5DAB3B2047F39B8B9B827FF432229A6, ____starsChest_0)); }
	inline ChestModelSO_t78F97E6AC8F488AD3C18236C4080E78CCF29B2A2 * get__starsChest_0() const { return ____starsChest_0; }
	inline ChestModelSO_t78F97E6AC8F488AD3C18236C4080E78CCF29B2A2 ** get_address_of__starsChest_0() { return &____starsChest_0; }
	inline void set__starsChest_0(ChestModelSO_t78F97E6AC8F488AD3C18236C4080E78CCF29B2A2 * value)
	{
		____starsChest_0 = value;
		Il2CppCodeGenWriteBarrier((&____starsChest_0), value);
	}

	inline static int32_t get_offset_of__tournamentsVO_1() { return static_cast<int32_t>(offsetof(LevelChestCommand_tA24853FAD5DAB3B2047F39B8B9B827FF432229A6, ____tournamentsVO_1)); }
	inline TournamentsVO_tCBA9A235AE4069263CE332B1E9150CDABD0E8016 * get__tournamentsVO_1() const { return ____tournamentsVO_1; }
	inline TournamentsVO_tCBA9A235AE4069263CE332B1E9150CDABD0E8016 ** get_address_of__tournamentsVO_1() { return &____tournamentsVO_1; }
	inline void set__tournamentsVO_1(TournamentsVO_tCBA9A235AE4069263CE332B1E9150CDABD0E8016 * value)
	{
		____tournamentsVO_1 = value;
		Il2CppCodeGenWriteBarrier((&____tournamentsVO_1), value);
	}

	inline static int32_t get_offset_of__voSaver_2() { return static_cast<int32_t>(offsetof(LevelChestCommand_tA24853FAD5DAB3B2047F39B8B9B827FF432229A6, ____voSaver_2)); }
	inline VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * get__voSaver_2() const { return ____voSaver_2; }
	inline VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F ** get_address_of__voSaver_2() { return &____voSaver_2; }
	inline void set__voSaver_2(VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * value)
	{
		____voSaver_2 = value;
		Il2CppCodeGenWriteBarrier((&____voSaver_2), value);
	}

	inline static int32_t get_offset_of__inventorySystem_3() { return static_cast<int32_t>(offsetof(LevelChestCommand_tA24853FAD5DAB3B2047F39B8B9B827FF432229A6, ____inventorySystem_3)); }
	inline InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 * get__inventorySystem_3() const { return ____inventorySystem_3; }
	inline InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 ** get_address_of__inventorySystem_3() { return &____inventorySystem_3; }
	inline void set__inventorySystem_3(InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 * value)
	{
		____inventorySystem_3 = value;
		Il2CppCodeGenWriteBarrier((&____inventorySystem_3), value);
	}

	inline static int32_t get_offset_of__inventoryVO_4() { return static_cast<int32_t>(offsetof(LevelChestCommand_tA24853FAD5DAB3B2047F39B8B9B827FF432229A6, ____inventoryVO_4)); }
	inline InventoryVO_tC7F0305FC930782BD2E4A47E3BD0504F0DCC6AD9 * get__inventoryVO_4() const { return ____inventoryVO_4; }
	inline InventoryVO_tC7F0305FC930782BD2E4A47E3BD0504F0DCC6AD9 ** get_address_of__inventoryVO_4() { return &____inventoryVO_4; }
	inline void set__inventoryVO_4(InventoryVO_tC7F0305FC930782BD2E4A47E3BD0504F0DCC6AD9 * value)
	{
		____inventoryVO_4 = value;
		Il2CppCodeGenWriteBarrier((&____inventoryVO_4), value);
	}
};

struct LevelChestCommand_tA24853FAD5DAB3B2047F39B8B9B827FF432229A6_StaticFields
{
public:
	// LevelChestEvent LevelChestCommand::LevelChestCommandEvent
	LevelChestEvent_tE4A44711C3740FE5BCA5CBEAE24289CB248D4429 * ___LevelChestCommandEvent_5;

public:
	inline static int32_t get_offset_of_LevelChestCommandEvent_5() { return static_cast<int32_t>(offsetof(LevelChestCommand_tA24853FAD5DAB3B2047F39B8B9B827FF432229A6_StaticFields, ___LevelChestCommandEvent_5)); }
	inline LevelChestEvent_tE4A44711C3740FE5BCA5CBEAE24289CB248D4429 * get_LevelChestCommandEvent_5() const { return ___LevelChestCommandEvent_5; }
	inline LevelChestEvent_tE4A44711C3740FE5BCA5CBEAE24289CB248D4429 ** get_address_of_LevelChestCommandEvent_5() { return &___LevelChestCommandEvent_5; }
	inline void set_LevelChestCommandEvent_5(LevelChestEvent_tE4A44711C3740FE5BCA5CBEAE24289CB248D4429 * value)
	{
		___LevelChestCommandEvent_5 = value;
		Il2CppCodeGenWriteBarrier((&___LevelChestCommandEvent_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELCHESTCOMMAND_TA24853FAD5DAB3B2047F39B8B9B827FF432229A6_H
#ifndef LEVELLOSTCOMMAND_T703967448459CC96A9AF820AF2EADA55D00B4CE0_H
#define LEVELLOSTCOMMAND_T703967448459CC96A9AF820AF2EADA55D00B4CE0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LevelLostCommand
struct  LevelLostCommand_t703967448459CC96A9AF820AF2EADA55D00B4CE0  : public BasicCommand_2_tFA40288012C0693610100789FFCBCFF70F5306CA
{
public:
	// GameSettingsSO LevelLostCommand::_gameSettingsSO
	GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C * ____gameSettingsSO_0;
	// InventorySystem LevelLostCommand::_inventorySystem
	InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 * ____inventorySystem_1;
	// Tayr.VOSaver LevelLostCommand::_voSaver
	VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * ____voSaver_2;
	// InventoryVO LevelLostCommand::_inventoryVO
	InventoryVO_tC7F0305FC930782BD2E4A47E3BD0504F0DCC6AD9 * ____inventoryVO_3;

public:
	inline static int32_t get_offset_of__gameSettingsSO_0() { return static_cast<int32_t>(offsetof(LevelLostCommand_t703967448459CC96A9AF820AF2EADA55D00B4CE0, ____gameSettingsSO_0)); }
	inline GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C * get__gameSettingsSO_0() const { return ____gameSettingsSO_0; }
	inline GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C ** get_address_of__gameSettingsSO_0() { return &____gameSettingsSO_0; }
	inline void set__gameSettingsSO_0(GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C * value)
	{
		____gameSettingsSO_0 = value;
		Il2CppCodeGenWriteBarrier((&____gameSettingsSO_0), value);
	}

	inline static int32_t get_offset_of__inventorySystem_1() { return static_cast<int32_t>(offsetof(LevelLostCommand_t703967448459CC96A9AF820AF2EADA55D00B4CE0, ____inventorySystem_1)); }
	inline InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 * get__inventorySystem_1() const { return ____inventorySystem_1; }
	inline InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 ** get_address_of__inventorySystem_1() { return &____inventorySystem_1; }
	inline void set__inventorySystem_1(InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 * value)
	{
		____inventorySystem_1 = value;
		Il2CppCodeGenWriteBarrier((&____inventorySystem_1), value);
	}

	inline static int32_t get_offset_of__voSaver_2() { return static_cast<int32_t>(offsetof(LevelLostCommand_t703967448459CC96A9AF820AF2EADA55D00B4CE0, ____voSaver_2)); }
	inline VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * get__voSaver_2() const { return ____voSaver_2; }
	inline VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F ** get_address_of__voSaver_2() { return &____voSaver_2; }
	inline void set__voSaver_2(VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * value)
	{
		____voSaver_2 = value;
		Il2CppCodeGenWriteBarrier((&____voSaver_2), value);
	}

	inline static int32_t get_offset_of__inventoryVO_3() { return static_cast<int32_t>(offsetof(LevelLostCommand_t703967448459CC96A9AF820AF2EADA55D00B4CE0, ____inventoryVO_3)); }
	inline InventoryVO_tC7F0305FC930782BD2E4A47E3BD0504F0DCC6AD9 * get__inventoryVO_3() const { return ____inventoryVO_3; }
	inline InventoryVO_tC7F0305FC930782BD2E4A47E3BD0504F0DCC6AD9 ** get_address_of__inventoryVO_3() { return &____inventoryVO_3; }
	inline void set__inventoryVO_3(InventoryVO_tC7F0305FC930782BD2E4A47E3BD0504F0DCC6AD9 * value)
	{
		____inventoryVO_3 = value;
		Il2CppCodeGenWriteBarrier((&____inventoryVO_3), value);
	}
};

struct LevelLostCommand_t703967448459CC96A9AF820AF2EADA55D00B4CE0_StaticFields
{
public:
	// LevelLostEvent LevelLostCommand::LevelLostCommandEvent
	LevelLostEvent_t94C8D8FEA2B83A32F83B354B37B05B07139C1A48 * ___LevelLostCommandEvent_4;

public:
	inline static int32_t get_offset_of_LevelLostCommandEvent_4() { return static_cast<int32_t>(offsetof(LevelLostCommand_t703967448459CC96A9AF820AF2EADA55D00B4CE0_StaticFields, ___LevelLostCommandEvent_4)); }
	inline LevelLostEvent_t94C8D8FEA2B83A32F83B354B37B05B07139C1A48 * get_LevelLostCommandEvent_4() const { return ___LevelLostCommandEvent_4; }
	inline LevelLostEvent_t94C8D8FEA2B83A32F83B354B37B05B07139C1A48 ** get_address_of_LevelLostCommandEvent_4() { return &___LevelLostCommandEvent_4; }
	inline void set_LevelLostCommandEvent_4(LevelLostEvent_t94C8D8FEA2B83A32F83B354B37B05B07139C1A48 * value)
	{
		___LevelLostCommandEvent_4 = value;
		Il2CppCodeGenWriteBarrier((&___LevelLostCommandEvent_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELLOSTCOMMAND_T703967448459CC96A9AF820AF2EADA55D00B4CE0_H
#ifndef LEVELWONCOMMAND_T1A01664F3F5F3F67674ED6534C4AEB6AA2ACB496_H
#define LEVELWONCOMMAND_T1A01664F3F5F3F67674ED6534C4AEB6AA2ACB496_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LevelWonCommand
struct  LevelWonCommand_t1A01664F3F5F3F67674ED6534C4AEB6AA2ACB496  : public BasicCommand_2_tFCE53C296FEA5AEA83DBB18C4CE2BF2CE9C66D16
{
public:
	// UnitsSO LevelWonCommand::_unitsConf
	UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 * ____unitsConf_0;
	// UserVO LevelWonCommand::_userVO
	UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * ____userVO_1;
	// TournamentsVO LevelWonCommand::_tournamentsVO
	TournamentsVO_tCBA9A235AE4069263CE332B1E9150CDABD0E8016 * ____tournamentsVO_2;
	// Tayr.VOSaver LevelWonCommand::_voSaver
	VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * ____voSaver_3;
	// BattleSettingsSO LevelWonCommand::_battleSettingsSO
	BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE * ____battleSettingsSO_4;
	// InventorySystem LevelWonCommand::_inventorySystem
	InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 * ____inventorySystem_5;
	// InventoryVO LevelWonCommand::_inventoryVO
	InventoryVO_tC7F0305FC930782BD2E4A47E3BD0504F0DCC6AD9 * ____inventoryVO_6;

public:
	inline static int32_t get_offset_of__unitsConf_0() { return static_cast<int32_t>(offsetof(LevelWonCommand_t1A01664F3F5F3F67674ED6534C4AEB6AA2ACB496, ____unitsConf_0)); }
	inline UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 * get__unitsConf_0() const { return ____unitsConf_0; }
	inline UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 ** get_address_of__unitsConf_0() { return &____unitsConf_0; }
	inline void set__unitsConf_0(UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 * value)
	{
		____unitsConf_0 = value;
		Il2CppCodeGenWriteBarrier((&____unitsConf_0), value);
	}

	inline static int32_t get_offset_of__userVO_1() { return static_cast<int32_t>(offsetof(LevelWonCommand_t1A01664F3F5F3F67674ED6534C4AEB6AA2ACB496, ____userVO_1)); }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * get__userVO_1() const { return ____userVO_1; }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 ** get_address_of__userVO_1() { return &____userVO_1; }
	inline void set__userVO_1(UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * value)
	{
		____userVO_1 = value;
		Il2CppCodeGenWriteBarrier((&____userVO_1), value);
	}

	inline static int32_t get_offset_of__tournamentsVO_2() { return static_cast<int32_t>(offsetof(LevelWonCommand_t1A01664F3F5F3F67674ED6534C4AEB6AA2ACB496, ____tournamentsVO_2)); }
	inline TournamentsVO_tCBA9A235AE4069263CE332B1E9150CDABD0E8016 * get__tournamentsVO_2() const { return ____tournamentsVO_2; }
	inline TournamentsVO_tCBA9A235AE4069263CE332B1E9150CDABD0E8016 ** get_address_of__tournamentsVO_2() { return &____tournamentsVO_2; }
	inline void set__tournamentsVO_2(TournamentsVO_tCBA9A235AE4069263CE332B1E9150CDABD0E8016 * value)
	{
		____tournamentsVO_2 = value;
		Il2CppCodeGenWriteBarrier((&____tournamentsVO_2), value);
	}

	inline static int32_t get_offset_of__voSaver_3() { return static_cast<int32_t>(offsetof(LevelWonCommand_t1A01664F3F5F3F67674ED6534C4AEB6AA2ACB496, ____voSaver_3)); }
	inline VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * get__voSaver_3() const { return ____voSaver_3; }
	inline VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F ** get_address_of__voSaver_3() { return &____voSaver_3; }
	inline void set__voSaver_3(VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * value)
	{
		____voSaver_3 = value;
		Il2CppCodeGenWriteBarrier((&____voSaver_3), value);
	}

	inline static int32_t get_offset_of__battleSettingsSO_4() { return static_cast<int32_t>(offsetof(LevelWonCommand_t1A01664F3F5F3F67674ED6534C4AEB6AA2ACB496, ____battleSettingsSO_4)); }
	inline BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE * get__battleSettingsSO_4() const { return ____battleSettingsSO_4; }
	inline BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE ** get_address_of__battleSettingsSO_4() { return &____battleSettingsSO_4; }
	inline void set__battleSettingsSO_4(BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE * value)
	{
		____battleSettingsSO_4 = value;
		Il2CppCodeGenWriteBarrier((&____battleSettingsSO_4), value);
	}

	inline static int32_t get_offset_of__inventorySystem_5() { return static_cast<int32_t>(offsetof(LevelWonCommand_t1A01664F3F5F3F67674ED6534C4AEB6AA2ACB496, ____inventorySystem_5)); }
	inline InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 * get__inventorySystem_5() const { return ____inventorySystem_5; }
	inline InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 ** get_address_of__inventorySystem_5() { return &____inventorySystem_5; }
	inline void set__inventorySystem_5(InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 * value)
	{
		____inventorySystem_5 = value;
		Il2CppCodeGenWriteBarrier((&____inventorySystem_5), value);
	}

	inline static int32_t get_offset_of__inventoryVO_6() { return static_cast<int32_t>(offsetof(LevelWonCommand_t1A01664F3F5F3F67674ED6534C4AEB6AA2ACB496, ____inventoryVO_6)); }
	inline InventoryVO_tC7F0305FC930782BD2E4A47E3BD0504F0DCC6AD9 * get__inventoryVO_6() const { return ____inventoryVO_6; }
	inline InventoryVO_tC7F0305FC930782BD2E4A47E3BD0504F0DCC6AD9 ** get_address_of__inventoryVO_6() { return &____inventoryVO_6; }
	inline void set__inventoryVO_6(InventoryVO_tC7F0305FC930782BD2E4A47E3BD0504F0DCC6AD9 * value)
	{
		____inventoryVO_6 = value;
		Il2CppCodeGenWriteBarrier((&____inventoryVO_6), value);
	}
};

struct LevelWonCommand_t1A01664F3F5F3F67674ED6534C4AEB6AA2ACB496_StaticFields
{
public:
	// LevelWonEvent LevelWonCommand::LevelWonCommandEvent
	LevelWonEvent_t52CF9D205E167284E58802E830A1FD5897098944 * ___LevelWonCommandEvent_7;

public:
	inline static int32_t get_offset_of_LevelWonCommandEvent_7() { return static_cast<int32_t>(offsetof(LevelWonCommand_t1A01664F3F5F3F67674ED6534C4AEB6AA2ACB496_StaticFields, ___LevelWonCommandEvent_7)); }
	inline LevelWonEvent_t52CF9D205E167284E58802E830A1FD5897098944 * get_LevelWonCommandEvent_7() const { return ___LevelWonCommandEvent_7; }
	inline LevelWonEvent_t52CF9D205E167284E58802E830A1FD5897098944 ** get_address_of_LevelWonCommandEvent_7() { return &___LevelWonCommandEvent_7; }
	inline void set_LevelWonCommandEvent_7(LevelWonEvent_t52CF9D205E167284E58802E830A1FD5897098944 * value)
	{
		___LevelWonCommandEvent_7 = value;
		Il2CppCodeGenWriteBarrier((&___LevelWonCommandEvent_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELWONCOMMAND_T1A01664F3F5F3F67674ED6534C4AEB6AA2ACB496_H
#ifndef MELEESPELLSYSTEM_T03811FEA71702E3ABC712FF669F6F7C0E69574AB_H
#define MELEESPELLSYSTEM_T03811FEA71702E3ABC712FF669F6F7C0E69574AB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MeleeSpellSystem
struct  MeleeSpellSystem_t03811FEA71702E3ABC712FF669F6F7C0E69574AB  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// SpellsSO MeleeSpellSystem::_spellSO
	SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * ____spellSO_3;
	// SoundSO MeleeSpellSystem::_soundSO
	SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73 * ____soundSO_4;
	// GameContext MeleeSpellSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_5;

public:
	inline static int32_t get_offset_of__spellSO_3() { return static_cast<int32_t>(offsetof(MeleeSpellSystem_t03811FEA71702E3ABC712FF669F6F7C0E69574AB, ____spellSO_3)); }
	inline SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * get__spellSO_3() const { return ____spellSO_3; }
	inline SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 ** get_address_of__spellSO_3() { return &____spellSO_3; }
	inline void set__spellSO_3(SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * value)
	{
		____spellSO_3 = value;
		Il2CppCodeGenWriteBarrier((&____spellSO_3), value);
	}

	inline static int32_t get_offset_of__soundSO_4() { return static_cast<int32_t>(offsetof(MeleeSpellSystem_t03811FEA71702E3ABC712FF669F6F7C0E69574AB, ____soundSO_4)); }
	inline SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73 * get__soundSO_4() const { return ____soundSO_4; }
	inline SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73 ** get_address_of__soundSO_4() { return &____soundSO_4; }
	inline void set__soundSO_4(SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73 * value)
	{
		____soundSO_4 = value;
		Il2CppCodeGenWriteBarrier((&____soundSO_4), value);
	}

	inline static int32_t get_offset_of__context_5() { return static_cast<int32_t>(offsetof(MeleeSpellSystem_t03811FEA71702E3ABC712FF669F6F7C0E69574AB, ____context_5)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_5() const { return ____context_5; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_5() { return &____context_5; }
	inline void set__context_5(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_5 = value;
		Il2CppCodeGenWriteBarrier((&____context_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MELEESPELLSYSTEM_T03811FEA71702E3ABC712FF669F6F7C0E69574AB_H
#ifndef PROJECTILECLOSESTSYSTEM_T75FAE18763DDA4B13EDB82EF7E062CEE4B68143A_H
#define PROJECTILECLOSESTSYSTEM_T75FAE18763DDA4B13EDB82EF7E062CEE4B68143A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ProjectileClosestSystem
struct  ProjectileClosestSystem_t75FAE18763DDA4B13EDB82EF7E062CEE4B68143A  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// SpellsSO ProjectileClosestSystem::_spellSO
	SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * ____spellSO_3;
	// GameContext ProjectileClosestSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_4;

public:
	inline static int32_t get_offset_of__spellSO_3() { return static_cast<int32_t>(offsetof(ProjectileClosestSystem_t75FAE18763DDA4B13EDB82EF7E062CEE4B68143A, ____spellSO_3)); }
	inline SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * get__spellSO_3() const { return ____spellSO_3; }
	inline SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 ** get_address_of__spellSO_3() { return &____spellSO_3; }
	inline void set__spellSO_3(SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * value)
	{
		____spellSO_3 = value;
		Il2CppCodeGenWriteBarrier((&____spellSO_3), value);
	}

	inline static int32_t get_offset_of__context_4() { return static_cast<int32_t>(offsetof(ProjectileClosestSystem_t75FAE18763DDA4B13EDB82EF7E062CEE4B68143A, ____context_4)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_4() const { return ____context_4; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_4() { return &____context_4; }
	inline void set__context_4(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_4 = value;
		Il2CppCodeGenWriteBarrier((&____context_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROJECTILECLOSESTSYSTEM_T75FAE18763DDA4B13EDB82EF7E062CEE4B68143A_H
#ifndef PROJECTILESELECTEDSYSTEM_T712BCFECE1EA815B621E4744613F1CA3653C5E12_H
#define PROJECTILESELECTEDSYSTEM_T712BCFECE1EA815B621E4744613F1CA3653C5E12_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ProjectileSelectedSystem
struct  ProjectileSelectedSystem_t712BCFECE1EA815B621E4744613F1CA3653C5E12  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// SpellsSO ProjectileSelectedSystem::_spellSO
	SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * ____spellSO_3;
	// GameContext ProjectileSelectedSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_4;

public:
	inline static int32_t get_offset_of__spellSO_3() { return static_cast<int32_t>(offsetof(ProjectileSelectedSystem_t712BCFECE1EA815B621E4744613F1CA3653C5E12, ____spellSO_3)); }
	inline SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * get__spellSO_3() const { return ____spellSO_3; }
	inline SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 ** get_address_of__spellSO_3() { return &____spellSO_3; }
	inline void set__spellSO_3(SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * value)
	{
		____spellSO_3 = value;
		Il2CppCodeGenWriteBarrier((&____spellSO_3), value);
	}

	inline static int32_t get_offset_of__context_4() { return static_cast<int32_t>(offsetof(ProjectileSelectedSystem_t712BCFECE1EA815B621E4744613F1CA3653C5E12, ____context_4)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_4() const { return ____context_4; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_4() { return &____context_4; }
	inline void set__context_4(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_4 = value;
		Il2CppCodeGenWriteBarrier((&____context_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROJECTILESELECTEDSYSTEM_T712BCFECE1EA815B621E4744613F1CA3653C5E12_H
#ifndef PROJECTILETRAPSPELLSYSTEM_TF00A64F0F06E4BF25F0AF16542FFAAF20E267C28_H
#define PROJECTILETRAPSPELLSYSTEM_TF00A64F0F06E4BF25F0AF16542FFAAF20E267C28_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ProjectileTrapSpellSystem
struct  ProjectileTrapSpellSystem_tF00A64F0F06E4BF25F0AF16542FFAAF20E267C28  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// SpellsSO ProjectileTrapSpellSystem::_spellSO
	SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * ____spellSO_3;
	// GameContext ProjectileTrapSpellSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_4;

public:
	inline static int32_t get_offset_of__spellSO_3() { return static_cast<int32_t>(offsetof(ProjectileTrapSpellSystem_tF00A64F0F06E4BF25F0AF16542FFAAF20E267C28, ____spellSO_3)); }
	inline SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * get__spellSO_3() const { return ____spellSO_3; }
	inline SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 ** get_address_of__spellSO_3() { return &____spellSO_3; }
	inline void set__spellSO_3(SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * value)
	{
		____spellSO_3 = value;
		Il2CppCodeGenWriteBarrier((&____spellSO_3), value);
	}

	inline static int32_t get_offset_of__context_4() { return static_cast<int32_t>(offsetof(ProjectileTrapSpellSystem_tF00A64F0F06E4BF25F0AF16542FFAAF20E267C28, ____context_4)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_4() const { return ____context_4; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_4() { return &____context_4; }
	inline void set__context_4(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_4 = value;
		Il2CppCodeGenWriteBarrier((&____context_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROJECTILETRAPSPELLSYSTEM_TF00A64F0F06E4BF25F0AF16542FFAAF20E267C28_H
#ifndef PULSEHEALEDSYSTEM_TE4B065619C617119639DF249685939883CFE0A5A_H
#define PULSEHEALEDSYSTEM_TE4B065619C617119639DF249685939883CFE0A5A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PulseHealedSystem
struct  PulseHealedSystem_tE4B065619C617119639DF249685939883CFE0A5A  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// SpellsSO PulseHealedSystem::_spellSO
	SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * ____spellSO_3;
	// GameContext PulseHealedSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_4;

public:
	inline static int32_t get_offset_of__spellSO_3() { return static_cast<int32_t>(offsetof(PulseHealedSystem_tE4B065619C617119639DF249685939883CFE0A5A, ____spellSO_3)); }
	inline SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * get__spellSO_3() const { return ____spellSO_3; }
	inline SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 ** get_address_of__spellSO_3() { return &____spellSO_3; }
	inline void set__spellSO_3(SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * value)
	{
		____spellSO_3 = value;
		Il2CppCodeGenWriteBarrier((&____spellSO_3), value);
	}

	inline static int32_t get_offset_of__context_4() { return static_cast<int32_t>(offsetof(PulseHealedSystem_tE4B065619C617119639DF249685939883CFE0A5A, ____context_4)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_4() const { return ____context_4; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_4() { return &____context_4; }
	inline void set__context_4(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_4 = value;
		Il2CppCodeGenWriteBarrier((&____context_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PULSEHEALEDSYSTEM_TE4B065619C617119639DF249685939883CFE0A5A_H
#ifndef RAINFIRESPELLSYSTEM_TF7EA20CB38B0FCD1FC1B43D85D75EB1E8FC5428C_H
#define RAINFIRESPELLSYSTEM_TF7EA20CB38B0FCD1FC1B43D85D75EB1E8FC5428C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RainFireSpellSystem
struct  RainFireSpellSystem_tF7EA20CB38B0FCD1FC1B43D85D75EB1E8FC5428C  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// SpellsSO RainFireSpellSystem::_spellSO
	SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * ____spellSO_3;
	// GameContext RainFireSpellSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_4;

public:
	inline static int32_t get_offset_of__spellSO_3() { return static_cast<int32_t>(offsetof(RainFireSpellSystem_tF7EA20CB38B0FCD1FC1B43D85D75EB1E8FC5428C, ____spellSO_3)); }
	inline SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * get__spellSO_3() const { return ____spellSO_3; }
	inline SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 ** get_address_of__spellSO_3() { return &____spellSO_3; }
	inline void set__spellSO_3(SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * value)
	{
		____spellSO_3 = value;
		Il2CppCodeGenWriteBarrier((&____spellSO_3), value);
	}

	inline static int32_t get_offset_of__context_4() { return static_cast<int32_t>(offsetof(RainFireSpellSystem_tF7EA20CB38B0FCD1FC1B43D85D75EB1E8FC5428C, ____context_4)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_4() const { return ____context_4; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_4() { return &____context_4; }
	inline void set__context_4(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_4 = value;
		Il2CppCodeGenWriteBarrier((&____context_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAINFIRESPELLSYSTEM_TF7EA20CB38B0FCD1FC1B43D85D75EB1E8FC5428C_H
#ifndef REVIVECOMMAND_T9DCA210E868E14B66BB5A0BD8C4F3080A6D63C3B_H
#define REVIVECOMMAND_T9DCA210E868E14B66BB5A0BD8C4F3080A6D63C3B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReviveCommand
struct  ReviveCommand_t9DCA210E868E14B66BB5A0BD8C4F3080A6D63C3B  : public BasicCommand_2_t8EA644DCD7371D25EC39CE9C1487E6348F50035D
{
public:
	// InventorySystem ReviveCommand::_inventorySystem
	InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 * ____inventorySystem_0;
	// Tayr.VOSaver ReviveCommand::_voSaver
	VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * ____voSaver_1;
	// InventoryVO ReviveCommand::_inventoryVO
	InventoryVO_tC7F0305FC930782BD2E4A47E3BD0504F0DCC6AD9 * ____inventoryVO_2;
	// BattleSettingsSO ReviveCommand::_battleSettingsSO
	BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE * ____battleSettingsSO_3;

public:
	inline static int32_t get_offset_of__inventorySystem_0() { return static_cast<int32_t>(offsetof(ReviveCommand_t9DCA210E868E14B66BB5A0BD8C4F3080A6D63C3B, ____inventorySystem_0)); }
	inline InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 * get__inventorySystem_0() const { return ____inventorySystem_0; }
	inline InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 ** get_address_of__inventorySystem_0() { return &____inventorySystem_0; }
	inline void set__inventorySystem_0(InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 * value)
	{
		____inventorySystem_0 = value;
		Il2CppCodeGenWriteBarrier((&____inventorySystem_0), value);
	}

	inline static int32_t get_offset_of__voSaver_1() { return static_cast<int32_t>(offsetof(ReviveCommand_t9DCA210E868E14B66BB5A0BD8C4F3080A6D63C3B, ____voSaver_1)); }
	inline VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * get__voSaver_1() const { return ____voSaver_1; }
	inline VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F ** get_address_of__voSaver_1() { return &____voSaver_1; }
	inline void set__voSaver_1(VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * value)
	{
		____voSaver_1 = value;
		Il2CppCodeGenWriteBarrier((&____voSaver_1), value);
	}

	inline static int32_t get_offset_of__inventoryVO_2() { return static_cast<int32_t>(offsetof(ReviveCommand_t9DCA210E868E14B66BB5A0BD8C4F3080A6D63C3B, ____inventoryVO_2)); }
	inline InventoryVO_tC7F0305FC930782BD2E4A47E3BD0504F0DCC6AD9 * get__inventoryVO_2() const { return ____inventoryVO_2; }
	inline InventoryVO_tC7F0305FC930782BD2E4A47E3BD0504F0DCC6AD9 ** get_address_of__inventoryVO_2() { return &____inventoryVO_2; }
	inline void set__inventoryVO_2(InventoryVO_tC7F0305FC930782BD2E4A47E3BD0504F0DCC6AD9 * value)
	{
		____inventoryVO_2 = value;
		Il2CppCodeGenWriteBarrier((&____inventoryVO_2), value);
	}

	inline static int32_t get_offset_of__battleSettingsSO_3() { return static_cast<int32_t>(offsetof(ReviveCommand_t9DCA210E868E14B66BB5A0BD8C4F3080A6D63C3B, ____battleSettingsSO_3)); }
	inline BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE * get__battleSettingsSO_3() const { return ____battleSettingsSO_3; }
	inline BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE ** get_address_of__battleSettingsSO_3() { return &____battleSettingsSO_3; }
	inline void set__battleSettingsSO_3(BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE * value)
	{
		____battleSettingsSO_3 = value;
		Il2CppCodeGenWriteBarrier((&____battleSettingsSO_3), value);
	}
};

struct ReviveCommand_t9DCA210E868E14B66BB5A0BD8C4F3080A6D63C3B_StaticFields
{
public:
	// ReviveCommandEvent ReviveCommand::ReviveCommandEvent
	ReviveCommandEvent_t5CCE33B3F24E5C83F3E608F719C653E55CDE4470 * ___ReviveCommandEvent_4;

public:
	inline static int32_t get_offset_of_ReviveCommandEvent_4() { return static_cast<int32_t>(offsetof(ReviveCommand_t9DCA210E868E14B66BB5A0BD8C4F3080A6D63C3B_StaticFields, ___ReviveCommandEvent_4)); }
	inline ReviveCommandEvent_t5CCE33B3F24E5C83F3E608F719C653E55CDE4470 * get_ReviveCommandEvent_4() const { return ___ReviveCommandEvent_4; }
	inline ReviveCommandEvent_t5CCE33B3F24E5C83F3E608F719C653E55CDE4470 ** get_address_of_ReviveCommandEvent_4() { return &___ReviveCommandEvent_4; }
	inline void set_ReviveCommandEvent_4(ReviveCommandEvent_t5CCE33B3F24E5C83F3E608F719C653E55CDE4470 * value)
	{
		___ReviveCommandEvent_4 = value;
		Il2CppCodeGenWriteBarrier((&___ReviveCommandEvent_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REVIVECOMMAND_T9DCA210E868E14B66BB5A0BD8C4F3080A6D63C3B_H
#ifndef SELFBUFFSPELLSYSTEM_TA1FEB6AEA8561A2E54F734CE25AC28CF3C0D83D7_H
#define SELFBUFFSPELLSYSTEM_TA1FEB6AEA8561A2E54F734CE25AC28CF3C0D83D7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SelfBuffSpellSystem
struct  SelfBuffSpellSystem_tA1FEB6AEA8561A2E54F734CE25AC28CF3C0D83D7  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// SpellsSO SelfBuffSpellSystem::_spellSO
	SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * ____spellSO_3;
	// GameContext SelfBuffSpellSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_4;

public:
	inline static int32_t get_offset_of__spellSO_3() { return static_cast<int32_t>(offsetof(SelfBuffSpellSystem_tA1FEB6AEA8561A2E54F734CE25AC28CF3C0D83D7, ____spellSO_3)); }
	inline SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * get__spellSO_3() const { return ____spellSO_3; }
	inline SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 ** get_address_of__spellSO_3() { return &____spellSO_3; }
	inline void set__spellSO_3(SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * value)
	{
		____spellSO_3 = value;
		Il2CppCodeGenWriteBarrier((&____spellSO_3), value);
	}

	inline static int32_t get_offset_of__context_4() { return static_cast<int32_t>(offsetof(SelfBuffSpellSystem_tA1FEB6AEA8561A2E54F734CE25AC28CF3C0D83D7, ____context_4)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_4() const { return ____context_4; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_4() { return &____context_4; }
	inline void set__context_4(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_4 = value;
		Il2CppCodeGenWriteBarrier((&____context_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELFBUFFSPELLSYSTEM_TA1FEB6AEA8561A2E54F734CE25AC28CF3C0D83D7_H
#ifndef SIMPLEBUFFSPELLSYSTEM_T8998549F5E0815F85C36A11A2FE338D3B0715643_H
#define SIMPLEBUFFSPELLSYSTEM_T8998549F5E0815F85C36A11A2FE338D3B0715643_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleBuffSpellSystem
struct  SimpleBuffSpellSystem_t8998549F5E0815F85C36A11A2FE338D3B0715643  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// SpellsSO SimpleBuffSpellSystem::_spellSO
	SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * ____spellSO_3;
	// GameContext SimpleBuffSpellSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_4;

public:
	inline static int32_t get_offset_of__spellSO_3() { return static_cast<int32_t>(offsetof(SimpleBuffSpellSystem_t8998549F5E0815F85C36A11A2FE338D3B0715643, ____spellSO_3)); }
	inline SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * get__spellSO_3() const { return ____spellSO_3; }
	inline SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 ** get_address_of__spellSO_3() { return &____spellSO_3; }
	inline void set__spellSO_3(SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * value)
	{
		____spellSO_3 = value;
		Il2CppCodeGenWriteBarrier((&____spellSO_3), value);
	}

	inline static int32_t get_offset_of__context_4() { return static_cast<int32_t>(offsetof(SimpleBuffSpellSystem_t8998549F5E0815F85C36A11A2FE338D3B0715643, ____context_4)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_4() const { return ____context_4; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_4() { return &____context_4; }
	inline void set__context_4(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_4 = value;
		Il2CppCodeGenWriteBarrier((&____context_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLEBUFFSPELLSYSTEM_T8998549F5E0815F85C36A11A2FE338D3B0715643_H
#ifndef SIMPLEPROJECTILESYSTEM_TDF8F60932B56B2B46FFF23B77E031DB33A5A7137_H
#define SIMPLEPROJECTILESYSTEM_TDF8F60932B56B2B46FFF23B77E031DB33A5A7137_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleProjectileSystem
struct  SimpleProjectileSystem_tDF8F60932B56B2B46FFF23B77E031DB33A5A7137  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// SpellsSO SimpleProjectileSystem::_spellSO
	SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * ____spellSO_3;
	// GameContext SimpleProjectileSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_4;

public:
	inline static int32_t get_offset_of__spellSO_3() { return static_cast<int32_t>(offsetof(SimpleProjectileSystem_tDF8F60932B56B2B46FFF23B77E031DB33A5A7137, ____spellSO_3)); }
	inline SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * get__spellSO_3() const { return ____spellSO_3; }
	inline SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 ** get_address_of__spellSO_3() { return &____spellSO_3; }
	inline void set__spellSO_3(SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * value)
	{
		____spellSO_3 = value;
		Il2CppCodeGenWriteBarrier((&____spellSO_3), value);
	}

	inline static int32_t get_offset_of__context_4() { return static_cast<int32_t>(offsetof(SimpleProjectileSystem_tDF8F60932B56B2B46FFF23B77E031DB33A5A7137, ____context_4)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_4() const { return ____context_4; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_4() { return &____context_4; }
	inline void set__context_4(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_4 = value;
		Il2CppCodeGenWriteBarrier((&____context_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLEPROJECTILESYSTEM_TDF8F60932B56B2B46FFF23B77E031DB33A5A7137_H
#ifndef SPELLINITIALIZESYSTEM_T1FEAF7FF0CB1BBE4A93D6A9212C4F8193AB7680B_H
#define SPELLINITIALIZESYSTEM_T1FEAF7FF0CB1BBE4A93D6A9212C4F8193AB7680B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpellInitializeSystem
struct  SpellInitializeSystem_t1FEAF7FF0CB1BBE4A93D6A9212C4F8193AB7680B  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// UnitsSO SpellInitializeSystem::_unitsSO
	UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 * ____unitsSO_3;
	// BattleSettingsSO SpellInitializeSystem::_battleSettingsSO
	BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE * ____battleSettingsSO_4;
	// GameContext SpellInitializeSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_5;

public:
	inline static int32_t get_offset_of__unitsSO_3() { return static_cast<int32_t>(offsetof(SpellInitializeSystem_t1FEAF7FF0CB1BBE4A93D6A9212C4F8193AB7680B, ____unitsSO_3)); }
	inline UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 * get__unitsSO_3() const { return ____unitsSO_3; }
	inline UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 ** get_address_of__unitsSO_3() { return &____unitsSO_3; }
	inline void set__unitsSO_3(UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 * value)
	{
		____unitsSO_3 = value;
		Il2CppCodeGenWriteBarrier((&____unitsSO_3), value);
	}

	inline static int32_t get_offset_of__battleSettingsSO_4() { return static_cast<int32_t>(offsetof(SpellInitializeSystem_t1FEAF7FF0CB1BBE4A93D6A9212C4F8193AB7680B, ____battleSettingsSO_4)); }
	inline BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE * get__battleSettingsSO_4() const { return ____battleSettingsSO_4; }
	inline BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE ** get_address_of__battleSettingsSO_4() { return &____battleSettingsSO_4; }
	inline void set__battleSettingsSO_4(BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE * value)
	{
		____battleSettingsSO_4 = value;
		Il2CppCodeGenWriteBarrier((&____battleSettingsSO_4), value);
	}

	inline static int32_t get_offset_of__context_5() { return static_cast<int32_t>(offsetof(SpellInitializeSystem_t1FEAF7FF0CB1BBE4A93D6A9212C4F8193AB7680B, ____context_5)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_5() const { return ____context_5; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_5() { return &____context_5; }
	inline void set__context_5(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_5 = value;
		Il2CppCodeGenWriteBarrier((&____context_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPELLINITIALIZESYSTEM_T1FEAF7FF0CB1BBE4A93D6A9212C4F8193AB7680B_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef TAPBOOSTERRECOGNIZERSYSTEM_T5D0F460E7F95A371DD06CFD0B7D4CC071294E034_H
#define TAPBOOSTERRECOGNIZERSYSTEM_T5D0F460E7F95A371DD06CFD0B7D4CC071294E034_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TapBoosterRecognizerSystem
struct  TapBoosterRecognizerSystem_t5D0F460E7F95A371DD06CFD0B7D4CC071294E034  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// SpellsSO TapBoosterRecognizerSystem::_spellsSO
	SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * ____spellsSO_3;
	// GameContext TapBoosterRecognizerSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_4;

public:
	inline static int32_t get_offset_of__spellsSO_3() { return static_cast<int32_t>(offsetof(TapBoosterRecognizerSystem_t5D0F460E7F95A371DD06CFD0B7D4CC071294E034, ____spellsSO_3)); }
	inline SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * get__spellsSO_3() const { return ____spellsSO_3; }
	inline SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 ** get_address_of__spellsSO_3() { return &____spellsSO_3; }
	inline void set__spellsSO_3(SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * value)
	{
		____spellsSO_3 = value;
		Il2CppCodeGenWriteBarrier((&____spellsSO_3), value);
	}

	inline static int32_t get_offset_of__context_4() { return static_cast<int32_t>(offsetof(TapBoosterRecognizerSystem_t5D0F460E7F95A371DD06CFD0B7D4CC071294E034, ____context_4)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_4() const { return ____context_4; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_4() { return &____context_4; }
	inline void set__context_4(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_4 = value;
		Il2CppCodeGenWriteBarrier((&____context_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAPBOOSTERRECOGNIZERSYSTEM_T5D0F460E7F95A371DD06CFD0B7D4CC071294E034_H
#ifndef TAPRECOGNIZERSYSTEM_T462545CB25F8C89AFE1809223C4444F0AD7C14AA_H
#define TAPRECOGNIZERSYSTEM_T462545CB25F8C89AFE1809223C4444F0AD7C14AA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TapRecognizerSystem
struct  TapRecognizerSystem_t462545CB25F8C89AFE1809223C4444F0AD7C14AA  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// SpellsSO TapRecognizerSystem::_spellsSO
	SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * ____spellsSO_3;
	// UnitsSO TapRecognizerSystem::_unitsSO
	UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 * ____unitsSO_4;
	// InventorySystem TapRecognizerSystem::_inventorySystem
	InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 * ____inventorySystem_5;
	// GameContext TapRecognizerSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_6;

public:
	inline static int32_t get_offset_of__spellsSO_3() { return static_cast<int32_t>(offsetof(TapRecognizerSystem_t462545CB25F8C89AFE1809223C4444F0AD7C14AA, ____spellsSO_3)); }
	inline SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * get__spellsSO_3() const { return ____spellsSO_3; }
	inline SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 ** get_address_of__spellsSO_3() { return &____spellsSO_3; }
	inline void set__spellsSO_3(SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * value)
	{
		____spellsSO_3 = value;
		Il2CppCodeGenWriteBarrier((&____spellsSO_3), value);
	}

	inline static int32_t get_offset_of__unitsSO_4() { return static_cast<int32_t>(offsetof(TapRecognizerSystem_t462545CB25F8C89AFE1809223C4444F0AD7C14AA, ____unitsSO_4)); }
	inline UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 * get__unitsSO_4() const { return ____unitsSO_4; }
	inline UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 ** get_address_of__unitsSO_4() { return &____unitsSO_4; }
	inline void set__unitsSO_4(UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 * value)
	{
		____unitsSO_4 = value;
		Il2CppCodeGenWriteBarrier((&____unitsSO_4), value);
	}

	inline static int32_t get_offset_of__inventorySystem_5() { return static_cast<int32_t>(offsetof(TapRecognizerSystem_t462545CB25F8C89AFE1809223C4444F0AD7C14AA, ____inventorySystem_5)); }
	inline InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 * get__inventorySystem_5() const { return ____inventorySystem_5; }
	inline InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 ** get_address_of__inventorySystem_5() { return &____inventorySystem_5; }
	inline void set__inventorySystem_5(InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 * value)
	{
		____inventorySystem_5 = value;
		Il2CppCodeGenWriteBarrier((&____inventorySystem_5), value);
	}

	inline static int32_t get_offset_of__context_6() { return static_cast<int32_t>(offsetof(TapRecognizerSystem_t462545CB25F8C89AFE1809223C4444F0AD7C14AA, ____context_6)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_6() const { return ____context_6; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_6() { return &____context_6; }
	inline void set__context_6(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_6 = value;
		Il2CppCodeGenWriteBarrier((&____context_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAPRECOGNIZERSYSTEM_T462545CB25F8C89AFE1809223C4444F0AD7C14AA_H
#ifndef TAPTAPNEARTOWERRECOGNIZERSYSTEM_T8B57B654D0955E7AE3FFA74E6DB826F64371DF96_H
#define TAPTAPNEARTOWERRECOGNIZERSYSTEM_T8B57B654D0955E7AE3FFA74E6DB826F64371DF96_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TapTapNearTowerRecognizerSystem
struct  TapTapNearTowerRecognizerSystem_t8B57B654D0955E7AE3FFA74E6DB826F64371DF96  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// SpellsSO TapTapNearTowerRecognizerSystem::_spellsSO
	SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * ____spellsSO_3;
	// InventorySystem TapTapNearTowerRecognizerSystem::_inventorySystem
	InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 * ____inventorySystem_4;
	// KickerManager TapTapNearTowerRecognizerSystem::_kickerManager
	KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA * ____kickerManager_5;
	// GameContext TapTapNearTowerRecognizerSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_6;

public:
	inline static int32_t get_offset_of__spellsSO_3() { return static_cast<int32_t>(offsetof(TapTapNearTowerRecognizerSystem_t8B57B654D0955E7AE3FFA74E6DB826F64371DF96, ____spellsSO_3)); }
	inline SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * get__spellsSO_3() const { return ____spellsSO_3; }
	inline SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 ** get_address_of__spellsSO_3() { return &____spellsSO_3; }
	inline void set__spellsSO_3(SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * value)
	{
		____spellsSO_3 = value;
		Il2CppCodeGenWriteBarrier((&____spellsSO_3), value);
	}

	inline static int32_t get_offset_of__inventorySystem_4() { return static_cast<int32_t>(offsetof(TapTapNearTowerRecognizerSystem_t8B57B654D0955E7AE3FFA74E6DB826F64371DF96, ____inventorySystem_4)); }
	inline InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 * get__inventorySystem_4() const { return ____inventorySystem_4; }
	inline InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 ** get_address_of__inventorySystem_4() { return &____inventorySystem_4; }
	inline void set__inventorySystem_4(InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 * value)
	{
		____inventorySystem_4 = value;
		Il2CppCodeGenWriteBarrier((&____inventorySystem_4), value);
	}

	inline static int32_t get_offset_of__kickerManager_5() { return static_cast<int32_t>(offsetof(TapTapNearTowerRecognizerSystem_t8B57B654D0955E7AE3FFA74E6DB826F64371DF96, ____kickerManager_5)); }
	inline KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA * get__kickerManager_5() const { return ____kickerManager_5; }
	inline KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA ** get_address_of__kickerManager_5() { return &____kickerManager_5; }
	inline void set__kickerManager_5(KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA * value)
	{
		____kickerManager_5 = value;
		Il2CppCodeGenWriteBarrier((&____kickerManager_5), value);
	}

	inline static int32_t get_offset_of__context_6() { return static_cast<int32_t>(offsetof(TapTapNearTowerRecognizerSystem_t8B57B654D0955E7AE3FFA74E6DB826F64371DF96, ____context_6)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_6() const { return ____context_6; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_6() { return &____context_6; }
	inline void set__context_6(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_6 = value;
		Il2CppCodeGenWriteBarrier((&____context_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAPTAPNEARTOWERRECOGNIZERSYSTEM_T8B57B654D0955E7AE3FFA74E6DB826F64371DF96_H
#ifndef TAPTAPTOWERRECOGNIZERSYSTEM_T1141F0D192034BB6E68561F07741008644A727C5_H
#define TAPTAPTOWERRECOGNIZERSYSTEM_T1141F0D192034BB6E68561F07741008644A727C5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TapTapTowerRecognizerSystem
struct  TapTapTowerRecognizerSystem_t1141F0D192034BB6E68561F07741008644A727C5  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// SpellsSO TapTapTowerRecognizerSystem::_spellsSO
	SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * ____spellsSO_3;
	// UnitsSO TapTapTowerRecognizerSystem::_unitsSO
	UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 * ____unitsSO_4;
	// InventorySystem TapTapTowerRecognizerSystem::_inventorySystem
	InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 * ____inventorySystem_5;
	// GameContext TapTapTowerRecognizerSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_6;

public:
	inline static int32_t get_offset_of__spellsSO_3() { return static_cast<int32_t>(offsetof(TapTapTowerRecognizerSystem_t1141F0D192034BB6E68561F07741008644A727C5, ____spellsSO_3)); }
	inline SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * get__spellsSO_3() const { return ____spellsSO_3; }
	inline SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 ** get_address_of__spellsSO_3() { return &____spellsSO_3; }
	inline void set__spellsSO_3(SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * value)
	{
		____spellsSO_3 = value;
		Il2CppCodeGenWriteBarrier((&____spellsSO_3), value);
	}

	inline static int32_t get_offset_of__unitsSO_4() { return static_cast<int32_t>(offsetof(TapTapTowerRecognizerSystem_t1141F0D192034BB6E68561F07741008644A727C5, ____unitsSO_4)); }
	inline UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 * get__unitsSO_4() const { return ____unitsSO_4; }
	inline UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 ** get_address_of__unitsSO_4() { return &____unitsSO_4; }
	inline void set__unitsSO_4(UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 * value)
	{
		____unitsSO_4 = value;
		Il2CppCodeGenWriteBarrier((&____unitsSO_4), value);
	}

	inline static int32_t get_offset_of__inventorySystem_5() { return static_cast<int32_t>(offsetof(TapTapTowerRecognizerSystem_t1141F0D192034BB6E68561F07741008644A727C5, ____inventorySystem_5)); }
	inline InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 * get__inventorySystem_5() const { return ____inventorySystem_5; }
	inline InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 ** get_address_of__inventorySystem_5() { return &____inventorySystem_5; }
	inline void set__inventorySystem_5(InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 * value)
	{
		____inventorySystem_5 = value;
		Il2CppCodeGenWriteBarrier((&____inventorySystem_5), value);
	}

	inline static int32_t get_offset_of__context_6() { return static_cast<int32_t>(offsetof(TapTapTowerRecognizerSystem_t1141F0D192034BB6E68561F07741008644A727C5, ____context_6)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_6() const { return ____context_6; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_6() { return &____context_6; }
	inline void set__context_6(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_6 = value;
		Il2CppCodeGenWriteBarrier((&____context_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAPTAPTOWERRECOGNIZERSYSTEM_T1141F0D192034BB6E68561F07741008644A727C5_H
#ifndef TOWERHPSYSTEM_T84316A8F9C617646AC3E5C22A27BB43115F204E6_H
#define TOWERHPSYSTEM_T84316A8F9C617646AC3E5C22A27BB43115F204E6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TowerHPSystem
struct  TowerHPSystem_t84316A8F9C617646AC3E5C22A27BB43115F204E6  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// GameContext TowerHPSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_3;

public:
	inline static int32_t get_offset_of__context_3() { return static_cast<int32_t>(offsetof(TowerHPSystem_t84316A8F9C617646AC3E5C22A27BB43115F204E6, ____context_3)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_3() const { return ____context_3; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_3() { return &____context_3; }
	inline void set__context_3(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_3 = value;
		Il2CppCodeGenWriteBarrier((&____context_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOWERHPSYSTEM_T84316A8F9C617646AC3E5C22A27BB43115F204E6_H
#ifndef TOWERSIMULATIONINITSYSTEM_T8BD7784BF9ADF591EE0C6E552324A68594209755_H
#define TOWERSIMULATIONINITSYSTEM_T8BD7784BF9ADF591EE0C6E552324A68594209755_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TowerSimulationInitSystem
struct  TowerSimulationInitSystem_t8BD7784BF9ADF591EE0C6E552324A68594209755  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// TowersSO TowerSimulationInitSystem::_towersSO
	TowersSO_t3B7DE821E5F8017950776EEFC42E680B361BCDF0 * ____towersSO_3;
	// GameContext TowerSimulationInitSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_4;

public:
	inline static int32_t get_offset_of__towersSO_3() { return static_cast<int32_t>(offsetof(TowerSimulationInitSystem_t8BD7784BF9ADF591EE0C6E552324A68594209755, ____towersSO_3)); }
	inline TowersSO_t3B7DE821E5F8017950776EEFC42E680B361BCDF0 * get__towersSO_3() const { return ____towersSO_3; }
	inline TowersSO_t3B7DE821E5F8017950776EEFC42E680B361BCDF0 ** get_address_of__towersSO_3() { return &____towersSO_3; }
	inline void set__towersSO_3(TowersSO_t3B7DE821E5F8017950776EEFC42E680B361BCDF0 * value)
	{
		____towersSO_3 = value;
		Il2CppCodeGenWriteBarrier((&____towersSO_3), value);
	}

	inline static int32_t get_offset_of__context_4() { return static_cast<int32_t>(offsetof(TowerSimulationInitSystem_t8BD7784BF9ADF591EE0C6E552324A68594209755, ____context_4)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_4() const { return ____context_4; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_4() { return &____context_4; }
	inline void set__context_4(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_4 = value;
		Il2CppCodeGenWriteBarrier((&____context_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOWERSIMULATIONINITSYSTEM_T8BD7784BF9ADF591EE0C6E552324A68594209755_H
#ifndef TRAPINITSYSTEM_TC9F18B169655A56BD6F5E2EFF80F9000DCA10921_H
#define TRAPINITSYSTEM_TC9F18B169655A56BD6F5E2EFF80F9000DCA10921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TrapInitSystem
struct  TrapInitSystem_tC9F18B169655A56BD6F5E2EFF80F9000DCA10921  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// TrapsSO TrapInitSystem::_trapsSO
	TrapsSO_tFF060852B3652DD05F33E1B19B8EC9166468D9C2 * ____trapsSO_3;
	// GameContext TrapInitSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_4;

public:
	inline static int32_t get_offset_of__trapsSO_3() { return static_cast<int32_t>(offsetof(TrapInitSystem_tC9F18B169655A56BD6F5E2EFF80F9000DCA10921, ____trapsSO_3)); }
	inline TrapsSO_tFF060852B3652DD05F33E1B19B8EC9166468D9C2 * get__trapsSO_3() const { return ____trapsSO_3; }
	inline TrapsSO_tFF060852B3652DD05F33E1B19B8EC9166468D9C2 ** get_address_of__trapsSO_3() { return &____trapsSO_3; }
	inline void set__trapsSO_3(TrapsSO_tFF060852B3652DD05F33E1B19B8EC9166468D9C2 * value)
	{
		____trapsSO_3 = value;
		Il2CppCodeGenWriteBarrier((&____trapsSO_3), value);
	}

	inline static int32_t get_offset_of__context_4() { return static_cast<int32_t>(offsetof(TrapInitSystem_tC9F18B169655A56BD6F5E2EFF80F9000DCA10921, ____context_4)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_4() const { return ____context_4; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_4() { return &____context_4; }
	inline void set__context_4(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_4 = value;
		Il2CppCodeGenWriteBarrier((&____context_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRAPINITSYSTEM_TC9F18B169655A56BD6F5E2EFF80F9000DCA10921_H
#ifndef UNITYEVENT_1_T0652FEB0149B16693CCB68FD6F3DAA7700BF71FF_H
#define UNITYEVENT_1_T0652FEB0149B16693CCB68FD6F3DAA7700BF71FF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<ConsumeSpellCommandResultModel>
struct  UnityEvent_1_t0652FEB0149B16693CCB68FD6F3DAA7700BF71FF  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_1_t0652FEB0149B16693CCB68FD6F3DAA7700BF71FF, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T0652FEB0149B16693CCB68FD6F3DAA7700BF71FF_H
#ifndef UNITYEVENT_1_T62450C330F1961131441F611B9AA7CB4914018C0_H
#define UNITYEVENT_1_T62450C330F1961131441F611B9AA7CB4914018C0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<LevelLostCommandResultModel>
struct  UnityEvent_1_t62450C330F1961131441F611B9AA7CB4914018C0  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_1_t62450C330F1961131441F611B9AA7CB4914018C0, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T62450C330F1961131441F611B9AA7CB4914018C0_H
#ifndef UNITYEVENT_1_TBA241D8685760C56245055C0FCA6171115DB0C2C_H
#define UNITYEVENT_1_TBA241D8685760C56245055C0FCA6171115DB0C2C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<LevelWonCommandResultModel>
struct  UnityEvent_1_tBA241D8685760C56245055C0FCA6171115DB0C2C  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_1_tBA241D8685760C56245055C0FCA6171115DB0C2C, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_TBA241D8685760C56245055C0FCA6171115DB0C2C_H
#ifndef UNITYEVENT_1_TA2400135A85AF1E3D12668B8E737F54C750DE1F5_H
#define UNITYEVENT_1_TA2400135A85AF1E3D12668B8E737F54C750DE1F5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<ReviveCommandResultModel>
struct  UnityEvent_1_tA2400135A85AF1E3D12668B8E737F54C750DE1F5  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_1_tA2400135A85AF1E3D12668B8E737F54C750DE1F5, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_TA2400135A85AF1E3D12668B8E737F54C750DE1F5_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef CONSUMESPELLEVENT_T99A1EF82A5CC840F993D8568A4547414594DA516_H
#define CONSUMESPELLEVENT_T99A1EF82A5CC840F993D8568A4547414594DA516_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ConsumeSpellEvent
struct  ConsumeSpellEvent_t99A1EF82A5CC840F993D8568A4547414594DA516  : public UnityEvent_1_t0652FEB0149B16693CCB68FD6F3DAA7700BF71FF
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSUMESPELLEVENT_T99A1EF82A5CC840F993D8568A4547414594DA516_H
#ifndef CREATETRAPAFTERTIMECOMPONENT_TF67C6E72F8FD0ED2B18CC70C37EAB23DAFECE6A9_H
#define CREATETRAPAFTERTIMECOMPONENT_TF67C6E72F8FD0ED2B18CC70C37EAB23DAFECE6A9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CreateTrapAfterTimeComponent
struct  CreateTrapAfterTimeComponent_tF67C6E72F8FD0ED2B18CC70C37EAB23DAFECE6A9  : public RuntimeObject
{
public:
	// System.Single CreateTrapAfterTimeComponent::TimeToCreate
	float ___TimeToCreate_0;
	// System.String CreateTrapAfterTimeComponent::TrapId
	String_t* ___TrapId_1;
	// UnityEngine.Vector3 CreateTrapAfterTimeComponent::Position
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___Position_2;
	// UnityEngine.Vector3 CreateTrapAfterTimeComponent::Rotation
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___Rotation_3;
	// System.Single CreateTrapAfterTimeComponent::Duration
	float ___Duration_4;

public:
	inline static int32_t get_offset_of_TimeToCreate_0() { return static_cast<int32_t>(offsetof(CreateTrapAfterTimeComponent_tF67C6E72F8FD0ED2B18CC70C37EAB23DAFECE6A9, ___TimeToCreate_0)); }
	inline float get_TimeToCreate_0() const { return ___TimeToCreate_0; }
	inline float* get_address_of_TimeToCreate_0() { return &___TimeToCreate_0; }
	inline void set_TimeToCreate_0(float value)
	{
		___TimeToCreate_0 = value;
	}

	inline static int32_t get_offset_of_TrapId_1() { return static_cast<int32_t>(offsetof(CreateTrapAfterTimeComponent_tF67C6E72F8FD0ED2B18CC70C37EAB23DAFECE6A9, ___TrapId_1)); }
	inline String_t* get_TrapId_1() const { return ___TrapId_1; }
	inline String_t** get_address_of_TrapId_1() { return &___TrapId_1; }
	inline void set_TrapId_1(String_t* value)
	{
		___TrapId_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrapId_1), value);
	}

	inline static int32_t get_offset_of_Position_2() { return static_cast<int32_t>(offsetof(CreateTrapAfterTimeComponent_tF67C6E72F8FD0ED2B18CC70C37EAB23DAFECE6A9, ___Position_2)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_Position_2() const { return ___Position_2; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_Position_2() { return &___Position_2; }
	inline void set_Position_2(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___Position_2 = value;
	}

	inline static int32_t get_offset_of_Rotation_3() { return static_cast<int32_t>(offsetof(CreateTrapAfterTimeComponent_tF67C6E72F8FD0ED2B18CC70C37EAB23DAFECE6A9, ___Rotation_3)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_Rotation_3() const { return ___Rotation_3; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_Rotation_3() { return &___Rotation_3; }
	inline void set_Rotation_3(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___Rotation_3 = value;
	}

	inline static int32_t get_offset_of_Duration_4() { return static_cast<int32_t>(offsetof(CreateTrapAfterTimeComponent_tF67C6E72F8FD0ED2B18CC70C37EAB23DAFECE6A9, ___Duration_4)); }
	inline float get_Duration_4() const { return ___Duration_4; }
	inline float* get_address_of_Duration_4() { return &___Duration_4; }
	inline void set_Duration_4(float value)
	{
		___Duration_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CREATETRAPAFTERTIMECOMPONENT_TF67C6E72F8FD0ED2B18CC70C37EAB23DAFECE6A9_H
#ifndef ERTUGRULFEATURE_TC4183E2BE6E3B2EDB7F366467E3D8E03B5A00484_H
#define ERTUGRULFEATURE_TC4183E2BE6E3B2EDB7F366467E3D8E03B5A00484_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ErtugrulFeature
struct  ErtugrulFeature_tC4183E2BE6E3B2EDB7F366467E3D8E03B5A00484  : public Feature_t3D2CE4A6E0A21FB131F20D2F64B7A6C289F0CF0F
{
public:
	// Zenject.DiContainer ErtugrulFeature::_diContainer
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____diContainer_4;
	// Contexts ErtugrulFeature::_contexts
	Contexts_t313FEE68C5FB0568E30785C33C0F6209F5F7B87D * ____contexts_5;

public:
	inline static int32_t get_offset_of__diContainer_4() { return static_cast<int32_t>(offsetof(ErtugrulFeature_tC4183E2BE6E3B2EDB7F366467E3D8E03B5A00484, ____diContainer_4)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__diContainer_4() const { return ____diContainer_4; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__diContainer_4() { return &____diContainer_4; }
	inline void set__diContainer_4(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____diContainer_4 = value;
		Il2CppCodeGenWriteBarrier((&____diContainer_4), value);
	}

	inline static int32_t get_offset_of__contexts_5() { return static_cast<int32_t>(offsetof(ErtugrulFeature_tC4183E2BE6E3B2EDB7F366467E3D8E03B5A00484, ____contexts_5)); }
	inline Contexts_t313FEE68C5FB0568E30785C33C0F6209F5F7B87D * get__contexts_5() const { return ____contexts_5; }
	inline Contexts_t313FEE68C5FB0568E30785C33C0F6209F5F7B87D ** get_address_of__contexts_5() { return &____contexts_5; }
	inline void set__contexts_5(Contexts_t313FEE68C5FB0568E30785C33C0F6209F5F7B87D * value)
	{
		____contexts_5 = value;
		Il2CppCodeGenWriteBarrier((&____contexts_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ERTUGRULFEATURE_TC4183E2BE6E3B2EDB7F366467E3D8E03B5A00484_H
#ifndef LEVELLOSTEVENT_T94C8D8FEA2B83A32F83B354B37B05B07139C1A48_H
#define LEVELLOSTEVENT_T94C8D8FEA2B83A32F83B354B37B05B07139C1A48_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LevelLostEvent
struct  LevelLostEvent_t94C8D8FEA2B83A32F83B354B37B05B07139C1A48  : public UnityEvent_1_t62450C330F1961131441F611B9AA7CB4914018C0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELLOSTEVENT_T94C8D8FEA2B83A32F83B354B37B05B07139C1A48_H
#ifndef LEVELWONEVENT_T52CF9D205E167284E58802E830A1FD5897098944_H
#define LEVELWONEVENT_T52CF9D205E167284E58802E830A1FD5897098944_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LevelWonEvent
struct  LevelWonEvent_t52CF9D205E167284E58802E830A1FD5897098944  : public UnityEvent_1_tBA241D8685760C56245055C0FCA6171115DB0C2C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELWONEVENT_T52CF9D205E167284E58802E830A1FD5897098944_H
#ifndef REVIVECOMMANDEVENT_T5CCE33B3F24E5C83F3E608F719C653E55CDE4470_H
#define REVIVECOMMANDEVENT_T5CCE33B3F24E5C83F3E608F719C653E55CDE4470_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReviveCommandEvent
struct  ReviveCommandEvent_t5CCE33B3F24E5C83F3E608F719C653E55CDE4470  : public UnityEvent_1_tA2400135A85AF1E3D12668B8E737F54C750DE1F5
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REVIVECOMMANDEVENT_T5CCE33B3F24E5C83F3E608F719C653E55CDE4470_H
#ifndef SPELLCONTROLLER_TF10DAA53E6D7502133083BF8AD162F9CBEE65643_H
#define SPELLCONTROLLER_TF10DAA53E6D7502133083BF8AD162F9CBEE65643_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpellController
struct  SpellController_tF10DAA53E6D7502133083BF8AD162F9CBEE65643 
{
public:
	// System.Int32 SpellController::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SpellController_tF10DAA53E6D7502133083BF8AD162F9CBEE65643, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPELLCONTROLLER_TF10DAA53E6D7502133083BF8AD162F9CBEE65643_H
#ifndef SPELLSTATUS_T74773B90ED28D30474EBE18DD4F0EDDD5676BAF2_H
#define SPELLSTATUS_T74773B90ED28D30474EBE18DD4F0EDDD5676BAF2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpellStatus
struct  SpellStatus_t74773B90ED28D30474EBE18DD4F0EDDD5676BAF2 
{
public:
	// System.Int32 SpellStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SpellStatus_t74773B90ED28D30474EBE18DD4F0EDDD5676BAF2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPELLSTATUS_T74773B90ED28D30474EBE18DD4F0EDDD5676BAF2_H
#ifndef TAPTAPGROUNDRECOGNIZERCOMPONENT_T207E4FE7146E5E9BC8D7E5DB27AFD96287159EBE_H
#define TAPTAPGROUNDRECOGNIZERCOMPONENT_T207E4FE7146E5E9BC8D7E5DB27AFD96287159EBE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TapTapGroundRecognizerComponent
struct  TapTapGroundRecognizerComponent_t207E4FE7146E5E9BC8D7E5DB27AFD96287159EBE  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 TapTapGroundRecognizerComponent::TargetPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___TargetPosition_0;

public:
	inline static int32_t get_offset_of_TargetPosition_0() { return static_cast<int32_t>(offsetof(TapTapGroundRecognizerComponent_t207E4FE7146E5E9BC8D7E5DB27AFD96287159EBE, ___TargetPosition_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_TargetPosition_0() const { return ___TargetPosition_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_TargetPosition_0() { return &___TargetPosition_0; }
	inline void set_TargetPosition_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___TargetPosition_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAPTAPGROUNDRECOGNIZERCOMPONENT_T207E4FE7146E5E9BC8D7E5DB27AFD96287159EBE_H
#ifndef REFREAHTYPE_TD4E50A740AAB4EA48BFF2B426369C53D543441DE_H
#define REFREAHTYPE_TD4E50A740AAB4EA48BFF2B426369C53D543441DE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.RefreahType
struct  RefreahType_tD4E50A740AAB4EA48BFF2B426369C53D543441DE 
{
public:
	// System.Int32 Tayr.RefreahType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RefreahType_tD4E50A740AAB4EA48BFF2B426369C53D543441DE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFREAHTYPE_TD4E50A740AAB4EA48BFF2B426369C53D543441DE_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef DEFENSESPELLCONTROLLERCOMPONENT_T4283F32282BCE9C8ABE66413E8B4906B6301C034_H
#define DEFENSESPELLCONTROLLERCOMPONENT_T4283F32282BCE9C8ABE66413E8B4906B6301C034_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DefenseSpellControllerComponent
struct  DefenseSpellControllerComponent_t4283F32282BCE9C8ABE66413E8B4906B6301C034  : public RuntimeObject
{
public:
	// SpellController DefenseSpellControllerComponent::SpellController
	int32_t ___SpellController_0;

public:
	inline static int32_t get_offset_of_SpellController_0() { return static_cast<int32_t>(offsetof(DefenseSpellControllerComponent_t4283F32282BCE9C8ABE66413E8B4906B6301C034, ___SpellController_0)); }
	inline int32_t get_SpellController_0() const { return ___SpellController_0; }
	inline int32_t* get_address_of_SpellController_0() { return &___SpellController_0; }
	inline void set_SpellController_0(int32_t value)
	{
		___SpellController_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFENSESPELLCONTROLLERCOMPONENT_T4283F32282BCE9C8ABE66413E8B4906B6301C034_H
#ifndef SIMULATIONFEATURE_TE452891FF1D5A4B359ED58C1EF1F7B2276A87EF2_H
#define SIMULATIONFEATURE_TE452891FF1D5A4B359ED58C1EF1F7B2276A87EF2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimulationFeature
struct  SimulationFeature_tE452891FF1D5A4B359ED58C1EF1F7B2276A87EF2  : public ErtugrulFeature_tC4183E2BE6E3B2EDB7F366467E3D8E03B5A00484
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMULATIONFEATURE_TE452891FF1D5A4B359ED58C1EF1F7B2276A87EF2_H
#ifndef SPELLCONTROLLERCOMPONENT_T936E0EB199CF01409A1D795A141996B3DA945DE3_H
#define SPELLCONTROLLERCOMPONENT_T936E0EB199CF01409A1D795A141996B3DA945DE3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpellControllerComponent
struct  SpellControllerComponent_t936E0EB199CF01409A1D795A141996B3DA945DE3  : public RuntimeObject
{
public:
	// SpellController SpellControllerComponent::SpellController
	int32_t ___SpellController_0;

public:
	inline static int32_t get_offset_of_SpellController_0() { return static_cast<int32_t>(offsetof(SpellControllerComponent_t936E0EB199CF01409A1D795A141996B3DA945DE3, ___SpellController_0)); }
	inline int32_t get_SpellController_0() const { return ___SpellController_0; }
	inline int32_t* get_address_of_SpellController_0() { return &___SpellController_0; }
	inline void set_SpellController_0(int32_t value)
	{
		___SpellController_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPELLCONTROLLERCOMPONENT_T936E0EB199CF01409A1D795A141996B3DA945DE3_H
#ifndef SPELLSTATUSCOMPONENT_TE0686D438F8B76E6D2CF301821D36FEA309F452E_H
#define SPELLSTATUSCOMPONENT_TE0686D438F8B76E6D2CF301821D36FEA309F452E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpellStatusComponent
struct  SpellStatusComponent_tE0686D438F8B76E6D2CF301821D36FEA309F452E  : public RuntimeObject
{
public:
	// SpellStatus SpellStatusComponent::SpellStatus
	int32_t ___SpellStatus_0;

public:
	inline static int32_t get_offset_of_SpellStatus_0() { return static_cast<int32_t>(offsetof(SpellStatusComponent_tE0686D438F8B76E6D2CF301821D36FEA309F452E, ___SpellStatus_0)); }
	inline int32_t get_SpellStatus_0() const { return ___SpellStatus_0; }
	inline int32_t* get_address_of_SpellStatus_0() { return &___SpellStatus_0; }
	inline void set_SpellStatus_0(int32_t value)
	{
		___SpellStatus_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPELLSTATUSCOMPONENT_TE0686D438F8B76E6D2CF301821D36FEA309F452E_H
#ifndef VARIABLE_T5D003723AEAA82A1E36B619BD98E937C05793BFD_H
#define VARIABLE_T5D003723AEAA82A1E36B619BD98E937C05793BFD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.Variable
struct  Variable_t5D003723AEAA82A1E36B619BD98E937C05793BFD  : public RuntimeObject
{
public:
	// Tayr.VariableChangedEvent Tayr.Variable::_onVariableChanged
	VariableChangedEvent_t553579619C432FFE7C7636199CE57457FA699BA4 * ____onVariableChanged_0;
	// System.Int32 Tayr.Variable::_value
	int32_t ____value_1;
	// Tayr.RefreahType Tayr.Variable::_refreshType
	int32_t ____refreshType_2;
	// System.Boolean Tayr.Variable::_isInitialized
	bool ____isInitialized_3;

public:
	inline static int32_t get_offset_of__onVariableChanged_0() { return static_cast<int32_t>(offsetof(Variable_t5D003723AEAA82A1E36B619BD98E937C05793BFD, ____onVariableChanged_0)); }
	inline VariableChangedEvent_t553579619C432FFE7C7636199CE57457FA699BA4 * get__onVariableChanged_0() const { return ____onVariableChanged_0; }
	inline VariableChangedEvent_t553579619C432FFE7C7636199CE57457FA699BA4 ** get_address_of__onVariableChanged_0() { return &____onVariableChanged_0; }
	inline void set__onVariableChanged_0(VariableChangedEvent_t553579619C432FFE7C7636199CE57457FA699BA4 * value)
	{
		____onVariableChanged_0 = value;
		Il2CppCodeGenWriteBarrier((&____onVariableChanged_0), value);
	}

	inline static int32_t get_offset_of__value_1() { return static_cast<int32_t>(offsetof(Variable_t5D003723AEAA82A1E36B619BD98E937C05793BFD, ____value_1)); }
	inline int32_t get__value_1() const { return ____value_1; }
	inline int32_t* get_address_of__value_1() { return &____value_1; }
	inline void set__value_1(int32_t value)
	{
		____value_1 = value;
	}

	inline static int32_t get_offset_of__refreshType_2() { return static_cast<int32_t>(offsetof(Variable_t5D003723AEAA82A1E36B619BD98E937C05793BFD, ____refreshType_2)); }
	inline int32_t get__refreshType_2() const { return ____refreshType_2; }
	inline int32_t* get_address_of__refreshType_2() { return &____refreshType_2; }
	inline void set__refreshType_2(int32_t value)
	{
		____refreshType_2 = value;
	}

	inline static int32_t get_offset_of__isInitialized_3() { return static_cast<int32_t>(offsetof(Variable_t5D003723AEAA82A1E36B619BD98E937C05793BFD, ____isInitialized_3)); }
	inline bool get__isInitialized_3() const { return ____isInitialized_3; }
	inline bool* get_address_of__isInitialized_3() { return &____isInitialized_3; }
	inline void set__isInitialized_3(bool value)
	{
		____isInitialized_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VARIABLE_T5D003723AEAA82A1E36B619BD98E937C05793BFD_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef LEVELVARIABLE_T8E3E27789809A8DFA12D41DB3927AC1DC90D4279_H
#define LEVELVARIABLE_T8E3E27789809A8DFA12D41DB3927AC1DC90D4279_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LevelVariable
struct  LevelVariable_t8E3E27789809A8DFA12D41DB3927AC1DC90D4279  : public Variable_t5D003723AEAA82A1E36B619BD98E937C05793BFD
{
public:
	// UserVO LevelVariable::_userVO
	UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * ____userVO_4;

public:
	inline static int32_t get_offset_of__userVO_4() { return static_cast<int32_t>(offsetof(LevelVariable_t8E3E27789809A8DFA12D41DB3927AC1DC90D4279, ____userVO_4)); }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * get__userVO_4() const { return ____userVO_4; }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 ** get_address_of__userVO_4() { return &____userVO_4; }
	inline void set__userVO_4(UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * value)
	{
		____userVO_4 = value;
		Il2CppCodeGenWriteBarrier((&____userVO_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELVARIABLE_T8E3E27789809A8DFA12D41DB3927AC1DC90D4279_H
#ifndef STARVARIABLE_T5610FC328147BA512296EC37D396F70C4D050676_H
#define STARVARIABLE_T5610FC328147BA512296EC37D396F70C4D050676_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StarVariable
struct  StarVariable_t5610FC328147BA512296EC37D396F70C4D050676  : public Variable_t5D003723AEAA82A1E36B619BD98E937C05793BFD
{
public:
	// UserVO StarVariable::_userVO
	UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * ____userVO_4;
	// TournamentsVO StarVariable::_tournamentsVO
	TournamentsVO_tCBA9A235AE4069263CE332B1E9150CDABD0E8016 * ____tournamentsVO_5;

public:
	inline static int32_t get_offset_of__userVO_4() { return static_cast<int32_t>(offsetof(StarVariable_t5610FC328147BA512296EC37D396F70C4D050676, ____userVO_4)); }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * get__userVO_4() const { return ____userVO_4; }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 ** get_address_of__userVO_4() { return &____userVO_4; }
	inline void set__userVO_4(UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * value)
	{
		____userVO_4 = value;
		Il2CppCodeGenWriteBarrier((&____userVO_4), value);
	}

	inline static int32_t get_offset_of__tournamentsVO_5() { return static_cast<int32_t>(offsetof(StarVariable_t5610FC328147BA512296EC37D396F70C4D050676, ____tournamentsVO_5)); }
	inline TournamentsVO_tCBA9A235AE4069263CE332B1E9150CDABD0E8016 * get__tournamentsVO_5() const { return ____tournamentsVO_5; }
	inline TournamentsVO_tCBA9A235AE4069263CE332B1E9150CDABD0E8016 ** get_address_of__tournamentsVO_5() { return &____tournamentsVO_5; }
	inline void set__tournamentsVO_5(TournamentsVO_tCBA9A235AE4069263CE332B1E9150CDABD0E8016 * value)
	{
		____tournamentsVO_5 = value;
		Il2CppCodeGenWriteBarrier((&____tournamentsVO_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STARVARIABLE_T5610FC328147BA512296EC37D396F70C4D050676_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef TMONOBEHAVIOUR_TC5EA4E5C9CF2F8B1360110F3DCB3CA86F7D68FF3_H
#define TMONOBEHAVIOUR_TC5EA4E5C9CF2F8B1360110F3DCB3CA86F7D68FF3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.TMonoBehaviour
struct  TMonoBehaviour_tC5EA4E5C9CF2F8B1360110F3DCB3CA86F7D68FF3  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMONOBEHAVIOUR_TC5EA4E5C9CF2F8B1360110F3DCB3CA86F7D68FF3_H
#ifndef CHESTCARDCOMPONENT_TFBA242D00DC22D455606C8608C021C39FE551E12_H
#define CHESTCARDCOMPONENT_TFBA242D00DC22D455606C8608C021C39FE551E12_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChestCardComponent
struct  ChestCardComponent_tFBA242D00DC22D455606C8608C021C39FE551E12  : public TMonoBehaviour_tC5EA4E5C9CF2F8B1360110F3DCB3CA86F7D68FF3
{
public:
	// TMPro.TextMeshProUGUI ChestCardComponent::_count
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____count_4;

public:
	inline static int32_t get_offset_of__count_4() { return static_cast<int32_t>(offsetof(ChestCardComponent_tFBA242D00DC22D455606C8608C021C39FE551E12, ____count_4)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__count_4() const { return ____count_4; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__count_4() { return &____count_4; }
	inline void set__count_4(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____count_4 = value;
		Il2CppCodeGenWriteBarrier((&____count_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHESTCARDCOMPONENT_TFBA242D00DC22D455606C8608C021C39FE551E12_H
#ifndef CHESTGOTCHACOMPONENT_TA318577CE122D49B2C8F7D8D9BCD3F5E2BB0EA45_H
#define CHESTGOTCHACOMPONENT_TA318577CE122D49B2C8F7D8D9BCD3F5E2BB0EA45_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChestGotchaComponent
struct  ChestGotchaComponent_tA318577CE122D49B2C8F7D8D9BCD3F5E2BB0EA45  : public TMonoBehaviour_tC5EA4E5C9CF2F8B1360110F3DCB3CA86F7D68FF3
{
public:
	// UnityEngine.Animator ChestGotchaComponent::_chestAnimator
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ____chestAnimator_4;
	// Tayr.ILibrary ChestGotchaComponent::_uiMain
	RuntimeObject* ____uiMain_5;
	// System.Collections.Generic.List`1<ItemContentModel> ChestGotchaComponent::_rewards
	List_1_t2C6CF342B96E130723D45845A1FD2053CBAF0F27 * ____rewards_6;
	// UnityEngine.GameObject ChestGotchaComponent::_lastCard
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____lastCard_7;
	// System.Int32 ChestGotchaComponent::_rewardIndex
	int32_t ____rewardIndex_8;

public:
	inline static int32_t get_offset_of__chestAnimator_4() { return static_cast<int32_t>(offsetof(ChestGotchaComponent_tA318577CE122D49B2C8F7D8D9BCD3F5E2BB0EA45, ____chestAnimator_4)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get__chestAnimator_4() const { return ____chestAnimator_4; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of__chestAnimator_4() { return &____chestAnimator_4; }
	inline void set__chestAnimator_4(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		____chestAnimator_4 = value;
		Il2CppCodeGenWriteBarrier((&____chestAnimator_4), value);
	}

	inline static int32_t get_offset_of__uiMain_5() { return static_cast<int32_t>(offsetof(ChestGotchaComponent_tA318577CE122D49B2C8F7D8D9BCD3F5E2BB0EA45, ____uiMain_5)); }
	inline RuntimeObject* get__uiMain_5() const { return ____uiMain_5; }
	inline RuntimeObject** get_address_of__uiMain_5() { return &____uiMain_5; }
	inline void set__uiMain_5(RuntimeObject* value)
	{
		____uiMain_5 = value;
		Il2CppCodeGenWriteBarrier((&____uiMain_5), value);
	}

	inline static int32_t get_offset_of__rewards_6() { return static_cast<int32_t>(offsetof(ChestGotchaComponent_tA318577CE122D49B2C8F7D8D9BCD3F5E2BB0EA45, ____rewards_6)); }
	inline List_1_t2C6CF342B96E130723D45845A1FD2053CBAF0F27 * get__rewards_6() const { return ____rewards_6; }
	inline List_1_t2C6CF342B96E130723D45845A1FD2053CBAF0F27 ** get_address_of__rewards_6() { return &____rewards_6; }
	inline void set__rewards_6(List_1_t2C6CF342B96E130723D45845A1FD2053CBAF0F27 * value)
	{
		____rewards_6 = value;
		Il2CppCodeGenWriteBarrier((&____rewards_6), value);
	}

	inline static int32_t get_offset_of__lastCard_7() { return static_cast<int32_t>(offsetof(ChestGotchaComponent_tA318577CE122D49B2C8F7D8D9BCD3F5E2BB0EA45, ____lastCard_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__lastCard_7() const { return ____lastCard_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__lastCard_7() { return &____lastCard_7; }
	inline void set__lastCard_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____lastCard_7 = value;
		Il2CppCodeGenWriteBarrier((&____lastCard_7), value);
	}

	inline static int32_t get_offset_of__rewardIndex_8() { return static_cast<int32_t>(offsetof(ChestGotchaComponent_tA318577CE122D49B2C8F7D8D9BCD3F5E2BB0EA45, ____rewardIndex_8)); }
	inline int32_t get__rewardIndex_8() const { return ____rewardIndex_8; }
	inline int32_t* get_address_of__rewardIndex_8() { return &____rewardIndex_8; }
	inline void set__rewardIndex_8(int32_t value)
	{
		____rewardIndex_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHESTGOTCHACOMPONENT_TA318577CE122D49B2C8F7D8D9BCD3F5E2BB0EA45_H
#ifndef LEVELCHESTCOMPONENT_T72D74F7CC3B050957B6074881BD7D1E8B0ABB2E6_H
#define LEVELCHESTCOMPONENT_T72D74F7CC3B050957B6074881BD7D1E8B0ABB2E6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LevelChestComponent
struct  LevelChestComponent_t72D74F7CC3B050957B6074881BD7D1E8B0ABB2E6  : public TMonoBehaviour_tC5EA4E5C9CF2F8B1360110F3DCB3CA86F7D68FF3
{
public:
	// UnityEngine.UI.Image LevelChestComponent::_progress
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____progress_4;
	// TMPro.TextMeshProUGUI LevelChestComponent::_progressText
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____progressText_5;
	// UnityEngine.UI.Image LevelChestComponent::_progressOpen
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____progressOpen_6;
	// TMPro.TextMeshProUGUI LevelChestComponent::_progressTextOpen
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____progressTextOpen_7;
	// UnityEngine.UI.Button LevelChestComponent::_openChest
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____openChest_8;
	// Tayr.ILibrary LevelChestComponent::_uiMain
	RuntimeObject* ____uiMain_9;
	// Zenject.DiContainer LevelChestComponent::_diContainer
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____diContainer_10;
	// TournamentsVO LevelChestComponent::_tournamentsVO
	TournamentsVO_tCBA9A235AE4069263CE332B1E9150CDABD0E8016 * ____tournamentsVO_11;
	// LevelVariable LevelChestComponent::_levelVariable
	LevelVariable_t8E3E27789809A8DFA12D41DB3927AC1DC90D4279 * ____levelVariable_12;
	// Quest LevelChestComponent::_starQuest
	Quest_tBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021 * ____starQuest_13;

public:
	inline static int32_t get_offset_of__progress_4() { return static_cast<int32_t>(offsetof(LevelChestComponent_t72D74F7CC3B050957B6074881BD7D1E8B0ABB2E6, ____progress_4)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__progress_4() const { return ____progress_4; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__progress_4() { return &____progress_4; }
	inline void set__progress_4(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____progress_4 = value;
		Il2CppCodeGenWriteBarrier((&____progress_4), value);
	}

	inline static int32_t get_offset_of__progressText_5() { return static_cast<int32_t>(offsetof(LevelChestComponent_t72D74F7CC3B050957B6074881BD7D1E8B0ABB2E6, ____progressText_5)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__progressText_5() const { return ____progressText_5; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__progressText_5() { return &____progressText_5; }
	inline void set__progressText_5(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____progressText_5 = value;
		Il2CppCodeGenWriteBarrier((&____progressText_5), value);
	}

	inline static int32_t get_offset_of__progressOpen_6() { return static_cast<int32_t>(offsetof(LevelChestComponent_t72D74F7CC3B050957B6074881BD7D1E8B0ABB2E6, ____progressOpen_6)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__progressOpen_6() const { return ____progressOpen_6; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__progressOpen_6() { return &____progressOpen_6; }
	inline void set__progressOpen_6(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____progressOpen_6 = value;
		Il2CppCodeGenWriteBarrier((&____progressOpen_6), value);
	}

	inline static int32_t get_offset_of__progressTextOpen_7() { return static_cast<int32_t>(offsetof(LevelChestComponent_t72D74F7CC3B050957B6074881BD7D1E8B0ABB2E6, ____progressTextOpen_7)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__progressTextOpen_7() const { return ____progressTextOpen_7; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__progressTextOpen_7() { return &____progressTextOpen_7; }
	inline void set__progressTextOpen_7(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____progressTextOpen_7 = value;
		Il2CppCodeGenWriteBarrier((&____progressTextOpen_7), value);
	}

	inline static int32_t get_offset_of__openChest_8() { return static_cast<int32_t>(offsetof(LevelChestComponent_t72D74F7CC3B050957B6074881BD7D1E8B0ABB2E6, ____openChest_8)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__openChest_8() const { return ____openChest_8; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__openChest_8() { return &____openChest_8; }
	inline void set__openChest_8(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____openChest_8 = value;
		Il2CppCodeGenWriteBarrier((&____openChest_8), value);
	}

	inline static int32_t get_offset_of__uiMain_9() { return static_cast<int32_t>(offsetof(LevelChestComponent_t72D74F7CC3B050957B6074881BD7D1E8B0ABB2E6, ____uiMain_9)); }
	inline RuntimeObject* get__uiMain_9() const { return ____uiMain_9; }
	inline RuntimeObject** get_address_of__uiMain_9() { return &____uiMain_9; }
	inline void set__uiMain_9(RuntimeObject* value)
	{
		____uiMain_9 = value;
		Il2CppCodeGenWriteBarrier((&____uiMain_9), value);
	}

	inline static int32_t get_offset_of__diContainer_10() { return static_cast<int32_t>(offsetof(LevelChestComponent_t72D74F7CC3B050957B6074881BD7D1E8B0ABB2E6, ____diContainer_10)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__diContainer_10() const { return ____diContainer_10; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__diContainer_10() { return &____diContainer_10; }
	inline void set__diContainer_10(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____diContainer_10 = value;
		Il2CppCodeGenWriteBarrier((&____diContainer_10), value);
	}

	inline static int32_t get_offset_of__tournamentsVO_11() { return static_cast<int32_t>(offsetof(LevelChestComponent_t72D74F7CC3B050957B6074881BD7D1E8B0ABB2E6, ____tournamentsVO_11)); }
	inline TournamentsVO_tCBA9A235AE4069263CE332B1E9150CDABD0E8016 * get__tournamentsVO_11() const { return ____tournamentsVO_11; }
	inline TournamentsVO_tCBA9A235AE4069263CE332B1E9150CDABD0E8016 ** get_address_of__tournamentsVO_11() { return &____tournamentsVO_11; }
	inline void set__tournamentsVO_11(TournamentsVO_tCBA9A235AE4069263CE332B1E9150CDABD0E8016 * value)
	{
		____tournamentsVO_11 = value;
		Il2CppCodeGenWriteBarrier((&____tournamentsVO_11), value);
	}

	inline static int32_t get_offset_of__levelVariable_12() { return static_cast<int32_t>(offsetof(LevelChestComponent_t72D74F7CC3B050957B6074881BD7D1E8B0ABB2E6, ____levelVariable_12)); }
	inline LevelVariable_t8E3E27789809A8DFA12D41DB3927AC1DC90D4279 * get__levelVariable_12() const { return ____levelVariable_12; }
	inline LevelVariable_t8E3E27789809A8DFA12D41DB3927AC1DC90D4279 ** get_address_of__levelVariable_12() { return &____levelVariable_12; }
	inline void set__levelVariable_12(LevelVariable_t8E3E27789809A8DFA12D41DB3927AC1DC90D4279 * value)
	{
		____levelVariable_12 = value;
		Il2CppCodeGenWriteBarrier((&____levelVariable_12), value);
	}

	inline static int32_t get_offset_of__starQuest_13() { return static_cast<int32_t>(offsetof(LevelChestComponent_t72D74F7CC3B050957B6074881BD7D1E8B0ABB2E6, ____starQuest_13)); }
	inline Quest_tBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021 * get__starQuest_13() const { return ____starQuest_13; }
	inline Quest_tBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021 ** get_address_of__starQuest_13() { return &____starQuest_13; }
	inline void set__starQuest_13(Quest_tBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021 * value)
	{
		____starQuest_13 = value;
		Il2CppCodeGenWriteBarrier((&____starQuest_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELCHESTCOMPONENT_T72D74F7CC3B050957B6074881BD7D1E8B0ABB2E6_H
#ifndef STARCHESTCOMPONENT_T8ADEE7CB2C641D2EB6AD4D08E19B94D627A99508_H
#define STARCHESTCOMPONENT_T8ADEE7CB2C641D2EB6AD4D08E19B94D627A99508_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StarChestComponent
struct  StarChestComponent_t8ADEE7CB2C641D2EB6AD4D08E19B94D627A99508  : public TMonoBehaviour_tC5EA4E5C9CF2F8B1360110F3DCB3CA86F7D68FF3
{
public:
	// UnityEngine.UI.Image StarChestComponent::_progress
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____progress_4;
	// TMPro.TextMeshProUGUI StarChestComponent::_progressText
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____progressText_5;
	// UnityEngine.UI.Image StarChestComponent::_progressOpen
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____progressOpen_6;
	// TMPro.TextMeshProUGUI StarChestComponent::_progressTextOpen
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____progressTextOpen_7;
	// UnityEngine.UI.Button StarChestComponent::_openChest
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____openChest_8;
	// Zenject.DiContainer StarChestComponent::_diContainer
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____diContainer_9;
	// Tayr.ILibrary StarChestComponent::_uiMain
	RuntimeObject* ____uiMain_10;
	// UserEvents StarChestComponent::_userEvents
	UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * ____userEvents_11;
	// UserVO StarChestComponent::_userVO
	UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * ____userVO_12;
	// StarVariable StarChestComponent::_starVariable
	StarVariable_t5610FC328147BA512296EC37D396F70C4D050676 * ____starVariable_13;

public:
	inline static int32_t get_offset_of__progress_4() { return static_cast<int32_t>(offsetof(StarChestComponent_t8ADEE7CB2C641D2EB6AD4D08E19B94D627A99508, ____progress_4)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__progress_4() const { return ____progress_4; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__progress_4() { return &____progress_4; }
	inline void set__progress_4(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____progress_4 = value;
		Il2CppCodeGenWriteBarrier((&____progress_4), value);
	}

	inline static int32_t get_offset_of__progressText_5() { return static_cast<int32_t>(offsetof(StarChestComponent_t8ADEE7CB2C641D2EB6AD4D08E19B94D627A99508, ____progressText_5)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__progressText_5() const { return ____progressText_5; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__progressText_5() { return &____progressText_5; }
	inline void set__progressText_5(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____progressText_5 = value;
		Il2CppCodeGenWriteBarrier((&____progressText_5), value);
	}

	inline static int32_t get_offset_of__progressOpen_6() { return static_cast<int32_t>(offsetof(StarChestComponent_t8ADEE7CB2C641D2EB6AD4D08E19B94D627A99508, ____progressOpen_6)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__progressOpen_6() const { return ____progressOpen_6; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__progressOpen_6() { return &____progressOpen_6; }
	inline void set__progressOpen_6(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____progressOpen_6 = value;
		Il2CppCodeGenWriteBarrier((&____progressOpen_6), value);
	}

	inline static int32_t get_offset_of__progressTextOpen_7() { return static_cast<int32_t>(offsetof(StarChestComponent_t8ADEE7CB2C641D2EB6AD4D08E19B94D627A99508, ____progressTextOpen_7)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__progressTextOpen_7() const { return ____progressTextOpen_7; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__progressTextOpen_7() { return &____progressTextOpen_7; }
	inline void set__progressTextOpen_7(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____progressTextOpen_7 = value;
		Il2CppCodeGenWriteBarrier((&____progressTextOpen_7), value);
	}

	inline static int32_t get_offset_of__openChest_8() { return static_cast<int32_t>(offsetof(StarChestComponent_t8ADEE7CB2C641D2EB6AD4D08E19B94D627A99508, ____openChest_8)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__openChest_8() const { return ____openChest_8; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__openChest_8() { return &____openChest_8; }
	inline void set__openChest_8(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____openChest_8 = value;
		Il2CppCodeGenWriteBarrier((&____openChest_8), value);
	}

	inline static int32_t get_offset_of__diContainer_9() { return static_cast<int32_t>(offsetof(StarChestComponent_t8ADEE7CB2C641D2EB6AD4D08E19B94D627A99508, ____diContainer_9)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__diContainer_9() const { return ____diContainer_9; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__diContainer_9() { return &____diContainer_9; }
	inline void set__diContainer_9(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____diContainer_9 = value;
		Il2CppCodeGenWriteBarrier((&____diContainer_9), value);
	}

	inline static int32_t get_offset_of__uiMain_10() { return static_cast<int32_t>(offsetof(StarChestComponent_t8ADEE7CB2C641D2EB6AD4D08E19B94D627A99508, ____uiMain_10)); }
	inline RuntimeObject* get__uiMain_10() const { return ____uiMain_10; }
	inline RuntimeObject** get_address_of__uiMain_10() { return &____uiMain_10; }
	inline void set__uiMain_10(RuntimeObject* value)
	{
		____uiMain_10 = value;
		Il2CppCodeGenWriteBarrier((&____uiMain_10), value);
	}

	inline static int32_t get_offset_of__userEvents_11() { return static_cast<int32_t>(offsetof(StarChestComponent_t8ADEE7CB2C641D2EB6AD4D08E19B94D627A99508, ____userEvents_11)); }
	inline UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * get__userEvents_11() const { return ____userEvents_11; }
	inline UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 ** get_address_of__userEvents_11() { return &____userEvents_11; }
	inline void set__userEvents_11(UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * value)
	{
		____userEvents_11 = value;
		Il2CppCodeGenWriteBarrier((&____userEvents_11), value);
	}

	inline static int32_t get_offset_of__userVO_12() { return static_cast<int32_t>(offsetof(StarChestComponent_t8ADEE7CB2C641D2EB6AD4D08E19B94D627A99508, ____userVO_12)); }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * get__userVO_12() const { return ____userVO_12; }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 ** get_address_of__userVO_12() { return &____userVO_12; }
	inline void set__userVO_12(UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * value)
	{
		____userVO_12 = value;
		Il2CppCodeGenWriteBarrier((&____userVO_12), value);
	}

	inline static int32_t get_offset_of__starVariable_13() { return static_cast<int32_t>(offsetof(StarChestComponent_t8ADEE7CB2C641D2EB6AD4D08E19B94D627A99508, ____starVariable_13)); }
	inline StarVariable_t5610FC328147BA512296EC37D396F70C4D050676 * get__starVariable_13() const { return ____starVariable_13; }
	inline StarVariable_t5610FC328147BA512296EC37D396F70C4D050676 ** get_address_of__starVariable_13() { return &____starVariable_13; }
	inline void set__starVariable_13(StarVariable_t5610FC328147BA512296EC37D396F70C4D050676 * value)
	{
		____starVariable_13 = value;
		Il2CppCodeGenWriteBarrier((&____starVariable_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STARCHESTCOMPONENT_T8ADEE7CB2C641D2EB6AD4D08E19B94D627A99508_H
#ifndef BASICNODE_1_T71BE851FBBAFC8D36139BFC87691573E29FA8ED0_H
#define BASICNODE_1_T71BE851FBBAFC8D36139BFC87691573E29FA8ED0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.BasicNode`1<GotchaNodeData>
struct  BasicNode_1_t71BE851FBBAFC8D36139BFC87691573E29FA8ED0  : public TMonoBehaviour_tC5EA4E5C9CF2F8B1360110F3DCB3CA86F7D68FF3
{
public:
	// T Tayr.BasicNode`1::Data
	GotchaNodeData_t7B74A837B128AFAF6F873873674CC2D974DB1EEB * ___Data_4;
	// Tayr.INodeAnimationHandler Tayr.BasicNode`1::_nodeAnimationHandler
	RuntimeObject* ____nodeAnimationHandler_5;
	// Tayr.NodeAnimator Tayr.BasicNode`1::_nodeAnimator
	NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F * ____nodeAnimator_6;

public:
	inline static int32_t get_offset_of_Data_4() { return static_cast<int32_t>(offsetof(BasicNode_1_t71BE851FBBAFC8D36139BFC87691573E29FA8ED0, ___Data_4)); }
	inline GotchaNodeData_t7B74A837B128AFAF6F873873674CC2D974DB1EEB * get_Data_4() const { return ___Data_4; }
	inline GotchaNodeData_t7B74A837B128AFAF6F873873674CC2D974DB1EEB ** get_address_of_Data_4() { return &___Data_4; }
	inline void set_Data_4(GotchaNodeData_t7B74A837B128AFAF6F873873674CC2D974DB1EEB * value)
	{
		___Data_4 = value;
		Il2CppCodeGenWriteBarrier((&___Data_4), value);
	}

	inline static int32_t get_offset_of__nodeAnimationHandler_5() { return static_cast<int32_t>(offsetof(BasicNode_1_t71BE851FBBAFC8D36139BFC87691573E29FA8ED0, ____nodeAnimationHandler_5)); }
	inline RuntimeObject* get__nodeAnimationHandler_5() const { return ____nodeAnimationHandler_5; }
	inline RuntimeObject** get_address_of__nodeAnimationHandler_5() { return &____nodeAnimationHandler_5; }
	inline void set__nodeAnimationHandler_5(RuntimeObject* value)
	{
		____nodeAnimationHandler_5 = value;
		Il2CppCodeGenWriteBarrier((&____nodeAnimationHandler_5), value);
	}

	inline static int32_t get_offset_of__nodeAnimator_6() { return static_cast<int32_t>(offsetof(BasicNode_1_t71BE851FBBAFC8D36139BFC87691573E29FA8ED0, ____nodeAnimator_6)); }
	inline NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F * get__nodeAnimator_6() const { return ____nodeAnimator_6; }
	inline NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F ** get_address_of__nodeAnimator_6() { return &____nodeAnimator_6; }
	inline void set__nodeAnimator_6(NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F * value)
	{
		____nodeAnimator_6 = value;
		Il2CppCodeGenWriteBarrier((&____nodeAnimator_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICNODE_1_T71BE851FBBAFC8D36139BFC87691573E29FA8ED0_H
#ifndef BASICGOTCHANODE_TC38E11ED54EEB8CDB9613BDD5CA7A78395D5F9EC_H
#define BASICGOTCHANODE_TC38E11ED54EEB8CDB9613BDD5CA7A78395D5F9EC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BasicGotchaNode
struct  BasicGotchaNode_tC38E11ED54EEB8CDB9613BDD5CA7A78395D5F9EC  : public BasicNode_1_t71BE851FBBAFC8D36139BFC87691573E29FA8ED0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICGOTCHANODE_TC38E11ED54EEB8CDB9613BDD5CA7A78395D5F9EC_H
#ifndef GOLDGOTCHANODE_TD301E7AAB8037C9718720DA60F3A662E9D574BAE_H
#define GOLDGOTCHANODE_TD301E7AAB8037C9718720DA60F3A662E9D574BAE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoldGotchaNode
struct  GoldGotchaNode_tD301E7AAB8037C9718720DA60F3A662E9D574BAE  : public BasicGotchaNode_tC38E11ED54EEB8CDB9613BDD5CA7A78395D5F9EC
{
public:
	// ChestGotchaComponent GoldGotchaNode::_chestGotchaComponent
	ChestGotchaComponent_tA318577CE122D49B2C8F7D8D9BCD3F5E2BB0EA45 * ____chestGotchaComponent_7;

public:
	inline static int32_t get_offset_of__chestGotchaComponent_7() { return static_cast<int32_t>(offsetof(GoldGotchaNode_tD301E7AAB8037C9718720DA60F3A662E9D574BAE, ____chestGotchaComponent_7)); }
	inline ChestGotchaComponent_tA318577CE122D49B2C8F7D8D9BCD3F5E2BB0EA45 * get__chestGotchaComponent_7() const { return ____chestGotchaComponent_7; }
	inline ChestGotchaComponent_tA318577CE122D49B2C8F7D8D9BCD3F5E2BB0EA45 ** get_address_of__chestGotchaComponent_7() { return &____chestGotchaComponent_7; }
	inline void set__chestGotchaComponent_7(ChestGotchaComponent_tA318577CE122D49B2C8F7D8D9BCD3F5E2BB0EA45 * value)
	{
		____chestGotchaComponent_7 = value;
		Il2CppCodeGenWriteBarrier((&____chestGotchaComponent_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GOLDGOTCHANODE_TD301E7AAB8037C9718720DA60F3A662E9D574BAE_H
#ifndef SILVERGOTCHANODE_TAD996AF28A32DEC9F70F89E37C6635D3B9634CC6_H
#define SILVERGOTCHANODE_TAD996AF28A32DEC9F70F89E37C6635D3B9634CC6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SilverGotchaNode
struct  SilverGotchaNode_tAD996AF28A32DEC9F70F89E37C6635D3B9634CC6  : public BasicGotchaNode_tC38E11ED54EEB8CDB9613BDD5CA7A78395D5F9EC
{
public:
	// ChestGotchaComponent SilverGotchaNode::_chestGotchaComponent
	ChestGotchaComponent_tA318577CE122D49B2C8F7D8D9BCD3F5E2BB0EA45 * ____chestGotchaComponent_7;

public:
	inline static int32_t get_offset_of__chestGotchaComponent_7() { return static_cast<int32_t>(offsetof(SilverGotchaNode_tAD996AF28A32DEC9F70F89E37C6635D3B9634CC6, ____chestGotchaComponent_7)); }
	inline ChestGotchaComponent_tA318577CE122D49B2C8F7D8D9BCD3F5E2BB0EA45 * get__chestGotchaComponent_7() const { return ____chestGotchaComponent_7; }
	inline ChestGotchaComponent_tA318577CE122D49B2C8F7D8D9BCD3F5E2BB0EA45 ** get_address_of__chestGotchaComponent_7() { return &____chestGotchaComponent_7; }
	inline void set__chestGotchaComponent_7(ChestGotchaComponent_tA318577CE122D49B2C8F7D8D9BCD3F5E2BB0EA45 * value)
	{
		____chestGotchaComponent_7 = value;
		Il2CppCodeGenWriteBarrier((&____chestGotchaComponent_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SILVERGOTCHANODE_TAD996AF28A32DEC9F70F89E37C6635D3B9634CC6_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8300 = { sizeof (ProjectileMovementSystem_t9B5DFF53BDDD117EC20BFF18FD35E54B638F6E10), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8300[3] = 
{
	ProjectileMovementSystem_t9B5DFF53BDDD117EC20BFF18FD35E54B638F6E10::get_offset_of__entities_0(),
	ProjectileMovementSystem_t9B5DFF53BDDD117EC20BFF18FD35E54B638F6E10::get_offset_of__context_1(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8301 = { sizeof (RaycastUtils_t75B4B4DA2EC9161B7E9694AA3B787525AEC1ABCA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8302 = { sizeof (TapTapTowerRecognizerComponent_t89827D4F7E4C2C288ACA589DD3798E442CB820E4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8302[1] = 
{
	TapTapTowerRecognizerComponent_t89827D4F7E4C2C288ACA589DD3798E442CB820E4::get_offset_of_TargetId_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8303 = { sizeof (TapTapGroundRecognizerComponent_t207E4FE7146E5E9BC8D7E5DB27AFD96287159EBE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8303[1] = 
{
	TapTapGroundRecognizerComponent_t207E4FE7146E5E9BC8D7E5DB27AFD96287159EBE::get_offset_of_TargetPosition_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8304 = { sizeof (TapBoosterRecognizerSystem_t5D0F460E7F95A371DD06CFD0B7D4CC071294E034), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8304[2] = 
{
	TapBoosterRecognizerSystem_t5D0F460E7F95A371DD06CFD0B7D4CC071294E034::get_offset_of__spellsSO_3(),
	TapBoosterRecognizerSystem_t5D0F460E7F95A371DD06CFD0B7D4CC071294E034::get_offset_of__context_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8305 = { sizeof (TapRecognizerSystem_t462545CB25F8C89AFE1809223C4444F0AD7C14AA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8305[4] = 
{
	TapRecognizerSystem_t462545CB25F8C89AFE1809223C4444F0AD7C14AA::get_offset_of__spellsSO_3(),
	TapRecognizerSystem_t462545CB25F8C89AFE1809223C4444F0AD7C14AA::get_offset_of__unitsSO_4(),
	TapRecognizerSystem_t462545CB25F8C89AFE1809223C4444F0AD7C14AA::get_offset_of__inventorySystem_5(),
	TapRecognizerSystem_t462545CB25F8C89AFE1809223C4444F0AD7C14AA::get_offset_of__context_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8306 = { sizeof (TapTapNearTowerRecognizerSystem_t8B57B654D0955E7AE3FFA74E6DB826F64371DF96), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8306[4] = 
{
	TapTapNearTowerRecognizerSystem_t8B57B654D0955E7AE3FFA74E6DB826F64371DF96::get_offset_of__spellsSO_3(),
	TapTapNearTowerRecognizerSystem_t8B57B654D0955E7AE3FFA74E6DB826F64371DF96::get_offset_of__inventorySystem_4(),
	TapTapNearTowerRecognizerSystem_t8B57B654D0955E7AE3FFA74E6DB826F64371DF96::get_offset_of__kickerManager_5(),
	TapTapNearTowerRecognizerSystem_t8B57B654D0955E7AE3FFA74E6DB826F64371DF96::get_offset_of__context_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8307 = { sizeof (TapTapTowerRecognizerSystem_t1141F0D192034BB6E68561F07741008644A727C5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8307[4] = 
{
	TapTapTowerRecognizerSystem_t1141F0D192034BB6E68561F07741008644A727C5::get_offset_of__spellsSO_3(),
	TapTapTowerRecognizerSystem_t1141F0D192034BB6E68561F07741008644A727C5::get_offset_of__unitsSO_4(),
	TapTapTowerRecognizerSystem_t1141F0D192034BB6E68561F07741008644A727C5::get_offset_of__inventorySystem_5(),
	TapTapTowerRecognizerSystem_t1141F0D192034BB6E68561F07741008644A727C5::get_offset_of__context_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8308 = { sizeof (SimulationFeature_tE452891FF1D5A4B359ED58C1EF1F7B2276A87EF2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8309 = { sizeof (LoopingAIComponent_t9AA3BF1DC9DA6157F248DD04289BEBABA54B1EBC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8309[2] = 
{
	LoopingAIComponent_t9AA3BF1DC9DA6157F248DD04289BEBABA54B1EBC::get_offset_of_LastLoopTime_0(),
	LoopingAIComponent_t9AA3BF1DC9DA6157F248DD04289BEBABA54B1EBC::get_offset_of_Frequency_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8310 = { sizeof (LoopingAISystem_t104C524417DE5C4BDFC9FB1AD4A1A57ED37F9B06), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8310[3] = 
{
	LoopingAISystem_t104C524417DE5C4BDFC9FB1AD4A1A57ED37F9B06::get_offset_of__spellsSO_0(),
	LoopingAISystem_t104C524417DE5C4BDFC9FB1AD4A1A57ED37F9B06::get_offset_of__entities_1(),
	LoopingAISystem_t104C524417DE5C4BDFC9FB1AD4A1A57ED37F9B06::get_offset_of__context_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8311 = { sizeof (PulseAIComponent_tF2952693A05C9C863A4F15B53411CDE887287D9E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8311[2] = 
{
	PulseAIComponent_tF2952693A05C9C863A4F15B53411CDE887287D9E::get_offset_of_LastPulseTime_0(),
	PulseAIComponent_tF2952693A05C9C863A4F15B53411CDE887287D9E::get_offset_of_Frequency_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8312 = { sizeof (PulseAISystem_tAEEA3660C9ADDF1E15251FAA417EB456E79FE402), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8312[3] = 
{
	PulseAISystem_tAEEA3660C9ADDF1E15251FAA417EB456E79FE402::get_offset_of__spellsSO_0(),
	PulseAISystem_tAEEA3660C9ADDF1E15251FAA417EB456E79FE402::get_offset_of__entities_1(),
	PulseAISystem_tAEEA3660C9ADDF1E15251FAA417EB456E79FE402::get_offset_of__context_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8313 = { sizeof (PulseHealedSystem_tE4B065619C617119639DF249685939883CFE0A5A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8313[2] = 
{
	PulseHealedSystem_tE4B065619C617119639DF249685939883CFE0A5A::get_offset_of__spellSO_3(),
	PulseHealedSystem_tE4B065619C617119639DF249685939883CFE0A5A::get_offset_of__context_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8314 = { sizeof (AnimatorTriggerComponent_tDC969D009A6F3EA063E87BA74E26FC7DB024F149), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8314[2] = 
{
	AnimatorTriggerComponent_tDC969D009A6F3EA063E87BA74E26FC7DB024F149::get_offset_of_TargetId_0(),
	AnimatorTriggerComponent_tDC969D009A6F3EA063E87BA74E26FC7DB024F149::get_offset_of_TriggerName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8315 = { sizeof (SpellComponent_t4675125CB9902D20CF5FE1D1BFE8D3C06B147A40), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8315[1] = 
{
	SpellComponent_t4675125CB9902D20CF5FE1D1BFE8D3C06B147A40::get_offset_of_SpellId_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8316 = { sizeof (SpellIndexComponent_tDF0FF7388ABB141C0263B711256C2C6912F523DF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8316[1] = 
{
	SpellIndexComponent_tDF0FF7388ABB141C0263B711256C2C6912F523DF::get_offset_of_Index_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8317 = { sizeof (SpellMetaComponent_tB4965B165064C7E469BFB468BFB0B2ED45329109), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8317[1] = 
{
	SpellMetaComponent_tB4965B165064C7E469BFB468BFB0B2ED45329109::get_offset_of_SpellId_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8318 = { sizeof (SpellStatusComponent_tE0686D438F8B76E6D2CF301821D36FEA309F452E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8318[1] = 
{
	SpellStatusComponent_tE0686D438F8B76E6D2CF301821D36FEA309F452E::get_offset_of_SpellStatus_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8319 = { sizeof (SpellControllerComponent_t936E0EB199CF01409A1D795A141996B3DA945DE3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8319[1] = 
{
	SpellControllerComponent_t936E0EB199CF01409A1D795A141996B3DA945DE3::get_offset_of_SpellController_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8320 = { sizeof (DefenseSpellComponent_tC6D1C273AF1ACE089CFA2FFCFBBB896DDE348936), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8320[1] = 
{
	DefenseSpellComponent_tC6D1C273AF1ACE089CFA2FFCFBBB896DDE348936::get_offset_of_SpellId_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8321 = { sizeof (DefenseSpellControllerComponent_t4283F32282BCE9C8ABE66413E8B4906B6301C034), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8321[1] = 
{
	DefenseSpellControllerComponent_t4283F32282BCE9C8ABE66413E8B4906B6301C034::get_offset_of_SpellController_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8322 = { sizeof (DefenseIdComponent_t273FEC2B955E7A431924573AEDD1622BFB4AB9D5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8322[1] = 
{
	DefenseIdComponent_t273FEC2B955E7A431924573AEDD1622BFB4AB9D5::get_offset_of_DefenseId_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8323 = { sizeof (BlockingSpell_tEC8ACE0D193BB3B7997CFE67A30BB0F454CB6007), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8324 = { sizeof (SpellStatus_t74773B90ED28D30474EBE18DD4F0EDDD5676BAF2)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable8324[3] = 
{
	SpellStatus_t74773B90ED28D30474EBE18DD4F0EDDD5676BAF2::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8325 = { sizeof (AoeComponent_tF428CE243F66E1239169CEBB7B3058DF28C85809), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8325[1] = 
{
	AoeComponent_tF428CE243F66E1239169CEBB7B3058DF28C85809::get_offset_of_Radius_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8326 = { sizeof (AoeProjectileSpellSystem_t7F5B8C1B69D33C96BDA6433FA78CD6B8D79C3DF0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8326[2] = 
{
	AoeProjectileSpellSystem_t7F5B8C1B69D33C96BDA6433FA78CD6B8D79C3DF0::get_offset_of__spellSO_3(),
	AoeProjectileSpellSystem_t7F5B8C1B69D33C96BDA6433FA78CD6B8D79C3DF0::get_offset_of__context_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8327 = { sizeof (SimpleBuffSpellSystem_t8998549F5E0815F85C36A11A2FE338D3B0715643), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8327[2] = 
{
	SimpleBuffSpellSystem_t8998549F5E0815F85C36A11A2FE338D3B0715643::get_offset_of__spellSO_3(),
	SimpleBuffSpellSystem_t8998549F5E0815F85C36A11A2FE338D3B0715643::get_offset_of__context_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8328 = { sizeof (SimpleProjectileSystem_tDF8F60932B56B2B46FFF23B77E031DB33A5A7137), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8328[2] = 
{
	SimpleProjectileSystem_tDF8F60932B56B2B46FFF23B77E031DB33A5A7137::get_offset_of__spellSO_3(),
	SimpleProjectileSystem_tDF8F60932B56B2B46FFF23B77E031DB33A5A7137::get_offset_of__context_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8329 = { sizeof (CreateTrapAfterTimeComponent_tF67C6E72F8FD0ED2B18CC70C37EAB23DAFECE6A9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8329[5] = 
{
	CreateTrapAfterTimeComponent_tF67C6E72F8FD0ED2B18CC70C37EAB23DAFECE6A9::get_offset_of_TimeToCreate_0(),
	CreateTrapAfterTimeComponent_tF67C6E72F8FD0ED2B18CC70C37EAB23DAFECE6A9::get_offset_of_TrapId_1(),
	CreateTrapAfterTimeComponent_tF67C6E72F8FD0ED2B18CC70C37EAB23DAFECE6A9::get_offset_of_Position_2(),
	CreateTrapAfterTimeComponent_tF67C6E72F8FD0ED2B18CC70C37EAB23DAFECE6A9::get_offset_of_Rotation_3(),
	CreateTrapAfterTimeComponent_tF67C6E72F8FD0ED2B18CC70C37EAB23DAFECE6A9::get_offset_of_Duration_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8330 = { sizeof (CreateTrapAfterTimeSystem_tA798566C21ADB31B4D0CC794B25A3E085C93B90D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8330[2] = 
{
	CreateTrapAfterTimeSystem_tA798566C21ADB31B4D0CC794B25A3E085C93B90D::get_offset_of__context_0(),
	CreateTrapAfterTimeSystem_tA798566C21ADB31B4D0CC794B25A3E085C93B90D::get_offset_of__group_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8331 = { sizeof (ProjectileTrapSpellSystem_tF00A64F0F06E4BF25F0AF16542FFAAF20E267C28), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8331[2] = 
{
	ProjectileTrapSpellSystem_tF00A64F0F06E4BF25F0AF16542FFAAF20E267C28::get_offset_of__spellSO_3(),
	ProjectileTrapSpellSystem_tF00A64F0F06E4BF25F0AF16542FFAAF20E267C28::get_offset_of__context_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8332 = { sizeof (SelfBuffSpellSystem_tA1FEB6AEA8561A2E54F734CE25AC28CF3C0D83D7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8332[2] = 
{
	SelfBuffSpellSystem_tA1FEB6AEA8561A2E54F734CE25AC28CF3C0D83D7::get_offset_of__spellSO_3(),
	SelfBuffSpellSystem_tA1FEB6AEA8561A2E54F734CE25AC28CF3C0D83D7::get_offset_of__context_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8333 = { sizeof (CompanionComponent_t25D4800A96B514082EF457F3C3CD6DC54A49CB57), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8333[1] = 
{
	CompanionComponent_t25D4800A96B514082EF457F3C3CD6DC54A49CB57::get_offset_of_CompanionId_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8334 = { sizeof (CompanionSpellSystem_t99E789E2AF35FEA8CCC51E566303551634100D01), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8334[3] = 
{
	CompanionSpellSystem_t99E789E2AF35FEA8CCC51E566303551634100D01::get_offset_of__spellSO_3(),
	CompanionSpellSystem_t99E789E2AF35FEA8CCC51E566303551634100D01::get_offset_of__unitsSO_4(),
	CompanionSpellSystem_t99E789E2AF35FEA8CCC51E566303551634100D01::get_offset_of__context_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8335 = { sizeof (DecoyComponent_tF6B0B470DD347730957875051E3D5670D3839070), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8335[2] = 
{
	DecoyComponent_tF6B0B470DD347730957875051E3D5670D3839070::get_offset_of_DecoyBundle_0(),
	DecoyComponent_tF6B0B470DD347730957875051E3D5670D3839070::get_offset_of_DecoyPrefab_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8336 = { sizeof (DecoyHPSystem_t0AEE898AE1311AF423CE18B9025ADE3977E260AC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8336[1] = 
{
	DecoyHPSystem_t0AEE898AE1311AF423CE18B9025ADE3977E260AC::get_offset_of__context_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8337 = { sizeof (DecoySpellSystem_t582E869AC6345726713035A6A5EACDD3E1085188), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8337[3] = 
{
	DecoySpellSystem_t582E869AC6345726713035A6A5EACDD3E1085188::get_offset_of__spellSO_3(),
	DecoySpellSystem_t582E869AC6345726713035A6A5EACDD3E1085188::get_offset_of__unitsSO_4(),
	DecoySpellSystem_t582E869AC6345726713035A6A5EACDD3E1085188::get_offset_of__context_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8338 = { sizeof (MeleeSpellSystem_t03811FEA71702E3ABC712FF669F6F7C0E69574AB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8338[3] = 
{
	MeleeSpellSystem_t03811FEA71702E3ABC712FF669F6F7C0E69574AB::get_offset_of__spellSO_3(),
	MeleeSpellSystem_t03811FEA71702E3ABC712FF669F6F7C0E69574AB::get_offset_of__soundSO_4(),
	MeleeSpellSystem_t03811FEA71702E3ABC712FF669F6F7C0E69574AB::get_offset_of__context_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8339 = { sizeof (ProjectileClosestSystem_t75FAE18763DDA4B13EDB82EF7E062CEE4B68143A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8339[2] = 
{
	ProjectileClosestSystem_t75FAE18763DDA4B13EDB82EF7E062CEE4B68143A::get_offset_of__spellSO_3(),
	ProjectileClosestSystem_t75FAE18763DDA4B13EDB82EF7E062CEE4B68143A::get_offset_of__context_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8340 = { sizeof (ProjectileSelectedSystem_t712BCFECE1EA815B621E4744613F1CA3653C5E12), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8340[2] = 
{
	ProjectileSelectedSystem_t712BCFECE1EA815B621E4744613F1CA3653C5E12::get_offset_of__spellSO_3(),
	ProjectileSelectedSystem_t712BCFECE1EA815B621E4744613F1CA3653C5E12::get_offset_of__context_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8341 = { sizeof (RainFireSpellSystem_tF7EA20CB38B0FCD1FC1B43D85D75EB1E8FC5428C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8341[2] = 
{
	RainFireSpellSystem_tF7EA20CB38B0FCD1FC1B43D85D75EB1E8FC5428C::get_offset_of__spellSO_3(),
	RainFireSpellSystem_tF7EA20CB38B0FCD1FC1B43D85D75EB1E8FC5428C::get_offset_of__context_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8342 = { sizeof (SpellInitializeSystem_t1FEAF7FF0CB1BBE4A93D6A9212C4F8193AB7680B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8342[3] = 
{
	SpellInitializeSystem_t1FEAF7FF0CB1BBE4A93D6A9212C4F8193AB7680B::get_offset_of__unitsSO_3(),
	SpellInitializeSystem_t1FEAF7FF0CB1BBE4A93D6A9212C4F8193AB7680B::get_offset_of__battleSettingsSO_4(),
	SpellInitializeSystem_t1FEAF7FF0CB1BBE4A93D6A9212C4F8193AB7680B::get_offset_of__context_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8343 = { sizeof (AIUtils_tF258624F778C8B3EB6206D1106823C3A93CB7E3A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8344 = { sizeof (BattleTimeComponent_tEDFAA2735C49A6FA764F8DBEEE56EDF39A6E11EE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8344[1] = 
{
	BattleTimeComponent_tEDFAA2735C49A6FA764F8DBEEE56EDF39A6E11EE::get_offset_of_Time_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8345 = { sizeof (BattleTimeSystem_t3C3EA048C7A829A065CFC3F2EB900EA617549C3F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8345[1] = 
{
	BattleTimeSystem_t3C3EA048C7A829A065CFC3F2EB900EA617549C3F::get_offset_of__context_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8346 = { sizeof (TowerHealingComponent_tBF1C6AD3469D91F868A272D1ED2BD56CDB4D93D4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8347 = { sizeof (TowerHealingDuration_t247E3B8EA86A4D76A40D407854846D21CCABF915), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8347[1] = 
{
	TowerHealingDuration_t247E3B8EA86A4D76A40D407854846D21CCABF915::get_offset_of_Duration_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8348 = { sizeof (TowerHealingStartTimeComponent_tD7958630EA379A94C28205095CD7F9C18C07EFAB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8348[1] = 
{
	TowerHealingStartTimeComponent_tD7958630EA379A94C28205095CD7F9C18C07EFAB::get_offset_of_StartTime_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8349 = { sizeof (TowerHealingSystem_t35BED9065265E4593021A609BC41DFFCE7250461), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8349[3] = 
{
	TowerHealingSystem_t35BED9065265E4593021A609BC41DFFCE7250461::get_offset_of__towersSO_0(),
	TowerHealingSystem_t35BED9065265E4593021A609BC41DFFCE7250461::get_offset_of__context_1(),
	TowerHealingSystem_t35BED9065265E4593021A609BC41DFFCE7250461::get_offset_of__towers_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8350 = { sizeof (TowerHPSystem_t84316A8F9C617646AC3E5C22A27BB43115F204E6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8350[1] = 
{
	TowerHPSystem_t84316A8F9C617646AC3E5C22A27BB43115F204E6::get_offset_of__context_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8351 = { sizeof (TowerSimulationInitSystem_t8BD7784BF9ADF591EE0C6E552324A68594209755), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8351[2] = 
{
	TowerSimulationInitSystem_t8BD7784BF9ADF591EE0C6E552324A68594209755::get_offset_of__towersSO_3(),
	TowerSimulationInitSystem_t8BD7784BF9ADF591EE0C6E552324A68594209755::get_offset_of__context_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8352 = { sizeof (TrapInitSystem_tC9F18B169655A56BD6F5E2EFF80F9000DCA10921), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8352[2] = 
{
	TrapInitSystem_tC9F18B169655A56BD6F5E2EFF80F9000DCA10921::get_offset_of__trapsSO_3(),
	TrapInitSystem_tC9F18B169655A56BD6F5E2EFF80F9000DCA10921::get_offset_of__context_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8353 = { sizeof (ChaptersUtils_tCB388F0B35B67640D1813E4EDD9D3910634EF833), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8354 = { sizeof (CheatsDictionary_tAE0AD2EE44BAE0EDDF8737127DFFB63BE8F0357B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8355 = { sizeof (ClearDataCheat_t9A397895E8DC547509E94B030C5B57BC814FA254), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8356 = { sizeof (CommandCheats_tA39912D252DB4C3A6B45F65FF565794793DA1037), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8357 = { sizeof (ConfCheats_t8778C80FBE0D5B00BDF12A4B0CB263D6D0C60D7B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8357[1] = 
{
	ConfCheats_t8778C80FBE0D5B00BDF12A4B0CB263D6D0C60D7B::get_offset_of__diContainer_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8358 = { sizeof (DisplayNameCheat_tA9CF3AD1FAD29502051B6B253F45071962C38EEE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8358[1] = 
{
	DisplayNameCheat_tA9CF3AD1FAD29502051B6B253F45071962C38EEE::get_offset_of__gameSpark_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8359 = { sizeof (GeneralCheats_tA76EB336B088C807D1B53B0A573317CEF56CCF76), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8359[4] = 
{
	GeneralCheats_tA76EB336B088C807D1B53B0A573317CEF56CCF76::get_offset_of__voSaver_0(),
	GeneralCheats_tA76EB336B088C807D1B53B0A573317CEF56CCF76::get_offset_of__userVO_1(),
	GeneralCheats_tA76EB336B088C807D1B53B0A573317CEF56CCF76::get_offset_of__inventorySystem_2(),
	GeneralCheats_tA76EB336B088C807D1B53B0A573317CEF56CCF76::get_offset_of__inventoryVO_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8360 = { sizeof (NotificationCheat_tCE02BBFC7C7ECB3543798505AA3969BD07D41748), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8360[1] = 
{
	NotificationCheat_tCE02BBFC7C7ECB3543798505AA3969BD07D41748::get_offset_of__notificationsManager_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8361 = { sizeof (StartLevelCheat_tED489B7E94B35861FBF1812AE937C30957714A96), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8361[2] = 
{
	StartLevelCheat_tED489B7E94B35861FBF1812AE937C30957714A96::get_offset_of__userVO_0(),
	StartLevelCheat_tED489B7E94B35861FBF1812AE937C30957714A96::get_offset_of__voSaver_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8362 = { sizeof (SubmitLevelCheat_t77B4717B7CE62D7C292C127633F118AD5C13D22F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8362[2] = 
{
	SubmitLevelCheat_t77B4717B7CE62D7C292C127633F118AD5C13D22F::get_offset_of__gameSpark_0(),
	SubmitLevelCheat_t77B4717B7CE62D7C292C127633F118AD5C13D22F::get_offset_of__userVO_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8363 = { sizeof (U3CU3Ec__DisplayClass2_0_tFA4ECACEC58A7A5E144BF7182AF518243943300C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8363[2] = 
{
	U3CU3Ec__DisplayClass2_0_tFA4ECACEC58A7A5E144BF7182AF518243943300C::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass2_0_tFA4ECACEC58A7A5E144BF7182AF518243943300C::get_offset_of_stars_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8364 = { sizeof (SubmitTrophyCheat_t8C8A63FD5BD974542EC4BB7B1E70B6CFABC9541C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8364[2] = 
{
	SubmitTrophyCheat_t8C8A63FD5BD974542EC4BB7B1E70B6CFABC9541C::get_offset_of__gameSpark_0(),
	SubmitTrophyCheat_t8C8A63FD5BD974542EC4BB7B1E70B6CFABC9541C::get_offset_of__userVO_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8365 = { sizeof (ChestCardComponent_tFBA242D00DC22D455606C8608C021C39FE551E12), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8365[1] = 
{
	ChestCardComponent_tFBA242D00DC22D455606C8608C021C39FE551E12::get_offset_of__count_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8366 = { sizeof (ChestGotchaComponent_tA318577CE122D49B2C8F7D8D9BCD3F5E2BB0EA45), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8366[5] = 
{
	ChestGotchaComponent_tA318577CE122D49B2C8F7D8D9BCD3F5E2BB0EA45::get_offset_of__chestAnimator_4(),
	ChestGotchaComponent_tA318577CE122D49B2C8F7D8D9BCD3F5E2BB0EA45::get_offset_of__uiMain_5(),
	ChestGotchaComponent_tA318577CE122D49B2C8F7D8D9BCD3F5E2BB0EA45::get_offset_of__rewards_6(),
	ChestGotchaComponent_tA318577CE122D49B2C8F7D8D9BCD3F5E2BB0EA45::get_offset_of__lastCard_7(),
	ChestGotchaComponent_tA318577CE122D49B2C8F7D8D9BCD3F5E2BB0EA45::get_offset_of__rewardIndex_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8367 = { sizeof (LevelChestComponent_t72D74F7CC3B050957B6074881BD7D1E8B0ABB2E6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8367[10] = 
{
	LevelChestComponent_t72D74F7CC3B050957B6074881BD7D1E8B0ABB2E6::get_offset_of__progress_4(),
	LevelChestComponent_t72D74F7CC3B050957B6074881BD7D1E8B0ABB2E6::get_offset_of__progressText_5(),
	LevelChestComponent_t72D74F7CC3B050957B6074881BD7D1E8B0ABB2E6::get_offset_of__progressOpen_6(),
	LevelChestComponent_t72D74F7CC3B050957B6074881BD7D1E8B0ABB2E6::get_offset_of__progressTextOpen_7(),
	LevelChestComponent_t72D74F7CC3B050957B6074881BD7D1E8B0ABB2E6::get_offset_of__openChest_8(),
	LevelChestComponent_t72D74F7CC3B050957B6074881BD7D1E8B0ABB2E6::get_offset_of__uiMain_9(),
	LevelChestComponent_t72D74F7CC3B050957B6074881BD7D1E8B0ABB2E6::get_offset_of__diContainer_10(),
	LevelChestComponent_t72D74F7CC3B050957B6074881BD7D1E8B0ABB2E6::get_offset_of__tournamentsVO_11(),
	LevelChestComponent_t72D74F7CC3B050957B6074881BD7D1E8B0ABB2E6::get_offset_of__levelVariable_12(),
	LevelChestComponent_t72D74F7CC3B050957B6074881BD7D1E8B0ABB2E6::get_offset_of__starQuest_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8368 = { sizeof (LevelVariable_t8E3E27789809A8DFA12D41DB3927AC1DC90D4279), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8368[1] = 
{
	LevelVariable_t8E3E27789809A8DFA12D41DB3927AC1DC90D4279::get_offset_of__userVO_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8369 = { sizeof (BasicGotchaNode_tC38E11ED54EEB8CDB9613BDD5CA7A78395D5F9EC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8370 = { sizeof (GotchaNodeData_t7B74A837B128AFAF6F873873674CC2D974DB1EEB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8370[1] = 
{
	GotchaNodeData_t7B74A837B128AFAF6F873873674CC2D974DB1EEB::get_offset_of_Rewards_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8371 = { sizeof (GoldGotchaNode_tD301E7AAB8037C9718720DA60F3A662E9D574BAE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8371[1] = 
{
	GoldGotchaNode_tD301E7AAB8037C9718720DA60F3A662E9D574BAE::get_offset_of__chestGotchaComponent_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8372 = { sizeof (SilverGotchaNode_tAD996AF28A32DEC9F70F89E37C6635D3B9634CC6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8372[1] = 
{
	SilverGotchaNode_tAD996AF28A32DEC9F70F89E37C6635D3B9634CC6::get_offset_of__chestGotchaComponent_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8373 = { sizeof (StarChestComponent_t8ADEE7CB2C641D2EB6AD4D08E19B94D627A99508), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8373[10] = 
{
	StarChestComponent_t8ADEE7CB2C641D2EB6AD4D08E19B94D627A99508::get_offset_of__progress_4(),
	StarChestComponent_t8ADEE7CB2C641D2EB6AD4D08E19B94D627A99508::get_offset_of__progressText_5(),
	StarChestComponent_t8ADEE7CB2C641D2EB6AD4D08E19B94D627A99508::get_offset_of__progressOpen_6(),
	StarChestComponent_t8ADEE7CB2C641D2EB6AD4D08E19B94D627A99508::get_offset_of__progressTextOpen_7(),
	StarChestComponent_t8ADEE7CB2C641D2EB6AD4D08E19B94D627A99508::get_offset_of__openChest_8(),
	StarChestComponent_t8ADEE7CB2C641D2EB6AD4D08E19B94D627A99508::get_offset_of__diContainer_9(),
	StarChestComponent_t8ADEE7CB2C641D2EB6AD4D08E19B94D627A99508::get_offset_of__uiMain_10(),
	StarChestComponent_t8ADEE7CB2C641D2EB6AD4D08E19B94D627A99508::get_offset_of__userEvents_11(),
	StarChestComponent_t8ADEE7CB2C641D2EB6AD4D08E19B94D627A99508::get_offset_of__userVO_12(),
	StarChestComponent_t8ADEE7CB2C641D2EB6AD4D08E19B94D627A99508::get_offset_of__starVariable_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8374 = { sizeof (StarVariable_t5610FC328147BA512296EC37D396F70C4D050676), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8374[2] = 
{
	StarVariable_t5610FC328147BA512296EC37D396F70C4D050676::get_offset_of__userVO_4(),
	StarVariable_t5610FC328147BA512296EC37D396F70C4D050676::get_offset_of__tournamentsVO_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8375 = { sizeof (ChestsUtils_t025BA5FD13B87313CCA5F605FE7066399F0443C8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8376 = { sizeof (U3CU3Ec_tCF9FE9503A64A6801962BD22B858BEA8C612E5A7), -1, sizeof(U3CU3Ec_tCF9FE9503A64A6801962BD22B858BEA8C612E5A7_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8376[2] = 
{
	U3CU3Ec_tCF9FE9503A64A6801962BD22B858BEA8C612E5A7_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tCF9FE9503A64A6801962BD22B858BEA8C612E5A7_StaticFields::get_offset_of_U3CU3E9__1_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8377 = { sizeof (ConsumeSpellCommand_tECBA7AA1F4987C7D973B7A472CF6AE49C9C17B2D), -1, sizeof(ConsumeSpellCommand_tECBA7AA1F4987C7D973B7A472CF6AE49C9C17B2D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8377[5] = 
{
	ConsumeSpellCommand_tECBA7AA1F4987C7D973B7A472CF6AE49C9C17B2D::get_offset_of__gameSettingsSO_0(),
	ConsumeSpellCommand_tECBA7AA1F4987C7D973B7A472CF6AE49C9C17B2D::get_offset_of__inventorySystem_1(),
	ConsumeSpellCommand_tECBA7AA1F4987C7D973B7A472CF6AE49C9C17B2D::get_offset_of__voSaver_2(),
	ConsumeSpellCommand_tECBA7AA1F4987C7D973B7A472CF6AE49C9C17B2D::get_offset_of__inventoryVO_3(),
	ConsumeSpellCommand_tECBA7AA1F4987C7D973B7A472CF6AE49C9C17B2D_StaticFields::get_offset_of_ConsumeSpellCommandEvent_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8378 = { sizeof (ConsumeSpellCommandModel_t0D5A73862A452D6C88F74A66703AE278AA372D9A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8378[1] = 
{
	ConsumeSpellCommandModel_t0D5A73862A452D6C88F74A66703AE278AA372D9A::get_offset_of_SpellId_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8379 = { sizeof (ConsumeSpellCommandResultModel_t67C78E5BE640F4619BDECA9482ECA9E947D7E27A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8380 = { sizeof (ConsumeSpellEvent_t99A1EF82A5CC840F993D8568A4547414594DA516), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8381 = { sizeof (LevelLostCommand_t703967448459CC96A9AF820AF2EADA55D00B4CE0), -1, sizeof(LevelLostCommand_t703967448459CC96A9AF820AF2EADA55D00B4CE0_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8381[5] = 
{
	LevelLostCommand_t703967448459CC96A9AF820AF2EADA55D00B4CE0::get_offset_of__gameSettingsSO_0(),
	LevelLostCommand_t703967448459CC96A9AF820AF2EADA55D00B4CE0::get_offset_of__inventorySystem_1(),
	LevelLostCommand_t703967448459CC96A9AF820AF2EADA55D00B4CE0::get_offset_of__voSaver_2(),
	LevelLostCommand_t703967448459CC96A9AF820AF2EADA55D00B4CE0::get_offset_of__inventoryVO_3(),
	LevelLostCommand_t703967448459CC96A9AF820AF2EADA55D00B4CE0_StaticFields::get_offset_of_LevelLostCommandEvent_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8382 = { sizeof (LevelLostCommandModel_t4CD66AE0DDD1133CCF8B325881903764D8F85760), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8382[3] = 
{
	LevelLostCommandModel_t4CD66AE0DDD1133CCF8B325881903764D8F85760::get_offset_of_LevelNumber_0(),
	LevelLostCommandModel_t4CD66AE0DDD1133CCF8B325881903764D8F85760::get_offset_of_Stars_1(),
	LevelLostCommandModel_t4CD66AE0DDD1133CCF8B325881903764D8F85760::get_offset_of_ResponseData_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8383 = { sizeof (LevelLostCommandResultModel_tCD8892EDE40EEDD2DA9FB8BF2A01A253B668B06B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8383[1] = 
{
	LevelLostCommandResultModel_tCD8892EDE40EEDD2DA9FB8BF2A01A253B668B06B::get_offset_of_Stars_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8384 = { sizeof (LevelLostEvent_t94C8D8FEA2B83A32F83B354B37B05B07139C1A48), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8385 = { sizeof (LevelWonCommand_t1A01664F3F5F3F67674ED6534C4AEB6AA2ACB496), -1, sizeof(LevelWonCommand_t1A01664F3F5F3F67674ED6534C4AEB6AA2ACB496_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8385[8] = 
{
	LevelWonCommand_t1A01664F3F5F3F67674ED6534C4AEB6AA2ACB496::get_offset_of__unitsConf_0(),
	LevelWonCommand_t1A01664F3F5F3F67674ED6534C4AEB6AA2ACB496::get_offset_of__userVO_1(),
	LevelWonCommand_t1A01664F3F5F3F67674ED6534C4AEB6AA2ACB496::get_offset_of__tournamentsVO_2(),
	LevelWonCommand_t1A01664F3F5F3F67674ED6534C4AEB6AA2ACB496::get_offset_of__voSaver_3(),
	LevelWonCommand_t1A01664F3F5F3F67674ED6534C4AEB6AA2ACB496::get_offset_of__battleSettingsSO_4(),
	LevelWonCommand_t1A01664F3F5F3F67674ED6534C4AEB6AA2ACB496::get_offset_of__inventorySystem_5(),
	LevelWonCommand_t1A01664F3F5F3F67674ED6534C4AEB6AA2ACB496::get_offset_of__inventoryVO_6(),
	LevelWonCommand_t1A01664F3F5F3F67674ED6534C4AEB6AA2ACB496_StaticFields::get_offset_of_LevelWonCommandEvent_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8386 = { sizeof (LevelWonCommandModel_t1A8C787CFE6EA68E97E76B2C5C25238208FCF53A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8386[1] = 
{
	LevelWonCommandModel_t1A8C787CFE6EA68E97E76B2C5C25238208FCF53A::get_offset_of_Stars_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8387 = { sizeof (LevelWonCommandResultModel_t858BFA2B17BB11DDE488DA1E1E66078C6E920091), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8387[1] = 
{
	LevelWonCommandResultModel_t858BFA2B17BB11DDE488DA1E1E66078C6E920091::get_offset_of_Stars_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8388 = { sizeof (LevelWonEvent_t52CF9D205E167284E58802E830A1FD5897098944), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8389 = { sizeof (ReviveCommand_t9DCA210E868E14B66BB5A0BD8C4F3080A6D63C3B), -1, sizeof(ReviveCommand_t9DCA210E868E14B66BB5A0BD8C4F3080A6D63C3B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8389[5] = 
{
	ReviveCommand_t9DCA210E868E14B66BB5A0BD8C4F3080A6D63C3B::get_offset_of__inventorySystem_0(),
	ReviveCommand_t9DCA210E868E14B66BB5A0BD8C4F3080A6D63C3B::get_offset_of__voSaver_1(),
	ReviveCommand_t9DCA210E868E14B66BB5A0BD8C4F3080A6D63C3B::get_offset_of__inventoryVO_2(),
	ReviveCommand_t9DCA210E868E14B66BB5A0BD8C4F3080A6D63C3B::get_offset_of__battleSettingsSO_3(),
	ReviveCommand_t9DCA210E868E14B66BB5A0BD8C4F3080A6D63C3B_StaticFields::get_offset_of_ReviveCommandEvent_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8390 = { sizeof (ReviveCommandModel_t9016F442CB25E5AED7385F73B0CE9C969ED51879), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8391 = { sizeof (ReviveCommandResultModel_t483C14D7B2131E717207F02F0FFE04752FFCB0E1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8391[1] = 
{
	ReviveCommandResultModel_t483C14D7B2131E717207F02F0FFE04752FFCB0E1::get_offset_of_Revived_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8392 = { sizeof (ReviveCommandEvent_t5CCE33B3F24E5C83F3E608F719C653E55CDE4470), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8393 = { sizeof (BattleCommandUtils_t81B76E8E86DEDEFFDC1103E10D3C8650921E84DC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8394 = { sizeof (AddSpellsCheatCommand_t82FD966051B1B85FE769FC1137874BB39F67C60F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8394[4] = 
{
	AddSpellsCheatCommand_t82FD966051B1B85FE769FC1137874BB39F67C60F::get_offset_of__inventorySystem_0(),
	AddSpellsCheatCommand_t82FD966051B1B85FE769FC1137874BB39F67C60F::get_offset_of__voSaver_1(),
	AddSpellsCheatCommand_t82FD966051B1B85FE769FC1137874BB39F67C60F::get_offset_of__inventoryVO_2(),
	AddSpellsCheatCommand_t82FD966051B1B85FE769FC1137874BB39F67C60F::get_offset_of__battleSettingsSO_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8395 = { sizeof (AddSpellsCheatCommandModel_t791A2D188E67AC49BD841E2A757671764EF2231D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8395[1] = 
{
	AddSpellsCheatCommandModel_t791A2D188E67AC49BD841E2A757671764EF2231D::get_offset_of_Count_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8396 = { sizeof (AddSpellsCheatCommandResultModel_tBD0463F2503315AC84C891CB7060E403C5BB22B0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8397 = { sizeof (LevelChestCommand_tA24853FAD5DAB3B2047F39B8B9B827FF432229A6), -1, sizeof(LevelChestCommand_tA24853FAD5DAB3B2047F39B8B9B827FF432229A6_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8397[6] = 
{
	LevelChestCommand_tA24853FAD5DAB3B2047F39B8B9B827FF432229A6::get_offset_of__starsChest_0(),
	LevelChestCommand_tA24853FAD5DAB3B2047F39B8B9B827FF432229A6::get_offset_of__tournamentsVO_1(),
	LevelChestCommand_tA24853FAD5DAB3B2047F39B8B9B827FF432229A6::get_offset_of__voSaver_2(),
	LevelChestCommand_tA24853FAD5DAB3B2047F39B8B9B827FF432229A6::get_offset_of__inventorySystem_3(),
	LevelChestCommand_tA24853FAD5DAB3B2047F39B8B9B827FF432229A6::get_offset_of__inventoryVO_4(),
	LevelChestCommand_tA24853FAD5DAB3B2047F39B8B9B827FF432229A6_StaticFields::get_offset_of_LevelChestCommandEvent_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8398 = { sizeof (LevelChestCommandModel_t34B3A388CD2A718C1293052B731684DF1038F485), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8399 = { sizeof (LevelChestCommandResultModel_tF4ECE52AC2A37B46679689F0AA89616692B936F2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8399[1] = 
{
	LevelChestCommandResultModel_tF4ECE52AC2A37B46679689F0AA89616692B936F2::get_offset_of_Rewards_0(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
