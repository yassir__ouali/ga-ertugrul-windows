﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.List`1<System.Type>
struct List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0;
// System.Collections.Generic.List`1<Zenject.TypeValuePair>
struct List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69;
// System.Func`2<Zenject.DiContainer,Zenject.IProvider>
struct Func_2_t3BE49542C4369B3A5CA2F7EFB4B994EA5A9C449C;
// System.Func`2<Zenject.InjectContext,System.Object>
struct Func_2_tE24EB23C7D8F4AD0A3A34C5DB2C9F329886F48F2;
// System.Func`2<Zenject.InjectContext,UnityEngine.GameObject>
struct Func_2_tD5D4A9F4A7BCFBB67D83DD4F52BFE19A8153D2D1;
// System.Func`2<Zenject.InjectContext,UnityEngine.Transform>
struct Func_2_tD7382E4A0354665A42C6D76850F29E2926A3063C;
// System.Func`3<System.Type,Zenject.IPrefabInstantiator,Zenject.IProvider>
struct Func_3_t11D22BBD9531DD4E1FDE18B3937E1DC07ED8DB79;
// System.String
struct String_t;
// System.Type
struct Type_t;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// Zenject.BindFinalizerWrapper
struct BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8;
// Zenject.BindInfo
struct BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991;
// Zenject.BindingCondition
struct BindingCondition_t3BC89ABD74DA160C3396036CE0360DE414B4ADD8;
// Zenject.DiContainer
struct DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5;
// Zenject.FromBinder
struct FromBinder_t69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75;
// Zenject.GameObjectCreationParameters
struct GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A;
// Zenject.IBindingFinalizer
struct IBindingFinalizer_t08B71AF38DA228689170154233A3C472FE81BD83;
// Zenject.PrefabBindingFinalizer
struct PrefabBindingFinalizer_t355616E5B859E3E8B597F710035FAE139DF073FE;
// Zenject.PrefabBindingFinalizer/<>c__DisplayClass5_0
struct U3CU3Ec__DisplayClass5_0_t25A424B5F0CD0CBB038687263E0AA9B93DA940DA;
// Zenject.PrefabBindingFinalizer/<>c__DisplayClass6_0
struct U3CU3Ec__DisplayClass6_0_tCA46D26FA4A790B5596A5EEB43024EABF63DD91E;
// Zenject.PrefabInstantiatorCached
struct PrefabInstantiatorCached_tB23BE6E42DED15DF8DD349E6E8C80C45CBDA95D5;
// Zenject.PrefabResourceBindingFinalizer
struct PrefabResourceBindingFinalizer_t8527DD2D663940B5BE41029F8B5C51128D446CC4;
// Zenject.PrefabResourceBindingFinalizer/<>c__DisplayClass5_0
struct U3CU3Ec__DisplayClass5_0_tC4DFEACC4F54B76864B4C241BAB809A1D105B6F3;
// Zenject.PrefabResourceBindingFinalizer/<>c__DisplayClass6_0
struct U3CU3Ec__DisplayClass6_0_t8DCC1F5B55699D1CFF1D77EC63B83C993536E074;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef BINDFINALIZERWRAPPER_T043E4C78D018E6AF48A956B33FF3AEA55CDD86C8_H
#define BINDFINALIZERWRAPPER_T043E4C78D018E6AF48A956B33FF3AEA55CDD86C8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.BindFinalizerWrapper
struct  BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8  : public RuntimeObject
{
public:
	// Zenject.IBindingFinalizer Zenject.BindFinalizerWrapper::_subFinalizer
	RuntimeObject* ____subFinalizer_0;

public:
	inline static int32_t get_offset_of__subFinalizer_0() { return static_cast<int32_t>(offsetof(BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8, ____subFinalizer_0)); }
	inline RuntimeObject* get__subFinalizer_0() const { return ____subFinalizer_0; }
	inline RuntimeObject** get_address_of__subFinalizer_0() { return &____subFinalizer_0; }
	inline void set__subFinalizer_0(RuntimeObject* value)
	{
		____subFinalizer_0 = value;
		Il2CppCodeGenWriteBarrier((&____subFinalizer_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDFINALIZERWRAPPER_T043E4C78D018E6AF48A956B33FF3AEA55CDD86C8_H
#ifndef BINDINGUTIL_T6D57223F2867DCF395084BB1CFE781F8DDCBD096_H
#define BINDINGUTIL_T6D57223F2867DCF395084BB1CFE781F8DDCBD096_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.BindingUtil
struct  BindingUtil_t6D57223F2867DCF395084BB1CFE781F8DDCBD096  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGUTIL_T6D57223F2867DCF395084BB1CFE781F8DDCBD096_H
#ifndef FACTORYBINDINFO_T13E5B099259F31F1EF31D3550FE94DF78C9F1C00_H
#define FACTORYBINDINFO_T13E5B099259F31F1EF31D3550FE94DF78C9F1C00_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.FactoryBindInfo
struct  FactoryBindInfo_t13E5B099259F31F1EF31D3550FE94DF78C9F1C00  : public RuntimeObject
{
public:
	// System.Type Zenject.FactoryBindInfo::<FactoryType>k__BackingField
	Type_t * ___U3CFactoryTypeU3Ek__BackingField_0;
	// System.Func`2<Zenject.DiContainer,Zenject.IProvider> Zenject.FactoryBindInfo::<ProviderFunc>k__BackingField
	Func_2_t3BE49542C4369B3A5CA2F7EFB4B994EA5A9C449C * ___U3CProviderFuncU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CFactoryTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(FactoryBindInfo_t13E5B099259F31F1EF31D3550FE94DF78C9F1C00, ___U3CFactoryTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CFactoryTypeU3Ek__BackingField_0() const { return ___U3CFactoryTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CFactoryTypeU3Ek__BackingField_0() { return &___U3CFactoryTypeU3Ek__BackingField_0; }
	inline void set_U3CFactoryTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CFactoryTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFactoryTypeU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CProviderFuncU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(FactoryBindInfo_t13E5B099259F31F1EF31D3550FE94DF78C9F1C00, ___U3CProviderFuncU3Ek__BackingField_1)); }
	inline Func_2_t3BE49542C4369B3A5CA2F7EFB4B994EA5A9C449C * get_U3CProviderFuncU3Ek__BackingField_1() const { return ___U3CProviderFuncU3Ek__BackingField_1; }
	inline Func_2_t3BE49542C4369B3A5CA2F7EFB4B994EA5A9C449C ** get_address_of_U3CProviderFuncU3Ek__BackingField_1() { return &___U3CProviderFuncU3Ek__BackingField_1; }
	inline void set_U3CProviderFuncU3Ek__BackingField_1(Func_2_t3BE49542C4369B3A5CA2F7EFB4B994EA5A9C449C * value)
	{
		___U3CProviderFuncU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CProviderFuncU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACTORYBINDINFO_T13E5B099259F31F1EF31D3550FE94DF78C9F1C00_H
#ifndef U3CU3EC_TD42D911B6777DD155EA9826FA9CE2DD0A8D53944_H
#define U3CU3EC_TD42D911B6777DD155EA9826FA9CE2DD0A8D53944_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.FromBinder_<>c
struct  U3CU3Ec_tD42D911B6777DD155EA9826FA9CE2DD0A8D53944  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tD42D911B6777DD155EA9826FA9CE2DD0A8D53944_StaticFields
{
public:
	// Zenject.FromBinder_<>c Zenject.FromBinder_<>c::<>9
	U3CU3Ec_tD42D911B6777DD155EA9826FA9CE2DD0A8D53944 * ___U3CU3E9_0;
	// System.Func`3<System.Type,Zenject.IPrefabInstantiator,Zenject.IProvider> Zenject.FromBinder_<>c::<>9__23_0
	Func_3_t11D22BBD9531DD4E1FDE18B3937E1DC07ED8DB79 * ___U3CU3E9__23_0_1;
	// System.Func`3<System.Type,Zenject.IPrefabInstantiator,Zenject.IProvider> Zenject.FromBinder_<>c::<>9__25_0
	Func_3_t11D22BBD9531DD4E1FDE18B3937E1DC07ED8DB79 * ___U3CU3E9__25_0_2;
	// System.Func`3<System.Type,Zenject.IPrefabInstantiator,Zenject.IProvider> Zenject.FromBinder_<>c::<>9__27_0
	Func_3_t11D22BBD9531DD4E1FDE18B3937E1DC07ED8DB79 * ___U3CU3E9__27_0_3;
	// System.Func`3<System.Type,Zenject.IPrefabInstantiator,Zenject.IProvider> Zenject.FromBinder_<>c::<>9__29_0
	Func_3_t11D22BBD9531DD4E1FDE18B3937E1DC07ED8DB79 * ___U3CU3E9__29_0_4;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tD42D911B6777DD155EA9826FA9CE2DD0A8D53944_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tD42D911B6777DD155EA9826FA9CE2DD0A8D53944 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tD42D911B6777DD155EA9826FA9CE2DD0A8D53944 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tD42D911B6777DD155EA9826FA9CE2DD0A8D53944 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__23_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tD42D911B6777DD155EA9826FA9CE2DD0A8D53944_StaticFields, ___U3CU3E9__23_0_1)); }
	inline Func_3_t11D22BBD9531DD4E1FDE18B3937E1DC07ED8DB79 * get_U3CU3E9__23_0_1() const { return ___U3CU3E9__23_0_1; }
	inline Func_3_t11D22BBD9531DD4E1FDE18B3937E1DC07ED8DB79 ** get_address_of_U3CU3E9__23_0_1() { return &___U3CU3E9__23_0_1; }
	inline void set_U3CU3E9__23_0_1(Func_3_t11D22BBD9531DD4E1FDE18B3937E1DC07ED8DB79 * value)
	{
		___U3CU3E9__23_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__23_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__25_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_tD42D911B6777DD155EA9826FA9CE2DD0A8D53944_StaticFields, ___U3CU3E9__25_0_2)); }
	inline Func_3_t11D22BBD9531DD4E1FDE18B3937E1DC07ED8DB79 * get_U3CU3E9__25_0_2() const { return ___U3CU3E9__25_0_2; }
	inline Func_3_t11D22BBD9531DD4E1FDE18B3937E1DC07ED8DB79 ** get_address_of_U3CU3E9__25_0_2() { return &___U3CU3E9__25_0_2; }
	inline void set_U3CU3E9__25_0_2(Func_3_t11D22BBD9531DD4E1FDE18B3937E1DC07ED8DB79 * value)
	{
		___U3CU3E9__25_0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__25_0_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__27_0_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_tD42D911B6777DD155EA9826FA9CE2DD0A8D53944_StaticFields, ___U3CU3E9__27_0_3)); }
	inline Func_3_t11D22BBD9531DD4E1FDE18B3937E1DC07ED8DB79 * get_U3CU3E9__27_0_3() const { return ___U3CU3E9__27_0_3; }
	inline Func_3_t11D22BBD9531DD4E1FDE18B3937E1DC07ED8DB79 ** get_address_of_U3CU3E9__27_0_3() { return &___U3CU3E9__27_0_3; }
	inline void set_U3CU3E9__27_0_3(Func_3_t11D22BBD9531DD4E1FDE18B3937E1DC07ED8DB79 * value)
	{
		___U3CU3E9__27_0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__27_0_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__29_0_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_tD42D911B6777DD155EA9826FA9CE2DD0A8D53944_StaticFields, ___U3CU3E9__29_0_4)); }
	inline Func_3_t11D22BBD9531DD4E1FDE18B3937E1DC07ED8DB79 * get_U3CU3E9__29_0_4() const { return ___U3CU3E9__29_0_4; }
	inline Func_3_t11D22BBD9531DD4E1FDE18B3937E1DC07ED8DB79 ** get_address_of_U3CU3E9__29_0_4() { return &___U3CU3E9__29_0_4; }
	inline void set_U3CU3E9__29_0_4(Func_3_t11D22BBD9531DD4E1FDE18B3937E1DC07ED8DB79 * value)
	{
		___U3CU3E9__29_0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__29_0_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TD42D911B6777DD155EA9826FA9CE2DD0A8D53944_H
#ifndef U3CU3EC__DISPLAYCLASS13_0_T8C9B7A792A631ED0604A908F4C081ADFE35B576F_H
#define U3CU3EC__DISPLAYCLASS13_0_T8C9B7A792A631ED0604A908F4C081ADFE35B576F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.FromBinder_<>c__DisplayClass13_0
struct  U3CU3Ec__DisplayClass13_0_t8C9B7A792A631ED0604A908F4C081ADFE35B576F  : public RuntimeObject
{
public:
	// System.Object Zenject.FromBinder_<>c__DisplayClass13_0::subIdentifier
	RuntimeObject * ___subIdentifier_0;

public:
	inline static int32_t get_offset_of_subIdentifier_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass13_0_t8C9B7A792A631ED0604A908F4C081ADFE35B576F, ___subIdentifier_0)); }
	inline RuntimeObject * get_subIdentifier_0() const { return ___subIdentifier_0; }
	inline RuntimeObject ** get_address_of_subIdentifier_0() { return &___subIdentifier_0; }
	inline void set_subIdentifier_0(RuntimeObject * value)
	{
		___subIdentifier_0 = value;
		Il2CppCodeGenWriteBarrier((&___subIdentifier_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS13_0_T8C9B7A792A631ED0604A908F4C081ADFE35B576F_H
#ifndef U3CU3EC__DISPLAYCLASS16_0_TAF427052AE85F370174DDB84CA0B5A8E5438A375_H
#define U3CU3EC__DISPLAYCLASS16_0_TAF427052AE85F370174DDB84CA0B5A8E5438A375_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.FromBinder_<>c__DisplayClass16_0
struct  U3CU3Ec__DisplayClass16_0_tAF427052AE85F370174DDB84CA0B5A8E5438A375  : public RuntimeObject
{
public:
	// System.Type Zenject.FromBinder_<>c__DisplayClass16_0::factoryType
	Type_t * ___factoryType_0;
	// Zenject.FromBinder Zenject.FromBinder_<>c__DisplayClass16_0::<>4__this
	FromBinder_t69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_factoryType_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass16_0_tAF427052AE85F370174DDB84CA0B5A8E5438A375, ___factoryType_0)); }
	inline Type_t * get_factoryType_0() const { return ___factoryType_0; }
	inline Type_t ** get_address_of_factoryType_0() { return &___factoryType_0; }
	inline void set_factoryType_0(Type_t * value)
	{
		___factoryType_0 = value;
		Il2CppCodeGenWriteBarrier((&___factoryType_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass16_0_tAF427052AE85F370174DDB84CA0B5A8E5438A375, ___U3CU3E4__this_1)); }
	inline FromBinder_t69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline FromBinder_t69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(FromBinder_t69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS16_0_TAF427052AE85F370174DDB84CA0B5A8E5438A375_H
#ifndef U3CU3EC__DISPLAYCLASS17_0_TC648F1B5B583F7E0D8A3844E0ADD99F8BA479666_H
#define U3CU3EC__DISPLAYCLASS17_0_TC648F1B5B583F7E0D8A3844E0ADD99F8BA479666_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.FromBinder_<>c__DisplayClass17_0
struct  U3CU3Ec__DisplayClass17_0_tC648F1B5B583F7E0D8A3844E0ADD99F8BA479666  : public RuntimeObject
{
public:
	// UnityEngine.GameObject Zenject.FromBinder_<>c__DisplayClass17_0::gameObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___gameObject_0;
	// Zenject.FromBinder Zenject.FromBinder_<>c__DisplayClass17_0::<>4__this
	FromBinder_t69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_gameObject_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass17_0_tC648F1B5B583F7E0D8A3844E0ADD99F8BA479666, ___gameObject_0)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_gameObject_0() const { return ___gameObject_0; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_gameObject_0() { return &___gameObject_0; }
	inline void set_gameObject_0(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___gameObject_0 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass17_0_tC648F1B5B583F7E0D8A3844E0ADD99F8BA479666, ___U3CU3E4__this_1)); }
	inline FromBinder_t69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline FromBinder_t69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(FromBinder_t69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS17_0_TC648F1B5B583F7E0D8A3844E0ADD99F8BA479666_H
#ifndef U3CU3EC__DISPLAYCLASS18_0_T3FF94B09CF1F4F7BDB70BE103424A29CD3211973_H
#define U3CU3EC__DISPLAYCLASS18_0_T3FF94B09CF1F4F7BDB70BE103424A29CD3211973_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.FromBinder_<>c__DisplayClass18_0
struct  U3CU3Ec__DisplayClass18_0_t3FF94B09CF1F4F7BDB70BE103424A29CD3211973  : public RuntimeObject
{
public:
	// System.Func`2<Zenject.InjectContext,UnityEngine.GameObject> Zenject.FromBinder_<>c__DisplayClass18_0::gameObjectGetter
	Func_2_tD5D4A9F4A7BCFBB67D83DD4F52BFE19A8153D2D1 * ___gameObjectGetter_0;
	// Zenject.FromBinder Zenject.FromBinder_<>c__DisplayClass18_0::<>4__this
	FromBinder_t69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_gameObjectGetter_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass18_0_t3FF94B09CF1F4F7BDB70BE103424A29CD3211973, ___gameObjectGetter_0)); }
	inline Func_2_tD5D4A9F4A7BCFBB67D83DD4F52BFE19A8153D2D1 * get_gameObjectGetter_0() const { return ___gameObjectGetter_0; }
	inline Func_2_tD5D4A9F4A7BCFBB67D83DD4F52BFE19A8153D2D1 ** get_address_of_gameObjectGetter_0() { return &___gameObjectGetter_0; }
	inline void set_gameObjectGetter_0(Func_2_tD5D4A9F4A7BCFBB67D83DD4F52BFE19A8153D2D1 * value)
	{
		___gameObjectGetter_0 = value;
		Il2CppCodeGenWriteBarrier((&___gameObjectGetter_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass18_0_t3FF94B09CF1F4F7BDB70BE103424A29CD3211973, ___U3CU3E4__this_1)); }
	inline FromBinder_t69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline FromBinder_t69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(FromBinder_t69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS18_0_T3FF94B09CF1F4F7BDB70BE103424A29CD3211973_H
#ifndef U3CU3EC__DISPLAYCLASS21_0_TB5FCF8489A0035459C86E3B94CFFBE6C32BA16B5_H
#define U3CU3EC__DISPLAYCLASS21_0_TB5FCF8489A0035459C86E3B94CFFBE6C32BA16B5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.FromBinder_<>c__DisplayClass21_0
struct  U3CU3Ec__DisplayClass21_0_tB5FCF8489A0035459C86E3B94CFFBE6C32BA16B5  : public RuntimeObject
{
public:
	// Zenject.FromBinder Zenject.FromBinder_<>c__DisplayClass21_0::<>4__this
	FromBinder_t69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75 * ___U3CU3E4__this_0;
	// Zenject.GameObjectCreationParameters Zenject.FromBinder_<>c__DisplayClass21_0::gameObjectInfo
	GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A * ___gameObjectInfo_1;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass21_0_tB5FCF8489A0035459C86E3B94CFFBE6C32BA16B5, ___U3CU3E4__this_0)); }
	inline FromBinder_t69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline FromBinder_t69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(FromBinder_t69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_gameObjectInfo_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass21_0_tB5FCF8489A0035459C86E3B94CFFBE6C32BA16B5, ___gameObjectInfo_1)); }
	inline GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A * get_gameObjectInfo_1() const { return ___gameObjectInfo_1; }
	inline GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A ** get_address_of_gameObjectInfo_1() { return &___gameObjectInfo_1; }
	inline void set_gameObjectInfo_1(GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A * value)
	{
		___gameObjectInfo_1 = value;
		Il2CppCodeGenWriteBarrier((&___gameObjectInfo_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS21_0_TB5FCF8489A0035459C86E3B94CFFBE6C32BA16B5_H
#ifndef U3CU3EC__DISPLAYCLASS32_0_T42768AFF71357DC9C1410EB7B8EBD5FC01F7D94F_H
#define U3CU3EC__DISPLAYCLASS32_0_T42768AFF71357DC9C1410EB7B8EBD5FC01F7D94F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.FromBinder_<>c__DisplayClass32_0
struct  U3CU3Ec__DisplayClass32_0_t42768AFF71357DC9C1410EB7B8EBD5FC01F7D94F  : public RuntimeObject
{
public:
	// System.String Zenject.FromBinder_<>c__DisplayClass32_0::resourcePath
	String_t* ___resourcePath_0;
	// Zenject.FromBinder Zenject.FromBinder_<>c__DisplayClass32_0::<>4__this
	FromBinder_t69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75 * ___U3CU3E4__this_1;
	// System.Boolean Zenject.FromBinder_<>c__DisplayClass32_0::createNew
	bool ___createNew_2;

public:
	inline static int32_t get_offset_of_resourcePath_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass32_0_t42768AFF71357DC9C1410EB7B8EBD5FC01F7D94F, ___resourcePath_0)); }
	inline String_t* get_resourcePath_0() const { return ___resourcePath_0; }
	inline String_t** get_address_of_resourcePath_0() { return &___resourcePath_0; }
	inline void set_resourcePath_0(String_t* value)
	{
		___resourcePath_0 = value;
		Il2CppCodeGenWriteBarrier((&___resourcePath_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass32_0_t42768AFF71357DC9C1410EB7B8EBD5FC01F7D94F, ___U3CU3E4__this_1)); }
	inline FromBinder_t69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline FromBinder_t69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(FromBinder_t69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}

	inline static int32_t get_offset_of_createNew_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass32_0_t42768AFF71357DC9C1410EB7B8EBD5FC01F7D94F, ___createNew_2)); }
	inline bool get_createNew_2() const { return ___createNew_2; }
	inline bool* get_address_of_createNew_2() { return &___createNew_2; }
	inline void set_createNew_2(bool value)
	{
		___createNew_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS32_0_T42768AFF71357DC9C1410EB7B8EBD5FC01F7D94F_H
#ifndef U3CU3EC__DISPLAYCLASS33_0_T7B859D0DF90A1C3EBAF80FD80916EDC2998CB576_H
#define U3CU3EC__DISPLAYCLASS33_0_T7B859D0DF90A1C3EBAF80FD80916EDC2998CB576_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.FromBinder_<>c__DisplayClass33_0
struct  U3CU3Ec__DisplayClass33_0_t7B859D0DF90A1C3EBAF80FD80916EDC2998CB576  : public RuntimeObject
{
public:
	// System.String Zenject.FromBinder_<>c__DisplayClass33_0::resourcePath
	String_t* ___resourcePath_0;

public:
	inline static int32_t get_offset_of_resourcePath_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass33_0_t7B859D0DF90A1C3EBAF80FD80916EDC2998CB576, ___resourcePath_0)); }
	inline String_t* get_resourcePath_0() const { return ___resourcePath_0; }
	inline String_t** get_address_of_resourcePath_0() { return &___resourcePath_0; }
	inline void set_resourcePath_0(String_t* value)
	{
		___resourcePath_0 = value;
		Il2CppCodeGenWriteBarrier((&___resourcePath_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS33_0_T7B859D0DF90A1C3EBAF80FD80916EDC2998CB576_H
#ifndef U3CU3EC__DISPLAYCLASS34_0_TFC082F8B42385B131EB243C4AC9B1A282BE953CE_H
#define U3CU3EC__DISPLAYCLASS34_0_TFC082F8B42385B131EB243C4AC9B1A282BE953CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.FromBinder_<>c__DisplayClass34_0
struct  U3CU3Ec__DisplayClass34_0_tFC082F8B42385B131EB243C4AC9B1A282BE953CE  : public RuntimeObject
{
public:
	// System.Func`2<Zenject.InjectContext,System.Object> Zenject.FromBinder_<>c__DisplayClass34_0::method
	Func_2_tE24EB23C7D8F4AD0A3A34C5DB2C9F329886F48F2 * ___method_0;

public:
	inline static int32_t get_offset_of_method_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass34_0_tFC082F8B42385B131EB243C4AC9B1A282BE953CE, ___method_0)); }
	inline Func_2_tE24EB23C7D8F4AD0A3A34C5DB2C9F329886F48F2 * get_method_0() const { return ___method_0; }
	inline Func_2_tE24EB23C7D8F4AD0A3A34C5DB2C9F329886F48F2 ** get_address_of_method_0() { return &___method_0; }
	inline void set_method_0(Func_2_tE24EB23C7D8F4AD0A3A34C5DB2C9F329886F48F2 * value)
	{
		___method_0 = value;
		Il2CppCodeGenWriteBarrier((&___method_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS34_0_TFC082F8B42385B131EB243C4AC9B1A282BE953CE_H
#ifndef U3CU3EC__DISPLAYCLASS39_0_T8D3E47005D45F205218A4C929EE9C9C94BA68083_H
#define U3CU3EC__DISPLAYCLASS39_0_T8D3E47005D45F205218A4C929EE9C9C94BA68083_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.FromBinder_<>c__DisplayClass39_0
struct  U3CU3Ec__DisplayClass39_0_t8D3E47005D45F205218A4C929EE9C9C94BA68083  : public RuntimeObject
{
public:
	// System.Object Zenject.FromBinder_<>c__DisplayClass39_0::instance
	RuntimeObject * ___instance_0;

public:
	inline static int32_t get_offset_of_instance_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass39_0_t8D3E47005D45F205218A4C929EE9C9C94BA68083, ___instance_0)); }
	inline RuntimeObject * get_instance_0() const { return ___instance_0; }
	inline RuntimeObject ** get_address_of_instance_0() { return &___instance_0; }
	inline void set_instance_0(RuntimeObject * value)
	{
		___instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS39_0_T8D3E47005D45F205218A4C929EE9C9C94BA68083_H
#ifndef IDBINDER_T775EA698C1098F885F2C2C78D2535B79AD0AF012_H
#define IDBINDER_T775EA698C1098F885F2C2C78D2535B79AD0AF012_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.IdBinder
struct  IdBinder_t775EA698C1098F885F2C2C78D2535B79AD0AF012  : public RuntimeObject
{
public:
	// Zenject.BindInfo Zenject.IdBinder::_bindInfo
	BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * ____bindInfo_0;

public:
	inline static int32_t get_offset_of__bindInfo_0() { return static_cast<int32_t>(offsetof(IdBinder_t775EA698C1098F885F2C2C78D2535B79AD0AF012, ____bindInfo_0)); }
	inline BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * get__bindInfo_0() const { return ____bindInfo_0; }
	inline BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 ** get_address_of__bindInfo_0() { return &____bindInfo_0; }
	inline void set__bindInfo_0(BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * value)
	{
		____bindInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&____bindInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IDBINDER_T775EA698C1098F885F2C2C78D2535B79AD0AF012_H
#ifndef NONLAZYBINDER_TA9FB0768A2F39559FBDDCAFBF940AAD240B7C43C_H
#define NONLAZYBINDER_TA9FB0768A2F39559FBDDCAFBF940AAD240B7C43C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.NonLazyBinder
struct  NonLazyBinder_tA9FB0768A2F39559FBDDCAFBF940AAD240B7C43C  : public RuntimeObject
{
public:
	// Zenject.BindInfo Zenject.NonLazyBinder::<BindInfo>k__BackingField
	BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * ___U3CBindInfoU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CBindInfoU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(NonLazyBinder_tA9FB0768A2F39559FBDDCAFBF940AAD240B7C43C, ___U3CBindInfoU3Ek__BackingField_0)); }
	inline BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * get_U3CBindInfoU3Ek__BackingField_0() const { return ___U3CBindInfoU3Ek__BackingField_0; }
	inline BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 ** get_address_of_U3CBindInfoU3Ek__BackingField_0() { return &___U3CBindInfoU3Ek__BackingField_0; }
	inline void set_U3CBindInfoU3Ek__BackingField_0(BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * value)
	{
		___U3CBindInfoU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBindInfoU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NONLAZYBINDER_TA9FB0768A2F39559FBDDCAFBF940AAD240B7C43C_H
#ifndef NULLBINDINGFINALIZER_TFEAF0122BA4AF8A5209DE0330F0BFF444F06AD65_H
#define NULLBINDINGFINALIZER_TFEAF0122BA4AF8A5209DE0330F0BFF444F06AD65_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.NullBindingFinalizer
struct  NullBindingFinalizer_tFEAF0122BA4AF8A5209DE0330F0BFF444F06AD65  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLBINDINGFINALIZER_TFEAF0122BA4AF8A5209DE0330F0BFF444F06AD65_H
#ifndef U3CU3EC__DISPLAYCLASS5_0_T25A424B5F0CD0CBB038687263E0AA9B93DA940DA_H
#define U3CU3EC__DISPLAYCLASS5_0_T25A424B5F0CD0CBB038687263E0AA9B93DA940DA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.PrefabBindingFinalizer_<>c__DisplayClass5_0
struct  U3CU3Ec__DisplayClass5_0_t25A424B5F0CD0CBB038687263E0AA9B93DA940DA  : public RuntimeObject
{
public:
	// Zenject.DiContainer Zenject.PrefabBindingFinalizer_<>c__DisplayClass5_0::container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ___container_0;
	// Zenject.PrefabBindingFinalizer Zenject.PrefabBindingFinalizer_<>c__DisplayClass5_0::<>4__this
	PrefabBindingFinalizer_t355616E5B859E3E8B597F710035FAE139DF073FE * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_container_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_t25A424B5F0CD0CBB038687263E0AA9B93DA940DA, ___container_0)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get_container_0() const { return ___container_0; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of_container_0() { return &___container_0; }
	inline void set_container_0(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		___container_0 = value;
		Il2CppCodeGenWriteBarrier((&___container_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_t25A424B5F0CD0CBB038687263E0AA9B93DA940DA, ___U3CU3E4__this_1)); }
	inline PrefabBindingFinalizer_t355616E5B859E3E8B597F710035FAE139DF073FE * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline PrefabBindingFinalizer_t355616E5B859E3E8B597F710035FAE139DF073FE ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(PrefabBindingFinalizer_t355616E5B859E3E8B597F710035FAE139DF073FE * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS5_0_T25A424B5F0CD0CBB038687263E0AA9B93DA940DA_H
#ifndef U3CU3EC__DISPLAYCLASS5_1_T57E7420F85E66387484EEC4ADD7C03D081763C17_H
#define U3CU3EC__DISPLAYCLASS5_1_T57E7420F85E66387484EEC4ADD7C03D081763C17_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.PrefabBindingFinalizer_<>c__DisplayClass5_1
struct  U3CU3Ec__DisplayClass5_1_t57E7420F85E66387484EEC4ADD7C03D081763C17  : public RuntimeObject
{
public:
	// Zenject.PrefabInstantiatorCached Zenject.PrefabBindingFinalizer_<>c__DisplayClass5_1::prefabCreator
	PrefabInstantiatorCached_tB23BE6E42DED15DF8DD349E6E8C80C45CBDA95D5 * ___prefabCreator_0;
	// Zenject.PrefabBindingFinalizer_<>c__DisplayClass5_0 Zenject.PrefabBindingFinalizer_<>c__DisplayClass5_1::CSU24<>8__locals1
	U3CU3Ec__DisplayClass5_0_t25A424B5F0CD0CBB038687263E0AA9B93DA940DA * ___CSU24U3CU3E8__locals1_1;

public:
	inline static int32_t get_offset_of_prefabCreator_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_1_t57E7420F85E66387484EEC4ADD7C03D081763C17, ___prefabCreator_0)); }
	inline PrefabInstantiatorCached_tB23BE6E42DED15DF8DD349E6E8C80C45CBDA95D5 * get_prefabCreator_0() const { return ___prefabCreator_0; }
	inline PrefabInstantiatorCached_tB23BE6E42DED15DF8DD349E6E8C80C45CBDA95D5 ** get_address_of_prefabCreator_0() { return &___prefabCreator_0; }
	inline void set_prefabCreator_0(PrefabInstantiatorCached_tB23BE6E42DED15DF8DD349E6E8C80C45CBDA95D5 * value)
	{
		___prefabCreator_0 = value;
		Il2CppCodeGenWriteBarrier((&___prefabCreator_0), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E8__locals1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_1_t57E7420F85E66387484EEC4ADD7C03D081763C17, ___CSU24U3CU3E8__locals1_1)); }
	inline U3CU3Ec__DisplayClass5_0_t25A424B5F0CD0CBB038687263E0AA9B93DA940DA * get_CSU24U3CU3E8__locals1_1() const { return ___CSU24U3CU3E8__locals1_1; }
	inline U3CU3Ec__DisplayClass5_0_t25A424B5F0CD0CBB038687263E0AA9B93DA940DA ** get_address_of_CSU24U3CU3E8__locals1_1() { return &___CSU24U3CU3E8__locals1_1; }
	inline void set_CSU24U3CU3E8__locals1_1(U3CU3Ec__DisplayClass5_0_t25A424B5F0CD0CBB038687263E0AA9B93DA940DA * value)
	{
		___CSU24U3CU3E8__locals1_1 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E8__locals1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS5_1_T57E7420F85E66387484EEC4ADD7C03D081763C17_H
#ifndef U3CU3EC__DISPLAYCLASS6_0_TCA46D26FA4A790B5596A5EEB43024EABF63DD91E_H
#define U3CU3EC__DISPLAYCLASS6_0_TCA46D26FA4A790B5596A5EEB43024EABF63DD91E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.PrefabBindingFinalizer_<>c__DisplayClass6_0
struct  U3CU3Ec__DisplayClass6_0_tCA46D26FA4A790B5596A5EEB43024EABF63DD91E  : public RuntimeObject
{
public:
	// Zenject.DiContainer Zenject.PrefabBindingFinalizer_<>c__DisplayClass6_0::container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ___container_0;
	// Zenject.PrefabBindingFinalizer Zenject.PrefabBindingFinalizer_<>c__DisplayClass6_0::<>4__this
	PrefabBindingFinalizer_t355616E5B859E3E8B597F710035FAE139DF073FE * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_container_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_tCA46D26FA4A790B5596A5EEB43024EABF63DD91E, ___container_0)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get_container_0() const { return ___container_0; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of_container_0() { return &___container_0; }
	inline void set_container_0(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		___container_0 = value;
		Il2CppCodeGenWriteBarrier((&___container_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_tCA46D26FA4A790B5596A5EEB43024EABF63DD91E, ___U3CU3E4__this_1)); }
	inline PrefabBindingFinalizer_t355616E5B859E3E8B597F710035FAE139DF073FE * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline PrefabBindingFinalizer_t355616E5B859E3E8B597F710035FAE139DF073FE ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(PrefabBindingFinalizer_t355616E5B859E3E8B597F710035FAE139DF073FE * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS6_0_TCA46D26FA4A790B5596A5EEB43024EABF63DD91E_H
#ifndef U3CU3EC__DISPLAYCLASS6_1_T2CBD0732A519FC58DECC5102B741A42EFE09C381_H
#define U3CU3EC__DISPLAYCLASS6_1_T2CBD0732A519FC58DECC5102B741A42EFE09C381_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.PrefabBindingFinalizer_<>c__DisplayClass6_1
struct  U3CU3Ec__DisplayClass6_1_t2CBD0732A519FC58DECC5102B741A42EFE09C381  : public RuntimeObject
{
public:
	// Zenject.PrefabInstantiatorCached Zenject.PrefabBindingFinalizer_<>c__DisplayClass6_1::prefabCreator
	PrefabInstantiatorCached_tB23BE6E42DED15DF8DD349E6E8C80C45CBDA95D5 * ___prefabCreator_0;
	// Zenject.PrefabBindingFinalizer_<>c__DisplayClass6_0 Zenject.PrefabBindingFinalizer_<>c__DisplayClass6_1::CSU24<>8__locals1
	U3CU3Ec__DisplayClass6_0_tCA46D26FA4A790B5596A5EEB43024EABF63DD91E * ___CSU24U3CU3E8__locals1_1;

public:
	inline static int32_t get_offset_of_prefabCreator_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_1_t2CBD0732A519FC58DECC5102B741A42EFE09C381, ___prefabCreator_0)); }
	inline PrefabInstantiatorCached_tB23BE6E42DED15DF8DD349E6E8C80C45CBDA95D5 * get_prefabCreator_0() const { return ___prefabCreator_0; }
	inline PrefabInstantiatorCached_tB23BE6E42DED15DF8DD349E6E8C80C45CBDA95D5 ** get_address_of_prefabCreator_0() { return &___prefabCreator_0; }
	inline void set_prefabCreator_0(PrefabInstantiatorCached_tB23BE6E42DED15DF8DD349E6E8C80C45CBDA95D5 * value)
	{
		___prefabCreator_0 = value;
		Il2CppCodeGenWriteBarrier((&___prefabCreator_0), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E8__locals1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_1_t2CBD0732A519FC58DECC5102B741A42EFE09C381, ___CSU24U3CU3E8__locals1_1)); }
	inline U3CU3Ec__DisplayClass6_0_tCA46D26FA4A790B5596A5EEB43024EABF63DD91E * get_CSU24U3CU3E8__locals1_1() const { return ___CSU24U3CU3E8__locals1_1; }
	inline U3CU3Ec__DisplayClass6_0_tCA46D26FA4A790B5596A5EEB43024EABF63DD91E ** get_address_of_CSU24U3CU3E8__locals1_1() { return &___CSU24U3CU3E8__locals1_1; }
	inline void set_CSU24U3CU3E8__locals1_1(U3CU3Ec__DisplayClass6_0_tCA46D26FA4A790B5596A5EEB43024EABF63DD91E * value)
	{
		___CSU24U3CU3E8__locals1_1 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E8__locals1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS6_1_T2CBD0732A519FC58DECC5102B741A42EFE09C381_H
#ifndef U3CU3EC__DISPLAYCLASS5_0_TC4DFEACC4F54B76864B4C241BAB809A1D105B6F3_H
#define U3CU3EC__DISPLAYCLASS5_0_TC4DFEACC4F54B76864B4C241BAB809A1D105B6F3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.PrefabResourceBindingFinalizer_<>c__DisplayClass5_0
struct  U3CU3Ec__DisplayClass5_0_tC4DFEACC4F54B76864B4C241BAB809A1D105B6F3  : public RuntimeObject
{
public:
	// Zenject.DiContainer Zenject.PrefabResourceBindingFinalizer_<>c__DisplayClass5_0::container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ___container_0;
	// Zenject.PrefabResourceBindingFinalizer Zenject.PrefabResourceBindingFinalizer_<>c__DisplayClass5_0::<>4__this
	PrefabResourceBindingFinalizer_t8527DD2D663940B5BE41029F8B5C51128D446CC4 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_container_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_tC4DFEACC4F54B76864B4C241BAB809A1D105B6F3, ___container_0)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get_container_0() const { return ___container_0; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of_container_0() { return &___container_0; }
	inline void set_container_0(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		___container_0 = value;
		Il2CppCodeGenWriteBarrier((&___container_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_tC4DFEACC4F54B76864B4C241BAB809A1D105B6F3, ___U3CU3E4__this_1)); }
	inline PrefabResourceBindingFinalizer_t8527DD2D663940B5BE41029F8B5C51128D446CC4 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline PrefabResourceBindingFinalizer_t8527DD2D663940B5BE41029F8B5C51128D446CC4 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(PrefabResourceBindingFinalizer_t8527DD2D663940B5BE41029F8B5C51128D446CC4 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS5_0_TC4DFEACC4F54B76864B4C241BAB809A1D105B6F3_H
#ifndef U3CU3EC__DISPLAYCLASS5_1_T365ACF1A4C33286F722DC1C73486121465F5C606_H
#define U3CU3EC__DISPLAYCLASS5_1_T365ACF1A4C33286F722DC1C73486121465F5C606_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.PrefabResourceBindingFinalizer_<>c__DisplayClass5_1
struct  U3CU3Ec__DisplayClass5_1_t365ACF1A4C33286F722DC1C73486121465F5C606  : public RuntimeObject
{
public:
	// Zenject.PrefabInstantiatorCached Zenject.PrefabResourceBindingFinalizer_<>c__DisplayClass5_1::prefabCreator
	PrefabInstantiatorCached_tB23BE6E42DED15DF8DD349E6E8C80C45CBDA95D5 * ___prefabCreator_0;
	// Zenject.PrefabResourceBindingFinalizer_<>c__DisplayClass5_0 Zenject.PrefabResourceBindingFinalizer_<>c__DisplayClass5_1::CSU24<>8__locals1
	U3CU3Ec__DisplayClass5_0_tC4DFEACC4F54B76864B4C241BAB809A1D105B6F3 * ___CSU24U3CU3E8__locals1_1;

public:
	inline static int32_t get_offset_of_prefabCreator_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_1_t365ACF1A4C33286F722DC1C73486121465F5C606, ___prefabCreator_0)); }
	inline PrefabInstantiatorCached_tB23BE6E42DED15DF8DD349E6E8C80C45CBDA95D5 * get_prefabCreator_0() const { return ___prefabCreator_0; }
	inline PrefabInstantiatorCached_tB23BE6E42DED15DF8DD349E6E8C80C45CBDA95D5 ** get_address_of_prefabCreator_0() { return &___prefabCreator_0; }
	inline void set_prefabCreator_0(PrefabInstantiatorCached_tB23BE6E42DED15DF8DD349E6E8C80C45CBDA95D5 * value)
	{
		___prefabCreator_0 = value;
		Il2CppCodeGenWriteBarrier((&___prefabCreator_0), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E8__locals1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_1_t365ACF1A4C33286F722DC1C73486121465F5C606, ___CSU24U3CU3E8__locals1_1)); }
	inline U3CU3Ec__DisplayClass5_0_tC4DFEACC4F54B76864B4C241BAB809A1D105B6F3 * get_CSU24U3CU3E8__locals1_1() const { return ___CSU24U3CU3E8__locals1_1; }
	inline U3CU3Ec__DisplayClass5_0_tC4DFEACC4F54B76864B4C241BAB809A1D105B6F3 ** get_address_of_CSU24U3CU3E8__locals1_1() { return &___CSU24U3CU3E8__locals1_1; }
	inline void set_CSU24U3CU3E8__locals1_1(U3CU3Ec__DisplayClass5_0_tC4DFEACC4F54B76864B4C241BAB809A1D105B6F3 * value)
	{
		___CSU24U3CU3E8__locals1_1 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E8__locals1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS5_1_T365ACF1A4C33286F722DC1C73486121465F5C606_H
#ifndef U3CU3EC__DISPLAYCLASS6_0_T8DCC1F5B55699D1CFF1D77EC63B83C993536E074_H
#define U3CU3EC__DISPLAYCLASS6_0_T8DCC1F5B55699D1CFF1D77EC63B83C993536E074_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.PrefabResourceBindingFinalizer_<>c__DisplayClass6_0
struct  U3CU3Ec__DisplayClass6_0_t8DCC1F5B55699D1CFF1D77EC63B83C993536E074  : public RuntimeObject
{
public:
	// Zenject.DiContainer Zenject.PrefabResourceBindingFinalizer_<>c__DisplayClass6_0::container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ___container_0;
	// Zenject.PrefabResourceBindingFinalizer Zenject.PrefabResourceBindingFinalizer_<>c__DisplayClass6_0::<>4__this
	PrefabResourceBindingFinalizer_t8527DD2D663940B5BE41029F8B5C51128D446CC4 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_container_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_t8DCC1F5B55699D1CFF1D77EC63B83C993536E074, ___container_0)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get_container_0() const { return ___container_0; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of_container_0() { return &___container_0; }
	inline void set_container_0(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		___container_0 = value;
		Il2CppCodeGenWriteBarrier((&___container_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_t8DCC1F5B55699D1CFF1D77EC63B83C993536E074, ___U3CU3E4__this_1)); }
	inline PrefabResourceBindingFinalizer_t8527DD2D663940B5BE41029F8B5C51128D446CC4 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline PrefabResourceBindingFinalizer_t8527DD2D663940B5BE41029F8B5C51128D446CC4 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(PrefabResourceBindingFinalizer_t8527DD2D663940B5BE41029F8B5C51128D446CC4 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS6_0_T8DCC1F5B55699D1CFF1D77EC63B83C993536E074_H
#ifndef U3CU3EC__DISPLAYCLASS6_1_T82C961472E9F241D5D1F1481A1F2D4F651A479E0_H
#define U3CU3EC__DISPLAYCLASS6_1_T82C961472E9F241D5D1F1481A1F2D4F651A479E0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.PrefabResourceBindingFinalizer_<>c__DisplayClass6_1
struct  U3CU3Ec__DisplayClass6_1_t82C961472E9F241D5D1F1481A1F2D4F651A479E0  : public RuntimeObject
{
public:
	// Zenject.PrefabInstantiatorCached Zenject.PrefabResourceBindingFinalizer_<>c__DisplayClass6_1::prefabCreator
	PrefabInstantiatorCached_tB23BE6E42DED15DF8DD349E6E8C80C45CBDA95D5 * ___prefabCreator_0;
	// Zenject.PrefabResourceBindingFinalizer_<>c__DisplayClass6_0 Zenject.PrefabResourceBindingFinalizer_<>c__DisplayClass6_1::CSU24<>8__locals1
	U3CU3Ec__DisplayClass6_0_t8DCC1F5B55699D1CFF1D77EC63B83C993536E074 * ___CSU24U3CU3E8__locals1_1;

public:
	inline static int32_t get_offset_of_prefabCreator_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_1_t82C961472E9F241D5D1F1481A1F2D4F651A479E0, ___prefabCreator_0)); }
	inline PrefabInstantiatorCached_tB23BE6E42DED15DF8DD349E6E8C80C45CBDA95D5 * get_prefabCreator_0() const { return ___prefabCreator_0; }
	inline PrefabInstantiatorCached_tB23BE6E42DED15DF8DD349E6E8C80C45CBDA95D5 ** get_address_of_prefabCreator_0() { return &___prefabCreator_0; }
	inline void set_prefabCreator_0(PrefabInstantiatorCached_tB23BE6E42DED15DF8DD349E6E8C80C45CBDA95D5 * value)
	{
		___prefabCreator_0 = value;
		Il2CppCodeGenWriteBarrier((&___prefabCreator_0), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E8__locals1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_1_t82C961472E9F241D5D1F1481A1F2D4F651A479E0, ___CSU24U3CU3E8__locals1_1)); }
	inline U3CU3Ec__DisplayClass6_0_t8DCC1F5B55699D1CFF1D77EC63B83C993536E074 * get_CSU24U3CU3E8__locals1_1() const { return ___CSU24U3CU3E8__locals1_1; }
	inline U3CU3Ec__DisplayClass6_0_t8DCC1F5B55699D1CFF1D77EC63B83C993536E074 ** get_address_of_CSU24U3CU3E8__locals1_1() { return &___CSU24U3CU3E8__locals1_1; }
	inline void set_CSU24U3CU3E8__locals1_1(U3CU3Ec__DisplayClass6_0_t8DCC1F5B55699D1CFF1D77EC63B83C993536E074 * value)
	{
		___CSU24U3CU3E8__locals1_1 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E8__locals1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS6_1_T82C961472E9F241D5D1F1481A1F2D4F651A479E0_H
#ifndef PROVIDERBINDINGFINALIZER_T3C2E6FBD89B98A057674B926F42EF13E4ED1EAAE_H
#define PROVIDERBINDINGFINALIZER_T3C2E6FBD89B98A057674B926F42EF13E4ED1EAAE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ProviderBindingFinalizer
struct  ProviderBindingFinalizer_t3C2E6FBD89B98A057674B926F42EF13E4ED1EAAE  : public RuntimeObject
{
public:
	// Zenject.BindInfo Zenject.ProviderBindingFinalizer::<BindInfo>k__BackingField
	BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * ___U3CBindInfoU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CBindInfoU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ProviderBindingFinalizer_t3C2E6FBD89B98A057674B926F42EF13E4ED1EAAE, ___U3CBindInfoU3Ek__BackingField_0)); }
	inline BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * get_U3CBindInfoU3Ek__BackingField_0() const { return ___U3CBindInfoU3Ek__BackingField_0; }
	inline BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 ** get_address_of_U3CBindInfoU3Ek__BackingField_0() { return &___U3CBindInfoU3Ek__BackingField_0; }
	inline void set_U3CBindInfoU3Ek__BackingField_0(BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * value)
	{
		___U3CBindInfoU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBindInfoU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROVIDERBINDINGFINALIZER_T3C2E6FBD89B98A057674B926F42EF13E4ED1EAAE_H
#ifndef SUBCONTAINERBINDER_T24411FF88DADEE6DFBC16EE722E0F4944E8732D6_H
#define SUBCONTAINERBINDER_T24411FF88DADEE6DFBC16EE722E0F4944E8732D6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerBinder
struct  SubContainerBinder_t24411FF88DADEE6DFBC16EE722E0F4944E8732D6  : public RuntimeObject
{
public:
	// Zenject.BindInfo Zenject.SubContainerBinder::_bindInfo
	BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * ____bindInfo_0;
	// Zenject.BindFinalizerWrapper Zenject.SubContainerBinder::_finalizerWrapper
	BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 * ____finalizerWrapper_1;
	// System.Object Zenject.SubContainerBinder::_subIdentifier
	RuntimeObject * ____subIdentifier_2;

public:
	inline static int32_t get_offset_of__bindInfo_0() { return static_cast<int32_t>(offsetof(SubContainerBinder_t24411FF88DADEE6DFBC16EE722E0F4944E8732D6, ____bindInfo_0)); }
	inline BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * get__bindInfo_0() const { return ____bindInfo_0; }
	inline BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 ** get_address_of__bindInfo_0() { return &____bindInfo_0; }
	inline void set__bindInfo_0(BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * value)
	{
		____bindInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&____bindInfo_0), value);
	}

	inline static int32_t get_offset_of__finalizerWrapper_1() { return static_cast<int32_t>(offsetof(SubContainerBinder_t24411FF88DADEE6DFBC16EE722E0F4944E8732D6, ____finalizerWrapper_1)); }
	inline BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 * get__finalizerWrapper_1() const { return ____finalizerWrapper_1; }
	inline BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 ** get_address_of__finalizerWrapper_1() { return &____finalizerWrapper_1; }
	inline void set__finalizerWrapper_1(BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 * value)
	{
		____finalizerWrapper_1 = value;
		Il2CppCodeGenWriteBarrier((&____finalizerWrapper_1), value);
	}

	inline static int32_t get_offset_of__subIdentifier_2() { return static_cast<int32_t>(offsetof(SubContainerBinder_t24411FF88DADEE6DFBC16EE722E0F4944E8732D6, ____subIdentifier_2)); }
	inline RuntimeObject * get__subIdentifier_2() const { return ____subIdentifier_2; }
	inline RuntimeObject ** get_address_of__subIdentifier_2() { return &____subIdentifier_2; }
	inline void set__subIdentifier_2(RuntimeObject * value)
	{
		____subIdentifier_2 = value;
		Il2CppCodeGenWriteBarrier((&____subIdentifier_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBCONTAINERBINDER_T24411FF88DADEE6DFBC16EE722E0F4944E8732D6_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#define QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef ARGNONLAZYBINDER_T56DEDF75B9E224623A2E117702BC79D8C9E84383_H
#define ARGNONLAZYBINDER_T56DEDF75B9E224623A2E117702BC79D8C9E84383_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ArgNonLazyBinder
struct  ArgNonLazyBinder_t56DEDF75B9E224623A2E117702BC79D8C9E84383  : public NonLazyBinder_tA9FB0768A2F39559FBDDCAFBF940AAD240B7C43C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGNONLAZYBINDER_T56DEDF75B9E224623A2E117702BC79D8C9E84383_H
#ifndef COPYNONLAZYBINDER_TC55396C09D2CF16C40164CC4E24D58AC45632D11_H
#define COPYNONLAZYBINDER_TC55396C09D2CF16C40164CC4E24D58AC45632D11_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.CopyNonLazyBinder
struct  CopyNonLazyBinder_tC55396C09D2CF16C40164CC4E24D58AC45632D11  : public NonLazyBinder_tA9FB0768A2F39559FBDDCAFBF940AAD240B7C43C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COPYNONLAZYBINDER_TC55396C09D2CF16C40164CC4E24D58AC45632D11_H
#ifndef PREFABBINDINGFINALIZER_T355616E5B859E3E8B597F710035FAE139DF073FE_H
#define PREFABBINDINGFINALIZER_T355616E5B859E3E8B597F710035FAE139DF073FE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.PrefabBindingFinalizer
struct  PrefabBindingFinalizer_t355616E5B859E3E8B597F710035FAE139DF073FE  : public ProviderBindingFinalizer_t3C2E6FBD89B98A057674B926F42EF13E4ED1EAAE
{
public:
	// Zenject.GameObjectCreationParameters Zenject.PrefabBindingFinalizer::_gameObjectBindInfo
	GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A * ____gameObjectBindInfo_1;
	// UnityEngine.Object Zenject.PrefabBindingFinalizer::_prefab
	Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ____prefab_2;
	// System.Func`3<System.Type,Zenject.IPrefabInstantiator,Zenject.IProvider> Zenject.PrefabBindingFinalizer::_providerFactory
	Func_3_t11D22BBD9531DD4E1FDE18B3937E1DC07ED8DB79 * ____providerFactory_3;

public:
	inline static int32_t get_offset_of__gameObjectBindInfo_1() { return static_cast<int32_t>(offsetof(PrefabBindingFinalizer_t355616E5B859E3E8B597F710035FAE139DF073FE, ____gameObjectBindInfo_1)); }
	inline GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A * get__gameObjectBindInfo_1() const { return ____gameObjectBindInfo_1; }
	inline GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A ** get_address_of__gameObjectBindInfo_1() { return &____gameObjectBindInfo_1; }
	inline void set__gameObjectBindInfo_1(GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A * value)
	{
		____gameObjectBindInfo_1 = value;
		Il2CppCodeGenWriteBarrier((&____gameObjectBindInfo_1), value);
	}

	inline static int32_t get_offset_of__prefab_2() { return static_cast<int32_t>(offsetof(PrefabBindingFinalizer_t355616E5B859E3E8B597F710035FAE139DF073FE, ____prefab_2)); }
	inline Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * get__prefab_2() const { return ____prefab_2; }
	inline Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 ** get_address_of__prefab_2() { return &____prefab_2; }
	inline void set__prefab_2(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * value)
	{
		____prefab_2 = value;
		Il2CppCodeGenWriteBarrier((&____prefab_2), value);
	}

	inline static int32_t get_offset_of__providerFactory_3() { return static_cast<int32_t>(offsetof(PrefabBindingFinalizer_t355616E5B859E3E8B597F710035FAE139DF073FE, ____providerFactory_3)); }
	inline Func_3_t11D22BBD9531DD4E1FDE18B3937E1DC07ED8DB79 * get__providerFactory_3() const { return ____providerFactory_3; }
	inline Func_3_t11D22BBD9531DD4E1FDE18B3937E1DC07ED8DB79 ** get_address_of__providerFactory_3() { return &____providerFactory_3; }
	inline void set__providerFactory_3(Func_3_t11D22BBD9531DD4E1FDE18B3937E1DC07ED8DB79 * value)
	{
		____providerFactory_3 = value;
		Il2CppCodeGenWriteBarrier((&____providerFactory_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREFABBINDINGFINALIZER_T355616E5B859E3E8B597F710035FAE139DF073FE_H
#ifndef PREFABRESOURCEBINDINGFINALIZER_T8527DD2D663940B5BE41029F8B5C51128D446CC4_H
#define PREFABRESOURCEBINDINGFINALIZER_T8527DD2D663940B5BE41029F8B5C51128D446CC4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.PrefabResourceBindingFinalizer
struct  PrefabResourceBindingFinalizer_t8527DD2D663940B5BE41029F8B5C51128D446CC4  : public ProviderBindingFinalizer_t3C2E6FBD89B98A057674B926F42EF13E4ED1EAAE
{
public:
	// Zenject.GameObjectCreationParameters Zenject.PrefabResourceBindingFinalizer::_gameObjectBindInfo
	GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A * ____gameObjectBindInfo_1;
	// System.String Zenject.PrefabResourceBindingFinalizer::_resourcePath
	String_t* ____resourcePath_2;
	// System.Func`3<System.Type,Zenject.IPrefabInstantiator,Zenject.IProvider> Zenject.PrefabResourceBindingFinalizer::_providerFactory
	Func_3_t11D22BBD9531DD4E1FDE18B3937E1DC07ED8DB79 * ____providerFactory_3;

public:
	inline static int32_t get_offset_of__gameObjectBindInfo_1() { return static_cast<int32_t>(offsetof(PrefabResourceBindingFinalizer_t8527DD2D663940B5BE41029F8B5C51128D446CC4, ____gameObjectBindInfo_1)); }
	inline GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A * get__gameObjectBindInfo_1() const { return ____gameObjectBindInfo_1; }
	inline GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A ** get_address_of__gameObjectBindInfo_1() { return &____gameObjectBindInfo_1; }
	inline void set__gameObjectBindInfo_1(GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A * value)
	{
		____gameObjectBindInfo_1 = value;
		Il2CppCodeGenWriteBarrier((&____gameObjectBindInfo_1), value);
	}

	inline static int32_t get_offset_of__resourcePath_2() { return static_cast<int32_t>(offsetof(PrefabResourceBindingFinalizer_t8527DD2D663940B5BE41029F8B5C51128D446CC4, ____resourcePath_2)); }
	inline String_t* get__resourcePath_2() const { return ____resourcePath_2; }
	inline String_t** get_address_of__resourcePath_2() { return &____resourcePath_2; }
	inline void set__resourcePath_2(String_t* value)
	{
		____resourcePath_2 = value;
		Il2CppCodeGenWriteBarrier((&____resourcePath_2), value);
	}

	inline static int32_t get_offset_of__providerFactory_3() { return static_cast<int32_t>(offsetof(PrefabResourceBindingFinalizer_t8527DD2D663940B5BE41029F8B5C51128D446CC4, ____providerFactory_3)); }
	inline Func_3_t11D22BBD9531DD4E1FDE18B3937E1DC07ED8DB79 * get__providerFactory_3() const { return ____providerFactory_3; }
	inline Func_3_t11D22BBD9531DD4E1FDE18B3937E1DC07ED8DB79 ** get_address_of__providerFactory_3() { return &____providerFactory_3; }
	inline void set__providerFactory_3(Func_3_t11D22BBD9531DD4E1FDE18B3937E1DC07ED8DB79 * value)
	{
		____providerFactory_3 = value;
		Il2CppCodeGenWriteBarrier((&____providerFactory_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREFABRESOURCEBINDINGFINALIZER_T8527DD2D663940B5BE41029F8B5C51128D446CC4_H
#ifndef SCOPENONLAZYBINDER_T7BC7AB773CA2EAECC76C3B733A1271EE6866646A_H
#define SCOPENONLAZYBINDER_T7BC7AB773CA2EAECC76C3B733A1271EE6866646A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ScopeNonLazyBinder
struct  ScopeNonLazyBinder_t7BC7AB773CA2EAECC76C3B733A1271EE6866646A  : public NonLazyBinder_tA9FB0768A2F39559FBDDCAFBF940AAD240B7C43C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCOPENONLAZYBINDER_T7BC7AB773CA2EAECC76C3B733A1271EE6866646A_H
#ifndef NULLABLE_1_T1AF22E72609C109A63AC7BE47F596B5956149D41_H
#define NULLABLE_1_T1AF22E72609C109A63AC7BE47F596B5956149D41_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<UnityEngine.Quaternion>
struct  Nullable_1_t1AF22E72609C109A63AC7BE47F596B5956149D41 
{
public:
	// T System.Nullable`1::value
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t1AF22E72609C109A63AC7BE47F596B5956149D41, ___value_0)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_value_0() const { return ___value_0; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t1AF22E72609C109A63AC7BE47F596B5956149D41, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T1AF22E72609C109A63AC7BE47F596B5956149D41_H
#ifndef NULLABLE_1_T75BFB9848B0C0C6930A6973335B59621D36E6203_H
#define NULLABLE_1_T75BFB9848B0C0C6930A6973335B59621D36E6203_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<UnityEngine.Vector3>
struct  Nullable_1_t75BFB9848B0C0C6930A6973335B59621D36E6203 
{
public:
	// T System.Nullable`1::value
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t75BFB9848B0C0C6930A6973335B59621D36E6203, ___value_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_value_0() const { return ___value_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t75BFB9848B0C0C6930A6973335B59621D36E6203, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T75BFB9848B0C0C6930A6973335B59621D36E6203_H
#ifndef CONDITIONCOPYNONLAZYBINDER_TF7F37C5F1508AAB283151819E412C853A8D94B9B_H
#define CONDITIONCOPYNONLAZYBINDER_TF7F37C5F1508AAB283151819E412C853A8D94B9B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ConditionCopyNonLazyBinder
struct  ConditionCopyNonLazyBinder_tF7F37C5F1508AAB283151819E412C853A8D94B9B  : public CopyNonLazyBinder_tC55396C09D2CF16C40164CC4E24D58AC45632D11
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONDITIONCOPYNONLAZYBINDER_TF7F37C5F1508AAB283151819E412C853A8D94B9B_H
#ifndef INVALIDBINDRESPONSES_TF86F894C536EBB639E3D76953623A03398F924E5_H
#define INVALIDBINDRESPONSES_TF86F894C536EBB639E3D76953623A03398F924E5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.InvalidBindResponses
struct  InvalidBindResponses_tF86F894C536EBB639E3D76953623A03398F924E5 
{
public:
	// System.Int32 Zenject.InvalidBindResponses::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InvalidBindResponses_tF86F894C536EBB639E3D76953623A03398F924E5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVALIDBINDRESPONSES_TF86F894C536EBB639E3D76953623A03398F924E5_H
#ifndef POOLEXPANDMETHODS_T880D0B909A07FAA5A93BBE2918D20E825322B07E_H
#define POOLEXPANDMETHODS_T880D0B909A07FAA5A93BBE2918D20E825322B07E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.PoolExpandMethods
struct  PoolExpandMethods_t880D0B909A07FAA5A93BBE2918D20E825322B07E 
{
public:
	// System.Int32 Zenject.PoolExpandMethods::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PoolExpandMethods_t880D0B909A07FAA5A93BBE2918D20E825322B07E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POOLEXPANDMETHODS_T880D0B909A07FAA5A93BBE2918D20E825322B07E_H
#ifndef SCOPEARGNONLAZYBINDER_T1D85A2EED80F3DAE72346683D48CCCEAB4738BA6_H
#define SCOPEARGNONLAZYBINDER_T1D85A2EED80F3DAE72346683D48CCCEAB4738BA6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ScopeArgNonLazyBinder
struct  ScopeArgNonLazyBinder_t1D85A2EED80F3DAE72346683D48CCCEAB4738BA6  : public ArgNonLazyBinder_t56DEDF75B9E224623A2E117702BC79D8C9E84383
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCOPEARGNONLAZYBINDER_T1D85A2EED80F3DAE72346683D48CCCEAB4738BA6_H
#ifndef SCOPETYPES_T315A9DCBF5346C5A17A873669FFE3A13751201A8_H
#define SCOPETYPES_T315A9DCBF5346C5A17A873669FFE3A13751201A8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ScopeTypes
struct  ScopeTypes_t315A9DCBF5346C5A17A873669FFE3A13751201A8 
{
public:
	// System.Int32 Zenject.ScopeTypes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ScopeTypes_t315A9DCBF5346C5A17A873669FFE3A13751201A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCOPETYPES_T315A9DCBF5346C5A17A873669FFE3A13751201A8_H
#ifndef TOCHOICES_T6D37BE8E3C1FCAFC151B4DF68384D5D67A526ADF_H
#define TOCHOICES_T6D37BE8E3C1FCAFC151B4DF68384D5D67A526ADF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ToChoices
struct  ToChoices_t6D37BE8E3C1FCAFC151B4DF68384D5D67A526ADF 
{
public:
	// System.Int32 Zenject.ToChoices::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ToChoices_t6D37BE8E3C1FCAFC151B4DF68384D5D67A526ADF, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOCHOICES_T6D37BE8E3C1FCAFC151B4DF68384D5D67A526ADF_H
#ifndef ARGCONDITIONCOPYNONLAZYBINDER_T5F1B4361E0805E20DDBEDEBC61B90CFFBB68B21B_H
#define ARGCONDITIONCOPYNONLAZYBINDER_T5F1B4361E0805E20DDBEDEBC61B90CFFBB68B21B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ArgConditionCopyNonLazyBinder
struct  ArgConditionCopyNonLazyBinder_t5F1B4361E0805E20DDBEDEBC61B90CFFBB68B21B  : public ConditionCopyNonLazyBinder_tF7F37C5F1508AAB283151819E412C853A8D94B9B
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGCONDITIONCOPYNONLAZYBINDER_T5F1B4361E0805E20DDBEDEBC61B90CFFBB68B21B_H
#ifndef BINDINFO_T56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991_H
#define BINDINFO_T56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.BindInfo
struct  BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991  : public RuntimeObject
{
public:
	// System.String Zenject.BindInfo::<ContextInfo>k__BackingField
	String_t* ___U3CContextInfoU3Ek__BackingField_0;
	// System.Boolean Zenject.BindInfo::<RequireExplicitScope>k__BackingField
	bool ___U3CRequireExplicitScopeU3Ek__BackingField_1;
	// System.Object Zenject.BindInfo::<Identifier>k__BackingField
	RuntimeObject * ___U3CIdentifierU3Ek__BackingField_2;
	// System.Collections.Generic.List`1<System.Type> Zenject.BindInfo::<ContractTypes>k__BackingField
	List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0 * ___U3CContractTypesU3Ek__BackingField_3;
	// System.Boolean Zenject.BindInfo::<CopyIntoAllSubContainers>k__BackingField
	bool ___U3CCopyIntoAllSubContainersU3Ek__BackingField_4;
	// Zenject.InvalidBindResponses Zenject.BindInfo::<InvalidBindResponse>k__BackingField
	int32_t ___U3CInvalidBindResponseU3Ek__BackingField_5;
	// System.Boolean Zenject.BindInfo::<NonLazy>k__BackingField
	bool ___U3CNonLazyU3Ek__BackingField_6;
	// Zenject.BindingCondition Zenject.BindInfo::<Condition>k__BackingField
	BindingCondition_t3BC89ABD74DA160C3396036CE0360DE414B4ADD8 * ___U3CConditionU3Ek__BackingField_7;
	// Zenject.ToChoices Zenject.BindInfo::<ToChoice>k__BackingField
	int32_t ___U3CToChoiceU3Ek__BackingField_8;
	// System.Collections.Generic.List`1<System.Type> Zenject.BindInfo::<ToTypes>k__BackingField
	List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0 * ___U3CToTypesU3Ek__BackingField_9;
	// Zenject.ScopeTypes Zenject.BindInfo::<Scope>k__BackingField
	int32_t ___U3CScopeU3Ek__BackingField_10;
	// System.Object Zenject.BindInfo::<ConcreteIdentifier>k__BackingField
	RuntimeObject * ___U3CConcreteIdentifierU3Ek__BackingField_11;
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.BindInfo::<Arguments>k__BackingField
	List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * ___U3CArgumentsU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_U3CContextInfoU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991, ___U3CContextInfoU3Ek__BackingField_0)); }
	inline String_t* get_U3CContextInfoU3Ek__BackingField_0() const { return ___U3CContextInfoU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CContextInfoU3Ek__BackingField_0() { return &___U3CContextInfoU3Ek__BackingField_0; }
	inline void set_U3CContextInfoU3Ek__BackingField_0(String_t* value)
	{
		___U3CContextInfoU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CContextInfoU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CRequireExplicitScopeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991, ___U3CRequireExplicitScopeU3Ek__BackingField_1)); }
	inline bool get_U3CRequireExplicitScopeU3Ek__BackingField_1() const { return ___U3CRequireExplicitScopeU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CRequireExplicitScopeU3Ek__BackingField_1() { return &___U3CRequireExplicitScopeU3Ek__BackingField_1; }
	inline void set_U3CRequireExplicitScopeU3Ek__BackingField_1(bool value)
	{
		___U3CRequireExplicitScopeU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CIdentifierU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991, ___U3CIdentifierU3Ek__BackingField_2)); }
	inline RuntimeObject * get_U3CIdentifierU3Ek__BackingField_2() const { return ___U3CIdentifierU3Ek__BackingField_2; }
	inline RuntimeObject ** get_address_of_U3CIdentifierU3Ek__BackingField_2() { return &___U3CIdentifierU3Ek__BackingField_2; }
	inline void set_U3CIdentifierU3Ek__BackingField_2(RuntimeObject * value)
	{
		___U3CIdentifierU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CIdentifierU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CContractTypesU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991, ___U3CContractTypesU3Ek__BackingField_3)); }
	inline List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0 * get_U3CContractTypesU3Ek__BackingField_3() const { return ___U3CContractTypesU3Ek__BackingField_3; }
	inline List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0 ** get_address_of_U3CContractTypesU3Ek__BackingField_3() { return &___U3CContractTypesU3Ek__BackingField_3; }
	inline void set_U3CContractTypesU3Ek__BackingField_3(List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0 * value)
	{
		___U3CContractTypesU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CContractTypesU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CCopyIntoAllSubContainersU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991, ___U3CCopyIntoAllSubContainersU3Ek__BackingField_4)); }
	inline bool get_U3CCopyIntoAllSubContainersU3Ek__BackingField_4() const { return ___U3CCopyIntoAllSubContainersU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CCopyIntoAllSubContainersU3Ek__BackingField_4() { return &___U3CCopyIntoAllSubContainersU3Ek__BackingField_4; }
	inline void set_U3CCopyIntoAllSubContainersU3Ek__BackingField_4(bool value)
	{
		___U3CCopyIntoAllSubContainersU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CInvalidBindResponseU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991, ___U3CInvalidBindResponseU3Ek__BackingField_5)); }
	inline int32_t get_U3CInvalidBindResponseU3Ek__BackingField_5() const { return ___U3CInvalidBindResponseU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CInvalidBindResponseU3Ek__BackingField_5() { return &___U3CInvalidBindResponseU3Ek__BackingField_5; }
	inline void set_U3CInvalidBindResponseU3Ek__BackingField_5(int32_t value)
	{
		___U3CInvalidBindResponseU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CNonLazyU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991, ___U3CNonLazyU3Ek__BackingField_6)); }
	inline bool get_U3CNonLazyU3Ek__BackingField_6() const { return ___U3CNonLazyU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CNonLazyU3Ek__BackingField_6() { return &___U3CNonLazyU3Ek__BackingField_6; }
	inline void set_U3CNonLazyU3Ek__BackingField_6(bool value)
	{
		___U3CNonLazyU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CConditionU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991, ___U3CConditionU3Ek__BackingField_7)); }
	inline BindingCondition_t3BC89ABD74DA160C3396036CE0360DE414B4ADD8 * get_U3CConditionU3Ek__BackingField_7() const { return ___U3CConditionU3Ek__BackingField_7; }
	inline BindingCondition_t3BC89ABD74DA160C3396036CE0360DE414B4ADD8 ** get_address_of_U3CConditionU3Ek__BackingField_7() { return &___U3CConditionU3Ek__BackingField_7; }
	inline void set_U3CConditionU3Ek__BackingField_7(BindingCondition_t3BC89ABD74DA160C3396036CE0360DE414B4ADD8 * value)
	{
		___U3CConditionU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CConditionU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CToChoiceU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991, ___U3CToChoiceU3Ek__BackingField_8)); }
	inline int32_t get_U3CToChoiceU3Ek__BackingField_8() const { return ___U3CToChoiceU3Ek__BackingField_8; }
	inline int32_t* get_address_of_U3CToChoiceU3Ek__BackingField_8() { return &___U3CToChoiceU3Ek__BackingField_8; }
	inline void set_U3CToChoiceU3Ek__BackingField_8(int32_t value)
	{
		___U3CToChoiceU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CToTypesU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991, ___U3CToTypesU3Ek__BackingField_9)); }
	inline List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0 * get_U3CToTypesU3Ek__BackingField_9() const { return ___U3CToTypesU3Ek__BackingField_9; }
	inline List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0 ** get_address_of_U3CToTypesU3Ek__BackingField_9() { return &___U3CToTypesU3Ek__BackingField_9; }
	inline void set_U3CToTypesU3Ek__BackingField_9(List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0 * value)
	{
		___U3CToTypesU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CToTypesU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3CScopeU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991, ___U3CScopeU3Ek__BackingField_10)); }
	inline int32_t get_U3CScopeU3Ek__BackingField_10() const { return ___U3CScopeU3Ek__BackingField_10; }
	inline int32_t* get_address_of_U3CScopeU3Ek__BackingField_10() { return &___U3CScopeU3Ek__BackingField_10; }
	inline void set_U3CScopeU3Ek__BackingField_10(int32_t value)
	{
		___U3CScopeU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CConcreteIdentifierU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991, ___U3CConcreteIdentifierU3Ek__BackingField_11)); }
	inline RuntimeObject * get_U3CConcreteIdentifierU3Ek__BackingField_11() const { return ___U3CConcreteIdentifierU3Ek__BackingField_11; }
	inline RuntimeObject ** get_address_of_U3CConcreteIdentifierU3Ek__BackingField_11() { return &___U3CConcreteIdentifierU3Ek__BackingField_11; }
	inline void set_U3CConcreteIdentifierU3Ek__BackingField_11(RuntimeObject * value)
	{
		___U3CConcreteIdentifierU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CConcreteIdentifierU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CArgumentsU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991, ___U3CArgumentsU3Ek__BackingField_12)); }
	inline List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * get_U3CArgumentsU3Ek__BackingField_12() const { return ___U3CArgumentsU3Ek__BackingField_12; }
	inline List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 ** get_address_of_U3CArgumentsU3Ek__BackingField_12() { return &___U3CArgumentsU3Ek__BackingField_12; }
	inline void set_U3CArgumentsU3Ek__BackingField_12(List_1_tCEA704C275810C578B5AC9C854F275F3CAF7BA69 * value)
	{
		___U3CArgumentsU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CArgumentsU3Ek__BackingField_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINFO_T56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991_H
#ifndef GAMEOBJECTCREATIONPARAMETERS_TA152144ABF0AD90D0409296C1BFEDAC3958B1B9A_H
#define GAMEOBJECTCREATIONPARAMETERS_TA152144ABF0AD90D0409296C1BFEDAC3958B1B9A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.GameObjectCreationParameters
struct  GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A  : public RuntimeObject
{
public:
	// System.String Zenject.GameObjectCreationParameters::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
	// System.String Zenject.GameObjectCreationParameters::<GroupName>k__BackingField
	String_t* ___U3CGroupNameU3Ek__BackingField_1;
	// UnityEngine.Transform Zenject.GameObjectCreationParameters::<ParentTransform>k__BackingField
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___U3CParentTransformU3Ek__BackingField_2;
	// System.Func`2<Zenject.InjectContext,UnityEngine.Transform> Zenject.GameObjectCreationParameters::<ParentTransformGetter>k__BackingField
	Func_2_tD7382E4A0354665A42C6D76850F29E2926A3063C * ___U3CParentTransformGetterU3Ek__BackingField_3;
	// System.Nullable`1<UnityEngine.Vector3> Zenject.GameObjectCreationParameters::<Position>k__BackingField
	Nullable_1_t75BFB9848B0C0C6930A6973335B59621D36E6203  ___U3CPositionU3Ek__BackingField_4;
	// System.Nullable`1<UnityEngine.Quaternion> Zenject.GameObjectCreationParameters::<Rotation>k__BackingField
	Nullable_1_t1AF22E72609C109A63AC7BE47F596B5956149D41  ___U3CRotationU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A, ___U3CNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CNameU3Ek__BackingField_0() const { return ___U3CNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_0() { return &___U3CNameU3Ek__BackingField_0; }
	inline void set_U3CNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CGroupNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A, ___U3CGroupNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CGroupNameU3Ek__BackingField_1() const { return ___U3CGroupNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CGroupNameU3Ek__BackingField_1() { return &___U3CGroupNameU3Ek__BackingField_1; }
	inline void set_U3CGroupNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CGroupNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGroupNameU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CParentTransformU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A, ___U3CParentTransformU3Ek__BackingField_2)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_U3CParentTransformU3Ek__BackingField_2() const { return ___U3CParentTransformU3Ek__BackingField_2; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_U3CParentTransformU3Ek__BackingField_2() { return &___U3CParentTransformU3Ek__BackingField_2; }
	inline void set_U3CParentTransformU3Ek__BackingField_2(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___U3CParentTransformU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CParentTransformU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CParentTransformGetterU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A, ___U3CParentTransformGetterU3Ek__BackingField_3)); }
	inline Func_2_tD7382E4A0354665A42C6D76850F29E2926A3063C * get_U3CParentTransformGetterU3Ek__BackingField_3() const { return ___U3CParentTransformGetterU3Ek__BackingField_3; }
	inline Func_2_tD7382E4A0354665A42C6D76850F29E2926A3063C ** get_address_of_U3CParentTransformGetterU3Ek__BackingField_3() { return &___U3CParentTransformGetterU3Ek__BackingField_3; }
	inline void set_U3CParentTransformGetterU3Ek__BackingField_3(Func_2_tD7382E4A0354665A42C6D76850F29E2926A3063C * value)
	{
		___U3CParentTransformGetterU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CParentTransformGetterU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CPositionU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A, ___U3CPositionU3Ek__BackingField_4)); }
	inline Nullable_1_t75BFB9848B0C0C6930A6973335B59621D36E6203  get_U3CPositionU3Ek__BackingField_4() const { return ___U3CPositionU3Ek__BackingField_4; }
	inline Nullable_1_t75BFB9848B0C0C6930A6973335B59621D36E6203 * get_address_of_U3CPositionU3Ek__BackingField_4() { return &___U3CPositionU3Ek__BackingField_4; }
	inline void set_U3CPositionU3Ek__BackingField_4(Nullable_1_t75BFB9848B0C0C6930A6973335B59621D36E6203  value)
	{
		___U3CPositionU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CRotationU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A, ___U3CRotationU3Ek__BackingField_5)); }
	inline Nullable_1_t1AF22E72609C109A63AC7BE47F596B5956149D41  get_U3CRotationU3Ek__BackingField_5() const { return ___U3CRotationU3Ek__BackingField_5; }
	inline Nullable_1_t1AF22E72609C109A63AC7BE47F596B5956149D41 * get_address_of_U3CRotationU3Ek__BackingField_5() { return &___U3CRotationU3Ek__BackingField_5; }
	inline void set_U3CRotationU3Ek__BackingField_5(Nullable_1_t1AF22E72609C109A63AC7BE47F596B5956149D41  value)
	{
		___U3CRotationU3Ek__BackingField_5 = value;
	}
};

struct GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A_StaticFields
{
public:
	// Zenject.GameObjectCreationParameters Zenject.GameObjectCreationParameters::Default
	GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A * ___Default_6;

public:
	inline static int32_t get_offset_of_Default_6() { return static_cast<int32_t>(offsetof(GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A_StaticFields, ___Default_6)); }
	inline GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A * get_Default_6() const { return ___Default_6; }
	inline GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A ** get_address_of_Default_6() { return &___Default_6; }
	inline void set_Default_6(GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A * value)
	{
		___Default_6 = value;
		Il2CppCodeGenWriteBarrier((&___Default_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECTCREATIONPARAMETERS_TA152144ABF0AD90D0409296C1BFEDAC3958B1B9A_H
#ifndef MEMORYPOOLBINDINFO_TB7A75979568ABAD9B4C0F599B76DF4BDBD3A5D01_H
#define MEMORYPOOLBINDINFO_TB7A75979568ABAD9B4C0F599B76DF4BDBD3A5D01_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.MemoryPoolBindInfo
struct  MemoryPoolBindInfo_tB7A75979568ABAD9B4C0F599B76DF4BDBD3A5D01  : public RuntimeObject
{
public:
	// Zenject.PoolExpandMethods Zenject.MemoryPoolBindInfo::<ExpandMethod>k__BackingField
	int32_t ___U3CExpandMethodU3Ek__BackingField_0;
	// System.Int32 Zenject.MemoryPoolBindInfo::<InitialSize>k__BackingField
	int32_t ___U3CInitialSizeU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CExpandMethodU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MemoryPoolBindInfo_tB7A75979568ABAD9B4C0F599B76DF4BDBD3A5D01, ___U3CExpandMethodU3Ek__BackingField_0)); }
	inline int32_t get_U3CExpandMethodU3Ek__BackingField_0() const { return ___U3CExpandMethodU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CExpandMethodU3Ek__BackingField_0() { return &___U3CExpandMethodU3Ek__BackingField_0; }
	inline void set_U3CExpandMethodU3Ek__BackingField_0(int32_t value)
	{
		___U3CExpandMethodU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CInitialSizeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(MemoryPoolBindInfo_tB7A75979568ABAD9B4C0F599B76DF4BDBD3A5D01, ___U3CInitialSizeU3Ek__BackingField_1)); }
	inline int32_t get_U3CInitialSizeU3Ek__BackingField_1() const { return ___U3CInitialSizeU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CInitialSizeU3Ek__BackingField_1() { return &___U3CInitialSizeU3Ek__BackingField_1; }
	inline void set_U3CInitialSizeU3Ek__BackingField_1(int32_t value)
	{
		___U3CInitialSizeU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMORYPOOLBINDINFO_TB7A75979568ABAD9B4C0F599B76DF4BDBD3A5D01_H
#ifndef SCOPECONDITIONCOPYNONLAZYBINDER_T6221939C8173223799835D9DF0944D6D777C3EC6_H
#define SCOPECONDITIONCOPYNONLAZYBINDER_T6221939C8173223799835D9DF0944D6D777C3EC6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ScopeConditionCopyNonLazyBinder
struct  ScopeConditionCopyNonLazyBinder_t6221939C8173223799835D9DF0944D6D777C3EC6  : public ConditionCopyNonLazyBinder_tF7F37C5F1508AAB283151819E412C853A8D94B9B
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCOPECONDITIONCOPYNONLAZYBINDER_T6221939C8173223799835D9DF0944D6D777C3EC6_H
#ifndef TRANSFORMCONDITIONCOPYNONLAZYBINDER_T1D4F796012991D42811487B4BF4D01311600463F_H
#define TRANSFORMCONDITIONCOPYNONLAZYBINDER_T1D4F796012991D42811487B4BF4D01311600463F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.TransformConditionCopyNonLazyBinder
struct  TransformConditionCopyNonLazyBinder_t1D4F796012991D42811487B4BF4D01311600463F  : public ConditionCopyNonLazyBinder_tF7F37C5F1508AAB283151819E412C853A8D94B9B
{
public:
	// Zenject.GameObjectCreationParameters Zenject.TransformConditionCopyNonLazyBinder::<GameObjectInfo>k__BackingField
	GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A * ___U3CGameObjectInfoU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CGameObjectInfoU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(TransformConditionCopyNonLazyBinder_t1D4F796012991D42811487B4BF4D01311600463F, ___U3CGameObjectInfoU3Ek__BackingField_1)); }
	inline GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A * get_U3CGameObjectInfoU3Ek__BackingField_1() const { return ___U3CGameObjectInfoU3Ek__BackingField_1; }
	inline GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A ** get_address_of_U3CGameObjectInfoU3Ek__BackingField_1() { return &___U3CGameObjectInfoU3Ek__BackingField_1; }
	inline void set_U3CGameObjectInfoU3Ek__BackingField_1(GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A * value)
	{
		___U3CGameObjectInfoU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGameObjectInfoU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMCONDITIONCOPYNONLAZYBINDER_T1D4F796012991D42811487B4BF4D01311600463F_H
#ifndef TRANSFORMSCOPEARGNONLAZYBINDER_TC3A7B2A5ABFBEAFD4EEA48F7A69DD43547055BD2_H
#define TRANSFORMSCOPEARGNONLAZYBINDER_TC3A7B2A5ABFBEAFD4EEA48F7A69DD43547055BD2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.TransformScopeArgNonLazyBinder
struct  TransformScopeArgNonLazyBinder_tC3A7B2A5ABFBEAFD4EEA48F7A69DD43547055BD2  : public ScopeArgNonLazyBinder_t1D85A2EED80F3DAE72346683D48CCCEAB4738BA6
{
public:
	// Zenject.GameObjectCreationParameters Zenject.TransformScopeArgNonLazyBinder::<GameObjectInfo>k__BackingField
	GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A * ___U3CGameObjectInfoU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CGameObjectInfoU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(TransformScopeArgNonLazyBinder_tC3A7B2A5ABFBEAFD4EEA48F7A69DD43547055BD2, ___U3CGameObjectInfoU3Ek__BackingField_1)); }
	inline GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A * get_U3CGameObjectInfoU3Ek__BackingField_1() const { return ___U3CGameObjectInfoU3Ek__BackingField_1; }
	inline GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A ** get_address_of_U3CGameObjectInfoU3Ek__BackingField_1() { return &___U3CGameObjectInfoU3Ek__BackingField_1; }
	inline void set_U3CGameObjectInfoU3Ek__BackingField_1(GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A * value)
	{
		___U3CGameObjectInfoU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGameObjectInfoU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMSCOPEARGNONLAZYBINDER_TC3A7B2A5ABFBEAFD4EEA48F7A69DD43547055BD2_H
#ifndef IDSCOPECONDITIONCOPYNONLAZYBINDER_TF4C46379BF6B4595053DBCAB75767E759A18ECEE_H
#define IDSCOPECONDITIONCOPYNONLAZYBINDER_TF4C46379BF6B4595053DBCAB75767E759A18ECEE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.IdScopeConditionCopyNonLazyBinder
struct  IdScopeConditionCopyNonLazyBinder_tF4C46379BF6B4595053DBCAB75767E759A18ECEE  : public ScopeConditionCopyNonLazyBinder_t6221939C8173223799835D9DF0944D6D777C3EC6
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IDSCOPECONDITIONCOPYNONLAZYBINDER_TF4C46379BF6B4595053DBCAB75767E759A18ECEE_H
#ifndef NAMETRANSFORMCONDITIONCOPYNONLAZYBINDER_T2E9664EE069D9C6CA15EFEDD64FAE2B4125AC36D_H
#define NAMETRANSFORMCONDITIONCOPYNONLAZYBINDER_T2E9664EE069D9C6CA15EFEDD64FAE2B4125AC36D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.NameTransformConditionCopyNonLazyBinder
struct  NameTransformConditionCopyNonLazyBinder_t2E9664EE069D9C6CA15EFEDD64FAE2B4125AC36D  : public TransformConditionCopyNonLazyBinder_t1D4F796012991D42811487B4BF4D01311600463F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMETRANSFORMCONDITIONCOPYNONLAZYBINDER_T2E9664EE069D9C6CA15EFEDD64FAE2B4125AC36D_H
#ifndef NAMETRANSFORMSCOPEARGNONLAZYBINDER_T08B87C4199E9FF9BAE889E3FBCB0B98778D427CB_H
#define NAMETRANSFORMSCOPEARGNONLAZYBINDER_T08B87C4199E9FF9BAE889E3FBCB0B98778D427CB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.NameTransformScopeArgNonLazyBinder
struct  NameTransformScopeArgNonLazyBinder_t08B87C4199E9FF9BAE889E3FBCB0B98778D427CB  : public TransformScopeArgNonLazyBinder_tC3A7B2A5ABFBEAFD4EEA48F7A69DD43547055BD2
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMETRANSFORMSCOPEARGNONLAZYBINDER_T08B87C4199E9FF9BAE889E3FBCB0B98778D427CB_H
#ifndef SCOPEARGCONDITIONCOPYNONLAZYBINDER_T218A003451367EEC6A439D63A0F38B964EF07B33_H
#define SCOPEARGCONDITIONCOPYNONLAZYBINDER_T218A003451367EEC6A439D63A0F38B964EF07B33_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ScopeArgConditionCopyNonLazyBinder
struct  ScopeArgConditionCopyNonLazyBinder_t218A003451367EEC6A439D63A0F38B964EF07B33  : public ArgConditionCopyNonLazyBinder_t5F1B4361E0805E20DDBEDEBC61B90CFFBB68B21B
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCOPEARGCONDITIONCOPYNONLAZYBINDER_T218A003451367EEC6A439D63A0F38B964EF07B33_H
#ifndef TRANSFORMSCOPECONDITIONCOPYNONLAZYBINDER_T46CC5B84B7D047747104BEB70C03A1BDB2DF92C0_H
#define TRANSFORMSCOPECONDITIONCOPYNONLAZYBINDER_T46CC5B84B7D047747104BEB70C03A1BDB2DF92C0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.TransformScopeConditionCopyNonLazyBinder
struct  TransformScopeConditionCopyNonLazyBinder_t46CC5B84B7D047747104BEB70C03A1BDB2DF92C0  : public ScopeConditionCopyNonLazyBinder_t6221939C8173223799835D9DF0944D6D777C3EC6
{
public:
	// Zenject.GameObjectCreationParameters Zenject.TransformScopeConditionCopyNonLazyBinder::<GameObjectInfo>k__BackingField
	GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A * ___U3CGameObjectInfoU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CGameObjectInfoU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(TransformScopeConditionCopyNonLazyBinder_t46CC5B84B7D047747104BEB70C03A1BDB2DF92C0, ___U3CGameObjectInfoU3Ek__BackingField_1)); }
	inline GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A * get_U3CGameObjectInfoU3Ek__BackingField_1() const { return ___U3CGameObjectInfoU3Ek__BackingField_1; }
	inline GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A ** get_address_of_U3CGameObjectInfoU3Ek__BackingField_1() { return &___U3CGameObjectInfoU3Ek__BackingField_1; }
	inline void set_U3CGameObjectInfoU3Ek__BackingField_1(GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A * value)
	{
		___U3CGameObjectInfoU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGameObjectInfoU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMSCOPECONDITIONCOPYNONLAZYBINDER_T46CC5B84B7D047747104BEB70C03A1BDB2DF92C0_H
#ifndef FROMBINDER_T69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75_H
#define FROMBINDER_T69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.FromBinder
struct  FromBinder_t69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75  : public ScopeArgConditionCopyNonLazyBinder_t218A003451367EEC6A439D63A0F38B964EF07B33
{
public:
	// Zenject.BindFinalizerWrapper Zenject.FromBinder::<FinalizerWrapper>k__BackingField
	BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 * ___U3CFinalizerWrapperU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CFinalizerWrapperU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(FromBinder_t69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75, ___U3CFinalizerWrapperU3Ek__BackingField_1)); }
	inline BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 * get_U3CFinalizerWrapperU3Ek__BackingField_1() const { return ___U3CFinalizerWrapperU3Ek__BackingField_1; }
	inline BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 ** get_address_of_U3CFinalizerWrapperU3Ek__BackingField_1() { return &___U3CFinalizerWrapperU3Ek__BackingField_1; }
	inline void set_U3CFinalizerWrapperU3Ek__BackingField_1(BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 * value)
	{
		___U3CFinalizerWrapperU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFinalizerWrapperU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FROMBINDER_T69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75_H
#ifndef NAMETRANSFORMSCOPECONDITIONCOPYNONLAZYBINDER_TB09C8C70E6F175CA1D387776A1BEAC48F14BE95D_H
#define NAMETRANSFORMSCOPECONDITIONCOPYNONLAZYBINDER_TB09C8C70E6F175CA1D387776A1BEAC48F14BE95D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.NameTransformScopeConditionCopyNonLazyBinder
struct  NameTransformScopeConditionCopyNonLazyBinder_tB09C8C70E6F175CA1D387776A1BEAC48F14BE95D  : public TransformScopeConditionCopyNonLazyBinder_t46CC5B84B7D047747104BEB70C03A1BDB2DF92C0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMETRANSFORMSCOPECONDITIONCOPYNONLAZYBINDER_TB09C8C70E6F175CA1D387776A1BEAC48F14BE95D_H
#ifndef TRANSFORMSCOPEARGCONDITIONCOPYNONLAZYBINDER_T947C359F342B44C3E2536391CFB5256090B808C3_H
#define TRANSFORMSCOPEARGCONDITIONCOPYNONLAZYBINDER_T947C359F342B44C3E2536391CFB5256090B808C3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.TransformScopeArgConditionCopyNonLazyBinder
struct  TransformScopeArgConditionCopyNonLazyBinder_t947C359F342B44C3E2536391CFB5256090B808C3  : public ScopeArgConditionCopyNonLazyBinder_t218A003451367EEC6A439D63A0F38B964EF07B33
{
public:
	// Zenject.GameObjectCreationParameters Zenject.TransformScopeArgConditionCopyNonLazyBinder::<GameObjectInfo>k__BackingField
	GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A * ___U3CGameObjectInfoU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CGameObjectInfoU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(TransformScopeArgConditionCopyNonLazyBinder_t947C359F342B44C3E2536391CFB5256090B808C3, ___U3CGameObjectInfoU3Ek__BackingField_1)); }
	inline GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A * get_U3CGameObjectInfoU3Ek__BackingField_1() const { return ___U3CGameObjectInfoU3Ek__BackingField_1; }
	inline GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A ** get_address_of_U3CGameObjectInfoU3Ek__BackingField_1() { return &___U3CGameObjectInfoU3Ek__BackingField_1; }
	inline void set_U3CGameObjectInfoU3Ek__BackingField_1(GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A * value)
	{
		___U3CGameObjectInfoU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGameObjectInfoU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMSCOPEARGCONDITIONCOPYNONLAZYBINDER_T947C359F342B44C3E2536391CFB5256090B808C3_H
#ifndef FROMBINDERNONGENERIC_T677B667DE367A055191AC7B64A622B8495C6CF89_H
#define FROMBINDERNONGENERIC_T677B667DE367A055191AC7B64A622B8495C6CF89_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.FromBinderNonGeneric
struct  FromBinderNonGeneric_t677B667DE367A055191AC7B64A622B8495C6CF89  : public FromBinder_t69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FROMBINDERNONGENERIC_T677B667DE367A055191AC7B64A622B8495C6CF89_H
#ifndef NAMETRANSFORMSCOPEARGCONDITIONCOPYNONLAZYBINDER_T6174B2AD75883778AD86E3966D11453FF5AFB7F4_H
#define NAMETRANSFORMSCOPEARGCONDITIONCOPYNONLAZYBINDER_T6174B2AD75883778AD86E3966D11453FF5AFB7F4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.NameTransformScopeArgConditionCopyNonLazyBinder
struct  NameTransformScopeArgConditionCopyNonLazyBinder_t6174B2AD75883778AD86E3966D11453FF5AFB7F4  : public TransformScopeArgConditionCopyNonLazyBinder_t947C359F342B44C3E2536391CFB5256090B808C3
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMETRANSFORMSCOPEARGCONDITIONCOPYNONLAZYBINDER_T6174B2AD75883778AD86E3966D11453FF5AFB7F4_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7100 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7100[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7101 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7101[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7102 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7102[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7103 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7103[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7104 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7104[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7105 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7105[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7106 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7107 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7107[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7108 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7108[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7109 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7109[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7110 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7110[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7111 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7111[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7112 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7112[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7113 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7113[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7114 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7114[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7115 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7115[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7116 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7116[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7117 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7117[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7118 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7118[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7119 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7119[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7120 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7120[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7121 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7122 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7123 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7124 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7125 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7126 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7127 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7128 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7129 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7130 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7131 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7132 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7133 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7133[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7134 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7134[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7135 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7135[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7136 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7137 = { sizeof (FromBinder_t69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7137[1] = 
{
	FromBinder_t69F7334AAF8E6876A7C7B81D3EDF082DBC45BF75::get_offset_of_U3CFinalizerWrapperU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7138 = { sizeof (U3CU3Ec__DisplayClass13_0_t8C9B7A792A631ED0604A908F4C081ADFE35B576F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7138[1] = 
{
	U3CU3Ec__DisplayClass13_0_t8C9B7A792A631ED0604A908F4C081ADFE35B576F::get_offset_of_subIdentifier_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7139 = { sizeof (U3CU3Ec__DisplayClass16_0_tAF427052AE85F370174DDB84CA0B5A8E5438A375), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7139[2] = 
{
	U3CU3Ec__DisplayClass16_0_tAF427052AE85F370174DDB84CA0B5A8E5438A375::get_offset_of_factoryType_0(),
	U3CU3Ec__DisplayClass16_0_tAF427052AE85F370174DDB84CA0B5A8E5438A375::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7140 = { sizeof (U3CU3Ec__DisplayClass17_0_tC648F1B5B583F7E0D8A3844E0ADD99F8BA479666), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7140[2] = 
{
	U3CU3Ec__DisplayClass17_0_tC648F1B5B583F7E0D8A3844E0ADD99F8BA479666::get_offset_of_gameObject_0(),
	U3CU3Ec__DisplayClass17_0_tC648F1B5B583F7E0D8A3844E0ADD99F8BA479666::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7141 = { sizeof (U3CU3Ec__DisplayClass18_0_t3FF94B09CF1F4F7BDB70BE103424A29CD3211973), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7141[2] = 
{
	U3CU3Ec__DisplayClass18_0_t3FF94B09CF1F4F7BDB70BE103424A29CD3211973::get_offset_of_gameObjectGetter_0(),
	U3CU3Ec__DisplayClass18_0_t3FF94B09CF1F4F7BDB70BE103424A29CD3211973::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7142 = { sizeof (U3CU3Ec__DisplayClass21_0_tB5FCF8489A0035459C86E3B94CFFBE6C32BA16B5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7142[2] = 
{
	U3CU3Ec__DisplayClass21_0_tB5FCF8489A0035459C86E3B94CFFBE6C32BA16B5::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass21_0_tB5FCF8489A0035459C86E3B94CFFBE6C32BA16B5::get_offset_of_gameObjectInfo_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7143 = { sizeof (U3CU3Ec_tD42D911B6777DD155EA9826FA9CE2DD0A8D53944), -1, sizeof(U3CU3Ec_tD42D911B6777DD155EA9826FA9CE2DD0A8D53944_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7143[5] = 
{
	U3CU3Ec_tD42D911B6777DD155EA9826FA9CE2DD0A8D53944_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tD42D911B6777DD155EA9826FA9CE2DD0A8D53944_StaticFields::get_offset_of_U3CU3E9__23_0_1(),
	U3CU3Ec_tD42D911B6777DD155EA9826FA9CE2DD0A8D53944_StaticFields::get_offset_of_U3CU3E9__25_0_2(),
	U3CU3Ec_tD42D911B6777DD155EA9826FA9CE2DD0A8D53944_StaticFields::get_offset_of_U3CU3E9__27_0_3(),
	U3CU3Ec_tD42D911B6777DD155EA9826FA9CE2DD0A8D53944_StaticFields::get_offset_of_U3CU3E9__29_0_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7144 = { sizeof (U3CU3Ec__DisplayClass32_0_t42768AFF71357DC9C1410EB7B8EBD5FC01F7D94F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7144[3] = 
{
	U3CU3Ec__DisplayClass32_0_t42768AFF71357DC9C1410EB7B8EBD5FC01F7D94F::get_offset_of_resourcePath_0(),
	U3CU3Ec__DisplayClass32_0_t42768AFF71357DC9C1410EB7B8EBD5FC01F7D94F::get_offset_of_U3CU3E4__this_1(),
	U3CU3Ec__DisplayClass32_0_t42768AFF71357DC9C1410EB7B8EBD5FC01F7D94F::get_offset_of_createNew_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7145 = { sizeof (U3CU3Ec__DisplayClass33_0_t7B859D0DF90A1C3EBAF80FD80916EDC2998CB576), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7145[1] = 
{
	U3CU3Ec__DisplayClass33_0_t7B859D0DF90A1C3EBAF80FD80916EDC2998CB576::get_offset_of_resourcePath_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7146 = { sizeof (U3CU3Ec__DisplayClass34_0_tFC082F8B42385B131EB243C4AC9B1A282BE953CE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7146[1] = 
{
	U3CU3Ec__DisplayClass34_0_tFC082F8B42385B131EB243C4AC9B1A282BE953CE::get_offset_of_method_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7147 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7147[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7148 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7148[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7149 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7149[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7150 = { sizeof (U3CU3Ec__DisplayClass39_0_t8D3E47005D45F205218A4C929EE9C9C94BA68083), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7150[1] = 
{
	U3CU3Ec__DisplayClass39_0_t8D3E47005D45F205218A4C929EE9C9C94BA68083::get_offset_of_instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7151 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7152 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7152[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7153 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7153[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7154 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7154[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7155 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7155[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7156 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7156[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7157 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7157[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7158 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7158[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7159 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7159[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7160 = { sizeof (FromBinderNonGeneric_t677B667DE367A055191AC7B64A622B8495C6CF89), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7161 = { sizeof (NameTransformConditionCopyNonLazyBinder_t2E9664EE069D9C6CA15EFEDD64FAE2B4125AC36D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7162 = { sizeof (NameTransformScopeArgConditionCopyNonLazyBinder_t6174B2AD75883778AD86E3966D11453FF5AFB7F4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7163 = { sizeof (NameTransformScopeArgNonLazyBinder_t08B87C4199E9FF9BAE889E3FBCB0B98778D427CB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7164 = { sizeof (NameTransformScopeConditionCopyNonLazyBinder_tB09C8C70E6F175CA1D387776A1BEAC48F14BE95D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7165 = { sizeof (TransformConditionCopyNonLazyBinder_t1D4F796012991D42811487B4BF4D01311600463F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7165[1] = 
{
	TransformConditionCopyNonLazyBinder_t1D4F796012991D42811487B4BF4D01311600463F::get_offset_of_U3CGameObjectInfoU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7166 = { sizeof (TransformScopeArgConditionCopyNonLazyBinder_t947C359F342B44C3E2536391CFB5256090B808C3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7166[1] = 
{
	TransformScopeArgConditionCopyNonLazyBinder_t947C359F342B44C3E2536391CFB5256090B808C3::get_offset_of_U3CGameObjectInfoU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7167 = { sizeof (TransformScopeArgNonLazyBinder_tC3A7B2A5ABFBEAFD4EEA48F7A69DD43547055BD2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7167[1] = 
{
	TransformScopeArgNonLazyBinder_tC3A7B2A5ABFBEAFD4EEA48F7A69DD43547055BD2::get_offset_of_U3CGameObjectInfoU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7168 = { sizeof (TransformScopeConditionCopyNonLazyBinder_t46CC5B84B7D047747104BEB70C03A1BDB2DF92C0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7168[1] = 
{
	TransformScopeConditionCopyNonLazyBinder_t46CC5B84B7D047747104BEB70C03A1BDB2DF92C0::get_offset_of_U3CGameObjectInfoU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7169 = { sizeof (IdBinder_t775EA698C1098F885F2C2C78D2535B79AD0AF012), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7169[1] = 
{
	IdBinder_t775EA698C1098F885F2C2C78D2535B79AD0AF012::get_offset_of__bindInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7170 = { sizeof (IdScopeConditionCopyNonLazyBinder_tF4C46379BF6B4595053DBCAB75767E759A18ECEE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7171 = { sizeof (NonLazyBinder_tA9FB0768A2F39559FBDDCAFBF940AAD240B7C43C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7171[1] = 
{
	NonLazyBinder_tA9FB0768A2F39559FBDDCAFBF940AAD240B7C43C::get_offset_of_U3CBindInfoU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7172 = { sizeof (ScopeArgConditionCopyNonLazyBinder_t218A003451367EEC6A439D63A0F38B964EF07B33), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7173 = { sizeof (ScopeArgNonLazyBinder_t1D85A2EED80F3DAE72346683D48CCCEAB4738BA6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7174 = { sizeof (ScopeConditionCopyNonLazyBinder_t6221939C8173223799835D9DF0944D6D777C3EC6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7175 = { sizeof (ScopeNonLazyBinder_t7BC7AB773CA2EAECC76C3B733A1271EE6866646A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7176 = { sizeof (SubContainerBinder_t24411FF88DADEE6DFBC16EE722E0F4944E8732D6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7176[3] = 
{
	SubContainerBinder_t24411FF88DADEE6DFBC16EE722E0F4944E8732D6::get_offset_of__bindInfo_0(),
	SubContainerBinder_t24411FF88DADEE6DFBC16EE722E0F4944E8732D6::get_offset_of__finalizerWrapper_1(),
	SubContainerBinder_t24411FF88DADEE6DFBC16EE722E0F4944E8732D6::get_offset_of__subIdentifier_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7177 = { sizeof (ScopeTypes_t315A9DCBF5346C5A17A873669FFE3A13751201A8)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7177[5] = 
{
	ScopeTypes_t315A9DCBF5346C5A17A873669FFE3A13751201A8::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7178 = { sizeof (ToChoices_t6D37BE8E3C1FCAFC151B4DF68384D5D67A526ADF)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7178[3] = 
{
	ToChoices_t6D37BE8E3C1FCAFC151B4DF68384D5D67A526ADF::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7179 = { sizeof (InvalidBindResponses_tF86F894C536EBB639E3D76953623A03398F924E5)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7179[3] = 
{
	InvalidBindResponses_tF86F894C536EBB639E3D76953623A03398F924E5::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7180 = { sizeof (BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7180[13] = 
{
	BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991::get_offset_of_U3CContextInfoU3Ek__BackingField_0(),
	BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991::get_offset_of_U3CRequireExplicitScopeU3Ek__BackingField_1(),
	BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991::get_offset_of_U3CIdentifierU3Ek__BackingField_2(),
	BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991::get_offset_of_U3CContractTypesU3Ek__BackingField_3(),
	BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991::get_offset_of_U3CCopyIntoAllSubContainersU3Ek__BackingField_4(),
	BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991::get_offset_of_U3CInvalidBindResponseU3Ek__BackingField_5(),
	BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991::get_offset_of_U3CNonLazyU3Ek__BackingField_6(),
	BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991::get_offset_of_U3CConditionU3Ek__BackingField_7(),
	BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991::get_offset_of_U3CToChoiceU3Ek__BackingField_8(),
	BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991::get_offset_of_U3CToTypesU3Ek__BackingField_9(),
	BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991::get_offset_of_U3CScopeU3Ek__BackingField_10(),
	BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991::get_offset_of_U3CConcreteIdentifierU3Ek__BackingField_11(),
	BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991::get_offset_of_U3CArgumentsU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7181 = { sizeof (FactoryBindInfo_t13E5B099259F31F1EF31D3550FE94DF78C9F1C00), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7181[2] = 
{
	FactoryBindInfo_t13E5B099259F31F1EF31D3550FE94DF78C9F1C00::get_offset_of_U3CFactoryTypeU3Ek__BackingField_0(),
	FactoryBindInfo_t13E5B099259F31F1EF31D3550FE94DF78C9F1C00::get_offset_of_U3CProviderFuncU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7182 = { sizeof (GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A), -1, sizeof(GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7182[7] = 
{
	GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A::get_offset_of_U3CNameU3Ek__BackingField_0(),
	GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A::get_offset_of_U3CGroupNameU3Ek__BackingField_1(),
	GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A::get_offset_of_U3CParentTransformU3Ek__BackingField_2(),
	GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A::get_offset_of_U3CParentTransformGetterU3Ek__BackingField_3(),
	GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A::get_offset_of_U3CPositionU3Ek__BackingField_4(),
	GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A::get_offset_of_U3CRotationU3Ek__BackingField_5(),
	GameObjectCreationParameters_tA152144ABF0AD90D0409296C1BFEDAC3958B1B9A_StaticFields::get_offset_of_Default_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7183 = { sizeof (PoolExpandMethods_t880D0B909A07FAA5A93BBE2918D20E825322B07E)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7183[4] = 
{
	PoolExpandMethods_t880D0B909A07FAA5A93BBE2918D20E825322B07E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7184 = { sizeof (MemoryPoolBindInfo_tB7A75979568ABAD9B4C0F599B76DF4BDBD3A5D01), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7184[2] = 
{
	MemoryPoolBindInfo_tB7A75979568ABAD9B4C0F599B76DF4BDBD3A5D01::get_offset_of_U3CExpandMethodU3Ek__BackingField_0(),
	MemoryPoolBindInfo_tB7A75979568ABAD9B4C0F599B76DF4BDBD3A5D01::get_offset_of_U3CInitialSizeU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7185 = { sizeof (BindingUtil_t6D57223F2867DCF395084BB1CFE781F8DDCBD096), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7186 = { sizeof (BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7186[1] = 
{
	BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8::get_offset_of__subFinalizer_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7187 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7188 = { sizeof (NullBindingFinalizer_tFEAF0122BA4AF8A5209DE0330F0BFF444F06AD65), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7189 = { sizeof (PrefabBindingFinalizer_t355616E5B859E3E8B597F710035FAE139DF073FE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7189[3] = 
{
	PrefabBindingFinalizer_t355616E5B859E3E8B597F710035FAE139DF073FE::get_offset_of__gameObjectBindInfo_1(),
	PrefabBindingFinalizer_t355616E5B859E3E8B597F710035FAE139DF073FE::get_offset_of__prefab_2(),
	PrefabBindingFinalizer_t355616E5B859E3E8B597F710035FAE139DF073FE::get_offset_of__providerFactory_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7190 = { sizeof (U3CU3Ec__DisplayClass5_0_t25A424B5F0CD0CBB038687263E0AA9B93DA940DA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7190[2] = 
{
	U3CU3Ec__DisplayClass5_0_t25A424B5F0CD0CBB038687263E0AA9B93DA940DA::get_offset_of_container_0(),
	U3CU3Ec__DisplayClass5_0_t25A424B5F0CD0CBB038687263E0AA9B93DA940DA::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7191 = { sizeof (U3CU3Ec__DisplayClass5_1_t57E7420F85E66387484EEC4ADD7C03D081763C17), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7191[2] = 
{
	U3CU3Ec__DisplayClass5_1_t57E7420F85E66387484EEC4ADD7C03D081763C17::get_offset_of_prefabCreator_0(),
	U3CU3Ec__DisplayClass5_1_t57E7420F85E66387484EEC4ADD7C03D081763C17::get_offset_of_CSU24U3CU3E8__locals1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7192 = { sizeof (U3CU3Ec__DisplayClass6_0_tCA46D26FA4A790B5596A5EEB43024EABF63DD91E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7192[2] = 
{
	U3CU3Ec__DisplayClass6_0_tCA46D26FA4A790B5596A5EEB43024EABF63DD91E::get_offset_of_container_0(),
	U3CU3Ec__DisplayClass6_0_tCA46D26FA4A790B5596A5EEB43024EABF63DD91E::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7193 = { sizeof (U3CU3Ec__DisplayClass6_1_t2CBD0732A519FC58DECC5102B741A42EFE09C381), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7193[2] = 
{
	U3CU3Ec__DisplayClass6_1_t2CBD0732A519FC58DECC5102B741A42EFE09C381::get_offset_of_prefabCreator_0(),
	U3CU3Ec__DisplayClass6_1_t2CBD0732A519FC58DECC5102B741A42EFE09C381::get_offset_of_CSU24U3CU3E8__locals1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7194 = { sizeof (PrefabResourceBindingFinalizer_t8527DD2D663940B5BE41029F8B5C51128D446CC4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7194[3] = 
{
	PrefabResourceBindingFinalizer_t8527DD2D663940B5BE41029F8B5C51128D446CC4::get_offset_of__gameObjectBindInfo_1(),
	PrefabResourceBindingFinalizer_t8527DD2D663940B5BE41029F8B5C51128D446CC4::get_offset_of__resourcePath_2(),
	PrefabResourceBindingFinalizer_t8527DD2D663940B5BE41029F8B5C51128D446CC4::get_offset_of__providerFactory_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7195 = { sizeof (U3CU3Ec__DisplayClass5_0_tC4DFEACC4F54B76864B4C241BAB809A1D105B6F3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7195[2] = 
{
	U3CU3Ec__DisplayClass5_0_tC4DFEACC4F54B76864B4C241BAB809A1D105B6F3::get_offset_of_container_0(),
	U3CU3Ec__DisplayClass5_0_tC4DFEACC4F54B76864B4C241BAB809A1D105B6F3::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7196 = { sizeof (U3CU3Ec__DisplayClass5_1_t365ACF1A4C33286F722DC1C73486121465F5C606), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7196[2] = 
{
	U3CU3Ec__DisplayClass5_1_t365ACF1A4C33286F722DC1C73486121465F5C606::get_offset_of_prefabCreator_0(),
	U3CU3Ec__DisplayClass5_1_t365ACF1A4C33286F722DC1C73486121465F5C606::get_offset_of_CSU24U3CU3E8__locals1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7197 = { sizeof (U3CU3Ec__DisplayClass6_0_t8DCC1F5B55699D1CFF1D77EC63B83C993536E074), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7197[2] = 
{
	U3CU3Ec__DisplayClass6_0_t8DCC1F5B55699D1CFF1D77EC63B83C993536E074::get_offset_of_container_0(),
	U3CU3Ec__DisplayClass6_0_t8DCC1F5B55699D1CFF1D77EC63B83C993536E074::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7198 = { sizeof (U3CU3Ec__DisplayClass6_1_t82C961472E9F241D5D1F1481A1F2D4F651A479E0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7198[2] = 
{
	U3CU3Ec__DisplayClass6_1_t82C961472E9F241D5D1F1481A1F2D4F651A479E0::get_offset_of_prefabCreator_0(),
	U3CU3Ec__DisplayClass6_1_t82C961472E9F241D5D1F1481A1F2D4F651A479E0::get_offset_of_CSU24U3CU3E8__locals1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7199 = { sizeof (ProviderBindingFinalizer_t3C2E6FBD89B98A057674B926F42EF13E4ED1EAAE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7199[1] = 
{
	ProviderBindingFinalizer_t3C2E6FBD89B98A057674B926F42EF13E4ED1EAAE::get_offset_of_U3CBindInfoU3Ek__BackingField_0(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
