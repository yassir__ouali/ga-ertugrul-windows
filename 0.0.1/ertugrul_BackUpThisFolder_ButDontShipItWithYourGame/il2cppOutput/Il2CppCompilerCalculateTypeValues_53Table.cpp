﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Org.BouncyCastle.Math.BigInteger
struct BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91;
// Org.BouncyCastle.Math.BigInteger[]
struct BigIntegerU5BU5D_tE1C878446A6F27F09B57A1E5ECEBEF841F575311;
// Org.BouncyCastle.Math.EC.AbstractF2mPoint[]
struct AbstractF2mPointU5BU5D_t75025014FD06ECF73A522CA4427FD210274898E1;
// Org.BouncyCastle.Math.EC.Custom.Sec.SecP256K1Point
struct SecP256K1Point_t73B95080B3D33CB70644A9BFBBA68EA06B821FA7;
// Org.BouncyCastle.Math.EC.Custom.Sec.SecP256R1Point
struct SecP256R1Point_t295A7951697911EDEDBEBBF5A781E9D3071E086C;
// Org.BouncyCastle.Math.EC.Custom.Sec.SecP384R1Point
struct SecP384R1Point_tA00FA4528D94813D8BAE66A3EF5E79F07BD62D29;
// Org.BouncyCastle.Math.EC.Custom.Sec.SecP521R1Point
struct SecP521R1Point_tECEE1E8F65AA393C27ACFC30A38A1A57D092C117;
// Org.BouncyCastle.Math.EC.Custom.Sec.SecT113R1Point
struct SecT113R1Point_t11FE9563DDC91F15F35658317310EDF469EFFC55;
// Org.BouncyCastle.Math.EC.Custom.Sec.SecT113R2Point
struct SecT113R2Point_t815197D019F8BBF408E4B3829239619835C98EF6;
// Org.BouncyCastle.Math.EC.Custom.Sec.SecT131R1Point
struct SecT131R1Point_t650635A631EAF1EE11C6B446D0FB58D417AD74D7;
// Org.BouncyCastle.Math.EC.Custom.Sec.SecT131R2Point
struct SecT131R2Point_tA6875915938E95528D2C942965CA3AB629785851;
// Org.BouncyCastle.Math.EC.Custom.Sec.SecT163K1Point
struct SecT163K1Point_t6A7BAF7AF848A31595D0C59C983E50D6E8C10EDC;
// Org.BouncyCastle.Math.EC.Custom.Sec.SecT163R1Point
struct SecT163R1Point_t337935F97D827A290DA27C9833B9C31B8672E57E;
// Org.BouncyCastle.Math.EC.Custom.Sec.SecT163R2Point
struct SecT163R2Point_t48E9B3A96F8B4DD71022478E2AE63A7015F2F682;
// Org.BouncyCastle.Math.EC.Custom.Sec.SecT193R1Point
struct SecT193R1Point_t5E2007F0FA3823BF8A80DCDB359AA04BC2DD43E4;
// Org.BouncyCastle.Math.EC.Custom.Sec.SecT193R2Point
struct SecT193R2Point_tEF874435F9B32986838CDE770CECE8F6DB0711CB;
// Org.BouncyCastle.Math.EC.Custom.Sec.SecT233K1Point
struct SecT233K1Point_t55AC6FAC7F0B890D2C66429AB21918B15EB034DA;
// Org.BouncyCastle.Math.EC.Custom.Sec.SecT233R1Point
struct SecT233R1Point_tA9DCCEB01CCB8ED6862B7B7605362C4C58B1DC76;
// Org.BouncyCastle.Math.EC.Custom.Sec.SecT239K1Point
struct SecT239K1Point_tA128635B479380D6A48C9690715188870A470EA0;
// Org.BouncyCastle.Math.EC.Custom.Sec.SecT283K1Point
struct SecT283K1Point_tE59E5378A535C6ED38C1A2AFC9CB0DC47A69D8F3;
// Org.BouncyCastle.Math.EC.Custom.Sec.SecT283R1Point
struct SecT283R1Point_tF68A0F98707126343F1D8D05DF7D69AA718C3A8B;
// Org.BouncyCastle.Math.EC.Custom.Sec.SecT409K1Point
struct SecT409K1Point_t8BA8093464B27205D0D5D6431AC79B0963985791;
// Org.BouncyCastle.Math.EC.Custom.Sec.SecT409R1Point
struct SecT409R1Point_tCE020433FAD15C841CD2C5EE9A07908B32062BBB;
// Org.BouncyCastle.Math.EC.Custom.Sec.SecT571FieldElement
struct SecT571FieldElement_t9EF7730630D1449DD2009986E22E25491864423B;
// Org.BouncyCastle.Math.EC.Custom.Sec.SecT571K1Point
struct SecT571K1Point_tC7B3CA5C02D4A361FC41724CA5DBF92648999946;
// Org.BouncyCastle.Math.EC.Custom.Sec.SecT571R1Point
struct SecT571R1Point_tDAED963E59F595895CD9B5F8A3B55079F5DE9FE7;
// Org.BouncyCastle.Math.EC.ECCurve
struct ECCurve_t08AD8E722914B0B03C49C2D27B79B1D4E4770D95;
// Org.BouncyCastle.Math.EC.ECFieldElement
struct ECFieldElement_tE2FF8892A60D0ACBC3FB665E41B0DEAA0308514C;
// Org.BouncyCastle.Math.EC.ECFieldElement[]
struct ECFieldElementU5BU5D_tD63CAB58E0E42F9436F12569071F0663360A7609;
// Org.BouncyCastle.Math.EC.ECPoint
struct ECPoint_tCC0C4447DEFF70FC9D68F5149ECCCC0533CE4399;
// Org.BouncyCastle.Math.EC.ECPointMap
struct ECPointMap_t442441C33554CF30301CC99A8355DC91C429CD57;
// Org.BouncyCastle.Math.EC.ECPoint[]
struct ECPointU5BU5D_t12087A0694B5116AE6DA4708C7F3938BBDC98C5C;
// Org.BouncyCastle.Math.EC.Endo.ECEndomorphism
struct ECEndomorphism_t1C3A93A26892C9712CFA1B9ACAD2ABA7E367425C;
// Org.BouncyCastle.Math.EC.Endo.GlvEndomorphism
struct GlvEndomorphism_t724FABA33A733FA585495E0BEE926F47EE535461;
// Org.BouncyCastle.Math.EC.Endo.GlvTypeBParameters
struct GlvTypeBParameters_t39B069A23C048B4E871EB64BBE2F714DDD864095;
// Org.BouncyCastle.Math.EC.Multiplier.ECMultiplier
struct ECMultiplier_tD7F75847287D50B6E100B15DF8A7C4DC6C40753B;
// Org.BouncyCastle.Math.Field.IFiniteField
struct IFiniteField_t3875D72597A5D0021445EA9C8402D94F7B904229;
// Org.BouncyCastle.Math.Field.IPolynomial
struct IPolynomial_t14EFF78965C088A2491BD32EE7D903B9B293B681;
// Org.BouncyCastle.Security.SecureRandom
struct SecureRandom_t0B6C2C3EDF11BF4FD22E44CE8997F6A0D2F142B0;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.String
struct String_t;
// System.UInt32[]
struct UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB;
// System.UInt64[]
struct UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef SECP256K1FIELD_T24CCD5F14810A50A32E41B44DD92F4AD00D37E45_H
#define SECP256K1FIELD_T24CCD5F14810A50A32E41B44DD92F4AD00D37E45_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecP256K1Field
struct  SecP256K1Field_t24CCD5F14810A50A32E41B44DD92F4AD00D37E45  : public RuntimeObject
{
public:

public:
};

struct SecP256K1Field_t24CCD5F14810A50A32E41B44DD92F4AD00D37E45_StaticFields
{
public:
	// System.UInt32[] Org.BouncyCastle.Math.EC.Custom.Sec.SecP256K1Field::P
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___P_0;
	// System.UInt32[] Org.BouncyCastle.Math.EC.Custom.Sec.SecP256K1Field::PExt
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___PExt_1;
	// System.UInt32[] Org.BouncyCastle.Math.EC.Custom.Sec.SecP256K1Field::PExtInv
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___PExtInv_2;

public:
	inline static int32_t get_offset_of_P_0() { return static_cast<int32_t>(offsetof(SecP256K1Field_t24CCD5F14810A50A32E41B44DD92F4AD00D37E45_StaticFields, ___P_0)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_P_0() const { return ___P_0; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_P_0() { return &___P_0; }
	inline void set_P_0(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___P_0 = value;
		Il2CppCodeGenWriteBarrier((&___P_0), value);
	}

	inline static int32_t get_offset_of_PExt_1() { return static_cast<int32_t>(offsetof(SecP256K1Field_t24CCD5F14810A50A32E41B44DD92F4AD00D37E45_StaticFields, ___PExt_1)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_PExt_1() const { return ___PExt_1; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_PExt_1() { return &___PExt_1; }
	inline void set_PExt_1(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___PExt_1 = value;
		Il2CppCodeGenWriteBarrier((&___PExt_1), value);
	}

	inline static int32_t get_offset_of_PExtInv_2() { return static_cast<int32_t>(offsetof(SecP256K1Field_t24CCD5F14810A50A32E41B44DD92F4AD00D37E45_StaticFields, ___PExtInv_2)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_PExtInv_2() const { return ___PExtInv_2; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_PExtInv_2() { return &___PExtInv_2; }
	inline void set_PExtInv_2(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___PExtInv_2 = value;
		Il2CppCodeGenWriteBarrier((&___PExtInv_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP256K1FIELD_T24CCD5F14810A50A32E41B44DD92F4AD00D37E45_H
#ifndef SECP256R1FIELD_TE496FAC39A67AEB48EF815A739CD7BB6C6BCD815_H
#define SECP256R1FIELD_TE496FAC39A67AEB48EF815A739CD7BB6C6BCD815_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecP256R1Field
struct  SecP256R1Field_tE496FAC39A67AEB48EF815A739CD7BB6C6BCD815  : public RuntimeObject
{
public:

public:
};

struct SecP256R1Field_tE496FAC39A67AEB48EF815A739CD7BB6C6BCD815_StaticFields
{
public:
	// System.UInt32[] Org.BouncyCastle.Math.EC.Custom.Sec.SecP256R1Field::P
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___P_0;
	// System.UInt32[] Org.BouncyCastle.Math.EC.Custom.Sec.SecP256R1Field::PExt
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___PExt_1;

public:
	inline static int32_t get_offset_of_P_0() { return static_cast<int32_t>(offsetof(SecP256R1Field_tE496FAC39A67AEB48EF815A739CD7BB6C6BCD815_StaticFields, ___P_0)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_P_0() const { return ___P_0; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_P_0() { return &___P_0; }
	inline void set_P_0(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___P_0 = value;
		Il2CppCodeGenWriteBarrier((&___P_0), value);
	}

	inline static int32_t get_offset_of_PExt_1() { return static_cast<int32_t>(offsetof(SecP256R1Field_tE496FAC39A67AEB48EF815A739CD7BB6C6BCD815_StaticFields, ___PExt_1)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_PExt_1() const { return ___PExt_1; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_PExt_1() { return &___PExt_1; }
	inline void set_PExt_1(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___PExt_1 = value;
		Il2CppCodeGenWriteBarrier((&___PExt_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP256R1FIELD_TE496FAC39A67AEB48EF815A739CD7BB6C6BCD815_H
#ifndef SECP384R1FIELD_TADDA4C62510A5EA24D0D61FFA87D39080C5DB004_H
#define SECP384R1FIELD_TADDA4C62510A5EA24D0D61FFA87D39080C5DB004_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecP384R1Field
struct  SecP384R1Field_tADDA4C62510A5EA24D0D61FFA87D39080C5DB004  : public RuntimeObject
{
public:

public:
};

struct SecP384R1Field_tADDA4C62510A5EA24D0D61FFA87D39080C5DB004_StaticFields
{
public:
	// System.UInt32[] Org.BouncyCastle.Math.EC.Custom.Sec.SecP384R1Field::P
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___P_0;
	// System.UInt32[] Org.BouncyCastle.Math.EC.Custom.Sec.SecP384R1Field::PExt
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___PExt_1;
	// System.UInt32[] Org.BouncyCastle.Math.EC.Custom.Sec.SecP384R1Field::PExtInv
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___PExtInv_2;

public:
	inline static int32_t get_offset_of_P_0() { return static_cast<int32_t>(offsetof(SecP384R1Field_tADDA4C62510A5EA24D0D61FFA87D39080C5DB004_StaticFields, ___P_0)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_P_0() const { return ___P_0; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_P_0() { return &___P_0; }
	inline void set_P_0(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___P_0 = value;
		Il2CppCodeGenWriteBarrier((&___P_0), value);
	}

	inline static int32_t get_offset_of_PExt_1() { return static_cast<int32_t>(offsetof(SecP384R1Field_tADDA4C62510A5EA24D0D61FFA87D39080C5DB004_StaticFields, ___PExt_1)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_PExt_1() const { return ___PExt_1; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_PExt_1() { return &___PExt_1; }
	inline void set_PExt_1(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___PExt_1 = value;
		Il2CppCodeGenWriteBarrier((&___PExt_1), value);
	}

	inline static int32_t get_offset_of_PExtInv_2() { return static_cast<int32_t>(offsetof(SecP384R1Field_tADDA4C62510A5EA24D0D61FFA87D39080C5DB004_StaticFields, ___PExtInv_2)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_PExtInv_2() const { return ___PExtInv_2; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_PExtInv_2() { return &___PExtInv_2; }
	inline void set_PExtInv_2(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___PExtInv_2 = value;
		Il2CppCodeGenWriteBarrier((&___PExtInv_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP384R1FIELD_TADDA4C62510A5EA24D0D61FFA87D39080C5DB004_H
#ifndef SECP521R1FIELD_TEF9772D9A5C9B93D7A2051F8A3285C9101D6EC5C_H
#define SECP521R1FIELD_TEF9772D9A5C9B93D7A2051F8A3285C9101D6EC5C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecP521R1Field
struct  SecP521R1Field_tEF9772D9A5C9B93D7A2051F8A3285C9101D6EC5C  : public RuntimeObject
{
public:

public:
};

struct SecP521R1Field_tEF9772D9A5C9B93D7A2051F8A3285C9101D6EC5C_StaticFields
{
public:
	// System.UInt32[] Org.BouncyCastle.Math.EC.Custom.Sec.SecP521R1Field::P
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___P_0;

public:
	inline static int32_t get_offset_of_P_0() { return static_cast<int32_t>(offsetof(SecP521R1Field_tEF9772D9A5C9B93D7A2051F8A3285C9101D6EC5C_StaticFields, ___P_0)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_P_0() const { return ___P_0; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_P_0() { return &___P_0; }
	inline void set_P_0(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___P_0 = value;
		Il2CppCodeGenWriteBarrier((&___P_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP521R1FIELD_TEF9772D9A5C9B93D7A2051F8A3285C9101D6EC5C_H
#ifndef SECT113FIELD_T7FE017730BFFC2B84B9FB07235FC6DF0578BDA3C_H
#define SECT113FIELD_T7FE017730BFFC2B84B9FB07235FC6DF0578BDA3C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecT113Field
struct  SecT113Field_t7FE017730BFFC2B84B9FB07235FC6DF0578BDA3C  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT113FIELD_T7FE017730BFFC2B84B9FB07235FC6DF0578BDA3C_H
#ifndef SECT131FIELD_T1F7B3AFCA00D1B479925D7FCA70B07B8F62FFB98_H
#define SECT131FIELD_T1F7B3AFCA00D1B479925D7FCA70B07B8F62FFB98_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecT131Field
struct  SecT131Field_t1F7B3AFCA00D1B479925D7FCA70B07B8F62FFB98  : public RuntimeObject
{
public:

public:
};

struct SecT131Field_t1F7B3AFCA00D1B479925D7FCA70B07B8F62FFB98_StaticFields
{
public:
	// System.UInt64[] Org.BouncyCastle.Math.EC.Custom.Sec.SecT131Field::ROOT_Z
	UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* ___ROOT_Z_0;

public:
	inline static int32_t get_offset_of_ROOT_Z_0() { return static_cast<int32_t>(offsetof(SecT131Field_t1F7B3AFCA00D1B479925D7FCA70B07B8F62FFB98_StaticFields, ___ROOT_Z_0)); }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* get_ROOT_Z_0() const { return ___ROOT_Z_0; }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4** get_address_of_ROOT_Z_0() { return &___ROOT_Z_0; }
	inline void set_ROOT_Z_0(UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* value)
	{
		___ROOT_Z_0 = value;
		Il2CppCodeGenWriteBarrier((&___ROOT_Z_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT131FIELD_T1F7B3AFCA00D1B479925D7FCA70B07B8F62FFB98_H
#ifndef SECT163FIELD_TD9E85435CA2A00D2280642412D46FA11D8E48317_H
#define SECT163FIELD_TD9E85435CA2A00D2280642412D46FA11D8E48317_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecT163Field
struct  SecT163Field_tD9E85435CA2A00D2280642412D46FA11D8E48317  : public RuntimeObject
{
public:

public:
};

struct SecT163Field_tD9E85435CA2A00D2280642412D46FA11D8E48317_StaticFields
{
public:
	// System.UInt64[] Org.BouncyCastle.Math.EC.Custom.Sec.SecT163Field::ROOT_Z
	UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* ___ROOT_Z_0;

public:
	inline static int32_t get_offset_of_ROOT_Z_0() { return static_cast<int32_t>(offsetof(SecT163Field_tD9E85435CA2A00D2280642412D46FA11D8E48317_StaticFields, ___ROOT_Z_0)); }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* get_ROOT_Z_0() const { return ___ROOT_Z_0; }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4** get_address_of_ROOT_Z_0() { return &___ROOT_Z_0; }
	inline void set_ROOT_Z_0(UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* value)
	{
		___ROOT_Z_0 = value;
		Il2CppCodeGenWriteBarrier((&___ROOT_Z_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT163FIELD_TD9E85435CA2A00D2280642412D46FA11D8E48317_H
#ifndef SECT193FIELD_TEBCD38351A881C400B7E15CB1FE77CCB962FCC9F_H
#define SECT193FIELD_TEBCD38351A881C400B7E15CB1FE77CCB962FCC9F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecT193Field
struct  SecT193Field_tEBCD38351A881C400B7E15CB1FE77CCB962FCC9F  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT193FIELD_TEBCD38351A881C400B7E15CB1FE77CCB962FCC9F_H
#ifndef SECT233FIELD_T9B70548D86F30DCB84D1F285047EC911CA6F5D9E_H
#define SECT233FIELD_T9B70548D86F30DCB84D1F285047EC911CA6F5D9E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecT233Field
struct  SecT233Field_t9B70548D86F30DCB84D1F285047EC911CA6F5D9E  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT233FIELD_T9B70548D86F30DCB84D1F285047EC911CA6F5D9E_H
#ifndef SECT239FIELD_T5992BC9617E4EFB8401A0B59955C01369D4DA51B_H
#define SECT239FIELD_T5992BC9617E4EFB8401A0B59955C01369D4DA51B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecT239Field
struct  SecT239Field_t5992BC9617E4EFB8401A0B59955C01369D4DA51B  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT239FIELD_T5992BC9617E4EFB8401A0B59955C01369D4DA51B_H
#ifndef SECT283FIELD_T2B99B4DE13EC25DF2E313ABC11E098E65B5EB5E3_H
#define SECT283FIELD_T2B99B4DE13EC25DF2E313ABC11E098E65B5EB5E3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecT283Field
struct  SecT283Field_t2B99B4DE13EC25DF2E313ABC11E098E65B5EB5E3  : public RuntimeObject
{
public:

public:
};

struct SecT283Field_t2B99B4DE13EC25DF2E313ABC11E098E65B5EB5E3_StaticFields
{
public:
	// System.UInt64[] Org.BouncyCastle.Math.EC.Custom.Sec.SecT283Field::ROOT_Z
	UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* ___ROOT_Z_0;

public:
	inline static int32_t get_offset_of_ROOT_Z_0() { return static_cast<int32_t>(offsetof(SecT283Field_t2B99B4DE13EC25DF2E313ABC11E098E65B5EB5E3_StaticFields, ___ROOT_Z_0)); }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* get_ROOT_Z_0() const { return ___ROOT_Z_0; }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4** get_address_of_ROOT_Z_0() { return &___ROOT_Z_0; }
	inline void set_ROOT_Z_0(UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* value)
	{
		___ROOT_Z_0 = value;
		Il2CppCodeGenWriteBarrier((&___ROOT_Z_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT283FIELD_T2B99B4DE13EC25DF2E313ABC11E098E65B5EB5E3_H
#ifndef SECT409FIELD_TB63C2C0AA8342F9278E96568B4D6B707C0941294_H
#define SECT409FIELD_TB63C2C0AA8342F9278E96568B4D6B707C0941294_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecT409Field
struct  SecT409Field_tB63C2C0AA8342F9278E96568B4D6B707C0941294  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT409FIELD_TB63C2C0AA8342F9278E96568B4D6B707C0941294_H
#ifndef SECT571FIELD_TEB4A9472322DF831416F318C64DAFC854F545396_H
#define SECT571FIELD_TEB4A9472322DF831416F318C64DAFC854F545396_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecT571Field
struct  SecT571Field_tEB4A9472322DF831416F318C64DAFC854F545396  : public RuntimeObject
{
public:

public:
};

struct SecT571Field_tEB4A9472322DF831416F318C64DAFC854F545396_StaticFields
{
public:
	// System.UInt64[] Org.BouncyCastle.Math.EC.Custom.Sec.SecT571Field::ROOT_Z
	UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* ___ROOT_Z_0;

public:
	inline static int32_t get_offset_of_ROOT_Z_0() { return static_cast<int32_t>(offsetof(SecT571Field_tEB4A9472322DF831416F318C64DAFC854F545396_StaticFields, ___ROOT_Z_0)); }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* get_ROOT_Z_0() const { return ___ROOT_Z_0; }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4** get_address_of_ROOT_Z_0() { return &___ROOT_Z_0; }
	inline void set_ROOT_Z_0(UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* value)
	{
		___ROOT_Z_0 = value;
		Il2CppCodeGenWriteBarrier((&___ROOT_Z_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT571FIELD_TEB4A9472322DF831416F318C64DAFC854F545396_H
#ifndef ECCURVE_T08AD8E722914B0B03C49C2D27B79B1D4E4770D95_H
#define ECCURVE_T08AD8E722914B0B03C49C2D27B79B1D4E4770D95_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.ECCurve
struct  ECCurve_t08AD8E722914B0B03C49C2D27B79B1D4E4770D95  : public RuntimeObject
{
public:
	// Org.BouncyCastle.Math.Field.IFiniteField Org.BouncyCastle.Math.EC.ECCurve::m_field
	RuntimeObject* ___m_field_0;
	// Org.BouncyCastle.Math.EC.ECFieldElement Org.BouncyCastle.Math.EC.ECCurve::m_a
	ECFieldElement_tE2FF8892A60D0ACBC3FB665E41B0DEAA0308514C * ___m_a_1;
	// Org.BouncyCastle.Math.EC.ECFieldElement Org.BouncyCastle.Math.EC.ECCurve::m_b
	ECFieldElement_tE2FF8892A60D0ACBC3FB665E41B0DEAA0308514C * ___m_b_2;
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Math.EC.ECCurve::m_order
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___m_order_3;
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Math.EC.ECCurve::m_cofactor
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___m_cofactor_4;
	// System.Int32 Org.BouncyCastle.Math.EC.ECCurve::m_coord
	int32_t ___m_coord_5;
	// Org.BouncyCastle.Math.EC.Endo.ECEndomorphism Org.BouncyCastle.Math.EC.ECCurve::m_endomorphism
	RuntimeObject* ___m_endomorphism_6;
	// Org.BouncyCastle.Math.EC.Multiplier.ECMultiplier Org.BouncyCastle.Math.EC.ECCurve::m_multiplier
	RuntimeObject* ___m_multiplier_7;

public:
	inline static int32_t get_offset_of_m_field_0() { return static_cast<int32_t>(offsetof(ECCurve_t08AD8E722914B0B03C49C2D27B79B1D4E4770D95, ___m_field_0)); }
	inline RuntimeObject* get_m_field_0() const { return ___m_field_0; }
	inline RuntimeObject** get_address_of_m_field_0() { return &___m_field_0; }
	inline void set_m_field_0(RuntimeObject* value)
	{
		___m_field_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_field_0), value);
	}

	inline static int32_t get_offset_of_m_a_1() { return static_cast<int32_t>(offsetof(ECCurve_t08AD8E722914B0B03C49C2D27B79B1D4E4770D95, ___m_a_1)); }
	inline ECFieldElement_tE2FF8892A60D0ACBC3FB665E41B0DEAA0308514C * get_m_a_1() const { return ___m_a_1; }
	inline ECFieldElement_tE2FF8892A60D0ACBC3FB665E41B0DEAA0308514C ** get_address_of_m_a_1() { return &___m_a_1; }
	inline void set_m_a_1(ECFieldElement_tE2FF8892A60D0ACBC3FB665E41B0DEAA0308514C * value)
	{
		___m_a_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_a_1), value);
	}

	inline static int32_t get_offset_of_m_b_2() { return static_cast<int32_t>(offsetof(ECCurve_t08AD8E722914B0B03C49C2D27B79B1D4E4770D95, ___m_b_2)); }
	inline ECFieldElement_tE2FF8892A60D0ACBC3FB665E41B0DEAA0308514C * get_m_b_2() const { return ___m_b_2; }
	inline ECFieldElement_tE2FF8892A60D0ACBC3FB665E41B0DEAA0308514C ** get_address_of_m_b_2() { return &___m_b_2; }
	inline void set_m_b_2(ECFieldElement_tE2FF8892A60D0ACBC3FB665E41B0DEAA0308514C * value)
	{
		___m_b_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_b_2), value);
	}

	inline static int32_t get_offset_of_m_order_3() { return static_cast<int32_t>(offsetof(ECCurve_t08AD8E722914B0B03C49C2D27B79B1D4E4770D95, ___m_order_3)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_m_order_3() const { return ___m_order_3; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_m_order_3() { return &___m_order_3; }
	inline void set_m_order_3(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___m_order_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_order_3), value);
	}

	inline static int32_t get_offset_of_m_cofactor_4() { return static_cast<int32_t>(offsetof(ECCurve_t08AD8E722914B0B03C49C2D27B79B1D4E4770D95, ___m_cofactor_4)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_m_cofactor_4() const { return ___m_cofactor_4; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_m_cofactor_4() { return &___m_cofactor_4; }
	inline void set_m_cofactor_4(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___m_cofactor_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_cofactor_4), value);
	}

	inline static int32_t get_offset_of_m_coord_5() { return static_cast<int32_t>(offsetof(ECCurve_t08AD8E722914B0B03C49C2D27B79B1D4E4770D95, ___m_coord_5)); }
	inline int32_t get_m_coord_5() const { return ___m_coord_5; }
	inline int32_t* get_address_of_m_coord_5() { return &___m_coord_5; }
	inline void set_m_coord_5(int32_t value)
	{
		___m_coord_5 = value;
	}

	inline static int32_t get_offset_of_m_endomorphism_6() { return static_cast<int32_t>(offsetof(ECCurve_t08AD8E722914B0B03C49C2D27B79B1D4E4770D95, ___m_endomorphism_6)); }
	inline RuntimeObject* get_m_endomorphism_6() const { return ___m_endomorphism_6; }
	inline RuntimeObject** get_address_of_m_endomorphism_6() { return &___m_endomorphism_6; }
	inline void set_m_endomorphism_6(RuntimeObject* value)
	{
		___m_endomorphism_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_endomorphism_6), value);
	}

	inline static int32_t get_offset_of_m_multiplier_7() { return static_cast<int32_t>(offsetof(ECCurve_t08AD8E722914B0B03C49C2D27B79B1D4E4770D95, ___m_multiplier_7)); }
	inline RuntimeObject* get_m_multiplier_7() const { return ___m_multiplier_7; }
	inline RuntimeObject** get_address_of_m_multiplier_7() { return &___m_multiplier_7; }
	inline void set_m_multiplier_7(RuntimeObject* value)
	{
		___m_multiplier_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_multiplier_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECCURVE_T08AD8E722914B0B03C49C2D27B79B1D4E4770D95_H
#ifndef ECFIELDELEMENT_TE2FF8892A60D0ACBC3FB665E41B0DEAA0308514C_H
#define ECFIELDELEMENT_TE2FF8892A60D0ACBC3FB665E41B0DEAA0308514C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.ECFieldElement
struct  ECFieldElement_tE2FF8892A60D0ACBC3FB665E41B0DEAA0308514C  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECFIELDELEMENT_TE2FF8892A60D0ACBC3FB665E41B0DEAA0308514C_H
#ifndef ECPOINT_TCC0C4447DEFF70FC9D68F5149ECCCC0533CE4399_H
#define ECPOINT_TCC0C4447DEFF70FC9D68F5149ECCCC0533CE4399_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.ECPoint
struct  ECPoint_tCC0C4447DEFF70FC9D68F5149ECCCC0533CE4399  : public RuntimeObject
{
public:
	// Org.BouncyCastle.Math.EC.ECCurve Org.BouncyCastle.Math.EC.ECPoint::m_curve
	ECCurve_t08AD8E722914B0B03C49C2D27B79B1D4E4770D95 * ___m_curve_1;
	// Org.BouncyCastle.Math.EC.ECFieldElement Org.BouncyCastle.Math.EC.ECPoint::m_x
	ECFieldElement_tE2FF8892A60D0ACBC3FB665E41B0DEAA0308514C * ___m_x_2;
	// Org.BouncyCastle.Math.EC.ECFieldElement Org.BouncyCastle.Math.EC.ECPoint::m_y
	ECFieldElement_tE2FF8892A60D0ACBC3FB665E41B0DEAA0308514C * ___m_y_3;
	// Org.BouncyCastle.Math.EC.ECFieldElement[] Org.BouncyCastle.Math.EC.ECPoint::m_zs
	ECFieldElementU5BU5D_tD63CAB58E0E42F9436F12569071F0663360A7609* ___m_zs_4;
	// System.Boolean Org.BouncyCastle.Math.EC.ECPoint::m_withCompression
	bool ___m_withCompression_5;
	// System.Collections.IDictionary Org.BouncyCastle.Math.EC.ECPoint::m_preCompTable
	RuntimeObject* ___m_preCompTable_6;

public:
	inline static int32_t get_offset_of_m_curve_1() { return static_cast<int32_t>(offsetof(ECPoint_tCC0C4447DEFF70FC9D68F5149ECCCC0533CE4399, ___m_curve_1)); }
	inline ECCurve_t08AD8E722914B0B03C49C2D27B79B1D4E4770D95 * get_m_curve_1() const { return ___m_curve_1; }
	inline ECCurve_t08AD8E722914B0B03C49C2D27B79B1D4E4770D95 ** get_address_of_m_curve_1() { return &___m_curve_1; }
	inline void set_m_curve_1(ECCurve_t08AD8E722914B0B03C49C2D27B79B1D4E4770D95 * value)
	{
		___m_curve_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_curve_1), value);
	}

	inline static int32_t get_offset_of_m_x_2() { return static_cast<int32_t>(offsetof(ECPoint_tCC0C4447DEFF70FC9D68F5149ECCCC0533CE4399, ___m_x_2)); }
	inline ECFieldElement_tE2FF8892A60D0ACBC3FB665E41B0DEAA0308514C * get_m_x_2() const { return ___m_x_2; }
	inline ECFieldElement_tE2FF8892A60D0ACBC3FB665E41B0DEAA0308514C ** get_address_of_m_x_2() { return &___m_x_2; }
	inline void set_m_x_2(ECFieldElement_tE2FF8892A60D0ACBC3FB665E41B0DEAA0308514C * value)
	{
		___m_x_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_x_2), value);
	}

	inline static int32_t get_offset_of_m_y_3() { return static_cast<int32_t>(offsetof(ECPoint_tCC0C4447DEFF70FC9D68F5149ECCCC0533CE4399, ___m_y_3)); }
	inline ECFieldElement_tE2FF8892A60D0ACBC3FB665E41B0DEAA0308514C * get_m_y_3() const { return ___m_y_3; }
	inline ECFieldElement_tE2FF8892A60D0ACBC3FB665E41B0DEAA0308514C ** get_address_of_m_y_3() { return &___m_y_3; }
	inline void set_m_y_3(ECFieldElement_tE2FF8892A60D0ACBC3FB665E41B0DEAA0308514C * value)
	{
		___m_y_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_y_3), value);
	}

	inline static int32_t get_offset_of_m_zs_4() { return static_cast<int32_t>(offsetof(ECPoint_tCC0C4447DEFF70FC9D68F5149ECCCC0533CE4399, ___m_zs_4)); }
	inline ECFieldElementU5BU5D_tD63CAB58E0E42F9436F12569071F0663360A7609* get_m_zs_4() const { return ___m_zs_4; }
	inline ECFieldElementU5BU5D_tD63CAB58E0E42F9436F12569071F0663360A7609** get_address_of_m_zs_4() { return &___m_zs_4; }
	inline void set_m_zs_4(ECFieldElementU5BU5D_tD63CAB58E0E42F9436F12569071F0663360A7609* value)
	{
		___m_zs_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_zs_4), value);
	}

	inline static int32_t get_offset_of_m_withCompression_5() { return static_cast<int32_t>(offsetof(ECPoint_tCC0C4447DEFF70FC9D68F5149ECCCC0533CE4399, ___m_withCompression_5)); }
	inline bool get_m_withCompression_5() const { return ___m_withCompression_5; }
	inline bool* get_address_of_m_withCompression_5() { return &___m_withCompression_5; }
	inline void set_m_withCompression_5(bool value)
	{
		___m_withCompression_5 = value;
	}

	inline static int32_t get_offset_of_m_preCompTable_6() { return static_cast<int32_t>(offsetof(ECPoint_tCC0C4447DEFF70FC9D68F5149ECCCC0533CE4399, ___m_preCompTable_6)); }
	inline RuntimeObject* get_m_preCompTable_6() const { return ___m_preCompTable_6; }
	inline RuntimeObject** get_address_of_m_preCompTable_6() { return &___m_preCompTable_6; }
	inline void set_m_preCompTable_6(RuntimeObject* value)
	{
		___m_preCompTable_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_preCompTable_6), value);
	}
};

struct ECPoint_tCC0C4447DEFF70FC9D68F5149ECCCC0533CE4399_StaticFields
{
public:
	// Org.BouncyCastle.Math.EC.ECFieldElement[] Org.BouncyCastle.Math.EC.ECPoint::EMPTY_ZS
	ECFieldElementU5BU5D_tD63CAB58E0E42F9436F12569071F0663360A7609* ___EMPTY_ZS_0;

public:
	inline static int32_t get_offset_of_EMPTY_ZS_0() { return static_cast<int32_t>(offsetof(ECPoint_tCC0C4447DEFF70FC9D68F5149ECCCC0533CE4399_StaticFields, ___EMPTY_ZS_0)); }
	inline ECFieldElementU5BU5D_tD63CAB58E0E42F9436F12569071F0663360A7609* get_EMPTY_ZS_0() const { return ___EMPTY_ZS_0; }
	inline ECFieldElementU5BU5D_tD63CAB58E0E42F9436F12569071F0663360A7609** get_address_of_EMPTY_ZS_0() { return &___EMPTY_ZS_0; }
	inline void set_EMPTY_ZS_0(ECFieldElementU5BU5D_tD63CAB58E0E42F9436F12569071F0663360A7609* value)
	{
		___EMPTY_ZS_0 = value;
		Il2CppCodeGenWriteBarrier((&___EMPTY_ZS_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECPOINT_TCC0C4447DEFF70FC9D68F5149ECCCC0533CE4399_H
#ifndef GLVTYPEBENDOMORPHISM_TAF04909F590E9A60E7019BA4B81C5FF31ABD6AA1_H
#define GLVTYPEBENDOMORPHISM_TAF04909F590E9A60E7019BA4B81C5FF31ABD6AA1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Endo.GlvTypeBEndomorphism
struct  GlvTypeBEndomorphism_tAF04909F590E9A60E7019BA4B81C5FF31ABD6AA1  : public RuntimeObject
{
public:
	// Org.BouncyCastle.Math.EC.ECCurve Org.BouncyCastle.Math.EC.Endo.GlvTypeBEndomorphism::m_curve
	ECCurve_t08AD8E722914B0B03C49C2D27B79B1D4E4770D95 * ___m_curve_0;
	// Org.BouncyCastle.Math.EC.Endo.GlvTypeBParameters Org.BouncyCastle.Math.EC.Endo.GlvTypeBEndomorphism::m_parameters
	GlvTypeBParameters_t39B069A23C048B4E871EB64BBE2F714DDD864095 * ___m_parameters_1;
	// Org.BouncyCastle.Math.EC.ECPointMap Org.BouncyCastle.Math.EC.Endo.GlvTypeBEndomorphism::m_pointMap
	RuntimeObject* ___m_pointMap_2;

public:
	inline static int32_t get_offset_of_m_curve_0() { return static_cast<int32_t>(offsetof(GlvTypeBEndomorphism_tAF04909F590E9A60E7019BA4B81C5FF31ABD6AA1, ___m_curve_0)); }
	inline ECCurve_t08AD8E722914B0B03C49C2D27B79B1D4E4770D95 * get_m_curve_0() const { return ___m_curve_0; }
	inline ECCurve_t08AD8E722914B0B03C49C2D27B79B1D4E4770D95 ** get_address_of_m_curve_0() { return &___m_curve_0; }
	inline void set_m_curve_0(ECCurve_t08AD8E722914B0B03C49C2D27B79B1D4E4770D95 * value)
	{
		___m_curve_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_curve_0), value);
	}

	inline static int32_t get_offset_of_m_parameters_1() { return static_cast<int32_t>(offsetof(GlvTypeBEndomorphism_tAF04909F590E9A60E7019BA4B81C5FF31ABD6AA1, ___m_parameters_1)); }
	inline GlvTypeBParameters_t39B069A23C048B4E871EB64BBE2F714DDD864095 * get_m_parameters_1() const { return ___m_parameters_1; }
	inline GlvTypeBParameters_t39B069A23C048B4E871EB64BBE2F714DDD864095 ** get_address_of_m_parameters_1() { return &___m_parameters_1; }
	inline void set_m_parameters_1(GlvTypeBParameters_t39B069A23C048B4E871EB64BBE2F714DDD864095 * value)
	{
		___m_parameters_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_parameters_1), value);
	}

	inline static int32_t get_offset_of_m_pointMap_2() { return static_cast<int32_t>(offsetof(GlvTypeBEndomorphism_tAF04909F590E9A60E7019BA4B81C5FF31ABD6AA1, ___m_pointMap_2)); }
	inline RuntimeObject* get_m_pointMap_2() const { return ___m_pointMap_2; }
	inline RuntimeObject** get_address_of_m_pointMap_2() { return &___m_pointMap_2; }
	inline void set_m_pointMap_2(RuntimeObject* value)
	{
		___m_pointMap_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_pointMap_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLVTYPEBENDOMORPHISM_TAF04909F590E9A60E7019BA4B81C5FF31ABD6AA1_H
#ifndef GLVTYPEBPARAMETERS_T39B069A23C048B4E871EB64BBE2F714DDD864095_H
#define GLVTYPEBPARAMETERS_T39B069A23C048B4E871EB64BBE2F714DDD864095_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Endo.GlvTypeBParameters
struct  GlvTypeBParameters_t39B069A23C048B4E871EB64BBE2F714DDD864095  : public RuntimeObject
{
public:
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Math.EC.Endo.GlvTypeBParameters::m_beta
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___m_beta_0;
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Math.EC.Endo.GlvTypeBParameters::m_lambda
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___m_lambda_1;
	// Org.BouncyCastle.Math.BigInteger[] Org.BouncyCastle.Math.EC.Endo.GlvTypeBParameters::m_v1
	BigIntegerU5BU5D_tE1C878446A6F27F09B57A1E5ECEBEF841F575311* ___m_v1_2;
	// Org.BouncyCastle.Math.BigInteger[] Org.BouncyCastle.Math.EC.Endo.GlvTypeBParameters::m_v2
	BigIntegerU5BU5D_tE1C878446A6F27F09B57A1E5ECEBEF841F575311* ___m_v2_3;
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Math.EC.Endo.GlvTypeBParameters::m_g1
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___m_g1_4;
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Math.EC.Endo.GlvTypeBParameters::m_g2
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___m_g2_5;
	// System.Int32 Org.BouncyCastle.Math.EC.Endo.GlvTypeBParameters::m_bits
	int32_t ___m_bits_6;

public:
	inline static int32_t get_offset_of_m_beta_0() { return static_cast<int32_t>(offsetof(GlvTypeBParameters_t39B069A23C048B4E871EB64BBE2F714DDD864095, ___m_beta_0)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_m_beta_0() const { return ___m_beta_0; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_m_beta_0() { return &___m_beta_0; }
	inline void set_m_beta_0(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___m_beta_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_beta_0), value);
	}

	inline static int32_t get_offset_of_m_lambda_1() { return static_cast<int32_t>(offsetof(GlvTypeBParameters_t39B069A23C048B4E871EB64BBE2F714DDD864095, ___m_lambda_1)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_m_lambda_1() const { return ___m_lambda_1; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_m_lambda_1() { return &___m_lambda_1; }
	inline void set_m_lambda_1(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___m_lambda_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_lambda_1), value);
	}

	inline static int32_t get_offset_of_m_v1_2() { return static_cast<int32_t>(offsetof(GlvTypeBParameters_t39B069A23C048B4E871EB64BBE2F714DDD864095, ___m_v1_2)); }
	inline BigIntegerU5BU5D_tE1C878446A6F27F09B57A1E5ECEBEF841F575311* get_m_v1_2() const { return ___m_v1_2; }
	inline BigIntegerU5BU5D_tE1C878446A6F27F09B57A1E5ECEBEF841F575311** get_address_of_m_v1_2() { return &___m_v1_2; }
	inline void set_m_v1_2(BigIntegerU5BU5D_tE1C878446A6F27F09B57A1E5ECEBEF841F575311* value)
	{
		___m_v1_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_v1_2), value);
	}

	inline static int32_t get_offset_of_m_v2_3() { return static_cast<int32_t>(offsetof(GlvTypeBParameters_t39B069A23C048B4E871EB64BBE2F714DDD864095, ___m_v2_3)); }
	inline BigIntegerU5BU5D_tE1C878446A6F27F09B57A1E5ECEBEF841F575311* get_m_v2_3() const { return ___m_v2_3; }
	inline BigIntegerU5BU5D_tE1C878446A6F27F09B57A1E5ECEBEF841F575311** get_address_of_m_v2_3() { return &___m_v2_3; }
	inline void set_m_v2_3(BigIntegerU5BU5D_tE1C878446A6F27F09B57A1E5ECEBEF841F575311* value)
	{
		___m_v2_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_v2_3), value);
	}

	inline static int32_t get_offset_of_m_g1_4() { return static_cast<int32_t>(offsetof(GlvTypeBParameters_t39B069A23C048B4E871EB64BBE2F714DDD864095, ___m_g1_4)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_m_g1_4() const { return ___m_g1_4; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_m_g1_4() { return &___m_g1_4; }
	inline void set_m_g1_4(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___m_g1_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_g1_4), value);
	}

	inline static int32_t get_offset_of_m_g2_5() { return static_cast<int32_t>(offsetof(GlvTypeBParameters_t39B069A23C048B4E871EB64BBE2F714DDD864095, ___m_g2_5)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_m_g2_5() const { return ___m_g2_5; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_m_g2_5() { return &___m_g2_5; }
	inline void set_m_g2_5(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___m_g2_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_g2_5), value);
	}

	inline static int32_t get_offset_of_m_bits_6() { return static_cast<int32_t>(offsetof(GlvTypeBParameters_t39B069A23C048B4E871EB64BBE2F714DDD864095, ___m_bits_6)); }
	inline int32_t get_m_bits_6() const { return ___m_bits_6; }
	inline int32_t* get_address_of_m_bits_6() { return &___m_bits_6; }
	inline void set_m_bits_6(int32_t value)
	{
		___m_bits_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLVTYPEBPARAMETERS_T39B069A23C048B4E871EB64BBE2F714DDD864095_H
#ifndef ABSTRACTECMULTIPLIER_T53B6ADC66D5D126840E4ADFF2C4B048592CB16D3_H
#define ABSTRACTECMULTIPLIER_T53B6ADC66D5D126840E4ADFF2C4B048592CB16D3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Multiplier.AbstractECMultiplier
struct  AbstractECMultiplier_t53B6ADC66D5D126840E4ADFF2C4B048592CB16D3  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTECMULTIPLIER_T53B6ADC66D5D126840E4ADFF2C4B048592CB16D3_H
#ifndef FIXEDPOINTPRECOMPINFO_T026E1CC2AE3FDB86A198646DA1BD0A1066B23A80_H
#define FIXEDPOINTPRECOMPINFO_T026E1CC2AE3FDB86A198646DA1BD0A1066B23A80_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Multiplier.FixedPointPreCompInfo
struct  FixedPointPreCompInfo_t026E1CC2AE3FDB86A198646DA1BD0A1066B23A80  : public RuntimeObject
{
public:
	// Org.BouncyCastle.Math.EC.ECPoint[] Org.BouncyCastle.Math.EC.Multiplier.FixedPointPreCompInfo::m_preComp
	ECPointU5BU5D_t12087A0694B5116AE6DA4708C7F3938BBDC98C5C* ___m_preComp_0;
	// System.Int32 Org.BouncyCastle.Math.EC.Multiplier.FixedPointPreCompInfo::m_width
	int32_t ___m_width_1;

public:
	inline static int32_t get_offset_of_m_preComp_0() { return static_cast<int32_t>(offsetof(FixedPointPreCompInfo_t026E1CC2AE3FDB86A198646DA1BD0A1066B23A80, ___m_preComp_0)); }
	inline ECPointU5BU5D_t12087A0694B5116AE6DA4708C7F3938BBDC98C5C* get_m_preComp_0() const { return ___m_preComp_0; }
	inline ECPointU5BU5D_t12087A0694B5116AE6DA4708C7F3938BBDC98C5C** get_address_of_m_preComp_0() { return &___m_preComp_0; }
	inline void set_m_preComp_0(ECPointU5BU5D_t12087A0694B5116AE6DA4708C7F3938BBDC98C5C* value)
	{
		___m_preComp_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_preComp_0), value);
	}

	inline static int32_t get_offset_of_m_width_1() { return static_cast<int32_t>(offsetof(FixedPointPreCompInfo_t026E1CC2AE3FDB86A198646DA1BD0A1066B23A80, ___m_width_1)); }
	inline int32_t get_m_width_1() const { return ___m_width_1; }
	inline int32_t* get_address_of_m_width_1() { return &___m_width_1; }
	inline void set_m_width_1(int32_t value)
	{
		___m_width_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIXEDPOINTPRECOMPINFO_T026E1CC2AE3FDB86A198646DA1BD0A1066B23A80_H
#ifndef FIXEDPOINTUTILITIES_T0AE152237232F5997834243B1B5B3F99A2719FFB_H
#define FIXEDPOINTUTILITIES_T0AE152237232F5997834243B1B5B3F99A2719FFB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Multiplier.FixedPointUtilities
struct  FixedPointUtilities_t0AE152237232F5997834243B1B5B3F99A2719FFB  : public RuntimeObject
{
public:

public:
};

struct FixedPointUtilities_t0AE152237232F5997834243B1B5B3F99A2719FFB_StaticFields
{
public:
	// System.String Org.BouncyCastle.Math.EC.Multiplier.FixedPointUtilities::PRECOMP_NAME
	String_t* ___PRECOMP_NAME_0;

public:
	inline static int32_t get_offset_of_PRECOMP_NAME_0() { return static_cast<int32_t>(offsetof(FixedPointUtilities_t0AE152237232F5997834243B1B5B3F99A2719FFB_StaticFields, ___PRECOMP_NAME_0)); }
	inline String_t* get_PRECOMP_NAME_0() const { return ___PRECOMP_NAME_0; }
	inline String_t** get_address_of_PRECOMP_NAME_0() { return &___PRECOMP_NAME_0; }
	inline void set_PRECOMP_NAME_0(String_t* value)
	{
		___PRECOMP_NAME_0 = value;
		Il2CppCodeGenWriteBarrier((&___PRECOMP_NAME_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIXEDPOINTUTILITIES_T0AE152237232F5997834243B1B5B3F99A2719FFB_H
#ifndef WNAFPRECOMPINFO_T7718F0CE35D08CF11B747D138B0EEBAA71009746_H
#define WNAFPRECOMPINFO_T7718F0CE35D08CF11B747D138B0EEBAA71009746_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Multiplier.WNafPreCompInfo
struct  WNafPreCompInfo_t7718F0CE35D08CF11B747D138B0EEBAA71009746  : public RuntimeObject
{
public:
	// Org.BouncyCastle.Math.EC.ECPoint[] Org.BouncyCastle.Math.EC.Multiplier.WNafPreCompInfo::m_preComp
	ECPointU5BU5D_t12087A0694B5116AE6DA4708C7F3938BBDC98C5C* ___m_preComp_0;
	// Org.BouncyCastle.Math.EC.ECPoint[] Org.BouncyCastle.Math.EC.Multiplier.WNafPreCompInfo::m_preCompNeg
	ECPointU5BU5D_t12087A0694B5116AE6DA4708C7F3938BBDC98C5C* ___m_preCompNeg_1;
	// Org.BouncyCastle.Math.EC.ECPoint Org.BouncyCastle.Math.EC.Multiplier.WNafPreCompInfo::m_twice
	ECPoint_tCC0C4447DEFF70FC9D68F5149ECCCC0533CE4399 * ___m_twice_2;

public:
	inline static int32_t get_offset_of_m_preComp_0() { return static_cast<int32_t>(offsetof(WNafPreCompInfo_t7718F0CE35D08CF11B747D138B0EEBAA71009746, ___m_preComp_0)); }
	inline ECPointU5BU5D_t12087A0694B5116AE6DA4708C7F3938BBDC98C5C* get_m_preComp_0() const { return ___m_preComp_0; }
	inline ECPointU5BU5D_t12087A0694B5116AE6DA4708C7F3938BBDC98C5C** get_address_of_m_preComp_0() { return &___m_preComp_0; }
	inline void set_m_preComp_0(ECPointU5BU5D_t12087A0694B5116AE6DA4708C7F3938BBDC98C5C* value)
	{
		___m_preComp_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_preComp_0), value);
	}

	inline static int32_t get_offset_of_m_preCompNeg_1() { return static_cast<int32_t>(offsetof(WNafPreCompInfo_t7718F0CE35D08CF11B747D138B0EEBAA71009746, ___m_preCompNeg_1)); }
	inline ECPointU5BU5D_t12087A0694B5116AE6DA4708C7F3938BBDC98C5C* get_m_preCompNeg_1() const { return ___m_preCompNeg_1; }
	inline ECPointU5BU5D_t12087A0694B5116AE6DA4708C7F3938BBDC98C5C** get_address_of_m_preCompNeg_1() { return &___m_preCompNeg_1; }
	inline void set_m_preCompNeg_1(ECPointU5BU5D_t12087A0694B5116AE6DA4708C7F3938BBDC98C5C* value)
	{
		___m_preCompNeg_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_preCompNeg_1), value);
	}

	inline static int32_t get_offset_of_m_twice_2() { return static_cast<int32_t>(offsetof(WNafPreCompInfo_t7718F0CE35D08CF11B747D138B0EEBAA71009746, ___m_twice_2)); }
	inline ECPoint_tCC0C4447DEFF70FC9D68F5149ECCCC0533CE4399 * get_m_twice_2() const { return ___m_twice_2; }
	inline ECPoint_tCC0C4447DEFF70FC9D68F5149ECCCC0533CE4399 ** get_address_of_m_twice_2() { return &___m_twice_2; }
	inline void set_m_twice_2(ECPoint_tCC0C4447DEFF70FC9D68F5149ECCCC0533CE4399 * value)
	{
		___m_twice_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_twice_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WNAFPRECOMPINFO_T7718F0CE35D08CF11B747D138B0EEBAA71009746_H
#ifndef WNAFUTILITIES_T5C561C6BAF9EDDB2CA522857116F071440DE6AAA_H
#define WNAFUTILITIES_T5C561C6BAF9EDDB2CA522857116F071440DE6AAA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Multiplier.WNafUtilities
struct  WNafUtilities_t5C561C6BAF9EDDB2CA522857116F071440DE6AAA  : public RuntimeObject
{
public:

public:
};

struct WNafUtilities_t5C561C6BAF9EDDB2CA522857116F071440DE6AAA_StaticFields
{
public:
	// System.String Org.BouncyCastle.Math.EC.Multiplier.WNafUtilities::PRECOMP_NAME
	String_t* ___PRECOMP_NAME_0;
	// System.Int32[] Org.BouncyCastle.Math.EC.Multiplier.WNafUtilities::DEFAULT_WINDOW_SIZE_CUTOFFS
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DEFAULT_WINDOW_SIZE_CUTOFFS_1;
	// System.Byte[] Org.BouncyCastle.Math.EC.Multiplier.WNafUtilities::EMPTY_BYTES
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___EMPTY_BYTES_2;
	// System.Int32[] Org.BouncyCastle.Math.EC.Multiplier.WNafUtilities::EMPTY_INTS
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___EMPTY_INTS_3;
	// Org.BouncyCastle.Math.EC.ECPoint[] Org.BouncyCastle.Math.EC.Multiplier.WNafUtilities::EMPTY_POINTS
	ECPointU5BU5D_t12087A0694B5116AE6DA4708C7F3938BBDC98C5C* ___EMPTY_POINTS_4;

public:
	inline static int32_t get_offset_of_PRECOMP_NAME_0() { return static_cast<int32_t>(offsetof(WNafUtilities_t5C561C6BAF9EDDB2CA522857116F071440DE6AAA_StaticFields, ___PRECOMP_NAME_0)); }
	inline String_t* get_PRECOMP_NAME_0() const { return ___PRECOMP_NAME_0; }
	inline String_t** get_address_of_PRECOMP_NAME_0() { return &___PRECOMP_NAME_0; }
	inline void set_PRECOMP_NAME_0(String_t* value)
	{
		___PRECOMP_NAME_0 = value;
		Il2CppCodeGenWriteBarrier((&___PRECOMP_NAME_0), value);
	}

	inline static int32_t get_offset_of_DEFAULT_WINDOW_SIZE_CUTOFFS_1() { return static_cast<int32_t>(offsetof(WNafUtilities_t5C561C6BAF9EDDB2CA522857116F071440DE6AAA_StaticFields, ___DEFAULT_WINDOW_SIZE_CUTOFFS_1)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DEFAULT_WINDOW_SIZE_CUTOFFS_1() const { return ___DEFAULT_WINDOW_SIZE_CUTOFFS_1; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DEFAULT_WINDOW_SIZE_CUTOFFS_1() { return &___DEFAULT_WINDOW_SIZE_CUTOFFS_1; }
	inline void set_DEFAULT_WINDOW_SIZE_CUTOFFS_1(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DEFAULT_WINDOW_SIZE_CUTOFFS_1 = value;
		Il2CppCodeGenWriteBarrier((&___DEFAULT_WINDOW_SIZE_CUTOFFS_1), value);
	}

	inline static int32_t get_offset_of_EMPTY_BYTES_2() { return static_cast<int32_t>(offsetof(WNafUtilities_t5C561C6BAF9EDDB2CA522857116F071440DE6AAA_StaticFields, ___EMPTY_BYTES_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_EMPTY_BYTES_2() const { return ___EMPTY_BYTES_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_EMPTY_BYTES_2() { return &___EMPTY_BYTES_2; }
	inline void set_EMPTY_BYTES_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___EMPTY_BYTES_2 = value;
		Il2CppCodeGenWriteBarrier((&___EMPTY_BYTES_2), value);
	}

	inline static int32_t get_offset_of_EMPTY_INTS_3() { return static_cast<int32_t>(offsetof(WNafUtilities_t5C561C6BAF9EDDB2CA522857116F071440DE6AAA_StaticFields, ___EMPTY_INTS_3)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_EMPTY_INTS_3() const { return ___EMPTY_INTS_3; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_EMPTY_INTS_3() { return &___EMPTY_INTS_3; }
	inline void set_EMPTY_INTS_3(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___EMPTY_INTS_3 = value;
		Il2CppCodeGenWriteBarrier((&___EMPTY_INTS_3), value);
	}

	inline static int32_t get_offset_of_EMPTY_POINTS_4() { return static_cast<int32_t>(offsetof(WNafUtilities_t5C561C6BAF9EDDB2CA522857116F071440DE6AAA_StaticFields, ___EMPTY_POINTS_4)); }
	inline ECPointU5BU5D_t12087A0694B5116AE6DA4708C7F3938BBDC98C5C* get_EMPTY_POINTS_4() const { return ___EMPTY_POINTS_4; }
	inline ECPointU5BU5D_t12087A0694B5116AE6DA4708C7F3938BBDC98C5C** get_address_of_EMPTY_POINTS_4() { return &___EMPTY_POINTS_4; }
	inline void set_EMPTY_POINTS_4(ECPointU5BU5D_t12087A0694B5116AE6DA4708C7F3938BBDC98C5C* value)
	{
		___EMPTY_POINTS_4 = value;
		Il2CppCodeGenWriteBarrier((&___EMPTY_POINTS_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WNAFUTILITIES_T5C561C6BAF9EDDB2CA522857116F071440DE6AAA_H
#ifndef WTAUNAFPRECOMPINFO_TB2474DD90D64E4C74787B55B6EEAB92C2C2A8662_H
#define WTAUNAFPRECOMPINFO_TB2474DD90D64E4C74787B55B6EEAB92C2C2A8662_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Multiplier.WTauNafPreCompInfo
struct  WTauNafPreCompInfo_tB2474DD90D64E4C74787B55B6EEAB92C2C2A8662  : public RuntimeObject
{
public:
	// Org.BouncyCastle.Math.EC.AbstractF2mPoint[] Org.BouncyCastle.Math.EC.Multiplier.WTauNafPreCompInfo::m_preComp
	AbstractF2mPointU5BU5D_t75025014FD06ECF73A522CA4427FD210274898E1* ___m_preComp_0;

public:
	inline static int32_t get_offset_of_m_preComp_0() { return static_cast<int32_t>(offsetof(WTauNafPreCompInfo_tB2474DD90D64E4C74787B55B6EEAB92C2C2A8662, ___m_preComp_0)); }
	inline AbstractF2mPointU5BU5D_t75025014FD06ECF73A522CA4427FD210274898E1* get_m_preComp_0() const { return ___m_preComp_0; }
	inline AbstractF2mPointU5BU5D_t75025014FD06ECF73A522CA4427FD210274898E1** get_address_of_m_preComp_0() { return &___m_preComp_0; }
	inline void set_m_preComp_0(AbstractF2mPointU5BU5D_t75025014FD06ECF73A522CA4427FD210274898E1* value)
	{
		___m_preComp_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_preComp_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WTAUNAFPRECOMPINFO_TB2474DD90D64E4C74787B55B6EEAB92C2C2A8662_H
#ifndef FINITEFIELDS_T0A726393363CD545AF14EB735BD270DB62C6FD41_H
#define FINITEFIELDS_T0A726393363CD545AF14EB735BD270DB62C6FD41_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.Field.FiniteFields
struct  FiniteFields_t0A726393363CD545AF14EB735BD270DB62C6FD41  : public RuntimeObject
{
public:

public:
};

struct FiniteFields_t0A726393363CD545AF14EB735BD270DB62C6FD41_StaticFields
{
public:
	// Org.BouncyCastle.Math.Field.IFiniteField Org.BouncyCastle.Math.Field.FiniteFields::GF_2
	RuntimeObject* ___GF_2_0;
	// Org.BouncyCastle.Math.Field.IFiniteField Org.BouncyCastle.Math.Field.FiniteFields::GF_3
	RuntimeObject* ___GF_3_1;

public:
	inline static int32_t get_offset_of_GF_2_0() { return static_cast<int32_t>(offsetof(FiniteFields_t0A726393363CD545AF14EB735BD270DB62C6FD41_StaticFields, ___GF_2_0)); }
	inline RuntimeObject* get_GF_2_0() const { return ___GF_2_0; }
	inline RuntimeObject** get_address_of_GF_2_0() { return &___GF_2_0; }
	inline void set_GF_2_0(RuntimeObject* value)
	{
		___GF_2_0 = value;
		Il2CppCodeGenWriteBarrier((&___GF_2_0), value);
	}

	inline static int32_t get_offset_of_GF_3_1() { return static_cast<int32_t>(offsetof(FiniteFields_t0A726393363CD545AF14EB735BD270DB62C6FD41_StaticFields, ___GF_3_1)); }
	inline RuntimeObject* get_GF_3_1() const { return ___GF_3_1; }
	inline RuntimeObject** get_address_of_GF_3_1() { return &___GF_3_1; }
	inline void set_GF_3_1(RuntimeObject* value)
	{
		___GF_3_1 = value;
		Il2CppCodeGenWriteBarrier((&___GF_3_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FINITEFIELDS_T0A726393363CD545AF14EB735BD270DB62C6FD41_H
#ifndef GF2POLYNOMIAL_T268B6A651AE4801B8EB70232EECB1403A44363C7_H
#define GF2POLYNOMIAL_T268B6A651AE4801B8EB70232EECB1403A44363C7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.Field.GF2Polynomial
struct  GF2Polynomial_t268B6A651AE4801B8EB70232EECB1403A44363C7  : public RuntimeObject
{
public:
	// System.Int32[] Org.BouncyCastle.Math.Field.GF2Polynomial::exponents
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___exponents_0;

public:
	inline static int32_t get_offset_of_exponents_0() { return static_cast<int32_t>(offsetof(GF2Polynomial_t268B6A651AE4801B8EB70232EECB1403A44363C7, ___exponents_0)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_exponents_0() const { return ___exponents_0; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_exponents_0() { return &___exponents_0; }
	inline void set_exponents_0(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___exponents_0 = value;
		Il2CppCodeGenWriteBarrier((&___exponents_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GF2POLYNOMIAL_T268B6A651AE4801B8EB70232EECB1403A44363C7_H
#ifndef GENERICPOLYNOMIALEXTENSIONFIELD_T92D8755AAC2EA9D5A2EAD32AA1F87555CBFD311A_H
#define GENERICPOLYNOMIALEXTENSIONFIELD_T92D8755AAC2EA9D5A2EAD32AA1F87555CBFD311A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.Field.GenericPolynomialExtensionField
struct  GenericPolynomialExtensionField_t92D8755AAC2EA9D5A2EAD32AA1F87555CBFD311A  : public RuntimeObject
{
public:
	// Org.BouncyCastle.Math.Field.IFiniteField Org.BouncyCastle.Math.Field.GenericPolynomialExtensionField::subfield
	RuntimeObject* ___subfield_0;
	// Org.BouncyCastle.Math.Field.IPolynomial Org.BouncyCastle.Math.Field.GenericPolynomialExtensionField::minimalPolynomial
	RuntimeObject* ___minimalPolynomial_1;

public:
	inline static int32_t get_offset_of_subfield_0() { return static_cast<int32_t>(offsetof(GenericPolynomialExtensionField_t92D8755AAC2EA9D5A2EAD32AA1F87555CBFD311A, ___subfield_0)); }
	inline RuntimeObject* get_subfield_0() const { return ___subfield_0; }
	inline RuntimeObject** get_address_of_subfield_0() { return &___subfield_0; }
	inline void set_subfield_0(RuntimeObject* value)
	{
		___subfield_0 = value;
		Il2CppCodeGenWriteBarrier((&___subfield_0), value);
	}

	inline static int32_t get_offset_of_minimalPolynomial_1() { return static_cast<int32_t>(offsetof(GenericPolynomialExtensionField_t92D8755AAC2EA9D5A2EAD32AA1F87555CBFD311A, ___minimalPolynomial_1)); }
	inline RuntimeObject* get_minimalPolynomial_1() const { return ___minimalPolynomial_1; }
	inline RuntimeObject** get_address_of_minimalPolynomial_1() { return &___minimalPolynomial_1; }
	inline void set_minimalPolynomial_1(RuntimeObject* value)
	{
		___minimalPolynomial_1 = value;
		Il2CppCodeGenWriteBarrier((&___minimalPolynomial_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICPOLYNOMIALEXTENSIONFIELD_T92D8755AAC2EA9D5A2EAD32AA1F87555CBFD311A_H
#ifndef PRIMEFIELD_TC739CE15084D2C1F30625BBDF0DB282FA0C68E85_H
#define PRIMEFIELD_TC739CE15084D2C1F30625BBDF0DB282FA0C68E85_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.Field.PrimeField
struct  PrimeField_tC739CE15084D2C1F30625BBDF0DB282FA0C68E85  : public RuntimeObject
{
public:
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Math.Field.PrimeField::characteristic
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___characteristic_0;

public:
	inline static int32_t get_offset_of_characteristic_0() { return static_cast<int32_t>(offsetof(PrimeField_tC739CE15084D2C1F30625BBDF0DB282FA0C68E85, ___characteristic_0)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_characteristic_0() const { return ___characteristic_0; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_characteristic_0() { return &___characteristic_0; }
	inline void set_characteristic_0(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___characteristic_0 = value;
		Il2CppCodeGenWriteBarrier((&___characteristic_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRIMEFIELD_TC739CE15084D2C1F30625BBDF0DB282FA0C68E85_H
#ifndef INTERLEAVE_T8B36F4EB3242C1AB7CF0A68CFFE28547FF25B87C_H
#define INTERLEAVE_T8B36F4EB3242C1AB7CF0A68CFFE28547FF25B87C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.Raw.Interleave
struct  Interleave_t8B36F4EB3242C1AB7CF0A68CFFE28547FF25B87C  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERLEAVE_T8B36F4EB3242C1AB7CF0A68CFFE28547FF25B87C_H
#ifndef MOD_T5026E714058F605ACC98A7F33710A603850979E8_H
#define MOD_T5026E714058F605ACC98A7F33710A603850979E8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.Raw.Mod
struct  Mod_t5026E714058F605ACC98A7F33710A603850979E8  : public RuntimeObject
{
public:

public:
};

struct Mod_t5026E714058F605ACC98A7F33710A603850979E8_StaticFields
{
public:
	// Org.BouncyCastle.Security.SecureRandom Org.BouncyCastle.Math.Raw.Mod::RandomSource
	SecureRandom_t0B6C2C3EDF11BF4FD22E44CE8997F6A0D2F142B0 * ___RandomSource_0;

public:
	inline static int32_t get_offset_of_RandomSource_0() { return static_cast<int32_t>(offsetof(Mod_t5026E714058F605ACC98A7F33710A603850979E8_StaticFields, ___RandomSource_0)); }
	inline SecureRandom_t0B6C2C3EDF11BF4FD22E44CE8997F6A0D2F142B0 * get_RandomSource_0() const { return ___RandomSource_0; }
	inline SecureRandom_t0B6C2C3EDF11BF4FD22E44CE8997F6A0D2F142B0 ** get_address_of_RandomSource_0() { return &___RandomSource_0; }
	inline void set_RandomSource_0(SecureRandom_t0B6C2C3EDF11BF4FD22E44CE8997F6A0D2F142B0 * value)
	{
		___RandomSource_0 = value;
		Il2CppCodeGenWriteBarrier((&___RandomSource_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOD_T5026E714058F605ACC98A7F33710A603850979E8_H
#ifndef NAT_T1A3C75E3A07B58588BEBA310D2E23695EBE3C496_H
#define NAT_T1A3C75E3A07B58588BEBA310D2E23695EBE3C496_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.Raw.Nat
struct  Nat_t1A3C75E3A07B58588BEBA310D2E23695EBE3C496  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAT_T1A3C75E3A07B58588BEBA310D2E23695EBE3C496_H
#ifndef NAT128_TBEDF74DA30BFC396E9C10D1DD21FCD56F9A57626_H
#define NAT128_TBEDF74DA30BFC396E9C10D1DD21FCD56F9A57626_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.Raw.Nat128
struct  Nat128_tBEDF74DA30BFC396E9C10D1DD21FCD56F9A57626  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAT128_TBEDF74DA30BFC396E9C10D1DD21FCD56F9A57626_H
#ifndef NAT160_T7506F58D3F2E4A558A6BF24251EF2986CD4EE02F_H
#define NAT160_T7506F58D3F2E4A558A6BF24251EF2986CD4EE02F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.Raw.Nat160
struct  Nat160_t7506F58D3F2E4A558A6BF24251EF2986CD4EE02F  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAT160_T7506F58D3F2E4A558A6BF24251EF2986CD4EE02F_H
#ifndef ABSTRACTF2MCURVE_T4AA98C03B9F426713CE47E921AA07B9623F34391_H
#define ABSTRACTF2MCURVE_T4AA98C03B9F426713CE47E921AA07B9623F34391_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.AbstractF2mCurve
struct  AbstractF2mCurve_t4AA98C03B9F426713CE47E921AA07B9623F34391  : public ECCurve_t08AD8E722914B0B03C49C2D27B79B1D4E4770D95
{
public:
	// Org.BouncyCastle.Math.BigInteger[] Org.BouncyCastle.Math.EC.AbstractF2mCurve::si
	BigIntegerU5BU5D_tE1C878446A6F27F09B57A1E5ECEBEF841F575311* ___si_8;

public:
	inline static int32_t get_offset_of_si_8() { return static_cast<int32_t>(offsetof(AbstractF2mCurve_t4AA98C03B9F426713CE47E921AA07B9623F34391, ___si_8)); }
	inline BigIntegerU5BU5D_tE1C878446A6F27F09B57A1E5ECEBEF841F575311* get_si_8() const { return ___si_8; }
	inline BigIntegerU5BU5D_tE1C878446A6F27F09B57A1E5ECEBEF841F575311** get_address_of_si_8() { return &___si_8; }
	inline void set_si_8(BigIntegerU5BU5D_tE1C878446A6F27F09B57A1E5ECEBEF841F575311* value)
	{
		___si_8 = value;
		Il2CppCodeGenWriteBarrier((&___si_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTF2MCURVE_T4AA98C03B9F426713CE47E921AA07B9623F34391_H
#ifndef ABSTRACTFPCURVE_TC851390204D76E2F3E8CBDC56187D3375F05FF75_H
#define ABSTRACTFPCURVE_TC851390204D76E2F3E8CBDC56187D3375F05FF75_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.AbstractFpCurve
struct  AbstractFpCurve_tC851390204D76E2F3E8CBDC56187D3375F05FF75  : public ECCurve_t08AD8E722914B0B03C49C2D27B79B1D4E4770D95
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTFPCURVE_TC851390204D76E2F3E8CBDC56187D3375F05FF75_H
#ifndef SECP256K1FIELDELEMENT_TAD0D5656F3082824DA68A3EA2D7124AA21207579_H
#define SECP256K1FIELDELEMENT_TAD0D5656F3082824DA68A3EA2D7124AA21207579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecP256K1FieldElement
struct  SecP256K1FieldElement_tAD0D5656F3082824DA68A3EA2D7124AA21207579  : public ECFieldElement_tE2FF8892A60D0ACBC3FB665E41B0DEAA0308514C
{
public:
	// System.UInt32[] Org.BouncyCastle.Math.EC.Custom.Sec.SecP256K1FieldElement::x
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___x_1;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(SecP256K1FieldElement_tAD0D5656F3082824DA68A3EA2D7124AA21207579, ___x_1)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_x_1() const { return ___x_1; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___x_1 = value;
		Il2CppCodeGenWriteBarrier((&___x_1), value);
	}
};

struct SecP256K1FieldElement_tAD0D5656F3082824DA68A3EA2D7124AA21207579_StaticFields
{
public:
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Math.EC.Custom.Sec.SecP256K1FieldElement::Q
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___Q_0;

public:
	inline static int32_t get_offset_of_Q_0() { return static_cast<int32_t>(offsetof(SecP256K1FieldElement_tAD0D5656F3082824DA68A3EA2D7124AA21207579_StaticFields, ___Q_0)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_Q_0() const { return ___Q_0; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_Q_0() { return &___Q_0; }
	inline void set_Q_0(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___Q_0 = value;
		Il2CppCodeGenWriteBarrier((&___Q_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP256K1FIELDELEMENT_TAD0D5656F3082824DA68A3EA2D7124AA21207579_H
#ifndef SECP256R1FIELDELEMENT_T8BF1F4BC229166173D5D9884FE6A1B955299003C_H
#define SECP256R1FIELDELEMENT_T8BF1F4BC229166173D5D9884FE6A1B955299003C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecP256R1FieldElement
struct  SecP256R1FieldElement_t8BF1F4BC229166173D5D9884FE6A1B955299003C  : public ECFieldElement_tE2FF8892A60D0ACBC3FB665E41B0DEAA0308514C
{
public:
	// System.UInt32[] Org.BouncyCastle.Math.EC.Custom.Sec.SecP256R1FieldElement::x
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___x_1;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(SecP256R1FieldElement_t8BF1F4BC229166173D5D9884FE6A1B955299003C, ___x_1)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_x_1() const { return ___x_1; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___x_1 = value;
		Il2CppCodeGenWriteBarrier((&___x_1), value);
	}
};

struct SecP256R1FieldElement_t8BF1F4BC229166173D5D9884FE6A1B955299003C_StaticFields
{
public:
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Math.EC.Custom.Sec.SecP256R1FieldElement::Q
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___Q_0;

public:
	inline static int32_t get_offset_of_Q_0() { return static_cast<int32_t>(offsetof(SecP256R1FieldElement_t8BF1F4BC229166173D5D9884FE6A1B955299003C_StaticFields, ___Q_0)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_Q_0() const { return ___Q_0; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_Q_0() { return &___Q_0; }
	inline void set_Q_0(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___Q_0 = value;
		Il2CppCodeGenWriteBarrier((&___Q_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP256R1FIELDELEMENT_T8BF1F4BC229166173D5D9884FE6A1B955299003C_H
#ifndef SECP384R1FIELDELEMENT_T10047E0C5A2477110224F067DE1749A0811A837D_H
#define SECP384R1FIELDELEMENT_T10047E0C5A2477110224F067DE1749A0811A837D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecP384R1FieldElement
struct  SecP384R1FieldElement_t10047E0C5A2477110224F067DE1749A0811A837D  : public ECFieldElement_tE2FF8892A60D0ACBC3FB665E41B0DEAA0308514C
{
public:
	// System.UInt32[] Org.BouncyCastle.Math.EC.Custom.Sec.SecP384R1FieldElement::x
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___x_1;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(SecP384R1FieldElement_t10047E0C5A2477110224F067DE1749A0811A837D, ___x_1)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_x_1() const { return ___x_1; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___x_1 = value;
		Il2CppCodeGenWriteBarrier((&___x_1), value);
	}
};

struct SecP384R1FieldElement_t10047E0C5A2477110224F067DE1749A0811A837D_StaticFields
{
public:
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Math.EC.Custom.Sec.SecP384R1FieldElement::Q
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___Q_0;

public:
	inline static int32_t get_offset_of_Q_0() { return static_cast<int32_t>(offsetof(SecP384R1FieldElement_t10047E0C5A2477110224F067DE1749A0811A837D_StaticFields, ___Q_0)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_Q_0() const { return ___Q_0; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_Q_0() { return &___Q_0; }
	inline void set_Q_0(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___Q_0 = value;
		Il2CppCodeGenWriteBarrier((&___Q_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP384R1FIELDELEMENT_T10047E0C5A2477110224F067DE1749A0811A837D_H
#ifndef SECP521R1FIELDELEMENT_T20122F081AC9EA686EFB0B8649FC60EF6792C24F_H
#define SECP521R1FIELDELEMENT_T20122F081AC9EA686EFB0B8649FC60EF6792C24F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecP521R1FieldElement
struct  SecP521R1FieldElement_t20122F081AC9EA686EFB0B8649FC60EF6792C24F  : public ECFieldElement_tE2FF8892A60D0ACBC3FB665E41B0DEAA0308514C
{
public:
	// System.UInt32[] Org.BouncyCastle.Math.EC.Custom.Sec.SecP521R1FieldElement::x
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___x_1;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(SecP521R1FieldElement_t20122F081AC9EA686EFB0B8649FC60EF6792C24F, ___x_1)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_x_1() const { return ___x_1; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___x_1 = value;
		Il2CppCodeGenWriteBarrier((&___x_1), value);
	}
};

struct SecP521R1FieldElement_t20122F081AC9EA686EFB0B8649FC60EF6792C24F_StaticFields
{
public:
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Math.EC.Custom.Sec.SecP521R1FieldElement::Q
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___Q_0;

public:
	inline static int32_t get_offset_of_Q_0() { return static_cast<int32_t>(offsetof(SecP521R1FieldElement_t20122F081AC9EA686EFB0B8649FC60EF6792C24F_StaticFields, ___Q_0)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_Q_0() const { return ___Q_0; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_Q_0() { return &___Q_0; }
	inline void set_Q_0(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___Q_0 = value;
		Il2CppCodeGenWriteBarrier((&___Q_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP521R1FIELDELEMENT_T20122F081AC9EA686EFB0B8649FC60EF6792C24F_H
#ifndef SECT113FIELDELEMENT_T7769E2514486C96383271692ED57E8CEE8AAF050_H
#define SECT113FIELDELEMENT_T7769E2514486C96383271692ED57E8CEE8AAF050_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecT113FieldElement
struct  SecT113FieldElement_t7769E2514486C96383271692ED57E8CEE8AAF050  : public ECFieldElement_tE2FF8892A60D0ACBC3FB665E41B0DEAA0308514C
{
public:
	// System.UInt64[] Org.BouncyCastle.Math.EC.Custom.Sec.SecT113FieldElement::x
	UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* ___x_0;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(SecT113FieldElement_t7769E2514486C96383271692ED57E8CEE8AAF050, ___x_0)); }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* get_x_0() const { return ___x_0; }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4** get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* value)
	{
		___x_0 = value;
		Il2CppCodeGenWriteBarrier((&___x_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT113FIELDELEMENT_T7769E2514486C96383271692ED57E8CEE8AAF050_H
#ifndef SECT131FIELDELEMENT_TF4A8DB14108F9419F12F280364EA6A5BEA3D1BDE_H
#define SECT131FIELDELEMENT_TF4A8DB14108F9419F12F280364EA6A5BEA3D1BDE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecT131FieldElement
struct  SecT131FieldElement_tF4A8DB14108F9419F12F280364EA6A5BEA3D1BDE  : public ECFieldElement_tE2FF8892A60D0ACBC3FB665E41B0DEAA0308514C
{
public:
	// System.UInt64[] Org.BouncyCastle.Math.EC.Custom.Sec.SecT131FieldElement::x
	UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* ___x_0;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(SecT131FieldElement_tF4A8DB14108F9419F12F280364EA6A5BEA3D1BDE, ___x_0)); }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* get_x_0() const { return ___x_0; }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4** get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* value)
	{
		___x_0 = value;
		Il2CppCodeGenWriteBarrier((&___x_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT131FIELDELEMENT_TF4A8DB14108F9419F12F280364EA6A5BEA3D1BDE_H
#ifndef SECT163FIELDELEMENT_T0FEFDCFBAD3998866C45862284914966A0417D2A_H
#define SECT163FIELDELEMENT_T0FEFDCFBAD3998866C45862284914966A0417D2A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecT163FieldElement
struct  SecT163FieldElement_t0FEFDCFBAD3998866C45862284914966A0417D2A  : public ECFieldElement_tE2FF8892A60D0ACBC3FB665E41B0DEAA0308514C
{
public:
	// System.UInt64[] Org.BouncyCastle.Math.EC.Custom.Sec.SecT163FieldElement::x
	UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* ___x_0;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(SecT163FieldElement_t0FEFDCFBAD3998866C45862284914966A0417D2A, ___x_0)); }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* get_x_0() const { return ___x_0; }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4** get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* value)
	{
		___x_0 = value;
		Il2CppCodeGenWriteBarrier((&___x_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT163FIELDELEMENT_T0FEFDCFBAD3998866C45862284914966A0417D2A_H
#ifndef SECT193FIELDELEMENT_TF52AD9A3BB90ABC7F3C9F705C46D8E963297C6F0_H
#define SECT193FIELDELEMENT_TF52AD9A3BB90ABC7F3C9F705C46D8E963297C6F0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecT193FieldElement
struct  SecT193FieldElement_tF52AD9A3BB90ABC7F3C9F705C46D8E963297C6F0  : public ECFieldElement_tE2FF8892A60D0ACBC3FB665E41B0DEAA0308514C
{
public:
	// System.UInt64[] Org.BouncyCastle.Math.EC.Custom.Sec.SecT193FieldElement::x
	UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* ___x_0;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(SecT193FieldElement_tF52AD9A3BB90ABC7F3C9F705C46D8E963297C6F0, ___x_0)); }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* get_x_0() const { return ___x_0; }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4** get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* value)
	{
		___x_0 = value;
		Il2CppCodeGenWriteBarrier((&___x_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT193FIELDELEMENT_TF52AD9A3BB90ABC7F3C9F705C46D8E963297C6F0_H
#ifndef SECT233FIELDELEMENT_T1A8CD620A7896D5F216E4114BBC8EE828552A189_H
#define SECT233FIELDELEMENT_T1A8CD620A7896D5F216E4114BBC8EE828552A189_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecT233FieldElement
struct  SecT233FieldElement_t1A8CD620A7896D5F216E4114BBC8EE828552A189  : public ECFieldElement_tE2FF8892A60D0ACBC3FB665E41B0DEAA0308514C
{
public:
	// System.UInt64[] Org.BouncyCastle.Math.EC.Custom.Sec.SecT233FieldElement::x
	UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* ___x_0;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(SecT233FieldElement_t1A8CD620A7896D5F216E4114BBC8EE828552A189, ___x_0)); }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* get_x_0() const { return ___x_0; }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4** get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* value)
	{
		___x_0 = value;
		Il2CppCodeGenWriteBarrier((&___x_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT233FIELDELEMENT_T1A8CD620A7896D5F216E4114BBC8EE828552A189_H
#ifndef SECT239FIELDELEMENT_T1D0B47BC6465C2CD3CCA3D41F4F1FAB703E72B45_H
#define SECT239FIELDELEMENT_T1D0B47BC6465C2CD3CCA3D41F4F1FAB703E72B45_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecT239FieldElement
struct  SecT239FieldElement_t1D0B47BC6465C2CD3CCA3D41F4F1FAB703E72B45  : public ECFieldElement_tE2FF8892A60D0ACBC3FB665E41B0DEAA0308514C
{
public:
	// System.UInt64[] Org.BouncyCastle.Math.EC.Custom.Sec.SecT239FieldElement::x
	UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* ___x_0;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(SecT239FieldElement_t1D0B47BC6465C2CD3CCA3D41F4F1FAB703E72B45, ___x_0)); }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* get_x_0() const { return ___x_0; }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4** get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* value)
	{
		___x_0 = value;
		Il2CppCodeGenWriteBarrier((&___x_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT239FIELDELEMENT_T1D0B47BC6465C2CD3CCA3D41F4F1FAB703E72B45_H
#ifndef SECT283FIELDELEMENT_T2DB52791BCEC9E0D22438D398608B51921422692_H
#define SECT283FIELDELEMENT_T2DB52791BCEC9E0D22438D398608B51921422692_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecT283FieldElement
struct  SecT283FieldElement_t2DB52791BCEC9E0D22438D398608B51921422692  : public ECFieldElement_tE2FF8892A60D0ACBC3FB665E41B0DEAA0308514C
{
public:
	// System.UInt64[] Org.BouncyCastle.Math.EC.Custom.Sec.SecT283FieldElement::x
	UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* ___x_0;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(SecT283FieldElement_t2DB52791BCEC9E0D22438D398608B51921422692, ___x_0)); }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* get_x_0() const { return ___x_0; }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4** get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* value)
	{
		___x_0 = value;
		Il2CppCodeGenWriteBarrier((&___x_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT283FIELDELEMENT_T2DB52791BCEC9E0D22438D398608B51921422692_H
#ifndef SECT409FIELDELEMENT_T771A26E7C9D081C6A75B4D13C01CA2713AAC4CBB_H
#define SECT409FIELDELEMENT_T771A26E7C9D081C6A75B4D13C01CA2713AAC4CBB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecT409FieldElement
struct  SecT409FieldElement_t771A26E7C9D081C6A75B4D13C01CA2713AAC4CBB  : public ECFieldElement_tE2FF8892A60D0ACBC3FB665E41B0DEAA0308514C
{
public:
	// System.UInt64[] Org.BouncyCastle.Math.EC.Custom.Sec.SecT409FieldElement::x
	UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* ___x_0;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(SecT409FieldElement_t771A26E7C9D081C6A75B4D13C01CA2713AAC4CBB, ___x_0)); }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* get_x_0() const { return ___x_0; }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4** get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* value)
	{
		___x_0 = value;
		Il2CppCodeGenWriteBarrier((&___x_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT409FIELDELEMENT_T771A26E7C9D081C6A75B4D13C01CA2713AAC4CBB_H
#ifndef SECT571FIELDELEMENT_T9EF7730630D1449DD2009986E22E25491864423B_H
#define SECT571FIELDELEMENT_T9EF7730630D1449DD2009986E22E25491864423B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecT571FieldElement
struct  SecT571FieldElement_t9EF7730630D1449DD2009986E22E25491864423B  : public ECFieldElement_tE2FF8892A60D0ACBC3FB665E41B0DEAA0308514C
{
public:
	// System.UInt64[] Org.BouncyCastle.Math.EC.Custom.Sec.SecT571FieldElement::x
	UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* ___x_0;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(SecT571FieldElement_t9EF7730630D1449DD2009986E22E25491864423B, ___x_0)); }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* get_x_0() const { return ___x_0; }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4** get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* value)
	{
		___x_0 = value;
		Il2CppCodeGenWriteBarrier((&___x_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT571FIELDELEMENT_T9EF7730630D1449DD2009986E22E25491864423B_H
#ifndef ECPOINTBASE_T7EE8D65EFE4A2748B20490039B5F88DFD8D8BF73_H
#define ECPOINTBASE_T7EE8D65EFE4A2748B20490039B5F88DFD8D8BF73_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.ECPointBase
struct  ECPointBase_t7EE8D65EFE4A2748B20490039B5F88DFD8D8BF73  : public ECPoint_tCC0C4447DEFF70FC9D68F5149ECCCC0533CE4399
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECPOINTBASE_T7EE8D65EFE4A2748B20490039B5F88DFD8D8BF73_H
#ifndef FIXEDPOINTCOMBMULTIPLIER_T8E62E787A7ACE99B8BA58FEB7E421E4F174A7218_H
#define FIXEDPOINTCOMBMULTIPLIER_T8E62E787A7ACE99B8BA58FEB7E421E4F174A7218_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Multiplier.FixedPointCombMultiplier
struct  FixedPointCombMultiplier_t8E62E787A7ACE99B8BA58FEB7E421E4F174A7218  : public AbstractECMultiplier_t53B6ADC66D5D126840E4ADFF2C4B048592CB16D3
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIXEDPOINTCOMBMULTIPLIER_T8E62E787A7ACE99B8BA58FEB7E421E4F174A7218_H
#ifndef GLVMULTIPLIER_T007132D5C7C548A7F6297CAA0330256A278E0A7C_H
#define GLVMULTIPLIER_T007132D5C7C548A7F6297CAA0330256A278E0A7C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Multiplier.GlvMultiplier
struct  GlvMultiplier_t007132D5C7C548A7F6297CAA0330256A278E0A7C  : public AbstractECMultiplier_t53B6ADC66D5D126840E4ADFF2C4B048592CB16D3
{
public:
	// Org.BouncyCastle.Math.EC.ECCurve Org.BouncyCastle.Math.EC.Multiplier.GlvMultiplier::curve
	ECCurve_t08AD8E722914B0B03C49C2D27B79B1D4E4770D95 * ___curve_0;
	// Org.BouncyCastle.Math.EC.Endo.GlvEndomorphism Org.BouncyCastle.Math.EC.Multiplier.GlvMultiplier::glvEndomorphism
	RuntimeObject* ___glvEndomorphism_1;

public:
	inline static int32_t get_offset_of_curve_0() { return static_cast<int32_t>(offsetof(GlvMultiplier_t007132D5C7C548A7F6297CAA0330256A278E0A7C, ___curve_0)); }
	inline ECCurve_t08AD8E722914B0B03C49C2D27B79B1D4E4770D95 * get_curve_0() const { return ___curve_0; }
	inline ECCurve_t08AD8E722914B0B03C49C2D27B79B1D4E4770D95 ** get_address_of_curve_0() { return &___curve_0; }
	inline void set_curve_0(ECCurve_t08AD8E722914B0B03C49C2D27B79B1D4E4770D95 * value)
	{
		___curve_0 = value;
		Il2CppCodeGenWriteBarrier((&___curve_0), value);
	}

	inline static int32_t get_offset_of_glvEndomorphism_1() { return static_cast<int32_t>(offsetof(GlvMultiplier_t007132D5C7C548A7F6297CAA0330256A278E0A7C, ___glvEndomorphism_1)); }
	inline RuntimeObject* get_glvEndomorphism_1() const { return ___glvEndomorphism_1; }
	inline RuntimeObject** get_address_of_glvEndomorphism_1() { return &___glvEndomorphism_1; }
	inline void set_glvEndomorphism_1(RuntimeObject* value)
	{
		___glvEndomorphism_1 = value;
		Il2CppCodeGenWriteBarrier((&___glvEndomorphism_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLVMULTIPLIER_T007132D5C7C548A7F6297CAA0330256A278E0A7C_H
#ifndef WNAFL2RMULTIPLIER_T2F3A759650B75812D286727F7E1DBA95430D3B2A_H
#define WNAFL2RMULTIPLIER_T2F3A759650B75812D286727F7E1DBA95430D3B2A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Multiplier.WNafL2RMultiplier
struct  WNafL2RMultiplier_t2F3A759650B75812D286727F7E1DBA95430D3B2A  : public AbstractECMultiplier_t53B6ADC66D5D126840E4ADFF2C4B048592CB16D3
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WNAFL2RMULTIPLIER_T2F3A759650B75812D286727F7E1DBA95430D3B2A_H
#ifndef WTAUNAFMULTIPLIER_T299D29F97AD9E7726B59728F537703F061A457C1_H
#define WTAUNAFMULTIPLIER_T299D29F97AD9E7726B59728F537703F061A457C1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Multiplier.WTauNafMultiplier
struct  WTauNafMultiplier_t299D29F97AD9E7726B59728F537703F061A457C1  : public AbstractECMultiplier_t53B6ADC66D5D126840E4ADFF2C4B048592CB16D3
{
public:

public:
};

struct WTauNafMultiplier_t299D29F97AD9E7726B59728F537703F061A457C1_StaticFields
{
public:
	// System.String Org.BouncyCastle.Math.EC.Multiplier.WTauNafMultiplier::PRECOMP_NAME
	String_t* ___PRECOMP_NAME_0;

public:
	inline static int32_t get_offset_of_PRECOMP_NAME_0() { return static_cast<int32_t>(offsetof(WTauNafMultiplier_t299D29F97AD9E7726B59728F537703F061A457C1_StaticFields, ___PRECOMP_NAME_0)); }
	inline String_t* get_PRECOMP_NAME_0() const { return ___PRECOMP_NAME_0; }
	inline String_t** get_address_of_PRECOMP_NAME_0() { return &___PRECOMP_NAME_0; }
	inline void set_PRECOMP_NAME_0(String_t* value)
	{
		___PRECOMP_NAME_0 = value;
		Il2CppCodeGenWriteBarrier((&___PRECOMP_NAME_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WTAUNAFMULTIPLIER_T299D29F97AD9E7726B59728F537703F061A457C1_H
#ifndef ABSTRACTF2MPOINT_T31951E0CF9009420711A3FF51DE270702A830FAD_H
#define ABSTRACTF2MPOINT_T31951E0CF9009420711A3FF51DE270702A830FAD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.AbstractF2mPoint
struct  AbstractF2mPoint_t31951E0CF9009420711A3FF51DE270702A830FAD  : public ECPointBase_t7EE8D65EFE4A2748B20490039B5F88DFD8D8BF73
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTF2MPOINT_T31951E0CF9009420711A3FF51DE270702A830FAD_H
#ifndef ABSTRACTFPPOINT_T3AC6C98A238F7A4B7FFAE487A33FCB48BCE6C0F7_H
#define ABSTRACTFPPOINT_T3AC6C98A238F7A4B7FFAE487A33FCB48BCE6C0F7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.AbstractFpPoint
struct  AbstractFpPoint_t3AC6C98A238F7A4B7FFAE487A33FCB48BCE6C0F7  : public ECPointBase_t7EE8D65EFE4A2748B20490039B5F88DFD8D8BF73
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTFPPOINT_T3AC6C98A238F7A4B7FFAE487A33FCB48BCE6C0F7_H
#ifndef SECP256K1CURVE_TC57EA0CD3D7A7017477D984223AB239D1CC733D3_H
#define SECP256K1CURVE_TC57EA0CD3D7A7017477D984223AB239D1CC733D3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecP256K1Curve
struct  SecP256K1Curve_tC57EA0CD3D7A7017477D984223AB239D1CC733D3  : public AbstractFpCurve_tC851390204D76E2F3E8CBDC56187D3375F05FF75
{
public:
	// Org.BouncyCastle.Math.EC.Custom.Sec.SecP256K1Point Org.BouncyCastle.Math.EC.Custom.Sec.SecP256K1Curve::m_infinity
	SecP256K1Point_t73B95080B3D33CB70644A9BFBBA68EA06B821FA7 * ___m_infinity_9;

public:
	inline static int32_t get_offset_of_m_infinity_9() { return static_cast<int32_t>(offsetof(SecP256K1Curve_tC57EA0CD3D7A7017477D984223AB239D1CC733D3, ___m_infinity_9)); }
	inline SecP256K1Point_t73B95080B3D33CB70644A9BFBBA68EA06B821FA7 * get_m_infinity_9() const { return ___m_infinity_9; }
	inline SecP256K1Point_t73B95080B3D33CB70644A9BFBBA68EA06B821FA7 ** get_address_of_m_infinity_9() { return &___m_infinity_9; }
	inline void set_m_infinity_9(SecP256K1Point_t73B95080B3D33CB70644A9BFBBA68EA06B821FA7 * value)
	{
		___m_infinity_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_infinity_9), value);
	}
};

struct SecP256K1Curve_tC57EA0CD3D7A7017477D984223AB239D1CC733D3_StaticFields
{
public:
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Math.EC.Custom.Sec.SecP256K1Curve::q
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___q_8;

public:
	inline static int32_t get_offset_of_q_8() { return static_cast<int32_t>(offsetof(SecP256K1Curve_tC57EA0CD3D7A7017477D984223AB239D1CC733D3_StaticFields, ___q_8)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_q_8() const { return ___q_8; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_q_8() { return &___q_8; }
	inline void set_q_8(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___q_8 = value;
		Il2CppCodeGenWriteBarrier((&___q_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP256K1CURVE_TC57EA0CD3D7A7017477D984223AB239D1CC733D3_H
#ifndef SECP256R1CURVE_T541948E2F0D1D5AD75E1FD0ACBB3EBD301AC2C30_H
#define SECP256R1CURVE_T541948E2F0D1D5AD75E1FD0ACBB3EBD301AC2C30_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecP256R1Curve
struct  SecP256R1Curve_t541948E2F0D1D5AD75E1FD0ACBB3EBD301AC2C30  : public AbstractFpCurve_tC851390204D76E2F3E8CBDC56187D3375F05FF75
{
public:
	// Org.BouncyCastle.Math.EC.Custom.Sec.SecP256R1Point Org.BouncyCastle.Math.EC.Custom.Sec.SecP256R1Curve::m_infinity
	SecP256R1Point_t295A7951697911EDEDBEBBF5A781E9D3071E086C * ___m_infinity_9;

public:
	inline static int32_t get_offset_of_m_infinity_9() { return static_cast<int32_t>(offsetof(SecP256R1Curve_t541948E2F0D1D5AD75E1FD0ACBB3EBD301AC2C30, ___m_infinity_9)); }
	inline SecP256R1Point_t295A7951697911EDEDBEBBF5A781E9D3071E086C * get_m_infinity_9() const { return ___m_infinity_9; }
	inline SecP256R1Point_t295A7951697911EDEDBEBBF5A781E9D3071E086C ** get_address_of_m_infinity_9() { return &___m_infinity_9; }
	inline void set_m_infinity_9(SecP256R1Point_t295A7951697911EDEDBEBBF5A781E9D3071E086C * value)
	{
		___m_infinity_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_infinity_9), value);
	}
};

struct SecP256R1Curve_t541948E2F0D1D5AD75E1FD0ACBB3EBD301AC2C30_StaticFields
{
public:
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Math.EC.Custom.Sec.SecP256R1Curve::q
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___q_8;

public:
	inline static int32_t get_offset_of_q_8() { return static_cast<int32_t>(offsetof(SecP256R1Curve_t541948E2F0D1D5AD75E1FD0ACBB3EBD301AC2C30_StaticFields, ___q_8)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_q_8() const { return ___q_8; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_q_8() { return &___q_8; }
	inline void set_q_8(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___q_8 = value;
		Il2CppCodeGenWriteBarrier((&___q_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP256R1CURVE_T541948E2F0D1D5AD75E1FD0ACBB3EBD301AC2C30_H
#ifndef SECP384R1CURVE_TBFE2A712929B52D32E2DDD31B3E38CC85BD25901_H
#define SECP384R1CURVE_TBFE2A712929B52D32E2DDD31B3E38CC85BD25901_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecP384R1Curve
struct  SecP384R1Curve_tBFE2A712929B52D32E2DDD31B3E38CC85BD25901  : public AbstractFpCurve_tC851390204D76E2F3E8CBDC56187D3375F05FF75
{
public:
	// Org.BouncyCastle.Math.EC.Custom.Sec.SecP384R1Point Org.BouncyCastle.Math.EC.Custom.Sec.SecP384R1Curve::m_infinity
	SecP384R1Point_tA00FA4528D94813D8BAE66A3EF5E79F07BD62D29 * ___m_infinity_9;

public:
	inline static int32_t get_offset_of_m_infinity_9() { return static_cast<int32_t>(offsetof(SecP384R1Curve_tBFE2A712929B52D32E2DDD31B3E38CC85BD25901, ___m_infinity_9)); }
	inline SecP384R1Point_tA00FA4528D94813D8BAE66A3EF5E79F07BD62D29 * get_m_infinity_9() const { return ___m_infinity_9; }
	inline SecP384R1Point_tA00FA4528D94813D8BAE66A3EF5E79F07BD62D29 ** get_address_of_m_infinity_9() { return &___m_infinity_9; }
	inline void set_m_infinity_9(SecP384R1Point_tA00FA4528D94813D8BAE66A3EF5E79F07BD62D29 * value)
	{
		___m_infinity_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_infinity_9), value);
	}
};

struct SecP384R1Curve_tBFE2A712929B52D32E2DDD31B3E38CC85BD25901_StaticFields
{
public:
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Math.EC.Custom.Sec.SecP384R1Curve::q
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___q_8;

public:
	inline static int32_t get_offset_of_q_8() { return static_cast<int32_t>(offsetof(SecP384R1Curve_tBFE2A712929B52D32E2DDD31B3E38CC85BD25901_StaticFields, ___q_8)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_q_8() const { return ___q_8; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_q_8() { return &___q_8; }
	inline void set_q_8(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___q_8 = value;
		Il2CppCodeGenWriteBarrier((&___q_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP384R1CURVE_TBFE2A712929B52D32E2DDD31B3E38CC85BD25901_H
#ifndef SECP521R1CURVE_T1644010879D55FA816214B2D767941F29AA354DF_H
#define SECP521R1CURVE_T1644010879D55FA816214B2D767941F29AA354DF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecP521R1Curve
struct  SecP521R1Curve_t1644010879D55FA816214B2D767941F29AA354DF  : public AbstractFpCurve_tC851390204D76E2F3E8CBDC56187D3375F05FF75
{
public:
	// Org.BouncyCastle.Math.EC.Custom.Sec.SecP521R1Point Org.BouncyCastle.Math.EC.Custom.Sec.SecP521R1Curve::m_infinity
	SecP521R1Point_tECEE1E8F65AA393C27ACFC30A38A1A57D092C117 * ___m_infinity_9;

public:
	inline static int32_t get_offset_of_m_infinity_9() { return static_cast<int32_t>(offsetof(SecP521R1Curve_t1644010879D55FA816214B2D767941F29AA354DF, ___m_infinity_9)); }
	inline SecP521R1Point_tECEE1E8F65AA393C27ACFC30A38A1A57D092C117 * get_m_infinity_9() const { return ___m_infinity_9; }
	inline SecP521R1Point_tECEE1E8F65AA393C27ACFC30A38A1A57D092C117 ** get_address_of_m_infinity_9() { return &___m_infinity_9; }
	inline void set_m_infinity_9(SecP521R1Point_tECEE1E8F65AA393C27ACFC30A38A1A57D092C117 * value)
	{
		___m_infinity_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_infinity_9), value);
	}
};

struct SecP521R1Curve_t1644010879D55FA816214B2D767941F29AA354DF_StaticFields
{
public:
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Math.EC.Custom.Sec.SecP521R1Curve::q
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___q_8;

public:
	inline static int32_t get_offset_of_q_8() { return static_cast<int32_t>(offsetof(SecP521R1Curve_t1644010879D55FA816214B2D767941F29AA354DF_StaticFields, ___q_8)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_q_8() const { return ___q_8; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_q_8() { return &___q_8; }
	inline void set_q_8(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___q_8 = value;
		Il2CppCodeGenWriteBarrier((&___q_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP521R1CURVE_T1644010879D55FA816214B2D767941F29AA354DF_H
#ifndef SECT113R1CURVE_T95C1F4B08F7AE366397933983A93505A6190B758_H
#define SECT113R1CURVE_T95C1F4B08F7AE366397933983A93505A6190B758_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecT113R1Curve
struct  SecT113R1Curve_t95C1F4B08F7AE366397933983A93505A6190B758  : public AbstractF2mCurve_t4AA98C03B9F426713CE47E921AA07B9623F34391
{
public:
	// Org.BouncyCastle.Math.EC.Custom.Sec.SecT113R1Point Org.BouncyCastle.Math.EC.Custom.Sec.SecT113R1Curve::m_infinity
	SecT113R1Point_t11FE9563DDC91F15F35658317310EDF469EFFC55 * ___m_infinity_9;

public:
	inline static int32_t get_offset_of_m_infinity_9() { return static_cast<int32_t>(offsetof(SecT113R1Curve_t95C1F4B08F7AE366397933983A93505A6190B758, ___m_infinity_9)); }
	inline SecT113R1Point_t11FE9563DDC91F15F35658317310EDF469EFFC55 * get_m_infinity_9() const { return ___m_infinity_9; }
	inline SecT113R1Point_t11FE9563DDC91F15F35658317310EDF469EFFC55 ** get_address_of_m_infinity_9() { return &___m_infinity_9; }
	inline void set_m_infinity_9(SecT113R1Point_t11FE9563DDC91F15F35658317310EDF469EFFC55 * value)
	{
		___m_infinity_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_infinity_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT113R1CURVE_T95C1F4B08F7AE366397933983A93505A6190B758_H
#ifndef SECT113R2CURVE_T05698035683EF81A8DC2EFE83F44645933F4B8DC_H
#define SECT113R2CURVE_T05698035683EF81A8DC2EFE83F44645933F4B8DC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecT113R2Curve
struct  SecT113R2Curve_t05698035683EF81A8DC2EFE83F44645933F4B8DC  : public AbstractF2mCurve_t4AA98C03B9F426713CE47E921AA07B9623F34391
{
public:
	// Org.BouncyCastle.Math.EC.Custom.Sec.SecT113R2Point Org.BouncyCastle.Math.EC.Custom.Sec.SecT113R2Curve::m_infinity
	SecT113R2Point_t815197D019F8BBF408E4B3829239619835C98EF6 * ___m_infinity_9;

public:
	inline static int32_t get_offset_of_m_infinity_9() { return static_cast<int32_t>(offsetof(SecT113R2Curve_t05698035683EF81A8DC2EFE83F44645933F4B8DC, ___m_infinity_9)); }
	inline SecT113R2Point_t815197D019F8BBF408E4B3829239619835C98EF6 * get_m_infinity_9() const { return ___m_infinity_9; }
	inline SecT113R2Point_t815197D019F8BBF408E4B3829239619835C98EF6 ** get_address_of_m_infinity_9() { return &___m_infinity_9; }
	inline void set_m_infinity_9(SecT113R2Point_t815197D019F8BBF408E4B3829239619835C98EF6 * value)
	{
		___m_infinity_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_infinity_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT113R2CURVE_T05698035683EF81A8DC2EFE83F44645933F4B8DC_H
#ifndef SECT131R1CURVE_TF04D847B53F6892A71114EB769D9F1A2ADFDC350_H
#define SECT131R1CURVE_TF04D847B53F6892A71114EB769D9F1A2ADFDC350_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecT131R1Curve
struct  SecT131R1Curve_tF04D847B53F6892A71114EB769D9F1A2ADFDC350  : public AbstractF2mCurve_t4AA98C03B9F426713CE47E921AA07B9623F34391
{
public:
	// Org.BouncyCastle.Math.EC.Custom.Sec.SecT131R1Point Org.BouncyCastle.Math.EC.Custom.Sec.SecT131R1Curve::m_infinity
	SecT131R1Point_t650635A631EAF1EE11C6B446D0FB58D417AD74D7 * ___m_infinity_9;

public:
	inline static int32_t get_offset_of_m_infinity_9() { return static_cast<int32_t>(offsetof(SecT131R1Curve_tF04D847B53F6892A71114EB769D9F1A2ADFDC350, ___m_infinity_9)); }
	inline SecT131R1Point_t650635A631EAF1EE11C6B446D0FB58D417AD74D7 * get_m_infinity_9() const { return ___m_infinity_9; }
	inline SecT131R1Point_t650635A631EAF1EE11C6B446D0FB58D417AD74D7 ** get_address_of_m_infinity_9() { return &___m_infinity_9; }
	inline void set_m_infinity_9(SecT131R1Point_t650635A631EAF1EE11C6B446D0FB58D417AD74D7 * value)
	{
		___m_infinity_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_infinity_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT131R1CURVE_TF04D847B53F6892A71114EB769D9F1A2ADFDC350_H
#ifndef SECT131R2CURVE_TC4A0C0E7D0E0AA8D2E2634F73281E30242CAA100_H
#define SECT131R2CURVE_TC4A0C0E7D0E0AA8D2E2634F73281E30242CAA100_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecT131R2Curve
struct  SecT131R2Curve_tC4A0C0E7D0E0AA8D2E2634F73281E30242CAA100  : public AbstractF2mCurve_t4AA98C03B9F426713CE47E921AA07B9623F34391
{
public:
	// Org.BouncyCastle.Math.EC.Custom.Sec.SecT131R2Point Org.BouncyCastle.Math.EC.Custom.Sec.SecT131R2Curve::m_infinity
	SecT131R2Point_tA6875915938E95528D2C942965CA3AB629785851 * ___m_infinity_9;

public:
	inline static int32_t get_offset_of_m_infinity_9() { return static_cast<int32_t>(offsetof(SecT131R2Curve_tC4A0C0E7D0E0AA8D2E2634F73281E30242CAA100, ___m_infinity_9)); }
	inline SecT131R2Point_tA6875915938E95528D2C942965CA3AB629785851 * get_m_infinity_9() const { return ___m_infinity_9; }
	inline SecT131R2Point_tA6875915938E95528D2C942965CA3AB629785851 ** get_address_of_m_infinity_9() { return &___m_infinity_9; }
	inline void set_m_infinity_9(SecT131R2Point_tA6875915938E95528D2C942965CA3AB629785851 * value)
	{
		___m_infinity_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_infinity_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT131R2CURVE_TC4A0C0E7D0E0AA8D2E2634F73281E30242CAA100_H
#ifndef SECT163K1CURVE_TBB0FD03C5F274ABE7B125B9C4B82FE88618115AA_H
#define SECT163K1CURVE_TBB0FD03C5F274ABE7B125B9C4B82FE88618115AA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecT163K1Curve
struct  SecT163K1Curve_tBB0FD03C5F274ABE7B125B9C4B82FE88618115AA  : public AbstractF2mCurve_t4AA98C03B9F426713CE47E921AA07B9623F34391
{
public:
	// Org.BouncyCastle.Math.EC.Custom.Sec.SecT163K1Point Org.BouncyCastle.Math.EC.Custom.Sec.SecT163K1Curve::m_infinity
	SecT163K1Point_t6A7BAF7AF848A31595D0C59C983E50D6E8C10EDC * ___m_infinity_9;

public:
	inline static int32_t get_offset_of_m_infinity_9() { return static_cast<int32_t>(offsetof(SecT163K1Curve_tBB0FD03C5F274ABE7B125B9C4B82FE88618115AA, ___m_infinity_9)); }
	inline SecT163K1Point_t6A7BAF7AF848A31595D0C59C983E50D6E8C10EDC * get_m_infinity_9() const { return ___m_infinity_9; }
	inline SecT163K1Point_t6A7BAF7AF848A31595D0C59C983E50D6E8C10EDC ** get_address_of_m_infinity_9() { return &___m_infinity_9; }
	inline void set_m_infinity_9(SecT163K1Point_t6A7BAF7AF848A31595D0C59C983E50D6E8C10EDC * value)
	{
		___m_infinity_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_infinity_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT163K1CURVE_TBB0FD03C5F274ABE7B125B9C4B82FE88618115AA_H
#ifndef SECT163R1CURVE_TD96CDE4871D9D9F30CF8DDEF455A2A777CD98713_H
#define SECT163R1CURVE_TD96CDE4871D9D9F30CF8DDEF455A2A777CD98713_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecT163R1Curve
struct  SecT163R1Curve_tD96CDE4871D9D9F30CF8DDEF455A2A777CD98713  : public AbstractF2mCurve_t4AA98C03B9F426713CE47E921AA07B9623F34391
{
public:
	// Org.BouncyCastle.Math.EC.Custom.Sec.SecT163R1Point Org.BouncyCastle.Math.EC.Custom.Sec.SecT163R1Curve::m_infinity
	SecT163R1Point_t337935F97D827A290DA27C9833B9C31B8672E57E * ___m_infinity_9;

public:
	inline static int32_t get_offset_of_m_infinity_9() { return static_cast<int32_t>(offsetof(SecT163R1Curve_tD96CDE4871D9D9F30CF8DDEF455A2A777CD98713, ___m_infinity_9)); }
	inline SecT163R1Point_t337935F97D827A290DA27C9833B9C31B8672E57E * get_m_infinity_9() const { return ___m_infinity_9; }
	inline SecT163R1Point_t337935F97D827A290DA27C9833B9C31B8672E57E ** get_address_of_m_infinity_9() { return &___m_infinity_9; }
	inline void set_m_infinity_9(SecT163R1Point_t337935F97D827A290DA27C9833B9C31B8672E57E * value)
	{
		___m_infinity_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_infinity_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT163R1CURVE_TD96CDE4871D9D9F30CF8DDEF455A2A777CD98713_H
#ifndef SECT163R2CURVE_T445D90CA284E53218D4B4B7A863588456BE00F5C_H
#define SECT163R2CURVE_T445D90CA284E53218D4B4B7A863588456BE00F5C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecT163R2Curve
struct  SecT163R2Curve_t445D90CA284E53218D4B4B7A863588456BE00F5C  : public AbstractF2mCurve_t4AA98C03B9F426713CE47E921AA07B9623F34391
{
public:
	// Org.BouncyCastle.Math.EC.Custom.Sec.SecT163R2Point Org.BouncyCastle.Math.EC.Custom.Sec.SecT163R2Curve::m_infinity
	SecT163R2Point_t48E9B3A96F8B4DD71022478E2AE63A7015F2F682 * ___m_infinity_9;

public:
	inline static int32_t get_offset_of_m_infinity_9() { return static_cast<int32_t>(offsetof(SecT163R2Curve_t445D90CA284E53218D4B4B7A863588456BE00F5C, ___m_infinity_9)); }
	inline SecT163R2Point_t48E9B3A96F8B4DD71022478E2AE63A7015F2F682 * get_m_infinity_9() const { return ___m_infinity_9; }
	inline SecT163R2Point_t48E9B3A96F8B4DD71022478E2AE63A7015F2F682 ** get_address_of_m_infinity_9() { return &___m_infinity_9; }
	inline void set_m_infinity_9(SecT163R2Point_t48E9B3A96F8B4DD71022478E2AE63A7015F2F682 * value)
	{
		___m_infinity_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_infinity_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT163R2CURVE_T445D90CA284E53218D4B4B7A863588456BE00F5C_H
#ifndef SECT193R1CURVE_TD4EB61EBFFFC0F19BDB1CE67D004F031F8A15F02_H
#define SECT193R1CURVE_TD4EB61EBFFFC0F19BDB1CE67D004F031F8A15F02_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecT193R1Curve
struct  SecT193R1Curve_tD4EB61EBFFFC0F19BDB1CE67D004F031F8A15F02  : public AbstractF2mCurve_t4AA98C03B9F426713CE47E921AA07B9623F34391
{
public:
	// Org.BouncyCastle.Math.EC.Custom.Sec.SecT193R1Point Org.BouncyCastle.Math.EC.Custom.Sec.SecT193R1Curve::m_infinity
	SecT193R1Point_t5E2007F0FA3823BF8A80DCDB359AA04BC2DD43E4 * ___m_infinity_9;

public:
	inline static int32_t get_offset_of_m_infinity_9() { return static_cast<int32_t>(offsetof(SecT193R1Curve_tD4EB61EBFFFC0F19BDB1CE67D004F031F8A15F02, ___m_infinity_9)); }
	inline SecT193R1Point_t5E2007F0FA3823BF8A80DCDB359AA04BC2DD43E4 * get_m_infinity_9() const { return ___m_infinity_9; }
	inline SecT193R1Point_t5E2007F0FA3823BF8A80DCDB359AA04BC2DD43E4 ** get_address_of_m_infinity_9() { return &___m_infinity_9; }
	inline void set_m_infinity_9(SecT193R1Point_t5E2007F0FA3823BF8A80DCDB359AA04BC2DD43E4 * value)
	{
		___m_infinity_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_infinity_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT193R1CURVE_TD4EB61EBFFFC0F19BDB1CE67D004F031F8A15F02_H
#ifndef SECT193R2CURVE_T6A733FF6BB077F5C6286746398C681152C794BDD_H
#define SECT193R2CURVE_T6A733FF6BB077F5C6286746398C681152C794BDD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecT193R2Curve
struct  SecT193R2Curve_t6A733FF6BB077F5C6286746398C681152C794BDD  : public AbstractF2mCurve_t4AA98C03B9F426713CE47E921AA07B9623F34391
{
public:
	// Org.BouncyCastle.Math.EC.Custom.Sec.SecT193R2Point Org.BouncyCastle.Math.EC.Custom.Sec.SecT193R2Curve::m_infinity
	SecT193R2Point_tEF874435F9B32986838CDE770CECE8F6DB0711CB * ___m_infinity_9;

public:
	inline static int32_t get_offset_of_m_infinity_9() { return static_cast<int32_t>(offsetof(SecT193R2Curve_t6A733FF6BB077F5C6286746398C681152C794BDD, ___m_infinity_9)); }
	inline SecT193R2Point_tEF874435F9B32986838CDE770CECE8F6DB0711CB * get_m_infinity_9() const { return ___m_infinity_9; }
	inline SecT193R2Point_tEF874435F9B32986838CDE770CECE8F6DB0711CB ** get_address_of_m_infinity_9() { return &___m_infinity_9; }
	inline void set_m_infinity_9(SecT193R2Point_tEF874435F9B32986838CDE770CECE8F6DB0711CB * value)
	{
		___m_infinity_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_infinity_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT193R2CURVE_T6A733FF6BB077F5C6286746398C681152C794BDD_H
#ifndef SECT233K1CURVE_T2E0066860970733CDE7870C8E75D7654DC7A5C46_H
#define SECT233K1CURVE_T2E0066860970733CDE7870C8E75D7654DC7A5C46_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecT233K1Curve
struct  SecT233K1Curve_t2E0066860970733CDE7870C8E75D7654DC7A5C46  : public AbstractF2mCurve_t4AA98C03B9F426713CE47E921AA07B9623F34391
{
public:
	// Org.BouncyCastle.Math.EC.Custom.Sec.SecT233K1Point Org.BouncyCastle.Math.EC.Custom.Sec.SecT233K1Curve::m_infinity
	SecT233K1Point_t55AC6FAC7F0B890D2C66429AB21918B15EB034DA * ___m_infinity_9;

public:
	inline static int32_t get_offset_of_m_infinity_9() { return static_cast<int32_t>(offsetof(SecT233K1Curve_t2E0066860970733CDE7870C8E75D7654DC7A5C46, ___m_infinity_9)); }
	inline SecT233K1Point_t55AC6FAC7F0B890D2C66429AB21918B15EB034DA * get_m_infinity_9() const { return ___m_infinity_9; }
	inline SecT233K1Point_t55AC6FAC7F0B890D2C66429AB21918B15EB034DA ** get_address_of_m_infinity_9() { return &___m_infinity_9; }
	inline void set_m_infinity_9(SecT233K1Point_t55AC6FAC7F0B890D2C66429AB21918B15EB034DA * value)
	{
		___m_infinity_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_infinity_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT233K1CURVE_T2E0066860970733CDE7870C8E75D7654DC7A5C46_H
#ifndef SECT233R1CURVE_T496B52AEF7DB82F365410CB8363A4D6095FAFAA2_H
#define SECT233R1CURVE_T496B52AEF7DB82F365410CB8363A4D6095FAFAA2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecT233R1Curve
struct  SecT233R1Curve_t496B52AEF7DB82F365410CB8363A4D6095FAFAA2  : public AbstractF2mCurve_t4AA98C03B9F426713CE47E921AA07B9623F34391
{
public:
	// Org.BouncyCastle.Math.EC.Custom.Sec.SecT233R1Point Org.BouncyCastle.Math.EC.Custom.Sec.SecT233R1Curve::m_infinity
	SecT233R1Point_tA9DCCEB01CCB8ED6862B7B7605362C4C58B1DC76 * ___m_infinity_9;

public:
	inline static int32_t get_offset_of_m_infinity_9() { return static_cast<int32_t>(offsetof(SecT233R1Curve_t496B52AEF7DB82F365410CB8363A4D6095FAFAA2, ___m_infinity_9)); }
	inline SecT233R1Point_tA9DCCEB01CCB8ED6862B7B7605362C4C58B1DC76 * get_m_infinity_9() const { return ___m_infinity_9; }
	inline SecT233R1Point_tA9DCCEB01CCB8ED6862B7B7605362C4C58B1DC76 ** get_address_of_m_infinity_9() { return &___m_infinity_9; }
	inline void set_m_infinity_9(SecT233R1Point_tA9DCCEB01CCB8ED6862B7B7605362C4C58B1DC76 * value)
	{
		___m_infinity_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_infinity_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT233R1CURVE_T496B52AEF7DB82F365410CB8363A4D6095FAFAA2_H
#ifndef SECT239K1CURVE_T70B6A2853D985306A0F6E8930BDAC4724EA3E5F8_H
#define SECT239K1CURVE_T70B6A2853D985306A0F6E8930BDAC4724EA3E5F8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecT239K1Curve
struct  SecT239K1Curve_t70B6A2853D985306A0F6E8930BDAC4724EA3E5F8  : public AbstractF2mCurve_t4AA98C03B9F426713CE47E921AA07B9623F34391
{
public:
	// Org.BouncyCastle.Math.EC.Custom.Sec.SecT239K1Point Org.BouncyCastle.Math.EC.Custom.Sec.SecT239K1Curve::m_infinity
	SecT239K1Point_tA128635B479380D6A48C9690715188870A470EA0 * ___m_infinity_9;

public:
	inline static int32_t get_offset_of_m_infinity_9() { return static_cast<int32_t>(offsetof(SecT239K1Curve_t70B6A2853D985306A0F6E8930BDAC4724EA3E5F8, ___m_infinity_9)); }
	inline SecT239K1Point_tA128635B479380D6A48C9690715188870A470EA0 * get_m_infinity_9() const { return ___m_infinity_9; }
	inline SecT239K1Point_tA128635B479380D6A48C9690715188870A470EA0 ** get_address_of_m_infinity_9() { return &___m_infinity_9; }
	inline void set_m_infinity_9(SecT239K1Point_tA128635B479380D6A48C9690715188870A470EA0 * value)
	{
		___m_infinity_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_infinity_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT239K1CURVE_T70B6A2853D985306A0F6E8930BDAC4724EA3E5F8_H
#ifndef SECT283K1CURVE_T6C724D4AF8BFD3E2A636F76CA392F9ACC7865FAA_H
#define SECT283K1CURVE_T6C724D4AF8BFD3E2A636F76CA392F9ACC7865FAA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecT283K1Curve
struct  SecT283K1Curve_t6C724D4AF8BFD3E2A636F76CA392F9ACC7865FAA  : public AbstractF2mCurve_t4AA98C03B9F426713CE47E921AA07B9623F34391
{
public:
	// Org.BouncyCastle.Math.EC.Custom.Sec.SecT283K1Point Org.BouncyCastle.Math.EC.Custom.Sec.SecT283K1Curve::m_infinity
	SecT283K1Point_tE59E5378A535C6ED38C1A2AFC9CB0DC47A69D8F3 * ___m_infinity_9;

public:
	inline static int32_t get_offset_of_m_infinity_9() { return static_cast<int32_t>(offsetof(SecT283K1Curve_t6C724D4AF8BFD3E2A636F76CA392F9ACC7865FAA, ___m_infinity_9)); }
	inline SecT283K1Point_tE59E5378A535C6ED38C1A2AFC9CB0DC47A69D8F3 * get_m_infinity_9() const { return ___m_infinity_9; }
	inline SecT283K1Point_tE59E5378A535C6ED38C1A2AFC9CB0DC47A69D8F3 ** get_address_of_m_infinity_9() { return &___m_infinity_9; }
	inline void set_m_infinity_9(SecT283K1Point_tE59E5378A535C6ED38C1A2AFC9CB0DC47A69D8F3 * value)
	{
		___m_infinity_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_infinity_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT283K1CURVE_T6C724D4AF8BFD3E2A636F76CA392F9ACC7865FAA_H
#ifndef SECT283R1CURVE_T517AC3FBAE68F0145612CDDD8A45314F1FBDB7D9_H
#define SECT283R1CURVE_T517AC3FBAE68F0145612CDDD8A45314F1FBDB7D9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecT283R1Curve
struct  SecT283R1Curve_t517AC3FBAE68F0145612CDDD8A45314F1FBDB7D9  : public AbstractF2mCurve_t4AA98C03B9F426713CE47E921AA07B9623F34391
{
public:
	// Org.BouncyCastle.Math.EC.Custom.Sec.SecT283R1Point Org.BouncyCastle.Math.EC.Custom.Sec.SecT283R1Curve::m_infinity
	SecT283R1Point_tF68A0F98707126343F1D8D05DF7D69AA718C3A8B * ___m_infinity_9;

public:
	inline static int32_t get_offset_of_m_infinity_9() { return static_cast<int32_t>(offsetof(SecT283R1Curve_t517AC3FBAE68F0145612CDDD8A45314F1FBDB7D9, ___m_infinity_9)); }
	inline SecT283R1Point_tF68A0F98707126343F1D8D05DF7D69AA718C3A8B * get_m_infinity_9() const { return ___m_infinity_9; }
	inline SecT283R1Point_tF68A0F98707126343F1D8D05DF7D69AA718C3A8B ** get_address_of_m_infinity_9() { return &___m_infinity_9; }
	inline void set_m_infinity_9(SecT283R1Point_tF68A0F98707126343F1D8D05DF7D69AA718C3A8B * value)
	{
		___m_infinity_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_infinity_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT283R1CURVE_T517AC3FBAE68F0145612CDDD8A45314F1FBDB7D9_H
#ifndef SECT409K1CURVE_TE67797CACC543D1BE3DE27938FD9EAA3F52BE281_H
#define SECT409K1CURVE_TE67797CACC543D1BE3DE27938FD9EAA3F52BE281_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecT409K1Curve
struct  SecT409K1Curve_tE67797CACC543D1BE3DE27938FD9EAA3F52BE281  : public AbstractF2mCurve_t4AA98C03B9F426713CE47E921AA07B9623F34391
{
public:
	// Org.BouncyCastle.Math.EC.Custom.Sec.SecT409K1Point Org.BouncyCastle.Math.EC.Custom.Sec.SecT409K1Curve::m_infinity
	SecT409K1Point_t8BA8093464B27205D0D5D6431AC79B0963985791 * ___m_infinity_9;

public:
	inline static int32_t get_offset_of_m_infinity_9() { return static_cast<int32_t>(offsetof(SecT409K1Curve_tE67797CACC543D1BE3DE27938FD9EAA3F52BE281, ___m_infinity_9)); }
	inline SecT409K1Point_t8BA8093464B27205D0D5D6431AC79B0963985791 * get_m_infinity_9() const { return ___m_infinity_9; }
	inline SecT409K1Point_t8BA8093464B27205D0D5D6431AC79B0963985791 ** get_address_of_m_infinity_9() { return &___m_infinity_9; }
	inline void set_m_infinity_9(SecT409K1Point_t8BA8093464B27205D0D5D6431AC79B0963985791 * value)
	{
		___m_infinity_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_infinity_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT409K1CURVE_TE67797CACC543D1BE3DE27938FD9EAA3F52BE281_H
#ifndef SECT409R1CURVE_T5FBF26F8595DD21119C384EA7EC2DE8C940EA303_H
#define SECT409R1CURVE_T5FBF26F8595DD21119C384EA7EC2DE8C940EA303_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecT409R1Curve
struct  SecT409R1Curve_t5FBF26F8595DD21119C384EA7EC2DE8C940EA303  : public AbstractF2mCurve_t4AA98C03B9F426713CE47E921AA07B9623F34391
{
public:
	// Org.BouncyCastle.Math.EC.Custom.Sec.SecT409R1Point Org.BouncyCastle.Math.EC.Custom.Sec.SecT409R1Curve::m_infinity
	SecT409R1Point_tCE020433FAD15C841CD2C5EE9A07908B32062BBB * ___m_infinity_9;

public:
	inline static int32_t get_offset_of_m_infinity_9() { return static_cast<int32_t>(offsetof(SecT409R1Curve_t5FBF26F8595DD21119C384EA7EC2DE8C940EA303, ___m_infinity_9)); }
	inline SecT409R1Point_tCE020433FAD15C841CD2C5EE9A07908B32062BBB * get_m_infinity_9() const { return ___m_infinity_9; }
	inline SecT409R1Point_tCE020433FAD15C841CD2C5EE9A07908B32062BBB ** get_address_of_m_infinity_9() { return &___m_infinity_9; }
	inline void set_m_infinity_9(SecT409R1Point_tCE020433FAD15C841CD2C5EE9A07908B32062BBB * value)
	{
		___m_infinity_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_infinity_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT409R1CURVE_T5FBF26F8595DD21119C384EA7EC2DE8C940EA303_H
#ifndef SECT571K1CURVE_T5063698C3CD26A28FFE6D3B6F5A9BE0F1863DE5E_H
#define SECT571K1CURVE_T5063698C3CD26A28FFE6D3B6F5A9BE0F1863DE5E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecT571K1Curve
struct  SecT571K1Curve_t5063698C3CD26A28FFE6D3B6F5A9BE0F1863DE5E  : public AbstractF2mCurve_t4AA98C03B9F426713CE47E921AA07B9623F34391
{
public:
	// Org.BouncyCastle.Math.EC.Custom.Sec.SecT571K1Point Org.BouncyCastle.Math.EC.Custom.Sec.SecT571K1Curve::m_infinity
	SecT571K1Point_tC7B3CA5C02D4A361FC41724CA5DBF92648999946 * ___m_infinity_9;

public:
	inline static int32_t get_offset_of_m_infinity_9() { return static_cast<int32_t>(offsetof(SecT571K1Curve_t5063698C3CD26A28FFE6D3B6F5A9BE0F1863DE5E, ___m_infinity_9)); }
	inline SecT571K1Point_tC7B3CA5C02D4A361FC41724CA5DBF92648999946 * get_m_infinity_9() const { return ___m_infinity_9; }
	inline SecT571K1Point_tC7B3CA5C02D4A361FC41724CA5DBF92648999946 ** get_address_of_m_infinity_9() { return &___m_infinity_9; }
	inline void set_m_infinity_9(SecT571K1Point_tC7B3CA5C02D4A361FC41724CA5DBF92648999946 * value)
	{
		___m_infinity_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_infinity_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT571K1CURVE_T5063698C3CD26A28FFE6D3B6F5A9BE0F1863DE5E_H
#ifndef SECT571R1CURVE_TA50E4FD2F2511B362F86D0456CE538A44B3ADE78_H
#define SECT571R1CURVE_TA50E4FD2F2511B362F86D0456CE538A44B3ADE78_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecT571R1Curve
struct  SecT571R1Curve_tA50E4FD2F2511B362F86D0456CE538A44B3ADE78  : public AbstractF2mCurve_t4AA98C03B9F426713CE47E921AA07B9623F34391
{
public:
	// Org.BouncyCastle.Math.EC.Custom.Sec.SecT571R1Point Org.BouncyCastle.Math.EC.Custom.Sec.SecT571R1Curve::m_infinity
	SecT571R1Point_tDAED963E59F595895CD9B5F8A3B55079F5DE9FE7 * ___m_infinity_9;

public:
	inline static int32_t get_offset_of_m_infinity_9() { return static_cast<int32_t>(offsetof(SecT571R1Curve_tA50E4FD2F2511B362F86D0456CE538A44B3ADE78, ___m_infinity_9)); }
	inline SecT571R1Point_tDAED963E59F595895CD9B5F8A3B55079F5DE9FE7 * get_m_infinity_9() const { return ___m_infinity_9; }
	inline SecT571R1Point_tDAED963E59F595895CD9B5F8A3B55079F5DE9FE7 ** get_address_of_m_infinity_9() { return &___m_infinity_9; }
	inline void set_m_infinity_9(SecT571R1Point_tDAED963E59F595895CD9B5F8A3B55079F5DE9FE7 * value)
	{
		___m_infinity_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_infinity_9), value);
	}
};

struct SecT571R1Curve_tA50E4FD2F2511B362F86D0456CE538A44B3ADE78_StaticFields
{
public:
	// Org.BouncyCastle.Math.EC.Custom.Sec.SecT571FieldElement Org.BouncyCastle.Math.EC.Custom.Sec.SecT571R1Curve::SecT571R1_B
	SecT571FieldElement_t9EF7730630D1449DD2009986E22E25491864423B * ___SecT571R1_B_10;
	// Org.BouncyCastle.Math.EC.Custom.Sec.SecT571FieldElement Org.BouncyCastle.Math.EC.Custom.Sec.SecT571R1Curve::SecT571R1_B_SQRT
	SecT571FieldElement_t9EF7730630D1449DD2009986E22E25491864423B * ___SecT571R1_B_SQRT_11;

public:
	inline static int32_t get_offset_of_SecT571R1_B_10() { return static_cast<int32_t>(offsetof(SecT571R1Curve_tA50E4FD2F2511B362F86D0456CE538A44B3ADE78_StaticFields, ___SecT571R1_B_10)); }
	inline SecT571FieldElement_t9EF7730630D1449DD2009986E22E25491864423B * get_SecT571R1_B_10() const { return ___SecT571R1_B_10; }
	inline SecT571FieldElement_t9EF7730630D1449DD2009986E22E25491864423B ** get_address_of_SecT571R1_B_10() { return &___SecT571R1_B_10; }
	inline void set_SecT571R1_B_10(SecT571FieldElement_t9EF7730630D1449DD2009986E22E25491864423B * value)
	{
		___SecT571R1_B_10 = value;
		Il2CppCodeGenWriteBarrier((&___SecT571R1_B_10), value);
	}

	inline static int32_t get_offset_of_SecT571R1_B_SQRT_11() { return static_cast<int32_t>(offsetof(SecT571R1Curve_tA50E4FD2F2511B362F86D0456CE538A44B3ADE78_StaticFields, ___SecT571R1_B_SQRT_11)); }
	inline SecT571FieldElement_t9EF7730630D1449DD2009986E22E25491864423B * get_SecT571R1_B_SQRT_11() const { return ___SecT571R1_B_SQRT_11; }
	inline SecT571FieldElement_t9EF7730630D1449DD2009986E22E25491864423B ** get_address_of_SecT571R1_B_SQRT_11() { return &___SecT571R1_B_SQRT_11; }
	inline void set_SecT571R1_B_SQRT_11(SecT571FieldElement_t9EF7730630D1449DD2009986E22E25491864423B * value)
	{
		___SecT571R1_B_SQRT_11 = value;
		Il2CppCodeGenWriteBarrier((&___SecT571R1_B_SQRT_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT571R1CURVE_TA50E4FD2F2511B362F86D0456CE538A44B3ADE78_H
#ifndef SECP224R1POINT_T3DD809782D037568CA3A200FC33A8DA7390405CD_H
#define SECP224R1POINT_T3DD809782D037568CA3A200FC33A8DA7390405CD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecP224R1Point
struct  SecP224R1Point_t3DD809782D037568CA3A200FC33A8DA7390405CD  : public AbstractFpPoint_t3AC6C98A238F7A4B7FFAE487A33FCB48BCE6C0F7
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP224R1POINT_T3DD809782D037568CA3A200FC33A8DA7390405CD_H
#ifndef SECP256K1POINT_T73B95080B3D33CB70644A9BFBBA68EA06B821FA7_H
#define SECP256K1POINT_T73B95080B3D33CB70644A9BFBBA68EA06B821FA7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecP256K1Point
struct  SecP256K1Point_t73B95080B3D33CB70644A9BFBBA68EA06B821FA7  : public AbstractFpPoint_t3AC6C98A238F7A4B7FFAE487A33FCB48BCE6C0F7
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP256K1POINT_T73B95080B3D33CB70644A9BFBBA68EA06B821FA7_H
#ifndef SECP256R1POINT_T295A7951697911EDEDBEBBF5A781E9D3071E086C_H
#define SECP256R1POINT_T295A7951697911EDEDBEBBF5A781E9D3071E086C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecP256R1Point
struct  SecP256R1Point_t295A7951697911EDEDBEBBF5A781E9D3071E086C  : public AbstractFpPoint_t3AC6C98A238F7A4B7FFAE487A33FCB48BCE6C0F7
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP256R1POINT_T295A7951697911EDEDBEBBF5A781E9D3071E086C_H
#ifndef SECP384R1POINT_TA00FA4528D94813D8BAE66A3EF5E79F07BD62D29_H
#define SECP384R1POINT_TA00FA4528D94813D8BAE66A3EF5E79F07BD62D29_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecP384R1Point
struct  SecP384R1Point_tA00FA4528D94813D8BAE66A3EF5E79F07BD62D29  : public AbstractFpPoint_t3AC6C98A238F7A4B7FFAE487A33FCB48BCE6C0F7
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP384R1POINT_TA00FA4528D94813D8BAE66A3EF5E79F07BD62D29_H
#ifndef SECP521R1POINT_TECEE1E8F65AA393C27ACFC30A38A1A57D092C117_H
#define SECP521R1POINT_TECEE1E8F65AA393C27ACFC30A38A1A57D092C117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecP521R1Point
struct  SecP521R1Point_tECEE1E8F65AA393C27ACFC30A38A1A57D092C117  : public AbstractFpPoint_t3AC6C98A238F7A4B7FFAE487A33FCB48BCE6C0F7
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP521R1POINT_TECEE1E8F65AA393C27ACFC30A38A1A57D092C117_H
#ifndef SECT113R1POINT_T11FE9563DDC91F15F35658317310EDF469EFFC55_H
#define SECT113R1POINT_T11FE9563DDC91F15F35658317310EDF469EFFC55_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecT113R1Point
struct  SecT113R1Point_t11FE9563DDC91F15F35658317310EDF469EFFC55  : public AbstractF2mPoint_t31951E0CF9009420711A3FF51DE270702A830FAD
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT113R1POINT_T11FE9563DDC91F15F35658317310EDF469EFFC55_H
#ifndef SECT113R2POINT_T815197D019F8BBF408E4B3829239619835C98EF6_H
#define SECT113R2POINT_T815197D019F8BBF408E4B3829239619835C98EF6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecT113R2Point
struct  SecT113R2Point_t815197D019F8BBF408E4B3829239619835C98EF6  : public AbstractF2mPoint_t31951E0CF9009420711A3FF51DE270702A830FAD
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT113R2POINT_T815197D019F8BBF408E4B3829239619835C98EF6_H
#ifndef SECT131R1POINT_T650635A631EAF1EE11C6B446D0FB58D417AD74D7_H
#define SECT131R1POINT_T650635A631EAF1EE11C6B446D0FB58D417AD74D7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecT131R1Point
struct  SecT131R1Point_t650635A631EAF1EE11C6B446D0FB58D417AD74D7  : public AbstractF2mPoint_t31951E0CF9009420711A3FF51DE270702A830FAD
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT131R1POINT_T650635A631EAF1EE11C6B446D0FB58D417AD74D7_H
#ifndef SECT131R2POINT_TA6875915938E95528D2C942965CA3AB629785851_H
#define SECT131R2POINT_TA6875915938E95528D2C942965CA3AB629785851_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecT131R2Point
struct  SecT131R2Point_tA6875915938E95528D2C942965CA3AB629785851  : public AbstractF2mPoint_t31951E0CF9009420711A3FF51DE270702A830FAD
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT131R2POINT_TA6875915938E95528D2C942965CA3AB629785851_H
#ifndef SECT163K1POINT_T6A7BAF7AF848A31595D0C59C983E50D6E8C10EDC_H
#define SECT163K1POINT_T6A7BAF7AF848A31595D0C59C983E50D6E8C10EDC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecT163K1Point
struct  SecT163K1Point_t6A7BAF7AF848A31595D0C59C983E50D6E8C10EDC  : public AbstractF2mPoint_t31951E0CF9009420711A3FF51DE270702A830FAD
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT163K1POINT_T6A7BAF7AF848A31595D0C59C983E50D6E8C10EDC_H
#ifndef SECT163R1POINT_T337935F97D827A290DA27C9833B9C31B8672E57E_H
#define SECT163R1POINT_T337935F97D827A290DA27C9833B9C31B8672E57E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecT163R1Point
struct  SecT163R1Point_t337935F97D827A290DA27C9833B9C31B8672E57E  : public AbstractF2mPoint_t31951E0CF9009420711A3FF51DE270702A830FAD
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT163R1POINT_T337935F97D827A290DA27C9833B9C31B8672E57E_H
#ifndef SECT163R2POINT_T48E9B3A96F8B4DD71022478E2AE63A7015F2F682_H
#define SECT163R2POINT_T48E9B3A96F8B4DD71022478E2AE63A7015F2F682_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecT163R2Point
struct  SecT163R2Point_t48E9B3A96F8B4DD71022478E2AE63A7015F2F682  : public AbstractF2mPoint_t31951E0CF9009420711A3FF51DE270702A830FAD
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT163R2POINT_T48E9B3A96F8B4DD71022478E2AE63A7015F2F682_H
#ifndef SECT193R1POINT_T5E2007F0FA3823BF8A80DCDB359AA04BC2DD43E4_H
#define SECT193R1POINT_T5E2007F0FA3823BF8A80DCDB359AA04BC2DD43E4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecT193R1Point
struct  SecT193R1Point_t5E2007F0FA3823BF8A80DCDB359AA04BC2DD43E4  : public AbstractF2mPoint_t31951E0CF9009420711A3FF51DE270702A830FAD
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT193R1POINT_T5E2007F0FA3823BF8A80DCDB359AA04BC2DD43E4_H
#ifndef SECT193R2POINT_TEF874435F9B32986838CDE770CECE8F6DB0711CB_H
#define SECT193R2POINT_TEF874435F9B32986838CDE770CECE8F6DB0711CB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecT193R2Point
struct  SecT193R2Point_tEF874435F9B32986838CDE770CECE8F6DB0711CB  : public AbstractF2mPoint_t31951E0CF9009420711A3FF51DE270702A830FAD
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT193R2POINT_TEF874435F9B32986838CDE770CECE8F6DB0711CB_H
#ifndef SECT233K1POINT_T55AC6FAC7F0B890D2C66429AB21918B15EB034DA_H
#define SECT233K1POINT_T55AC6FAC7F0B890D2C66429AB21918B15EB034DA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecT233K1Point
struct  SecT233K1Point_t55AC6FAC7F0B890D2C66429AB21918B15EB034DA  : public AbstractF2mPoint_t31951E0CF9009420711A3FF51DE270702A830FAD
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT233K1POINT_T55AC6FAC7F0B890D2C66429AB21918B15EB034DA_H
#ifndef SECT233R1POINT_TA9DCCEB01CCB8ED6862B7B7605362C4C58B1DC76_H
#define SECT233R1POINT_TA9DCCEB01CCB8ED6862B7B7605362C4C58B1DC76_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecT233R1Point
struct  SecT233R1Point_tA9DCCEB01CCB8ED6862B7B7605362C4C58B1DC76  : public AbstractF2mPoint_t31951E0CF9009420711A3FF51DE270702A830FAD
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT233R1POINT_TA9DCCEB01CCB8ED6862B7B7605362C4C58B1DC76_H
#ifndef SECT239K1POINT_TA128635B479380D6A48C9690715188870A470EA0_H
#define SECT239K1POINT_TA128635B479380D6A48C9690715188870A470EA0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecT239K1Point
struct  SecT239K1Point_tA128635B479380D6A48C9690715188870A470EA0  : public AbstractF2mPoint_t31951E0CF9009420711A3FF51DE270702A830FAD
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT239K1POINT_TA128635B479380D6A48C9690715188870A470EA0_H
#ifndef SECT283K1POINT_TE59E5378A535C6ED38C1A2AFC9CB0DC47A69D8F3_H
#define SECT283K1POINT_TE59E5378A535C6ED38C1A2AFC9CB0DC47A69D8F3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecT283K1Point
struct  SecT283K1Point_tE59E5378A535C6ED38C1A2AFC9CB0DC47A69D8F3  : public AbstractF2mPoint_t31951E0CF9009420711A3FF51DE270702A830FAD
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT283K1POINT_TE59E5378A535C6ED38C1A2AFC9CB0DC47A69D8F3_H
#ifndef SECT283R1POINT_TF68A0F98707126343F1D8D05DF7D69AA718C3A8B_H
#define SECT283R1POINT_TF68A0F98707126343F1D8D05DF7D69AA718C3A8B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecT283R1Point
struct  SecT283R1Point_tF68A0F98707126343F1D8D05DF7D69AA718C3A8B  : public AbstractF2mPoint_t31951E0CF9009420711A3FF51DE270702A830FAD
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT283R1POINT_TF68A0F98707126343F1D8D05DF7D69AA718C3A8B_H
#ifndef SECT409K1POINT_T8BA8093464B27205D0D5D6431AC79B0963985791_H
#define SECT409K1POINT_T8BA8093464B27205D0D5D6431AC79B0963985791_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecT409K1Point
struct  SecT409K1Point_t8BA8093464B27205D0D5D6431AC79B0963985791  : public AbstractF2mPoint_t31951E0CF9009420711A3FF51DE270702A830FAD
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT409K1POINT_T8BA8093464B27205D0D5D6431AC79B0963985791_H
#ifndef SECT409R1POINT_TCE020433FAD15C841CD2C5EE9A07908B32062BBB_H
#define SECT409R1POINT_TCE020433FAD15C841CD2C5EE9A07908B32062BBB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecT409R1Point
struct  SecT409R1Point_tCE020433FAD15C841CD2C5EE9A07908B32062BBB  : public AbstractF2mPoint_t31951E0CF9009420711A3FF51DE270702A830FAD
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT409R1POINT_TCE020433FAD15C841CD2C5EE9A07908B32062BBB_H
#ifndef SECT571K1POINT_TC7B3CA5C02D4A361FC41724CA5DBF92648999946_H
#define SECT571K1POINT_TC7B3CA5C02D4A361FC41724CA5DBF92648999946_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecT571K1Point
struct  SecT571K1Point_tC7B3CA5C02D4A361FC41724CA5DBF92648999946  : public AbstractF2mPoint_t31951E0CF9009420711A3FF51DE270702A830FAD
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT571K1POINT_TC7B3CA5C02D4A361FC41724CA5DBF92648999946_H
#ifndef SECT571R1POINT_TDAED963E59F595895CD9B5F8A3B55079F5DE9FE7_H
#define SECT571R1POINT_TDAED963E59F595895CD9B5F8A3B55079F5DE9FE7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecT571R1Point
struct  SecT571R1Point_tDAED963E59F595895CD9B5F8A3B55079F5DE9FE7  : public AbstractF2mPoint_t31951E0CF9009420711A3FF51DE270702A830FAD
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT571R1POINT_TDAED963E59F595895CD9B5F8A3B55079F5DE9FE7_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5300 = { sizeof (SecP224R1Point_t3DD809782D037568CA3A200FC33A8DA7390405CD), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5301 = { sizeof (SecP256K1Curve_tC57EA0CD3D7A7017477D984223AB239D1CC733D3), -1, sizeof(SecP256K1Curve_tC57EA0CD3D7A7017477D984223AB239D1CC733D3_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5301[2] = 
{
	SecP256K1Curve_tC57EA0CD3D7A7017477D984223AB239D1CC733D3_StaticFields::get_offset_of_q_8(),
	SecP256K1Curve_tC57EA0CD3D7A7017477D984223AB239D1CC733D3::get_offset_of_m_infinity_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5302 = { sizeof (SecP256K1Field_t24CCD5F14810A50A32E41B44DD92F4AD00D37E45), -1, sizeof(SecP256K1Field_t24CCD5F14810A50A32E41B44DD92F4AD00D37E45_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5302[3] = 
{
	SecP256K1Field_t24CCD5F14810A50A32E41B44DD92F4AD00D37E45_StaticFields::get_offset_of_P_0(),
	SecP256K1Field_t24CCD5F14810A50A32E41B44DD92F4AD00D37E45_StaticFields::get_offset_of_PExt_1(),
	SecP256K1Field_t24CCD5F14810A50A32E41B44DD92F4AD00D37E45_StaticFields::get_offset_of_PExtInv_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5303 = { sizeof (SecP256K1FieldElement_tAD0D5656F3082824DA68A3EA2D7124AA21207579), -1, sizeof(SecP256K1FieldElement_tAD0D5656F3082824DA68A3EA2D7124AA21207579_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5303[2] = 
{
	SecP256K1FieldElement_tAD0D5656F3082824DA68A3EA2D7124AA21207579_StaticFields::get_offset_of_Q_0(),
	SecP256K1FieldElement_tAD0D5656F3082824DA68A3EA2D7124AA21207579::get_offset_of_x_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5304 = { sizeof (SecP256K1Point_t73B95080B3D33CB70644A9BFBBA68EA06B821FA7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5305 = { sizeof (SecP256R1Curve_t541948E2F0D1D5AD75E1FD0ACBB3EBD301AC2C30), -1, sizeof(SecP256R1Curve_t541948E2F0D1D5AD75E1FD0ACBB3EBD301AC2C30_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5305[2] = 
{
	SecP256R1Curve_t541948E2F0D1D5AD75E1FD0ACBB3EBD301AC2C30_StaticFields::get_offset_of_q_8(),
	SecP256R1Curve_t541948E2F0D1D5AD75E1FD0ACBB3EBD301AC2C30::get_offset_of_m_infinity_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5306 = { sizeof (SecP256R1Field_tE496FAC39A67AEB48EF815A739CD7BB6C6BCD815), -1, sizeof(SecP256R1Field_tE496FAC39A67AEB48EF815A739CD7BB6C6BCD815_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5306[2] = 
{
	SecP256R1Field_tE496FAC39A67AEB48EF815A739CD7BB6C6BCD815_StaticFields::get_offset_of_P_0(),
	SecP256R1Field_tE496FAC39A67AEB48EF815A739CD7BB6C6BCD815_StaticFields::get_offset_of_PExt_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5307 = { sizeof (SecP256R1FieldElement_t8BF1F4BC229166173D5D9884FE6A1B955299003C), -1, sizeof(SecP256R1FieldElement_t8BF1F4BC229166173D5D9884FE6A1B955299003C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5307[2] = 
{
	SecP256R1FieldElement_t8BF1F4BC229166173D5D9884FE6A1B955299003C_StaticFields::get_offset_of_Q_0(),
	SecP256R1FieldElement_t8BF1F4BC229166173D5D9884FE6A1B955299003C::get_offset_of_x_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5308 = { sizeof (SecP256R1Point_t295A7951697911EDEDBEBBF5A781E9D3071E086C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5309 = { sizeof (SecP384R1Curve_tBFE2A712929B52D32E2DDD31B3E38CC85BD25901), -1, sizeof(SecP384R1Curve_tBFE2A712929B52D32E2DDD31B3E38CC85BD25901_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5309[2] = 
{
	SecP384R1Curve_tBFE2A712929B52D32E2DDD31B3E38CC85BD25901_StaticFields::get_offset_of_q_8(),
	SecP384R1Curve_tBFE2A712929B52D32E2DDD31B3E38CC85BD25901::get_offset_of_m_infinity_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5310 = { sizeof (SecP384R1Field_tADDA4C62510A5EA24D0D61FFA87D39080C5DB004), -1, sizeof(SecP384R1Field_tADDA4C62510A5EA24D0D61FFA87D39080C5DB004_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5310[3] = 
{
	SecP384R1Field_tADDA4C62510A5EA24D0D61FFA87D39080C5DB004_StaticFields::get_offset_of_P_0(),
	SecP384R1Field_tADDA4C62510A5EA24D0D61FFA87D39080C5DB004_StaticFields::get_offset_of_PExt_1(),
	SecP384R1Field_tADDA4C62510A5EA24D0D61FFA87D39080C5DB004_StaticFields::get_offset_of_PExtInv_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5311 = { sizeof (SecP384R1FieldElement_t10047E0C5A2477110224F067DE1749A0811A837D), -1, sizeof(SecP384R1FieldElement_t10047E0C5A2477110224F067DE1749A0811A837D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5311[2] = 
{
	SecP384R1FieldElement_t10047E0C5A2477110224F067DE1749A0811A837D_StaticFields::get_offset_of_Q_0(),
	SecP384R1FieldElement_t10047E0C5A2477110224F067DE1749A0811A837D::get_offset_of_x_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5312 = { sizeof (SecP384R1Point_tA00FA4528D94813D8BAE66A3EF5E79F07BD62D29), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5313 = { sizeof (SecP521R1Curve_t1644010879D55FA816214B2D767941F29AA354DF), -1, sizeof(SecP521R1Curve_t1644010879D55FA816214B2D767941F29AA354DF_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5313[2] = 
{
	SecP521R1Curve_t1644010879D55FA816214B2D767941F29AA354DF_StaticFields::get_offset_of_q_8(),
	SecP521R1Curve_t1644010879D55FA816214B2D767941F29AA354DF::get_offset_of_m_infinity_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5314 = { sizeof (SecP521R1Field_tEF9772D9A5C9B93D7A2051F8A3285C9101D6EC5C), -1, sizeof(SecP521R1Field_tEF9772D9A5C9B93D7A2051F8A3285C9101D6EC5C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5314[1] = 
{
	SecP521R1Field_tEF9772D9A5C9B93D7A2051F8A3285C9101D6EC5C_StaticFields::get_offset_of_P_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5315 = { sizeof (SecP521R1FieldElement_t20122F081AC9EA686EFB0B8649FC60EF6792C24F), -1, sizeof(SecP521R1FieldElement_t20122F081AC9EA686EFB0B8649FC60EF6792C24F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5315[2] = 
{
	SecP521R1FieldElement_t20122F081AC9EA686EFB0B8649FC60EF6792C24F_StaticFields::get_offset_of_Q_0(),
	SecP521R1FieldElement_t20122F081AC9EA686EFB0B8649FC60EF6792C24F::get_offset_of_x_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5316 = { sizeof (SecP521R1Point_tECEE1E8F65AA393C27ACFC30A38A1A57D092C117), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5317 = { sizeof (SecT113Field_t7FE017730BFFC2B84B9FB07235FC6DF0578BDA3C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5318 = { sizeof (SecT113FieldElement_t7769E2514486C96383271692ED57E8CEE8AAF050), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5318[1] = 
{
	SecT113FieldElement_t7769E2514486C96383271692ED57E8CEE8AAF050::get_offset_of_x_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5319 = { sizeof (SecT113R1Curve_t95C1F4B08F7AE366397933983A93505A6190B758), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5319[1] = 
{
	SecT113R1Curve_t95C1F4B08F7AE366397933983A93505A6190B758::get_offset_of_m_infinity_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5320 = { sizeof (SecT113R1Point_t11FE9563DDC91F15F35658317310EDF469EFFC55), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5321 = { sizeof (SecT113R2Curve_t05698035683EF81A8DC2EFE83F44645933F4B8DC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5321[1] = 
{
	SecT113R2Curve_t05698035683EF81A8DC2EFE83F44645933F4B8DC::get_offset_of_m_infinity_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5322 = { sizeof (SecT113R2Point_t815197D019F8BBF408E4B3829239619835C98EF6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5323 = { sizeof (SecT131Field_t1F7B3AFCA00D1B479925D7FCA70B07B8F62FFB98), -1, sizeof(SecT131Field_t1F7B3AFCA00D1B479925D7FCA70B07B8F62FFB98_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5323[1] = 
{
	SecT131Field_t1F7B3AFCA00D1B479925D7FCA70B07B8F62FFB98_StaticFields::get_offset_of_ROOT_Z_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5324 = { sizeof (SecT131FieldElement_tF4A8DB14108F9419F12F280364EA6A5BEA3D1BDE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5324[1] = 
{
	SecT131FieldElement_tF4A8DB14108F9419F12F280364EA6A5BEA3D1BDE::get_offset_of_x_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5325 = { sizeof (SecT131R1Curve_tF04D847B53F6892A71114EB769D9F1A2ADFDC350), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5325[1] = 
{
	SecT131R1Curve_tF04D847B53F6892A71114EB769D9F1A2ADFDC350::get_offset_of_m_infinity_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5326 = { sizeof (SecT131R1Point_t650635A631EAF1EE11C6B446D0FB58D417AD74D7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5327 = { sizeof (SecT131R2Curve_tC4A0C0E7D0E0AA8D2E2634F73281E30242CAA100), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5327[1] = 
{
	SecT131R2Curve_tC4A0C0E7D0E0AA8D2E2634F73281E30242CAA100::get_offset_of_m_infinity_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5328 = { sizeof (SecT131R2Point_tA6875915938E95528D2C942965CA3AB629785851), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5329 = { sizeof (SecT163Field_tD9E85435CA2A00D2280642412D46FA11D8E48317), -1, sizeof(SecT163Field_tD9E85435CA2A00D2280642412D46FA11D8E48317_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5329[1] = 
{
	SecT163Field_tD9E85435CA2A00D2280642412D46FA11D8E48317_StaticFields::get_offset_of_ROOT_Z_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5330 = { sizeof (SecT163FieldElement_t0FEFDCFBAD3998866C45862284914966A0417D2A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5330[1] = 
{
	SecT163FieldElement_t0FEFDCFBAD3998866C45862284914966A0417D2A::get_offset_of_x_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5331 = { sizeof (SecT163K1Curve_tBB0FD03C5F274ABE7B125B9C4B82FE88618115AA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5331[1] = 
{
	SecT163K1Curve_tBB0FD03C5F274ABE7B125B9C4B82FE88618115AA::get_offset_of_m_infinity_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5332 = { sizeof (SecT163K1Point_t6A7BAF7AF848A31595D0C59C983E50D6E8C10EDC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5333 = { sizeof (SecT163R1Curve_tD96CDE4871D9D9F30CF8DDEF455A2A777CD98713), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5333[1] = 
{
	SecT163R1Curve_tD96CDE4871D9D9F30CF8DDEF455A2A777CD98713::get_offset_of_m_infinity_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5334 = { sizeof (SecT163R1Point_t337935F97D827A290DA27C9833B9C31B8672E57E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5335 = { sizeof (SecT163R2Curve_t445D90CA284E53218D4B4B7A863588456BE00F5C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5335[1] = 
{
	SecT163R2Curve_t445D90CA284E53218D4B4B7A863588456BE00F5C::get_offset_of_m_infinity_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5336 = { sizeof (SecT163R2Point_t48E9B3A96F8B4DD71022478E2AE63A7015F2F682), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5337 = { sizeof (SecT193Field_tEBCD38351A881C400B7E15CB1FE77CCB962FCC9F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5338 = { sizeof (SecT193FieldElement_tF52AD9A3BB90ABC7F3C9F705C46D8E963297C6F0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5338[1] = 
{
	SecT193FieldElement_tF52AD9A3BB90ABC7F3C9F705C46D8E963297C6F0::get_offset_of_x_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5339 = { sizeof (SecT193R1Curve_tD4EB61EBFFFC0F19BDB1CE67D004F031F8A15F02), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5339[1] = 
{
	SecT193R1Curve_tD4EB61EBFFFC0F19BDB1CE67D004F031F8A15F02::get_offset_of_m_infinity_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5340 = { sizeof (SecT193R1Point_t5E2007F0FA3823BF8A80DCDB359AA04BC2DD43E4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5341 = { sizeof (SecT193R2Curve_t6A733FF6BB077F5C6286746398C681152C794BDD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5341[1] = 
{
	SecT193R2Curve_t6A733FF6BB077F5C6286746398C681152C794BDD::get_offset_of_m_infinity_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5342 = { sizeof (SecT193R2Point_tEF874435F9B32986838CDE770CECE8F6DB0711CB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5343 = { sizeof (SecT233Field_t9B70548D86F30DCB84D1F285047EC911CA6F5D9E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5344 = { sizeof (SecT233FieldElement_t1A8CD620A7896D5F216E4114BBC8EE828552A189), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5344[1] = 
{
	SecT233FieldElement_t1A8CD620A7896D5F216E4114BBC8EE828552A189::get_offset_of_x_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5345 = { sizeof (SecT233K1Curve_t2E0066860970733CDE7870C8E75D7654DC7A5C46), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5345[1] = 
{
	SecT233K1Curve_t2E0066860970733CDE7870C8E75D7654DC7A5C46::get_offset_of_m_infinity_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5346 = { sizeof (SecT233K1Point_t55AC6FAC7F0B890D2C66429AB21918B15EB034DA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5347 = { sizeof (SecT233R1Curve_t496B52AEF7DB82F365410CB8363A4D6095FAFAA2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5347[1] = 
{
	SecT233R1Curve_t496B52AEF7DB82F365410CB8363A4D6095FAFAA2::get_offset_of_m_infinity_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5348 = { sizeof (SecT233R1Point_tA9DCCEB01CCB8ED6862B7B7605362C4C58B1DC76), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5349 = { sizeof (SecT239Field_t5992BC9617E4EFB8401A0B59955C01369D4DA51B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5350 = { sizeof (SecT239FieldElement_t1D0B47BC6465C2CD3CCA3D41F4F1FAB703E72B45), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5350[1] = 
{
	SecT239FieldElement_t1D0B47BC6465C2CD3CCA3D41F4F1FAB703E72B45::get_offset_of_x_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5351 = { sizeof (SecT239K1Curve_t70B6A2853D985306A0F6E8930BDAC4724EA3E5F8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5351[1] = 
{
	SecT239K1Curve_t70B6A2853D985306A0F6E8930BDAC4724EA3E5F8::get_offset_of_m_infinity_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5352 = { sizeof (SecT239K1Point_tA128635B479380D6A48C9690715188870A470EA0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5353 = { sizeof (SecT283Field_t2B99B4DE13EC25DF2E313ABC11E098E65B5EB5E3), -1, sizeof(SecT283Field_t2B99B4DE13EC25DF2E313ABC11E098E65B5EB5E3_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5353[1] = 
{
	SecT283Field_t2B99B4DE13EC25DF2E313ABC11E098E65B5EB5E3_StaticFields::get_offset_of_ROOT_Z_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5354 = { sizeof (SecT283FieldElement_t2DB52791BCEC9E0D22438D398608B51921422692), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5354[1] = 
{
	SecT283FieldElement_t2DB52791BCEC9E0D22438D398608B51921422692::get_offset_of_x_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5355 = { sizeof (SecT283K1Curve_t6C724D4AF8BFD3E2A636F76CA392F9ACC7865FAA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5355[1] = 
{
	SecT283K1Curve_t6C724D4AF8BFD3E2A636F76CA392F9ACC7865FAA::get_offset_of_m_infinity_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5356 = { sizeof (SecT283K1Point_tE59E5378A535C6ED38C1A2AFC9CB0DC47A69D8F3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5357 = { sizeof (SecT283R1Curve_t517AC3FBAE68F0145612CDDD8A45314F1FBDB7D9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5357[1] = 
{
	SecT283R1Curve_t517AC3FBAE68F0145612CDDD8A45314F1FBDB7D9::get_offset_of_m_infinity_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5358 = { sizeof (SecT283R1Point_tF68A0F98707126343F1D8D05DF7D69AA718C3A8B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5359 = { sizeof (SecT409Field_tB63C2C0AA8342F9278E96568B4D6B707C0941294), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5360 = { sizeof (SecT409FieldElement_t771A26E7C9D081C6A75B4D13C01CA2713AAC4CBB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5360[1] = 
{
	SecT409FieldElement_t771A26E7C9D081C6A75B4D13C01CA2713AAC4CBB::get_offset_of_x_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5361 = { sizeof (SecT409K1Curve_tE67797CACC543D1BE3DE27938FD9EAA3F52BE281), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5361[1] = 
{
	SecT409K1Curve_tE67797CACC543D1BE3DE27938FD9EAA3F52BE281::get_offset_of_m_infinity_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5362 = { sizeof (SecT409K1Point_t8BA8093464B27205D0D5D6431AC79B0963985791), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5363 = { sizeof (SecT409R1Curve_t5FBF26F8595DD21119C384EA7EC2DE8C940EA303), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5363[1] = 
{
	SecT409R1Curve_t5FBF26F8595DD21119C384EA7EC2DE8C940EA303::get_offset_of_m_infinity_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5364 = { sizeof (SecT409R1Point_tCE020433FAD15C841CD2C5EE9A07908B32062BBB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5365 = { sizeof (SecT571Field_tEB4A9472322DF831416F318C64DAFC854F545396), -1, sizeof(SecT571Field_tEB4A9472322DF831416F318C64DAFC854F545396_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5365[1] = 
{
	SecT571Field_tEB4A9472322DF831416F318C64DAFC854F545396_StaticFields::get_offset_of_ROOT_Z_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5366 = { sizeof (SecT571FieldElement_t9EF7730630D1449DD2009986E22E25491864423B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5366[1] = 
{
	SecT571FieldElement_t9EF7730630D1449DD2009986E22E25491864423B::get_offset_of_x_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5367 = { sizeof (SecT571K1Curve_t5063698C3CD26A28FFE6D3B6F5A9BE0F1863DE5E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5367[1] = 
{
	SecT571K1Curve_t5063698C3CD26A28FFE6D3B6F5A9BE0F1863DE5E::get_offset_of_m_infinity_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5368 = { sizeof (SecT571K1Point_tC7B3CA5C02D4A361FC41724CA5DBF92648999946), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5369 = { sizeof (SecT571R1Curve_tA50E4FD2F2511B362F86D0456CE538A44B3ADE78), -1, sizeof(SecT571R1Curve_tA50E4FD2F2511B362F86D0456CE538A44B3ADE78_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5369[3] = 
{
	SecT571R1Curve_tA50E4FD2F2511B362F86D0456CE538A44B3ADE78::get_offset_of_m_infinity_9(),
	SecT571R1Curve_tA50E4FD2F2511B362F86D0456CE538A44B3ADE78_StaticFields::get_offset_of_SecT571R1_B_10(),
	SecT571R1Curve_tA50E4FD2F2511B362F86D0456CE538A44B3ADE78_StaticFields::get_offset_of_SecT571R1_B_SQRT_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5370 = { sizeof (SecT571R1Point_tDAED963E59F595895CD9B5F8A3B55079F5DE9FE7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5371 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5372 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5373 = { sizeof (GlvTypeBEndomorphism_tAF04909F590E9A60E7019BA4B81C5FF31ABD6AA1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5373[3] = 
{
	GlvTypeBEndomorphism_tAF04909F590E9A60E7019BA4B81C5FF31ABD6AA1::get_offset_of_m_curve_0(),
	GlvTypeBEndomorphism_tAF04909F590E9A60E7019BA4B81C5FF31ABD6AA1::get_offset_of_m_parameters_1(),
	GlvTypeBEndomorphism_tAF04909F590E9A60E7019BA4B81C5FF31ABD6AA1::get_offset_of_m_pointMap_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5374 = { sizeof (GlvTypeBParameters_t39B069A23C048B4E871EB64BBE2F714DDD864095), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5374[7] = 
{
	GlvTypeBParameters_t39B069A23C048B4E871EB64BBE2F714DDD864095::get_offset_of_m_beta_0(),
	GlvTypeBParameters_t39B069A23C048B4E871EB64BBE2F714DDD864095::get_offset_of_m_lambda_1(),
	GlvTypeBParameters_t39B069A23C048B4E871EB64BBE2F714DDD864095::get_offset_of_m_v1_2(),
	GlvTypeBParameters_t39B069A23C048B4E871EB64BBE2F714DDD864095::get_offset_of_m_v2_3(),
	GlvTypeBParameters_t39B069A23C048B4E871EB64BBE2F714DDD864095::get_offset_of_m_g1_4(),
	GlvTypeBParameters_t39B069A23C048B4E871EB64BBE2F714DDD864095::get_offset_of_m_g2_5(),
	GlvTypeBParameters_t39B069A23C048B4E871EB64BBE2F714DDD864095::get_offset_of_m_bits_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5375 = { sizeof (AbstractECMultiplier_t53B6ADC66D5D126840E4ADFF2C4B048592CB16D3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5376 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5377 = { sizeof (FixedPointCombMultiplier_t8E62E787A7ACE99B8BA58FEB7E421E4F174A7218), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5378 = { sizeof (FixedPointPreCompInfo_t026E1CC2AE3FDB86A198646DA1BD0A1066B23A80), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5378[2] = 
{
	FixedPointPreCompInfo_t026E1CC2AE3FDB86A198646DA1BD0A1066B23A80::get_offset_of_m_preComp_0(),
	FixedPointPreCompInfo_t026E1CC2AE3FDB86A198646DA1BD0A1066B23A80::get_offset_of_m_width_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5379 = { sizeof (FixedPointUtilities_t0AE152237232F5997834243B1B5B3F99A2719FFB), -1, sizeof(FixedPointUtilities_t0AE152237232F5997834243B1B5B3F99A2719FFB_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5379[1] = 
{
	FixedPointUtilities_t0AE152237232F5997834243B1B5B3F99A2719FFB_StaticFields::get_offset_of_PRECOMP_NAME_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5380 = { sizeof (GlvMultiplier_t007132D5C7C548A7F6297CAA0330256A278E0A7C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5380[2] = 
{
	GlvMultiplier_t007132D5C7C548A7F6297CAA0330256A278E0A7C::get_offset_of_curve_0(),
	GlvMultiplier_t007132D5C7C548A7F6297CAA0330256A278E0A7C::get_offset_of_glvEndomorphism_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5381 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5382 = { sizeof (WNafL2RMultiplier_t2F3A759650B75812D286727F7E1DBA95430D3B2A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5383 = { sizeof (WNafPreCompInfo_t7718F0CE35D08CF11B747D138B0EEBAA71009746), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5383[3] = 
{
	WNafPreCompInfo_t7718F0CE35D08CF11B747D138B0EEBAA71009746::get_offset_of_m_preComp_0(),
	WNafPreCompInfo_t7718F0CE35D08CF11B747D138B0EEBAA71009746::get_offset_of_m_preCompNeg_1(),
	WNafPreCompInfo_t7718F0CE35D08CF11B747D138B0EEBAA71009746::get_offset_of_m_twice_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5384 = { sizeof (WNafUtilities_t5C561C6BAF9EDDB2CA522857116F071440DE6AAA), -1, sizeof(WNafUtilities_t5C561C6BAF9EDDB2CA522857116F071440DE6AAA_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5384[5] = 
{
	WNafUtilities_t5C561C6BAF9EDDB2CA522857116F071440DE6AAA_StaticFields::get_offset_of_PRECOMP_NAME_0(),
	WNafUtilities_t5C561C6BAF9EDDB2CA522857116F071440DE6AAA_StaticFields::get_offset_of_DEFAULT_WINDOW_SIZE_CUTOFFS_1(),
	WNafUtilities_t5C561C6BAF9EDDB2CA522857116F071440DE6AAA_StaticFields::get_offset_of_EMPTY_BYTES_2(),
	WNafUtilities_t5C561C6BAF9EDDB2CA522857116F071440DE6AAA_StaticFields::get_offset_of_EMPTY_INTS_3(),
	WNafUtilities_t5C561C6BAF9EDDB2CA522857116F071440DE6AAA_StaticFields::get_offset_of_EMPTY_POINTS_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5385 = { sizeof (WTauNafMultiplier_t299D29F97AD9E7726B59728F537703F061A457C1), -1, sizeof(WTauNafMultiplier_t299D29F97AD9E7726B59728F537703F061A457C1_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5385[1] = 
{
	WTauNafMultiplier_t299D29F97AD9E7726B59728F537703F061A457C1_StaticFields::get_offset_of_PRECOMP_NAME_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5386 = { sizeof (WTauNafPreCompInfo_tB2474DD90D64E4C74787B55B6EEAB92C2C2A8662), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5386[1] = 
{
	WTauNafPreCompInfo_tB2474DD90D64E4C74787B55B6EEAB92C2C2A8662::get_offset_of_m_preComp_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5387 = { sizeof (FiniteFields_t0A726393363CD545AF14EB735BD270DB62C6FD41), -1, sizeof(FiniteFields_t0A726393363CD545AF14EB735BD270DB62C6FD41_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5387[2] = 
{
	FiniteFields_t0A726393363CD545AF14EB735BD270DB62C6FD41_StaticFields::get_offset_of_GF_2_0(),
	FiniteFields_t0A726393363CD545AF14EB735BD270DB62C6FD41_StaticFields::get_offset_of_GF_3_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5388 = { sizeof (GF2Polynomial_t268B6A651AE4801B8EB70232EECB1403A44363C7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5388[1] = 
{
	GF2Polynomial_t268B6A651AE4801B8EB70232EECB1403A44363C7::get_offset_of_exponents_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5389 = { sizeof (GenericPolynomialExtensionField_t92D8755AAC2EA9D5A2EAD32AA1F87555CBFD311A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5389[2] = 
{
	GenericPolynomialExtensionField_t92D8755AAC2EA9D5A2EAD32AA1F87555CBFD311A::get_offset_of_subfield_0(),
	GenericPolynomialExtensionField_t92D8755AAC2EA9D5A2EAD32AA1F87555CBFD311A::get_offset_of_minimalPolynomial_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5390 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5391 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5392 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5393 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5394 = { sizeof (PrimeField_tC739CE15084D2C1F30625BBDF0DB282FA0C68E85), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5394[1] = 
{
	PrimeField_tC739CE15084D2C1F30625BBDF0DB282FA0C68E85::get_offset_of_characteristic_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5395 = { sizeof (Interleave_t8B36F4EB3242C1AB7CF0A68CFFE28547FF25B87C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5396 = { sizeof (Mod_t5026E714058F605ACC98A7F33710A603850979E8), -1, sizeof(Mod_t5026E714058F605ACC98A7F33710A603850979E8_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5396[1] = 
{
	Mod_t5026E714058F605ACC98A7F33710A603850979E8_StaticFields::get_offset_of_RandomSource_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5397 = { sizeof (Nat_t1A3C75E3A07B58588BEBA310D2E23695EBE3C496), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5398 = { sizeof (Nat128_tBEDF74DA30BFC396E9C10D1DD21FCD56F9A57626), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5399 = { sizeof (Nat160_t7506F58D3F2E4A558A6BF24251EF2986CD4EE02F), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
