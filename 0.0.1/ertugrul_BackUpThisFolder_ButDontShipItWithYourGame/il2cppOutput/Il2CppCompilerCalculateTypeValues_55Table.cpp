﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Org.BouncyCastle.Crypto.Tls.TlsCipherFactory
struct TlsCipherFactory_t5995BB8E2767BA0519D3BA1629F238C499C47DE9;
// Org.BouncyCastle.Crypto.Tls.TlsClientContext
struct TlsClientContext_tE01FA93AE9165EE22FA37C2E6B3C32E99C17B989;
// Org.BouncyCastle.X509.X509Certificate
struct X509Certificate_t9350C69721F95FA663C0FD6ED97CDF09B8037786;
// SuperSocket.ClientEngine.ConnectedCallback
struct ConnectedCallback_t27F6846DBFF78119A3FB59CB3C577CFB61BD2D75;
// SuperSocket.ClientEngine.DataEventArgs
struct DataEventArgs_tAD557B50CCC2D8F34E8B607EF7B6A1C7FA6614A3;
// SuperSocket.ClientEngine.IBatchQueue`1<System.ArraySegment`1<System.Byte>>
struct IBatchQueue_1_tD5B08A60253CD67A4B3C511A871EAFF9C17038CE;
// SuperSocket.ClientEngine.IProxyConnector
struct IProxyConnector_t1EE22B8387D000E7E884CB990B0AFBECD2C95108;
// SuperSocket.ClientEngine.PosList`1<System.ArraySegment`1<System.Byte>>
struct PosList_1_tF22B6140877DA647665E0F81051342E9906F4098;
// SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Byte>
struct ArraySegmentEx_1_tAE28209676C8724BA42ACC7CC56900802D94A54E;
// SuperSocket.ClientEngine.Protocol.ArraySegmentList
struct ArraySegmentList_t3FB5043710933FF6704A1AFCE6295DE8CE401F18;
// SuperSocket.ClientEngine.Protocol.IClientCommandReader`1<WebSocket4Net.WebSocketCommandInfo>
struct IClientCommandReader_1_t5171BBC6618FDB31B3217AB86D3667048F8BEC61;
// SuperSocket.ClientEngine.SearchMarkState`1<System.Byte>
struct SearchMarkState_1_t9CA9B71862282152C0C864021BBEE57C176B07D3;
// SuperSocket.ClientEngine.TcpClientSession
struct TcpClientSession_t9CF522EBB9A6C11BF178D2A41F2B8F54075DB9AA;
// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.Action`1<System.Byte[]>
struct Action_1_t67D6E8441D0DE589716B25B9D8F0D4412B593398;
// System.Action`1<System.String>
struct Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.String,SuperSocket.ClientEngine.Protocol.ICommand`2<WebSocket4Net.WebSocket,WebSocket4Net.WebSocketCommandInfo>>
struct Dictionary_2_t9ABC8A2F3204DE9FD1B6186FBF6DB71754F8F806;
// System.Collections.Generic.IDictionary`2<System.String,System.Object>
struct IDictionary_2_t1E5681AFFD86E8189077B1EE929272E2AF245A91;
// System.Collections.Generic.IList`1<SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Byte>>
struct IList_1_tDD34C53F3B6F55B9BF14848CF487ADB693591DF7;
// System.Collections.Generic.List`1<System.Char>
struct List_1_t028AAE01C4834286B7892F4498364F964CD8B316;
// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>
struct List_1_t03E69AA5D95F83EE79F8D6698A0C4E6340602A19;
// System.Collections.Generic.List`1<WebSocket4Net.Protocol.WebSocketDataFrame>
struct List_1_tB743623D7C86BACC71309E175C141687B4362BE5;
// System.Collections.IList
struct IList_tA637AB426E16F84F84ACC2813BDCF3A0414AF0AA;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.EventHandler
struct EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C;
// System.EventHandler`1<SuperSocket.ClientEngine.DataEventArgs>
struct EventHandler_1_tC90AC821DF9506E308EC2B34BE4D3DF05C7E4513;
// System.EventHandler`1<SuperSocket.ClientEngine.ErrorEventArgs>
struct EventHandler_1_t191D441D6952099231D3D3122D875173BEA68907;
// System.EventHandler`1<SuperSocket.ClientEngine.ProxyEventArgs>
struct EventHandler_1_t671742AB0AECF62E29BC8F5C2FD957D3ED1B2C7A;
// System.EventHandler`1<System.Xml.Linq.XObjectChangeEventArgs>
struct EventHandler_1_t893F89009B075641965395B3F7C20AA5549D00AC;
// System.EventHandler`1<WebSocket4Net.DataReceivedEventArgs>
struct EventHandler_1_t9C43B6F8703DA25284C5FD4CAF4423F4F967B65B;
// System.EventHandler`1<WebSocket4Net.MessageReceivedEventArgs>
struct EventHandler_1_tAB38B678A61DB7B67F5F5D79AEE8C2D32B768B51;
// System.Exception
struct Exception_t;
// System.Func`2<Org.BouncyCastle.Crypto.Tls.Certificate,System.String>
struct Func_2_t81AB5F1E16530F25286C7FCBA77049466BE9C90A;
// System.Func`2<System.ArraySegment`1<System.Byte>,System.Boolean>
struct Func_2_tAE16D74C9F08627D0E0D5F623E5AA350022A44B7;
// System.Func`2<System.Char,System.Boolean>
struct Func_2_t987FE48B7C07E5DDABA6CD66D8FFFF1627A0CEE5;
// System.Func`2<System.Int32,System.Int32>
struct Func_2_t7E959DEAF1C1E69DDAA4CC9284FA7F005E80606E;
// System.Func`2<WebSocket4Net.Protocol.IProtocolProcessor,System.Int32>
struct Func_2_t5ED58A84E0C866574612D3A526CA3CEA44B94F81;
// System.Func`2<WebSocket4Net.Protocol.WebSocketDataFrame,System.Int32>
struct Func_2_t620ED5CE01B9C27DAF57848FD5BC909D5ECBA9DE;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.IO.Stream
struct Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7;
// System.IO.Stream/ReadWriteTask
struct ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80;
// System.IO.StreamReader
struct StreamReader_t62E68063760DCD2FC036AE132DE69C24B7ED001E;
// System.IO.StreamWriter
struct StreamWriter_t989B894EF3BFCDF6FF5F5F068402A4F835FC8E8E;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Net.EndPoint
struct EndPoint_tD87FCEF2780A951E8CE8D808C345FBF2C088D980;
// System.Net.IPAddress[]
struct IPAddressU5BU5D_t7F25C4C038C43BFDA8EA84969112E82BADC38BC3;
// System.Net.Sockets.Socket
struct Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8;
// System.Net.Sockets.SocketAsyncEventArgs
struct SocketAsyncEventArgs_t5E05ABA36B4740D22D5E746DC4E7763AA448CAA7;
// System.Random
struct Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Text.Encoding
struct Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4;
// System.Threading.SemaphoreSlim
struct SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048;
// System.Threading.Timer
struct Timer_t67FAB8E41573B4FA09CA56AE30725AF4297C2553;
// System.Timers.Timer
struct Timer_t238C45D8635596EEA04884048EF70F96775D5ADC;
// System.Type[]
struct TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F;
// System.Uri
struct Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// System.WeakReference
struct WeakReference_t0495CC81CD6403E662B7700B802443F6F730E39D;
// System.Xml.Linq.XContainer
struct XContainer_t17E75C21493AAC7D12D6159A3A8C5308B3DBC599;
// System.Xml.Linq.XHashtable`1<System.WeakReference>
struct XHashtable_1_t0B1F552C698411530920D275A9C48ED8F72B66F2;
// System.Xml.Linq.XHashtable`1<System.Xml.Linq.XName>
struct XHashtable_1_tD5FCDADAB0E25D7F54936D993C541EF22988B4E1;
// System.Xml.Linq.XNamespace
struct XNamespace_t771F5C81A42B6DFC65678BB24EE414F8F9E7831D;
// WebSocket4Net.Protocol.FramePartReader.IDataFramePartReader
struct IDataFramePartReader_t1CE04038B101EDD302CE5E992658D9371CBBE0EF;
// WebSocket4Net.Protocol.ICloseStatusCode
struct ICloseStatusCode_tAF24E8DD1449A9C28EA2EB951B98F74FFFFD4ECB;
// WebSocket4Net.Protocol.IProtocolProcessor
struct IProtocolProcessor_tBE9640F58914BC14368ED699E67EF119EC32BB06;
// WebSocket4Net.Protocol.IProtocolProcessor[]
struct IProtocolProcessorU5BU5D_tD93BA92F26A4FEE5D61B0BDE7A98FC35F06402BB;
// WebSocket4Net.Protocol.ProtocolProcessorFactory
struct ProtocolProcessorFactory_t55DA417FFED0B9138D1330C2AE71C3B1501755FA;
// WebSocket4Net.Protocol.WebSocketDataFrame
struct WebSocketDataFrame_tAF30418C83AF20230D871C344C668A3157898C96;
// WebSocket4Net.WebSocket
struct WebSocket_t231538678AB4B76293D12A78A9D798992897B3BD;
// WebSocket4Net.WebSocketCommandInfo
struct WebSocketCommandInfo_t78D3A11DD086E0229D1AD938B61A9A9FC5CB133D;

struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;



#ifndef U3CMODULEU3E_T1E56E51F0CA5E53824F1EC0104C755EDCF29A5D8_H
#define U3CMODULEU3E_T1E56E51F0CA5E53824F1EC0104C755EDCF29A5D8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t1E56E51F0CA5E53824F1EC0104C755EDCF29A5D8 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T1E56E51F0CA5E53824F1EC0104C755EDCF29A5D8_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef GAMESPARKSTIMER_T73F609039E6A360DB023437BF00AA039000F4797_H
#define GAMESPARKSTIMER_T73F609039E6A360DB023437BF00AA039000F4797_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Core.GameSparksTimer
struct  GameSparksTimer_t73F609039E6A360DB023437BF00AA039000F4797  : public RuntimeObject
{
public:
	// System.Timers.Timer GameSparks.Core.GameSparksTimer::m_timer
	Timer_t238C45D8635596EEA04884048EF70F96775D5ADC * ___m_timer_0;
	// System.Action GameSparks.Core.GameSparksTimer::m_callback
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___m_callback_1;

public:
	inline static int32_t get_offset_of_m_timer_0() { return static_cast<int32_t>(offsetof(GameSparksTimer_t73F609039E6A360DB023437BF00AA039000F4797, ___m_timer_0)); }
	inline Timer_t238C45D8635596EEA04884048EF70F96775D5ADC * get_m_timer_0() const { return ___m_timer_0; }
	inline Timer_t238C45D8635596EEA04884048EF70F96775D5ADC ** get_address_of_m_timer_0() { return &___m_timer_0; }
	inline void set_m_timer_0(Timer_t238C45D8635596EEA04884048EF70F96775D5ADC * value)
	{
		___m_timer_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_timer_0), value);
	}

	inline static int32_t get_offset_of_m_callback_1() { return static_cast<int32_t>(offsetof(GameSparksTimer_t73F609039E6A360DB023437BF00AA039000F4797, ___m_callback_1)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_m_callback_1() const { return ___m_callback_1; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_m_callback_1() { return &___m_callback_1; }
	inline void set_m_callback_1(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___m_callback_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_callback_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMESPARKSTIMER_T73F609039E6A360DB023437BF00AA039000F4797_H
#ifndef GAMESPARKSUTIL_T157B0A24B339CAF4CB0F1E4C2D1284E26CF3E187_H
#define GAMESPARKSUTIL_T157B0A24B339CAF4CB0F1E4C2D1284E26CF3E187_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Core.GameSparksUtil
struct  GameSparksUtil_t157B0A24B339CAF4CB0F1E4C2D1284E26CF3E187  : public RuntimeObject
{
public:

public:
};

struct GameSparksUtil_t157B0A24B339CAF4CB0F1E4C2D1284E26CF3E187_StaticFields
{
public:
	// System.Action`1<System.String> GameSparks.Core.GameSparksUtil::LogMessageHandler
	Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * ___LogMessageHandler_0;

public:
	inline static int32_t get_offset_of_LogMessageHandler_0() { return static_cast<int32_t>(offsetof(GameSparksUtil_t157B0A24B339CAF4CB0F1E4C2D1284E26CF3E187_StaticFields, ___LogMessageHandler_0)); }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * get_LogMessageHandler_0() const { return ___LogMessageHandler_0; }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 ** get_address_of_LogMessageHandler_0() { return &___LogMessageHandler_0; }
	inline void set_LogMessageHandler_0(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * value)
	{
		___LogMessageHandler_0 = value;
		Il2CppCodeGenWriteBarrier((&___LogMessageHandler_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMESPARKSUTIL_T157B0A24B339CAF4CB0F1E4C2D1284E26CF3E187_H
#ifndef QUEUEREADER_TB32CB4ADFC9AFD3BBB5770452A547A858EC59A86_H
#define QUEUEREADER_TB32CB4ADFC9AFD3BBB5770452A547A858EC59A86_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Core.QueueReader
struct  QueueReader_tB32CB4ADFC9AFD3BBB5770452A547A858EC59A86  : public RuntimeObject
{
public:
	// System.IO.StreamReader GameSparks.Core.QueueReader::sr
	StreamReader_t62E68063760DCD2FC036AE132DE69C24B7ED001E * ___sr_0;
	// System.String GameSparks.Core.QueueReader::fileName
	String_t* ___fileName_1;

public:
	inline static int32_t get_offset_of_sr_0() { return static_cast<int32_t>(offsetof(QueueReader_tB32CB4ADFC9AFD3BBB5770452A547A858EC59A86, ___sr_0)); }
	inline StreamReader_t62E68063760DCD2FC036AE132DE69C24B7ED001E * get_sr_0() const { return ___sr_0; }
	inline StreamReader_t62E68063760DCD2FC036AE132DE69C24B7ED001E ** get_address_of_sr_0() { return &___sr_0; }
	inline void set_sr_0(StreamReader_t62E68063760DCD2FC036AE132DE69C24B7ED001E * value)
	{
		___sr_0 = value;
		Il2CppCodeGenWriteBarrier((&___sr_0), value);
	}

	inline static int32_t get_offset_of_fileName_1() { return static_cast<int32_t>(offsetof(QueueReader_tB32CB4ADFC9AFD3BBB5770452A547A858EC59A86, ___fileName_1)); }
	inline String_t* get_fileName_1() const { return ___fileName_1; }
	inline String_t** get_address_of_fileName_1() { return &___fileName_1; }
	inline void set_fileName_1(String_t* value)
	{
		___fileName_1 = value;
		Il2CppCodeGenWriteBarrier((&___fileName_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUEUEREADER_TB32CB4ADFC9AFD3BBB5770452A547A858EC59A86_H
#ifndef QUEUEWRITER_T0CB01E31FA2FACB2F0760EFA02851B397F4093C1_H
#define QUEUEWRITER_T0CB01E31FA2FACB2F0760EFA02851B397F4093C1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Core.QueueWriter
struct  QueueWriter_t0CB01E31FA2FACB2F0760EFA02851B397F4093C1  : public RuntimeObject
{
public:
	// System.IO.StreamWriter GameSparks.Core.QueueWriter::sw
	StreamWriter_t989B894EF3BFCDF6FF5F5F068402A4F835FC8E8E * ___sw_0;

public:
	inline static int32_t get_offset_of_sw_0() { return static_cast<int32_t>(offsetof(QueueWriter_t0CB01E31FA2FACB2F0760EFA02851B397F4093C1, ___sw_0)); }
	inline StreamWriter_t989B894EF3BFCDF6FF5F5F068402A4F835FC8E8E * get_sw_0() const { return ___sw_0; }
	inline StreamWriter_t989B894EF3BFCDF6FF5F5F068402A4F835FC8E8E ** get_address_of_sw_0() { return &___sw_0; }
	inline void set_sw_0(StreamWriter_t989B894EF3BFCDF6FF5F5F068402A4F835FC8E8E * value)
	{
		___sw_0 = value;
		Il2CppCodeGenWriteBarrier((&___sw_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUEUEWRITER_T0CB01E31FA2FACB2F0760EFA02851B397F4093C1_H
#ifndef GAMESPARKSWEBSOCKET_TE1594ED2F1C86FD1D299356B8C97C68674BF9534_H
#define GAMESPARKSWEBSOCKET_TE1594ED2F1C86FD1D299356B8C97C68674BF9534_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.GameSparksWebSocket
struct  GameSparksWebSocket_tE1594ED2F1C86FD1D299356B8C97C68674BF9534  : public RuntimeObject
{
public:
	// System.Action`1<System.String> GameSparks.GameSparksWebSocket::onMessage
	Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * ___onMessage_0;
	// System.Action`1<System.Byte[]> GameSparks.GameSparksWebSocket::onBinaryMessage
	Action_1_t67D6E8441D0DE589716B25B9D8F0D4412B593398 * ___onBinaryMessage_1;
	// System.Action GameSparks.GameSparksWebSocket::onClose
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___onClose_2;
	// System.Action GameSparks.GameSparksWebSocket::onOpen
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___onOpen_3;
	// System.Action`1<System.String> GameSparks.GameSparksWebSocket::onError
	Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * ___onError_4;
	// WebSocket4Net.WebSocket GameSparks.GameSparksWebSocket::ws
	WebSocket_t231538678AB4B76293D12A78A9D798992897B3BD * ___ws_5;

public:
	inline static int32_t get_offset_of_onMessage_0() { return static_cast<int32_t>(offsetof(GameSparksWebSocket_tE1594ED2F1C86FD1D299356B8C97C68674BF9534, ___onMessage_0)); }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * get_onMessage_0() const { return ___onMessage_0; }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 ** get_address_of_onMessage_0() { return &___onMessage_0; }
	inline void set_onMessage_0(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * value)
	{
		___onMessage_0 = value;
		Il2CppCodeGenWriteBarrier((&___onMessage_0), value);
	}

	inline static int32_t get_offset_of_onBinaryMessage_1() { return static_cast<int32_t>(offsetof(GameSparksWebSocket_tE1594ED2F1C86FD1D299356B8C97C68674BF9534, ___onBinaryMessage_1)); }
	inline Action_1_t67D6E8441D0DE589716B25B9D8F0D4412B593398 * get_onBinaryMessage_1() const { return ___onBinaryMessage_1; }
	inline Action_1_t67D6E8441D0DE589716B25B9D8F0D4412B593398 ** get_address_of_onBinaryMessage_1() { return &___onBinaryMessage_1; }
	inline void set_onBinaryMessage_1(Action_1_t67D6E8441D0DE589716B25B9D8F0D4412B593398 * value)
	{
		___onBinaryMessage_1 = value;
		Il2CppCodeGenWriteBarrier((&___onBinaryMessage_1), value);
	}

	inline static int32_t get_offset_of_onClose_2() { return static_cast<int32_t>(offsetof(GameSparksWebSocket_tE1594ED2F1C86FD1D299356B8C97C68674BF9534, ___onClose_2)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_onClose_2() const { return ___onClose_2; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_onClose_2() { return &___onClose_2; }
	inline void set_onClose_2(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___onClose_2 = value;
		Il2CppCodeGenWriteBarrier((&___onClose_2), value);
	}

	inline static int32_t get_offset_of_onOpen_3() { return static_cast<int32_t>(offsetof(GameSparksWebSocket_tE1594ED2F1C86FD1D299356B8C97C68674BF9534, ___onOpen_3)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_onOpen_3() const { return ___onOpen_3; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_onOpen_3() { return &___onOpen_3; }
	inline void set_onOpen_3(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___onOpen_3 = value;
		Il2CppCodeGenWriteBarrier((&___onOpen_3), value);
	}

	inline static int32_t get_offset_of_onError_4() { return static_cast<int32_t>(offsetof(GameSparksWebSocket_tE1594ED2F1C86FD1D299356B8C97C68674BF9534, ___onError_4)); }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * get_onError_4() const { return ___onError_4; }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 ** get_address_of_onError_4() { return &___onError_4; }
	inline void set_onError_4(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * value)
	{
		___onError_4 = value;
		Il2CppCodeGenWriteBarrier((&___onError_4), value);
	}

	inline static int32_t get_offset_of_ws_5() { return static_cast<int32_t>(offsetof(GameSparksWebSocket_tE1594ED2F1C86FD1D299356B8C97C68674BF9534, ___ws_5)); }
	inline WebSocket_t231538678AB4B76293D12A78A9D798992897B3BD * get_ws_5() const { return ___ws_5; }
	inline WebSocket_t231538678AB4B76293D12A78A9D798992897B3BD ** get_address_of_ws_5() { return &___ws_5; }
	inline void set_ws_5(WebSocket_t231538678AB4B76293D12A78A9D798992897B3BD * value)
	{
		___ws_5 = value;
		Il2CppCodeGenWriteBarrier((&___ws_5), value);
	}
};

struct GameSparksWebSocket_tE1594ED2F1C86FD1D299356B8C97C68674BF9534_StaticFields
{
public:
	// System.Net.EndPoint GameSparks.GameSparksWebSocket::<Proxy>k__BackingField
	EndPoint_tD87FCEF2780A951E8CE8D808C345FBF2C088D980 * ___U3CProxyU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CProxyU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(GameSparksWebSocket_tE1594ED2F1C86FD1D299356B8C97C68674BF9534_StaticFields, ___U3CProxyU3Ek__BackingField_6)); }
	inline EndPoint_tD87FCEF2780A951E8CE8D808C345FBF2C088D980 * get_U3CProxyU3Ek__BackingField_6() const { return ___U3CProxyU3Ek__BackingField_6; }
	inline EndPoint_tD87FCEF2780A951E8CE8D808C345FBF2C088D980 ** get_address_of_U3CProxyU3Ek__BackingField_6() { return &___U3CProxyU3Ek__BackingField_6; }
	inline void set_U3CProxyU3Ek__BackingField_6(EndPoint_tD87FCEF2780A951E8CE8D808C345FBF2C088D980 * value)
	{
		___U3CProxyU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CProxyU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMESPARKSWEBSOCKET_TE1594ED2F1C86FD1D299356B8C97C68674BF9534_H
#ifndef GSTLSAUTHENTICATION_TC82EA27062C5D8E4C93250F7CB58A4A08B90108E_H
#define GSTLSAUTHENTICATION_TC82EA27062C5D8E4C93250F7CB58A4A08B90108E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.RT.GSTlsAuthentication
struct  GSTlsAuthentication_tC82EA27062C5D8E4C93250F7CB58A4A08B90108E  : public RuntimeObject
{
public:
	// System.Collections.IList GameSparks.RT.GSTlsAuthentication::validCertNames
	RuntimeObject* ___validCertNames_1;

public:
	inline static int32_t get_offset_of_validCertNames_1() { return static_cast<int32_t>(offsetof(GSTlsAuthentication_tC82EA27062C5D8E4C93250F7CB58A4A08B90108E, ___validCertNames_1)); }
	inline RuntimeObject* get_validCertNames_1() const { return ___validCertNames_1; }
	inline RuntimeObject** get_address_of_validCertNames_1() { return &___validCertNames_1; }
	inline void set_validCertNames_1(RuntimeObject* value)
	{
		___validCertNames_1 = value;
		Il2CppCodeGenWriteBarrier((&___validCertNames_1), value);
	}
};

struct GSTlsAuthentication_tC82EA27062C5D8E4C93250F7CB58A4A08B90108E_StaticFields
{
public:
	// Org.BouncyCastle.X509.X509Certificate GameSparks.RT.GSTlsAuthentication::rootCert
	X509Certificate_t9350C69721F95FA663C0FD6ED97CDF09B8037786 * ___rootCert_0;

public:
	inline static int32_t get_offset_of_rootCert_0() { return static_cast<int32_t>(offsetof(GSTlsAuthentication_tC82EA27062C5D8E4C93250F7CB58A4A08B90108E_StaticFields, ___rootCert_0)); }
	inline X509Certificate_t9350C69721F95FA663C0FD6ED97CDF09B8037786 * get_rootCert_0() const { return ___rootCert_0; }
	inline X509Certificate_t9350C69721F95FA663C0FD6ED97CDF09B8037786 ** get_address_of_rootCert_0() { return &___rootCert_0; }
	inline void set_rootCert_0(X509Certificate_t9350C69721F95FA663C0FD6ED97CDF09B8037786 * value)
	{
		___rootCert_0 = value;
		Il2CppCodeGenWriteBarrier((&___rootCert_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GSTLSAUTHENTICATION_TC82EA27062C5D8E4C93250F7CB58A4A08B90108E_H
#ifndef GSTLSVERIFYCERTIFICATE_T4CDE88F435AAEEBBE8DD7B4316D3C207DC83CE1A_H
#define GSTLSVERIFYCERTIFICATE_T4CDE88F435AAEEBBE8DD7B4316D3C207DC83CE1A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.RT.GSTlsVerifyCertificate
struct  GSTlsVerifyCertificate_t4CDE88F435AAEEBBE8DD7B4316D3C207DC83CE1A  : public RuntimeObject
{
public:

public:
};

struct GSTlsVerifyCertificate_t4CDE88F435AAEEBBE8DD7B4316D3C207DC83CE1A_StaticFields
{
public:
	// System.Func`2<Org.BouncyCastle.Crypto.Tls.Certificate,System.String> GameSparks.RT.GSTlsVerifyCertificate::<OnVerifyCertificate>k__BackingField
	Func_2_t81AB5F1E16530F25286C7FCBA77049466BE9C90A * ___U3COnVerifyCertificateU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3COnVerifyCertificateU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(GSTlsVerifyCertificate_t4CDE88F435AAEEBBE8DD7B4316D3C207DC83CE1A_StaticFields, ___U3COnVerifyCertificateU3Ek__BackingField_0)); }
	inline Func_2_t81AB5F1E16530F25286C7FCBA77049466BE9C90A * get_U3COnVerifyCertificateU3Ek__BackingField_0() const { return ___U3COnVerifyCertificateU3Ek__BackingField_0; }
	inline Func_2_t81AB5F1E16530F25286C7FCBA77049466BE9C90A ** get_address_of_U3COnVerifyCertificateU3Ek__BackingField_0() { return &___U3COnVerifyCertificateU3Ek__BackingField_0; }
	inline void set_U3COnVerifyCertificateU3Ek__BackingField_0(Func_2_t81AB5F1E16530F25286C7FCBA77049466BE9C90A * value)
	{
		___U3COnVerifyCertificateU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3COnVerifyCertificateU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GSTLSVERIFYCERTIFICATE_T4CDE88F435AAEEBBE8DD7B4316D3C207DC83CE1A_H
#ifndef ABSTRACTTLSPEER_T26CA3EF032C693BEB7331E2A0972A8620596CC9F_H
#define ABSTRACTTLSPEER_T26CA3EF032C693BEB7331E2A0972A8620596CC9F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.AbstractTlsPeer
struct  AbstractTlsPeer_t26CA3EF032C693BEB7331E2A0972A8620596CC9F  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTTLSPEER_T26CA3EF032C693BEB7331E2A0972A8620596CC9F_H
#ifndef CONNECTASYNCEXTENSION_TE679A81CC66C20E643FD289C2C64B55148FBB653_H
#define CONNECTASYNCEXTENSION_TE679A81CC66C20E643FD289C2C64B55148FBB653_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SuperSocket.ClientEngine.ConnectAsyncExtension
struct  ConnectAsyncExtension_tE679A81CC66C20E643FD289C2C64B55148FBB653  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONNECTASYNCEXTENSION_TE679A81CC66C20E643FD289C2C64B55148FBB653_H
#ifndef CONNECTTOKEN_T86B48BA76FA4374492AA67EF9DD1EB7E162AE45A_H
#define CONNECTTOKEN_T86B48BA76FA4374492AA67EF9DD1EB7E162AE45A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SuperSocket.ClientEngine.ConnectAsyncExtension_ConnectToken
struct  ConnectToken_t86B48BA76FA4374492AA67EF9DD1EB7E162AE45A  : public RuntimeObject
{
public:
	// System.Object SuperSocket.ClientEngine.ConnectAsyncExtension_ConnectToken::<State>k__BackingField
	RuntimeObject * ___U3CStateU3Ek__BackingField_0;
	// SuperSocket.ClientEngine.ConnectedCallback SuperSocket.ClientEngine.ConnectAsyncExtension_ConnectToken::<Callback>k__BackingField
	ConnectedCallback_t27F6846DBFF78119A3FB59CB3C577CFB61BD2D75 * ___U3CCallbackU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CStateU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ConnectToken_t86B48BA76FA4374492AA67EF9DD1EB7E162AE45A, ___U3CStateU3Ek__BackingField_0)); }
	inline RuntimeObject * get_U3CStateU3Ek__BackingField_0() const { return ___U3CStateU3Ek__BackingField_0; }
	inline RuntimeObject ** get_address_of_U3CStateU3Ek__BackingField_0() { return &___U3CStateU3Ek__BackingField_0; }
	inline void set_U3CStateU3Ek__BackingField_0(RuntimeObject * value)
	{
		___U3CStateU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CStateU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ConnectToken_t86B48BA76FA4374492AA67EF9DD1EB7E162AE45A, ___U3CCallbackU3Ek__BackingField_1)); }
	inline ConnectedCallback_t27F6846DBFF78119A3FB59CB3C577CFB61BD2D75 * get_U3CCallbackU3Ek__BackingField_1() const { return ___U3CCallbackU3Ek__BackingField_1; }
	inline ConnectedCallback_t27F6846DBFF78119A3FB59CB3C577CFB61BD2D75 ** get_address_of_U3CCallbackU3Ek__BackingField_1() { return &___U3CCallbackU3Ek__BackingField_1; }
	inline void set_U3CCallbackU3Ek__BackingField_1(ConnectedCallback_t27F6846DBFF78119A3FB59CB3C577CFB61BD2D75 * value)
	{
		___U3CCallbackU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONNECTTOKEN_T86B48BA76FA4374492AA67EF9DD1EB7E162AE45A_H
#ifndef DNSCONNECTSTATE_TE731E1C5E763BCFBDD6AB16F53EBBB8CEF7F1348_H
#define DNSCONNECTSTATE_TE731E1C5E763BCFBDD6AB16F53EBBB8CEF7F1348_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SuperSocket.ClientEngine.ConnectAsyncExtension_DnsConnectState
struct  DnsConnectState_tE731E1C5E763BCFBDD6AB16F53EBBB8CEF7F1348  : public RuntimeObject
{
public:
	// System.Net.IPAddress[] SuperSocket.ClientEngine.ConnectAsyncExtension_DnsConnectState::<Addresses>k__BackingField
	IPAddressU5BU5D_t7F25C4C038C43BFDA8EA84969112E82BADC38BC3* ___U3CAddressesU3Ek__BackingField_0;
	// System.Int32 SuperSocket.ClientEngine.ConnectAsyncExtension_DnsConnectState::<NextAddressIndex>k__BackingField
	int32_t ___U3CNextAddressIndexU3Ek__BackingField_1;
	// System.Int32 SuperSocket.ClientEngine.ConnectAsyncExtension_DnsConnectState::<Port>k__BackingField
	int32_t ___U3CPortU3Ek__BackingField_2;
	// System.Net.Sockets.Socket SuperSocket.ClientEngine.ConnectAsyncExtension_DnsConnectState::<Socket4>k__BackingField
	Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 * ___U3CSocket4U3Ek__BackingField_3;
	// System.Net.Sockets.Socket SuperSocket.ClientEngine.ConnectAsyncExtension_DnsConnectState::<Socket6>k__BackingField
	Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 * ___U3CSocket6U3Ek__BackingField_4;
	// System.Object SuperSocket.ClientEngine.ConnectAsyncExtension_DnsConnectState::<State>k__BackingField
	RuntimeObject * ___U3CStateU3Ek__BackingField_5;
	// SuperSocket.ClientEngine.ConnectedCallback SuperSocket.ClientEngine.ConnectAsyncExtension_DnsConnectState::<Callback>k__BackingField
	ConnectedCallback_t27F6846DBFF78119A3FB59CB3C577CFB61BD2D75 * ___U3CCallbackU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CAddressesU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DnsConnectState_tE731E1C5E763BCFBDD6AB16F53EBBB8CEF7F1348, ___U3CAddressesU3Ek__BackingField_0)); }
	inline IPAddressU5BU5D_t7F25C4C038C43BFDA8EA84969112E82BADC38BC3* get_U3CAddressesU3Ek__BackingField_0() const { return ___U3CAddressesU3Ek__BackingField_0; }
	inline IPAddressU5BU5D_t7F25C4C038C43BFDA8EA84969112E82BADC38BC3** get_address_of_U3CAddressesU3Ek__BackingField_0() { return &___U3CAddressesU3Ek__BackingField_0; }
	inline void set_U3CAddressesU3Ek__BackingField_0(IPAddressU5BU5D_t7F25C4C038C43BFDA8EA84969112E82BADC38BC3* value)
	{
		___U3CAddressesU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAddressesU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CNextAddressIndexU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(DnsConnectState_tE731E1C5E763BCFBDD6AB16F53EBBB8CEF7F1348, ___U3CNextAddressIndexU3Ek__BackingField_1)); }
	inline int32_t get_U3CNextAddressIndexU3Ek__BackingField_1() const { return ___U3CNextAddressIndexU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CNextAddressIndexU3Ek__BackingField_1() { return &___U3CNextAddressIndexU3Ek__BackingField_1; }
	inline void set_U3CNextAddressIndexU3Ek__BackingField_1(int32_t value)
	{
		___U3CNextAddressIndexU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CPortU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(DnsConnectState_tE731E1C5E763BCFBDD6AB16F53EBBB8CEF7F1348, ___U3CPortU3Ek__BackingField_2)); }
	inline int32_t get_U3CPortU3Ek__BackingField_2() const { return ___U3CPortU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CPortU3Ek__BackingField_2() { return &___U3CPortU3Ek__BackingField_2; }
	inline void set_U3CPortU3Ek__BackingField_2(int32_t value)
	{
		___U3CPortU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CSocket4U3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(DnsConnectState_tE731E1C5E763BCFBDD6AB16F53EBBB8CEF7F1348, ___U3CSocket4U3Ek__BackingField_3)); }
	inline Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 * get_U3CSocket4U3Ek__BackingField_3() const { return ___U3CSocket4U3Ek__BackingField_3; }
	inline Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 ** get_address_of_U3CSocket4U3Ek__BackingField_3() { return &___U3CSocket4U3Ek__BackingField_3; }
	inline void set_U3CSocket4U3Ek__BackingField_3(Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 * value)
	{
		___U3CSocket4U3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSocket4U3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CSocket6U3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(DnsConnectState_tE731E1C5E763BCFBDD6AB16F53EBBB8CEF7F1348, ___U3CSocket6U3Ek__BackingField_4)); }
	inline Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 * get_U3CSocket6U3Ek__BackingField_4() const { return ___U3CSocket6U3Ek__BackingField_4; }
	inline Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 ** get_address_of_U3CSocket6U3Ek__BackingField_4() { return &___U3CSocket6U3Ek__BackingField_4; }
	inline void set_U3CSocket6U3Ek__BackingField_4(Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 * value)
	{
		___U3CSocket6U3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSocket6U3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CStateU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(DnsConnectState_tE731E1C5E763BCFBDD6AB16F53EBBB8CEF7F1348, ___U3CStateU3Ek__BackingField_5)); }
	inline RuntimeObject * get_U3CStateU3Ek__BackingField_5() const { return ___U3CStateU3Ek__BackingField_5; }
	inline RuntimeObject ** get_address_of_U3CStateU3Ek__BackingField_5() { return &___U3CStateU3Ek__BackingField_5; }
	inline void set_U3CStateU3Ek__BackingField_5(RuntimeObject * value)
	{
		___U3CStateU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CStateU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(DnsConnectState_tE731E1C5E763BCFBDD6AB16F53EBBB8CEF7F1348, ___U3CCallbackU3Ek__BackingField_6)); }
	inline ConnectedCallback_t27F6846DBFF78119A3FB59CB3C577CFB61BD2D75 * get_U3CCallbackU3Ek__BackingField_6() const { return ___U3CCallbackU3Ek__BackingField_6; }
	inline ConnectedCallback_t27F6846DBFF78119A3FB59CB3C577CFB61BD2D75 ** get_address_of_U3CCallbackU3Ek__BackingField_6() { return &___U3CCallbackU3Ek__BackingField_6; }
	inline void set_U3CCallbackU3Ek__BackingField_6(ConnectedCallback_t27F6846DBFF78119A3FB59CB3C577CFB61BD2D75 * value)
	{
		___U3CCallbackU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DNSCONNECTSTATE_TE731E1C5E763BCFBDD6AB16F53EBBB8CEF7F1348_H
#ifndef EXTENSIONS_TDC0399F167916AA28EC96A1E44D4A468D856259D_H
#define EXTENSIONS_TDC0399F167916AA28EC96A1E44D4A468D856259D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SuperSocket.ClientEngine.Extensions
struct  Extensions_tDC0399F167916AA28EC96A1E44D4A468D856259D  : public RuntimeObject
{
public:

public:
};

struct Extensions_tDC0399F167916AA28EC96A1E44D4A468D856259D_StaticFields
{
public:
	// System.Random SuperSocket.ClientEngine.Extensions::m_Random
	Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F * ___m_Random_0;

public:
	inline static int32_t get_offset_of_m_Random_0() { return static_cast<int32_t>(offsetof(Extensions_tDC0399F167916AA28EC96A1E44D4A468D856259D_StaticFields, ___m_Random_0)); }
	inline Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F * get_m_Random_0() const { return ___m_Random_0; }
	inline Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F ** get_address_of_m_Random_0() { return &___m_Random_0; }
	inline void set_m_Random_0(Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F * value)
	{
		___m_Random_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Random_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENSIONS_TDC0399F167916AA28EC96A1E44D4A468D856259D_H
#ifndef ARRAYSEGMENTLIST_1_T967C28DFCD37B71848E14923C7F59D43D7DB33A6_H
#define ARRAYSEGMENTLIST_1_T967C28DFCD37B71848E14923C7F59D43D7DB33A6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SuperSocket.ClientEngine.Protocol.ArraySegmentList`1<System.Byte>
struct  ArraySegmentList_1_t967C28DFCD37B71848E14923C7F59D43D7DB33A6  : public RuntimeObject
{
public:
	// System.Collections.Generic.IList`1<SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<T>> SuperSocket.ClientEngine.Protocol.ArraySegmentList`1::m_Segments
	RuntimeObject* ___m_Segments_0;
	// SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<T> SuperSocket.ClientEngine.Protocol.ArraySegmentList`1::m_PrevSegment
	ArraySegmentEx_1_tAE28209676C8724BA42ACC7CC56900802D94A54E * ___m_PrevSegment_1;
	// System.Int32 SuperSocket.ClientEngine.Protocol.ArraySegmentList`1::m_PrevSegmentIndex
	int32_t ___m_PrevSegmentIndex_2;
	// System.Int32 SuperSocket.ClientEngine.Protocol.ArraySegmentList`1::m_Count
	int32_t ___m_Count_3;

public:
	inline static int32_t get_offset_of_m_Segments_0() { return static_cast<int32_t>(offsetof(ArraySegmentList_1_t967C28DFCD37B71848E14923C7F59D43D7DB33A6, ___m_Segments_0)); }
	inline RuntimeObject* get_m_Segments_0() const { return ___m_Segments_0; }
	inline RuntimeObject** get_address_of_m_Segments_0() { return &___m_Segments_0; }
	inline void set_m_Segments_0(RuntimeObject* value)
	{
		___m_Segments_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Segments_0), value);
	}

	inline static int32_t get_offset_of_m_PrevSegment_1() { return static_cast<int32_t>(offsetof(ArraySegmentList_1_t967C28DFCD37B71848E14923C7F59D43D7DB33A6, ___m_PrevSegment_1)); }
	inline ArraySegmentEx_1_tAE28209676C8724BA42ACC7CC56900802D94A54E * get_m_PrevSegment_1() const { return ___m_PrevSegment_1; }
	inline ArraySegmentEx_1_tAE28209676C8724BA42ACC7CC56900802D94A54E ** get_address_of_m_PrevSegment_1() { return &___m_PrevSegment_1; }
	inline void set_m_PrevSegment_1(ArraySegmentEx_1_tAE28209676C8724BA42ACC7CC56900802D94A54E * value)
	{
		___m_PrevSegment_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PrevSegment_1), value);
	}

	inline static int32_t get_offset_of_m_PrevSegmentIndex_2() { return static_cast<int32_t>(offsetof(ArraySegmentList_1_t967C28DFCD37B71848E14923C7F59D43D7DB33A6, ___m_PrevSegmentIndex_2)); }
	inline int32_t get_m_PrevSegmentIndex_2() const { return ___m_PrevSegmentIndex_2; }
	inline int32_t* get_address_of_m_PrevSegmentIndex_2() { return &___m_PrevSegmentIndex_2; }
	inline void set_m_PrevSegmentIndex_2(int32_t value)
	{
		___m_PrevSegmentIndex_2 = value;
	}

	inline static int32_t get_offset_of_m_Count_3() { return static_cast<int32_t>(offsetof(ArraySegmentList_1_t967C28DFCD37B71848E14923C7F59D43D7DB33A6, ___m_Count_3)); }
	inline int32_t get_m_Count_3() const { return ___m_Count_3; }
	inline int32_t* get_address_of_m_Count_3() { return &___m_Count_3; }
	inline void set_m_Count_3(int32_t value)
	{
		___m_Count_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRAYSEGMENTLIST_1_T967C28DFCD37B71848E14923C7F59D43D7DB33A6_H
#ifndef CONNECTCONTEXT_T733BBB94497088F90C3FA43A78F9E172FA4406C5_H
#define CONNECTCONTEXT_T733BBB94497088F90C3FA43A78F9E172FA4406C5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SuperSocket.ClientEngine.Proxy.HttpConnectProxy_ConnectContext
struct  ConnectContext_t733BBB94497088F90C3FA43A78F9E172FA4406C5  : public RuntimeObject
{
public:
	// System.Net.Sockets.Socket SuperSocket.ClientEngine.Proxy.HttpConnectProxy_ConnectContext::<Socket>k__BackingField
	Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 * ___U3CSocketU3Ek__BackingField_0;
	// SuperSocket.ClientEngine.SearchMarkState`1<System.Byte> SuperSocket.ClientEngine.Proxy.HttpConnectProxy_ConnectContext::<SearchState>k__BackingField
	SearchMarkState_1_t9CA9B71862282152C0C864021BBEE57C176B07D3 * ___U3CSearchStateU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CSocketU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ConnectContext_t733BBB94497088F90C3FA43A78F9E172FA4406C5, ___U3CSocketU3Ek__BackingField_0)); }
	inline Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 * get_U3CSocketU3Ek__BackingField_0() const { return ___U3CSocketU3Ek__BackingField_0; }
	inline Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 ** get_address_of_U3CSocketU3Ek__BackingField_0() { return &___U3CSocketU3Ek__BackingField_0; }
	inline void set_U3CSocketU3Ek__BackingField_0(Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 * value)
	{
		___U3CSocketU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSocketU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CSearchStateU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ConnectContext_t733BBB94497088F90C3FA43A78F9E172FA4406C5, ___U3CSearchStateU3Ek__BackingField_1)); }
	inline SearchMarkState_1_t9CA9B71862282152C0C864021BBEE57C176B07D3 * get_U3CSearchStateU3Ek__BackingField_1() const { return ___U3CSearchStateU3Ek__BackingField_1; }
	inline SearchMarkState_1_t9CA9B71862282152C0C864021BBEE57C176B07D3 ** get_address_of_U3CSearchStateU3Ek__BackingField_1() { return &___U3CSearchStateU3Ek__BackingField_1; }
	inline void set_U3CSearchStateU3Ek__BackingField_1(SearchMarkState_1_t9CA9B71862282152C0C864021BBEE57C176B07D3 * value)
	{
		___U3CSearchStateU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSearchStateU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONNECTCONTEXT_T733BBB94497088F90C3FA43A78F9E172FA4406C5_H
#ifndef PROXYCONNECTORBASE_T66DBD023064F63BFABF4E28901CEF3C83BC92A09_H
#define PROXYCONNECTORBASE_T66DBD023064F63BFABF4E28901CEF3C83BC92A09_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SuperSocket.ClientEngine.Proxy.ProxyConnectorBase
struct  ProxyConnectorBase_t66DBD023064F63BFABF4E28901CEF3C83BC92A09  : public RuntimeObject
{
public:
	// System.EventHandler`1<SuperSocket.ClientEngine.ProxyEventArgs> SuperSocket.ClientEngine.Proxy.ProxyConnectorBase::m_Completed
	EventHandler_1_t671742AB0AECF62E29BC8F5C2FD957D3ED1B2C7A * ___m_Completed_1;
	// System.Net.EndPoint SuperSocket.ClientEngine.Proxy.ProxyConnectorBase::<ProxyEndPoint>k__BackingField
	EndPoint_tD87FCEF2780A951E8CE8D808C345FBF2C088D980 * ___U3CProxyEndPointU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_m_Completed_1() { return static_cast<int32_t>(offsetof(ProxyConnectorBase_t66DBD023064F63BFABF4E28901CEF3C83BC92A09, ___m_Completed_1)); }
	inline EventHandler_1_t671742AB0AECF62E29BC8F5C2FD957D3ED1B2C7A * get_m_Completed_1() const { return ___m_Completed_1; }
	inline EventHandler_1_t671742AB0AECF62E29BC8F5C2FD957D3ED1B2C7A ** get_address_of_m_Completed_1() { return &___m_Completed_1; }
	inline void set_m_Completed_1(EventHandler_1_t671742AB0AECF62E29BC8F5C2FD957D3ED1B2C7A * value)
	{
		___m_Completed_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Completed_1), value);
	}

	inline static int32_t get_offset_of_U3CProxyEndPointU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ProxyConnectorBase_t66DBD023064F63BFABF4E28901CEF3C83BC92A09, ___U3CProxyEndPointU3Ek__BackingField_2)); }
	inline EndPoint_tD87FCEF2780A951E8CE8D808C345FBF2C088D980 * get_U3CProxyEndPointU3Ek__BackingField_2() const { return ___U3CProxyEndPointU3Ek__BackingField_2; }
	inline EndPoint_tD87FCEF2780A951E8CE8D808C345FBF2C088D980 ** get_address_of_U3CProxyEndPointU3Ek__BackingField_2() { return &___U3CProxyEndPointU3Ek__BackingField_2; }
	inline void set_U3CProxyEndPointU3Ek__BackingField_2(EndPoint_tD87FCEF2780A951E8CE8D808C345FBF2C088D980 * value)
	{
		___U3CProxyEndPointU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CProxyEndPointU3Ek__BackingField_2), value);
	}
};

struct ProxyConnectorBase_t66DBD023064F63BFABF4E28901CEF3C83BC92A09_StaticFields
{
public:
	// System.Text.Encoding SuperSocket.ClientEngine.Proxy.ProxyConnectorBase::ASCIIEncoding
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___ASCIIEncoding_0;

public:
	inline static int32_t get_offset_of_ASCIIEncoding_0() { return static_cast<int32_t>(offsetof(ProxyConnectorBase_t66DBD023064F63BFABF4E28901CEF3C83BC92A09_StaticFields, ___ASCIIEncoding_0)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get_ASCIIEncoding_0() const { return ___ASCIIEncoding_0; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of_ASCIIEncoding_0() { return &___ASCIIEncoding_0; }
	inline void set_ASCIIEncoding_0(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		___ASCIIEncoding_0 = value;
		Il2CppCodeGenWriteBarrier((&___ASCIIEncoding_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROXYCONNECTORBASE_T66DBD023064F63BFABF4E28901CEF3C83BC92A09_H
#ifndef SSLASYNCSTATE_T60F1683B2FD929CDBF0E40BAEFD4F3A34A2D9D0E_H
#define SSLASYNCSTATE_T60F1683B2FD929CDBF0E40BAEFD4F3A34A2D9D0E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SuperSocket.ClientEngine.SslStreamTcpSession_SslAsyncState
struct  SslAsyncState_t60F1683B2FD929CDBF0E40BAEFD4F3A34A2D9D0E  : public RuntimeObject
{
public:
	// System.IO.Stream SuperSocket.ClientEngine.SslStreamTcpSession_SslAsyncState::<SslStream>k__BackingField
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___U3CSslStreamU3Ek__BackingField_0;
	// System.Net.Sockets.Socket SuperSocket.ClientEngine.SslStreamTcpSession_SslAsyncState::<Client>k__BackingField
	Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 * ___U3CClientU3Ek__BackingField_1;
	// SuperSocket.ClientEngine.PosList`1<System.ArraySegment`1<System.Byte>> SuperSocket.ClientEngine.SslStreamTcpSession_SslAsyncState::<SendingItems>k__BackingField
	PosList_1_tF22B6140877DA647665E0F81051342E9906F4098 * ___U3CSendingItemsU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CSslStreamU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(SslAsyncState_t60F1683B2FD929CDBF0E40BAEFD4F3A34A2D9D0E, ___U3CSslStreamU3Ek__BackingField_0)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_U3CSslStreamU3Ek__BackingField_0() const { return ___U3CSslStreamU3Ek__BackingField_0; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_U3CSslStreamU3Ek__BackingField_0() { return &___U3CSslStreamU3Ek__BackingField_0; }
	inline void set_U3CSslStreamU3Ek__BackingField_0(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___U3CSslStreamU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSslStreamU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CClientU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(SslAsyncState_t60F1683B2FD929CDBF0E40BAEFD4F3A34A2D9D0E, ___U3CClientU3Ek__BackingField_1)); }
	inline Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 * get_U3CClientU3Ek__BackingField_1() const { return ___U3CClientU3Ek__BackingField_1; }
	inline Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 ** get_address_of_U3CClientU3Ek__BackingField_1() { return &___U3CClientU3Ek__BackingField_1; }
	inline void set_U3CClientU3Ek__BackingField_1(Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 * value)
	{
		___U3CClientU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CClientU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CSendingItemsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(SslAsyncState_t60F1683B2FD929CDBF0E40BAEFD4F3A34A2D9D0E, ___U3CSendingItemsU3Ek__BackingField_2)); }
	inline PosList_1_tF22B6140877DA647665E0F81051342E9906F4098 * get_U3CSendingItemsU3Ek__BackingField_2() const { return ___U3CSendingItemsU3Ek__BackingField_2; }
	inline PosList_1_tF22B6140877DA647665E0F81051342E9906F4098 ** get_address_of_U3CSendingItemsU3Ek__BackingField_2() { return &___U3CSendingItemsU3Ek__BackingField_2; }
	inline void set_U3CSendingItemsU3Ek__BackingField_2(PosList_1_tF22B6140877DA647665E0F81051342E9906F4098 * value)
	{
		___U3CSendingItemsU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSendingItemsU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SSLASYNCSTATE_T60F1683B2FD929CDBF0E40BAEFD4F3A34A2D9D0E_H
#ifndef EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#define EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventArgs
struct  EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E  : public RuntimeObject
{
public:

public:
};

struct EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields, ___Empty_0)); }
	inline EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#ifndef MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#define MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MarshalByRefObject
struct  MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF  : public RuntimeObject
{
public:
	// System.Object System.MarshalByRefObject::_identity
	RuntimeObject * ____identity_0;

public:
	inline static int32_t get_offset_of__identity_0() { return static_cast<int32_t>(offsetof(MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF, ____identity_0)); }
	inline RuntimeObject * get__identity_0() const { return ____identity_0; }
	inline RuntimeObject ** get_address_of__identity_0() { return &____identity_0; }
	inline void set__identity_0(RuntimeObject * value)
	{
		____identity_0 = value;
		Il2CppCodeGenWriteBarrier((&____identity_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF_marshaled_pinvoke
{
	Il2CppIUnknown* ____identity_0;
};
// Native definition for COM marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF_marshaled_com
{
	Il2CppIUnknown* ____identity_0;
};
#endif // MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#ifndef ENDPOINT_TD87FCEF2780A951E8CE8D808C345FBF2C088D980_H
#define ENDPOINT_TD87FCEF2780A951E8CE8D808C345FBF2C088D980_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.EndPoint
struct  EndPoint_tD87FCEF2780A951E8CE8D808C345FBF2C088D980  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENDPOINT_TD87FCEF2780A951E8CE8D808C345FBF2C088D980_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef NAMESERIALIZER_T7C4F2D6B7D9A8EF7996BB9C613A3AF6243E85D2A_H
#define NAMESERIALIZER_T7C4F2D6B7D9A8EF7996BB9C613A3AF6243E85D2A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Linq.NameSerializer
struct  NameSerializer_t7C4F2D6B7D9A8EF7996BB9C613A3AF6243E85D2A  : public RuntimeObject
{
public:
	// System.String System.Xml.Linq.NameSerializer::expandedName
	String_t* ___expandedName_0;

public:
	inline static int32_t get_offset_of_expandedName_0() { return static_cast<int32_t>(offsetof(NameSerializer_t7C4F2D6B7D9A8EF7996BB9C613A3AF6243E85D2A, ___expandedName_0)); }
	inline String_t* get_expandedName_0() const { return ___expandedName_0; }
	inline String_t** get_address_of_expandedName_0() { return &___expandedName_0; }
	inline void set_expandedName_0(String_t* value)
	{
		___expandedName_0 = value;
		Il2CppCodeGenWriteBarrier((&___expandedName_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMESERIALIZER_T7C4F2D6B7D9A8EF7996BB9C613A3AF6243E85D2A_H
#ifndef XNAME_T09FB17CAE73454416CC2CC694E68A9AFFD3C8F7A_H
#define XNAME_T09FB17CAE73454416CC2CC694E68A9AFFD3C8F7A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Linq.XName
struct  XName_t09FB17CAE73454416CC2CC694E68A9AFFD3C8F7A  : public RuntimeObject
{
public:
	// System.Xml.Linq.XNamespace System.Xml.Linq.XName::ns
	XNamespace_t771F5C81A42B6DFC65678BB24EE414F8F9E7831D * ___ns_0;
	// System.String System.Xml.Linq.XName::localName
	String_t* ___localName_1;
	// System.Int32 System.Xml.Linq.XName::hashCode
	int32_t ___hashCode_2;

public:
	inline static int32_t get_offset_of_ns_0() { return static_cast<int32_t>(offsetof(XName_t09FB17CAE73454416CC2CC694E68A9AFFD3C8F7A, ___ns_0)); }
	inline XNamespace_t771F5C81A42B6DFC65678BB24EE414F8F9E7831D * get_ns_0() const { return ___ns_0; }
	inline XNamespace_t771F5C81A42B6DFC65678BB24EE414F8F9E7831D ** get_address_of_ns_0() { return &___ns_0; }
	inline void set_ns_0(XNamespace_t771F5C81A42B6DFC65678BB24EE414F8F9E7831D * value)
	{
		___ns_0 = value;
		Il2CppCodeGenWriteBarrier((&___ns_0), value);
	}

	inline static int32_t get_offset_of_localName_1() { return static_cast<int32_t>(offsetof(XName_t09FB17CAE73454416CC2CC694E68A9AFFD3C8F7A, ___localName_1)); }
	inline String_t* get_localName_1() const { return ___localName_1; }
	inline String_t** get_address_of_localName_1() { return &___localName_1; }
	inline void set_localName_1(String_t* value)
	{
		___localName_1 = value;
		Il2CppCodeGenWriteBarrier((&___localName_1), value);
	}

	inline static int32_t get_offset_of_hashCode_2() { return static_cast<int32_t>(offsetof(XName_t09FB17CAE73454416CC2CC694E68A9AFFD3C8F7A, ___hashCode_2)); }
	inline int32_t get_hashCode_2() const { return ___hashCode_2; }
	inline int32_t* get_address_of_hashCode_2() { return &___hashCode_2; }
	inline void set_hashCode_2(int32_t value)
	{
		___hashCode_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XNAME_T09FB17CAE73454416CC2CC694E68A9AFFD3C8F7A_H
#ifndef XNAMESPACE_T771F5C81A42B6DFC65678BB24EE414F8F9E7831D_H
#define XNAMESPACE_T771F5C81A42B6DFC65678BB24EE414F8F9E7831D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Linq.XNamespace
struct  XNamespace_t771F5C81A42B6DFC65678BB24EE414F8F9E7831D  : public RuntimeObject
{
public:
	// System.String System.Xml.Linq.XNamespace::namespaceName
	String_t* ___namespaceName_4;
	// System.Int32 System.Xml.Linq.XNamespace::hashCode
	int32_t ___hashCode_5;
	// System.Xml.Linq.XHashtable`1<System.Xml.Linq.XName> System.Xml.Linq.XNamespace::names
	XHashtable_1_tD5FCDADAB0E25D7F54936D993C541EF22988B4E1 * ___names_6;

public:
	inline static int32_t get_offset_of_namespaceName_4() { return static_cast<int32_t>(offsetof(XNamespace_t771F5C81A42B6DFC65678BB24EE414F8F9E7831D, ___namespaceName_4)); }
	inline String_t* get_namespaceName_4() const { return ___namespaceName_4; }
	inline String_t** get_address_of_namespaceName_4() { return &___namespaceName_4; }
	inline void set_namespaceName_4(String_t* value)
	{
		___namespaceName_4 = value;
		Il2CppCodeGenWriteBarrier((&___namespaceName_4), value);
	}

	inline static int32_t get_offset_of_hashCode_5() { return static_cast<int32_t>(offsetof(XNamespace_t771F5C81A42B6DFC65678BB24EE414F8F9E7831D, ___hashCode_5)); }
	inline int32_t get_hashCode_5() const { return ___hashCode_5; }
	inline int32_t* get_address_of_hashCode_5() { return &___hashCode_5; }
	inline void set_hashCode_5(int32_t value)
	{
		___hashCode_5 = value;
	}

	inline static int32_t get_offset_of_names_6() { return static_cast<int32_t>(offsetof(XNamespace_t771F5C81A42B6DFC65678BB24EE414F8F9E7831D, ___names_6)); }
	inline XHashtable_1_tD5FCDADAB0E25D7F54936D993C541EF22988B4E1 * get_names_6() const { return ___names_6; }
	inline XHashtable_1_tD5FCDADAB0E25D7F54936D993C541EF22988B4E1 ** get_address_of_names_6() { return &___names_6; }
	inline void set_names_6(XHashtable_1_tD5FCDADAB0E25D7F54936D993C541EF22988B4E1 * value)
	{
		___names_6 = value;
		Il2CppCodeGenWriteBarrier((&___names_6), value);
	}
};

struct XNamespace_t771F5C81A42B6DFC65678BB24EE414F8F9E7831D_StaticFields
{
public:
	// System.Xml.Linq.XHashtable`1<System.WeakReference> System.Xml.Linq.XNamespace::namespaces
	XHashtable_1_t0B1F552C698411530920D275A9C48ED8F72B66F2 * ___namespaces_0;
	// System.WeakReference System.Xml.Linq.XNamespace::refNone
	WeakReference_t0495CC81CD6403E662B7700B802443F6F730E39D * ___refNone_1;
	// System.WeakReference System.Xml.Linq.XNamespace::refXml
	WeakReference_t0495CC81CD6403E662B7700B802443F6F730E39D * ___refXml_2;
	// System.WeakReference System.Xml.Linq.XNamespace::refXmlns
	WeakReference_t0495CC81CD6403E662B7700B802443F6F730E39D * ___refXmlns_3;

public:
	inline static int32_t get_offset_of_namespaces_0() { return static_cast<int32_t>(offsetof(XNamespace_t771F5C81A42B6DFC65678BB24EE414F8F9E7831D_StaticFields, ___namespaces_0)); }
	inline XHashtable_1_t0B1F552C698411530920D275A9C48ED8F72B66F2 * get_namespaces_0() const { return ___namespaces_0; }
	inline XHashtable_1_t0B1F552C698411530920D275A9C48ED8F72B66F2 ** get_address_of_namespaces_0() { return &___namespaces_0; }
	inline void set_namespaces_0(XHashtable_1_t0B1F552C698411530920D275A9C48ED8F72B66F2 * value)
	{
		___namespaces_0 = value;
		Il2CppCodeGenWriteBarrier((&___namespaces_0), value);
	}

	inline static int32_t get_offset_of_refNone_1() { return static_cast<int32_t>(offsetof(XNamespace_t771F5C81A42B6DFC65678BB24EE414F8F9E7831D_StaticFields, ___refNone_1)); }
	inline WeakReference_t0495CC81CD6403E662B7700B802443F6F730E39D * get_refNone_1() const { return ___refNone_1; }
	inline WeakReference_t0495CC81CD6403E662B7700B802443F6F730E39D ** get_address_of_refNone_1() { return &___refNone_1; }
	inline void set_refNone_1(WeakReference_t0495CC81CD6403E662B7700B802443F6F730E39D * value)
	{
		___refNone_1 = value;
		Il2CppCodeGenWriteBarrier((&___refNone_1), value);
	}

	inline static int32_t get_offset_of_refXml_2() { return static_cast<int32_t>(offsetof(XNamespace_t771F5C81A42B6DFC65678BB24EE414F8F9E7831D_StaticFields, ___refXml_2)); }
	inline WeakReference_t0495CC81CD6403E662B7700B802443F6F730E39D * get_refXml_2() const { return ___refXml_2; }
	inline WeakReference_t0495CC81CD6403E662B7700B802443F6F730E39D ** get_address_of_refXml_2() { return &___refXml_2; }
	inline void set_refXml_2(WeakReference_t0495CC81CD6403E662B7700B802443F6F730E39D * value)
	{
		___refXml_2 = value;
		Il2CppCodeGenWriteBarrier((&___refXml_2), value);
	}

	inline static int32_t get_offset_of_refXmlns_3() { return static_cast<int32_t>(offsetof(XNamespace_t771F5C81A42B6DFC65678BB24EE414F8F9E7831D_StaticFields, ___refXmlns_3)); }
	inline WeakReference_t0495CC81CD6403E662B7700B802443F6F730E39D * get_refXmlns_3() const { return ___refXmlns_3; }
	inline WeakReference_t0495CC81CD6403E662B7700B802443F6F730E39D ** get_address_of_refXmlns_3() { return &___refXmlns_3; }
	inline void set_refXmlns_3(WeakReference_t0495CC81CD6403E662B7700B802443F6F730E39D * value)
	{
		___refXmlns_3 = value;
		Il2CppCodeGenWriteBarrier((&___refXmlns_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XNAMESPACE_T771F5C81A42B6DFC65678BB24EE414F8F9E7831D_H
#ifndef XOBJECT_T0DA241208A0EC65CA16AEA485DD80F21A693EDEF_H
#define XOBJECT_T0DA241208A0EC65CA16AEA485DD80F21A693EDEF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Linq.XObject
struct  XObject_t0DA241208A0EC65CA16AEA485DD80F21A693EDEF  : public RuntimeObject
{
public:
	// System.Xml.Linq.XContainer System.Xml.Linq.XObject::parent
	XContainer_t17E75C21493AAC7D12D6159A3A8C5308B3DBC599 * ___parent_0;
	// System.Object System.Xml.Linq.XObject::annotations
	RuntimeObject * ___annotations_1;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(XObject_t0DA241208A0EC65CA16AEA485DD80F21A693EDEF, ___parent_0)); }
	inline XContainer_t17E75C21493AAC7D12D6159A3A8C5308B3DBC599 * get_parent_0() const { return ___parent_0; }
	inline XContainer_t17E75C21493AAC7D12D6159A3A8C5308B3DBC599 ** get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(XContainer_t17E75C21493AAC7D12D6159A3A8C5308B3DBC599 * value)
	{
		___parent_0 = value;
		Il2CppCodeGenWriteBarrier((&___parent_0), value);
	}

	inline static int32_t get_offset_of_annotations_1() { return static_cast<int32_t>(offsetof(XObject_t0DA241208A0EC65CA16AEA485DD80F21A693EDEF, ___annotations_1)); }
	inline RuntimeObject * get_annotations_1() const { return ___annotations_1; }
	inline RuntimeObject ** get_address_of_annotations_1() { return &___annotations_1; }
	inline void set_annotations_1(RuntimeObject * value)
	{
		___annotations_1 = value;
		Il2CppCodeGenWriteBarrier((&___annotations_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XOBJECT_T0DA241208A0EC65CA16AEA485DD80F21A693EDEF_H
#ifndef XOBJECTCHANGEANNOTATION_TF9972FFFFA58338DFC8684004D7C3D037B72419E_H
#define XOBJECTCHANGEANNOTATION_TF9972FFFFA58338DFC8684004D7C3D037B72419E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Linq.XObjectChangeAnnotation
struct  XObjectChangeAnnotation_tF9972FFFFA58338DFC8684004D7C3D037B72419E  : public RuntimeObject
{
public:
	// System.EventHandler`1<System.Xml.Linq.XObjectChangeEventArgs> System.Xml.Linq.XObjectChangeAnnotation::changing
	EventHandler_1_t893F89009B075641965395B3F7C20AA5549D00AC * ___changing_0;
	// System.EventHandler`1<System.Xml.Linq.XObjectChangeEventArgs> System.Xml.Linq.XObjectChangeAnnotation::changed
	EventHandler_1_t893F89009B075641965395B3F7C20AA5549D00AC * ___changed_1;

public:
	inline static int32_t get_offset_of_changing_0() { return static_cast<int32_t>(offsetof(XObjectChangeAnnotation_tF9972FFFFA58338DFC8684004D7C3D037B72419E, ___changing_0)); }
	inline EventHandler_1_t893F89009B075641965395B3F7C20AA5549D00AC * get_changing_0() const { return ___changing_0; }
	inline EventHandler_1_t893F89009B075641965395B3F7C20AA5549D00AC ** get_address_of_changing_0() { return &___changing_0; }
	inline void set_changing_0(EventHandler_1_t893F89009B075641965395B3F7C20AA5549D00AC * value)
	{
		___changing_0 = value;
		Il2CppCodeGenWriteBarrier((&___changing_0), value);
	}

	inline static int32_t get_offset_of_changed_1() { return static_cast<int32_t>(offsetof(XObjectChangeAnnotation_tF9972FFFFA58338DFC8684004D7C3D037B72419E, ___changed_1)); }
	inline EventHandler_1_t893F89009B075641965395B3F7C20AA5549D00AC * get_changed_1() const { return ___changed_1; }
	inline EventHandler_1_t893F89009B075641965395B3F7C20AA5549D00AC ** get_address_of_changed_1() { return &___changed_1; }
	inline void set_changed_1(EventHandler_1_t893F89009B075641965395B3F7C20AA5549D00AC * value)
	{
		___changed_1 = value;
		Il2CppCodeGenWriteBarrier((&___changed_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XOBJECTCHANGEANNOTATION_TF9972FFFFA58338DFC8684004D7C3D037B72419E_H
#ifndef WEBSOCKETCOMMANDBASE_T2121F47322A7F3C4FC75E91FA1F46F3B39EE32D9_H
#define WEBSOCKETCOMMANDBASE_T2121F47322A7F3C4FC75E91FA1F46F3B39EE32D9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocket4Net.Command.WebSocketCommandBase
struct  WebSocketCommandBase_t2121F47322A7F3C4FC75E91FA1F46F3B39EE32D9  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBSOCKETCOMMANDBASE_T2121F47322A7F3C4FC75E91FA1F46F3B39EE32D9_H
#ifndef EXTENSIONS_TF44DE23091552C5D9D740D937587B78809D7026A_H
#define EXTENSIONS_TF44DE23091552C5D9D740D937587B78809D7026A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocket4Net.Extensions
struct  Extensions_tF44DE23091552C5D9D740D937587B78809D7026A  : public RuntimeObject
{
public:

public:
};

struct Extensions_tF44DE23091552C5D9D740D937587B78809D7026A_StaticFields
{
public:
	// System.Char[] WebSocket4Net.Extensions::m_CrCf
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___m_CrCf_0;
	// System.Type[] WebSocket4Net.Extensions::m_SimpleTypes
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___m_SimpleTypes_1;

public:
	inline static int32_t get_offset_of_m_CrCf_0() { return static_cast<int32_t>(offsetof(Extensions_tF44DE23091552C5D9D740D937587B78809D7026A_StaticFields, ___m_CrCf_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_m_CrCf_0() const { return ___m_CrCf_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_m_CrCf_0() { return &___m_CrCf_0; }
	inline void set_m_CrCf_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___m_CrCf_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_CrCf_0), value);
	}

	inline static int32_t get_offset_of_m_SimpleTypes_1() { return static_cast<int32_t>(offsetof(Extensions_tF44DE23091552C5D9D740D937587B78809D7026A_StaticFields, ___m_SimpleTypes_1)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get_m_SimpleTypes_1() const { return ___m_SimpleTypes_1; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of_m_SimpleTypes_1() { return &___m_SimpleTypes_1; }
	inline void set_m_SimpleTypes_1(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		___m_SimpleTypes_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_SimpleTypes_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENSIONS_TF44DE23091552C5D9D740D937587B78809D7026A_H
#ifndef CLOSESTATUSCODEHYBI10_TB3EAB89831F235D52C9D77109C6E2E257EB5F87A_H
#define CLOSESTATUSCODEHYBI10_TB3EAB89831F235D52C9D77109C6E2E257EB5F87A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocket4Net.Protocol.CloseStatusCodeHybi10
struct  CloseStatusCodeHybi10_tB3EAB89831F235D52C9D77109C6E2E257EB5F87A  : public RuntimeObject
{
public:
	// System.Int16 WebSocket4Net.Protocol.CloseStatusCodeHybi10::<NormalClosure>k__BackingField
	int16_t ___U3CNormalClosureU3Ek__BackingField_0;
	// System.Int16 WebSocket4Net.Protocol.CloseStatusCodeHybi10::<GoingAway>k__BackingField
	int16_t ___U3CGoingAwayU3Ek__BackingField_1;
	// System.Int16 WebSocket4Net.Protocol.CloseStatusCodeHybi10::<ProtocolError>k__BackingField
	int16_t ___U3CProtocolErrorU3Ek__BackingField_2;
	// System.Int16 WebSocket4Net.Protocol.CloseStatusCodeHybi10::<NotAcceptableData>k__BackingField
	int16_t ___U3CNotAcceptableDataU3Ek__BackingField_3;
	// System.Int16 WebSocket4Net.Protocol.CloseStatusCodeHybi10::<TooLargeFrame>k__BackingField
	int16_t ___U3CTooLargeFrameU3Ek__BackingField_4;
	// System.Int16 WebSocket4Net.Protocol.CloseStatusCodeHybi10::<InvalidUTF8>k__BackingField
	int16_t ___U3CInvalidUTF8U3Ek__BackingField_5;
	// System.Int16 WebSocket4Net.Protocol.CloseStatusCodeHybi10::<ViolatePolicy>k__BackingField
	int16_t ___U3CViolatePolicyU3Ek__BackingField_6;
	// System.Int16 WebSocket4Net.Protocol.CloseStatusCodeHybi10::<ExtensionNotMatch>k__BackingField
	int16_t ___U3CExtensionNotMatchU3Ek__BackingField_7;
	// System.Int16 WebSocket4Net.Protocol.CloseStatusCodeHybi10::<UnexpectedCondition>k__BackingField
	int16_t ___U3CUnexpectedConditionU3Ek__BackingField_8;
	// System.Int16 WebSocket4Net.Protocol.CloseStatusCodeHybi10::<TLSHandshakeFailure>k__BackingField
	int16_t ___U3CTLSHandshakeFailureU3Ek__BackingField_9;
	// System.Int16 WebSocket4Net.Protocol.CloseStatusCodeHybi10::<NoStatusCode>k__BackingField
	int16_t ___U3CNoStatusCodeU3Ek__BackingField_10;

public:
	inline static int32_t get_offset_of_U3CNormalClosureU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CloseStatusCodeHybi10_tB3EAB89831F235D52C9D77109C6E2E257EB5F87A, ___U3CNormalClosureU3Ek__BackingField_0)); }
	inline int16_t get_U3CNormalClosureU3Ek__BackingField_0() const { return ___U3CNormalClosureU3Ek__BackingField_0; }
	inline int16_t* get_address_of_U3CNormalClosureU3Ek__BackingField_0() { return &___U3CNormalClosureU3Ek__BackingField_0; }
	inline void set_U3CNormalClosureU3Ek__BackingField_0(int16_t value)
	{
		___U3CNormalClosureU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CGoingAwayU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CloseStatusCodeHybi10_tB3EAB89831F235D52C9D77109C6E2E257EB5F87A, ___U3CGoingAwayU3Ek__BackingField_1)); }
	inline int16_t get_U3CGoingAwayU3Ek__BackingField_1() const { return ___U3CGoingAwayU3Ek__BackingField_1; }
	inline int16_t* get_address_of_U3CGoingAwayU3Ek__BackingField_1() { return &___U3CGoingAwayU3Ek__BackingField_1; }
	inline void set_U3CGoingAwayU3Ek__BackingField_1(int16_t value)
	{
		___U3CGoingAwayU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CProtocolErrorU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CloseStatusCodeHybi10_tB3EAB89831F235D52C9D77109C6E2E257EB5F87A, ___U3CProtocolErrorU3Ek__BackingField_2)); }
	inline int16_t get_U3CProtocolErrorU3Ek__BackingField_2() const { return ___U3CProtocolErrorU3Ek__BackingField_2; }
	inline int16_t* get_address_of_U3CProtocolErrorU3Ek__BackingField_2() { return &___U3CProtocolErrorU3Ek__BackingField_2; }
	inline void set_U3CProtocolErrorU3Ek__BackingField_2(int16_t value)
	{
		___U3CProtocolErrorU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CNotAcceptableDataU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(CloseStatusCodeHybi10_tB3EAB89831F235D52C9D77109C6E2E257EB5F87A, ___U3CNotAcceptableDataU3Ek__BackingField_3)); }
	inline int16_t get_U3CNotAcceptableDataU3Ek__BackingField_3() const { return ___U3CNotAcceptableDataU3Ek__BackingField_3; }
	inline int16_t* get_address_of_U3CNotAcceptableDataU3Ek__BackingField_3() { return &___U3CNotAcceptableDataU3Ek__BackingField_3; }
	inline void set_U3CNotAcceptableDataU3Ek__BackingField_3(int16_t value)
	{
		___U3CNotAcceptableDataU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CTooLargeFrameU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(CloseStatusCodeHybi10_tB3EAB89831F235D52C9D77109C6E2E257EB5F87A, ___U3CTooLargeFrameU3Ek__BackingField_4)); }
	inline int16_t get_U3CTooLargeFrameU3Ek__BackingField_4() const { return ___U3CTooLargeFrameU3Ek__BackingField_4; }
	inline int16_t* get_address_of_U3CTooLargeFrameU3Ek__BackingField_4() { return &___U3CTooLargeFrameU3Ek__BackingField_4; }
	inline void set_U3CTooLargeFrameU3Ek__BackingField_4(int16_t value)
	{
		___U3CTooLargeFrameU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CInvalidUTF8U3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(CloseStatusCodeHybi10_tB3EAB89831F235D52C9D77109C6E2E257EB5F87A, ___U3CInvalidUTF8U3Ek__BackingField_5)); }
	inline int16_t get_U3CInvalidUTF8U3Ek__BackingField_5() const { return ___U3CInvalidUTF8U3Ek__BackingField_5; }
	inline int16_t* get_address_of_U3CInvalidUTF8U3Ek__BackingField_5() { return &___U3CInvalidUTF8U3Ek__BackingField_5; }
	inline void set_U3CInvalidUTF8U3Ek__BackingField_5(int16_t value)
	{
		___U3CInvalidUTF8U3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CViolatePolicyU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(CloseStatusCodeHybi10_tB3EAB89831F235D52C9D77109C6E2E257EB5F87A, ___U3CViolatePolicyU3Ek__BackingField_6)); }
	inline int16_t get_U3CViolatePolicyU3Ek__BackingField_6() const { return ___U3CViolatePolicyU3Ek__BackingField_6; }
	inline int16_t* get_address_of_U3CViolatePolicyU3Ek__BackingField_6() { return &___U3CViolatePolicyU3Ek__BackingField_6; }
	inline void set_U3CViolatePolicyU3Ek__BackingField_6(int16_t value)
	{
		___U3CViolatePolicyU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CExtensionNotMatchU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(CloseStatusCodeHybi10_tB3EAB89831F235D52C9D77109C6E2E257EB5F87A, ___U3CExtensionNotMatchU3Ek__BackingField_7)); }
	inline int16_t get_U3CExtensionNotMatchU3Ek__BackingField_7() const { return ___U3CExtensionNotMatchU3Ek__BackingField_7; }
	inline int16_t* get_address_of_U3CExtensionNotMatchU3Ek__BackingField_7() { return &___U3CExtensionNotMatchU3Ek__BackingField_7; }
	inline void set_U3CExtensionNotMatchU3Ek__BackingField_7(int16_t value)
	{
		___U3CExtensionNotMatchU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CUnexpectedConditionU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(CloseStatusCodeHybi10_tB3EAB89831F235D52C9D77109C6E2E257EB5F87A, ___U3CUnexpectedConditionU3Ek__BackingField_8)); }
	inline int16_t get_U3CUnexpectedConditionU3Ek__BackingField_8() const { return ___U3CUnexpectedConditionU3Ek__BackingField_8; }
	inline int16_t* get_address_of_U3CUnexpectedConditionU3Ek__BackingField_8() { return &___U3CUnexpectedConditionU3Ek__BackingField_8; }
	inline void set_U3CUnexpectedConditionU3Ek__BackingField_8(int16_t value)
	{
		___U3CUnexpectedConditionU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CTLSHandshakeFailureU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(CloseStatusCodeHybi10_tB3EAB89831F235D52C9D77109C6E2E257EB5F87A, ___U3CTLSHandshakeFailureU3Ek__BackingField_9)); }
	inline int16_t get_U3CTLSHandshakeFailureU3Ek__BackingField_9() const { return ___U3CTLSHandshakeFailureU3Ek__BackingField_9; }
	inline int16_t* get_address_of_U3CTLSHandshakeFailureU3Ek__BackingField_9() { return &___U3CTLSHandshakeFailureU3Ek__BackingField_9; }
	inline void set_U3CTLSHandshakeFailureU3Ek__BackingField_9(int16_t value)
	{
		___U3CTLSHandshakeFailureU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CNoStatusCodeU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(CloseStatusCodeHybi10_tB3EAB89831F235D52C9D77109C6E2E257EB5F87A, ___U3CNoStatusCodeU3Ek__BackingField_10)); }
	inline int16_t get_U3CNoStatusCodeU3Ek__BackingField_10() const { return ___U3CNoStatusCodeU3Ek__BackingField_10; }
	inline int16_t* get_address_of_U3CNoStatusCodeU3Ek__BackingField_10() { return &___U3CNoStatusCodeU3Ek__BackingField_10; }
	inline void set_U3CNoStatusCodeU3Ek__BackingField_10(int16_t value)
	{
		___U3CNoStatusCodeU3Ek__BackingField_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLOSESTATUSCODEHYBI10_TB3EAB89831F235D52C9D77109C6E2E257EB5F87A_H
#ifndef CLOSESTATUSCODERFC6455_T4FFBA83B0178B701BA6C83A5F8D0DA855ED00E03_H
#define CLOSESTATUSCODERFC6455_T4FFBA83B0178B701BA6C83A5F8D0DA855ED00E03_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocket4Net.Protocol.CloseStatusCodeRfc6455
struct  CloseStatusCodeRfc6455_t4FFBA83B0178B701BA6C83A5F8D0DA855ED00E03  : public RuntimeObject
{
public:
	// System.Int16 WebSocket4Net.Protocol.CloseStatusCodeRfc6455::<NormalClosure>k__BackingField
	int16_t ___U3CNormalClosureU3Ek__BackingField_0;
	// System.Int16 WebSocket4Net.Protocol.CloseStatusCodeRfc6455::<GoingAway>k__BackingField
	int16_t ___U3CGoingAwayU3Ek__BackingField_1;
	// System.Int16 WebSocket4Net.Protocol.CloseStatusCodeRfc6455::<ProtocolError>k__BackingField
	int16_t ___U3CProtocolErrorU3Ek__BackingField_2;
	// System.Int16 WebSocket4Net.Protocol.CloseStatusCodeRfc6455::<NotAcceptableData>k__BackingField
	int16_t ___U3CNotAcceptableDataU3Ek__BackingField_3;
	// System.Int16 WebSocket4Net.Protocol.CloseStatusCodeRfc6455::<TooLargeFrame>k__BackingField
	int16_t ___U3CTooLargeFrameU3Ek__BackingField_4;
	// System.Int16 WebSocket4Net.Protocol.CloseStatusCodeRfc6455::<InvalidUTF8>k__BackingField
	int16_t ___U3CInvalidUTF8U3Ek__BackingField_5;
	// System.Int16 WebSocket4Net.Protocol.CloseStatusCodeRfc6455::<ViolatePolicy>k__BackingField
	int16_t ___U3CViolatePolicyU3Ek__BackingField_6;
	// System.Int16 WebSocket4Net.Protocol.CloseStatusCodeRfc6455::<ExtensionNotMatch>k__BackingField
	int16_t ___U3CExtensionNotMatchU3Ek__BackingField_7;
	// System.Int16 WebSocket4Net.Protocol.CloseStatusCodeRfc6455::<UnexpectedCondition>k__BackingField
	int16_t ___U3CUnexpectedConditionU3Ek__BackingField_8;
	// System.Int16 WebSocket4Net.Protocol.CloseStatusCodeRfc6455::<NoStatusCode>k__BackingField
	int16_t ___U3CNoStatusCodeU3Ek__BackingField_9;

public:
	inline static int32_t get_offset_of_U3CNormalClosureU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CloseStatusCodeRfc6455_t4FFBA83B0178B701BA6C83A5F8D0DA855ED00E03, ___U3CNormalClosureU3Ek__BackingField_0)); }
	inline int16_t get_U3CNormalClosureU3Ek__BackingField_0() const { return ___U3CNormalClosureU3Ek__BackingField_0; }
	inline int16_t* get_address_of_U3CNormalClosureU3Ek__BackingField_0() { return &___U3CNormalClosureU3Ek__BackingField_0; }
	inline void set_U3CNormalClosureU3Ek__BackingField_0(int16_t value)
	{
		___U3CNormalClosureU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CGoingAwayU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CloseStatusCodeRfc6455_t4FFBA83B0178B701BA6C83A5F8D0DA855ED00E03, ___U3CGoingAwayU3Ek__BackingField_1)); }
	inline int16_t get_U3CGoingAwayU3Ek__BackingField_1() const { return ___U3CGoingAwayU3Ek__BackingField_1; }
	inline int16_t* get_address_of_U3CGoingAwayU3Ek__BackingField_1() { return &___U3CGoingAwayU3Ek__BackingField_1; }
	inline void set_U3CGoingAwayU3Ek__BackingField_1(int16_t value)
	{
		___U3CGoingAwayU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CProtocolErrorU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CloseStatusCodeRfc6455_t4FFBA83B0178B701BA6C83A5F8D0DA855ED00E03, ___U3CProtocolErrorU3Ek__BackingField_2)); }
	inline int16_t get_U3CProtocolErrorU3Ek__BackingField_2() const { return ___U3CProtocolErrorU3Ek__BackingField_2; }
	inline int16_t* get_address_of_U3CProtocolErrorU3Ek__BackingField_2() { return &___U3CProtocolErrorU3Ek__BackingField_2; }
	inline void set_U3CProtocolErrorU3Ek__BackingField_2(int16_t value)
	{
		___U3CProtocolErrorU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CNotAcceptableDataU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(CloseStatusCodeRfc6455_t4FFBA83B0178B701BA6C83A5F8D0DA855ED00E03, ___U3CNotAcceptableDataU3Ek__BackingField_3)); }
	inline int16_t get_U3CNotAcceptableDataU3Ek__BackingField_3() const { return ___U3CNotAcceptableDataU3Ek__BackingField_3; }
	inline int16_t* get_address_of_U3CNotAcceptableDataU3Ek__BackingField_3() { return &___U3CNotAcceptableDataU3Ek__BackingField_3; }
	inline void set_U3CNotAcceptableDataU3Ek__BackingField_3(int16_t value)
	{
		___U3CNotAcceptableDataU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CTooLargeFrameU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(CloseStatusCodeRfc6455_t4FFBA83B0178B701BA6C83A5F8D0DA855ED00E03, ___U3CTooLargeFrameU3Ek__BackingField_4)); }
	inline int16_t get_U3CTooLargeFrameU3Ek__BackingField_4() const { return ___U3CTooLargeFrameU3Ek__BackingField_4; }
	inline int16_t* get_address_of_U3CTooLargeFrameU3Ek__BackingField_4() { return &___U3CTooLargeFrameU3Ek__BackingField_4; }
	inline void set_U3CTooLargeFrameU3Ek__BackingField_4(int16_t value)
	{
		___U3CTooLargeFrameU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CInvalidUTF8U3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(CloseStatusCodeRfc6455_t4FFBA83B0178B701BA6C83A5F8D0DA855ED00E03, ___U3CInvalidUTF8U3Ek__BackingField_5)); }
	inline int16_t get_U3CInvalidUTF8U3Ek__BackingField_5() const { return ___U3CInvalidUTF8U3Ek__BackingField_5; }
	inline int16_t* get_address_of_U3CInvalidUTF8U3Ek__BackingField_5() { return &___U3CInvalidUTF8U3Ek__BackingField_5; }
	inline void set_U3CInvalidUTF8U3Ek__BackingField_5(int16_t value)
	{
		___U3CInvalidUTF8U3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CViolatePolicyU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(CloseStatusCodeRfc6455_t4FFBA83B0178B701BA6C83A5F8D0DA855ED00E03, ___U3CViolatePolicyU3Ek__BackingField_6)); }
	inline int16_t get_U3CViolatePolicyU3Ek__BackingField_6() const { return ___U3CViolatePolicyU3Ek__BackingField_6; }
	inline int16_t* get_address_of_U3CViolatePolicyU3Ek__BackingField_6() { return &___U3CViolatePolicyU3Ek__BackingField_6; }
	inline void set_U3CViolatePolicyU3Ek__BackingField_6(int16_t value)
	{
		___U3CViolatePolicyU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CExtensionNotMatchU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(CloseStatusCodeRfc6455_t4FFBA83B0178B701BA6C83A5F8D0DA855ED00E03, ___U3CExtensionNotMatchU3Ek__BackingField_7)); }
	inline int16_t get_U3CExtensionNotMatchU3Ek__BackingField_7() const { return ___U3CExtensionNotMatchU3Ek__BackingField_7; }
	inline int16_t* get_address_of_U3CExtensionNotMatchU3Ek__BackingField_7() { return &___U3CExtensionNotMatchU3Ek__BackingField_7; }
	inline void set_U3CExtensionNotMatchU3Ek__BackingField_7(int16_t value)
	{
		___U3CExtensionNotMatchU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CUnexpectedConditionU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(CloseStatusCodeRfc6455_t4FFBA83B0178B701BA6C83A5F8D0DA855ED00E03, ___U3CUnexpectedConditionU3Ek__BackingField_8)); }
	inline int16_t get_U3CUnexpectedConditionU3Ek__BackingField_8() const { return ___U3CUnexpectedConditionU3Ek__BackingField_8; }
	inline int16_t* get_address_of_U3CUnexpectedConditionU3Ek__BackingField_8() { return &___U3CUnexpectedConditionU3Ek__BackingField_8; }
	inline void set_U3CUnexpectedConditionU3Ek__BackingField_8(int16_t value)
	{
		___U3CUnexpectedConditionU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CNoStatusCodeU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(CloseStatusCodeRfc6455_t4FFBA83B0178B701BA6C83A5F8D0DA855ED00E03, ___U3CNoStatusCodeU3Ek__BackingField_9)); }
	inline int16_t get_U3CNoStatusCodeU3Ek__BackingField_9() const { return ___U3CNoStatusCodeU3Ek__BackingField_9; }
	inline int16_t* get_address_of_U3CNoStatusCodeU3Ek__BackingField_9() { return &___U3CNoStatusCodeU3Ek__BackingField_9; }
	inline void set_U3CNoStatusCodeU3Ek__BackingField_9(int16_t value)
	{
		___U3CNoStatusCodeU3Ek__BackingField_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLOSESTATUSCODERFC6455_T4FFBA83B0178B701BA6C83A5F8D0DA855ED00E03_H
#ifndef DRAFTHYBI10DATAREADER_T25DCD4F60B884278B6C8B02C53F70CE6BE3BDE3F_H
#define DRAFTHYBI10DATAREADER_T25DCD4F60B884278B6C8B02C53F70CE6BE3BDE3F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocket4Net.Protocol.DraftHybi10DataReader
struct  DraftHybi10DataReader_t25DCD4F60B884278B6C8B02C53F70CE6BE3BDE3F  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<WebSocket4Net.Protocol.WebSocketDataFrame> WebSocket4Net.Protocol.DraftHybi10DataReader::m_PreviousFrames
	List_1_tB743623D7C86BACC71309E175C141687B4362BE5 * ___m_PreviousFrames_0;
	// WebSocket4Net.Protocol.WebSocketDataFrame WebSocket4Net.Protocol.DraftHybi10DataReader::m_Frame
	WebSocketDataFrame_tAF30418C83AF20230D871C344C668A3157898C96 * ___m_Frame_1;
	// WebSocket4Net.Protocol.FramePartReader.IDataFramePartReader WebSocket4Net.Protocol.DraftHybi10DataReader::m_PartReader
	RuntimeObject* ___m_PartReader_2;
	// System.Int32 WebSocket4Net.Protocol.DraftHybi10DataReader::m_LastPartLength
	int32_t ___m_LastPartLength_3;

public:
	inline static int32_t get_offset_of_m_PreviousFrames_0() { return static_cast<int32_t>(offsetof(DraftHybi10DataReader_t25DCD4F60B884278B6C8B02C53F70CE6BE3BDE3F, ___m_PreviousFrames_0)); }
	inline List_1_tB743623D7C86BACC71309E175C141687B4362BE5 * get_m_PreviousFrames_0() const { return ___m_PreviousFrames_0; }
	inline List_1_tB743623D7C86BACC71309E175C141687B4362BE5 ** get_address_of_m_PreviousFrames_0() { return &___m_PreviousFrames_0; }
	inline void set_m_PreviousFrames_0(List_1_tB743623D7C86BACC71309E175C141687B4362BE5 * value)
	{
		___m_PreviousFrames_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_PreviousFrames_0), value);
	}

	inline static int32_t get_offset_of_m_Frame_1() { return static_cast<int32_t>(offsetof(DraftHybi10DataReader_t25DCD4F60B884278B6C8B02C53F70CE6BE3BDE3F, ___m_Frame_1)); }
	inline WebSocketDataFrame_tAF30418C83AF20230D871C344C668A3157898C96 * get_m_Frame_1() const { return ___m_Frame_1; }
	inline WebSocketDataFrame_tAF30418C83AF20230D871C344C668A3157898C96 ** get_address_of_m_Frame_1() { return &___m_Frame_1; }
	inline void set_m_Frame_1(WebSocketDataFrame_tAF30418C83AF20230D871C344C668A3157898C96 * value)
	{
		___m_Frame_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Frame_1), value);
	}

	inline static int32_t get_offset_of_m_PartReader_2() { return static_cast<int32_t>(offsetof(DraftHybi10DataReader_t25DCD4F60B884278B6C8B02C53F70CE6BE3BDE3F, ___m_PartReader_2)); }
	inline RuntimeObject* get_m_PartReader_2() const { return ___m_PartReader_2; }
	inline RuntimeObject** get_address_of_m_PartReader_2() { return &___m_PartReader_2; }
	inline void set_m_PartReader_2(RuntimeObject* value)
	{
		___m_PartReader_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_PartReader_2), value);
	}

	inline static int32_t get_offset_of_m_LastPartLength_3() { return static_cast<int32_t>(offsetof(DraftHybi10DataReader_t25DCD4F60B884278B6C8B02C53F70CE6BE3BDE3F, ___m_LastPartLength_3)); }
	inline int32_t get_m_LastPartLength_3() const { return ___m_LastPartLength_3; }
	inline int32_t* get_address_of_m_LastPartLength_3() { return &___m_LastPartLength_3; }
	inline void set_m_LastPartLength_3(int32_t value)
	{
		___m_LastPartLength_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAFTHYBI10DATAREADER_T25DCD4F60B884278B6C8B02C53F70CE6BE3BDE3F_H
#ifndef DATAFRAMEPARTREADER_TDE67A43BE1F64AE2D208BBCFAF2AF25933B20221_H
#define DATAFRAMEPARTREADER_TDE67A43BE1F64AE2D208BBCFAF2AF25933B20221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocket4Net.Protocol.FramePartReader.DataFramePartReader
struct  DataFramePartReader_tDE67A43BE1F64AE2D208BBCFAF2AF25933B20221  : public RuntimeObject
{
public:

public:
};

struct DataFramePartReader_tDE67A43BE1F64AE2D208BBCFAF2AF25933B20221_StaticFields
{
public:
	// WebSocket4Net.Protocol.FramePartReader.IDataFramePartReader WebSocket4Net.Protocol.FramePartReader.DataFramePartReader::<FixPartReader>k__BackingField
	RuntimeObject* ___U3CFixPartReaderU3Ek__BackingField_0;
	// WebSocket4Net.Protocol.FramePartReader.IDataFramePartReader WebSocket4Net.Protocol.FramePartReader.DataFramePartReader::<ExtendedLenghtReader>k__BackingField
	RuntimeObject* ___U3CExtendedLenghtReaderU3Ek__BackingField_1;
	// WebSocket4Net.Protocol.FramePartReader.IDataFramePartReader WebSocket4Net.Protocol.FramePartReader.DataFramePartReader::<MaskKeyReader>k__BackingField
	RuntimeObject* ___U3CMaskKeyReaderU3Ek__BackingField_2;
	// WebSocket4Net.Protocol.FramePartReader.IDataFramePartReader WebSocket4Net.Protocol.FramePartReader.DataFramePartReader::<PayloadDataReader>k__BackingField
	RuntimeObject* ___U3CPayloadDataReaderU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CFixPartReaderU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DataFramePartReader_tDE67A43BE1F64AE2D208BBCFAF2AF25933B20221_StaticFields, ___U3CFixPartReaderU3Ek__BackingField_0)); }
	inline RuntimeObject* get_U3CFixPartReaderU3Ek__BackingField_0() const { return ___U3CFixPartReaderU3Ek__BackingField_0; }
	inline RuntimeObject** get_address_of_U3CFixPartReaderU3Ek__BackingField_0() { return &___U3CFixPartReaderU3Ek__BackingField_0; }
	inline void set_U3CFixPartReaderU3Ek__BackingField_0(RuntimeObject* value)
	{
		___U3CFixPartReaderU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFixPartReaderU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CExtendedLenghtReaderU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(DataFramePartReader_tDE67A43BE1F64AE2D208BBCFAF2AF25933B20221_StaticFields, ___U3CExtendedLenghtReaderU3Ek__BackingField_1)); }
	inline RuntimeObject* get_U3CExtendedLenghtReaderU3Ek__BackingField_1() const { return ___U3CExtendedLenghtReaderU3Ek__BackingField_1; }
	inline RuntimeObject** get_address_of_U3CExtendedLenghtReaderU3Ek__BackingField_1() { return &___U3CExtendedLenghtReaderU3Ek__BackingField_1; }
	inline void set_U3CExtendedLenghtReaderU3Ek__BackingField_1(RuntimeObject* value)
	{
		___U3CExtendedLenghtReaderU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CExtendedLenghtReaderU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CMaskKeyReaderU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(DataFramePartReader_tDE67A43BE1F64AE2D208BBCFAF2AF25933B20221_StaticFields, ___U3CMaskKeyReaderU3Ek__BackingField_2)); }
	inline RuntimeObject* get_U3CMaskKeyReaderU3Ek__BackingField_2() const { return ___U3CMaskKeyReaderU3Ek__BackingField_2; }
	inline RuntimeObject** get_address_of_U3CMaskKeyReaderU3Ek__BackingField_2() { return &___U3CMaskKeyReaderU3Ek__BackingField_2; }
	inline void set_U3CMaskKeyReaderU3Ek__BackingField_2(RuntimeObject* value)
	{
		___U3CMaskKeyReaderU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMaskKeyReaderU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CPayloadDataReaderU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(DataFramePartReader_tDE67A43BE1F64AE2D208BBCFAF2AF25933B20221_StaticFields, ___U3CPayloadDataReaderU3Ek__BackingField_3)); }
	inline RuntimeObject* get_U3CPayloadDataReaderU3Ek__BackingField_3() const { return ___U3CPayloadDataReaderU3Ek__BackingField_3; }
	inline RuntimeObject** get_address_of_U3CPayloadDataReaderU3Ek__BackingField_3() { return &___U3CPayloadDataReaderU3Ek__BackingField_3; }
	inline void set_U3CPayloadDataReaderU3Ek__BackingField_3(RuntimeObject* value)
	{
		___U3CPayloadDataReaderU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPayloadDataReaderU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATAFRAMEPARTREADER_TDE67A43BE1F64AE2D208BBCFAF2AF25933B20221_H
#ifndef PROTOCOLPROCESSORFACTORY_T55DA417FFED0B9138D1330C2AE71C3B1501755FA_H
#define PROTOCOLPROCESSORFACTORY_T55DA417FFED0B9138D1330C2AE71C3B1501755FA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocket4Net.Protocol.ProtocolProcessorFactory
struct  ProtocolProcessorFactory_t55DA417FFED0B9138D1330C2AE71C3B1501755FA  : public RuntimeObject
{
public:
	// WebSocket4Net.Protocol.IProtocolProcessor[] WebSocket4Net.Protocol.ProtocolProcessorFactory::m_OrderedProcessors
	IProtocolProcessorU5BU5D_tD93BA92F26A4FEE5D61B0BDE7A98FC35F06402BB* ___m_OrderedProcessors_0;

public:
	inline static int32_t get_offset_of_m_OrderedProcessors_0() { return static_cast<int32_t>(offsetof(ProtocolProcessorFactory_t55DA417FFED0B9138D1330C2AE71C3B1501755FA, ___m_OrderedProcessors_0)); }
	inline IProtocolProcessorU5BU5D_tD93BA92F26A4FEE5D61B0BDE7A98FC35F06402BB* get_m_OrderedProcessors_0() const { return ___m_OrderedProcessors_0; }
	inline IProtocolProcessorU5BU5D_tD93BA92F26A4FEE5D61B0BDE7A98FC35F06402BB** get_address_of_m_OrderedProcessors_0() { return &___m_OrderedProcessors_0; }
	inline void set_m_OrderedProcessors_0(IProtocolProcessorU5BU5D_tD93BA92F26A4FEE5D61B0BDE7A98FC35F06402BB* value)
	{
		___m_OrderedProcessors_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_OrderedProcessors_0), value);
	}
};

struct ProtocolProcessorFactory_t55DA417FFED0B9138D1330C2AE71C3B1501755FA_StaticFields
{
public:
	// System.Func`2<WebSocket4Net.Protocol.IProtocolProcessor,System.Int32> WebSocket4Net.Protocol.ProtocolProcessorFactory::CSU24<>9__CachedAnonymousMethodDelegate1
	Func_2_t5ED58A84E0C866574612D3A526CA3CEA44B94F81 * ___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1;
	// System.Func`2<System.Int32,System.Int32> WebSocket4Net.Protocol.ProtocolProcessorFactory::CSU24<>9__CachedAnonymousMethodDelegate6
	Func_2_t7E959DEAF1C1E69DDAA4CC9284FA7F005E80606E * ___CSU24U3CU3E9__CachedAnonymousMethodDelegate6_2;

public:
	inline static int32_t get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1() { return static_cast<int32_t>(offsetof(ProtocolProcessorFactory_t55DA417FFED0B9138D1330C2AE71C3B1501755FA_StaticFields, ___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1)); }
	inline Func_2_t5ED58A84E0C866574612D3A526CA3CEA44B94F81 * get_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1() const { return ___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1; }
	inline Func_2_t5ED58A84E0C866574612D3A526CA3CEA44B94F81 ** get_address_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1() { return &___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1; }
	inline void set_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1(Func_2_t5ED58A84E0C866574612D3A526CA3CEA44B94F81 * value)
	{
		___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate6_2() { return static_cast<int32_t>(offsetof(ProtocolProcessorFactory_t55DA417FFED0B9138D1330C2AE71C3B1501755FA_StaticFields, ___CSU24U3CU3E9__CachedAnonymousMethodDelegate6_2)); }
	inline Func_2_t7E959DEAF1C1E69DDAA4CC9284FA7F005E80606E * get_CSU24U3CU3E9__CachedAnonymousMethodDelegate6_2() const { return ___CSU24U3CU3E9__CachedAnonymousMethodDelegate6_2; }
	inline Func_2_t7E959DEAF1C1E69DDAA4CC9284FA7F005E80606E ** get_address_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate6_2() { return &___CSU24U3CU3E9__CachedAnonymousMethodDelegate6_2; }
	inline void set_CSU24U3CU3E9__CachedAnonymousMethodDelegate6_2(Func_2_t7E959DEAF1C1E69DDAA4CC9284FA7F005E80606E * value)
	{
		___CSU24U3CU3E9__CachedAnonymousMethodDelegate6_2 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E9__CachedAnonymousMethodDelegate6_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROTOCOLPROCESSORFACTORY_T55DA417FFED0B9138D1330C2AE71C3B1501755FA_H
#ifndef READERBASE_T57E7CDB15563532730AB07F0FF785C424397BAC6_H
#define READERBASE_T57E7CDB15563532730AB07F0FF785C424397BAC6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocket4Net.Protocol.ReaderBase
struct  ReaderBase_t57E7CDB15563532730AB07F0FF785C424397BAC6  : public RuntimeObject
{
public:
	// SuperSocket.ClientEngine.Protocol.ArraySegmentList WebSocket4Net.Protocol.ReaderBase::m_BufferSegments
	ArraySegmentList_t3FB5043710933FF6704A1AFCE6295DE8CE401F18 * ___m_BufferSegments_0;
	// WebSocket4Net.WebSocket WebSocket4Net.Protocol.ReaderBase::<WebSocket>k__BackingField
	WebSocket_t231538678AB4B76293D12A78A9D798992897B3BD * ___U3CWebSocketU3Ek__BackingField_1;
	// SuperSocket.ClientEngine.Protocol.IClientCommandReader`1<WebSocket4Net.WebSocketCommandInfo> WebSocket4Net.Protocol.ReaderBase::<NextCommandReader>k__BackingField
	RuntimeObject* ___U3CNextCommandReaderU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_m_BufferSegments_0() { return static_cast<int32_t>(offsetof(ReaderBase_t57E7CDB15563532730AB07F0FF785C424397BAC6, ___m_BufferSegments_0)); }
	inline ArraySegmentList_t3FB5043710933FF6704A1AFCE6295DE8CE401F18 * get_m_BufferSegments_0() const { return ___m_BufferSegments_0; }
	inline ArraySegmentList_t3FB5043710933FF6704A1AFCE6295DE8CE401F18 ** get_address_of_m_BufferSegments_0() { return &___m_BufferSegments_0; }
	inline void set_m_BufferSegments_0(ArraySegmentList_t3FB5043710933FF6704A1AFCE6295DE8CE401F18 * value)
	{
		___m_BufferSegments_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_BufferSegments_0), value);
	}

	inline static int32_t get_offset_of_U3CWebSocketU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ReaderBase_t57E7CDB15563532730AB07F0FF785C424397BAC6, ___U3CWebSocketU3Ek__BackingField_1)); }
	inline WebSocket_t231538678AB4B76293D12A78A9D798992897B3BD * get_U3CWebSocketU3Ek__BackingField_1() const { return ___U3CWebSocketU3Ek__BackingField_1; }
	inline WebSocket_t231538678AB4B76293D12A78A9D798992897B3BD ** get_address_of_U3CWebSocketU3Ek__BackingField_1() { return &___U3CWebSocketU3Ek__BackingField_1; }
	inline void set_U3CWebSocketU3Ek__BackingField_1(WebSocket_t231538678AB4B76293D12A78A9D798992897B3BD * value)
	{
		___U3CWebSocketU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CWebSocketU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CNextCommandReaderU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ReaderBase_t57E7CDB15563532730AB07F0FF785C424397BAC6, ___U3CNextCommandReaderU3Ek__BackingField_2)); }
	inline RuntimeObject* get_U3CNextCommandReaderU3Ek__BackingField_2() const { return ___U3CNextCommandReaderU3Ek__BackingField_2; }
	inline RuntimeObject** get_address_of_U3CNextCommandReaderU3Ek__BackingField_2() { return &___U3CNextCommandReaderU3Ek__BackingField_2; }
	inline void set_U3CNextCommandReaderU3Ek__BackingField_2(RuntimeObject* value)
	{
		___U3CNextCommandReaderU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNextCommandReaderU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READERBASE_T57E7CDB15563532730AB07F0FF785C424397BAC6_H
#ifndef WEBSOCKETDATAFRAME_TAF30418C83AF20230D871C344C668A3157898C96_H
#define WEBSOCKETDATAFRAME_TAF30418C83AF20230D871C344C668A3157898C96_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocket4Net.Protocol.WebSocketDataFrame
struct  WebSocketDataFrame_tAF30418C83AF20230D871C344C668A3157898C96  : public RuntimeObject
{
public:
	// SuperSocket.ClientEngine.Protocol.ArraySegmentList WebSocket4Net.Protocol.WebSocketDataFrame::m_InnerData
	ArraySegmentList_t3FB5043710933FF6704A1AFCE6295DE8CE401F18 * ___m_InnerData_0;
	// System.Int64 WebSocket4Net.Protocol.WebSocketDataFrame::m_ActualPayloadLength
	int64_t ___m_ActualPayloadLength_1;
	// System.Byte[] WebSocket4Net.Protocol.WebSocketDataFrame::<MaskKey>k__BackingField
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___U3CMaskKeyU3Ek__BackingField_2;
	// System.Byte[] WebSocket4Net.Protocol.WebSocketDataFrame::<ExtensionData>k__BackingField
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___U3CExtensionDataU3Ek__BackingField_3;
	// System.Byte[] WebSocket4Net.Protocol.WebSocketDataFrame::<ApplicationData>k__BackingField
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___U3CApplicationDataU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_m_InnerData_0() { return static_cast<int32_t>(offsetof(WebSocketDataFrame_tAF30418C83AF20230D871C344C668A3157898C96, ___m_InnerData_0)); }
	inline ArraySegmentList_t3FB5043710933FF6704A1AFCE6295DE8CE401F18 * get_m_InnerData_0() const { return ___m_InnerData_0; }
	inline ArraySegmentList_t3FB5043710933FF6704A1AFCE6295DE8CE401F18 ** get_address_of_m_InnerData_0() { return &___m_InnerData_0; }
	inline void set_m_InnerData_0(ArraySegmentList_t3FB5043710933FF6704A1AFCE6295DE8CE401F18 * value)
	{
		___m_InnerData_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_InnerData_0), value);
	}

	inline static int32_t get_offset_of_m_ActualPayloadLength_1() { return static_cast<int32_t>(offsetof(WebSocketDataFrame_tAF30418C83AF20230D871C344C668A3157898C96, ___m_ActualPayloadLength_1)); }
	inline int64_t get_m_ActualPayloadLength_1() const { return ___m_ActualPayloadLength_1; }
	inline int64_t* get_address_of_m_ActualPayloadLength_1() { return &___m_ActualPayloadLength_1; }
	inline void set_m_ActualPayloadLength_1(int64_t value)
	{
		___m_ActualPayloadLength_1 = value;
	}

	inline static int32_t get_offset_of_U3CMaskKeyU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(WebSocketDataFrame_tAF30418C83AF20230D871C344C668A3157898C96, ___U3CMaskKeyU3Ek__BackingField_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_U3CMaskKeyU3Ek__BackingField_2() const { return ___U3CMaskKeyU3Ek__BackingField_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_U3CMaskKeyU3Ek__BackingField_2() { return &___U3CMaskKeyU3Ek__BackingField_2; }
	inline void set_U3CMaskKeyU3Ek__BackingField_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___U3CMaskKeyU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMaskKeyU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CExtensionDataU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(WebSocketDataFrame_tAF30418C83AF20230D871C344C668A3157898C96, ___U3CExtensionDataU3Ek__BackingField_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_U3CExtensionDataU3Ek__BackingField_3() const { return ___U3CExtensionDataU3Ek__BackingField_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_U3CExtensionDataU3Ek__BackingField_3() { return &___U3CExtensionDataU3Ek__BackingField_3; }
	inline void set_U3CExtensionDataU3Ek__BackingField_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___U3CExtensionDataU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CExtensionDataU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CApplicationDataU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(WebSocketDataFrame_tAF30418C83AF20230D871C344C668A3157898C96, ___U3CApplicationDataU3Ek__BackingField_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_U3CApplicationDataU3Ek__BackingField_4() const { return ___U3CApplicationDataU3Ek__BackingField_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_U3CApplicationDataU3Ek__BackingField_4() { return &___U3CApplicationDataU3Ek__BackingField_4; }
	inline void set_U3CApplicationDataU3Ek__BackingField_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___U3CApplicationDataU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CApplicationDataU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBSOCKETDATAFRAME_TAF30418C83AF20230D871C344C668A3157898C96_H
#ifndef WEBSOCKETCOMMANDINFO_T78D3A11DD086E0229D1AD938B61A9A9FC5CB133D_H
#define WEBSOCKETCOMMANDINFO_T78D3A11DD086E0229D1AD938B61A9A9FC5CB133D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocket4Net.WebSocketCommandInfo
struct  WebSocketCommandInfo_t78D3A11DD086E0229D1AD938B61A9A9FC5CB133D  : public RuntimeObject
{
public:
	// System.String WebSocket4Net.WebSocketCommandInfo::<Key>k__BackingField
	String_t* ___U3CKeyU3Ek__BackingField_0;
	// System.Byte[] WebSocket4Net.WebSocketCommandInfo::<Data>k__BackingField
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___U3CDataU3Ek__BackingField_1;
	// System.String WebSocket4Net.WebSocketCommandInfo::<Text>k__BackingField
	String_t* ___U3CTextU3Ek__BackingField_2;
	// System.Int16 WebSocket4Net.WebSocketCommandInfo::<CloseStatusCode>k__BackingField
	int16_t ___U3CCloseStatusCodeU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CKeyU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(WebSocketCommandInfo_t78D3A11DD086E0229D1AD938B61A9A9FC5CB133D, ___U3CKeyU3Ek__BackingField_0)); }
	inline String_t* get_U3CKeyU3Ek__BackingField_0() const { return ___U3CKeyU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CKeyU3Ek__BackingField_0() { return &___U3CKeyU3Ek__BackingField_0; }
	inline void set_U3CKeyU3Ek__BackingField_0(String_t* value)
	{
		___U3CKeyU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CKeyU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(WebSocketCommandInfo_t78D3A11DD086E0229D1AD938B61A9A9FC5CB133D, ___U3CDataU3Ek__BackingField_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_U3CDataU3Ek__BackingField_1() const { return ___U3CDataU3Ek__BackingField_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_U3CDataU3Ek__BackingField_1() { return &___U3CDataU3Ek__BackingField_1; }
	inline void set_U3CDataU3Ek__BackingField_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___U3CDataU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CTextU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(WebSocketCommandInfo_t78D3A11DD086E0229D1AD938B61A9A9FC5CB133D, ___U3CTextU3Ek__BackingField_2)); }
	inline String_t* get_U3CTextU3Ek__BackingField_2() const { return ___U3CTextU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CTextU3Ek__BackingField_2() { return &___U3CTextU3Ek__BackingField_2; }
	inline void set_U3CTextU3Ek__BackingField_2(String_t* value)
	{
		___U3CTextU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTextU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CCloseStatusCodeU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(WebSocketCommandInfo_t78D3A11DD086E0229D1AD938B61A9A9FC5CB133D, ___U3CCloseStatusCodeU3Ek__BackingField_3)); }
	inline int16_t get_U3CCloseStatusCodeU3Ek__BackingField_3() const { return ___U3CCloseStatusCodeU3Ek__BackingField_3; }
	inline int16_t* get_address_of_U3CCloseStatusCodeU3Ek__BackingField_3() { return &___U3CCloseStatusCodeU3Ek__BackingField_3; }
	inline void set_U3CCloseStatusCodeU3Ek__BackingField_3(int16_t value)
	{
		___U3CCloseStatusCodeU3Ek__BackingField_3 = value;
	}
};

struct WebSocketCommandInfo_t78D3A11DD086E0229D1AD938B61A9A9FC5CB133D_StaticFields
{
public:
	// System.Func`2<WebSocket4Net.Protocol.WebSocketDataFrame,System.Int32> WebSocket4Net.WebSocketCommandInfo::CSU24<>9__CachedAnonymousMethodDelegate1
	Func_2_t620ED5CE01B9C27DAF57848FD5BC909D5ECBA9DE * ___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_4;

public:
	inline static int32_t get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_4() { return static_cast<int32_t>(offsetof(WebSocketCommandInfo_t78D3A11DD086E0229D1AD938B61A9A9FC5CB133D_StaticFields, ___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_4)); }
	inline Func_2_t620ED5CE01B9C27DAF57848FD5BC909D5ECBA9DE * get_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_4() const { return ___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_4; }
	inline Func_2_t620ED5CE01B9C27DAF57848FD5BC909D5ECBA9DE ** get_address_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_4() { return &___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_4; }
	inline void set_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_4(Func_2_t620ED5CE01B9C27DAF57848FD5BC909D5ECBA9DE * value)
	{
		___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_4 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBSOCKETCOMMANDINFO_T78D3A11DD086E0229D1AD938B61A9A9FC5CB133D_H
#ifndef __STATICARRAYINITTYPESIZEU3D36_T5B0C0E5859692BC733B20EE930353284B7AB95E4_H
#define __STATICARRAYINITTYPESIZEU3D36_T5B0C0E5859692BC733B20EE930353284B7AB95E4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>U7B19DCFADBU2DF7C0U2D46D9U2DA532U2DDE6625B321CEU7D___StaticArrayInitTypeSizeU3D36
struct  __StaticArrayInitTypeSizeU3D36_t5B0C0E5859692BC733B20EE930353284B7AB95E4 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D36_t5B0C0E5859692BC733B20EE930353284B7AB95E4__padding[36];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D36_T5B0C0E5859692BC733B20EE930353284B7AB95E4_H
#ifndef ABSTRACTTLSCLIENT_TC6DAEE1B4537D4C522014C4971FC936BD5C964F7_H
#define ABSTRACTTLSCLIENT_TC6DAEE1B4537D4C522014C4971FC936BD5C964F7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.AbstractTlsClient
struct  AbstractTlsClient_tC6DAEE1B4537D4C522014C4971FC936BD5C964F7  : public AbstractTlsPeer_t26CA3EF032C693BEB7331E2A0972A8620596CC9F
{
public:
	// Org.BouncyCastle.Crypto.Tls.TlsCipherFactory Org.BouncyCastle.Crypto.Tls.AbstractTlsClient::mCipherFactory
	RuntimeObject* ___mCipherFactory_0;
	// Org.BouncyCastle.Crypto.Tls.TlsClientContext Org.BouncyCastle.Crypto.Tls.AbstractTlsClient::mContext
	RuntimeObject* ___mContext_1;
	// System.Collections.IList Org.BouncyCastle.Crypto.Tls.AbstractTlsClient::mSupportedSignatureAlgorithms
	RuntimeObject* ___mSupportedSignatureAlgorithms_2;
	// System.Int32[] Org.BouncyCastle.Crypto.Tls.AbstractTlsClient::mNamedCurves
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___mNamedCurves_3;
	// System.Byte[] Org.BouncyCastle.Crypto.Tls.AbstractTlsClient::mClientECPointFormats
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mClientECPointFormats_4;
	// System.Byte[] Org.BouncyCastle.Crypto.Tls.AbstractTlsClient::mServerECPointFormats
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mServerECPointFormats_5;
	// System.Int32 Org.BouncyCastle.Crypto.Tls.AbstractTlsClient::mSelectedCipherSuite
	int32_t ___mSelectedCipherSuite_6;
	// System.Int16 Org.BouncyCastle.Crypto.Tls.AbstractTlsClient::mSelectedCompressionMethod
	int16_t ___mSelectedCompressionMethod_7;

public:
	inline static int32_t get_offset_of_mCipherFactory_0() { return static_cast<int32_t>(offsetof(AbstractTlsClient_tC6DAEE1B4537D4C522014C4971FC936BD5C964F7, ___mCipherFactory_0)); }
	inline RuntimeObject* get_mCipherFactory_0() const { return ___mCipherFactory_0; }
	inline RuntimeObject** get_address_of_mCipherFactory_0() { return &___mCipherFactory_0; }
	inline void set_mCipherFactory_0(RuntimeObject* value)
	{
		___mCipherFactory_0 = value;
		Il2CppCodeGenWriteBarrier((&___mCipherFactory_0), value);
	}

	inline static int32_t get_offset_of_mContext_1() { return static_cast<int32_t>(offsetof(AbstractTlsClient_tC6DAEE1B4537D4C522014C4971FC936BD5C964F7, ___mContext_1)); }
	inline RuntimeObject* get_mContext_1() const { return ___mContext_1; }
	inline RuntimeObject** get_address_of_mContext_1() { return &___mContext_1; }
	inline void set_mContext_1(RuntimeObject* value)
	{
		___mContext_1 = value;
		Il2CppCodeGenWriteBarrier((&___mContext_1), value);
	}

	inline static int32_t get_offset_of_mSupportedSignatureAlgorithms_2() { return static_cast<int32_t>(offsetof(AbstractTlsClient_tC6DAEE1B4537D4C522014C4971FC936BD5C964F7, ___mSupportedSignatureAlgorithms_2)); }
	inline RuntimeObject* get_mSupportedSignatureAlgorithms_2() const { return ___mSupportedSignatureAlgorithms_2; }
	inline RuntimeObject** get_address_of_mSupportedSignatureAlgorithms_2() { return &___mSupportedSignatureAlgorithms_2; }
	inline void set_mSupportedSignatureAlgorithms_2(RuntimeObject* value)
	{
		___mSupportedSignatureAlgorithms_2 = value;
		Il2CppCodeGenWriteBarrier((&___mSupportedSignatureAlgorithms_2), value);
	}

	inline static int32_t get_offset_of_mNamedCurves_3() { return static_cast<int32_t>(offsetof(AbstractTlsClient_tC6DAEE1B4537D4C522014C4971FC936BD5C964F7, ___mNamedCurves_3)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_mNamedCurves_3() const { return ___mNamedCurves_3; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_mNamedCurves_3() { return &___mNamedCurves_3; }
	inline void set_mNamedCurves_3(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___mNamedCurves_3 = value;
		Il2CppCodeGenWriteBarrier((&___mNamedCurves_3), value);
	}

	inline static int32_t get_offset_of_mClientECPointFormats_4() { return static_cast<int32_t>(offsetof(AbstractTlsClient_tC6DAEE1B4537D4C522014C4971FC936BD5C964F7, ___mClientECPointFormats_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mClientECPointFormats_4() const { return ___mClientECPointFormats_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mClientECPointFormats_4() { return &___mClientECPointFormats_4; }
	inline void set_mClientECPointFormats_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mClientECPointFormats_4 = value;
		Il2CppCodeGenWriteBarrier((&___mClientECPointFormats_4), value);
	}

	inline static int32_t get_offset_of_mServerECPointFormats_5() { return static_cast<int32_t>(offsetof(AbstractTlsClient_tC6DAEE1B4537D4C522014C4971FC936BD5C964F7, ___mServerECPointFormats_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mServerECPointFormats_5() const { return ___mServerECPointFormats_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mServerECPointFormats_5() { return &___mServerECPointFormats_5; }
	inline void set_mServerECPointFormats_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mServerECPointFormats_5 = value;
		Il2CppCodeGenWriteBarrier((&___mServerECPointFormats_5), value);
	}

	inline static int32_t get_offset_of_mSelectedCipherSuite_6() { return static_cast<int32_t>(offsetof(AbstractTlsClient_tC6DAEE1B4537D4C522014C4971FC936BD5C964F7, ___mSelectedCipherSuite_6)); }
	inline int32_t get_mSelectedCipherSuite_6() const { return ___mSelectedCipherSuite_6; }
	inline int32_t* get_address_of_mSelectedCipherSuite_6() { return &___mSelectedCipherSuite_6; }
	inline void set_mSelectedCipherSuite_6(int32_t value)
	{
		___mSelectedCipherSuite_6 = value;
	}

	inline static int32_t get_offset_of_mSelectedCompressionMethod_7() { return static_cast<int32_t>(offsetof(AbstractTlsClient_tC6DAEE1B4537D4C522014C4971FC936BD5C964F7, ___mSelectedCompressionMethod_7)); }
	inline int16_t get_mSelectedCompressionMethod_7() const { return ___mSelectedCompressionMethod_7; }
	inline int16_t* get_address_of_mSelectedCompressionMethod_7() { return &___mSelectedCompressionMethod_7; }
	inline void set_mSelectedCompressionMethod_7(int16_t value)
	{
		___mSelectedCompressionMethod_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTTLSCLIENT_TC6DAEE1B4537D4C522014C4971FC936BD5C964F7_H
#ifndef DATAEVENTARGS_TAD557B50CCC2D8F34E8B607EF7B6A1C7FA6614A3_H
#define DATAEVENTARGS_TAD557B50CCC2D8F34E8B607EF7B6A1C7FA6614A3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SuperSocket.ClientEngine.DataEventArgs
struct  DataEventArgs_tAD557B50CCC2D8F34E8B607EF7B6A1C7FA6614A3  : public EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E
{
public:
	// System.Byte[] SuperSocket.ClientEngine.DataEventArgs::<Data>k__BackingField
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___U3CDataU3Ek__BackingField_1;
	// System.Int32 SuperSocket.ClientEngine.DataEventArgs::<Offset>k__BackingField
	int32_t ___U3COffsetU3Ek__BackingField_2;
	// System.Int32 SuperSocket.ClientEngine.DataEventArgs::<Length>k__BackingField
	int32_t ___U3CLengthU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(DataEventArgs_tAD557B50CCC2D8F34E8B607EF7B6A1C7FA6614A3, ___U3CDataU3Ek__BackingField_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_U3CDataU3Ek__BackingField_1() const { return ___U3CDataU3Ek__BackingField_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_U3CDataU3Ek__BackingField_1() { return &___U3CDataU3Ek__BackingField_1; }
	inline void set_U3CDataU3Ek__BackingField_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___U3CDataU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3COffsetU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(DataEventArgs_tAD557B50CCC2D8F34E8B607EF7B6A1C7FA6614A3, ___U3COffsetU3Ek__BackingField_2)); }
	inline int32_t get_U3COffsetU3Ek__BackingField_2() const { return ___U3COffsetU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3COffsetU3Ek__BackingField_2() { return &___U3COffsetU3Ek__BackingField_2; }
	inline void set_U3COffsetU3Ek__BackingField_2(int32_t value)
	{
		___U3COffsetU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CLengthU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(DataEventArgs_tAD557B50CCC2D8F34E8B607EF7B6A1C7FA6614A3, ___U3CLengthU3Ek__BackingField_3)); }
	inline int32_t get_U3CLengthU3Ek__BackingField_3() const { return ___U3CLengthU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CLengthU3Ek__BackingField_3() { return &___U3CLengthU3Ek__BackingField_3; }
	inline void set_U3CLengthU3Ek__BackingField_3(int32_t value)
	{
		___U3CLengthU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATAEVENTARGS_TAD557B50CCC2D8F34E8B607EF7B6A1C7FA6614A3_H
#ifndef ERROREVENTARGS_T5AA425041F8070B3C1E447B51615536ECDB56D78_H
#define ERROREVENTARGS_T5AA425041F8070B3C1E447B51615536ECDB56D78_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SuperSocket.ClientEngine.ErrorEventArgs
struct  ErrorEventArgs_t5AA425041F8070B3C1E447B51615536ECDB56D78  : public EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E
{
public:
	// System.Exception SuperSocket.ClientEngine.ErrorEventArgs::<Exception>k__BackingField
	Exception_t * ___U3CExceptionU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CExceptionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ErrorEventArgs_t5AA425041F8070B3C1E447B51615536ECDB56D78, ___U3CExceptionU3Ek__BackingField_1)); }
	inline Exception_t * get_U3CExceptionU3Ek__BackingField_1() const { return ___U3CExceptionU3Ek__BackingField_1; }
	inline Exception_t ** get_address_of_U3CExceptionU3Ek__BackingField_1() { return &___U3CExceptionU3Ek__BackingField_1; }
	inline void set_U3CExceptionU3Ek__BackingField_1(Exception_t * value)
	{
		___U3CExceptionU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CExceptionU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ERROREVENTARGS_T5AA425041F8070B3C1E447B51615536ECDB56D78_H
#ifndef ARRAYSEGMENTLIST_T3FB5043710933FF6704A1AFCE6295DE8CE401F18_H
#define ARRAYSEGMENTLIST_T3FB5043710933FF6704A1AFCE6295DE8CE401F18_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SuperSocket.ClientEngine.Protocol.ArraySegmentList
struct  ArraySegmentList_t3FB5043710933FF6704A1AFCE6295DE8CE401F18  : public ArraySegmentList_1_t967C28DFCD37B71848E14923C7F59D43D7DB33A6
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRAYSEGMENTLIST_T3FB5043710933FF6704A1AFCE6295DE8CE401F18_H
#ifndef HTTPCONNECTPROXY_T2F371BBDAC376A0D71C428D002057AEAAECD341E_H
#define HTTPCONNECTPROXY_T2F371BBDAC376A0D71C428D002057AEAAECD341E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SuperSocket.ClientEngine.Proxy.HttpConnectProxy
struct  HttpConnectProxy_t2F371BBDAC376A0D71C428D002057AEAAECD341E  : public ProxyConnectorBase_t66DBD023064F63BFABF4E28901CEF3C83BC92A09
{
public:
	// System.Int32 SuperSocket.ClientEngine.Proxy.HttpConnectProxy::m_ReceiveBufferSize
	int32_t ___m_ReceiveBufferSize_4;

public:
	inline static int32_t get_offset_of_m_ReceiveBufferSize_4() { return static_cast<int32_t>(offsetof(HttpConnectProxy_t2F371BBDAC376A0D71C428D002057AEAAECD341E, ___m_ReceiveBufferSize_4)); }
	inline int32_t get_m_ReceiveBufferSize_4() const { return ___m_ReceiveBufferSize_4; }
	inline int32_t* get_address_of_m_ReceiveBufferSize_4() { return &___m_ReceiveBufferSize_4; }
	inline void set_m_ReceiveBufferSize_4(int32_t value)
	{
		___m_ReceiveBufferSize_4 = value;
	}
};

struct HttpConnectProxy_t2F371BBDAC376A0D71C428D002057AEAAECD341E_StaticFields
{
public:
	// System.Byte[] SuperSocket.ClientEngine.Proxy.HttpConnectProxy::m_LineSeparator
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___m_LineSeparator_3;

public:
	inline static int32_t get_offset_of_m_LineSeparator_3() { return static_cast<int32_t>(offsetof(HttpConnectProxy_t2F371BBDAC376A0D71C428D002057AEAAECD341E_StaticFields, ___m_LineSeparator_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_m_LineSeparator_3() const { return ___m_LineSeparator_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_m_LineSeparator_3() { return &___m_LineSeparator_3; }
	inline void set_m_LineSeparator_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___m_LineSeparator_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_LineSeparator_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPCONNECTPROXY_T2F371BBDAC376A0D71C428D002057AEAAECD341E_H
#ifndef PROXYEVENTARGS_T13ACBA30C49309CAD705A1AF7AC4229845542B49_H
#define PROXYEVENTARGS_T13ACBA30C49309CAD705A1AF7AC4229845542B49_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SuperSocket.ClientEngine.ProxyEventArgs
struct  ProxyEventArgs_t13ACBA30C49309CAD705A1AF7AC4229845542B49  : public EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E
{
public:
	// System.Boolean SuperSocket.ClientEngine.ProxyEventArgs::<Connected>k__BackingField
	bool ___U3CConnectedU3Ek__BackingField_1;
	// System.Net.Sockets.Socket SuperSocket.ClientEngine.ProxyEventArgs::<Socket>k__BackingField
	Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 * ___U3CSocketU3Ek__BackingField_2;
	// System.Exception SuperSocket.ClientEngine.ProxyEventArgs::<Exception>k__BackingField
	Exception_t * ___U3CExceptionU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CConnectedU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ProxyEventArgs_t13ACBA30C49309CAD705A1AF7AC4229845542B49, ___U3CConnectedU3Ek__BackingField_1)); }
	inline bool get_U3CConnectedU3Ek__BackingField_1() const { return ___U3CConnectedU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CConnectedU3Ek__BackingField_1() { return &___U3CConnectedU3Ek__BackingField_1; }
	inline void set_U3CConnectedU3Ek__BackingField_1(bool value)
	{
		___U3CConnectedU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CSocketU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ProxyEventArgs_t13ACBA30C49309CAD705A1AF7AC4229845542B49, ___U3CSocketU3Ek__BackingField_2)); }
	inline Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 * get_U3CSocketU3Ek__BackingField_2() const { return ___U3CSocketU3Ek__BackingField_2; }
	inline Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 ** get_address_of_U3CSocketU3Ek__BackingField_2() { return &___U3CSocketU3Ek__BackingField_2; }
	inline void set_U3CSocketU3Ek__BackingField_2(Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 * value)
	{
		___U3CSocketU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSocketU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CExceptionU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ProxyEventArgs_t13ACBA30C49309CAD705A1AF7AC4229845542B49, ___U3CExceptionU3Ek__BackingField_3)); }
	inline Exception_t * get_U3CExceptionU3Ek__BackingField_3() const { return ___U3CExceptionU3Ek__BackingField_3; }
	inline Exception_t ** get_address_of_U3CExceptionU3Ek__BackingField_3() { return &___U3CExceptionU3Ek__BackingField_3; }
	inline void set_U3CExceptionU3Ek__BackingField_3(Exception_t * value)
	{
		___U3CExceptionU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CExceptionU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROXYEVENTARGS_T13ACBA30C49309CAD705A1AF7AC4229845542B49_H
#ifndef ARRAYSEGMENT_1_T5B17204266E698CC035E2A7F6435A4F78286D0FA_H
#define ARRAYSEGMENT_1_T5B17204266E698CC035E2A7F6435A4F78286D0FA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArraySegment`1<System.Byte>
struct  ArraySegment_1_t5B17204266E698CC035E2A7F6435A4F78286D0FA 
{
public:
	// T[] System.ArraySegment`1::_array
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____array_0;
	// System.Int32 System.ArraySegment`1::_offset
	int32_t ____offset_1;
	// System.Int32 System.ArraySegment`1::_count
	int32_t ____count_2;

public:
	inline static int32_t get_offset_of__array_0() { return static_cast<int32_t>(offsetof(ArraySegment_1_t5B17204266E698CC035E2A7F6435A4F78286D0FA, ____array_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__array_0() const { return ____array_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__array_0() { return &____array_0; }
	inline void set__array_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____array_0 = value;
		Il2CppCodeGenWriteBarrier((&____array_0), value);
	}

	inline static int32_t get_offset_of__offset_1() { return static_cast<int32_t>(offsetof(ArraySegment_1_t5B17204266E698CC035E2A7F6435A4F78286D0FA, ____offset_1)); }
	inline int32_t get__offset_1() const { return ____offset_1; }
	inline int32_t* get_address_of__offset_1() { return &____offset_1; }
	inline void set__offset_1(int32_t value)
	{
		____offset_1 = value;
	}

	inline static int32_t get_offset_of__count_2() { return static_cast<int32_t>(offsetof(ArraySegment_1_t5B17204266E698CC035E2A7F6435A4F78286D0FA, ____count_2)); }
	inline int32_t get__count_2() const { return ____count_2; }
	inline int32_t* get_address_of__count_2() { return &____count_2; }
	inline void set__count_2(int32_t value)
	{
		____count_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRAYSEGMENT_1_T5B17204266E698CC035E2A7F6435A4F78286D0FA_H
#ifndef DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#define DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 
{
public:
	// System.UInt64 System.DateTime::dateData
	uint64_t ___dateData_44;

public:
	inline static int32_t get_offset_of_dateData_44() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132, ___dateData_44)); }
	inline uint64_t get_dateData_44() const { return ___dateData_44; }
	inline uint64_t* get_address_of_dateData_44() { return &___dateData_44; }
	inline void set_dateData_44(uint64_t value)
	{
		___dateData_44 = value;
	}
};

struct DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields
{
public:
	// System.Int32[] System.DateTime::DaysToMonth365
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth365_29;
	// System.Int32[] System.DateTime::DaysToMonth366
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth366_30;
	// System.DateTime System.DateTime::MinValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MinValue_31;
	// System.DateTime System.DateTime::MaxValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MaxValue_32;

public:
	inline static int32_t get_offset_of_DaysToMonth365_29() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth365_29)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth365_29() const { return ___DaysToMonth365_29; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth365_29() { return &___DaysToMonth365_29; }
	inline void set_DaysToMonth365_29(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth365_29 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth365_29), value);
	}

	inline static int32_t get_offset_of_DaysToMonth366_30() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth366_30)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth366_30() const { return ___DaysToMonth366_30; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth366_30() { return &___DaysToMonth366_30; }
	inline void set_DaysToMonth366_30(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth366_30 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth366_30), value);
	}

	inline static int32_t get_offset_of_MinValue_31() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MinValue_31)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MinValue_31() const { return ___MinValue_31; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MinValue_31() { return &___MinValue_31; }
	inline void set_MinValue_31(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MinValue_31 = value;
	}

	inline static int32_t get_offset_of_MaxValue_32() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MaxValue_32)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MaxValue_32() const { return ___MaxValue_32; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MaxValue_32() { return &___MaxValue_32; }
	inline void set_MaxValue_32(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MaxValue_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#define STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Stream
struct  Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7  : public MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF
{
public:
	// System.IO.Stream_ReadWriteTask System.IO.Stream::_activeReadWriteTask
	ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * ____activeReadWriteTask_3;
	// System.Threading.SemaphoreSlim System.IO.Stream::_asyncActiveSemaphore
	SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * ____asyncActiveSemaphore_4;

public:
	inline static int32_t get_offset_of__activeReadWriteTask_3() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7, ____activeReadWriteTask_3)); }
	inline ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * get__activeReadWriteTask_3() const { return ____activeReadWriteTask_3; }
	inline ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 ** get_address_of__activeReadWriteTask_3() { return &____activeReadWriteTask_3; }
	inline void set__activeReadWriteTask_3(ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * value)
	{
		____activeReadWriteTask_3 = value;
		Il2CppCodeGenWriteBarrier((&____activeReadWriteTask_3), value);
	}

	inline static int32_t get_offset_of__asyncActiveSemaphore_4() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7, ____asyncActiveSemaphore_4)); }
	inline SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * get__asyncActiveSemaphore_4() const { return ____asyncActiveSemaphore_4; }
	inline SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 ** get_address_of__asyncActiveSemaphore_4() { return &____asyncActiveSemaphore_4; }
	inline void set__asyncActiveSemaphore_4(SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * value)
	{
		____asyncActiveSemaphore_4 = value;
		Il2CppCodeGenWriteBarrier((&____asyncActiveSemaphore_4), value);
	}
};

struct Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7_StaticFields
{
public:
	// System.IO.Stream System.IO.Stream::Null
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___Null_1;

public:
	inline static int32_t get_offset_of_Null_1() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7_StaticFields, ___Null_1)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_Null_1() const { return ___Null_1; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_Null_1() { return &___Null_1; }
	inline void set_Null_1(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___Null_1 = value;
		Il2CppCodeGenWriteBarrier((&___Null_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#ifndef INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#define INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef DNSENDPOINT2_TF8C7125EC532B5E62DBFDF73AFA6954FB54CE436_H
#define DNSENDPOINT2_TF8C7125EC532B5E62DBFDF73AFA6954FB54CE436_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.DnsEndPoint2
struct  DnsEndPoint2_tF8C7125EC532B5E62DBFDF73AFA6954FB54CE436  : public EndPoint_tD87FCEF2780A951E8CE8D808C345FBF2C088D980
{
public:
	// System.String System.Net.DnsEndPoint2::<Host>k__BackingField
	String_t* ___U3CHostU3Ek__BackingField_0;
	// System.Int32 System.Net.DnsEndPoint2::<Port>k__BackingField
	int32_t ___U3CPortU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CHostU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DnsEndPoint2_tF8C7125EC532B5E62DBFDF73AFA6954FB54CE436, ___U3CHostU3Ek__BackingField_0)); }
	inline String_t* get_U3CHostU3Ek__BackingField_0() const { return ___U3CHostU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CHostU3Ek__BackingField_0() { return &___U3CHostU3Ek__BackingField_0; }
	inline void set_U3CHostU3Ek__BackingField_0(String_t* value)
	{
		___U3CHostU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CHostU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CPortU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(DnsEndPoint2_tF8C7125EC532B5E62DBFDF73AFA6954FB54CE436, ___U3CPortU3Ek__BackingField_1)); }
	inline int32_t get_U3CPortU3Ek__BackingField_1() const { return ___U3CPortU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CPortU3Ek__BackingField_1() { return &___U3CPortU3Ek__BackingField_1; }
	inline void set_U3CPortU3Ek__BackingField_1(int32_t value)
	{
		___U3CPortU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DNSENDPOINT2_TF8C7125EC532B5E62DBFDF73AFA6954FB54CE436_H
#ifndef NULLABLE_1_TB4C4F9557481ABDC6DFB4E6C55B481A29DDCC299_H
#define NULLABLE_1_TB4C4F9557481ABDC6DFB4E6C55B481A29DDCC299_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Byte>
struct  Nullable_1_tB4C4F9557481ABDC6DFB4E6C55B481A29DDCC299 
{
public:
	// T System.Nullable`1::value
	uint8_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_tB4C4F9557481ABDC6DFB4E6C55B481A29DDCC299, ___value_0)); }
	inline uint8_t get_value_0() const { return ___value_0; }
	inline uint8_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(uint8_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_tB4C4F9557481ABDC6DFB4E6C55B481A29DDCC299, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_TB4C4F9557481ABDC6DFB4E6C55B481A29DDCC299_H
#ifndef NULLABLE_1_T0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB_H
#define NULLABLE_1_T0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Int32>
struct  Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef BADREQUEST_T3278D3848E90CD4D3251BD1D2239D650E08E44F5_H
#define BADREQUEST_T3278D3848E90CD4D3251BD1D2239D650E08E44F5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocket4Net.Command.BadRequest
struct  BadRequest_t3278D3848E90CD4D3251BD1D2239D650E08E44F5  : public WebSocketCommandBase_t2121F47322A7F3C4FC75E91FA1F46F3B39EE32D9
{
public:

public:
};

struct BadRequest_t3278D3848E90CD4D3251BD1D2239D650E08E44F5_StaticFields
{
public:
	// System.String[] WebSocket4Net.Command.BadRequest::m_ValueSeparator
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___m_ValueSeparator_0;

public:
	inline static int32_t get_offset_of_m_ValueSeparator_0() { return static_cast<int32_t>(offsetof(BadRequest_t3278D3848E90CD4D3251BD1D2239D650E08E44F5_StaticFields, ___m_ValueSeparator_0)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_m_ValueSeparator_0() const { return ___m_ValueSeparator_0; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_m_ValueSeparator_0() { return &___m_ValueSeparator_0; }
	inline void set_m_ValueSeparator_0(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___m_ValueSeparator_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ValueSeparator_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BADREQUEST_T3278D3848E90CD4D3251BD1D2239D650E08E44F5_H
#ifndef BINARY_TBF1D30556818C0850CBE7E48D973D1A4D0CC69E4_H
#define BINARY_TBF1D30556818C0850CBE7E48D973D1A4D0CC69E4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocket4Net.Command.Binary
struct  Binary_tBF1D30556818C0850CBE7E48D973D1A4D0CC69E4  : public WebSocketCommandBase_t2121F47322A7F3C4FC75E91FA1F46F3B39EE32D9
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINARY_TBF1D30556818C0850CBE7E48D973D1A4D0CC69E4_H
#ifndef CLOSE_T411DD4E4942770A52A577E19D51BDB3B823DD39A_H
#define CLOSE_T411DD4E4942770A52A577E19D51BDB3B823DD39A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocket4Net.Command.Close
struct  Close_t411DD4E4942770A52A577E19D51BDB3B823DD39A  : public WebSocketCommandBase_t2121F47322A7F3C4FC75E91FA1F46F3B39EE32D9
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLOSE_T411DD4E4942770A52A577E19D51BDB3B823DD39A_H
#ifndef HANDSHAKE_T2A500A4A75F9643C1E5264309BD51752A24F9BD8_H
#define HANDSHAKE_T2A500A4A75F9643C1E5264309BD51752A24F9BD8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocket4Net.Command.Handshake
struct  Handshake_t2A500A4A75F9643C1E5264309BD51752A24F9BD8  : public WebSocketCommandBase_t2121F47322A7F3C4FC75E91FA1F46F3B39EE32D9
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HANDSHAKE_T2A500A4A75F9643C1E5264309BD51752A24F9BD8_H
#ifndef PING_TE9E70FDB453BF6C9CFC1CF2E063B73910E8C887C_H
#define PING_TE9E70FDB453BF6C9CFC1CF2E063B73910E8C887C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocket4Net.Command.Ping
struct  Ping_tE9E70FDB453BF6C9CFC1CF2E063B73910E8C887C  : public WebSocketCommandBase_t2121F47322A7F3C4FC75E91FA1F46F3B39EE32D9
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PING_TE9E70FDB453BF6C9CFC1CF2E063B73910E8C887C_H
#ifndef PONG_T767A1B2AF987EA021BBA40BF5C84D2DF776DE56A_H
#define PONG_T767A1B2AF987EA021BBA40BF5C84D2DF776DE56A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocket4Net.Command.Pong
struct  Pong_t767A1B2AF987EA021BBA40BF5C84D2DF776DE56A  : public WebSocketCommandBase_t2121F47322A7F3C4FC75E91FA1F46F3B39EE32D9
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PONG_T767A1B2AF987EA021BBA40BF5C84D2DF776DE56A_H
#ifndef TEXT_T7814FFBFB79538E40C988D03428C81D2CC353F53_H
#define TEXT_T7814FFBFB79538E40C988D03428C81D2CC353F53_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocket4Net.Command.Text
struct  Text_t7814FFBFB79538E40C988D03428C81D2CC353F53  : public WebSocketCommandBase_t2121F47322A7F3C4FC75E91FA1F46F3B39EE32D9
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXT_T7814FFBFB79538E40C988D03428C81D2CC353F53_H
#ifndef DATARECEIVEDEVENTARGS_T60F42187E42DA680BDD0246C783FE2D8FB9BEFE9_H
#define DATARECEIVEDEVENTARGS_T60F42187E42DA680BDD0246C783FE2D8FB9BEFE9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocket4Net.DataReceivedEventArgs
struct  DataReceivedEventArgs_t60F42187E42DA680BDD0246C783FE2D8FB9BEFE9  : public EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E
{
public:
	// System.Byte[] WebSocket4Net.DataReceivedEventArgs::<Data>k__BackingField
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___U3CDataU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(DataReceivedEventArgs_t60F42187E42DA680BDD0246C783FE2D8FB9BEFE9, ___U3CDataU3Ek__BackingField_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_U3CDataU3Ek__BackingField_1() const { return ___U3CDataU3Ek__BackingField_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_U3CDataU3Ek__BackingField_1() { return &___U3CDataU3Ek__BackingField_1; }
	inline void set_U3CDataU3Ek__BackingField_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___U3CDataU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATARECEIVEDEVENTARGS_T60F42187E42DA680BDD0246C783FE2D8FB9BEFE9_H
#ifndef MESSAGERECEIVEDEVENTARGS_T5A823DF5C37C8706FC40BD7B27ABD7FE50C8736B_H
#define MESSAGERECEIVEDEVENTARGS_T5A823DF5C37C8706FC40BD7B27ABD7FE50C8736B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocket4Net.MessageReceivedEventArgs
struct  MessageReceivedEventArgs_t5A823DF5C37C8706FC40BD7B27ABD7FE50C8736B  : public EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E
{
public:
	// System.String WebSocket4Net.MessageReceivedEventArgs::<Message>k__BackingField
	String_t* ___U3CMessageU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CMessageU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(MessageReceivedEventArgs_t5A823DF5C37C8706FC40BD7B27ABD7FE50C8736B, ___U3CMessageU3Ek__BackingField_1)); }
	inline String_t* get_U3CMessageU3Ek__BackingField_1() const { return ___U3CMessageU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CMessageU3Ek__BackingField_1() { return &___U3CMessageU3Ek__BackingField_1; }
	inline void set_U3CMessageU3Ek__BackingField_1(String_t* value)
	{
		___U3CMessageU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMessageU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGERECEIVEDEVENTARGS_T5A823DF5C37C8706FC40BD7B27ABD7FE50C8736B_H
#ifndef EXTENDEDLENGHTREADER_T4CA69182EE1EF98A06A1D5EC74B8F2C5A95D7DF1_H
#define EXTENDEDLENGHTREADER_T4CA69182EE1EF98A06A1D5EC74B8F2C5A95D7DF1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocket4Net.Protocol.FramePartReader.ExtendedLenghtReader
struct  ExtendedLenghtReader_t4CA69182EE1EF98A06A1D5EC74B8F2C5A95D7DF1  : public DataFramePartReader_tDE67A43BE1F64AE2D208BBCFAF2AF25933B20221
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENDEDLENGHTREADER_T4CA69182EE1EF98A06A1D5EC74B8F2C5A95D7DF1_H
#ifndef FIXPARTREADER_TB1C89F0B19F35C09955DCC9BEFD9A748B65749E6_H
#define FIXPARTREADER_TB1C89F0B19F35C09955DCC9BEFD9A748B65749E6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocket4Net.Protocol.FramePartReader.FixPartReader
struct  FixPartReader_tB1C89F0B19F35C09955DCC9BEFD9A748B65749E6  : public DataFramePartReader_tDE67A43BE1F64AE2D208BBCFAF2AF25933B20221
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIXPARTREADER_TB1C89F0B19F35C09955DCC9BEFD9A748B65749E6_H
#ifndef MASKKEYREADER_T9D4908BF0FB59B52E3D88077AF5CD76080018A69_H
#define MASKKEYREADER_T9D4908BF0FB59B52E3D88077AF5CD76080018A69_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocket4Net.Protocol.FramePartReader.MaskKeyReader
struct  MaskKeyReader_t9D4908BF0FB59B52E3D88077AF5CD76080018A69  : public DataFramePartReader_tDE67A43BE1F64AE2D208BBCFAF2AF25933B20221
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKKEYREADER_T9D4908BF0FB59B52E3D88077AF5CD76080018A69_H
#ifndef PAYLOADDATAREADER_T7FD1E46C718C976D124956DE45BC4AEA45D49875_H
#define PAYLOADDATAREADER_T7FD1E46C718C976D124956DE45BC4AEA45D49875_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocket4Net.Protocol.FramePartReader.PayloadDataReader
struct  PayloadDataReader_t7FD1E46C718C976D124956DE45BC4AEA45D49875  : public DataFramePartReader_tDE67A43BE1F64AE2D208BBCFAF2AF25933B20221
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAYLOADDATAREADER_T7FD1E46C718C976D124956DE45BC4AEA45D49875_H
#ifndef HANDSHAKEREADER_T247979AEC98CA1018DA56534ECCD1EBA23CBFC0C_H
#define HANDSHAKEREADER_T247979AEC98CA1018DA56534ECCD1EBA23CBFC0C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocket4Net.Protocol.HandshakeReader
struct  HandshakeReader_t247979AEC98CA1018DA56534ECCD1EBA23CBFC0C  : public ReaderBase_t57E7CDB15563532730AB07F0FF785C424397BAC6
{
public:
	// SuperSocket.ClientEngine.SearchMarkState`1<System.Byte> WebSocket4Net.Protocol.HandshakeReader::m_HeadSeachState
	SearchMarkState_1_t9CA9B71862282152C0C864021BBEE57C176B07D3 * ___m_HeadSeachState_5;

public:
	inline static int32_t get_offset_of_m_HeadSeachState_5() { return static_cast<int32_t>(offsetof(HandshakeReader_t247979AEC98CA1018DA56534ECCD1EBA23CBFC0C, ___m_HeadSeachState_5)); }
	inline SearchMarkState_1_t9CA9B71862282152C0C864021BBEE57C176B07D3 * get_m_HeadSeachState_5() const { return ___m_HeadSeachState_5; }
	inline SearchMarkState_1_t9CA9B71862282152C0C864021BBEE57C176B07D3 ** get_address_of_m_HeadSeachState_5() { return &___m_HeadSeachState_5; }
	inline void set_m_HeadSeachState_5(SearchMarkState_1_t9CA9B71862282152C0C864021BBEE57C176B07D3 * value)
	{
		___m_HeadSeachState_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_HeadSeachState_5), value);
	}
};

struct HandshakeReader_t247979AEC98CA1018DA56534ECCD1EBA23CBFC0C_StaticFields
{
public:
	// System.String WebSocket4Net.Protocol.HandshakeReader::BadRequestCode
	String_t* ___BadRequestCode_3;
	// System.Byte[] WebSocket4Net.Protocol.HandshakeReader::HeaderTerminator
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___HeaderTerminator_4;

public:
	inline static int32_t get_offset_of_BadRequestCode_3() { return static_cast<int32_t>(offsetof(HandshakeReader_t247979AEC98CA1018DA56534ECCD1EBA23CBFC0C_StaticFields, ___BadRequestCode_3)); }
	inline String_t* get_BadRequestCode_3() const { return ___BadRequestCode_3; }
	inline String_t** get_address_of_BadRequestCode_3() { return &___BadRequestCode_3; }
	inline void set_BadRequestCode_3(String_t* value)
	{
		___BadRequestCode_3 = value;
		Il2CppCodeGenWriteBarrier((&___BadRequestCode_3), value);
	}

	inline static int32_t get_offset_of_HeaderTerminator_4() { return static_cast<int32_t>(offsetof(HandshakeReader_t247979AEC98CA1018DA56534ECCD1EBA23CBFC0C_StaticFields, ___HeaderTerminator_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_HeaderTerminator_4() const { return ___HeaderTerminator_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_HeaderTerminator_4() { return &___HeaderTerminator_4; }
	inline void set_HeaderTerminator_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___HeaderTerminator_4 = value;
		Il2CppCodeGenWriteBarrier((&___HeaderTerminator_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HANDSHAKEREADER_T247979AEC98CA1018DA56534ECCD1EBA23CBFC0C_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3EU7B19DCFADBU2DF7C0U2D46D9U2DA532U2DDE6625B321CEU7D_T15CEDF0DBF0B8C9B4B10E7F1889BD810545675FB_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3EU7B19DCFADBU2DF7C0U2D46D9U2DA532U2DDE6625B321CEU7D_T15CEDF0DBF0B8C9B4B10E7F1889BD810545675FB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>U7B19DCFADBU2DF7C0U2D46D9U2DA532U2DDE6625B321CEU7D
struct  U3CPrivateImplementationDetailsU3EU7B19DCFADBU2DF7C0U2D46D9U2DA532U2DDE6625B321CEU7D_t15CEDF0DBF0B8C9B4B10E7F1889BD810545675FB  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3EU7B19DCFADBU2DF7C0U2D46D9U2DA532U2DDE6625B321CEU7D_t15CEDF0DBF0B8C9B4B10E7F1889BD810545675FB_StaticFields
{
public:
	// <PrivateImplementationDetails>U7B19DCFADBU2DF7C0U2D46D9U2DA532U2DDE6625B321CEU7D___StaticArrayInitTypeSizeU3D36 <PrivateImplementationDetails>U7B19DCFADBU2DF7C0U2D46D9U2DA532U2DDE6625B321CEU7D::U24U24method0x60002a4U2D1
	__StaticArrayInitTypeSizeU3D36_t5B0C0E5859692BC733B20EE930353284B7AB95E4  ___U24U24method0x60002a4U2D1_0;

public:
	inline static int32_t get_offset_of_U24U24method0x60002a4U2D1_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7B19DCFADBU2DF7C0U2D46D9U2DA532U2DDE6625B321CEU7D_t15CEDF0DBF0B8C9B4B10E7F1889BD810545675FB_StaticFields, ___U24U24method0x60002a4U2D1_0)); }
	inline __StaticArrayInitTypeSizeU3D36_t5B0C0E5859692BC733B20EE930353284B7AB95E4  get_U24U24method0x60002a4U2D1_0() const { return ___U24U24method0x60002a4U2D1_0; }
	inline __StaticArrayInitTypeSizeU3D36_t5B0C0E5859692BC733B20EE930353284B7AB95E4 * get_address_of_U24U24method0x60002a4U2D1_0() { return &___U24U24method0x60002a4U2D1_0; }
	inline void set_U24U24method0x60002a4U2D1_0(__StaticArrayInitTypeSizeU3D36_t5B0C0E5859692BC733B20EE930353284B7AB95E4  value)
	{
		___U24U24method0x60002a4U2D1_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3EU7B19DCFADBU2DF7C0U2D46D9U2DA532U2DDE6625B321CEU7D_T15CEDF0DBF0B8C9B4B10E7F1889BD810545675FB_H
#ifndef GAMESPARKSWEBSOCKETSTATE_T1581131C5935866AAF3021C070F016DC5ABDAAC1_H
#define GAMESPARKSWEBSOCKETSTATE_T1581131C5935866AAF3021C070F016DC5ABDAAC1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.GameSparksWebSocketState
struct  GameSparksWebSocketState_t1581131C5935866AAF3021C070F016DC5ABDAAC1 
{
public:
	// System.Int32 GameSparks.GameSparksWebSocketState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GameSparksWebSocketState_t1581131C5935866AAF3021C070F016DC5ABDAAC1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMESPARKSWEBSOCKETSTATE_T1581131C5935866AAF3021C070F016DC5ABDAAC1_H
#ifndef DUPLEXTLSSTREAM_T5F3DDF883886B8B18D0F12C2D0DFD16D9DC53768_H
#define DUPLEXTLSSTREAM_T5F3DDF883886B8B18D0F12C2D0DFD16D9DC53768_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.RT.DuplexTlsStream
struct  DuplexTlsStream_t5F3DDF883886B8B18D0F12C2D0DFD16D9DC53768  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// System.IO.Stream GameSparks.RT.DuplexTlsStream::wrapped
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___wrapped_5;

public:
	inline static int32_t get_offset_of_wrapped_5() { return static_cast<int32_t>(offsetof(DuplexTlsStream_t5F3DDF883886B8B18D0F12C2D0DFD16D9DC53768, ___wrapped_5)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_wrapped_5() const { return ___wrapped_5; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_wrapped_5() { return &___wrapped_5; }
	inline void set_wrapped_5(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___wrapped_5 = value;
		Il2CppCodeGenWriteBarrier((&___wrapped_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DUPLEXTLSSTREAM_T5F3DDF883886B8B18D0F12C2D0DFD16D9DC53768_H
#ifndef DEFAULTTLSCLIENT_T3AEEC6138E8000BF0EB194E242413CB61B9EFF0E_H
#define DEFAULTTLSCLIENT_T3AEEC6138E8000BF0EB194E242413CB61B9EFF0E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.DefaultTlsClient
struct  DefaultTlsClient_t3AEEC6138E8000BF0EB194E242413CB61B9EFF0E  : public AbstractTlsClient_tC6DAEE1B4537D4C522014C4971FC936BD5C964F7
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTTLSCLIENT_T3AEEC6138E8000BF0EB194E242413CB61B9EFF0E_H
#ifndef CLIENTSESSION_T9A79D55CDA93B439BD063F2DD8D73084EB748A98_H
#define CLIENTSESSION_T9A79D55CDA93B439BD063F2DD8D73084EB748A98_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SuperSocket.ClientEngine.ClientSession
struct  ClientSession_t9A79D55CDA93B439BD063F2DD8D73084EB748A98  : public RuntimeObject
{
public:
	// System.EventHandler SuperSocket.ClientEngine.ClientSession::m_Closed
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * ___m_Closed_0;
	// System.EventHandler`1<SuperSocket.ClientEngine.ErrorEventArgs> SuperSocket.ClientEngine.ClientSession::m_Error
	EventHandler_1_t191D441D6952099231D3D3122D875173BEA68907 * ___m_Error_1;
	// System.EventHandler SuperSocket.ClientEngine.ClientSession::m_Connected
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * ___m_Connected_2;
	// System.EventHandler`1<SuperSocket.ClientEngine.DataEventArgs> SuperSocket.ClientEngine.ClientSession::m_DataReceived
	EventHandler_1_tC90AC821DF9506E308EC2B34BE4D3DF05C7E4513 * ___m_DataReceived_3;
	// SuperSocket.ClientEngine.DataEventArgs SuperSocket.ClientEngine.ClientSession::m_DataArgs
	DataEventArgs_tAD557B50CCC2D8F34E8B607EF7B6A1C7FA6614A3 * ___m_DataArgs_4;
	// System.Net.Sockets.Socket SuperSocket.ClientEngine.ClientSession::<Client>k__BackingField
	Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 * ___U3CClientU3Ek__BackingField_5;
	// System.Net.EndPoint SuperSocket.ClientEngine.ClientSession::<RemoteEndPoint>k__BackingField
	EndPoint_tD87FCEF2780A951E8CE8D808C345FBF2C088D980 * ___U3CRemoteEndPointU3Ek__BackingField_6;
	// System.Boolean SuperSocket.ClientEngine.ClientSession::<IsConnected>k__BackingField
	bool ___U3CIsConnectedU3Ek__BackingField_7;
	// System.Boolean SuperSocket.ClientEngine.ClientSession::<NoDeplay>k__BackingField
	bool ___U3CNoDeplayU3Ek__BackingField_8;
	// System.Int32 SuperSocket.ClientEngine.ClientSession::<SendingQueueSize>k__BackingField
	int32_t ___U3CSendingQueueSizeU3Ek__BackingField_9;
	// System.Int32 SuperSocket.ClientEngine.ClientSession::<ReceiveBufferSize>k__BackingField
	int32_t ___U3CReceiveBufferSizeU3Ek__BackingField_10;
	// SuperSocket.ClientEngine.IProxyConnector SuperSocket.ClientEngine.ClientSession::<Proxy>k__BackingField
	RuntimeObject* ___U3CProxyU3Ek__BackingField_11;
	// System.ArraySegment`1<System.Byte> SuperSocket.ClientEngine.ClientSession::<Buffer>k__BackingField
	ArraySegment_1_t5B17204266E698CC035E2A7F6435A4F78286D0FA  ___U3CBufferU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_m_Closed_0() { return static_cast<int32_t>(offsetof(ClientSession_t9A79D55CDA93B439BD063F2DD8D73084EB748A98, ___m_Closed_0)); }
	inline EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * get_m_Closed_0() const { return ___m_Closed_0; }
	inline EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C ** get_address_of_m_Closed_0() { return &___m_Closed_0; }
	inline void set_m_Closed_0(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * value)
	{
		___m_Closed_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Closed_0), value);
	}

	inline static int32_t get_offset_of_m_Error_1() { return static_cast<int32_t>(offsetof(ClientSession_t9A79D55CDA93B439BD063F2DD8D73084EB748A98, ___m_Error_1)); }
	inline EventHandler_1_t191D441D6952099231D3D3122D875173BEA68907 * get_m_Error_1() const { return ___m_Error_1; }
	inline EventHandler_1_t191D441D6952099231D3D3122D875173BEA68907 ** get_address_of_m_Error_1() { return &___m_Error_1; }
	inline void set_m_Error_1(EventHandler_1_t191D441D6952099231D3D3122D875173BEA68907 * value)
	{
		___m_Error_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Error_1), value);
	}

	inline static int32_t get_offset_of_m_Connected_2() { return static_cast<int32_t>(offsetof(ClientSession_t9A79D55CDA93B439BD063F2DD8D73084EB748A98, ___m_Connected_2)); }
	inline EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * get_m_Connected_2() const { return ___m_Connected_2; }
	inline EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C ** get_address_of_m_Connected_2() { return &___m_Connected_2; }
	inline void set_m_Connected_2(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * value)
	{
		___m_Connected_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Connected_2), value);
	}

	inline static int32_t get_offset_of_m_DataReceived_3() { return static_cast<int32_t>(offsetof(ClientSession_t9A79D55CDA93B439BD063F2DD8D73084EB748A98, ___m_DataReceived_3)); }
	inline EventHandler_1_tC90AC821DF9506E308EC2B34BE4D3DF05C7E4513 * get_m_DataReceived_3() const { return ___m_DataReceived_3; }
	inline EventHandler_1_tC90AC821DF9506E308EC2B34BE4D3DF05C7E4513 ** get_address_of_m_DataReceived_3() { return &___m_DataReceived_3; }
	inline void set_m_DataReceived_3(EventHandler_1_tC90AC821DF9506E308EC2B34BE4D3DF05C7E4513 * value)
	{
		___m_DataReceived_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_DataReceived_3), value);
	}

	inline static int32_t get_offset_of_m_DataArgs_4() { return static_cast<int32_t>(offsetof(ClientSession_t9A79D55CDA93B439BD063F2DD8D73084EB748A98, ___m_DataArgs_4)); }
	inline DataEventArgs_tAD557B50CCC2D8F34E8B607EF7B6A1C7FA6614A3 * get_m_DataArgs_4() const { return ___m_DataArgs_4; }
	inline DataEventArgs_tAD557B50CCC2D8F34E8B607EF7B6A1C7FA6614A3 ** get_address_of_m_DataArgs_4() { return &___m_DataArgs_4; }
	inline void set_m_DataArgs_4(DataEventArgs_tAD557B50CCC2D8F34E8B607EF7B6A1C7FA6614A3 * value)
	{
		___m_DataArgs_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_DataArgs_4), value);
	}

	inline static int32_t get_offset_of_U3CClientU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(ClientSession_t9A79D55CDA93B439BD063F2DD8D73084EB748A98, ___U3CClientU3Ek__BackingField_5)); }
	inline Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 * get_U3CClientU3Ek__BackingField_5() const { return ___U3CClientU3Ek__BackingField_5; }
	inline Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 ** get_address_of_U3CClientU3Ek__BackingField_5() { return &___U3CClientU3Ek__BackingField_5; }
	inline void set_U3CClientU3Ek__BackingField_5(Socket_t47148BFA7740C9C45A69F2F3722F734B9DCA45D8 * value)
	{
		___U3CClientU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CClientU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CRemoteEndPointU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(ClientSession_t9A79D55CDA93B439BD063F2DD8D73084EB748A98, ___U3CRemoteEndPointU3Ek__BackingField_6)); }
	inline EndPoint_tD87FCEF2780A951E8CE8D808C345FBF2C088D980 * get_U3CRemoteEndPointU3Ek__BackingField_6() const { return ___U3CRemoteEndPointU3Ek__BackingField_6; }
	inline EndPoint_tD87FCEF2780A951E8CE8D808C345FBF2C088D980 ** get_address_of_U3CRemoteEndPointU3Ek__BackingField_6() { return &___U3CRemoteEndPointU3Ek__BackingField_6; }
	inline void set_U3CRemoteEndPointU3Ek__BackingField_6(EndPoint_tD87FCEF2780A951E8CE8D808C345FBF2C088D980 * value)
	{
		___U3CRemoteEndPointU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRemoteEndPointU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CIsConnectedU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(ClientSession_t9A79D55CDA93B439BD063F2DD8D73084EB748A98, ___U3CIsConnectedU3Ek__BackingField_7)); }
	inline bool get_U3CIsConnectedU3Ek__BackingField_7() const { return ___U3CIsConnectedU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CIsConnectedU3Ek__BackingField_7() { return &___U3CIsConnectedU3Ek__BackingField_7; }
	inline void set_U3CIsConnectedU3Ek__BackingField_7(bool value)
	{
		___U3CIsConnectedU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CNoDeplayU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(ClientSession_t9A79D55CDA93B439BD063F2DD8D73084EB748A98, ___U3CNoDeplayU3Ek__BackingField_8)); }
	inline bool get_U3CNoDeplayU3Ek__BackingField_8() const { return ___U3CNoDeplayU3Ek__BackingField_8; }
	inline bool* get_address_of_U3CNoDeplayU3Ek__BackingField_8() { return &___U3CNoDeplayU3Ek__BackingField_8; }
	inline void set_U3CNoDeplayU3Ek__BackingField_8(bool value)
	{
		___U3CNoDeplayU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CSendingQueueSizeU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(ClientSession_t9A79D55CDA93B439BD063F2DD8D73084EB748A98, ___U3CSendingQueueSizeU3Ek__BackingField_9)); }
	inline int32_t get_U3CSendingQueueSizeU3Ek__BackingField_9() const { return ___U3CSendingQueueSizeU3Ek__BackingField_9; }
	inline int32_t* get_address_of_U3CSendingQueueSizeU3Ek__BackingField_9() { return &___U3CSendingQueueSizeU3Ek__BackingField_9; }
	inline void set_U3CSendingQueueSizeU3Ek__BackingField_9(int32_t value)
	{
		___U3CSendingQueueSizeU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CReceiveBufferSizeU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(ClientSession_t9A79D55CDA93B439BD063F2DD8D73084EB748A98, ___U3CReceiveBufferSizeU3Ek__BackingField_10)); }
	inline int32_t get_U3CReceiveBufferSizeU3Ek__BackingField_10() const { return ___U3CReceiveBufferSizeU3Ek__BackingField_10; }
	inline int32_t* get_address_of_U3CReceiveBufferSizeU3Ek__BackingField_10() { return &___U3CReceiveBufferSizeU3Ek__BackingField_10; }
	inline void set_U3CReceiveBufferSizeU3Ek__BackingField_10(int32_t value)
	{
		___U3CReceiveBufferSizeU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CProxyU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(ClientSession_t9A79D55CDA93B439BD063F2DD8D73084EB748A98, ___U3CProxyU3Ek__BackingField_11)); }
	inline RuntimeObject* get_U3CProxyU3Ek__BackingField_11() const { return ___U3CProxyU3Ek__BackingField_11; }
	inline RuntimeObject** get_address_of_U3CProxyU3Ek__BackingField_11() { return &___U3CProxyU3Ek__BackingField_11; }
	inline void set_U3CProxyU3Ek__BackingField_11(RuntimeObject* value)
	{
		___U3CProxyU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CProxyU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CBufferU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(ClientSession_t9A79D55CDA93B439BD063F2DD8D73084EB748A98, ___U3CBufferU3Ek__BackingField_12)); }
	inline ArraySegment_1_t5B17204266E698CC035E2A7F6435A4F78286D0FA  get_U3CBufferU3Ek__BackingField_12() const { return ___U3CBufferU3Ek__BackingField_12; }
	inline ArraySegment_1_t5B17204266E698CC035E2A7F6435A4F78286D0FA * get_address_of_U3CBufferU3Ek__BackingField_12() { return &___U3CBufferU3Ek__BackingField_12; }
	inline void set_U3CBufferU3Ek__BackingField_12(ArraySegment_1_t5B17204266E698CC035E2A7F6435A4F78286D0FA  value)
	{
		___U3CBufferU3Ek__BackingField_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIENTSESSION_T9A79D55CDA93B439BD063F2DD8D73084EB748A98_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef DRAFTHYBI00DATAREADER_T1B8006EAAB3E4801F8FF6F5DCC4A1BE2631886F5_H
#define DRAFTHYBI00DATAREADER_T1B8006EAAB3E4801F8FF6F5DCC4A1BE2631886F5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocket4Net.Protocol.DraftHybi00DataReader
struct  DraftHybi00DataReader_t1B8006EAAB3E4801F8FF6F5DCC4A1BE2631886F5  : public ReaderBase_t57E7CDB15563532730AB07F0FF785C424397BAC6
{
public:
	// System.Nullable`1<System.Byte> WebSocket4Net.Protocol.DraftHybi00DataReader::m_Type
	Nullable_1_tB4C4F9557481ABDC6DFB4E6C55B481A29DDCC299  ___m_Type_3;
	// System.Int32 WebSocket4Net.Protocol.DraftHybi00DataReader::m_TempLength
	int32_t ___m_TempLength_4;
	// System.Nullable`1<System.Int32> WebSocket4Net.Protocol.DraftHybi00DataReader::m_Length
	Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  ___m_Length_5;

public:
	inline static int32_t get_offset_of_m_Type_3() { return static_cast<int32_t>(offsetof(DraftHybi00DataReader_t1B8006EAAB3E4801F8FF6F5DCC4A1BE2631886F5, ___m_Type_3)); }
	inline Nullable_1_tB4C4F9557481ABDC6DFB4E6C55B481A29DDCC299  get_m_Type_3() const { return ___m_Type_3; }
	inline Nullable_1_tB4C4F9557481ABDC6DFB4E6C55B481A29DDCC299 * get_address_of_m_Type_3() { return &___m_Type_3; }
	inline void set_m_Type_3(Nullable_1_tB4C4F9557481ABDC6DFB4E6C55B481A29DDCC299  value)
	{
		___m_Type_3 = value;
	}

	inline static int32_t get_offset_of_m_TempLength_4() { return static_cast<int32_t>(offsetof(DraftHybi00DataReader_t1B8006EAAB3E4801F8FF6F5DCC4A1BE2631886F5, ___m_TempLength_4)); }
	inline int32_t get_m_TempLength_4() const { return ___m_TempLength_4; }
	inline int32_t* get_address_of_m_TempLength_4() { return &___m_TempLength_4; }
	inline void set_m_TempLength_4(int32_t value)
	{
		___m_TempLength_4 = value;
	}

	inline static int32_t get_offset_of_m_Length_5() { return static_cast<int32_t>(offsetof(DraftHybi00DataReader_t1B8006EAAB3E4801F8FF6F5DCC4A1BE2631886F5, ___m_Length_5)); }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  get_m_Length_5() const { return ___m_Length_5; }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB * get_address_of_m_Length_5() { return &___m_Length_5; }
	inline void set_m_Length_5(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  value)
	{
		___m_Length_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAFTHYBI00DATAREADER_T1B8006EAAB3E4801F8FF6F5DCC4A1BE2631886F5_H
#ifndef DRAFTHYBI00HANDSHAKEREADER_TD44E168F119A99B96D89F899EA2874605A0DC9A7_H
#define DRAFTHYBI00HANDSHAKEREADER_TD44E168F119A99B96D89F899EA2874605A0DC9A7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocket4Net.Protocol.DraftHybi00HandshakeReader
struct  DraftHybi00HandshakeReader_tD44E168F119A99B96D89F899EA2874605A0DC9A7  : public HandshakeReader_t247979AEC98CA1018DA56534ECCD1EBA23CBFC0C
{
public:
	// System.Int32 WebSocket4Net.Protocol.DraftHybi00HandshakeReader::m_ReceivedChallengeLength
	int32_t ___m_ReceivedChallengeLength_6;
	// System.Int32 WebSocket4Net.Protocol.DraftHybi00HandshakeReader::m_ExpectedChallengeLength
	int32_t ___m_ExpectedChallengeLength_7;
	// WebSocket4Net.WebSocketCommandInfo WebSocket4Net.Protocol.DraftHybi00HandshakeReader::m_HandshakeCommand
	WebSocketCommandInfo_t78D3A11DD086E0229D1AD938B61A9A9FC5CB133D * ___m_HandshakeCommand_8;
	// System.Byte[] WebSocket4Net.Protocol.DraftHybi00HandshakeReader::m_Challenges
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___m_Challenges_9;

public:
	inline static int32_t get_offset_of_m_ReceivedChallengeLength_6() { return static_cast<int32_t>(offsetof(DraftHybi00HandshakeReader_tD44E168F119A99B96D89F899EA2874605A0DC9A7, ___m_ReceivedChallengeLength_6)); }
	inline int32_t get_m_ReceivedChallengeLength_6() const { return ___m_ReceivedChallengeLength_6; }
	inline int32_t* get_address_of_m_ReceivedChallengeLength_6() { return &___m_ReceivedChallengeLength_6; }
	inline void set_m_ReceivedChallengeLength_6(int32_t value)
	{
		___m_ReceivedChallengeLength_6 = value;
	}

	inline static int32_t get_offset_of_m_ExpectedChallengeLength_7() { return static_cast<int32_t>(offsetof(DraftHybi00HandshakeReader_tD44E168F119A99B96D89F899EA2874605A0DC9A7, ___m_ExpectedChallengeLength_7)); }
	inline int32_t get_m_ExpectedChallengeLength_7() const { return ___m_ExpectedChallengeLength_7; }
	inline int32_t* get_address_of_m_ExpectedChallengeLength_7() { return &___m_ExpectedChallengeLength_7; }
	inline void set_m_ExpectedChallengeLength_7(int32_t value)
	{
		___m_ExpectedChallengeLength_7 = value;
	}

	inline static int32_t get_offset_of_m_HandshakeCommand_8() { return static_cast<int32_t>(offsetof(DraftHybi00HandshakeReader_tD44E168F119A99B96D89F899EA2874605A0DC9A7, ___m_HandshakeCommand_8)); }
	inline WebSocketCommandInfo_t78D3A11DD086E0229D1AD938B61A9A9FC5CB133D * get_m_HandshakeCommand_8() const { return ___m_HandshakeCommand_8; }
	inline WebSocketCommandInfo_t78D3A11DD086E0229D1AD938B61A9A9FC5CB133D ** get_address_of_m_HandshakeCommand_8() { return &___m_HandshakeCommand_8; }
	inline void set_m_HandshakeCommand_8(WebSocketCommandInfo_t78D3A11DD086E0229D1AD938B61A9A9FC5CB133D * value)
	{
		___m_HandshakeCommand_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_HandshakeCommand_8), value);
	}

	inline static int32_t get_offset_of_m_Challenges_9() { return static_cast<int32_t>(offsetof(DraftHybi00HandshakeReader_tD44E168F119A99B96D89F899EA2874605A0DC9A7, ___m_Challenges_9)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_m_Challenges_9() const { return ___m_Challenges_9; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_m_Challenges_9() { return &___m_Challenges_9; }
	inline void set_m_Challenges_9(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___m_Challenges_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Challenges_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAFTHYBI00HANDSHAKEREADER_TD44E168F119A99B96D89F899EA2874605A0DC9A7_H
#ifndef DRAFTHYBI10HANDSHAKEREADER_T5CBE41D552446741DD587177EAC5EE76C90B504A_H
#define DRAFTHYBI10HANDSHAKEREADER_T5CBE41D552446741DD587177EAC5EE76C90B504A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocket4Net.Protocol.DraftHybi10HandshakeReader
struct  DraftHybi10HandshakeReader_t5CBE41D552446741DD587177EAC5EE76C90B504A  : public HandshakeReader_t247979AEC98CA1018DA56534ECCD1EBA23CBFC0C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAFTHYBI10HANDSHAKEREADER_T5CBE41D552446741DD587177EAC5EE76C90B504A_H
#ifndef WEBSOCKETSTATE_TDC245B01F1B2B105368BD47561F2EB76420ECEB0_H
#define WEBSOCKETSTATE_TDC245B01F1B2B105368BD47561F2EB76420ECEB0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocket4Net.WebSocketState
struct  WebSocketState_tDC245B01F1B2B105368BD47561F2EB76420ECEB0 
{
public:
	// System.Int32 WebSocket4Net.WebSocketState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WebSocketState_tDC245B01F1B2B105368BD47561F2EB76420ECEB0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBSOCKETSTATE_TDC245B01F1B2B105368BD47561F2EB76420ECEB0_H
#ifndef WEBSOCKETVERSION_TBEBD9FE6DD3A0FA1A709542182931F60349A8C11_H
#define WEBSOCKETVERSION_TBEBD9FE6DD3A0FA1A709542182931F60349A8C11_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocket4Net.WebSocketVersion
struct  WebSocketVersion_tBEBD9FE6DD3A0FA1A709542182931F60349A8C11 
{
public:
	// System.Int32 WebSocket4Net.WebSocketVersion::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WebSocketVersion_tBEBD9FE6DD3A0FA1A709542182931F60349A8C11, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBSOCKETVERSION_TBEBD9FE6DD3A0FA1A709542182931F60349A8C11_H
#ifndef GSTLSCLIENT_TF62502461009667A6418D66DE53E678EDB4739E4_H
#define GSTLSCLIENT_TF62502461009667A6418D66DE53E678EDB4739E4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.RT.GSTlsClient
struct  GSTlsClient_tF62502461009667A6418D66DE53E678EDB4739E4  : public DefaultTlsClient_t3AEEC6138E8000BF0EB194E242413CB61B9EFF0E
{
public:
	// System.String GameSparks.RT.GSTlsClient::hostName
	String_t* ___hostName_8;

public:
	inline static int32_t get_offset_of_hostName_8() { return static_cast<int32_t>(offsetof(GSTlsClient_tF62502461009667A6418D66DE53E678EDB4739E4, ___hostName_8)); }
	inline String_t* get_hostName_8() const { return ___hostName_8; }
	inline String_t** get_address_of_hostName_8() { return &___hostName_8; }
	inline void set_hostName_8(String_t* value)
	{
		___hostName_8 = value;
		Il2CppCodeGenWriteBarrier((&___hostName_8), value);
	}
};

struct GSTlsClient_tF62502461009667A6418D66DE53E678EDB4739E4_StaticFields
{
public:
	// System.Action`1<System.String> GameSparks.RT.GSTlsClient::logger
	Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * ___logger_9;

public:
	inline static int32_t get_offset_of_logger_9() { return static_cast<int32_t>(offsetof(GSTlsClient_tF62502461009667A6418D66DE53E678EDB4739E4_StaticFields, ___logger_9)); }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * get_logger_9() const { return ___logger_9; }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 ** get_address_of_logger_9() { return &___logger_9; }
	inline void set_logger_9(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * value)
	{
		___logger_9 = value;
		Il2CppCodeGenWriteBarrier((&___logger_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GSTLSCLIENT_TF62502461009667A6418D66DE53E678EDB4739E4_H
#ifndef TCPCLIENTSESSION_T9CF522EBB9A6C11BF178D2A41F2B8F54075DB9AA_H
#define TCPCLIENTSESSION_T9CF522EBB9A6C11BF178D2A41F2B8F54075DB9AA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SuperSocket.ClientEngine.TcpClientSession
struct  TcpClientSession_t9CF522EBB9A6C11BF178D2A41F2B8F54075DB9AA  : public ClientSession_t9A79D55CDA93B439BD063F2DD8D73084EB748A98
{
public:
	// System.Boolean SuperSocket.ClientEngine.TcpClientSession::m_InConnecting
	bool ___m_InConnecting_13;
	// SuperSocket.ClientEngine.IBatchQueue`1<System.ArraySegment`1<System.Byte>> SuperSocket.ClientEngine.TcpClientSession::m_SendingQueue
	RuntimeObject* ___m_SendingQueue_14;
	// SuperSocket.ClientEngine.PosList`1<System.ArraySegment`1<System.Byte>> SuperSocket.ClientEngine.TcpClientSession::m_SendingItems
	PosList_1_tF22B6140877DA647665E0F81051342E9906F4098 * ___m_SendingItems_15;
	// System.Int32 SuperSocket.ClientEngine.TcpClientSession::m_IsSending
	int32_t ___m_IsSending_16;
	// System.String SuperSocket.ClientEngine.TcpClientSession::<HostName>k__BackingField
	String_t* ___U3CHostNameU3Ek__BackingField_17;

public:
	inline static int32_t get_offset_of_m_InConnecting_13() { return static_cast<int32_t>(offsetof(TcpClientSession_t9CF522EBB9A6C11BF178D2A41F2B8F54075DB9AA, ___m_InConnecting_13)); }
	inline bool get_m_InConnecting_13() const { return ___m_InConnecting_13; }
	inline bool* get_address_of_m_InConnecting_13() { return &___m_InConnecting_13; }
	inline void set_m_InConnecting_13(bool value)
	{
		___m_InConnecting_13 = value;
	}

	inline static int32_t get_offset_of_m_SendingQueue_14() { return static_cast<int32_t>(offsetof(TcpClientSession_t9CF522EBB9A6C11BF178D2A41F2B8F54075DB9AA, ___m_SendingQueue_14)); }
	inline RuntimeObject* get_m_SendingQueue_14() const { return ___m_SendingQueue_14; }
	inline RuntimeObject** get_address_of_m_SendingQueue_14() { return &___m_SendingQueue_14; }
	inline void set_m_SendingQueue_14(RuntimeObject* value)
	{
		___m_SendingQueue_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_SendingQueue_14), value);
	}

	inline static int32_t get_offset_of_m_SendingItems_15() { return static_cast<int32_t>(offsetof(TcpClientSession_t9CF522EBB9A6C11BF178D2A41F2B8F54075DB9AA, ___m_SendingItems_15)); }
	inline PosList_1_tF22B6140877DA647665E0F81051342E9906F4098 * get_m_SendingItems_15() const { return ___m_SendingItems_15; }
	inline PosList_1_tF22B6140877DA647665E0F81051342E9906F4098 ** get_address_of_m_SendingItems_15() { return &___m_SendingItems_15; }
	inline void set_m_SendingItems_15(PosList_1_tF22B6140877DA647665E0F81051342E9906F4098 * value)
	{
		___m_SendingItems_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_SendingItems_15), value);
	}

	inline static int32_t get_offset_of_m_IsSending_16() { return static_cast<int32_t>(offsetof(TcpClientSession_t9CF522EBB9A6C11BF178D2A41F2B8F54075DB9AA, ___m_IsSending_16)); }
	inline int32_t get_m_IsSending_16() const { return ___m_IsSending_16; }
	inline int32_t* get_address_of_m_IsSending_16() { return &___m_IsSending_16; }
	inline void set_m_IsSending_16(int32_t value)
	{
		___m_IsSending_16 = value;
	}

	inline static int32_t get_offset_of_U3CHostNameU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(TcpClientSession_t9CF522EBB9A6C11BF178D2A41F2B8F54075DB9AA, ___U3CHostNameU3Ek__BackingField_17)); }
	inline String_t* get_U3CHostNameU3Ek__BackingField_17() const { return ___U3CHostNameU3Ek__BackingField_17; }
	inline String_t** get_address_of_U3CHostNameU3Ek__BackingField_17() { return &___U3CHostNameU3Ek__BackingField_17; }
	inline void set_U3CHostNameU3Ek__BackingField_17(String_t* value)
	{
		___U3CHostNameU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CHostNameU3Ek__BackingField_17), value);
	}
};

struct TcpClientSession_t9CF522EBB9A6C11BF178D2A41F2B8F54075DB9AA_StaticFields
{
public:
	// System.Func`2<System.ArraySegment`1<System.Byte>,System.Boolean> SuperSocket.ClientEngine.TcpClientSession::CSU24<>9__CachedAnonymousMethodDelegate1
	Func_2_tAE16D74C9F08627D0E0D5F623E5AA350022A44B7 * ___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_18;

public:
	inline static int32_t get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_18() { return static_cast<int32_t>(offsetof(TcpClientSession_t9CF522EBB9A6C11BF178D2A41F2B8F54075DB9AA_StaticFields, ___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_18)); }
	inline Func_2_tAE16D74C9F08627D0E0D5F623E5AA350022A44B7 * get_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_18() const { return ___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_18; }
	inline Func_2_tAE16D74C9F08627D0E0D5F623E5AA350022A44B7 ** get_address_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_18() { return &___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_18; }
	inline void set_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_18(Func_2_tAE16D74C9F08627D0E0D5F623E5AA350022A44B7 * value)
	{
		___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_18 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TCPCLIENTSESSION_T9CF522EBB9A6C11BF178D2A41F2B8F54075DB9AA_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef PROTOCOLPROCESSORBASE_T5E71A2A673E9C3C47913247FCB5B825123C3C256_H
#define PROTOCOLPROCESSORBASE_T5E71A2A673E9C3C47913247FCB5B825123C3C256_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocket4Net.Protocol.ProtocolProcessorBase
struct  ProtocolProcessorBase_t5E71A2A673E9C3C47913247FCB5B825123C3C256  : public RuntimeObject
{
public:
	// WebSocket4Net.Protocol.ICloseStatusCode WebSocket4Net.Protocol.ProtocolProcessorBase::<CloseStatusCode>k__BackingField
	RuntimeObject* ___U3CCloseStatusCodeU3Ek__BackingField_1;
	// WebSocket4Net.WebSocketVersion WebSocket4Net.Protocol.ProtocolProcessorBase::<Version>k__BackingField
	int32_t ___U3CVersionU3Ek__BackingField_2;
	// System.String WebSocket4Net.Protocol.ProtocolProcessorBase::<VersionTag>k__BackingField
	String_t* ___U3CVersionTagU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CCloseStatusCodeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ProtocolProcessorBase_t5E71A2A673E9C3C47913247FCB5B825123C3C256, ___U3CCloseStatusCodeU3Ek__BackingField_1)); }
	inline RuntimeObject* get_U3CCloseStatusCodeU3Ek__BackingField_1() const { return ___U3CCloseStatusCodeU3Ek__BackingField_1; }
	inline RuntimeObject** get_address_of_U3CCloseStatusCodeU3Ek__BackingField_1() { return &___U3CCloseStatusCodeU3Ek__BackingField_1; }
	inline void set_U3CCloseStatusCodeU3Ek__BackingField_1(RuntimeObject* value)
	{
		___U3CCloseStatusCodeU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCloseStatusCodeU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CVersionU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ProtocolProcessorBase_t5E71A2A673E9C3C47913247FCB5B825123C3C256, ___U3CVersionU3Ek__BackingField_2)); }
	inline int32_t get_U3CVersionU3Ek__BackingField_2() const { return ___U3CVersionU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CVersionU3Ek__BackingField_2() { return &___U3CVersionU3Ek__BackingField_2; }
	inline void set_U3CVersionU3Ek__BackingField_2(int32_t value)
	{
		___U3CVersionU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CVersionTagU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ProtocolProcessorBase_t5E71A2A673E9C3C47913247FCB5B825123C3C256, ___U3CVersionTagU3Ek__BackingField_3)); }
	inline String_t* get_U3CVersionTagU3Ek__BackingField_3() const { return ___U3CVersionTagU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CVersionTagU3Ek__BackingField_3() { return &___U3CVersionTagU3Ek__BackingField_3; }
	inline void set_U3CVersionTagU3Ek__BackingField_3(String_t* value)
	{
		___U3CVersionTagU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CVersionTagU3Ek__BackingField_3), value);
	}
};

struct ProtocolProcessorBase_t5E71A2A673E9C3C47913247FCB5B825123C3C256_StaticFields
{
public:
	// System.String[] WebSocket4Net.Protocol.ProtocolProcessorBase::ExptectedResponseVerbLines
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___ExptectedResponseVerbLines_0;

public:
	inline static int32_t get_offset_of_ExptectedResponseVerbLines_0() { return static_cast<int32_t>(offsetof(ProtocolProcessorBase_t5E71A2A673E9C3C47913247FCB5B825123C3C256_StaticFields, ___ExptectedResponseVerbLines_0)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_ExptectedResponseVerbLines_0() const { return ___ExptectedResponseVerbLines_0; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_ExptectedResponseVerbLines_0() { return &___ExptectedResponseVerbLines_0; }
	inline void set_ExptectedResponseVerbLines_0(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___ExptectedResponseVerbLines_0 = value;
		Il2CppCodeGenWriteBarrier((&___ExptectedResponseVerbLines_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROTOCOLPROCESSORBASE_T5E71A2A673E9C3C47913247FCB5B825123C3C256_H
#ifndef U3CU3EC__DISPLAYCLASS3_T06944B821589CB77DDCFEBEE1C926639B4B0F962_H
#define U3CU3EC__DISPLAYCLASS3_T06944B821589CB77DDCFEBEE1C926639B4B0F962_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocket4Net.Protocol.ProtocolProcessorFactory_<>c__DisplayClass3
struct  U3CU3Ec__DisplayClass3_t06944B821589CB77DDCFEBEE1C926639B4B0F962  : public RuntimeObject
{
public:
	// WebSocket4Net.WebSocketVersion WebSocket4Net.Protocol.ProtocolProcessorFactory_<>c__DisplayClass3::version
	int32_t ___version_0;

public:
	inline static int32_t get_offset_of_version_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_t06944B821589CB77DDCFEBEE1C926639B4B0F962, ___version_0)); }
	inline int32_t get_version_0() const { return ___version_0; }
	inline int32_t* get_address_of_version_0() { return &___version_0; }
	inline void set_version_0(int32_t value)
	{
		___version_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS3_T06944B821589CB77DDCFEBEE1C926639B4B0F962_H
#ifndef WEBSOCKET_T231538678AB4B76293D12A78A9D798992897B3BD_H
#define WEBSOCKET_T231538678AB4B76293D12A78A9D798992897B3BD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocket4Net.WebSocket
struct  WebSocket_t231538678AB4B76293D12A78A9D798992897B3BD  : public RuntimeObject
{
public:
	// System.Int32 WebSocket4Net.WebSocket::m_StateCode
	int32_t ___m_StateCode_0;
	// System.Collections.Generic.Dictionary`2<System.String,SuperSocket.ClientEngine.Protocol.ICommand`2<WebSocket4Net.WebSocket,WebSocket4Net.WebSocketCommandInfo>> WebSocket4Net.WebSocket::m_CommandDict
	Dictionary_2_t9ABC8A2F3204DE9FD1B6186FBF6DB71754F8F806 * ___m_CommandDict_1;
	// System.Threading.Timer WebSocket4Net.WebSocket::m_WebSocketTimer
	Timer_t67FAB8E41573B4FA09CA56AE30725AF4297C2553 * ___m_WebSocketTimer_3;
	// System.String WebSocket4Net.WebSocket::m_LastPingRequest
	String_t* ___m_LastPingRequest_4;
	// System.EventHandler WebSocket4Net.WebSocket::m_Opened
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * ___m_Opened_5;
	// System.EventHandler`1<WebSocket4Net.MessageReceivedEventArgs> WebSocket4Net.WebSocket::m_MessageReceived
	EventHandler_1_tAB38B678A61DB7B67F5F5D79AEE8C2D32B768B51 * ___m_MessageReceived_6;
	// System.EventHandler`1<WebSocket4Net.DataReceivedEventArgs> WebSocket4Net.WebSocket::m_DataReceived
	EventHandler_1_t9C43B6F8703DA25284C5FD4CAF4423F4F967B65B * ___m_DataReceived_7;
	// System.EventHandler WebSocket4Net.WebSocket::m_Closed
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * ___m_Closed_8;
	// System.EventHandler`1<SuperSocket.ClientEngine.ErrorEventArgs> WebSocket4Net.WebSocket::m_Error
	EventHandler_1_t191D441D6952099231D3D3122D875173BEA68907 * ___m_Error_9;
	// SuperSocket.ClientEngine.TcpClientSession WebSocket4Net.WebSocket::<Client>k__BackingField
	TcpClientSession_t9CF522EBB9A6C11BF178D2A41F2B8F54075DB9AA * ___U3CClientU3Ek__BackingField_10;
	// WebSocket4Net.WebSocketVersion WebSocket4Net.WebSocket::<Version>k__BackingField
	int32_t ___U3CVersionU3Ek__BackingField_11;
	// System.DateTime WebSocket4Net.WebSocket::<LastActiveTime>k__BackingField
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___U3CLastActiveTimeU3Ek__BackingField_12;
	// System.Boolean WebSocket4Net.WebSocket::<EnableAutoSendPing>k__BackingField
	bool ___U3CEnableAutoSendPingU3Ek__BackingField_13;
	// System.Int32 WebSocket4Net.WebSocket::<AutoSendPingInterval>k__BackingField
	int32_t ___U3CAutoSendPingIntervalU3Ek__BackingField_14;
	// WebSocket4Net.Protocol.IProtocolProcessor WebSocket4Net.WebSocket::<ProtocolProcessor>k__BackingField
	RuntimeObject* ___U3CProtocolProcessorU3Ek__BackingField_15;
	// System.Uri WebSocket4Net.WebSocket::<TargetUri>k__BackingField
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___U3CTargetUriU3Ek__BackingField_16;
	// System.String WebSocket4Net.WebSocket::<SubProtocol>k__BackingField
	String_t* ___U3CSubProtocolU3Ek__BackingField_17;
	// System.Collections.Generic.IDictionary`2<System.String,System.Object> WebSocket4Net.WebSocket::<Items>k__BackingField
	RuntimeObject* ___U3CItemsU3Ek__BackingField_18;
	// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>> WebSocket4Net.WebSocket::<Cookies>k__BackingField
	List_1_t03E69AA5D95F83EE79F8D6698A0C4E6340602A19 * ___U3CCookiesU3Ek__BackingField_19;
	// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>> WebSocket4Net.WebSocket::<CustomHeaderItems>k__BackingField
	List_1_t03E69AA5D95F83EE79F8D6698A0C4E6340602A19 * ___U3CCustomHeaderItemsU3Ek__BackingField_20;
	// System.Boolean WebSocket4Net.WebSocket::<Handshaked>k__BackingField
	bool ___U3CHandshakedU3Ek__BackingField_21;
	// SuperSocket.ClientEngine.IProxyConnector WebSocket4Net.WebSocket::<Proxy>k__BackingField
	RuntimeObject* ___U3CProxyU3Ek__BackingField_22;
	// SuperSocket.ClientEngine.Protocol.IClientCommandReader`1<WebSocket4Net.WebSocketCommandInfo> WebSocket4Net.WebSocket::<CommandReader>k__BackingField
	RuntimeObject* ___U3CCommandReaderU3Ek__BackingField_23;
	// System.Boolean WebSocket4Net.WebSocket::<NotSpecifiedVersion>k__BackingField
	bool ___U3CNotSpecifiedVersionU3Ek__BackingField_24;
	// System.String WebSocket4Net.WebSocket::<LastPongResponse>k__BackingField
	String_t* ___U3CLastPongResponseU3Ek__BackingField_25;
	// System.String WebSocket4Net.WebSocket::<HandshakeHost>k__BackingField
	String_t* ___U3CHandshakeHostU3Ek__BackingField_26;
	// System.String WebSocket4Net.WebSocket::<Origin>k__BackingField
	String_t* ___U3COriginU3Ek__BackingField_27;
	// System.Boolean WebSocket4Net.WebSocket::<NoDelay>k__BackingField
	bool ___U3CNoDelayU3Ek__BackingField_28;

public:
	inline static int32_t get_offset_of_m_StateCode_0() { return static_cast<int32_t>(offsetof(WebSocket_t231538678AB4B76293D12A78A9D798992897B3BD, ___m_StateCode_0)); }
	inline int32_t get_m_StateCode_0() const { return ___m_StateCode_0; }
	inline int32_t* get_address_of_m_StateCode_0() { return &___m_StateCode_0; }
	inline void set_m_StateCode_0(int32_t value)
	{
		___m_StateCode_0 = value;
	}

	inline static int32_t get_offset_of_m_CommandDict_1() { return static_cast<int32_t>(offsetof(WebSocket_t231538678AB4B76293D12A78A9D798992897B3BD, ___m_CommandDict_1)); }
	inline Dictionary_2_t9ABC8A2F3204DE9FD1B6186FBF6DB71754F8F806 * get_m_CommandDict_1() const { return ___m_CommandDict_1; }
	inline Dictionary_2_t9ABC8A2F3204DE9FD1B6186FBF6DB71754F8F806 ** get_address_of_m_CommandDict_1() { return &___m_CommandDict_1; }
	inline void set_m_CommandDict_1(Dictionary_2_t9ABC8A2F3204DE9FD1B6186FBF6DB71754F8F806 * value)
	{
		___m_CommandDict_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_CommandDict_1), value);
	}

	inline static int32_t get_offset_of_m_WebSocketTimer_3() { return static_cast<int32_t>(offsetof(WebSocket_t231538678AB4B76293D12A78A9D798992897B3BD, ___m_WebSocketTimer_3)); }
	inline Timer_t67FAB8E41573B4FA09CA56AE30725AF4297C2553 * get_m_WebSocketTimer_3() const { return ___m_WebSocketTimer_3; }
	inline Timer_t67FAB8E41573B4FA09CA56AE30725AF4297C2553 ** get_address_of_m_WebSocketTimer_3() { return &___m_WebSocketTimer_3; }
	inline void set_m_WebSocketTimer_3(Timer_t67FAB8E41573B4FA09CA56AE30725AF4297C2553 * value)
	{
		___m_WebSocketTimer_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_WebSocketTimer_3), value);
	}

	inline static int32_t get_offset_of_m_LastPingRequest_4() { return static_cast<int32_t>(offsetof(WebSocket_t231538678AB4B76293D12A78A9D798992897B3BD, ___m_LastPingRequest_4)); }
	inline String_t* get_m_LastPingRequest_4() const { return ___m_LastPingRequest_4; }
	inline String_t** get_address_of_m_LastPingRequest_4() { return &___m_LastPingRequest_4; }
	inline void set_m_LastPingRequest_4(String_t* value)
	{
		___m_LastPingRequest_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_LastPingRequest_4), value);
	}

	inline static int32_t get_offset_of_m_Opened_5() { return static_cast<int32_t>(offsetof(WebSocket_t231538678AB4B76293D12A78A9D798992897B3BD, ___m_Opened_5)); }
	inline EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * get_m_Opened_5() const { return ___m_Opened_5; }
	inline EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C ** get_address_of_m_Opened_5() { return &___m_Opened_5; }
	inline void set_m_Opened_5(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * value)
	{
		___m_Opened_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Opened_5), value);
	}

	inline static int32_t get_offset_of_m_MessageReceived_6() { return static_cast<int32_t>(offsetof(WebSocket_t231538678AB4B76293D12A78A9D798992897B3BD, ___m_MessageReceived_6)); }
	inline EventHandler_1_tAB38B678A61DB7B67F5F5D79AEE8C2D32B768B51 * get_m_MessageReceived_6() const { return ___m_MessageReceived_6; }
	inline EventHandler_1_tAB38B678A61DB7B67F5F5D79AEE8C2D32B768B51 ** get_address_of_m_MessageReceived_6() { return &___m_MessageReceived_6; }
	inline void set_m_MessageReceived_6(EventHandler_1_tAB38B678A61DB7B67F5F5D79AEE8C2D32B768B51 * value)
	{
		___m_MessageReceived_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_MessageReceived_6), value);
	}

	inline static int32_t get_offset_of_m_DataReceived_7() { return static_cast<int32_t>(offsetof(WebSocket_t231538678AB4B76293D12A78A9D798992897B3BD, ___m_DataReceived_7)); }
	inline EventHandler_1_t9C43B6F8703DA25284C5FD4CAF4423F4F967B65B * get_m_DataReceived_7() const { return ___m_DataReceived_7; }
	inline EventHandler_1_t9C43B6F8703DA25284C5FD4CAF4423F4F967B65B ** get_address_of_m_DataReceived_7() { return &___m_DataReceived_7; }
	inline void set_m_DataReceived_7(EventHandler_1_t9C43B6F8703DA25284C5FD4CAF4423F4F967B65B * value)
	{
		___m_DataReceived_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_DataReceived_7), value);
	}

	inline static int32_t get_offset_of_m_Closed_8() { return static_cast<int32_t>(offsetof(WebSocket_t231538678AB4B76293D12A78A9D798992897B3BD, ___m_Closed_8)); }
	inline EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * get_m_Closed_8() const { return ___m_Closed_8; }
	inline EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C ** get_address_of_m_Closed_8() { return &___m_Closed_8; }
	inline void set_m_Closed_8(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * value)
	{
		___m_Closed_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_Closed_8), value);
	}

	inline static int32_t get_offset_of_m_Error_9() { return static_cast<int32_t>(offsetof(WebSocket_t231538678AB4B76293D12A78A9D798992897B3BD, ___m_Error_9)); }
	inline EventHandler_1_t191D441D6952099231D3D3122D875173BEA68907 * get_m_Error_9() const { return ___m_Error_9; }
	inline EventHandler_1_t191D441D6952099231D3D3122D875173BEA68907 ** get_address_of_m_Error_9() { return &___m_Error_9; }
	inline void set_m_Error_9(EventHandler_1_t191D441D6952099231D3D3122D875173BEA68907 * value)
	{
		___m_Error_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Error_9), value);
	}

	inline static int32_t get_offset_of_U3CClientU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(WebSocket_t231538678AB4B76293D12A78A9D798992897B3BD, ___U3CClientU3Ek__BackingField_10)); }
	inline TcpClientSession_t9CF522EBB9A6C11BF178D2A41F2B8F54075DB9AA * get_U3CClientU3Ek__BackingField_10() const { return ___U3CClientU3Ek__BackingField_10; }
	inline TcpClientSession_t9CF522EBB9A6C11BF178D2A41F2B8F54075DB9AA ** get_address_of_U3CClientU3Ek__BackingField_10() { return &___U3CClientU3Ek__BackingField_10; }
	inline void set_U3CClientU3Ek__BackingField_10(TcpClientSession_t9CF522EBB9A6C11BF178D2A41F2B8F54075DB9AA * value)
	{
		___U3CClientU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CClientU3Ek__BackingField_10), value);
	}

	inline static int32_t get_offset_of_U3CVersionU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(WebSocket_t231538678AB4B76293D12A78A9D798992897B3BD, ___U3CVersionU3Ek__BackingField_11)); }
	inline int32_t get_U3CVersionU3Ek__BackingField_11() const { return ___U3CVersionU3Ek__BackingField_11; }
	inline int32_t* get_address_of_U3CVersionU3Ek__BackingField_11() { return &___U3CVersionU3Ek__BackingField_11; }
	inline void set_U3CVersionU3Ek__BackingField_11(int32_t value)
	{
		___U3CVersionU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CLastActiveTimeU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(WebSocket_t231538678AB4B76293D12A78A9D798992897B3BD, ___U3CLastActiveTimeU3Ek__BackingField_12)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_U3CLastActiveTimeU3Ek__BackingField_12() const { return ___U3CLastActiveTimeU3Ek__BackingField_12; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_U3CLastActiveTimeU3Ek__BackingField_12() { return &___U3CLastActiveTimeU3Ek__BackingField_12; }
	inline void set_U3CLastActiveTimeU3Ek__BackingField_12(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___U3CLastActiveTimeU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CEnableAutoSendPingU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(WebSocket_t231538678AB4B76293D12A78A9D798992897B3BD, ___U3CEnableAutoSendPingU3Ek__BackingField_13)); }
	inline bool get_U3CEnableAutoSendPingU3Ek__BackingField_13() const { return ___U3CEnableAutoSendPingU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CEnableAutoSendPingU3Ek__BackingField_13() { return &___U3CEnableAutoSendPingU3Ek__BackingField_13; }
	inline void set_U3CEnableAutoSendPingU3Ek__BackingField_13(bool value)
	{
		___U3CEnableAutoSendPingU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3CAutoSendPingIntervalU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(WebSocket_t231538678AB4B76293D12A78A9D798992897B3BD, ___U3CAutoSendPingIntervalU3Ek__BackingField_14)); }
	inline int32_t get_U3CAutoSendPingIntervalU3Ek__BackingField_14() const { return ___U3CAutoSendPingIntervalU3Ek__BackingField_14; }
	inline int32_t* get_address_of_U3CAutoSendPingIntervalU3Ek__BackingField_14() { return &___U3CAutoSendPingIntervalU3Ek__BackingField_14; }
	inline void set_U3CAutoSendPingIntervalU3Ek__BackingField_14(int32_t value)
	{
		___U3CAutoSendPingIntervalU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CProtocolProcessorU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(WebSocket_t231538678AB4B76293D12A78A9D798992897B3BD, ___U3CProtocolProcessorU3Ek__BackingField_15)); }
	inline RuntimeObject* get_U3CProtocolProcessorU3Ek__BackingField_15() const { return ___U3CProtocolProcessorU3Ek__BackingField_15; }
	inline RuntimeObject** get_address_of_U3CProtocolProcessorU3Ek__BackingField_15() { return &___U3CProtocolProcessorU3Ek__BackingField_15; }
	inline void set_U3CProtocolProcessorU3Ek__BackingField_15(RuntimeObject* value)
	{
		___U3CProtocolProcessorU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CProtocolProcessorU3Ek__BackingField_15), value);
	}

	inline static int32_t get_offset_of_U3CTargetUriU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(WebSocket_t231538678AB4B76293D12A78A9D798992897B3BD, ___U3CTargetUriU3Ek__BackingField_16)); }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * get_U3CTargetUriU3Ek__BackingField_16() const { return ___U3CTargetUriU3Ek__BackingField_16; }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E ** get_address_of_U3CTargetUriU3Ek__BackingField_16() { return &___U3CTargetUriU3Ek__BackingField_16; }
	inline void set_U3CTargetUriU3Ek__BackingField_16(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * value)
	{
		___U3CTargetUriU3Ek__BackingField_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTargetUriU3Ek__BackingField_16), value);
	}

	inline static int32_t get_offset_of_U3CSubProtocolU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(WebSocket_t231538678AB4B76293D12A78A9D798992897B3BD, ___U3CSubProtocolU3Ek__BackingField_17)); }
	inline String_t* get_U3CSubProtocolU3Ek__BackingField_17() const { return ___U3CSubProtocolU3Ek__BackingField_17; }
	inline String_t** get_address_of_U3CSubProtocolU3Ek__BackingField_17() { return &___U3CSubProtocolU3Ek__BackingField_17; }
	inline void set_U3CSubProtocolU3Ek__BackingField_17(String_t* value)
	{
		___U3CSubProtocolU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSubProtocolU3Ek__BackingField_17), value);
	}

	inline static int32_t get_offset_of_U3CItemsU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(WebSocket_t231538678AB4B76293D12A78A9D798992897B3BD, ___U3CItemsU3Ek__BackingField_18)); }
	inline RuntimeObject* get_U3CItemsU3Ek__BackingField_18() const { return ___U3CItemsU3Ek__BackingField_18; }
	inline RuntimeObject** get_address_of_U3CItemsU3Ek__BackingField_18() { return &___U3CItemsU3Ek__BackingField_18; }
	inline void set_U3CItemsU3Ek__BackingField_18(RuntimeObject* value)
	{
		___U3CItemsU3Ek__BackingField_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3CItemsU3Ek__BackingField_18), value);
	}

	inline static int32_t get_offset_of_U3CCookiesU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(WebSocket_t231538678AB4B76293D12A78A9D798992897B3BD, ___U3CCookiesU3Ek__BackingField_19)); }
	inline List_1_t03E69AA5D95F83EE79F8D6698A0C4E6340602A19 * get_U3CCookiesU3Ek__BackingField_19() const { return ___U3CCookiesU3Ek__BackingField_19; }
	inline List_1_t03E69AA5D95F83EE79F8D6698A0C4E6340602A19 ** get_address_of_U3CCookiesU3Ek__BackingField_19() { return &___U3CCookiesU3Ek__BackingField_19; }
	inline void set_U3CCookiesU3Ek__BackingField_19(List_1_t03E69AA5D95F83EE79F8D6698A0C4E6340602A19 * value)
	{
		___U3CCookiesU3Ek__BackingField_19 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCookiesU3Ek__BackingField_19), value);
	}

	inline static int32_t get_offset_of_U3CCustomHeaderItemsU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(WebSocket_t231538678AB4B76293D12A78A9D798992897B3BD, ___U3CCustomHeaderItemsU3Ek__BackingField_20)); }
	inline List_1_t03E69AA5D95F83EE79F8D6698A0C4E6340602A19 * get_U3CCustomHeaderItemsU3Ek__BackingField_20() const { return ___U3CCustomHeaderItemsU3Ek__BackingField_20; }
	inline List_1_t03E69AA5D95F83EE79F8D6698A0C4E6340602A19 ** get_address_of_U3CCustomHeaderItemsU3Ek__BackingField_20() { return &___U3CCustomHeaderItemsU3Ek__BackingField_20; }
	inline void set_U3CCustomHeaderItemsU3Ek__BackingField_20(List_1_t03E69AA5D95F83EE79F8D6698A0C4E6340602A19 * value)
	{
		___U3CCustomHeaderItemsU3Ek__BackingField_20 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCustomHeaderItemsU3Ek__BackingField_20), value);
	}

	inline static int32_t get_offset_of_U3CHandshakedU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(WebSocket_t231538678AB4B76293D12A78A9D798992897B3BD, ___U3CHandshakedU3Ek__BackingField_21)); }
	inline bool get_U3CHandshakedU3Ek__BackingField_21() const { return ___U3CHandshakedU3Ek__BackingField_21; }
	inline bool* get_address_of_U3CHandshakedU3Ek__BackingField_21() { return &___U3CHandshakedU3Ek__BackingField_21; }
	inline void set_U3CHandshakedU3Ek__BackingField_21(bool value)
	{
		___U3CHandshakedU3Ek__BackingField_21 = value;
	}

	inline static int32_t get_offset_of_U3CProxyU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(WebSocket_t231538678AB4B76293D12A78A9D798992897B3BD, ___U3CProxyU3Ek__BackingField_22)); }
	inline RuntimeObject* get_U3CProxyU3Ek__BackingField_22() const { return ___U3CProxyU3Ek__BackingField_22; }
	inline RuntimeObject** get_address_of_U3CProxyU3Ek__BackingField_22() { return &___U3CProxyU3Ek__BackingField_22; }
	inline void set_U3CProxyU3Ek__BackingField_22(RuntimeObject* value)
	{
		___U3CProxyU3Ek__BackingField_22 = value;
		Il2CppCodeGenWriteBarrier((&___U3CProxyU3Ek__BackingField_22), value);
	}

	inline static int32_t get_offset_of_U3CCommandReaderU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(WebSocket_t231538678AB4B76293D12A78A9D798992897B3BD, ___U3CCommandReaderU3Ek__BackingField_23)); }
	inline RuntimeObject* get_U3CCommandReaderU3Ek__BackingField_23() const { return ___U3CCommandReaderU3Ek__BackingField_23; }
	inline RuntimeObject** get_address_of_U3CCommandReaderU3Ek__BackingField_23() { return &___U3CCommandReaderU3Ek__BackingField_23; }
	inline void set_U3CCommandReaderU3Ek__BackingField_23(RuntimeObject* value)
	{
		___U3CCommandReaderU3Ek__BackingField_23 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCommandReaderU3Ek__BackingField_23), value);
	}

	inline static int32_t get_offset_of_U3CNotSpecifiedVersionU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(WebSocket_t231538678AB4B76293D12A78A9D798992897B3BD, ___U3CNotSpecifiedVersionU3Ek__BackingField_24)); }
	inline bool get_U3CNotSpecifiedVersionU3Ek__BackingField_24() const { return ___U3CNotSpecifiedVersionU3Ek__BackingField_24; }
	inline bool* get_address_of_U3CNotSpecifiedVersionU3Ek__BackingField_24() { return &___U3CNotSpecifiedVersionU3Ek__BackingField_24; }
	inline void set_U3CNotSpecifiedVersionU3Ek__BackingField_24(bool value)
	{
		___U3CNotSpecifiedVersionU3Ek__BackingField_24 = value;
	}

	inline static int32_t get_offset_of_U3CLastPongResponseU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(WebSocket_t231538678AB4B76293D12A78A9D798992897B3BD, ___U3CLastPongResponseU3Ek__BackingField_25)); }
	inline String_t* get_U3CLastPongResponseU3Ek__BackingField_25() const { return ___U3CLastPongResponseU3Ek__BackingField_25; }
	inline String_t** get_address_of_U3CLastPongResponseU3Ek__BackingField_25() { return &___U3CLastPongResponseU3Ek__BackingField_25; }
	inline void set_U3CLastPongResponseU3Ek__BackingField_25(String_t* value)
	{
		___U3CLastPongResponseU3Ek__BackingField_25 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLastPongResponseU3Ek__BackingField_25), value);
	}

	inline static int32_t get_offset_of_U3CHandshakeHostU3Ek__BackingField_26() { return static_cast<int32_t>(offsetof(WebSocket_t231538678AB4B76293D12A78A9D798992897B3BD, ___U3CHandshakeHostU3Ek__BackingField_26)); }
	inline String_t* get_U3CHandshakeHostU3Ek__BackingField_26() const { return ___U3CHandshakeHostU3Ek__BackingField_26; }
	inline String_t** get_address_of_U3CHandshakeHostU3Ek__BackingField_26() { return &___U3CHandshakeHostU3Ek__BackingField_26; }
	inline void set_U3CHandshakeHostU3Ek__BackingField_26(String_t* value)
	{
		___U3CHandshakeHostU3Ek__BackingField_26 = value;
		Il2CppCodeGenWriteBarrier((&___U3CHandshakeHostU3Ek__BackingField_26), value);
	}

	inline static int32_t get_offset_of_U3COriginU3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(WebSocket_t231538678AB4B76293D12A78A9D798992897B3BD, ___U3COriginU3Ek__BackingField_27)); }
	inline String_t* get_U3COriginU3Ek__BackingField_27() const { return ___U3COriginU3Ek__BackingField_27; }
	inline String_t** get_address_of_U3COriginU3Ek__BackingField_27() { return &___U3COriginU3Ek__BackingField_27; }
	inline void set_U3COriginU3Ek__BackingField_27(String_t* value)
	{
		___U3COriginU3Ek__BackingField_27 = value;
		Il2CppCodeGenWriteBarrier((&___U3COriginU3Ek__BackingField_27), value);
	}

	inline static int32_t get_offset_of_U3CNoDelayU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(WebSocket_t231538678AB4B76293D12A78A9D798992897B3BD, ___U3CNoDelayU3Ek__BackingField_28)); }
	inline bool get_U3CNoDelayU3Ek__BackingField_28() const { return ___U3CNoDelayU3Ek__BackingField_28; }
	inline bool* get_address_of_U3CNoDelayU3Ek__BackingField_28() { return &___U3CNoDelayU3Ek__BackingField_28; }
	inline void set_U3CNoDelayU3Ek__BackingField_28(bool value)
	{
		___U3CNoDelayU3Ek__BackingField_28 = value;
	}
};

struct WebSocket_t231538678AB4B76293D12A78A9D798992897B3BD_StaticFields
{
public:
	// WebSocket4Net.Protocol.ProtocolProcessorFactory WebSocket4Net.WebSocket::m_ProtocolProcessorFactory
	ProtocolProcessorFactory_t55DA417FFED0B9138D1330C2AE71C3B1501755FA * ___m_ProtocolProcessorFactory_2;

public:
	inline static int32_t get_offset_of_m_ProtocolProcessorFactory_2() { return static_cast<int32_t>(offsetof(WebSocket_t231538678AB4B76293D12A78A9D798992897B3BD_StaticFields, ___m_ProtocolProcessorFactory_2)); }
	inline ProtocolProcessorFactory_t55DA417FFED0B9138D1330C2AE71C3B1501755FA * get_m_ProtocolProcessorFactory_2() const { return ___m_ProtocolProcessorFactory_2; }
	inline ProtocolProcessorFactory_t55DA417FFED0B9138D1330C2AE71C3B1501755FA ** get_address_of_m_ProtocolProcessorFactory_2() { return &___m_ProtocolProcessorFactory_2; }
	inline void set_m_ProtocolProcessorFactory_2(ProtocolProcessorFactory_t55DA417FFED0B9138D1330C2AE71C3B1501755FA * value)
	{
		___m_ProtocolProcessorFactory_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_ProtocolProcessorFactory_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBSOCKET_T231538678AB4B76293D12A78A9D798992897B3BD_H
#ifndef READDELEGATE_T264EEB5F4B254FA08B9839A5684FDC237A566958_H
#define READDELEGATE_T264EEB5F4B254FA08B9839A5684FDC237A566958_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.RT.DuplexTlsStream_ReadDelegate
struct  ReadDelegate_t264EEB5F4B254FA08B9839A5684FDC237A566958  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READDELEGATE_T264EEB5F4B254FA08B9839A5684FDC237A566958_H
#ifndef WRITEDELEGATE_TADF42C77ACC966499334BF60ADFF16CC179DC42D_H
#define WRITEDELEGATE_TADF42C77ACC966499334BF60ADFF16CC179DC42D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.RT.DuplexTlsStream_WriteDelegate
struct  WriteDelegate_tADF42C77ACC966499334BF60ADFF16CC179DC42D  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WRITEDELEGATE_TADF42C77ACC966499334BF60ADFF16CC179DC42D_H
#ifndef ASYNCTCPSESSION_T11D7E0905C954014437A15AED87999BA76A7AA7B_H
#define ASYNCTCPSESSION_T11D7E0905C954014437A15AED87999BA76A7AA7B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SuperSocket.ClientEngine.AsyncTcpSession
struct  AsyncTcpSession_t11D7E0905C954014437A15AED87999BA76A7AA7B  : public TcpClientSession_t9CF522EBB9A6C11BF178D2A41F2B8F54075DB9AA
{
public:
	// System.Net.Sockets.SocketAsyncEventArgs SuperSocket.ClientEngine.AsyncTcpSession::m_SocketEventArgs
	SocketAsyncEventArgs_t5E05ABA36B4740D22D5E746DC4E7763AA448CAA7 * ___m_SocketEventArgs_19;
	// System.Net.Sockets.SocketAsyncEventArgs SuperSocket.ClientEngine.AsyncTcpSession::m_SocketEventArgsSend
	SocketAsyncEventArgs_t5E05ABA36B4740D22D5E746DC4E7763AA448CAA7 * ___m_SocketEventArgsSend_20;

public:
	inline static int32_t get_offset_of_m_SocketEventArgs_19() { return static_cast<int32_t>(offsetof(AsyncTcpSession_t11D7E0905C954014437A15AED87999BA76A7AA7B, ___m_SocketEventArgs_19)); }
	inline SocketAsyncEventArgs_t5E05ABA36B4740D22D5E746DC4E7763AA448CAA7 * get_m_SocketEventArgs_19() const { return ___m_SocketEventArgs_19; }
	inline SocketAsyncEventArgs_t5E05ABA36B4740D22D5E746DC4E7763AA448CAA7 ** get_address_of_m_SocketEventArgs_19() { return &___m_SocketEventArgs_19; }
	inline void set_m_SocketEventArgs_19(SocketAsyncEventArgs_t5E05ABA36B4740D22D5E746DC4E7763AA448CAA7 * value)
	{
		___m_SocketEventArgs_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_SocketEventArgs_19), value);
	}

	inline static int32_t get_offset_of_m_SocketEventArgsSend_20() { return static_cast<int32_t>(offsetof(AsyncTcpSession_t11D7E0905C954014437A15AED87999BA76A7AA7B, ___m_SocketEventArgsSend_20)); }
	inline SocketAsyncEventArgs_t5E05ABA36B4740D22D5E746DC4E7763AA448CAA7 * get_m_SocketEventArgsSend_20() const { return ___m_SocketEventArgsSend_20; }
	inline SocketAsyncEventArgs_t5E05ABA36B4740D22D5E746DC4E7763AA448CAA7 ** get_address_of_m_SocketEventArgsSend_20() { return &___m_SocketEventArgsSend_20; }
	inline void set_m_SocketEventArgsSend_20(SocketAsyncEventArgs_t5E05ABA36B4740D22D5E746DC4E7763AA448CAA7 * value)
	{
		___m_SocketEventArgsSend_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_SocketEventArgsSend_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCTCPSESSION_T11D7E0905C954014437A15AED87999BA76A7AA7B_H
#ifndef CONNECTEDCALLBACK_T27F6846DBFF78119A3FB59CB3C577CFB61BD2D75_H
#define CONNECTEDCALLBACK_T27F6846DBFF78119A3FB59CB3C577CFB61BD2D75_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SuperSocket.ClientEngine.ConnectedCallback
struct  ConnectedCallback_t27F6846DBFF78119A3FB59CB3C577CFB61BD2D75  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONNECTEDCALLBACK_T27F6846DBFF78119A3FB59CB3C577CFB61BD2D75_H
#ifndef SSLSTREAMTCPSESSION_TF0FC88875D6DEA00C115C0A7EAC4E113E5D68FD1_H
#define SSLSTREAMTCPSESSION_TF0FC88875D6DEA00C115C0A7EAC4E113E5D68FD1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SuperSocket.ClientEngine.SslStreamTcpSession
struct  SslStreamTcpSession_tF0FC88875D6DEA00C115C0A7EAC4E113E5D68FD1  : public TcpClientSession_t9CF522EBB9A6C11BF178D2A41F2B8F54075DB9AA
{
public:
	// System.IO.Stream SuperSocket.ClientEngine.SslStreamTcpSession::m_SslStream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___m_SslStream_19;

public:
	inline static int32_t get_offset_of_m_SslStream_19() { return static_cast<int32_t>(offsetof(SslStreamTcpSession_tF0FC88875D6DEA00C115C0A7EAC4E113E5D68FD1, ___m_SslStream_19)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_m_SslStream_19() const { return ___m_SslStream_19; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_m_SslStream_19() { return &___m_SslStream_19; }
	inline void set_m_SslStream_19(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___m_SslStream_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_SslStream_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SSLSTREAMTCPSESSION_TF0FC88875D6DEA00C115C0A7EAC4E113E5D68FD1_H
#ifndef DRAFTHYBI00PROCESSOR_TF2362DEF89137A77A4417E961DD95995E4B1EF60_H
#define DRAFTHYBI00PROCESSOR_TF2362DEF89137A77A4417E961DD95995E4B1EF60_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocket4Net.Protocol.DraftHybi00Processor
struct  DraftHybi00Processor_tF2362DEF89137A77A4417E961DD95995E4B1EF60  : public ProtocolProcessorBase_t5E71A2A673E9C3C47913247FCB5B825123C3C256
{
public:
	// System.Byte[] WebSocket4Net.Protocol.DraftHybi00Processor::m_ExpectedChallenge
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___m_ExpectedChallenge_8;

public:
	inline static int32_t get_offset_of_m_ExpectedChallenge_8() { return static_cast<int32_t>(offsetof(DraftHybi00Processor_tF2362DEF89137A77A4417E961DD95995E4B1EF60, ___m_ExpectedChallenge_8)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_m_ExpectedChallenge_8() const { return ___m_ExpectedChallenge_8; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_m_ExpectedChallenge_8() { return &___m_ExpectedChallenge_8; }
	inline void set_m_ExpectedChallenge_8(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___m_ExpectedChallenge_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_ExpectedChallenge_8), value);
	}
};

struct DraftHybi00Processor_tF2362DEF89137A77A4417E961DD95995E4B1EF60_StaticFields
{
public:
	// System.Collections.Generic.List`1<System.Char> WebSocket4Net.Protocol.DraftHybi00Processor::m_CharLib
	List_1_t028AAE01C4834286B7892F4498364F964CD8B316 * ___m_CharLib_4;
	// System.Collections.Generic.List`1<System.Char> WebSocket4Net.Protocol.DraftHybi00Processor::m_DigLib
	List_1_t028AAE01C4834286B7892F4498364F964CD8B316 * ___m_DigLib_5;
	// System.Random WebSocket4Net.Protocol.DraftHybi00Processor::m_Random
	Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F * ___m_Random_6;
	// System.Byte[] WebSocket4Net.Protocol.DraftHybi00Processor::CloseHandshake
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___CloseHandshake_7;
	// System.Func`2<System.Char,System.Boolean> WebSocket4Net.Protocol.DraftHybi00Processor::CSU24<>9__CachedAnonymousMethodDelegate2
	Func_2_t987FE48B7C07E5DDABA6CD66D8FFFF1627A0CEE5 * ___CSU24U3CU3E9__CachedAnonymousMethodDelegate2_9;
	// System.Func`2<System.Char,System.Boolean> WebSocket4Net.Protocol.DraftHybi00Processor::CSU24<>9__CachedAnonymousMethodDelegate3
	Func_2_t987FE48B7C07E5DDABA6CD66D8FFFF1627A0CEE5 * ___CSU24U3CU3E9__CachedAnonymousMethodDelegate3_10;

public:
	inline static int32_t get_offset_of_m_CharLib_4() { return static_cast<int32_t>(offsetof(DraftHybi00Processor_tF2362DEF89137A77A4417E961DD95995E4B1EF60_StaticFields, ___m_CharLib_4)); }
	inline List_1_t028AAE01C4834286B7892F4498364F964CD8B316 * get_m_CharLib_4() const { return ___m_CharLib_4; }
	inline List_1_t028AAE01C4834286B7892F4498364F964CD8B316 ** get_address_of_m_CharLib_4() { return &___m_CharLib_4; }
	inline void set_m_CharLib_4(List_1_t028AAE01C4834286B7892F4498364F964CD8B316 * value)
	{
		___m_CharLib_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_CharLib_4), value);
	}

	inline static int32_t get_offset_of_m_DigLib_5() { return static_cast<int32_t>(offsetof(DraftHybi00Processor_tF2362DEF89137A77A4417E961DD95995E4B1EF60_StaticFields, ___m_DigLib_5)); }
	inline List_1_t028AAE01C4834286B7892F4498364F964CD8B316 * get_m_DigLib_5() const { return ___m_DigLib_5; }
	inline List_1_t028AAE01C4834286B7892F4498364F964CD8B316 ** get_address_of_m_DigLib_5() { return &___m_DigLib_5; }
	inline void set_m_DigLib_5(List_1_t028AAE01C4834286B7892F4498364F964CD8B316 * value)
	{
		___m_DigLib_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_DigLib_5), value);
	}

	inline static int32_t get_offset_of_m_Random_6() { return static_cast<int32_t>(offsetof(DraftHybi00Processor_tF2362DEF89137A77A4417E961DD95995E4B1EF60_StaticFields, ___m_Random_6)); }
	inline Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F * get_m_Random_6() const { return ___m_Random_6; }
	inline Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F ** get_address_of_m_Random_6() { return &___m_Random_6; }
	inline void set_m_Random_6(Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F * value)
	{
		___m_Random_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Random_6), value);
	}

	inline static int32_t get_offset_of_CloseHandshake_7() { return static_cast<int32_t>(offsetof(DraftHybi00Processor_tF2362DEF89137A77A4417E961DD95995E4B1EF60_StaticFields, ___CloseHandshake_7)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_CloseHandshake_7() const { return ___CloseHandshake_7; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_CloseHandshake_7() { return &___CloseHandshake_7; }
	inline void set_CloseHandshake_7(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___CloseHandshake_7 = value;
		Il2CppCodeGenWriteBarrier((&___CloseHandshake_7), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate2_9() { return static_cast<int32_t>(offsetof(DraftHybi00Processor_tF2362DEF89137A77A4417E961DD95995E4B1EF60_StaticFields, ___CSU24U3CU3E9__CachedAnonymousMethodDelegate2_9)); }
	inline Func_2_t987FE48B7C07E5DDABA6CD66D8FFFF1627A0CEE5 * get_CSU24U3CU3E9__CachedAnonymousMethodDelegate2_9() const { return ___CSU24U3CU3E9__CachedAnonymousMethodDelegate2_9; }
	inline Func_2_t987FE48B7C07E5DDABA6CD66D8FFFF1627A0CEE5 ** get_address_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate2_9() { return &___CSU24U3CU3E9__CachedAnonymousMethodDelegate2_9; }
	inline void set_CSU24U3CU3E9__CachedAnonymousMethodDelegate2_9(Func_2_t987FE48B7C07E5DDABA6CD66D8FFFF1627A0CEE5 * value)
	{
		___CSU24U3CU3E9__CachedAnonymousMethodDelegate2_9 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E9__CachedAnonymousMethodDelegate2_9), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate3_10() { return static_cast<int32_t>(offsetof(DraftHybi00Processor_tF2362DEF89137A77A4417E961DD95995E4B1EF60_StaticFields, ___CSU24U3CU3E9__CachedAnonymousMethodDelegate3_10)); }
	inline Func_2_t987FE48B7C07E5DDABA6CD66D8FFFF1627A0CEE5 * get_CSU24U3CU3E9__CachedAnonymousMethodDelegate3_10() const { return ___CSU24U3CU3E9__CachedAnonymousMethodDelegate3_10; }
	inline Func_2_t987FE48B7C07E5DDABA6CD66D8FFFF1627A0CEE5 ** get_address_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate3_10() { return &___CSU24U3CU3E9__CachedAnonymousMethodDelegate3_10; }
	inline void set_CSU24U3CU3E9__CachedAnonymousMethodDelegate3_10(Func_2_t987FE48B7C07E5DDABA6CD66D8FFFF1627A0CEE5 * value)
	{
		___CSU24U3CU3E9__CachedAnonymousMethodDelegate3_10 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E9__CachedAnonymousMethodDelegate3_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAFTHYBI00PROCESSOR_TF2362DEF89137A77A4417E961DD95995E4B1EF60_H
#ifndef DRAFTHYBI10PROCESSOR_T0585829A81EC22397B319D636B932895846A80E7_H
#define DRAFTHYBI10PROCESSOR_T0585829A81EC22397B319D636B932895846A80E7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocket4Net.Protocol.DraftHybi10Processor
struct  DraftHybi10Processor_t0585829A81EC22397B319D636B932895846A80E7  : public ProtocolProcessorBase_t5E71A2A673E9C3C47913247FCB5B825123C3C256
{
public:
	// System.String WebSocket4Net.Protocol.DraftHybi10Processor::m_ExpectedAcceptKey
	String_t* ___m_ExpectedAcceptKey_4;

public:
	inline static int32_t get_offset_of_m_ExpectedAcceptKey_4() { return static_cast<int32_t>(offsetof(DraftHybi10Processor_t0585829A81EC22397B319D636B932895846A80E7, ___m_ExpectedAcceptKey_4)); }
	inline String_t* get_m_ExpectedAcceptKey_4() const { return ___m_ExpectedAcceptKey_4; }
	inline String_t** get_address_of_m_ExpectedAcceptKey_4() { return &___m_ExpectedAcceptKey_4; }
	inline void set_m_ExpectedAcceptKey_4(String_t* value)
	{
		___m_ExpectedAcceptKey_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_ExpectedAcceptKey_4), value);
	}
};

struct DraftHybi10Processor_t0585829A81EC22397B319D636B932895846A80E7_StaticFields
{
public:
	// System.Random WebSocket4Net.Protocol.DraftHybi10Processor::m_Random
	Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F * ___m_Random_5;

public:
	inline static int32_t get_offset_of_m_Random_5() { return static_cast<int32_t>(offsetof(DraftHybi10Processor_t0585829A81EC22397B319D636B932895846A80E7_StaticFields, ___m_Random_5)); }
	inline Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F * get_m_Random_5() const { return ___m_Random_5; }
	inline Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F ** get_address_of_m_Random_5() { return &___m_Random_5; }
	inline void set_m_Random_5(Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F * value)
	{
		___m_Random_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Random_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAFTHYBI10PROCESSOR_T0585829A81EC22397B319D636B932895846A80E7_H
#ifndef RFC6455PROCESSOR_TA1E5EE67AD6BD43B76791ED55D422F1A2922C858_H
#define RFC6455PROCESSOR_TA1E5EE67AD6BD43B76791ED55D422F1A2922C858_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocket4Net.Protocol.Rfc6455Processor
struct  Rfc6455Processor_tA1E5EE67AD6BD43B76791ED55D422F1A2922C858  : public DraftHybi10Processor_t0585829A81EC22397B319D636B932895846A80E7
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RFC6455PROCESSOR_TA1E5EE67AD6BD43B76791ED55D422F1A2922C858_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5500 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5501 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5501[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5502 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5502[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5503 = { sizeof (ConnectedCallback_t27F6846DBFF78119A3FB59CB3C577CFB61BD2D75), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5504 = { sizeof (ConnectAsyncExtension_tE679A81CC66C20E643FD289C2C64B55148FBB653), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5505 = { sizeof (ConnectToken_t86B48BA76FA4374492AA67EF9DD1EB7E162AE45A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5505[2] = 
{
	ConnectToken_t86B48BA76FA4374492AA67EF9DD1EB7E162AE45A::get_offset_of_U3CStateU3Ek__BackingField_0(),
	ConnectToken_t86B48BA76FA4374492AA67EF9DD1EB7E162AE45A::get_offset_of_U3CCallbackU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5506 = { sizeof (DnsConnectState_tE731E1C5E763BCFBDD6AB16F53EBBB8CEF7F1348), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5506[7] = 
{
	DnsConnectState_tE731E1C5E763BCFBDD6AB16F53EBBB8CEF7F1348::get_offset_of_U3CAddressesU3Ek__BackingField_0(),
	DnsConnectState_tE731E1C5E763BCFBDD6AB16F53EBBB8CEF7F1348::get_offset_of_U3CNextAddressIndexU3Ek__BackingField_1(),
	DnsConnectState_tE731E1C5E763BCFBDD6AB16F53EBBB8CEF7F1348::get_offset_of_U3CPortU3Ek__BackingField_2(),
	DnsConnectState_tE731E1C5E763BCFBDD6AB16F53EBBB8CEF7F1348::get_offset_of_U3CSocket4U3Ek__BackingField_3(),
	DnsConnectState_tE731E1C5E763BCFBDD6AB16F53EBBB8CEF7F1348::get_offset_of_U3CSocket6U3Ek__BackingField_4(),
	DnsConnectState_tE731E1C5E763BCFBDD6AB16F53EBBB8CEF7F1348::get_offset_of_U3CStateU3Ek__BackingField_5(),
	DnsConnectState_tE731E1C5E763BCFBDD6AB16F53EBBB8CEF7F1348::get_offset_of_U3CCallbackU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5507 = { sizeof (DnsEndPoint2_tF8C7125EC532B5E62DBFDF73AFA6954FB54CE436), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5507[2] = 
{
	DnsEndPoint2_tF8C7125EC532B5E62DBFDF73AFA6954FB54CE436::get_offset_of_U3CHostU3Ek__BackingField_0(),
	DnsEndPoint2_tF8C7125EC532B5E62DBFDF73AFA6954FB54CE436::get_offset_of_U3CPortU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5508 = { sizeof (Extensions_tDC0399F167916AA28EC96A1E44D4A468D856259D), -1, sizeof(Extensions_tDC0399F167916AA28EC96A1E44D4A468D856259D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5508[1] = 
{
	Extensions_tDC0399F167916AA28EC96A1E44D4A468D856259D_StaticFields::get_offset_of_m_Random_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5509 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5510 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5511 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5511[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5512 = { sizeof (ProxyEventArgs_t13ACBA30C49309CAD705A1AF7AC4229845542B49), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5512[3] = 
{
	ProxyEventArgs_t13ACBA30C49309CAD705A1AF7AC4229845542B49::get_offset_of_U3CConnectedU3Ek__BackingField_1(),
	ProxyEventArgs_t13ACBA30C49309CAD705A1AF7AC4229845542B49::get_offset_of_U3CSocketU3Ek__BackingField_2(),
	ProxyEventArgs_t13ACBA30C49309CAD705A1AF7AC4229845542B49::get_offset_of_U3CExceptionU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5513 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5513[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5514 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5515 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5516 = { sizeof (ClientSession_t9A79D55CDA93B439BD063F2DD8D73084EB748A98), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5516[13] = 
{
	ClientSession_t9A79D55CDA93B439BD063F2DD8D73084EB748A98::get_offset_of_m_Closed_0(),
	ClientSession_t9A79D55CDA93B439BD063F2DD8D73084EB748A98::get_offset_of_m_Error_1(),
	ClientSession_t9A79D55CDA93B439BD063F2DD8D73084EB748A98::get_offset_of_m_Connected_2(),
	ClientSession_t9A79D55CDA93B439BD063F2DD8D73084EB748A98::get_offset_of_m_DataReceived_3(),
	ClientSession_t9A79D55CDA93B439BD063F2DD8D73084EB748A98::get_offset_of_m_DataArgs_4(),
	ClientSession_t9A79D55CDA93B439BD063F2DD8D73084EB748A98::get_offset_of_U3CClientU3Ek__BackingField_5(),
	ClientSession_t9A79D55CDA93B439BD063F2DD8D73084EB748A98::get_offset_of_U3CRemoteEndPointU3Ek__BackingField_6(),
	ClientSession_t9A79D55CDA93B439BD063F2DD8D73084EB748A98::get_offset_of_U3CIsConnectedU3Ek__BackingField_7(),
	ClientSession_t9A79D55CDA93B439BD063F2DD8D73084EB748A98::get_offset_of_U3CNoDeplayU3Ek__BackingField_8(),
	ClientSession_t9A79D55CDA93B439BD063F2DD8D73084EB748A98::get_offset_of_U3CSendingQueueSizeU3Ek__BackingField_9(),
	ClientSession_t9A79D55CDA93B439BD063F2DD8D73084EB748A98::get_offset_of_U3CReceiveBufferSizeU3Ek__BackingField_10(),
	ClientSession_t9A79D55CDA93B439BD063F2DD8D73084EB748A98::get_offset_of_U3CProxyU3Ek__BackingField_11(),
	ClientSession_t9A79D55CDA93B439BD063F2DD8D73084EB748A98::get_offset_of_U3CBufferU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5517 = { sizeof (TcpClientSession_t9CF522EBB9A6C11BF178D2A41F2B8F54075DB9AA), -1, sizeof(TcpClientSession_t9CF522EBB9A6C11BF178D2A41F2B8F54075DB9AA_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5517[6] = 
{
	TcpClientSession_t9CF522EBB9A6C11BF178D2A41F2B8F54075DB9AA::get_offset_of_m_InConnecting_13(),
	TcpClientSession_t9CF522EBB9A6C11BF178D2A41F2B8F54075DB9AA::get_offset_of_m_SendingQueue_14(),
	TcpClientSession_t9CF522EBB9A6C11BF178D2A41F2B8F54075DB9AA::get_offset_of_m_SendingItems_15(),
	TcpClientSession_t9CF522EBB9A6C11BF178D2A41F2B8F54075DB9AA::get_offset_of_m_IsSending_16(),
	TcpClientSession_t9CF522EBB9A6C11BF178D2A41F2B8F54075DB9AA::get_offset_of_U3CHostNameU3Ek__BackingField_17(),
	TcpClientSession_t9CF522EBB9A6C11BF178D2A41F2B8F54075DB9AA_StaticFields::get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5518 = { sizeof (AsyncTcpSession_t11D7E0905C954014437A15AED87999BA76A7AA7B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5518[2] = 
{
	AsyncTcpSession_t11D7E0905C954014437A15AED87999BA76A7AA7B::get_offset_of_m_SocketEventArgs_19(),
	AsyncTcpSession_t11D7E0905C954014437A15AED87999BA76A7AA7B::get_offset_of_m_SocketEventArgsSend_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5519 = { sizeof (DataEventArgs_tAD557B50CCC2D8F34E8B607EF7B6A1C7FA6614A3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5519[3] = 
{
	DataEventArgs_tAD557B50CCC2D8F34E8B607EF7B6A1C7FA6614A3::get_offset_of_U3CDataU3Ek__BackingField_1(),
	DataEventArgs_tAD557B50CCC2D8F34E8B607EF7B6A1C7FA6614A3::get_offset_of_U3COffsetU3Ek__BackingField_2(),
	DataEventArgs_tAD557B50CCC2D8F34E8B607EF7B6A1C7FA6614A3::get_offset_of_U3CLengthU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5520 = { sizeof (ErrorEventArgs_t5AA425041F8070B3C1E447B51615536ECDB56D78), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5520[1] = 
{
	ErrorEventArgs_t5AA425041F8070B3C1E447B51615536ECDB56D78::get_offset_of_U3CExceptionU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5521 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5521[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5522 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5522[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5523 = { sizeof (ArraySegmentList_t3FB5043710933FF6704A1AFCE6295DE8CE401F18), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5524 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5525 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5526 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5527 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5528 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5529 = { sizeof (GameSparksTimer_t73F609039E6A360DB023437BF00AA039000F4797), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5529[2] = 
{
	GameSparksTimer_t73F609039E6A360DB023437BF00AA039000F4797::get_offset_of_m_timer_0(),
	GameSparksTimer_t73F609039E6A360DB023437BF00AA039000F4797::get_offset_of_m_callback_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5530 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5531 = { sizeof (GameSparksWebSocket_tE1594ED2F1C86FD1D299356B8C97C68674BF9534), -1, sizeof(GameSparksWebSocket_tE1594ED2F1C86FD1D299356B8C97C68674BF9534_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5531[7] = 
{
	GameSparksWebSocket_tE1594ED2F1C86FD1D299356B8C97C68674BF9534::get_offset_of_onMessage_0(),
	GameSparksWebSocket_tE1594ED2F1C86FD1D299356B8C97C68674BF9534::get_offset_of_onBinaryMessage_1(),
	GameSparksWebSocket_tE1594ED2F1C86FD1D299356B8C97C68674BF9534::get_offset_of_onClose_2(),
	GameSparksWebSocket_tE1594ED2F1C86FD1D299356B8C97C68674BF9534::get_offset_of_onOpen_3(),
	GameSparksWebSocket_tE1594ED2F1C86FD1D299356B8C97C68674BF9534::get_offset_of_onError_4(),
	GameSparksWebSocket_tE1594ED2F1C86FD1D299356B8C97C68674BF9534::get_offset_of_ws_5(),
	GameSparksWebSocket_tE1594ED2F1C86FD1D299356B8C97C68674BF9534_StaticFields::get_offset_of_U3CProxyU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5532 = { sizeof (GameSparksUtil_t157B0A24B339CAF4CB0F1E4C2D1284E26CF3E187), -1, sizeof(GameSparksUtil_t157B0A24B339CAF4CB0F1E4C2D1284E26CF3E187_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5532[1] = 
{
	GameSparksUtil_t157B0A24B339CAF4CB0F1E4C2D1284E26CF3E187_StaticFields::get_offset_of_LogMessageHandler_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5533 = { sizeof (GameSparksWebSocketState_t1581131C5935866AAF3021C070F016DC5ABDAAC1)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5533[6] = 
{
	GameSparksWebSocketState_t1581131C5935866AAF3021C070F016DC5ABDAAC1::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5534 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5535 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5536 = { sizeof (QueueReader_tB32CB4ADFC9AFD3BBB5770452A547A858EC59A86), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5536[2] = 
{
	QueueReader_tB32CB4ADFC9AFD3BBB5770452A547A858EC59A86::get_offset_of_sr_0(),
	QueueReader_tB32CB4ADFC9AFD3BBB5770452A547A858EC59A86::get_offset_of_fileName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5537 = { sizeof (QueueWriter_t0CB01E31FA2FACB2F0760EFA02851B397F4093C1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5537[1] = 
{
	QueueWriter_t0CB01E31FA2FACB2F0760EFA02851B397F4093C1::get_offset_of_sw_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5538 = { sizeof (WebSocketCommandBase_t2121F47322A7F3C4FC75E91FA1F46F3B39EE32D9), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5539 = { sizeof (BadRequest_t3278D3848E90CD4D3251BD1D2239D650E08E44F5), -1, sizeof(BadRequest_t3278D3848E90CD4D3251BD1D2239D650E08E44F5_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5539[1] = 
{
	BadRequest_t3278D3848E90CD4D3251BD1D2239D650E08E44F5_StaticFields::get_offset_of_m_ValueSeparator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5540 = { sizeof (Binary_tBF1D30556818C0850CBE7E48D973D1A4D0CC69E4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5541 = { sizeof (Close_t411DD4E4942770A52A577E19D51BDB3B823DD39A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5542 = { sizeof (Handshake_t2A500A4A75F9643C1E5264309BD51752A24F9BD8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5543 = { sizeof (Ping_tE9E70FDB453BF6C9CFC1CF2E063B73910E8C887C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5544 = { sizeof (Pong_t767A1B2AF987EA021BBA40BF5C84D2DF776DE56A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5545 = { sizeof (Text_t7814FFBFB79538E40C988D03428C81D2CC353F53), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5546 = { sizeof (DataReceivedEventArgs_t60F42187E42DA680BDD0246C783FE2D8FB9BEFE9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5546[1] = 
{
	DataReceivedEventArgs_t60F42187E42DA680BDD0246C783FE2D8FB9BEFE9::get_offset_of_U3CDataU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5547 = { sizeof (Extensions_tF44DE23091552C5D9D740D937587B78809D7026A), -1, sizeof(Extensions_tF44DE23091552C5D9D740D937587B78809D7026A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5547[2] = 
{
	Extensions_tF44DE23091552C5D9D740D937587B78809D7026A_StaticFields::get_offset_of_m_CrCf_0(),
	Extensions_tF44DE23091552C5D9D740D937587B78809D7026A_StaticFields::get_offset_of_m_SimpleTypes_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5548 = { sizeof (MessageReceivedEventArgs_t5A823DF5C37C8706FC40BD7B27ABD7FE50C8736B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5548[1] = 
{
	MessageReceivedEventArgs_t5A823DF5C37C8706FC40BD7B27ABD7FE50C8736B::get_offset_of_U3CMessageU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5549 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5550 = { sizeof (CloseStatusCodeHybi10_tB3EAB89831F235D52C9D77109C6E2E257EB5F87A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5550[11] = 
{
	CloseStatusCodeHybi10_tB3EAB89831F235D52C9D77109C6E2E257EB5F87A::get_offset_of_U3CNormalClosureU3Ek__BackingField_0(),
	CloseStatusCodeHybi10_tB3EAB89831F235D52C9D77109C6E2E257EB5F87A::get_offset_of_U3CGoingAwayU3Ek__BackingField_1(),
	CloseStatusCodeHybi10_tB3EAB89831F235D52C9D77109C6E2E257EB5F87A::get_offset_of_U3CProtocolErrorU3Ek__BackingField_2(),
	CloseStatusCodeHybi10_tB3EAB89831F235D52C9D77109C6E2E257EB5F87A::get_offset_of_U3CNotAcceptableDataU3Ek__BackingField_3(),
	CloseStatusCodeHybi10_tB3EAB89831F235D52C9D77109C6E2E257EB5F87A::get_offset_of_U3CTooLargeFrameU3Ek__BackingField_4(),
	CloseStatusCodeHybi10_tB3EAB89831F235D52C9D77109C6E2E257EB5F87A::get_offset_of_U3CInvalidUTF8U3Ek__BackingField_5(),
	CloseStatusCodeHybi10_tB3EAB89831F235D52C9D77109C6E2E257EB5F87A::get_offset_of_U3CViolatePolicyU3Ek__BackingField_6(),
	CloseStatusCodeHybi10_tB3EAB89831F235D52C9D77109C6E2E257EB5F87A::get_offset_of_U3CExtensionNotMatchU3Ek__BackingField_7(),
	CloseStatusCodeHybi10_tB3EAB89831F235D52C9D77109C6E2E257EB5F87A::get_offset_of_U3CUnexpectedConditionU3Ek__BackingField_8(),
	CloseStatusCodeHybi10_tB3EAB89831F235D52C9D77109C6E2E257EB5F87A::get_offset_of_U3CTLSHandshakeFailureU3Ek__BackingField_9(),
	CloseStatusCodeHybi10_tB3EAB89831F235D52C9D77109C6E2E257EB5F87A::get_offset_of_U3CNoStatusCodeU3Ek__BackingField_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5551 = { sizeof (CloseStatusCodeRfc6455_t4FFBA83B0178B701BA6C83A5F8D0DA855ED00E03), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5551[10] = 
{
	CloseStatusCodeRfc6455_t4FFBA83B0178B701BA6C83A5F8D0DA855ED00E03::get_offset_of_U3CNormalClosureU3Ek__BackingField_0(),
	CloseStatusCodeRfc6455_t4FFBA83B0178B701BA6C83A5F8D0DA855ED00E03::get_offset_of_U3CGoingAwayU3Ek__BackingField_1(),
	CloseStatusCodeRfc6455_t4FFBA83B0178B701BA6C83A5F8D0DA855ED00E03::get_offset_of_U3CProtocolErrorU3Ek__BackingField_2(),
	CloseStatusCodeRfc6455_t4FFBA83B0178B701BA6C83A5F8D0DA855ED00E03::get_offset_of_U3CNotAcceptableDataU3Ek__BackingField_3(),
	CloseStatusCodeRfc6455_t4FFBA83B0178B701BA6C83A5F8D0DA855ED00E03::get_offset_of_U3CTooLargeFrameU3Ek__BackingField_4(),
	CloseStatusCodeRfc6455_t4FFBA83B0178B701BA6C83A5F8D0DA855ED00E03::get_offset_of_U3CInvalidUTF8U3Ek__BackingField_5(),
	CloseStatusCodeRfc6455_t4FFBA83B0178B701BA6C83A5F8D0DA855ED00E03::get_offset_of_U3CViolatePolicyU3Ek__BackingField_6(),
	CloseStatusCodeRfc6455_t4FFBA83B0178B701BA6C83A5F8D0DA855ED00E03::get_offset_of_U3CExtensionNotMatchU3Ek__BackingField_7(),
	CloseStatusCodeRfc6455_t4FFBA83B0178B701BA6C83A5F8D0DA855ED00E03::get_offset_of_U3CUnexpectedConditionU3Ek__BackingField_8(),
	CloseStatusCodeRfc6455_t4FFBA83B0178B701BA6C83A5F8D0DA855ED00E03::get_offset_of_U3CNoStatusCodeU3Ek__BackingField_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5552 = { sizeof (ReaderBase_t57E7CDB15563532730AB07F0FF785C424397BAC6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5552[3] = 
{
	ReaderBase_t57E7CDB15563532730AB07F0FF785C424397BAC6::get_offset_of_m_BufferSegments_0(),
	ReaderBase_t57E7CDB15563532730AB07F0FF785C424397BAC6::get_offset_of_U3CWebSocketU3Ek__BackingField_1(),
	ReaderBase_t57E7CDB15563532730AB07F0FF785C424397BAC6::get_offset_of_U3CNextCommandReaderU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5553 = { sizeof (DraftHybi00DataReader_t1B8006EAAB3E4801F8FF6F5DCC4A1BE2631886F5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5553[3] = 
{
	DraftHybi00DataReader_t1B8006EAAB3E4801F8FF6F5DCC4A1BE2631886F5::get_offset_of_m_Type_3(),
	DraftHybi00DataReader_t1B8006EAAB3E4801F8FF6F5DCC4A1BE2631886F5::get_offset_of_m_TempLength_4(),
	DraftHybi00DataReader_t1B8006EAAB3E4801F8FF6F5DCC4A1BE2631886F5::get_offset_of_m_Length_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5554 = { sizeof (HandshakeReader_t247979AEC98CA1018DA56534ECCD1EBA23CBFC0C), -1, sizeof(HandshakeReader_t247979AEC98CA1018DA56534ECCD1EBA23CBFC0C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5554[3] = 
{
	HandshakeReader_t247979AEC98CA1018DA56534ECCD1EBA23CBFC0C_StaticFields::get_offset_of_BadRequestCode_3(),
	HandshakeReader_t247979AEC98CA1018DA56534ECCD1EBA23CBFC0C_StaticFields::get_offset_of_HeaderTerminator_4(),
	HandshakeReader_t247979AEC98CA1018DA56534ECCD1EBA23CBFC0C::get_offset_of_m_HeadSeachState_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5555 = { sizeof (DraftHybi00HandshakeReader_tD44E168F119A99B96D89F899EA2874605A0DC9A7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5555[4] = 
{
	DraftHybi00HandshakeReader_tD44E168F119A99B96D89F899EA2874605A0DC9A7::get_offset_of_m_ReceivedChallengeLength_6(),
	DraftHybi00HandshakeReader_tD44E168F119A99B96D89F899EA2874605A0DC9A7::get_offset_of_m_ExpectedChallengeLength_7(),
	DraftHybi00HandshakeReader_tD44E168F119A99B96D89F899EA2874605A0DC9A7::get_offset_of_m_HandshakeCommand_8(),
	DraftHybi00HandshakeReader_tD44E168F119A99B96D89F899EA2874605A0DC9A7::get_offset_of_m_Challenges_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5556 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5557 = { sizeof (ProtocolProcessorBase_t5E71A2A673E9C3C47913247FCB5B825123C3C256), -1, sizeof(ProtocolProcessorBase_t5E71A2A673E9C3C47913247FCB5B825123C3C256_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5557[4] = 
{
	ProtocolProcessorBase_t5E71A2A673E9C3C47913247FCB5B825123C3C256_StaticFields::get_offset_of_ExptectedResponseVerbLines_0(),
	ProtocolProcessorBase_t5E71A2A673E9C3C47913247FCB5B825123C3C256::get_offset_of_U3CCloseStatusCodeU3Ek__BackingField_1(),
	ProtocolProcessorBase_t5E71A2A673E9C3C47913247FCB5B825123C3C256::get_offset_of_U3CVersionU3Ek__BackingField_2(),
	ProtocolProcessorBase_t5E71A2A673E9C3C47913247FCB5B825123C3C256::get_offset_of_U3CVersionTagU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5558 = { sizeof (DraftHybi00Processor_tF2362DEF89137A77A4417E961DD95995E4B1EF60), -1, sizeof(DraftHybi00Processor_tF2362DEF89137A77A4417E961DD95995E4B1EF60_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5558[7] = 
{
	DraftHybi00Processor_tF2362DEF89137A77A4417E961DD95995E4B1EF60_StaticFields::get_offset_of_m_CharLib_4(),
	DraftHybi00Processor_tF2362DEF89137A77A4417E961DD95995E4B1EF60_StaticFields::get_offset_of_m_DigLib_5(),
	DraftHybi00Processor_tF2362DEF89137A77A4417E961DD95995E4B1EF60_StaticFields::get_offset_of_m_Random_6(),
	DraftHybi00Processor_tF2362DEF89137A77A4417E961DD95995E4B1EF60_StaticFields::get_offset_of_CloseHandshake_7(),
	DraftHybi00Processor_tF2362DEF89137A77A4417E961DD95995E4B1EF60::get_offset_of_m_ExpectedChallenge_8(),
	DraftHybi00Processor_tF2362DEF89137A77A4417E961DD95995E4B1EF60_StaticFields::get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate2_9(),
	DraftHybi00Processor_tF2362DEF89137A77A4417E961DD95995E4B1EF60_StaticFields::get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate3_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5559 = { sizeof (DraftHybi10DataReader_t25DCD4F60B884278B6C8B02C53F70CE6BE3BDE3F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5559[4] = 
{
	DraftHybi10DataReader_t25DCD4F60B884278B6C8B02C53F70CE6BE3BDE3F::get_offset_of_m_PreviousFrames_0(),
	DraftHybi10DataReader_t25DCD4F60B884278B6C8B02C53F70CE6BE3BDE3F::get_offset_of_m_Frame_1(),
	DraftHybi10DataReader_t25DCD4F60B884278B6C8B02C53F70CE6BE3BDE3F::get_offset_of_m_PartReader_2(),
	DraftHybi10DataReader_t25DCD4F60B884278B6C8B02C53F70CE6BE3BDE3F::get_offset_of_m_LastPartLength_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5560 = { sizeof (DraftHybi10HandshakeReader_t5CBE41D552446741DD587177EAC5EE76C90B504A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5561 = { sizeof (DraftHybi10Processor_t0585829A81EC22397B319D636B932895846A80E7), -1, sizeof(DraftHybi10Processor_t0585829A81EC22397B319D636B932895846A80E7_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5561[2] = 
{
	DraftHybi10Processor_t0585829A81EC22397B319D636B932895846A80E7::get_offset_of_m_ExpectedAcceptKey_4(),
	DraftHybi10Processor_t0585829A81EC22397B319D636B932895846A80E7_StaticFields::get_offset_of_m_Random_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5562 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5563 = { sizeof (DataFramePartReader_tDE67A43BE1F64AE2D208BBCFAF2AF25933B20221), -1, sizeof(DataFramePartReader_tDE67A43BE1F64AE2D208BBCFAF2AF25933B20221_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5563[4] = 
{
	DataFramePartReader_tDE67A43BE1F64AE2D208BBCFAF2AF25933B20221_StaticFields::get_offset_of_U3CFixPartReaderU3Ek__BackingField_0(),
	DataFramePartReader_tDE67A43BE1F64AE2D208BBCFAF2AF25933B20221_StaticFields::get_offset_of_U3CExtendedLenghtReaderU3Ek__BackingField_1(),
	DataFramePartReader_tDE67A43BE1F64AE2D208BBCFAF2AF25933B20221_StaticFields::get_offset_of_U3CMaskKeyReaderU3Ek__BackingField_2(),
	DataFramePartReader_tDE67A43BE1F64AE2D208BBCFAF2AF25933B20221_StaticFields::get_offset_of_U3CPayloadDataReaderU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5564 = { sizeof (ExtendedLenghtReader_t4CA69182EE1EF98A06A1D5EC74B8F2C5A95D7DF1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5565 = { sizeof (FixPartReader_tB1C89F0B19F35C09955DCC9BEFD9A748B65749E6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5566 = { sizeof (MaskKeyReader_t9D4908BF0FB59B52E3D88077AF5CD76080018A69), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5567 = { sizeof (PayloadDataReader_t7FD1E46C718C976D124956DE45BC4AEA45D49875), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5568 = { sizeof (ProtocolProcessorFactory_t55DA417FFED0B9138D1330C2AE71C3B1501755FA), -1, sizeof(ProtocolProcessorFactory_t55DA417FFED0B9138D1330C2AE71C3B1501755FA_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5568[3] = 
{
	ProtocolProcessorFactory_t55DA417FFED0B9138D1330C2AE71C3B1501755FA::get_offset_of_m_OrderedProcessors_0(),
	ProtocolProcessorFactory_t55DA417FFED0B9138D1330C2AE71C3B1501755FA_StaticFields::get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1(),
	ProtocolProcessorFactory_t55DA417FFED0B9138D1330C2AE71C3B1501755FA_StaticFields::get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate6_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5569 = { sizeof (U3CU3Ec__DisplayClass3_t06944B821589CB77DDCFEBEE1C926639B4B0F962), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5569[1] = 
{
	U3CU3Ec__DisplayClass3_t06944B821589CB77DDCFEBEE1C926639B4B0F962::get_offset_of_version_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5570 = { sizeof (Rfc6455Processor_tA1E5EE67AD6BD43B76791ED55D422F1A2922C858), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5571 = { sizeof (WebSocketDataFrame_tAF30418C83AF20230D871C344C668A3157898C96), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5571[5] = 
{
	WebSocketDataFrame_tAF30418C83AF20230D871C344C668A3157898C96::get_offset_of_m_InnerData_0(),
	WebSocketDataFrame_tAF30418C83AF20230D871C344C668A3157898C96::get_offset_of_m_ActualPayloadLength_1(),
	WebSocketDataFrame_tAF30418C83AF20230D871C344C668A3157898C96::get_offset_of_U3CMaskKeyU3Ek__BackingField_2(),
	WebSocketDataFrame_tAF30418C83AF20230D871C344C668A3157898C96::get_offset_of_U3CExtensionDataU3Ek__BackingField_3(),
	WebSocketDataFrame_tAF30418C83AF20230D871C344C668A3157898C96::get_offset_of_U3CApplicationDataU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5572 = { sizeof (WebSocket_t231538678AB4B76293D12A78A9D798992897B3BD), -1, sizeof(WebSocket_t231538678AB4B76293D12A78A9D798992897B3BD_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5572[29] = 
{
	WebSocket_t231538678AB4B76293D12A78A9D798992897B3BD::get_offset_of_m_StateCode_0(),
	WebSocket_t231538678AB4B76293D12A78A9D798992897B3BD::get_offset_of_m_CommandDict_1(),
	WebSocket_t231538678AB4B76293D12A78A9D798992897B3BD_StaticFields::get_offset_of_m_ProtocolProcessorFactory_2(),
	WebSocket_t231538678AB4B76293D12A78A9D798992897B3BD::get_offset_of_m_WebSocketTimer_3(),
	WebSocket_t231538678AB4B76293D12A78A9D798992897B3BD::get_offset_of_m_LastPingRequest_4(),
	WebSocket_t231538678AB4B76293D12A78A9D798992897B3BD::get_offset_of_m_Opened_5(),
	WebSocket_t231538678AB4B76293D12A78A9D798992897B3BD::get_offset_of_m_MessageReceived_6(),
	WebSocket_t231538678AB4B76293D12A78A9D798992897B3BD::get_offset_of_m_DataReceived_7(),
	WebSocket_t231538678AB4B76293D12A78A9D798992897B3BD::get_offset_of_m_Closed_8(),
	WebSocket_t231538678AB4B76293D12A78A9D798992897B3BD::get_offset_of_m_Error_9(),
	WebSocket_t231538678AB4B76293D12A78A9D798992897B3BD::get_offset_of_U3CClientU3Ek__BackingField_10(),
	WebSocket_t231538678AB4B76293D12A78A9D798992897B3BD::get_offset_of_U3CVersionU3Ek__BackingField_11(),
	WebSocket_t231538678AB4B76293D12A78A9D798992897B3BD::get_offset_of_U3CLastActiveTimeU3Ek__BackingField_12(),
	WebSocket_t231538678AB4B76293D12A78A9D798992897B3BD::get_offset_of_U3CEnableAutoSendPingU3Ek__BackingField_13(),
	WebSocket_t231538678AB4B76293D12A78A9D798992897B3BD::get_offset_of_U3CAutoSendPingIntervalU3Ek__BackingField_14(),
	WebSocket_t231538678AB4B76293D12A78A9D798992897B3BD::get_offset_of_U3CProtocolProcessorU3Ek__BackingField_15(),
	WebSocket_t231538678AB4B76293D12A78A9D798992897B3BD::get_offset_of_U3CTargetUriU3Ek__BackingField_16(),
	WebSocket_t231538678AB4B76293D12A78A9D798992897B3BD::get_offset_of_U3CSubProtocolU3Ek__BackingField_17(),
	WebSocket_t231538678AB4B76293D12A78A9D798992897B3BD::get_offset_of_U3CItemsU3Ek__BackingField_18(),
	WebSocket_t231538678AB4B76293D12A78A9D798992897B3BD::get_offset_of_U3CCookiesU3Ek__BackingField_19(),
	WebSocket_t231538678AB4B76293D12A78A9D798992897B3BD::get_offset_of_U3CCustomHeaderItemsU3Ek__BackingField_20(),
	WebSocket_t231538678AB4B76293D12A78A9D798992897B3BD::get_offset_of_U3CHandshakedU3Ek__BackingField_21(),
	WebSocket_t231538678AB4B76293D12A78A9D798992897B3BD::get_offset_of_U3CProxyU3Ek__BackingField_22(),
	WebSocket_t231538678AB4B76293D12A78A9D798992897B3BD::get_offset_of_U3CCommandReaderU3Ek__BackingField_23(),
	WebSocket_t231538678AB4B76293D12A78A9D798992897B3BD::get_offset_of_U3CNotSpecifiedVersionU3Ek__BackingField_24(),
	WebSocket_t231538678AB4B76293D12A78A9D798992897B3BD::get_offset_of_U3CLastPongResponseU3Ek__BackingField_25(),
	WebSocket_t231538678AB4B76293D12A78A9D798992897B3BD::get_offset_of_U3CHandshakeHostU3Ek__BackingField_26(),
	WebSocket_t231538678AB4B76293D12A78A9D798992897B3BD::get_offset_of_U3COriginU3Ek__BackingField_27(),
	WebSocket_t231538678AB4B76293D12A78A9D798992897B3BD::get_offset_of_U3CNoDelayU3Ek__BackingField_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5573 = { sizeof (WebSocketCommandInfo_t78D3A11DD086E0229D1AD938B61A9A9FC5CB133D), -1, sizeof(WebSocketCommandInfo_t78D3A11DD086E0229D1AD938B61A9A9FC5CB133D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5573[5] = 
{
	WebSocketCommandInfo_t78D3A11DD086E0229D1AD938B61A9A9FC5CB133D::get_offset_of_U3CKeyU3Ek__BackingField_0(),
	WebSocketCommandInfo_t78D3A11DD086E0229D1AD938B61A9A9FC5CB133D::get_offset_of_U3CDataU3Ek__BackingField_1(),
	WebSocketCommandInfo_t78D3A11DD086E0229D1AD938B61A9A9FC5CB133D::get_offset_of_U3CTextU3Ek__BackingField_2(),
	WebSocketCommandInfo_t78D3A11DD086E0229D1AD938B61A9A9FC5CB133D::get_offset_of_U3CCloseStatusCodeU3Ek__BackingField_3(),
	WebSocketCommandInfo_t78D3A11DD086E0229D1AD938B61A9A9FC5CB133D_StaticFields::get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5574 = { sizeof (WebSocketState_tDC245B01F1B2B105368BD47561F2EB76420ECEB0)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5574[6] = 
{
	WebSocketState_tDC245B01F1B2B105368BD47561F2EB76420ECEB0::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5575 = { sizeof (WebSocketVersion_tBEBD9FE6DD3A0FA1A709542182931F60349A8C11)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5575[5] = 
{
	WebSocketVersion_tBEBD9FE6DD3A0FA1A709542182931F60349A8C11::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5576 = { sizeof (GSTlsVerifyCertificate_t4CDE88F435AAEEBBE8DD7B4316D3C207DC83CE1A), -1, sizeof(GSTlsVerifyCertificate_t4CDE88F435AAEEBBE8DD7B4316D3C207DC83CE1A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5576[1] = 
{
	GSTlsVerifyCertificate_t4CDE88F435AAEEBBE8DD7B4316D3C207DC83CE1A_StaticFields::get_offset_of_U3COnVerifyCertificateU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5577 = { sizeof (GSTlsClient_tF62502461009667A6418D66DE53E678EDB4739E4), -1, sizeof(GSTlsClient_tF62502461009667A6418D66DE53E678EDB4739E4_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5577[2] = 
{
	GSTlsClient_tF62502461009667A6418D66DE53E678EDB4739E4::get_offset_of_hostName_8(),
	GSTlsClient_tF62502461009667A6418D66DE53E678EDB4739E4_StaticFields::get_offset_of_logger_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5578 = { sizeof (GSTlsAuthentication_tC82EA27062C5D8E4C93250F7CB58A4A08B90108E), -1, sizeof(GSTlsAuthentication_tC82EA27062C5D8E4C93250F7CB58A4A08B90108E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5578[2] = 
{
	GSTlsAuthentication_tC82EA27062C5D8E4C93250F7CB58A4A08B90108E_StaticFields::get_offset_of_rootCert_0(),
	GSTlsAuthentication_tC82EA27062C5D8E4C93250F7CB58A4A08B90108E::get_offset_of_validCertNames_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5579 = { sizeof (DuplexTlsStream_t5F3DDF883886B8B18D0F12C2D0DFD16D9DC53768), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5579[1] = 
{
	DuplexTlsStream_t5F3DDF883886B8B18D0F12C2D0DFD16D9DC53768::get_offset_of_wrapped_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5580 = { sizeof (ReadDelegate_t264EEB5F4B254FA08B9839A5684FDC237A566958), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5581 = { sizeof (WriteDelegate_tADF42C77ACC966499334BF60ADFF16CC179DC42D), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5582 = { sizeof (SslStreamTcpSession_tF0FC88875D6DEA00C115C0A7EAC4E113E5D68FD1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5582[1] = 
{
	SslStreamTcpSession_tF0FC88875D6DEA00C115C0A7EAC4E113E5D68FD1::get_offset_of_m_SslStream_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5583 = { sizeof (SslAsyncState_t60F1683B2FD929CDBF0E40BAEFD4F3A34A2D9D0E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5583[3] = 
{
	SslAsyncState_t60F1683B2FD929CDBF0E40BAEFD4F3A34A2D9D0E::get_offset_of_U3CSslStreamU3Ek__BackingField_0(),
	SslAsyncState_t60F1683B2FD929CDBF0E40BAEFD4F3A34A2D9D0E::get_offset_of_U3CClientU3Ek__BackingField_1(),
	SslAsyncState_t60F1683B2FD929CDBF0E40BAEFD4F3A34A2D9D0E::get_offset_of_U3CSendingItemsU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5584 = { sizeof (ProxyConnectorBase_t66DBD023064F63BFABF4E28901CEF3C83BC92A09), -1, sizeof(ProxyConnectorBase_t66DBD023064F63BFABF4E28901CEF3C83BC92A09_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5584[3] = 
{
	ProxyConnectorBase_t66DBD023064F63BFABF4E28901CEF3C83BC92A09_StaticFields::get_offset_of_ASCIIEncoding_0(),
	ProxyConnectorBase_t66DBD023064F63BFABF4E28901CEF3C83BC92A09::get_offset_of_m_Completed_1(),
	ProxyConnectorBase_t66DBD023064F63BFABF4E28901CEF3C83BC92A09::get_offset_of_U3CProxyEndPointU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5585 = { sizeof (HttpConnectProxy_t2F371BBDAC376A0D71C428D002057AEAAECD341E), -1, sizeof(HttpConnectProxy_t2F371BBDAC376A0D71C428D002057AEAAECD341E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5585[2] = 
{
	HttpConnectProxy_t2F371BBDAC376A0D71C428D002057AEAAECD341E_StaticFields::get_offset_of_m_LineSeparator_3(),
	HttpConnectProxy_t2F371BBDAC376A0D71C428D002057AEAAECD341E::get_offset_of_m_ReceiveBufferSize_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5586 = { sizeof (ConnectContext_t733BBB94497088F90C3FA43A78F9E172FA4406C5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5586[2] = 
{
	ConnectContext_t733BBB94497088F90C3FA43A78F9E172FA4406C5::get_offset_of_U3CSocketU3Ek__BackingField_0(),
	ConnectContext_t733BBB94497088F90C3FA43A78F9E172FA4406C5::get_offset_of_U3CSearchStateU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5587 = { sizeof (U3CPrivateImplementationDetailsU3EU7B19DCFADBU2DF7C0U2D46D9U2DA532U2DDE6625B321CEU7D_t15CEDF0DBF0B8C9B4B10E7F1889BD810545675FB), -1, sizeof(U3CPrivateImplementationDetailsU3EU7B19DCFADBU2DF7C0U2D46D9U2DA532U2DDE6625B321CEU7D_t15CEDF0DBF0B8C9B4B10E7F1889BD810545675FB_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5587[1] = 
{
	U3CPrivateImplementationDetailsU3EU7B19DCFADBU2DF7C0U2D46D9U2DA532U2DDE6625B321CEU7D_t15CEDF0DBF0B8C9B4B10E7F1889BD810545675FB_StaticFields::get_offset_of_U24U24method0x60002a4U2D1_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5588 = { sizeof (__StaticArrayInitTypeSizeU3D36_t5B0C0E5859692BC733B20EE930353284B7AB95E4)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D36_t5B0C0E5859692BC733B20EE930353284B7AB95E4 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5589 = { sizeof (U3CModuleU3E_t1E56E51F0CA5E53824F1EC0104C755EDCF29A5D8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5590 = { sizeof (XName_t09FB17CAE73454416CC2CC694E68A9AFFD3C8F7A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5590[3] = 
{
	XName_t09FB17CAE73454416CC2CC694E68A9AFFD3C8F7A::get_offset_of_ns_0(),
	XName_t09FB17CAE73454416CC2CC694E68A9AFFD3C8F7A::get_offset_of_localName_1(),
	XName_t09FB17CAE73454416CC2CC694E68A9AFFD3C8F7A::get_offset_of_hashCode_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5591 = { sizeof (NameSerializer_t7C4F2D6B7D9A8EF7996BB9C613A3AF6243E85D2A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5591[1] = 
{
	NameSerializer_t7C4F2D6B7D9A8EF7996BB9C613A3AF6243E85D2A::get_offset_of_expandedName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5592 = { sizeof (XNamespace_t771F5C81A42B6DFC65678BB24EE414F8F9E7831D), -1, sizeof(XNamespace_t771F5C81A42B6DFC65678BB24EE414F8F9E7831D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5592[7] = 
{
	XNamespace_t771F5C81A42B6DFC65678BB24EE414F8F9E7831D_StaticFields::get_offset_of_namespaces_0(),
	XNamespace_t771F5C81A42B6DFC65678BB24EE414F8F9E7831D_StaticFields::get_offset_of_refNone_1(),
	XNamespace_t771F5C81A42B6DFC65678BB24EE414F8F9E7831D_StaticFields::get_offset_of_refXml_2(),
	XNamespace_t771F5C81A42B6DFC65678BB24EE414F8F9E7831D_StaticFields::get_offset_of_refXmlns_3(),
	XNamespace_t771F5C81A42B6DFC65678BB24EE414F8F9E7831D::get_offset_of_namespaceName_4(),
	XNamespace_t771F5C81A42B6DFC65678BB24EE414F8F9E7831D::get_offset_of_hashCode_5(),
	XNamespace_t771F5C81A42B6DFC65678BB24EE414F8F9E7831D::get_offset_of_names_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5593 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5593[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5594 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5595 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5595[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5596 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5596[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5597 = { sizeof (XObject_t0DA241208A0EC65CA16AEA485DD80F21A693EDEF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5597[2] = 
{
	XObject_t0DA241208A0EC65CA16AEA485DD80F21A693EDEF::get_offset_of_parent_0(),
	XObject_t0DA241208A0EC65CA16AEA485DD80F21A693EDEF::get_offset_of_annotations_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5598 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5598[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5599 = { sizeof (XObjectChangeAnnotation_tF9972FFFFA58338DFC8684004D7C3D037B72419E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5599[2] = 
{
	XObjectChangeAnnotation_tF9972FFFFA58338DFC8684004D7C3D037B72419E::get_offset_of_changing_0(),
	XObjectChangeAnnotation_tF9972FFFFA58338DFC8684004D7C3D037B72419E::get_offset_of_changed_1(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
