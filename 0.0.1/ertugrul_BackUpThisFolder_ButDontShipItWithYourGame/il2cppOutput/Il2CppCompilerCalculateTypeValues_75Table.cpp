﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.Action`1<Zenject.DiContainer>
struct Action_1_t47FB75EB7E275E7EC20C1ADC32AC936BD0BD4E84;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.Type,Zenject.ZenjectTypeInfo>
struct Dictionary_2_t6BA2F0666D38DF096F45B41AB67474768713CE58;
// System.Collections.Generic.Dictionary`2<UnityEngine.SceneManagement.Scene,Zenject.SceneContext>
struct Dictionary_2_tBF30C50B2A41E14F03B109BD5B4CB0BD409ED51A;
// System.Collections.Generic.Dictionary`2<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.ISignalHandler>>
struct Dictionary_2_t86769489CE30B4DF4E100726F6DBAEB5426914F3;
// System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.IFixedTickable>>
struct LinkedList_1_tBBDF9B16D26EC4D4125BD3544EFCF2BB5B034AEC;
// System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ILateTickable>>
struct LinkedList_1_t58FC5DC068D268925ACCCB10D5000C44F34C14F7;
// System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>
struct LinkedList_1_tFE981B1647A6CB01772C7ADE559050413D3C2A11;
// System.Collections.Generic.List`1<ModestTree.Util.ValuePair`2<System.Type,System.Int32>>
struct List_1_t1789D56162D6592672665E24594EEF62242C02AD;
// System.Collections.Generic.List`1<System.Type>
struct List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0;
// System.Collections.Generic.List`1<Zenject.DisposableManager/DisposableInfo>
struct List_1_t215C824324A5F93D9AD746B240CADA7024FD6E5F;
// System.Collections.Generic.List`1<Zenject.DisposableManager/LateDisposableInfo>
struct List_1_tC82F8F214CE2B50FCFFF0DA5B16AFE7A187DE16E;
// System.Collections.Generic.List`1<Zenject.GuiRenderableManager/RenderableInfo>
struct List_1_t65C300941F2B865CF3205ED72E9DB32907E3D60E;
// System.Collections.Generic.List`1<Zenject.IFixedTickable>
struct List_1_t38D179FF9DBA1621B955766571B9285B8C82D0C0;
// System.Collections.Generic.List`1<Zenject.ILateTickable>
struct List_1_tA0FEBB86373E09790B3119D49BB132AF39CC8501;
// System.Collections.Generic.List`1<Zenject.IMemoryPool>
struct List_1_tEA3DAF67A74E24686AF74A106BC10F72B3AE5E25;
// System.Collections.Generic.List`1<Zenject.ITickable>
struct List_1_t6AC9EDC1F701BA128484C3672692B4A6025F149A;
// System.Collections.Generic.List`1<Zenject.InitializableManager/InitializableInfo>
struct List_1_t5ED07C7C7BF89997A758213D8F43623242932E37;
// System.Collections.Generic.List`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.IFixedTickable>>
struct List_1_t9F4BD1E9D1073E4238B05CCE07350F764C08B4A3;
// System.Collections.Generic.List`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ILateTickable>>
struct List_1_t6B84519EDCA0D36C2A2582D3956B2FBB018881DC;
// System.Collections.Generic.List`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>
struct List_1_tEC7EBEAB21D126318DE29351C73F8FDC2CDBB8EF;
// System.Func`2<ModestTree.Util.ValuePair`2<System.Type,System.Int32>,System.Int32>
struct Func_2_tB33DB0601EFB82905DF1AE5730828B918DC19DBD;
// System.Func`2<ModestTree.Util.ValuePair`2<System.Type,System.Int32>,System.Type>
struct Func_2_t2BAC6B7180FBCB00D561A8DE37970649E8F9228C;
// System.Func`2<System.Collections.Generic.List`1<Zenject.ISignalHandler>,System.Collections.Generic.IEnumerable`1<Zenject.ISignalHandler>>
struct Func_2_t0E839BE25BE5FB09C0BD94FE098AAFF87902125B;
// System.Func`2<Zenject.DisposableManager/DisposableInfo,System.Int32>
struct Func_2_tCDB2CA85B71B7CCBDB36FFA362CDD9894806AA58;
// System.Func`2<Zenject.DisposableManager/LateDisposableInfo,System.Int32>
struct Func_2_t97BBBF87CF2EE8A6B605FEED2AADAAE60EAE94A5;
// System.Func`2<Zenject.GuiRenderableManager/RenderableInfo,System.Int32>
struct Func_2_tBDFEF3A5F7493668E88A94FBD94B4F45A5BC621B;
// System.Func`2<Zenject.InitializableManager/InitializableInfo,System.Int32>
struct Func_2_t555EE9EF10BC9BEF7B0AFCDF0D22623812AFC123;
// System.IDisposable
struct IDisposable_t7218B22548186B208D65EA5B7870503810A2D15A;
// System.String
struct String_t;
// System.Text.RegularExpressions.Regex
struct Regex_tFD46E63A462E852189FD6AB4E2B0B67C4D8FDBDF;
// System.Type
struct Type_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// Zenject.BindFinalizerWrapper
struct BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8;
// Zenject.BindInfo
struct BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991;
// Zenject.BindingId
struct BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA;
// Zenject.DiContainer
struct DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5;
// Zenject.DisposableManager
struct DisposableManager_tCB394E2EA1FE174EB07A1082C5F1760FFE6F9ADC;
// Zenject.FixedTickablesTaskUpdater
struct FixedTickablesTaskUpdater_t72C0FFEE087922D42CEBB55EE5243DC64D8B367A;
// Zenject.GuiRenderableManager
struct GuiRenderableManager_tC531B6419C6A6E8E138BA24F00B89723BFB02039;
// Zenject.IFixedTickable
struct IFixedTickable_tE21C9C3E6F9B5CD50439CC73EFB795B6F0884C7A;
// Zenject.IGuiRenderable
struct IGuiRenderable_tA5CD53A98D1C1AE38F56490E931097C21B561EC3;
// Zenject.IInitializable
struct IInitializable_t8CDB5239D7F1A24EC49AD4017F6D65D2A8A5F345;
// Zenject.ILateDisposable
struct ILateDisposable_tFC8F10A1D73EF8738756317B8C43EA4C2EF1EE1B;
// Zenject.ILateTickable
struct ILateTickable_tBA178F04BC56F1ED0A12CB051DE22E1B99D8FF81;
// Zenject.ITickable
struct ITickable_tB1E852FC8B953CA984BCEAFFA1A34198C4AB388C;
// Zenject.InitializableManager
struct InitializableManager_tD805DAFAB6779958C0DEB4BBDA8736C4446A34E9;
// Zenject.LateTickablesTaskUpdater
struct LateTickablesTaskUpdater_t66115F857C2D233BB36D6086F2FFC6D32580B9EB;
// Zenject.SceneContext
struct SceneContext_tA69C18F583C410BB2DA86B5C1F86F27459DA8CA2;
// Zenject.SceneContextRegistry
struct SceneContextRegistry_tEBF3D91B318174447F3993BDE553F5D431B9A7FD;
// Zenject.SignalManager
struct SignalManager_t38E64F258F79FF33F3FDBB9423271BEFF03D7673;
// Zenject.SignalSettings
struct SignalSettings_tD753AA3D16CEA36F97CDAEC5428C5217F1EB6540;
// Zenject.TickableManager
struct TickableManager_t9CDD6FA2E97FC2B4BD99A06A36D29DBE66C03598;
// Zenject.TickablesTaskUpdater
struct TickablesTaskUpdater_tF277702A3B140691294521F1606DE293E0DE7897;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#define ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef DISPOSABLEMANAGER_TCB394E2EA1FE174EB07A1082C5F1760FFE6F9ADC_H
#define DISPOSABLEMANAGER_TCB394E2EA1FE174EB07A1082C5F1760FFE6F9ADC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.DisposableManager
struct  DisposableManager_tCB394E2EA1FE174EB07A1082C5F1760FFE6F9ADC  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Zenject.DisposableManager_DisposableInfo> Zenject.DisposableManager::_disposables
	List_1_t215C824324A5F93D9AD746B240CADA7024FD6E5F * ____disposables_0;
	// System.Collections.Generic.List`1<Zenject.DisposableManager_LateDisposableInfo> Zenject.DisposableManager::_lateDisposables
	List_1_tC82F8F214CE2B50FCFFF0DA5B16AFE7A187DE16E * ____lateDisposables_1;
	// System.Boolean Zenject.DisposableManager::_disposed
	bool ____disposed_2;
	// System.Boolean Zenject.DisposableManager::_lateDisposed
	bool ____lateDisposed_3;

public:
	inline static int32_t get_offset_of__disposables_0() { return static_cast<int32_t>(offsetof(DisposableManager_tCB394E2EA1FE174EB07A1082C5F1760FFE6F9ADC, ____disposables_0)); }
	inline List_1_t215C824324A5F93D9AD746B240CADA7024FD6E5F * get__disposables_0() const { return ____disposables_0; }
	inline List_1_t215C824324A5F93D9AD746B240CADA7024FD6E5F ** get_address_of__disposables_0() { return &____disposables_0; }
	inline void set__disposables_0(List_1_t215C824324A5F93D9AD746B240CADA7024FD6E5F * value)
	{
		____disposables_0 = value;
		Il2CppCodeGenWriteBarrier((&____disposables_0), value);
	}

	inline static int32_t get_offset_of__lateDisposables_1() { return static_cast<int32_t>(offsetof(DisposableManager_tCB394E2EA1FE174EB07A1082C5F1760FFE6F9ADC, ____lateDisposables_1)); }
	inline List_1_tC82F8F214CE2B50FCFFF0DA5B16AFE7A187DE16E * get__lateDisposables_1() const { return ____lateDisposables_1; }
	inline List_1_tC82F8F214CE2B50FCFFF0DA5B16AFE7A187DE16E ** get_address_of__lateDisposables_1() { return &____lateDisposables_1; }
	inline void set__lateDisposables_1(List_1_tC82F8F214CE2B50FCFFF0DA5B16AFE7A187DE16E * value)
	{
		____lateDisposables_1 = value;
		Il2CppCodeGenWriteBarrier((&____lateDisposables_1), value);
	}

	inline static int32_t get_offset_of__disposed_2() { return static_cast<int32_t>(offsetof(DisposableManager_tCB394E2EA1FE174EB07A1082C5F1760FFE6F9ADC, ____disposed_2)); }
	inline bool get__disposed_2() const { return ____disposed_2; }
	inline bool* get_address_of__disposed_2() { return &____disposed_2; }
	inline void set__disposed_2(bool value)
	{
		____disposed_2 = value;
	}

	inline static int32_t get_offset_of__lateDisposed_3() { return static_cast<int32_t>(offsetof(DisposableManager_tCB394E2EA1FE174EB07A1082C5F1760FFE6F9ADC, ____lateDisposed_3)); }
	inline bool get__lateDisposed_3() const { return ____lateDisposed_3; }
	inline bool* get_address_of__lateDisposed_3() { return &____lateDisposed_3; }
	inline void set__lateDisposed_3(bool value)
	{
		____lateDisposed_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISPOSABLEMANAGER_TCB394E2EA1FE174EB07A1082C5F1760FFE6F9ADC_H
#ifndef U3CU3EC_TF8E4EB4BF1F15218F9296FA6DD1687807325F7D9_H
#define U3CU3EC_TF8E4EB4BF1F15218F9296FA6DD1687807325F7D9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.DisposableManager_<>c
struct  U3CU3Ec_tF8E4EB4BF1F15218F9296FA6DD1687807325F7D9  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tF8E4EB4BF1F15218F9296FA6DD1687807325F7D9_StaticFields
{
public:
	// Zenject.DisposableManager_<>c Zenject.DisposableManager_<>c::<>9
	U3CU3Ec_tF8E4EB4BF1F15218F9296FA6DD1687807325F7D9 * ___U3CU3E9_0;
	// System.Func`2<ModestTree.Util.ValuePair`2<System.Type,System.Int32>,System.Int32> Zenject.DisposableManager_<>c::<>9__4_1
	Func_2_tB33DB0601EFB82905DF1AE5730828B918DC19DBD * ___U3CU3E9__4_1_1;
	// System.Func`2<ModestTree.Util.ValuePair`2<System.Type,System.Int32>,System.Int32> Zenject.DisposableManager_<>c::<>9__4_3
	Func_2_tB33DB0601EFB82905DF1AE5730828B918DC19DBD * ___U3CU3E9__4_3_2;
	// System.Func`2<Zenject.DisposableManager_LateDisposableInfo,System.Int32> Zenject.DisposableManager_<>c::<>9__8_0
	Func_2_t97BBBF87CF2EE8A6B605FEED2AADAAE60EAE94A5 * ___U3CU3E9__8_0_3;
	// System.Func`2<Zenject.DisposableManager_DisposableInfo,System.Int32> Zenject.DisposableManager_<>c::<>9__9_0
	Func_2_tCDB2CA85B71B7CCBDB36FFA362CDD9894806AA58 * ___U3CU3E9__9_0_4;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tF8E4EB4BF1F15218F9296FA6DD1687807325F7D9_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tF8E4EB4BF1F15218F9296FA6DD1687807325F7D9 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tF8E4EB4BF1F15218F9296FA6DD1687807325F7D9 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tF8E4EB4BF1F15218F9296FA6DD1687807325F7D9 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__4_1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tF8E4EB4BF1F15218F9296FA6DD1687807325F7D9_StaticFields, ___U3CU3E9__4_1_1)); }
	inline Func_2_tB33DB0601EFB82905DF1AE5730828B918DC19DBD * get_U3CU3E9__4_1_1() const { return ___U3CU3E9__4_1_1; }
	inline Func_2_tB33DB0601EFB82905DF1AE5730828B918DC19DBD ** get_address_of_U3CU3E9__4_1_1() { return &___U3CU3E9__4_1_1; }
	inline void set_U3CU3E9__4_1_1(Func_2_tB33DB0601EFB82905DF1AE5730828B918DC19DBD * value)
	{
		___U3CU3E9__4_1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__4_1_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__4_3_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_tF8E4EB4BF1F15218F9296FA6DD1687807325F7D9_StaticFields, ___U3CU3E9__4_3_2)); }
	inline Func_2_tB33DB0601EFB82905DF1AE5730828B918DC19DBD * get_U3CU3E9__4_3_2() const { return ___U3CU3E9__4_3_2; }
	inline Func_2_tB33DB0601EFB82905DF1AE5730828B918DC19DBD ** get_address_of_U3CU3E9__4_3_2() { return &___U3CU3E9__4_3_2; }
	inline void set_U3CU3E9__4_3_2(Func_2_tB33DB0601EFB82905DF1AE5730828B918DC19DBD * value)
	{
		___U3CU3E9__4_3_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__4_3_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__8_0_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_tF8E4EB4BF1F15218F9296FA6DD1687807325F7D9_StaticFields, ___U3CU3E9__8_0_3)); }
	inline Func_2_t97BBBF87CF2EE8A6B605FEED2AADAAE60EAE94A5 * get_U3CU3E9__8_0_3() const { return ___U3CU3E9__8_0_3; }
	inline Func_2_t97BBBF87CF2EE8A6B605FEED2AADAAE60EAE94A5 ** get_address_of_U3CU3E9__8_0_3() { return &___U3CU3E9__8_0_3; }
	inline void set_U3CU3E9__8_0_3(Func_2_t97BBBF87CF2EE8A6B605FEED2AADAAE60EAE94A5 * value)
	{
		___U3CU3E9__8_0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__8_0_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__9_0_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_tF8E4EB4BF1F15218F9296FA6DD1687807325F7D9_StaticFields, ___U3CU3E9__9_0_4)); }
	inline Func_2_tCDB2CA85B71B7CCBDB36FFA362CDD9894806AA58 * get_U3CU3E9__9_0_4() const { return ___U3CU3E9__9_0_4; }
	inline Func_2_tCDB2CA85B71B7CCBDB36FFA362CDD9894806AA58 ** get_address_of_U3CU3E9__9_0_4() { return &___U3CU3E9__9_0_4; }
	inline void set_U3CU3E9__9_0_4(Func_2_tCDB2CA85B71B7CCBDB36FFA362CDD9894806AA58 * value)
	{
		___U3CU3E9__9_0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__9_0_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TF8E4EB4BF1F15218F9296FA6DD1687807325F7D9_H
#ifndef U3CU3EC__DISPLAYCLASS4_0_T479536EF753C830063F487D1CDB17384FED70372_H
#define U3CU3EC__DISPLAYCLASS4_0_T479536EF753C830063F487D1CDB17384FED70372_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.DisposableManager_<>c__DisplayClass4_0
struct  U3CU3Ec__DisplayClass4_0_t479536EF753C830063F487D1CDB17384FED70372  : public RuntimeObject
{
public:
	// System.IDisposable Zenject.DisposableManager_<>c__DisplayClass4_0::disposable
	RuntimeObject* ___disposable_0;

public:
	inline static int32_t get_offset_of_disposable_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_t479536EF753C830063F487D1CDB17384FED70372, ___disposable_0)); }
	inline RuntimeObject* get_disposable_0() const { return ___disposable_0; }
	inline RuntimeObject** get_address_of_disposable_0() { return &___disposable_0; }
	inline void set_disposable_0(RuntimeObject* value)
	{
		___disposable_0 = value;
		Il2CppCodeGenWriteBarrier((&___disposable_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS4_0_T479536EF753C830063F487D1CDB17384FED70372_H
#ifndef U3CU3EC__DISPLAYCLASS4_1_TEDC62BF0F96622C661ADCED44FABD29E95F00E6F_H
#define U3CU3EC__DISPLAYCLASS4_1_TEDC62BF0F96622C661ADCED44FABD29E95F00E6F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.DisposableManager_<>c__DisplayClass4_1
struct  U3CU3Ec__DisplayClass4_1_tEDC62BF0F96622C661ADCED44FABD29E95F00E6F  : public RuntimeObject
{
public:
	// Zenject.ILateDisposable Zenject.DisposableManager_<>c__DisplayClass4_1::lateDisposable
	RuntimeObject* ___lateDisposable_0;

public:
	inline static int32_t get_offset_of_lateDisposable_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_1_tEDC62BF0F96622C661ADCED44FABD29E95F00E6F, ___lateDisposable_0)); }
	inline RuntimeObject* get_lateDisposable_0() const { return ___lateDisposable_0; }
	inline RuntimeObject** get_address_of_lateDisposable_0() { return &___lateDisposable_0; }
	inline void set_lateDisposable_0(RuntimeObject* value)
	{
		___lateDisposable_0 = value;
		Il2CppCodeGenWriteBarrier((&___lateDisposable_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS4_1_TEDC62BF0F96622C661ADCED44FABD29E95F00E6F_H
#ifndef U3CU3EC__DISPLAYCLASS7_0_T88399ACE864238F807998453463C5E4D86A49007_H
#define U3CU3EC__DISPLAYCLASS7_0_T88399ACE864238F807998453463C5E4D86A49007_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.DisposableManager_<>c__DisplayClass7_0
struct  U3CU3Ec__DisplayClass7_0_t88399ACE864238F807998453463C5E4D86A49007  : public RuntimeObject
{
public:
	// System.IDisposable Zenject.DisposableManager_<>c__DisplayClass7_0::disposable
	RuntimeObject* ___disposable_0;

public:
	inline static int32_t get_offset_of_disposable_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass7_0_t88399ACE864238F807998453463C5E4D86A49007, ___disposable_0)); }
	inline RuntimeObject* get_disposable_0() const { return ___disposable_0; }
	inline RuntimeObject** get_address_of_disposable_0() { return &___disposable_0; }
	inline void set_disposable_0(RuntimeObject* value)
	{
		___disposable_0 = value;
		Il2CppCodeGenWriteBarrier((&___disposable_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS7_0_T88399ACE864238F807998453463C5E4D86A49007_H
#ifndef DISPOSABLEINFO_T170B8D5A1C5227BB4833C70484FCF3514B6F13A3_H
#define DISPOSABLEINFO_T170B8D5A1C5227BB4833C70484FCF3514B6F13A3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.DisposableManager_DisposableInfo
struct  DisposableInfo_t170B8D5A1C5227BB4833C70484FCF3514B6F13A3  : public RuntimeObject
{
public:
	// System.IDisposable Zenject.DisposableManager_DisposableInfo::Disposable
	RuntimeObject* ___Disposable_0;
	// System.Int32 Zenject.DisposableManager_DisposableInfo::Priority
	int32_t ___Priority_1;

public:
	inline static int32_t get_offset_of_Disposable_0() { return static_cast<int32_t>(offsetof(DisposableInfo_t170B8D5A1C5227BB4833C70484FCF3514B6F13A3, ___Disposable_0)); }
	inline RuntimeObject* get_Disposable_0() const { return ___Disposable_0; }
	inline RuntimeObject** get_address_of_Disposable_0() { return &___Disposable_0; }
	inline void set_Disposable_0(RuntimeObject* value)
	{
		___Disposable_0 = value;
		Il2CppCodeGenWriteBarrier((&___Disposable_0), value);
	}

	inline static int32_t get_offset_of_Priority_1() { return static_cast<int32_t>(offsetof(DisposableInfo_t170B8D5A1C5227BB4833C70484FCF3514B6F13A3, ___Priority_1)); }
	inline int32_t get_Priority_1() const { return ___Priority_1; }
	inline int32_t* get_address_of_Priority_1() { return &___Priority_1; }
	inline void set_Priority_1(int32_t value)
	{
		___Priority_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISPOSABLEINFO_T170B8D5A1C5227BB4833C70484FCF3514B6F13A3_H
#ifndef LATEDISPOSABLEINFO_T1C039847D21F8D03F94453CFCE13D0B19637BA59_H
#define LATEDISPOSABLEINFO_T1C039847D21F8D03F94453CFCE13D0B19637BA59_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.DisposableManager_LateDisposableInfo
struct  LateDisposableInfo_t1C039847D21F8D03F94453CFCE13D0B19637BA59  : public RuntimeObject
{
public:
	// Zenject.ILateDisposable Zenject.DisposableManager_LateDisposableInfo::LateDisposable
	RuntimeObject* ___LateDisposable_0;
	// System.Int32 Zenject.DisposableManager_LateDisposableInfo::Priority
	int32_t ___Priority_1;

public:
	inline static int32_t get_offset_of_LateDisposable_0() { return static_cast<int32_t>(offsetof(LateDisposableInfo_t1C039847D21F8D03F94453CFCE13D0B19637BA59, ___LateDisposable_0)); }
	inline RuntimeObject* get_LateDisposable_0() const { return ___LateDisposable_0; }
	inline RuntimeObject** get_address_of_LateDisposable_0() { return &___LateDisposable_0; }
	inline void set_LateDisposable_0(RuntimeObject* value)
	{
		___LateDisposable_0 = value;
		Il2CppCodeGenWriteBarrier((&___LateDisposable_0), value);
	}

	inline static int32_t get_offset_of_Priority_1() { return static_cast<int32_t>(offsetof(LateDisposableInfo_t1C039847D21F8D03F94453CFCE13D0B19637BA59, ___Priority_1)); }
	inline int32_t get_Priority_1() const { return ___Priority_1; }
	inline int32_t* get_address_of_Priority_1() { return &___Priority_1; }
	inline void set_Priority_1(int32_t value)
	{
		___Priority_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LATEDISPOSABLEINFO_T1C039847D21F8D03F94453CFCE13D0B19637BA59_H
#ifndef GUIRENDERABLEMANAGER_TC531B6419C6A6E8E138BA24F00B89723BFB02039_H
#define GUIRENDERABLEMANAGER_TC531B6419C6A6E8E138BA24F00B89723BFB02039_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.GuiRenderableManager
struct  GuiRenderableManager_tC531B6419C6A6E8E138BA24F00B89723BFB02039  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Zenject.GuiRenderableManager_RenderableInfo> Zenject.GuiRenderableManager::_renderables
	List_1_t65C300941F2B865CF3205ED72E9DB32907E3D60E * ____renderables_0;

public:
	inline static int32_t get_offset_of__renderables_0() { return static_cast<int32_t>(offsetof(GuiRenderableManager_tC531B6419C6A6E8E138BA24F00B89723BFB02039, ____renderables_0)); }
	inline List_1_t65C300941F2B865CF3205ED72E9DB32907E3D60E * get__renderables_0() const { return ____renderables_0; }
	inline List_1_t65C300941F2B865CF3205ED72E9DB32907E3D60E ** get_address_of__renderables_0() { return &____renderables_0; }
	inline void set__renderables_0(List_1_t65C300941F2B865CF3205ED72E9DB32907E3D60E * value)
	{
		____renderables_0 = value;
		Il2CppCodeGenWriteBarrier((&____renderables_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUIRENDERABLEMANAGER_TC531B6419C6A6E8E138BA24F00B89723BFB02039_H
#ifndef U3CU3EC_T85EFB76798B2F1DB670FCA54235040FD2A810B9A_H
#define U3CU3EC_T85EFB76798B2F1DB670FCA54235040FD2A810B9A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.GuiRenderableManager_<>c
struct  U3CU3Ec_t85EFB76798B2F1DB670FCA54235040FD2A810B9A  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t85EFB76798B2F1DB670FCA54235040FD2A810B9A_StaticFields
{
public:
	// Zenject.GuiRenderableManager_<>c Zenject.GuiRenderableManager_<>c::<>9
	U3CU3Ec_t85EFB76798B2F1DB670FCA54235040FD2A810B9A * ___U3CU3E9_0;
	// System.Func`2<ModestTree.Util.ValuePair`2<System.Type,System.Int32>,System.Int32> Zenject.GuiRenderableManager_<>c::<>9__1_2
	Func_2_tB33DB0601EFB82905DF1AE5730828B918DC19DBD * ___U3CU3E9__1_2_1;
	// System.Func`2<Zenject.GuiRenderableManager_RenderableInfo,System.Int32> Zenject.GuiRenderableManager_<>c::<>9__1_0
	Func_2_tBDFEF3A5F7493668E88A94FBD94B4F45A5BC621B * ___U3CU3E9__1_0_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t85EFB76798B2F1DB670FCA54235040FD2A810B9A_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t85EFB76798B2F1DB670FCA54235040FD2A810B9A * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t85EFB76798B2F1DB670FCA54235040FD2A810B9A ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t85EFB76798B2F1DB670FCA54235040FD2A810B9A * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__1_2_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t85EFB76798B2F1DB670FCA54235040FD2A810B9A_StaticFields, ___U3CU3E9__1_2_1)); }
	inline Func_2_tB33DB0601EFB82905DF1AE5730828B918DC19DBD * get_U3CU3E9__1_2_1() const { return ___U3CU3E9__1_2_1; }
	inline Func_2_tB33DB0601EFB82905DF1AE5730828B918DC19DBD ** get_address_of_U3CU3E9__1_2_1() { return &___U3CU3E9__1_2_1; }
	inline void set_U3CU3E9__1_2_1(Func_2_tB33DB0601EFB82905DF1AE5730828B918DC19DBD * value)
	{
		___U3CU3E9__1_2_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__1_2_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__1_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t85EFB76798B2F1DB670FCA54235040FD2A810B9A_StaticFields, ___U3CU3E9__1_0_2)); }
	inline Func_2_tBDFEF3A5F7493668E88A94FBD94B4F45A5BC621B * get_U3CU3E9__1_0_2() const { return ___U3CU3E9__1_0_2; }
	inline Func_2_tBDFEF3A5F7493668E88A94FBD94B4F45A5BC621B ** get_address_of_U3CU3E9__1_0_2() { return &___U3CU3E9__1_0_2; }
	inline void set_U3CU3E9__1_0_2(Func_2_tBDFEF3A5F7493668E88A94FBD94B4F45A5BC621B * value)
	{
		___U3CU3E9__1_0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__1_0_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T85EFB76798B2F1DB670FCA54235040FD2A810B9A_H
#ifndef U3CU3EC__DISPLAYCLASS1_0_T668E7FAA7CCBE02D8D13DA0A36BA48FA457B538A_H
#define U3CU3EC__DISPLAYCLASS1_0_T668E7FAA7CCBE02D8D13DA0A36BA48FA457B538A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.GuiRenderableManager_<>c__DisplayClass1_0
struct  U3CU3Ec__DisplayClass1_0_t668E7FAA7CCBE02D8D13DA0A36BA48FA457B538A  : public RuntimeObject
{
public:
	// Zenject.IGuiRenderable Zenject.GuiRenderableManager_<>c__DisplayClass1_0::renderable
	RuntimeObject* ___renderable_0;

public:
	inline static int32_t get_offset_of_renderable_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass1_0_t668E7FAA7CCBE02D8D13DA0A36BA48FA457B538A, ___renderable_0)); }
	inline RuntimeObject* get_renderable_0() const { return ___renderable_0; }
	inline RuntimeObject** get_address_of_renderable_0() { return &___renderable_0; }
	inline void set_renderable_0(RuntimeObject* value)
	{
		___renderable_0 = value;
		Il2CppCodeGenWriteBarrier((&___renderable_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS1_0_T668E7FAA7CCBE02D8D13DA0A36BA48FA457B538A_H
#ifndef RENDERABLEINFO_T2AC58E82060D2F28405BD116283BD0C834DE0B17_H
#define RENDERABLEINFO_T2AC58E82060D2F28405BD116283BD0C834DE0B17_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.GuiRenderableManager_RenderableInfo
struct  RenderableInfo_t2AC58E82060D2F28405BD116283BD0C834DE0B17  : public RuntimeObject
{
public:
	// Zenject.IGuiRenderable Zenject.GuiRenderableManager_RenderableInfo::Renderable
	RuntimeObject* ___Renderable_0;
	// System.Int32 Zenject.GuiRenderableManager_RenderableInfo::Priority
	int32_t ___Priority_1;

public:
	inline static int32_t get_offset_of_Renderable_0() { return static_cast<int32_t>(offsetof(RenderableInfo_t2AC58E82060D2F28405BD116283BD0C834DE0B17, ___Renderable_0)); }
	inline RuntimeObject* get_Renderable_0() const { return ___Renderable_0; }
	inline RuntimeObject** get_address_of_Renderable_0() { return &___Renderable_0; }
	inline void set_Renderable_0(RuntimeObject* value)
	{
		___Renderable_0 = value;
		Il2CppCodeGenWriteBarrier((&___Renderable_0), value);
	}

	inline static int32_t get_offset_of_Priority_1() { return static_cast<int32_t>(offsetof(RenderableInfo_t2AC58E82060D2F28405BD116283BD0C834DE0B17, ___Priority_1)); }
	inline int32_t get_Priority_1() const { return ___Priority_1; }
	inline int32_t* get_address_of_Priority_1() { return &___Priority_1; }
	inline void set_Priority_1(int32_t value)
	{
		___Priority_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERABLEINFO_T2AC58E82060D2F28405BD116283BD0C834DE0B17_H
#ifndef INITIALIZABLEMANAGER_TD805DAFAB6779958C0DEB4BBDA8736C4446A34E9_H
#define INITIALIZABLEMANAGER_TD805DAFAB6779958C0DEB4BBDA8736C4446A34E9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.InitializableManager
struct  InitializableManager_tD805DAFAB6779958C0DEB4BBDA8736C4446A34E9  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Zenject.InitializableManager_InitializableInfo> Zenject.InitializableManager::_initializables
	List_1_t5ED07C7C7BF89997A758213D8F43623242932E37 * ____initializables_0;
	// System.Boolean Zenject.InitializableManager::_hasInitialized
	bool ____hasInitialized_1;

public:
	inline static int32_t get_offset_of__initializables_0() { return static_cast<int32_t>(offsetof(InitializableManager_tD805DAFAB6779958C0DEB4BBDA8736C4446A34E9, ____initializables_0)); }
	inline List_1_t5ED07C7C7BF89997A758213D8F43623242932E37 * get__initializables_0() const { return ____initializables_0; }
	inline List_1_t5ED07C7C7BF89997A758213D8F43623242932E37 ** get_address_of__initializables_0() { return &____initializables_0; }
	inline void set__initializables_0(List_1_t5ED07C7C7BF89997A758213D8F43623242932E37 * value)
	{
		____initializables_0 = value;
		Il2CppCodeGenWriteBarrier((&____initializables_0), value);
	}

	inline static int32_t get_offset_of__hasInitialized_1() { return static_cast<int32_t>(offsetof(InitializableManager_tD805DAFAB6779958C0DEB4BBDA8736C4446A34E9, ____hasInitialized_1)); }
	inline bool get__hasInitialized_1() const { return ____hasInitialized_1; }
	inline bool* get_address_of__hasInitialized_1() { return &____hasInitialized_1; }
	inline void set__hasInitialized_1(bool value)
	{
		____hasInitialized_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INITIALIZABLEMANAGER_TD805DAFAB6779958C0DEB4BBDA8736C4446A34E9_H
#ifndef U3CU3EC_T479EC722B066AB61FE75C61422C933886503B76F_H
#define U3CU3EC_T479EC722B066AB61FE75C61422C933886503B76F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.InitializableManager_<>c
struct  U3CU3Ec_t479EC722B066AB61FE75C61422C933886503B76F  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t479EC722B066AB61FE75C61422C933886503B76F_StaticFields
{
public:
	// Zenject.InitializableManager_<>c Zenject.InitializableManager_<>c::<>9
	U3CU3Ec_t479EC722B066AB61FE75C61422C933886503B76F * ___U3CU3E9_0;
	// System.Func`2<ModestTree.Util.ValuePair`2<System.Type,System.Int32>,System.Int32> Zenject.InitializableManager_<>c::<>9__2_1
	Func_2_tB33DB0601EFB82905DF1AE5730828B918DC19DBD * ___U3CU3E9__2_1_1;
	// System.Func`2<Zenject.InitializableManager_InitializableInfo,System.Int32> Zenject.InitializableManager_<>c::<>9__3_0
	Func_2_t555EE9EF10BC9BEF7B0AFCDF0D22623812AFC123 * ___U3CU3E9__3_0_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t479EC722B066AB61FE75C61422C933886503B76F_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t479EC722B066AB61FE75C61422C933886503B76F * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t479EC722B066AB61FE75C61422C933886503B76F ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t479EC722B066AB61FE75C61422C933886503B76F * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__2_1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t479EC722B066AB61FE75C61422C933886503B76F_StaticFields, ___U3CU3E9__2_1_1)); }
	inline Func_2_tB33DB0601EFB82905DF1AE5730828B918DC19DBD * get_U3CU3E9__2_1_1() const { return ___U3CU3E9__2_1_1; }
	inline Func_2_tB33DB0601EFB82905DF1AE5730828B918DC19DBD ** get_address_of_U3CU3E9__2_1_1() { return &___U3CU3E9__2_1_1; }
	inline void set_U3CU3E9__2_1_1(Func_2_tB33DB0601EFB82905DF1AE5730828B918DC19DBD * value)
	{
		___U3CU3E9__2_1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__2_1_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__3_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t479EC722B066AB61FE75C61422C933886503B76F_StaticFields, ___U3CU3E9__3_0_2)); }
	inline Func_2_t555EE9EF10BC9BEF7B0AFCDF0D22623812AFC123 * get_U3CU3E9__3_0_2() const { return ___U3CU3E9__3_0_2; }
	inline Func_2_t555EE9EF10BC9BEF7B0AFCDF0D22623812AFC123 ** get_address_of_U3CU3E9__3_0_2() { return &___U3CU3E9__3_0_2; }
	inline void set_U3CU3E9__3_0_2(Func_2_t555EE9EF10BC9BEF7B0AFCDF0D22623812AFC123 * value)
	{
		___U3CU3E9__3_0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__3_0_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T479EC722B066AB61FE75C61422C933886503B76F_H
#ifndef U3CU3EC__DISPLAYCLASS2_0_TF29DA2F6CF9258673E63A4E1D0306DD2B774348E_H
#define U3CU3EC__DISPLAYCLASS2_0_TF29DA2F6CF9258673E63A4E1D0306DD2B774348E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.InitializableManager_<>c__DisplayClass2_0
struct  U3CU3Ec__DisplayClass2_0_tF29DA2F6CF9258673E63A4E1D0306DD2B774348E  : public RuntimeObject
{
public:
	// Zenject.IInitializable Zenject.InitializableManager_<>c__DisplayClass2_0::initializable
	RuntimeObject* ___initializable_0;

public:
	inline static int32_t get_offset_of_initializable_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_tF29DA2F6CF9258673E63A4E1D0306DD2B774348E, ___initializable_0)); }
	inline RuntimeObject* get_initializable_0() const { return ___initializable_0; }
	inline RuntimeObject** get_address_of_initializable_0() { return &___initializable_0; }
	inline void set_initializable_0(RuntimeObject* value)
	{
		___initializable_0 = value;
		Il2CppCodeGenWriteBarrier((&___initializable_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS2_0_TF29DA2F6CF9258673E63A4E1D0306DD2B774348E_H
#ifndef INITIALIZABLEINFO_T6BA7D4503EFC7A38FA8FE914FC7116C14AA3B986_H
#define INITIALIZABLEINFO_T6BA7D4503EFC7A38FA8FE914FC7116C14AA3B986_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.InitializableManager_InitializableInfo
struct  InitializableInfo_t6BA7D4503EFC7A38FA8FE914FC7116C14AA3B986  : public RuntimeObject
{
public:
	// Zenject.IInitializable Zenject.InitializableManager_InitializableInfo::Initializable
	RuntimeObject* ___Initializable_0;
	// System.Int32 Zenject.InitializableManager_InitializableInfo::Priority
	int32_t ___Priority_1;

public:
	inline static int32_t get_offset_of_Initializable_0() { return static_cast<int32_t>(offsetof(InitializableInfo_t6BA7D4503EFC7A38FA8FE914FC7116C14AA3B986, ___Initializable_0)); }
	inline RuntimeObject* get_Initializable_0() const { return ___Initializable_0; }
	inline RuntimeObject** get_address_of_Initializable_0() { return &___Initializable_0; }
	inline void set_Initializable_0(RuntimeObject* value)
	{
		___Initializable_0 = value;
		Il2CppCodeGenWriteBarrier((&___Initializable_0), value);
	}

	inline static int32_t get_offset_of_Priority_1() { return static_cast<int32_t>(offsetof(InitializableInfo_t6BA7D4503EFC7A38FA8FE914FC7116C14AA3B986, ___Priority_1)); }
	inline int32_t get_Priority_1() const { return ___Priority_1; }
	inline int32_t* get_address_of_Priority_1() { return &___Priority_1; }
	inline void set_Priority_1(int32_t value)
	{
		___Priority_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INITIALIZABLEINFO_T6BA7D4503EFC7A38FA8FE914FC7116C14AA3B986_H
#ifndef INSTALLERBASE_T4F464ACE8FEB8CEEE7B4352A271F09894C28AD79_H
#define INSTALLERBASE_T4F464ACE8FEB8CEEE7B4352A271F09894C28AD79_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.InstallerBase
struct  InstallerBase_t4F464ACE8FEB8CEEE7B4352A271F09894C28AD79  : public RuntimeObject
{
public:
	// Zenject.DiContainer Zenject.InstallerBase::_container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____container_0;

public:
	inline static int32_t get_offset_of__container_0() { return static_cast<int32_t>(offsetof(InstallerBase_t4F464ACE8FEB8CEEE7B4352A271F09894C28AD79, ____container_0)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__container_0() const { return ____container_0; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__container_0() { return &____container_0; }
	inline void set__container_0(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____container_0 = value;
		Il2CppCodeGenWriteBarrier((&____container_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTALLERBASE_T4F464ACE8FEB8CEEE7B4352A271F09894C28AD79_H
#ifndef KERNEL_TBFCF518491D3E7A46A3537F00925D7F50949B357_H
#define KERNEL_TBFCF518491D3E7A46A3537F00925D7F50949B357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.Kernel
struct  Kernel_tBFCF518491D3E7A46A3537F00925D7F50949B357  : public RuntimeObject
{
public:
	// Zenject.TickableManager Zenject.Kernel::_tickableManager
	TickableManager_t9CDD6FA2E97FC2B4BD99A06A36D29DBE66C03598 * ____tickableManager_0;
	// Zenject.InitializableManager Zenject.Kernel::_initializableManager
	InitializableManager_tD805DAFAB6779958C0DEB4BBDA8736C4446A34E9 * ____initializableManager_1;
	// Zenject.DisposableManager Zenject.Kernel::_disposablesManager
	DisposableManager_tCB394E2EA1FE174EB07A1082C5F1760FFE6F9ADC * ____disposablesManager_2;

public:
	inline static int32_t get_offset_of__tickableManager_0() { return static_cast<int32_t>(offsetof(Kernel_tBFCF518491D3E7A46A3537F00925D7F50949B357, ____tickableManager_0)); }
	inline TickableManager_t9CDD6FA2E97FC2B4BD99A06A36D29DBE66C03598 * get__tickableManager_0() const { return ____tickableManager_0; }
	inline TickableManager_t9CDD6FA2E97FC2B4BD99A06A36D29DBE66C03598 ** get_address_of__tickableManager_0() { return &____tickableManager_0; }
	inline void set__tickableManager_0(TickableManager_t9CDD6FA2E97FC2B4BD99A06A36D29DBE66C03598 * value)
	{
		____tickableManager_0 = value;
		Il2CppCodeGenWriteBarrier((&____tickableManager_0), value);
	}

	inline static int32_t get_offset_of__initializableManager_1() { return static_cast<int32_t>(offsetof(Kernel_tBFCF518491D3E7A46A3537F00925D7F50949B357, ____initializableManager_1)); }
	inline InitializableManager_tD805DAFAB6779958C0DEB4BBDA8736C4446A34E9 * get__initializableManager_1() const { return ____initializableManager_1; }
	inline InitializableManager_tD805DAFAB6779958C0DEB4BBDA8736C4446A34E9 ** get_address_of__initializableManager_1() { return &____initializableManager_1; }
	inline void set__initializableManager_1(InitializableManager_tD805DAFAB6779958C0DEB4BBDA8736C4446A34E9 * value)
	{
		____initializableManager_1 = value;
		Il2CppCodeGenWriteBarrier((&____initializableManager_1), value);
	}

	inline static int32_t get_offset_of__disposablesManager_2() { return static_cast<int32_t>(offsetof(Kernel_tBFCF518491D3E7A46A3537F00925D7F50949B357, ____disposablesManager_2)); }
	inline DisposableManager_tCB394E2EA1FE174EB07A1082C5F1760FFE6F9ADC * get__disposablesManager_2() const { return ____disposablesManager_2; }
	inline DisposableManager_tCB394E2EA1FE174EB07A1082C5F1760FFE6F9ADC ** get_address_of__disposablesManager_2() { return &____disposablesManager_2; }
	inline void set__disposablesManager_2(DisposableManager_tCB394E2EA1FE174EB07A1082C5F1760FFE6F9ADC * value)
	{
		____disposablesManager_2 = value;
		Il2CppCodeGenWriteBarrier((&____disposablesManager_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KERNEL_TBFCF518491D3E7A46A3537F00925D7F50949B357_H
#ifndef NONLAZYBINDER_TA9FB0768A2F39559FBDDCAFBF940AAD240B7C43C_H
#define NONLAZYBINDER_TA9FB0768A2F39559FBDDCAFBF940AAD240B7C43C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.NonLazyBinder
struct  NonLazyBinder_tA9FB0768A2F39559FBDDCAFBF940AAD240B7C43C  : public RuntimeObject
{
public:
	// Zenject.BindInfo Zenject.NonLazyBinder::<BindInfo>k__BackingField
	BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * ___U3CBindInfoU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CBindInfoU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(NonLazyBinder_tA9FB0768A2F39559FBDDCAFBF940AAD240B7C43C, ___U3CBindInfoU3Ek__BackingField_0)); }
	inline BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * get_U3CBindInfoU3Ek__BackingField_0() const { return ___U3CBindInfoU3Ek__BackingField_0; }
	inline BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 ** get_address_of_U3CBindInfoU3Ek__BackingField_0() { return &___U3CBindInfoU3Ek__BackingField_0; }
	inline void set_U3CBindInfoU3Ek__BackingField_0(BindInfo_t56A8A30F2AD3D1AF612F7DFB5616C0EC81B2F991 * value)
	{
		___U3CBindInfoU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBindInfoU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NONLAZYBINDER_TA9FB0768A2F39559FBDDCAFBF940AAD240B7C43C_H
#ifndef POOLCLEANUPCHECKER_T220D33C1227BB69FD01AD538C6865689D4DF14E8_H
#define POOLCLEANUPCHECKER_T220D33C1227BB69FD01AD538C6865689D4DF14E8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.PoolCleanupChecker
struct  PoolCleanupChecker_t220D33C1227BB69FD01AD538C6865689D4DF14E8  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Zenject.IMemoryPool> Zenject.PoolCleanupChecker::_poolFactories
	List_1_tEA3DAF67A74E24686AF74A106BC10F72B3AE5E25 * ____poolFactories_0;

public:
	inline static int32_t get_offset_of__poolFactories_0() { return static_cast<int32_t>(offsetof(PoolCleanupChecker_t220D33C1227BB69FD01AD538C6865689D4DF14E8, ____poolFactories_0)); }
	inline List_1_tEA3DAF67A74E24686AF74A106BC10F72B3AE5E25 * get__poolFactories_0() const { return ____poolFactories_0; }
	inline List_1_tEA3DAF67A74E24686AF74A106BC10F72B3AE5E25 ** get_address_of__poolFactories_0() { return &____poolFactories_0; }
	inline void set__poolFactories_0(List_1_tEA3DAF67A74E24686AF74A106BC10F72B3AE5E25 * value)
	{
		____poolFactories_0 = value;
		Il2CppCodeGenWriteBarrier((&____poolFactories_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POOLCLEANUPCHECKER_T220D33C1227BB69FD01AD538C6865689D4DF14E8_H
#ifndef PROFILEBLOCK_T564682EC012FC24C9DBB2DAE56896ED9082AC2FA_H
#define PROFILEBLOCK_T564682EC012FC24C9DBB2DAE56896ED9082AC2FA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ProfileBlock
struct  ProfileBlock_t564682EC012FC24C9DBB2DAE56896ED9082AC2FA  : public RuntimeObject
{
public:

public:
};

struct ProfileBlock_t564682EC012FC24C9DBB2DAE56896ED9082AC2FA_StaticFields
{
public:
	// System.Text.RegularExpressions.Regex Zenject.ProfileBlock::<ProfilePattern>k__BackingField
	Regex_tFD46E63A462E852189FD6AB4E2B0B67C4D8FDBDF * ___U3CProfilePatternU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CProfilePatternU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ProfileBlock_t564682EC012FC24C9DBB2DAE56896ED9082AC2FA_StaticFields, ___U3CProfilePatternU3Ek__BackingField_0)); }
	inline Regex_tFD46E63A462E852189FD6AB4E2B0B67C4D8FDBDF * get_U3CProfilePatternU3Ek__BackingField_0() const { return ___U3CProfilePatternU3Ek__BackingField_0; }
	inline Regex_tFD46E63A462E852189FD6AB4E2B0B67C4D8FDBDF ** get_address_of_U3CProfilePatternU3Ek__BackingField_0() { return &___U3CProfilePatternU3Ek__BackingField_0; }
	inline void set_U3CProfilePatternU3Ek__BackingField_0(Regex_tFD46E63A462E852189FD6AB4E2B0B67C4D8FDBDF * value)
	{
		___U3CProfilePatternU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CProfilePatternU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROFILEBLOCK_T564682EC012FC24C9DBB2DAE56896ED9082AC2FA_H
#ifndef SCENECONTEXTREGISTRY_TEBF3D91B318174447F3993BDE553F5D431B9A7FD_H
#define SCENECONTEXTREGISTRY_TEBF3D91B318174447F3993BDE553F5D431B9A7FD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SceneContextRegistry
struct  SceneContextRegistry_tEBF3D91B318174447F3993BDE553F5D431B9A7FD  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<UnityEngine.SceneManagement.Scene,Zenject.SceneContext> Zenject.SceneContextRegistry::_map
	Dictionary_2_tBF30C50B2A41E14F03B109BD5B4CB0BD409ED51A * ____map_0;

public:
	inline static int32_t get_offset_of__map_0() { return static_cast<int32_t>(offsetof(SceneContextRegistry_tEBF3D91B318174447F3993BDE553F5D431B9A7FD, ____map_0)); }
	inline Dictionary_2_tBF30C50B2A41E14F03B109BD5B4CB0BD409ED51A * get__map_0() const { return ____map_0; }
	inline Dictionary_2_tBF30C50B2A41E14F03B109BD5B4CB0BD409ED51A ** get_address_of__map_0() { return &____map_0; }
	inline void set__map_0(Dictionary_2_tBF30C50B2A41E14F03B109BD5B4CB0BD409ED51A * value)
	{
		____map_0 = value;
		Il2CppCodeGenWriteBarrier((&____map_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENECONTEXTREGISTRY_TEBF3D91B318174447F3993BDE553F5D431B9A7FD_H
#ifndef SCENECONTEXTREGISTRYADDERANDREMOVER_T5A47B219C754FDC6E48B2187C7F078C538EA3C88_H
#define SCENECONTEXTREGISTRYADDERANDREMOVER_T5A47B219C754FDC6E48B2187C7F078C538EA3C88_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SceneContextRegistryAdderAndRemover
struct  SceneContextRegistryAdderAndRemover_t5A47B219C754FDC6E48B2187C7F078C538EA3C88  : public RuntimeObject
{
public:
	// Zenject.SceneContextRegistry Zenject.SceneContextRegistryAdderAndRemover::_registry
	SceneContextRegistry_tEBF3D91B318174447F3993BDE553F5D431B9A7FD * ____registry_0;
	// Zenject.SceneContext Zenject.SceneContextRegistryAdderAndRemover::_sceneContext
	SceneContext_tA69C18F583C410BB2DA86B5C1F86F27459DA8CA2 * ____sceneContext_1;

public:
	inline static int32_t get_offset_of__registry_0() { return static_cast<int32_t>(offsetof(SceneContextRegistryAdderAndRemover_t5A47B219C754FDC6E48B2187C7F078C538EA3C88, ____registry_0)); }
	inline SceneContextRegistry_tEBF3D91B318174447F3993BDE553F5D431B9A7FD * get__registry_0() const { return ____registry_0; }
	inline SceneContextRegistry_tEBF3D91B318174447F3993BDE553F5D431B9A7FD ** get_address_of__registry_0() { return &____registry_0; }
	inline void set__registry_0(SceneContextRegistry_tEBF3D91B318174447F3993BDE553F5D431B9A7FD * value)
	{
		____registry_0 = value;
		Il2CppCodeGenWriteBarrier((&____registry_0), value);
	}

	inline static int32_t get_offset_of__sceneContext_1() { return static_cast<int32_t>(offsetof(SceneContextRegistryAdderAndRemover_t5A47B219C754FDC6E48B2187C7F078C538EA3C88, ____sceneContext_1)); }
	inline SceneContext_tA69C18F583C410BB2DA86B5C1F86F27459DA8CA2 * get__sceneContext_1() const { return ____sceneContext_1; }
	inline SceneContext_tA69C18F583C410BB2DA86B5C1F86F27459DA8CA2 ** get_address_of__sceneContext_1() { return &____sceneContext_1; }
	inline void set__sceneContext_1(SceneContext_tA69C18F583C410BB2DA86B5C1F86F27459DA8CA2 * value)
	{
		____sceneContext_1 = value;
		Il2CppCodeGenWriteBarrier((&____sceneContext_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENECONTEXTREGISTRYADDERANDREMOVER_T5A47B219C754FDC6E48B2187C7F078C538EA3C88_H
#ifndef SIGNALBASE_T1A82E76F19A3098BB4AF7EDD41C29E6CAD3F95B3_H
#define SIGNALBASE_T1A82E76F19A3098BB4AF7EDD41C29E6CAD3F95B3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SignalBase
struct  SignalBase_t1A82E76F19A3098BB4AF7EDD41C29E6CAD3F95B3  : public RuntimeObject
{
public:
	// Zenject.SignalManager Zenject.SignalBase::_manager
	SignalManager_t38E64F258F79FF33F3FDBB9423271BEFF03D7673 * ____manager_0;
	// Zenject.BindingId Zenject.SignalBase::<SignalId>k__BackingField
	BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA * ___U3CSignalIdU3Ek__BackingField_1;
	// Zenject.SignalSettings Zenject.SignalBase::<Settings>k__BackingField
	SignalSettings_tD753AA3D16CEA36F97CDAEC5428C5217F1EB6540 * ___U3CSettingsU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of__manager_0() { return static_cast<int32_t>(offsetof(SignalBase_t1A82E76F19A3098BB4AF7EDD41C29E6CAD3F95B3, ____manager_0)); }
	inline SignalManager_t38E64F258F79FF33F3FDBB9423271BEFF03D7673 * get__manager_0() const { return ____manager_0; }
	inline SignalManager_t38E64F258F79FF33F3FDBB9423271BEFF03D7673 ** get_address_of__manager_0() { return &____manager_0; }
	inline void set__manager_0(SignalManager_t38E64F258F79FF33F3FDBB9423271BEFF03D7673 * value)
	{
		____manager_0 = value;
		Il2CppCodeGenWriteBarrier((&____manager_0), value);
	}

	inline static int32_t get_offset_of_U3CSignalIdU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(SignalBase_t1A82E76F19A3098BB4AF7EDD41C29E6CAD3F95B3, ___U3CSignalIdU3Ek__BackingField_1)); }
	inline BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA * get_U3CSignalIdU3Ek__BackingField_1() const { return ___U3CSignalIdU3Ek__BackingField_1; }
	inline BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA ** get_address_of_U3CSignalIdU3Ek__BackingField_1() { return &___U3CSignalIdU3Ek__BackingField_1; }
	inline void set_U3CSignalIdU3Ek__BackingField_1(BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA * value)
	{
		___U3CSignalIdU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSignalIdU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CSettingsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(SignalBase_t1A82E76F19A3098BB4AF7EDD41C29E6CAD3F95B3, ___U3CSettingsU3Ek__BackingField_2)); }
	inline SignalSettings_tD753AA3D16CEA36F97CDAEC5428C5217F1EB6540 * get_U3CSettingsU3Ek__BackingField_2() const { return ___U3CSettingsU3Ek__BackingField_2; }
	inline SignalSettings_tD753AA3D16CEA36F97CDAEC5428C5217F1EB6540 ** get_address_of_U3CSettingsU3Ek__BackingField_2() { return &___U3CSettingsU3Ek__BackingField_2; }
	inline void set_U3CSettingsU3Ek__BackingField_2(SignalSettings_tD753AA3D16CEA36F97CDAEC5428C5217F1EB6540 * value)
	{
		___U3CSettingsU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSettingsU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGNALBASE_T1A82E76F19A3098BB4AF7EDD41C29E6CAD3F95B3_H
#ifndef SIGNALEXTENSIONS_T33C6464932CFDFDA4BEB1F734024765C8A955F25_H
#define SIGNALEXTENSIONS_T33C6464932CFDFDA4BEB1F734024765C8A955F25_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SignalExtensions
struct  SignalExtensions_t33C6464932CFDFDA4BEB1F734024765C8A955F25  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGNALEXTENSIONS_T33C6464932CFDFDA4BEB1F734024765C8A955F25_H
#ifndef SIGNALHANDLERBASE_TE5F99D6B541F6C778B7647020EBB4CD5843D6841_H
#define SIGNALHANDLERBASE_TE5F99D6B541F6C778B7647020EBB4CD5843D6841_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SignalHandlerBase
struct  SignalHandlerBase_tE5F99D6B541F6C778B7647020EBB4CD5843D6841  : public RuntimeObject
{
public:
	// Zenject.SignalManager Zenject.SignalHandlerBase::_manager
	SignalManager_t38E64F258F79FF33F3FDBB9423271BEFF03D7673 * ____manager_0;
	// Zenject.BindingId Zenject.SignalHandlerBase::_signalId
	BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA * ____signalId_1;

public:
	inline static int32_t get_offset_of__manager_0() { return static_cast<int32_t>(offsetof(SignalHandlerBase_tE5F99D6B541F6C778B7647020EBB4CD5843D6841, ____manager_0)); }
	inline SignalManager_t38E64F258F79FF33F3FDBB9423271BEFF03D7673 * get__manager_0() const { return ____manager_0; }
	inline SignalManager_t38E64F258F79FF33F3FDBB9423271BEFF03D7673 ** get_address_of__manager_0() { return &____manager_0; }
	inline void set__manager_0(SignalManager_t38E64F258F79FF33F3FDBB9423271BEFF03D7673 * value)
	{
		____manager_0 = value;
		Il2CppCodeGenWriteBarrier((&____manager_0), value);
	}

	inline static int32_t get_offset_of__signalId_1() { return static_cast<int32_t>(offsetof(SignalHandlerBase_tE5F99D6B541F6C778B7647020EBB4CD5843D6841, ____signalId_1)); }
	inline BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA * get__signalId_1() const { return ____signalId_1; }
	inline BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA ** get_address_of__signalId_1() { return &____signalId_1; }
	inline void set__signalId_1(BindingId_tDE4E18829D06364806DC2F3F04F0574D68DF92BA * value)
	{
		____signalId_1 = value;
		Il2CppCodeGenWriteBarrier((&____signalId_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGNALHANDLERBASE_TE5F99D6B541F6C778B7647020EBB4CD5843D6841_H
#ifndef SIGNALHANDLERBINDER_T4AB70F7A7BD1596D46B88F834A400EBEDE185771_H
#define SIGNALHANDLERBINDER_T4AB70F7A7BD1596D46B88F834A400EBEDE185771_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SignalHandlerBinder
struct  SignalHandlerBinder_t4AB70F7A7BD1596D46B88F834A400EBEDE185771  : public RuntimeObject
{
public:
	// Zenject.BindFinalizerWrapper Zenject.SignalHandlerBinder::_finalizerWrapper
	BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 * ____finalizerWrapper_0;
	// System.Type Zenject.SignalHandlerBinder::_signalType
	Type_t * ____signalType_1;
	// Zenject.DiContainer Zenject.SignalHandlerBinder::_container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____container_2;
	// System.Object Zenject.SignalHandlerBinder::<Identifier>k__BackingField
	RuntimeObject * ___U3CIdentifierU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of__finalizerWrapper_0() { return static_cast<int32_t>(offsetof(SignalHandlerBinder_t4AB70F7A7BD1596D46B88F834A400EBEDE185771, ____finalizerWrapper_0)); }
	inline BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 * get__finalizerWrapper_0() const { return ____finalizerWrapper_0; }
	inline BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 ** get_address_of__finalizerWrapper_0() { return &____finalizerWrapper_0; }
	inline void set__finalizerWrapper_0(BindFinalizerWrapper_t043E4C78D018E6AF48A956B33FF3AEA55CDD86C8 * value)
	{
		____finalizerWrapper_0 = value;
		Il2CppCodeGenWriteBarrier((&____finalizerWrapper_0), value);
	}

	inline static int32_t get_offset_of__signalType_1() { return static_cast<int32_t>(offsetof(SignalHandlerBinder_t4AB70F7A7BD1596D46B88F834A400EBEDE185771, ____signalType_1)); }
	inline Type_t * get__signalType_1() const { return ____signalType_1; }
	inline Type_t ** get_address_of__signalType_1() { return &____signalType_1; }
	inline void set__signalType_1(Type_t * value)
	{
		____signalType_1 = value;
		Il2CppCodeGenWriteBarrier((&____signalType_1), value);
	}

	inline static int32_t get_offset_of__container_2() { return static_cast<int32_t>(offsetof(SignalHandlerBinder_t4AB70F7A7BD1596D46B88F834A400EBEDE185771, ____container_2)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__container_2() const { return ____container_2; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__container_2() { return &____container_2; }
	inline void set__container_2(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____container_2 = value;
		Il2CppCodeGenWriteBarrier((&____container_2), value);
	}

	inline static int32_t get_offset_of_U3CIdentifierU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(SignalHandlerBinder_t4AB70F7A7BD1596D46B88F834A400EBEDE185771, ___U3CIdentifierU3Ek__BackingField_3)); }
	inline RuntimeObject * get_U3CIdentifierU3Ek__BackingField_3() const { return ___U3CIdentifierU3Ek__BackingField_3; }
	inline RuntimeObject ** get_address_of_U3CIdentifierU3Ek__BackingField_3() { return &___U3CIdentifierU3Ek__BackingField_3; }
	inline void set_U3CIdentifierU3Ek__BackingField_3(RuntimeObject * value)
	{
		___U3CIdentifierU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CIdentifierU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGNALHANDLERBINDER_T4AB70F7A7BD1596D46B88F834A400EBEDE185771_H
#ifndef SIGNALMANAGER_T38E64F258F79FF33F3FDBB9423271BEFF03D7673_H
#define SIGNALMANAGER_T38E64F258F79FF33F3FDBB9423271BEFF03D7673_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SignalManager
struct  SignalManager_t38E64F258F79FF33F3FDBB9423271BEFF03D7673  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.ISignalHandler>> Zenject.SignalManager::_signalHandlers
	Dictionary_2_t86769489CE30B4DF4E100726F6DBAEB5426914F3 * ____signalHandlers_0;

public:
	inline static int32_t get_offset_of__signalHandlers_0() { return static_cast<int32_t>(offsetof(SignalManager_t38E64F258F79FF33F3FDBB9423271BEFF03D7673, ____signalHandlers_0)); }
	inline Dictionary_2_t86769489CE30B4DF4E100726F6DBAEB5426914F3 * get__signalHandlers_0() const { return ____signalHandlers_0; }
	inline Dictionary_2_t86769489CE30B4DF4E100726F6DBAEB5426914F3 ** get_address_of__signalHandlers_0() { return &____signalHandlers_0; }
	inline void set__signalHandlers_0(Dictionary_2_t86769489CE30B4DF4E100726F6DBAEB5426914F3 * value)
	{
		____signalHandlers_0 = value;
		Il2CppCodeGenWriteBarrier((&____signalHandlers_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGNALMANAGER_T38E64F258F79FF33F3FDBB9423271BEFF03D7673_H
#ifndef U3CU3EC_T772F9F5DDEA4C5A7BE65DE8A6F05DC9FE7D11513_H
#define U3CU3EC_T772F9F5DDEA4C5A7BE65DE8A6F05DC9FE7D11513_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SignalManager_<>c
struct  U3CU3Ec_t772F9F5DDEA4C5A7BE65DE8A6F05DC9FE7D11513  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t772F9F5DDEA4C5A7BE65DE8A6F05DC9FE7D11513_StaticFields
{
public:
	// Zenject.SignalManager_<>c Zenject.SignalManager_<>c::<>9
	U3CU3Ec_t772F9F5DDEA4C5A7BE65DE8A6F05DC9FE7D11513 * ___U3CU3E9_0;
	// System.Func`2<System.Collections.Generic.List`1<Zenject.ISignalHandler>,System.Collections.Generic.IEnumerable`1<Zenject.ISignalHandler>> Zenject.SignalManager_<>c::<>9__6_0
	Func_2_t0E839BE25BE5FB09C0BD94FE098AAFF87902125B * ___U3CU3E9__6_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t772F9F5DDEA4C5A7BE65DE8A6F05DC9FE7D11513_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t772F9F5DDEA4C5A7BE65DE8A6F05DC9FE7D11513 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t772F9F5DDEA4C5A7BE65DE8A6F05DC9FE7D11513 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t772F9F5DDEA4C5A7BE65DE8A6F05DC9FE7D11513 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__6_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t772F9F5DDEA4C5A7BE65DE8A6F05DC9FE7D11513_StaticFields, ___U3CU3E9__6_0_1)); }
	inline Func_2_t0E839BE25BE5FB09C0BD94FE098AAFF87902125B * get_U3CU3E9__6_0_1() const { return ___U3CU3E9__6_0_1; }
	inline Func_2_t0E839BE25BE5FB09C0BD94FE098AAFF87902125B ** get_address_of_U3CU3E9__6_0_1() { return &___U3CU3E9__6_0_1; }
	inline void set_U3CU3E9__6_0_1(Func_2_t0E839BE25BE5FB09C0BD94FE098AAFF87902125B * value)
	{
		___U3CU3E9__6_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__6_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T772F9F5DDEA4C5A7BE65DE8A6F05DC9FE7D11513_H
#ifndef SIGNALSETTINGS_TD753AA3D16CEA36F97CDAEC5428C5217F1EB6540_H
#define SIGNALSETTINGS_TD753AA3D16CEA36F97CDAEC5428C5217F1EB6540_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SignalSettings
struct  SignalSettings_tD753AA3D16CEA36F97CDAEC5428C5217F1EB6540  : public RuntimeObject
{
public:
	// System.Boolean Zenject.SignalSettings::RequiresHandler
	bool ___RequiresHandler_0;

public:
	inline static int32_t get_offset_of_RequiresHandler_0() { return static_cast<int32_t>(offsetof(SignalSettings_tD753AA3D16CEA36F97CDAEC5428C5217F1EB6540, ___RequiresHandler_0)); }
	inline bool get_RequiresHandler_0() const { return ___RequiresHandler_0; }
	inline bool* get_address_of_RequiresHandler_0() { return &___RequiresHandler_0; }
	inline void set_RequiresHandler_0(bool value)
	{
		___RequiresHandler_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGNALSETTINGS_TD753AA3D16CEA36F97CDAEC5428C5217F1EB6540_H
#ifndef TASKUPDATER_1_TF15E7A444C3B28FDD13C69DB1573920C371C8291_H
#define TASKUPDATER_1_TF15E7A444C3B28FDD13C69DB1573920C371C8291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.TaskUpdater`1<Zenject.IFixedTickable>
struct  TaskUpdater_1_tF15E7A444C3B28FDD13C69DB1573920C371C8291  : public RuntimeObject
{
public:
	// System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1_TaskInfo<TTask>> Zenject.TaskUpdater`1::_tasks
	LinkedList_1_tBBDF9B16D26EC4D4125BD3544EFCF2BB5B034AEC * ____tasks_0;
	// System.Collections.Generic.List`1<Zenject.TaskUpdater`1_TaskInfo<TTask>> Zenject.TaskUpdater`1::_queuedTasks
	List_1_t9F4BD1E9D1073E4238B05CCE07350F764C08B4A3 * ____queuedTasks_1;

public:
	inline static int32_t get_offset_of__tasks_0() { return static_cast<int32_t>(offsetof(TaskUpdater_1_tF15E7A444C3B28FDD13C69DB1573920C371C8291, ____tasks_0)); }
	inline LinkedList_1_tBBDF9B16D26EC4D4125BD3544EFCF2BB5B034AEC * get__tasks_0() const { return ____tasks_0; }
	inline LinkedList_1_tBBDF9B16D26EC4D4125BD3544EFCF2BB5B034AEC ** get_address_of__tasks_0() { return &____tasks_0; }
	inline void set__tasks_0(LinkedList_1_tBBDF9B16D26EC4D4125BD3544EFCF2BB5B034AEC * value)
	{
		____tasks_0 = value;
		Il2CppCodeGenWriteBarrier((&____tasks_0), value);
	}

	inline static int32_t get_offset_of__queuedTasks_1() { return static_cast<int32_t>(offsetof(TaskUpdater_1_tF15E7A444C3B28FDD13C69DB1573920C371C8291, ____queuedTasks_1)); }
	inline List_1_t9F4BD1E9D1073E4238B05CCE07350F764C08B4A3 * get__queuedTasks_1() const { return ____queuedTasks_1; }
	inline List_1_t9F4BD1E9D1073E4238B05CCE07350F764C08B4A3 ** get_address_of__queuedTasks_1() { return &____queuedTasks_1; }
	inline void set__queuedTasks_1(List_1_t9F4BD1E9D1073E4238B05CCE07350F764C08B4A3 * value)
	{
		____queuedTasks_1 = value;
		Il2CppCodeGenWriteBarrier((&____queuedTasks_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TASKUPDATER_1_TF15E7A444C3B28FDD13C69DB1573920C371C8291_H
#ifndef TASKUPDATER_1_TFC343732437855A35518A11195097D498C85B8B1_H
#define TASKUPDATER_1_TFC343732437855A35518A11195097D498C85B8B1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.TaskUpdater`1<Zenject.ILateTickable>
struct  TaskUpdater_1_tFC343732437855A35518A11195097D498C85B8B1  : public RuntimeObject
{
public:
	// System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1_TaskInfo<TTask>> Zenject.TaskUpdater`1::_tasks
	LinkedList_1_t58FC5DC068D268925ACCCB10D5000C44F34C14F7 * ____tasks_0;
	// System.Collections.Generic.List`1<Zenject.TaskUpdater`1_TaskInfo<TTask>> Zenject.TaskUpdater`1::_queuedTasks
	List_1_t6B84519EDCA0D36C2A2582D3956B2FBB018881DC * ____queuedTasks_1;

public:
	inline static int32_t get_offset_of__tasks_0() { return static_cast<int32_t>(offsetof(TaskUpdater_1_tFC343732437855A35518A11195097D498C85B8B1, ____tasks_0)); }
	inline LinkedList_1_t58FC5DC068D268925ACCCB10D5000C44F34C14F7 * get__tasks_0() const { return ____tasks_0; }
	inline LinkedList_1_t58FC5DC068D268925ACCCB10D5000C44F34C14F7 ** get_address_of__tasks_0() { return &____tasks_0; }
	inline void set__tasks_0(LinkedList_1_t58FC5DC068D268925ACCCB10D5000C44F34C14F7 * value)
	{
		____tasks_0 = value;
		Il2CppCodeGenWriteBarrier((&____tasks_0), value);
	}

	inline static int32_t get_offset_of__queuedTasks_1() { return static_cast<int32_t>(offsetof(TaskUpdater_1_tFC343732437855A35518A11195097D498C85B8B1, ____queuedTasks_1)); }
	inline List_1_t6B84519EDCA0D36C2A2582D3956B2FBB018881DC * get__queuedTasks_1() const { return ____queuedTasks_1; }
	inline List_1_t6B84519EDCA0D36C2A2582D3956B2FBB018881DC ** get_address_of__queuedTasks_1() { return &____queuedTasks_1; }
	inline void set__queuedTasks_1(List_1_t6B84519EDCA0D36C2A2582D3956B2FBB018881DC * value)
	{
		____queuedTasks_1 = value;
		Il2CppCodeGenWriteBarrier((&____queuedTasks_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TASKUPDATER_1_TFC343732437855A35518A11195097D498C85B8B1_H
#ifndef TASKUPDATER_1_T92F8BF304492103E44E9BD41A6D6AB797527FFDF_H
#define TASKUPDATER_1_T92F8BF304492103E44E9BD41A6D6AB797527FFDF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.TaskUpdater`1<Zenject.ITickable>
struct  TaskUpdater_1_t92F8BF304492103E44E9BD41A6D6AB797527FFDF  : public RuntimeObject
{
public:
	// System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1_TaskInfo<TTask>> Zenject.TaskUpdater`1::_tasks
	LinkedList_1_tFE981B1647A6CB01772C7ADE559050413D3C2A11 * ____tasks_0;
	// System.Collections.Generic.List`1<Zenject.TaskUpdater`1_TaskInfo<TTask>> Zenject.TaskUpdater`1::_queuedTasks
	List_1_tEC7EBEAB21D126318DE29351C73F8FDC2CDBB8EF * ____queuedTasks_1;

public:
	inline static int32_t get_offset_of__tasks_0() { return static_cast<int32_t>(offsetof(TaskUpdater_1_t92F8BF304492103E44E9BD41A6D6AB797527FFDF, ____tasks_0)); }
	inline LinkedList_1_tFE981B1647A6CB01772C7ADE559050413D3C2A11 * get__tasks_0() const { return ____tasks_0; }
	inline LinkedList_1_tFE981B1647A6CB01772C7ADE559050413D3C2A11 ** get_address_of__tasks_0() { return &____tasks_0; }
	inline void set__tasks_0(LinkedList_1_tFE981B1647A6CB01772C7ADE559050413D3C2A11 * value)
	{
		____tasks_0 = value;
		Il2CppCodeGenWriteBarrier((&____tasks_0), value);
	}

	inline static int32_t get_offset_of__queuedTasks_1() { return static_cast<int32_t>(offsetof(TaskUpdater_1_t92F8BF304492103E44E9BD41A6D6AB797527FFDF, ____queuedTasks_1)); }
	inline List_1_tEC7EBEAB21D126318DE29351C73F8FDC2CDBB8EF * get__queuedTasks_1() const { return ____queuedTasks_1; }
	inline List_1_tEC7EBEAB21D126318DE29351C73F8FDC2CDBB8EF ** get_address_of__queuedTasks_1() { return &____queuedTasks_1; }
	inline void set__queuedTasks_1(List_1_tEC7EBEAB21D126318DE29351C73F8FDC2CDBB8EF * value)
	{
		____queuedTasks_1 = value;
		Il2CppCodeGenWriteBarrier((&____queuedTasks_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TASKUPDATER_1_T92F8BF304492103E44E9BD41A6D6AB797527FFDF_H
#ifndef TICKABLEMANAGER_T9CDD6FA2E97FC2B4BD99A06A36D29DBE66C03598_H
#define TICKABLEMANAGER_T9CDD6FA2E97FC2B4BD99A06A36D29DBE66C03598_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.TickableManager
struct  TickableManager_t9CDD6FA2E97FC2B4BD99A06A36D29DBE66C03598  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Zenject.ITickable> Zenject.TickableManager::_tickables
	List_1_t6AC9EDC1F701BA128484C3672692B4A6025F149A * ____tickables_0;
	// System.Collections.Generic.List`1<Zenject.IFixedTickable> Zenject.TickableManager::_fixedTickables
	List_1_t38D179FF9DBA1621B955766571B9285B8C82D0C0 * ____fixedTickables_1;
	// System.Collections.Generic.List`1<Zenject.ILateTickable> Zenject.TickableManager::_lateTickables
	List_1_tA0FEBB86373E09790B3119D49BB132AF39CC8501 * ____lateTickables_2;
	// System.Collections.Generic.List`1<ModestTree.Util.ValuePair`2<System.Type,System.Int32>> Zenject.TickableManager::_priorities
	List_1_t1789D56162D6592672665E24594EEF62242C02AD * ____priorities_3;
	// System.Collections.Generic.List`1<ModestTree.Util.ValuePair`2<System.Type,System.Int32>> Zenject.TickableManager::_fixedPriorities
	List_1_t1789D56162D6592672665E24594EEF62242C02AD * ____fixedPriorities_4;
	// System.Collections.Generic.List`1<ModestTree.Util.ValuePair`2<System.Type,System.Int32>> Zenject.TickableManager::_latePriorities
	List_1_t1789D56162D6592672665E24594EEF62242C02AD * ____latePriorities_5;
	// Zenject.TickablesTaskUpdater Zenject.TickableManager::_updater
	TickablesTaskUpdater_tF277702A3B140691294521F1606DE293E0DE7897 * ____updater_6;
	// Zenject.FixedTickablesTaskUpdater Zenject.TickableManager::_fixedUpdater
	FixedTickablesTaskUpdater_t72C0FFEE087922D42CEBB55EE5243DC64D8B367A * ____fixedUpdater_7;
	// Zenject.LateTickablesTaskUpdater Zenject.TickableManager::_lateUpdater
	LateTickablesTaskUpdater_t66115F857C2D233BB36D6086F2FFC6D32580B9EB * ____lateUpdater_8;
	// System.Boolean Zenject.TickableManager::_isPaused
	bool ____isPaused_9;

public:
	inline static int32_t get_offset_of__tickables_0() { return static_cast<int32_t>(offsetof(TickableManager_t9CDD6FA2E97FC2B4BD99A06A36D29DBE66C03598, ____tickables_0)); }
	inline List_1_t6AC9EDC1F701BA128484C3672692B4A6025F149A * get__tickables_0() const { return ____tickables_0; }
	inline List_1_t6AC9EDC1F701BA128484C3672692B4A6025F149A ** get_address_of__tickables_0() { return &____tickables_0; }
	inline void set__tickables_0(List_1_t6AC9EDC1F701BA128484C3672692B4A6025F149A * value)
	{
		____tickables_0 = value;
		Il2CppCodeGenWriteBarrier((&____tickables_0), value);
	}

	inline static int32_t get_offset_of__fixedTickables_1() { return static_cast<int32_t>(offsetof(TickableManager_t9CDD6FA2E97FC2B4BD99A06A36D29DBE66C03598, ____fixedTickables_1)); }
	inline List_1_t38D179FF9DBA1621B955766571B9285B8C82D0C0 * get__fixedTickables_1() const { return ____fixedTickables_1; }
	inline List_1_t38D179FF9DBA1621B955766571B9285B8C82D0C0 ** get_address_of__fixedTickables_1() { return &____fixedTickables_1; }
	inline void set__fixedTickables_1(List_1_t38D179FF9DBA1621B955766571B9285B8C82D0C0 * value)
	{
		____fixedTickables_1 = value;
		Il2CppCodeGenWriteBarrier((&____fixedTickables_1), value);
	}

	inline static int32_t get_offset_of__lateTickables_2() { return static_cast<int32_t>(offsetof(TickableManager_t9CDD6FA2E97FC2B4BD99A06A36D29DBE66C03598, ____lateTickables_2)); }
	inline List_1_tA0FEBB86373E09790B3119D49BB132AF39CC8501 * get__lateTickables_2() const { return ____lateTickables_2; }
	inline List_1_tA0FEBB86373E09790B3119D49BB132AF39CC8501 ** get_address_of__lateTickables_2() { return &____lateTickables_2; }
	inline void set__lateTickables_2(List_1_tA0FEBB86373E09790B3119D49BB132AF39CC8501 * value)
	{
		____lateTickables_2 = value;
		Il2CppCodeGenWriteBarrier((&____lateTickables_2), value);
	}

	inline static int32_t get_offset_of__priorities_3() { return static_cast<int32_t>(offsetof(TickableManager_t9CDD6FA2E97FC2B4BD99A06A36D29DBE66C03598, ____priorities_3)); }
	inline List_1_t1789D56162D6592672665E24594EEF62242C02AD * get__priorities_3() const { return ____priorities_3; }
	inline List_1_t1789D56162D6592672665E24594EEF62242C02AD ** get_address_of__priorities_3() { return &____priorities_3; }
	inline void set__priorities_3(List_1_t1789D56162D6592672665E24594EEF62242C02AD * value)
	{
		____priorities_3 = value;
		Il2CppCodeGenWriteBarrier((&____priorities_3), value);
	}

	inline static int32_t get_offset_of__fixedPriorities_4() { return static_cast<int32_t>(offsetof(TickableManager_t9CDD6FA2E97FC2B4BD99A06A36D29DBE66C03598, ____fixedPriorities_4)); }
	inline List_1_t1789D56162D6592672665E24594EEF62242C02AD * get__fixedPriorities_4() const { return ____fixedPriorities_4; }
	inline List_1_t1789D56162D6592672665E24594EEF62242C02AD ** get_address_of__fixedPriorities_4() { return &____fixedPriorities_4; }
	inline void set__fixedPriorities_4(List_1_t1789D56162D6592672665E24594EEF62242C02AD * value)
	{
		____fixedPriorities_4 = value;
		Il2CppCodeGenWriteBarrier((&____fixedPriorities_4), value);
	}

	inline static int32_t get_offset_of__latePriorities_5() { return static_cast<int32_t>(offsetof(TickableManager_t9CDD6FA2E97FC2B4BD99A06A36D29DBE66C03598, ____latePriorities_5)); }
	inline List_1_t1789D56162D6592672665E24594EEF62242C02AD * get__latePriorities_5() const { return ____latePriorities_5; }
	inline List_1_t1789D56162D6592672665E24594EEF62242C02AD ** get_address_of__latePriorities_5() { return &____latePriorities_5; }
	inline void set__latePriorities_5(List_1_t1789D56162D6592672665E24594EEF62242C02AD * value)
	{
		____latePriorities_5 = value;
		Il2CppCodeGenWriteBarrier((&____latePriorities_5), value);
	}

	inline static int32_t get_offset_of__updater_6() { return static_cast<int32_t>(offsetof(TickableManager_t9CDD6FA2E97FC2B4BD99A06A36D29DBE66C03598, ____updater_6)); }
	inline TickablesTaskUpdater_tF277702A3B140691294521F1606DE293E0DE7897 * get__updater_6() const { return ____updater_6; }
	inline TickablesTaskUpdater_tF277702A3B140691294521F1606DE293E0DE7897 ** get_address_of__updater_6() { return &____updater_6; }
	inline void set__updater_6(TickablesTaskUpdater_tF277702A3B140691294521F1606DE293E0DE7897 * value)
	{
		____updater_6 = value;
		Il2CppCodeGenWriteBarrier((&____updater_6), value);
	}

	inline static int32_t get_offset_of__fixedUpdater_7() { return static_cast<int32_t>(offsetof(TickableManager_t9CDD6FA2E97FC2B4BD99A06A36D29DBE66C03598, ____fixedUpdater_7)); }
	inline FixedTickablesTaskUpdater_t72C0FFEE087922D42CEBB55EE5243DC64D8B367A * get__fixedUpdater_7() const { return ____fixedUpdater_7; }
	inline FixedTickablesTaskUpdater_t72C0FFEE087922D42CEBB55EE5243DC64D8B367A ** get_address_of__fixedUpdater_7() { return &____fixedUpdater_7; }
	inline void set__fixedUpdater_7(FixedTickablesTaskUpdater_t72C0FFEE087922D42CEBB55EE5243DC64D8B367A * value)
	{
		____fixedUpdater_7 = value;
		Il2CppCodeGenWriteBarrier((&____fixedUpdater_7), value);
	}

	inline static int32_t get_offset_of__lateUpdater_8() { return static_cast<int32_t>(offsetof(TickableManager_t9CDD6FA2E97FC2B4BD99A06A36D29DBE66C03598, ____lateUpdater_8)); }
	inline LateTickablesTaskUpdater_t66115F857C2D233BB36D6086F2FFC6D32580B9EB * get__lateUpdater_8() const { return ____lateUpdater_8; }
	inline LateTickablesTaskUpdater_t66115F857C2D233BB36D6086F2FFC6D32580B9EB ** get_address_of__lateUpdater_8() { return &____lateUpdater_8; }
	inline void set__lateUpdater_8(LateTickablesTaskUpdater_t66115F857C2D233BB36D6086F2FFC6D32580B9EB * value)
	{
		____lateUpdater_8 = value;
		Il2CppCodeGenWriteBarrier((&____lateUpdater_8), value);
	}

	inline static int32_t get_offset_of__isPaused_9() { return static_cast<int32_t>(offsetof(TickableManager_t9CDD6FA2E97FC2B4BD99A06A36D29DBE66C03598, ____isPaused_9)); }
	inline bool get__isPaused_9() const { return ____isPaused_9; }
	inline bool* get_address_of__isPaused_9() { return &____isPaused_9; }
	inline void set__isPaused_9(bool value)
	{
		____isPaused_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TICKABLEMANAGER_T9CDD6FA2E97FC2B4BD99A06A36D29DBE66C03598_H
#ifndef U3CU3EC_T38677C43FFD46FE34008A422035537F6B98BA7E2_H
#define U3CU3EC_T38677C43FFD46FE34008A422035537F6B98BA7E2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.TickableManager_<>c
struct  U3CU3Ec_t38677C43FFD46FE34008A422035537F6B98BA7E2  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t38677C43FFD46FE34008A422035537F6B98BA7E2_StaticFields
{
public:
	// Zenject.TickableManager_<>c Zenject.TickableManager_<>c::<>9
	U3CU3Ec_t38677C43FFD46FE34008A422035537F6B98BA7E2 * ___U3CU3E9_0;
	// System.Func`2<ModestTree.Util.ValuePair`2<System.Type,System.Int32>,System.Type> Zenject.TickableManager_<>c::<>9__17_0
	Func_2_t2BAC6B7180FBCB00D561A8DE37970649E8F9228C * ___U3CU3E9__17_0_1;
	// System.Func`2<ModestTree.Util.ValuePair`2<System.Type,System.Int32>,System.Int32> Zenject.TickableManager_<>c::<>9__17_2
	Func_2_tB33DB0601EFB82905DF1AE5730828B918DC19DBD * ___U3CU3E9__17_2_2;
	// System.Func`2<ModestTree.Util.ValuePair`2<System.Type,System.Int32>,System.Type> Zenject.TickableManager_<>c::<>9__18_0
	Func_2_t2BAC6B7180FBCB00D561A8DE37970649E8F9228C * ___U3CU3E9__18_0_3;
	// System.Func`2<ModestTree.Util.ValuePair`2<System.Type,System.Int32>,System.Int32> Zenject.TickableManager_<>c::<>9__18_2
	Func_2_tB33DB0601EFB82905DF1AE5730828B918DC19DBD * ___U3CU3E9__18_2_4;
	// System.Func`2<ModestTree.Util.ValuePair`2<System.Type,System.Int32>,System.Type> Zenject.TickableManager_<>c::<>9__19_0
	Func_2_t2BAC6B7180FBCB00D561A8DE37970649E8F9228C * ___U3CU3E9__19_0_5;
	// System.Func`2<ModestTree.Util.ValuePair`2<System.Type,System.Int32>,System.Int32> Zenject.TickableManager_<>c::<>9__19_2
	Func_2_tB33DB0601EFB82905DF1AE5730828B918DC19DBD * ___U3CU3E9__19_2_6;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t38677C43FFD46FE34008A422035537F6B98BA7E2_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t38677C43FFD46FE34008A422035537F6B98BA7E2 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t38677C43FFD46FE34008A422035537F6B98BA7E2 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t38677C43FFD46FE34008A422035537F6B98BA7E2 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__17_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t38677C43FFD46FE34008A422035537F6B98BA7E2_StaticFields, ___U3CU3E9__17_0_1)); }
	inline Func_2_t2BAC6B7180FBCB00D561A8DE37970649E8F9228C * get_U3CU3E9__17_0_1() const { return ___U3CU3E9__17_0_1; }
	inline Func_2_t2BAC6B7180FBCB00D561A8DE37970649E8F9228C ** get_address_of_U3CU3E9__17_0_1() { return &___U3CU3E9__17_0_1; }
	inline void set_U3CU3E9__17_0_1(Func_2_t2BAC6B7180FBCB00D561A8DE37970649E8F9228C * value)
	{
		___U3CU3E9__17_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__17_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__17_2_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t38677C43FFD46FE34008A422035537F6B98BA7E2_StaticFields, ___U3CU3E9__17_2_2)); }
	inline Func_2_tB33DB0601EFB82905DF1AE5730828B918DC19DBD * get_U3CU3E9__17_2_2() const { return ___U3CU3E9__17_2_2; }
	inline Func_2_tB33DB0601EFB82905DF1AE5730828B918DC19DBD ** get_address_of_U3CU3E9__17_2_2() { return &___U3CU3E9__17_2_2; }
	inline void set_U3CU3E9__17_2_2(Func_2_tB33DB0601EFB82905DF1AE5730828B918DC19DBD * value)
	{
		___U3CU3E9__17_2_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__17_2_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__18_0_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t38677C43FFD46FE34008A422035537F6B98BA7E2_StaticFields, ___U3CU3E9__18_0_3)); }
	inline Func_2_t2BAC6B7180FBCB00D561A8DE37970649E8F9228C * get_U3CU3E9__18_0_3() const { return ___U3CU3E9__18_0_3; }
	inline Func_2_t2BAC6B7180FBCB00D561A8DE37970649E8F9228C ** get_address_of_U3CU3E9__18_0_3() { return &___U3CU3E9__18_0_3; }
	inline void set_U3CU3E9__18_0_3(Func_2_t2BAC6B7180FBCB00D561A8DE37970649E8F9228C * value)
	{
		___U3CU3E9__18_0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__18_0_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__18_2_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_t38677C43FFD46FE34008A422035537F6B98BA7E2_StaticFields, ___U3CU3E9__18_2_4)); }
	inline Func_2_tB33DB0601EFB82905DF1AE5730828B918DC19DBD * get_U3CU3E9__18_2_4() const { return ___U3CU3E9__18_2_4; }
	inline Func_2_tB33DB0601EFB82905DF1AE5730828B918DC19DBD ** get_address_of_U3CU3E9__18_2_4() { return &___U3CU3E9__18_2_4; }
	inline void set_U3CU3E9__18_2_4(Func_2_tB33DB0601EFB82905DF1AE5730828B918DC19DBD * value)
	{
		___U3CU3E9__18_2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__18_2_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__19_0_5() { return static_cast<int32_t>(offsetof(U3CU3Ec_t38677C43FFD46FE34008A422035537F6B98BA7E2_StaticFields, ___U3CU3E9__19_0_5)); }
	inline Func_2_t2BAC6B7180FBCB00D561A8DE37970649E8F9228C * get_U3CU3E9__19_0_5() const { return ___U3CU3E9__19_0_5; }
	inline Func_2_t2BAC6B7180FBCB00D561A8DE37970649E8F9228C ** get_address_of_U3CU3E9__19_0_5() { return &___U3CU3E9__19_0_5; }
	inline void set_U3CU3E9__19_0_5(Func_2_t2BAC6B7180FBCB00D561A8DE37970649E8F9228C * value)
	{
		___U3CU3E9__19_0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__19_0_5), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__19_2_6() { return static_cast<int32_t>(offsetof(U3CU3Ec_t38677C43FFD46FE34008A422035537F6B98BA7E2_StaticFields, ___U3CU3E9__19_2_6)); }
	inline Func_2_tB33DB0601EFB82905DF1AE5730828B918DC19DBD * get_U3CU3E9__19_2_6() const { return ___U3CU3E9__19_2_6; }
	inline Func_2_tB33DB0601EFB82905DF1AE5730828B918DC19DBD ** get_address_of_U3CU3E9__19_2_6() { return &___U3CU3E9__19_2_6; }
	inline void set_U3CU3E9__19_2_6(Func_2_tB33DB0601EFB82905DF1AE5730828B918DC19DBD * value)
	{
		___U3CU3E9__19_2_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__19_2_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T38677C43FFD46FE34008A422035537F6B98BA7E2_H
#ifndef U3CU3EC__DISPLAYCLASS17_0_T75F7AEF263B69816A7B30D234F527DA4763D7215_H
#define U3CU3EC__DISPLAYCLASS17_0_T75F7AEF263B69816A7B30D234F527DA4763D7215_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.TickableManager_<>c__DisplayClass17_0
struct  U3CU3Ec__DisplayClass17_0_t75F7AEF263B69816A7B30D234F527DA4763D7215  : public RuntimeObject
{
public:
	// Zenject.IFixedTickable Zenject.TickableManager_<>c__DisplayClass17_0::tickable
	RuntimeObject* ___tickable_0;

public:
	inline static int32_t get_offset_of_tickable_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass17_0_t75F7AEF263B69816A7B30D234F527DA4763D7215, ___tickable_0)); }
	inline RuntimeObject* get_tickable_0() const { return ___tickable_0; }
	inline RuntimeObject** get_address_of_tickable_0() { return &___tickable_0; }
	inline void set_tickable_0(RuntimeObject* value)
	{
		___tickable_0 = value;
		Il2CppCodeGenWriteBarrier((&___tickable_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS17_0_T75F7AEF263B69816A7B30D234F527DA4763D7215_H
#ifndef U3CU3EC__DISPLAYCLASS18_0_TE43DA7214898BE45D3DD3B4EBCAE95009452BFD4_H
#define U3CU3EC__DISPLAYCLASS18_0_TE43DA7214898BE45D3DD3B4EBCAE95009452BFD4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.TickableManager_<>c__DisplayClass18_0
struct  U3CU3Ec__DisplayClass18_0_tE43DA7214898BE45D3DD3B4EBCAE95009452BFD4  : public RuntimeObject
{
public:
	// Zenject.ITickable Zenject.TickableManager_<>c__DisplayClass18_0::tickable
	RuntimeObject* ___tickable_0;

public:
	inline static int32_t get_offset_of_tickable_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass18_0_tE43DA7214898BE45D3DD3B4EBCAE95009452BFD4, ___tickable_0)); }
	inline RuntimeObject* get_tickable_0() const { return ___tickable_0; }
	inline RuntimeObject** get_address_of_tickable_0() { return &___tickable_0; }
	inline void set_tickable_0(RuntimeObject* value)
	{
		___tickable_0 = value;
		Il2CppCodeGenWriteBarrier((&___tickable_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS18_0_TE43DA7214898BE45D3DD3B4EBCAE95009452BFD4_H
#ifndef U3CU3EC__DISPLAYCLASS19_0_T6AC9BFB6C0E5A0E188BFF0D6A3755A31E61FF188_H
#define U3CU3EC__DISPLAYCLASS19_0_T6AC9BFB6C0E5A0E188BFF0D6A3755A31E61FF188_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.TickableManager_<>c__DisplayClass19_0
struct  U3CU3Ec__DisplayClass19_0_t6AC9BFB6C0E5A0E188BFF0D6A3755A31E61FF188  : public RuntimeObject
{
public:
	// Zenject.ILateTickable Zenject.TickableManager_<>c__DisplayClass19_0::tickable
	RuntimeObject* ___tickable_0;

public:
	inline static int32_t get_offset_of_tickable_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass19_0_t6AC9BFB6C0E5A0E188BFF0D6A3755A31E61FF188, ___tickable_0)); }
	inline RuntimeObject* get_tickable_0() const { return ___tickable_0; }
	inline RuntimeObject** get_address_of_tickable_0() { return &___tickable_0; }
	inline void set_tickable_0(RuntimeObject* value)
	{
		___tickable_0 = value;
		Il2CppCodeGenWriteBarrier((&___tickable_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS19_0_T6AC9BFB6C0E5A0E188BFF0D6A3755A31E61FF188_H
#ifndef TYPEANALYZER_T19EB8063B9BA23AD8FAAB2AF5228A8D8D086F722_H
#define TYPEANALYZER_T19EB8063B9BA23AD8FAAB2AF5228A8D8D086F722_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.TypeAnalyzer
struct  TypeAnalyzer_t19EB8063B9BA23AD8FAAB2AF5228A8D8D086F722  : public RuntimeObject
{
public:

public:
};

struct TypeAnalyzer_t19EB8063B9BA23AD8FAAB2AF5228A8D8D086F722_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Type,Zenject.ZenjectTypeInfo> Zenject.TypeAnalyzer::_typeInfo
	Dictionary_2_t6BA2F0666D38DF096F45B41AB67474768713CE58 * ____typeInfo_0;

public:
	inline static int32_t get_offset_of__typeInfo_0() { return static_cast<int32_t>(offsetof(TypeAnalyzer_t19EB8063B9BA23AD8FAAB2AF5228A8D8D086F722_StaticFields, ____typeInfo_0)); }
	inline Dictionary_2_t6BA2F0666D38DF096F45B41AB67474768713CE58 * get__typeInfo_0() const { return ____typeInfo_0; }
	inline Dictionary_2_t6BA2F0666D38DF096F45B41AB67474768713CE58 ** get_address_of__typeInfo_0() { return &____typeInfo_0; }
	inline void set__typeInfo_0(Dictionary_2_t6BA2F0666D38DF096F45B41AB67474768713CE58 * value)
	{
		____typeInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&____typeInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEANALYZER_T19EB8063B9BA23AD8FAAB2AF5228A8D8D086F722_H
#ifndef PRESERVEATTRIBUTE_TF43F26FBD37217D5D84FA64A21C86AB4DCE29CC6_H
#define PRESERVEATTRIBUTE_TF43F26FBD37217D5D84FA64A21C86AB4DCE29CC6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModestTree.Util.PreserveAttribute
struct  PreserveAttribute_tF43F26FBD37217D5D84FA64A21C86AB4DCE29CC6  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRESERVEATTRIBUTE_TF43F26FBD37217D5D84FA64A21C86AB4DCE29CC6_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef COPYNONLAZYBINDER_TC55396C09D2CF16C40164CC4E24D58AC45632D11_H
#define COPYNONLAZYBINDER_TC55396C09D2CF16C40164CC4E24D58AC45632D11_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.CopyNonLazyBinder
struct  CopyNonLazyBinder_tC55396C09D2CF16C40164CC4E24D58AC45632D11  : public NonLazyBinder_tA9FB0768A2F39559FBDDCAFBF940AAD240B7C43C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COPYNONLAZYBINDER_TC55396C09D2CF16C40164CC4E24D58AC45632D11_H
#ifndef FIXEDTICKABLESTASKUPDATER_T72C0FFEE087922D42CEBB55EE5243DC64D8B367A_H
#define FIXEDTICKABLESTASKUPDATER_T72C0FFEE087922D42CEBB55EE5243DC64D8B367A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.FixedTickablesTaskUpdater
struct  FixedTickablesTaskUpdater_t72C0FFEE087922D42CEBB55EE5243DC64D8B367A  : public TaskUpdater_1_tF15E7A444C3B28FDD13C69DB1573920C371C8291
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIXEDTICKABLESTASKUPDATER_T72C0FFEE087922D42CEBB55EE5243DC64D8B367A_H
#ifndef INSTALLER_1_TA6525573951F8C34D981A66C4B3891E4358E2520_H
#define INSTALLER_1_TA6525573951F8C34D981A66C4B3891E4358E2520_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.Installer`1<Zenject.ActionInstaller>
struct  Installer_1_tA6525573951F8C34D981A66C4B3891E4358E2520  : public InstallerBase_t4F464ACE8FEB8CEEE7B4352A271F09894C28AD79
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTALLER_1_TA6525573951F8C34D981A66C4B3891E4358E2520_H
#ifndef INSTALLER_2_TF91F88932F45B8EFBDA9E6E848C6C4C2201296C5_H
#define INSTALLER_2_TF91F88932F45B8EFBDA9E6E848C6C4C2201296C5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.Installer`2<System.Collections.Generic.List`1<System.Type>,Zenject.ExecutionOrderInstaller>
struct  Installer_2_tF91F88932F45B8EFBDA9E6E848C6C4C2201296C5  : public InstallerBase_t4F464ACE8FEB8CEEE7B4352A271F09894C28AD79
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTALLER_2_TF91F88932F45B8EFBDA9E6E848C6C4C2201296C5_H
#ifndef LATETICKABLESTASKUPDATER_T66115F857C2D233BB36D6086F2FFC6D32580B9EB_H
#define LATETICKABLESTASKUPDATER_T66115F857C2D233BB36D6086F2FFC6D32580B9EB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.LateTickablesTaskUpdater
struct  LateTickablesTaskUpdater_t66115F857C2D233BB36D6086F2FFC6D32580B9EB  : public TaskUpdater_1_tFC343732437855A35518A11195097D498C85B8B1
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LATETICKABLESTASKUPDATER_T66115F857C2D233BB36D6086F2FFC6D32580B9EB_H
#ifndef SIGNALHANDLERBINDERWITHID_TF2F486B2BBE4565DB9C112C9D5755BDCFE7BCD27_H
#define SIGNALHANDLERBINDERWITHID_TF2F486B2BBE4565DB9C112C9D5755BDCFE7BCD27_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SignalHandlerBinderWithId
struct  SignalHandlerBinderWithId_tF2F486B2BBE4565DB9C112C9D5755BDCFE7BCD27  : public SignalHandlerBinder_t4AB70F7A7BD1596D46B88F834A400EBEDE185771
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGNALHANDLERBINDERWITHID_TF2F486B2BBE4565DB9C112C9D5755BDCFE7BCD27_H
#ifndef STATICMETHODSIGNALHANDLER_TD43BEE8CBD1A5149095864A157D07492C1090485_H
#define STATICMETHODSIGNALHANDLER_TD43BEE8CBD1A5149095864A157D07492C1090485_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.StaticMethodSignalHandler
struct  StaticMethodSignalHandler_tD43BEE8CBD1A5149095864A157D07492C1090485  : public SignalHandlerBase_tE5F99D6B541F6C778B7647020EBB4CD5843D6841
{
public:
	// System.Action Zenject.StaticMethodSignalHandler::_method
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ____method_2;

public:
	inline static int32_t get_offset_of__method_2() { return static_cast<int32_t>(offsetof(StaticMethodSignalHandler_tD43BEE8CBD1A5149095864A157D07492C1090485, ____method_2)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get__method_2() const { return ____method_2; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of__method_2() { return &____method_2; }
	inline void set__method_2(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		____method_2 = value;
		Il2CppCodeGenWriteBarrier((&____method_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATICMETHODSIGNALHANDLER_TD43BEE8CBD1A5149095864A157D07492C1090485_H
#ifndef TICKABLESTASKUPDATER_TF277702A3B140691294521F1606DE293E0DE7897_H
#define TICKABLESTASKUPDATER_TF277702A3B140691294521F1606DE293E0DE7897_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.TickablesTaskUpdater
struct  TickablesTaskUpdater_tF277702A3B140691294521F1606DE293E0DE7897  : public TaskUpdater_1_t92F8BF304492103E44E9BD41A6D6AB797527FFDF
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TICKABLESTASKUPDATER_TF277702A3B140691294521F1606DE293E0DE7897_H
#ifndef ZENJECTALLOWDURINGVALIDATIONATTRIBUTE_T5894E52430C1843BFB8A7F5AF2DEA5583745D3C6_H
#define ZENJECTALLOWDURINGVALIDATIONATTRIBUTE_T5894E52430C1843BFB8A7F5AF2DEA5583745D3C6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ZenjectAllowDuringValidationAttribute
struct  ZenjectAllowDuringValidationAttribute_t5894E52430C1843BFB8A7F5AF2DEA5583745D3C6  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZENJECTALLOWDURINGVALIDATIONATTRIBUTE_T5894E52430C1843BFB8A7F5AF2DEA5583745D3C6_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef ACTIONINSTALLER_T01044D07A248D88566B1F14C77EEB1F51235C5E8_H
#define ACTIONINSTALLER_T01044D07A248D88566B1F14C77EEB1F51235C5E8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ActionInstaller
struct  ActionInstaller_t01044D07A248D88566B1F14C77EEB1F51235C5E8  : public Installer_1_tA6525573951F8C34D981A66C4B3891E4358E2520
{
public:
	// System.Action`1<Zenject.DiContainer> Zenject.ActionInstaller::_installMethod
	Action_1_t47FB75EB7E275E7EC20C1ADC32AC936BD0BD4E84 * ____installMethod_1;

public:
	inline static int32_t get_offset_of__installMethod_1() { return static_cast<int32_t>(offsetof(ActionInstaller_t01044D07A248D88566B1F14C77EEB1F51235C5E8, ____installMethod_1)); }
	inline Action_1_t47FB75EB7E275E7EC20C1ADC32AC936BD0BD4E84 * get__installMethod_1() const { return ____installMethod_1; }
	inline Action_1_t47FB75EB7E275E7EC20C1ADC32AC936BD0BD4E84 ** get_address_of__installMethod_1() { return &____installMethod_1; }
	inline void set__installMethod_1(Action_1_t47FB75EB7E275E7EC20C1ADC32AC936BD0BD4E84 * value)
	{
		____installMethod_1 = value;
		Il2CppCodeGenWriteBarrier((&____installMethod_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIONINSTALLER_T01044D07A248D88566B1F14C77EEB1F51235C5E8_H
#ifndef CONDITIONCOPYNONLAZYBINDER_TF7F37C5F1508AAB283151819E412C853A8D94B9B_H
#define CONDITIONCOPYNONLAZYBINDER_TF7F37C5F1508AAB283151819E412C853A8D94B9B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ConditionCopyNonLazyBinder
struct  ConditionCopyNonLazyBinder_tF7F37C5F1508AAB283151819E412C853A8D94B9B  : public CopyNonLazyBinder_tC55396C09D2CF16C40164CC4E24D58AC45632D11
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONDITIONCOPYNONLAZYBINDER_TF7F37C5F1508AAB283151819E412C853A8D94B9B_H
#ifndef EXECUTIONORDERINSTALLER_T8AA9E0919417ED06C9813B63A95849558AA393C5_H
#define EXECUTIONORDERINSTALLER_T8AA9E0919417ED06C9813B63A95849558AA393C5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ExecutionOrderInstaller
struct  ExecutionOrderInstaller_t8AA9E0919417ED06C9813B63A95849558AA393C5  : public Installer_2_tF91F88932F45B8EFBDA9E6E848C6C4C2201296C5
{
public:
	// System.Collections.Generic.List`1<System.Type> Zenject.ExecutionOrderInstaller::_typeOrder
	List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0 * ____typeOrder_1;

public:
	inline static int32_t get_offset_of__typeOrder_1() { return static_cast<int32_t>(offsetof(ExecutionOrderInstaller_t8AA9E0919417ED06C9813B63A95849558AA393C5, ____typeOrder_1)); }
	inline List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0 * get__typeOrder_1() const { return ____typeOrder_1; }
	inline List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0 ** get_address_of__typeOrder_1() { return &____typeOrder_1; }
	inline void set__typeOrder_1(List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0 * value)
	{
		____typeOrder_1 = value;
		Il2CppCodeGenWriteBarrier((&____typeOrder_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXECUTIONORDERINSTALLER_T8AA9E0919417ED06C9813B63A95849558AA393C5_H
#ifndef INJECTSOURCES_T643ECF8A27899CEE7D40760762395F6C3E359049_H
#define INJECTSOURCES_T643ECF8A27899CEE7D40760762395F6C3E359049_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.InjectSources
struct  InjectSources_t643ECF8A27899CEE7D40760762395F6C3E359049 
{
public:
	// System.Int32 Zenject.InjectSources::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InjectSources_t643ECF8A27899CEE7D40760762395F6C3E359049, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INJECTSOURCES_T643ECF8A27899CEE7D40760762395F6C3E359049_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef INJECTATTRIBUTEBASE_TB713A40D8E7E8BDC6CD1DDF1C13D71BFE3D6C8DC_H
#define INJECTATTRIBUTEBASE_TB713A40D8E7E8BDC6CD1DDF1C13D71BFE3D6C8DC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.InjectAttributeBase
struct  InjectAttributeBase_tB713A40D8E7E8BDC6CD1DDF1C13D71BFE3D6C8DC  : public PreserveAttribute_tF43F26FBD37217D5D84FA64A21C86AB4DCE29CC6
{
public:
	// System.Boolean Zenject.InjectAttributeBase::<Optional>k__BackingField
	bool ___U3COptionalU3Ek__BackingField_0;
	// System.Object Zenject.InjectAttributeBase::<Id>k__BackingField
	RuntimeObject * ___U3CIdU3Ek__BackingField_1;
	// Zenject.InjectSources Zenject.InjectAttributeBase::<Source>k__BackingField
	int32_t ___U3CSourceU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3COptionalU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(InjectAttributeBase_tB713A40D8E7E8BDC6CD1DDF1C13D71BFE3D6C8DC, ___U3COptionalU3Ek__BackingField_0)); }
	inline bool get_U3COptionalU3Ek__BackingField_0() const { return ___U3COptionalU3Ek__BackingField_0; }
	inline bool* get_address_of_U3COptionalU3Ek__BackingField_0() { return &___U3COptionalU3Ek__BackingField_0; }
	inline void set_U3COptionalU3Ek__BackingField_0(bool value)
	{
		___U3COptionalU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CIdU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(InjectAttributeBase_tB713A40D8E7E8BDC6CD1DDF1C13D71BFE3D6C8DC, ___U3CIdU3Ek__BackingField_1)); }
	inline RuntimeObject * get_U3CIdU3Ek__BackingField_1() const { return ___U3CIdU3Ek__BackingField_1; }
	inline RuntimeObject ** get_address_of_U3CIdU3Ek__BackingField_1() { return &___U3CIdU3Ek__BackingField_1; }
	inline void set_U3CIdU3Ek__BackingField_1(RuntimeObject * value)
	{
		___U3CIdU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CIdU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CSourceU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(InjectAttributeBase_tB713A40D8E7E8BDC6CD1DDF1C13D71BFE3D6C8DC, ___U3CSourceU3Ek__BackingField_2)); }
	inline int32_t get_U3CSourceU3Ek__BackingField_2() const { return ___U3CSourceU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CSourceU3Ek__BackingField_2() { return &___U3CSourceU3Ek__BackingField_2; }
	inline void set_U3CSourceU3Ek__BackingField_2(int32_t value)
	{
		___U3CSourceU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INJECTATTRIBUTEBASE_TB713A40D8E7E8BDC6CD1DDF1C13D71BFE3D6C8DC_H
#ifndef SIGNALBINDER_TAF04C55124D4BDEFC6038E643AD06EFA454E3584_H
#define SIGNALBINDER_TAF04C55124D4BDEFC6038E643AD06EFA454E3584_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SignalBinder
struct  SignalBinder_tAF04C55124D4BDEFC6038E643AD06EFA454E3584  : public ConditionCopyNonLazyBinder_tF7F37C5F1508AAB283151819E412C853A8D94B9B
{
public:
	// Zenject.SignalSettings Zenject.SignalBinder::_signalSettings
	SignalSettings_tD753AA3D16CEA36F97CDAEC5428C5217F1EB6540 * ____signalSettings_1;

public:
	inline static int32_t get_offset_of__signalSettings_1() { return static_cast<int32_t>(offsetof(SignalBinder_tAF04C55124D4BDEFC6038E643AD06EFA454E3584, ____signalSettings_1)); }
	inline SignalSettings_tD753AA3D16CEA36F97CDAEC5428C5217F1EB6540 * get__signalSettings_1() const { return ____signalSettings_1; }
	inline SignalSettings_tD753AA3D16CEA36F97CDAEC5428C5217F1EB6540 ** get_address_of__signalSettings_1() { return &____signalSettings_1; }
	inline void set__signalSettings_1(SignalSettings_tD753AA3D16CEA36F97CDAEC5428C5217F1EB6540 * value)
	{
		____signalSettings_1 = value;
		Il2CppCodeGenWriteBarrier((&____signalSettings_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGNALBINDER_TAF04C55124D4BDEFC6038E643AD06EFA454E3584_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef INJECTATTRIBUTE_T021BE444D07BE398EBC7949F054A4CC4CD7846D5_H
#define INJECTATTRIBUTE_T021BE444D07BE398EBC7949F054A4CC4CD7846D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.InjectAttribute
struct  InjectAttribute_t021BE444D07BE398EBC7949F054A4CC4CD7846D5  : public InjectAttributeBase_tB713A40D8E7E8BDC6CD1DDF1C13D71BFE3D6C8DC
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INJECTATTRIBUTE_T021BE444D07BE398EBC7949F054A4CC4CD7846D5_H
#ifndef INJECTLOCALATTRIBUTE_TBEBCA9BE617040D9D810BA0C35067D72B9A77821_H
#define INJECTLOCALATTRIBUTE_TBEBCA9BE617040D9D810BA0C35067D72B9A77821_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.InjectLocalAttribute
struct  InjectLocalAttribute_tBEBCA9BE617040D9D810BA0C35067D72B9A77821  : public InjectAttributeBase_tB713A40D8E7E8BDC6CD1DDF1C13D71BFE3D6C8DC
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INJECTLOCALATTRIBUTE_TBEBCA9BE617040D9D810BA0C35067D72B9A77821_H
#ifndef INJECTOPTIONALATTRIBUTE_T1087DB51DF0147B2F4B1D58DD5F82E826009C3FD_H
#define INJECTOPTIONALATTRIBUTE_T1087DB51DF0147B2F4B1D58DD5F82E826009C3FD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.InjectOptionalAttribute
struct  InjectOptionalAttribute_t1087DB51DF0147B2F4B1D58DD5F82E826009C3FD  : public InjectAttributeBase_tB713A40D8E7E8BDC6CD1DDF1C13D71BFE3D6C8DC
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INJECTOPTIONALATTRIBUTE_T1087DB51DF0147B2F4B1D58DD5F82E826009C3FD_H
#ifndef SIGNALBINDERWITHID_TF62257893A9AA47A533D47DC5DB0C0C046F67B81_H
#define SIGNALBINDERWITHID_TF62257893A9AA47A533D47DC5DB0C0C046F67B81_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SignalBinderWithId
struct  SignalBinderWithId_tF62257893A9AA47A533D47DC5DB0C0C046F67B81  : public SignalBinder_tAF04C55124D4BDEFC6038E643AD06EFA454E3584
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGNALBINDERWITHID_TF62257893A9AA47A533D47DC5DB0C0C046F67B81_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef GUIRENDERER_TCF3B6848056B4A03EB2174897E220DBAD8885BC9_H
#define GUIRENDERER_TCF3B6848056B4A03EB2174897E220DBAD8885BC9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.GuiRenderer
struct  GuiRenderer_tCF3B6848056B4A03EB2174897E220DBAD8885BC9  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Zenject.GuiRenderableManager Zenject.GuiRenderer::_renderableManager
	GuiRenderableManager_tC531B6419C6A6E8E138BA24F00B89723BFB02039 * ____renderableManager_4;

public:
	inline static int32_t get_offset_of__renderableManager_4() { return static_cast<int32_t>(offsetof(GuiRenderer_tCF3B6848056B4A03EB2174897E220DBAD8885BC9, ____renderableManager_4)); }
	inline GuiRenderableManager_tC531B6419C6A6E8E138BA24F00B89723BFB02039 * get__renderableManager_4() const { return ____renderableManager_4; }
	inline GuiRenderableManager_tC531B6419C6A6E8E138BA24F00B89723BFB02039 ** get_address_of__renderableManager_4() { return &____renderableManager_4; }
	inline void set__renderableManager_4(GuiRenderableManager_tC531B6419C6A6E8E138BA24F00B89723BFB02039 * value)
	{
		____renderableManager_4 = value;
		Il2CppCodeGenWriteBarrier((&____renderableManager_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUIRENDERER_TCF3B6848056B4A03EB2174897E220DBAD8885BC9_H
#ifndef MONOKERNEL_T297C7FA263D67830E721CBF0F9C909FAFB569412_H
#define MONOKERNEL_T297C7FA263D67830E721CBF0F9C909FAFB569412_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.MonoKernel
struct  MonoKernel_t297C7FA263D67830E721CBF0F9C909FAFB569412  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Zenject.TickableManager Zenject.MonoKernel::_tickableManager
	TickableManager_t9CDD6FA2E97FC2B4BD99A06A36D29DBE66C03598 * ____tickableManager_4;
	// Zenject.InitializableManager Zenject.MonoKernel::_initializableManager
	InitializableManager_tD805DAFAB6779958C0DEB4BBDA8736C4446A34E9 * ____initializableManager_5;
	// Zenject.DisposableManager Zenject.MonoKernel::_disposablesManager
	DisposableManager_tCB394E2EA1FE174EB07A1082C5F1760FFE6F9ADC * ____disposablesManager_6;
	// System.Boolean Zenject.MonoKernel::_hasInitialized
	bool ____hasInitialized_7;
	// System.Boolean Zenject.MonoKernel::_isDestroyed
	bool ____isDestroyed_8;

public:
	inline static int32_t get_offset_of__tickableManager_4() { return static_cast<int32_t>(offsetof(MonoKernel_t297C7FA263D67830E721CBF0F9C909FAFB569412, ____tickableManager_4)); }
	inline TickableManager_t9CDD6FA2E97FC2B4BD99A06A36D29DBE66C03598 * get__tickableManager_4() const { return ____tickableManager_4; }
	inline TickableManager_t9CDD6FA2E97FC2B4BD99A06A36D29DBE66C03598 ** get_address_of__tickableManager_4() { return &____tickableManager_4; }
	inline void set__tickableManager_4(TickableManager_t9CDD6FA2E97FC2B4BD99A06A36D29DBE66C03598 * value)
	{
		____tickableManager_4 = value;
		Il2CppCodeGenWriteBarrier((&____tickableManager_4), value);
	}

	inline static int32_t get_offset_of__initializableManager_5() { return static_cast<int32_t>(offsetof(MonoKernel_t297C7FA263D67830E721CBF0F9C909FAFB569412, ____initializableManager_5)); }
	inline InitializableManager_tD805DAFAB6779958C0DEB4BBDA8736C4446A34E9 * get__initializableManager_5() const { return ____initializableManager_5; }
	inline InitializableManager_tD805DAFAB6779958C0DEB4BBDA8736C4446A34E9 ** get_address_of__initializableManager_5() { return &____initializableManager_5; }
	inline void set__initializableManager_5(InitializableManager_tD805DAFAB6779958C0DEB4BBDA8736C4446A34E9 * value)
	{
		____initializableManager_5 = value;
		Il2CppCodeGenWriteBarrier((&____initializableManager_5), value);
	}

	inline static int32_t get_offset_of__disposablesManager_6() { return static_cast<int32_t>(offsetof(MonoKernel_t297C7FA263D67830E721CBF0F9C909FAFB569412, ____disposablesManager_6)); }
	inline DisposableManager_tCB394E2EA1FE174EB07A1082C5F1760FFE6F9ADC * get__disposablesManager_6() const { return ____disposablesManager_6; }
	inline DisposableManager_tCB394E2EA1FE174EB07A1082C5F1760FFE6F9ADC ** get_address_of__disposablesManager_6() { return &____disposablesManager_6; }
	inline void set__disposablesManager_6(DisposableManager_tCB394E2EA1FE174EB07A1082C5F1760FFE6F9ADC * value)
	{
		____disposablesManager_6 = value;
		Il2CppCodeGenWriteBarrier((&____disposablesManager_6), value);
	}

	inline static int32_t get_offset_of__hasInitialized_7() { return static_cast<int32_t>(offsetof(MonoKernel_t297C7FA263D67830E721CBF0F9C909FAFB569412, ____hasInitialized_7)); }
	inline bool get__hasInitialized_7() const { return ____hasInitialized_7; }
	inline bool* get_address_of__hasInitialized_7() { return &____hasInitialized_7; }
	inline void set__hasInitialized_7(bool value)
	{
		____hasInitialized_7 = value;
	}

	inline static int32_t get_offset_of__isDestroyed_8() { return static_cast<int32_t>(offsetof(MonoKernel_t297C7FA263D67830E721CBF0F9C909FAFB569412, ____isDestroyed_8)); }
	inline bool get__isDestroyed_8() const { return ____isDestroyed_8; }
	inline bool* get_address_of__isDestroyed_8() { return &____isDestroyed_8; }
	inline void set__isDestroyed_8(bool value)
	{
		____isDestroyed_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOKERNEL_T297C7FA263D67830E721CBF0F9C909FAFB569412_H
#ifndef DEFAULTGAMEOBJECTKERNEL_TEFE39ECEBCD84D7296BF820481303DD933300211_H
#define DEFAULTGAMEOBJECTKERNEL_TEFE39ECEBCD84D7296BF820481303DD933300211_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.DefaultGameObjectKernel
struct  DefaultGameObjectKernel_tEFE39ECEBCD84D7296BF820481303DD933300211  : public MonoKernel_t297C7FA263D67830E721CBF0F9C909FAFB569412
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTGAMEOBJECTKERNEL_TEFE39ECEBCD84D7296BF820481303DD933300211_H
#ifndef PROJECTKERNEL_TC9D59436FEAD42E4DE77F148A6A66D5E3A7DEC65_H
#define PROJECTKERNEL_TC9D59436FEAD42E4DE77F148A6A66D5E3A7DEC65_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ProjectKernel
struct  ProjectKernel_tC9D59436FEAD42E4DE77F148A6A66D5E3A7DEC65  : public MonoKernel_t297C7FA263D67830E721CBF0F9C909FAFB569412
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROJECTKERNEL_TC9D59436FEAD42E4DE77F148A6A66D5E3A7DEC65_H
#ifndef SCENEKERNEL_T3B5AE9FDC200C05E2F7EFE4C8FF5984150F92080_H
#define SCENEKERNEL_T3B5AE9FDC200C05E2F7EFE4C8FF5984150F92080_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SceneKernel
struct  SceneKernel_t3B5AE9FDC200C05E2F7EFE4C8FF5984150F92080  : public MonoKernel_t297C7FA263D67830E721CBF0F9C909FAFB569412
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENEKERNEL_T3B5AE9FDC200C05E2F7EFE4C8FF5984150F92080_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7500 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7501 = { sizeof (DisposableManager_tCB394E2EA1FE174EB07A1082C5F1760FFE6F9ADC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7501[4] = 
{
	DisposableManager_tCB394E2EA1FE174EB07A1082C5F1760FFE6F9ADC::get_offset_of__disposables_0(),
	DisposableManager_tCB394E2EA1FE174EB07A1082C5F1760FFE6F9ADC::get_offset_of__lateDisposables_1(),
	DisposableManager_tCB394E2EA1FE174EB07A1082C5F1760FFE6F9ADC::get_offset_of__disposed_2(),
	DisposableManager_tCB394E2EA1FE174EB07A1082C5F1760FFE6F9ADC::get_offset_of__lateDisposed_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7502 = { sizeof (DisposableInfo_t170B8D5A1C5227BB4833C70484FCF3514B6F13A3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7502[2] = 
{
	DisposableInfo_t170B8D5A1C5227BB4833C70484FCF3514B6F13A3::get_offset_of_Disposable_0(),
	DisposableInfo_t170B8D5A1C5227BB4833C70484FCF3514B6F13A3::get_offset_of_Priority_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7503 = { sizeof (LateDisposableInfo_t1C039847D21F8D03F94453CFCE13D0B19637BA59), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7503[2] = 
{
	LateDisposableInfo_t1C039847D21F8D03F94453CFCE13D0B19637BA59::get_offset_of_LateDisposable_0(),
	LateDisposableInfo_t1C039847D21F8D03F94453CFCE13D0B19637BA59::get_offset_of_Priority_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7504 = { sizeof (U3CU3Ec__DisplayClass4_0_t479536EF753C830063F487D1CDB17384FED70372), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7504[1] = 
{
	U3CU3Ec__DisplayClass4_0_t479536EF753C830063F487D1CDB17384FED70372::get_offset_of_disposable_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7505 = { sizeof (U3CU3Ec__DisplayClass4_1_tEDC62BF0F96622C661ADCED44FABD29E95F00E6F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7505[1] = 
{
	U3CU3Ec__DisplayClass4_1_tEDC62BF0F96622C661ADCED44FABD29E95F00E6F::get_offset_of_lateDisposable_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7506 = { sizeof (U3CU3Ec_tF8E4EB4BF1F15218F9296FA6DD1687807325F7D9), -1, sizeof(U3CU3Ec_tF8E4EB4BF1F15218F9296FA6DD1687807325F7D9_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7506[5] = 
{
	U3CU3Ec_tF8E4EB4BF1F15218F9296FA6DD1687807325F7D9_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tF8E4EB4BF1F15218F9296FA6DD1687807325F7D9_StaticFields::get_offset_of_U3CU3E9__4_1_1(),
	U3CU3Ec_tF8E4EB4BF1F15218F9296FA6DD1687807325F7D9_StaticFields::get_offset_of_U3CU3E9__4_3_2(),
	U3CU3Ec_tF8E4EB4BF1F15218F9296FA6DD1687807325F7D9_StaticFields::get_offset_of_U3CU3E9__8_0_3(),
	U3CU3Ec_tF8E4EB4BF1F15218F9296FA6DD1687807325F7D9_StaticFields::get_offset_of_U3CU3E9__9_0_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7507 = { sizeof (U3CU3Ec__DisplayClass7_0_t88399ACE864238F807998453463C5E4D86A49007), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7507[1] = 
{
	U3CU3Ec__DisplayClass7_0_t88399ACE864238F807998453463C5E4D86A49007::get_offset_of_disposable_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7508 = { sizeof (GuiRenderableManager_tC531B6419C6A6E8E138BA24F00B89723BFB02039), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7508[1] = 
{
	GuiRenderableManager_tC531B6419C6A6E8E138BA24F00B89723BFB02039::get_offset_of__renderables_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7509 = { sizeof (RenderableInfo_t2AC58E82060D2F28405BD116283BD0C834DE0B17), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7509[2] = 
{
	RenderableInfo_t2AC58E82060D2F28405BD116283BD0C834DE0B17::get_offset_of_Renderable_0(),
	RenderableInfo_t2AC58E82060D2F28405BD116283BD0C834DE0B17::get_offset_of_Priority_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7510 = { sizeof (U3CU3Ec__DisplayClass1_0_t668E7FAA7CCBE02D8D13DA0A36BA48FA457B538A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7510[1] = 
{
	U3CU3Ec__DisplayClass1_0_t668E7FAA7CCBE02D8D13DA0A36BA48FA457B538A::get_offset_of_renderable_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7511 = { sizeof (U3CU3Ec_t85EFB76798B2F1DB670FCA54235040FD2A810B9A), -1, sizeof(U3CU3Ec_t85EFB76798B2F1DB670FCA54235040FD2A810B9A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7511[3] = 
{
	U3CU3Ec_t85EFB76798B2F1DB670FCA54235040FD2A810B9A_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t85EFB76798B2F1DB670FCA54235040FD2A810B9A_StaticFields::get_offset_of_U3CU3E9__1_2_1(),
	U3CU3Ec_t85EFB76798B2F1DB670FCA54235040FD2A810B9A_StaticFields::get_offset_of_U3CU3E9__1_0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7512 = { sizeof (GuiRenderer_tCF3B6848056B4A03EB2174897E220DBAD8885BC9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7512[1] = 
{
	GuiRenderer_tCF3B6848056B4A03EB2174897E220DBAD8885BC9::get_offset_of__renderableManager_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7513 = { sizeof (InitializableManager_tD805DAFAB6779958C0DEB4BBDA8736C4446A34E9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7513[2] = 
{
	InitializableManager_tD805DAFAB6779958C0DEB4BBDA8736C4446A34E9::get_offset_of__initializables_0(),
	InitializableManager_tD805DAFAB6779958C0DEB4BBDA8736C4446A34E9::get_offset_of__hasInitialized_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7514 = { sizeof (InitializableInfo_t6BA7D4503EFC7A38FA8FE914FC7116C14AA3B986), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7514[2] = 
{
	InitializableInfo_t6BA7D4503EFC7A38FA8FE914FC7116C14AA3B986::get_offset_of_Initializable_0(),
	InitializableInfo_t6BA7D4503EFC7A38FA8FE914FC7116C14AA3B986::get_offset_of_Priority_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7515 = { sizeof (U3CU3Ec__DisplayClass2_0_tF29DA2F6CF9258673E63A4E1D0306DD2B774348E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7515[1] = 
{
	U3CU3Ec__DisplayClass2_0_tF29DA2F6CF9258673E63A4E1D0306DD2B774348E::get_offset_of_initializable_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7516 = { sizeof (U3CU3Ec_t479EC722B066AB61FE75C61422C933886503B76F), -1, sizeof(U3CU3Ec_t479EC722B066AB61FE75C61422C933886503B76F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7516[3] = 
{
	U3CU3Ec_t479EC722B066AB61FE75C61422C933886503B76F_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t479EC722B066AB61FE75C61422C933886503B76F_StaticFields::get_offset_of_U3CU3E9__2_1_1(),
	U3CU3Ec_t479EC722B066AB61FE75C61422C933886503B76F_StaticFields::get_offset_of_U3CU3E9__3_0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7517 = { sizeof (DefaultGameObjectKernel_tEFE39ECEBCD84D7296BF820481303DD933300211), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7518 = { sizeof (Kernel_tBFCF518491D3E7A46A3537F00925D7F50949B357), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7518[3] = 
{
	Kernel_tBFCF518491D3E7A46A3537F00925D7F50949B357::get_offset_of__tickableManager_0(),
	Kernel_tBFCF518491D3E7A46A3537F00925D7F50949B357::get_offset_of__initializableManager_1(),
	Kernel_tBFCF518491D3E7A46A3537F00925D7F50949B357::get_offset_of__disposablesManager_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7519 = { sizeof (MonoKernel_t297C7FA263D67830E721CBF0F9C909FAFB569412), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7519[5] = 
{
	MonoKernel_t297C7FA263D67830E721CBF0F9C909FAFB569412::get_offset_of__tickableManager_4(),
	MonoKernel_t297C7FA263D67830E721CBF0F9C909FAFB569412::get_offset_of__initializableManager_5(),
	MonoKernel_t297C7FA263D67830E721CBF0F9C909FAFB569412::get_offset_of__disposablesManager_6(),
	MonoKernel_t297C7FA263D67830E721CBF0F9C909FAFB569412::get_offset_of__hasInitialized_7(),
	MonoKernel_t297C7FA263D67830E721CBF0F9C909FAFB569412::get_offset_of__isDestroyed_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7520 = { sizeof (ProjectKernel_tC9D59436FEAD42E4DE77F148A6A66D5E3A7DEC65), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7521 = { sizeof (SceneKernel_t3B5AE9FDC200C05E2F7EFE4C8FF5984150F92080), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7522 = { sizeof (SceneContextRegistry_tEBF3D91B318174447F3993BDE553F5D431B9A7FD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7522[1] = 
{
	SceneContextRegistry_tEBF3D91B318174447F3993BDE553F5D431B9A7FD::get_offset_of__map_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7523 = { sizeof (SceneContextRegistryAdderAndRemover_t5A47B219C754FDC6E48B2187C7F078C538EA3C88), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7523[2] = 
{
	SceneContextRegistryAdderAndRemover_t5A47B219C754FDC6E48B2187C7F078C538EA3C88::get_offset_of__registry_0(),
	SceneContextRegistryAdderAndRemover_t5A47B219C754FDC6E48B2187C7F078C538EA3C88::get_offset_of__sceneContext_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7524 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7524[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7525 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7525[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7526 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7526[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7527 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7527[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7528 = { sizeof (TickablesTaskUpdater_tF277702A3B140691294521F1606DE293E0DE7897), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7529 = { sizeof (LateTickablesTaskUpdater_t66115F857C2D233BB36D6086F2FFC6D32580B9EB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7530 = { sizeof (FixedTickablesTaskUpdater_t72C0FFEE087922D42CEBB55EE5243DC64D8B367A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7531 = { sizeof (TickableManager_t9CDD6FA2E97FC2B4BD99A06A36D29DBE66C03598), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7531[10] = 
{
	TickableManager_t9CDD6FA2E97FC2B4BD99A06A36D29DBE66C03598::get_offset_of__tickables_0(),
	TickableManager_t9CDD6FA2E97FC2B4BD99A06A36D29DBE66C03598::get_offset_of__fixedTickables_1(),
	TickableManager_t9CDD6FA2E97FC2B4BD99A06A36D29DBE66C03598::get_offset_of__lateTickables_2(),
	TickableManager_t9CDD6FA2E97FC2B4BD99A06A36D29DBE66C03598::get_offset_of__priorities_3(),
	TickableManager_t9CDD6FA2E97FC2B4BD99A06A36D29DBE66C03598::get_offset_of__fixedPriorities_4(),
	TickableManager_t9CDD6FA2E97FC2B4BD99A06A36D29DBE66C03598::get_offset_of__latePriorities_5(),
	TickableManager_t9CDD6FA2E97FC2B4BD99A06A36D29DBE66C03598::get_offset_of__updater_6(),
	TickableManager_t9CDD6FA2E97FC2B4BD99A06A36D29DBE66C03598::get_offset_of__fixedUpdater_7(),
	TickableManager_t9CDD6FA2E97FC2B4BD99A06A36D29DBE66C03598::get_offset_of__lateUpdater_8(),
	TickableManager_t9CDD6FA2E97FC2B4BD99A06A36D29DBE66C03598::get_offset_of__isPaused_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7532 = { sizeof (U3CU3Ec__DisplayClass17_0_t75F7AEF263B69816A7B30D234F527DA4763D7215), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7532[1] = 
{
	U3CU3Ec__DisplayClass17_0_t75F7AEF263B69816A7B30D234F527DA4763D7215::get_offset_of_tickable_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7533 = { sizeof (U3CU3Ec_t38677C43FFD46FE34008A422035537F6B98BA7E2), -1, sizeof(U3CU3Ec_t38677C43FFD46FE34008A422035537F6B98BA7E2_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7533[7] = 
{
	U3CU3Ec_t38677C43FFD46FE34008A422035537F6B98BA7E2_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t38677C43FFD46FE34008A422035537F6B98BA7E2_StaticFields::get_offset_of_U3CU3E9__17_0_1(),
	U3CU3Ec_t38677C43FFD46FE34008A422035537F6B98BA7E2_StaticFields::get_offset_of_U3CU3E9__17_2_2(),
	U3CU3Ec_t38677C43FFD46FE34008A422035537F6B98BA7E2_StaticFields::get_offset_of_U3CU3E9__18_0_3(),
	U3CU3Ec_t38677C43FFD46FE34008A422035537F6B98BA7E2_StaticFields::get_offset_of_U3CU3E9__18_2_4(),
	U3CU3Ec_t38677C43FFD46FE34008A422035537F6B98BA7E2_StaticFields::get_offset_of_U3CU3E9__19_0_5(),
	U3CU3Ec_t38677C43FFD46FE34008A422035537F6B98BA7E2_StaticFields::get_offset_of_U3CU3E9__19_2_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7534 = { sizeof (U3CU3Ec__DisplayClass18_0_tE43DA7214898BE45D3DD3B4EBCAE95009452BFD4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7534[1] = 
{
	U3CU3Ec__DisplayClass18_0_tE43DA7214898BE45D3DD3B4EBCAE95009452BFD4::get_offset_of_tickable_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7535 = { sizeof (U3CU3Ec__DisplayClass19_0_t6AC9BFB6C0E5A0E188BFF0D6A3755A31E61FF188), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7535[1] = 
{
	U3CU3Ec__DisplayClass19_0_t6AC9BFB6C0E5A0E188BFF0D6A3755A31E61FF188::get_offset_of_tickable_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7536 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7536[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7537 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7537[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7538 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7538[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7539 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7539[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7540 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7540[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7541 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7541[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7542 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7543 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7544 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7544[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7545 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7546 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7546[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7547 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7548 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7548[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7549 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7550 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7550[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7551 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7552 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7552[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7553 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7554 = { sizeof (SignalSettings_tD753AA3D16CEA36F97CDAEC5428C5217F1EB6540), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7554[1] = 
{
	SignalSettings_tD753AA3D16CEA36F97CDAEC5428C5217F1EB6540::get_offset_of_RequiresHandler_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7555 = { sizeof (SignalBase_t1A82E76F19A3098BB4AF7EDD41C29E6CAD3F95B3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7555[3] = 
{
	SignalBase_t1A82E76F19A3098BB4AF7EDD41C29E6CAD3F95B3::get_offset_of__manager_0(),
	SignalBase_t1A82E76F19A3098BB4AF7EDD41C29E6CAD3F95B3::get_offset_of_U3CSignalIdU3Ek__BackingField_1(),
	SignalBase_t1A82E76F19A3098BB4AF7EDD41C29E6CAD3F95B3::get_offset_of_U3CSettingsU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7556 = { sizeof (SignalBinder_tAF04C55124D4BDEFC6038E643AD06EFA454E3584), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7556[1] = 
{
	SignalBinder_tAF04C55124D4BDEFC6038E643AD06EFA454E3584::get_offset_of__signalSettings_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7557 = { sizeof (SignalBinderWithId_tF62257893A9AA47A533D47DC5DB0C0C046F67B81), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7558 = { sizeof (SignalExtensions_t33C6464932CFDFDA4BEB1F734024765C8A955F25), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7559 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7559[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7560 = { sizeof (SignalHandlerBase_tE5F99D6B541F6C778B7647020EBB4CD5843D6841), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7560[2] = 
{
	SignalHandlerBase_tE5F99D6B541F6C778B7647020EBB4CD5843D6841::get_offset_of__manager_0(),
	SignalHandlerBase_tE5F99D6B541F6C778B7647020EBB4CD5843D6841::get_offset_of__signalId_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7561 = { sizeof (SignalHandlerBinder_t4AB70F7A7BD1596D46B88F834A400EBEDE185771), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7561[4] = 
{
	SignalHandlerBinder_t4AB70F7A7BD1596D46B88F834A400EBEDE185771::get_offset_of__finalizerWrapper_0(),
	SignalHandlerBinder_t4AB70F7A7BD1596D46B88F834A400EBEDE185771::get_offset_of__signalType_1(),
	SignalHandlerBinder_t4AB70F7A7BD1596D46B88F834A400EBEDE185771::get_offset_of__container_2(),
	SignalHandlerBinder_t4AB70F7A7BD1596D46B88F834A400EBEDE185771::get_offset_of_U3CIdentifierU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7562 = { sizeof (SignalHandlerBinderWithId_tF2F486B2BBE4565DB9C112C9D5755BDCFE7BCD27), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7563 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7563[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7564 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7565 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7565[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7566 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7567 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7567[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7568 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7569 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7569[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7570 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7571 = { sizeof (SignalManager_t38E64F258F79FF33F3FDBB9423271BEFF03D7673), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7571[1] = 
{
	SignalManager_t38E64F258F79FF33F3FDBB9423271BEFF03D7673::get_offset_of__signalHandlers_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7572 = { sizeof (U3CU3Ec_t772F9F5DDEA4C5A7BE65DE8A6F05DC9FE7D11513), -1, sizeof(U3CU3Ec_t772F9F5DDEA4C5A7BE65DE8A6F05DC9FE7D11513_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7572[2] = 
{
	U3CU3Ec_t772F9F5DDEA4C5A7BE65DE8A6F05DC9FE7D11513_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t772F9F5DDEA4C5A7BE65DE8A6F05DC9FE7D11513_StaticFields::get_offset_of_U3CU3E9__6_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7573 = { sizeof (StaticMethodSignalHandler_tD43BEE8CBD1A5149095864A157D07492C1090485), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7573[1] = 
{
	StaticMethodSignalHandler_tD43BEE8CBD1A5149095864A157D07492C1090485::get_offset_of__method_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7574 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7574[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7575 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7575[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7576 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7576[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7577 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7577[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7578 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7578[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7579 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7579[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7580 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7580[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7581 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7581[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7582 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7582[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7583 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7584 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7585 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7586 = { sizeof (InjectAttribute_t021BE444D07BE398EBC7949F054A4CC4CD7846D5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7587 = { sizeof (InjectAttributeBase_tB713A40D8E7E8BDC6CD1DDF1C13D71BFE3D6C8DC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7587[3] = 
{
	InjectAttributeBase_tB713A40D8E7E8BDC6CD1DDF1C13D71BFE3D6C8DC::get_offset_of_U3COptionalU3Ek__BackingField_0(),
	InjectAttributeBase_tB713A40D8E7E8BDC6CD1DDF1C13D71BFE3D6C8DC::get_offset_of_U3CIdU3Ek__BackingField_1(),
	InjectAttributeBase_tB713A40D8E7E8BDC6CD1DDF1C13D71BFE3D6C8DC::get_offset_of_U3CSourceU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7588 = { sizeof (InjectLocalAttribute_tBEBCA9BE617040D9D810BA0C35067D72B9A77821), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7589 = { sizeof (InjectOptionalAttribute_t1087DB51DF0147B2F4B1D58DD5F82E826009C3FD), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7590 = { sizeof (InjectSources_t643ECF8A27899CEE7D40760762395F6C3E359049)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7590[5] = 
{
	InjectSources_t643ECF8A27899CEE7D40760762395F6C3E359049::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7591 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7592 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7593 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7594 = { sizeof (ZenjectAllowDuringValidationAttribute_t5894E52430C1843BFB8A7F5AF2DEA5583745D3C6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7595 = { sizeof (ActionInstaller_t01044D07A248D88566B1F14C77EEB1F51235C5E8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7595[1] = 
{
	ActionInstaller_t01044D07A248D88566B1F14C77EEB1F51235C5E8::get_offset_of__installMethod_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7596 = { sizeof (ExecutionOrderInstaller_t8AA9E0919417ED06C9813B63A95849558AA393C5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7596[1] = 
{
	ExecutionOrderInstaller_t8AA9E0919417ED06C9813B63A95849558AA393C5::get_offset_of__typeOrder_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7597 = { sizeof (PoolCleanupChecker_t220D33C1227BB69FD01AD538C6865689D4DF14E8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7597[1] = 
{
	PoolCleanupChecker_t220D33C1227BB69FD01AD538C6865689D4DF14E8::get_offset_of__poolFactories_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7598 = { sizeof (ProfileBlock_t564682EC012FC24C9DBB2DAE56896ED9082AC2FA), -1, sizeof(ProfileBlock_t564682EC012FC24C9DBB2DAE56896ED9082AC2FA_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7598[1] = 
{
	ProfileBlock_t564682EC012FC24C9DBB2DAE56896ED9082AC2FA_StaticFields::get_offset_of_U3CProfilePatternU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7599 = { sizeof (TypeAnalyzer_t19EB8063B9BA23AD8FAAB2AF5228A8D8D086F722), -1, sizeof(TypeAnalyzer_t19EB8063B9BA23AD8FAAB2AF5228A8D8D086F722_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7599[1] = 
{
	TypeAnalyzer_t19EB8063B9BA23AD8FAAB2AF5228A8D8D086F722_StaticFields::get_offset_of__typeInfo_0(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
