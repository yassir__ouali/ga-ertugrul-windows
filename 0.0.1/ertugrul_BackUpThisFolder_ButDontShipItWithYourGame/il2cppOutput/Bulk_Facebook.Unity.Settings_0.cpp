﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
struct GenericVirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
struct GenericInterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// Facebook.Unity.Settings.FacebookSettings
struct FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D;
// Facebook.Unity.Settings.FacebookSettings/<>c
struct U3CU3Ec_t40659AD935CCBCC35589D12303DF7D93D8B4ABA8;
// Facebook.Unity.Settings.FacebookSettings/OnChangeCallback
struct OnChangeCallback_t490B1A020446401D3A4B7030FA7A0D9AF521AFBB;
// Facebook.Unity.Settings.FacebookSettings/OnChangeCallback[]
struct OnChangeCallbackU5BU5D_t85B97440A781E22FA6B3F6D40237D68989DE5A7A;
// Facebook.Unity.Settings.FacebookSettings/UrlSchemes
struct UrlSchemes_t7B8D8455C2FE4B28F5F677FFAE86E14077D94D8C;
// Facebook.Unity.Settings.FacebookSettings/UrlSchemes[]
struct UrlSchemesU5BU5D_t5303172A26BDEC234CBC1631F090FC673F004326;
// System.Action`1<Facebook.Unity.Settings.FacebookSettings/OnChangeCallback>
struct Action_1_t605040A52441734E3EA5810595C95A0D0568C238;
// System.Action`1<System.Object>
struct Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Collections.Generic.List`1<Facebook.Unity.Settings.FacebookSettings/OnChangeCallback>
struct List_1_t4761B02A8CB42D5714C127D74C8BD8BCBD280D33;
// System.Collections.Generic.List`1<Facebook.Unity.Settings.FacebookSettings/UrlSchemes>
struct List_1_tEBAA293BF803F2D17E20E8B6FE34EF3792F6FA6B;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D;
// System.Collections.Generic.List`1<System.String>
struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3;
// System.Delegate
struct Delegate_t;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0;
// UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734;

extern RuntimeClass* Action_1_t605040A52441734E3EA5810595C95A0D0568C238_il2cpp_TypeInfo_var;
extern RuntimeClass* FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t4761B02A8CB42D5714C127D74C8BD8BCBD280D33_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_tEBAA293BF803F2D17E20E8B6FE34EF3792F6FA6B_il2cpp_TypeInfo_var;
extern RuntimeClass* Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern RuntimeClass* U3CU3Ec_t40659AD935CCBCC35589D12303DF7D93D8B4ABA8_il2cpp_TypeInfo_var;
extern RuntimeClass* UrlSchemes_t7B8D8455C2FE4B28F5F677FFAE86E14077D94D8C_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral11A3B430794567143B2D380848C73908507F1D59;
extern String_t* _stringLiteral3D7479543F221F46EE46D1F7AB8E7AD53734F2B9;
extern String_t* _stringLiteral4FB43108E78F4D80425CB74F01AC6D0A59E0A3D3;
extern String_t* _stringLiteralB6589FC6AB0DC82CF12099D1C2D40AB994E8410C;
extern const RuntimeMethod* Action_1__ctor_mCDDBCF9D6DB0D55DA31F3EB00A54CCFDCDFC7064_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m9B9A17315AE28254D75EDF0BDD68D83CB3B8B4F2_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_mA348FA1140766465189459D25B01EB179001DE83_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_mC0B071C59951D54F8FE74C42DFECE1519E24F2EB_RuntimeMethod_var;
extern const RuntimeMethod* List_1_ForEach_mA6A810C3D8B78C414C9FE29C629B0AB7506D6DB3_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Remove_mCC3529501BE616F61AADD2B818FBC70B75905105_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m193A1CCAFC83743EFB9ADBC8FDB87850CD0C9C7A_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_mA011B26BD878095268926AAE1523A7B080A39A20_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_mDA22758D73530683C950C5CCF39BDB4E7E1F3F06_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_mB739B0066E5F7EBDBA9978F24A73D26D4FAE5BED_RuntimeMethod_var;
extern const RuntimeMethod* ScriptableObject_CreateInstance_TisFacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_mE7E98CFB820A2EC3037FAEB169EC29B7161CD8E4_RuntimeMethod_var;
extern const RuntimeMethod* U3CU3Ec_U3CSettingsChangedU3Eb__76_0_m031FF08969EB09D1B8300C64ECF933BEEB7BF99E_RuntimeMethod_var;
extern const uint32_t FacebookSettings_RegisterChangeEventCallback_mA82B49D95A698556243CB1AEE8E971C36726CEDA_MetadataUsageId;
extern const uint32_t FacebookSettings_SettingsChanged_m63C85A637D0698D4AEA8F5B79D45609DE9FF722F_MetadataUsageId;
extern const uint32_t FacebookSettings_UnregisterChangeEventCallback_mB90FA54E3450C9137E147E1F9733F193D7AABD8B_MetadataUsageId;
extern const uint32_t FacebookSettings__cctor_m175D7BD588BA109CF3319B0EF4E447146BDAFA78_MetadataUsageId;
extern const uint32_t FacebookSettings__ctor_m4796A2B919F21695D7B45C9E268682ACE2BE1716_MetadataUsageId;
extern const uint32_t FacebookSettings_get_AdvertiserIDCollectionEnabled_m4DD83E89DA98EE8995722179C7AC622D44CFDAF3_MetadataUsageId;
extern const uint32_t FacebookSettings_get_AppId_m7DCBDAD029BFA64A332B4D8C674ED53BD39A7FA3_MetadataUsageId;
extern const uint32_t FacebookSettings_get_AppIds_m0C3290B4AFF702DBB528DF47F991217B815C0C41_MetadataUsageId;
extern const uint32_t FacebookSettings_get_AppLabels_mAF6B70ED254BD0E1C663C934C13C1FC627F0D2EC_MetadataUsageId;
extern const uint32_t FacebookSettings_get_AppLinkSchemes_mFBA649443DFCA429B12D869987385980B5A6853C_MetadataUsageId;
extern const uint32_t FacebookSettings_get_AutoLogAppEventsEnabled_mC273B9B56076AB18E10A4A2D275FD21B71E16A38_MetadataUsageId;
extern const uint32_t FacebookSettings_get_ChannelUrl_m5434BFEFBF71FE2A27853952FA5618250E850830_MetadataUsageId;
extern const uint32_t FacebookSettings_get_ClientToken_mFE2350548E2BFFDE25A0792A4D251B2E8573D1EA_MetadataUsageId;
extern const uint32_t FacebookSettings_get_ClientTokens_mF619C3FC47B4CDBF10C0CC8D6BDFF404AD277B87_MetadataUsageId;
extern const uint32_t FacebookSettings_get_Cookie_mE587EAFF364E8C6E85B96CD232B6492A390AE20D_MetadataUsageId;
extern const uint32_t FacebookSettings_get_FrictionlessRequests_m5B78574AE3A72513F3E04A8AB754CF41F1B14767_MetadataUsageId;
extern const uint32_t FacebookSettings_get_Instance_m5AA0850DCB8EB477CBA9777312CA4FDB9F6184DC_MetadataUsageId;
extern const uint32_t FacebookSettings_get_IosURLSuffix_m34E46490711FF5DB738006AFA012C5DFE88CDCAC_MetadataUsageId;
extern const uint32_t FacebookSettings_get_IsValidAppId_mFB768FAD141DDF4B5CBD997CF5F7AF49BA945843_MetadataUsageId;
extern const uint32_t FacebookSettings_get_Logging_mD29CBD1ADEA9616010596389836C17A03EE00F01_MetadataUsageId;
extern const uint32_t FacebookSettings_get_NullableInstance_mB9E0EA95ABDEC1B0AAE3A7808FD2F1E9965A98CB_MetadataUsageId;
extern const uint32_t FacebookSettings_get_SelectedAppIndex_m3DBF263911DBC04F99D1606451F61FF1B66F2E8B_MetadataUsageId;
extern const uint32_t FacebookSettings_get_Status_mA8ED29022B4DC50AA6BAA74276761F50FDD4118C_MetadataUsageId;
extern const uint32_t FacebookSettings_get_UploadAccessToken_m99C1236A489B23B374182AE379A4AF1AEB574FCB_MetadataUsageId;
extern const uint32_t FacebookSettings_get_Xfbml_m984DD60D50FDC4B94BBA6EFA20D068E9E5C904EC_MetadataUsageId;
extern const uint32_t FacebookSettings_set_AdvertiserIDCollectionEnabled_mA7BFBD8F0AAAC7415D54EA0D01E15CAAF7F68A0C_MetadataUsageId;
extern const uint32_t FacebookSettings_set_AppIds_m5CC2EFB0DB654A596A8C5963B8420F0197F01BDA_MetadataUsageId;
extern const uint32_t FacebookSettings_set_AppLabels_m1797CCEC7E8D390136D50248A4950FB1BC6E9F7B_MetadataUsageId;
extern const uint32_t FacebookSettings_set_AppLinkSchemes_mD57C8D0D5CD6C7286F9FBF2A745D3B789DE7DDD9_MetadataUsageId;
extern const uint32_t FacebookSettings_set_AutoLogAppEventsEnabled_m870F388BC58518A161C1B29D849DD51F2712D1E8_MetadataUsageId;
extern const uint32_t FacebookSettings_set_ClientTokens_m96C8C6D128C67DD90B3D49709ABE54AA28BEE677_MetadataUsageId;
extern const uint32_t FacebookSettings_set_Cookie_m37B989712829A9FC6CCFAB2803D0FC9580862ED7_MetadataUsageId;
extern const uint32_t FacebookSettings_set_FrictionlessRequests_mB59F5CB1B0246E51C5A77DCD542C6295AE2726A0_MetadataUsageId;
extern const uint32_t FacebookSettings_set_IosURLSuffix_m037025957C9BC65425347C888DC828280D5DA7EE_MetadataUsageId;
extern const uint32_t FacebookSettings_set_Logging_m74F5536272605B1684A3416D35E3EFB1068DF207_MetadataUsageId;
extern const uint32_t FacebookSettings_set_SelectedAppIndex_m9A8D2435E395E95F8D08FB661C6B10569A44B3AF_MetadataUsageId;
extern const uint32_t FacebookSettings_set_Status_mA9AE0A65CC579F73870E1B8A69A2E04F55FFFE8A_MetadataUsageId;
extern const uint32_t FacebookSettings_set_UploadAccessToken_mA405A29F8D46797B5314E19C1991F40851871046_MetadataUsageId;
extern const uint32_t FacebookSettings_set_Xfbml_mF50D4190258CA2967A33779281A46F7E75649947_MetadataUsageId;
extern const uint32_t U3CU3Ec__cctor_mDE57DA4F578EE7C96E604EA3A4CD427F86AF9A9B_MetadataUsageId;
extern const uint32_t UrlSchemes__ctor_mD2CB85DBE160B9144012A889732A4CF3E6FBBA62_MetadataUsageId;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;

struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;


#ifndef U3CMODULEU3E_T7D11BB4BD4C1ADC1F64CC310633FC1BEE61632FE_H
#define U3CMODULEU3E_T7D11BB4BD4C1ADC1F64CC310633FC1BEE61632FE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t7D11BB4BD4C1ADC1F64CC310633FC1BEE61632FE 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T7D11BB4BD4C1ADC1F64CC310633FC1BEE61632FE_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CU3EC_T40659AD935CCBCC35589D12303DF7D93D8B4ABA8_H
#define U3CU3EC_T40659AD935CCBCC35589D12303DF7D93D8B4ABA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Settings.FacebookSettings_<>c
struct  U3CU3Ec_t40659AD935CCBCC35589D12303DF7D93D8B4ABA8  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t40659AD935CCBCC35589D12303DF7D93D8B4ABA8_StaticFields
{
public:
	// Facebook.Unity.Settings.FacebookSettings_<>c Facebook.Unity.Settings.FacebookSettings_<>c::<>9
	U3CU3Ec_t40659AD935CCBCC35589D12303DF7D93D8B4ABA8 * ___U3CU3E9_0;
	// System.Action`1<Facebook.Unity.Settings.FacebookSettings_OnChangeCallback> Facebook.Unity.Settings.FacebookSettings_<>c::<>9__76_0
	Action_1_t605040A52441734E3EA5810595C95A0D0568C238 * ___U3CU3E9__76_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t40659AD935CCBCC35589D12303DF7D93D8B4ABA8_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t40659AD935CCBCC35589D12303DF7D93D8B4ABA8 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t40659AD935CCBCC35589D12303DF7D93D8B4ABA8 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t40659AD935CCBCC35589D12303DF7D93D8B4ABA8 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__76_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t40659AD935CCBCC35589D12303DF7D93D8B4ABA8_StaticFields, ___U3CU3E9__76_0_1)); }
	inline Action_1_t605040A52441734E3EA5810595C95A0D0568C238 * get_U3CU3E9__76_0_1() const { return ___U3CU3E9__76_0_1; }
	inline Action_1_t605040A52441734E3EA5810595C95A0D0568C238 ** get_address_of_U3CU3E9__76_0_1() { return &___U3CU3E9__76_0_1; }
	inline void set_U3CU3E9__76_0_1(Action_1_t605040A52441734E3EA5810595C95A0D0568C238 * value)
	{
		___U3CU3E9__76_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__76_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T40659AD935CCBCC35589D12303DF7D93D8B4ABA8_H
#ifndef URLSCHEMES_T7B8D8455C2FE4B28F5F677FFAE86E14077D94D8C_H
#define URLSCHEMES_T7B8D8455C2FE4B28F5F677FFAE86E14077D94D8C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Settings.FacebookSettings_UrlSchemes
struct  UrlSchemes_t7B8D8455C2FE4B28F5F677FFAE86E14077D94D8C  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.String> Facebook.Unity.Settings.FacebookSettings_UrlSchemes::list
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___list_0;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(UrlSchemes_t7B8D8455C2FE4B28F5F677FFAE86E14077D94D8C, ___list_0)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_list_0() const { return ___list_0; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URLSCHEMES_T7B8D8455C2FE4B28F5F677FFAE86E14077D94D8C_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef LIST_1_T4761B02A8CB42D5714C127D74C8BD8BCBD280D33_H
#define LIST_1_T4761B02A8CB42D5714C127D74C8BD8BCBD280D33_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<Facebook.Unity.Settings.FacebookSettings_OnChangeCallback>
struct  List_1_t4761B02A8CB42D5714C127D74C8BD8BCBD280D33  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	OnChangeCallbackU5BU5D_t85B97440A781E22FA6B3F6D40237D68989DE5A7A* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t4761B02A8CB42D5714C127D74C8BD8BCBD280D33, ____items_1)); }
	inline OnChangeCallbackU5BU5D_t85B97440A781E22FA6B3F6D40237D68989DE5A7A* get__items_1() const { return ____items_1; }
	inline OnChangeCallbackU5BU5D_t85B97440A781E22FA6B3F6D40237D68989DE5A7A** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(OnChangeCallbackU5BU5D_t85B97440A781E22FA6B3F6D40237D68989DE5A7A* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t4761B02A8CB42D5714C127D74C8BD8BCBD280D33, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t4761B02A8CB42D5714C127D74C8BD8BCBD280D33, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t4761B02A8CB42D5714C127D74C8BD8BCBD280D33, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_t4761B02A8CB42D5714C127D74C8BD8BCBD280D33_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	OnChangeCallbackU5BU5D_t85B97440A781E22FA6B3F6D40237D68989DE5A7A* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t4761B02A8CB42D5714C127D74C8BD8BCBD280D33_StaticFields, ____emptyArray_5)); }
	inline OnChangeCallbackU5BU5D_t85B97440A781E22FA6B3F6D40237D68989DE5A7A* get__emptyArray_5() const { return ____emptyArray_5; }
	inline OnChangeCallbackU5BU5D_t85B97440A781E22FA6B3F6D40237D68989DE5A7A** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(OnChangeCallbackU5BU5D_t85B97440A781E22FA6B3F6D40237D68989DE5A7A* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T4761B02A8CB42D5714C127D74C8BD8BCBD280D33_H
#ifndef LIST_1_TEBAA293BF803F2D17E20E8B6FE34EF3792F6FA6B_H
#define LIST_1_TEBAA293BF803F2D17E20E8B6FE34EF3792F6FA6B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<Facebook.Unity.Settings.FacebookSettings_UrlSchemes>
struct  List_1_tEBAA293BF803F2D17E20E8B6FE34EF3792F6FA6B  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	UrlSchemesU5BU5D_t5303172A26BDEC234CBC1631F090FC673F004326* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tEBAA293BF803F2D17E20E8B6FE34EF3792F6FA6B, ____items_1)); }
	inline UrlSchemesU5BU5D_t5303172A26BDEC234CBC1631F090FC673F004326* get__items_1() const { return ____items_1; }
	inline UrlSchemesU5BU5D_t5303172A26BDEC234CBC1631F090FC673F004326** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(UrlSchemesU5BU5D_t5303172A26BDEC234CBC1631F090FC673F004326* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tEBAA293BF803F2D17E20E8B6FE34EF3792F6FA6B, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tEBAA293BF803F2D17E20E8B6FE34EF3792F6FA6B, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tEBAA293BF803F2D17E20E8B6FE34EF3792F6FA6B, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_tEBAA293BF803F2D17E20E8B6FE34EF3792F6FA6B_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	UrlSchemesU5BU5D_t5303172A26BDEC234CBC1631F090FC673F004326* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tEBAA293BF803F2D17E20E8B6FE34EF3792F6FA6B_StaticFields, ____emptyArray_5)); }
	inline UrlSchemesU5BU5D_t5303172A26BDEC234CBC1631F090FC673F004326* get__emptyArray_5() const { return ____emptyArray_5; }
	inline UrlSchemesU5BU5D_t5303172A26BDEC234CBC1631F090FC673F004326** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(UrlSchemesU5BU5D_t5303172A26BDEC234CBC1631F090FC673F004326* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_TEBAA293BF803F2D17E20E8B6FE34EF3792F6FA6B_H
#ifndef LIST_1_TE8032E48C661C350FF9550E9063D595C0AB25CD3_H
#define LIST_1_TE8032E48C661C350FF9550E9063D595C0AB25CD3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.String>
struct  List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3, ____items_1)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get__items_1() const { return ____items_1; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3_StaticFields, ____emptyArray_5)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get__emptyArray_5() const { return ____emptyArray_5; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_TE8032E48C661C350FF9550E9063D595C0AB25CD3_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#define BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifndef INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#define INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#define SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_pinvoke : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_com : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifndef FACEBOOKSETTINGS_T1A2119548288F7AD79D590C587740A8EED14D74D_H
#define FACEBOOKSETTINGS_T1A2119548288F7AD79D590C587740A8EED14D74D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Settings.FacebookSettings
struct  FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// System.Int32 Facebook.Unity.Settings.FacebookSettings::selectedAppIndex
	int32_t ___selectedAppIndex_9;
	// System.Collections.Generic.List`1<System.String> Facebook.Unity.Settings.FacebookSettings::clientTokens
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___clientTokens_10;
	// System.Collections.Generic.List`1<System.String> Facebook.Unity.Settings.FacebookSettings::appIds
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___appIds_11;
	// System.Collections.Generic.List`1<System.String> Facebook.Unity.Settings.FacebookSettings::appLabels
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___appLabels_12;
	// System.Boolean Facebook.Unity.Settings.FacebookSettings::cookie
	bool ___cookie_13;
	// System.Boolean Facebook.Unity.Settings.FacebookSettings::logging
	bool ___logging_14;
	// System.Boolean Facebook.Unity.Settings.FacebookSettings::status
	bool ___status_15;
	// System.Boolean Facebook.Unity.Settings.FacebookSettings::xfbml
	bool ___xfbml_16;
	// System.Boolean Facebook.Unity.Settings.FacebookSettings::frictionlessRequests
	bool ___frictionlessRequests_17;
	// System.String Facebook.Unity.Settings.FacebookSettings::iosURLSuffix
	String_t* ___iosURLSuffix_18;
	// System.Collections.Generic.List`1<Facebook.Unity.Settings.FacebookSettings_UrlSchemes> Facebook.Unity.Settings.FacebookSettings::appLinkSchemes
	List_1_tEBAA293BF803F2D17E20E8B6FE34EF3792F6FA6B * ___appLinkSchemes_19;
	// System.String Facebook.Unity.Settings.FacebookSettings::uploadAccessToken
	String_t* ___uploadAccessToken_20;
	// System.Boolean Facebook.Unity.Settings.FacebookSettings::autoLogAppEventsEnabled
	bool ___autoLogAppEventsEnabled_21;
	// System.Boolean Facebook.Unity.Settings.FacebookSettings::advertiserIDCollectionEnabled
	bool ___advertiserIDCollectionEnabled_22;

public:
	inline static int32_t get_offset_of_selectedAppIndex_9() { return static_cast<int32_t>(offsetof(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D, ___selectedAppIndex_9)); }
	inline int32_t get_selectedAppIndex_9() const { return ___selectedAppIndex_9; }
	inline int32_t* get_address_of_selectedAppIndex_9() { return &___selectedAppIndex_9; }
	inline void set_selectedAppIndex_9(int32_t value)
	{
		___selectedAppIndex_9 = value;
	}

	inline static int32_t get_offset_of_clientTokens_10() { return static_cast<int32_t>(offsetof(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D, ___clientTokens_10)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_clientTokens_10() const { return ___clientTokens_10; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_clientTokens_10() { return &___clientTokens_10; }
	inline void set_clientTokens_10(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___clientTokens_10 = value;
		Il2CppCodeGenWriteBarrier((&___clientTokens_10), value);
	}

	inline static int32_t get_offset_of_appIds_11() { return static_cast<int32_t>(offsetof(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D, ___appIds_11)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_appIds_11() const { return ___appIds_11; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_appIds_11() { return &___appIds_11; }
	inline void set_appIds_11(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___appIds_11 = value;
		Il2CppCodeGenWriteBarrier((&___appIds_11), value);
	}

	inline static int32_t get_offset_of_appLabels_12() { return static_cast<int32_t>(offsetof(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D, ___appLabels_12)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_appLabels_12() const { return ___appLabels_12; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_appLabels_12() { return &___appLabels_12; }
	inline void set_appLabels_12(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___appLabels_12 = value;
		Il2CppCodeGenWriteBarrier((&___appLabels_12), value);
	}

	inline static int32_t get_offset_of_cookie_13() { return static_cast<int32_t>(offsetof(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D, ___cookie_13)); }
	inline bool get_cookie_13() const { return ___cookie_13; }
	inline bool* get_address_of_cookie_13() { return &___cookie_13; }
	inline void set_cookie_13(bool value)
	{
		___cookie_13 = value;
	}

	inline static int32_t get_offset_of_logging_14() { return static_cast<int32_t>(offsetof(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D, ___logging_14)); }
	inline bool get_logging_14() const { return ___logging_14; }
	inline bool* get_address_of_logging_14() { return &___logging_14; }
	inline void set_logging_14(bool value)
	{
		___logging_14 = value;
	}

	inline static int32_t get_offset_of_status_15() { return static_cast<int32_t>(offsetof(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D, ___status_15)); }
	inline bool get_status_15() const { return ___status_15; }
	inline bool* get_address_of_status_15() { return &___status_15; }
	inline void set_status_15(bool value)
	{
		___status_15 = value;
	}

	inline static int32_t get_offset_of_xfbml_16() { return static_cast<int32_t>(offsetof(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D, ___xfbml_16)); }
	inline bool get_xfbml_16() const { return ___xfbml_16; }
	inline bool* get_address_of_xfbml_16() { return &___xfbml_16; }
	inline void set_xfbml_16(bool value)
	{
		___xfbml_16 = value;
	}

	inline static int32_t get_offset_of_frictionlessRequests_17() { return static_cast<int32_t>(offsetof(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D, ___frictionlessRequests_17)); }
	inline bool get_frictionlessRequests_17() const { return ___frictionlessRequests_17; }
	inline bool* get_address_of_frictionlessRequests_17() { return &___frictionlessRequests_17; }
	inline void set_frictionlessRequests_17(bool value)
	{
		___frictionlessRequests_17 = value;
	}

	inline static int32_t get_offset_of_iosURLSuffix_18() { return static_cast<int32_t>(offsetof(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D, ___iosURLSuffix_18)); }
	inline String_t* get_iosURLSuffix_18() const { return ___iosURLSuffix_18; }
	inline String_t** get_address_of_iosURLSuffix_18() { return &___iosURLSuffix_18; }
	inline void set_iosURLSuffix_18(String_t* value)
	{
		___iosURLSuffix_18 = value;
		Il2CppCodeGenWriteBarrier((&___iosURLSuffix_18), value);
	}

	inline static int32_t get_offset_of_appLinkSchemes_19() { return static_cast<int32_t>(offsetof(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D, ___appLinkSchemes_19)); }
	inline List_1_tEBAA293BF803F2D17E20E8B6FE34EF3792F6FA6B * get_appLinkSchemes_19() const { return ___appLinkSchemes_19; }
	inline List_1_tEBAA293BF803F2D17E20E8B6FE34EF3792F6FA6B ** get_address_of_appLinkSchemes_19() { return &___appLinkSchemes_19; }
	inline void set_appLinkSchemes_19(List_1_tEBAA293BF803F2D17E20E8B6FE34EF3792F6FA6B * value)
	{
		___appLinkSchemes_19 = value;
		Il2CppCodeGenWriteBarrier((&___appLinkSchemes_19), value);
	}

	inline static int32_t get_offset_of_uploadAccessToken_20() { return static_cast<int32_t>(offsetof(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D, ___uploadAccessToken_20)); }
	inline String_t* get_uploadAccessToken_20() const { return ___uploadAccessToken_20; }
	inline String_t** get_address_of_uploadAccessToken_20() { return &___uploadAccessToken_20; }
	inline void set_uploadAccessToken_20(String_t* value)
	{
		___uploadAccessToken_20 = value;
		Il2CppCodeGenWriteBarrier((&___uploadAccessToken_20), value);
	}

	inline static int32_t get_offset_of_autoLogAppEventsEnabled_21() { return static_cast<int32_t>(offsetof(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D, ___autoLogAppEventsEnabled_21)); }
	inline bool get_autoLogAppEventsEnabled_21() const { return ___autoLogAppEventsEnabled_21; }
	inline bool* get_address_of_autoLogAppEventsEnabled_21() { return &___autoLogAppEventsEnabled_21; }
	inline void set_autoLogAppEventsEnabled_21(bool value)
	{
		___autoLogAppEventsEnabled_21 = value;
	}

	inline static int32_t get_offset_of_advertiserIDCollectionEnabled_22() { return static_cast<int32_t>(offsetof(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D, ___advertiserIDCollectionEnabled_22)); }
	inline bool get_advertiserIDCollectionEnabled_22() const { return ___advertiserIDCollectionEnabled_22; }
	inline bool* get_address_of_advertiserIDCollectionEnabled_22() { return &___advertiserIDCollectionEnabled_22; }
	inline void set_advertiserIDCollectionEnabled_22(bool value)
	{
		___advertiserIDCollectionEnabled_22 = value;
	}
};

struct FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_StaticFields
{
public:
	// System.Collections.Generic.List`1<Facebook.Unity.Settings.FacebookSettings_OnChangeCallback> Facebook.Unity.Settings.FacebookSettings::onChangeCallbacks
	List_1_t4761B02A8CB42D5714C127D74C8BD8BCBD280D33 * ___onChangeCallbacks_7;
	// Facebook.Unity.Settings.FacebookSettings Facebook.Unity.Settings.FacebookSettings::instance
	FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D * ___instance_8;

public:
	inline static int32_t get_offset_of_onChangeCallbacks_7() { return static_cast<int32_t>(offsetof(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_StaticFields, ___onChangeCallbacks_7)); }
	inline List_1_t4761B02A8CB42D5714C127D74C8BD8BCBD280D33 * get_onChangeCallbacks_7() const { return ___onChangeCallbacks_7; }
	inline List_1_t4761B02A8CB42D5714C127D74C8BD8BCBD280D33 ** get_address_of_onChangeCallbacks_7() { return &___onChangeCallbacks_7; }
	inline void set_onChangeCallbacks_7(List_1_t4761B02A8CB42D5714C127D74C8BD8BCBD280D33 * value)
	{
		___onChangeCallbacks_7 = value;
		Il2CppCodeGenWriteBarrier((&___onChangeCallbacks_7), value);
	}

	inline static int32_t get_offset_of_instance_8() { return static_cast<int32_t>(offsetof(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_StaticFields, ___instance_8)); }
	inline FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D * get_instance_8() const { return ___instance_8; }
	inline FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D ** get_address_of_instance_8() { return &___instance_8; }
	inline void set_instance_8(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D * value)
	{
		___instance_8 = value;
		Il2CppCodeGenWriteBarrier((&___instance_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACEBOOKSETTINGS_T1A2119548288F7AD79D590C587740A8EED14D74D_H
#ifndef ONCHANGECALLBACK_T490B1A020446401D3A4B7030FA7A0D9AF521AFBB_H
#define ONCHANGECALLBACK_T490B1A020446401D3A4B7030FA7A0D9AF521AFBB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Settings.FacebookSettings_OnChangeCallback
struct  OnChangeCallback_t490B1A020446401D3A4B7030FA7A0D9AF521AFBB  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONCHANGECALLBACK_T490B1A020446401D3A4B7030FA7A0D9AF521AFBB_H
#ifndef ACTION_1_T605040A52441734E3EA5810595C95A0D0568C238_H
#define ACTION_1_T605040A52441734E3EA5810595C95A0D0568C238_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`1<Facebook.Unity.Settings.FacebookSettings_OnChangeCallback>
struct  Action_1_t605040A52441734E3EA5810595C95A0D0568C238  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_1_T605040A52441734E3EA5810595C95A0D0568C238_H
#ifndef ASYNCCALLBACK_T3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4_H
#define ASYNCCALLBACK_T3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.AsyncCallback
struct  AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCCALLBACK_T3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4_H
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Delegate_t * m_Items[1];

public:
	inline Delegate_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Delegate_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Delegate_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Delegate_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Delegate_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Delegate_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};


// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_mFDB8AD680C600072736579BBF5F38F7416396588_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, int32_t p0, const RuntimeMethod* method);
// !!0 UnityEngine.ScriptableObject::CreateInstance<System.Object>()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * ScriptableObject_CreateInstance_TisRuntimeObject_m7A8F75139352BA04C2EEC1D72D430FAC94C753DE_gshared (const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
extern "C" IL2CPP_METHOD_ATTR void List_1_Add_m6930161974C7504C80F52EC379EF012387D43138_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<System.Object>::Remove(!0)
extern "C" IL2CPP_METHOD_ATTR bool List_1_Remove_m908B647BB9F807676DACE34E3E73475C3C3751D4_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void System.Action`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void Action_1__ctor_mAFC7442D9D3CEC6701C3C5599F8CF12476095510_gshared (Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::ForEach(System.Action`1<!0>)
extern "C" IL2CPP_METHOD_ATTR void List_1_ForEach_m1FF0FB75577597F41D35ED6D7471CD96BA2CA65F_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method);

// Facebook.Unity.Settings.FacebookSettings Facebook.Unity.Settings.FacebookSettings::get_Instance()
extern "C" IL2CPP_METHOD_ATTR FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D * FacebookSettings_get_Instance_m5AA0850DCB8EB477CBA9777312CA4FDB9F6184DC (const RuntimeMethod* method);
// System.Void Facebook.Unity.Settings.FacebookSettings::SettingsChanged()
extern "C" IL2CPP_METHOD_ATTR void FacebookSettings_SettingsChanged_m63C85A637D0698D4AEA8F5B79D45609DE9FF722F (const RuntimeMethod* method);
// System.Collections.Generic.List`1<System.String> Facebook.Unity.Settings.FacebookSettings::get_AppIds()
extern "C" IL2CPP_METHOD_ATTR List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * FacebookSettings_get_AppIds_m0C3290B4AFF702DBB528DF47F991217B815C0C41 (const RuntimeMethod* method);
// System.Int32 Facebook.Unity.Settings.FacebookSettings::get_SelectedAppIndex()
extern "C" IL2CPP_METHOD_ATTR int32_t FacebookSettings_get_SelectedAppIndex_m3DBF263911DBC04F99D1606451F61FF1B66F2E8B (const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.String>::get_Item(System.Int32)
inline String_t* List_1_get_Item_mB739B0066E5F7EBDBA9978F24A73D26D4FAE5BED (List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * __this, int32_t p0, const RuntimeMethod* method)
{
	return ((  String_t* (*) (List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 *, int32_t, const RuntimeMethod*))List_1_get_Item_mFDB8AD680C600072736579BBF5F38F7416396588_gshared)(__this, p0, method);
}
// System.Collections.Generic.List`1<System.String> Facebook.Unity.Settings.FacebookSettings::get_ClientTokens()
extern "C" IL2CPP_METHOD_ATTR List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * FacebookSettings_get_ClientTokens_mF619C3FC47B4CDBF10C0CC8D6BDFF404AD277B87 (const RuntimeMethod* method);
// System.String Facebook.Unity.Settings.FacebookSettings::get_AppId()
extern "C" IL2CPP_METHOD_ATTR String_t* FacebookSettings_get_AppId_m7DCBDAD029BFA64A332B4D8C674ED53BD39A7FA3 (const RuntimeMethod* method);
// System.Int32 System.String::get_Length()
extern "C" IL2CPP_METHOD_ATTR int32_t String_get_Length_mD48C8A16A5CF1914F330DCE82D9BE15C3BEDD018 (String_t* __this, const RuntimeMethod* method);
// System.Boolean System.String::Equals(System.String)
extern "C" IL2CPP_METHOD_ATTR bool String_Equals_m9C4D78DFA0979504FE31429B64A4C26DF48020D1 (String_t* __this, String_t* p0, const RuntimeMethod* method);
// System.Boolean System.String::op_Inequality(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR bool String_op_Inequality_m0BD184A74F453A72376E81CC6CAEE2556B80493E (String_t* p0, String_t* p1, const RuntimeMethod* method);
// Facebook.Unity.Settings.FacebookSettings Facebook.Unity.Settings.FacebookSettings::get_NullableInstance()
extern "C" IL2CPP_METHOD_ATTR FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D * FacebookSettings_get_NullableInstance_mB9E0EA95ABDEC1B0AAE3A7808FD2F1E9965A98CB (const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR bool Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * p0, Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * p1, const RuntimeMethod* method);
// !!0 UnityEngine.ScriptableObject::CreateInstance<Facebook.Unity.Settings.FacebookSettings>()
inline FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D * ScriptableObject_CreateInstance_TisFacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_mE7E98CFB820A2EC3037FAEB169EC29B7161CD8E4 (const RuntimeMethod* method)
{
	return ((  FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D * (*) (const RuntimeMethod*))ScriptableObject_CreateInstance_TisRuntimeObject_m7A8F75139352BA04C2EEC1D72D430FAC94C753DE_gshared)(method);
}
// UnityEngine.Object UnityEngine.Resources::Load(System.String)
extern "C" IL2CPP_METHOD_ATTR Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * Resources_Load_mDBE6B83A74A52A1A6F5A68F7E5BC112DBB81B3C1 (String_t* p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<Facebook.Unity.Settings.FacebookSettings/OnChangeCallback>::Add(!0)
inline void List_1_Add_mC0B071C59951D54F8FE74C42DFECE1519E24F2EB (List_1_t4761B02A8CB42D5714C127D74C8BD8BCBD280D33 * __this, OnChangeCallback_t490B1A020446401D3A4B7030FA7A0D9AF521AFBB * p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t4761B02A8CB42D5714C127D74C8BD8BCBD280D33 *, OnChangeCallback_t490B1A020446401D3A4B7030FA7A0D9AF521AFBB *, const RuntimeMethod*))List_1_Add_m6930161974C7504C80F52EC379EF012387D43138_gshared)(__this, p0, method);
}
// System.Boolean System.Collections.Generic.List`1<Facebook.Unity.Settings.FacebookSettings/OnChangeCallback>::Remove(!0)
inline bool List_1_Remove_mCC3529501BE616F61AADD2B818FBC70B75905105 (List_1_t4761B02A8CB42D5714C127D74C8BD8BCBD280D33 * __this, OnChangeCallback_t490B1A020446401D3A4B7030FA7A0D9AF521AFBB * p0, const RuntimeMethod* method)
{
	return ((  bool (*) (List_1_t4761B02A8CB42D5714C127D74C8BD8BCBD280D33 *, OnChangeCallback_t490B1A020446401D3A4B7030FA7A0D9AF521AFBB *, const RuntimeMethod*))List_1_Remove_m908B647BB9F807676DACE34E3E73475C3C3751D4_gshared)(__this, p0, method);
}
// System.Void System.Action`1<Facebook.Unity.Settings.FacebookSettings/OnChangeCallback>::.ctor(System.Object,System.IntPtr)
inline void Action_1__ctor_mCDDBCF9D6DB0D55DA31F3EB00A54CCFDCDFC7064 (Action_1_t605040A52441734E3EA5810595C95A0D0568C238 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method)
{
	((  void (*) (Action_1_t605040A52441734E3EA5810595C95A0D0568C238 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Action_1__ctor_mAFC7442D9D3CEC6701C3C5599F8CF12476095510_gshared)(__this, p0, p1, method);
}
// System.Void System.Collections.Generic.List`1<Facebook.Unity.Settings.FacebookSettings/OnChangeCallback>::ForEach(System.Action`1<!0>)
inline void List_1_ForEach_mA6A810C3D8B78C414C9FE29C629B0AB7506D6DB3 (List_1_t4761B02A8CB42D5714C127D74C8BD8BCBD280D33 * __this, Action_1_t605040A52441734E3EA5810595C95A0D0568C238 * p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t4761B02A8CB42D5714C127D74C8BD8BCBD280D33 *, Action_1_t605040A52441734E3EA5810595C95A0D0568C238 *, const RuntimeMethod*))List_1_ForEach_m1FF0FB75577597F41D35ED6D7471CD96BA2CA65F_gshared)(__this, p0, method);
}
// System.Void System.Collections.Generic.List`1<System.String>::.ctor()
inline void List_1__ctor_mDA22758D73530683C950C5CCF39BDB4E7E1F3F06 (List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 *, const RuntimeMethod*))List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<System.String>::Add(!0)
inline void List_1_Add_mA348FA1140766465189459D25B01EB179001DE83 (List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * __this, String_t* p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 *, String_t*, const RuntimeMethod*))List_1_Add_m6930161974C7504C80F52EC379EF012387D43138_gshared)(__this, p0, method);
}
// System.Void System.Collections.Generic.List`1<Facebook.Unity.Settings.FacebookSettings/UrlSchemes>::.ctor()
inline void List_1__ctor_m193A1CCAFC83743EFB9ADBC8FDB87850CD0C9C7A (List_1_tEBAA293BF803F2D17E20E8B6FE34EF3792F6FA6B * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_tEBAA293BF803F2D17E20E8B6FE34EF3792F6FA6B *, const RuntimeMethod*))List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_gshared)(__this, method);
}
// System.Void Facebook.Unity.Settings.FacebookSettings/UrlSchemes::.ctor(System.Collections.Generic.List`1<System.String>)
extern "C" IL2CPP_METHOD_ATTR void UrlSchemes__ctor_mD2CB85DBE160B9144012A889732A4CF3E6FBBA62 (UrlSchemes_t7B8D8455C2FE4B28F5F677FFAE86E14077D94D8C * __this, List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___schemes0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<Facebook.Unity.Settings.FacebookSettings/UrlSchemes>::Add(!0)
inline void List_1_Add_m9B9A17315AE28254D75EDF0BDD68D83CB3B8B4F2 (List_1_tEBAA293BF803F2D17E20E8B6FE34EF3792F6FA6B * __this, UrlSchemes_t7B8D8455C2FE4B28F5F677FFAE86E14077D94D8C * p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_tEBAA293BF803F2D17E20E8B6FE34EF3792F6FA6B *, UrlSchemes_t7B8D8455C2FE4B28F5F677FFAE86E14077D94D8C *, const RuntimeMethod*))List_1_Add_m6930161974C7504C80F52EC379EF012387D43138_gshared)(__this, p0, method);
}
// System.Void UnityEngine.ScriptableObject::.ctor()
extern "C" IL2CPP_METHOD_ATTR void ScriptableObject__ctor_m6E2B3821A4A361556FC12E9B1C71E1D5DC002C5B (ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<Facebook.Unity.Settings.FacebookSettings/OnChangeCallback>::.ctor()
inline void List_1__ctor_mA011B26BD878095268926AAE1523A7B080A39A20 (List_1_t4761B02A8CB42D5714C127D74C8BD8BCBD280D33 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t4761B02A8CB42D5714C127D74C8BD8BCBD280D33 *, const RuntimeMethod*))List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_gshared)(__this, method);
}
// System.Void Facebook.Unity.Settings.FacebookSettings/<>c::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m53CB9A84121F3B2E66EB8CEFC7608554F9A38A5C (U3CU3Ec_t40659AD935CCBCC35589D12303DF7D93D8B4ABA8 * __this, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void Facebook.Unity.Settings.FacebookSettings/OnChangeCallback::Invoke()
extern "C" IL2CPP_METHOD_ATTR void OnChangeCallback_Invoke_mB8A883493EA3BE29A9D2F841CF981794A0D1FB83 (OnChangeCallback_t490B1A020446401D3A4B7030FA7A0D9AF521AFBB * __this, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Int32 Facebook.Unity.Settings.FacebookSettings::get_SelectedAppIndex()
extern "C" IL2CPP_METHOD_ATTR int32_t FacebookSettings_get_SelectedAppIndex_m3DBF263911DBC04F99D1606451F61FF1B66F2E8B (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_get_SelectedAppIndex_m3DBF263911DBC04F99D1606451F61FF1B66F2E8B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_il2cpp_TypeInfo_var);
		FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D * L_0 = FacebookSettings_get_Instance_m5AA0850DCB8EB477CBA9777312CA4FDB9F6184DC(/*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = L_0->get_selectedAppIndex_9();
		return L_1;
	}
}
// System.Void Facebook.Unity.Settings.FacebookSettings::set_SelectedAppIndex(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void FacebookSettings_set_SelectedAppIndex_m9A8D2435E395E95F8D08FB661C6B10569A44B3AF (int32_t ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_set_SelectedAppIndex_m9A8D2435E395E95F8D08FB661C6B10569A44B3AF_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_il2cpp_TypeInfo_var);
		FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D * L_0 = FacebookSettings_get_Instance_m5AA0850DCB8EB477CBA9777312CA4FDB9F6184DC(/*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = L_0->get_selectedAppIndex_9();
		int32_t L_2 = ___value0;
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_001d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_il2cpp_TypeInfo_var);
		FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D * L_3 = FacebookSettings_get_Instance_m5AA0850DCB8EB477CBA9777312CA4FDB9F6184DC(/*hidden argument*/NULL);
		int32_t L_4 = ___value0;
		NullCheck(L_3);
		L_3->set_selectedAppIndex_9(L_4);
		FacebookSettings_SettingsChanged_m63C85A637D0698D4AEA8F5B79D45609DE9FF722F(/*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// System.Collections.Generic.List`1<System.String> Facebook.Unity.Settings.FacebookSettings::get_AppIds()
extern "C" IL2CPP_METHOD_ATTR List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * FacebookSettings_get_AppIds_m0C3290B4AFF702DBB528DF47F991217B815C0C41 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_get_AppIds_m0C3290B4AFF702DBB528DF47F991217B815C0C41_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_il2cpp_TypeInfo_var);
		FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D * L_0 = FacebookSettings_get_Instance_m5AA0850DCB8EB477CBA9777312CA4FDB9F6184DC(/*hidden argument*/NULL);
		NullCheck(L_0);
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_1 = L_0->get_appIds_11();
		return L_1;
	}
}
// System.Void Facebook.Unity.Settings.FacebookSettings::set_AppIds(System.Collections.Generic.List`1<System.String>)
extern "C" IL2CPP_METHOD_ATTR void FacebookSettings_set_AppIds_m5CC2EFB0DB654A596A8C5963B8420F0197F01BDA (List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_set_AppIds_m5CC2EFB0DB654A596A8C5963B8420F0197F01BDA_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_il2cpp_TypeInfo_var);
		FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D * L_0 = FacebookSettings_get_Instance_m5AA0850DCB8EB477CBA9777312CA4FDB9F6184DC(/*hidden argument*/NULL);
		NullCheck(L_0);
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_1 = L_0->get_appIds_11();
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_2 = ___value0;
		if ((((RuntimeObject*)(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 *)L_1) == ((RuntimeObject*)(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 *)L_2)))
		{
			goto IL_001d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_il2cpp_TypeInfo_var);
		FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D * L_3 = FacebookSettings_get_Instance_m5AA0850DCB8EB477CBA9777312CA4FDB9F6184DC(/*hidden argument*/NULL);
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_4 = ___value0;
		NullCheck(L_3);
		L_3->set_appIds_11(L_4);
		FacebookSettings_SettingsChanged_m63C85A637D0698D4AEA8F5B79D45609DE9FF722F(/*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// System.Collections.Generic.List`1<System.String> Facebook.Unity.Settings.FacebookSettings::get_AppLabels()
extern "C" IL2CPP_METHOD_ATTR List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * FacebookSettings_get_AppLabels_mAF6B70ED254BD0E1C663C934C13C1FC627F0D2EC (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_get_AppLabels_mAF6B70ED254BD0E1C663C934C13C1FC627F0D2EC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_il2cpp_TypeInfo_var);
		FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D * L_0 = FacebookSettings_get_Instance_m5AA0850DCB8EB477CBA9777312CA4FDB9F6184DC(/*hidden argument*/NULL);
		NullCheck(L_0);
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_1 = L_0->get_appLabels_12();
		return L_1;
	}
}
// System.Void Facebook.Unity.Settings.FacebookSettings::set_AppLabels(System.Collections.Generic.List`1<System.String>)
extern "C" IL2CPP_METHOD_ATTR void FacebookSettings_set_AppLabels_m1797CCEC7E8D390136D50248A4950FB1BC6E9F7B (List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_set_AppLabels_m1797CCEC7E8D390136D50248A4950FB1BC6E9F7B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_il2cpp_TypeInfo_var);
		FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D * L_0 = FacebookSettings_get_Instance_m5AA0850DCB8EB477CBA9777312CA4FDB9F6184DC(/*hidden argument*/NULL);
		NullCheck(L_0);
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_1 = L_0->get_appLabels_12();
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_2 = ___value0;
		if ((((RuntimeObject*)(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 *)L_1) == ((RuntimeObject*)(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 *)L_2)))
		{
			goto IL_001d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_il2cpp_TypeInfo_var);
		FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D * L_3 = FacebookSettings_get_Instance_m5AA0850DCB8EB477CBA9777312CA4FDB9F6184DC(/*hidden argument*/NULL);
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_4 = ___value0;
		NullCheck(L_3);
		L_3->set_appLabels_12(L_4);
		FacebookSettings_SettingsChanged_m63C85A637D0698D4AEA8F5B79D45609DE9FF722F(/*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// System.Collections.Generic.List`1<System.String> Facebook.Unity.Settings.FacebookSettings::get_ClientTokens()
extern "C" IL2CPP_METHOD_ATTR List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * FacebookSettings_get_ClientTokens_mF619C3FC47B4CDBF10C0CC8D6BDFF404AD277B87 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_get_ClientTokens_mF619C3FC47B4CDBF10C0CC8D6BDFF404AD277B87_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_il2cpp_TypeInfo_var);
		FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D * L_0 = FacebookSettings_get_Instance_m5AA0850DCB8EB477CBA9777312CA4FDB9F6184DC(/*hidden argument*/NULL);
		NullCheck(L_0);
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_1 = L_0->get_clientTokens_10();
		return L_1;
	}
}
// System.Void Facebook.Unity.Settings.FacebookSettings::set_ClientTokens(System.Collections.Generic.List`1<System.String>)
extern "C" IL2CPP_METHOD_ATTR void FacebookSettings_set_ClientTokens_m96C8C6D128C67DD90B3D49709ABE54AA28BEE677 (List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_set_ClientTokens_m96C8C6D128C67DD90B3D49709ABE54AA28BEE677_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_il2cpp_TypeInfo_var);
		FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D * L_0 = FacebookSettings_get_Instance_m5AA0850DCB8EB477CBA9777312CA4FDB9F6184DC(/*hidden argument*/NULL);
		NullCheck(L_0);
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_1 = L_0->get_clientTokens_10();
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_2 = ___value0;
		if ((((RuntimeObject*)(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 *)L_1) == ((RuntimeObject*)(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 *)L_2)))
		{
			goto IL_001d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_il2cpp_TypeInfo_var);
		FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D * L_3 = FacebookSettings_get_Instance_m5AA0850DCB8EB477CBA9777312CA4FDB9F6184DC(/*hidden argument*/NULL);
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_4 = ___value0;
		NullCheck(L_3);
		L_3->set_clientTokens_10(L_4);
		FacebookSettings_SettingsChanged_m63C85A637D0698D4AEA8F5B79D45609DE9FF722F(/*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// System.String Facebook.Unity.Settings.FacebookSettings::get_AppId()
extern "C" IL2CPP_METHOD_ATTR String_t* FacebookSettings_get_AppId_m7DCBDAD029BFA64A332B4D8C674ED53BD39A7FA3 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_get_AppId_m7DCBDAD029BFA64A332B4D8C674ED53BD39A7FA3_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_il2cpp_TypeInfo_var);
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_0 = FacebookSettings_get_AppIds_m0C3290B4AFF702DBB528DF47F991217B815C0C41(/*hidden argument*/NULL);
		int32_t L_1 = FacebookSettings_get_SelectedAppIndex_m3DBF263911DBC04F99D1606451F61FF1B66F2E8B(/*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_2 = List_1_get_Item_mB739B0066E5F7EBDBA9978F24A73D26D4FAE5BED(L_0, L_1, /*hidden argument*/List_1_get_Item_mB739B0066E5F7EBDBA9978F24A73D26D4FAE5BED_RuntimeMethod_var);
		return L_2;
	}
}
// System.String Facebook.Unity.Settings.FacebookSettings::get_ClientToken()
extern "C" IL2CPP_METHOD_ATTR String_t* FacebookSettings_get_ClientToken_mFE2350548E2BFFDE25A0792A4D251B2E8573D1EA (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_get_ClientToken_mFE2350548E2BFFDE25A0792A4D251B2E8573D1EA_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_il2cpp_TypeInfo_var);
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_0 = FacebookSettings_get_ClientTokens_mF619C3FC47B4CDBF10C0CC8D6BDFF404AD277B87(/*hidden argument*/NULL);
		int32_t L_1 = FacebookSettings_get_SelectedAppIndex_m3DBF263911DBC04F99D1606451F61FF1B66F2E8B(/*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_2 = List_1_get_Item_mB739B0066E5F7EBDBA9978F24A73D26D4FAE5BED(L_0, L_1, /*hidden argument*/List_1_get_Item_mB739B0066E5F7EBDBA9978F24A73D26D4FAE5BED_RuntimeMethod_var);
		return L_2;
	}
}
// System.Boolean Facebook.Unity.Settings.FacebookSettings::get_IsValidAppId()
extern "C" IL2CPP_METHOD_ATTR bool FacebookSettings_get_IsValidAppId_mFB768FAD141DDF4B5CBD997CF5F7AF49BA945843 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_get_IsValidAppId_mFB768FAD141DDF4B5CBD997CF5F7AF49BA945843_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_il2cpp_TypeInfo_var);
		String_t* L_0 = FacebookSettings_get_AppId_m7DCBDAD029BFA64A332B4D8C674ED53BD39A7FA3(/*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0027;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_il2cpp_TypeInfo_var);
		String_t* L_1 = FacebookSettings_get_AppId_m7DCBDAD029BFA64A332B4D8C674ED53BD39A7FA3(/*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_mD48C8A16A5CF1914F330DCE82D9BE15C3BEDD018(L_1, /*hidden argument*/NULL);
		if ((((int32_t)L_2) <= ((int32_t)0)))
		{
			goto IL_0027;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_il2cpp_TypeInfo_var);
		String_t* L_3 = FacebookSettings_get_AppId_m7DCBDAD029BFA64A332B4D8C674ED53BD39A7FA3(/*hidden argument*/NULL);
		NullCheck(L_3);
		bool L_4 = String_Equals_m9C4D78DFA0979504FE31429B64A4C26DF48020D1(L_3, _stringLiteralB6589FC6AB0DC82CF12099D1C2D40AB994E8410C, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_4) == ((int32_t)0))? 1 : 0);
	}

IL_0027:
	{
		return (bool)0;
	}
}
// System.Boolean Facebook.Unity.Settings.FacebookSettings::get_Cookie()
extern "C" IL2CPP_METHOD_ATTR bool FacebookSettings_get_Cookie_mE587EAFF364E8C6E85B96CD232B6492A390AE20D (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_get_Cookie_mE587EAFF364E8C6E85B96CD232B6492A390AE20D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_il2cpp_TypeInfo_var);
		FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D * L_0 = FacebookSettings_get_Instance_m5AA0850DCB8EB477CBA9777312CA4FDB9F6184DC(/*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = L_0->get_cookie_13();
		return L_1;
	}
}
// System.Void Facebook.Unity.Settings.FacebookSettings::set_Cookie(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void FacebookSettings_set_Cookie_m37B989712829A9FC6CCFAB2803D0FC9580862ED7 (bool ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_set_Cookie_m37B989712829A9FC6CCFAB2803D0FC9580862ED7_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_il2cpp_TypeInfo_var);
		FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D * L_0 = FacebookSettings_get_Instance_m5AA0850DCB8EB477CBA9777312CA4FDB9F6184DC(/*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = L_0->get_cookie_13();
		bool L_2 = ___value0;
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_001d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_il2cpp_TypeInfo_var);
		FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D * L_3 = FacebookSettings_get_Instance_m5AA0850DCB8EB477CBA9777312CA4FDB9F6184DC(/*hidden argument*/NULL);
		bool L_4 = ___value0;
		NullCheck(L_3);
		L_3->set_cookie_13(L_4);
		FacebookSettings_SettingsChanged_m63C85A637D0698D4AEA8F5B79D45609DE9FF722F(/*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// System.Boolean Facebook.Unity.Settings.FacebookSettings::get_Logging()
extern "C" IL2CPP_METHOD_ATTR bool FacebookSettings_get_Logging_mD29CBD1ADEA9616010596389836C17A03EE00F01 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_get_Logging_mD29CBD1ADEA9616010596389836C17A03EE00F01_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_il2cpp_TypeInfo_var);
		FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D * L_0 = FacebookSettings_get_Instance_m5AA0850DCB8EB477CBA9777312CA4FDB9F6184DC(/*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = L_0->get_logging_14();
		return L_1;
	}
}
// System.Void Facebook.Unity.Settings.FacebookSettings::set_Logging(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void FacebookSettings_set_Logging_m74F5536272605B1684A3416D35E3EFB1068DF207 (bool ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_set_Logging_m74F5536272605B1684A3416D35E3EFB1068DF207_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_il2cpp_TypeInfo_var);
		FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D * L_0 = FacebookSettings_get_Instance_m5AA0850DCB8EB477CBA9777312CA4FDB9F6184DC(/*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = L_0->get_logging_14();
		bool L_2 = ___value0;
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_001d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_il2cpp_TypeInfo_var);
		FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D * L_3 = FacebookSettings_get_Instance_m5AA0850DCB8EB477CBA9777312CA4FDB9F6184DC(/*hidden argument*/NULL);
		bool L_4 = ___value0;
		NullCheck(L_3);
		L_3->set_logging_14(L_4);
		FacebookSettings_SettingsChanged_m63C85A637D0698D4AEA8F5B79D45609DE9FF722F(/*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// System.Boolean Facebook.Unity.Settings.FacebookSettings::get_Status()
extern "C" IL2CPP_METHOD_ATTR bool FacebookSettings_get_Status_mA8ED29022B4DC50AA6BAA74276761F50FDD4118C (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_get_Status_mA8ED29022B4DC50AA6BAA74276761F50FDD4118C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_il2cpp_TypeInfo_var);
		FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D * L_0 = FacebookSettings_get_Instance_m5AA0850DCB8EB477CBA9777312CA4FDB9F6184DC(/*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = L_0->get_status_15();
		return L_1;
	}
}
// System.Void Facebook.Unity.Settings.FacebookSettings::set_Status(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void FacebookSettings_set_Status_mA9AE0A65CC579F73870E1B8A69A2E04F55FFFE8A (bool ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_set_Status_mA9AE0A65CC579F73870E1B8A69A2E04F55FFFE8A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_il2cpp_TypeInfo_var);
		FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D * L_0 = FacebookSettings_get_Instance_m5AA0850DCB8EB477CBA9777312CA4FDB9F6184DC(/*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = L_0->get_status_15();
		bool L_2 = ___value0;
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_001d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_il2cpp_TypeInfo_var);
		FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D * L_3 = FacebookSettings_get_Instance_m5AA0850DCB8EB477CBA9777312CA4FDB9F6184DC(/*hidden argument*/NULL);
		bool L_4 = ___value0;
		NullCheck(L_3);
		L_3->set_status_15(L_4);
		FacebookSettings_SettingsChanged_m63C85A637D0698D4AEA8F5B79D45609DE9FF722F(/*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// System.Boolean Facebook.Unity.Settings.FacebookSettings::get_Xfbml()
extern "C" IL2CPP_METHOD_ATTR bool FacebookSettings_get_Xfbml_m984DD60D50FDC4B94BBA6EFA20D068E9E5C904EC (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_get_Xfbml_m984DD60D50FDC4B94BBA6EFA20D068E9E5C904EC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_il2cpp_TypeInfo_var);
		FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D * L_0 = FacebookSettings_get_Instance_m5AA0850DCB8EB477CBA9777312CA4FDB9F6184DC(/*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = L_0->get_xfbml_16();
		return L_1;
	}
}
// System.Void Facebook.Unity.Settings.FacebookSettings::set_Xfbml(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void FacebookSettings_set_Xfbml_mF50D4190258CA2967A33779281A46F7E75649947 (bool ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_set_Xfbml_mF50D4190258CA2967A33779281A46F7E75649947_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_il2cpp_TypeInfo_var);
		FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D * L_0 = FacebookSettings_get_Instance_m5AA0850DCB8EB477CBA9777312CA4FDB9F6184DC(/*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = L_0->get_xfbml_16();
		bool L_2 = ___value0;
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_001d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_il2cpp_TypeInfo_var);
		FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D * L_3 = FacebookSettings_get_Instance_m5AA0850DCB8EB477CBA9777312CA4FDB9F6184DC(/*hidden argument*/NULL);
		bool L_4 = ___value0;
		NullCheck(L_3);
		L_3->set_xfbml_16(L_4);
		FacebookSettings_SettingsChanged_m63C85A637D0698D4AEA8F5B79D45609DE9FF722F(/*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// System.String Facebook.Unity.Settings.FacebookSettings::get_IosURLSuffix()
extern "C" IL2CPP_METHOD_ATTR String_t* FacebookSettings_get_IosURLSuffix_m34E46490711FF5DB738006AFA012C5DFE88CDCAC (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_get_IosURLSuffix_m34E46490711FF5DB738006AFA012C5DFE88CDCAC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_il2cpp_TypeInfo_var);
		FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D * L_0 = FacebookSettings_get_Instance_m5AA0850DCB8EB477CBA9777312CA4FDB9F6184DC(/*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_1 = L_0->get_iosURLSuffix_18();
		return L_1;
	}
}
// System.Void Facebook.Unity.Settings.FacebookSettings::set_IosURLSuffix(System.String)
extern "C" IL2CPP_METHOD_ATTR void FacebookSettings_set_IosURLSuffix_m037025957C9BC65425347C888DC828280D5DA7EE (String_t* ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_set_IosURLSuffix_m037025957C9BC65425347C888DC828280D5DA7EE_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_il2cpp_TypeInfo_var);
		FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D * L_0 = FacebookSettings_get_Instance_m5AA0850DCB8EB477CBA9777312CA4FDB9F6184DC(/*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_1 = L_0->get_iosURLSuffix_18();
		String_t* L_2 = ___value0;
		bool L_3 = String_op_Inequality_m0BD184A74F453A72376E81CC6CAEE2556B80493E(L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0022;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_il2cpp_TypeInfo_var);
		FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D * L_4 = FacebookSettings_get_Instance_m5AA0850DCB8EB477CBA9777312CA4FDB9F6184DC(/*hidden argument*/NULL);
		String_t* L_5 = ___value0;
		NullCheck(L_4);
		L_4->set_iosURLSuffix_18(L_5);
		FacebookSettings_SettingsChanged_m63C85A637D0698D4AEA8F5B79D45609DE9FF722F(/*hidden argument*/NULL);
	}

IL_0022:
	{
		return;
	}
}
// System.String Facebook.Unity.Settings.FacebookSettings::get_ChannelUrl()
extern "C" IL2CPP_METHOD_ATTR String_t* FacebookSettings_get_ChannelUrl_m5434BFEFBF71FE2A27853952FA5618250E850830 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_get_ChannelUrl_m5434BFEFBF71FE2A27853952FA5618250E850830_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		return _stringLiteral3D7479543F221F46EE46D1F7AB8E7AD53734F2B9;
	}
}
// System.Boolean Facebook.Unity.Settings.FacebookSettings::get_FrictionlessRequests()
extern "C" IL2CPP_METHOD_ATTR bool FacebookSettings_get_FrictionlessRequests_m5B78574AE3A72513F3E04A8AB754CF41F1B14767 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_get_FrictionlessRequests_m5B78574AE3A72513F3E04A8AB754CF41F1B14767_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_il2cpp_TypeInfo_var);
		FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D * L_0 = FacebookSettings_get_Instance_m5AA0850DCB8EB477CBA9777312CA4FDB9F6184DC(/*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = L_0->get_frictionlessRequests_17();
		return L_1;
	}
}
// System.Void Facebook.Unity.Settings.FacebookSettings::set_FrictionlessRequests(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void FacebookSettings_set_FrictionlessRequests_mB59F5CB1B0246E51C5A77DCD542C6295AE2726A0 (bool ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_set_FrictionlessRequests_mB59F5CB1B0246E51C5A77DCD542C6295AE2726A0_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_il2cpp_TypeInfo_var);
		FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D * L_0 = FacebookSettings_get_Instance_m5AA0850DCB8EB477CBA9777312CA4FDB9F6184DC(/*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = L_0->get_frictionlessRequests_17();
		bool L_2 = ___value0;
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_001d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_il2cpp_TypeInfo_var);
		FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D * L_3 = FacebookSettings_get_Instance_m5AA0850DCB8EB477CBA9777312CA4FDB9F6184DC(/*hidden argument*/NULL);
		bool L_4 = ___value0;
		NullCheck(L_3);
		L_3->set_frictionlessRequests_17(L_4);
		FacebookSettings_SettingsChanged_m63C85A637D0698D4AEA8F5B79D45609DE9FF722F(/*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// System.Collections.Generic.List`1<Facebook.Unity.Settings.FacebookSettings_UrlSchemes> Facebook.Unity.Settings.FacebookSettings::get_AppLinkSchemes()
extern "C" IL2CPP_METHOD_ATTR List_1_tEBAA293BF803F2D17E20E8B6FE34EF3792F6FA6B * FacebookSettings_get_AppLinkSchemes_mFBA649443DFCA429B12D869987385980B5A6853C (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_get_AppLinkSchemes_mFBA649443DFCA429B12D869987385980B5A6853C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_il2cpp_TypeInfo_var);
		FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D * L_0 = FacebookSettings_get_Instance_m5AA0850DCB8EB477CBA9777312CA4FDB9F6184DC(/*hidden argument*/NULL);
		NullCheck(L_0);
		List_1_tEBAA293BF803F2D17E20E8B6FE34EF3792F6FA6B * L_1 = L_0->get_appLinkSchemes_19();
		return L_1;
	}
}
// System.Void Facebook.Unity.Settings.FacebookSettings::set_AppLinkSchemes(System.Collections.Generic.List`1<Facebook.Unity.Settings.FacebookSettings_UrlSchemes>)
extern "C" IL2CPP_METHOD_ATTR void FacebookSettings_set_AppLinkSchemes_mD57C8D0D5CD6C7286F9FBF2A745D3B789DE7DDD9 (List_1_tEBAA293BF803F2D17E20E8B6FE34EF3792F6FA6B * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_set_AppLinkSchemes_mD57C8D0D5CD6C7286F9FBF2A745D3B789DE7DDD9_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_il2cpp_TypeInfo_var);
		FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D * L_0 = FacebookSettings_get_Instance_m5AA0850DCB8EB477CBA9777312CA4FDB9F6184DC(/*hidden argument*/NULL);
		NullCheck(L_0);
		List_1_tEBAA293BF803F2D17E20E8B6FE34EF3792F6FA6B * L_1 = L_0->get_appLinkSchemes_19();
		List_1_tEBAA293BF803F2D17E20E8B6FE34EF3792F6FA6B * L_2 = ___value0;
		if ((((RuntimeObject*)(List_1_tEBAA293BF803F2D17E20E8B6FE34EF3792F6FA6B *)L_1) == ((RuntimeObject*)(List_1_tEBAA293BF803F2D17E20E8B6FE34EF3792F6FA6B *)L_2)))
		{
			goto IL_001d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_il2cpp_TypeInfo_var);
		FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D * L_3 = FacebookSettings_get_Instance_m5AA0850DCB8EB477CBA9777312CA4FDB9F6184DC(/*hidden argument*/NULL);
		List_1_tEBAA293BF803F2D17E20E8B6FE34EF3792F6FA6B * L_4 = ___value0;
		NullCheck(L_3);
		L_3->set_appLinkSchemes_19(L_4);
		FacebookSettings_SettingsChanged_m63C85A637D0698D4AEA8F5B79D45609DE9FF722F(/*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// System.String Facebook.Unity.Settings.FacebookSettings::get_UploadAccessToken()
extern "C" IL2CPP_METHOD_ATTR String_t* FacebookSettings_get_UploadAccessToken_m99C1236A489B23B374182AE379A4AF1AEB574FCB (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_get_UploadAccessToken_m99C1236A489B23B374182AE379A4AF1AEB574FCB_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_il2cpp_TypeInfo_var);
		FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D * L_0 = FacebookSettings_get_Instance_m5AA0850DCB8EB477CBA9777312CA4FDB9F6184DC(/*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_1 = L_0->get_uploadAccessToken_20();
		return L_1;
	}
}
// System.Void Facebook.Unity.Settings.FacebookSettings::set_UploadAccessToken(System.String)
extern "C" IL2CPP_METHOD_ATTR void FacebookSettings_set_UploadAccessToken_mA405A29F8D46797B5314E19C1991F40851871046 (String_t* ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_set_UploadAccessToken_mA405A29F8D46797B5314E19C1991F40851871046_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_il2cpp_TypeInfo_var);
		FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D * L_0 = FacebookSettings_get_Instance_m5AA0850DCB8EB477CBA9777312CA4FDB9F6184DC(/*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_1 = L_0->get_uploadAccessToken_20();
		String_t* L_2 = ___value0;
		bool L_3 = String_op_Inequality_m0BD184A74F453A72376E81CC6CAEE2556B80493E(L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0022;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_il2cpp_TypeInfo_var);
		FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D * L_4 = FacebookSettings_get_Instance_m5AA0850DCB8EB477CBA9777312CA4FDB9F6184DC(/*hidden argument*/NULL);
		String_t* L_5 = ___value0;
		NullCheck(L_4);
		L_4->set_uploadAccessToken_20(L_5);
		FacebookSettings_SettingsChanged_m63C85A637D0698D4AEA8F5B79D45609DE9FF722F(/*hidden argument*/NULL);
	}

IL_0022:
	{
		return;
	}
}
// System.Boolean Facebook.Unity.Settings.FacebookSettings::get_AutoLogAppEventsEnabled()
extern "C" IL2CPP_METHOD_ATTR bool FacebookSettings_get_AutoLogAppEventsEnabled_mC273B9B56076AB18E10A4A2D275FD21B71E16A38 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_get_AutoLogAppEventsEnabled_mC273B9B56076AB18E10A4A2D275FD21B71E16A38_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_il2cpp_TypeInfo_var);
		FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D * L_0 = FacebookSettings_get_Instance_m5AA0850DCB8EB477CBA9777312CA4FDB9F6184DC(/*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = L_0->get_autoLogAppEventsEnabled_21();
		return L_1;
	}
}
// System.Void Facebook.Unity.Settings.FacebookSettings::set_AutoLogAppEventsEnabled(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void FacebookSettings_set_AutoLogAppEventsEnabled_m870F388BC58518A161C1B29D849DD51F2712D1E8 (bool ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_set_AutoLogAppEventsEnabled_m870F388BC58518A161C1B29D849DD51F2712D1E8_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_il2cpp_TypeInfo_var);
		FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D * L_0 = FacebookSettings_get_Instance_m5AA0850DCB8EB477CBA9777312CA4FDB9F6184DC(/*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = L_0->get_autoLogAppEventsEnabled_21();
		bool L_2 = ___value0;
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_001d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_il2cpp_TypeInfo_var);
		FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D * L_3 = FacebookSettings_get_Instance_m5AA0850DCB8EB477CBA9777312CA4FDB9F6184DC(/*hidden argument*/NULL);
		bool L_4 = ___value0;
		NullCheck(L_3);
		L_3->set_autoLogAppEventsEnabled_21(L_4);
		FacebookSettings_SettingsChanged_m63C85A637D0698D4AEA8F5B79D45609DE9FF722F(/*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// System.Boolean Facebook.Unity.Settings.FacebookSettings::get_AdvertiserIDCollectionEnabled()
extern "C" IL2CPP_METHOD_ATTR bool FacebookSettings_get_AdvertiserIDCollectionEnabled_m4DD83E89DA98EE8995722179C7AC622D44CFDAF3 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_get_AdvertiserIDCollectionEnabled_m4DD83E89DA98EE8995722179C7AC622D44CFDAF3_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_il2cpp_TypeInfo_var);
		FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D * L_0 = FacebookSettings_get_Instance_m5AA0850DCB8EB477CBA9777312CA4FDB9F6184DC(/*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = L_0->get_advertiserIDCollectionEnabled_22();
		return L_1;
	}
}
// System.Void Facebook.Unity.Settings.FacebookSettings::set_AdvertiserIDCollectionEnabled(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void FacebookSettings_set_AdvertiserIDCollectionEnabled_mA7BFBD8F0AAAC7415D54EA0D01E15CAAF7F68A0C (bool ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_set_AdvertiserIDCollectionEnabled_mA7BFBD8F0AAAC7415D54EA0D01E15CAAF7F68A0C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_il2cpp_TypeInfo_var);
		FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D * L_0 = FacebookSettings_get_Instance_m5AA0850DCB8EB477CBA9777312CA4FDB9F6184DC(/*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = L_0->get_advertiserIDCollectionEnabled_22();
		bool L_2 = ___value0;
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_001d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_il2cpp_TypeInfo_var);
		FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D * L_3 = FacebookSettings_get_Instance_m5AA0850DCB8EB477CBA9777312CA4FDB9F6184DC(/*hidden argument*/NULL);
		bool L_4 = ___value0;
		NullCheck(L_3);
		L_3->set_advertiserIDCollectionEnabled_22(L_4);
		FacebookSettings_SettingsChanged_m63C85A637D0698D4AEA8F5B79D45609DE9FF722F(/*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// Facebook.Unity.Settings.FacebookSettings Facebook.Unity.Settings.FacebookSettings::get_Instance()
extern "C" IL2CPP_METHOD_ATTR FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D * FacebookSettings_get_Instance_m5AA0850DCB8EB477CBA9777312CA4FDB9F6184DC (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_get_Instance_m5AA0850DCB8EB477CBA9777312CA4FDB9F6184DC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_il2cpp_TypeInfo_var);
		FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D * L_0 = FacebookSettings_get_NullableInstance_mB9E0EA95ABDEC1B0AAE3A7808FD2F1E9965A98CB(/*hidden argument*/NULL);
		((FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_StaticFields*)il2cpp_codegen_static_fields_for(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_il2cpp_TypeInfo_var))->set_instance_8(L_0);
		FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D * L_1 = ((FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_StaticFields*)il2cpp_codegen_static_fields_for(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_il2cpp_TypeInfo_var))->get_instance_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_1, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0021;
		}
	}
	{
		FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D * L_3 = ScriptableObject_CreateInstance_TisFacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_mE7E98CFB820A2EC3037FAEB169EC29B7161CD8E4(/*hidden argument*/ScriptableObject_CreateInstance_TisFacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_mE7E98CFB820A2EC3037FAEB169EC29B7161CD8E4_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_il2cpp_TypeInfo_var);
		((FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_StaticFields*)il2cpp_codegen_static_fields_for(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_il2cpp_TypeInfo_var))->set_instance_8(L_3);
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_il2cpp_TypeInfo_var);
		FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D * L_4 = ((FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_StaticFields*)il2cpp_codegen_static_fields_for(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_il2cpp_TypeInfo_var))->get_instance_8();
		return L_4;
	}
}
// Facebook.Unity.Settings.FacebookSettings Facebook.Unity.Settings.FacebookSettings::get_NullableInstance()
extern "C" IL2CPP_METHOD_ATTR FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D * FacebookSettings_get_NullableInstance_mB9E0EA95ABDEC1B0AAE3A7808FD2F1E9965A98CB (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_get_NullableInstance_mB9E0EA95ABDEC1B0AAE3A7808FD2F1E9965A98CB_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_il2cpp_TypeInfo_var);
		FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D * L_0 = ((FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_StaticFields*)il2cpp_codegen_static_fields_for(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_il2cpp_TypeInfo_var))->get_instance_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_0, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * L_2 = Resources_Load_mDBE6B83A74A52A1A6F5A68F7E5BC112DBB81B3C1(_stringLiteral11A3B430794567143B2D380848C73908507F1D59, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_il2cpp_TypeInfo_var);
		((FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_StaticFields*)il2cpp_codegen_static_fields_for(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_il2cpp_TypeInfo_var))->set_instance_8(((FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D *)IsInstClass((RuntimeObject*)L_2, FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_il2cpp_TypeInfo_var)));
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_il2cpp_TypeInfo_var);
		FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D * L_3 = ((FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_StaticFields*)il2cpp_codegen_static_fields_for(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_il2cpp_TypeInfo_var))->get_instance_8();
		return L_3;
	}
}
// System.Void Facebook.Unity.Settings.FacebookSettings::RegisterChangeEventCallback(Facebook.Unity.Settings.FacebookSettings_OnChangeCallback)
extern "C" IL2CPP_METHOD_ATTR void FacebookSettings_RegisterChangeEventCallback_mA82B49D95A698556243CB1AEE8E971C36726CEDA (OnChangeCallback_t490B1A020446401D3A4B7030FA7A0D9AF521AFBB * ___callback0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_RegisterChangeEventCallback_mA82B49D95A698556243CB1AEE8E971C36726CEDA_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_il2cpp_TypeInfo_var);
		List_1_t4761B02A8CB42D5714C127D74C8BD8BCBD280D33 * L_0 = ((FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_StaticFields*)il2cpp_codegen_static_fields_for(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_il2cpp_TypeInfo_var))->get_onChangeCallbacks_7();
		OnChangeCallback_t490B1A020446401D3A4B7030FA7A0D9AF521AFBB * L_1 = ___callback0;
		NullCheck(L_0);
		List_1_Add_mC0B071C59951D54F8FE74C42DFECE1519E24F2EB(L_0, L_1, /*hidden argument*/List_1_Add_mC0B071C59951D54F8FE74C42DFECE1519E24F2EB_RuntimeMethod_var);
		return;
	}
}
// System.Void Facebook.Unity.Settings.FacebookSettings::UnregisterChangeEventCallback(Facebook.Unity.Settings.FacebookSettings_OnChangeCallback)
extern "C" IL2CPP_METHOD_ATTR void FacebookSettings_UnregisterChangeEventCallback_mB90FA54E3450C9137E147E1F9733F193D7AABD8B (OnChangeCallback_t490B1A020446401D3A4B7030FA7A0D9AF521AFBB * ___callback0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_UnregisterChangeEventCallback_mB90FA54E3450C9137E147E1F9733F193D7AABD8B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_il2cpp_TypeInfo_var);
		List_1_t4761B02A8CB42D5714C127D74C8BD8BCBD280D33 * L_0 = ((FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_StaticFields*)il2cpp_codegen_static_fields_for(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_il2cpp_TypeInfo_var))->get_onChangeCallbacks_7();
		OnChangeCallback_t490B1A020446401D3A4B7030FA7A0D9AF521AFBB * L_1 = ___callback0;
		NullCheck(L_0);
		List_1_Remove_mCC3529501BE616F61AADD2B818FBC70B75905105(L_0, L_1, /*hidden argument*/List_1_Remove_mCC3529501BE616F61AADD2B818FBC70B75905105_RuntimeMethod_var);
		return;
	}
}
// System.Void Facebook.Unity.Settings.FacebookSettings::SettingsChanged()
extern "C" IL2CPP_METHOD_ATTR void FacebookSettings_SettingsChanged_m63C85A637D0698D4AEA8F5B79D45609DE9FF722F (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_SettingsChanged_m63C85A637D0698D4AEA8F5B79D45609DE9FF722F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Action_1_t605040A52441734E3EA5810595C95A0D0568C238 * G_B2_0 = NULL;
	List_1_t4761B02A8CB42D5714C127D74C8BD8BCBD280D33 * G_B2_1 = NULL;
	Action_1_t605040A52441734E3EA5810595C95A0D0568C238 * G_B1_0 = NULL;
	List_1_t4761B02A8CB42D5714C127D74C8BD8BCBD280D33 * G_B1_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_il2cpp_TypeInfo_var);
		List_1_t4761B02A8CB42D5714C127D74C8BD8BCBD280D33 * L_0 = ((FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_StaticFields*)il2cpp_codegen_static_fields_for(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_il2cpp_TypeInfo_var))->get_onChangeCallbacks_7();
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t40659AD935CCBCC35589D12303DF7D93D8B4ABA8_il2cpp_TypeInfo_var);
		Action_1_t605040A52441734E3EA5810595C95A0D0568C238 * L_1 = ((U3CU3Ec_t40659AD935CCBCC35589D12303DF7D93D8B4ABA8_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t40659AD935CCBCC35589D12303DF7D93D8B4ABA8_il2cpp_TypeInfo_var))->get_U3CU3E9__76_0_1();
		Action_1_t605040A52441734E3EA5810595C95A0D0568C238 * L_2 = L_1;
		G_B1_0 = L_2;
		G_B1_1 = L_0;
		if (L_2)
		{
			G_B2_0 = L_2;
			G_B2_1 = L_0;
			goto IL_0024;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t40659AD935CCBCC35589D12303DF7D93D8B4ABA8_il2cpp_TypeInfo_var);
		U3CU3Ec_t40659AD935CCBCC35589D12303DF7D93D8B4ABA8 * L_3 = ((U3CU3Ec_t40659AD935CCBCC35589D12303DF7D93D8B4ABA8_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t40659AD935CCBCC35589D12303DF7D93D8B4ABA8_il2cpp_TypeInfo_var))->get_U3CU3E9_0();
		Action_1_t605040A52441734E3EA5810595C95A0D0568C238 * L_4 = (Action_1_t605040A52441734E3EA5810595C95A0D0568C238 *)il2cpp_codegen_object_new(Action_1_t605040A52441734E3EA5810595C95A0D0568C238_il2cpp_TypeInfo_var);
		Action_1__ctor_mCDDBCF9D6DB0D55DA31F3EB00A54CCFDCDFC7064(L_4, L_3, (intptr_t)((intptr_t)U3CU3Ec_U3CSettingsChangedU3Eb__76_0_m031FF08969EB09D1B8300C64ECF933BEEB7BF99E_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_mCDDBCF9D6DB0D55DA31F3EB00A54CCFDCDFC7064_RuntimeMethod_var);
		Action_1_t605040A52441734E3EA5810595C95A0D0568C238 * L_5 = L_4;
		((U3CU3Ec_t40659AD935CCBCC35589D12303DF7D93D8B4ABA8_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t40659AD935CCBCC35589D12303DF7D93D8B4ABA8_il2cpp_TypeInfo_var))->set_U3CU3E9__76_0_1(L_5);
		G_B2_0 = L_5;
		G_B2_1 = G_B1_1;
	}

IL_0024:
	{
		NullCheck(G_B2_1);
		List_1_ForEach_mA6A810C3D8B78C414C9FE29C629B0AB7506D6DB3(G_B2_1, G_B2_0, /*hidden argument*/List_1_ForEach_mA6A810C3D8B78C414C9FE29C629B0AB7506D6DB3_RuntimeMethod_var);
		return;
	}
}
// System.Void Facebook.Unity.Settings.FacebookSettings::.ctor()
extern "C" IL2CPP_METHOD_ATTR void FacebookSettings__ctor_m4796A2B919F21695D7B45C9E268682ACE2BE1716 (FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings__ctor_m4796A2B919F21695D7B45C9E268682ACE2BE1716_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_0 = (List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 *)il2cpp_codegen_object_new(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3_il2cpp_TypeInfo_var);
		List_1__ctor_mDA22758D73530683C950C5CCF39BDB4E7E1F3F06(L_0, /*hidden argument*/List_1__ctor_mDA22758D73530683C950C5CCF39BDB4E7E1F3F06_RuntimeMethod_var);
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_1 = L_0;
		String_t* L_2 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		NullCheck(L_1);
		List_1_Add_mA348FA1140766465189459D25B01EB179001DE83(L_1, L_2, /*hidden argument*/List_1_Add_mA348FA1140766465189459D25B01EB179001DE83_RuntimeMethod_var);
		__this->set_clientTokens_10(L_1);
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_3 = (List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 *)il2cpp_codegen_object_new(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3_il2cpp_TypeInfo_var);
		List_1__ctor_mDA22758D73530683C950C5CCF39BDB4E7E1F3F06(L_3, /*hidden argument*/List_1__ctor_mDA22758D73530683C950C5CCF39BDB4E7E1F3F06_RuntimeMethod_var);
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_4 = L_3;
		NullCheck(L_4);
		List_1_Add_mA348FA1140766465189459D25B01EB179001DE83(L_4, _stringLiteralB6589FC6AB0DC82CF12099D1C2D40AB994E8410C, /*hidden argument*/List_1_Add_mA348FA1140766465189459D25B01EB179001DE83_RuntimeMethod_var);
		__this->set_appIds_11(L_4);
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_5 = (List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 *)il2cpp_codegen_object_new(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3_il2cpp_TypeInfo_var);
		List_1__ctor_mDA22758D73530683C950C5CCF39BDB4E7E1F3F06(L_5, /*hidden argument*/List_1__ctor_mDA22758D73530683C950C5CCF39BDB4E7E1F3F06_RuntimeMethod_var);
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_6 = L_5;
		NullCheck(L_6);
		List_1_Add_mA348FA1140766465189459D25B01EB179001DE83(L_6, _stringLiteral4FB43108E78F4D80425CB74F01AC6D0A59E0A3D3, /*hidden argument*/List_1_Add_mA348FA1140766465189459D25B01EB179001DE83_RuntimeMethod_var);
		__this->set_appLabels_12(L_6);
		__this->set_cookie_13((bool)1);
		__this->set_logging_14((bool)1);
		__this->set_status_15((bool)1);
		__this->set_frictionlessRequests_17((bool)1);
		String_t* L_7 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		__this->set_iosURLSuffix_18(L_7);
		List_1_tEBAA293BF803F2D17E20E8B6FE34EF3792F6FA6B * L_8 = (List_1_tEBAA293BF803F2D17E20E8B6FE34EF3792F6FA6B *)il2cpp_codegen_object_new(List_1_tEBAA293BF803F2D17E20E8B6FE34EF3792F6FA6B_il2cpp_TypeInfo_var);
		List_1__ctor_m193A1CCAFC83743EFB9ADBC8FDB87850CD0C9C7A(L_8, /*hidden argument*/List_1__ctor_m193A1CCAFC83743EFB9ADBC8FDB87850CD0C9C7A_RuntimeMethod_var);
		List_1_tEBAA293BF803F2D17E20E8B6FE34EF3792F6FA6B * L_9 = L_8;
		UrlSchemes_t7B8D8455C2FE4B28F5F677FFAE86E14077D94D8C * L_10 = (UrlSchemes_t7B8D8455C2FE4B28F5F677FFAE86E14077D94D8C *)il2cpp_codegen_object_new(UrlSchemes_t7B8D8455C2FE4B28F5F677FFAE86E14077D94D8C_il2cpp_TypeInfo_var);
		UrlSchemes__ctor_mD2CB85DBE160B9144012A889732A4CF3E6FBBA62(L_10, (List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 *)NULL, /*hidden argument*/NULL);
		NullCheck(L_9);
		List_1_Add_m9B9A17315AE28254D75EDF0BDD68D83CB3B8B4F2(L_9, L_10, /*hidden argument*/List_1_Add_m9B9A17315AE28254D75EDF0BDD68D83CB3B8B4F2_RuntimeMethod_var);
		__this->set_appLinkSchemes_19(L_9);
		String_t* L_11 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		__this->set_uploadAccessToken_20(L_11);
		__this->set_autoLogAppEventsEnabled_21((bool)1);
		__this->set_advertiserIDCollectionEnabled_22((bool)1);
		ScriptableObject__ctor_m6E2B3821A4A361556FC12E9B1C71E1D5DC002C5B(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.Settings.FacebookSettings::.cctor()
extern "C" IL2CPP_METHOD_ATTR void FacebookSettings__cctor_m175D7BD588BA109CF3319B0EF4E447146BDAFA78 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings__cctor_m175D7BD588BA109CF3319B0EF4E447146BDAFA78_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t4761B02A8CB42D5714C127D74C8BD8BCBD280D33 * L_0 = (List_1_t4761B02A8CB42D5714C127D74C8BD8BCBD280D33 *)il2cpp_codegen_object_new(List_1_t4761B02A8CB42D5714C127D74C8BD8BCBD280D33_il2cpp_TypeInfo_var);
		List_1__ctor_mA011B26BD878095268926AAE1523A7B080A39A20(L_0, /*hidden argument*/List_1__ctor_mA011B26BD878095268926AAE1523A7B080A39A20_RuntimeMethod_var);
		((FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_StaticFields*)il2cpp_codegen_static_fields_for(FacebookSettings_t1A2119548288F7AD79D590C587740A8EED14D74D_il2cpp_TypeInfo_var))->set_onChangeCallbacks_7(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Facebook.Unity.Settings.FacebookSettings_<>c::.cctor()
extern "C" IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_mDE57DA4F578EE7C96E604EA3A4CD427F86AF9A9B (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__cctor_mDE57DA4F578EE7C96E604EA3A4CD427F86AF9A9B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_t40659AD935CCBCC35589D12303DF7D93D8B4ABA8 * L_0 = (U3CU3Ec_t40659AD935CCBCC35589D12303DF7D93D8B4ABA8 *)il2cpp_codegen_object_new(U3CU3Ec_t40659AD935CCBCC35589D12303DF7D93D8B4ABA8_il2cpp_TypeInfo_var);
		U3CU3Ec__ctor_m53CB9A84121F3B2E66EB8CEFC7608554F9A38A5C(L_0, /*hidden argument*/NULL);
		((U3CU3Ec_t40659AD935CCBCC35589D12303DF7D93D8B4ABA8_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t40659AD935CCBCC35589D12303DF7D93D8B4ABA8_il2cpp_TypeInfo_var))->set_U3CU3E9_0(L_0);
		return;
	}
}
// System.Void Facebook.Unity.Settings.FacebookSettings_<>c::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m53CB9A84121F3B2E66EB8CEFC7608554F9A38A5C (U3CU3Ec_t40659AD935CCBCC35589D12303DF7D93D8B4ABA8 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.Settings.FacebookSettings_<>c::<SettingsChanged>b__76_0(Facebook.Unity.Settings.FacebookSettings_OnChangeCallback)
extern "C" IL2CPP_METHOD_ATTR void U3CU3Ec_U3CSettingsChangedU3Eb__76_0_m031FF08969EB09D1B8300C64ECF933BEEB7BF99E (U3CU3Ec_t40659AD935CCBCC35589D12303DF7D93D8B4ABA8 * __this, OnChangeCallback_t490B1A020446401D3A4B7030FA7A0D9AF521AFBB * ___callback0, const RuntimeMethod* method)
{
	{
		OnChangeCallback_t490B1A020446401D3A4B7030FA7A0D9AF521AFBB * L_0 = ___callback0;
		NullCheck(L_0);
		OnChangeCallback_Invoke_mB8A883493EA3BE29A9D2F841CF981794A0D1FB83(L_0, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern "C"  void DelegatePInvokeWrapper_OnChangeCallback_t490B1A020446401D3A4B7030FA7A0D9AF521AFBB (OnChangeCallback_t490B1A020446401D3A4B7030FA7A0D9AF521AFBB * __this, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.Void Facebook.Unity.Settings.FacebookSettings_OnChangeCallback::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void OnChangeCallback__ctor_mAA7888F8CA7FC5FBC6FD69F110E616CDB2F81AE5 (OnChangeCallback_t490B1A020446401D3A4B7030FA7A0D9AF521AFBB * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Facebook.Unity.Settings.FacebookSettings_OnChangeCallback::Invoke()
extern "C" IL2CPP_METHOD_ATTR void OnChangeCallback_Invoke_mB8A883493EA3BE29A9D2F841CF981794A0D1FB83 (OnChangeCallback_t490B1A020446401D3A4B7030FA7A0D9AF521AFBB * __this, const RuntimeMethod* method)
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		il2cpp_array_size_t length = delegatesToInvoke->max_length;
		for (il2cpp_array_size_t i = 0; i < length; i++)
		{
			Delegate_t* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
			RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
			RuntimeObject* targetThis = currentDelegate->get_m_target_2();
			if (!il2cpp_codegen_method_is_virtual(targetMethod))
			{
				il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
			}
			bool ___methodIsStatic = MethodIsStatic(targetMethod);
			int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
			if (___methodIsStatic)
			{
				if (___parameterCount == 0)
				{
					// open
					typedef void (*FunctionPointerType) (const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetMethod);
				}
				else
				{
					// closed
					typedef void (*FunctionPointerType) (void*, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, targetMethod);
				}
			}
			else
			{
				// closed
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
					{
						if (il2cpp_codegen_method_is_generic_instance(targetMethod))
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								GenericInterfaceActionInvoker0::Invoke(targetMethod, targetThis);
							else
								GenericVirtActionInvoker0::Invoke(targetMethod, targetThis);
						}
						else
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								InterfaceActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis);
							else
								VirtActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis);
						}
					}
				}
				else
				{
					typedef void (*FunctionPointerType) (void*, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, targetMethod);
				}
			}
		}
	}
	else
	{
		Il2CppMethodPointer targetMethodPointer = __this->get_method_ptr_0();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(__this->get_method_3());
		RuntimeObject* targetThis = __this->get_m_target_2();
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 0)
			{
				// open
				typedef void (*FunctionPointerType) (const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, targetMethod);
			}
		}
		else
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_generic_instance(targetMethod))
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							GenericInterfaceActionInvoker0::Invoke(targetMethod, targetThis);
						else
							GenericVirtActionInvoker0::Invoke(targetMethod, targetThis);
					}
					else
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							InterfaceActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis);
						else
							VirtActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis);
					}
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (void*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, targetMethod);
			}
		}
	}
}
// System.IAsyncResult Facebook.Unity.Settings.FacebookSettings_OnChangeCallback::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* OnChangeCallback_BeginInvoke_m8C51DDD501DB6FD9EA11B8C6CED6CE7A2861D582 (OnChangeCallback_t490B1A020446401D3A4B7030FA7A0D9AF521AFBB * __this, AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___callback0, RuntimeObject * ___object1, const RuntimeMethod* method)
{
	void *__d_args[1] = {0};
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback0, (RuntimeObject*)___object1);
}
// System.Void Facebook.Unity.Settings.FacebookSettings_OnChangeCallback::EndInvoke(System.IAsyncResult)
extern "C" IL2CPP_METHOD_ATTR void OnChangeCallback_EndInvoke_m6CC1923BC799C388B94052742FB9914CB90D1FB4 (OnChangeCallback_t490B1A020446401D3A4B7030FA7A0D9AF521AFBB * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Facebook.Unity.Settings.FacebookSettings_UrlSchemes::.ctor(System.Collections.Generic.List`1<System.String>)
extern "C" IL2CPP_METHOD_ATTR void UrlSchemes__ctor_mD2CB85DBE160B9144012A889732A4CF3E6FBBA62 (UrlSchemes_t7B8D8455C2FE4B28F5F677FFAE86E14077D94D8C * __this, List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___schemes0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UrlSchemes__ctor_mD2CB85DBE160B9144012A889732A4CF3E6FBBA62_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UrlSchemes_t7B8D8455C2FE4B28F5F677FFAE86E14077D94D8C * G_B2_0 = NULL;
	UrlSchemes_t7B8D8455C2FE4B28F5F677FFAE86E14077D94D8C * G_B1_0 = NULL;
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * G_B3_0 = NULL;
	UrlSchemes_t7B8D8455C2FE4B28F5F677FFAE86E14077D94D8C * G_B3_1 = NULL;
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_0 = ___schemes0;
		G_B1_0 = __this;
		if (!L_0)
		{
			G_B2_0 = __this;
			goto IL_000d;
		}
	}
	{
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_1 = ___schemes0;
		G_B3_0 = L_1;
		G_B3_1 = G_B1_0;
		goto IL_0012;
	}

IL_000d:
	{
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_2 = (List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 *)il2cpp_codegen_object_new(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3_il2cpp_TypeInfo_var);
		List_1__ctor_mDA22758D73530683C950C5CCF39BDB4E7E1F3F06(L_2, /*hidden argument*/List_1__ctor_mDA22758D73530683C950C5CCF39BDB4E7E1F3F06_RuntimeMethod_var);
		G_B3_0 = L_2;
		G_B3_1 = G_B2_0;
	}

IL_0012:
	{
		NullCheck(G_B3_1);
		G_B3_1->set_list_0(G_B3_0);
		return;
	}
}
// System.Collections.Generic.List`1<System.String> Facebook.Unity.Settings.FacebookSettings_UrlSchemes::get_Schemes()
extern "C" IL2CPP_METHOD_ATTR List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * UrlSchemes_get_Schemes_mF718F68169111C32EA6612B9AE3A89C7C4223306 (UrlSchemes_t7B8D8455C2FE4B28F5F677FFAE86E14077D94D8C * __this, const RuntimeMethod* method)
{
	{
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_0 = __this->get_list_0();
		return L_0;
	}
}
// System.Void Facebook.Unity.Settings.FacebookSettings_UrlSchemes::set_Schemes(System.Collections.Generic.List`1<System.String>)
extern "C" IL2CPP_METHOD_ATTR void UrlSchemes_set_Schemes_m8B8CEA7FA28983F9320A9A8AEF588FF2A1E069F7 (UrlSchemes_t7B8D8455C2FE4B28F5F677FFAE86E14077D94D8C * __this, List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___value0, const RuntimeMethod* method)
{
	{
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_0 = ___value0;
		__this->set_list_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
