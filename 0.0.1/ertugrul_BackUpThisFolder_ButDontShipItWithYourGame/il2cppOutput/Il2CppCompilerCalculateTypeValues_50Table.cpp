﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Org.BouncyCastle.Asn1.Asn1Object
struct Asn1Object_tB780E50883209D5C975E83B3D16257DFC3CCE36D;
// Org.BouncyCastle.Asn1.Asn1OctetString
struct Asn1OctetString_t2088F9E88E4C97BB9CF3B2FFC53CFBAF9B8B79CB;
// Org.BouncyCastle.Asn1.DerObjectIdentifier
struct DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441;
// Org.BouncyCastle.Asn1.X9.X9ECParameters
struct X9ECParameters_t818A7DD4D13D91F0D36622C6C117D7E9A10E51C3;
// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder
struct X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE;
// Org.BouncyCastle.Asn1.X9.X9ECPoint
struct X9ECPoint_t0FFE1DD3974DC6DE1C0428FDD84A3FF10D29F65A;
// Org.BouncyCastle.Asn1.X9.X9FieldID
struct X9FieldID_t8A4FF1A99F5C9431C28964FA64FD3032C9A64019;
// Org.BouncyCastle.Crypto.AsymmetricKeyParameter
struct AsymmetricKeyParameter_tD6918D64A0FB774AF79E386A85415CE53D5EC0D7;
// Org.BouncyCastle.Crypto.Engines.RsaCoreEngine
struct RsaCoreEngine_t34DD3F15CAE1DA560B6C6C8E475F6951E197DC64;
// Org.BouncyCastle.Crypto.IAsymmetricBlockCipher
struct IAsymmetricBlockCipher_tA8433E11E9552E327EFAF108F2BDBA0A9F331B29;
// Org.BouncyCastle.Crypto.IBlockCipher
struct IBlockCipher_tB1BAE99FDCBFA6193F91C083240BC00B7D01A612;
// Org.BouncyCastle.Crypto.Parameters.DHParameters
struct DHParameters_t15DBCF5CD22F83E806A78FF80CE2853863825513;
// Org.BouncyCastle.Crypto.Parameters.DHPrivateKeyParameters
struct DHPrivateKeyParameters_t7A930900443BCA929045970B99A92EADD777049C;
// Org.BouncyCastle.Crypto.Parameters.ECPrivateKeyParameters
struct ECPrivateKeyParameters_tC1317EEDAEF16688C98CD41053BB95C6751BBD1F;
// Org.BouncyCastle.Crypto.Parameters.RsaKeyParameters
struct RsaKeyParameters_t57791EDEBAF0AD93C55C7FCF645D7CBB6284D308;
// Org.BouncyCastle.Math.BigInteger
struct BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91;
// Org.BouncyCastle.Math.EC.ECCurve
struct ECCurve_t08AD8E722914B0B03C49C2D27B79B1D4E4770D95;
// Org.BouncyCastle.Math.EC.ECFieldElement
struct ECFieldElement_tE2FF8892A60D0ACBC3FB665E41B0DEAA0308514C;
// Org.BouncyCastle.Math.EC.ECPoint
struct ECPoint_tCC0C4447DEFF70FC9D68F5149ECCCC0533CE4399;
// Org.BouncyCastle.Security.SecureRandom
struct SecureRandom_t0B6C2C3EDF11BF4FD22E44CE8997F6A0D2F142B0;
// System.Boolean[]
struct BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Byte[][]
struct ByteU5BU5DU5BU5D_tD1CB918775FFB351821F10AC338FECDDE22DEEC7;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.Collections.IList
struct IList_tA637AB426E16F84F84ACC2813BDCF3A0414AF0AA;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.IO.MemoryStream
struct MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C;
// System.Int16[]
struct Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Int64[]
struct Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.String
struct String_t;
// System.UInt32[]
struct UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB;
// System.UInt32[][]
struct UInt32U5BU5DU5BU5D_tBFBD606F8E3022C93666CAF6F7DA13AD70895D8C;
// System.UInt64[]
struct UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4;

struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef ASN1ENCODABLE_T390FCE82F5F52E41A7320BBF2E8BF18DA1622A8B_H
#define ASN1ENCODABLE_T390FCE82F5F52E41A7320BBF2E8BF18DA1622A8B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.Asn1Encodable
struct  Asn1Encodable_t390FCE82F5F52E41A7320BBF2E8BF18DA1622A8B  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASN1ENCODABLE_T390FCE82F5F52E41A7320BBF2E8BF18DA1622A8B_H
#ifndef X9ECPARAMETERSHOLDER_TB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE_H
#define X9ECPARAMETERSHOLDER_TB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder
struct  X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE  : public RuntimeObject
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParameters Org.BouncyCastle.Asn1.X9.X9ECParametersHolder::parameters
	X9ECParameters_t818A7DD4D13D91F0D36622C6C117D7E9A10E51C3 * ___parameters_0;

public:
	inline static int32_t get_offset_of_parameters_0() { return static_cast<int32_t>(offsetof(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE, ___parameters_0)); }
	inline X9ECParameters_t818A7DD4D13D91F0D36622C6C117D7E9A10E51C3 * get_parameters_0() const { return ___parameters_0; }
	inline X9ECParameters_t818A7DD4D13D91F0D36622C6C117D7E9A10E51C3 ** get_address_of_parameters_0() { return &___parameters_0; }
	inline void set_parameters_0(X9ECParameters_t818A7DD4D13D91F0D36622C6C117D7E9A10E51C3 * value)
	{
		___parameters_0 = value;
		Il2CppCodeGenWriteBarrier((&___parameters_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X9ECPARAMETERSHOLDER_TB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE_H
#ifndef X9INTEGERCONVERTER_TF9DC48605C76F129105AE178024F07081EC6472E_H
#define X9INTEGERCONVERTER_TF9DC48605C76F129105AE178024F07081EC6472E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.X9.X9IntegerConverter
struct  X9IntegerConverter_tF9DC48605C76F129105AE178024F07081EC6472E  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X9INTEGERCONVERTER_TF9DC48605C76F129105AE178024F07081EC6472E_H
#ifndef X9OBJECTIDENTIFIERS_T3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_H
#define X9OBJECTIDENTIFIERS_T3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers
struct  X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B  : public RuntimeObject
{
public:

public:
};

struct X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::ansi_X9_62
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___ansi_X9_62_0;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::IdFieldType
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdFieldType_1;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::PrimeField
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___PrimeField_2;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::CharacteristicTwoField
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___CharacteristicTwoField_3;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::GNBasis
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___GNBasis_4;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::TPBasis
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___TPBasis_5;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::PPBasis
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___PPBasis_6;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::id_ecSigType
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___id_ecSigType_7;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::ECDsaWithSha1
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___ECDsaWithSha1_8;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::id_publicKeyType
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___id_publicKeyType_9;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::IdECPublicKey
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdECPublicKey_10;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::ECDsaWithSha2
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___ECDsaWithSha2_11;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::ECDsaWithSha224
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___ECDsaWithSha224_12;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::ECDsaWithSha256
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___ECDsaWithSha256_13;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::ECDsaWithSha384
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___ECDsaWithSha384_14;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::ECDsaWithSha512
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___ECDsaWithSha512_15;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::EllipticCurve
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___EllipticCurve_16;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::CTwoCurve
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___CTwoCurve_17;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::C2Pnb163v1
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___C2Pnb163v1_18;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::C2Pnb163v2
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___C2Pnb163v2_19;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::C2Pnb163v3
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___C2Pnb163v3_20;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::C2Pnb176w1
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___C2Pnb176w1_21;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::C2Tnb191v1
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___C2Tnb191v1_22;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::C2Tnb191v2
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___C2Tnb191v2_23;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::C2Tnb191v3
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___C2Tnb191v3_24;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::C2Onb191v4
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___C2Onb191v4_25;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::C2Onb191v5
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___C2Onb191v5_26;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::C2Pnb208w1
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___C2Pnb208w1_27;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::C2Tnb239v1
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___C2Tnb239v1_28;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::C2Tnb239v2
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___C2Tnb239v2_29;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::C2Tnb239v3
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___C2Tnb239v3_30;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::C2Onb239v4
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___C2Onb239v4_31;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::C2Onb239v5
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___C2Onb239v5_32;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::C2Pnb272w1
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___C2Pnb272w1_33;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::C2Pnb304w1
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___C2Pnb304w1_34;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::C2Tnb359v1
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___C2Tnb359v1_35;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::C2Pnb368w1
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___C2Pnb368w1_36;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::C2Tnb431r1
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___C2Tnb431r1_37;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::PrimeCurve
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___PrimeCurve_38;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::Prime192v1
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___Prime192v1_39;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::Prime192v2
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___Prime192v2_40;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::Prime192v3
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___Prime192v3_41;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::Prime239v1
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___Prime239v1_42;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::Prime239v2
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___Prime239v2_43;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::Prime239v3
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___Prime239v3_44;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::Prime256v1
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___Prime256v1_45;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::IdDsa
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdDsa_46;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::IdDsaWithSha1
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___IdDsaWithSha1_47;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::X9x63Scheme
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___X9x63Scheme_48;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::DHSinglePassStdDHSha1KdfScheme
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___DHSinglePassStdDHSha1KdfScheme_49;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::DHSinglePassCofactorDHSha1KdfScheme
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___DHSinglePassCofactorDHSha1KdfScheme_50;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::MqvSinglePassSha1KdfScheme
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___MqvSinglePassSha1KdfScheme_51;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::ansi_x9_42
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___ansi_x9_42_52;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::DHPublicNumber
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___DHPublicNumber_53;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::X9x42Schemes
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___X9x42Schemes_54;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::DHStatic
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___DHStatic_55;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::DHEphem
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___DHEphem_56;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::DHOneFlow
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___DHOneFlow_57;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::DHHybrid1
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___DHHybrid1_58;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::DHHybrid2
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___DHHybrid2_59;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::DHHybridOneFlow
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___DHHybridOneFlow_60;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::Mqv2
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___Mqv2_61;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X9.X9ObjectIdentifiers::Mqv1
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___Mqv1_62;

public:
	inline static int32_t get_offset_of_ansi_X9_62_0() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields, ___ansi_X9_62_0)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_ansi_X9_62_0() const { return ___ansi_X9_62_0; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_ansi_X9_62_0() { return &___ansi_X9_62_0; }
	inline void set_ansi_X9_62_0(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___ansi_X9_62_0 = value;
		Il2CppCodeGenWriteBarrier((&___ansi_X9_62_0), value);
	}

	inline static int32_t get_offset_of_IdFieldType_1() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields, ___IdFieldType_1)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdFieldType_1() const { return ___IdFieldType_1; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdFieldType_1() { return &___IdFieldType_1; }
	inline void set_IdFieldType_1(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdFieldType_1 = value;
		Il2CppCodeGenWriteBarrier((&___IdFieldType_1), value);
	}

	inline static int32_t get_offset_of_PrimeField_2() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields, ___PrimeField_2)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_PrimeField_2() const { return ___PrimeField_2; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_PrimeField_2() { return &___PrimeField_2; }
	inline void set_PrimeField_2(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___PrimeField_2 = value;
		Il2CppCodeGenWriteBarrier((&___PrimeField_2), value);
	}

	inline static int32_t get_offset_of_CharacteristicTwoField_3() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields, ___CharacteristicTwoField_3)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_CharacteristicTwoField_3() const { return ___CharacteristicTwoField_3; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_CharacteristicTwoField_3() { return &___CharacteristicTwoField_3; }
	inline void set_CharacteristicTwoField_3(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___CharacteristicTwoField_3 = value;
		Il2CppCodeGenWriteBarrier((&___CharacteristicTwoField_3), value);
	}

	inline static int32_t get_offset_of_GNBasis_4() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields, ___GNBasis_4)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_GNBasis_4() const { return ___GNBasis_4; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_GNBasis_4() { return &___GNBasis_4; }
	inline void set_GNBasis_4(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___GNBasis_4 = value;
		Il2CppCodeGenWriteBarrier((&___GNBasis_4), value);
	}

	inline static int32_t get_offset_of_TPBasis_5() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields, ___TPBasis_5)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_TPBasis_5() const { return ___TPBasis_5; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_TPBasis_5() { return &___TPBasis_5; }
	inline void set_TPBasis_5(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___TPBasis_5 = value;
		Il2CppCodeGenWriteBarrier((&___TPBasis_5), value);
	}

	inline static int32_t get_offset_of_PPBasis_6() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields, ___PPBasis_6)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_PPBasis_6() const { return ___PPBasis_6; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_PPBasis_6() { return &___PPBasis_6; }
	inline void set_PPBasis_6(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___PPBasis_6 = value;
		Il2CppCodeGenWriteBarrier((&___PPBasis_6), value);
	}

	inline static int32_t get_offset_of_id_ecSigType_7() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields, ___id_ecSigType_7)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_id_ecSigType_7() const { return ___id_ecSigType_7; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_id_ecSigType_7() { return &___id_ecSigType_7; }
	inline void set_id_ecSigType_7(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___id_ecSigType_7 = value;
		Il2CppCodeGenWriteBarrier((&___id_ecSigType_7), value);
	}

	inline static int32_t get_offset_of_ECDsaWithSha1_8() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields, ___ECDsaWithSha1_8)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_ECDsaWithSha1_8() const { return ___ECDsaWithSha1_8; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_ECDsaWithSha1_8() { return &___ECDsaWithSha1_8; }
	inline void set_ECDsaWithSha1_8(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___ECDsaWithSha1_8 = value;
		Il2CppCodeGenWriteBarrier((&___ECDsaWithSha1_8), value);
	}

	inline static int32_t get_offset_of_id_publicKeyType_9() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields, ___id_publicKeyType_9)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_id_publicKeyType_9() const { return ___id_publicKeyType_9; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_id_publicKeyType_9() { return &___id_publicKeyType_9; }
	inline void set_id_publicKeyType_9(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___id_publicKeyType_9 = value;
		Il2CppCodeGenWriteBarrier((&___id_publicKeyType_9), value);
	}

	inline static int32_t get_offset_of_IdECPublicKey_10() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields, ___IdECPublicKey_10)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdECPublicKey_10() const { return ___IdECPublicKey_10; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdECPublicKey_10() { return &___IdECPublicKey_10; }
	inline void set_IdECPublicKey_10(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdECPublicKey_10 = value;
		Il2CppCodeGenWriteBarrier((&___IdECPublicKey_10), value);
	}

	inline static int32_t get_offset_of_ECDsaWithSha2_11() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields, ___ECDsaWithSha2_11)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_ECDsaWithSha2_11() const { return ___ECDsaWithSha2_11; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_ECDsaWithSha2_11() { return &___ECDsaWithSha2_11; }
	inline void set_ECDsaWithSha2_11(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___ECDsaWithSha2_11 = value;
		Il2CppCodeGenWriteBarrier((&___ECDsaWithSha2_11), value);
	}

	inline static int32_t get_offset_of_ECDsaWithSha224_12() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields, ___ECDsaWithSha224_12)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_ECDsaWithSha224_12() const { return ___ECDsaWithSha224_12; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_ECDsaWithSha224_12() { return &___ECDsaWithSha224_12; }
	inline void set_ECDsaWithSha224_12(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___ECDsaWithSha224_12 = value;
		Il2CppCodeGenWriteBarrier((&___ECDsaWithSha224_12), value);
	}

	inline static int32_t get_offset_of_ECDsaWithSha256_13() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields, ___ECDsaWithSha256_13)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_ECDsaWithSha256_13() const { return ___ECDsaWithSha256_13; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_ECDsaWithSha256_13() { return &___ECDsaWithSha256_13; }
	inline void set_ECDsaWithSha256_13(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___ECDsaWithSha256_13 = value;
		Il2CppCodeGenWriteBarrier((&___ECDsaWithSha256_13), value);
	}

	inline static int32_t get_offset_of_ECDsaWithSha384_14() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields, ___ECDsaWithSha384_14)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_ECDsaWithSha384_14() const { return ___ECDsaWithSha384_14; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_ECDsaWithSha384_14() { return &___ECDsaWithSha384_14; }
	inline void set_ECDsaWithSha384_14(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___ECDsaWithSha384_14 = value;
		Il2CppCodeGenWriteBarrier((&___ECDsaWithSha384_14), value);
	}

	inline static int32_t get_offset_of_ECDsaWithSha512_15() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields, ___ECDsaWithSha512_15)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_ECDsaWithSha512_15() const { return ___ECDsaWithSha512_15; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_ECDsaWithSha512_15() { return &___ECDsaWithSha512_15; }
	inline void set_ECDsaWithSha512_15(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___ECDsaWithSha512_15 = value;
		Il2CppCodeGenWriteBarrier((&___ECDsaWithSha512_15), value);
	}

	inline static int32_t get_offset_of_EllipticCurve_16() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields, ___EllipticCurve_16)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_EllipticCurve_16() const { return ___EllipticCurve_16; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_EllipticCurve_16() { return &___EllipticCurve_16; }
	inline void set_EllipticCurve_16(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___EllipticCurve_16 = value;
		Il2CppCodeGenWriteBarrier((&___EllipticCurve_16), value);
	}

	inline static int32_t get_offset_of_CTwoCurve_17() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields, ___CTwoCurve_17)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_CTwoCurve_17() const { return ___CTwoCurve_17; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_CTwoCurve_17() { return &___CTwoCurve_17; }
	inline void set_CTwoCurve_17(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___CTwoCurve_17 = value;
		Il2CppCodeGenWriteBarrier((&___CTwoCurve_17), value);
	}

	inline static int32_t get_offset_of_C2Pnb163v1_18() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields, ___C2Pnb163v1_18)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_C2Pnb163v1_18() const { return ___C2Pnb163v1_18; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_C2Pnb163v1_18() { return &___C2Pnb163v1_18; }
	inline void set_C2Pnb163v1_18(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___C2Pnb163v1_18 = value;
		Il2CppCodeGenWriteBarrier((&___C2Pnb163v1_18), value);
	}

	inline static int32_t get_offset_of_C2Pnb163v2_19() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields, ___C2Pnb163v2_19)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_C2Pnb163v2_19() const { return ___C2Pnb163v2_19; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_C2Pnb163v2_19() { return &___C2Pnb163v2_19; }
	inline void set_C2Pnb163v2_19(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___C2Pnb163v2_19 = value;
		Il2CppCodeGenWriteBarrier((&___C2Pnb163v2_19), value);
	}

	inline static int32_t get_offset_of_C2Pnb163v3_20() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields, ___C2Pnb163v3_20)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_C2Pnb163v3_20() const { return ___C2Pnb163v3_20; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_C2Pnb163v3_20() { return &___C2Pnb163v3_20; }
	inline void set_C2Pnb163v3_20(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___C2Pnb163v3_20 = value;
		Il2CppCodeGenWriteBarrier((&___C2Pnb163v3_20), value);
	}

	inline static int32_t get_offset_of_C2Pnb176w1_21() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields, ___C2Pnb176w1_21)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_C2Pnb176w1_21() const { return ___C2Pnb176w1_21; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_C2Pnb176w1_21() { return &___C2Pnb176w1_21; }
	inline void set_C2Pnb176w1_21(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___C2Pnb176w1_21 = value;
		Il2CppCodeGenWriteBarrier((&___C2Pnb176w1_21), value);
	}

	inline static int32_t get_offset_of_C2Tnb191v1_22() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields, ___C2Tnb191v1_22)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_C2Tnb191v1_22() const { return ___C2Tnb191v1_22; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_C2Tnb191v1_22() { return &___C2Tnb191v1_22; }
	inline void set_C2Tnb191v1_22(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___C2Tnb191v1_22 = value;
		Il2CppCodeGenWriteBarrier((&___C2Tnb191v1_22), value);
	}

	inline static int32_t get_offset_of_C2Tnb191v2_23() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields, ___C2Tnb191v2_23)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_C2Tnb191v2_23() const { return ___C2Tnb191v2_23; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_C2Tnb191v2_23() { return &___C2Tnb191v2_23; }
	inline void set_C2Tnb191v2_23(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___C2Tnb191v2_23 = value;
		Il2CppCodeGenWriteBarrier((&___C2Tnb191v2_23), value);
	}

	inline static int32_t get_offset_of_C2Tnb191v3_24() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields, ___C2Tnb191v3_24)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_C2Tnb191v3_24() const { return ___C2Tnb191v3_24; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_C2Tnb191v3_24() { return &___C2Tnb191v3_24; }
	inline void set_C2Tnb191v3_24(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___C2Tnb191v3_24 = value;
		Il2CppCodeGenWriteBarrier((&___C2Tnb191v3_24), value);
	}

	inline static int32_t get_offset_of_C2Onb191v4_25() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields, ___C2Onb191v4_25)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_C2Onb191v4_25() const { return ___C2Onb191v4_25; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_C2Onb191v4_25() { return &___C2Onb191v4_25; }
	inline void set_C2Onb191v4_25(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___C2Onb191v4_25 = value;
		Il2CppCodeGenWriteBarrier((&___C2Onb191v4_25), value);
	}

	inline static int32_t get_offset_of_C2Onb191v5_26() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields, ___C2Onb191v5_26)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_C2Onb191v5_26() const { return ___C2Onb191v5_26; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_C2Onb191v5_26() { return &___C2Onb191v5_26; }
	inline void set_C2Onb191v5_26(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___C2Onb191v5_26 = value;
		Il2CppCodeGenWriteBarrier((&___C2Onb191v5_26), value);
	}

	inline static int32_t get_offset_of_C2Pnb208w1_27() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields, ___C2Pnb208w1_27)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_C2Pnb208w1_27() const { return ___C2Pnb208w1_27; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_C2Pnb208w1_27() { return &___C2Pnb208w1_27; }
	inline void set_C2Pnb208w1_27(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___C2Pnb208w1_27 = value;
		Il2CppCodeGenWriteBarrier((&___C2Pnb208w1_27), value);
	}

	inline static int32_t get_offset_of_C2Tnb239v1_28() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields, ___C2Tnb239v1_28)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_C2Tnb239v1_28() const { return ___C2Tnb239v1_28; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_C2Tnb239v1_28() { return &___C2Tnb239v1_28; }
	inline void set_C2Tnb239v1_28(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___C2Tnb239v1_28 = value;
		Il2CppCodeGenWriteBarrier((&___C2Tnb239v1_28), value);
	}

	inline static int32_t get_offset_of_C2Tnb239v2_29() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields, ___C2Tnb239v2_29)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_C2Tnb239v2_29() const { return ___C2Tnb239v2_29; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_C2Tnb239v2_29() { return &___C2Tnb239v2_29; }
	inline void set_C2Tnb239v2_29(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___C2Tnb239v2_29 = value;
		Il2CppCodeGenWriteBarrier((&___C2Tnb239v2_29), value);
	}

	inline static int32_t get_offset_of_C2Tnb239v3_30() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields, ___C2Tnb239v3_30)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_C2Tnb239v3_30() const { return ___C2Tnb239v3_30; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_C2Tnb239v3_30() { return &___C2Tnb239v3_30; }
	inline void set_C2Tnb239v3_30(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___C2Tnb239v3_30 = value;
		Il2CppCodeGenWriteBarrier((&___C2Tnb239v3_30), value);
	}

	inline static int32_t get_offset_of_C2Onb239v4_31() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields, ___C2Onb239v4_31)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_C2Onb239v4_31() const { return ___C2Onb239v4_31; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_C2Onb239v4_31() { return &___C2Onb239v4_31; }
	inline void set_C2Onb239v4_31(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___C2Onb239v4_31 = value;
		Il2CppCodeGenWriteBarrier((&___C2Onb239v4_31), value);
	}

	inline static int32_t get_offset_of_C2Onb239v5_32() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields, ___C2Onb239v5_32)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_C2Onb239v5_32() const { return ___C2Onb239v5_32; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_C2Onb239v5_32() { return &___C2Onb239v5_32; }
	inline void set_C2Onb239v5_32(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___C2Onb239v5_32 = value;
		Il2CppCodeGenWriteBarrier((&___C2Onb239v5_32), value);
	}

	inline static int32_t get_offset_of_C2Pnb272w1_33() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields, ___C2Pnb272w1_33)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_C2Pnb272w1_33() const { return ___C2Pnb272w1_33; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_C2Pnb272w1_33() { return &___C2Pnb272w1_33; }
	inline void set_C2Pnb272w1_33(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___C2Pnb272w1_33 = value;
		Il2CppCodeGenWriteBarrier((&___C2Pnb272w1_33), value);
	}

	inline static int32_t get_offset_of_C2Pnb304w1_34() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields, ___C2Pnb304w1_34)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_C2Pnb304w1_34() const { return ___C2Pnb304w1_34; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_C2Pnb304w1_34() { return &___C2Pnb304w1_34; }
	inline void set_C2Pnb304w1_34(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___C2Pnb304w1_34 = value;
		Il2CppCodeGenWriteBarrier((&___C2Pnb304w1_34), value);
	}

	inline static int32_t get_offset_of_C2Tnb359v1_35() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields, ___C2Tnb359v1_35)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_C2Tnb359v1_35() const { return ___C2Tnb359v1_35; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_C2Tnb359v1_35() { return &___C2Tnb359v1_35; }
	inline void set_C2Tnb359v1_35(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___C2Tnb359v1_35 = value;
		Il2CppCodeGenWriteBarrier((&___C2Tnb359v1_35), value);
	}

	inline static int32_t get_offset_of_C2Pnb368w1_36() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields, ___C2Pnb368w1_36)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_C2Pnb368w1_36() const { return ___C2Pnb368w1_36; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_C2Pnb368w1_36() { return &___C2Pnb368w1_36; }
	inline void set_C2Pnb368w1_36(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___C2Pnb368w1_36 = value;
		Il2CppCodeGenWriteBarrier((&___C2Pnb368w1_36), value);
	}

	inline static int32_t get_offset_of_C2Tnb431r1_37() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields, ___C2Tnb431r1_37)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_C2Tnb431r1_37() const { return ___C2Tnb431r1_37; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_C2Tnb431r1_37() { return &___C2Tnb431r1_37; }
	inline void set_C2Tnb431r1_37(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___C2Tnb431r1_37 = value;
		Il2CppCodeGenWriteBarrier((&___C2Tnb431r1_37), value);
	}

	inline static int32_t get_offset_of_PrimeCurve_38() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields, ___PrimeCurve_38)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_PrimeCurve_38() const { return ___PrimeCurve_38; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_PrimeCurve_38() { return &___PrimeCurve_38; }
	inline void set_PrimeCurve_38(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___PrimeCurve_38 = value;
		Il2CppCodeGenWriteBarrier((&___PrimeCurve_38), value);
	}

	inline static int32_t get_offset_of_Prime192v1_39() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields, ___Prime192v1_39)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_Prime192v1_39() const { return ___Prime192v1_39; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_Prime192v1_39() { return &___Prime192v1_39; }
	inline void set_Prime192v1_39(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___Prime192v1_39 = value;
		Il2CppCodeGenWriteBarrier((&___Prime192v1_39), value);
	}

	inline static int32_t get_offset_of_Prime192v2_40() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields, ___Prime192v2_40)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_Prime192v2_40() const { return ___Prime192v2_40; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_Prime192v2_40() { return &___Prime192v2_40; }
	inline void set_Prime192v2_40(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___Prime192v2_40 = value;
		Il2CppCodeGenWriteBarrier((&___Prime192v2_40), value);
	}

	inline static int32_t get_offset_of_Prime192v3_41() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields, ___Prime192v3_41)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_Prime192v3_41() const { return ___Prime192v3_41; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_Prime192v3_41() { return &___Prime192v3_41; }
	inline void set_Prime192v3_41(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___Prime192v3_41 = value;
		Il2CppCodeGenWriteBarrier((&___Prime192v3_41), value);
	}

	inline static int32_t get_offset_of_Prime239v1_42() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields, ___Prime239v1_42)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_Prime239v1_42() const { return ___Prime239v1_42; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_Prime239v1_42() { return &___Prime239v1_42; }
	inline void set_Prime239v1_42(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___Prime239v1_42 = value;
		Il2CppCodeGenWriteBarrier((&___Prime239v1_42), value);
	}

	inline static int32_t get_offset_of_Prime239v2_43() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields, ___Prime239v2_43)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_Prime239v2_43() const { return ___Prime239v2_43; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_Prime239v2_43() { return &___Prime239v2_43; }
	inline void set_Prime239v2_43(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___Prime239v2_43 = value;
		Il2CppCodeGenWriteBarrier((&___Prime239v2_43), value);
	}

	inline static int32_t get_offset_of_Prime239v3_44() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields, ___Prime239v3_44)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_Prime239v3_44() const { return ___Prime239v3_44; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_Prime239v3_44() { return &___Prime239v3_44; }
	inline void set_Prime239v3_44(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___Prime239v3_44 = value;
		Il2CppCodeGenWriteBarrier((&___Prime239v3_44), value);
	}

	inline static int32_t get_offset_of_Prime256v1_45() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields, ___Prime256v1_45)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_Prime256v1_45() const { return ___Prime256v1_45; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_Prime256v1_45() { return &___Prime256v1_45; }
	inline void set_Prime256v1_45(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___Prime256v1_45 = value;
		Il2CppCodeGenWriteBarrier((&___Prime256v1_45), value);
	}

	inline static int32_t get_offset_of_IdDsa_46() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields, ___IdDsa_46)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdDsa_46() const { return ___IdDsa_46; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdDsa_46() { return &___IdDsa_46; }
	inline void set_IdDsa_46(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdDsa_46 = value;
		Il2CppCodeGenWriteBarrier((&___IdDsa_46), value);
	}

	inline static int32_t get_offset_of_IdDsaWithSha1_47() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields, ___IdDsaWithSha1_47)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_IdDsaWithSha1_47() const { return ___IdDsaWithSha1_47; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_IdDsaWithSha1_47() { return &___IdDsaWithSha1_47; }
	inline void set_IdDsaWithSha1_47(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___IdDsaWithSha1_47 = value;
		Il2CppCodeGenWriteBarrier((&___IdDsaWithSha1_47), value);
	}

	inline static int32_t get_offset_of_X9x63Scheme_48() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields, ___X9x63Scheme_48)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_X9x63Scheme_48() const { return ___X9x63Scheme_48; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_X9x63Scheme_48() { return &___X9x63Scheme_48; }
	inline void set_X9x63Scheme_48(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___X9x63Scheme_48 = value;
		Il2CppCodeGenWriteBarrier((&___X9x63Scheme_48), value);
	}

	inline static int32_t get_offset_of_DHSinglePassStdDHSha1KdfScheme_49() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields, ___DHSinglePassStdDHSha1KdfScheme_49)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_DHSinglePassStdDHSha1KdfScheme_49() const { return ___DHSinglePassStdDHSha1KdfScheme_49; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_DHSinglePassStdDHSha1KdfScheme_49() { return &___DHSinglePassStdDHSha1KdfScheme_49; }
	inline void set_DHSinglePassStdDHSha1KdfScheme_49(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___DHSinglePassStdDHSha1KdfScheme_49 = value;
		Il2CppCodeGenWriteBarrier((&___DHSinglePassStdDHSha1KdfScheme_49), value);
	}

	inline static int32_t get_offset_of_DHSinglePassCofactorDHSha1KdfScheme_50() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields, ___DHSinglePassCofactorDHSha1KdfScheme_50)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_DHSinglePassCofactorDHSha1KdfScheme_50() const { return ___DHSinglePassCofactorDHSha1KdfScheme_50; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_DHSinglePassCofactorDHSha1KdfScheme_50() { return &___DHSinglePassCofactorDHSha1KdfScheme_50; }
	inline void set_DHSinglePassCofactorDHSha1KdfScheme_50(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___DHSinglePassCofactorDHSha1KdfScheme_50 = value;
		Il2CppCodeGenWriteBarrier((&___DHSinglePassCofactorDHSha1KdfScheme_50), value);
	}

	inline static int32_t get_offset_of_MqvSinglePassSha1KdfScheme_51() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields, ___MqvSinglePassSha1KdfScheme_51)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_MqvSinglePassSha1KdfScheme_51() const { return ___MqvSinglePassSha1KdfScheme_51; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_MqvSinglePassSha1KdfScheme_51() { return &___MqvSinglePassSha1KdfScheme_51; }
	inline void set_MqvSinglePassSha1KdfScheme_51(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___MqvSinglePassSha1KdfScheme_51 = value;
		Il2CppCodeGenWriteBarrier((&___MqvSinglePassSha1KdfScheme_51), value);
	}

	inline static int32_t get_offset_of_ansi_x9_42_52() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields, ___ansi_x9_42_52)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_ansi_x9_42_52() const { return ___ansi_x9_42_52; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_ansi_x9_42_52() { return &___ansi_x9_42_52; }
	inline void set_ansi_x9_42_52(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___ansi_x9_42_52 = value;
		Il2CppCodeGenWriteBarrier((&___ansi_x9_42_52), value);
	}

	inline static int32_t get_offset_of_DHPublicNumber_53() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields, ___DHPublicNumber_53)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_DHPublicNumber_53() const { return ___DHPublicNumber_53; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_DHPublicNumber_53() { return &___DHPublicNumber_53; }
	inline void set_DHPublicNumber_53(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___DHPublicNumber_53 = value;
		Il2CppCodeGenWriteBarrier((&___DHPublicNumber_53), value);
	}

	inline static int32_t get_offset_of_X9x42Schemes_54() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields, ___X9x42Schemes_54)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_X9x42Schemes_54() const { return ___X9x42Schemes_54; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_X9x42Schemes_54() { return &___X9x42Schemes_54; }
	inline void set_X9x42Schemes_54(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___X9x42Schemes_54 = value;
		Il2CppCodeGenWriteBarrier((&___X9x42Schemes_54), value);
	}

	inline static int32_t get_offset_of_DHStatic_55() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields, ___DHStatic_55)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_DHStatic_55() const { return ___DHStatic_55; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_DHStatic_55() { return &___DHStatic_55; }
	inline void set_DHStatic_55(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___DHStatic_55 = value;
		Il2CppCodeGenWriteBarrier((&___DHStatic_55), value);
	}

	inline static int32_t get_offset_of_DHEphem_56() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields, ___DHEphem_56)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_DHEphem_56() const { return ___DHEphem_56; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_DHEphem_56() { return &___DHEphem_56; }
	inline void set_DHEphem_56(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___DHEphem_56 = value;
		Il2CppCodeGenWriteBarrier((&___DHEphem_56), value);
	}

	inline static int32_t get_offset_of_DHOneFlow_57() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields, ___DHOneFlow_57)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_DHOneFlow_57() const { return ___DHOneFlow_57; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_DHOneFlow_57() { return &___DHOneFlow_57; }
	inline void set_DHOneFlow_57(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___DHOneFlow_57 = value;
		Il2CppCodeGenWriteBarrier((&___DHOneFlow_57), value);
	}

	inline static int32_t get_offset_of_DHHybrid1_58() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields, ___DHHybrid1_58)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_DHHybrid1_58() const { return ___DHHybrid1_58; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_DHHybrid1_58() { return &___DHHybrid1_58; }
	inline void set_DHHybrid1_58(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___DHHybrid1_58 = value;
		Il2CppCodeGenWriteBarrier((&___DHHybrid1_58), value);
	}

	inline static int32_t get_offset_of_DHHybrid2_59() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields, ___DHHybrid2_59)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_DHHybrid2_59() const { return ___DHHybrid2_59; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_DHHybrid2_59() { return &___DHHybrid2_59; }
	inline void set_DHHybrid2_59(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___DHHybrid2_59 = value;
		Il2CppCodeGenWriteBarrier((&___DHHybrid2_59), value);
	}

	inline static int32_t get_offset_of_DHHybridOneFlow_60() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields, ___DHHybridOneFlow_60)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_DHHybridOneFlow_60() const { return ___DHHybridOneFlow_60; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_DHHybridOneFlow_60() { return &___DHHybridOneFlow_60; }
	inline void set_DHHybridOneFlow_60(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___DHHybridOneFlow_60 = value;
		Il2CppCodeGenWriteBarrier((&___DHHybridOneFlow_60), value);
	}

	inline static int32_t get_offset_of_Mqv2_61() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields, ___Mqv2_61)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_Mqv2_61() const { return ___Mqv2_61; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_Mqv2_61() { return &___Mqv2_61; }
	inline void set_Mqv2_61(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___Mqv2_61 = value;
		Il2CppCodeGenWriteBarrier((&___Mqv2_61), value);
	}

	inline static int32_t get_offset_of_Mqv1_62() { return static_cast<int32_t>(offsetof(X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields, ___Mqv1_62)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_Mqv1_62() const { return ___Mqv1_62; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_Mqv1_62() { return &___Mqv1_62; }
	inline void set_Mqv1_62(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___Mqv1_62 = value;
		Il2CppCodeGenWriteBarrier((&___Mqv1_62), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X9OBJECTIDENTIFIERS_T3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_H
#ifndef DHBASICAGREEMENT_T6EE26230D930EE4EF01B84A796634B99F42315BA_H
#define DHBASICAGREEMENT_T6EE26230D930EE4EF01B84A796634B99F42315BA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Agreement.DHBasicAgreement
struct  DHBasicAgreement_t6EE26230D930EE4EF01B84A796634B99F42315BA  : public RuntimeObject
{
public:
	// Org.BouncyCastle.Crypto.Parameters.DHPrivateKeyParameters Org.BouncyCastle.Crypto.Agreement.DHBasicAgreement::key
	DHPrivateKeyParameters_t7A930900443BCA929045970B99A92EADD777049C * ___key_0;
	// Org.BouncyCastle.Crypto.Parameters.DHParameters Org.BouncyCastle.Crypto.Agreement.DHBasicAgreement::dhParams
	DHParameters_t15DBCF5CD22F83E806A78FF80CE2853863825513 * ___dhParams_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(DHBasicAgreement_t6EE26230D930EE4EF01B84A796634B99F42315BA, ___key_0)); }
	inline DHPrivateKeyParameters_t7A930900443BCA929045970B99A92EADD777049C * get_key_0() const { return ___key_0; }
	inline DHPrivateKeyParameters_t7A930900443BCA929045970B99A92EADD777049C ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(DHPrivateKeyParameters_t7A930900443BCA929045970B99A92EADD777049C * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_dhParams_1() { return static_cast<int32_t>(offsetof(DHBasicAgreement_t6EE26230D930EE4EF01B84A796634B99F42315BA, ___dhParams_1)); }
	inline DHParameters_t15DBCF5CD22F83E806A78FF80CE2853863825513 * get_dhParams_1() const { return ___dhParams_1; }
	inline DHParameters_t15DBCF5CD22F83E806A78FF80CE2853863825513 ** get_address_of_dhParams_1() { return &___dhParams_1; }
	inline void set_dhParams_1(DHParameters_t15DBCF5CD22F83E806A78FF80CE2853863825513 * value)
	{
		___dhParams_1 = value;
		Il2CppCodeGenWriteBarrier((&___dhParams_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DHBASICAGREEMENT_T6EE26230D930EE4EF01B84A796634B99F42315BA_H
#ifndef ECDHBASICAGREEMENT_T1CD435E5D2E794AAB5162C860E4D29FDE8E16527_H
#define ECDHBASICAGREEMENT_T1CD435E5D2E794AAB5162C860E4D29FDE8E16527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Agreement.ECDHBasicAgreement
struct  ECDHBasicAgreement_t1CD435E5D2E794AAB5162C860E4D29FDE8E16527  : public RuntimeObject
{
public:
	// Org.BouncyCastle.Crypto.Parameters.ECPrivateKeyParameters Org.BouncyCastle.Crypto.Agreement.ECDHBasicAgreement::privKey
	ECPrivateKeyParameters_tC1317EEDAEF16688C98CD41053BB95C6751BBD1F * ___privKey_0;

public:
	inline static int32_t get_offset_of_privKey_0() { return static_cast<int32_t>(offsetof(ECDHBasicAgreement_t1CD435E5D2E794AAB5162C860E4D29FDE8E16527, ___privKey_0)); }
	inline ECPrivateKeyParameters_tC1317EEDAEF16688C98CD41053BB95C6751BBD1F * get_privKey_0() const { return ___privKey_0; }
	inline ECPrivateKeyParameters_tC1317EEDAEF16688C98CD41053BB95C6751BBD1F ** get_address_of_privKey_0() { return &___privKey_0; }
	inline void set_privKey_0(ECPrivateKeyParameters_tC1317EEDAEF16688C98CD41053BB95C6751BBD1F * value)
	{
		___privKey_0 = value;
		Il2CppCodeGenWriteBarrier((&___privKey_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECDHBASICAGREEMENT_T1CD435E5D2E794AAB5162C860E4D29FDE8E16527_H
#ifndef ASYMMETRICCIPHERKEYPAIR_T96A8B415347C6BD7702CCBB377729ADF4E3E8342_H
#define ASYMMETRICCIPHERKEYPAIR_T96A8B415347C6BD7702CCBB377729ADF4E3E8342_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.AsymmetricCipherKeyPair
struct  AsymmetricCipherKeyPair_t96A8B415347C6BD7702CCBB377729ADF4E3E8342  : public RuntimeObject
{
public:
	// Org.BouncyCastle.Crypto.AsymmetricKeyParameter Org.BouncyCastle.Crypto.AsymmetricCipherKeyPair::publicParameter
	AsymmetricKeyParameter_tD6918D64A0FB774AF79E386A85415CE53D5EC0D7 * ___publicParameter_0;
	// Org.BouncyCastle.Crypto.AsymmetricKeyParameter Org.BouncyCastle.Crypto.AsymmetricCipherKeyPair::privateParameter
	AsymmetricKeyParameter_tD6918D64A0FB774AF79E386A85415CE53D5EC0D7 * ___privateParameter_1;

public:
	inline static int32_t get_offset_of_publicParameter_0() { return static_cast<int32_t>(offsetof(AsymmetricCipherKeyPair_t96A8B415347C6BD7702CCBB377729ADF4E3E8342, ___publicParameter_0)); }
	inline AsymmetricKeyParameter_tD6918D64A0FB774AF79E386A85415CE53D5EC0D7 * get_publicParameter_0() const { return ___publicParameter_0; }
	inline AsymmetricKeyParameter_tD6918D64A0FB774AF79E386A85415CE53D5EC0D7 ** get_address_of_publicParameter_0() { return &___publicParameter_0; }
	inline void set_publicParameter_0(AsymmetricKeyParameter_tD6918D64A0FB774AF79E386A85415CE53D5EC0D7 * value)
	{
		___publicParameter_0 = value;
		Il2CppCodeGenWriteBarrier((&___publicParameter_0), value);
	}

	inline static int32_t get_offset_of_privateParameter_1() { return static_cast<int32_t>(offsetof(AsymmetricCipherKeyPair_t96A8B415347C6BD7702CCBB377729ADF4E3E8342, ___privateParameter_1)); }
	inline AsymmetricKeyParameter_tD6918D64A0FB774AF79E386A85415CE53D5EC0D7 * get_privateParameter_1() const { return ___privateParameter_1; }
	inline AsymmetricKeyParameter_tD6918D64A0FB774AF79E386A85415CE53D5EC0D7 ** get_address_of_privateParameter_1() { return &___privateParameter_1; }
	inline void set_privateParameter_1(AsymmetricKeyParameter_tD6918D64A0FB774AF79E386A85415CE53D5EC0D7 * value)
	{
		___privateParameter_1 = value;
		Il2CppCodeGenWriteBarrier((&___privateParameter_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYMMETRICCIPHERKEYPAIR_T96A8B415347C6BD7702CCBB377729ADF4E3E8342_H
#ifndef ASYMMETRICKEYPARAMETER_TD6918D64A0FB774AF79E386A85415CE53D5EC0D7_H
#define ASYMMETRICKEYPARAMETER_TD6918D64A0FB774AF79E386A85415CE53D5EC0D7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.AsymmetricKeyParameter
struct  AsymmetricKeyParameter_tD6918D64A0FB774AF79E386A85415CE53D5EC0D7  : public RuntimeObject
{
public:
	// System.Boolean Org.BouncyCastle.Crypto.AsymmetricKeyParameter::privateKey
	bool ___privateKey_0;

public:
	inline static int32_t get_offset_of_privateKey_0() { return static_cast<int32_t>(offsetof(AsymmetricKeyParameter_tD6918D64A0FB774AF79E386A85415CE53D5EC0D7, ___privateKey_0)); }
	inline bool get_privateKey_0() const { return ___privateKey_0; }
	inline bool* get_address_of_privateKey_0() { return &___privateKey_0; }
	inline void set_privateKey_0(bool value)
	{
		___privateKey_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYMMETRICKEYPARAMETER_TD6918D64A0FB774AF79E386A85415CE53D5EC0D7_H
#ifndef CHECK_T6DF797A6EACE96C6B22C06FAE9A43E0EF147D0D1_H
#define CHECK_T6DF797A6EACE96C6B22C06FAE9A43E0EF147D0D1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Check
struct  Check_t6DF797A6EACE96C6B22C06FAE9A43E0EF147D0D1  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHECK_T6DF797A6EACE96C6B22C06FAE9A43E0EF147D0D1_H
#ifndef CIPHERKEYGENERATOR_TB9597B68C6815C63C622AA3320A0E81BF8568CE1_H
#define CIPHERKEYGENERATOR_TB9597B68C6815C63C622AA3320A0E81BF8568CE1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.CipherKeyGenerator
struct  CipherKeyGenerator_tB9597B68C6815C63C622AA3320A0E81BF8568CE1  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CIPHERKEYGENERATOR_TB9597B68C6815C63C622AA3320A0E81BF8568CE1_H
#ifndef GENERALDIGEST_T3B10A5BF867429F885F41CE318BA1665427D50D6_H
#define GENERALDIGEST_T3B10A5BF867429F885F41CE318BA1665427D50D6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Digests.GeneralDigest
struct  GeneralDigest_t3B10A5BF867429F885F41CE318BA1665427D50D6  : public RuntimeObject
{
public:
	// System.Byte[] Org.BouncyCastle.Crypto.Digests.GeneralDigest::xBuf
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___xBuf_0;
	// System.Int32 Org.BouncyCastle.Crypto.Digests.GeneralDigest::xBufOff
	int32_t ___xBufOff_1;
	// System.Int64 Org.BouncyCastle.Crypto.Digests.GeneralDigest::byteCount
	int64_t ___byteCount_2;

public:
	inline static int32_t get_offset_of_xBuf_0() { return static_cast<int32_t>(offsetof(GeneralDigest_t3B10A5BF867429F885F41CE318BA1665427D50D6, ___xBuf_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_xBuf_0() const { return ___xBuf_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_xBuf_0() { return &___xBuf_0; }
	inline void set_xBuf_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___xBuf_0 = value;
		Il2CppCodeGenWriteBarrier((&___xBuf_0), value);
	}

	inline static int32_t get_offset_of_xBufOff_1() { return static_cast<int32_t>(offsetof(GeneralDigest_t3B10A5BF867429F885F41CE318BA1665427D50D6, ___xBufOff_1)); }
	inline int32_t get_xBufOff_1() const { return ___xBufOff_1; }
	inline int32_t* get_address_of_xBufOff_1() { return &___xBufOff_1; }
	inline void set_xBufOff_1(int32_t value)
	{
		___xBufOff_1 = value;
	}

	inline static int32_t get_offset_of_byteCount_2() { return static_cast<int32_t>(offsetof(GeneralDigest_t3B10A5BF867429F885F41CE318BA1665427D50D6, ___byteCount_2)); }
	inline int64_t get_byteCount_2() const { return ___byteCount_2; }
	inline int64_t* get_address_of_byteCount_2() { return &___byteCount_2; }
	inline void set_byteCount_2(int64_t value)
	{
		___byteCount_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERALDIGEST_T3B10A5BF867429F885F41CE318BA1665427D50D6_H
#ifndef GOST3411DIGEST_TB644E93B56F8C4CA364A53FE2975CDB5F29FE0AE_H
#define GOST3411DIGEST_TB644E93B56F8C4CA364A53FE2975CDB5F29FE0AE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Digests.Gost3411Digest
struct  Gost3411Digest_tB644E93B56F8C4CA364A53FE2975CDB5F29FE0AE  : public RuntimeObject
{
public:
	// System.Byte[] Org.BouncyCastle.Crypto.Digests.Gost3411Digest::H
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___H_0;
	// System.Byte[] Org.BouncyCastle.Crypto.Digests.Gost3411Digest::L
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___L_1;
	// System.Byte[] Org.BouncyCastle.Crypto.Digests.Gost3411Digest::M
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___M_2;
	// System.Byte[] Org.BouncyCastle.Crypto.Digests.Gost3411Digest::Sum
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___Sum_3;
	// System.Byte[][] Org.BouncyCastle.Crypto.Digests.Gost3411Digest::C
	ByteU5BU5DU5BU5D_tD1CB918775FFB351821F10AC338FECDDE22DEEC7* ___C_4;
	// System.Byte[] Org.BouncyCastle.Crypto.Digests.Gost3411Digest::xBuf
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___xBuf_5;
	// System.Int32 Org.BouncyCastle.Crypto.Digests.Gost3411Digest::xBufOff
	int32_t ___xBufOff_6;
	// System.UInt64 Org.BouncyCastle.Crypto.Digests.Gost3411Digest::byteCount
	uint64_t ___byteCount_7;
	// Org.BouncyCastle.Crypto.IBlockCipher Org.BouncyCastle.Crypto.Digests.Gost3411Digest::cipher
	RuntimeObject* ___cipher_8;
	// System.Byte[] Org.BouncyCastle.Crypto.Digests.Gost3411Digest::sBox
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___sBox_9;
	// System.Byte[] Org.BouncyCastle.Crypto.Digests.Gost3411Digest::K
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___K_10;
	// System.Byte[] Org.BouncyCastle.Crypto.Digests.Gost3411Digest::a
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___a_11;
	// System.Int16[] Org.BouncyCastle.Crypto.Digests.Gost3411Digest::wS
	Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* ___wS_12;
	// System.Int16[] Org.BouncyCastle.Crypto.Digests.Gost3411Digest::w_S
	Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* ___w_S_13;
	// System.Byte[] Org.BouncyCastle.Crypto.Digests.Gost3411Digest::S
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___S_14;
	// System.Byte[] Org.BouncyCastle.Crypto.Digests.Gost3411Digest::U
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___U_15;
	// System.Byte[] Org.BouncyCastle.Crypto.Digests.Gost3411Digest::V
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___V_16;
	// System.Byte[] Org.BouncyCastle.Crypto.Digests.Gost3411Digest::W
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___W_17;

public:
	inline static int32_t get_offset_of_H_0() { return static_cast<int32_t>(offsetof(Gost3411Digest_tB644E93B56F8C4CA364A53FE2975CDB5F29FE0AE, ___H_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_H_0() const { return ___H_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_H_0() { return &___H_0; }
	inline void set_H_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___H_0 = value;
		Il2CppCodeGenWriteBarrier((&___H_0), value);
	}

	inline static int32_t get_offset_of_L_1() { return static_cast<int32_t>(offsetof(Gost3411Digest_tB644E93B56F8C4CA364A53FE2975CDB5F29FE0AE, ___L_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_L_1() const { return ___L_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_L_1() { return &___L_1; }
	inline void set_L_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___L_1 = value;
		Il2CppCodeGenWriteBarrier((&___L_1), value);
	}

	inline static int32_t get_offset_of_M_2() { return static_cast<int32_t>(offsetof(Gost3411Digest_tB644E93B56F8C4CA364A53FE2975CDB5F29FE0AE, ___M_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_M_2() const { return ___M_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_M_2() { return &___M_2; }
	inline void set_M_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___M_2 = value;
		Il2CppCodeGenWriteBarrier((&___M_2), value);
	}

	inline static int32_t get_offset_of_Sum_3() { return static_cast<int32_t>(offsetof(Gost3411Digest_tB644E93B56F8C4CA364A53FE2975CDB5F29FE0AE, ___Sum_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_Sum_3() const { return ___Sum_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_Sum_3() { return &___Sum_3; }
	inline void set_Sum_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___Sum_3 = value;
		Il2CppCodeGenWriteBarrier((&___Sum_3), value);
	}

	inline static int32_t get_offset_of_C_4() { return static_cast<int32_t>(offsetof(Gost3411Digest_tB644E93B56F8C4CA364A53FE2975CDB5F29FE0AE, ___C_4)); }
	inline ByteU5BU5DU5BU5D_tD1CB918775FFB351821F10AC338FECDDE22DEEC7* get_C_4() const { return ___C_4; }
	inline ByteU5BU5DU5BU5D_tD1CB918775FFB351821F10AC338FECDDE22DEEC7** get_address_of_C_4() { return &___C_4; }
	inline void set_C_4(ByteU5BU5DU5BU5D_tD1CB918775FFB351821F10AC338FECDDE22DEEC7* value)
	{
		___C_4 = value;
		Il2CppCodeGenWriteBarrier((&___C_4), value);
	}

	inline static int32_t get_offset_of_xBuf_5() { return static_cast<int32_t>(offsetof(Gost3411Digest_tB644E93B56F8C4CA364A53FE2975CDB5F29FE0AE, ___xBuf_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_xBuf_5() const { return ___xBuf_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_xBuf_5() { return &___xBuf_5; }
	inline void set_xBuf_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___xBuf_5 = value;
		Il2CppCodeGenWriteBarrier((&___xBuf_5), value);
	}

	inline static int32_t get_offset_of_xBufOff_6() { return static_cast<int32_t>(offsetof(Gost3411Digest_tB644E93B56F8C4CA364A53FE2975CDB5F29FE0AE, ___xBufOff_6)); }
	inline int32_t get_xBufOff_6() const { return ___xBufOff_6; }
	inline int32_t* get_address_of_xBufOff_6() { return &___xBufOff_6; }
	inline void set_xBufOff_6(int32_t value)
	{
		___xBufOff_6 = value;
	}

	inline static int32_t get_offset_of_byteCount_7() { return static_cast<int32_t>(offsetof(Gost3411Digest_tB644E93B56F8C4CA364A53FE2975CDB5F29FE0AE, ___byteCount_7)); }
	inline uint64_t get_byteCount_7() const { return ___byteCount_7; }
	inline uint64_t* get_address_of_byteCount_7() { return &___byteCount_7; }
	inline void set_byteCount_7(uint64_t value)
	{
		___byteCount_7 = value;
	}

	inline static int32_t get_offset_of_cipher_8() { return static_cast<int32_t>(offsetof(Gost3411Digest_tB644E93B56F8C4CA364A53FE2975CDB5F29FE0AE, ___cipher_8)); }
	inline RuntimeObject* get_cipher_8() const { return ___cipher_8; }
	inline RuntimeObject** get_address_of_cipher_8() { return &___cipher_8; }
	inline void set_cipher_8(RuntimeObject* value)
	{
		___cipher_8 = value;
		Il2CppCodeGenWriteBarrier((&___cipher_8), value);
	}

	inline static int32_t get_offset_of_sBox_9() { return static_cast<int32_t>(offsetof(Gost3411Digest_tB644E93B56F8C4CA364A53FE2975CDB5F29FE0AE, ___sBox_9)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_sBox_9() const { return ___sBox_9; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_sBox_9() { return &___sBox_9; }
	inline void set_sBox_9(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___sBox_9 = value;
		Il2CppCodeGenWriteBarrier((&___sBox_9), value);
	}

	inline static int32_t get_offset_of_K_10() { return static_cast<int32_t>(offsetof(Gost3411Digest_tB644E93B56F8C4CA364A53FE2975CDB5F29FE0AE, ___K_10)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_K_10() const { return ___K_10; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_K_10() { return &___K_10; }
	inline void set_K_10(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___K_10 = value;
		Il2CppCodeGenWriteBarrier((&___K_10), value);
	}

	inline static int32_t get_offset_of_a_11() { return static_cast<int32_t>(offsetof(Gost3411Digest_tB644E93B56F8C4CA364A53FE2975CDB5F29FE0AE, ___a_11)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_a_11() const { return ___a_11; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_a_11() { return &___a_11; }
	inline void set_a_11(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___a_11 = value;
		Il2CppCodeGenWriteBarrier((&___a_11), value);
	}

	inline static int32_t get_offset_of_wS_12() { return static_cast<int32_t>(offsetof(Gost3411Digest_tB644E93B56F8C4CA364A53FE2975CDB5F29FE0AE, ___wS_12)); }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* get_wS_12() const { return ___wS_12; }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28** get_address_of_wS_12() { return &___wS_12; }
	inline void set_wS_12(Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* value)
	{
		___wS_12 = value;
		Il2CppCodeGenWriteBarrier((&___wS_12), value);
	}

	inline static int32_t get_offset_of_w_S_13() { return static_cast<int32_t>(offsetof(Gost3411Digest_tB644E93B56F8C4CA364A53FE2975CDB5F29FE0AE, ___w_S_13)); }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* get_w_S_13() const { return ___w_S_13; }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28** get_address_of_w_S_13() { return &___w_S_13; }
	inline void set_w_S_13(Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* value)
	{
		___w_S_13 = value;
		Il2CppCodeGenWriteBarrier((&___w_S_13), value);
	}

	inline static int32_t get_offset_of_S_14() { return static_cast<int32_t>(offsetof(Gost3411Digest_tB644E93B56F8C4CA364A53FE2975CDB5F29FE0AE, ___S_14)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_S_14() const { return ___S_14; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_S_14() { return &___S_14; }
	inline void set_S_14(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___S_14 = value;
		Il2CppCodeGenWriteBarrier((&___S_14), value);
	}

	inline static int32_t get_offset_of_U_15() { return static_cast<int32_t>(offsetof(Gost3411Digest_tB644E93B56F8C4CA364A53FE2975CDB5F29FE0AE, ___U_15)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_U_15() const { return ___U_15; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_U_15() { return &___U_15; }
	inline void set_U_15(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___U_15 = value;
		Il2CppCodeGenWriteBarrier((&___U_15), value);
	}

	inline static int32_t get_offset_of_V_16() { return static_cast<int32_t>(offsetof(Gost3411Digest_tB644E93B56F8C4CA364A53FE2975CDB5F29FE0AE, ___V_16)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_V_16() const { return ___V_16; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_V_16() { return &___V_16; }
	inline void set_V_16(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___V_16 = value;
		Il2CppCodeGenWriteBarrier((&___V_16), value);
	}

	inline static int32_t get_offset_of_W_17() { return static_cast<int32_t>(offsetof(Gost3411Digest_tB644E93B56F8C4CA364A53FE2975CDB5F29FE0AE, ___W_17)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_W_17() const { return ___W_17; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_W_17() { return &___W_17; }
	inline void set_W_17(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___W_17 = value;
		Il2CppCodeGenWriteBarrier((&___W_17), value);
	}
};

struct Gost3411Digest_tB644E93B56F8C4CA364A53FE2975CDB5F29FE0AE_StaticFields
{
public:
	// System.Byte[] Org.BouncyCastle.Crypto.Digests.Gost3411Digest::C2
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___C2_18;

public:
	inline static int32_t get_offset_of_C2_18() { return static_cast<int32_t>(offsetof(Gost3411Digest_tB644E93B56F8C4CA364A53FE2975CDB5F29FE0AE_StaticFields, ___C2_18)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_C2_18() const { return ___C2_18; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_C2_18() { return &___C2_18; }
	inline void set_C2_18(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___C2_18 = value;
		Il2CppCodeGenWriteBarrier((&___C2_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GOST3411DIGEST_TB644E93B56F8C4CA364A53FE2975CDB5F29FE0AE_H
#ifndef KECCAKDIGEST_TEFB4CFB19A2FC24160E02A8821D4BAE8862E3AEB_H
#define KECCAKDIGEST_TEFB4CFB19A2FC24160E02A8821D4BAE8862E3AEB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Digests.KeccakDigest
struct  KeccakDigest_tEFB4CFB19A2FC24160E02A8821D4BAE8862E3AEB  : public RuntimeObject
{
public:
	// System.Byte[] Org.BouncyCastle.Crypto.Digests.KeccakDigest::state
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___state_2;
	// System.Byte[] Org.BouncyCastle.Crypto.Digests.KeccakDigest::dataQueue
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___dataQueue_3;
	// System.Int32 Org.BouncyCastle.Crypto.Digests.KeccakDigest::rate
	int32_t ___rate_4;
	// System.Int32 Org.BouncyCastle.Crypto.Digests.KeccakDigest::bitsInQueue
	int32_t ___bitsInQueue_5;
	// System.Int32 Org.BouncyCastle.Crypto.Digests.KeccakDigest::fixedOutputLength
	int32_t ___fixedOutputLength_6;
	// System.Boolean Org.BouncyCastle.Crypto.Digests.KeccakDigest::squeezing
	bool ___squeezing_7;
	// System.Int32 Org.BouncyCastle.Crypto.Digests.KeccakDigest::bitsAvailableForSqueezing
	int32_t ___bitsAvailableForSqueezing_8;
	// System.Byte[] Org.BouncyCastle.Crypto.Digests.KeccakDigest::chunk
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___chunk_9;
	// System.Byte[] Org.BouncyCastle.Crypto.Digests.KeccakDigest::oneByte
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___oneByte_10;
	// System.UInt64[] Org.BouncyCastle.Crypto.Digests.KeccakDigest::C
	UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* ___C_11;
	// System.UInt64[] Org.BouncyCastle.Crypto.Digests.KeccakDigest::tempA
	UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* ___tempA_12;
	// System.UInt64[] Org.BouncyCastle.Crypto.Digests.KeccakDigest::chiC
	UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* ___chiC_13;

public:
	inline static int32_t get_offset_of_state_2() { return static_cast<int32_t>(offsetof(KeccakDigest_tEFB4CFB19A2FC24160E02A8821D4BAE8862E3AEB, ___state_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_state_2() const { return ___state_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_state_2() { return &___state_2; }
	inline void set_state_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___state_2 = value;
		Il2CppCodeGenWriteBarrier((&___state_2), value);
	}

	inline static int32_t get_offset_of_dataQueue_3() { return static_cast<int32_t>(offsetof(KeccakDigest_tEFB4CFB19A2FC24160E02A8821D4BAE8862E3AEB, ___dataQueue_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_dataQueue_3() const { return ___dataQueue_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_dataQueue_3() { return &___dataQueue_3; }
	inline void set_dataQueue_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___dataQueue_3 = value;
		Il2CppCodeGenWriteBarrier((&___dataQueue_3), value);
	}

	inline static int32_t get_offset_of_rate_4() { return static_cast<int32_t>(offsetof(KeccakDigest_tEFB4CFB19A2FC24160E02A8821D4BAE8862E3AEB, ___rate_4)); }
	inline int32_t get_rate_4() const { return ___rate_4; }
	inline int32_t* get_address_of_rate_4() { return &___rate_4; }
	inline void set_rate_4(int32_t value)
	{
		___rate_4 = value;
	}

	inline static int32_t get_offset_of_bitsInQueue_5() { return static_cast<int32_t>(offsetof(KeccakDigest_tEFB4CFB19A2FC24160E02A8821D4BAE8862E3AEB, ___bitsInQueue_5)); }
	inline int32_t get_bitsInQueue_5() const { return ___bitsInQueue_5; }
	inline int32_t* get_address_of_bitsInQueue_5() { return &___bitsInQueue_5; }
	inline void set_bitsInQueue_5(int32_t value)
	{
		___bitsInQueue_5 = value;
	}

	inline static int32_t get_offset_of_fixedOutputLength_6() { return static_cast<int32_t>(offsetof(KeccakDigest_tEFB4CFB19A2FC24160E02A8821D4BAE8862E3AEB, ___fixedOutputLength_6)); }
	inline int32_t get_fixedOutputLength_6() const { return ___fixedOutputLength_6; }
	inline int32_t* get_address_of_fixedOutputLength_6() { return &___fixedOutputLength_6; }
	inline void set_fixedOutputLength_6(int32_t value)
	{
		___fixedOutputLength_6 = value;
	}

	inline static int32_t get_offset_of_squeezing_7() { return static_cast<int32_t>(offsetof(KeccakDigest_tEFB4CFB19A2FC24160E02A8821D4BAE8862E3AEB, ___squeezing_7)); }
	inline bool get_squeezing_7() const { return ___squeezing_7; }
	inline bool* get_address_of_squeezing_7() { return &___squeezing_7; }
	inline void set_squeezing_7(bool value)
	{
		___squeezing_7 = value;
	}

	inline static int32_t get_offset_of_bitsAvailableForSqueezing_8() { return static_cast<int32_t>(offsetof(KeccakDigest_tEFB4CFB19A2FC24160E02A8821D4BAE8862E3AEB, ___bitsAvailableForSqueezing_8)); }
	inline int32_t get_bitsAvailableForSqueezing_8() const { return ___bitsAvailableForSqueezing_8; }
	inline int32_t* get_address_of_bitsAvailableForSqueezing_8() { return &___bitsAvailableForSqueezing_8; }
	inline void set_bitsAvailableForSqueezing_8(int32_t value)
	{
		___bitsAvailableForSqueezing_8 = value;
	}

	inline static int32_t get_offset_of_chunk_9() { return static_cast<int32_t>(offsetof(KeccakDigest_tEFB4CFB19A2FC24160E02A8821D4BAE8862E3AEB, ___chunk_9)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_chunk_9() const { return ___chunk_9; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_chunk_9() { return &___chunk_9; }
	inline void set_chunk_9(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___chunk_9 = value;
		Il2CppCodeGenWriteBarrier((&___chunk_9), value);
	}

	inline static int32_t get_offset_of_oneByte_10() { return static_cast<int32_t>(offsetof(KeccakDigest_tEFB4CFB19A2FC24160E02A8821D4BAE8862E3AEB, ___oneByte_10)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_oneByte_10() const { return ___oneByte_10; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_oneByte_10() { return &___oneByte_10; }
	inline void set_oneByte_10(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___oneByte_10 = value;
		Il2CppCodeGenWriteBarrier((&___oneByte_10), value);
	}

	inline static int32_t get_offset_of_C_11() { return static_cast<int32_t>(offsetof(KeccakDigest_tEFB4CFB19A2FC24160E02A8821D4BAE8862E3AEB, ___C_11)); }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* get_C_11() const { return ___C_11; }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4** get_address_of_C_11() { return &___C_11; }
	inline void set_C_11(UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* value)
	{
		___C_11 = value;
		Il2CppCodeGenWriteBarrier((&___C_11), value);
	}

	inline static int32_t get_offset_of_tempA_12() { return static_cast<int32_t>(offsetof(KeccakDigest_tEFB4CFB19A2FC24160E02A8821D4BAE8862E3AEB, ___tempA_12)); }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* get_tempA_12() const { return ___tempA_12; }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4** get_address_of_tempA_12() { return &___tempA_12; }
	inline void set_tempA_12(UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* value)
	{
		___tempA_12 = value;
		Il2CppCodeGenWriteBarrier((&___tempA_12), value);
	}

	inline static int32_t get_offset_of_chiC_13() { return static_cast<int32_t>(offsetof(KeccakDigest_tEFB4CFB19A2FC24160E02A8821D4BAE8862E3AEB, ___chiC_13)); }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* get_chiC_13() const { return ___chiC_13; }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4** get_address_of_chiC_13() { return &___chiC_13; }
	inline void set_chiC_13(UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* value)
	{
		___chiC_13 = value;
		Il2CppCodeGenWriteBarrier((&___chiC_13), value);
	}
};

struct KeccakDigest_tEFB4CFB19A2FC24160E02A8821D4BAE8862E3AEB_StaticFields
{
public:
	// System.UInt64[] Org.BouncyCastle.Crypto.Digests.KeccakDigest::KeccakRoundConstants
	UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* ___KeccakRoundConstants_0;
	// System.Int32[] Org.BouncyCastle.Crypto.Digests.KeccakDigest::KeccakRhoOffsets
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___KeccakRhoOffsets_1;

public:
	inline static int32_t get_offset_of_KeccakRoundConstants_0() { return static_cast<int32_t>(offsetof(KeccakDigest_tEFB4CFB19A2FC24160E02A8821D4BAE8862E3AEB_StaticFields, ___KeccakRoundConstants_0)); }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* get_KeccakRoundConstants_0() const { return ___KeccakRoundConstants_0; }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4** get_address_of_KeccakRoundConstants_0() { return &___KeccakRoundConstants_0; }
	inline void set_KeccakRoundConstants_0(UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* value)
	{
		___KeccakRoundConstants_0 = value;
		Il2CppCodeGenWriteBarrier((&___KeccakRoundConstants_0), value);
	}

	inline static int32_t get_offset_of_KeccakRhoOffsets_1() { return static_cast<int32_t>(offsetof(KeccakDigest_tEFB4CFB19A2FC24160E02A8821D4BAE8862E3AEB_StaticFields, ___KeccakRhoOffsets_1)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_KeccakRhoOffsets_1() const { return ___KeccakRhoOffsets_1; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_KeccakRhoOffsets_1() { return &___KeccakRhoOffsets_1; }
	inline void set_KeccakRhoOffsets_1(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___KeccakRhoOffsets_1 = value;
		Il2CppCodeGenWriteBarrier((&___KeccakRhoOffsets_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KECCAKDIGEST_TEFB4CFB19A2FC24160E02A8821D4BAE8862E3AEB_H
#ifndef LONGDIGEST_T043705804892FF0397DAA2E061CCF2DE8BA8BB38_H
#define LONGDIGEST_T043705804892FF0397DAA2E061CCF2DE8BA8BB38_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Digests.LongDigest
struct  LongDigest_t043705804892FF0397DAA2E061CCF2DE8BA8BB38  : public RuntimeObject
{
public:
	// System.Int32 Org.BouncyCastle.Crypto.Digests.LongDigest::MyByteLength
	int32_t ___MyByteLength_0;
	// System.Byte[] Org.BouncyCastle.Crypto.Digests.LongDigest::xBuf
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___xBuf_1;
	// System.Int32 Org.BouncyCastle.Crypto.Digests.LongDigest::xBufOff
	int32_t ___xBufOff_2;
	// System.Int64 Org.BouncyCastle.Crypto.Digests.LongDigest::byteCount1
	int64_t ___byteCount1_3;
	// System.Int64 Org.BouncyCastle.Crypto.Digests.LongDigest::byteCount2
	int64_t ___byteCount2_4;
	// System.UInt64 Org.BouncyCastle.Crypto.Digests.LongDigest::H1
	uint64_t ___H1_5;
	// System.UInt64 Org.BouncyCastle.Crypto.Digests.LongDigest::H2
	uint64_t ___H2_6;
	// System.UInt64 Org.BouncyCastle.Crypto.Digests.LongDigest::H3
	uint64_t ___H3_7;
	// System.UInt64 Org.BouncyCastle.Crypto.Digests.LongDigest::H4
	uint64_t ___H4_8;
	// System.UInt64 Org.BouncyCastle.Crypto.Digests.LongDigest::H5
	uint64_t ___H5_9;
	// System.UInt64 Org.BouncyCastle.Crypto.Digests.LongDigest::H6
	uint64_t ___H6_10;
	// System.UInt64 Org.BouncyCastle.Crypto.Digests.LongDigest::H7
	uint64_t ___H7_11;
	// System.UInt64 Org.BouncyCastle.Crypto.Digests.LongDigest::H8
	uint64_t ___H8_12;
	// System.UInt64[] Org.BouncyCastle.Crypto.Digests.LongDigest::W
	UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* ___W_13;
	// System.Int32 Org.BouncyCastle.Crypto.Digests.LongDigest::wOff
	int32_t ___wOff_14;

public:
	inline static int32_t get_offset_of_MyByteLength_0() { return static_cast<int32_t>(offsetof(LongDigest_t043705804892FF0397DAA2E061CCF2DE8BA8BB38, ___MyByteLength_0)); }
	inline int32_t get_MyByteLength_0() const { return ___MyByteLength_0; }
	inline int32_t* get_address_of_MyByteLength_0() { return &___MyByteLength_0; }
	inline void set_MyByteLength_0(int32_t value)
	{
		___MyByteLength_0 = value;
	}

	inline static int32_t get_offset_of_xBuf_1() { return static_cast<int32_t>(offsetof(LongDigest_t043705804892FF0397DAA2E061CCF2DE8BA8BB38, ___xBuf_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_xBuf_1() const { return ___xBuf_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_xBuf_1() { return &___xBuf_1; }
	inline void set_xBuf_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___xBuf_1 = value;
		Il2CppCodeGenWriteBarrier((&___xBuf_1), value);
	}

	inline static int32_t get_offset_of_xBufOff_2() { return static_cast<int32_t>(offsetof(LongDigest_t043705804892FF0397DAA2E061CCF2DE8BA8BB38, ___xBufOff_2)); }
	inline int32_t get_xBufOff_2() const { return ___xBufOff_2; }
	inline int32_t* get_address_of_xBufOff_2() { return &___xBufOff_2; }
	inline void set_xBufOff_2(int32_t value)
	{
		___xBufOff_2 = value;
	}

	inline static int32_t get_offset_of_byteCount1_3() { return static_cast<int32_t>(offsetof(LongDigest_t043705804892FF0397DAA2E061CCF2DE8BA8BB38, ___byteCount1_3)); }
	inline int64_t get_byteCount1_3() const { return ___byteCount1_3; }
	inline int64_t* get_address_of_byteCount1_3() { return &___byteCount1_3; }
	inline void set_byteCount1_3(int64_t value)
	{
		___byteCount1_3 = value;
	}

	inline static int32_t get_offset_of_byteCount2_4() { return static_cast<int32_t>(offsetof(LongDigest_t043705804892FF0397DAA2E061CCF2DE8BA8BB38, ___byteCount2_4)); }
	inline int64_t get_byteCount2_4() const { return ___byteCount2_4; }
	inline int64_t* get_address_of_byteCount2_4() { return &___byteCount2_4; }
	inline void set_byteCount2_4(int64_t value)
	{
		___byteCount2_4 = value;
	}

	inline static int32_t get_offset_of_H1_5() { return static_cast<int32_t>(offsetof(LongDigest_t043705804892FF0397DAA2E061CCF2DE8BA8BB38, ___H1_5)); }
	inline uint64_t get_H1_5() const { return ___H1_5; }
	inline uint64_t* get_address_of_H1_5() { return &___H1_5; }
	inline void set_H1_5(uint64_t value)
	{
		___H1_5 = value;
	}

	inline static int32_t get_offset_of_H2_6() { return static_cast<int32_t>(offsetof(LongDigest_t043705804892FF0397DAA2E061CCF2DE8BA8BB38, ___H2_6)); }
	inline uint64_t get_H2_6() const { return ___H2_6; }
	inline uint64_t* get_address_of_H2_6() { return &___H2_6; }
	inline void set_H2_6(uint64_t value)
	{
		___H2_6 = value;
	}

	inline static int32_t get_offset_of_H3_7() { return static_cast<int32_t>(offsetof(LongDigest_t043705804892FF0397DAA2E061CCF2DE8BA8BB38, ___H3_7)); }
	inline uint64_t get_H3_7() const { return ___H3_7; }
	inline uint64_t* get_address_of_H3_7() { return &___H3_7; }
	inline void set_H3_7(uint64_t value)
	{
		___H3_7 = value;
	}

	inline static int32_t get_offset_of_H4_8() { return static_cast<int32_t>(offsetof(LongDigest_t043705804892FF0397DAA2E061CCF2DE8BA8BB38, ___H4_8)); }
	inline uint64_t get_H4_8() const { return ___H4_8; }
	inline uint64_t* get_address_of_H4_8() { return &___H4_8; }
	inline void set_H4_8(uint64_t value)
	{
		___H4_8 = value;
	}

	inline static int32_t get_offset_of_H5_9() { return static_cast<int32_t>(offsetof(LongDigest_t043705804892FF0397DAA2E061CCF2DE8BA8BB38, ___H5_9)); }
	inline uint64_t get_H5_9() const { return ___H5_9; }
	inline uint64_t* get_address_of_H5_9() { return &___H5_9; }
	inline void set_H5_9(uint64_t value)
	{
		___H5_9 = value;
	}

	inline static int32_t get_offset_of_H6_10() { return static_cast<int32_t>(offsetof(LongDigest_t043705804892FF0397DAA2E061CCF2DE8BA8BB38, ___H6_10)); }
	inline uint64_t get_H6_10() const { return ___H6_10; }
	inline uint64_t* get_address_of_H6_10() { return &___H6_10; }
	inline void set_H6_10(uint64_t value)
	{
		___H6_10 = value;
	}

	inline static int32_t get_offset_of_H7_11() { return static_cast<int32_t>(offsetof(LongDigest_t043705804892FF0397DAA2E061CCF2DE8BA8BB38, ___H7_11)); }
	inline uint64_t get_H7_11() const { return ___H7_11; }
	inline uint64_t* get_address_of_H7_11() { return &___H7_11; }
	inline void set_H7_11(uint64_t value)
	{
		___H7_11 = value;
	}

	inline static int32_t get_offset_of_H8_12() { return static_cast<int32_t>(offsetof(LongDigest_t043705804892FF0397DAA2E061CCF2DE8BA8BB38, ___H8_12)); }
	inline uint64_t get_H8_12() const { return ___H8_12; }
	inline uint64_t* get_address_of_H8_12() { return &___H8_12; }
	inline void set_H8_12(uint64_t value)
	{
		___H8_12 = value;
	}

	inline static int32_t get_offset_of_W_13() { return static_cast<int32_t>(offsetof(LongDigest_t043705804892FF0397DAA2E061CCF2DE8BA8BB38, ___W_13)); }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* get_W_13() const { return ___W_13; }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4** get_address_of_W_13() { return &___W_13; }
	inline void set_W_13(UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* value)
	{
		___W_13 = value;
		Il2CppCodeGenWriteBarrier((&___W_13), value);
	}

	inline static int32_t get_offset_of_wOff_14() { return static_cast<int32_t>(offsetof(LongDigest_t043705804892FF0397DAA2E061CCF2DE8BA8BB38, ___wOff_14)); }
	inline int32_t get_wOff_14() const { return ___wOff_14; }
	inline int32_t* get_address_of_wOff_14() { return &___wOff_14; }
	inline void set_wOff_14(int32_t value)
	{
		___wOff_14 = value;
	}
};

struct LongDigest_t043705804892FF0397DAA2E061CCF2DE8BA8BB38_StaticFields
{
public:
	// System.UInt64[] Org.BouncyCastle.Crypto.Digests.LongDigest::K
	UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* ___K_15;

public:
	inline static int32_t get_offset_of_K_15() { return static_cast<int32_t>(offsetof(LongDigest_t043705804892FF0397DAA2E061CCF2DE8BA8BB38_StaticFields, ___K_15)); }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* get_K_15() const { return ___K_15; }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4** get_address_of_K_15() { return &___K_15; }
	inline void set_K_15(UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* value)
	{
		___K_15 = value;
		Il2CppCodeGenWriteBarrier((&___K_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LONGDIGEST_T043705804892FF0397DAA2E061CCF2DE8BA8BB38_H
#ifndef MD2DIGEST_TC26D7F8500823CAB8729DD2D58C59A59173485CE_H
#define MD2DIGEST_TC26D7F8500823CAB8729DD2D58C59A59173485CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Digests.MD2Digest
struct  MD2Digest_tC26D7F8500823CAB8729DD2D58C59A59173485CE  : public RuntimeObject
{
public:
	// System.Byte[] Org.BouncyCastle.Crypto.Digests.MD2Digest::X
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___X_0;
	// System.Int32 Org.BouncyCastle.Crypto.Digests.MD2Digest::xOff
	int32_t ___xOff_1;
	// System.Byte[] Org.BouncyCastle.Crypto.Digests.MD2Digest::M
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___M_2;
	// System.Int32 Org.BouncyCastle.Crypto.Digests.MD2Digest::mOff
	int32_t ___mOff_3;
	// System.Byte[] Org.BouncyCastle.Crypto.Digests.MD2Digest::C
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___C_4;
	// System.Int32 Org.BouncyCastle.Crypto.Digests.MD2Digest::COff
	int32_t ___COff_5;

public:
	inline static int32_t get_offset_of_X_0() { return static_cast<int32_t>(offsetof(MD2Digest_tC26D7F8500823CAB8729DD2D58C59A59173485CE, ___X_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_X_0() const { return ___X_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_X_0() { return &___X_0; }
	inline void set_X_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___X_0 = value;
		Il2CppCodeGenWriteBarrier((&___X_0), value);
	}

	inline static int32_t get_offset_of_xOff_1() { return static_cast<int32_t>(offsetof(MD2Digest_tC26D7F8500823CAB8729DD2D58C59A59173485CE, ___xOff_1)); }
	inline int32_t get_xOff_1() const { return ___xOff_1; }
	inline int32_t* get_address_of_xOff_1() { return &___xOff_1; }
	inline void set_xOff_1(int32_t value)
	{
		___xOff_1 = value;
	}

	inline static int32_t get_offset_of_M_2() { return static_cast<int32_t>(offsetof(MD2Digest_tC26D7F8500823CAB8729DD2D58C59A59173485CE, ___M_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_M_2() const { return ___M_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_M_2() { return &___M_2; }
	inline void set_M_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___M_2 = value;
		Il2CppCodeGenWriteBarrier((&___M_2), value);
	}

	inline static int32_t get_offset_of_mOff_3() { return static_cast<int32_t>(offsetof(MD2Digest_tC26D7F8500823CAB8729DD2D58C59A59173485CE, ___mOff_3)); }
	inline int32_t get_mOff_3() const { return ___mOff_3; }
	inline int32_t* get_address_of_mOff_3() { return &___mOff_3; }
	inline void set_mOff_3(int32_t value)
	{
		___mOff_3 = value;
	}

	inline static int32_t get_offset_of_C_4() { return static_cast<int32_t>(offsetof(MD2Digest_tC26D7F8500823CAB8729DD2D58C59A59173485CE, ___C_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_C_4() const { return ___C_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_C_4() { return &___C_4; }
	inline void set_C_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___C_4 = value;
		Il2CppCodeGenWriteBarrier((&___C_4), value);
	}

	inline static int32_t get_offset_of_COff_5() { return static_cast<int32_t>(offsetof(MD2Digest_tC26D7F8500823CAB8729DD2D58C59A59173485CE, ___COff_5)); }
	inline int32_t get_COff_5() const { return ___COff_5; }
	inline int32_t* get_address_of_COff_5() { return &___COff_5; }
	inline void set_COff_5(int32_t value)
	{
		___COff_5 = value;
	}
};

struct MD2Digest_tC26D7F8500823CAB8729DD2D58C59A59173485CE_StaticFields
{
public:
	// System.Byte[] Org.BouncyCastle.Crypto.Digests.MD2Digest::S
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___S_6;

public:
	inline static int32_t get_offset_of_S_6() { return static_cast<int32_t>(offsetof(MD2Digest_tC26D7F8500823CAB8729DD2D58C59A59173485CE_StaticFields, ___S_6)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_S_6() const { return ___S_6; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_S_6() { return &___S_6; }
	inline void set_S_6(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___S_6 = value;
		Il2CppCodeGenWriteBarrier((&___S_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MD2DIGEST_TC26D7F8500823CAB8729DD2D58C59A59173485CE_H
#ifndef NULLDIGEST_T0E2AA2F965790ED3744B24CB685F4C0EE80D259A_H
#define NULLDIGEST_T0E2AA2F965790ED3744B24CB685F4C0EE80D259A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Digests.NullDigest
struct  NullDigest_t0E2AA2F965790ED3744B24CB685F4C0EE80D259A  : public RuntimeObject
{
public:
	// System.IO.MemoryStream Org.BouncyCastle.Crypto.Digests.NullDigest::bOut
	MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * ___bOut_0;

public:
	inline static int32_t get_offset_of_bOut_0() { return static_cast<int32_t>(offsetof(NullDigest_t0E2AA2F965790ED3744B24CB685F4C0EE80D259A, ___bOut_0)); }
	inline MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * get_bOut_0() const { return ___bOut_0; }
	inline MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C ** get_address_of_bOut_0() { return &___bOut_0; }
	inline void set_bOut_0(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * value)
	{
		___bOut_0 = value;
		Il2CppCodeGenWriteBarrier((&___bOut_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLDIGEST_T0E2AA2F965790ED3744B24CB685F4C0EE80D259A_H
#ifndef TIGERDIGEST_T65B59C08483681A97EC0475F4409DFFFC2C80D94_H
#define TIGERDIGEST_T65B59C08483681A97EC0475F4409DFFFC2C80D94_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Digests.TigerDigest
struct  TigerDigest_t65B59C08483681A97EC0475F4409DFFFC2C80D94  : public RuntimeObject
{
public:
	// System.Int64 Org.BouncyCastle.Crypto.Digests.TigerDigest::a
	int64_t ___a_4;
	// System.Int64 Org.BouncyCastle.Crypto.Digests.TigerDigest::b
	int64_t ___b_5;
	// System.Int64 Org.BouncyCastle.Crypto.Digests.TigerDigest::c
	int64_t ___c_6;
	// System.Int64 Org.BouncyCastle.Crypto.Digests.TigerDigest::byteCount
	int64_t ___byteCount_7;
	// System.Byte[] Org.BouncyCastle.Crypto.Digests.TigerDigest::Buffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___Buffer_8;
	// System.Int32 Org.BouncyCastle.Crypto.Digests.TigerDigest::bOff
	int32_t ___bOff_9;
	// System.Int64[] Org.BouncyCastle.Crypto.Digests.TigerDigest::x
	Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* ___x_10;
	// System.Int32 Org.BouncyCastle.Crypto.Digests.TigerDigest::xOff
	int32_t ___xOff_11;

public:
	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(TigerDigest_t65B59C08483681A97EC0475F4409DFFFC2C80D94, ___a_4)); }
	inline int64_t get_a_4() const { return ___a_4; }
	inline int64_t* get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(int64_t value)
	{
		___a_4 = value;
	}

	inline static int32_t get_offset_of_b_5() { return static_cast<int32_t>(offsetof(TigerDigest_t65B59C08483681A97EC0475F4409DFFFC2C80D94, ___b_5)); }
	inline int64_t get_b_5() const { return ___b_5; }
	inline int64_t* get_address_of_b_5() { return &___b_5; }
	inline void set_b_5(int64_t value)
	{
		___b_5 = value;
	}

	inline static int32_t get_offset_of_c_6() { return static_cast<int32_t>(offsetof(TigerDigest_t65B59C08483681A97EC0475F4409DFFFC2C80D94, ___c_6)); }
	inline int64_t get_c_6() const { return ___c_6; }
	inline int64_t* get_address_of_c_6() { return &___c_6; }
	inline void set_c_6(int64_t value)
	{
		___c_6 = value;
	}

	inline static int32_t get_offset_of_byteCount_7() { return static_cast<int32_t>(offsetof(TigerDigest_t65B59C08483681A97EC0475F4409DFFFC2C80D94, ___byteCount_7)); }
	inline int64_t get_byteCount_7() const { return ___byteCount_7; }
	inline int64_t* get_address_of_byteCount_7() { return &___byteCount_7; }
	inline void set_byteCount_7(int64_t value)
	{
		___byteCount_7 = value;
	}

	inline static int32_t get_offset_of_Buffer_8() { return static_cast<int32_t>(offsetof(TigerDigest_t65B59C08483681A97EC0475F4409DFFFC2C80D94, ___Buffer_8)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_Buffer_8() const { return ___Buffer_8; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_Buffer_8() { return &___Buffer_8; }
	inline void set_Buffer_8(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___Buffer_8 = value;
		Il2CppCodeGenWriteBarrier((&___Buffer_8), value);
	}

	inline static int32_t get_offset_of_bOff_9() { return static_cast<int32_t>(offsetof(TigerDigest_t65B59C08483681A97EC0475F4409DFFFC2C80D94, ___bOff_9)); }
	inline int32_t get_bOff_9() const { return ___bOff_9; }
	inline int32_t* get_address_of_bOff_9() { return &___bOff_9; }
	inline void set_bOff_9(int32_t value)
	{
		___bOff_9 = value;
	}

	inline static int32_t get_offset_of_x_10() { return static_cast<int32_t>(offsetof(TigerDigest_t65B59C08483681A97EC0475F4409DFFFC2C80D94, ___x_10)); }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* get_x_10() const { return ___x_10; }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F** get_address_of_x_10() { return &___x_10; }
	inline void set_x_10(Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* value)
	{
		___x_10 = value;
		Il2CppCodeGenWriteBarrier((&___x_10), value);
	}

	inline static int32_t get_offset_of_xOff_11() { return static_cast<int32_t>(offsetof(TigerDigest_t65B59C08483681A97EC0475F4409DFFFC2C80D94, ___xOff_11)); }
	inline int32_t get_xOff_11() const { return ___xOff_11; }
	inline int32_t* get_address_of_xOff_11() { return &___xOff_11; }
	inline void set_xOff_11(int32_t value)
	{
		___xOff_11 = value;
	}
};

struct TigerDigest_t65B59C08483681A97EC0475F4409DFFFC2C80D94_StaticFields
{
public:
	// System.Int64[] Org.BouncyCastle.Crypto.Digests.TigerDigest::t1
	Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* ___t1_0;
	// System.Int64[] Org.BouncyCastle.Crypto.Digests.TigerDigest::t2
	Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* ___t2_1;
	// System.Int64[] Org.BouncyCastle.Crypto.Digests.TigerDigest::t3
	Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* ___t3_2;
	// System.Int64[] Org.BouncyCastle.Crypto.Digests.TigerDigest::t4
	Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* ___t4_3;

public:
	inline static int32_t get_offset_of_t1_0() { return static_cast<int32_t>(offsetof(TigerDigest_t65B59C08483681A97EC0475F4409DFFFC2C80D94_StaticFields, ___t1_0)); }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* get_t1_0() const { return ___t1_0; }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F** get_address_of_t1_0() { return &___t1_0; }
	inline void set_t1_0(Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* value)
	{
		___t1_0 = value;
		Il2CppCodeGenWriteBarrier((&___t1_0), value);
	}

	inline static int32_t get_offset_of_t2_1() { return static_cast<int32_t>(offsetof(TigerDigest_t65B59C08483681A97EC0475F4409DFFFC2C80D94_StaticFields, ___t2_1)); }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* get_t2_1() const { return ___t2_1; }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F** get_address_of_t2_1() { return &___t2_1; }
	inline void set_t2_1(Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* value)
	{
		___t2_1 = value;
		Il2CppCodeGenWriteBarrier((&___t2_1), value);
	}

	inline static int32_t get_offset_of_t3_2() { return static_cast<int32_t>(offsetof(TigerDigest_t65B59C08483681A97EC0475F4409DFFFC2C80D94_StaticFields, ___t3_2)); }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* get_t3_2() const { return ___t3_2; }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F** get_address_of_t3_2() { return &___t3_2; }
	inline void set_t3_2(Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* value)
	{
		___t3_2 = value;
		Il2CppCodeGenWriteBarrier((&___t3_2), value);
	}

	inline static int32_t get_offset_of_t4_3() { return static_cast<int32_t>(offsetof(TigerDigest_t65B59C08483681A97EC0475F4409DFFFC2C80D94_StaticFields, ___t4_3)); }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* get_t4_3() const { return ___t4_3; }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F** get_address_of_t4_3() { return &___t4_3; }
	inline void set_t4_3(Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* value)
	{
		___t4_3 = value;
		Il2CppCodeGenWriteBarrier((&___t4_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIGERDIGEST_T65B59C08483681A97EC0475F4409DFFFC2C80D94_H
#ifndef WHIRLPOOLDIGEST_TFF37FE0BB1D89CE67CC8462C381E9303A241F821_H
#define WHIRLPOOLDIGEST_TFF37FE0BB1D89CE67CC8462C381E9303A241F821_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Digests.WhirlpoolDigest
struct  WhirlpoolDigest_tFF37FE0BB1D89CE67CC8462C381E9303A241F821  : public RuntimeObject
{
public:
	// System.Int64[] Org.BouncyCastle.Crypto.Digests.WhirlpoolDigest::_rc
	Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* ____rc_9;
	// System.Byte[] Org.BouncyCastle.Crypto.Digests.WhirlpoolDigest::_buffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____buffer_11;
	// System.Int32 Org.BouncyCastle.Crypto.Digests.WhirlpoolDigest::_bufferPos
	int32_t ____bufferPos_12;
	// System.Int16[] Org.BouncyCastle.Crypto.Digests.WhirlpoolDigest::_bitCount
	Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* ____bitCount_13;
	// System.Int64[] Org.BouncyCastle.Crypto.Digests.WhirlpoolDigest::_hash
	Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* ____hash_14;
	// System.Int64[] Org.BouncyCastle.Crypto.Digests.WhirlpoolDigest::_K
	Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* ____K_15;
	// System.Int64[] Org.BouncyCastle.Crypto.Digests.WhirlpoolDigest::_L
	Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* ____L_16;
	// System.Int64[] Org.BouncyCastle.Crypto.Digests.WhirlpoolDigest::_block
	Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* ____block_17;
	// System.Int64[] Org.BouncyCastle.Crypto.Digests.WhirlpoolDigest::_state
	Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* ____state_18;

public:
	inline static int32_t get_offset_of__rc_9() { return static_cast<int32_t>(offsetof(WhirlpoolDigest_tFF37FE0BB1D89CE67CC8462C381E9303A241F821, ____rc_9)); }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* get__rc_9() const { return ____rc_9; }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F** get_address_of__rc_9() { return &____rc_9; }
	inline void set__rc_9(Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* value)
	{
		____rc_9 = value;
		Il2CppCodeGenWriteBarrier((&____rc_9), value);
	}

	inline static int32_t get_offset_of__buffer_11() { return static_cast<int32_t>(offsetof(WhirlpoolDigest_tFF37FE0BB1D89CE67CC8462C381E9303A241F821, ____buffer_11)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__buffer_11() const { return ____buffer_11; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__buffer_11() { return &____buffer_11; }
	inline void set__buffer_11(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____buffer_11 = value;
		Il2CppCodeGenWriteBarrier((&____buffer_11), value);
	}

	inline static int32_t get_offset_of__bufferPos_12() { return static_cast<int32_t>(offsetof(WhirlpoolDigest_tFF37FE0BB1D89CE67CC8462C381E9303A241F821, ____bufferPos_12)); }
	inline int32_t get__bufferPos_12() const { return ____bufferPos_12; }
	inline int32_t* get_address_of__bufferPos_12() { return &____bufferPos_12; }
	inline void set__bufferPos_12(int32_t value)
	{
		____bufferPos_12 = value;
	}

	inline static int32_t get_offset_of__bitCount_13() { return static_cast<int32_t>(offsetof(WhirlpoolDigest_tFF37FE0BB1D89CE67CC8462C381E9303A241F821, ____bitCount_13)); }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* get__bitCount_13() const { return ____bitCount_13; }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28** get_address_of__bitCount_13() { return &____bitCount_13; }
	inline void set__bitCount_13(Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* value)
	{
		____bitCount_13 = value;
		Il2CppCodeGenWriteBarrier((&____bitCount_13), value);
	}

	inline static int32_t get_offset_of__hash_14() { return static_cast<int32_t>(offsetof(WhirlpoolDigest_tFF37FE0BB1D89CE67CC8462C381E9303A241F821, ____hash_14)); }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* get__hash_14() const { return ____hash_14; }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F** get_address_of__hash_14() { return &____hash_14; }
	inline void set__hash_14(Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* value)
	{
		____hash_14 = value;
		Il2CppCodeGenWriteBarrier((&____hash_14), value);
	}

	inline static int32_t get_offset_of__K_15() { return static_cast<int32_t>(offsetof(WhirlpoolDigest_tFF37FE0BB1D89CE67CC8462C381E9303A241F821, ____K_15)); }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* get__K_15() const { return ____K_15; }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F** get_address_of__K_15() { return &____K_15; }
	inline void set__K_15(Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* value)
	{
		____K_15 = value;
		Il2CppCodeGenWriteBarrier((&____K_15), value);
	}

	inline static int32_t get_offset_of__L_16() { return static_cast<int32_t>(offsetof(WhirlpoolDigest_tFF37FE0BB1D89CE67CC8462C381E9303A241F821, ____L_16)); }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* get__L_16() const { return ____L_16; }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F** get_address_of__L_16() { return &____L_16; }
	inline void set__L_16(Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* value)
	{
		____L_16 = value;
		Il2CppCodeGenWriteBarrier((&____L_16), value);
	}

	inline static int32_t get_offset_of__block_17() { return static_cast<int32_t>(offsetof(WhirlpoolDigest_tFF37FE0BB1D89CE67CC8462C381E9303A241F821, ____block_17)); }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* get__block_17() const { return ____block_17; }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F** get_address_of__block_17() { return &____block_17; }
	inline void set__block_17(Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* value)
	{
		____block_17 = value;
		Il2CppCodeGenWriteBarrier((&____block_17), value);
	}

	inline static int32_t get_offset_of__state_18() { return static_cast<int32_t>(offsetof(WhirlpoolDigest_tFF37FE0BB1D89CE67CC8462C381E9303A241F821, ____state_18)); }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* get__state_18() const { return ____state_18; }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F** get_address_of__state_18() { return &____state_18; }
	inline void set__state_18(Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* value)
	{
		____state_18 = value;
		Il2CppCodeGenWriteBarrier((&____state_18), value);
	}
};

struct WhirlpoolDigest_tFF37FE0BB1D89CE67CC8462C381E9303A241F821_StaticFields
{
public:
	// System.Int32[] Org.BouncyCastle.Crypto.Digests.WhirlpoolDigest::SBOX
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___SBOX_0;
	// System.Int64[] Org.BouncyCastle.Crypto.Digests.WhirlpoolDigest::C0
	Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* ___C0_1;
	// System.Int64[] Org.BouncyCastle.Crypto.Digests.WhirlpoolDigest::C1
	Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* ___C1_2;
	// System.Int64[] Org.BouncyCastle.Crypto.Digests.WhirlpoolDigest::C2
	Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* ___C2_3;
	// System.Int64[] Org.BouncyCastle.Crypto.Digests.WhirlpoolDigest::C3
	Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* ___C3_4;
	// System.Int64[] Org.BouncyCastle.Crypto.Digests.WhirlpoolDigest::C4
	Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* ___C4_5;
	// System.Int64[] Org.BouncyCastle.Crypto.Digests.WhirlpoolDigest::C5
	Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* ___C5_6;
	// System.Int64[] Org.BouncyCastle.Crypto.Digests.WhirlpoolDigest::C6
	Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* ___C6_7;
	// System.Int64[] Org.BouncyCastle.Crypto.Digests.WhirlpoolDigest::C7
	Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* ___C7_8;
	// System.Int16[] Org.BouncyCastle.Crypto.Digests.WhirlpoolDigest::EIGHT
	Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* ___EIGHT_10;

public:
	inline static int32_t get_offset_of_SBOX_0() { return static_cast<int32_t>(offsetof(WhirlpoolDigest_tFF37FE0BB1D89CE67CC8462C381E9303A241F821_StaticFields, ___SBOX_0)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_SBOX_0() const { return ___SBOX_0; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_SBOX_0() { return &___SBOX_0; }
	inline void set_SBOX_0(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___SBOX_0 = value;
		Il2CppCodeGenWriteBarrier((&___SBOX_0), value);
	}

	inline static int32_t get_offset_of_C0_1() { return static_cast<int32_t>(offsetof(WhirlpoolDigest_tFF37FE0BB1D89CE67CC8462C381E9303A241F821_StaticFields, ___C0_1)); }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* get_C0_1() const { return ___C0_1; }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F** get_address_of_C0_1() { return &___C0_1; }
	inline void set_C0_1(Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* value)
	{
		___C0_1 = value;
		Il2CppCodeGenWriteBarrier((&___C0_1), value);
	}

	inline static int32_t get_offset_of_C1_2() { return static_cast<int32_t>(offsetof(WhirlpoolDigest_tFF37FE0BB1D89CE67CC8462C381E9303A241F821_StaticFields, ___C1_2)); }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* get_C1_2() const { return ___C1_2; }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F** get_address_of_C1_2() { return &___C1_2; }
	inline void set_C1_2(Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* value)
	{
		___C1_2 = value;
		Il2CppCodeGenWriteBarrier((&___C1_2), value);
	}

	inline static int32_t get_offset_of_C2_3() { return static_cast<int32_t>(offsetof(WhirlpoolDigest_tFF37FE0BB1D89CE67CC8462C381E9303A241F821_StaticFields, ___C2_3)); }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* get_C2_3() const { return ___C2_3; }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F** get_address_of_C2_3() { return &___C2_3; }
	inline void set_C2_3(Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* value)
	{
		___C2_3 = value;
		Il2CppCodeGenWriteBarrier((&___C2_3), value);
	}

	inline static int32_t get_offset_of_C3_4() { return static_cast<int32_t>(offsetof(WhirlpoolDigest_tFF37FE0BB1D89CE67CC8462C381E9303A241F821_StaticFields, ___C3_4)); }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* get_C3_4() const { return ___C3_4; }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F** get_address_of_C3_4() { return &___C3_4; }
	inline void set_C3_4(Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* value)
	{
		___C3_4 = value;
		Il2CppCodeGenWriteBarrier((&___C3_4), value);
	}

	inline static int32_t get_offset_of_C4_5() { return static_cast<int32_t>(offsetof(WhirlpoolDigest_tFF37FE0BB1D89CE67CC8462C381E9303A241F821_StaticFields, ___C4_5)); }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* get_C4_5() const { return ___C4_5; }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F** get_address_of_C4_5() { return &___C4_5; }
	inline void set_C4_5(Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* value)
	{
		___C4_5 = value;
		Il2CppCodeGenWriteBarrier((&___C4_5), value);
	}

	inline static int32_t get_offset_of_C5_6() { return static_cast<int32_t>(offsetof(WhirlpoolDigest_tFF37FE0BB1D89CE67CC8462C381E9303A241F821_StaticFields, ___C5_6)); }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* get_C5_6() const { return ___C5_6; }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F** get_address_of_C5_6() { return &___C5_6; }
	inline void set_C5_6(Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* value)
	{
		___C5_6 = value;
		Il2CppCodeGenWriteBarrier((&___C5_6), value);
	}

	inline static int32_t get_offset_of_C6_7() { return static_cast<int32_t>(offsetof(WhirlpoolDigest_tFF37FE0BB1D89CE67CC8462C381E9303A241F821_StaticFields, ___C6_7)); }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* get_C6_7() const { return ___C6_7; }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F** get_address_of_C6_7() { return &___C6_7; }
	inline void set_C6_7(Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* value)
	{
		___C6_7 = value;
		Il2CppCodeGenWriteBarrier((&___C6_7), value);
	}

	inline static int32_t get_offset_of_C7_8() { return static_cast<int32_t>(offsetof(WhirlpoolDigest_tFF37FE0BB1D89CE67CC8462C381E9303A241F821_StaticFields, ___C7_8)); }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* get_C7_8() const { return ___C7_8; }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F** get_address_of_C7_8() { return &___C7_8; }
	inline void set_C7_8(Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* value)
	{
		___C7_8 = value;
		Il2CppCodeGenWriteBarrier((&___C7_8), value);
	}

	inline static int32_t get_offset_of_EIGHT_10() { return static_cast<int32_t>(offsetof(WhirlpoolDigest_tFF37FE0BB1D89CE67CC8462C381E9303A241F821_StaticFields, ___EIGHT_10)); }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* get_EIGHT_10() const { return ___EIGHT_10; }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28** get_address_of_EIGHT_10() { return &___EIGHT_10; }
	inline void set_EIGHT_10(Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* value)
	{
		___EIGHT_10 = value;
		Il2CppCodeGenWriteBarrier((&___EIGHT_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WHIRLPOOLDIGEST_TFF37FE0BB1D89CE67CC8462C381E9303A241F821_H
#ifndef CUSTOMNAMEDCURVES_TB73015B5EF02BC0E5C4EC0048428B7F017B5EB7B_H
#define CUSTOMNAMEDCURVES_TB73015B5EF02BC0E5C4EC0048428B7F017B5EB7B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.EC.CustomNamedCurves
struct  CustomNamedCurves_tB73015B5EF02BC0E5C4EC0048428B7F017B5EB7B  : public RuntimeObject
{
public:

public:
};

struct CustomNamedCurves_tB73015B5EF02BC0E5C4EC0048428B7F017B5EB7B_StaticFields
{
public:
	// System.Collections.IDictionary Org.BouncyCastle.Crypto.EC.CustomNamedCurves::nameToCurve
	RuntimeObject* ___nameToCurve_0;
	// System.Collections.IDictionary Org.BouncyCastle.Crypto.EC.CustomNamedCurves::nameToOid
	RuntimeObject* ___nameToOid_1;
	// System.Collections.IDictionary Org.BouncyCastle.Crypto.EC.CustomNamedCurves::oidToCurve
	RuntimeObject* ___oidToCurve_2;
	// System.Collections.IDictionary Org.BouncyCastle.Crypto.EC.CustomNamedCurves::oidToName
	RuntimeObject* ___oidToName_3;
	// System.Collections.IList Org.BouncyCastle.Crypto.EC.CustomNamedCurves::names
	RuntimeObject* ___names_4;

public:
	inline static int32_t get_offset_of_nameToCurve_0() { return static_cast<int32_t>(offsetof(CustomNamedCurves_tB73015B5EF02BC0E5C4EC0048428B7F017B5EB7B_StaticFields, ___nameToCurve_0)); }
	inline RuntimeObject* get_nameToCurve_0() const { return ___nameToCurve_0; }
	inline RuntimeObject** get_address_of_nameToCurve_0() { return &___nameToCurve_0; }
	inline void set_nameToCurve_0(RuntimeObject* value)
	{
		___nameToCurve_0 = value;
		Il2CppCodeGenWriteBarrier((&___nameToCurve_0), value);
	}

	inline static int32_t get_offset_of_nameToOid_1() { return static_cast<int32_t>(offsetof(CustomNamedCurves_tB73015B5EF02BC0E5C4EC0048428B7F017B5EB7B_StaticFields, ___nameToOid_1)); }
	inline RuntimeObject* get_nameToOid_1() const { return ___nameToOid_1; }
	inline RuntimeObject** get_address_of_nameToOid_1() { return &___nameToOid_1; }
	inline void set_nameToOid_1(RuntimeObject* value)
	{
		___nameToOid_1 = value;
		Il2CppCodeGenWriteBarrier((&___nameToOid_1), value);
	}

	inline static int32_t get_offset_of_oidToCurve_2() { return static_cast<int32_t>(offsetof(CustomNamedCurves_tB73015B5EF02BC0E5C4EC0048428B7F017B5EB7B_StaticFields, ___oidToCurve_2)); }
	inline RuntimeObject* get_oidToCurve_2() const { return ___oidToCurve_2; }
	inline RuntimeObject** get_address_of_oidToCurve_2() { return &___oidToCurve_2; }
	inline void set_oidToCurve_2(RuntimeObject* value)
	{
		___oidToCurve_2 = value;
		Il2CppCodeGenWriteBarrier((&___oidToCurve_2), value);
	}

	inline static int32_t get_offset_of_oidToName_3() { return static_cast<int32_t>(offsetof(CustomNamedCurves_tB73015B5EF02BC0E5C4EC0048428B7F017B5EB7B_StaticFields, ___oidToName_3)); }
	inline RuntimeObject* get_oidToName_3() const { return ___oidToName_3; }
	inline RuntimeObject** get_address_of_oidToName_3() { return &___oidToName_3; }
	inline void set_oidToName_3(RuntimeObject* value)
	{
		___oidToName_3 = value;
		Il2CppCodeGenWriteBarrier((&___oidToName_3), value);
	}

	inline static int32_t get_offset_of_names_4() { return static_cast<int32_t>(offsetof(CustomNamedCurves_tB73015B5EF02BC0E5C4EC0048428B7F017B5EB7B_StaticFields, ___names_4)); }
	inline RuntimeObject* get_names_4() const { return ___names_4; }
	inline RuntimeObject** get_address_of_names_4() { return &___names_4; }
	inline void set_names_4(RuntimeObject* value)
	{
		___names_4 = value;
		Il2CppCodeGenWriteBarrier((&___names_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMNAMEDCURVES_TB73015B5EF02BC0E5C4EC0048428B7F017B5EB7B_H
#ifndef PKCS1ENCODING_TB3FC05E5649E6B90D8850455A787609E71FA427B_H
#define PKCS1ENCODING_TB3FC05E5649E6B90D8850455A787609E71FA427B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Encodings.Pkcs1Encoding
struct  Pkcs1Encoding_tB3FC05E5649E6B90D8850455A787609E71FA427B  : public RuntimeObject
{
public:
	// Org.BouncyCastle.Security.SecureRandom Org.BouncyCastle.Crypto.Encodings.Pkcs1Encoding::random
	SecureRandom_t0B6C2C3EDF11BF4FD22E44CE8997F6A0D2F142B0 * ___random_1;
	// Org.BouncyCastle.Crypto.IAsymmetricBlockCipher Org.BouncyCastle.Crypto.Encodings.Pkcs1Encoding::engine
	RuntimeObject* ___engine_2;
	// System.Boolean Org.BouncyCastle.Crypto.Encodings.Pkcs1Encoding::forEncryption
	bool ___forEncryption_3;
	// System.Boolean Org.BouncyCastle.Crypto.Encodings.Pkcs1Encoding::forPrivateKey
	bool ___forPrivateKey_4;
	// System.Boolean Org.BouncyCastle.Crypto.Encodings.Pkcs1Encoding::useStrictLength
	bool ___useStrictLength_5;
	// System.Int32 Org.BouncyCastle.Crypto.Encodings.Pkcs1Encoding::pLen
	int32_t ___pLen_6;
	// System.Byte[] Org.BouncyCastle.Crypto.Encodings.Pkcs1Encoding::fallback
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___fallback_7;

public:
	inline static int32_t get_offset_of_random_1() { return static_cast<int32_t>(offsetof(Pkcs1Encoding_tB3FC05E5649E6B90D8850455A787609E71FA427B, ___random_1)); }
	inline SecureRandom_t0B6C2C3EDF11BF4FD22E44CE8997F6A0D2F142B0 * get_random_1() const { return ___random_1; }
	inline SecureRandom_t0B6C2C3EDF11BF4FD22E44CE8997F6A0D2F142B0 ** get_address_of_random_1() { return &___random_1; }
	inline void set_random_1(SecureRandom_t0B6C2C3EDF11BF4FD22E44CE8997F6A0D2F142B0 * value)
	{
		___random_1 = value;
		Il2CppCodeGenWriteBarrier((&___random_1), value);
	}

	inline static int32_t get_offset_of_engine_2() { return static_cast<int32_t>(offsetof(Pkcs1Encoding_tB3FC05E5649E6B90D8850455A787609E71FA427B, ___engine_2)); }
	inline RuntimeObject* get_engine_2() const { return ___engine_2; }
	inline RuntimeObject** get_address_of_engine_2() { return &___engine_2; }
	inline void set_engine_2(RuntimeObject* value)
	{
		___engine_2 = value;
		Il2CppCodeGenWriteBarrier((&___engine_2), value);
	}

	inline static int32_t get_offset_of_forEncryption_3() { return static_cast<int32_t>(offsetof(Pkcs1Encoding_tB3FC05E5649E6B90D8850455A787609E71FA427B, ___forEncryption_3)); }
	inline bool get_forEncryption_3() const { return ___forEncryption_3; }
	inline bool* get_address_of_forEncryption_3() { return &___forEncryption_3; }
	inline void set_forEncryption_3(bool value)
	{
		___forEncryption_3 = value;
	}

	inline static int32_t get_offset_of_forPrivateKey_4() { return static_cast<int32_t>(offsetof(Pkcs1Encoding_tB3FC05E5649E6B90D8850455A787609E71FA427B, ___forPrivateKey_4)); }
	inline bool get_forPrivateKey_4() const { return ___forPrivateKey_4; }
	inline bool* get_address_of_forPrivateKey_4() { return &___forPrivateKey_4; }
	inline void set_forPrivateKey_4(bool value)
	{
		___forPrivateKey_4 = value;
	}

	inline static int32_t get_offset_of_useStrictLength_5() { return static_cast<int32_t>(offsetof(Pkcs1Encoding_tB3FC05E5649E6B90D8850455A787609E71FA427B, ___useStrictLength_5)); }
	inline bool get_useStrictLength_5() const { return ___useStrictLength_5; }
	inline bool* get_address_of_useStrictLength_5() { return &___useStrictLength_5; }
	inline void set_useStrictLength_5(bool value)
	{
		___useStrictLength_5 = value;
	}

	inline static int32_t get_offset_of_pLen_6() { return static_cast<int32_t>(offsetof(Pkcs1Encoding_tB3FC05E5649E6B90D8850455A787609E71FA427B, ___pLen_6)); }
	inline int32_t get_pLen_6() const { return ___pLen_6; }
	inline int32_t* get_address_of_pLen_6() { return &___pLen_6; }
	inline void set_pLen_6(int32_t value)
	{
		___pLen_6 = value;
	}

	inline static int32_t get_offset_of_fallback_7() { return static_cast<int32_t>(offsetof(Pkcs1Encoding_tB3FC05E5649E6B90D8850455A787609E71FA427B, ___fallback_7)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_fallback_7() const { return ___fallback_7; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_fallback_7() { return &___fallback_7; }
	inline void set_fallback_7(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___fallback_7 = value;
		Il2CppCodeGenWriteBarrier((&___fallback_7), value);
	}
};

struct Pkcs1Encoding_tB3FC05E5649E6B90D8850455A787609E71FA427B_StaticFields
{
public:
	// System.Boolean[] Org.BouncyCastle.Crypto.Encodings.Pkcs1Encoding::strictLengthEnabled
	BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* ___strictLengthEnabled_0;

public:
	inline static int32_t get_offset_of_strictLengthEnabled_0() { return static_cast<int32_t>(offsetof(Pkcs1Encoding_tB3FC05E5649E6B90D8850455A787609E71FA427B_StaticFields, ___strictLengthEnabled_0)); }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* get_strictLengthEnabled_0() const { return ___strictLengthEnabled_0; }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040** get_address_of_strictLengthEnabled_0() { return &___strictLengthEnabled_0; }
	inline void set_strictLengthEnabled_0(BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* value)
	{
		___strictLengthEnabled_0 = value;
		Il2CppCodeGenWriteBarrier((&___strictLengthEnabled_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKCS1ENCODING_TB3FC05E5649E6B90D8850455A787609E71FA427B_H
#ifndef AESENGINE_T4C35E0FD810D2F75EBEF934ACA7F6F9834C294E8_H
#define AESENGINE_T4C35E0FD810D2F75EBEF934ACA7F6F9834C294E8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Engines.AesEngine
struct  AesEngine_t4C35E0FD810D2F75EBEF934ACA7F6F9834C294E8  : public RuntimeObject
{
public:
	// System.Int32 Org.BouncyCastle.Crypto.Engines.AesEngine::ROUNDS
	int32_t ___ROUNDS_5;
	// System.UInt32[][] Org.BouncyCastle.Crypto.Engines.AesEngine::WorkingKey
	UInt32U5BU5DU5BU5D_tBFBD606F8E3022C93666CAF6F7DA13AD70895D8C* ___WorkingKey_6;
	// System.UInt32 Org.BouncyCastle.Crypto.Engines.AesEngine::C0
	uint32_t ___C0_7;
	// System.UInt32 Org.BouncyCastle.Crypto.Engines.AesEngine::C1
	uint32_t ___C1_8;
	// System.UInt32 Org.BouncyCastle.Crypto.Engines.AesEngine::C2
	uint32_t ___C2_9;
	// System.UInt32 Org.BouncyCastle.Crypto.Engines.AesEngine::C3
	uint32_t ___C3_10;
	// System.Boolean Org.BouncyCastle.Crypto.Engines.AesEngine::forEncryption
	bool ___forEncryption_11;

public:
	inline static int32_t get_offset_of_ROUNDS_5() { return static_cast<int32_t>(offsetof(AesEngine_t4C35E0FD810D2F75EBEF934ACA7F6F9834C294E8, ___ROUNDS_5)); }
	inline int32_t get_ROUNDS_5() const { return ___ROUNDS_5; }
	inline int32_t* get_address_of_ROUNDS_5() { return &___ROUNDS_5; }
	inline void set_ROUNDS_5(int32_t value)
	{
		___ROUNDS_5 = value;
	}

	inline static int32_t get_offset_of_WorkingKey_6() { return static_cast<int32_t>(offsetof(AesEngine_t4C35E0FD810D2F75EBEF934ACA7F6F9834C294E8, ___WorkingKey_6)); }
	inline UInt32U5BU5DU5BU5D_tBFBD606F8E3022C93666CAF6F7DA13AD70895D8C* get_WorkingKey_6() const { return ___WorkingKey_6; }
	inline UInt32U5BU5DU5BU5D_tBFBD606F8E3022C93666CAF6F7DA13AD70895D8C** get_address_of_WorkingKey_6() { return &___WorkingKey_6; }
	inline void set_WorkingKey_6(UInt32U5BU5DU5BU5D_tBFBD606F8E3022C93666CAF6F7DA13AD70895D8C* value)
	{
		___WorkingKey_6 = value;
		Il2CppCodeGenWriteBarrier((&___WorkingKey_6), value);
	}

	inline static int32_t get_offset_of_C0_7() { return static_cast<int32_t>(offsetof(AesEngine_t4C35E0FD810D2F75EBEF934ACA7F6F9834C294E8, ___C0_7)); }
	inline uint32_t get_C0_7() const { return ___C0_7; }
	inline uint32_t* get_address_of_C0_7() { return &___C0_7; }
	inline void set_C0_7(uint32_t value)
	{
		___C0_7 = value;
	}

	inline static int32_t get_offset_of_C1_8() { return static_cast<int32_t>(offsetof(AesEngine_t4C35E0FD810D2F75EBEF934ACA7F6F9834C294E8, ___C1_8)); }
	inline uint32_t get_C1_8() const { return ___C1_8; }
	inline uint32_t* get_address_of_C1_8() { return &___C1_8; }
	inline void set_C1_8(uint32_t value)
	{
		___C1_8 = value;
	}

	inline static int32_t get_offset_of_C2_9() { return static_cast<int32_t>(offsetof(AesEngine_t4C35E0FD810D2F75EBEF934ACA7F6F9834C294E8, ___C2_9)); }
	inline uint32_t get_C2_9() const { return ___C2_9; }
	inline uint32_t* get_address_of_C2_9() { return &___C2_9; }
	inline void set_C2_9(uint32_t value)
	{
		___C2_9 = value;
	}

	inline static int32_t get_offset_of_C3_10() { return static_cast<int32_t>(offsetof(AesEngine_t4C35E0FD810D2F75EBEF934ACA7F6F9834C294E8, ___C3_10)); }
	inline uint32_t get_C3_10() const { return ___C3_10; }
	inline uint32_t* get_address_of_C3_10() { return &___C3_10; }
	inline void set_C3_10(uint32_t value)
	{
		___C3_10 = value;
	}

	inline static int32_t get_offset_of_forEncryption_11() { return static_cast<int32_t>(offsetof(AesEngine_t4C35E0FD810D2F75EBEF934ACA7F6F9834C294E8, ___forEncryption_11)); }
	inline bool get_forEncryption_11() const { return ___forEncryption_11; }
	inline bool* get_address_of_forEncryption_11() { return &___forEncryption_11; }
	inline void set_forEncryption_11(bool value)
	{
		___forEncryption_11 = value;
	}
};

struct AesEngine_t4C35E0FD810D2F75EBEF934ACA7F6F9834C294E8_StaticFields
{
public:
	// System.Byte[] Org.BouncyCastle.Crypto.Engines.AesEngine::S
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___S_0;
	// System.Byte[] Org.BouncyCastle.Crypto.Engines.AesEngine::Si
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___Si_1;
	// System.Byte[] Org.BouncyCastle.Crypto.Engines.AesEngine::rcon
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___rcon_2;
	// System.UInt32[] Org.BouncyCastle.Crypto.Engines.AesEngine::T0
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___T0_3;
	// System.UInt32[] Org.BouncyCastle.Crypto.Engines.AesEngine::Tinv0
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___Tinv0_4;

public:
	inline static int32_t get_offset_of_S_0() { return static_cast<int32_t>(offsetof(AesEngine_t4C35E0FD810D2F75EBEF934ACA7F6F9834C294E8_StaticFields, ___S_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_S_0() const { return ___S_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_S_0() { return &___S_0; }
	inline void set_S_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___S_0 = value;
		Il2CppCodeGenWriteBarrier((&___S_0), value);
	}

	inline static int32_t get_offset_of_Si_1() { return static_cast<int32_t>(offsetof(AesEngine_t4C35E0FD810D2F75EBEF934ACA7F6F9834C294E8_StaticFields, ___Si_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_Si_1() const { return ___Si_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_Si_1() { return &___Si_1; }
	inline void set_Si_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___Si_1 = value;
		Il2CppCodeGenWriteBarrier((&___Si_1), value);
	}

	inline static int32_t get_offset_of_rcon_2() { return static_cast<int32_t>(offsetof(AesEngine_t4C35E0FD810D2F75EBEF934ACA7F6F9834C294E8_StaticFields, ___rcon_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_rcon_2() const { return ___rcon_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_rcon_2() { return &___rcon_2; }
	inline void set_rcon_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___rcon_2 = value;
		Il2CppCodeGenWriteBarrier((&___rcon_2), value);
	}

	inline static int32_t get_offset_of_T0_3() { return static_cast<int32_t>(offsetof(AesEngine_t4C35E0FD810D2F75EBEF934ACA7F6F9834C294E8_StaticFields, ___T0_3)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_T0_3() const { return ___T0_3; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_T0_3() { return &___T0_3; }
	inline void set_T0_3(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___T0_3 = value;
		Il2CppCodeGenWriteBarrier((&___T0_3), value);
	}

	inline static int32_t get_offset_of_Tinv0_4() { return static_cast<int32_t>(offsetof(AesEngine_t4C35E0FD810D2F75EBEF934ACA7F6F9834C294E8_StaticFields, ___Tinv0_4)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_Tinv0_4() const { return ___Tinv0_4; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_Tinv0_4() { return &___Tinv0_4; }
	inline void set_Tinv0_4(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___Tinv0_4 = value;
		Il2CppCodeGenWriteBarrier((&___Tinv0_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AESENGINE_T4C35E0FD810D2F75EBEF934ACA7F6F9834C294E8_H
#ifndef CAMELLIAENGINE_T7A84F7BB09CA0382510AE0D93B8110274195483E_H
#define CAMELLIAENGINE_T7A84F7BB09CA0382510AE0D93B8110274195483E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Engines.CamelliaEngine
struct  CamelliaEngine_t7A84F7BB09CA0382510AE0D93B8110274195483E  : public RuntimeObject
{
public:
	// System.Boolean Org.BouncyCastle.Crypto.Engines.CamelliaEngine::initialised
	bool ___initialised_0;
	// System.Boolean Org.BouncyCastle.Crypto.Engines.CamelliaEngine::_keyIs128
	bool ____keyIs128_1;
	// System.UInt32[] Org.BouncyCastle.Crypto.Engines.CamelliaEngine::subkey
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___subkey_2;
	// System.UInt32[] Org.BouncyCastle.Crypto.Engines.CamelliaEngine::kw
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___kw_3;
	// System.UInt32[] Org.BouncyCastle.Crypto.Engines.CamelliaEngine::ke
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___ke_4;
	// System.UInt32[] Org.BouncyCastle.Crypto.Engines.CamelliaEngine::state
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___state_5;

public:
	inline static int32_t get_offset_of_initialised_0() { return static_cast<int32_t>(offsetof(CamelliaEngine_t7A84F7BB09CA0382510AE0D93B8110274195483E, ___initialised_0)); }
	inline bool get_initialised_0() const { return ___initialised_0; }
	inline bool* get_address_of_initialised_0() { return &___initialised_0; }
	inline void set_initialised_0(bool value)
	{
		___initialised_0 = value;
	}

	inline static int32_t get_offset_of__keyIs128_1() { return static_cast<int32_t>(offsetof(CamelliaEngine_t7A84F7BB09CA0382510AE0D93B8110274195483E, ____keyIs128_1)); }
	inline bool get__keyIs128_1() const { return ____keyIs128_1; }
	inline bool* get_address_of__keyIs128_1() { return &____keyIs128_1; }
	inline void set__keyIs128_1(bool value)
	{
		____keyIs128_1 = value;
	}

	inline static int32_t get_offset_of_subkey_2() { return static_cast<int32_t>(offsetof(CamelliaEngine_t7A84F7BB09CA0382510AE0D93B8110274195483E, ___subkey_2)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_subkey_2() const { return ___subkey_2; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_subkey_2() { return &___subkey_2; }
	inline void set_subkey_2(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___subkey_2 = value;
		Il2CppCodeGenWriteBarrier((&___subkey_2), value);
	}

	inline static int32_t get_offset_of_kw_3() { return static_cast<int32_t>(offsetof(CamelliaEngine_t7A84F7BB09CA0382510AE0D93B8110274195483E, ___kw_3)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_kw_3() const { return ___kw_3; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_kw_3() { return &___kw_3; }
	inline void set_kw_3(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___kw_3 = value;
		Il2CppCodeGenWriteBarrier((&___kw_3), value);
	}

	inline static int32_t get_offset_of_ke_4() { return static_cast<int32_t>(offsetof(CamelliaEngine_t7A84F7BB09CA0382510AE0D93B8110274195483E, ___ke_4)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_ke_4() const { return ___ke_4; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_ke_4() { return &___ke_4; }
	inline void set_ke_4(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___ke_4 = value;
		Il2CppCodeGenWriteBarrier((&___ke_4), value);
	}

	inline static int32_t get_offset_of_state_5() { return static_cast<int32_t>(offsetof(CamelliaEngine_t7A84F7BB09CA0382510AE0D93B8110274195483E, ___state_5)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_state_5() const { return ___state_5; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_state_5() { return &___state_5; }
	inline void set_state_5(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___state_5 = value;
		Il2CppCodeGenWriteBarrier((&___state_5), value);
	}
};

struct CamelliaEngine_t7A84F7BB09CA0382510AE0D93B8110274195483E_StaticFields
{
public:
	// System.UInt32[] Org.BouncyCastle.Crypto.Engines.CamelliaEngine::SIGMA
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___SIGMA_6;
	// System.UInt32[] Org.BouncyCastle.Crypto.Engines.CamelliaEngine::SBOX1_1110
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___SBOX1_1110_7;
	// System.UInt32[] Org.BouncyCastle.Crypto.Engines.CamelliaEngine::SBOX4_4404
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___SBOX4_4404_8;
	// System.UInt32[] Org.BouncyCastle.Crypto.Engines.CamelliaEngine::SBOX2_0222
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___SBOX2_0222_9;
	// System.UInt32[] Org.BouncyCastle.Crypto.Engines.CamelliaEngine::SBOX3_3033
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___SBOX3_3033_10;

public:
	inline static int32_t get_offset_of_SIGMA_6() { return static_cast<int32_t>(offsetof(CamelliaEngine_t7A84F7BB09CA0382510AE0D93B8110274195483E_StaticFields, ___SIGMA_6)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_SIGMA_6() const { return ___SIGMA_6; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_SIGMA_6() { return &___SIGMA_6; }
	inline void set_SIGMA_6(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___SIGMA_6 = value;
		Il2CppCodeGenWriteBarrier((&___SIGMA_6), value);
	}

	inline static int32_t get_offset_of_SBOX1_1110_7() { return static_cast<int32_t>(offsetof(CamelliaEngine_t7A84F7BB09CA0382510AE0D93B8110274195483E_StaticFields, ___SBOX1_1110_7)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_SBOX1_1110_7() const { return ___SBOX1_1110_7; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_SBOX1_1110_7() { return &___SBOX1_1110_7; }
	inline void set_SBOX1_1110_7(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___SBOX1_1110_7 = value;
		Il2CppCodeGenWriteBarrier((&___SBOX1_1110_7), value);
	}

	inline static int32_t get_offset_of_SBOX4_4404_8() { return static_cast<int32_t>(offsetof(CamelliaEngine_t7A84F7BB09CA0382510AE0D93B8110274195483E_StaticFields, ___SBOX4_4404_8)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_SBOX4_4404_8() const { return ___SBOX4_4404_8; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_SBOX4_4404_8() { return &___SBOX4_4404_8; }
	inline void set_SBOX4_4404_8(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___SBOX4_4404_8 = value;
		Il2CppCodeGenWriteBarrier((&___SBOX4_4404_8), value);
	}

	inline static int32_t get_offset_of_SBOX2_0222_9() { return static_cast<int32_t>(offsetof(CamelliaEngine_t7A84F7BB09CA0382510AE0D93B8110274195483E_StaticFields, ___SBOX2_0222_9)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_SBOX2_0222_9() const { return ___SBOX2_0222_9; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_SBOX2_0222_9() { return &___SBOX2_0222_9; }
	inline void set_SBOX2_0222_9(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___SBOX2_0222_9 = value;
		Il2CppCodeGenWriteBarrier((&___SBOX2_0222_9), value);
	}

	inline static int32_t get_offset_of_SBOX3_3033_10() { return static_cast<int32_t>(offsetof(CamelliaEngine_t7A84F7BB09CA0382510AE0D93B8110274195483E_StaticFields, ___SBOX3_3033_10)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_SBOX3_3033_10() const { return ___SBOX3_3033_10; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_SBOX3_3033_10() { return &___SBOX3_3033_10; }
	inline void set_SBOX3_3033_10(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___SBOX3_3033_10 = value;
		Il2CppCodeGenWriteBarrier((&___SBOX3_3033_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMELLIAENGINE_T7A84F7BB09CA0382510AE0D93B8110274195483E_H
#ifndef DESENGINE_T88E9C8F289740CDAE4C6C0EAEABA2059935EF450_H
#define DESENGINE_T88E9C8F289740CDAE4C6C0EAEABA2059935EF450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Engines.DesEngine
struct  DesEngine_t88E9C8F289740CDAE4C6C0EAEABA2059935EF450  : public RuntimeObject
{
public:
	// System.Int32[] Org.BouncyCastle.Crypto.Engines.DesEngine::workingKey
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___workingKey_0;

public:
	inline static int32_t get_offset_of_workingKey_0() { return static_cast<int32_t>(offsetof(DesEngine_t88E9C8F289740CDAE4C6C0EAEABA2059935EF450, ___workingKey_0)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_workingKey_0() const { return ___workingKey_0; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_workingKey_0() { return &___workingKey_0; }
	inline void set_workingKey_0(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___workingKey_0 = value;
		Il2CppCodeGenWriteBarrier((&___workingKey_0), value);
	}
};

struct DesEngine_t88E9C8F289740CDAE4C6C0EAEABA2059935EF450_StaticFields
{
public:
	// System.Int16[] Org.BouncyCastle.Crypto.Engines.DesEngine::bytebit
	Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* ___bytebit_1;
	// System.Int32[] Org.BouncyCastle.Crypto.Engines.DesEngine::bigbyte
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___bigbyte_2;
	// System.Byte[] Org.BouncyCastle.Crypto.Engines.DesEngine::pc1
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___pc1_3;
	// System.Byte[] Org.BouncyCastle.Crypto.Engines.DesEngine::totrot
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___totrot_4;
	// System.Byte[] Org.BouncyCastle.Crypto.Engines.DesEngine::pc2
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___pc2_5;
	// System.UInt32[] Org.BouncyCastle.Crypto.Engines.DesEngine::SP1
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___SP1_6;
	// System.UInt32[] Org.BouncyCastle.Crypto.Engines.DesEngine::SP2
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___SP2_7;
	// System.UInt32[] Org.BouncyCastle.Crypto.Engines.DesEngine::SP3
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___SP3_8;
	// System.UInt32[] Org.BouncyCastle.Crypto.Engines.DesEngine::SP4
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___SP4_9;
	// System.UInt32[] Org.BouncyCastle.Crypto.Engines.DesEngine::SP5
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___SP5_10;
	// System.UInt32[] Org.BouncyCastle.Crypto.Engines.DesEngine::SP6
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___SP6_11;
	// System.UInt32[] Org.BouncyCastle.Crypto.Engines.DesEngine::SP7
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___SP7_12;
	// System.UInt32[] Org.BouncyCastle.Crypto.Engines.DesEngine::SP8
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___SP8_13;

public:
	inline static int32_t get_offset_of_bytebit_1() { return static_cast<int32_t>(offsetof(DesEngine_t88E9C8F289740CDAE4C6C0EAEABA2059935EF450_StaticFields, ___bytebit_1)); }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* get_bytebit_1() const { return ___bytebit_1; }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28** get_address_of_bytebit_1() { return &___bytebit_1; }
	inline void set_bytebit_1(Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* value)
	{
		___bytebit_1 = value;
		Il2CppCodeGenWriteBarrier((&___bytebit_1), value);
	}

	inline static int32_t get_offset_of_bigbyte_2() { return static_cast<int32_t>(offsetof(DesEngine_t88E9C8F289740CDAE4C6C0EAEABA2059935EF450_StaticFields, ___bigbyte_2)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_bigbyte_2() const { return ___bigbyte_2; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_bigbyte_2() { return &___bigbyte_2; }
	inline void set_bigbyte_2(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___bigbyte_2 = value;
		Il2CppCodeGenWriteBarrier((&___bigbyte_2), value);
	}

	inline static int32_t get_offset_of_pc1_3() { return static_cast<int32_t>(offsetof(DesEngine_t88E9C8F289740CDAE4C6C0EAEABA2059935EF450_StaticFields, ___pc1_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_pc1_3() const { return ___pc1_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_pc1_3() { return &___pc1_3; }
	inline void set_pc1_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___pc1_3 = value;
		Il2CppCodeGenWriteBarrier((&___pc1_3), value);
	}

	inline static int32_t get_offset_of_totrot_4() { return static_cast<int32_t>(offsetof(DesEngine_t88E9C8F289740CDAE4C6C0EAEABA2059935EF450_StaticFields, ___totrot_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_totrot_4() const { return ___totrot_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_totrot_4() { return &___totrot_4; }
	inline void set_totrot_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___totrot_4 = value;
		Il2CppCodeGenWriteBarrier((&___totrot_4), value);
	}

	inline static int32_t get_offset_of_pc2_5() { return static_cast<int32_t>(offsetof(DesEngine_t88E9C8F289740CDAE4C6C0EAEABA2059935EF450_StaticFields, ___pc2_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_pc2_5() const { return ___pc2_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_pc2_5() { return &___pc2_5; }
	inline void set_pc2_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___pc2_5 = value;
		Il2CppCodeGenWriteBarrier((&___pc2_5), value);
	}

	inline static int32_t get_offset_of_SP1_6() { return static_cast<int32_t>(offsetof(DesEngine_t88E9C8F289740CDAE4C6C0EAEABA2059935EF450_StaticFields, ___SP1_6)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_SP1_6() const { return ___SP1_6; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_SP1_6() { return &___SP1_6; }
	inline void set_SP1_6(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___SP1_6 = value;
		Il2CppCodeGenWriteBarrier((&___SP1_6), value);
	}

	inline static int32_t get_offset_of_SP2_7() { return static_cast<int32_t>(offsetof(DesEngine_t88E9C8F289740CDAE4C6C0EAEABA2059935EF450_StaticFields, ___SP2_7)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_SP2_7() const { return ___SP2_7; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_SP2_7() { return &___SP2_7; }
	inline void set_SP2_7(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___SP2_7 = value;
		Il2CppCodeGenWriteBarrier((&___SP2_7), value);
	}

	inline static int32_t get_offset_of_SP3_8() { return static_cast<int32_t>(offsetof(DesEngine_t88E9C8F289740CDAE4C6C0EAEABA2059935EF450_StaticFields, ___SP3_8)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_SP3_8() const { return ___SP3_8; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_SP3_8() { return &___SP3_8; }
	inline void set_SP3_8(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___SP3_8 = value;
		Il2CppCodeGenWriteBarrier((&___SP3_8), value);
	}

	inline static int32_t get_offset_of_SP4_9() { return static_cast<int32_t>(offsetof(DesEngine_t88E9C8F289740CDAE4C6C0EAEABA2059935EF450_StaticFields, ___SP4_9)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_SP4_9() const { return ___SP4_9; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_SP4_9() { return &___SP4_9; }
	inline void set_SP4_9(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___SP4_9 = value;
		Il2CppCodeGenWriteBarrier((&___SP4_9), value);
	}

	inline static int32_t get_offset_of_SP5_10() { return static_cast<int32_t>(offsetof(DesEngine_t88E9C8F289740CDAE4C6C0EAEABA2059935EF450_StaticFields, ___SP5_10)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_SP5_10() const { return ___SP5_10; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_SP5_10() { return &___SP5_10; }
	inline void set_SP5_10(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___SP5_10 = value;
		Il2CppCodeGenWriteBarrier((&___SP5_10), value);
	}

	inline static int32_t get_offset_of_SP6_11() { return static_cast<int32_t>(offsetof(DesEngine_t88E9C8F289740CDAE4C6C0EAEABA2059935EF450_StaticFields, ___SP6_11)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_SP6_11() const { return ___SP6_11; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_SP6_11() { return &___SP6_11; }
	inline void set_SP6_11(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___SP6_11 = value;
		Il2CppCodeGenWriteBarrier((&___SP6_11), value);
	}

	inline static int32_t get_offset_of_SP7_12() { return static_cast<int32_t>(offsetof(DesEngine_t88E9C8F289740CDAE4C6C0EAEABA2059935EF450_StaticFields, ___SP7_12)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_SP7_12() const { return ___SP7_12; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_SP7_12() { return &___SP7_12; }
	inline void set_SP7_12(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___SP7_12 = value;
		Il2CppCodeGenWriteBarrier((&___SP7_12), value);
	}

	inline static int32_t get_offset_of_SP8_13() { return static_cast<int32_t>(offsetof(DesEngine_t88E9C8F289740CDAE4C6C0EAEABA2059935EF450_StaticFields, ___SP8_13)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_SP8_13() const { return ___SP8_13; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_SP8_13() { return &___SP8_13; }
	inline void set_SP8_13(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___SP8_13 = value;
		Il2CppCodeGenWriteBarrier((&___SP8_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESENGINE_T88E9C8F289740CDAE4C6C0EAEABA2059935EF450_H
#ifndef GOST28147ENGINE_TDB141472A559444EDC7FBFC2FA573A33F542D532_H
#define GOST28147ENGINE_TDB141472A559444EDC7FBFC2FA573A33F542D532_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Engines.Gost28147Engine
struct  Gost28147Engine_tDB141472A559444EDC7FBFC2FA573A33F542D532  : public RuntimeObject
{
public:
	// System.Int32[] Org.BouncyCastle.Crypto.Engines.Gost28147Engine::workingKey
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___workingKey_0;
	// System.Boolean Org.BouncyCastle.Crypto.Engines.Gost28147Engine::forEncryption
	bool ___forEncryption_1;
	// System.Byte[] Org.BouncyCastle.Crypto.Engines.Gost28147Engine::S
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___S_2;

public:
	inline static int32_t get_offset_of_workingKey_0() { return static_cast<int32_t>(offsetof(Gost28147Engine_tDB141472A559444EDC7FBFC2FA573A33F542D532, ___workingKey_0)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_workingKey_0() const { return ___workingKey_0; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_workingKey_0() { return &___workingKey_0; }
	inline void set_workingKey_0(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___workingKey_0 = value;
		Il2CppCodeGenWriteBarrier((&___workingKey_0), value);
	}

	inline static int32_t get_offset_of_forEncryption_1() { return static_cast<int32_t>(offsetof(Gost28147Engine_tDB141472A559444EDC7FBFC2FA573A33F542D532, ___forEncryption_1)); }
	inline bool get_forEncryption_1() const { return ___forEncryption_1; }
	inline bool* get_address_of_forEncryption_1() { return &___forEncryption_1; }
	inline void set_forEncryption_1(bool value)
	{
		___forEncryption_1 = value;
	}

	inline static int32_t get_offset_of_S_2() { return static_cast<int32_t>(offsetof(Gost28147Engine_tDB141472A559444EDC7FBFC2FA573A33F542D532, ___S_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_S_2() const { return ___S_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_S_2() { return &___S_2; }
	inline void set_S_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___S_2 = value;
		Il2CppCodeGenWriteBarrier((&___S_2), value);
	}
};

struct Gost28147Engine_tDB141472A559444EDC7FBFC2FA573A33F542D532_StaticFields
{
public:
	// System.Byte[] Org.BouncyCastle.Crypto.Engines.Gost28147Engine::Sbox_Default
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___Sbox_Default_3;
	// System.Byte[] Org.BouncyCastle.Crypto.Engines.Gost28147Engine::ESbox_Test
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___ESbox_Test_4;
	// System.Byte[] Org.BouncyCastle.Crypto.Engines.Gost28147Engine::ESbox_A
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___ESbox_A_5;
	// System.Byte[] Org.BouncyCastle.Crypto.Engines.Gost28147Engine::ESbox_B
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___ESbox_B_6;
	// System.Byte[] Org.BouncyCastle.Crypto.Engines.Gost28147Engine::ESbox_C
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___ESbox_C_7;
	// System.Byte[] Org.BouncyCastle.Crypto.Engines.Gost28147Engine::ESbox_D
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___ESbox_D_8;
	// System.Byte[] Org.BouncyCastle.Crypto.Engines.Gost28147Engine::DSbox_Test
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___DSbox_Test_9;
	// System.Byte[] Org.BouncyCastle.Crypto.Engines.Gost28147Engine::DSbox_A
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___DSbox_A_10;
	// System.Collections.IDictionary Org.BouncyCastle.Crypto.Engines.Gost28147Engine::sBoxes
	RuntimeObject* ___sBoxes_11;

public:
	inline static int32_t get_offset_of_Sbox_Default_3() { return static_cast<int32_t>(offsetof(Gost28147Engine_tDB141472A559444EDC7FBFC2FA573A33F542D532_StaticFields, ___Sbox_Default_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_Sbox_Default_3() const { return ___Sbox_Default_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_Sbox_Default_3() { return &___Sbox_Default_3; }
	inline void set_Sbox_Default_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___Sbox_Default_3 = value;
		Il2CppCodeGenWriteBarrier((&___Sbox_Default_3), value);
	}

	inline static int32_t get_offset_of_ESbox_Test_4() { return static_cast<int32_t>(offsetof(Gost28147Engine_tDB141472A559444EDC7FBFC2FA573A33F542D532_StaticFields, ___ESbox_Test_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_ESbox_Test_4() const { return ___ESbox_Test_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_ESbox_Test_4() { return &___ESbox_Test_4; }
	inline void set_ESbox_Test_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___ESbox_Test_4 = value;
		Il2CppCodeGenWriteBarrier((&___ESbox_Test_4), value);
	}

	inline static int32_t get_offset_of_ESbox_A_5() { return static_cast<int32_t>(offsetof(Gost28147Engine_tDB141472A559444EDC7FBFC2FA573A33F542D532_StaticFields, ___ESbox_A_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_ESbox_A_5() const { return ___ESbox_A_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_ESbox_A_5() { return &___ESbox_A_5; }
	inline void set_ESbox_A_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___ESbox_A_5 = value;
		Il2CppCodeGenWriteBarrier((&___ESbox_A_5), value);
	}

	inline static int32_t get_offset_of_ESbox_B_6() { return static_cast<int32_t>(offsetof(Gost28147Engine_tDB141472A559444EDC7FBFC2FA573A33F542D532_StaticFields, ___ESbox_B_6)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_ESbox_B_6() const { return ___ESbox_B_6; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_ESbox_B_6() { return &___ESbox_B_6; }
	inline void set_ESbox_B_6(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___ESbox_B_6 = value;
		Il2CppCodeGenWriteBarrier((&___ESbox_B_6), value);
	}

	inline static int32_t get_offset_of_ESbox_C_7() { return static_cast<int32_t>(offsetof(Gost28147Engine_tDB141472A559444EDC7FBFC2FA573A33F542D532_StaticFields, ___ESbox_C_7)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_ESbox_C_7() const { return ___ESbox_C_7; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_ESbox_C_7() { return &___ESbox_C_7; }
	inline void set_ESbox_C_7(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___ESbox_C_7 = value;
		Il2CppCodeGenWriteBarrier((&___ESbox_C_7), value);
	}

	inline static int32_t get_offset_of_ESbox_D_8() { return static_cast<int32_t>(offsetof(Gost28147Engine_tDB141472A559444EDC7FBFC2FA573A33F542D532_StaticFields, ___ESbox_D_8)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_ESbox_D_8() const { return ___ESbox_D_8; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_ESbox_D_8() { return &___ESbox_D_8; }
	inline void set_ESbox_D_8(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___ESbox_D_8 = value;
		Il2CppCodeGenWriteBarrier((&___ESbox_D_8), value);
	}

	inline static int32_t get_offset_of_DSbox_Test_9() { return static_cast<int32_t>(offsetof(Gost28147Engine_tDB141472A559444EDC7FBFC2FA573A33F542D532_StaticFields, ___DSbox_Test_9)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_DSbox_Test_9() const { return ___DSbox_Test_9; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_DSbox_Test_9() { return &___DSbox_Test_9; }
	inline void set_DSbox_Test_9(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___DSbox_Test_9 = value;
		Il2CppCodeGenWriteBarrier((&___DSbox_Test_9), value);
	}

	inline static int32_t get_offset_of_DSbox_A_10() { return static_cast<int32_t>(offsetof(Gost28147Engine_tDB141472A559444EDC7FBFC2FA573A33F542D532_StaticFields, ___DSbox_A_10)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_DSbox_A_10() const { return ___DSbox_A_10; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_DSbox_A_10() { return &___DSbox_A_10; }
	inline void set_DSbox_A_10(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___DSbox_A_10 = value;
		Il2CppCodeGenWriteBarrier((&___DSbox_A_10), value);
	}

	inline static int32_t get_offset_of_sBoxes_11() { return static_cast<int32_t>(offsetof(Gost28147Engine_tDB141472A559444EDC7FBFC2FA573A33F542D532_StaticFields, ___sBoxes_11)); }
	inline RuntimeObject* get_sBoxes_11() const { return ___sBoxes_11; }
	inline RuntimeObject** get_address_of_sBoxes_11() { return &___sBoxes_11; }
	inline void set_sBoxes_11(RuntimeObject* value)
	{
		___sBoxes_11 = value;
		Il2CppCodeGenWriteBarrier((&___sBoxes_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GOST28147ENGINE_TDB141472A559444EDC7FBFC2FA573A33F542D532_H
#ifndef RC4ENGINE_T7DD4678C1AFD6D45B92C008CE07A2F9E7D81A576_H
#define RC4ENGINE_T7DD4678C1AFD6D45B92C008CE07A2F9E7D81A576_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Engines.RC4Engine
struct  RC4Engine_t7DD4678C1AFD6D45B92C008CE07A2F9E7D81A576  : public RuntimeObject
{
public:
	// System.Byte[] Org.BouncyCastle.Crypto.Engines.RC4Engine::engineState
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___engineState_1;
	// System.Int32 Org.BouncyCastle.Crypto.Engines.RC4Engine::x
	int32_t ___x_2;
	// System.Int32 Org.BouncyCastle.Crypto.Engines.RC4Engine::y
	int32_t ___y_3;
	// System.Byte[] Org.BouncyCastle.Crypto.Engines.RC4Engine::workingKey
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___workingKey_4;

public:
	inline static int32_t get_offset_of_engineState_1() { return static_cast<int32_t>(offsetof(RC4Engine_t7DD4678C1AFD6D45B92C008CE07A2F9E7D81A576, ___engineState_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_engineState_1() const { return ___engineState_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_engineState_1() { return &___engineState_1; }
	inline void set_engineState_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___engineState_1 = value;
		Il2CppCodeGenWriteBarrier((&___engineState_1), value);
	}

	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(RC4Engine_t7DD4678C1AFD6D45B92C008CE07A2F9E7D81A576, ___x_2)); }
	inline int32_t get_x_2() const { return ___x_2; }
	inline int32_t* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(int32_t value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(RC4Engine_t7DD4678C1AFD6D45B92C008CE07A2F9E7D81A576, ___y_3)); }
	inline int32_t get_y_3() const { return ___y_3; }
	inline int32_t* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(int32_t value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_workingKey_4() { return static_cast<int32_t>(offsetof(RC4Engine_t7DD4678C1AFD6D45B92C008CE07A2F9E7D81A576, ___workingKey_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_workingKey_4() const { return ___workingKey_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_workingKey_4() { return &___workingKey_4; }
	inline void set_workingKey_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___workingKey_4 = value;
		Il2CppCodeGenWriteBarrier((&___workingKey_4), value);
	}
};

struct RC4Engine_t7DD4678C1AFD6D45B92C008CE07A2F9E7D81A576_StaticFields
{
public:
	// System.Int32 Org.BouncyCastle.Crypto.Engines.RC4Engine::STATE_LENGTH
	int32_t ___STATE_LENGTH_0;

public:
	inline static int32_t get_offset_of_STATE_LENGTH_0() { return static_cast<int32_t>(offsetof(RC4Engine_t7DD4678C1AFD6D45B92C008CE07A2F9E7D81A576_StaticFields, ___STATE_LENGTH_0)); }
	inline int32_t get_STATE_LENGTH_0() const { return ___STATE_LENGTH_0; }
	inline int32_t* get_address_of_STATE_LENGTH_0() { return &___STATE_LENGTH_0; }
	inline void set_STATE_LENGTH_0(int32_t value)
	{
		___STATE_LENGTH_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RC4ENGINE_T7DD4678C1AFD6D45B92C008CE07A2F9E7D81A576_H
#ifndef RSABLINDEDENGINE_TEBCD12A0EB19941C48DD1394340083BA0F2CAB9C_H
#define RSABLINDEDENGINE_TEBCD12A0EB19941C48DD1394340083BA0F2CAB9C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Engines.RsaBlindedEngine
struct  RsaBlindedEngine_tEBCD12A0EB19941C48DD1394340083BA0F2CAB9C  : public RuntimeObject
{
public:
	// Org.BouncyCastle.Crypto.Engines.RsaCoreEngine Org.BouncyCastle.Crypto.Engines.RsaBlindedEngine::core
	RsaCoreEngine_t34DD3F15CAE1DA560B6C6C8E475F6951E197DC64 * ___core_0;
	// Org.BouncyCastle.Crypto.Parameters.RsaKeyParameters Org.BouncyCastle.Crypto.Engines.RsaBlindedEngine::key
	RsaKeyParameters_t57791EDEBAF0AD93C55C7FCF645D7CBB6284D308 * ___key_1;
	// Org.BouncyCastle.Security.SecureRandom Org.BouncyCastle.Crypto.Engines.RsaBlindedEngine::random
	SecureRandom_t0B6C2C3EDF11BF4FD22E44CE8997F6A0D2F142B0 * ___random_2;

public:
	inline static int32_t get_offset_of_core_0() { return static_cast<int32_t>(offsetof(RsaBlindedEngine_tEBCD12A0EB19941C48DD1394340083BA0F2CAB9C, ___core_0)); }
	inline RsaCoreEngine_t34DD3F15CAE1DA560B6C6C8E475F6951E197DC64 * get_core_0() const { return ___core_0; }
	inline RsaCoreEngine_t34DD3F15CAE1DA560B6C6C8E475F6951E197DC64 ** get_address_of_core_0() { return &___core_0; }
	inline void set_core_0(RsaCoreEngine_t34DD3F15CAE1DA560B6C6C8E475F6951E197DC64 * value)
	{
		___core_0 = value;
		Il2CppCodeGenWriteBarrier((&___core_0), value);
	}

	inline static int32_t get_offset_of_key_1() { return static_cast<int32_t>(offsetof(RsaBlindedEngine_tEBCD12A0EB19941C48DD1394340083BA0F2CAB9C, ___key_1)); }
	inline RsaKeyParameters_t57791EDEBAF0AD93C55C7FCF645D7CBB6284D308 * get_key_1() const { return ___key_1; }
	inline RsaKeyParameters_t57791EDEBAF0AD93C55C7FCF645D7CBB6284D308 ** get_address_of_key_1() { return &___key_1; }
	inline void set_key_1(RsaKeyParameters_t57791EDEBAF0AD93C55C7FCF645D7CBB6284D308 * value)
	{
		___key_1 = value;
		Il2CppCodeGenWriteBarrier((&___key_1), value);
	}

	inline static int32_t get_offset_of_random_2() { return static_cast<int32_t>(offsetof(RsaBlindedEngine_tEBCD12A0EB19941C48DD1394340083BA0F2CAB9C, ___random_2)); }
	inline SecureRandom_t0B6C2C3EDF11BF4FD22E44CE8997F6A0D2F142B0 * get_random_2() const { return ___random_2; }
	inline SecureRandom_t0B6C2C3EDF11BF4FD22E44CE8997F6A0D2F142B0 ** get_address_of_random_2() { return &___random_2; }
	inline void set_random_2(SecureRandom_t0B6C2C3EDF11BF4FD22E44CE8997F6A0D2F142B0 * value)
	{
		___random_2 = value;
		Il2CppCodeGenWriteBarrier((&___random_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RSABLINDEDENGINE_TEBCD12A0EB19941C48DD1394340083BA0F2CAB9C_H
#ifndef RSACOREENGINE_T34DD3F15CAE1DA560B6C6C8E475F6951E197DC64_H
#define RSACOREENGINE_T34DD3F15CAE1DA560B6C6C8E475F6951E197DC64_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Engines.RsaCoreEngine
struct  RsaCoreEngine_t34DD3F15CAE1DA560B6C6C8E475F6951E197DC64  : public RuntimeObject
{
public:
	// Org.BouncyCastle.Crypto.Parameters.RsaKeyParameters Org.BouncyCastle.Crypto.Engines.RsaCoreEngine::key
	RsaKeyParameters_t57791EDEBAF0AD93C55C7FCF645D7CBB6284D308 * ___key_0;
	// System.Boolean Org.BouncyCastle.Crypto.Engines.RsaCoreEngine::forEncryption
	bool ___forEncryption_1;
	// System.Int32 Org.BouncyCastle.Crypto.Engines.RsaCoreEngine::bitSize
	int32_t ___bitSize_2;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(RsaCoreEngine_t34DD3F15CAE1DA560B6C6C8E475F6951E197DC64, ___key_0)); }
	inline RsaKeyParameters_t57791EDEBAF0AD93C55C7FCF645D7CBB6284D308 * get_key_0() const { return ___key_0; }
	inline RsaKeyParameters_t57791EDEBAF0AD93C55C7FCF645D7CBB6284D308 ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(RsaKeyParameters_t57791EDEBAF0AD93C55C7FCF645D7CBB6284D308 * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_forEncryption_1() { return static_cast<int32_t>(offsetof(RsaCoreEngine_t34DD3F15CAE1DA560B6C6C8E475F6951E197DC64, ___forEncryption_1)); }
	inline bool get_forEncryption_1() const { return ___forEncryption_1; }
	inline bool* get_address_of_forEncryption_1() { return &___forEncryption_1; }
	inline void set_forEncryption_1(bool value)
	{
		___forEncryption_1 = value;
	}

	inline static int32_t get_offset_of_bitSize_2() { return static_cast<int32_t>(offsetof(RsaCoreEngine_t34DD3F15CAE1DA560B6C6C8E475F6951E197DC64, ___bitSize_2)); }
	inline int32_t get_bitSize_2() const { return ___bitSize_2; }
	inline int32_t* get_address_of_bitSize_2() { return &___bitSize_2; }
	inline void set_bitSize_2(int32_t value)
	{
		___bitSize_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RSACOREENGINE_T34DD3F15CAE1DA560B6C6C8E475F6951E197DC64_H
#ifndef SALSA20ENGINE_T5346D9847A884ABE981EC15CAEC9A2E09A6B4E5C_H
#define SALSA20ENGINE_T5346D9847A884ABE981EC15CAEC9A2E09A6B4E5C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Engines.Salsa20Engine
struct  Salsa20Engine_t5346D9847A884ABE981EC15CAEC9A2E09A6B4E5C  : public RuntimeObject
{
public:
	// System.Int32 Org.BouncyCastle.Crypto.Engines.Salsa20Engine::rounds
	int32_t ___rounds_3;
	// System.Int32 Org.BouncyCastle.Crypto.Engines.Salsa20Engine::index
	int32_t ___index_4;
	// System.UInt32[] Org.BouncyCastle.Crypto.Engines.Salsa20Engine::engineState
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___engineState_5;
	// System.UInt32[] Org.BouncyCastle.Crypto.Engines.Salsa20Engine::x
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___x_6;
	// System.Byte[] Org.BouncyCastle.Crypto.Engines.Salsa20Engine::keyStream
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___keyStream_7;
	// System.Boolean Org.BouncyCastle.Crypto.Engines.Salsa20Engine::initialised
	bool ___initialised_8;
	// System.UInt32 Org.BouncyCastle.Crypto.Engines.Salsa20Engine::cW0
	uint32_t ___cW0_9;
	// System.UInt32 Org.BouncyCastle.Crypto.Engines.Salsa20Engine::cW1
	uint32_t ___cW1_10;
	// System.UInt32 Org.BouncyCastle.Crypto.Engines.Salsa20Engine::cW2
	uint32_t ___cW2_11;

public:
	inline static int32_t get_offset_of_rounds_3() { return static_cast<int32_t>(offsetof(Salsa20Engine_t5346D9847A884ABE981EC15CAEC9A2E09A6B4E5C, ___rounds_3)); }
	inline int32_t get_rounds_3() const { return ___rounds_3; }
	inline int32_t* get_address_of_rounds_3() { return &___rounds_3; }
	inline void set_rounds_3(int32_t value)
	{
		___rounds_3 = value;
	}

	inline static int32_t get_offset_of_index_4() { return static_cast<int32_t>(offsetof(Salsa20Engine_t5346D9847A884ABE981EC15CAEC9A2E09A6B4E5C, ___index_4)); }
	inline int32_t get_index_4() const { return ___index_4; }
	inline int32_t* get_address_of_index_4() { return &___index_4; }
	inline void set_index_4(int32_t value)
	{
		___index_4 = value;
	}

	inline static int32_t get_offset_of_engineState_5() { return static_cast<int32_t>(offsetof(Salsa20Engine_t5346D9847A884ABE981EC15CAEC9A2E09A6B4E5C, ___engineState_5)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_engineState_5() const { return ___engineState_5; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_engineState_5() { return &___engineState_5; }
	inline void set_engineState_5(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___engineState_5 = value;
		Il2CppCodeGenWriteBarrier((&___engineState_5), value);
	}

	inline static int32_t get_offset_of_x_6() { return static_cast<int32_t>(offsetof(Salsa20Engine_t5346D9847A884ABE981EC15CAEC9A2E09A6B4E5C, ___x_6)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_x_6() const { return ___x_6; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_x_6() { return &___x_6; }
	inline void set_x_6(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___x_6 = value;
		Il2CppCodeGenWriteBarrier((&___x_6), value);
	}

	inline static int32_t get_offset_of_keyStream_7() { return static_cast<int32_t>(offsetof(Salsa20Engine_t5346D9847A884ABE981EC15CAEC9A2E09A6B4E5C, ___keyStream_7)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_keyStream_7() const { return ___keyStream_7; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_keyStream_7() { return &___keyStream_7; }
	inline void set_keyStream_7(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___keyStream_7 = value;
		Il2CppCodeGenWriteBarrier((&___keyStream_7), value);
	}

	inline static int32_t get_offset_of_initialised_8() { return static_cast<int32_t>(offsetof(Salsa20Engine_t5346D9847A884ABE981EC15CAEC9A2E09A6B4E5C, ___initialised_8)); }
	inline bool get_initialised_8() const { return ___initialised_8; }
	inline bool* get_address_of_initialised_8() { return &___initialised_8; }
	inline void set_initialised_8(bool value)
	{
		___initialised_8 = value;
	}

	inline static int32_t get_offset_of_cW0_9() { return static_cast<int32_t>(offsetof(Salsa20Engine_t5346D9847A884ABE981EC15CAEC9A2E09A6B4E5C, ___cW0_9)); }
	inline uint32_t get_cW0_9() const { return ___cW0_9; }
	inline uint32_t* get_address_of_cW0_9() { return &___cW0_9; }
	inline void set_cW0_9(uint32_t value)
	{
		___cW0_9 = value;
	}

	inline static int32_t get_offset_of_cW1_10() { return static_cast<int32_t>(offsetof(Salsa20Engine_t5346D9847A884ABE981EC15CAEC9A2E09A6B4E5C, ___cW1_10)); }
	inline uint32_t get_cW1_10() const { return ___cW1_10; }
	inline uint32_t* get_address_of_cW1_10() { return &___cW1_10; }
	inline void set_cW1_10(uint32_t value)
	{
		___cW1_10 = value;
	}

	inline static int32_t get_offset_of_cW2_11() { return static_cast<int32_t>(offsetof(Salsa20Engine_t5346D9847A884ABE981EC15CAEC9A2E09A6B4E5C, ___cW2_11)); }
	inline uint32_t get_cW2_11() const { return ___cW2_11; }
	inline uint32_t* get_address_of_cW2_11() { return &___cW2_11; }
	inline void set_cW2_11(uint32_t value)
	{
		___cW2_11 = value;
	}
};

struct Salsa20Engine_t5346D9847A884ABE981EC15CAEC9A2E09A6B4E5C_StaticFields
{
public:
	// System.Int32 Org.BouncyCastle.Crypto.Engines.Salsa20Engine::DEFAULT_ROUNDS
	int32_t ___DEFAULT_ROUNDS_0;
	// System.Byte[] Org.BouncyCastle.Crypto.Engines.Salsa20Engine::sigma
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___sigma_1;
	// System.Byte[] Org.BouncyCastle.Crypto.Engines.Salsa20Engine::tau
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___tau_2;

public:
	inline static int32_t get_offset_of_DEFAULT_ROUNDS_0() { return static_cast<int32_t>(offsetof(Salsa20Engine_t5346D9847A884ABE981EC15CAEC9A2E09A6B4E5C_StaticFields, ___DEFAULT_ROUNDS_0)); }
	inline int32_t get_DEFAULT_ROUNDS_0() const { return ___DEFAULT_ROUNDS_0; }
	inline int32_t* get_address_of_DEFAULT_ROUNDS_0() { return &___DEFAULT_ROUNDS_0; }
	inline void set_DEFAULT_ROUNDS_0(int32_t value)
	{
		___DEFAULT_ROUNDS_0 = value;
	}

	inline static int32_t get_offset_of_sigma_1() { return static_cast<int32_t>(offsetof(Salsa20Engine_t5346D9847A884ABE981EC15CAEC9A2E09A6B4E5C_StaticFields, ___sigma_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_sigma_1() const { return ___sigma_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_sigma_1() { return &___sigma_1; }
	inline void set_sigma_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___sigma_1 = value;
		Il2CppCodeGenWriteBarrier((&___sigma_1), value);
	}

	inline static int32_t get_offset_of_tau_2() { return static_cast<int32_t>(offsetof(Salsa20Engine_t5346D9847A884ABE981EC15CAEC9A2E09A6B4E5C_StaticFields, ___tau_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_tau_2() const { return ___tau_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_tau_2() { return &___tau_2; }
	inline void set_tau_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___tau_2 = value;
		Il2CppCodeGenWriteBarrier((&___tau_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SALSA20ENGINE_T5346D9847A884ABE981EC15CAEC9A2E09A6B4E5C_H
#ifndef KEYGENERATIONPARAMETERS_TDA1E9297A6F944BA4FF44DBC3E94089DB7F7D2B6_H
#define KEYGENERATIONPARAMETERS_TDA1E9297A6F944BA4FF44DBC3E94089DB7F7D2B6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.KeyGenerationParameters
struct  KeyGenerationParameters_tDA1E9297A6F944BA4FF44DBC3E94089DB7F7D2B6  : public RuntimeObject
{
public:
	// Org.BouncyCastle.Security.SecureRandom Org.BouncyCastle.Crypto.KeyGenerationParameters::random
	SecureRandom_t0B6C2C3EDF11BF4FD22E44CE8997F6A0D2F142B0 * ___random_0;
	// System.Int32 Org.BouncyCastle.Crypto.KeyGenerationParameters::strength
	int32_t ___strength_1;

public:
	inline static int32_t get_offset_of_random_0() { return static_cast<int32_t>(offsetof(KeyGenerationParameters_tDA1E9297A6F944BA4FF44DBC3E94089DB7F7D2B6, ___random_0)); }
	inline SecureRandom_t0B6C2C3EDF11BF4FD22E44CE8997F6A0D2F142B0 * get_random_0() const { return ___random_0; }
	inline SecureRandom_t0B6C2C3EDF11BF4FD22E44CE8997F6A0D2F142B0 ** get_address_of_random_0() { return &___random_0; }
	inline void set_random_0(SecureRandom_t0B6C2C3EDF11BF4FD22E44CE8997F6A0D2F142B0 * value)
	{
		___random_0 = value;
		Il2CppCodeGenWriteBarrier((&___random_0), value);
	}

	inline static int32_t get_offset_of_strength_1() { return static_cast<int32_t>(offsetof(KeyGenerationParameters_tDA1E9297A6F944BA4FF44DBC3E94089DB7F7D2B6, ___strength_1)); }
	inline int32_t get_strength_1() const { return ___strength_1; }
	inline int32_t* get_address_of_strength_1() { return &___strength_1; }
	inline void set_strength_1(int32_t value)
	{
		___strength_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYGENERATIONPARAMETERS_TDA1E9297A6F944BA4FF44DBC3E94089DB7F7D2B6_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef C2PNB304W1HOLDER_T6A9A5917DB2CA3D55130781A13C8D44F3E1C5006_H
#define C2PNB304W1HOLDER_T6A9A5917DB2CA3D55130781A13C8D44F3E1C5006_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.X9.X962NamedCurves_C2pnb304w1Holder
struct  C2pnb304w1Holder_t6A9A5917DB2CA3D55130781A13C8D44F3E1C5006  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct C2pnb304w1Holder_t6A9A5917DB2CA3D55130781A13C8D44F3E1C5006_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Asn1.X9.X962NamedCurves_C2pnb304w1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(C2pnb304w1Holder_t6A9A5917DB2CA3D55130781A13C8D44F3E1C5006_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // C2PNB304W1HOLDER_T6A9A5917DB2CA3D55130781A13C8D44F3E1C5006_H
#ifndef C2PNB368W1HOLDER_T5052AD1B0C8BBC973E159B7672DFB7DBCD04978F_H
#define C2PNB368W1HOLDER_T5052AD1B0C8BBC973E159B7672DFB7DBCD04978F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.X9.X962NamedCurves_C2pnb368w1Holder
struct  C2pnb368w1Holder_t5052AD1B0C8BBC973E159B7672DFB7DBCD04978F  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct C2pnb368w1Holder_t5052AD1B0C8BBC973E159B7672DFB7DBCD04978F_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Asn1.X9.X962NamedCurves_C2pnb368w1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(C2pnb368w1Holder_t5052AD1B0C8BBC973E159B7672DFB7DBCD04978F_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // C2PNB368W1HOLDER_T5052AD1B0C8BBC973E159B7672DFB7DBCD04978F_H
#ifndef C2TNB359V1HOLDER_T8B93223E821CADB44BDBCE85BCD4077EE4888861_H
#define C2TNB359V1HOLDER_T8B93223E821CADB44BDBCE85BCD4077EE4888861_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.X9.X962NamedCurves_C2tnb359v1Holder
struct  C2tnb359v1Holder_t8B93223E821CADB44BDBCE85BCD4077EE4888861  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct C2tnb359v1Holder_t8B93223E821CADB44BDBCE85BCD4077EE4888861_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Asn1.X9.X962NamedCurves_C2tnb359v1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(C2tnb359v1Holder_t8B93223E821CADB44BDBCE85BCD4077EE4888861_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // C2TNB359V1HOLDER_T8B93223E821CADB44BDBCE85BCD4077EE4888861_H
#ifndef C2TNB431R1HOLDER_T6504DEEFE40EDE731560CC455FE297CB7A4FAFAC_H
#define C2TNB431R1HOLDER_T6504DEEFE40EDE731560CC455FE297CB7A4FAFAC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.X9.X962NamedCurves_C2tnb431r1Holder
struct  C2tnb431r1Holder_t6504DEEFE40EDE731560CC455FE297CB7A4FAFAC  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct C2tnb431r1Holder_t6504DEEFE40EDE731560CC455FE297CB7A4FAFAC_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Asn1.X9.X962NamedCurves_C2tnb431r1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(C2tnb431r1Holder_t6504DEEFE40EDE731560CC455FE297CB7A4FAFAC_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // C2TNB431R1HOLDER_T6504DEEFE40EDE731560CC455FE297CB7A4FAFAC_H
#ifndef X962PARAMETERS_T31C99000F1443EF4D6AC7977CD4F676FAB2AD4E0_H
#define X962PARAMETERS_T31C99000F1443EF4D6AC7977CD4F676FAB2AD4E0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.X9.X962Parameters
struct  X962Parameters_t31C99000F1443EF4D6AC7977CD4F676FAB2AD4E0  : public Asn1Encodable_t390FCE82F5F52E41A7320BBF2E8BF18DA1622A8B
{
public:
	// Org.BouncyCastle.Asn1.Asn1Object Org.BouncyCastle.Asn1.X9.X962Parameters::_params
	Asn1Object_tB780E50883209D5C975E83B3D16257DFC3CCE36D * ____params_0;

public:
	inline static int32_t get_offset_of__params_0() { return static_cast<int32_t>(offsetof(X962Parameters_t31C99000F1443EF4D6AC7977CD4F676FAB2AD4E0, ____params_0)); }
	inline Asn1Object_tB780E50883209D5C975E83B3D16257DFC3CCE36D * get__params_0() const { return ____params_0; }
	inline Asn1Object_tB780E50883209D5C975E83B3D16257DFC3CCE36D ** get_address_of__params_0() { return &____params_0; }
	inline void set__params_0(Asn1Object_tB780E50883209D5C975E83B3D16257DFC3CCE36D * value)
	{
		____params_0 = value;
		Il2CppCodeGenWriteBarrier((&____params_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X962PARAMETERS_T31C99000F1443EF4D6AC7977CD4F676FAB2AD4E0_H
#ifndef X9CURVE_T6E9B5BFAE7A0958585B3B6112DAE30405F1EFCD8_H
#define X9CURVE_T6E9B5BFAE7A0958585B3B6112DAE30405F1EFCD8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.X9.X9Curve
struct  X9Curve_t6E9B5BFAE7A0958585B3B6112DAE30405F1EFCD8  : public Asn1Encodable_t390FCE82F5F52E41A7320BBF2E8BF18DA1622A8B
{
public:
	// Org.BouncyCastle.Math.EC.ECCurve Org.BouncyCastle.Asn1.X9.X9Curve::curve
	ECCurve_t08AD8E722914B0B03C49C2D27B79B1D4E4770D95 * ___curve_0;
	// System.Byte[] Org.BouncyCastle.Asn1.X9.X9Curve::seed
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___seed_1;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X9.X9Curve::fieldIdentifier
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___fieldIdentifier_2;

public:
	inline static int32_t get_offset_of_curve_0() { return static_cast<int32_t>(offsetof(X9Curve_t6E9B5BFAE7A0958585B3B6112DAE30405F1EFCD8, ___curve_0)); }
	inline ECCurve_t08AD8E722914B0B03C49C2D27B79B1D4E4770D95 * get_curve_0() const { return ___curve_0; }
	inline ECCurve_t08AD8E722914B0B03C49C2D27B79B1D4E4770D95 ** get_address_of_curve_0() { return &___curve_0; }
	inline void set_curve_0(ECCurve_t08AD8E722914B0B03C49C2D27B79B1D4E4770D95 * value)
	{
		___curve_0 = value;
		Il2CppCodeGenWriteBarrier((&___curve_0), value);
	}

	inline static int32_t get_offset_of_seed_1() { return static_cast<int32_t>(offsetof(X9Curve_t6E9B5BFAE7A0958585B3B6112DAE30405F1EFCD8, ___seed_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_seed_1() const { return ___seed_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_seed_1() { return &___seed_1; }
	inline void set_seed_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___seed_1 = value;
		Il2CppCodeGenWriteBarrier((&___seed_1), value);
	}

	inline static int32_t get_offset_of_fieldIdentifier_2() { return static_cast<int32_t>(offsetof(X9Curve_t6E9B5BFAE7A0958585B3B6112DAE30405F1EFCD8, ___fieldIdentifier_2)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_fieldIdentifier_2() const { return ___fieldIdentifier_2; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_fieldIdentifier_2() { return &___fieldIdentifier_2; }
	inline void set_fieldIdentifier_2(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___fieldIdentifier_2 = value;
		Il2CppCodeGenWriteBarrier((&___fieldIdentifier_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X9CURVE_T6E9B5BFAE7A0958585B3B6112DAE30405F1EFCD8_H
#ifndef X9ECPARAMETERS_T818A7DD4D13D91F0D36622C6C117D7E9A10E51C3_H
#define X9ECPARAMETERS_T818A7DD4D13D91F0D36622C6C117D7E9A10E51C3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.X9.X9ECParameters
struct  X9ECParameters_t818A7DD4D13D91F0D36622C6C117D7E9A10E51C3  : public Asn1Encodable_t390FCE82F5F52E41A7320BBF2E8BF18DA1622A8B
{
public:
	// Org.BouncyCastle.Asn1.X9.X9FieldID Org.BouncyCastle.Asn1.X9.X9ECParameters::fieldID
	X9FieldID_t8A4FF1A99F5C9431C28964FA64FD3032C9A64019 * ___fieldID_0;
	// Org.BouncyCastle.Math.EC.ECCurve Org.BouncyCastle.Asn1.X9.X9ECParameters::curve
	ECCurve_t08AD8E722914B0B03C49C2D27B79B1D4E4770D95 * ___curve_1;
	// Org.BouncyCastle.Asn1.X9.X9ECPoint Org.BouncyCastle.Asn1.X9.X9ECParameters::g
	X9ECPoint_t0FFE1DD3974DC6DE1C0428FDD84A3FF10D29F65A * ___g_2;
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Asn1.X9.X9ECParameters::n
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___n_3;
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Asn1.X9.X9ECParameters::h
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___h_4;
	// System.Byte[] Org.BouncyCastle.Asn1.X9.X9ECParameters::seed
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___seed_5;

public:
	inline static int32_t get_offset_of_fieldID_0() { return static_cast<int32_t>(offsetof(X9ECParameters_t818A7DD4D13D91F0D36622C6C117D7E9A10E51C3, ___fieldID_0)); }
	inline X9FieldID_t8A4FF1A99F5C9431C28964FA64FD3032C9A64019 * get_fieldID_0() const { return ___fieldID_0; }
	inline X9FieldID_t8A4FF1A99F5C9431C28964FA64FD3032C9A64019 ** get_address_of_fieldID_0() { return &___fieldID_0; }
	inline void set_fieldID_0(X9FieldID_t8A4FF1A99F5C9431C28964FA64FD3032C9A64019 * value)
	{
		___fieldID_0 = value;
		Il2CppCodeGenWriteBarrier((&___fieldID_0), value);
	}

	inline static int32_t get_offset_of_curve_1() { return static_cast<int32_t>(offsetof(X9ECParameters_t818A7DD4D13D91F0D36622C6C117D7E9A10E51C3, ___curve_1)); }
	inline ECCurve_t08AD8E722914B0B03C49C2D27B79B1D4E4770D95 * get_curve_1() const { return ___curve_1; }
	inline ECCurve_t08AD8E722914B0B03C49C2D27B79B1D4E4770D95 ** get_address_of_curve_1() { return &___curve_1; }
	inline void set_curve_1(ECCurve_t08AD8E722914B0B03C49C2D27B79B1D4E4770D95 * value)
	{
		___curve_1 = value;
		Il2CppCodeGenWriteBarrier((&___curve_1), value);
	}

	inline static int32_t get_offset_of_g_2() { return static_cast<int32_t>(offsetof(X9ECParameters_t818A7DD4D13D91F0D36622C6C117D7E9A10E51C3, ___g_2)); }
	inline X9ECPoint_t0FFE1DD3974DC6DE1C0428FDD84A3FF10D29F65A * get_g_2() const { return ___g_2; }
	inline X9ECPoint_t0FFE1DD3974DC6DE1C0428FDD84A3FF10D29F65A ** get_address_of_g_2() { return &___g_2; }
	inline void set_g_2(X9ECPoint_t0FFE1DD3974DC6DE1C0428FDD84A3FF10D29F65A * value)
	{
		___g_2 = value;
		Il2CppCodeGenWriteBarrier((&___g_2), value);
	}

	inline static int32_t get_offset_of_n_3() { return static_cast<int32_t>(offsetof(X9ECParameters_t818A7DD4D13D91F0D36622C6C117D7E9A10E51C3, ___n_3)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_n_3() const { return ___n_3; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_n_3() { return &___n_3; }
	inline void set_n_3(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___n_3 = value;
		Il2CppCodeGenWriteBarrier((&___n_3), value);
	}

	inline static int32_t get_offset_of_h_4() { return static_cast<int32_t>(offsetof(X9ECParameters_t818A7DD4D13D91F0D36622C6C117D7E9A10E51C3, ___h_4)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_h_4() const { return ___h_4; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_h_4() { return &___h_4; }
	inline void set_h_4(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___h_4 = value;
		Il2CppCodeGenWriteBarrier((&___h_4), value);
	}

	inline static int32_t get_offset_of_seed_5() { return static_cast<int32_t>(offsetof(X9ECParameters_t818A7DD4D13D91F0D36622C6C117D7E9A10E51C3, ___seed_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_seed_5() const { return ___seed_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_seed_5() { return &___seed_5; }
	inline void set_seed_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___seed_5 = value;
		Il2CppCodeGenWriteBarrier((&___seed_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X9ECPARAMETERS_T818A7DD4D13D91F0D36622C6C117D7E9A10E51C3_H
#ifndef X9ECPOINT_T0FFE1DD3974DC6DE1C0428FDD84A3FF10D29F65A_H
#define X9ECPOINT_T0FFE1DD3974DC6DE1C0428FDD84A3FF10D29F65A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.X9.X9ECPoint
struct  X9ECPoint_t0FFE1DD3974DC6DE1C0428FDD84A3FF10D29F65A  : public Asn1Encodable_t390FCE82F5F52E41A7320BBF2E8BF18DA1622A8B
{
public:
	// Org.BouncyCastle.Asn1.Asn1OctetString Org.BouncyCastle.Asn1.X9.X9ECPoint::encoding
	Asn1OctetString_t2088F9E88E4C97BB9CF3B2FFC53CFBAF9B8B79CB * ___encoding_0;
	// Org.BouncyCastle.Math.EC.ECCurve Org.BouncyCastle.Asn1.X9.X9ECPoint::c
	ECCurve_t08AD8E722914B0B03C49C2D27B79B1D4E4770D95 * ___c_1;
	// Org.BouncyCastle.Math.EC.ECPoint Org.BouncyCastle.Asn1.X9.X9ECPoint::p
	ECPoint_tCC0C4447DEFF70FC9D68F5149ECCCC0533CE4399 * ___p_2;

public:
	inline static int32_t get_offset_of_encoding_0() { return static_cast<int32_t>(offsetof(X9ECPoint_t0FFE1DD3974DC6DE1C0428FDD84A3FF10D29F65A, ___encoding_0)); }
	inline Asn1OctetString_t2088F9E88E4C97BB9CF3B2FFC53CFBAF9B8B79CB * get_encoding_0() const { return ___encoding_0; }
	inline Asn1OctetString_t2088F9E88E4C97BB9CF3B2FFC53CFBAF9B8B79CB ** get_address_of_encoding_0() { return &___encoding_0; }
	inline void set_encoding_0(Asn1OctetString_t2088F9E88E4C97BB9CF3B2FFC53CFBAF9B8B79CB * value)
	{
		___encoding_0 = value;
		Il2CppCodeGenWriteBarrier((&___encoding_0), value);
	}

	inline static int32_t get_offset_of_c_1() { return static_cast<int32_t>(offsetof(X9ECPoint_t0FFE1DD3974DC6DE1C0428FDD84A3FF10D29F65A, ___c_1)); }
	inline ECCurve_t08AD8E722914B0B03C49C2D27B79B1D4E4770D95 * get_c_1() const { return ___c_1; }
	inline ECCurve_t08AD8E722914B0B03C49C2D27B79B1D4E4770D95 ** get_address_of_c_1() { return &___c_1; }
	inline void set_c_1(ECCurve_t08AD8E722914B0B03C49C2D27B79B1D4E4770D95 * value)
	{
		___c_1 = value;
		Il2CppCodeGenWriteBarrier((&___c_1), value);
	}

	inline static int32_t get_offset_of_p_2() { return static_cast<int32_t>(offsetof(X9ECPoint_t0FFE1DD3974DC6DE1C0428FDD84A3FF10D29F65A, ___p_2)); }
	inline ECPoint_tCC0C4447DEFF70FC9D68F5149ECCCC0533CE4399 * get_p_2() const { return ___p_2; }
	inline ECPoint_tCC0C4447DEFF70FC9D68F5149ECCCC0533CE4399 ** get_address_of_p_2() { return &___p_2; }
	inline void set_p_2(ECPoint_tCC0C4447DEFF70FC9D68F5149ECCCC0533CE4399 * value)
	{
		___p_2 = value;
		Il2CppCodeGenWriteBarrier((&___p_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X9ECPOINT_T0FFE1DD3974DC6DE1C0428FDD84A3FF10D29F65A_H
#ifndef X9FIELDELEMENT_T90E143BC3E2319C2E326CA02BC3B0EBC8807660E_H
#define X9FIELDELEMENT_T90E143BC3E2319C2E326CA02BC3B0EBC8807660E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.X9.X9FieldElement
struct  X9FieldElement_t90E143BC3E2319C2E326CA02BC3B0EBC8807660E  : public Asn1Encodable_t390FCE82F5F52E41A7320BBF2E8BF18DA1622A8B
{
public:
	// Org.BouncyCastle.Math.EC.ECFieldElement Org.BouncyCastle.Asn1.X9.X9FieldElement::f
	ECFieldElement_tE2FF8892A60D0ACBC3FB665E41B0DEAA0308514C * ___f_0;

public:
	inline static int32_t get_offset_of_f_0() { return static_cast<int32_t>(offsetof(X9FieldElement_t90E143BC3E2319C2E326CA02BC3B0EBC8807660E, ___f_0)); }
	inline ECFieldElement_tE2FF8892A60D0ACBC3FB665E41B0DEAA0308514C * get_f_0() const { return ___f_0; }
	inline ECFieldElement_tE2FF8892A60D0ACBC3FB665E41B0DEAA0308514C ** get_address_of_f_0() { return &___f_0; }
	inline void set_f_0(ECFieldElement_tE2FF8892A60D0ACBC3FB665E41B0DEAA0308514C * value)
	{
		___f_0 = value;
		Il2CppCodeGenWriteBarrier((&___f_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X9FIELDELEMENT_T90E143BC3E2319C2E326CA02BC3B0EBC8807660E_H
#ifndef X9FIELDID_T8A4FF1A99F5C9431C28964FA64FD3032C9A64019_H
#define X9FIELDID_T8A4FF1A99F5C9431C28964FA64FD3032C9A64019_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.X9.X9FieldID
struct  X9FieldID_t8A4FF1A99F5C9431C28964FA64FD3032C9A64019  : public Asn1Encodable_t390FCE82F5F52E41A7320BBF2E8BF18DA1622A8B
{
public:
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.X9.X9FieldID::id
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___id_0;
	// Org.BouncyCastle.Asn1.Asn1Object Org.BouncyCastle.Asn1.X9.X9FieldID::parameters
	Asn1Object_tB780E50883209D5C975E83B3D16257DFC3CCE36D * ___parameters_1;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(X9FieldID_t8A4FF1A99F5C9431C28964FA64FD3032C9A64019, ___id_0)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_id_0() const { return ___id_0; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier((&___id_0), value);
	}

	inline static int32_t get_offset_of_parameters_1() { return static_cast<int32_t>(offsetof(X9FieldID_t8A4FF1A99F5C9431C28964FA64FD3032C9A64019, ___parameters_1)); }
	inline Asn1Object_tB780E50883209D5C975E83B3D16257DFC3CCE36D * get_parameters_1() const { return ___parameters_1; }
	inline Asn1Object_tB780E50883209D5C975E83B3D16257DFC3CCE36D ** get_address_of_parameters_1() { return &___parameters_1; }
	inline void set_parameters_1(Asn1Object_tB780E50883209D5C975E83B3D16257DFC3CCE36D * value)
	{
		___parameters_1 = value;
		Il2CppCodeGenWriteBarrier((&___parameters_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X9FIELDID_T8A4FF1A99F5C9431C28964FA64FD3032C9A64019_H
#ifndef CRYPTOEXCEPTION_TD693C94FBD529A1E01A8DFE8D03A039EA27CFB8C_H
#define CRYPTOEXCEPTION_TD693C94FBD529A1E01A8DFE8D03A039EA27CFB8C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.CryptoException
struct  CryptoException_tD693C94FBD529A1E01A8DFE8D03A039EA27CFB8C  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRYPTOEXCEPTION_TD693C94FBD529A1E01A8DFE8D03A039EA27CFB8C_H
#ifndef MD4DIGEST_TF9D2B79B358121F811BC66B2BED64550FE6F3A29_H
#define MD4DIGEST_TF9D2B79B358121F811BC66B2BED64550FE6F3A29_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Digests.MD4Digest
struct  MD4Digest_tF9D2B79B358121F811BC66B2BED64550FE6F3A29  : public GeneralDigest_t3B10A5BF867429F885F41CE318BA1665427D50D6
{
public:
	// System.Int32 Org.BouncyCastle.Crypto.Digests.MD4Digest::H1
	int32_t ___H1_3;
	// System.Int32 Org.BouncyCastle.Crypto.Digests.MD4Digest::H2
	int32_t ___H2_4;
	// System.Int32 Org.BouncyCastle.Crypto.Digests.MD4Digest::H3
	int32_t ___H3_5;
	// System.Int32 Org.BouncyCastle.Crypto.Digests.MD4Digest::H4
	int32_t ___H4_6;
	// System.Int32[] Org.BouncyCastle.Crypto.Digests.MD4Digest::X
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___X_7;
	// System.Int32 Org.BouncyCastle.Crypto.Digests.MD4Digest::xOff
	int32_t ___xOff_8;

public:
	inline static int32_t get_offset_of_H1_3() { return static_cast<int32_t>(offsetof(MD4Digest_tF9D2B79B358121F811BC66B2BED64550FE6F3A29, ___H1_3)); }
	inline int32_t get_H1_3() const { return ___H1_3; }
	inline int32_t* get_address_of_H1_3() { return &___H1_3; }
	inline void set_H1_3(int32_t value)
	{
		___H1_3 = value;
	}

	inline static int32_t get_offset_of_H2_4() { return static_cast<int32_t>(offsetof(MD4Digest_tF9D2B79B358121F811BC66B2BED64550FE6F3A29, ___H2_4)); }
	inline int32_t get_H2_4() const { return ___H2_4; }
	inline int32_t* get_address_of_H2_4() { return &___H2_4; }
	inline void set_H2_4(int32_t value)
	{
		___H2_4 = value;
	}

	inline static int32_t get_offset_of_H3_5() { return static_cast<int32_t>(offsetof(MD4Digest_tF9D2B79B358121F811BC66B2BED64550FE6F3A29, ___H3_5)); }
	inline int32_t get_H3_5() const { return ___H3_5; }
	inline int32_t* get_address_of_H3_5() { return &___H3_5; }
	inline void set_H3_5(int32_t value)
	{
		___H3_5 = value;
	}

	inline static int32_t get_offset_of_H4_6() { return static_cast<int32_t>(offsetof(MD4Digest_tF9D2B79B358121F811BC66B2BED64550FE6F3A29, ___H4_6)); }
	inline int32_t get_H4_6() const { return ___H4_6; }
	inline int32_t* get_address_of_H4_6() { return &___H4_6; }
	inline void set_H4_6(int32_t value)
	{
		___H4_6 = value;
	}

	inline static int32_t get_offset_of_X_7() { return static_cast<int32_t>(offsetof(MD4Digest_tF9D2B79B358121F811BC66B2BED64550FE6F3A29, ___X_7)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_X_7() const { return ___X_7; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_X_7() { return &___X_7; }
	inline void set_X_7(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___X_7 = value;
		Il2CppCodeGenWriteBarrier((&___X_7), value);
	}

	inline static int32_t get_offset_of_xOff_8() { return static_cast<int32_t>(offsetof(MD4Digest_tF9D2B79B358121F811BC66B2BED64550FE6F3A29, ___xOff_8)); }
	inline int32_t get_xOff_8() const { return ___xOff_8; }
	inline int32_t* get_address_of_xOff_8() { return &___xOff_8; }
	inline void set_xOff_8(int32_t value)
	{
		___xOff_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MD4DIGEST_TF9D2B79B358121F811BC66B2BED64550FE6F3A29_H
#ifndef MD5DIGEST_T647344D0C318D22F61BD117B16ED64CA6E458FBA_H
#define MD5DIGEST_T647344D0C318D22F61BD117B16ED64CA6E458FBA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Digests.MD5Digest
struct  MD5Digest_t647344D0C318D22F61BD117B16ED64CA6E458FBA  : public GeneralDigest_t3B10A5BF867429F885F41CE318BA1665427D50D6
{
public:
	// System.UInt32 Org.BouncyCastle.Crypto.Digests.MD5Digest::H1
	uint32_t ___H1_3;
	// System.UInt32 Org.BouncyCastle.Crypto.Digests.MD5Digest::H2
	uint32_t ___H2_4;
	// System.UInt32 Org.BouncyCastle.Crypto.Digests.MD5Digest::H3
	uint32_t ___H3_5;
	// System.UInt32 Org.BouncyCastle.Crypto.Digests.MD5Digest::H4
	uint32_t ___H4_6;
	// System.UInt32[] Org.BouncyCastle.Crypto.Digests.MD5Digest::X
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___X_7;
	// System.Int32 Org.BouncyCastle.Crypto.Digests.MD5Digest::xOff
	int32_t ___xOff_8;

public:
	inline static int32_t get_offset_of_H1_3() { return static_cast<int32_t>(offsetof(MD5Digest_t647344D0C318D22F61BD117B16ED64CA6E458FBA, ___H1_3)); }
	inline uint32_t get_H1_3() const { return ___H1_3; }
	inline uint32_t* get_address_of_H1_3() { return &___H1_3; }
	inline void set_H1_3(uint32_t value)
	{
		___H1_3 = value;
	}

	inline static int32_t get_offset_of_H2_4() { return static_cast<int32_t>(offsetof(MD5Digest_t647344D0C318D22F61BD117B16ED64CA6E458FBA, ___H2_4)); }
	inline uint32_t get_H2_4() const { return ___H2_4; }
	inline uint32_t* get_address_of_H2_4() { return &___H2_4; }
	inline void set_H2_4(uint32_t value)
	{
		___H2_4 = value;
	}

	inline static int32_t get_offset_of_H3_5() { return static_cast<int32_t>(offsetof(MD5Digest_t647344D0C318D22F61BD117B16ED64CA6E458FBA, ___H3_5)); }
	inline uint32_t get_H3_5() const { return ___H3_5; }
	inline uint32_t* get_address_of_H3_5() { return &___H3_5; }
	inline void set_H3_5(uint32_t value)
	{
		___H3_5 = value;
	}

	inline static int32_t get_offset_of_H4_6() { return static_cast<int32_t>(offsetof(MD5Digest_t647344D0C318D22F61BD117B16ED64CA6E458FBA, ___H4_6)); }
	inline uint32_t get_H4_6() const { return ___H4_6; }
	inline uint32_t* get_address_of_H4_6() { return &___H4_6; }
	inline void set_H4_6(uint32_t value)
	{
		___H4_6 = value;
	}

	inline static int32_t get_offset_of_X_7() { return static_cast<int32_t>(offsetof(MD5Digest_t647344D0C318D22F61BD117B16ED64CA6E458FBA, ___X_7)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_X_7() const { return ___X_7; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_X_7() { return &___X_7; }
	inline void set_X_7(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___X_7 = value;
		Il2CppCodeGenWriteBarrier((&___X_7), value);
	}

	inline static int32_t get_offset_of_xOff_8() { return static_cast<int32_t>(offsetof(MD5Digest_t647344D0C318D22F61BD117B16ED64CA6E458FBA, ___xOff_8)); }
	inline int32_t get_xOff_8() const { return ___xOff_8; }
	inline int32_t* get_address_of_xOff_8() { return &___xOff_8; }
	inline void set_xOff_8(int32_t value)
	{
		___xOff_8 = value;
	}
};

struct MD5Digest_t647344D0C318D22F61BD117B16ED64CA6E458FBA_StaticFields
{
public:
	// System.Int32 Org.BouncyCastle.Crypto.Digests.MD5Digest::S11
	int32_t ___S11_9;
	// System.Int32 Org.BouncyCastle.Crypto.Digests.MD5Digest::S12
	int32_t ___S12_10;
	// System.Int32 Org.BouncyCastle.Crypto.Digests.MD5Digest::S13
	int32_t ___S13_11;
	// System.Int32 Org.BouncyCastle.Crypto.Digests.MD5Digest::S14
	int32_t ___S14_12;
	// System.Int32 Org.BouncyCastle.Crypto.Digests.MD5Digest::S21
	int32_t ___S21_13;
	// System.Int32 Org.BouncyCastle.Crypto.Digests.MD5Digest::S22
	int32_t ___S22_14;
	// System.Int32 Org.BouncyCastle.Crypto.Digests.MD5Digest::S23
	int32_t ___S23_15;
	// System.Int32 Org.BouncyCastle.Crypto.Digests.MD5Digest::S24
	int32_t ___S24_16;
	// System.Int32 Org.BouncyCastle.Crypto.Digests.MD5Digest::S31
	int32_t ___S31_17;
	// System.Int32 Org.BouncyCastle.Crypto.Digests.MD5Digest::S32
	int32_t ___S32_18;
	// System.Int32 Org.BouncyCastle.Crypto.Digests.MD5Digest::S33
	int32_t ___S33_19;
	// System.Int32 Org.BouncyCastle.Crypto.Digests.MD5Digest::S34
	int32_t ___S34_20;
	// System.Int32 Org.BouncyCastle.Crypto.Digests.MD5Digest::S41
	int32_t ___S41_21;
	// System.Int32 Org.BouncyCastle.Crypto.Digests.MD5Digest::S42
	int32_t ___S42_22;
	// System.Int32 Org.BouncyCastle.Crypto.Digests.MD5Digest::S43
	int32_t ___S43_23;
	// System.Int32 Org.BouncyCastle.Crypto.Digests.MD5Digest::S44
	int32_t ___S44_24;

public:
	inline static int32_t get_offset_of_S11_9() { return static_cast<int32_t>(offsetof(MD5Digest_t647344D0C318D22F61BD117B16ED64CA6E458FBA_StaticFields, ___S11_9)); }
	inline int32_t get_S11_9() const { return ___S11_9; }
	inline int32_t* get_address_of_S11_9() { return &___S11_9; }
	inline void set_S11_9(int32_t value)
	{
		___S11_9 = value;
	}

	inline static int32_t get_offset_of_S12_10() { return static_cast<int32_t>(offsetof(MD5Digest_t647344D0C318D22F61BD117B16ED64CA6E458FBA_StaticFields, ___S12_10)); }
	inline int32_t get_S12_10() const { return ___S12_10; }
	inline int32_t* get_address_of_S12_10() { return &___S12_10; }
	inline void set_S12_10(int32_t value)
	{
		___S12_10 = value;
	}

	inline static int32_t get_offset_of_S13_11() { return static_cast<int32_t>(offsetof(MD5Digest_t647344D0C318D22F61BD117B16ED64CA6E458FBA_StaticFields, ___S13_11)); }
	inline int32_t get_S13_11() const { return ___S13_11; }
	inline int32_t* get_address_of_S13_11() { return &___S13_11; }
	inline void set_S13_11(int32_t value)
	{
		___S13_11 = value;
	}

	inline static int32_t get_offset_of_S14_12() { return static_cast<int32_t>(offsetof(MD5Digest_t647344D0C318D22F61BD117B16ED64CA6E458FBA_StaticFields, ___S14_12)); }
	inline int32_t get_S14_12() const { return ___S14_12; }
	inline int32_t* get_address_of_S14_12() { return &___S14_12; }
	inline void set_S14_12(int32_t value)
	{
		___S14_12 = value;
	}

	inline static int32_t get_offset_of_S21_13() { return static_cast<int32_t>(offsetof(MD5Digest_t647344D0C318D22F61BD117B16ED64CA6E458FBA_StaticFields, ___S21_13)); }
	inline int32_t get_S21_13() const { return ___S21_13; }
	inline int32_t* get_address_of_S21_13() { return &___S21_13; }
	inline void set_S21_13(int32_t value)
	{
		___S21_13 = value;
	}

	inline static int32_t get_offset_of_S22_14() { return static_cast<int32_t>(offsetof(MD5Digest_t647344D0C318D22F61BD117B16ED64CA6E458FBA_StaticFields, ___S22_14)); }
	inline int32_t get_S22_14() const { return ___S22_14; }
	inline int32_t* get_address_of_S22_14() { return &___S22_14; }
	inline void set_S22_14(int32_t value)
	{
		___S22_14 = value;
	}

	inline static int32_t get_offset_of_S23_15() { return static_cast<int32_t>(offsetof(MD5Digest_t647344D0C318D22F61BD117B16ED64CA6E458FBA_StaticFields, ___S23_15)); }
	inline int32_t get_S23_15() const { return ___S23_15; }
	inline int32_t* get_address_of_S23_15() { return &___S23_15; }
	inline void set_S23_15(int32_t value)
	{
		___S23_15 = value;
	}

	inline static int32_t get_offset_of_S24_16() { return static_cast<int32_t>(offsetof(MD5Digest_t647344D0C318D22F61BD117B16ED64CA6E458FBA_StaticFields, ___S24_16)); }
	inline int32_t get_S24_16() const { return ___S24_16; }
	inline int32_t* get_address_of_S24_16() { return &___S24_16; }
	inline void set_S24_16(int32_t value)
	{
		___S24_16 = value;
	}

	inline static int32_t get_offset_of_S31_17() { return static_cast<int32_t>(offsetof(MD5Digest_t647344D0C318D22F61BD117B16ED64CA6E458FBA_StaticFields, ___S31_17)); }
	inline int32_t get_S31_17() const { return ___S31_17; }
	inline int32_t* get_address_of_S31_17() { return &___S31_17; }
	inline void set_S31_17(int32_t value)
	{
		___S31_17 = value;
	}

	inline static int32_t get_offset_of_S32_18() { return static_cast<int32_t>(offsetof(MD5Digest_t647344D0C318D22F61BD117B16ED64CA6E458FBA_StaticFields, ___S32_18)); }
	inline int32_t get_S32_18() const { return ___S32_18; }
	inline int32_t* get_address_of_S32_18() { return &___S32_18; }
	inline void set_S32_18(int32_t value)
	{
		___S32_18 = value;
	}

	inline static int32_t get_offset_of_S33_19() { return static_cast<int32_t>(offsetof(MD5Digest_t647344D0C318D22F61BD117B16ED64CA6E458FBA_StaticFields, ___S33_19)); }
	inline int32_t get_S33_19() const { return ___S33_19; }
	inline int32_t* get_address_of_S33_19() { return &___S33_19; }
	inline void set_S33_19(int32_t value)
	{
		___S33_19 = value;
	}

	inline static int32_t get_offset_of_S34_20() { return static_cast<int32_t>(offsetof(MD5Digest_t647344D0C318D22F61BD117B16ED64CA6E458FBA_StaticFields, ___S34_20)); }
	inline int32_t get_S34_20() const { return ___S34_20; }
	inline int32_t* get_address_of_S34_20() { return &___S34_20; }
	inline void set_S34_20(int32_t value)
	{
		___S34_20 = value;
	}

	inline static int32_t get_offset_of_S41_21() { return static_cast<int32_t>(offsetof(MD5Digest_t647344D0C318D22F61BD117B16ED64CA6E458FBA_StaticFields, ___S41_21)); }
	inline int32_t get_S41_21() const { return ___S41_21; }
	inline int32_t* get_address_of_S41_21() { return &___S41_21; }
	inline void set_S41_21(int32_t value)
	{
		___S41_21 = value;
	}

	inline static int32_t get_offset_of_S42_22() { return static_cast<int32_t>(offsetof(MD5Digest_t647344D0C318D22F61BD117B16ED64CA6E458FBA_StaticFields, ___S42_22)); }
	inline int32_t get_S42_22() const { return ___S42_22; }
	inline int32_t* get_address_of_S42_22() { return &___S42_22; }
	inline void set_S42_22(int32_t value)
	{
		___S42_22 = value;
	}

	inline static int32_t get_offset_of_S43_23() { return static_cast<int32_t>(offsetof(MD5Digest_t647344D0C318D22F61BD117B16ED64CA6E458FBA_StaticFields, ___S43_23)); }
	inline int32_t get_S43_23() const { return ___S43_23; }
	inline int32_t* get_address_of_S43_23() { return &___S43_23; }
	inline void set_S43_23(int32_t value)
	{
		___S43_23 = value;
	}

	inline static int32_t get_offset_of_S44_24() { return static_cast<int32_t>(offsetof(MD5Digest_t647344D0C318D22F61BD117B16ED64CA6E458FBA_StaticFields, ___S44_24)); }
	inline int32_t get_S44_24() const { return ___S44_24; }
	inline int32_t* get_address_of_S44_24() { return &___S44_24; }
	inline void set_S44_24(int32_t value)
	{
		___S44_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MD5DIGEST_T647344D0C318D22F61BD117B16ED64CA6E458FBA_H
#ifndef RIPEMD128DIGEST_T641DBF9D8CB0F3E0FCD072972F4FA6F9EA6FEA71_H
#define RIPEMD128DIGEST_T641DBF9D8CB0F3E0FCD072972F4FA6F9EA6FEA71_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Digests.RipeMD128Digest
struct  RipeMD128Digest_t641DBF9D8CB0F3E0FCD072972F4FA6F9EA6FEA71  : public GeneralDigest_t3B10A5BF867429F885F41CE318BA1665427D50D6
{
public:
	// System.Int32 Org.BouncyCastle.Crypto.Digests.RipeMD128Digest::H0
	int32_t ___H0_3;
	// System.Int32 Org.BouncyCastle.Crypto.Digests.RipeMD128Digest::H1
	int32_t ___H1_4;
	// System.Int32 Org.BouncyCastle.Crypto.Digests.RipeMD128Digest::H2
	int32_t ___H2_5;
	// System.Int32 Org.BouncyCastle.Crypto.Digests.RipeMD128Digest::H3
	int32_t ___H3_6;
	// System.Int32[] Org.BouncyCastle.Crypto.Digests.RipeMD128Digest::X
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___X_7;
	// System.Int32 Org.BouncyCastle.Crypto.Digests.RipeMD128Digest::xOff
	int32_t ___xOff_8;

public:
	inline static int32_t get_offset_of_H0_3() { return static_cast<int32_t>(offsetof(RipeMD128Digest_t641DBF9D8CB0F3E0FCD072972F4FA6F9EA6FEA71, ___H0_3)); }
	inline int32_t get_H0_3() const { return ___H0_3; }
	inline int32_t* get_address_of_H0_3() { return &___H0_3; }
	inline void set_H0_3(int32_t value)
	{
		___H0_3 = value;
	}

	inline static int32_t get_offset_of_H1_4() { return static_cast<int32_t>(offsetof(RipeMD128Digest_t641DBF9D8CB0F3E0FCD072972F4FA6F9EA6FEA71, ___H1_4)); }
	inline int32_t get_H1_4() const { return ___H1_4; }
	inline int32_t* get_address_of_H1_4() { return &___H1_4; }
	inline void set_H1_4(int32_t value)
	{
		___H1_4 = value;
	}

	inline static int32_t get_offset_of_H2_5() { return static_cast<int32_t>(offsetof(RipeMD128Digest_t641DBF9D8CB0F3E0FCD072972F4FA6F9EA6FEA71, ___H2_5)); }
	inline int32_t get_H2_5() const { return ___H2_5; }
	inline int32_t* get_address_of_H2_5() { return &___H2_5; }
	inline void set_H2_5(int32_t value)
	{
		___H2_5 = value;
	}

	inline static int32_t get_offset_of_H3_6() { return static_cast<int32_t>(offsetof(RipeMD128Digest_t641DBF9D8CB0F3E0FCD072972F4FA6F9EA6FEA71, ___H3_6)); }
	inline int32_t get_H3_6() const { return ___H3_6; }
	inline int32_t* get_address_of_H3_6() { return &___H3_6; }
	inline void set_H3_6(int32_t value)
	{
		___H3_6 = value;
	}

	inline static int32_t get_offset_of_X_7() { return static_cast<int32_t>(offsetof(RipeMD128Digest_t641DBF9D8CB0F3E0FCD072972F4FA6F9EA6FEA71, ___X_7)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_X_7() const { return ___X_7; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_X_7() { return &___X_7; }
	inline void set_X_7(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___X_7 = value;
		Il2CppCodeGenWriteBarrier((&___X_7), value);
	}

	inline static int32_t get_offset_of_xOff_8() { return static_cast<int32_t>(offsetof(RipeMD128Digest_t641DBF9D8CB0F3E0FCD072972F4FA6F9EA6FEA71, ___xOff_8)); }
	inline int32_t get_xOff_8() const { return ___xOff_8; }
	inline int32_t* get_address_of_xOff_8() { return &___xOff_8; }
	inline void set_xOff_8(int32_t value)
	{
		___xOff_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RIPEMD128DIGEST_T641DBF9D8CB0F3E0FCD072972F4FA6F9EA6FEA71_H
#ifndef RIPEMD160DIGEST_T9812101E248DBEBA8B8ECB8DEE92F00530C0D74A_H
#define RIPEMD160DIGEST_T9812101E248DBEBA8B8ECB8DEE92F00530C0D74A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Digests.RipeMD160Digest
struct  RipeMD160Digest_t9812101E248DBEBA8B8ECB8DEE92F00530C0D74A  : public GeneralDigest_t3B10A5BF867429F885F41CE318BA1665427D50D6
{
public:
	// System.Int32 Org.BouncyCastle.Crypto.Digests.RipeMD160Digest::H0
	int32_t ___H0_3;
	// System.Int32 Org.BouncyCastle.Crypto.Digests.RipeMD160Digest::H1
	int32_t ___H1_4;
	// System.Int32 Org.BouncyCastle.Crypto.Digests.RipeMD160Digest::H2
	int32_t ___H2_5;
	// System.Int32 Org.BouncyCastle.Crypto.Digests.RipeMD160Digest::H3
	int32_t ___H3_6;
	// System.Int32 Org.BouncyCastle.Crypto.Digests.RipeMD160Digest::H4
	int32_t ___H4_7;
	// System.Int32[] Org.BouncyCastle.Crypto.Digests.RipeMD160Digest::X
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___X_8;
	// System.Int32 Org.BouncyCastle.Crypto.Digests.RipeMD160Digest::xOff
	int32_t ___xOff_9;

public:
	inline static int32_t get_offset_of_H0_3() { return static_cast<int32_t>(offsetof(RipeMD160Digest_t9812101E248DBEBA8B8ECB8DEE92F00530C0D74A, ___H0_3)); }
	inline int32_t get_H0_3() const { return ___H0_3; }
	inline int32_t* get_address_of_H0_3() { return &___H0_3; }
	inline void set_H0_3(int32_t value)
	{
		___H0_3 = value;
	}

	inline static int32_t get_offset_of_H1_4() { return static_cast<int32_t>(offsetof(RipeMD160Digest_t9812101E248DBEBA8B8ECB8DEE92F00530C0D74A, ___H1_4)); }
	inline int32_t get_H1_4() const { return ___H1_4; }
	inline int32_t* get_address_of_H1_4() { return &___H1_4; }
	inline void set_H1_4(int32_t value)
	{
		___H1_4 = value;
	}

	inline static int32_t get_offset_of_H2_5() { return static_cast<int32_t>(offsetof(RipeMD160Digest_t9812101E248DBEBA8B8ECB8DEE92F00530C0D74A, ___H2_5)); }
	inline int32_t get_H2_5() const { return ___H2_5; }
	inline int32_t* get_address_of_H2_5() { return &___H2_5; }
	inline void set_H2_5(int32_t value)
	{
		___H2_5 = value;
	}

	inline static int32_t get_offset_of_H3_6() { return static_cast<int32_t>(offsetof(RipeMD160Digest_t9812101E248DBEBA8B8ECB8DEE92F00530C0D74A, ___H3_6)); }
	inline int32_t get_H3_6() const { return ___H3_6; }
	inline int32_t* get_address_of_H3_6() { return &___H3_6; }
	inline void set_H3_6(int32_t value)
	{
		___H3_6 = value;
	}

	inline static int32_t get_offset_of_H4_7() { return static_cast<int32_t>(offsetof(RipeMD160Digest_t9812101E248DBEBA8B8ECB8DEE92F00530C0D74A, ___H4_7)); }
	inline int32_t get_H4_7() const { return ___H4_7; }
	inline int32_t* get_address_of_H4_7() { return &___H4_7; }
	inline void set_H4_7(int32_t value)
	{
		___H4_7 = value;
	}

	inline static int32_t get_offset_of_X_8() { return static_cast<int32_t>(offsetof(RipeMD160Digest_t9812101E248DBEBA8B8ECB8DEE92F00530C0D74A, ___X_8)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_X_8() const { return ___X_8; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_X_8() { return &___X_8; }
	inline void set_X_8(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___X_8 = value;
		Il2CppCodeGenWriteBarrier((&___X_8), value);
	}

	inline static int32_t get_offset_of_xOff_9() { return static_cast<int32_t>(offsetof(RipeMD160Digest_t9812101E248DBEBA8B8ECB8DEE92F00530C0D74A, ___xOff_9)); }
	inline int32_t get_xOff_9() const { return ___xOff_9; }
	inline int32_t* get_address_of_xOff_9() { return &___xOff_9; }
	inline void set_xOff_9(int32_t value)
	{
		___xOff_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RIPEMD160DIGEST_T9812101E248DBEBA8B8ECB8DEE92F00530C0D74A_H
#ifndef RIPEMD256DIGEST_T561D57139D5F60FE7160BD17668889024BA16914_H
#define RIPEMD256DIGEST_T561D57139D5F60FE7160BD17668889024BA16914_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Digests.RipeMD256Digest
struct  RipeMD256Digest_t561D57139D5F60FE7160BD17668889024BA16914  : public GeneralDigest_t3B10A5BF867429F885F41CE318BA1665427D50D6
{
public:
	// System.Int32 Org.BouncyCastle.Crypto.Digests.RipeMD256Digest::H0
	int32_t ___H0_3;
	// System.Int32 Org.BouncyCastle.Crypto.Digests.RipeMD256Digest::H1
	int32_t ___H1_4;
	// System.Int32 Org.BouncyCastle.Crypto.Digests.RipeMD256Digest::H2
	int32_t ___H2_5;
	// System.Int32 Org.BouncyCastle.Crypto.Digests.RipeMD256Digest::H3
	int32_t ___H3_6;
	// System.Int32 Org.BouncyCastle.Crypto.Digests.RipeMD256Digest::H4
	int32_t ___H4_7;
	// System.Int32 Org.BouncyCastle.Crypto.Digests.RipeMD256Digest::H5
	int32_t ___H5_8;
	// System.Int32 Org.BouncyCastle.Crypto.Digests.RipeMD256Digest::H6
	int32_t ___H6_9;
	// System.Int32 Org.BouncyCastle.Crypto.Digests.RipeMD256Digest::H7
	int32_t ___H7_10;
	// System.Int32[] Org.BouncyCastle.Crypto.Digests.RipeMD256Digest::X
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___X_11;
	// System.Int32 Org.BouncyCastle.Crypto.Digests.RipeMD256Digest::xOff
	int32_t ___xOff_12;

public:
	inline static int32_t get_offset_of_H0_3() { return static_cast<int32_t>(offsetof(RipeMD256Digest_t561D57139D5F60FE7160BD17668889024BA16914, ___H0_3)); }
	inline int32_t get_H0_3() const { return ___H0_3; }
	inline int32_t* get_address_of_H0_3() { return &___H0_3; }
	inline void set_H0_3(int32_t value)
	{
		___H0_3 = value;
	}

	inline static int32_t get_offset_of_H1_4() { return static_cast<int32_t>(offsetof(RipeMD256Digest_t561D57139D5F60FE7160BD17668889024BA16914, ___H1_4)); }
	inline int32_t get_H1_4() const { return ___H1_4; }
	inline int32_t* get_address_of_H1_4() { return &___H1_4; }
	inline void set_H1_4(int32_t value)
	{
		___H1_4 = value;
	}

	inline static int32_t get_offset_of_H2_5() { return static_cast<int32_t>(offsetof(RipeMD256Digest_t561D57139D5F60FE7160BD17668889024BA16914, ___H2_5)); }
	inline int32_t get_H2_5() const { return ___H2_5; }
	inline int32_t* get_address_of_H2_5() { return &___H2_5; }
	inline void set_H2_5(int32_t value)
	{
		___H2_5 = value;
	}

	inline static int32_t get_offset_of_H3_6() { return static_cast<int32_t>(offsetof(RipeMD256Digest_t561D57139D5F60FE7160BD17668889024BA16914, ___H3_6)); }
	inline int32_t get_H3_6() const { return ___H3_6; }
	inline int32_t* get_address_of_H3_6() { return &___H3_6; }
	inline void set_H3_6(int32_t value)
	{
		___H3_6 = value;
	}

	inline static int32_t get_offset_of_H4_7() { return static_cast<int32_t>(offsetof(RipeMD256Digest_t561D57139D5F60FE7160BD17668889024BA16914, ___H4_7)); }
	inline int32_t get_H4_7() const { return ___H4_7; }
	inline int32_t* get_address_of_H4_7() { return &___H4_7; }
	inline void set_H4_7(int32_t value)
	{
		___H4_7 = value;
	}

	inline static int32_t get_offset_of_H5_8() { return static_cast<int32_t>(offsetof(RipeMD256Digest_t561D57139D5F60FE7160BD17668889024BA16914, ___H5_8)); }
	inline int32_t get_H5_8() const { return ___H5_8; }
	inline int32_t* get_address_of_H5_8() { return &___H5_8; }
	inline void set_H5_8(int32_t value)
	{
		___H5_8 = value;
	}

	inline static int32_t get_offset_of_H6_9() { return static_cast<int32_t>(offsetof(RipeMD256Digest_t561D57139D5F60FE7160BD17668889024BA16914, ___H6_9)); }
	inline int32_t get_H6_9() const { return ___H6_9; }
	inline int32_t* get_address_of_H6_9() { return &___H6_9; }
	inline void set_H6_9(int32_t value)
	{
		___H6_9 = value;
	}

	inline static int32_t get_offset_of_H7_10() { return static_cast<int32_t>(offsetof(RipeMD256Digest_t561D57139D5F60FE7160BD17668889024BA16914, ___H7_10)); }
	inline int32_t get_H7_10() const { return ___H7_10; }
	inline int32_t* get_address_of_H7_10() { return &___H7_10; }
	inline void set_H7_10(int32_t value)
	{
		___H7_10 = value;
	}

	inline static int32_t get_offset_of_X_11() { return static_cast<int32_t>(offsetof(RipeMD256Digest_t561D57139D5F60FE7160BD17668889024BA16914, ___X_11)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_X_11() const { return ___X_11; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_X_11() { return &___X_11; }
	inline void set_X_11(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___X_11 = value;
		Il2CppCodeGenWriteBarrier((&___X_11), value);
	}

	inline static int32_t get_offset_of_xOff_12() { return static_cast<int32_t>(offsetof(RipeMD256Digest_t561D57139D5F60FE7160BD17668889024BA16914, ___xOff_12)); }
	inline int32_t get_xOff_12() const { return ___xOff_12; }
	inline int32_t* get_address_of_xOff_12() { return &___xOff_12; }
	inline void set_xOff_12(int32_t value)
	{
		___xOff_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RIPEMD256DIGEST_T561D57139D5F60FE7160BD17668889024BA16914_H
#ifndef RIPEMD320DIGEST_T7B5823A351529F50FED8E65C36751DD9F1A75DBF_H
#define RIPEMD320DIGEST_T7B5823A351529F50FED8E65C36751DD9F1A75DBF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Digests.RipeMD320Digest
struct  RipeMD320Digest_t7B5823A351529F50FED8E65C36751DD9F1A75DBF  : public GeneralDigest_t3B10A5BF867429F885F41CE318BA1665427D50D6
{
public:
	// System.Int32 Org.BouncyCastle.Crypto.Digests.RipeMD320Digest::H0
	int32_t ___H0_3;
	// System.Int32 Org.BouncyCastle.Crypto.Digests.RipeMD320Digest::H1
	int32_t ___H1_4;
	// System.Int32 Org.BouncyCastle.Crypto.Digests.RipeMD320Digest::H2
	int32_t ___H2_5;
	// System.Int32 Org.BouncyCastle.Crypto.Digests.RipeMD320Digest::H3
	int32_t ___H3_6;
	// System.Int32 Org.BouncyCastle.Crypto.Digests.RipeMD320Digest::H4
	int32_t ___H4_7;
	// System.Int32 Org.BouncyCastle.Crypto.Digests.RipeMD320Digest::H5
	int32_t ___H5_8;
	// System.Int32 Org.BouncyCastle.Crypto.Digests.RipeMD320Digest::H6
	int32_t ___H6_9;
	// System.Int32 Org.BouncyCastle.Crypto.Digests.RipeMD320Digest::H7
	int32_t ___H7_10;
	// System.Int32 Org.BouncyCastle.Crypto.Digests.RipeMD320Digest::H8
	int32_t ___H8_11;
	// System.Int32 Org.BouncyCastle.Crypto.Digests.RipeMD320Digest::H9
	int32_t ___H9_12;
	// System.Int32[] Org.BouncyCastle.Crypto.Digests.RipeMD320Digest::X
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___X_13;
	// System.Int32 Org.BouncyCastle.Crypto.Digests.RipeMD320Digest::xOff
	int32_t ___xOff_14;

public:
	inline static int32_t get_offset_of_H0_3() { return static_cast<int32_t>(offsetof(RipeMD320Digest_t7B5823A351529F50FED8E65C36751DD9F1A75DBF, ___H0_3)); }
	inline int32_t get_H0_3() const { return ___H0_3; }
	inline int32_t* get_address_of_H0_3() { return &___H0_3; }
	inline void set_H0_3(int32_t value)
	{
		___H0_3 = value;
	}

	inline static int32_t get_offset_of_H1_4() { return static_cast<int32_t>(offsetof(RipeMD320Digest_t7B5823A351529F50FED8E65C36751DD9F1A75DBF, ___H1_4)); }
	inline int32_t get_H1_4() const { return ___H1_4; }
	inline int32_t* get_address_of_H1_4() { return &___H1_4; }
	inline void set_H1_4(int32_t value)
	{
		___H1_4 = value;
	}

	inline static int32_t get_offset_of_H2_5() { return static_cast<int32_t>(offsetof(RipeMD320Digest_t7B5823A351529F50FED8E65C36751DD9F1A75DBF, ___H2_5)); }
	inline int32_t get_H2_5() const { return ___H2_5; }
	inline int32_t* get_address_of_H2_5() { return &___H2_5; }
	inline void set_H2_5(int32_t value)
	{
		___H2_5 = value;
	}

	inline static int32_t get_offset_of_H3_6() { return static_cast<int32_t>(offsetof(RipeMD320Digest_t7B5823A351529F50FED8E65C36751DD9F1A75DBF, ___H3_6)); }
	inline int32_t get_H3_6() const { return ___H3_6; }
	inline int32_t* get_address_of_H3_6() { return &___H3_6; }
	inline void set_H3_6(int32_t value)
	{
		___H3_6 = value;
	}

	inline static int32_t get_offset_of_H4_7() { return static_cast<int32_t>(offsetof(RipeMD320Digest_t7B5823A351529F50FED8E65C36751DD9F1A75DBF, ___H4_7)); }
	inline int32_t get_H4_7() const { return ___H4_7; }
	inline int32_t* get_address_of_H4_7() { return &___H4_7; }
	inline void set_H4_7(int32_t value)
	{
		___H4_7 = value;
	}

	inline static int32_t get_offset_of_H5_8() { return static_cast<int32_t>(offsetof(RipeMD320Digest_t7B5823A351529F50FED8E65C36751DD9F1A75DBF, ___H5_8)); }
	inline int32_t get_H5_8() const { return ___H5_8; }
	inline int32_t* get_address_of_H5_8() { return &___H5_8; }
	inline void set_H5_8(int32_t value)
	{
		___H5_8 = value;
	}

	inline static int32_t get_offset_of_H6_9() { return static_cast<int32_t>(offsetof(RipeMD320Digest_t7B5823A351529F50FED8E65C36751DD9F1A75DBF, ___H6_9)); }
	inline int32_t get_H6_9() const { return ___H6_9; }
	inline int32_t* get_address_of_H6_9() { return &___H6_9; }
	inline void set_H6_9(int32_t value)
	{
		___H6_9 = value;
	}

	inline static int32_t get_offset_of_H7_10() { return static_cast<int32_t>(offsetof(RipeMD320Digest_t7B5823A351529F50FED8E65C36751DD9F1A75DBF, ___H7_10)); }
	inline int32_t get_H7_10() const { return ___H7_10; }
	inline int32_t* get_address_of_H7_10() { return &___H7_10; }
	inline void set_H7_10(int32_t value)
	{
		___H7_10 = value;
	}

	inline static int32_t get_offset_of_H8_11() { return static_cast<int32_t>(offsetof(RipeMD320Digest_t7B5823A351529F50FED8E65C36751DD9F1A75DBF, ___H8_11)); }
	inline int32_t get_H8_11() const { return ___H8_11; }
	inline int32_t* get_address_of_H8_11() { return &___H8_11; }
	inline void set_H8_11(int32_t value)
	{
		___H8_11 = value;
	}

	inline static int32_t get_offset_of_H9_12() { return static_cast<int32_t>(offsetof(RipeMD320Digest_t7B5823A351529F50FED8E65C36751DD9F1A75DBF, ___H9_12)); }
	inline int32_t get_H9_12() const { return ___H9_12; }
	inline int32_t* get_address_of_H9_12() { return &___H9_12; }
	inline void set_H9_12(int32_t value)
	{
		___H9_12 = value;
	}

	inline static int32_t get_offset_of_X_13() { return static_cast<int32_t>(offsetof(RipeMD320Digest_t7B5823A351529F50FED8E65C36751DD9F1A75DBF, ___X_13)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_X_13() const { return ___X_13; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_X_13() { return &___X_13; }
	inline void set_X_13(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___X_13 = value;
		Il2CppCodeGenWriteBarrier((&___X_13), value);
	}

	inline static int32_t get_offset_of_xOff_14() { return static_cast<int32_t>(offsetof(RipeMD320Digest_t7B5823A351529F50FED8E65C36751DD9F1A75DBF, ___xOff_14)); }
	inline int32_t get_xOff_14() const { return ___xOff_14; }
	inline int32_t* get_address_of_xOff_14() { return &___xOff_14; }
	inline void set_xOff_14(int32_t value)
	{
		___xOff_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RIPEMD320DIGEST_T7B5823A351529F50FED8E65C36751DD9F1A75DBF_H
#ifndef SHA1DIGEST_T5D1533BB233BE3AE0FE89BFB36C3267722F65CD7_H
#define SHA1DIGEST_T5D1533BB233BE3AE0FE89BFB36C3267722F65CD7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Digests.Sha1Digest
struct  Sha1Digest_t5D1533BB233BE3AE0FE89BFB36C3267722F65CD7  : public GeneralDigest_t3B10A5BF867429F885F41CE318BA1665427D50D6
{
public:
	// System.UInt32 Org.BouncyCastle.Crypto.Digests.Sha1Digest::H1
	uint32_t ___H1_3;
	// System.UInt32 Org.BouncyCastle.Crypto.Digests.Sha1Digest::H2
	uint32_t ___H2_4;
	// System.UInt32 Org.BouncyCastle.Crypto.Digests.Sha1Digest::H3
	uint32_t ___H3_5;
	// System.UInt32 Org.BouncyCastle.Crypto.Digests.Sha1Digest::H4
	uint32_t ___H4_6;
	// System.UInt32 Org.BouncyCastle.Crypto.Digests.Sha1Digest::H5
	uint32_t ___H5_7;
	// System.UInt32[] Org.BouncyCastle.Crypto.Digests.Sha1Digest::X
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___X_8;
	// System.Int32 Org.BouncyCastle.Crypto.Digests.Sha1Digest::xOff
	int32_t ___xOff_9;

public:
	inline static int32_t get_offset_of_H1_3() { return static_cast<int32_t>(offsetof(Sha1Digest_t5D1533BB233BE3AE0FE89BFB36C3267722F65CD7, ___H1_3)); }
	inline uint32_t get_H1_3() const { return ___H1_3; }
	inline uint32_t* get_address_of_H1_3() { return &___H1_3; }
	inline void set_H1_3(uint32_t value)
	{
		___H1_3 = value;
	}

	inline static int32_t get_offset_of_H2_4() { return static_cast<int32_t>(offsetof(Sha1Digest_t5D1533BB233BE3AE0FE89BFB36C3267722F65CD7, ___H2_4)); }
	inline uint32_t get_H2_4() const { return ___H2_4; }
	inline uint32_t* get_address_of_H2_4() { return &___H2_4; }
	inline void set_H2_4(uint32_t value)
	{
		___H2_4 = value;
	}

	inline static int32_t get_offset_of_H3_5() { return static_cast<int32_t>(offsetof(Sha1Digest_t5D1533BB233BE3AE0FE89BFB36C3267722F65CD7, ___H3_5)); }
	inline uint32_t get_H3_5() const { return ___H3_5; }
	inline uint32_t* get_address_of_H3_5() { return &___H3_5; }
	inline void set_H3_5(uint32_t value)
	{
		___H3_5 = value;
	}

	inline static int32_t get_offset_of_H4_6() { return static_cast<int32_t>(offsetof(Sha1Digest_t5D1533BB233BE3AE0FE89BFB36C3267722F65CD7, ___H4_6)); }
	inline uint32_t get_H4_6() const { return ___H4_6; }
	inline uint32_t* get_address_of_H4_6() { return &___H4_6; }
	inline void set_H4_6(uint32_t value)
	{
		___H4_6 = value;
	}

	inline static int32_t get_offset_of_H5_7() { return static_cast<int32_t>(offsetof(Sha1Digest_t5D1533BB233BE3AE0FE89BFB36C3267722F65CD7, ___H5_7)); }
	inline uint32_t get_H5_7() const { return ___H5_7; }
	inline uint32_t* get_address_of_H5_7() { return &___H5_7; }
	inline void set_H5_7(uint32_t value)
	{
		___H5_7 = value;
	}

	inline static int32_t get_offset_of_X_8() { return static_cast<int32_t>(offsetof(Sha1Digest_t5D1533BB233BE3AE0FE89BFB36C3267722F65CD7, ___X_8)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_X_8() const { return ___X_8; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_X_8() { return &___X_8; }
	inline void set_X_8(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___X_8 = value;
		Il2CppCodeGenWriteBarrier((&___X_8), value);
	}

	inline static int32_t get_offset_of_xOff_9() { return static_cast<int32_t>(offsetof(Sha1Digest_t5D1533BB233BE3AE0FE89BFB36C3267722F65CD7, ___xOff_9)); }
	inline int32_t get_xOff_9() const { return ___xOff_9; }
	inline int32_t* get_address_of_xOff_9() { return &___xOff_9; }
	inline void set_xOff_9(int32_t value)
	{
		___xOff_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHA1DIGEST_T5D1533BB233BE3AE0FE89BFB36C3267722F65CD7_H
#ifndef SHA224DIGEST_T4055A3B2F16FB90F8B361840F96997E133B16DDF_H
#define SHA224DIGEST_T4055A3B2F16FB90F8B361840F96997E133B16DDF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Digests.Sha224Digest
struct  Sha224Digest_t4055A3B2F16FB90F8B361840F96997E133B16DDF  : public GeneralDigest_t3B10A5BF867429F885F41CE318BA1665427D50D6
{
public:
	// System.UInt32 Org.BouncyCastle.Crypto.Digests.Sha224Digest::H1
	uint32_t ___H1_3;
	// System.UInt32 Org.BouncyCastle.Crypto.Digests.Sha224Digest::H2
	uint32_t ___H2_4;
	// System.UInt32 Org.BouncyCastle.Crypto.Digests.Sha224Digest::H3
	uint32_t ___H3_5;
	// System.UInt32 Org.BouncyCastle.Crypto.Digests.Sha224Digest::H4
	uint32_t ___H4_6;
	// System.UInt32 Org.BouncyCastle.Crypto.Digests.Sha224Digest::H5
	uint32_t ___H5_7;
	// System.UInt32 Org.BouncyCastle.Crypto.Digests.Sha224Digest::H6
	uint32_t ___H6_8;
	// System.UInt32 Org.BouncyCastle.Crypto.Digests.Sha224Digest::H7
	uint32_t ___H7_9;
	// System.UInt32 Org.BouncyCastle.Crypto.Digests.Sha224Digest::H8
	uint32_t ___H8_10;
	// System.UInt32[] Org.BouncyCastle.Crypto.Digests.Sha224Digest::X
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___X_11;
	// System.Int32 Org.BouncyCastle.Crypto.Digests.Sha224Digest::xOff
	int32_t ___xOff_12;

public:
	inline static int32_t get_offset_of_H1_3() { return static_cast<int32_t>(offsetof(Sha224Digest_t4055A3B2F16FB90F8B361840F96997E133B16DDF, ___H1_3)); }
	inline uint32_t get_H1_3() const { return ___H1_3; }
	inline uint32_t* get_address_of_H1_3() { return &___H1_3; }
	inline void set_H1_3(uint32_t value)
	{
		___H1_3 = value;
	}

	inline static int32_t get_offset_of_H2_4() { return static_cast<int32_t>(offsetof(Sha224Digest_t4055A3B2F16FB90F8B361840F96997E133B16DDF, ___H2_4)); }
	inline uint32_t get_H2_4() const { return ___H2_4; }
	inline uint32_t* get_address_of_H2_4() { return &___H2_4; }
	inline void set_H2_4(uint32_t value)
	{
		___H2_4 = value;
	}

	inline static int32_t get_offset_of_H3_5() { return static_cast<int32_t>(offsetof(Sha224Digest_t4055A3B2F16FB90F8B361840F96997E133B16DDF, ___H3_5)); }
	inline uint32_t get_H3_5() const { return ___H3_5; }
	inline uint32_t* get_address_of_H3_5() { return &___H3_5; }
	inline void set_H3_5(uint32_t value)
	{
		___H3_5 = value;
	}

	inline static int32_t get_offset_of_H4_6() { return static_cast<int32_t>(offsetof(Sha224Digest_t4055A3B2F16FB90F8B361840F96997E133B16DDF, ___H4_6)); }
	inline uint32_t get_H4_6() const { return ___H4_6; }
	inline uint32_t* get_address_of_H4_6() { return &___H4_6; }
	inline void set_H4_6(uint32_t value)
	{
		___H4_6 = value;
	}

	inline static int32_t get_offset_of_H5_7() { return static_cast<int32_t>(offsetof(Sha224Digest_t4055A3B2F16FB90F8B361840F96997E133B16DDF, ___H5_7)); }
	inline uint32_t get_H5_7() const { return ___H5_7; }
	inline uint32_t* get_address_of_H5_7() { return &___H5_7; }
	inline void set_H5_7(uint32_t value)
	{
		___H5_7 = value;
	}

	inline static int32_t get_offset_of_H6_8() { return static_cast<int32_t>(offsetof(Sha224Digest_t4055A3B2F16FB90F8B361840F96997E133B16DDF, ___H6_8)); }
	inline uint32_t get_H6_8() const { return ___H6_8; }
	inline uint32_t* get_address_of_H6_8() { return &___H6_8; }
	inline void set_H6_8(uint32_t value)
	{
		___H6_8 = value;
	}

	inline static int32_t get_offset_of_H7_9() { return static_cast<int32_t>(offsetof(Sha224Digest_t4055A3B2F16FB90F8B361840F96997E133B16DDF, ___H7_9)); }
	inline uint32_t get_H7_9() const { return ___H7_9; }
	inline uint32_t* get_address_of_H7_9() { return &___H7_9; }
	inline void set_H7_9(uint32_t value)
	{
		___H7_9 = value;
	}

	inline static int32_t get_offset_of_H8_10() { return static_cast<int32_t>(offsetof(Sha224Digest_t4055A3B2F16FB90F8B361840F96997E133B16DDF, ___H8_10)); }
	inline uint32_t get_H8_10() const { return ___H8_10; }
	inline uint32_t* get_address_of_H8_10() { return &___H8_10; }
	inline void set_H8_10(uint32_t value)
	{
		___H8_10 = value;
	}

	inline static int32_t get_offset_of_X_11() { return static_cast<int32_t>(offsetof(Sha224Digest_t4055A3B2F16FB90F8B361840F96997E133B16DDF, ___X_11)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_X_11() const { return ___X_11; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_X_11() { return &___X_11; }
	inline void set_X_11(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___X_11 = value;
		Il2CppCodeGenWriteBarrier((&___X_11), value);
	}

	inline static int32_t get_offset_of_xOff_12() { return static_cast<int32_t>(offsetof(Sha224Digest_t4055A3B2F16FB90F8B361840F96997E133B16DDF, ___xOff_12)); }
	inline int32_t get_xOff_12() const { return ___xOff_12; }
	inline int32_t* get_address_of_xOff_12() { return &___xOff_12; }
	inline void set_xOff_12(int32_t value)
	{
		___xOff_12 = value;
	}
};

struct Sha224Digest_t4055A3B2F16FB90F8B361840F96997E133B16DDF_StaticFields
{
public:
	// System.UInt32[] Org.BouncyCastle.Crypto.Digests.Sha224Digest::K
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___K_13;

public:
	inline static int32_t get_offset_of_K_13() { return static_cast<int32_t>(offsetof(Sha224Digest_t4055A3B2F16FB90F8B361840F96997E133B16DDF_StaticFields, ___K_13)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_K_13() const { return ___K_13; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_K_13() { return &___K_13; }
	inline void set_K_13(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___K_13 = value;
		Il2CppCodeGenWriteBarrier((&___K_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHA224DIGEST_T4055A3B2F16FB90F8B361840F96997E133B16DDF_H
#ifndef SHA256DIGEST_T3F74BE4A7A03825407AA99B6E6D52E9DB5016358_H
#define SHA256DIGEST_T3F74BE4A7A03825407AA99B6E6D52E9DB5016358_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Digests.Sha256Digest
struct  Sha256Digest_t3F74BE4A7A03825407AA99B6E6D52E9DB5016358  : public GeneralDigest_t3B10A5BF867429F885F41CE318BA1665427D50D6
{
public:
	// System.UInt32 Org.BouncyCastle.Crypto.Digests.Sha256Digest::H1
	uint32_t ___H1_3;
	// System.UInt32 Org.BouncyCastle.Crypto.Digests.Sha256Digest::H2
	uint32_t ___H2_4;
	// System.UInt32 Org.BouncyCastle.Crypto.Digests.Sha256Digest::H3
	uint32_t ___H3_5;
	// System.UInt32 Org.BouncyCastle.Crypto.Digests.Sha256Digest::H4
	uint32_t ___H4_6;
	// System.UInt32 Org.BouncyCastle.Crypto.Digests.Sha256Digest::H5
	uint32_t ___H5_7;
	// System.UInt32 Org.BouncyCastle.Crypto.Digests.Sha256Digest::H6
	uint32_t ___H6_8;
	// System.UInt32 Org.BouncyCastle.Crypto.Digests.Sha256Digest::H7
	uint32_t ___H7_9;
	// System.UInt32 Org.BouncyCastle.Crypto.Digests.Sha256Digest::H8
	uint32_t ___H8_10;
	// System.UInt32[] Org.BouncyCastle.Crypto.Digests.Sha256Digest::X
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___X_11;
	// System.Int32 Org.BouncyCastle.Crypto.Digests.Sha256Digest::xOff
	int32_t ___xOff_12;

public:
	inline static int32_t get_offset_of_H1_3() { return static_cast<int32_t>(offsetof(Sha256Digest_t3F74BE4A7A03825407AA99B6E6D52E9DB5016358, ___H1_3)); }
	inline uint32_t get_H1_3() const { return ___H1_3; }
	inline uint32_t* get_address_of_H1_3() { return &___H1_3; }
	inline void set_H1_3(uint32_t value)
	{
		___H1_3 = value;
	}

	inline static int32_t get_offset_of_H2_4() { return static_cast<int32_t>(offsetof(Sha256Digest_t3F74BE4A7A03825407AA99B6E6D52E9DB5016358, ___H2_4)); }
	inline uint32_t get_H2_4() const { return ___H2_4; }
	inline uint32_t* get_address_of_H2_4() { return &___H2_4; }
	inline void set_H2_4(uint32_t value)
	{
		___H2_4 = value;
	}

	inline static int32_t get_offset_of_H3_5() { return static_cast<int32_t>(offsetof(Sha256Digest_t3F74BE4A7A03825407AA99B6E6D52E9DB5016358, ___H3_5)); }
	inline uint32_t get_H3_5() const { return ___H3_5; }
	inline uint32_t* get_address_of_H3_5() { return &___H3_5; }
	inline void set_H3_5(uint32_t value)
	{
		___H3_5 = value;
	}

	inline static int32_t get_offset_of_H4_6() { return static_cast<int32_t>(offsetof(Sha256Digest_t3F74BE4A7A03825407AA99B6E6D52E9DB5016358, ___H4_6)); }
	inline uint32_t get_H4_6() const { return ___H4_6; }
	inline uint32_t* get_address_of_H4_6() { return &___H4_6; }
	inline void set_H4_6(uint32_t value)
	{
		___H4_6 = value;
	}

	inline static int32_t get_offset_of_H5_7() { return static_cast<int32_t>(offsetof(Sha256Digest_t3F74BE4A7A03825407AA99B6E6D52E9DB5016358, ___H5_7)); }
	inline uint32_t get_H5_7() const { return ___H5_7; }
	inline uint32_t* get_address_of_H5_7() { return &___H5_7; }
	inline void set_H5_7(uint32_t value)
	{
		___H5_7 = value;
	}

	inline static int32_t get_offset_of_H6_8() { return static_cast<int32_t>(offsetof(Sha256Digest_t3F74BE4A7A03825407AA99B6E6D52E9DB5016358, ___H6_8)); }
	inline uint32_t get_H6_8() const { return ___H6_8; }
	inline uint32_t* get_address_of_H6_8() { return &___H6_8; }
	inline void set_H6_8(uint32_t value)
	{
		___H6_8 = value;
	}

	inline static int32_t get_offset_of_H7_9() { return static_cast<int32_t>(offsetof(Sha256Digest_t3F74BE4A7A03825407AA99B6E6D52E9DB5016358, ___H7_9)); }
	inline uint32_t get_H7_9() const { return ___H7_9; }
	inline uint32_t* get_address_of_H7_9() { return &___H7_9; }
	inline void set_H7_9(uint32_t value)
	{
		___H7_9 = value;
	}

	inline static int32_t get_offset_of_H8_10() { return static_cast<int32_t>(offsetof(Sha256Digest_t3F74BE4A7A03825407AA99B6E6D52E9DB5016358, ___H8_10)); }
	inline uint32_t get_H8_10() const { return ___H8_10; }
	inline uint32_t* get_address_of_H8_10() { return &___H8_10; }
	inline void set_H8_10(uint32_t value)
	{
		___H8_10 = value;
	}

	inline static int32_t get_offset_of_X_11() { return static_cast<int32_t>(offsetof(Sha256Digest_t3F74BE4A7A03825407AA99B6E6D52E9DB5016358, ___X_11)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_X_11() const { return ___X_11; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_X_11() { return &___X_11; }
	inline void set_X_11(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___X_11 = value;
		Il2CppCodeGenWriteBarrier((&___X_11), value);
	}

	inline static int32_t get_offset_of_xOff_12() { return static_cast<int32_t>(offsetof(Sha256Digest_t3F74BE4A7A03825407AA99B6E6D52E9DB5016358, ___xOff_12)); }
	inline int32_t get_xOff_12() const { return ___xOff_12; }
	inline int32_t* get_address_of_xOff_12() { return &___xOff_12; }
	inline void set_xOff_12(int32_t value)
	{
		___xOff_12 = value;
	}
};

struct Sha256Digest_t3F74BE4A7A03825407AA99B6E6D52E9DB5016358_StaticFields
{
public:
	// System.UInt32[] Org.BouncyCastle.Crypto.Digests.Sha256Digest::K
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___K_13;

public:
	inline static int32_t get_offset_of_K_13() { return static_cast<int32_t>(offsetof(Sha256Digest_t3F74BE4A7A03825407AA99B6E6D52E9DB5016358_StaticFields, ___K_13)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_K_13() const { return ___K_13; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_K_13() { return &___K_13; }
	inline void set_K_13(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___K_13 = value;
		Il2CppCodeGenWriteBarrier((&___K_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHA256DIGEST_T3F74BE4A7A03825407AA99B6E6D52E9DB5016358_H
#ifndef SHA384DIGEST_T22195F1F88F9F90ECE845E368637FCFA58144D6C_H
#define SHA384DIGEST_T22195F1F88F9F90ECE845E368637FCFA58144D6C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Digests.Sha384Digest
struct  Sha384Digest_t22195F1F88F9F90ECE845E368637FCFA58144D6C  : public LongDigest_t043705804892FF0397DAA2E061CCF2DE8BA8BB38
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHA384DIGEST_T22195F1F88F9F90ECE845E368637FCFA58144D6C_H
#ifndef SHA3DIGEST_T94D2D74FDDEC25215DC989F84319513E346D8E1E_H
#define SHA3DIGEST_T94D2D74FDDEC25215DC989F84319513E346D8E1E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Digests.Sha3Digest
struct  Sha3Digest_t94D2D74FDDEC25215DC989F84319513E346D8E1E  : public KeccakDigest_tEFB4CFB19A2FC24160E02A8821D4BAE8862E3AEB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHA3DIGEST_T94D2D74FDDEC25215DC989F84319513E346D8E1E_H
#ifndef SHA512DIGEST_T3E26A90EE75F594E83339CA735984309CB9B7263_H
#define SHA512DIGEST_T3E26A90EE75F594E83339CA735984309CB9B7263_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Digests.Sha512Digest
struct  Sha512Digest_t3E26A90EE75F594E83339CA735984309CB9B7263  : public LongDigest_t043705804892FF0397DAA2E061CCF2DE8BA8BB38
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHA512DIGEST_T3E26A90EE75F594E83339CA735984309CB9B7263_H
#ifndef SHA512TDIGEST_TDB0EA86904BE1D448D78ACED5DB7139242E1BBDA_H
#define SHA512TDIGEST_TDB0EA86904BE1D448D78ACED5DB7139242E1BBDA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Digests.Sha512tDigest
struct  Sha512tDigest_tDB0EA86904BE1D448D78ACED5DB7139242E1BBDA  : public LongDigest_t043705804892FF0397DAA2E061CCF2DE8BA8BB38
{
public:
	// System.Int32 Org.BouncyCastle.Crypto.Digests.Sha512tDigest::digestLength
	int32_t ___digestLength_16;
	// System.UInt64 Org.BouncyCastle.Crypto.Digests.Sha512tDigest::H1t
	uint64_t ___H1t_17;
	// System.UInt64 Org.BouncyCastle.Crypto.Digests.Sha512tDigest::H2t
	uint64_t ___H2t_18;
	// System.UInt64 Org.BouncyCastle.Crypto.Digests.Sha512tDigest::H3t
	uint64_t ___H3t_19;
	// System.UInt64 Org.BouncyCastle.Crypto.Digests.Sha512tDigest::H4t
	uint64_t ___H4t_20;
	// System.UInt64 Org.BouncyCastle.Crypto.Digests.Sha512tDigest::H5t
	uint64_t ___H5t_21;
	// System.UInt64 Org.BouncyCastle.Crypto.Digests.Sha512tDigest::H6t
	uint64_t ___H6t_22;
	// System.UInt64 Org.BouncyCastle.Crypto.Digests.Sha512tDigest::H7t
	uint64_t ___H7t_23;
	// System.UInt64 Org.BouncyCastle.Crypto.Digests.Sha512tDigest::H8t
	uint64_t ___H8t_24;

public:
	inline static int32_t get_offset_of_digestLength_16() { return static_cast<int32_t>(offsetof(Sha512tDigest_tDB0EA86904BE1D448D78ACED5DB7139242E1BBDA, ___digestLength_16)); }
	inline int32_t get_digestLength_16() const { return ___digestLength_16; }
	inline int32_t* get_address_of_digestLength_16() { return &___digestLength_16; }
	inline void set_digestLength_16(int32_t value)
	{
		___digestLength_16 = value;
	}

	inline static int32_t get_offset_of_H1t_17() { return static_cast<int32_t>(offsetof(Sha512tDigest_tDB0EA86904BE1D448D78ACED5DB7139242E1BBDA, ___H1t_17)); }
	inline uint64_t get_H1t_17() const { return ___H1t_17; }
	inline uint64_t* get_address_of_H1t_17() { return &___H1t_17; }
	inline void set_H1t_17(uint64_t value)
	{
		___H1t_17 = value;
	}

	inline static int32_t get_offset_of_H2t_18() { return static_cast<int32_t>(offsetof(Sha512tDigest_tDB0EA86904BE1D448D78ACED5DB7139242E1BBDA, ___H2t_18)); }
	inline uint64_t get_H2t_18() const { return ___H2t_18; }
	inline uint64_t* get_address_of_H2t_18() { return &___H2t_18; }
	inline void set_H2t_18(uint64_t value)
	{
		___H2t_18 = value;
	}

	inline static int32_t get_offset_of_H3t_19() { return static_cast<int32_t>(offsetof(Sha512tDigest_tDB0EA86904BE1D448D78ACED5DB7139242E1BBDA, ___H3t_19)); }
	inline uint64_t get_H3t_19() const { return ___H3t_19; }
	inline uint64_t* get_address_of_H3t_19() { return &___H3t_19; }
	inline void set_H3t_19(uint64_t value)
	{
		___H3t_19 = value;
	}

	inline static int32_t get_offset_of_H4t_20() { return static_cast<int32_t>(offsetof(Sha512tDigest_tDB0EA86904BE1D448D78ACED5DB7139242E1BBDA, ___H4t_20)); }
	inline uint64_t get_H4t_20() const { return ___H4t_20; }
	inline uint64_t* get_address_of_H4t_20() { return &___H4t_20; }
	inline void set_H4t_20(uint64_t value)
	{
		___H4t_20 = value;
	}

	inline static int32_t get_offset_of_H5t_21() { return static_cast<int32_t>(offsetof(Sha512tDigest_tDB0EA86904BE1D448D78ACED5DB7139242E1BBDA, ___H5t_21)); }
	inline uint64_t get_H5t_21() const { return ___H5t_21; }
	inline uint64_t* get_address_of_H5t_21() { return &___H5t_21; }
	inline void set_H5t_21(uint64_t value)
	{
		___H5t_21 = value;
	}

	inline static int32_t get_offset_of_H6t_22() { return static_cast<int32_t>(offsetof(Sha512tDigest_tDB0EA86904BE1D448D78ACED5DB7139242E1BBDA, ___H6t_22)); }
	inline uint64_t get_H6t_22() const { return ___H6t_22; }
	inline uint64_t* get_address_of_H6t_22() { return &___H6t_22; }
	inline void set_H6t_22(uint64_t value)
	{
		___H6t_22 = value;
	}

	inline static int32_t get_offset_of_H7t_23() { return static_cast<int32_t>(offsetof(Sha512tDigest_tDB0EA86904BE1D448D78ACED5DB7139242E1BBDA, ___H7t_23)); }
	inline uint64_t get_H7t_23() const { return ___H7t_23; }
	inline uint64_t* get_address_of_H7t_23() { return &___H7t_23; }
	inline void set_H7t_23(uint64_t value)
	{
		___H7t_23 = value;
	}

	inline static int32_t get_offset_of_H8t_24() { return static_cast<int32_t>(offsetof(Sha512tDigest_tDB0EA86904BE1D448D78ACED5DB7139242E1BBDA, ___H8t_24)); }
	inline uint64_t get_H8t_24() const { return ___H8t_24; }
	inline uint64_t* get_address_of_H8t_24() { return &___H8t_24; }
	inline void set_H8t_24(uint64_t value)
	{
		___H8t_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHA512TDIGEST_TDB0EA86904BE1D448D78ACED5DB7139242E1BBDA_H
#ifndef SHAKEDIGEST_T43101B71FFF13FEE7CDF47A8393959C16BC54392_H
#define SHAKEDIGEST_T43101B71FFF13FEE7CDF47A8393959C16BC54392_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Digests.ShakeDigest
struct  ShakeDigest_t43101B71FFF13FEE7CDF47A8393959C16BC54392  : public KeccakDigest_tEFB4CFB19A2FC24160E02A8821D4BAE8862E3AEB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHAKEDIGEST_T43101B71FFF13FEE7CDF47A8393959C16BC54392_H
#ifndef CURVE25519HOLDER_TEDC6552D2DC46135C31AAB5CAAE4C68B56FA7009_H
#define CURVE25519HOLDER_TEDC6552D2DC46135C31AAB5CAAE4C68B56FA7009_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.EC.CustomNamedCurves_Curve25519Holder
struct  Curve25519Holder_tEDC6552D2DC46135C31AAB5CAAE4C68B56FA7009  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct Curve25519Holder_tEDC6552D2DC46135C31AAB5CAAE4C68B56FA7009_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Crypto.EC.CustomNamedCurves_Curve25519Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Curve25519Holder_tEDC6552D2DC46135C31AAB5CAAE4C68B56FA7009_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURVE25519HOLDER_TEDC6552D2DC46135C31AAB5CAAE4C68B56FA7009_H
#ifndef SECP128R1HOLDER_TEE2D151BBFD1BBD13084C1B33283DFFA57F7F766_H
#define SECP128R1HOLDER_TEE2D151BBFD1BBD13084C1B33283DFFA57F7F766_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecP128R1Holder
struct  SecP128R1Holder_tEE2D151BBFD1BBD13084C1B33283DFFA57F7F766  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct SecP128R1Holder_tEE2D151BBFD1BBD13084C1B33283DFFA57F7F766_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecP128R1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(SecP128R1Holder_tEE2D151BBFD1BBD13084C1B33283DFFA57F7F766_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP128R1HOLDER_TEE2D151BBFD1BBD13084C1B33283DFFA57F7F766_H
#ifndef SECP160K1HOLDER_T2189D87F11B9A00F78C1ABDD2BC8BBCD5D5AC760_H
#define SECP160K1HOLDER_T2189D87F11B9A00F78C1ABDD2BC8BBCD5D5AC760_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecP160K1Holder
struct  SecP160K1Holder_t2189D87F11B9A00F78C1ABDD2BC8BBCD5D5AC760  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct SecP160K1Holder_t2189D87F11B9A00F78C1ABDD2BC8BBCD5D5AC760_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecP160K1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(SecP160K1Holder_t2189D87F11B9A00F78C1ABDD2BC8BBCD5D5AC760_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP160K1HOLDER_T2189D87F11B9A00F78C1ABDD2BC8BBCD5D5AC760_H
#ifndef SECP160R1HOLDER_T0F1F9E9E39D021B73964428DC06846071259E075_H
#define SECP160R1HOLDER_T0F1F9E9E39D021B73964428DC06846071259E075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecP160R1Holder
struct  SecP160R1Holder_t0F1F9E9E39D021B73964428DC06846071259E075  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct SecP160R1Holder_t0F1F9E9E39D021B73964428DC06846071259E075_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecP160R1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(SecP160R1Holder_t0F1F9E9E39D021B73964428DC06846071259E075_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP160R1HOLDER_T0F1F9E9E39D021B73964428DC06846071259E075_H
#ifndef SECP160R2HOLDER_TB952668B9343E59C29CFBF2BA0FDB0F860E9A2E4_H
#define SECP160R2HOLDER_TB952668B9343E59C29CFBF2BA0FDB0F860E9A2E4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecP160R2Holder
struct  SecP160R2Holder_tB952668B9343E59C29CFBF2BA0FDB0F860E9A2E4  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct SecP160R2Holder_tB952668B9343E59C29CFBF2BA0FDB0F860E9A2E4_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecP160R2Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(SecP160R2Holder_tB952668B9343E59C29CFBF2BA0FDB0F860E9A2E4_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP160R2HOLDER_TB952668B9343E59C29CFBF2BA0FDB0F860E9A2E4_H
#ifndef SECP192K1HOLDER_T1854155A8EE7F5EC4366BF344522F9B049322334_H
#define SECP192K1HOLDER_T1854155A8EE7F5EC4366BF344522F9B049322334_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecP192K1Holder
struct  SecP192K1Holder_t1854155A8EE7F5EC4366BF344522F9B049322334  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct SecP192K1Holder_t1854155A8EE7F5EC4366BF344522F9B049322334_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecP192K1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(SecP192K1Holder_t1854155A8EE7F5EC4366BF344522F9B049322334_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP192K1HOLDER_T1854155A8EE7F5EC4366BF344522F9B049322334_H
#ifndef SECP192R1HOLDER_T1838B785B620A28A5D3FD6C3A5EE8D1BA5026AA0_H
#define SECP192R1HOLDER_T1838B785B620A28A5D3FD6C3A5EE8D1BA5026AA0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecP192R1Holder
struct  SecP192R1Holder_t1838B785B620A28A5D3FD6C3A5EE8D1BA5026AA0  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct SecP192R1Holder_t1838B785B620A28A5D3FD6C3A5EE8D1BA5026AA0_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecP192R1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(SecP192R1Holder_t1838B785B620A28A5D3FD6C3A5EE8D1BA5026AA0_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP192R1HOLDER_T1838B785B620A28A5D3FD6C3A5EE8D1BA5026AA0_H
#ifndef SECP224K1HOLDER_T228EE1894DA5592B605AFA3D429B254F9F22BD13_H
#define SECP224K1HOLDER_T228EE1894DA5592B605AFA3D429B254F9F22BD13_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecP224K1Holder
struct  SecP224K1Holder_t228EE1894DA5592B605AFA3D429B254F9F22BD13  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct SecP224K1Holder_t228EE1894DA5592B605AFA3D429B254F9F22BD13_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecP224K1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(SecP224K1Holder_t228EE1894DA5592B605AFA3D429B254F9F22BD13_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP224K1HOLDER_T228EE1894DA5592B605AFA3D429B254F9F22BD13_H
#ifndef SECP224R1HOLDER_TA7671CD963C3387945C8A1A15086ACF8E089381A_H
#define SECP224R1HOLDER_TA7671CD963C3387945C8A1A15086ACF8E089381A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecP224R1Holder
struct  SecP224R1Holder_tA7671CD963C3387945C8A1A15086ACF8E089381A  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct SecP224R1Holder_tA7671CD963C3387945C8A1A15086ACF8E089381A_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecP224R1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(SecP224R1Holder_tA7671CD963C3387945C8A1A15086ACF8E089381A_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP224R1HOLDER_TA7671CD963C3387945C8A1A15086ACF8E089381A_H
#ifndef SECP256K1HOLDER_T8571333793FE4231818C78C6D6C507CEADE3DA6F_H
#define SECP256K1HOLDER_T8571333793FE4231818C78C6D6C507CEADE3DA6F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecP256K1Holder
struct  SecP256K1Holder_t8571333793FE4231818C78C6D6C507CEADE3DA6F  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct SecP256K1Holder_t8571333793FE4231818C78C6D6C507CEADE3DA6F_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecP256K1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(SecP256K1Holder_t8571333793FE4231818C78C6D6C507CEADE3DA6F_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP256K1HOLDER_T8571333793FE4231818C78C6D6C507CEADE3DA6F_H
#ifndef SECP256R1HOLDER_T359C1A2C94CACF440F21AE4A82F2B6B68AF3AA05_H
#define SECP256R1HOLDER_T359C1A2C94CACF440F21AE4A82F2B6B68AF3AA05_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecP256R1Holder
struct  SecP256R1Holder_t359C1A2C94CACF440F21AE4A82F2B6B68AF3AA05  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct SecP256R1Holder_t359C1A2C94CACF440F21AE4A82F2B6B68AF3AA05_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecP256R1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(SecP256R1Holder_t359C1A2C94CACF440F21AE4A82F2B6B68AF3AA05_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP256R1HOLDER_T359C1A2C94CACF440F21AE4A82F2B6B68AF3AA05_H
#ifndef SECP384R1HOLDER_T9BBC3EA9F967051B3712E7A6F1B06394B13CA002_H
#define SECP384R1HOLDER_T9BBC3EA9F967051B3712E7A6F1B06394B13CA002_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecP384R1Holder
struct  SecP384R1Holder_t9BBC3EA9F967051B3712E7A6F1B06394B13CA002  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct SecP384R1Holder_t9BBC3EA9F967051B3712E7A6F1B06394B13CA002_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecP384R1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(SecP384R1Holder_t9BBC3EA9F967051B3712E7A6F1B06394B13CA002_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP384R1HOLDER_T9BBC3EA9F967051B3712E7A6F1B06394B13CA002_H
#ifndef SECP521R1HOLDER_TBDDA59122AC4593188452368A9DF9049BED61D6C_H
#define SECP521R1HOLDER_TBDDA59122AC4593188452368A9DF9049BED61D6C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecP521R1Holder
struct  SecP521R1Holder_tBDDA59122AC4593188452368A9DF9049BED61D6C  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct SecP521R1Holder_tBDDA59122AC4593188452368A9DF9049BED61D6C_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecP521R1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(SecP521R1Holder_tBDDA59122AC4593188452368A9DF9049BED61D6C_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP521R1HOLDER_TBDDA59122AC4593188452368A9DF9049BED61D6C_H
#ifndef SECT113R1HOLDER_TD74EB859A51B727FE10FE52316FFD6DF1CF0B91C_H
#define SECT113R1HOLDER_TD74EB859A51B727FE10FE52316FFD6DF1CF0B91C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecT113R1Holder
struct  SecT113R1Holder_tD74EB859A51B727FE10FE52316FFD6DF1CF0B91C  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct SecT113R1Holder_tD74EB859A51B727FE10FE52316FFD6DF1CF0B91C_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecT113R1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(SecT113R1Holder_tD74EB859A51B727FE10FE52316FFD6DF1CF0B91C_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT113R1HOLDER_TD74EB859A51B727FE10FE52316FFD6DF1CF0B91C_H
#ifndef SECT113R2HOLDER_TDDF8D1D2EA30175BC9086D14533E62BE9F55FF20_H
#define SECT113R2HOLDER_TDDF8D1D2EA30175BC9086D14533E62BE9F55FF20_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecT113R2Holder
struct  SecT113R2Holder_tDDF8D1D2EA30175BC9086D14533E62BE9F55FF20  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct SecT113R2Holder_tDDF8D1D2EA30175BC9086D14533E62BE9F55FF20_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecT113R2Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(SecT113R2Holder_tDDF8D1D2EA30175BC9086D14533E62BE9F55FF20_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT113R2HOLDER_TDDF8D1D2EA30175BC9086D14533E62BE9F55FF20_H
#ifndef SECT131R1HOLDER_T4004EE12094FFEC06BC9D42A2D6E8F3BAC64D0E3_H
#define SECT131R1HOLDER_T4004EE12094FFEC06BC9D42A2D6E8F3BAC64D0E3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecT131R1Holder
struct  SecT131R1Holder_t4004EE12094FFEC06BC9D42A2D6E8F3BAC64D0E3  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct SecT131R1Holder_t4004EE12094FFEC06BC9D42A2D6E8F3BAC64D0E3_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecT131R1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(SecT131R1Holder_t4004EE12094FFEC06BC9D42A2D6E8F3BAC64D0E3_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT131R1HOLDER_T4004EE12094FFEC06BC9D42A2D6E8F3BAC64D0E3_H
#ifndef SECT131R2HOLDER_T4A2A6B65A23DB1CAC7DE749E4F4C66D2446252C5_H
#define SECT131R2HOLDER_T4A2A6B65A23DB1CAC7DE749E4F4C66D2446252C5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecT131R2Holder
struct  SecT131R2Holder_t4A2A6B65A23DB1CAC7DE749E4F4C66D2446252C5  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct SecT131R2Holder_t4A2A6B65A23DB1CAC7DE749E4F4C66D2446252C5_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecT131R2Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(SecT131R2Holder_t4A2A6B65A23DB1CAC7DE749E4F4C66D2446252C5_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT131R2HOLDER_T4A2A6B65A23DB1CAC7DE749E4F4C66D2446252C5_H
#ifndef SECT163K1HOLDER_T6C9770BF521BC1D3645D3093BC3E8A4BE9AEC73E_H
#define SECT163K1HOLDER_T6C9770BF521BC1D3645D3093BC3E8A4BE9AEC73E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecT163K1Holder
struct  SecT163K1Holder_t6C9770BF521BC1D3645D3093BC3E8A4BE9AEC73E  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct SecT163K1Holder_t6C9770BF521BC1D3645D3093BC3E8A4BE9AEC73E_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecT163K1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(SecT163K1Holder_t6C9770BF521BC1D3645D3093BC3E8A4BE9AEC73E_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT163K1HOLDER_T6C9770BF521BC1D3645D3093BC3E8A4BE9AEC73E_H
#ifndef SECT163R1HOLDER_T07B93108864561CDC55E615C5EE3A2DB14366684_H
#define SECT163R1HOLDER_T07B93108864561CDC55E615C5EE3A2DB14366684_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecT163R1Holder
struct  SecT163R1Holder_t07B93108864561CDC55E615C5EE3A2DB14366684  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct SecT163R1Holder_t07B93108864561CDC55E615C5EE3A2DB14366684_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecT163R1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(SecT163R1Holder_t07B93108864561CDC55E615C5EE3A2DB14366684_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT163R1HOLDER_T07B93108864561CDC55E615C5EE3A2DB14366684_H
#ifndef SECT163R2HOLDER_TB3329D587A5729ACD56A7A366150113ACE218DFC_H
#define SECT163R2HOLDER_TB3329D587A5729ACD56A7A366150113ACE218DFC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecT163R2Holder
struct  SecT163R2Holder_tB3329D587A5729ACD56A7A366150113ACE218DFC  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct SecT163R2Holder_tB3329D587A5729ACD56A7A366150113ACE218DFC_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecT163R2Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(SecT163R2Holder_tB3329D587A5729ACD56A7A366150113ACE218DFC_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT163R2HOLDER_TB3329D587A5729ACD56A7A366150113ACE218DFC_H
#ifndef SECT193R1HOLDER_TA268FDA70215BD3FFB5FE93B70D7CA3F7A4DEE19_H
#define SECT193R1HOLDER_TA268FDA70215BD3FFB5FE93B70D7CA3F7A4DEE19_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecT193R1Holder
struct  SecT193R1Holder_tA268FDA70215BD3FFB5FE93B70D7CA3F7A4DEE19  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct SecT193R1Holder_tA268FDA70215BD3FFB5FE93B70D7CA3F7A4DEE19_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecT193R1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(SecT193R1Holder_tA268FDA70215BD3FFB5FE93B70D7CA3F7A4DEE19_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT193R1HOLDER_TA268FDA70215BD3FFB5FE93B70D7CA3F7A4DEE19_H
#ifndef SECT193R2HOLDER_T98F1D65132D0B301FF86BDC108D3BDDDA6BE7159_H
#define SECT193R2HOLDER_T98F1D65132D0B301FF86BDC108D3BDDDA6BE7159_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecT193R2Holder
struct  SecT193R2Holder_t98F1D65132D0B301FF86BDC108D3BDDDA6BE7159  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct SecT193R2Holder_t98F1D65132D0B301FF86BDC108D3BDDDA6BE7159_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecT193R2Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(SecT193R2Holder_t98F1D65132D0B301FF86BDC108D3BDDDA6BE7159_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT193R2HOLDER_T98F1D65132D0B301FF86BDC108D3BDDDA6BE7159_H
#ifndef SECT233K1HOLDER_TC26BBB3F3E2A74AFD990AC6BA8493E63B260E46F_H
#define SECT233K1HOLDER_TC26BBB3F3E2A74AFD990AC6BA8493E63B260E46F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecT233K1Holder
struct  SecT233K1Holder_tC26BBB3F3E2A74AFD990AC6BA8493E63B260E46F  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct SecT233K1Holder_tC26BBB3F3E2A74AFD990AC6BA8493E63B260E46F_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecT233K1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(SecT233K1Holder_tC26BBB3F3E2A74AFD990AC6BA8493E63B260E46F_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT233K1HOLDER_TC26BBB3F3E2A74AFD990AC6BA8493E63B260E46F_H
#ifndef SECT233R1HOLDER_TB2B079B50850002ED4DECED1374BCB874A8FFFC5_H
#define SECT233R1HOLDER_TB2B079B50850002ED4DECED1374BCB874A8FFFC5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecT233R1Holder
struct  SecT233R1Holder_tB2B079B50850002ED4DECED1374BCB874A8FFFC5  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct SecT233R1Holder_tB2B079B50850002ED4DECED1374BCB874A8FFFC5_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecT233R1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(SecT233R1Holder_tB2B079B50850002ED4DECED1374BCB874A8FFFC5_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT233R1HOLDER_TB2B079B50850002ED4DECED1374BCB874A8FFFC5_H
#ifndef SECT239K1HOLDER_TED78A8A5CC4E9F4BD1DF73A16D186058548B539E_H
#define SECT239K1HOLDER_TED78A8A5CC4E9F4BD1DF73A16D186058548B539E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecT239K1Holder
struct  SecT239K1Holder_tED78A8A5CC4E9F4BD1DF73A16D186058548B539E  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct SecT239K1Holder_tED78A8A5CC4E9F4BD1DF73A16D186058548B539E_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecT239K1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(SecT239K1Holder_tED78A8A5CC4E9F4BD1DF73A16D186058548B539E_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT239K1HOLDER_TED78A8A5CC4E9F4BD1DF73A16D186058548B539E_H
#ifndef SECT283K1HOLDER_TA05B2F0A2D147F514B887C906D2D3B04CCCA714A_H
#define SECT283K1HOLDER_TA05B2F0A2D147F514B887C906D2D3B04CCCA714A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecT283K1Holder
struct  SecT283K1Holder_tA05B2F0A2D147F514B887C906D2D3B04CCCA714A  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct SecT283K1Holder_tA05B2F0A2D147F514B887C906D2D3B04CCCA714A_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecT283K1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(SecT283K1Holder_tA05B2F0A2D147F514B887C906D2D3B04CCCA714A_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT283K1HOLDER_TA05B2F0A2D147F514B887C906D2D3B04CCCA714A_H
#ifndef SECT283R1HOLDER_T94C45D625CF019BCD0AA54226E8AC39D2CCC6410_H
#define SECT283R1HOLDER_T94C45D625CF019BCD0AA54226E8AC39D2CCC6410_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecT283R1Holder
struct  SecT283R1Holder_t94C45D625CF019BCD0AA54226E8AC39D2CCC6410  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct SecT283R1Holder_t94C45D625CF019BCD0AA54226E8AC39D2CCC6410_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecT283R1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(SecT283R1Holder_t94C45D625CF019BCD0AA54226E8AC39D2CCC6410_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT283R1HOLDER_T94C45D625CF019BCD0AA54226E8AC39D2CCC6410_H
#ifndef SECT409K1HOLDER_TDFA985A4493E8A407B6A3824B3F4B0FB9973D136_H
#define SECT409K1HOLDER_TDFA985A4493E8A407B6A3824B3F4B0FB9973D136_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecT409K1Holder
struct  SecT409K1Holder_tDFA985A4493E8A407B6A3824B3F4B0FB9973D136  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct SecT409K1Holder_tDFA985A4493E8A407B6A3824B3F4B0FB9973D136_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecT409K1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(SecT409K1Holder_tDFA985A4493E8A407B6A3824B3F4B0FB9973D136_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT409K1HOLDER_TDFA985A4493E8A407B6A3824B3F4B0FB9973D136_H
#ifndef SECT409R1HOLDER_T543AA73064415D52EC5842DB981CC7965C789AAE_H
#define SECT409R1HOLDER_T543AA73064415D52EC5842DB981CC7965C789AAE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecT409R1Holder
struct  SecT409R1Holder_t543AA73064415D52EC5842DB981CC7965C789AAE  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct SecT409R1Holder_t543AA73064415D52EC5842DB981CC7965C789AAE_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecT409R1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(SecT409R1Holder_t543AA73064415D52EC5842DB981CC7965C789AAE_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT409R1HOLDER_T543AA73064415D52EC5842DB981CC7965C789AAE_H
#ifndef SECT571K1HOLDER_T4150E630B7BC0A4DF9ECC9EC64AB909E6E69E487_H
#define SECT571K1HOLDER_T4150E630B7BC0A4DF9ECC9EC64AB909E6E69E487_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecT571K1Holder
struct  SecT571K1Holder_t4150E630B7BC0A4DF9ECC9EC64AB909E6E69E487  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct SecT571K1Holder_t4150E630B7BC0A4DF9ECC9EC64AB909E6E69E487_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecT571K1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(SecT571K1Holder_t4150E630B7BC0A4DF9ECC9EC64AB909E6E69E487_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT571K1HOLDER_T4150E630B7BC0A4DF9ECC9EC64AB909E6E69E487_H
#ifndef SECT571R1HOLDER_T60AB12FF555AD7B4BE6FF459642EB0079CFC5A5E_H
#define SECT571R1HOLDER_T60AB12FF555AD7B4BE6FF459642EB0079CFC5A5E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecT571R1Holder
struct  SecT571R1Holder_t60AB12FF555AD7B4BE6FF459642EB0079CFC5A5E  : public X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE
{
public:

public:
};

struct SecT571R1Holder_t60AB12FF555AD7B4BE6FF459642EB0079CFC5A5E_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.X9.X9ECParametersHolder Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecT571R1Holder::Instance
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(SecT571R1Holder_t60AB12FF555AD7B4BE6FF459642EB0079CFC5A5E_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT571R1HOLDER_T60AB12FF555AD7B4BE6FF459642EB0079CFC5A5E_H
#ifndef CHACHAENGINE_T6CC53FC5D18C19BC24AF41B6A6D0B073CA6A36B2_H
#define CHACHAENGINE_T6CC53FC5D18C19BC24AF41B6A6D0B073CA6A36B2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Engines.ChaChaEngine
struct  ChaChaEngine_t6CC53FC5D18C19BC24AF41B6A6D0B073CA6A36B2  : public Salsa20Engine_t5346D9847A884ABE981EC15CAEC9A2E09A6B4E5C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHACHAENGINE_T6CC53FC5D18C19BC24AF41B6A6D0B073CA6A36B2_H
#ifndef DESEDEENGINE_T08325DC827B79EB29E302109606CA98232397B25_H
#define DESEDEENGINE_T08325DC827B79EB29E302109606CA98232397B25_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Engines.DesEdeEngine
struct  DesEdeEngine_t08325DC827B79EB29E302109606CA98232397B25  : public DesEngine_t88E9C8F289740CDAE4C6C0EAEABA2059935EF450
{
public:
	// System.Int32[] Org.BouncyCastle.Crypto.Engines.DesEdeEngine::workingKey1
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___workingKey1_14;
	// System.Int32[] Org.BouncyCastle.Crypto.Engines.DesEdeEngine::workingKey2
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___workingKey2_15;
	// System.Int32[] Org.BouncyCastle.Crypto.Engines.DesEdeEngine::workingKey3
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___workingKey3_16;
	// System.Boolean Org.BouncyCastle.Crypto.Engines.DesEdeEngine::forEncryption
	bool ___forEncryption_17;

public:
	inline static int32_t get_offset_of_workingKey1_14() { return static_cast<int32_t>(offsetof(DesEdeEngine_t08325DC827B79EB29E302109606CA98232397B25, ___workingKey1_14)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_workingKey1_14() const { return ___workingKey1_14; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_workingKey1_14() { return &___workingKey1_14; }
	inline void set_workingKey1_14(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___workingKey1_14 = value;
		Il2CppCodeGenWriteBarrier((&___workingKey1_14), value);
	}

	inline static int32_t get_offset_of_workingKey2_15() { return static_cast<int32_t>(offsetof(DesEdeEngine_t08325DC827B79EB29E302109606CA98232397B25, ___workingKey2_15)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_workingKey2_15() const { return ___workingKey2_15; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_workingKey2_15() { return &___workingKey2_15; }
	inline void set_workingKey2_15(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___workingKey2_15 = value;
		Il2CppCodeGenWriteBarrier((&___workingKey2_15), value);
	}

	inline static int32_t get_offset_of_workingKey3_16() { return static_cast<int32_t>(offsetof(DesEdeEngine_t08325DC827B79EB29E302109606CA98232397B25, ___workingKey3_16)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_workingKey3_16() const { return ___workingKey3_16; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_workingKey3_16() { return &___workingKey3_16; }
	inline void set_workingKey3_16(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___workingKey3_16 = value;
		Il2CppCodeGenWriteBarrier((&___workingKey3_16), value);
	}

	inline static int32_t get_offset_of_forEncryption_17() { return static_cast<int32_t>(offsetof(DesEdeEngine_t08325DC827B79EB29E302109606CA98232397B25, ___forEncryption_17)); }
	inline bool get_forEncryption_17() const { return ___forEncryption_17; }
	inline bool* get_address_of_forEncryption_17() { return &___forEncryption_17; }
	inline void set_forEncryption_17(bool value)
	{
		___forEncryption_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESEDEENGINE_T08325DC827B79EB29E302109606CA98232397B25_H
#ifndef DATALENGTHEXCEPTION_T540F65271B6646C685F96E53492AC3DEF11E3346_H
#define DATALENGTHEXCEPTION_T540F65271B6646C685F96E53492AC3DEF11E3346_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.DataLengthException
struct  DataLengthException_t540F65271B6646C685F96E53492AC3DEF11E3346  : public CryptoException_tD693C94FBD529A1E01A8DFE8D03A039EA27CFB8C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATALENGTHEXCEPTION_T540F65271B6646C685F96E53492AC3DEF11E3346_H
#ifndef INVALIDCIPHERTEXTEXCEPTION_T070D1031C2583A6F9C4BA67B5961FDC28E61BC07_H
#define INVALIDCIPHERTEXTEXCEPTION_T070D1031C2583A6F9C4BA67B5961FDC28E61BC07_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.InvalidCipherTextException
struct  InvalidCipherTextException_t070D1031C2583A6F9C4BA67B5961FDC28E61BC07  : public CryptoException_tD693C94FBD529A1E01A8DFE8D03A039EA27CFB8C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVALIDCIPHERTEXTEXCEPTION_T070D1031C2583A6F9C4BA67B5961FDC28E61BC07_H
#ifndef MAXBYTESEXCEEDEDEXCEPTION_T24004EE9E969BBCAE4EFE62ADCCBCD4549A75D10_H
#define MAXBYTESEXCEEDEDEXCEPTION_T24004EE9E969BBCAE4EFE62ADCCBCD4549A75D10_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.MaxBytesExceededException
struct  MaxBytesExceededException_t24004EE9E969BBCAE4EFE62ADCCBCD4549A75D10  : public CryptoException_tD693C94FBD529A1E01A8DFE8D03A039EA27CFB8C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAXBYTESEXCEEDEDEXCEPTION_T24004EE9E969BBCAE4EFE62ADCCBCD4549A75D10_H
#ifndef OUTPUTLENGTHEXCEPTION_T496A3DBAA66ABC16AF714D203667993600F513CC_H
#define OUTPUTLENGTHEXCEPTION_T496A3DBAA66ABC16AF714D203667993600F513CC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.OutputLengthException
struct  OutputLengthException_t496A3DBAA66ABC16AF714D203667993600F513CC  : public DataLengthException_t540F65271B6646C685F96E53492AC3DEF11E3346
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OUTPUTLENGTHEXCEPTION_T496A3DBAA66ABC16AF714D203667993600F513CC_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5000 = { sizeof (C2pnb304w1Holder_t6A9A5917DB2CA3D55130781A13C8D44F3E1C5006), -1, sizeof(C2pnb304w1Holder_t6A9A5917DB2CA3D55130781A13C8D44F3E1C5006_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5000[1] = 
{
	C2pnb304w1Holder_t6A9A5917DB2CA3D55130781A13C8D44F3E1C5006_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5001 = { sizeof (C2tnb359v1Holder_t8B93223E821CADB44BDBCE85BCD4077EE4888861), -1, sizeof(C2tnb359v1Holder_t8B93223E821CADB44BDBCE85BCD4077EE4888861_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5001[1] = 
{
	C2tnb359v1Holder_t8B93223E821CADB44BDBCE85BCD4077EE4888861_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5002 = { sizeof (C2pnb368w1Holder_t5052AD1B0C8BBC973E159B7672DFB7DBCD04978F), -1, sizeof(C2pnb368w1Holder_t5052AD1B0C8BBC973E159B7672DFB7DBCD04978F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5002[1] = 
{
	C2pnb368w1Holder_t5052AD1B0C8BBC973E159B7672DFB7DBCD04978F_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5003 = { sizeof (C2tnb431r1Holder_t6504DEEFE40EDE731560CC455FE297CB7A4FAFAC), -1, sizeof(C2tnb431r1Holder_t6504DEEFE40EDE731560CC455FE297CB7A4FAFAC_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5003[1] = 
{
	C2tnb431r1Holder_t6504DEEFE40EDE731560CC455FE297CB7A4FAFAC_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5004 = { sizeof (X962Parameters_t31C99000F1443EF4D6AC7977CD4F676FAB2AD4E0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5004[1] = 
{
	X962Parameters_t31C99000F1443EF4D6AC7977CD4F676FAB2AD4E0::get_offset_of__params_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5005 = { sizeof (X9Curve_t6E9B5BFAE7A0958585B3B6112DAE30405F1EFCD8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5005[3] = 
{
	X9Curve_t6E9B5BFAE7A0958585B3B6112DAE30405F1EFCD8::get_offset_of_curve_0(),
	X9Curve_t6E9B5BFAE7A0958585B3B6112DAE30405F1EFCD8::get_offset_of_seed_1(),
	X9Curve_t6E9B5BFAE7A0958585B3B6112DAE30405F1EFCD8::get_offset_of_fieldIdentifier_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5006 = { sizeof (X9ECParameters_t818A7DD4D13D91F0D36622C6C117D7E9A10E51C3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5006[6] = 
{
	X9ECParameters_t818A7DD4D13D91F0D36622C6C117D7E9A10E51C3::get_offset_of_fieldID_0(),
	X9ECParameters_t818A7DD4D13D91F0D36622C6C117D7E9A10E51C3::get_offset_of_curve_1(),
	X9ECParameters_t818A7DD4D13D91F0D36622C6C117D7E9A10E51C3::get_offset_of_g_2(),
	X9ECParameters_t818A7DD4D13D91F0D36622C6C117D7E9A10E51C3::get_offset_of_n_3(),
	X9ECParameters_t818A7DD4D13D91F0D36622C6C117D7E9A10E51C3::get_offset_of_h_4(),
	X9ECParameters_t818A7DD4D13D91F0D36622C6C117D7E9A10E51C3::get_offset_of_seed_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5007 = { sizeof (X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5007[1] = 
{
	X9ECParametersHolder_tB46A7EA8C00B910EDFCF0F5BCC4DE614C0DF39CE::get_offset_of_parameters_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5008 = { sizeof (X9ECPoint_t0FFE1DD3974DC6DE1C0428FDD84A3FF10D29F65A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5008[3] = 
{
	X9ECPoint_t0FFE1DD3974DC6DE1C0428FDD84A3FF10D29F65A::get_offset_of_encoding_0(),
	X9ECPoint_t0FFE1DD3974DC6DE1C0428FDD84A3FF10D29F65A::get_offset_of_c_1(),
	X9ECPoint_t0FFE1DD3974DC6DE1C0428FDD84A3FF10D29F65A::get_offset_of_p_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5009 = { sizeof (X9FieldElement_t90E143BC3E2319C2E326CA02BC3B0EBC8807660E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5009[1] = 
{
	X9FieldElement_t90E143BC3E2319C2E326CA02BC3B0EBC8807660E::get_offset_of_f_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5010 = { sizeof (X9FieldID_t8A4FF1A99F5C9431C28964FA64FD3032C9A64019), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5010[2] = 
{
	X9FieldID_t8A4FF1A99F5C9431C28964FA64FD3032C9A64019::get_offset_of_id_0(),
	X9FieldID_t8A4FF1A99F5C9431C28964FA64FD3032C9A64019::get_offset_of_parameters_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5011 = { sizeof (X9IntegerConverter_tF9DC48605C76F129105AE178024F07081EC6472E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5012 = { sizeof (X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B), -1, sizeof(X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5012[63] = 
{
	X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields::get_offset_of_ansi_X9_62_0(),
	X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields::get_offset_of_IdFieldType_1(),
	X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields::get_offset_of_PrimeField_2(),
	X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields::get_offset_of_CharacteristicTwoField_3(),
	X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields::get_offset_of_GNBasis_4(),
	X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields::get_offset_of_TPBasis_5(),
	X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields::get_offset_of_PPBasis_6(),
	X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields::get_offset_of_id_ecSigType_7(),
	X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields::get_offset_of_ECDsaWithSha1_8(),
	X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields::get_offset_of_id_publicKeyType_9(),
	X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields::get_offset_of_IdECPublicKey_10(),
	X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields::get_offset_of_ECDsaWithSha2_11(),
	X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields::get_offset_of_ECDsaWithSha224_12(),
	X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields::get_offset_of_ECDsaWithSha256_13(),
	X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields::get_offset_of_ECDsaWithSha384_14(),
	X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields::get_offset_of_ECDsaWithSha512_15(),
	X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields::get_offset_of_EllipticCurve_16(),
	X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields::get_offset_of_CTwoCurve_17(),
	X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields::get_offset_of_C2Pnb163v1_18(),
	X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields::get_offset_of_C2Pnb163v2_19(),
	X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields::get_offset_of_C2Pnb163v3_20(),
	X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields::get_offset_of_C2Pnb176w1_21(),
	X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields::get_offset_of_C2Tnb191v1_22(),
	X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields::get_offset_of_C2Tnb191v2_23(),
	X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields::get_offset_of_C2Tnb191v3_24(),
	X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields::get_offset_of_C2Onb191v4_25(),
	X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields::get_offset_of_C2Onb191v5_26(),
	X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields::get_offset_of_C2Pnb208w1_27(),
	X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields::get_offset_of_C2Tnb239v1_28(),
	X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields::get_offset_of_C2Tnb239v2_29(),
	X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields::get_offset_of_C2Tnb239v3_30(),
	X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields::get_offset_of_C2Onb239v4_31(),
	X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields::get_offset_of_C2Onb239v5_32(),
	X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields::get_offset_of_C2Pnb272w1_33(),
	X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields::get_offset_of_C2Pnb304w1_34(),
	X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields::get_offset_of_C2Tnb359v1_35(),
	X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields::get_offset_of_C2Pnb368w1_36(),
	X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields::get_offset_of_C2Tnb431r1_37(),
	X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields::get_offset_of_PrimeCurve_38(),
	X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields::get_offset_of_Prime192v1_39(),
	X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields::get_offset_of_Prime192v2_40(),
	X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields::get_offset_of_Prime192v3_41(),
	X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields::get_offset_of_Prime239v1_42(),
	X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields::get_offset_of_Prime239v2_43(),
	X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields::get_offset_of_Prime239v3_44(),
	X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields::get_offset_of_Prime256v1_45(),
	X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields::get_offset_of_IdDsa_46(),
	X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields::get_offset_of_IdDsaWithSha1_47(),
	X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields::get_offset_of_X9x63Scheme_48(),
	X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields::get_offset_of_DHSinglePassStdDHSha1KdfScheme_49(),
	X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields::get_offset_of_DHSinglePassCofactorDHSha1KdfScheme_50(),
	X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields::get_offset_of_MqvSinglePassSha1KdfScheme_51(),
	X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields::get_offset_of_ansi_x9_42_52(),
	X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields::get_offset_of_DHPublicNumber_53(),
	X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields::get_offset_of_X9x42Schemes_54(),
	X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields::get_offset_of_DHStatic_55(),
	X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields::get_offset_of_DHEphem_56(),
	X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields::get_offset_of_DHOneFlow_57(),
	X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields::get_offset_of_DHHybrid1_58(),
	X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields::get_offset_of_DHHybrid2_59(),
	X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields::get_offset_of_DHHybridOneFlow_60(),
	X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields::get_offset_of_Mqv2_61(),
	X9ObjectIdentifiers_t3BFFC978B3484C8521DAD8FFEF79B2FCCB61503B_StaticFields::get_offset_of_Mqv1_62(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5013 = { sizeof (AsymmetricCipherKeyPair_t96A8B415347C6BD7702CCBB377729ADF4E3E8342), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5013[2] = 
{
	AsymmetricCipherKeyPair_t96A8B415347C6BD7702CCBB377729ADF4E3E8342::get_offset_of_publicParameter_0(),
	AsymmetricCipherKeyPair_t96A8B415347C6BD7702CCBB377729ADF4E3E8342::get_offset_of_privateParameter_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5014 = { sizeof (AsymmetricKeyParameter_tD6918D64A0FB774AF79E386A85415CE53D5EC0D7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5014[1] = 
{
	AsymmetricKeyParameter_tD6918D64A0FB774AF79E386A85415CE53D5EC0D7::get_offset_of_privateKey_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5015 = { sizeof (Check_t6DF797A6EACE96C6B22C06FAE9A43E0EF147D0D1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5016 = { sizeof (CipherKeyGenerator_tB9597B68C6815C63C622AA3320A0E81BF8568CE1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5017 = { sizeof (CryptoException_tD693C94FBD529A1E01A8DFE8D03A039EA27CFB8C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5018 = { sizeof (DataLengthException_t540F65271B6646C685F96E53492AC3DEF11E3346), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5019 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5020 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5021 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5022 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5023 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5024 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5025 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5026 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5027 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5028 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5029 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5030 = { sizeof (InvalidCipherTextException_t070D1031C2583A6F9C4BA67B5961FDC28E61BC07), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5031 = { sizeof (KeyGenerationParameters_tDA1E9297A6F944BA4FF44DBC3E94089DB7F7D2B6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5031[2] = 
{
	KeyGenerationParameters_tDA1E9297A6F944BA4FF44DBC3E94089DB7F7D2B6::get_offset_of_random_0(),
	KeyGenerationParameters_tDA1E9297A6F944BA4FF44DBC3E94089DB7F7D2B6::get_offset_of_strength_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5032 = { sizeof (MaxBytesExceededException_t24004EE9E969BBCAE4EFE62ADCCBCD4549A75D10), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5033 = { sizeof (OutputLengthException_t496A3DBAA66ABC16AF714D203667993600F513CC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5034 = { sizeof (DHBasicAgreement_t6EE26230D930EE4EF01B84A796634B99F42315BA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5034[2] = 
{
	DHBasicAgreement_t6EE26230D930EE4EF01B84A796634B99F42315BA::get_offset_of_key_0(),
	DHBasicAgreement_t6EE26230D930EE4EF01B84A796634B99F42315BA::get_offset_of_dhParams_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5035 = { sizeof (ECDHBasicAgreement_t1CD435E5D2E794AAB5162C860E4D29FDE8E16527), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5035[1] = 
{
	ECDHBasicAgreement_t1CD435E5D2E794AAB5162C860E4D29FDE8E16527::get_offset_of_privKey_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5036 = { sizeof (Gost3411Digest_tB644E93B56F8C4CA364A53FE2975CDB5F29FE0AE), -1, sizeof(Gost3411Digest_tB644E93B56F8C4CA364A53FE2975CDB5F29FE0AE_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5036[19] = 
{
	Gost3411Digest_tB644E93B56F8C4CA364A53FE2975CDB5F29FE0AE::get_offset_of_H_0(),
	Gost3411Digest_tB644E93B56F8C4CA364A53FE2975CDB5F29FE0AE::get_offset_of_L_1(),
	Gost3411Digest_tB644E93B56F8C4CA364A53FE2975CDB5F29FE0AE::get_offset_of_M_2(),
	Gost3411Digest_tB644E93B56F8C4CA364A53FE2975CDB5F29FE0AE::get_offset_of_Sum_3(),
	Gost3411Digest_tB644E93B56F8C4CA364A53FE2975CDB5F29FE0AE::get_offset_of_C_4(),
	Gost3411Digest_tB644E93B56F8C4CA364A53FE2975CDB5F29FE0AE::get_offset_of_xBuf_5(),
	Gost3411Digest_tB644E93B56F8C4CA364A53FE2975CDB5F29FE0AE::get_offset_of_xBufOff_6(),
	Gost3411Digest_tB644E93B56F8C4CA364A53FE2975CDB5F29FE0AE::get_offset_of_byteCount_7(),
	Gost3411Digest_tB644E93B56F8C4CA364A53FE2975CDB5F29FE0AE::get_offset_of_cipher_8(),
	Gost3411Digest_tB644E93B56F8C4CA364A53FE2975CDB5F29FE0AE::get_offset_of_sBox_9(),
	Gost3411Digest_tB644E93B56F8C4CA364A53FE2975CDB5F29FE0AE::get_offset_of_K_10(),
	Gost3411Digest_tB644E93B56F8C4CA364A53FE2975CDB5F29FE0AE::get_offset_of_a_11(),
	Gost3411Digest_tB644E93B56F8C4CA364A53FE2975CDB5F29FE0AE::get_offset_of_wS_12(),
	Gost3411Digest_tB644E93B56F8C4CA364A53FE2975CDB5F29FE0AE::get_offset_of_w_S_13(),
	Gost3411Digest_tB644E93B56F8C4CA364A53FE2975CDB5F29FE0AE::get_offset_of_S_14(),
	Gost3411Digest_tB644E93B56F8C4CA364A53FE2975CDB5F29FE0AE::get_offset_of_U_15(),
	Gost3411Digest_tB644E93B56F8C4CA364A53FE2975CDB5F29FE0AE::get_offset_of_V_16(),
	Gost3411Digest_tB644E93B56F8C4CA364A53FE2975CDB5F29FE0AE::get_offset_of_W_17(),
	Gost3411Digest_tB644E93B56F8C4CA364A53FE2975CDB5F29FE0AE_StaticFields::get_offset_of_C2_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5037 = { sizeof (GeneralDigest_t3B10A5BF867429F885F41CE318BA1665427D50D6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5037[3] = 
{
	GeneralDigest_t3B10A5BF867429F885F41CE318BA1665427D50D6::get_offset_of_xBuf_0(),
	GeneralDigest_t3B10A5BF867429F885F41CE318BA1665427D50D6::get_offset_of_xBufOff_1(),
	GeneralDigest_t3B10A5BF867429F885F41CE318BA1665427D50D6::get_offset_of_byteCount_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5038 = { sizeof (KeccakDigest_tEFB4CFB19A2FC24160E02A8821D4BAE8862E3AEB), -1, sizeof(KeccakDigest_tEFB4CFB19A2FC24160E02A8821D4BAE8862E3AEB_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5038[14] = 
{
	KeccakDigest_tEFB4CFB19A2FC24160E02A8821D4BAE8862E3AEB_StaticFields::get_offset_of_KeccakRoundConstants_0(),
	KeccakDigest_tEFB4CFB19A2FC24160E02A8821D4BAE8862E3AEB_StaticFields::get_offset_of_KeccakRhoOffsets_1(),
	KeccakDigest_tEFB4CFB19A2FC24160E02A8821D4BAE8862E3AEB::get_offset_of_state_2(),
	KeccakDigest_tEFB4CFB19A2FC24160E02A8821D4BAE8862E3AEB::get_offset_of_dataQueue_3(),
	KeccakDigest_tEFB4CFB19A2FC24160E02A8821D4BAE8862E3AEB::get_offset_of_rate_4(),
	KeccakDigest_tEFB4CFB19A2FC24160E02A8821D4BAE8862E3AEB::get_offset_of_bitsInQueue_5(),
	KeccakDigest_tEFB4CFB19A2FC24160E02A8821D4BAE8862E3AEB::get_offset_of_fixedOutputLength_6(),
	KeccakDigest_tEFB4CFB19A2FC24160E02A8821D4BAE8862E3AEB::get_offset_of_squeezing_7(),
	KeccakDigest_tEFB4CFB19A2FC24160E02A8821D4BAE8862E3AEB::get_offset_of_bitsAvailableForSqueezing_8(),
	KeccakDigest_tEFB4CFB19A2FC24160E02A8821D4BAE8862E3AEB::get_offset_of_chunk_9(),
	KeccakDigest_tEFB4CFB19A2FC24160E02A8821D4BAE8862E3AEB::get_offset_of_oneByte_10(),
	KeccakDigest_tEFB4CFB19A2FC24160E02A8821D4BAE8862E3AEB::get_offset_of_C_11(),
	KeccakDigest_tEFB4CFB19A2FC24160E02A8821D4BAE8862E3AEB::get_offset_of_tempA_12(),
	KeccakDigest_tEFB4CFB19A2FC24160E02A8821D4BAE8862E3AEB::get_offset_of_chiC_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5039 = { sizeof (LongDigest_t043705804892FF0397DAA2E061CCF2DE8BA8BB38), -1, sizeof(LongDigest_t043705804892FF0397DAA2E061CCF2DE8BA8BB38_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5039[16] = 
{
	LongDigest_t043705804892FF0397DAA2E061CCF2DE8BA8BB38::get_offset_of_MyByteLength_0(),
	LongDigest_t043705804892FF0397DAA2E061CCF2DE8BA8BB38::get_offset_of_xBuf_1(),
	LongDigest_t043705804892FF0397DAA2E061CCF2DE8BA8BB38::get_offset_of_xBufOff_2(),
	LongDigest_t043705804892FF0397DAA2E061CCF2DE8BA8BB38::get_offset_of_byteCount1_3(),
	LongDigest_t043705804892FF0397DAA2E061CCF2DE8BA8BB38::get_offset_of_byteCount2_4(),
	LongDigest_t043705804892FF0397DAA2E061CCF2DE8BA8BB38::get_offset_of_H1_5(),
	LongDigest_t043705804892FF0397DAA2E061CCF2DE8BA8BB38::get_offset_of_H2_6(),
	LongDigest_t043705804892FF0397DAA2E061CCF2DE8BA8BB38::get_offset_of_H3_7(),
	LongDigest_t043705804892FF0397DAA2E061CCF2DE8BA8BB38::get_offset_of_H4_8(),
	LongDigest_t043705804892FF0397DAA2E061CCF2DE8BA8BB38::get_offset_of_H5_9(),
	LongDigest_t043705804892FF0397DAA2E061CCF2DE8BA8BB38::get_offset_of_H6_10(),
	LongDigest_t043705804892FF0397DAA2E061CCF2DE8BA8BB38::get_offset_of_H7_11(),
	LongDigest_t043705804892FF0397DAA2E061CCF2DE8BA8BB38::get_offset_of_H8_12(),
	LongDigest_t043705804892FF0397DAA2E061CCF2DE8BA8BB38::get_offset_of_W_13(),
	LongDigest_t043705804892FF0397DAA2E061CCF2DE8BA8BB38::get_offset_of_wOff_14(),
	LongDigest_t043705804892FF0397DAA2E061CCF2DE8BA8BB38_StaticFields::get_offset_of_K_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5040 = { sizeof (MD2Digest_tC26D7F8500823CAB8729DD2D58C59A59173485CE), -1, sizeof(MD2Digest_tC26D7F8500823CAB8729DD2D58C59A59173485CE_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5040[7] = 
{
	MD2Digest_tC26D7F8500823CAB8729DD2D58C59A59173485CE::get_offset_of_X_0(),
	MD2Digest_tC26D7F8500823CAB8729DD2D58C59A59173485CE::get_offset_of_xOff_1(),
	MD2Digest_tC26D7F8500823CAB8729DD2D58C59A59173485CE::get_offset_of_M_2(),
	MD2Digest_tC26D7F8500823CAB8729DD2D58C59A59173485CE::get_offset_of_mOff_3(),
	MD2Digest_tC26D7F8500823CAB8729DD2D58C59A59173485CE::get_offset_of_C_4(),
	MD2Digest_tC26D7F8500823CAB8729DD2D58C59A59173485CE::get_offset_of_COff_5(),
	MD2Digest_tC26D7F8500823CAB8729DD2D58C59A59173485CE_StaticFields::get_offset_of_S_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5041 = { sizeof (MD4Digest_tF9D2B79B358121F811BC66B2BED64550FE6F3A29), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5041[6] = 
{
	MD4Digest_tF9D2B79B358121F811BC66B2BED64550FE6F3A29::get_offset_of_H1_3(),
	MD4Digest_tF9D2B79B358121F811BC66B2BED64550FE6F3A29::get_offset_of_H2_4(),
	MD4Digest_tF9D2B79B358121F811BC66B2BED64550FE6F3A29::get_offset_of_H3_5(),
	MD4Digest_tF9D2B79B358121F811BC66B2BED64550FE6F3A29::get_offset_of_H4_6(),
	MD4Digest_tF9D2B79B358121F811BC66B2BED64550FE6F3A29::get_offset_of_X_7(),
	MD4Digest_tF9D2B79B358121F811BC66B2BED64550FE6F3A29::get_offset_of_xOff_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5042 = { sizeof (MD5Digest_t647344D0C318D22F61BD117B16ED64CA6E458FBA), -1, sizeof(MD5Digest_t647344D0C318D22F61BD117B16ED64CA6E458FBA_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5042[22] = 
{
	MD5Digest_t647344D0C318D22F61BD117B16ED64CA6E458FBA::get_offset_of_H1_3(),
	MD5Digest_t647344D0C318D22F61BD117B16ED64CA6E458FBA::get_offset_of_H2_4(),
	MD5Digest_t647344D0C318D22F61BD117B16ED64CA6E458FBA::get_offset_of_H3_5(),
	MD5Digest_t647344D0C318D22F61BD117B16ED64CA6E458FBA::get_offset_of_H4_6(),
	MD5Digest_t647344D0C318D22F61BD117B16ED64CA6E458FBA::get_offset_of_X_7(),
	MD5Digest_t647344D0C318D22F61BD117B16ED64CA6E458FBA::get_offset_of_xOff_8(),
	MD5Digest_t647344D0C318D22F61BD117B16ED64CA6E458FBA_StaticFields::get_offset_of_S11_9(),
	MD5Digest_t647344D0C318D22F61BD117B16ED64CA6E458FBA_StaticFields::get_offset_of_S12_10(),
	MD5Digest_t647344D0C318D22F61BD117B16ED64CA6E458FBA_StaticFields::get_offset_of_S13_11(),
	MD5Digest_t647344D0C318D22F61BD117B16ED64CA6E458FBA_StaticFields::get_offset_of_S14_12(),
	MD5Digest_t647344D0C318D22F61BD117B16ED64CA6E458FBA_StaticFields::get_offset_of_S21_13(),
	MD5Digest_t647344D0C318D22F61BD117B16ED64CA6E458FBA_StaticFields::get_offset_of_S22_14(),
	MD5Digest_t647344D0C318D22F61BD117B16ED64CA6E458FBA_StaticFields::get_offset_of_S23_15(),
	MD5Digest_t647344D0C318D22F61BD117B16ED64CA6E458FBA_StaticFields::get_offset_of_S24_16(),
	MD5Digest_t647344D0C318D22F61BD117B16ED64CA6E458FBA_StaticFields::get_offset_of_S31_17(),
	MD5Digest_t647344D0C318D22F61BD117B16ED64CA6E458FBA_StaticFields::get_offset_of_S32_18(),
	MD5Digest_t647344D0C318D22F61BD117B16ED64CA6E458FBA_StaticFields::get_offset_of_S33_19(),
	MD5Digest_t647344D0C318D22F61BD117B16ED64CA6E458FBA_StaticFields::get_offset_of_S34_20(),
	MD5Digest_t647344D0C318D22F61BD117B16ED64CA6E458FBA_StaticFields::get_offset_of_S41_21(),
	MD5Digest_t647344D0C318D22F61BD117B16ED64CA6E458FBA_StaticFields::get_offset_of_S42_22(),
	MD5Digest_t647344D0C318D22F61BD117B16ED64CA6E458FBA_StaticFields::get_offset_of_S43_23(),
	MD5Digest_t647344D0C318D22F61BD117B16ED64CA6E458FBA_StaticFields::get_offset_of_S44_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5043 = { sizeof (NullDigest_t0E2AA2F965790ED3744B24CB685F4C0EE80D259A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5043[1] = 
{
	NullDigest_t0E2AA2F965790ED3744B24CB685F4C0EE80D259A::get_offset_of_bOut_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5044 = { sizeof (RipeMD128Digest_t641DBF9D8CB0F3E0FCD072972F4FA6F9EA6FEA71), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5044[6] = 
{
	RipeMD128Digest_t641DBF9D8CB0F3E0FCD072972F4FA6F9EA6FEA71::get_offset_of_H0_3(),
	RipeMD128Digest_t641DBF9D8CB0F3E0FCD072972F4FA6F9EA6FEA71::get_offset_of_H1_4(),
	RipeMD128Digest_t641DBF9D8CB0F3E0FCD072972F4FA6F9EA6FEA71::get_offset_of_H2_5(),
	RipeMD128Digest_t641DBF9D8CB0F3E0FCD072972F4FA6F9EA6FEA71::get_offset_of_H3_6(),
	RipeMD128Digest_t641DBF9D8CB0F3E0FCD072972F4FA6F9EA6FEA71::get_offset_of_X_7(),
	RipeMD128Digest_t641DBF9D8CB0F3E0FCD072972F4FA6F9EA6FEA71::get_offset_of_xOff_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5045 = { sizeof (RipeMD160Digest_t9812101E248DBEBA8B8ECB8DEE92F00530C0D74A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5045[7] = 
{
	RipeMD160Digest_t9812101E248DBEBA8B8ECB8DEE92F00530C0D74A::get_offset_of_H0_3(),
	RipeMD160Digest_t9812101E248DBEBA8B8ECB8DEE92F00530C0D74A::get_offset_of_H1_4(),
	RipeMD160Digest_t9812101E248DBEBA8B8ECB8DEE92F00530C0D74A::get_offset_of_H2_5(),
	RipeMD160Digest_t9812101E248DBEBA8B8ECB8DEE92F00530C0D74A::get_offset_of_H3_6(),
	RipeMD160Digest_t9812101E248DBEBA8B8ECB8DEE92F00530C0D74A::get_offset_of_H4_7(),
	RipeMD160Digest_t9812101E248DBEBA8B8ECB8DEE92F00530C0D74A::get_offset_of_X_8(),
	RipeMD160Digest_t9812101E248DBEBA8B8ECB8DEE92F00530C0D74A::get_offset_of_xOff_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5046 = { sizeof (RipeMD256Digest_t561D57139D5F60FE7160BD17668889024BA16914), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5046[10] = 
{
	RipeMD256Digest_t561D57139D5F60FE7160BD17668889024BA16914::get_offset_of_H0_3(),
	RipeMD256Digest_t561D57139D5F60FE7160BD17668889024BA16914::get_offset_of_H1_4(),
	RipeMD256Digest_t561D57139D5F60FE7160BD17668889024BA16914::get_offset_of_H2_5(),
	RipeMD256Digest_t561D57139D5F60FE7160BD17668889024BA16914::get_offset_of_H3_6(),
	RipeMD256Digest_t561D57139D5F60FE7160BD17668889024BA16914::get_offset_of_H4_7(),
	RipeMD256Digest_t561D57139D5F60FE7160BD17668889024BA16914::get_offset_of_H5_8(),
	RipeMD256Digest_t561D57139D5F60FE7160BD17668889024BA16914::get_offset_of_H6_9(),
	RipeMD256Digest_t561D57139D5F60FE7160BD17668889024BA16914::get_offset_of_H7_10(),
	RipeMD256Digest_t561D57139D5F60FE7160BD17668889024BA16914::get_offset_of_X_11(),
	RipeMD256Digest_t561D57139D5F60FE7160BD17668889024BA16914::get_offset_of_xOff_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5047 = { sizeof (RipeMD320Digest_t7B5823A351529F50FED8E65C36751DD9F1A75DBF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5047[12] = 
{
	RipeMD320Digest_t7B5823A351529F50FED8E65C36751DD9F1A75DBF::get_offset_of_H0_3(),
	RipeMD320Digest_t7B5823A351529F50FED8E65C36751DD9F1A75DBF::get_offset_of_H1_4(),
	RipeMD320Digest_t7B5823A351529F50FED8E65C36751DD9F1A75DBF::get_offset_of_H2_5(),
	RipeMD320Digest_t7B5823A351529F50FED8E65C36751DD9F1A75DBF::get_offset_of_H3_6(),
	RipeMD320Digest_t7B5823A351529F50FED8E65C36751DD9F1A75DBF::get_offset_of_H4_7(),
	RipeMD320Digest_t7B5823A351529F50FED8E65C36751DD9F1A75DBF::get_offset_of_H5_8(),
	RipeMD320Digest_t7B5823A351529F50FED8E65C36751DD9F1A75DBF::get_offset_of_H6_9(),
	RipeMD320Digest_t7B5823A351529F50FED8E65C36751DD9F1A75DBF::get_offset_of_H7_10(),
	RipeMD320Digest_t7B5823A351529F50FED8E65C36751DD9F1A75DBF::get_offset_of_H8_11(),
	RipeMD320Digest_t7B5823A351529F50FED8E65C36751DD9F1A75DBF::get_offset_of_H9_12(),
	RipeMD320Digest_t7B5823A351529F50FED8E65C36751DD9F1A75DBF::get_offset_of_X_13(),
	RipeMD320Digest_t7B5823A351529F50FED8E65C36751DD9F1A75DBF::get_offset_of_xOff_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5048 = { sizeof (Sha3Digest_t94D2D74FDDEC25215DC989F84319513E346D8E1E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5049 = { sizeof (Sha1Digest_t5D1533BB233BE3AE0FE89BFB36C3267722F65CD7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5049[7] = 
{
	Sha1Digest_t5D1533BB233BE3AE0FE89BFB36C3267722F65CD7::get_offset_of_H1_3(),
	Sha1Digest_t5D1533BB233BE3AE0FE89BFB36C3267722F65CD7::get_offset_of_H2_4(),
	Sha1Digest_t5D1533BB233BE3AE0FE89BFB36C3267722F65CD7::get_offset_of_H3_5(),
	Sha1Digest_t5D1533BB233BE3AE0FE89BFB36C3267722F65CD7::get_offset_of_H4_6(),
	Sha1Digest_t5D1533BB233BE3AE0FE89BFB36C3267722F65CD7::get_offset_of_H5_7(),
	Sha1Digest_t5D1533BB233BE3AE0FE89BFB36C3267722F65CD7::get_offset_of_X_8(),
	Sha1Digest_t5D1533BB233BE3AE0FE89BFB36C3267722F65CD7::get_offset_of_xOff_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5050 = { sizeof (Sha224Digest_t4055A3B2F16FB90F8B361840F96997E133B16DDF), -1, sizeof(Sha224Digest_t4055A3B2F16FB90F8B361840F96997E133B16DDF_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5050[11] = 
{
	Sha224Digest_t4055A3B2F16FB90F8B361840F96997E133B16DDF::get_offset_of_H1_3(),
	Sha224Digest_t4055A3B2F16FB90F8B361840F96997E133B16DDF::get_offset_of_H2_4(),
	Sha224Digest_t4055A3B2F16FB90F8B361840F96997E133B16DDF::get_offset_of_H3_5(),
	Sha224Digest_t4055A3B2F16FB90F8B361840F96997E133B16DDF::get_offset_of_H4_6(),
	Sha224Digest_t4055A3B2F16FB90F8B361840F96997E133B16DDF::get_offset_of_H5_7(),
	Sha224Digest_t4055A3B2F16FB90F8B361840F96997E133B16DDF::get_offset_of_H6_8(),
	Sha224Digest_t4055A3B2F16FB90F8B361840F96997E133B16DDF::get_offset_of_H7_9(),
	Sha224Digest_t4055A3B2F16FB90F8B361840F96997E133B16DDF::get_offset_of_H8_10(),
	Sha224Digest_t4055A3B2F16FB90F8B361840F96997E133B16DDF::get_offset_of_X_11(),
	Sha224Digest_t4055A3B2F16FB90F8B361840F96997E133B16DDF::get_offset_of_xOff_12(),
	Sha224Digest_t4055A3B2F16FB90F8B361840F96997E133B16DDF_StaticFields::get_offset_of_K_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5051 = { sizeof (Sha256Digest_t3F74BE4A7A03825407AA99B6E6D52E9DB5016358), -1, sizeof(Sha256Digest_t3F74BE4A7A03825407AA99B6E6D52E9DB5016358_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5051[11] = 
{
	Sha256Digest_t3F74BE4A7A03825407AA99B6E6D52E9DB5016358::get_offset_of_H1_3(),
	Sha256Digest_t3F74BE4A7A03825407AA99B6E6D52E9DB5016358::get_offset_of_H2_4(),
	Sha256Digest_t3F74BE4A7A03825407AA99B6E6D52E9DB5016358::get_offset_of_H3_5(),
	Sha256Digest_t3F74BE4A7A03825407AA99B6E6D52E9DB5016358::get_offset_of_H4_6(),
	Sha256Digest_t3F74BE4A7A03825407AA99B6E6D52E9DB5016358::get_offset_of_H5_7(),
	Sha256Digest_t3F74BE4A7A03825407AA99B6E6D52E9DB5016358::get_offset_of_H6_8(),
	Sha256Digest_t3F74BE4A7A03825407AA99B6E6D52E9DB5016358::get_offset_of_H7_9(),
	Sha256Digest_t3F74BE4A7A03825407AA99B6E6D52E9DB5016358::get_offset_of_H8_10(),
	Sha256Digest_t3F74BE4A7A03825407AA99B6E6D52E9DB5016358::get_offset_of_X_11(),
	Sha256Digest_t3F74BE4A7A03825407AA99B6E6D52E9DB5016358::get_offset_of_xOff_12(),
	Sha256Digest_t3F74BE4A7A03825407AA99B6E6D52E9DB5016358_StaticFields::get_offset_of_K_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5052 = { sizeof (Sha384Digest_t22195F1F88F9F90ECE845E368637FCFA58144D6C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5053 = { sizeof (Sha512Digest_t3E26A90EE75F594E83339CA735984309CB9B7263), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5054 = { sizeof (Sha512tDigest_tDB0EA86904BE1D448D78ACED5DB7139242E1BBDA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5054[9] = 
{
	Sha512tDigest_tDB0EA86904BE1D448D78ACED5DB7139242E1BBDA::get_offset_of_digestLength_16(),
	Sha512tDigest_tDB0EA86904BE1D448D78ACED5DB7139242E1BBDA::get_offset_of_H1t_17(),
	Sha512tDigest_tDB0EA86904BE1D448D78ACED5DB7139242E1BBDA::get_offset_of_H2t_18(),
	Sha512tDigest_tDB0EA86904BE1D448D78ACED5DB7139242E1BBDA::get_offset_of_H3t_19(),
	Sha512tDigest_tDB0EA86904BE1D448D78ACED5DB7139242E1BBDA::get_offset_of_H4t_20(),
	Sha512tDigest_tDB0EA86904BE1D448D78ACED5DB7139242E1BBDA::get_offset_of_H5t_21(),
	Sha512tDigest_tDB0EA86904BE1D448D78ACED5DB7139242E1BBDA::get_offset_of_H6t_22(),
	Sha512tDigest_tDB0EA86904BE1D448D78ACED5DB7139242E1BBDA::get_offset_of_H7t_23(),
	Sha512tDigest_tDB0EA86904BE1D448D78ACED5DB7139242E1BBDA::get_offset_of_H8t_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5055 = { sizeof (ShakeDigest_t43101B71FFF13FEE7CDF47A8393959C16BC54392), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5056 = { sizeof (TigerDigest_t65B59C08483681A97EC0475F4409DFFFC2C80D94), -1, sizeof(TigerDigest_t65B59C08483681A97EC0475F4409DFFFC2C80D94_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5056[12] = 
{
	TigerDigest_t65B59C08483681A97EC0475F4409DFFFC2C80D94_StaticFields::get_offset_of_t1_0(),
	TigerDigest_t65B59C08483681A97EC0475F4409DFFFC2C80D94_StaticFields::get_offset_of_t2_1(),
	TigerDigest_t65B59C08483681A97EC0475F4409DFFFC2C80D94_StaticFields::get_offset_of_t3_2(),
	TigerDigest_t65B59C08483681A97EC0475F4409DFFFC2C80D94_StaticFields::get_offset_of_t4_3(),
	TigerDigest_t65B59C08483681A97EC0475F4409DFFFC2C80D94::get_offset_of_a_4(),
	TigerDigest_t65B59C08483681A97EC0475F4409DFFFC2C80D94::get_offset_of_b_5(),
	TigerDigest_t65B59C08483681A97EC0475F4409DFFFC2C80D94::get_offset_of_c_6(),
	TigerDigest_t65B59C08483681A97EC0475F4409DFFFC2C80D94::get_offset_of_byteCount_7(),
	TigerDigest_t65B59C08483681A97EC0475F4409DFFFC2C80D94::get_offset_of_Buffer_8(),
	TigerDigest_t65B59C08483681A97EC0475F4409DFFFC2C80D94::get_offset_of_bOff_9(),
	TigerDigest_t65B59C08483681A97EC0475F4409DFFFC2C80D94::get_offset_of_x_10(),
	TigerDigest_t65B59C08483681A97EC0475F4409DFFFC2C80D94::get_offset_of_xOff_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5057 = { sizeof (WhirlpoolDigest_tFF37FE0BB1D89CE67CC8462C381E9303A241F821), -1, sizeof(WhirlpoolDigest_tFF37FE0BB1D89CE67CC8462C381E9303A241F821_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5057[19] = 
{
	WhirlpoolDigest_tFF37FE0BB1D89CE67CC8462C381E9303A241F821_StaticFields::get_offset_of_SBOX_0(),
	WhirlpoolDigest_tFF37FE0BB1D89CE67CC8462C381E9303A241F821_StaticFields::get_offset_of_C0_1(),
	WhirlpoolDigest_tFF37FE0BB1D89CE67CC8462C381E9303A241F821_StaticFields::get_offset_of_C1_2(),
	WhirlpoolDigest_tFF37FE0BB1D89CE67CC8462C381E9303A241F821_StaticFields::get_offset_of_C2_3(),
	WhirlpoolDigest_tFF37FE0BB1D89CE67CC8462C381E9303A241F821_StaticFields::get_offset_of_C3_4(),
	WhirlpoolDigest_tFF37FE0BB1D89CE67CC8462C381E9303A241F821_StaticFields::get_offset_of_C4_5(),
	WhirlpoolDigest_tFF37FE0BB1D89CE67CC8462C381E9303A241F821_StaticFields::get_offset_of_C5_6(),
	WhirlpoolDigest_tFF37FE0BB1D89CE67CC8462C381E9303A241F821_StaticFields::get_offset_of_C6_7(),
	WhirlpoolDigest_tFF37FE0BB1D89CE67CC8462C381E9303A241F821_StaticFields::get_offset_of_C7_8(),
	WhirlpoolDigest_tFF37FE0BB1D89CE67CC8462C381E9303A241F821::get_offset_of__rc_9(),
	WhirlpoolDigest_tFF37FE0BB1D89CE67CC8462C381E9303A241F821_StaticFields::get_offset_of_EIGHT_10(),
	WhirlpoolDigest_tFF37FE0BB1D89CE67CC8462C381E9303A241F821::get_offset_of__buffer_11(),
	WhirlpoolDigest_tFF37FE0BB1D89CE67CC8462C381E9303A241F821::get_offset_of__bufferPos_12(),
	WhirlpoolDigest_tFF37FE0BB1D89CE67CC8462C381E9303A241F821::get_offset_of__bitCount_13(),
	WhirlpoolDigest_tFF37FE0BB1D89CE67CC8462C381E9303A241F821::get_offset_of__hash_14(),
	WhirlpoolDigest_tFF37FE0BB1D89CE67CC8462C381E9303A241F821::get_offset_of__K_15(),
	WhirlpoolDigest_tFF37FE0BB1D89CE67CC8462C381E9303A241F821::get_offset_of__L_16(),
	WhirlpoolDigest_tFF37FE0BB1D89CE67CC8462C381E9303A241F821::get_offset_of__block_17(),
	WhirlpoolDigest_tFF37FE0BB1D89CE67CC8462C381E9303A241F821::get_offset_of__state_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5058 = { sizeof (CustomNamedCurves_tB73015B5EF02BC0E5C4EC0048428B7F017B5EB7B), -1, sizeof(CustomNamedCurves_tB73015B5EF02BC0E5C4EC0048428B7F017B5EB7B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5058[5] = 
{
	CustomNamedCurves_tB73015B5EF02BC0E5C4EC0048428B7F017B5EB7B_StaticFields::get_offset_of_nameToCurve_0(),
	CustomNamedCurves_tB73015B5EF02BC0E5C4EC0048428B7F017B5EB7B_StaticFields::get_offset_of_nameToOid_1(),
	CustomNamedCurves_tB73015B5EF02BC0E5C4EC0048428B7F017B5EB7B_StaticFields::get_offset_of_oidToCurve_2(),
	CustomNamedCurves_tB73015B5EF02BC0E5C4EC0048428B7F017B5EB7B_StaticFields::get_offset_of_oidToName_3(),
	CustomNamedCurves_tB73015B5EF02BC0E5C4EC0048428B7F017B5EB7B_StaticFields::get_offset_of_names_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5059 = { sizeof (Curve25519Holder_tEDC6552D2DC46135C31AAB5CAAE4C68B56FA7009), -1, sizeof(Curve25519Holder_tEDC6552D2DC46135C31AAB5CAAE4C68B56FA7009_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5059[1] = 
{
	Curve25519Holder_tEDC6552D2DC46135C31AAB5CAAE4C68B56FA7009_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5060 = { sizeof (SecP128R1Holder_tEE2D151BBFD1BBD13084C1B33283DFFA57F7F766), -1, sizeof(SecP128R1Holder_tEE2D151BBFD1BBD13084C1B33283DFFA57F7F766_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5060[1] = 
{
	SecP128R1Holder_tEE2D151BBFD1BBD13084C1B33283DFFA57F7F766_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5061 = { sizeof (SecP160K1Holder_t2189D87F11B9A00F78C1ABDD2BC8BBCD5D5AC760), -1, sizeof(SecP160K1Holder_t2189D87F11B9A00F78C1ABDD2BC8BBCD5D5AC760_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5061[1] = 
{
	SecP160K1Holder_t2189D87F11B9A00F78C1ABDD2BC8BBCD5D5AC760_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5062 = { sizeof (SecP160R1Holder_t0F1F9E9E39D021B73964428DC06846071259E075), -1, sizeof(SecP160R1Holder_t0F1F9E9E39D021B73964428DC06846071259E075_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5062[1] = 
{
	SecP160R1Holder_t0F1F9E9E39D021B73964428DC06846071259E075_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5063 = { sizeof (SecP160R2Holder_tB952668B9343E59C29CFBF2BA0FDB0F860E9A2E4), -1, sizeof(SecP160R2Holder_tB952668B9343E59C29CFBF2BA0FDB0F860E9A2E4_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5063[1] = 
{
	SecP160R2Holder_tB952668B9343E59C29CFBF2BA0FDB0F860E9A2E4_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5064 = { sizeof (SecP192K1Holder_t1854155A8EE7F5EC4366BF344522F9B049322334), -1, sizeof(SecP192K1Holder_t1854155A8EE7F5EC4366BF344522F9B049322334_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5064[1] = 
{
	SecP192K1Holder_t1854155A8EE7F5EC4366BF344522F9B049322334_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5065 = { sizeof (SecP192R1Holder_t1838B785B620A28A5D3FD6C3A5EE8D1BA5026AA0), -1, sizeof(SecP192R1Holder_t1838B785B620A28A5D3FD6C3A5EE8D1BA5026AA0_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5065[1] = 
{
	SecP192R1Holder_t1838B785B620A28A5D3FD6C3A5EE8D1BA5026AA0_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5066 = { sizeof (SecP224K1Holder_t228EE1894DA5592B605AFA3D429B254F9F22BD13), -1, sizeof(SecP224K1Holder_t228EE1894DA5592B605AFA3D429B254F9F22BD13_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5066[1] = 
{
	SecP224K1Holder_t228EE1894DA5592B605AFA3D429B254F9F22BD13_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5067 = { sizeof (SecP224R1Holder_tA7671CD963C3387945C8A1A15086ACF8E089381A), -1, sizeof(SecP224R1Holder_tA7671CD963C3387945C8A1A15086ACF8E089381A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5067[1] = 
{
	SecP224R1Holder_tA7671CD963C3387945C8A1A15086ACF8E089381A_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5068 = { sizeof (SecP256K1Holder_t8571333793FE4231818C78C6D6C507CEADE3DA6F), -1, sizeof(SecP256K1Holder_t8571333793FE4231818C78C6D6C507CEADE3DA6F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5068[1] = 
{
	SecP256K1Holder_t8571333793FE4231818C78C6D6C507CEADE3DA6F_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5069 = { sizeof (SecP256R1Holder_t359C1A2C94CACF440F21AE4A82F2B6B68AF3AA05), -1, sizeof(SecP256R1Holder_t359C1A2C94CACF440F21AE4A82F2B6B68AF3AA05_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5069[1] = 
{
	SecP256R1Holder_t359C1A2C94CACF440F21AE4A82F2B6B68AF3AA05_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5070 = { sizeof (SecP384R1Holder_t9BBC3EA9F967051B3712E7A6F1B06394B13CA002), -1, sizeof(SecP384R1Holder_t9BBC3EA9F967051B3712E7A6F1B06394B13CA002_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5070[1] = 
{
	SecP384R1Holder_t9BBC3EA9F967051B3712E7A6F1B06394B13CA002_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5071 = { sizeof (SecP521R1Holder_tBDDA59122AC4593188452368A9DF9049BED61D6C), -1, sizeof(SecP521R1Holder_tBDDA59122AC4593188452368A9DF9049BED61D6C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5071[1] = 
{
	SecP521R1Holder_tBDDA59122AC4593188452368A9DF9049BED61D6C_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5072 = { sizeof (SecT113R1Holder_tD74EB859A51B727FE10FE52316FFD6DF1CF0B91C), -1, sizeof(SecT113R1Holder_tD74EB859A51B727FE10FE52316FFD6DF1CF0B91C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5072[1] = 
{
	SecT113R1Holder_tD74EB859A51B727FE10FE52316FFD6DF1CF0B91C_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5073 = { sizeof (SecT113R2Holder_tDDF8D1D2EA30175BC9086D14533E62BE9F55FF20), -1, sizeof(SecT113R2Holder_tDDF8D1D2EA30175BC9086D14533E62BE9F55FF20_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5073[1] = 
{
	SecT113R2Holder_tDDF8D1D2EA30175BC9086D14533E62BE9F55FF20_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5074 = { sizeof (SecT131R1Holder_t4004EE12094FFEC06BC9D42A2D6E8F3BAC64D0E3), -1, sizeof(SecT131R1Holder_t4004EE12094FFEC06BC9D42A2D6E8F3BAC64D0E3_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5074[1] = 
{
	SecT131R1Holder_t4004EE12094FFEC06BC9D42A2D6E8F3BAC64D0E3_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5075 = { sizeof (SecT131R2Holder_t4A2A6B65A23DB1CAC7DE749E4F4C66D2446252C5), -1, sizeof(SecT131R2Holder_t4A2A6B65A23DB1CAC7DE749E4F4C66D2446252C5_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5075[1] = 
{
	SecT131R2Holder_t4A2A6B65A23DB1CAC7DE749E4F4C66D2446252C5_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5076 = { sizeof (SecT163K1Holder_t6C9770BF521BC1D3645D3093BC3E8A4BE9AEC73E), -1, sizeof(SecT163K1Holder_t6C9770BF521BC1D3645D3093BC3E8A4BE9AEC73E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5076[1] = 
{
	SecT163K1Holder_t6C9770BF521BC1D3645D3093BC3E8A4BE9AEC73E_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5077 = { sizeof (SecT163R1Holder_t07B93108864561CDC55E615C5EE3A2DB14366684), -1, sizeof(SecT163R1Holder_t07B93108864561CDC55E615C5EE3A2DB14366684_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5077[1] = 
{
	SecT163R1Holder_t07B93108864561CDC55E615C5EE3A2DB14366684_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5078 = { sizeof (SecT163R2Holder_tB3329D587A5729ACD56A7A366150113ACE218DFC), -1, sizeof(SecT163R2Holder_tB3329D587A5729ACD56A7A366150113ACE218DFC_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5078[1] = 
{
	SecT163R2Holder_tB3329D587A5729ACD56A7A366150113ACE218DFC_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5079 = { sizeof (SecT193R1Holder_tA268FDA70215BD3FFB5FE93B70D7CA3F7A4DEE19), -1, sizeof(SecT193R1Holder_tA268FDA70215BD3FFB5FE93B70D7CA3F7A4DEE19_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5079[1] = 
{
	SecT193R1Holder_tA268FDA70215BD3FFB5FE93B70D7CA3F7A4DEE19_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5080 = { sizeof (SecT193R2Holder_t98F1D65132D0B301FF86BDC108D3BDDDA6BE7159), -1, sizeof(SecT193R2Holder_t98F1D65132D0B301FF86BDC108D3BDDDA6BE7159_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5080[1] = 
{
	SecT193R2Holder_t98F1D65132D0B301FF86BDC108D3BDDDA6BE7159_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5081 = { sizeof (SecT233K1Holder_tC26BBB3F3E2A74AFD990AC6BA8493E63B260E46F), -1, sizeof(SecT233K1Holder_tC26BBB3F3E2A74AFD990AC6BA8493E63B260E46F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5081[1] = 
{
	SecT233K1Holder_tC26BBB3F3E2A74AFD990AC6BA8493E63B260E46F_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5082 = { sizeof (SecT233R1Holder_tB2B079B50850002ED4DECED1374BCB874A8FFFC5), -1, sizeof(SecT233R1Holder_tB2B079B50850002ED4DECED1374BCB874A8FFFC5_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5082[1] = 
{
	SecT233R1Holder_tB2B079B50850002ED4DECED1374BCB874A8FFFC5_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5083 = { sizeof (SecT239K1Holder_tED78A8A5CC4E9F4BD1DF73A16D186058548B539E), -1, sizeof(SecT239K1Holder_tED78A8A5CC4E9F4BD1DF73A16D186058548B539E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5083[1] = 
{
	SecT239K1Holder_tED78A8A5CC4E9F4BD1DF73A16D186058548B539E_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5084 = { sizeof (SecT283K1Holder_tA05B2F0A2D147F514B887C906D2D3B04CCCA714A), -1, sizeof(SecT283K1Holder_tA05B2F0A2D147F514B887C906D2D3B04CCCA714A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5084[1] = 
{
	SecT283K1Holder_tA05B2F0A2D147F514B887C906D2D3B04CCCA714A_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5085 = { sizeof (SecT283R1Holder_t94C45D625CF019BCD0AA54226E8AC39D2CCC6410), -1, sizeof(SecT283R1Holder_t94C45D625CF019BCD0AA54226E8AC39D2CCC6410_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5085[1] = 
{
	SecT283R1Holder_t94C45D625CF019BCD0AA54226E8AC39D2CCC6410_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5086 = { sizeof (SecT409K1Holder_tDFA985A4493E8A407B6A3824B3F4B0FB9973D136), -1, sizeof(SecT409K1Holder_tDFA985A4493E8A407B6A3824B3F4B0FB9973D136_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5086[1] = 
{
	SecT409K1Holder_tDFA985A4493E8A407B6A3824B3F4B0FB9973D136_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5087 = { sizeof (SecT409R1Holder_t543AA73064415D52EC5842DB981CC7965C789AAE), -1, sizeof(SecT409R1Holder_t543AA73064415D52EC5842DB981CC7965C789AAE_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5087[1] = 
{
	SecT409R1Holder_t543AA73064415D52EC5842DB981CC7965C789AAE_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5088 = { sizeof (SecT571K1Holder_t4150E630B7BC0A4DF9ECC9EC64AB909E6E69E487), -1, sizeof(SecT571K1Holder_t4150E630B7BC0A4DF9ECC9EC64AB909E6E69E487_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5088[1] = 
{
	SecT571K1Holder_t4150E630B7BC0A4DF9ECC9EC64AB909E6E69E487_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5089 = { sizeof (SecT571R1Holder_t60AB12FF555AD7B4BE6FF459642EB0079CFC5A5E), -1, sizeof(SecT571R1Holder_t60AB12FF555AD7B4BE6FF459642EB0079CFC5A5E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5089[1] = 
{
	SecT571R1Holder_t60AB12FF555AD7B4BE6FF459642EB0079CFC5A5E_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5090 = { sizeof (Pkcs1Encoding_tB3FC05E5649E6B90D8850455A787609E71FA427B), -1, sizeof(Pkcs1Encoding_tB3FC05E5649E6B90D8850455A787609E71FA427B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5090[8] = 
{
	Pkcs1Encoding_tB3FC05E5649E6B90D8850455A787609E71FA427B_StaticFields::get_offset_of_strictLengthEnabled_0(),
	Pkcs1Encoding_tB3FC05E5649E6B90D8850455A787609E71FA427B::get_offset_of_random_1(),
	Pkcs1Encoding_tB3FC05E5649E6B90D8850455A787609E71FA427B::get_offset_of_engine_2(),
	Pkcs1Encoding_tB3FC05E5649E6B90D8850455A787609E71FA427B::get_offset_of_forEncryption_3(),
	Pkcs1Encoding_tB3FC05E5649E6B90D8850455A787609E71FA427B::get_offset_of_forPrivateKey_4(),
	Pkcs1Encoding_tB3FC05E5649E6B90D8850455A787609E71FA427B::get_offset_of_useStrictLength_5(),
	Pkcs1Encoding_tB3FC05E5649E6B90D8850455A787609E71FA427B::get_offset_of_pLen_6(),
	Pkcs1Encoding_tB3FC05E5649E6B90D8850455A787609E71FA427B::get_offset_of_fallback_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5091 = { sizeof (AesEngine_t4C35E0FD810D2F75EBEF934ACA7F6F9834C294E8), -1, sizeof(AesEngine_t4C35E0FD810D2F75EBEF934ACA7F6F9834C294E8_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5091[12] = 
{
	AesEngine_t4C35E0FD810D2F75EBEF934ACA7F6F9834C294E8_StaticFields::get_offset_of_S_0(),
	AesEngine_t4C35E0FD810D2F75EBEF934ACA7F6F9834C294E8_StaticFields::get_offset_of_Si_1(),
	AesEngine_t4C35E0FD810D2F75EBEF934ACA7F6F9834C294E8_StaticFields::get_offset_of_rcon_2(),
	AesEngine_t4C35E0FD810D2F75EBEF934ACA7F6F9834C294E8_StaticFields::get_offset_of_T0_3(),
	AesEngine_t4C35E0FD810D2F75EBEF934ACA7F6F9834C294E8_StaticFields::get_offset_of_Tinv0_4(),
	AesEngine_t4C35E0FD810D2F75EBEF934ACA7F6F9834C294E8::get_offset_of_ROUNDS_5(),
	AesEngine_t4C35E0FD810D2F75EBEF934ACA7F6F9834C294E8::get_offset_of_WorkingKey_6(),
	AesEngine_t4C35E0FD810D2F75EBEF934ACA7F6F9834C294E8::get_offset_of_C0_7(),
	AesEngine_t4C35E0FD810D2F75EBEF934ACA7F6F9834C294E8::get_offset_of_C1_8(),
	AesEngine_t4C35E0FD810D2F75EBEF934ACA7F6F9834C294E8::get_offset_of_C2_9(),
	AesEngine_t4C35E0FD810D2F75EBEF934ACA7F6F9834C294E8::get_offset_of_C3_10(),
	AesEngine_t4C35E0FD810D2F75EBEF934ACA7F6F9834C294E8::get_offset_of_forEncryption_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5092 = { sizeof (CamelliaEngine_t7A84F7BB09CA0382510AE0D93B8110274195483E), -1, sizeof(CamelliaEngine_t7A84F7BB09CA0382510AE0D93B8110274195483E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5092[11] = 
{
	CamelliaEngine_t7A84F7BB09CA0382510AE0D93B8110274195483E::get_offset_of_initialised_0(),
	CamelliaEngine_t7A84F7BB09CA0382510AE0D93B8110274195483E::get_offset_of__keyIs128_1(),
	CamelliaEngine_t7A84F7BB09CA0382510AE0D93B8110274195483E::get_offset_of_subkey_2(),
	CamelliaEngine_t7A84F7BB09CA0382510AE0D93B8110274195483E::get_offset_of_kw_3(),
	CamelliaEngine_t7A84F7BB09CA0382510AE0D93B8110274195483E::get_offset_of_ke_4(),
	CamelliaEngine_t7A84F7BB09CA0382510AE0D93B8110274195483E::get_offset_of_state_5(),
	CamelliaEngine_t7A84F7BB09CA0382510AE0D93B8110274195483E_StaticFields::get_offset_of_SIGMA_6(),
	CamelliaEngine_t7A84F7BB09CA0382510AE0D93B8110274195483E_StaticFields::get_offset_of_SBOX1_1110_7(),
	CamelliaEngine_t7A84F7BB09CA0382510AE0D93B8110274195483E_StaticFields::get_offset_of_SBOX4_4404_8(),
	CamelliaEngine_t7A84F7BB09CA0382510AE0D93B8110274195483E_StaticFields::get_offset_of_SBOX2_0222_9(),
	CamelliaEngine_t7A84F7BB09CA0382510AE0D93B8110274195483E_StaticFields::get_offset_of_SBOX3_3033_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5093 = { sizeof (ChaChaEngine_t6CC53FC5D18C19BC24AF41B6A6D0B073CA6A36B2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5094 = { sizeof (DesEdeEngine_t08325DC827B79EB29E302109606CA98232397B25), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5094[4] = 
{
	DesEdeEngine_t08325DC827B79EB29E302109606CA98232397B25::get_offset_of_workingKey1_14(),
	DesEdeEngine_t08325DC827B79EB29E302109606CA98232397B25::get_offset_of_workingKey2_15(),
	DesEdeEngine_t08325DC827B79EB29E302109606CA98232397B25::get_offset_of_workingKey3_16(),
	DesEdeEngine_t08325DC827B79EB29E302109606CA98232397B25::get_offset_of_forEncryption_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5095 = { sizeof (DesEngine_t88E9C8F289740CDAE4C6C0EAEABA2059935EF450), -1, sizeof(DesEngine_t88E9C8F289740CDAE4C6C0EAEABA2059935EF450_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5095[14] = 
{
	DesEngine_t88E9C8F289740CDAE4C6C0EAEABA2059935EF450::get_offset_of_workingKey_0(),
	DesEngine_t88E9C8F289740CDAE4C6C0EAEABA2059935EF450_StaticFields::get_offset_of_bytebit_1(),
	DesEngine_t88E9C8F289740CDAE4C6C0EAEABA2059935EF450_StaticFields::get_offset_of_bigbyte_2(),
	DesEngine_t88E9C8F289740CDAE4C6C0EAEABA2059935EF450_StaticFields::get_offset_of_pc1_3(),
	DesEngine_t88E9C8F289740CDAE4C6C0EAEABA2059935EF450_StaticFields::get_offset_of_totrot_4(),
	DesEngine_t88E9C8F289740CDAE4C6C0EAEABA2059935EF450_StaticFields::get_offset_of_pc2_5(),
	DesEngine_t88E9C8F289740CDAE4C6C0EAEABA2059935EF450_StaticFields::get_offset_of_SP1_6(),
	DesEngine_t88E9C8F289740CDAE4C6C0EAEABA2059935EF450_StaticFields::get_offset_of_SP2_7(),
	DesEngine_t88E9C8F289740CDAE4C6C0EAEABA2059935EF450_StaticFields::get_offset_of_SP3_8(),
	DesEngine_t88E9C8F289740CDAE4C6C0EAEABA2059935EF450_StaticFields::get_offset_of_SP4_9(),
	DesEngine_t88E9C8F289740CDAE4C6C0EAEABA2059935EF450_StaticFields::get_offset_of_SP5_10(),
	DesEngine_t88E9C8F289740CDAE4C6C0EAEABA2059935EF450_StaticFields::get_offset_of_SP6_11(),
	DesEngine_t88E9C8F289740CDAE4C6C0EAEABA2059935EF450_StaticFields::get_offset_of_SP7_12(),
	DesEngine_t88E9C8F289740CDAE4C6C0EAEABA2059935EF450_StaticFields::get_offset_of_SP8_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5096 = { sizeof (Gost28147Engine_tDB141472A559444EDC7FBFC2FA573A33F542D532), -1, sizeof(Gost28147Engine_tDB141472A559444EDC7FBFC2FA573A33F542D532_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5096[12] = 
{
	Gost28147Engine_tDB141472A559444EDC7FBFC2FA573A33F542D532::get_offset_of_workingKey_0(),
	Gost28147Engine_tDB141472A559444EDC7FBFC2FA573A33F542D532::get_offset_of_forEncryption_1(),
	Gost28147Engine_tDB141472A559444EDC7FBFC2FA573A33F542D532::get_offset_of_S_2(),
	Gost28147Engine_tDB141472A559444EDC7FBFC2FA573A33F542D532_StaticFields::get_offset_of_Sbox_Default_3(),
	Gost28147Engine_tDB141472A559444EDC7FBFC2FA573A33F542D532_StaticFields::get_offset_of_ESbox_Test_4(),
	Gost28147Engine_tDB141472A559444EDC7FBFC2FA573A33F542D532_StaticFields::get_offset_of_ESbox_A_5(),
	Gost28147Engine_tDB141472A559444EDC7FBFC2FA573A33F542D532_StaticFields::get_offset_of_ESbox_B_6(),
	Gost28147Engine_tDB141472A559444EDC7FBFC2FA573A33F542D532_StaticFields::get_offset_of_ESbox_C_7(),
	Gost28147Engine_tDB141472A559444EDC7FBFC2FA573A33F542D532_StaticFields::get_offset_of_ESbox_D_8(),
	Gost28147Engine_tDB141472A559444EDC7FBFC2FA573A33F542D532_StaticFields::get_offset_of_DSbox_Test_9(),
	Gost28147Engine_tDB141472A559444EDC7FBFC2FA573A33F542D532_StaticFields::get_offset_of_DSbox_A_10(),
	Gost28147Engine_tDB141472A559444EDC7FBFC2FA573A33F542D532_StaticFields::get_offset_of_sBoxes_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5097 = { sizeof (RC4Engine_t7DD4678C1AFD6D45B92C008CE07A2F9E7D81A576), -1, sizeof(RC4Engine_t7DD4678C1AFD6D45B92C008CE07A2F9E7D81A576_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5097[5] = 
{
	RC4Engine_t7DD4678C1AFD6D45B92C008CE07A2F9E7D81A576_StaticFields::get_offset_of_STATE_LENGTH_0(),
	RC4Engine_t7DD4678C1AFD6D45B92C008CE07A2F9E7D81A576::get_offset_of_engineState_1(),
	RC4Engine_t7DD4678C1AFD6D45B92C008CE07A2F9E7D81A576::get_offset_of_x_2(),
	RC4Engine_t7DD4678C1AFD6D45B92C008CE07A2F9E7D81A576::get_offset_of_y_3(),
	RC4Engine_t7DD4678C1AFD6D45B92C008CE07A2F9E7D81A576::get_offset_of_workingKey_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5098 = { sizeof (RsaBlindedEngine_tEBCD12A0EB19941C48DD1394340083BA0F2CAB9C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5098[3] = 
{
	RsaBlindedEngine_tEBCD12A0EB19941C48DD1394340083BA0F2CAB9C::get_offset_of_core_0(),
	RsaBlindedEngine_tEBCD12A0EB19941C48DD1394340083BA0F2CAB9C::get_offset_of_key_1(),
	RsaBlindedEngine_tEBCD12A0EB19941C48DD1394340083BA0F2CAB9C::get_offset_of_random_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5099 = { sizeof (RsaCoreEngine_t34DD3F15CAE1DA560B6C6C8E475F6951E197DC64), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5099[3] = 
{
	RsaCoreEngine_t34DD3F15CAE1DA560B6C6C8E475F6951E197DC64::get_offset_of_key_0(),
	RsaCoreEngine_t34DD3F15CAE1DA560B6C6C8E475F6951E197DC64::get_offset_of_forEncryption_1(),
	RsaCoreEngine_t34DD3F15CAE1DA560B6C6C8E475F6951E197DC64::get_offset_of_bitSize_2(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
